	.text
	.file	"PpmdDecoder.bc"
	.globl	_ZN9NCompress5NPpmd8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoderD2Ev,@function
_ZN9NCompress5NPpmd8CDecoderD2Ev:       # @_ZN9NCompress5NPpmd8CDecoderD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress5NPpmd8CDecoderE+120, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NPpmd8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress5NPpmd8CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NPpmd8CDecoderE+184, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	$_ZTVN9NCompress5NPpmd8CDecoderE+320, 32(%rbx)
	movq	48(%rbx), %rdi
.Ltmp0:
	callq	MidFree
.Ltmp1:
# BB#1:
	leaq	160(%rbx), %rdi
.Ltmp2:
	movl	$_ZN9NCompress5NPpmdL10g_BigAllocE, %esi
	callq	Ppmd7_Free
.Ltmp3:
# BB#2:
	movq	19368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp7:
	callq	*16(%rax)
.Ltmp8:
.LBB0_4:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	addq	$96, %rbx
.Ltmp13:
	movq	%rbx, %rdi
	callq	_ZN14CByteInBufWrap4FreeEv
.Ltmp14:
# BB#5:                                 # %_ZN14CByteInBufWrapD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_9:
.Ltmp9:
	movq	%rax, %r14
	jmp	.LBB0_10
.LBB0_8:
.Ltmp15:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_6:
.Ltmp4:
	movq	%rax, %r14
	movq	19368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_10
# BB#7:
	movq	(%rdi), %rax
.Ltmp5:
	callq	*16(%rax)
.Ltmp6:
.LBB0_10:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit12
	addq	$96, %rbx
.Ltmp10:
	movq	%rbx, %rdi
	callq	_ZN14CByteInBufWrap4FreeEv
.Ltmp11:
# BB#11:                                # %_ZN14CByteInBufWrapD2Ev.exit10
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_12:
.Ltmp12:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN9NCompress5NPpmd8CDecoderD2Ev, .Lfunc_end0-_ZN9NCompress5NPpmd8CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp5-.Ltmp14          #   Call between .Ltmp14 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp11-.Ltmp5          #   Call between .Ltmp5 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	1                       #   On action: 1
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Lfunc_end0-.Ltmp11     #   Call between .Ltmp11 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZThn8_N9NCompress5NPpmd8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CDecoderD1Ev,@function
_ZThn8_N9NCompress5NPpmd8CDecoderD1Ev:  # @_ZThn8_N9NCompress5NPpmd8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress5NPpmd8CDecoderD2Ev # TAILCALL
.Lfunc_end2:
	.size	_ZThn8_N9NCompress5NPpmd8CDecoderD1Ev, .Lfunc_end2-_ZThn8_N9NCompress5NPpmd8CDecoderD1Ev
	.cfi_endproc

	.globl	_ZThn16_N9NCompress5NPpmd8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CDecoderD1Ev,@function
_ZThn16_N9NCompress5NPpmd8CDecoderD1Ev: # @_ZThn16_N9NCompress5NPpmd8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress5NPpmd8CDecoderD2Ev # TAILCALL
.Lfunc_end3:
	.size	_ZThn16_N9NCompress5NPpmd8CDecoderD1Ev, .Lfunc_end3-_ZThn16_N9NCompress5NPpmd8CDecoderD1Ev
	.cfi_endproc

	.globl	_ZThn24_N9NCompress5NPpmd8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress5NPpmd8CDecoderD1Ev,@function
_ZThn24_N9NCompress5NPpmd8CDecoderD1Ev: # @_ZThn24_N9NCompress5NPpmd8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress5NPpmd8CDecoderD2Ev # TAILCALL
.Lfunc_end4:
	.size	_ZThn24_N9NCompress5NPpmd8CDecoderD1Ev, .Lfunc_end4-_ZThn24_N9NCompress5NPpmd8CDecoderD1Ev
	.cfi_endproc

	.globl	_ZThn32_N9NCompress5NPpmd8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress5NPpmd8CDecoderD1Ev,@function
_ZThn32_N9NCompress5NPpmd8CDecoderD1Ev: # @_ZThn32_N9NCompress5NPpmd8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress5NPpmd8CDecoderD2Ev # TAILCALL
.Lfunc_end5:
	.size	_ZThn32_N9NCompress5NPpmd8CDecoderD1Ev, .Lfunc_end5-_ZThn32_N9NCompress5NPpmd8CDecoderD1Ev
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoderD0Ev,@function
_ZN9NCompress5NPpmd8CDecoderD0Ev:       # @_ZN9NCompress5NPpmd8CDecoderD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp16:
	callq	_ZN9NCompress5NPpmd8CDecoderD2Ev
.Ltmp17:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_2:
.Ltmp18:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN9NCompress5NPpmd8CDecoderD0Ev, .Lfunc_end6-_ZN9NCompress5NPpmd8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp17     #   Call between .Ltmp17 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress5NPpmd8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CDecoderD0Ev,@function
_ZThn8_N9NCompress5NPpmd8CDecoderD0Ev:  # @_ZThn8_N9NCompress5NPpmd8CDecoderD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp19:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CDecoderD2Ev
.Ltmp20:
# BB#1:                                 # %_ZN9NCompress5NPpmd8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB7_2:
.Ltmp21:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZThn8_N9NCompress5NPpmd8CDecoderD0Ev, .Lfunc_end7-_ZThn8_N9NCompress5NPpmd8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin2   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn16_N9NCompress5NPpmd8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CDecoderD0Ev,@function
_ZThn16_N9NCompress5NPpmd8CDecoderD0Ev: # @_ZThn16_N9NCompress5NPpmd8CDecoderD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp22:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CDecoderD2Ev
.Ltmp23:
# BB#1:                                 # %_ZN9NCompress5NPpmd8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB8_2:
.Ltmp24:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZThn16_N9NCompress5NPpmd8CDecoderD0Ev, .Lfunc_end8-_ZThn16_N9NCompress5NPpmd8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin3   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end8-.Ltmp23     #   Call between .Ltmp23 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn24_N9NCompress5NPpmd8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress5NPpmd8CDecoderD0Ev,@function
_ZThn24_N9NCompress5NPpmd8CDecoderD0Ev: # @_ZThn24_N9NCompress5NPpmd8CDecoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp25:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CDecoderD2Ev
.Ltmp26:
# BB#1:                                 # %_ZN9NCompress5NPpmd8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB9_2:
.Ltmp27:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZThn24_N9NCompress5NPpmd8CDecoderD0Ev, .Lfunc_end9-_ZThn24_N9NCompress5NPpmd8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp25-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin4   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end9-.Ltmp26     #   Call between .Ltmp26 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn32_N9NCompress5NPpmd8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress5NPpmd8CDecoderD0Ev,@function
_ZThn32_N9NCompress5NPpmd8CDecoderD0Ev: # @_ZThn32_N9NCompress5NPpmd8CDecoderD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-32, %rbx
.Ltmp28:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CDecoderD2Ev
.Ltmp29:
# BB#1:                                 # %_ZN9NCompress5NPpmd8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB10_2:
.Ltmp30:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZThn32_N9NCompress5NPpmd8CDecoderD0Ev, .Lfunc_end10-_ZThn32_N9NCompress5NPpmd8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp28-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin5   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end10-.Ltmp29    #   Call between .Ltmp29 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj,@function
_ZN9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj: # @_ZN9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$5, %edx
	jb	.LBB11_6
# BB#1:
	movb	(%rsi), %cl
	movb	%cl, 19344(%rbx)
	movl	$-2147467263, %eax      # imm = 0x80004001
	cmpb	$2, %cl
	jb	.LBB11_6
# BB#2:
	cmpb	$64, %cl
	ja	.LBB11_6
# BB#3:
	movl	1(%rsi), %r14d
	leal	-2048(%r14), %ecx
	cmpl	$-2085, %ecx            # imm = 0xF7DB
	ja	.LBB11_6
# BB#4:
	leaq	96(%rbx), %rdi
	movl	$1048576, %esi          # imm = 0x100000
	callq	_ZN14CByteInBufWrap5AllocEj
	movl	%eax, %ecx
	movl	$-2147024882, %eax      # imm = 0x8007000E
	testb	%cl, %cl
	je	.LBB11_6
# BB#5:
	addq	$160, %rbx
	movl	$_ZN9NCompress5NPpmdL10g_BigAllocE, %edx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	Ppmd7_Alloc
	xorl	%ecx, %ecx
	testl	%eax, %eax
	movl	$-2147024882, %eax      # imm = 0x8007000E
	cmovnel	%ecx, %eax
.LBB11_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	_ZN9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end11-_ZN9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj,@function
_ZThn8_N9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj: # @_ZThn8_N9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$5, %edx
	jb	.LBB12_6
# BB#1:
	addq	$-8, %rbx
	movb	(%rsi), %cl
	movb	%cl, 19344(%rbx)
	movl	$-2147467263, %eax      # imm = 0x80004001
	cmpb	$2, %cl
	jb	.LBB12_6
# BB#2:
	cmpb	$64, %cl
	ja	.LBB12_6
# BB#3:
	movl	1(%rsi), %r14d
	leal	-2048(%r14), %ecx
	cmpl	$-2085, %ecx            # imm = 0xF7DB
	ja	.LBB12_6
# BB#4:
	leaq	96(%rbx), %rdi
	movl	$1048576, %esi          # imm = 0x100000
	callq	_ZN14CByteInBufWrap5AllocEj
	movl	%eax, %ecx
	movl	$-2147024882, %eax      # imm = 0x8007000E
	testb	%cl, %cl
	je	.LBB12_6
# BB#5:
	addq	$160, %rbx
	movl	$_ZN9NCompress5NPpmdL10g_BigAllocE, %edx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	Ppmd7_Alloc
	xorl	%ecx, %ecx
	testl	%eax, %eax
	movl	$-2147024882, %eax      # imm = 0x8007000E
	cmovnel	%ecx, %eax
.LBB12_6:                               # %_ZN9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	_ZThn8_N9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end12-_ZThn8_N9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj,@function
_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj: # @_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 64
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %r13
	movl	19348(%r13), %eax
	xorl	%ebx, %ebx
	testl	%eax, %eax
	je	.LBB13_4
# BB#1:
	cmpl	$2, %eax
	je	.LBB13_21
# BB#2:
	cmpl	$3, %eax
	jne	.LBB13_6
	jmp	.LBB13_20
.LBB13_4:
	movq	120(%r13), %rax
	movq	%rax, 104(%r13)
	movq	%rax, 112(%r13)
	movq	$0, 144(%r13)
	movb	$0, 152(%r13)
	movl	$0, 156(%r13)
	leaq	56(%r13), %rdi
	callq	Ppmd7z_RangeDec_Init
	testl	%eax, %eax
	je	.LBB13_19
# BB#5:
	movl	$1, 19348(%r13)
	leaq	160(%r13), %rdi
	movzbl	19344(%r13), %esi
	callq	Ppmd7_Init
.LBB13_6:
	cmpb	$0, 19345(%r13)
	je	.LBB13_8
# BB#7:
	movq	19352(%r13), %rax
	subq	19360(%r13), %rax
	movl	%ebp, %ecx
	cmpq	%rax, %rcx
	cmovbel	%ebp, %eax
	movl	%eax, %ebp
.LBB13_8:
	testl	%ebp, %ebp
	je	.LBB13_14
# BB#9:                                 # %.lr.ph
	leaq	160(%r13), %r15
	leaq	56(%r13), %rbx
	leaq	152(%r13), %r14
	movl	%ebp, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_10:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	Ppmd7_DecodeSymbol
	testl	%eax, %eax
	js	.LBB13_13
# BB#11:                                #   in Loop: Header=BB13_10 Depth=1
	movzbl	(%r14), %ecx
	testb	%cl, %cl
	jne	.LBB13_13
# BB#12:                                #   in Loop: Header=BB13_10 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movb	%al, (%rcx,%rbp)
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB13_10
.LBB13_13:                              # %.._crit_edge.loopexit_crit_edge
	xorl	%ebx, %ebx
	jmp	.LBB13_15
.LBB13_14:                              # %.._crit_edge_crit_edge
	leaq	152(%r13), %r14
	xorl	%ebp, %ebp
	xorl	%eax, %eax
.LBB13_15:                              # %._crit_edge
	movl	%ebp, %ecx
	addq	%rcx, 19360(%r13)
	cmpb	$0, (%r14)
	je	.LBB13_17
# BB#16:
	movl	$3, 19348(%r13)
	movl	156(%r13), %ebx
	jmp	.LBB13_21
.LBB13_17:
	testl	%eax, %eax
	jns	.LBB13_21
# BB#18:
	xorl	%ecx, %ecx
	cmpl	$-1, %eax
	setne	%cl
	orl	$2, %ecx
	movl	%ecx, 19348(%r13)
	jmp	.LBB13_21
.LBB13_19:
	movl	$3, 19348(%r13)
.LBB13_20:
	movl	$1, %ebx
.LBB13_21:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj, .Lfunc_end13-_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress5NPpmd8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress5NPpmd8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 64
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	cmpq	$0, 48(%rbx)
	jne	.LBB14_3
# BB#1:
	movl	$1048576, %edi          # imm = 0x100000
	callq	MidAlloc
	movq	%rax, 48(%rbx)
	testq	%rax, %rax
	je	.LBB14_2
.LBB14_3:
	movq	%rbp, 136(%rbx)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	*56(%rax)
	movq	19360(%rbx), %rbp
	movq	48(%rbx), %rsi
	movl	$1048576, %edx          # imm = 0x100000
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj
	movl	%eax, %r13d
	movq	19360(%rbx), %rdx
	subq	%rbp, %rdx
	movq	48(%rbx), %rsi
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	movl	%eax, %ebp
	testq	%r15, %r15
	je	.LBB14_12
# BB#4:                                 # %.critedge.preheader
	testl	%ebp, %ebp
	jne	.LBB14_20
# BB#5:
	leaq	19360(%rbx), %r12
	movl	%r13d, %ebp
	.p2align	4, 0x90
.LBB14_6:                               # %.lr.ph56
                                        # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	jne	.LBB14_20
# BB#7:                                 #   in Loop: Header=BB14_6 Depth=1
	xorl	%ebp, %ebp
	cmpl	$2, 19348(%rbx)
	je	.LBB14_20
# BB#8:                                 #   in Loop: Header=BB14_6 Depth=1
	movq	104(%rbx), %rax
	addq	144(%rbx), %rax
	subq	120(%rbx), %rax
	movq	%rax, (%rsp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%rsp, %rsi
	movq	%r12, %rdx
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB14_19
# BB#9:                                 # %.thread
                                        #   in Loop: Header=BB14_6 Depth=1
	cmpb	$0, 19345(%rbx)
	je	.LBB14_11
# BB#10:                                #   in Loop: Header=BB14_6 Depth=1
	movq	19360(%rbx), %rax
	cmpq	19352(%rbx), %rax
	jae	.LBB14_20
.LBB14_11:                              # %.critedge.backedge
                                        #   in Loop: Header=BB14_6 Depth=1
	movq	19360(%rbx), %r13
	movq	48(%rbx), %rsi
	movl	$1048576, %edx          # imm = 0x100000
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj
	movl	%eax, %ebp
	movq	19360(%rbx), %rdx
	subq	%r13, %rdx
	movq	48(%rbx), %rsi
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	je	.LBB14_6
	jmp	.LBB14_19
.LBB14_12:                              # %.critedge.us.preheader
	testl	%ebp, %ebp
	jne	.LBB14_20
# BB#13:                                # %.lr.ph.preheader
	movl	%r13d, %ebp
	.p2align	4, 0x90
.LBB14_14:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	jne	.LBB14_20
# BB#15:                                #   in Loop: Header=BB14_14 Depth=1
	xorl	%ebp, %ebp
	cmpl	$2, 19348(%rbx)
	je	.LBB14_20
# BB#16:                                # %.thread.us
                                        #   in Loop: Header=BB14_14 Depth=1
	cmpb	$0, 19345(%rbx)
	je	.LBB14_18
# BB#17:                                #   in Loop: Header=BB14_14 Depth=1
	movq	19360(%rbx), %rax
	cmpq	19352(%rbx), %rax
	jae	.LBB14_20
.LBB14_18:                              # %.critedge.us.backedge
                                        #   in Loop: Header=BB14_14 Depth=1
	movq	19360(%rbx), %r15
	movq	48(%rbx), %rsi
	movl	$1048576, %edx          # imm = 0x100000
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj
	movl	%eax, %ebp
	movq	19360(%rbx), %rdx
	subq	%r15, %rdx
	movq	48(%rbx), %rsi
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	je	.LBB14_14
.LBB14_19:
	movl	%eax, %ebp
	jmp	.LBB14_20
.LBB14_2:
	movl	$-2147024882, %ebp      # imm = 0x8007000E
.LBB14_20:                              # %.thread42
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN9NCompress5NPpmd8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end14-_ZN9NCompress5NPpmd8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy,@function
_ZN9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy: # @_ZN9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	setne	19345(%rdi)
	je	.LBB15_2
# BB#1:
	movq	(%rsi), %rax
	movq	%rax, 19352(%rdi)
.LBB15_2:
	movq	$0, 19360(%rdi)
	movl	$0, 19348(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end15:
	.size	_ZN9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy, .Lfunc_end15-_ZN9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZThn24_N9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy,@function
_ZThn24_N9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy: # @_ZThn24_N9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	setne	19321(%rdi)
	je	.LBB16_2
# BB#1:
	movq	(%rsi), %rax
	movq	%rax, 19328(%rdi)
.LBB16_2:                               # %_ZN9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy.exit
	movq	$0, 19336(%rdi)
	movl	$0, 19324(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	_ZThn24_N9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy, .Lfunc_end16-_ZThn24_N9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream,@function
_ZN9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream: # @_ZN9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -24
.Lcfi70:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB17_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB17_2:
	movq	19368(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB17_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB17_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%rbx, 19368(%r14)
	movq	%rbx, 136(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end17:
	.size	_ZN9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end17-_ZN9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn16_N9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream,@function
_ZThn16_N9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream: # @_ZThn16_N9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -24
.Lcfi75:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB18_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB18_2:
	movq	19352(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB18_4:                               # %_ZN9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream.exit
	movq	%rbx, 19352(%r14)
	movq	%rbx, 120(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	_ZThn16_N9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream, .Lfunc_end18-_ZThn16_N9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder15ReleaseInStreamEv,@function
_ZN9NCompress5NPpmd8CDecoder15ReleaseInStreamEv: # @_ZN9NCompress5NPpmd8CDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 16
.Lcfi77:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	19368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 19368(%rbx)
.LBB19_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end19:
	.size	_ZN9NCompress5NPpmd8CDecoder15ReleaseInStreamEv, .Lfunc_end19-_ZN9NCompress5NPpmd8CDecoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZThn16_N9NCompress5NPpmd8CDecoder15ReleaseInStreamEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CDecoder15ReleaseInStreamEv,@function
_ZThn16_N9NCompress5NPpmd8CDecoder15ReleaseInStreamEv: # @_ZThn16_N9NCompress5NPpmd8CDecoder15ReleaseInStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 16
.Lcfi79:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	19352(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 19352(%rbx)
.LBB20_2:                               # %_ZN9NCompress5NPpmd8CDecoder15ReleaseInStreamEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end20:
	.size	_ZThn16_N9NCompress5NPpmd8CDecoder15ReleaseInStreamEv, .Lfunc_end20-_ZThn16_N9NCompress5NPpmd8CDecoder15ReleaseInStreamEv
	.cfi_endproc

	.globl	_ZN9NCompress5NPpmd8CDecoder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder4ReadEPvjPj,@function
_ZN9NCompress5NPpmd8CDecoder4ReadEPvjPj: # @_ZN9NCompress5NPpmd8CDecoder4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 32
.Lcfi83:
	.cfi_offset %rbx, -32
.Lcfi84:
	.cfi_offset %r14, -24
.Lcfi85:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	movq	19360(%rbx), %r15
	callq	_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj
	testq	%r14, %r14
	je	.LBB21_2
# BB#1:
	movl	19360(%rbx), %ecx
	subl	%r15d, %ecx
	movl	%ecx, (%r14)
.LBB21_2:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	_ZN9NCompress5NPpmd8CDecoder4ReadEPvjPj, .Lfunc_end21-_ZN9NCompress5NPpmd8CDecoder4ReadEPvjPj
	.cfi_endproc

	.globl	_ZThn32_N9NCompress5NPpmd8CDecoder4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress5NPpmd8CDecoder4ReadEPvjPj,@function
_ZThn32_N9NCompress5NPpmd8CDecoder4ReadEPvjPj: # @_ZThn32_N9NCompress5NPpmd8CDecoder4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 32
.Lcfi89:
	.cfi_offset %rbx, -32
.Lcfi90:
	.cfi_offset %r14, -24
.Lcfi91:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	movq	19328(%rbx), %r15
	addq	$-32, %rbx
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CDecoder8CodeSpecEPhj
	testq	%r14, %r14
	je	.LBB22_2
# BB#1:
	movl	19360(%rbx), %ecx
	subl	%r15d, %ecx
	movl	%ecx, (%r14)
.LBB22_2:                               # %_ZN9NCompress5NPpmd8CDecoder4ReadEPvjPj.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	_ZThn32_N9NCompress5NPpmd8CDecoder4ReadEPvjPj, .Lfunc_end22-_ZThn32_N9NCompress5NPpmd8CDecoder4ReadEPvjPj
	.cfi_endproc

	.section	.text._ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB23_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB23_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB23_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB23_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB23_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB23_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB23_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB23_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB23_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB23_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB23_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB23_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB23_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB23_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB23_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB23_16
.LBB23_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressSetDecoderProperties2(%rip), %cl
	jne	.LBB23_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+1(%rip), %al
	jne	.LBB23_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+2(%rip), %al
	jne	.LBB23_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+3(%rip), %al
	jne	.LBB23_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+4(%rip), %al
	jne	.LBB23_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+5(%rip), %al
	jne	.LBB23_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+6(%rip), %al
	jne	.LBB23_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+7(%rip), %al
	jne	.LBB23_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+8(%rip), %al
	jne	.LBB23_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+9(%rip), %al
	jne	.LBB23_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+10(%rip), %al
	jne	.LBB23_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+11(%rip), %al
	jne	.LBB23_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+12(%rip), %al
	jne	.LBB23_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+13(%rip), %al
	jne	.LBB23_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+14(%rip), %al
	jne	.LBB23_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetDecoderProperties2+15(%rip), %al
	jne	.LBB23_33
.LBB23_16:
	leaq	8(%rdi), %rax
	jmp	.LBB23_84
.LBB23_33:                              # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_ICompressSetInStream(%rip), %cl
	jne	.LBB23_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetInStream+1(%rip), %al
	jne	.LBB23_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetInStream+2(%rip), %al
	jne	.LBB23_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetInStream+3(%rip), %al
	jne	.LBB23_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetInStream+4(%rip), %al
	jne	.LBB23_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetInStream+5(%rip), %al
	jne	.LBB23_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetInStream+6(%rip), %al
	jne	.LBB23_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetInStream+7(%rip), %al
	jne	.LBB23_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetInStream+8(%rip), %al
	jne	.LBB23_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetInStream+9(%rip), %al
	jne	.LBB23_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetInStream+10(%rip), %al
	jne	.LBB23_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetInStream+11(%rip), %al
	jne	.LBB23_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetInStream+12(%rip), %al
	jne	.LBB23_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetInStream+13(%rip), %al
	jne	.LBB23_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetInStream+14(%rip), %al
	jne	.LBB23_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetInStream+15(%rip), %al
	jne	.LBB23_50
# BB#49:
	leaq	16(%rdi), %rax
	jmp	.LBB23_84
.LBB23_50:                              # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_ICompressSetOutStreamSize(%rip), %cl
	jne	.LBB23_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+1(%rip), %al
	jne	.LBB23_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+2(%rip), %al
	jne	.LBB23_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+3(%rip), %al
	jne	.LBB23_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+4(%rip), %al
	jne	.LBB23_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+5(%rip), %al
	jne	.LBB23_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+6(%rip), %al
	jne	.LBB23_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+7(%rip), %al
	jne	.LBB23_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+8(%rip), %al
	jne	.LBB23_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+9(%rip), %al
	jne	.LBB23_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+10(%rip), %al
	jne	.LBB23_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+11(%rip), %al
	jne	.LBB23_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+12(%rip), %al
	jne	.LBB23_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+13(%rip), %al
	jne	.LBB23_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+14(%rip), %al
	jne	.LBB23_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+15(%rip), %al
	jne	.LBB23_67
# BB#66:
	leaq	24(%rdi), %rax
	jmp	.LBB23_84
.LBB23_67:                              # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISequentialInStream(%rip), %cl
	jne	.LBB23_85
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_ISequentialInStream+1(%rip), %cl
	jne	.LBB23_85
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_ISequentialInStream+2(%rip), %cl
	jne	.LBB23_85
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_ISequentialInStream+3(%rip), %cl
	jne	.LBB23_85
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_ISequentialInStream+4(%rip), %cl
	jne	.LBB23_85
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_ISequentialInStream+5(%rip), %cl
	jne	.LBB23_85
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_ISequentialInStream+6(%rip), %cl
	jne	.LBB23_85
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_ISequentialInStream+7(%rip), %cl
	jne	.LBB23_85
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_ISequentialInStream+8(%rip), %cl
	jne	.LBB23_85
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_ISequentialInStream+9(%rip), %cl
	jne	.LBB23_85
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_ISequentialInStream+10(%rip), %cl
	jne	.LBB23_85
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_ISequentialInStream+11(%rip), %cl
	jne	.LBB23_85
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_ISequentialInStream+12(%rip), %cl
	jne	.LBB23_85
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_ISequentialInStream+13(%rip), %cl
	jne	.LBB23_85
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_ISequentialInStream+14(%rip), %cl
	jne	.LBB23_85
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_ISequentialInStream+15(%rip), %cl
	jne	.LBB23_85
# BB#83:
	leaq	32(%rdi), %rax
.LBB23_84:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB23_85:                              # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end23:
	.size	_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end23-_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress5NPpmd8CDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress5NPpmd8CDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress5NPpmd8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder6AddRefEv,@function
_ZN9NCompress5NPpmd8CDecoder6AddRefEv:  # @_ZN9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	40(%rdi), %eax
	incl	%eax
	movl	%eax, 40(%rdi)
	retq
.Lfunc_end24:
	.size	_ZN9NCompress5NPpmd8CDecoder6AddRefEv, .Lfunc_end24-_ZN9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress5NPpmd8CDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress5NPpmd8CDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress5NPpmd8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoder7ReleaseEv,@function
_ZN9NCompress5NPpmd8CDecoder7ReleaseEv: # @_ZN9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 16
	movl	40(%rdi), %eax
	decl	%eax
	movl	%eax, 40(%rdi)
	jne	.LBB25_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB25_2:
	popq	%rcx
	retq
.Lfunc_end25:
	.size	_ZN9NCompress5NPpmd8CDecoder7ReleaseEv, .Lfunc_end25-_ZN9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end26:
	.size	_ZThn8_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end26-_ZThn8_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NPpmd8CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress5NPpmd8CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress5NPpmd8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CDecoder6AddRefEv,@function
_ZThn8_N9NCompress5NPpmd8CDecoder6AddRefEv: # @_ZThn8_N9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end27:
	.size	_ZThn8_N9NCompress5NPpmd8CDecoder6AddRefEv, .Lfunc_end27-_ZThn8_N9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NPpmd8CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress5NPpmd8CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NPpmd8CDecoder7ReleaseEv,@function
_ZThn8_N9NCompress5NPpmd8CDecoder7ReleaseEv: # @_ZThn8_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB28_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB28_2:                               # %_ZN9NCompress5NPpmd8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end28:
	.size	_ZThn8_N9NCompress5NPpmd8CDecoder7ReleaseEv, .Lfunc_end28-_ZThn8_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end29:
	.size	_ZThn16_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end29-_ZThn16_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress5NPpmd8CDecoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress5NPpmd8CDecoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress5NPpmd8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CDecoder6AddRefEv,@function
_ZThn16_N9NCompress5NPpmd8CDecoder6AddRefEv: # @_ZThn16_N9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end30:
	.size	_ZThn16_N9NCompress5NPpmd8CDecoder6AddRefEv, .Lfunc_end30-_ZThn16_N9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress5NPpmd8CDecoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress5NPpmd8CDecoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NPpmd8CDecoder7ReleaseEv,@function
_ZThn16_N9NCompress5NPpmd8CDecoder7ReleaseEv: # @_ZThn16_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB31_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB31_2:                               # %_ZN9NCompress5NPpmd8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end31:
	.size	_ZThn16_N9NCompress5NPpmd8CDecoder7ReleaseEv, .Lfunc_end31-_ZThn16_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end32:
	.size	_ZThn24_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end32-_ZThn24_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress5NPpmd8CDecoder6AddRefEv,"axG",@progbits,_ZThn24_N9NCompress5NPpmd8CDecoder6AddRefEv,comdat
	.weak	_ZThn24_N9NCompress5NPpmd8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress5NPpmd8CDecoder6AddRefEv,@function
_ZThn24_N9NCompress5NPpmd8CDecoder6AddRefEv: # @_ZThn24_N9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end33:
	.size	_ZThn24_N9NCompress5NPpmd8CDecoder6AddRefEv, .Lfunc_end33-_ZThn24_N9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress5NPpmd8CDecoder7ReleaseEv,"axG",@progbits,_ZThn24_N9NCompress5NPpmd8CDecoder7ReleaseEv,comdat
	.weak	_ZThn24_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress5NPpmd8CDecoder7ReleaseEv,@function
_ZThn24_N9NCompress5NPpmd8CDecoder7ReleaseEv: # @_ZThn24_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB34_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB34_2:                               # %_ZN9NCompress5NPpmd8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end34:
	.size	_ZThn24_N9NCompress5NPpmd8CDecoder7ReleaseEv, .Lfunc_end34-_ZThn24_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn32_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn32_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn32_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn32_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end35:
	.size	_ZThn32_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end35-_ZThn32_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress5NPpmd8CDecoder6AddRefEv,"axG",@progbits,_ZThn32_N9NCompress5NPpmd8CDecoder6AddRefEv,comdat
	.weak	_ZThn32_N9NCompress5NPpmd8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress5NPpmd8CDecoder6AddRefEv,@function
_ZThn32_N9NCompress5NPpmd8CDecoder6AddRefEv: # @_ZThn32_N9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end36:
	.size	_ZThn32_N9NCompress5NPpmd8CDecoder6AddRefEv, .Lfunc_end36-_ZThn32_N9NCompress5NPpmd8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress5NPpmd8CDecoder7ReleaseEv,"axG",@progbits,_ZThn32_N9NCompress5NPpmd8CDecoder7ReleaseEv,comdat
	.weak	_ZThn32_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress5NPpmd8CDecoder7ReleaseEv,@function
_ZThn32_N9NCompress5NPpmd8CDecoder7ReleaseEv: # @_ZThn32_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB37_2
# BB#1:
	addq	$-32, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB37_2:                               # %_ZN9NCompress5NPpmd8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end37:
	.size	_ZThn32_N9NCompress5NPpmd8CDecoder7ReleaseEv, .Lfunc_end37-_ZThn32_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmdL10SzBigAllocEPvm,@function
_ZN9NCompress5NPpmdL10SzBigAllocEPvm:   # @_ZN9NCompress5NPpmdL10SzBigAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigAlloc                # TAILCALL
.Lfunc_end38:
	.size	_ZN9NCompress5NPpmdL10SzBigAllocEPvm, .Lfunc_end38-_ZN9NCompress5NPpmdL10SzBigAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_,@function
_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_:   # @_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigFree                 # TAILCALL
.Lfunc_end39:
	.size	_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_, .Lfunc_end39-_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_
	.cfi_endproc

	.type	_ZTVN9NCompress5NPpmd8CDecoderE,@object # @_ZTVN9NCompress5NPpmd8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress5NPpmd8CDecoderE
	.p2align	3
_ZTVN9NCompress5NPpmd8CDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress5NPpmd8CDecoderE
	.quad	_ZN9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress5NPpmd8CDecoder6AddRefEv
	.quad	_ZN9NCompress5NPpmd8CDecoder7ReleaseEv
	.quad	_ZN9NCompress5NPpmd8CDecoderD2Ev
	.quad	_ZN9NCompress5NPpmd8CDecoderD0Ev
	.quad	_ZN9NCompress5NPpmd8CDecoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj
	.quad	_ZN9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy
	.quad	_ZN9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress5NPpmd8CDecoder15ReleaseInStreamEv
	.quad	_ZN9NCompress5NPpmd8CDecoder4ReadEPvjPj
	.quad	-8
	.quad	_ZTIN9NCompress5NPpmd8CDecoderE
	.quad	_ZThn8_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress5NPpmd8CDecoder6AddRefEv
	.quad	_ZThn8_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.quad	_ZThn8_N9NCompress5NPpmd8CDecoderD1Ev
	.quad	_ZThn8_N9NCompress5NPpmd8CDecoderD0Ev
	.quad	_ZThn8_N9NCompress5NPpmd8CDecoder21SetDecoderProperties2EPKhj
	.quad	-16
	.quad	_ZTIN9NCompress5NPpmd8CDecoderE
	.quad	_ZThn16_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress5NPpmd8CDecoder6AddRefEv
	.quad	_ZThn16_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.quad	_ZThn16_N9NCompress5NPpmd8CDecoderD1Ev
	.quad	_ZThn16_N9NCompress5NPpmd8CDecoderD0Ev
	.quad	_ZThn16_N9NCompress5NPpmd8CDecoder11SetInStreamEP19ISequentialInStream
	.quad	_ZThn16_N9NCompress5NPpmd8CDecoder15ReleaseInStreamEv
	.quad	-24
	.quad	_ZTIN9NCompress5NPpmd8CDecoderE
	.quad	_ZThn24_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N9NCompress5NPpmd8CDecoder6AddRefEv
	.quad	_ZThn24_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.quad	_ZThn24_N9NCompress5NPpmd8CDecoderD1Ev
	.quad	_ZThn24_N9NCompress5NPpmd8CDecoderD0Ev
	.quad	_ZThn24_N9NCompress5NPpmd8CDecoder16SetOutStreamSizeEPKy
	.quad	-32
	.quad	_ZTIN9NCompress5NPpmd8CDecoderE
	.quad	_ZThn32_N9NCompress5NPpmd8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn32_N9NCompress5NPpmd8CDecoder6AddRefEv
	.quad	_ZThn32_N9NCompress5NPpmd8CDecoder7ReleaseEv
	.quad	_ZThn32_N9NCompress5NPpmd8CDecoderD1Ev
	.quad	_ZThn32_N9NCompress5NPpmd8CDecoderD0Ev
	.quad	_ZThn32_N9NCompress5NPpmd8CDecoder4ReadEPvjPj
	.size	_ZTVN9NCompress5NPpmd8CDecoderE, 368

	.type	_ZN9NCompress5NPpmdL10g_BigAllocE,@object # @_ZN9NCompress5NPpmdL10g_BigAllocE
	.data
	.p2align	3
_ZN9NCompress5NPpmdL10g_BigAllocE:
	.quad	_ZN9NCompress5NPpmdL10SzBigAllocEPvm
	.quad	_ZN9NCompress5NPpmdL9SzBigFreeEPvS1_
	.size	_ZN9NCompress5NPpmdL10g_BigAllocE, 16

	.type	_ZTSN9NCompress5NPpmd8CDecoderE,@object # @_ZTSN9NCompress5NPpmd8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress5NPpmd8CDecoderE
	.p2align	4
_ZTSN9NCompress5NPpmd8CDecoderE:
	.asciz	"N9NCompress5NPpmd8CDecoderE"
	.size	_ZTSN9NCompress5NPpmd8CDecoderE, 28

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS30ICompressSetDecoderProperties2,@object # @_ZTS30ICompressSetDecoderProperties2
	.section	.rodata._ZTS30ICompressSetDecoderProperties2,"aG",@progbits,_ZTS30ICompressSetDecoderProperties2,comdat
	.weak	_ZTS30ICompressSetDecoderProperties2
	.p2align	4
_ZTS30ICompressSetDecoderProperties2:
	.asciz	"30ICompressSetDecoderProperties2"
	.size	_ZTS30ICompressSetDecoderProperties2, 33

	.type	_ZTI30ICompressSetDecoderProperties2,@object # @_ZTI30ICompressSetDecoderProperties2
	.section	.rodata._ZTI30ICompressSetDecoderProperties2,"aG",@progbits,_ZTI30ICompressSetDecoderProperties2,comdat
	.weak	_ZTI30ICompressSetDecoderProperties2
	.p2align	4
_ZTI30ICompressSetDecoderProperties2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30ICompressSetDecoderProperties2
	.quad	_ZTI8IUnknown
	.size	_ZTI30ICompressSetDecoderProperties2, 24

	.type	_ZTS20ICompressSetInStream,@object # @_ZTS20ICompressSetInStream
	.section	.rodata._ZTS20ICompressSetInStream,"aG",@progbits,_ZTS20ICompressSetInStream,comdat
	.weak	_ZTS20ICompressSetInStream
	.p2align	4
_ZTS20ICompressSetInStream:
	.asciz	"20ICompressSetInStream"
	.size	_ZTS20ICompressSetInStream, 23

	.type	_ZTI20ICompressSetInStream,@object # @_ZTI20ICompressSetInStream
	.section	.rodata._ZTI20ICompressSetInStream,"aG",@progbits,_ZTI20ICompressSetInStream,comdat
	.weak	_ZTI20ICompressSetInStream
	.p2align	4
_ZTI20ICompressSetInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ICompressSetInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ICompressSetInStream, 24

	.type	_ZTS25ICompressSetOutStreamSize,@object # @_ZTS25ICompressSetOutStreamSize
	.section	.rodata._ZTS25ICompressSetOutStreamSize,"aG",@progbits,_ZTS25ICompressSetOutStreamSize,comdat
	.weak	_ZTS25ICompressSetOutStreamSize
	.p2align	4
_ZTS25ICompressSetOutStreamSize:
	.asciz	"25ICompressSetOutStreamSize"
	.size	_ZTS25ICompressSetOutStreamSize, 28

	.type	_ZTI25ICompressSetOutStreamSize,@object # @_ZTI25ICompressSetOutStreamSize
	.section	.rodata._ZTI25ICompressSetOutStreamSize,"aG",@progbits,_ZTI25ICompressSetOutStreamSize,comdat
	.weak	_ZTI25ICompressSetOutStreamSize
	.p2align	4
_ZTI25ICompressSetOutStreamSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25ICompressSetOutStreamSize
	.quad	_ZTI8IUnknown
	.size	_ZTI25ICompressSetOutStreamSize, 24

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress5NPpmd8CDecoderE,@object # @_ZTIN9NCompress5NPpmd8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress5NPpmd8CDecoderE
	.p2align	4
_ZTIN9NCompress5NPpmd8CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress5NPpmd8CDecoderE
	.long	1                       # 0x1
	.long	6                       # 0x6
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI30ICompressSetDecoderProperties2
	.quad	2050                    # 0x802
	.quad	_ZTI20ICompressSetInStream
	.quad	4098                    # 0x1002
	.quad	_ZTI25ICompressSetOutStreamSize
	.quad	6146                    # 0x1802
	.quad	_ZTI19ISequentialInStream
	.quad	8194                    # 0x2002
	.quad	_ZTI13CMyUnknownImp
	.quad	10242                   # 0x2802
	.size	_ZTIN9NCompress5NPpmd8CDecoderE, 120


	.globl	_ZN9NCompress5NPpmd8CDecoderD1Ev
	.type	_ZN9NCompress5NPpmd8CDecoderD1Ev,@function
_ZN9NCompress5NPpmd8CDecoderD1Ev = _ZN9NCompress5NPpmd8CDecoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
