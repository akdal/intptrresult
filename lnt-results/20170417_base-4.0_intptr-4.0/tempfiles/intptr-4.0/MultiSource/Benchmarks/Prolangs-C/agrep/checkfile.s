	.text
	.file	"checkfile.bc"
	.globl	check_file
	.p2align	4, 0x90
	.type	check_file,@function
check_file:                             # @check_file
	.cfi_startproc
# BB#0:
	subq	$152, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 160
	movq	%rdi, %rax
	leaq	8(%rsp), %rdx
	movl	$1, %edi
	movq	%rax, %rsi
	callq	__xstat
	testl	%eax, %eax
	je	.LBB0_1
# BB#2:
	callq	__errno_location
	cmpl	$2, (%rax)
	setne	%al
	addb	%al, %al
	movzbl	%al, %eax
	orl	$-3, %eax
	addq	$152, %rsp
	retq
.LBB0_1:
	xorl	%eax, %eax
	addq	$152, %rsp
	retq
.Lfunc_end0:
	.size	check_file, .Lfunc_end0-check_file
	.cfi_endproc

	.type	ibuf,@object            # @ibuf
	.comm	ibuf,512,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
