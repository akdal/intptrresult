	.text
	.file	"subspic.bc"
	.globl	Substitute_Frame_Buffer
	.p2align	4, 0x90
	.type	Substitute_Frame_Buffer,@function
Substitute_Frame_Buffer:                # @Substitute_Frame_Buffer
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	%edi, %eax
	movl	Second_Field(%rip), %edx
	orl	%edx, %esi
	je	.LBB0_14
# BB#1:
	movl	picture_coding_type(%rip), %ecx
	testl	%edx, %edx
	je	.LBB0_3
# BB#2:
	cmpl	$3, picture_structure(%rip)
	je	.LBB0_3
# BB#8:
	cmpl	$2, %ecx
	jne	.LBB0_14
# BB#9:
	cmpl	$1, Substitute_Frame_Buffer.previous_picture_coding_type(%rip)
	je	.LBB0_11
# BB#10:
	movl	temporal_reference(%rip), %ecx
	leal	-1(%rax,%rcx), %eax
	subl	Substitute_Frame_Buffer.previous_anchor_temporal_reference(%rip), %eax
.LBB0_11:
	movq	Substitute_Picture_Filename(%rip), %rdi
	movl	$current_frame, %esi
	jmp	.LBB0_12
.LBB0_3:
	cmpl	$2, %ecx
	jne	.LBB0_5
# BB#4:
	decl	%eax
	movq	Substitute_Picture_Filename(%rip), %rdi
	movl	$forward_reference_frame, %esi
.LBB0_12:
	movl	%eax, %edx
	jmp	.LBB0_13
.LBB0_5:
	cmpl	$3, %ecx
	jne	.LBB0_14
# BB#6:
	cmpl	$1, Substitute_Frame_Buffer.bgate(%rip)
	je	.LBB0_14
# BB#7:
	movl	Substitute_Frame_Buffer.previous_temporal_reference(%rip), %ecx
	leal	-1(%rax,%rcx), %edx
	subl	temporal_reference(%rip), %edx
	movq	Substitute_Picture_Filename(%rip), %rdi
	movl	$backward_reference_frame, %esi
.LBB0_13:
	callq	Read_Frame
.LBB0_14:
	movl	picture_coding_type(%rip), %eax
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	sete	%cl
	movl	%ecx, Substitute_Frame_Buffer.bgate(%rip)
	movl	picture_structure(%rip), %ecx
	movl	Second_Field(%rip), %edx
	cmpl	$3, %ecx
	je	.LBB0_16
# BB#15:
	testl	%edx, %edx
	jne	.LBB0_17
.LBB0_16:
	movl	temporal_reference(%rip), %esi
	movl	%esi, Substitute_Frame_Buffer.previous_temporal_reference(%rip)
.LBB0_17:
	testl	%edx, %edx
	setne	%dl
	cmpl	$3, %eax
	setne	%sil
	cmpl	$3, %ecx
	sete	%cl
	orb	%dl, %cl
	cmpb	$1, %cl
	jne	.LBB0_20
# BB#18:
	testb	%sil, %sil
	je	.LBB0_20
# BB#19:
	movl	temporal_reference(%rip), %ecx
	movl	%ecx, Substitute_Frame_Buffer.previous_anchor_temporal_reference(%rip)
.LBB0_20:
	movl	%eax, Substitute_Frame_Buffer.previous_picture_coding_type(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	Substitute_Frame_Buffer, .Lfunc_end0-Substitute_Frame_Buffer
	.cfi_endproc

	.p2align	4, 0x90
	.type	Read_Frame,@function
Read_Frame:                             # @Read_Frame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$632, %rsp              # imm = 0x278
.Lcfi7:
	.cfi_def_cfa_offset 688
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	testl	%r15d, %r15d
	jns	.LBB1_2
# BB#1:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	printf
.LBB1_2:
	cmpl	$0, Big_Picture_Flag(%rip)
	je	.LBB1_8
# BB#3:
	movl	$.L.str.2, %esi
	movq	%rbp, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_15
# BB#4:
	movl	Coded_Picture_Height(%rip), %ebp
	imull	Coded_Picture_Width(%rip), %ebp
	movl	chroma_format(%rip), %esi
	cmpl	$1, %esi
	je	.LBB1_16
# BB#5:
	cmpl	$2, %esi
	je	.LBB1_17
# BB#6:
	cmpl	$3, %esi
	jne	.LBB1_18
# BB#7:
	leal	(%rbp,%rbp,2), %ebp
	jmp	.LBB1_19
.LBB1_8:                                # %.critedge
	leaq	368(%rsp), %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	%r15d, %edx
	callq	sprintf
	leaq	112(%rsp), %rbp
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	movq	substitute_frame(%rip), %r15
	movslq	Coded_Picture_Height(%rip), %rax
	movslq	Coded_Picture_Width(%rip), %rbx
	imulq	%rax, %rbx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %ebp
	movl	%ebp, %edi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	read
	movq	%rax, %rdx
	cmpl	%ebx, %edx
	je	.LBB1_10
# BB#9:
	leaq	112(%rsp), %rcx
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	movl	%ebx, %edx
	callq	printf
.LBB1_10:                               # %Read_Component.exit.i
	movl	%ebp, %edi
	callq	close
	leaq	112(%rsp), %rbp
	leaq	368(%rsp), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sprintf
	movq	substitute_frame+8(%rip), %r15
	movslq	Chroma_Height(%rip), %rax
	movslq	Chroma_Width(%rip), %rbx
	imulq	%rax, %rbx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %ebp
	movl	%ebp, %edi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	read
	movq	%rax, %rdx
	cmpl	%ebx, %edx
	je	.LBB1_12
# BB#11:
	leaq	112(%rsp), %rcx
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	movl	%ebx, %edx
	callq	printf
.LBB1_12:                               # %Read_Component.exit1.i
	movl	%ebp, %edi
	callq	close
	leaq	112(%rsp), %rbp
	leaq	368(%rsp), %rdx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sprintf
	movq	substitute_frame+16(%rip), %r15
	movslq	Chroma_Height(%rip), %rax
	movslq	Chroma_Width(%rip), %rbx
	imulq	%rax, %rbx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	open
	movl	%eax, %ebp
	movl	%ebp, %edi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	read
	movq	%rax, %rdx
	cmpl	%ebx, %edx
	je	.LBB1_14
# BB#13:
	leaq	112(%rsp), %rcx
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	movl	%ebx, %edx
	callq	printf
.LBB1_14:                               # %Read_Components.exit
	movl	%ebp, %edi
	callq	close
	jmp	.LBB1_28
.LBB1_15:
	movl	$Error_Text, %edi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	sprintf
	movl	$.Lstr, %edi
	callq	puts
	jmp	.LBB1_28
.LBB1_16:
	leal	(%rbp,%rbp,2), %ebp
	sarl	%ebp
	jmp	.LBB1_19
.LBB1_17:
	addl	%ebp, %ebp
	jmp	.LBB1_19
.LBB1_18:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_19:
	imull	%r15d, %ebp
	movslq	%ebp, %rsi
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	cmpl	$0, Coded_Picture_Height(%rip)
	jle	.LBB1_21
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph7.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	Coded_Picture_Width(%rip), %rdx
	movslq	%ebp, %rbp
	movq	%rdx, %rdi
	imulq	%rbp, %rdi
	addq	substitute_frame(%rip), %rdi
	movl	$1, %esi
	movq	%rbx, %rcx
	callq	fread
	incl	%ebp
	cmpl	Coded_Picture_Height(%rip), %ebp
	jl	.LBB1_20
.LBB1_21:                               # %.preheader1.i
	cmpl	$0, Chroma_Height(%rip)
	jle	.LBB1_27
# BB#22:                                # %.lr.ph4.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph4.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	Chroma_Width(%rip), %rdx
	movslq	%ebp, %rbp
	movq	%rdx, %rdi
	imulq	%rbp, %rdi
	addq	substitute_frame+8(%rip), %rdi
	movl	$1, %esi
	movq	%rbx, %rcx
	callq	fread
	incl	%ebp
	movl	Chroma_Height(%rip), %eax
	cmpl	%eax, %ebp
	jl	.LBB1_23
# BB#24:                                # %.preheader.i
	testl	%eax, %eax
	jle	.LBB1_27
# BB#25:                                # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_26:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	Chroma_Width(%rip), %rdx
	movslq	%ebp, %rbp
	movq	%rdx, %rdi
	imulq	%rbp, %rdi
	addq	substitute_frame+16(%rip), %rdi
	movl	$1, %esi
	movq	%rbx, %rcx
	callq	fread
	incl	%ebp
	cmpl	Chroma_Height(%rip), %ebp
	jl	.LBB1_26
.LBB1_27:                               # %Extract_Components.exit
	movq	%rbx, %rdi
	callq	fclose
.LBB1_28:
	movb	$1, %al
	movl	$0, 36(%rsp)            # 4-byte Folded Spill
	cmpl	$0, Second_Field(%rip)
	je	.LBB1_31
# BB#29:
	cmpl	$2, picture_coding_type(%rip)
	jne	.LBB1_31
# BB#30:
	movl	picture_structure(%rip), %eax
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	sete	%cl
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	cmpl	$3, %eax
	sete	%al
.LBB1_31:
	movslq	Coded_Picture_Width(%rip), %rdx
	movb	%al, 3(%rsp)            # 1-byte Spill
	cmpb	$1, %al
	movl	$1, %eax
	adcl	$0, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%rdx, %rcx
	testq	%rdx, %rdx
	jle	.LBB1_51
# BB#32:
	movl	Coded_Picture_Height(%rip), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB1_51
# BB#33:                                # %.preheader.us.preheader.i
	movq	substitute_frame(%rip), %r9
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbp
	movq	%rcx, %rdx
	movl	%edx, %edi
	movl	%edx, %eax
	imull	4(%rsp), %eax           # 4-byte Folded Reload
	xorl	%ecx, %ecx
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movslq	%eax, %rsi
	cmovneq	%rcx, %rdx
	cmpb	$0, 3(%rsp)             # 1-byte Folded Reload
	cmovneq	%rcx, %rdx
	leaq	(%rdx,%rdi), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	-1(%rdi), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%edi, %eax
	andl	$31, %eax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	subq	%rax, %rdi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	leaq	16(%r9,%rdx), %r10
	leaq	16(%rbp), %r8
	leaq	(%r9,%rdx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	3(%rbp), %r12
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	leaq	3(%r9,%rdx), %r11
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_34:                               # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_42 Depth 2
                                        #     Child Loop BB1_46 Depth 2
                                        #     Child Loop BB1_49 Depth 2
	cmpl	$32, 8(%rsp)            # 4-byte Folded Reload
	jae	.LBB1_36
# BB#35:                                #   in Loop: Header=BB1_34 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB1_44
	.p2align	4, 0x90
.LBB1_36:                               # %min.iters.checked
                                        #   in Loop: Header=BB1_34 Depth=1
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB1_40
# BB#37:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_34 Depth=1
	movq	%rsi, %rdx
	imulq	%rcx, %rdx
	leaq	(%rbp,%rdx), %rdi
	movq	80(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdx), %rbp
	addq	%r9, %rbp
	cmpq	%rbp, %rdi
	movq	24(%rsp), %rbp          # 8-byte Reload
	jae	.LBB1_41
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_34 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rdx), %rdi
	addq	%rbp, %rdi
	addq	72(%rsp), %rdx          # 8-byte Folded Reload
	addq	%r9, %rdx
	cmpq	%rdi, %rdx
	jae	.LBB1_41
.LBB1_40:                               #   in Loop: Header=BB1_34 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB1_44
.LBB1_41:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_34 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%r8, %rdi
	movq	%r10, %rbp
	.p2align	4, 0x90
.LBB1_42:                               # %vector.body
                                        #   Parent Loop BB1_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rbp
	addq	$32, %rdi
	addq	$-32, %rdx
	jne	.LBB1_42
# BB#43:                                # %middle.block
                                        #   in Loop: Header=BB1_34 Depth=1
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB1_50
	.p2align	4, 0x90
.LBB1_44:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB1_34 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%r13d, %edx
	movq	56(%rsp), %r14          # 8-byte Reload
	subq	%r13, %r14
	andq	$3, %rdx
	je	.LBB1_47
# BB#45:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB1_34 Depth=1
	leaq	(%rbp,%rax), %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rax), %rbp
	negq	%rdx
	.p2align	4, 0x90
.LBB1_46:                               # %scalar.ph.prol
                                        #   Parent Loop BB1_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp,%r13), %ebx
	movb	%bl, (%rdi,%r13)
	incq	%r13
	incq	%rdx
	jne	.LBB1_46
.LBB1_47:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB1_34 Depth=1
	cmpq	$3, %r14
	movq	24(%rsp), %rbp          # 8-byte Reload
	jb	.LBB1_50
# BB#48:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB1_34 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	subq	%r13, %rdx
	leaq	(%r12,%r13), %rdi
	addq	%r11, %r13
	.p2align	4, 0x90
.LBB1_49:                               # %scalar.ph
                                        #   Parent Loop BB1_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-3(%r13), %ebx
	movb	%bl, -3(%rdi)
	movzbl	-2(%r13), %ebx
	movb	%bl, -2(%rdi)
	movzbl	-1(%r13), %ebx
	movb	%bl, -1(%rdi)
	movzbl	(%r13), %ebx
	movb	%bl, (%rdi)
	addq	$4, %rdi
	addq	$4, %r13
	addq	$-4, %rdx
	jne	.LBB1_49
.LBB1_50:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB1_34 Depth=1
	addq	%rsi, %rax
	addl	4(%rsp), %r15d          # 4-byte Folded Reload
	incq	%rcx
	addq	%rsi, %r10
	addq	%rsi, %r8
	addq	%rsi, %r12
	addq	%rsi, %r11
	cmpl	48(%rsp), %r15d         # 4-byte Folded Reload
	jl	.LBB1_34
.LBB1_51:                               # %Copy_Frame.exit
	movslq	Chroma_Width(%rip), %rdx
	testq	%rdx, %rdx
	movl	Chroma_Height(%rip), %r11d
	jle	.LBB1_72
# BB#52:                                # %Copy_Frame.exit
	testl	%r11d, %r11d
	jle	.LBB1_72
# BB#53:                                # %.preheader.us.preheader.i25
	movl	%r11d, 48(%rsp)         # 4-byte Spill
	movq	substitute_frame+8(%rip), %rsi
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r10
	movl	%edx, %edi
	movl	%edx, %eax
	imull	4(%rsp), %eax           # 4-byte Folded Reload
	xorl	%ecx, %ecx
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movslq	%eax, %r13
	movq	%rdx, %rbx
	cmovneq	%rcx, %rbx
	cmpb	$0, 3(%rsp)             # 1-byte Folded Reload
	cmovneq	%rcx, %rbx
	leaq	(%rbx,%rdi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	-1(%rdi), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%edx, %eax
	andl	$31, %eax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rax, 104(%rsp)         # 8-byte Spill
	subq	%rax, %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	leaq	16(%rsi,%rbx), %rbp
	leaq	16(%r10), %rax
	leaq	(%rsi,%rbx), %rdi
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	leaq	3(%r10), %r15
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	leaq	3(%rsi,%rbx), %r11
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%r10, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_54:                               # %.preheader.us.i29
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_62 Depth 2
                                        #     Child Loop BB1_66 Depth 2
                                        #     Child Loop BB1_69 Depth 2
	cmpl	$32, %edx
	jae	.LBB1_56
# BB#55:                                #   in Loop: Header=BB1_54 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB1_64
	.p2align	4, 0x90
.LBB1_56:                               # %min.iters.checked63
                                        #   in Loop: Header=BB1_54 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB1_60
# BB#57:                                # %vector.memcheck78
                                        #   in Loop: Header=BB1_54 Depth=1
	movq	%r13, %rsi
	imulq	%rcx, %rsi
	leaq	(%r10,%rsi), %rdi
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rsi), %rbx
	movq	72(%rsp), %rdx          # 8-byte Reload
	addq	%rdx, %rbx
	cmpq	%rbx, %rdi
	jae	.LBB1_61
# BB#58:                                # %vector.memcheck78
                                        #   in Loop: Header=BB1_54 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rsi), %rdi
	addq	%r10, %rdi
	addq	96(%rsp), %rsi          # 8-byte Folded Reload
	addq	%rdx, %rsi
	cmpq	%rdi, %rsi
	jae	.LBB1_61
.LBB1_60:                               #   in Loop: Header=BB1_54 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB1_64
.LBB1_61:                               # %vector.body59.preheader
                                        #   in Loop: Header=BB1_54 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %rbx
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB1_62:                               # %vector.body59
                                        #   Parent Loop BB1_54 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	addq	$32, %rdi
	addq	$32, %rbx
	addq	$-32, %rsi
	jne	.LBB1_62
# BB#63:                                # %middle.block60
                                        #   in Loop: Header=BB1_54 Depth=1
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB1_70
	.p2align	4, 0x90
.LBB1_64:                               # %scalar.ph61.preheader
                                        #   in Loop: Header=BB1_54 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	subl	%r14d, %esi
	movq	40(%rsp), %r12          # 8-byte Reload
	subq	%r14, %r12
	andq	$3, %rsi
	je	.LBB1_67
# BB#65:                                # %scalar.ph61.prol.preheader
                                        #   in Loop: Header=BB1_54 Depth=1
	leaq	(%r10,%r9), %rbx
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r9), %rdi
	negq	%rsi
	.p2align	4, 0x90
.LBB1_66:                               # %scalar.ph61.prol
                                        #   Parent Loop BB1_54 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%r14), %r10d
	movb	%r10b, (%rbx,%r14)
	incq	%r14
	incq	%rsi
	jne	.LBB1_66
.LBB1_67:                               # %scalar.ph61.prol.loopexit
                                        #   in Loop: Header=BB1_54 Depth=1
	cmpq	$3, %r12
	movq	56(%rsp), %r10          # 8-byte Reload
	jb	.LBB1_70
# BB#68:                                # %scalar.ph61.preheader.new
                                        #   in Loop: Header=BB1_54 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	subq	%r14, %rbx
	leaq	(%r15,%r14), %rsi
	addq	%r11, %r14
	.p2align	4, 0x90
.LBB1_69:                               # %scalar.ph61
                                        #   Parent Loop BB1_54 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-3(%r14), %edx
	movb	%dl, -3(%rsi)
	movzbl	-2(%r14), %edx
	movb	%dl, -2(%rsi)
	movzbl	-1(%r14), %edx
	movb	%dl, -1(%rsi)
	movzbl	(%r14), %edx
	movb	%dl, (%rsi)
	addq	$4, %rsi
	addq	$4, %r14
	addq	$-4, %rbx
	jne	.LBB1_69
.LBB1_70:                               # %._crit_edge.us.i35
                                        #   in Loop: Header=BB1_54 Depth=1
	addq	%r13, %r9
	addl	4(%rsp), %r8d           # 4-byte Folded Reload
	incq	%rcx
	addq	%r13, %rbp
	addq	%r13, %rax
	addq	%r13, %r15
	addq	%r13, %r11
	cmpl	48(%rsp), %r8d          # 4-byte Folded Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	jl	.LBB1_54
# BB#71:                                # %Copy_Frame.exit36.loopexit
	movl	Chroma_Width(%rip), %edx
	movl	Chroma_Height(%rip), %r11d
.LBB1_72:                               # %Copy_Frame.exit36
	testl	%edx, %edx
	jle	.LBB1_92
# BB#73:                                # %Copy_Frame.exit36
	testl	%r11d, %r11d
	jle	.LBB1_92
# BB#74:                                # %.preheader.us.preheader.i40
	movq	substitute_frame+16(%rip), %rcx
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
	movl	%edx, %eax
	imull	4(%rsp), %eax           # 4-byte Folded Reload
	xorl	%ebx, %ebx
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	cltq
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movslq	%edx, %rax
	cmovneq	%rbx, %rax
	cmpb	$0, 3(%rsp)             # 1-byte Folded Reload
	cmovneq	%rbx, %rax
	movl	%edx, %esi
	leaq	(%rax,%rsi), %rbp
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	leaq	-1(%rsi), %rbp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	%edx, %ebp
	andl	$31, %ebp
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	subq	%rbp, %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	16(%rcx,%rax), %r13
	leaq	16(%rdi), %r10
	leaq	(%rcx,%rax), %rsi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	leaq	3(%rdi), %r9
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	3(%rcx,%rax), %r8
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_75:                               # %.preheader.us.i44
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_83 Depth 2
                                        #     Child Loop BB1_87 Depth 2
                                        #     Child Loop BB1_90 Depth 2
	cmpl	$32, %edx
	jae	.LBB1_77
# BB#76:                                #   in Loop: Header=BB1_75 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB1_85
	.p2align	4, 0x90
.LBB1_77:                               # %min.iters.checked94
                                        #   in Loop: Header=BB1_75 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB1_81
# BB#78:                                # %vector.memcheck109
                                        #   in Loop: Header=BB1_75 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	imulq	%rbx, %rax
	leaq	(%rdi,%rax), %rsi
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax), %rdi
	addq	%rcx, %rdi
	cmpq	%rdi, %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	jae	.LBB1_82
# BB#79:                                # %vector.memcheck109
                                        #   in Loop: Header=BB1_75 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rax), %rsi
	addq	%rdi, %rsi
	addq	64(%rsp), %rax          # 8-byte Folded Reload
	addq	%rcx, %rax
	cmpq	%rsi, %rax
	jae	.LBB1_82
.LBB1_81:                               #   in Loop: Header=BB1_75 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB1_85
.LBB1_82:                               # %vector.body90.preheader
                                        #   in Loop: Header=BB1_75 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r10, %rax
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB1_83:                               # %vector.body90
                                        #   Parent Loop BB1_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	$32, %rdi
	addq	$32, %rax
	addq	$-32, %rsi
	jne	.LBB1_83
# BB#84:                                # %middle.block91
                                        #   in Loop: Header=BB1_75 Depth=1
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	je	.LBB1_91
	.p2align	4, 0x90
.LBB1_85:                               # %scalar.ph92.preheader
                                        #   in Loop: Header=BB1_75 Depth=1
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %rbx
	movl	%r11d, %ecx
	movq	%rdx, %rbp
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	subl	%r15d, %esi
	movq	40(%rsp), %r11          # 8-byte Reload
	subq	%r15, %r11
	andq	$3, %rsi
	je	.LBB1_88
# BB#86:                                # %scalar.ph92.prol.preheader
                                        #   in Loop: Header=BB1_75 Depth=1
	leaq	(%rdi,%r12), %rax
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r12), %rdi
	negq	%rsi
	.p2align	4, 0x90
.LBB1_87:                               # %scalar.ph92.prol
                                        #   Parent Loop BB1_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%r15), %edx
	movb	%dl, (%rax,%r15)
	incq	%r15
	incq	%rsi
	jne	.LBB1_87
.LBB1_88:                               # %scalar.ph92.prol.loopexit
                                        #   in Loop: Header=BB1_75 Depth=1
	cmpq	$3, %r11
	movq	%rbp, %rdx
	movl	%ecx, %r11d
	movq	%rbx, %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	jb	.LBB1_91
# BB#89:                                # %scalar.ph92.preheader.new
                                        #   in Loop: Header=BB1_75 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
	subq	%r15, %rbp
	leaq	(%r9,%r15), %rsi
	addq	%r8, %r15
	.p2align	4, 0x90
.LBB1_90:                               # %scalar.ph92
                                        #   Parent Loop BB1_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-3(%r15), %eax
	movb	%al, -3(%rsi)
	movzbl	-2(%r15), %eax
	movb	%al, -2(%rsi)
	movzbl	-1(%r15), %eax
	movb	%al, -1(%rsi)
	movzbl	(%r15), %eax
	movb	%al, (%rsi)
	addq	$4, %rsi
	addq	$4, %r15
	addq	$-4, %rbp
	jne	.LBB1_90
.LBB1_91:                               # %._crit_edge.us.i50
                                        #   in Loop: Header=BB1_75 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	addq	%rax, %r12
	addl	4(%rsp), %r14d          # 4-byte Folded Reload
	incq	%rbx
	addq	%rax, %r13
	addq	%rax, %r10
	addq	%rax, %r9
	addq	%rax, %r8
	cmpl	%r11d, %r14d
	jl	.LBB1_75
.LBB1_92:                               # %Copy_Frame.exit51
	addq	$632, %rsp              # imm = 0x278
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	Read_Frame, .Lfunc_end1-Read_Frame
	.cfi_endproc

	.type	Substitute_Frame_Buffer.previous_temporal_reference,@object # @Substitute_Frame_Buffer.previous_temporal_reference
	.local	Substitute_Frame_Buffer.previous_temporal_reference
	.comm	Substitute_Frame_Buffer.previous_temporal_reference,4,4
	.type	Substitute_Frame_Buffer.previous_anchor_temporal_reference,@object # @Substitute_Frame_Buffer.previous_anchor_temporal_reference
	.local	Substitute_Frame_Buffer.previous_anchor_temporal_reference
	.comm	Substitute_Frame_Buffer.previous_anchor_temporal_reference,4,4
	.type	Substitute_Frame_Buffer.previous_picture_coding_type,@object # @Substitute_Frame_Buffer.previous_picture_coding_type
	.local	Substitute_Frame_Buffer.previous_picture_coding_type
	.comm	Substitute_Frame_Buffer.previous_picture_coding_type,4,4
	.type	Substitute_Frame_Buffer.bgate,@object # @Substitute_Frame_Buffer.bgate
	.local	Substitute_Frame_Buffer.bgate
	.comm	Substitute_Frame_Buffer.bgate,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ERROR: framenum (%d) is less than zero\n"
	.size	.L.str, 40

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rb"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Couldn't open %s\n"
	.size	.L.str.3, 18

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"ERROR: chroma_format (%d) not recognized\n"
	.size	.L.str.4, 42

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s.Y"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%s.U"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s.V"
	.size	.L.str.7, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"was able to read only %d bytes of %d of file %s\n"
	.size	.L.str.9, 49

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"was unable to substitute frame"
	.size	.Lstr, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
