	.text
	.file	"btDefaultCollisionConfiguration.bc"
	.globl	_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo,@function
_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo: # @_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	$_ZTV31btDefaultCollisionConfiguration+16, (%r12)
	movl	$356, %edi              # imm = 0x164
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	xorl	%esi, %esi
	movl	$356, %edx              # imm = 0x164
	movq	%rbx, %rdi
	callq	memset
	movq	%rbx, 64(%r12)
	cmpl	$0, 40(%r14)
	movl	$_ZTV33btMinkowskiPenetrationDepthSolver+16, %eax
	movl	$_ZTV30btGjkEpaPenetrationDepthSolver+16, %ebx
	cmoveq	%rax, %rbx
	movl	$8, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rbx, (%rax)
	movq	%rax, 72(%r12)
	movl	$40, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movq	64(%r12), %rsi
	movq	72(%r12), %rdx
	movq	%rbx, %rdi
	callq	_ZN23btConvexConvexAlgorithm10CreateFuncC1EP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
	movq	%rbx, 80(%r12)
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movb	$0, 8(%rax)
	movq	$_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 88(%r12)
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movb	$0, 8(%rax)
	movq	$_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE+16, (%rax)
	movq	%rax, 96(%r12)
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movb	$0, 8(%rax)
	movq	$_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 104(%r12)
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movb	$0, 8(%rax)
	movq	$_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE+16, (%rax)
	movq	%rax, 112(%r12)
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movb	$0, 8(%rax)
	movq	$_ZTVN16btEmptyAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 120(%r12)
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movb	$0, 8(%rax)
	movq	$_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 128(%r12)
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movb	$0, 8(%rax)
	movq	$_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 144(%r12)
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	$_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 152(%r12)
	movb	$1, 8(%rax)
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movb	$0, 8(%rax)
	movq	$_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 136(%r12)
	movl	$24, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movb	$0, 8(%rax)
	movq	$_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE+16, (%rax)
	movl	$1, 12(%rax)
	movl	$1, 16(%rax)
	movq	%rax, 168(%r12)
	movl	$24, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	$_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE+16, (%rax)
	movl	$1, 12(%rax)
	movl	$1, 16(%rax)
	movq	%rax, 160(%r12)
	movb	$1, 8(%rax)
	movl	32(%r14), %eax
	cmpl	$159, %eax
	movl	$160, %ebp
	cmovgl	%eax, %ebp
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	movb	$0, 24(%r12)
	movq	%rax, 16(%r12)
	jmp	.LBB0_3
.LBB0_2:
	movb	$1, 24(%r12)
	movl	$32, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	36(%r14), %r15d
	xorps	%xmm0, %xmm0
	movups	%xmm0, 9(%rbx)
	movups	%xmm0, (%rbx)
	movl	$16, %esi
	movq	%r15, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, (%rbx)
	movl	%r15d, 8(%rbx)
	movq	%rbx, 16(%r12)
.LBB0_3:
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_5
# BB#4:
	movb	$0, 40(%r12)
	movq	%rax, 32(%r12)
	jmp	.LBB0_15
.LBB0_5:                                # %.noexc55
	movb	$1, 40(%r12)
	movl	$32, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	24(%r14), %eax
	movl	$744, (%rbx)            # imm = 0x2E8
	movl	%eax, 4(%rbx)
	imull	$744, %eax, %edi        # imm = 0x2E8
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 24(%rbx)
	movq	%rax, 16(%rbx)
	movl	4(%rbx), %edi
	movl	%edi, 8(%rbx)
	movl	%edi, %r13d
	decl	%r13d
	je	.LBB0_14
# BB#6:                                 # %.lr.ph.i50
	movslq	(%rbx), %rcx
	addl	$-2, %edi
	movl	%r13d, %edx
	andl	$7, %edx
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	je	.LBB0_7
# BB#8:                                 # %.prol.preheader93
	negl	%edx
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	leaq	(%rbp,%rcx), %rsi
	movq	%rsi, (%rbp)
	decl	%r13d
	incl	%edx
	movq	%rsi, %rbp
	jne	.LBB0_9
	jmp	.LBB0_10
.LBB0_7:
	movq	%rax, %rsi
.LBB0_10:                               # %.prol.loopexit94
	leaq	1(%rdi), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	cmpl	$7, %edi
	jb	.LBB0_13
# BB#11:                                # %.lr.ph.i50.new
	leaq	(%rcx,%rcx), %r10
	leaq	(%rcx,%rcx,2), %r15
	leaq	(,%rcx,4), %r9
	leaq	(%rcx,%rcx,4), %rbp
	leaq	(%r10,%r10,2), %rbx
	leaq	(,%rcx,8), %r8
	movq	%r8, %r11
	subq	%rcx, %r11
	.p2align	4, 0x90
.LBB0_12:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rsi,%rcx), %rdx
	movq	%rdx, (%rsi)
	addq	%rcx, %rdx
	leaq	(%rsi,%r10), %r14
	movq	%r14, (%rsi,%rcx)
	addq	%rcx, %rdx
	leaq	(%rsi,%r15), %rdi
	movq	%rdi, (%rsi,%rcx,2)
	addq	%rcx, %rdx
	leaq	(%rsi,%r9), %rdi
	movq	%rdi, (%rsi,%r15)
	addq	%rcx, %rdx
	leaq	(%rsi,%rbp), %rdi
	movq	%rdi, (%rsi,%rcx,4)
	addq	%rcx, %rdx
	leaq	(%rsi,%rbx), %rdi
	movq	%rdi, (%rsi,%rbp)
	addq	%rcx, %rdx
	leaq	(%rsi,%r11), %rdi
	movq	%rdi, (%rsi,%rbx)
	addq	%rcx, %rdx
	leaq	(%rsi,%r8), %rdi
	movq	%rdi, (%rsi,%r11)
	addl	$-8, %r13d
	movq	%rdx, %rsi
	jne	.LBB0_12
.LBB0_13:                               # %._crit_edge.loopexit.i53
	imulq	24(%rsp), %rcx          # 8-byte Folded Reload
	addq	%rcx, %rax
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	8(%rsp), %ebp           # 4-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB0_14:
	movq	$0, (%rax)
	movq	%rbx, 32(%r12)
.LBB0_15:
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB0_17
# BB#16:
	movb	$0, 56(%r12)
	movq	%rax, 48(%r12)
	jmp	.LBB0_27
.LBB0_17:                               # %.noexc
	movb	$1, 56(%r12)
	movl	$32, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	28(%r14), %edi
	movl	%ebp, (%rbx)
	movl	%edi, 4(%rbx)
	imull	%ebp, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 24(%rbx)
	movq	%rax, 16(%rbx)
	movl	4(%rbx), %esi
	movl	%esi, 8(%rbx)
	movl	%esi, %edi
	decl	%edi
	je	.LBB0_26
# BB#18:                                # %.lr.ph.i
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movslq	(%rbx), %rcx
	addl	$-2, %esi
	movl	%edi, %edx
	andl	$7, %edx
	je	.LBB0_19
# BB#20:                                # %.prol.preheader
	negl	%edx
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_21:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rbx,%rcx), %rbp
	movq	%rbp, (%rbx)
	decl	%edi
	incl	%edx
	movq	%rbp, %rbx
	jne	.LBB0_21
	jmp	.LBB0_22
.LBB0_19:
	movq	%rax, %rbp
.LBB0_22:                               # %.prol.loopexit
	leaq	1(%rsi), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpl	$7, %esi
	jb	.LBB0_25
# BB#23:                                # %.lr.ph.i.new
	leaq	(%rcx,%rcx), %r10
	leaq	(%rcx,%rcx,2), %r14
	leaq	(,%rcx,4), %r9
	leaq	(%rcx,%rcx,4), %r13
	leaq	(%r10,%r10,2), %r15
	leaq	(,%rcx,8), %r11
	movq	%r11, %rbx
	subq	%rcx, %rbx
	.p2align	4, 0x90
.LBB0_24:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rbp,%rcx), %rsi
	movq	%rsi, (%rbp)
	addq	%rcx, %rsi
	leaq	(%rbp,%r10), %r8
	movq	%r8, (%rbp,%rcx)
	addq	%rcx, %rsi
	leaq	(%rbp,%r14), %rdx
	movq	%rdx, (%rbp,%rcx,2)
	addq	%rcx, %rsi
	leaq	(%rbp,%r9), %rdx
	movq	%rdx, (%rbp,%r14)
	addq	%rcx, %rsi
	leaq	(%rbp,%r13), %rdx
	movq	%rdx, (%rbp,%rcx,4)
	addq	%rcx, %rsi
	leaq	(%rbp,%r15), %rdx
	movq	%rdx, (%rbp,%r13)
	addq	%rcx, %rsi
	leaq	(%rbp,%rbx), %rdx
	movq	%rdx, (%rbp,%r15)
	addq	%rcx, %rsi
	leaq	(%rbp,%r11), %rdx
	movq	%rdx, (%rbp,%rbx)
	addl	$-8, %edi
	movq	%rsi, %rbp
	jne	.LBB0_24
.LBB0_25:                               # %._crit_edge.loopexit.i
	imulq	8(%rsp), %rcx           # 8-byte Folded Reload
	addq	%rcx, %rax
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_26:
	movq	$0, (%rax)
	movq	%rbx, 48(%r12)
.LBB0_27:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo, .Lfunc_end0-_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
	.cfi_endproc

	.globl	_ZN31btDefaultCollisionConfigurationD2Ev
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfigurationD2Ev,@function
_ZN31btDefaultCollisionConfigurationD2Ev: # @_ZN31btDefaultCollisionConfigurationD2Ev
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	$_ZTV31btDefaultCollisionConfiguration+16, (%rbx)
	cmpb	$0, 24(%rbx)
	je	.LBB1_12
# BB#1:
	movq	16(%rbx), %r14
	cmpl	$0, 12(%r14)
	jne	.LBB1_11
# BB#2:
	cmpb	$0, 24(%r14)
	movq	%r14, %rax
	movq	%r14, %r15
	jne	.LBB1_6
# BB#3:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#5:
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%rbx), %rax
	movq	%rax, %r15
	jmp	.LBB1_6
.LBB1_4:
	movq	%r14, %rax
	movq	%r14, %r15
.LBB1_6:                                # %_ZN12btStackAlloc7destroyEv.exit
	movq	$0, (%r14)
	movl	$0, 12(%r14)
	cmpl	$0, 12(%r15)
	movq	%r15, %r14
	jne	.LBB1_11
# BB#7:
	cmpb	$0, 24(%r15)
	jne	.LBB1_10
# BB#8:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB1_10
# BB#9:
	callq	_Z21btAlignedFreeInternalPv
	movq	16(%rbx), %rax
.LBB1_10:                               # %._crit_edge.i.i
	movq	$0, (%r15)
	movl	$0, 12(%r15)
	movq	%rax, %r14
.LBB1_11:                               # %_ZN12btStackAllocD2Ev.exit
	movq	%r14, %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB1_12:
	cmpb	$0, 56(%rbx)
	je	.LBB1_14
# BB#13:                                # %_ZN15btPoolAllocatorD2Ev.exit
	movq	48(%rbx), %rax
	movq	24(%rax), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	48(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB1_14:
	cmpb	$0, 40(%rbx)
	je	.LBB1_16
# BB#15:                                # %_ZN15btPoolAllocatorD2Ev.exit5
	movq	32(%rbx), %rax
	movq	24(%rax), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	32(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB1_16:
	movq	80(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	80(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	88(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	88(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	96(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	96(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	104(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	104(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	112(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	112(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	120(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	120(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	128(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	128(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	144(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	144(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	152(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	152(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	136(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	136(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	168(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	168(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	160(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	160(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	64(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	72(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	72(%rbx), %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.Lfunc_end1:
	.size	_ZN31btDefaultCollisionConfigurationD2Ev, .Lfunc_end1-_ZN31btDefaultCollisionConfigurationD2Ev
	.cfi_endproc

	.globl	_ZN31btDefaultCollisionConfigurationD0Ev
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfigurationD0Ev,@function
_ZN31btDefaultCollisionConfigurationD0Ev: # @_ZN31btDefaultCollisionConfigurationD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN31btDefaultCollisionConfigurationD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN31btDefaultCollisionConfigurationD0Ev, .Lfunc_end2-_ZN31btDefaultCollisionConfigurationD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii,@function
_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii: # @_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$8, %esi
	jne	.LBB3_3
# BB#1:
	cmpl	$8, %edx
	jne	.LBB3_3
# BB#2:
	subq	$-128, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_3:
	cmpl	$8, %esi
	jne	.LBB3_6
# BB#4:
	cmpl	$1, %edx
	jne	.LBB3_6
# BB#5:
	addq	$144, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_6:
	cmpl	$1, %esi
	jne	.LBB3_9
# BB#7:
	cmpl	$8, %edx
	jne	.LBB3_9
# BB#8:
	addq	$152, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_9:
	movl	%edx, %eax
	orl	%esi, %eax
	je	.LBB3_10
# BB#11:
	cmpl	$19, %esi
	jg	.LBB3_14
# BB#12:
	cmpl	$28, %edx
	jne	.LBB3_14
# BB#13:
	addq	$168, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_10:
	addq	$136, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_14:
	cmpl	$28, %esi
	jne	.LBB3_17
# BB#15:
	cmpl	$19, %edx
	jg	.LBB3_17
# BB#16:
	addq	$160, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_17:
	cmpl	$19, %esi
	jg	.LBB3_22
# BB#18:
	cmpl	$19, %edx
	jg	.LBB3_20
# BB#19:
	addq	$80, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_22:
	cmpl	$19, %edx
	jg	.LBB3_25
# BB#23:
	leal	-21(%rsi), %eax
	cmpl	$8, %eax
	ja	.LBB3_25
# BB#24:
	addq	$96, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_25:
	cmpl	$31, %esi
	jne	.LBB3_27
# BB#26:
	addq	$104, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_20:
	leal	-21(%rdx), %eax
	cmpl	$8, %eax
	ja	.LBB3_27
# BB#21:
	addq	$88, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_27:                               # %.thread33
	cmpl	$31, %edx
	jne	.LBB3_29
# BB#28:
	addq	$112, %rdi
	movq	(%rdi), %rax
	retq
.LBB3_29:
	addq	$120, %rdi
	movq	(%rdi), %rax
	retq
.Lfunc_end3:
	.size	_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii, .Lfunc_end3-_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii
	.cfi_endproc

	.globl	_ZN31btDefaultCollisionConfiguration35setConvexConvexMultipointIterationsEii
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfiguration35setConvexConvexMultipointIterationsEii,@function
_ZN31btDefaultCollisionConfiguration35setConvexConvexMultipointIterationsEii: # @_ZN31btDefaultCollisionConfiguration35setConvexConvexMultipointIterationsEii
	.cfi_startproc
# BB#0:
	movq	80(%rdi), %rax
	movl	%esi, 32(%rax)
	movl	%edx, 36(%rax)
	retq
.Lfunc_end4:
	.size	_ZN31btDefaultCollisionConfiguration35setConvexConvexMultipointIterationsEii, .Lfunc_end4-_ZN31btDefaultCollisionConfiguration35setConvexConvexMultipointIterationsEii
	.cfi_endproc

	.section	.text._ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv,"axG",@progbits,_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv,comdat
	.weak	_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv,@function
_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv: # @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	retq
.Lfunc_end5:
	.size	_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv, .Lfunc_end5-_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv
	.cfi_endproc

	.section	.text._ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv,"axG",@progbits,_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv,comdat
	.weak	_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv,@function
_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv: # @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv
	.cfi_startproc
# BB#0:
	movq	48(%rdi), %rax
	retq
.Lfunc_end6:
	.size	_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv, .Lfunc_end6-_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv
	.cfi_endproc

	.section	.text._ZN31btDefaultCollisionConfiguration17getStackAllocatorEv,"axG",@progbits,_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv,comdat
	.weak	_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv,@function
_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv: # @_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end7:
	.size	_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv, .Lfunc_end7-_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv
	.cfi_endproc

	.section	.text._ZN30btCollisionAlgorithmCreateFuncD2Ev,"axG",@progbits,_ZN30btCollisionAlgorithmCreateFuncD2Ev,comdat
	.weak	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.p2align	4, 0x90
	.type	_ZN30btCollisionAlgorithmCreateFuncD2Ev,@function
_ZN30btCollisionAlgorithmCreateFuncD2Ev: # @_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZN30btCollisionAlgorithmCreateFuncD2Ev, .Lfunc_end8-_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.cfi_endproc

	.section	.text._ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev: # @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end9:
	.size	_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end9-_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 48
.Lcfi29:
	.cfi_offset %rbx, -40
.Lcfi30:
	.cfi_offset %r12, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$120, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end10-_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev,"axG",@progbits,_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev,comdat
	.weak	_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev,@function
_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev: # @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end11:
	.size	_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev, .Lfunc_end11-_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 48
.Lcfi38:
	.cfi_offset %rbx, -40
.Lcfi39:
	.cfi_offset %r12, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$120, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end12-_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev: # @_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end13:
	.size	_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end13-_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -40
.Lcfi48:
	.cfi_offset %r12, -32
.Lcfi49:
	.cfi_offset %r14, -24
.Lcfi50:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$72, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end14-_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev,"axG",@progbits,_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev,comdat
	.weak	_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev,@function
_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev: # @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end15:
	.size	_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev, .Lfunc_end15-_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 48
.Lcfi56:
	.cfi_offset %rbx, -40
.Lcfi57:
	.cfi_offset %r12, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$72, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end16-_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN16btEmptyAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN16btEmptyAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN16btEmptyAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN16btEmptyAlgorithm10CreateFuncD0Ev,@function
_ZN16btEmptyAlgorithm10CreateFuncD0Ev:  # @_ZN16btEmptyAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end17:
	.size	_ZN16btEmptyAlgorithm10CreateFuncD0Ev, .Lfunc_end17-_ZN16btEmptyAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -24
.Lcfi64:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	(%r14), %rdi
	movq	(%rdi), %rax
	movl	$16, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZN16btEmptyAlgorithmC1ERK36btCollisionAlgorithmConstructionInfo
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end18-_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev: # @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end19:
	.size	_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end19-_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 48
.Lcfi70:
	.cfi_offset %rbx, -40
.Lcfi71:
	.cfi_offset %r12, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$32, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	_ZN32btSphereSphereCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end20-_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev: # @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end21:
	.size	_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end21-_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 48
.Lcfi79:
	.cfi_offset %rbx, -48
.Lcfi80:
	.cfi_offset %r12, -40
.Lcfi81:
	.cfi_offset %r13, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$40, %esi
	callq	*96(%rax)
	movq	%rax, %r13
	movq	8(%rbx), %rsi
	movzbl	8(%r12), %r9d
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	_ZN34btSphereTriangleCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end22-_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev: # @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end23:
	.size	_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end23-_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 48
.Lcfi89:
	.cfi_offset %rbx, -40
.Lcfi90:
	.cfi_offset %r12, -32
.Lcfi91:
	.cfi_offset %r14, -24
.Lcfi92:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$32, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	_ZN26btBoxBoxCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end24:
	.size	_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end24-_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev: # @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end25:
	.size	_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end25-_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 48
.Lcfi98:
	.cfi_offset %rbx, -48
.Lcfi99:
	.cfi_offset %r12, -40
.Lcfi100:
	.cfi_offset %r13, -32
.Lcfi101:
	.cfi_offset %r14, -24
.Lcfi102:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	(%r13), %rdi
	movq	(%rdi), %rax
	movl	$48, %esi
	callq	*96(%rax)
	movq	%rax, %r12
	movl	12(%rbx), %r10d
	movl	16(%rbx), %eax
	cmpb	$0, 8(%rbx)
	je	.LBB26_1
# BB#2:
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	pushq	%rax
.Lcfi103:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	callq	_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii
	addq	$16, %rsp
.Lcfi105:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB26_3
.LBB26_1:
	xorl	%esi, %esi
	movl	$0, %r9d
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	pushq	%rax
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	callq	_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_bii
	addq	$16, %rsp
.Lcfi108:
	.cfi_adjust_cfa_offset -16
.LBB26_3:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end26-_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.type	_ZTV31btDefaultCollisionConfiguration,@object # @_ZTV31btDefaultCollisionConfiguration
	.section	.rodata,"a",@progbits
	.globl	_ZTV31btDefaultCollisionConfiguration
	.p2align	3
_ZTV31btDefaultCollisionConfiguration:
	.quad	0
	.quad	_ZTI31btDefaultCollisionConfiguration
	.quad	_ZN31btDefaultCollisionConfigurationD2Ev
	.quad	_ZN31btDefaultCollisionConfigurationD0Ev
	.quad	_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv
	.quad	_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv
	.quad	_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv
	.quad	_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii
	.size	_ZTV31btDefaultCollisionConfiguration, 64

	.type	_ZTS31btDefaultCollisionConfiguration,@object # @_ZTS31btDefaultCollisionConfiguration
	.globl	_ZTS31btDefaultCollisionConfiguration
	.p2align	4
_ZTS31btDefaultCollisionConfiguration:
	.asciz	"31btDefaultCollisionConfiguration"
	.size	_ZTS31btDefaultCollisionConfiguration, 34

	.type	_ZTS24btCollisionConfiguration,@object # @_ZTS24btCollisionConfiguration
	.section	.rodata._ZTS24btCollisionConfiguration,"aG",@progbits,_ZTS24btCollisionConfiguration,comdat
	.weak	_ZTS24btCollisionConfiguration
	.p2align	4
_ZTS24btCollisionConfiguration:
	.asciz	"24btCollisionConfiguration"
	.size	_ZTS24btCollisionConfiguration, 27

	.type	_ZTI24btCollisionConfiguration,@object # @_ZTI24btCollisionConfiguration
	.section	.rodata._ZTI24btCollisionConfiguration,"aG",@progbits,_ZTI24btCollisionConfiguration,comdat
	.weak	_ZTI24btCollisionConfiguration
	.p2align	3
_ZTI24btCollisionConfiguration:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS24btCollisionConfiguration
	.size	_ZTI24btCollisionConfiguration, 16

	.type	_ZTI31btDefaultCollisionConfiguration,@object # @_ZTI31btDefaultCollisionConfiguration
	.section	.rodata,"a",@progbits
	.globl	_ZTI31btDefaultCollisionConfiguration
	.p2align	4
_ZTI31btDefaultCollisionConfiguration:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS31btDefaultCollisionConfiguration
	.quad	_ZTI24btCollisionConfiguration
	.size	_ZTI31btDefaultCollisionConfiguration, 24

	.type	_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE,@object # @_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE,@object # @_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE:
	.asciz	"N33btConvexConcaveCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE, 50

	.type	_ZTS30btCollisionAlgorithmCreateFunc,@object # @_ZTS30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTS30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTS30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTS30btCollisionAlgorithmCreateFunc
	.p2align	4
_ZTS30btCollisionAlgorithmCreateFunc:
	.asciz	"30btCollisionAlgorithmCreateFunc"
	.size	_ZTS30btCollisionAlgorithmCreateFunc, 33

	.type	_ZTI30btCollisionAlgorithmCreateFunc,@object # @_ZTI30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTI30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTI30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTI30btCollisionAlgorithmCreateFunc
	.p2align	3
_ZTI30btCollisionAlgorithmCreateFunc:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS30btCollisionAlgorithmCreateFunc
	.size	_ZTI30btCollisionAlgorithmCreateFunc, 16

	.type	_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE,@object # @_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE, 24

	.type	_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE,@object # @_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE
	.section	.rodata._ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE,"aG",@progbits,_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE,comdat
	.weak	_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE
	.p2align	3
_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE:
	.quad	0
	.quad	_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev
	.quad	_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE, 40

	.type	_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE,@object # @_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE
	.section	.rodata._ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE,"aG",@progbits,_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE,comdat
	.weak	_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE
	.p2align	4
_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE:
	.asciz	"N33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE"
	.size	_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE, 57

	.type	_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE,@object # @_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE
	.section	.rodata._ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE,"aG",@progbits,_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE,comdat
	.weak	_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE
	.p2align	4
_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE, 24

	.type	_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE,@object # @_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN28btCompoundCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE,@object # @_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN28btCompoundCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE:
	.asciz	"N28btCompoundCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE, 45

	.type	_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE,@object # @_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN28btCompoundCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE, 24

	.type	_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE,@object # @_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE
	.section	.rodata._ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE,"aG",@progbits,_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE,comdat
	.weak	_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE
	.p2align	3
_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE:
	.quad	0
	.quad	_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev
	.quad	_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE, 40

	.type	_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE,@object # @_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE
	.section	.rodata._ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE,"aG",@progbits,_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE,comdat
	.weak	_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE
	.p2align	4
_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE:
	.asciz	"N28btCompoundCollisionAlgorithm17SwappedCreateFuncE"
	.size	_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE, 52

	.type	_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE,@object # @_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE
	.section	.rodata._ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE,"aG",@progbits,_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE,comdat
	.weak	_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE
	.p2align	4
_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE, 24

	.type	_ZTVN16btEmptyAlgorithm10CreateFuncE,@object # @_ZTVN16btEmptyAlgorithm10CreateFuncE
	.section	.rodata._ZTVN16btEmptyAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN16btEmptyAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN16btEmptyAlgorithm10CreateFuncE
	.p2align	3
_ZTVN16btEmptyAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN16btEmptyAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN16btEmptyAlgorithm10CreateFuncD0Ev
	.quad	_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN16btEmptyAlgorithm10CreateFuncE, 40

	.type	_ZTSN16btEmptyAlgorithm10CreateFuncE,@object # @_ZTSN16btEmptyAlgorithm10CreateFuncE
	.section	.rodata._ZTSN16btEmptyAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN16btEmptyAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN16btEmptyAlgorithm10CreateFuncE
	.p2align	4
_ZTSN16btEmptyAlgorithm10CreateFuncE:
	.asciz	"N16btEmptyAlgorithm10CreateFuncE"
	.size	_ZTSN16btEmptyAlgorithm10CreateFuncE, 33

	.type	_ZTIN16btEmptyAlgorithm10CreateFuncE,@object # @_ZTIN16btEmptyAlgorithm10CreateFuncE
	.section	.rodata._ZTIN16btEmptyAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN16btEmptyAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN16btEmptyAlgorithm10CreateFuncE
	.p2align	4
_ZTIN16btEmptyAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN16btEmptyAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN16btEmptyAlgorithm10CreateFuncE, 24

	.type	_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE,@object # @_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE,@object # @_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE:
	.asciz	"N32btSphereSphereCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE, 49

	.type	_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE,@object # @_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE, 24

	.type	_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE,@object # @_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE,@object # @_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE:
	.asciz	"N34btSphereTriangleCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE, 51

	.type	_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE,@object # @_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE, 24

	.type	_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE,@object # @_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE,@object # @_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE:
	.asciz	"N26btBoxBoxCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE, 43

	.type	_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE,@object # @_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE, 24

	.type	_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE,@object # @_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE,@object # @_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE:
	.asciz	"N31btConvexPlaneCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE, 48

	.type	_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE,@object # @_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE, 24


	.globl	_ZN31btDefaultCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo
	.type	_ZN31btDefaultCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo,@function
_ZN31btDefaultCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo = _ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
	.globl	_ZN31btDefaultCollisionConfigurationD1Ev
	.type	_ZN31btDefaultCollisionConfigurationD1Ev,@function
_ZN31btDefaultCollisionConfigurationD1Ev = _ZN31btDefaultCollisionConfigurationD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
