	.text
	.file	"image.bc"
	.globl	MbAffPostProc
	.p2align	4, 0x90
	.type	MbAffPostProc,@function
MbAffPostProc:                          # @MbAffPostProc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1096, %rsp             # imm = 0x448
.Lcfi6:
	.cfi_def_cfa_offset 1152
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	img(%rip), %rbp
	cmpl	$0, 15536(%rbp)
	movl	15348(%rbp), %ecx
	je	.LBB0_15
# BB#1:                                 # %.preheader69
	testl	%ecx, %ecx
	jle	.LBB0_29
# BB#2:                                 # %.lr.ph82.preheader
	movq	6472(%rax), %r13
	addq	$24, 24(%rsp)           # 8-byte Folded Spill
	xorl	%r15d, %r15d
	movq	%r13, 32(%rsp)          # 8-byte Spill
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_28:                               # %.loopexit67..lr.ph82_crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	enc_picture(%rip), %rax
.LBB0_3:                                # %.lr.ph82
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
                                        #     Child Loop BB0_7 Depth 2
                                        #     Child Loop BB0_10 Depth 2
                                        #     Child Loop BB0_13 Depth 2
                                        #     Child Loop BB0_31 Depth 2
                                        #     Child Loop BB0_35 Depth 2
	movq	6480(%rax), %rax
	cmpb	$0, (%rax,%r15)
	je	.LBB0_27
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	leaq	4(%rsp), %rsi
	movq	%rsp, %rdx
	callq	get_mb_pos
	movslq	(%rsp), %r8
	movslq	4(%rsp), %rsi
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r8,8), %rax
	movq	%rax, %rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_5:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdx), %rbp
	movups	16(%rbp,%rsi,2), %xmm0
	movups	%xmm0, 80(%rsp,%rdi)
	movups	(%rbp,%rsi,2), %xmm0
	movups	%xmm0, 64(%rsp,%rdi)
	movq	-16(%rdx), %rbp
	movups	16(%rbp,%rsi,2), %xmm0
	movups	%xmm0, 112(%rsp,%rdi)
	movups	(%rbp,%rsi,2), %xmm0
	movups	%xmm0, 96(%rsp,%rdi)
	movq	-8(%rdx), %rbp
	movups	16(%rbp,%rsi,2), %xmm0
	movups	%xmm0, 144(%rsp,%rdi)
	movups	(%rbp,%rsi,2), %xmm0
	movups	%xmm0, 128(%rsp,%rdi)
	movq	(%rdx), %rbp
	movups	16(%rbp,%rsi,2), %xmm0
	movups	%xmm0, 176(%rsp,%rdi)
	movups	(%rbp,%rsi,2), %xmm0
	movups	%xmm0, 160(%rsp,%rdi)
	subq	$-128, %rdi
	addq	$32, %rdx
	cmpq	$1024, %rdi             # imm = 0x400
	jne	.LBB0_5
# BB#6:                                 # %.preheader68.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader68
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rax), %rdi
	movups	64(%rsp,%rdx), %xmm0
	movups	80(%rsp,%rdx), %xmm1
	movups	%xmm1, 16(%rdi,%rsi,2)
	movups	%xmm0, (%rdi,%rsi,2)
	movq	-16(%rax), %rdi
	movups	576(%rsp,%rdx), %xmm0
	movups	592(%rsp,%rdx), %xmm1
	movups	%xmm1, 16(%rdi,%rsi,2)
	movups	%xmm0, (%rdi,%rsi,2)
	movq	-8(%rax), %rdi
	movups	96(%rsp,%rdx), %xmm0
	movups	112(%rsp,%rdx), %xmm1
	movups	%xmm1, 16(%rdi,%rsi,2)
	movups	%xmm0, (%rdi,%rsi,2)
	movq	(%rax), %rdi
	movups	608(%rsp,%rdx), %xmm0
	movups	624(%rsp,%rdx), %xmm1
	movups	%xmm1, 16(%rdi,%rsi,2)
	movups	%xmm0, (%rdi,%rsi,2)
	addq	$32, %rax
	addq	$64, %rdx
	cmpq	$512, %rdx              # imm = 0x200
	jne	.LBB0_7
# BB#8:                                 # %.preheader66
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	img(%rip), %rbp
	movl	15544(%rbp), %ebx
	movl	$16, %eax
	xorl	%edx, %edx
	idivl	%ebx
	movl	%eax, %edi
	movl	%esi, %eax
	cltd
	idivl	%edi
	movl	%eax, %edi
	movl	%edi, 4(%rsp)
	movslq	15548(%rbp), %rcx
	movl	$16, %eax
	xorl	%edx, %edx
	idivl	%ecx
	movl	%eax, %esi
	movl	%r8d, %eax
	cltd
	idivl	%esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	testq	%rcx, %rcx
	movl	%eax, (%rsp)
	jle	.LBB0_27
# BB#9:                                 # %.lr.ph77
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movl	%ebx, 52(%rsp)          # 4-byte Spill
	movslq	%ebx, %rbx
	movl	%edi, 48(%rsp)          # 4-byte Spill
	movslq	%edi, %rbp
	movq	%r13, %rdx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rcx), %r13
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movslq	%eax, %r15
	addq	%rbx, %rbx
	shlq	$3, %r15
	addq	(%rdx), %r15
	leaq	64(%rsp), %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_10:                               #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r12,8), %rax
	leaq	(%rax,%rbp,2), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	incq	%r12
	addq	$32, %r14
	cmpq	%r13, %r12
	jl	.LBB0_10
# BB#11:                                # %.preheader65
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	jle	.LBB0_27
# BB#12:                                # %.lr.ph79.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %r12d
	leaq	64(%rsp), %r14
	xorl	%ebx, %ebx
	movl	52(%rsp), %edx          # 4-byte Reload
	movl	48(%rsp), %esi          # 4-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_14:                               # %._crit_edge104
                                        #   in Loop: Header=BB0_13 Depth=2
	movl	(%rsp), %ecx
	movl	4(%rsp), %esi
	movl	15544(%rbp), %edx
	addq	$32, %r14
	addl	$2, %r12d
.LBB0_13:                               # %.lr.ph79
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	leal	-1(%rcx,%r12), %ecx
	movslq	%ecx, %rcx
	movslq	%esi, %rdi
	addq	%rdi, %rdi
	addq	(%rax,%rcx,8), %rdi
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	movq	%r14, %rsi
	callq	memcpy
	movq	(%r13), %rax
	movl	(%rsp), %ecx
	addl	%r12d, %ecx
	movslq	%ecx, %rcx
	movslq	4(%rsp), %rdi
	addq	%rdi, %rdi
	addq	(%rax,%rcx,8), %rdi
	movq	img(%rip), %rax
	movl	15548(%rax), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	leaq	64(%rsp,%rcx), %rsi
	movslq	15544(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rbp
	movslq	15548(%rbp), %r15
	cmpq	%r15, %rbx
	jl	.LBB0_14
# BB#25:                                # %._crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%r15d, %r15d
	jle	.LBB0_26
# BB#30:                                # %.lr.ph77.1
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	4(%rsp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rax
	leaq	(%r15,%r15), %r13
	movslq	(%rsp), %r14
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movslq	15544(%rbp), %r12
	addq	%r12, %r12
	shlq	$3, %r14
	addq	8(%rax), %r14
	leaq	64(%rsp), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_31:                               #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rbx,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,2), %rsi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	memcpy
	incq	%rbx
	addq	$32, %rbp
	cmpq	%r13, %rbx
	jl	.LBB0_31
# BB#32:                                # %.preheader65.1
                                        #   in Loop: Header=BB0_3 Depth=1
	testl	%r15d, %r15d
	jle	.LBB0_33
# BB#34:                                # %.lr.ph79.1.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %r12d
	leaq	64(%rsp), %r14
	xorl	%ebx, %ebx
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_35:                               # %.lr.ph79.1
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rax
	movl	(%rsp), %ecx
	leal	-1(%r12,%rcx), %ecx
	movslq	%ecx, %rcx
	movslq	4(%rsp), %rdi
	addq	%rdi, %rdi
	addq	(%rax,%rcx,8), %rdi
	movslq	15544(%rbp), %rdx
	addq	%rdx, %rdx
	movq	%r14, %rsi
	callq	memcpy
	movq	8(%r13), %rax
	movl	(%rsp), %ecx
	addl	%r12d, %ecx
	movslq	%ecx, %rcx
	movslq	4(%rsp), %rdi
	addq	%rdi, %rdi
	addq	(%rax,%rcx,8), %rdi
	movq	img(%rip), %rax
	movl	15548(%rax), %ecx
	addl	%ebx, %ecx
	movslq	%ecx, %rcx
	shlq	$5, %rcx
	leaq	64(%rsp,%rcx), %rsi
	movslq	15544(%rax), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbx
	movq	img(%rip), %rbp
	movslq	15548(%rbp), %rax
	addq	$32, %r14
	addl	$2, %r12d
	cmpq	%rax, %rbx
	jl	.LBB0_35
	jmp	.LBB0_27
.LBB0_26:                               #   in Loop: Header=BB0_3 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB0_27
.LBB0_33:                               #   in Loop: Header=BB0_3 Depth=1
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_27:                               # %.loopexit67
                                        #   in Loop: Header=BB0_3 Depth=1
	addq	$2, %r15
	movslq	15348(%rbp), %rax
	cmpq	%rax, %r15
	jl	.LBB0_28
	jmp	.LBB0_29
.LBB0_15:                               # %.preheader63
	testl	%ecx, %ecx
	jle	.LBB0_29
# BB#16:                                # %.lr.ph.preheader
	addq	$24, 24(%rsp)           # 8-byte Folded Spill
	xorl	%ebx, %ebx
	leaq	4(%rsp), %r14
	movq	%rsp, %r15
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_24:                               # %.loopexit..lr.ph_crit_edge
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	enc_picture(%rip), %rax
.LBB0_17:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_21 Depth 2
	movq	6480(%rax), %rax
	cmpb	$0, (%rax,%rbx)
	je	.LBB0_23
# BB#18:                                #   in Loop: Header=BB0_17 Depth=1
	xorl	%ecx, %ecx
	movl	%ebx, %edi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	get_mb_pos
	movslq	4(%rsp), %rax
	movslq	(%rsp), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	movq	%rcx, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_19:                               #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdx), %rdi
	movups	16(%rdi,%rax,2), %xmm0
	movups	%xmm0, 80(%rsp,%rsi)
	movups	(%rdi,%rax,2), %xmm0
	movups	%xmm0, 64(%rsp,%rsi)
	movq	-16(%rdx), %rdi
	movups	16(%rdi,%rax,2), %xmm0
	movups	%xmm0, 112(%rsp,%rsi)
	movups	(%rdi,%rax,2), %xmm0
	movups	%xmm0, 96(%rsp,%rsi)
	movq	-8(%rdx), %rdi
	movups	16(%rdi,%rax,2), %xmm0
	movups	%xmm0, 144(%rsp,%rsi)
	movups	(%rdi,%rax,2), %xmm0
	movups	%xmm0, 128(%rsp,%rsi)
	movq	(%rdx), %rdi
	movups	16(%rdi,%rax,2), %xmm0
	movups	%xmm0, 176(%rsp,%rsi)
	movups	(%rdi,%rax,2), %xmm0
	movups	%xmm0, 160(%rsp,%rsi)
	subq	$-128, %rsi
	addq	$32, %rdx
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB0_19
# BB#20:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_17 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_21:                               # %.preheader
                                        #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rcx), %rsi
	movups	64(%rsp,%rdx), %xmm0
	movups	80(%rsp,%rdx), %xmm1
	movups	%xmm1, 16(%rsi,%rax,2)
	movups	%xmm0, (%rsi,%rax,2)
	movq	-16(%rcx), %rsi
	movups	576(%rsp,%rdx), %xmm0
	movups	592(%rsp,%rdx), %xmm1
	movups	%xmm1, 16(%rsi,%rax,2)
	movups	%xmm0, (%rsi,%rax,2)
	movq	-8(%rcx), %rsi
	movups	96(%rsp,%rdx), %xmm0
	movups	112(%rsp,%rdx), %xmm1
	movups	%xmm1, 16(%rsi,%rax,2)
	movups	%xmm0, (%rsi,%rax,2)
	movq	(%rcx), %rsi
	movups	608(%rsp,%rdx), %xmm0
	movups	624(%rsp,%rdx), %xmm1
	movups	%xmm1, 16(%rsi,%rax,2)
	movups	%xmm0, (%rsi,%rax,2)
	addq	$32, %rcx
	addq	$64, %rdx
	cmpq	$512, %rdx              # imm = 0x200
	jne	.LBB0_21
# BB#22:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	img(%rip), %rbp
.LBB0_23:                               # %.loopexit
                                        #   in Loop: Header=BB0_17 Depth=1
	addq	$2, %rbx
	movslq	15348(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_24
.LBB0_29:                               # %.loopexit64
	addq	$1096, %rsp             # imm = 0x448
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	MbAffPostProc, .Lfunc_end0-MbAffPostProc
	.cfi_endproc

	.globl	code_a_picture
	.p2align	4, 0x90
	.type	code_a_picture,@function
code_a_picture:                         # @code_a_picture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	input(%rip), %rsi
	movl	1560(%rsi), %ebx
	movq	img(%rip), %rcx
	movl	(%rcx), %edi
	movl	start_frame_no_in_this_IGOP(%rip), %ebp
	movl	%edi, %edx
	subl	%ebp, %edx
	testl	%ebx, %ebx
	je	.LBB1_2
# BB#1:
	movl	%edx, %eax
	cltd
	idivl	%ebx
.LBB1_2:
	movq	%r14, 14208(%rcx)
	cmpl	%ebp, %edi
	jne	.LBB1_4
# BB#3:
	movb	$1, %al
	cmpl	$2, 24(%rcx)
	jne	.LBB1_10
.LBB1_4:
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.LBB1_10
# BB#5:
	movl	1568(%rsi), %edx
	testl	%edx, %edx
	je	.LBB1_10
# BB#6:
	movl	20(%rcx), %eax
	cmpl	$4, %eax
	je	.LBB1_9
# BB#7:
	cmpl	$2, %eax
	jne	.LBB1_8
.LBB1_9:
	cmpl	$2, 24(%rcx)
	setne	%al
	jmp	.LBB1_10
.LBB1_8:
	xorl	%eax, %eax
.LBB1_10:
	movzbl	%al, %eax
	movl	%eax, 4(%r14)
	movl	$0, (%r14)
	movl	$0, 820(%r14)
	movq	$0, 812(%r14)
	callq	RandomIntraNewPicture
	movq	img(%rip), %rdi
	movl	$1, 15436(%rdi)
	movq	active_pps(%rip), %rsi
	movq	active_sps(%rip), %rdx
	callq	FmoInit
	callq	FmoStartPicture
	callq	CalculateQuantParam
	callq	CalculateOffsetParam
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB1_12
# BB#11:
	callq	CalculateQuant8Param
	callq	CalculateOffset8Param
.LBB1_12:
	callq	reset_pic_bin_count
	movq	img(%rip), %rax
	movl	$0, 15600(%rax)
	cmpl	$0, 15348(%rax)
	je	.LBB1_17
# BB#13:                                # %.preheader25.preheader
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	jmp	.LBB1_15
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph28
                                        #   in Loop: Header=BB1_15 Depth=1
	movl	%ebx, %edi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	encode_one_slice
	addl	%eax, %ebp
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	callq	FmoSetLastMacroblockInSlice
	movq	img(%rip), %rax
	incl	16(%rax)
	movq	stats(%rip), %rax
	movl	$0, 32(%rax)
.LBB1_15:                               # %.lr.ph28
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	FmoSliceGroupCompletelyCoded
	testl	%eax, %eax
	je	.LBB1_14
# BB#16:                                # %._crit_edge
                                        #   in Loop: Header=BB1_15 Depth=1
	incl	%ebx
	movq	img(%rip), %rax
	cmpl	15348(%rax), %ebp
	jb	.LBB1_15
.LBB1_17:                               # %._crit_edge31
	callq	FmoEndPicture
	movq	input(%rip), %rax
	cmpl	$3, 4168(%rax)
	jne	.LBB1_23
# BB#18:
	movq	img(%rip), %rdi
	cmpl	$1, 20(%rdi)
	je	.LBB1_23
# BB#19:                                # %.preheader
	cmpl	$0, 4728(%rax)
	jle	.LBB1_23
# BB#20:                                # %.lr.ph.preheader
	movq	decs(%rip), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	xorl	%edx, %edx
	callq	DeblockFrame
	movq	input(%rip), %rax
	cmpl	$2, 4728(%rax)
	jl	.LBB1_23
# BB#21:                                # %.lr.ph..lr.ph_crit_edge.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rdi
	movq	decs(%rip), %rax
	movq	24(%rax), %rax
	movq	(%rax,%rbx,8), %rsi
	xorl	%edx, %edx
	callq	DeblockFrame
	incq	%rbx
	movq	input(%rip), %rax
	movslq	4728(%rax), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_22
.LBB1_23:                               # %.loopexit
	movq	img(%rip), %rdi
	movq	enc_picture(%rip), %rax
	movq	6440(%rax), %rsi
	movq	6472(%rax), %rdx
	callq	DeblockFrame
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	je	.LBB1_24
# BB#25:
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	MbAffPostProc           # TAILCALL
.LBB1_24:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	code_a_picture, .Lfunc_end1-code_a_picture
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
.LCPI2_2:
	.long	1062836634              # float 0.850000023
	.long	1065353216              # float 1
.LCPI2_3:
	.long	1066611507              # float 1.14999998
	.long	1065353216              # float 1
.LCPI2_4:
	.quad	4613937818241073152     # double 3
.LCPI2_5:
	.quad	4604300115038500291     # double 0.68000000000000005
.LCPI2_6:
	.quad	4621819117588971520     # double 10
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_1:
	.long	1065353216              # float 1
	.text
	.globl	encode_one_frame
	.p2align	4, 0x90
	.type	encode_one_frame,@function
encode_one_frame:                       # @encode_one_frame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 192
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	$0, me_time(%rip)
	movq	img(%rip), %rax
	movl	$0, 14260(%rax)
	movq	$0, enc_frame_picture(%rip)
	movq	$0, enc_frame_picture2(%rip)
	movq	$0, enc_frame_picture3(%rip)
	leaq	120(%rsp), %rdi
	callq	ftime
	leaq	96(%rsp), %rdi
	callq	time
	movq	img(%rip), %rsi
	movl	$0, 15408(%rsi)
	movq	input(%rip), %rax
	cmpl	$0, 2092(%rax)
	je	.LBB2_3
# BB#1:
	cmpl	$0, (%rsi)
	je	.LBB2_3
# BB#2:
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	write_PPS
	movq	stats(%rip), %rcx
	movl	%eax, 2260(%rcx)
	addl	%eax, 2256(%rcx)
	movq	img(%rip), %rsi
.LBB2_3:
	movq	imgY_org_frm(%rip), %rax
	movq	%rax, imgY_org(%rip)
	movq	imgUV_org_frm(%rip), %rax
	movq	%rax, imgUV_org(%rip)
	movq	last_P_no_frm(%rip), %rcx
	movq	%rcx, last_P_no(%rip)
	movq	$0, 12(%rsi)
	movq	stats(%rip), %rax
	movl	$0, 32(%rax)
	movl	$0, 120(%rsi)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 176(%rsi)
	movupd	%xmm0, 160(%rsi)
	movslq	15352(%rsi), %rax
	testq	%rax, %rax
	jle	.LBB2_10
# BB#4:                                 # %.lr.ph52.i
	movq	14224(%rsi), %rdi
	leaq	-1(%rax), %r8
	movq	%rax, %rbx
	xorl	%ebp, %ebp
	andq	$7, %rbx
	je	.LBB2_7
# BB#5:                                 # %.prol.preheader392
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movl	$-1, (%rdx)
	incq	%rbp
	addq	$536, %rdx              # imm = 0x218
	cmpq	%rbp, %rbx
	jne	.LBB2_6
.LBB2_7:                                # %.prol.loopexit393
	cmpq	$7, %r8
	jb	.LBB2_10
# BB#8:                                 # %.lr.ph52.i.new
	subq	%rbp, %rax
	imulq	$536, %rbp, %rdx        # imm = 0x218
	addq	%rdx, %rdi
	.p2align	4, 0x90
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movl	$-1, (%rdi)
	movl	$-1, 536(%rdi)
	movl	$-1, 1072(%rdi)
	movl	$-1, 1608(%rdi)
	movl	$-1, 2144(%rdi)
	movl	$-1, 2680(%rdi)
	movl	$-1, 3216(%rdi)
	movl	$-1, 3752(%rdi)
	addq	$4288, %rdi             # imm = 0x10C0
	addq	$-8, %rax
	jne	.LBB2_9
.LBB2_10:                               # %._crit_edge53.i
	cmpl	$0, 14364(%rsi)
	je	.LBB2_19
# BB#11:
	movq	input(%rip), %r8
	movl	20(%r8), %edx
	incl	%edx
	movl	%edx, 14360(%rsi)
	movl	start_tr_in_this_IGOP(%rip), %r9d
	movl	(%rsi), %eax
	subl	start_frame_no_in_this_IGOP(%rip), %eax
	leal	-1(%rax), %edi
	imull	%edx, %edi
	addl	%r9d, %edi
	imull	%edx, %eax
	movl	%edi, (%rcx)
	cmpl	$2, 15240(%rsi)
	jl	.LBB2_14
# BB#12:                                # %.lr.ph.i.preheader
	movl	$1, %ebx
	movl	%edi, %edx
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	subl	14360(%rsi), %edx
	movl	%edx, (%rcx,%rbx,4)
	incq	%rbx
	movslq	15240(%rsi), %rbp
	cmpq	%rbp, %rbx
	jl	.LBB2_13
.LBB2_14:                               # %._crit_edge.i
	addl	%r9d, %eax
	movl	4144(%r8), %ecx
	testl	%ecx, %ecx
	je	.LBB2_17
# BB#15:
	movl	(%rsi), %edx
	incl	%edx
	cmpl	8(%r8), %edx
	jne	.LBB2_17
# BB#16:
	movl	%ecx, %eax
	subl	%edi, %eax
	movl	%eax, 14360(%rsi)
	movl	%ecx, %eax
.LBB2_17:
	movl	20(%r8), %ecx
	incl	%ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	2096(%r8), %xmm1
	addsd	.LCPI2_0(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 14352(%rsi)
	movl	2968(%r8), %ecx
	cmpl	$3, %ecx
	jne	.LBB2_33
# BB#18:                                # %.thread.i
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, 14352(%rsi)
	movl	14364(%rsi), %ebp
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	jmp	.LBB2_34
.LBB2_19:
	movl	(%rsi), %eax
	movl	start_frame_no_in_this_IGOP(%rip), %edi
	movl	%eax, %edx
	subl	%edi, %edx
	movq	input(%rip), %rbp
	movl	20(%rbp), %ecx
	incl	%ecx
	imull	%edx, %ecx
	addl	start_tr_in_this_IGOP(%rip), %ecx
	movl	%ecx, 14248(%rsi)
	movl	14332(%rsi), %edx
	movl	%edx, 14336(%rsi)
	movl	%ecx, 14332(%rsi)
	movl	4144(%rbp), %edx
	testl	%edx, %edx
	je	.LBB2_22
# BB#20:
	leal	1(%rax), %ebx
	cmpl	8(%rbp), %ebx
	jne	.LBB2_22
# BB#21:
	movl	%edx, 14248(%rsi)
	movl	%edx, %ecx
.LBB2_22:
	cmpl	%edi, %eax
	je	.LBB2_25
# BB#23:
	cmpl	$0, 2096(%rbp)
	je	.LBB2_25
# BB#24:
	movl	%ecx, nextP_tr_frm(%rip)
.LBB2_25:
	cmpl	$0, 5116(%rbp)
	jne	.LBB2_73
# BB#26:
	movl	20(%rsi), %r8d
	movl	4156(%rbp), %ebx
	cmpl	$2, %r8d
	jne	.LBB2_53
# BB#27:
	testl	%ebx, %ebx
	jle	.LBB2_62
# BB#28:
	cmpl	%ebx, %ecx
	jl	.LBB2_30
# BB#29:
	cmpl	$0, 2152(%rbp)
	je	.LBB2_32
.LBB2_30:
	leal	(%rbx,%rbx), %r8d
	movl	%ecx, %eax
	cltd
	idivl	%r8d
	cmpl	%ebx, %edx
	jl	.LBB2_62
# BB#31:
	cmpl	$1, 2152(%rbp)
	jne	.LBB2_62
.LBB2_32:
	leaq	4160(%rbp), %rax
	jmp	.LBB2_63
.LBB2_33:
	testl	%ecx, %ecx
	movl	14364(%rsi), %ebp
	je	.LBB2_43
.LBB2_34:
	movq	gop_structure(%rip), %rcx
	leal	-1(%rbp), %edx
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movl	4(%rcx,%rdx,8), %ecx
	incl	%ecx
	xorl	%ebx, %ebx
.LBB2_35:
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %edx
	addl	%edi, %edx
	leal	-1(%rax), %ecx
	cmpl	%eax, %edx
	cmovll	%edx, %ecx
	movl	%ecx, 14248(%rsi)
	cmpl	$0, 5116(%r8)
	jne	.LBB2_75
# BB#36:
	testb	%bl, %bl
	je	.LBB2_44
# BB#37:
	movl	4156(%r8), %ebp
	testl	%ebp, %ebp
	jle	.LBB2_45
# BB#38:
	cmpl	%ebp, %ecx
	jl	.LBB2_40
# BB#39:
	cmpl	$0, 2152(%r8)
	je	.LBB2_42
.LBB2_40:
	leal	(%rbp,%rbp), %edi
	movl	%ecx, %eax
	cltd
	idivl	%edi
	cmpl	%ebp, %edx
	jl	.LBB2_45
# BB#41:
	cmpl	$1, 2152(%r8)
	jne	.LBB2_45
.LBB2_42:
	leaq	4152(%r8), %rax
	jmp	.LBB2_46
.LBB2_43:
	movb	$1, %bl
	movl	%ebp, %ecx
	jmp	.LBB2_35
.LBB2_44:
	movq	gop_structure(%rip), %rax
	decl	%ebp
	movslq	%ebp, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	12(%rax,%rcx,8), %eax
	jmp	.LBB2_61
.LBB2_45:
	leaq	2104(%r8), %rax
.LBB2_46:
	movl	(%rax), %eax
	movl	%eax, 36(%rsi)
	cmpl	$0, 15360(%rsi)
	je	.LBB2_75
# BB#47:
	testl	%ebp, %ebp
	jle	.LBB2_59
# BB#48:
	cmpl	%ebp, %ecx
	jl	.LBB2_50
# BB#49:
	cmpl	$0, 2152(%r8)
	je	.LBB2_52
.LBB2_50:
	leal	(%rbp,%rbp), %edi
	movl	%ecx, %eax
	cltd
	idivl	%edi
	cmpl	%ebp, %edx
	jl	.LBB2_59
# BB#51:
	cmpl	$1, 2152(%r8)
	jne	.LBB2_59
.LBB2_52:
	leaq	4152(%r8), %rax
	addq	$4164, %r8              # imm = 0x1044
	jmp	.LBB2_60
.LBB2_53:
	testl	%ebx, %ebx
	jle	.LBB2_64
# BB#54:
	cmpl	%ebx, %ecx
	jl	.LBB2_56
# BB#55:
	cmpl	$0, 2152(%rbp)
	je	.LBB2_58
.LBB2_56:
	leal	(%rbx,%rbx), %r9d
	movl	%ecx, %eax
	cltd
	idivl	%r9d
	cmpl	%ebx, %edx
	jl	.LBB2_64
# BB#57:
	cmpl	$1, 2152(%rbp)
	jne	.LBB2_64
.LBB2_58:
	movl	4148(%rbp), %edx
	jmp	.LBB2_65
.LBB2_59:
	leaq	2104(%r8), %rax
	addq	$2108, %r8              # imm = 0x83C
.LBB2_60:
	xorl	%ecx, %ecx
	subl	15452(%rsi), %ecx
	movl	(%r8), %edx
	addl	(%rax), %edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	cmpl	$52, %edx
	movl	$51, %eax
	cmovll	%edx, %eax
.LBB2_61:                               # %init_frame.exit
	movl	%eax, 36(%rsi)
	jmp	.LBB2_75
.LBB2_62:
	leaq	12(%rbp), %rax
.LBB2_63:
	movl	(%rax), %eax
	movl	%eax, 36(%rsi)
	jmp	.LBB2_73
.LBB2_64:
	movl	16(%rbp), %edx
.LBB2_65:
	xorl	%eax, %eax
	cmpl	$0, 15360(%rsi)
	jne	.LBB2_67
# BB#66:
	movl	5756(%rbp), %eax
.LBB2_67:
	addl	%edx, %eax
	movl	%eax, 36(%rsi)
	cmpl	$3, %r8d
	jne	.LBB2_73
# BB#68:
	testl	%ebx, %ebx
	jle	.LBB2_70
# BB#69:
	leal	(%rbx,%rbx), %r8d
	movl	%ecx, %eax
	cltd
	idivl	%r8d
	cmpl	%ebx, %edx
	jge	.LBB2_71
.LBB2_70:
	movl	2140(%rbp), %eax
	movl	%eax, 36(%rsi)
	movl	2144(%rbp), %eax
	jmp	.LBB2_72
.LBB2_71:
	movl	4148(%rbp), %eax
	subl	16(%rbp), %eax
	movl	2140(%rbp), %ecx
	addl	%eax, %ecx
	movl	%ecx, 36(%rsi)
	addl	2144(%rbp), %eax
.LBB2_72:
	movl	%eax, 40(%rsi)
.LBB2_73:
	movl	112(%rsi), %eax
	movl	%eax, 116(%rsi)
	movl	68(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.LBB2_75
# BB#74:
	movl	(%rsi), %eax
	movl	68(%rsi), %ebp
	subl	%edi, %eax
	cltd
	idivl	%ecx
	movl	%ebp, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%ebp, %ecx
	sarl	$4, %ecx
	cltd
	idivl	%ecx
	movl	%edx, 112(%rsi)
.LBB2_75:                               # %init_frame.exit
	movl	15452(%rsi), %eax
	addl	36(%rsi), %eax
	movl	%eax, 44(%rsi)
	movl	15248(%rsi), %edi
	callq	UpdateSubseqInfo
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$-1, %ecx
	callq	UpdateSceneInformation
	movq	img(%rip), %rcx
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 15368(%rcx)
	movslq	14364(%rcx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_80
# BB#76:
	movq	input(%rip), %rax
	movl	(%rcx), %esi
	decl	%esi
	subl	start_frame_no_in_this_IGOP(%rip), %esi
	movl	20(%rax), %r14d
	incl	%r14d
	imull	%esi, %r14d
	addl	start_tr_in_this_IGOP(%rip), %r14d
	cmpl	$0, 2968(%rax)
	movsd	14352(%rcx), %xmm0      # xmm0 = mem[0],zero
	je	.LBB2_78
# BB#77:
	movq	gop_structure(%rip), %rsi
	leaq	(%rdx,%rdx,2), %rdx
	movl	-20(%rsi,%rdx,8), %edx
	incl	%edx
.LBB2_78:
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %edx
	addl	%edx, %r14d
.LBB2_79:
	movl	%r14d, frame_no(%rip)
	jmp	.LBB2_83
.LBB2_80:
	movl	(%rcx), %esi
	subl	start_frame_no_in_this_IGOP(%rip), %esi
	movq	input(%rip), %rax
	movl	20(%rax), %edx
	incl	%edx
	imull	%esi, %edx
	addl	start_tr_in_this_IGOP(%rip), %edx
	movl	%edx, frame_no(%rip)
	movl	4144(%rax), %r14d
	testl	%r14d, %r14d
	je	.LBB2_82
# BB#81:
	movl	(%rcx), %esi
	incl	%esi
	cmpl	8(%rax), %esi
	je	.LBB2_79
.LBB2_82:
	movl	%edx, %r14d
.LBB2_83:                               # %CalculateFrameNumber.exit
	movslq	276(%rax), %r15
	movl	56(%rax), %edi
	movl	60(%rax), %ebx
	movl	5268(%rax), %edx
	movl	5264(%rax), %esi
	movl	15440(%rcx), %ecx
	movl	%ecx, %ebp
	sarl	$31, %ebp
	shrl	$29, %ebp
	addl	%ecx, %ebp
	sarl	$3, %ebp
	movl	%ebx, %ecx
	imull	%edi, %ecx
	movl	%esi, 16(%rsp)          # 4-byte Spill
	movl	%esi, %r12d
	movl	%edx, 40(%rsp)          # 4-byte Spill
	imull	%edx, %r12d
	imull	%ebp, %ecx
	imull	%ebp, %r12d
	cmpl	$1, 5272(%rax)
	movl	%edi, 24(%rsp)          # 4-byte Spill
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	jne	.LBB2_85
# BB#84:
	cmpl	$3, 64(%rax)
	sete	%al
	movl	%eax, (%rsp)            # 4-byte Spill
	jmp	.LBB2_86
.LBB2_85:
	movl	$0, (%rsp)              # 4-byte Folded Spill
.LBB2_86:
	leal	(%rcx,%r12,2), %ebx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %edi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB2_88
# BB#87:
	movl	$.L.str.26, %edi
	callq	no_mem_exit
.LBB2_88:
	movslq	%ebx, %rbx
	movl	p_in(%rip), %edi
	xorl	%edx, %edx
	movq	%r15, %rsi
	callq	lseek64
	cmpq	%r15, %rax
	je	.LBB2_90
# BB#89:
	movl	$.L.str.27, %edi
	movl	$-1, %esi
	callq	error
.LBB2_90:
	movl	p_in(%rip), %edi
	movq	input(%rip), %rax
	movslq	1572(%rax), %rsi
	imulq	%rbx, %rsi
	movl	$1, %edx
	callq	lseek64
	cmpq	$-1, %rax
	jne	.LBB2_92
# BB#91:
	movq	input(%rip), %rax
	movl	1572(%rax), %ecx
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.28, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$-1, %esi
	callq	error
.LBB2_92:
	movl	p_in(%rip), %edi
	movslq	%r14d, %rsi
	imulq	%rbx, %rsi
	movl	$1, %edx
	callq	lseek64
	cmpq	$-1, %rax
	jne	.LBB2_94
# BB#93:
	movq	input(%rip), %rax
	addl	1572(%rax), %r14d
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.28, %edx
	xorl	%eax, %eax
	movl	%r14d, %ecx
	callq	snprintf
	movl	$errortext, %edi
	movl	$-1, %esi
	callq	error
.LBB2_94:
	movq	img(%rip), %rax
	testb	$7, 15440(%rax)
	jne	.LBB2_506
# BB#95:
	cmpb	$0, (%rsp)              # 1-byte Folded Reload
	je	.LBB2_97
# BB#96:
	movl	p_in(%rip), %edi
	movabsq	$6148914691236517206, %rcx # imm = 0x5555555555555556
	movq	%rbx, %rax
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	leaq	(%rax,%rdx), %rsi
	movl	$1, %edx
	callq	lseek64
.LBB2_97:
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movl	p_in(%rip), %edi
	movslq	8(%rsp), %rbx           # 4-byte Folded Reload
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	read
	cmpq	%rbx, %rax
	jne	.LBB2_505
# BB#98:
	movq	imgY_org_frm(%rip), %rdi
	movq	%r13, %rsi
	movl	24(%rsp), %edx          # 4-byte Reload
	movl	32(%rsp), %ecx          # 4-byte Reload
	movl	%ebp, %r8d
	callq	buf2img
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB2_105
# BB#99:
	movl	p_in(%rip), %edi
	movslq	%r12d, %rbx
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	read
	cmpq	%rbx, %rax
	jne	.LBB2_505
# BB#100:
	movq	imgUV_org_frm(%rip), %rax
	movq	(%rax), %rdi
	movq	%r13, %rsi
	movl	40(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %edx
	movl	16(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %ecx
	movl	%ebp, %r8d
	callq	buf2img
	cmpb	$0, (%rsp)              # 1-byte Folded Reload
	je	.LBB2_102
# BB#101:
	movl	p_in(%rip), %edi
	movq	64(%rsp), %rsi          # 8-byte Reload
	negq	%rsi
	movl	$1, %edx
	callq	lseek64
.LBB2_102:
	movl	p_in(%rip), %edi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	read
	cmpq	%rbx, %rax
	jne	.LBB2_505
# BB#103:
	movq	imgUV_org_frm(%rip), %rax
	movq	8(%rax), %rdi
	movq	%r13, %rsi
	movl	%r14d, %edx
	movl	%r15d, %ecx
	movl	%ebp, %r8d
	callq	buf2img
	cmpb	$0, (%rsp)              # 1-byte Folded Reload
	je	.LBB2_105
# BB#104:
	movl	p_in(%rip), %edi
	movq	64(%rsp), %rax          # 8-byte Reload
	addq	%rax, %rax
	movabsq	$6148914691236517206, %rcx # imm = 0x5555555555555556
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	leaq	(%rax,%rdx), %rsi
	movl	$1, %edx
	callq	lseek64
.LBB2_105:                              # %ReadOneFrame.exit
	movq	%r13, %rdi
	callq	free
	movq	input(%rip), %rax
	movl	60(%rax), %r11d
	movq	img(%rip), %rcx
	movslq	52(%rcx), %rdx
	movq	%rdx, %r12
	movl	%edx, %r10d
	movslq	68(%rcx), %r15
	movslq	5268(%rax), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	5264(%rax), %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	64(%rcx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movslq	80(%rcx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testl	%r11d, %r11d
	jle	.LBB2_115
# BB#106:                               # %.preheader64.lr.ph.i
	movq	72(%rsp), %rax          # 8-byte Reload
	movslq	56(%rax), %rcx
	cmpl	%r10d, %ecx
	jge	.LBB2_115
# BB#107:                               # %.preheader64.us.preheader.i
	movq	imgY_org_frm(%rip), %r13
	movq	%r12, %rax
	movl	%eax, %edi
	subl	%ecx, %edi
	leaq	-1(%rax), %r14
	subq	%rcx, %r14
	andl	$7, %edi
	movq	%rdi, %r9
	negq	%r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_108:                              # %.preheader64.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_110 Depth 2
                                        #     Child Loop BB2_113 Depth 2
	testq	%rdi, %rdi
	movq	(%r13,%rbp,8), %rsi
	movzwl	-2(%rsi,%rcx,2), %edx
	movq	%rcx, %rax
	je	.LBB2_111
# BB#109:                               # %.prol.preheader387
                                        #   in Loop: Header=BB2_108 Depth=1
	movq	%r9, %rbx
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB2_110:                              #   Parent Loop BB2_108 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%dx, (%rsi,%rax,2)
	incq	%rax
	incq	%rbx
	jne	.LBB2_110
.LBB2_111:                              # %.prol.loopexit388
                                        #   in Loop: Header=BB2_108 Depth=1
	cmpq	$7, %r14
	jb	.LBB2_114
# BB#112:                               # %.preheader64.us.i.new
                                        #   in Loop: Header=BB2_108 Depth=1
	movq	%r12, %r8
	subq	%rax, %r8
	leaq	14(%rsi,%rax,2), %rax
	.p2align	4, 0x90
.LBB2_113:                              #   Parent Loop BB2_108 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%dx, -14(%rax)
	movw	%dx, -12(%rax)
	movw	%dx, -10(%rax)
	movw	%dx, -8(%rax)
	movw	%dx, -6(%rax)
	movw	%dx, -4(%rax)
	movw	%dx, -2(%rax)
	movw	%dx, (%rax)
	addq	$16, %rax
	addq	$-8, %r8
	jne	.LBB2_113
.LBB2_114:                              # %._crit_edge78.us.i
                                        #   in Loop: Header=BB2_108 Depth=1
	incq	%rbp
	cmpq	%r11, %rbp
	jne	.LBB2_108
.LBB2_115:                              # %.preheader63.i
	cmpl	%r15d, %r11d
	jge	.LBB2_135
# BB#116:                               # %.preheader62.lr.ph.i
	testl	%r12d, %r12d
	jle	.LBB2_135
# BB#117:                               # %.preheader62.us.preheader.i
	movq	imgY_org_frm(%rip), %r8
	movslq	%r11d, %rdx
	leaq	-1(%r10), %r11
	andl	$15, %r12d
	movq	%r10, %r9
	movq	%r12, %r14
	subq	%r12, %r9
	.p2align	4, 0x90
.LBB2_118:                              # %.preheader62.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_126 Depth 2
                                        #     Child Loop BB2_130 Depth 2
                                        #     Child Loop BB2_133 Depth 2
	cmpl	$16, %r10d
	movq	-8(%r8,%rdx,8), %rbp
	movq	(%r8,%rdx,8), %rax
	jae	.LBB2_120
# BB#119:                               #   in Loop: Header=BB2_118 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB2_128
	.p2align	4, 0x90
.LBB2_120:                              # %min.iters.checked
                                        #   in Loop: Header=BB2_118 Depth=1
	testq	%r9, %r9
	je	.LBB2_124
# BB#121:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_118 Depth=1
	leaq	(%rbp,%r10,2), %rsi
	cmpq	%rsi, %rax
	jae	.LBB2_125
# BB#122:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_118 Depth=1
	leaq	(%rax,%r10,2), %rsi
	cmpq	%rsi, %rbp
	jae	.LBB2_125
.LBB2_124:                              #   in Loop: Header=BB2_118 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB2_128
.LBB2_125:                              # %vector.body.preheader
                                        #   in Loop: Header=BB2_118 Depth=1
	leaq	16(%rbp), %rsi
	leaq	16(%rax), %rdi
	movq	%r9, %rbx
	.p2align	4, 0x90
.LBB2_126:                              # %vector.body
                                        #   Parent Loop BB2_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rsi), %xmm0
	movupd	(%rsi), %xmm1
	movupd	%xmm0, -16(%rdi)
	movupd	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-16, %rbx
	jne	.LBB2_126
# BB#127:                               # %middle.block
                                        #   in Loop: Header=BB2_118 Depth=1
	testl	%r14d, %r14d
	movq	%r9, %rbx
	je	.LBB2_134
	.p2align	4, 0x90
.LBB2_128:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_118 Depth=1
	movl	%r10d, %edi
	subl	%ebx, %edi
	movq	%r11, %rsi
	subq	%rbx, %rsi
	andq	$7, %rdi
	je	.LBB2_131
# BB#129:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB2_118 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB2_130:                              # %scalar.ph.prol
                                        #   Parent Loop BB2_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbp,%rbx,2), %ecx
	movw	%cx, (%rax,%rbx,2)
	incq	%rbx
	incq	%rdi
	jne	.LBB2_130
.LBB2_131:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB2_118 Depth=1
	cmpq	$7, %rsi
	jb	.LBB2_134
# BB#132:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB2_118 Depth=1
	movq	%r10, %rsi
	subq	%rbx, %rsi
	leaq	14(%rax,%rbx,2), %rax
	leaq	14(%rbp,%rbx,2), %rbp
	.p2align	4, 0x90
.LBB2_133:                              # %scalar.ph
                                        #   Parent Loop BB2_118 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-14(%rbp), %ecx
	movw	%cx, -14(%rax)
	movzwl	-12(%rbp), %ecx
	movw	%cx, -12(%rax)
	movzwl	-10(%rbp), %ecx
	movw	%cx, -10(%rax)
	movzwl	-8(%rbp), %ecx
	movw	%cx, -8(%rax)
	movzwl	-6(%rbp), %ecx
	movw	%cx, -6(%rax)
	movzwl	-4(%rbp), %ecx
	movw	%cx, -4(%rax)
	movzwl	-2(%rbp), %ecx
	movw	%cx, -2(%rax)
	movzwl	(%rbp), %ecx
	movw	%cx, (%rax)
	addq	$16, %rax
	addq	$16, %rbp
	addq	$-8, %rsi
	jne	.LBB2_133
.LBB2_134:                              # %._crit_edge73.us.i
                                        #   in Loop: Header=BB2_118 Depth=1
	incq	%rdx
	cmpq	%r15, %rdx
	jne	.LBB2_118
.LBB2_135:                              # %._crit_edge75.i
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 15536(%rax)
	movq	8(%rsp), %r14           # 8-byte Reload
	je	.LBB2_178
# BB#136:                               # %.preheader61.i
	movl	%r14d, %r10d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	jle	.LBB2_154
# BB#137:                               # %.preheader60.lr.ph.i
	cmpl	%r10d, %ecx
	jge	.LBB2_154
# BB#138:                               # %.preheader60.us.preheader.i
	movq	imgUV_org_frm(%rip), %rax
	movq	(%rax), %r15
	movq	8(%rax), %r12
	leaq	-1(%rcx), %rdx
	movq	%r14, %rax
	subq	%rcx, %rax
	leaq	-1(%r14), %rdi
	subq	%rcx, %rdi
	movl	%eax, %esi
	andl	$3, %esi
	andl	$7, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	negq	%rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	negq	%rsi
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_139:                              # %.lver.check
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_149 Depth 2
                                        #     Child Loop BB2_152 Depth 2
                                        #     Child Loop BB2_143 Depth 2
                                        #     Child Loop BB2_146 Depth 2
	movq	(%r15,%r8,8), %rbp
	movq	(%r12,%r8,8), %r13
	leaq	(%rbp,%rdx,2), %rax
	leaq	(%r13,%r14,2), %rsi
	cmpq	%rsi, %rax
	jae	.LBB2_147
# BB#140:                               # %.lver.check
                                        #   in Loop: Header=BB2_139 Depth=1
	leaq	(%rbp,%r14,2), %rax
	leaq	(%r13,%rdx,2), %rsi
	cmpq	%rax, %rsi
	jae	.LBB2_147
# BB#141:                               # %.ph.lver.orig.preheader
                                        #   in Loop: Header=BB2_139 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	movq	%rcx, %rax
	je	.LBB2_144
# BB#142:                               # %.ph.lver.orig.prol.preheader
                                        #   in Loop: Header=BB2_139 Depth=1
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB2_143:                              # %.ph.lver.orig.prol
                                        #   Parent Loop BB2_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%rbp,%rax,2), %ebx
	movw	%bx, (%rbp,%rax,2)
	movzwl	-2(%r13,%rax,2), %ebx
	movw	%bx, (%r13,%rax,2)
	incq	%rax
	incq	%rsi
	jne	.LBB2_143
.LBB2_144:                              # %.ph.lver.orig.prol.loopexit
                                        #   in Loop: Header=BB2_139 Depth=1
	cmpq	$3, %rdi
	jb	.LBB2_153
# BB#145:                               # %.ph.lver.orig.preheader.new
                                        #   in Loop: Header=BB2_139 Depth=1
	movq	%r14, %rbx
	subq	%rax, %rbx
	leaq	(%r13,%rax,2), %rsi
	leaq	(%rbp,%rax,2), %rbp
	.p2align	4, 0x90
.LBB2_146:                              # %.ph.lver.orig
                                        #   Parent Loop BB2_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%rbp), %eax
	movw	%ax, (%rbp)
	movzwl	-2(%rsi), %eax
	movw	%ax, (%rsi)
	movzwl	(%rbp), %eax
	movw	%ax, 2(%rbp)
	movzwl	(%rsi), %eax
	movw	%ax, 2(%rsi)
	movzwl	2(%rbp), %eax
	movw	%ax, 4(%rbp)
	movzwl	2(%rsi), %eax
	movw	%ax, 4(%rsi)
	movzwl	4(%rbp), %eax
	movw	%ax, 6(%rbp)
	movzwl	4(%rsi), %eax
	movw	%ax, 6(%rsi)
	addq	$8, %rsi
	addq	$8, %rbp
	addq	$-4, %rbx
	jne	.LBB2_146
	jmp	.LBB2_153
	.p2align	4, 0x90
.LBB2_147:                              # %.ph
                                        #   in Loop: Header=BB2_139 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	movzwl	-2(%rbp,%rcx,2), %r9d
	movzwl	-2(%r13,%rcx,2), %r11d
	movq	%rcx, %rax
	je	.LBB2_150
# BB#148:                               # %.prol.preheader376
                                        #   in Loop: Header=BB2_139 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB2_149:                              #   Parent Loop BB2_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%r9w, (%rbp,%rax,2)
	movw	%r11w, (%r13,%rax,2)
	incq	%rax
	incq	%rsi
	jne	.LBB2_149
.LBB2_150:                              # %.prol.loopexit377
                                        #   in Loop: Header=BB2_139 Depth=1
	cmpq	$7, %rdi
	jb	.LBB2_153
# BB#151:                               # %.ph.new
                                        #   in Loop: Header=BB2_139 Depth=1
	movq	%r14, %rbx
	subq	%rax, %rbx
	leaq	14(%r13,%rax,2), %rsi
	leaq	14(%rbp,%rax,2), %rbp
	.p2align	4, 0x90
.LBB2_152:                              #   Parent Loop BB2_139 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%r9w, -14(%rbp)
	movw	%r11w, -14(%rsi)
	movw	%r9w, -12(%rbp)
	movw	%r11w, -12(%rsi)
	movw	%r9w, -10(%rbp)
	movw	%r11w, -10(%rsi)
	movw	%r9w, -8(%rbp)
	movw	%r11w, -8(%rsi)
	movw	%r9w, -6(%rbp)
	movw	%r11w, -6(%rsi)
	movw	%r9w, -4(%rbp)
	movw	%r11w, -4(%rsi)
	movw	%r9w, -2(%rbp)
	movw	%r11w, -2(%rsi)
	movw	%r9w, (%rbp)
	movw	%r11w, (%rsi)
	addq	$16, %rsi
	addq	$16, %rbp
	addq	$-8, %rbx
	jne	.LBB2_152
.LBB2_153:                              # %._crit_edge69.us.i
                                        #   in Loop: Header=BB2_139 Depth=1
	incq	%r8
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	jne	.LBB2_139
.LBB2_154:                              # %.preheader59.i
	movq	40(%rsp), %r8           # 8-byte Reload
	cmpl	%r8d, (%rsp)            # 4-byte Folded Reload
	jge	.LBB2_178
# BB#155:                               # %.preheader.lr.ph.i
	testl	%r14d, %r14d
	jle	.LBB2_178
# BB#156:                               # %.preheader.us.preheader.i
	movq	imgUV_org_frm(%rip), %rax
	movslq	(%rsp), %r9             # 4-byte Folded Reload
	movq	(%rax), %r15
	movq	8(%rax), %r12
	leaq	-1(%r10), %r11
	andl	$15, %r14d
	movq	%r10, %rax
	subq	%r14, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	jmp	.LBB2_160
.LBB2_157:                              #   in Loop: Header=BB2_160 Depth=1
	xorl	%edi, %edi
	movq	16(%rsp), %r11          # 8-byte Reload
	jmp	.LBB2_172
.LBB2_158:                              #   in Loop: Header=BB2_160 Depth=1
	xorl	%edi, %edi
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	jmp	.LBB2_172
.LBB2_159:                              #   in Loop: Header=BB2_160 Depth=1
	xorl	%edi, %edi
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	jmp	.LBB2_172
	.p2align	4, 0x90
.LBB2_160:                              # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_169 Depth 2
                                        #     Child Loop BB2_174 Depth 2
                                        #     Child Loop BB2_176 Depth 2
	cmpl	$16, %r10d
	movq	-8(%r15,%r9,8), %r13
	movq	(%r15,%r9,8), %rdx
	movq	-8(%r12,%r9,8), %rbp
	movq	(%r12,%r9,8), %rsi
	jb	.LBB2_171
# BB#162:                               # %min.iters.checked280
                                        #   in Loop: Header=BB2_160 Depth=1
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB2_171
# BB#163:                               # %vector.memcheck312
                                        #   in Loop: Header=BB2_160 Depth=1
	leaq	(%rdx,%r10,2), %rcx
	leaq	(%rsi,%r10,2), %rdi
	leaq	(%r13,%r10,2), %r11
	leaq	(%rbp,%r10,2), %r8
	cmpq	%rdi, %rdx
	sbbb	%al, %al
	cmpq	%rcx, %rsi
	sbbb	%r14b, %r14b
	andb	%al, %r14b
	cmpq	%r11, %rdx
	sbbb	%al, %al
	cmpq	%rcx, %r13
	sbbb	%r12b, %r12b
	cmpq	%r8, %rdx
	sbbb	%bl, %bl
	cmpq	%rcx, %rbp
	sbbb	%cl, %cl
	movb	%cl, 24(%rsp)           # 1-byte Spill
	cmpq	%r11, %rsi
	sbbb	%r15b, %r15b
	cmpq	%rdi, %r13
	sbbb	%cl, %cl
	movb	%cl, 32(%rsp)           # 1-byte Spill
	cmpq	%r8, %rsi
	sbbb	%r11b, %r11b
	cmpq	%rdi, %rbp
	sbbb	%dil, %dil
	testb	$1, %r14b
	jne	.LBB2_158
# BB#164:                               # %vector.memcheck312
                                        #   in Loop: Header=BB2_160 Depth=1
	andb	%r12b, %al
	andb	$1, %al
	movq	8(%rsp), %r14           # 8-byte Reload
	jne	.LBB2_158
# BB#165:                               # %vector.memcheck312
                                        #   in Loop: Header=BB2_160 Depth=1
	andb	24(%rsp), %bl           # 1-byte Folded Reload
	andb	$1, %bl
	movq	48(%rsp), %r12          # 8-byte Reload
	jne	.LBB2_159
# BB#166:                               # %vector.memcheck312
                                        #   in Loop: Header=BB2_160 Depth=1
	andb	32(%rsp), %r15b         # 1-byte Folded Reload
	andb	$1, %r15b
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	jne	.LBB2_157
# BB#167:                               # %vector.memcheck312
                                        #   in Loop: Header=BB2_160 Depth=1
	andb	%dil, %r11b
	andb	$1, %r11b
	movl	$0, %edi
	movq	16(%rsp), %r11          # 8-byte Reload
	jne	.LBB2_172
# BB#168:                               # %vector.body276.preheader
                                        #   in Loop: Header=BB2_160 Depth=1
	leaq	16(%r13), %rdi
	leaq	16(%rdx), %rbx
	leaq	16(%rbp), %rax
	leaq	16(%rsi), %rcx
	movq	(%rsp), %r11            # 8-byte Reload
	.p2align	4, 0x90
.LBB2_169:                              # %vector.body276
                                        #   Parent Loop BB2_160 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movupd	%xmm0, -16(%rcx)
	movupd	%xmm1, (%rcx)
	addq	$32, %rdi
	addq	$32, %rbx
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-16, %r11
	jne	.LBB2_169
# BB#170:                               # %middle.block277
                                        #   in Loop: Header=BB2_160 Depth=1
	testl	%r14d, %r14d
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	jne	.LBB2_172
	jmp	.LBB2_177
	.p2align	4, 0x90
.LBB2_171:                              #   in Loop: Header=BB2_160 Depth=1
	xorl	%edi, %edi
.LBB2_172:                              # %scalar.ph278.preheader
                                        #   in Loop: Header=BB2_160 Depth=1
	movl	%r10d, %ecx
	subl	%edi, %ecx
	movq	%r11, %rax
	subq	%rdi, %rax
	andq	$3, %rcx
	je	.LBB2_175
# BB#173:                               # %scalar.ph278.prol.preheader
                                        #   in Loop: Header=BB2_160 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB2_174:                              # %scalar.ph278.prol
                                        #   Parent Loop BB2_160 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r13,%rdi,2), %ebx
	movw	%bx, (%rdx,%rdi,2)
	movzwl	(%rbp,%rdi,2), %ebx
	movw	%bx, (%rsi,%rdi,2)
	incq	%rdi
	incq	%rcx
	jne	.LBB2_174
.LBB2_175:                              # %scalar.ph278.prol.loopexit
                                        #   in Loop: Header=BB2_160 Depth=1
	cmpq	$3, %rax
	jb	.LBB2_177
	.p2align	4, 0x90
.LBB2_176:                              # %scalar.ph278
                                        #   Parent Loop BB2_160 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r13,%rdi,2), %eax
	movw	%ax, (%rdx,%rdi,2)
	movzwl	(%rbp,%rdi,2), %eax
	movw	%ax, (%rsi,%rdi,2)
	movzwl	2(%r13,%rdi,2), %eax
	movw	%ax, 2(%rdx,%rdi,2)
	movzwl	2(%rbp,%rdi,2), %eax
	movw	%ax, 2(%rsi,%rdi,2)
	movzwl	4(%r13,%rdi,2), %eax
	movw	%ax, 4(%rdx,%rdi,2)
	movzwl	4(%rbp,%rdi,2), %eax
	movw	%ax, 4(%rsi,%rdi,2)
	movzwl	6(%r13,%rdi,2), %eax
	movw	%ax, 6(%rdx,%rdi,2)
	movzwl	6(%rbp,%rdi,2), %eax
	movw	%ax, 6(%rsi,%rdi,2)
	addq	$4, %rdi
	cmpq	%rdi, %r10
	jne	.LBB2_176
.LBB2_177:                              # %._crit_edge.us.i
                                        #   in Loop: Header=BB2_160 Depth=1
	incq	%r9
	cmpq	%r8, %r9
	jne	.LBB2_160
.LBB2_178:                              # %PaddAutoCropBorders.exit
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	2112(%rsi), %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 14452(%rcx)
	movl	4752(%rsi), %eax
	movl	%eax, 14440(%rcx)
	movl	4756(%rsi), %eax
	movl	%eax, 14444(%rcx)
	movl	4760(%rsi), %eax
	movl	%eax, 14448(%rcx)
	movl	5652(%rsi), %eax
	movl	%eax, 15260(%rcx)
	movslq	20(%rcx), %rax
	incl	frame_ctr(,%rax,4)
	movq	snr(%rip), %rax
	incl	120(%rax)
	cmpl	$3, 20(%rcx)
	jne	.LBB2_181
# BB#179:
	cmpl	$0, 2152(%rsi)
	je	.LBB2_182
# BB#180:
	movl	$1, sp2_frame_indicator(%rip)
	callq	read_SP_coefficients
	movq	input(%rip), %rsi
	jmp	.LBB2_182
.LBB2_181:
	movl	$0, sp2_frame_indicator(%rip)
.LBB2_182:
	movl	5116(%rsi), %eax
	cmpl	$1, 4704(%rsi)
	jne	.LBB2_186
# BB#183:
	testl	%eax, %eax
	je	.LBB2_185
# BB#184:
	movq	generic_RC(%rip), %rax
	movl	$1, 4(%rax)
.LBB2_185:
	movq	img(%rip), %rax
	movl	$1, 15312(%rax)
	movq	top_pic(%rip), %rdi
	movq	bottom_pic(%rip), %rsi
	callq	field_picture
	movq	img(%rip), %rax
	movl	$1, 14256(%rax)
	movl	$1, %edx
	jmp	.LBB2_283
.LBB2_186:
	testl	%eax, %eax
	je	.LBB2_188
# BB#187:
	movq	generic_RC(%rip), %rcx
	movl	$0, 4(%rcx)
.LBB2_188:
	movl	4708(%rsi), %ecx
	testl	%ecx, %ecx
	je	.LBB2_190
# BB#189:
	movl	$1, mb_adaptive(%rip)
.LBB2_190:
	testl	%eax, %eax
	movq	img(%rip), %rax
	movl	$0, 15312(%rax)
	je	.LBB2_200
# BB#191:
	testl	%ecx, %ecx
	movl	5128(%rsi), %edx
	je	.LBB2_196
# BB#192:
	cmpl	15352(%rax), %edx
	jae	.LBB2_196
# BB#193:
	cmpl	$0, 20(%rax)
	je	.LBB2_195
# BB#194:
	cmpl	$1, 5136(%rsi)
	jne	.LBB2_196
.LBB2_195:
	movl	(%rax), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	setne	%cl
	shll	%cl, %edx
.LBB2_196:                              # %._crit_edge
	movl	%edx, 15404(%rax)
	cmpl	$0, 2940(%rsi)
	je	.LBB2_198
# BB#197:
	movq	quadratic_RC_init(%rip), %rdi
	movq	quadratic_RC(%rip), %rsi
	callq	copy_rc_jvt
	movq	generic_RC_init(%rip), %rdi
	movq	generic_RC(%rip), %rsi
	callq	copy_rc_generic
.LBB2_198:
	movq	quadratic_RC(%rip), %rdi
	movss	.LCPI2_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movl	$1, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	callq	rc_init_pict
	movq	quadratic_RC(%rip), %rdi
	xorl	%esi, %esi
	callq	*updateQP(%rip)
	movq	img(%rip), %rcx
	movl	%eax, 36(%rcx)
	movl	$0, QP(%rip)
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	je	.LBB2_200
# BB#199:
	movq	generic_RC(%rip), %rax
	movl	$0, (%rax)
.LBB2_200:
	movq	input(%rip), %rax
	cmpl	$0, 1576(%rax)
	je	.LBB2_202
# BB#201:
	movq	PicParSet(%rip), %rax
	movq	%rax, active_pps(%rip)
.LBB2_202:
	movq	frame_pic_1(%rip), %rdi
	xorl	%esi, %esi
	callq	frame_picture
	movq	input(%rip), %rax
	cmpl	$0, 2944(%rax)
	jne	.LBB2_204
# BB#203:
	movq	img(%rip), %rcx
	cmpl	$2, 20(%rcx)
	je	.LBB2_265
.LBB2_204:
	cmpl	$0, 2940(%rax)
	movq	img(%rip), %rcx
	je	.LBB2_265
# BB#205:
	movl	20(%rcx), %ebp
	movl	36(%rcx), %ebx
	movl	intras(%rip), %r15d
	movl	15608(%rcx), %r14d
	cmpl	$0, 5116(%rax)
	movl	%ebp, %eax
	je	.LBB2_207
# BB#206:
	movq	quadratic_RC_best(%rip), %rdi
	movq	quadratic_RC(%rip), %rsi
	callq	copy_rc_jvt
	movq	generic_RC_best(%rip), %rdi
	movq	generic_RC(%rip), %rsi
	callq	copy_rc_generic
	movq	img(%rip), %rcx
	movl	20(%rcx), %eax
.LBB2_207:
	cmpl	$2, %eax
	je	.LBB2_211
# BB#208:
	movq	input(%rip), %rdx
	cmpl	$0, 1576(%rdx)
	je	.LBB2_211
# BB#209:
	testl	%eax, %eax
	je	.LBB2_235
# BB#210:
	movq	PicParSet+16(%rip), %rax
	movq	%rax, active_pps(%rip)
	jmp	.LBB2_213
.LBB2_211:
	cmpl	$0, 15260(%rcx)
	jne	.LBB2_213
# BB#212:
	decl	36(%rcx)
.LBB2_213:                              # %.thread51.i
	movq	active_pps(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	36(%rcx), %r12d
	movl	$0, 15408(%rcx)
	movq	input(%rip), %rax
.LBB2_214:
	cmpl	$0, 5116(%rax)
	je	.LBB2_216
# BB#215:
	movq	quadratic_RC(%rip), %rdi
	movq	quadratic_RC_init(%rip), %rsi
	callq	copy_rc_jvt
	movq	generic_RC(%rip), %rdi
	movq	generic_RC_init(%rip), %rsi
	callq	copy_rc_generic
	movq	quadratic_RC(%rip), %rdi
	movq	input(%rip), %rax
	xorl	%ecx, %ecx
	cmpl	$0, 1576(%rax)
	setne	%cl
	movss	.LCPI2_2(,%rcx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movl	$1, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	callq	rc_init_pict
	movq	quadratic_RC(%rip), %rdi
	xorl	%esi, %esi
	callq	*updateQP(%rip)
	movq	img(%rip), %rcx
	movl	%eax, 36(%rcx)
.LBB2_216:
	movq	frame_pic_2(%rip), %rdi
	movl	$1, %esi
	callq	frame_picture
	movq	frame_pic_1(%rip), %rdi
	movq	frame_pic_2(%rip), %rsi
	movl	%ebx, %edx
	callq	picture_coding_decision
	movq	img(%rip), %rcx
	movl	%eax, 14260(%rcx)
	testl	%eax, %eax
	je	.LBB2_219
# BB#217:
	movl	intras(%rip), %r15d
	movq	frame_pic_2(%rip), %rax
	movq	%rax, frame_pic(%rip)
	movl	15608(%rcx), %r14d
	movq	input(%rip), %rax
	xorl	%r13d, %r13d
	cmpl	$0, 5116(%rax)
	je	.LBB2_225
# BB#218:
	movq	quadratic_RC_best(%rip), %rdi
	movq	quadratic_RC(%rip), %rsi
	callq	copy_rc_jvt
	movq	generic_RC_best(%rip), %rdi
	movq	generic_RC(%rip), %rsi
	callq	copy_rc_generic
	movq	img(%rip), %rcx
	jmp	.LBB2_225
.LBB2_219:
	xorl	%r13d, %r13d
.LBB2_220:
	movq	enc_frame_picture(%rip), %rax
	movq	%rax, enc_picture(%rip)
	cmpl	$2, 20(%rcx)
	je	.LBB2_223
# BB#221:
	movq	input(%rip), %rax
	cmpl	$0, 1576(%rax)
	je	.LBB2_223
# BB#222:
	movl	%ebx, 36(%rcx)
	movq	PicParSet(%rip), %rax
	movq	%rax, active_pps(%rip)
	jmp	.LBB2_224
.LBB2_223:
	movl	%ebx, 36(%rcx)
.LBB2_224:
	movl	%r15d, intras(%rip)
	movq	frame_pic_1(%rip), %rax
	movq	%rax, frame_pic(%rip)
.LBB2_225:
	movl	20(%rcx), %eax
	cmpl	$2, %eax
	movl	%ebp, (%rsp)            # 4-byte Spill
	jne	.LBB2_228
# BB#226:
	movq	PicParSet(%rip), %rax
	movq	%rax, active_pps(%rip)
	cmpl	$0, 15260(%rcx)
	jne	.LBB2_240
# BB#227:
	leal	1(%rbx), %eax
	jmp	.LBB2_239
.LBB2_228:
	movl	%ebx, 36(%rcx)
	testl	%eax, %eax
	je	.LBB2_232
# BB#229:
	movq	input(%rip), %rax
	cmpl	$0, 1576(%rax)
	movl	%r12d, %ebp
	je	.LBB2_238
# BB#230:
	xorl	%edi, %edi
	callq	test_wp_B_slice
	cmpl	$1, %eax
	je	.LBB2_244
# BB#237:                               # %._crit_edge.i139
	movq	input(%rip), %rax
	movq	img(%rip), %rcx
.LBB2_238:
	movl	2956(%rax), %r13d
	cmpl	$1, 15360(%rcx)
	sbbl	%eax, %eax
	notl	%eax
	orl	$1, %eax
	addl	%ebx, %eax
.LBB2_239:
	movl	%eax, 36(%rcx)
.LBB2_240:
	movl	$0, 15408(%rcx)
	testl	%r13d, %r13d
	movl	%r12d, %ebp
	je	.LBB2_247
# BB#241:
	movq	$0, enc_frame_picture3(%rip)
	movl	%ebx, 36(%rcx)
	movl	14260(%rcx), %eax
	cmpl	$1, %eax
	jne	.LBB2_258
	jmp	.LBB2_260
.LBB2_232:
	imull	$100, intras(%rip), %eax
	xorl	%edx, %edx
	divl	15352(%rcx)
	cmpl	$75, %eax
	movl	%r12d, %ebp
	jb	.LBB2_242
# BB#233:
	movl	$2, 20(%rcx)
.LBB2_234:                              # %.thread.sink.split.i
	movl	$PicParSet, %eax
	jmp	.LBB2_245
.LBB2_235:
	xorl	%edi, %edi
	callq	test_wp_P_slice
	cmpl	$1, %eax
	jne	.LBB2_494
# BB#236:
	movq	PicParSet+8(%rip), %rax
	movq	%rax, active_pps(%rip)
	movq	img(%rip), %rcx
	jmp	.LBB2_213
.LBB2_242:
	movq	input(%rip), %rax
	cmpl	$0, 1576(%rax)
	je	.LBB2_246
# BB#243:
	movl	$1, %edi
	callq	test_wp_P_slice
	cmpl	$1, %eax
	jne	.LBB2_497
.LBB2_244:
	movl	$PicParSet+8, %eax
.LBB2_245:                              # %.thread.sink.split.i
	movq	(%rax), %rax
	movq	%rax, active_pps(%rip)
	movq	img(%rip), %rcx
.LBB2_246:                              # %.thread.i140
	movl	$0, 15408(%rcx)
.LBB2_247:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB2_249
# BB#248:
	movq	quadratic_RC(%rip), %rdi
	movq	quadratic_RC_init(%rip), %rsi
	callq	copy_rc_jvt
	movq	generic_RC(%rip), %rdi
	movq	generic_RC_init(%rip), %rsi
	callq	copy_rc_generic
	movq	quadratic_RC(%rip), %rdi
	movq	input(%rip), %rax
	xorl	%ecx, %ecx
	cmpl	$0, 1576(%rax)
	setne	%cl
	movss	.LCPI2_3(,%rcx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movl	$1, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	callq	rc_init_pict
	movq	quadratic_RC(%rip), %rdi
	xorl	%esi, %esi
	callq	*updateQP(%rip)
	movq	img(%rip), %rcx
	movl	%eax, 36(%rcx)
.LBB2_249:
	movq	frame_pic_3(%rip), %rdi
	movl	$2, %esi
	callq	frame_picture
	movq	img(%rip), %rax
	cmpl	$0, 14260(%rax)
	je	.LBB2_251
# BB#250:
	movq	frame_pic_2(%rip), %rdi
	movq	frame_pic_3(%rip), %rsi
	movl	%ebx, %edx
	callq	picture_coding_decision
	movq	img(%rip), %rcx
	addl	14260(%rcx), %eax
	jmp	.LBB2_252
.LBB2_251:
	movq	frame_pic_1(%rip), %rdi
	movq	frame_pic_3(%rip), %rsi
	movl	%ebx, %edx
	callq	picture_coding_decision
	addl	%eax, %eax
	movq	img(%rip), %rcx
.LBB2_252:
	movl	%eax, 14260(%rcx)
	movq	input(%rip), %rdx
	cmpl	$0, 5116(%rdx)
	je	.LBB2_255
# BB#253:
	cmpl	$2, %eax
	jne	.LBB2_257
# BB#254:
	movq	quadratic_RC_best(%rip), %rdi
	movq	quadratic_RC(%rip), %rsi
	callq	copy_rc_jvt
	movq	generic_RC_best(%rip), %rdi
	movq	generic_RC(%rip), %rsi
	callq	copy_rc_generic
	movq	img(%rip), %rcx
	movl	14260(%rcx), %eax
.LBB2_255:
	cmpl	$2, %eax
	jne	.LBB2_257
# BB#256:                               # %.thread60.i
	movl	15608(%rcx), %r14d
	jmp	.LBB2_262
.LBB2_257:                              # %.thread59.i
	cmpl	$1, %eax
	je	.LBB2_260
.LBB2_258:                              # %.thread59.i
	testl	%eax, %eax
	jne	.LBB2_262
# BB#259:
	movq	enc_frame_picture(%rip), %rax
	movq	%rax, enc_picture(%rip)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, 20(%rcx)
	movq	PicParSet(%rip), %rax
	jmp	.LBB2_261
.LBB2_260:
	movq	enc_frame_picture2(%rip), %rax
	movq	%rax, enc_picture(%rip)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, 20(%rcx)
	movl	%ebp, %ebx
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB2_261:                              # %.sink.split.i
	movq	%rax, active_pps(%rip)
	movl	%ebx, 36(%rcx)
	movl	%r15d, intras(%rip)
.LBB2_262:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB2_264
# BB#263:
	movq	quadratic_RC(%rip), %rdi
	movq	quadratic_RC_best(%rip), %rsi
	callq	copy_rc_jvt
	movq	generic_RC(%rip), %rdi
	movq	generic_RC_best(%rip), %rsi
	callq	copy_rc_generic
	movq	img(%rip), %rcx
.LBB2_264:                              # %rdPictureCoding.exit
	movl	%r14d, 15608(%rcx)
.LBB2_265:                              # %._crit_edge226
	movl	15608(%rcx), %r14d
	movl	20(%rcx), %eax
	cmpl	$3, %eax
	jne	.LBB2_269
# BB#266:                               # %._crit_edge226
	movl	si_frame_indicator(%rip), %ecx
	testl	%ecx, %ecx
	jne	.LBB2_269
# BB#267:
	movq	input(%rip), %rax
	cmpl	$0, 2148(%rax)
	je	.LBB2_270
# BB#268:
	movl	$1, si_frame_indicator(%rip)
	movq	frame_pic_si(%rip), %rdi
	xorl	%esi, %esi
	callq	frame_picture
	movq	img(%rip), %rax
	movl	20(%rax), %eax
.LBB2_269:
	cmpl	$3, %eax
	jne	.LBB2_272
.LBB2_270:                              # %.thread
	movq	input(%rip), %rax
	cmpl	$0, 2156(%rax)
	je	.LBB2_272
# BB#271:
	callq	output_SP_coefficients
.LBB2_272:
	movq	input(%rip), %rax
	cmpl	$0, 4708(%rax)
	je	.LBB2_274
# BB#273:
	movl	$0, mb_adaptive(%rip)
.LBB2_274:
	cmpl	$2, 4704(%rax)
	jne	.LBB2_281
# BB#275:
	cmpl	$0, 5116(%rax)
	je	.LBB2_277
# BB#276:
	movq	generic_RC(%rip), %rax
	movl	$1, 4(%rax)
.LBB2_277:
	movq	img(%rip), %rax
	movl	$0, 15408(%rax)
	movl	$0, 15412(%rax)
	movl	$1, 15312(%rax)
	movq	top_pic(%rip), %rdi
	movq	bottom_pic(%rip), %rsi
	callq	field_picture
	movq	img(%rip), %rax
	movl	14260(%rax), %ecx
	cmpl	$1, %ecx
	movl	$frame_pic_2, %edx
	movl	$frame_pic_3, %esi
	cmoveq	%rdx, %rsi
	testl	%ecx, %ecx
	movl	$frame_pic_1, %ecx
	cmovneq	%rsi, %rcx
	movq	(%rcx), %rbp
	movq	top_pic(%rip), %rbx
	movq	bottom_pic(%rip), %r15
	cvtsi2sdl	15460(%rax), %xmm1
	movl	36(%rax), %eax
	addl	$-12, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	.LCPI2_4(%rip), %xmm0
	addsd	%xmm1, %xmm0
	callq	exp2
	movapd	%xmm0, %xmm2
	mulsd	.LCPI2_5(%rip), %xmm2
	movss	812(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	816(%rbp), %xmm0
	addss	820(%rbp), %xmm0
	movss	812(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	816(%rbx), %xmm1
	addss	820(%rbx), %xmm1
	movl	808(%rbx), %edi
	addl	808(%r15), %edi
	movl	808(%rbp), %esi
	callq	decide_fld_frame
	movq	img(%rip), %rcx
	movl	%eax, 14256(%rcx)
	testl	%eax, %eax
	je	.LBB2_279
# BB#278:
	movl	15608(%rcx), %r14d
.LBB2_279:
	movl	%eax, %edi
	callq	update_field_frame_contexts
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	movq	img(%rip), %rax
	movl	14256(%rax), %edx
	je	.LBB2_282
# BB#280:
	xorl	%ecx, %ecx
	testl	%edx, %edx
	sete	%cl
	movq	generic_RC(%rip), %rsi
	movl	%ecx, 8(%rsi)
	jmp	.LBB2_282
.LBB2_281:
	movq	img(%rip), %rax
	movl	$0, 14256(%rax)
	xorl	%edx, %edx
.LBB2_282:                              # %._crit_edge231
	movl	%r14d, 15608(%rax)
.LBB2_283:
	movq	stats(%rip), %rcx
	leaq	2240(%rcx), %rsi
	leaq	2244(%rcx), %rdi
	testl	%edx, %edx
	cmoveq	%rsi, %rdi
	movl	(%rdi), %esi
	addl	%esi, 40(%rcx)
	movl	20(%rax), %ecx
	cmpl	$1, %ecx
	je	.LBB2_285
# BB#284:
	movl	%edx, 14328(%rax)
.LBB2_285:
	testl	%edx, %edx
	movq	imgY_org_frm(%rip), %rdx
	movq	%rdx, imgY_org(%rip)
	movq	imgUV_org_frm(%rip), %rsi
	movq	%rsi, imgUV_org(%rip)
	je	.LBB2_302
# BB#286:
	movq	snr(%rip), %rax
	movl	$0, (%rax)
	movq	$0, 4(%rax)
	movq	top_pic(%rip), %rdi
	callq	writeout_picture
	movq	bottom_pic(%rip), %rdi
.LBB2_287:
	callq	writeout_picture
.LBB2_288:
	movq	frame_pic_si(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_290
# BB#289:
	callq	free_slice_list
.LBB2_290:
	movq	frame_pic_3(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_292
# BB#291:
	callq	free_slice_list
.LBB2_292:
	movq	frame_pic_2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_294
# BB#293:
	callq	free_slice_list
.LBB2_294:
	movq	frame_pic_1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_296
# BB#295:
	callq	free_slice_list
.LBB2_296:
	movq	top_pic(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_298
# BB#297:
	callq	free_slice_list
.LBB2_298:
	movq	bottom_pic(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_300
# BB#299:
	callq	free_slice_list
.LBB2_300:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB2_308
# BB#301:
	movq	stats(%rip), %rax
	movl	16(%rax), %r13d
	subl	24(%rax), %r13d
	movq	quadratic_RC(%rip), %rdi
	movl	%r13d, %esi
	callq	rc_update_pict_frame
	movq	input(%rip), %rax
	cmpl	$0, 4704(%rax)
	jne	.LBB2_314
	jmp	.LBB2_309
.LBB2_302:
	movq	input(%rip), %rdi
	cmpl	$0, 4704(%rdi)
	jne	.LBB2_304
# BB#303:
	cmpl	$0, 4708(%rdi)
	je	.LBB2_305
.LBB2_304:
	movl	(%rax), %ebp
	leal	1(%rbp,%rbp), %ebx
	movl	$1, 14252(%rax)
	shrl	$31, %ebx
	leal	1(%rbx,%rbp,2), %ebp
	sarl	%ebp
	movl	%ebp, (%rax)
	movl	15588(%rax), %ebp
	addl	60(%rdi), %ebp
	movl	%ebp, 68(%rax)
	movl	84(%rax), %ebp
	movl	%ebp, 80(%rax)
	movq	snr(%rip), %rbp
	movl	$0, (%rbp)
	movl	$0, 4(%rbp)
	movl	$0, 8(%rbp)
	movq	%rdx, imgY_org(%rip)
	movq	%rsi, imgUV_org(%rip)
.LBB2_305:                              # %frame_mode_buffer.exit
	cmpl	$0, 2940(%rdi)
	je	.LBB2_347
# BB#306:
	cmpl	$2, 14260(%rax)
	jne	.LBB2_345
# BB#307:
	movq	frame_pic_3(%rip), %rdi
	jmp	.LBB2_287
.LBB2_308:
	xorl	%r13d, %r13d
	cmpl	$0, 4704(%rax)
	jne	.LBB2_314
.LBB2_309:
	cmpl	$3, 4168(%rax)
	jne	.LBB2_312
# BB#310:
	movq	img(%rip), %rcx
	cmpl	$1, 20(%rcx)
	je	.LBB2_312
# BB#311:
	callq	UpdateDecoders
	movq	input(%rip), %rax
.LBB2_312:
	cmpl	$0, 4732(%rax)
	je	.LBB2_314
# BB#313:
	callq	UpdatePixelMap
	movq	input(%rip), %rax
.LBB2_314:
	cmpl	$0, 5112(%rax)
	je	.LBB2_325
# BB#315:
	movq	img(%rip), %rdi
	movl	15520(%rdi), %edx
	movl	15524(%rdi), %ecx
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	imull	%edx, %edx
	movslq	60(%rax), %r14
	movslq	56(%rax), %r10
	movl	%r10d, %esi
	imull	%r14d, %esi
	movslq	5264(%rax), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	5268(%rax), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	$0, 14256(%rdi)
	movl	%r13d, 64(%rsp)         # 4-byte Spill
	movl	%edx, 72(%rsp)          # 4-byte Spill
	movl	%esi, 56(%rsp)          # 4-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	je	.LBB2_326
# BB#316:                               # %.preheader129.i
	testl	%r10d, %r10d
	jle	.LBB2_351
# BB#317:                               # %.preheader128.lr.ph.i
	testl	%r14d, %r14d
	jle	.LBB2_351
# BB#318:                               # %.preheader128.us.preheader.i
	movq	imgY_org(%rip), %rax
	movq	imgY_com(%rip), %rcx
	movq	14232(%rdi), %r9
	movl	%r14d, %r13d
	andl	$1, %r13d
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	8(%rcx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	8(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_319:                              # %.preheader128.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_323 Depth 2
	testq	%r13, %r13
	jne	.LBB2_321
# BB#320:                               #   in Loop: Header=BB2_319 Depth=1
	xorl	%ebp, %ebp
	cmpl	$1, %r14d
	jne	.LBB2_322
	jmp	.LBB2_324
	.p2align	4, 0x90
.LBB2_321:                              #   in Loop: Header=BB2_319 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movzwl	(%rax,%rbx,2), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rdx
	movzwl	(%rdx,%rbx,2), %edx
	subq	%rdx, %rax
	movslq	(%r9,%rax,4), %rax
	addq	%rax, %r12
	movl	$1, %ebp
	cmpl	$1, %r14d
	je	.LBB2_324
.LBB2_322:                              # %.preheader128.us.i.new
                                        #   in Loop: Header=BB2_319 Depth=1
	movq	%r14, %rdx
	subq	%rbp, %rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,8), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB2_323:                              #   Parent Loop BB2_319 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbp), %rsi
	movq	(%rbp), %r11
	movzwl	(%rsi,%rbx,2), %esi
	movq	-8(%rax), %r8
	movq	(%rax), %r15
	movzwl	(%r8,%rbx,2), %edi
	subq	%rdi, %rsi
	movslq	(%r9,%rsi,4), %rsi
	addq	%r12, %rsi
	movzwl	(%r11,%rbx,2), %edi
	movzwl	(%r15,%rbx,2), %ecx
	subq	%rcx, %rdi
	movslq	(%r9,%rdi,4), %r12
	addq	%rsi, %r12
	addq	$16, %rax
	addq	$16, %rbp
	addq	$-2, %rdx
	jne	.LBB2_323
.LBB2_324:                              # %._crit_edge164.us.i
                                        #   in Loop: Header=BB2_319 Depth=1
	incq	%rbx
	cmpq	%r10, %rbx
	jne	.LBB2_319
	jmp	.LBB2_352
.LBB2_325:
	movq	snr(%rip), %rcx
	movl	$0, (%rcx)
	movl	$0, 4(%rcx)
	movl	$0, 8(%rcx)
	movl	$0, 96(%rcx)
	movq	$0, 100(%rcx)
	jmp	.LBB2_379
.LBB2_351:
	xorl	%r12d, %r12d
.LBB2_352:                              # %._crit_edge168.i
	xorl	%r13d, %r13d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB2_364
# BB#353:                               # %._crit_edge168.i
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	15536(%rsi), %eax
	testl	%eax, %eax
	je	.LBB2_364
# BB#354:                               # %.preheader125.lr.ph.i
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_361
# BB#355:                               # %.preheader125.us.preheader.i
	movq	imgUV_org(%rip), %rcx
	movq	imgUV_com(%rip), %rdx
	movq	14232(%rsi), %r9
	movq	(%rcx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	(%rdx), %rbx
	movq	8(%rdx), %r10
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_356:                              # %.preheader125.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_357 Depth 2
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rcx
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	%r10, %r15
	movq	8(%rsp), %rdx           # 8-byte Reload
	.p2align	4, 0x90
.LBB2_357:                              #   Parent Loop BB2_356 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rbp
	movzwl	(%rbp,%rsi,2), %ebp
	movq	(%rcx), %r8
	movzwl	(%r8,%rsi,2), %eax
	subq	%rax, %rbp
	movslq	(%r9,%rbp,4), %rax
	addq	%rax, %r14
	movq	(%r11), %rax
	movzwl	(%rax,%rsi,2), %eax
	movq	(%r15), %rbp
	movzwl	(%rbp,%rsi,2), %ebp
	subq	%rbp, %rax
	movslq	(%r9,%rax,4), %rax
	addq	%rax, %r13
	addq	$8, %r15
	addq	$8, %r11
	addq	$8, %rcx
	addq	$8, %rdi
	decq	%rdx
	jne	.LBB2_357
# BB#358:                               # %._crit_edge153.us.i
                                        #   in Loop: Header=BB2_356 Depth=1
	incq	%rsi
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	jne	.LBB2_356
	jmp	.LBB2_365
.LBB2_326:
	movq	imgY_org_frm(%rip), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rcx, imgY_org(%rip)
	movq	imgUV_org_frm(%rip), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rcx, imgUV_org(%rip)
	cmpl	$2, 4704(%rax)
	jne	.LBB2_328
# BB#327:
	movq	enc_frame_picture(%rip), %rax
	movq	%rax, enc_picture(%rip)
.LBB2_328:                              # %.preheader124.i
	xorl	%r12d, %r12d
	testl	%r14d, %r14d
	jle	.LBB2_337
# BB#329:                               # %.preheader124.i
	testl	%r10d, %r10d
	jle	.LBB2_337
# BB#330:                               # %.preheader123.us.preheader.i
	movq	enc_picture(%rip), %rcx
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	14232(%rax), %rax
	movq	6440(%rcx), %r13
	movl	%r14d, %r15d
	andl	$1, %r15d
	leaq	8(%r13), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_331:                              # %.preheader123.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_335 Depth 2
	testq	%r15, %r15
	jne	.LBB2_333
# BB#332:                               #   in Loop: Header=BB2_331 Depth=1
	xorl	%ebp, %ebp
	cmpl	$1, %r14d
	jne	.LBB2_334
	jmp	.LBB2_336
	.p2align	4, 0x90
.LBB2_333:                              #   in Loop: Header=BB2_331 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rdx
	movzwl	(%rdx,%r8,2), %edx
	movq	(%r13), %rbp
	movzwl	(%rbp,%r8,2), %ebp
	subq	%rbp, %rdx
	movslq	(%rax,%rdx,4), %rdx
	addq	%rdx, %r12
	movl	$1, %ebp
	cmpl	$1, %r14d
	je	.LBB2_336
.LBB2_334:                              # %.preheader123.us.i.new
                                        #   in Loop: Header=BB2_331 Depth=1
	movq	%r14, %rdx
	subq	%rbp, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp,8), %rbx
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB2_335:                              #   Parent Loop BB2_331 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rbp), %rdi
	movq	(%rbp), %rcx
	movzwl	(%rdi,%r8,2), %edi
	movq	-8(%rbx), %r11
	movq	(%rbx), %r9
	movzwl	(%r11,%r8,2), %esi
	subq	%rsi, %rdi
	movslq	(%rax,%rdi,4), %rsi
	addq	%r12, %rsi
	movzwl	(%rcx,%r8,2), %ecx
	movzwl	(%r9,%r8,2), %edi
	subq	%rdi, %rcx
	movslq	(%rax,%rcx,4), %r12
	addq	%rsi, %r12
	addq	$16, %rbx
	addq	$16, %rbp
	addq	$-2, %rdx
	jne	.LBB2_335
.LBB2_336:                              # %._crit_edge143.us.i
                                        #   in Loop: Header=BB2_331 Depth=1
	incq	%r8
	cmpq	%r10, %r8
	jne	.LBB2_331
.LBB2_337:                              # %._crit_edge147.i
	xorl	%r13d, %r13d
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_364
# BB#338:                               # %._crit_edge147.i
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB2_364
# BB#339:                               # %._crit_edge147.i
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	15536(%rcx), %eax
	testl	%eax, %eax
	je	.LBB2_364
# BB#340:                               # %.preheader.us.preheader.i144
	movq	enc_picture(%rip), %rax
	movq	14232(%rcx), %r9
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	6472(%rax), %rax
	movq	(%rax), %r11
	movq	8(%rax), %r15
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_341:                              # %.preheader.us.i145
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_342 Depth 2
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r11, %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%r15, %rdx
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB2_342:                              #   Parent Loop BB2_341 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rsi
	movzwl	(%rsi,%r10,2), %esi
	movq	(%rcx), %r8
	movzwl	(%r8,%r10,2), %ebx
	subq	%rbx, %rsi
	movslq	(%r9,%rsi,4), %rsi
	addq	%rsi, %r14
	movq	(%rax), %rsi
	movzwl	(%rsi,%r10,2), %esi
	movq	(%rdx), %rbx
	movzwl	(%rbx,%r10,2), %ebx
	subq	%rbx, %rsi
	movslq	(%r9,%rsi,4), %rsi
	addq	%rsi, %r13
	addq	$8, %rdx
	addq	$8, %rax
	addq	$8, %rcx
	addq	$8, %rdi
	decq	%rbp
	jne	.LBB2_342
# BB#343:                               # %._crit_edge.us.i148
                                        #   in Loop: Header=BB2_341 Depth=1
	incq	%r10
	cmpq	(%rsp), %r10            # 8-byte Folded Reload
	jne	.LBB2_341
	jmp	.LBB2_365
.LBB2_345:
	cmpl	$1, 14260(%rax)
	jne	.LBB2_347
# BB#346:
	movq	frame_pic_2(%rip), %rdi
	jmp	.LBB2_287
.LBB2_347:                              # %.thread257
	cmpl	$3, %ecx
	jne	.LBB2_350
# BB#348:                               # %.thread257
	cmpl	$1, si_frame_indicator(%rip)
	jne	.LBB2_350
# BB#349:
	movq	frame_pic_si(%rip), %rdi
	callq	writeout_picture
	movl	$0, si_frame_indicator(%rip)
	jmp	.LBB2_288
.LBB2_350:
	movq	frame_pic_1(%rip), %rdi
	jmp	.LBB2_287
.LBB2_361:
	xorl	%r13d, %r13d
.LBB2_364:
	xorl	%r14d, %r14d
.LBB2_365:                              # %.loopexit122.i
	cvtsi2ssq	%r12, %xmm0
	movq	snr(%rip), %rax
	movss	%xmm0, 96(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%r14, %xmm0
	movss	%xmm0, 100(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%r13, %xmm0
	movss	%xmm0, 104(%rax)
	testq	%r12, %r12
	movl	72(%rsp), %eax          # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movl	56(%rsp), %eax          # 4-byte Reload
	cvtsi2sdl	%eax, %xmm1
	je	.LBB2_367
# BB#366:                               # %.loopexit122.i
	cvtsi2sdq	%r12, %xmm2
	jmp	.LBB2_368
.LBB2_367:
	movsd	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero
.LBB2_368:                              # %.loopexit122.i
	divsd	%xmm2, %xmm1
	mulsd	%xmm1, %xmm0
	callq	log10
	mulsd	.LCPI2_6(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movq	snr(%rip), %rax
	movss	%xmm0, (%rax)
	movq	img(%rip), %rcx
	cmpl	$0, 15536(%rcx)
	je	.LBB2_374
# BB#369:
	movl	84(%rsp), %eax          # 4-byte Reload
	imull	%eax, %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	imull	8(%rsp), %ecx           # 4-byte Folded Reload
	testq	%r14, %r14
	movl	%eax, %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	je	.LBB2_371
# BB#370:
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%r14, %xmm1
.LBB2_371:
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	divsd	%xmm1, %xmm0
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	mulsd	%xmm2, %xmm0
	callq	log10
	testq	%r13, %r13
	mulsd	.LCPI2_6(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movq	snr(%rip), %rax
	movss	%xmm0, 4(%rax)
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	je	.LBB2_373
# BB#372:
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r13, %xmm0
.LBB2_373:
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	log10
	mulsd	.LCPI2_6(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	movq	snr(%rip), %rax
	movq	img(%rip), %rcx
	jmp	.LBB2_375
.LBB2_374:
	movl	$0, 4(%rax)
	xorpd	%xmm1, %xmm1
.LBB2_375:
	movss	%xmm1, 8(%rax)
	cmpl	$0, (%rcx)
	movl	64(%rsp), %r13d         # 4-byte Reload
	je	.LBB2_377
# BB#376:
	movl	120(%rax), %edx
	leal	-1(%rdx), %esi
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%esi, %xmm2
	movss	84(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	cvtsi2ssl	%edx, %xmm3
	divss	%xmm3, %xmm4
	movss	%xmm4, 84(%rax)
	movss	88(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	4(%rax), %xmm4
	divss	%xmm3, %xmm4
	movss	%xmm4, 88(%rax)
	movss	92(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm4, %xmm1
	divss	%xmm3, %xmm1
	movss	%xmm1, 92(%rax)
	movss	108(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addss	96(%rax), %xmm1
	divss	%xmm3, %xmm1
	movss	%xmm1, 108(%rax)
	movss	112(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addss	100(%rax), %xmm1
	divss	%xmm3, %xmm1
	movss	%xmm1, 112(%rax)
	mulss	116(%rax), %xmm2
	addss	104(%rax), %xmm2
	divss	%xmm3, %xmm2
	movss	%xmm2, 116(%rax)
	jmp	.LBB2_378
.LBB2_377:                              # %.loopexit.loopexit183.i
	movl	(%rax), %edx
	movl	%edx, 12(%rax)
	movl	4(%rax), %esi
	movl	%esi, 16(%rax)
	movss	%xmm1, 20(%rax)
	movl	%edx, 84(%rax)
	movl	%esi, 88(%rax)
	movss	%xmm1, 92(%rax)
	movl	96(%rax), %esi
	movl	%esi, 108(%rax)
	movl	100(%rax), %esi
	movl	%esi, 112(%rax)
	movl	104(%rax), %esi
	movl	%esi, 116(%rax)
	movd	%edx, %xmm0
	xorps	%xmm1, %xmm1
	movups	%xmm1, 68(%rax)
	movups	%xmm1, 56(%rax)
	movups	%xmm1, 40(%rax)
	movups	%xmm1, 24(%rax)
.LBB2_378:                              # %find_snr.exit
	movslq	20(%rcx), %rcx
	movl	frame_ctr(,%rcx,4), %edx
	leal	-1(%rdx), %esi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%esi, %xmm1
	movss	24(%rax,%rcx,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	divss	%xmm0, %xmm2
	movss	%xmm2, 24(%rax,%rcx,4)
	movss	44(%rax,%rcx,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	4(%rax), %xmm2
	divss	%xmm0, %xmm2
	movss	%xmm2, 44(%rax,%rcx,4)
	mulss	64(%rax,%rcx,4), %xmm1
	addss	8(%rax), %xmm1
	divss	%xmm0, %xmm1
	movss	%xmm1, 64(%rax,%rcx,4)
	movq	input(%rip), %rax
.LBB2_379:
	movl	5084(%rax), %r8d
	testl	%r8d, %r8d
	je	.LBB2_398
# BB#380:
	movl	key_frame(%rip), %eax
	testl	%eax, %eax
	je	.LBB2_398
# BB#381:                               # %.preheader164
	movq	img(%rip), %rax
	movslq	52(%rax), %r9
	testq	%r9, %r9
	jle	.LBB2_390
# BB#382:                               # %.preheader163.lr.ph
	movslq	68(%rax), %r11
	testq	%r11, %r11
	jle	.LBB2_390
# BB#383:                               # %.preheader163.us.preheader
	movq	imgY_tmp(%rip), %rsi
	movq	enc_frame_picture(%rip), %rax
	movq	6440(%rax), %rdi
	leaq	-1(%r11), %r10
	movl	%r11d, %edx
	andl	$3, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_384:                              # %.preheader163.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_385 Depth 2
                                        #     Child Loop BB2_387 Depth 2
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB2_386
	.p2align	4, 0x90
.LBB2_385:                              #   Parent Loop BB2_384 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rcx,8), %rbp
	movzwl	(%rbp,%rax,2), %ebp
	movq	(%rsi,%rcx,8), %rbx
	movw	%bp, (%rbx,%rax,2)
	incq	%rcx
	cmpq	%rcx, %rdx
	jne	.LBB2_385
.LBB2_386:                              # %.prol.loopexit361
                                        #   in Loop: Header=BB2_384 Depth=1
	cmpq	$3, %r10
	jb	.LBB2_388
	.p2align	4, 0x90
.LBB2_387:                              #   Parent Loop BB2_384 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rcx,8), %rbx
	movzwl	(%rbx,%rax,2), %ebx
	movq	(%rsi,%rcx,8), %rbp
	movw	%bx, (%rbp,%rax,2)
	movq	8(%rdi,%rcx,8), %rbx
	movzwl	(%rbx,%rax,2), %ebx
	movq	8(%rsi,%rcx,8), %rbp
	movw	%bx, (%rbp,%rax,2)
	movq	16(%rdi,%rcx,8), %rbx
	movzwl	(%rbx,%rax,2), %ebx
	movq	16(%rsi,%rcx,8), %rbp
	movw	%bx, (%rbp,%rax,2)
	movq	24(%rdi,%rcx,8), %rbx
	movzwl	(%rbx,%rax,2), %ebx
	movq	24(%rsi,%rcx,8), %rbp
	movw	%bx, (%rbp,%rax,2)
	addq	$4, %rcx
	cmpq	%r11, %rcx
	jl	.LBB2_387
.LBB2_388:                              # %._crit_edge183.us
                                        #   in Loop: Header=BB2_384 Depth=1
	incq	%rax
	cmpq	%r9, %rax
	jl	.LBB2_384
# BB#389:                               # %.preheader161.loopexit
	movq	img(%rip), %rax
.LBB2_390:                              # %.preheader161
	movslq	64(%rax), %r9
	testq	%r9, %r9
	jle	.LBB2_398
# BB#391:                               # %.preheader160.lr.ph
	movslq	80(%rax), %r14
	testq	%r14, %r14
	jle	.LBB2_398
# BB#392:                               # %.preheader160.us.preheader
	movq	imgUV_tmp(%rip), %r15
	movq	imgUV_tmp+8(%rip), %rdi
	movq	enc_frame_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rbx
	movl	%r14d, %r10d
	andl	$1, %r10d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_393:                              # %.preheader160.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_396 Depth 2
	testq	%r10, %r10
	jne	.LBB2_395
# BB#394:                               #   in Loop: Header=BB2_393 Depth=1
	xorl	%eax, %eax
	cmpl	$1, %r14d
	jne	.LBB2_396
	jmp	.LBB2_397
	.p2align	4, 0x90
.LBB2_395:                              #   in Loop: Header=BB2_393 Depth=1
	movq	(%rbp), %rax
	movzwl	(%rax,%rcx,2), %r11d
	movq	(%r15), %rax
	movw	%r11w, (%rax,%rcx,2)
	movq	(%rbx), %rax
	movzwl	(%rax,%rcx,2), %r11d
	movq	(%rdi), %rax
	movw	%r11w, (%rax,%rcx,2)
	movl	$1, %eax
	cmpl	$1, %r14d
	je	.LBB2_397
	.p2align	4, 0x90
.LBB2_396:                              #   Parent Loop BB2_393 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp,%rax,8), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	movq	(%r15,%rax,8), %rsi
	movw	%dx, (%rsi,%rcx,2)
	movq	(%rbx,%rax,8), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	movq	(%rdi,%rax,8), %rsi
	movw	%dx, (%rsi,%rcx,2)
	movq	8(%rbp,%rax,8), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	movq	8(%r15,%rax,8), %rsi
	movw	%dx, (%rsi,%rcx,2)
	movq	8(%rbx,%rax,8), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	movq	8(%rdi,%rax,8), %rsi
	movw	%dx, (%rsi,%rcx,2)
	addq	$2, %rax
	cmpq	%r14, %rax
	jl	.LBB2_396
.LBB2_397:                              # %._crit_edge179.us
                                        #   in Loop: Header=BB2_393 Depth=1
	incq	%rcx
	cmpq	%r9, %rcx
	jl	.LBB2_393
.LBB2_398:                              # %.loopexit162
	testl	%r8d, %r8d
	je	.LBB2_417
# BB#399:                               # %.loopexit162
	movl	redundant_coding(%rip), %eax
	testl	%eax, %eax
	je	.LBB2_417
# BB#400:                               # %.preheader159
	movq	img(%rip), %rax
	movslq	52(%rax), %r8
	testq	%r8, %r8
	jle	.LBB2_409
# BB#401:                               # %.preheader158.lr.ph
	movslq	68(%rax), %r10
	testq	%r10, %r10
	jle	.LBB2_409
# BB#402:                               # %.preheader158.us.preheader
	movq	imgY_tmp(%rip), %rdx
	movq	enc_frame_picture(%rip), %rax
	movq	6440(%rax), %rsi
	leaq	-1(%r10), %r9
	movl	%r10d, %ecx
	andl	$3, %ecx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_403:                              # %.preheader158.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_404 Depth 2
                                        #     Child Loop BB2_406 Depth 2
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB2_405
	.p2align	4, 0x90
.LBB2_404:                              #   Parent Loop BB2_403 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rax,8), %rdi
	movzwl	(%rdi,%rbx,2), %edi
	movq	(%rsi,%rax,8), %rbp
	movw	%di, (%rbp,%rbx,2)
	incq	%rax
	cmpq	%rax, %rcx
	jne	.LBB2_404
.LBB2_405:                              # %.prol.loopexit353
                                        #   in Loop: Header=BB2_403 Depth=1
	cmpq	$3, %r9
	jb	.LBB2_407
	.p2align	4, 0x90
.LBB2_406:                              #   Parent Loop BB2_403 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rax,8), %rdi
	movzwl	(%rdi,%rbx,2), %edi
	movq	(%rsi,%rax,8), %rbp
	movw	%di, (%rbp,%rbx,2)
	movq	8(%rdx,%rax,8), %rdi
	movzwl	(%rdi,%rbx,2), %edi
	movq	8(%rsi,%rax,8), %rbp
	movw	%di, (%rbp,%rbx,2)
	movq	16(%rdx,%rax,8), %rdi
	movzwl	(%rdi,%rbx,2), %edi
	movq	16(%rsi,%rax,8), %rbp
	movw	%di, (%rbp,%rbx,2)
	movq	24(%rdx,%rax,8), %rdi
	movzwl	(%rdi,%rbx,2), %edi
	movq	24(%rsi,%rax,8), %rbp
	movw	%di, (%rbp,%rbx,2)
	addq	$4, %rax
	cmpq	%r10, %rax
	jl	.LBB2_406
.LBB2_407:                              # %._crit_edge175.us
                                        #   in Loop: Header=BB2_403 Depth=1
	incq	%rbx
	cmpq	%r8, %rbx
	jl	.LBB2_403
# BB#408:                               # %.preheader157.loopexit
	movq	img(%rip), %rax
.LBB2_409:                              # %.preheader157
	movslq	64(%rax), %r8
	testq	%r8, %r8
	jle	.LBB2_417
# BB#410:                               # %.preheader.lr.ph
	movslq	80(%rax), %r11
	testq	%r11, %r11
	jle	.LBB2_417
# BB#411:                               # %.preheader.us.preheader
	movq	imgUV_tmp(%rip), %r14
	movq	imgUV_tmp+8(%rip), %rsi
	movq	enc_frame_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rbp
	movl	%r11d, %r9d
	andl	$1, %r9d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_412:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_415 Depth 2
	testq	%r9, %r9
	jne	.LBB2_414
# BB#413:                               #   in Loop: Header=BB2_412 Depth=1
	xorl	%ebx, %ebx
	cmpl	$1, %r11d
	jne	.LBB2_415
	jmp	.LBB2_416
	.p2align	4, 0x90
.LBB2_414:                              #   in Loop: Header=BB2_412 Depth=1
	movq	(%r14), %rbx
	movzwl	(%rbx,%rax,2), %r10d
	movq	(%rdi), %rbx
	movw	%r10w, (%rbx,%rax,2)
	movq	(%rsi), %rbx
	movzwl	(%rbx,%rax,2), %r10d
	movq	(%rbp), %rbx
	movw	%r10w, (%rbx,%rax,2)
	movl	$1, %ebx
	cmpl	$1, %r11d
	je	.LBB2_416
	.p2align	4, 0x90
.LBB2_415:                              #   Parent Loop BB2_412 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rbx,8), %rcx
	movzwl	(%rcx,%rax,2), %ecx
	movq	(%rdi,%rbx,8), %rdx
	movw	%cx, (%rdx,%rax,2)
	movq	(%rsi,%rbx,8), %rcx
	movzwl	(%rcx,%rax,2), %ecx
	movq	(%rbp,%rbx,8), %rdx
	movw	%cx, (%rdx,%rax,2)
	movq	8(%r14,%rbx,8), %rcx
	movzwl	(%rcx,%rax,2), %ecx
	movq	8(%rdi,%rbx,8), %rdx
	movw	%cx, (%rdx,%rax,2)
	movq	8(%rsi,%rbx,8), %rcx
	movzwl	(%rcx,%rax,2), %ecx
	movq	8(%rbp,%rbx,8), %rdx
	movw	%cx, (%rdx,%rax,2)
	addq	$2, %rbx
	cmpq	%r11, %rbx
	jl	.LBB2_415
.LBB2_416:                              # %._crit_edge.us
                                        #   in Loop: Header=BB2_412 Depth=1
	incq	%rax
	cmpq	%r8, %rax
	jl	.LBB2_412
.LBB2_417:                              # %.loopexit
	leaq	88(%rsp), %rdi
	callq	time
	leaq	104(%rsp), %rdi
	callq	ftime
	movq	88(%rsp), %rax
	movzwl	112(%rsp), %ecx
	movzwl	128(%rsp), %edx
	subq	96(%rsp), %rax
	imulq	$1000, %rax, %rax       # imm = 0x3E8
	subq	%rdx, %rcx
	addq	tot_time(%rip), %rcx
	addq	%rax, %rcx
	movq	%rcx, tot_time(%rip)
	movq	input(%rip), %rax
	movq	img(%rip), %rcx
	movl	14256(%rcx), %edx
	cmpl	$2, 4704(%rax)
	jne	.LBB2_420
# BB#418:
	testl	%edx, %edx
	je	.LBB2_422
# BB#419:
	movq	enc_bottom_picture(%rip), %rdi
	callq	store_picture_in_dpb
	movq	enc_frame_picture(%rip), %rdi
	jmp	.LBB2_431
.LBB2_420:
	testl	%edx, %edx
	je	.LBB2_423
# BB#421:
	movq	enc_bottom_picture(%rip), %rdi
	callq	store_picture_in_dpb
	jmp	.LBB2_432
.LBB2_422:
	movq	enc_frame_picture(%rip), %rdi
	callq	replace_top_pic_with_frame
	movq	enc_bottom_picture(%rip), %rdi
	jmp	.LBB2_431
.LBB2_423:
	movl	14260(%rcx), %ecx
	cmpl	$1, %ecx
	je	.LBB2_426
# BB#424:
	cmpl	$2, %ecx
	jne	.LBB2_427
# BB#425:
	movq	enc_frame_picture3(%rip), %rdi
	callq	store_picture_in_dpb
	movq	enc_frame_picture(%rip), %rdi
	callq	free_storable_picture
	movq	enc_frame_picture2(%rip), %rdi
	jmp	.LBB2_431
.LBB2_426:
	movq	enc_frame_picture2(%rip), %rdi
	callq	store_picture_in_dpb
	movq	enc_frame_picture(%rip), %rdi
	jmp	.LBB2_430
.LBB2_427:
	cmpl	$0, 5084(%rax)
	je	.LBB2_429
# BB#428:
	cmpl	$0, key_frame(%rip)
	jne	.LBB2_432
.LBB2_429:
	movq	enc_frame_picture(%rip), %rdi
	callq	store_picture_in_dpb
	movq	enc_frame_picture2(%rip), %rdi
.LBB2_430:
	callq	free_storable_picture
	movq	enc_frame_picture3(%rip), %rdi
.LBB2_431:
	callq	free_storable_picture
.LBB2_432:
	movq	img(%rip), %rcx
	movl	15352(%rcx), %esi
	movl	%esi, %eax
	shrl	%eax
	addl	15608(%rcx), %eax
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, 15604(%rcx)
	movq	input(%rip), %rdx
	cmpl	$0, 5116(%rdx)
	je	.LBB2_436
# BB#433:
	cmpl	$1, 20(%rcx)
	je	.LBB2_436
# BB#434:
	cmpl	%esi, 5128(%rdx)
	jae	.LBB2_436
# BB#435:
	movq	quadratic_RC(%rip), %rdx
	movl	%eax, 1356(%rdx)
.LBB2_436:
	movq	stats(%rip), %rax
	movl	16(%rax), %edx
	subl	24(%rax), %edx
	movslq	%edx, %rdx
	movq	Bit_Buffer(%rip), %rsi
	movq	total_frame_buffer(%rip), %rdi
	movq	%rdx, (%rsi,%rdi,8)
	incq	total_frame_buffer(%rip)
	cmpl	$2, 15272(%rcx)
	jne	.LBB2_441
# BB#437:
	movl	encode_one_frame.consecutive_non_reference_pictures(%rip), %edx
	incl	%edx
	xorl	%esi, %esi
	cmpl	$0, 15360(%rcx)
	cmovel	%edx, %esi
	movl	%esi, encode_one_frame.consecutive_non_reference_pictures(%rip)
	cmpl	$1, %esi
	jg	.LBB2_439
# BB#438:
	movl	frame_no(%rip), %ecx
	cmpl	encode_one_frame.prev_frame_no(%rip), %ecx
	jge	.LBB2_440
.LBB2_439:
	movl	$.L.str, %edi
	movl	$-1, %esi
	callq	error
	movl	frame_no(%rip), %ecx
	movq	stats(%rip), %rax
.LBB2_440:
	movl	%ecx, encode_one_frame.prev_frame_no(%rip)
.LBB2_441:
	cmpl	$0, 2260(%rax)
	je	.LBB2_444
# BB#442:
	movq	input(%rip), %rax
	cmpl	$0, 5112(%rax)
	je	.LBB2_444
# BB#443:
	movl	frame_no(%rip), %esi
	movl	$.L.str.12, %edi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
.LBB2_444:                              # %ReportNALNonVLCBits.exit
	movq	img(%rip), %rax
	movl	(%rax), %edx
	movq	input(%rip), %rcx
	cmpl	start_frame_no_in_this_IGOP(%rip), %edx
	jne	.LBB2_448
# BB#445:
	movl	5112(%rcx), %ecx
	cmpl	$2, %ecx
	je	.LBB2_459
# BB#446:
	cmpl	$1, %ecx
	jne	.LBB2_460
# BB#447:
	movl	frame_no(%rip), %esi
	movl	15360(%rax), %ebp
	movl	15604(%rax), %ecx
	movq	snr(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cmpl	$0, 14256(%rax)
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.14, %eax
	movl	$.L.str.15, %ebx
	cmovneq	%rax, %rbx
	movl	$.L.str.13, %edi
	movl	$0, %edx
	movl	$0, %r8d
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbp
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_460
.LBB2_448:
	cmpl	$0, 5116(%rcx)
	je	.LBB2_452
# BB#449:
	cmpl	$0, 4704(%rcx)
	jne	.LBB2_451
# BB#450:
	cmpl	$0, 4708(%rcx)
	je	.LBB2_485
.LBB2_451:
	movq	stats(%rip), %rdx
	movq	16(%rdx), %rdx
	movq	quadratic_RC(%rip), %rsi
	movl	1520(%rsi), %edi
	movq	%rdx, 1520(%rsi)
	movl	%edx, %r13d
	subl	%edi, %r13d
.LBB2_452:
	movl	20(%rax), %edx
	cmpl	$1, %edx
	je	.LBB2_474
# BB#453:
	cmpl	$3, %edx
	je	.LBB2_477
# BB#454:
	cmpl	$2, %edx
	jne	.LBB2_480
# BB#455:
	movq	stats(%rip), %rdx
	movq	16(%rdx), %rsi
	subq	24(%rdx), %rsi
	addq	%rsi, 720(%rdx)
	movl	5112(%rcx), %ecx
	cmpl	$2, %ecx
	je	.LBB2_486
# BB#456:
	cmpl	$1, %ecx
	jne	.LBB2_465
# BB#457:
	movq	14208(%rax), %rdx
	movl	frame_no(%rip), %esi
	movl	15604(%rax), %ecx
	movq	snr(%rip), %rdi
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	cmpl	$0, 14256(%rax)
	movl	$.L.str.14, %edi
	movl	$.L.str.15, %ebp
	cmovneq	%rdi, %rbp
	movl	15360(%rax), %ebx
	cmpl	$1, 4(%rdx)
	jne	.LBB2_491
# BB#458:
	movl	$.L.str.13, %edi
	movl	$0, %edx
	movl	$0, %r8d
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbx
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_465
.LBB2_459:
	movl	frame_no(%rip), %esi
	movl	15604(%rax), %r8d
	movq	snr(%rip), %rcx
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	cmpl	$0, 14256(%rax)
	movl	$.L.str.14, %ecx
	movl	$.L.str.15, %ebp
	cmovneq	%rcx, %rbp
	movl	intras(%rip), %r10d
	movl	14260(%rax), %r15d
	movl	14456(%rax), %r11d
	movl	14460(%rax), %r14d
	movl	15360(%rax), %ebx
	subq	$8, %rsp
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.16, %edi
	movl	$0, %edx
	movl	$0, %ecx
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbx
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$64, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset -64
.LBB2_460:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB2_464
# BB#461:
	cmpl	$0, 4704(%rax)
	jne	.LBB2_463
# BB#462:
	cmpl	$0, 4708(%rax)
	je	.LBB2_464
.LBB2_463:
	movq	stats(%rip), %rax
	movq	16(%rax), %rax
	movq	quadratic_RC(%rip), %rcx
	movq	%rax, 1512(%rcx)
.LBB2_464:                              # %ReportFirstframe.exit
	movq	stats(%rip), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 720(%rax)
	movq	$0, 16(%rax)
.LBB2_465:                              # %ReportIntra.exit
	movq	input(%rip), %rax
	cmpl	$0, 5112(%rax)
	jne	.LBB2_467
# BB#466:
	movl	frame_no(%rip), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
.LBB2_467:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stats(%rip), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 24(%rax)
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB2_473
# BB#468:
	movq	quadratic_RC(%rip), %rdi
	movl	%r13d, %esi
	callq	rc_update_pict
	movq	img(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB2_470
# BB#469:
	movq	input(%rip), %rcx
	cmpl	$1, 5136(%rcx)
	jne	.LBB2_473
.LBB2_470:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB2_473
# BB#471:
	movq	quadratic_RC(%rip), %rdi
	callq	updateRCModel
	movq	input(%rip), %rax
	cmpl	$3, 5136(%rax)
	jne	.LBB2_473
# BB#472:
	callq	ComputeFrameMAD
	movq	quadratic_RC(%rip), %rax
	movsd	%xmm0, 1432(%rax)
.LBB2_473:
	movq	stats(%rip), %rax
	movl	$0, 2260(%rax)
	movq	img(%rip), %rax
	movl	(%rax), %ecx
	xorl	%eax, %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	setne	%al
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_474:
	movq	stats(%rip), %rdx
	movq	16(%rdx), %rsi
	subq	24(%rdx), %rsi
	addq	%rsi, 736(%rdx)
	movl	5112(%rcx), %ecx
	cmpl	$2, %ecx
	je	.LBB2_488
# BB#475:
	cmpl	$1, %ecx
	jne	.LBB2_465
# BB#476:
	movl	frame_no(%rip), %esi
	movl	15360(%rax), %ebp
	movl	15604(%rax), %ecx
	movq	snr(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cmpl	$0, 14256(%rax)
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.14, %eax
	movl	$.L.str.15, %ebx
	cmovneq	%rax, %rbx
	movl	$.L.str.21, %edi
	movl	$0, %edx
	movl	$0, %r8d
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbp
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_465
.LBB2_477:
	movq	stats(%rip), %rdx
	movq	16(%rdx), %rsi
	subq	24(%rdx), %rsi
	addq	%rsi, 728(%rdx)
	movl	5112(%rcx), %ecx
	cmpl	$2, %ecx
	je	.LBB2_489
# BB#478:
	cmpl	$1, %ecx
	jne	.LBB2_465
# BB#479:
	movl	frame_no(%rip), %esi
	movl	15360(%rax), %ebp
	movl	15604(%rax), %ecx
	movq	snr(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cmpl	$0, 14256(%rax)
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.14, %eax
	movl	$.L.str.15, %ebx
	cmovneq	%rax, %rbx
	movl	$.L.str.19, %edi
	movl	$0, %edx
	movl	$0, %r8d
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbp
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_465
.LBB2_480:
	movq	stats(%rip), %rdx
	movq	16(%rdx), %rsi
	subq	24(%rdx), %rsi
	addq	%rsi, 728(%rdx)
	movl	5112(%rcx), %edx
	cmpl	$2, %edx
	je	.LBB2_490
# BB#481:
	cmpl	$1, %edx
	jne	.LBB2_465
# BB#482:
	cmpl	$0, 5084(%rcx)
	je	.LBB2_493
# BB#483:
	cmpl	$0, redundant_coding(%rip)
	je	.LBB2_496
# BB#484:
	movl	15360(%rax), %ebp
	movl	15604(%rax), %edx
	movq	snr(%rip), %rcx
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cmpl	$0, 14256(%rax)
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.14, %eax
	movl	$.L.str.15, %r9d
	cmovneq	%rax, %r9
	subq	$8, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.24, %edi
	xorl	%esi, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movb	$3, %al
	pushq	%rbp
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_465
.LBB2_485:
	movq	stats(%rip), %rdx
	movl	16(%rdx), %r13d
	subl	24(%rdx), %r13d
	jmp	.LBB2_452
.LBB2_486:
	movq	14208(%rax), %rcx
	movl	frame_no(%rip), %esi
	movl	15604(%rax), %r8d
	movq	snr(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	cmpl	$0, 14256(%rax)
	movl	$.L.str.14, %edx
	movl	$.L.str.15, %ebp
	cmovneq	%rdx, %rbp
	movl	intras(%rip), %r10d
	movl	14456(%rax), %r11d
	movl	14460(%rax), %r14d
	movl	14260(%rax), %r15d
	movl	15360(%rax), %ebx
	cmpl	$1, 4(%rcx)
	jne	.LBB2_492
# BB#487:
	subq	$8, %rsp
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.16, %edi
	movl	$0, %edx
	movl	$0, %ecx
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbx
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$64, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB2_465
.LBB2_488:
	movl	frame_no(%rip), %esi
	movq	active_pps(%rip), %rcx
	movl	196(%rcx), %ecx
	movl	15604(%rax), %r8d
	movq	snr(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	cmpl	$0, 14256(%rax)
	movl	$.L.str.14, %edx
	movl	$.L.str.15, %ebp
	cmovneq	%rdx, %rbp
	movl	intras(%rip), %r10d
	movl	14260(%rax), %r12d
	movl	14452(%rax), %r11d
	movl	14456(%rax), %r14d
	movl	14460(%rax), %r15d
	movl	15360(%rax), %ebx
	movl	$.L.str.22, %edi
	movl	$0, %edx
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbx
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$64, %rsp
.Lcfi73:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB2_465
.LBB2_489:
	movl	frame_no(%rip), %esi
	movq	active_pps(%rip), %rcx
	movl	192(%rcx), %ecx
	movl	15604(%rax), %r8d
	movq	snr(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	cmpl	$0, 14256(%rax)
	movl	$.L.str.14, %edx
	movl	$.L.str.15, %ebp
	cmovneq	%rdx, %rbp
	movl	intras(%rip), %r10d
	movl	14260(%rax), %r15d
	movl	14456(%rax), %r11d
	movl	14460(%rax), %r14d
	movl	15360(%rax), %ebx
	subq	$8, %rsp
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.20, %edi
	movl	$0, %edx
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbx
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$64, %rsp
.Lcfi82:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB2_465
.LBB2_490:
	movl	frame_no(%rip), %esi
	movq	active_pps(%rip), %rcx
	movl	192(%rcx), %ecx
	movl	15604(%rax), %r8d
	movq	snr(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	cmpl	$0, 14256(%rax)
	movl	$.L.str.14, %edx
	movl	$.L.str.15, %ebp
	cmovneq	%rdx, %rbp
	movl	intras(%rip), %r10d
	movl	14260(%rax), %r15d
	movl	14456(%rax), %r11d
	movl	14460(%rax), %r14d
	movl	15360(%rax), %ebx
	subq	$8, %rsp
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.25, %edi
	movl	$0, %edx
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbx
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$64, %rsp
.Lcfi91:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB2_465
.LBB2_491:
	movl	$.L.str.17, %edi
	movl	$0, %edx
	movl	$0, %r8d
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbx
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi94:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_465
.LBB2_492:
	subq	$8, %rsp
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.18, %edi
	movl	$0, %edx
	movl	$0, %ecx
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbx
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$64, %rsp
.Lcfi103:
	.cfi_adjust_cfa_offset -64
	jmp	.LBB2_465
.LBB2_493:
	movl	frame_no(%rip), %esi
	movl	15360(%rax), %ebp
	movl	15604(%rax), %ecx
	movq	snr(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cmpl	$0, 14256(%rax)
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.14, %eax
	movl	$.L.str.15, %ebx
	cmovneq	%rax, %rbx
	movl	$.L.str.23, %edi
	movl	$0, %edx
	movl	$0, %r8d
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbp
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi106:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_465
.LBB2_494:
	movq	input(%rip), %rax
	movl	2948(%rax), %r13d
	movq	PicParSet(%rip), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, active_pps(%rip)
	movq	img(%rip), %rcx
	movl	36(%rcx), %edx
	cmpl	$0, 15260(%rcx)
	je	.LBB2_501
# BB#495:
	movl	%edx, %r12d
	jmp	.LBB2_502
.LBB2_496:
	movl	frame_no(%rip), %esi
	movl	15360(%rax), %ebp
	movl	15604(%rax), %ecx
	movq	snr(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cmpl	$0, 14256(%rax)
	cvtss2sd	%xmm2, %xmm2
	movl	$.L.str.14, %eax
	movl	$.L.str.15, %ebx
	cmovneq	%rax, %rbx
	movl	$.L.str.23, %edi
	movl	$0, %edx
	movl	$0, %r8d
	movl	$0, %r9d
	movb	$3, %al
	pushq	%rbp
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi109:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_465
.LBB2_497:
	movq	input(%rip), %rax
	cmpl	$0, 2952(%rax)
	je	.LBB2_499
# BB#498:
	movq	active_sps(%rip), %rcx
	cmpl	$66, 4(%rcx)
	jne	.LBB2_504
.LBB2_499:
	movl	2948(%rax), %r13d
	movq	PicParSet(%rip), %rax
	movq	%rax, active_pps(%rip)
	movq	img(%rip), %rcx
	cmpl	$0, 15260(%rcx)
	jne	.LBB2_240
# BB#500:
	incl	36(%rcx)
	jmp	.LBB2_240
.LBB2_501:
	decl	%edx
	movl	%edx, %r12d
	movl	%edx, 36(%rcx)
.LBB2_502:                              # %._crit_edge222
	movl	$0, 15408(%rcx)
	testl	%r13d, %r13d
	je	.LBB2_214
# BB#503:                               # %.thread54.i
	movl	$0, 14260(%rcx)
	movq	$0, enc_frame_picture2(%rip)
	jmp	.LBB2_220
.LBB2_504:
	movq	img(%rip), %rax
	movl	$1, 20(%rax)
	jmp	.LBB2_234
.LBB2_505:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	callq	report_stats_on_error
	movl	$-1, %edi
	callq	exit
.LBB2_506:
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end2:
	.size	encode_one_frame, .Lfunc_end2-encode_one_frame
	.cfi_endproc

	.globl	read_SP_coefficients
	.p2align	4, 0x90
	.type	read_SP_coefficients,@function
read_SP_coefficients:                   # @read_SP_coefficients
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 32
.Lcfi113:
	.cfi_offset %rbx, -32
.Lcfi114:
	.cfi_offset %r14, -24
.Lcfi115:
	.cfi_offset %r15, -16
	movq	input(%rip), %rdi
	movl	4156(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.LBB3_5
# BB#1:
	movq	img(%rip), %rax
	movl	14248(%rax), %eax
	leal	(%rcx,%rcx), %esi
	cltd
	idivl	%esi
	cmpl	%ecx, %edx
	jge	.LBB3_2
.LBB3_5:
	addq	$2672, %rdi             # imm = 0xA70
	movl	$.L.str.8, %esi
	callq	fopen64
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB3_7
# BB#6:
	movq	input(%rip), %rsi
	addq	$2416, %rsi             # imm = 0x970
	jmp	.LBB3_4
.LBB3_2:
	addq	$2416, %rdi             # imm = 0x970
	movl	$.L.str.8, %esi
	callq	fopen64
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB3_3
.LBB3_7:
	movq	img(%rip), %rax
	imull	$3, 88(%rax), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	number_sp2_frames(%rip), %rax
	movslq	%ecx, %rsi
	imulq	%rax, %rsi
	shlq	$2, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	fseek
	testl	%eax, %eax
	jne	.LBB3_8
# BB#10:
	incl	number_sp2_frames(%rip)
	movq	img(%rip), %rax
	cmpl	$0, 68(%rax)
	jle	.LBB3_12
# BB#11:                                # %.lr.ph26.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_15:                               # %.lr.ph26
                                        # =>This Inner Loop Header: Depth=1
	movslq	52(%rax), %r15
	movq	lrec(%rip), %rax
	movq	(%rax,%rbx,8), %rdi
	movl	$4, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	fread
	cmpl	%eax, %r15d
	jne	.LBB3_16
# BB#14:                                #   in Loop: Header=BB3_15 Depth=1
	incq	%rbx
	movq	img(%rip), %rax
	movslq	68(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB3_15
.LBB3_12:                               # %.preheader.preheader
	cmpl	$0, 80(%rax)
	jle	.LBB3_23
# BB#13:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	64(%rax), %r15
	movq	lrec_uv(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movl	$4, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	fread
	cmpl	%eax, %r15d
	jne	.LBB3_16
# BB#17:                                #   in Loop: Header=BB3_18 Depth=1
	incq	%rbx
	movq	img(%rip), %rax
	movslq	80(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB3_18
# BB#19:                                # %._crit_edge
	testl	%ecx, %ecx
	jle	.LBB3_23
# BB#20:                                # %.lr.ph.1.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_21:                               # %.lr.ph.1
                                        # =>This Inner Loop Header: Depth=1
	movslq	64(%rax), %r15
	movq	lrec_uv(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rbx,8), %rdi
	movl	$4, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	fread
	cmpl	%eax, %r15d
	jne	.LBB3_16
# BB#22:                                #   in Loop: Header=BB3_21 Depth=1
	incq	%rbx
	movq	img(%rip), %rax
	movslq	80(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB3_21
.LBB3_23:                               # %._crit_edge.1
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	fclose                  # TAILCALL
.LBB3_16:
	movl	$.Lstr.1, %edi
.LBB3_9:
	callq	puts
	movl	$-1, %edi
	callq	exit
.LBB3_8:
	movl	$.Lstr.2, %edi
	jmp	.LBB3_9
.LBB3_3:
	movq	input(%rip), %rsi
	addq	$2672, %rsi             # imm = 0xA70
.LBB3_4:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end3:
	.size	read_SP_coefficients, .Lfunc_end3-read_SP_coefficients
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1065353216              # float 1
	.text
	.globl	field_picture
	.p2align	4, 0x90
	.type	field_picture,@function
field_picture:                          # @field_picture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 32
.Lcfi119:
	.cfi_offset %rbx, -32
.Lcfi120:
	.cfi_offset %r14, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movl	$0, 15608(%rax)
	movq	stats(%rip), %rcx
	leaq	2244(%rcx), %rdx
	movl	$0, 2244(%rcx)
	movq	%rdx, 2248(%rcx)
	shll	(%rax)
	shll	15240(%rax)
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	input(%rip), %rcx
	movl	15588(%rax), %esi
	addl	60(%rcx), %esi
	movl	%esi, %edx
	shrl	$31, %edx
	addl	%esi, %edx
	sarl	%edx
	movl	%edx, 68(%rax)
	movl	84(%rax), %ecx
	movl	%ecx, %r8d
	shrl	$31, %r8d
	addl	%ecx, %r8d
	sarl	%r8d
	movl	%r8d, 80(%rax)
	movl	$1, 14256(%rax)
	movl	15352(%rax), %ecx
	shrl	%ecx
	movl	%ecx, 15348(%rax)
	movl	24(%rax), %edi
	movl	52(%rax), %esi
	movl	64(%rax), %ecx
	callq	alloc_storable_picture
	movq	%rax, enc_top_picture(%rip)
	movq	img(%rip), %rcx
	movl	15316(%rcx), %edx
	movl	%edx, 4(%rax)
	movl	%edx, 16(%rax)
	movl	15332(%rcx), %esi
	movl	%esi, 6364(%rax)
	movl	%esi, 6360(%rax)
	movl	$0, 6428(%rax)
	movl	$0, 15268(%rcx)
	movl	$0, 6432(%rax)
	movq	$get_mb_block_pos_normal, get_mb_block_pos(%rip)
	movq	$getNonAffNeighbour, getNeighbour(%rip)
	movl	%edx, 15328(%rcx)
	movl	$1, 24(%rcx)
	movq	%rax, enc_picture(%rip)
	movq	active_sps(%rip), %rdx
	movl	1148(%rdx), %esi
	movl	%esi, 6564(%rax)
	movl	1160(%rdx), %esi
	movl	%esi, 6568(%rax)
	movl	32(%rdx), %edi
	movl	%edi, 6560(%rax)
	testl	%esi, %esi
	je	.LBB4_1
# BB#2:
	movups	1164(%rdx), %xmm0
	jmp	.LBB4_3
.LBB4_1:
	xorps	%xmm0, %xmm0
.LBB4_3:                                # %copy_params.exit
	movups	%xmm0, 6572(%rax)
	movl	$0, 14252(%rcx)
	movq	imgY_org_top(%rip), %rax
	movq	%rax, imgY_org(%rip)
	movq	imgUV_org_top(%rip), %rax
	movq	%rax, imgUV_org(%rip)
	callq	init_field
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB4_5
# BB#4:
	decl	nextP_tr_fld(%rip)
.LBB4_5:
	movl	$1, 14256(%rax)
	movq	input(%rip), %rcx
	cmpl	$0, 5116(%rcx)
	je	.LBB4_10
# BB#6:
	movl	5128(%rcx), %edx
	movl	%edx, 15404(%rax)
	movq	quadratic_RC(%rip), %rdi
	cmpl	$1, 4704(%rcx)
	jne	.LBB4_8
# BB#7:
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorl	%esi, %esi
	movl	$1, %edx
	movl	$1, %ecx
	jmp	.LBB4_9
.LBB4_8:
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorl	%esi, %esi
	movl	$1, %edx
	xorl	%ecx, %ecx
.LBB4_9:
	callq	rc_init_pict
	movq	quadratic_RC(%rip), %rdi
	movl	$1, %esi
	callq	*updateQP(%rip)
	movq	img(%rip), %rcx
	movl	%eax, 36(%rcx)
	movq	generic_RC(%rip), %rax
	movl	$1, (%rax)
.LBB4_10:
	movq	top_pic(%rip), %rdi
	callq	code_a_picture
	movq	enc_picture(%rip), %rax
	movl	$1, (%rax)
	movq	enc_top_picture(%rip), %rdi
	callq	store_picture_in_dpb
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	(%rcx), %ebp
	shll	$3, %ebp
	movl	%ebp, 808(%rbx)
	movl	24(%rax), %edi
	movl	52(%rax), %esi
	movl	68(%rax), %edx
	movl	64(%rax), %ecx
	movl	80(%rax), %r8d
	callq	alloc_storable_picture
	movq	%rax, enc_bottom_picture(%rip)
	movq	img(%rip), %rcx
	movl	15320(%rcx), %edx
	movl	%edx, 4(%rax)
	movl	%edx, 16(%rax)
	movl	15332(%rcx), %esi
	movl	%esi, 6364(%rax)
	movl	%esi, 6360(%rax)
	movl	$0, 6428(%rax)
	movl	$0, 15268(%rcx)
	movl	$0, 6432(%rax)
	movq	$get_mb_block_pos_normal, get_mb_block_pos(%rip)
	movq	$getNonAffNeighbour, getNeighbour(%rip)
	movl	%edx, 15328(%rcx)
	movl	$2, 24(%rcx)
	movq	%rax, enc_picture(%rip)
	movq	active_sps(%rip), %rdx
	movl	1148(%rdx), %esi
	movl	%esi, 6564(%rax)
	movl	1160(%rdx), %esi
	movl	%esi, 6568(%rax)
	movl	32(%rdx), %edi
	movl	%edi, 6560(%rax)
	testl	%esi, %esi
	je	.LBB4_11
# BB#12:
	movups	1164(%rdx), %xmm0
	jmp	.LBB4_13
.LBB4_11:
	xorps	%xmm0, %xmm0
.LBB4_13:                               # %copy_params.exit10
	movups	%xmm0, 6572(%rax)
	movl	$1, 14252(%rcx)
	movq	imgY_org_bot(%rip), %rax
	movq	%rax, imgY_org(%rip)
	movq	imgUV_org_bot(%rip), %rax
	movq	%rax, imgUV_org(%rip)
	incl	(%rcx)
	callq	init_field
	movq	img(%rip), %rax
	movl	20(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB4_17
# BB#14:                                # %copy_params.exit10
	cmpl	$1, %ecx
	jne	.LBB4_16
# BB#15:                                # %.thread
	incl	nextP_tr_fld(%rip)
.LBB4_16:                               # %._crit_edge
	movq	input(%rip), %rcx
	jmp	.LBB4_19
.LBB4_17:
	movq	input(%rip), %rcx
	cmpl	$1, 4712(%rcx)
	je	.LBB4_19
# BB#18:
	xorl	%edx, %edx
	cmpl	$2, 2964(%rcx)
	sete	%dl
	movl	%edx, 20(%rax)
.LBB4_19:
	movl	$1, 14256(%rax)
	cmpl	$0, 5116(%rcx)
	je	.LBB4_21
# BB#20:
	movq	quadratic_RC(%rip), %rdi
	movl	%ebp, 1552(%rdi)
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	rc_init_pict
	movq	quadratic_RC(%rip), %rdi
	xorl	%esi, %esi
	callq	*updateQP(%rip)
	movq	img(%rip), %rcx
	movl	%eax, 36(%rcx)
	movq	generic_RC(%rip), %rax
	movl	$0, (%rax)
.LBB4_21:
	movq	enc_picture(%rip), %rax
	movl	$2, (%rax)
	movq	bottom_pic(%rip), %rdi
	callq	code_a_picture
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	(%rcx), %ecx
	shll	$3, %ecx
	movl	%ecx, 808(%r14)
	movl	(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%edx, (%rax)
	movl	15240(%rax), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%edx, 15240(%rax)
	movq	input(%rip), %rcx
	movl	15588(%rax), %edx
	addl	60(%rcx), %edx
	movl	%edx, 68(%rax)
	movl	84(%rax), %ecx
	movl	%ecx, 80(%rax)
	callq	combine_field
	movq	imgY_org_frm(%rip), %rax
	movq	%rax, imgY_org(%rip)
	movq	imgUV_org_frm(%rip), %rax
	movq	%rax, imgUV_org(%rip)
	callq	find_distortion
	movq	snr(%rip), %rax
	movl	(%rax), %ecx
	movl	%ecx, 812(%rbx)
	movl	4(%rax), %ecx
	movl	%ecx, 816(%rbx)
	movl	8(%rax), %eax
	movl	%eax, 820(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	field_picture, .Lfunc_end4-field_picture
	.cfi_endproc

	.globl	frame_picture
	.p2align	4, 0x90
	.type	frame_picture,@function
frame_picture:                          # @frame_picture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -24
.Lcfi126:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	img(%rip), %rax
	movl	$0, 15608(%rax)
	movl	$0, 24(%rax)
	movl	15352(%rax), %ecx
	movl	%ecx, 15348(%rax)
	cmpl	$2, %ebp
	jne	.LBB5_4
# BB#1:
	movl	68(%rax), %edx
	movl	52(%rax), %esi
	movl	64(%rax), %ecx
	movl	80(%rax), %r8d
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	callq	alloc_storable_picture
	movq	%rax, enc_frame_picture3(%rip)
	movq	img(%rip), %rcx
	movl	15324(%rcx), %edx
	movl	%edx, 4(%rax)
	movl	%edx, 15328(%rcx)
	movl	15316(%rcx), %esi
	movl	%esi, 8(%rax)
	movl	15320(%rcx), %esi
	movl	%esi, 12(%rax)
	movl	%edx, 16(%rax)
	movl	15332(%rcx), %edx
	movl	%edx, 6364(%rax)
	movl	%edx, 6360(%rax)
	movl	$1, 6428(%rax)
	movq	input(%rip), %rdx
	xorl	%esi, %esi
	cmpl	$0, 4708(%rdx)
	setne	%sil
	movl	%esi, 15268(%rcx)
	movl	%esi, 6432(%rax)
	movl	$get_mb_block_pos_mbaff, %edx
	movl	$get_mb_block_pos_normal, %esi
	cmovneq	%rdx, %rsi
	movq	%rsi, get_mb_block_pos(%rip)
	movl	$getAffNeighbour, %edx
	movl	$getNonAffNeighbour, %esi
	cmovneq	%rdx, %rsi
	movq	%rsi, getNeighbour(%rip)
	movq	%rax, enc_picture(%rip)
	movq	active_sps(%rip), %rdx
	movl	1148(%rdx), %esi
	movl	%esi, 6564(%rax)
	movl	1160(%rdx), %esi
	movl	%esi, 6568(%rax)
	movl	32(%rdx), %edi
	movl	%edi, 6560(%rax)
	testl	%esi, %esi
	jne	.LBB5_2
# BB#3:
	movq	$0, 6572(%rax)
	movl	$0, 6580(%rax)
	jmp	.LBB5_8
.LBB5_4:
	movl	68(%rax), %edx
	movl	52(%rax), %esi
	movl	64(%rax), %ecx
	movl	80(%rax), %r8d
	xorl	%edi, %edi
	callq	alloc_storable_picture
	cmpl	$1, %ebp
	jne	.LBB5_11
# BB#5:
	movq	%rax, enc_frame_picture2(%rip)
	jmp	.LBB5_6
.LBB5_11:
	movq	%rax, enc_frame_picture(%rip)
.LBB5_6:
	movq	img(%rip), %rcx
	movl	15324(%rcx), %edx
	movl	%edx, 4(%rax)
	movl	%edx, 15328(%rcx)
	movl	15316(%rcx), %esi
	movl	%esi, 8(%rax)
	movl	15320(%rcx), %esi
	movl	%esi, 12(%rax)
	movl	%edx, 16(%rax)
	movl	15332(%rcx), %edx
	movl	%edx, 6364(%rax)
	movl	%edx, 6360(%rax)
	movl	$1, 6428(%rax)
	movq	input(%rip), %rdx
	xorl	%esi, %esi
	cmpl	$0, 4708(%rdx)
	setne	%sil
	movl	%esi, 15268(%rcx)
	movl	%esi, 6432(%rax)
	movl	$get_mb_block_pos_mbaff, %edx
	movl	$get_mb_block_pos_normal, %esi
	cmovneq	%rdx, %rsi
	movq	%rsi, get_mb_block_pos(%rip)
	movl	$getAffNeighbour, %edx
	movl	$getNonAffNeighbour, %esi
	cmovneq	%rdx, %rsi
	movq	%rsi, getNeighbour(%rip)
	movq	%rax, enc_picture(%rip)
	movq	active_sps(%rip), %rdx
	movl	1148(%rdx), %esi
	movl	%esi, 6564(%rax)
	movl	1160(%rdx), %esi
	movl	%esi, 6568(%rax)
	movl	32(%rdx), %edi
	movl	%edi, 6560(%rax)
	testl	%esi, %esi
	je	.LBB5_7
.LBB5_2:
	movl	1164(%rdx), %esi
	movl	%esi, 6572(%rax)
	movl	1168(%rdx), %esi
	movl	%esi, 6576(%rax)
	movl	1172(%rdx), %esi
	movl	%esi, 6580(%rax)
	movl	1176(%rdx), %ebp
	jmp	.LBB5_8
.LBB5_7:
	movq	$0, 6572(%rax)
	movl	$0, 6580(%rax)
	xorl	%ebp, %ebp
.LBB5_8:                                # %copy_params.exit7
	movl	%ebp, 6584(%rax)
	movq	stats(%rip), %rax
	leaq	2240(%rax), %rdx
	movl	$0, 2240(%rax)
	movq	%rdx, 2248(%rax)
	movl	$0, 14256(%rcx)
	movq	%rbx, %rdi
	callq	code_a_picture
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movq	24(%rcx), %rcx
	movq	(%rcx), %rcx
	movl	(%rcx), %ecx
	shll	$3, %ecx
	movl	%ecx, 808(%rbx)
	cmpl	$0, 24(%rax)
	jne	.LBB5_10
# BB#9:
	callq	find_distortion
	movq	snr(%rip), %rax
	movl	(%rax), %ecx
	movl	%ecx, 812(%rbx)
	movl	4(%rax), %ecx
	movl	%ecx, 816(%rbx)
	movl	8(%rax), %eax
	movl	%eax, 820(%rbx)
.LBB5_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	frame_picture, .Lfunc_end5-frame_picture
	.cfi_endproc

	.globl	output_SP_coefficients
	.p2align	4, 0x90
	.type	output_SP_coefficients,@function
output_SP_coefficients:                 # @output_SP_coefficients
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 32
.Lcfi130:
	.cfi_offset %rbx, -24
.Lcfi131:
	.cfi_offset %r14, -16
	movl	$2160, %edi             # imm = 0x870
	addq	input(%rip), %rdi
	cmpl	$0, number_sp2_frames(%rip)
	je	.LBB6_1
# BB#3:
	movl	$.L.str.7, %esi
	callq	fopen64
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB6_4
	jmp	.LBB6_14
.LBB6_1:
	movl	$.L.str.5, %esi
	callq	fopen64
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB6_14
# BB#2:
	incl	number_sp2_frames(%rip)
.LBB6_4:
	movq	img(%rip), %rax
	cmpl	$0, 68(%rax)
	jle	.LBB6_7
# BB#5:                                 # %.lr.ph23.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph23
                                        # =>This Inner Loop Header: Depth=1
	movq	lrec(%rip), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movslq	52(%rax), %rdx
	movl	$4, %esi
	movq	%r14, %rcx
	callq	fwrite
	incq	%rbx
	movq	img(%rip), %rax
	movslq	68(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_6
.LBB6_7:                                # %.preheader.preheader
	cmpl	$0, 80(%rax)
	jle	.LBB6_13
# BB#8:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	lrec_uv(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movslq	64(%rax), %rdx
	movl	$4, %esi
	movq	%r14, %rcx
	callq	fwrite
	incq	%rbx
	movq	img(%rip), %rax
	movslq	80(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_9
# BB#10:                                # %._crit_edge
	testl	%ecx, %ecx
	jle	.LBB6_13
# BB#11:                                # %.lr.ph.1.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_12:                               # %.lr.ph.1
                                        # =>This Inner Loop Header: Depth=1
	movq	lrec_uv(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movslq	64(%rax), %rdx
	movl	$4, %esi
	movq	%r14, %rcx
	callq	fwrite
	incq	%rbx
	movq	img(%rip), %rax
	movslq	80(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_12
.LBB6_13:                               # %._crit_edge.1
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	fclose                  # TAILCALL
.LBB6_14:
	movq	input(%rip), %rsi
	addq	$2160, %rsi             # imm = 0x870
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end6:
	.size	output_SP_coefficients, .Lfunc_end6-output_SP_coefficients
	.cfi_endproc

	.p2align	4, 0x90
	.type	writeout_picture,@function
writeout_picture:                       # @writeout_picture
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi138:
	.cfi_def_cfa_offset 64
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	%rdi, 14208(%rax)
	cmpl	$0, (%rdi)
	jle	.LBB7_12
# BB#1:                                 # %.lr.ph4.preheader
	xorl	%r15d, %r15d
	movq	%rdi, (%rsp)            # 8-byte Spill
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_10:                               # %._crit_edge
                                        #   in Loop: Header=BB7_2 Depth=1
	incq	%r15
	movq	(%rsp), %rdi            # 8-byte Reload
	movslq	(%rdi), %rax
	cmpq	%rax, %r15
	jge	.LBB7_12
# BB#11:                                # %._crit_edge..lr.ph4_crit_edge
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	img(%rip), %rax
.LBB7_2:                                # %.lr.ph4
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
	movq	8(%rdi,%r15,8), %r13
	movl	12(%r13), %ecx
	movl	%ecx, 12(%rax)
	movl	16(%r13), %eax
	testl	%eax, %eax
	jle	.LBB7_10
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB7_2 Depth=1
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_4:                                #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%r13), %rcx
	movq	(%rcx,%rbp), %r12
	cmpl	$0, 40(%r12)
	je	.LBB7_9
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=2
	movq	img(%rip), %rax
	movl	15444(%rax), %ecx
	shll	$8, %ecx
	movl	15448(%rax), %edx
	shll	$9, %edx
	leal	128(%rcx,%rdx), %edi
	imull	15352(%rax), %edi
	addl	$500, %edi              # imm = 0x1F4
	callq	AllocNALU
	movq	%rax, %rbx
	movq	img(%rip), %rax
	xorl	%ecx, %ecx
	movl	12(%rax), %eax
	orl	%r14d, %eax
	sete	%cl
	addl	$3, %ecx
	movl	%ecx, (%rbx)
	movl	(%r12), %edx
	leal	1(%rdx), %eax
	movl	%eax, 4(%rbx)
	movq	24(%rbx), %rdi
	incq	%rdi
	movq	32(%r12), %rsi
	callq	memcpy
	movq	img(%rip), %rdx
	movq	14208(%rdx), %rax
	cmpl	$0, 4(%rax)
	je	.LBB7_7
# BB#6:                                 #   in Loop: Header=BB7_4 Depth=2
	movl	$3, %ecx
	movl	$5, %eax
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_7:                                #   in Loop: Header=BB7_4 Depth=2
	movq	input(%rip), %rcx
	leal	2(%r14), %eax
	cmpl	$0, 4016(%rcx)
	movl	$1, %ecx
	cmovel	%ecx, %eax
	xorl	%ecx, %ecx
	cmpl	$0, 15360(%rdx)
	setne	%cl
	addl	%ecx, %ecx
.LBB7_8:                                # %writeUnit.exit
                                        #   in Loop: Header=BB7_4 Depth=2
	movl	%eax, 12(%rbx)
	movl	%ecx, 16(%rbx)
	movl	$0, 20(%rbx)
	movq	%rbx, %rdi
	callq	*WriteNALU(%rip)
	cltq
	movq	stats(%rip), %rcx
	addq	%rax, 16(%rcx)
	movq	%rbx, %rdi
	callq	FreeNALU
	movl	16(%r13), %eax
.LBB7_9:                                #   in Loop: Header=BB7_4 Depth=2
	incq	%r14
	movslq	%eax, %rcx
	addq	$104, %rbp
	cmpq	%rcx, %r14
	jl	.LBB7_4
	jmp	.LBB7_10
.LBB7_12:                               # %._crit_edge5
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	writeout_picture, .Lfunc_end7-writeout_picture
	.cfi_endproc

	.globl	copy_params
	.p2align	4, 0x90
	.type	copy_params,@function
copy_params:                            # @copy_params
	.cfi_startproc
# BB#0:
	movq	active_sps(%rip), %rcx
	movl	1148(%rcx), %edx
	movq	enc_picture(%rip), %rax
	movl	%edx, 6564(%rax)
	movl	1160(%rcx), %edx
	movl	%edx, 6568(%rax)
	movl	32(%rcx), %esi
	movl	%esi, 6560(%rax)
	testl	%edx, %edx
	je	.LBB8_2
# BB#1:
	movl	1164(%rcx), %edx
	movl	%edx, 6572(%rax)
	movl	1168(%rcx), %edx
	movl	%edx, 6576(%rax)
	movl	1172(%rcx), %edx
	movl	%edx, 6580(%rax)
	movl	1176(%rcx), %ecx
	movl	%ecx, 6584(%rax)
	retq
.LBB8_2:
	movq	$0, 6572(%rax)
	movl	$0, 6580(%rax)
	xorl	%ecx, %ecx
	movl	%ecx, 6584(%rax)
	retq
.Lfunc_end8:
	.size	copy_params, .Lfunc_end8-copy_params
	.cfi_endproc

	.p2align	4, 0x90
	.type	find_distortion,@function
find_distortion:                        # @find_distortion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
.Lcfi151:
	.cfi_offset %rbx, -56
.Lcfi152:
	.cfi_offset %r12, -48
.Lcfi153:
	.cfi_offset %r13, -40
.Lcfi154:
	.cfi_offset %r14, -32
.Lcfi155:
	.cfi_offset %r15, -24
.Lcfi156:
	.cfi_offset %rbp, -16
	movq	img(%rip), %r10
	cmpl	$0, 24(%r10)
	je	.LBB9_20
# BB#1:                                 # %.preheader84
	movq	input(%rip), %r8
	movslq	56(%r8), %r9
	testq	%r9, %r9
	jle	.LBB9_2
# BB#3:                                 # %.preheader83.lr.ph
	movslq	60(%r8), %r14
	testq	%r14, %r14
	jle	.LBB9_4
# BB#5:                                 # %.preheader83.us.preheader
	movq	imgY_org(%rip), %r12
	movq	imgY_com(%rip), %r13
	movq	14232(%r10), %rsi
	movl	%r14d, %r11d
	andl	$1, %r11d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_6:                                # %.preheader83.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_10 Depth 2
	testq	%r11, %r11
	jne	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_6 Depth=1
	xorl	%edi, %edi
	cmpl	$1, %r14d
	jne	.LBB9_10
	jmp	.LBB9_11
	.p2align	4, 0x90
.LBB9_8:                                #   in Loop: Header=BB9_6 Depth=1
	movq	(%r12), %rdi
	movzwl	(%rdi,%rbx,2), %r15d
	movq	(%r13), %rdi
	movzwl	(%rdi,%rbx,2), %edi
	subq	%rdi, %r15
	movslq	(%rsi,%r15,4), %rdi
	addq	%rdi, %rbp
	movl	$1, %edi
	cmpl	$1, %r14d
	je	.LBB9_11
	.p2align	4, 0x90
.LBB9_10:                               #   Parent Loop BB9_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rdi,8), %rcx
	movzwl	(%rcx,%rbx,2), %ecx
	movq	(%r13,%rdi,8), %rdx
	movzwl	(%rdx,%rbx,2), %edx
	subq	%rdx, %rcx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rbp, %rcx
	movq	8(%r12,%rdi,8), %rax
	movzwl	(%rax,%rbx,2), %eax
	movq	8(%r13,%rdi,8), %rdx
	movzwl	(%rdx,%rbx,2), %edx
	subq	%rdx, %rax
	movslq	(%rsi,%rax,4), %rbp
	addq	%rcx, %rbp
	addq	$2, %rdi
	cmpq	%r14, %rdi
	jl	.LBB9_10
.LBB9_11:                               # %._crit_edge118.us
                                        #   in Loop: Header=BB9_6 Depth=1
	incq	%rbx
	cmpq	%r9, %rbx
	jl	.LBB9_6
	jmp	.LBB9_12
.LBB9_20:
	movq	imgY_org_frm(%rip), %r12
	movq	%r12, imgY_org(%rip)
	movq	imgUV_org_frm(%rip), %r8
	movq	%r8, imgUV_org(%rip)
	movq	input(%rip), %r9
	movslq	56(%r9), %r11
	testq	%r11, %r11
	jle	.LBB9_21
# BB#22:                                # %.preheader79.lr.ph
	movslq	60(%r9), %r15
	testq	%r15, %r15
	jle	.LBB9_23
# BB#24:                                # %.preheader79.us.preheader
	movq	enc_picture(%rip), %rax
	movq	14232(%r10), %rcx
	movq	6440(%rax), %rdx
	movl	%r15d, %r14d
	andl	$1, %r14d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_25:                               # %.preheader79.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_29 Depth 2
	testq	%r14, %r14
	jne	.LBB9_27
# BB#26:                                #   in Loop: Header=BB9_25 Depth=1
	xorl	%esi, %esi
	cmpl	$1, %r15d
	jne	.LBB9_29
	jmp	.LBB9_30
	.p2align	4, 0x90
.LBB9_27:                               #   in Loop: Header=BB9_25 Depth=1
	movq	(%r12), %rax
	movzwl	(%rax,%rbx,2), %eax
	movq	(%rdx), %rsi
	movzwl	(%rsi,%rbx,2), %esi
	subq	%rsi, %rax
	movslq	(%rcx,%rax,4), %rax
	addq	%rax, %rbp
	movl	$1, %esi
	cmpl	$1, %r15d
	je	.LBB9_30
	.p2align	4, 0x90
.LBB9_29:                               #   Parent Loop BB9_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rsi,8), %rax
	movzwl	(%rax,%rbx,2), %eax
	movq	(%rdx,%rsi,8), %rdi
	movzwl	(%rdi,%rbx,2), %edi
	subq	%rdi, %rax
	movslq	(%rcx,%rax,4), %rax
	addq	%rbp, %rax
	movq	8(%r12,%rsi,8), %rdi
	movzwl	(%rdi,%rbx,2), %edi
	movq	8(%rdx,%rsi,8), %rbp
	movzwl	(%rbp,%rbx,2), %ebp
	subq	%rbp, %rdi
	movslq	(%rcx,%rdi,4), %rbp
	addq	%rax, %rbp
	addq	$2, %rsi
	cmpq	%r15, %rsi
	jl	.LBB9_29
.LBB9_30:                               # %._crit_edge97.us
                                        #   in Loop: Header=BB9_25 Depth=1
	incq	%rbx
	cmpq	%r11, %rbx
	jl	.LBB9_25
	jmp	.LBB9_31
.LBB9_2:
	xorl	%ebp, %ebp
	cmpl	$0, 15536(%r10)
	jne	.LBB9_14
	jmp	.LBB9_13
.LBB9_4:
	xorl	%ebp, %ebp
.LBB9_12:                               # %._crit_edge122
	cmpl	$0, 15536(%r10)
	je	.LBB9_13
.LBB9_14:                               # %.preheader81
	movslq	5268(%r8), %r9
	testq	%r9, %r9
	jle	.LBB9_13
# BB#15:                                # %.preheader80.lr.ph
	movslq	5264(%r8), %r8
	testq	%r8, %r8
	jle	.LBB9_13
# BB#16:                                # %.preheader80.us.preheader
	movq	imgUV_org(%rip), %rcx
	movq	imgUV_com(%rip), %rdx
	movq	14232(%r10), %r12
	movq	(%rcx), %r10
	movq	8(%rcx), %r11
	movq	(%rdx), %r14
	movq	8(%rdx), %r15
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_17:                               # %.preheader80.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_18 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB9_18:                               #   Parent Loop BB9_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rdi,8), %rbx
	movzwl	(%rbx,%rsi,2), %ebx
	movq	(%r14,%rdi,8), %rax
	movzwl	(%rax,%rsi,2), %eax
	subq	%rax, %rbx
	movslq	(%r12,%rbx,4), %rax
	addq	%rax, %rcx
	movq	(%r11,%rdi,8), %rax
	movzwl	(%rax,%rsi,2), %eax
	movq	(%r15,%rdi,8), %rbx
	movzwl	(%rbx,%rsi,2), %ebx
	subq	%rbx, %rax
	movslq	(%r12,%rax,4), %rax
	addq	%rax, %rdx
	incq	%rdi
	cmpq	%r8, %rdi
	jl	.LBB9_18
# BB#19:                                # %._crit_edge107.us
                                        #   in Loop: Header=BB9_17 Depth=1
	incq	%rsi
	cmpq	%r9, %rsi
	jl	.LBB9_17
	jmp	.LBB9_38
.LBB9_21:
	xorl	%ebp, %ebp
	cmpl	$0, 15536(%r10)
	jne	.LBB9_32
	jmp	.LBB9_13
.LBB9_23:
	xorl	%ebp, %ebp
.LBB9_31:                               # %._crit_edge101
	cmpl	$0, 15536(%r10)
	je	.LBB9_13
.LBB9_32:                               # %.preheader78
	movslq	5268(%r9), %r11
	testq	%r11, %r11
	jle	.LBB9_13
# BB#33:                                # %.preheader.lr.ph
	movslq	5264(%r9), %r9
	testq	%r9, %r9
	jle	.LBB9_13
# BB#34:                                # %.preheader.us.preheader
	movq	enc_picture(%rip), %rax
	movq	14232(%r10), %r12
	movq	(%r8), %r10
	movq	8(%r8), %r8
	movq	6472(%rax), %rax
	movq	(%rax), %r14
	movq	8(%rax), %r15
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_35:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_36 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB9_36:                               #   Parent Loop BB9_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rdi,8), %rax
	movzwl	(%rax,%rsi,2), %eax
	movq	(%r14,%rdi,8), %rbx
	movzwl	(%rbx,%rsi,2), %ebx
	subq	%rbx, %rax
	movslq	(%r12,%rax,4), %rax
	addq	%rax, %rcx
	movq	(%r8,%rdi,8), %rax
	movzwl	(%rax,%rsi,2), %eax
	movq	(%r15,%rdi,8), %rbx
	movzwl	(%rbx,%rsi,2), %ebx
	subq	%rbx, %rax
	movslq	(%r12,%rax,4), %rax
	addq	%rax, %rdx
	incq	%rdi
	cmpq	%r9, %rdi
	jl	.LBB9_36
# BB#37:                                # %._crit_edge.us
                                        #   in Loop: Header=BB9_35 Depth=1
	incq	%rsi
	cmpq	%r11, %rsi
	jl	.LBB9_35
	jmp	.LBB9_38
.LBB9_13:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.LBB9_38:                               # %.loopexit
	cvtsi2ssq	%rbp, %xmm0
	movq	snr(%rip), %rax
	movss	%xmm0, (%rax)
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rcx, %xmm0
	movss	%xmm0, 4(%rax)
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rdx, %xmm0
	movss	%xmm0, 8(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	find_distortion, .Lfunc_end9-find_distortion
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	4607182418800017408     # double 1
.LCPI10_1:
	.quad	4611686018427387904     # double 2
	.text
	.p2align	4, 0x90
	.type	init_field,@function
init_field:                             # @init_field
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 32
.Lcfi160:
	.cfi_offset %rbx, -32
.Lcfi161:
	.cfi_offset %r14, -24
.Lcfi162:
	.cfi_offset %rbp, -16
	movq	last_P_no_fld(%rip), %rdx
	movq	%rdx, last_P_no(%rip)
	movq	img(%rip), %rsi
	movl	$0, 12(%rsi)
	movl	$0, 16(%rsi)
	movq	stats(%rip), %rax
	movl	$0, 32(%rax)
	movq	input(%rip), %r8
	movl	20(%r8), %ecx
	leal	(%rcx,%rcx), %r9d
	movl	%r9d, 20(%r8)
	movl	2096(%r8), %r11d
	leal	(%r11,%r11), %r10d
	movl	%r10d, 2096(%r8)
	movl	(%rsi), %ebx
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	sarl	%eax
	movl	%eax, (%rsi)
	movl	15240(%rsi), %ebp
	movl	%ebp, %edi
	shrl	$31, %edi
	addl	%ebp, %edi
	sarl	%edi
	movl	%edi, 15240(%rsi)
	movl	$0, 120(%rsi)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 176(%rsi)
	movupd	%xmm0, 160(%rsi)
	leal	2(%rcx,%rcx), %ecx
	cmpl	$0, 14364(%rsi)
	je	.LBB10_1
# BB#36:
	movl	%ecx, 14360(%rsi)
	leal	-1(%rax), %ebx
	imull	%ecx, %ebx
	movl	14252(%rsi), %edi
	addl	%edi, %ebx
	imull	%ecx, %eax
	addl	%edi, %eax
	testl	%edi, %edi
	je	.LBB10_37
# BB#40:
	leal	-1(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	%ebx, 4(%rdx)
	movl	15240(%rsi), %edi
	testl	%edi, %edi
	jle	.LBB10_43
# BB#41:                                # %.lr.ph59.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_42:                              # %.lr.ph59
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rcx,8), %edi
	subl	14360(%rsi), %edi
	movl	%edi, 8(%rdx,%rcx,8)
	movl	4(%rdx,%rcx,8), %edi
	subl	14360(%rsi), %edi
	movl	%edi, 12(%rdx,%rcx,8)
	movslq	15240(%rsi), %rdi
	incq	%rcx
	cmpq	%rdi, %rcx
	jl	.LBB10_42
	jmp	.LBB10_43
.LBB10_1:
	imull	%eax, %ecx
	movl	14252(%rsi), %edx
	addl	%edx, %ecx
	movl	%ecx, 14248(%rsi)
	testl	%edx, %edx
	jne	.LBB10_3
# BB#2:
	movl	14340(%rsi), %edx
	movl	%edx, 14344(%rsi)
	movl	%ecx, 14340(%rsi)
.LBB10_3:
	movl	4144(%r8), %edx
	testl	%edx, %edx
	je	.LBB10_6
# BB#4:
	incl	%eax
	cmpl	8(%r8), %eax
	jne	.LBB10_6
# BB#5:
	movl	%edx, 14248(%rsi)
	movl	%edx, %ecx
.LBB10_6:
	incl	%ebx
	cmpl	$3, %ebx
	jb	.LBB10_9
# BB#7:
	testl	%r11d, %r11d
	je	.LBB10_9
# BB#8:
	movl	%ecx, nextP_tr_fld(%rip)
.LBB10_9:
	cmpl	$0, 5116(%r8)
	jne	.LBB10_34
# BB#10:
	movl	20(%rsi), %r11d
	movl	4156(%r8), %ebx
	cmpl	$2, %r11d
	jne	.LBB10_19
# BB#11:
	testl	%ebx, %ebx
	jle	.LBB10_17
# BB#12:
	cmpl	%ebx, %ecx
	jl	.LBB10_14
# BB#13:
	cmpl	$0, 2152(%r8)
	je	.LBB10_16
.LBB10_14:
	leal	(%rbx,%rbx), %ebp
	movl	%ecx, %eax
	cltd
	idivl	%ebp
	cmpl	%ebx, %edx
	jl	.LBB10_17
# BB#15:
	cmpl	$1, 2152(%r8)
	jne	.LBB10_17
.LBB10_16:
	leaq	4160(%r8), %rax
	jmp	.LBB10_18
.LBB10_37:
	leal	1(%rbx), %ecx
	movl	%ecx, (%rdx)
	movl	%ebx, 4(%rdx)
	movl	15240(%rsi), %edi
	testl	%edi, %edi
	jle	.LBB10_43
# BB#38:                                # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_39:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%rcx,8), %edi
	subl	14360(%rsi), %edi
	movl	%edi, 8(%rdx,%rcx,8)
	movl	4(%rdx,%rcx,8), %edi
	subl	14360(%rsi), %edi
	movl	%edi, 12(%rdx,%rcx,8)
	movslq	15240(%rsi), %rdi
	incq	%rcx
	cmpq	%rdi, %rcx
	jl	.LBB10_39
.LBB10_43:                              # %.loopexit
	movl	4144(%r8), %ecx
	testl	%ecx, %ecx
	je	.LBB10_46
# BB#44:
	movl	(%rsi), %edx
	incl	%edx
	cmpl	8(%r8), %edx
	jne	.LBB10_46
# BB#45:
	movl	%ecx, %eax
	subl	%ebx, %eax
	movl	%eax, 14360(%rsi)
	movl	%ecx, %eax
.LBB10_46:
	movl	20(%r8), %r9d
	leal	1(%r9), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movl	2096(%r8), %r10d
	cvtsi2sdl	%r10d, %xmm1
	addsd	.LCPI10_0(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 14352(%rsi)
	movl	2968(%r8), %ecx
	cmpl	$3, %ecx
	jne	.LBB10_49
# BB#47:                                # %.thread
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, 14352(%rsi)
	movl	14364(%rsi), %edx
	movsd	.LCPI10_1(%rip), %xmm0  # xmm0 = mem[0],zero
	jmp	.LBB10_48
.LBB10_49:
	testl	%ecx, %ecx
	addsd	.LCPI10_0(%rip), %xmm0
	movl	14364(%rsi), %edx
	je	.LBB10_50
.LBB10_48:
	movq	gop_structure(%rip), %rcx
	leal	-1(%rdx), %ebp
	movslq	%ebp, %rbp
	leaq	(%rbp,%rbp,2), %rbp
	movl	4(%rcx,%rbp,8), %ecx
	incl	%ecx
	xorl	%r14d, %r14d
.LBB10_51:
	leaq	14252(%rsi), %r11
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %ebp
	addl	%ebx, %ebp
	leal	-1(%rax), %ecx
	cmpl	%eax, %ebp
	cmovll	%ebp, %ecx
	movl	%ecx, 14248(%rsi)
	cmpl	$0, 5116(%r8)
	jne	.LBB10_71
# BB#52:
	testb	%r14b, %r14b
	je	.LBB10_69
# BB#53:
	movl	4156(%r8), %ebx
	testl	%ebx, %ebx
	jle	.LBB10_59
# BB#54:
	cmpl	%ebx, %ecx
	jl	.LBB10_56
# BB#55:
	cmpl	$0, 2152(%r8)
	je	.LBB10_58
.LBB10_56:
	leal	(%rbx,%rbx), %ebp
	movl	%ecx, %eax
	cltd
	idivl	%ebp
	cmpl	%ebx, %edx
	jl	.LBB10_59
# BB#57:
	cmpl	$1, 2152(%r8)
	jne	.LBB10_59
.LBB10_58:
	leaq	4152(%r8), %rax
	jmp	.LBB10_60
.LBB10_50:
	movb	$1, %r14b
	movl	%edx, %ecx
	jmp	.LBB10_51
.LBB10_69:
	movq	gop_structure(%rip), %rax
	decl	%edx
	movslq	%edx, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movl	12(%rax,%rcx,8), %eax
	jmp	.LBB10_70
.LBB10_59:
	leaq	2104(%r8), %rax
.LBB10_60:
	movl	(%rax), %eax
	movl	%eax, 36(%rsi)
	cmpl	$0, 15360(%rsi)
	je	.LBB10_71
# BB#61:
	testl	%ebx, %ebx
	jle	.LBB10_67
# BB#62:
	cmpl	%ebx, %ecx
	jl	.LBB10_64
# BB#63:
	cmpl	$0, 2152(%r8)
	je	.LBB10_66
.LBB10_64:
	leal	(%rbx,%rbx), %ebp
	movl	%ecx, %eax
	cltd
	idivl	%ebp
	cmpl	%ebx, %edx
	jl	.LBB10_67
# BB#65:
	cmpl	$1, 2152(%r8)
	jne	.LBB10_67
.LBB10_66:
	leaq	4152(%r8), %rax
	leaq	4164(%r8), %rcx
	jmp	.LBB10_68
.LBB10_19:
	testl	%ebx, %ebx
	jle	.LBB10_25
# BB#20:
	cmpl	%ebx, %ecx
	jl	.LBB10_22
# BB#21:
	cmpl	$0, 2152(%r8)
	je	.LBB10_24
.LBB10_22:
	leal	(%rbx,%rbx), %ebp
	movl	%ecx, %eax
	cltd
	idivl	%ebp
	cmpl	%ebx, %edx
	jl	.LBB10_25
# BB#23:
	cmpl	$1, 2152(%r8)
	jne	.LBB10_25
.LBB10_24:
	movl	4148(%r8), %edx
	jmp	.LBB10_26
.LBB10_67:
	leaq	2104(%r8), %rax
	leaq	2108(%r8), %rcx
.LBB10_68:
	xorl	%edx, %edx
	subl	15452(%rsi), %edx
	movl	(%rcx), %ecx
	addl	(%rax), %ecx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	cmpl	$52, %ecx
	movl	$51, %eax
	cmovll	%ecx, %eax
.LBB10_70:
	movl	%eax, 36(%rsi)
	jmp	.LBB10_71
.LBB10_17:
	leaq	12(%r8), %rax
.LBB10_18:
	movl	(%rax), %eax
	movl	%eax, 36(%rsi)
	jmp	.LBB10_34
.LBB10_25:
	movl	16(%r8), %edx
.LBB10_26:
	xorl	%eax, %eax
	cmpl	$0, 15360(%rsi)
	jne	.LBB10_28
# BB#27:
	movl	5756(%r8), %eax
.LBB10_28:
	addl	%edx, %eax
	movl	%eax, 36(%rsi)
	cmpl	$3, %r11d
	jne	.LBB10_34
# BB#29:
	testl	%ebx, %ebx
	jle	.LBB10_32
# BB#30:
	leal	(%rbx,%rbx), %ebp
	movl	%ecx, %eax
	cltd
	idivl	%ebp
	cmpl	%ebx, %edx
	jge	.LBB10_31
.LBB10_32:
	movl	2140(%r8), %eax
	movl	%eax, 36(%rsi)
	movl	2144(%r8), %eax
	jmp	.LBB10_33
.LBB10_31:
	movl	4148(%r8), %eax
	subl	16(%r8), %eax
	movl	2140(%r8), %ecx
	addl	%eax, %ecx
	movl	%ecx, 36(%rsi)
	addl	2144(%r8), %eax
.LBB10_33:
	movl	%eax, 40(%rsi)
.LBB10_34:
	leaq	14252(%rsi), %r11
	movl	112(%rsi), %eax
	movl	%eax, 116(%rsi)
	movl	68(%r8), %ecx
	testl	%ecx, %ecx
	jle	.LBB10_71
# BB#35:
	movl	(%rsi), %eax
	movl	52(%rsi), %ebx
	cltd
	idivl	%ecx
	movl	%ebx, %ecx
	sarl	$31, %ecx
	shrl	$28, %ecx
	addl	%ebx, %ecx
	sarl	$4, %ecx
	cltd
	idivl	%ecx
	movl	%edx, 112(%rsi)
.LBB10_71:
	movl	15452(%rsi), %eax
	addl	36(%rsi), %eax
	movl	%eax, 44(%rsi)
	movl	%r9d, %eax
	shrl	$31, %eax
	addl	%r9d, %eax
	sarl	%eax
	movl	%eax, 20(%r8)
	movl	%r10d, %eax
	shrl	$31, %eax
	addl	%r10d, %eax
	sarl	%eax
	movl	%eax, 2096(%r8)
	addl	%edi, %edi
	movl	%edi, 15240(%rsi)
	movl	(%rsi), %eax
	addl	%eax, %eax
	addl	(%r11), %eax
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	init_field, .Lfunc_end10-init_field
	.cfi_endproc

	.globl	UnifiedOneForthPix
	.p2align	4, 0x90
	.type	UnifiedOneForthPix,@function
UnifiedOneForthPix:                     # @UnifiedOneForthPix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi163:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi164:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 48
.Lcfi168:
	.cfi_offset %rbx, -40
.Lcfi169:
	.cfi_offset %r14, -32
.Lcfi170:
	.cfi_offset %r15, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpq	$0, 6448(%rbx)
	je	.LBB11_1
.LBB11_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_1:
	movl	6392(%rbx), %r15d
	movl	6396(%rbx), %r14d
	addl	$40, %r14d
	addl	$40, %r15d
	leaq	6448(%rbx), %rbp
	movl	$4, %esi
	movl	$4, %edx
	movq	%rbp, %rdi
	movl	%r14d, %ecx
	movl	%r15d, %r8d
	callq	get_mem4Dpel
	cmpq	$0, (%rbp)
	jne	.LBB11_3
# BB#2:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB11_3:
	movq	input(%rip), %rax
	cmpl	$0, 5772(%rax)
	je	.LBB11_11
# BB#4:
	movq	img(%rip), %rax
	movl	15536(%rax), %eax
	testl	%eax, %eax
	je	.LBB11_11
# BB#5:
	leaq	6464(%rbx), %rdi
	cmpl	$1, %eax
	jne	.LBB11_7
# BB#6:
	movl	%r14d, %r8d
	shrl	$31, %r8d
	addl	%r14d, %r8d
	sarl	%r8d
	movl	%r15d, %r9d
	shrl	$31, %r9d
	addl	%r15d, %r9d
	sarl	%r9d
	movl	$2, %esi
	movl	$8, %edx
	movl	$8, %ecx
	jmp	.LBB11_10
.LBB11_7:
	cmpl	$2, %eax
	jne	.LBB11_9
# BB#8:
	movl	%r15d, %r9d
	shrl	$31, %r9d
	addl	%r15d, %r9d
	sarl	%r9d
	movl	$2, %esi
	movl	$4, %edx
	movl	$8, %ecx
	movl	%r14d, %r8d
	jmp	.LBB11_10
.LBB11_9:
	movl	$2, %esi
	movl	$4, %edx
	movl	$4, %ecx
	movl	%r14d, %r8d
	movl	%r15d, %r9d
.LBB11_10:
	callq	get_mem5Dpel
.LBB11_11:
	movq	%rbx, %rdi
	callq	getSubImagesLuma
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB11_13
# BB#12:
	movq	input(%rip), %rax
	cmpl	$0, 5772(%rax)
	je	.LBB11_13
# BB#14:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	getSubImagesChroma      # TAILCALL
.Lfunc_end11:
	.size	UnifiedOneForthPix, .Lfunc_end11-UnifiedOneForthPix
	.cfi_endproc

	.globl	dummy_slice_too_big
	.p2align	4, 0x90
	.type	dummy_slice_too_big,@function
dummy_slice_too_big:                    # @dummy_slice_too_big
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	dummy_slice_too_big, .Lfunc_end12-dummy_slice_too_big
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.zero	16,2
	.text
	.globl	copy_rdopt_data
	.p2align	4, 0x90
	.type	copy_rdopt_data,@function
copy_rdopt_data:                        # @copy_rdopt_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi172:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi173:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi174:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi175:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi176:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi177:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi178:
	.cfi_def_cfa_offset 224
.Lcfi179:
	.cfi_offset %rbx, -56
.Lcfi180:
	.cfi_offset %r12, -48
.Lcfi181:
	.cfi_offset %r13, -40
.Lcfi182:
	.cfi_offset %r14, -32
.Lcfi183:
	.cfi_offset %r15, -24
.Lcfi184:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14224(%rax), %rbx
	movslq	12(%rax), %rcx
	imulq	$536, %rcx, %rbp        # imm = 0x218
	leaq	(%rbx,%rbp), %rdi
	movl	20(%rax), %r15d
	movslq	432(%rbx,%rbp), %r14
	movq	rdopt(%rip), %rcx
	movl	1560(%rcx), %edx
	movl	1656(%rcx), %esi
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movl	%edx, 72(%rbx,%rbp)
	movl	1640(%rcx), %edx
	movl	%edx, 364(%rbx,%rbp)
	movq	1648(%rcx), %rdx
	movq	%rdx, 368(%rbx,%rbp)
	movzwl	1564(%rcx), %edx
	movw	%dx, 480(%rbx,%rbp)
	movl	1712(%rcx), %edx
	movl	%edx, 15244(%rax)
	movl	1732(%rcx), %eax
	movl	%eax, 496(%rbx,%rbp)
	movl	1736(%rcx), %eax
	movl	%eax, 500(%rbx,%rbp)
	movl	1744(%rcx), %eax
	movl	%eax, 504(%rbx,%rbp)
	movl	1740(%rcx), %eax
	movl	%eax, 4(%rbx,%rbp)
	movl	1728(%rcx), %eax
	movl	%eax, 8(%rbx,%rbp)
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	callq	set_chroma_qp
	movq	rdopt(%rip), %rcx
	movl	1716(%rcx), %eax
	movl	%eax, 416(%rbx,%rbp)
	movq	img(%rip), %rax
	cmpl	$-3, 15528(%rax)
	jl	.LBB13_1
# BB#2:                                 # %.preheader235.preheader
	xorl	%r13d, %r13d
	jmp	.LBB13_3
	.p2align	4, 0x90
.LBB13_4:                               # %.preheader235..preheader235_crit_edge
                                        #   in Loop: Header=BB13_3 Depth=1
	incq	%r13
	movq	rdopt(%rip), %rcx
.LBB13_3:                               # %.preheader235
                                        # =>This Inner Loop Header: Depth=1
	movq	14160(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rdi
	movq	1544(%rcx), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rdi
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movq	14160(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rdi
	movq	rdopt(%rip), %rax
	movq	1544(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rsi
	movl	$260, %edx              # imm = 0x104
	callq	memcpy
	movq	img(%rip), %rax
	movslq	15528(%rax), %rcx
	addq	$3, %rcx
	cmpq	%rcx, %r13
	jl	.LBB13_4
# BB#5:                                 # %.preheader232.preheader.loopexit
	movl	%r15d, %r13d
	movq	rdopt(%rip), %rcx
	jmp	.LBB13_6
.LBB13_1:
	movl	%r15d, %r13d
.LBB13_6:                               # %.preheader232.preheader
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %r12
	leaq	72(%rbx,%rbp), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	14168(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	1552(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	img(%rip), %rax
	movq	14168(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	rdopt(%rip), %rcx
	movq	1552(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	img(%rip), %rax
	movq	14168(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	rdopt(%rip), %rcx
	movq	1552(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	img(%rip), %rax
	movq	14168(%rax), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	movq	rdopt(%rip), %rcx
	movq	1552(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	img(%rip), %rax
	movq	14168(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	rdopt(%rip), %rcx
	movq	1552(%rcx), %rcx
	movq	16(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	img(%rip), %rax
	movq	14168(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movq	rdopt(%rip), %rcx
	movq	1552(%rcx), %rcx
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdx
	movq	%rdx, 64(%rax)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	48(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	img(%rip), %r9
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	(%rcx), %r11
	movl	168(%r9), %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB13_7:                               # =>This Inner Loop Header: Depth=1
	movl	172(%r9), %eax
	addl	%esi, %eax
	movslq	%eax, %rbx
	movq	(%r11,%rbx,8), %rax
	movslq	%ecx, %rcx
	movq	rdopt(%rip), %rdx
	movl	1680(%rdx,%rsi,4), %edx
	movl	%edx, (%rax,%rcx)
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %r10
	movq	6496(%rdx), %r8
	movq	(%r10), %r11
	movq	(%r11,%rbx,8), %rax
	movq	img(%rip), %r9
	movslq	168(%r9), %rcx
	movsbq	(%rax,%rcx), %rbp
	imulq	$264, %r14, %rdi        # imm = 0x108
	addq	%rdx, %rdi
	movq	24(%rdi,%rbp,8), %r15
	movq	(%r8), %rbp
	movq	(%rbp,%rbx,8), %rbp
	movq	%r15, (%rbp,%rcx,8)
	movsbq	1(%rax,%rcx), %rbx
	movq	24(%rdi,%rbx,8), %rbx
	movq	%rbx, 8(%rbp,%rcx,8)
	movsbq	2(%rax,%rcx), %rbx
	movq	24(%rdi,%rbx,8), %rbx
	movq	%rbx, 16(%rbp,%rcx,8)
	movsbq	3(%rax,%rcx), %rax
	movq	24(%rdi,%rax,8), %rax
	movq	%rax, 24(%rbp,%rcx,8)
	incq	%rsi
	cmpq	$4, %rsi
	jne	.LBB13_7
# BB#8:
	movl	%r13d, %r15d
	cmpl	$1, %r15d
	jne	.LBB13_11
# BB#9:                                 # %.preheader230
	incl	%r14d
	movslq	%r14d, %rax
	movq	8(%r10), %r11
	xorl	%esi, %esi
	imulq	$264, %rax, %r8         # imm = 0x108
	.p2align	4, 0x90
.LBB13_10:                              # =>This Inner Loop Header: Depth=1
	movl	172(%r9), %eax
	addl	%esi, %eax
	cltq
	movq	(%r11,%rax,8), %rdx
	movslq	%ecx, %rcx
	movq	rdopt(%rip), %rbp
	movl	1696(%rbp,%rsi,4), %ebp
	movl	%ebp, (%rdx,%rcx)
	movq	enc_picture(%rip), %rdx
	movq	6488(%rdx), %rcx
	movq	6496(%rdx), %r10
	movq	8(%rcx), %r11
	movq	(%r11,%rax,8), %rdi
	movq	img(%rip), %r9
	movslq	168(%r9), %rcx
	movsbq	(%rdi,%rcx), %rbx
	leaq	(%rdx,%r8), %rbp
	movq	24(%rbp,%rbx,8), %r14
	movq	8(%r10), %rbx
	movq	(%rbx,%rax,8), %rax
	movq	%r14, (%rax,%rcx,8)
	movsbq	1(%rdi,%rcx), %rbx
	movq	24(%rbp,%rbx,8), %rbx
	movq	%rbx, 8(%rax,%rcx,8)
	movsbq	2(%rdi,%rcx), %rbx
	movq	24(%rbp,%rbx,8), %rbx
	movq	%rbx, 16(%rax,%rcx,8)
	movsbq	3(%rdi,%rcx), %rdi
	movq	24(%rbp,%rdi,8), %rdi
	movq	%rdi, 24(%rax,%rcx,8)
	incq	%rsi
	cmpq	$4, %rsi
	jne	.LBB13_10
.LBB13_11:                              # %.preheader229.preheader
	movq	6440(%rdx), %rax
	movslq	180(%r9), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	176(%r9), %rcx
	movq	rdopt(%rip), %rdx
	movups	8(%rdx), %xmm0
	movups	24(%rdx), %xmm1
	movups	%xmm1, 16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	movl	$2, %eax
	movl	$72, %ecx
	jmp	.LBB13_12
	.p2align	4, 0x90
.LBB13_54:                              # %.preheader229..preheader229_crit_edge.1
                                        #   in Loop: Header=BB13_12 Depth=1
	movq	enc_picture(%rip), %rdx
	movq	img(%rip), %rsi
	movq	6440(%rdx), %rdx
	movl	180(%rsi), %edi
	addl	%eax, %edi
	movslq	%edi, %rdi
	movq	(%rdx,%rdi,8), %rdx
	movslq	176(%rsi), %rsi
	movq	rdopt(%rip), %rdi
	movups	(%rdi,%rcx), %xmm0
	movups	16(%rdi,%rcx), %xmm1
	movups	%xmm1, 16(%rdx,%rsi,2)
	movups	%xmm0, (%rdx,%rsi,2)
	addq	$64, %rcx
	addl	$2, %eax
.LBB13_12:                              # %.preheader229..preheader229_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rdx
	movq	img(%rip), %rsi
	movq	6440(%rdx), %rdx
	movl	180(%rsi), %edi
	leal	-1(%rax,%rdi), %edi
	movslq	%edi, %rdi
	movq	(%rdx,%rdi,8), %rdx
	movslq	176(%rsi), %rsi
	movq	rdopt(%rip), %rdi
	movups	-32(%rdi,%rcx), %xmm0
	movups	-16(%rdi,%rcx), %xmm1
	movups	%xmm1, 16(%rdx,%rsi,2)
	movups	%xmm0, (%rdx,%rsi,2)
	cmpq	$520, %rcx              # imm = 0x208
	jne	.LBB13_54
# BB#13:
	movq	img(%rip), %r8
	cmpl	$0, 15536(%r8)
	je	.LBB13_17
# BB#14:                                # %.preheader227
	cmpl	$0, 15548(%r8)
	jle	.LBB13_17
# BB#15:                                # %.lr.ph247.preheader
	xorl	%ebp, %ebp
	movl	$520, %ebx              # imm = 0x208
	.p2align	4, 0x90
.LBB13_16:                              # %.lr.ph247
                                        # =>This Inner Loop Header: Depth=1
	movq	enc_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	(%rax), %rax
	movl	188(%r8), %ecx
	addl	%ebp, %ecx
	movslq	%ecx, %rcx
	movslq	184(%r8), %rdi
	addq	%rdi, %rdi
	addq	(%rax,%rcx,8), %rdi
	movq	rdopt(%rip), %rsi
	addq	%rbx, %rsi
	movslq	15544(%r8), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	movq	enc_picture(%rip), %rax
	movq	6472(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movl	188(%rcx), %edx
	addl	%ebp, %edx
	movslq	%edx, %rdx
	movslq	184(%rcx), %rdi
	addq	%rdi, %rdi
	addq	(%rax,%rdx,8), %rdi
	movq	rdopt(%rip), %rax
	leaq	512(%rax,%rbx), %rsi
	movslq	15544(%rcx), %rdx
	addq	%rdx, %rdx
	callq	memcpy
	incq	%rbp
	movq	img(%rip), %r8
	movslq	15548(%r8), %rax
	addq	$32, %rbx
	cmpq	%rax, %rbp
	jl	.LBB13_16
.LBB13_17:                              # %.loopexit228
	movq	rdopt(%rip), %rax
	movups	1568(%rax), %xmm0
	movq	32(%rsp), %r13          # 8-byte Reload
	movups	%xmm0, 376(%r13,%r12)
	movups	1584(%rax), %xmm0
	movups	%xmm0, 392(%r13,%r12)
	movl	1720(%rax), %ecx
	movl	%ecx, 472(%r13,%r12)
	movl	24(%rsp), %ecx          # 4-byte Reload
	cmpl	$13, %ecx
	je	.LBB13_24
# BB#18:                                # %.loopexit228
	cmpl	$9, %ecx
	je	.LBB13_24
# BB#19:                                # %.loopexit228
	cmpl	$8, %ecx
	jne	.LBB13_22
# BB#20:                                # %.lr.ph243.preheader
	movups	1608(%rax), %xmm0
	movups	%xmm0, 332(%r13,%r12)
	movslq	172(%r8), %rcx
	movq	128(%r8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movslq	168(%r8), %rsi
	movq	1600(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movl	(%rax,%rsi), %eax
	movl	%eax, (%rdx,%rsi)
	movq	img(%rip), %r8
	movl	172(%r8), %eax
	addl	$3, %eax
	cmpl	%eax, %ecx
	jge	.LBB13_27
	.p2align	4, 0x90
.LBB13_21:                              # %.lr.ph243..lr.ph243_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	rdopt(%rip), %rax
	movq	128(%r8), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movslq	168(%r8), %rsi
	movq	1600(%rax), %rax
	movq	8(%rax,%rcx,8), %rax
	leaq	1(%rcx), %rcx
	movl	(%rax,%rsi), %eax
	movl	%eax, (%rdx,%rsi)
	movq	img(%rip), %r8
	movslq	172(%r8), %rax
	addq	$3, %rax
	cmpq	%rax, %rcx
	jl	.LBB13_21
	jmp	.LBB13_27
.LBB13_24:
	orl	$4, %ecx
	cmpl	$13, %ecx
	jne	.LBB13_27
# BB#25:                                # %.lr.ph245.preheader
	movups	1608(%rax), %xmm0
	movups	%xmm0, 332(%r13,%r12)
	movups	1624(%rax), %xmm0
	movups	%xmm0, 348(%r13,%r12)
	movslq	172(%r8), %rcx
	movq	128(%r8), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movslq	168(%r8), %rsi
	movq	1600(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movl	(%rax,%rsi), %eax
	movl	%eax, (%rdx,%rsi)
	movq	img(%rip), %r8
	movl	172(%r8), %eax
	addl	$3, %eax
	cmpl	%eax, %ecx
	jge	.LBB13_27
	.p2align	4, 0x90
.LBB13_26:                              # %.lr.ph245..lr.ph245_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	rdopt(%rip), %rax
	movq	128(%r8), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movslq	168(%r8), %rsi
	movq	1600(%rax), %rax
	movq	8(%rax,%rcx,8), %rax
	leaq	1(%rcx), %rcx
	movl	(%rax,%rsi), %eax
	movl	%eax, (%rdx,%rsi)
	movq	img(%rip), %r8
	movslq	172(%r8), %rax
	addq	$3, %rax
	cmpq	%rax, %rcx
	jl	.LBB13_26
	jmp	.LBB13_27
.LBB13_22:                              # %.lr.ph.preheader
	movaps	.LCPI13_0(%rip), %xmm0  # xmm0 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
	movups	%xmm0, 332(%r13,%r12)
	movslq	172(%r8), %rax
	decq	%rax
	.p2align	4, 0x90
.LBB13_23:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	128(%r8), %rcx
	movq	8(%rcx,%rax,8), %rcx
	movslq	168(%r8), %rdx
	movl	$33686018, (%rcx,%rdx)  # imm = 0x2020202
	movq	img(%rip), %r8
	movslq	172(%r8), %rcx
	addq	$3, %rcx
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB13_23
.LBB13_27:                              # %.loopexit224
	cmpl	$0, 15268(%r8)
	je	.LBB13_53
# BB#28:
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movq	rdopt(%rip), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movslq	32(%r8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_29:                              # %.preheader83.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_31 Depth 2
                                        #       Child Loop BB13_32 Depth 3
                                        #         Child Loop BB13_33 Depth 4
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB13_36
# BB#30:                                # %.preheader82.i.us.preheader
                                        #   in Loop: Header=BB13_29 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	1664(%rax), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	1672(%rax), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	14376(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	14384(%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB13_31:                              # %.preheader82.i.us
                                        #   Parent Loop BB13_29 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_32 Depth 3
                                        #         Child Loop BB13_33 Depth 4
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rsi
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rsi
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB13_32:                              # %.preheader.i.us
                                        #   Parent Loop BB13_29 Depth=1
                                        #     Parent Loop BB13_31 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB13_33 Depth 4
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r10,8), %r11
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r10,8), %r14
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r10,8), %r13
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r10,8), %r12
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r10,8), %r9
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r10,8), %rdx
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r10,8), %rdi
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r10,8), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_33:                              #   Parent Loop BB13_29 Depth=1
                                        #     Parent Loop BB13_31 Depth=2
                                        #       Parent Loop BB13_32 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r11,%rbp), %rbx
	movzwl	(%rbx), %r8d
	movq	(%r14,%rbp), %r15
	movw	%r8w, (%r15)
	movzwl	2(%rbx), %ebx
	movw	%bx, 2(%r15)
	movq	(%r13,%rbp), %rbx
	movzwl	(%rbx), %esi
	movq	(%r12,%rbp), %rcx
	movw	%si, (%rcx)
	movzwl	2(%rbx), %esi
	movw	%si, 2(%rcx)
	movq	(%r9,%rbp), %rcx
	movzwl	(%rcx), %esi
	movq	(%rdx,%rbp), %rbx
	movw	%si, (%rbx)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rbx)
	movq	(%rdi,%rbp), %rcx
	movzwl	(%rcx), %esi
	movq	(%rax,%rbp), %rbx
	movw	%si, (%rbx)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rbx)
	addq	$8, %rbp
	cmpq	$72, %rbp
	jne	.LBB13_33
# BB#34:                                #   in Loop: Header=BB13_32 Depth=3
	incq	%r10
	cmpq	48(%rsp), %r10          # 8-byte Folded Reload
	jne	.LBB13_32
# BB#35:                                # %._crit_edge.i.loopexit.us
                                        #   in Loop: Header=BB13_31 Depth=2
	movq	112(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	cmpq	$4, %rdx
	jne	.LBB13_31
.LBB13_36:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB13_29 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	$4, %rcx
	jne	.LBB13_29
# BB#37:                                # %copy_motion_vectors_MB.exit
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	addl	$-9, %eax
	cmpl	$6, %eax
	jae	.LBB13_38
# BB#50:                                # %switch.hole_check
	movl	$51, %ecx
	btl	%eax, %ecx
	jb	.LBB13_51
.LBB13_38:                              # %.preheader.preheader
	xorl	%eax, %eax
	xorl	%r11d, %r11d
	movl	12(%rsp), %r10d         # 4-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB13_39:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_40 Depth 2
	movl	%r11d, %r12d
	shrl	$31, %r12d
	addl	%r11d, %r12d
	andl	$-2, %r12d
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	xorl	%ebp, %ebp
	movl	%r12d, 32(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB13_40:                              #   Parent Loop BB13_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	addl	%r12d, %eax
	cltq
	movq	104(%rsp), %rcx         # 8-byte Reload
	movslq	376(%rcx,%rax,4), %r9
	movl	392(%rcx,%rax,4), %r14d
	cmpl	$1, %r14d
	jne	.LBB13_41
# BB#42:                                #   in Loop: Header=BB13_40 Depth=2
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %r15
	movq	(%r15), %rdx
	movl	168(%r8), %esi
	movl	172(%r8), %ecx
	leal	(%rcx,%r11), %eax
	cltq
	movq	(%rdx,%rax,8), %rax
	leal	(%rbp,%rsi), %ebx
	movslq	%ebx, %rbx
	movq	(%rax,%rbx,8), %rax
	movw	$0, (%rax)
	xorl	%ebx, %ebx
	jmp	.LBB13_43
	.p2align	4, 0x90
.LBB13_41:                              #   in Loop: Header=BB13_40 Depth=2
	movq	rdopt(%rip), %rbx
	movq	1672(%rbx), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax), %r13
	movsbq	1680(%rbx,%rdi), %rcx
	movq	(%r13,%rcx,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movzwl	(%rcx), %r12d
	movq	enc_picture(%rip), %rcx
	movq	6512(%rcx), %r15
	movq	(%r15), %rdx
	movl	168(%r8), %esi
	movl	172(%r8), %ecx
	leal	(%rcx,%r11), %eax
	cltq
	movq	(%rdx,%rax,8), %r10
	leal	(%rbp,%rsi), %r8d
	movslq	%r8d, %rax
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	(%r10,%rax,8), %rax
	movw	%r12w, (%rax)
	movl	32(%rsp), %r12d         # 4-byte Reload
	movl	12(%rsp), %r10d         # 4-byte Reload
	movsbq	1680(%rbx,%rdi), %rax
	movq	(%r13,%rax,8), %rax
	movq	(%rax,%r9,8), %rax
	movw	2(%rax), %bx
.LBB13_43:                              #   in Loop: Header=BB13_40 Depth=2
	addl	%r11d, %ecx
	movslq	%ecx, %rcx
	movq	(%rdx,%rcx,8), %rax
	addl	%ebp, %esi
	movslq	%esi, %rdx
	movq	(%rax,%rdx,8), %rax
	movw	%bx, 2(%rax)
	cmpl	$1, %r10d
	jne	.LBB13_48
# BB#44:                                #   in Loop: Header=BB13_40 Depth=2
	testl	%r14d, %r14d
	je	.LBB13_46
# BB#45:                                #   in Loop: Header=BB13_40 Depth=2
	movq	rdopt(%rip), %rsi
	movq	1672(%rsi), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%rbp,8), %rax
	movq	8(%rax), %rbx
	movsbq	1696(%rsi,%rdi), %rax
	movq	(%rbx,%rax,8), %rax
	movq	(%rax,%r9,8), %rax
	movzwl	(%rax), %r8d
	movq	8(%r15), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rdx,8), %rax
	movw	%r8w, (%rax)
	movq	16(%rsp), %r8           # 8-byte Reload
	movsbq	1696(%rsi,%rdi), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movw	2(%rcx), %cx
	jmp	.LBB13_47
.LBB13_46:                              #   in Loop: Header=BB13_40 Depth=2
	movq	8(%r15), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rdx,8), %rax
	movw	$0, (%rax)
	xorl	%ecx, %ecx
.LBB13_47:                              #   in Loop: Header=BB13_40 Depth=2
	movw	%cx, 2(%rax)
.LBB13_48:                              #   in Loop: Header=BB13_40 Depth=2
	incq	%rbp
	incq	%rdi
	cmpq	$4, %rbp
	jne	.LBB13_40
# BB#49:                                #   in Loop: Header=BB13_39 Depth=1
	incq	%r11
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$4, %rax
	cmpq	$4, %r11
	jne	.LBB13_39
.LBB13_53:                              # %.loopexit
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_51:                              # %switch.lookup.preheader
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movslq	172(%rdx), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	168(%rdx), %rcx
	movq	(%rax,%rcx,8), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	movq	8(%rax,%rdx,8), %rax
	movslq	168(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	movups	%xmm0, (%rax)
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	movq	16(%rax,%rdx,8), %rax
	movslq	168(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	movups	%xmm0, (%rax)
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	movq	24(%rax,%rdx,8), %rax
	movslq	168(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	movups	%xmm0, (%rax)
	cmpl	$1, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB13_53
# BB#52:                                # %.preheader222.preheader
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	168(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	movups	%xmm0, (%rax)
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	movq	8(%rax,%rdx,8), %rax
	movslq	168(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	movups	%xmm0, (%rax)
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	movq	16(%rax,%rdx,8), %rax
	movslq	168(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	movups	%xmm0, (%rax)
	movq	enc_picture(%rip), %rax
	movq	6512(%rax), %rax
	movq	8(%rax), %rax
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	movq	24(%rax,%rdx,8), %rax
	movslq	168(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	movups	%xmm0, (%rax)
	jmp	.LBB13_53
.Lfunc_end13:
	.size	copy_rdopt_data, .Lfunc_end13-copy_rdopt_data
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.text
	.globl	buf2img
	.p2align	4, 0x90
	.type	buf2img,@function
buf2img:                                # @buf2img
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi187:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi188:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi189:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi190:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi191:
	.cfi_def_cfa_offset 208
.Lcfi192:
	.cfi_offset %rbx, -56
.Lcfi193:
	.cfi_offset %r12, -48
.Lcfi194:
	.cfi_offset %r13, -40
.Lcfi195:
	.cfi_offset %r14, -32
.Lcfi196:
	.cfi_offset %r15, -24
.Lcfi197:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movl	%edx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	cmpl	$3, %ebx
	jb	.LBB14_2
# BB#1:
	movl	$.L.str.3, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB14_2:
	callq	testEndian
	testl	%eax, %eax
	je	.LBB14_24
# BB#3:
	cmpl	$1, %ebx
	je	.LBB14_35
# BB#4:
	cmpl	$2, %ebx
	je	.LBB14_54
# BB#5:
	cmpl	$4, %ebx
	jne	.LBB14_23
# BB#6:                                 # %.preheader93
	testl	%ebp, %ebp
	jle	.LBB14_23
# BB#7:                                 # %.preheader93
	testl	%r15d, %r15d
	jle	.LBB14_23
# BB#8:                                 # %.preheader92.us.preheader
	movl	%r15d, %r13d
	movl	%ebp, %r9d
	leal	(,%r15,4), %r12d
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%r13,4), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	-1(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r15d, %eax
	andl	$3, %eax
	movq	%r13, %r14
	movq	%rax, 32(%rsp)          # 8-byte Spill
	subq	%rax, %r14
	movl	$1, %r11d
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movdqa	.LCPI14_0(%rip), %xmm1  # xmm1 = [0,255,0,0,0,0,0,0,0,255,0,0,0,0,0,0]
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.LBB14_12
.LBB14_9:                               # %vector.body.preheader
                                        #   in Loop: Header=BB14_12 Depth=1
	movq	%r14, %rax
	movl	%r10d, %edx
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB14_10:                              # %vector.body
                                        #   Parent Loop BB14_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movdqu	(%rdi,%rdx), %xmm2
	movdqa	%xmm2, %xmm3
	punpckhdq	%xmm0, %xmm3    # xmm3 = xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movdqa	%xmm2, %xmm4
	psrlq	$8, %xmm4
	movdqa	%xmm3, %xmm5
	psrlq	$8, %xmm5
	pand	%xmm1, %xmm5
	pand	%xmm1, %xmm4
	psrlq	$24, %xmm3
	psrlq	$24, %xmm2
	por	%xmm4, %xmm2
	por	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3,4,5,6,7]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movq	%xmm2, (%rbx)
	addq	$8, %rbx
	addl	$16, %edx
	addq	$-4, %rax
	jne	.LBB14_10
# BB#11:                                # %middle.block
                                        #   in Loop: Header=BB14_12 Depth=1
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movq	%r14, %rax
	movl	%r14d, %edx
	jne	.LBB14_17
	jmp	.LBB14_22
	.p2align	4, 0x90
.LBB14_12:                              # %.preheader92.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_10 Depth 2
                                        #     Child Loop BB14_21 Depth 2
	cmpl	$4, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %r8
	jb	.LBB14_16
# BB#13:                                # %min.iters.checked
                                        #   in Loop: Header=BB14_12 Depth=1
	testq	%r14, %r14
	je	.LBB14_16
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB14_12 Depth=1
	movl	%r12d, %eax
	imull	%ecx, %eax
	cltq
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax), %rdx
	cmpq	%rdx, %r8
	movq	(%rsp), %rdi            # 8-byte Reload
	jae	.LBB14_9
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB14_12 Depth=1
	addq	%rdi, %rax
	leaq	(%r8,%r13,2), %rdx
	cmpq	%rdx, %rax
	jae	.LBB14_9
	.p2align	4, 0x90
.LBB14_16:                              #   in Loop: Header=BB14_12 Depth=1
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB14_17:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB14_12 Depth=1
	movl	%r13d, %ebx
	subl	%eax, %ebx
	testb	$1, %bl
	jne	.LBB14_19
# BB#18:                                #   in Loop: Header=BB14_12 Depth=1
	movq	%rax, %rbx
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB14_20
	jmp	.LBB14_22
	.p2align	4, 0x90
.LBB14_19:                              # %scalar.ph.prol
                                        #   in Loop: Header=BB14_12 Depth=1
	movl	%ecx, %ebx
	imull	%r15d, %ebx
	addl	%edx, %ebx
	movslq	%ebx, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	(%rdi,%rbx,4), %ebx
	movq	%r9, %rdi
	movl	%ebx, %r9d
	shrl	$8, %r9d
	andl	$65280, %r9d            # imm = 0xFF00
	shrq	$24, %rbx
	orl	%r9d, %ebx
	movq	%rdi, %r9
	movw	%bx, (%r8,%rax,2)
	leaq	1(%rax), %rbx
	incl	%edx
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	je	.LBB14_22
.LBB14_20:                              # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB14_12 Depth=1
	movq	%r13, %rax
	subq	%rbx, %rax
	leaq	2(%r8,%rbx,2), %rbx
	leal	(%rdx,%r11), %edi
	shll	$2, %edi
	movslq	%edi, %r8
	movq	(%rsp), %rdi            # 8-byte Reload
	addq	%rdi, %r8
	addl	%esi, %edx
	shll	$2, %edx
	movslq	%edx, %rdx
	addq	%rdi, %rdx
	.p2align	4, 0x90
.LBB14_21:                              # %scalar.ph
                                        #   Parent Loop BB14_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %edi
	movl	%edi, %ebp
	shrl	$8, %ebp
	andl	$65280, %ebp            # imm = 0xFF00
	shrq	$24, %rdi
	orl	%ebp, %edi
	movw	%di, -2(%rbx)
	movl	(%r8), %edi
	movl	%edi, %ebp
	shrl	$8, %ebp
	andl	$65280, %ebp            # imm = 0xFF00
	shrq	$24, %rdi
	orl	%ebp, %edi
	movw	%di, (%rbx)
	addq	$4, %rbx
	addq	$8, %r8
	addq	$8, %rdx
	addq	$-2, %rax
	jne	.LBB14_21
.LBB14_22:                              # %._crit_edge108.us
                                        #   in Loop: Header=BB14_12 Depth=1
	incq	%rcx
	addl	%r12d, %r10d
	addl	%r15d, %r11d
	addl	%r15d, %esi
	cmpq	%r9, %rcx
	jne	.LBB14_12
.LBB14_23:                              # %.loopexit94
	movl	$.L.str.4, %edi
	movl	$500, %esi              # imm = 0x1F4
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	error                   # TAILCALL
.LBB14_24:                              # %.preheader85
	testl	%ebp, %ebp
	jle	.LBB14_71
# BB#25:                                # %.preheader85
	testl	%r15d, %r15d
	jle	.LBB14_71
# BB#26:                                # %.preheader.us.preheader
	movslq	%ebx, %rsi
	movslq	%r15d, %rax
	movl	%r15d, %ecx
	movl	%ebp, %edx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	leaq	-1(%rcx), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%ecx, %r12d
	andl	$3, %r12d
	movq	%rsi, %rcx
	movq	%rax, 112(%rsp)         # 8-byte Spill
	imulq	%rax, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	(,%rsi,4), %rax
	movl	$3, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	$2, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	$1, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	(%rsp), %rbp            # 8-byte Reload
	xorl	%r13d, %r13d
	movq	%r12, 88(%rsp)          # 8-byte Spill
	movq	%rax, 128(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB14_27:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_29 Depth 2
                                        #     Child Loop BB14_33 Depth 2
	testq	%r12, %r12
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	je	.LBB14_30
# BB#28:                                # %.prol.preheader
                                        #   in Loop: Header=BB14_27 Depth=1
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	.p2align	4, 0x90
.LBB14_29:                              #   Parent Loop BB14_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r13,8), %rdi
	movw	$0, (%rdi,%rbx)
	addq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	memcpy
	movq	8(%rsp), %rdx           # 8-byte Reload
	incq	%r14
	addq	%rdx, %rbp
	addq	$2, %rbx
	cmpq	%r14, %r12
	jne	.LBB14_29
	jmp	.LBB14_31
	.p2align	4, 0x90
.LBB14_30:                              #   in Loop: Header=BB14_27 Depth=1
	xorl	%r14d, %r14d
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB14_31:                              # %.prol.loopexit
                                        #   in Loop: Header=BB14_27 Depth=1
	cmpq	$3, 96(%rsp)            # 8-byte Folded Reload
	movq	%r15, %rbx
	jb	.LBB14_34
# BB#32:                                # %.preheader.us.new
                                        #   in Loop: Header=BB14_27 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	subq	%r14, %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%r14,%rax), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	imulq	%rdx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	6(%r14,%r14), %r15
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%r14,%rax), %rax
	imulq	%rdx, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%r14,%rax), %rax
	imulq	%rdx, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	addq	40(%rsp), %r14          # 8-byte Folded Reload
	imulq	%rdx, %r14
	movq	(%rsp), %r12            # 8-byte Reload
	.p2align	4, 0x90
.LBB14_33:                              #   Parent Loop BB14_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	(%rbx,%r13,8), %rax
	leaq	-6(%rax,%r15), %rdi
	movw	$0, -6(%rax,%r15)
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%r12,%rax), %rsi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdx
	callq	memcpy
	movq	(%rbx,%r13,8), %rax
	leaq	-4(%rax,%r15), %rdi
	movw	$0, -4(%rax,%r15)
	leaq	(%r12,%r14), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	(%rbx,%r13,8), %rax
	leaq	-2(%rax,%r15), %rdi
	movw	$0, -2(%rax,%r15)
	movq	144(%rsp), %rax         # 8-byte Reload
	leaq	(%r12,%rax), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	(%rbx,%r13,8), %rdi
	movw	$0, (%rdi,%r15)
	addq	%r15, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r12,%rax), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	128(%rsp), %rdi         # 8-byte Reload
	addq	%rdi, %r12
	addq	$8, %r15
	addq	$-4, %rcx
	jne	.LBB14_33
.LBB14_34:                              # %._crit_edge.us
                                        #   in Loop: Header=BB14_27 Depth=1
	incq	%r13
	movq	120(%rsp), %rbp         # 8-byte Reload
	addq	80(%rsp), %rbp          # 8-byte Folded Reload
	movq	112(%rsp), %rax         # 8-byte Reload
	addq	%rax, 64(%rsp)          # 8-byte Folded Spill
	addq	%rax, 56(%rsp)          # 8-byte Folded Spill
	addq	%rax, 48(%rsp)          # 8-byte Folded Spill
	addq	%rax, 40(%rsp)          # 8-byte Folded Spill
	cmpq	104(%rsp), %r13         # 8-byte Folded Reload
	movq	88(%rsp), %r12          # 8-byte Reload
	jne	.LBB14_27
	jmp	.LBB14_71
.LBB14_35:                              # %.preheader87
	testl	%ebp, %ebp
	jle	.LBB14_71
# BB#36:                                # %.preheader87
	testl	%r15d, %r15d
	jle	.LBB14_71
# BB#37:                                # %.preheader86.us.preheader
	movslq	%r15d, %r14
	movl	%r15d, %ebx
	movl	%ebp, %r11d
	leaq	-1(%rbx), %r10
	movl	%r15d, %eax
	andl	$15, %eax
	movq	%rbx, %r9
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subq	%rax, %r9
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	8(%rax), %r12
	xorl	%r13d, %r13d
	pxor	%xmm0, %xmm0
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB14_38:                              # %.preheader86.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_46 Depth 2
                                        #     Child Loop BB14_50 Depth 2
                                        #     Child Loop BB14_52 Depth 2
	cmpl	$16, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rsi
	jae	.LBB14_40
# BB#39:                                #   in Loop: Header=BB14_38 Depth=1
	xorl	%edx, %edx
	jmp	.LBB14_48
	.p2align	4, 0x90
.LBB14_40:                              # %min.iters.checked210
                                        #   in Loop: Header=BB14_38 Depth=1
	testq	%r9, %r9
	je	.LBB14_44
# BB#41:                                # %vector.memcheck224
                                        #   in Loop: Header=BB14_38 Depth=1
	movq	%r14, %rax
	imulq	%r13, %rax
	leaq	(%rbx,%rax), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	addq	%rdx, %rcx
	cmpq	%rcx, %rsi
	jae	.LBB14_45
# BB#42:                                # %vector.memcheck224
                                        #   in Loop: Header=BB14_38 Depth=1
	addq	%rdx, %rax
	leaq	(%rsi,%rbx,2), %rcx
	cmpq	%rcx, %rax
	jae	.LBB14_45
.LBB14_44:                              #   in Loop: Header=BB14_38 Depth=1
	xorl	%edx, %edx
	jmp	.LBB14_48
.LBB14_45:                              # %vector.body206.preheader
                                        #   in Loop: Header=BB14_38 Depth=1
	leaq	16(%rsi), %rax
	movq	%r9, %rcx
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB14_46:                              # %vector.body206
                                        #   Parent Loop BB14_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	movq	(%rdx), %xmm2           # xmm2 = mem[0],zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	movdqu	%xmm1, -16(%rax)
	movdqu	%xmm2, (%rax)
	addq	$16, %rdx
	addq	$32, %rax
	addq	$-16, %rcx
	jne	.LBB14_46
# BB#47:                                # %middle.block207
                                        #   in Loop: Header=BB14_38 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	%r9, %rdx
	je	.LBB14_53
	.p2align	4, 0x90
.LBB14_48:                              # %scalar.ph208.preheader
                                        #   in Loop: Header=BB14_38 Depth=1
	movl	%ebx, %ecx
	subl	%edx, %ecx
	movq	%r10, %rax
	subq	%rdx, %rax
	andq	$3, %rcx
	je	.LBB14_51
# BB#49:                                # %scalar.ph208.prol.preheader
                                        #   in Loop: Header=BB14_38 Depth=1
	leaq	(%rbp,%rdx), %rdi
	negq	%rcx
	.p2align	4, 0x90
.LBB14_50:                              # %scalar.ph208.prol
                                        #   Parent Loop BB14_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi), %r8d
	movw	%r8w, (%rsi,%rdx,2)
	incq	%rdx
	incq	%rdi
	incq	%rcx
	jne	.LBB14_50
.LBB14_51:                              # %scalar.ph208.prol.loopexit
                                        #   in Loop: Header=BB14_38 Depth=1
	cmpq	$3, %rax
	jb	.LBB14_53
	.p2align	4, 0x90
.LBB14_52:                              # %scalar.ph208
                                        #   Parent Loop BB14_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp,%rdx), %eax
	movw	%ax, (%rsi,%rdx,2)
	movzbl	1(%rbp,%rdx), %eax
	movw	%ax, 2(%rsi,%rdx,2)
	movzbl	2(%rbp,%rdx), %eax
	movw	%ax, 4(%rsi,%rdx,2)
	movzbl	3(%rbp,%rdx), %eax
	movw	%ax, 6(%rsi,%rdx,2)
	addq	$4, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB14_52
.LBB14_53:                              # %._crit_edge99.us
                                        #   in Loop: Header=BB14_38 Depth=1
	incq	%r13
	addq	%r14, %r12
	addq	%r14, %rbp
	cmpq	%r11, %r13
	jne	.LBB14_38
	jmp	.LBB14_71
.LBB14_54:                              # %.preheader90
	testl	%ebp, %ebp
	jle	.LBB14_71
# BB#55:                                # %.preheader90
	testl	%r15d, %r15d
	jle	.LBB14_71
# BB#56:                                # %.preheader89.us.preheader
	movl	%r15d, %r13d
	movl	%ebp, %r11d
	leal	(%r15,%r15), %r12d
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%r13,2), %r8
	leaq	-1(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r15d, %eax
	andl	$15, %eax
	movq	%r13, %r14
	movq	%rax, 24(%rsp)          # 8-byte Spill
	subq	%rax, %r14
	movl	$1, %r10d
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	jmp	.LBB14_60
.LBB14_57:                              # %vector.body167.preheader
                                        #   in Loop: Header=BB14_60 Depth=1
	movq	%r8, %rbp
	leaq	16(%rbx), %rax
	movq	%r14, %rdx
	movl	%r9d, %r8d
	.p2align	4, 0x90
.LBB14_58:                              # %vector.body167
                                        #   Parent Loop BB14_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r8d, %r8
	movdqu	(%rdi,%r8), %xmm1
	movdqu	16(%rdi,%r8), %xmm2
	movdqa	%xmm1, %xmm3
	punpckhbw	%xmm0, %xmm3    # xmm3 = xmm3[8],xmm0[8],xmm3[9],xmm0[9],xmm3[10],xmm0[10],xmm3[11],xmm0[11],xmm3[12],xmm0[12],xmm3[13],xmm0[13],xmm3[14],xmm0[14],xmm3[15],xmm0[15]
	pshuflw	$177, %xmm3, %xmm3      # xmm3 = xmm3[1,0,3,2,4,5,6,7]
	pshufhw	$177, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,5,4,7,6]
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	pshufhw	$177, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,5,4,7,6]
	packuswb	%xmm3, %xmm1
	movdqa	%xmm2, %xmm3
	punpckhbw	%xmm0, %xmm3    # xmm3 = xmm3[8],xmm0[8],xmm3[9],xmm0[9],xmm3[10],xmm0[10],xmm3[11],xmm0[11],xmm3[12],xmm0[12],xmm3[13],xmm0[13],xmm3[14],xmm0[14],xmm3[15],xmm0[15]
	pshuflw	$177, %xmm3, %xmm3      # xmm3 = xmm3[1,0,3,2,4,5,6,7]
	pshufhw	$177, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,5,4,7,6]
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	pshuflw	$177, %xmm2, %xmm2      # xmm2 = xmm2[1,0,3,2,4,5,6,7]
	pshufhw	$177, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,5,4,7,6]
	packuswb	%xmm3, %xmm2
	movdqu	%xmm1, -16(%rax)
	movdqu	%xmm2, (%rax)
	addq	$32, %rax
	addl	$32, %r8d
	addq	$-16, %rdx
	jne	.LBB14_58
# BB#59:                                # %middle.block168
                                        #   in Loop: Header=BB14_60 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	%r14, %rax
	movl	%r14d, %edx
	movq	%rbp, %r8
	jne	.LBB14_65
	jmp	.LBB14_70
	.p2align	4, 0x90
.LBB14_60:                              # %.preheader89.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_58 Depth 2
                                        #     Child Loop BB14_69 Depth 2
	cmpl	$16, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %rbx
	jb	.LBB14_64
# BB#61:                                # %min.iters.checked171
                                        #   in Loop: Header=BB14_60 Depth=1
	testq	%r14, %r14
	je	.LBB14_64
# BB#62:                                # %vector.memcheck186
                                        #   in Loop: Header=BB14_60 Depth=1
	movl	%r12d, %eax
	imull	%ecx, %eax
	cltq
	leaq	(%r8,%rax), %rdx
	cmpq	%rdx, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
	jae	.LBB14_57
# BB#63:                                # %vector.memcheck186
                                        #   in Loop: Header=BB14_60 Depth=1
	addq	%rdi, %rax
	leaq	(%rbx,%r13,2), %rdx
	cmpq	%rdx, %rax
	jae	.LBB14_57
	.p2align	4, 0x90
.LBB14_64:                              #   in Loop: Header=BB14_60 Depth=1
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB14_65:                              # %scalar.ph169.preheader
                                        #   in Loop: Header=BB14_60 Depth=1
	movl	%r13d, %edi
	subl	%eax, %edi
	testb	$1, %dil
	jne	.LBB14_67
# BB#66:                                #   in Loop: Header=BB14_60 Depth=1
	movq	%rax, %rdi
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	jne	.LBB14_68
	jmp	.LBB14_70
	.p2align	4, 0x90
.LBB14_67:                              # %scalar.ph169.prol
                                        #   in Loop: Header=BB14_60 Depth=1
	movl	%ecx, %edi
	imull	%r15d, %edi
	addl	%edx, %edi
	movslq	%edi, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movzwl	(%rbp,%rdi,2), %edi
	rolw	$8, %di
	movw	%di, (%rbx,%rax,2)
	leaq	1(%rax), %rdi
	incl	%edx
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	je	.LBB14_70
.LBB14_68:                              # %scalar.ph169.preheader.new
                                        #   in Loop: Header=BB14_60 Depth=1
	movq	%r13, %rax
	subq	%rdi, %rax
	leaq	2(%rbx,%rdi,2), %rbx
	leal	(%rdx,%r10), %edi
	addl	%edi, %edi
	movslq	%edi, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	addq	%rbp, %rdi
	addl	%esi, %edx
	addl	%edx, %edx
	movslq	%edx, %rdx
	addq	%rbp, %rdx
	.p2align	4, 0x90
.LBB14_69:                              # %scalar.ph169
                                        #   Parent Loop BB14_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdx), %ebp
	rolw	$8, %bp
	movw	%bp, -2(%rbx)
	movzwl	(%rdi), %ebp
	rolw	$8, %bp
	movw	%bp, (%rbx)
	addq	$4, %rbx
	addq	$4, %rdi
	addq	$4, %rdx
	addq	$-2, %rax
	jne	.LBB14_69
.LBB14_70:                              # %._crit_edge103.us
                                        #   in Loop: Header=BB14_60 Depth=1
	incq	%rcx
	addl	%r12d, %r9d
	addl	%r15d, %r10d
	addl	%r15d, %esi
	cmpq	%r11, %rcx
	jne	.LBB14_60
.LBB14_71:                              # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	buf2img, .Lfunc_end14-buf2img
	.cfi_endproc

	.type	enc_picture,@object     # @enc_picture
	.comm	enc_picture,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	encode_one_frame.prev_frame_no,@object # @encode_one_frame.prev_frame_no
	.local	encode_one_frame.prev_frame_no
	.comm	encode_one_frame.prev_frame_no,4,4
	.type	encode_one_frame.consecutive_non_reference_pictures,@object # @encode_one_frame.consecutive_non_reference_pictures
	.local	encode_one_frame.consecutive_non_reference_pictures
	.comm	encode_one_frame.consecutive_non_reference_pictures,4,4
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	enc_frame_picture,@object # @enc_frame_picture
	.comm	enc_frame_picture,8,8
	.type	enc_frame_picture2,@object # @enc_frame_picture2
	.comm	enc_frame_picture2,8,8
	.type	enc_frame_picture3,@object # @enc_frame_picture3
	.comm	enc_frame_picture3,8,8
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	quadratic_RC_init,@object # @quadratic_RC_init
	.comm	quadratic_RC_init,8,8
	.type	quadratic_RC,@object    # @quadratic_RC
	.comm	quadratic_RC,8,8
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	updateQP,@object        # @updateQP
	.comm	updateQP,8,8
	.type	QP,@object              # @QP
	.comm	QP,4,4
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	enc_bottom_picture,@object # @enc_bottom_picture
	.comm	enc_bottom_picture,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"POC type 2 cannot be applied for the coding pattern where the encoding /decoding order of pictures are different from the output order.\n"
	.size	.L.str, 137

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Completed Encoding Frame %05d.\r"
	.size	.L.str.1, 32

	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	enc_top_picture,@object # @enc_top_picture
	.comm	enc_top_picture,8,8
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"alloc_storable_picture: s->imgY_sub"
	.size	.L.str.2, 36

	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Source picture has higher bit depth than imgpel data type. Please recompile with larger data type for imgpel."
	.size	.L.str.3, 110

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"reading only from formats of 8, 16 or 32 bit allowed on big endian architecture"
	.size	.L.str.4, 80

	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"wb"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Fatal: cannot open SP output file '%s', exit (-1)\n"
	.size	.L.str.6, 51

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"ab"
	.size	.L.str.7, 3

	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"rb"
	.size	.L.str.8, 3

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Fatal: cannot open SP input file '%s', exit (-1)\n"
	.size	.L.str.9, 50

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	height_pad,@object      # @height_pad
	.comm	height_pad,4,4
	.type	width_pad,@object       # @width_pad
	.comm	width_pad,4,4
	.type	height_pad_cr,@object   # @height_pad_cr
	.comm	height_pad_cr,4,4
	.type	width_pad_cr,@object    # @width_pad_cr
	.comm	width_pad_cr,4,4
	.type	seiHasBufferingPeriod_info,@object # @seiHasBufferingPeriod_info
	.comm	seiHasBufferingPeriod_info,4,4
	.type	seiBufferingPeriod,@object # @seiBufferingPeriod
	.comm	seiBufferingPeriod,280,8
	.type	seiHasPicTiming_info,@object # @seiHasPicTiming_info
	.comm	seiHasPicTiming_info,4,4
	.type	seiPicTiming,@object    # @seiPicTiming
	.comm	seiPicTiming,152,8
	.type	WriteNALU,@object       # @WriteNALU
	.comm	WriteNALU,8,8
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	quadratic_RC_best,@object # @quadratic_RC_best
	.comm	quadratic_RC_best,8,8
	.type	last_P_no,@object       # @last_P_no
	.comm	last_P_no,8,8
	.type	last_P_no_frm,@object   # @last_P_no_frm
	.comm	last_P_no_frm,8,8
	.type	last_P_no_fld,@object   # @last_P_no_fld
	.comm	last_P_no_fld,8,8
	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%04d(NVB)%8d \n"
	.size	.L.str.12, 15

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%04d(IDR)%8d   %2d %7.3f %7.3f %7.3f %9d %7d    %3s    %d\n"
	.size	.L.str.13, 59

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"FLD"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"FRM"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%04d(IDR)%8d %1d %2d %7.3f %7.3f %7.3f %9d %7d    %3s %5d   %2d %2d  %d   %d\n"
	.size	.L.str.16, 78

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%04d(I)  %8d   %2d %7.3f %7.3f %7.3f %9d %7d    %3s    %d\n"
	.size	.L.str.17, 59

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%04d(I)  %8d %1d %2d %7.3f %7.3f %7.3f %9d %7d    %3s %5d   %2d %2d  %d   %d\n"
	.size	.L.str.18, 78

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%04d(SP) %8d   %2d %7.3f %7.3f %7.3f %9d %7d    %3s    %d\n"
	.size	.L.str.19, 59

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%04d(SP) %8d %1d %2d %7.3f %7.3f %7.3f %9d %7d    %3s %5d   %2d %2d  %d   %d\n"
	.size	.L.str.20, 78

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%04d(B)  %8d   %2d %7.3f %7.3f %7.3f %9d %7d    %3s    %d\n"
	.size	.L.str.21, 59

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%04d(B)  %8d %1d %2d %7.3f %7.3f %7.3f %9d %7d    %3s %5d %1d %2d %2d  %d   %d\n"
	.size	.L.str.22, 80

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"%04d(P)  %8d   %2d %7.3f %7.3f %7.3f %9d %7d    %3s    %d\n"
	.size	.L.str.23, 59

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"    (R)  %8d   %2d %7.3f %7.3f %7.3f %9d %7d    %3s    %d\n"
	.size	.L.str.24, 59

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"%04d(P)  %8d %1d %2d %7.3f %7.3f %7.3f %9d %7d    %3s %5d   %2d %2d  %d   %d\n"
	.size	.L.str.25, 78

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"ReadOneFrame: buf"
	.size	.L.str.26, 18

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"ReadOneFrame: cannot fseek to (Header size) in p_in"
	.size	.L.str.27, 52

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"ReadOneFrame: cannot advance file pointer in p_in beyond frame %d\n"
	.size	.L.str.28, 67

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"ReadOneFrame: cannot read %d bytes from input file, unexpected EOF?, exiting"
	.size	.L.str.29, 77

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"ReadOneFrame (NOT IMPLEMENTED): pic unit size on disk must be divided by 8"
	.size	.L.str.30, 75

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"Fatal: cannot read in SP input file, exit (-1)"
	.size	.Lstr.1, 47

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Fatal: cannot seek in SP input file, exit (-1)"
	.size	.Lstr.2, 47


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
