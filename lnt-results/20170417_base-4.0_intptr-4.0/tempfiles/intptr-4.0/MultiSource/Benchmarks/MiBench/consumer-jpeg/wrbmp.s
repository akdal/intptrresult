	.text
	.file	"wrbmp.bc"
	.globl	jinit_write_bmp
	.p2align	4, 0x90
	.type	jinit_write_bmp,@function
jinit_write_bmp:                        # @jinit_write_bmp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$80, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	$start_output_bmp, (%r14)
	movq	$finish_output_bmp, 16(%r14)
	movl	%ebp, 48(%r14)
	movl	56(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB0_3
# BB#1:
	cmpl	$1, %eax
	jne	.LBB0_4
# BB#2:
	movq	$put_gray_rows, 8(%r14)
	jmp	.LBB0_5
.LBB0_3:
	cmpl	$0, 100(%rbx)
	movl	$put_gray_rows, %eax
	movl	$put_pixel_rows, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 8(%r14)
	jmp	.LBB0_5
.LBB0_4:
	movq	(%rbx), %rax
	movl	$1005, 40(%rax)         # imm = 0x3ED
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_5:
	movq	%rbx, %rdi
	callq	jpeg_calc_output_dimensions
	movl	140(%rbx), %ebp
	imull	128(%rbx), %ebp
	movl	%ebp, 64(%r14)
	decl	%ebp
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	incl	%eax
	testb	$3, %bpl
	jne	.LBB0_6
# BB#7:
	movl	%ebp, 68(%r14)
	movl	%eax, 72(%r14)
	movq	8(%rbx), %rax
	movl	132(%rbx), %r8d
	movl	$1, %esi
	xorl	%edx, %edx
	movl	$1, %r9d
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	callq	*32(%rax)
	movq	%rax, 56(%r14)
	movl	$0, 76(%r14)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_9
# BB#8:
	incl	36(%rax)
.LBB0_9:
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$1, %ecx
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	*16(%rax)
	movq	%rax, 32(%r14)
	movl	$1, 40(%r14)
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_write_bmp, .Lfunc_end0-jinit_write_bmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_output_bmp,@function
start_output_bmp:                       # @start_output_bmp
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	start_output_bmp, .Lfunc_end1-start_output_bmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_output_bmp,@function
finish_output_bmp:                      # @finish_output_bmp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 128
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movq	24(%rbp), %r13
	movq	16(%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpl	$0, 48(%rbp)
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	je	.LBB2_12
# BB#1:
	cmpl	$2, 56(%r14)
	jne	.LBB2_2
# BB#3:
	cmpl	$0, 100(%r14)
	movb	$24, %sil
	je	.LBB2_5
# BB#4:
	movb	$8, %sil
.LBB2_5:
	setne	%al
	movzbl	%al, %ebx
	shll	$8, %ebx
	jmp	.LBB2_6
.LBB2_12:
	cmpl	$2, 56(%r14)
	jne	.LBB2_13
# BB#14:
	cmpl	$0, 100(%r14)
	movb	$24, %al
	je	.LBB2_16
# BB#15:
	movb	$8, %al
.LBB2_16:
	setne	%cl
	movzbl	%cl, %ebx
	shll	$8, %ebx
	jmp	.LBB2_17
.LBB2_2:
	movl	$256, %ebx              # imm = 0x100
	movb	$8, %sil
.LBB2_6:
	leal	(%rbx,%rbx,2), %edx
	movl	%edx, %edi
	orl	$26, %edi
	movl	68(%rbp), %eax
	movl	132(%r14), %ecx
	imulq	%rcx, %rax
	addq	%rdi, %rax
	movl	$0, 8(%rsp)
	movw	$0, 18(%rsp)
	movb	$66, 2(%rsp)
	movb	$77, 3(%rsp)
	movb	%al, 4(%rsp)
	movb	%ah, 5(%rsp)  # NOREX
	movq	%rax, %rdi
	shrq	$16, %rdi
	movb	%dil, 6(%rsp)
	shrq	$24, %rax
	movb	%al, 7(%rsp)
	movb	$26, 12(%rsp)
	movb	%dh, 13(%rsp)  # NOREX
	movb	$0, 14(%rsp)
	movb	$0, 15(%rsp)
	movw	$12, 16(%rsp)
	movl	128(%r14), %eax
	movb	%al, 20(%rsp)
	movb	%ah, 21(%rsp)  # NOREX
	movb	%cl, 22(%rsp)
	movb	%ch, 23(%rsp)  # NOREX
	movb	$1, 24(%rsp)
	movb	$0, 25(%rsp)
	movb	%sil, 26(%rsp)
	movb	$0, 27(%rsp)
	leaq	2(%rsp), %rdi
	movl	$1, %esi
	movl	$14, %edx
	movq	%r13, %rcx
	callq	fwrite
	cmpq	$14, %rax
	je	.LBB2_8
# BB#7:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB2_8:
	movq	24(%rbp), %rcx
	leaq	16(%rsp), %rdi
	movl	$1, %esi
	movl	$12, %edx
	callq	fwrite
	cmpq	$12, %rax
	je	.LBB2_10
# BB#9:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB2_10:
	testl	%ebx, %ebx
	je	.LBB2_26
# BB#11:
	movq	24(%rbp), %rsi
	movl	$3, %ecx
	jmp	.LBB2_25
.LBB2_13:
	movl	$256, %ebx              # imm = 0x100
	movb	$8, %al
.LBB2_17:
	leal	(,%rbx,4), %esi
	orl	$54, %esi
	movl	68(%rbp), %edx
	movl	132(%r14), %ecx
	imulq	%rcx, %rdx
	addq	%rsi, %rdx
	movl	$0, 8(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movq	$0, 48(%rsp)
	movb	$66, 2(%rsp)
	movb	$77, 3(%rsp)
	movb	%dl, 4(%rsp)
	movb	%dh, 5(%rsp)  # NOREX
	movq	%rdx, %rsi
	shrq	$16, %rsi
	movb	%sil, 6(%rsp)
	shrq	$24, %rdx
	movb	%dl, 7(%rsp)
	movb	$54, 12(%rsp)
	movl	%ebx, %edx
	shrl	$6, %edx
	movb	%dl, 13(%rsp)
	movb	$0, 14(%rsp)
	movb	$0, 15(%rsp)
	movw	$40, 16(%rsp)
	movl	128(%r14), %edx
	movb	%dl, 20(%rsp)
	movb	%dh, 21(%rsp)  # NOREX
	movl	%edx, %esi
	shrl	$16, %esi
	movb	%sil, 22(%rsp)
	shrl	$24, %edx
	movb	%dl, 23(%rsp)
	movb	%cl, 24(%rsp)
	movb	%ch, 25(%rsp)  # NOREX
	movl	%ecx, %edx
	shrl	$16, %edx
	movb	%dl, 26(%rsp)
	shrl	$24, %ecx
	movb	%cl, 27(%rsp)
	movb	$1, 28(%rsp)
	movb	$0, 29(%rsp)
	movb	%al, 30(%rsp)
	movb	$0, 31(%rsp)
	cmpb	$2, 368(%r14)
	jne	.LBB2_19
# BB#18:
	movzwl	370(%r14), %ecx
	movb	$100, %dl
	movl	%ecx, %eax
	mulb	%dl
	movb	%al, 40(%rsp)
	imulq	$100, %rcx, %rax
	movb	%ah, 41(%rsp)  # NOREX
	shrq	$16, %rax
	movb	%al, 42(%rsp)
	movb	$0, 43(%rsp)
	movzwl	372(%r14), %ecx
	movl	%ecx, %eax
	mulb	%dl
	movb	%al, 44(%rsp)
	imulq	$100, %rcx, %rax
	movb	%ah, 45(%rsp)  # NOREX
	shrq	$16, %rax
	movb	%al, 46(%rsp)
	movb	$0, 47(%rsp)
.LBB2_19:
	movb	$0, 48(%rsp)
	movb	%bh, 49(%rsp)  # NOREX
	leaq	2(%rsp), %rdi
	movl	$1, %esi
	movl	$14, %edx
	movq	%r13, %rcx
	callq	fwrite
	cmpq	$14, %rax
	je	.LBB2_21
# BB#20:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB2_21:
	movq	24(%rbp), %rcx
	leaq	16(%rsp), %rdi
	movl	$1, %esi
	movl	$40, %edx
	callq	fwrite
	cmpq	$40, %rax
	je	.LBB2_23
# BB#22:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB2_23:
	testl	%ebx, %ebx
	je	.LBB2_26
# BB#24:
	movq	24(%rbp), %rsi
	movl	$4, %ecx
.LBB2_25:                               # %write_bmp_header.exit
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	write_colormap
.LBB2_26:                               # %write_bmp_header.exit
	leaq	132(%r14), %r12
	movl	(%r12), %r15d
	testl	%r15d, %r15d
	je	.LBB2_38
# BB#27:                                # %.lr.ph54
	movq	56(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB2_35
# BB#28:                                # %.lr.ph54.split.us.preheader
	movl	%r15d, %ebp
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_33:                               # %.loopexit.us..lr.ph54.split.us_crit_edge
                                        #   in Loop: Header=BB2_29 Depth=1
	movl	(%r12), %r15d
	movq	56(%rsp), %rcx          # 8-byte Reload
.LBB2_29:                               # %.lr.ph54.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_31 Depth 2
	movl	%r15d, %eax
	subl	%ebp, %r15d
	movq	%r15, 8(%rcx)
	movq	%rax, 16(%rcx)
	movq	%r14, %rdi
	callq	*(%rcx)
	movq	8(%r14), %rax
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	56(%rbx), %rsi
	decl	%ebp
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	*56(%rax)
	movl	68(%rbx), %ebx
	testl	%ebx, %ebx
	je	.LBB2_32
# BB#30:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB2_29 Depth=1
	movq	(%rax), %r15
	.p2align	4, 0x90
.LBB2_31:                               # %.lr.ph.us
                                        #   Parent Loop BB2_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r15), %edi
	movq	%r13, %rsi
	callq	_IO_putc
	incq	%r15
	decl	%ebx
	jne	.LBB2_31
.LBB2_32:                               # %.loopexit.us
                                        #   in Loop: Header=BB2_29 Depth=1
	testl	%ebp, %ebp
	jne	.LBB2_33
	jmp	.LBB2_38
	.p2align	4, 0x90
.LBB2_35:                               # %.lr.ph54.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_37 Depth 2
	movq	8(%r14), %rax
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	56(%rbx), %rsi
	decl	%r15d
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	%r15d, %edx
	callq	*56(%rax)
	movl	68(%rbx), %ebp
	testl	%ebp, %ebp
	je	.LBB2_34
# BB#36:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_35 Depth=1
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB2_37:                               # %.lr.ph
                                        #   Parent Loop BB2_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %edi
	movq	%r13, %rsi
	callq	_IO_putc
	incq	%rbx
	decl	%ebp
	jne	.LBB2_37
.LBB2_34:                               # %.loopexit
                                        #   in Loop: Header=BB2_35 Depth=1
	testl	%r15d, %r15d
	jne	.LBB2_35
.LBB2_38:                               # %._crit_edge
	movq	56(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB2_40
# BB#39:
	incl	32(%rax)
.LBB2_40:
	movq	%r13, %rdi
	callq	fflush
	movq	%r13, %rdi
	callq	ferror
	testl	%eax, %eax
	je	.LBB2_41
# BB#42:
	movq	(%r14), %rax
	movl	$36, 40(%rax)
	movq	%r14, %rdi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.LBB2_41:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	finish_output_bmp, .Lfunc_end2-finish_output_bmp
	.cfi_endproc

	.p2align	4, 0x90
	.type	put_gray_rows,@function
put_gray_rows:                          # @put_gray_rows
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movq	56(%r14), %rsi
	movl	76(%r14), %edx
	movl	$1, %ecx
	movl	$1, %r8d
	callq	*56(%rax)
	incl	76(%r14)
	movq	(%rax), %rdi
	movl	128(%rbx), %r10d
	testl	%r10d, %r10d
	je	.LBB3_23
# BB#1:                                 # %.lr.ph35.preheader
	movq	32(%r14), %rcx
	movq	(%rcx), %rcx
	leal	-1(%r10), %edx
	leaq	1(%rdx), %r8
	cmpq	$32, %r8
	jae	.LBB3_3
# BB#2:
	movq	%rdi, %rbx
	jmp	.LBB3_17
.LBB3_3:                                # %min.iters.checked
	movabsq	$8589934560, %r9        # imm = 0x1FFFFFFE0
	andq	%r8, %r9
	je	.LBB3_7
# BB#4:                                 # %vector.memcheck
	leaq	1(%rcx,%rdx), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB3_8
# BB#5:                                 # %vector.memcheck
	leaq	1(%rdi,%rdx), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB3_8
.LBB3_7:
	movq	%rdi, %rbx
.LBB3_17:                               # %.lr.ph35.preheader48
	leal	-1(%r10), %esi
	movl	%r10d, %edx
	andl	$7, %edx
	je	.LBB3_20
# BB#18:                                # %.lr.ph35.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB3_19:                               # %.lr.ph35.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %eax
	incq	%rcx
	movb	%al, (%rbx)
	incq	%rbx
	decl	%r10d
	incl	%edx
	jne	.LBB3_19
.LBB3_20:                               # %.lr.ph35.prol.loopexit
	cmpl	$7, %esi
	jb	.LBB3_22
	.p2align	4, 0x90
.LBB3_21:                               # %.lr.ph35
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %eax
	movb	%al, (%rbx)
	movzbl	1(%rcx), %eax
	movb	%al, 1(%rbx)
	movzbl	2(%rcx), %eax
	movb	%al, 2(%rbx)
	movzbl	3(%rcx), %eax
	movb	%al, 3(%rbx)
	movzbl	4(%rcx), %eax
	movb	%al, 4(%rbx)
	movzbl	5(%rcx), %eax
	movb	%al, 5(%rbx)
	movzbl	6(%rcx), %eax
	movb	%al, 6(%rbx)
	movzbl	7(%rcx), %eax
	movb	%al, 7(%rbx)
	addq	$8, %rcx
	addq	$8, %rbx
	addl	$-8, %r10d
	jne	.LBB3_21
.LBB3_22:                               # %._crit_edge36.loopexit
	addq	%r8, %rdi
.LBB3_23:                               # %._crit_edge36
	movl	72(%r14), %eax
	testl	%eax, %eax
	jle	.LBB3_25
# BB#24:                                # %.lr.ph.preheader
	movl	%eax, %ecx
	notl	%ecx
	cmpl	$-3, %ecx
	movl	$-2, %edx
	cmovgl	%ecx, %edx
	leal	1(%rax,%rdx), %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB3_25:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_8:                                # %vector.body.preheader
	leaq	-32(%r9), %rbx
	movl	%ebx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB3_11
# BB#9:                                 # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_10:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdx), %xmm0
	movups	16(%rcx,%rdx), %xmm1
	movups	%xmm0, (%rdi,%rdx)
	movups	%xmm1, 16(%rdi,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB3_10
	jmp	.LBB3_12
.LBB3_11:
	xorl	%edx, %edx
.LBB3_12:                               # %vector.body.prol.loopexit
	cmpq	$96, %rbx
	jb	.LBB3_15
# BB#13:                                # %vector.body.preheader.new
	movq	%r9, %rsi
	subq	%rdx, %rsi
	leaq	112(%rcx,%rdx), %rbx
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB3_14:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rbx
	subq	$-128, %rdx
	addq	$-128, %rsi
	jne	.LBB3_14
.LBB3_15:                               # %middle.block
	cmpq	%r9, %r8
	je	.LBB3_22
# BB#16:
	subl	%r9d, %r10d
	leaq	(%rdi,%r9), %rbx
	addq	%r9, %rcx
	jmp	.LBB3_17
.Lfunc_end3:
	.size	put_gray_rows, .Lfunc_end3-put_gray_rows
	.cfi_endproc

	.p2align	4, 0x90
	.type	put_pixel_rows,@function
put_pixel_rows:                         # @put_pixel_rows
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movq	56(%r14), %rsi
	movl	76(%r14), %edx
	movl	$1, %ecx
	movl	$1, %r8d
	callq	*56(%rax)
	incl	76(%r14)
	movq	(%rax), %rdi
	movl	128(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB4_8
# BB#1:                                 # %.lr.ph40.preheader
	movq	32(%r14), %rax
	movq	(%rax), %rax
	leal	-1(%rcx), %ebx
	testb	$1, %cl
	jne	.LBB4_3
# BB#2:
	movq	%rdi, %rdx
	jmp	.LBB4_4
.LBB4_3:                                # %.lr.ph40.prol
	movb	(%rax), %cl
	movb	%cl, 2(%rdi)
	movb	1(%rax), %cl
	movb	%cl, 1(%rdi)
	movb	2(%rax), %cl
	addq	$3, %rax
	movb	%cl, (%rdi)
	leaq	3(%rdi), %rdx
	movl	%ebx, %ecx
.LBB4_4:                                # %.lr.ph40.prol.loopexit
	leaq	3(%rbx,%rbx,2), %rsi
	testl	%ebx, %ebx
	je	.LBB4_7
# BB#5:                                 # %.lr.ph40.preheader.new
	addq	$5, %rdx
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph40
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ebx
	movb	%bl, -3(%rdx)
	movzbl	1(%rax), %ebx
	movb	%bl, -4(%rdx)
	movzbl	2(%rax), %ebx
	movb	%bl, -5(%rdx)
	movzbl	3(%rax), %ebx
	movb	%bl, (%rdx)
	movzbl	4(%rax), %ebx
	movb	%bl, -1(%rdx)
	movzbl	5(%rax), %ebx
	movb	%bl, -2(%rdx)
	addq	$6, %rdx
	addq	$6, %rax
	addl	$-2, %ecx
	jne	.LBB4_6
.LBB4_7:                                # %._crit_edge41.loopexit
	addq	%rsi, %rdi
.LBB4_8:                                # %._crit_edge41
	movl	72(%r14), %eax
	testl	%eax, %eax
	jle	.LBB4_10
# BB#9:                                 # %.lr.ph.preheader
	movl	%eax, %ecx
	notl	%ecx
	cmpl	$-3, %ecx
	movl	$-2, %edx
	cmovgl	%ecx, %edx
	leal	1(%rax,%rdx), %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB4_10:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	put_pixel_rows, .Lfunc_end4-put_pixel_rows
	.cfi_endproc

	.p2align	4, 0x90
	.type	write_colormap,@function
write_colormap:                         # @write_colormap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 64
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r12d
	movq	%rsi, %rbx
	movq	152(%rdi), %r15
	testq	%r15, %r15
	je	.LBB5_6
# BB#1:
	movl	148(%rdi), %r13d
	cmpl	$3, 136(%rdi)
	jne	.LBB5_10
# BB#2:                                 # %.preheader2
	testl	%r13d, %r13d
	jle	.LBB5_16
# BB#3:                                 # %.lr.ph9
	movq	%rdi, (%rsp)            # 8-byte Spill
	xorl	%ebp, %ebp
	cmpl	$4, %r14d
	jne	.LBB5_5
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph9.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movzbl	(%rax,%rbp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	8(%r15), %rax
	movzbl	(%rax,%rbp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%r15), %rax
	movzbl	(%rax,%rbp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB5_4
	jmp	.LBB5_19
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph9.split
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movzbl	(%rax,%rbp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	8(%r15), %rax
	movzbl	(%rax,%rbp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%r15), %rax
	movzbl	(%rax,%rbp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB5_5
	jmp	.LBB5_19
.LBB5_6:                                # %.preheader1
	movq	%rdi, %r15
	xorl	%ebp, %ebp
	cmpl	$4, %r14d
	jne	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                # %.preheader1.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	incl	%ebp
	cmpl	$256, %ebp              # imm = 0x100
	jne	.LBB5_7
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_8:                                # %.preheader1.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	incl	%ebp
	cmpl	$256, %ebp              # imm = 0x100
	jne	.LBB5_8
.LBB5_9:
	movl	$256, %r13d             # imm = 0x100
	movq	%r15, %rdi
	cmpl	%r12d, %r13d
	jg	.LBB5_20
	jmp	.LBB5_21
.LBB5_10:                               # %.preheader4
	testl	%r13d, %r13d
	jle	.LBB5_16
# BB#11:                                # %.lr.ph11
	movq	%rdi, (%rsp)            # 8-byte Spill
	cmpl	$4, %r14d
	jne	.LBB5_17
# BB#12:                                # %.lr.ph11.split.us.preheader
	movl	%r14d, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph11.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movzbl	(%rax,%r14), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%r15), %rax
	movzbl	(%rax,%r14), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%r15), %rax
	movzbl	(%rax,%r14), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	incq	%r14
	cmpq	%r14, %r13
	jne	.LBB5_13
# BB#14:
	movl	%ebp, %r14d
	jmp	.LBB5_19
.LBB5_16:
	xorl	%r13d, %r13d
	cmpl	%r12d, %r13d
	jg	.LBB5_20
	jmp	.LBB5_21
.LBB5_17:                               # %.lr.ph11.split.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_18:                               # %.lr.ph11.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movzbl	(%rax,%rbp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%r15), %rax
	movzbl	(%rax,%rbp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%r15), %rax
	movzbl	(%rax,%rbp), %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB5_18
.LBB5_19:
	movq	(%rsp), %rdi            # 8-byte Reload
	cmpl	%r12d, %r13d
	jle	.LBB5_21
.LBB5_20:
	movq	(%rdi), %rax
	movl	$1039, 40(%rax)         # imm = 0x40F
	movl	%r13d, 44(%rax)
	callq	*(%rax)
.LBB5_21:                               # %.preheader
	cmpl	%r12d, %r13d
	jge	.LBB5_25
# BB#22:                                # %.lr.ph
	subl	%r13d, %r12d
	cmpl	$4, %r14d
	jne	.LBB5_24
	.p2align	4, 0x90
.LBB5_23:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	decl	%r12d
	jne	.LBB5_23
	jmp	.LBB5_25
	.p2align	4, 0x90
.LBB5_24:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	xorl	%edi, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	decl	%r12d
	jne	.LBB5_24
.LBB5_25:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	write_colormap, .Lfunc_end5-write_colormap
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
