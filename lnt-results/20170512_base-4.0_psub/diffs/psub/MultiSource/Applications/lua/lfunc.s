	.text
	.file	"lfunc.bc"
	.hidden	luaF_newCclosure
	.globl	luaF_newCclosure
	.p2align	4, 0x90
	.type	luaF_newCclosure,@function
luaF_newCclosure:                       # @luaF_newCclosure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movl	%r15d, %eax
	shll	$4, %eax
	addl	$40, %eax
	movslq	%eax, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	luaM_realloc_
	movq	%rax, %rbp
	movl	$6, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	luaC_link
	movb	$1, 10(%rbp)
	movq	%r14, 24(%rbp)
	movb	%r15b, 11(%rbp)
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	luaF_newCclosure, .Lfunc_end0-luaF_newCclosure
	.cfi_endproc

	.hidden	luaF_newLclosure
	.globl	luaF_newLclosure
	.p2align	4, 0x90
	.type	luaF_newLclosure,@function
luaF_newLclosure:                       # @luaF_newLclosure
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movl	%esi, %ebx
	movq	%rdi, %r12
	leal	40(,%rbx,8), %eax
	movslq	%eax, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	luaM_realloc_
	movq	%rax, %r14
	movl	$6, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	luaC_link
	movb	$0, 10(%r14)
	movq	%r15, 24(%r14)
	movb	%bl, 11(%r14)
	testl	%ebx, %ebx
	je	.LBB1_2
# BB#1:                                 # %.lr.ph
	movslq	%ebx, %rax
	leaq	32(,%rax,8), %rax
	decl	%ebx
	leaq	(,%rbx,8), %rcx
	subq	%rcx, %rax
	movq	%r14, %rdi
	addq	%rax, %rdi
	leaq	8(,%rbx,8), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB1_2:                                # %._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	luaF_newLclosure, .Lfunc_end1-luaF_newLclosure
	.cfi_endproc

	.hidden	luaF_newupval
	.globl	luaF_newupval
	.p2align	4, 0x90
	.type	luaF_newupval,@function
luaF_newupval:                          # @luaF_newupval
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$40, %ecx
	callq	luaM_realloc_
	movq	%rax, %rbx
	movl	$10, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaC_link
	leaq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	movl	$0, 32(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	luaF_newupval, .Lfunc_end2-luaF_newupval
	.cfi_endproc

	.hidden	luaF_findupval
	.globl	luaF_findupval
	.p2align	4, 0x90
	.type	luaF_findupval,@function
luaF_findupval:                         # @luaF_findupval
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	32(%rdi), %r14
	movq	152(%rdi), %rax
	leaq	152(%rdi), %rbx
	testq	%rax, %rax
	jne	.LBB3_2
	jmp	.LBB3_7
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%rax, %rbx
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB3_7
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r15, 16(%rax)
	jb	.LBB3_7
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	jne	.LBB3_6
# BB#4:
	movb	9(%rax), %cl
	movb	32(%r14), %dl
	notb	%dl
	andb	%cl, %dl
	testb	$3, %dl
	je	.LBB3_8
# BB#5:
	xorb	$3, %cl
	movb	%cl, 9(%rax)
	jmp	.LBB3_8
.LBB3_7:                                # %.critedge
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$40, %ecx
	callq	luaM_realloc_
	movb	$10, 8(%rax)
	movb	32(%r14), %cl
	andb	$3, %cl
	movb	%cl, 9(%rax)
	movq	%r15, 16(%rax)
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	movq	%rax, (%rbx)
	leaq	184(%r14), %rcx
	movq	%rcx, 24(%rax)
	movq	216(%r14), %rcx
	movq	%rcx, 32(%rax)
	movq	%rax, 24(%rcx)
	movq	%rax, 216(%r14)
.LBB3_8:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	luaF_findupval, .Lfunc_end3-luaF_findupval
	.cfi_endproc

	.hidden	luaF_freeupval
	.globl	luaF_freeupval
	.p2align	4, 0x90
	.type	luaF_freeupval,@function
luaF_freeupval:                         # @luaF_freeupval
	.cfi_startproc
# BB#0:
	leaq	24(%rsi), %rax
	cmpq	%rax, 16(%rsi)
	je	.LBB4_2
# BB#1:
	movq	24(%rsi), %rax
	movq	32(%rsi), %rcx
	movq	%rax, 24(%rcx)
	movq	24(%rsi), %rax
	movq	%rcx, 32(%rax)
.LBB4_2:
	movl	$40, %edx
	xorl	%ecx, %ecx
	jmp	luaM_realloc_           # TAILCALL
.Lfunc_end4:
	.size	luaF_freeupval, .Lfunc_end4-luaF_freeupval
	.cfi_endproc

	.hidden	luaF_close
	.globl	luaF_close
	.p2align	4, 0x90
	.type	luaF_close,@function
luaF_close:                             # @luaF_close
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	152(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB5_9
# BB#1:                                 # %.lr.ph
	movq	32(%rbx), %r15
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rsi), %rax
	cmpq	%r14, %rax
	jb	.LBB5_9
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	(%rsi), %rcx
	movq	%rcx, 152(%rbx)
	movzbl	32(%r15), %edx
	notb	%dl
	andb	9(%rsi), %dl
	leaq	24(%rsi), %rcx
	testb	$3, %dl
	je	.LBB5_7
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	cmpq	%rcx, %rax
	je	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	24(%rsi), %rax
	movq	32(%rsi), %rcx
	movq	%rax, 24(%rcx)
	movq	24(%rsi), %rax
	movq	%rcx, 32(%rax)
.LBB5_6:                                # %luaF_freeupval.exit
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	$40, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                #   in Loop: Header=BB5_2 Depth=1
	movq	24(%rsi), %rdx
	movq	32(%rsi), %rdi
	movq	%rdx, 24(%rdi)
	movq	24(%rsi), %rdx
	movq	%rdi, 32(%rdx)
	movq	(%rax), %rdx
	movq	%rdx, 24(%rsi)
	movl	8(%rax), %eax
	movl	%eax, 32(%rsi)
	movq	%rcx, 16(%rsi)
	movq	%rbx, %rdi
	callq	luaC_linkupval
.LBB5_8:                                # %.backedge
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	152(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.LBB5_2
.LBB5_9:                                # %.critedge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	luaF_close, .Lfunc_end5-luaF_close
	.cfi_endproc

	.hidden	luaF_newproto
	.globl	luaF_newproto
	.p2align	4, 0x90
	.type	luaF_newproto,@function
luaF_newproto:                          # @luaF_newproto
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$120, %ecx
	callq	luaM_realloc_
	movq	%rax, %rbx
	movl	$9, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaC_link
	xorps	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movq	$0, 96(%rbx)
	movl	$0, 112(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	luaF_newproto, .Lfunc_end6-luaF_newproto
	.cfi_endproc

	.hidden	luaF_freeproto
	.globl	luaF_freeproto
	.p2align	4, 0x90
	.type	luaF_freeproto,@function
luaF_freeproto:                         # @luaF_freeproto
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%rbx), %rsi
	movslq	80(%rbx), %rdx
	shlq	$2, %rdx
	xorl	%ecx, %ecx
	callq	luaM_realloc_
	movq	32(%rbx), %rsi
	movslq	88(%rbx), %rdx
	shlq	$3, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	luaM_realloc_
	movq	16(%rbx), %rsi
	movslq	76(%rbx), %rdx
	shlq	$4, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	luaM_realloc_
	movq	40(%rbx), %rsi
	movslq	84(%rbx), %rdx
	shlq	$2, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	luaM_realloc_
	movq	48(%rbx), %rsi
	movslq	92(%rbx), %rdx
	shlq	$4, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	luaM_realloc_
	movq	56(%rbx), %rsi
	movslq	72(%rbx), %rdx
	shlq	$3, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	luaM_realloc_
	movl	$120, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	luaM_realloc_           # TAILCALL
.Lfunc_end7:
	.size	luaF_freeproto, .Lfunc_end7-luaF_freeproto
	.cfi_endproc

	.hidden	luaF_freeclosure
	.globl	luaF_freeclosure
	.p2align	4, 0x90
	.type	luaF_freeclosure,@function
luaF_freeclosure:                       # @luaF_freeclosure
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpb	$0, 10(%rsi)
	setne	%al
	movzbl	11(%rsi), %ecx
	decl	%ecx
	leal	8(,%rax,8), %edx
	imull	%ecx, %edx
	leal	48(%rdx,%rax,8), %eax
	movslq	%eax, %rdx
	xorl	%ecx, %ecx
	jmp	luaM_realloc_           # TAILCALL
.Lfunc_end8:
	.size	luaF_freeclosure, .Lfunc_end8-luaF_freeclosure
	.cfi_endproc

	.hidden	luaF_getlocalname
	.globl	luaF_getlocalname
	.p2align	4, 0x90
	.type	luaF_getlocalname,@function
luaF_getlocalname:                      # @luaF_getlocalname
	.cfi_startproc
# BB#0:
	movslq	92(%rdi), %r8
	testq	%r8, %r8
	jle	.LBB9_1
# BB#2:                                 # %.lr.ph
	movq	48(%rdi), %rdi
	addq	$12, %rdi
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	%edx, -4(%rdi)
	jg	.LBB9_8
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	cmpl	%edx, (%rdi)
	jle	.LBB9_7
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=1
	decl	%esi
	je	.LBB9_6
.LBB9_7:                                #   in Loop: Header=BB9_3 Depth=1
	incq	%rcx
	addq	$16, %rdi
	cmpq	%r8, %rcx
	jl	.LBB9_3
.LBB9_8:                                # %.critedge
	retq
.LBB9_1:
	xorl	%eax, %eax
	retq
.LBB9_6:
	movq	-12(%rdi), %rax
	addq	$24, %rax
	retq
.Lfunc_end9:
	.size	luaF_getlocalname, .Lfunc_end9-luaF_getlocalname
	.cfi_endproc

	.hidden	luaM_realloc_
	.hidden	luaC_link
	.hidden	luaC_linkupval

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
