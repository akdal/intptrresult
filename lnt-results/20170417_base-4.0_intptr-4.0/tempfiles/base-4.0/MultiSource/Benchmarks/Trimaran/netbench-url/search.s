	.text
	.file	"search.bc"
	.globl	calculate_bm_table
	.p2align	4, 0x90
	.type	calculate_bm_table,@function
calculate_bm_table:                     # @calculate_bm_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	testq	%rax, %rax
	je	.LBB0_10
# BB#1:                                 # %vector.body
	movd	%ebp, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%rax)
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, 32(%rax)
	movdqu	%xmm0, 48(%rax)
	movdqu	%xmm0, 64(%rax)
	movdqu	%xmm0, 80(%rax)
	movdqu	%xmm0, 96(%rax)
	movdqu	%xmm0, 112(%rax)
	movdqu	%xmm0, 128(%rax)
	movdqu	%xmm0, 144(%rax)
	movdqu	%xmm0, 160(%rax)
	movdqu	%xmm0, 176(%rax)
	movdqu	%xmm0, 192(%rax)
	movdqu	%xmm0, 208(%rax)
	movdqu	%xmm0, 224(%rax)
	movdqu	%xmm0, 240(%rax)
	movdqu	%xmm0, 256(%rax)
	movdqu	%xmm0, 272(%rax)
	movdqu	%xmm0, 288(%rax)
	movdqu	%xmm0, 304(%rax)
	movdqu	%xmm0, 320(%rax)
	movdqu	%xmm0, 336(%rax)
	movdqu	%xmm0, 352(%rax)
	movdqu	%xmm0, 368(%rax)
	movdqu	%xmm0, 384(%rax)
	movdqu	%xmm0, 400(%rax)
	movdqu	%xmm0, 416(%rax)
	movdqu	%xmm0, 432(%rax)
	movdqu	%xmm0, 448(%rax)
	movdqu	%xmm0, 464(%rax)
	movdqu	%xmm0, 480(%rax)
	movdqu	%xmm0, 496(%rax)
	movdqu	%xmm0, 512(%rax)
	movdqu	%xmm0, 528(%rax)
	movdqu	%xmm0, 544(%rax)
	movdqu	%xmm0, 560(%rax)
	movdqu	%xmm0, 576(%rax)
	movdqu	%xmm0, 592(%rax)
	movdqu	%xmm0, 608(%rax)
	movdqu	%xmm0, 624(%rax)
	movdqu	%xmm0, 640(%rax)
	movdqu	%xmm0, 656(%rax)
	movdqu	%xmm0, 672(%rax)
	movdqu	%xmm0, 688(%rax)
	movdqu	%xmm0, 704(%rax)
	movdqu	%xmm0, 720(%rax)
	movdqu	%xmm0, 736(%rax)
	movdqu	%xmm0, 752(%rax)
	movdqu	%xmm0, 768(%rax)
	movdqu	%xmm0, 784(%rax)
	movdqu	%xmm0, 800(%rax)
	movdqu	%xmm0, 816(%rax)
	movdqu	%xmm0, 832(%rax)
	movdqu	%xmm0, 848(%rax)
	movdqu	%xmm0, 864(%rax)
	movdqu	%xmm0, 880(%rax)
	movdqu	%xmm0, 896(%rax)
	movdqu	%xmm0, 912(%rax)
	movdqu	%xmm0, 928(%rax)
	movdqu	%xmm0, 944(%rax)
	movdqu	%xmm0, 960(%rax)
	movdqu	%xmm0, 976(%rax)
	movdqu	%xmm0, 992(%rax)
	movdqu	%xmm0, 1008(%rax)
	testl	%ebp, %ebp
	jle	.LBB0_9
# BB#2:                                 # %.lr.ph
	movl	%ebp, %r8d
	leaq	-1(%r8), %rsi
	movq	%r8, %rdi
	andq	$3, %rdi
	je	.LBB0_3
# BB#4:                                 # %.prol.preheader
	decl	%ebp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r14,%rdx), %ebx
	movl	%ebp, (%rax,%rbx,4)
	incq	%rdx
	decl	%ebp
	cmpq	%rdx, %rdi
	jne	.LBB0_5
	jmp	.LBB0_6
.LBB0_3:
	xorl	%edx, %edx
.LBB0_6:                                # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB0_9
# BB#7:                                 # %.lr.ph.new
	leal	3(%rdx), %r9d
	notl	%r9d
	leal	2(%rdx), %edi
	notl	%edi
	movl	%edx, %ebp
	notl	%ebp
	leal	1(%rdx), %ebx
	notl	%ebx
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	leal	(%r8,%rbp), %ecx
	movzbl	(%r14,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	leal	(%rbx,%r8), %ecx
	movzbl	1(%r14,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	leal	(%rdi,%r8), %ecx
	movzbl	2(%r14,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	leal	(%r9,%r8), %ecx
	movzbl	3(%r14,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	addq	$4, %rdx
	addq	$-4, %r9
	addq	$-4, %rdi
	addq	$-4, %rbp
	addq	$-4, %rbx
	cmpq	%rdx, %r8
	jne	.LBB0_8
.LBB0_9:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB0_10:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.calculate_bm_table, %esi
	movl	$60, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	_fatal
.Lfunc_end0:
	.size	calculate_bm_table, .Lfunc_end0-calculate_bm_table
	.cfi_endproc

	.globl	boyer_moore
	.p2align	4, 0x90
	.type	boyer_moore,@function
boyer_moore:                            # @boyer_moore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 80
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movl	%ecx, %r13d
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	$-1, %eax
	cmpl	%ebx, %r13d
	jg	.LBB1_10
# BB#1:                                 # %.preheader.lr.ph
	leal	-1(%r13), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	cltq
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	movl	%r13d, %eax
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB1_3:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %r14d
	cltq
	movzbl	-1(%rbp,%rax), %eax
	movl	(%r12,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=2
	addl	%r14d, %ecx
	leal	(%rcx,%r13), %eax
	cmpl	%ebx, %eax
	jle	.LBB1_3
.LBB1_5:                                # %.critedge
                                        #   in Loop: Header=BB1_2 Depth=1
	movslq	%r14d, %r15
	leaq	(%rbp,%r15), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	memcmp
	cmpl	%ebx, %r15d
	jge	.LBB1_8
# BB#6:                                 # %.critedge
                                        #   in Loop: Header=BB1_2 Depth=1
	testl	%eax, %eax
	je	.LBB1_7
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=1
	addl	4(%rsp), %r14d          # 4-byte Folded Reload
	leal	(%r14,%r13), %eax
	cmpl	%ebx, %eax
	jle	.LBB1_2
# BB#9:
	movl	$-1, %eax
	jmp	.LBB1_10
.LBB1_7:
	movl	%r14d, %eax
.LBB1_10:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	boyer_moore, .Lfunc_end1-boyer_moore
	.cfi_endproc

	.globl	find_destination
	.p2align	4, 0x90
	.type	find_destination,@function
find_destination:                       # @find_destination
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 96
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%esi, %r13d
	movq	%rdi, %rbx
	movq	tree_head(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_11
# BB#1:                                 # %.lr.ph73.preheader
	movl	%r13d, 36(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph73
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
                                        #       Child Loop BB2_5 Depth 3
                                        #     Child Loop BB2_14 Depth 2
                                        #       Child Loop BB2_16 Depth 3
                                        #         Child Loop BB2_17 Depth 4
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	16(%rax), %r14
	cmpl	%r13d, %r14d
	jg	.LBB2_10
# BB#3:                                 # %.preheader.lr.ph.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	24(%rax), %rbp
	movq	%r14, %rdx
	decq	%rdx
	xorl	%r15d, %r15d
	movl	%r14d, %eax
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_4:                                # %.preheader.i
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_5 Depth 3
	movl	%r15d, %ecx
	.p2align	4, 0x90
.LBB2_5:                                #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %r15d
	cltq
	movzbl	-1(%rbx,%rax), %eax
	movl	(%rbp,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=3
	addl	%r15d, %ecx
	leal	(%rcx,%r14), %eax
	cmpl	%r13d, %eax
	jle	.LBB2_5
.LBB2_7:                                # %.critedge.i
                                        #   in Loop: Header=BB2_4 Depth=2
	movslq	%r15d, %r12
	leaq	(%rbx,%r12), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	memcmp
	cmpl	%r13d, %r12d
	jge	.LBB2_9
# BB#8:                                 # %.critedge.i
                                        #   in Loop: Header=BB2_4 Depth=2
	testl	%eax, %eax
	je	.LBB2_12
.LBB2_9:                                #   in Loop: Header=BB2_4 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	addl	%edx, %r15d
	leal	(%r15,%r14), %eax
	cmpl	%r13d, %eax
	jle	.LBB2_4
	jmp	.LBB2_10
	.p2align	4, 0x90
.LBB2_12:                               # %boyer_moore.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$-1, %r15d
	je	.LBB2_10
# BB#13:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	40(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB2_10
	.p2align	4, 0x90
.LBB2_14:                               # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_16 Depth 3
                                        #         Child Loop BB2_17 Depth 4
	movl	36(%rcx), %eax
	cmpl	%r13d, %eax
	movl	%r13d, %ebp
	cmovlel	%eax, %ebp
	testl	%eax, %eax
	cmovel	%r13d, %ebp
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	32(%rcx), %r12
	cmpl	%ebp, %r12d
	jg	.LBB2_22
# BB#15:                                # %.preheader.lr.ph.i46
                                        #   in Loop: Header=BB2_14 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	40(%rax), %r15
	movq	%r12, %rdx
	decq	%rdx
	xorl	%r14d, %r14d
	movl	%r12d, %eax
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_16:                               # %.preheader.i48
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_14 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_17 Depth 4
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_14 Depth=2
                                        #       Parent Loop BB2_16 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%ecx, %r14d
	cltq
	movzbl	-1(%rbx,%rax), %eax
	movl	(%r15,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_17 Depth=4
	addl	%r14d, %ecx
	leal	(%rcx,%r12), %eax
	cmpl	%ebp, %eax
	jle	.LBB2_17
.LBB2_19:                               # %.critedge.i59
                                        #   in Loop: Header=BB2_16 Depth=3
	movslq	%r14d, %r13
	leaq	(%rbx,%r13), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	memcmp
	cmpl	%ebp, %r13d
	jge	.LBB2_21
# BB#20:                                # %.critedge.i59
                                        #   in Loop: Header=BB2_16 Depth=3
	testl	%eax, %eax
	je	.LBB2_23
.LBB2_21:                               #   in Loop: Header=BB2_16 Depth=3
	movq	16(%rsp), %rdx          # 8-byte Reload
	addl	%edx, %r14d
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jle	.LBB2_16
	jmp	.LBB2_22
	.p2align	4, 0x90
.LBB2_23:                               # %boyer_moore.exit61
                                        #   in Loop: Header=BB2_14 Depth=2
	cmpl	$-1, %r14d
	jne	.LBB2_24
.LBB2_22:                               # %.thread
                                        #   in Loop: Header=BB2_14 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	56(%rcx), %rcx
	testq	%rcx, %rcx
	movl	36(%rsp), %r13d         # 4-byte Reload
	jne	.LBB2_14
	.p2align	4, 0x90
.LBB2_10:                               # %boyer_moore.exit.thread
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB2_2
.LBB2_11:
	xorl	%eax, %eax
	jmp	.LBB2_27
.LBB2_24:
	movq	8(%rsp), %rbp           # 8-byte Reload
	cmpl	$0, 4(%rbp)
	jne	.LBB2_26
# BB#25:
	movq	16(%rbp), %rdi
	movl	12(%rbx), %edx
	movl	16(%rbx), %ecx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB2_26:
	movq	48(%rbp), %rax
.LBB2_27:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	find_destination, .Lfunc_end2-find_destination
	.cfi_endproc

	.globl	NewPatternNode
	.p2align	4, 0x90
	.type	NewPatternNode,@function
NewPatternNode:                         # @NewPatternNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	movq	%rdx, %r15
	movl	%esi, %r12d
	movl	%edi, %r13d
	movl	$64, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB3_14
# BB#1:
	movl	%r13d, (%r14)
	movl	%r12d, 4(%r14)
	movslq	%ebp, %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 24(%r14)
	testq	%rax, %rax
	je	.LBB3_2
# BB#4:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	strncpy
	movl	%r12d, 32(%r14)
	movl	%ebx, 36(%r14)
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	testq	%rax, %rax
	je	.LBB3_15
# BB#5:                                 # %vector.body
	movd	%ebp, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%rax)
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, 32(%rax)
	movdqu	%xmm0, 48(%rax)
	movdqu	%xmm0, 64(%rax)
	movdqu	%xmm0, 80(%rax)
	movdqu	%xmm0, 96(%rax)
	movdqu	%xmm0, 112(%rax)
	movdqu	%xmm0, 128(%rax)
	movdqu	%xmm0, 144(%rax)
	movdqu	%xmm0, 160(%rax)
	movdqu	%xmm0, 176(%rax)
	movdqu	%xmm0, 192(%rax)
	movdqu	%xmm0, 208(%rax)
	movdqu	%xmm0, 224(%rax)
	movdqu	%xmm0, 240(%rax)
	movdqu	%xmm0, 256(%rax)
	movdqu	%xmm0, 272(%rax)
	movdqu	%xmm0, 288(%rax)
	movdqu	%xmm0, 304(%rax)
	movdqu	%xmm0, 320(%rax)
	movdqu	%xmm0, 336(%rax)
	movdqu	%xmm0, 352(%rax)
	movdqu	%xmm0, 368(%rax)
	movdqu	%xmm0, 384(%rax)
	movdqu	%xmm0, 400(%rax)
	movdqu	%xmm0, 416(%rax)
	movdqu	%xmm0, 432(%rax)
	movdqu	%xmm0, 448(%rax)
	movdqu	%xmm0, 464(%rax)
	movdqu	%xmm0, 480(%rax)
	movdqu	%xmm0, 496(%rax)
	movdqu	%xmm0, 512(%rax)
	movdqu	%xmm0, 528(%rax)
	movdqu	%xmm0, 544(%rax)
	movdqu	%xmm0, 560(%rax)
	movdqu	%xmm0, 576(%rax)
	movdqu	%xmm0, 592(%rax)
	movdqu	%xmm0, 608(%rax)
	movdqu	%xmm0, 624(%rax)
	movdqu	%xmm0, 640(%rax)
	movdqu	%xmm0, 656(%rax)
	movdqu	%xmm0, 672(%rax)
	movdqu	%xmm0, 688(%rax)
	movdqu	%xmm0, 704(%rax)
	movdqu	%xmm0, 720(%rax)
	movdqu	%xmm0, 736(%rax)
	movdqu	%xmm0, 752(%rax)
	movdqu	%xmm0, 768(%rax)
	movdqu	%xmm0, 784(%rax)
	movdqu	%xmm0, 800(%rax)
	movdqu	%xmm0, 816(%rax)
	movdqu	%xmm0, 832(%rax)
	movdqu	%xmm0, 848(%rax)
	movdqu	%xmm0, 864(%rax)
	movdqu	%xmm0, 880(%rax)
	movdqu	%xmm0, 896(%rax)
	movdqu	%xmm0, 912(%rax)
	movdqu	%xmm0, 928(%rax)
	movdqu	%xmm0, 944(%rax)
	movdqu	%xmm0, 960(%rax)
	movdqu	%xmm0, 976(%rax)
	movdqu	%xmm0, 992(%rax)
	movdqu	%xmm0, 1008(%rax)
	testl	%ebp, %ebp
	jle	.LBB3_13
# BB#6:                                 # %.lr.ph.i
	movl	%ebp, %r8d
	leaq	-1(%r8), %rsi
	movq	%r8, %rdi
	andq	$3, %rdi
	je	.LBB3_7
# BB#8:                                 # %.prol.preheader
	decl	%ebp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r15,%rdx), %ebx
	movl	%ebp, (%rax,%rbx,4)
	incq	%rdx
	decl	%ebp
	cmpq	%rdx, %rdi
	jne	.LBB3_9
	jmp	.LBB3_10
.LBB3_7:
	xorl	%edx, %edx
.LBB3_10:                               # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB3_13
# BB#11:                                # %.lr.ph.i.new
	leal	3(%rdx), %r9d
	notl	%r9d
	leal	2(%rdx), %edi
	notl	%edi
	leal	1(%rdx), %ebp
	notl	%ebp
	movl	%edx, %ebx
	notl	%ebx
	.p2align	4, 0x90
.LBB3_12:                               # =>This Inner Loop Header: Depth=1
	leal	(%r8,%rbx), %ecx
	movzbl	(%r15,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	leal	(%rbp,%r8), %ecx
	movzbl	1(%r15,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	leal	(%rdi,%r8), %ecx
	movzbl	2(%r15,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	leal	(%r9,%r8), %ecx
	movzbl	3(%r15,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	addq	$4, %rdx
	addq	$-4, %r9
	addq	$-4, %rdi
	addq	$-4, %rbp
	addq	$-4, %rbx
	cmpq	%rdx, %r8
	jne	.LBB3_12
.LBB3_13:                               # %calculate_bm_table.exit
	movq	%rax, 40(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_14:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.NewPatternNode, %esi
	movl	$170, %edx
	movl	$.L.str.3, %ecx
	xorl	%eax, %eax
	callq	_fatal
.LBB3_2:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.NewPatternNode, %esi
	movl	$175, %edx
	jmp	.LBB3_3
.LBB3_15:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.calculate_bm_table, %esi
	movl	$60, %edx
.LBB3_3:
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	_fatal
.Lfunc_end3:
	.size	NewPatternNode, .Lfunc_end3-NewPatternNode
	.cfi_endproc

	.globl	NewStrTreeNode
	.p2align	4, 0x90
	.type	NewStrTreeNode,@function
NewStrTreeNode:                         # @NewStrTreeNode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 64
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r15
	movl	%edi, %ebx
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB4_14
# BB#1:
	movl	%ebx, (%r14)
	movslq	%ebp, %r12
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, %r13
	movq	%r13, 8(%r14)
	testq	%r13, %r13
	je	.LBB4_2
# BB#4:
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	strncpy
	movl	%r12d, 16(%r14)
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	testq	%rax, %rax
	je	.LBB4_15
# BB#5:                                 # %vector.body
	movd	%ebp, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%rax)
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, 32(%rax)
	movdqu	%xmm0, 48(%rax)
	movdqu	%xmm0, 64(%rax)
	movdqu	%xmm0, 80(%rax)
	movdqu	%xmm0, 96(%rax)
	movdqu	%xmm0, 112(%rax)
	movdqu	%xmm0, 128(%rax)
	movdqu	%xmm0, 144(%rax)
	movdqu	%xmm0, 160(%rax)
	movdqu	%xmm0, 176(%rax)
	movdqu	%xmm0, 192(%rax)
	movdqu	%xmm0, 208(%rax)
	movdqu	%xmm0, 224(%rax)
	movdqu	%xmm0, 240(%rax)
	movdqu	%xmm0, 256(%rax)
	movdqu	%xmm0, 272(%rax)
	movdqu	%xmm0, 288(%rax)
	movdqu	%xmm0, 304(%rax)
	movdqu	%xmm0, 320(%rax)
	movdqu	%xmm0, 336(%rax)
	movdqu	%xmm0, 352(%rax)
	movdqu	%xmm0, 368(%rax)
	movdqu	%xmm0, 384(%rax)
	movdqu	%xmm0, 400(%rax)
	movdqu	%xmm0, 416(%rax)
	movdqu	%xmm0, 432(%rax)
	movdqu	%xmm0, 448(%rax)
	movdqu	%xmm0, 464(%rax)
	movdqu	%xmm0, 480(%rax)
	movdqu	%xmm0, 496(%rax)
	movdqu	%xmm0, 512(%rax)
	movdqu	%xmm0, 528(%rax)
	movdqu	%xmm0, 544(%rax)
	movdqu	%xmm0, 560(%rax)
	movdqu	%xmm0, 576(%rax)
	movdqu	%xmm0, 592(%rax)
	movdqu	%xmm0, 608(%rax)
	movdqu	%xmm0, 624(%rax)
	movdqu	%xmm0, 640(%rax)
	movdqu	%xmm0, 656(%rax)
	movdqu	%xmm0, 672(%rax)
	movdqu	%xmm0, 688(%rax)
	movdqu	%xmm0, 704(%rax)
	movdqu	%xmm0, 720(%rax)
	movdqu	%xmm0, 736(%rax)
	movdqu	%xmm0, 752(%rax)
	movdqu	%xmm0, 768(%rax)
	movdqu	%xmm0, 784(%rax)
	movdqu	%xmm0, 800(%rax)
	movdqu	%xmm0, 816(%rax)
	movdqu	%xmm0, 832(%rax)
	movdqu	%xmm0, 848(%rax)
	movdqu	%xmm0, 864(%rax)
	movdqu	%xmm0, 880(%rax)
	movdqu	%xmm0, 896(%rax)
	movdqu	%xmm0, 912(%rax)
	movdqu	%xmm0, 928(%rax)
	movdqu	%xmm0, 944(%rax)
	movdqu	%xmm0, 960(%rax)
	movdqu	%xmm0, 976(%rax)
	movdqu	%xmm0, 992(%rax)
	movdqu	%xmm0, 1008(%rax)
	testl	%ebp, %ebp
	jle	.LBB4_13
# BB#6:                                 # %.lr.ph.i
	movl	%ebp, %r8d
	leaq	-1(%r8), %rsi
	movq	%r8, %rdi
	andq	$3, %rdi
	je	.LBB4_7
# BB#8:                                 # %.prol.preheader
	decl	%ebp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r13,%rdx), %ebx
	movl	%ebp, (%rax,%rbx,4)
	incq	%rdx
	decl	%ebp
	cmpq	%rdx, %rdi
	jne	.LBB4_9
	jmp	.LBB4_10
.LBB4_7:
	xorl	%edx, %edx
.LBB4_10:                               # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB4_13
# BB#11:                                # %.lr.ph.i.new
	leal	3(%rdx), %r9d
	notl	%r9d
	leal	2(%rdx), %edi
	notl	%edi
	leal	1(%rdx), %ebp
	notl	%ebp
	movl	%edx, %ebx
	notl	%ebx
	.p2align	4, 0x90
.LBB4_12:                               # =>This Inner Loop Header: Depth=1
	leal	(%r8,%rbx), %ecx
	movzbl	(%r13,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	leal	(%rbp,%r8), %ecx
	movzbl	1(%r13,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	leal	(%rdi,%r8), %ecx
	movzbl	2(%r13,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	leal	(%r9,%r8), %ecx
	movzbl	3(%r13,%rdx), %esi
	movl	%ecx, (%rax,%rsi,4)
	addq	$4, %rdx
	addq	$-4, %r9
	addq	$-4, %rdi
	addq	$-4, %rbp
	addq	$-4, %rbx
	cmpq	%rdx, %r8
	jne	.LBB4_12
.LBB4_13:                               # %calculate_bm_table.exit
	movq	%rax, 24(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_14:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.NewStrTreeNode, %esi
	movl	$190, %edx
	movl	$.L.str.3, %ecx
	xorl	%eax, %eax
	callq	_fatal
.LBB4_2:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.NewStrTreeNode, %esi
	movl	$194, %edx
	jmp	.LBB4_3
.LBB4_15:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.calculate_bm_table, %esi
	movl	$60, %edx
.LBB4_3:
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	_fatal
.Lfunc_end4:
	.size	NewStrTreeNode, .Lfunc_end4-NewStrTreeNode
	.cfi_endproc

	.globl	find_lcs
	.p2align	4, 0x90
	.type	find_lcs,@function
find_lcs:                               # @find_lcs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi64:
	.cfi_def_cfa_offset 128
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movl	%ecx, %r15d
	movl	%esi, %r14d
	movq	%rdi, %r13
	cmpl	%r14d, %r15d
	movl	%r15d, %ebp
	cmovgl	%r14d, %ebp
	movq	%rdx, %r12
	cmovgq	%r13, %r12
	movl	%r14d, %eax
	cmovgel	%r15d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cmovgq	%rdx, %r13
	movl	$1024, %edi             # imm = 0x400
	callq	malloc
	testq	%rax, %rax
	je	.LBB5_32
# BB#1:                                 # %vector.body
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movd	%ebp, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%rax)
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, 32(%rax)
	movdqu	%xmm0, 48(%rax)
	movdqu	%xmm0, 64(%rax)
	movdqu	%xmm0, 80(%rax)
	movdqu	%xmm0, 96(%rax)
	movdqu	%xmm0, 112(%rax)
	movdqu	%xmm0, 128(%rax)
	movdqu	%xmm0, 144(%rax)
	movdqu	%xmm0, 160(%rax)
	movdqu	%xmm0, 176(%rax)
	movdqu	%xmm0, 192(%rax)
	movdqu	%xmm0, 208(%rax)
	movdqu	%xmm0, 224(%rax)
	movdqu	%xmm0, 240(%rax)
	movdqu	%xmm0, 256(%rax)
	movdqu	%xmm0, 272(%rax)
	movdqu	%xmm0, 288(%rax)
	movdqu	%xmm0, 304(%rax)
	movdqu	%xmm0, 320(%rax)
	movdqu	%xmm0, 336(%rax)
	movdqu	%xmm0, 352(%rax)
	movdqu	%xmm0, 368(%rax)
	movdqu	%xmm0, 384(%rax)
	movdqu	%xmm0, 400(%rax)
	movdqu	%xmm0, 416(%rax)
	movdqu	%xmm0, 432(%rax)
	movdqu	%xmm0, 448(%rax)
	movdqu	%xmm0, 464(%rax)
	movdqu	%xmm0, 480(%rax)
	movdqu	%xmm0, 496(%rax)
	movdqu	%xmm0, 512(%rax)
	movdqu	%xmm0, 528(%rax)
	movdqu	%xmm0, 544(%rax)
	movdqu	%xmm0, 560(%rax)
	movdqu	%xmm0, 576(%rax)
	movdqu	%xmm0, 592(%rax)
	movdqu	%xmm0, 608(%rax)
	movdqu	%xmm0, 624(%rax)
	movdqu	%xmm0, 640(%rax)
	movdqu	%xmm0, 656(%rax)
	movdqu	%xmm0, 672(%rax)
	movdqu	%xmm0, 688(%rax)
	movdqu	%xmm0, 704(%rax)
	movdqu	%xmm0, 720(%rax)
	movdqu	%xmm0, 736(%rax)
	movdqu	%xmm0, 752(%rax)
	movdqu	%xmm0, 768(%rax)
	movdqu	%xmm0, 784(%rax)
	movdqu	%xmm0, 800(%rax)
	movdqu	%xmm0, 816(%rax)
	movdqu	%xmm0, 832(%rax)
	movdqu	%xmm0, 848(%rax)
	movdqu	%xmm0, 864(%rax)
	movdqu	%xmm0, 880(%rax)
	movdqu	%xmm0, 896(%rax)
	movdqu	%xmm0, 912(%rax)
	movdqu	%xmm0, 928(%rax)
	movdqu	%xmm0, 944(%rax)
	movdqu	%xmm0, 960(%rax)
	movdqu	%xmm0, 976(%rax)
	movdqu	%xmm0, 992(%rax)
	movdqu	%xmm0, 1008(%rax)
	leal	-1(%rbp), %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	movq	%rbp, %r11
	jle	.LBB5_8
# BB#2:                                 # %.lr.ph.i
	movl	%r11d, %edx
	leaq	-1(%rdx), %rsi
	movq	%rdx, %rdi
	xorl	%ecx, %ecx
	andq	$3, %rdi
	je	.LBB5_5
# BB#3:                                 # %.prol.preheader
	movq	24(%rsp), %rbp          # 8-byte Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
	.p2align	4, 0x90
.LBB5_4:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%r12,%rcx), %ebx
	movl	%ebp, (%rax,%rbx,4)
	incq	%rcx
	decl	%ebp
	cmpq	%rcx, %rdi
	jne	.LBB5_4
.LBB5_5:                                # %.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB5_8
# BB#6:                                 # %.lr.ph.i.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	3(%rcx), %rsi
	leaq	3(%r12,%rcx), %rdx
	movl	%r14d, %edi
	notl	%edi
	movl	%r15d, %ebp
	notl	%ebp
	cmpl	%ebp, %edi
	cmovgel	%edi, %ebp
	movl	$-2, %edi
	subl	%ebp, %edi
	movl	%edi, %r9d
	subl	%esi, %r9d
	leal	2(%rcx), %esi
	movl	%edi, %r10d
	subl	%esi, %r10d
	leal	1(%rcx), %ebx
	movl	%edi, %esi
	subl	%ebx, %esi
	subl	%ecx, %edi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_7:                                # =>This Inner Loop Header: Depth=1
	leal	(%rdi,%rcx), %ebx
	movzbl	-3(%rdx), %ebp
	movl	%ebx, (%rax,%rbp,4)
	leal	(%rsi,%rcx), %ebp
	movzbl	-2(%rdx), %ebx
	movl	%ebp, (%rax,%rbx,4)
	leal	(%r10,%rcx), %ebp
	movzbl	-1(%rdx), %ebx
	movl	%ebp, (%rax,%rbx,4)
	leal	(%r9,%rcx), %ebp
	movzbl	(%rdx), %ebx
	movl	%ebp, (%rax,%rbx,4)
	addq	$-4, %rcx
	addq	$4, %rdx
	cmpq	%rcx, %r8
	jne	.LBB5_7
.LBB5_8:                                # %calculate_bm_table.exit
	movslq	24(%rsp), %rcx          # 4-byte Folded Reload
	movzbl	(%r13,%rcx), %ecx
	movl	(%rax,%rcx,4), %ecx
	movq	%r11, %r10
	movslq	%r11d, %r11
	notl	%r14d
	notl	%r15d
	cmpl	%r15d, %r14d
	cmovgel	%r14d, %r15d
	movl	$-3, %edx
	subl	%r15d, %edx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	leaq	-1(%r13), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	notl	%r15d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	xorl	%r9d, %r9d
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
.LBB5_9:                                # %.thread.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_11 Depth 2
                                        #       Child Loop BB5_15 Depth 3
                                        #       Child Loop BB5_23 Depth 3
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	cmpl	12(%rsp), %r9d          # 4-byte Folded Reload
	jge	.LBB5_30
# BB#10:                                # %.lr.ph124
                                        #   in Loop: Header=BB5_9 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	1(%rdx), %edx
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movl	%r9d, %esi
	.p2align	4, 0x90
.LBB5_11:                               #   Parent Loop BB5_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_15 Depth 3
                                        #       Child Loop BB5_23 Depth 3
	leal	(%rcx,%rsi), %r9d
	movq	24(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%r9), %r14d
	movslq	%r14d, %rbp
	movzbl	(%r13,%rbp), %edx
	movl	(%rax,%rdx,4), %r8d
	cmpl	%r10d, %r8d
	movl	36(%rsp), %ebx          # 4-byte Reload
	je	.LBB5_29
# BB#12:                                # %.preheader
                                        #   in Loop: Header=BB5_11 Depth=2
	cmpl	%r8d, %r10d
	jle	.LBB5_18
# BB#13:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_11 Depth=2
	movq	24(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	subl	%r8d, %edi
	movslq	%edi, %rdi
	cmpb	%dl, (%r12,%rdi)
	jne	.LBB5_18
# BB#14:                                # %.lr.ph167
                                        #   in Loop: Header=BB5_11 Depth=2
	movslq	%r8d, %rdi
	addq	64(%rsp), %rbp          # 8-byte Folded Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%edx, %r13d
	subl	%r8d, %r13d
	incq	%rdi
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_15:                               #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rdi, %r11
	jle	.LBB5_16
# BB#20:                                # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB5_15 Depth=3
	leal	(%r13,%r15), %edx
	movslq	%edx, %rdx
	movzbl	(%r12,%rdx), %ebx
	leaq	-1(%r15), %rdx
	incq	%rdi
	cmpb	(%rbp,%r15), %bl
	movq	%rdx, %r15
	je	.LBB5_15
# BB#21:                                # %.lr.ph..critedge.preheader.loopexit_crit_edge
                                        #   in Loop: Header=BB5_11 Depth=2
	addl	%edx, %r14d
	subl	%edx, %r8d
	jmp	.LBB5_17
.LBB5_16:                               # %.critedge.preheader.loopexit
                                        #   in Loop: Header=BB5_11 Depth=2
	subl	%r15d, %r8d
	incl	%r8d
	addl	8(%rsp), %ecx           # 4-byte Folded Reload
	addl	%esi, %ecx
	addl	%r15d, %ecx
	movl	%ecx, %r14d
.LBB5_17:                               # %.critedge.preheader
                                        #   in Loop: Header=BB5_11 Depth=2
	movl	4(%rsp), %r15d          # 4-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
.LBB5_18:                               # %.critedge.preheader
                                        #   in Loop: Header=BB5_11 Depth=2
	movslq	%r14d, %rdi
	incq	%rdi
	testl	%r8d, %r8d
	jle	.LBB5_19
# BB#22:                                # %.lr.ph117.preheader
                                        #   in Loop: Header=BB5_11 Depth=2
	movslq	%r8d, %rbp
	movq	%rdi, %rcx
	addq	%r13, %rcx
	incq	%rbp
	movl	%r15d, %edx
	subl	%r8d, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_23:                               # %.lr.ph117
                                        #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rdx,%rsi), %ebx
	movslq	%ebx, %rbx
	movzbl	(%r12,%rbx), %ebx
	cmpb	(%rcx,%rsi), %bl
	jne	.LBB5_25
# BB#24:                                # %.critedge
                                        #   in Loop: Header=BB5_23 Depth=3
	incq	%rsi
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB5_23
.LBB5_25:                               # %.lr.ph117..critedge102.loopexit_crit_edge
                                        #   in Loop: Header=BB5_11 Depth=2
	leal	(%rdi,%rsi), %edi
	jmp	.LBB5_26
.LBB5_19:                               #   in Loop: Header=BB5_11 Depth=2
	xorl	%esi, %esi
.LBB5_26:                               # %.critedge102
                                        #   in Loop: Header=BB5_11 Depth=2
	movl	$1, %ebx
	cmpl	16(%rsp), %esi          # 4-byte Folded Reload
	jg	.LBB5_27
.LBB5_29:                               # %.thread.backedge
                                        #   in Loop: Header=BB5_11 Depth=2
	cmpl	12(%rsp), %r9d          # 4-byte Folded Reload
	movl	%ebx, %ecx
	movl	%r9d, %esi
	jl	.LBB5_11
	jmp	.LBB5_30
	.p2align	4, 0x90
.LBB5_27:                               #   in Loop: Header=BB5_9 Depth=1
	subl	%esi, %edi
	movslq	%edi, %rdx
	addq	%r13, %rdx
	movl	$1, %ecx
	cmpl	%r10d, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	jne	.LBB5_9
# BB#28:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%r10d, (%rax)
	jmp	.LBB5_31
.LBB5_30:                               # %.thread.outer._crit_edge
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%edx, (%rcx)
	movq	%rax, %rdi
	callq	free
	movq	56(%rsp), %rdx          # 8-byte Reload
.LBB5_31:
	movq	%rdx, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_32:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.calculate_bm_table, %esi
	movl	$60, %edx
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	_fatal
.Lfunc_end5:
	.size	find_lcs, .Lfunc_end5-find_lcs
	.cfi_endproc

	.globl	insert_rule
	.p2align	4, 0x90
	.type	insert_rule,@function
insert_rule:                            # @insert_rule
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi77:
	.cfi_def_cfa_offset 64
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %r15d
	movq	%rdi, %r13
	movq	tree_head(%rip), %r12
	testq	%r12, %r12
	je	.LBB6_1
# BB#2:                                 # %.lr.ph
	movl	%ebx, (%rsp)            # 4-byte Spill
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
                                        # implicit-def: %RBP
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rdi
	movl	16(%r12), %esi
	movq	%r13, %rdx
	movl	%r15d, %ecx
	leaq	4(%rsp), %r8
	callq	find_lcs
	movl	4(%rsp), %ecx
	cmpl	%r14d, %ecx
	cmovaq	%rax, %rbp
	cmoval	%ecx, %r14d
	cmovaq	%r12, %rbx
	movq	32(%r12), %r12
	testq	%r12, %r12
	jne	.LBB6_3
# BB#4:                                 # %._crit_edge
	cmpl	$2, %r14d
	ja	.LBB6_10
# BB#5:                                 # %._crit_edge.thread
	movq	tree_head(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_6
# BB#7:
	movl	(%rax), %edi
	incl	%edi
	jmp	.LBB6_8
.LBB6_1:
	xorl	%edi, %edi
	jmp	.LBB6_9
.LBB6_10:
	movq	8(%rbx), %rdi
	movl	%r14d, %edx
	movq	%rbp, %rsi
	callq	strncpy
	movl	%r14d, 16(%rbx)
	movq	40(%rbx), %rax
	movl	(%rax), %edi
	incl	%edi
	movl	$1, %esi
	movq	%r13, %rdx
	movl	%r15d, %ecx
	movl	(%rsp), %r8d            # 4-byte Reload
	callq	NewPatternNode
	movq	40(%rbx), %rcx
	movq	%rcx, 56(%rax)
	movq	%rax, 40(%rbx)
	jmp	.LBB6_11
.LBB6_6:
	xorl	%edi, %edi
.LBB6_8:                                # %._crit_edge.thread.thread
	movl	(%rsp), %ebx            # 4-byte Reload
.LBB6_9:                                # %._crit_edge.thread.thread
	movq	%r13, %rsi
	movl	%r15d, %edx
	callq	NewStrTreeNode
	movq	%rax, %rbp
	xorl	%edi, %edi
	movl	$1, %esi
	movq	%r13, %rdx
	movl	%r15d, %ecx
	movl	%ebx, %r8d
	callq	NewPatternNode
	movq	$0, 56(%rax)
	movq	%rax, 40(%rbp)
	movq	tree_head(%rip), %rax
	movq	%rax, 32(%rbp)
	movq	%rbp, tree_head(%rip)
.LBB6_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	insert_rule, .Lfunc_end6-insert_rule
	.cfi_endproc

	.globl	db_init
	.p2align	4, 0x90
	.type	db_init,@function
db_init:                                # @db_init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$2088, %rsp             # imm = 0x828
.Lcfi90:
	.cfi_def_cfa_offset 2144
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movl	$.L.str.4, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB7_16
# BB#1:                                 # %.preheader21
	leaq	32(%rsp), %rdi
	movl	$2048, %esi             # imm = 0x800
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB7_15
# BB#2:                                 # %.lr.ph
	leaq	33(%rsp), %r15
	leaq	20(%rsp), %rbx
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_11 Depth 2
	movsbl	32(%rsp), %r8d
	cmpl	$50, %r8d
	je	.LBB7_7
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	cmpl	$49, %r8d
	jne	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=1
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	12(%rsp), %rdx
	callq	sscanf
	movl	$0, 16(%rsp)
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_7:                                #   in Loop: Header=BB7_3 Depth=1
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	leaq	12(%rsp), %rdx
	leaq	16(%rsp), %rcx
	callq	sscanf
.LBB7_8:                                #   in Loop: Header=BB7_3 Depth=1
	movslq	12(%rsp), %r14
	leaq	1(%r14), %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB7_17
# BB#9:                                 # %.preheader20
                                        #   in Loop: Header=BB7_3 Depth=1
	testl	%r14d, %r14d
	jle	.LBB7_14
# BB#10:                                # %.preheader.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	xorl	%ebp, %ebp
	movq	%r15, %r13
	.p2align	4, 0x90
.LBB7_11:                               #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$44, (%r15)
	leaq	1(%r15), %r15
	jne	.LBB7_11
# BB#12:                                #   in Loop: Header=BB7_11 Depth=2
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	sscanf
	movzbl	20(%rsp), %eax
	movb	%al, (%r12,%rbp)
	incq	%rbp
	movslq	12(%rsp), %r14
	cmpq	%r14, %rbp
	jl	.LBB7_11
# BB#13:                                #   in Loop: Header=BB7_3 Depth=1
	movq	%r13, %r15
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB7_14:                               # %._crit_edge
                                        #   in Loop: Header=BB7_3 Depth=1
	movslq	%r14d, %rsi
	movb	$0, (%r12,%rsi)
	movl	16(%rsp), %edx
	movq	%r12, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	insert_rule
	movl	$2048, %esi             # imm = 0x800
	leaq	32(%rsp), %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB7_3
.LBB7_15:                               # %._crit_edge27
	addq	$2088, %rsp             # imm = 0x828
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_6:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.db_init, %esi
	movl	$379, %edx              # imm = 0x17B
	movl	$.L.str.8, %ecx
	xorl	%eax, %eax
	callq	_fatal
.LBB7_17:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.db_init, %esi
	movl	$383, %edx              # imm = 0x17F
	movl	$.L.str.1, %ecx
	xorl	%eax, %eax
	callq	_fatal
.LBB7_16:
	movl	$.L.str, %edi
	movl	$.L__FUNCTION__.db_init, %esi
	movl	$360, %edx              # imm = 0x168
	movl	$.L.str.5, %ecx
	xorl	%eax, %eax
	callq	_fatal
.Lfunc_end7:
	.size	db_init, .Lfunc_end7-db_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/Trimaran/netbench-url/search.c"
	.size	.L.str, 92

	.type	.L__FUNCTION__.calculate_bm_table,@object # @__FUNCTION__.calculate_bm_table
.L__FUNCTION__.calculate_bm_table:
	.asciz	"calculate_bm_table"
	.size	.L__FUNCTION__.calculate_bm_table, 19

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Not enough virtual memory \n"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Source: %x Destination %x \n"
	.size	.L.str.2, 28

	.type	.L__FUNCTION__.NewPatternNode,@object # @__FUNCTION__.NewPatternNode
.L__FUNCTION__.NewPatternNode:
	.asciz	"NewPatternNode"
	.size	.L__FUNCTION__.NewPatternNode, 15

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Run out of virtual memory \n"
	.size	.L.str.3, 28

	.type	.L__FUNCTION__.NewStrTreeNode,@object # @__FUNCTION__.NewStrTreeNode
.L__FUNCTION__.NewStrTreeNode:
	.asciz	"NewStrTreeNode"
	.size	.L__FUNCTION__.NewStrTreeNode, 15

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"r"
	.size	.L.str.4, 2

	.type	.L__FUNCTION__.db_init,@object # @__FUNCTION__.db_init
.L__FUNCTION__.db_init:
	.asciz	"db_init"
	.size	.L__FUNCTION__.db_init, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Can not open the input file\n"
	.size	.L.str.5, 29

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"(%d)"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"(%d:%d)"
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"unrecognized input line start: %c \n"
	.size	.L.str.8, 36

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	" %d"
	.size	.L.str.9, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
