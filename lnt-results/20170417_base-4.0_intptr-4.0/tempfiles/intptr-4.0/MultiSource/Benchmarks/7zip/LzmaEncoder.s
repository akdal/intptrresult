	.text
	.file	"LzmaEncoder.bc"
	.globl	_ZN9NCompress5NLzma8CEncoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma8CEncoderC2Ev,@function
_ZN9NCompress5NLzma8CEncoderC2Ev:       # @_ZN9NCompress5NLzma8CEncoderC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$0, 24(%rbx)
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress5NLzma8CEncoderE+160, 16(%rbx)
	movq	$0, 32(%rbx)
	movl	$_ZN9NCompress5NLzmaL7g_AllocE, %edi
	callq	LzmaEnc_Create
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	popq	%rbx
	retq
.LBB0_2:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end0:
	.size	_ZN9NCompress5NLzma8CEncoderC2Ev, .Lfunc_end0-_ZN9NCompress5NLzma8CEncoderC2Ev
	.cfi_endproc

	.globl	_ZN9NCompress5NLzma8CEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma8CEncoderD2Ev,@function
_ZN9NCompress5NLzma8CEncoderD2Ev:       # @_ZN9NCompress5NLzma8CEncoderD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rdi)
	movq	$_ZTVN9NCompress5NLzma8CEncoderE+160, 16(%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	movl	$_ZN9NCompress5NLzmaL7g_AllocE, %esi
	movl	$_ZN9NCompress5NLzmaL10g_BigAllocE, %edx
	callq	LzmaEnc_Destroy
.LBB1_2:
	popq	%rax
	retq
.Lfunc_end1:
	.size	_ZN9NCompress5NLzma8CEncoderD2Ev, .Lfunc_end1-_ZN9NCompress5NLzma8CEncoderD2Ev
	.cfi_endproc

	.globl	_ZThn8_N9NCompress5NLzma8CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NLzma8CEncoderD1Ev,@function
_ZThn8_N9NCompress5NLzma8CEncoderD1Ev:  # @_ZThn8_N9NCompress5NLzma8CEncoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rdi)
	movq	$_ZTVN9NCompress5NLzma8CEncoderE+160, 8(%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	movl	$_ZN9NCompress5NLzmaL7g_AllocE, %esi
	movl	$_ZN9NCompress5NLzmaL10g_BigAllocE, %edx
	jmp	LzmaEnc_Destroy         # TAILCALL
.LBB2_1:                                # %_ZN9NCompress5NLzma8CEncoderD2Ev.exit
	retq
.Lfunc_end2:
	.size	_ZThn8_N9NCompress5NLzma8CEncoderD1Ev, .Lfunc_end2-_ZThn8_N9NCompress5NLzma8CEncoderD1Ev
	.cfi_endproc

	.globl	_ZThn16_N9NCompress5NLzma8CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NLzma8CEncoderD1Ev,@function
_ZThn16_N9NCompress5NLzma8CEncoderD1Ev: # @_ZThn16_N9NCompress5NLzma8CEncoderD1Ev
	.cfi_startproc
# BB#0:
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -16(%rdi)
	movq	$_ZTVN9NCompress5NLzma8CEncoderE+160, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB3_1
# BB#2:
	movl	$_ZN9NCompress5NLzmaL7g_AllocE, %esi
	movl	$_ZN9NCompress5NLzmaL10g_BigAllocE, %edx
	jmp	LzmaEnc_Destroy         # TAILCALL
.LBB3_1:                                # %_ZN9NCompress5NLzma8CEncoderD2Ev.exit
	retq
.Lfunc_end3:
	.size	_ZThn16_N9NCompress5NLzma8CEncoderD1Ev, .Lfunc_end3-_ZThn16_N9NCompress5NLzma8CEncoderD1Ev
	.cfi_endproc

	.globl	_ZN9NCompress5NLzma8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma8CEncoderD0Ev,@function
_ZN9NCompress5NLzma8CEncoderD0Ev:       # @_ZN9NCompress5NLzma8CEncoderD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress5NLzma8CEncoderE+160, 16(%rbx)
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_2
# BB#1:
.Ltmp0:
	movl	$_ZN9NCompress5NLzmaL7g_AllocE, %esi
	movl	$_ZN9NCompress5NLzmaL10g_BigAllocE, %edx
	callq	LzmaEnc_Destroy
.Ltmp1:
.LBB4_2:                                # %_ZN9NCompress5NLzma8CEncoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_3:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN9NCompress5NLzma8CEncoderD0Ev, .Lfunc_end4-_ZN9NCompress5NLzma8CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end4-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress5NLzma8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NLzma8CEncoderD0Ev,@function
_ZThn8_N9NCompress5NLzma8CEncoderD0Ev:  # @_ZThn8_N9NCompress5NLzma8CEncoderD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	$_ZTVN9NCompress5NLzma8CEncoderE+160, 8(%rax)
	movq	24(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
.Ltmp3:
	movl	$_ZN9NCompress5NLzmaL7g_AllocE, %esi
	movl	$_ZN9NCompress5NLzmaL10g_BigAllocE, %edx
	callq	LzmaEnc_Destroy
.Ltmp4:
.LBB5_2:                                # %_ZN9NCompress5NLzma8CEncoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB5_3:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZThn8_N9NCompress5NLzma8CEncoderD0Ev, .Lfunc_end5-_ZThn8_N9NCompress5NLzma8CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end5-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn16_N9NCompress5NLzma8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NLzma8CEncoderD0Ev,@function
_ZThn16_N9NCompress5NLzma8CEncoderD0Ev: # @_ZThn16_N9NCompress5NLzma8CEncoderD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+96, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN9NCompress5NLzma8CEncoderE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -16(%rax)
	movq	$_ZTVN9NCompress5NLzma8CEncoderE+160, (%rax)
	movq	16(%rax), %rdi
	leaq	-16(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB6_2
# BB#1:
.Ltmp6:
	movl	$_ZN9NCompress5NLzmaL7g_AllocE, %esi
	movl	$_ZN9NCompress5NLzmaL10g_BigAllocE, %edx
	callq	LzmaEnc_Destroy
.Ltmp7:
.LBB6_2:                                # %_ZN9NCompress5NLzma8CEncoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_3:
.Ltmp8:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZThn16_N9NCompress5NLzma8CEncoderD0Ev, .Lfunc_end6-_ZThn16_N9NCompress5NLzma8CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin2    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps,@function
_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps: # @_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movzwl	(%rsi), %ecx
	movl	$-2147024809, %eax      # imm = 0x80070057
	movzwl	%cx, %ecx
	cmpl	$9, %edi
	jne	.LBB7_12
# BB#1:
	cmpl	$8, %ecx
	jne	.LBB7_24
# BB#2:
	movq	8(%rsi), %r8
	movl	(%r8), %edi
	leal	-97(%rdi), %ecx
	leal	-32(%rdi), %esi
	cmpl	$26, %ecx
	cmovael	%edi, %esi
	cmpl	$66, %esi
	je	.LBB7_8
# BB#3:
	cmpl	$72, %esi
	jne	.LBB7_24
# BB#4:
	movl	4(%r8), %ecx
	leal	-97(%rcx), %esi
	leal	-32(%rcx), %edi
	cmpl	$26, %esi
	cmovael	%ecx, %edi
	cmpl	$67, %edi
	jne	.LBB7_24
# BB#5:
	cmpl	$52, 8(%r8)
	jne	.LBB7_24
# BB#6:
	cmpl	$0, 12(%r8)
	jne	.LBB7_24
# BB#7:
	movabsq	$17179869184, %rax      # imm = 0x400000000
	movq	%rax, 28(%rdx)
	jmp	.LBB7_23
.LBB7_12:
	cmpl	$19, %ecx
	jne	.LBB7_24
# BB#13:
	decl	%edi
	cmpl	$11, %edi
	ja	.LBB7_24
# BB#14:
	movl	8(%rsi), %ecx
	jmpq	*.LJTI7_0(,%rdi,8)
.LBB7_18:
	addq	$4, %rdx
	jmp	.LBB7_22
.LBB7_8:
	movl	4(%r8), %ecx
	leal	-97(%rcx), %esi
	leal	-32(%rcx), %edi
	cmpl	$26, %esi
	cmovael	%ecx, %edi
	cmpl	$84, %edi
	jne	.LBB7_24
# BB#9:
	movl	8(%r8), %esi
	leal	-50(%rsi), %ecx
	cmpl	$2, %ecx
	ja	.LBB7_24
# BB#10:
	movl	12(%r8), %ecx
	leal	-97(%rcx), %r8d
	leal	-32(%rcx), %edi
	cmpl	$26, %r8d
	cmovael	%ecx, %edi
	testl	%edi, %edi
	jne	.LBB7_24
# BB#11:
	addl	$-48, %esi
	movl	$1, 28(%rdx)
	movl	%esi, 32(%rdx)
	jmp	.LBB7_23
.LBB7_19:
	addq	$16, %rdx
	jmp	.LBB7_22
.LBB7_16:
	addq	$36, %rdx
	jmp	.LBB7_22
.LBB7_21:
	addq	$8, %rdx
	jmp	.LBB7_22
.LBB7_20:
	addq	$12, %rdx
	jmp	.LBB7_22
.LBB7_15:
	addq	$24, %rdx
	jmp	.LBB7_22
.LBB7_17:
	addq	$20, %rdx
.LBB7_22:
	movl	%ecx, (%rdx)
.LBB7_23:                               # %_ZN9NCompress5NLzmaL16ParseMatchFinderEPKwPiS3_.exit
	xorl	%eax, %eax
.LBB7_24:                               # %_ZN9NCompress5NLzmaL16ParseMatchFinderEPKwPiS3_.exit
	retq
.Lfunc_end7:
	.size	_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps, .Lfunc_end7-_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_18
	.quad	.LBB7_24
	.quad	.LBB7_24
	.quad	.LBB7_24
	.quad	.LBB7_19
	.quad	.LBB7_21
	.quad	.LBB7_20
	.quad	.LBB7_15
	.quad	.LBB7_24
	.quad	.LBB7_16
	.quad	.LBB7_24
	.quad	.LBB7_17

	.text
	.globl	_ZN9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 112
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	leaq	8(%rsp), %rdi
	callq	LzmaEncProps_Init
	testl	%ebp, %ebp
	je	.LBB8_31
# BB#1:                                 # %.lr.ph
	leaq	32(%rsp), %r10
	leaq	20(%rsp), %r12
	leaq	16(%rsp), %r13
	movl	%ebp, %r11d
	addq	$8, %rbx
	xorl	%edx, %edx
	movabsq	$17179869184, %r9       # imm = 0x400000000
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rdx,4), %edi
	cmpl	$13, %edi
	je	.LBB8_6
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpl	$14, %edi
	jne	.LBB8_8
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	movzwl	-8(%rbx), %eax
	cmpl	$11, %eax
	jne	.LBB8_34
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	movzwl	(%rbx), %eax
	xorl	%esi, %esi
	cmpl	$65535, %eax            # imm = 0xFFFF
	sete	%sil
	movl	%esi, 48(%rsp)
	jmp	.LBB8_30
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	movzwl	-8(%rbx), %eax
	cmpl	$19, %eax
	jne	.LBB8_34
# BB#7:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	(%rbx), %eax
	movl	%eax, 52(%rsp)
	jmp	.LBB8_30
	.p2align	4, 0x90
.LBB8_8:                                #   in Loop: Header=BB8_2 Depth=1
	movzwl	-8(%rbx), %esi
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$9, %edi
	jne	.LBB8_16
# BB#9:                                 #   in Loop: Header=BB8_2 Depth=1
	movzwl	%si, %esi
	cmpl	$8, %esi
	jne	.LBB8_32
# BB#10:                                #   in Loop: Header=BB8_2 Depth=1
	movq	(%rbx), %rdi
	movl	(%rdi), %ecx
	leal	-97(%rcx), %ebp
	leal	-32(%rcx), %esi
	cmpl	$26, %ebp
	cmovael	%ecx, %esi
	cmpl	$66, %esi
	je	.LBB8_20
# BB#11:                                #   in Loop: Header=BB8_2 Depth=1
	cmpl	$72, %esi
	jne	.LBB8_32
# BB#12:                                #   in Loop: Header=BB8_2 Depth=1
	movl	4(%rdi), %ecx
	leal	-97(%rcx), %esi
	leal	-32(%rcx), %ebp
	cmpl	$26, %esi
	cmovael	%ecx, %ebp
	cmpl	$67, %ebp
	jne	.LBB8_32
# BB#13:                                #   in Loop: Header=BB8_2 Depth=1
	cmpl	$52, 8(%rdi)
	jne	.LBB8_32
# BB#14:                                #   in Loop: Header=BB8_2 Depth=1
	cmpl	$0, 12(%rdi)
	jne	.LBB8_32
# BB#15:                                #   in Loop: Header=BB8_2 Depth=1
	movq	%r9, 36(%rsp)
	jmp	.LBB8_30
.LBB8_16:                               #   in Loop: Header=BB8_2 Depth=1
	movzwl	%si, %ecx
	cmpl	$19, %ecx
	jne	.LBB8_32
# BB#17:                                #   in Loop: Header=BB8_2 Depth=1
	decl	%edi
	cmpl	$11, %edi
	ja	.LBB8_32
# BB#18:                                #   in Loop: Header=BB8_2 Depth=1
	movl	(%rbx), %r8d
	movq	%r10, %rsi
	jmpq	*.LJTI8_0(,%rdi,8)
.LBB8_19:                               #   in Loop: Header=BB8_2 Depth=1
	leaq	12(%rsp), %rsi
	jmp	.LBB8_29
.LBB8_20:                               #   in Loop: Header=BB8_2 Depth=1
	movl	4(%rdi), %ecx
	leal	-97(%rcx), %esi
	leal	-32(%rcx), %ebp
	cmpl	$26, %esi
	cmovael	%ecx, %ebp
	cmpl	$84, %ebp
	jne	.LBB8_32
# BB#21:                                #   in Loop: Header=BB8_2 Depth=1
	movl	8(%rdi), %esi
	leal	-50(%rsi), %ecx
	cmpl	$2, %ecx
	ja	.LBB8_32
# BB#22:                                #   in Loop: Header=BB8_2 Depth=1
	movl	12(%rdi), %ecx
	leal	-97(%rcx), %edi
	leal	-32(%rcx), %ebp
	cmpl	$26, %edi
	cmovael	%ecx, %ebp
	testl	%ebp, %ebp
	jne	.LBB8_32
# BB#23:                                #   in Loop: Header=BB8_2 Depth=1
	addl	$-48, %esi
	movl	$1, 36(%rsp)
	movl	%esi, 40(%rsp)
	jmp	.LBB8_30
.LBB8_24:                               #   in Loop: Header=BB8_2 Depth=1
	leaq	24(%rsp), %rsi
	jmp	.LBB8_29
.LBB8_25:                               #   in Loop: Header=BB8_2 Depth=1
	movq	%r13, %rsi
	jmp	.LBB8_29
.LBB8_26:                               #   in Loop: Header=BB8_2 Depth=1
	movq	%r12, %rsi
	jmp	.LBB8_29
.LBB8_27:                               #   in Loop: Header=BB8_2 Depth=1
	leaq	44(%rsp), %rsi
	jmp	.LBB8_29
.LBB8_28:                               #   in Loop: Header=BB8_2 Depth=1
	leaq	28(%rsp), %rsi
.LBB8_29:                               #   in Loop: Header=BB8_2 Depth=1
	movl	%r8d, (%rsi)
	.p2align	4, 0x90
.LBB8_30:                               # %_ZN9NCompress5NLzma11SetLzmaPropEjRK14tagPROPVARIANTR14_CLzmaEncProps.exit
                                        #   in Loop: Header=BB8_2 Depth=1
	incq	%rdx
	addq	$16, %rbx
	cmpq	%r11, %rdx
	jb	.LBB8_2
.LBB8_31:                               # %._crit_edge
	movq	32(%r14), %rdi
	leaq	8(%rsp), %rsi
	callq	LzmaEnc_SetProps
	movl	%eax, %edi
	callq	_Z13SResToHRESULTi
	jmp	.LBB8_32
.LBB8_34:
	movl	$-2147024809, %eax      # imm = 0x80070057
.LBB8_32:                               # %.thread
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end8-_ZN9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_19
	.quad	.LBB8_32
	.quad	.LBB8_32
	.quad	.LBB8_32
	.quad	.LBB8_24
	.quad	.LBB8_25
	.quad	.LBB8_26
	.quad	.LBB8_29
	.quad	.LBB8_32
	.quad	.LBB8_27
	.quad	.LBB8_32
	.quad	.LBB8_28

	.text
	.globl	_ZThn8_N9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZThn8_N9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZThn8_N9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj # TAILCALL
.Lfunc_end9:
	.size	_ZThn8_N9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end9-_ZThn8_N9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZN9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZN9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZN9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	$5, 8(%rsp)
	movq	32(%rdi), %rdi
	leaq	3(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	LzmaEnc_WriteProperties
	testl	%eax, %eax
	jne	.LBB10_2
# BB#1:
	movq	8(%rsp), %rdx
	leaq	3(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
.LBB10_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end10:
	.size	_ZN9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end10-_ZN9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZThn16_N9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZThn16_N9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZThn16_N9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 32
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	$5, 8(%rsp)
	movq	16(%rdi), %rdi
	leaq	3(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	LzmaEnc_WriteProperties
	testl	%eax, %eax
	jne	.LBB11_2
# BB#1:
	movq	8(%rsp), %rdx
	leaq	3(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
.LBB11_2:                               # %_ZN9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream.exit
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZThn16_N9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end11-_ZThn16_N9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN9NCompress5NLzma8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress5NLzma8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress5NLzma8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 144
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movq	%rdx, %r14
	movq	%rdi, %r15
	leaq	32(%rsp), %r12
	movq	%r12, %rdi
	callq	_ZN16CSeqInStreamWrapC1EP19ISequentialInStream
	leaq	56(%rsp), %r13
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	_ZN17CSeqOutStreamWrapC1EP20ISequentialOutStream
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN21CCompressProgressWrapC1EP21ICompressProgressInfo
	movq	32(%r15), %rdi
	testq	%rbp, %rbp
	cmoveq	%rbp, %rbx
	movl	$_ZN9NCompress5NLzmaL7g_AllocE, %r8d
	movl	$_ZN9NCompress5NLzmaL10g_BigAllocE, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	LzmaEnc_Encode
	cmpl	$10, %eax
	je	.LBB12_5
# BB#1:
	cmpl	$9, %eax
	je	.LBB12_4
# BB#2:
	cmpl	$8, %eax
	jne	.LBB12_7
# BB#3:
	movl	48(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB12_8
	jmp	.LBB12_7
.LBB12_5:
	movl	24(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB12_8
	jmp	.LBB12_7
.LBB12_4:
	movl	72(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB12_8
.LBB12_7:                               # %.thread
	movl	%eax, %edi
	callq	_Z13SResToHRESULTi
	movl	%eax, %ecx
.LBB12_8:
	movl	%ecx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN9NCompress5NLzma8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end12-_ZN9NCompress5NLzma8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc

	.section	.text._ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB13_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB13_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB13_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB13_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB13_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB13_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB13_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB13_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB13_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB13_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB13_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB13_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB13_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB13_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB13_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB13_16
.LBB13_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressSetCoderProperties(%rip), %cl
	jne	.LBB13_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+1(%rip), %al
	jne	.LBB13_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+2(%rip), %al
	jne	.LBB13_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+3(%rip), %al
	jne	.LBB13_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+4(%rip), %al
	jne	.LBB13_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+5(%rip), %al
	jne	.LBB13_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+6(%rip), %al
	jne	.LBB13_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+7(%rip), %al
	jne	.LBB13_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+8(%rip), %al
	jne	.LBB13_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+9(%rip), %al
	jne	.LBB13_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+10(%rip), %al
	jne	.LBB13_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+11(%rip), %al
	jne	.LBB13_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+12(%rip), %al
	jne	.LBB13_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+13(%rip), %al
	jne	.LBB13_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+14(%rip), %al
	jne	.LBB13_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetCoderProperties+15(%rip), %al
	jne	.LBB13_33
.LBB13_16:
	leaq	8(%rdi), %rax
	jmp	.LBB13_50
.LBB13_33:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressWriteCoderProperties(%rip), %cl
	jne	.LBB13_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+1(%rip), %cl
	jne	.LBB13_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+2(%rip), %cl
	jne	.LBB13_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+3(%rip), %cl
	jne	.LBB13_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+4(%rip), %cl
	jne	.LBB13_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+5(%rip), %cl
	jne	.LBB13_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+6(%rip), %cl
	jne	.LBB13_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+7(%rip), %cl
	jne	.LBB13_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+8(%rip), %cl
	jne	.LBB13_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+9(%rip), %cl
	jne	.LBB13_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+10(%rip), %cl
	jne	.LBB13_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+11(%rip), %cl
	jne	.LBB13_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+12(%rip), %cl
	jne	.LBB13_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+13(%rip), %cl
	jne	.LBB13_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+14(%rip), %cl
	jne	.LBB13_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_ICompressWriteCoderProperties+15(%rip), %cl
	jne	.LBB13_51
# BB#49:
	leaq	16(%rdi), %rax
.LBB13_50:                              # %_ZeqRK4GUIDS1_.exit10.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB13_51:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end13-_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress5NLzma8CEncoder6AddRefEv,"axG",@progbits,_ZN9NCompress5NLzma8CEncoder6AddRefEv,comdat
	.weak	_ZN9NCompress5NLzma8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma8CEncoder6AddRefEv,@function
_ZN9NCompress5NLzma8CEncoder6AddRefEv:  # @_ZN9NCompress5NLzma8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN9NCompress5NLzma8CEncoder6AddRefEv, .Lfunc_end14-_ZN9NCompress5NLzma8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress5NLzma8CEncoder7ReleaseEv,"axG",@progbits,_ZN9NCompress5NLzma8CEncoder7ReleaseEv,comdat
	.weak	_ZN9NCompress5NLzma8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzma8CEncoder7ReleaseEv,@function
_ZN9NCompress5NLzma8CEncoder7ReleaseEv: # @_ZN9NCompress5NLzma8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB15_2:
	popq	%rcx
	retq
.Lfunc_end15:
	.size	_ZN9NCompress5NLzma8CEncoder7ReleaseEv, .Lfunc_end15-_ZN9NCompress5NLzma8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end16:
	.size	_ZThn8_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end16-_ZThn8_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NLzma8CEncoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress5NLzma8CEncoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress5NLzma8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NLzma8CEncoder6AddRefEv,@function
_ZThn8_N9NCompress5NLzma8CEncoder6AddRefEv: # @_ZThn8_N9NCompress5NLzma8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end17:
	.size	_ZThn8_N9NCompress5NLzma8CEncoder6AddRefEv, .Lfunc_end17-_ZThn8_N9NCompress5NLzma8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NLzma8CEncoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress5NLzma8CEncoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress5NLzma8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NLzma8CEncoder7ReleaseEv,@function
_ZThn8_N9NCompress5NLzma8CEncoder7ReleaseEv: # @_ZThn8_N9NCompress5NLzma8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB18_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB18_2:                               # %_ZN9NCompress5NLzma8CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZThn8_N9NCompress5NLzma8CEncoder7ReleaseEv, .Lfunc_end18-_ZThn8_N9NCompress5NLzma8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end19:
	.size	_ZThn16_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end19-_ZThn16_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress5NLzma8CEncoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress5NLzma8CEncoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress5NLzma8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NLzma8CEncoder6AddRefEv,@function
_ZThn16_N9NCompress5NLzma8CEncoder6AddRefEv: # @_ZThn16_N9NCompress5NLzma8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end20:
	.size	_ZThn16_N9NCompress5NLzma8CEncoder6AddRefEv, .Lfunc_end20-_ZThn16_N9NCompress5NLzma8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress5NLzma8CEncoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress5NLzma8CEncoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress5NLzma8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress5NLzma8CEncoder7ReleaseEv,@function
_ZThn16_N9NCompress5NLzma8CEncoder7ReleaseEv: # @_ZThn16_N9NCompress5NLzma8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB21_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB21_2:                               # %_ZN9NCompress5NLzma8CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZThn16_N9NCompress5NLzma8CEncoder7ReleaseEv, .Lfunc_end21-_ZThn16_N9NCompress5NLzma8CEncoder7ReleaseEv
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzmaL7SzAllocEPvm,@function
_ZN9NCompress5NLzmaL7SzAllocEPvm:       # @_ZN9NCompress5NLzmaL7SzAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyAlloc                 # TAILCALL
.Lfunc_end22:
	.size	_ZN9NCompress5NLzmaL7SzAllocEPvm, .Lfunc_end22-_ZN9NCompress5NLzmaL7SzAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzmaL6SzFreeEPvS1_,@function
_ZN9NCompress5NLzmaL6SzFreeEPvS1_:      # @_ZN9NCompress5NLzmaL6SzFreeEPvS1_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	MyFree                  # TAILCALL
.Lfunc_end23:
	.size	_ZN9NCompress5NLzmaL6SzFreeEPvS1_, .Lfunc_end23-_ZN9NCompress5NLzmaL6SzFreeEPvS1_
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzmaL10SzBigAllocEPvm,@function
_ZN9NCompress5NLzmaL10SzBigAllocEPvm:   # @_ZN9NCompress5NLzmaL10SzBigAllocEPvm
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigAlloc                # TAILCALL
.Lfunc_end24:
	.size	_ZN9NCompress5NLzmaL10SzBigAllocEPvm, .Lfunc_end24-_ZN9NCompress5NLzmaL10SzBigAllocEPvm
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress5NLzmaL9SzBigFreeEPvS1_,@function
_ZN9NCompress5NLzmaL9SzBigFreeEPvS1_:   # @_ZN9NCompress5NLzmaL9SzBigFreeEPvS1_
	.cfi_startproc
# BB#0:
	movq	%rsi, %rdi
	jmp	BigFree                 # TAILCALL
.Lfunc_end25:
	.size	_ZN9NCompress5NLzmaL9SzBigFreeEPvS1_, .Lfunc_end25-_ZN9NCompress5NLzmaL9SzBigFreeEPvS1_
	.cfi_endproc

	.type	_ZTVN9NCompress5NLzma8CEncoderE,@object # @_ZTVN9NCompress5NLzma8CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress5NLzma8CEncoderE
	.p2align	3
_ZTVN9NCompress5NLzma8CEncoderE:
	.quad	0
	.quad	_ZTIN9NCompress5NLzma8CEncoderE
	.quad	_ZN9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress5NLzma8CEncoder6AddRefEv
	.quad	_ZN9NCompress5NLzma8CEncoder7ReleaseEv
	.quad	_ZN9NCompress5NLzma8CEncoderD2Ev
	.quad	_ZN9NCompress5NLzma8CEncoderD0Ev
	.quad	_ZN9NCompress5NLzma8CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	_ZN9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.quad	-8
	.quad	_ZTIN9NCompress5NLzma8CEncoderE
	.quad	_ZThn8_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress5NLzma8CEncoder6AddRefEv
	.quad	_ZThn8_N9NCompress5NLzma8CEncoder7ReleaseEv
	.quad	_ZThn8_N9NCompress5NLzma8CEncoderD1Ev
	.quad	_ZThn8_N9NCompress5NLzma8CEncoderD0Ev
	.quad	_ZThn8_N9NCompress5NLzma8CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	-16
	.quad	_ZTIN9NCompress5NLzma8CEncoderE
	.quad	_ZThn16_N9NCompress5NLzma8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress5NLzma8CEncoder6AddRefEv
	.quad	_ZThn16_N9NCompress5NLzma8CEncoder7ReleaseEv
	.quad	_ZThn16_N9NCompress5NLzma8CEncoderD1Ev
	.quad	_ZThn16_N9NCompress5NLzma8CEncoderD0Ev
	.quad	_ZThn16_N9NCompress5NLzma8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.size	_ZTVN9NCompress5NLzma8CEncoderE, 208

	.type	_ZN9NCompress5NLzmaL7g_AllocE,@object # @_ZN9NCompress5NLzmaL7g_AllocE
	.data
	.p2align	3
_ZN9NCompress5NLzmaL7g_AllocE:
	.quad	_ZN9NCompress5NLzmaL7SzAllocEPvm
	.quad	_ZN9NCompress5NLzmaL6SzFreeEPvS1_
	.size	_ZN9NCompress5NLzmaL7g_AllocE, 16

	.type	_ZN9NCompress5NLzmaL10g_BigAllocE,@object # @_ZN9NCompress5NLzmaL10g_BigAllocE
	.p2align	3
_ZN9NCompress5NLzmaL10g_BigAllocE:
	.quad	_ZN9NCompress5NLzmaL10SzBigAllocEPvm
	.quad	_ZN9NCompress5NLzmaL9SzBigFreeEPvS1_
	.size	_ZN9NCompress5NLzmaL10g_BigAllocE, 16

	.type	_ZTSN9NCompress5NLzma8CEncoderE,@object # @_ZTSN9NCompress5NLzma8CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress5NLzma8CEncoderE
	.p2align	4
_ZTSN9NCompress5NLzma8CEncoderE:
	.asciz	"N9NCompress5NLzma8CEncoderE"
	.size	_ZTSN9NCompress5NLzma8CEncoderE, 28

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS27ICompressSetCoderProperties,@object # @_ZTS27ICompressSetCoderProperties
	.section	.rodata._ZTS27ICompressSetCoderProperties,"aG",@progbits,_ZTS27ICompressSetCoderProperties,comdat
	.weak	_ZTS27ICompressSetCoderProperties
	.p2align	4
_ZTS27ICompressSetCoderProperties:
	.asciz	"27ICompressSetCoderProperties"
	.size	_ZTS27ICompressSetCoderProperties, 30

	.type	_ZTI27ICompressSetCoderProperties,@object # @_ZTI27ICompressSetCoderProperties
	.section	.rodata._ZTI27ICompressSetCoderProperties,"aG",@progbits,_ZTI27ICompressSetCoderProperties,comdat
	.weak	_ZTI27ICompressSetCoderProperties
	.p2align	4
_ZTI27ICompressSetCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27ICompressSetCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI27ICompressSetCoderProperties, 24

	.type	_ZTS29ICompressWriteCoderProperties,@object # @_ZTS29ICompressWriteCoderProperties
	.section	.rodata._ZTS29ICompressWriteCoderProperties,"aG",@progbits,_ZTS29ICompressWriteCoderProperties,comdat
	.weak	_ZTS29ICompressWriteCoderProperties
	.p2align	4
_ZTS29ICompressWriteCoderProperties:
	.asciz	"29ICompressWriteCoderProperties"
	.size	_ZTS29ICompressWriteCoderProperties, 32

	.type	_ZTI29ICompressWriteCoderProperties,@object # @_ZTI29ICompressWriteCoderProperties
	.section	.rodata._ZTI29ICompressWriteCoderProperties,"aG",@progbits,_ZTI29ICompressWriteCoderProperties,comdat
	.weak	_ZTI29ICompressWriteCoderProperties
	.p2align	4
_ZTI29ICompressWriteCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29ICompressWriteCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI29ICompressWriteCoderProperties, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress5NLzma8CEncoderE,@object # @_ZTIN9NCompress5NLzma8CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress5NLzma8CEncoderE
	.p2align	4
_ZTIN9NCompress5NLzma8CEncoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress5NLzma8CEncoderE
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	2050                    # 0x802
	.quad	_ZTI29ICompressWriteCoderProperties
	.quad	4098                    # 0x1002
	.quad	_ZTI13CMyUnknownImp
	.quad	6146                    # 0x1802
	.size	_ZTIN9NCompress5NLzma8CEncoderE, 88


	.globl	_ZN9NCompress5NLzma8CEncoderC1Ev
	.type	_ZN9NCompress5NLzma8CEncoderC1Ev,@function
_ZN9NCompress5NLzma8CEncoderC1Ev = _ZN9NCompress5NLzma8CEncoderC2Ev
	.globl	_ZN9NCompress5NLzma8CEncoderD1Ev
	.type	_ZN9NCompress5NLzma8CEncoderD1Ev,@function
_ZN9NCompress5NLzma8CEncoderD1Ev = _ZN9NCompress5NLzma8CEncoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
