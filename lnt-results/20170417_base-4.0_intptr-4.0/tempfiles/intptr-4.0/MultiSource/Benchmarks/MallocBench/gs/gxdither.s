	.text
	.file	"gxdither.bc"
	.globl	gx_color_render
	.p2align	4, 0x90
	.type	gx_color_render,@function
gx_color_render:                        # @gx_color_render
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	448(%rbp), %rcx
	movb	8(%rdi), %al
	testb	%al, %al
	je	.LBB0_6
# BB#1:
	movzwl	6(%rdi), %edx
	cmpl	$65535, %edx            # imm = 0xFFFF
	je	.LBB0_5
# BB#2:
	testw	%dx, %dx
	jne	.LBB0_6
# BB#3:
	movq	24(%rcx), %rax
	jmp	.LBB0_4
.LBB0_6:
	movq	(%rcx), %r15
	movzwl	44(%r15), %ebx
	cmpq	$255, %rbx
	jb	.LBB0_8
# BB#7:
	incq	%rbx
	movzwl	(%rdi), %esi
	imull	%ebx, %esi
	movzwl	2(%rdi), %edx
	imull	%ebx, %edx
	movzwl	4(%rdi), %ecx
	imull	%ebx, %ecx
	movq	8(%r15), %rax
	shrl	$16, %esi
	shrl	$16, %edx
	shrl	$16, %ecx
.LBB0_18:
	movq	%r15, %rdi
	callq	*40(%rax)
	jmp	.LBB0_4
.LBB0_8:
	movq	288(%rbp), %rcx
	movl	24(%rcx), %r12d
	testb	%al, %al
	setne	%al
	cmpl	$0, 40(%r15)
	je	.LBB0_10
# BB#9:
	testb	%al, %al
	jne	.LBB0_10
# BB#16:
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movzwl	(%rdi), %ebp
	imulq	%rbx, %rbp
	movzwl	2(%rdi), %esi
	imulq	%rbx, %rsi
	movzwl	4(%rdi), %ecx
	imulq	%rbx, %rcx
	movabsq	$-9223231297218904063, %rdi # imm = 0x8000800080008001
	movq	%rbp, %rax
	mulq	%rdi
	movq	%rdx, %r9
	shrq	$15, %r9
	movq	%rsi, %rax
	mulq	%rdi
	movq	%rdx, %rbx
	shrq	$15, %rbx
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rdx, %rdi
	shrq	$15, %rdi
	movzwl	%r9w, %eax
	imull	$-65535, %eax, %r12d    # imm = 0xFFFF0001
	addl	%ebp, %r12d
	movzwl	%bx, %eax
	imull	$-65535, %eax, %r13d    # imm = 0xFFFF0001
	addl	%esi, %r13d
	movzwl	%di, %eax
	imull	$-65535, %eax, %ebp     # imm = 0xFFFF0001
	addl	%ecx, %ebp
	movl	%r13d, %eax
	orl	%r12d, %eax
	orw	%bp, %ax
	je	.LBB0_17
# BB#19:
	movzwl	%r12w, %ecx
	movzwl	%r13w, %eax
	movw	$1, %r8w
	cmpl	$32768, %ecx            # imm = 0x8000
	jb	.LBB0_20
# BB#21:
	xorl	$65535, %ecx            # imm = 0xFFFF
	leal	1(%r9), %r9d
	movw	$30, %dx
	movw	$-1, %si
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%ecx, %r12d
	jmp	.LBB0_22
.LBB0_5:
	movq	16(%rcx), %rax
.LBB0_4:
	movq	%rax, (%r14)
	movq	%rax, 8(%r14)
	movl	$0, 16(%r14)
.LBB0_35:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_10:
	imulq	%r12, %rbx
	incq	%rbx
	cmpb	$0, 9(%rdi)
	je	.LBB0_12
# BB#11:
	movzwl	6(%rdi), %eax
	jmp	.LBB0_13
.LBB0_17:                               # %.critedge
	movq	8(%r15), %rax
	movzwl	%r9w, %esi
	movzwl	%bx, %edx
	movzwl	%di, %ecx
	jmp	.LBB0_18
.LBB0_12:
	callq	gx_color_luminance
.LBB0_13:
	movzwl	%ax, %eax
	imulq	%rbx, %rax
	shrq	$16, %rax
	xorl	%edx, %edx
	divq	%r12
	movq	%rax, %rbx
	movl	%edx, 16(%r14)
	movq	8(%r15), %rax
	movzwl	%bx, %esi
	movq	%r15, %rdi
	movl	%esi, %edx
	movl	%esi, %ecx
	callq	*40(%rax)
	movq	%rax, (%r14)
	cmpl	$0, 16(%r14)
	je	.LBB0_14
# BB#15:
	incl	%ebx
	movq	8(%r15), %rax
	movzwl	%bx, %esi
	movq	%r15, %rdi
	movl	%esi, %edx
	movl	%esi, %ecx
	callq	*40(%rax)
	movq	%rax, 8(%r14)
	movq	%r14, %rdi
	movq	%rbp, %rsi
	jmp	.LBB0_40
.LBB0_14:
	movq	%rax, 8(%r14)
	jmp	.LBB0_35
.LBB0_20:
	xorl	%edx, %edx
	movw	$1, %cx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
.LBB0_22:
	movzwl	%bp, %ecx
	cmpl	$32768, %eax            # imm = 0x8000
	jb	.LBB0_24
# BB#23:
	xorl	$65535, %eax            # imm = 0xFFFF
	leal	1(%rbx), %ebx
	movswl	%dx, %edx
	addl	$59, %edx
	movw	$-1, %r8w
	movl	%eax, %r13d
.LBB0_24:
	cmpl	$32768, %ecx            # imm = 0x8000
	movl	%r8d, 20(%rsp)          # 4-byte Spill
	jb	.LBB0_25
# BB#26:
	xorl	$65535, %ecx            # imm = 0xFFFF
	leal	1(%rdi), %edi
	addl	$11, %edx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movw	$-1, %ax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%ecx, %ebp
	jmp	.LBB0_27
.LBB0_25:
	movw	$1, %ax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%edx, 16(%rsp)          # 4-byte Spill
.LBB0_27:
	movq	8(%r15), %rax
	movzwl	%r9w, %esi
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movzwl	%bx, %edx
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movzwl	%di, %ecx
	movq	%r15, %rdi
	movq	%r9, %rbx
	callq	*40(%rax)
	movq	%rax, %r8
	movq	%r8, (%r14)
	movzwl	%r13w, %edx
	movzwl	%r12w, %ecx
	movzwl	%bp, %edi
	movq	%rbx, %r11
	xorl	%esi, %esi
	cmpl	%ecx, %edx
	jbe	.LBB0_29
# BB#28:
	cmpl	%edx, %edi
	seta	%sil
	movl	%edi, %eax
	cmovbew	%r13w, %ax
	leal	2(%rsi,%rsi), %r10d
	xorl	%esi, %esi
	cmpl	%ecx, %edi
	seta	%sil
	cmoval	%r13d, %r12d
	cmovbel	%r13d, %ebp
	leal	3(%rsi,%rsi,2), %r9d
	jmp	.LBB0_30
.LBB0_29:
	cmpl	%ecx, %edi
	seta	%sil
	movl	%edi, %eax
	cmovbew	%r12w, %ax
	leal	1(%rsi,%rsi,2), %r10d
	xorl	%esi, %esi
	cmpl	%edx, %edi
	seta	%sil
	cmovbel	%r12d, %ebp
	cmoval	%r12d, %r13d
	leal	3(%rsi,%rsi), %r9d
	movl	%ebp, %r12d
	movl	%r13d, %ebp
.LBB0_30:
	movq	40(%rsp), %r13          # 8-byte Reload
	movzwl	%r12w, %esi
	movzwl	%bp, %ebp
	addl	%esi, %ebp
	movzwl	%ax, %eax
	imulq	$100, %rax, %rbx
	imulq	$71, %rbp, %rsi
	addl	%ecx, %edx
	addl	%edi, %edx
	imulq	$62, %rdx, %rcx
	cmpq	%rsi, %rbx
	jbe	.LBB0_32
# BB#31:
	cmpq	%rbx, %rcx
	cmovaq	%rdx, %rax
	movl	$196605, %edx           # imm = 0x2FFFD
	movl	$65535, %ecx            # imm = 0xFFFF
	cmovaq	%rdx, %rcx
	movl	$7, %edx
	cmoval	%edx, %r10d
	jmp	.LBB0_33
.LBB0_32:
	cmpq	%rsi, %rcx
	cmovaq	%rdx, %rbp
	movl	$196605, %eax           # imm = 0x2FFFD
	movl	$131070, %ecx           # imm = 0x1FFFE
	cmovaq	%rax, %rcx
	movl	$7, %eax
	cmoval	%eax, %r9d
	movq	%rbp, %rax
	movl	%r9d, %r10d
.LBB0_33:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	imulq	%r13, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %r12
	movl	%r12d, 16(%r14)
	testl	%r12d, %r12d
	je	.LBB0_34
# BB#36:
	xorl	%eax, %eax
	testb	$1, %r10b
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovew	%ax, %cx
	addl	%ecx, %r11d
	testb	$2, %r10b
	movl	20(%rsp), %ecx          # 4-byte Reload
	cmovew	%ax, %cx
	addl	%ecx, %edi
	testb	$4, %r10b
	movl	8(%rsp), %ecx           # 4-byte Reload
	cmovew	%ax, %cx
	addl	%ecx, %ebp
	movslq	%r10d, %rax
	movl	16(%rsp), %ecx          # 4-byte Reload
	cmpw	%cx, lum(%rax,%rax)
	jae	.LBB0_38
# BB#37:
	movq	%r8, 8(%r14)
	movq	8(%r15), %rax
	movzwl	%r11w, %esi
	movzwl	%di, %edx
	movzwl	%bp, %ecx
	movq	%r15, %rdi
	callq	*40(%rax)
	movq	%rax, (%r14)
	subl	%r12d, %r13d
	movl	%r13d, 16(%r14)
	jmp	.LBB0_39
.LBB0_34:
	movq	%r8, 8(%r14)
	jmp	.LBB0_35
.LBB0_38:
	movq	8(%r15), %rax
	movzwl	%r11w, %esi
	movzwl	%di, %edx
	movzwl	%bp, %ecx
	movq	%r15, %rdi
	callq	*40(%rax)
	movq	%rax, 8(%r14)
.LBB0_39:
	movq	%r14, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB0_40:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	gx_color_load           # TAILCALL
.Lfunc_end0:
	.size	gx_color_render, .Lfunc_end0-gx_color_render
	.cfi_endproc

	.type	lum,@object             # @lum
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
lum:
	.short	0                       # 0x0
	.short	30                      # 0x1e
	.short	59                      # 0x3b
	.short	89                      # 0x59
	.short	11                      # 0xb
	.short	41                      # 0x29
	.short	70                      # 0x46
	.short	100                     # 0x64
	.size	lum, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
