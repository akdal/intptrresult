	.text
	.file	"matrix.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI0_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	_Z8mkmatrixii
	.p2align	4, 0x90
	.type	_Z8mkmatrixii,@function
_Z8mkmatrixii:                          # @_Z8mkmatrixii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movl	%edi, %r14d
	movslq	%r14d, %rbx
	leaq	(,%rbx,8), %rdi
	callq	malloc
	movq	%rax, (%rsp)            # 8-byte Spill
	testl	%ebx, %ebx
	jle	.LBB0_14
# BB#1:                                 # %.lr.ph28
	movslq	%ebp, %rax
	leaq	(,%rax,4), %r15
	testl	%eax, %eax
	jle	.LBB0_2
# BB#4:                                 # %.lr.ph28.split.us.preheader
	movl	%ebp, %r13d
	movl	%r14d, %ebx
	movl	%ebp, %eax
	andl	$7, %eax
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subq	%rax, %r13
	movl	$1, %r14d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph28.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_12 Depth 2
	movq	%r15, %rdi
	callq	malloc
	cmpl	$8, %ebp
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx,%r12,8)
	jb	.LBB0_6
# BB#7:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_5 Depth=1
	testq	%r13, %r13
	je	.LBB0_6
# BB#8:                                 # %vector.ph
                                        #   in Loop: Header=BB0_5 Depth=1
	leal	(%r14,%r13), %ecx
	movd	%r14d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	paddd	.LCPI0_0(%rip), %xmm0
	leaq	16(%rax), %rdx
	movq	%r13, %rsi
	movdqa	.LCPI0_1(%rip), %xmm2   # xmm2 = [4,4,4,4]
	movdqa	.LCPI0_2(%rip), %xmm3   # xmm3 = [8,8,8,8]
	.p2align	4, 0x90
.LBB0_9:                                # %vector.body
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	%xmm0, %xmm1
	paddd	%xmm2, %xmm1
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	paddd	%xmm3, %xmm0
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB0_9
# BB#10:                                # %middle.block
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	movq	%r13, %rsi
	jne	.LBB0_11
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_5 Depth=1
	xorl	%esi, %esi
	movl	%r14d, %ecx
.LBB0_11:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	leaq	(%rax,%rsi,4), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, (%rax)
	incl	%ecx
	addq	$4, %rax
	decq	%rdx
	jne	.LBB0_12
.LBB0_13:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_5 Depth=1
	addl	%ebp, %r14d
	incq	%r12
	cmpq	%rbx, %r12
	jne	.LBB0_5
	jmp	.LBB0_14
.LBB0_2:                                # %.lr.ph28.split.preheader
	movl	%r14d, %ebp
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph28.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, (%rbx)
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB0_3
.LBB0_14:                               # %._crit_edge29
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z8mkmatrixii, .Lfunc_end0-_Z8mkmatrixii
	.cfi_endproc

	.globl	_Z10zeromatrixiiPPi
	.p2align	4, 0x90
	.type	_Z10zeromatrixiiPPi,@function
_Z10zeromatrixiiPPi:                    # @_Z10zeromatrixiiPPi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%edi, %edi
	jle	.LBB1_7
# BB#1:
	testl	%esi, %esi
	jle	.LBB1_7
# BB#2:                                 # %.preheader.us.preheader
	decl	%esi
	leaq	4(,%rsi,4), %r12
	movl	%edi, %r15d
	leaq	-1(%r15), %r13
	movq	%r15, %rbp
	xorl	%ebx, %ebx
	andq	$7, %rbp
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB1_3
.LBB1_4:                                # %.preheader.us.prol.loopexit
	cmpq	$7, %r13
	jb	.LBB1_7
# BB#5:                                 # %.preheader.us.preheader.new
	subq	%rbx, %r15
	leaq	56(%r14,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB1_6
.LBB1_7:                                # %._crit_edge14
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z10zeromatrixiiPPi, .Lfunc_end1-_Z10zeromatrixiiPPi
	.cfi_endproc

	.globl	_Z10freematrixiPPi
	.p2align	4, 0x90
	.type	_Z10freematrixiPPi,@function
_Z10freematrixiPPi:                     # @_Z10freematrixiPPi
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	testl	%edi, %edi
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edi, %rbx
	incq	%rbx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%r14,%rbx,8), %rdi
	callq	free
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB2_2
.LBB2_3:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	_Z10freematrixiPPi, .Lfunc_end2-_Z10freematrixiPPi
	.cfi_endproc

	.globl	_Z5mmultiiPPiS0_S0_
	.p2align	4, 0x90
	.type	_Z5mmultiiPPiS0_S0_,@function
_Z5mmultiiPPiS0_S0_:                    # @_Z5mmultiiPPiS0_S0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -48
.Lcfi37:
	.cfi_offset %r12, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	testl	%edi, %edi
	jle	.LBB3_9
# BB#1:
	testl	%esi, %esi
	jle	.LBB3_9
# BB#2:                                 # %.preheader.us.us.preheader.preheader
	movl	%esi, %r15d
	movl	%edi, %r9d
	leaq	-1(%r15), %r11
	movl	%r15d, %r12d
	andl	$3, %r12d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader.us.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
                                        #       Child Loop BB3_5 Depth 3
                                        #       Child Loop BB3_10 Depth 3
	movq	(%rdx,%r10,8), %rdi
	movq	(%r8,%r10,8), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_4:                                # %.preheader.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_5 Depth 3
                                        #       Child Loop BB3_10 Depth 3
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	testq	%r12, %r12
	je	.LBB3_6
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx,%rsi,8), %rax
	movl	(%rax,%rbx,4), %eax
	imull	(%rdi,%rsi,4), %eax
	addl	%eax, %ebp
	incq	%rsi
	cmpq	%rsi, %r12
	jne	.LBB3_5
.LBB3_6:                                # %.prol.loopexit
                                        #   in Loop: Header=BB3_4 Depth=2
	cmpq	$3, %r11
	jb	.LBB3_7
	.p2align	4, 0x90
.LBB3_10:                               #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx,%rsi,8), %rax
	movl	(%rax,%rbx,4), %eax
	imull	(%rdi,%rsi,4), %eax
	addl	%ebp, %eax
	movq	8(%rcx,%rsi,8), %rbp
	movl	(%rbp,%rbx,4), %ebp
	imull	4(%rdi,%rsi,4), %ebp
	addl	%eax, %ebp
	movq	16(%rcx,%rsi,8), %rax
	movl	(%rax,%rbx,4), %eax
	imull	8(%rdi,%rsi,4), %eax
	addl	%ebp, %eax
	movq	24(%rcx,%rsi,8), %rbp
	movl	(%rbp,%rbx,4), %ebp
	imull	12(%rdi,%rsi,4), %ebp
	addl	%eax, %ebp
	addq	$4, %rsi
	cmpq	%rsi, %r15
	jne	.LBB3_10
.LBB3_7:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB3_4 Depth=2
	movl	%ebp, (%r14,%rbx,4)
	incq	%rbx
	cmpq	%rsi, %rbx
	jne	.LBB3_4
# BB#8:                                 # %._crit_edge37.us
                                        #   in Loop: Header=BB3_3 Depth=1
	incq	%r10
	cmpq	%r9, %r10
	jne	.LBB3_3
.LBB3_9:                                # %._crit_edge39
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_Z5mmultiiPPiS0_S0_, .Lfunc_end3-_Z5mmultiiPPiS0_S0_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
.LCPI4_1:
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
.LCPI4_2:
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
.LCPI4_3:
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
.LCPI4_4:
	.long	20                      # 0x14
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	23                      # 0x17
.LCPI4_5:
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	27                      # 0x1b
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi47:
	.cfi_def_cfa_offset 592
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movl	$100000, %eax           # imm = 0x186A0
	movq	%rax, (%rsp)            # 8-byte Spill
	cmpl	$2, %edi
	jne	.LBB4_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB4_2:
	movl	$240, %edi
	callq	malloc
	movq	%rax, %rbp
	movl	$1, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph28.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$120, %edi
	callq	malloc
	movq	%rax, (%rbp,%r14,8)
	movl	%ebx, (%rax)
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_0(%rip), %xmm1
	movdqu	%xmm1, 4(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_1(%rip), %xmm1
	movdqu	%xmm1, 20(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_2(%rip), %xmm1
	leal	13(%rbx), %ecx
	movdqu	%xmm1, 36(%rax)
	movl	%ecx, 52(%rax)
	leal	14(%rbx), %ecx
	movl	%ecx, 56(%rax)
	leal	15(%rbx), %ecx
	movl	%ecx, 60(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_3(%rip), %xmm1
	movdqu	%xmm1, 64(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_4(%rip), %xmm1
	movdqu	%xmm1, 80(%rax)
	paddd	.LCPI4_5(%rip), %xmm0
	movdqu	%xmm0, 96(%rax)
	leal	28(%rbx), %ecx
	movl	%ecx, 112(%rax)
	leal	29(%rbx), %ecx
	movl	%ecx, 116(%rax)
	incq	%r14
	addl	$30, %ebx
	cmpq	$30, %r14
	jne	.LBB4_3
# BB#4:                                 # %_Z8mkmatrixii.exit
	movl	$240, %edi
	callq	malloc
	movq	%rax, %r14
	movl	$1, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph28.split.us.i27
                                        # =>This Inner Loop Header: Depth=1
	movl	$120, %edi
	callq	malloc
	movq	%rax, (%r14,%r15,8)
	movl	%ebx, (%rax)
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_0(%rip), %xmm1
	movdqu	%xmm1, 4(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_1(%rip), %xmm1
	movdqu	%xmm1, 20(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_2(%rip), %xmm1
	leal	13(%rbx), %ecx
	movdqu	%xmm1, 36(%rax)
	movl	%ecx, 52(%rax)
	leal	14(%rbx), %ecx
	movl	%ecx, 56(%rax)
	leal	15(%rbx), %ecx
	movl	%ecx, 60(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_3(%rip), %xmm1
	movdqu	%xmm1, 64(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_4(%rip), %xmm1
	movdqu	%xmm1, 80(%rax)
	paddd	.LCPI4_5(%rip), %xmm0
	movdqu	%xmm0, 96(%rax)
	leal	28(%rbx), %ecx
	movl	%ecx, 112(%rax)
	leal	29(%rbx), %ecx
	movl	%ecx, 116(%rax)
	incq	%r15
	addl	$30, %ebx
	cmpq	$30, %r15
	jne	.LBB4_5
# BB#6:                                 # %_Z8mkmatrixii.exit35
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movl	$240, %edi
	callq	malloc
	movq	%rax, %r15
	movl	$1, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph28.split.us.i39
                                        # =>This Inner Loop Header: Depth=1
	movl	$120, %edi
	callq	malloc
	movq	%rax, (%r15,%r14,8)
	movl	%ebx, (%rax)
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_0(%rip), %xmm1
	movdqu	%xmm1, 4(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_1(%rip), %xmm1
	movdqu	%xmm1, 20(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_2(%rip), %xmm1
	leal	13(%rbx), %ecx
	movdqu	%xmm1, 36(%rax)
	movl	%ecx, 52(%rax)
	leal	14(%rbx), %ecx
	movl	%ecx, 56(%rax)
	leal	15(%rbx), %ecx
	movl	%ecx, 60(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_3(%rip), %xmm1
	movdqu	%xmm1, 64(%rax)
	movdqa	%xmm0, %xmm1
	paddd	.LCPI4_4(%rip), %xmm1
	movdqu	%xmm1, 80(%rax)
	paddd	.LCPI4_5(%rip), %xmm0
	movdqu	%xmm0, 96(%rax)
	leal	28(%rbx), %ecx
	movl	%ecx, 112(%rax)
	leal	29(%rbx), %ecx
	movl	%ecx, 116(%rax)
	incq	%r14
	addl	$30, %ebx
	cmpq	$30, %r14
	jne	.LBB4_7
# BB#8:                                 # %_Z8mkmatrixii.exit47.preheader
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB4_15
# BB#9:                                 # %.preheader.us.us.preheader.i.preheader.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rcx
	movq	%rcx, 456(%rsp)         # 8-byte Spill
	movq	8(%rax), %rdx
	movq	%rdx, 424(%rsp)         # 8-byte Spill
	movq	16(%rax), %rdx
	movq	%rdx, 416(%rsp)         # 8-byte Spill
	movq	24(%rax), %rdx
	movq	%rdx, 408(%rsp)         # 8-byte Spill
	movq	32(%rax), %rdx
	movq	%rdx, 400(%rsp)         # 8-byte Spill
	movq	40(%rax), %rdx
	movq	%rdx, 392(%rsp)         # 8-byte Spill
	movq	48(%rax), %rdx
	movq	%rdx, 384(%rsp)         # 8-byte Spill
	movq	56(%rax), %rdx
	movq	%rdx, 376(%rsp)         # 8-byte Spill
	movq	64(%rax), %rdx
	movq	%rdx, 368(%rsp)         # 8-byte Spill
	movq	72(%rax), %rdx
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	movq	80(%rax), %rdx
	movq	%rdx, 352(%rsp)         # 8-byte Spill
	movq	88(%rax), %rdx
	movq	%rdx, 344(%rsp)         # 8-byte Spill
	movq	96(%rax), %rdx
	movq	%rdx, 336(%rsp)         # 8-byte Spill
	movq	104(%rax), %rdx
	movq	%rdx, 328(%rsp)         # 8-byte Spill
	movq	112(%rax), %rdx
	movq	%rdx, 320(%rsp)         # 8-byte Spill
	movq	120(%rax), %rdx
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movq	128(%rax), %rdx
	movq	%rdx, 304(%rsp)         # 8-byte Spill
	movq	136(%rax), %rdx
	movq	%rdx, 296(%rsp)         # 8-byte Spill
	movq	144(%rax), %rdx
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	movq	152(%rax), %rdx
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	movq	160(%rax), %rdx
	movq	%rdx, 272(%rsp)         # 8-byte Spill
	movq	168(%rax), %rdx
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	movq	176(%rax), %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	movq	184(%rax), %rdx
	movq	%rdx, 248(%rsp)         # 8-byte Spill
	movq	192(%rax), %rdx
	movq	%rdx, 240(%rsp)         # 8-byte Spill
	movq	200(%rax), %rdx
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	movq	208(%rax), %rdx
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	movq	216(%rax), %rdx
	movq	%rdx, 216(%rsp)         # 8-byte Spill
	movq	224(%rax), %rdx
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	movq	232(%rax), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%r15, 440(%rsp)         # 8-byte Spill
	movq	%rbp, 432(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_10:                               # %.preheader.us.us.preheader.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_11 Depth 2
                                        #       Child Loop BB4_12 Depth 3
	movq	%rax, 192(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_11:                               # %.preheader.us.us.preheader.i
                                        #   Parent Loop BB4_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_12 Depth 3
	movq	(%rbp,%rax,8), %r13
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movq	(%r15,%rax,8), %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	408(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rbx         # 8-byte Reload
	movq	392(%rsp), %rax         # 8-byte Reload
	movq	384(%rsp), %r8          # 8-byte Reload
	movq	376(%rsp), %r11         # 8-byte Reload
	movq	368(%rsp), %r14         # 8-byte Reload
	movq	360(%rsp), %r9          # 8-byte Reload
	movq	352(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	344(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	336(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movq	328(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movq	320(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movq	312(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	304(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	280(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	248(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	240(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	224(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	216(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	208(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	200(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_12:                               # %.preheader.us.us.i
                                        #   Parent Loop BB4_10 Depth=1
                                        #     Parent Loop BB4_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rdx,4), %ecx
	imull	(%r13), %ecx
	movq	184(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %edx
	imull	4(%r13), %edx
	addl	%ecx, %edx
	movq	%rsi, 528(%rsp)         # 8-byte Spill
	movl	(%rsi), %ecx
	imull	8(%r13), %ecx
	addl	%edx, %ecx
	movq	%rdi, 520(%rsp)         # 8-byte Spill
	movl	(%rdi), %edx
	imull	12(%r13), %edx
	addl	%ecx, %edx
	movq	%rbx, 512(%rsp)         # 8-byte Spill
	movl	(%rbx), %ecx
	imull	16(%r13), %ecx
	addl	%edx, %ecx
	movq	%rax, 504(%rsp)         # 8-byte Spill
	movl	(%rax), %edx
	imull	20(%r13), %edx
	addl	%ecx, %edx
	movq	%r8, 496(%rsp)          # 8-byte Spill
	movl	(%r8), %ecx
	imull	24(%r13), %ecx
	addl	%edx, %ecx
	movq	%r11, 488(%rsp)         # 8-byte Spill
	movl	(%r11), %edx
	imull	28(%r13), %edx
	addl	%ecx, %edx
	movq	%r14, 480(%rsp)         # 8-byte Spill
	movl	(%r14), %ecx
	imull	32(%r13), %ecx
	addl	%edx, %ecx
	movq	%r9, 472(%rsp)          # 8-byte Spill
	movl	(%r9), %edx
	imull	36(%r13), %edx
	addl	%ecx, %edx
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	imull	40(%r13), %ecx
	addl	%edx, %ecx
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edx
	imull	44(%r13), %edx
	addl	%ecx, %edx
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	imull	48(%r13), %ecx
	addl	%edx, %ecx
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edx
	imull	52(%r13), %edx
	addl	%ecx, %edx
	movq	136(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	imull	56(%r13), %ecx
	addl	%edx, %ecx
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edx
	imull	60(%r13), %edx
	addl	%ecx, %edx
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	imull	64(%r13), %ecx
	addl	%edx, %ecx
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edx
	imull	68(%r13), %edx
	addl	%ecx, %edx
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	imull	72(%r13), %ecx
	addl	%edx, %ecx
	movq	96(%rsp), %r15          # 8-byte Reload
	movl	(%r15), %edx
	imull	76(%r13), %edx
	addl	%ecx, %edx
	movq	88(%rsp), %r14          # 8-byte Reload
	movl	(%r14), %ecx
	imull	80(%r13), %ecx
	addl	%edx, %ecx
	movq	80(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %edx
	imull	84(%r13), %edx
	addl	%ecx, %edx
	movq	72(%rsp), %r11          # 8-byte Reload
	movl	(%r11), %ecx
	imull	88(%r13), %ecx
	addl	%edx, %ecx
	movq	64(%rsp), %r9           # 8-byte Reload
	movl	(%r9), %edx
	imull	92(%r13), %edx
	addl	%ecx, %edx
	movq	56(%rsp), %r8           # 8-byte Reload
	movl	(%r8), %ecx
	imull	96(%r13), %ecx
	addl	%edx, %ecx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	(%rdi), %edx
	imull	100(%r13), %edx
	addl	%ecx, %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi), %ecx
	imull	104(%r13), %ecx
	addl	%edx, %ecx
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	imull	108(%r13), %edx
	addl	%ecx, %edx
	movq	24(%rsp), %r10          # 8-byte Reload
	movl	(%r10), %ecx
	imull	112(%r13), %ecx
	addl	%edx, %ecx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp), %edx
	imull	116(%r13), %edx
	addl	%ecx, %edx
	movq	464(%rsp), %rcx         # 8-byte Reload
	movq	176(%rsp), %r12         # 8-byte Reload
	movl	%edx, (%rcx,%r12,4)
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	184(%rsp), %rcx         # 8-byte Reload
	incq	%rdx
	addq	$4, %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	addq	$4, %r10
	movq	%r10, 24(%rsp)          # 8-byte Spill
	addq	$4, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	addq	$4, %rsi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	addq	$4, %rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	addq	$4, %r8
	movq	%r8, 56(%rsp)           # 8-byte Spill
	addq	$4, %r9
	movq	%r9, 64(%rsp)           # 8-byte Spill
	addq	$4, %r11
	movq	%r11, 72(%rsp)          # 8-byte Spill
	addq	$4, %rbx
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	addq	$4, %r14
	movq	%r14, 88(%rsp)          # 8-byte Spill
	addq	$4, %r15
	movq	%r15, 96(%rsp)          # 8-byte Spill
	addq	$4, 104(%rsp)           # 8-byte Folded Spill
	addq	$4, 112(%rsp)           # 8-byte Folded Spill
	addq	$4, 120(%rsp)           # 8-byte Folded Spill
	addq	$4, 128(%rsp)           # 8-byte Folded Spill
	addq	$4, 136(%rsp)           # 8-byte Folded Spill
	addq	$4, 144(%rsp)           # 8-byte Folded Spill
	addq	$4, 152(%rsp)           # 8-byte Folded Spill
	addq	$4, 160(%rsp)           # 8-byte Folded Spill
	addq	$4, 168(%rsp)           # 8-byte Folded Spill
	movq	472(%rsp), %r9          # 8-byte Reload
	addq	$4, %r9
	movq	480(%rsp), %r14         # 8-byte Reload
	addq	$4, %r14
	movq	488(%rsp), %r11         # 8-byte Reload
	addq	$4, %r11
	movq	496(%rsp), %rax         # 8-byte Reload
	addq	$4, %rax
	movq	%rax, %r8
	movq	504(%rsp), %rax         # 8-byte Reload
	addq	$4, %rax
	movq	512(%rsp), %rbx         # 8-byte Reload
	addq	$4, %rbx
	movq	520(%rsp), %rdi         # 8-byte Reload
	addq	$4, %rdi
	movq	528(%rsp), %rsi         # 8-byte Reload
	addq	$4, %rsi
	addq	$4, %rcx
	cmpq	$30, %rdx
	jne	.LBB4_12
# BB#13:                                # %._crit_edge37.us.i
                                        #   in Loop: Header=BB4_11 Depth=2
	movq	448(%rsp), %rax         # 8-byte Reload
	incq	%rax
	cmpq	$30, %rax
	movq	440(%rsp), %r15         # 8-byte Reload
	movq	432(%rsp), %rbp         # 8-byte Reload
	jne	.LBB4_11
# BB#14:                                # %_Z5mmultiiPPiS0_S0_.exit
                                        #   in Loop: Header=BB4_10 Depth=1
	movq	192(%rsp), %rax         # 8-byte Reload
	incl	%eax
	cmpl	(%rsp), %eax            # 4-byte Folded Reload
	jne	.LBB4_10
.LBB4_15:                               # %_Z8mkmatrixii.exit47._crit_edge
	movq	(%r15), %rax
	movl	(%rax), %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSolsEi
	movq	%rax, %rbx
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	16(%r15), %rax
	movl	12(%rax), %esi
	movq	%rbx, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rbx
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	24(%r15), %rax
	movl	8(%rax), %esi
	movq	%rbx, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rbx
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	32(%r15), %rax
	movl	16(%rax), %esi
	movq	%rbx, %rdi
	callq	_ZNSolsEi
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %r13
	testq	%r13, %r13
	je	.LBB4_20
# BB#16:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%r13)
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB4_18
# BB#17:
	movb	67(%r13), %al
	jmp	.LBB4_19
.LBB4_18:
	movq	%r13, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%r13), %rax
	movl	$10, %esi
	movq	%r13, %rdi
	callq	*48(%rax)
.LBB4_19:                               # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	232(%rbp), %rdi
	callq	free
	movq	224(%rbp), %rdi
	callq	free
	movq	216(%rbp), %rdi
	callq	free
	movq	208(%rbp), %rdi
	callq	free
	movq	200(%rbp), %rdi
	callq	free
	movq	192(%rbp), %rdi
	callq	free
	movq	184(%rbp), %rdi
	callq	free
	movq	176(%rbp), %rdi
	callq	free
	movq	168(%rbp), %rdi
	callq	free
	movq	160(%rbp), %rdi
	callq	free
	movq	152(%rbp), %rdi
	callq	free
	movq	144(%rbp), %rdi
	callq	free
	movq	136(%rbp), %rdi
	callq	free
	movq	128(%rbp), %rdi
	callq	free
	movq	120(%rbp), %rdi
	callq	free
	movq	112(%rbp), %rdi
	callq	free
	movq	104(%rbp), %rdi
	callq	free
	movq	96(%rbp), %rdi
	callq	free
	movq	88(%rbp), %rdi
	callq	free
	movq	80(%rbp), %rdi
	callq	free
	movq	72(%rbp), %rdi
	callq	free
	movq	64(%rbp), %rdi
	callq	free
	movq	56(%rbp), %rdi
	callq	free
	movq	48(%rbp), %rdi
	callq	free
	movq	40(%rbp), %rdi
	callq	free
	movq	32(%rbp), %rdi
	callq	free
	movq	24(%rbp), %rdi
	callq	free
	movq	16(%rbp), %rdi
	callq	free
	movq	8(%rbp), %rdi
	callq	free
	movq	(%rbp), %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	232(%rbx), %rdi
	callq	free
	movq	224(%rbx), %rdi
	callq	free
	movq	216(%rbx), %rdi
	callq	free
	movq	208(%rbx), %rdi
	callq	free
	movq	200(%rbx), %rdi
	callq	free
	movq	192(%rbx), %rdi
	callq	free
	movq	184(%rbx), %rdi
	callq	free
	movq	176(%rbx), %rdi
	callq	free
	movq	168(%rbx), %rdi
	callq	free
	movq	160(%rbx), %rdi
	callq	free
	movq	152(%rbx), %rdi
	callq	free
	movq	144(%rbx), %rdi
	callq	free
	movq	136(%rbx), %rdi
	callq	free
	movq	128(%rbx), %rdi
	callq	free
	movq	120(%rbx), %rdi
	callq	free
	movq	112(%rbx), %rdi
	callq	free
	movq	104(%rbx), %rdi
	callq	free
	movq	96(%rbx), %rdi
	callq	free
	movq	88(%rbx), %rdi
	callq	free
	movq	80(%rbx), %rdi
	callq	free
	movq	72(%rbx), %rdi
	callq	free
	movq	64(%rbx), %rdi
	callq	free
	movq	56(%rbx), %rdi
	callq	free
	movq	48(%rbx), %rdi
	callq	free
	movq	40(%rbx), %rdi
	callq	free
	movq	32(%rbx), %rdi
	callq	free
	movq	24(%rbx), %rdi
	callq	free
	movq	16(%rbx), %rdi
	callq	free
	movq	8(%rbx), %rdi
	callq	free
	movq	(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	232(%r15), %rdi
	callq	free
	movq	224(%r15), %rdi
	callq	free
	movq	216(%r15), %rdi
	callq	free
	movq	208(%r15), %rdi
	callq	free
	movq	200(%r15), %rdi
	callq	free
	movq	192(%r15), %rdi
	callq	free
	movq	184(%r15), %rdi
	callq	free
	movq	176(%r15), %rdi
	callq	free
	movq	168(%r15), %rdi
	callq	free
	movq	160(%r15), %rdi
	callq	free
	movq	152(%r15), %rdi
	callq	free
	movq	144(%r15), %rdi
	callq	free
	movq	136(%r15), %rdi
	callq	free
	movq	128(%r15), %rdi
	callq	free
	movq	120(%r15), %rdi
	callq	free
	movq	112(%r15), %rdi
	callq	free
	movq	104(%r15), %rdi
	callq	free
	movq	96(%r15), %rdi
	callq	free
	movq	88(%r15), %rdi
	callq	free
	movq	80(%r15), %rdi
	callq	free
	movq	72(%r15), %rdi
	callq	free
	movq	64(%r15), %rdi
	callq	free
	movq	56(%r15), %rdi
	callq	free
	movq	48(%r15), %rdi
	callq	free
	movq	40(%r15), %rdi
	callq	free
	movq	32(%r15), %rdi
	callq	free
	movq	24(%r15), %rdi
	callq	free
	movq	16(%r15), %rdi
	callq	free
	movq	8(%r15), %rdi
	callq	free
	movq	(%r15), %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	xorl	%eax, %eax
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_20:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_matrix.ii,@function
_GLOBAL__sub_I_matrix.ii:               # @_GLOBAL__sub_I_matrix.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end5:
	.size	_GLOBAL__sub_I_matrix.ii, .Lfunc_end5-_GLOBAL__sub_I_matrix.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" "
	.size	.L.str, 2

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_matrix.ii

	.ident	"clang version 5.0.0 (https://github.com/aqjune/clang-intptr.git 2b963f0b79b3094bfc850c67ca5db086f5651807) (https://github.com/aqjune/llvm-intptr.git 90a84f9e4908c61106c413cb18b15a9bb8978e0d)"
	.section	".note.GNU-stack","",@progbits
