	.text
	.file	"g723_24.bc"
	.globl	g723_24_encoder
	.p2align	4, 0x90
	.type	g723_24_encoder,@function
g723_24_encoder:                        # @g723_24_encoder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%edi, %ebp
	cmpl	$1, %esi
	je	.LBB0_4
# BB#1:
	cmpl	$3, %esi
	je	.LBB0_6
# BB#2:
	movl	$-1, %r12d
	cmpl	$2, %esi
	jne	.LBB0_7
# BB#3:
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	alaw2linear
	jmp	.LBB0_5
.LBB0_4:
	xorl	%eax, %eax
	movl	%ebp, %edi
	callq	ulaw2linear
.LBB0_5:
	movl	%eax, %ebp
.LBB0_6:
	shrl	$2, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	predictor_zero
	movl	%eax, %ebx
	movswl	%bx, %r15d
	shrl	%r15d
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	predictor_pole
	addl	%ebx, %eax
	movswl	%ax, %ebx
	sarl	%ebx
	subl	%ebx, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	step_size
	movswl	%bp, %edi
	movswl	%ax, %r13d
	movl	$qtab_723_24, %edx
	movl	$3, %ecx
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	quantize
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movswl	%ax, %r12d
	movl	%r12d, %edi
	andl	$4, %edi
	movswq	%ax, %rbp
	movswl	_dqlntab(%rbp,%rbp), %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	reconstruct
	movswl	%ax, %r8d
	shll	$16, %eax
	movl	%r8d, %ecx
	andl	$16383, %ecx            # imm = 0x3FFF
	negl	%ecx
	testl	%eax, %eax
	cmovnsl	%r8d, %ecx
	addl	%ebx, %ecx
	movswl	%cx, %r9d
	subl	%ebx, %r15d
	addl	%r9d, %r15d
	movswl	_witab(%rbp,%rbp), %edx
	movswl	_fitab(%rbp,%rbp), %ecx
	movswl	%r15w, %ebp
	movl	$3, %edi
	movl	$0, %eax
	movl	%r13d, %esi
	pushq	%r14
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	update
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
.LBB0_7:
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	g723_24_encoder, .Lfunc_end0-g723_24_encoder
	.cfi_endproc

	.globl	g723_24_decoder
	.p2align	4, 0x90
	.type	g723_24_decoder,@function
g723_24_decoder:                        # @g723_24_decoder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 80
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movl	%edi, %r14d
	movl	%r14d, %r15d
	andl	$7, %r15d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	predictor_zero
	movl	%eax, %ebx
	movswl	%bx, %r12d
	shrl	%r12d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	callq	predictor_pole
	addl	%ebx, %eax
	movswl	%ax, %r13d
	sarl	%r13d
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	step_size
	andl	$4, %r14d
	movswl	_dqlntab(%r15,%r15), %esi
	movswl	%ax, %ebx
	xorl	%eax, %eax
	movl	%r14d, %edi
	movl	%ebx, %edx
	callq	reconstruct
	movswl	%ax, %r8d
	shll	$16, %eax
	movl	%r8d, %r14d
	andl	$16383, %r14d           # imm = 0x3FFF
	negl	%r14d
	testl	%eax, %eax
	cmovnsl	%r8d, %r14d
	addl	%r13d, %r14d
	movswl	%r14w, %eax
	movl	%r13d, (%rsp)           # 4-byte Spill
	subl	%r13d, %r12d
	addl	%eax, %r12d
	movswl	_witab(%r15,%r15), %edx
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movswl	_fitab(%r15,%r15), %ecx
	movl	%ebx, %r15d
	movl	%eax, %ebx
	movswl	%r12w, %ebp
	movl	$3, %edi
	movl	$0, %eax
	movl	%r15d, %esi
	movl	%ebx, %r9d
	pushq	16(%rsp)                # 8-byte Folded Reload
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	callq	update
	movl	20(%rsp), %eax          # 4-byte Reload
	addq	$16, %rsp
.Lcfi31:
	.cfi_adjust_cfa_offset -16
	cmpl	$1, %eax
	je	.LBB1_4
# BB#1:
	cmpl	$3, %eax
	je	.LBB1_5
# BB#2:
	movl	$-1, %r14d
	cmpl	$2, %eax
	jne	.LBB1_6
# BB#3:
	movl	$4, %r8d
	movl	$qtab_723_24, %r9d
	xorl	%eax, %eax
	movl	%ebx, %edi
	movl	(%rsp), %esi            # 4-byte Reload
	movl	%r15d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	tandem_adjust_alaw      # TAILCALL
.LBB1_4:
	movl	$4, %r8d
	movl	$qtab_723_24, %r9d
	xorl	%eax, %eax
	movl	%ebx, %edi
	movl	(%rsp), %esi            # 4-byte Reload
	movl	%r15d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	tandem_adjust_ulaw      # TAILCALL
.LBB1_5:
	shll	$16, %r14d
	sarl	$14, %r14d
.LBB1_6:
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	g723_24_decoder, .Lfunc_end1-g723_24_decoder
	.cfi_endproc

	.type	qtab_723_24,@object     # @qtab_723_24
	.data
	.p2align	1
qtab_723_24:
	.short	8                       # 0x8
	.short	218                     # 0xda
	.short	331                     # 0x14b
	.size	qtab_723_24, 6

	.type	_dqlntab,@object        # @_dqlntab
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
_dqlntab:
	.short	63488                   # 0xf800
	.short	135                     # 0x87
	.short	273                     # 0x111
	.short	373                     # 0x175
	.short	373                     # 0x175
	.short	273                     # 0x111
	.short	135                     # 0x87
	.short	63488                   # 0xf800
	.size	_dqlntab, 16

	.type	_witab,@object          # @_witab
	.p2align	4
_witab:
	.short	65408                   # 0xff80
	.short	960                     # 0x3c0
	.short	4384                    # 0x1120
	.short	18624                   # 0x48c0
	.short	18624                   # 0x48c0
	.short	4384                    # 0x1120
	.short	960                     # 0x3c0
	.short	65408                   # 0xff80
	.size	_witab, 16

	.type	_fitab,@object          # @_fitab
	.p2align	4
_fitab:
	.short	0                       # 0x0
	.short	512                     # 0x200
	.short	1024                    # 0x400
	.short	3584                    # 0xe00
	.short	3584                    # 0xe00
	.short	1024                    # 0x400
	.short	512                     # 0x200
	.short	0                       # 0x0
	.size	_fitab, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
