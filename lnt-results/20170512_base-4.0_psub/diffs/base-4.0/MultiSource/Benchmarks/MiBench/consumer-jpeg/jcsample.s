	.text
	.file	"jcsample.bc"
	.globl	jinit_downsampler
	.p2align	4, 0x90
	.type	jinit_downsampler,@function
jinit_downsampler:                      # @jinit_downsampler
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	8(%r15), %rax
	movl	$1, %esi
	movl	$104, %edx
	callq	*(%rax)
	movq	%rax, %r14
	movq	%r14, 472(%r15)
	movq	$start_pass_downsample, (%r14)
	movq	$sep_downsample, 8(%r14)
	movl	$0, 16(%r14)
	cmpl	$0, 260(%r15)
	je	.LBB0_2
# BB#1:
	movq	(%r15), %rax
	movl	$23, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB0_2:
	movl	68(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB0_23
# BB#3:                                 # %.lr.ph
	movq	80(%r15), %rbx
	addq	$12, %rbx
	movl	$1, %ebp
	xorl	%r12d, %r12d
	jmp	.LBB0_4
.LBB0_8:                                #   in Loop: Header=BB0_4 Depth=1
	movq	$fullsize_downsample, 24(%r14,%r12,8)
	jmp	.LBB0_20
.LBB0_18:                               #   in Loop: Header=BB0_4 Depth=1
	movq	$int_downsample, 24(%r14,%r12,8)
	xorl	%ebp, %ebp
	jmp	.LBB0_20
.LBB0_15:                               #   in Loop: Header=BB0_4 Depth=1
	movq	$h2v2_downsample, 24(%r14,%r12,8)
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rbx), %esi
	movl	304(%r15), %eax
	cmpl	%eax, %esi
	jne	.LBB0_9
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	movl	(%rbx), %edx
	cmpl	308(%r15), %edx
	jne	.LBB0_9
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=1
	cmpl	$0, 264(%r15)
	je	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_4 Depth=1
	movq	$fullsize_smooth_downsample, 24(%r14,%r12,8)
	movl	$1, 16(%r14)
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_4 Depth=1
	leal	(%rsi,%rsi), %edx
	cmpl	%eax, %edx
	jne	.LBB0_16
# BB#10:                                #   in Loop: Header=BB0_4 Depth=1
	movl	(%rbx), %edx
	cmpl	308(%r15), %edx
	jne	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_4 Depth=1
	movq	$h2v1_downsample, 24(%r14,%r12,8)
	xorl	%ebp, %ebp
	jmp	.LBB0_20
.LBB0_12:                               #   in Loop: Header=BB0_4 Depth=1
	movl	(%rbx), %edx
	addl	%edx, %edx
	cmpl	308(%r15), %edx
	jne	.LBB0_16
# BB#13:                                #   in Loop: Header=BB0_4 Depth=1
	cmpl	$0, 264(%r15)
	je	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_4 Depth=1
	movq	$h2v2_smooth_downsample, 24(%r14,%r12,8)
	movl	$1, 16(%r14)
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_16:                               # %.thread67
                                        #   in Loop: Header=BB0_4 Depth=1
	cltd
	idivl	%esi
	testl	%edx, %edx
	jne	.LBB0_19
# BB#17:                                #   in Loop: Header=BB0_4 Depth=1
	movl	308(%r15), %eax
	cltd
	idivl	(%rbx)
	testl	%edx, %edx
	je	.LBB0_18
.LBB0_19:                               #   in Loop: Header=BB0_4 Depth=1
	movq	(%r15), %rax
	movl	$37, 40(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
	movl	68(%r15), %ecx
.LBB0_20:                               #   in Loop: Header=BB0_4 Depth=1
	incq	%r12
	movslq	%ecx, %rax
	addq	$96, %rbx
	cmpq	%rax, %r12
	jl	.LBB0_4
# BB#21:                                # %._crit_edge
	testl	%ebp, %ebp
	jne	.LBB0_23
# BB#22:                                # %._crit_edge
	movl	264(%r15), %eax
	testl	%eax, %eax
	je	.LBB0_23
# BB#24:
	movq	(%r15), %rax
	movl	$98, 40(%rax)
	movq	8(%rax), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB0_23:                               # %._crit_edge.thread
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_downsampler, .Lfunc_end0-jinit_downsampler
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_downsample,@function
start_pass_downsample:                  # @start_pass_downsample
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	start_pass_downsample, .Lfunc_end1-start_pass_downsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	sep_downsample,@function
sep_downsample:                         # @sep_downsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 80
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%r8d, %r12d
	movq	%rcx, %r13
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpl	$0, 68(%rbx)
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph
	movq	472(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	80(%rbx), %rbp
	movl	%edx, %eax
	xorl	%r15d, %r15d
	shlq	$3, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14,%r15,8), %rdx
	addq	8(%rsp), %rdx           # 8-byte Folded Reload
	movq	(%r13,%r15,8), %rax
	movl	12(%rbp), %ecx
	imull	%r12d, %ecx
	leaq	(%rax,%rcx,8), %rcx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	16(%rsp), %rax          # 8-byte Reload
	callq	*24(%rax,%r15,8)
	incq	%r15
	addq	$96, %rbp
	movslq	68(%rbx), %rax
	cmpq	%rax, %r15
	jl	.LBB2_2
.LBB2_3:                                # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	sep_downsample, .Lfunc_end2-sep_downsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	fullsize_smooth_downsample,@function
fullsize_smooth_downsample:             # @fullsize_smooth_downsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 112
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movl	28(%rsi), %r15d
	shll	$3, %r15d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	308(%rdi), %r12d
	cmpl	$-1, %r12d
	jl	.LBB3_9
# BB#1:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	40(%rax), %ebp
	movl	%r15d, %eax
	subl	%ebp, %eax
	testl	%eax, %eax
	jle	.LBB3_9
# BB#2:                                 # %.lr.ph26.split.us.preheader.i
	addl	$2, %r12d
	movl	%r15d, %eax
	notl	%eax
	addl	%ebp, %eax
	cmpl	$-3, %eax
	movl	$-2, %ecx
	cmovgl	%eax, %ecx
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movl	%r15d, %r14d
	orl	$1, %r14d
	subl	%ebp, %r14d
	addl	%ecx, %r14d
	incq	%r14
	leaq	-1(%r12), %rax
	movq	%r12, %r15
	andq	$3, %r15
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB3_3
# BB#4:                                 # %.lr.ph26.split.us.i.prol.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph26.split.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%r13,%rbx,8), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB3_5
	jmp	.LBB3_6
.LBB3_3:
	xorl	%ebx, %ebx
.LBB3_6:                                # %.lr.ph26.split.us.i.prol.loopexit
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jb	.LBB3_9
# BB#7:                                 # %.lr.ph26.split.us.preheader.i.new
	subq	%rbx, %r12
	leaq	(%r13,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB3_8:                                # %.lr.ph26.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbx), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memset
	movq	(%rbx), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memset
	movq	8(%rbx), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memset
	movq	16(%rbx), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memset
	addq	$32, %rbx
	addq	$-4, %r12
	jne	.LBB3_8
.LBB3_9:                                # %expand_right_edge.exit
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 12(%rax)
	jle	.LBB3_14
# BB#10:                                # %.lr.ph
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	264(%rax), %r13
	movq	%r13, %rdx
	shlq	$9, %rdx
	movl	$65536, %r10d           # imm = 0x10000
	subq	%rdx, %r10
	shlq	$6, %r13
	leal	-3(%r15), %eax
	orl	$2, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$2, %eax
	subl	%r15d, %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_12 Depth 2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r12,8), %r11
	movq	-8(%rax,%r12,8), %rsi
	movq	8(%rax,%r12,8), %rdx
	incq	%r12
	movzbl	(%rsi), %edi
	movzbl	(%rdx), %ebx
	addl	%edi, %ebx
	movzbl	(%r11), %r8d
	addl	%r8d, %ebx
	movzbl	1(%rsi), %ebp
	movzbl	1(%rdx), %edi
	addl	%ebp, %edi
	movzbl	1(%r11), %ebp
	addl	%edi, %ebp
	leal	(%rbx,%rbx), %edi
	subl	%r8d, %edi
	addl	%ebp, %edi
	movl	%r10d, %r14d
	imull	%r8d, %r14d
	movl	%r13d, %eax
	imull	%edi, %eax
	leal	32768(%r14,%rax), %eax
	shrl	$16, %eax
	movb	%al, (%rcx)
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	1(%rcx), %rdi
	addq	$2, %rdx
	addq	$2, %rsi
	leaq	2(%r11), %r14
	movl	36(%rsp), %r8d          # 4-byte Reload
	.p2align	4, 0x90
.LBB3_12:                               #   Parent Loop BB3_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %r15d
	movzbl	(%rsi), %eax
	movzbl	(%rdx), %r9d
	addl	%eax, %r9d
	movzbl	(%r14), %ebp
	addl	%r9d, %ebp
	addl	%r15d, %ebx
	movzbl	-1(%r14), %eax
	subl	%eax, %ebx
	addl	%ebp, %ebx
	movl	%r10d, %ecx
	imull	%eax, %ecx
	movl	%r13d, %eax
	imull	%ebx, %eax
	leal	32768(%rcx,%rax), %eax
	shrl	$16, %eax
	movb	%al, (%rdi)
	incq	%rdi
	incq	%rdx
	incq	%rsi
	incq	%r14
	incl	%r8d
	movl	%r15d, %ebx
	jne	.LBB3_12
# BB#13:                                #   in Loop: Header=BB3_11 Depth=1
	leal	(%r15,%rbp,2), %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movzbl	(%r11,%rsi), %ecx
	subl	%ecx, %eax
	movl	%r10d, %edx
	imull	%ecx, %edx
	movl	%r13d, %ecx
	imull	%eax, %ecx
	leal	32768(%rdx,%rcx), %eax
	shrl	$16, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movb	%al, (%rcx,%rsi)
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	12(%rax), %rax
	cmpq	%rax, %r12
	jl	.LBB3_11
.LBB3_14:                               # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	fullsize_smooth_downsample, .Lfunc_end3-fullsize_smooth_downsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	fullsize_downsample,@function
fullsize_downsample:                    # @fullsize_downsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	40(%rbx), %r9d
	movl	308(%rbx), %r8d
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%rdx, %rdi
	movq	%r14, %rdx
	callq	jcopy_sample_rows
	movl	308(%rbx), %r12d
	testl	%r12d, %r12d
	jle	.LBB4_9
# BB#1:
	movl	40(%rbx), %r13d
	movl	28(%r15), %eax
	shll	$3, %eax
	movl	%eax, %ecx
	subl	%r13d, %ecx
	testl	%ecx, %ecx
	jle	.LBB4_9
# BB#2:                                 # %.lr.ph26.split.us.preheader.i
	leal	-1(%r13), %ecx
	subl	%eax, %ecx
	cmpl	$-3, %ecx
	movl	$-2, %edx
	cmovgl	%ecx, %edx
	movl	$1, %r15d
	subl	%r13d, %r15d
	addl	%eax, %r15d
	addl	%edx, %r15d
	incq	%r15
	leaq	-1(%r12), %rax
	movq	%r12, %rbx
	andq	$3, %rbx
	movq	%rax, (%rsp)            # 8-byte Spill
	je	.LBB4_3
# BB#4:                                 # %.lr.ph26.split.us.i.prol.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph26.split.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%r15, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB4_5
	jmp	.LBB4_6
.LBB4_3:
	xorl	%ebp, %ebp
.LBB4_6:                                # %.lr.ph26.split.us.i.prol.loopexit
	cmpq	$3, (%rsp)              # 8-byte Folded Reload
	jb	.LBB4_9
# BB#7:                                 # %.lr.ph26.split.us.preheader.i.new
	subq	%rbp, %r12
	leaq	24(%r14,%rbp,8), %rbx
	.p2align	4, 0x90
.LBB4_8:                                # %.lr.ph26.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rbx), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	(%rbx), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%r15, %rdx
	callq	memset
	addq	$32, %rbx
	addq	$-4, %r12
	jne	.LBB4_8
.LBB4_9:                                # %expand_right_edge.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	fullsize_downsample, .Lfunc_end4-fullsize_downsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	h2v1_downsample,@function
h2v1_downsample:                        # @h2v1_downsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 96
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	28(%rsi), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	308(%rdi), %ebp
	testl	%ebp, %ebp
	jle	.LBB5_7
# BB#1:
	movl	40(%rdi), %r14d
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebx
	shll	$4, %ebx
	movl	%ebx, %eax
	subl	%r14d, %eax
	testl	%eax, %eax
	jle	.LBB5_7
# BB#2:                                 # %.lr.ph26.split.us.preheader.i
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movl	%ebx, %eax
	notl	%eax
	addl	%r14d, %eax
	cmpl	$-3, %eax
	movl	$-2, %ecx
	cmovgl	%eax, %ecx
	orl	$1, %ebx
	subl	%r14d, %ebx
	addl	%ecx, %ebx
	incq	%rbx
	leaq	-1(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %r13
	xorl	%r15d, %r15d
	andq	$3, %r13
	je	.LBB5_4
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph26.split.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%r15,8), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	incq	%r15
	cmpq	%r15, %r13
	jne	.LBB5_3
.LBB5_4:                                # %.lr.ph26.split.us.i.prol.loopexit
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	jb	.LBB5_7
# BB#5:                                 # %.lr.ph26.split.us.preheader.i.new
	subq	%r15, %rbp
	leaq	24(%r12,%r15,8), %r15
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph26.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%r15), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	-16(%r15), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	-8(%r15), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	(%r15), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	addq	$32, %r15
	addq	$-4, %rbp
	jne	.LBB5_6
.LBB5_7:                                # %expand_right_edge.exit.preheader
	movq	32(%rsp), %r8           # 8-byte Reload
	movl	12(%r8), %eax
	testl	%eax, %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
	jle	.LBB5_17
# BB#8:                                 # %.lr.ph41
	shll	$3, %ebx
	testl	%ebx, %ebx
	je	.LBB5_13
# BB#9:                                 # %.lr.ph41.split.us.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph41.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_11 Depth 2
	movq	(%r13,%rax,8), %rcx
	movq	(%r12,%rax,8), %rdx
	movl	%ebx, %esi
	.p2align	4, 0x90
.LBB5_11:                               #   Parent Loop BB5_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdx), %edi
	movzbl	1(%rdx), %ebp
	addl	%edi, %ebp
	shrl	%ebp
	movb	%bpl, (%rcx)
	movzbl	2(%rdx), %edi
	movzbl	3(%rdx), %ebp
	leal	1(%rdi,%rbp), %edi
	shrl	%edi
	movb	%dil, 1(%rcx)
	addq	$4, %rdx
	addq	$2, %rcx
	addl	$-2, %esi
	jne	.LBB5_11
# BB#12:                                # %._crit_edge.us
                                        #   in Loop: Header=BB5_10 Depth=1
	incq	%rax
	movslq	12(%r8), %rcx
	cmpq	%rcx, %rax
	jl	.LBB5_10
	jmp	.LBB5_17
.LBB5_13:                               # %.lr.ph41.split.preheader
	leal	-1(%rax), %edx
	movl	%eax, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB5_15
	.p2align	4, 0x90
.LBB5_14:                               # %.lr.ph41.split.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB5_14
.LBB5_15:                               # %.lr.ph41.split.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB5_17
	.p2align	4, 0x90
.LBB5_16:                               # %.lr.ph41.split
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %ecx
	cmpl	%eax, %ecx
	jl	.LBB5_16
.LBB5_17:                               # %expand_right_edge.exit._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	h2v1_downsample, .Lfunc_end5-h2v1_downsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	h2v2_smooth_downsample,@function
h2v2_smooth_downsample:                 # @h2v2_smooth_downsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 160
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movl	28(%rsi), %r14d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	308(%rdi), %r12d
	cmpl	$-1, %r12d
	jl	.LBB6_9
# BB#1:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	40(%rax), %ebp
	movl	%r14d, %r15d
	shll	$4, %r15d
	movl	%r15d, %eax
	subl	%ebp, %eax
	testl	%eax, %eax
	jle	.LBB6_9
# BB#2:                                 # %.lr.ph26.split.us.preheader.i
	movq	%r14, 24(%rsp)          # 8-byte Spill
	addl	$2, %r12d
	movl	%r15d, %eax
	notl	%eax
	addl	%ebp, %eax
	cmpl	$-3, %eax
	movl	$-2, %ecx
	cmovgl	%eax, %ecx
	orl	$1, %r15d
	subl	%ebp, %r15d
	addl	%ecx, %r15d
	incq	%r15
	leaq	-1(%r12), %rax
	movq	%r12, %r14
	andq	$3, %r14
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB6_3
# BB#4:                                 # %.lr.ph26.split.us.i.prol.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph26.split.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%r13,%rbx,8), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r15, %rdx
	callq	memset
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB6_5
	jmp	.LBB6_6
.LBB6_3:
	xorl	%ebx, %ebx
.LBB6_6:                                # %.lr.ph26.split.us.i.prol.loopexit
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	jb	.LBB6_9
# BB#7:                                 # %.lr.ph26.split.us.preheader.i.new
	subq	%rbx, %r12
	leaq	(%r13,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph26.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbx), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	(%rbx), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	8(%rbx), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	16(%rbx), %rdi
	movzbl	-1(%rdi,%rbp), %esi
	addq	%rbp, %rdi
	movq	%r15, %rdx
	callq	memset
	addq	$32, %rbx
	addq	$-4, %r12
	jne	.LBB6_8
.LBB6_9:                                # %expand_right_edge.exit
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 12(%rax)
	jle	.LBB6_15
# BB#10:                                # %.lr.ph
	shll	$3, %r14d
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	264(%rax), %rax
	imull	$-80, %eax, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shlq	$4, %rax
	leal	-2(%r14), %edx
	leaq	(%rdx,%rdx), %rsi
	orq	$2, %rsi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	orq	$1, %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	$2, %edx
	subl	%r14d, %edx
	movl	%edx, 36(%rsp)          # 4-byte Spill
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r13, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_12 Depth 2
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	(%rdx,%rsi,8), %r10
	movq	(%r13,%rdi,8), %rbp
	movq	%rdi, %rdx
	orq	$1, %rdx
	movq	(%r13,%rdx,8), %rsi
	movq	-8(%r13,%rdi,8), %r15
	movq	16(%r13,%rdi,8), %r11
	addq	$2, %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movzbl	(%rbp), %edx
	movzbl	1(%rbp), %ebx
	addl	%edx, %ebx
	movzbl	(%rsi), %edi
	addl	%edi, %ebx
	movzbl	1(%rsi), %r12d
	addl	%ebx, %r12d
	movzbl	(%r15), %r9d
	movzbl	1(%r15), %r8d
	addl	%edx, %edi
	movzbl	(%r11), %edx
	addl	%r9d, %edi
	addl	%r8d, %edi
	movzbl	1(%r11), %ebx
	addl	%edx, %edi
	addl	%ebx, %edi
	movzbl	2(%rbp), %ebx
	addq	$2, %rbp
	addl	%ebx, %edi
	movzbl	2(%rsi), %ebx
	addl	%ebx, %edi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	leaq	2(%rsi), %r14
	addl	%r9d, %edx
	movzbl	2(%r15), %ebx
	addl	%edx, %ebx
	movzbl	2(%r11), %edx
	addl	%ebx, %edx
	movq	%r15, 88(%rsp)          # 8-byte Spill
	leaq	2(%r15), %r15
	leal	(%rdx,%rdi,2), %edx
	movl	%eax, %edi
	imull	%edx, %edi
	movq	%r11, 96(%rsp)          # 8-byte Spill
	leaq	2(%r11), %r8
	imull	%ecx, %r12d
	leal	32768(%r12,%rdi), %edx
	shrl	$16, %edx
	movb	%dl, (%r10)
	movq	%r10, 16(%rsp)          # 8-byte Spill
	leaq	1(%r10), %r9
	movl	36(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB6_12
	.p2align	4, 0x90
.LBB6_13:                               #   in Loop: Header=BB6_12 Depth=2
	movzbl	2(%rbp), %esi
	addq	$2, %rbp
	addl	%edx, %esi
	movzbl	-1(%r14), %edx
	addl	%esi, %edx
	movzbl	2(%r14), %esi
	addq	$2, %r14
	addl	%edx, %esi
	movzbl	-1(%r15), %edx
	movzbl	2(%r15), %edi
	leaq	2(%r15), %r15
	addl	%edx, %edi
	movzbl	-1(%r8), %edx
	addl	%edi, %edx
	leal	(%rdx,%rsi,2), %edx
	movzbl	2(%r8), %esi
	addq	$2, %r8
	addl	%edx, %esi
	imull	%ecx, %r10d
	movl	%eax, %edx
	imull	%esi, %edx
	leal	32768(%r10,%rdx), %edx
	shrl	$16, %edx
	movb	%dl, (%r9)
	incl	%r13d
	incq	%r9
.LBB6_12:                               #   Parent Loop BB6_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbp), %edx
	movzbl	1(%rbp), %esi
	addq	%rsi, %rdx
	movzbl	(%r14), %ebx
	addq	%rdx, %rbx
	movzbl	1(%r14), %edi
	leaq	(%rdi,%rbx), %r10
	movzbl	(%r15), %edx
	movzbl	1(%r15), %r11d
	addq	%r11, %rdx
	movzbl	(%r8), %ebx
	addq	%rdx, %rbx
	movzbl	1(%r8), %r12d
	addq	%r12, %rbx
	movzbl	-1(%rbp), %edx
	addq	%rbx, %rdx
	testl	%r13d, %r13d
	jne	.LBB6_13
# BB#14:                                #   in Loop: Header=BB6_11 Depth=1
	addl	%esi, %edi
	addl	%edx, %edi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movzbl	-1(%rdx,%rbp), %edx
	addl	%edi, %edx
	addl	%r11d, %r12d
	movq	88(%rsp), %rsi          # 8-byte Reload
	movzbl	-1(%rsi,%rbp), %esi
	addl	%r12d, %esi
	movq	96(%rsp), %rdi          # 8-byte Reload
	movzbl	-1(%rdi,%rbp), %edi
	addl	%esi, %edi
	leal	(%rdi,%rdx,2), %edx
	movl	%eax, %esi
	imull	%edx, %esi
	imull	%ecx, %r10d
	leal	32768(%r10,%rsi), %edx
	shrl	$16, %edx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	%dl, (%rdi,%rsi)
	movq	8(%rsp), %rsi           # 8-byte Reload
	incq	%rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movslq	12(%rdx), %rdx
	cmpq	%rdx, %rsi
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	jl	.LBB6_11
.LBB6_15:                               # %._crit_edge
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	h2v2_smooth_downsample, .Lfunc_end6-h2v2_smooth_downsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	h2v2_downsample,@function
h2v2_downsample:                        # @h2v2_downsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi81:
	.cfi_def_cfa_offset 96
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	28(%rsi), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	308(%rdi), %ebp
	testl	%ebp, %ebp
	jle	.LBB7_7
# BB#1:
	movl	40(%rdi), %r14d
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebx
	shll	$4, %ebx
	movl	%ebx, %eax
	subl	%r14d, %eax
	testl	%eax, %eax
	jle	.LBB7_7
# BB#2:                                 # %.lr.ph26.split.us.preheader.i
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movl	%ebx, %eax
	notl	%eax
	addl	%r14d, %eax
	cmpl	$-3, %eax
	movl	$-2, %ecx
	cmovgl	%eax, %ecx
	orl	$1, %ebx
	subl	%r14d, %ebx
	addl	%ecx, %ebx
	incq	%rbx
	leaq	-1(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %r13
	xorl	%r15d, %r15d
	andq	$3, %r13
	je	.LBB7_4
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph26.split.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%r15,8), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	incq	%r15
	cmpq	%r15, %r13
	jne	.LBB7_3
.LBB7_4:                                # %.lr.ph26.split.us.i.prol.loopexit
	cmpq	$3, 16(%rsp)            # 8-byte Folded Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	jb	.LBB7_7
# BB#5:                                 # %.lr.ph26.split.us.preheader.i.new
	subq	%r15, %rbp
	leaq	24(%r12,%r15,8), %r15
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph26.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%r15), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	-16(%r15), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	-8(%r15), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	(%r15), %rdi
	movzbl	-1(%rdi,%r14), %esi
	addq	%r14, %rdi
	movq	%rbx, %rdx
	callq	memset
	addq	$32, %r15
	addq	$-4, %rbp
	jne	.LBB7_6
.LBB7_7:                                # %expand_right_edge.exit.preheader
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	12(%r14), %eax
	testl	%eax, %eax
	movl	12(%rsp), %r15d         # 4-byte Reload
	jle	.LBB7_17
# BB#8:                                 # %.lr.ph55
	shll	$3, %r15d
	testl	%r15d, %r15d
	je	.LBB7_13
# BB#9:                                 # %.lr.ph55.split.us.preheader
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB7_10:                               # %.lr.ph55.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_11 Depth 2
	movq	(%r13,%r8,8), %rdx
	movq	(%r12,%r9,8), %rsi
	movq	%r9, %rdi
	orq	$1, %rdi
	movq	(%r12,%rdi,8), %rdi
	movl	$1, %ebp
	movl	%r15d, %ebx
	.p2align	4, 0x90
.LBB7_11:                               #   Parent Loop BB7_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rsi), %eax
	movzbl	1(%rsi), %ecx
	movzbl	(%rdi), %r10d
	movzbl	1(%rdi), %r11d
	addl	%ebp, %eax
	addl	%ecx, %eax
	addl	%r10d, %eax
	addl	%r11d, %eax
	shrl	$2, %eax
	movb	%al, (%rdx)
	incq	%rdx
	xorl	$3, %ebp
	addq	$2, %rsi
	addq	$2, %rdi
	decl	%ebx
	jne	.LBB7_11
# BB#12:                                # %._crit_edge.us
                                        #   in Loop: Header=BB7_10 Depth=1
	addq	$2, %r9
	incq	%r8
	movslq	12(%r14), %rax
	cmpq	%rax, %r8
	jl	.LBB7_10
	jmp	.LBB7_17
.LBB7_13:                               # %.lr.ph55.split.preheader
	leal	-1(%rax), %edx
	movl	%eax, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB7_15
	.p2align	4, 0x90
.LBB7_14:                               # %.lr.ph55.split.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB7_14
.LBB7_15:                               # %.lr.ph55.split.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB7_17
	.p2align	4, 0x90
.LBB7_16:                               # %.lr.ph55.split
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %ecx
	cmpl	%eax, %ecx
	jl	.LBB7_16
.LBB7_17:                               # %expand_right_edge.exit._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	h2v2_downsample, .Lfunc_end7-h2v2_downsample
	.cfi_endproc

	.p2align	4, 0x90
	.type	int_downsample,@function
int_downsample:                         # @int_downsample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi94:
	.cfi_def_cfa_offset 176
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rdx, %r14
	movl	28(%rsi), %ebx
	shll	$3, %ebx
	movl	40(%rdi), %r13d
	movl	304(%rdi), %eax
	cltd
	idivl	8(%rsi)
	movl	%eax, %r12d
	movl	308(%rdi), %edi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movl	12(%rsi), %ecx
	movl	%edi, %eax
	cltd
	idivl	%ecx
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	%edi, %edi
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	jle	.LBB8_10
# BB#1:
	movl	%r12d, %edx
	imull	%ebx, %edx
	movl	%edx, %eax
	subl	%r13d, %eax
	testl	%eax, %eax
	jle	.LBB8_10
# BB#2:                                 # %.lr.ph26.split.us.preheader.i
	movl	%edx, %eax
	notl	%eax
	addl	%r13d, %eax
	cmpl	$-3, %eax
	movl	$-2, %ecx
	cmovgl	%eax, %ecx
	orl	$1, %edx
	subl	%r13d, %edx
	addl	%ecx, %edx
	incq	%rdx
	leaq	-1(%rdi), %r15
	movq	%rdi, %rax
	andq	$3, %rax
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	je	.LBB8_3
# BB#4:                                 # %.lr.ph26.split.us.i.prol.preheader
	xorl	%ebp, %ebp
	movq	%rdx, %rbx
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph26.split.us.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r13, %r15
	movq	%rbx, %r13
	movq	%rax, %rbx
	callq	memset
	movq	%rbx, %rax
	movq	%r13, %rbx
	movq	%r15, %r13
	incq	%rbp
	cmpq	%rbp, %rax
	jne	.LBB8_5
	jmp	.LBB8_6
.LBB8_3:
	xorl	%ebp, %ebp
	movq	%rdx, %rbx
.LBB8_6:                                # %.lr.ph26.split.us.i.prol.loopexit
	cmpq	$3, 24(%rsp)            # 8-byte Folded Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	jb	.LBB8_9
# BB#7:                                 # %.lr.ph26.split.us.preheader.i.new
	subq	%rbp, %r15
	leaq	24(%r14,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB8_8:                                # %.lr.ph26.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rbp), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	-16(%rbp), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	-8(%rbp), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	(%rbp), %rdi
	movzbl	-1(%rdi,%r13), %esi
	addq	%r13, %rdi
	movq	%rbx, %rdx
	callq	memset
	addq	$32, %rbp
	addq	$-4, %r15
	jne	.LBB8_8
.LBB8_9:                                # %expand_right_edge.exit.preheader.loopexit
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %ecx
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB8_10:                               # %expand_right_edge.exit.preheader
	testl	%ecx, %ecx
	jle	.LBB8_21
# BB#11:                                # %.lr.ph104
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%edx, %eax
	imull	%r12d, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	cltq
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movslq	%edx, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%eax, %ebp
	leal	-1(%r12), %edi
	incq	%rdi
	leal	-1(%rbx), %eax
	incq	%rax
	movabsq	$8589934560, %rsi       # imm = 0x1FFFFFFE0
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andq	%rsi, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-32(%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	$5, %eax
	incl	%eax
	addq	$28, %rsi
	andq	%rdi, %rsi
	leaq	-4(%rsi), %rcx
	shrq	$2, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	andl	$7, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	negq	%rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_26 Depth 2
                                        #     Child Loop BB8_29 Depth 2
                                        #     Child Loop BB8_19 Depth 2
                                        #     Child Loop BB8_15 Depth 2
                                        #       Child Loop BB8_33 Depth 3
                                        #         Child Loop BB8_44 Depth 4
                                        #         Child Loop BB8_35 Depth 4
	testl	%ebx, %ebx
	je	.LBB8_20
# BB#13:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB8_12 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r8
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB8_17
# BB#14:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB8_12 Depth=1
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB8_15:                               # %.preheader.us
                                        #   Parent Loop BB8_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_33 Depth 3
                                        #         Child Loop BB8_44 Depth 4
                                        #         Child Loop BB8_35 Depth 4
	movl	%eax, %r13d
	testl	%r12d, %r12d
	jle	.LBB8_16
# BB#32:                                # %.lr.ph.us.us.preheader
                                        #   in Loop: Header=BB8_15 Depth=2
	xorl	%edx, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_33:                               # %.lr.ph.us.us
                                        #   Parent Loop BB8_12 Depth=1
                                        #     Parent Loop BB8_15 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_44 Depth 4
                                        #         Child Loop BB8_35 Depth 4
	leaq	(%rdx,%r15), %rcx
	movq	(%r14,%rcx,8), %rcx
	leaq	(%rcx,%r13), %r10
	xorl	%ebx, %ebx
	cmpq	$3, %rdi
	jbe	.LBB8_34
# BB#38:                                # %min.iters.checked
                                        #   in Loop: Header=BB8_33 Depth=3
	testq	%rsi, %rsi
	je	.LBB8_34
# BB#39:                                # %vector.ph
                                        #   in Loop: Header=BB8_33 Depth=3
	movd	%rax, %xmm3
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	jne	.LBB8_40
# BB#41:                                # %vector.body.prol
                                        #   in Loop: Header=BB8_33 Depth=3
	movzwl	(%r10), %eax
	movd	%eax, %xmm2
	movzwl	2(%r10), %eax
	movd	%eax, %xmm1
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	paddq	%xmm3, %xmm2
	movl	$4, %r11d
	movdqa	%xmm2, %xmm3
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB8_43
	jmp	.LBB8_45
.LBB8_40:                               #   in Loop: Header=BB8_33 Depth=3
	pxor	%xmm1, %xmm1
                                        # implicit-def: %XMM2
	xorl	%r11d, %r11d
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB8_45
.LBB8_43:                               # %vector.ph.new
                                        #   in Loop: Header=BB8_33 Depth=3
	movq	%rsi, %rax
	subq	%r11, %rax
	addq	%r13, %rcx
	leaq	6(%r11,%rcx), %rcx
	movdqa	%xmm3, %xmm2
	.p2align	4, 0x90
.LBB8_44:                               # %vector.body
                                        #   Parent Loop BB8_12 Depth=1
                                        #     Parent Loop BB8_15 Depth=2
                                        #       Parent Loop BB8_33 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	-6(%rcx), %ebx
	movd	%ebx, %xmm3
	movzwl	-4(%rcx), %ebx
	movd	%ebx, %xmm4
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	paddq	%xmm2, %xmm3
	paddq	%xmm1, %xmm4
	movzwl	-2(%rcx), %ebx
	movd	%ebx, %xmm2
	movzwl	(%rcx), %ebx
	movd	%ebx, %xmm1
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	paddq	%xmm3, %xmm2
	paddq	%xmm4, %xmm1
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB8_44
.LBB8_45:                               # %middle.block
                                        #   in Loop: Header=BB8_33 Depth=3
	paddq	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm1       # xmm1 = xmm2[2,3,0,1]
	paddq	%xmm2, %xmm1
	movd	%xmm1, %rax
	cmpq	%rsi, %rdi
	je	.LBB8_36
# BB#46:                                #   in Loop: Header=BB8_33 Depth=3
	addq	%rsi, %r10
	movl	%esi, %ebx
	.p2align	4, 0x90
.LBB8_34:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_33 Depth=3
	movl	%r12d, %ecx
	subl	%ebx, %ecx
	.p2align	4, 0x90
.LBB8_35:                               # %scalar.ph
                                        #   Parent Loop BB8_12 Depth=1
                                        #     Parent Loop BB8_15 Depth=2
                                        #       Parent Loop BB8_33 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r10), %ebx
	incq	%r10
	addq	%rbx, %rax
	decl	%ecx
	jne	.LBB8_35
.LBB8_36:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB8_33 Depth=3
	incq	%rdx
	cmpq	%rbp, %rdx
	jne	.LBB8_33
	jmp	.LBB8_37
	.p2align	4, 0x90
.LBB8_16:                               #   in Loop: Header=BB8_15 Depth=2
	xorl	%eax, %eax
.LBB8_37:                               # %._crit_edge82.us
                                        #   in Loop: Header=BB8_15 Depth=2
	addq	72(%rsp), %rax          # 8-byte Folded Reload
	cqto
	idivq	64(%rsp)                # 8-byte Folded Reload
	movb	%al, (%r8)
	incq	%r8
	incl	%r9d
	leal	(%r13,%r12), %eax
	movq	56(%rsp), %rbx          # 8-byte Reload
	cmpl	%ebx, %r9d
	jne	.LBB8_15
	jmp	.LBB8_20
	.p2align	4, 0x90
.LBB8_17:                               # %.preheader.preheader
                                        #   in Loop: Header=BB8_12 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	cqto
	idivq	64(%rsp)                # 8-byte Folded Reload
	xorl	%ecx, %ecx
	cmpq	$31, 40(%rsp)           # 8-byte Folded Reload
	jbe	.LBB8_18
# BB#22:                                # %min.iters.checked128
                                        #   in Loop: Header=BB8_12 Depth=1
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB8_18
# BB#23:                                # %vector.ph132
                                        #   in Loop: Header=BB8_12 Depth=1
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	movzbl	%al, %ecx
	movd	%ecx, %xmm1
	punpcklbw	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm1, %xmm1       # xmm1 = xmm1[0,0,1,1]
	je	.LBB8_24
# BB#25:                                # %vector.body124.prol.preheader
                                        #   in Loop: Header=BB8_12 Depth=1
	movq	80(%rsp), %rcx          # 8-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_26:                               # %vector.body124.prol
                                        #   Parent Loop BB8_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm1, (%r8,%rdx)
	movdqu	%xmm1, 16(%r8,%rdx)
	addq	$32, %rdx
	incq	%rcx
	jne	.LBB8_26
	jmp	.LBB8_27
.LBB8_24:                               #   in Loop: Header=BB8_12 Depth=1
	xorl	%edx, %edx
.LBB8_27:                               # %vector.body124.prol.loopexit
                                        #   in Loop: Header=BB8_12 Depth=1
	cmpq	$224, 96(%rsp)          # 8-byte Folded Reload
	jb	.LBB8_30
# BB#28:                                # %vector.ph132.new
                                        #   in Loop: Header=BB8_12 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	subq	%rdx, %rcx
	leaq	240(%r8,%rdx), %rdx
	.p2align	4, 0x90
.LBB8_29:                               # %vector.body124
                                        #   Parent Loop BB8_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm1, -240(%rdx)
	movdqu	%xmm1, -224(%rdx)
	movdqu	%xmm1, -208(%rdx)
	movdqu	%xmm1, -192(%rdx)
	movdqu	%xmm1, -176(%rdx)
	movdqu	%xmm1, -160(%rdx)
	movdqu	%xmm1, -144(%rdx)
	movdqu	%xmm1, -128(%rdx)
	movdqu	%xmm1, -112(%rdx)
	movdqu	%xmm1, -96(%rdx)
	movdqu	%xmm1, -80(%rdx)
	movdqu	%xmm1, -64(%rdx)
	movdqu	%xmm1, -48(%rdx)
	movdqu	%xmm1, -32(%rdx)
	movdqu	%xmm1, -16(%rdx)
	movdqu	%xmm1, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-256, %rcx
	jne	.LBB8_29
.LBB8_30:                               # %middle.block125
                                        #   in Loop: Header=BB8_12 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpq	%rcx, 40(%rsp)          # 8-byte Folded Reload
	je	.LBB8_20
# BB#31:                                #   in Loop: Header=BB8_12 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	addq	%rcx, %r8
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
.LBB8_18:                               # %.preheader.preheader150
                                        #   in Loop: Header=BB8_12 Depth=1
	movl	%ebx, %edx
	subl	%ecx, %edx
	.p2align	4, 0x90
.LBB8_19:                               # %.preheader
                                        #   Parent Loop BB8_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	%al, (%r8)
	incq	%r8
	decl	%edx
	jne	.LBB8_19
	.p2align	4, 0x90
.LBB8_20:                               # %expand_right_edge.exit
                                        #   in Loop: Header=BB8_12 Depth=1
	addq	112(%rsp), %r15         # 8-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	movq	48(%rsp), %rax          # 8-byte Reload
	movslq	12(%rax), %rax
	movq	%rdx, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmpq	%rax, %rdx
	jl	.LBB8_12
.LBB8_21:                               # %expand_right_edge.exit._crit_edge
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	int_downsample, .Lfunc_end8-int_downsample
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
