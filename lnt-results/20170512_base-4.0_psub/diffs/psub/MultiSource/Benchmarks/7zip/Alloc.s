	.text
	.file	"Alloc.bc"
	.globl	MyAlloc
	.p2align	4, 0x90
	.type	MyAlloc,@function
MyAlloc:                                # @MyAlloc
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB0_1
# BB#2:
	jmp	malloc                  # TAILCALL
.LBB0_1:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	MyAlloc, .Lfunc_end0-MyAlloc
	.cfi_endproc

	.globl	MyFree
	.p2align	4, 0x90
	.type	MyFree,@function
MyFree:                                 # @MyFree
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	MyFree, .Lfunc_end1-MyFree
	.cfi_endproc

	.globl	MidAlloc
	.p2align	4, 0x90
	.type	MidAlloc,@function
MidAlloc:                               # @MidAlloc
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	jmp	malloc                  # TAILCALL
.LBB2_1:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	MidAlloc, .Lfunc_end2-MidAlloc
	.cfi_endproc

	.p2align	4, 0x90
	.type	VirtualAlloc,@function
VirtualAlloc:                           # @VirtualAlloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi2:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
.Lcfi3:
	.cfi_offset %rbx, -56
.Lcfi4:
	.cfi_offset %r12, -48
.Lcfi5:
	.cfi_offset %r13, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
	movq	%rdi, %rbx
	testl	%esi, %esi
	je	.LBB3_17
# BB#1:
	movl	$VirtualAlloc.mutex, %edi
	callq	pthread_mutex_lock
	movl	$g_HugePageAddr, %r14d
	xorl	%r12d, %r12d
	movl	$3, %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	$0, g_HugePageAddr-24(,%rax,8)
	je	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpq	$0, g_HugePageAddr-16(,%rax,8)
	je	.LBB3_4
# BB#13:                                #   in Loop: Header=BB3_2 Depth=1
	cmpq	$0, g_HugePageAddr-8(,%rax,8)
	je	.LBB3_14
# BB#15:                                #   in Loop: Header=BB3_2 Depth=1
	cmpq	$0, g_HugePageAddr(,%rax,8)
	je	.LBB3_6
# BB#16:                                #   in Loop: Header=BB3_2 Depth=1
	addq	$4, %r15
	addq	$32, %r14
	leaq	4(%rax), %rcx
	incq	%rax
	cmpq	$64, %rax
	movq	%rcx, %rax
	jl	.LBB3_2
	jmp	.LBB3_12
.LBB3_17:
	movq	%rbx, %rdi
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	malloc                  # TAILCALL
.LBB3_5:
	movq	%rbx, -48(%rbp)         # 8-byte Spill
	leaq	g_HugePageAddr(,%r15,8), %r14
	jmp	.LBB3_7
.LBB3_4:
	movq	%rbx, -48(%rbp)         # 8-byte Spill
	orq	$1, %r15
	leaq	g_HugePageAddr(,%r15,8), %r14
	jmp	.LBB3_7
.LBB3_14:
	movq	%rbx, -48(%rbp)         # 8-byte Spill
	orq	$2, %r15
	leaq	g_HugePageAddr(,%r15,8), %r14
	jmp	.LBB3_7
.LBB3_6:                                # %._crit_edge
	movq	%rbx, -48(%rbp)         # 8-byte Spill
	addq	$24, %r14
	movq	%rax, %r15
.LBB3_7:
	movq	g_HugetlbPath(%rip), %r12
	movq	%r12, %rdi
	callq	strlen
	movq	%rsp, -56(%rbp)         # 8-byte Spill
	leal	12(%rax), %ecx
	addq	$15, %rcx
	movabsq	$8589934576, %rdx       # imm = 0x1FFFFFFF0
	andq	%rcx, %rdx
	movq	%rsp, %r13
	subq	%rdx, %r13
	movq	%r13, %rsp
	movslq	%eax, %rbx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movabsq	$6365935209031546671, %rax # imm = 0x585858582D7A372F
	movq	%rax, (%rbx,%r13)
	movb	$0, 10(%r13,%rbx)
	movw	$22616, 8(%r13,%rbx)    # imm = 0x5858
	movq	%r13, %rdi
	callq	mkstemp64
	movl	%eax, %r12d
	movq	%r13, %rdi
	callq	unlink
	testl	%r12d, %r12d
	js	.LBB3_8
# BB#9:
	xorl	%edi, %edi
	movl	$3, %edx
	movl	$1, %ecx
	xorl	%r9d, %r9d
	movq	-48(%rbp), %r13         # 8-byte Reload
	movq	%r13, %rsi
	movl	%r12d, %r8d
	callq	mmap64
	movq	%rax, %rbx
	movl	%r12d, %edi
	xorl	%r12d, %r12d
	callq	close
	cmpq	$-1, %rbx
	je	.LBB3_11
# BB#10:
	movq	%r13, g_HugePageLen(,%r15,8)
	movq	%rbx, (%r14)
	movq	%rbx, %r12
.LBB3_11:
	movq	-56(%rbp), %rsp         # 8-byte Reload
.LBB3_12:
	movl	$VirtualAlloc.mutex, %edi
	callq	pthread_mutex_unlock
	movq	%r12, %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_8:
	movq	stderr(%rip), %rbx
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	xorl	%r12d, %r12d
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	fprintf
	jmp	.LBB3_11
.Lfunc_end3:
	.size	VirtualAlloc, .Lfunc_end3-VirtualAlloc
	.cfi_endproc

	.globl	MidFree
	.p2align	4, 0x90
	.type	MidFree,@function
MidFree:                                # @MidFree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB4_8
# BB#1:                                 # %.preheader.preheader
	movl	$g_HugePageAddr, %ebx
	xorl	%eax, %eax
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB4_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdi, g_HugePageAddr-24(,%rcx,8)
	je	.LBB4_5
# BB#3:                                 # %.preheader.112
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpq	%rdi, g_HugePageAddr-16(,%rcx,8)
	je	.LBB4_4
# BB#9:                                 # %.preheader.213
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpq	%rdi, g_HugePageAddr-8(,%rcx,8)
	je	.LBB4_10
# BB#11:                                # %.preheader.314
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpq	%rdi, g_HugePageAddr(,%rcx,8)
	je	.LBB4_6
# BB#12:                                #   in Loop: Header=BB4_2 Depth=1
	addq	$4, %rax
	addq	$32, %rbx
	leaq	4(%rcx), %rdx
	incq	%rcx
	cmpq	$64, %rcx
	movq	%rdx, %rcx
	jl	.LBB4_2
# BB#13:
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB4_4:
	orq	$1, %rax
.LBB4_5:
	leaq	g_HugePageAddr(,%rax,8), %rbx
	jmp	.LBB4_7
.LBB4_10:
	orq	$2, %rax
	leaq	g_HugePageAddr(,%rax,8), %rbx
	jmp	.LBB4_7
.LBB4_6:                                # %.preheader.314._crit_edge
	addq	$24, %rbx
	movq	%rcx, %rax
.LBB4_7:
	movq	g_HugePageLen(,%rax,8), %rsi
	callq	munmap
	movq	$0, (%rbx)
.LBB4_8:                                # %VirtualFree.exit
	popq	%rbx
	retq
.Lfunc_end4:
	.size	MidFree, .Lfunc_end4-MidFree
	.cfi_endproc

	.globl	largePageMinimum
	.p2align	4, 0x90
	.type	largePageMinimum,@function
largePageMinimum:                       # @largePageMinimum
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movl	$.L.str, %edi
	callq	getenv
	movq	%rax, g_HugetlbPath(%rip)
	testq	%rax, %rax
	jne	.LBB5_9
# BB#1:
	movb	$0, largePageMinimum.dir_hugetlbfs(%rip)
	movl	$.L.str.1, %edi
	movl	$.L.str.2, %esi
	callq	setmntent
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_6
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	getmntent
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_5
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	16(%rbx), %rdi
	movl	$.L.str.3, %esi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB5_2
# BB#4:
	movq	8(%rbx), %rsi
	movl	$largePageMinimum.dir_hugetlbfs, %edi
	callq	strcpy
.LBB5_5:                                # %.loopexit
	movq	%r14, %rdi
	callq	endmntent
.LBB5_6:
	cmpb	$0, largePageMinimum.dir_hugetlbfs(%rip)
	je	.LBB5_8
# BB#7:
	movq	$largePageMinimum.dir_hugetlbfs, g_HugetlbPath(%rip)
	movl	$largePageMinimum.dir_hugetlbfs, %eax
	jmp	.LBB5_9
.LBB5_8:
	movq	g_HugetlbPath(%rip), %rax
	testq	%rax, %rax
	je	.LBB5_10
.LBB5_9:                                # %.thread
	movl	$16, %esi
	movq	%rax, %rdi
	callq	pathconf
	movq	%rax, %rbx
	callq	getpagesize
	cltq
	xorl	%ecx, %ecx
	cmpq	%rax, %rbx
	cmovbeq	%rcx, %rbx
	movq	%rbx, %rax
.LBB5_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_10:
	xorl	%eax, %eax
	jmp	.LBB5_11
.Lfunc_end5:
	.size	largePageMinimum, .Lfunc_end5-largePageMinimum
	.cfi_endproc

	.globl	SetLargePageSize
	.p2align	4, 0x90
	.type	SetLargePageSize,@function
SetLargePageSize:                       # @SetLargePageSize
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	callq	largePageMinimum
	testq	%rax, %rax
	je	.LBB6_3
# BB#1:
	leaq	-1(%rax), %rcx
	testq	%rax, %rcx
	jne	.LBB6_3
# BB#2:
	movq	%rax, g_LargePageSize(%rip)
.LBB6_3:
	popq	%rax
	retq
.Lfunc_end6:
	.size	SetLargePageSize, .Lfunc_end6-SetLargePageSize
	.cfi_endproc

	.globl	BigAlloc
	.p2align	4, 0x90
	.type	BigAlloc,@function
BigAlloc:                               # @BigAlloc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB7_1
# BB#2:
	cmpq	$262144, %rbx           # imm = 0x40000
	jb	.LBB7_6
# BB#3:
	movq	g_LargePageSize(%rip), %rdi
	leaq	-1(%rdi), %rax
	cmpq	$1073741823, %rax       # imm = 0x3FFFFFFF
	ja	.LBB7_6
# BB#4:
	leaq	-1(%rbx,%rdi), %rax
	negq	%rdi
	andq	%rax, %rdi
	movl	$1, %esi
	callq	VirtualAlloc
	testq	%rax, %rax
	je	.LBB7_6
# BB#5:
	popq	%rbx
	retq
.LBB7_6:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	malloc                  # TAILCALL
.LBB7_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	BigAlloc, .Lfunc_end7-BigAlloc
	.cfi_endproc

	.globl	BigFree
	.p2align	4, 0x90
	.type	BigFree,@function
BigFree:                                # @BigFree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB8_8
# BB#1:                                 # %.preheader.preheader
	movl	$g_HugePageAddr, %ebx
	xorl	%eax, %eax
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdi, g_HugePageAddr-24(,%rcx,8)
	je	.LBB8_5
# BB#3:                                 # %.preheader.112
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpq	%rdi, g_HugePageAddr-16(,%rcx,8)
	je	.LBB8_4
# BB#9:                                 # %.preheader.213
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpq	%rdi, g_HugePageAddr-8(,%rcx,8)
	je	.LBB8_10
# BB#11:                                # %.preheader.314
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpq	%rdi, g_HugePageAddr(,%rcx,8)
	je	.LBB8_6
# BB#12:                                #   in Loop: Header=BB8_2 Depth=1
	addq	$4, %rax
	addq	$32, %rbx
	leaq	4(%rcx), %rdx
	incq	%rcx
	cmpq	$64, %rcx
	movq	%rdx, %rcx
	jl	.LBB8_2
# BB#13:
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB8_4:
	orq	$1, %rax
.LBB8_5:
	leaq	g_HugePageAddr(,%rax,8), %rbx
	jmp	.LBB8_7
.LBB8_10:
	orq	$2, %rax
	leaq	g_HugePageAddr(,%rax,8), %rbx
	jmp	.LBB8_7
.LBB8_6:                                # %.preheader.314._crit_edge
	addq	$24, %rbx
	movq	%rcx, %rax
.LBB8_7:
	movq	g_HugePageLen(,%rax,8), %rsi
	callq	munmap
	movq	$0, (%rbx)
.LBB8_8:                                # %VirtualFree.exit
	popq	%rbx
	retq
.Lfunc_end8:
	.size	BigFree, .Lfunc_end8-BigFree
	.cfi_endproc

	.type	g_LargePageSize,@object # @g_LargePageSize
	.bss
	.globl	g_LargePageSize
	.p2align	3
g_LargePageSize:
	.quad	0                       # 0x0
	.size	g_LargePageSize, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"HUGETLB_PATH"
	.size	.L.str, 13

	.type	g_HugetlbPath,@object   # @g_HugetlbPath
	.local	g_HugetlbPath
	.comm	g_HugetlbPath,8,8
	.type	largePageMinimum.dir_hugetlbfs,@object # @largePageMinimum.dir_hugetlbfs
	.local	largePageMinimum.dir_hugetlbfs
	.comm	largePageMinimum.dir_hugetlbfs,1024,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/etc/mtab"
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"r"
	.size	.L.str.2, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"hugetlbfs"
	.size	.L.str.3, 10

	.type	VirtualAlloc.mutex,@object # @VirtualAlloc.mutex
	.local	VirtualAlloc.mutex
	.comm	VirtualAlloc.mutex,40,8
	.type	g_HugePageAddr,@object  # @g_HugePageAddr
	.local	g_HugePageAddr
	.comm	g_HugePageAddr,512,16
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"/7z-XXXXXX"
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"cant't open %s (%s)\n"
	.size	.L.str.5, 21

	.type	g_HugePageLen,@object   # @g_HugePageLen
	.local	g_HugePageLen
	.comm	g_HugePageLen,512,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
