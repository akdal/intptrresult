	.text
	.file	"z34.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI0_1:
	.quad	4676566000559194112     # double 46080
.LCPI0_3:
	.quad	-4512606827698978816    # double -8388607
.LCPI0_4:
	.quad	4710765209155796992     # double 8388607
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	RotateSize
	.p2align	4, 0x90
	.type	RotateSize,@function
RotateSize:                             # @RotateSize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	cvtsi2sdl	%r9d, %xmm0
	addsd	%xmm0, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	divsd	.LCPI0_1(%rip), %xmm0
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movl	56(%r8), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movl	48(%r8), %r14d
	movl	52(%r8), %r15d
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r15d, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	cvtsi2ssl	%r14d, %xmm2
	movaps	.LCPI0_2(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm3, %xmm2
	movl	60(%r8), %ebx
	cvtsi2ssl	%ebx, %xmm1
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movl	%ebp, %eax
	orl	%r15d, %eax
	xorpd	%xmm4, %xmm4
	xorpd	%xmm5, %xmm5
	je	.LBB0_4
# BB#1:
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	atan2
	movapd	%xmm0, %xmm4
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm5, %xmm5
	sqrtsd	%xmm0, %xmm5
	ucomisd	%xmm5, %xmm5
	jnp	.LBB0_3
# BB#2:                                 # %call.sqrt
	movsd	%xmm4, 24(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	24(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm5
.LBB0_3:
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	.LCPI0_2(%rip), %xmm3   # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
.LBB0_4:
	movsd	%xmm5, 64(%rsp)         # 8-byte Spill
	xorps	%xmm3, %xmm1
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	addsd	56(%rsp), %xmm4         # 8-byte Folded Reload
	movsd	%xmm4, 24(%rsp)         # 8-byte Spill
	movapd	%xmm4, %xmm0
	callq	cos
	mulsd	64(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	64(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	.LCPI0_3(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI0_4(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	movsd	8(%rsp), %xmm4          # 8-byte Reload
                                        # xmm4 = mem[0],zero
	minsd	%xmm4, %xmm2
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	maxsd	%xmm3, %xmm4
	movsd	%xmm4, 8(%rsp)          # 8-byte Spill
	minsd	%xmm0, %xmm1
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	maxsd	%xmm3, %xmm0
	movsd	%xmm0, 112(%rsp)        # 8-byte Spill
	orl	%r14d, %r15d
	xorpd	%xmm0, %xmm0
	je	.LBB0_7
# BB#5:
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	atan2
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_7
# BB#6:                                 # %call.sqrt76
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB0_7:
	movsd	%xmm0, 104(%rsp)        # 8-byte Spill
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	callq	cos
	mulsd	104(%rsp), %xmm0        # 8-byte Folded Reload
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movapd	%xmm0, %xmm1
	mulsd	104(%rsp), %xmm1        # 8-byte Folded Reload
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	minsd	%xmm2, %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	maxsd	8(%rsp), %xmm2          # 8-byte Folded Reload
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	minsd	%xmm1, %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	maxsd	112(%rsp), %xmm1        # 8-byte Folded Reload
	movsd	%xmm1, 104(%rsp)        # 8-byte Spill
	orl	%ebx, %r14d
	xorpd	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	je	.LBB0_10
# BB#8:
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	atan2
	movapd	%xmm0, %xmm1
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm2
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm2
	jnp	.LBB0_10
# BB#9:                                 # %call.sqrt78
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
.LBB0_10:
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	addsd	56(%rsp), %xmm1         # 8-byte Folded Reload
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movapd	%xmm1, %xmm0
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movapd	%xmm0, %xmm1
	mulsd	8(%rsp), %xmm1          # 8-byte Folded Reload
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	40(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	minsd	%xmm2, %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	maxsd	16(%rsp), %xmm2         # 8-byte Folded Reload
	movsd	%xmm2, 40(%rsp)         # 8-byte Spill
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	minsd	%xmm1, %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	maxsd	104(%rsp), %xmm1        # 8-byte Folded Reload
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	orl	%ebx, %ebp
	xorpd	%xmm2, %xmm2
	xorpd	%xmm1, %xmm1
	je	.LBB0_13
# BB#11:
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	atan2
	movapd	%xmm0, %xmm1
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	48(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm2
	jnp	.LBB0_13
# BB#12:                                # %call.sqrt80
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
.LBB0_13:
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	callq	cos
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	48(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	minsd	%xmm1, %xmm2
	maxsd	40(%rsp), %xmm1         # 8-byte Folded Reload
	movapd	%xmm1, %xmm3
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	minsd	%xmm0, %xmm1
	maxsd	16(%rsp), %xmm0         # 8-byte Folded Reload
	cvttsd2si	%xmm2, %eax
	negl	%eax
	movl	%eax, (%r13)
	cvttsd2si	%xmm3, %eax
	movl	%eax, (%r12)
	cvttsd2si	%xmm0, %eax
	movq	120(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	cvttsd2si	%xmm1, %eax
	negl	%eax
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	RotateSize, .Lfunc_end0-RotateSize
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
