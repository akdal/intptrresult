	.text
	.file	"fftFunctions.bc"
	.globl	keika
	.p2align	4, 0x90
	.type	keika,@function
keika:                                  # @keika
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %ebp
	movq	%rdi, %rcx
	testl	%ebp, %ebp
	jne	.LBB0_2
# BB#1:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
.LBB0_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$8, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	incl	%ebp
	leal	1(%rbx), %ecx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	cmpl	%ebx, %ebp
	jne	.LBB0_3
# BB#4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$19, %esi
	movl	$1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB0_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	keika, .Lfunc_end0-keika
	.cfi_endproc

	.globl	maxItch
	.p2align	4, 0x90
	.type	maxItch,@function
maxItch:                                # @maxItch
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB1_3
# BB#4:                                 # %.lr.ph.prol.preheader
	xorpd	%xmm1, %xmm1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	incq	%rdx
	cmpq	%rdx, %rsi
	movapd	%xmm0, %xmm1
	jne	.LBB1_5
	jmp	.LBB1_6
.LBB1_1:
	xorps	%xmm0, %xmm0
	retq
.LBB1_3:
	xorl	%edx, %edx
	xorpd	%xmm0, %xmm0
.LBB1_6:                                # %.lr.ph.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB1_9
# BB#7:                                 # %.lr.ph.preheader.new
	subq	%rdx, %rax
	leaq	24(%rdi,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-24(%rcx), %xmm1        # xmm1 = mem[0],zero
	movsd	-16(%rcx), %xmm2        # xmm2 = mem[0],zero
	maxsd	%xmm0, %xmm1
	maxsd	%xmm1, %xmm2
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	maxsd	%xmm2, %xmm1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB1_8
.LBB1_9:                                # %._crit_edge
	retq
.Lfunc_end1:
	.size	maxItch, .Lfunc_end1-maxItch
	.cfi_endproc

	.globl	calcNaiseki
	.p2align	4, 0x90
	.type	calcNaiseki,@function
calcNaiseki:                            # @calcNaiseki
	.cfi_startproc
# BB#0:
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rsi), %xmm1          # xmm1 = mem[0],zero
	mulsd	(%rdx), %xmm0
	movsd	8(%rdx), %xmm2          # xmm2 = mem[0],zero
	movapd	%xmm1, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, (%rdi)
	mulsd	(%rsi), %xmm2
	mulsd	(%rdx), %xmm1
	subsd	%xmm2, %xmm1
	movsd	%xmm1, 8(%rdi)
	retq
.Lfunc_end2:
	.size	calcNaiseki, .Lfunc_end2-calcNaiseki
	.cfi_endproc

	.globl	AllocateFukusosuuVec
	.p2align	4, 0x90
	.type	AllocateFukusosuuVec,@function
AllocateFukusosuuVec:                   # @AllocateFukusosuuVec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movslq	%ebp, %rdi
	movl	$16, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_1
.LBB3_2:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB3_1:
	movq	stderr(%rip), %rdi
	xorl	%ebx, %ebx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	jmp	.LBB3_2
.Lfunc_end3:
	.size	AllocateFukusosuuVec, .Lfunc_end3-AllocateFukusosuuVec
	.cfi_endproc

	.globl	AllocateFukusosuuMtx
	.p2align	4, 0x90
	.type	AllocateFukusosuuMtx,@function
AllocateFukusosuuMtx:                   # @AllocateFukusosuuMtx
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	%edi, %r15d
	leal	1(%r15), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB4_6
# BB#1:                                 # %.preheader
	testl	%r15d, %r15d
	jle	.LBB4_2
# BB#3:                                 # %.lr.ph
	movslq	%r14d, %rbx
	movslq	%r15d, %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_4:                                # =>This Inner Loop Header: Depth=1
	movl	$16, %esi
	movq	%rbx, %rdi
	callq	calloc
	testq	%rax, %rax
	je	.LBB4_5
# BB#7:                                 # %AllocateFukusosuuVec.exit
                                        #   in Loop: Header=BB4_4 Depth=1
	movq	%rax, (%r12,%rbp,8)
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB4_4
	jmp	.LBB4_8
.LBB4_2:                                # %.preheader.._crit_edge_crit_edge
	movslq	%r15d, %r13
.LBB4_8:                                # %._crit_edge
	movq	$0, (%r12,%r13,8)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_5:                                # %AllocateFukusosuuVec.exit.thread
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	fprintf
	movq	$0, (%r12,%rbp,8)
.LBB4_6:
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	AllocateFukusosuuMtx, .Lfunc_end4-AllocateFukusosuuMtx
	.cfi_endproc

	.globl	AllocateFukusosuuCub
	.p2align	4, 0x90
	.type	AllocateFukusosuuCub,@function
AllocateFukusosuuCub:                   # @AllocateFukusosuuCub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movl	%esi, %r13d
	movl	%edi, %r14d
	leal	1(%r14), %eax
	movslq	%eax, %rdi
	movl	$8, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB5_2
# BB#1:
	movl	$.L.str.6, %edi
	callq	ErrorExit
.LBB5_2:                                # %.preheader
	testl	%r14d, %r14d
	jle	.LBB5_5
# BB#3:                                 # %.lr.ph.preheader
	movl	%r14d, %ebx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edi
	movl	%r12d, %esi
	callq	AllocateFukusosuuMtx
	movq	%rax, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB5_4
.LBB5_5:                                # %._crit_edge
	movslq	%r14d, %rax
	movq	$0, (%r15,%rax,8)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	AllocateFukusosuuCub, .Lfunc_end5-AllocateFukusosuuCub
	.cfi_endproc

	.globl	FreeFukusosuuVec
	.p2align	4, 0x90
	.type	FreeFukusosuuVec,@function
FreeFukusosuuVec:                       # @FreeFukusosuuVec
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end6:
	.size	FreeFukusosuuVec, .Lfunc_end6-FreeFukusosuuVec
	.cfi_endproc

	.globl	FreeFukusosuuMtx
	.p2align	4, 0x90
	.type	FreeFukusosuuMtx,@function
FreeFukusosuuMtx:                       # @FreeFukusosuuMtx
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -24
.Lcfi40:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB7_3
# BB#1:                                 # %.lr.ph.preheader
	leaq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	free
	movq	(%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.LBB7_2
.LBB7_3:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end7:
	.size	FreeFukusosuuMtx, .Lfunc_end7-FreeFukusosuuMtx
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	-4556648919363013837    # double -9999.8999999999996
	.text
	.globl	getKouho
	.p2align	4, 0x90
	.type	getKouho,@function
getKouho:                               # @getKouho
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -48
.Lcfi47:
	.cfi_offset %r12, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	testl	%esi, %esi
	jle	.LBB8_1
# BB#2:                                 # %.preheader.lr.ph
	movl	%ecx, %r10d
	shrl	$31, %r10d
	addl	%ecx, %r10d
	sarl	%r10d
	testl	%ecx, %ecx
	jle	.LBB8_3
# BB#12:                                # %.preheader.us.preheader
	movl	%ecx, %r15d
	movl	%esi, %r8d
	leaq	-1(%r15), %r9
	movl	%r15d, %r12d
	andl	$3, %r12d
	xorl	%r11d, %r11d
	movsd	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movabsq	$-4556648919363013837, %r14 # imm = 0xC0C387F333333333
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_13:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_16 Depth 2
                                        #     Child Loop BB8_18 Depth 2
	xorl	%ebx, %ebx
	testq	%r12, %r12
	je	.LBB8_14
# BB#15:                                # %.prol.preheader
                                        #   in Loop: Header=BB8_13 Depth=1
	movapd	%xmm0, %xmm2
	.p2align	4, 0x90
.LBB8_16:                               #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm2, %xmm1
	maxsd	%xmm2, %xmm1
	cmoval	%ebx, %ebp
	incq	%rbx
	cmpq	%rbx, %r12
	movapd	%xmm1, %xmm2
	jne	.LBB8_16
	jmp	.LBB8_17
	.p2align	4, 0x90
.LBB8_14:                               #   in Loop: Header=BB8_13 Depth=1
	movapd	%xmm0, %xmm1
.LBB8_17:                               # %.prol.loopexit
                                        #   in Loop: Header=BB8_13 Depth=1
	cmpq	$3, %r9
	jb	.LBB8_19
	.p2align	4, 0x90
.LBB8_18:                               #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	maxsd	%xmm1, %xmm2
	cmoval	%ebx, %ebp
	movsd	8(%rdx,%rbx,8), %xmm1   # xmm1 = mem[0],zero
	leal	1(%rbx), %eax
	ucomisd	%xmm2, %xmm1
	maxsd	%xmm2, %xmm1
	cmovbel	%ebp, %eax
	movsd	16(%rdx,%rbx,8), %xmm2  # xmm2 = mem[0],zero
	leal	2(%rbx), %ecx
	ucomisd	%xmm1, %xmm2
	maxsd	%xmm1, %xmm2
	cmovbel	%eax, %ecx
	movsd	24(%rdx,%rbx,8), %xmm1  # xmm1 = mem[0],zero
	leal	3(%rbx), %ebp
	ucomisd	%xmm2, %xmm1
	maxsd	%xmm2, %xmm1
	cmovbel	%ecx, %ebp
	addq	$4, %rbx
	cmpq	%r15, %rbx
	jne	.LBB8_18
.LBB8_19:                               # %._crit_edge.us
                                        #   in Loop: Header=BB8_13 Depth=1
	movslq	%ebp, %rax
	movq	%r14, (%rdx,%rax,8)
	subl	%r10d, %eax
	movl	%eax, (%rdi,%r11,4)
	incq	%r11
	cmpq	%r8, %r11
	jne	.LBB8_13
	jmp	.LBB8_20
.LBB8_1:
	xorl	%esi, %esi
	jmp	.LBB8_20
.LBB8_3:                                # %.preheader.preheader
	negl	%r10d
	movl	%esi, %eax
	cmpl	$7, %esi
	jbe	.LBB8_4
# BB#7:                                 # %min.iters.checked
	movl	%esi, %r8d
	andl	$7, %r8d
	movq	%rax, %rbp
	subq	%r8, %rbp
	je	.LBB8_4
# BB#8:                                 # %vector.ph
	movd	%r10d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%rdi), %rbx
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB8_9:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-8, %rcx
	jne	.LBB8_9
# BB#10:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB8_5
	jmp	.LBB8_11
.LBB8_4:
	xorl	%ebp, %ebp
.LBB8_5:                                # %.preheader.preheader53
	leaq	(%rdi,%rbp,4), %rdi
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB8_6:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%r10d, (%rdi)
	addq	$4, %rdi
	decq	%rax
	jne	.LBB8_6
.LBB8_11:                               # %._crit_edge37.loopexit49
	movabsq	$-4556648919363013837, %rax # imm = 0xC0C387F333333333
	movq	%rax, (%rdx)
.LBB8_20:                               # %._crit_edge37
	movl	%esi, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	getKouho, .Lfunc_end8-getKouho
	.cfi_endproc

	.globl	zurasu2
	.p2align	4, 0x90
	.type	zurasu2,@function
zurasu2:                                # @zurasu2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	56(%rsp), %r12
	testl	%edi, %edi
	jle	.LBB9_1
# BB#25:                                # %.preheader31
	testl	%esi, %esi
	jle	.LBB9_40
# BB#26:                                # %.lr.ph37.preheader
	movl	%esi, %r11d
	cmpl	$3, %esi
	jbe	.LBB9_27
# BB#34:                                # %min.iters.checked95
	andl	$3, %esi
	movq	%r11, %rax
	subq	%rsi, %rax
	je	.LBB9_27
# BB#35:                                # %vector.memcheck108
	leaq	(%rcx,%r11,8), %rbp
	cmpq	%r9, %rbp
	jbe	.LBB9_37
# BB#36:                                # %vector.memcheck108
	leaq	(%r9,%r11,8), %rbp
	cmpq	%rcx, %rbp
	jbe	.LBB9_37
.LBB9_27:
	xorl	%eax, %eax
.LBB9_28:                               # %.lr.ph37.preheader149
	movl	%r11d, %ebx
	subl	%eax, %ebx
	leaq	-1(%r11), %rsi
	subq	%rax, %rsi
	andq	$7, %rbx
	je	.LBB9_31
# BB#29:                                # %.lr.ph37.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB9_30:                               # %.lr.ph37.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rax,8), %rbp
	movq	%rbp, (%r9,%rax,8)
	incq	%rax
	incq	%rbx
	jne	.LBB9_30
.LBB9_31:                               # %.lr.ph37.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB9_40
# BB#32:                                # %.lr.ph37.preheader149.new
	subq	%rax, %r11
	leaq	56(%r9,%rax,8), %rsi
	leaq	56(%rcx,%rax,8), %rax
	.p2align	4, 0x90
.LBB9_33:                               # %.lr.ph37
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rax), %rcx
	movq	%rcx, -56(%rsi)
	movq	-48(%rax), %rcx
	movq	%rcx, -48(%rsi)
	movq	-40(%rax), %rcx
	movq	%rcx, -40(%rsi)
	movq	-32(%rax), %rcx
	movq	%rcx, -32(%rsi)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rsi)
	movq	-16(%rax), %rcx
	movq	%rcx, -16(%rsi)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rsi)
	movq	(%rax), %rcx
	movq	%rcx, (%rsi)
	addq	$64, %rsi
	addq	$64, %rax
	addq	$-8, %r11
	jne	.LBB9_33
.LBB9_40:                               # %.preheader
	testl	%edx, %edx
	jle	.LBB9_61
# BB#41:                                # %.lr.ph
	movslq	%edi, %rcx
	movl	%edx, %r10d
	cmpl	$3, %edx
	jbe	.LBB9_42
# BB#49:                                # %min.iters.checked124
	andl	$3, %edx
	movq	%r10, %rax
	subq	%rdx, %rax
	je	.LBB9_42
# BB#50:                                # %vector.memcheck137
	leaq	-8(%r8,%r10,8), %rdi
	cmpq	%r12, %rdi
	jbe	.LBB9_52
# BB#51:                                # %vector.memcheck137
	leaq	-8(%r12,%r10,8), %rdi
	cmpq	%r8, %rdi
	jbe	.LBB9_52
.LBB9_42:
	xorl	%eax, %eax
.LBB9_43:                               # %scalar.ph122.preheader
	movl	%r10d, %edi
	subl	%eax, %edi
	leaq	-1(%r10), %rdx
	subq	%rax, %rdx
	andq	$7, %rdi
	je	.LBB9_46
# BB#44:                                # %scalar.ph122.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB9_45:                               # %scalar.ph122.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rax,8), %rbp
	addq	%rcx, %rbp
	movq	%rbp, (%r12,%rax,8)
	incq	%rax
	incq	%rdi
	jne	.LBB9_45
.LBB9_46:                               # %scalar.ph122.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB9_61
# BB#47:                                # %scalar.ph122.preheader.new
	subq	%rax, %r10
	leaq	56(%r12,%rax,8), %rdx
	leaq	56(%r8,%rax,8), %rax
	.p2align	4, 0x90
.LBB9_48:                               # %scalar.ph122
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rax), %rsi
	addq	%rcx, %rsi
	movq	%rsi, -56(%rdx)
	movq	-48(%rax), %rsi
	addq	%rcx, %rsi
	movq	%rsi, -48(%rdx)
	movq	-40(%rax), %rsi
	addq	%rcx, %rsi
	movq	%rsi, -40(%rdx)
	movq	-32(%rax), %rsi
	addq	%rcx, %rsi
	movq	%rsi, -32(%rdx)
	movq	-24(%rax), %rsi
	addq	%rcx, %rsi
	movq	%rsi, -24(%rdx)
	movq	-16(%rax), %rsi
	addq	%rcx, %rsi
	movq	%rsi, -16(%rdx)
	movq	-8(%rax), %rsi
	addq	%rcx, %rsi
	movq	%rsi, -8(%rdx)
	movq	(%rax), %rsi
	addq	%rcx, %rsi
	movq	%rsi, (%rdx)
	addq	$64, %rdx
	addq	$64, %rax
	addq	$-8, %r10
	jne	.LBB9_48
	jmp	.LBB9_61
.LBB9_1:                                # %.preheader34
	testl	%esi, %esi
	jle	.LBB9_16
# BB#2:                                 # %.lr.ph41
	movslq	%edi, %rdi
	negq	%rdi
	movl	%esi, %r11d
	cmpl	$3, %esi
	jbe	.LBB9_3
# BB#10:                                # %min.iters.checked
	andl	$3, %esi
	movq	%r11, %rax
	subq	%rsi, %rax
	je	.LBB9_3
# BB#11:                                # %vector.memcheck
	leaq	-8(%rcx,%r11,8), %rbx
	cmpq	%r9, %rbx
	jbe	.LBB9_13
# BB#12:                                # %vector.memcheck
	leaq	-8(%r9,%r11,8), %rbx
	cmpq	%rcx, %rbx
	jbe	.LBB9_13
.LBB9_3:
	xorl	%eax, %eax
.LBB9_4:                                # %scalar.ph.preheader
	movl	%r11d, %ebx
	subl	%eax, %ebx
	leaq	-1(%r11), %r14
	subq	%rax, %r14
	andq	$7, %rbx
	je	.LBB9_7
# BB#5:                                 # %scalar.ph.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB9_6:                                # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rax,8), %rsi
	addq	%rdi, %rsi
	movq	%rsi, (%r9,%rax,8)
	incq	%rax
	incq	%rbx
	jne	.LBB9_6
.LBB9_7:                                # %scalar.ph.prol.loopexit
	cmpq	$7, %r14
	jb	.LBB9_16
# BB#8:                                 # %scalar.ph.preheader.new
	subq	%rax, %r11
	leaq	56(%r9,%rax,8), %rsi
	leaq	56(%rcx,%rax,8), %rax
	.p2align	4, 0x90
.LBB9_9:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rax), %rcx
	addq	%rdi, %rcx
	movq	%rcx, -56(%rsi)
	movq	-48(%rax), %rcx
	addq	%rdi, %rcx
	movq	%rcx, -48(%rsi)
	movq	-40(%rax), %rcx
	addq	%rdi, %rcx
	movq	%rcx, -40(%rsi)
	movq	-32(%rax), %rcx
	addq	%rdi, %rcx
	movq	%rcx, -32(%rsi)
	movq	-24(%rax), %rcx
	addq	%rdi, %rcx
	movq	%rcx, -24(%rsi)
	movq	-16(%rax), %rcx
	addq	%rdi, %rcx
	movq	%rcx, -16(%rsi)
	movq	-8(%rax), %rcx
	addq	%rdi, %rcx
	movq	%rcx, -8(%rsi)
	movq	(%rax), %rcx
	addq	%rdi, %rcx
	movq	%rcx, (%rsi)
	addq	$64, %rsi
	addq	$64, %rax
	addq	$-8, %r11
	jne	.LBB9_9
.LBB9_16:                               # %.preheader32
	testl	%edx, %edx
	jle	.LBB9_61
# BB#17:                                # %.lr.ph39.preheader
	movl	%edx, %eax
	cmpl	$3, %edx
	jbe	.LBB9_18
# BB#55:                                # %min.iters.checked66
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB9_18
# BB#56:                                # %vector.memcheck79
	leaq	(%r8,%rax,8), %rsi
	cmpq	%r12, %rsi
	jbe	.LBB9_58
# BB#57:                                # %vector.memcheck79
	leaq	(%r12,%rax,8), %rsi
	cmpq	%r8, %rsi
	jbe	.LBB9_58
.LBB9_18:
	xorl	%ecx, %ecx
.LBB9_19:                               # %.lr.ph39.preheader150
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB9_22
# BB#20:                                # %.lr.ph39.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB9_21:                               # %.lr.ph39.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rcx,8), %rdi
	movq	%rdi, (%r12,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB9_21
.LBB9_22:                               # %.lr.ph39.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB9_61
# BB#23:                                # %.lr.ph39.preheader150.new
	subq	%rcx, %rax
	leaq	56(%r12,%rcx,8), %rdx
	leaq	56(%r8,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB9_24:                               # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx), %rsi
	movq	%rsi, -56(%rdx)
	movq	-48(%rcx), %rsi
	movq	%rsi, -48(%rdx)
	movq	-40(%rcx), %rsi
	movq	%rsi, -40(%rdx)
	movq	-32(%rcx), %rsi
	movq	%rsi, -32(%rdx)
	movq	-24(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movq	-16(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movq	-8(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movq	(%rcx), %rsi
	movq	%rsi, (%rdx)
	addq	$64, %rdx
	addq	$64, %rcx
	addq	$-8, %rax
	jne	.LBB9_24
.LBB9_61:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_37:                               # %vector.body91.preheader
	leaq	16(%rcx), %r14
	leaq	16(%r9), %rbx
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB9_38:                               # %vector.body91
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%r14), %xmm0
	movdqu	(%r14), %xmm1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm1, (%rbx)
	addq	$32, %r14
	addq	$32, %rbx
	addq	$-4, %r15
	jne	.LBB9_38
# BB#39:                                # %middle.block92
	testl	%esi, %esi
	jne	.LBB9_28
	jmp	.LBB9_40
.LBB9_52:                               # %vector.body120.preheader
	leaq	16(%r8), %rdi
	leaq	16(%r12), %rbx
	movq	%rax, %r9
	.p2align	4, 0x90
.LBB9_53:                               # %vector.body120
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movd	%xmm0, %r11
	addq	%rcx, %r11
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r14
	addq	%rcx, %r14
	movd	%xmm1, %rbp
	addq	%rcx, %rbp
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movd	%xmm0, %rsi
	addq	%rcx, %rsi
	movd	%r11, %xmm0
	movd	%r14, %xmm1
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movd	%rbp, %xmm1
	movd	%rsi, %xmm2
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm1, (%rbx)
	addq	$32, %rdi
	addq	$32, %rbx
	addq	$-4, %r9
	jne	.LBB9_53
# BB#54:                                # %middle.block121
	testl	%edx, %edx
	jne	.LBB9_43
	jmp	.LBB9_61
.LBB9_13:                               # %vector.body.preheader
	leaq	16(%rcx), %r14
	leaq	16(%r9), %rbx
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB9_14:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%r14), %xmm0
	movdqu	(%r14), %xmm1
	movd	%xmm0, %r12
	addq	%rdi, %r12
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r13
	addq	%rdi, %r13
	movd	%xmm1, %rbp
	addq	%rdi, %rbp
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movd	%xmm0, %r10
	addq	%rdi, %r10
	movd	%r12, %xmm0
	movd	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movd	%rbp, %xmm1
	movd	%r10, %xmm2
	punpcklqdq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm1, (%rbx)
	addq	$32, %r14
	addq	$32, %rbx
	addq	$-4, %r15
	jne	.LBB9_14
# BB#15:                                # %middle.block
	testl	%esi, %esi
	movq	56(%rsp), %r12
	jne	.LBB9_4
	jmp	.LBB9_16
.LBB9_58:                               # %vector.body62.preheader
	leaq	16(%r8), %rsi
	leaq	16(%r12), %rdi
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB9_59:                               # %vector.body62
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-4, %rbx
	jne	.LBB9_59
# BB#60:                                # %middle.block63
	testl	%edx, %edx
	jne	.LBB9_19
	jmp	.LBB9_61
.Lfunc_end9:
	.size	zurasu2, .Lfunc_end9-zurasu2
	.cfi_endproc

	.globl	zurasu
	.p2align	4, 0x90
	.type	zurasu,@function
zurasu:                                 # @zurasu
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 64
.Lcfi70:
	.cfi_offset %rbx, -56
.Lcfi71:
	.cfi_offset %r12, -48
.Lcfi72:
	.cfi_offset %r13, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, %r15
	movq	%rcx, %rbx
	movl	%edi, %r14d
	movq	64(%rsp), %r13
	testl	%r14d, %r14d
	movl	%edx, 4(%rsp)           # 4-byte Spill
	jle	.LBB10_1
# BB#7:                                 # %.preheader31
	testl	%esi, %esi
	jle	.LBB10_10
# BB#8:                                 # %.lr.ph37.preheader
	movl	%esi, %ebp
	.p2align	4, 0x90
.LBB10_9:                               # %.lr.ph37
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	movq	(%rbx), %rsi
	callq	strcpy
	addq	$8, %r12
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB10_9
.LBB10_10:                              # %.preheader
	movl	4(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB10_13
# BB#11:                                # %.lr.ph
	movslq	%r14d, %rbx
	movl	%eax, %ebp
	.p2align	4, 0x90
.LBB10_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	movq	(%r15), %rsi
	addq	%rbx, %rsi
	callq	strcpy
	addq	$8, %r13
	addq	$8, %r15
	decq	%rbp
	jne	.LBB10_12
	jmp	.LBB10_13
.LBB10_1:                               # %.preheader34
	testl	%esi, %esi
	jle	.LBB10_4
# BB#2:                                 # %.lr.ph41
	movslq	%r14d, %rbp
	negq	%rbp
	movl	%esi, %r14d
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	movq	(%rbx), %rsi
	addq	%rbp, %rsi
	callq	strcpy
	addq	$8, %r12
	addq	$8, %rbx
	decq	%r14
	jne	.LBB10_3
.LBB10_4:                               # %.preheader32
	movl	4(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB10_13
# BB#5:                                 # %.lr.ph39.preheader
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB10_6:                               # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	movq	(%r15), %rsi
	callq	strcpy
	addq	$8, %r13
	addq	$8, %r15
	decq	%rbx
	jne	.LBB10_6
.LBB10_13:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	zurasu, .Lfunc_end10-zurasu
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4636737291354636288     # double 100
.LCPI11_1:
	.quad	4648488871632306176     # double 600
	.text
	.globl	alignableReagion
	.p2align	4, 0x90
	.type	alignableReagion,@function
alignableReagion:                       # @alignableReagion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi82:
	.cfi_def_cfa_offset 144
.Lcfi83:
	.cfi_offset %rbx, -56
.Lcfi84:
	.cfi_offset %r12, -48
.Lcfi85:
	.cfi_offset %r13, -40
.Lcfi86:
	.cfi_offset %r14, -32
.Lcfi87:
	.cfi_offset %r15, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rcx, %r15
	movl	%esi, %r14d
	movl	%edi, %r12d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	(%rdx), %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%rax, %r13
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	(%r15), %r15
	movq	%r15, %rdi
	callq	strlen
	cmpq	%rax, %r13
	movq	%r15, %rdi
	cmovbq	%rbp, %rdi
	cmovaq	%rbp, %r15
	callq	strlen
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r13
	movl	fftWinSize(%rip), %eax
	addl	%eax, %r13d
	movl	alignableReagion.alloclen(%rip), %ecx
	cmpl	%r13d, %ecx
	jge	.LBB11_5
# BB#1:
	testl	%ecx, %ecx
	je	.LBB11_3
# BB#2:
	movq	alignableReagion.stra(%rip), %rdi
	callq	FreeDoubleVec
	jmp	.LBB11_4
.LBB11_3:
	cvtsi2sdl	fftThreshold(%rip), %xmm0
	divsd	.LCPI11_0(%rip), %xmm0
	mulsd	.LCPI11_1(%rip), %xmm0
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, alignableReagion.threshold(%rip)
.LBB11_4:
	movl	%r13d, %edi
	callq	AllocateDoubleVec
	movq	%rax, alignableReagion.stra(%rip)
	movl	%r13d, alignableReagion.alloclen(%rip)
.LBB11_5:                               # %.preheader202
	xorpd	%xmm0, %xmm0
	testl	%r12d, %r12d
	jle	.LBB11_16
# BB#6:                                 # %.preheader202
	testl	%r14d, %r14d
	movq	8(%rsp), %r10           # 8-byte Reload
	jle	.LBB11_16
# BB#7:                                 # %.preheader201.us.preheader
	movl	%r14d, %r9d
	movl	%r12d, %ecx
	leaq	-1(%r9), %rdx
	movl	%r9d, %esi
	andl	$3, %esi
	leaq	24(%rbx), %r8
	xorpd	%xmm0, %xmm0
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_8:                               # %.preheader201.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_11 Depth 2
                                        #     Child Loop BB11_14 Depth 2
	testq	%rsi, %rsi
	movsd	(%r10,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	je	.LBB11_9
# BB#10:                                # %.prol.preheader306
                                        #   in Loop: Header=BB11_8 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB11_11:                              #   Parent Loop BB11_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbx,%rdi,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	incq	%rdi
	cmpq	%rdi, %rsi
	jne	.LBB11_11
	jmp	.LBB11_12
	.p2align	4, 0x90
.LBB11_9:                               #   in Loop: Header=BB11_8 Depth=1
	xorl	%edi, %edi
.LBB11_12:                              # %.prol.loopexit307
                                        #   in Loop: Header=BB11_8 Depth=1
	cmpq	$3, %rdx
	jb	.LBB11_15
# BB#13:                                # %.preheader201.us.new
                                        #   in Loop: Header=BB11_8 Depth=1
	movq	%r9, %rax
	subq	%rdi, %rax
	leaq	(%r8,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB11_14:                              #   Parent Loop BB11_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-24(%rdi), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movsd	-16(%rdi), %xmm0        # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	movsd	-8(%rdi), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	addq	$32, %rdi
	addq	$-4, %rax
	jne	.LBB11_14
.LBB11_15:                              # %._crit_edge247.us
                                        #   in Loop: Header=BB11_8 Depth=1
	incq	%rbp
	cmpq	%rcx, %rbp
	jne	.LBB11_8
.LBB11_16:                              # %.preheader200
	movq	144(%rsp), %r15
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB11_51
# BB#17:                                # %.preheader199.lr.ph
	movq	alignableReagion.stra(%rip), %rax
	movl	(%rsp), %r13d           # 4-byte Reload
	movl	%r14d, %r8d
	movl	%r12d, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r8, 80(%rsp)           # 8-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<kill> %R8<def>
	andl	$1, %r8d
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	8(%rcx), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	8(%rbx), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	xorpd	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB11_18:                              # %.preheader199
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_24 Depth 2
                                        #     Child Loop BB11_31 Depth 2
                                        #     Child Loop BB11_33 Depth 2
                                        #     Child Loop BB11_45 Depth 2
                                        #       Child Loop BB11_46 Depth 3
                                        #     Child Loop BB11_49 Depth 2
	movapd	%xmm1, alignableReagion.prf1+192(%rip)
	movapd	%xmm1, alignableReagion.prf1+176(%rip)
	movapd	%xmm1, alignableReagion.prf1+160(%rip)
	movapd	%xmm1, alignableReagion.prf1+144(%rip)
	movapd	%xmm1, alignableReagion.prf1+128(%rip)
	movapd	%xmm1, alignableReagion.prf1+112(%rip)
	movapd	%xmm1, alignableReagion.prf1+96(%rip)
	movapd	%xmm1, alignableReagion.prf1+80(%rip)
	movapd	%xmm1, alignableReagion.prf1+64(%rip)
	movapd	%xmm1, alignableReagion.prf1+48(%rip)
	movapd	%xmm1, alignableReagion.prf1+32(%rip)
	movapd	%xmm1, alignableReagion.prf1+16(%rip)
	movapd	%xmm1, alignableReagion.prf1(%rip)
	movapd	%xmm1, alignableReagion.prf2+192(%rip)
	movapd	%xmm1, alignableReagion.prf2+176(%rip)
	movapd	%xmm1, alignableReagion.prf2+160(%rip)
	movapd	%xmm1, alignableReagion.prf2+144(%rip)
	movapd	%xmm1, alignableReagion.prf2+128(%rip)
	movapd	%xmm1, alignableReagion.prf2+112(%rip)
	movapd	%xmm1, alignableReagion.prf2+96(%rip)
	movapd	%xmm1, alignableReagion.prf2+80(%rip)
	movapd	%xmm1, alignableReagion.prf2+64(%rip)
	movapd	%xmm1, alignableReagion.prf2+48(%rip)
	movapd	%xmm1, alignableReagion.prf2+32(%rip)
	movapd	%xmm1, alignableReagion.prf2+16(%rip)
	movapd	%xmm1, alignableReagion.prf2(%rip)
	testl	%r12d, %r12d
	jle	.LBB11_25
# BB#19:                                # %.lr.ph227.preheader
                                        #   in Loop: Header=BB11_18 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB11_21
# BB#20:                                #   in Loop: Header=BB11_18 Depth=1
	xorl	%edx, %edx
	cmpl	$1, %r12d
	jne	.LBB11_23
	jmp	.LBB11_25
	.p2align	4, 0x90
.LBB11_21:                              # %.lr.ph227.prol
                                        #   in Loop: Header=BB11_18 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	(%rcx), %xmm3           # xmm3 = mem[0],zero
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rdx
	movsbq	(%rdx,%rsi), %rdx
	movslq	amino_n(,%rdx,4), %rdx
	addsd	alignableReagion.prf1(,%rdx,8), %xmm3
	movsd	%xmm3, alignableReagion.prf1(,%rdx,8)
	movl	$1, %edx
	cmpl	$1, %r12d
	je	.LBB11_25
.LBB11_23:                              # %.lr.ph227.preheader.new
                                        #   in Loop: Header=BB11_18 Depth=1
	movq	72(%rsp), %rbp          # 8-byte Reload
	subq	%rdx, %rbp
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdi
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB11_24:                              # %.lr.ph227
                                        #   Parent Loop BB11_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdi), %xmm3         # xmm3 = mem[0],zero
	movq	-8(%rdx), %rcx
	movsbq	(%rcx,%rsi), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	addsd	alignableReagion.prf1(,%rcx,8), %xmm3
	movsd	%xmm3, alignableReagion.prf1(,%rcx,8)
	movsd	(%rdi), %xmm3           # xmm3 = mem[0],zero
	movq	(%rdx), %rcx
	movsbq	(%rcx,%rsi), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	addsd	alignableReagion.prf1(,%rcx,8), %xmm3
	movsd	%xmm3, alignableReagion.prf1(,%rcx,8)
	addq	$16, %rdi
	addq	$16, %rdx
	addq	$-2, %rbp
	jne	.LBB11_24
.LBB11_25:                              # %.preheader197
                                        #   in Loop: Header=BB11_18 Depth=1
	testl	%r14d, %r14d
	jle	.LBB11_32
# BB#26:                                # %.lr.ph229.preheader
                                        #   in Loop: Header=BB11_18 Depth=1
	testq	%r8, %r8
	jne	.LBB11_28
# BB#27:                                #   in Loop: Header=BB11_18 Depth=1
	xorl	%edx, %edx
	cmpl	$1, %r14d
	jne	.LBB11_30
	jmp	.LBB11_32
	.p2align	4, 0x90
.LBB11_28:                              # %.lr.ph229.prol
                                        #   in Loop: Header=BB11_18 Depth=1
	movsd	(%rbx), %xmm3           # xmm3 = mem[0],zero
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rdx
	movsbq	(%rdx,%rsi), %rdx
	movslq	amino_n(,%rdx,4), %rdx
	addsd	alignableReagion.prf2(,%rdx,8), %xmm3
	movsd	%xmm3, alignableReagion.prf2(,%rdx,8)
	movl	$1, %edx
	cmpl	$1, %r14d
	je	.LBB11_32
.LBB11_30:                              # %.lr.ph229.preheader.new
                                        #   in Loop: Header=BB11_18 Depth=1
	movq	80(%rsp), %rbp          # 8-byte Reload
	subq	%rdx, %rbp
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdi
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB11_31:                              # %.lr.ph229
                                        #   Parent Loop BB11_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rdi), %xmm3         # xmm3 = mem[0],zero
	movq	-8(%rdx), %rcx
	movsbq	(%rcx,%rsi), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	addsd	alignableReagion.prf2(,%rcx,8), %xmm3
	movsd	%xmm3, alignableReagion.prf2(,%rcx,8)
	movsd	(%rdi), %xmm3           # xmm3 = mem[0],zero
	movq	(%rdx), %rcx
	movsbq	(%rcx,%rsi), %rcx
	movslq	amino_n(,%rcx,4), %rcx
	addsd	alignableReagion.prf2(,%rcx,8), %xmm3
	movsd	%xmm3, alignableReagion.prf2(,%rcx,8)
	addq	$16, %rdi
	addq	$16, %rdx
	addq	$-2, %rbp
	jne	.LBB11_31
.LBB11_32:                              # %.preheader196.preheader
                                        #   in Loop: Header=BB11_18 Depth=1
	movl	$26, %r10d
	movl	$27, %r9d
	movl	$26, %r11d
	.p2align	4, 0x90
.LBB11_33:                              # %.preheader196
                                        #   Parent Loop BB11_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	alignableReagion.prf1-16(,%r9,8), %xmm3 # xmm3 = mem[0],zero
	ucomisd	%xmm2, %xmm3
	jne	.LBB11_34
	jnp	.LBB11_35
.LBB11_34:                              #   in Loop: Header=BB11_33 Depth=2
	movslq	%r11d, %rcx
	leal	-2(%r9), %r11d
	movl	%r11d, alignableReagion.hat1(,%rcx,4)
.LBB11_35:                              #   in Loop: Header=BB11_33 Depth=2
	movsd	alignableReagion.prf2-16(,%r9,8), %xmm3 # xmm3 = mem[0],zero
	ucomisd	%xmm2, %xmm3
	jne	.LBB11_36
	jnp	.LBB11_37
.LBB11_36:                              #   in Loop: Header=BB11_33 Depth=2
	movslq	%r10d, %rcx
	leal	-2(%r9), %r10d
	movl	%r10d, alignableReagion.hat2(,%rcx,4)
.LBB11_37:                              # %.preheader196.1305
                                        #   in Loop: Header=BB11_33 Depth=2
	movsd	alignableReagion.prf1-24(,%r9,8), %xmm3 # xmm3 = mem[0],zero
	ucomisd	%xmm2, %xmm3
	jne	.LBB11_38
	jnp	.LBB11_39
.LBB11_38:                              #   in Loop: Header=BB11_33 Depth=2
	movslq	%r11d, %rcx
	leal	-3(%r9), %r11d
	movl	%r11d, alignableReagion.hat1(,%rcx,4)
.LBB11_39:                              #   in Loop: Header=BB11_33 Depth=2
	movsd	alignableReagion.prf2-24(,%r9,8), %xmm3 # xmm3 = mem[0],zero
	ucomisd	%xmm2, %xmm3
	jne	.LBB11_40
	jnp	.LBB11_41
.LBB11_40:                              #   in Loop: Header=BB11_33 Depth=2
	movslq	%r10d, %rcx
	leal	-3(%r9), %r10d
	movl	%r10d, alignableReagion.hat2(,%rcx,4)
.LBB11_41:                              #   in Loop: Header=BB11_33 Depth=2
	addq	$-2, %r9
	cmpq	$1, %r9
	jg	.LBB11_33
# BB#42:                                #   in Loop: Header=BB11_18 Depth=1
	movslq	%r11d, %rcx
	movl	$-1, alignableReagion.hat1(,%rcx,4)
	movslq	%r10d, %rcx
	movl	$-1, alignableReagion.hat2(,%rcx,4)
	movq	$0, (%rax,%rsi,8)
	movl	alignableReagion.hat1+104(%rip), %edx
	xorpd	%xmm3, %xmm3
	cmpl	$-1, %edx
	je	.LBB11_50
# BB#43:                                # %.preheader195.lr.ph
                                        #   in Loop: Header=BB11_18 Depth=1
	movl	alignableReagion.hat2+104(%rip), %ebp
	cmpl	$-1, %ebp
	je	.LBB11_49
# BB#44:                                # %.preheader195.preheader
                                        #   in Loop: Header=BB11_18 Depth=1
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB11_45:                              # %.preheader195
                                        #   Parent Loop BB11_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_46 Depth 3
	movslq	%edx, %rdx
	movsd	alignableReagion.prf1(,%rdx,8), %xmm4 # xmm4 = mem[0],zero
	movl	%ebp, %edi
	.p2align	4, 0x90
.LBB11_46:                              #   Parent Loop BB11_18 Depth=1
                                        #     Parent Loop BB11_45 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edi, %rcx
	imulq	$104, %rdx, %rdi
	xorps	%xmm5, %xmm5
	cvtsi2sdl	n_disFFT(%rdi,%rcx,4), %xmm5
	mulsd	%xmm4, %xmm5
	mulsd	alignableReagion.prf2(,%rcx,8), %xmm5
	addsd	%xmm5, %xmm3
	movl	alignableReagion.hat2(,%rcx,4), %edi
	cmpl	$-1, %edi
	jne	.LBB11_46
# BB#47:                                # %._crit_edge236
                                        #   in Loop: Header=BB11_45 Depth=2
	movl	alignableReagion.hat1(,%rdx,4), %edx
	cmpl	$-1, %edx
	jne	.LBB11_45
# BB#48:                                # %._crit_edge241.loopexit288
                                        #   in Loop: Header=BB11_18 Depth=1
	movsd	%xmm3, (%rax,%rsi,8)
	jmp	.LBB11_50
	.p2align	4, 0x90
.LBB11_49:                              # %.preheader195.us
                                        #   Parent Loop BB11_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rcx
	movl	alignableReagion.hat1(,%rcx,4), %edx
	cmpl	$-1, %edx
	jne	.LBB11_49
.LBB11_50:                              # %._crit_edge241
                                        #   in Loop: Header=BB11_18 Depth=1
	divsd	%xmm0, %xmm3
	movsd	%xmm3, (%rax,%rsi,8)
	incq	%rsi
	cmpq	%r13, %rsi
	jne	.LBB11_18
.LBB11_51:                              # %._crit_edge243
	movl	$0, 24(%r15)
	movl	$0, 76(%r15)
	movslq	fftWinSize(%rip), %rsi
	testq	%rsi, %rsi
	jle	.LBB11_52
# BB#53:                                # %.lr.ph223
	movq	alignableReagion.stra(%rip), %rcx
	leaq	-1(%rsi), %rax
	movq	%rsi, %rdi
	andq	$7, %rdi
	je	.LBB11_54
# BB#55:                                # %.prol.preheader
	xorpd	%xmm2, %xmm2
	xorl	%edx, %edx
	movq	(%rsp), %r8             # 8-byte Reload
	.p2align	4, 0x90
.LBB11_56:                              # =>This Inner Loop Header: Depth=1
	addsd	(%rcx,%rdx,8), %xmm2
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB11_56
	jmp	.LBB11_57
.LBB11_52:
	xorpd	%xmm2, %xmm2
	movq	(%rsp), %r8             # 8-byte Reload
	jmp	.LBB11_58
.LBB11_54:
	xorl	%edx, %edx
	xorpd	%xmm2, %xmm2
	movq	(%rsp), %r8             # 8-byte Reload
.LBB11_57:                              # %.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB11_58
	.p2align	4, 0x90
.LBB11_77:                              # =>This Inner Loop Header: Depth=1
	addsd	(%rcx,%rdx,8), %xmm2
	addsd	8(%rcx,%rdx,8), %xmm2
	addsd	16(%rcx,%rdx,8), %xmm2
	addsd	24(%rcx,%rdx,8), %xmm2
	addsd	32(%rcx,%rdx,8), %xmm2
	addsd	40(%rcx,%rdx,8), %xmm2
	addsd	48(%rcx,%rdx,8), %xmm2
	addsd	56(%rcx,%rdx,8), %xmm2
	addq	$8, %rdx
	cmpq	%rsi, %rdx
	jl	.LBB11_77
.LBB11_58:                              # %.preheader
	movl	%r8d, %eax
	subl	%esi, %eax
	xorl	%r12d, %r12d
	cmpl	$2, %eax
	jl	.LBB11_76
# BB#59:                                # %.lr.ph.preheader
	xorl	%edx, %edx
	xorpd	%xmm3, %xmm3
	movl	$1, %ebp
	movl	$1, %edi
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB11_60:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rbx
	movq	alignableReagion.stra(%rip), %rax
	subsd	(%rax,%rbx,8), %xmm2
	leal	(%rbx,%rsi), %edx
	movslq	%edx, %rdx
	addsd	(%rax,%rdx,8), %xmm2
	movsd	alignableReagion.threshold(%rip), %xmm0 # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB11_64
# BB#61:                                #   in Loop: Header=BB11_60 Depth=1
	leal	1(%rbx), %eax
	incl	%ecx
	testl	%r14d, %r14d
	cmovel	%edi, %r14d
	cmovel	%eax, %r13d
	xorpd	%xmm1, %xmm1
	je	.LBB11_63
# BB#62:                                #   in Loop: Header=BB11_60 Depth=1
	movapd	%xmm3, %xmm1
.LBB11_63:                              #   in Loop: Header=BB11_60 Depth=1
	cmovel	%edi, %ecx
	addsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm3
.LBB11_64:                              #   in Loop: Header=BB11_60 Depth=1
	ucomisd	%xmm2, %xmm0
	setae	%dl
	cmpl	$150, %ecx
	setg	%al
	testl	%r14d, %r14d
	je	.LBB11_72
# BB#65:                                #   in Loop: Header=BB11_60 Depth=1
	orb	%dl, %al
	je	.LBB11_72
# BB#66:                                #   in Loop: Header=BB11_60 Depth=1
	cmpl	%esi, %ecx
	jle	.LBB11_67
# BB#68:                                #   in Loop: Header=BB11_60 Depth=1
	xorl	%eax, %eax
	cmpl	$150, %ecx
	setg	%al
	movl	%r13d, (%r15)
	leal	1(%rbx), %ecx
	movl	%ecx, 4(%r15)
	movl	fftWinSize(%rip), %esi
	addl	%esi, %r13d
	leal	(%rbx,%r13), %ecx
	leal	1(%rbx,%r13), %edx
	shrl	$31, %edx
	leal	1(%rdx,%rcx), %ecx
	sarl	%ecx
	movl	%ecx, 8(%r15)
	movsd	%xmm3, 16(%r15)
	movl	%eax, 24(%r15)
	movl	%eax, 76(%r15)
	addq	$48, %r15
	incl	%r12d
	movl	%ebp, %r13d
	jmp	.LBB11_69
	.p2align	4, 0x90
.LBB11_67:                              # %._crit_edge285
                                        #   in Loop: Header=BB11_60 Depth=1
	leal	1(%rbx), %r13d
.LBB11_69:                              #   in Loop: Header=BB11_60 Depth=1
	xorpd	%xmm3, %xmm3
	xorl	%r14d, %r14d
	cmpl	$99998, %r12d           # imm = 0x1869E
	jl	.LBB11_71
# BB#70:                                #   in Loop: Header=BB11_60 Depth=1
	movl	$.L.str.7, %edi
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	callq	ErrorExit
	xorpd	%xmm3, %xmm3
	movl	$1, %edi
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	(%rsp), %r8             # 8-byte Reload
	movl	fftWinSize(%rip), %esi
.LBB11_71:                              #   in Loop: Header=BB11_60 Depth=1
	xorl	%ecx, %ecx
.LBB11_72:                              #   in Loop: Header=BB11_60 Depth=1
	incq	%rbp
	movl	%r8d, %eax
	subl	%esi, %eax
	cltq
	leaq	1(%rbx), %rdx
	addq	$2, %rbx
	cmpq	%rax, %rbx
	jl	.LBB11_60
# BB#73:                                # %._crit_edge
	testl	%r14d, %r14d
	je	.LBB11_76
# BB#74:                                # %._crit_edge
	cmpl	%esi, %ecx
	jle	.LBB11_76
# BB#75:
	leaq	1(%rdx), %rax
	movl	%eax, 4(%r15)
	movl	%r13d, (%r15)
	addl	fftWinSize(%rip), %r13d
	leal	(%r13,%rdx), %eax
	leal	1(%rdx,%r13), %ecx
	shrl	$31, %ecx
	leal	1(%rcx,%rax), %eax
	sarl	%eax
	movl	%eax, 8(%r15)
	movsd	%xmm3, 16(%r15)
	incl	%r12d
.LBB11_76:                              # %._crit_edge.thread
	movl	%r12d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	alignableReagion, .Lfunc_end11-alignableReagion
	.cfi_endproc

	.globl	blockAlign
	.p2align	4, 0x90
	.type	blockAlign,@function
blockAlign:                             # @blockAlign
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi95:
	.cfi_def_cfa_offset 80
.Lcfi96:
	.cfi_offset %rbx, -56
.Lcfi97:
	.cfi_offset %r12, -48
.Lcfi98:
	.cfi_offset %r13, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	cmpq	$0, blockAlign.crossscore(%rip)
	jne	.LBB12_2
# BB#1:
	movl	$100000, %edi           # imm = 0x186A0
	movl	$100000, %esi           # imm = 0x186A0
	callq	AllocateDoubleMtx
	movq	%rax, blockAlign.crossscore(%rip)
	movl	$100000, %edi           # imm = 0x186A0
	movl	$100000, %esi           # imm = 0x186A0
	callq	AllocateIntMtx
	movq	%rax, blockAlign.track(%rip)
.LBB12_2:                               # %.preheader156
	movslq	(%r14), %rbp
	testq	%rbp, %rbp
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%rbx, (%rsp)            # 8-byte Spill
	jle	.LBB12_41
# BB#3:                                 # %.preheader155.us.preheader
	movl	%ebp, %r11d
	movq	blockAlign.crossscore(%rip), %r13
	leaq	-4(%rbp), %r9
	movl	%r9d, %r10d
	shrl	$2, %r10d
	incl	%r10d
	movq	%rbp, %r8
	andq	$-4, %r8
	andl	$3, %r10d
	movq	%r10, %rax
	negq	%rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB12_4:                               # %.preheader155.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_14 Depth 2
                                        #     Child Loop BB12_17 Depth 2
                                        #     Child Loop BB12_19 Depth 2
	cmpl	$4, %ebp
	movq	(%r15,%rdx,8), %rsi
	movq	(%r13,%rdx,8), %rax
	jae	.LBB12_6
# BB#5:                                 #   in Loop: Header=BB12_4 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB12_19
	.p2align	4, 0x90
.LBB12_6:                               # %min.iters.checked
                                        #   in Loop: Header=BB12_4 Depth=1
	testq	%r8, %r8
	je	.LBB12_7
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB12_4 Depth=1
	leaq	(%rsi,%rbp,8), %rcx
	cmpq	%rcx, %rax
	jae	.LBB12_11
# BB#9:                                 # %vector.memcheck
                                        #   in Loop: Header=BB12_4 Depth=1
	leaq	(%rax,%rbp,8), %rcx
	cmpq	%rcx, %rsi
	jae	.LBB12_11
# BB#10:                                #   in Loop: Header=BB12_4 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB12_19
.LBB12_7:                               #   in Loop: Header=BB12_4 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB12_19
.LBB12_11:                              # %vector.body.preheader
                                        #   in Loop: Header=BB12_4 Depth=1
	testq	%r10, %r10
	je	.LBB12_12
# BB#13:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB12_4 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_14:                              # %vector.body.prol
                                        #   Parent Loop BB12_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%rsi,%rcx,8), %xmm0
	movupd	16(%rsi,%rcx,8), %xmm1
	movupd	%xmm0, (%rax,%rcx,8)
	movupd	%xmm1, 16(%rax,%rcx,8)
	addq	$4, %rcx
	incq	%rdi
	jne	.LBB12_14
	jmp	.LBB12_15
.LBB12_12:                              #   in Loop: Header=BB12_4 Depth=1
	xorl	%ecx, %ecx
.LBB12_15:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB12_4 Depth=1
	cmpq	$12, %r9
	jb	.LBB12_18
# BB#16:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB12_4 Depth=1
	movq	%r8, %rdi
	subq	%rcx, %rdi
	leaq	112(%rax,%rcx,8), %r15
	leaq	112(%rsi,%rcx,8), %r12
	.p2align	4, 0x90
.LBB12_17:                              # %vector.body
                                        #   Parent Loop BB12_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%r12), %xmm0
	movups	-96(%r12), %xmm1
	movups	%xmm0, -112(%r15)
	movups	%xmm1, -96(%r15)
	movups	-80(%r12), %xmm0
	movups	-64(%r12), %xmm1
	movups	%xmm0, -80(%r15)
	movups	%xmm1, -64(%r15)
	movups	-48(%r12), %xmm0
	movups	-32(%r12), %xmm1
	movups	%xmm0, -48(%r15)
	movups	%xmm1, -32(%r15)
	movupd	-16(%r12), %xmm0
	movupd	(%r12), %xmm1
	movupd	%xmm0, -16(%r15)
	movupd	%xmm1, (%r15)
	subq	$-128, %r15
	subq	$-128, %r12
	addq	$-16, %rdi
	jne	.LBB12_17
.LBB12_18:                              # %middle.block
                                        #   in Loop: Header=BB12_4 Depth=1
	cmpq	%r8, %rbp
	movq	%r8, %rcx
	movq	%rbx, %r15
	je	.LBB12_20
	.p2align	4, 0x90
.LBB12_19:                              # %scalar.ph
                                        #   Parent Loop BB12_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rcx,8), %rdi
	movq	%rdi, (%rax,%rcx,8)
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB12_19
.LBB12_20:                              # %._crit_edge184.us
                                        #   in Loop: Header=BB12_4 Depth=1
	incq	%rdx
	cmpq	%r11, %rdx
	jne	.LBB12_4
# BB#21:                                # %.preheader154
	testl	%ebp, %ebp
	jle	.LBB12_41
# BB#22:                                # %.preheader152
	leal	-1(%rbp), %ebx
	leaq	4(,%rbx,4), %r13
	movl	$blockAlign.ocut1, %edi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r13, %rdx
	callq	memcpy
	movl	$blockAlign.ocut2, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r13, %rdx
	callq	memcpy
	leaq	8(,%rbx,8), %r13
	movl	$blockAlign.max, %edi
	xorl	%esi, %esi
	movq	%r13, %rdx
	callq	memset
	movl	$blockAlign.point, %edi
	xorl	%esi, %esi
	movq	%r13, %rdx
	callq	memset
	cmpl	$1, %ebp
	je	.LBB12_23
# BB#29:                                # %.lr.ph173
	movq	blockAlign.crossscore(%rip), %r9
	movq	blockAlign.track(%rip), %r8
	movl	$1, %edx
	.p2align	4, 0x90
.LBB12_30:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_32 Depth 2
	xorpd	%xmm0, %xmm0
	cmpl	$2, %ebp
	xorpd	%xmm5, %xmm5
	jl	.LBB12_39
# BB#31:                                # %.lr.ph170
                                        #   in Loop: Header=BB12_30 Depth=1
	leaq	-1(%rdx), %rax
	movq	-8(%r9,%rdx,8), %rsi
	movq	(%r8,%rdx,8), %rdi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	movq	(%r9,%rdx,8), %rcx
	cvtsi2sdl	%eax, %xmm2
	xorpd	%xmm0, %xmm0
	xorl	%eax, %eax
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB12_32:                              #   Parent Loop BB12_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi,%rax,8), %xmm5    # xmm5 = mem[0],zero
	movl	$0, 4(%rdi,%rax,4)
	movsd	blockAlign.max+8(,%rax,8), %xmm4 # xmm4 = mem[0],zero
	movl	penalty(%rip), %ebp
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%ebp, %xmm6
	addsd	%xmm4, %xmm6
	ucomisd	%xmm5, %xmm6
	jbe	.LBB12_34
# BB#33:                                #   in Loop: Header=BB12_32 Depth=2
	movsd	blockAlign.point+8(,%rax,8), %xmm5 # xmm5 = mem[0],zero
	subsd	%xmm1, %xmm5
	cvttsd2si	%xmm5, %ebp
	movl	%ebp, 4(%rdi,%rax,4)
	movl	penalty(%rip), %ebp
	movapd	%xmm6, %xmm5
.LBB12_34:                              #   in Loop: Header=BB12_32 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%ebp, %xmm6
	addsd	%xmm3, %xmm6
	ucomisd	%xmm5, %xmm6
	jbe	.LBB12_36
# BB#35:                                #   in Loop: Header=BB12_32 Depth=2
	leal	1(%rax), %ebp
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%ebp, %xmm5
	subsd	%xmm0, %xmm5
	cvttsd2si	%xmm5, %ebp
	movl	%ebp, 4(%rdi,%rax,4)
	movapd	%xmm6, %xmm5
.LBB12_36:                              #   in Loop: Header=BB12_32 Depth=2
	addsd	8(%rcx,%rax,8), %xmm5
	movsd	%xmm5, 8(%rcx,%rax,8)
	movsd	(%rsi,%rax,8), %xmm5    # xmm5 = mem[0],zero
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%eax, %xmm6
	movapd	%xmm3, %xmm7
	cmpltsd	%xmm5, %xmm7
	andpd	%xmm7, %xmm6
	andnpd	%xmm0, %xmm7
	orpd	%xmm6, %xmm7
	movapd	%xmm7, %xmm0
	ucomisd	%xmm4, %xmm5
	jbe	.LBB12_38
# BB#37:                                #   in Loop: Header=BB12_32 Depth=2
	movsd	%xmm5, blockAlign.max+8(,%rax,8)
	movsd	%xmm2, blockAlign.point+8(,%rax,8)
.LBB12_38:                              #   in Loop: Header=BB12_32 Depth=2
	maxsd	%xmm3, %xmm5
	movslq	(%r14), %rbp
	leaq	1(%rax), %rbx
	addq	$2, %rax
	cmpq	%rbp, %rax
	movq	%rbx, %rax
	movapd	%xmm5, %xmm3
	jl	.LBB12_32
.LBB12_39:                              # %._crit_edge171
                                        #   in Loop: Header=BB12_30 Depth=1
	incq	%rdx
	movslq	%ebp, %rax
	cmpq	%rax, %rdx
	jl	.LBB12_30
# BB#40:                                # %.preheader151
	movsd	%xmm5, blockAlign.maxi(%rip)
	movsd	%xmm0, blockAlign.pointi(%rip)
	testl	%ebp, %ebp
	jg	.LBB12_24
.LBB12_41:                              # %._crit_edge166
	decl	%ebp
	movl	%ebp, blockAlign.result1+399996(%rip)
	movl	%ebp, blockAlign.result2+399996(%rip)
	movl	$99999, %ecx            # imm = 0x1869F
	movl	$99999, %edx            # imm = 0x1869F
	movq	blockAlign.track(%rip), %r8
	movl	%ebp, %esi
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	(%rsp), %r10            # 8-byte Reload
	.p2align	4, 0x90
.LBB12_42:                              # %._crit_edge215
                                        # =>This Inner Loop Header: Depth=1
	testl	%esi, %esi
	je	.LBB12_43
# BB#49:                                # %._crit_edge215
                                        #   in Loop: Header=BB12_42 Depth=1
	testl	%ebp, %ebp
	movl	%edx, %eax
	je	.LBB12_44
# BB#50:                                #   in Loop: Header=BB12_42 Depth=1
	movslq	%esi, %rax
	movq	(%r8,%rax,8), %rax
	movslq	%ebp, %rdi
	movl	(%rax,%rdi,4), %eax
	testl	%eax, %eax
	je	.LBB12_51
# BB#52:                                #   in Loop: Header=BB12_42 Depth=1
	jle	.LBB12_54
# BB#53:                                #   in Loop: Header=BB12_42 Depth=1
	decl	%esi
	leaq	-1(%rdx), %rdi
	movl	%esi, blockAlign.result1-4(,%rdx,4)
	subl	%eax, %ebp
	jmp	.LBB12_56
	.p2align	4, 0x90
.LBB12_51:                              #   in Loop: Header=BB12_42 Depth=1
	decl	%esi
	jmp	.LBB12_55
	.p2align	4, 0x90
.LBB12_54:                              #   in Loop: Header=BB12_42 Depth=1
	addl	%eax, %esi
.LBB12_55:                              #   in Loop: Header=BB12_42 Depth=1
	leaq	-1(%rdx), %rdi
	movl	%esi, blockAlign.result1-4(,%rdx,4)
	decl	%ebp
.LBB12_56:                              #   in Loop: Header=BB12_42 Depth=1
	movl	%ebp, blockAlign.result2(,%rdi,4)
	decl	%ecx
	cmpq	$1, %rdx
	movl	%ecx, %eax
	movq	%rdi, %rdx
	jg	.LBB12_42
	jmp	.LBB12_44
.LBB12_43:
	movl	%edx, %eax
.LBB12_44:                              # %.preheader
	xorl	%ebp, %ebp
	cmpl	$99999, %eax            # imm = 0x1869F
	jg	.LBB12_62
# BB#45:                                # %.lr.ph.preheader
	movslq	%eax, %rcx
	xorl	%ebp, %ebp
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB12_46:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	blockAlign.result1(,%rcx,4), %rdx
	movq	(%r15,%rdx,8), %rax
	movl	blockAlign.result2(,%rcx,4), %edi
	movslq	%edi, %rsi
	movsd	(%rax,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB12_47
	jnp	.LBB12_61
.LBB12_47:                              #   in Loop: Header=BB12_46 Depth=1
	movslq	blockAlign.result1-4(,%rcx,4), %rax
	cmpl	%eax, %edx
	movl	blockAlign.result2-4(,%rcx,4), %ebx
	jne	.LBB12_57
# BB#48:                                #   in Loop: Header=BB12_46 Depth=1
	movl	%ebx, %edi
	jmp	.LBB12_58
.LBB12_57:                              #   in Loop: Header=BB12_46 Depth=1
	cmpl	%ebx, %edi
	jne	.LBB12_60
.LBB12_58:                              # %._crit_edge217
                                        #   in Loop: Header=BB12_46 Depth=1
	movq	(%r15,%rax,8), %rax
	movslq	%edi, %rdi
	ucomisd	(%rax,%rdi,8), %xmm1
	jbe	.LBB12_60
# BB#59:                                #   in Loop: Header=BB12_46 Depth=1
	decl	%ebp
.LBB12_60:                              #   in Loop: Header=BB12_46 Depth=1
	movl	blockAlign.ocut1(,%rdx,4), %eax
	movslq	%ebp, %rbp
	movl	%eax, (%r10,%rbp,4)
	movl	blockAlign.ocut2(,%rsi,4), %eax
	movl	%eax, (%r9,%rbp,4)
	incl	%ebp
.LBB12_61:                              #   in Loop: Header=BB12_46 Depth=1
	incq	%rcx
	cmpq	$100000, %rcx           # imm = 0x186A0
	jne	.LBB12_46
.LBB12_62:                              # %._crit_edge
	movl	%ebp, (%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_23:
	movl	$1, %ebp
.LBB12_24:                              # %.preheader150.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB12_25:                              # %.preheader150
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_27 Depth 2
	movq	stderr(%rip), %rcx
	testl	%ebp, %ebp
	jle	.LBB12_28
# BB#26:                                # %.lr.ph162.preheader
                                        #   in Loop: Header=BB12_25 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_27:                              # %.lr.ph162
                                        #   Parent Loop BB12_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	blockAlign.track(%rip), %rax
	movq	(%rax,%r13,8), %rax
	movl	(%rax,%rbp,4), %edx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	incq	%rbp
	movslq	(%r14), %rax
	movq	stderr(%rip), %rcx
	cmpq	%rax, %rbp
	jl	.LBB12_27
.LBB12_28:                              # %._crit_edge163
                                        #   in Loop: Header=BB12_25 Depth=1
	movl	$10, %edi
	movq	%rcx, %rsi
	callq	fputc
	incq	%r13
	movslq	(%r14), %rbp
	cmpq	%rbp, %r13
	jl	.LBB12_25
	jmp	.LBB12_41
.Lfunc_end12:
	.size	blockAlign, .Lfunc_end12-blockAlign
	.cfi_endproc

	.globl	blockAlign2
	.p2align	4, 0x90
	.type	blockAlign2,@function
blockAlign2:                            # @blockAlign2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi108:
	.cfi_def_cfa_offset 128
.Lcfi109:
	.cfi_offset %rbx, -56
.Lcfi110:
	.cfi_offset %r12, -48
.Lcfi111:
	.cfi_offset %r13, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%r9, (%rsp)             # 8-byte Spill
	movl	(%r9), %r13d
	leal	2(%r13), %edx
	cmpl	%edx, blockAlign2.crossscoresize(%rip)
	jge	.LBB13_8
# BB#1:
	movl	%edx, blockAlign2.crossscoresize(%rip)
	cmpl	$0, fftkeika(%rip)
	jne	.LBB13_2
.LBB13_3:
	movq	blockAlign2.track(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_5
# BB#4:
	callq	FreeIntMtx
.LBB13_5:
	movq	blockAlign2.crossscore(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_7
# BB#6:
	callq	FreeDoubleMtx
.LBB13_7:
	movl	blockAlign2.crossscoresize(%rip), %edi
	movl	%edi, %esi
	callq	AllocateIntMtx
	movq	%rax, blockAlign2.track(%rip)
	movl	blockAlign2.crossscoresize(%rip), %edi
	movl	%edi, %esi
	callq	AllocateDoubleMtx
	movq	%rax, blockAlign2.crossscore(%rip)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	(%rax), %r13d
.LBB13_8:                               # %.preheader169
	testl	%r13d, %r13d
	jle	.LBB13_29
# BB#9:                                 # %.preheader168.us.preheader
	movq	blockAlign2.crossscore(%rip), %r11
	movslq	%r13d, %rcx
	movl	%r13d, %r14d
	leaq	-4(%rcx), %r9
	movl	%r9d, %r10d
	shrl	$2, %r10d
	incl	%r10d
	movq	%rcx, %r15
	andq	$-4, %r15
	andl	$3, %r10d
	movq	%r10, %r8
	negq	%r8
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_10:                              # %.preheader168.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_20 Depth 2
                                        #     Child Loop BB13_23 Depth 2
                                        #     Child Loop BB13_25 Depth 2
	cmpl	$4, %r13d
	movq	(%r12,%rbx,8), %rsi
	movq	(%r11,%rbx,8), %rdi
	jae	.LBB13_12
# BB#11:                                #   in Loop: Header=BB13_10 Depth=1
	xorl	%eax, %eax
	jmp	.LBB13_25
	.p2align	4, 0x90
.LBB13_12:                              # %min.iters.checked
                                        #   in Loop: Header=BB13_10 Depth=1
	testq	%r15, %r15
	je	.LBB13_13
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB13_10 Depth=1
	leaq	(%rsi,%rcx,8), %rax
	cmpq	%rax, %rdi
	jae	.LBB13_17
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB13_10 Depth=1
	leaq	(%rdi,%rcx,8), %rax
	cmpq	%rax, %rsi
	jae	.LBB13_17
# BB#16:                                #   in Loop: Header=BB13_10 Depth=1
	xorl	%eax, %eax
	jmp	.LBB13_25
.LBB13_13:                              #   in Loop: Header=BB13_10 Depth=1
	xorl	%eax, %eax
	jmp	.LBB13_25
.LBB13_17:                              # %vector.body.preheader
                                        #   in Loop: Header=BB13_10 Depth=1
	testq	%r10, %r10
	je	.LBB13_18
# BB#19:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB13_10 Depth=1
	movq	%r8, %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_20:                              # %vector.body.prol
                                        #   Parent Loop BB13_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%rsi,%rax,8), %xmm0
	movupd	16(%rsi,%rax,8), %xmm1
	movupd	%xmm0, (%rdi,%rax,8)
	movupd	%xmm1, 16(%rdi,%rax,8)
	addq	$4, %rax
	incq	%rdx
	jne	.LBB13_20
	jmp	.LBB13_21
.LBB13_18:                              #   in Loop: Header=BB13_10 Depth=1
	xorl	%eax, %eax
.LBB13_21:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB13_10 Depth=1
	cmpq	$12, %r9
	jb	.LBB13_24
# BB#22:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB13_10 Depth=1
	movq	%r15, %rbp
	subq	%rax, %rbp
	leaq	112(%rdi,%rax,8), %rdx
	leaq	112(%rsi,%rax,8), %rax
	.p2align	4, 0x90
.LBB13_23:                              # %vector.body
                                        #   Parent Loop BB13_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rax), %xmm0
	movups	-96(%rax), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rax), %xmm0
	movups	-64(%rax), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rax), %xmm0
	movups	-32(%rax), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	movupd	%xmm0, -16(%rdx)
	movupd	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rax
	addq	$-16, %rbp
	jne	.LBB13_23
.LBB13_24:                              # %middle.block
                                        #   in Loop: Header=BB13_10 Depth=1
	cmpq	%r15, %rcx
	movq	%r15, %rax
	je	.LBB13_26
	.p2align	4, 0x90
.LBB13_25:                              # %scalar.ph
                                        #   Parent Loop BB13_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%rax,8), %rdx
	movq	%rdx, (%rdi,%rax,8)
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB13_25
.LBB13_26:                              # %._crit_edge198.us
                                        #   in Loop: Header=BB13_10 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	jne	.LBB13_10
# BB#27:                                # %.preheader167
	testl	%r13d, %r13d
	jle	.LBB13_29
# BB#28:                                # %.preheader166
	leal	-1(%r13), %eax
	leaq	4(,%rax,4), %rbx
	movl	$blockAlign2.ocut1, %edi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	memcpy
	movl	$blockAlign2.ocut2, %edi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	memcpy
	cmpl	$1, %r13d
	jne	.LBB13_30
.LBB13_29:                              # %.preheader166.._crit_edge192_crit_edge
	movq	blockAlign2.track(%rip), %rbp
.LBB13_92:                              # %._crit_edge192
	decl	%r13d
	movl	%r13d, blockAlign2.result1+399996(%rip)
	movl	%r13d, blockAlign2.result2+399996(%rip)
	movl	$99999, %eax            # imm = 0x1869F
	movl	$99999, %ecx            # imm = 0x1869F
	movl	%r13d, %edx
	movq	32(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB13_93:                              # %._crit_edge245
                                        # =>This Inner Loop Header: Depth=1
	testl	%edx, %edx
	je	.LBB13_94
# BB#100:                               # %._crit_edge245
                                        #   in Loop: Header=BB13_93 Depth=1
	testl	%r13d, %r13d
	movl	%ecx, %edi
	je	.LBB13_95
# BB#101:                               #   in Loop: Header=BB13_93 Depth=1
	movslq	%edx, %rsi
	movq	(%rbp,%rsi,8), %rsi
	movslq	%r13d, %rdi
	movl	(%rsi,%rdi,4), %edi
	testl	%edi, %edi
	je	.LBB13_102
# BB#103:                               #   in Loop: Header=BB13_93 Depth=1
	jle	.LBB13_105
# BB#104:                               #   in Loop: Header=BB13_93 Depth=1
	decl	%edx
	leaq	-1(%rcx), %rsi
	movl	%edx, blockAlign2.result1-4(,%rcx,4)
	subl	%edi, %r13d
	jmp	.LBB13_107
	.p2align	4, 0x90
.LBB13_102:                             #   in Loop: Header=BB13_93 Depth=1
	decl	%edx
	jmp	.LBB13_106
	.p2align	4, 0x90
.LBB13_105:                             #   in Loop: Header=BB13_93 Depth=1
	addl	%edi, %edx
.LBB13_106:                             #   in Loop: Header=BB13_93 Depth=1
	leaq	-1(%rcx), %rsi
	movl	%edx, blockAlign2.result1-4(,%rcx,4)
	decl	%r13d
.LBB13_107:                             #   in Loop: Header=BB13_93 Depth=1
	movl	%r13d, blockAlign2.result2(,%rsi,4)
	decl	%eax
	cmpq	$1, %rcx
	movl	%eax, %edi
	movq	%rsi, %rcx
	jg	.LBB13_93
	jmp	.LBB13_95
.LBB13_94:
	movl	%ecx, %edi
.LBB13_95:                              # %.preheader
	xorl	%eax, %eax
	cmpl	$99999, %edi            # imm = 0x1869F
	movq	24(%rsp), %r9           # 8-byte Reload
	jg	.LBB13_113
# BB#96:                                # %.lr.ph.preheader
	movslq	%edi, %rcx
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB13_97:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	blockAlign2.result1(,%rcx,4), %rdx
	movq	(%r12,%rdx,8), %rbp
	movl	blockAlign2.result2(,%rcx,4), %edi
	movslq	%edi, %rsi
	movsd	(%rbp,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB13_98
	jnp	.LBB13_112
.LBB13_98:                              #   in Loop: Header=BB13_97 Depth=1
	movslq	blockAlign2.result1-4(,%rcx,4), %rbp
	cmpl	%ebp, %edx
	movl	blockAlign2.result2-4(,%rcx,4), %ebx
	jne	.LBB13_108
# BB#99:                                #   in Loop: Header=BB13_97 Depth=1
	movl	%ebx, %edi
	jmp	.LBB13_109
.LBB13_108:                             #   in Loop: Header=BB13_97 Depth=1
	cmpl	%ebx, %edi
	jne	.LBB13_111
.LBB13_109:                             # %._crit_edge247
                                        #   in Loop: Header=BB13_97 Depth=1
	movq	(%r12,%rbp,8), %rbp
	movslq	%edi, %rdi
	ucomisd	(%rbp,%rdi,8), %xmm1
	jbe	.LBB13_111
# BB#110:                               #   in Loop: Header=BB13_97 Depth=1
	decl	%eax
.LBB13_111:                             #   in Loop: Header=BB13_97 Depth=1
	movl	blockAlign2.ocut1(,%rdx,4), %edx
	cltq
	movl	%edx, (%r9,%rax,4)
	movl	blockAlign2.ocut2(,%rsi,4), %edx
	movl	%edx, (%r8,%rax,4)
	incl	%eax
.LBB13_112:                             #   in Loop: Header=BB13_97 Depth=1
	incq	%rcx
	cmpq	$100000, %rcx           # imm = 0x186A0
	jne	.LBB13_97
.LBB13_113:                             # %._crit_edge
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%eax, (%rcx)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_30:                              # %.preheader165.lr.ph
	movq	blockAlign2.crossscore(%rip), %r8
	movl	$-1, %ecx
	movq	blockAlign2.track(%rip), %rbp
	movl	$1, %edi
	xorl	%edx, %edx
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_31:                              # %.preheader165
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_34 Depth 2
                                        #       Child Loop BB13_79 Depth 3
                                        #     Child Loop BB13_38 Depth 2
                                        #       Child Loop BB13_57 Depth 3
                                        #       Child Loop BB13_46 Depth 3
	cmpl	$2, %r13d
	jl	.LBB13_91
# BB#32:                                # %.lr.ph183
                                        #   in Loop: Header=BB13_31 Depth=1
	cmpq	$2, %rdi
	movq	-8(%r8,%rdi,8), %rsi
	movq	(%rbp,%rdi,8), %rbx
	movq	(%r8,%rdi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	jle	.LBB13_33
# BB#37:                                # %.lr.ph183.split.us.preheader
                                        #   in Loop: Header=BB13_31 Depth=1
	leal	-1(%rdx), %eax
	movl	%ecx, %r12d
	movsd	blockAlign2.maxj(%rip), %xmm2 # xmm2 = mem[0],zero
	movl	%eax, 20(%rsp)          # 4-byte Spill
	andl	$1, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	$-1, %r14d
	xorl	%r9d, %r9d
	movl	$1, %r10d
	.p2align	4, 0x90
.LBB13_38:                              # %.lr.ph183.split.us
                                        #   Parent Loop BB13_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_57 Depth 3
                                        #       Child Loop BB13_46 Depth 3
	xorpd	%xmm0, %xmm0
	cmpq	$2, %r10
	xorpd	%xmm1, %xmm1
	movl	$0, %r15d
	jle	.LBB13_39
# BB#67:                                # %.lr.ph175.us
                                        #   in Loop: Header=BB13_38 Depth=2
	leal	-1(%r9), %ecx
	testb	$1, %cl
	jne	.LBB13_69
# BB#68:                                #   in Loop: Header=BB13_38 Depth=2
	xorpd	%xmm1, %xmm1
	xorl	%eax, %eax
	jmp	.LBB13_72
	.p2align	4, 0x90
.LBB13_69:                              #   in Loop: Header=BB13_38 Depth=2
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm2, %xmm1
	ja	.LBB13_71
# BB#70:                                #   in Loop: Header=BB13_38 Depth=2
	xorpd	%xmm1, %xmm1
.LBB13_71:                              #   in Loop: Header=BB13_38 Depth=2
	movl	$1, %eax
.LBB13_72:                              # %.prol.loopexit310
                                        #   in Loop: Header=BB13_38 Depth=2
	cmpl	$1, %ecx
	movl	$0, %r15d
	je	.LBB13_39
# BB#73:                                # %.lr.ph175.us.new
                                        #   in Loop: Header=BB13_38 Depth=2
	movl	%r14d, %ebp
	leal	-1(%r13), %ecx
	movslq	%ecx, %rcx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB13_57:                              #   Parent Loop BB13_31 Depth=1
                                        #     Parent Loop BB13_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rax, %rax
	je	.LBB13_60
# BB#58:                                #   in Loop: Header=BB13_57 Depth=3
	cmpq	%rcx, %rax
	jge	.LBB13_60
# BB#59:                                #   in Loop: Header=BB13_57 Depth=3
	cmpq	%rcx, %r10
	jl	.LBB13_62
.LBB13_60:                              #   in Loop: Header=BB13_57 Depth=3
	movsd	(%rsi,%rax,8), %xmm3    # xmm3 = mem[0],zero
	ucomisd	%xmm2, %xmm3
	jbe	.LBB13_62
# BB#61:                                #   in Loop: Header=BB13_57 Depth=3
	movapd	%xmm3, %xmm1
	movl	%eax, %r15d
.LBB13_62:                              #   in Loop: Header=BB13_57 Depth=3
	leaq	1(%rax), %rdx
	cmpq	%rcx, %rdx
	jge	.LBB13_64
# BB#63:                                #   in Loop: Header=BB13_57 Depth=3
	cmpq	%rcx, %r10
	jl	.LBB13_66
.LBB13_64:                              #   in Loop: Header=BB13_57 Depth=3
	movsd	8(%rsi,%rax,8), %xmm3   # xmm3 = mem[0],zero
	ucomisd	%xmm2, %xmm3
	jbe	.LBB13_66
# BB#65:                                #   in Loop: Header=BB13_57 Depth=3
	incl	%eax
	movapd	%xmm3, %xmm1
	movl	%eax, %r15d
.LBB13_66:                              #   in Loop: Header=BB13_57 Depth=3
	incq	%rdx
	cmpq	%rbp, %rdx
	movq	%rdx, %rax
	jne	.LBB13_57
.LBB13_39:                              # %._crit_edge176.us
                                        #   in Loop: Header=BB13_38 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB13_41
# BB#40:                                #   in Loop: Header=BB13_38 Depth=2
	xorl	%edx, %edx
	jmp	.LBB13_44
	.p2align	4, 0x90
.LBB13_41:                              #   in Loop: Header=BB13_38 Depth=2
	movq	(%r8), %rax
	movsd	-8(%rax,%r10,8), %xmm2  # xmm2 = mem[0],zero
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm2
	jbe	.LBB13_43
# BB#42:                                #   in Loop: Header=BB13_38 Depth=2
	movapd	%xmm2, %xmm0
.LBB13_43:                              # %.prol.loopexit318
                                        #   in Loop: Header=BB13_38 Depth=2
	movl	$1, %edx
.LBB13_44:                              # %.prol.loopexit318
                                        #   in Loop: Header=BB13_38 Depth=2
	xorl	%r11d, %r11d
	cmpl	$1, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB13_56
# BB#45:                                # %._crit_edge176.us.new
                                        #   in Loop: Header=BB13_38 Depth=2
	decl	%r13d
	movslq	%r13d, %rbp
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB13_46:                              #   Parent Loop BB13_31 Depth=1
                                        #     Parent Loop BB13_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rdx, %rdx
	je	.LBB13_49
# BB#47:                                #   in Loop: Header=BB13_46 Depth=3
	cmpq	%rbp, %rdx
	jge	.LBB13_49
# BB#48:                                #   in Loop: Header=BB13_46 Depth=3
	cmpq	%rbp, %rdi
	jl	.LBB13_51
.LBB13_49:                              #   in Loop: Header=BB13_46 Depth=3
	movq	(%r8,%rdx,8), %rax
	movsd	-8(%rax,%r10,8), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB13_51
# BB#50:                                #   in Loop: Header=BB13_46 Depth=3
	movapd	%xmm2, %xmm0
	movl	%edx, %r11d
.LBB13_51:                              #   in Loop: Header=BB13_46 Depth=3
	leaq	1(%rdx), %rcx
	cmpq	%rbp, %rcx
	jge	.LBB13_53
# BB#52:                                #   in Loop: Header=BB13_46 Depth=3
	cmpq	%rbp, %rdi
	jl	.LBB13_55
.LBB13_53:                              #   in Loop: Header=BB13_46 Depth=3
	movq	8(%r8,%rdx,8), %rax
	movsd	-8(%rax,%r10,8), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB13_55
# BB#54:                                #   in Loop: Header=BB13_46 Depth=3
	incl	%edx
	movapd	%xmm2, %xmm0
	movl	%edx, %r11d
.LBB13_55:                              #   in Loop: Header=BB13_46 Depth=3
	incq	%rcx
	cmpq	%r12, %rcx
	movq	%rcx, %rdx
	jne	.LBB13_46
.LBB13_56:                              # %._crit_edge180.us
                                        #   in Loop: Header=BB13_38 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	penalty(%rip), %xmm2
	addsd	%xmm2, %xmm1
	addsd	%xmm2, %xmm0
	movsd	-8(%rsi,%r10,8), %xmm2  # xmm2 = mem[0],zero
	movl	%r10d, %eax
	subl	%r15d, %eax
	ucomisd	%xmm2, %xmm1
	movl	$0, %ecx
	cmovbel	%ecx, %eax
	movapd	%xmm1, %xmm3
	maxsd	%xmm2, %xmm3
	movl	%r11d, %ecx
	subl	%edi, %ecx
	ucomisd	%xmm3, %xmm0
	cmovbel	%eax, %ecx
	movapd	%xmm0, %xmm2
	maxsd	%xmm3, %xmm2
	movl	%ecx, (%rbx,%r10,4)
	movq	40(%rsp), %rax          # 8-byte Reload
	addsd	(%rax,%r10,8), %xmm2
	movsd	%xmm2, (%rax,%r10,8)
	incq	%r10
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	(%rax), %r13
	incl	%r14d
	incl	%r9d
	cmpq	%r13, %r10
	movapd	%xmm0, %xmm2
	jl	.LBB13_38
	jmp	.LBB13_90
	.p2align	4, 0x90
.LBB13_33:                              # %.lr.ph183.split.preheader
                                        #   in Loop: Header=BB13_31 Depth=1
	movl	%edi, %r9d
	negl	%r9d
	movl	$-1, %r10d
	xorl	%r14d, %r14d
	movsd	blockAlign2.maxj(%rip), %xmm0 # xmm0 = mem[0],zero
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB13_34:                              # %.lr.ph183.split
                                        #   Parent Loop BB13_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_79 Depth 3
	movq	%rbx, %r12
	xorpd	%xmm1, %xmm1
	cmpq	$3, %rbp
	movl	$0, %r15d
	jl	.LBB13_89
# BB#35:                                # %.lr.ph175
                                        #   in Loop: Header=BB13_34 Depth=2
	leal	-1(%r14), %eax
	testb	$1, %al
	jne	.LBB13_74
# BB#36:                                #   in Loop: Header=BB13_34 Depth=2
	xorpd	%xmm1, %xmm1
	xorl	%ebx, %ebx
	jmp	.LBB13_77
	.p2align	4, 0x90
.LBB13_74:                              #   in Loop: Header=BB13_34 Depth=2
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB13_76
# BB#75:                                #   in Loop: Header=BB13_34 Depth=2
	xorpd	%xmm1, %xmm1
.LBB13_76:                              #   in Loop: Header=BB13_34 Depth=2
	movl	$1, %ebx
.LBB13_77:                              # %.prol.loopexit
                                        #   in Loop: Header=BB13_34 Depth=2
	cmpl	$1, %eax
	movl	$0, %r15d
	je	.LBB13_89
# BB#78:                                # %.lr.ph175.new
                                        #   in Loop: Header=BB13_34 Depth=2
	movl	%r10d, %edx
	decl	%r13d
	movslq	%r13d, %rax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB13_79:                              #   Parent Loop BB13_31 Depth=1
                                        #     Parent Loop BB13_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rbx, %rbx
	je	.LBB13_82
# BB#80:                                #   in Loop: Header=BB13_79 Depth=3
	cmpq	%rax, %rbx
	jge	.LBB13_82
# BB#81:                                #   in Loop: Header=BB13_79 Depth=3
	cmpq	%rax, %rbp
	jl	.LBB13_84
.LBB13_82:                              #   in Loop: Header=BB13_79 Depth=3
	movsd	(%rsi,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB13_84
# BB#83:                                #   in Loop: Header=BB13_79 Depth=3
	movapd	%xmm2, %xmm1
	movl	%ebx, %r15d
.LBB13_84:                              #   in Loop: Header=BB13_79 Depth=3
	leaq	1(%rbx), %rcx
	cmpq	%rax, %rcx
	jge	.LBB13_86
# BB#85:                                #   in Loop: Header=BB13_79 Depth=3
	cmpq	%rax, %rbp
	jl	.LBB13_88
.LBB13_86:                              #   in Loop: Header=BB13_79 Depth=3
	movsd	8(%rsi,%rbx,8), %xmm2   # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB13_88
# BB#87:                                #   in Loop: Header=BB13_79 Depth=3
	incl	%ebx
	movapd	%xmm2, %xmm1
	movl	%ebx, %r15d
.LBB13_88:                              #   in Loop: Header=BB13_79 Depth=3
	incq	%rcx
	cmpq	%rdx, %rcx
	movq	%rcx, %rbx
	jne	.LBB13_79
.LBB13_89:                              # %._crit_edge176
                                        #   in Loop: Header=BB13_34 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	penalty(%rip), %xmm0
	addsd	%xmm0, %xmm1
	movsd	-8(%rsi,%rbp,8), %xmm2  # xmm2 = mem[0],zero
	movl	%ebp, %eax
	subl	%r15d, %eax
	xorl	%r11d, %r11d
	ucomisd	%xmm2, %xmm1
	cmovbel	%r11d, %eax
	movapd	%xmm1, %xmm3
	maxsd	%xmm2, %xmm3
	ucomisd	%xmm3, %xmm0
	cmoval	%r9d, %eax
	movapd	%xmm0, %xmm2
	maxsd	%xmm3, %xmm2
	movq	%r12, %rbx
	movl	%eax, (%rbx,%rbp,4)
	movq	40(%rsp), %rax          # 8-byte Reload
	addsd	(%rax,%rbp,8), %xmm2
	movsd	%xmm2, (%rax,%rbp,8)
	incq	%rbp
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	(%rax), %r13
	incl	%r10d
	incl	%r14d
	cmpq	%r13, %rbp
	jl	.LBB13_34
.LBB13_90:                              # %._crit_edge184
                                        #   in Loop: Header=BB13_31 Depth=1
	movsd	%xmm1, blockAlign2.maxi(%rip)
	movsd	%xmm0, blockAlign2.maxj(%rip)
	movl	%r15d, blockAlign2.pointi(%rip)
	movl	%r11d, blockAlign2.pointj(%rip)
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
.LBB13_91:                              #   in Loop: Header=BB13_31 Depth=1
	incq	%rdi
	movslq	%r13d, %rax
	incl	%ecx
	incl	%edx
	cmpq	%rax, %rdi
	jl	.LBB13_31
	jmp	.LBB13_92
.LBB13_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB13_3
.Lfunc_end13:
	.size	blockAlign2, .Lfunc_end13-blockAlign2
	.cfi_endproc

	.globl	blockAlign3
	.p2align	4, 0x90
	.type	blockAlign3,@function
blockAlign3:                            # @blockAlign3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi121:
	.cfi_def_cfa_offset 96
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r13
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	(%r14), %eax
	leal	2(%rax), %edx
	cmpl	%edx, blockAlign3.crossscoresize(%rip)
	jge	.LBB14_12
# BB#1:
	movl	%edx, blockAlign3.crossscoresize(%rip)
	cmpl	$0, fftkeika(%rip)
	jne	.LBB14_2
.LBB14_3:
	movq	blockAlign3.track(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB14_5
# BB#4:
	callq	FreeIntMtx
.LBB14_5:
	movq	blockAlign3.crossscore(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB14_7
# BB#6:
	callq	FreeDoubleMtx
.LBB14_7:
	movq	blockAlign3.jumppos(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB14_9
# BB#8:
	callq	FreeIntVec
.LBB14_9:
	movq	blockAlign3.jumpscore(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB14_11
# BB#10:
	callq	FreeDoubleVec
.LBB14_11:
	movl	blockAlign3.crossscoresize(%rip), %edi
	movl	%edi, %esi
	callq	AllocateIntMtx
	movq	%rax, blockAlign3.track(%rip)
	movl	blockAlign3.crossscoresize(%rip), %edi
	movl	%edi, %esi
	callq	AllocateDoubleMtx
	movq	%rax, blockAlign3.crossscore(%rip)
	movl	blockAlign3.crossscoresize(%rip), %edi
	callq	AllocateIntVec
	movq	%rax, blockAlign3.jumppos(%rip)
	movl	blockAlign3.crossscoresize(%rip), %edi
	callq	AllocateDoubleVec
	movq	%rax, blockAlign3.jumpscore(%rip)
	movl	(%r14), %eax
.LBB14_12:                              # %.preheader156
	testl	%eax, %eax
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jle	.LBB14_44
# BB#13:                                # %.preheader155.us.preheader
	movq	blockAlign3.crossscore(%rip), %r11
	movslq	%eax, %rdx
	movl	%eax, %r15d
	leaq	-4(%rdx), %r10
	movq	%r10, 32(%rsp)          # 8-byte Spill
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	shrl	$2, %r10d
	incl	%r10d
	movq	%rdx, %r9
	andq	$-4, %r9
	andl	$3, %r10d
	movq	%r10, %rcx
	negq	%rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_14:                              # %.preheader155.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_24 Depth 2
                                        #     Child Loop BB14_27 Depth 2
                                        #     Child Loop BB14_29 Depth 2
	cmpl	$4, %eax
	movq	(%r13,%rbp,8), %rdi
	movq	(%r11,%rbp,8), %rcx
	jae	.LBB14_16
# BB#15:                                #   in Loop: Header=BB14_14 Depth=1
	xorl	%esi, %esi
	jmp	.LBB14_29
	.p2align	4, 0x90
.LBB14_16:                              # %min.iters.checked
                                        #   in Loop: Header=BB14_14 Depth=1
	testq	%r9, %r9
	je	.LBB14_17
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB14_14 Depth=1
	leaq	(%rdi,%rdx,8), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB14_21
# BB#19:                                # %vector.memcheck
                                        #   in Loop: Header=BB14_14 Depth=1
	leaq	(%rcx,%rdx,8), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB14_21
# BB#20:                                #   in Loop: Header=BB14_14 Depth=1
	xorl	%esi, %esi
	jmp	.LBB14_29
.LBB14_17:                              #   in Loop: Header=BB14_14 Depth=1
	xorl	%esi, %esi
	jmp	.LBB14_29
.LBB14_21:                              # %vector.body.preheader
                                        #   in Loop: Header=BB14_14 Depth=1
	testq	%r10, %r10
	je	.LBB14_22
# BB#23:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB14_14 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB14_24:                              # %vector.body.prol
                                        #   Parent Loop BB14_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%rdi,%rsi,8), %xmm0
	movups	16(%rdi,%rsi,8), %xmm1
	movupd	%xmm0, (%rcx,%rsi,8)
	movups	%xmm1, 16(%rcx,%rsi,8)
	addq	$4, %rsi
	incq	%rbx
	jne	.LBB14_24
	jmp	.LBB14_25
.LBB14_22:                              #   in Loop: Header=BB14_14 Depth=1
	xorl	%esi, %esi
.LBB14_25:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB14_14 Depth=1
	cmpq	$12, 32(%rsp)           # 8-byte Folded Reload
	jb	.LBB14_28
# BB#26:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB14_14 Depth=1
	movq	%r9, %rbx
	subq	%rsi, %rbx
	leaq	112(%rcx,%rsi,8), %r12
	leaq	112(%rdi,%rsi,8), %r8
	.p2align	4, 0x90
.LBB14_27:                              # %vector.body
                                        #   Parent Loop BB14_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%r8), %xmm0
	movups	-96(%r8), %xmm1
	movups	%xmm0, -112(%r12)
	movups	%xmm1, -96(%r12)
	movups	-80(%r8), %xmm0
	movups	-64(%r8), %xmm1
	movups	%xmm0, -80(%r12)
	movups	%xmm1, -64(%r12)
	movups	-48(%r8), %xmm0
	movups	-32(%r8), %xmm1
	movups	%xmm0, -48(%r12)
	movups	%xmm1, -32(%r12)
	movupd	-16(%r8), %xmm0
	movups	(%r8), %xmm1
	movupd	%xmm0, -16(%r12)
	movups	%xmm1, (%r12)
	subq	$-128, %r12
	subq	$-128, %r8
	addq	$-16, %rbx
	jne	.LBB14_27
.LBB14_28:                              # %middle.block
                                        #   in Loop: Header=BB14_14 Depth=1
	cmpq	%r9, %rdx
	movq	%r9, %rsi
	je	.LBB14_30
	.p2align	4, 0x90
.LBB14_29:                              # %scalar.ph
                                        #   Parent Loop BB14_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rsi,8), %rbx
	movq	%rbx, (%rcx,%rsi,8)
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB14_29
.LBB14_30:                              # %._crit_edge174.us
                                        #   in Loop: Header=BB14_14 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jne	.LBB14_14
# BB#31:                                # %.preheader154
	testl	%eax, %eax
	jle	.LBB14_44
# BB#32:                                # %.lr.ph169
	decl	%eax
	leaq	4(,%rax,4), %rbx
	movl	$blockAlign3.ocut1, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbx, %rdx
	callq	memcpy
	movl	$blockAlign3.ocut2, %edi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	memcpy
	movq	blockAlign3.jumpscore(%rip), %rcx
	xorl	%edx, %edx
	movq	blockAlign3.jumppos(%rip), %rsi
	movabsq	$-4571364736809679454, %r15 # imm = 0xC08F3FFDF3B645A2
	.p2align	4, 0x90
.LBB14_33:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, (%rcx,%rdx,8)
	movl	$-1, (%rsi,%rdx,4)
	incq	%rdx
	movslq	(%r14), %rax
	cmpq	%rax, %rdx
	jl	.LBB14_33
# BB#34:                                # %.preheader152
	cmpl	$2, %eax
	jl	.LBB14_44
# BB#35:                                # %.lr.ph165.preheader
	movl	$1, %ebx
	jmp	.LBB14_36
.LBB14_37:                              # %.lr.ph162
                                        #   in Loop: Header=BB14_36 Depth=1
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB14_38:                              #   Parent Loop BB14_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	fprintf
	movq	blockAlign3.crossscore(%rip), %rax
	movq	-8(%rax,%rbx,8), %rcx
	movsd	-8(%rcx,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	movq	blockAlign3.track(%rip), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movl	$0, (%rdx,%rbp,4)
	movq	(%rax,%rbx,8), %rax
	addsd	(%rax,%rbp,8), %xmm0
	movsd	%xmm0, (%rax,%rbp,8)
	movsd	(%rcx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	ucomisd	blockAlign3.jumpscorei(%rip), %xmm0
	jbe	.LBB14_40
# BB#39:                                #   in Loop: Header=BB14_38 Depth=2
	movsd	%xmm0, blockAlign3.jumpscorei(%rip)
	movl	%ebp, blockAlign3.jumpposi(%rip)
.LBB14_40:                              #   in Loop: Header=BB14_38 Depth=2
	movq	blockAlign3.jumpscore(%rip), %rcx
	movsd	-8(%rax,%rbp,8), %xmm0  # xmm0 = mem[0],zero
	ucomisd	(%rcx,%rbp,8), %xmm0
	jbe	.LBB14_42
# BB#41:                                #   in Loop: Header=BB14_38 Depth=2
	movsd	%xmm0, (%rcx,%rbp,8)
	movq	blockAlign3.jumppos(%rip), %rax
	movl	%ebx, (%rax,%rbp,4)
.LBB14_42:                              #   in Loop: Header=BB14_38 Depth=2
	incq	%rbp
	movslq	(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB14_38
	jmp	.LBB14_43
	.p2align	4, 0x90
.LBB14_36:                              # %.lr.ph165
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_38 Depth 2
	movq	%r15, blockAlign3.jumpscorei(%rip)
	movl	$-1, blockAlign3.jumpposi(%rip)
	cmpl	$2, %eax
	jge	.LBB14_37
.LBB14_43:                              # %._crit_edge163
                                        #   in Loop: Header=BB14_36 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB14_36
.LBB14_44:                              # %._crit_edge166
	decl	%eax
	movl	%eax, blockAlign3.result1+399996(%rip)
	movl	%eax, blockAlign3.result2+399996(%rip)
	movl	$99999, %ebx            # imm = 0x1869F
	movl	$99999, %esi            # imm = 0x1869F
	movq	blockAlign3.track(%rip), %rdx
	movl	%eax, %edi
	.p2align	4, 0x90
.LBB14_45:                              # %._crit_edge194
                                        # =>This Inner Loop Header: Depth=1
	testl	%edi, %edi
	je	.LBB14_46
# BB#52:                                # %._crit_edge194
                                        #   in Loop: Header=BB14_45 Depth=1
	testl	%eax, %eax
	movl	%esi, %ecx
	je	.LBB14_47
# BB#53:                                #   in Loop: Header=BB14_45 Depth=1
	movslq	%edi, %rcx
	movq	(%rdx,%rcx,8), %rcx
	movslq	%eax, %rbp
	movl	(%rcx,%rbp,4), %ecx
	testl	%ecx, %ecx
	je	.LBB14_54
# BB#55:                                #   in Loop: Header=BB14_45 Depth=1
	jle	.LBB14_57
# BB#56:                                #   in Loop: Header=BB14_45 Depth=1
	decl	%edi
	leaq	-1(%rsi), %rbp
	movl	%edi, blockAlign3.result1-4(,%rsi,4)
	subl	%ecx, %eax
	jmp	.LBB14_59
	.p2align	4, 0x90
.LBB14_54:                              #   in Loop: Header=BB14_45 Depth=1
	decl	%edi
	jmp	.LBB14_58
	.p2align	4, 0x90
.LBB14_57:                              #   in Loop: Header=BB14_45 Depth=1
	addl	%ecx, %edi
.LBB14_58:                              #   in Loop: Header=BB14_45 Depth=1
	leaq	-1(%rsi), %rbp
	movl	%edi, blockAlign3.result1-4(,%rsi,4)
	decl	%eax
.LBB14_59:                              #   in Loop: Header=BB14_45 Depth=1
	movl	%eax, blockAlign3.result2(,%rbp,4)
	decl	%ebx
	cmpq	$1, %rsi
	movl	%ebx, %ecx
	movq	%rbp, %rsi
	jg	.LBB14_45
	jmp	.LBB14_47
.LBB14_46:
	movl	%esi, %ecx
.LBB14_47:                              # %.preheader
	xorl	%eax, %eax
	cmpl	$99999, %ecx            # imm = 0x1869F
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	jg	.LBB14_65
# BB#48:                                # %.lr.ph.preheader
	movslq	%ecx, %rcx
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB14_49:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	blockAlign3.result1(,%rcx,4), %rdx
	movq	(%r13,%rdx,8), %rbp
	movl	blockAlign3.result2(,%rcx,4), %edi
	movslq	%edi, %rsi
	movsd	(%rbp,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB14_50
	jnp	.LBB14_64
.LBB14_50:                              #   in Loop: Header=BB14_49 Depth=1
	movslq	blockAlign3.result1-4(,%rcx,4), %rbp
	cmpl	%ebp, %edx
	movl	blockAlign3.result2-4(,%rcx,4), %ebx
	jne	.LBB14_60
# BB#51:                                #   in Loop: Header=BB14_49 Depth=1
	movl	%ebx, %edi
	jmp	.LBB14_61
.LBB14_60:                              #   in Loop: Header=BB14_49 Depth=1
	cmpl	%ebx, %edi
	jne	.LBB14_63
.LBB14_61:                              # %._crit_edge196
                                        #   in Loop: Header=BB14_49 Depth=1
	movq	(%r13,%rbp,8), %rbp
	movslq	%edi, %rdi
	ucomisd	(%rbp,%rdi,8), %xmm1
	jbe	.LBB14_63
# BB#62:                                #   in Loop: Header=BB14_49 Depth=1
	decl	%eax
.LBB14_63:                              #   in Loop: Header=BB14_49 Depth=1
	movl	blockAlign3.ocut1(,%rdx,4), %edx
	cltq
	movl	%edx, (%r9,%rax,4)
	movl	blockAlign3.ocut2(,%rsi,4), %edx
	movl	%edx, (%r8,%rax,4)
	incl	%eax
.LBB14_64:                              #   in Loop: Header=BB14_49 Depth=1
	incq	%rcx
	cmpq	$100000, %rcx           # imm = 0x186A0
	jne	.LBB14_49
.LBB14_65:                              # %._crit_edge
	movl	%eax, (%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB14_3
.Lfunc_end14:
	.size	blockAlign3, .Lfunc_end14-blockAlign3
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s :         "
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\b\b\b\b\b\b\b\b"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%3d /%3d"
	.size	.L.str.2, 9

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\b\b\b\b\b\b\b\bdone.     \n"
	.size	.L.str.3, 20

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Cannot allocate %d FukusosuuVec\n"
	.size	.L.str.4, 33

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Cannot allocate %d x %d FukusosuuVecMtx\n"
	.size	.L.str.5, 41

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Cannot allocate Fukusosuu"
	.size	.L.str.6, 26

	.type	alignableReagion.stra,@object # @alignableReagion.stra
	.local	alignableReagion.stra
	.comm	alignableReagion.stra,8,8
	.type	alignableReagion.alloclen,@object # @alignableReagion.alloclen
	.local	alignableReagion.alloclen
	.comm	alignableReagion.alloclen,4,4
	.type	alignableReagion.threshold,@object # @alignableReagion.threshold
	.local	alignableReagion.threshold
	.comm	alignableReagion.threshold,8,8
	.type	alignableReagion.prf1,@object # @alignableReagion.prf1
	.local	alignableReagion.prf1
	.comm	alignableReagion.prf1,208,16
	.type	alignableReagion.prf2,@object # @alignableReagion.prf2
	.local	alignableReagion.prf2
	.comm	alignableReagion.prf2,208,16
	.type	alignableReagion.hat1,@object # @alignableReagion.hat1
	.local	alignableReagion.hat1
	.comm	alignableReagion.hat1,108,16
	.type	alignableReagion.hat2,@object # @alignableReagion.hat2
	.local	alignableReagion.hat2
	.comm	alignableReagion.hat2,108,16
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"TOO MANY SEGMENTS!"
	.size	.L.str.7, 19

	.type	blockAlign.result1,@object # @blockAlign.result1
	.local	blockAlign.result1
	.comm	blockAlign.result1,400000,16
	.type	blockAlign.result2,@object # @blockAlign.result2
	.local	blockAlign.result2
	.comm	blockAlign.result2,400000,16
	.type	blockAlign.ocut1,@object # @blockAlign.ocut1
	.local	blockAlign.ocut1
	.comm	blockAlign.ocut1,400000,16
	.type	blockAlign.ocut2,@object # @blockAlign.ocut2
	.local	blockAlign.ocut2
	.comm	blockAlign.ocut2,400000,16
	.type	blockAlign.max,@object  # @blockAlign.max
	.local	blockAlign.max
	.comm	blockAlign.max,800000,16
	.type	blockAlign.maxi,@object # @blockAlign.maxi
	.local	blockAlign.maxi
	.comm	blockAlign.maxi,8,8
	.type	blockAlign.point,@object # @blockAlign.point
	.local	blockAlign.point
	.comm	blockAlign.point,800000,16
	.type	blockAlign.pointi,@object # @blockAlign.pointi
	.local	blockAlign.pointi
	.comm	blockAlign.pointi,8,8
	.type	blockAlign.crossscore,@object # @blockAlign.crossscore
	.local	blockAlign.crossscore
	.comm	blockAlign.crossscore,8,8
	.type	blockAlign.track,@object # @blockAlign.track
	.local	blockAlign.track
	.comm	blockAlign.track,8,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%3d "
	.size	.L.str.8, 5

	.type	blockAlign2.crossscoresize,@object # @blockAlign2.crossscoresize
	.local	blockAlign2.crossscoresize
	.comm	blockAlign2.crossscoresize,4,4
	.type	blockAlign2.result1,@object # @blockAlign2.result1
	.local	blockAlign2.result1
	.comm	blockAlign2.result1,400000,16
	.type	blockAlign2.result2,@object # @blockAlign2.result2
	.local	blockAlign2.result2
	.comm	blockAlign2.result2,400000,16
	.type	blockAlign2.ocut1,@object # @blockAlign2.ocut1
	.local	blockAlign2.ocut1
	.comm	blockAlign2.ocut1,400000,16
	.type	blockAlign2.ocut2,@object # @blockAlign2.ocut2
	.local	blockAlign2.ocut2
	.comm	blockAlign2.ocut2,400000,16
	.type	blockAlign2.crossscore,@object # @blockAlign2.crossscore
	.local	blockAlign2.crossscore
	.comm	blockAlign2.crossscore,8,8
	.type	blockAlign2.track,@object # @blockAlign2.track
	.local	blockAlign2.track
	.comm	blockAlign2.track,8,8
	.type	blockAlign2.maxj,@object # @blockAlign2.maxj
	.local	blockAlign2.maxj
	.comm	blockAlign2.maxj,8,8
	.type	blockAlign2.maxi,@object # @blockAlign2.maxi
	.local	blockAlign2.maxi
	.comm	blockAlign2.maxi,8,8
	.type	blockAlign2.pointj,@object # @blockAlign2.pointj
	.local	blockAlign2.pointj
	.comm	blockAlign2.pointj,4,4
	.type	blockAlign2.pointi,@object # @blockAlign2.pointi
	.local	blockAlign2.pointi
	.comm	blockAlign2.pointi,4,4
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"allocating crossscore and track, size = %d\n"
	.size	.L.str.10, 44

	.type	blockAlign3.crossscoresize,@object # @blockAlign3.crossscoresize
	.local	blockAlign3.crossscoresize
	.comm	blockAlign3.crossscoresize,4,4
	.type	blockAlign3.jumpposi,@object # @blockAlign3.jumpposi
	.local	blockAlign3.jumpposi
	.comm	blockAlign3.jumpposi,4,4
	.type	blockAlign3.jumppos,@object # @blockAlign3.jumppos
	.local	blockAlign3.jumppos
	.comm	blockAlign3.jumppos,8,8
	.type	blockAlign3.jumpscorei,@object # @blockAlign3.jumpscorei
	.local	blockAlign3.jumpscorei
	.comm	blockAlign3.jumpscorei,8,8
	.type	blockAlign3.jumpscore,@object # @blockAlign3.jumpscore
	.local	blockAlign3.jumpscore
	.comm	blockAlign3.jumpscore,8,8
	.type	blockAlign3.result1,@object # @blockAlign3.result1
	.local	blockAlign3.result1
	.comm	blockAlign3.result1,400000,16
	.type	blockAlign3.result2,@object # @blockAlign3.result2
	.local	blockAlign3.result2
	.comm	blockAlign3.result2,400000,16
	.type	blockAlign3.ocut1,@object # @blockAlign3.ocut1
	.local	blockAlign3.ocut1
	.comm	blockAlign3.ocut1,400000,16
	.type	blockAlign3.ocut2,@object # @blockAlign3.ocut2
	.local	blockAlign3.ocut2
	.comm	blockAlign3.ocut2,400000,16
	.type	blockAlign3.crossscore,@object # @blockAlign3.crossscore
	.local	blockAlign3.crossscore
	.comm	blockAlign3.crossscore,8,8
	.type	blockAlign3.track,@object # @blockAlign3.track
	.local	blockAlign3.track
	.comm	blockAlign3.track,8,8
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"in blockalign3, ### i=%d, j=%d\n"
	.size	.L.str.11, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
