	.text
	.file	"opening.bc"
	.globl	opening
	.p2align	4, 0x90
	.type	opening,@function
opening:                                # @opening
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movl	%ecx, %eax
	orl	$2, %eax
	movslq	(%r14), %rdx
	imulq	$44, %rdx, %rdx
	movl	opening.tree(%rdx), %r8d
	movl	$18, %ebx
	movl	$18, %edx
	subl	%r8d, %edx
	cmpl	$3, %eax
	cmovnel	%r8d, %edx
	movl	%edx, (%rdi)
	orl	$1, %ecx
	movslq	(%r14), %rax
	imulq	$44, %rax, %rax
	movl	opening.tree+4(%rax), %eax
	subl	%eax, %ebx
	cmpl	$3, %ecx
	cmovnel	%eax, %ebx
	movl	%ebx, (%rsi)
	movzbl	(%r14), %ecx
	xorl	%eax, %eax
	movl	$1032447, %edx          # imm = 0xFC0FF
	btq	%rcx, %rdx
	jae	.LBB0_2
# BB#1:
	movl	$rd, %edi
	callq	random_nasko
	movl	rd(%rip), %eax
	movslq	(%r14), %rcx
	imulq	$44, %rcx, %rcx
	cltd
	idivl	opening.tree+8(%rcx)
	movslq	%edx, %rax
	movl	opening.tree+12(%rcx,%rax,4), %eax
	movl	%eax, (%r14)
	movl	$1, %eax
.LBB0_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	opening, .Lfunc_end0-opening
	.cfi_endproc

	.type	opening.tree,@object    # @opening.tree
	.section	.rodata,"a",@progbits
	.p2align	4
opening.tree:
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	8                       # 0x8
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	20                      # 0x14
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	14                      # 0xe
	.long	15                      # 0xf
	.long	16                      # 0x10
	.long	17                      # 0x11
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	13                      # 0xd
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	13                      # 0xd
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	32
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	0                       # 0x0
	.zero	32
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	32
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	0                       # 0x0
	.zero	32
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	0                       # 0x0
	.zero	32
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.zero	32
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	1                       # 0x1
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	1                       # 0x1
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	1                       # 0x1
	.long	10                      # 0xa
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	13                      # 0xd
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	13                      # 0xd
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	6                       # 0x6
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	13                      # 0xd
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	32
	.size	opening.tree, 924


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
