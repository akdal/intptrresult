	.text
	.file	"StreamObjects.bc"
	.globl	_ZN12CBufInStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN12CBufInStream4ReadEPvjPj,@function
_ZN12CBufInStream4ReadEPvjPj:           # @_ZN12CBufInStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB0_2
# BB#1:
	movl	$0, (%r14)
.LBB0_2:
	testl	%edx, %edx
	je	.LBB0_6
# BB#3:
	movq	24(%r15), %rcx
	movq	32(%r15), %rbx
	movl	$-2147467259, %eax      # imm = 0x80004005
	subq	%rcx, %rbx
	jb	.LBB0_7
# BB#4:
	movl	%edx, %eax
	cmpq	%rax, %rbx
	cmovaq	%rax, %rbx
	addq	16(%r15), %rcx
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, 24(%r15)
	testq	%r14, %r14
	je	.LBB0_6
# BB#5:
	movl	%ebx, (%r14)
.LBB0_6:
	xorl	%eax, %eax
.LBB0_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN12CBufInStream4ReadEPvjPj, .Lfunc_end0-_ZN12CBufInStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN12CBufInStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN12CBufInStream4SeekExjPy,@function
_ZN12CBufInStream4SeekExjPy:            # @_ZN12CBufInStream4SeekExjPy
	.cfi_startproc
# BB#0:
	cmpl	$2, %edx
	je	.LBB1_4
# BB#1:
	cmpl	$1, %edx
	je	.LBB1_3
# BB#2:
	movl	$-2147287039, %eax      # imm = 0x80030001
	testl	%edx, %edx
	je	.LBB1_5
	jmp	.LBB1_7
.LBB1_4:
	addq	32(%rdi), %rsi
	jmp	.LBB1_5
.LBB1_3:
	addq	24(%rdi), %rsi
.LBB1_5:
	movq	%rsi, 24(%rdi)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB1_7
# BB#6:
	movq	%rsi, (%rcx)
.LBB1_7:
	retq
.Lfunc_end1:
	.size	_ZN12CBufInStream4SeekExjPy, .Lfunc_end1-_ZN12CBufInStream4SeekExjPy
	.cfi_endproc

	.globl	_ZN14CByteDynBuffer4FreeEv
	.p2align	4, 0x90
	.type	_ZN14CByteDynBuffer4FreeEv,@function
_ZN14CByteDynBuffer4FreeEv:             # @_ZN14CByteDynBuffer4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	callq	free
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN14CByteDynBuffer4FreeEv, .Lfunc_end2-_ZN14CByteDynBuffer4FreeEv
	.cfi_endproc

	.globl	_ZN14CByteDynBuffer14EnsureCapacityEm
	.p2align	4, 0x90
	.type	_ZN14CByteDynBuffer14EnsureCapacityEm,@function
_ZN14CByteDynBuffer14EnsureCapacityEm:  # @_ZN14CByteDynBuffer14EnsureCapacityEm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movb	$1, %r14b
	cmpq	%rsi, %rax
	jae	.LBB3_4
# BB#1:
	movq	%rax, %rcx
	shrq	$2, %rcx
	cmpq	$8, %rax
	movl	$16, %edx
	movl	$4, %ebp
	cmovaq	%rdx, %rbp
	cmpq	$64, %rax
	cmovaq	%rcx, %rbp
	addq	%rax, %rbp
	cmpq	%rsi, %rbp
	cmovbeq	%rsi, %rbp
	movq	8(%rbx), %rdi
	movq	%rbp, %rsi
	callq	realloc
	testq	%rax, %rax
	je	.LBB3_2
# BB#3:
	movq	%rax, 8(%rbx)
	movq	%rbp, (%rbx)
	jmp	.LBB3_4
.LBB3_2:
	xorl	%r14d, %r14d
.LBB3_4:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN14CByteDynBuffer14EnsureCapacityEm, .Lfunc_end3-_ZN14CByteDynBuffer14EnsureCapacityEm
	.cfi_endproc

	.globl	_ZN19CDynBufSeqOutStream19GetBufPtrForWritingEm
	.p2align	4, 0x90
	.type	_ZN19CDynBufSeqOutStream19GetBufPtrForWritingEm,@function
_ZN19CDynBufSeqOutStream19GetBufPtrForWritingEm: # @_ZN19CDynBufSeqOutStream19GetBufPtrForWritingEm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	32(%r14), %rax
	addq	%rax, %rsi
	jae	.LBB4_2
.LBB4_7:
	xorl	%eax, %eax
	jmp	.LBB4_8
.LBB4_2:
	movq	16(%r14), %rcx
	cmpq	%rsi, %rcx
	jae	.LBB4_5
# BB#3:
	movq	%rcx, %rax
	shrq	$2, %rax
	cmpq	$8, %rcx
	movl	$16, %edx
	movl	$4, %ebx
	cmovaq	%rdx, %rbx
	cmpq	$64, %rcx
	cmovaq	%rax, %rbx
	addq	%rcx, %rbx
	cmpq	%rsi, %rbx
	cmovbeq	%rsi, %rbx
	movq	24(%r14), %rdi
	movq	%rbx, %rsi
	callq	realloc
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB4_7
# BB#4:
	movq	%rcx, 24(%r14)
	movq	%rbx, 16(%r14)
	movq	32(%r14), %rax
	jmp	.LBB4_6
.LBB4_5:                                # %._crit_edge
	movq	24(%r14), %rcx
.LBB4_6:
	addq	%rcx, %rax
.LBB4_8:                                # %_ZN14CByteDynBuffer14EnsureCapacityEm.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZN19CDynBufSeqOutStream19GetBufPtrForWritingEm, .Lfunc_end4-_ZN19CDynBufSeqOutStream19GetBufPtrForWritingEm
	.cfi_endproc

	.globl	_ZNK19CDynBufSeqOutStream12CopyToBufferER7CBufferIhE
	.p2align	4, 0x90
	.type	_ZNK19CDynBufSeqOutStream12CopyToBufferER7CBufferIhE,@function
_ZNK19CDynBufSeqOutStream12CopyToBufferER7CBufferIhE: # @_ZNK19CDynBufSeqOutStream12CopyToBufferER7CBufferIhE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r13, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	32(%r14), %r13
	movq	8(%r15), %rbx
	cmpq	%r13, %rbx
	jne	.LBB5_2
# BB#1:                                 # %._ZN7CBufferIhE11SetCapacityEm.exit_crit_edge
	movq	16(%r15), %r12
	jmp	.LBB5_9
.LBB5_2:
	testq	%r13, %r13
	je	.LBB5_3
# BB#4:
	movq	%r13, %rdi
	callq	_Znam
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.LBB5_6
# BB#5:
	movq	16(%r15), %rsi
	cmpq	%r13, %rbx
	cmovaeq	%r13, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	memmove
	jmp	.LBB5_6
.LBB5_3:
	xorl	%r12d, %r12d
.LBB5_6:
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	movq	%r13, %rax
	je	.LBB5_8
# BB#7:
	callq	_ZdaPv
	movq	32(%r14), %rax
.LBB5_8:
	movq	%r12, 16(%r15)
	movq	%r13, 8(%r15)
	movq	%rax, %r13
.LBB5_9:                                # %_ZN7CBufferIhE11SetCapacityEm.exit
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	memcpy                  # TAILCALL
.Lfunc_end5:
	.size	_ZNK19CDynBufSeqOutStream12CopyToBufferER7CBufferIhE, .Lfunc_end5-_ZNK19CDynBufSeqOutStream12CopyToBufferER7CBufferIhE
	.cfi_endproc

	.globl	_ZN19CDynBufSeqOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN19CDynBufSeqOutStream5WriteEPKvjPj,@function
_ZN19CDynBufSeqOutStream5WriteEPKvjPj:  # @_ZN19CDynBufSeqOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 64
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movl	%edx, %r14d
	movq	%rdi, %rbp
	testq	%rbx, %rbx
	je	.LBB6_2
# BB#1:
	movl	$0, (%rbx)
.LBB6_2:
	testl	%r14d, %r14d
	je	.LBB6_11
# BB#3:
	movl	%r14d, %r13d
	movq	32(%rbp), %rdi
	movq	%rdi, %rax
	addq	%r13, %rax
	movl	$-2147024882, %r12d     # imm = 0x8007000E
	jb	.LBB6_12
# BB#4:
	movq	16(%rbp), %rcx
	cmpq	%rax, %rcx
	jae	.LBB6_5
# BB#6:
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rcx, %rdx
	shrq	$2, %rdx
	cmpq	$8, %rcx
	movl	$16, %esi
	movl	$4, %r15d
	cmovaq	%rsi, %r15
	cmpq	$64, %rcx
	cmovaq	%rdx, %r15
	addq	%rcx, %r15
	cmpq	%rax, %r15
	cmovbeq	%rax, %r15
	movq	24(%rbp), %rdi
	movq	%r15, %rsi
	callq	realloc
	testq	%rax, %rax
	je	.LBB6_12
# BB#7:
	movq	%rax, 24(%rbp)
	movq	%r15, 16(%rbp)
	movq	32(%rbp), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	addq	%rax, %rdi
	jne	.LBB6_9
	jmp	.LBB6_12
.LBB6_5:                                # %._crit_edge.i
	movq	24(%rbp), %rax
	addq	%rax, %rdi
	je	.LBB6_12
.LBB6_9:
	movq	%r13, %rdx
	callq	memcpy
	addq	%r13, 32(%rbp)
	testq	%rbx, %rbx
	je	.LBB6_11
# BB#10:
	movl	%r14d, (%rbx)
.LBB6_11:                               # %_ZN19CDynBufSeqOutStream19GetBufPtrForWritingEm.exit.thread
	xorl	%r12d, %r12d
.LBB6_12:                               # %_ZN19CDynBufSeqOutStream19GetBufPtrForWritingEm.exit.thread
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN19CDynBufSeqOutStream5WriteEPKvjPj, .Lfunc_end6-_ZN19CDynBufSeqOutStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN19CBufPtrSeqOutStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN19CBufPtrSeqOutStream5WriteEPKvjPj,@function
_ZN19CBufPtrSeqOutStream5WriteEPKvjPj:  # @_ZN19CBufPtrSeqOutStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %r15d
	movq	%rdi, %rbp
	movq	24(%rbp), %rbx
	movq	32(%rbp), %rdi
	subq	%rdi, %rbx
	movl	%r15d, %eax
	cmpq	%rax, %rbx
	cmovaq	%rax, %rbx
	addq	16(%rbp), %rdi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, 32(%rbp)
	testq	%r14, %r14
	je	.LBB7_2
# BB#1:
	movl	%ebx, (%r14)
.LBB7_2:
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%ecx, %eax
	testl	%r15d, %r15d
	cmovel	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN19CBufPtrSeqOutStream5WriteEPKvjPj, .Lfunc_end7-_ZN19CBufPtrSeqOutStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN29CSequentialOutStreamSizeCount5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamSizeCount5WriteEPKvjPj,@function
_ZN29CSequentialOutStreamSizeCount5WriteEPKvjPj: # @_ZN29CSequentialOutStreamSizeCount5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -24
.Lcfi55:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	movl	4(%rsp), %ecx
	addq	%rcx, 24(%rbx)
	testq	%r14, %r14
	je	.LBB8_2
# BB#1:
	movl	%ecx, (%r14)
.LBB8_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	_ZN29CSequentialOutStreamSizeCount5WriteEPKvjPj, .Lfunc_end8-_ZN29CSequentialOutStreamSizeCount5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN15CCachedInStream4FreeEv
	.p2align	4, 0x90
	.type	_ZN15CCachedInStream4FreeEv,@function
_ZN15CCachedInStream4FreeEv:            # @_ZN15CCachedInStream4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 16
.Lcfi57:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	callq	MyFree
	movq	$0, 16(%rbx)
	movq	24(%rbx), %rdi
	callq	MidFree
	movq	$0, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN15CCachedInStream4FreeEv, .Lfunc_end9-_ZN15CCachedInStream4FreeEv
	.cfi_endproc

	.globl	_ZN15CCachedInStream5AllocEjj
	.p2align	4, 0x90
	.type	_ZN15CCachedInStream5AllocEjj,@function
_ZN15CCachedInStream5AllocEjj:          # @_ZN15CCachedInStream5AllocEjj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 48
.Lcfi63:
	.cfi_offset %rbx, -40
.Lcfi64:
	.cfi_offset %r12, -32
.Lcfi65:
	.cfi_offset %r14, -24
.Lcfi66:
	.cfi_offset %r15, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movq	%rdi, %r12
	leal	(%r15,%r14), %ecx
	cmpl	$63, %ecx
	jbe	.LBB10_2
.LBB10_12:
	xorl	%eax, %eax
	jmp	.LBB10_13
.LBB10_2:
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rbx
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_4
# BB#3:
	cmpq	32(%r12), %rbx
	je	.LBB10_6
.LBB10_4:
	callq	MidFree
	movq	%rbx, %rdi
	callq	MidAlloc
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.LBB10_12
# BB#5:
	movq	%rbx, 32(%r12)
.LBB10_6:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB10_8
# BB#7:
	cmpl	%r15d, 44(%r12)
	je	.LBB10_10
.LBB10_8:
	callq	MyFree
	movl	$8, %edi
	movl	%r15d, %ecx
	shlq	%cl, %rdi
	callq	MyAlloc
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.LBB10_12
# BB#9:
	movl	%r15d, 44(%r12)
.LBB10_10:
	movl	%r14d, 40(%r12)
	movb	$1, %al
.LBB10_13:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZN15CCachedInStream5AllocEjj, .Lfunc_end10-_ZN15CCachedInStream5AllocEjj
	.cfi_endproc

	.globl	_ZN15CCachedInStream4InitEy
	.p2align	4, 0x90
	.type	_ZN15CCachedInStream4InitEy,@function
_ZN15CCachedInStream4InitEy:            # @_ZN15CCachedInStream4InitEy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 16
	movq	%rsi, 48(%rdi)
	movq	$0, 56(%rdi)
	movb	44(%rdi), %cl
	movl	$1, %eax
	movl	$1, %edx
	shlq	%cl, %rdx
	movq	16(%rdi), %rdi
	cmpq	$1, %rdx
	cmovbeq	%rax, %rdx
	shlq	$3, %rdx
	movl	$255, %esi
	callq	memset
	popq	%rax
	retq
.Lfunc_end11:
	.size	_ZN15CCachedInStream4InitEy, .Lfunc_end11-_ZN15CCachedInStream4InitEy
	.cfi_endproc

	.globl	_ZN15CCachedInStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN15CCachedInStream4ReadEPvjPj,@function
_ZN15CCachedInStream4ReadEPvjPj:        # @_ZN15CCachedInStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 64
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	testq	%rcx, %rcx
	je	.LBB12_2
# BB#1:
	movl	$0, (%rcx)
.LBB12_2:
	testl	%edx, %edx
	je	.LBB12_15
# BB#3:
	movq	48(%r12), %r14
	movq	56(%r12), %rsi
	movl	$-2147467259, %eax      # imm = 0x80004005
	subq	%rsi, %r14
	jb	.LBB12_16
# BB#4:
	movl	%edx, %eax
	cmpq	%r14, %rax
	cmovbel	%edx, %r14d
	testl	%r14d, %r14d
	je	.LBB12_15
# BB#5:                                 # %.lr.ph
	testq	%rcx, %rcx
	je	.LBB12_11
# BB#6:                                 # %.lr.ph.split.us.preheader
	movq	%rbx, %rbp
	movq	%rcx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB12_7:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	40(%r12), %edx
	movq	%rsi, %r13
	movl	%edx, %ecx
	shrq	%cl, %r13
	movzbl	44(%r12), %ecx
	movl	$1, %ebx
	shlq	%cl, %rbx
	decq	%rbx
	andq	%r13, %rbx
	movq	%rbx, %r15
	movl	%edx, %ecx
	shlq	%cl, %r15
	addq	24(%r12), %r15
	movq	16(%r12), %rax
	cmpq	%r13, (%rax,%rbx,8)
	je	.LBB12_10
# BB#8:                                 #   in Loop: Header=BB12_7 Depth=1
	movq	(%r12), %r8
	movq	48(%r12), %rsi
	movq	%r13, %rax
	movl	%edx, %ecx
	shlq	%cl, %rax
	subq	%rax, %rsi
	movl	$1, %eax
	shlq	%cl, %rax
	cmpq	%rsi, %rax
	cmovaq	%rsi, %rax
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%rax, %rcx
	callq	*56(%r8)
	testl	%eax, %eax
	jne	.LBB12_16
# BB#9:                                 # %.thread.us
                                        #   in Loop: Header=BB12_7 Depth=1
	movq	16(%r12), %rax
	movq	%r13, (%rax,%rbx,8)
	movq	56(%r12), %rsi
	movl	40(%r12), %edx
.LBB12_10:                              # %.thread69.us
                                        #   in Loop: Header=BB12_7 Depth=1
	movl	$1, %ebx
	movl	%edx, %ecx
	shlq	%cl, %rbx
	leaq	-1(%rbx), %rax
	andq	%rsi, %rax
	subq	%rax, %rbx
	movl	%r14d, %ecx
	cmpq	%rcx, %rbx
	cmovaeq	%rcx, %rbx
	addq	%rax, %r15
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	(%rsp), %rax            # 8-byte Reload
	addl	%ebx, (%rax)
	addq	%rbx, %rbp
	movq	%rbx, %rsi
	addq	56(%r12), %rsi
	movq	%rsi, 56(%r12)
	subl	%ebx, %r14d
	jne	.LBB12_7
	jmp	.LBB12_15
	.p2align	4, 0x90
.LBB12_11:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, (%rsp)            # 8-byte Spill
	movl	40(%r12), %eax
	movq	%rsi, %r15
	movl	%eax, %ecx
	shrq	%cl, %r15
	movzbl	44(%r12), %ecx
	movl	$1, %ebx
	shlq	%cl, %rbx
	decq	%rbx
	andq	%r15, %rbx
	movq	%rbx, %r13
	movl	%eax, %ecx
	shlq	%cl, %r13
	addq	24(%r12), %r13
	movq	16(%r12), %rcx
	cmpq	%r15, (%rcx,%rbx,8)
	je	.LBB12_14
# BB#12:                                #   in Loop: Header=BB12_11 Depth=1
	movq	(%r12), %r8
	movq	48(%r12), %rdx
	movq	%r15, %rsi
	movl	%eax, %ecx
	shlq	%cl, %rsi
	subq	%rsi, %rdx
	movl	$1, %ebp
	shlq	%cl, %rbp
	cmpq	%rdx, %rbp
	cmovaq	%rdx, %rbp
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%rbp, %rcx
	callq	*56(%r8)
	testl	%eax, %eax
	jne	.LBB12_16
# BB#13:                                # %.thread
                                        #   in Loop: Header=BB12_11 Depth=1
	movq	16(%r12), %rax
	movq	%r15, (%rax,%rbx,8)
	movq	56(%r12), %rsi
	movl	40(%r12), %eax
.LBB12_14:                              # %.thread69
                                        #   in Loop: Header=BB12_11 Depth=1
	movl	$1, %ebp
	movl	%eax, %ecx
	shlq	%cl, %rbp
	leaq	-1(%rbp), %rax
	andq	%rsi, %rax
	subq	%rax, %rbp
	movl	%r14d, %ecx
	cmpq	%rcx, %rbp
	cmovaeq	%rcx, %rbp
	addq	%rax, %r13
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	addq	%rbp, %rbx
	movq	%rbp, %rsi
	addq	56(%r12), %rsi
	movq	%rsi, 56(%r12)
	subl	%ebp, %r14d
	jne	.LBB12_11
.LBB12_15:
	xorl	%eax, %eax
.LBB12_16:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN15CCachedInStream4ReadEPvjPj, .Lfunc_end12-_ZN15CCachedInStream4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN15CCachedInStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN15CCachedInStream4SeekExjPy,@function
_ZN15CCachedInStream4SeekExjPy:         # @_ZN15CCachedInStream4SeekExjPy
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	je	.LBB13_6
# BB#1:
	cmpl	$2, %edx
	je	.LBB13_4
# BB#2:
	movl	$-2147287039, %eax      # imm = 0x80030001
	cmpl	$1, %edx
	jne	.LBB13_8
# BB#3:
	leaq	56(%rdi), %rax
	jmp	.LBB13_5
.LBB13_4:
	leaq	48(%rdi), %rax
.LBB13_5:                               # %.sink.split
	addq	(%rax), %rsi
.LBB13_6:
	movq	%rsi, 56(%rdi)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB13_8
# BB#7:
	movq	%rsi, (%rcx)
.LBB13_8:
	retq
.Lfunc_end13:
	.size	_ZN15CCachedInStream4SeekExjPy, .Lfunc_end13-_ZN15CCachedInStream4SeekExjPy
	.cfi_endproc

	.section	.text._ZN12CBufInStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN12CBufInStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN12CBufInStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN12CBufInStream14QueryInterfaceERK4GUIDPPv,@function
_ZN12CBufInStream14QueryInterfaceERK4GUIDPPv: # @_ZN12CBufInStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB14_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB14_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB14_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB14_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB14_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB14_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB14_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB14_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB14_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB14_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB14_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB14_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB14_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB14_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB14_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB14_32
.LBB14_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInStream(%rip), %cl
	jne	.LBB14_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IInStream+1(%rip), %cl
	jne	.LBB14_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IInStream+2(%rip), %cl
	jne	.LBB14_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IInStream+3(%rip), %cl
	jne	.LBB14_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IInStream+4(%rip), %cl
	jne	.LBB14_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IInStream+5(%rip), %cl
	jne	.LBB14_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IInStream+6(%rip), %cl
	jne	.LBB14_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IInStream+7(%rip), %cl
	jne	.LBB14_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IInStream+8(%rip), %cl
	jne	.LBB14_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IInStream+9(%rip), %cl
	jne	.LBB14_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IInStream+10(%rip), %cl
	jne	.LBB14_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IInStream+11(%rip), %cl
	jne	.LBB14_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IInStream+12(%rip), %cl
	jne	.LBB14_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IInStream+13(%rip), %cl
	jne	.LBB14_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IInStream+14(%rip), %cl
	jne	.LBB14_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IInStream+15(%rip), %cl
	jne	.LBB14_33
.LBB14_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB14_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZN12CBufInStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end14-_ZN12CBufInStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN12CBufInStream6AddRefEv,"axG",@progbits,_ZN12CBufInStream6AddRefEv,comdat
	.weak	_ZN12CBufInStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN12CBufInStream6AddRefEv,@function
_ZN12CBufInStream6AddRefEv:             # @_ZN12CBufInStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end15:
	.size	_ZN12CBufInStream6AddRefEv, .Lfunc_end15-_ZN12CBufInStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN12CBufInStream7ReleaseEv,"axG",@progbits,_ZN12CBufInStream7ReleaseEv,comdat
	.weak	_ZN12CBufInStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN12CBufInStream7ReleaseEv,@function
_ZN12CBufInStream7ReleaseEv:            # @_ZN12CBufInStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB16_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB16_2:
	popq	%rcx
	retq
.Lfunc_end16:
	.size	_ZN12CBufInStream7ReleaseEv, .Lfunc_end16-_ZN12CBufInStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN12CBufInStreamD2Ev,"axG",@progbits,_ZN12CBufInStreamD2Ev,comdat
	.weak	_ZN12CBufInStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN12CBufInStreamD2Ev,@function
_ZN12CBufInStreamD2Ev:                  # @_ZN12CBufInStreamD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV12CBufInStream+16, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB17_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB17_1:                               # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
	retq
.Lfunc_end17:
	.size	_ZN12CBufInStreamD2Ev, .Lfunc_end17-_ZN12CBufInStreamD2Ev
	.cfi_endproc

	.section	.text._ZN12CBufInStreamD0Ev,"axG",@progbits,_ZN12CBufInStreamD0Ev,comdat
	.weak	_ZN12CBufInStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN12CBufInStreamD0Ev,@function
_ZN12CBufInStreamD0Ev:                  # @_ZN12CBufInStreamD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -24
.Lcfi87:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV12CBufInStream+16, (%rbx)
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp0:
	callq	*16(%rax)
.Ltmp1:
.LBB18_2:                               # %_ZN12CBufInStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_3:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZN12CBufInStreamD0Ev, .Lfunc_end18-_ZN12CBufInStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end18-.Ltmp1     #   Call between .Ltmp1 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN19CDynBufSeqOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN19CDynBufSeqOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN19CDynBufSeqOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN19CDynBufSeqOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN19CDynBufSeqOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN19CDynBufSeqOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB19_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB19_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB19_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB19_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB19_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB19_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB19_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB19_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB19_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB19_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB19_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB19_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB19_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB19_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB19_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB19_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB19_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZN19CDynBufSeqOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end19-_ZN19CDynBufSeqOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN19CDynBufSeqOutStream6AddRefEv,"axG",@progbits,_ZN19CDynBufSeqOutStream6AddRefEv,comdat
	.weak	_ZN19CDynBufSeqOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN19CDynBufSeqOutStream6AddRefEv,@function
_ZN19CDynBufSeqOutStream6AddRefEv:      # @_ZN19CDynBufSeqOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end20:
	.size	_ZN19CDynBufSeqOutStream6AddRefEv, .Lfunc_end20-_ZN19CDynBufSeqOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN19CDynBufSeqOutStream7ReleaseEv,"axG",@progbits,_ZN19CDynBufSeqOutStream7ReleaseEv,comdat
	.weak	_ZN19CDynBufSeqOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN19CDynBufSeqOutStream7ReleaseEv,@function
_ZN19CDynBufSeqOutStream7ReleaseEv:     # @_ZN19CDynBufSeqOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB21_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB21_2:
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZN19CDynBufSeqOutStream7ReleaseEv, .Lfunc_end21-_ZN19CDynBufSeqOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN19CDynBufSeqOutStreamD2Ev,"axG",@progbits,_ZN19CDynBufSeqOutStreamD2Ev,comdat
	.weak	_ZN19CDynBufSeqOutStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN19CDynBufSeqOutStreamD2Ev,@function
_ZN19CDynBufSeqOutStreamD2Ev:           # @_ZN19CDynBufSeqOutStreamD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 16
.Lcfi91:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV19CDynBufSeqOutStream+16, (%rbx)
	movq	24(%rbx), %rdi
	callq	free
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	popq	%rbx
	retq
.Lfunc_end22:
	.size	_ZN19CDynBufSeqOutStreamD2Ev, .Lfunc_end22-_ZN19CDynBufSeqOutStreamD2Ev
	.cfi_endproc

	.section	.text._ZN19CDynBufSeqOutStreamD0Ev,"axG",@progbits,_ZN19CDynBufSeqOutStreamD0Ev,comdat
	.weak	_ZN19CDynBufSeqOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN19CDynBufSeqOutStreamD0Ev,@function
_ZN19CDynBufSeqOutStreamD0Ev:           # @_ZN19CDynBufSeqOutStreamD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 16
.Lcfi93:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV19CDynBufSeqOutStream+16, (%rbx)
	movq	24(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end23:
	.size	_ZN19CDynBufSeqOutStreamD0Ev, .Lfunc_end23-_ZN19CDynBufSeqOutStreamD0Ev
	.cfi_endproc

	.section	.text._ZN19CBufPtrSeqOutStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN19CBufPtrSeqOutStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN19CBufPtrSeqOutStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN19CBufPtrSeqOutStream14QueryInterfaceERK4GUIDPPv,@function
_ZN19CBufPtrSeqOutStream14QueryInterfaceERK4GUIDPPv: # @_ZN19CBufPtrSeqOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB24_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB24_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB24_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB24_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB24_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB24_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB24_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB24_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB24_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB24_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB24_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB24_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB24_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB24_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB24_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB24_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB24_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN19CBufPtrSeqOutStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end24-_ZN19CBufPtrSeqOutStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN19CBufPtrSeqOutStream6AddRefEv,"axG",@progbits,_ZN19CBufPtrSeqOutStream6AddRefEv,comdat
	.weak	_ZN19CBufPtrSeqOutStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN19CBufPtrSeqOutStream6AddRefEv,@function
_ZN19CBufPtrSeqOutStream6AddRefEv:      # @_ZN19CBufPtrSeqOutStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end25:
	.size	_ZN19CBufPtrSeqOutStream6AddRefEv, .Lfunc_end25-_ZN19CBufPtrSeqOutStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN19CBufPtrSeqOutStream7ReleaseEv,"axG",@progbits,_ZN19CBufPtrSeqOutStream7ReleaseEv,comdat
	.weak	_ZN19CBufPtrSeqOutStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN19CBufPtrSeqOutStream7ReleaseEv,@function
_ZN19CBufPtrSeqOutStream7ReleaseEv:     # @_ZN19CBufPtrSeqOutStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi95:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB26_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB26_2:
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZN19CBufPtrSeqOutStream7ReleaseEv, .Lfunc_end26-_ZN19CBufPtrSeqOutStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN19CBufPtrSeqOutStreamD0Ev,"axG",@progbits,_ZN19CBufPtrSeqOutStreamD0Ev,comdat
	.weak	_ZN19CBufPtrSeqOutStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN19CBufPtrSeqOutStreamD0Ev,@function
_ZN19CBufPtrSeqOutStreamD0Ev:           # @_ZN19CBufPtrSeqOutStreamD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end27:
	.size	_ZN19CBufPtrSeqOutStreamD0Ev, .Lfunc_end27-_ZN19CBufPtrSeqOutStreamD0Ev
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamSizeCount14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN29CSequentialOutStreamSizeCount14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN29CSequentialOutStreamSizeCount14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamSizeCount14QueryInterfaceERK4GUIDPPv,@function
_ZN29CSequentialOutStreamSizeCount14QueryInterfaceERK4GUIDPPv: # @_ZN29CSequentialOutStreamSizeCount14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB28_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB28_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB28_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB28_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB28_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB28_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB28_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB28_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB28_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB28_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB28_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB28_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB28_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB28_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB28_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB28_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB28_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end28:
	.size	_ZN29CSequentialOutStreamSizeCount14QueryInterfaceERK4GUIDPPv, .Lfunc_end28-_ZN29CSequentialOutStreamSizeCount14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamSizeCount6AddRefEv,"axG",@progbits,_ZN29CSequentialOutStreamSizeCount6AddRefEv,comdat
	.weak	_ZN29CSequentialOutStreamSizeCount6AddRefEv
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamSizeCount6AddRefEv,@function
_ZN29CSequentialOutStreamSizeCount6AddRefEv: # @_ZN29CSequentialOutStreamSizeCount6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end29:
	.size	_ZN29CSequentialOutStreamSizeCount6AddRefEv, .Lfunc_end29-_ZN29CSequentialOutStreamSizeCount6AddRefEv
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamSizeCount7ReleaseEv,"axG",@progbits,_ZN29CSequentialOutStreamSizeCount7ReleaseEv,comdat
	.weak	_ZN29CSequentialOutStreamSizeCount7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamSizeCount7ReleaseEv,@function
_ZN29CSequentialOutStreamSizeCount7ReleaseEv: # @_ZN29CSequentialOutStreamSizeCount7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB30_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB30_2:
	popq	%rcx
	retq
.Lfunc_end30:
	.size	_ZN29CSequentialOutStreamSizeCount7ReleaseEv, .Lfunc_end30-_ZN29CSequentialOutStreamSizeCount7ReleaseEv
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamSizeCountD2Ev,"axG",@progbits,_ZN29CSequentialOutStreamSizeCountD2Ev,comdat
	.weak	_ZN29CSequentialOutStreamSizeCountD2Ev
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamSizeCountD2Ev,@function
_ZN29CSequentialOutStreamSizeCountD2Ev: # @_ZN29CSequentialOutStreamSizeCountD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV29CSequentialOutStreamSizeCount+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB31_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB31_1:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	retq
.Lfunc_end31:
	.size	_ZN29CSequentialOutStreamSizeCountD2Ev, .Lfunc_end31-_ZN29CSequentialOutStreamSizeCountD2Ev
	.cfi_endproc

	.section	.text._ZN29CSequentialOutStreamSizeCountD0Ev,"axG",@progbits,_ZN29CSequentialOutStreamSizeCountD0Ev,comdat
	.weak	_ZN29CSequentialOutStreamSizeCountD0Ev
	.p2align	4, 0x90
	.type	_ZN29CSequentialOutStreamSizeCountD0Ev,@function
_ZN29CSequentialOutStreamSizeCountD0Ev: # @_ZN29CSequentialOutStreamSizeCountD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 32
.Lcfi101:
	.cfi_offset %rbx, -24
.Lcfi102:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV29CSequentialOutStreamSizeCount+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp3:
	callq	*16(%rax)
.Ltmp4:
.LBB32_2:                               # %_ZN29CSequentialOutStreamSizeCountD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB32_3:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN29CSequentialOutStreamSizeCountD0Ev, .Lfunc_end32-_ZN29CSequentialOutStreamSizeCountD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end32-.Ltmp4     #   Call between .Ltmp4 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN15CCachedInStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN15CCachedInStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN15CCachedInStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN15CCachedInStream14QueryInterfaceERK4GUIDPPv,@function
_ZN15CCachedInStream14QueryInterfaceERK4GUIDPPv: # @_ZN15CCachedInStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB33_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB33_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB33_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB33_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB33_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB33_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB33_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB33_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB33_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB33_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB33_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB33_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB33_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB33_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB33_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB33_48
.LBB33_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ISequentialInStream(%rip), %cl
	jne	.LBB33_32
# BB#17:
	movb	1(%rsi), %al
	cmpb	IID_ISequentialInStream+1(%rip), %al
	jne	.LBB33_32
# BB#18:
	movb	2(%rsi), %al
	cmpb	IID_ISequentialInStream+2(%rip), %al
	jne	.LBB33_32
# BB#19:
	movb	3(%rsi), %al
	cmpb	IID_ISequentialInStream+3(%rip), %al
	jne	.LBB33_32
# BB#20:
	movb	4(%rsi), %al
	cmpb	IID_ISequentialInStream+4(%rip), %al
	jne	.LBB33_32
# BB#21:
	movb	5(%rsi), %al
	cmpb	IID_ISequentialInStream+5(%rip), %al
	jne	.LBB33_32
# BB#22:
	movb	6(%rsi), %al
	cmpb	IID_ISequentialInStream+6(%rip), %al
	jne	.LBB33_32
# BB#23:
	movb	7(%rsi), %al
	cmpb	IID_ISequentialInStream+7(%rip), %al
	jne	.LBB33_32
# BB#24:
	movb	8(%rsi), %al
	cmpb	IID_ISequentialInStream+8(%rip), %al
	jne	.LBB33_32
# BB#25:
	movb	9(%rsi), %al
	cmpb	IID_ISequentialInStream+9(%rip), %al
	jne	.LBB33_32
# BB#26:
	movb	10(%rsi), %al
	cmpb	IID_ISequentialInStream+10(%rip), %al
	jne	.LBB33_32
# BB#27:
	movb	11(%rsi), %al
	cmpb	IID_ISequentialInStream+11(%rip), %al
	jne	.LBB33_32
# BB#28:
	movb	12(%rsi), %al
	cmpb	IID_ISequentialInStream+12(%rip), %al
	jne	.LBB33_32
# BB#29:
	movb	13(%rsi), %al
	cmpb	IID_ISequentialInStream+13(%rip), %al
	jne	.LBB33_32
# BB#30:
	movb	14(%rsi), %al
	cmpb	IID_ISequentialInStream+14(%rip), %al
	jne	.LBB33_32
# BB#31:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ISequentialInStream+15(%rip), %al
	je	.LBB33_48
.LBB33_32:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInStream(%rip), %cl
	jne	.LBB33_49
# BB#33:
	movb	1(%rsi), %cl
	cmpb	IID_IInStream+1(%rip), %cl
	jne	.LBB33_49
# BB#34:
	movb	2(%rsi), %cl
	cmpb	IID_IInStream+2(%rip), %cl
	jne	.LBB33_49
# BB#35:
	movb	3(%rsi), %cl
	cmpb	IID_IInStream+3(%rip), %cl
	jne	.LBB33_49
# BB#36:
	movb	4(%rsi), %cl
	cmpb	IID_IInStream+4(%rip), %cl
	jne	.LBB33_49
# BB#37:
	movb	5(%rsi), %cl
	cmpb	IID_IInStream+5(%rip), %cl
	jne	.LBB33_49
# BB#38:
	movb	6(%rsi), %cl
	cmpb	IID_IInStream+6(%rip), %cl
	jne	.LBB33_49
# BB#39:
	movb	7(%rsi), %cl
	cmpb	IID_IInStream+7(%rip), %cl
	jne	.LBB33_49
# BB#40:
	movb	8(%rsi), %cl
	cmpb	IID_IInStream+8(%rip), %cl
	jne	.LBB33_49
# BB#41:
	movb	9(%rsi), %cl
	cmpb	IID_IInStream+9(%rip), %cl
	jne	.LBB33_49
# BB#42:
	movb	10(%rsi), %cl
	cmpb	IID_IInStream+10(%rip), %cl
	jne	.LBB33_49
# BB#43:
	movb	11(%rsi), %cl
	cmpb	IID_IInStream+11(%rip), %cl
	jne	.LBB33_49
# BB#44:
	movb	12(%rsi), %cl
	cmpb	IID_IInStream+12(%rip), %cl
	jne	.LBB33_49
# BB#45:
	movb	13(%rsi), %cl
	cmpb	IID_IInStream+13(%rip), %cl
	jne	.LBB33_49
# BB#46:
	movb	14(%rsi), %cl
	cmpb	IID_IInStream+14(%rip), %cl
	jne	.LBB33_49
# BB#47:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_IInStream+15(%rip), %cl
	jne	.LBB33_49
.LBB33_48:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB33_49:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end33:
	.size	_ZN15CCachedInStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end33-_ZN15CCachedInStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN15CCachedInStream6AddRefEv,"axG",@progbits,_ZN15CCachedInStream6AddRefEv,comdat
	.weak	_ZN15CCachedInStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN15CCachedInStream6AddRefEv,@function
_ZN15CCachedInStream6AddRefEv:          # @_ZN15CCachedInStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end34:
	.size	_ZN15CCachedInStream6AddRefEv, .Lfunc_end34-_ZN15CCachedInStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN15CCachedInStream7ReleaseEv,"axG",@progbits,_ZN15CCachedInStream7ReleaseEv,comdat
	.weak	_ZN15CCachedInStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN15CCachedInStream7ReleaseEv,@function
_ZN15CCachedInStream7ReleaseEv:         # @_ZN15CCachedInStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB35_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB35_2:
	popq	%rcx
	retq
.Lfunc_end35:
	.size	_ZN15CCachedInStream7ReleaseEv, .Lfunc_end35-_ZN15CCachedInStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN15CCachedInStreamD2Ev,"axG",@progbits,_ZN15CCachedInStreamD2Ev,comdat
	.weak	_ZN15CCachedInStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN15CCachedInStreamD2Ev,@function
_ZN15CCachedInStreamD2Ev:               # @_ZN15CCachedInStreamD2Ev
	.cfi_startproc
# BB#0:                                 # %.noexc
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 16
.Lcfi106:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV15CCachedInStream+16, (%rbx)
	movq	16(%rbx), %rdi
	callq	MyFree
	movq	$0, 16(%rbx)
	movq	24(%rbx), %rdi
	callq	MidFree
	movq	$0, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end36:
	.size	_ZN15CCachedInStreamD2Ev, .Lfunc_end36-_ZN15CCachedInStreamD2Ev
	.cfi_endproc

	.section	.text._ZN15CCachedInStreamD0Ev,"axG",@progbits,_ZN15CCachedInStreamD0Ev,comdat
	.weak	_ZN15CCachedInStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN15CCachedInStreamD0Ev,@function
_ZN15CCachedInStreamD0Ev:               # @_ZN15CCachedInStreamD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 32
.Lcfi110:
	.cfi_offset %rbx, -24
.Lcfi111:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV15CCachedInStream+16, (%rbx)
	movq	16(%rbx), %rdi
.Ltmp6:
	callq	MyFree
.Ltmp7:
# BB#1:                                 # %.noexc
	movq	$0, 16(%rbx)
	movq	24(%rbx), %rdi
.Ltmp8:
	callq	MidFree
.Ltmp9:
# BB#2:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB37_3:
.Ltmp10:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end37:
	.size	_ZN15CCachedInStreamD0Ev, .Lfunc_end37-_ZN15CCachedInStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp9-.Ltmp6           #   Call between .Ltmp6 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin2   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Lfunc_end37-.Ltmp9     #   Call between .Ltmp9 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end38:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end38-_ZN8IUnknownD2Ev
	.cfi_endproc

	.type	_ZTV12CBufInStream,@object # @_ZTV12CBufInStream
	.section	.rodata,"a",@progbits
	.globl	_ZTV12CBufInStream
	.p2align	3
_ZTV12CBufInStream:
	.quad	0
	.quad	_ZTI12CBufInStream
	.quad	_ZN12CBufInStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN12CBufInStream6AddRefEv
	.quad	_ZN12CBufInStream7ReleaseEv
	.quad	_ZN12CBufInStreamD2Ev
	.quad	_ZN12CBufInStreamD0Ev
	.quad	_ZN12CBufInStream4ReadEPvjPj
	.quad	_ZN12CBufInStream4SeekExjPy
	.size	_ZTV12CBufInStream, 72

	.type	_ZTS12CBufInStream,@object # @_ZTS12CBufInStream
	.globl	_ZTS12CBufInStream
_ZTS12CBufInStream:
	.asciz	"12CBufInStream"
	.size	_ZTS12CBufInStream, 15

	.type	_ZTS9IInStream,@object  # @_ZTS9IInStream
	.section	.rodata._ZTS9IInStream,"aG",@progbits,_ZTS9IInStream,comdat
	.weak	_ZTS9IInStream
_ZTS9IInStream:
	.asciz	"9IInStream"
	.size	_ZTS9IInStream, 11

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTI9IInStream,@object  # @_ZTI9IInStream
	.section	.rodata._ZTI9IInStream,"aG",@progbits,_ZTI9IInStream,comdat
	.weak	_ZTI9IInStream
	.p2align	4
_ZTI9IInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9IInStream
	.quad	_ZTI19ISequentialInStream
	.size	_ZTI9IInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI12CBufInStream,@object # @_ZTI12CBufInStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI12CBufInStream
	.p2align	4
_ZTI12CBufInStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS12CBufInStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI9IInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI12CBufInStream, 56

	.type	_ZTV19CDynBufSeqOutStream,@object # @_ZTV19CDynBufSeqOutStream
	.globl	_ZTV19CDynBufSeqOutStream
	.p2align	3
_ZTV19CDynBufSeqOutStream:
	.quad	0
	.quad	_ZTI19CDynBufSeqOutStream
	.quad	_ZN19CDynBufSeqOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN19CDynBufSeqOutStream6AddRefEv
	.quad	_ZN19CDynBufSeqOutStream7ReleaseEv
	.quad	_ZN19CDynBufSeqOutStreamD2Ev
	.quad	_ZN19CDynBufSeqOutStreamD0Ev
	.quad	_ZN19CDynBufSeqOutStream5WriteEPKvjPj
	.size	_ZTV19CDynBufSeqOutStream, 64

	.type	_ZTS19CDynBufSeqOutStream,@object # @_ZTS19CDynBufSeqOutStream
	.globl	_ZTS19CDynBufSeqOutStream
	.p2align	4
_ZTS19CDynBufSeqOutStream:
	.asciz	"19CDynBufSeqOutStream"
	.size	_ZTS19CDynBufSeqOutStream, 22

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTI19CDynBufSeqOutStream,@object # @_ZTI19CDynBufSeqOutStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI19CDynBufSeqOutStream
	.p2align	4
_ZTI19CDynBufSeqOutStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS19CDynBufSeqOutStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI19CDynBufSeqOutStream, 56

	.type	_ZTV19CBufPtrSeqOutStream,@object # @_ZTV19CBufPtrSeqOutStream
	.globl	_ZTV19CBufPtrSeqOutStream
	.p2align	3
_ZTV19CBufPtrSeqOutStream:
	.quad	0
	.quad	_ZTI19CBufPtrSeqOutStream
	.quad	_ZN19CBufPtrSeqOutStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN19CBufPtrSeqOutStream6AddRefEv
	.quad	_ZN19CBufPtrSeqOutStream7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN19CBufPtrSeqOutStreamD0Ev
	.quad	_ZN19CBufPtrSeqOutStream5WriteEPKvjPj
	.size	_ZTV19CBufPtrSeqOutStream, 64

	.type	_ZTS19CBufPtrSeqOutStream,@object # @_ZTS19CBufPtrSeqOutStream
	.globl	_ZTS19CBufPtrSeqOutStream
	.p2align	4
_ZTS19CBufPtrSeqOutStream:
	.asciz	"19CBufPtrSeqOutStream"
	.size	_ZTS19CBufPtrSeqOutStream, 22

	.type	_ZTI19CBufPtrSeqOutStream,@object # @_ZTI19CBufPtrSeqOutStream
	.globl	_ZTI19CBufPtrSeqOutStream
	.p2align	4
_ZTI19CBufPtrSeqOutStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS19CBufPtrSeqOutStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI19CBufPtrSeqOutStream, 56

	.type	_ZTV29CSequentialOutStreamSizeCount,@object # @_ZTV29CSequentialOutStreamSizeCount
	.globl	_ZTV29CSequentialOutStreamSizeCount
	.p2align	3
_ZTV29CSequentialOutStreamSizeCount:
	.quad	0
	.quad	_ZTI29CSequentialOutStreamSizeCount
	.quad	_ZN29CSequentialOutStreamSizeCount14QueryInterfaceERK4GUIDPPv
	.quad	_ZN29CSequentialOutStreamSizeCount6AddRefEv
	.quad	_ZN29CSequentialOutStreamSizeCount7ReleaseEv
	.quad	_ZN29CSequentialOutStreamSizeCountD2Ev
	.quad	_ZN29CSequentialOutStreamSizeCountD0Ev
	.quad	_ZN29CSequentialOutStreamSizeCount5WriteEPKvjPj
	.size	_ZTV29CSequentialOutStreamSizeCount, 64

	.type	_ZTS29CSequentialOutStreamSizeCount,@object # @_ZTS29CSequentialOutStreamSizeCount
	.globl	_ZTS29CSequentialOutStreamSizeCount
	.p2align	4
_ZTS29CSequentialOutStreamSizeCount:
	.asciz	"29CSequentialOutStreamSizeCount"
	.size	_ZTS29CSequentialOutStreamSizeCount, 32

	.type	_ZTI29CSequentialOutStreamSizeCount,@object # @_ZTI29CSequentialOutStreamSizeCount
	.globl	_ZTI29CSequentialOutStreamSizeCount
	.p2align	4
_ZTI29CSequentialOutStreamSizeCount:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS29CSequentialOutStreamSizeCount
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI20ISequentialOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI29CSequentialOutStreamSizeCount, 56

	.type	_ZTV15CCachedInStream,@object # @_ZTV15CCachedInStream
	.globl	_ZTV15CCachedInStream
	.p2align	3
_ZTV15CCachedInStream:
	.quad	0
	.quad	_ZTI15CCachedInStream
	.quad	_ZN15CCachedInStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN15CCachedInStream6AddRefEv
	.quad	_ZN15CCachedInStream7ReleaseEv
	.quad	_ZN15CCachedInStreamD2Ev
	.quad	_ZN15CCachedInStreamD0Ev
	.quad	_ZN15CCachedInStream4ReadEPvjPj
	.quad	_ZN15CCachedInStream4SeekExjPy
	.quad	__cxa_pure_virtual
	.size	_ZTV15CCachedInStream, 80

	.type	_ZTS15CCachedInStream,@object # @_ZTS15CCachedInStream
	.globl	_ZTS15CCachedInStream
	.p2align	4
_ZTS15CCachedInStream:
	.asciz	"15CCachedInStream"
	.size	_ZTS15CCachedInStream, 18

	.type	_ZTI15CCachedInStream,@object # @_ZTI15CCachedInStream
	.globl	_ZTI15CCachedInStream
	.p2align	4
_ZTI15CCachedInStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS15CCachedInStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI9IInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI15CCachedInStream, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
