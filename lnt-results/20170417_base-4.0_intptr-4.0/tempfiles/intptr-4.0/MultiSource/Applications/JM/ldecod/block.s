	.text
	.file	"block.bc"
	.globl	intrapred
	.p2align	4, 0x90
	.type	intrapred,@function
intrapred:                              # @intrapred
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi6:
	.cfi_def_cfa_offset 336
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movq	dec_picture(%rip), %rax
	movq	316920(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	4(%rdi), %r13d
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	5544(%rdi), %rax
	movslq	%r8d, %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leal	-1(%r14), %ebp
	movslq	%r15d, %rbx
	leaq	128(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r13d, %edi
	movl	%ebp, %esi
	movl	%r15d, %edx
	callq	*getNeighbour(%rip)
	leaq	1(%rbx), %rdx
	leaq	152(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r13d, %edi
	movl	%ebp, %esi
	movq	%rdx, 32(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	leaq	2(%rbx), %rdx
	leaq	176(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r13d, %edi
	movl	%ebp, %esi
	movq	%rdx, 48(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	leaq	3(%rbx), %rdx
	movq	%r14, %rbx
	leaq	200(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r13d, %edi
	movl	%ebp, %esi
	movq	%rdx, 24(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*getNeighbour(%rip)
	leal	-1(%r15), %r14d
	leaq	104(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r13d, %edi
	movl	%ebx, %esi
	movl	%r14d, %edx
	callq	*getNeighbour(%rip)
	leal	4(%rbx), %esi
	leaq	232(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%r13d, %edi
	movl	%r14d, %edx
	callq	*getNeighbour(%rip)
	leaq	256(%rsp), %r12
	xorl	%ecx, %ecx
	movl	%r13d, %edi
	movl	%ebp, %esi
	movl	%r14d, %edx
	movq	%r12, %r8
	callq	*getNeighbour(%rip)
	cmpl	$0, 232(%rsp)
	je	.LBB0_2
# BB#1:
	movl	%r15d, %eax
	orl	$8, %eax
	cmpl	$4, %ebx
	setne	%cl
	cmpl	$12, %eax
	setne	%al
	orb	%cl, %al
	movq	64(%rsp), %r10          # 8-byte Reload
	movq	%r15, %r11
	movq	56(%rsp), %r9           # 8-byte Reload
	jmp	.LBB0_3
.LBB0_2:
	movq	64(%rsp), %r10          # 8-byte Reload
	movq	%r15, %r11
	movq	56(%rsp), %r9           # 8-byte Reload
	xorl	%eax, %eax
.LBB0_3:
	movzbl	%al, %edx
	movl	%edx, 232(%rsp)
	movq	active_pps(%rip), %rax
	cmpl	$0, 1148(%rax)
	je	.LBB0_18
# BB#4:                                 # %.preheader635
	xorl	%edi, %edi
	cmpl	$0, 128(%rsp)
	movl	$0, %ecx
	je	.LBB0_6
# BB#5:
	movq	16(%r10), %rax
	movslq	132(%rsp), %rcx
	movl	(%rax,%rcx,4), %ecx
.LBB0_6:
	cmpl	$0, 152(%rsp)
	je	.LBB0_8
# BB#7:
	movq	16(%r10), %rax
	movslq	156(%rsp), %rsi
	movl	(%rax,%rsi,4), %edi
.LBB0_8:
	xorl	%r8d, %r8d
	cmpl	$0, 176(%rsp)
	movl	$0, %ebp
	je	.LBB0_10
# BB#9:
	movq	16(%r10), %rsi
	movslq	180(%rsp), %rbp
	movl	(%rsi,%rbp,4), %ebp
.LBB0_10:
	andl	$1, %ecx
	cmpl	$0, 200(%rsp)
	je	.LBB0_12
# BB#11:
	movq	16(%r10), %rax
	movslq	204(%rsp), %rsi
	movl	(%rax,%rsi,4), %r8d
.LBB0_12:
	andl	%ecx, %edi
	xorl	%esi, %esi
	cmpl	$0, 104(%rsp)
	movl	$0, %r13d
	je	.LBB0_14
# BB#13:
	movq	16(%r10), %rcx
	movq	%rbx, %rax
	movslq	108(%rsp), %rbx
	movl	(%rcx,%rbx,4), %r13d
	movq	%rax, %rbx
.LBB0_14:
	andl	%edi, %ebp
	testl	%edx, %edx
	je	.LBB0_16
# BB#15:
	movq	16(%r10), %rdx
	movslq	236(%rsp), %rsi
	movl	(%rdx,%rsi,4), %esi
.LBB0_16:
	andl	%ebp, %r8d
	cmpl	$0, 256(%rsp)
	je	.LBB0_20
# BB#17:
	movslq	260(%rsp), %r12
	shlq	$2, %r12
	addq	16(%r10), %r12
	jmp	.LBB0_19
.LBB0_18:
	movl	128(%rsp), %r8d
	movl	104(%rsp), %r13d
	movl	%edx, %esi
.LBB0_19:                               # %.sink.split
	movl	(%r12), %r12d
	jmp	.LBB0_21
.LBB0_20:
	xorl	%r12d, %r12d
.LBB0_21:
	movq	%rbx, %r14
	testl	%r13d, %r13d
	je	.LBB0_23
# BB#22:
	movslq	124(%rsp), %rdi
	movq	(%r9,%rdi,8), %rdi
	movslq	120(%rsp), %rbp
	movzwl	(%rdi,%rbp,2), %ebx
	movzwl	2(%rdi,%rbp,2), %eax
	movzwl	4(%rdi,%rbp,2), %ecx
	movw	%cx, 12(%rsp)           # 2-byte Spill
	movzwl	6(%rdi,%rbp,2), %r15d
	jmp	.LBB0_24
.LBB0_23:
	movzwl	5892(%r10), %r15d
	movw	%r15w, 12(%rsp)         # 2-byte Spill
	movl	%r15d, %eax
	movl	%r15d, %ebx
.LBB0_24:
	testl	%esi, %esi
	movl	%r15d, %ebp
	movl	%r15d, %edx
	movl	%r15d, %ecx
	movl	%r15d, %esi
	je	.LBB0_26
# BB#25:
	movslq	252(%rsp), %rsi
	movq	(%r9,%rsi,8), %rsi
	movslq	248(%rsp), %rdi
	movzwl	(%rsi,%rdi,2), %ebp
	movzwl	2(%rsi,%rdi,2), %edx
	movzwl	4(%rsi,%rdi,2), %ecx
	movzwl	6(%rsi,%rdi,2), %esi
.LBB0_26:
	testl	%r8d, %r8d
	movw	%cx, 86(%rsp)           # 2-byte Spill
	movw	%dx, 84(%rsp)           # 2-byte Spill
	movw	%bp, 82(%rsp)           # 2-byte Spill
	movw	%si, 102(%rsp)          # 2-byte Spill
	je	.LBB0_28
# BB#27:
	movslq	148(%rsp), %rsi
	movq	(%r9,%rsi,8), %rsi
	movslq	144(%rsp), %rdi
	movzwl	(%rsi,%rdi,2), %ecx
	movw	%cx, 14(%rsp)           # 2-byte Spill
	movslq	172(%rsp), %rsi
	movq	(%r9,%rsi,8), %rsi
	movslq	168(%rsp), %rdi
	movzwl	(%rsi,%rdi,2), %ecx
	movslq	196(%rsp), %rsi
	movq	(%r9,%rsi,8), %rsi
	movslq	192(%rsp), %rdi
	movzwl	(%rsi,%rdi,2), %edx
	movslq	220(%rsp), %rsi
	movq	(%r9,%rsi,8), %rsi
	movslq	216(%rsp), %rdi
	movzwl	(%rsi,%rdi,2), %ebp
	testl	%r12d, %r12d
	jne	.LBB0_29
	jmp	.LBB0_30
.LBB0_28:
	movzwl	5892(%r10), %esi
	movl	%esi, %ecx
	movl	%esi, %edx
	movw	%si, 14(%rsp)           # 2-byte Spill
	movl	%esi, %ebp
	testl	%r12d, %r12d
	je	.LBB0_30
.LBB0_29:
	movslq	276(%rsp), %rsi
	movq	(%r9,%rsi,8), %rsi
	movslq	272(%rsp), %rdi
	movzwl	(%rsi,%rdi,2), %edi
	jmp	.LBB0_31
.LBB0_30:
	movzwl	5892(%r10), %edi
.LBB0_31:
	movq	88(%rsp), %rsi          # 8-byte Reload
	cmpb	$8, %sil
	ja	.LBB0_41
# BB#32:
	movw	%di, 46(%rsp)           # 2-byte Spill
	movw	%bp, 22(%rsp)           # 2-byte Spill
	movw	%cx, 18(%rsp)           # 2-byte Spill
	movw	%bx, 16(%rsp)           # 2-byte Spill
	movw	%dx, 20(%rsp)           # 2-byte Spill
	movw	%ax, 88(%rsp)           # 2-byte Spill
	leal	1(%r11), %eax
	leal	3(%r11), %ecx
	movq	%r14, %rbx
	leal	2(%rbx), %edx
	addl	$2, %r11d
	leal	1(%rbx), %ebp
	leal	3(%rbx), %edi
	jmpq	*.LJTI0_0(,%rsi,8)
.LBB0_33:
	testl	%r13d, %r13d
	jne	.LBB0_35
# BB#34:
	movl	4(%r10), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	64(%rsp), %r10          # 8-byte Reload
.LBB0_35:                               # %.preheader630
	movslq	124(%rsp), %rax
	movq	(%r9,%rax,8), %rax
	movslq	%ebx, %rcx
	movslq	120(%rsp), %rdx
	movzwl	(%rax,%rdx,2), %esi
	movq	72(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	104(%r10,%rdi), %rdi
	movw	%si, (%rdi,%rcx,2)
	movzwl	2(%rax,%rdx,2), %esi
	movw	%si, 2(%rdi,%rcx,2)
	movzwl	4(%rax,%rdx,2), %esi
	movw	%si, 4(%rdi,%rcx,2)
	movzwl	6(%rax,%rdx,2), %esi
	movw	%si, 6(%rdi,%rcx,2)
	movzwl	(%rax,%rdx,2), %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	104(%r10,%rdi), %rdi
	movw	%si, (%rdi,%rcx,2)
	movzwl	2(%rax,%rdx,2), %esi
	movw	%si, 2(%rdi,%rcx,2)
	movzwl	4(%rax,%rdx,2), %esi
	movw	%si, 4(%rdi,%rcx,2)
	movzwl	6(%rax,%rdx,2), %esi
	movw	%si, 6(%rdi,%rcx,2)
	movzwl	(%rax,%rdx,2), %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	104(%r10,%rdi), %rdi
	movw	%si, (%rdi,%rcx,2)
	movzwl	2(%rax,%rdx,2), %esi
	movw	%si, 2(%rdi,%rcx,2)
	movzwl	4(%rax,%rdx,2), %esi
	movw	%si, 4(%rdi,%rcx,2)
	movzwl	6(%rax,%rdx,2), %esi
	movw	%si, 6(%rdi,%rcx,2)
	movzwl	(%rax,%rdx,2), %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	104(%r10,%rdi), %rdi
	movw	%si, (%rdi,%rcx,2)
	movzwl	2(%rax,%rdx,2), %esi
	movw	%si, 2(%rdi,%rcx,2)
	movzwl	4(%rax,%rdx,2), %esi
	movw	%si, 4(%rdi,%rcx,2)
	movzwl	6(%rax,%rdx,2), %eax
	movw	%ax, 6(%rdi,%rcx,2)
	jmp	.LBB0_75
.LBB0_36:
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movl	%edx, 48(%rsp)          # 4-byte Spill
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%edi, 24(%rsp)          # 4-byte Spill
	testl	%r13d, %r13d
	je	.LBB0_39
# BB#37:
	testl	%r8d, %r8d
	je	.LBB0_39
# BB#38:
	testl	%r12d, %r12d
	jne	.LBB0_40
.LBB0_39:
	movl	4(%r10), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%r11, %r14
	callq	printf
	movq	%r14, %r11
	movq	64(%rsp), %r10          # 8-byte Reload
.LBB0_40:
	movzwl	22(%rsp), %eax          # 2-byte Folded Reload
	movzwl	20(%rsp), %ecx          # 2-byte Folded Reload
	movzwl	18(%rsp), %edx          # 2-byte Folded Reload
	leal	(%rdx,%rcx,2), %esi
	leal	2(%rax,%rsi), %eax
	shrl	$2, %eax
	movslq	56(%rsp), %rsi          # 4-byte Folded Reload
	movslq	%ebx, %r8
	shlq	$5, %rsi
	leaq	104(%r10,%rsi), %rdi
	movw	%ax, (%rdi,%r8,2)
	movzwl	14(%rsp), %eax          # 2-byte Folded Reload
	leal	(%rax,%rdx,2), %esi
	leal	2(%rcx,%rsi), %ecx
	shrl	$2, %ecx
	movq	%r10, %r12
	movslq	%ebp, %r10
	movw	%cx, (%rdi,%r10,2)
	movslq	%r11d, %rsi
	shlq	$5, %rsi
	leaq	104(%r12,%rsi), %rsi
	movw	%cx, (%rsi,%r8,2)
	movzwl	46(%rsp), %r9d          # 2-byte Folded Reload
	leal	(%rdx,%rax,2), %ecx
	leal	2(%r9,%rcx), %ecx
	shrl	$2, %ecx
	movslq	48(%rsp), %r14          # 4-byte Folded Reload
	movw	%cx, (%rdi,%r14,2)
	movw	%cx, (%rsi,%r10,2)
	movslq	32(%rsp), %rbx          # 4-byte Folded Reload
	shlq	$5, %rbx
	leaq	104(%r12,%rbx), %rbx
	movw	%cx, (%rbx,%r8,2)
	movzwl	16(%rsp), %r11d         # 2-byte Folded Reload
	addl	%r11d, %eax
	leal	2(%rax,%r9,2), %ecx
	shrl	$2, %ecx
	movslq	24(%rsp), %rax          # 4-byte Folded Reload
	movw	%cx, (%rdi,%rax,2)
	movq	72(%rsp), %rdx          # 8-byte Reload
	shlq	$5, %rdx
	leaq	104(%r12,%rdx), %rdi
	movw	%cx, (%rsi,%r14,2)
	movw	%cx, (%rbx,%r10,2)
	movw	%cx, (%rdi,%r8,2)
	movzwl	88(%rsp), %ecx          # 2-byte Folded Reload
	leal	(%rcx,%r11,2), %edx
	leal	2(%r9,%rdx), %edx
	shrl	$2, %edx
	movw	%dx, (%rsi,%rax,2)
	movw	%dx, (%rbx,%r14,2)
	movw	%dx, (%rdi,%r10,2)
	movzwl	12(%rsp), %edx          # 2-byte Folded Reload
	leal	(%rdx,%rcx,2), %esi
	leal	2(%r11,%rsi), %esi
	shrl	$2, %esi
	movw	%si, (%rbx,%rax,2)
	movw	%si, (%rdi,%r14,2)
	movzwl	%r15w, %esi
	leal	(%rsi,%rdx,2), %edx
	leal	2(%rcx,%rdx), %ecx
	shrl	$2, %ecx
	movw	%cx, (%rdi,%rax,2)
	jmp	.LBB0_75
.LBB0_41:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	$1, %eax
	jmp	.LBB0_76
.LBB0_42:
	testl	%r8d, %r8d
	jne	.LBB0_44
# BB#43:
	movl	4(%r10), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	64(%rsp), %r10          # 8-byte Reload
.LBB0_44:                               # %.preheader632.preheader
	movslq	%ebx, %rax
	movslq	148(%rsp), %rcx
	movq	(%r9,%rcx,8), %rcx
	movslq	144(%rsp), %rdx
	movzwl	(%rcx,%rdx,2), %esi
	movq	72(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	104(%r10,%rdi), %rdi
	movw	%si, (%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %esi
	movw	%si, 2(%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %esi
	movw	%si, 4(%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %ecx
	movw	%cx, 6(%rdi,%rax,2)
	movslq	172(%rsp), %rcx
	movq	(%r9,%rcx,8), %rcx
	movslq	168(%rsp), %rdx
	movzwl	(%rcx,%rdx,2), %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	104(%r10,%rdi), %rdi
	movw	%si, (%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %esi
	movw	%si, 2(%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %esi
	movw	%si, 4(%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %ecx
	movw	%cx, 6(%rdi,%rax,2)
	movslq	196(%rsp), %rcx
	movq	(%r9,%rcx,8), %rcx
	movslq	192(%rsp), %rdx
	movzwl	(%rcx,%rdx,2), %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	104(%r10,%rdi), %rdi
	movw	%si, (%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %esi
	movw	%si, 2(%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %esi
	movw	%si, 4(%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %ecx
	movw	%cx, 6(%rdi,%rax,2)
	movslq	220(%rsp), %rcx
	movq	(%r9,%rcx,8), %rcx
	movslq	216(%rsp), %rdx
	movzwl	(%rcx,%rdx,2), %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	leaq	104(%r10,%rdi), %rdi
	movw	%si, (%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %esi
	movw	%si, 2(%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %esi
	movw	%si, 4(%rdi,%rax,2)
	movzwl	(%rcx,%rdx,2), %ecx
	movw	%cx, 6(%rdi,%rax,2)
	jmp	.LBB0_75
.LBB0_45:
	testl	%r13d, %r13d
	je	.LBB0_67
# BB#46:
	testl	%r8d, %r8d
	je	.LBB0_67
# BB#47:
	movzwl	16(%rsp), %r8d          # 2-byte Folded Reload
	movzwl	88(%rsp), %ecx          # 2-byte Folded Reload
	movzwl	12(%rsp), %edx          # 2-byte Folded Reload
	movzwl	%r15w, %esi
	movzwl	14(%rsp), %edi          # 2-byte Folded Reload
	movzwl	18(%rsp), %ebp          # 2-byte Folded Reload
	movq	%rbx, %r9
	movzwl	20(%rsp), %ebx          # 2-byte Folded Reload
	movzwl	22(%rsp), %eax          # 2-byte Folded Reload
	addl	%edx, %esi
	addl	%ecx, %esi
	addl	%r8d, %esi
	addl	%edi, %esi
	addl	%ebp, %esi
	addl	%ebx, %esi
	movq	%r9, %rbx
	leal	(%rsi,%rax), %ecx
	leal	4(%rax,%rsi), %eax
	sarl	$31, %eax
	shrl	$29, %eax
	leal	4(%rax,%rcx), %eax
	sarl	$3, %eax
	jmp	.LBB0_74
.LBB0_48:
	movl	%edx, 48(%rsp)          # 4-byte Spill
	movl	%eax, %r12d
	movl	%edi, 24(%rsp)          # 4-byte Spill
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	movzwl	12(%rsp), %r14d         # 2-byte Folded Reload
	jne	.LBB0_50
# BB#49:
	movl	4(%r10), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movq	64(%rsp), %r10          # 8-byte Reload
.LBB0_50:
	movzwl	16(%rsp), %ecx          # 2-byte Folded Reload
	movzwl	%r14w, %eax
	movzwl	88(%rsp), %edx          # 2-byte Folded Reload
	leal	2(%rax,%rcx), %ecx
	leal	(%rcx,%rdx,2), %ecx
	shrl	$2, %ecx
	movslq	%ebx, %r9
	movq	72(%rsp), %rdi          # 8-byte Reload
	shlq	$5, %rdi
	movq	%r10, %rsi
	leaq	104(%rsi,%rdi), %r10
	movw	%cx, (%r10,%r9,2)
	movzwl	%r15w, %r8d
	leal	2(%r8,%rdx), %ecx
	leal	(%rcx,%rax,2), %ecx
	shrl	$2, %ecx
	movslq	%r12d, %rdx
	shlq	$5, %rdx
	leaq	104(%rsi,%rdx), %r11
	movw	%cx, (%r11,%r9,2)
	movslq	%ebp, %rdi
	movw	%cx, (%r10,%rdi,2)
	movzwl	82(%rsp), %ebp          # 2-byte Folded Reload
	leal	2(%rax,%r8,2), %eax
	addl	%ebp, %eax
	shrl	$2, %eax
	movslq	32(%rsp), %rcx          # 4-byte Folded Reload
	shlq	$5, %rcx
	leaq	104(%rsi,%rcx), %r14
	movw	%ax, (%r14,%r9,2)
	movw	%ax, (%r11,%rdi,2)
	movslq	48(%rsp), %rbx          # 4-byte Folded Reload
	movw	%ax, (%r10,%rbx,2)
	movslq	56(%rsp), %rax          # 4-byte Folded Reload
	shlq	$5, %rax
	leaq	104(%rsi,%rax), %rax
	movzwl	84(%rsp), %edx          # 2-byte Folded Reload
	leal	2(%r8,%rdx), %ecx
	leal	(%rcx,%rbp,2), %esi
	shrl	$2, %esi
	movw	%si, (%rax,%r9,2)
	movw	%si, (%r14,%rdi,2)
	movw	%si, (%r11,%rbx,2)
	movslq	24(%rsp), %rcx          # 4-byte Folded Reload
	movw	%si, (%r10,%rcx,2)
	movzwl	86(%rsp), %esi          # 2-byte Folded Reload
	addl	%esi, %ebp
	leal	2(%rbp,%rdx,2), %ebp
	shrl	$2, %ebp
	movw	%bp, (%rax,%rdi,2)
	movw	%bp, (%r14,%rbx,2)
	movw	%bp, (%r11,%rcx,2)
	movzwl	102(%rsp), %edi         # 2-byte Folded Reload
	addl	%edi, %edx
	leal	2(%rdx,%rsi,2), %edx
	shrl	$2, %edx
	movw	%dx, (%rax,%rbx,2)
	movw	%dx, (%r14,%rcx,2)
	leal	(%rdi,%rdi,2), %edx
	leal	2(%rsi,%rdx), %edx
	shrl	$2, %edx
	movw	%dx, (%rax,%rcx,2)
	jmp	.LBB0_75
.LBB0_51:
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%edi, 24(%rsp)          # 4-byte Spill
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	testl	%r13d, %r13d
	je	.LBB0_54
# BB#52:
	testl	%r8d, %r8d
	je	.LBB0_54
# BB#53:
	testl	%r12d, %r12d
	jne	.LBB0_55
.LBB0_54:
	movl	4(%r10), %esi
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%ebp, %r12d
	movq	%r11, %r14
	movl	%edx, %ebp
	callq	printf
	movl	%ebp, %edx
	movq	%r14, %r11
	movl	%r12d, %ebp
	movq	64(%rsp), %r10          # 8-byte Reload
.LBB0_55:
	movzwl	46(%rsp), %eax          # 2-byte Folded Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movzwl	16(%rsp), %r13d         # 2-byte Folded Reload
	leal	1(%r13,%rax), %eax
	shrl	%eax
	movslq	%r11d, %rcx
	movslq	%ebp, %r14
	shlq	$5, %rcx
	leaq	104(%r10,%rcx), %r8
	movw	%ax, (%r8,%r14,2)
	movslq	%ebx, %r9
	movq	72(%rsp), %rcx          # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%r10,%rcx), %rbx
	movw	%ax, (%rbx,%r9,2)
	movzwl	88(%rsp), %edi          # 2-byte Folded Reload
	leal	1(%rdi,%r13), %ecx
	shrl	%ecx
	movslq	%edx, %r12
	movw	%cx, (%r8,%r12,2)
	movw	%cx, (%rbx,%r14,2)
	movzwl	12(%rsp), %ecx          # 2-byte Folded Reload
	leal	1(%rcx,%rdi), %esi
	shrl	%esi
	movslq	24(%rsp), %rdx          # 4-byte Folded Reload
	movw	%si, (%r8,%rdx,2)
	movw	%si, (%rbx,%r12,2)
	movzwl	%r15w, %r11d
	leal	1(%r11,%rcx), %esi
	shrl	%esi
	movw	%si, (%rbx,%rdx,2)
	movslq	56(%rsp), %rsi          # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%r10,%rsi), %rbp
	movslq	32(%rsp), %rsi          # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%r10,%rsi), %rsi
	movzwl	14(%rsp), %r10d         # 2-byte Folded Reload
	movl	%r13d, %eax
	addl	%r10d, %eax
	movq	64(%rsp), %rbx          # 8-byte Reload
	leal	2(%rax,%rbx,2), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%r14,2)
	movw	%ax, (%rsi,%r9,2)
	leal	(%rdi,%r13,2), %eax
	leal	2(%rbx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%r12,2)
	movw	%ax, (%rsi,%r14,2)
	leal	(%rcx,%rdi,2), %eax
	leal	2(%r13,%rax), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%rdx,2)
	movw	%ax, (%rsi,%r12,2)
	leal	(%r11,%rcx,2), %eax
	leal	2(%rdi,%rax), %eax
	shrl	$2, %eax
	movw	%ax, (%rsi,%rdx,2)
	movzwl	18(%rsp), %eax          # 2-byte Folded Reload
	leal	(%rax,%r10,2), %ecx
	leal	2(%rbx,%rcx), %ecx
	shrl	$2, %ecx
	movw	%cx, (%r8,%r9,2)
	leal	(%r10,%rax,2), %eax
	movzwl	20(%rsp), %ecx          # 2-byte Folded Reload
	leal	2(%rcx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, (%rbp,%r9,2)
	jmp	.LBB0_75
.LBB0_56:
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	movl	%edx, 48(%rsp)          # 4-byte Spill
	movl	%eax, %ebp
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %r15
	testl	%r13d, %r13d
	je	.LBB0_59
# BB#57:
	testl	%r8d, %r8d
	je	.LBB0_59
# BB#58:
	testl	%r12d, %r12d
	jne	.LBB0_60
.LBB0_59:
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	%edi, %ebx
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movl	%ebx, %edi
.LBB0_60:
	movzwl	46(%rsp), %r11d         # 2-byte Folded Reload
	movzwl	14(%rsp), %esi          # 2-byte Folded Reload
	leal	1(%rsi,%r11), %eax
	shrl	%eax
	movslq	%ebp, %rdx
	movslq	48(%rsp), %r10          # 4-byte Folded Reload
	shlq	$5, %rdx
	movq	64(%rsp), %r14          # 8-byte Reload
	leaq	104(%r14,%rdx), %rbx
	movw	%ax, (%rbx,%r10,2)
	movslq	%r15d, %r8
	movq	72(%rsp), %rcx          # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%r14,%rcx), %rdx
	movw	%ax, (%rdx,%r8,2)
	movzwl	16(%rsp), %eax          # 2-byte Folded Reload
	movl	%esi, %ecx
	addl	%eax, %ecx
	leal	2(%rcx,%r11,2), %ecx
	shrl	$2, %ecx
	movslq	%edi, %rbp
	movw	%cx, (%rbx,%rbp,2)
	movslq	24(%rsp), %r9           # 4-byte Folded Reload
	movw	%cx, (%rdx,%r9,2)
	movzwl	88(%rsp), %ecx          # 2-byte Folded Reload
	leal	(%rcx,%rax,2), %edi
	leal	2(%r11,%rdi), %edi
	shrl	$2, %edi
	movw	%di, (%rdx,%r10,2)
	movzwl	12(%rsp), %edi          # 2-byte Folded Reload
	leal	(%rdi,%rcx,2), %ecx
	leal	2(%rax,%rcx), %eax
	shrl	$2, %eax
	movw	%ax, (%rdx,%rbp,2)
	movzwl	18(%rsp), %eax          # 2-byte Folded Reload
	leal	1(%rsi,%rax), %ecx
	shrl	%ecx
	movslq	32(%rsp), %rdx          # 4-byte Folded Reload
	shlq	$5, %rdx
	leaq	104(%r14,%rdx), %rdx
	movw	%cx, (%rdx,%r10,2)
	movw	%cx, (%rbx,%r8,2)
	leal	(%rax,%rsi,2), %ecx
	leal	2(%r11,%rcx), %ecx
	shrl	$2, %ecx
	movw	%cx, (%rdx,%rbp,2)
	movw	%cx, (%rbx,%r9,2)
	movzwl	20(%rsp), %ecx          # 2-byte Folded Reload
	movslq	56(%rsp), %rdi          # 4-byte Folded Reload
	shlq	$5, %rdi
	leaq	104(%r14,%rdi), %rdi
	leal	1(%rax,%rcx), %ebx
	shrl	%ebx
	movw	%bx, (%rdi,%r10,2)
	movw	%bx, (%rdx,%r8,2)
	leal	(%rsi,%rax,2), %esi
	leal	2(%rcx,%rsi), %esi
	shrl	$2, %esi
	movw	%si, (%rdi,%rbp,2)
	movw	%si, (%rdx,%r9,2)
	movzwl	22(%rsp), %edx          # 2-byte Folded Reload
	leal	1(%rcx,%rdx), %esi
	shrl	%esi
	movw	%si, (%rdi,%r8,2)
	leal	(%rax,%rcx,2), %eax
	leal	2(%rdx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, (%rdi,%r9,2)
	jmp	.LBB0_75
.LBB0_61:
	movl	%ebp, %r14d
	movl	%edx, 48(%rsp)          # 4-byte Spill
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%edi, 24(%rsp)          # 4-byte Spill
	movzwl	12(%rsp), %r12d         # 2-byte Folded Reload
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movq	%r11, %rbp
	testl	%r13d, %r13d
	jne	.LBB0_63
# BB#62:
	movl	4(%r10), %esi
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	movq	64(%rsp), %r10          # 8-byte Reload
.LBB0_63:
	movzwl	16(%rsp), %r9d          # 2-byte Folded Reload
	movzwl	88(%rsp), %r11d         # 2-byte Folded Reload
	leal	1(%r11,%r9), %eax
	shrl	%eax
	movslq	%ebx, %r8
	movq	72(%rsp), %rcx          # 8-byte Reload
	shlq	$5, %rcx
	leaq	104(%r10,%rcx), %rcx
	movw	%ax, (%rcx,%r8,2)
	movzwl	%r12w, %edx
	leal	1(%rdx,%r11), %eax
	shrl	%eax
	movslq	%ebp, %rsi
	shlq	$5, %rsi
	leaq	104(%r10,%rsi), %rsi
	movw	%ax, (%rsi,%r8,2)
	movslq	%r14d, %r13
	movw	%ax, (%rcx,%r13,2)
	movzwl	%r15w, %edi
	leal	1(%rdi,%rdx), %ebp
	shrl	%ebp
	movw	%bp, (%rsi,%r13,2)
	movslq	48(%rsp), %rax          # 4-byte Folded Reload
	movw	%bp, (%rcx,%rax,2)
	movzwl	82(%rsp), %ebp          # 2-byte Folded Reload
	leal	1(%rdi,%rbp), %ebx
	shrl	%ebx
	movw	%bx, (%rsi,%rax,2)
	movslq	24(%rsp), %r14          # 4-byte Folded Reload
	movw	%bx, (%rcx,%r14,2)
	movq	%r10, %r12
	movzwl	84(%rsp), %r10d         # 2-byte Folded Reload
	leal	1(%rbp,%r10), %ecx
	shrl	%ecx
	movw	%cx, (%rsi,%r14,2)
	leal	(%rdx,%r11,2), %ecx
	leal	2(%r9,%rcx), %ecx
	shrl	$2, %ecx
	movslq	32(%rsp), %rsi          # 4-byte Folded Reload
	shlq	$5, %rsi
	leaq	104(%r12,%rsi), %rsi
	movw	%cx, (%rsi,%r8,2)
	leal	2(%rdi,%rdx,2), %ecx
	addl	%r11d, %ecx
	movslq	56(%rsp), %rbx          # 4-byte Folded Reload
	shlq	$5, %rbx
	leaq	104(%r12,%rbx), %rbx
	shrl	$2, %ecx
	movw	%cx, (%rbx,%r8,2)
	movw	%cx, (%rsi,%r13,2)
	leal	(%rdx,%rdi,2), %ecx
	leal	2(%rbp,%rcx), %ecx
	shrl	$2, %ecx
	movw	%cx, (%rbx,%r13,2)
	movw	%cx, (%rsi,%rax,2)
	leal	2(%rdi,%rbp,2), %ecx
	addl	%r10d, %ecx
	shrl	$2, %ecx
	movw	%cx, (%rbx,%rax,2)
	movw	%cx, (%rsi,%r14,2)
	leal	(%rbp,%r10,2), %eax
	movzwl	86(%rsp), %ecx          # 2-byte Folded Reload
	leal	2(%rcx,%rax), %eax
	shrl	$2, %eax
	movw	%ax, (%rbx,%r14,2)
	jmp	.LBB0_75
.LBB0_64:
	movl	%ecx, %r13d
	movl	%ebp, %r15d
	movl	%edx, %r14d
	movl	%eax, %r12d
	movl	%edi, 24(%rsp)          # 4-byte Spill
	testl	%r8d, %r8d
	jne	.LBB0_66
# BB#65:
	movl	4(%r10), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%r11, %rbp
	callq	printf
	movq	%rbp, %r11
	movq	64(%rsp), %r10          # 8-byte Reload
.LBB0_66:
	movzwl	14(%rsp), %ecx          # 2-byte Folded Reload
	movzwl	18(%rsp), %edi          # 2-byte Folded Reload
	leal	1(%rcx,%rdi), %edx
	shrl	%edx
	movslq	%ebx, %r8
	movq	72(%rsp), %rax          # 8-byte Reload
	shlq	$5, %rax
	leaq	104(%r10,%rax), %rbp
	movw	%dx, (%rbp,%r8,2)
	movzwl	20(%rsp), %edx          # 2-byte Folded Reload
	leal	(%rcx,%rdi,2), %ecx
	leal	2(%rdx,%rcx), %esi
	shrl	$2, %esi
	movslq	%r15d, %r9
	movw	%si, (%rbp,%r9,2)
	leal	1(%rdi,%rdx), %ebx
	shrl	%ebx
	movslq	%r12d, %rsi
	shlq	$5, %rsi
	leaq	104(%r10,%rsi), %rax
	movw	%bx, (%rax,%r8,2)
	movslq	%r14d, %r14
	movw	%bx, (%rbp,%r14,2)
	movzwl	22(%rsp), %esi          # 2-byte Folded Reload
	movzwl	%si, %ebx
	leal	(%rdi,%rdx,2), %edi
	leal	2(%rbx,%rdi), %edi
	shrl	$2, %edi
	movw	%di, (%rax,%r9,2)
	movslq	24(%rsp), %rcx          # 4-byte Folded Reload
	movw	%di, (%rbp,%rcx,2)
	leal	1(%rdx,%rbx), %edi
	shrl	%edi
	movslq	%r11d, %rbp
	shlq	$5, %rbp
	leaq	104(%r10,%rbp), %rbp
	movw	%di, (%rbp,%r8,2)
	movw	%di, (%rax,%r14,2)
	addl	%ebx, %edx
	leal	2(%rdx,%rbx,2), %edx
	shrl	$2, %edx
	movw	%dx, (%rbp,%r9,2)
	movw	%dx, (%rax,%rcx,2)
	movslq	%r13d, %rax
	shlq	$5, %rax
	leaq	104(%r10,%rax), %rax
	movw	%si, (%rax,%rcx,2)
	movw	%si, (%rax,%r14,2)
	movw	%si, (%rbp,%r14,2)
	movw	%si, (%rax,%r8,2)
	movw	%si, (%rax,%r9,2)
	movw	%si, (%rbp,%rcx,2)
	jmp	.LBB0_75
.LBB0_67:
	testl	%r13d, %r13d
	movzwl	88(%rsp), %esi          # 2-byte Folded Reload
	movzwl	20(%rsp), %edx          # 2-byte Folded Reload
	movzwl	16(%rsp), %eax          # 2-byte Folded Reload
	movzwl	18(%rsp), %ecx          # 2-byte Folded Reload
	movzwl	22(%rsp), %edi          # 2-byte Folded Reload
	jne	.LBB0_70
# BB#68:
	testl	%r8d, %r8d
	je	.LBB0_70
# BB#69:
	movzwl	14(%rsp), %eax          # 2-byte Folded Reload
	movzwl	%cx, %ecx
	movzwl	%dx, %edx
	movzwl	%di, %esi
	addl	%eax, %ecx
	addl	%edx, %ecx
	leal	2(%rsi,%rcx), %eax
	shrl	$2, %eax
	jmp	.LBB0_74
.LBB0_70:
	testl	%r13d, %r13d
	je	.LBB0_73
# BB#71:
	testl	%r8d, %r8d
	jne	.LBB0_73
# BB#72:
	movzwl	%ax, %eax
	movzwl	%si, %ecx
	movzwl	12(%rsp), %edx          # 2-byte Folded Reload
	movzwl	%r15w, %esi
	addl	%edx, %esi
	addl	%ecx, %esi
	leal	2(%rax,%rsi), %eax
	shrl	$2, %eax
	jmp	.LBB0_74
.LBB0_73:
	movl	5892(%r10), %eax
.LBB0_74:                               # %.preheader
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movslq	%ebx, %rcx
	shlq	$5, %rdx
	leaq	104(%r10,%rdx), %rdx
	movw	%ax, (%rdx,%rcx,2)
	movw	%ax, 2(%rdx,%rcx,2)
	movw	%ax, 4(%rdx,%rcx,2)
	movw	%ax, 6(%rdx,%rcx,2)
	shlq	$5, %rbp
	leaq	104(%r10,%rbp), %rdx
	movw	%ax, (%rdx,%rcx,2)
	movw	%ax, 2(%rdx,%rcx,2)
	movw	%ax, 4(%rdx,%rcx,2)
	movw	%ax, 6(%rdx,%rcx,2)
	shlq	$5, %rdi
	leaq	104(%r10,%rdi), %rdx
	movw	%ax, (%rdx,%rcx,2)
	movw	%ax, 2(%rdx,%rcx,2)
	movw	%ax, 4(%rdx,%rcx,2)
	movw	%ax, 6(%rdx,%rcx,2)
	shlq	$5, %rsi
	leaq	104(%r10,%rsi), %rdx
	movw	%ax, (%rdx,%rcx,2)
	movw	%ax, 2(%rdx,%rcx,2)
	movw	%ax, 4(%rdx,%rcx,2)
	movw	%ax, 6(%rdx,%rcx,2)
.LBB0_75:                               # %.loopexit
	xorl	%eax, %eax
.LBB0_76:                               # %.loopexit
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	intrapred, .Lfunc_end0-intrapred
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_33
	.quad	.LBB0_42
	.quad	.LBB0_45
	.quad	.LBB0_48
	.quad	.LBB0_36
	.quad	.LBB0_51
	.quad	.LBB0_56
	.quad	.LBB0_61
	.quad	.LBB0_64

	.text
	.globl	intrapred_luma_16x16
	.p2align	4, 0x90
	.type	intrapred_luma_16x16,@function
intrapred_luma_16x16:                   # @intrapred_luma_16x16
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi19:
	.cfi_def_cfa_offset 496
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movq	dec_picture(%rip), %rax
	movq	316920(%rax), %r12
	movl	4(%r13), %r15d
	leaq	32(%rsp), %rbp
	xorl	%ebx, %ebx
	movq	getNeighbour(%rip), %rax
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	leal	-1(%rbx), %edx
	movl	$-1, %esi
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	movq	%rbp, %r8
	callq	*%rax
	incq	%rbx
	movq	getNeighbour(%rip), %rax
	addq	$24, %rbp
	cmpq	$17, %rbx
	jne	.LBB1_1
# BB#2:
	xorl	%ebp, %ebp
	movq	%rsp, %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	callq	*%rax
	movq	active_pps(%rip), %rax
	movl	(%rsp), %r8d
	cmpl	$0, 1148(%rax)
	je	.LBB1_13
# BB#3:
	testl	%r8d, %r8d
	je	.LBB1_5
# BB#4:
	movq	16(%r13), %rax
	movslq	4(%rsp), %rcx
	movl	(%rax,%rcx,4), %ebp
.LBB1_5:                                # %._crit_edge
	movl	$1, %eax
	movl	$52, %ecx
	.p2align	4, 0x90
.LBB1_6:                                # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	cmpl	$0, 4(%rsp,%rcx)
	movl	$0, %esi
	je	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=1
	movq	16(%r13), %rsi
	movslq	8(%rsp,%rcx), %rdi
	movl	(%rsi,%rdi,4), %esi
.LBB1_8:                                #   in Loop: Header=BB1_6 Depth=1
	andl	%eax, %esi
	cmpl	$0, 28(%rsp,%rcx)
	je	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_6 Depth=1
	movq	16(%r13), %rax
	movslq	32(%rsp,%rcx), %rdx
	movl	(%rax,%rdx,4), %edx
.LBB1_10:                               #   in Loop: Header=BB1_6 Depth=1
	movl	%edx, %eax
	andl	%esi, %eax
	addq	$48, %rcx
	cmpq	$436, %rcx              # imm = 0x1B4
	jne	.LBB1_6
# BB#11:
	cmpl	$0, 32(%rsp)
	je	.LBB1_14
# BB#12:
	movq	16(%r13), %rcx
	movslq	36(%rsp), %rdx
	movl	(%rcx,%rdx,4), %edx
	jmp	.LBB1_15
.LBB1_13:
	movl	32(%rsp), %edx
	movl	56(%rsp), %eax
	cmpl	$3, %r14d
	jbe	.LBB1_16
	jmp	.LBB1_21
.LBB1_14:
	xorl	%edx, %edx
.LBB1_15:
	movl	%ebp, %r8d
	cmpl	$3, %r14d
	ja	.LBB1_21
.LBB1_16:
	movl	%r14d, %ecx
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_17:
	testl	%r8d, %r8d
	jne	.LBB1_19
# BB#18:
	movl	$.L.str.9, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB1_19:                               # %.preheader170
	movslq	20(%rsp), %rax
	movq	(%r12,%rax,8), %rcx
	movslq	16(%rsp), %rdx
	xorl	%eax, %eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_20:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx,%rdx,2), %edi
	movw	%di, 104(%r13,%rsi)
	movzwl	2(%rcx,%rdx,2), %edi
	movw	%di, 106(%r13,%rsi)
	movzwl	4(%rcx,%rdx,2), %edi
	movw	%di, 108(%r13,%rsi)
	movzwl	6(%rcx,%rdx,2), %edi
	movw	%di, 110(%r13,%rsi)
	movzwl	8(%rcx,%rdx,2), %edi
	movw	%di, 112(%r13,%rsi)
	movzwl	10(%rcx,%rdx,2), %edi
	movw	%di, 114(%r13,%rsi)
	movzwl	12(%rcx,%rdx,2), %edi
	movw	%di, 116(%r13,%rsi)
	movzwl	14(%rcx,%rdx,2), %edi
	movw	%di, 118(%r13,%rsi)
	movzwl	16(%rcx,%rdx,2), %edi
	movw	%di, 120(%r13,%rsi)
	movzwl	18(%rcx,%rdx,2), %edi
	movw	%di, 122(%r13,%rsi)
	movzwl	20(%rcx,%rdx,2), %edi
	movw	%di, 124(%r13,%rsi)
	movzwl	22(%rcx,%rdx,2), %edi
	movw	%di, 126(%r13,%rsi)
	movzwl	24(%rcx,%rdx,2), %edi
	movw	%di, 128(%r13,%rsi)
	movzwl	26(%rcx,%rdx,2), %edi
	movw	%di, 130(%r13,%rsi)
	movzwl	28(%rcx,%rdx,2), %edi
	movw	%di, 132(%r13,%rsi)
	movzwl	30(%rcx,%rdx,2), %edi
	movw	%di, 134(%r13,%rsi)
	addq	$32, %rsi
	cmpq	$512, %rsi              # imm = 0x200
	jne	.LBB1_20
	jmp	.LBB1_56
.LBB1_21:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movl	$1, %eax
	jmp	.LBB1_56
.LBB1_22:
	testl	%eax, %eax
	jne	.LBB1_24
# BB#23:
	movl	$.L.str.10, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB1_24:                               # %.preheader171.preheader
	addq	$134, %r13
	movl	$44, %eax
	.p2align	4, 0x90
.LBB1_25:                               # %.preheader171
                                        # =>This Inner Loop Header: Depth=1
	movslq	32(%rsp,%rax), %rcx
	movq	(%r12,%rcx,8), %rdx
	movslq	28(%rsp,%rax), %rcx
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -30(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -28(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -26(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -24(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -22(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -20(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -18(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -16(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -14(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -12(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -10(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -8(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -6(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -4(%r13)
	movzwl	(%rdx,%rcx,2), %esi
	movw	%si, -2(%r13)
	movzwl	(%rdx,%rcx,2), %ecx
	movw	%cx, (%r13)
	addq	$32, %r13
	addq	$24, %rax
	cmpq	$428, %rax              # imm = 0x1AC
	jne	.LBB1_25
	jmp	.LBB1_55
.LBB1_27:                               # %.preheader177
	testl	%r8d, %r8d
	je	.LBB1_45
# BB#28:                                # %.preheader177.split.us.preheader
	movslq	20(%rsp), %rcx
	movslq	16(%rsp), %r9
	leaq	76(%rsp), %rbp
	addq	%r9, %r9
	addq	(%r12,%rcx,8), %r9
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB1_29:                               # %.preheader177.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%r9,%rbx,2), %ecx
	incq	%rbx
	testl	%eax, %eax
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_29 Depth=1
	movslq	(%rbp), %rdi
	movq	(%r12,%rdi,8), %rdi
	movslq	-4(%rbp), %rsi
	movzwl	(%rdi,%rsi,2), %esi
	addl	%esi, %r10d
.LBB1_31:                               # %.preheader177.split.us._crit_edge
                                        #   in Loop: Header=BB1_29 Depth=1
	addl	%ecx, %edx
	addq	$24, %rbp
	cmpq	$16, %rbx
	jne	.LBB1_29
	jmp	.LBB1_48
.LBB1_32:
	testl	%eax, %eax
	je	.LBB1_35
# BB#33:
	testl	%r8d, %r8d
	je	.LBB1_35
# BB#34:
	testl	%edx, %edx
	jne	.LBB1_36
.LBB1_35:
	movl	$.L.str.11, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB1_36:                               # %.preheader180
	movslq	20(%rsp), %rax
	movq	(%r12,%rax,8), %r11
	movslq	16(%rsp), %rax
	movslq	52(%rsp), %r9
	movl	48(%rsp), %r10d
	leal	6(%rax), %r8d
	leaq	220(%rsp), %rbp
	leaq	268(%rsp), %rdx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	16(%r11,%rax,2), %r14
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_37:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%r14,%rcx,2), %ebx
	cmpq	$7, %rcx
	movl	%r8d, %edi
	movq	%r11, %rsi
	jne	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_37 Depth=1
	movq	(%r12,%r9,8), %rsi
	movl	%r10d, %edi
.LBB1_39:                               #   in Loop: Header=BB1_37 Depth=1
	movslq	%edi, %rdi
	movzwl	(%rsi,%rdi,2), %esi
	subl	%esi, %ebx
	incq	%rcx
	imull	%ecx, %ebx
	addl	%ebx, %eax
	movslq	(%rdx), %rsi
	movq	(%r12,%rsi,8), %rsi
	movslq	-4(%rdx), %rdi
	movzwl	(%rsi,%rdi,2), %esi
	movslq	(%rbp), %rdi
	movq	(%r12,%rdi,8), %rdi
	movslq	-4(%rbp), %rbx
	movzwl	(%rdi,%rbx,2), %edi
	subl	%edi, %esi
	imull	%ecx, %esi
	addl	%esi, %r15d
	decl	%r8d
	addq	$-24, %rbp
	addq	$24, %rdx
	cmpq	$8, %rcx
	jne	.LBB1_37
# BB#40:
	leal	32(%rax,%rax,4), %eax
	sarl	$6, %eax
	leal	32(%r15,%r15,4), %r9d
	sarl	$6, %r9d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzwl	30(%r11,%rcx,2), %ecx
	movslq	436(%rsp), %rdx
	movq	(%r12,%rdx,8), %rdx
	movslq	432(%rsp), %rsi
	movzwl	(%rdx,%rsi,2), %r8d
	addl	%ecx, %r8d
	shll	$4, %r8d
	addl	$16, %r8d
	leaq	106(%r13), %r10
	imull	$-6, %eax, %r14d
	leal	(,%r9,8), %ecx
	subl	%r9d, %ecx
	subl	%ecx, %r14d
	leal	(%rax,%rax), %r15d
	imull	$-7, %r9d, %ebp
	leal	(,%rax,8), %ecx
	subl	%eax, %ecx
	subl	%ecx, %ebp
	xorl	%eax, %eax
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB1_41:                               # %.preheader178
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_42 Depth 2
	movl	%r8d, %edx
	movl	$16, %ecx
	movq	%r10, %rbx
	.p2align	4, 0x90
.LBB1_42:                               #   Parent Loop BB1_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	5900(%r13), %esi
	leal	(%rbp,%rdx), %edi
	sarl	$5, %edi
	cmovsl	%eax, %edi
	cmpl	%esi, %edi
	cmovlw	%di, %si
	movw	%si, -2(%rbx)
	movl	5900(%r13), %esi
	leal	(%r14,%rdx), %edi
	sarl	$5, %edi
	cmovsl	%eax, %edi
	cmpl	%esi, %edi
	cmovlw	%di, %si
	movw	%si, (%rbx)
	addq	$4, %rbx
	addl	%r15d, %edx
	addq	$-2, %rcx
	jne	.LBB1_42
# BB#43:                                #   in Loop: Header=BB1_41 Depth=1
	incq	%r11
	addq	$32, %r10
	addl	%r9d, %r8d
	cmpq	$16, %r11
	jne	.LBB1_41
	jmp	.LBB1_55
.LBB1_45:                               # %.preheader177.split
	testl	%eax, %eax
	je	.LBB1_51
# BB#46:                                # %.preheader177.split.split.preheader
	xorl	%edx, %edx
	movl	$68, %edi
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB1_47:                               # %.preheader177.split.split
                                        # =>This Inner Loop Header: Depth=1
	movslq	8(%rsp,%rdi), %rcx
	movq	(%r12,%rcx,8), %rcx
	movslq	4(%rsp,%rdi), %rsi
	movzwl	(%rcx,%rsi,2), %ecx
	addl	%r10d, %ecx
	movslq	32(%rsp,%rdi), %rsi
	movq	(%r12,%rsi,8), %rsi
	movslq	28(%rsp,%rdi), %rbp
	movzwl	(%rsi,%rbp,2), %r10d
	addl	%ecx, %r10d
	addq	$48, %rdi
	cmpq	$452, %rdi              # imm = 0x1C4
	jne	.LBB1_47
.LBB1_48:                               # %.us-lcssa.us
	testl	%eax, %eax
	setne	%dil
	xorl	%ebp, %ebp
	testl	%r8d, %r8d
	je	.LBB1_52
# BB#49:                                # %.us-lcssa.us
	testl	%eax, %eax
	je	.LBB1_52
# BB#50:
	leal	16(%r10,%rdx), %ebp
	sarl	$5, %ebp
	jmp	.LBB1_52
.LBB1_51:
	xorl	%edi, %edi
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	xorl	%ebp, %ebp
.LBB1_52:                               # %.us-lcssa.us.thread
	addl	$8, %r10d
	sarl	$4, %r10d
	testb	%dil, %dil
	cmovel	%ebp, %r10d
	testl	%r8d, %r8d
	cmovnel	%ebp, %r10d
	addl	$8, %edx
	sarl	$4, %edx
	testb	%dil, %dil
	cmovnel	%r10d, %edx
	testl	%r8d, %r8d
	cmovel	%r10d, %edx
	orl	%r8d, %eax
	jne	.LBB1_54
# BB#53:
	movl	5892(%r13), %edx
.LBB1_54:                               # %vector.body
	movd	%edx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, 104(%r13)
	movdqu	%xmm0, 136(%r13)
	movdqu	%xmm0, 168(%r13)
	movdqu	%xmm0, 200(%r13)
	movdqu	%xmm0, 232(%r13)
	movdqu	%xmm0, 264(%r13)
	movdqu	%xmm0, 296(%r13)
	movdqu	%xmm0, 328(%r13)
	movdqu	%xmm0, 360(%r13)
	movdqu	%xmm0, 392(%r13)
	movdqu	%xmm0, 424(%r13)
	movdqu	%xmm0, 456(%r13)
	movdqu	%xmm0, 488(%r13)
	movdqu	%xmm0, 520(%r13)
	movdqu	%xmm0, 552(%r13)
	movdqu	%xmm0, 584(%r13)
	movdqu	%xmm0, 120(%r13)
	movdqu	%xmm0, 152(%r13)
	movdqu	%xmm0, 184(%r13)
	movdqu	%xmm0, 216(%r13)
	movdqu	%xmm0, 248(%r13)
	movdqu	%xmm0, 280(%r13)
	movdqu	%xmm0, 312(%r13)
	movdqu	%xmm0, 344(%r13)
	movdqu	%xmm0, 376(%r13)
	movdqu	%xmm0, 408(%r13)
	movdqu	%xmm0, 440(%r13)
	movdqu	%xmm0, 472(%r13)
	movdqu	%xmm0, 504(%r13)
	movdqu	%xmm0, 536(%r13)
	movdqu	%xmm0, 568(%r13)
	movdqu	%xmm0, 600(%r13)
.LBB1_55:                               # %.loopexit
	xorl	%eax, %eax
.LBB1_56:                               # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	intrapred_luma_16x16, .Lfunc_end1-intrapred_luma_16x16
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_17
	.quad	.LBB1_22
	.quad	.LBB1_27
	.quad	.LBB1_32

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI2_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI2_2:
	.quad	4                       # 0x4
	.quad	4                       # 0x4
	.text
	.globl	intrapred_chroma
	.p2align	4, 0x90
	.type	intrapred_chroma,@function
intrapred_chroma:                       # @intrapred_chroma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$664, %rsp              # imm = 0x298
.Lcfi32:
	.cfi_def_cfa_offset 720
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%esi, 60(%rsp)          # 4-byte Spill
	movq	%rdi, %r13
	movq	dec_picture(%rip), %rax
	movq	316928(%rax), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	317044(%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	5600(%r13), %r14
	movl	4(%r13), %r15d
	movl	5932(%r13), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movslq	5936(%r13), %rcx
	testq	%rcx, %rcx
	movq	getNeighbour(%rip), %rax
	movq	%rcx, %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	js	.LBB2_3
# BB#1:                                 # %.lr.ph544.preheader
	leal	1(%rcx), %r12d
	leaq	176(%rsp), %rbp
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph544
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, %esi
	movl	$1, %ecx
	movl	%r15d, %edi
	movl	%ebx, %edx
	movq	%rbp, %r8
	callq	*%rax
	movq	getNeighbour(%rip), %rax
	addq	$24, %rbp
	incl	%ebx
	decq	%r12
	jne	.LBB2_2
.LBB2_3:                                # %._crit_edge545
	xorl	%r12d, %r12d
	leaq	120(%rsp), %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	movl	$1, %ecx
	movl	%r15d, %edi
	callq	*%rax
	movq	active_pps(%rip), %rax
	movl	120(%rsp), %edx
	cmpl	$0, 1148(%rax)
	je	.LBB2_14
# BB#4:
	testl	%edx, %edx
	je	.LBB2_6
# BB#5:
	movq	16(%r13), %rax
	movslq	124(%rsp), %rcx
	movl	(%rax,%rcx,4), %r12d
.LBB2_6:
	movq	48(%rsp), %r8           # 8-byte Reload
	movl	%r8d, %ecx
	shrl	$31, %ecx
	addl	%r8d, %ecx
	sarl	%ecx
	movl	$1, %r9d
	cmpl	$2, %r8d
	movl	$1, %ebp
	jl	.LBB2_11
# BB#7:                                 # %.lr.ph539
	movslq	%ecx, %rdx
	leaq	204(%rsp), %rsi
	movl	$1, %eax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	incq	%rdi
	cmpl	$0, -4(%rsi)
	movl	$0, %ebp
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	movq	16(%r13), %rbp
	movslq	(%rsi), %rbx
	movl	(%rbp,%rbx,4), %ebp
.LBB2_10:                               #   in Loop: Header=BB2_8 Depth=1
	andl	%eax, %ebp
	addq	$24, %rsi
	cmpq	%rdx, %rdi
	movl	%ebp, %eax
	jl	.LBB2_8
.LBB2_11:                               # %.preheader460
	cmpl	%r8d, %ecx
	jge	.LBB2_25
# BB#12:                                # %.lr.ph533
	movslq	%ecx, %rdx
	movl	%r8d, %eax
	subl	%ecx, %eax
	leaq	-1(%r8), %rcx
	testb	$1, %al
	jne	.LBB2_15
# BB#13:
	movl	$1, %esi
                                        # implicit-def: %R9D
	movq	%rdx, %rdi
	cmpq	%rdx, %rcx
	jne	.LBB2_19
	jmp	.LBB2_25
.LBB2_14:
	movl	176(%rsp), %ecx
	movl	200(%rsp), %r9d
	movl	%r9d, %ebp
	movq	48(%rsp), %r8           # 8-byte Reload
	jmp	.LBB2_29
.LBB2_15:
	leaq	1(%rdx), %rdi
	leaq	(%rdi,%rdi,2), %rax
	cmpl	$0, 176(%rsp,%rax,8)
	je	.LBB2_17
# BB#16:
	movq	16(%r13), %rsi
	movslq	180(%rsp,%rax,8), %rax
	movl	(%rsi,%rax,4), %r9d
	jmp	.LBB2_18
.LBB2_17:
	xorl	%r9d, %r9d
.LBB2_18:
	andl	$1, %r9d
	movl	%r9d, %esi
	cmpq	%rdx, %rcx
	je	.LBB2_25
.LBB2_19:                               # %.lr.ph533.new
	movq	%r8, %rcx
	subq	%rdi, %rcx
	leaq	(%rdi,%rdi,2), %rax
	leaq	228(%rsp,%rax,8), %rdx
	.p2align	4, 0x90
.LBB2_20:                               # =>This Inner Loop Header: Depth=1
	xorl	%r9d, %r9d
	cmpl	$0, -28(%rdx)
	movl	$0, %eax
	je	.LBB2_22
# BB#21:                                #   in Loop: Header=BB2_20 Depth=1
	movq	16(%r13), %rax
	movslq	-24(%rdx), %rdi
	movl	(%rax,%rdi,4), %eax
.LBB2_22:                               #   in Loop: Header=BB2_20 Depth=1
	andl	%esi, %eax
	cmpl	$0, -4(%rdx)
	je	.LBB2_24
# BB#23:                                #   in Loop: Header=BB2_20 Depth=1
	movq	16(%r13), %rsi
	movslq	(%rdx), %rdi
	movl	(%rsi,%rdi,4), %r9d
.LBB2_24:                               #   in Loop: Header=BB2_20 Depth=1
	andl	%eax, %r9d
	addq	$48, %rdx
	addq	$-2, %rcx
	movl	%r9d, %esi
	jne	.LBB2_20
.LBB2_25:                               # %._crit_edge534
	cmpl	$0, 176(%rsp)
	je	.LBB2_27
# BB#26:
	movq	16(%r13), %rax
	movslq	180(%rsp), %rcx
	movl	(%rax,%rcx,4), %ecx
	jmp	.LBB2_28
.LBB2_27:
	xorl	%ecx, %ecx
.LBB2_28:
	movl	%r12d, %edx
.LBB2_29:
	movq	32(%rsp), %rsi          # 8-byte Reload
	imulq	$408, %r15, %rax        # imm = 0x198
	movl	352(%r14,%rax), %eax
	cmpq	$3, %rax
	ja	.LBB2_121
# BB#30:
	movabsq	$9223372036854775792, %rbx # imm = 0x7FFFFFFFFFFFFFF0
	movq	%r13, 64(%rsp)          # 8-byte Spill
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_31:                               # %.preheader459
	decq	96(%rsp)                # 8-byte Folded Spill
	movl	5924(%r13), %edi
	cmpl	$2, %edi
	jl	.LBB2_77
# BB#32:                                # %.preheader458.lr.ph
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	%ecx, 116(%rsp)         # 4-byte Spill
	movl	5896(%r13), %r8d
	testl	%edx, %edx
	setne	%al
	movslq	60(%rsp), %rcx          # 4-byte Folded Reload
	movq	%rcx, (%rsp)            # 8-byte Spill
	movslq	140(%rsp), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	%ebp, 72(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	setne	%cl
	andb	%al, %cl
	movb	%cl, 104(%rsp)          # 1-byte Spill
	movl	%r9d, 40(%rsp)          # 4-byte Spill
	testl	%r9d, %r9d
	setne	%cl
	andb	%al, %cl
	movb	%cl, 31(%rsp)           # 1-byte Spill
	shrl	%edi
	movslq	136(%rsp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, %r14
	shlq	$5, %r14
	shlq	$6, %rax
	leaq	.Lintrapred_chroma.block_pos(%rax), %r9
	leaq	592(%rsp), %r10
	xorl	%r15d, %r15d
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_33:                               # %.preheader458
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_34 Depth 2
	movq	$-4, %r11
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movq	%r10, 160(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_34:                               #   Parent Loop BB2_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r14,%r11), %rax
	movzbl	subblk_offset_y+4(%rax,%r15,4), %ebx
	movzbl	subblk_offset_x+4(%rax,%r15,4), %r12d
	movl	%r8d, (%r10)
	movl	(%r9), %eax
	cmpq	$3, %rax
	ja	.LBB2_73
# BB#35:                                #   in Loop: Header=BB2_34 Depth=2
	jmpq	*.LJTI2_1(,%rax,8)
.LBB2_36:                               #   in Loop: Header=BB2_34 Depth=2
	xorl	%ecx, %ecx
	testl	%edx, %edx
	movl	$0, %ebp
	je	.LBB2_40
# BB#37:                                # %.preheader445
                                        #   in Loop: Header=BB2_34 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rsi,%rax,8), %rax
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rdi
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r12), %rax
	movzwl	(%rdi,%rax,2), %ebp
	leaq	3(%r12), %rdx
	cmpq	%rdx, %r12
	jae	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_34 Depth=2
	movzwl	2(%rdi,%rax,2), %edx
	addl	%ebp, %edx
	movzwl	4(%rdi,%rax,2), %esi
	addl	%edx, %esi
	movzwl	6(%rdi,%rax,2), %ebp
	addl	%esi, %ebp
	movq	32(%rsp), %rsi          # 8-byte Reload
.LBB2_39:                               #   in Loop: Header=BB2_34 Depth=2
	movl	16(%rsp), %edx          # 4-byte Reload
.LBB2_40:                               # %.loopexit446
                                        #   in Loop: Header=BB2_34 Depth=2
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	je	.LBB2_62
# BB#41:                                # %.preheader444
                                        #   in Loop: Header=BB2_34 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rsi,%rax,8), %rdi
	leaq	1(%rbx), %rax
	leaq	(%rax,%rax,2), %rcx
	movslq	196(%rsp,%rcx,8), %rdx
	movq	(%rdi,%rdx,8), %rdx
	movslq	192(%rsp,%rcx,8), %rcx
	movzwl	(%rdx,%rcx,2), %ecx
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %rax
	jae	.LBB2_43
# BB#42:                                #   in Loop: Header=BB2_34 Depth=2
	leaq	2(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	movslq	196(%rsp,%rax,8), %rdx
	movq	(%rdi,%rdx,8), %rdx
	movslq	192(%rsp,%rax,8), %rax
	movzwl	(%rdx,%rax,2), %eax
	addl	%ecx, %eax
	leaq	(%rbx,%rbx,2), %rcx
	movslq	268(%rsp,%rcx,8), %rdx
	movq	(%rdi,%rdx,8), %rdx
	movslq	264(%rsp,%rcx,8), %rsi
	movzwl	(%rdx,%rsi,2), %edx
	movq	32(%rsp), %rsi          # 8-byte Reload
	addl	%eax, %edx
	movslq	292(%rsp,%rcx,8), %rax
	movq	(%rdi,%rax,8), %rax
	movslq	288(%rsp,%rcx,8), %rcx
	movzwl	(%rax,%rcx,2), %ecx
	addl	%edx, %ecx
.LBB2_43:                               #   in Loop: Header=BB2_34 Depth=2
	cmpb	$0, 104(%rsp)           # 1-byte Folded Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	je	.LBB2_62
# BB#44:                                #   in Loop: Header=BB2_34 Depth=2
	leal	4(%rbp,%rcx), %eax
	sarl	$3, %eax
	movl	%eax, (%r10)
	jmp	.LBB2_73
	.p2align	4, 0x90
.LBB2_45:                               #   in Loop: Header=BB2_34 Depth=2
	testl	%edx, %edx
	jne	.LBB2_68
# BB#46:                                #   in Loop: Header=BB2_34 Depth=2
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	je	.LBB2_73
# BB#47:                                # %.preheader449
                                        #   in Loop: Header=BB2_34 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rsi,%rax,8), %rcx
	leaq	1(%rbx), %rax
	leaq	(%rax,%rax,2), %rdx
	movslq	196(%rsp,%rdx,8), %rdi
	movq	(%rcx,%rdi,8), %rdi
	movslq	192(%rsp,%rdx,8), %rdx
	movzwl	(%rdi,%rdx,2), %edi
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %rax
	jae	.LBB2_49
# BB#48:                                #   in Loop: Header=BB2_34 Depth=2
	leaq	2(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	movslq	196(%rsp,%rax,8), %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	192(%rsp,%rax,8), %rax
	movzwl	(%rdx,%rax,2), %eax
	addl	%edi, %eax
	leaq	(%rbx,%rbx,2), %rdx
	movslq	268(%rsp,%rdx,8), %rdi
	movq	(%rcx,%rdi,8), %rdi
	movslq	264(%rsp,%rdx,8), %rbx
	movzwl	(%rdi,%rbx,2), %ebx
	addl	%eax, %ebx
	movslq	292(%rsp,%rdx,8), %rax
	movq	(%rcx,%rax,8), %rax
	movslq	288(%rsp,%rdx,8), %rcx
	movzwl	(%rax,%rcx,2), %edi
	addl	%ebx, %edi
.LBB2_49:                               #   in Loop: Header=BB2_34 Depth=2
	movl	16(%rsp), %edx          # 4-byte Reload
	addl	$2, %edi
	sarl	$2, %edi
	movl	%edi, (%r10)
	jmp	.LBB2_73
	.p2align	4, 0x90
.LBB2_50:                               #   in Loop: Header=BB2_34 Depth=2
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	je	.LBB2_67
# BB#51:                                # %.preheader451
                                        #   in Loop: Header=BB2_34 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rsi,%rax,8), %rcx
	leaq	1(%rbx), %rax
	leaq	(%rax,%rax,2), %rdx
	movslq	196(%rsp,%rdx,8), %rdi
	movq	(%rcx,%rdi,8), %rdi
	movslq	192(%rsp,%rdx,8), %rdx
	movzwl	(%rdi,%rdx,2), %edi
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %rax
	jae	.LBB2_53
# BB#52:                                #   in Loop: Header=BB2_34 Depth=2
	leaq	2(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	movslq	196(%rsp,%rax,8), %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	192(%rsp,%rax,8), %rax
	movzwl	(%rdx,%rax,2), %eax
	addl	%edi, %eax
	leaq	(%rbx,%rbx,2), %rdx
	movslq	268(%rsp,%rdx,8), %rdi
	movq	(%rcx,%rdi,8), %rdi
	movslq	264(%rsp,%rdx,8), %rbx
	movzwl	(%rdi,%rbx,2), %ebx
	addl	%eax, %ebx
	movslq	292(%rsp,%rdx,8), %rax
	movq	(%rcx,%rax,8), %rax
	movslq	288(%rsp,%rdx,8), %rcx
	movzwl	(%rax,%rcx,2), %edi
	addl	%ebx, %edi
.LBB2_53:                               #   in Loop: Header=BB2_34 Depth=2
	addl	$2, %edi
	sarl	$2, %edi
	movl	%edi, (%r10)
	movl	16(%rsp), %edx          # 4-byte Reload
	jmp	.LBB2_73
	.p2align	4, 0x90
.LBB2_54:                               #   in Loop: Header=BB2_34 Depth=2
	xorl	%ebp, %ebp
	testl	%edx, %edx
	movl	$0, %r13d
	je	.LBB2_57
# BB#55:                                # %.preheader456
                                        #   in Loop: Header=BB2_34 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rsi,%rax,8), %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movq	80(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%r12), %rax
	movzwl	(%rcx,%rax,2), %r13d
	leaq	3(%r12), %rax
	cmpq	%rax, %r12
	jae	.LBB2_57
# BB#56:                                #   in Loop: Header=BB2_34 Depth=2
	leaq	(%rdi,%r12), %rax
	movzwl	2(%rcx,%rax,2), %edx
	addl	%r13d, %edx
	movzwl	4(%rcx,%rax,2), %edi
	addl	%edx, %edi
	movl	16(%rsp), %edx          # 4-byte Reload
	movzwl	6(%rcx,%rax,2), %r13d
	addl	%edi, %r13d
.LBB2_57:                               # %.loopexit457
                                        #   in Loop: Header=BB2_34 Depth=2
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	je	.LBB2_65
# BB#58:                                # %.preheader455
                                        #   in Loop: Header=BB2_34 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rsi,%rax,8), %rcx
	leaq	1(%rbx), %rax
	leaq	(%rax,%rax,2), %rdx
	movslq	196(%rsp,%rdx,8), %rdi
	movq	(%rcx,%rdi,8), %rdi
	movslq	192(%rsp,%rdx,8), %rdx
	movzwl	(%rdi,%rdx,2), %ebp
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %rax
	jae	.LBB2_60
# BB#59:                                #   in Loop: Header=BB2_34 Depth=2
	leaq	2(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	movslq	196(%rsp,%rax,8), %rdx
	movq	(%rcx,%rdx,8), %rdx
	movslq	192(%rsp,%rax,8), %rax
	movzwl	(%rdx,%rax,2), %eax
	addl	%ebp, %eax
	leaq	(%rbx,%rbx,2), %rdx
	movslq	268(%rsp,%rdx,8), %rdi
	movq	(%rcx,%rdi,8), %rdi
	movslq	264(%rsp,%rdx,8), %rbx
	movzwl	(%rdi,%rbx,2), %edi
	addl	%eax, %edi
	movslq	292(%rsp,%rdx,8), %rax
	movq	(%rcx,%rax,8), %rax
	movslq	288(%rsp,%rdx,8), %rcx
	movzwl	(%rax,%rcx,2), %ebp
	addl	%edi, %ebp
.LBB2_60:                               #   in Loop: Header=BB2_34 Depth=2
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	je	.LBB2_65
# BB#61:                                #   in Loop: Header=BB2_34 Depth=2
	leal	4(%r13,%rbp), %eax
	sarl	$3, %eax
	movl	%eax, (%r10)
	movq	64(%rsp), %r13          # 8-byte Reload
	jmp	.LBB2_73
.LBB2_62:                               # %.thread
                                        #   in Loop: Header=BB2_34 Depth=2
	testl	%edx, %edx
	jne	.LBB2_72
# BB#63:                                #   in Loop: Header=BB2_34 Depth=2
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	je	.LBB2_73
# BB#64:                                #   in Loop: Header=BB2_34 Depth=2
	addl	$2, %ecx
	sarl	$2, %ecx
	movl	%ecx, (%r10)
	jmp	.LBB2_73
.LBB2_65:                               # %.thread429
                                        #   in Loop: Header=BB2_34 Depth=2
	testl	%edx, %edx
	je	.LBB2_71
# BB#66:                                #   in Loop: Header=BB2_34 Depth=2
	addl	$2, %r13d
	sarl	$2, %r13d
	movl	%r13d, (%r10)
	movq	64(%rsp), %r13          # 8-byte Reload
	jmp	.LBB2_73
.LBB2_67:                               #   in Loop: Header=BB2_34 Depth=2
	testl	%edx, %edx
	je	.LBB2_73
.LBB2_68:                               # %.preheader447
                                        #   in Loop: Header=BB2_34 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rsi,%rax,8), %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdi
	movq	80(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r12), %rax
	movzwl	(%rdi,%rax,2), %ecx
	leaq	3(%r12), %rax
	cmpq	%rax, %r12
	jae	.LBB2_70
# BB#69:                                #   in Loop: Header=BB2_34 Depth=2
	leaq	(%rbx,%r12), %rax
	movzwl	2(%rdi,%rax,2), %edx
	addl	%ecx, %edx
	movzwl	4(%rdi,%rax,2), %ebx
	addl	%edx, %ebx
	movl	16(%rsp), %edx          # 4-byte Reload
	movzwl	6(%rdi,%rax,2), %ecx
	addl	%ebx, %ecx
.LBB2_70:                               #   in Loop: Header=BB2_34 Depth=2
	addl	$2, %ecx
	sarl	$2, %ecx
	movl	%ecx, (%r10)
	jmp	.LBB2_73
.LBB2_71:                               #   in Loop: Header=BB2_34 Depth=2
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	je	.LBB2_73
.LBB2_72:                               #   in Loop: Header=BB2_34 Depth=2
	addl	$2, %ebp
	sarl	$2, %ebp
	movl	%ebp, (%r10)
	.p2align	4, 0x90
.LBB2_73:                               # %.thread425
                                        #   in Loop: Header=BB2_34 Depth=2
	addq	$4, %r10
	addq	$4, %r9
	incq	%r11
	jne	.LBB2_34
# BB#74:                                #   in Loop: Header=BB2_33 Depth=1
	incq	%r15
	movq	160(%rsp), %r10         # 8-byte Reload
	addq	$16, %r10
	movq	168(%rsp), %r9          # 8-byte Reload
	addq	$16, %r9
	movq	152(%rsp), %rdi         # 8-byte Reload
	cmpq	%rdi, %r15
	jl	.LBB2_33
# BB#75:                                # %thread-pre-split
	movq	144(%rsp), %rax         # 8-byte Reload
	cmpl	$3, %eax
	movq	48(%rsp), %r8           # 8-byte Reload
	movl	72(%rsp), %ebp          # 4-byte Reload
	movl	40(%rsp), %r9d          # 4-byte Reload
	movl	116(%rsp), %ecx         # 4-byte Reload
	ja	.LBB2_121
# BB#76:                                # %thread-pre-split
	movl	%eax, %eax
	movabsq	$9223372036854775792, %rbx # imm = 0x7FFFFFFFFFFFFFF0
	jmpq	*.LJTI2_2(,%rax,8)
.LBB2_77:                               # %.preheader437
	cmpl	$2, 5924(%r13)
	jl	.LBB2_209
# BB#78:                                # %.preheader436.lr.ph
	movslq	96(%rsp), %r9           # 4-byte Folded Reload
	shlq	$5, %r9
	leaq	592(%rsp), %rcx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB2_79:                               # %.preheader436
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_80 Depth 2
	movq	$-4, %r15
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB2_80:                               # %.preheader435
                                        #   Parent Loop BB2_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r9,%r15), %rax
	movzbl	subblk_offset_y+4(%rax,%r10,4), %esi
	movzbl	subblk_offset_x+4(%rax,%r10,4), %ebp
	movzwl	(%rdi), %ebx
	movq	%rsi, %rcx
	shlq	$5, %rcx
	leaq	(%r13,%rcx), %rax
	movw	%bx, 104(%rax,%rbp,2)
	leaq	1(%rbp), %r11
	leaq	3(%rbp), %r14
	cmpq	%r14, %rbp
	jae	.LBB2_82
# BB#81:                                #   in Loop: Header=BB2_80 Depth=2
	movw	%bx, 104(%rax,%r11,2)
	movw	%bx, 108(%rax,%rbp,2)
	movw	%bx, 110(%rax,%rbp,2)
.LBB2_82:                               #   in Loop: Header=BB2_80 Depth=2
	leaq	3(%rsi), %rdx
	cmpq	%rdx, %rsi
	jae	.LBB2_87
# BB#83:                                # %.preheader435.1
                                        #   in Loop: Header=BB2_80 Depth=2
	leaq	1(%rsi), %r12
	shlq	$5, %r12
	leaq	(%r13,%r12), %r8
	cmpq	%r14, %rbp
	movw	%bx, 104(%r8,%rbp,2)
	jae	.LBB2_85
# BB#84:                                #   in Loop: Header=BB2_80 Depth=2
	leaq	104(%r13,%rcx), %rax
	leaq	104(%r13,%r12), %rcx
	movw	%bx, (%rcx,%r11,2)
	movw	%bx, 4(%rcx,%rbp,2)
	movw	%bx, 6(%rcx,%rbp,2)
	movw	%bx, 64(%rax,%rbp,2)
	movw	%bx, 64(%rax,%r11,2)
	movw	%bx, 68(%rax,%rbp,2)
	movw	%bx, 70(%rax,%rbp,2)
	addq	$3, %rsi
	movq	%rsi, %rax
	shlq	$5, %rax
	leaq	104(%r13,%rax), %rax
	movw	%bx, (%rax,%rbp,2)
	movw	%bx, (%rax,%r11,2)
	movw	%bx, 4(%rax,%rbp,2)
	movq	%r14, %rbp
	movq	%rsi, %rdx
	jmp	.LBB2_86
	.p2align	4, 0x90
.LBB2_85:                               # %.preheader435.3584.thread664
                                        #   in Loop: Header=BB2_80 Depth=2
	movw	%bx, 168(%rax,%rbp,2)
.LBB2_86:                               # %.sink.split
                                        #   in Loop: Header=BB2_80 Depth=2
	shlq	$5, %rdx
	addq	%r13, %rdx
	movw	%bx, 104(%rdx,%rbp,2)
.LBB2_87:                               #   in Loop: Header=BB2_80 Depth=2
	addq	$4, %rdi
	incq	%r15
	jne	.LBB2_80
# BB#88:                                #   in Loop: Header=BB2_79 Depth=1
	incq	%r10
	movl	5924(%r13), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	addq	$16, %rcx
	cmpq	%rax, %r10
	jl	.LBB2_79
	jmp	.LBB2_209
.LBB2_89:
	testl	%edx, %edx
	jne	.LBB2_91
# BB#90:
	movl	$.L.str.15, %edi
	movl	$-1, %esi
	callq	error
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
.LBB2_91:                               # %.preheader442
	cmpl	$2, %r8d
	jl	.LBB2_209
# BB#92:                                # %.split504.us.preheader
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_209
# BB#93:                                # %.lr.ph501.us
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	60(%rsp), %r9           # 4-byte Folded Reload
	movslq	140(%rsp), %r10
	movl	%r8d, %ecx
	shrl	$31, %ecx
	addl	%r8d, %ecx
	sarl	%ecx
	movslq	136(%rsp), %r14
	movslq	%ecx, %rcx
	movq	(%rsi,%r9,8), %rdx
	movq	(%rdx,%r10,8), %rbp
	movslq	%eax, %r15
	leaq	104(%r13), %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_94:                               # %.lr.ph497.us.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_95 Depth 2
	leaq	(%r14,%rsi), %rdx
	movzwl	(%rbp,%rdx,2), %edi
	movq	%rax, %rbx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_95:                               #   Parent Loop BB2_94 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%di, (%rbx)
	incq	%rdx
	addq	$32, %rbx
	cmpq	%rcx, %rdx
	jl	.LBB2_95
# BB#96:                                # %._crit_edge498.us.us
                                        #   in Loop: Header=BB2_94 Depth=1
	incq	%rsi
	addq	$2, %rax
	cmpq	%r15, %rsi
	jl	.LBB2_94
# BB#97:                                # %._crit_edge502.us-lcssa.us.us
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_102
# BB#98:                                # %.lr.ph501.us.1
	leal	(%r15,%r15), %eax
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%r9,8), %rdx
	movq	(%rdx,%r10,8), %r8
	movslq	%eax, %rbx
	leaq	104(%r13,%r15,2), %rsi
	movq	%r15, %rdi
	.p2align	4, 0x90
.LBB2_99:                               # %.lr.ph497.us.us.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_100 Depth 2
	leaq	(%r14,%rdi), %rax
	movzwl	(%r8,%rax,2), %ebp
	movq	%rsi, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_100:                              #   Parent Loop BB2_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%bp, (%rax)
	incq	%rdx
	addq	$32, %rax
	cmpq	%rcx, %rdx
	jl	.LBB2_100
# BB#101:                               # %._crit_edge498.us.us.1
                                        #   in Loop: Header=BB2_99 Depth=1
	incq	%rdi
	addq	$2, %rsi
	cmpq	%rbx, %rdi
	jl	.LBB2_99
.LBB2_102:                              # %.us-lcssa505.us
	cmpl	$1, 48(%rsp)            # 4-byte Folded Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	jle	.LBB2_209
# BB#103:                               # %.split504.us.preheader.1
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jl	.LBB2_209
# BB#104:                               # %.lr.ph501.us.1615
	leal	(%rcx,%rcx), %eax
	movslq	%eax, %rbp
	movq	(%rdx,%r9,8), %rax
	movq	(%rax,%r10,8), %r11
	movq	%rcx, %r8
	shlq	$5, %r8
	leaq	104(%r13,%r8), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_105:                              # %.lr.ph497.us.us.1617
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_106 Depth 2
	leaq	(%r14,%rdi), %rax
	movzwl	(%r11,%rax,2), %ebx
	movq	%rsi, %rax
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB2_106:                              #   Parent Loop BB2_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%bx, (%rax)
	incq	%rdx
	addq	$32, %rax
	cmpq	%rbp, %rdx
	jl	.LBB2_106
# BB#107:                               # %._crit_edge498.us.us.1621
                                        #   in Loop: Header=BB2_105 Depth=1
	incq	%rdi
	addq	$2, %rsi
	cmpq	%r15, %rdi
	jl	.LBB2_105
# BB#108:                               # %._crit_edge502.us-lcssa.us.us.1623
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	jl	.LBB2_209
# BB#109:                               # %.lr.ph501.us.1.1
	leal	(%r15,%r15), %eax
	movq	(%rdx,%r9,8), %rdx
	movq	(%rdx,%r10,8), %r9
	movslq	%eax, %rdi
	leaq	(%r8,%r15,2), %rax
	leaq	104(%r13,%rax), %rbx
	.p2align	4, 0x90
.LBB2_110:                              # %.lr.ph497.us.us.1.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_111 Depth 2
	leaq	(%r14,%r15), %rax
	movzwl	(%r9,%rax,2), %esi
	movq	%rbx, %rax
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB2_111:                              #   Parent Loop BB2_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%si, (%rax)
	incq	%rdx
	addq	$32, %rax
	cmpq	%rbp, %rdx
	jl	.LBB2_111
# BB#112:                               # %._crit_edge498.us.us.1.1
                                        #   in Loop: Header=BB2_110 Depth=1
	incq	%r15
	addq	$2, %rbx
	cmpq	%rdi, %r15
	jl	.LBB2_110
	jmp	.LBB2_209
.LBB2_113:
	testl	%ebp, %ebp
	je	.LBB2_117
# BB#114:
	testl	%ecx, %ecx
	je	.LBB2_117
# BB#115:
	testl	%edx, %edx
	je	.LBB2_117
# BB#116:
	testl	%r9d, %r9d
	jne	.LBB2_118
.LBB2_117:
	movl	$.L.str.13, %edi
	movl	$-1, %esi
	callq	error
	movq	32(%rsp), %rsi          # 8-byte Reload
.LBB2_118:
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, %r11d
	shrl	$31, %r11d
	addl	%ecx, %r11d
	sarl	%r11d
	movslq	60(%rsp), %rax          # 4-byte Folded Reload
	movq	(%rsi,%rax,8), %r12
	movslq	140(%rsp), %rax
	movq	(%r12,%rax,8), %r15
	movl	136(%rsp), %eax
	leal	-1(%rcx,%rax), %edx
	movslq	%edx, %rdx
	movzwl	(%r15,%rdx,2), %r13d
	movslq	196(%rsp), %rdx
	movq	(%r12,%rdx,8), %rdx
	movslq	192(%rsp), %rsi
	movzwl	(%rdx,%rsi,2), %r8d
	movq	%r13, 104(%rsp)         # 8-byte Spill
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	subl	%r8d, %r13d
	imull	%r11d, %r13d
	cmpl	$4, %ecx
	jl	.LBB2_197
# BB#119:                               # %.lr.ph472
	leal	-1(%r11), %edx
	leal	(%rax,%r11), %esi
	movslq	%esi, %r9
	movslq	%edx, %r14
	testq	%r14, %r14
	movl	$1, %r10d
	cmovgq	%r14, %r10
	cmpq	$4, %r10
	jb	.LBB2_194
# BB#186:                               # %min.iters.checked751
	movq	%rbx, %rcx
	addq	$12, %rcx
	andq	%r10, %rcx
	je	.LBB2_194
# BB#187:                               # %vector.scevcheck
	testq	%r14, %r14
	movl	$1, %edx
	cmovgq	%r14, %rdx
	decq	%rdx
	leal	-2(%rax,%r11), %eax
	movl	%eax, %esi
	subl	%edx, %esi
	cmpl	%eax, %esi
	jg	.LBB2_194
# BB#188:                               # %vector.scevcheck
	shrq	$32, %rdx
	jne	.LBB2_194
# BB#189:                               # %vector.body746.preheader
	movd	%r13d, %xmm0
	leaq	(%r15,%r9,2), %rdi
	movl	$1, %edx
	movd	%rdx, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movaps	.LCPI2_0(%rip), %xmm2   # xmm2 = [2,3]
	pxor	%xmm3, %xmm3
	movdqa	.LCPI2_1(%rip), %xmm8   # xmm8 = [1,1,1,1]
	movdqa	.LCPI2_2(%rip), %xmm5   # xmm5 = [4,4]
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB2_190:                              # %vector.body746
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %xmm6           # xmm6 = mem[0],zero
	punpcklwd	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0],xmm6[1],xmm3[1],xmm6[2],xmm3[2],xmm6[3],xmm3[3]
	cltq
	movq	-6(%r15,%rax,2), %xmm7  # xmm7 = mem[0],zero
	pshuflw	$27, %xmm7, %xmm7       # xmm7 = xmm7[3,2,1,0,4,5,6,7]
	punpcklwd	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1],xmm7[2],xmm3[2],xmm7[3],xmm3[3]
	psubd	%xmm7, %xmm6
	movdqa	%xmm1, %xmm7
	shufps	$136, %xmm2, %xmm7      # xmm7 = xmm7[0,2],xmm2[0,2]
	paddd	%xmm8, %xmm7
	pshufd	$245, %xmm6, %xmm4      # xmm4 = xmm6[1,1,3,3]
	pmuludq	%xmm7, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm7, %xmm7      # xmm7 = xmm7[1,1,3,3]
	pmuludq	%xmm4, %xmm7
	pshufd	$232, %xmm7, %xmm4      # xmm4 = xmm7[0,2,2,3]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	paddd	%xmm6, %xmm0
	paddq	%xmm5, %xmm1
	paddq	%xmm5, %xmm2
	addq	$8, %rdi
	addl	$-4, %eax
	addq	$-4, %rsi
	jne	.LBB2_190
# BB#191:                               # %middle.block747
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %r13d
	cmpq	%rcx, %r10
	jne	.LBB2_195
	jmp	.LBB2_197
.LBB2_121:
	movl	$.L.str.16, %edi
	movl	$600, %esi              # imm = 0x258
	callq	error
	jmp	.LBB2_209
.LBB2_122:
	testl	%r9d, %r9d
	je	.LBB2_124
# BB#123:
	testl	%ebp, %ebp
	jne	.LBB2_125
.LBB2_124:
	movl	$.L.str.14, %edi
	movl	$-1, %esi
	callq	error
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
.LBB2_125:                              # %.preheader440
	cmpl	$2, %r8d
	jl	.LBB2_209
# BB#126:                               # %.split.us.preheader
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jl	.LBB2_209
# BB#127:                               # %.lr.ph481.us.us.preheader
	movl	%r8d, %eax
	shrl	$31, %eax
	addl	%r8d, %eax
	sarl	%eax
	movslq	60(%rsp), %rdi          # 4-byte Folded Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%edx, %ecx
	shrl	$31, %ecx
	addl	%edx, %ecx
	sarl	%ecx
	movslq	%eax, %r14
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	(%rsi,%rdi,8), %r15
	movslq	%ecx, %rcx
	testq	%rcx, %rcx
	movl	$1, %r8d
	cmovgq	%rcx, %r8
	movq	%r13, %rax
	movq	%r8, %r13
	movabsq	$9223372036854775792, %rdx # imm = 0x7FFFFFFFFFFFFFF0
	andq	%rdx, %r13
	leaq	-16(%r13), %r9
	movl	%r9d, %r11d
	shrl	$4, %r11d
	incl	%r11d
	andl	$3, %r11d
	leaq	120(%rax), %rbp
	leaq	104(%rax), %rax
	movq	%r11, %r10
	negq	%r10
	xorl	%r12d, %r12d
	jmp	.LBB2_132
.LBB2_128:                              #   in Loop: Header=BB2_132 Depth=1
	xorl	%edi, %edi
.LBB2_129:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_132 Depth=1
	cmpq	$48, %r9
	jb	.LBB2_131
	.p2align	4, 0x90
.LBB2_130:                              # %vector.body
                                        #   Parent Loop BB2_132 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rbp,%rdi,2)
	movdqu	%xmm0, (%rbp,%rdi,2)
	movdqu	%xmm0, 16(%rbp,%rdi,2)
	movdqu	%xmm0, 32(%rbp,%rdi,2)
	movdqu	%xmm0, 48(%rbp,%rdi,2)
	movdqu	%xmm0, 64(%rbp,%rdi,2)
	movdqu	%xmm0, 80(%rbp,%rdi,2)
	movdqu	%xmm0, 96(%rbp,%rdi,2)
	addq	$64, %rdi
	cmpq	%rdi, %r13
	jne	.LBB2_130
.LBB2_131:                              # %middle.block
                                        #   in Loop: Header=BB2_132 Depth=1
	cmpq	%r13, %r8
	movq	%r13, %rdi
	jne	.LBB2_138
	jmp	.LBB2_140
	.p2align	4, 0x90
.LBB2_132:                              # %.lr.ph481.us.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_136 Depth 2
                                        #     Child Loop BB2_130 Depth 2
                                        #     Child Loop BB2_139 Depth 2
	incq	%r12
	leaq	(%r12,%r12,2), %rdx
	movslq	196(%rsp,%rdx,8), %rsi
	movq	(%r15,%rsi,8), %rsi
	movslq	192(%rsp,%rdx,8), %rdx
	movw	(%rsi,%rdx,2), %si
	cmpq	$15, %r8
	jbe	.LBB2_137
# BB#133:                               # %min.iters.checked
                                        #   in Loop: Header=BB2_132 Depth=1
	testq	%r13, %r13
	je	.LBB2_137
# BB#134:                               # %vector.ph
                                        #   in Loop: Header=BB2_132 Depth=1
	testq	%r11, %r11
	movd	%esi, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	je	.LBB2_128
# BB#135:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB2_132 Depth=1
	movq	%r10, %rbx
	movq	%rbp, %rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_136:                              # %vector.body.prol
                                        #   Parent Loop BB2_132 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$16, %rdi
	addq	$32, %rdx
	incq	%rbx
	jne	.LBB2_136
	jmp	.LBB2_129
	.p2align	4, 0x90
.LBB2_137:                              #   in Loop: Header=BB2_132 Depth=1
	xorl	%edi, %edi
.LBB2_138:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_132 Depth=1
	leaq	(%rax,%rdi,2), %rdx
	.p2align	4, 0x90
.LBB2_139:                              # %scalar.ph
                                        #   Parent Loop BB2_132 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%si, (%rdx)
	incq	%rdi
	addq	$2, %rdx
	cmpq	%rcx, %rdi
	jl	.LBB2_139
.LBB2_140:                              # %..loopexit439_crit_edge.us.us
                                        #   in Loop: Header=BB2_132 Depth=1
	addq	$32, %rbp
	addq	$32, %rax
	cmpq	%r14, %r12
	jl	.LBB2_132
# BB#141:                               # %._crit_edge485.us
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_155
# BB#142:                               # %.lr.ph481.us.us.preheader.1
	leal	(%rcx,%rcx), %eax
	movslq	%eax, %rdi
	leaq	1(%rcx), %r13
	cmpq	%rdi, %r13
	cmovlq	%rdi, %r13
	subq	%rcx, %r13
	leaq	-16(%r13), %r9
	movl	%r9d, %r12d
	shrl	$4, %r12d
	incl	%r12d
	movq	%r13, %rbp
	andq	$-16, %rbp
	leaq	(%rcx,%rbp), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	andl	$3, %r12d
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	120(%rax,%rcx,2), %r10
	movq	%r12, %rdx
	negq	%rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	leaq	104(%rax), %rdx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_143:                              # %.lr.ph481.us.us.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_147 Depth 2
                                        #     Child Loop BB2_150 Depth 2
                                        #     Child Loop BB2_153 Depth 2
	incq	%r8
	cmpq	$16, %r13
	leaq	(%r8,%r8,2), %rax
	movslq	196(%rsp,%rax,8), %rsi
	movq	(%r15,%rsi,8), %rsi
	movslq	192(%rsp,%rax,8), %rax
	movw	(%rsi,%rax,2), %r11w
	movq	%rcx, %rsi
	jb	.LBB2_152
# BB#144:                               # %min.iters.checked689
                                        #   in Loop: Header=BB2_143 Depth=1
	testq	%rbp, %rbp
	movq	%rcx, %rsi
	je	.LBB2_152
# BB#145:                               # %vector.ph693
                                        #   in Loop: Header=BB2_143 Depth=1
	testq	%r12, %r12
	movd	%r11d, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	je	.LBB2_148
# BB#146:                               # %vector.body684.prol.preheader
                                        #   in Loop: Header=BB2_143 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r10, %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_147:                              # %vector.body684.prol
                                        #   Parent Loop BB2_143 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rax)
	movdqu	%xmm0, (%rax)
	addq	$16, %rsi
	addq	$32, %rax
	incq	%rbx
	jne	.LBB2_147
	jmp	.LBB2_149
.LBB2_148:                              #   in Loop: Header=BB2_143 Depth=1
	xorl	%esi, %esi
.LBB2_149:                              # %vector.body684.prol.loopexit
                                        #   in Loop: Header=BB2_143 Depth=1
	cmpq	$48, %r9
	jb	.LBB2_151
	.p2align	4, 0x90
.LBB2_150:                              # %vector.body684
                                        #   Parent Loop BB2_143 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%r10,%rsi,2)
	movdqu	%xmm0, (%r10,%rsi,2)
	movdqu	%xmm0, 16(%r10,%rsi,2)
	movdqu	%xmm0, 32(%r10,%rsi,2)
	movdqu	%xmm0, 48(%r10,%rsi,2)
	movdqu	%xmm0, 64(%r10,%rsi,2)
	movdqu	%xmm0, 80(%r10,%rsi,2)
	movdqu	%xmm0, 96(%r10,%rsi,2)
	addq	$64, %rsi
	cmpq	%rsi, %rbp
	jne	.LBB2_150
.LBB2_151:                              # %middle.block685
                                        #   in Loop: Header=BB2_143 Depth=1
	cmpq	%rbp, %r13
	movq	(%rsp), %rsi            # 8-byte Reload
	je	.LBB2_154
	.p2align	4, 0x90
.LBB2_152:                              # %scalar.ph686.preheader
                                        #   in Loop: Header=BB2_143 Depth=1
	leaq	(%rdx,%rsi,2), %rax
	.p2align	4, 0x90
.LBB2_153:                              # %scalar.ph686
                                        #   Parent Loop BB2_143 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%r11w, (%rax)
	incq	%rsi
	addq	$2, %rax
	cmpq	%rdi, %rsi
	jl	.LBB2_153
.LBB2_154:                              # %..loopexit439_crit_edge.us.us.1
                                        #   in Loop: Header=BB2_143 Depth=1
	addq	$32, %r10
	addq	$32, %rdx
	cmpq	%r14, %r8
	jl	.LBB2_143
.LBB2_155:                              # %.us-lcssa.us
	cmpl	$1, 48(%rsp)            # 4-byte Folded Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	jle	.LBB2_209
# BB#156:                               # %.split.us.preheader.1
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jl	.LBB2_209
# BB#157:                               # %.lr.ph481.us.us.preheader.1599
	leal	(%r14,%r14), %eax
	movslq	%eax, %r8
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rsi,%rax,8), %r12
	testq	%rcx, %rcx
	movl	$1, %r15d
	cmovgq	%rcx, %r15
	movabsq	$9223372036854775792, %rax # imm = 0x7FFFFFFFFFFFFFF0
	andq	%r15, %rax
	movq	%rax, %r9
	leaq	-16(%rax), %r10
	movl	%r10d, %r11d
	shrl	$4, %r11d
	incl	%r11d
	andl	$3, %r11d
	movq	%r14, %rax
	shlq	$5, %rax
	leaq	120(%rdx,%rax), %rbx
	movq	%r11, %rsi
	negq	%rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	104(%rdx,%rax), %rdi
	movq	%r14, %r13
	.p2align	4, 0x90
.LBB2_158:                              # %.lr.ph481.us.us.1602
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_163 Depth 2
                                        #     Child Loop BB2_167 Depth 2
                                        #     Child Loop BB2_170 Depth 2
	incq	%r13
	cmpq	$16, %r15
	leaq	(%r13,%r13,2), %rax
	movslq	196(%rsp,%rax,8), %rdx
	movq	(%r12,%rdx,8), %rdx
	movslq	192(%rsp,%rax,8), %rax
	movw	(%rdx,%rax,2), %si
	jae	.LBB2_160
# BB#159:                               #   in Loop: Header=BB2_158 Depth=1
	xorl	%edx, %edx
	jmp	.LBB2_169
	.p2align	4, 0x90
.LBB2_160:                              # %min.iters.checked709
                                        #   in Loop: Header=BB2_158 Depth=1
	testq	%r9, %r9
	je	.LBB2_164
# BB#161:                               # %vector.ph713
                                        #   in Loop: Header=BB2_158 Depth=1
	testq	%r11, %r11
	movd	%esi, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	je	.LBB2_165
# BB#162:                               # %vector.body704.prol.preheader
                                        #   in Loop: Header=BB2_158 Depth=1
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbx, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_163:                              # %vector.body704.prol
                                        #   Parent Loop BB2_158 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rax)
	movdqu	%xmm0, (%rax)
	addq	$16, %rdx
	addq	$32, %rax
	incq	%rbp
	jne	.LBB2_163
	jmp	.LBB2_166
.LBB2_164:                              #   in Loop: Header=BB2_158 Depth=1
	xorl	%edx, %edx
	jmp	.LBB2_169
.LBB2_165:                              #   in Loop: Header=BB2_158 Depth=1
	xorl	%edx, %edx
.LBB2_166:                              # %vector.body704.prol.loopexit
                                        #   in Loop: Header=BB2_158 Depth=1
	cmpq	$48, %r10
	movq	%r9, %rax
	jb	.LBB2_168
	.p2align	4, 0x90
.LBB2_167:                              # %vector.body704
                                        #   Parent Loop BB2_158 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rbx,%rdx,2)
	movdqu	%xmm0, (%rbx,%rdx,2)
	movdqu	%xmm0, 16(%rbx,%rdx,2)
	movdqu	%xmm0, 32(%rbx,%rdx,2)
	movdqu	%xmm0, 48(%rbx,%rdx,2)
	movdqu	%xmm0, 64(%rbx,%rdx,2)
	movdqu	%xmm0, 80(%rbx,%rdx,2)
	movdqu	%xmm0, 96(%rbx,%rdx,2)
	addq	$64, %rdx
	cmpq	%rdx, %rax
	jne	.LBB2_167
.LBB2_168:                              # %middle.block705
                                        #   in Loop: Header=BB2_158 Depth=1
	cmpq	%rax, %r15
	movq	%rax, %rdx
	je	.LBB2_171
	.p2align	4, 0x90
.LBB2_169:                              # %scalar.ph706.preheader
                                        #   in Loop: Header=BB2_158 Depth=1
	leaq	(%rdi,%rdx,2), %rax
	.p2align	4, 0x90
.LBB2_170:                              # %scalar.ph706
                                        #   Parent Loop BB2_158 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%si, (%rax)
	incq	%rdx
	addq	$2, %rax
	cmpq	%rcx, %rdx
	jl	.LBB2_170
.LBB2_171:                              # %..loopexit439_crit_edge.us.us.1605
                                        #   in Loop: Header=BB2_158 Depth=1
	addq	$32, %rbx
	addq	$32, %rdi
	cmpq	%r8, %r13
	jl	.LBB2_158
# BB#172:                               # %._crit_edge485.us.1607
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	jl	.LBB2_209
# BB#173:                               # %.lr.ph481.us.us.preheader.1.1
	leal	(%rcx,%rcx), %eax
	movslq	%eax, %rbp
	leaq	1(%rcx), %r13
	cmpq	%rbp, %r13
	cmovlq	%rbp, %r13
	subq	%rcx, %r13
	leaq	-16(%r13), %r9
	movl	%r9d, %r10d
	shrl	$4, %r10d
	incl	%r10d
	movq	%r13, %rbx
	andq	$-16, %rbx
	leaq	(%rcx,%rbx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	andl	$3, %r10d
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,2), %rax
	leaq	120(%rdx,%rax), %r11
	movq	%r10, %rax
	negq	%rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	104(%rdx,%rsi), %rdi
	.p2align	4, 0x90
.LBB2_174:                              # %.lr.ph481.us.us.1.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_178 Depth 2
                                        #     Child Loop BB2_181 Depth 2
                                        #     Child Loop BB2_184 Depth 2
	incq	%r14
	cmpq	$16, %r13
	leaq	(%r14,%r14,2), %rax
	movslq	196(%rsp,%rax,8), %rdx
	movq	(%r12,%rdx,8), %rdx
	movslq	192(%rsp,%rax,8), %rax
	movw	(%rdx,%rax,2), %si
	movq	%rcx, %rdx
	jb	.LBB2_183
# BB#175:                               # %min.iters.checked729
                                        #   in Loop: Header=BB2_174 Depth=1
	testq	%rbx, %rbx
	movq	%rcx, %rdx
	je	.LBB2_183
# BB#176:                               # %vector.ph733
                                        #   in Loop: Header=BB2_174 Depth=1
	testq	%r10, %r10
	movd	%esi, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	je	.LBB2_179
# BB#177:                               # %vector.body724.prol.preheader
                                        #   in Loop: Header=BB2_174 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%r11, %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_178:                              # %vector.body724.prol
                                        #   Parent Loop BB2_174 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rax)
	movdqu	%xmm0, (%rax)
	addq	$16, %rdx
	addq	$32, %rax
	incq	%r15
	jne	.LBB2_178
	jmp	.LBB2_180
.LBB2_179:                              #   in Loop: Header=BB2_174 Depth=1
	xorl	%edx, %edx
.LBB2_180:                              # %vector.body724.prol.loopexit
                                        #   in Loop: Header=BB2_174 Depth=1
	cmpq	$48, %r9
	jb	.LBB2_182
	.p2align	4, 0x90
.LBB2_181:                              # %vector.body724
                                        #   Parent Loop BB2_174 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%r11,%rdx,2)
	movdqu	%xmm0, (%r11,%rdx,2)
	movdqu	%xmm0, 16(%r11,%rdx,2)
	movdqu	%xmm0, 32(%r11,%rdx,2)
	movdqu	%xmm0, 48(%r11,%rdx,2)
	movdqu	%xmm0, 64(%r11,%rdx,2)
	movdqu	%xmm0, 80(%r11,%rdx,2)
	movdqu	%xmm0, 96(%r11,%rdx,2)
	addq	$64, %rdx
	cmpq	%rdx, %rbx
	jne	.LBB2_181
.LBB2_182:                              # %middle.block725
                                        #   in Loop: Header=BB2_174 Depth=1
	cmpq	%rbx, %r13
	movq	(%rsp), %rdx            # 8-byte Reload
	je	.LBB2_185
.LBB2_183:                              # %scalar.ph726.preheader
                                        #   in Loop: Header=BB2_174 Depth=1
	leaq	(%rdi,%rdx,2), %rax
	.p2align	4, 0x90
.LBB2_184:                              # %scalar.ph726
                                        #   Parent Loop BB2_174 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%si, (%rax)
	incq	%rdx
	addq	$2, %rax
	cmpq	%rbp, %rdx
	jl	.LBB2_184
.LBB2_185:                              # %..loopexit439_crit_edge.us.us.1.1
                                        #   in Loop: Header=BB2_174 Depth=1
	addq	$32, %r11
	addq	$32, %rdi
	cmpq	%r8, %r14
	jl	.LBB2_174
	jmp	.LBB2_209
.LBB2_194:
	xorl	%ecx, %ecx
.LBB2_195:                              # %scalar.ph748.preheader
	movl	$-2, %eax
	subl	%ecx, %eax
	addl	%r9d, %eax
	addq	%rcx, %r9
	leaq	(%r15,%r9,2), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_196:                              # %scalar.ph748
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdx,%rsi,2), %ebx
	cltq
	movzwl	(%r15,%rax,2), %ebp
	subl	%ebp, %ebx
	leal	1(%rcx,%rsi), %ebp
	imull	%ebx, %ebp
	addl	%ebp, %r13d
	decl	%eax
	leaq	1(%rcx,%rsi), %rbp
	incq	%rsi
	cmpq	%r14, %rbp
	jl	.LBB2_196
.LBB2_197:                              # %._crit_edge473
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r14d
	shrl	$31, %r14d
	addl	%ecx, %r14d
	sarl	%r14d
	leaq	(%rcx,%rcx,2), %rax
	movslq	196(%rsp,%rax,8), %rdx
	movq	(%r12,%rdx,8), %rdx
	movslq	192(%rsp,%rax,8), %rax
	movzwl	(%rdx,%rax,2), %r9d
	movl	%r9d, %esi
	subl	%r8d, %esi
	imull	%r14d, %esi
	cmpl	$4, %ecx
	jl	.LBB2_200
# BB#198:                               # %.lr.ph467
	leal	-1(%r14), %eax
	leal	1(%r14), %edx
	movslq	%eax, %r8
	movslq	%edx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	196(%rsp,%rax,8), %rdx
	leaq	148(%rsp,%rax,8), %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_199:                              # =>This Inner Loop Header: Depth=1
	incq	%rax
	movslq	(%rdx), %rcx
	movq	(%r12,%rcx,8), %rcx
	movslq	-4(%rdx), %rbp
	movzwl	(%rcx,%rbp,2), %ecx
	movslq	(%rbx), %rbp
	movq	(%r12,%rbp,8), %rbp
	movslq	-4(%rbx), %rdi
	movzwl	(%rbp,%rdi,2), %edi
	subl	%edi, %ecx
	movl	%eax, %edi
	imull	%ecx, %edi
	addl	%edi, %esi
	addq	$24, %rdx
	addq	$-24, %rbx
	cmpq	%rax, %r8
	jg	.LBB2_199
.LBB2_200:                              # %._crit_edge468
	movq	8(%rsp), %rbp           # 8-byte Reload
	cmpl	$8, %ebp
	movl	$17, %eax
	movl	$5, %edx
	movl	$5, %edi
	cmovel	%eax, %edi
	setne	%cl
	imull	%edi, %r13d
	leal	(%r13,%rbp,2), %edi
	addb	$5, %cl
	sarl	%cl, %edi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rdi          # 8-byte Reload
	cmpl	$8, %edi
	cmovel	%eax, %edx
	setne	%cl
	imull	%esi, %edx
	leal	(%rdx,%rdi,2), %r10d
	addb	$5, %cl
	sarl	%cl, %r10d
	testl	%edi, %edi
	movq	64(%rsp), %rbx          # 8-byte Reload
	jle	.LBB2_209
# BB#201:                               # %.preheader.lr.ph
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB2_209
# BB#202:                               # %.preheader.us.preheader
	movq	104(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r9), %eax
	shll	$4, %eax
	movl	$1, %esi
	movl	$1, %edx
	subl	%r11d, %edx
	subl	%r14d, %esi
	addl	$16, %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	%edi, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movl	%edx, %eax
	movq	40(%rsp), %rdx          # 8-byte Reload
	imull	%edx, %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%r10d, %eax
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	imull	%esi, %eax
	shll	$4, %r9d
	addl	%eax, %r9d
	shll	$4, %ecx
	leal	16(%rcx,%r9), %r8d
	leal	(%rdx,%rdx), %ecx
	leaq	106(%rbx), %r15
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_203:                              # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_207 Depth 2
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	jne	.LBB2_205
# BB#204:                               #   in Loop: Header=BB2_203 Depth=1
	xorl	%eax, %eax
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB2_206
	jmp	.LBB2_208
	.p2align	4, 0x90
.LBB2_205:                              #   in Loop: Header=BB2_203 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r12), %eax
	imull	%r10d, %eax
	addl	88(%rsp), %eax          # 4-byte Folded Reload
	movl	5904(%rbx), %edx
	addl	32(%rsp), %eax          # 4-byte Folded Reload
	sarl	$5, %eax
	cmovsl	%esi, %eax
	cmpl	%edx, %eax
	cmovlw	%ax, %dx
	movq	%r12, %rax
	shlq	$5, %rax
	movw	%dx, 104(%rbx,%rax)
	movl	$1, %eax
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB2_208
.LBB2_206:                              # %.preheader.us.new
                                        #   in Loop: Header=BB2_203 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	subq	%rax, %rdx
	movq	72(%rsp), %r9           # 8-byte Reload
	leal	1(%r9,%rax), %edi
	movq	40(%rsp), %rbp          # 8-byte Reload
	imull	%ebp, %edi
	addl	%r8d, %edi
	leal	(%r9,%rax), %r9d
	imull	%ebp, %r9d
	addl	%r8d, %r9d
	leaq	(%r15,%rax,2), %r11
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_207:                              #   Parent Loop BB2_203 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	5904(%rbx), %eax
	leal	(%r9,%r14), %r13d
	sarl	$5, %r13d
	cmovsl	%esi, %r13d
	cmpl	%eax, %r13d
	cmovlw	%r13w, %ax
	movq	64(%rsp), %rbx          # 8-byte Reload
	movw	%ax, -2(%r11)
	movl	5904(%rbx), %eax
	leal	(%rdi,%r14), %ebp
	sarl	$5, %ebp
	cmovsl	%esi, %ebp
	cmpl	%eax, %ebp
	cmovlw	%bp, %ax
	movw	%ax, (%r11)
	addl	%ecx, %r14d
	addq	$4, %r11
	addq	$-2, %rdx
	jne	.LBB2_207
.LBB2_208:                              # %._crit_edge.us
                                        #   in Loop: Header=BB2_203 Depth=1
	incq	%r12
	addl	%r10d, %r8d
	addq	$32, %r15
	cmpq	(%rsp), %r12            # 8-byte Folded Reload
	jne	.LBB2_203
.LBB2_209:                              # %.loopexit
	addq	$664, %rsp              # imm = 0x298
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	intrapred_chroma, .Lfunc_end2-intrapred_chroma
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_31
	.quad	.LBB2_122
	.quad	.LBB2_89
	.quad	.LBB2_113
.LJTI2_1:
	.quad	.LBB2_36
	.quad	.LBB2_45
	.quad	.LBB2_50
	.quad	.LBB2_54
.LJTI2_2:
	.quad	.LBB2_77
	.quad	.LBB2_122
	.quad	.LBB2_89
	.quad	.LBB2_113

	.text
	.globl	itrans
	.p2align	4, 0x90
	.type	itrans,@function
itrans:                                 # @itrans
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -48
.Lcfi45:
	.cfi_offset %r12, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	28(%rdi), %eax
	addl	5884(%rdi), %eax
	jne	.LBB3_1
# BB#5:
	leaq	5904(%rdi), %rax
	leaq	5900(%rdi), %rbp
	testl	%r9d, %r9d
	cmoveq	%rbp, %rax
	movl	(%rax), %r15d
	cmpl	$1, 5920(%rdi)
	jne	.LBB3_2
# BB#6:                                 # %.preheader123
	movslq	%ecx, %rax
	movslq	%r8d, %rcx
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	leaq	(%rax,%rax,2), %rax
	shlq	$8, %rax
	shlq	$6, %rcx
	addq	%rax, %rcx
	leaq	2420(%rdi,%rcx), %rcx
	shlq	$5, %rdx
	leaq	(%rdx,%rsi,2), %rax
	leaq	110(%rdi,%rax), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_7:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-6(%rax,%rsi,2), %ebp
	addl	-12(%rcx,%rsi), %ebp
	cmovsl	%edx, %ebp
	cmpl	%r15d, %ebp
	cmovgl	%r15d, %ebp
	movl	%ebp, 1384(%rdi,%rsi,4)
	movzwl	-4(%rax,%rsi,2), %ebp
	addl	-8(%rcx,%rsi), %ebp
	cmovsl	%edx, %ebp
	cmpl	%r15d, %ebp
	cmovgl	%r15d, %ebp
	movl	%ebp, 1388(%rdi,%rsi,4)
	movzwl	-2(%rax,%rsi,2), %ebp
	addl	-4(%rcx,%rsi), %ebp
	cmovsl	%edx, %ebp
	cmpl	%r15d, %ebp
	cmovgl	%r15d, %ebp
	movl	%ebp, 1392(%rdi,%rsi,4)
	movzwl	(%rax,%rsi,2), %ebp
	addl	(%rcx,%rsi), %ebp
	cmovsl	%edx, %ebp
	cmpl	%r15d, %ebp
	cmovgl	%r15d, %ebp
	movl	%ebp, 1396(%rdi,%rsi,4)
	addq	$16, %rsi
	cmpq	$64, %rsi
	jne	.LBB3_7
	jmp	.LBB3_8
.LBB3_1:                                # %.thread
	leaq	5904(%rdi), %rax
	leaq	5900(%rdi), %rbp
	testl	%r9d, %r9d
	cmoveq	%rbp, %rax
	movl	(%rax), %r15d
.LBB3_2:                                # %.preheader126
	movslq	%ecx, %rax
	movslq	%r8d, %r10
	leaq	(%rax,%rax,2), %rcx
	shlq	$8, %rcx
	addq	%rdi, %rcx
	shlq	$6, %r10
	movl	2408(%r10,%rcx), %eax
	movl	2412(%r10,%rcx), %r8d
	movl	2416(%r10,%rcx), %r9d
	movl	2420(%r10,%rcx), %ebp
	leal	(%r9,%rax), %r11d
	subl	%r9d, %eax
	movl	%r8d, %ebx
	sarl	%ebx
	subl	%ebp, %ebx
	sarl	%ebp
	addl	%r8d, %ebp
	leal	(%rbp,%r11), %r8d
	movl	%r8d, 1384(%rdi)
	subl	%ebp, %r11d
	movl	%r11d, 1396(%rdi)
	leal	(%rbx,%rax), %ebp
	movl	%ebp, 1388(%rdi)
	subl	%ebx, %eax
	movl	%eax, 1392(%rdi)
	movl	2424(%r10,%rcx), %eax
	movl	2428(%r10,%rcx), %r9d
	movl	2432(%r10,%rcx), %r11d
	movl	2436(%r10,%rcx), %ebp
	leal	(%r11,%rax), %r14d
	subl	%r11d, %eax
	movl	%r9d, %ebx
	sarl	%ebx
	subl	%ebp, %ebx
	sarl	%ebp
	addl	%r9d, %ebp
	leal	(%rbp,%r14), %r9d
	movl	%r9d, 1448(%rdi)
	subl	%ebp, %r14d
	movl	%r14d, 1460(%rdi)
	leal	(%rbx,%rax), %ebp
	movl	%ebp, 1452(%rdi)
	subl	%ebx, %eax
	movl	%eax, 1456(%rdi)
	movl	2440(%r10,%rcx), %eax
	movl	2444(%r10,%rcx), %r11d
	movl	2448(%r10,%rcx), %r14d
	movl	2452(%r10,%rcx), %ebp
	leal	(%r14,%rax), %r12d
	subl	%r14d, %eax
	movl	%r11d, %ebx
	sarl	%ebx
	subl	%ebp, %ebx
	sarl	%ebp
	addl	%r11d, %ebp
	leal	(%rbp,%r12), %r11d
	movl	%r11d, 1512(%rdi)
	subl	%ebp, %r12d
	movl	%r12d, 1524(%rdi)
	leal	(%rbx,%rax), %ebp
	movl	%ebp, 1516(%rdi)
	subl	%ebx, %eax
	movl	%eax, 1520(%rdi)
	movl	2456(%r10,%rcx), %eax
	movl	2460(%r10,%rcx), %r14d
	movl	2464(%r10,%rcx), %r12d
	movl	2468(%r10,%rcx), %ebx
	leal	(%r12,%rax), %r10d
	subl	%r12d, %eax
	movl	%r14d, %ebp
	sarl	%ebp
	subl	%ebx, %ebp
	sarl	%ebx
	addl	%r14d, %ebx
	leal	(%rbx,%r10), %r12d
	movl	%r12d, 1576(%rdi)
	subl	%ebx, %r10d
	movl	%r10d, 1588(%rdi)
	leal	(%rbp,%rax), %ebx
	movl	%ebx, 1580(%rdi)
	subl	%ebp, %eax
	movl	%eax, 1584(%rdi)
	movslq	%edx, %rax
	movslq	%esi, %rdx
	shlq	$5, %rax
	leaq	(%rax,%rdx,2), %r14
	xorl	%r10d, %r10d
	xorl	%esi, %esi
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_4:                                # %._crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	1388(%rdi,%rsi,2), %r8d
	movl	1452(%rdi,%rsi,2), %r9d
	movl	1516(%rdi,%rsi,2), %r11d
	movl	1580(%rdi,%rsi,2), %r12d
	addq	$2, %rsi
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movl	%r8d, %ebp
	subl	%r11d, %ebp
	movl	%r9d, %eax
	sarl	%eax
	subl	%r12d, %eax
	sarl	%r12d
	addl	%r9d, %r12d
	leaq	(%rdi,%rsi), %r9
	movzwl	104(%r14,%r9), %ecx
	shll	$6, %ecx
	leal	32(%r11,%r8), %edx
	leal	(%rdx,%r12), %ebx
	addl	%ecx, %ebx
	sarl	$6, %ebx
	cmovsl	%r10d, %ebx
	cmpl	%r15d, %ebx
	cmovgl	%r15d, %ebx
	movl	%ebx, 1384(%rdi,%rsi,2)
	movzwl	136(%r14,%r9), %ecx
	shll	$6, %ecx
	leal	32(%rbp), %ebx
	leal	32(%rbp,%rax), %ebp
	addl	%ecx, %ebp
	sarl	$6, %ebp
	cmovsl	%r10d, %ebp
	cmpl	%r15d, %ebp
	cmovgl	%r15d, %ebp
	movl	%ebp, 1448(%rdi,%rsi,2)
	movzwl	168(%r14,%r9), %ecx
	shll	$6, %ecx
	subl	%eax, %ebx
	addl	%ecx, %ebx
	sarl	$6, %ebx
	cmovsl	%r10d, %ebx
	cmpl	%r15d, %ebx
	cmovgl	%r15d, %ebx
	movl	%ebx, 1512(%rdi,%rsi,2)
	movzwl	200(%r14,%r9), %eax
	shll	$6, %eax
	subl	%r12d, %edx
	addl	%eax, %edx
	sarl	$6, %edx
	cmovsl	%r10d, %edx
	cmpl	%r15d, %edx
	cmovgl	%r15d, %edx
	movl	%edx, 1576(%rdi,%rsi,2)
	cmpq	$6, %rsi
	jne	.LBB3_4
.LBB3_8:                                # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	itrans, .Lfunc_end3-itrans
	.cfi_endproc

	.globl	AssignQuantParam
	.p2align	4, 0x90
	.type	AssignQuantParam,@function
AssignQuantParam:                       # @AssignQuantParam
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 16
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	20(%rbx), %ecx
	movl	36(%rsi), %eax
	testl	%ecx, %ecx
	je	.LBB4_3
# BB#1:
	testl	%eax, %eax
	jne	.LBB4_4
# BB#2:
	movb	$1, %cl
	xorl	%eax, %eax
	cmpl	$0, 24(%rbx)
	jne	.LBB4_42
	jmp	.LBB4_44
.LBB4_3:
	testl	%eax, %eax
	je	.LBB4_88
.LBB4_4:                                # %.thread
	cmpl	$0, 40(%rsi)
	je	.LBB4_7
# BB#5:
	cmpl	$0, 968(%rsi)
	je	.LBB4_8
.LBB4_7:
	movl	$quant_intra_default, %edx
	jmp	.LBB4_9
.LBB4_8:
	leaq	72(%rsi), %rdx
.LBB4_9:
	movq	%rdx, qmatrix(%rip)
	cmpl	$0, 44(%rsi)
	je	.LBB4_13
# BB#10:
	cmpl	$0, 972(%rsi)
	je	.LBB4_12
# BB#11:
	movl	$quant_intra_default, %edx
	jmp	.LBB4_13
.LBB4_12:
	leaq	136(%rsi), %rdx
.LBB4_13:
	movq	%rdx, qmatrix+8(%rip)
	cmpl	$0, 48(%rsi)
	je	.LBB4_17
# BB#14:
	cmpl	$0, 976(%rsi)
	je	.LBB4_16
# BB#15:
	movl	$quant_intra_default, %edx
	jmp	.LBB4_17
.LBB4_16:
	leaq	200(%rsi), %rdx
.LBB4_17:
	movq	%rdx, qmatrix+16(%rip)
	cmpl	$0, 52(%rsi)
	je	.LBB4_20
# BB#18:
	cmpl	$0, 980(%rsi)
	je	.LBB4_21
.LBB4_20:
	movl	$quant_inter_default, %edx
	jmp	.LBB4_22
.LBB4_21:
	leaq	264(%rsi), %rdx
.LBB4_22:
	movq	%rdx, qmatrix+24(%rip)
	cmpl	$0, 56(%rsi)
	je	.LBB4_26
# BB#23:
	cmpl	$0, 984(%rsi)
	je	.LBB4_25
# BB#24:
	movl	$quant_inter_default, %edx
	jmp	.LBB4_26
.LBB4_25:
	leaq	328(%rsi), %rdx
.LBB4_26:
	movq	%rdx, qmatrix+32(%rip)
	cmpl	$0, 60(%rsi)
	je	.LBB4_30
# BB#27:
	cmpl	$0, 988(%rsi)
	je	.LBB4_29
# BB#28:
	movl	$quant_inter_default, %edx
	jmp	.LBB4_30
.LBB4_29:
	leaq	392(%rsi), %rdx
.LBB4_30:
	movq	%rdx, qmatrix+40(%rip)
	cmpl	$0, 64(%rsi)
	je	.LBB4_33
# BB#31:
	cmpl	$0, 992(%rsi)
	je	.LBB4_34
.LBB4_33:
	movl	$quant8_intra_default, %edx
	jmp	.LBB4_35
.LBB4_34:
	leaq	456(%rsi), %rdx
.LBB4_35:
	movq	%rdx, qmatrix+48(%rip)
	cmpl	$0, 68(%rsi)
	je	.LBB4_38
# BB#36:
	cmpl	$0, 996(%rsi)
	je	.LBB4_39
.LBB4_38:
	movl	$quant8_inter_default, %esi
	jmp	.LBB4_40
.LBB4_39:
	addq	$712, %rsi              # imm = 0x2C8
.LBB4_40:                               # %.loopexit103
	testl	%ecx, %ecx
	movq	%rsi, qmatrix+56(%rip)
	je	.LBB4_89
# BB#41:
	xorl	%ecx, %ecx
	cmpl	$0, 24(%rbx)
	je	.LBB4_44
.LBB4_42:
	cmpl	$0, 952(%rbx)
	je	.LBB4_46
# BB#43:
	movl	$quant_intra_default, %ecx
	jmp	.LBB4_47
.LBB4_44:
	testb	%cl, %cl
	je	.LBB4_48
# BB#45:
	movl	$quant_intra_default, %ecx
	jmp	.LBB4_47
.LBB4_46:
	leaq	56(%rbx), %rcx
.LBB4_47:                               # %.sink.split27
	movq	%rcx, qmatrix(%rip)
.LBB4_48:
	cmpl	$0, 28(%rbx)
	je	.LBB4_51
# BB#49:
	cmpl	$0, 956(%rbx)
	je	.LBB4_52
# BB#50:
	movl	$quant_intra_default, %ecx
	jmp	.LBB4_53
.LBB4_51:
	movq	qmatrix(%rip), %rcx
	jmp	.LBB4_53
.LBB4_52:
	leaq	120(%rbx), %rcx
.LBB4_53:
	movq	%rcx, qmatrix+8(%rip)
	cmpl	$0, 32(%rbx)
	je	.LBB4_56
# BB#54:
	cmpl	$0, 960(%rbx)
	je	.LBB4_57
# BB#55:
	movl	$quant_intra_default, %ecx
	jmp	.LBB4_58
.LBB4_56:
	movq	qmatrix+8(%rip), %rcx
	jmp	.LBB4_58
.LBB4_57:
	leaq	184(%rbx), %rcx
.LBB4_58:
	movq	%rcx, qmatrix+16(%rip)
	cmpl	$0, 36(%rbx)
	je	.LBB4_61
# BB#59:
	cmpl	$0, 964(%rbx)
	je	.LBB4_63
# BB#60:
	movl	$quant_inter_default, %ecx
	jmp	.LBB4_64
.LBB4_61:
	testl	%eax, %eax
	jne	.LBB4_65
# BB#62:
	movl	$quant_inter_default, %ecx
	jmp	.LBB4_64
.LBB4_63:
	leaq	248(%rbx), %rcx
.LBB4_64:                               # %.sink.split27.3
	movq	%rcx, qmatrix+24(%rip)
.LBB4_65:
	cmpl	$0, 40(%rbx)
	je	.LBB4_68
# BB#66:
	cmpl	$0, 968(%rbx)
	je	.LBB4_69
# BB#67:
	movl	$quant_inter_default, %ecx
	jmp	.LBB4_70
.LBB4_68:
	movq	qmatrix+24(%rip), %rcx
	jmp	.LBB4_70
.LBB4_69:
	leaq	312(%rbx), %rcx
.LBB4_70:
	movq	%rcx, qmatrix+32(%rip)
	cmpl	$0, 44(%rbx)
	je	.LBB4_73
# BB#71:
	cmpl	$0, 972(%rbx)
	je	.LBB4_74
# BB#72:
	movl	$quant_inter_default, %ecx
	jmp	.LBB4_75
.LBB4_73:
	movq	qmatrix+32(%rip), %rcx
	jmp	.LBB4_75
.LBB4_74:
	leaq	376(%rbx), %rcx
.LBB4_75:
	movq	%rcx, qmatrix+40(%rip)
	cmpl	$0, 48(%rbx)
	je	.LBB4_78
# BB#76:
	cmpl	$0, 976(%rbx)
	je	.LBB4_80
# BB#77:
	movl	$quant8_intra_default, %ecx
	jmp	.LBB4_81
.LBB4_78:
	testl	%eax, %eax
	jne	.LBB4_82
# BB#79:
	movl	$quant8_intra_default, %ecx
	jmp	.LBB4_81
.LBB4_80:
	leaq	440(%rbx), %rcx
.LBB4_81:                               # %.sink.split34.6
	movq	%rcx, qmatrix+48(%rip)
.LBB4_82:
	cmpl	$0, 52(%rbx)
	je	.LBB4_85
# BB#83:
	cmpl	$0, 980(%rbx)
	jne	.LBB4_86
# BB#87:
	leaq	696(%rbx), %rax
	movq	%rax, qmatrix+56(%rip)
	jmp	.LBB4_89
.LBB4_85:
	testl	%eax, %eax
	jne	.LBB4_89
.LBB4_86:
	movl	$quant8_inter_default, %eax
	movq	%rax, qmatrix+56(%rip)
	jmp	.LBB4_89
.LBB4_88:                               # %.preheader.preheader
	movl	$quant_org, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, qmatrix(%rip)
	movdqa	%xmm0, qmatrix+16(%rip)
	movdqa	%xmm0, qmatrix+32(%rip)
	movl	$quant8_org, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	%xmm0, qmatrix+48(%rip)
.LBB4_89:                               # %.loopexit
	callq	CalculateQuantParam
	cmpl	$0, 16(%rbx)
	je	.LBB4_91
# BB#90:
	popq	%rbx
	jmp	CalculateQuant8Param    # TAILCALL
.LBB4_91:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	AssignQuantParam, .Lfunc_end4-AssignQuantParam
	.cfi_endproc

	.globl	CalculateQuantParam
	.p2align	4, 0x90
	.type	CalculateQuantParam,@function
CalculateQuantParam:                    # @CalculateQuantParam
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 48
.Lcfi56:
	.cfi_offset %rbx, -48
.Lcfi57:
	.cfi_offset %r12, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	qmatrix(%rip), %r8
	movq	qmatrix+8(%rip), %r14
	movq	qmatrix+16(%rip), %r15
	movq	qmatrix+24(%rip), %r12
	movq	qmatrix+32(%rip), %rdi
	movq	qmatrix+40(%rip), %rax
	movl	$dequant_coef+12, %r9d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB5_1:                                # %.preheader59
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_2 Depth 2
	movq	%r10, %rbx
	movq	%r9, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rcx), %ebp
	movl	(%r8,%rdx), %esi
	imull	%ebp, %esi
	movl	%esi, InvLevelScale4x4Luma_Intra(%rbx)
	movl	(%r14,%rdx), %esi
	imull	%ebp, %esi
	movl	%esi, InvLevelScale4x4Chroma_Intra(%rbx)
	movl	(%r15,%rdx), %esi
	imull	%ebp, %esi
	movl	%esi, InvLevelScale4x4Chroma_Intra+384(%rbx)
	movl	(%r12,%rdx), %esi
	imull	%ebp, %esi
	movl	%esi, InvLevelScale4x4Luma_Inter(%rbx)
	movl	(%rdi,%rdx), %esi
	imull	%ebp, %esi
	movl	%esi, InvLevelScale4x4Chroma_Inter(%rbx)
	imull	(%rax,%rdx), %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Inter+384(%rbx)
	movl	-8(%rcx), %esi
	movl	16(%r8,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Luma_Intra+16(%rbx)
	movl	16(%r14,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Intra+16(%rbx)
	movl	16(%r15,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Intra+400(%rbx)
	movl	16(%r12,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Luma_Inter+16(%rbx)
	movl	16(%rdi,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Inter+16(%rbx)
	imull	16(%rax,%rdx), %esi
	movl	%esi, InvLevelScale4x4Chroma_Inter+400(%rbx)
	movl	-4(%rcx), %esi
	movl	32(%r8,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Luma_Intra+32(%rbx)
	movl	32(%r14,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Intra+32(%rbx)
	movl	32(%r15,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Intra+416(%rbx)
	movl	32(%r12,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Luma_Inter+32(%rbx)
	movl	32(%rdi,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Inter+32(%rbx)
	imull	32(%rax,%rdx), %esi
	movl	%esi, InvLevelScale4x4Chroma_Inter+416(%rbx)
	movl	(%rcx), %esi
	movl	48(%r8,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Luma_Intra+48(%rbx)
	movl	48(%r14,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Intra+48(%rbx)
	movl	48(%r15,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Intra+432(%rbx)
	movl	48(%r12,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Luma_Inter+48(%rbx)
	movl	48(%rdi,%rdx), %ebp
	imull	%esi, %ebp
	movl	%ebp, InvLevelScale4x4Chroma_Inter+48(%rbx)
	imull	48(%rax,%rdx), %esi
	movl	%esi, InvLevelScale4x4Chroma_Inter+432(%rbx)
	addq	$4, %rdx
	addq	$16, %rcx
	addq	$4, %rbx
	cmpq	$16, %rdx
	jne	.LBB5_2
# BB#3:                                 #   in Loop: Header=BB5_1 Depth=1
	incq	%r11
	addq	$64, %r9
	addq	$64, %r10
	cmpq	$6, %r11
	jne	.LBB5_1
# BB#4:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	CalculateQuantParam, .Lfunc_end5-CalculateQuantParam
	.cfi_endproc

	.globl	itrans_2
	.p2align	4, 0x90
	.type	itrans_2,@function
itrans_2:                               # @itrans_2
	.cfi_startproc
# BB#0:                                 # %.preheader96
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 40
.Lcfi65:
	.cfi_offset %rbx, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movl	5884(%rdi), %eax
	addl	28(%rdi), %eax
	movslq	%eax, %r8
	imulq	$715827883, %r8, %rcx   # imm = 0x2AAAAAAB
	movq	%rcx, %rax
	shrq	$63, %rax
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	2408(%rdi), %edx
	movl	3176(%rdi), %r9d
	movl	3944(%rdi), %eax
	movl	4712(%rdi), %r11d
	leal	(%rax,%rdx), %r10d
	subl	%eax, %edx
	movl	%r9d, %ebx
	subl	%r11d, %ebx
	addl	%r9d, %r11d
	leal	(%r11,%r10), %eax
	movl	%eax, 2408(%rdi)
	leal	(%rbx,%rdx), %esi
	movl	%esi, 3176(%rdi)
	subl	%ebx, %edx
	movl	%edx, 3944(%rdi)
	subl	%r11d, %r10d
	movl	%r10d, 4712(%rdi)
	movl	2472(%rdi), %edx
	movl	3240(%rdi), %r9d
	movl	4008(%rdi), %r10d
	movl	4776(%rdi), %r14d
	leal	(%r10,%rdx), %r11d
	subl	%r10d, %edx
	movl	%r9d, %ebx
	subl	%r14d, %ebx
	addl	%r9d, %r14d
	leal	(%r14,%r11), %r9d
	movl	%r9d, 2472(%rdi)
	leal	(%rbx,%rdx), %esi
	movl	%esi, 3240(%rdi)
	subl	%ebx, %edx
	movl	%edx, 4008(%rdi)
	subl	%r14d, %r11d
	movl	%r11d, 4776(%rdi)
	movl	2536(%rdi), %edx
	movl	3304(%rdi), %r10d
	movl	4072(%rdi), %r11d
	movl	4840(%rdi), %r15d
	leal	(%r11,%rdx), %r14d
	subl	%r11d, %edx
	movl	%r10d, %ebx
	subl	%r15d, %ebx
	addl	%r10d, %r15d
	leal	(%r15,%r14), %r10d
	movl	%r10d, 2536(%rdi)
	leal	(%rbx,%rdx), %esi
	movl	%esi, 3304(%rdi)
	subl	%ebx, %edx
	movl	%edx, 4072(%rdi)
	subl	%r15d, %r14d
	movl	%r14d, 4840(%rdi)
	movl	2600(%rdi), %edx
	movl	3368(%rdi), %r11d
	movl	4136(%rdi), %r14d
	movl	4904(%rdi), %esi
	leal	(%r14,%rdx), %r15d
	subl	%r14d, %edx
	movl	%r11d, %ebx
	subl	%esi, %ebx
	addl	%r11d, %esi
	leal	(%rsi,%r15), %r11d
	movl	%r11d, 2600(%rdi)
	leal	(%rbx,%rdx), %ebp
	movl	%ebp, 3368(%rdi)
	subl	%ebx, %edx
	movl	%edx, 4136(%rdi)
	subl	%esi, %r15d
	movl	%r15d, 4904(%rdi)
	leal	(%rcx,%rcx), %edx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %r8d
	movslq	%r8d, %rdx
	shlq	$6, %rdx
	leaq	InvLevelScale4x4Luma_Intra(%rdx), %r8
	xorl	%esi, %esi
	jmp	.LBB6_1
	.p2align	4, 0x90
.LBB6_2:                                # %._crit_edge
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	3176(%rdi,%rsi), %eax
	movl	3240(%rdi,%rsi), %r9d
	movl	3304(%rdi,%rsi), %r10d
	movl	3368(%rdi,%rsi), %r11d
	addq	$768, %rsi              # imm = 0x300
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	leal	(%r10,%rax), %edx
	subl	%r10d, %eax
	movl	%r9d, %ebp
	subl	%r11d, %ebp
	addl	%r9d, %r11d
	leal	(%r11,%rdx), %ebx
	imull	(%r8), %ebx
	shll	%cl, %ebx
	addl	$32, %ebx
	sarl	$6, %ebx
	movl	%ebx, 2408(%rdi,%rsi)
	leal	(%rbp,%rax), %ebx
	imull	(%r8), %ebx
	shll	%cl, %ebx
	addl	$32, %ebx
	sarl	$6, %ebx
	movl	%ebx, 2472(%rdi,%rsi)
	subl	%ebp, %eax
	imull	(%r8), %eax
	shll	%cl, %eax
	addl	$32, %eax
	sarl	$6, %eax
	movl	%eax, 2536(%rdi,%rsi)
	subl	%r11d, %edx
	imull	(%r8), %edx
	shll	%cl, %edx
	addl	$32, %edx
	sarl	$6, %edx
	movl	%edx, 2600(%rdi,%rsi)
	cmpq	$2304, %rsi             # imm = 0x900
	jne	.LBB6_2
# BB#3:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	itrans_2, .Lfunc_end6-itrans_2
	.cfi_endproc

	.globl	itrans_sp
	.p2align	4, 0x90
	.type	itrans_sp,@function
itrans_sp:                              # @itrans_sp
	.cfi_startproc
# BB#0:                                 # %.preheader226
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi75:
	.cfi_def_cfa_offset 112
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movl	%r8d, -36(%rsp)         # 4-byte Spill
	movl	%ecx, -40(%rsp)         # 4-byte Spill
	movslq	28(%rdi), %rax
	imulq	$715827883, %rax, %r11  # imm = 0x2AAAAAAB
	movq	%rax, %r8
	movq	%r11, %rax
	shrq	$63, %rax
	shrq	$32, %r11
	addl	%eax, %r11d
	movslq	32(%rdi), %rcx
	imulq	$715827883, %rcx, %rax  # imm = 0x2AAAAAAB
	movq	%rcx, %rbx
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	leal	(%rax,%rcx), %r9d
	leal	15(%rax,%rcx), %ecx
	movl	$1, %ebp
	movl	%ecx, -92(%rsp)         # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movslq	%edx, %rax
	shlq	$5, %rax
	addq	%rdi, %rax
	leal	(%r11,%r11), %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %r8d
	movq	%r8, -104(%rsp)         # 8-byte Spill
	leal	(%r9,%r9), %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %ebx
	movq	%rbx, -88(%rsp)         # 8-byte Spill
	movl	%ebp, %r12d
	shrl	$31, %r12d
	addl	%ebp, %r12d
	movslq	%esi, %rbp
	movzwl	104(%rax,%rbp,2), %ebx
	movzwl	106(%rax,%rbp,2), %ecx
	movzwl	108(%rax,%rbp,2), %r10d
	movzwl	110(%rax,%rbp,2), %r15d
	movzwl	136(%rax,%rbp,2), %r8d
	movzwl	138(%rax,%rbp,2), %r13d
	movzwl	140(%rax,%rbp,2), %edx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	movzwl	142(%rax,%rbp,2), %edx
	movq	%rdx, -80(%rsp)         # 8-byte Spill
	movzwl	168(%rax,%rbp,2), %edx
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	movzwl	170(%rax,%rbp,2), %r14d
	movzwl	172(%rax,%rbp,2), %edx
	movq	%rdx, -48(%rsp)         # 8-byte Spill
	movzwl	174(%rax,%rbp,2), %edx
	movq	%rdx, -72(%rsp)         # 8-byte Spill
	movzwl	200(%rax,%rbp,2), %edx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movzwl	202(%rax,%rbp,2), %edx
	movzwl	204(%rax,%rbp,2), %esi
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	movzwl	206(%rax,%rbp,2), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	sarl	%r12d
	cmpl	$4, 44(%rdi)
	movq	%r9, %rax
	cmovel	%eax, %r11d
	movq	-104(%rsp), %rax        # 8-byte Reload
	cmovel	-88(%rsp), %eax         # 4-byte Folded Reload
	movq	%rax, -104(%rsp)        # 8-byte Spill
	leal	(%r15,%rbx), %ebp
	subl	%r15d, %ebx
	leal	(%r10,%rcx), %eax
	subl	%r10d, %ecx
	leal	(%rax,%rbp), %r15d
	subl	%eax, %ebp
	leal	(%rcx,%rbx,2), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	addl	%ecx, %ecx
	subl	%ecx, %ebx
	movl	%ebx, 16(%rsp)
	movq	-80(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r8), %ebx
	subl	%eax, %r8d
	movq	-64(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r13), %ecx
	subl	%eax, %r13d
	leal	(%rcx,%rbx), %esi
	subl	%ecx, %ebx
	movq	%r8, %rax
	leal	(%r13,%rax,2), %r8d
	addl	%r13d, %r13d
	subl	%r13d, %eax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movq	-112(%rsp), %rcx        # 8-byte Reload
	movq	-72(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx), %r10d
	subl	%eax, %ecx
	movq	-48(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %r13d
	subl	%eax, %r14d
	leal	(%r13,%r10), %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	subl	%r13d, %r10d
	movq	%r10, -48(%rsp)         # 8-byte Spill
	leal	(%r14,%rcx,2), %eax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	addl	%r14d, %r14d
	subl	%r14d, %ecx
	movq	%rcx, -112(%rsp)        # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	leal	(%r10,%rcx), %r14d
	subl	%r10d, %ecx
	movq	%rcx, %r10
	movq	-56(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rdx), %r13d
	subl	%ecx, %edx
	leal	(%r13,%r14), %ecx
	subl	%r13d, %r14d
	leal	(%rdx,%r10,2), %eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	addl	%edx, %edx
	subl	%edx, %r10d
	leal	(%rcx,%r15), %edx
	subl	%ecx, %r15d
	movq	-80(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rsi), %ecx
	subl	%eax, %esi
	leal	(%rcx,%rdx), %r13d
	movl	%r13d, -32(%rsp)
	subl	%ecx, %edx
	movl	%edx, -24(%rsp)
	leal	(%rsi,%r15,2), %ecx
	movl	%ecx, -28(%rsp)
	addl	%esi, %esi
	subl	%esi, %r15d
	movl	%r15d, -20(%rsp)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	-56(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rcx), %eax
	subl	%edx, %ecx
	movq	-72(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r8), %r15d
	subl	%edx, %r8d
	leal	(%r15,%rax), %edx
	movl	%edx, -16(%rsp)
	subl	%r15d, %eax
	movl	%eax, -8(%rsp)
	leal	(%r8,%rcx,2), %eax
	movl	%eax, -12(%rsp)
	addl	%r8d, %r8d
	subl	%r8d, %ecx
	movl	%ecx, -4(%rsp)
	leal	(%r14,%rbp), %eax
	subl	%r14d, %ebp
	movq	-48(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rbx), %ecx
	subl	%edx, %ebx
	leal	(%rcx,%rax), %edx
	movl	%edx, (%rsp)
	subl	%ecx, %eax
	movl	%eax, 8(%rsp)
	leal	(%rbx,%rbp,2), %eax
	movl	%eax, 4(%rsp)
	addl	%ebx, %ebx
	subl	%ebx, %ebp
	movl	%ebp, 12(%rsp)
	movl	16(%rsp), %eax
	leal	(%r10,%rax), %ecx
	subl	%r10d, %eax
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movq	-64(%rsp), %rbp         # 8-byte Reload
	leal	(%rsi,%rbp), %edx
	subl	%esi, %ebp
	leal	(%rdx,%rcx), %esi
	movl	%esi, 16(%rsp)
	movl	%ecx, -128(%rsp)
	movl	%eax, -116(%rsp)
	movl	%edx, -124(%rsp)
	subl	%edx, %ecx
	movl	%ecx, 24(%rsp)
	movq	%rbp, %rdx
	leal	(%rdx,%rax,2), %ecx
	movl	%ecx, 20(%rsp)
	movl	%edx, -120(%rsp)
	addl	%edx, %edx
	subl	%edx, %eax
	movl	%eax, 28(%rsp)
	movslq	-40(%rsp), %rax         # 4-byte Folded Reload
	movslq	-36(%rsp), %rdx         # 4-byte Folded Reload
	movslq	-104(%rsp), %rcx        # 4-byte Folded Reload
	movslq	-88(%rsp), %r13         # 4-byte Folded Reload
	leaq	(%rax,%rax,2), %rax
	shlq	$8, %rax
	shlq	$6, %rdx
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	movq	%rax, -88(%rsp)         # 8-byte Spill
	leaq	(%rax,%rdx), %rax
	leaq	2408(%rdi,%rax), %r15
	shlq	$6, %rcx
	leaq	dequant_coef(%rcx), %r8
	shlq	$6, %r13
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB7_1:                                # %.preheader220
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
	xorl	%esi, %esi
	movq	%r15, -104(%rsp)        # 8-byte Spill
	jmp	.LBB7_2
.LBB7_5:                                #   in Loop: Header=BB7_2 Depth=2
	imull	%eax, %r14d
	imull	A(%rsi,%r10,4), %r14d
	movl	%r11d, %ecx
	shll	%cl, %r14d
	sarl	$6, %r14d
	leaq	-32(%rsp,%rsi), %rax
	addl	(%rax,%r10,4), %r14d
	movl	%r14d, %eax
	negl	%eax
	cmovll	%r14d, %eax
	leaq	(%r13,%rsi), %rdx
	imull	quant_coef(%rdx,%r10,4), %eax
	addl	%r12d, %eax
	movl	-92(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%r14d, %r14d
	cmovnsl	%ecx, %eax
	imull	dequant_coef(%rdx,%r10,4), %eax
	movl	%r9d, %ecx
	shll	%cl, %eax
	jmp	.LBB7_6
	.p2align	4, 0x90
.LBB7_2:                                #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r15), %eax
	movl	%r11d, %ecx
	sarl	%cl, %eax
	leaq	(%r8,%rsi), %rcx
	movl	(%rcx,%r10,4), %r14d
	cltd
	idivl	%r14d
	movl	%eax, (%r15)
	cmpl	$0, 36(%rdi)
	jne	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=2
	cmpl	$4, 44(%rdi)
	jne	.LBB7_5
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=2
	leaq	-32(%rsp,%rsi), %rcx
	movl	(%rcx,%r10,4), %ebp
	movl	%ebp, %edx
	negl	%edx
	cmovll	%ebp, %edx
	leaq	(%r13,%rsi), %rbx
	imull	quant_coef(%rbx,%r10,4), %edx
	addl	%r12d, %edx
	movl	-92(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%ebp, %ebp
	cmovnsl	%ecx, %edx
	addl	%eax, %edx
	movl	%edx, %eax
	negl	%eax
	cmovll	%edx, %eax
	imull	dequant_coef(%rbx,%r10,4), %eax
	movl	%r9d, %ecx
	shll	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%edx, %edx
	cmovnsl	%ecx, %eax
.LBB7_6:                                #   in Loop: Header=BB7_2 Depth=2
	movl	%eax, (%r15)
	addq	$4, %r15
	addq	$16, %rsi
	cmpq	$64, %rsi
	jne	.LBB7_2
# BB#7:                                 #   in Loop: Header=BB7_1 Depth=1
	incq	%r10
	movq	-104(%rsp), %r15        # 8-byte Reload
	addq	$16, %r15
	cmpq	$4, %r10
	jne	.LBB7_1
# BB#8:                                 # %.preheader218
	movq	-88(%rsp), %rbx         # 8-byte Reload
	addq	%rdi, %rbx
	movq	-112(%rsp), %r8         # 8-byte Reload
	movups	2408(%r8,%rbx), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%rbp,%rsi), %edx
	movl	%edx, 1384(%rdi)
	subl	%ebp, %esi
	movl	%esi, 1396(%rdi)
	leal	(%rax,%rcx), %edx
	movl	%edx, 1388(%rdi)
	subl	%eax, %ecx
	movl	%ecx, 1392(%rdi)
	movups	2424(%r8,%rbx), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%rbp,%rsi), %edx
	movl	%edx, 1448(%rdi)
	subl	%ebp, %esi
	movl	%esi, 1460(%rdi)
	leal	(%rax,%rcx), %edx
	movl	%edx, 1452(%rdi)
	subl	%eax, %ecx
	movl	%ecx, 1456(%rdi)
	movups	2440(%r8,%rbx), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%rbp,%rsi), %edx
	movl	%edx, 1512(%rdi)
	subl	%ebp, %esi
	movl	%esi, 1524(%rdi)
	leal	(%rax,%rcx), %edx
	movl	%edx, 1516(%rdi)
	subl	%eax, %ecx
	movl	%ecx, 1520(%rdi)
	movups	2456(%r8,%rbx), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%rbp,%rsi), %edx
	movl	%edx, 1576(%rdi)
	subl	%ebp, %esi
	movl	%esi, 1588(%rdi)
	leal	(%rax,%rcx), %edx
	movl	%edx, 1580(%rdi)
	subl	%eax, %ecx
	movl	%ecx, 1584(%rdi)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_9:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	1384(%rdi,%rcx,4), %r14d
	movl	1448(%rdi,%rcx,4), %r9d
	movl	1512(%rdi,%rcx,4), %r11d
	movl	1576(%rdi,%rcx,4), %r10d
	movl	%r14d, %r15d
	subl	%r11d, %r15d
	movl	%r9d, %eax
	sarl	%eax
	subl	%r10d, %eax
	movl	%r10d, %esi
	sarl	%esi
	addl	%r9d, %esi
	movl	5900(%rdi), %ebx
	leal	32(%r11,%r14), %edx
	leal	(%rdx,%rsi), %ebp
	sarl	$6, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1384(%rdi,%rcx,4)
	movl	5900(%rdi), %ebx
	subl	%esi, %edx
	sarl	$6, %edx
	cmovsl	%r8d, %edx
	cmpl	%ebx, %edx
	cmovgl	%ebx, %edx
	movl	%edx, 1576(%rdi,%rcx,4)
	movl	5900(%rdi), %edx
	leal	32(%r15), %esi
	leal	32(%r15,%rax), %ebx
	sarl	$6, %ebx
	cmovsl	%r8d, %ebx
	cmpl	%edx, %ebx
	cmovgl	%edx, %ebx
	movl	%ebx, 1448(%rdi,%rcx,4)
	movl	5900(%rdi), %edx
	subl	%eax, %esi
	sarl	$6, %esi
	cmovsl	%r8d, %esi
	cmpl	%edx, %esi
	cmovgl	%edx, %esi
	movl	%esi, 1512(%rdi,%rcx,4)
	incq	%rcx
	cmpq	$4, %rcx
	jne	.LBB7_9
# BB#10:
	movl	%r14d, -128(%rsp)
	movl	%r9d, -124(%rsp)
	movl	%r11d, -120(%rsp)
	movl	%r10d, -116(%rsp)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	itrans_sp, .Lfunc_end7-itrans_sp
	.cfi_endproc

	.globl	copyblock_sp
	.p2align	4, 0x90
	.type	copyblock_sp,@function
copyblock_sp:                           # @copyblock_sp
	.cfi_startproc
# BB#0:                                 # %.preheader163
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 128
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movslq	32(%rdi), %rcx
	imulq	$715827883, %rcx, %rax  # imm = 0x2AAAAAAB
	movq	%rcx, %r10
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	leal	(%rax,%rcx), %r8d
	leal	15(%rax,%rcx), %r9d
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movslq	%esi, %rcx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movslq	%edx, %rbx
	shlq	$5, %rbx
	addq	%rdi, %rbx
	movzwl	104(%rbx,%rcx,2), %ebp
	movzwl	106(%rbx,%rcx,2), %eax
	movzwl	108(%rbx,%rcx,2), %r13d
	movzwl	110(%rbx,%rcx,2), %r14d
	movzwl	136(%rbx,%rcx,2), %edx
	movq	%rdx, -72(%rsp)         # 8-byte Spill
	movzwl	138(%rbx,%rcx,2), %r15d
	movzwl	140(%rbx,%rcx,2), %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movzwl	142(%rbx,%rcx,2), %edx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movzwl	168(%rbx,%rcx,2), %edx
	movq	%rdx, -80(%rsp)         # 8-byte Spill
	movzwl	170(%rbx,%rcx,2), %esi
	movzwl	172(%rbx,%rcx,2), %edx
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	movzwl	174(%rbx,%rcx,2), %edx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	movzwl	200(%rbx,%rcx,2), %edx
	movq	%rdx, -88(%rsp)         # 8-byte Spill
	movzwl	202(%rbx,%rcx,2), %r11d
	movzwl	204(%rbx,%rcx,2), %edx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movzwl	206(%rbx,%rcx,2), %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leal	(%r8,%r8), %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %r10d
	movq	%r10, 48(%rsp)          # 8-byte Spill
	movl	$1, %ebx
	movl	%r9d, %ecx
	shll	%cl, %ebx
	movl	%ebx, %r12d
	shrl	$31, %r12d
	addl	%ebx, %r12d
	leal	(%r14,%rbp), %ebx
	subl	%r14d, %ebp
	leal	(%r13,%rax), %ecx
	subl	%r13d, %eax
	leal	(%rcx,%rbx), %r10d
	subl	%ecx, %ebx
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	leal	(%rax,%rbp,2), %r14d
	addl	%eax, %eax
	subl	%eax, %ebp
	movl	%ebp, -16(%rsp)
	movq	-72(%rsp), %rax         # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	(%rcx,%rax), %ebp
	subl	%ecx, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%r15), %ecx
	subl	%edx, %r15d
	leal	(%rcx,%rbp), %edx
	subl	%ecx, %ebp
	movq	%rax, %rcx
	leal	(%r15,%rcx,2), %ebx
	addl	%r15d, %r15d
	subl	%r15d, %ecx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	movq	-80(%rsp), %rcx         # 8-byte Reload
	movq	-104(%rsp), %r15        # 8-byte Reload
	leal	(%r15,%rcx), %eax
	subl	%r15d, %ecx
	movq	-96(%rsp), %r13         # 8-byte Reload
	leal	(%r13,%rsi), %r15d
	subl	%r13d, %esi
	leal	(%r15,%rax), %r13d
	subl	%r15d, %eax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leal	(%rsi,%rcx,2), %eax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	addl	%esi, %esi
	subl	%esi, %ecx
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	movq	-88(%rsp), %rcx         # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movq	%rcx, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%r11), %r15d
	subl	%ecx, %r11d
	leal	(%r15,%rsi), %ecx
	subl	%r15d, %esi
	leal	(%r11,%rax,2), %r15d
	addl	%r11d, %r11d
	subl	%r11d, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	leal	(%rcx,%r10), %r11d
	subl	%ecx, %r10d
	leal	(%r13,%rdx), %ecx
	subl	%r13d, %edx
	leal	(%rcx,%r11), %r13d
	subl	%ecx, %r11d
	movl	%r13d, -64(%rsp)
	movl	%r11d, -56(%rsp)
	leal	(%rdx,%r10,2), %ecx
	movl	%ecx, -60(%rsp)
	addl	%edx, %edx
	subl	%edx, %r10d
	movl	%r10d, -52(%rsp)
	leal	(%r15,%r14), %eax
	subl	%r15d, %r14d
	movq	-104(%rsp), %rdx        # 8-byte Reload
	leal	(%rdx,%rbx), %ecx
	subl	%edx, %ebx
	leal	(%rcx,%rax), %r10d
	subl	%ecx, %eax
	movl	%r10d, -48(%rsp)
	movl	%eax, -40(%rsp)
	leal	(%rbx,%r14,2), %eax
	movl	%eax, -44(%rsp)
	addl	%ebx, %ebx
	subl	%ebx, %r14d
	movl	%r14d, -36(%rsp)
	movq	40(%rsp), %rcx          # 8-byte Reload
	leal	(%rsi,%rcx), %eax
	subl	%esi, %ecx
	movq	%rcx, %rsi
	movq	-96(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rbp), %ecx
	subl	%edx, %ebp
	leal	(%rcx,%rax), %r14d
	subl	%ecx, %eax
	movl	%r14d, -32(%rsp)
	movl	%eax, -24(%rsp)
	leal	(%rbp,%rsi,2), %eax
	movl	%eax, -28(%rsp)
	addl	%ebp, %ebp
	subl	%ebp, %esi
	movl	%esi, -20(%rsp)
	movl	-16(%rsp), %eax
	movq	-88(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	movl	%ecx, -128(%rsp)
	subl	%edx, %eax
	movl	%eax, -116(%rsp)
	movq	-72(%rsp), %rsi         # 8-byte Reload
	movq	-80(%rsp), %rbp         # 8-byte Reload
	leal	(%rbp,%rsi), %edx
	movl	%edx, -124(%rsp)
	subl	%ebp, %esi
	leal	(%rdx,%rcx), %r15d
	subl	%edx, %ecx
	movl	%r15d, -16(%rsp)
	movl	%ecx, -8(%rsp)
	leal	(%rsi,%rax,2), %ecx
	movl	%ecx, -12(%rsp)
	sarl	%r12d
	movl	%esi, -120(%rsp)
	addl	%esi, %esi
	subl	%esi, %eax
	movl	%eax, -4(%rsp)
	movslq	48(%rsp), %rbx          # 4-byte Folded Reload
	shlq	$6, %rbx
	leaq	1396(%rdi), %rax
	xorl	%ebp, %ebp
	jmp	.LBB8_1
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader157..preheader157_crit_edge
                                        #   in Loop: Header=BB8_1 Depth=1
	movl	-60(%rsp,%rbp), %r13d
	movl	-44(%rsp,%rbp), %r10d
	movl	-28(%rsp,%rbp), %r14d
	movl	-12(%rsp,%rbp), %r15d
	addq	$4, %rbp
	addq	$64, %rax
.LBB8_1:                                # %.preheader157
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %edx
	negl	%edx
	cmovll	%r13d, %edx
	imull	quant_coef(%rbx,%rbp), %edx
	addl	%r12d, %edx
	movl	%r9d, %ecx
	sarl	%cl, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%r13d, %r13d
	cmovnsl	%ecx, %edx
	imull	dequant_coef(%rbx,%rbp), %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	movl	%edx, -12(%rax)
	movl	%r10d, %edx
	negl	%edx
	cmovll	%r10d, %edx
	imull	quant_coef+16(%rbx,%rbp), %edx
	addl	%r12d, %edx
	movl	%r9d, %ecx
	sarl	%cl, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%r10d, %r10d
	cmovnsl	%ecx, %edx
	imull	dequant_coef+16(%rbx,%rbp), %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	movl	%edx, -8(%rax)
	movl	%r14d, %edx
	negl	%edx
	cmovll	%r14d, %edx
	imull	quant_coef+32(%rbx,%rbp), %edx
	addl	%r12d, %edx
	movl	%r9d, %ecx
	sarl	%cl, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%r14d, %r14d
	cmovnsl	%ecx, %edx
	imull	dequant_coef+32(%rbx,%rbp), %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	movl	%edx, -4(%rax)
	movl	%r15d, %edx
	negl	%edx
	cmovll	%r15d, %edx
	imull	quant_coef+48(%rbx,%rbp), %edx
	addl	%r12d, %edx
	movl	%r9d, %ecx
	sarl	%cl, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%r15d, %r15d
	cmovnsl	%ecx, %edx
	imull	dequant_coef+48(%rbx,%rbp), %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	movl	%edx, (%rax)
	cmpq	$12, %rbp
	jne	.LBB8_2
# BB#3:                                 # %.preheader155
	movups	1384(%rdi), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%rbp,%rsi), %edx
	movl	%edx, 1384(%rdi)
	subl	%ebp, %esi
	movl	%esi, 1396(%rdi)
	leal	(%rax,%rcx), %edx
	movl	%edx, 1388(%rdi)
	subl	%eax, %ecx
	movl	%ecx, 1392(%rdi)
	movups	1448(%rdi), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%rbp,%rsi), %edx
	movl	%edx, 1448(%rdi)
	subl	%ebp, %esi
	movl	%esi, 1460(%rdi)
	leal	(%rax,%rcx), %edx
	movl	%edx, 1452(%rdi)
	subl	%eax, %ecx
	movl	%ecx, 1456(%rdi)
	movups	1512(%rdi), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%rbp,%rsi), %edx
	movl	%edx, 1512(%rdi)
	subl	%ebp, %esi
	movl	%esi, 1524(%rdi)
	leal	(%rax,%rcx), %edx
	movl	%edx, 1516(%rdi)
	subl	%eax, %ecx
	movl	%ecx, 1520(%rdi)
	movups	1576(%rdi), %xmm0
	movaps	%xmm0, -128(%rsp)
	movl	-120(%rsp), %eax
	movl	-128(%rsp), %ecx
	movl	-124(%rsp), %edx
	leal	(%rax,%rcx), %esi
	subl	%eax, %ecx
	movl	%edx, %eax
	sarl	%eax
	movl	-116(%rsp), %ebp
	subl	%ebp, %eax
	sarl	%ebp
	addl	%edx, %ebp
	leal	(%rbp,%rsi), %edx
	movl	%edx, 1576(%rdi)
	subl	%ebp, %esi
	movl	%esi, 1588(%rdi)
	leal	(%rax,%rcx), %edx
	movl	%edx, 1580(%rdi)
	subl	%eax, %ecx
	movl	%ecx, 1584(%rdi)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_4:                                # %.preheader153
                                        # =>This Inner Loop Header: Depth=1
	movl	1384(%rdi,%rcx,4), %r14d
	movl	1448(%rdi,%rcx,4), %r9d
	movl	1512(%rdi,%rcx,4), %r11d
	movl	1576(%rdi,%rcx,4), %r10d
	movl	%r14d, %r15d
	subl	%r11d, %r15d
	movl	%r9d, %eax
	sarl	%eax
	subl	%r10d, %eax
	movl	%r10d, %esi
	sarl	%esi
	addl	%r9d, %esi
	movl	5900(%rdi), %ebx
	leal	32(%r11,%r14), %edx
	leal	(%rdx,%rsi), %ebp
	sarl	$6, %ebp
	cmovsl	%r8d, %ebp
	cmpl	%ebx, %ebp
	cmovgl	%ebx, %ebp
	movl	%ebp, 1384(%rdi,%rcx,4)
	movl	5900(%rdi), %ebx
	subl	%esi, %edx
	sarl	$6, %edx
	cmovsl	%r8d, %edx
	cmpl	%ebx, %edx
	cmovgl	%ebx, %edx
	movl	%edx, 1576(%rdi,%rcx,4)
	movl	5900(%rdi), %edx
	leal	32(%r15), %esi
	leal	32(%r15,%rax), %ebx
	sarl	$6, %ebx
	cmovsl	%r8d, %ebx
	cmpl	%edx, %ebx
	cmovgl	%edx, %ebx
	movl	%ebx, 1448(%rdi,%rcx,4)
	movl	5900(%rdi), %edx
	subl	%eax, %esi
	sarl	$6, %esi
	cmovsl	%r8d, %esi
	cmpl	%edx, %esi
	cmovgl	%edx, %esi
	movl	%esi, 1512(%rdi,%rcx,4)
	incq	%rcx
	cmpq	$4, %rcx
	jne	.LBB8_4
# BB#5:                                 # %.preheader152
	movl	%r14d, -128(%rsp)
	movl	%r9d, -124(%rsp)
	movl	%r11d, -120(%rsp)
	movl	%r10d, -116(%rsp)
	movq	dec_picture(%rip), %rax
	movq	316920(%rax), %r8
	movl	80(%rdi), %r9d
	movl	84(%rdi), %ebx
	movq	64(%rsp), %r10          # 8-byte Reload
	leal	(%r9,%r10), %edx
	movslq	%edx, %rdx
	movq	(%r8,%rdx,8), %rax
	movzwl	1384(%rdi), %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	leal	(%rbx,%rcx), %edx
	movslq	%edx, %rdx
	movw	%si, (%rax,%rdx,2)
	movzwl	1388(%rdi), %ebp
	leal	1(%rbx,%rcx), %esi
	movslq	%esi, %rsi
	movw	%bp, (%rax,%rsi,2)
	movzwl	1392(%rdi), %r11d
	leal	2(%rbx,%rcx), %ebp
	movslq	%ebp, %rbp
	movw	%r11w, (%rax,%rbp,2)
	movzwl	1396(%rdi), %r11d
	leal	3(%rbx,%rcx), %ebx
	movslq	%ebx, %rbx
	movw	%r11w, (%rax,%rbx,2)
	leal	1(%r9,%r10), %eax
	cltq
	movq	(%r8,%rax,8), %rax
	movzwl	1448(%rdi), %ecx
	movw	%cx, (%rax,%rdx,2)
	movzwl	1452(%rdi), %ecx
	movw	%cx, (%rax,%rsi,2)
	movzwl	1456(%rdi), %ecx
	movw	%cx, (%rax,%rbp,2)
	movzwl	1460(%rdi), %ecx
	movw	%cx, (%rax,%rbx,2)
	leal	2(%r9,%r10), %eax
	cltq
	movq	(%r8,%rax,8), %rax
	movzwl	1512(%rdi), %ecx
	movw	%cx, (%rax,%rdx,2)
	movzwl	1516(%rdi), %ecx
	movw	%cx, (%rax,%rsi,2)
	movzwl	1520(%rdi), %ecx
	movw	%cx, (%rax,%rbp,2)
	movzwl	1524(%rdi), %ecx
	movw	%cx, (%rax,%rbx,2)
	leal	3(%r9,%r10), %eax
	cltq
	movq	(%r8,%rax,8), %rax
	movzwl	1576(%rdi), %ecx
	movw	%cx, (%rax,%rdx,2)
	movzwl	1580(%rdi), %ecx
	movw	%cx, (%rax,%rsi,2)
	movzwl	1584(%rdi), %ecx
	movw	%cx, (%rax,%rbp,2)
	movzwl	1588(%rdi), %ecx
	movw	%cx, (%rax,%rbx,2)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	copyblock_sp, .Lfunc_end8-copyblock_sp
	.cfi_endproc

	.globl	itrans_sp_chroma
	.p2align	4, 0x90
	.type	itrans_sp_chroma,@function
itrans_sp_chroma:                       # @itrans_sp_chroma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi101:
	.cfi_def_cfa_offset 320
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movslq	28(%rdi), %rax
	testq	%rax, %rax
	js	.LBB9_2
# BB#1:
	movzbl	QP_SCALE_CR(%rax), %eax
.LBB9_2:                                # %.thread
	movslq	32(%rdi), %rdx
	testq	%rdx, %rdx
	movq	%rdi, -104(%rsp)        # 8-byte Spill
	js	.LBB9_3
# BB#4:
	movzbl	QP_SCALE_CR(%rdx), %r15d
	imull	$171, %r15d, %ebp
	andl	$64512, %ebp            # imm = 0xFC00
	shrl	$10, %ebp
	jmp	.LBB9_5
.LBB9_3:                                # %.thread291
	movslq	%edx, %rcx
	imulq	$715827883, %rcx, %rbp  # imm = 0x2AAAAAAB
	movq	%rbp, %rcx
	shrq	$63, %rcx
	shrq	$32, %rbp
	addl	%ecx, %ebp
	movl	%edx, %r15d
.LBB9_5:
	movslq	%r15d, %rcx
	imulq	$715827883, %rcx, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rcx
	addl	%esi, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,2), %esi
	leal	15(%rbp), %ecx
	movl	$1, %edi
	movl	%ecx, -68(%rsp)         # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rdi        # 8-byte Reload
	cmpl	$4, 44(%rdi)
	cmovel	%edx, %eax
	jne	.LBB9_8
# BB#6:
	testl	%edx, %edx
	js	.LBB9_8
# BB#7:
	movzbl	QP_SCALE_CR(%rdx), %eax
.LBB9_8:                                # %.thread293
	movq	%rbp, -96(%rsp)         # 8-byte Spill
	subl	%esi, %r15d
	movq	-80(%rsp), %rcx         # 8-byte Reload
	sarl	%ecx
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	movslq	%eax, %r8
	imulq	$715827883, %r8, %rdx   # imm = 0x2AAAAAAB
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	addl	%edx, %edx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %r8d
	xorl	%edx, %edx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB9_9:                                # %.preheader313
                                        # =>This Inner Loop Header: Depth=1
	movzwl	104(%rdi,%rdx,8), %esi
	movl	%esi, (%rsp,%rdx)
	movzwl	106(%rdi,%rdx,8), %esi
	movl	%esi, 32(%rsp,%rdx)
	movzwl	108(%rdi,%rdx,8), %esi
	movl	%esi, 64(%rsp,%rdx)
	movzwl	110(%rdi,%rdx,8), %esi
	movl	%esi, 96(%rsp,%rdx)
	movzwl	112(%rdi,%rdx,8), %esi
	movl	%esi, 128(%rsp,%rdx)
	movzwl	114(%rdi,%rdx,8), %esi
	movl	%esi, 160(%rsp,%rdx)
	movzwl	116(%rdi,%rdx,8), %esi
	movl	%esi, 192(%rsp,%rdx)
	movzwl	118(%rdi,%rdx,8), %esi
	movl	%esi, 224(%rsp,%rdx)
	movups	%xmm0, 104(%rdi,%rdx,8)
	addq	$4, %rdx
	cmpq	$32, %rdx
	jne	.LBB9_9
# BB#10:                                # %.preheader312
	cltq
	imulq	$715827883, %rax, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rax
	shrq	$63, %rax
	shrq	$32, %rcx
	addl	%eax, %ecx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	leaq	108(%rsp), %r10
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB9_11:                               # %.preheader311
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_12 Depth 2
	movq	%r10, %rdx
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB9_12:                               # %.preheader309
                                        #   Parent Loop BB9_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rdx), %esi
	movl	-108(%rdx), %ecx
	movl	-76(%rdx), %ebx
	leal	(%rsi,%rcx), %ebp
	subl	%esi, %ecx
	movl	-44(%rdx), %esi
	leal	(%rsi,%rbx), %eax
	subl	%esi, %ebx
	leal	(%rax,%rbp), %esi
	movl	%esi, -108(%rdx)
	subl	%eax, %ebp
	movl	%ebp, -44(%rdx)
	leal	(%rbx,%rcx,2), %eax
	movl	%eax, -76(%rdx)
	addl	%ebx, %ebx
	subl	%ebx, %ecx
	movl	%ecx, -12(%rdx)
	movl	-8(%rdx), %eax
	movl	-104(%rdx), %ecx
	movl	-72(%rdx), %esi
	leal	(%rax,%rcx), %ebx
	subl	%eax, %ecx
	movl	-40(%rdx), %eax
	leal	(%rax,%rsi), %ebp
	subl	%eax, %esi
	leal	(%rbp,%rbx), %eax
	movl	%eax, -104(%rdx)
	subl	%ebp, %ebx
	movl	%ebx, -40(%rdx)
	leal	(%rsi,%rcx,2), %eax
	movl	%eax, -72(%rdx)
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, -8(%rdx)
	movl	-4(%rdx), %eax
	movl	-100(%rdx), %ecx
	movl	-68(%rdx), %esi
	leal	(%rax,%rcx), %ebx
	subl	%eax, %ecx
	movl	-36(%rdx), %eax
	leal	(%rax,%rsi), %ebp
	subl	%eax, %esi
	leal	(%rbp,%rbx), %eax
	movl	%eax, -100(%rdx)
	subl	%ebp, %ebx
	movl	%ebx, -36(%rdx)
	leal	(%rsi,%rcx,2), %eax
	movl	%eax, -68(%rdx)
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, -4(%rdx)
	movl	(%rdx), %eax
	movl	-96(%rdx), %ecx
	movl	-64(%rdx), %esi
	leal	(%rax,%rcx), %ebx
	subl	%eax, %ecx
	movl	-32(%rdx), %eax
	leal	(%rax,%rsi), %ebp
	subl	%eax, %esi
	leal	(%rbp,%rbx), %eax
	movl	%eax, -96(%rdx)
	subl	%ebp, %ebx
	movl	%ebx, -32(%rdx)
	leal	(%rsi,%rcx,2), %eax
	movl	%eax, -64(%rdx)
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, (%rdx)
	movl	-96(%rdx), %eax
	movl	-108(%rdx), %ecx
	movl	-104(%rdx), %esi
	leal	(%rax,%rcx), %ebx
	subl	%eax, %ecx
	movl	-100(%rdx), %eax
	leal	(%rax,%rsi), %ebp
	subl	%eax, %esi
	leal	(%rbp,%rbx), %eax
	movl	%eax, -108(%rdx)
	subl	%ebp, %ebx
	movl	%ebx, -100(%rdx)
	leal	(%rsi,%rcx,2), %eax
	movl	%eax, -104(%rdx)
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, -96(%rdx)
	movl	-64(%rdx), %eax
	movl	-76(%rdx), %ecx
	movl	-72(%rdx), %esi
	leal	(%rax,%rcx), %ebx
	subl	%eax, %ecx
	movl	-68(%rdx), %eax
	leal	(%rax,%rsi), %ebp
	subl	%eax, %esi
	leal	(%rbp,%rbx), %eax
	movl	%eax, -76(%rdx)
	subl	%ebp, %ebx
	movl	%ebx, -68(%rdx)
	leal	(%rsi,%rcx,2), %eax
	movl	%eax, -72(%rdx)
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, -64(%rdx)
	movl	-32(%rdx), %eax
	movl	-44(%rdx), %ecx
	movl	-40(%rdx), %esi
	leal	(%rax,%rcx), %ebx
	subl	%eax, %ecx
	movl	-36(%rdx), %eax
	leal	(%rax,%rsi), %ebp
	subl	%eax, %esi
	leal	(%rbp,%rbx), %eax
	movl	%eax, -44(%rdx)
	subl	%ebp, %ebx
	movl	%ebx, -36(%rdx)
	leal	(%rsi,%rcx,2), %eax
	movl	%eax, -40(%rdx)
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, -32(%rdx)
	movl	(%rdx), %eax
	movl	-12(%rdx), %ecx
	movl	-8(%rdx), %esi
	leal	(%rax,%rcx), %ebx
	subl	%eax, %ecx
	movl	-4(%rdx), %eax
	leal	(%rax,%rsi), %ebp
	subl	%eax, %esi
	leal	(%rbp,%rbx), %eax
	movl	%eax, -12(%rdx)
	subl	%ebp, %ebx
	movl	%ebx, -4(%rdx)
	leal	(%rsi,%rcx,2), %eax
	movl	%eax, -8(%rdx)
	addl	%esi, %esi
	subl	%esi, %ecx
	movl	%ecx, (%rdx)
	addq	$4, %r11
	subq	$-128, %rdx
	cmpq	$5, %r11
	jl	.LBB9_12
# BB#13:                                #   in Loop: Header=BB9_11 Depth=1
	addq	$4, %r9
	addq	$16, %r10
	cmpq	$5, %r9
	jl	.LBB9_11
# BB#14:                                # %.preheader308
	movl	128(%rsp), %eax
	movl	(%rsp), %ecx
	movl	16(%rsp), %edx
	leal	(%rax,%rcx), %esi
	leal	(%rsi,%rdx), %ebp
	movl	144(%rsp), %ebx
	addl	%ebx, %ebp
	movl	%ebp, -128(%rsp)
	subl	%eax, %ecx
	leal	(%rcx,%rdx), %eax
	subl	%ebx, %eax
	movl	%eax, -124(%rsp)
	subl	%edx, %esi
	subl	%ebx, %esi
	movl	%esi, -120(%rsp)
	subl	%edx, %ecx
	addl	%ebx, %ecx
	movl	%ecx, -116(%rsp)
	movl	36(%rdi), %r13d
	movslq	%r8d, %r14
	shlq	$6, %r14
	leaq	dequant_coef(%r14), %rcx
	movslq	%r15d, %rbx
	shlq	$6, %rbx
	leaq	quant_coef(%rbx), %r9
	movq	-80(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax), %r11d
	movq	-96(%rsp), %r10         # 8-byte Reload
	leal	16(%r10), %r15d
	movl	%r12d, -36(%rsp)        # 4-byte Spill
	movslq	%r12d, %r12
	leaq	dequant_coef(%rbx), %rsi
	testl	%r13d, %r13d
	movq	%rsi, -88(%rsp)         # 8-byte Spill
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	je	.LBB9_31
# BB#15:                                # %.preheader308.split.preheader
	movl	-128(%rsp), %ebp
	movl	%ebp, %edx
	negl	%edx
	cmovll	%ebp, %edx
	movl	(%r9), %r8d
	imull	%r8d, %edx
	addl	%r11d, %edx
	movl	%r15d, %ecx
	sarl	%cl, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	leaq	(%r12,%r12,2), %rdx
	shlq	$8, %rdx
	movl	%ecx, %eax
	negl	%eax
	testl	%ebp, %ebp
	cmovnsl	%ecx, %eax
	addl	2664(%rdi,%rdx), %eax
	movl	(%rsi), %r10d
	imull	%r10d, %eax
	movq	-96(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	movl	%eax, -128(%rsp)
	movl	-120(%rsp), %esi
	movl	%esi, %eax
	negl	%eax
	cmovll	%esi, %eax
	imull	%r8d, %eax
	addl	%r11d, %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %ebp
	negl	%ebp
	testl	%esi, %esi
	cmovnsl	%ecx, %ebp
	addl	2728(%rdi,%rdx), %ebp
	imull	%r10d, %ebp
	movq	-96(%rsp), %r10         # 8-byte Reload
	jmp	.LBB9_16
.LBB9_31:                               # %.preheader308.split.us.preheader
	cmpl	$4, 44(%rdi)
	movl	-128(%rsp), %ebp
	jne	.LBB9_30
# BB#32:
	movl	%ebp, %eax
	negl	%eax
	cmovll	%ebp, %eax
	movl	(%r9), %r8d
	imull	%r8d, %eax
	addl	%r11d, %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	leaq	(%r12,%r12,2), %rdx
	shlq	$8, %rdx
	movl	%ecx, %eax
	negl	%eax
	testl	%ebp, %ebp
	cmovnsl	%ecx, %eax
	addl	2664(%rdi,%rdx), %eax
	movl	(%rsi), %edi
	imull	%edi, %eax
	movl	%r10d, %ecx
	shll	%cl, %eax
	movl	%eax, -128(%rsp)
	movl	-120(%rsp), %esi
	movl	%esi, %eax
	negl	%eax
	cmovll	%esi, %eax
	imull	%r8d, %eax
	addl	%r11d, %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %ebp
	negl	%ebp
	testl	%esi, %esi
	cmovnsl	%ecx, %ebp
	movq	-104(%rsp), %rax        # 8-byte Reload
	addl	2728(%rax,%rdx), %ebp
	imull	%edi, %ebp
	movq	-104(%rsp), %rdi        # 8-byte Reload
	jmp	.LBB9_16
.LBB9_30:
	leaq	(%r12,%r12,2), %rdx
	shlq	$8, %rdx
	movq	%rsi, %r8
	movl	(%rcx), %esi
	movl	2664(%rdi,%rdx), %eax
	imull	%esi, %eax
	shll	$4, %eax
	movq	%r12, -56(%rsp)         # 8-byte Spill
	movq	-64(%rsp), %r12         # 8-byte Reload
	movl	%r12d, %ecx
	shll	%cl, %eax
	sarl	$5, %eax
	addl	%ebp, %eax
	movl	%eax, %edi
	negl	%edi
	cmovll	%eax, %edi
	movl	(%r9), %r10d
	imull	%r10d, %edi
	addl	%r11d, %edi
	movl	%r15d, %ecx
	sarl	%cl, %edi
	movl	%edi, %ecx
	negl	%ecx
	cmovll	%edi, %ecx
	movl	%ecx, %edi
	negl	%edi
	testl	%eax, %eax
	cmovnsl	%ecx, %edi
	movl	(%r8), %r8d
	imull	%r8d, %edi
	movq	-96(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edi
	movl	%edi, -128(%rsp)
	movq	-104(%rsp), %rdi        # 8-byte Reload
	imull	2728(%rdi,%rdx), %esi
	shll	$4, %esi
	movl	%r12d, %ecx
	movq	-56(%rsp), %r12         # 8-byte Reload
	shll	%cl, %esi
	sarl	$5, %esi
	addl	-120(%rsp), %esi
	movl	%esi, %eax
	negl	%eax
	cmovll	%esi, %eax
	imull	%r10d, %eax
	movq	-96(%rsp), %r10         # 8-byte Reload
	addl	%r11d, %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %ebp
	negl	%ebp
	testl	%esi, %esi
	cmovnsl	%ecx, %ebp
	imull	%r8d, %ebp
.LBB9_16:                               # %.us-lcssa.us
	movl	%r10d, %ecx
	shll	%cl, %ebp
	leaq	1(%r12), %rsi
	testl	%r13d, %r13d
	movl	%ebp, -120(%rsp)
	je	.LBB9_33
# BB#17:                                # %.preheader308.split.preheader.1
	movl	-124(%rsp), %eax
	movl	%eax, %edi
	negl	%edi
	cmovll	%eax, %edi
	movl	(%r9), %edx
	imull	%edx, %edi
	addl	%r11d, %edi
	movl	%r15d, %ecx
	sarl	%cl, %edi
	movl	%edi, %ecx
	negl	%ecx
	cmovll	%edi, %ecx
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$8, %rsi
	movl	%ecx, %edi
	negl	%edi
	testl	%eax, %eax
	cmovnsl	%ecx, %edi
	movq	-104(%rsp), %rax        # 8-byte Reload
	addl	2664(%rax,%rsi), %edi
	movq	-88(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	imull	%eax, %edi
	movl	%r10d, %ecx
	shll	%cl, %edi
	movl	%edi, -124(%rsp)
	movl	-116(%rsp), %ebp
	movl	%ebp, %edi
	negl	%edi
	cmovll	%ebp, %edi
	imull	%edx, %edi
	addl	%r11d, %edi
	movl	%r15d, %ecx
	sarl	%cl, %edi
	movl	%edi, %ecx
	negl	%ecx
	cmovll	%edi, %ecx
	movq	-104(%rsp), %rdi        # 8-byte Reload
	movl	%ecx, %edx
	negl	%edx
	testl	%ebp, %ebp
	cmovnsl	%ecx, %edx
	addl	2728(%rdi,%rsi), %edx
	jmp	.LBB9_36
.LBB9_33:                               # %.preheader308.split.us.preheader.1
	cmpl	$4, 44(%rdi)
	movl	-124(%rsp), %edx
	jne	.LBB9_34
# BB#35:
	movl	%edx, %eax
	negl	%eax
	cmovll	%edx, %eax
	movl	(%r9), %ebp
	imull	%ebp, %eax
	addl	%r11d, %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$8, %rsi
	movl	%ecx, %eax
	negl	%eax
	testl	%edx, %edx
	cmovnsl	%ecx, %eax
	addl	2664(%rdi,%rsi), %eax
	movq	-88(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %edx
	imull	%edx, %eax
	movl	%r10d, %ecx
	shll	%cl, %eax
	movl	%eax, -124(%rsp)
	movl	-116(%rsp), %edi
	movl	%edi, %eax
	negl	%eax
	cmovll	%edi, %eax
	imull	%ebp, %eax
	addl	%r11d, %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%edi, %edi
	movq	-104(%rsp), %rdi        # 8-byte Reload
	cmovnsl	%ecx, %eax
	addl	2728(%rdi,%rsi), %eax
	jmp	.LBB9_36
.LBB9_34:
	leaq	(%rsi,%rsi,2), %rbp
	shlq	$8, %rbp
	movq	-48(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %esi
	movl	2664(%rdi,%rbp), %eax
	imull	%esi, %eax
	shll	$4, %eax
	movq	%r12, %r13
	movq	-64(%rsp), %r12         # 8-byte Reload
	movl	%r12d, %ecx
	shll	%cl, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	movl	(%r9), %r8d
	imull	%r8d, %edx
	addl	%r11d, %edx
	movl	%r15d, %ecx
	sarl	%cl, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	%ecx, %edi
	negl	%edi
	testl	%eax, %eax
	cmovnsl	%ecx, %edi
	movq	-88(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edx
	imull	%edx, %edi
	movl	%r10d, %ecx
	shll	%cl, %edi
	movl	%edi, -124(%rsp)
	movq	-104(%rsp), %rdi        # 8-byte Reload
	imull	2728(%rdi,%rbp), %esi
	shll	$4, %esi
	movl	%r12d, %ecx
	movq	%r13, %r12
	shll	%cl, %esi
	sarl	$5, %esi
	addl	-116(%rsp), %esi
	movl	%esi, %eax
	negl	%eax
	cmovll	%esi, %eax
	imull	%r8d, %eax
	addl	%r11d, %eax
	movl	%r15d, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	testl	%esi, %esi
	cmovnsl	%ecx, %eax
.LBB9_36:                               # %.us-lcssa.us.loopexit347.1
	imull	%eax, %edx
	movl	%r10d, %ecx
	shll	%cl, %edx
	movl	%edx, -116(%rsp)
	leaq	(%r12,%r12,2), %rax
	shlq	$8, %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leaq	2664(%rdi,%rax), %r9
	movq	%rsp, %r8
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_18:                               # %.preheader306
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_19 Depth 2
                                        #       Child Loop BB9_20 Depth 3
                                        #         Child Loop BB9_21 Depth 4
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	movq	%r8, -16(%rsp)          # 8-byte Spill
	movq	%r9, -8(%rsp)           # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_19:                               # %.preheader305
                                        #   Parent Loop BB9_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_20 Depth 3
                                        #         Child Loop BB9_21 Depth 4
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	movq	%r8, -88(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	movq	%r9, -48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_20:                               # %.preheader
                                        #   Parent Loop BB9_18 Depth=1
                                        #     Parent Loop BB9_19 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB9_21 Depth 4
	movl	$4, %r15d
	movq	%r8, %r13
	movq	%r11, %rsi
	movq	%r9, %rbp
	jmp	.LBB9_21
.LBB9_24:                               #   in Loop: Header=BB9_21 Depth=4
	imull	%eax, %r10d
	imull	A(%rsi), %r10d
	movq	-64(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %r10d
	sarl	$6, %r10d
	addl	(%r13), %r10d
	movl	%r10d, %eax
	negl	%eax
	cmovll	%r10d, %eax
	imull	quant_coef(%rbx,%rsi), %eax
	addl	-80(%rsp), %eax         # 4-byte Folded Reload
	movl	-68(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%r10d, %r10d
	cmovnsl	%ecx, %edx
	jmp	.LBB9_25
	.p2align	4, 0x90
.LBB9_21:                               #   Parent Loop BB9_18 Depth=1
                                        #     Parent Loop BB9_19 Depth=2
                                        #       Parent Loop BB9_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rbp), %eax
	movq	-64(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	sarl	%cl, %eax
	movl	dequant_coef(%r14,%rsi), %r10d
	cltd
	idivl	%r10d
	movl	%eax, (%rbp)
	cmpl	$0, 36(%rdi)
	jne	.LBB9_23
# BB#22:                                #   in Loop: Header=BB9_21 Depth=4
	cmpl	$4, 44(%rdi)
	jne	.LBB9_24
.LBB9_23:                               #   in Loop: Header=BB9_21 Depth=4
	movl	(%r13), %edi
	movl	%edi, %edx
	negl	%edx
	cmovll	%edi, %edx
	imull	quant_coef(%rbx,%rsi), %edx
	addl	-80(%rsp), %edx         # 4-byte Folded Reload
	movl	-68(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	%ecx, %edx
	negl	%edx
	testl	%edi, %edi
	movq	-104(%rsp), %rdi        # 8-byte Reload
	cmovnsl	%ecx, %edx
	addl	%eax, %edx
.LBB9_25:                               #   in Loop: Header=BB9_21 Depth=4
	movq	-96(%rsp), %rcx         # 8-byte Reload
	imull	dequant_coef(%rbx,%rsi), %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edx
	movl	%edx, (%rbp)
	addq	$16, %rbp
	addq	$4, %rsi
	addq	$4, %r13
	decq	%r15
	jne	.LBB9_21
# BB#26:                                #   in Loop: Header=BB9_20 Depth=3
	incq	%r12
	addq	$4, %r9
	addq	$16, %r11
	addq	$32, %r8
	cmpq	$4, %r12
	jne	.LBB9_20
# BB#27:                                #   in Loop: Header=BB9_19 Depth=2
	movq	-56(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	-48(%rsp), %r9          # 8-byte Reload
	addq	$768, %r9               # imm = 0x300
	movq	-88(%rsp), %r8          # 8-byte Reload
	subq	$-128, %r8
	cmpq	$2, %rdx
	jne	.LBB9_19
# BB#28:                                #   in Loop: Header=BB9_18 Depth=1
	movq	-24(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	-8(%rsp), %r9           # 8-byte Reload
	addq	$64, %r9
	movq	-16(%rsp), %r8          # 8-byte Reload
	addq	$16, %r8
	cmpq	$2, %rdx
	jne	.LBB9_18
# BB#29:
	movl	-128(%rsp), %eax
	movl	-124(%rsp), %esi
	leal	(%rsi,%rax), %edx
	movl	-120(%rsp), %ecx
	movq	%rdi, %rbx
	leal	(%rdx,%rcx), %edi
	movl	-116(%rsp), %ebp
	addl	%ebp, %edi
	sarl	%edi
	movq	-32(%rsp), %r8          # 8-byte Reload
	movl	%edi, 2664(%rbx,%r8)
	subl	%esi, %eax
	leal	(%rax,%rcx), %esi
	subl	%ebp, %esi
	sarl	%esi
	movl	-36(%rsp), %edi         # 4-byte Reload
	incl	%edi
	movslq	%edi, %rdi
	leaq	(%rdi,%rdi,2), %rdi
	shlq	$8, %rdi
	movl	%esi, 2664(%rbx,%rdi)
	subl	%ecx, %edx
	subl	%ebp, %edx
	sarl	%edx
	movl	%edx, 2728(%rbx,%r8)
	subl	%ecx, %eax
	addl	%ebp, %eax
	sarl	%eax
	movl	%eax, 2728(%rbx,%rdi)
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	itrans_sp_chroma, .Lfunc_end9-itrans_sp_chroma
	.cfi_endproc

	.type	quant_intra_default,@object # @quant_intra_default
	.data
	.globl	quant_intra_default
	.p2align	4
quant_intra_default:
	.long	6                       # 0x6
	.long	13                      # 0xd
	.long	20                      # 0x14
	.long	28                      # 0x1c
	.long	13                      # 0xd
	.long	20                      # 0x14
	.long	28                      # 0x1c
	.long	32                      # 0x20
	.long	20                      # 0x14
	.long	28                      # 0x1c
	.long	32                      # 0x20
	.long	37                      # 0x25
	.long	28                      # 0x1c
	.long	32                      # 0x20
	.long	37                      # 0x25
	.long	42                      # 0x2a
	.size	quant_intra_default, 64

	.type	quant_inter_default,@object # @quant_inter_default
	.globl	quant_inter_default
	.p2align	4
quant_inter_default:
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	20                      # 0x14
	.long	24                      # 0x18
	.long	14                      # 0xe
	.long	20                      # 0x14
	.long	24                      # 0x18
	.long	27                      # 0x1b
	.long	20                      # 0x14
	.long	24                      # 0x18
	.long	27                      # 0x1b
	.long	30                      # 0x1e
	.long	24                      # 0x18
	.long	27                      # 0x1b
	.long	30                      # 0x1e
	.long	34                      # 0x22
	.size	quant_inter_default, 64

	.type	quant8_intra_default,@object # @quant8_intra_default
	.globl	quant8_intra_default
	.p2align	4
quant8_intra_default:
	.long	6                       # 0x6
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	16                      # 0x10
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	29                      # 0x1d
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	29                      # 0x1d
	.long	31                      # 0x1f
	.long	16                      # 0x10
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	29                      # 0x1d
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	29                      # 0x1d
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	36                      # 0x24
	.long	23                      # 0x17
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	29                      # 0x1d
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	36                      # 0x24
	.long	38                      # 0x26
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	29                      # 0x1d
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	36                      # 0x24
	.long	38                      # 0x26
	.long	40                      # 0x28
	.long	27                      # 0x1b
	.long	29                      # 0x1d
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	36                      # 0x24
	.long	38                      # 0x26
	.long	40                      # 0x28
	.long	42                      # 0x2a
	.size	quant8_intra_default, 256

	.type	quant8_inter_default,@object # @quant8_inter_default
	.globl	quant8_inter_default
	.p2align	4
quant8_inter_default:
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	15                      # 0xf
	.long	17                      # 0x11
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	24                      # 0x18
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	17                      # 0x11
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	15                      # 0xf
	.long	17                      # 0x11
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	17                      # 0x11
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	32                      # 0x20
	.long	22                      # 0x16
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	32                      # 0x20
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	27                      # 0x1b
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	32                      # 0x20
	.long	33                      # 0x21
	.long	35                      # 0x23
	.size	quant8_inter_default, 256

	.type	quant_org,@object       # @quant_org
	.globl	quant_org
	.p2align	4
quant_org:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.size	quant_org, 64

	.type	quant8_org,@object      # @quant8_org
	.globl	quant8_org
	.p2align	4
quant8_org:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.size	quant8_org, 256

	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"warning: Intra_4x4_Vertical prediction mode not allowed at mb %d\n"
	.size	.L.str, 66

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"warning: Intra_4x4_Horizontal prediction mode not allowed at mb %d\n"
	.size	.L.str.1, 68

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"warning: Intra_4x4_Diagonal_Down_Right prediction mode not allowed at mb %d\n"
	.size	.L.str.2, 77

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"warning: Intra_4x4_Diagonal_Down_Left prediction mode not allowed at mb %d\n"
	.size	.L.str.3, 76

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"warning: Intra_4x4_Vertical_Right prediction mode not allowed at mb %d\n"
	.size	.L.str.4, 72

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"warning: Intra_4x4_Vertical_Left prediction mode not allowed at mb %d\n"
	.size	.L.str.5, 71

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"warning: Intra_4x4_Horizontal_Up prediction mode not allowed at mb %d\n"
	.size	.L.str.6, 71

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"warning: Intra_4x4_Horizontal_Down prediction mode not allowed at mb %d\n"
	.size	.L.str.7, 73

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Error: illegal intra_4x4 prediction mode: %d\n"
	.size	.L.str.8, 46

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"invalid 16x16 intra pred Mode VERT_PRED_16"
	.size	.L.str.9, 43

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"invalid 16x16 intra pred Mode HOR_PRED_16"
	.size	.L.str.10, 42

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"invalid 16x16 intra pred Mode PLANE_16"
	.size	.L.str.11, 39

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"illegal 16x16 intra prediction mode input: %d\n"
	.size	.L.str.12, 47

	.type	.Lintrapred_chroma.block_pos,@object # @intrapred_chroma.block_pos
	.section	.rodata,"a",@progbits
	.p2align	4
.Lintrapred_chroma.block_pos:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.zero	16
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.size	.Lintrapred_chroma.block_pos, 192

	.type	.L.str.13,@object       # @.str.13
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.13:
	.asciz	"unexpected PLANE_8 chroma intra prediction mode"
	.size	.L.str.13, 48

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"unexpected HOR_PRED_8 chroma intra prediction mode"
	.size	.L.str.14, 51

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"unexpected VERT_PRED_8 chroma intra prediction mode"
	.size	.L.str.15, 52

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"illegal chroma intra prediction mode"
	.size	.L.str.16, 37

	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	quant_coef,@object      # @quant_coef
	.section	.rodata,"a",@progbits
	.p2align	4
quant_coef:
	.long	13107                   # 0x3333
	.long	8066                    # 0x1f82
	.long	13107                   # 0x3333
	.long	8066                    # 0x1f82
	.long	8066                    # 0x1f82
	.long	5243                    # 0x147b
	.long	8066                    # 0x1f82
	.long	5243                    # 0x147b
	.long	13107                   # 0x3333
	.long	8066                    # 0x1f82
	.long	13107                   # 0x3333
	.long	8066                    # 0x1f82
	.long	8066                    # 0x1f82
	.long	5243                    # 0x147b
	.long	8066                    # 0x1f82
	.long	5243                    # 0x147b
	.long	11916                   # 0x2e8c
	.long	7490                    # 0x1d42
	.long	11916                   # 0x2e8c
	.long	7490                    # 0x1d42
	.long	7490                    # 0x1d42
	.long	4660                    # 0x1234
	.long	7490                    # 0x1d42
	.long	4660                    # 0x1234
	.long	11916                   # 0x2e8c
	.long	7490                    # 0x1d42
	.long	11916                   # 0x2e8c
	.long	7490                    # 0x1d42
	.long	7490                    # 0x1d42
	.long	4660                    # 0x1234
	.long	7490                    # 0x1d42
	.long	4660                    # 0x1234
	.long	10082                   # 0x2762
	.long	6554                    # 0x199a
	.long	10082                   # 0x2762
	.long	6554                    # 0x199a
	.long	6554                    # 0x199a
	.long	4194                    # 0x1062
	.long	6554                    # 0x199a
	.long	4194                    # 0x1062
	.long	10082                   # 0x2762
	.long	6554                    # 0x199a
	.long	10082                   # 0x2762
	.long	6554                    # 0x199a
	.long	6554                    # 0x199a
	.long	4194                    # 0x1062
	.long	6554                    # 0x199a
	.long	4194                    # 0x1062
	.long	9362                    # 0x2492
	.long	5825                    # 0x16c1
	.long	9362                    # 0x2492
	.long	5825                    # 0x16c1
	.long	5825                    # 0x16c1
	.long	3647                    # 0xe3f
	.long	5825                    # 0x16c1
	.long	3647                    # 0xe3f
	.long	9362                    # 0x2492
	.long	5825                    # 0x16c1
	.long	9362                    # 0x2492
	.long	5825                    # 0x16c1
	.long	5825                    # 0x16c1
	.long	3647                    # 0xe3f
	.long	5825                    # 0x16c1
	.long	3647                    # 0xe3f
	.long	8192                    # 0x2000
	.long	5243                    # 0x147b
	.long	8192                    # 0x2000
	.long	5243                    # 0x147b
	.long	5243                    # 0x147b
	.long	3355                    # 0xd1b
	.long	5243                    # 0x147b
	.long	3355                    # 0xd1b
	.long	8192                    # 0x2000
	.long	5243                    # 0x147b
	.long	8192                    # 0x2000
	.long	5243                    # 0x147b
	.long	5243                    # 0x147b
	.long	3355                    # 0xd1b
	.long	5243                    # 0x147b
	.long	3355                    # 0xd1b
	.long	7282                    # 0x1c72
	.long	4559                    # 0x11cf
	.long	7282                    # 0x1c72
	.long	4559                    # 0x11cf
	.long	4559                    # 0x11cf
	.long	2893                    # 0xb4d
	.long	4559                    # 0x11cf
	.long	2893                    # 0xb4d
	.long	7282                    # 0x1c72
	.long	4559                    # 0x11cf
	.long	7282                    # 0x1c72
	.long	4559                    # 0x11cf
	.long	4559                    # 0x11cf
	.long	2893                    # 0xb4d
	.long	4559                    # 0x11cf
	.long	2893                    # 0xb4d
	.size	quant_coef, 384

	.type	A,@object               # @A
	.p2align	4
A:
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	20                      # 0x14
	.long	25                      # 0x19
	.size	A, 64

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
