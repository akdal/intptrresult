	.text
	.file	"systems.bc"
	.globl	Next_Packet
	.p2align	4, 0x90
	.type	Next_Packet,@function
Next_Packet:                            # @Next_Packet
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_11:                               #   in Loop: Header=BB0_1 Depth=1
	movq	ld(%rip), %rax
	movq	2056(%rax), %rcx
	addq	$2056, %rax             # imm = 0x808
	addq	$8, %rcx
	movq	%rcx, (%rax)
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_3 Depth 2
	callq	Get_Word
	movl	%eax, %ebp
	shll	$16, %ebp
	callq	Get_Word
	movl	%eax, %ebx
	orl	%ebp, %ebx
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        #   in Loop: Header=BB0_3 Depth=2
	shll	$8, %ebx
	callq	Get_Byte
	orl	%eax, %ebx
.LBB0_3:                                # %.lr.ph
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %eax
	andl	$-256, %eax
	cmpl	$256, %eax              # imm = 0x100
	jne	.LBB0_2
# BB#4:                                 # %._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$442, %ebx              # imm = 0x1BA
	je	.LBB0_11
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$480, %ebx              # imm = 0x1E0
	je	.LBB0_12
# BB#6:                                 # %._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$441, %ebx              # imm = 0x1B9
	je	.LBB0_7
# BB#27:                                #   in Loop: Header=BB0_1 Depth=1
	cmpl	$443, %ebx              # imm = 0x1BB
	jb	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_1 Depth=1
	callq	Get_Word
	movq	ld(%rip), %rcx
	movl	%eax, %eax
	addq	2056(%rcx), %rax
	addq	$2056, %rcx             # imm = 0x808
	movq	%rax, (%rcx)
	jmp	.LBB0_1
.LBB0_12:
	callq	Get_Word
	movq	ld(%rip), %rcx
	movl	%eax, %eax
	addq	2056(%rcx), %rax
	movq	%rax, 2088(%rcx)
	callq	Get_Byte
	movl	%eax, %ecx
	andl	$-64, %ecx
	cmpl	$128, %ecx
	jne	.LBB0_15
# BB#13:
	movq	ld(%rip), %rax
	incq	2056(%rax)
	callq	Get_Byte
	movq	ld(%rip), %rcx
	movl	%eax, %eax
	addq	%rax, 2056(%rcx)
	movl	$.Lstr, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	puts                    # TAILCALL
	.p2align	4, 0x90
.LBB0_14:                               # %.preheader
                                        #   in Loop: Header=BB0_15 Depth=1
	callq	Get_Byte
.LBB0_15:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$255, %eax
	je	.LBB0_14
# BB#16:                                # %.loopexit
	cmpl	$64, %eax
	jb	.LBB0_19
# BB#17:
	cmpl	$128, %eax
	jae	.LBB0_26
# BB#18:
	movq	ld(%rip), %rax
	incq	2056(%rax)
	callq	Get_Byte
.LBB0_19:
	cmpl	$48, %eax
	jb	.LBB0_23
# BB#20:
	cmpl	$64, %eax
	jae	.LBB0_26
# BB#21:
	movl	$9, %eax
	jmp	.LBB0_22
.LBB0_7:                                # %.preheader27
	movq	ld(%rip), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movb	$0, 4(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$0, 5(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$1, 6(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$-73, 7(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$0, 8(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$0, 9(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$1, 10(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$-73, 11(%rcx,%rax)
	addq	$8, %rax
	movq	ld(%rip), %rcx
	cmpq	$2048, %rax             # imm = 0x800
	jl	.LBB0_8
# BB#9:
	leaq	4(%rcx), %rax
	movq	%rax, 2056(%rcx)
	leaq	2052(%rcx), %rax
	movq	%rax, 2088(%rcx)
	jmp	.LBB0_10
.LBB0_23:
	cmpl	$31, %eax
	jbe	.LBB0_25
# BB#24:
	movl	$4, %eax
.LBB0_22:                               # %.sink.split
	movq	ld(%rip), %rcx
	addq	%rax, 2056(%rcx)
	jmp	.LBB0_10
.LBB0_25:
	cmpl	$15, %eax
	jne	.LBB0_26
.LBB0_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB0_29:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_26:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	Next_Packet, .Lfunc_end0-Next_Packet
	.cfi_endproc

	.globl	Get_Long
	.p2align	4, 0x90
	.type	Get_Long,@function
Get_Long:                               # @Get_Long
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	callq	Get_Word
	movl	%eax, %ebx
	shll	$16, %ebx
	callq	Get_Word
	orl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	Get_Long, .Lfunc_end1-Get_Long
	.cfi_endproc

	.globl	Flush_Buffer32
	.p2align	4, 0x90
	.type	Flush_Buffer32,@function
Flush_Buffer32:                         # @Flush_Buffer32
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	ld(%rip), %rdx
	movl	$0, 2080(%rdx)
	movl	2096(%rdx), %r14d
	leal	-32(%r14), %ebp
	cmpl	$0, System_Stream_Flag(%rip)
	je	.LBB2_8
# BB#1:
	movq	2056(%rdx), %rax
	movq	2088(%rdx), %rcx
	leaq	-4(%rcx), %rsi
	cmpq	%rsi, %rax
	jae	.LBB2_2
.LBB2_8:                                # %.preheader
	cmpl	$24, %ebp
	jg	.LBB2_15
# BB#9:                                 # %.lr.ph.preheader
	movl	$56, %ebx
	subl	%r14d, %ebx
	movl	%ebx, %r15d
	andl	$-8, %r15d
	addl	%r14d, %r15d
	movq	2056(%rdx), %rax
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	2052(%rdx), %rcx
	cmpq	%rcx, %rax
	jb	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_10 Depth=1
	callq	Fill_Buffer
	movq	ld(%rip), %rdx
	movq	2056(%rdx), %rax
.LBB2_12:                               #   in Loop: Header=BB2_10 Depth=1
	leaq	1(%rax), %rsi
	movq	%rsi, 2056(%rdx)
	movzbl	(%rax), %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	orl	%eax, 2080(%rdx)
	addl	$8, %ebp
	addl	$-8, %ebx
	cmpl	$25, %ebp
	movq	%rsi, %rax
	jl	.LBB2_10
# BB#13:                                # %.loopexit.loopexit
	addl	$-24, %r15d
	movl	%r15d, %ebp
	jmp	.LBB2_15
.LBB2_2:                                # %.preheader9
	cmpl	$24, %ebp
	jg	.LBB2_15
# BB#3:                                 # %.lr.ph13.preheader
	movl	$56, %ebx
	subl	%r14d, %ebx
	movl	%ebx, %ebp
	andl	$-8, %ebp
	addl	%r14d, %ebp
	addl	$-24, %r14d
	cmpq	%rcx, %rax
	jb	.LBB2_6
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_7:                                # %..lr.ph13_crit_edge
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	2056(%rdx), %rax
	movq	2088(%rdx), %rcx
	addl	$-8, %ebx
	addl	$8, %r14d
	cmpq	%rcx, %rax
	jb	.LBB2_6
.LBB2_5:
	callq	Next_Packet
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	callq	Get_Byte
	movl	%ebx, %ecx
	shll	%cl, %eax
	movq	ld(%rip), %rdx
	orl	%eax, 2080(%rdx)
	cmpl	$24, %r14d
	jle	.LBB2_7
# BB#14:                                # %.loopexit.loopexit15
	addl	$-24, %ebp
.LBB2_15:                               # %.loopexit
	movl	%ebp, 2096(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	Flush_Buffer32, .Lfunc_end2-Flush_Buffer32
	.cfi_endproc

	.globl	Get_Bits32
	.p2align	4, 0x90
	.type	Get_Bits32,@function
Get_Bits32:                             # @Get_Bits32
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movl	$32, %edi
	callq	Show_Bits
	movl	%eax, %ebx
	callq	Flush_Buffer32
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	Get_Bits32, .Lfunc_end3-Get_Bits32
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"Error in packet header\n"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Unexpected startcode %08x in system layer\n"
	.size	.L.str.2, 43

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"MPEG-2 PES packet"
	.size	.Lstr, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
