	.text
	.file	"util.bc"
	.globl	d_dup_pathname_str
	.p2align	4, 0x90
	.type	d_dup_pathname_str,@function
d_dup_pathname_str:                     # @d_dup_pathname_str
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB0_8
# BB#1:
	movabsq	$4294967296, %r14       # imm = 0x100000000
	cmpb	$34, (%r12)
	jne	.LBB0_6
# BB#2:                                 # %.preheader.preheader
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rbx), %eax
	incq	%rbx
	cmpb	$34, %al
	je	.LBB0_5
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	testb	%al, %al
	jne	.LBB0_3
.LBB0_5:                                # %.critedge
	incq	%r12
	subq	%r12, %rbx
	movq	%rbx, %rdi
	shlq	$32, %rdi
	addq	%r14, %rdi
	sarq	$32, %rdi
	callq	malloc
	movq	%rax, %r14
	movslq	%ebx, %r15
	jmp	.LBB0_7
.LBB0_8:
	movl	$1, %edi
	movl	$1, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	calloc                  # TAILCALL
.LBB0_6:
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %r15
	shlq	$32, %rax
	addq	%rax, %r14
	sarq	$32, %r14
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, %r14
	movslq	%r15d, %r15
.LBB0_7:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	memcpy
	movb	$0, (%r14,%r15)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	d_dup_pathname_str, .Lfunc_end0-d_dup_pathname_str
	.cfi_endproc

	.globl	dup_str
	.p2align	4, 0x90
	.type	dup_str,@function
dup_str:                                # @dup_str
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	subq	%r14, %rbx
	movq	%rbx, %rax
	shlq	$32, %rax
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	addq	%rax, %rdi
	sarq	$32, %rdi
	callq	malloc
	movq	%rax, %r15
	movslq	%ebx, %rbx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movb	$0, (%r15,%rbx)
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	dup_str, .Lfunc_end1-dup_str
	.cfi_endproc

	.globl	strhashl
	.p2align	4, 0x90
	.type	strhashl,@function
strhashl:                               # @strhashl
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph.preheader
	testb	$1, %sil
	jne	.LBB2_4
# BB#3:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	cmpl	$1, %esi
	jne	.LBB2_8
	jmp	.LBB2_14
.LBB2_1:
	xorl	%eax, %eax
	retq
.LBB2_4:                                # %.lr.ph.prol
	movsbl	(%rdi), %eax
	movl	%eax, %ecx
	andl	$-268435456, %ecx       # imm = 0xF0000000
	je	.LBB2_6
# BB#5:
	movl	%ecx, %edx
	shrl	$24, %edx
	xorl	%ecx, %eax
	xorl	%edx, %eax
.LBB2_6:
	incq	%rdi
	movl	$1, %ecx
	cmpl	$1, %esi
	je	.LBB2_14
.LBB2_8:                                # %.lr.ph.preheader.new
	subl	%ecx, %esi
	.p2align	4, 0x90
.LBB2_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	shll	$4, %eax
	movsbl	(%rdi), %ecx
	addl	%eax, %ecx
	movl	%ecx, %eax
	andl	$-268435456, %eax       # imm = 0xF0000000
	je	.LBB2_11
# BB#10:                                #   in Loop: Header=BB2_9 Depth=1
	movl	%eax, %edx
	shrl	$24, %edx
	xorl	%eax, %ecx
	xorl	%edx, %ecx
.LBB2_11:                               # %.lr.ph.119
                                        #   in Loop: Header=BB2_9 Depth=1
	shll	$4, %ecx
	movsbl	1(%rdi), %eax
	addl	%ecx, %eax
	movl	%eax, %ecx
	andl	$-268435456, %ecx       # imm = 0xF0000000
	je	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_9 Depth=1
	movl	%ecx, %edx
	shrl	$24, %edx
	xorl	%ecx, %eax
	xorl	%edx, %eax
.LBB2_13:                               #   in Loop: Header=BB2_9 Depth=1
	addq	$2, %rdi
	addl	$-2, %esi
	jne	.LBB2_9
.LBB2_14:                               # %._crit_edge
	retq
.Lfunc_end2:
	.size	strhashl, .Lfunc_end2-strhashl
	.cfi_endproc

	.globl	buf_read
	.p2align	4, 0x90
	.type	buf_read,@function
buf_read:                               # @buf_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 192
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	$0, (%r15)
	movl	$0, (%r14)
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jle	.LBB3_1
# BB#2:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rdx
	movl	$1, %edi
	movl	%r12d, %esi
	callq	__fxstat
	movq	48(%rsp), %rbp
	movl	%ebp, (%r14)
	movq	%rbp, %rbx
	shlq	$32, %rbx
	movabsq	$8589934592, %rdi       # imm = 0x200000000
	addq	%rbx, %rdi
	sarq	$32, %rdi
	callq	malloc
	movq	%rax, (%r15)
	movslq	%ebp, %rdx
	movb	$0, (%rax,%rdx)
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	addq	%rbx, %rcx
	sarq	$32, %rcx
	movb	$0, (%rax,%rcx)
	movl	%r12d, %edi
	movq	%rax, %rsi
	callq	read
	movl	%r12d, %edi
	callq	close
	movl	(%r14), %eax
	jmp	.LBB3_3
.LBB3_1:
	movl	$-1, %eax
.LBB3_3:
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	buf_read, .Lfunc_end3-buf_read
	.cfi_endproc

	.globl	sbuf_read
	.p2align	4, 0x90
	.type	sbuf_read,@function
sbuf_read:                              # @sbuf_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 192
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	open
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jle	.LBB4_2
# BB#1:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rdx
	movl	$1, %edi
	movl	%r14d, %esi
	callq	__fxstat
	movq	48(%rsp), %r15
	movq	%r15, %rbx
	shlq	$32, %rbx
	movabsq	$8589934592, %rdi       # imm = 0x200000000
	addq	%rbx, %rdi
	sarq	$32, %rdi
	callq	malloc
	movq	%rax, %rbp
	movslq	%r15d, %r15
	movb	$0, (%rbp,%r15)
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rbx, %rax
	sarq	$32, %rax
	movb	$0, (%rbp,%rax)
	movl	%r14d, %edi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	read
	movl	%r14d, %edi
	callq	close
	xorl	%ebx, %ebx
	testl	%r15d, %r15d
	cmovnsq	%rbp, %rbx
.LBB4_2:                                # %buf_read.exit
	movq	%rbx, %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	sbuf_read, .Lfunc_end4-sbuf_read
	.cfi_endproc

	.globl	d_fail
	.p2align	4, 0x90
	.type	d_fail,@function
d_fail:                                 # @d_fail
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
	subq	$464, %rsp              # imm = 0x1D0
.Lcfi36:
	.cfi_def_cfa_offset 480
.Lcfi37:
	.cfi_offset %rbx, -16
	movq	%rdi, %r10
	testb	%al, %al
	je	.LBB5_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB5_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	480(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	leaq	208(%rsp), %rbx
	movl	$255, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r10, %rcx
	callq	snprintf
	movq	stderr(%rip), %rdi
	movq	%rsp, %rdx
	movq	%rbx, %rsi
	callq	vfprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	d_fail, .Lfunc_end5-d_fail
	.cfi_endproc

	.globl	vec_add_internal
	.p2align	4, 0x90
	.type	vec_add_internal,@function
vec_add_internal:                       # @vec_add_internal
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -48
.Lcfi44:
	.cfi_offset %r12, -40
.Lcfi45:
	.cfi_offset %r13, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	(%r13), %ebx
	testq	%rbx, %rbx
	je	.LBB6_1
# BB#2:
	movq	8(%r13), %r15
	leaq	16(%r13), %r12
	cmpq	%r12, %r15
	je	.LBB6_3
# BB#4:
	testb	$7, %bl
	jne	.LBB6_9
# BB#5:
	movl	%ebx, %eax
	sarl	$3, %eax
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	sarl	%eax
	incl	%ecx
	testb	$1, %dl
	je	.LBB6_6
# BB#7:
	testl	%eax, %eax
	jne	.LBB6_9
# BB#8:
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rsi
	shlq	$3, %rsi
	movq	%r15, %rdi
	callq	realloc
	movq	%rax, %r15
	movq	%r15, 8(%r13)
	movl	(%r13), %ebx
	jmp	.LBB6_9
.LBB6_1:
	leaq	16(%r13), %r15
	movq	%r15, 8(%r13)
	xorl	%ebx, %ebx
	jmp	.LBB6_9
.LBB6_3:
	movl	$64, %edi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, 8(%r13)
	movq	%rbx, %rdx
	shlq	$3, %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	memcpy
.LBB6_9:
	leal	1(%rbx), %eax
	movl	%eax, (%r13)
	movl	%ebx, %eax
	movq	%r14, (%r15,%rax,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	vec_add_internal, .Lfunc_end6-vec_add_internal
	.cfi_endproc

	.globl	vec_eq
	.p2align	4, 0x90
	.type	vec_eq,@function
vec_eq:                                 # @vec_eq
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	(%rsi), %ecx
	jne	.LBB7_6
# BB#1:                                 # %.preheader
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.LBB7_6
# BB#2:                                 # %.lr.ph
	movq	8(%rdi), %r8
	movq	8(%rsi), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rdi,8), %rdx
	cmpq	(%rsi,%rdi,8), %rdx
	jne	.LBB7_5
# BB#3:                                 #   in Loop: Header=BB7_4 Depth=1
	incq	%rdi
	cmpl	%ecx, %edi
	jb	.LBB7_4
	jmp	.LBB7_6
.LBB7_5:
	xorl	%eax, %eax
.LBB7_6:                                # %.loopexit
	retq
.Lfunc_end7:
	.size	vec_eq, .Lfunc_end7-vec_eq
	.cfi_endproc

	.globl	stack_push_internal
	.p2align	4, 0x90
	.type	stack_push_internal,@function
stack_push_internal:                    # @stack_push_internal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 64
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	(%r15), %r12
	movq	16(%r15), %rbp
	subq	%r12, %rbp
	leaq	24(%r15), %rax
	movq	%rbp, %rcx
	shrq	$2, %rcx
	movslq	%ecx, %r13
	leaq	(,%r13,8), %rsi
	cmpq	%rax, %r12
	je	.LBB8_1
# BB#2:
	movq	%r12, %rdi
	callq	realloc
	movq	%rax, %rbx
	movq	%rbx, 16(%r15)
	shrq	$3, %rbp
	movslq	%ebp, %rbp
	jmp	.LBB8_3
.LBB8_1:
	movq	%rsi, %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 16(%r15)
	shrq	$3, %rbp
	movslq	%ebp, %rbp
	leaq	(,%rbp,8), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	memcpy
.LBB8_3:
	movq	%rbx, (%r15)
	leaq	(%rbx,%r13,8), %rax
	movq	%rax, 8(%r15)
	leaq	8(%rbx,%rbp,8), %rax
	movq	%rax, 16(%r15)
	movq	%r14, (%rbx,%rbp,8)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	stack_push_internal, .Lfunc_end8-stack_push_internal
	.cfi_endproc

	.globl	set_add
	.p2align	4, 0x90
	.type	set_add,@function
set_add:                                # @set_add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 64
.Lcfi68:
	.cfi_offset %rbx, -56
.Lcfi69:
	.cfi_offset %r12, -48
.Lcfi70:
	.cfi_offset %r13, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	jmp	.LBB9_2
	.p2align	4, 0x90
.LBB9_1:                                # %set_union.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB9_2:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_20 Depth 2
	movslq	(%r12), %rbp
	testq	%rbp, %rbp
	movl	%ebp, %ebx
	je	.LBB9_14
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	xorl	%edx, %edx
	movq	%r14, %rax
	divq	%rbp
	movq	%rdx, %rax
	cmpl	%ebx, %eax
	jae	.LBB9_15
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	8(%r12), %r15
	movl	%eax, %ecx
	movq	(%r15,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB9_23
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
	xorl	%ecx, %ecx
	cmpq	%r14, %rdx
	je	.LBB9_28
# BB#6:                                 #   in Loop: Header=BB9_2 Depth=1
	incl	%eax
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%ebp
	movl	%edx, %eax
	movq	(%r15,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.LBB9_25
# BB#7:                                 #   in Loop: Header=BB9_2 Depth=1
	cmpq	%r14, %rdx
	je	.LBB9_28
# BB#8:                                 #   in Loop: Header=BB9_2 Depth=1
	incl	%eax
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%ebp
	movl	%edx, %eax
	movq	(%r15,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.LBB9_25
# BB#9:                                 #   in Loop: Header=BB9_2 Depth=1
	cmpq	%r14, %rdx
	je	.LBB9_28
# BB#10:                                #   in Loop: Header=BB9_2 Depth=1
	incl	%eax
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%ebx
	movl	%edx, %eax
	movq	(%r15,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.LBB9_25
# BB#11:                                #   in Loop: Header=BB9_2 Depth=1
	cmpq	%r14, %rdx
	je	.LBB9_28
# BB#12:                                #   in Loop: Header=BB9_2 Depth=1
	incl	%eax
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%ebx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	(%r15,%rdx,8), %rax
	testq	%rax, %rax
	je	.LBB9_26
# BB#13:                                #   in Loop: Header=BB9_2 Depth=1
	cmpq	%r14, %rax
	jne	.LBB9_16
	jmp	.LBB9_28
	.p2align	4, 0x90
.LBB9_14:                               #   in Loop: Header=BB9_2 Depth=1
	movl	$2, %eax
	xorl	%r15d, %r15d
	jmp	.LBB9_17
	.p2align	4, 0x90
.LBB9_15:                               # %.._crit_edge_crit_edge
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	8(%r12), %r15
.LBB9_16:                               # %._crit_edge
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	4(%r12), %eax
	incl	%eax
.LBB9_17:                               # %.critedge
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	%eax, 4(%r12)
	movl	%eax, %eax
	movl	prime2(,%rax,4), %r13d
	movl	%r13d, (%r12)
	shlq	$3, %r13
	movq	%r13, %rdi
	callq	malloc
	movq	%rax, 8(%r12)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r13, %rdx
	callq	memset
	testq	%r15, %r15
	je	.LBB9_2
# BB#18:                                # %.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB9_1
# BB#19:                                # %.lr.ph67.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB9_20:                               # %.lr.ph67
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB9_22
# BB#21:                                #   in Loop: Header=BB9_20 Depth=2
	movq	%r12, %rdi
	callq	set_add
.LBB9_22:                               #   in Loop: Header=BB9_20 Depth=2
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB9_20
	jmp	.LBB9_1
.LBB9_25:
	leaq	(%r15,%rax,8), %rax
	jmp	.LBB9_27
.LBB9_23:
	leaq	(%r15,%rcx,8), %rax
	jmp	.LBB9_27
.LBB9_26:
	leaq	(%r15,%rdx,8), %rax
.LBB9_27:
	movq	%r14, (%rax)
	movl	$1, %ecx
.LBB9_28:                               # %.thread
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	set_add, .Lfunc_end9-set_add
	.cfi_endproc

	.globl	set_union
	.p2align	4, 0x90
	.type	set_union,@function
set_union:                              # @set_union
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 48
.Lcfi79:
	.cfi_offset %rbx, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	(%rbx), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	je	.LBB10_5
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rcx
	movl	%ebp, %edx
	movq	(%rcx,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%r14, %rdi
	callq	set_add
	xorl	%ecx, %ecx
	orl	%r15d, %eax
	setne	%cl
	movl	(%rbx), %eax
	movl	%ecx, %r15d
.LBB10_4:                               #   in Loop: Header=BB10_2 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jb	.LBB10_2
.LBB10_5:                               # %._crit_edge
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	set_union, .Lfunc_end10-set_union
	.cfi_endproc

	.globl	set_add_fn
	.p2align	4, 0x90
	.type	set_add_fn,@function
set_add_fn:                             # @set_add_fn
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi86:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 64
.Lcfi90:
	.cfi_offset %rbx, -56
.Lcfi91:
	.cfi_offset %r12, -48
.Lcfi92:
	.cfi_offset %r13, -40
.Lcfi93:
	.cfi_offset %r14, -32
.Lcfi94:
	.cfi_offset %r15, -24
.Lcfi95:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	%r14, (%rsp)            # 8-byte Spill
	jmp	.LBB11_1
	.p2align	4, 0x90
.LBB11_18:                              # %set_union_fn.exit
                                        #   in Loop: Header=BB11_1 Depth=1
	movq	%r12, %rdi
	callq	free
.LBB11_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_4 Depth 2
                                        #     Child Loop BB11_15 Depth 2
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*(%r15)
	movl	(%r13), %r12d
	testl	%r12d, %r12d
	je	.LBB11_2
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB11_1 Depth=1
	xorl	%edx, %edx
	divl	%r12d
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB11_4:                               # %.lr.ph
                                        #   Parent Loop BB11_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13), %rcx
	movl	%edx, %ebx
	movq	(%rcx,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB11_5
# BB#6:                                 #   in Loop: Header=BB11_4 Depth=2
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r15, %rdx
	callq	*8(%r15)
	testl	%eax, %eax
	je	.LBB11_7
# BB#9:                                 #   in Loop: Header=BB11_4 Depth=2
	incl	%ebx
	xorl	%edx, %edx
	movl	%ebx, %eax
	divl	%r12d
	movl	(%r13), %ebp
	cmpl	$4, %r14d
	jg	.LBB11_11
# BB#10:                                #   in Loop: Header=BB11_4 Depth=2
	incl	%r14d
	cmpl	%ebp, %edx
	jb	.LBB11_4
.LBB11_11:                              # %._crit_edge
                                        #   in Loop: Header=BB11_1 Depth=1
	movq	8(%r13), %r12
	movl	4(%r13), %eax
	incl	%eax
	movq	(%rsp), %r14            # 8-byte Reload
	jmp	.LBB11_12
	.p2align	4, 0x90
.LBB11_2:                               #   in Loop: Header=BB11_1 Depth=1
	movl	$2, %eax
	xorl	%r12d, %r12d
                                        # implicit-def: %EBP
.LBB11_12:                              # %.critedge
                                        #   in Loop: Header=BB11_1 Depth=1
	movl	%eax, 4(%r13)
	movl	%eax, %eax
	movl	prime2(,%rax,4), %ebx
	movl	%ebx, (%r13)
	shlq	$3, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 8(%r13)
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memset
	testq	%r12, %r12
	je	.LBB11_1
# BB#13:                                # %.preheader
                                        #   in Loop: Header=BB11_1 Depth=1
	testl	%ebp, %ebp
	je	.LBB11_18
# BB#14:                                # %.lr.ph81.preheader
                                        #   in Loop: Header=BB11_1 Depth=1
	movl	%ebp, %ebx
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB11_15:                              # %.lr.ph81
                                        #   Parent Loop BB11_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB11_17
# BB#16:                                #   in Loop: Header=BB11_15 Depth=2
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	set_add_fn
.LBB11_17:                              #   in Loop: Header=BB11_15 Depth=2
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB11_15
	jmp	.LBB11_18
.LBB11_5:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	jmp	.LBB11_8
.LBB11_7:
	movq	8(%r13), %rax
	movq	(%rax,%rbx,8), %rax
.LBB11_8:                               # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	set_add_fn, .Lfunc_end11-set_add_fn
	.cfi_endproc

	.globl	set_union_fn
	.p2align	4, 0x90
	.type	set_union_fn,@function
set_union_fn:                           # @set_union_fn
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 48
.Lcfi101:
	.cfi_offset %rbx, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.LBB12_5
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rcx
	movl	%ebp, %edx
	movq	(%rcx,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.LBB12_4
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	set_add_fn
	movl	(%rbx), %eax
.LBB12_4:                               #   in Loop: Header=BB12_2 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jb	.LBB12_2
.LBB12_5:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	set_union_fn, .Lfunc_end12-set_union_fn
	.cfi_endproc

	.globl	set_to_vec
	.p2align	4, 0x90
	.type	set_to_vec,@function
set_to_vec:                             # @set_to_vec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi108:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi109:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi111:
	.cfi_def_cfa_offset 112
.Lcfi112:
	.cfi_offset %rbx, -56
.Lcfi113:
	.cfi_offset %r12, -48
.Lcfi114:
	.cfi_offset %r13, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	(%r15), %r12d
	movl	%r12d, 16(%rsp)
	movq	8(%r15), %rdi
	movq	%rdi, 24(%rsp)
	leaq	16(%r15), %rsi
	cmpq	%rsi, %rdi
	jne	.LBB13_2
# BB#1:
	leaq	32(%rsp), %rdi
	movq	16(%rsi), %rax
	movq	%rax, 48(%rsp)
	movups	(%rsi), %xmm0
	movups	%xmm0, 32(%rsp)
	movq	%rdi, 24(%rsp)
.LBB13_2:
	movl	$0, (%r15)
	movq	$0, 8(%r15)
	testl	%r12d, %r12d
	je	.LBB13_16
# BB#3:                                 # %.lr.ph
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB13_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_11 Depth 2
	movl	%ebp, %eax
	movq	(%rdi,%rax,8), %r14
	testq	%r14, %r14
	je	.LBB13_15
# BB#5:                                 #   in Loop: Header=BB13_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB13_6
# BB#7:                                 #   in Loop: Header=BB13_4 Depth=1
	movq	8(%r15), %r13
	cmpq	%rsi, %r13
	je	.LBB13_8
# BB#9:                                 #   in Loop: Header=BB13_4 Depth=1
	testb	$7, %bl
	jne	.LBB13_14
# BB#10:                                #   in Loop: Header=BB13_4 Depth=1
	movl	%ebx, %eax
	sarl	$3, %eax
	movl	$3, %ecx
	.p2align	4, 0x90
.LBB13_11:                              #   Parent Loop BB13_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %edx
	sarl	%eax
	incl	%ecx
	testb	$1, %dl
	je	.LBB13_11
# BB#12:                                #   in Loop: Header=BB13_4 Depth=1
	testl	%eax, %eax
	jne	.LBB13_14
# BB#13:                                #   in Loop: Header=BB13_4 Depth=1
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %rsi
	shlq	$3, %rsi
	movq	%r13, %rdi
	callq	realloc
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rax, %r13
	movq	%r13, 8(%r15)
	movl	(%r15), %ebx
	movl	16(%rsp), %r12d
	jmp	.LBB13_14
	.p2align	4, 0x90
.LBB13_6:                               #   in Loop: Header=BB13_4 Depth=1
	movq	%rsi, 8(%r15)
	xorl	%ebx, %ebx
	movq	%rsi, %r13
	jmp	.LBB13_14
.LBB13_8:                               #   in Loop: Header=BB13_4 Depth=1
	movl	$64, %edi
	callq	malloc
	movq	%rax, %r13
	movq	%r13, 8(%r15)
	movl	%ebx, %edx
	shlq	$3, %rdx
	movq	%r13, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	memcpy
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB13_14:                              # %vec_add_internal.exit
                                        #   in Loop: Header=BB13_4 Depth=1
	movl	%ebx, %eax
	leal	1(%rbx), %ecx
	movl	%ecx, (%r15)
	movq	%r14, (%r13,%rax,8)
	movq	24(%rsp), %rdi
	movl	%ecx, %ebx
.LBB13_15:                              #   in Loop: Header=BB13_4 Depth=1
	incl	%ebp
	cmpl	%r12d, %ebp
	jb	.LBB13_4
.LBB13_16:                              # %._crit_edge
	callq	free
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	set_to_vec, .Lfunc_end13-set_to_vec
	.cfi_endproc

	.globl	int_list_diff
	.p2align	4, 0x90
	.type	int_list_diff,@function
int_list_diff:                          # @int_list_diff
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	testl	%eax, %eax
	jns	.LBB14_2
	jmp	.LBB14_14
	.p2align	4, 0x90
.LBB14_4:                               # %.preheader23._crit_edge
                                        #   in Loop: Header=BB14_2 Depth=1
	addq	$4, %rdx
	movl	4(%rsi), %eax
	addq	$4, %rsi
.LBB14_1:                               #   in Loop: Header=BB14_2 Depth=1
	testl	%eax, %eax
	js	.LBB14_14
.LBB14_2:                               # %.preheader25
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_3 Depth 2
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jns	.LBB14_3
	jmp	.LBB14_6
	.p2align	4, 0x90
.LBB14_10:                              # %._crit_edge
                                        #   in Loop: Header=BB14_3 Depth=2
	addq	$4, %rsi
	addq	$4, %rdi
	movl	(%rdx), %ecx
.LBB14_3:                               # %.preheader23
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ecx, %eax
	je	.LBB14_4
# BB#8:                                 # %.lr.ph154
                                        #   in Loop: Header=BB14_3 Depth=2
	cmpl	%ecx, %eax
	jge	.LBB14_11
# BB#9:                                 #   in Loop: Header=BB14_3 Depth=2
	movl	%eax, (%rdi)
	movl	4(%rsi), %eax
	testl	%eax, %eax
	jns	.LBB14_10
	jmp	.LBB14_13
.LBB14_11:                              #   in Loop: Header=BB14_3 Depth=2
	cmpl	%eax, %ecx
	jge	.LBB14_1
# BB#12:                                #   in Loop: Header=BB14_3 Depth=2
	movl	4(%rdx), %ecx
	addq	$4, %rdx
	testl	%ecx, %ecx
	jns	.LBB14_3
# BB#5:                                 # %.preheader
	testl	%eax, %eax
	js	.LBB14_14
.LBB14_6:                               # %.lr.ph.preheader
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB14_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, (%rdi)
	addq	$4, %rdi
	movl	(%rsi), %eax
	addq	$4, %rsi
	testl	%eax, %eax
	jns	.LBB14_7
	jmp	.LBB14_14
.LBB14_13:                              # %.loopexit.loopexit161
	addq	$4, %rdi
.LBB14_14:                              # %.loopexit
	movl	$-1, (%rdi)
	retq
.Lfunc_end14:
	.size	int_list_diff, .Lfunc_end14-int_list_diff
	.cfi_endproc

	.globl	int_list_intersect
	.p2align	4, 0x90
	.type	int_list_intersect,@function
int_list_intersect:                     # @int_list_intersect
	.cfi_startproc
# BB#0:
	jmp	.LBB15_1
	.p2align	4, 0x90
.LBB15_7:                               # %.outer
                                        #   in Loop: Header=BB15_1 Depth=1
	movl	%ecx, (%rdi)
	addq	$4, %rdi
	addq	$4, %rdx
.LBB15_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_5 Depth 2
                                        #       Child Loop BB15_6 Depth 3
	movl	(%rsi), %eax
	testl	%eax, %eax
	js	.LBB15_10
# BB#2:                                 # %.preheader17.lr.ph
                                        #   in Loop: Header=BB15_1 Depth=1
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jns	.LBB15_5
	jmp	.LBB15_10
	.p2align	4, 0x90
.LBB15_9:                               #   in Loop: Header=BB15_6 Depth=3
	movl	(%rsi), %eax
	addq	$4, %rsi
	testl	%eax, %eax
	jns	.LBB15_6
	jmp	.LBB15_10
	.p2align	4, 0x90
.LBB15_11:                              #   in Loop: Header=BB15_5 Depth=2
	addq	$-4, %rsi
	cmpl	%eax, %ecx
	jl	.LBB15_12
# BB#3:                                 # %.loopexit19
                                        #   in Loop: Header=BB15_5 Depth=2
	testl	%eax, %eax
	jns	.LBB15_4
	jmp	.LBB15_10
	.p2align	4, 0x90
.LBB15_12:                              #   in Loop: Header=BB15_5 Depth=2
	movl	4(%rdx), %ecx
	addq	$4, %rdx
.LBB15_4:                               # %.preheader17
                                        #   in Loop: Header=BB15_5 Depth=2
	testl	%ecx, %ecx
	js	.LBB15_10
.LBB15_5:                               # %.preheader
                                        #   Parent Loop BB15_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_6 Depth 3
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB15_6:                               #   Parent Loop BB15_1 Depth=1
                                        #     Parent Loop BB15_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ecx, %eax
	je	.LBB15_7
# BB#8:                                 #   in Loop: Header=BB15_6 Depth=3
	jl	.LBB15_9
	jmp	.LBB15_11
.LBB15_10:                              # %.loopexit
	movl	$-1, (%rdi)
	retq
.Lfunc_end15:
	.size	int_list_intersect, .Lfunc_end15-int_list_intersect
	.cfi_endproc

	.globl	int_list_dup
	.p2align	4, 0x90
	.type	int_list_dup,@function
int_list_dup:                           # @int_list_dup
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 16
.Lcfi119:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	-4(%rbx), %rax
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$-1, 4(%rax)
	leaq	4(%rax), %rax
	jne	.LBB16_1
# BB#2:
	movl	$4, %edi
	subq	%rbx, %rdi
	addq	%rax, %rdi
	andq	$-4, %rdi
	callq	malloc
	movl	(%rbx), %ecx
	cmpl	$-1, %ecx
	je	.LBB16_3
# BB#4:                                 # %.lr.ph.preheader
	addq	$4, %rbx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB16_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%rdx)
	addq	$4, %rdx
	movl	(%rbx), %ecx
	addq	$4, %rbx
	cmpl	$-1, %ecx
	jne	.LBB16_5
	jmp	.LBB16_6
.LBB16_3:
	movq	%rax, %rdx
.LBB16_6:                               # %._crit_edge
	movl	$-1, (%rdx)
	popq	%rbx
	retq
.Lfunc_end16:
	.size	int_list_dup, .Lfunc_end16-int_list_dup
	.cfi_endproc

	.globl	escape_string
	.p2align	4, 0x90
	.type	escape_string,@function
escape_string:                          # @escape_string
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi124:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi126:
	.cfi_def_cfa_offset 64
.Lcfi127:
	.cfi_offset %rbx, -56
.Lcfi128:
	.cfi_offset %r12, -48
.Lcfi129:
	.cfi_offset %r13, -40
.Lcfi130:
	.cfi_offset %r14, -32
.Lcfi131:
	.cfi_offset %r15, -24
.Lcfi132:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	callq	strlen
	leaq	4(,%rax,4), %rdi
	callq	malloc
	movq	%rax, %r14
	movb	(%rbp), %bl
	testb	%bl, %bl
	movq	%r14, %r15
	je	.LBB17_19
# BB#1:                                 # %.lr.ph.preheader
	incq	%rbp
	movl	$55, %r12d
	movq	%r14, %r15
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%bl, %r13d
	leal	-7(%r13), %eax
	cmpl	$85, %eax
	ja	.LBB17_12
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB17_2 Depth=1
	jmpq	*.LJTI17_0(,%rax,8)
.LBB17_11:                              #   in Loop: Header=BB17_2 Depth=1
	movb	$92, (%r15)
	movb	%bl, 1(%r15)
	addq	$2, %r15
	jmp	.LBB17_18
.LBB17_10:                              #   in Loop: Header=BB17_2 Depth=1
	movw	$24924, (%r15)          # imm = 0x615C
	addq	$2, %r15
	jmp	.LBB17_18
.LBB17_4:                               #   in Loop: Header=BB17_2 Depth=1
	movw	$25180, (%r15)          # imm = 0x625C
	addq	$2, %r15
	jmp	.LBB17_18
.LBB17_8:                               #   in Loop: Header=BB17_2 Depth=1
	movw	$29788, (%r15)          # imm = 0x745C
	addq	$2, %r15
	jmp	.LBB17_18
.LBB17_6:                               #   in Loop: Header=BB17_2 Depth=1
	movw	$28252, (%r15)          # imm = 0x6E5C
	addq	$2, %r15
	jmp	.LBB17_18
.LBB17_9:                               #   in Loop: Header=BB17_2 Depth=1
	movw	$30300, (%r15)          # imm = 0x765C
	addq	$2, %r15
	jmp	.LBB17_18
.LBB17_5:                               #   in Loop: Header=BB17_2 Depth=1
	movw	$26204, (%r15)          # imm = 0x665C
	addq	$2, %r15
	jmp	.LBB17_18
.LBB17_7:                               #   in Loop: Header=BB17_2 Depth=1
	movw	$29276, (%r15)          # imm = 0x725C
	addq	$2, %r15
	jmp	.LBB17_18
.LBB17_12:                              #   in Loop: Header=BB17_2 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	%bl, %rcx
	testb	$64, 1(%rax,%rcx,2)
	jne	.LBB17_13
# BB#14:                                #   in Loop: Header=BB17_2 Depth=1
	movb	$92, (%r15)
	movb	$120, 1(%r15)
	sarl	$4, %r13d
	cmpl	$9, %r13d
	movl	$48, %eax
	cmovgl	%r12d, %eax
	addl	%r13d, %eax
	movb	%al, 2(%r15)
	andb	$15, %bl
	cmpb	$9, %bl
	ja	.LBB17_15
# BB#16:                                #   in Loop: Header=BB17_2 Depth=1
	orb	$48, %bl
	jmp	.LBB17_17
.LBB17_13:                              #   in Loop: Header=BB17_2 Depth=1
	movb	%bl, (%r15)
	incq	%r15
	jmp	.LBB17_18
.LBB17_15:                              #   in Loop: Header=BB17_2 Depth=1
	addb	$55, %bl
.LBB17_17:                              #   in Loop: Header=BB17_2 Depth=1
	movb	%bl, 3(%r15)
	addq	$4, %r15
	.p2align	4, 0x90
.LBB17_18:                              #   in Loop: Header=BB17_2 Depth=1
	movzbl	(%rbp), %ebx
	incq	%rbp
	testb	%bl, %bl
	jne	.LBB17_2
.LBB17_19:                              # %._crit_edge
	movb	$0, (%r15)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	escape_string, .Lfunc_end17-escape_string
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI17_0:
	.quad	.LBB17_10
	.quad	.LBB17_4
	.quad	.LBB17_8
	.quad	.LBB17_6
	.quad	.LBB17_9
	.quad	.LBB17_5
	.quad	.LBB17_7
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_11
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_12
	.quad	.LBB17_11

	.text
	.globl	d_free
	.p2align	4, 0x90
	.type	d_free,@function
d_free:                                 # @d_free
	.cfi_startproc
# BB#0:
	jmp	free                    # TAILCALL
.Lfunc_end18:
	.size	d_free, .Lfunc_end18-d_free
	.cfi_endproc

	.type	prime2,@object          # @prime2
	.data
	.globl	prime2
	.p2align	4
prime2:
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	13                      # 0xd
	.long	31                      # 0x1f
	.long	61                      # 0x3d
	.long	127                     # 0x7f
	.long	251                     # 0xfb
	.long	509                     # 0x1fd
	.long	1021                    # 0x3fd
	.long	2039                    # 0x7f7
	.long	4093                    # 0xffd
	.long	8191                    # 0x1fff
	.long	16381                   # 0x3ffd
	.long	32749                   # 0x7fed
	.long	65521                   # 0xfff1
	.long	131071                  # 0x1ffff
	.long	262139                  # 0x3fffb
	.long	524287                  # 0x7ffff
	.long	1048573                 # 0xffffd
	.long	2097143                 # 0x1ffff7
	.long	4194301                 # 0x3ffffd
	.long	8388593                 # 0x7ffff1
	.long	16777213                # 0xfffffd
	.long	33554393                # 0x1ffffd9
	.long	67108859                # 0x3fffffb
	.long	134217689               # 0x7ffffd9
	.long	268435399               # 0xfffffc7
	.long	536870909               # 0x1ffffffd
	.size	prime2, 116

	.type	verbose_level,@object   # @verbose_level
	.bss
	.globl	verbose_level
	.p2align	2
verbose_level:
	.long	0                       # 0x0
	.size	verbose_level, 4

	.type	debug_level,@object     # @debug_level
	.globl	debug_level
	.p2align	2
debug_level:
	.long	0                       # 0x0
	.size	debug_level, 4

	.type	test_level,@object      # @test_level
	.globl	test_level
	.p2align	2
test_level:
	.long	0                       # 0x0
	.size	test_level, 4

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"fail: %s\n"
	.size	.L.str.1, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
