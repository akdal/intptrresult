	.text
	.file	"CabBlockInStream.bc"
	.globl	_ZN8NArchive4NCab17CCabBlockInStream6CreateEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab17CCabBlockInStream6CreateEv,@function
_ZN8NArchive4NCab17CCabBlockInStream6CreateEv: # @_ZN8NArchive4NCab17CCabBlockInStream6CreateEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB0_2
# BB#1:
	movl	$65536, %edi            # imm = 0x10000
	callq	MyAlloc
	movq	%rax, 24(%rbx)
.LBB0_2:
	testq	%rax, %rax
	setne	%al
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN8NArchive4NCab17CCabBlockInStream6CreateEv, .Lfunc_end0-_ZN8NArchive4NCab17CCabBlockInStream6CreateEv
	.cfi_endproc

	.globl	_ZN8NArchive4NCab17CCabBlockInStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab17CCabBlockInStreamD2Ev,@function
_ZN8NArchive4NCab17CCabBlockInStreamD2Ev: # @_ZN8NArchive4NCab17CCabBlockInStreamD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NCab17CCabBlockInStreamE+16, (%rbx)
	movq	24(%rbx), %rdi
.Ltmp0:
	callq	MyFree
.Ltmp1:
# BB#1:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp6:
	callq	*16(%rax)
.Ltmp7:
.LBB1_3:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_6:
.Ltmp8:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_4:
.Ltmp2:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_7
# BB#5:
	movq	(%rdi), %rax
.Ltmp3:
	callq	*16(%rax)
.Ltmp4:
.LBB1_7:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit4
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_8:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN8NArchive4NCab17CCabBlockInStreamD2Ev, .Lfunc_end1-_ZN8NArchive4NCab17CCabBlockInStreamD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp7           #   Call between .Ltmp7 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN8NArchive4NCab17CCabBlockInStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab17CCabBlockInStreamD0Ev,@function
_ZN8NArchive4NCab17CCabBlockInStreamD0Ev: # @_ZN8NArchive4NCab17CCabBlockInStreamD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN8NArchive4NCab17CCabBlockInStreamE+16, (%rbx)
	movq	24(%rbx), %rdi
.Ltmp9:
	callq	MyFree
.Ltmp10:
# BB#1:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB3_3:                                # %_ZN8NArchive4NCab17CCabBlockInStreamD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_7:
.Ltmp17:
	movq	%rax, %r14
	jmp	.LBB3_8
.LBB3_4:
.Ltmp11:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_8
# BB#5:
	movq	(%rdi), %rax
.Ltmp12:
	callq	*16(%rax)
.Ltmp13:
.LBB3_8:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_6:
.Ltmp14:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN8NArchive4NCab17CCabBlockInStreamD0Ev, .Lfunc_end3-_ZN8NArchive4NCab17CCabBlockInStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	1                       #   On action: 1
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp13     #   Call between .Ltmp13 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI4_1:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI4_2:
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.text
	.globl	_ZN8NArchive4NCab10CCheckSum26UpdateEPKvj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab10CCheckSum26UpdateEPKvj,@function
_ZN8NArchive4NCab10CCheckSum26UpdateEPKvj: # @_ZN8NArchive4NCab10CCheckSum26UpdateEPKvj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	(%rdi), %ebp
	testl	%edx, %edx
	jne	.LBB4_3
	jmp	.LBB4_2
.LBB4_5:                                # %.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movzbl	8(%rdi), %ecx
	xorl	%ebp, %ecx
	movzbl	9(%rdi), %ebx
	shll	$8, %ebx
	xorl	%ecx, %ebx
	movzbl	10(%rdi), %ecx
	shll	$16, %ecx
	movzbl	11(%rdi), %ebp
	shll	$24, %ebp
	xorl	%ecx, %ebp
	xorl	%ebx, %ebp
	.p2align	4, 0x90
.LBB4_1:                                #   in Loop: Header=BB4_3 Depth=1
	testl	%edx, %edx
	je	.LBB4_2
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movslq	4(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB4_6
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movzbl	(%rsi), %ebx
	incq	%rsi
	movb	%bl, 8(%rdi,%rcx)
	movl	4(%rdi), %ecx
	incl	%ecx
	andl	$3, %ecx
	movl	%ecx, 4(%rdi)
	decl	%edx
	testl	%ecx, %ecx
	jne	.LBB4_1
	jmp	.LBB4_5
.LBB4_2:                                # %._crit_edge50.thread
	movl	%ebp, (%rdi)
	jmp	.LBB4_20
.LBB4_6:                                # %.critedge
	movl	%edx, %r9d
	shrl	$2, %r9d
	je	.LBB4_16
# BB#7:                                 # %.lr.ph49.preheader
	leal	-1(%r9), %r10d
	leaq	4(,%r10,4), %r8
	incq	%r10
	cmpq	$8, %r10
	jae	.LBB4_9
# BB#8:
	movq	%rsi, %rcx
	jmp	.LBB4_14
.LBB4_9:                                # %min.iters.checked
	movabsq	$8589934584, %rbx       # imm = 0x1FFFFFFF8
	movq	%r10, %rcx
	andq	%rbx, %rcx
	movq	%r10, %r11
	andq	%rbx, %r11
	je	.LBB4_10
# BB#11:                                # %vector.ph
	subl	%ecx, %r9d
	leaq	(%rsi,%r11,4), %rcx
	movd	%ebp, %xmm0
	movl	%edx, %eax
	shrl	$2, %eax
	decl	%eax
	incq	%rax
	andq	%rbx, %rax
	pxor	%xmm1, %xmm1
	xorl	%ebx, %ebx
	movdqa	.LCPI4_0(%rip), %xmm2   # xmm2 = [0,255,0,0,0,255,0,0,0,255,0,0,0,255,0,0]
	por	.LCPI4_1(%rip), %xmm2
	por	.LCPI4_2(%rip), %xmm2
	.p2align	4, 0x90
.LBB4_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rsi,%rbx,4), %xmm3
	movdqu	16(%rsi,%rbx,4), %xmm4
	movdqa	%xmm3, %xmm5
	pand	%xmm2, %xmm5
	movdqa	%xmm4, %xmm6
	pand	%xmm2, %xmm6
	psrld	$24, %xmm3
	psrld	$24, %xmm4
	pslld	$24, %xmm3
	pslld	$24, %xmm4
	por	%xmm5, %xmm3
	por	%xmm6, %xmm4
	pxor	%xmm3, %xmm0
	pxor	%xmm4, %xmm1
	addq	$8, %rbx
	cmpq	%rbx, %rax
	jne	.LBB4_12
# BB#13:                                # %middle.block
	pxor	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	pxor	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	pxor	%xmm0, %xmm1
	movd	%xmm1, %ebp
	cmpq	%r11, %r10
	jne	.LBB4_14
	jmp	.LBB4_15
.LBB4_10:
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph49
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %ebx
	movzbl	1(%rcx), %eax
	shll	$8, %eax
	orl	%ebx, %eax
	movzbl	2(%rcx), %ebx
	shll	$16, %ebx
	orl	%eax, %ebx
	movzbl	3(%rcx), %eax
	shll	$24, %eax
	orl	%ebx, %eax
	xorl	%eax, %ebp
	addq	$4, %rcx
	decl	%r9d
	jne	.LBB4_14
.LBB4_15:                               # %._crit_edge50.loopexit
	addq	%r8, %rsi
.LBB4_16:                               # %._crit_edge50
	movl	%ebp, (%rdi)
	andl	$3, %edx
	je	.LBB4_20
# BB#17:
	movb	(%rsi), %al
	movslq	4(%rdi), %rcx
	movb	%al, 8(%rdi,%rcx)
	movl	4(%rdi), %eax
	incl	%eax
	andl	$3, %eax
	movl	%eax, 4(%rdi)
	cmpl	$1, %edx
	je	.LBB4_20
# BB#18:
	movb	1(%rsi), %cl
	movl	%eax, %eax
	movb	%cl, 8(%rdi,%rax)
	movl	4(%rdi), %eax
	incl	%eax
	andl	$3, %eax
	movl	%eax, 4(%rdi)
	cmpl	$2, %edx
	je	.LBB4_20
# BB#19:
	movb	2(%rsi), %cl
	movl	%eax, %eax
	movb	%cl, 8(%rdi,%rax)
	movl	4(%rdi), %eax
	incl	%eax
	andl	$3, %eax
	movl	%eax, 4(%rdi)
.LBB4_20:                               # %._crit_edge
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN8NArchive4NCab10CCheckSum26UpdateEPKvj, .Lfunc_end4-_ZN8NArchive4NCab10CCheckSum26UpdateEPKvj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI5_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI5_2:
	.long	4294967288              # 0xfffffff8
	.long	4294967288              # 0xfffffff8
	.long	4294967288              # 0xfffffff8
	.long	4294967288              # 0xfffffff8
.LCPI5_3:
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
.LCPI5_4:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	_ZN8NArchive4NCab17CCabBlockInStream7PreReadERjS2_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab17CCabBlockInStream7PreReadERjS2_,@function
_ZN8NArchive4NCab17CCabBlockInStream7PreReadERjS2_: # @_ZN8NArchive4NCab17CCabBlockInStream7PreReadERjS2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 112
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$0, 16(%rsp)
	movq	16(%rbx), %rdi
	leaq	8(%rsp), %rsi
	movl	$8, %edx
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB5_2
.LBB5_1:
	movl	%ebp, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_2:
	movl	16(%rsp), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rsp)
	movzbl	8(%rsp,%rax), %edx
	movl	%edx, 52(%rsp)          # 4-byte Spill
	leal	2(%rax), %edx
	movl	%edx, 16(%rsp)
	movzbl	8(%rsp,%rcx), %ecx
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	leal	3(%rax), %ecx
	movl	%ecx, 16(%rsp)
	movzbl	8(%rsp,%rdx), %r13d
	leal	4(%rax), %edx
	movl	%edx, 16(%rsp)
	movzbl	8(%rsp,%rcx), %r12d
	leal	5(%rax), %ecx
	movl	%ecx, 16(%rsp)
	movzbl	8(%rsp,%rdx), %edx
	leal	6(%rax), %esi
	movl	%esi, 16(%rsp)
	movzbl	8(%rsp,%rcx), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movl	%ecx, (%r14)
	leal	7(%rax), %ecx
	movl	%ecx, 16(%rsp)
	movzbl	8(%rsp,%rsi), %edx
	leal	8(%rax), %eax
	movl	%eax, 16(%rsp)
	movzbl	8(%rsp,%rcx), %eax
	shll	$8, %eax
	orl	%edx, %eax
	movl	%eax, (%r15)
	movl	44(%rbx), %edx
	testq	%rdx, %rdx
	je	.LBB5_4
# BB#3:
	movq	16(%rbx), %rdi
	movq	24(%rbx), %rsi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB5_1
.LBB5_4:
	movl	%r12d, 44(%rsp)         # 4-byte Spill
	movl	$0, 32(%rbx)
	movq	$0, 32(%rsp)
	movl	(%r14), %r12d
	cmpb	$0, 49(%rbx)
	je	.LBB5_6
# BB#5:
	cmpl	$0, 36(%rbx)
	je	.LBB5_13
.LBB5_6:                                # %._crit_edge
	movl	36(%rbx), %esi
	movl	$65536, %eax            # imm = 0x10000
	subl	%esi, %eax
	movl	$1, %ebp
	cmpl	%r12d, %eax
	jb	.LBB5_1
# BB#7:
	testl	%r12d, %r12d
	je	.LBB5_10
# BB#8:
	movl	%r12d, %r12d
	movq	%r12, 24(%rsp)
	movq	16(%rbx), %rdi
	addq	24(%rbx), %rsi
	leaq	24(%rsp), %rdx
	callq	_Z10ReadStreamP19ISequentialInStreamPvPm
	testl	%eax, %eax
	jne	.LBB5_15
# BB#9:
	movl	36(%rbx), %esi
	addq	24(%rbx), %rsi
	movl	24(%rsp), %edx
	leaq	32(%rsp), %rdi
	callq	_ZN8NArchive4NCab10CCheckSum26UpdateEPKvj
	movq	24(%rsp), %rax
	movl	36(%rbx), %esi
	addl	%eax, %esi
	movl	%esi, 36(%rbx)
	cmpq	%r12, %rax
	jne	.LBB5_1
.LBB5_10:                               # %.thread66
	movl	48(%rsp), %eax          # 4-byte Reload
	shll	$8, %eax
	addl	52(%rsp), %eax          # 4-byte Folded Reload
	shll	$16, %r13d
	orl	%eax, %r13d
	movl	44(%rsp), %ebp          # 4-byte Reload
	shll	$24, %ebp
	orl	%r13d, %ebp
	movl	%esi, 40(%rbx)
	movslq	36(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB5_27
# BB#11:                                # %.lr.ph.i
	movl	32(%rsp), %esi
	cmpl	$8, %eax
	jb	.LBB5_23
# BB#16:                                # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	je	.LBB5_23
# BB#17:                                # %vector.ph
	movd	%esi, %xmm0
	movd	%eax, %xmm1
	pshufd	$0, %xmm1, %xmm13       # xmm13 = xmm1[0,0,0,0]
	movdqa	.LCPI5_0(%rip), %xmm7   # xmm7 = [0,1,2,3]
	pxor	%xmm12, %xmm12
	leaq	44(%rsp), %rcx
	movdqa	.LCPI5_1(%rip), %xmm8   # xmm8 = [4,4,4,4]
	movdqa	.LCPI5_2(%rip), %xmm10  # xmm10 = [4294967288,4294967288,4294967288,4294967288]
	movdqa	.LCPI5_3(%rip), %xmm11  # xmm11 = [1065353216,1065353216,1065353216,1065353216]
	movdqa	.LCPI5_4(%rip), %xmm9   # xmm9 = [8,8,8,8]
	movq	%rdx, %rsi
	pxor	%xmm6, %xmm6
	.p2align	4, 0x90
.LBB5_18:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movd	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm12, %xmm5   # xmm5 = xmm5[0],xmm12[0],xmm5[1],xmm12[1],xmm5[2],xmm12[2],xmm5[3],xmm12[3],xmm5[4],xmm12[4],xmm5[5],xmm12[5],xmm5[6],xmm12[6],xmm5[7],xmm12[7]
	punpcklwd	%xmm12, %xmm5   # xmm5 = xmm5[0],xmm12[0],xmm5[1],xmm12[1],xmm5[2],xmm12[2],xmm5[3],xmm12[3]
	punpcklbw	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3],xmm4[4],xmm12[4],xmm4[5],xmm12[5],xmm4[6],xmm12[6],xmm4[7],xmm12[7]
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	movdqa	%xmm7, %xmm2
	paddd	%xmm8, %xmm2
	movdqa	%xmm13, %xmm1
	psubd	%xmm7, %xmm1
	movdqa	%xmm13, %xmm3
	psubd	%xmm2, %xmm3
	pslld	$3, %xmm1
	pslld	$3, %xmm3
	paddd	%xmm10, %xmm1
	paddd	%xmm10, %xmm3
	pslld	$23, %xmm1
	paddd	%xmm11, %xmm1
	cvttps2dq	%xmm1, %xmm1
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm5, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm2      # xmm2 = xmm5[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	pslld	$23, %xmm3
	paddd	%xmm11, %xmm3
	cvttps2dq	%xmm3, %xmm2
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm3      # xmm3 = xmm4[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	pxor	%xmm1, %xmm0
	pxor	%xmm2, %xmm6
	paddd	%xmm9, %xmm7
	addq	$8, %rcx
	addq	$-8, %rsi
	jne	.LBB5_18
# BB#19:                                # %middle.block
	pxor	%xmm0, %xmm6
	pshufd	$78, %xmm6, %xmm0       # xmm0 = xmm6[2,3,0,1]
	pxor	%xmm6, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	pxor	%xmm0, %xmm1
	movd	%xmm1, %esi
	cmpq	%rdx, %rax
	jne	.LBB5_24
	jmp	.LBB5_26
.LBB5_13:
	movl	$1, %ebp
	cmpl	$2, %r12d
	jb	.LBB5_1
# BB#14:
	movq	16(%rbx), %rdi
	leaq	24(%rsp), %rsi
	movl	$2, %edx
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	testl	%eax, %eax
	je	.LBB5_20
.LBB5_15:
	movl	%eax, %ebp
	jmp	.LBB5_1
.LBB5_23:
	xorl	%edx, %edx
.LBB5_24:                               # %scalar.ph.preheader
	movq	%rax, %rdi
	subq	%rdx, %rdi
	leal	-8(,%rax,8), %ecx
	leal	(,%rdx,8), %eax
	subl	%eax, %ecx
	leaq	40(%rsp,%rdx), %rax
	.p2align	4, 0x90
.LBB5_25:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	shll	%cl, %edx
	xorl	%edx, %esi
	addl	$-8, %ecx
	incq	%rax
	decq	%rdi
	jne	.LBB5_25
.LBB5_26:                               # %_ZN8NArchive4NCab10CCheckSum216FinishDataUpdateEv.exit.loopexit
	movl	%esi, 32(%rsp)
.LBB5_27:                               # %_ZN8NArchive4NCab10CCheckSum216FinishDataUpdateEv.exit
	testl	%ebp, %ebp
	je	.LBB5_29
# BB#28:
	movl	(%r15), %eax
	shll	$16, %eax
	orl	(%r14), %eax
	xorl	32(%rsp), %eax
	movl	%eax, 32(%rsp)
	cmpl	%ebp, %eax
	setne	%al
	jmp	.LBB5_30
.LBB5_29:
	xorl	%eax, %eax
.LBB5_30:
	orb	%al, 48(%rbx)
	movzbl	%al, %ebp
	jmp	.LBB5_1
.LBB5_20:
	cmpb	$67, 24(%rsp)
	jne	.LBB5_1
# BB#21:
	cmpb	$75, 25(%rsp)
	jne	.LBB5_1
# BB#22:                                # %.thread64
	addl	$-2, %r12d
	leaq	32(%rsp), %rdi
	leaq	24(%rsp), %rsi
	movl	$2, %edx
	callq	_ZN8NArchive4NCab10CCheckSum26UpdateEPKvj
	jmp	.LBB5_6
.Lfunc_end5:
	.size	_ZN8NArchive4NCab17CCabBlockInStream7PreReadERjS2_, .Lfunc_end5-_ZN8NArchive4NCab17CCabBlockInStream7PreReadERjS2_
	.cfi_endproc

	.globl	_ZN8NArchive4NCab17CCabBlockInStream4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab17CCabBlockInStream4ReadEPvjPj,@function
_ZN8NArchive4NCab17CCabBlockInStream4ReadEPvjPj: # @_ZN8NArchive4NCab17CCabBlockInStream4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movl	%edx, %ebx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB6_2
# BB#1:
	movl	$0, (%r14)
.LBB6_2:
	testl	%ebx, %ebx
	je	.LBB6_6
# BB#3:
	movl	36(%r15), %eax
	testl	%eax, %eax
	je	.LBB6_6
# BB#4:
	cmpl	%ebx, %eax
	cmovbl	%eax, %ebx
	movl	32(%r15), %eax
	addq	24(%r15), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	callq	memmove
	addl	%ebx, 32(%r15)
	subl	%ebx, 36(%r15)
	testq	%r14, %r14
	je	.LBB6_6
# BB#5:
	movl	%ebx, (%r14)
.LBB6_6:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN8NArchive4NCab17CCabBlockInStream4ReadEPvjPj, .Lfunc_end6-_ZN8NArchive4NCab17CCabBlockInStream4ReadEPvjPj
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab17CCabBlockInStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NCab17CCabBlockInStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NCab17CCabBlockInStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab17CCabBlockInStream14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NCab17CCabBlockInStream14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NCab17CCabBlockInStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi35:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB7_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB7_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB7_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB7_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB7_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB7_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB7_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB7_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB7_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB7_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB7_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB7_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB7_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB7_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB7_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB7_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB7_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end7:
	.size	_ZN8NArchive4NCab17CCabBlockInStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end7-_ZN8NArchive4NCab17CCabBlockInStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab17CCabBlockInStream6AddRefEv,"axG",@progbits,_ZN8NArchive4NCab17CCabBlockInStream6AddRefEv,comdat
	.weak	_ZN8NArchive4NCab17CCabBlockInStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab17CCabBlockInStream6AddRefEv,@function
_ZN8NArchive4NCab17CCabBlockInStream6AddRefEv: # @_ZN8NArchive4NCab17CCabBlockInStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end8:
	.size	_ZN8NArchive4NCab17CCabBlockInStream6AddRefEv, .Lfunc_end8-_ZN8NArchive4NCab17CCabBlockInStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NCab17CCabBlockInStream7ReleaseEv,"axG",@progbits,_ZN8NArchive4NCab17CCabBlockInStream7ReleaseEv,comdat
	.weak	_ZN8NArchive4NCab17CCabBlockInStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab17CCabBlockInStream7ReleaseEv,@function
_ZN8NArchive4NCab17CCabBlockInStream7ReleaseEv: # @_ZN8NArchive4NCab17CCabBlockInStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB9_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB9_2:
	popq	%rcx
	retq
.Lfunc_end9:
	.size	_ZN8NArchive4NCab17CCabBlockInStream7ReleaseEv, .Lfunc_end9-_ZN8NArchive4NCab17CCabBlockInStream7ReleaseEv
	.cfi_endproc

	.type	_ZTVN8NArchive4NCab17CCabBlockInStreamE,@object # @_ZTVN8NArchive4NCab17CCabBlockInStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive4NCab17CCabBlockInStreamE
	.p2align	3
_ZTVN8NArchive4NCab17CCabBlockInStreamE:
	.quad	0
	.quad	_ZTIN8NArchive4NCab17CCabBlockInStreamE
	.quad	_ZN8NArchive4NCab17CCabBlockInStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NCab17CCabBlockInStream6AddRefEv
	.quad	_ZN8NArchive4NCab17CCabBlockInStream7ReleaseEv
	.quad	_ZN8NArchive4NCab17CCabBlockInStreamD2Ev
	.quad	_ZN8NArchive4NCab17CCabBlockInStreamD0Ev
	.quad	_ZN8NArchive4NCab17CCabBlockInStream4ReadEPvjPj
	.size	_ZTVN8NArchive4NCab17CCabBlockInStreamE, 64

	.type	_ZTSN8NArchive4NCab17CCabBlockInStreamE,@object # @_ZTSN8NArchive4NCab17CCabBlockInStreamE
	.globl	_ZTSN8NArchive4NCab17CCabBlockInStreamE
	.p2align	4
_ZTSN8NArchive4NCab17CCabBlockInStreamE:
	.asciz	"N8NArchive4NCab17CCabBlockInStreamE"
	.size	_ZTSN8NArchive4NCab17CCabBlockInStreamE, 36

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive4NCab17CCabBlockInStreamE,@object # @_ZTIN8NArchive4NCab17CCabBlockInStreamE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NCab17CCabBlockInStreamE
	.p2align	4
_ZTIN8NArchive4NCab17CCabBlockInStreamE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NCab17CCabBlockInStreamE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN8NArchive4NCab17CCabBlockInStreamE, 56


	.globl	_ZN8NArchive4NCab17CCabBlockInStreamD1Ev
	.type	_ZN8NArchive4NCab17CCabBlockInStreamD1Ev,@function
_ZN8NArchive4NCab17CCabBlockInStreamD1Ev = _ZN8NArchive4NCab17CCabBlockInStreamD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
