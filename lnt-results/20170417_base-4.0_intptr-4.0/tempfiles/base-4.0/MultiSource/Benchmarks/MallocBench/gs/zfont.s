	.text
	.file	"zfont.bc"
	.globl	zfont_init
	.p2align	4, 0x90
	.type	zfont_init,@function
zfont_init:                             # @zfont_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$alloc, %edi
	movl	$alloc_free, %esi
	callq	gs_font_dir_alloc
	movq	%rax, ifont_dir(%rip)
	movl	$.L.str, %edi
	movl	$name_FontDirectory, %esi
	callq	name_enter
	movl	$.L.str.1, %edi
	movl	$name_FontMatrix, %esi
	callq	name_enter
	movl	$.L.str.2, %edi
	movl	$name_FontType, %esi
	callq	name_enter
	movl	$.L.str.3, %edi
	movl	$name_FontBBox, %esi
	callq	name_enter
	movl	$.L.str.4, %edi
	movl	$name_Encoding, %esi
	callq	name_enter
	movl	$.L.str.5, %edi
	movl	$name_PaintType, %esi
	callq	name_enter
	movl	$.L.str.6, %edi
	movl	$name_UniqueID, %esi
	callq	name_enter
	movl	$.L.str.7, %edi
	movl	$name_BuildChar, %esi
	callq	name_enter
	movl	$.L.str.8, %edi
	movl	$name_Type1BuildChar, %esi
	callq	name_enter
	movl	$.L.str.9, %edi
	movl	$name_Private, %esi
	callq	name_enter
	movl	$.L.str.10, %edi
	movl	$name_CharStrings, %esi
	callq	name_enter
	movl	$.L.str.11, %edi
	movl	$name_FID, %esi
	callq	name_enter
	movl	$.L.str.12, %edi
	movl	$name_Subrs, %esi
	callq	name_enter
	movl	$.L.str.13, %edi
	movl	$name_lenIV, %esi
	popq	%rax
	jmp	name_enter              # TAILCALL
.Lfunc_end0:
	.size	zfont_init, .Lfunc_end0-zfont_init
	.cfi_endproc

	.globl	zdefinefont
	.p2align	4, 0x90
	.type	zdefinefont,@function
zdefinefont:                            # @zdefinefont
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 288
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$-17, %ebp
	cmpq	%rbx, osp_nargs+8(%rip)
	ja	.LBB1_12
# BB#1:
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$8, %eax
	jne	.LBB1_12
# BB#2:
	leaq	104(%rsp), %rcx
	movl	$dstack, %edi
	movl	$dstack, %esi
	movl	$name_FontDirectory, %edx
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_11
# BB#3:
	leaq	96(%rsp), %rcx
	movl	$name_FontMatrix, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_11
# BB#4:
	leaq	40(%rsp), %rcx
	movl	$name_FontType, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_11
# BB#5:
	movq	40(%rsp), %rax
	movzwl	8(%rax), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB1_11
# BB#6:
	cmpq	$255, (%rax)
	ja	.LBB1_11
# BB#13:
	leaq	88(%rsp), %rcx
	movl	$name_FontBBox, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_11
# BB#14:
	leaq	64(%rsp), %rcx
	movl	$name_Encoding, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_11
# BB#15:
	movq	96(%rsp), %rdi
	leaq	136(%rsp), %rsi
	xorl	%eax, %eax
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB1_11
# BB#16:
	movq	88(%rsp), %rax
	movb	8(%rax), %cl
	shrb	$2, %cl
	cmpb	$10, %cl
	je	.LBB1_18
# BB#17:
	testb	%cl, %cl
	jne	.LBB1_11
.LBB1_18:
	movq	64(%rsp), %rcx
	movb	8(%rcx), %cl
	shrb	$2, %cl
	cmpb	$10, %cl
	je	.LBB1_20
# BB#19:
	testb	%cl, %cl
	jne	.LBB1_11
.LBB1_20:
	movzwl	10(%rax), %ecx
	cmpl	$4, %ecx
	jne	.LBB1_11
# BB#21:
	movq	(%rax), %rdi
	addq	$48, %rdi
	leaq	112(%rsp), %rdx
	movl	$4, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB1_11
# BB#22:
	leaq	80(%rsp), %rcx
	movl	$name_PaintType, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_33
# BB#23:
	movq	80(%rsp), %rax
	movzwl	8(%rax), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB1_11
# BB#24:
	movq	(%rax), %r12
	cmpq	$65535, %r12            # imm = 0xFFFF
	ja	.LBB1_11
	jmp	.LBB1_34
.LBB1_33:
	movl	$-1, %r12d
.LBB1_34:
	leaq	24(%rsp), %rcx
	movl	$name_UniqueID, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_39
# BB#35:
	movq	24(%rsp), %rax
	movzwl	8(%rax), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB1_11
# BB#36:
	movq	(%rax), %r13
	cmpq	$16777215, %r13         # imm = 0xFFFFFF
	ja	.LBB1_11
	jmp	.LBB1_40
.LBB1_39:
	movq	$-1, %r13
.LBB1_40:
	leaq	16(%rsp), %rcx
	movl	$name_BuildChar, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	movq	$0, zdefinefont.no_subrs(%rip)
	movw	$52, zdefinefont.no_subrs+8(%rip)
	movw	$0, zdefinefont.no_subrs+10(%rip)
	movq	$zdefinefont.no_subrs, 8(%rsp)
	movq	$0, zdefinefont.no_charstrings(%rip)
	movw	$32, zdefinefont.no_charstrings+8(%rip)
	movq	$zdefinefont.no_charstrings, 32(%rsp)
	movq	40(%rsp), %rcx
	cmpq	$1, (%rcx)
	jne	.LBB1_44
# BB#41:
	testl	%eax, %eax
	jg	.LBB1_11
# BB#52:
	leaq	32(%rsp), %rcx
	movl	$name_CharStrings, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_11
# BB#53:
	movq	32(%rsp), %rax
	movzwl	8(%rax), %eax
	andl	$252, %eax
	cmpl	$8, %eax
	jne	.LBB1_11
# BB#54:
	leaq	48(%rsp), %rcx
	movl	$name_Private, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_11
# BB#55:
	movq	48(%rsp), %rdi
	movzwl	8(%rdi), %eax
	andl	$252, %eax
	cmpl	$8, %eax
	jne	.LBB1_11
# BB#56:
	leaq	8(%rsp), %rcx
	movl	$name_Subrs, %edx
	movq	%rdi, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_59
# BB#57:
	movq	8(%rsp), %rax
	movb	8(%rax), %al
	shrb	$2, %al
	cmpb	$10, %al
	je	.LBB1_59
# BB#58:
	testb	%al, %al
	jne	.LBB1_11
.LBB1_59:
	movq	48(%rsp), %rdi
	leaq	72(%rsp), %rcx
	movl	$name_lenIV, %edx
	movq	%rdi, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_68
# BB#60:
	movq	72(%rsp), %rax
	movzwl	8(%rax), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB1_11
# BB#61:
	movq	(%rax), %rcx
	movq	%rcx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	cmpq	$255, %rcx
	ja	.LBB1_11
	jmp	.LBB1_69
.LBB1_44:
	testl	%eax, %eax
	jle	.LBB1_11
# BB#45:
	movq	16(%rsp), %rax
	movw	8(%rax), %ax
	movl	%eax, %ecx
	shrb	$2, %cl
	cmpb	$10, %cl
	je	.LBB1_47
# BB#46:
	testb	%cl, %cl
	jne	.LBB1_12
.LBB1_47:
	andl	$3, %eax
	movl	$-7, %ebp
	cmpl	$3, %eax
                                        # implicit-def: %EAX
	movq	%rax, (%rsp)            # 8-byte Spill
	jne	.LBB1_12
.LBB1_48:
	leaq	72(%rsp), %rcx
	movl	$name_FID, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	movzwl	8(%rbx), %ecx
	testb	$1, %ch
	jne	.LBB1_63
# BB#49:
	testl	%eax, %eax
	movl	$-10, %ebp
	jle	.LBB1_12
.LBB1_50:
	andl	$65279, %ecx            # imm = 0xFEFF
	movw	%cx, 8(%rbx)
	movq	104(%rsp), %rdi
	leaq	-16(%rbx), %r14
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	dict_put
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB1_12
# BB#51:
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r14)
	addq	$-16, osp(%rip)
	xorl	%ebp, %ebp
	jmp	.LBB1_12
.LBB1_11:
	movl	$-10, %ebp
.LBB1_12:                               # %.thread95
	movl	%ebp, %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_63:
	testl	%eax, %eax
	movl	$-10, %ebp
	jg	.LBB1_12
# BB#64:
	movl	$1, %edi
	movl	$184, %esi
	movl	$.L.str.14, %edx
	callq	alloc
	movq	%rax, %r14
	movl	$-25, %ebp
	testq	%r14, %r14
	je	.LBB1_12
# BB#65:
	movl	$1, %edi
	movl	$112, %esi
	movl	$.L.str.15, %edx
	callq	alloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_12
# BB#66:
	movq	%r14, 48(%rsp)
	movw	$16, 56(%rsp)
	leaq	48(%rsp), %rdx
	movl	$name_FID, %esi
	movq	%rbx, %rdi
	callq	dict_put
	movl	%eax, %ebp
	testl	%ebp, %ebp
	js	.LBB1_12
# BB#67:
	movups	(%rbx), %xmm0
	movups	%xmm0, (%r15)
	movq	16(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 16(%r15)
	movq	64(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%r15)
	movq	32(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 48(%r15)
	movq	8(%rsp), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 64(%r15)
	movq	$z1_subr_proc, 80(%r15)
	movq	$z1_pop_proc, 88(%r15)
	movq	%r15, 96(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, 104(%r15)
	movq	%r14, 16(%r14)
	movq	ifont_dir(%rip), %rax
	movq	%rax, 24(%r14)
	movq	%r15, 32(%r14)
	movups	216(%rsp), %xmm0
	movups	%xmm0, 120(%r14)
	movups	200(%rsp), %xmm0
	movups	%xmm0, 104(%r14)
	movups	136(%rsp), %xmm0
	movups	152(%rsp), %xmm1
	movups	168(%rsp), %xmm2
	movups	184(%rsp), %xmm3
	movups	%xmm3, 88(%r14)
	movups	%xmm2, 72(%r14)
	movups	%xmm1, 56(%r14)
	movups	%xmm0, 40(%r14)
	movq	40(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, 136(%r14)
	movaps	112(%rsp), %xmm0
	movups	%xmm0, 140(%r14)
	movq	$gs_no_build_char_proc, 168(%r14)
	movl	%r12d, 156(%r14)
	movq	%r13, 160(%r14)
	movw	8(%rbx), %cx
	jmp	.LBB1_50
.LBB1_68:
	movl	$4, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB1_69:
	testq	%r13, %r13
	js	.LBB1_73
# BB#70:
	movq	48(%rsp), %rdi
	leaq	24(%rsp), %rcx
	movl	$name_UniqueID, %edx
	movq	%rdi, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	jle	.LBB1_73
# BB#71:
	movq	24(%rsp), %rax
	movzwl	8(%rax), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB1_73
# BB#72:
	cmpq	%r13, (%rax)
	je	.LBB1_74
.LBB1_73:
	movq	$-1, %r13
.LBB1_74:
	movq	$name_Type1BuildChar, 16(%rsp)
	orb	$1, name_Type1BuildChar+8(%rip)
	jmp	.LBB1_48
.Lfunc_end1:
	.size	zdefinefont, .Lfunc_end1-zdefinefont
	.cfi_endproc

	.globl	add_FID
	.p2align	4, 0x90
	.type	add_FID,@function
add_FID:                                # @add_FID
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 32
	movq	%rsi, 8(%rsp)
	movw	$16, 16(%rsp)
	leaq	8(%rsp), %rdx
	movl	$name_FID, %esi
	callq	dict_put
	addq	$24, %rsp
	retq
.Lfunc_end2:
	.size	add_FID, .Lfunc_end2-add_FID
	.cfi_endproc

	.globl	zscalefont
	.p2align	4, 0x90
	.type	zscalefont,@function
zscalefont:                             # @zscalefont
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 128
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	12(%rsp), %rdx
	movl	$1, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB3_3
# BB#1:
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	leaq	16(%rsp), %rdi
	movaps	%xmm0, %xmm1
	callq	gs_make_scaling
	testl	%eax, %eax
	js	.LBB3_3
# BB#2:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	make_font
.LBB3_3:
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end3:
	.size	zscalefont, .Lfunc_end3-zscalefont
	.cfi_endproc

	.globl	make_font
	.p2align	4, 0x90
	.type	make_font,@function
make_font:                              # @make_font
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 96
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	$6, 8(%rsp)
	movw	$20, 16(%rsp)
	movzwl	-8(%r15), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$8, %ecx
	jne	.LBB4_11
# BB#1:
	leaq	-16(%r15), %r14
	leaq	40(%rsp), %rcx
	movl	$name_FID, %edx
	movq	%r14, %rdi
	movq	%r14, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	js	.LBB4_11
# BB#2:
	movq	40(%rsp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB4_3
# BB#4:
	movq	ifont_dir(%rip), %rdi
	movq	%rsp, %rcx
	leaq	56(%rsp), %r8
	movq	%rbx, %rdx
	callq	gs_makefont
	testl	%eax, %eax
	js	.LBB4_11
# BB#5:
	movq	%r14, %rdi
	callq	dict_maxlength
	leaq	24(%rsp), %rsi
	movl	%eax, %edi
	callq	dict_create
	testl	%eax, %eax
	js	.LBB4_11
# BB#6:
	leaq	24(%rsp), %rsi
	movq	%r14, %rdi
	callq	dict_copy
	testl	%eax, %eax
	js	.LBB4_11
# BB#7:
	leaq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	zarray
	testl	%eax, %eax
	js	.LBB4_11
# BB#8:
	leaq	24(%rsp), %rdi
	leaq	8(%rsp), %rdx
	movl	$name_FontMatrix, %esi
	callq	dict_put
	testl	%eax, %eax
	js	.LBB4_11
# BB#9:
	movq	(%rsp), %rax
	movq	%rax, 40(%rsp)
	movw	$16, 48(%rsp)
	leaq	24(%rsp), %rdi
	leaq	40(%rsp), %rdx
	movl	$name_FID, %esi
	callq	dict_put
	testl	%eax, %eax
	js	.LBB4_11
# BB#10:
	movq	8(%rsp), %rax
	movq	(%rsp), %rcx
	movups	120(%rcx), %xmm0
	movups	%xmm0, 80(%rax)
	movups	104(%rcx), %xmm0
	movups	%xmm0, 64(%rax)
	movups	40(%rcx), %xmm0
	movups	56(%rcx), %xmm1
	movups	72(%rcx), %xmm2
	movups	88(%rcx), %xmm3
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movups	24(%rsp), %xmm0
	movups	%xmm0, (%r14)
	andb	$-2, -7(%r15)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB4_11
.LBB4_3:
	movl	$-10, %eax
.LBB4_11:
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	make_font, .Lfunc_end4-make_font
	.cfi_endproc

	.globl	zmakefont
	.p2align	4, 0x90
	.type	zmakefont,@function
zmakefont:                              # @zmakefont
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
	subq	$96, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 112
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsp, %rsi
	xorl	%eax, %eax
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB5_2
# BB#1:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	make_font
.LBB5_2:
	addq	$96, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	zmakefont, .Lfunc_end5-zmakefont
	.cfi_endproc

	.globl	zsetfont
	.p2align	4, 0x90
	.type	zsetfont,@function
zsetfont:                               # @zsetfont
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$8, %ecx
	jne	.LBB6_6
# BB#1:
	leaq	8(%rsp), %rcx
	movl	$name_FID, %edx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	js	.LBB6_6
# BB#2:
	movq	8(%rsp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB6_3
# BB#4:
	movq	igs(%rip), %rdi
	callq	gs_setfont
	testl	%eax, %eax
	js	.LBB6_6
# BB#5:
	movups	(%rbx), %xmm0
	movups	%xmm0, istate+56(%rip)
	addq	$-16, osp(%rip)
	jmp	.LBB6_6
.LBB6_3:
	movl	$-10, %eax
.LBB6_6:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	zsetfont, .Lfunc_end6-zsetfont
	.cfi_endproc

	.globl	font_param
	.p2align	4, 0x90
	.type	font_param,@function
font_param:                             # @font_param
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$8, %ecx
	jne	.LBB7_3
# BB#1:
	leaq	8(%rsp), %rcx
	movl	$name_FID, %edx
	movq	%rdi, %rsi
	callq	dict_lookup
	testl	%eax, %eax
	js	.LBB7_3
# BB#2:
	movq	8(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$-10, %eax
	cmovnel	%ecx, %eax
.LBB7_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end7:
	.size	font_param, .Lfunc_end7-font_param
	.cfi_endproc

	.globl	zcurrentfont
	.p2align	4, 0x90
	.type	zcurrentfont,@function
zcurrentfont:                           # @zcurrentfont
	.cfi_startproc
# BB#0:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB8_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB8_2:
	movups	istate+56(%rip), %xmm0
	movups	%xmm0, (%rax)
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	zcurrentfont, .Lfunc_end8-zcurrentfont
	.cfi_endproc

	.globl	zcachestatus
	.p2align	4, 0x90
	.type	zcachestatus,@function
zcachestatus:                           # @zcachestatus
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 48
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	ifont_dir(%rip), %rdi
	movq	%rsp, %rsi
	callq	gs_cachestatus
	leaq	112(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB9_1
# BB#2:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB9_3
.LBB9_1:                                # %.lr.ph.i.preheader
	movl	(%rsp), %eax
	movq	%rax, 16(%rbx)
	movw	$20, 24(%rbx)
	movl	4(%rsp), %eax
	movq	%rax, 32(%rbx)
	movw	$20, 40(%rbx)
	movl	8(%rsp), %eax
	movq	%rax, 48(%rbx)
	movw	$20, 56(%rbx)
	movl	12(%rsp), %eax
	movq	%rax, 64(%rbx)
	movw	$20, 72(%rbx)
	movl	16(%rsp), %eax
	movq	%rax, 80(%rbx)
	movw	$20, 88(%rbx)
	movl	20(%rsp), %eax
	movq	%rax, 96(%rbx)
	movw	$20, 104(%rbx)
	movl	24(%rsp), %eax
	movq	%rax, 112(%rbx)
	movw	$20, 120(%rbx)
	xorl	%eax, %eax
.LBB9_3:                                # %make_uint_array.exit
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end9:
	.size	zcachestatus, .Lfunc_end9-zcachestatus
	.cfi_endproc

	.globl	make_uint_array
	.p2align	4, 0x90
	.type	make_uint_array,@function
make_uint_array:                        # @make_uint_array
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	jle	.LBB10_6
# BB#1:                                 # %.lr.ph.preheader
	leal	-1(%rdx), %r8d
	movl	%edx, %r9d
	xorl	%ecx, %ecx
	andl	$3, %r9d
	je	.LBB10_3
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	movq	%rax, (%rdi)
	movw	$20, 8(%rdi)
	incl	%ecx
	addq	$16, %rdi
	addq	$4, %rsi
	cmpl	%ecx, %r9d
	jne	.LBB10_2
.LBB10_3:                               # %.lr.ph.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB10_6
# BB#4:                                 # %.lr.ph.preheader.new
	subl	%ecx, %edx
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	movq	%rax, (%rdi)
	movw	$20, 8(%rdi)
	movl	4(%rsi), %eax
	movq	%rax, 16(%rdi)
	movw	$20, 24(%rdi)
	movl	8(%rsi), %eax
	movq	%rax, 32(%rdi)
	movw	$20, 40(%rdi)
	movl	12(%rsi), %eax
	movq	%rax, 48(%rdi)
	movw	$20, 56(%rdi)
	addq	$16, %rsi
	addq	$64, %rdi
	addl	$-4, %edx
	jne	.LBB10_5
.LBB10_6:                               # %._crit_edge
	retq
.Lfunc_end10:
	.size	make_uint_array, .Lfunc_end10-make_uint_array
	.cfi_endproc

	.globl	zsetcachelimit
	.p2align	4, 0x90
	.type	zsetcachelimit,@function
zsetcachelimit:                         # @zsetcachelimit
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
.Lcfi38:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	callq	num_params
	movl	%eax, %ecx
	cmpl	$1, %ecx
	jne	.LBB11_1
# BB#2:
	movq	(%rbx), %rsi
	movq	%rsi, %rax
	shrq	$32, %rax
	movl	$-15, %eax
	jne	.LBB11_4
# BB#3:
	movq	ifont_dir(%rip), %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	gs_setcachelimit
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB11_4:
	popq	%rbx
	retq
.LBB11_1:
	testl	%ecx, %ecx
	movl	$-20, %eax
	cmovsl	%ecx, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	zsetcachelimit, .Lfunc_end11-zsetcachelimit
	.cfi_endproc

	.globl	zsetcacheparams
	.p2align	4, 0x90
	.type	zsetcacheparams,@function
zsetcacheparams:                        # @zsetcacheparams
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 32
.Lcfi42:
	.cfi_offset %rbx, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	cmpl	$24, %eax
	jne	.LBB12_1
.LBB12_10:                              # %.critedge.thread
	xorl	%eax, %eax
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	zcleartomark            # TAILCALL
.LBB12_1:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	num_params
	cmpl	$1, %eax
	jne	.LBB12_2
# BB#3:
	movq	(%rbx), %r14
	movq	%r14, %rax
	shrq	$32, %rax
	movl	$-15, %ebp
	jne	.LBB12_9
# BB#4:
	movzwl	-8(%rbx), %eax
	andl	$252, %eax
	cmpl	$24, %eax
	jne	.LBB12_5
.LBB12_8:                               # %.critedge
	movq	ifont_dir(%rip), %rdi
	movl	%r14d, %esi
	callq	gs_setcacheupper
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jns	.LBB12_10
	jmp	.LBB12_9
.LBB12_5:
	leaq	-16(%rbx), %rdi
	movl	$1, %esi
	xorl	%edx, %edx
	callq	num_params
	cmpl	$1, %eax
	jne	.LBB12_2
# BB#6:
	movq	-16(%rbx), %rsi
	movq	%rsi, %rax
	shrq	$32, %rax
	jne	.LBB12_9
# BB#7:
	movq	ifont_dir(%rip), %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	gs_setcachelower
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jns	.LBB12_8
	jmp	.LBB12_9
.LBB12_2:
	testl	%eax, %eax
	movl	$-20, %ebp
	cmovsl	%eax, %ebp
.LBB12_9:                               # %.thread
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	zsetcacheparams, .Lfunc_end12-zsetcacheparams
	.cfi_endproc

	.globl	zcurrentcacheparams
	.p2align	4, 0x90
	.type	zcurrentcacheparams,@function
zcurrentcacheparams:                    # @zcurrentcacheparams
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	ifont_dir(%rip), %rdi
	callq	gs_currentcachelower
	movl	%eax, %ebp
	movq	ifont_dir(%rip), %rdi
	callq	gs_currentcacheupper
	leaq	48(%rbx), %rcx
	movq	%rcx, osp(%rip)
	cmpq	ostop(%rip), %rcx
	jbe	.LBB13_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB13_3
.LBB13_2:                               # %.lr.ph.i
	movq	$0, 16(%rbx)
	movw	$24, 24(%rbx)
	movl	%ebp, %ecx
	movq	%rcx, 32(%rbx)
	movw	$20, 40(%rbx)
	movl	%eax, %eax
	movq	%rax, 48(%rbx)
	movw	$20, 56(%rbx)
	xorl	%eax, %eax
.LBB13_3:                               # %make_uint_array.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	zcurrentcacheparams, .Lfunc_end13-zcurrentcacheparams
	.cfi_endproc

	.globl	zfont_op_init
	.p2align	4, 0x90
	.type	zfont_op_init,@function
zfont_op_init:                          # @zfont_op_init
	.cfi_startproc
# BB#0:
	movl	$zfont_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end14:
	.size	zfont_op_init, .Lfunc_end14-zfont_op_init
	.cfi_endproc

	.type	ifont_dir,@object       # @ifont_dir
	.comm	ifont_dir,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"FontDirectory"
	.size	.L.str, 14

	.type	name_FontDirectory,@object # @name_FontDirectory
	.comm	name_FontDirectory,16,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"FontMatrix"
	.size	.L.str.1, 11

	.type	name_FontMatrix,@object # @name_FontMatrix
	.comm	name_FontMatrix,16,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"FontType"
	.size	.L.str.2, 9

	.type	name_FontType,@object   # @name_FontType
	.comm	name_FontType,16,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"FontBBox"
	.size	.L.str.3, 9

	.type	name_FontBBox,@object   # @name_FontBBox
	.comm	name_FontBBox,16,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Encoding"
	.size	.L.str.4, 9

	.type	name_Encoding,@object   # @name_Encoding
	.comm	name_Encoding,16,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"PaintType"
	.size	.L.str.5, 10

	.type	name_PaintType,@object  # @name_PaintType
	.comm	name_PaintType,16,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"UniqueID"
	.size	.L.str.6, 9

	.type	name_UniqueID,@object   # @name_UniqueID
	.comm	name_UniqueID,16,8
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"BuildChar"
	.size	.L.str.7, 10

	.type	name_BuildChar,@object  # @name_BuildChar
	.comm	name_BuildChar,16,8
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Type1BuildChar"
	.size	.L.str.8, 15

	.type	name_Type1BuildChar,@object # @name_Type1BuildChar
	.comm	name_Type1BuildChar,16,8
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Private"
	.size	.L.str.9, 8

	.type	name_Private,@object    # @name_Private
	.comm	name_Private,16,8
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"CharStrings"
	.size	.L.str.10, 12

	.type	name_CharStrings,@object # @name_CharStrings
	.comm	name_CharStrings,16,8
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"FID"
	.size	.L.str.11, 4

	.type	name_FID,@object        # @name_FID
	.comm	name_FID,16,8
	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Subrs"
	.size	.L.str.12, 6

	.type	name_Subrs,@object      # @name_Subrs
	.comm	name_Subrs,16,8
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"lenIV"
	.size	.L.str.13, 6

	.type	name_lenIV,@object      # @name_lenIV
	.comm	name_lenIV,16,8
	.type	zdefinefont.no_subrs,@object # @zdefinefont.no_subrs
	.local	zdefinefont.no_subrs
	.comm	zdefinefont.no_subrs,16,8
	.type	zdefinefont.no_charstrings,@object # @zdefinefont.no_charstrings
	.local	zdefinefont.no_charstrings
	.comm	zdefinefont.no_charstrings,16,8
	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"definefont(font)"
	.size	.L.str.14, 17

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"definefont(data)"
	.size	.L.str.15, 17

	.type	zfont_op_init.my_defs,@object # @zfont_op_init.my_defs
	.data
	.p2align	4
zfont_op_init.my_defs:
	.quad	.L.str.16
	.quad	zcurrentfont
	.quad	.L.str.17
	.quad	zdefinefont
	.quad	.L.str.18
	.quad	zmakefont
	.quad	.L.str.19
	.quad	zscalefont
	.quad	.L.str.20
	.quad	zsetfont
	.quad	.L.str.21
	.quad	zcachestatus
	.quad	.L.str.22
	.quad	zsetcachelimit
	.quad	.L.str.23
	.quad	zsetcacheparams
	.quad	.L.str.24
	.quad	zcurrentcacheparams
	.zero	16
	.size	zfont_op_init.my_defs, 160

	.type	.L.str.16,@object       # @.str.16
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.16:
	.asciz	"0currentfont"
	.size	.L.str.16, 13

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"2definefont"
	.size	.L.str.17, 12

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"2makefont"
	.size	.L.str.18, 10

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"2scalefont"
	.size	.L.str.19, 11

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"1setfont"
	.size	.L.str.20, 9

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"0cachestatus"
	.size	.L.str.21, 13

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"1setcachelimit"
	.size	.L.str.22, 15

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"1setcacheparams"
	.size	.L.str.23, 16

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"0currentcacheparams"
	.size	.L.str.24, 20


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
