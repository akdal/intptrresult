	.text
	.file	"Divsol.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4611686018427387904     # double 2
	.text
	.globl	HouseMatrix
	.p2align	4, 0x90
	.type	HouseMatrix,@function
HouseMatrix:                            # @HouseMatrix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	xty
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%r14, %rdi
	callq	MakeID
	cmpl	%r15d, %ebp
	jg	.LBB0_12
# BB#1:                                 # %.preheader.preheader
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movslq	%ebp, %r9
	movslq	%r15d, %rcx
	cmpq	%r9, %rcx
	movq	%r9, %rax
	cmovgeq	%rcx, %rax
	leaq	1(%rax), %r8
	movq	%r8, %r12
	subq	%r9, %r12
	leaq	(%rbx,%r9,8), %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r12, %r10
	andq	$-4, %r10
	leaq	(%r9,%r10), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	2(%r9), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	16(%rbx,%r9,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #     Child Loop BB0_10 Depth 2
	cmpq	$4, %r12
	leaq	(%rbx,%rax,8), %rdx
	movq	(%r14,%rax,8), %rdi
	movq	%r9, %rbp
	jb	.LBB0_9
# BB#3:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_2 Depth=1
	testq	%r10, %r10
	movq	%r9, %rbp
	je	.LBB0_9
# BB#4:                                 # %vector.memcheck
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	(%rdi,%r9,8), %rbp
	leaq	(%rdi,%r8,8), %rsi
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	sbbb	%r11b, %r11b
	cmpq	%rsi, (%rsp)            # 8-byte Folded Reload
	sbbb	%r15b, %r15b
	andb	%r11b, %r15b
	cmpq	%rdx, %rbp
	sbbb	%r11b, %r11b
	cmpq	%rsi, %rdx
	sbbb	%r13b, %r13b
	testb	$1, %r15b
	movq	%r9, %rbp
	jne	.LBB0_9
# BB#5:                                 # %vector.memcheck
                                        #   in Loop: Header=BB0_2 Depth=1
	andb	%r13b, %r11b
	andb	$1, %r11b
	movq	%r9, %rbp
	jne	.LBB0_9
# BB#6:                                 # %vector.ph
                                        #   in Loop: Header=BB0_2 Depth=1
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulpd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rdi,%rsi,8), %rbp
	movq	%r10, %r13
	movq	8(%rsp), %r11           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_7:                                # %vector.body
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%r11), %xmm2
	movupd	(%r11), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	-16(%rbp), %xmm4
	movupd	(%rbp), %xmm5
	subpd	%xmm2, %xmm4
	subpd	%xmm3, %xmm5
	movupd	%xmm4, -16(%rbp)
	movupd	%xmm5, (%rbp)
	addq	$32, %rbp
	addq	$32, %r11
	addq	$-4, %r13
	jne	.LBB0_7
# BB#8:                                 # %middle.block
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpq	%r10, %r12
	movq	24(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_11
	.p2align	4, 0x90
.LBB0_9:                                # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	decq	%rbp
	.p2align	4, 0x90
.LBB0_10:                               # %scalar.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	mulsd	8(%rbx,%rbp,8), %xmm1
	movsd	8(%rdi,%rbp,8), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%rdi,%rbp,8)
	incq	%rbp
	cmpq	%rcx, %rbp
	jl	.LBB0_10
.LBB0_11:                               # %._crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpq	%rcx, %rax
	leaq	1(%rax), %rax
	jl	.LBB0_2
.LBB0_12:                               # %._crit_edge28
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	HouseMatrix, .Lfunc_end0-HouseMatrix
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4611686018427387904     # double 2
	.text
	.globl	ApplyHouse
	.p2align	4, 0x90
	.type	ApplyHouse,@function
ApplyHouse:                             # @ApplyHouse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 64
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r13, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	newMatrix
	movq	%rax, %r15
	callq	newMatrix
	movq	%rax, %r12
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	movl	$50, %ecx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	xty
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, (%rsp)           # 16-byte Spill
	movq	%r12, %rdi
	callq	MakeID
	movapd	(%rsp), %xmm3           # 16-byte Reload
	leaq	408(%rbx), %rdi
	.p2align	4, 0x90
.LBB1_1:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #     Child Loop BB1_9 Depth 2
	leaq	(%rbx,%r13,8), %rcx
	movq	(%r12,%r13,8), %rdx
	leaq	408(%rdx), %rsi
	cmpq	%rdi, %rdx
	sbbb	%r8b, %r8b
	cmpq	%rsi, %rbx
	sbbb	%al, %al
	andb	%r8b, %al
	cmpq	%rcx, %rdx
	sbbb	%r8b, %r8b
	cmpq	%rsi, %rcx
	sbbb	%sil, %sil
	testb	$1, %al
	jne	.LBB1_2
# BB#3:                                 # %.preheader.i
                                        #   in Loop: Header=BB1_1 Depth=1
	andb	%sil, %r8b
	andb	$1, %r8b
	movl	$0, %esi
	jne	.LBB1_7
# BB#4:                                 # %vector.ph
                                        #   in Loop: Header=BB1_1 Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulpd	%xmm3, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movl	$2, %esi
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_12:                               # %vector.body.1
                                        #   in Loop: Header=BB1_5 Depth=2
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	mulpd	%xmm3, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movupd	(%rbx,%rsi,8), %xmm2
	mulpd	%xmm1, %xmm2
	movupd	(%rdx,%rsi,8), %xmm1
	subpd	%xmm2, %xmm1
	movupd	%xmm1, (%rdx,%rsi,8)
	addq	$4, %rsi
.LBB1_5:                                # %vector.body
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-16(%rbx,%rsi,8), %xmm1
	mulpd	%xmm0, %xmm1
	movupd	-16(%rdx,%rsi,8), %xmm2
	subpd	%xmm1, %xmm2
	movupd	%xmm2, -16(%rdx,%rsi,8)
	cmpq	$50, %rsi
	jne	.LBB1_12
# BB#6:                                 #   in Loop: Header=BB1_1 Depth=1
	movl	$50, %esi
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_2:                                #   in Loop: Header=BB1_1 Depth=1
	xorl	%esi, %esi
.LBB1_7:                                # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB1_1 Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	mulsd	(%rbx,%rsi,8), %xmm0
	movsd	(%rdx,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx,%rsi,8)
	cmpq	$50, %rsi
	je	.LBB1_10
# BB#8:                                 # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB1_1 Depth=1
	addq	$2, %rsi
	.p2align	4, 0x90
.LBB1_9:                                # %scalar.ph
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	mulsd	-8(%rbx,%rsi,8), %xmm0
	movsd	-8(%rdx,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%rdx,%rsi,8)
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	mulsd	(%rbx,%rsi,8), %xmm0
	movsd	(%rdx,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx,%rsi,8)
	addq	$2, %rsi
	cmpq	$52, %rsi
	jne	.LBB1_9
.LBB1_10:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_1 Depth=1
	incq	%r13
	cmpq	$51, %r13
	jne	.LBB1_1
# BB#11:                                # %HouseMatrix.exit
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	matrixMult
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	matrixMult
	movq	%r12, %rdi
	callq	freeMatrix
	movq	%r15, %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	freeMatrix              # TAILCALL
.Lfunc_end1:
	.size	ApplyHouse, .Lfunc_end1-ApplyHouse
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	WeirdHouse
	.p2align	4, 0x90
	.type	WeirdHouse,@function
WeirdHouse:                             # @WeirdHouse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 80
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movl	%ecx, %r15d
	movq	%rsi, %r13
	movslq	%edx, %r14
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	(%rdi,%r14,8), %rax
	cmpl	%ebp, %r15d
	jle	.LBB2_2
# BB#1:                                 # %.._crit_edge39_crit_edge
	movslq	%ebp, %rbx
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	jmp	.LBB2_5
.LBB2_2:                                # %.lr.ph38
	movslq	%r15d, %rcx
	movslq	%ebp, %rbx
	decq	%rcx
	xorpd	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movsd	8(%rax,%rcx,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm0, %xmm1
	incq	%rcx
	cmpq	%rbx, %rcx
	jl	.LBB2_3
# BB#4:
	movsd	%xmm1, (%rsp)           # 8-byte Spill
.LBB2_5:                                # %._crit_edge39
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	sign
	movl	%eax, %r12d
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_7
# BB#6:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB2_7:                                # %._crit_edge39.split
	cmpl	%ebp, %r15d
	jge	.LBB2_26
# BB#8:                                 # %.lr.ph
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r12d, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	movslq	%r15d, %rcx
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	cmpq	$4, %rdx
	jb	.LBB2_20
# BB#9:                                 # %min.iters.checked
	movq	%rdx, %r8
	andq	$-4, %r8
	je	.LBB2_20
# BB#10:                                # %vector.memcheck
	leaq	(%r13,%rcx,8), %rsi
	leaq	(%rax,%rbx,8), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB2_12
# BB#11:                                # %vector.memcheck
	leaq	(%r13,%rbx,8), %rsi
	leaq	(%rax,%rcx,8), %rdi
	cmpq	%rsi, %rdi
	jb	.LBB2_20
.LBB2_12:                               # %vector.ph
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	-4(%r8), %rsi
	movq	%rsi, %rdi
	shrq	$2, %rdi
	btl	$2, %esi
	jb	.LBB2_13
# BB#14:                                # %vector.body.prol
	movupd	(%rax,%rcx,8), %xmm2
	movupd	16(%rax,%rcx,8), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, (%r13,%rcx,8)
	movupd	%xmm3, 16(%r13,%rcx,8)
	movl	$4, %esi
	testq	%rdi, %rdi
	jne	.LBB2_16
	jmp	.LBB2_18
.LBB2_13:
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB2_18
.LBB2_16:                               # %vector.ph.new
	movq	%r8, %rdi
	subq	%rsi, %rdi
	addq	%rcx, %rsi
	leaq	48(%r13,%rsi,8), %rbp
	leaq	48(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB2_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-48(%rsi), %xmm2
	movupd	-32(%rsi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -48(%rbp)
	movupd	%xmm3, -32(%rbp)
	movupd	-16(%rsi), %xmm2
	movupd	(%rsi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rbp)
	movupd	%xmm3, (%rbp)
	addq	$64, %rbp
	addq	$64, %rsi
	addq	$-8, %rdi
	jne	.LBB2_17
.LBB2_18:                               # %middle.block
	cmpq	%r8, %rdx
	je	.LBB2_26
# BB#19:
	addq	%r8, %rcx
.LBB2_20:                               # %scalar.ph.preheader
	movl	%ebx, %esi
	subl	%ecx, %esi
	leaq	-1(%rbx), %rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB2_23
# BB#21:                                # %scalar.ph.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB2_22:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%r13,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_22
.LBB2_23:                               # %scalar.ph.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB2_26
# BB#24:                                # %scalar.ph.preheader.new
	movq	%rbx, %rdx
	subq	%rcx, %rdx
	leaq	24(%r13,%rcx,8), %rsi
	leaq	24(%rax,%rcx,8), %rax
	.p2align	4, 0x90
.LBB2_25:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-24(%rax), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -24(%rsi)
	movsd	-16(%rax), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -16(%rsi)
	movsd	-8(%rax), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%rsi)
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-4, %rdx
	jne	.LBB2_25
.LBB2_26:                               # %._crit_edge
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, (%r13,%rbx,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	WeirdHouse, .Lfunc_end2-WeirdHouse
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	-4616189618054758400    # double -1
.LCPI3_1:
	.quad	4632233691727265792     # double 50
.LCPI3_2:
	.quad	4607182418800017408     # double 1
.LCPI3_3:
	.quad	4457293557087583675     # double 1.0E-10
.LCPI3_4:
	.quad	4602678819172646912     # double 0.5
.LCPI3_5:
	.quad	0                       # double 0
	.text
	.globl	DivideAndSolve
	.p2align	4, 0x90
	.type	DivideAndSolve,@function
DivideAndSolve:                         # @DivideAndSolve
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 160
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	callq	newIdMatrix
	callq	newMatrix
	callq	newVector
	incl	%ebx
	cmpl	$50, %ebx
	jg	.LBB3_53
# BB#1:                                 # %.lr.ph177.preheader
	cvtsi2sdl	%ebx, %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	leaq	48(%rax), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	24(%rax), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movsd	.LCPI3_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph177
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_16 Depth 2
                                        #     Child Loop BB3_13 Depth 2
                                        #     Child Loop BB3_6 Depth 2
                                        #     Child Loop BB3_23 Depth 2
                                        #     Child Loop BB3_38 Depth 2
                                        #     Child Loop BB3_43 Depth 2
                                        #     Child Loop BB3_46 Depth 2
                                        #     Child Loop BB3_49 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	subsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %r13d
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	cvttsd2si	%xmm0, %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	cmpl	%ecx, %r13d
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movsd	%xmm3, 72(%rsp)         # 8-byte Spill
	jle	.LBB3_4
# BB#3:                                 # %._crit_edge160.thread
                                        #   in Loop: Header=BB3_2 Depth=1
	movslq	%ebx, %rax
	leaq	(%r14,%rax,8), %r15
	movq	(%r14,%rax,8), %rax
	movl	16(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB3_21
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph159
                                        #   in Loop: Header=BB3_2 Depth=1
	movapd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	minsd	.LCPI3_1(%rip), %xmm0
	cvttsd2si	%xmm0, %r15d
	leal	1(%r15), %edx
	testl	%ebx, %ebx
	movl	%r13d, 36(%rsp)         # 4-byte Spill
	jle	.LBB3_11
# BB#5:                                 # %.lr.ph159.split.us.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	leal	-1(%rbx), %ecx
	leaq	8(,%rcx,8), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	%edx, %r12d
	movslq	%edx, %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$49, %eax
	subl	%r15d, %eax
	leaq	8(,%rax,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
                                        # kill: %R13D<def> %R13D<kill> %R13<def>
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph159.split.us
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	movl	%r13d, %esi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	callq	norm
	ucomisd	.LCPI3_5, %xmm0
	movq	8(%rsp), %rbp           # 8-byte Reload
	jne	.LBB3_7
	jnp	.LBB3_9
.LBB3_7:                                # %.lr.ph.us
                                        #   in Loop: Header=BB3_6 Depth=2
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movl	%r13d, %edx
	movl	%ebx, %ecx
	movl	%r15d, %r8d
	callq	House
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	memset
	cmpl	$51, %r12d
	jge	.LBB3_8
# BB#10:                                # %.lr.ph156.us.preheader
                                        #   in Loop: Header=BB3_6 Depth=2
	xorl	%esi, %esi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	callq	memset
.LBB3_8:                                # %._crit_edge.us
                                        #   in Loop: Header=BB3_6 Depth=2
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ApplyHouse
.LBB3_9:                                #   in Loop: Header=BB3_6 Depth=2
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	callq	printf
	movq	%rbp, %rdi
	callq	printVector
	movq	%r14, %rdi
	callq	printMatrix
	cmpl	16(%rsp), %r13d         # 4-byte Folded Reload
	leal	1(%r13), %eax
	movl	%eax, %r13d
	jl	.LBB3_6
	jmp	.LBB3_19
.LBB3_11:                               # %.lr.ph159.split
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	%edx, %eax
	cmpl	$51, %edx
	movl	%r13d, %ebp
	jge	.LBB3_16
# BB#12:                                # %.lr.ph159.split.split.us.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	cltq
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$49, %eax
	subl	%r15d, %eax
	leaq	8(,%rax,8), %r13
	movl	36(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r12d
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph159.split.split.us
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	movl	%r12d, %esi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	callq	norm
	ucomisd	.LCPI3_5, %xmm0
	movq	8(%rsp), %rbp           # 8-byte Reload
	jne	.LBB3_14
	jnp	.LBB3_15
.LBB3_14:                               # %.preheader.us165
                                        #   in Loop: Header=BB3_13 Depth=2
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movl	%r12d, %edx
	movl	%ebx, %ecx
	movl	%r15d, %r8d
	callq	House
	xorl	%esi, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r13, %rdx
	callq	memset
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	ApplyHouse
.LBB3_15:                               #   in Loop: Header=BB3_13 Depth=2
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	callq	printf
	movq	%rbp, %rdi
	callq	printVector
	movq	%r14, %rdi
	callq	printMatrix
	cmpl	16(%rsp), %r12d         # 4-byte Folded Reload
	leal	1(%r12), %eax
	movl	%eax, %r12d
	jl	.LBB3_13
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph159.split.split
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	movl	%ebp, %esi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	callq	norm
	ucomisd	.LCPI3_5, %xmm0
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB3_17
	jnp	.LBB3_18
.LBB3_17:                               # %.preheader
                                        #   in Loop: Header=BB3_16 Depth=2
	movq	%r14, %rdi
	movq	%r12, %rsi
	movl	%ebp, %edx
	movl	%ebx, %ecx
	movl	%r15d, %r8d
	callq	House
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	ApplyHouse
.LBB3_18:                               #   in Loop: Header=BB3_16 Depth=2
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	callq	printf
	movq	%r12, %rdi
	callq	printVector
	movq	%r14, %rdi
	callq	printMatrix
	cmpl	16(%rsp), %ebp          # 4-byte Folded Reload
	leal	1(%rbp), %eax
	movl	%eax, %ebp
	jl	.LBB3_16
	.p2align	4, 0x90
.LBB3_19:                               # %._crit_edge160
                                        #   in Loop: Header=BB3_2 Depth=1
	movslq	%ebx, %rax
	leaq	(%r14,%rax,8), %r15
	movq	(%r14,%rax,8), %rax
	movl	36(%rsp), %r13d         # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %r13d
	jle	.LBB3_22
# BB#20:                                #   in Loop: Header=BB3_2 Depth=1
	movl	%edx, %ebx
.LBB3_21:                               # %.._crit_edge39_crit_edge.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movslq	%ebx, %rbp
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	jmp	.LBB3_25
	.p2align	4, 0x90
.LBB3_22:                               # %.lr.ph38.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movslq	%r13d, %rcx
	movslq	%edx, %rbp
	decq	%rcx
	xorpd	%xmm1, %xmm1
	movl	%edx, %ebx
	.p2align	4, 0x90
.LBB3_23:                               #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	8(%rax,%rcx,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm0, %xmm1
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB3_23
# BB#24:                                #   in Loop: Header=BB3_2 Depth=1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
.LBB3_25:                               # %._crit_edge39.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movsd	(%rax,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	callq	sign
	movl	%eax, %r12d
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB3_27
# BB#26:                                # %call.sqrt
                                        #   in Loop: Header=BB3_2 Depth=1
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB3_27:                               # %._crit_edge39.i.split
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpl	%ebx, %r13d
	jge	.LBB3_47
# BB#28:                                # %.lr.ph.i
                                        #   in Loop: Header=BB3_2 Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r12d, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	movsd	.LCPI3_2(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	movq	(%r15), %rax
	movslq	%r13d, %rcx
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	cmpq	$4, %rdx
	jb	.LBB3_41
# BB#29:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%rdx, %r8
	andq	$-4, %r8
	je	.LBB3_41
# BB#30:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	leaq	(%rbx,%rcx,8), %rsi
	leaq	(%rax,%rbp,8), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB3_33
# BB#31:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	(%rbx,%rbp,8), %rsi
	leaq	(%rax,%rcx,8), %rdi
	cmpq	%rsi, %rdi
	jae	.LBB3_33
# BB#32:                                #   in Loop: Header=BB3_2 Depth=1
	movl	16(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB3_41
.LBB3_33:                               # %vector.ph
                                        #   in Loop: Header=BB3_2 Depth=1
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	-4(%r8), %rsi
	movq	%rsi, %rdi
	shrq	$2, %rdi
	btl	$2, %esi
	jb	.LBB3_34
# BB#35:                                # %vector.body.prol
                                        #   in Loop: Header=BB3_2 Depth=1
	movupd	(%rax,%rcx,8), %xmm2
	movupd	16(%rax,%rcx,8), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movq	8(%rsp), %rsi           # 8-byte Reload
	movupd	%xmm2, (%rsi,%rcx,8)
	movupd	%xmm3, 16(%rsi,%rcx,8)
	movl	$4, %esi
	testq	%rdi, %rdi
	jne	.LBB3_37
	jmp	.LBB3_39
.LBB3_34:                               #   in Loop: Header=BB3_2 Depth=1
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB3_39
.LBB3_37:                               # %vector.ph.new
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%r8, %rdi
	subq	%rsi, %rdi
	addq	%rcx, %rsi
	movq	48(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rsi,8), %rbx
	leaq	48(%rax,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB3_38:                               # %vector.body
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rsi), %xmm2
	movupd	-32(%rsi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -48(%rbx)
	movupd	%xmm3, -32(%rbx)
	movupd	-16(%rsi), %xmm2
	movupd	(%rsi), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rbx)
	movupd	%xmm3, (%rbx)
	addq	$64, %rbx
	addq	$64, %rsi
	addq	$-8, %rdi
	jne	.LBB3_38
.LBB3_39:                               # %middle.block
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%r8, %rdx
	movl	16(%rsp), %ebx          # 4-byte Reload
	je	.LBB3_47
# BB#40:                                #   in Loop: Header=BB3_2 Depth=1
	addq	%r8, %rcx
	.p2align	4, 0x90
.LBB3_41:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB3_44
# BB#42:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	negq	%rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB3_43:                               # %scalar.ph.prol
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdi,%rcx,8)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_43
.LBB3_44:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	$3, %rdx
	jb	.LBB3_47
# BB#45:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rsi
	leaq	24(%rax,%rcx,8), %rax
	.p2align	4, 0x90
.LBB3_46:                               # %scalar.ph
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-24(%rax), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -24(%rsi)
	movsd	-16(%rax), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -16(%rsi)
	movsd	-8(%rax), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%rsi)
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-4, %rdx
	jne	.LBB3_46
.LBB3_47:                               # %WeirdHouse.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, (%rsi,%rbp,8)
	movq	%r14, %rdi
	callq	ApplyHouse
	movq	(%r15), %rax
	movslq	%ebx, %rbx
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movslq	40(%rsp), %rbp          # 4-byte Folded Reload
	movq	-8(%r14,%rbp,8), %rcx
	movsd	(%rcx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	subsd	8(%rax,%rbx,8), %xmm1
	mulsd	.LCPI3_3(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB3_52
# BB#48:                                # %.lr.ph170
                                        #   in Loop: Header=BB3_2 Depth=1
	decq	%rbp
	leaq	1(%rbx), %r12
	movslq	%r13d, %r13
	.p2align	4, 0x90
.LBB3_49:                               #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	callq	printMatrix
	movq	(%r14,%rbp,8), %rax
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movq	(%r15), %rax
	movsd	(%rax,%r12,8), %xmm1    # xmm1 = mem[0],zero
	subsd	%xmm1, %xmm0
	mulsd	.LCPI3_4(%rip), %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	(%rax,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	callq	sign
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB3_51
# BB#50:                                # %call.sqrt244
                                        #   in Loop: Header=BB3_49 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB3_51:                               # %.split
                                        #   in Loop: Header=BB3_49 Depth=2
	mulsd	%xmm0, %xmm2
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm2, %xmm1
	movq	(%r14,%rbp,8), %rax
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movq	(%r15), %rax
	movsd	(%rax,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	leaq	96(%rsp), %rdi
	leaq	88(%rsp), %rsi
	callq	Givens
	movsd	96(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	88(%rsp), %xmm1         # xmm1 = mem[0],zero
	xorl	%ecx, %ecx
	movl	$50, %r8d
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	ApplyGivens
	movq	(%r14,%r13,8), %rax
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	movq	(%r15), %rax
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movq	(%r14,%rbp,8), %rcx
	movsd	(%rcx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	subsd	(%rax,%r12,8), %xmm1
	mulsd	.LCPI3_3(%rip), %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB3_49
.LBB3_52:                               # %._crit_edge171
                                        #   in Loop: Header=BB3_2 Depth=1
	cvttsd2si	72(%rsp), %ebx  # 8-byte Folded Reload
	cmpl	$51, %ebx
	movq	8(%rsp), %rax           # 8-byte Reload
	movsd	.LCPI3_0(%rip), %xmm2   # xmm2 = mem[0],zero
	jl	.LBB3_2
.LBB3_53:                               # %._crit_edge178
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	DivideAndSolve, .Lfunc_end3-DivideAndSolve
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"m=%i, rowstart=%i, rowend=%i\n"
	.size	.L.str, 30

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%e\n"
	.size	.L.str.1, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
