	.text
	.file	"z41.bc"
	.globl	ReadFromFile
	.p2align	4, 0x90
	.type	ReadFromFile,@function
ReadFromFile:                           # @ReadFromFile
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movl	%edx, %eax
	movl	$3, %edx
	xorl	%r8d, %r8d
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%eax, %ecx
	callq	LexPush
	callq	LexGetToken
	movq	%rax, %rbx
	movq	%rbx, 8(%rsp)
	cmpb	$102, 32(%rbx)
	je	.LBB0_2
# BB#1:
	addq	$32, %rbx
	movl	$41, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
.LBB0_2:
	movq	StartSym(%rip), %rsi
	leaq	8(%rsp), %rdi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	Parse
	movq	%rax, %rbx
	cmpq	$0, 8(%rsp)
	jne	.LBB0_4
# BB#3:
	cmpb	$2, 32(%rbx)
	je	.LBB0_5
.LBB0_4:
	movq	%rbx, %r8
	addq	$32, %r8
	movl	$41, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_5:
	callq	LexPop
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	ReadFromFile, .Lfunc_end0-ReadFromFile
	.cfi_endproc

	.globl	AppendToFile
	.p2align	4, 0x90
	.type	AppendToFile,@function
AppendToFile:                           # @AppendToFile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 56
	subq	$552, %rsp              # imm = 0x228
.Lcfi9:
	.cfi_def_cfa_offset 608
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r12
	movl	%esi, %r15d
	movq	%rdi, %r14
	movzwl	last_write_fnum(%rip), %eax
	cmpw	%r15w, %ax
	je	.LBB1_8
# BB#1:
	testw	%ax, %ax
	je	.LBB1_3
# BB#2:
	movq	last_write_fp(%rip), %rdi
	callq	fclose
.LBB1_3:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r14, %rbp
	movzwl	%r15w, %r13d
	movl	%r13d, %edi
	callq	FileName
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	strlen
	incq	%rax
	cmpq	$512, %rax              # imm = 0x200
	jb	.LBB1_5
# BB#4:
	movl	%r13d, %edi
	callq	PosOfFile
	movq	%rax, %r8
	movq	$.L.str.3, (%rsp)
	movl	$41, %edi
	movl	$3, %esi
	movl	$.L.str.2, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r9
	callq	Error
.LBB1_5:
	leaq	32(%rsp), %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	movw	$120, 32(%rsp,%rax)
	movl	%r13d, %edi
	callq	FileTestUpdated
	testl	%eax, %eax
	movl	$.L.str.4, %eax
	movl	$.L.str.5, %esi
	cmovneq	%rax, %rsi
	movq	%r14, %rdi
	callq	fopen
	movq	%rax, last_write_fp(%rip)
	testq	%rax, %rax
	jne	.LBB1_7
# BB#6:
	movq	no_fpos(%rip), %r8
	leaq	32(%rsp), %r9
	movl	$41, %edi
	movl	$4, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	last_write_fp(%rip), %rax
.LBB1_7:
	movw	%r15w, last_write_fnum(%rip)
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	fseek
	movq	%rbp, %r14
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB1_8:
	movq	last_write_fp(%rip), %rdi
	callq	ftell
	movl	%eax, (%r12)
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movzwl	%r15w, %ebx
	movl	%ebx, %edi
	callq	FileGetLineCount
	movl	%eax, 20(%rsp)
	incl	%eax
	movl	%eax, (%rbp)
	leaq	20(%rsp), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%ebx, %ecx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	20(%rsp), %esi
	addl	$2, %esi
	movl	%ebx, %edi
	callq	FileSetUpdated
	addq	$552, %rsp              # imm = 0x228
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	AppendToFile, .Lfunc_end1-AppendToFile
	.cfi_endproc

	.p2align	4, 0x90
	.type	WriteObject,@function
WriteObject:                            # @WriteObject
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 96
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
	movq	%rdx, %r12
	movl	%esi, %r14d
	movq	%rdi, %r15
	movzbl	32(%r15), %edi
	movl	%edi, %eax
	addb	$-2, %al
	cmpb	$97, %al
	ja	.LBB2_185
# BB#1:
	movl	$.L.str.30, %ebp
	movl	$5, %ecx
	movzbl	%al, %eax
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_78:
	movl	%ebx, %ebp
	movq	8(%r15), %rbx
	.p2align	4, 0x90
.LBB2_79:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_79
# BB#80:
	cmpb	$2, %al
	je	.LBB2_82
# BB#81:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_82:                               # %.loopexit415
	cmpl	$100, %r14d
	jl	.LBB2_84
# BB#83:
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
.LBB2_84:
	movq	80(%rbx), %rdi
	movzbl	43(%rdi), %eax
	shll	$16, %eax
	testl	$65536, %eax            # imm = 0x10000
	jne	.LBB2_88
# BB#85:
	movq	48(%rdi), %rax
	cmpq	StartSym(%rip), %rax
	je	.LBB2_88
# BB#86:                                # %need_lvis.exit
	cmpb	$-113, 32(%rax)
	jne	.LBB2_88
# BB#87:
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	80(%rbx), %rdi
.LBB2_88:                               # %need_lvis.exit.thread
	callq	SymName
	movq	last_write_fp(%rip), %rsi
	movq	%rax, %rdi
	callq	fputs
	cmpb	$6, 32(%r15)
	movl	$.L.str.28, %eax
	movl	$.L.str.29, %edi
	cmoveq	%rax, %rdi
	movq	last_write_fp(%rip), %rsi
	callq	fputs
	movq	(%r15), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB2_89:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB2_89
# BB#90:
	movzwl	%bp, %ecx
	movl	$105, %esi
	jmp	.LBB2_168
.LBB2_170:
	cmpb	$53, %dil
	movl	$.L.str.98, %eax
	movl	$.L.str.99, %edi
	cmoveq	%rax, %rdi
	movq	last_write_fp(%rip), %rsi
	callq	fputs
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.100, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	8(%r15), %rbp
	.p2align	4, 0x90
.LBB2_171:                              # =>This Inner Loop Header: Depth=1
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB2_171
# BB#172:
	cmpb	$11, %al
	je	.LBB2_187
# BB#173:
	cmpb	$19, %al
	je	.LBB2_175
# BB#174:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.101, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_175:                              # %.preheader420
	movq	8(%rbp), %r14
	cmpq	%rbp, %r14
	jne	.LBB2_177
	jmp	.LBB2_188
	.p2align	4, 0x90
.LBB2_183:                              # %.backedge422
                                        #   in Loop: Header=BB2_177 Depth=1
	movq	8(%r14), %r14
	cmpq	%rbp, %r14
	je	.LBB2_188
.LBB2_177:                              # %.lr.ph465
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_178 Depth 2
	leaq	16(%r14), %rbx
	jmp	.LBB2_178
	.p2align	4, 0x90
.LBB2_189:                              #   in Loop: Header=BB2_178 Depth=2
	addq	$16, %rbx
.LBB2_178:                              #   Parent Loop BB2_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_189
# BB#179:                               #   in Loop: Header=BB2_177 Depth=1
	cmpb	$1, %al
	je	.LBB2_183
# BB#180:                               #   in Loop: Header=BB2_177 Depth=1
	cmpb	$11, %al
	je	.LBB2_182
# BB#181:                               #   in Loop: Header=BB2_177 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.102, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_182:                              # %.loopexit419
                                        #   in Loop: Header=BB2_177 Depth=1
	addq	$64, %rbx
	movq	last_write_fp(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incl	(%r12)
	jmp	.LBB2_183
.LBB2_66:
	movq	(%r15), %rbp
	movq	80(%r15), %rax
	cmpq	%r15, %rbp
	je	.LBB2_70
	.p2align	4, 0x90
.LBB2_67:                               # %.preheader414
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB2_67
# BB#68:                                # %.preheader414
	cmpb	$82, %cl
	jne	.LBB2_70
# BB#69:
	testq	%rbp, %rbp
	je	.LBB2_70
# BB#74:                                # %.thread402
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incl	(%r12)
	movzwl	%bx, %ecx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incl	(%r12)
	jmp	.LBB2_77
.LBB2_185:
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.104, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.105, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB2_186
.LBB2_91:
	movl	$.L.str.31, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_2:
	movq	last_write_fp(%rip), %rsi
	cmpl	$8, %r14d
	jl	.LBB2_5
# BB#3:
	movb	64(%r15), %al
	testb	%al, %al
	jne	.LBB2_5
# BB#4:
	movl	$123, %edi
	callq	fputc
	jmp	.LBB2_30
.LBB2_6:
	movq	%r15, %rdi
	callq	StringQuotedWord
	movq	last_write_fp(%rip), %rsi
	movq	%rax, %rdi
	callq	fputs
	jmp	.LBB2_186
.LBB2_8:
	movl	$7, %ecx
	jmp	.LBB2_9
.LBB2_7:
	movl	$6, %ecx
.LBB2_9:
	movl	%r14d, 16(%rsp)         # 4-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	cmpl	%r14d, %ecx
	jge	.LBB2_11
# BB#10:
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
.LBB2_11:                               # %.preheader408
	movq	8(%r15), %r13
	cmpq	%r15, %r13
	je	.LBB2_29
# BB#12:                                # %.preheader407.lr.ph.preheader
	movl	12(%rsp), %r14d         # 4-byte Reload
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_13:                               # %.preheader407
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_14 Depth 2
                                        #     Child Loop BB2_41 Depth 2
                                        #     Child Loop BB2_31 Depth 2
                                        #     Child Loop BB2_21 Depth 2
                                        #     Child Loop BB2_24 Depth 2
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB2_14:                               #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB2_14
# BB#15:                                #   in Loop: Header=BB2_13 Depth=1
	cmpb	$1, %al
	jne	.LBB2_39
# BB#16:                                #   in Loop: Header=BB2_13 Depth=1
	movq	8(%rbp), %r14
	cmpq	%rbp, %r14
	je	.LBB2_17
	.p2align	4, 0x90
.LBB2_31:                               # %.preheader406
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB2_31
# BB#32:                                #   in Loop: Header=BB2_13 Depth=1
	movq	last_write_fp(%rip), %rsi
	cmpb	$17, 32(%r15)
	jne	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$32, %edi
	callq	fputc
	jmp	.LBB2_35
	.p2align	4, 0x90
.LBB2_39:                               #   in Loop: Header=BB2_13 Depth=1
	cmpb	$17, 32(%r15)
	jne	.LBB2_46
# BB#40:                                #   in Loop: Header=BB2_13 Depth=1
	movq	8(%r13), %r12
	cmpq	%r15, %r12
	movl	12(%rsp), %esi          # 4-byte Reload
	je	.LBB2_45
	.p2align	4, 0x90
.LBB2_41:                               #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %r12
	movq	(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB2_41
# BB#42:                                #   in Loop: Header=BB2_13 Depth=1
	cmpb	$1, %al
	je	.LBB2_44
# BB#43:                                #   in Loop: Header=BB2_13 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_44:                               # %.loopexit
                                        #   in Loop: Header=BB2_13 Depth=1
	movzbl	42(%r12), %eax
	movzbl	41(%r12), %ecx
	addl	%eax, %ecx
	movl	$7, %esi
	movl	$103, %eax
	cmovel	%eax, %esi
.LBB2_45:                               #   in Loop: Header=BB2_13 Depth=1
	addq	$8, %r13
	cmpl	%esi, %r14d
	cmovgel	%r14d, %esi
	movzwl	%bx, %ecx
	movq	%rbp, %rdi
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdx
	callq	WriteObject
	movq	(%r13), %r13
	cmpq	%r15, %r13
	jne	.LBB2_13
	jmp	.LBB2_29
.LBB2_46:                               #   in Loop: Header=BB2_13 Depth=1
	movzwl	%bx, %ecx
	movq	%rbp, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	%r12, %rdx
	callq	WriteObject
	addq	$8, %r13
	movq	(%r13), %r13
	cmpq	%r15, %r13
	jne	.LBB2_13
	jmp	.LBB2_29
.LBB2_17:                               #   in Loop: Header=BB2_13 Depth=1
	cmpb	$17, 32(%r15)
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_13 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_19:                               # %.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	movzbl	42(%rbp), %ecx
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	je	.LBB2_20
	.p2align	4, 0x90
.LBB2_21:                               # %.lr.ph
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movb	42(%rbp), %al
	movzbl	%al, %ecx
	incl	%ebx
	cmpl	%ecx, %ebx
	jl	.LBB2_21
	jmp	.LBB2_22
.LBB2_34:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$10, %edi
	callq	fputc
	incl	(%r12)
.LBB2_35:                               #   in Loop: Header=BB2_13 Depth=1
	movzbl	32(%r15), %edi
	movzwl	44(%rbp), %edx
	movl	%edx, %esi
	shrl	$8, %esi
	andl	$1, %esi
	shrl	$9, %edx
	andl	$1, %edx
	callq	EchoCatOp
	movq	last_write_fp(%rip), %rsi
	movq	%rax, %rdi
	callq	fputs
	movb	32(%r14), %al
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB2_37
# BB#36:                                #   in Loop: Header=BB2_13 Depth=1
	cmpb	$0, 64(%r14)
	je	.LBB2_38
.LBB2_37:                               #   in Loop: Header=BB2_13 Depth=1
	movzwl	%bx, %ecx
	movl	$105, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	WriteObject
.LBB2_38:                               #   in Loop: Header=BB2_13 Depth=1
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movl	12(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB2_27
.LBB2_20:                               #   in Loop: Header=BB2_13 Depth=1
	xorl	%eax, %eax
.LBB2_22:                               # %._crit_edge450
                                        #   in Loop: Header=BB2_13 Depth=1
	addl	%ecx, (%r12)
	movzbl	41(%rbp), %ecx
	testl	%ecx, %ecx
	je	.LBB2_26
# BB#23:                                # %.lr.ph455.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_24:                               # %.lr.ph455
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movzbl	41(%rbp), %ecx
	incl	%ebx
	cmpl	%ecx, %ebx
	jl	.LBB2_24
# BB#25:                                # %._crit_edge456.loopexit
                                        #   in Loop: Header=BB2_13 Depth=1
	movb	42(%rbp), %al
.LBB2_26:                               # %._crit_edge456
                                        #   in Loop: Header=BB2_13 Depth=1
	movzbl	%al, %eax
	addl	%ecx, %eax
	movl	$7, %r14d
	movl	$103, %eax
	cmovel	%eax, %r14d
	movl	20(%rsp), %ebx          # 4-byte Reload
.LBB2_27:                               # %.outer.backedge
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	8(%r13), %r13
	cmpq	%r15, %r13
	jne	.LBB2_13
.LBB2_29:                               # %.outer._crit_edge
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	jl	.LBB2_30
	jmp	.LBB2_186
.LBB2_92:
	movl	$.L.str.32, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_93:
	movl	$.L.str.33, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_94:
	movl	$.L.str.34, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_95:
	movl	$.L.str.35, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_96:
	movl	$.L.str.36, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_97:
	movl	$.L.str.37, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_98:
	movl	$.L.str.38, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_99:
	movl	$.L.str.39, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_100:
	movl	$.L.str.40, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_101:
	movl	$.L.str.41, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_102:
	movl	$.L.str.42, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_103:
	movl	$.L.str.43, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_104:
	movl	$.L.str.44, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_105:
	movl	$.L.str.45, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_106:
	movl	$.L.str.46, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_107:
	movl	$.L.str.47, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_108:
	movl	$.L.str.48, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_109:
	movl	$.L.str.49, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_110:
	movl	$.L.str.50, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_111:
	movl	$.L.str.51, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_112:
	movl	$.L.str.52, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_113:
	movl	$.L.str.53, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_115:
	movl	$.L.str.55, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_116:
	movl	$.L.str.56, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_114:
	movl	$.L.str.54, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_117:
	movl	$.L.str.57, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_118:
	movl	$.L.str.58, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_119:
	movl	$.L.str.59, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_120:
	movl	$.L.str.60, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_121:
	movl	$.L.str.61, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_122:
	movl	$.L.str.62, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_123:
	movl	$.L.str.63, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_124:
	movl	$.L.str.64, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_125:
	movl	$.L.str.65, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_126:
	movl	$.L.str.66, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_184:
	movq	last_write_fp(%rip), %rsi
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	FilterWrite
	jmp	.LBB2_186
.LBB2_127:
	movl	$.L.str.67, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_128:
	movl	$.L.str.68, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_129:
	movl	$.L.str.69, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_130:
	movl	$.L.str.70, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_131:
	movl	$.L.str.71, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_132:
	movl	$.L.str.72, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_133:
	movl	$.L.str.73, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_134:
	movl	$.L.str.74, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_135:
	movl	$.L.str.75, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_136:
	movl	$.L.str.76, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_137:
	movl	$.L.str.77, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_138:
	movl	$.L.str.78, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_139:
	movl	$.L.str.79, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_140:
	movl	$.L.str.80, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_141:
	movl	$.L.str.81, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_142:
	movl	$.L.str.82, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_143:
	movl	$.L.str.83, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_144:
	movl	$.L.str.84, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_145:
	movl	$.L.str.85, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_146:
	movl	$.L.str.86, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_147:
	movl	$.L.str.87, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_148:
	movl	$.L.str.88, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_149:
	movl	$.L.str.89, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_48:
	cmpq	%r15, 8(%r15)
	je	.LBB2_49
# BB#51:
	movzwl	%bx, %r14d
	leaq	28(%rsp), %rdx
	leaq	24(%rsp), %rcx
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	EnvWriteRetrieve
	testl	%eax, %eax
	movq	last_write_fp(%rip), %rcx
	je	.LBB2_53
# BB#52:
	movl	$.L.str.18, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rdi
	movl	28(%rsp), %edx
	movl	24(%rsp), %ecx
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB2_50
.LBB2_150:
	movl	$.L.str.90, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_151:
	movl	$.L.str.91, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_152:
	movl	$.L.str.92, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_153:
	movl	$.L.str.93, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_154:
	movl	$.L.str.94, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_155:
	movl	$.L.str.95, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_156:
	movl	$.L.str.96, %ebp
	cmpl	$100, %r14d
	jge	.LBB2_159
	jmp	.LBB2_160
.LBB2_157:
	movl	$.L.str.97, %ebp
.LBB2_158:
	cmpl	$100, %r14d
	jl	.LBB2_160
.LBB2_159:
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
.LBB2_160:
	movq	8(%r15), %rdi
	cmpq	(%r15), %rdi
	je	.LBB2_163
	.p2align	4, 0x90
.LBB2_161:                              # %.preheader417
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB2_161
# BB#162:
	movzwl	%bx, %ecx
	movl	$100, %esi
	movq	%r12, %rdx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
.LBB2_163:
	movq	last_write_fp(%rip), %rsi
	movq	%rbp, %rdi
	callq	fputs
	movq	(%r15), %rbp
	cmpq	%r15, %rbp
	je	.LBB2_169
	.p2align	4, 0x90
.LBB2_164:                              # %.preheader416
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB2_164
# BB#165:
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	cmpb	$92, 32(%r15)
	jne	.LBB2_167
# BB#166:
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movzwl	%bx, %ecx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
	jmp	.LBB2_169
.LBB2_167:
	movzwl	%bx, %ecx
	movl	$100, %esi
	movq	%rbp, %rdi
.LBB2_168:
	movq	%r12, %rdx
	callq	WriteObject
.LBB2_169:
	cmpl	$100, %r14d
	jl	.LBB2_186
.LBB2_30:
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
.LBB2_186:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_70:                               # %.thread
	movzbl	40(%rax), %ecx
	cmpl	%r14d, %ecx
	jle	.LBB2_71
# BB#75:
	cmpl	$103, %r14d
	jl	.LBB2_73
# BB#76:
	xorl	%ebp, %ebp
	jmp	.LBB2_77
.LBB2_187:
	addq	$64, %rbp
	movq	last_write_fp(%rip), %rsi
	movq	%rbp, %rdi
	callq	fputs
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
.LBB2_188:                              # %.loopexit421
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.103, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	cmpb	$53, 32(%r15)
	movl	$.L.str.98, %eax
	movl	$.L.str.99, %edi
	cmoveq	%rax, %rdi
	movq	last_write_fp(%rip), %rsi
	callq	fputs
	jmp	.LBB2_186
.LBB2_5:
	addq	$64, %r15
	movq	%r15, %rdi
	callq	fputs
	jmp	.LBB2_186
.LBB2_71:
	xorl	%ebp, %ebp
	cmpl	$102, %r14d
	jg	.LBB2_77
# BB#72:
	movzwl	41(%rax), %eax
	andl	$12, %eax
	jne	.LBB2_77
.LBB2_73:                               # %.critedge
	movzwl	%bx, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	WriteClosure
	jmp	.LBB2_186
.LBB2_77:                               # %.thread404
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movzwl	%bx, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rcx
	callq	WriteClosure
	jmp	.LBB2_30
.LBB2_49:
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB2_50
.LBB2_53:
	movq	%rcx, %rdi
	callq	ftell
	movq	%r12, %rbx
	movl	(%r12), %ecx
	movq	%r15, %rdi
	movl	%r14d, %esi
	movl	%eax, %edx
	callq	EnvWriteInsert
	movq	(%r15), %rbp
	movq	8(%r15), %r12
	cmpq	%rbp, %r12
	je	.LBB2_54
.LBB2_58:                               # %.preheader412
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB2_58
# BB#59:                                # %.preheader412
	cmpb	$82, %al
	movq	%rbx, %r12
	je	.LBB2_61
# BB#60:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_61:                               # %.loopexit413
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.23, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movl	%r14d, %ecx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incl	(%r12)
	movq	8(%r15), %rbx
.LBB2_62:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_62
# BB#63:
	cmpb	$2, %al
	je	.LBB2_65
# BB#64:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_65:                               # %.loopexit411
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movl	%r14d, %ecx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
.LBB2_50:
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incl	(%r12)
	jmp	.LBB2_186
.LBB2_54:                               # %.preheader409
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB2_54
# BB#55:                                # %.preheader409
	cmpb	$2, %al
	je	.LBB2_57
# BB#56:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_57:                               # %.loopexit410
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.21, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movl	%r14d, %ecx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incl	(%rbx)
	jmp	.LBB2_186
.Lfunc_end2:
	.size	WriteObject, .Lfunc_end2-WriteObject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_66
	.quad	.LBB2_185
	.quad	.LBB2_91
	.quad	.LBB2_158
	.quad	.LBB2_78
	.quad	.LBB2_78
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_2
	.quad	.LBB2_6
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_8
	.quad	.LBB2_7
	.quad	.LBB2_9
	.quad	.LBB2_92
	.quad	.LBB2_93
	.quad	.LBB2_94
	.quad	.LBB2_95
	.quad	.LBB2_96
	.quad	.LBB2_97
	.quad	.LBB2_98
	.quad	.LBB2_99
	.quad	.LBB2_100
	.quad	.LBB2_101
	.quad	.LBB2_102
	.quad	.LBB2_103
	.quad	.LBB2_104
	.quad	.LBB2_105
	.quad	.LBB2_106
	.quad	.LBB2_107
	.quad	.LBB2_108
	.quad	.LBB2_109
	.quad	.LBB2_110
	.quad	.LBB2_111
	.quad	.LBB2_112
	.quad	.LBB2_113
	.quad	.LBB2_115
	.quad	.LBB2_116
	.quad	.LBB2_114
	.quad	.LBB2_117
	.quad	.LBB2_118
	.quad	.LBB2_119
	.quad	.LBB2_120
	.quad	.LBB2_121
	.quad	.LBB2_122
	.quad	.LBB2_123
	.quad	.LBB2_124
	.quad	.LBB2_170
	.quad	.LBB2_170
	.quad	.LBB2_125
	.quad	.LBB2_126
	.quad	.LBB2_184
	.quad	.LBB2_127
	.quad	.LBB2_128
	.quad	.LBB2_129
	.quad	.LBB2_130
	.quad	.LBB2_131
	.quad	.LBB2_132
	.quad	.LBB2_133
	.quad	.LBB2_134
	.quad	.LBB2_135
	.quad	.LBB2_136
	.quad	.LBB2_137
	.quad	.LBB2_138
	.quad	.LBB2_139
	.quad	.LBB2_140
	.quad	.LBB2_141
	.quad	.LBB2_142
	.quad	.LBB2_143
	.quad	.LBB2_144
	.quad	.LBB2_145
	.quad	.LBB2_146
	.quad	.LBB2_147
	.quad	.LBB2_148
	.quad	.LBB2_149
	.quad	.LBB2_185
	.quad	.LBB2_48
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_185
	.quad	.LBB2_150
	.quad	.LBB2_151
	.quad	.LBB2_152
	.quad	.LBB2_153
	.quad	.LBB2_154
	.quad	.LBB2_155
	.quad	.LBB2_156
	.quad	.LBB2_157

	.text
	.globl	CloseFiles
	.p2align	4, 0x90
	.type	CloseFiles,@function
CloseFiles:                             # @CloseFiles
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
	subq	$1040, %rsp             # imm = 0x410
.Lcfi32:
	.cfi_def_cfa_offset 1072
.Lcfi33:
	.cfi_offset %rbx, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	cmpw	$0, last_write_fnum(%rip)
	je	.LBB3_2
# BB#1:
	movq	last_write_fp(%rip), %rdi
	callq	fclose
.LBB3_2:
	xorl	%edi, %edi
	callq	FirstFile
	testw	%ax, %ax
	je	.LBB3_5
# BB#3:                                 # %.lr.ph15.preheader
	leaq	16(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph15
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %ebp
	movl	%ebp, %edi
	callq	FileName
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	callq	strlen
	movl	$6581294, 16(%rsp,%rax) # imm = 0x646C2E
	movq	%rbx, %rdi
	callq	remove
	movl	%ebp, %edi
	callq	NextFile
	testw	%ax, %ax
	jne	.LBB3_4
.LBB3_5:                                # %._crit_edge16
	movl	$3, %edi
	callq	FirstFile
	testw	%ax, %ax
	je	.LBB3_13
# BB#6:                                 # %.lr.ph.preheader
	leaq	16(%rsp), %rbx
	leaq	528(%rsp), %r14
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%ax, %ebp
	movl	%ebp, %edi
	callq	FileTestUpdated
	testl	%eax, %eax
	je	.LBB3_12
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=1
	movl	%ebp, %edi
	callq	FileName
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	strcpy
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	movw	$120, 528(%rsp,%rax)
	movl	$.L.str.11, %esi
	movq	%rbx, %rdi
	callq	fopen
	testq	%rax, %rax
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_7 Depth=1
	movq	%rax, %rdi
	callq	fclose
	movq	%rbx, %rdi
	callq	remove
.LBB3_10:                               #   in Loop: Header=BB3_7 Depth=1
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	rename
	testl	%eax, %eax
	je	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_7 Depth=1
	movq	no_fpos(%rip), %r8
	movq	%rbx, (%rsp)
	movl	$41, %edi
	movl	$5, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%r14, %r9
	callq	Error
.LBB3_12:                               #   in Loop: Header=BB3_7 Depth=1
	movl	%ebp, %edi
	callq	NextFile
	testw	%ax, %ax
	jne	.LBB3_7
.LBB3_13:                               # %._crit_edge
	addq	$1040, %rsp             # imm = 0x410
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	CloseFiles, .Lfunc_end3-CloseFiles
	.cfi_endproc

	.p2align	4, 0x90
	.type	WriteClosure,@function
WriteClosure:                           # @WriteClosure
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 80
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r15
	movq	%rdi, %r13
	movq	80(%r13), %r12
	cmpq	$0, 112(%r12)
	je	.LBB4_4
# BB#1:
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.106, %edi
	movl	$5, %esi
.LBB4_2:                                # %need_lvis.exit130.thread
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
.LBB4_3:                                # %need_lvis.exit130.thread
	movq	%r12, %rdi
	callq	SymName
	movq	last_write_fp(%rip), %rsi
	movq	%rax, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fputs                   # TAILCALL
.LBB4_4:
	movq	%r13, %rdi
	movq	%rcx, %rsi
	callq	OptimizeParameterList
	movq	8(%r13), %r14
	cmpq	%r13, %r14
	je	.LBB4_55
# BB#5:                                 # %.preheader.lr.ph.lr.ph.lr.ph
	movb	$1, %al
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB4_39
.LBB4_6:                                #   in Loop: Header=BB4_39 Depth=1
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	jne	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_39 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.108, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rax
.LBB4_8:                                #   in Loop: Header=BB4_39 Depth=1
	addq	$16, %rax
	.p2align	4, 0x90
.LBB4_9:                                #   Parent Loop BB4_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB4_9
# BB#10:                                #   in Loop: Header=BB4_39 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_16
# BB#11:                                #   in Loop: Header=BB4_39 Depth=1
	movzbl	43(%r12), %eax
	shll	$16, %eax
	testl	$65536, %eax            # imm = 0x10000
	jne	.LBB4_15
# BB#12:                                #   in Loop: Header=BB4_39 Depth=1
	movq	48(%r12), %rax
	cmpq	StartSym(%rip), %rax
	je	.LBB4_15
# BB#13:                                # %need_lvis.exit
                                        #   in Loop: Header=BB4_39 Depth=1
	cmpb	$-113, 32(%rax)
	jne	.LBB4_15
# BB#14:                                #   in Loop: Header=BB4_39 Depth=1
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
.LBB4_15:                               # %need_lvis.exit.thread
                                        #   in Loop: Header=BB4_39 Depth=1
	movq	%r12, %rdi
	callq	SymName
	movq	last_write_fp(%rip), %rsi
	movq	%rax, %rdi
	callq	fputs
	movl	$1, 16(%rsp)            # 4-byte Folded Spill
.LBB4_16:                               #   in Loop: Header=BB4_39 Depth=1
	movq	last_write_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incl	(%r15)
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	80(%rbp), %rdi
	cmpb	$32, 125(%rdi)
	jne	.LBB4_30
# BB#17:                                #   in Loop: Header=BB4_39 Depth=1
	callq	SymName
	movq	last_write_fp(%rip), %rsi
	movq	%rax, %rdi
	callq	fputs
	jmp	.LBB4_31
.LBB4_18:                               #   in Loop: Header=BB4_39 Depth=1
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	jne	.LBB4_20
# BB#19:                                #   in Loop: Header=BB4_39 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.111, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rax
.LBB4_20:                               #   in Loop: Header=BB4_39 Depth=1
	addq	$16, %rax
	.p2align	4, 0x90
.LBB4_21:                               #   Parent Loop BB4_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB4_21
# BB#22:                                #   in Loop: Header=BB4_39 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_28
# BB#23:                                #   in Loop: Header=BB4_39 Depth=1
	movzbl	43(%r12), %eax
	shll	$16, %eax
	testl	$65536, %eax            # imm = 0x10000
	jne	.LBB4_27
# BB#24:                                #   in Loop: Header=BB4_39 Depth=1
	movq	48(%r12), %rax
	cmpq	StartSym(%rip), %rax
	je	.LBB4_27
# BB#25:                                # %need_lvis.exit131
                                        #   in Loop: Header=BB4_39 Depth=1
	cmpb	$-113, 32(%rax)
	jne	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_39 Depth=1
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
.LBB4_27:                               # %need_lvis.exit131.thread
                                        #   in Loop: Header=BB4_39 Depth=1
	movq	%r12, %rdi
	callq	SymName
	movq	last_write_fp(%rip), %rsi
	movq	%rax, %rdi
	callq	fputs
	movl	$1, 16(%rsp)            # 4-byte Folded Spill
.LBB4_28:                               #   in Loop: Header=BB4_39 Depth=1
	movq	last_write_fp(%rip), %rsi
	testb	$1, 20(%rsp)            # 1-byte Folded Reload
	je	.LBB4_32
# BB#29:                                #   in Loop: Header=BB4_39 Depth=1
	movl	$32, %edi
	callq	fputc
	cmpq	$0, 104(%r12)
	jne	.LBB4_33
	jmp	.LBB4_35
.LBB4_30:                               #   in Loop: Header=BB4_39 Depth=1
	movq	last_write_fp(%rip), %rsi
	movl	$92, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movq	80(%rbp), %rax
	movsbl	125(%rax), %edi
	callq	fputc
.LBB4_31:                               # %.outer
                                        #   in Loop: Header=BB4_39 Depth=1
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movl	12(%rsp), %eax          # 4-byte Reload
	movzwl	%ax, %ecx
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%eax, %ebx
	movq	%r15, %rdx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	callq	fputc
	movq	8(%r14), %r14
	cmpq	%r13, %r14
	jne	.LBB4_39
	jmp	.LBB4_54
.LBB4_32:                               #   in Loop: Header=BB4_39 Depth=1
	movl	$10, %edi
	callq	fputc
	incl	(%r15)
	cmpq	$0, 104(%r12)
	je	.LBB4_35
.LBB4_33:                               #   in Loop: Header=BB4_39 Depth=1
	cmpb	$57, 32(%rbx)
	jne	.LBB4_35
# BB#34:                                #   in Loop: Header=BB4_39 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	movzwl	%ax, %ecx
	xorl	%esi, %esi
	jmp	.LBB4_37
.LBB4_35:                               #   in Loop: Header=BB4_39 Depth=1
	movzwl	41(%r12), %eax
	testb	$1, %ah
	jne	.LBB4_38
# BB#36:                                #   in Loop: Header=BB4_39 Depth=1
	movzbl	40(%r12), %esi
	movl	12(%rsp), %eax          # 4-byte Reload
	movzwl	%ax, %ecx
.LBB4_37:                               # %.outer132.backedge
                                        #   in Loop: Header=BB4_39 Depth=1
	movq	%rbx, %rdi
	movl	%eax, %ebx
	movq	%r15, %rdx
	callq	WriteObject
	jmp	.LBB4_52
.LBB4_38:                               #   in Loop: Header=BB4_39 Depth=1
	movq	last_write_fp(%rip), %rsi
	movl	$123, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movl	12(%rsp), %eax          # 4-byte Reload
	movzwl	%ax, %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%eax, %ebx
	movq	%r15, %rdx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	last_write_fp(%rip), %rsi
	movl	$125, %edi
	jmp	.LBB4_51
	.p2align	4, 0x90
.LBB4_39:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_40 Depth 2
                                        #     Child Loop BB4_21 Depth 2
                                        #     Child Loop BB4_9 Depth 2
                                        #     Child Loop BB4_49 Depth 2
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB4_40:                               #   Parent Loop BB4_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB4_40
# BB#41:                                #   in Loop: Header=BB4_39 Depth=1
	cmpb	$10, %al
	jne	.LBB4_52
# BB#42:                                #   in Loop: Header=BB4_39 Depth=1
	movq	80(%rbp), %rax
	movzbl	32(%rax), %edi
	cmpl	$144, %edi
	je	.LBB4_46
# BB#43:                                #   in Loop: Header=BB4_39 Depth=1
	cmpb	$-111, %dil
	je	.LBB4_6
# BB#44:                                #   in Loop: Header=BB4_39 Depth=1
	cmpb	$-110, %dil
	je	.LBB4_18
# BB#45:                                #   in Loop: Header=BB4_39 Depth=1
	movl	%ebx, %ebp
	movq	no_fpos(%rip), %rbx
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.104, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.113, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	movl	%ebp, %ebx
	callq	Error
	jmp	.LBB4_52
	.p2align	4, 0x90
.LBB4_46:                               #   in Loop: Header=BB4_39 Depth=1
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	jne	.LBB4_48
# BB#47:                                #   in Loop: Header=BB4_39 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.107, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbp), %rax
.LBB4_48:                               #   in Loop: Header=BB4_39 Depth=1
	addq	$16, %rax
	.p2align	4, 0x90
.LBB4_49:                               #   Parent Loop BB4_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdi
	leaq	16(%rdi), %rax
	cmpb	$0, 32(%rdi)
	je	.LBB4_49
# BB#50:                                #   in Loop: Header=BB4_39 Depth=1
	movzbl	40(%r12), %esi
	movzwl	%bx, %ecx
	movq	%r15, %rdx
	callq	WriteObject
	movq	last_write_fp(%rip), %rsi
	movl	$32, %edi
.LBB4_51:                               # %.outer132.backedge
                                        #   in Loop: Header=BB4_39 Depth=1
	callq	fputc
.LBB4_52:                               # %.outer132.backedge
                                        #   in Loop: Header=BB4_39 Depth=1
	movq	8(%r14), %r14
	cmpq	%r13, %r14
	jne	.LBB4_39
# BB#53:                                # %.outer132._crit_edge
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB4_55
.LBB4_54:                               # %.outer132._crit_edge.thread179
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_55:                               # %.outer132._crit_edge.thread
	movzbl	43(%r12), %eax
	shll	$16, %eax
	testl	$65536, %eax            # imm = 0x10000
	jne	.LBB4_3
# BB#56:
	movq	48(%r12), %rax
	cmpq	StartSym(%rip), %rax
	je	.LBB4_3
# BB#57:                                # %need_lvis.exit130
	cmpb	$-113, 32(%rax)
	jne	.LBB4_3
# BB#58:
	movq	last_write_fp(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$3, %esi
	jmp	.LBB4_2
.Lfunc_end4:
	.size	WriteClosure, .Lfunc_end4-WriteClosure
	.cfi_endproc

	.p2align	4, 0x90
	.type	OptimizeParameterList,@function
OptimizeParameterList:                  # @OptimizeParameterList
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 48
.Lcfi54:
	.cfi_offset %rbx, -48
.Lcfi55:
	.cfi_offset %r12, -40
.Lcfi56:
	.cfi_offset %r13, -32
.Lcfi57:
	.cfi_offset %r14, -24
.Lcfi58:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpb	$2, 32(%r14)
	je	.LBB5_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.114, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_2:
	testq	%r15, %r15
	je	.LBB5_20
# BB#3:                                 # %.preheader
	movq	8(%r14), %r12
	cmpq	%r14, %r12
	jne	.LBB5_5
	jmp	.LBB5_20
	.p2align	4, 0x90
.LBB5_19:                               # %.backedge41
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	8(%r12), %r12
	cmpq	%r14, %r12
	je	.LBB5_20
.LBB5_5:                                # %.lr.ph51
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_6 Depth 2
                                        #     Child Loop BB5_9 Depth 2
                                        #     Child Loop BB5_14 Depth 2
                                        #       Child Loop BB5_15 Depth 3
	leaq	16(%r12), %rax
	jmp	.LBB5_6
	.p2align	4, 0x90
.LBB5_23:                               #   in Loop: Header=BB5_6 Depth=2
	addq	$16, %rax
.LBB5_6:                                #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB5_23
# BB#7:                                 #   in Loop: Header=BB5_5 Depth=1
	cmpb	$10, %cl
	jne	.LBB5_19
# BB#8:                                 #   in Loop: Header=BB5_5 Depth=1
	movq	8(%rax), %rbx
	.p2align	4, 0x90
.LBB5_9:                                #   Parent Loop BB5_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB5_9
# BB#10:                                #   in Loop: Header=BB5_5 Depth=1
	cmpb	$2, %al
	je	.LBB5_18
# BB#11:                                #   in Loop: Header=BB5_5 Depth=1
	cmpb	$17, %al
	jne	.LBB5_19
# BB#12:                                #   in Loop: Header=BB5_5 Depth=1
	movq	8(%rbx), %r13
	cmpq	%rbx, %r13
	jne	.LBB5_14
	jmp	.LBB5_19
	.p2align	4, 0x90
.LBB5_17:                               #   in Loop: Header=BB5_14 Depth=2
	movq	%r15, %rsi
	callq	Optimize
	movq	8(%r13), %r13
	cmpq	%rbx, %r13
	je	.LBB5_19
.LBB5_14:                               # %.lr.ph
                                        #   Parent Loop BB5_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_15 Depth 3
	movq	8(%r13), %rax
	leaq	16(%rax), %rdi
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_21:                               #   in Loop: Header=BB5_15 Depth=3
	addq	$16, %rdi
.LBB5_15:                               #   Parent Loop BB5_5 Depth=1
                                        #     Parent Loop BB5_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi), %rdi
	movzbl	32(%rdi), %ecx
	testb	%cl, %cl
	je	.LBB5_21
# BB#16:                                #   in Loop: Header=BB5_14 Depth=2
	cmpb	$2, %cl
	je	.LBB5_17
# BB#22:                                # %.backedge.loopexit
                                        #   in Loop: Header=BB5_14 Depth=2
	movq	%rax, %r13
	cmpq	%rbx, %r13
	jne	.LBB5_14
	jmp	.LBB5_19
.LBB5_18:                               #   in Loop: Header=BB5_5 Depth=1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	Optimize
	jmp	.LBB5_19
.LBB5_20:                               # %.loopexit40
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	OptimizeParameterList, .Lfunc_end5-OptimizeParameterList
	.cfi_endproc

	.p2align	4, 0x90
	.type	Optimize,@function
Optimize:                               # @Optimize
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -24
.Lcfi63:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	%rbx, 8(%rbx)
	je	.LBB6_2
# BB#1:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	OptimizeParameterList
.LBB6_2:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	ParameterCheck
	testq	%rax, %rax
	je	.LBB6_8
# BB#3:
	movq	%rbx, zz_hold(%rip)
	movq	24(%rbx), %rcx
	cmpq	%rbx, %rcx
	je	.LBB6_4
# BB#5:
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rbx), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB6_7
# BB#6:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	jmp	.LBB6_7
.LBB6_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_4:                                # %.thread
	movq	$0, xx_tmp(%rip)
	movq	%rax, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB6_7:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	DisposeObject           # TAILCALL
.Lfunc_end6:
	.size	Optimize, .Lfunc_end6-Optimize
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"database index file seems to be out of date"
	.size	.L.str, 44

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"syntax error in database file"
	.size	.L.str.1, 30

	.type	last_write_fnum,@object # @last_write_fnum
	.local	last_write_fnum
	.comm	last_write_fnum,2,2
	.type	last_write_fp,@object   # @last_write_fp
	.local	last_write_fp
	.comm	last_write_fp,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"file name %s%s is too long"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"x"
	.size	.L.str.3, 2

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"a"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"w"
	.size	.L.str.5, 2

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"cannot append to database file %s"
	.size	.L.str.6, 34

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"r"
	.size	.L.str.11, 2

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"rename(%s, %s) failed"
	.size	.L.str.12, 22

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"assert failed in %s"
	.size	.L.str.13, 20

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"WriteObject: Down(y) == y!"
	.size	.L.str.14, 27

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"WriteObject: next_gap!"
	.size	.L.str.16, 23

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"@@C"
	.size	.L.str.17, 4

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"@@D"
	.size	.L.str.18, 4

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	" \"%d %d\""
	.size	.L.str.19, 9

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"WriteObject: ENV/CLOSURE!"
	.size	.L.str.20, 26

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"@@A"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"WriteObject: ENV/ENV!"
	.size	.L.str.22, 22

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"@@B"
	.size	.L.str.23, 4

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"WriteObject: ENV/ENV+CLOSURE!"
	.size	.L.str.24, 30

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"@@E"
	.size	.L.str.25, 4

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"WriteObject/CROSS: type(y) != CLOSURE!"
	.size	.L.str.26, 39

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"@@V"
	.size	.L.str.27, 4

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"&&"
	.size	.L.str.28, 3

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"&&&"
	.size	.L.str.29, 4

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"@Null"
	.size	.L.str.30, 6

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"@PageLabel"
	.size	.L.str.31, 11

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"@BeginHeaderComponent"
	.size	.L.str.32, 22

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"@EndHeaderComponent"
	.size	.L.str.33, 20

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"@SetHeaderComponent"
	.size	.L.str.34, 20

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"@ClearHeaderComponent"
	.size	.L.str.35, 22

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"@OneCol"
	.size	.L.str.36, 8

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"@OneRow"
	.size	.L.str.37, 8

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"@Wide"
	.size	.L.str.38, 6

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"@High"
	.size	.L.str.39, 6

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"@HShift"
	.size	.L.str.40, 8

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"@VShift"
	.size	.L.str.41, 8

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"@HScale"
	.size	.L.str.42, 8

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"@VScale"
	.size	.L.str.43, 8

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"@HCover"
	.size	.L.str.44, 8

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"@VCover"
	.size	.L.str.45, 8

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"@Scale"
	.size	.L.str.46, 7

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"@KernShrink"
	.size	.L.str.47, 12

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"@HContract"
	.size	.L.str.48, 11

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"@VContract"
	.size	.L.str.49, 11

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"@HLimited"
	.size	.L.str.50, 10

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"@VLimited"
	.size	.L.str.51, 10

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"@HExpand"
	.size	.L.str.52, 9

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"@VExpand"
	.size	.L.str.53, 9

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"@StartHVSpan"
	.size	.L.str.54, 13

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"@StartHSpan"
	.size	.L.str.55, 12

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"@StartVSpan"
	.size	.L.str.56, 12

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"@HSpan"
	.size	.L.str.57, 7

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"@VSpan"
	.size	.L.str.58, 7

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"@PAdjust"
	.size	.L.str.59, 9

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"@HAdjust"
	.size	.L.str.60, 9

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"@VAdjust"
	.size	.L.str.61, 9

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"@Rotate"
	.size	.L.str.62, 8

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"@Background"
	.size	.L.str.63, 12

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"@Case"
	.size	.L.str.64, 6

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"@Yield"
	.size	.L.str.65, 7

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"@BackEnd"
	.size	.L.str.66, 9

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"@Char"
	.size	.L.str.67, 6

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"@Font"
	.size	.L.str.68, 6

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"@Space"
	.size	.L.str.69, 7

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"@YUnit"
	.size	.L.str.70, 7

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"@ZUnit"
	.size	.L.str.71, 7

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"@Break"
	.size	.L.str.72, 7

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"@Underline"
	.size	.L.str.73, 11

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"@SetColour"
	.size	.L.str.74, 11

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"@Outline"
	.size	.L.str.75, 9

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"@Language"
	.size	.L.str.76, 10

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"@CurrLang"
	.size	.L.str.77, 10

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"@CurrFamily"
	.size	.L.str.78, 12

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"@CurrFace"
	.size	.L.str.79, 10

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"@CurrYUnit"
	.size	.L.str.80, 11

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"@CurrZUnit"
	.size	.L.str.81, 11

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"@Common"
	.size	.L.str.82, 8

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"@Rump"
	.size	.L.str.83, 6

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"@Meld"
	.size	.L.str.84, 6

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"@Insert"
	.size	.L.str.85, 8

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"@OneOf"
	.size	.L.str.86, 7

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"@Next"
	.size	.L.str.87, 6

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"@Plus"
	.size	.L.str.88, 6

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"@Minus"
	.size	.L.str.89, 7

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"@Open"
	.size	.L.str.90, 6

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"@Tagged"
	.size	.L.str.91, 8

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"@IncludeGraphic"
	.size	.L.str.92, 16

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"@SysIncludeGraphic"
	.size	.L.str.93, 19

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"@PlainGraphic"
	.size	.L.str.94, 14

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"@Graphic"
	.size	.L.str.95, 9

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"@LinkSource"
	.size	.L.str.96, 12

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"@LinkDest"
	.size	.L.str.97, 10

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"@Verbatim"
	.size	.L.str.98, 10

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"@RawVerbatim"
	.size	.L.str.99, 13

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"@Begin"
	.size	.L.str.100, 7

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"WriteObject/VERBATIM!"
	.size	.L.str.101, 22

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"WriteObject/VERBATIM/WORD!"
	.size	.L.str.102, 27

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"@End"
	.size	.L.str.103, 5

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"assert failed in %s %s"
	.size	.L.str.104, 23

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"WriteObject:"
	.size	.L.str.105, 13

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"@LUse"
	.size	.L.str.106, 6

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"WriteObject/CLOSURE: LPAR!"
	.size	.L.str.107, 27

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"WriteObject/CLOSURE: NPAR!"
	.size	.L.str.108, 27

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"WriteObject/CLOSURE: RPAR!"
	.size	.L.str.111, 27

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"WriteClosure:"
	.size	.L.str.113, 14

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"OptimizeParameterList: type(x) != CLOSURE!"
	.size	.L.str.114, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
