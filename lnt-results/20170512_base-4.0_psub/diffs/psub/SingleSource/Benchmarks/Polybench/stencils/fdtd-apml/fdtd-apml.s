	.text
	.file	"fdtd-apml.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_1:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi7:
	.cfi_def_cfa_offset 352
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$33800, %edx            # imm = 0x8408
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#1:
	movq	8(%rsp), %r13
	testq	%r13, %r13
	je	.LBB6_104
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$33800, %edx            # imm = 0x8408
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#4:                                 # %polybench_alloc_data.exit149
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$33800, %edx            # imm = 0x8408
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#5:                                 # %polybench_alloc_data.exit149
	movq	8(%rsp), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#6:                                 # %polybench_alloc_data.exit151
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$33800, %edx            # imm = 0x8408
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#7:                                 # %polybench_alloc_data.exit151
	movq	8(%rsp), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#8:                                 # %polybench_alloc_data.exit153
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$2197000, %edx          # imm = 0x218608
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#9:                                 # %polybench_alloc_data.exit153
	movq	8(%rsp), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#10:                                # %polybench_alloc_data.exit155
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$2197000, %edx          # imm = 0x218608
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#11:                                # %polybench_alloc_data.exit155
	movq	8(%rsp), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#12:                                # %polybench_alloc_data.exit157
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$2197000, %edx          # imm = 0x218608
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#13:                                # %polybench_alloc_data.exit157
	movq	8(%rsp), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#14:                                # %polybench_alloc_data.exit159
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$2197000, %edx          # imm = 0x218608
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#15:                                # %polybench_alloc_data.exit159
	movq	8(%rsp), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#16:                                # %polybench_alloc_data.exit161
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$2197000, %edx          # imm = 0x218608
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#17:                                # %polybench_alloc_data.exit161
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_104
# BB#18:                                # %polybench_alloc_data.exit163
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$2197000, %edx          # imm = 0x218608
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#19:                                # %polybench_alloc_data.exit163
	movq	8(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_104
# BB#20:                                # %polybench_alloc_data.exit165
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$2197000, %edx          # imm = 0x218608
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#21:                                # %polybench_alloc_data.exit165
	movq	8(%rsp), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#22:                                # %polybench_alloc_data.exit167
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$2197000, %edx          # imm = 0x218608
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#23:                                # %polybench_alloc_data.exit167
	movq	8(%rsp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#24:                                # %polybench_alloc_data.exit169
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$520, %edx              # imm = 0x208
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#25:                                # %polybench_alloc_data.exit169
	movq	8(%rsp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#26:                                # %polybench_alloc_data.exit171
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$520, %edx              # imm = 0x208
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#27:                                # %polybench_alloc_data.exit171
	movq	8(%rsp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#28:                                # %polybench_alloc_data.exit173
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$520, %edx              # imm = 0x208
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#29:                                # %polybench_alloc_data.exit173
	movq	8(%rsp), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#30:                                # %polybench_alloc_data.exit175
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$520, %edx              # imm = 0x208
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#31:                                # %polybench_alloc_data.exit175
	movq	8(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#32:                                # %polybench_alloc_data.exit177
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$520, %edx              # imm = 0x208
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#33:                                # %polybench_alloc_data.exit177
	movq	8(%rsp), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_104
# BB#34:                                # %polybench_alloc_data.exit179
	movq	%r14, 144(%rsp)         # 8-byte Spill
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$520, %edx              # imm = 0x208
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_104
# BB#35:                                # %polybench_alloc_data.exit179
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.LBB6_104
# BB#36:                                # %polybench_alloc_data.exit181
	subq	$8, %rsp
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %rdi
	leaq	296(%rsp), %rsi
	movq	%r13, %rdx
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	200(%rsp), %r14         # 8-byte Reload
	movq	%r14, %r8
	movq	168(%rsp), %r15         # 8-byte Reload
	movq	%r15, %r9
	movq	%rax, 216(%rsp)         # 8-byte Spill
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	192(%rsp)               # 8-byte Folded Reload
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)               # 8-byte Folded Reload
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	movq	208(%rsp), %rbp         # 8-byte Reload
	pushq	%rbp
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	init_array
	movq	%rbp, %r9
	movq	%r15, %rdx
	movq	%r14, %rsi
	addq	$64, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -64
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movsd	288(%rsp), %xmm1        # xmm1 = mem[0],zero
	movq	%r13, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	%r13, 224(%rsp)         # 8-byte Spill
	movq	176(%rsp), %r11         # 8-byte Reload
	movq	208(%rsp), %r10         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_37:                               # %.preheader9.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_38 Depth 2
                                        #       Child Loop BB6_39 Depth 3
                                        #       Child Loop BB6_41 Depth 3
	imulq	$33800, %r14, %rbp      # imm = 0x8408
	leaq	33792(%rsi,%rbp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	imulq	$520, %r14, %rbx        # imm = 0x208
	leaq	512(%r13,%rbx), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	leaq	512(%rax,%rbx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	33792(%rdx,%rbp), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	33792(%rdi,%rbp), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	leaq	33792(%r9,%rbp), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rcx), %rbx
	leaq	(%r9,%rcx), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rcx), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rcx), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	%rcx, %rbp
	xorl	%r8d, %r8d
	movq	%rbx, 256(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_38:                               # %.preheader.i
                                        #   Parent Loop BB6_37 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_39 Depth 3
                                        #       Child Loop BB6_41 Depth 3
	leaq	1(%r8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	leaq	(%rax,%r8,8), %r12
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	leaq	(%rax,%r8,8), %rax
	movq	%rbp, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movq	184(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_39:                               #   Parent Loop BB6_37 Depth=1
                                        #     Parent Loop BB6_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi,%rbp), %xmm2      # xmm2 = mem[0],zero
	subsd	520(%rsi,%rbp), %xmm2
	addsd	8(%rdx,%rbp), %xmm2
	subsd	(%rdx,%rbp), %xmm2
	movsd	%xmm2, (%r12)
	movsd	(%r11,%r8,8), %xmm3     # xmm3 = mem[0],zero
	movsd	(%r10,%r8,8), %xmm4     # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm3
	mulsd	(%rdi,%rbp), %xmm3
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movsd	%xmm3, (%rax)
	movsd	(%r13,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rbx,8), %xmm4    # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm2
	mulsd	(%r9,%rbp), %xmm2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r14,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r14,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	divsd	%xmm4, %xmm2
	mulsd	(%rdi,%rbp), %xmm2
	subsd	%xmm2, %xmm5
	movsd	%xmm5, (%r9,%rbp)
	movq	(%rax), %rcx
	movq	%rcx, (%rdi,%rbp)
	incq	%rbx
	addq	$8, %rbp
	cmpq	$64, %rbx
	jne	.LBB6_39
# BB#40:                                #   in Loop: Header=BB6_38 Depth=2
	movq	168(%rsp), %r15         # 8-byte Reload
	leaq	(%rsi,%r15), %rcx
	imulq	$520, %r8, %rbp         # imm = 0x208
	movsd	512(%rbp,%rcx), %xmm2   # xmm2 = mem[0],zero
	imulq	$520, 56(%rsp), %rbx    # 8-byte Folded Reload
                                        # imm = 0x208
	subsd	512(%rbx,%rcx), %xmm2
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rcx,%rsi), %rcx
	addsd	(%rcx,%r8,8), %xmm2
	leaq	(%rdx,%r15), %rcx
	subsd	512(%rbp,%rcx), %xmm2
	movsd	%xmm2, (%r12)
	movsd	(%r11,%r8,8), %xmm3     # xmm3 = mem[0],zero
	movsd	(%r10,%r8,8), %xmm4     # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm3
	leaq	(%rdi,%r15), %rbx
	mulsd	512(%rbp,%rbx), %xmm3
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movsd	%xmm3, (%rax)
	movsd	512(%r13), %xmm2        # xmm2 = mem[0],zero
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	512(%rcx), %xmm4        # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm2
	leaq	(%r9,%r15), %rcx
	mulsd	512(%rbp,%rcx), %xmm2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r14,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movq	32(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r14,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	divsd	%xmm4, %xmm2
	mulsd	512(%rbp,%rbx), %xmm2
	subsd	%xmm2, %xmm5
	movsd	%xmm5, 512(%rbp,%rcx)
	movq	(%rax), %rcx
	movq	%rcx, 512(%rbp,%rbx)
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rdi,%rcx), %r9
	movq	40(%rsp), %r15          # 8-byte Reload
	xorl	%ebp, %ebp
	movq	256(%rsp), %rbx         # 8-byte Reload
	movq	248(%rsp), %rdx         # 8-byte Reload
	movq	240(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_41:                               #   Parent Loop BB6_37 Depth=1
                                        #     Parent Loop BB6_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	33280(%rdi,%rbp), %xmm2 # xmm2 = mem[0],zero
	subsd	(%r15), %xmm2
	addsd	33288(%rsi,%rbp), %xmm2
	subsd	33280(%rsi,%rbp), %xmm2
	movsd	%xmm2, (%r12)
	movsd	512(%r11), %xmm3        # xmm3 = mem[0],zero
	movsd	(%r10,%r8,8), %xmm4     # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm3
	mulsd	(%r9,%rbp), %xmm3
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movsd	%xmm3, (%rax)
	movsd	(%r13,%rbp), %xmm2      # xmm2 = mem[0],zero
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rbp), %xmm4      # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm2
	mulsd	33280(%rdx,%rbp), %xmm2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r14,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r14,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	divsd	%xmm4, %xmm2
	mulsd	33280(%rbx,%rbp), %xmm2
	subsd	%xmm2, %xmm5
	movsd	%xmm5, 33280(%rdx,%rbp)
	movq	(%rax), %rcx
	movq	%rcx, 33280(%rbx,%rbp)
	addq	$8, %rbp
	addq	$8, %r15
	cmpq	$512, %rbp              # imm = 0x200
	jne	.LBB6_41
# BB#42:                                #   in Loop: Header=BB6_38 Depth=2
	movq	72(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx), %xmm2           # xmm2 = mem[0],zero
	movq	136(%rsp), %rcx         # 8-byte Reload
	subsd	(%rcx), %xmm2
	movq	128(%rsp), %rcx         # 8-byte Reload
	addsd	(%rcx), %xmm2
	movq	120(%rsp), %rcx         # 8-byte Reload
	subsd	(%rcx), %xmm2
	movsd	%xmm2, (%r12)
	movsd	512(%r11), %xmm3        # xmm3 = mem[0],zero
	movsd	512(%r10), %xmm4        # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm3
	movq	112(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx), %xmm3
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movsd	%xmm3, (%rax)
	movsd	512(%r13), %xmm2        # xmm2 = mem[0],zero
	movq	16(%rsp), %rdx          # 8-byte Reload
	movsd	512(%rdx), %xmm4        # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm2
	movq	264(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx), %xmm2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%r14,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movq	32(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%r14,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	divsd	%xmm4, %xmm2
	mulsd	(%rcx), %xmm2
	subsd	%xmm2, %xmm5
	movsd	%xmm5, (%rdx)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	64(%rsp), %rbp          # 8-byte Reload
	addq	$520, %rbp              # imm = 0x208
	movq	56(%rsp), %r8           # 8-byte Reload
	cmpq	$64, %r8
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	192(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	152(%rsp), %r9          # 8-byte Reload
	jne	.LBB6_38
# BB#43:                                #   in Loop: Header=BB6_37 Depth=1
	incq	%r14
	movq	216(%rsp), %rcx         # 8-byte Reload
	addq	$33800, %rcx            # imm = 0x8408
	addq	$520, 40(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x208
	cmpq	$64, %r14
	movq	224(%rsp), %r13         # 8-byte Reload
	jne	.LBB6_37
# BB#44:                                # %kernel_fdtd_apml.exit
	subq	$8, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %rdi
	leaq	296(%rsp), %rsi
	movq	%r13, %rdx
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	%r15, %r8
	movq	104(%rsp), %r14         # 8-byte Reload
	movq	%r14, %r9
	pushq	216(%rsp)               # 8-byte Folded Reload
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	192(%rsp)               # 8-byte Folded Reload
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)               # 8-byte Folded Reload
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	movq	144(%rsp), %rbp         # 8-byte Reload
	pushq	%rbp
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	callq	init_array
	movq	%r15, %rsi
	movq	%rbp, %r9
	movq	%r14, %rdi
	addq	$64, %rsp
.Lcfi31:
	.cfi_adjust_cfa_offset -64
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	xorl	%ecx, %ecx
	movsd	288(%rsp), %xmm1        # xmm1 = mem[0],zero
	movq	%r13, 40(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movq	184(%rsp), %r11         # 8-byte Reload
	movq	176(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_45:                               # %.preheader9.i183
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_46 Depth 2
                                        #       Child Loop BB6_47 Depth 3
                                        #       Child Loop BB6_49 Depth 3
	imulq	$33800, %r12, %rbp      # imm = 0x8408
	leaq	33792(%rsi,%rbp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	imulq	$520, %r12, %rbx        # imm = 0x208
	leaq	512(%r13,%rbx), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	leaq	512(%rax,%rbx), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	33792(%rdi,%rbp), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	144(%rsp), %rax         # 8-byte Reload
	leaq	33792(%rax,%rbp), %rbx
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	leaq	33792(%r9,%rbp), %rbp
	movq	%rbp, 264(%rsp)         # 8-byte Spill
	leaq	(%rax,%rcx), %r10
	leaq	(%r9,%rcx), %rdx
	leaq	(%rdi,%rcx), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rcx), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	%rcx, 216(%rsp)         # 8-byte Spill
	movq	%rcx, %rbp
	xorl	%r8d, %r8d
	movq	%r10, 256(%rsp)         # 8-byte Spill
	movq	%rdx, 248(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_46:                               # %.preheader.i186
                                        #   Parent Loop BB6_45 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_47 Depth 3
                                        #       Child Loop BB6_49 Depth 3
	leaq	1(%r8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	leaq	(%rax,%r8,8), %r14
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	leaq	(%rax,%r8,8), %rax
	movq	%rbp, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movq	208(%rsp), %r13         # 8-byte Reload
	movq	144(%rsp), %r10         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_47:                               #   Parent Loop BB6_45 Depth=1
                                        #     Parent Loop BB6_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi,%rbp), %xmm2      # xmm2 = mem[0],zero
	subsd	520(%rsi,%rbp), %xmm2
	addsd	8(%rdi,%rbp), %xmm2
	subsd	(%rdi,%rbp), %xmm2
	movsd	%xmm2, (%r14)
	movsd	(%r15,%r8,8), %xmm3     # xmm3 = mem[0],zero
	movsd	(%r13,%r8,8), %xmm4     # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm3
	mulsd	(%r10,%rbp), %xmm3
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movsd	%xmm3, (%rax)
	movsd	(%r11,%rbx,8), %xmm2    # xmm2 = mem[0],zero
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rbx,8), %xmm4    # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm2
	mulsd	(%r9,%rbp), %xmm2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r12,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	divsd	%xmm4, %xmm2
	mulsd	(%r10,%rbp), %xmm2
	subsd	%xmm2, %xmm5
	movsd	%xmm5, (%r9,%rbp)
	movq	(%rax), %rcx
	movq	%rcx, (%r10,%rbp)
	incq	%rbx
	addq	$8, %rbp
	cmpq	$64, %rbx
	jne	.LBB6_47
# BB#48:                                #   in Loop: Header=BB6_46 Depth=2
	movq	168(%rsp), %rdx         # 8-byte Reload
	leaq	(%rsi,%rdx), %rcx
	imulq	$520, %r8, %rbp         # imm = 0x208
	movsd	512(%rbp,%rcx), %xmm2   # xmm2 = mem[0],zero
	imulq	$520, 56(%rsp), %rbx    # 8-byte Folded Reload
                                        # imm = 0x208
	subsd	512(%rbx,%rcx), %xmm2
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rcx,%rsi), %rcx
	addsd	(%rcx,%r8,8), %xmm2
	leaq	(%rdi,%rdx), %rcx
	subsd	512(%rbp,%rcx), %xmm2
	movsd	%xmm2, (%r14)
	movsd	(%r15,%r8,8), %xmm3     # xmm3 = mem[0],zero
	movsd	(%r13,%r8,8), %xmm4     # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm3
	leaq	(%r10,%rdx), %rbx
	mulsd	512(%rbp,%rbx), %xmm3
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movsd	%xmm3, (%rax)
	movsd	512(%r11), %xmm2        # xmm2 = mem[0],zero
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	512(%rcx), %xmm4        # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm2
	leaq	(%r9,%rdx), %rcx
	mulsd	512(%rbp,%rcx), %xmm2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r12,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movq	32(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	divsd	%xmm4, %xmm2
	mulsd	512(%rbp,%rbx), %xmm2
	subsd	%xmm2, %xmm5
	movsd	%xmm5, 512(%rbp,%rcx)
	movq	(%rax), %rcx
	movq	%rcx, 512(%rbp,%rbx)
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%r10,%rcx), %r9
	movq	40(%rsp), %rbp          # 8-byte Reload
	xorl	%ebx, %ebx
	movq	256(%rsp), %r10         # 8-byte Reload
	movq	248(%rsp), %rdx         # 8-byte Reload
	movq	240(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_49:                               #   Parent Loop BB6_45 Depth=1
                                        #     Parent Loop BB6_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	33280(%rdi,%rbx), %xmm2 # xmm2 = mem[0],zero
	subsd	(%rbp), %xmm2
	addsd	33288(%rsi,%rbx), %xmm2
	subsd	33280(%rsi,%rbx), %xmm2
	movsd	%xmm2, (%r14)
	movsd	512(%r15), %xmm3        # xmm3 = mem[0],zero
	movsd	(%r13,%r8,8), %xmm4     # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm3
	mulsd	(%r9,%rbx), %xmm3
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movsd	%xmm3, (%rax)
	movsd	(%r11,%rbx), %xmm2      # xmm2 = mem[0],zero
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rbx), %xmm4      # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm2
	mulsd	33280(%rdx,%rbx), %xmm2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r12,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	divsd	%xmm4, %xmm2
	mulsd	33280(%r10,%rbx), %xmm2
	subsd	%xmm2, %xmm5
	movsd	%xmm5, 33280(%rdx,%rbx)
	movq	(%rax), %rcx
	movq	%rcx, 33280(%r10,%rbx)
	addq	$8, %rbx
	addq	$8, %rbp
	cmpq	$512, %rbx              # imm = 0x200
	jne	.LBB6_49
# BB#50:                                #   in Loop: Header=BB6_46 Depth=2
	movq	72(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx), %xmm2           # xmm2 = mem[0],zero
	movq	136(%rsp), %rcx         # 8-byte Reload
	subsd	(%rcx), %xmm2
	movq	128(%rsp), %rcx         # 8-byte Reload
	addsd	(%rcx), %xmm2
	movq	120(%rsp), %rcx         # 8-byte Reload
	subsd	(%rcx), %xmm2
	movsd	%xmm2, (%r14)
	movsd	512(%r15), %xmm3        # xmm3 = mem[0],zero
	movsd	512(%r13), %xmm4        # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm3
	movq	112(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx), %xmm3
	movapd	%xmm1, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movsd	%xmm3, (%rax)
	movsd	512(%r11), %xmm2        # xmm2 = mem[0],zero
	movq	16(%rsp), %rsi          # 8-byte Reload
	movsd	512(%rsi), %xmm4        # xmm4 = mem[0],zero
	divsd	%xmm4, %xmm2
	movq	264(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi), %xmm2
	movq	24(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r12,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	mulsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movq	32(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	divsd	%xmm4, %xmm2
	mulsd	(%rcx), %xmm2
	subsd	%xmm2, %xmm5
	movsd	%xmm5, (%rsi)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	64(%rsp), %rbp          # 8-byte Reload
	addq	$520, %rbp              # imm = 0x208
	movq	56(%rsp), %r8           # 8-byte Reload
	cmpq	$64, %r8
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	104(%rsp), %rsi         # 8-byte Reload
	jne	.LBB6_46
# BB#51:                                #   in Loop: Header=BB6_45 Depth=1
	incq	%r12
	movq	216(%rsp), %rcx         # 8-byte Reload
	addq	$33800, %rcx            # imm = 0x8408
	addq	$520, 40(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x208
	cmpq	$64, %r12
	movq	224(%rsp), %r13         # 8-byte Reload
	jne	.LBB6_45
# BB#52:                                # %.preheader1.i.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_0(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movq	200(%rsp), %rbp         # 8-byte Reload
	movq	192(%rsp), %r9          # 8-byte Reload
.LBB6_53:                               # %.preheader1.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_54 Depth 2
                                        #       Child Loop BB6_56 Depth 3
	movq	%rax, %rbx
	xorl	%ecx, %ecx
.LBB6_54:                               # %.preheader.i196
                                        #   Parent Loop BB6_53 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_56 Depth 3
	movq	%rbx, %rsi
	movl	$1, %r8d
	jmp	.LBB6_56
	.p2align	4, 0x90
.LBB6_55:                               #   in Loop: Header=BB6_56 Depth=3
	addq	$2, %r8
	addq	$2, %rsi
.LBB6_56:                               #   Parent Loop BB6_53 Depth=1
                                        #     Parent Loop BB6_54 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbp,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movq	144(%rsp), %rdi         # 8-byte Reload
	movsd	-8(%rdi,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_98
# BB#57:                                #   in Loop: Header=BB6_56 Depth=3
	cmpq	$65, %r8
	jge	.LBB6_59
# BB#58:                                #   in Loop: Header=BB6_56 Depth=3
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movq	144(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	jbe	.LBB6_55
	jmp	.LBB6_99
	.p2align	4, 0x90
.LBB6_59:                               #   in Loop: Header=BB6_54 Depth=2
	incq	%rcx
	addq	$65, %rbx
	cmpq	$65, %rcx
	jl	.LBB6_54
# BB#60:                                #   in Loop: Header=BB6_53 Depth=1
	incq	%rdx
	addq	$4225, %rax             # imm = 0x1081
	cmpq	$65, %rdx
	jl	.LBB6_53
# BB#61:                                # %.preheader1.i200.preheader
	xorl	%edx, %edx
	movl	$1, %eax
.LBB6_62:                               # %.preheader1.i200
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_63 Depth 2
                                        #       Child Loop BB6_65 Depth 3
	movq	%rax, %rsi
	xorl	%ecx, %ecx
.LBB6_63:                               # %.preheader.i202
                                        #   Parent Loop BB6_62 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_65 Depth 3
	movq	%rsi, %rdi
	movl	$1, %r8d
	jmp	.LBB6_65
.LBB6_64:                               #   in Loop: Header=BB6_65 Depth=3
	addq	$2, %r8
	addq	$2, %rdi
.LBB6_65:                               #   Parent Loop BB6_62 Depth=1
                                        #     Parent Loop BB6_63 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%r9,%rdi,8), %xmm0   # xmm0 = mem[0],zero
	movq	104(%rsp), %rbp         # 8-byte Reload
	movsd	-8(%rbp,%rdi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_102
# BB#66:                                #   in Loop: Header=BB6_65 Depth=3
	cmpq	$65, %r8
	jge	.LBB6_68
# BB#67:                                #   in Loop: Header=BB6_65 Depth=3
	movsd	(%r9,%rdi,8), %xmm0     # xmm0 = mem[0],zero
	movq	104(%rsp), %rbp         # 8-byte Reload
	movsd	(%rbp,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	jbe	.LBB6_64
	jmp	.LBB6_103
.LBB6_68:                               #   in Loop: Header=BB6_63 Depth=2
	incq	%rcx
	addq	$65, %rsi
	cmpq	$65, %rcx
	jl	.LBB6_63
# BB#69:                                #   in Loop: Header=BB6_62 Depth=1
	incq	%rdx
	addq	$4225, %rax             # imm = 0x1081
	cmpq	$65, %rdx
	jl	.LBB6_62
# BB#70:                                # %.preheader1.i211.preheader
	xorl	%edx, %edx
	movl	$1, %eax
.LBB6_71:                               # %.preheader1.i211
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_72 Depth 2
                                        #       Child Loop BB6_74 Depth 3
	movq	%rax, %rsi
	xorl	%ecx, %ecx
.LBB6_72:                               # %.preheader.i213
                                        #   Parent Loop BB6_71 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_74 Depth 3
	movq	%rsi, %rdi
	movl	$1, %r8d
	jmp	.LBB6_74
.LBB6_73:                               #   in Loop: Header=BB6_74 Depth=3
	addq	$2, %r8
	addq	$2, %rdi
.LBB6_74:                               #   Parent Loop BB6_71 Depth=1
                                        #     Parent Loop BB6_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	160(%rsp), %rbp         # 8-byte Reload
	movsd	-8(%rbp,%rdi,8), %xmm0  # xmm0 = mem[0],zero
	movq	96(%rsp), %rbp          # 8-byte Reload
	movsd	-8(%rbp,%rdi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_105
# BB#75:                                #   in Loop: Header=BB6_74 Depth=3
	cmpq	$65, %r8
	jge	.LBB6_77
# BB#76:                                #   in Loop: Header=BB6_74 Depth=3
	movq	160(%rsp), %rbp         # 8-byte Reload
	movsd	(%rbp,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movq	96(%rsp), %rbp          # 8-byte Reload
	movsd	(%rbp,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	jbe	.LBB6_73
	jmp	.LBB6_106
.LBB6_77:                               #   in Loop: Header=BB6_72 Depth=2
	incq	%rcx
	addq	$65, %rsi
	cmpq	$65, %rcx
	jl	.LBB6_72
# BB#78:                                #   in Loop: Header=BB6_71 Depth=1
	incq	%rdx
	addq	$4225, %rax             # imm = 0x1081
	cmpq	$65, %rdx
	jl	.LBB6_71
# BB#79:                                # %.preheader1.i222.preheader
	xorl	%edx, %edx
	movl	$1, %eax
.LBB6_80:                               # %.preheader1.i222
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_81 Depth 2
                                        #       Child Loop BB6_83 Depth 3
	movq	%rax, %rsi
	xorl	%ecx, %ecx
.LBB6_81:                               # %.preheader.i224
                                        #   Parent Loop BB6_80 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_83 Depth 3
	movq	%rsi, %rdi
	movl	$1, %r8d
	jmp	.LBB6_83
.LBB6_82:                               #   in Loop: Header=BB6_83 Depth=3
	addq	$2, %r8
	addq	$2, %rdi
.LBB6_83:                               #   Parent Loop BB6_80 Depth=1
                                        #     Parent Loop BB6_81 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	152(%rsp), %rbp         # 8-byte Reload
	movsd	-8(%rbp,%rdi,8), %xmm0  # xmm0 = mem[0],zero
	movq	88(%rsp), %rbp          # 8-byte Reload
	movsd	-8(%rbp,%rdi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_107
# BB#84:                                #   in Loop: Header=BB6_83 Depth=3
	cmpq	$65, %r8
	jge	.LBB6_86
# BB#85:                                #   in Loop: Header=BB6_83 Depth=3
	movq	152(%rsp), %rbp         # 8-byte Reload
	movsd	(%rbp,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movq	88(%rsp), %rbp          # 8-byte Reload
	movsd	(%rbp,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	jbe	.LBB6_82
	jmp	.LBB6_108
.LBB6_86:                               #   in Loop: Header=BB6_81 Depth=2
	incq	%rcx
	addq	$65, %rsi
	cmpq	$65, %rcx
	jl	.LBB6_81
# BB#87:                                #   in Loop: Header=BB6_80 Depth=1
	incq	%rdx
	addq	$4225, %rax             # imm = 0x1081
	cmpq	$65, %rdx
	jl	.LBB6_80
# BB#88:                                # %.preheader4.i.preheader
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 136(%rsp)         # 8-byte Spill
.LBB6_89:                               # %.preheader4.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_90 Depth 2
                                        #       Child Loop BB6_94 Depth 3
                                        #       Child Loop BB6_92 Depth 3
	movq	72(%rsp), %rax          # 8-byte Reload
	shlq	$6, %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
.LBB6_90:                               # %.preheader.i233
                                        #   Parent Loop BB6_89 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_94 Depth 3
                                        #       Child Loop BB6_92 Depth 3
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	(%r12,%rax), %rax
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB6_93
# BB#91:                                # %.preheader.split.us.i.preheader
                                        #   in Loop: Header=BB6_90 Depth=2
	xorl	%ebx, %ebx
.LBB6_92:                               # %.preheader.split.us.i
                                        #   Parent Loop BB6_89 Depth=1
                                        #     Parent Loop BB6_90 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stderr(%rip), %rdi
	movq	64(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	56(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incq	%rbx
	cmpq	$65, %rbx
	jne	.LBB6_92
	jmp	.LBB6_95
.LBB6_93:                               # %.preheader.split.i.preheader
                                        #   in Loop: Header=BB6_90 Depth=2
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	movl	$65, %ebp
.LBB6_94:                               # %.preheader.split.i
                                        #   Parent Loop BB6_89 Depth=1
                                        #     Parent Loop BB6_90 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	stderr(%rip), %rdi
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rdi
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	addq	$8, %r15
	addq	$8, %r14
	addq	$8, %r13
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB6_94
.LBB6_95:                               # %.us-lcssa.us.i
                                        #   in Loop: Header=BB6_90 Depth=2
	movq	%r12, %rax
	incq	%rax
	addq	$520, 40(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x208
	addq	$520, 48(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x208
	addq	$520, 56(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x208
	addq	$520, 64(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x208
	movq	%rax, %r12
	cmpq	$65, %rax
	jne	.LBB6_90
# BB#96:                                #   in Loop: Header=BB6_89 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	addq	$33800, 136(%rsp)       # 8-byte Folded Spill
                                        # imm = 0x8408
	addq	$33800, 112(%rsp)       # 8-byte Folded Spill
                                        # imm = 0x8408
	addq	$33800, 120(%rsp)       # 8-byte Folded Spill
                                        # imm = 0x8408
	addq	$33800, 128(%rsp)       # 8-byte Folded Spill
                                        # imm = 0x8408
	movq	%rcx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	cmpq	$65, %rcx
	jne	.LBB6_89
# BB#97:                                # %print_array.exit
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	224(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	280(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	272(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	200(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	192(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	152(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	184(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	176(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	208(%rsp), %rdi         # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_101
.LBB6_98:                               # %check_FP.exit.threadsplit
	decq	%r8
.LBB6_99:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r9d
	pushq	%r8
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%rcx
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset -16
.LBB6_100:
	movl	$1, %eax
.LBB6_101:
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_102:                              # %check_FP.exit209.threadsplit
	decq	%r8
.LBB6_103:                              # %check_FP.exit209.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r9d
	pushq	%r8
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%rcx
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB6_100
.LBB6_104:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB6_105:                              # %check_FP.exit220.threadsplit
	decq	%r8
.LBB6_106:                              # %check_FP.exit220.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r9d
	pushq	%r8
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	%rcx
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB6_100
.LBB6_107:                              # %check_FP.exit231.threadsplit
	decq	%r8
.LBB6_108:                              # %check_FP.exit231.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r9d
	pushq	%r8
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%rcx
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$16, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB6_100
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4607182418800017408     # double 1
.LCPI7_1:
	.quad	4580160821035794432     # double 0.015625
.LCPI7_2:
	.quad	4611686018427387904     # double 2
.LCPI7_3:
	.quad	4613937818241073152     # double 3
.LCPI7_4:
	.quad	4616189618054758400     # double 4
.LCPI7_5:
	.quad	4617315517961601024     # double 5
.LCPI7_6:
	.quad	4618441417868443648     # double 6
.LCPI7_7:
	.quad	4621819117588971520     # double 10
.LCPI7_8:
	.quad	4622382067542392832     # double 11
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_9:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI7_10:
	.quad	4580160821035794432     # double 0.015625
	.quad	4580160821035794432     # double 0.015625
.LCPI7_11:
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
.LCPI7_12:
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
.LCPI7_13:
	.quad	2                       # 0x2
	.quad	2                       # 0x2
	.text
	.p2align	4, 0x90
	.type	init_array,@function
init_array:                             # @init_array
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%r9, -80(%rsp)          # 8-byte Spill
	movq	%r8, -88(%rsp)          # 8-byte Spill
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	%rdx, -48(%rsp)         # 8-byte Spill
	movq	104(%rsp), %r10
	movq	96(%rsp), %r11
	movq	88(%rsp), %r9
	movq	80(%rsp), %r8
	movq	72(%rsp), %rbp
	movq	64(%rsp), %rax
	movabsq	$4657366328514969600, %rbx # imm = 0x40A24A0000000000
	movq	%rbx, (%rdi)
	movabsq	$4631107791820423168, %rdi # imm = 0x4045000000000000
	movq	%rdi, (%rsi)
	xorl	%esi, %esi
	movsd	.LCPI7_0(%rip), %xmm10  # xmm10 = mem[0],zero
	movsd	.LCPI7_1(%rip), %xmm14  # xmm14 = mem[0],zero
	movsd	.LCPI7_2(%rip), %xmm11  # xmm11 = mem[0],zero
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_1:                                #   in Loop: Header=BB7_2 Depth=1
	cvtsi2sdl	%edi, %xmm0
	movapd	%xmm0, %xmm2
	addsd	%xmm10, %xmm2
	mulsd	%xmm14, %xmm2
	movsd	%xmm2, 8(%rax,%rsi,8)
	addsd	%xmm11, %xmm0
	mulsd	%xmm14, %xmm0
	movsd	%xmm0, 8(%rbp,%rsi,8)
	incq	%rdi
	movq	%rdi, %rsi
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%esi, %xmm3
	movapd	%xmm3, %xmm4
	addsd	%xmm10, %xmm4
	mulsd	%xmm14, %xmm4
	movsd	%xmm4, (%rax,%rsi,8)
	addsd	%xmm11, %xmm3
	mulsd	%xmm14, %xmm3
	movsd	%xmm3, (%rbp,%rsi,8)
	leaq	1(%rsi), %rdi
	cmpq	$65, %rdi
	jne	.LBB7_1
# BB#3:                                 # %.preheader3.preheader
	xorl	%eax, %eax
	movsd	.LCPI7_3(%rip), %xmm15  # xmm15 = mem[0],zero
	movsd	.LCPI7_4(%rip), %xmm4   # xmm4 = mem[0],zero
	jmp	.LBB7_5
	.p2align	4, 0x90
.LBB7_4:                                # %.preheader3.1
                                        #   in Loop: Header=BB7_5 Depth=1
	cvtsi2sdl	%esi, %xmm0
	movapd	%xmm0, %xmm2
	addsd	%xmm15, %xmm2
	mulsd	%xmm14, %xmm2
	movsd	%xmm2, 8(%r8,%rax,8)
	addsd	%xmm4, %xmm0
	mulsd	%xmm14, %xmm0
	movsd	%xmm0, 8(%r9,%rax,8)
	incq	%rsi
	movq	%rsi, %rax
.LBB7_5:                                # %.preheader3
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%eax, %xmm5
	movapd	%xmm5, %xmm6
	addsd	%xmm15, %xmm6
	mulsd	%xmm14, %xmm6
	movsd	%xmm6, (%r8,%rax,8)
	addsd	%xmm4, %xmm5
	mulsd	%xmm14, %xmm5
	movsd	%xmm5, (%r9,%rax,8)
	leaq	1(%rax), %rsi
	cmpq	$65, %rsi
	jne	.LBB7_4
# BB#6:                                 # %.preheader2.preheader
	xorl	%eax, %eax
	movsd	.LCPI7_5(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI7_6(%rip), %xmm5   # xmm5 = mem[0],zero
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_7:                                # %.preheader2.1
                                        #   in Loop: Header=BB7_8 Depth=1
	cvtsi2sdl	%esi, %xmm0
	movapd	%xmm0, %xmm2
	addsd	%xmm4, %xmm2
	mulsd	%xmm14, %xmm2
	movsd	%xmm2, 8(%r11,%rax,8)
	addsd	%xmm5, %xmm0
	mulsd	%xmm14, %xmm0
	movsd	%xmm0, 8(%r10,%rax,8)
	incq	%rsi
	movq	%rsi, %rax
.LBB7_8:                                # %.preheader2
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%eax, %xmm6
	movapd	%xmm6, %xmm7
	addsd	%xmm4, %xmm7
	mulsd	%xmm14, %xmm7
	movsd	%xmm7, (%r11,%rax,8)
	addsd	%xmm5, %xmm6
	mulsd	%xmm14, %xmm6
	movsd	%xmm6, (%r10,%rax,8)
	leaq	1(%rax), %rsi
	cmpq	$65, %rsi
	jne	.LBB7_7
# BB#9:                                 # %.preheader.preheader
	xorl	%edx, %edx
	movsd	.LCPI7_7(%rip), %xmm8   # xmm8 = mem[0],zero
	movsd	.LCPI7_8(%rip), %xmm9   # xmm9 = mem[0],zero
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	%xmm0, -24(%rsp)        # 16-byte Spill
	movapd	.LCPI7_9(%rip), %xmm3   # xmm3 = [1.000000e+00,1.000000e+00]
	movapd	.LCPI7_10(%rip), %xmm13 # xmm13 = [1.562500e-02,1.562500e-02]
	movapd	.LCPI7_11(%rip), %xmm4  # xmm4 = [2.000000e+00,2.000000e+00]
	movapd	.LCPI7_12(%rip), %xmm1  # xmm1 = [3.000000e+00,3.000000e+00]
	movdqa	.LCPI7_13(%rip), %xmm12 # xmm12 = [2,2]
	movq	56(%rsp), %r10
	movq	-80(%rsp), %r11         # 8-byte Reload
	movq	-88(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB7_10:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_13 Depth 2
                                        #       Child Loop BB7_17 Depth 3
                                        #       Child Loop BB7_20 Depth 3
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	%xmm0, -32(%rsp)        # 8-byte Spill
	movq	%r10, -56(%rsp)         # 8-byte Spill
	movq	%r11, -64(%rsp)         # 8-byte Spill
	movq	%r14, -72(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_13:                               # %min.iters.checked
                                        #   Parent Loop BB7_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_17 Depth 3
                                        #       Child Loop BB7_20 Depth 3
	movq	%r13, %rsi
	imulq	$33800, %rdx, %r9       # imm = 0x8408
	movq	-88(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rbx
	imulq	$520, %rsi, %r15        # imm = 0x208
	leaq	520(%r15,%rbx), %rbp
	addq	%r15, %rbx
	movq	-80(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rax
	leaq	520(%r15,%rax), %r13
	addq	%r15, %rax
	addq	56(%rsp), %r9
	leaq	(%r9,%r15), %rdi
	leaq	520(%r15,%r9), %r15
	imulq	$520, %rdx, %r12        # imm = 0x208
	movq	-40(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r12), %r9
	addq	-48(%rsp), %r12         # 8-byte Folded Reload
	cmpq	%r13, %rbx
	sbbb	%r8b, %r8b
	cmpq	%rbp, %rax
	sbbb	%cl, %cl
	andb	%r8b, %cl
	cmpq	%r15, %rbx
	sbbb	%bl, %bl
	cmpq	%rbp, %rdi
	sbbb	%bpl, %bpl
	cmpq	%r15, %rax
	sbbb	%al, %al
	cmpq	%r13, %rdi
	leaq	1(%rsi), %r13
	cvtsi2sdl	%r13d, %xmm5
	movsd	-32(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm5
	addsd	%xmm8, %xmm5
	mulsd	%xmm14, %xmm5
	movsd	%xmm5, (%r9,%rsi,8)
	leal	2(%rsi), %edi
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%edi, %xmm5
	mulsd	%xmm0, %xmm5
	addsd	%xmm9, %xmm5
	mulsd	%xmm14, %xmm5
	movsd	%xmm5, (%r12,%rsi,8)
	leal	3(%rsi), %edi
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%edi, %xmm5
	leal	4(%rsi), %edi
	cvtsi2sdl	%edi, %xmm6
	leal	5(%rsi), %esi
	cvtsi2sdl	%esi, %xmm7
	sbbb	%sil, %sil
	testb	$1, %cl
	mulsd	%xmm0, %xmm5
	mulsd	%xmm0, %xmm6
	mulsd	%xmm0, %xmm7
	jne	.LBB7_12
# BB#14:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_13 Depth=2
	andb	%bpl, %bl
	andb	$1, %bl
	jne	.LBB7_12
# BB#15:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_13 Depth=2
	andb	%sil, %al
	andb	$1, %al
	movl	$0, %eax
	jne	.LBB7_19
# BB#16:                                # %vector.ph
                                        #   in Loop: Header=BB7_13 Depth=2
	movapd	%xmm5, %xmm8
	movlhps	%xmm8, %xmm8            # xmm8 = xmm8[0,0]
	movapd	%xmm6, %xmm9
	movlhps	%xmm9, %xmm9            # xmm9 = xmm9[0,0]
	movapd	%xmm7, %xmm10
	movlhps	%xmm10, %xmm10          # xmm10 = xmm10[0,0]
	movl	$64, %eax
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%r14, %rbx
	movdqa	%xmm12, %xmm15
	movdqa	-24(%rsp), %xmm12       # 16-byte Reload
	.p2align	4, 0x90
.LBB7_17:                               # %vector.body
                                        #   Parent Loop BB7_10 Depth=1
                                        #     Parent Loop BB7_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	pshufd	$232, %xmm12, %xmm11    # xmm11 = xmm12[0,2,2,3]
	cvtdq2pd	%xmm11, %xmm0
	movaps	%xmm8, %xmm2
	addpd	%xmm0, %xmm2
	addpd	%xmm3, %xmm2
	mulpd	%xmm13, %xmm2
	movupd	%xmm2, (%rbx)
	movaps	%xmm9, %xmm2
	addpd	%xmm0, %xmm2
	addpd	%xmm4, %xmm2
	mulpd	%xmm13, %xmm2
	movupd	%xmm2, (%rdi)
	addpd	%xmm10, %xmm0
	addpd	%xmm1, %xmm0
	mulpd	%xmm13, %xmm0
	movupd	%xmm0, (%rsi)
	paddq	%xmm15, %xmm12
	addq	$16, %rbx
	addq	$16, %rdi
	addq	$16, %rsi
	addq	$-2, %rax
	jne	.LBB7_17
# BB#18:                                #   in Loop: Header=BB7_13 Depth=2
	movl	$64, %eax
	movsd	.LCPI7_7(%rip), %xmm8   # xmm8 = mem[0],zero
	movsd	.LCPI7_8(%rip), %xmm9   # xmm9 = mem[0],zero
	movsd	.LCPI7_0(%rip), %xmm10  # xmm10 = mem[0],zero
	movsd	.LCPI7_2(%rip), %xmm11  # xmm11 = mem[0],zero
	movdqa	%xmm15, %xmm12
	movsd	.LCPI7_3(%rip), %xmm15  # xmm15 = mem[0],zero
	jmp	.LBB7_19
	.p2align	4, 0x90
.LBB7_12:                               #   in Loop: Header=BB7_13 Depth=2
	xorl	%eax, %eax
.LBB7_19:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB7_13 Depth=2
	leaq	(%r14,%rax,8), %rsi
	leaq	(%r11,%rax,8), %rdi
	leaq	(%r10,%rax,8), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_20:                               # %scalar.ph
                                        #   Parent Loop BB7_10 Depth=1
                                        #     Parent Loop BB7_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rax,%rbx), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movapd	%xmm5, %xmm2
	addsd	%xmm0, %xmm2
	addsd	%xmm10, %xmm2
	mulsd	%xmm14, %xmm2
	movsd	%xmm2, (%rsi,%rbx,8)
	movapd	%xmm6, %xmm2
	addsd	%xmm0, %xmm2
	addsd	%xmm11, %xmm2
	mulsd	%xmm14, %xmm2
	movsd	%xmm2, (%rdi,%rbx,8)
	addsd	%xmm7, %xmm0
	addsd	%xmm15, %xmm0
	mulsd	%xmm14, %xmm0
	movsd	%xmm0, (%rbp,%rbx,8)
	leaq	1(%rax,%rbx), %rcx
	incq	%rbx
	cmpq	$65, %rcx
	jne	.LBB7_20
# BB#21:                                # %.loopexit
                                        #   in Loop: Header=BB7_13 Depth=2
	addq	$520, %r14              # imm = 0x208
	addq	$520, %r11              # imm = 0x208
	addq	$520, %r10              # imm = 0x208
	cmpq	$65, %r13
	jne	.LBB7_13
# BB#22:                                #   in Loop: Header=BB7_10 Depth=1
	incq	%rdx
	movq	-72(%rsp), %r14         # 8-byte Reload
	addq	$33800, %r14            # imm = 0x8408
	movq	-64(%rsp), %r11         # 8-byte Reload
	addq	$33800, %r11            # imm = 0x8408
	movq	-56(%rsp), %r10         # 8-byte Reload
	addq	$33800, %r10            # imm = 0x8408
	cmpq	$65, %rdx
	jne	.LBB7_10
# BB#23:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	init_array, .Lfunc_end7-init_array
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d][%d] = %lf and B[%d][%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 84

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%0.2lf "
	.size	.L.str.3, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
