	.text
	.file	"btConvexPointCloudShape.bc"
	.globl	_ZN23btConvexPointCloudShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN23btConvexPointCloudShape15setLocalScalingERK9btVector3,@function
_ZN23btConvexPointCloudShape15setLocalScalingERK9btVector3: # @_ZN23btConvexPointCloudShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 24(%rdi)
	jmp	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv # TAILCALL
.Lfunc_end0:
	.size	_ZN23btConvexPointCloudShape15setLocalScalingERK9btVector3, .Lfunc_end0-_ZN23btConvexPointCloudShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1065353216              # float 1
.LCPI1_1:
	.long	953267991               # float 9.99999974E-5
.LCPI1_2:
	.long	3713928043              # float -9.99999984E+17
	.text
	.globl	_ZNK23btConvexPointCloudShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK23btConvexPointCloudShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK23btConvexPointCloudShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movss	(%rsi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm10, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm11, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB1_2
# BB#1:
	xorps	%xmm10, %xmm10
	movss	.LCPI1_0(%rip), %xmm9   # xmm9 = mem[0],zero,zero,zero
	xorps	%xmm11, %xmm11
	jmp	.LBB1_5
.LBB1_2:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_4
# BB#3:                                 # %call.sqrt
	movss	%xmm9, 12(%rsp)         # 4-byte Spill
	movss	%xmm10, 8(%rsp)         # 4-byte Spill
	movss	%xmm11, 4(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm11         # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm10         # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB1_4:                                # %.split
	movss	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm9
	mulss	%xmm0, %xmm10
	mulss	%xmm0, %xmm11
.LBB1_5:                                # %.preheader
	movslq	112(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB1_6
# BB#8:                                 # %.lr.ph
	movq	104(%rbx), %rcx
	movsd	24(%rbx), %xmm8         # xmm8 = mem[0],zero
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addq	$8, %rcx
	xorps	%xmm1, %xmm1
	movss	.LCPI1_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	xorl	%edx, %edx
	xorps	%xmm5, %xmm5
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movsd	-8(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulps	%xmm8, %xmm0
	movss	(%rcx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	movaps	%xmm0, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm9, %xmm6
	mulss	%xmm0, %xmm6
	mulss	%xmm10, %xmm2
	addss	%xmm6, %xmm2
	movaps	%xmm11, %xmm6
	mulss	%xmm7, %xmm6
	addss	%xmm2, %xmm6
	ucomiss	%xmm4, %xmm6
	ja	.LBB1_10
# BB#11:                                #   in Loop: Header=BB1_9 Depth=1
	movaps	%xmm5, %xmm0
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_10:                               #   in Loop: Header=BB1_9 Depth=1
	xorps	%xmm1, %xmm1
	movss	%xmm7, %xmm1            # xmm1 = xmm7[0],xmm1[1,2,3]
.LBB1_12:                               #   in Loop: Header=BB1_9 Depth=1
	maxss	%xmm4, %xmm6
	incq	%rdx
	addq	$16, %rcx
	cmpq	%rax, %rdx
	movaps	%xmm6, %xmm4
	movaps	%xmm0, %xmm5
	jl	.LBB1_9
	jmp	.LBB1_7
.LBB1_6:
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
.LBB1_7:                                # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZNK23btConvexPointCloudShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end1-_ZNK23btConvexPointCloudShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.globl	_ZNK23btConvexPointCloudShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK23btConvexPointCloudShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK23btConvexPointCloudShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	testl	%ecx, %ecx
	jle	.LBB2_8
# BB#1:                                 # %.lr.ph42.preheader
	movl	%ecx, %r8d
	leaq	-1(%r8), %r9
	movq	%r8, %r10
	andq	$7, %r10
	je	.LBB2_2
# BB#3:                                 # %.lr.ph42.prol.preheader
	leaq	12(%rdx), %rax
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph42.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$-581039253, (%rax)     # imm = 0xDD5E0B6B
	incq	%r11
	addq	$16, %rax
	cmpq	%r11, %r10
	jne	.LBB2_4
	jmp	.LBB2_5
.LBB2_2:
	xorl	%r11d, %r11d
.LBB2_5:                                # %.lr.ph42.prol.loopexit
	cmpq	$7, %r9
	jb	.LBB2_8
# BB#6:                                 # %.lr.ph42.preheader.new
	subq	%r11, %r8
	shlq	$4, %r11
	leaq	124(%rdx,%r11), %rax
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	movl	$-581039253, -112(%rax) # imm = 0xDD5E0B6B
	movl	$-581039253, -96(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -80(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -64(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -48(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -32(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, -16(%rax)  # imm = 0xDD5E0B6B
	movl	$-581039253, (%rax)     # imm = 0xDD5E0B6B
	subq	$-128, %rax
	addq	$-8, %r8
	jne	.LBB2_7
.LBB2_8:                                # %.preheader
	movl	112(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB2_20
# BB#9:                                 # %.lr.ph38
	testl	%ecx, %ecx
	jle	.LBB2_10
# BB#14:                                # %.lr.ph38.split.us.preheader
	movl	%ecx, %r8d
	addq	$8, %rsi
	addq	$12, %rdx
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB2_15:                               # %.lr.ph38.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_16 Depth 2
	movq	104(%rdi), %rax
	movq	%r9, %rcx
	shlq	$4, %rcx
	movsd	(%rax,%rcx), %xmm1      # xmm1 = mem[0],zero
	movsd	24(%rdi), %xmm0         # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	8(%rax,%rcx), %xmm2     # xmm2 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movq	%r8, %r10
	movq	%rdx, %rcx
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB2_16:                               #   Parent Loop BB2_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	-8(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	-4(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm5, %xmm4
	ucomiss	(%rcx), %xmm4
	jbe	.LBB2_18
# BB#17:                                #   in Loop: Header=BB2_16 Depth=2
	movlps	%xmm0, -12(%rcx)
	movlps	%xmm3, -4(%rcx)
	movss	%xmm4, (%rcx)
.LBB2_18:                               #   in Loop: Header=BB2_16 Depth=2
	addq	$16, %rax
	addq	$16, %rcx
	decq	%r10
	jne	.LBB2_16
# BB#19:                                # %._crit_edge.us
                                        #   in Loop: Header=BB2_15 Depth=1
	incq	%r9
	movslq	112(%rdi), %rax
	cmpq	%rax, %r9
	jl	.LBB2_15
	jmp	.LBB2_20
.LBB2_10:                               # %.lr.ph38.split.preheader
	leal	-1(%rax), %edx
	movl	%eax, %esi
	xorl	%ecx, %ecx
	andl	$7, %esi
	je	.LBB2_12
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph38.split.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%ecx
	cmpl	%ecx, %esi
	jne	.LBB2_11
.LBB2_12:                               # %.lr.ph38.split.prol.loopexit
	cmpl	$7, %edx
	jb	.LBB2_20
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph38.split
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %ecx
	cmpl	%eax, %ecx
	jl	.LBB2_13
.LBB2_20:                               # %._crit_edge39
	retq
.Lfunc_end2:
	.size	_ZNK23btConvexPointCloudShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end2-_ZNK23btConvexPointCloudShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	679477248               # float 1.42108547E-14
.LCPI3_2:
	.long	3212836864              # float -1
.LCPI3_3:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_1:
	.long	3212836864              # float -1
	.long	3212836864              # float -1
	.zero	4
	.zero	4
	.text
	.globl	_ZNK23btConvexPointCloudShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape24localGetSupportingVertexERK9btVector3,@function
_ZNK23btConvexPointCloudShape24localGetSupportingVertexERK9btVector3: # @_ZNK23btConvexPointCloudShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 96
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*104(%rax)
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB3_2
	jnp	.LBB3_1
.LBB3_2:
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI3_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm2
	seta	%al
	movd	%eax, %xmm1
	pshufd	$80, %xmm1, %xmm2       # xmm2 = xmm1[0,0,1,1]
	pslld	$31, %xmm2
	psrad	$31, %xmm2
	movdqa	%xmm2, %xmm1
	pandn	%xmm0, %xmm1
	pand	.LCPI3_1(%rip), %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movdqa	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	jbe	.LBB3_4
# BB#3:
	movss	.LCPI3_2(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
.LBB3_4:
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_6
# BB#5:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movdqa	%xmm2, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movdqa	(%rsp), %xmm2           # 16-byte Reload
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB3_6:                                # %.split
	movss	.LCPI3_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	movaps	(%rsp), %xmm2           # 16-byte Reload
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addps	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm2, %xmm1
	jmp	.LBB3_7
.LBB3_1:
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	32(%rsp), %xmm1         # 16-byte Reload
.LBB3_7:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZNK23btConvexPointCloudShape24localGetSupportingVertexERK9btVector3, .Lfunc_end3-_ZNK23btConvexPointCloudShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.globl	_ZNK23btConvexPointCloudShape14getNumVerticesEv
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape14getNumVerticesEv,@function
_ZNK23btConvexPointCloudShape14getNumVerticesEv: # @_ZNK23btConvexPointCloudShape14getNumVerticesEv
	.cfi_startproc
# BB#0:
	movl	112(%rdi), %eax
	retq
.Lfunc_end4:
	.size	_ZNK23btConvexPointCloudShape14getNumVerticesEv, .Lfunc_end4-_ZNK23btConvexPointCloudShape14getNumVerticesEv
	.cfi_endproc

	.globl	_ZNK23btConvexPointCloudShape11getNumEdgesEv
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape11getNumEdgesEv,@function
_ZNK23btConvexPointCloudShape11getNumEdgesEv: # @_ZNK23btConvexPointCloudShape11getNumEdgesEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	_ZNK23btConvexPointCloudShape11getNumEdgesEv, .Lfunc_end5-_ZNK23btConvexPointCloudShape11getNumEdgesEv
	.cfi_endproc

	.globl	_ZNK23btConvexPointCloudShape7getEdgeEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape7getEdgeEiR9btVector3S1_,@function
_ZNK23btConvexPointCloudShape7getEdgeEiR9btVector3S1_: # @_ZNK23btConvexPointCloudShape7getEdgeEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end6:
	.size	_ZNK23btConvexPointCloudShape7getEdgeEiR9btVector3S1_, .Lfunc_end6-_ZNK23btConvexPointCloudShape7getEdgeEiR9btVector3S1_
	.cfi_endproc

	.globl	_ZNK23btConvexPointCloudShape9getVertexEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape9getVertexEiR9btVector3,@function
_ZNK23btConvexPointCloudShape9getVertexEiR9btVector3: # @_ZNK23btConvexPointCloudShape9getVertexEiR9btVector3
	.cfi_startproc
# BB#0:
	movq	104(%rdi), %rax
	movslq	%esi, %rcx
	shlq	$4, %rcx
	movsd	(%rax,%rcx), %xmm0      # xmm0 = mem[0],zero
	movsd	24(%rdi), %xmm1         # xmm1 = mem[0],zero
	mulps	%xmm0, %xmm1
	movss	8(%rax,%rcx), %xmm0     # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, (%rdx)
	movlps	%xmm2, 8(%rdx)
	retq
.Lfunc_end7:
	.size	_ZNK23btConvexPointCloudShape9getVertexEiR9btVector3, .Lfunc_end7-_ZNK23btConvexPointCloudShape9getVertexEiR9btVector3
	.cfi_endproc

	.globl	_ZNK23btConvexPointCloudShape12getNumPlanesEv
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape12getNumPlanesEv,@function
_ZNK23btConvexPointCloudShape12getNumPlanesEv: # @_ZNK23btConvexPointCloudShape12getNumPlanesEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	_ZNK23btConvexPointCloudShape12getNumPlanesEv, .Lfunc_end8-_ZNK23btConvexPointCloudShape12getNumPlanesEv
	.cfi_endproc

	.globl	_ZNK23btConvexPointCloudShape8getPlaneER9btVector3S1_i
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape8getPlaneER9btVector3S1_i,@function
_ZNK23btConvexPointCloudShape8getPlaneER9btVector3S1_i: # @_ZNK23btConvexPointCloudShape8getPlaneER9btVector3S1_i
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end9:
	.size	_ZNK23btConvexPointCloudShape8getPlaneER9btVector3S1_i, .Lfunc_end9-_ZNK23btConvexPointCloudShape8getPlaneER9btVector3S1_i
	.cfi_endproc

	.globl	_ZNK23btConvexPointCloudShape8isInsideERK9btVector3f
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape8isInsideERK9btVector3f,@function
_ZNK23btConvexPointCloudShape8isInsideERK9btVector3f: # @_ZNK23btConvexPointCloudShape8isInsideERK9btVector3f
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	_ZNK23btConvexPointCloudShape8isInsideERK9btVector3f, .Lfunc_end10-_ZNK23btConvexPointCloudShape8isInsideERK9btVector3f
	.cfi_endproc

	.section	.text._ZN23btConvexPointCloudShapeD0Ev,"axG",@progbits,_ZN23btConvexPointCloudShapeD0Ev,comdat
	.weak	_ZN23btConvexPointCloudShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN23btConvexPointCloudShapeD0Ev,@function
_ZN23btConvexPointCloudShapeD0Ev:       # @_ZN23btConvexPointCloudShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB11_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN23btConvexPointCloudShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN23btConvexPointCloudShapeD0Ev, .Lfunc_end11-_ZN23btConvexPointCloudShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp4     #   Call between .Ltmp4 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end12:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end12-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK23btConvexPointCloudShape7getNameEv,"axG",@progbits,_ZNK23btConvexPointCloudShape7getNameEv,comdat
	.weak	_ZNK23btConvexPointCloudShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK23btConvexPointCloudShape7getNameEv,@function
_ZNK23btConvexPointCloudShape7getNameEv: # @_ZNK23btConvexPointCloudShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end13:
	.size	_ZNK23btConvexPointCloudShape7getNameEv, .Lfunc_end13-_ZNK23btConvexPointCloudShape7getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end14-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end15:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end15-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end16:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end16-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end17:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end17-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end18:
	.size	__clang_call_terminate, .Lfunc_end18-__clang_call_terminate

	.type	_ZTV23btConvexPointCloudShape,@object # @_ZTV23btConvexPointCloudShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV23btConvexPointCloudShape
	.p2align	3
_ZTV23btConvexPointCloudShape:
	.quad	0
	.quad	_ZTI23btConvexPointCloudShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN23btConvexPointCloudShapeD0Ev
	.quad	_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN23btConvexPointCloudShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK23btConvexPointCloudShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK23btConvexPointCloudShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK23btConvexPointCloudShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK23btConvexPointCloudShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK23btConvexPointCloudShape14getNumVerticesEv
	.quad	_ZNK23btConvexPointCloudShape11getNumEdgesEv
	.quad	_ZNK23btConvexPointCloudShape7getEdgeEiR9btVector3S1_
	.quad	_ZNK23btConvexPointCloudShape9getVertexEiR9btVector3
	.quad	_ZNK23btConvexPointCloudShape12getNumPlanesEv
	.quad	_ZNK23btConvexPointCloudShape8getPlaneER9btVector3S1_i
	.quad	_ZNK23btConvexPointCloudShape8isInsideERK9btVector3f
	.size	_ZTV23btConvexPointCloudShape, 216

	.type	_ZTS23btConvexPointCloudShape,@object # @_ZTS23btConvexPointCloudShape
	.globl	_ZTS23btConvexPointCloudShape
	.p2align	4
_ZTS23btConvexPointCloudShape:
	.asciz	"23btConvexPointCloudShape"
	.size	_ZTS23btConvexPointCloudShape, 26

	.type	_ZTI23btConvexPointCloudShape,@object # @_ZTI23btConvexPointCloudShape
	.globl	_ZTI23btConvexPointCloudShape
	.p2align	4
_ZTI23btConvexPointCloudShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23btConvexPointCloudShape
	.quad	_ZTI34btPolyhedralConvexAabbCachingShape
	.size	_ZTI23btConvexPointCloudShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ConvexPointCloud"
	.size	.L.str, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
