	.text
	.file	"Deflate64Register.bc"
	.p2align	4, 0x90
	.type	_ZL20CreateCodecDeflate64v,@function
_ZL20CreateCodecDeflate64v:             # @_ZL20CreateCodecDeflate64v
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$3480, %edi             # imm = 0xD98
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp0:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderC2Ebb
.Ltmp1:
# BB#1:
	movl	$_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E+264, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	$_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E+328, 32(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZL20CreateCodecDeflate64v, .Lfunc_end0-_ZL20CreateCodecDeflate64v
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL23CreateCodecOutDeflate64v,@function
_ZL23CreateCodecOutDeflate64v:          # @_ZL23CreateCodecOutDeflate64v
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movl	$39792, %edi            # imm = 0x9B70
	callq	_Znwm
	movq	%rax, %rbx
	movl	$_ZTV27ICompressSetCoderProperties+16, %eax
	movd	%rax, %xmm0
	movl	$_ZTV14ICompressCoder+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$0, 16(%rbx)
	leaq	24(%rbx), %rdi
.Ltmp3:
	movl	$1, %esi
	callq	_ZN9NCompress8NDeflate8NEncoder6CCoderC2Eb
.Ltmp4:
# BB#1:
	movl	$_ZTVN9NCompress8NDeflate8NEncoder11CCOMCoder64E+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NEncoder11CCOMCoder64E+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_2:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZL23CreateCodecOutDeflate64v, .Lfunc_end1-_ZL23CreateCodecOutDeflate64v
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB2_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB2_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB2_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB2_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB2_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB2_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB2_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB2_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB2_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB2_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB2_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB2_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB2_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB2_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB2_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB2_16
.LBB2_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressGetInStreamProcessedSize(%rip), %cl
	jne	.LBB2_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+1(%rip), %al
	jne	.LBB2_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+2(%rip), %al
	jne	.LBB2_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+3(%rip), %al
	jne	.LBB2_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+4(%rip), %al
	jne	.LBB2_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+5(%rip), %al
	jne	.LBB2_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+6(%rip), %al
	jne	.LBB2_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+7(%rip), %al
	jne	.LBB2_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+8(%rip), %al
	jne	.LBB2_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+9(%rip), %al
	jne	.LBB2_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+10(%rip), %al
	jne	.LBB2_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+11(%rip), %al
	jne	.LBB2_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+12(%rip), %al
	jne	.LBB2_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+13(%rip), %al
	jne	.LBB2_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+14(%rip), %al
	jne	.LBB2_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_ICompressGetInStreamProcessedSize+15(%rip), %al
	jne	.LBB2_33
.LBB2_16:
	leaq	8(%rdi), %rax
	jmp	.LBB2_84
.LBB2_33:                               # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_ICompressSetInStream(%rip), %cl
	jne	.LBB2_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetInStream+1(%rip), %al
	jne	.LBB2_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetInStream+2(%rip), %al
	jne	.LBB2_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetInStream+3(%rip), %al
	jne	.LBB2_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetInStream+4(%rip), %al
	jne	.LBB2_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetInStream+5(%rip), %al
	jne	.LBB2_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetInStream+6(%rip), %al
	jne	.LBB2_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetInStream+7(%rip), %al
	jne	.LBB2_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetInStream+8(%rip), %al
	jne	.LBB2_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetInStream+9(%rip), %al
	jne	.LBB2_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetInStream+10(%rip), %al
	jne	.LBB2_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetInStream+11(%rip), %al
	jne	.LBB2_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetInStream+12(%rip), %al
	jne	.LBB2_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetInStream+13(%rip), %al
	jne	.LBB2_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetInStream+14(%rip), %al
	jne	.LBB2_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetInStream+15(%rip), %al
	jne	.LBB2_50
# BB#49:
	leaq	16(%rdi), %rax
	jmp	.LBB2_84
.LBB2_50:                               # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_ICompressSetOutStreamSize(%rip), %cl
	jne	.LBB2_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+1(%rip), %al
	jne	.LBB2_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+2(%rip), %al
	jne	.LBB2_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+3(%rip), %al
	jne	.LBB2_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+4(%rip), %al
	jne	.LBB2_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+5(%rip), %al
	jne	.LBB2_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+6(%rip), %al
	jne	.LBB2_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+7(%rip), %al
	jne	.LBB2_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+8(%rip), %al
	jne	.LBB2_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+9(%rip), %al
	jne	.LBB2_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+10(%rip), %al
	jne	.LBB2_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+11(%rip), %al
	jne	.LBB2_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+12(%rip), %al
	jne	.LBB2_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+13(%rip), %al
	jne	.LBB2_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+14(%rip), %al
	jne	.LBB2_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetOutStreamSize+15(%rip), %al
	jne	.LBB2_67
# BB#66:
	leaq	24(%rdi), %rax
	jmp	.LBB2_84
.LBB2_67:                               # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ISequentialInStream(%rip), %cl
	jne	.LBB2_85
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_ISequentialInStream+1(%rip), %cl
	jne	.LBB2_85
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_ISequentialInStream+2(%rip), %cl
	jne	.LBB2_85
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_ISequentialInStream+3(%rip), %cl
	jne	.LBB2_85
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_ISequentialInStream+4(%rip), %cl
	jne	.LBB2_85
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_ISequentialInStream+5(%rip), %cl
	jne	.LBB2_85
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_ISequentialInStream+6(%rip), %cl
	jne	.LBB2_85
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_ISequentialInStream+7(%rip), %cl
	jne	.LBB2_85
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_ISequentialInStream+8(%rip), %cl
	jne	.LBB2_85
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_ISequentialInStream+9(%rip), %cl
	jne	.LBB2_85
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_ISequentialInStream+10(%rip), %cl
	jne	.LBB2_85
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_ISequentialInStream+11(%rip), %cl
	jne	.LBB2_85
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_ISequentialInStream+12(%rip), %cl
	jne	.LBB2_85
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_ISequentialInStream+13(%rip), %cl
	jne	.LBB2_85
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_ISequentialInStream+14(%rip), %cl
	jne	.LBB2_85
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_ISequentialInStream+15(%rip), %cl
	jne	.LBB2_85
# BB#83:
	leaq	32(%rdi), %rax
.LBB2_84:                               # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB2_85:                               # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end2:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end2-_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	40(%rdi), %eax
	incl	%eax
	movl	%eax, 40(%rdi)
	retq
.Lfunc_end3:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end3-_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 16
	movl	40(%rdi), %eax
	decl	%eax
	movl	%eax, 40(%rdi)
	jne	.LBB4_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB4_2:
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end4-_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev,@function
_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev: # @_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+128, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+264, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+192, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	$_ZTVN9NCompress8NDeflate8NDecoder6CCoderE+328, 32(%rbx)
	leaq	112(%rbx), %rdi
.Ltmp6:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp7:
# BB#1:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp12:
	callq	*16(%rax)
.Ltmp13:
.LBB5_3:                                # %_ZN5NBitl12CBaseDecoderI9CInBufferED2Ev.exit
	leaq	48(%rbx), %rdi
.Ltmp24:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp25:
# BB#4:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp30:
	callq	*16(%rax)
.Ltmp31:
.LBB5_6:                                # %_ZN10COutBufferD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_20:
.Ltmp32:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_13:
.Ltmp14:
	movq	%rax, %r14
	jmp	.LBB5_14
.LBB5_10:
.Ltmp26:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_21
# BB#11:
	movq	(%rdi), %rax
.Ltmp27:
	callq	*16(%rax)
.Ltmp28:
	jmp	.LBB5_21
.LBB5_12:
.Ltmp29:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_7:
.Ltmp8:
	movq	%rax, %r14
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_14
# BB#8:
	movq	(%rdi), %rax
.Ltmp9:
	callq	*16(%rax)
.Ltmp10:
.LBB5_14:                               # %.body
	leaq	48(%rbx), %rdi
.Ltmp15:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp16:
# BB#15:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_21
# BB#16:
	movq	(%rdi), %rax
.Ltmp21:
	callq	*16(%rax)
.Ltmp22:
.LBB5_21:                               # %_ZN10COutBufferD2Ev.exit14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_9:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB5_22:
.Ltmp23:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB5_17:
.Ltmp17:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_23
# BB#18:
	movq	(%rdi), %rax
.Ltmp18:
	callq	*16(%rax)
.Ltmp19:
.LBB5_23:                               # %.body12
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB5_19:
.Ltmp20:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev, .Lfunc_end5-_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp6-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin2    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin2   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp27-.Ltmp31         #   Call between .Ltmp31 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin2   #     jumps to .Ltmp29
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 7 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin2   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp18-.Ltmp22         #   Call between .Ltmp22 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,"axG",@progbits,_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,comdat
	.weak	_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,@function
_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev: # @_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp33:
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp34:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB6_2:
.Ltmp35:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev, .Lfunc_end6-_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin3   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp34     #   Call between .Ltmp34 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end7:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end7-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end8:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end8-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB9_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB9_2:                                # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end9:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end9-_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,@function
_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev: # @_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end10:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev, .Lfunc_end10-_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,"axG",@progbits,_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,comdat
	.weak	_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,@function
_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev: # @_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp36:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp37:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB11_2:
.Ltmp38:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev, .Lfunc_end11-_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp36-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin4   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end11-.Ltmp37    #   Call between .Ltmp37 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end12:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end12-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end13:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end13-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB14_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB14_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end14-_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,@function
_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev: # @_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end15:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev, .Lfunc_end15-_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,"axG",@progbits,_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,comdat
	.weak	_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,@function
_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev: # @_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp39:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp40:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB16_2:
.Ltmp41:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev, .Lfunc_end16-_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp39-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin5   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end16-.Ltmp40    #   Call between .Ltmp40 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end17:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end17-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end18:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end18-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB19_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB19_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end19-_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,@function
_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev: # @_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end20:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev, .Lfunc_end20-_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.cfi_endproc

	.section	.text._ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,"axG",@progbits,_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,comdat
	.weak	_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,@function
_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev: # @_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp42:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp43:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB21_2:
.Ltmp44:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev, .Lfunc_end21-_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp42-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin6   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Lfunc_end21-.Ltmp43    #   Call between .Ltmp43 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end22:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end22-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end23:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv, .Lfunc_end23-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv,@function
_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv: # @_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB24_2
# BB#1:
	addq	$-32, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB24_2:                               # %_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv, .Lfunc_end24-_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev,@function
_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev: # @_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.cfi_startproc
# BB#0:
	addq	$-32, %rdi
	jmp	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev # TAILCALL
.Lfunc_end25:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev, .Lfunc_end25-_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.cfi_endproc

	.section	.text._ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,"axG",@progbits,_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,comdat
	.weak	_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.p2align	4, 0x90
	.type	_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev,@function
_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev: # @_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 32
.Lcfi44:
	.cfi_offset %rbx, -24
.Lcfi45:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-32, %rbx
.Ltmp45:
	movq	%rbx, %rdi
	callq	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
.Ltmp46:
# BB#1:                                 # %_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_2:
.Ltmp47:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev, .Lfunc_end26-_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp45-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin7   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp46    #   Call between .Ltmp46 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end27:
	.size	__clang_call_terminate, .Lfunc_end27-__clang_call_terminate

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end28:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end28-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN14ICompressCoderD0Ev,"axG",@progbits,_ZN14ICompressCoderD0Ev,comdat
	.weak	_ZN14ICompressCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN14ICompressCoderD0Ev,@function
_ZN14ICompressCoderD0Ev:                # @_ZN14ICompressCoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end29:
	.size	_ZN14ICompressCoderD0Ev, .Lfunc_end29-_ZN14ICompressCoderD0Ev
	.cfi_endproc

	.section	.text._ZN27ICompressSetCoderPropertiesD0Ev,"axG",@progbits,_ZN27ICompressSetCoderPropertiesD0Ev,comdat
	.weak	_ZN27ICompressSetCoderPropertiesD0Ev
	.p2align	4, 0x90
	.type	_ZN27ICompressSetCoderPropertiesD0Ev,@function
_ZN27ICompressSetCoderPropertiesD0Ev:   # @_ZN27ICompressSetCoderPropertiesD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end30:
	.size	_ZN27ICompressSetCoderPropertiesD0Ev, .Lfunc_end30-_ZN27ICompressSetCoderPropertiesD0Ev
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_Deflate64Register.ii,@function
_GLOBAL__sub_I_Deflate64Register.ii:    # @_GLOBAL__sub_I_Deflate64Register.ii
	.cfi_startproc
# BB#0:
	movl	$_ZL11g_CodecInfo, %edi
	jmp	_Z13RegisterCodecPK10CCodecInfo # TAILCALL
.Lfunc_end31:
	.size	_GLOBAL__sub_I_Deflate64Register.ii, .Lfunc_end31-_GLOBAL__sub_I_Deflate64Register.ii
	.cfi_endproc

	.type	_ZL11g_CodecInfo,@object # @_ZL11g_CodecInfo
	.data
	.p2align	3
_ZL11g_CodecInfo:
	.quad	_ZL20CreateCodecDeflate64v
	.quad	_ZL23CreateCodecOutDeflate64v
	.quad	262409                  # 0x40109
	.quad	.L.str
	.long	1                       # 0x1
	.byte	0                       # 0x0
	.zero	3
	.size	_ZL11g_CodecInfo, 40

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	68                      # 0x44
	.long	101                     # 0x65
	.long	102                     # 0x66
	.long	108                     # 0x6c
	.long	97                      # 0x61
	.long	116                     # 0x74
	.long	101                     # 0x65
	.long	54                      # 0x36
	.long	52                      # 0x34
	.long	0                       # 0x0
	.size	.L.str, 40

	.type	_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E,@object # @_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.section	.rodata._ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E,"aG",@progbits,_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E,comdat
	.weak	_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.p2align	3
_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E:
	.quad	0
	.quad	_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoderD2Ev
	.quad	_ZN9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS8_P21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	.quad	_ZN9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.quad	-8
	.quad	_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.quad	_ZThn8_N9NCompress8NDeflate8NDecoder6CCoder24GetInStreamProcessedSizeEPy
	.quad	-16
	.quad	_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder11SetInStreamEP19ISequentialInStream
	.quad	_ZThn16_N9NCompress8NDeflate8NDecoder6CCoder15ReleaseInStreamEv
	.quad	-24
	.quad	_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.quad	_ZThn24_N9NCompress8NDeflate8NDecoder6CCoder16SetOutStreamSizeEPKy
	.quad	-32
	.quad	_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder6AddRefEv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder7ReleaseEv
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D1Ev
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder11CCOMCoder64D0Ev
	.quad	_ZThn32_N9NCompress8NDeflate8NDecoder6CCoder4ReadEPvjPj
	.size	_ZTVN9NCompress8NDeflate8NDecoder11CCOMCoder64E, 376

	.type	_ZTSN9NCompress8NDeflate8NDecoder11CCOMCoder64E,@object # @_ZTSN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.section	.rodata._ZTSN9NCompress8NDeflate8NDecoder11CCOMCoder64E,"aG",@progbits,_ZTSN9NCompress8NDeflate8NDecoder11CCOMCoder64E,comdat
	.weak	_ZTSN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.p2align	4
_ZTSN9NCompress8NDeflate8NDecoder11CCOMCoder64E:
	.asciz	"N9NCompress8NDeflate8NDecoder11CCOMCoder64E"
	.size	_ZTSN9NCompress8NDeflate8NDecoder11CCOMCoder64E, 44

	.type	_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E,@object # @_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.section	.rodata._ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E,"aG",@progbits,_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E,comdat
	.weak	_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.p2align	4
_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN9NCompress8NDeflate8NDecoder11CCOMCoder64E
	.quad	_ZTIN9NCompress8NDeflate8NDecoder6CCoderE
	.size	_ZTIN9NCompress8NDeflate8NDecoder11CCOMCoder64E, 24

	.type	_ZTV14ICompressCoder,@object # @_ZTV14ICompressCoder
	.section	.rodata._ZTV14ICompressCoder,"aG",@progbits,_ZTV14ICompressCoder,comdat
	.weak	_ZTV14ICompressCoder
	.p2align	3
_ZTV14ICompressCoder:
	.quad	0
	.quad	_ZTI14ICompressCoder
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN14ICompressCoderD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV14ICompressCoder, 64

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTV27ICompressSetCoderProperties,@object # @_ZTV27ICompressSetCoderProperties
	.section	.rodata._ZTV27ICompressSetCoderProperties,"aG",@progbits,_ZTV27ICompressSetCoderProperties,comdat
	.weak	_ZTV27ICompressSetCoderProperties
	.p2align	3
_ZTV27ICompressSetCoderProperties:
	.quad	0
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN27ICompressSetCoderPropertiesD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV27ICompressSetCoderProperties, 64

	.type	_ZTS27ICompressSetCoderProperties,@object # @_ZTS27ICompressSetCoderProperties
	.section	.rodata._ZTS27ICompressSetCoderProperties,"aG",@progbits,_ZTS27ICompressSetCoderProperties,comdat
	.weak	_ZTS27ICompressSetCoderProperties
	.p2align	4
_ZTS27ICompressSetCoderProperties:
	.asciz	"27ICompressSetCoderProperties"
	.size	_ZTS27ICompressSetCoderProperties, 30

	.type	_ZTI27ICompressSetCoderProperties,@object # @_ZTI27ICompressSetCoderProperties
	.section	.rodata._ZTI27ICompressSetCoderProperties,"aG",@progbits,_ZTI27ICompressSetCoderProperties,comdat
	.weak	_ZTI27ICompressSetCoderProperties
	.p2align	4
_ZTI27ICompressSetCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27ICompressSetCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI27ICompressSetCoderProperties, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_Deflate64Register.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
