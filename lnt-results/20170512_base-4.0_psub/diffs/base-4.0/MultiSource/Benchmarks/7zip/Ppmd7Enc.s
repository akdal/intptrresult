	.text
	.file	"Ppmd7Enc.bc"
	.globl	Ppmd7z_RangeEnc_Init
	.p2align	4, 0x90
	.type	Ppmd7z_RangeEnc_Init,@function
Ppmd7z_RangeEnc_Init:                   # @Ppmd7z_RangeEnc_Init
	.cfi_startproc
# BB#0:
	movq	$0, (%rdi)
	movl	$-1, 8(%rdi)
	movb	$0, 12(%rdi)
	movq	$1, 16(%rdi)
	retq
.Lfunc_end0:
	.size	Ppmd7z_RangeEnc_Init, .Lfunc_end0-Ppmd7z_RangeEnc_Init
	.cfi_endproc

	.globl	Ppmd7z_RangeEnc_FlushData
	.p2align	4, 0x90
	.type	Ppmd7z_RangeEnc_FlushData,@function
Ppmd7z_RangeEnc_FlushData:              # @Ppmd7z_RangeEnc_FlushData
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB1_4
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB1_4
# BB#3:                                 # %._crit_edge12.i
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rbx), %rcx
	incq	%rcx
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_4:                                #   in Loop: Header=BB1_1 Depth=1
	movb	12(%rbx), %cl
	.p2align	4, 0x90
.LBB1_5:                                # %._crit_edge.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rdi
	shrq	$32, %rax
	addb	%cl, %al
	movzbl	%al, %esi
	callq	*(%rdi)
	decq	16(%rbx)
	movq	(%rbx), %rax
	movb	$-1, %cl
	jne	.LBB1_5
# BB#6:                                 #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 12(%rbx)
	movl	$1, %ecx
.LBB1_7:                                # %RangeEnc_ShiftLow.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%rcx, 16(%rbx)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, (%rbx)
	incl	%ebp
	cmpl	$5, %ebp
	jne	.LBB1_1
# BB#8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	Ppmd7z_RangeEnc_FlushData, .Lfunc_end1-Ppmd7z_RangeEnc_FlushData
	.cfi_endproc

	.globl	Ppmd7_EncodeSymbol
	.p2align	4, 0x90
	.type	Ppmd7_EncodeSymbol,@function
Ppmd7_EncodeSymbol:                     # @Ppmd7_EncodeSymbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi11:
	.cfi_def_cfa_offset 352
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %r9
	movzwl	(%r9), %r8d
	cmpl	$1, %r8d
	jne	.LBB2_1
# BB#37:
	leaq	2(%r9), %rbp
	movzbl	3(%r9), %edx
	shlq	$7, %rdx
	addq	%r14, %rdx
	movq	16(%r14), %rax
	movq	64(%r14), %rcx
	movl	8(%r9), %esi
	movzwl	(%rcx,%rsi), %ecx
	movzbl	683(%r14,%rcx), %esi
	addl	32(%r14), %esi
	movzbl	(%rax), %eax
	movzbl	940(%r14,%rax), %eax
	movl	%eax, 40(%r14)
	addl	%eax, %esi
	movzbl	2(%r9), %ecx
	movzbl	940(%r14,%rcx), %eax
	leal	(%rsi,%rax,2), %eax
	movl	44(%r14), %esi
	shrl	$26, %esi
	andl	$32, %esi
	addl	%eax, %esi
	leaq	2672(%rdx,%rsi,2), %r13
	movzwl	2672(%rdx,%rsi,2), %edi
	leaq	8(%rbx), %r12
	movl	8(%rbx), %edx
	movl	%edx, %esi
	shrl	$14, %esi
	imull	%edi, %esi
	cmpl	%r15d, %ecx
	jne	.LBB2_49
# BB#38:                                # %.critedge187
	movl	%esi, (%r12)
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB2_48
# BB#39:                                # %.lr.ph.i210
	movq	(%rbx), %rax
	.p2align	4, 0x90
.LBB2_40:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_44 Depth 2
	shll	$8, %esi
	movl	%esi, (%r12)
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB2_43
# BB#41:                                #   in Loop: Header=BB2_40 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB2_43
# BB#42:                                # %._crit_edge12.i.i214
                                        #   in Loop: Header=BB2_40 Depth=1
	movq	16(%rbx), %rcx
	incq	%rcx
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_43:                               #   in Loop: Header=BB2_40 Depth=1
	movb	12(%rbx), %cl
	.p2align	4, 0x90
.LBB2_44:                               # %._crit_edge.i.i216
                                        #   Parent Loop BB2_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rdi
	shrq	$32, %rax
	addb	%cl, %al
	movzbl	%al, %esi
	callq	*(%rdi)
	decq	16(%rbx)
	movq	(%rbx), %rax
	movb	$-1, %cl
	jne	.LBB2_44
# BB#45:                                #   in Loop: Header=BB2_40 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 12(%rbx)
	movl	8(%rbx), %esi
	movl	$1, %ecx
.LBB2_46:                               # %RangeEnc_ShiftLow.exit.i219
                                        #   in Loop: Header=BB2_40 Depth=1
	movq	%rcx, 16(%rbx)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, (%rbx)
	cmpl	$16777216, %esi         # imm = 0x1000000
	jb	.LBB2_40
# BB#47:                                # %RangeEnc_EncodeBit_0.exit.loopexit
	movw	(%r13), %di
.LBB2_48:                               # %RangeEnc_EncodeBit_0.exit
	movzwl	%di, %eax
	leal	32(%rax), %ecx
	shrl	$7, %ecx
	negl	%ecx
	leal	128(%rax,%rcx), %eax
	movw	%ax, (%r13)
	movq	%rbp, 16(%r14)
	movq	%r14, %rdi
	callq	Ppmd7_UpdateBin
	jmp	.LBB2_96
.LBB2_1:
	movq	64(%r14), %rbp
	movl	4(%r9), %edi
	movzbl	(%rbp,%rdi), %ecx
	cmpl	%r15d, %ecx
	jne	.LBB2_11
# BB#2:                                 # %.critedge
	addq	%rdi, %rbp
	movzbl	1(%rbp), %esi
	movzwl	2(%r9), %ecx
	movl	8(%rbx), %eax
	xorl	%edx, %edx
	divl	%ecx
	movq	(%rbx), %rcx
	imull	%esi, %eax
	movl	%eax, 8(%rbx)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB2_10
	.p2align	4, 0x90
.LBB2_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_7 Depth 2
	shll	$8, %eax
	movl	%eax, 8(%rbx)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	jne	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB2_6
# BB#5:                                 # %._crit_edge12.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	16(%rbx), %rdx
	incq	%rdx
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_3 Depth=1
	movb	12(%rbx), %al
	.p2align	4, 0x90
.LBB2_7:                                # %._crit_edge.i.i
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rdi
	shrq	$32, %rcx
	addb	%al, %cl
	movzbl	%cl, %esi
	callq	*(%rdi)
	decq	16(%rbx)
	movq	(%rbx), %rcx
	movb	$-1, %al
	jne	.LBB2_7
# BB#8:                                 #   in Loop: Header=BB2_3 Depth=1
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 12(%rbx)
	movl	8(%rbx), %eax
	movl	$1, %edx
.LBB2_9:                                # %RangeEnc_ShiftLow.exit.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rdx, 16(%rbx)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, (%rbx)
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB2_3
.LBB2_10:                               # %RangeEnc_Encode.exit
	movq	%rbp, 16(%r14)
	movq	%r14, %rdi
	callq	Ppmd7_Update1_0
	jmp	.LBB2_96
.LBB2_49:
	movl	%esi, %eax
	addq	(%rbx), %rax
	movq	%rax, (%rbx)
	subl	%esi, %edx
	movl	%edx, 8(%rbx)
	cmpl	$16777215, %edx         # imm = 0xFFFFFF
	ja	.LBB2_58
	.p2align	4, 0x90
.LBB2_50:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_54 Depth 2
	shll	$8, %edx
	movl	%edx, (%r12)
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB2_53
# BB#51:                                #   in Loop: Header=BB2_50 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB2_53
# BB#52:                                # %._crit_edge12.i.i224
                                        #   in Loop: Header=BB2_50 Depth=1
	movq	16(%rbx), %rcx
	incq	%rcx
	jmp	.LBB2_56
	.p2align	4, 0x90
.LBB2_53:                               #   in Loop: Header=BB2_50 Depth=1
	movb	12(%rbx), %cl
	.p2align	4, 0x90
.LBB2_54:                               # %._crit_edge.i.i226
                                        #   Parent Loop BB2_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rdi
	shrq	$32, %rax
	addb	%cl, %al
	movzbl	%al, %esi
	callq	*(%rdi)
	decq	16(%rbx)
	movq	(%rbx), %rax
	movb	$-1, %cl
	jne	.LBB2_54
# BB#55:                                #   in Loop: Header=BB2_50 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 12(%rbx)
	movl	8(%rbx), %edx
	movl	$1, %ecx
.LBB2_56:                               # %RangeEnc_ShiftLow.exit.i229
                                        #   in Loop: Header=BB2_50 Depth=1
	movq	%rcx, 16(%rbx)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, (%rbx)
	cmpl	$16777216, %edx         # imm = 0x1000000
	jb	.LBB2_50
# BB#57:                                # %RangeEnc_EncodeBit_1.exit.loopexit
	movw	(%r13), %di
	movb	(%rbp), %cl
.LBB2_58:                               # %RangeEnc_EncodeBit_1.exit
	leaq	64(%r14), %rbp
	movzwl	%di, %eax
	leal	32(%rax), %edx
	shrl	$7, %edx
	subl	%edx, %eax
	movw	%ax, (%r13)
	shrl	$10, %eax
	andl	$63, %eax
	movzbl	PPMD7_kExpEscape(%rax), %eax
	movl	%eax, 28(%r14)
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movdqa	%xmm0, 48(%rsp)
	movdqa	%xmm0, 32(%rsp)
	movzbl	%cl, %eax
	movb	$0, 32(%rsp,%rax)
	movl	$0, 32(%r14)
	jmp	.LBB2_59
.LBB2_11:
	leaq	64(%r14), %r10
	movl	$0, 32(%r14)
	movzbl	1(%rbp,%rdi), %ecx
	leal	-1(%r8), %edx
	movl	$1, %esi
	subl	%r8d, %esi
	leaq	6(%rbp,%rdi), %rbp
	.p2align	4, 0x90
.LBB2_12:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %eax
	movzbl	1(%rbp), %edi
	cmpl	%r15d, %eax
	je	.LBB2_13
# BB#22:                                #   in Loop: Header=BB2_12 Depth=1
	addl	%edi, %ecx
	addq	$6, %rbp
	incl	%esi
	jne	.LBB2_12
# BB#23:
	movq	16(%r14), %rax
	movzbl	(%rax), %eax
	movzbl	940(%r14,%rax), %eax
	movl	%eax, 40(%r14)
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movdqa	%xmm0, 48(%rsp)
	movdqa	%xmm0, 32(%rsp)
	movzbl	-6(%rbp), %eax
	addq	$-6, %rbp
	movb	$0, 32(%rsp,%rax)
	addl	$-2, %r8d
	movl	%edx, %edi
	andl	$3, %edi
	je	.LBB2_26
# BB#24:                                # %.prol.preheader
	negl	%edi
	.p2align	4, 0x90
.LBB2_25:                               # =>This Inner Loop Header: Depth=1
	movzbl	-6(%rbp), %eax
	addq	$-6, %rbp
	movb	$0, 32(%rsp,%rax)
	decl	%edx
	incl	%edi
	jne	.LBB2_25
.LBB2_26:                               # %.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB2_29
# BB#27:                                # %.new
	addq	$-6, %rbp
	.p2align	4, 0x90
.LBB2_28:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %eax
	movb	$0, 32(%rsp,%rax)
	movzbl	-6(%rbp), %eax
	movb	$0, 32(%rsp,%rax)
	movzbl	-12(%rbp), %eax
	movb	$0, 32(%rsp,%rax)
	movzbl	-18(%rbp), %eax
	movb	$0, 32(%rsp,%rax)
	addq	$-24, %rbp
	addl	$-4, %edx
	jne	.LBB2_28
.LBB2_29:
	movzwl	2(%r9), %esi
	movl	8(%rbx), %eax
	xorl	%edx, %edx
	divl	%esi
	movl	%esi, %edx
	subl	%ecx, %edx
	leaq	8(%rbx), %r12
	imull	%eax, %ecx
	addq	(%rbx), %rcx
	movq	%rcx, (%rbx)
	imull	%eax, %edx
	movl	%edx, 8(%rbx)
	movq	%r10, %rbp
	cmpl	$16777215, %edx         # imm = 0xFFFFFF
	ja	.LBB2_59
	.p2align	4, 0x90
.LBB2_30:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_34 Depth 2
	shll	$8, %edx
	movl	%edx, (%r12)
	movq	%rcx, %rax
	shrq	$32, %rax
	jne	.LBB2_33
# BB#31:                                #   in Loop: Header=BB2_30 Depth=1
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB2_33
# BB#32:                                # %._crit_edge12.i.i203
                                        #   in Loop: Header=BB2_30 Depth=1
	movq	16(%rbx), %rax
	incq	%rax
	jmp	.LBB2_36
	.p2align	4, 0x90
.LBB2_33:                               #   in Loop: Header=BB2_30 Depth=1
	movb	12(%rbx), %al
	.p2align	4, 0x90
.LBB2_34:                               # %._crit_edge.i.i205
                                        #   Parent Loop BB2_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rdi
	shrq	$32, %rcx
	addb	%al, %cl
	movzbl	%cl, %esi
	callq	*(%rdi)
	decq	16(%rbx)
	movq	(%rbx), %rcx
	movb	$-1, %al
	jne	.LBB2_34
# BB#35:                                #   in Loop: Header=BB2_30 Depth=1
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 12(%rbx)
	movl	8(%rbx), %edx
	movl	$1, %eax
.LBB2_36:                               # %RangeEnc_ShiftLow.exit.i208
                                        #   in Loop: Header=BB2_30 Depth=1
	movq	%rax, 16(%rbx)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, (%rbx)
	cmpl	$16777216, %edx         # imm = 0x1000000
	jb	.LBB2_30
	jmp	.LBB2_59
.LBB2_13:                               # %.critedge186
	movzwl	2(%r9), %esi
	movl	8(%rbx), %eax
	xorl	%edx, %edx
	divl	%esi
	imull	%eax, %ecx
	addq	(%rbx), %rcx
	movq	%rcx, (%rbx)
	imull	%eax, %edi
	movl	%edi, 8(%rbx)
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB2_21
	.p2align	4, 0x90
.LBB2_14:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_18 Depth 2
	shll	$8, %edi
	movl	%edi, 8(%rbx)
	movq	%rcx, %rax
	shrq	$32, %rax
	jne	.LBB2_17
# BB#15:                                #   in Loop: Header=BB2_14 Depth=1
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB2_17
# BB#16:                                # %._crit_edge12.i.i192
                                        #   in Loop: Header=BB2_14 Depth=1
	movq	16(%rbx), %rax
	incq	%rax
	jmp	.LBB2_20
	.p2align	4, 0x90
.LBB2_17:                               #   in Loop: Header=BB2_14 Depth=1
	movb	12(%rbx), %al
	.p2align	4, 0x90
.LBB2_18:                               # %._crit_edge.i.i194
                                        #   Parent Loop BB2_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rdi
	shrq	$32, %rcx
	addb	%al, %cl
	movzbl	%cl, %esi
	callq	*(%rdi)
	decq	16(%rbx)
	movq	(%rbx), %rcx
	movb	$-1, %al
	jne	.LBB2_18
# BB#19:                                #   in Loop: Header=BB2_14 Depth=1
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 12(%rbx)
	movl	8(%rbx), %edi
	movl	$1, %eax
.LBB2_20:                               # %RangeEnc_ShiftLow.exit.i197
                                        #   in Loop: Header=BB2_14 Depth=1
	movq	%rax, 16(%rbx)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, (%rbx)
	cmpl	$16777216, %edi         # imm = 0x1000000
	jb	.LBB2_14
.LBB2_21:                               # %RangeEnc_Encode.exit198
	movq	%rbp, 16(%r14)
	movq	%r14, %rdi
	callq	Ppmd7_Update1
	jmp	.LBB2_96
.LBB2_93:                               # %.loopexit.loopexit
                                        #   in Loop: Header=BB2_59 Depth=1
	movl	12(%rsp), %esi
.LBB2_94:                               # %.loopexit
                                        #   in Loop: Header=BB2_59 Depth=1
	movzwl	(%r15), %eax
	addl	%r13d, %eax
	addl	%esi, %eax
	movw	%ax, (%r15)
	movl	%r14d, %r15d
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB2_59:                               # %RangeEnc_Encode.exit209
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_60 Depth 2
                                        #     Child Loop BB2_63 Depth 2
                                        #     Child Loop BB2_86 Depth 2
                                        #       Child Loop BB2_90 Depth 3
	movq	(%r14), %rcx
	movzwl	(%rcx), %esi
	movl	24(%r14), %eax
	.p2align	4, 0x90
.LBB2_60:                               #   Parent Loop BB2_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rcx), %edx
	incl	%eax
	testq	%rdx, %rdx
	je	.LBB2_95
# BB#61:                                #   in Loop: Header=BB2_60 Depth=2
	movq	(%rbp), %rdi
	leaq	(%rdi,%rdx), %rcx
	movq	%rcx, (%r14)
	cmpw	%si, (%rdi,%rdx)
	je	.LBB2_60
# BB#62:                                #   in Loop: Header=BB2_59 Depth=1
	movq	%rbp, %r13
	movq	%r14, %rbp
	movl	%r15d, %r14d
	movl	%eax, 24(%rbp)
	movq	%rbp, %rdi
	leaq	12(%rsp), %rdx
	callq	Ppmd7_MakeEscFreq
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	(%rbp), %rcx
	movl	4(%rcx), %r15d
	movq	%r13, %rdi
	addq	(%r13), %r15
	movzwl	(%rcx), %ebp
	addl	$-2, %ebp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_63:                               #   Parent Loop BB2_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r15), %esi
	movzbl	32(%rsp,%rsi), %edx
	andb	1(%r15), %dl
	movzbl	%dl, %r13d
	addl	%ecx, %r13d
	cmpl	%r14d, %esi
	je	.LBB2_64
# BB#84:                                #   in Loop: Header=BB2_63 Depth=2
	movb	$0, 32(%rsp,%rsi)
	decl	%ebp
	addq	$6, %r15
	cmpl	$-2, %ebp
	movl	%r13d, %ecx
	jne	.LBB2_63
# BB#85:                                #   in Loop: Header=BB2_59 Depth=1
	movq	%rax, %r15
	movl	12(%rsp), %esi
	leal	(%rsi,%r13), %ecx
	movl	(%r12), %eax
	xorl	%edx, %edx
	divl	%ecx
	movl	%eax, %ecx
	imull	%r13d, %ecx
	addq	(%rbx), %rcx
	movq	%rcx, (%rbx)
	imull	%esi, %eax
	movl	%eax, (%r12)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	movq	%rdi, %rbp
	ja	.LBB2_94
	.p2align	4, 0x90
.LBB2_86:                               # %.lr.ph.i241
                                        #   Parent Loop BB2_59 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_90 Depth 3
	shll	$8, %eax
	movl	%eax, (%r12)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	jne	.LBB2_89
# BB#87:                                # %.lr.ph.i241
                                        #   in Loop: Header=BB2_86 Depth=2
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB2_89
# BB#88:                                # %._crit_edge12.i.i245
                                        #   in Loop: Header=BB2_86 Depth=2
	movq	16(%rbx), %rdx
	incq	%rdx
	jmp	.LBB2_92
	.p2align	4, 0x90
.LBB2_89:                               #   in Loop: Header=BB2_86 Depth=2
	movb	12(%rbx), %al
	.p2align	4, 0x90
.LBB2_90:                               # %._crit_edge.i.i247
                                        #   Parent Loop BB2_59 Depth=1
                                        #     Parent Loop BB2_86 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	24(%rbx), %rdi
	shrq	$32, %rcx
	addb	%al, %cl
	movzbl	%cl, %esi
	callq	*(%rdi)
	decq	16(%rbx)
	movq	(%rbx), %rcx
	movb	$-1, %al
	jne	.LBB2_90
# BB#91:                                #   in Loop: Header=BB2_86 Depth=2
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 12(%rbx)
	movl	(%r12), %eax
	movl	$1, %edx
.LBB2_92:                               # %RangeEnc_ShiftLow.exit.i250
                                        #   in Loop: Header=BB2_86 Depth=2
	movq	%rdx, 16(%rbx)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, (%rbx)
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB2_86
	jmp	.LBB2_93
.LBB2_95:                               # %.thread.loopexit
	movl	%eax, 24(%r14)
	jmp	.LBB2_96
.LBB2_64:                               # %.preheader.preheader
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpl	$-1, %ebp
	je	.LBB2_65
# BB#66:                                # %._crit_edge.preheader
	leal	1(%rbp), %esi
	testb	$1, %sil
	movq	16(%rsp), %r14          # 8-byte Reload
	jne	.LBB2_68
# BB#67:
                                        # implicit-def: %EDI
	movq	%r15, %rdx
	testl	%ebp, %ebp
	jne	.LBB2_70
	jmp	.LBB2_72
.LBB2_65:
	movl	%r13d, %edi
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB2_72
.LBB2_68:                               # %._crit_edge.prol
	leaq	6(%r15), %rdx
	movzbl	6(%r15), %eax
	movb	32(%rsp,%rax), %al
	andb	7(%r15), %al
	movzbl	%al, %edi
	addl	%r13d, %edi
	movl	%ebp, %esi
	movl	%edi, %r13d
	testl	%ebp, %ebp
	je	.LBB2_72
.LBB2_70:                               # %._crit_edge.preheader.new
	addq	$13, %rdx
	movl	%r13d, %edi
	.p2align	4, 0x90
.LBB2_71:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rdx), %eax
	movzbl	32(%rsp,%rax), %eax
	andb	-6(%rdx), %al
	movzbl	%al, %ebp
	addl	%edi, %ebp
	movzbl	-1(%rdx), %edi
	movzbl	32(%rsp,%rdi), %eax
	andb	(%rdx), %al
	movzbl	%al, %edi
	addl	%ebp, %edi
	addq	$12, %rdx
	addl	$-2, %esi
	jne	.LBB2_71
.LBB2_72:                               # %.preheader._crit_edge
	movzbl	1(%r15), %esi
	addl	12(%rsp), %edi
	movl	(%r12), %eax
	xorl	%edx, %edx
	divl	%edi
	imull	%eax, %ecx
	addq	(%rbx), %rcx
	movq	%rcx, (%rbx)
	imull	%eax, %esi
	movl	%esi, (%r12)
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB2_80
	.p2align	4, 0x90
.LBB2_73:                               # %.lr.ph.i230
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_77 Depth 2
	shll	$8, %esi
	movl	%esi, (%r12)
	movq	%rcx, %rax
	shrq	$32, %rax
	jne	.LBB2_76
# BB#74:                                # %.lr.ph.i230
                                        #   in Loop: Header=BB2_73 Depth=1
	cmpl	$-16777216, %ecx        # imm = 0xFF000000
	jb	.LBB2_76
# BB#75:                                # %._crit_edge12.i.i234
                                        #   in Loop: Header=BB2_73 Depth=1
	movq	16(%rbx), %rax
	incq	%rax
	jmp	.LBB2_79
	.p2align	4, 0x90
.LBB2_76:                               #   in Loop: Header=BB2_73 Depth=1
	movb	12(%rbx), %al
	.p2align	4, 0x90
.LBB2_77:                               # %._crit_edge.i.i236
                                        #   Parent Loop BB2_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rbx), %rdi
	shrq	$32, %rcx
	addb	%al, %cl
	movzbl	%cl, %esi
	callq	*(%rdi)
	decq	16(%rbx)
	movq	(%rbx), %rcx
	movb	$-1, %al
	jne	.LBB2_77
# BB#78:                                #   in Loop: Header=BB2_73 Depth=1
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 12(%rbx)
	movl	(%r12), %esi
	movl	$1, %eax
.LBB2_79:                               # %RangeEnc_ShiftLow.exit.i239
                                        #   in Loop: Header=BB2_73 Depth=1
	movq	%rax, 16(%rbx)
	andq	$16777215, %rcx         # imm = 0xFFFFFF
	shlq	$8, %rcx
	movq	%rcx, (%rbx)
	cmpl	$16777216, %esi         # imm = 0x1000000
	jb	.LBB2_73
.LBB2_80:                               # %RangeEnc_Encode.exit240
	movq	24(%rsp), %rdx          # 8-byte Reload
	movzbl	2(%rdx), %ecx
	cmpl	$6, %ecx
	ja	.LBB2_83
# BB#81:
	decb	3(%rdx)
	jne	.LBB2_83
# BB#82:
	shlw	(%rdx)
	movl	%ecx, %eax
	incb	%al
	movb	%al, 2(%rdx)
	movl	$3, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 3(%rdx)
.LBB2_83:
	movq	%r15, 16(%r14)
	movq	%r14, %rdi
	callq	Ppmd7_Update2
.LBB2_96:
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	Ppmd7_EncodeSymbol, .Lfunc_end2-Ppmd7_EncodeSymbol
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
