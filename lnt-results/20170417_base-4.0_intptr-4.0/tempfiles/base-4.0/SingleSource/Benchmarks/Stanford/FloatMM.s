	.text
	.file	"FloatMM.bc"
	.globl	Initrand
	.p2align	4, 0x90
	.type	Initrand,@function
Initrand:                               # @Initrand
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	retq
.Lfunc_end0:
	.size	Initrand, .Lfunc_end0-Initrand
	.cfi_endproc

	.globl	Rand
	.p2align	4, 0x90
	.type	Rand,@function
Rand:                                   # @Rand
	.cfi_startproc
# BB#0:
	imull	$1309, seed(%rip), %eax # imm = 0x51D
	addl	$13849, %eax            # imm = 0x3619
	movzwl	%ax, %eax
	movq	%rax, seed(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	Rand, .Lfunc_end1-Rand
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1077936128              # float 3
	.text
	.globl	rInitmatrix
	.p2align	4, 0x90
	.type	rInitmatrix,@function
rInitmatrix:                            # @rInitmatrix
	.cfi_startproc
# BB#0:
	movq	seed(%rip), %r10
	addq	$168, %rdi
	movl	$1, %r8d
	movl	$2290649225, %r9d       # imm = 0x88888889
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB2_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
	movl	$40, %esi
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB2_2:                                #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imull	$1309, %r10d, %ecx      # imm = 0x51D
	addl	$13849, %ecx            # imm = 0x3619
	movzwl	%cx, %r10d
	movq	%r10, %rcx
	imulq	%r9, %rcx
	shrq	$38, %rcx
	imull	$120, %ecx, %ecx
	movl	%r10d, %edx
	subl	%ecx, %edx
	addl	$-60, %edx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edx, %xmm1
	divss	%xmm0, %xmm1
	movss	%xmm1, (%rax)
	addq	$4, %rax
	decq	%rsi
	jne	.LBB2_2
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	incq	%r8
	addq	$164, %rdi
	cmpq	$41, %r8
	jne	.LBB2_1
# BB#4:
	movq	%r10, seed(%rip)
	retq
.Lfunc_end2:
	.size	rInitmatrix, .Lfunc_end2-rInitmatrix
	.cfi_endproc

	.globl	rInnerproduct
	.p2align	4, 0x90
	.type	rInnerproduct,@function
rInnerproduct:                          # @rInnerproduct
	.cfi_startproc
# BB#0:
	movl	$0, (%rdi)
	movslq	%ecx, %rax
	movslq	%r8d, %rcx
	imulq	$164, %rax, %rax
	addq	%rsi, %rax
	leaq	164(%rdx,%rcx,4), %rcx
	xorps	%xmm0, %xmm0
	movl	$2, %edx
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movss	-4(%rax,%rdx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rdi)
	movss	(%rax,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	mulss	164(%rcx), %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	addq	$2, %rdx
	addq	$328, %rcx              # imm = 0x148
	cmpq	$42, %rdx
	jne	.LBB3_1
# BB#2:
	retq
.Lfunc_end3:
	.size	rInnerproduct, .Lfunc_end3-rInnerproduct
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1077936128              # float 3
	.text
	.globl	Mm
	.p2align	4, 0x90
	.type	Mm,@function
Mm:                                     # @Mm
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	$74755, seed(%rip)      # imm = 0x12403
	movl	$74755, %ebx            # imm = 0x12403
	movl	$1, %r8d
	movl	$rma+168, %r9d
	movl	$2290649225, %r10d      # imm = 0x88888889
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB4_1:                                # %.preheader.i20
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_2 Depth 2
	movl	$40, %ecx
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB4_2:                                #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imull	$1309, %ebx, %eax       # imm = 0x51D
	addl	$13849, %eax            # imm = 0x3619
	movzwl	%ax, %ebx
	movq	%rbx, %rax
	imulq	%r10, %rax
	shrq	$38, %rax
	imull	$120, %eax, %r11d
	movl	%ebx, %eax
	subl	%r11d, %eax
	addl	$-60, %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	divss	%xmm0, %xmm1
	movss	%xmm1, (%rdx)
	addq	$4, %rdx
	decq	%rcx
	jne	.LBB4_2
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	incq	%r8
	addq	$164, %r9
	cmpq	$41, %r8
	jne	.LBB4_1
# BB#4:                                 # %rInitmatrix.exit26
	movq	%rbx, seed(%rip)
	movl	$1, %r8d
	movl	$rmb+168, %r9d
	movl	$2290649225, %r10d      # imm = 0x88888889
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_6 Depth 2
	movl	$40, %ecx
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB4_6:                                #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imull	$1309, %ebx, %esi       # imm = 0x51D
	addl	$13849, %esi            # imm = 0x3619
	movzwl	%si, %ebx
	movq	%rbx, %rsi
	imulq	%r10, %rsi
	shrq	$38, %rsi
	imull	$120, %esi, %esi
	movl	%ebx, %eax
	subl	%esi, %eax
	addl	$-60, %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	divss	%xmm0, %xmm1
	movss	%xmm1, (%rdx)
	addq	$4, %rdx
	decq	%rcx
	jne	.LBB4_6
# BB#7:                                 #   in Loop: Header=BB4_5 Depth=1
	incq	%r8
	addq	$164, %r9
	cmpq	$41, %r8
	jne	.LBB4_5
# BB#8:                                 # %rInitmatrix.exit
	movq	%rbx, seed(%rip)
	movl	$1, %r8d
	movl	$rma+172, %r9d
	.p2align	4, 0x90
.LBB4_9:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_10 Depth 2
                                        #       Child Loop BB4_11 Depth 3
	movl	$rmb+332, %r11d
	movl	$1, %esi
	.p2align	4, 0x90
.LBB4_10:                               #   Parent Loop BB4_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_11 Depth 3
	imulq	$164, %r8, %rax
	leaq	rmr(%rax,%rsi,4), %r10
	movl	$0, rmr(%rax,%rsi,4)
	xorps	%xmm0, %xmm0
	movq	%r9, %rcx
	movq	%r11, %rdx
	movl	$40, %eax
	.p2align	4, 0x90
.LBB4_11:                               #   Parent Loop BB4_9 Depth=1
                                        #     Parent Loop BB4_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	-4(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	-164(%rdx), %xmm1
	addss	%xmm0, %xmm1
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rdx), %xmm0
	addss	%xmm1, %xmm0
	addq	$328, %rdx              # imm = 0x148
	addq	$8, %rcx
	addq	$-2, %rax
	jne	.LBB4_11
# BB#12:                                # %rInnerproduct.exit
                                        #   in Loop: Header=BB4_10 Depth=2
	movss	%xmm0, (%r10)
	incq	%rsi
	addq	$4, %r11
	cmpq	$41, %rsi
	jne	.LBB4_10
# BB#13:                                #   in Loop: Header=BB4_9 Depth=1
	incq	%r8
	addq	$164, %r9
	cmpq	$41, %r8
	jne	.LBB4_9
# BB#14:
	cmpl	$39, %edi
	jg	.LBB4_15
# BB#16:
	movslq	%edi, %rax
	leaq	1(%rax), %rcx
	imulq	$164, %rcx, %rcx
	movss	rmr+4(%rcx,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	popq	%rbx
	jmp	printf                  # TAILCALL
.LBB4_15:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	Mm, .Lfunc_end4-Mm
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edi
	callq	Mm
	incl	%ebx
	cmpl	$5000, %ebx             # imm = 0x1388
	jne	.LBB5_1
# BB#2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc

	.type	seed,@object            # @seed
	.comm	seed,8,8
	.type	rma,@object             # @rma
	.comm	rma,6724,16
	.type	rmb,@object             # @rmb
	.comm	rmb,6724,16
	.type	rmr,@object             # @rmr
	.comm	rmr,6724,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%f\n"
	.size	.L.str, 4

	.type	value,@object           # @value
	.comm	value,4,4
	.type	fixed,@object           # @fixed
	.comm	fixed,4,4
	.type	floated,@object         # @floated
	.comm	floated,4,4
	.type	permarray,@object       # @permarray
	.comm	permarray,44,16
	.type	pctr,@object            # @pctr
	.comm	pctr,4,4
	.type	tree,@object            # @tree
	.comm	tree,8,8
	.type	stack,@object           # @stack
	.comm	stack,16,16
	.type	cellspace,@object       # @cellspace
	.comm	cellspace,152,16
	.type	freelist,@object        # @freelist
	.comm	freelist,4,4
	.type	movesdone,@object       # @movesdone
	.comm	movesdone,4,4
	.type	ima,@object             # @ima
	.comm	ima,6724,16
	.type	imb,@object             # @imb
	.comm	imb,6724,16
	.type	imr,@object             # @imr
	.comm	imr,6724,16
	.type	piececount,@object      # @piececount
	.comm	piececount,16,16
	.type	class,@object           # @class
	.comm	class,52,16
	.type	piecemax,@object        # @piecemax
	.comm	piecemax,52,16
	.type	puzzl,@object           # @puzzl
	.comm	puzzl,2048,16
	.type	p,@object               # @p
	.comm	p,26624,16
	.type	n,@object               # @n
	.comm	n,4,4
	.type	kount,@object           # @kount
	.comm	kount,4,4
	.type	sortlist,@object        # @sortlist
	.comm	sortlist,20004,16
	.type	biggest,@object         # @biggest
	.comm	biggest,4,4
	.type	littlest,@object        # @littlest
	.comm	littlest,4,4
	.type	top,@object             # @top
	.comm	top,4,4
	.type	z,@object               # @z
	.comm	z,2056,16
	.type	w,@object               # @w
	.comm	w,2056,16
	.type	e,@object               # @e
	.comm	e,1040,16
	.type	zr,@object              # @zr
	.comm	zr,4,4
	.type	zi,@object              # @zi
	.comm	zi,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
