	.text
	.file	"oopack_v1p8.bc"
	.globl	_ZNK12MaxBenchmark7c_styleEv
	.p2align	4, 0x90
	.type	_ZNK12MaxBenchmark7c_styleEv,@function
_ZNK12MaxBenchmark7c_styleEv:           # @_ZNK12MaxBenchmark7c_styleEv
	.cfi_startproc
# BB#0:
	movq	$-7992, %rax            # imm = 0xE0C8
	movsd	U(%rip), %xmm0          # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movsd	U+8000(%rax), %xmm1     # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	U+8008(%rax), %xmm2     # xmm2 = mem[0],zero
	maxsd	%xmm1, %xmm2
	movsd	U+8016(%rax), %xmm0     # xmm0 = mem[0],zero
	maxsd	%xmm2, %xmm0
	addq	$24, %rax
	jne	.LBB0_1
# BB#2:
	movsd	%xmm0, MaxResult(%rip)
	retq
.Lfunc_end0:
	.size	_ZNK12MaxBenchmark7c_styleEv, .Lfunc_end0-_ZNK12MaxBenchmark7c_styleEv
	.cfi_endproc

	.globl	_ZNK12MaxBenchmark9oop_styleEv
	.p2align	4, 0x90
	.type	_ZNK12MaxBenchmark9oop_styleEv,@function
_ZNK12MaxBenchmark9oop_styleEv:         # @_ZNK12MaxBenchmark9oop_styleEv
	.cfi_startproc
# BB#0:
	movq	$-7992, %rax            # imm = 0xE0C8
	movsd	U(%rip), %xmm0          # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movsd	U+8000(%rax), %xmm1     # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	U+8008(%rax), %xmm2     # xmm2 = mem[0],zero
	maxsd	%xmm1, %xmm2
	movsd	U+8016(%rax), %xmm0     # xmm0 = mem[0],zero
	maxsd	%xmm2, %xmm0
	addq	$24, %rax
	jne	.LBB1_1
# BB#2:
	movsd	%xmm0, MaxResult(%rip)
	retq
.Lfunc_end1:
	.size	_ZNK12MaxBenchmark9oop_styleEv, .Lfunc_end1-_ZNK12MaxBenchmark9oop_styleEv
	.cfi_endproc

	.globl	_ZNK12MaxBenchmark4initEv
	.p2align	4, 0x90
	.type	_ZNK12MaxBenchmark4initEv,@function
_ZNK12MaxBenchmark4initEv:              # @_ZNK12MaxBenchmark4initEv
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, U(,%rcx,8)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, U+8(,%rcx,8)
	addq	$2, %rcx
	addl	$-2, %eax
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB2_1
# BB#2:
	retq
.Lfunc_end2:
	.size	_ZNK12MaxBenchmark4initEv, .Lfunc_end2-_ZNK12MaxBenchmark4initEv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4652007308841189376     # double 1000
	.text
	.globl	_ZNK12MaxBenchmark5checkEiRdS0_
	.p2align	4, 0x90
	.type	_ZNK12MaxBenchmark5checkEiRdS0_,@function
_ZNK12MaxBenchmark5checkEiRdS0_:        # @_ZNK12MaxBenchmark5checkEiRdS0_
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%esi, %xmm0
	mulsd	.LCPI3_0(%rip), %xmm0
	movsd	%xmm0, (%rdx)
	movq	MaxResult(%rip), %rax
	movq	%rax, (%rcx)
	retq
.Lfunc_end3:
	.size	_ZNK12MaxBenchmark5checkEiRdS0_, .Lfunc_end3-_ZNK12MaxBenchmark5checkEiRdS0_
	.cfi_endproc

	.globl	_ZNK15MatrixBenchmark7c_styleEv
	.p2align	4, 0x90
	.type	_ZNK15MatrixBenchmark7c_styleEv,@function
_ZNK15MatrixBenchmark7c_styleEv:        # @_ZNK15MatrixBenchmark7c_styleEv
	.cfi_startproc
# BB#0:
	xorl	%r8d, %r8d
	movl	$C+8, %eax
	.p2align	4, 0x90
.LBB4_1:                                # %.preheader24
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_2 Depth 2
                                        #       Child Loop BB4_3 Depth 3
	imulq	$50, %r8, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_2:                                # %.preheader
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_3 Depth 3
	xorpd	%xmm0, %xmm0
	movq	$-20000, %rdi           # imm = 0xB1E0
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB4_3:                                #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	D+20000(%rdi,%rsi,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	D+20400(%rdi,%rsi,8), %xmm0
	addsd	%xmm1, %xmm0
	addq	$16, %rcx
	addq	$800, %rdi              # imm = 0x320
	jne	.LBB4_3
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=2
	leaq	(%rsi,%rdx), %rcx
	movsd	%xmm0, E(,%rcx,8)
	incq	%rsi
	cmpq	$50, %rsi
	jne	.LBB4_2
# BB#5:                                 #   in Loop: Header=BB4_1 Depth=1
	incq	%r8
	addq	$400, %rax              # imm = 0x190
	cmpq	$50, %r8
	jne	.LBB4_1
# BB#6:
	retq
.Lfunc_end4:
	.size	_ZNK15MatrixBenchmark7c_styleEv, .Lfunc_end4-_ZNK15MatrixBenchmark7c_styleEv
	.cfi_endproc

	.globl	_ZNK15MatrixBenchmark9oop_styleEv
	.p2align	4, 0x90
	.type	_ZNK15MatrixBenchmark9oop_styleEv,@function
_ZNK15MatrixBenchmark9oop_styleEv:      # @_ZNK15MatrixBenchmark9oop_styleEv
	.cfi_startproc
# BB#0:
	xorl	%r8d, %r8d
	movl	$C+8, %eax
	.p2align	4, 0x90
.LBB5_1:                                # %.preheader40
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_2 Depth 2
                                        #       Child Loop BB5_3 Depth 3
	imulq	$50, %r8, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_3 Depth 3
	xorpd	%xmm0, %xmm0
	movq	$-20000, %rdi           # imm = 0xB1E0
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB5_3:                                #   Parent Loop BB5_1 Depth=1
                                        #     Parent Loop BB5_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	D+20000(%rdi,%rsi,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	D+20400(%rdi,%rsi,8), %xmm0
	addsd	%xmm1, %xmm0
	addq	$16, %rcx
	addq	$800, %rdi              # imm = 0x320
	jne	.LBB5_3
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=2
	leaq	(%rsi,%rdx), %rcx
	movsd	%xmm0, E(,%rcx,8)
	incq	%rsi
	cmpq	$50, %rsi
	jne	.LBB5_2
# BB#5:                                 #   in Loop: Header=BB5_1 Depth=1
	incq	%r8
	addq	$400, %rax              # imm = 0x190
	cmpq	$50, %r8
	jne	.LBB5_1
# BB#6:
	retq
.Lfunc_end5:
	.size	_ZNK15MatrixBenchmark9oop_styleEv, .Lfunc_end5-_ZNK15MatrixBenchmark9oop_styleEv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	_ZNK15MatrixBenchmark4initEv
	.p2align	4, 0x90
	.type	_ZNK15MatrixBenchmark4initEv,@function
_ZNK15MatrixBenchmark4initEv:           # @_ZNK15MatrixBenchmark4initEv
	.cfi_startproc
# BB#0:
	movq	$-2500, %rax            # imm = 0xF63C
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	leal	2501(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movsd	%xmm1, C+20000(,%rax,8)
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, D+20000(,%rax,8)
	leal	2502(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movsd	%xmm1, C+20008(,%rax,8)
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, D+20008(,%rax,8)
	addq	$2, %rax
	jne	.LBB6_1
# BB#2:
	retq
.Lfunc_end6:
	.size	_ZNK15MatrixBenchmark4initEv, .Lfunc_end6-_ZNK15MatrixBenchmark4initEv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4687829947429945344     # double 2.5E+5
	.text
	.globl	_ZNK15MatrixBenchmark5checkEiRdS0_
	.p2align	4, 0x90
	.type	_ZNK15MatrixBenchmark5checkEiRdS0_,@function
_ZNK15MatrixBenchmark5checkEiRdS0_:     # @_ZNK15MatrixBenchmark5checkEiRdS0_
	.cfi_startproc
# BB#0:
	xorpd	%xmm0, %xmm0
	movq	$-20000, %rax           # imm = 0xB1E0
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	addsd	E+20000(%rax), %xmm0
	addsd	E+20008(%rax), %xmm0
	addsd	E+20016(%rax), %xmm0
	addsd	E+20024(%rax), %xmm0
	addsd	E+20032(%rax), %xmm0
	addq	$40, %rax
	jne	.LBB7_1
# BB#2:
	movsd	%xmm0, (%rcx)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	mulsd	.LCPI7_0(%rip), %xmm0
	movsd	%xmm0, (%rdx)
	retq
.Lfunc_end7:
	.size	_ZNK15MatrixBenchmark5checkEiRdS0_, .Lfunc_end7-_ZNK15MatrixBenchmark5checkEiRdS0_
	.cfi_endproc

	.globl	_ZNK17IteratorBenchmark7c_styleEv
	.p2align	4, 0x90
	.type	_ZNK17IteratorBenchmark7c_styleEv,@function
_ZNK17IteratorBenchmark7c_styleEv:      # @_ZNK17IteratorBenchmark7c_styleEv
	.cfi_startproc
# BB#0:
	xorpd	%xmm0, %xmm0
	movq	$-8000, %rax            # imm = 0xE0C0
	.p2align	4, 0x90
.LBB8_1:                                # =>This Inner Loop Header: Depth=1
	movsd	A+8000(%rax), %xmm1     # xmm1 = mem[0],zero
	mulsd	B+8000(%rax), %xmm1
	addsd	%xmm0, %xmm1
	movsd	A+8008(%rax), %xmm0     # xmm0 = mem[0],zero
	mulsd	B+8008(%rax), %xmm0
	addsd	%xmm1, %xmm0
	movsd	A+8016(%rax), %xmm1     # xmm1 = mem[0],zero
	mulsd	B+8016(%rax), %xmm1
	addsd	%xmm0, %xmm1
	movsd	A+8024(%rax), %xmm2     # xmm2 = mem[0],zero
	mulsd	B+8024(%rax), %xmm2
	addsd	%xmm1, %xmm2
	movsd	A+8032(%rax), %xmm0     # xmm0 = mem[0],zero
	mulsd	B+8032(%rax), %xmm0
	addsd	%xmm2, %xmm0
	addq	$40, %rax
	jne	.LBB8_1
# BB#2:
	movsd	%xmm0, IteratorResult(%rip)
	retq
.Lfunc_end8:
	.size	_ZNK17IteratorBenchmark7c_styleEv, .Lfunc_end8-_ZNK17IteratorBenchmark7c_styleEv
	.cfi_endproc

	.globl	_ZNK17IteratorBenchmark9oop_styleEv
	.p2align	4, 0x90
	.type	_ZNK17IteratorBenchmark9oop_styleEv,@function
_ZNK17IteratorBenchmark9oop_styleEv:    # @_ZNK17IteratorBenchmark9oop_styleEv
	.cfi_startproc
# BB#0:
	xorpd	%xmm0, %xmm0
	movq	$-8000, %rax            # imm = 0xE0C0
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movsd	A+8000(%rax), %xmm1     # xmm1 = mem[0],zero
	mulsd	B+8000(%rax), %xmm1
	addsd	%xmm0, %xmm1
	movsd	A+8008(%rax), %xmm0     # xmm0 = mem[0],zero
	mulsd	B+8008(%rax), %xmm0
	addsd	%xmm1, %xmm0
	movsd	A+8016(%rax), %xmm1     # xmm1 = mem[0],zero
	mulsd	B+8016(%rax), %xmm1
	addsd	%xmm0, %xmm1
	movsd	A+8024(%rax), %xmm2     # xmm2 = mem[0],zero
	mulsd	B+8024(%rax), %xmm2
	addsd	%xmm1, %xmm2
	movsd	A+8032(%rax), %xmm0     # xmm0 = mem[0],zero
	mulsd	B+8032(%rax), %xmm0
	addsd	%xmm2, %xmm0
	addq	$40, %rax
	jne	.LBB9_1
# BB#2:
	movsd	%xmm0, IteratorResult(%rip)
	retq
.Lfunc_end9:
	.size	_ZNK17IteratorBenchmark9oop_styleEv, .Lfunc_end9-_ZNK17IteratorBenchmark9oop_styleEv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	_ZNK17IteratorBenchmark4initEv
	.p2align	4, 0x90
	.type	_ZNK17IteratorBenchmark4initEv,@function
_ZNK17IteratorBenchmark4initEv:         # @_ZNK17IteratorBenchmark4initEv
	.cfi_startproc
# BB#0:
	movq	$-1000, %rax            # imm = 0xFC18
	movsd	.LCPI10_0(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	leal	1001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movsd	%xmm1, A+8000(,%rax,8)
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, B+8000(,%rax,8)
	leal	1002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movsd	%xmm1, A+8008(,%rax,8)
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, B+8008(,%rax,8)
	addq	$2, %rax
	jne	.LBB10_1
# BB#2:
	retq
.Lfunc_end10:
	.size	_ZNK17IteratorBenchmark4initEv, .Lfunc_end10-_ZNK17IteratorBenchmark4initEv
	.cfi_endproc

	.globl	_ZNK17IteratorBenchmark5checkEiRdS0_
	.p2align	4, 0x90
	.type	_ZNK17IteratorBenchmark5checkEiRdS0_,@function
_ZNK17IteratorBenchmark5checkEiRdS0_:   # @_ZNK17IteratorBenchmark5checkEiRdS0_
	.cfi_startproc
# BB#0:
	imull	$2000, %esi, %eax       # imm = 0x7D0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rdx)
	movq	IteratorResult(%rip), %rax
	movq	%rax, (%rcx)
	retq
.Lfunc_end11:
	.size	_ZNK17IteratorBenchmark5checkEiRdS0_, .Lfunc_end11-_ZNK17IteratorBenchmark5checkEiRdS0_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
.LCPI12_1:
	.quad	4605975682916830378     # double 0.8660254037844386
	.quad	4605975682916830378     # double 0.8660254037844386
	.text
	.globl	_ZNK16ComplexBenchmark7c_styleEv
	.p2align	4, 0x90
	.type	_ZNK16ComplexBenchmark7c_styleEv,@function
_ZNK16ComplexBenchmark7c_styleEv:       # @_ZNK16ComplexBenchmark7c_styleEv
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	$-16000, %rax           # imm = 0xC180
	movapd	.LCPI12_0(%rip), %xmm0  # xmm0 = [5.000000e-01,5.000000e-01]
	movapd	.LCPI12_1(%rip), %xmm1  # xmm1 = [8.660254e-01,8.660254e-01]
	.p2align	4, 0x90
.LBB12_1:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movapd	Y+16016(%rax), %xmm2
	movapd	Y+16000(%rax), %xmm3
	movapd	%xmm3, %xmm4
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	unpckhpd	%xmm2, %xmm3    # xmm3 = xmm3[1],xmm2[1]
	movapd	X+16016(%rax), %xmm2
	movapd	X+16000(%rax), %xmm5
	movapd	%xmm5, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	unpckhpd	%xmm2, %xmm5    # xmm5 = xmm5[1],xmm2[1]
	movapd	%xmm6, %xmm2
	mulpd	%xmm0, %xmm2
	addpd	%xmm4, %xmm2
	movapd	%xmm5, %xmm4
	mulpd	%xmm1, %xmm4
	subpd	%xmm4, %xmm2
	mulpd	%xmm0, %xmm5
	addpd	%xmm3, %xmm5
	mulpd	%xmm1, %xmm6
	addpd	%xmm5, %xmm6
	movapd	%xmm2, %xmm3
	unpcklpd	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0]
	movhlps	%xmm2, %xmm6            # xmm6 = xmm2[1],xmm6[1]
	movups	%xmm6, Y+16016(%rax)
	movupd	%xmm3, Y+16000(%rax)
	addq	$32, %rax
	jne	.LBB12_1
# BB#2:                                 # %middle.block
	retq
.Lfunc_end12:
	.size	_ZNK16ComplexBenchmark7c_styleEv, .Lfunc_end12-_ZNK16ComplexBenchmark7c_styleEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
.LCPI13_1:
	.quad	4605975682916830378     # double 0.8660254037844386
	.quad	4605975682916830378     # double 0.8660254037844386
	.text
	.globl	_ZNK16ComplexBenchmark9oop_styleEv
	.p2align	4, 0x90
	.type	_ZNK16ComplexBenchmark9oop_styleEv,@function
_ZNK16ComplexBenchmark9oop_styleEv:     # @_ZNK16ComplexBenchmark9oop_styleEv
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	$-16000, %rax           # imm = 0xC180
	movapd	.LCPI13_0(%rip), %xmm0  # xmm0 = [5.000000e-01,5.000000e-01]
	movapd	.LCPI13_1(%rip), %xmm1  # xmm1 = [8.660254e-01,8.660254e-01]
	.p2align	4, 0x90
.LBB13_1:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movapd	Y+16016(%rax), %xmm2
	movapd	Y+16000(%rax), %xmm3
	movapd	%xmm3, %xmm4
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	unpckhpd	%xmm2, %xmm3    # xmm3 = xmm3[1],xmm2[1]
	movapd	X+16016(%rax), %xmm2
	movapd	X+16000(%rax), %xmm5
	movapd	%xmm5, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	unpckhpd	%xmm2, %xmm5    # xmm5 = xmm5[1],xmm2[1]
	movapd	%xmm6, %xmm2
	mulpd	%xmm0, %xmm2
	movapd	%xmm5, %xmm7
	mulpd	%xmm1, %xmm7
	subpd	%xmm7, %xmm2
	mulpd	%xmm0, %xmm5
	mulpd	%xmm1, %xmm6
	addpd	%xmm5, %xmm6
	addpd	%xmm4, %xmm2
	addpd	%xmm3, %xmm6
	movapd	%xmm2, %xmm3
	unpcklpd	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0]
	movhlps	%xmm2, %xmm6            # xmm6 = xmm2[1],xmm6[1]
	movups	%xmm6, Y+16016(%rax)
	movupd	%xmm3, Y+16000(%rax)
	addq	$32, %rax
	jne	.LBB13_1
# BB#2:                                 # %middle.block
	retq
.Lfunc_end13:
	.size	_ZNK16ComplexBenchmark9oop_styleEv, .Lfunc_end13-_ZNK16ComplexBenchmark9oop_styleEv
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	_ZNK16ComplexBenchmark4initEv
	.p2align	4, 0x90
	.type	_ZNK16ComplexBenchmark4initEv,@function
_ZNK16ComplexBenchmark4initEv:          # @_ZNK16ComplexBenchmark4initEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$Y, %edi
	xorl	%esi, %esi
	movl	$16000, %edx            # imm = 0x3E80
	callq	memset
	movl	$1, %eax
	movq	$-16000, %rcx           # imm = 0xC180
	movsd	.LCPI14_0(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB14_1:                               # =>This Inner Loop Header: Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm1, X+16000(%rcx)
	movsd	%xmm2, X+16008(%rcx)
	leal	1(%rax), %edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm1, X+16016(%rcx)
	movsd	%xmm2, X+16024(%rcx)
	addl	$2, %eax
	addq	$32, %rcx
	jne	.LBB14_1
# BB#2:
	popq	%rax
	retq
.Lfunc_end14:
	.size	_ZNK16ComplexBenchmark4initEv, .Lfunc_end14-_ZNK16ComplexBenchmark4initEv
	.cfi_endproc

	.globl	_ZNK16ComplexBenchmark5checkEiRdS0_
	.p2align	4, 0x90
	.type	_ZNK16ComplexBenchmark5checkEiRdS0_,@function
_ZNK16ComplexBenchmark5checkEiRdS0_:    # @_ZNK16ComplexBenchmark5checkEiRdS0_
	.cfi_startproc
# BB#0:
	xorpd	%xmm0, %xmm0
	movq	$-16000, %rax           # imm = 0xC180
	.p2align	4, 0x90
.LBB15_1:                               # =>This Inner Loop Header: Depth=1
	movsd	Y+16000(%rax), %xmm1    # xmm1 = mem[0],zero
	addsd	Y+16008(%rax), %xmm1
	addsd	%xmm0, %xmm1
	movsd	Y+16016(%rax), %xmm0    # xmm0 = mem[0],zero
	addsd	Y+16024(%rax), %xmm0
	addsd	%xmm1, %xmm0
	addq	$32, %rax
	jne	.LBB15_1
# BB#2:
	movsd	%xmm0, (%rcx)
	imull	$8000, %esi, %eax       # imm = 0x1F40
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rdx)
	retq
.Lfunc_end15:
	.size	_ZNK16ComplexBenchmark5checkEiRdS0_, .Lfunc_end15-_ZNK16ComplexBenchmark5checkEiRdS0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI16_0:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI16_1:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_
	.p2align	4, 0x90
	.type	_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_,@function
_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_: # @_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%ecx, %r12d
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %r13
	movq	(%r13), %rax
	callq	*8(%rax)
	addq	%r13, %rbp
	movq	%r14, %r15
	andq	$1, %r15
	je	.LBB16_2
# BB#1:
	movq	(%rbp), %rax
	movq	-1(%rax,%r14), %rax
	jmp	.LBB16_3
.LBB16_2:
	movq	%r14, %rax
.LBB16_3:
	movq	%rbp, %rdi
	callq	*%rax
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*8(%rax)
	callq	clock
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%r12d, %r12d
	jle	.LBB16_7
# BB#4:                                 # %.lr.ph
	movl	%r12d, %ebx
	testq	%r15, %r15
	je	.LBB16_5
	.p2align	4, 0x90
.LBB16_6:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*-1(%rax,%r14)
	decl	%ebx
	jne	.LBB16_6
	jmp	.LBB16_7
	.p2align	4, 0x90
.LBB16_5:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	*%r14
	decl	%ebx
	jne	.LBB16_5
.LBB16_7:                               # %._crit_edge
	callq	clock
	movq	%rax, %rbx
	movq	(%r13), %rax
	leaq	32(%rsp), %rdx
	movq	%r13, %rdi
	movl	%r12d, %esi
	movq	96(%rsp), %rcx
	callq	*32(%rax)
	subq	8(%rsp), %rbx           # 8-byte Folded Reload
	cvtsi2sdq	%rbx, %xmm0
	divsd	.LCPI16_0(%rip), %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	movsd	%xmm0, (%rax)
	movsd	32(%rsp), %xmm1         # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	mulsd	.LCPI16_1(%rip), %xmm1
	movq	24(%rsp), %rax          # 8-byte Reload
	movsd	%xmm1, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_, .Lfunc_end16-_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_
	.cfi_endproc

	.globl	_ZN9Benchmark4findEPKc
	.p2align	4, 0x90
	.type	_ZN9Benchmark4findEPKc,@function
_ZN9Benchmark4findEPKc:                 # @_ZN9Benchmark4findEPKc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpl	$0, _ZN9Benchmark5countE(%rip)
	jle	.LBB17_1
# BB#3:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	_ZN9Benchmark4listE(,%rbx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB17_5
# BB#2:                                 #   in Loop: Header=BB17_4 Depth=1
	incq	%rbx
	movslq	_ZN9Benchmark5countE(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB17_4
	jmp	.LBB17_6
.LBB17_1:
	xorl	%r14d, %r14d
	jmp	.LBB17_6
.LBB17_5:
	movq	_ZN9Benchmark4listE(,%rbx,8), %r14
.LBB17_6:                               # %.loopexit
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZN9Benchmark4findEPKc, .Lfunc_end17-_ZN9Benchmark4findEPKc
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI18_0:
	.quad	4400016835940974592     # double 1.4210854715202004E-14
.LCPI18_1:
	.quad	-4823355200913801216    # double -1.4210854715202004E-14
	.text
	.globl	_ZNK9Benchmark9time_bothEi
	.p2align	4, 0x90
	.type	_ZNK9Benchmark9time_bothEi,@function
_ZNK9Benchmark9time_bothEi:             # @_ZNK9Benchmark9time_bothEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*8(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	callq	clock
	testl	%r14d, %r14d
	jle	.LBB18_3
# BB#1:                                 # %.lr.ph.split.us.i.preheader
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB18_2:                               # %.lr.ph.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	decl	%ebp
	jne	.LBB18_2
.LBB18_3:                               # %_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_.exit
	callq	clock
	movq	(%rbx), %rax
	leaq	24(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	*32(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*24(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	callq	clock
	testl	%r14d, %r14d
	jle	.LBB18_6
# BB#4:                                 # %.lr.ph.split.us.i21.preheader
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB18_5:                               # %.lr.ph.split.us.i21
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*24(%rax)
	decl	%ebp
	jne	.LBB18_5
.LBB18_6:                               # %_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_.exit22
	callq	clock
	movq	(%rbx), %rax
	leaq	24(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	*32(%rax)
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	movsd	(%rsp), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	minsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm2
	ucomisd	.LCPI18_0(%rip), %xmm2
	ja	.LBB18_8
# BB#7:                                 # %_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_.exit22
	movsd	.LCPI18_1(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	jbe	.LBB18_9
.LBB18_8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	callq	*(%rax)
	movq	%rax, %rcx
	movsd	8(%rsp), %xmm1          # xmm1 = mem[0],zero
	movsd	(%rsp), %xmm2           # xmm2 = mem[0],zero
	movl	$.L.str, %edi
	movb	$3, %al
	movq	%rcx, %rsi
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	printf
.LBB18_9:                               # %_ZNK9Benchmark8time_oneEMS_KFvvEiRdS2_S2_.exit22._crit_edge
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, %rcx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movl	%r14d, %edx
	callq	printf
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZNK9Benchmark9time_bothEi, .Lfunc_end18-_ZNK9Benchmark9time_bothEi
	.cfi_endproc

	.globl	_Z5UsageiPPc
	.p2align	4, 0x90
	.type	_Z5UsageiPPc,@function
_Z5UsageiPPc:                           # @_Z5UsageiPPc
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	movq	(%rsi), %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end19:
	.size	_Z5UsageiPPc, .Lfunc_end19-_Z5UsageiPPc
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
	subq	$136, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 176
.Lcfi33:
	.cfi_offset %rbx, -40
.Lcfi34:
	.cfi_offset %r12, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movw	$116, 12(%rsp)
	movl	$1970220641, 8(%rsp)    # imm = 0x756F2E61
	movabsq	$3472333798303621453, %rax # imm = 0x303035313D78614D
	movq	%rax, 32(%rsp)
	movw	$48, 40(%rsp)
	movabsq	$3620182069762875725, %rax # imm = 0x323D78697274614D
	movq	%rax, 16(%rsp)
	movb	$0, 26(%rsp)
	movw	$12336, 24(%rsp)        # imm = 0x3030
	movabsq	$13563782441564261, %rax # imm = 0x303030323D7865
	movq	%rax, 53(%rsp)
	movabsq	$4429401749935976259, %rax # imm = 0x3D78656C706D6F43
	movq	%rax, 48(%rsp)
	movabsq	$13563782407273842, %rax # imm = 0x30303030323D72
	movq	%rax, 71(%rsp)
	movabsq	$8245937404618568777, %rax # imm = 0x726F746172657449
	movq	%rax, 64(%rsp)
	leaq	8(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	32(%rsp), %rbx
	movq	%rbx, 88(%rsp)
	leaq	16(%rsp), %rax
	movq	%rax, 96(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 104(%rsp)
	leaq	64(%rsp), %rax
	movq	%rax, 112(%rsp)
	movq	$0, 120(%rsp)
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$4, 155(%rax)
	je	.LBB20_1
# BB#13:
	testb	$4, 135(%rax)
	je	.LBB20_1
# BB#14:
	testb	$4, 147(%rax)
	je	.LBB20_1
# BB#15:
	movl	$.L.str.10, %edi
	movl	$.L.str.11, %esi
	movl	$.L.str.11, %edx
	movl	$.L.str.12, %ecx
	movl	$.L.str.13, %r8d
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.14, %edi
	movl	$.L.str.15, %esi
	movl	$.L.str.16, %edx
	movl	$.L.str.17, %ecx
	movl	$.L.str.18, %r8d
	movl	$.L.str.17, %r9d
	movl	$0, %eax
	pushq	$.L.str.19
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.18
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$16, %rsp
.Lcfi39:
	.cfi_adjust_cfa_offset -16
	movl	$.L.str.10, %edi
	movl	$.L.str.20, %esi
	movl	$.L.str.21, %edx
	movl	$.L.str.22, %ecx
	movl	$.L.str.22, %r8d
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	printf
	movl	$1, %r15d
	jmp	.LBB20_2
	.p2align	4, 0x90
.LBB20_6:                               #   in Loop: Header=BB20_7 Depth=2
	incq	%rbx
	movslq	_ZN9Benchmark5countE(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB20_7
	jmp	.LBB20_16
	.p2align	4, 0x90
.LBB20_8:                               # %_ZN9Benchmark4findEPKc.exit
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	_ZN9Benchmark4listE(,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB20_16
# BB#9:                                 #   in Loop: Header=BB20_2 Depth=1
	movl	%r14d, %esi
	callq	_ZNK9Benchmark9time_bothEi
	jmp	.LBB20_10
	.p2align	4, 0x90
.LBB20_3:                               #   in Loop: Header=BB20_2 Depth=1
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	printf
.LBB20_10:                              #   in Loop: Header=BB20_2 Depth=1
	leaq	1(%r15), %rax
	cmpq	$4, %rax
	jg	.LBB20_12
# BB#11:                                # %._crit_edge44
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	88(%rsp,%r15,8), %rbx
	movq	%rax, %r15
.LBB20_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_7 Depth 2
	movl	$.L.str.24, %esi
	movq	%rbx, %rdi
	callq	strtok
	movq	%rax, %r12
	xorl	%edi, %edi
	movl	$.L.str.11, %esi
	callq	strtok
	testq	%rax, %rax
	je	.LBB20_3
# BB#4:                                 #   in Loop: Header=BB20_2 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	strtol
	movq	%rax, %r14
	cmpl	$0, _ZN9Benchmark5countE(%rip)
	jle	.LBB20_16
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB20_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_7:                               # %.lr.ph.i
                                        #   Parent Loop BB20_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	_ZN9Benchmark4listE(,%rbx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB20_6
	jmp	.LBB20_8
.LBB20_12:
	movl	$.Lstr.1, %edi
	callq	puts
	xorl	%eax, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB20_16:                              # %_ZN9Benchmark4findEPKc.exit.thread
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	printf
	callq	abort
.LBB20_1:
	leaq	80(%rsp), %rsi
	callq	_Z5UsageiPPc
.Lfunc_end20:
	.size	main, .Lfunc_end20-main
	.cfi_endproc

	.section	.text._ZNK12MaxBenchmark4nameEv,"axG",@progbits,_ZNK12MaxBenchmark4nameEv,comdat
	.weak	_ZNK12MaxBenchmark4nameEv
	.p2align	4, 0x90
	.type	_ZNK12MaxBenchmark4nameEv,@function
_ZNK12MaxBenchmark4nameEv:              # @_ZNK12MaxBenchmark4nameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.28, %eax
	retq
.Lfunc_end21:
	.size	_ZNK12MaxBenchmark4nameEv, .Lfunc_end21-_ZNK12MaxBenchmark4nameEv
	.cfi_endproc

	.section	.text._ZNK15MatrixBenchmark4nameEv,"axG",@progbits,_ZNK15MatrixBenchmark4nameEv,comdat
	.weak	_ZNK15MatrixBenchmark4nameEv
	.p2align	4, 0x90
	.type	_ZNK15MatrixBenchmark4nameEv,@function
_ZNK15MatrixBenchmark4nameEv:           # @_ZNK15MatrixBenchmark4nameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.29, %eax
	retq
.Lfunc_end22:
	.size	_ZNK15MatrixBenchmark4nameEv, .Lfunc_end22-_ZNK15MatrixBenchmark4nameEv
	.cfi_endproc

	.section	.text._ZNK17IteratorBenchmark4nameEv,"axG",@progbits,_ZNK17IteratorBenchmark4nameEv,comdat
	.weak	_ZNK17IteratorBenchmark4nameEv
	.p2align	4, 0x90
	.type	_ZNK17IteratorBenchmark4nameEv,@function
_ZNK17IteratorBenchmark4nameEv:         # @_ZNK17IteratorBenchmark4nameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.30, %eax
	retq
.Lfunc_end23:
	.size	_ZNK17IteratorBenchmark4nameEv, .Lfunc_end23-_ZNK17IteratorBenchmark4nameEv
	.cfi_endproc

	.section	.text._ZNK16ComplexBenchmark4nameEv,"axG",@progbits,_ZNK16ComplexBenchmark4nameEv,comdat
	.weak	_ZNK16ComplexBenchmark4nameEv
	.p2align	4, 0x90
	.type	_ZNK16ComplexBenchmark4nameEv,@function
_ZNK16ComplexBenchmark4nameEv:          # @_ZNK16ComplexBenchmark4nameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str.31, %eax
	retq
.Lfunc_end24:
	.size	_ZNK16ComplexBenchmark4nameEv, .Lfunc_end24-_ZNK16ComplexBenchmark4nameEv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_oopack_v1p8.ii,@function
_GLOBAL__sub_I_oopack_v1p8.ii:          # @_GLOBAL__sub_I_oopack_v1p8.ii
	.cfi_startproc
# BB#0:
	movslq	_ZN9Benchmark5countE(%rip), %rax
	movq	$TheMaxBenchmark, _ZN9Benchmark4listE(,%rax,8)
	movq	$_ZTV12MaxBenchmark+16, TheMaxBenchmark(%rip)
	movq	$TheMatrixBenchmark, _ZN9Benchmark4listE+8(,%rax,8)
	movq	$_ZTV15MatrixBenchmark+16, TheMatrixBenchmark(%rip)
	movq	$TheIteratorBenchmark, _ZN9Benchmark4listE+16(,%rax,8)
	movq	$_ZTV17IteratorBenchmark+16, TheIteratorBenchmark(%rip)
	leal	4(%rax), %ecx
	movl	%ecx, _ZN9Benchmark5countE(%rip)
	movq	$TheComplexBenchmark, _ZN9Benchmark4listE+24(,%rax,8)
	movq	$_ZTV16ComplexBenchmark+16, TheComplexBenchmark(%rip)
	retq
.Lfunc_end25:
	.size	_GLOBAL__sub_I_oopack_v1p8.ii, .Lfunc_end25-_GLOBAL__sub_I_oopack_v1p8.ii
	.cfi_endproc

	.type	_ZN9Benchmark5countE,@object # @_ZN9Benchmark5countE
	.bss
	.globl	_ZN9Benchmark5countE
	.p2align	2
_ZN9Benchmark5countE:
	.long	0                       # 0x0
	.size	_ZN9Benchmark5countE, 4

	.type	_ZN9Benchmark4listE,@object # @_ZN9Benchmark4listE
	.globl	_ZN9Benchmark4listE
	.p2align	4
_ZN9Benchmark4listE:
	.zero	32
	.size	_ZN9Benchmark4listE, 32

	.type	U,@object               # @U
	.globl	U
	.p2align	4
U:
	.zero	8000
	.size	U, 8000

	.type	MaxResult,@object       # @MaxResult
	.globl	MaxResult
	.p2align	3
MaxResult:
	.quad	0                       # double 0
	.size	MaxResult, 8

	.type	TheMaxBenchmark,@object # @TheMaxBenchmark
	.globl	TheMaxBenchmark
	.p2align	3
TheMaxBenchmark:
	.zero	8
	.size	TheMaxBenchmark, 8

	.type	C,@object               # @C
	.globl	C
	.p2align	4
C:
	.zero	20000
	.size	C, 20000

	.type	D,@object               # @D
	.globl	D
	.p2align	4
D:
	.zero	20000
	.size	D, 20000

	.type	E,@object               # @E
	.globl	E
	.p2align	4
E:
	.zero	20000
	.size	E, 20000

	.type	TheMatrixBenchmark,@object # @TheMatrixBenchmark
	.globl	TheMatrixBenchmark
	.p2align	3
TheMatrixBenchmark:
	.zero	8
	.size	TheMatrixBenchmark, 8

	.type	A,@object               # @A
	.globl	A
	.p2align	4
A:
	.zero	8000
	.size	A, 8000

	.type	B,@object               # @B
	.globl	B
	.p2align	4
B:
	.zero	8000
	.size	B, 8000

	.type	IteratorResult,@object  # @IteratorResult
	.globl	IteratorResult
	.p2align	3
IteratorResult:
	.quad	0                       # double 0
	.size	IteratorResult, 8

	.type	TheIteratorBenchmark,@object # @TheIteratorBenchmark
	.globl	TheIteratorBenchmark
	.p2align	3
TheIteratorBenchmark:
	.zero	8
	.size	TheIteratorBenchmark, 8

	.type	TheComplexBenchmark,@object # @TheComplexBenchmark
	.globl	TheComplexBenchmark
	.p2align	3
TheComplexBenchmark:
	.zero	8
	.size	TheComplexBenchmark, 8

	.type	X,@object               # @X
	.globl	X
	.p2align	4
X:
	.zero	16000
	.size	X, 16000

	.type	Y,@object               # @Y
	.globl	Y
	.p2align	4
Y:
	.zero	16000
	.size	Y, 16000

	.type	C_Seconds,@object       # @C_Seconds
	.data
	.globl	C_Seconds
	.p2align	3
C_Seconds:
	.quad	4607182418800017408     # double 1
	.size	C_Seconds, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%-10s: warning: relative checksum error of %g between C (%g) and oop (%g)\n"
	.size	.L.str, 75

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%-10s %10d\n"
	.size	.L.str.6, 12

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Version 1.7"
	.size	.L.str.7, 12

	.type	Version,@object         # @Version
	.data
	.globl	Version
	.p2align	3
Version:
	.quad	.L.str.7
	.size	Version, 8

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"Usage:\t%s test1=iterations1 test2=iterations2 ...\n"
	.size	.L.str.8, 51

	.type	.L_ZZ4mainE4str1,@object # @_ZZ4mainE4str1
.L_ZZ4mainE4str1:
	.asciz	"a.out"
	.size	.L_ZZ4mainE4str1, 6

	.type	.L_ZZ4mainE4str2,@object # @_ZZ4mainE4str2
.L_ZZ4mainE4str2:
	.asciz	"Max=15000"
	.size	.L_ZZ4mainE4str2, 10

	.type	.L_ZZ4mainE4str3,@object # @_ZZ4mainE4str3
.L_ZZ4mainE4str3:
	.asciz	"Matrix=200"
	.size	.L_ZZ4mainE4str3, 11

	.type	.L_ZZ4mainE4str4,@object # @_ZZ4mainE4str4
.L_ZZ4mainE4str4:
	.asciz	"Complex=2000"
	.size	.L_ZZ4mainE4str4, 13

	.type	.L_ZZ4mainE4str5,@object # @_ZZ4mainE4str5
.L_ZZ4mainE4str5:
	.asciz	"Iterator=20000"
	.size	.L_ZZ4mainE4str5, 15

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%-10s %10s  %11s  %11s  %5s\n"
	.size	.L.str.10, 29

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.zero	1
	.size	.L.str.11, 1

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Seconds  "
	.size	.L.str.12, 10

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Mflops  "
	.size	.L.str.13, 9

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%-10s %10s  %5s %5s  %5s %5s  %5s\n"
	.size	.L.str.14, 35

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Test"
	.size	.L.str.15, 5

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Iterations"
	.size	.L.str.16, 11

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	" C "
	.size	.L.str.17, 4

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"OOP"
	.size	.L.str.18, 4

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Ratio"
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"----"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"----------"
	.size	.L.str.21, 11

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"-----------"
	.size	.L.str.22, 12

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"-----"
	.size	.L.str.23, 6

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"="
	.size	.L.str.24, 2

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"missing iteration count for test '%s'\n"
	.size	.L.str.25, 39

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"skipping non-existent test = '%s'\n"
	.size	.L.str.26, 35

	.type	_ZTV12MaxBenchmark,@object # @_ZTV12MaxBenchmark
	.section	.rodata,"a",@progbits
	.globl	_ZTV12MaxBenchmark
	.p2align	3
_ZTV12MaxBenchmark:
	.quad	0
	.quad	_ZTI12MaxBenchmark
	.quad	_ZNK12MaxBenchmark4nameEv
	.quad	_ZNK12MaxBenchmark4initEv
	.quad	_ZNK12MaxBenchmark7c_styleEv
	.quad	_ZNK12MaxBenchmark9oop_styleEv
	.quad	_ZNK12MaxBenchmark5checkEiRdS0_
	.size	_ZTV12MaxBenchmark, 56

	.type	_ZTS12MaxBenchmark,@object # @_ZTS12MaxBenchmark
	.globl	_ZTS12MaxBenchmark
_ZTS12MaxBenchmark:
	.asciz	"12MaxBenchmark"
	.size	_ZTS12MaxBenchmark, 15

	.type	_ZTS9Benchmark,@object  # @_ZTS9Benchmark
	.section	.rodata._ZTS9Benchmark,"aG",@progbits,_ZTS9Benchmark,comdat
	.weak	_ZTS9Benchmark
_ZTS9Benchmark:
	.asciz	"9Benchmark"
	.size	_ZTS9Benchmark, 11

	.type	_ZTI9Benchmark,@object  # @_ZTI9Benchmark
	.section	.rodata._ZTI9Benchmark,"aG",@progbits,_ZTI9Benchmark,comdat
	.weak	_ZTI9Benchmark
	.p2align	3
_ZTI9Benchmark:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS9Benchmark
	.size	_ZTI9Benchmark, 16

	.type	_ZTI12MaxBenchmark,@object # @_ZTI12MaxBenchmark
	.section	.rodata,"a",@progbits
	.globl	_ZTI12MaxBenchmark
	.p2align	4
_ZTI12MaxBenchmark:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS12MaxBenchmark
	.quad	_ZTI9Benchmark
	.size	_ZTI12MaxBenchmark, 24

	.type	_ZTV15MatrixBenchmark,@object # @_ZTV15MatrixBenchmark
	.globl	_ZTV15MatrixBenchmark
	.p2align	3
_ZTV15MatrixBenchmark:
	.quad	0
	.quad	_ZTI15MatrixBenchmark
	.quad	_ZNK15MatrixBenchmark4nameEv
	.quad	_ZNK15MatrixBenchmark4initEv
	.quad	_ZNK15MatrixBenchmark7c_styleEv
	.quad	_ZNK15MatrixBenchmark9oop_styleEv
	.quad	_ZNK15MatrixBenchmark5checkEiRdS0_
	.size	_ZTV15MatrixBenchmark, 56

	.type	_ZTS15MatrixBenchmark,@object # @_ZTS15MatrixBenchmark
	.globl	_ZTS15MatrixBenchmark
	.p2align	4
_ZTS15MatrixBenchmark:
	.asciz	"15MatrixBenchmark"
	.size	_ZTS15MatrixBenchmark, 18

	.type	_ZTI15MatrixBenchmark,@object # @_ZTI15MatrixBenchmark
	.globl	_ZTI15MatrixBenchmark
	.p2align	4
_ZTI15MatrixBenchmark:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15MatrixBenchmark
	.quad	_ZTI9Benchmark
	.size	_ZTI15MatrixBenchmark, 24

	.type	_ZTV17IteratorBenchmark,@object # @_ZTV17IteratorBenchmark
	.globl	_ZTV17IteratorBenchmark
	.p2align	3
_ZTV17IteratorBenchmark:
	.quad	0
	.quad	_ZTI17IteratorBenchmark
	.quad	_ZNK17IteratorBenchmark4nameEv
	.quad	_ZNK17IteratorBenchmark4initEv
	.quad	_ZNK17IteratorBenchmark7c_styleEv
	.quad	_ZNK17IteratorBenchmark9oop_styleEv
	.quad	_ZNK17IteratorBenchmark5checkEiRdS0_
	.size	_ZTV17IteratorBenchmark, 56

	.type	_ZTS17IteratorBenchmark,@object # @_ZTS17IteratorBenchmark
	.globl	_ZTS17IteratorBenchmark
	.p2align	4
_ZTS17IteratorBenchmark:
	.asciz	"17IteratorBenchmark"
	.size	_ZTS17IteratorBenchmark, 20

	.type	_ZTI17IteratorBenchmark,@object # @_ZTI17IteratorBenchmark
	.globl	_ZTI17IteratorBenchmark
	.p2align	4
_ZTI17IteratorBenchmark:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS17IteratorBenchmark
	.quad	_ZTI9Benchmark
	.size	_ZTI17IteratorBenchmark, 24

	.type	_ZTV16ComplexBenchmark,@object # @_ZTV16ComplexBenchmark
	.globl	_ZTV16ComplexBenchmark
	.p2align	3
_ZTV16ComplexBenchmark:
	.quad	0
	.quad	_ZTI16ComplexBenchmark
	.quad	_ZNK16ComplexBenchmark4nameEv
	.quad	_ZNK16ComplexBenchmark4initEv
	.quad	_ZNK16ComplexBenchmark7c_styleEv
	.quad	_ZNK16ComplexBenchmark9oop_styleEv
	.quad	_ZNK16ComplexBenchmark5checkEiRdS0_
	.size	_ZTV16ComplexBenchmark, 56

	.type	_ZTS16ComplexBenchmark,@object # @_ZTS16ComplexBenchmark
	.globl	_ZTS16ComplexBenchmark
	.p2align	4
_ZTS16ComplexBenchmark:
	.asciz	"16ComplexBenchmark"
	.size	_ZTS16ComplexBenchmark, 19

	.type	_ZTI16ComplexBenchmark,@object # @_ZTI16ComplexBenchmark
	.globl	_ZTI16ComplexBenchmark
	.p2align	4
_ZTI16ComplexBenchmark:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16ComplexBenchmark
	.quad	_ZTI9Benchmark
	.size	_ZTI16ComplexBenchmark, 24

	.type	.L.str.28,@object       # @.str.28
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.28:
	.asciz	"Max"
	.size	.L.str.28, 4

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Matrix"
	.size	.L.str.29, 7

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Iterator"
	.size	.L.str.30, 9

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Complex"
	.size	.L.str.31, 8

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_oopack_v1p8.ii
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"E.g.:\ta.out  Max=5000 Matrix=50 Complex=2000  Iterator=5000"
	.size	.Lstr, 60

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"\nDONE!"
	.size	.Lstr.1, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
