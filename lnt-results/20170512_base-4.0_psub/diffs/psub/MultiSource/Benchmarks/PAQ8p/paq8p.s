	.text
	.file	"paq8p.bc"
	.globl	_Z4quitPKc
	.p2align	4, 0x90
	.type	_Z4quitPKc,@function
_Z4quitPKc:                             # @_Z4quitPKc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end0:
	.size	_Z4quitPKc, .Lfunc_end0-_Z4quitPKc
	.cfi_endproc

	.globl	_Z6equalsPKcS0_
	.p2align	4, 0x90
	.type	_Z6equalsPKcS0_,@function
_Z6equalsPKcS0_:                        # @_Z6equalsPKcS0_
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movsbl	(%rdi), %edx
	testl	%edx, %edx
	movb	(%rsi), %cl
	je	.LBB1_4
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	xorl	%eax, %eax
	testb	%cl, %cl
	je	.LBB1_3
# BB#6:                                 #   in Loop: Header=BB1_1 Depth=1
	movsbl	%cl, %r8d
	movl	%edx, %r9d
	addb	$-65, %r9b
	leal	32(%rdx), %r10d
	cmpb	$26, %r9b
	cmovael	%edx, %r10d
	addb	$-65, %cl
	leal	32(%r8), %edx
	cmpb	$26, %cl
	cmovael	%r8d, %edx
	incq	%rdi
	incq	%rsi
	cmpl	%edx, %r10d
	je	.LBB1_1
	jmp	.LBB1_5
.LBB1_3:
	xorl	%ecx, %ecx
.LBB1_4:                                # %.critedge
	xorl	%eax, %eax
	cmpb	%cl, %dl
	sete	%al
.LBB1_5:                                # %.loopexit
	retq
.Lfunc_end1:
	.size	_Z6equalsPKcS0_, .Lfunc_end1-_Z6equalsPKcS0_
	.cfi_endproc

	.section	.text._ZN6RandomD2Ev,"axG",@progbits,_ZN6RandomD2Ev,comdat
	.weak	_ZN6RandomD2Ev
	.p2align	4, 0x90
	.type	_ZN6RandomD2Ev,@function
_ZN6RandomD2Ev:                         # @_ZN6RandomD2Ev
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB2_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB2_2:                                # %_ZN5ArrayIjLi0EED2Ev.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	_ZN6RandomD2Ev, .Lfunc_end2-_ZN6RandomD2Ev
	.cfi_endproc

	.section	.text._ZN3BufD2Ev,"axG",@progbits,_ZN3BufD2Ev,comdat
	.weak	_ZN3BufD2Ev
	.p2align	4, 0x90
	.type	_ZN3BufD2Ev,@function
_ZN3BufD2Ev:                            # @_ZN3BufD2Ev
	.cfi_startproc
# BB#0:
	movl	programChecker(%rip), %eax
	subl	(%rdi), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB3_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB3_2:                                # %_ZN5ArrayIhLi0EED2Ev.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	_ZN3BufD2Ev, .Lfunc_end3-_ZN3BufD2Ev
	.cfi_endproc

	.section	.text._ZN4IlogD2Ev,"axG",@progbits,_ZN4IlogD2Ev,comdat
	.weak	_ZN4IlogD2Ev
	.p2align	4, 0x90
	.type	_ZN4IlogD2Ev,@function
_ZN4IlogD2Ev:                           # @_ZN4IlogD2Ev
	.cfi_startproc
# BB#0:
	movl	programChecker(%rip), %eax
	subl	(%rdi), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB4_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB4_2:                                # %_ZN5ArrayIhLi0EED2Ev.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end4:
	.size	_ZN4IlogD2Ev, .Lfunc_end4-_ZN4IlogD2Ev
	.cfi_endproc

	.text
	.globl	_ZN4IlogC2Ev
	.p2align	4, 0x90
	.type	_ZN4IlogC2Ev,@function
_ZN4IlogC2Ev:                           # @_ZN4IlogC2Ev
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, (%r14)
	movl	$65536, %eax            # imm = 0x10000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB5_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB5_2:                                # %_ZN14ProgramChecker5allocEi.exit.i.i
	movl	$65536, %edi            # imm = 0x10000
	movl	$1, %esi
	callq	calloc
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.LBB5_6
# BB#3:                                 # %_ZN5ArrayIhLi0EEC2Ei.exit
	movq	%rax, 16(%r14)
	movb	$16, 2(%rax)
	movl	$272336110, %ecx        # imm = 0x103B84EE
	movl	$7, %r8d
	xorl	%ebx, %ebx
	jmp	.LBB5_4
	.p2align	4, 0x90
.LBB5_5:                                # %._crit_edge.1
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	16(%r14), %rdi
	movl	$774541002, %eax        # imm = 0x2E2A8ECA
	xorl	%edx, %edx
	idivl	%r8d
	movl	%eax, %ecx
	addl	%esi, %ecx
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 4(%rdi,%rbx)
	addq	$2, %rbx
	addl	$4, %r8d
.LBB5_4:                                # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rdi
	leal	-2(%r8), %esi
	movl	$774541002, %eax        # imm = 0x2E2A8ECA
	xorl	%edx, %edx
	idivl	%esi
	movl	%eax, %esi
	addl	%ecx, %esi
	movl	%esi, %eax
	shrl	$24, %eax
	movb	%al, 3(%rdi,%rbx)
	cmpq	$65532, %rbx            # imm = 0xFFFC
	jne	.LBB5_5
# BB#7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_6:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Lfunc_end5:
	.size	_ZN4IlogC2Ev, .Lfunc_end5-_ZN4IlogC2Ev
	.cfi_endproc

	.section	.text._ZN5ArrayIhLi0EED2Ev,"axG",@progbits,_ZN5ArrayIhLi0EED2Ev,comdat
	.weak	_ZN5ArrayIhLi0EED2Ev
	.p2align	4, 0x90
	.type	_ZN5ArrayIhLi0EED2Ev,@function
_ZN5ArrayIhLi0EED2Ev:                   # @_ZN5ArrayIhLi0EED2Ev
	.cfi_startproc
# BB#0:
	movl	programChecker(%rip), %eax
	subl	(%rdi), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB6_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB6_2:                                # %_ZN14ProgramChecker5allocEi.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end6:
	.size	_ZN5ArrayIhLi0EED2Ev, .Lfunc_end6-_ZN5ArrayIhLi0EED2Ev
	.cfi_endproc

	.text
	.globl	_Z6squashi
	.p2align	4, 0x90
	.type	_Z6squashi,@function
_Z6squashi:                             # @_Z6squashi
	.cfi_startproc
# BB#0:
	movl	$4095, %eax             # imm = 0xFFF
	cmpl	$2047, %edi             # imm = 0x7FF
	jg	.LBB7_3
# BB#1:
	xorl	%eax, %eax
	cmpl	$-2047, %edi            # imm = 0xF801
	jl	.LBB7_3
# BB#2:
	movl	%edi, %eax
	andl	$127, %eax
	sarl	$7, %edi
	movl	$128, %ecx
	subl	%eax, %ecx
	movslq	%edi, %rdx
	imull	_ZZ6squashiE1t+64(,%rdx,4), %ecx
	imull	_ZZ6squashiE1t+68(,%rdx,4), %eax
	leal	64(%rcx,%rax), %eax
	sarl	$7, %eax
.LBB7_3:
	retq
.Lfunc_end7:
	.size	_Z6squashi, .Lfunc_end7-_Z6squashi
	.cfi_endproc

	.section	.text._ZN7StretchD2Ev,"axG",@progbits,_ZN7StretchD2Ev,comdat
	.weak	_ZN7StretchD2Ev
	.p2align	4, 0x90
	.type	_ZN7StretchD2Ev,@function
_ZN7StretchD2Ev:                        # @_ZN7StretchD2Ev
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	addl	%ecx, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB8_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB8_2:                                # %_ZN5ArrayIsLi0EED2Ev.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end8:
	.size	_ZN7StretchD2Ev, .Lfunc_end8-_ZN7StretchD2Ev
	.cfi_endproc

	.text
	.globl	_ZN7StretchC2Ev
	.p2align	4, 0x90
	.type	_ZN7StretchC2Ev,@function
_ZN7StretchC2Ev:                        # @_ZN7StretchC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -48
.Lcfi13:
	.cfi_offset %r12, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movabsq	$17592186048512, %rax   # imm = 0x100000001000
	movq	%rax, (%rbx)
	movl	$8192, %eax             # imm = 0x2000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB9_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB9_2:                                # %_ZN14ProgramChecker5allocEi.exit.i.i
	movl	$8192, %edi             # imm = 0x2000
	movl	$1, %esi
	callq	calloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB9_20
# BB#3:                                 # %_ZN5ArrayIsLi0EEC2Ei.exit
	movq	%rax, 16(%rbx)
	leaq	16(%rax), %r8
	movq	%rax, %r9
	addq	$112, %r9
	movl	$-2047, %esi            # imm = 0xF801
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_4:                                # %_Z6squashi.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_14 Depth 2
                                        #     Child Loop BB9_17 Depth 2
                                        #     Child Loop BB9_7 Depth 2
	movl	%esi, %edx
	andl	$127, %edx
	movl	%esi, %edi
	sarl	$7, %edi
	movl	$128, %ebp
	subl	%edx, %ebp
	movslq	%edi, %rdi
	imull	_ZZ6squashiE1t+64(,%rdi,4), %ebp
	imull	_ZZ6squashiE1t+68(,%rdi,4), %edx
	leal	64(%rbp,%rdx), %r12d
	sarl	$7, %r12d
	cmpl	%r12d, %ecx
	jg	.LBB9_8
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB9_4 Depth=1
	movslq	%ecx, %rbx
	movslq	%r12d, %rcx
	cmpq	%rcx, %rbx
	movq	%rcx, %r15
	cmovgeq	%rbx, %r15
	incq	%r15
	subq	%rbx, %r15
	cmpq	$15, %r15
	jbe	.LBB9_6
# BB#10:                                # %min.iters.checked
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	%r15, %r10
	andq	$-16, %r10
	movq	%r15, %r11
	andq	$-16, %r11
	je	.LBB9_6
# BB#11:                                # %vector.ph
                                        #   in Loop: Header=BB9_4 Depth=1
	movd	%esi, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	-16(%r11), %r14
	movl	%r14d, %edx
	shrl	$4, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB9_12
# BB#13:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB9_4 Depth=1
	leaq	(%r8,%rbx,2), %rbp
	negq	%rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB9_14:                               # %vector.body.prol
                                        #   Parent Loop BB9_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rbp,%rdi,2)
	movdqu	%xmm0, (%rbp,%rdi,2)
	addq	$16, %rdi
	incq	%rdx
	jne	.LBB9_14
	jmp	.LBB9_15
.LBB9_12:                               #   in Loop: Header=BB9_4 Depth=1
	xorl	%edi, %edi
.LBB9_15:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB9_4 Depth=1
	cmpq	$48, %r14
	jb	.LBB9_18
# BB#16:                                # %vector.ph.new
                                        #   in Loop: Header=BB9_4 Depth=1
	movq	%r11, %rdx
	subq	%rdi, %rdx
	addq	%rbx, %rdi
	leaq	(%r9,%rdi,2), %rdi
	.p2align	4, 0x90
.LBB9_17:                               # %vector.body
                                        #   Parent Loop BB9_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	subq	$-128, %rdi
	addq	$-64, %rdx
	jne	.LBB9_17
.LBB9_18:                               # %middle.block
                                        #   in Loop: Header=BB9_4 Depth=1
	cmpq	%r11, %r15
	je	.LBB9_8
# BB#19:                                #   in Loop: Header=BB9_4 Depth=1
	addq	%r10, %rbx
	.p2align	4, 0x90
.LBB9_6:                                # %scalar.ph.preheader
                                        #   in Loop: Header=BB9_4 Depth=1
	decq	%rbx
	.p2align	4, 0x90
.LBB9_7:                                # %scalar.ph
                                        #   Parent Loop BB9_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	%si, 2(%rax,%rbx,2)
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB9_7
.LBB9_8:                                # %._crit_edge
                                        #   in Loop: Header=BB9_4 Depth=1
	incl	%r12d
	incl	%esi
	cmpl	$2048, %esi             # imm = 0x800
	movl	%r12d, %ecx
	jne	.LBB9_4
# BB#9:
	movw	$2047, 8190(%rax)       # imm = 0x7FF
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_20:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Lfunc_end9:
	.size	_ZN7StretchC2Ev, .Lfunc_end9-_ZN7StretchC2Ev
	.cfi_endproc

	.globl	_Z11dot_productPsS_i
	.p2align	4, 0x90
	.type	_Z11dot_productPsS_i,@function
_Z11dot_productPsS_i:                   # @_Z11dot_productPsS_i
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	addl	$7, %edx
	andl	$-8, %edx
	testl	%edx, %edx
	jle	.LBB10_1
# BB#2:                                 # %.lr.ph.preheader
	movslq	%edx, %r10
	leaq	-1(%r10), %r8
	shrq	%r8
	incq	%r8
	cmpq	$7, %r8
	jbe	.LBB10_3
# BB#4:                                 # %min.iters.checked
	movq	%r8, %r14
	andq	$-8, %r14
	movq	%r8, %r9
	andq	$-8, %r9
	je	.LBB10_3
# BB#5:                                 # %vector.body.preheader
	addq	%r14, %r14
	leaq	16(%rdi), %rax
	leaq	16(%rsi), %rcx
	pxor	%xmm0, %xmm0
	movq	%r9, %r11
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB10_6:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rax), %xmm6
	movdqu	(%rax), %xmm9
	movdqu	-16(%rcx), %xmm7
	movdqu	(%rcx), %xmm8
	pshuflw	$232, %xmm6, %xmm2      # xmm2 = xmm6[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm5      # xmm5 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm7, %xmm2      # xmm2 = xmm7[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm3      # xmm3 = xmm2[0,2,2,3]
	movdqa	%xmm3, %xmm2
	pmulhw	%xmm5, %xmm2
	pmullw	%xmm5, %xmm3
	punpcklwd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3]
	pshuflw	$232, %xmm9, %xmm2      # xmm2 = xmm9[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm8, %xmm5      # xmm5 = xmm8[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	movdqa	%xmm4, %xmm5
	pmulhw	%xmm2, %xmm5
	pmullw	%xmm2, %xmm4
	punpcklwd	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1],xmm4[2],xmm5[2],xmm4[3],xmm5[3]
	pshuflw	$231, %xmm6, %xmm2      # xmm2 = xmm6[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$177, %xmm2, %xmm2      # xmm2 = xmm2[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm7, %xmm5      # xmm5 = xmm7[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm5, %xmm5      # xmm5 = xmm5[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshuflw	$177, %xmm5, %xmm5      # xmm5 = xmm5[1,0,3,2,4,5,6,7]
	movdqa	%xmm5, %xmm6
	pmulhw	%xmm2, %xmm6
	pmullw	%xmm2, %xmm5
	punpcklwd	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1],xmm5[2],xmm6[2],xmm5[3],xmm6[3]
	pshuflw	$231, %xmm9, %xmm2      # xmm2 = xmm9[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$177, %xmm2, %xmm2      # xmm2 = xmm2[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm8, %xmm6      # xmm6 = xmm8[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm6, %xmm6      # xmm6 = xmm6[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshuflw	$177, %xmm6, %xmm6      # xmm6 = xmm6[1,0,3,2,4,5,6,7]
	movdqa	%xmm6, %xmm7
	pmulhw	%xmm2, %xmm7
	pmullw	%xmm2, %xmm6
	punpcklwd	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1],xmm6[2],xmm7[2],xmm6[3],xmm7[3]
	paddd	%xmm3, %xmm5
	paddd	%xmm4, %xmm6
	psrad	$8, %xmm5
	psrad	$8, %xmm6
	paddd	%xmm5, %xmm0
	paddd	%xmm6, %xmm1
	addq	$32, %rax
	addq	$32, %rcx
	addq	$-8, %r11
	jne	.LBB10_6
# BB#7:                                 # %middle.block
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %eax
	cmpq	%r9, %r8
	jne	.LBB10_8
	jmp	.LBB10_9
.LBB10_3:
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB10_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rdi,%r14,2), %ecx
	movswl	(%rsi,%r14,2), %edx
	imull	%ecx, %edx
	movswl	2(%rdi,%r14,2), %ecx
	movswl	2(%rsi,%r14,2), %ebx
	imull	%ecx, %ebx
	addl	%edx, %ebx
	sarl	$8, %ebx
	addl	%ebx, %eax
	addq	$2, %r14
	cmpq	%r10, %r14
	jl	.LBB10_8
	jmp	.LBB10_9
.LBB10_1:
	xorl	%eax, %eax
.LBB10_9:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_Z11dot_productPsS_i, .Lfunc_end10-_Z11dot_productPsS_i
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI11_1:
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
.LCPI11_2:
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.text
	.globl	_Z5trainPsS_ii
	.p2align	4, 0x90
	.type	_Z5trainPsS_ii,@function
_Z5trainPsS_ii:                         # @_Z5trainPsS_ii
	.cfi_startproc
# BB#0:
	addl	$7, %edx
	andl	$-8, %edx
	testl	%edx, %edx
	jle	.LBB11_6
# BB#1:                                 # %.lr.ph.preheader
	movl	%edx, %r8d
	cmpl	$7, %edx
	jbe	.LBB11_7
# BB#2:                                 # %vector.memcheck
	leaq	(%rdi,%r8,2), %rdx
	cmpq	%rsi, %rdx
	jbe	.LBB11_4
# BB#3:                                 # %vector.memcheck
	leaq	(%rsi,%r8,2), %rdx
	cmpq	%rdi, %rdx
	jbe	.LBB11_4
	.p2align	4, 0x90
.LBB11_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rsi), %edx
	movswl	(%rdi), %eax
	imull	%ecx, %eax
	sarl	$15, %eax
	incl	%eax
	sarl	%eax
	addl	%edx, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	movl	$-32768, %edx           # imm = 0x8000
	cmovgl	%eax, %edx
	cmpl	$32767, %edx            # imm = 0x7FFF
	movw	$32767, %ax             # imm = 0x7FFF
	cmovlw	%dx, %ax
	movw	%ax, (%rsi)
	addq	$2, %rsi
	addq	$2, %rdi
	decq	%r8
	jne	.LBB11_7
	jmp	.LBB11_6
.LBB11_4:                               # %vector.ph
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	pshufd	$245, %xmm0, %xmm9      # xmm9 = xmm0[1,1,3,3]
	movdqa	.LCPI11_0(%rip), %xmm8  # xmm8 = [1,1,1,1]
	movdqa	.LCPI11_1(%rip), %xmm3  # xmm3 = [4294934528,4294934528,4294934528,4294934528]
	movdqa	.LCPI11_2(%rip), %xmm4  # xmm4 = [32767,32767,32767,32767]
	.p2align	4, 0x90
.LBB11_5:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsi), %xmm2          # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1],xmm5[2],xmm2[2],xmm5[3],xmm2[3]
	psrad	$16, %xmm5
	movq	(%rsi), %xmm2           # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1],xmm6[2],xmm2[2],xmm6[3],xmm2[3]
	psrad	$16, %xmm6
	movq	8(%rdi), %xmm2          # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	(%rdi), %xmm7           # xmm7 = mem[0],zero
	punpcklwd	%xmm7, %xmm7    # xmm7 = xmm7[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm7
	pshufd	$245, %xmm7, %xmm1      # xmm1 = xmm7[1,1,3,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm9, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1]
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm9, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	psrad	$15, %xmm2
	psrad	$15, %xmm7
	paddd	%xmm8, %xmm7
	paddd	%xmm8, %xmm2
	psrad	$1, %xmm2
	psrad	$1, %xmm7
	paddd	%xmm6, %xmm7
	paddd	%xmm5, %xmm2
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm3, %xmm1
	movdqa	%xmm7, %xmm5
	pcmpgtd	%xmm3, %xmm5
	pand	%xmm5, %xmm7
	pandn	%xmm3, %xmm5
	por	%xmm7, %xmm5
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	por	%xmm2, %xmm1
	movdqa	%xmm4, %xmm2
	pcmpgtd	%xmm1, %xmm2
	movdqa	%xmm4, %xmm6
	pcmpgtd	%xmm5, %xmm6
	pand	%xmm6, %xmm5
	pandn	%xmm4, %xmm6
	por	%xmm5, %xmm6
	pand	%xmm2, %xmm1
	pandn	%xmm4, %xmm2
	por	%xmm1, %xmm2
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm6
	psrad	$16, %xmm6
	packssdw	%xmm2, %xmm6
	movdqu	%xmm6, (%rsi)
	addq	$16, %rsi
	addq	$16, %rdi
	addq	$-8, %r8
	jne	.LBB11_5
.LBB11_6:                               # %._crit_edge
	retq
.Lfunc_end11:
	.size	_Z5trainPsS_ii, .Lfunc_end11-_Z5trainPsS_ii
	.cfi_endproc

	.globl	_ZN5MixerD2Ev
	.p2align	4, 0x90
	.type	_ZN5MixerD2Ev,@function
_ZN5MixerD2Ev:                          # @_ZN5MixerD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	128(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB12_3
# BB#1:
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN5MixerD2Ev
.Ltmp1:
# BB#2:
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB12_3:
	movl	104(%r15), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB12_5
# BB#4:
	movl	%eax, programChecker+4(%rip)
.LBB12_5:                               # %_ZN5ArrayIiLi0EED2Ev.exit
	movq	112(%r15), %rdi
	callq	free
	movl	64(%r15), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB12_7
# BB#6:
	movl	%eax, programChecker+4(%rip)
.LBB12_7:                               # %_ZN5ArrayIiLi0EED2Ev.exit2
	movq	72(%r15), %rdi
	callq	free
	movl	40(%r15), %ecx
	addl	%ecx, %ecx
	movl	$-16, %ebx
	movl	$-16, %eax
	subl	%ecx, %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB12_9
# BB#8:
	movl	%eax, programChecker+4(%rip)
.LBB12_9:                               # %_ZN5ArrayIsLi16EED2Ev.exit3
	movq	48(%r15), %rdi
	callq	free
	movl	16(%r15), %eax
	addl	%eax, %eax
	subl	%eax, %ebx
	addl	programChecker(%rip), %ebx
	movl	%ebx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ebx
	jle	.LBB12_11
# BB#10:
	movl	%ebx, programChecker+4(%rip)
.LBB12_11:                              # %_ZN5ArrayIsLi16EED2Ev.exit4
	movq	24(%r15), %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB12_12:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movl	104(%r15), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB12_14
# BB#13:
	movl	%eax, programChecker+4(%rip)
.LBB12_14:                              # %_ZN5ArrayIiLi0EED2Ev.exit5
	movq	112(%r15), %rdi
	callq	free
	movl	64(%r15), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB12_16
# BB#15:
	movl	%eax, programChecker+4(%rip)
.LBB12_16:                              # %_ZN5ArrayIiLi0EED2Ev.exit6
	movq	72(%r15), %rdi
	callq	free
	movl	40(%r15), %ecx
	addl	%ecx, %ecx
	movl	$-16, %ebx
	movl	$-16, %eax
	subl	%ecx, %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB12_18
# BB#17:
	movl	%eax, programChecker+4(%rip)
.LBB12_18:                              # %_ZN5ArrayIsLi16EED2Ev.exit7
	movq	48(%r15), %rdi
	callq	free
	movl	16(%r15), %eax
	addl	%eax, %eax
	subl	%eax, %ebx
	addl	programChecker(%rip), %ebx
	movl	%ebx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ebx
	jle	.LBB12_20
# BB#19:
	movl	%ebx, programChecker+4(%rip)
.LBB12_20:                              # %_ZN5ArrayIsLi16EED2Ev.exit
	movq	24(%r15), %rdi
	callq	free
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN5MixerD2Ev, .Lfunc_end12-_ZN5MixerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp1     #   Call between .Ltmp1 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN5ArrayIiLi0EED2Ev,"axG",@progbits,_ZN5ArrayIiLi0EED2Ev,comdat
	.weak	_ZN5ArrayIiLi0EED2Ev
	.p2align	4, 0x90
	.type	_ZN5ArrayIiLi0EED2Ev,@function
_ZN5ArrayIiLi0EED2Ev:                   # @_ZN5ArrayIiLi0EED2Ev
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB13_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB13_2:                               # %_ZN14ProgramChecker5allocEi.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end13:
	.size	_ZN5ArrayIiLi0EED2Ev, .Lfunc_end13-_ZN5ArrayIiLi0EED2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.long	2048                    # 0x800
	.long	2048                    # 0x800
	.long	2048                    # 0x800
	.long	2048                    # 0x800
	.text
	.globl	_ZN5MixerC2Eiiii
	.p2align	4, 0x90
	.type	_ZN5MixerC2Eiiii,@function
_ZN5MixerC2Eiiii:                       # @_ZN5MixerC2Eiiii
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 64
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movl	%r8d, %r12d
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movl	%esi, %ebp
	movq	%rdi, %r13
	addl	$7, %ebp
	andl	$-8, %ebp
	movl	%ebp, (%r13)
	movl	%r15d, 4(%r13)
	movl	%r14d, 8(%r13)
	movl	%ebp, 20(%r13)
	movl	%ebp, 16(%r13)
	testl	%ebp, %ebp
	jle	.LBB14_1
# BB#2:
	leal	16(%rbp,%rbp), %eax
	movl	programChecker(%rip), %ecx
	addl	%eax, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB14_4
# BB#3:
	movl	%ecx, programChecker+4(%rip)
.LBB14_4:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	je	.LBB14_77
# BB#5:
	movl	%eax, %ecx
	andl	$15, %ecx
	negq	%rcx
	leaq	16(%rax,%rcx), %rax
	movq	%rax, 32(%r13)
	jmp	.LBB14_6
.LBB14_1:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%r13)
.LBB14_6:                               # %_ZN5ArrayIsLi16EEC2Ei.exit
	movl	%ebp, %eax
	imull	%r15d, %eax
	movl	%eax, 44(%r13)
	movl	%eax, 40(%r13)
	testl	%eax, %eax
	jle	.LBB14_7
# BB#8:
	leal	16(%rax,%rax), %eax
	movl	programChecker(%rip), %ecx
	addl	%eax, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB14_10
# BB#9:
	movl	%ecx, programChecker+4(%rip)
.LBB14_10:                              # %_ZN14ProgramChecker5allocEi.exit.i.i20
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 48(%r13)
	testq	%rax, %rax
	je	.LBB14_11
# BB#13:
	movl	%eax, %ecx
	andl	$15, %ecx
	negq	%rcx
	leaq	16(%rax,%rcx), %rbx
	movq	%rbx, 56(%r13)
	jmp	.LBB14_14
.LBB14_7:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 48(%r13)
	xorl	%ebx, %ebx
.LBB14_14:                              # %_ZN5ArrayIsLi16EEC2Ei.exit21
	movl	%r14d, 68(%r13)
	movl	%r14d, 64(%r13)
	testl	%r14d, %r14d
	jle	.LBB14_15
# BB#16:
	leal	(,%r14,4), %eax
	movl	programChecker(%rip), %ecx
	addl	%eax, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB14_18
# BB#17:
	movl	%ecx, programChecker+4(%rip)
.LBB14_18:                              # %_ZN14ProgramChecker5allocEi.exit.i.i22
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 72(%r13)
	testq	%rax, %rax
	je	.LBB14_19
# BB#21:
	movq	%rax, 80(%r13)
	jmp	.LBB14_22
.LBB14_15:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 72(%r13)
.LBB14_22:                              # %_ZN5ArrayIiLi0EEC2Ei.exit
	testl	%r14d, %r14d
	movl	$0, 88(%r13)
	movl	$0, 92(%r13)
	movl	$0, 96(%r13)
	movl	%r14d, 108(%r13)
	movl	%r14d, 104(%r13)
	jle	.LBB14_28
# BB#23:
	shll	$2, %r14d
	movl	programChecker(%rip), %eax
	addl	%r14d, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB14_25
# BB#24:
	movl	%eax, programChecker+4(%rip)
.LBB14_25:                              # %_ZN14ProgramChecker5allocEi.exit.i.i24
	movslq	%r14d, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rcx
	movq	%rcx, 112(%r13)
	testq	%rcx, %rcx
	je	.LBB14_26
# BB#29:                                # %.lr.ph31
	movq	%rcx, 120(%r13)
	movq	$0, 128(%r13)
	movl	8(%r13), %r14d
	movslq	%r14d, %rsi
	testq	%rsi, %rsi
	movl	$1, %eax
	cmovgq	%rsi, %rax
	cmpq	$7, %rax
	jbe	.LBB14_30
# BB#32:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rdi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rax, %rdi
	je	.LBB14_30
# BB#33:                                # %vector.body.preheader
	leaq	-8(%rdi), %r8
	movl	%r8d, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB14_34
# BB#35:                                # %vector.body.prol.preheader
	negq	%rdx
	xorl	%ebp, %ebp
	movdqa	.LCPI14_0(%rip), %xmm0  # xmm0 = [2048,2048,2048,2048]
	.p2align	4, 0x90
.LBB14_36:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rcx,%rbp,4)
	movdqu	%xmm0, 16(%rcx,%rbp,4)
	addq	$8, %rbp
	incq	%rdx
	jne	.LBB14_36
	jmp	.LBB14_37
.LBB14_30:
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB14_31:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$2048, (%rcx,%rdi,4)    # imm = 0x800
	incq	%rdi
	cmpq	%rsi, %rdi
	jl	.LBB14_31
.LBB14_41:                              # %.preheader.loopexit
	movq	%r13, %r8
	subq	$-128, %r8
	movl	(%r13), %ebp
	movl	4(%r13), %r15d
	jmp	.LBB14_42
.LBB14_28:                              # %_ZN5ArrayIiLi0EEC2Ei.exit26
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 112(%r13)
	movq	%r13, %r8
	subq	$-128, %r8
	movq	$0, 128(%r13)
.LBB14_42:                              # %.preheader
	imull	%ebp, %r15d
	testl	%r15d, %r15d
	jle	.LBB14_55
# BB#43:                                # %.lr.ph
	movslq	%r15d, %rax
	cmpl	$15, %r15d
	jbe	.LBB14_44
# BB#45:                                # %min.iters.checked40
	movq	%rax, %rcx
	andq	$-16, %rcx
	je	.LBB14_44
# BB#46:                                # %vector.ph44
	movd	%r12d, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	-16(%rcx), %rdx
	movl	%edx, %edi
	shrl	$4, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB14_47
# BB#48:                                # %vector.body36.prol.preheader
	negq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB14_49:                              # %vector.body36.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rsi,2)
	movdqu	%xmm0, 16(%rbx,%rsi,2)
	addq	$16, %rsi
	incq	%rdi
	jne	.LBB14_49
	jmp	.LBB14_50
.LBB14_44:
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB14_54:                              # %scalar.ph38
                                        # =>This Inner Loop Header: Depth=1
	movw	%r12w, (%rbx,%rcx,2)
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB14_54
.LBB14_55:                              # %._crit_edge
	cmpl	$2, %r14d
	jl	.LBB14_59
# BB#56:
.Ltmp12:
	movq	%r8, %rbx
	movl	$136, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp13:
# BB#57:
.Ltmp15:
	movl	$1, %edx
	movl	$1, %ecx
	movl	$32767, %r8d            # imm = 0x7FFF
	movq	%rbp, %rdi
	movl	%r14d, %esi
	callq	_ZN5MixerC2Eiiii
.Ltmp16:
# BB#58:
	movq	%rbp, (%rbx)
.LBB14_59:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_47:
	xorl	%esi, %esi
.LBB14_50:                              # %vector.body36.prol.loopexit
	cmpq	$112, %rdx
	jb	.LBB14_53
# BB#51:                                # %vector.ph44.new
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	leaq	240(%rbx,%rsi,2), %rsi
	.p2align	4, 0x90
.LBB14_52:                              # %vector.body36
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rsi)
	movdqu	%xmm0, -224(%rsi)
	movdqu	%xmm0, -208(%rsi)
	movdqu	%xmm0, -192(%rsi)
	movdqu	%xmm0, -176(%rsi)
	movdqu	%xmm0, -160(%rsi)
	movdqu	%xmm0, -144(%rsi)
	movdqu	%xmm0, -128(%rsi)
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-128, %rdx
	jne	.LBB14_52
.LBB14_53:                              # %middle.block37
	cmpq	%rcx, %rax
	jne	.LBB14_54
	jmp	.LBB14_55
.LBB14_34:
	xorl	%ebp, %ebp
.LBB14_37:                              # %vector.body.prol.loopexit
	cmpq	$56, %r8
	jb	.LBB14_40
# BB#38:                                # %vector.body.preheader.new
	movq	%rdi, %rdx
	subq	%rbp, %rdx
	leaq	240(%rcx,%rbp,4), %rbp
	movdqa	.LCPI14_0(%rip), %xmm0  # xmm0 = [2048,2048,2048,2048]
	.p2align	4, 0x90
.LBB14_39:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rbp)
	movdqu	%xmm0, -224(%rbp)
	movdqu	%xmm0, -208(%rbp)
	movdqu	%xmm0, -192(%rbp)
	movdqu	%xmm0, -176(%rbp)
	movdqu	%xmm0, -160(%rbp)
	movdqu	%xmm0, -144(%rbp)
	movdqu	%xmm0, -128(%rbp)
	movdqu	%xmm0, -112(%rbp)
	movdqu	%xmm0, -96(%rbp)
	movdqu	%xmm0, -80(%rbp)
	movdqu	%xmm0, -64(%rbp)
	movdqu	%xmm0, -48(%rbp)
	movdqu	%xmm0, -32(%rbp)
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$256, %rbp              # imm = 0x100
	addq	$-64, %rdx
	jne	.LBB14_39
.LBB14_40:                              # %middle.block
	cmpq	%rdi, %rax
	jne	.LBB14_31
	jmp	.LBB14_41
.LBB14_77:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.LBB14_11:
.Ltmp3:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp4:
# BB#12:                                # %.noexc
.LBB14_19:
.Ltmp6:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp7:
# BB#20:                                # %.noexc23
.LBB14_26:
.Ltmp9:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp10:
# BB#27:                                # %.noexc25
.LBB14_62:
.Ltmp11:
	movq	%rax, %r14
	jmp	.LBB14_68
.LBB14_61:
.Ltmp8:
	movq	%rax, %r14
	jmp	.LBB14_71
.LBB14_60:
.Ltmp5:
	movq	%rax, %r14
	jmp	.LBB14_74
.LBB14_63:
.Ltmp17:
	movq	%rax, %r14
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB14_65
.LBB14_64:
.Ltmp14:
	movq	%rax, %r14
.LBB14_65:
	movl	104(%r13), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB14_67
# BB#66:
	movl	%eax, programChecker+4(%rip)
.LBB14_67:                              # %_ZN5ArrayIiLi0EED2Ev.exit
	movq	112(%r13), %rdi
	callq	free
.LBB14_68:
	movl	64(%r13), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB14_70
# BB#69:
	movl	%eax, programChecker+4(%rip)
.LBB14_70:                              # %_ZN5ArrayIiLi0EED2Ev.exit27
	movq	72(%r13), %rdi
	callq	free
.LBB14_71:
	movl	40(%r13), %ecx
	addl	%ecx, %ecx
	movl	$-16, %eax
	subl	%ecx, %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB14_73
# BB#72:
	movl	%eax, programChecker+4(%rip)
.LBB14_73:                              # %_ZN5ArrayIsLi16EED2Ev.exit28
	movq	48(%r13), %rdi
	callq	free
.LBB14_74:
	movl	16(%r13), %ecx
	addl	%ecx, %ecx
	movl	$-16, %eax
	subl	%ecx, %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB14_76
# BB#75:
	movl	%eax, programChecker+4(%rip)
.LBB14_76:                              # %_ZN5ArrayIsLi16EED2Ev.exit
	movq	24(%r13), %rdi
	callq	free
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN5MixerC2Eiiii, .Lfunc_end14-_ZN5MixerC2Eiiii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp16          #   Call between .Ltmp16 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 5 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 6 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Lfunc_end14-.Ltmp10    #   Call between .Ltmp10 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN4APM1C2Ei
	.p2align	4, 0x90
	.type	_ZN4APM1C2Ei,@function
_ZN4APM1C2Ei:                           # @_ZN4APM1C2Ei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$0, (%rbx)
	movl	%ebp, 4(%rbx)
	movl	%ebp, %eax
	shll	$5, %eax
	addl	%ebp, %eax
	movl	%eax, 12(%rbx)
	movl	%eax, 8(%rbx)
	testl	%ebp, %ebp
	jle	.LBB15_21
# BB#1:
	imull	$66, %ebp, %eax
	movl	programChecker(%rip), %ecx
	addl	%eax, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB15_3
# BB#2:
	movl	%ecx, programChecker+4(%rip)
.LBB15_3:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB15_23
# BB#4:                                 # %.preheader.lr.ph
	movq	%rax, 24(%rbx)
	movslq	%ebp, %r9
	leaq	66(%rax), %r8
	xorl	%r10d, %r10d
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB15_5:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_15 Depth 2
                                        #     Child Loop BB15_18 Depth 2
                                        #     Child Loop BB15_7 Depth 2
	testq	%r10, %r10
	je	.LBB15_6
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB15_5 Depth=1
	imulq	$66, %r10, %rcx
	leaq	(%rax,%rcx), %rdx
	cmpq	%r8, %rdx
	jae	.LBB15_20
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB15_5 Depth=1
	leaq	66(%rax,%rcx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB15_20
# BB#13:                                #   in Loop: Header=BB15_5 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB15_14
	.p2align	4, 0x90
.LBB15_6:                               # %.preheader.split.us.preheader
                                        #   in Loop: Header=BB15_5 Depth=1
	movq	$-2048, %rbp            # imm = 0xF800
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB15_7:                               # %.preheader.split.us
                                        #   Parent Loop BB15_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$4095, %edx             # imm = 0xFFF
	cmpq	$2047, %rbp             # imm = 0x7FF
	jg	.LBB15_10
# BB#8:                                 #   in Loop: Header=BB15_7 Depth=2
	xorl	%edx, %edx
	cmpq	$-2047, %rbp            # imm = 0xF801
	jl	.LBB15_10
# BB#9:                                 #   in Loop: Header=BB15_7 Depth=2
	movl	%ebp, %ecx
	sarl	$7, %ecx
	movslq	%ecx, %rcx
	movl	_ZZ6squashiE1t+64(,%rcx,4), %edx
.LBB15_10:                              # %_Z6squashi.exit.us
                                        #   in Loop: Header=BB15_7 Depth=2
	shll	$4, %edx
	movw	%dx, (%rdi)
	addq	$2, %rdi
	subq	$-128, %rbp
	cmpq	$2176, %rbp             # imm = 0x880
	jne	.LBB15_7
	jmp	.LBB15_19
	.p2align	4, 0x90
.LBB15_20:                              # %vector.body
                                        #   in Loop: Header=BB15_5 Depth=1
	movq	%r10, %rcx
	shlq	$5, %rcx
	addq	%r10, %rcx
	movups	(%rax), %xmm0
	movups	%xmm0, (%rax,%rcx,2)
	movups	16(%rax), %xmm0
	movups	%xmm0, 16(%rax,%rcx,2)
	movups	32(%rax), %xmm0
	movups	%xmm0, 32(%rax,%rcx,2)
	movups	48(%rax), %xmm0
	movups	%xmm0, 48(%rax,%rcx,2)
	movl	$32, %ebp
.LBB15_14:                              # %.preheader.split.prol.preheader
                                        #   in Loop: Header=BB15_5 Depth=1
	movl	$32, %r11d
	subq	%rbp, %r11
	leaq	(%rbx,%rbp,2), %rcx
	leaq	(%rax,%rbp,2), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_15:                              # %.preheader.split.prol
                                        #   Parent Loop BB15_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rsi,%rdx,2), %edi
	movw	%di, (%rcx,%rdx,2)
	incq	%rdx
	cmpq	$1, %rdx
	jne	.LBB15_15
# BB#16:                                # %.preheader.split.prol.loopexit
                                        #   in Loop: Header=BB15_5 Depth=1
	cmpq	$3, %r11
	jb	.LBB15_19
# BB#17:                                # %.preheader.split.preheader.new
                                        #   in Loop: Header=BB15_5 Depth=1
	addq	%rdx, %rbp
	addq	$3, %rbp
	.p2align	4, 0x90
.LBB15_18:                              # %.preheader.split
                                        #   Parent Loop BB15_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-6(%rax,%rbp,2), %ecx
	movw	%cx, -6(%rbx,%rbp,2)
	movzwl	-4(%rax,%rbp,2), %ecx
	movw	%cx, -4(%rbx,%rbp,2)
	movzwl	-2(%rax,%rbp,2), %ecx
	movw	%cx, -2(%rbx,%rbp,2)
	movzwl	(%rax,%rbp,2), %ecx
	movw	%cx, (%rbx,%rbp,2)
	addq	$4, %rbp
	cmpq	$36, %rbp
	jne	.LBB15_18
.LBB15_19:                              # %_ZN5ArrayItLi0EEC2Ei.exit
                                        #   in Loop: Header=BB15_5 Depth=1
	incq	%r10
	addq	$66, %rbx
	cmpq	%r9, %r10
	jl	.LBB15_5
	jmp	.LBB15_22
.LBB15_21:                              # %_ZN5ArrayItLi0EEC2Ei.exit.preheader
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
.LBB15_22:                              # %_ZN5ArrayItLi0EEC2Ei.exit._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB15_23:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Lfunc_end15:
	.size	_ZN4APM1C2Ei, .Lfunc_end15-_ZN4APM1C2Ei
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.text
	.globl	_ZN8StateMapC2Ei
	.p2align	4, 0x90
	.type	_ZN8StateMapC2Ei,@function
_ZN8StateMapC2Ei:                       # @_ZN8StateMapC2Ei
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
.Lcfi46:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	%esi, (%rbx)
	movl	$0, 4(%rbx)
	movl	%esi, 12(%rbx)
	movl	%esi, 8(%rbx)
	testl	%esi, %esi
	jle	.LBB16_5
# BB#1:
	shll	$2, %esi
	movl	programChecker(%rip), %eax
	addl	%esi, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB16_3
# BB#2:
	movl	%eax, programChecker+4(%rip)
.LBB16_3:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movslq	%esi, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB16_4
# BB#6:                                 # %.lr.ph
	movq	%rax, 24(%rbx)
	movslq	(%rbx), %rcx
	testq	%rcx, %rcx
	movl	$1, %esi
	cmovgq	%rcx, %rsi
	cmpq	$7, %rsi
	jbe	.LBB16_7
# BB#8:                                 # %min.iters.checked
	movabsq	$9223372036854775800, %rdx # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rsi, %rdx
	je	.LBB16_7
# BB#9:                                 # %vector.body.preheader
	leaq	-8(%rdx), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB16_10
# BB#11:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	movaps	.LCPI16_0(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	.p2align	4, 0x90
.LBB16_12:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rax,%rbx,4)
	movups	%xmm0, 16(%rax,%rbx,4)
	addq	$8, %rbx
	incq	%rdi
	jne	.LBB16_12
	jmp	.LBB16_13
.LBB16_7:
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_17:                              # %_ZN5ArrayIjLi0EEC2Ei.exit
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2147483648, (%rax,%rdx,4) # imm = 0x80000000
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB16_17
.LBB16_18:                              # %_ZN5ArrayIjLi0EEC2Ei.exit._crit_edge
	popq	%rbx
	retq
.LBB16_5:                               # %_ZN5ArrayIjLi0EEC2Ei.exit.preheader
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	popq	%rbx
	retq
.LBB16_10:
	xorl	%ebx, %ebx
.LBB16_13:                              # %vector.body.prol.loopexit
	cmpq	$56, %r8
	jb	.LBB16_16
# BB#14:                                # %vector.body.preheader.new
	movq	%rdx, %rdi
	subq	%rbx, %rdi
	leaq	240(%rax,%rbx,4), %rbx
	movaps	.LCPI16_0(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	.p2align	4, 0x90
.LBB16_15:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rbx)
	movups	%xmm0, -224(%rbx)
	movups	%xmm0, -208(%rbx)
	movups	%xmm0, -192(%rbx)
	movups	%xmm0, -176(%rbx)
	movups	%xmm0, -160(%rbx)
	movups	%xmm0, -144(%rbx)
	movups	%xmm0, -128(%rbx)
	movups	%xmm0, -112(%rbx)
	movups	%xmm0, -96(%rbx)
	movups	%xmm0, -80(%rbx)
	movups	%xmm0, -64(%rbx)
	movups	%xmm0, -48(%rbx)
	movups	%xmm0, -32(%rbx)
	movups	%xmm0, -16(%rbx)
	movups	%xmm0, (%rbx)
	addq	$256, %rbx              # imm = 0x100
	addq	$-64, %rdi
	jne	.LBB16_15
.LBB16_16:                              # %middle.block
	cmpq	%rdx, %rsi
	jne	.LBB16_17
	jmp	.LBB16_18
.LBB16_4:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Lfunc_end16:
	.size	_ZN8StateMapC2Ei, .Lfunc_end16-_ZN8StateMapC2Ei
	.cfi_endproc

	.section	.text._ZN5ArrayIjLi0EED2Ev,"axG",@progbits,_ZN5ArrayIjLi0EED2Ev,comdat
	.weak	_ZN5ArrayIjLi0EED2Ev
	.p2align	4, 0x90
	.type	_ZN5ArrayIjLi0EED2Ev,@function
_ZN5ArrayIjLi0EED2Ev:                   # @_ZN5ArrayIjLi0EED2Ev
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB17_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB17_2:                               # %_ZN14ProgramChecker5allocEi.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end17:
	.size	_ZN5ArrayIjLi0EED2Ev, .Lfunc_end17-_ZN5ArrayIjLi0EED2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.text
	.globl	_ZN3APMC2Ei
	.p2align	4, 0x90
	.type	_ZN3APMC2Ei,@function
_ZN3APMC2Ei:                            # @_ZN3APMC2Ei
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	leal	(,%r14,8), %eax
	leal	(%rax,%rax,2), %ebp
	movl	%ebp, (%rbx)
	movl	$0, 4(%rbx)
	movl	%ebp, 12(%rbx)
	movl	%ebp, 8(%rbx)
	testl	%r14d, %r14d
	jle	.LBB18_23
# BB#1:
	movl	%r14d, %eax
	shll	$5, %eax
	leal	(%rax,%rax,2), %eax
	movl	programChecker(%rip), %ecx
	addl	%eax, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB18_3
# BB#2:
	movl	%ecx, programChecker+4(%rip)
.LBB18_3:                               # %_ZN14ProgramChecker5allocEi.exit.i.i.i
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB18_25
# BB#4:                                 # %.lr.ph.i
	movq	%rax, 24(%rbx)
	movslq	%ebp, %rcx
	testq	%rcx, %rcx
	movl	$1, %esi
	cmovgq	%rcx, %rsi
	cmpq	$8, %rsi
	jae	.LBB18_6
# BB#5:
	xorl	%edx, %edx
	jmp	.LBB18_16
.LBB18_23:                              # %_ZN8StateMapC2Ei.exit.preheader.thread
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	jmp	.LBB18_24
.LBB18_6:                               # %min.iters.checked
	movabsq	$9223372036854775800, %rdx # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rsi, %rdx
	je	.LBB18_7
# BB#8:                                 # %vector.body.preheader
	leaq	-8(%rdx), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB18_9
# BB#10:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	movaps	.LCPI18_0(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	.p2align	4, 0x90
.LBB18_11:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rax,%rbp,4)
	movups	%xmm0, 16(%rax,%rbp,4)
	addq	$8, %rbp
	incq	%rdi
	jne	.LBB18_11
	jmp	.LBB18_12
.LBB18_7:
	xorl	%edx, %edx
	jmp	.LBB18_16
.LBB18_9:
	xorl	%ebp, %ebp
.LBB18_12:                              # %vector.body.prol.loopexit
	cmpq	$56, %r8
	jb	.LBB18_15
# BB#13:                                # %vector.body.preheader.new
	movq	%rdx, %rdi
	subq	%rbp, %rdi
	leaq	240(%rax,%rbp,4), %rbp
	movaps	.LCPI18_0(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	.p2align	4, 0x90
.LBB18_14:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rbp)
	movups	%xmm0, -224(%rbp)
	movups	%xmm0, -208(%rbp)
	movups	%xmm0, -192(%rbp)
	movups	%xmm0, -176(%rbp)
	movups	%xmm0, -160(%rbp)
	movups	%xmm0, -144(%rbp)
	movups	%xmm0, -128(%rbp)
	movups	%xmm0, -112(%rbp)
	movups	%xmm0, -96(%rbp)
	movups	%xmm0, -80(%rbp)
	movups	%xmm0, -64(%rbp)
	movups	%xmm0, -48(%rbp)
	movups	%xmm0, -32(%rbp)
	movups	%xmm0, -16(%rbp)
	movups	%xmm0, (%rbp)
	addq	$256, %rbp              # imm = 0x100
	addq	$-64, %rdi
	jne	.LBB18_14
.LBB18_15:                              # %middle.block
	cmpq	%rdx, %rsi
	je	.LBB18_17
	.p2align	4, 0x90
.LBB18_16:                              # %_ZN5ArrayIjLi0EEC2Ei.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2147483648, (%rax,%rdx,4) # imm = 0x80000000
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB18_16
.LBB18_17:                              # %_ZN8StateMapC2Ei.exit.preheader
	testl	%r14d, %r14d
	jle	.LBB18_24
# BB#18:                                # %.lr.ph.preheader
	movslq	(%rbx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB18_19:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rsi
	imulq	$715827883, %rsi, %rdi  # imm = 0x2AAAAAAB
	movq	%rdi, %rbp
	shrq	$63, %rbp
	shrq	$34, %rdi
	addl	%ebp, %edi
	shll	$3, %edi
	leal	(%rdi,%rdi,2), %edi
	subl	%edi, %esi
	shll	$13, %esi
	orl	$4096, %esi             # imm = 0x1000
	movslq	%esi, %rsi
	imulq	$715827883, %rsi, %rsi  # imm = 0x2AAAAAAB
	movq	%rsi, %rdi
	sarq	$35, %rdi
	shrq	$63, %rsi
	leal	-2048(%rdi,%rsi), %edi
	movl	$4095, %esi             # imm = 0xFFF
	cmpl	$2047, %edi             # imm = 0x7FF
	jg	.LBB18_22
# BB#20:                                #   in Loop: Header=BB18_19 Depth=1
	xorl	%esi, %esi
	cmpl	$-2047, %edi            # imm = 0xF801
	jl	.LBB18_22
# BB#21:                                #   in Loop: Header=BB18_19 Depth=1
	movl	%edi, %esi
	andl	$127, %esi
	sarl	$7, %edi
	movl	$128, %ebp
	subl	%esi, %ebp
	movslq	%edi, %rdi
	imull	_ZZ6squashiE1t+64(,%rdi,4), %ebp
	imull	_ZZ6squashiE1t+68(,%rdi,4), %esi
	leal	64(%rbp,%rsi), %esi
	sarl	$7, %esi
.LBB18_22:                              # %_Z6squashi.exit
                                        #   in Loop: Header=BB18_19 Depth=1
	shll	$20, %esi
	orl	$6, %esi
	movl	%esi, (%rax,%rdx,4)
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB18_19
.LBB18_24:                              # %_ZN8StateMapC2Ei.exit._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB18_25:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Lfunc_end18:
	.size	_ZN3APMC2Ei, .Lfunc_end18-_ZN3APMC2Ei
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_0:
	.long	256                     # 0x100
	.long	0                       # 0x0
	.long	256                     # 0x100
	.long	256                     # 0x100
.LCPI19_1:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.text
	.globl	_ZN10ContextMapC2Eii
	.p2align	4, 0x90
	.type	_ZN10ContextMapC2Eii,@function
_ZN10ContextMapC2Eii:                   # @_ZN10ContextMapC2Eii
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Lcfi55:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
	movl	%edx, %ebx
	movq	%rdi, %r13
	movl	%ebx, (%r13)
	sarl	$6, %esi
	movl	%esi, 12(%r13)
	movl	%esi, 8(%r13)
	testl	%esi, %esi
	jle	.LBB19_1
# BB#2:
	shll	$6, %esi
	addl	$64, %esi
	movl	programChecker(%rip), %eax
	addl	%esi, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB19_4
# BB#3:
	movl	%eax, programChecker+4(%rip)
.LBB19_4:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movslq	%esi, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 16(%r13)
	testq	%rax, %rax
	je	.LBB19_82
# BB#5:
	movl	%eax, %ecx
	andl	$63, %ecx
	negq	%rcx
	leaq	64(%rax,%rcx), %rcx
	movq	%rcx, %rax
	movq	%rax, -104(%rbp)        # 8-byte Spill
	movq	%rcx, 24(%r13)
	jmp	.LBB19_6
.LBB19_1:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r13)
	xorl	%eax, %eax
	movq	%rax, -104(%rbp)        # 8-byte Spill
.LBB19_6:                               # %_ZN5ArrayIN10ContextMap1EELi64EEC2Ei.exit
	movl	%ebx, 36(%r13)
	movl	%ebx, 32(%r13)
	testl	%ebx, %ebx
	movq	%rbx, -56(%rbp)         # 8-byte Spill
	jle	.LBB19_22
# BB#7:
	leal	(,%rbx,8), %r14d
	movl	programChecker(%rip), %ebx
	addl	%r14d, %ebx
	movl	%ebx, programChecker(%rip)
	movl	programChecker+4(%rip), %r15d
	cmpl	%r15d, %ebx
	jle	.LBB19_9
# BB#8:
	movl	%ebx, programChecker+4(%rip)
	movl	%ebx, %r15d
.LBB19_9:                               # %_ZN14ProgramChecker5allocEi.exit.i.i21
	movslq	%r14d, %r12
	movl	$1, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, 40(%r13)
	testq	%rax, %rax
	je	.LBB19_10
# BB#12:
	movq	%rax, %rcx
	movq	%rcx, -96(%rbp)         # 8-byte Spill
	movq	%rax, 48(%r13)
	movq	-56(%rbp), %rax         # 8-byte Reload
	movl	%eax, 60(%r13)
	movl	%eax, 56(%r13)
	addl	%r14d, %ebx
	movl	%ebx, programChecker(%rip)
	cmpl	%r15d, %ebx
	jle	.LBB19_14
# BB#13:
	movl	%ebx, programChecker+4(%rip)
	movl	%ebx, %r15d
.LBB19_14:                              # %_ZN14ProgramChecker5allocEi.exit.i.i22
	movl	$1, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.LBB19_15
# BB#17:
	movq	%rax, 72(%r13)
	movq	-56(%rbp), %rcx         # 8-byte Reload
	movl	%ecx, 84(%r13)
	movl	%ecx, 80(%r13)
	leal	(,%rcx,4), %eax
	leal	(%rbx,%rcx,4), %ebx
	movl	%ebx, programChecker(%rip)
	cmpl	%r15d, %ebx
	jle	.LBB19_19
# BB#18:
	movl	%ebx, programChecker+4(%rip)
	movl	%ebx, %r15d
.LBB19_19:                              # %_ZN14ProgramChecker5allocEi.exit.i.i25
	leaq	56(%r13), %rcx
	movq	%rcx, -80(%rbp)         # 8-byte Spill
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 88(%r13)
	testq	%rax, %rax
	je	.LBB19_20
# BB#23:
	movq	%rax, 96(%r13)
	movq	-56(%rbp), %rax         # 8-byte Reload
	movl	%eax, 108(%r13)
	movl	%eax, 104(%r13)
	addl	%r14d, %ebx
	movl	%ebx, programChecker(%rip)
	cmpl	%r15d, %ebx
	jle	.LBB19_25
# BB#24:
	movl	%ebx, programChecker+4(%rip)
.LBB19_25:                              # %_ZN14ProgramChecker5allocEi.exit.i.i27
	leaq	80(%r13), %r14
	movl	$1, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, 112(%r13)
	testq	%rax, %rax
	je	.LBB19_26
# BB#28:
	leaq	104(%r13), %r15
	movq	%rax, 120(%r13)
	movq	-56(%rbp), %rbx         # 8-byte Reload
	jmp	.LBB19_29
.LBB19_22:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%r13)
	leaq	56(%r13), %rax
	movq	%rax, -80(%rbp)         # 8-byte Spill
	movl	%ebx, 60(%r13)
	movl	%ebx, 56(%r13)
	movups	%xmm0, 64(%r13)
	leaq	80(%r13), %r14
	movl	%ebx, 84(%r13)
	movl	%ebx, 80(%r13)
	movups	%xmm0, 88(%r13)
	leaq	104(%r13), %r15
	movl	%ebx, 108(%r13)
	movl	%ebx, 104(%r13)
	movups	%xmm0, 112(%r13)
	xorl	%eax, %eax
	movq	%rax, -96(%rbp)         # 8-byte Spill
.LBB19_29:                              # %_ZN5ArrayIPhLi0EEC2Ei.exit29
	movslq	%ebx, %r12
	movl	$32, %ecx
	movq	%r12, %rax
	mulq	%rcx
	pushfq
	popq	%rcx
	addq	$8, %rax
	movq	$-1, %rdi
	cmovbq	%rdi, %rax
	movl	$0, 136(%r13)
	pushq	%rcx
	popfq
	cmovnoq	%rax, %rdi
.Ltmp30:
	callq	_Znam
.Ltmp31:
# BB#30:
	movq	%r12, (%rax)
	movq	%rax, %rcx
	addq	$8, %rcx
	testl	%ebx, %ebx
	je	.LBB19_31
# BB#32:
	movq	%rax, -72(%rbp)         # 8-byte Spill
	movq	%r15, -112(%rbp)        # 8-byte Spill
	movq	%r14, -64(%rbp)         # 8-byte Spill
	movq	%r13, -48(%rbp)         # 8-byte Spill
	movq	%r12, -120(%rbp)        # 8-byte Spill
	movq	%r12, %rbx
	shlq	$5, %rbx
	addq	%rcx, %rbx
	movl	programChecker(%rip), %r14d
	movl	programChecker+4(%rip), %r12d
	xorl	%r15d, %r15d
	movq	%rcx, -88(%rbp)         # 8-byte Spill
	movq	%rcx, %r13
	.p2align	4, 0x90
.LBB19_33:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_45 Depth 2
                                        #     Child Loop BB19_48 Depth 2
                                        #     Child Loop BB19_50 Depth 2
	movaps	.LCPI19_0(%rip), %xmm0  # xmm0 = [256,0,256,256]
	movups	%xmm0, (%r13)
	addl	$1024, %r14d            # imm = 0x400
	movl	%r14d, programChecker(%rip)
	cmpl	%r12d, %r14d
	jle	.LBB19_35
# BB#34:                                #   in Loop: Header=BB19_33 Depth=1
	movl	%r14d, programChecker+4(%rip)
	movl	%r14d, %r12d
.LBB19_35:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i
                                        #   in Loop: Header=BB19_33 Depth=1
	movl	$1024, %edi             # imm = 0x400
	movl	$1, %esi
	callq	calloc
	movq	%rax, 16(%r13)
	testq	%rax, %rax
	je	.LBB19_36
# BB#38:                                # %.lr.ph.i
                                        #   in Loop: Header=BB19_33 Depth=1
	movq	%rax, 24(%r13)
	movslq	(%r13), %rcx
	testq	%rcx, %rcx
	movl	$1, %esi
	cmovgq	%rcx, %rsi
	cmpq	$8, %rsi
	movaps	.LCPI19_1(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	jae	.LBB19_40
# BB#39:                                #   in Loop: Header=BB19_33 Depth=1
	xorl	%edx, %edx
	jmp	.LBB19_50
	.p2align	4, 0x90
.LBB19_40:                              # %min.iters.checked
                                        #   in Loop: Header=BB19_33 Depth=1
	movq	%rsi, %rdx
	movabsq	$9223372036854775800, %rdi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rdx
	je	.LBB19_41
# BB#42:                                # %vector.body.preheader
                                        #   in Loop: Header=BB19_33 Depth=1
	movq	%rbx, %r9
	leaq	-8(%rdx), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB19_43
# BB#44:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB19_33 Depth=1
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_45:                              # %vector.body.prol
                                        #   Parent Loop BB19_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, (%rax,%rbx,4)
	movups	%xmm0, 16(%rax,%rbx,4)
	addq	$8, %rbx
	incq	%rdi
	jne	.LBB19_45
	jmp	.LBB19_46
.LBB19_41:                              #   in Loop: Header=BB19_33 Depth=1
	xorl	%edx, %edx
	jmp	.LBB19_50
.LBB19_43:                              #   in Loop: Header=BB19_33 Depth=1
	xorl	%ebx, %ebx
.LBB19_46:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB19_33 Depth=1
	cmpq	$56, %r8
	jb	.LBB19_49
# BB#47:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB19_33 Depth=1
	movq	%rdx, %rdi
	subq	%rbx, %rdi
	leaq	240(%rax,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB19_48:                              # %vector.body
                                        #   Parent Loop BB19_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, -240(%rbx)
	movups	%xmm0, -224(%rbx)
	movups	%xmm0, -208(%rbx)
	movups	%xmm0, -192(%rbx)
	movups	%xmm0, -176(%rbx)
	movups	%xmm0, -160(%rbx)
	movups	%xmm0, -144(%rbx)
	movups	%xmm0, -128(%rbx)
	movups	%xmm0, -112(%rbx)
	movups	%xmm0, -96(%rbx)
	movups	%xmm0, -80(%rbx)
	movups	%xmm0, -64(%rbx)
	movups	%xmm0, -48(%rbx)
	movups	%xmm0, -32(%rbx)
	movups	%xmm0, -16(%rbx)
	movups	%xmm0, (%rbx)
	addq	$256, %rbx              # imm = 0x100
	addq	$-64, %rdi
	jne	.LBB19_48
.LBB19_49:                              # %middle.block
                                        #   in Loop: Header=BB19_33 Depth=1
	cmpq	%rdx, %rsi
	movq	%r9, %rbx
	je	.LBB19_51
	.p2align	4, 0x90
.LBB19_50:                              # %_ZN5ArrayIjLi0EEC2Ei.exit.i
                                        #   Parent Loop BB19_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$-2147483648, (%rax,%rdx,4) # imm = 0x80000000
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB19_50
.LBB19_51:                              # %_ZN8StateMapC2Ei.exit
                                        #   in Loop: Header=BB19_33 Depth=1
	addq	$32, %r13
	addq	$32, %r15
	cmpq	%rbx, %r13
	jne	.LBB19_33
# BB#52:                                # %.loopexit33
	movq	-48(%rbp), %rsi         # 8-byte Reload
	movq	-88(%rbp), %rax         # 8-byte Reload
	movq	%rax, 128(%rsi)
	movq	-56(%rbp), %rdx         # 8-byte Reload
	testl	%edx, %edx
	movq	-120(%rbp), %rdi        # 8-byte Reload
	jle	.LBB19_56
# BB#53:                                # %.lr.ph
	movq	-104(%rbp), %rax        # 8-byte Reload
	movq	%rax, %rcx
	addq	$15, %rcx
	movq	-96(%rbp), %rax         # 8-byte Reload
	movq	%rcx, (%rax)
	movq	72(%rsi), %rax
	movq	%rcx, (%rax)
	movq	48(%rsi), %rax
	movq	120(%rsi), %rcx
	movq	(%rax), %rax
	addq	$3, %rax
	movq	%rax, (%rcx)
	cmpl	$1, %edx
	je	.LBB19_56
# BB#54:                                # %._crit_edge39.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB19_55:                              # %._crit_edge39
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsi), %rcx
	movq	48(%rsi), %rdx
	addq	$15, %rcx
	movq	%rcx, (%rdx,%rax,8)
	movq	72(%rsi), %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	48(%rsi), %rcx
	movq	120(%rsi), %rdx
	movq	(%rcx,%rax,8), %rcx
	addq	$3, %rcx
	movq	%rcx, (%rdx,%rax,8)
	incq	%rax
	cmpq	%rdi, %rax
	jl	.LBB19_55
	jmp	.LBB19_56
.LBB19_31:                              # %.loopexit33.thread
	movq	%rcx, 128(%r13)
.LBB19_56:                              # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_36:
.Ltmp33:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp34:
# BB#37:                                # %.noexc30
.LBB19_82:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.LBB19_10:
.Ltmp27:
	movq	%r13, -48(%rbp)         # 8-byte Spill
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp28:
# BB#11:                                # %.noexc
.LBB19_15:
.Ltmp24:
	movq	%r13, -48(%rbp)         # 8-byte Spill
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp25:
# BB#16:                                # %.noexc23
.LBB19_20:
.Ltmp21:
	movq	%r13, -48(%rbp)         # 8-byte Spill
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp22:
# BB#21:                                # %.noexc26
.LBB19_26:
.Ltmp18:
	movq	%r14, -64(%rbp)         # 8-byte Spill
	movq	%r13, -48(%rbp)         # 8-byte Spill
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp19:
# BB#27:                                # %.noexc28
.LBB19_60:
.Ltmp20:
	movq	%rax, %r12
	jmp	.LBB19_70
.LBB19_59:
.Ltmp23:
	movq	%rax, %r12
	jmp	.LBB19_73
.LBB19_58:
.Ltmp26:
	movq	%rax, %r12
	jmp	.LBB19_76
.LBB19_57:
.Ltmp29:
	movq	%rax, %r12
	jmp	.LBB19_79
.LBB19_61:
.Ltmp35:
	movq	%rax, %r12
	cmpq	%r13, -88(%rbp)         # 8-byte Folded Reload
	je	.LBB19_65
.LBB19_62:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	-72(%rbp), %rax         # 8-byte Reload
	movl	-16(%rax,%r15), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB19_64
# BB#63:                                #   in Loop: Header=BB19_62 Depth=1
	movl	%eax, programChecker+4(%rip)
.LBB19_64:                              #   in Loop: Header=BB19_62 Depth=1
	movq	-72(%rbp), %rax         # 8-byte Reload
	movq	-8(%rax,%r15), %rdi
	callq	free
	addq	$-32, %r15
	jne	.LBB19_62
.LBB19_65:                              # %.loopexit
	movq	-72(%rbp), %rdi         # 8-byte Reload
	callq	_ZdaPv
	movq	-112(%rbp), %rax        # 8-byte Reload
	movl	(%rax), %eax
	movq	%rax, -56(%rbp)         # 8-byte Spill
	jmp	.LBB19_67
.LBB19_66:
.Ltmp32:
	movq	%r14, -64(%rbp)         # 8-byte Spill
	movq	%r13, -48(%rbp)         # 8-byte Spill
	movq	%rax, %r12
.LBB19_67:
	movq	-56(%rbp), %rcx         # 8-byte Reload
	shll	$3, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB19_69
# BB#68:
	movl	%eax, programChecker+4(%rip)
.LBB19_69:                              # %_ZN5ArrayIPhLi0EED2Ev.exit
	movq	-48(%rbp), %rax         # 8-byte Reload
	movq	112(%rax), %rdi
	callq	free
.LBB19_70:
	movq	-64(%rbp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB19_72
# BB#71:
	movl	%eax, programChecker+4(%rip)
.LBB19_72:                              # %_ZN5ArrayIjLi0EED2Ev.exit
	movq	-64(%rbp), %rax         # 8-byte Reload
	movq	8(%rax), %rdi
	callq	free
.LBB19_73:
	movq	-80(%rbp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	shll	$3, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB19_75
# BB#74:
	movl	%eax, programChecker+4(%rip)
.LBB19_75:                              # %_ZN5ArrayIPhLi0EED2Ev.exit31
	movq	-80(%rbp), %rax         # 8-byte Reload
	movq	8(%rax), %rdi
	callq	free
.LBB19_76:
	movq	-48(%rbp), %rax         # 8-byte Reload
	movl	32(%rax), %ecx
	shll	$3, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB19_78
# BB#77:
	movl	%eax, programChecker+4(%rip)
.LBB19_78:                              # %_ZN5ArrayIPhLi0EED2Ev.exit32
	movq	-48(%rbp), %rax         # 8-byte Reload
	movq	40(%rax), %rdi
	callq	free
.LBB19_79:
	movq	-48(%rbp), %rax         # 8-byte Reload
	movl	8(%rax), %ecx
	shll	$6, %ecx
	movl	$-64, %eax
	subl	%ecx, %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB19_81
# BB#80:
	movl	%eax, programChecker+4(%rip)
.LBB19_81:                              # %_ZN5ArrayIN10ContextMap1EELi64EED2Ev.exit
	movq	-48(%rbp), %rax         # 8-byte Reload
	movq	16(%rax), %rdi
	callq	free
	movq	%r12, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN10ContextMapC2Eii, .Lfunc_end19-_ZN10ContextMapC2Eii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin2   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp34-.Ltmp33         #   Call between .Ltmp33 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin2   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp27-.Ltmp34         #   Call between .Ltmp34 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp28-.Ltmp27         #   Call between .Ltmp27 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin2   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Lfunc_end19-.Ltmp19    #   Call between .Ltmp19 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8StateMapD2Ev,"axG",@progbits,_ZN8StateMapD2Ev,comdat
	.weak	_ZN8StateMapD2Ev
	.p2align	4, 0x90
	.type	_ZN8StateMapD2Ev,@function
_ZN8StateMapD2Ev:                       # @_ZN8StateMapD2Ev
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB20_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB20_2:                               # %_ZN5ArrayIjLi0EED2Ev.exit
	movq	16(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end20:
	.size	_ZN8StateMapD2Ev, .Lfunc_end20-_ZN8StateMapD2Ev
	.cfi_endproc

	.section	.text._ZN5ArrayIPhLi0EED2Ev,"axG",@progbits,_ZN5ArrayIPhLi0EED2Ev,comdat
	.weak	_ZN5ArrayIPhLi0EED2Ev
	.p2align	4, 0x90
	.type	_ZN5ArrayIPhLi0EED2Ev,@function
_ZN5ArrayIPhLi0EED2Ev:                  # @_ZN5ArrayIPhLi0EED2Ev
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	shll	$3, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB21_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB21_2:                               # %_ZN14ProgramChecker5allocEi.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end21:
	.size	_ZN5ArrayIPhLi0EED2Ev, .Lfunc_end21-_ZN5ArrayIPhLi0EED2Ev
	.cfi_endproc

	.text
	.globl	_ZN10ContextMapD2Ev
	.p2align	4, 0x90
	.type	_ZN10ContextMapD2Ev,@function
_ZN10ContextMapD2Ev:                    # @_ZN10ContextMapD2Ev
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 48
.Lcfi66:
	.cfi_offset %rbx, -40
.Lcfi67:
	.cfi_offset %r12, -32
.Lcfi68:
	.cfi_offset %r14, -24
.Lcfi69:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	128(%r15), %r12
	testq	%r12, %r12
	je	.LBB22_7
# BB#1:
	leaq	-8(%r12), %r14
	movq	-8(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB22_6
# BB#2:                                 # %.preheader.preheader
	shlq	$5, %rbx
	.p2align	4, 0x90
.LBB22_3:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	-24(%r12,%rbx), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB22_5
# BB#4:                                 #   in Loop: Header=BB22_3 Depth=1
	movl	%eax, programChecker+4(%rip)
.LBB22_5:                               #   in Loop: Header=BB22_3 Depth=1
	movq	-16(%r12,%rbx), %rdi
	callq	free
	addq	$-32, %rbx
	jne	.LBB22_3
.LBB22_6:                               # %.loopexit
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB22_7:
	movl	104(%r15), %ecx
	shll	$3, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB22_9
# BB#8:
	movl	%eax, programChecker+4(%rip)
.LBB22_9:                               # %_ZN5ArrayIPhLi0EED2Ev.exit
	movq	112(%r15), %rdi
	callq	free
	movl	80(%r15), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB22_11
# BB#10:
	movl	%eax, programChecker+4(%rip)
.LBB22_11:                              # %_ZN5ArrayIjLi0EED2Ev.exit
	movq	88(%r15), %rdi
	callq	free
	movl	56(%r15), %ecx
	shll	$3, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB22_13
# BB#12:
	movl	%eax, programChecker+4(%rip)
.LBB22_13:                              # %_ZN5ArrayIPhLi0EED2Ev.exit2
	movq	64(%r15), %rdi
	callq	free
	movl	32(%r15), %ecx
	shll	$3, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB22_15
# BB#14:
	movl	%eax, programChecker+4(%rip)
.LBB22_15:                              # %_ZN5ArrayIPhLi0EED2Ev.exit3
	movq	40(%r15), %rdi
	callq	free
	movl	8(%r15), %ecx
	shll	$6, %ecx
	movl	$-64, %eax
	subl	%ecx, %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB22_17
# BB#16:
	movl	%eax, programChecker+4(%rip)
.LBB22_17:                              # %_ZN5ArrayIN10ContextMap1EELi64EED2Ev.exit
	movq	16(%r15), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end22:
	.size	_ZN10ContextMapD2Ev, .Lfunc_end22-_ZN10ContextMapD2Ev
	.cfi_endproc

	.globl	_ZN10ContextMap4mix1ER5Mixeriiii
	.p2align	4, 0x90
	.type	_ZN10ContextMap4mix1ER5Mixeriiii,@function
_ZN10ContextMap4mix1ER5Mixeriiii:       # @_ZN10ContextMap4mix1ER5Mixeriiii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 160
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movl	%r8d, 4(%rsp)           # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r15
	cmpl	$0, 136(%r15)
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	jle	.LBB23_1
# BB#5:                                 # %.lr.ph
	leaq	32(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$8, %eax
	subl	%ecx, %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movl	$7, %eax
	subl	%ecx, %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	leaq	104(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	andl	$1, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	andl	$3, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	56(%r15), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movslq	%r9d, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	48(%r15), %r9
	xorl	%r13d, %r13d
	xorl	%r8d, %r8d
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movq	%r12, 96(%rsp)          # 8-byte Spill
	movq	%r15, 80(%rsp)          # 8-byte Spill
	movq	96(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB23_6:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%r13,2), %rax
	testq	%rax, %rax
	je	.LBB23_11
# BB#7:                                 #   in Loop: Header=BB23_6 Depth=1
	movzbl	(%rax), %ecx
	movq	88(%rsp), %rdx          # 8-byte Reload
	movzbl	_ZL11State_table(%rdx,%rcx,4), %edx
	cmpl	$204, %edx
	jb	.LBB23_10
# BB#8:                                 #   in Loop: Header=BB23_6 Depth=1
	movl	rnd+24(%rip), %ecx
	leal	1(%rcx), %esi
	movl	%esi, rnd+24(%rip)
	leal	41(%rcx), %edi
	andl	$63, %edi
	movq	rnd+16(%rip), %rbp
	addl	$10, %ecx
	andl	$63, %ecx
	movl	(%rbp,%rcx,4), %ebx
	xorl	(%rbp,%rdi,4), %ebx
	andl	$63, %esi
	movl	%ebx, (%rbp,%rsi,4)
	movl	$452, %ecx              # imm = 0x1C4
	subl	%edx, %ecx
	shrl	$3, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	testl	%ebx, %ebx
	je	.LBB23_10
# BB#9:                                 #   in Loop: Header=BB23_6 Depth=1
	addb	$-4, %dl
.LBB23_10:                              #   in Loop: Header=BB23_6 Depth=1
	movb	%dl, (%rax)
.LBB23_11:                              #   in Loop: Header=BB23_6 Depth=1
	movl	bpos(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB23_14
# BB#12:                                #   in Loop: Header=BB23_6 Depth=1
	movq	120(%r15), %rcx
	movq	(%rcx,%r13,2), %rcx
	cmpb	$0, (%rcx)
	je	.LBB23_13
.LBB23_14:                              # %thread-pre-split
                                        #   in Loop: Header=BB23_6 Depth=1
	cmpl	$7, %eax
	ja	.LBB23_20
# BB#15:                                # %thread-pre-split
                                        #   in Loop: Header=BB23_6 Depth=1
	movl	$74, %ecx
	btl	%eax, %ecx
	jb	.LBB23_38
# BB#16:                                # %thread-pre-split
                                        #   in Loop: Header=BB23_6 Depth=1
	movl	$36, %ecx
	btl	%eax, %ecx
	jb	.LBB23_19
# BB#17:                                # %thread-pre-split
                                        #   in Loop: Header=BB23_6 Depth=1
	movl	$144, %ecx
	btl	%eax, %ecx
	jae	.LBB23_20
# BB#18:                                #   in Loop: Header=BB23_6 Depth=1
	movq	72(%r15), %rax
	movq	(%rax,%r13,2), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	3(%rax,%rcx), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB23_31
	.p2align	4, 0x90
.LBB23_38:                              #   in Loop: Header=BB23_6 Depth=1
	movq	72(%r15), %rax
	movq	(%rax,%r13,2), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rax,%rcx), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB23_31
	.p2align	4, 0x90
.LBB23_19:                              #   in Loop: Header=BB23_6 Depth=1
	movq	96(%r15), %rax
	movl	(%rax,%r13), %esi
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rsi,%rax), %eax
	movl	8(%r15), %ecx
	decl	%ecx
	andl	%eax, %ecx
	movslq	%ecx, %rdi
	shlq	$6, %rdi
	addq	24(%r15), %rdi
	shrl	$16, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r8, %rbx
	callq	_ZN10ContextMap1E3getEt
	movq	%rbx, %r8
	movq	48(%r15), %rcx
	movq	%rax, (%rcx,%r13,2)
	movq	56(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB23_31
.LBB23_13:                              #   in Loop: Header=BB23_6 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	xorl	%eax, %eax
	jmp	.LBB23_31
.LBB23_20:                              #   in Loop: Header=BB23_6 Depth=1
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	96(%r15), %rax
	movl	(%rax,%r13), %esi
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rsi,%rax), %eax
	movl	8(%r15), %ecx
	decl	%ecx
	andl	%eax, %ecx
	movslq	%ecx, %rdi
	shlq	$6, %rdi
	addq	24(%r15), %rdi
	shrl	$16, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN10ContextMap1E3getEt
	movq	48(%r15), %rcx
	movq	%rax, (%rcx,%r13,2)
	movq	72(%r15), %rcx
	movq	%rax, (%rcx,%r13,2)
	movq	72(%r15), %rax
	movq	(%rax,%r13,2), %rax
	cmpb	$2, 3(%rax)
	jne	.LBB23_22
# BB#21:                                #   in Loop: Header=BB23_6 Depth=1
	movzbl	4(%rax), %ebp
	leal	256(%rbp), %ebx
	movq	96(%r15), %rax
	movl	(%rax,%r13), %esi
	movl	%ebx, %eax
	shrl	$6, %eax
	addl	%esi, %eax
	movl	8(%r15), %ecx
	decl	%ecx
	andl	%eax, %ecx
	movslq	%ecx, %rdi
	shlq	$6, %rdi
	addq	24(%r15), %rdi
	shrl	$16, %esi
	callq	_ZN10ContextMap1E3getEt
	movl	%ebp, %ecx
	shrl	$5, %ecx
	andl	$1, %ecx
	leaq	1(%rcx), %rdx
	movb	%dl, (%rax)
	movl	%ebx, %edx
	shrl	$4, %edx
	movl	%edx, %esi
	andl	$1, %esi
	incl	%esi
	movb	%sil, 1(%rax,%rcx)
	movl	%ebx, %ecx
	shrl	$3, %ecx
	movl	%ecx, %esi
	andl	$1, %esi
	incl	%esi
	andl	$3, %edx
	movb	%sil, 3(%rax,%rdx)
	movq	96(%r15), %rax
	movl	(%rax,%r13), %esi
	addl	%esi, %ecx
	movl	8(%r15), %eax
	decl	%eax
	andl	%ecx, %eax
	movslq	%eax, %rdi
	shlq	$6, %rdi
	addq	24(%r15), %rdi
	shrl	$16, %esi
	callq	_ZN10ContextMap1E3getEt
	movl	%ebp, %ecx
	shrl	$2, %ecx
	andl	$1, %ecx
	leaq	1(%rcx), %rdx
	movb	%dl, (%rax)
	shrl	%ebx
	movl	%ebx, %edx
	andl	$1, %edx
	incl	%edx
	movb	%dl, 1(%rax,%rcx)
	andb	$1, %bpl
	incb	%bpl
	andl	$3, %ebx
	movb	%bpl, 3(%rax,%rbx)
	movq	72(%r15), %rax
	movq	(%rax,%r13,2), %rax
	movb	$0, 6(%rax)
.LBB23_22:                              #   in Loop: Header=BB23_6 Depth=1
	movq	120(%r15), %rax
	movq	(%rax,%r13,2), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	movq	24(%rsp), %r8           # 8-byte Reload
	je	.LBB23_23
# BB#24:                                #   in Loop: Header=BB23_6 Depth=1
	movzbl	1(%rax), %edx
	movl	4(%rsp), %esi           # 4-byte Reload
	cmpl	%esi, %edx
	movl	%esi, %edx
	jne	.LBB23_25
# BB#26:                                #   in Loop: Header=BB23_6 Depth=1
	cmpb	$-3, %cl
	ja	.LBB23_28
# BB#27:                                #   in Loop: Header=BB23_6 Depth=1
	addb	$2, %cl
	movb	%cl, (%rax)
	jmp	.LBB23_30
.LBB23_23:                              #   in Loop: Header=BB23_6 Depth=1
	movb	$2, (%rax)
	movq	120(%r15), %rax
	movq	(%rax,%r13,2), %rax
	movl	4(%rsp), %ecx           # 4-byte Reload
	movb	%cl, 1(%rax)
	jmp	.LBB23_30
.LBB23_25:                              #   in Loop: Header=BB23_6 Depth=1
	movb	$1, (%rax)
	movq	120(%r15), %rax
	movq	(%rax,%r13,2), %rax
	movb	%dl, 1(%rax)
	jmp	.LBB23_30
.LBB23_28:                              #   in Loop: Header=BB23_6 Depth=1
	cmpb	$-1, %cl
	jne	.LBB23_30
# BB#29:                                #   in Loop: Header=BB23_6 Depth=1
	movb	$-128, (%rax)
.LBB23_30:                              #   in Loop: Header=BB23_6 Depth=1
	movq	72(%r15), %rax
	movq	(%rax,%r13,2), %rax
	addq	$3, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB23_31:                              #   in Loop: Header=BB23_6 Depth=1
	movq	16(%rcx), %rcx
	movq	%rax, (%rcx,%r13,2)
	movq	120(%r15), %rax
	movq	(%rax,%r13,2), %rax
	movzbl	1(%rax), %edx
	leal	256(%rdx), %esi
	movl	44(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	cmpl	8(%rsp), %esi           # 4-byte Folded Reload
	jne	.LBB23_32
# BB#33:                                #   in Loop: Header=BB23_6 Depth=1
	movzbl	(%rax), %eax
	movl	40(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	andl	$1, %edx
	leal	-1(%rdx,%rdx), %esi
	movq	ilog+16(%rip), %rcx
	movzbl	1(%rcx,%rax), %edx
	andl	$1, %eax
	xorl	$3, %eax
	movl	%eax, %ecx
	shll	%cl, %edx
	imull	%esi, %edx
	jmp	.LBB23_34
	.p2align	4, 0x90
.LBB23_32:                              #   in Loop: Header=BB23_6 Depth=1
	xorl	%edx, %edx
.LBB23_34:                              #   in Loop: Header=BB23_6 Depth=1
	movq	%r12, %rsi
	movslq	96(%rsi), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 96(%rsi)
	movq	32(%rsi), %r10
	movw	%dx, (%r10,%rax,2)
	movq	48(%r15), %r9
	movq	(%r9,%r13,2), %rax
	testq	%rax, %rax
	je	.LBB23_36
# BB#35:                                #   in Loop: Header=BB23_6 Depth=1
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movzbl	(%rax), %r9d
	movq	128(%r15), %rax
	movq	24(%rax,%r13,8), %rcx
	movslq	4(%rax,%r13,8), %rdx
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, %edi
	shrl	$10, %edi
	leal	1(%rsi), %ebp
	movl	%esi, %ebx
	orl	$1023, %ebx             # imm = 0x3FF
	andl	$1023, %esi             # imm = 0x3FF
	cmpl	$1023, %esi             # imm = 0x3FF
	cmovnel	%ebp, %ebx
	movl	y(%rip), %ebp
	shll	$22, %ebp
	subl	%edi, %ebp
	sarl	$3, %ebp
	imull	_ZL2dt(,%rsi,4), %ebp
	andl	$-1024, %ebp            # imm = 0xFC00
	addl	%ebx, %ebp
	movl	%ebp, (%rcx,%rdx,4)
	movl	%r9d, 4(%rax,%r13,8)
	movl	(%rcx,%r9,4), %esi
	movzbl	_ZL11State_table+3(,%r9,4), %edi
	movl	%esi, %ecx
	shrl	$24, %ecx
	movl	%ecx, %r15d
	xorl	$255, %r15d
	xorl	%eax, %eax
	cmpb	$0, %dil
	sete	%al
	movl	$0, %r11d
	cmovel	%r15d, %r11d
	xorl	%edx, %edx
	cmpb	$0, _ZL11State_table+2(,%r9,4)
	sete	%dl
	movl	$0, %ebx
	cmovel	%ecx, %ebx
	movl	$0, %ebp
	cmovel	%r15d, %ebp
	cmpb	$0, %dil
	movl	$0, %edi
	cmovel	%ecx, %edi
	shrl	$20, %esi
	movq	stretch+16(%rip), %r12
	movswl	(%r12,%rsi,2), %r12d
	sarl	$2, %r12d
	subl	%r15d, %ecx
	movq	80(%rsp), %r15          # 8-byte Reload
	movslq	96(%r14), %rsi
	movw	%r12w, (%r10,%rsi,2)
	movw	%cx, 2(%r10,%rsi,2)
	subl	%eax, %edx
	imull	%r12d, %edx
	movw	%dx, 4(%r10,%rsi,2)
	subl	%r11d, %ebx
	movw	%bx, 6(%r10,%rsi,2)
	subl	%ebp, %edi
	leal	5(%rsi), %eax
	movq	%r14, %r12
	movl	%eax, 96(%r14)
	movw	%di, 8(%r10,%rsi,2)
	cmpq	$1, %r9
	movq	24(%rsp), %r9           # 8-byte Reload
	sbbl	$-1, (%rsp)             # 4-byte Folded Spill
	jmp	.LBB23_37
	.p2align	4, 0x90
.LBB23_36:                              #   in Loop: Header=BB23_6 Depth=1
	movq	128(%r15), %rax
	movq	24(%rax,%r13,8), %rcx
	movslq	4(%rax,%r13,8), %rdx
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, %edi
	shrl	$10, %edi
	leal	1(%rsi), %ebp
	movl	%esi, %ebx
	orl	$1023, %ebx             # imm = 0x3FF
	andl	$1023, %esi             # imm = 0x3FF
	cmpl	$1023, %esi             # imm = 0x3FF
	cmovnel	%ebp, %ebx
	movl	y(%rip), %ebp
	shll	$22, %ebp
	subl	%edi, %ebp
	sarl	$3, %ebp
	imull	_ZL2dt(,%rsi,4), %ebp
	andl	$-1024, %ebp            # imm = 0xFC00
	addl	%ebx, %ebp
	movl	%ebp, (%rcx,%rdx,4)
	movl	$0, 4(%rax,%r13,8)
	movl	(%rcx), %eax
	movl	%eax, %ecx
	shrl	$20, %ecx
	movq	stretch+16(%rip), %rdx
	movzwl	(%rdx,%rcx,2), %ecx
	sarw	$2, %cx
	movq	%r12, %rsi
	movslq	96(%rsi), %rdx
	movw	%cx, (%r10,%rdx,2)
	shrl	$24, %eax
	movl	%eax, %ecx
	xorl	$255, %ecx
	subl	%ecx, %eax
	movw	%ax, 2(%r10,%rdx,2)
	movw	$0, 4(%r10,%rdx,2)
	movw	%ax, 6(%r10,%rdx,2)
	leal	5(%rdx), %ecx
	movl	%ecx, 96(%rsi)
	movw	%ax, 8(%r10,%rdx,2)
.LBB23_37:                              #   in Loop: Header=BB23_6 Depth=1
	incq	%r8
	movslq	136(%r15), %rax
	addq	$4, %r13
	cmpq	%rax, %r8
	jl	.LBB23_6
	jmp	.LBB23_2
.LBB23_1:
	movl	$0, (%rsp)              # 4-byte Folded Spill
.LBB23_2:                               # %._crit_edge
	cmpl	$7, 36(%rsp)            # 4-byte Folded Reload
	jne	.LBB23_4
# BB#3:
	movl	$0, 136(%r15)
.LBB23_4:
	movl	(%rsp), %eax            # 4-byte Reload
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_ZN10ContextMap4mix1ER5Mixeriiii, .Lfunc_end23-_ZN10ContextMap4mix1ER5Mixeriiii
	.cfi_endproc

	.section	.text._ZN10ContextMap1E3getEt,"axG",@progbits,_ZN10ContextMap1E3getEt,comdat
	.weak	_ZN10ContextMap1E3getEt
	.p2align	4, 0x90
	.type	_ZN10ContextMap1E3getEt,@function
_ZN10ContextMap1E3getEt:                # @_ZN10ContextMap1E3getEt
	.cfi_startproc
# BB#0:
	movzbl	14(%rdi), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpw	%si, (%rdi,%rcx,2)
	jne	.LBB24_1
# BB#3:
	leaq	(,%rcx,8), %rax
	subq	%rcx, %rax
	leaq	15(%rdi,%rax), %rax
	retq
.LBB24_1:                               # %.preheader.preheader
	cmpw	%si, (%rdi)
	jne	.LBB24_4
# BB#2:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.LBB24_6
.LBB24_4:                               # %.preheader.141
	movzbl	15(%rdi), %r9d
	testb	$-16, %al
	movl	$65535, %edx            # imm = 0xFFFF
	cmovel	%edx, %r9d
	testb	$15, %al
	cmovel	%edx, %r9d
	cmpw	%si, 2(%rdi)
	jne	.LBB24_9
# BB#5:
	movb	$1, %cl
	movl	$1, %edx
	jmp	.LBB24_6
.LBB24_9:
	movzbl	22(%rdi), %r10d
	xorl	%r8d, %r8d
	cmpl	%r9d, %r10d
	jae	.LBB24_11
# BB#10:
	movl	%eax, %edx
	andl	$240, %edx
	cmpl	$16, %edx
	setne	%r8b
	cmovel	%r9d, %r10d
	cmpl	$1, %ecx
	setne	%dl
	cmovnel	%r10d, %r9d
	andb	%r8b, %dl
	movzbl	%dl, %r8d
.LBB24_11:                              # %.preheader.242
	cmpw	%si, 4(%rdi)
	jne	.LBB24_13
# BB#12:
	movb	$2, %cl
	movl	$2, %edx
	jmp	.LBB24_6
.LBB24_13:
	movzbl	29(%rdi), %r11d
	cmpl	%r9d, %r11d
	jge	.LBB24_15
# BB#14:
	cmpl	$2, %ecx
	sete	%r10b
	movl	%eax, %edx
	andl	$240, %edx
	cmpl	$32, %edx
	sete	%dl
	orb	%r10b, %dl
	cmovel	%r11d, %r9d
	movl	$2, %edx
	cmovel	%edx, %r8d
.LBB24_15:                              # %.preheader.343
	cmpw	%si, 6(%rdi)
	jne	.LBB24_17
# BB#16:
	movb	$3, %cl
	movl	$3, %edx
	jmp	.LBB24_6
.LBB24_17:
	movzbl	36(%rdi), %r11d
	cmpl	%r9d, %r11d
	jge	.LBB24_19
# BB#18:
	cmpl	$3, %ecx
	sete	%r10b
	movl	%eax, %edx
	andl	$240, %edx
	cmpl	$48, %edx
	sete	%dl
	orb	%r10b, %dl
	cmovel	%r11d, %r9d
	movl	$3, %edx
	cmovel	%edx, %r8d
.LBB24_19:                              # %.preheader.444
	cmpw	%si, 8(%rdi)
	jne	.LBB24_21
# BB#20:
	movb	$4, %cl
	movl	$4, %edx
	jmp	.LBB24_6
.LBB24_21:
	movzbl	43(%rdi), %r11d
	cmpl	%r9d, %r11d
	jge	.LBB24_23
# BB#22:
	cmpl	$4, %ecx
	sete	%r10b
	movl	%eax, %edx
	andl	$240, %edx
	cmpl	$64, %edx
	sete	%dl
	orb	%r10b, %dl
	cmovel	%r11d, %r9d
	movl	$4, %edx
	cmovel	%edx, %r8d
.LBB24_23:                              # %.preheader.545
	cmpw	%si, 10(%rdi)
	jne	.LBB24_25
# BB#24:
	movb	$5, %cl
	movl	$5, %edx
	jmp	.LBB24_6
.LBB24_25:
	movzbl	50(%rdi), %r11d
	cmpl	%r9d, %r11d
	jge	.LBB24_27
# BB#26:
	cmpl	$5, %ecx
	sete	%r10b
	movl	%eax, %edx
	andl	$240, %edx
	cmpl	$80, %edx
	sete	%dl
	orb	%r10b, %dl
	cmovel	%r11d, %r9d
	movl	$5, %edx
	cmovel	%edx, %r8d
.LBB24_27:                              # %.preheader.646
	cmpw	%si, 12(%rdi)
	jne	.LBB24_29
# BB#28:
	movb	$6, %cl
	movl	$6, %edx
.LBB24_6:
	shlb	$4, %al
	orb	%cl, %al
	movb	%al, 14(%rdi)
	leaq	(,%rdx,8), %rax
	subq	%rdx, %rax
	leaq	15(%rdi,%rax), %rax
	retq
.LBB24_29:
	movzbl	57(%rdi), %edx
	cmpl	%r9d, %edx
	jge	.LBB24_31
# BB#30:
	andl	$240, %eax
	cmpl	$96, %eax
	movl	$6, %eax
	cmovel	%r8d, %eax
	cmpl	$6, %ecx
	cmovnel	%eax, %r8d
.LBB24_31:
	movl	%r8d, %eax
	orb	$-16, %al
	movb	%al, 14(%rdi)
	movslq	%r8d, %rax
	movw	%si, (%rdi,%rax,2)
	leaq	(,%rax,8), %rcx
	subq	%rax, %rcx
	leaq	15(%rdi,%rcx), %rax
	movb	$0, 21(%rdi,%rcx)
	movw	$0, 19(%rdi,%rcx)
	movl	$0, 15(%rdi,%rcx)
	retq
.Lfunc_end24:
	.size	_ZN10ContextMap1E3getEt, .Lfunc_end24-_ZN10ContextMap1E3getEt
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI25_0:
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.text
	.globl	_Z10matchModelR5Mixer
	.p2align	4, 0x90
	.type	_Z10matchModelR5Mixer,@function
_Z10matchModelR5Mixer:                  # @_Z10matchModelR5Mixer
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movb	_ZGVZ10matchModelR5MixerE1t(%rip), %al
	testb	%al, %al
	jne	.LBB25_11
# BB#1:
	movl	$_ZGVZ10matchModelR5MixerE1t, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB25_11
# BB#2:
	movb	level(%rip), %cl
	movl	$65536, %eax            # imm = 0x10000
	shll	%cl, %eax
	movl	%eax, _ZZ10matchModelR5MixerE1t+4(%rip)
	movl	%eax, _ZZ10matchModelR5MixerE1t(%rip)
	testl	%eax, %eax
	jle	.LBB25_3
# BB#4:
	shll	$2, %eax
	movl	programChecker(%rip), %ecx
	addl	%eax, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB25_6
# BB#5:
	movl	%ecx, programChecker+4(%rip)
.LBB25_6:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ10matchModelR5MixerE1t+8(%rip)
	testq	%rax, %rax
	je	.LBB25_7
# BB#9:
	movq	%rax, _ZZ10matchModelR5MixerE1t+16(%rip)
	jmp	.LBB25_10
.LBB25_3:
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZ10matchModelR5MixerE1t+8(%rip)
.LBB25_10:                              # %_ZN5ArrayIiLi0EEC2Ei.exit
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ10matchModelR5MixerE1t, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ10matchModelR5MixerE1t, %edi
	callq	__cxa_guard_release
.LBB25_11:
	movb	_ZGVZ10matchModelR5MixerE4scm1(%rip), %al
	testb	%al, %al
	jne	.LBB25_21
# BB#12:
	movl	$_ZGVZ10matchModelR5MixerE4scm1, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB25_21
# BB#13:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ10matchModelR5MixerE4scm1(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB25_15
# BB#14:
	movl	%eax, programChecker+4(%rip)
.LBB25_15:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ10matchModelR5MixerE4scm1+8(%rip)
	testq	%rax, %rax
	je	.LBB25_16
# BB#18:                                # %.lr.ph.i
	movq	%rax, _ZZ10matchModelR5MixerE4scm1+16(%rip)
	movl	$0, _ZZ10matchModelR5MixerE4scm1+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI25_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB25_19:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB25_19
# BB#20:                                # %middle.block
	movq	%rax, _ZZ10matchModelR5MixerE4scm1+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ10matchModelR5MixerE4scm1, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ10matchModelR5MixerE4scm1, %edi
	callq	__cxa_guard_release
.LBB25_21:
	cmpl	$0, bpos(%rip)
	je	.LBB25_23
# BB#22:                                # %._crit_edge
	movl	_ZZ10matchModelR5MixerE3len(%rip), %esi
	testl	%esi, %esi
	jne	.LBB25_35
	jmp	.LBB25_43
.LBB25_23:
	imull	$7976, _ZZ10matchModelR5MixerE1h(%rip), %edx # imm = 0x1F28
	movl	pos(%rip), %r10d
	leal	-1(%r10), %esi
	movl	buf(%rip), %r11d
	leal	-1(%r11), %ebp
	andl	%ebp, %esi
	movq	buf+16(%rip), %rcx
	movslq	%esi, %rbx
	movzbl	(%rcx,%rbx), %esi
	leal	1(%rdx,%rsi), %esi
	movl	_ZZ10matchModelR5MixerE1t(%rip), %edx
	decl	%edx
	andl	%esi, %edx
	movl	%edx, _ZZ10matchModelR5MixerE1h(%rip)
	movl	_ZZ10matchModelR5MixerE3len(%rip), %esi
	testl	%esi, %esi
	je	.LBB25_26
# BB#24:
	incl	%esi
	movl	%esi, _ZZ10matchModelR5MixerE3len(%rip)
	incl	_ZZ10matchModelR5MixerE3ptr(%rip)
	movq	_ZZ10matchModelR5MixerE1t+16(%rip), %r8
	movslq	%edx, %r9
.LBB25_33:                              # %.loopexit
	movl	%r10d, (%r8,%r9,4)
	movl	%esi, _ZZ10matchModelR5MixerE6result(%rip)
	movl	pos(%rip), %eax
	shll	$8, %eax
	movl	$-256, %ecx
	addl	_ZZ10matchModelR5MixerE4scm1(%rip), %ecx
	andl	%eax, %ecx
	movl	%ecx, _ZZ10matchModelR5MixerE4scm1+24(%rip)
	testl	%esi, %esi
	je	.LBB25_43
.LBB25_35:
	movl	pos(%rip), %edx
	decl	%edx
	movl	buf(%rip), %eax
	decl	%eax
	andl	%eax, %edx
	movq	buf+16(%rip), %rcx
	movslq	%edx, %rdx
	movb	(%rcx,%rdx), %bl
	movl	_ZZ10matchModelR5MixerE3ptr(%rip), %edx
	leal	-1(%rdx), %edi
	andl	%eax, %edi
	movslq	%edi, %rdi
	cmpb	(%rcx,%rdi), %bl
	jne	.LBB25_42
# BB#36:
	andl	%eax, %edx
	movslq	%edx, %rax
	movzbl	(%rcx,%rax), %eax
	leal	256(%rax), %edi
	movl	bpos(%rip), %edx
	movl	$8, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	cmpl	%edi, c0(%rip)
	jne	.LBB25_42
# BB#37:
	cmpl	$65535, %esi            # imm = 0xFFFF
	jl	.LBB25_39
# BB#38:
	movl	$65534, _ZZ10matchModelR5MixerE3len(%rip) # imm = 0xFFFE
	movl	$65534, %esi            # imm = 0xFFFE
.LBB25_39:
	movl	$7, %ecx
	subl	%edx, %ecx
	movq	ilog+16(%rip), %rdx
	movzwl	%si, %edi
	movzbl	(%rdx,%rdi), %edi
	shll	$2, %edi
	btl	%ecx, %eax
	jae	.LBB25_41
# BB#40:
	leaq	96(%r14), %rax
	movslq	96(%r14), %rbp
	leal	1(%rbp), %edx
	movl	%edx, 96(%r14)
	movq	32(%r14), %rcx
	movw	%di, (%rcx,%rbp,2)
	cmpl	$32, %esi
	movw	$32, %di
	cmovlw	%si, %di
	shll	$6, %edi
	jmp	.LBB25_45
.LBB25_42:
	movl	$0, _ZZ10matchModelR5MixerE3len(%rip)
	movslq	96(%r14), %rsi
	leal	1(%rsi), %edx
	movl	%edx, 96(%r14)
	movq	32(%r14), %rcx
	leaq	96(%r14), %rax
	jmp	.LBB25_44
.LBB25_43:
	leaq	96(%r14), %rax
	movslq	96(%r14), %rsi
	leal	1(%rsi), %edx
	movl	%edx, 96(%r14)
	movq	32(%r14), %rcx
.LBB25_44:
	movw	$0, (%rcx,%rsi,2)
	xorl	%edi, %edi
.LBB25_45:
	leal	1(%rdx), %esi
	movl	%esi, (%rax)
	movslq	%edx, %rdx
	movw	%di, (%rcx,%rdx,2)
	movl	y(%rip), %esi
	shll	$16, %esi
	movq	_ZZ10matchModelR5MixerE4scm1+32(%rip), %rdi
	movzwl	(%rdi), %ebp
	orl	$64, %esi
	subl	%ebp, %esi
	shrl	$7, %esi
	addl	%ebp, %esi
	movw	%si, (%rdi)
	movslq	c0(%rip), %rsi
	movslq	_ZZ10matchModelR5MixerE4scm1+24(%rip), %rdi
	addq	%rsi, %rdi
	movq	_ZZ10matchModelR5MixerE4scm1+16(%rip), %rsi
	leaq	(%rsi,%rdi,2), %rbp
	movq	%rbp, _ZZ10matchModelR5MixerE4scm1+32(%rip)
	movzwl	(%rsi,%rdi,2), %esi
	shrq	$3, %rsi
	andl	$8190, %esi             # imm = 0x1FFE
	movq	stretch+16(%rip), %rdi
	movzwl	(%rdi,%rsi), %esi
	leal	2(%rdx), %edi
	movl	%edi, (%rax)
	movw	%si, 2(%rcx,%rdx,2)
	movl	_ZZ10matchModelR5MixerE6result(%rip), %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB25_26:
	movq	_ZZ10matchModelR5MixerE1t+16(%rip), %r8
	movslq	%edx, %r9
	movl	(%r8,%r9,4), %edx
	movl	%edx, _ZZ10matchModelR5MixerE3ptr(%rip)
	xorl	%esi, %esi
	testl	%edx, %edx
	je	.LBB25_33
# BB#27:
	movl	%r10d, %eax
	subl	%edx, %eax
	cmpl	%r11d, %eax
	jge	.LBB25_33
# BB#28:                                # %.preheader.preheader
	movb	(%rcx,%rbx), %al
	leal	-1(%rdx), %ebx
	andl	%ebp, %ebx
	movslq	%ebx, %rbx
	cmpb	(%rcx,%rbx), %al
	jne	.LBB25_33
# BB#29:                                # %.preheader.preheader29
	addl	$-2, %edx
	leal	-2(%r10), %eax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB25_30:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	incl	%esi
	cmpl	$65533, %esi            # imm = 0xFFFD
	jg	.LBB25_32
# BB#31:                                # %.preheader
                                        #   in Loop: Header=BB25_30 Depth=1
	movl	%ebp, %edi
	andl	%eax, %edi
	movslq	%edi, %rdi
	movl	%edx, %ebx
	andl	%ebp, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rcx,%rbx), %ebx
	decl	%edx
	decl	%eax
	cmpb	%bl, (%rcx,%rdi)
	je	.LBB25_30
.LBB25_32:                              # %.loopexit.loopexit
	movl	%esi, _ZZ10matchModelR5MixerE3len(%rip)
	jmp	.LBB25_33
.LBB25_41:
	negl	%edi
	leaq	96(%r14), %rax
	movslq	96(%r14), %rbp
	leal	1(%rbp), %edx
	movl	%edx, 96(%r14)
	movq	32(%r14), %rcx
	movw	%di, (%rcx,%rbp,2)
	cmpl	$33, %esi
	movl	$32, %edi
	cmovll	%esi, %edi
	shll	$6, %edi
	negl	%edi
	jmp	.LBB25_45
.LBB25_16:
.Ltmp39:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp40:
# BB#17:                                # %.noexc12
.LBB25_7:
.Ltmp36:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp37:
# BB#8:                                 # %.noexc
.LBB25_46:
.Ltmp38:
	movq	%rax, %rbx
	movl	$_ZGVZ10matchModelR5MixerE1t, %edi
	jmp	.LBB25_47
.LBB25_25:
.Ltmp41:
	movq	%rax, %rbx
	movl	$_ZGVZ10matchModelR5MixerE4scm1, %edi
.LBB25_47:
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_Z10matchModelR5Mixer, .Lfunc_end25-_Z10matchModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp39-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin3   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin3   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end25-.Ltmp37    #   Call between .Ltmp37 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN25SmallStationaryContextMapD2Ev,"axG",@progbits,_ZN25SmallStationaryContextMapD2Ev,comdat
	.weak	_ZN25SmallStationaryContextMapD2Ev
	.p2align	4, 0x90
	.type	_ZN25SmallStationaryContextMapD2Ev,@function
_ZN25SmallStationaryContextMapD2Ev:     # @_ZN25SmallStationaryContextMapD2Ev
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	addl	%ecx, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB26_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB26_2:                               # %_ZN5ArrayItLi0EED2Ev.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end26:
	.size	_ZN25SmallStationaryContextMapD2Ev, .Lfunc_end26-_ZN25SmallStationaryContextMapD2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI27_0:
	.long	256                     # 0x100
	.long	0                       # 0x0
	.long	256                     # 0x100
	.long	256                     # 0x100
.LCPI27_1:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.text
	.globl	_Z8picModelR5Mixer
	.p2align	4, 0x90
	.type	_Z8picModelR5Mixer,@function
_Z8picModelR5Mixer:                     # @_Z8picModelR5Mixer
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 48
.Lcfi94:
	.cfi_offset %rbx, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movb	_ZGVZ8picModelR5MixerE1t(%rip), %al
	testb	%al, %al
	jne	.LBB27_8
# BB#1:
	movl	$_ZGVZ8picModelR5MixerE1t, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB27_8
# BB#2:
	movabsq	$283674000032256, %rax  # imm = 0x1020000010200
	movq	%rax, _ZZ8picModelR5MixerE1t(%rip)
	movl	$66048, %eax            # imm = 0x10200
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB27_4
# BB#3:
	movl	%eax, programChecker+4(%rip)
.LBB27_4:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movl	$66048, %edi            # imm = 0x10200
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8picModelR5MixerE1t+8(%rip)
	testq	%rax, %rax
	je	.LBB27_5
# BB#7:
	movq	%rax, _ZZ8picModelR5MixerE1t+16(%rip)
	movl	$_ZN5ArrayIhLi0EED2Ev, %edi
	movl	$_ZZ8picModelR5MixerE1t, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8picModelR5MixerE1t, %edi
	callq	__cxa_guard_release
.LBB27_8:
	movb	_ZGVZ8picModelR5MixerE2sm(%rip), %al
	testb	%al, %al
	jne	.LBB27_33
# BB#9:
	movl	$_ZGVZ8picModelR5MixerE2sm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB27_33
# BB#10:                                # %.preheader22.preheader
	movaps	.LCPI27_0(%rip), %xmm0  # xmm0 = [256,0,256,256]
	movaps	%xmm0, _ZZ8picModelR5MixerE2sm(%rip)
	movl	programChecker(%rip), %r14d
	leal	1024(%r14), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %ebp
	cmpl	%ebp, %eax
	jle	.LBB27_12
# BB#11:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %ebp
.LBB27_12:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i
	movl	$1024, %edi             # imm = 0x400
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8picModelR5MixerE2sm+16(%rip)
	testq	%rax, %rax
	je	.LBB27_13
# BB#24:                                # %vector.body
	movq	%rax, _ZZ8picModelR5MixerE2sm+24(%rip)
	movaps	.LCPI27_1(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	movups	%xmm0, 672(%rax)
	movups	%xmm0, 688(%rax)
	movups	%xmm0, 704(%rax)
	movups	%xmm0, 720(%rax)
	movups	%xmm0, 736(%rax)
	movups	%xmm0, 752(%rax)
	movups	%xmm0, 768(%rax)
	movups	%xmm0, 784(%rax)
	movups	%xmm0, 800(%rax)
	movups	%xmm0, 816(%rax)
	movups	%xmm0, 832(%rax)
	movups	%xmm0, 848(%rax)
	movups	%xmm0, 864(%rax)
	movups	%xmm0, 880(%rax)
	movups	%xmm0, 896(%rax)
	movups	%xmm0, 912(%rax)
	movups	%xmm0, 928(%rax)
	movups	%xmm0, 944(%rax)
	movups	%xmm0, 960(%rax)
	movups	%xmm0, 976(%rax)
	movups	%xmm0, 992(%rax)
	movups	%xmm0, 1008(%rax)
	movaps	.LCPI27_0(%rip), %xmm0  # xmm0 = [256,0,256,256]
	movaps	%xmm0, _ZZ8picModelR5MixerE2sm+32(%rip)
	leal	2048(%r14), %eax
	movl	%eax, programChecker(%rip)
	cmpl	%ebp, %eax
	jle	.LBB27_26
# BB#25:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %ebp
.LBB27_26:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i.1
	movl	$1, %ebx
	movl	$1024, %edi             # imm = 0x400
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8picModelR5MixerE2sm+48(%rip)
	testq	%rax, %rax
	je	.LBB27_27
# BB#28:                                # %vector.body30
	movq	%rax, _ZZ8picModelR5MixerE2sm+56(%rip)
	movaps	.LCPI27_1(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	movups	%xmm0, 672(%rax)
	movups	%xmm0, 688(%rax)
	movups	%xmm0, 704(%rax)
	movups	%xmm0, 720(%rax)
	movups	%xmm0, 736(%rax)
	movups	%xmm0, 752(%rax)
	movups	%xmm0, 768(%rax)
	movups	%xmm0, 784(%rax)
	movups	%xmm0, 800(%rax)
	movups	%xmm0, 816(%rax)
	movups	%xmm0, 832(%rax)
	movups	%xmm0, 848(%rax)
	movups	%xmm0, 864(%rax)
	movups	%xmm0, 880(%rax)
	movups	%xmm0, 896(%rax)
	movups	%xmm0, 912(%rax)
	movups	%xmm0, 928(%rax)
	movups	%xmm0, 944(%rax)
	movups	%xmm0, 960(%rax)
	movups	%xmm0, 976(%rax)
	movups	%xmm0, 992(%rax)
	movups	%xmm0, 1008(%rax)
	movaps	.LCPI27_0(%rip), %xmm0  # xmm0 = [256,0,256,256]
	movaps	%xmm0, _ZZ8picModelR5MixerE2sm+64(%rip)
	addl	$3072, %r14d            # imm = 0xC00
	movl	%r14d, programChecker(%rip)
	cmpl	%ebp, %r14d
	jle	.LBB27_30
# BB#29:
	movl	%r14d, programChecker+4(%rip)
.LBB27_30:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i.2
	movl	$1, %ebx
	movl	$1024, %edi             # imm = 0x400
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8picModelR5MixerE2sm+80(%rip)
	testq	%rax, %rax
	je	.LBB27_31
# BB#32:                                # %vector.body43
	movq	%rax, _ZZ8picModelR5MixerE2sm+88(%rip)
	movaps	.LCPI27_1(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	movups	%xmm0, 672(%rax)
	movups	%xmm0, 688(%rax)
	movups	%xmm0, 704(%rax)
	movups	%xmm0, 720(%rax)
	movups	%xmm0, 736(%rax)
	movups	%xmm0, 752(%rax)
	movups	%xmm0, 768(%rax)
	movups	%xmm0, 784(%rax)
	movups	%xmm0, 800(%rax)
	movups	%xmm0, 816(%rax)
	movups	%xmm0, 832(%rax)
	movups	%xmm0, 848(%rax)
	movups	%xmm0, 864(%rax)
	movups	%xmm0, 880(%rax)
	movups	%xmm0, 896(%rax)
	movups	%xmm0, 912(%rax)
	movups	%xmm0, 928(%rax)
	movups	%xmm0, 944(%rax)
	movups	%xmm0, 960(%rax)
	movups	%xmm0, 976(%rax)
	movups	%xmm0, 992(%rax)
	movups	%xmm0, 1008(%rax)
	movl	$__cxx_global_array_dtor, %edi
	xorl	%esi, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8picModelR5MixerE2sm, %edi
	callq	__cxa_guard_release
.LBB27_33:                              # %.preheader.preheader
	movq	_ZZ8picModelR5MixerE1t+16(%rip), %rax
	movslq	_ZZ8picModelR5MixerE3cxt(%rip), %rcx
	movzbl	(%rax,%rcx), %edx
	movslq	y(%rip), %rsi
	movb	_ZL11State_table(%rsi,%rdx,4), %dl
	movb	%dl, (%rax,%rcx)
	movq	_ZZ8picModelR5MixerE1t+16(%rip), %rax
	movslq	_ZZ8picModelR5MixerE3cxt+4(%rip), %rcx
	movzbl	(%rax,%rcx), %edx
	movslq	y(%rip), %rsi
	movb	_ZL11State_table(%rsi,%rdx,4), %dl
	movb	%dl, (%rax,%rcx)
	movq	_ZZ8picModelR5MixerE1t+16(%rip), %rax
	movslq	_ZZ8picModelR5MixerE3cxt+8(%rip), %rcx
	movzbl	(%rax,%rcx), %edx
	movslq	y(%rip), %rsi
	movb	_ZL11State_table(%rsi,%rdx,4), %dl
	movb	%dl, (%rax,%rcx)
	movl	_ZZ8picModelR5MixerE2r0(%rip), %eax
	movl	y(%rip), %edx
	leal	(%rdx,%rax,2), %r9d
	movl	%r9d, _ZZ8picModelR5MixerE2r0(%rip)
	movl	_ZZ8picModelR5MixerE2r1(%rip), %edi
	movl	pos(%rip), %ebx
	leal	-215(%rbx), %eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %eax
	movq	buf+16(%rip), %r10
	cltq
	movzbl	(%r10,%rax), %eax
	movl	$7, %ecx
	subl	bpos(%rip), %ecx
	shrl	%cl, %eax
	andl	$1, %eax
	leal	(%rdi,%rdi), %r8d
	leal	(%rax,%rdi,2), %eax
	movl	%eax, _ZZ8picModelR5MixerE2r1(%rip)
	movl	_ZZ8picModelR5MixerE2r2(%rip), %ebp
	leal	-431(%rbx), %eax
	andl	%esi, %eax
	cltq
	movzbl	(%r10,%rax), %eax
	shrl	%cl, %eax
	andl	$1, %eax
	leal	(%rax,%rbp,2), %eax
	movl	%eax, _ZZ8picModelR5MixerE2r2(%rip)
	movl	_ZZ8picModelR5MixerE2r3(%rip), %r11d
	addl	$-647, %ebx             # imm = 0xFD79
	andl	%esi, %ebx
	movslq	%ebx, %rsi
	movzbl	(%r10,%rsi), %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	andl	$1, %esi
	leal	(%rsi,%r11,2), %ecx
	movl	%ecx, _ZZ8picModelR5MixerE2r3(%rip)
	movl	%r9d, %ecx
	andl	$7, %ecx
	shrl	$3, %edi
	movl	%edi, %esi
	andl	$56, %esi
	orl	%ecx, %esi
	movl	%ebp, %ebx
	shrl	$2, %ebx
	andl	$192, %ebx
	orl	%esi, %ebx
	movl	%ebx, _ZZ8picModelR5MixerE3cxt(%rip)
	movl	%r9d, %ecx
	andl	$1, %ecx
	andl	$62, %edi
	movl	%ebp, %esi
	shrl	%esi
	andl	$64, %esi
	movl	%r11d, %eax
	andl	$128, %eax
	orl	%ecx, %edi
	orl	%esi, %edi
	leal	256(%rax,%rdi), %r10d
	movl	%r10d, _ZZ8picModelR5MixerE3cxt+4(%rip)
	andl	$63, %r9d
	andl	$16382, %r8d            # imm = 0x3FFE
	xorl	%r9d, %r8d
	shll	$3, %ebp
	andl	$32512, %ebp            # imm = 0x7F00
	shll	$6, %r11d
	andl	$63488, %r11d           # imm = 0xF800
	xorl	%ebp, %r11d
	xorl	%r8d, %r11d
	leaq	512(%r11), %rax
	movl	%eax, _ZZ8picModelR5MixerE3cxt+8(%rip)
	movq	_ZZ8picModelR5MixerE1t+16(%rip), %r8
	movzbl	(%r8,%rbx), %r9d
	movq	_ZZ8picModelR5MixerE2sm+24(%rip), %rax
	movslq	_ZZ8picModelR5MixerE2sm+4(%rip), %rbp
	movl	(%rax,%rbp,4), %ebx
	movl	%ebx, %edi
	shrl	$10, %edi
	leal	1(%rbx), %ecx
	movl	%ebx, %esi
	orl	$1023, %esi             # imm = 0x3FF
	andl	$1023, %ebx             # imm = 0x3FF
	cmpl	$1023, %ebx             # imm = 0x3FF
	cmovnel	%ecx, %esi
	shll	$22, %edx
	subl	%edi, %edx
	sarl	$3, %edx
	imull	_ZL2dt(,%rbx,4), %edx
	andl	$-1024, %edx            # imm = 0xFC00
	addl	%esi, %edx
	movl	%edx, (%rax,%rbp,4)
	movl	%r9d, _ZZ8picModelR5MixerE2sm+4(%rip)
	movl	(%rax,%r9,4), %eax
	shrl	$20, %eax
	movq	stretch+16(%rip), %r9
	movzwl	(%r9,%rax,2), %eax
	movslq	96(%r15), %rcx
	leal	1(%rcx), %esi
	movl	%esi, 96(%r15)
	movq	32(%r15), %r14
	movw	%ax, (%r14,%rcx,2)
	movzbl	(%r8,%r10), %edi
	movq	_ZZ8picModelR5MixerE2sm+56(%rip), %rax
	movslq	_ZZ8picModelR5MixerE2sm+36(%rip), %rcx
	movl	(%rax,%rcx,4), %ebp
	movl	%ebp, %ebx
	shrl	$10, %ebx
	leal	1(%rbp), %edx
	movl	%ebp, %esi
	orl	$1023, %esi             # imm = 0x3FF
	andl	$1023, %ebp             # imm = 0x3FF
	cmpl	$1023, %ebp             # imm = 0x3FF
	cmovnel	%edx, %esi
	movl	y(%rip), %edx
	shll	$22, %edx
	subl	%ebx, %edx
	sarl	$3, %edx
	imull	_ZL2dt(,%rbp,4), %edx
	andl	$-1024, %edx            # imm = 0xFC00
	addl	%esi, %edx
	movl	%edx, (%rax,%rcx,4)
	movl	%edi, _ZZ8picModelR5MixerE2sm+36(%rip)
	movl	(%rax,%rdi,4), %eax
	shrl	$20, %eax
	movzwl	(%r9,%rax,2), %eax
	movslq	96(%r15), %rcx
	leal	1(%rcx), %edx
	movl	%edx, 96(%r15)
	movw	%ax, (%r14,%rcx,2)
	movzbl	512(%r8,%r11), %eax
	movq	_ZZ8picModelR5MixerE2sm+88(%rip), %rcx
	movslq	_ZZ8picModelR5MixerE2sm+68(%rip), %rdx
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, %edi
	shrl	$10, %edi
	leal	1(%rsi), %ebp
	movl	%esi, %ebx
	orl	$1023, %ebx             # imm = 0x3FF
	andl	$1023, %esi             # imm = 0x3FF
	cmpl	$1023, %esi             # imm = 0x3FF
	cmovnel	%ebp, %ebx
	movl	y(%rip), %ebp
	shll	$22, %ebp
	subl	%edi, %ebp
	sarl	$3, %ebp
	imull	_ZL2dt(,%rsi,4), %ebp
	andl	$-1024, %ebp            # imm = 0xFC00
	addl	%ebx, %ebp
	movl	%ebp, (%rcx,%rdx,4)
	movl	%eax, _ZZ8picModelR5MixerE2sm+68(%rip)
	movl	(%rcx,%rax,4), %eax
	shrl	$20, %eax
	movzwl	(%r9,%rax,2), %eax
	movslq	96(%r15), %rcx
	leal	1(%rcx), %edx
	movl	%edx, 96(%r15)
	movw	%ax, (%r14,%rcx,2)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB27_5:
.Ltmp42:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp43:
# BB#6:                                 # %.noexc
.LBB27_13:
	movl	$_ZZ8picModelR5MixerE2sm, %ebp
	xorl	%ebx, %ebx
	jmp	.LBB27_14
.LBB27_27:
	movl	$_ZZ8picModelR5MixerE2sm+32, %ebp
	jmp	.LBB27_14
.LBB27_31:
	movl	$_ZZ8picModelR5MixerE2sm+64, %ebp
.LBB27_14:
.Ltmp45:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp46:
# BB#15:                                # %.noexc17
.LBB27_16:
.Ltmp44:
	movq	%rax, %r14
	movl	$_ZGVZ8picModelR5MixerE1t, %edi
	jmp	.LBB27_23
.LBB27_17:
.Ltmp47:
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB27_22
# BB#18:                                # %.preheader21.preheader
	movl	$_ZZ8picModelR5MixerE2sm, %r15d
.LBB27_19:                              # %.preheader21
                                        # =>This Inner Loop Header: Depth=1
	leaq	-32(%rbp), %rbx
	movl	-24(%rbp), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB27_21
# BB#20:                                #   in Loop: Header=BB27_19 Depth=1
	movl	%eax, programChecker+4(%rip)
.LBB27_21:                              #   in Loop: Header=BB27_19 Depth=1
	movq	-16(%rbp), %rdi
	callq	free
	cmpq	%rbx, %r15
	movq	%rbx, %rbp
	jne	.LBB27_19
.LBB27_22:                              # %.loopexit
	movl	$_ZGVZ8picModelR5MixerE2sm, %edi
.LBB27_23:
	callq	__cxa_guard_abort
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end27:
	.size	_Z8picModelR5Mixer, .Lfunc_end27-_Z8picModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp42-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin4   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin4   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Lfunc_end27-.Ltmp46    #   Call between .Ltmp46 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	__cxx_global_array_dtor,@function
__cxx_global_array_dtor:                # @__cxx_global_array_dtor
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 16
	movl	_ZZ8picModelR5MixerE2sm+72(%rip), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB28_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB28_2:
	movq	_ZZ8picModelR5MixerE2sm+80(%rip), %rdi
	callq	free
	movl	_ZZ8picModelR5MixerE2sm+40(%rip), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB28_4
# BB#3:
	movl	%eax, programChecker+4(%rip)
.LBB28_4:
	movq	_ZZ8picModelR5MixerE2sm+48(%rip), %rdi
	callq	free
	movl	_ZZ8picModelR5MixerE2sm+8(%rip), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB28_6
# BB#5:
	movl	%eax, programChecker+4(%rip)
.LBB28_6:
	movq	_ZZ8picModelR5MixerE2sm+16(%rip), %rdi
	popq	%rax
	jmp	free                    # TAILCALL
.Lfunc_end28:
	.size	__cxx_global_array_dtor, .Lfunc_end28-__cxx_global_array_dtor
	.cfi_endproc

	.text
	.globl	_Z9wordModelR5Mixer
	.p2align	4, 0x90
	.type	_Z9wordModelR5Mixer,@function
_Z9wordModelR5Mixer:                    # @_Z9wordModelR5Mixer
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movb	_ZGVZ9wordModelR5MixerE2cm(%rip), %al
	testb	%al, %al
	jne	.LBB29_4
# BB#1:
	movl	$_ZGVZ9wordModelR5MixerE2cm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB29_4
# BB#2:
	movb	level(%rip), %cl
	movl	$1048576, %esi          # imm = 0x100000
	shll	%cl, %esi
.Ltmp48:
	movl	$_ZZ9wordModelR5MixerE2cm, %edi
	movl	$20, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp49:
# BB#3:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ9wordModelR5MixerE2cm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9wordModelR5MixerE2cm, %edi
	callq	__cxa_guard_release
.LBB29_4:
	movl	bpos(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB29_6
# BB#5:                                 # %._crit_edge41
	movq	buf+16(%rip), %rdi
	jmp	.LBB29_15
.LBB29_6:
	movzbl	c4(%rip), %eax
	leal	-65(%rax), %edx
	leal	32(%rax), %ecx
	cmpl	$26, %edx
	cmovael	%eax, %ecx
	cmpl	$127, %ecx
	movl	_ZZ9wordModelR5MixerE5word0(%rip), %eax
	ja	.LBB29_8
# BB#7:
	leal	-97(%rcx), %edx
	cmpl	$26, %edx
	jb	.LBB29_8
# BB#9:
	xorl	%r10d, %r10d
	testl	%eax, %eax
	je	.LBB29_11
# BB#10:
	imull	$23, _ZZ9wordModelR5MixerE5word4(%rip), %edx
	movl	%edx, _ZZ9wordModelR5MixerE5word5(%rip)
	imull	$19, _ZZ9wordModelR5MixerE5word3(%rip), %edx
	movl	%edx, _ZZ9wordModelR5MixerE5word4(%rip)
	movl	_ZZ9wordModelR5MixerE5word2(%rip), %edx
	movl	%edx, %edi
	shll	$4, %edi
	addl	%edx, %edi
	movl	%edi, _ZZ9wordModelR5MixerE5word3(%rip)
	imull	$13, _ZZ9wordModelR5MixerE5word1(%rip), %edx
	movl	%edx, _ZZ9wordModelR5MixerE5word2(%rip)
	imull	$11, %eax, %eax
	movl	%eax, _ZZ9wordModelR5MixerE5word1(%rip)
	movl	$0, _ZZ9wordModelR5MixerE5word0(%rip)
	cmpl	$10, %ecx
	je	.LBB29_13
	jmp	.LBB29_12
.LBB29_8:
	imull	$8416, %eax, %r10d      # imm = 0x20E0
	addl	%ecx, %r10d
	movl	%r10d, _ZZ9wordModelR5MixerE5word0(%rip)
	imull	$15952, _ZZ9wordModelR5MixerE5text0(%rip), %eax # imm = 0x3E50
	addl	%ecx, %eax
	movl	%eax, _ZZ9wordModelR5MixerE5text0(%rip)
.LBB29_11:
	cmpl	$10, %ecx
	jne	.LBB29_12
.LBB29_13:
	movl	_ZZ9wordModelR5MixerE2nl(%rip), %eax
	movl	%eax, _ZZ9wordModelR5MixerE3nl1(%rip)
	movl	pos(%rip), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, _ZZ9wordModelR5MixerE2nl(%rip)
	jmp	.LBB29_14
.LBB29_12:                              # %._crit_edge
	movl	pos(%rip), %edx
	movl	_ZZ9wordModelR5MixerE2nl(%rip), %ecx
	movl	_ZZ9wordModelR5MixerE3nl1(%rip), %eax
.LBB29_14:
	movl	%edx, %edi
	subl	%ecx, %edi
	cmpl	$256, %edi              # imm = 0x100
	movl	$255, %r9d
	cmovll	%edi, %r9d
	addl	%r9d, %eax
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %eax
	movq	buf+16(%rip), %rbx
	cltq
	movzbl	(%rbx,%rax), %r8d
	imull	$271, %r10d, %eax       # imm = 0x10F
	decl	%edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rbx,%rdx), %r11d
	addl	%eax, %r11d
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %edi
	movl	%edi, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %r11d, %edi # imm = 0x3ADE68B3
	addl	%eax, %edi
	roll	$16, %edi
	imull	$123456791, %edi, %ebx  # imm = 0x75BCD17
	addl	%eax, %ebx
	movq	_ZZ9wordModelR5MixerE2cm+96(%rip), %rdi
	movl	%ebx, (%rdi,%rax,4)
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %ebx
	movl	%ebx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %r10d, %ebx # imm = 0x3ADE68B3
	addl	%eax, %ebx
	roll	$16, %ebx
	imull	$123456791, %ebx, %ebx  # imm = 0x75BCD17
	addl	%eax, %ebx
	movl	%ebx, (%rdi,%rax,4)
	movl	_ZZ9wordModelR5MixerE5word1(%rip), %ebx
	leal	(%rbx,%r11), %eax
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %esi  # imm = 0x3ADE68B3
	addl	%ecx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%ecx, %esi
	movl	%esi, (%rdi,%rcx,4)
	movl	%ebx, %ecx
	shll	$5, %ecx
	subl	%ebx, %ecx
	addl	%r10d, %ecx
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rsi
	leal	1(%rsi), %ebx
	movl	%ebx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%esi, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%esi, %ecx
	movl	%ecx, (%rdi,%rsi,4)
	movl	_ZZ9wordModelR5MixerE5word2(%rip), %esi
	imull	$29, %esi, %ecx
	addl	%eax, %ecx
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %ebx
	movl	%ebx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%rdi,%rax,4)
	movl	_ZZ9wordModelR5MixerE5text0(%rip), %eax
	movl	%eax, %ecx
	andl	$16777215, %ecx         # imm = 0xFFFFFF
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rbx
	leal	1(%rbx), %edx
	movl	%edx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%ebx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%ebx, %ecx
	movl	%ecx, (%rdi,%rbx,4)
	andl	$1048575, %eax          # imm = 0xFFFFF
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rdi,%rcx,4)
	addl	%r11d, %esi
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %esi, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movq	_ZZ9wordModelR5MixerE2cm+96(%rip), %rsi
	movl	%ecx, (%rsi,%rax,4)
	movl	_ZZ9wordModelR5MixerE5word3(%rip), %eax
	addl	%r11d, %eax
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rsi,%rcx,4)
	movl	_ZZ9wordModelR5MixerE5word4(%rip), %eax
	addl	%r11d, %eax
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rsi,%rcx,4)
	movl	_ZZ9wordModelR5MixerE5word5(%rip), %eax
	addl	%r11d, %eax
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rsi,%rcx,4)
	movl	pos(%rip), %eax
	leal	-1(%rax), %ecx
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %ecx
	movq	buf+16(%rip), %rdi
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	leal	-3(%rax), %ebx
	andl	%edx, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rdi,%rbx), %ebx
	shll	$8, %ebx
	orl	%ecx, %ebx
	addl	$-5, %eax
	andl	%edx, %eax
	cltq
	movzbl	(%rdi,%rax), %eax
	shll	$16, %eax
	orl	%ebx, %eax
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rsi,%rcx,4)
	movl	pos(%rip), %eax
	leal	-2(%rax), %ecx
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	leal	-4(%rax), %esi
	andl	%edx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdi,%rsi), %esi
	shll	$8, %esi
	orl	%ecx, %esi
	addl	$-6, %eax
	andl	%edx, %eax
	cltq
	movzbl	(%rdi,%rax), %eax
	shll	$16, %eax
	orl	%esi, %eax
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movq	_ZZ9wordModelR5MixerE2cm+96(%rip), %r10
	movl	%eax, (%r10,%rcx,4)
	movl	_ZZ9wordModelR5MixerE5word1(%rip), %eax
	addl	%r11d, %eax
	movl	_ZZ9wordModelR5MixerE5word3(%rip), %ecx
	addl	%ecx, %eax
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rdx
	leal	1(%rdx), %edi
	movl	%edi, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%edx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%edx, %eax
	movl	%eax, (%r10,%rdx,4)
	addl	_ZZ9wordModelR5MixerE5word2(%rip), %r11d
	addl	%ecx, %r11d
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %r11d, %ecx # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%r10,%rax,4)
	movl	%r9d, %edx
	imull	$987654323, %r9d, %eax  # imm = 0x3ADE68B3
	shll	$16, %r9d
	movl	pos(%rip), %ecx
	decl	%ecx
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ecx, %ebx
	movq	buf+16(%rip), %rdi
	movslq	%ebx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	orl	%r8d, %r9d
	orl	%ecx, %r9d
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %ebx
	movl	%ebx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %r9d, %ebx  # imm = 0x3ADE68B3
	addl	%ecx, %ebx
	roll	$16, %ebx
	imull	$123456791, %ebx, %ebx  # imm = 0x75BCD17
	addl	%ecx, %ebx
	movl	%ebx, (%r10,%rcx,4)
	movl	pos(%rip), %ecx
	decl	%ecx
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ecx, %ebx
	movslq	%ebx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	shll	$8, %ecx
	orl	%r8d, %ecx
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rbx
	leal	1(%rbx), %esi
	movl	%esi, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%ebx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%ebx, %ecx
	movl	%ecx, (%r10,%rbx,4)
	shll	$8, %edx
	movl	pos(%rip), %ecx
	decl	%ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%ecx, %esi
	movslq	%esi, %rcx
	movzbl	(%rdi,%rcx), %ecx
	orl	%edx, %ecx
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ9wordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movq	_ZZ9wordModelR5MixerE2cm+96(%rip), %rsi
	movl	%ecx, (%rsi,%rdx,4)
	movslq	_ZZ9wordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9wordModelR5MixerE2cm+136(%rip)
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rsi,%rcx,4)
	movl	bpos(%rip), %ecx
.LBB29_15:
	movl	c0(%rip), %edx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movslq	%esi, %rax
	movzbl	(%rdi,%rax), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ9wordModelR5MixerE2cm, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN10ContextMap4mix1ER5Mixeriiii # TAILCALL
.LBB29_16:
.Ltmp50:
	movq	%rax, %rbx
	movl	$_ZGVZ9wordModelR5MixerE2cm, %edi
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end29:
	.size	_Z9wordModelR5Mixer, .Lfunc_end29-_Z9wordModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp48-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin5   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end29-.Ltmp49    #   Call between .Ltmp49 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z11recordModelR5Mixer
	.p2align	4, 0x90
	.type	_Z11recordModelR5Mixer,@function
_Z11recordModelR5Mixer:                 # @_Z11recordModelR5Mixer
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movb	_ZGVZ11recordModelR5MixerE2cm(%rip), %al
	testb	%al, %al
	jne	.LBB30_4
# BB#1:
	movl	$_ZGVZ11recordModelR5MixerE2cm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB30_4
# BB#2:
.Ltmp51:
	movl	$_ZZ11recordModelR5MixerE2cm, %edi
	movl	$32768, %esi            # imm = 0x8000
	movl	$3, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp52:
# BB#3:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ11recordModelR5MixerE2cm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ11recordModelR5MixerE2cm, %edi
	callq	__cxa_guard_release
.LBB30_4:
	movb	_ZGVZ11recordModelR5MixerE2cn(%rip), %al
	testb	%al, %al
	jne	.LBB30_8
# BB#5:
	movl	$_ZGVZ11recordModelR5MixerE2cn, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB30_8
# BB#6:
.Ltmp54:
	movl	$_ZZ11recordModelR5MixerE2cn, %edi
	movl	$16384, %esi            # imm = 0x4000
	movl	$3, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp55:
# BB#7:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ11recordModelR5MixerE2cn, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ11recordModelR5MixerE2cn, %edi
	callq	__cxa_guard_release
.LBB30_8:
	movb	_ZGVZ11recordModelR5MixerE2co(%rip), %al
	testb	%al, %al
	jne	.LBB30_12
# BB#9:
	movl	$_ZGVZ11recordModelR5MixerE2co, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB30_12
# BB#10:
.Ltmp57:
	movl	$_ZZ11recordModelR5MixerE2co, %edi
	movl	$65536, %esi            # imm = 0x10000
	movl	$3, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp58:
# BB#11:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ11recordModelR5MixerE2co, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ11recordModelR5MixerE2co, %edi
	callq	__cxa_guard_release
.LBB30_12:
	movb	_ZGVZ11recordModelR5MixerE2cp(%rip), %al
	testb	%al, %al
	jne	.LBB30_16
# BB#13:
	movl	$_ZGVZ11recordModelR5MixerE2cp, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB30_16
# BB#14:
	movb	level(%rip), %cl
	movl	$65536, %esi            # imm = 0x10000
	shll	%cl, %esi
.Ltmp60:
	movl	$_ZZ11recordModelR5MixerE2cp, %edi
	movl	$3, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp61:
# BB#15:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ11recordModelR5MixerE2cp, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ11recordModelR5MixerE2cp, %edi
	callq	__cxa_guard_release
.LBB30_16:
	movl	bpos(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB30_18
# BB#17:                                # %._crit_edge
	movl	pos(%rip), %eax
	jmp	.LBB30_50
.LBB30_18:
	movl	c4(%rip), %ecx
	movzwl	%cx, %r8d
	movl	pos(%rip), %eax
	movzbl	%cl, %r9d
	movl	_ZZ11recordModelR5MixerE5cpos1(,%r9,4), %edx
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	$2, %ecx
	jl	.LBB30_33
# BB#19:
	movl	_ZZ11recordModelR5MixerE5cpos2(,%r9,4), %esi
	subl	%esi, %edx
	cmpl	%edx, %ecx
	jne	.LBB30_33
# BB#20:
	movl	_ZZ11recordModelR5MixerE5cpos3(,%r9,4), %edx
	subl	%edx, %esi
	cmpl	%esi, %ecx
	jne	.LBB30_33
# BB#21:
	subl	_ZZ11recordModelR5MixerE5cpos4(,%r9,4), %edx
	cmpl	%edx, %ecx
	jne	.LBB30_33
# BB#22:
	cmpl	$15, %ecx
	jle	.LBB30_23
.LBB30_25:
	cmpl	_ZZ11recordModelR5MixerE5rlen1(%rip), %ecx
	jne	.LBB30_30
# BB#26:
	movl	_ZZ11recordModelR5MixerE7rcount1(%rip), %esi
	incl	%esi
	movl	%esi, _ZZ11recordModelR5MixerE7rcount1(%rip)
	cmpl	$16, %esi
	jge	.LBB30_35
	jmp	.LBB30_36
.LBB30_23:
	imull	$-5, %ecx, %edx
	leal	-1(%rax,%rdx), %edi
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %edi
	movq	buf+16(%rip), %rsi
	movslq	%edi, %rdi
	movb	(%rsi,%rdi), %bl
	cmpb	%r8b, %bl
	jne	.LBB30_33
# BB#24:
	leal	(%rcx,%rcx), %edi
	leal	(%rdi,%rdi,2), %edi
	orl	$1, %edi
	subl	%edi, %eax
	andl	%eax, %edx
	movslq	%edx, %rax
	cmpb	%bl, (%rsi,%rax)
	je	.LBB30_25
	jmp	.LBB30_33
.LBB30_30:
	cmpl	_ZZ11recordModelR5MixerE5rlen2(%rip), %ecx
	jne	.LBB30_32
# BB#31:
	incl	_ZZ11recordModelR5MixerE7rcount2(%rip)
	jmp	.LBB30_33
.LBB30_32:
	movl	$_ZZ11recordModelR5MixerE7rcount1, %eax
	movl	_ZZ11recordModelR5MixerE7rcount1(%rip), %edx
	movl	$_ZZ11recordModelR5MixerE7rcount2, %esi
	cmpl	_ZZ11recordModelR5MixerE7rcount2(%rip), %edx
	movl	$_ZZ11recordModelR5MixerE5rlen2, %edx
	movl	$_ZZ11recordModelR5MixerE5rlen1, %edi
	cmovgq	%rdx, %rdi
	cmovleq	%rax, %rsi
	movl	%ecx, (%rdi)
	movl	$1, (%rsi)
.LBB30_33:                              # %thread-pre-split
	movl	_ZZ11recordModelR5MixerE7rcount1(%rip), %esi
	cmpl	$16, %esi
	jl	.LBB30_36
.LBB30_35:
	movl	_ZZ11recordModelR5MixerE5rlen1(%rip), %eax
	cmpl	%eax, _ZZ11recordModelR5MixerE4rlen(%rip)
	jne	.LBB30_38
.LBB30_36:
	cmpl	$16, _ZZ11recordModelR5MixerE7rcount2(%rip)
	jl	.LBB30_39
# BB#37:
	movl	_ZZ11recordModelR5MixerE5rlen2(%rip), %eax
	cmpl	%eax, _ZZ11recordModelR5MixerE4rlen(%rip)
	je	.LBB30_39
.LBB30_38:
	movl	%eax, _ZZ11recordModelR5MixerE4rlen(%rip)
	movl	$0, _ZZ11recordModelR5MixerE7rcount2(%rip)
	movl	$0, _ZZ11recordModelR5MixerE7rcount1(%rip)
.LBB30_39:
	movl	%r8d, %r10d
	shrl	$8, %r10d
	movl	%r9d, %edx
	shll	$8, %edx
	cmpl	$256, %ecx              # imm = 0x100
	movl	$255, %esi
	cmovll	%ecx, %esi
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%esi, %ecx
	sarl	$2, %ecx
	orl	%edx, %ecx
	movslq	_ZZ11recordModelR5MixerE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11recordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %esi  # imm = 0x75BCD17
	addl	%edx, %esi
	movq	_ZZ11recordModelR5MixerE2cm+96(%rip), %r11
	movl	%esi, (%r11,%rdx,4)
	movl	%r8d, %edx
	shll	$9, %edx
	movl	pos(%rip), %esi
	subl	_ZZ11recordModelR5MixerE5wpos1(,%r8,4), %esi
	cmpl	$16777216, %esi         # imm = 0x1000000
	jb	.LBB30_41
# BB#40:
	shrl	$16, %esi
	movq	ilog+16(%rip), %rdi
	movzbl	(%rdi,%rsi), %esi
	orl	$256, %esi              # imm = 0x100
	jmp	.LBB30_44
.LBB30_41:
	cmpl	$65536, %esi            # imm = 0x10000
	jb	.LBB30_43
# BB#42:
	shrl	$8, %esi
	movq	ilog+16(%rip), %rdi
	movzwl	%si, %esi
	movzbl	(%rdi,%rsi), %esi
	subl	$-128, %esi
	jmp	.LBB30_44
.LBB30_43:
	movq	ilog+16(%rip), %rdi
	movzwl	%si, %esi
	movzbl	(%rdi,%rsi), %esi
.LBB30_44:                              # %_Z4llogj.exit
	sarl	$2, %esi
	orl	%edx, %esi
	movslq	_ZZ11recordModelR5MixerE2cm+136(%rip), %rdx
	leal	1(%rdx), %edi
	movl	%edi, _ZZ11recordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%edx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%edx, %esi
	movl	%esi, (%r11,%rdx,4)
	movl	_ZZ11recordModelR5MixerE4rlen(%rip), %esi
	movl	pos(%rip), %ebx
	movl	%ebx, %edi
	subl	%esi, %edi
	movl	buf(%rip), %eax
	decl	%eax
	andl	%eax, %edi
	movq	buf+16(%rip), %rdx
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$10, %edi
	orl	%esi, %edi
	leal	(%rsi,%rsi), %ecx
	subl	%ecx, %ebx
	andl	%eax, %ebx
	movslq	%ebx, %rax
	movzbl	(%rdx,%rax), %eax
	shll	$18, %eax
	orl	%edi, %eax
	movslq	_ZZ11recordModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edi
	movl	%edi, _ZZ11recordModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%r11,%rcx,4)
	movl	%esi, %ecx
	shll	$8, %ecx
	movl	%r8d, %eax
	orl	%ecx, %eax
	movslq	_ZZ11recordModelR5MixerE2cn+136(%rip), %rbx
	leal	1(%rbx), %edi
	movl	%edi, _ZZ11recordModelR5MixerE2cn+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ebx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ebx, %eax
	movq	_ZZ11recordModelR5MixerE2cn+96(%rip), %rdi
	movl	%eax, (%rdi,%rbx,4)
	shll	$16, %esi
	orl	%r10d, %esi
	movslq	_ZZ11recordModelR5MixerE2cn+136(%rip), %rax
	leal	1(%rax), %ebx
	movl	%ebx, _ZZ11recordModelR5MixerE2cn+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%eax, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%eax, %esi
	movl	%esi, (%rdi,%rax,4)
	orl	%r9d, %ecx
	movslq	_ZZ11recordModelR5MixerE2cn+136(%rip), %rax
	leal	1(%rax), %esi
	movl	%esi, _ZZ11recordModelR5MixerE2cn+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%rdi,%rax,4)
	movl	pos(%rip), %eax
	leal	-1(%rax), %ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%ecx, %esi
	movslq	%esi, %rcx
	movzbl	(%rdx,%rcx), %ecx
	subl	_ZZ11recordModelR5MixerE5cpos1(,%rcx,4), %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	shll	$8, %ecx
	cmpl	$256, %eax              # imm = 0x100
	movl	$255, %edx
	cmovll	%eax, %edx
	orl	%ecx, %edx
	movslq	_ZZ11recordModelR5MixerE2co+136(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ11recordModelR5MixerE2co+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movq	_ZZ11recordModelR5MixerE2co+96(%rip), %r10
	movl	%ecx, (%r10,%rax,4)
	movl	pos(%rip), %ecx
	leal	-1(%rcx), %eax
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %eax
	movq	buf+16(%rip), %r11
	cltq
	movzbl	(%r11,%rax), %edi
	shll	$17, %edi
	leal	-2(%rcx), %eax
	andl	%edx, %eax
	cltq
	movzbl	(%r11,%rax), %eax
	shll	$9, %eax
	orl	%edi, %eax
	subl	_ZZ11recordModelR5MixerE5wpos1(,%r8,4), %ecx
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jb	.LBB30_46
# BB#45:
	shrl	$16, %ecx
	movq	ilog+16(%rip), %rdx
	movzbl	(%rdx,%rcx), %ecx
	orl	$256, %ecx              # imm = 0x100
	jmp	.LBB30_49
.LBB30_46:
	cmpl	$65536, %ecx            # imm = 0x10000
	jb	.LBB30_48
# BB#47:
	shrl	$8, %ecx
	movq	ilog+16(%rip), %rdx
	movzwl	%cx, %ecx
	movzbl	(%rdx,%rcx), %ecx
	subl	$-128, %ecx
	jmp	.LBB30_49
.LBB30_48:
	movq	ilog+16(%rip), %rdx
	movzwl	%cx, %ecx
	movzbl	(%rdx,%rcx), %ecx
.LBB30_49:                              # %_Z4llogj.exit56
	sarl	$2, %ecx
	orl	%ecx, %eax
	movslq	_ZZ11recordModelR5MixerE2co+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ11recordModelR5MixerE2co+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%r10,%rcx,4)
	movl	pos(%rip), %ecx
	movl	_ZZ11recordModelR5MixerE4rlen(%rip), %edi
	movl	%ecx, %eax
	cltd
	idivl	%edi
	leal	-1(%rcx), %eax
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %eax
	cltq
	movzbl	(%r11,%rax), %eax
	shll	$8, %eax
	subl	%edi, %ecx
	andl	%ebx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	orl	%eax, %ecx
	movslq	_ZZ11recordModelR5MixerE2co+136(%rip), %rax
	leal	1(%rax), %ebx
	movl	%ebx, _ZZ11recordModelR5MixerE2co+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%r10,%rax,4)
	movl	pos(%rip), %eax
	subl	%edi, %eax
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%eax, %ecx
	movslq	%ecx, %rax
	movzbl	(%r11,%rax), %eax
	shll	$10, %eax
	movl	%edx, %ecx
	shll	$18, %ecx
	orl	%edi, %ecx
	orl	%ecx, %eax
	movslq	_ZZ11recordModelR5MixerE2cp+136(%rip), %rbx
	leal	1(%rbx), %esi
	movl	%esi, _ZZ11recordModelR5MixerE2cp+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ebx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %esi  # imm = 0x75BCD17
	addl	%ebx, %esi
	movq	_ZZ11recordModelR5MixerE2cp+96(%rip), %rax
	movl	%esi, (%rax,%rbx,4)
	movl	pos(%rip), %esi
	decl	%esi
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%esi, %ebx
	movslq	%ebx, %rsi
	movzbl	(%r11,%rsi), %esi
	shll	$10, %esi
	orl	%ecx, %esi
	movslq	_ZZ11recordModelR5MixerE2cp+136(%rip), %rcx
	leal	1(%rcx), %ebx
	movl	%ebx, _ZZ11recordModelR5MixerE2cp+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%ecx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%ecx, %esi
	movl	%esi, (%rax,%rcx,4)
	shll	$12, %edi
	orl	%edx, %edi
	movslq	_ZZ11recordModelR5MixerE2cp+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ11recordModelR5MixerE2cp+136(%rip)
	imull	$987654323, %edi, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rax,%rcx,4)
	movl	_ZZ11recordModelR5MixerE5cpos3(,%r9,4), %eax
	movl	%eax, _ZZ11recordModelR5MixerE5cpos4(,%r9,4)
	movl	_ZZ11recordModelR5MixerE5cpos2(,%r9,4), %eax
	movl	%eax, _ZZ11recordModelR5MixerE5cpos3(,%r9,4)
	movl	_ZZ11recordModelR5MixerE5cpos1(,%r9,4), %eax
	movl	%eax, _ZZ11recordModelR5MixerE5cpos2(,%r9,4)
	movl	pos(%rip), %eax
	movl	%eax, _ZZ11recordModelR5MixerE5cpos1(,%r9,4)
	movl	%eax, _ZZ11recordModelR5MixerE5wpos1(,%r8,4)
	movl	bpos(%rip), %ecx
.LBB30_50:
	movl	c0(%rip), %edx
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ11recordModelR5MixerE2cm, %edi
	movq	%r14, %rsi
	callq	_ZN10ContextMap4mix1ER5Mixeriiii
	movl	c0(%rip), %edx
	movl	bpos(%rip), %ecx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ11recordModelR5MixerE2cn, %edi
	movq	%r14, %rsi
	callq	_ZN10ContextMap4mix1ER5Mixeriiii
	movl	c0(%rip), %edx
	movl	bpos(%rip), %ecx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ11recordModelR5MixerE2co, %edi
	movq	%r14, %rsi
	callq	_ZN10ContextMap4mix1ER5Mixeriiii
	movl	c0(%rip), %edx
	movl	bpos(%rip), %ecx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ11recordModelR5MixerE2cp, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN10ContextMap4mix1ER5Mixeriiii # TAILCALL
.LBB30_29:
.Ltmp62:
	movq	%rax, %rbx
	movl	$_ZGVZ11recordModelR5MixerE2cp, %edi
	jmp	.LBB30_52
.LBB30_28:
.Ltmp59:
	movq	%rax, %rbx
	movl	$_ZGVZ11recordModelR5MixerE2co, %edi
	jmp	.LBB30_52
.LBB30_27:
.Ltmp56:
	movq	%rax, %rbx
	movl	$_ZGVZ11recordModelR5MixerE2cn, %edi
	jmp	.LBB30_52
.LBB30_51:
.Ltmp53:
	movq	%rax, %rbx
	movl	$_ZGVZ11recordModelR5MixerE2cm, %edi
.LBB30_52:
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end30:
	.size	_Z11recordModelR5Mixer, .Lfunc_end30-_Z11recordModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp51-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin6   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin6   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin6   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin6   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin6   # >> Call Site 5 <<
	.long	.Lfunc_end30-.Ltmp61    #   Call between .Ltmp61 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z11sparseModelR5Mixerii
	.p2align	4, 0x90
	.type	_Z11sparseModelR5Mixerii,@function
_Z11sparseModelR5Mixerii:               # @_Z11sparseModelR5Mixerii
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -32
.Lcfi113:
	.cfi_offset %r14, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %ebp
	movq	%rdi, %r14
	movb	_ZGVZ11sparseModelR5MixeriiE2cm(%rip), %al
	testb	%al, %al
	jne	.LBB31_4
# BB#1:
	movl	$_ZGVZ11sparseModelR5MixeriiE2cm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB31_4
# BB#2:
	movb	level(%rip), %cl
	movl	$131072, %esi           # imm = 0x20000
	shll	%cl, %esi
.Ltmp63:
	movl	$_ZZ11sparseModelR5MixeriiE2cm, %edi
	movl	$48, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp64:
# BB#3:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ11sparseModelR5MixeriiE2cm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ11sparseModelR5MixeriiE2cm, %edi
	callq	__cxa_guard_release
.LBB31_4:
	movl	bpos(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB31_7
# BB#5:                                 # %._crit_edge
	movq	buf+16(%rip), %rax
	jmp	.LBB31_14
.LBB31_7:
	movl	$15790320, %eax         # imm = 0xF0F0F0
	andl	c4(%rip), %eax
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movq	_ZZ11sparseModelR5MixeriiE2cm+96(%rip), %rax
	movl	%edx, (%rax,%rcx,4)
	movl	$-252645136, %ecx       # imm = 0xF0F0F0F0
	andl	c4(%rip), %ecx
	orl	$1, %ecx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rax,%rdx,4)
	movl	$16316664, %ecx         # imm = 0xF8F8F8
	andl	c4(%rip), %ecx
	orl	$2, %ecx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rax,%rdx,4)
	movl	$-117901064, %ecx       # imm = 0xF8F8F8F8
	andl	c4(%rip), %ecx
	orl	$3, %ecx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rax,%rdx,4)
	movl	$14737632, %ecx         # imm = 0xE0E0E0
	andl	c4(%rip), %ecx
	orl	$4, %ecx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rax,%rdx,4)
	movl	$-522133280, %ecx       # imm = 0xE0E0E0E0
	andl	c4(%rip), %ecx
	orl	$5, %ecx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rax,%rdx,4)
	movl	$15790335, %ecx         # imm = 0xF0F0FF
	andl	c4(%rip), %ecx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	leal	1630958642(%rdx,%rcx), %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rax,%rdx,4)
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ebp, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movq	_ZZ11sparseModelR5MixeriiE2cm+96(%rip), %rcx
	movl	%edx, (%rcx,%rax,4)
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ebx, %edx  # imm = 0x3ADE68B3
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movl	%edx, (%rcx,%rax,4)
	movl	$16711935, %eax         # imm = 0xFF00FF
	andl	c4(%rip), %eax
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%edx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%edx, %eax
	movl	%eax, (%rcx,%rdx,4)
	movl	$-16776961, %eax        # imm = 0xFF0000FF
	andl	c4(%rip), %eax
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%edx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%edx, %eax
	movl	%eax, (%rcx,%rdx,4)
	movl	pos(%rip), %edx
	leal	-1(%rdx), %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	addl	$-5, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	shll	$8, %edx
	orl	%esi, %edx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%esi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movl	%edx, (%rcx,%rsi,4)
	movl	pos(%rip), %ecx
	leal	-1(%rcx), %edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	$-6, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movq	_ZZ11sparseModelR5MixeriiE2cm+96(%rip), %rbx
	movl	%ecx, (%rbx,%rdx,4)
	movl	pos(%rip), %ecx
	leal	-3(%rcx), %edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	$-6, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rbx,%rdx,4)
	movl	pos(%rip), %ecx
	leal	-4(%rcx), %edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	$-8, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %eax
	shll	$8, %eax
	orl	%edx, %eax
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rbx,%rcx,4)
	movl	$-4, %eax
	.p2align	4, 0x90
.LBB31_8:                               # =>This Inner Loop Header: Depth=1
	movl	pos(%rip), %edx
	leal	2(%rax,%rdx), %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movq	buf+16(%rip), %rcx
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	shll	$8, %esi
	leal	1(%rax,%rdx), %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	orl	%esi, %edx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%esi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movl	%edx, (%rbx,%rsi,4)
	movl	pos(%rip), %edx
	leal	(%rax,%rdx), %esi
	leal	2(%rax,%rdx), %edx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	shll	$8, %edx
	andl	%esi, %edi
	movslq	%edi, %rsi
	movzbl	(%rcx,%rsi), %esi
	orl	%edx, %esi
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %edi
	movl	%edi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%edx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%edx, %esi
	movl	%esi, (%rbx,%rdx,4)
	movl	pos(%rip), %edx
	leal	3(%rax,%rdx), %edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%edx, %esi
	movslq	%esi, %rdx
	movzbl	(%rcx,%rdx), %ecx
	shll	$8, %ecx
	orl	%ebp, %ecx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rbx,%rdx,4)
	decl	%eax
	cmpl	$-11, %eax
	jne	.LBB31_8
# BB#9:
	movl	c4(%rip), %ecx
	xorl	%eax, %eax
	testb	$1, %cl
	je	.LBB31_13
# BB#10:
	movzbl	%cl, %ebp
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movzwl	(%rax,%rbp,2), %ecx
	movl	$1, %eax
	testb	$4, %ch
	jne	.LBB31_13
# BB#11:
	movl	$2, %eax
	testb	$4, %cl
	jne	.LBB31_13
# BB#12:
	andl	$8192, %ecx             # imm = 0x2000
	shrl	$13, %ecx
	movl	$4, %eax
	subl	%ecx, %eax
.LBB31_13:
	movl	_ZZ11sparseModelR5MixeriiE4mask(%rip), %ecx
	shll	$3, %ecx
	orl	%eax, %ecx
	movl	%ecx, _ZZ11sparseModelR5MixeriiE4mask(%rip)
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %ecx, %edx  # imm = 0x3ADE68B3
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movl	%edx, (%rbx,%rax,4)
	movl	%ecx, %edx
	shll	$8, %edx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	orl	%edx, %esi
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rdx
	leal	1(%rdx), %edi
	movl	%edi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%edx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%edx, %esi
	movl	%esi, (%rbx,%rdx,4)
	movl	%ecx, %edx
	shll	$17, %edx
	movl	pos(%rip), %esi
	leal	-2(%rsi), %edi
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%ebp, %edi
	movslq	%edi, %rdi
	movzbl	(%rax,%rdi), %edi
	shll	$8, %edi
	orl	%edx, %edi
	addl	$-3, %esi
	andl	%ebp, %esi
	movslq	%esi, %rdx
	movzbl	(%rax,%rdx), %edx
	orl	%edi, %edx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%esi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movl	%edx, (%rbx,%rsi,4)
	andl	$511, %ecx              # imm = 0x1FF
	movl	c4(%rip), %edx
	shll	$9, %edx
	andl	$-505290752, %edx       # imm = 0xE1E1E000
	orl	%ecx, %edx
	movslq	_ZZ11sparseModelR5MixeriiE2cm+136(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, _ZZ11sparseModelR5MixeriiE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rbx,%rcx,4)
	movl	bpos(%rip), %ecx
.LBB31_14:
	movl	c0(%rip), %edx
	movl	pos(%rip), %esi
	decl	%esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%esi, %edi
	movslq	%edi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ11sparseModelR5MixeriiE2cm, %edi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_ZN10ContextMap4mix1ER5Mixeriiii # TAILCALL
.LBB31_6:
.Ltmp65:
	movq	%rax, %rbx
	movl	$_ZGVZ11sparseModelR5MixeriiE2cm, %edi
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end31:
	.size	_Z11sparseModelR5Mixerii, .Lfunc_end31-_Z11sparseModelR5Mixerii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp63-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin7   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Lfunc_end31-.Ltmp64    #   Call between .Ltmp64 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z13distanceModelR5Mixer
	.p2align	4, 0x90
	.type	_Z13distanceModelR5Mixer,@function
_Z13distanceModelR5Mixer:               # @_Z13distanceModelR5Mixer
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -24
.Lcfi119:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movb	_ZGVZ13distanceModelR5MixerE2cr(%rip), %al
	testb	%al, %al
	jne	.LBB32_4
# BB#1:
	movl	$_ZGVZ13distanceModelR5MixerE2cr, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB32_4
# BB#2:
	movb	level(%rip), %cl
	movl	$65536, %esi            # imm = 0x10000
	shll	%cl, %esi
.Ltmp66:
	movl	$_ZZ13distanceModelR5MixerE2cr, %edi
	movl	$3, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp67:
# BB#3:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ13distanceModelR5MixerE2cr, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ13distanceModelR5MixerE2cr, %edi
	callq	__cxa_guard_release
.LBB32_4:
	movl	bpos(%rip), %ecx
	testl	%ecx, %ecx
	jne	.LBB32_12
# BB#5:
	movl	c4(%rip), %eax
	movl	%eax, %ebx
	incb	%bl
	cmpb	$33, %bl
	ja	.LBB32_11
# BB#6:
	movl	$_ZZ13distanceModelR5MixerE5posnl, %edx
	movl	$_ZZ13distanceModelR5MixerE5pos20, %ecx
	movzbl	%bl, %esi
	jmpq	*.LJTI32_0(,%rsi,8)
.LBB32_8:
	movl	$_ZZ13distanceModelR5MixerE5pos00, %ecx
.LBB32_9:                               # %.sink.split.thread
	movq	%rcx, %rdx
.LBB32_10:                              # %.sink.split
	movl	pos(%rip), %ecx
	movl	%ecx, (%rdx)
.LBB32_11:
	movl	pos(%rip), %edx
	subl	_ZZ13distanceModelR5MixerE5pos00(%rip), %edx
	cmpl	$256, %edx              # imm = 0x100
	movl	$255, %ecx
	cmovgel	%ecx, %edx
	shll	$8, %eax
	movzwl	%ax, %eax
	orl	%eax, %edx
	movslq	_ZZ13distanceModelR5MixerE2cr+136(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ13distanceModelR5MixerE2cr+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%esi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edi  # imm = 0x75BCD17
	addl	%esi, %edi
	movq	_ZZ13distanceModelR5MixerE2cr+96(%rip), %rdx
	movl	%edi, (%rdx,%rsi,4)
	movl	pos(%rip), %esi
	subl	_ZZ13distanceModelR5MixerE5pos20(%rip), %esi
	cmpl	$256, %esi              # imm = 0x100
	cmovgel	%ecx, %esi
	orl	%eax, %esi
	movslq	_ZZ13distanceModelR5MixerE2cr+136(%rip), %rdi
	leal	1(%rdi), %ebx
	movl	%ebx, _ZZ13distanceModelR5MixerE2cr+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%edi, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%edi, %esi
	movl	%esi, (%rdx,%rdi,4)
	movl	pos(%rip), %esi
	subl	_ZZ13distanceModelR5MixerE5posnl(%rip), %esi
	cmpl	$256, %esi              # imm = 0x100
	cmovgel	%ecx, %esi
	addl	$234567, %eax           # imm = 0x39447
	orl	%esi, %eax
	movslq	_ZZ13distanceModelR5MixerE2cr+136(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, _ZZ13distanceModelR5MixerE2cr+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rdx,%rcx,4)
	movl	bpos(%rip), %ecx
.LBB32_12:
	movl	c0(%rip), %edx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ13distanceModelR5MixerE2cr, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN10ContextMap4mix1ER5Mixeriiii # TAILCALL
.LBB32_7:
.Ltmp68:
	movq	%rax, %rbx
	movl	$_ZGVZ13distanceModelR5MixerE2cr, %edi
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_Z13distanceModelR5Mixer, .Lfunc_end32-_Z13distanceModelR5Mixer
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI32_0:
	.quad	.LBB32_10
	.quad	.LBB32_8
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_10
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_10
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_11
	.quad	.LBB32_9
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp66-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin8   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Lfunc_end32-.Ltmp67    #   Call between .Ltmp67 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI33_0:
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.text
	.globl	_Z8bmpModelR5Mixer
	.p2align	4, 0x90
	.type	_Z8bmpModelR5Mixer,@function
_Z8bmpModelR5Mixer:                     # @_Z8bmpModelR5Mixer
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi124:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi126:
	.cfi_def_cfa_offset 80
.Lcfi127:
	.cfi_offset %rbx, -56
.Lcfi128:
	.cfi_offset %r12, -48
.Lcfi129:
	.cfi_offset %r13, -40
.Lcfi130:
	.cfi_offset %r14, -32
.Lcfi131:
	.cfi_offset %r15, -24
.Lcfi132:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movb	_ZGVZ8bmpModelR5MixerE4scm1(%rip), %al
	testb	%al, %al
	jne	.LBB33_10
# BB#1:
	movl	$_ZGVZ8bmpModelR5MixerE4scm1, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_10
# BB#2:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8bmpModelR5MixerE4scm1(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_4
# BB#3:
	movl	%eax, programChecker+4(%rip)
.LBB33_4:                               # %_ZN14ProgramChecker5allocEi.exit.i.i.i
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE4scm1+8(%rip)
	testq	%rax, %rax
	je	.LBB33_5
# BB#7:                                 # %.lr.ph.i
	movq	%rax, _ZZ8bmpModelR5MixerE4scm1+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE4scm1+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB33_8
# BB#9:                                 # %middle.block
	movq	%rax, _ZZ8bmpModelR5MixerE4scm1+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE4scm1, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE4scm1, %edi
	callq	__cxa_guard_release
.LBB33_10:
	movb	_ZGVZ8bmpModelR5MixerE4scm2(%rip), %al
	testb	%al, %al
	jne	.LBB33_20
# BB#11:
	movl	$_ZGVZ8bmpModelR5MixerE4scm2, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_20
# BB#12:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8bmpModelR5MixerE4scm2(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_14
# BB#13:
	movl	%eax, programChecker+4(%rip)
.LBB33_14:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i145
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE4scm2+8(%rip)
	testq	%rax, %rax
	je	.LBB33_15
# BB#17:                                # %.lr.ph.i146
	movq	%rax, _ZZ8bmpModelR5MixerE4scm2+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE4scm2+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_18:                              # %vector.body276
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB33_18
# BB#19:                                # %middle.block277
	movq	%rax, _ZZ8bmpModelR5MixerE4scm2+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE4scm2, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE4scm2, %edi
	callq	__cxa_guard_release
.LBB33_20:
	movb	_ZGVZ8bmpModelR5MixerE4scm3(%rip), %al
	testb	%al, %al
	jne	.LBB33_30
# BB#21:
	movl	$_ZGVZ8bmpModelR5MixerE4scm3, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_30
# BB#22:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8bmpModelR5MixerE4scm3(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_24
# BB#23:
	movl	%eax, programChecker+4(%rip)
.LBB33_24:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i151
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE4scm3+8(%rip)
	testq	%rax, %rax
	je	.LBB33_25
# BB#27:                                # %.lr.ph.i152
	movq	%rax, _ZZ8bmpModelR5MixerE4scm3+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE4scm3+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_28:                              # %vector.body289
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB33_28
# BB#29:                                # %middle.block290
	movq	%rax, _ZZ8bmpModelR5MixerE4scm3+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE4scm3, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE4scm3, %edi
	callq	__cxa_guard_release
.LBB33_30:
	movb	_ZGVZ8bmpModelR5MixerE4scm4(%rip), %al
	testb	%al, %al
	jne	.LBB33_40
# BB#31:
	movl	$_ZGVZ8bmpModelR5MixerE4scm4, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_40
# BB#32:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8bmpModelR5MixerE4scm4(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_34
# BB#33:
	movl	%eax, programChecker+4(%rip)
.LBB33_34:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i157
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE4scm4+8(%rip)
	testq	%rax, %rax
	je	.LBB33_35
# BB#37:                                # %.lr.ph.i158
	movq	%rax, _ZZ8bmpModelR5MixerE4scm4+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE4scm4+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_38:                              # %vector.body302
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB33_38
# BB#39:                                # %middle.block303
	movq	%rax, _ZZ8bmpModelR5MixerE4scm4+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE4scm4, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE4scm4, %edi
	callq	__cxa_guard_release
.LBB33_40:
	movb	_ZGVZ8bmpModelR5MixerE4scm5(%rip), %al
	testb	%al, %al
	jne	.LBB33_50
# BB#41:
	movl	$_ZGVZ8bmpModelR5MixerE4scm5, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_50
# BB#42:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8bmpModelR5MixerE4scm5(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_44
# BB#43:
	movl	%eax, programChecker+4(%rip)
.LBB33_44:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i163
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE4scm5+8(%rip)
	testq	%rax, %rax
	je	.LBB33_45
# BB#47:                                # %.lr.ph.i164
	movq	%rax, _ZZ8bmpModelR5MixerE4scm5+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE4scm5+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_48:                              # %vector.body315
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB33_48
# BB#49:                                # %middle.block316
	movq	%rax, _ZZ8bmpModelR5MixerE4scm5+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE4scm5, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE4scm5, %edi
	callq	__cxa_guard_release
.LBB33_50:
	movb	_ZGVZ8bmpModelR5MixerE4scm6(%rip), %al
	testb	%al, %al
	jne	.LBB33_60
# BB#51:
	movl	$_ZGVZ8bmpModelR5MixerE4scm6, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_60
# BB#52:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8bmpModelR5MixerE4scm6(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_54
# BB#53:
	movl	%eax, programChecker+4(%rip)
.LBB33_54:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i169
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE4scm6+8(%rip)
	testq	%rax, %rax
	je	.LBB33_55
# BB#57:                                # %.lr.ph.i170
	movq	%rax, _ZZ8bmpModelR5MixerE4scm6+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE4scm6+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_58:                              # %vector.body328
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB33_58
# BB#59:                                # %middle.block329
	movq	%rax, _ZZ8bmpModelR5MixerE4scm6+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE4scm6, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE4scm6, %edi
	callq	__cxa_guard_release
.LBB33_60:
	movb	_ZGVZ8bmpModelR5MixerE4scm7(%rip), %al
	testb	%al, %al
	jne	.LBB33_70
# BB#61:
	movl	$_ZGVZ8bmpModelR5MixerE4scm7, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_70
# BB#62:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8bmpModelR5MixerE4scm7(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_64
# BB#63:
	movl	%eax, programChecker+4(%rip)
.LBB33_64:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i175
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE4scm7+8(%rip)
	testq	%rax, %rax
	je	.LBB33_65
# BB#67:                                # %.lr.ph.i176
	movq	%rax, _ZZ8bmpModelR5MixerE4scm7+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE4scm7+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_68:                              # %vector.body341
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB33_68
# BB#69:                                # %middle.block342
	movq	%rax, _ZZ8bmpModelR5MixerE4scm7+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE4scm7, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE4scm7, %edi
	callq	__cxa_guard_release
.LBB33_70:
	movb	_ZGVZ8bmpModelR5MixerE4scm8(%rip), %al
	testb	%al, %al
	jne	.LBB33_80
# BB#71:
	movl	$_ZGVZ8bmpModelR5MixerE4scm8, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_80
# BB#72:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8bmpModelR5MixerE4scm8(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_74
# BB#73:
	movl	%eax, programChecker+4(%rip)
.LBB33_74:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i181
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE4scm8+8(%rip)
	testq	%rax, %rax
	je	.LBB33_75
# BB#77:                                # %.lr.ph.i182
	movq	%rax, _ZZ8bmpModelR5MixerE4scm8+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE4scm8+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_78:                              # %vector.body354
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB33_78
# BB#79:                                # %middle.block355
	movq	%rax, _ZZ8bmpModelR5MixerE4scm8+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE4scm8, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE4scm8, %edi
	callq	__cxa_guard_release
.LBB33_80:
	movb	_ZGVZ8bmpModelR5MixerE4scm9(%rip), %al
	testb	%al, %al
	jne	.LBB33_90
# BB#81:
	movl	$_ZGVZ8bmpModelR5MixerE4scm9, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_90
# BB#82:
	movabsq	$562949953552384, %rax  # imm = 0x2000000020000
	movq	%rax, _ZZ8bmpModelR5MixerE4scm9(%rip)
	movl	$262144, %eax           # imm = 0x40000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_84
# BB#83:
	movl	%eax, programChecker+4(%rip)
.LBB33_84:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i187
	movl	$262144, %edi           # imm = 0x40000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE4scm9+8(%rip)
	testq	%rax, %rax
	je	.LBB33_85
# BB#87:                                # %.lr.ph.i188
	movq	%rax, _ZZ8bmpModelR5MixerE4scm9+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE4scm9+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_88:                              # %vector.body367
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$131192, %rcx           # imm = 0x20078
	jne	.LBB33_88
# BB#89:                                # %middle.block368
	movq	%rax, _ZZ8bmpModelR5MixerE4scm9+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE4scm9, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE4scm9, %edi
	callq	__cxa_guard_release
.LBB33_90:
	movb	_ZGVZ8bmpModelR5MixerE5scm10(%rip), %al
	testb	%al, %al
	jne	.LBB33_100
# BB#91:
	movl	$_ZGVZ8bmpModelR5MixerE5scm10, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_100
# BB#92:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8bmpModelR5MixerE5scm10(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB33_94
# BB#93:
	movl	%eax, programChecker+4(%rip)
.LBB33_94:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i193
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8bmpModelR5MixerE5scm10+8(%rip)
	testq	%rax, %rax
	je	.LBB33_95
# BB#97:                                # %.lr.ph.i194
	movq	%rax, _ZZ8bmpModelR5MixerE5scm10+16(%rip)
	movl	$0, _ZZ8bmpModelR5MixerE5scm10+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI33_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB33_98:                              # %vector.body380
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB33_98
# BB#99:                                # %middle.block381
	movq	%rax, _ZZ8bmpModelR5MixerE5scm10+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE5scm10, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE5scm10, %edi
	callq	__cxa_guard_release
.LBB33_100:
	movb	_ZGVZ8bmpModelR5MixerE2cm(%rip), %al
	testb	%al, %al
	jne	.LBB33_104
# BB#101:
	movl	$_ZGVZ8bmpModelR5MixerE2cm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB33_104
# BB#102:
	movb	level(%rip), %cl
	movl	$262144, %esi           # imm = 0x40000
	shll	%cl, %esi
.Ltmp99:
	movl	$_ZZ8bmpModelR5MixerE2cm, %edi
	movl	$13, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp100:
# BB#103:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ8bmpModelR5MixerE2cm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8bmpModelR5MixerE2cm, %edi
	callq	__cxa_guard_release
.LBB33_104:
	cmpl	$0, bpos(%rip)
	jne	.LBB33_149
# BB#105:
	movl	pos(%rip), %eax
	leal	-54(%rax), %esi
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %esi
	movq	buf+16(%rip), %rdx
	movslq	%esi, %rsi
	cmpb	$66, (%rdx,%rsi)
	jne	.LBB33_124
# BB#106:
	leal	-53(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	cmpb	$77, (%rdx,%rsi)
	jne	.LBB33_124
# BB#107:
	leal	-44(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-43(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	leal	-42(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$16, %esi
	orl	%edi, %esi
	leal	-41(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$24, %edi
	orl	%esi, %edi
	cmpl	$54, %edi
	jne	.LBB33_124
# BB#108:
	leal	-40(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-39(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	leal	-38(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$16, %esi
	orl	%edi, %esi
	leal	-37(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$24, %edi
	orl	%esi, %edi
	cmpl	$40, %edi
	jne	.LBB33_124
# BB#109:
	leal	-24(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-23(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	leal	-22(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$16, %esi
	orl	%edi, %esi
	leal	-21(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$24, %edi
	orl	%esi, %edi
	jne	.LBB33_124
# BB#110:
	leal	-36(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-35(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	leal	-34(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$16, %esi
	orl	%edi, %esi
	leal	-33(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$24, %edi
	leal	3(%rdi,%rsi), %esi
	andl	$-4, %esi
	leal	(%rsi,%rsi,2), %esi
	movl	%esi, _ZZ8bmpModelR5MixerE1w(%rip)
	leal	-32(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	leal	-31(%rax), %ebp
	andl	%ecx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rdx,%rbp), %ebp
	leal	-30(%rax), %ebx
	andl	%ecx, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rdx,%rbx), %ebx
	leal	-29(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rcx
	movzbl	(%rdx,%rcx), %ecx
	movl	%eax, _ZZ8bmpModelR5MixerE3eoi(%rip)
	cmpl	$196607, %esi           # imm = 0x2FFFF
	jg	.LBB33_112
# BB#111:
	shll	$8, %ebp
	orl	%r8d, %ebp
	shll	$16, %ebx
	orl	%ebp, %ebx
	shll	$24, %ecx
	orl	%ebx, %ecx
	cmpl	$65536, %ecx            # imm = 0x10000
	jge	.LBB33_112
# BB#123:
	movl	%ecx, %edx
	imull	%esi, %edx
	addl	%eax, %edx
	movl	%edx, _ZZ8bmpModelR5MixerE3eoi(%rip)
	movslq	%esi, %rax
	imulq	$1431655766, %rax, %rsi # imm = 0x55555556
	movq	%rsi, %rax
	shrq	$63, %rax
	shrq	$32, %rsi
	addl	%eax, %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%ecx, %edx
	callq	printf
	cmpl	$0, bpos(%rip)
	jne	.LBB33_149
	jmp	.LBB33_124
.LBB33_112:
	movl	%eax, _ZZ8bmpModelR5MixerE3eoi(%rip)
.LBB33_124:                             # %.thread269
	movl	c4(%rip), %eax
	cmpl	$1229531648, %eax       # imm = 0x49492A00
	movl	pos(%rip), %r11d
	jne	.LBB33_125
# BB#126:
	movl	%r11d, _ZZ8bmpModelR5MixerE4tiff(%rip)
	movl	%r11d, %edx
	jmp	.LBB33_127
.LBB33_125:                             # %._crit_edge
	movl	_ZZ8bmpModelR5MixerE4tiff(%rip), %edx
.LBB33_127:
	movl	%r11d, %ecx
	subl	%edx, %ecx
	cmpl	$4, %ecx
	jne	.LBB33_129
# BB#128:
	cmpl	$134217728, %eax        # imm = 0x8000000
	je	.LBB33_129
.LBB33_148:                             # %.thread
	movl	$0, _ZZ8bmpModelR5MixerE4tiff(%rip)
.LBB33_149:                             # %.thread
	movl	pos(%rip), %edx
	cmpl	_ZZ8bmpModelR5MixerE3eoi(%rip), %edx
	jle	.LBB33_151
# BB#150:
	movl	$0, _ZZ8bmpModelR5MixerE1w(%rip)
	xorl	%eax, %eax
	jmp	.LBB33_154
.LBB33_151:
	cmpl	$0, bpos(%rip)
	jne	.LBB33_153
# BB#152:
	movslq	%edx, %rax
	imulq	$1431655766, %rax, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	leal	(%rax,%rax,2), %eax
	movl	%edx, %r14d
	subl	%eax, %r14d
	leal	-3(%rdx), %edi
	movl	buf(%rip), %ebp
	decl	%ebp
	movl	%ebp, %ecx
	andl	%edi, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	movl	_ZZ8bmpModelR5MixerE1w(%rip), %ebx
	leal	3(%rdx), %esi
	subl	%ebx, %esi
	andl	%ebp, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	leal	(%rsi,%rcx), %r11d
	subl	%ebx, %edx
	andl	%ebp, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%edx, %r11d
	subl	%ebx, %edi
	andl	%ebp, %edi
	movslq	%edi, %rdi
	movzbl	(%rax,%rdi), %edi
	addl	%edi, %r11d
	movl	%ecx, %ebp
	imull	%ebp, %ebp
	imull	%esi, %esi
	addl	%ebp, %esi
	imull	%edx, %edx
	addl	%esi, %edx
	imull	%edi, %edi
	addl	%edx, %edi
	movl	%r11d, %edx
	imull	%edx, %edx
	shrl	$2, %edx
	subl	%edx, %edi
	shrl	$2, %edi
	movq	ilog+16(%rip), %rdx
	movzwl	%di, %esi
	movzbl	(%rdx,%rsi), %r15d
	imull	$30005491, %ecx, %edx   # imm = 0x1C9D8F3
	imull	$50004239, %r14d, %r10d # imm = 0x2FB010F
	leal	19995673(%r10,%rdx), %edx
	movq	%r10, 8(%rsp)           # 8-byte Spill
	shrl	$3, %ecx
	movl	%r14d, %r9d
	shrl	$4, %r9d
	xorl	%r9d, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	xorl	$67108864, %edx         # imm = 0x4000000
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movq	_ZZ8bmpModelR5MixerE2cm+96(%rip), %rbp
	movl	%edx, (%rbp,%rcx,4)
	movl	pos(%rip), %ecx
	leal	-3(%rcx), %edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edi
	decl	%ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %eax
	imull	$30005491, %edi, %ecx   # imm = 0x1C9D8F3
	imull	$50004239, %eax, %edx   # imm = 0x2FB010F
	imull	$70004807, %r14d, %esi  # imm = 0x42C3047
	movl	%esi, 20(%rsp)          # 4-byte Spill
	addl	%esi, %ecx
	leal	290003459(%rdx,%rcx), %ecx
	shrl	$3, %edi
	shrl	$4, %eax
	movl	%r14d, %r8d
	shrl	$5, %r8d
	xorl	%r8d, %edi
	xorl	%eax, %edi
	xorl	%ecx, %edi
	shrl	$9, %ecx
	xorl	%edi, %ecx
	xorl	$67108863, %ecx         # imm = 0x3FFFFFF
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %esi
	movl	%esi, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%rbp,%rax,4)
	movl	pos(%rip), %ebx
	leal	-3(%rbx), %ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	leal	-1(%rbx), %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%rax,%rdi), %edi
	movq	%r12, (%rsp)            # 8-byte Spill
	movl	%edi, %r12d
	shrl	$2, %r12d
	addl	$-2, %ebx
	andl	%esi, %ebx
	imull	$30005491, %ecx, %esi   # imm = 0x1C9D8F3
	imull	$50004239, %r12d, %r12d # imm = 0x2FB010F
	imull	$110002499, %r14d, %r13d # imm = 0x68E8143
	addl	%r13d, %esi
	addl	%r12d, %esi
	movl	%r14d, %r12d
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	shrl	$6, %ebx
	imull	$70004807, %ebx, %ebx   # imm = 0x42C3047
	leal	600008937(%rbx,%rsi), %esi
	shrl	$3, %ecx
	shrl	$6, %edi
	shrl	$6, %r14d
	xorl	%r14d, %ecx
	xorl	%edi, %ecx
	xorl	%esi, %ecx
	shrl	$9, %esi
	xorl	%ecx, %esi
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edi
	movl	%edi, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%ecx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%ecx, %esi
	movl	%esi, (%rbp,%rcx,4)
	movl	_ZZ8bmpModelR5MixerE1w(%rip), %ecx
	movl	pos(%rip), %esi
	subl	%ecx, %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%esi, %edi
	movslq	%edi, %rsi
	movzbl	(%rax,%rsi), %esi
	imull	$30005491, %esi, %edi   # imm = 0x1C9D8F3
	leal	620004610(%r10,%rdi), %edi
	shrl	$3, %esi
	xorl	%r9d, %esi
	xorl	%edi, %esi
	shrl	$9, %edi
	xorl	%esi, %edi
	xorl	$67108865, %edi         # imm = 0x4000001
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rsi
	leal	1(%rsi), %ebx
	movl	%ebx, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edi, %edi  # imm = 0x3ADE68B3
	addl	%esi, %edi
	roll	$16, %edi
	imull	$123456791, %edi, %edi  # imm = 0x75BCD17
	addl	%esi, %edi
	movq	_ZZ8bmpModelR5MixerE2cm+96(%rip), %rbp
	movl	%edi, (%rbp,%rsi,4)
	movl	pos(%rip), %esi
	movl	%esi, %edi
	subl	%ecx, %edi
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%rax,%rdi), %edi
	decl	%esi
	andl	%ebx, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %eax
	imull	$30005491, %edi, %esi   # imm = 0x1C9D8F3
	imull	$50004239, %eax, %ebx   # imm = 0x2FB010F
	movl	20(%rsp), %r10d         # 4-byte Reload
	addl	%r10d, %esi
	leal	890012396(%rbx,%rsi), %esi
	shrl	$3, %edi
	shrl	$4, %eax
	xorl	%r8d, %edi
	xorl	%eax, %edi
	xorl	%esi, %edi
	shrl	$9, %esi
	xorl	%edi, %esi
	xorl	$67108862, %esi         # imm = 0x3FFFFFE
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %edi
	movl	%edi, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%eax, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%eax, %esi
	movl	%esi, (%rbp,%rax,4)
	movl	pos(%rip), %edi
	movl	%edi, %esi
	subl	%ecx, %esi
	movl	buf(%rip), %eax
	decl	%eax
	andl	%eax, %esi
	movq	buf+16(%rip), %rdx
	movslq	%esi, %rcx
	movzbl	(%rdx,%rcx), %ebx
	leal	-1(%rdi), %ecx
	andl	%eax, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx), %ecx
	addl	$-2, %edi
	andl	%eax, %edi
	movl	%ecx, %eax
	shrl	$2, %eax
	imull	$50004239, %eax, %eax   # imm = 0x2FB010F
	imull	$30005491, %ebx, %esi   # imm = 0x1C9D8F3
	addl	%r13d, %esi
	addl	%eax, %esi
	movslq	%edi, %rax
	movzbl	(%rdx,%rax), %eax
	shrl	$6, %eax
	imull	$70004807, %eax, %eax   # imm = 0x42C3047
	leal	1200017874(%rax,%rsi), %eax
	shrl	$3, %ebx
	shrl	$6, %ecx
	xorl	$1, %r14d
	xorl	%ebx, %ecx
	xorl	%r14d, %ecx
	xorl	%eax, %ecx
	shrl	$9, %eax
	xorl	%ecx, %eax
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rbp,%rcx,4)
	movl	pos(%rip), %edi
	leal	-3(%rdi), %eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	movl	%edi, %ecx
	subl	_ZZ8bmpModelR5MixerE1w(%rip), %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx), %ecx
	addl	%eax, %ecx
	movl	%ecx, %eax
	shrl	$3, %eax
	leal	-1(%rdi), %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rdx,%rbx), %ebx
	shrl	$5, %ebx
	addl	$-2, %edi
	andl	%esi, %edi
	movslq	%edi, %rsi
	movzbl	(%rdx,%rsi), %edx
	shrl	$5, %edx
	imull	$30005491, %eax, %eax   # imm = 0x1C9D8F3
	imull	$50004239, %ebx, %esi   # imm = 0x2FB010F
	imull	$70004807, %edx, %edx   # imm = 0x42C3047
	addl	%r13d, %eax
	addl	%esi, %eax
	leal	1400020853(%rdx,%rax), %eax
	shrl	$6, %ecx
	xorl	%r14d, %ecx
	xorl	%eax, %ecx
	shrl	$9, %eax
	xorl	%ecx, %eax
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movq	_ZZ8bmpModelR5MixerE2cm+96(%rip), %rax
	movl	%edx, (%rax,%rcx,4)
	movl	pos(%rip), %ecx
	leal	-1(%rcx), %edx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %edx
	movq	buf+16(%rip), %rsi
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %edx
	addl	$-2, %ecx
	andl	%edi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	imull	$30005491, %edx, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %ecx, %ebx   # imm = 0x2FB010F
	addl	%r10d, %edi
	leal	1490021333(%rbx,%rdi), %edi
	shrl	$3, %edx
	shrl	$4, %ecx
	xorl	$67108861, %r8d         # imm = 0x3FFFFFD
	xorl	%edx, %ecx
	xorl	%r8d, %ecx
	xorl	%edi, %ecx
	shrl	$9, %edi
	xorl	%ecx, %edi
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edi, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rax,%rcx,4)
	movl	pos(%rip), %edx
	leal	-3(%rdx), %ecx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	leal	-1(%rdx), %ebx
	andl	%edi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rsi,%rbx), %ebx
	addl	$-4, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %edx
	subl	%edx, %ebx
	imull	$30005491, %ecx, %edx   # imm = 0x1C9D8F3
	imull	$50004239, %ebx, %edi   # imm = 0x2FB010F
	addl	%r10d, %edx
	leal	1690024312(%rdi,%rdx), %edx
	shrl	$3, %ecx
	shrl	$4, %ebx
	xorl	%r8d, %ecx
	xorl	%ebx, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edi
	movl	%edi, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rax,%rcx,4)
	movl	pos(%rip), %eax
	leal	-3(%rax), %ecx
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	leal	-1(%rax), %edi
	andl	%edx, %edi
	movslq	%edi, %rdi
	movzbl	(%rsi,%rdi), %edi
	addl	%ecx, %edi
	addl	$-4, %eax
	andl	%edx, %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	subl	%eax, %edi
	imull	$30005491, %edi, %eax   # imm = 0x1C9D8F3
	movq	8(%rsp), %r13           # 8-byte Reload
	leal	1820022484(%r13,%rax), %eax
	shrl	$3, %edi
	xorl	%r9d, %edi
	xorl	%eax, %edi
	shrl	$9, %eax
	xorl	%edi, %eax
	xorl	$67108866, %eax         # imm = 0x4000002
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movq	_ZZ8bmpModelR5MixerE2cm+96(%rip), %r14
	movl	%eax, (%r14,%rcx,4)
	movl	_ZZ8bmpModelR5MixerE1w(%rip), %eax
	movl	pos(%rip), %edx
	movl	%edx, %edi
	subl	%eax, %edi
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %edi
	movq	buf+16(%rip), %rcx
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %ebp
	leal	-1(%rdx), %edi
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %esi
	movl	%eax, %edi
	notl	%edi
	addl	%edi, %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	subl	%edx, %esi
	imull	$30005491, %ebp, %edx   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %ebx   # imm = 0x2FB010F
	addl	%r10d, %edx
	leal	2090030270(%rbx,%rdx), %edx
	shrl	$3, %ebp
	xorl	%r8d, %ebp
	shrl	$4, %esi
	xorl	%esi, %ebp
	xorl	%edx, %ebp
	shrl	$9, %edx
	xorl	%ebp, %edx
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rsi
	leal	1(%rsi), %ebp
	movl	%ebp, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%esi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movl	%edx, (%r14,%rsi,4)
	movl	pos(%rip), %edx
	leal	-1(%rdx), %esi
	addl	%edx, %edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%eax, %edx
	movl	buf(%rip), %eax
	decl	%eax
	andl	%eax, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	andl	%eax, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	addl	%edx, %esi
	andl	%eax, %edi
	movl	%r11d, %eax
	shrl	$2, %eax
	xorl	%r11d, %r12d
	movslq	%edi, %rdx
	movzbl	(%rcx,%rdx), %ecx
	subl	%ecx, %esi
	imull	$30005491, %esi, %ecx   # imm = 0x1C9D8F3
	leal	-2074938854(%r13,%rcx), %ecx
	shrl	$3, %esi
	xorl	%r9d, %esi
	xorl	%ecx, %esi
	shrl	$9, %ecx
	xorl	%esi, %ecx
	xorl	$67108867, %ecx         # imm = 0x4000003
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%r14,%rdx,4)
	movl	%r15d, %ecx
	shrl	$5, %ecx
	imull	$30005491, %eax, %eax   # imm = 0x1C9D8F3
	imull	$50004239, %ecx, %ecx   # imm = 0x2FB010F
	addl	%r10d, %eax
	leal	-1804931068(%rcx,%rax), %eax
	shrl	$5, %r12d
	xorl	%eax, %r12d
	shrl	$9, %eax
	xorl	%r12d, %eax
	movq	(%rsp), %r12            # 8-byte Reload
	xorl	$67108860, %eax         # imm = 0x3FFFFFC
	movslq	_ZZ8bmpModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ8bmpModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movq	_ZZ8bmpModelR5MixerE2cm+96(%rip), %rdx
	movl	%eax, (%rdx,%rcx,4)
	movl	pos(%rip), %ebp
	leal	-3(%rbp), %ecx
	movl	buf(%rip), %esi
	decl	%esi
	movl	%esi, %edx
	andl	%ecx, %edx
	movq	buf+16(%rip), %rax
	movslq	%edx, %r8
	movzbl	(%rax,%r8), %edx
	movl	_ZZ8bmpModelR5MixerE1w(%rip), %r14d
	movl	%ebp, %ebx
	subl	%r14d, %ebx
	andl	%esi, %ebx
	movslq	%ebx, %r10
	movzbl	(%rax,%r10), %ebx
	addl	%edx, %ebx
	subl	%r14d, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	subl	%ecx, %ebx
	shll	$8, %ebx
	movl	$-256, %r9d
	movl	_ZZ8bmpModelR5MixerE4scm1(%rip), %ecx
	addl	%r9d, %ecx
	andl	%ebx, %ecx
	leal	3(%rbp), %edx
	movl	%ecx, _ZZ8bmpModelR5MixerE4scm1+24(%rip)
	movzbl	(%rax,%r8), %ecx
	subl	%r14d, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ecx, %edx
	movzbl	(%rax,%r10), %ecx
	subl	%ecx, %edx
	shll	$8, %edx
	movl	_ZZ8bmpModelR5MixerE4scm2(%rip), %ebx
	addl	%r9d, %ebx
	andl	%edx, %ebx
	leal	-6(%rbp), %ecx
	movl	%ebx, _ZZ8bmpModelR5MixerE4scm2+24(%rip)
	movl	$-3, %edx
	subl	%r14d, %edx
	addl	%ebp, %edx
	movl	%ebp, %ebx
	movzbl	(%rax,%r8), %ebp
	addl	%ebp, %ebp
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	subl	%ecx, %ebp
	shll	$8, %ebp
	movl	_ZZ8bmpModelR5MixerE4scm3(%rip), %ecx
	addl	%r9d, %ecx
	andl	%ebp, %ecx
	movl	%ecx, _ZZ8bmpModelR5MixerE4scm3+24(%rip)
	movzbl	(%rax,%r10), %ecx
	addl	%ecx, %ecx
	leal	(%r14,%r14), %ebp
	subl	%ebp, %ebx
	movl	%esi, %edi
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%rax,%rdi), %edi
	subl	%edi, %ecx
	shll	$8, %ecx
	movl	_ZZ8bmpModelR5MixerE4scm4(%rip), %edi
	addl	%r9d, %edi
	andl	%ecx, %edi
	movl	%edi, _ZZ8bmpModelR5MixerE4scm4+24(%rip)
	andl	%esi, %edx
	movslq	%edx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%ecx, %ecx
	addl	$-6, %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rdx
	movzbl	(%rax,%rdx), %eax
	subl	%eax, %ecx
	shll	$8, %ecx
	movl	_ZZ8bmpModelR5MixerE4scm5(%rip), %eax
	addl	%r9d, %eax
	andl	%ecx, %eax
	movl	%eax, _ZZ8bmpModelR5MixerE4scm5+24(%rip)
	movl	pos(%rip), %edi
	movl	$3, %r8d
	movl	$3, %ebx
	subl	%r14d, %ebx
	addl	%edi, %ebx
	movl	buf(%rip), %eax
	decl	%eax
	andl	%eax, %ebx
	movl	_ZZ8bmpModelR5MixerE1w(%rip), %esi
	subl	%esi, %r8d
	addl	%edi, %r8d
	movl	$2, %edx
	subl	%esi, %edx
	addl	%edi, %edx
	movl	%edi, %ecx
	subl	%ebp, %ecx
	movq	buf+16(%rip), %rbp
	movslq	%ebx, %rbx
	movzbl	(%rbp,%rbx), %ebx
	addl	%ebx, %ebx
	addl	$6, %ecx
	andl	%eax, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rbp,%rcx), %ecx
	subl	%ecx, %ebx
	shll	$8, %ebx
	movl	_ZZ8bmpModelR5MixerE4scm6(%rip), %ecx
	addl	%r9d, %ecx
	andl	%ebx, %ecx
	movl	%edi, %ebx
	subl	%esi, %ebx
	addl	%esi, %esi
	negl	%esi
	leal	3(%rdi,%rsi), %esi
	leal	-1(%rdi), %edi
	movl	%ecx, _ZZ8bmpModelR5MixerE4scm6+24(%rip)
	andl	%eax, %r8d
	movslq	%r8d, %rcx
	movzbl	(%rbp,%rcx), %r8d
	andl	%eax, %edi
	movslq	%edi, %rdi
	movzbl	(%rbp,%rdi), %edi
	addl	%r8d, %edi
	andl	%eax, %edx
	movslq	%edx, %rdx
	movzbl	(%rbp,%rdx), %edx
	subl	%edx, %edi
	shll	$8, %edi
	movl	_ZZ8bmpModelR5MixerE4scm7(%rip), %edx
	addl	%r9d, %edx
	andl	%edi, %edx
	movl	%edx, _ZZ8bmpModelR5MixerE4scm7+24(%rip)
	andl	%eax, %ebx
	movslq	%ebx, %rdx
	movzbl	(%rbp,%rdx), %edx
	movzbl	(%rbp,%rcx), %ecx
	addl	%edx, %ecx
	andl	%eax, %esi
	movslq	%esi, %rax
	movzbl	(%rbp,%rax), %eax
	subl	%eax, %ecx
	shll	$8, %ecx
	movl	_ZZ8bmpModelR5MixerE4scm8(%rip), %eax
	addl	%r9d, %eax
	andl	%ecx, %eax
	movl	%eax, _ZZ8bmpModelR5MixerE4scm8+24(%rip)
	shrl	$3, %r11d
	addl	%r15d, %r15d
	andl	$384, %r15d             # imm = 0x180
	orl	%r11d, %r15d
	shll	$8, %r15d
	addl	_ZZ8bmpModelR5MixerE4scm9(%rip), %r9d
	andl	%r15d, %r9d
	movl	%r9d, _ZZ8bmpModelR5MixerE4scm9+24(%rip)
.LBB33_153:
	movl	y(%rip), %edx
	shll	$16, %edx
	movq	_ZZ8bmpModelR5MixerE4scm1+32(%rip), %rax
	movzwl	(%rax), %ecx
	orl	$64, %edx
	movl	%edx, %esi
	subl	%ecx, %esi
	shrl	$7, %esi
	addl	%ecx, %esi
	movw	%si, (%rax)
	movl	c0(%rip), %r9d
	movl	_ZZ8bmpModelR5MixerE4scm1+24(%rip), %eax
	addl	%r9d, %eax
	movq	_ZZ8bmpModelR5MixerE4scm1+16(%rip), %rsi
	cltq
	leaq	(%rsi,%rax,2), %rdi
	movq	%rdi, _ZZ8bmpModelR5MixerE4scm1+32(%rip)
	movzwl	(%rsi,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movq	stretch+16(%rip), %rsi
	movzwl	(%rsi,%rax), %ecx
	movslq	96(%r12), %r8
	movq	32(%r12), %rdi
	movw	%cx, (%rdi,%r8,2)
	movq	_ZZ8bmpModelR5MixerE4scm2+32(%rip), %rax
	movzwl	(%rax), %ebx
	movl	%edx, %ecx
	subl	%ebx, %ecx
	shrl	$7, %ecx
	addl	%ebx, %ecx
	movw	%cx, (%rax)
	movl	_ZZ8bmpModelR5MixerE4scm2+24(%rip), %eax
	addl	%r9d, %eax
	movq	_ZZ8bmpModelR5MixerE4scm2+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbx
	movq	%rbx, _ZZ8bmpModelR5MixerE4scm2+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	movw	%ax, 2(%rdi,%r8,2)
	movq	_ZZ8bmpModelR5MixerE4scm3+32(%rip), %rax
	movzwl	(%rax), %ecx
	movl	%edx, %ebp
	subl	%ecx, %ebp
	shrl	$7, %ebp
	addl	%ecx, %ebp
	movw	%bp, (%rax)
	movl	_ZZ8bmpModelR5MixerE4scm3+24(%rip), %eax
	addl	%r9d, %eax
	movq	_ZZ8bmpModelR5MixerE4scm3+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbp
	movq	%rbp, _ZZ8bmpModelR5MixerE4scm3+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	movw	%ax, 4(%rdi,%r8,2)
	movq	_ZZ8bmpModelR5MixerE4scm4+32(%rip), %rax
	movzwl	(%rax), %ecx
	subl	%ecx, %edx
	shrl	$7, %edx
	addl	%ecx, %edx
	movw	%dx, (%rax)
	addl	_ZZ8bmpModelR5MixerE4scm4+24(%rip), %r9d
	movq	_ZZ8bmpModelR5MixerE4scm4+16(%rip), %rax
	movslq	%r9d, %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, _ZZ8bmpModelR5MixerE4scm4+32(%rip)
	movzwl	(%rax,%rcx,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	movw	%ax, 6(%rdi,%r8,2)
	movl	y(%rip), %edx
	shll	$16, %edx
	movq	_ZZ8bmpModelR5MixerE4scm5+32(%rip), %rax
	movzwl	(%rax), %ecx
	orl	$64, %edx
	movl	%edx, %esi
	subl	%ecx, %esi
	shrl	$7, %esi
	addl	%ecx, %esi
	movw	%si, (%rax)
	movl	c0(%rip), %r9d
	movl	_ZZ8bmpModelR5MixerE4scm5+24(%rip), %eax
	addl	%r9d, %eax
	movq	_ZZ8bmpModelR5MixerE4scm5+16(%rip), %rsi
	cltq
	leaq	(%rsi,%rax,2), %rdi
	movq	%rdi, _ZZ8bmpModelR5MixerE4scm5+32(%rip)
	movzwl	(%rsi,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movq	stretch+16(%rip), %rsi
	movzwl	(%rsi,%rax), %eax
	movq	32(%r12), %rdi
	movw	%ax, 8(%rdi,%r8,2)
	movq	_ZZ8bmpModelR5MixerE4scm6+32(%rip), %rax
	movzwl	(%rax), %ebp
	movl	%edx, %ecx
	subl	%ebp, %ecx
	shrl	$7, %ecx
	addl	%ebp, %ecx
	movw	%cx, (%rax)
	movl	_ZZ8bmpModelR5MixerE4scm6+24(%rip), %eax
	addl	%r9d, %eax
	movq	_ZZ8bmpModelR5MixerE4scm6+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbp
	movq	%rbp, _ZZ8bmpModelR5MixerE4scm6+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	movw	%ax, 10(%rdi,%r8,2)
	movq	_ZZ8bmpModelR5MixerE4scm7+32(%rip), %rax
	movzwl	(%rax), %ecx
	movl	%edx, %ebx
	subl	%ecx, %ebx
	shrl	$7, %ebx
	addl	%ecx, %ebx
	movw	%bx, (%rax)
	movl	_ZZ8bmpModelR5MixerE4scm7+24(%rip), %eax
	addl	%r9d, %eax
	movq	_ZZ8bmpModelR5MixerE4scm7+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbp
	movq	%rbp, _ZZ8bmpModelR5MixerE4scm7+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	movw	%ax, 12(%rdi,%r8,2)
	movq	_ZZ8bmpModelR5MixerE4scm8+32(%rip), %rax
	movzwl	(%rax), %ecx
	subl	%ecx, %edx
	shrl	$7, %edx
	addl	%ecx, %edx
	movw	%dx, (%rax)
	addl	_ZZ8bmpModelR5MixerE4scm8+24(%rip), %r9d
	movq	_ZZ8bmpModelR5MixerE4scm8+16(%rip), %rax
	movslq	%r9d, %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, _ZZ8bmpModelR5MixerE4scm8+32(%rip)
	movzwl	(%rax,%rcx,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	movw	%ax, 14(%rdi,%r8,2)
	movl	y(%rip), %r9d
	movl	%r9d, %ecx
	shll	$16, %ecx
	movq	_ZZ8bmpModelR5MixerE4scm9+32(%rip), %rax
	movzwl	(%rax), %edx
	orl	$64, %ecx
	movl	%ecx, %esi
	subl	%edx, %esi
	shrl	$7, %esi
	addl	%edx, %esi
	movw	%si, (%rax)
	movl	c0(%rip), %edx
	movl	_ZZ8bmpModelR5MixerE4scm9+24(%rip), %eax
	addl	%edx, %eax
	movq	_ZZ8bmpModelR5MixerE4scm9+16(%rip), %rsi
	cltq
	leaq	(%rsi,%rax,2), %rdi
	movq	%rdi, _ZZ8bmpModelR5MixerE4scm9+32(%rip)
	movzwl	(%rsi,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movq	stretch+16(%rip), %rsi
	movzwl	(%rsi,%rax), %eax
	movq	32(%r12), %rdi
	movw	%ax, 16(%rdi,%r8,2)
	movq	_ZZ8bmpModelR5MixerE5scm10+32(%rip), %rax
	movzwl	(%rax), %ebp
	subl	%ebp, %ecx
	shrl	$7, %ecx
	addl	%ebp, %ecx
	movw	%cx, (%rax)
	movl	_ZZ8bmpModelR5MixerE5scm10+24(%rip), %eax
	addl	%edx, %eax
	movq	_ZZ8bmpModelR5MixerE5scm10+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbp
	movq	%rbp, _ZZ8bmpModelR5MixerE5scm10+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	leal	10(%r8), %ecx
	movl	%ecx, 96(%r12)
	movw	%ax, 18(%rdi,%r8,2)
	movl	bpos(%rip), %ecx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	$_ZZ8bmpModelR5MixerE2cm, %edi
	movq	%r12, %rsi
	callq	_ZN10ContextMap4mix1ER5Mixeriiii
	movl	_ZZ8bmpModelR5MixerE1w(%rip), %eax
.LBB33_154:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB33_129:
	testl	%edx, %edx
	je	.LBB33_149
# BB#130:
	cmpl	$200, %ecx
	jne	.LBB33_149
# BB#131:
	leal	-196(%r11), %eax
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %eax
	movq	buf+16(%rip), %r13
	cltq
	movzbl	(%r13,%rax), %r9d
	leal	-195(%r11), %eax
	andl	%ebx, %eax
	cltq
	movzbl	(%r13,%rax), %ebp
	movl	$0, _ZZ8bmpModelR5MixerE1w(%rip)
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leal	6(%rdx), %eax
	leal	-12(%r11), %r10d
	xorl	%ecx, %ecx
	cmpl	%r10d, %eax
	movl	$0, %esi
	movl	$0, %edx
	jge	.LBB33_141
# BB#132:                               # %.lr.ph
	movq	%r12, (%rsp)            # 8-byte Spill
	shll	$8, %ebp
	leal	1(%rbp,%r9), %r12d
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB33_133:                             # =>This Inner Loop Header: Depth=1
	decl	%r12d
	cmpl	$1, %r12d
	jle	.LBB33_134
# BB#143:                               #   in Loop: Header=BB33_133 Depth=1
	leal	2(%rax), %ebp
	andl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%r13,%rbp), %ebp
	leal	3(%rax), %edi
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%r13,%rdi), %edi
	shll	$8, %edi
	leal	11(%rax), %r15d
	leal	-3(%rdi,%rbp), %edi
	cmpl	$1, %edi
	ja	.LBB33_146
# BB#144:                               #   in Loop: Header=BB33_133 Depth=1
	leal	4(%rax), %edi
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%r13,%rdi), %edi
	leal	5(%rax), %ebp
	andl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%r13,%rbp), %ebp
	shll	$8, %ebp
	orl	%edi, %ebp
	leal	6(%rax), %edi
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%r13,%rdi), %edi
	shll	$16, %edi
	orl	%ebp, %edi
	leal	7(%rax), %ebp
	andl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%r13,%rbp), %ebp
	shll	$24, %ebp
	orl	%edi, %ebp
	cmpl	$1, %ebp
	jne	.LBB33_146
# BB#145:                               #   in Loop: Header=BB33_133 Depth=1
	movl	%ebx, %edi
	andl	%eax, %edi
	movslq	%edi, %rdi
	movzbl	(%r13,%rdi), %edi
	leal	1(%rax), %ebp
	andl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%r13,%rbp), %ebp
	shll	$8, %ebp
	orl	%edi, %ebp
	leal	8(%rax), %edi
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%r13,%rdi), %r9d
	leal	9(%rax), %r14d
	andl	%ebx, %r14d
	movslq	%r14d, %rdi
	movzbl	(%r13,%rdi), %edi
	shll	$8, %edi
	orl	%r9d, %edi
	addl	$10, %eax
	andl	%ebx, %eax
	cltq
	movzbl	(%r13,%rax), %eax
	shll	$16, %eax
	orl	%edi, %eax
	movl	%ebx, %edi
	andl	%r15d, %edi
	movslq	%edi, %rdi
	movzbl	(%r13,%rdi), %edi
	shll	$24, %edi
	orl	%eax, %edi
	cmpl	$256, %ebp              # imm = 0x100
	cmovel	%edi, %esi
	cmpl	$257, %ebp              # imm = 0x101
	cmovel	%edi, %edx
	cmpl	$259, %ebp              # imm = 0x103
	cmovel	%edi, %r8d
	cmpl	$277, %ebp              # imm = 0x115
	cmovel	%edi, %ecx
.LBB33_146:                             #   in Loop: Header=BB33_133 Depth=1
	incl	%r15d
	cmpl	%r10d, %r15d
	movl	%r15d, %eax
	jl	.LBB33_133
.LBB33_134:                             # %.critedge
	testl	%edx, %edx
	jle	.LBB33_135
# BB#136:                               # %.critedge
	testl	%esi, %esi
	movq	(%rsp), %r12            # 8-byte Reload
	jle	.LBB33_141
# BB#137:
	movl	%esi, %eax
	imull	%edx, %eax
	cmpl	$51, %eax
	jl	.LBB33_141
# BB#138:
	cmpl	$1, %r8d
	jne	.LBB33_141
# BB#139:
	movl	%ecx, %edi
	orl	$2, %edi
	cmpl	$3, %edi
	jne	.LBB33_141
# BB#140:
	imull	%ecx, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	addl	%eax, %edi
	movl	%edi, _ZZ8bmpModelR5MixerE3eoi(%rip)
	movl	%ecx, %eax
	imull	%esi, %eax
	movl	%eax, _ZZ8bmpModelR5MixerE1w(%rip)
	cmpl	%r11d, _ZZ8bmpModelR5MixerE3eoi(%rip)
	jg	.LBB33_142
	jmp	.LBB33_147
.LBB33_135:
	movq	(%rsp), %r12            # 8-byte Reload
.LBB33_141:                             # %.critedge.thread
	cmpl	%r11d, _ZZ8bmpModelR5MixerE3eoi(%rip)
	jle	.LBB33_147
.LBB33_142:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB33_149
.LBB33_147:
	movl	$0, _ZZ8bmpModelR5MixerE1w(%rip)
	jmp	.LBB33_148
.LBB33_5:
.Ltmp69:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp70:
# BB#6:                                 # %.noexc
.LBB33_15:
.Ltmp72:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp73:
# BB#16:                                # %.noexc149
.LBB33_25:
.Ltmp75:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp76:
# BB#26:                                # %.noexc155
.LBB33_35:
.Ltmp78:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp79:
# BB#36:                                # %.noexc161
.LBB33_45:
.Ltmp81:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp82:
# BB#46:                                # %.noexc167
.LBB33_55:
.Ltmp84:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp85:
# BB#56:                                # %.noexc173
.LBB33_65:
.Ltmp87:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp88:
# BB#66:                                # %.noexc179
.LBB33_75:
.Ltmp90:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp91:
# BB#76:                                # %.noexc185
.LBB33_85:
.Ltmp93:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp94:
# BB#86:                                # %.noexc191
.LBB33_95:
.Ltmp96:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp97:
# BB#96:                                # %.noexc197
.LBB33_121:
.Ltmp98:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE5scm10, %edi
	jmp	.LBB33_156
.LBB33_120:
.Ltmp95:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE4scm9, %edi
	jmp	.LBB33_156
.LBB33_119:
.Ltmp92:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE4scm8, %edi
	jmp	.LBB33_156
.LBB33_118:
.Ltmp89:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE4scm7, %edi
	jmp	.LBB33_156
.LBB33_117:
.Ltmp86:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE4scm6, %edi
	jmp	.LBB33_156
.LBB33_116:
.Ltmp83:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE4scm5, %edi
	jmp	.LBB33_156
.LBB33_115:
.Ltmp80:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE4scm4, %edi
	jmp	.LBB33_156
.LBB33_114:
.Ltmp77:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE4scm3, %edi
	jmp	.LBB33_156
.LBB33_113:
.Ltmp74:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE4scm2, %edi
	jmp	.LBB33_156
.LBB33_155:
.Ltmp71:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE4scm1, %edi
	jmp	.LBB33_156
.LBB33_122:
.Ltmp101:
	movq	%rax, %rbx
	movl	$_ZGVZ8bmpModelR5MixerE2cm, %edi
.LBB33_156:
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end33:
	.size	_Z8bmpModelR5Mixer, .Lfunc_end33-_Z8bmpModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\254\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Ltmp99-.Lfunc_begin9   # >> Call Site 1 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin9  #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp69-.Ltmp100        #   Call between .Ltmp100 and .Ltmp69
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin9   # >> Call Site 3 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin9   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin9   # >> Call Site 4 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin9   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin9   # >> Call Site 5 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin9   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin9   # >> Call Site 6 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin9   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin9   # >> Call Site 7 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin9   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin9   # >> Call Site 8 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin9   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin9   # >> Call Site 9 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin9   #     jumps to .Ltmp89
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin9   # >> Call Site 10 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin9   #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp93-.Lfunc_begin9   # >> Call Site 11 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin9   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin9   # >> Call Site 12 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin9   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin9   # >> Call Site 13 <<
	.long	.Lfunc_end33-.Ltmp97    #   Call between .Ltmp97 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI34_0:
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.text
	.globl	_Z9model8bitR5Mixeri
	.p2align	4, 0x90
	.type	_Z9model8bitR5Mixeri,@function
_Z9model8bitR5Mixeri:                   # @_Z9model8bitR5Mixeri
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi136:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi137:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi139:
	.cfi_def_cfa_offset 80
.Lcfi140:
	.cfi_offset %rbx, -56
.Lcfi141:
	.cfi_offset %r12, -48
.Lcfi142:
	.cfi_offset %r13, -40
.Lcfi143:
	.cfi_offset %r14, -32
.Lcfi144:
	.cfi_offset %r15, -24
.Lcfi145:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r12
	movb	_ZGVZ9model8bitR5MixeriE4scm1(%rip), %al
	testb	%al, %al
	jne	.LBB34_10
# BB#1:
	movl	$_ZGVZ9model8bitR5MixeriE4scm1, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB34_10
# BB#2:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ9model8bitR5MixeriE4scm1(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB34_4
# BB#3:
	movl	%eax, programChecker+4(%rip)
.LBB34_4:                               # %_ZN14ProgramChecker5allocEi.exit.i.i.i
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9model8bitR5MixeriE4scm1+8(%rip)
	testq	%rax, %rax
	je	.LBB34_5
# BB#7:                                 # %.lr.ph.i
	movq	%rax, _ZZ9model8bitR5MixeriE4scm1+16(%rip)
	movl	$0, _ZZ9model8bitR5MixeriE4scm1+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI34_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB34_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB34_8
# BB#9:                                 # %middle.block
	movq	%rax, _ZZ9model8bitR5MixeriE4scm1+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ9model8bitR5MixeriE4scm1, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9model8bitR5MixeriE4scm1, %edi
	callq	__cxa_guard_release
.LBB34_10:
	movb	_ZGVZ9model8bitR5MixeriE4scm2(%rip), %al
	testb	%al, %al
	jne	.LBB34_20
# BB#11:
	movl	$_ZGVZ9model8bitR5MixeriE4scm2, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB34_20
# BB#12:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ9model8bitR5MixeriE4scm2(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB34_14
# BB#13:
	movl	%eax, programChecker+4(%rip)
.LBB34_14:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i134
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9model8bitR5MixeriE4scm2+8(%rip)
	testq	%rax, %rax
	je	.LBB34_15
# BB#17:                                # %.lr.ph.i135
	movq	%rax, _ZZ9model8bitR5MixeriE4scm2+16(%rip)
	movl	$0, _ZZ9model8bitR5MixeriE4scm2+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI34_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB34_18:                              # %vector.body185
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB34_18
# BB#19:                                # %middle.block186
	movq	%rax, _ZZ9model8bitR5MixeriE4scm2+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ9model8bitR5MixeriE4scm2, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9model8bitR5MixeriE4scm2, %edi
	callq	__cxa_guard_release
.LBB34_20:
	movb	_ZGVZ9model8bitR5MixeriE4scm3(%rip), %al
	testb	%al, %al
	jne	.LBB34_30
# BB#21:
	movl	$_ZGVZ9model8bitR5MixeriE4scm3, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB34_30
# BB#22:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ9model8bitR5MixeriE4scm3(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB34_24
# BB#23:
	movl	%eax, programChecker+4(%rip)
.LBB34_24:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i140
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9model8bitR5MixeriE4scm3+8(%rip)
	testq	%rax, %rax
	je	.LBB34_25
# BB#27:                                # %.lr.ph.i141
	movq	%rax, _ZZ9model8bitR5MixeriE4scm3+16(%rip)
	movl	$0, _ZZ9model8bitR5MixeriE4scm3+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI34_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB34_28:                              # %vector.body198
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB34_28
# BB#29:                                # %middle.block199
	movq	%rax, _ZZ9model8bitR5MixeriE4scm3+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ9model8bitR5MixeriE4scm3, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9model8bitR5MixeriE4scm3, %edi
	callq	__cxa_guard_release
.LBB34_30:
	movb	_ZGVZ9model8bitR5MixeriE4scm4(%rip), %al
	testb	%al, %al
	jne	.LBB34_40
# BB#31:
	movl	$_ZGVZ9model8bitR5MixeriE4scm4, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB34_40
# BB#32:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ9model8bitR5MixeriE4scm4(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB34_34
# BB#33:
	movl	%eax, programChecker+4(%rip)
.LBB34_34:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i146
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9model8bitR5MixeriE4scm4+8(%rip)
	testq	%rax, %rax
	je	.LBB34_35
# BB#37:                                # %.lr.ph.i147
	movq	%rax, _ZZ9model8bitR5MixeriE4scm4+16(%rip)
	movl	$0, _ZZ9model8bitR5MixeriE4scm4+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI34_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB34_38:                              # %vector.body211
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB34_38
# BB#39:                                # %middle.block212
	movq	%rax, _ZZ9model8bitR5MixeriE4scm4+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ9model8bitR5MixeriE4scm4, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9model8bitR5MixeriE4scm4, %edi
	callq	__cxa_guard_release
.LBB34_40:
	movb	_ZGVZ9model8bitR5MixeriE4scm5(%rip), %al
	testb	%al, %al
	jne	.LBB34_50
# BB#41:
	movl	$_ZGVZ9model8bitR5MixeriE4scm5, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB34_50
# BB#42:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ9model8bitR5MixeriE4scm5(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB34_44
# BB#43:
	movl	%eax, programChecker+4(%rip)
.LBB34_44:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i152
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9model8bitR5MixeriE4scm5+8(%rip)
	testq	%rax, %rax
	je	.LBB34_45
# BB#47:                                # %.lr.ph.i153
	movq	%rax, _ZZ9model8bitR5MixeriE4scm5+16(%rip)
	movl	$0, _ZZ9model8bitR5MixeriE4scm5+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI34_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB34_48:                              # %vector.body224
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB34_48
# BB#49:                                # %middle.block225
	movq	%rax, _ZZ9model8bitR5MixeriE4scm5+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ9model8bitR5MixeriE4scm5, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9model8bitR5MixeriE4scm5, %edi
	callq	__cxa_guard_release
.LBB34_50:
	movb	_ZGVZ9model8bitR5MixeriE4scm6(%rip), %al
	testb	%al, %al
	jne	.LBB34_60
# BB#51:
	movl	$_ZGVZ9model8bitR5MixeriE4scm6, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB34_60
# BB#52:
	movabsq	$562949953552384, %rax  # imm = 0x2000000020000
	movq	%rax, _ZZ9model8bitR5MixeriE4scm6(%rip)
	movl	$262144, %eax           # imm = 0x40000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB34_54
# BB#53:
	movl	%eax, programChecker+4(%rip)
.LBB34_54:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i158
	movl	$262144, %edi           # imm = 0x40000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9model8bitR5MixeriE4scm6+8(%rip)
	testq	%rax, %rax
	je	.LBB34_55
# BB#57:                                # %.lr.ph.i159
	movq	%rax, _ZZ9model8bitR5MixeriE4scm6+16(%rip)
	movl	$0, _ZZ9model8bitR5MixeriE4scm6+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI34_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB34_58:                              # %vector.body237
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$131192, %rcx           # imm = 0x20078
	jne	.LBB34_58
# BB#59:                                # %middle.block238
	movq	%rax, _ZZ9model8bitR5MixeriE4scm6+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ9model8bitR5MixeriE4scm6, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9model8bitR5MixeriE4scm6, %edi
	callq	__cxa_guard_release
.LBB34_60:
	movb	_ZGVZ9model8bitR5MixeriE4scm7(%rip), %al
	testb	%al, %al
	jne	.LBB34_70
# BB#61:
	movl	$_ZGVZ9model8bitR5MixeriE4scm7, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB34_70
# BB#62:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ9model8bitR5MixeriE4scm7(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB34_64
# BB#63:
	movl	%eax, programChecker+4(%rip)
.LBB34_64:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i164
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9model8bitR5MixeriE4scm7+8(%rip)
	testq	%rax, %rax
	je	.LBB34_65
# BB#67:                                # %.lr.ph.i165
	movq	%rax, _ZZ9model8bitR5MixeriE4scm7+16(%rip)
	movl	$0, _ZZ9model8bitR5MixeriE4scm7+24(%rip)
	movl	$120, %ecx
	movaps	.LCPI34_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB34_68:                              # %vector.body250
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax,%rcx,2)
	movups	%xmm0, -224(%rax,%rcx,2)
	movups	%xmm0, -208(%rax,%rcx,2)
	movups	%xmm0, -192(%rax,%rcx,2)
	movups	%xmm0, -176(%rax,%rcx,2)
	movups	%xmm0, -160(%rax,%rcx,2)
	movups	%xmm0, -144(%rax,%rcx,2)
	movups	%xmm0, -128(%rax,%rcx,2)
	movups	%xmm0, -112(%rax,%rcx,2)
	movups	%xmm0, -96(%rax,%rcx,2)
	movups	%xmm0, -80(%rax,%rcx,2)
	movups	%xmm0, -64(%rax,%rcx,2)
	movups	%xmm0, -48(%rax,%rcx,2)
	movups	%xmm0, -32(%rax,%rcx,2)
	movups	%xmm0, -16(%rax,%rcx,2)
	movups	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB34_68
# BB#69:                                # %middle.block251
	movq	%rax, _ZZ9model8bitR5MixeriE4scm7+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ9model8bitR5MixeriE4scm7, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9model8bitR5MixeriE4scm7, %edi
	callq	__cxa_guard_release
.LBB34_70:
	movb	_ZGVZ9model8bitR5MixeriE2cm(%rip), %al
	testb	%al, %al
	jne	.LBB34_74
# BB#71:
	movl	$_ZGVZ9model8bitR5MixeriE2cm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB34_74
# BB#72:
	movb	level(%rip), %cl
	movl	$262144, %esi           # imm = 0x40000
	shll	%cl, %esi
.Ltmp123:
	movl	$_ZZ9model8bitR5MixeriE2cm, %edi
	movl	$32, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp124:
# BB#73:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ9model8bitR5MixeriE2cm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9model8bitR5MixeriE2cm, %edi
	callq	__cxa_guard_release
.LBB34_74:
	cmpl	$0, bpos(%rip)
	je	.LBB34_83
# BB#75:                                # %._crit_edge
	movl	_ZZ9model8bitR5MixeriE4scm1+24(%rip), %ecx
	movl	_ZZ9model8bitR5MixeriE4scm2+24(%rip), %r14d
	movl	_ZZ9model8bitR5MixeriE4scm3+24(%rip), %r10d
	movl	_ZZ9model8bitR5MixeriE4scm4+24(%rip), %r11d
	jmp	.LBB34_84
.LBB34_83:
	movl	pos(%rip), %esi
	leal	-1(%rsi), %ecx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	movq	%r12, 16(%rsp)          # 8-byte Spill
	leal	-1(%r15), %r12d
	movl	%esi, %edx
	subl	%r12d, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %ebp
	leal	(%rbp,%rcx), %ebx
	movl	%esi, %edx
	subl	%r15d, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%edx, %ebx
	leal	1(%r15), %r13d
	subl	%r13d, %esi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	addl	%esi, %ebx
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	%ecx, %edi
	imull	%edi, %edi
	imull	%ebp, %ebp
	addl	%edi, %ebp
	movl	%edx, %edi
	imull	%edi, %edi
	addl	%ebp, %edi
	imull	%esi, %esi
	addl	%edi, %esi
	movl	%ebx, %edi
	imull	%edi, %edi
	shrl	$2, %edi
	subl	%edi, %esi
	shrl	$2, %esi
	movq	ilog+16(%rip), %rdi
	movzwl	%si, %esi
	movzbl	(%rdi,%rsi), %esi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	%ecx, %esi
	shrl	$2, %esi
	movl	%edx, %edi
	shrl	$2, %edi
	imull	$30005491, %esi, %esi   # imm = 0x1C9D8F3
	imull	$50004239, %edi, %edi   # imm = 0x2FB010F
	leal	19995673(%rsi,%rdi), %esi
	shrl	$5, %ecx
	shrl	$6, %edx
	xorl	%ecx, %edx
	orl	$67108864, %edx         # imm = 0x4000000
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%esi, %edx
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%esi, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rcx
	movl	%edx, (%rcx,%rsi,4)
	movl	pos(%rip), %edx
	leal	-1(%rdx), %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %ebp
	movl	%ebp, %esi
	shrl	$2, %esi
	addl	$-2, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %eax
	movl	%eax, %edx
	shrl	$2, %edx
	imull	$30005491, %esi, %esi   # imm = 0x1C9D8F3
	imull	$50004239, %edx, %edx   # imm = 0x2FB010F
	leal	219998652(%rsi,%rdx), %edx
	shrl	$5, %ebp
	shrl	$6, %eax
	xorl	%ebp, %eax
	orl	$67108864, %eax         # imm = 0x4000000
	xorl	%edx, %eax
	shrl	$9, %edx
	xorl	%edx, %eax
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%edx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%edx, %eax
	movl	%eax, (%rcx,%rdx,4)
	movl	pos(%rip), %edx
	movl	%edx, %esi
	subl	%r15d, %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %ebp
	movl	%ebp, %esi
	shrl	$2, %esi
	leal	(%r15,%r15), %r11d
	subl	%r11d, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	movl	%edx, %edi
	shrl	$2, %edi
	imull	$30005491, %esi, %esi   # imm = 0x1C9D8F3
	imull	$50004239, %edi, %edi   # imm = 0x2FB010F
	leal	420001631(%rsi,%rdi), %esi
	shrl	$5, %ebp
	shrl	$6, %edx
	xorl	%ebp, %edx
	orl	$67108864, %edx         # imm = 0x4000000
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%esi, %edx
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%esi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movl	%edx, (%rcx,%rsi,4)
	movl	pos(%rip), %ecx
	leal	-1(%rcx), %edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	movl	%edx, %edi
	shrl	$2, %edi
	subl	%r12d, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	movl	%ecx, %esi
	shrl	$2, %esi
	imull	$30005491, %edi, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	leal	620004610(%rdi,%rsi), %esi
	shrl	$5, %edx
	shrl	$6, %ecx
	xorl	%edx, %ecx
	xorl	$1, %ecx
	orl	$67108864, %ecx         # imm = 0x4000000
	xorl	%esi, %ecx
	shrl	$9, %esi
	xorl	%esi, %ecx
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rdi
	movl	%ecx, (%rdi,%rdx,4)
	movl	pos(%rip), %ecx
	movl	%ecx, %edx
	subl	%r15d, %edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %ebp
	movl	%ebp, %edx
	shrl	$2, %edx
	subl	%r13d, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %eax
	movl	%eax, %ecx
	shrl	$2, %ecx
	imull	$30005491, %edx, %edx   # imm = 0x1C9D8F3
	imull	$50004239, %ecx, %ecx   # imm = 0x2FB010F
	leal	820007589(%rdx,%rcx), %ecx
	shrl	$5, %ebp
	shrl	$6, %eax
	xorl	%ebp, %eax
	xorl	$1, %eax
	orl	$67108864, %eax         # imm = 0x4000000
	xorl	%ecx, %eax
	shrl	$9, %ecx
	xorl	%ecx, %eax
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%rdi,%rcx,4)
	movl	pos(%rip), %edx
	movl	%edx, %eax
	subl	%r13d, %eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %eax
	movq	buf+16(%rip), %rcx
	cltq
	movzbl	(%rcx,%rax), %ebx
	movl	%ebx, %ebp
	shrl	$2, %ebp
	leal	2(%r15), %eax
	subl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movl	%edx, %esi
	shrl	$2, %esi
	imull	$30005491, %ebp, %ebp   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	leal	1020010568(%rbp,%rsi), %esi
	shrl	$5, %ebx
	shrl	$6, %edx
	xorl	%ebx, %edx
	xorl	$1, %edx
	orl	$67108864, %edx         # imm = 0x4000000
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%esi, %edx
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %ebp
	movl	%ebp, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%esi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movl	%edx, (%rdi,%rsi,4)
	movl	pos(%rip), %edx
	movl	%edx, %esi
	subl	%r13d, %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %ebp
	movl	%ebp, %esi
	shrl	$2, %esi
	leal	2(%r15,%r15), %r8d
	subl	%r8d, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movl	%edx, %edi
	shrl	$2, %edi
	imull	$30005491, %esi, %esi   # imm = 0x1C9D8F3
	imull	$50004239, %edi, %edi   # imm = 0x2FB010F
	leal	1220013547(%rsi,%rdi), %esi
	shrl	$5, %ebp
	shrl	$6, %edx
	xorl	%ebp, %edx
	xorl	$1, %edx
	orl	$67108864, %edx         # imm = 0x4000000
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%esi, %edx
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%esi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rdi
	movl	%edx, (%rdi,%rsi,4)
	movl	pos(%rip), %edx
	movl	%edx, %esi
	subl	%r12d, %esi
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	movl	%esi, %ebp
	shrl	$2, %ebp
	leal	-2(%r15,%r15), %r9d
	subl	%r9d, %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %ecx
	movl	%ecx, %edx
	shrl	$2, %edx
	imull	$30005491, %ebp, %ebx   # imm = 0x1C9D8F3
	imull	$50004239, %edx, %edx   # imm = 0x2FB010F
	leal	1420016526(%rbx,%rdx), %edx
	shrl	$5, %esi
	shrl	$6, %ecx
	xorl	%esi, %ecx
	xorl	$2, %ecx
	orl	$67108864, %ecx         # imm = 0x4000000
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%edx, %ecx
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rdi,%rdx,4)
	movl	pos(%rip), %edx
	leal	-1(%rdx), %esi
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %esi
	movq	buf+16(%rip), %rcx
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	subl	%r15d, %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	addl	%esi, %edx
	movl	%edx, %esi
	shrl	%esi
	imull	$30005491, %esi, %esi   # imm = 0x1C9D8F3
	addl	$1570015266, %esi       # imm = 0x5D948822
	shrl	$4, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	xorl	$201326589, %esi        # imm = 0xBFFFFFD
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %ebx
	movl	%ebx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%edx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%edx, %esi
	movl	%esi, (%rdi,%rdx,4)
	movl	pos(%rip), %edx
	leal	-1(%rdx), %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	addl	$-2, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	addl	%esi, %edx
	movl	%edx, %esi
	shrl	%esi
	imull	$30005491, %esi, %esi   # imm = 0x1C9D8F3
	addl	$1770018245, %esi       # imm = 0x698055C5
	shrl	$4, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	xorl	$201326589, %esi        # imm = 0xBFFFFFD
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdi
	leal	1(%rdi), %edx
	movl	%edx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %esi, %edx  # imm = 0x3ADE68B3
	addl	%edi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %esi  # imm = 0x75BCD17
	addl	%edi, %esi
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rdx
	movl	%esi, (%rdx,%rdi,4)
	movl	pos(%rip), %esi
	movl	%esi, %edi
	subl	%r15d, %edi
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	subl	%r11d, %esi
	andl	%ebx, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %ecx
	addl	%edi, %ecx
	movl	%ecx, %esi
	shrl	%esi
	imull	$30005491, %esi, %esi   # imm = 0x1C9D8F3
	addl	$1970021224, %esi       # imm = 0x756C2368
	shrl	$4, %ecx
	xorl	%esi, %ecx
	shrl	$9, %esi
	xorl	%ecx, %esi
	xorl	$201326589, %esi        # imm = 0xBFFFFFD
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rcx
	leal	1(%rcx), %edi
	movl	%edi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%ecx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%ecx, %esi
	movl	%esi, (%rdx,%rcx,4)
	movl	pos(%rip), %esi
	leal	-1(%rsi), %edi
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %edi
	movq	buf+16(%rip), %rcx
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	subl	%r12d, %esi
	andl	%ebx, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	addl	%edi, %esi
	movl	%esi, %edi
	shrl	%edi
	imull	$30005491, %edi, %edi   # imm = 0x1C9D8F3
	addl	$-2124943093, %edi      # imm = 0x8157F10B
	shrl	$4, %esi
	xorl	%edi, %esi
	shrl	$9, %edi
	xorl	%esi, %edi
	xorl	$201326588, %edi        # imm = 0xBFFFFFC
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %ebx
	movl	%ebx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edi, %edi  # imm = 0x3ADE68B3
	addl	%esi, %edi
	roll	$16, %edi
	imull	$123456791, %edi, %edi  # imm = 0x75BCD17
	addl	%esi, %edi
	movl	%edi, (%rdx,%rsi,4)
	movl	pos(%rip), %edx
	movl	%edx, %esi
	subl	%r15d, %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	subl	%r13d, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	addl	%esi, %edx
	movl	%edx, %esi
	shrl	%esi
	imull	$30005491, %esi, %esi   # imm = 0x1C9D8F3
	addl	$-1924940114, %esi      # imm = 0x8D43BEAE
	shrl	$4, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	xorl	$201326588, %esi        # imm = 0xBFFFFFC
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdi
	leal	1(%rdi), %edx
	movl	%edx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %esi, %edx  # imm = 0x3ADE68B3
	addl	%edi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %esi  # imm = 0x75BCD17
	addl	%edi, %esi
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rdx
	movl	%esi, (%rdx,%rdi,4)
	movl	pos(%rip), %esi
	movl	%esi, %edi
	subl	%r13d, %edi
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	subl	%eax, %esi
	andl	%ebx, %esi
	movslq	%esi, %rax
	movzbl	(%rcx,%rax), %eax
	addl	%edi, %eax
	movl	%eax, %ecx
	shrl	%ecx
	imull	$30005491, %ecx, %ecx   # imm = 0x1C9D8F3
	addl	$-1724937135, %ecx      # imm = 0x992F8C51
	shrl	$4, %eax
	xorl	%ecx, %eax
	shrl	$9, %ecx
	xorl	%eax, %ecx
	xorl	$201326588, %ecx        # imm = 0xBFFFFFC
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rax
	leal	1(%rax), %esi
	movl	%esi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%rdx,%rax,4)
	movl	pos(%rip), %ecx
	movl	%ecx, %esi
	subl	%r13d, %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	subl	%r8d, %ecx
	andl	%edi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%esi, %ecx
	movl	%ecx, %esi
	shrl	%esi
	imull	$30005491, %esi, %esi   # imm = 0x1C9D8F3
	addl	$-1524934156, %esi      # imm = 0xA51B59F4
	shrl	$4, %ecx
	xorl	%esi, %ecx
	shrl	$9, %esi
	xorl	%ecx, %esi
	xorl	$201326588, %esi        # imm = 0xBFFFFFC
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rcx
	leal	1(%rcx), %edi
	movl	%edi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%ecx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%ecx, %esi
	movl	%esi, (%rdx,%rcx,4)
	movl	pos(%rip), %ecx
	movl	%ecx, %edx
	subl	%r12d, %edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	subl	%r9d, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edx, %ecx
	movl	%ecx, %edx
	shrl	%edx
	imull	$30005491, %edx, %edx   # imm = 0x1C9D8F3
	addl	$-1324931177, %edx      # imm = 0xB1072797
	shrl	$4, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	xorl	$201326587, %edx        # imm = 0xBFFFFFB
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%esi, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rcx
	movl	%edx, (%rcx,%rsi,4)
	movl	pos(%rip), %edi
	movl	%edi, %edx
	subl	%r15d, %edx
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	movl	%edx, %ebp
	shrl	$2, %ebp
	leal	-1(%rdi), %esi
	andl	%ebx, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	subl	%r12d, %edi
	andl	%ebx, %edi
	movl	%esi, %ebx
	shrl	$2, %ebx
	movslq	%edi, %rdi
	movzbl	(%rax,%rdi), %eax
	movl	%eax, %edi
	shrl	$2, %edi
	imull	$30005491, %ebp, %ebp   # imm = 0x1C9D8F3
	imull	$50004239, %ebx, %ebx   # imm = 0x2FB010F
	imull	$70004807, %edi, %edi   # imm = 0x42C3047
	addl	%ebp, %ebx
	leal	-1004919152(%rdi,%rbx), %edi
	shrl	$5, %edx
	shrl	$6, %esi
	shrl	$7, %eax
	xorl	%edx, %esi
	xorl	%eax, %esi
	xorl	%edi, %esi
	shrl	$9, %edi
	xorl	%esi, %edi
	xorl	$67108859, %edi         # imm = 0x3FFFFFB
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edi, %edx  # imm = 0x3ADE68B3
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movl	%edx, (%rcx,%rax,4)
	movl	pos(%rip), %esi
	movl	%esi, %ecx
	subl	%r12d, %ecx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	movl	%ecx, %ebx
	shrl	$2, %ebx
	movl	%esi, %edx
	subl	%r15d, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	movl	%edx, %ebp
	shrl	$2, %ebp
	subl	%r13d, %esi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	movl	%esi, %edi
	shrl	$2, %edi
	imull	$30005491, %ebx, %ebx   # imm = 0x1C9D8F3
	imull	$50004239, %ebp, %ebp   # imm = 0x2FB010F
	imull	$70004807, %edi, %edi   # imm = 0x42C3047
	addl	%ebx, %ebp
	leal	-804916173(%rdi,%rbp), %edi
	shrl	$5, %ecx
	shrl	$6, %edx
	shrl	$7, %esi
	xorl	%ecx, %edx
	xorl	%esi, %edx
	xorl	%edi, %edx
	shrl	$9, %edi
	xorl	%edx, %edi
	xorl	$67108859, %edi         # imm = 0x3FFFFFB
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edi, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %esi  # imm = 0x75BCD17
	addl	%edx, %esi
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rcx
	movl	%esi, (%rcx,%rdx,4)
	movl	pos(%rip), %esi
	leal	-1(%rsi), %edx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	movl	%edx, %ebx
	shrl	$2, %ebx
	movl	$1, %ebp
	subl	%r11d, %ebp
	addl	%esi, %ebp
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	subl	%r12d, %esi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	andl	%edi, %ebp
	movl	%esi, %edi
	shrl	$2, %edi
	movslq	%ebp, %rbp
	movzbl	(%rax,%rbp), %eax
	movl	%eax, %ebp
	shrl	$2, %ebp
	imull	$30005491, %ebx, %ebx   # imm = 0x1C9D8F3
	imull	$50004239, %edi, %edi   # imm = 0x2FB010F
	imull	$70004807, %ebp, %ebp   # imm = 0x42C3047
	addl	%ebx, %edi
	leal	-604913194(%rbp,%rdi), %edi
	shrl	$5, %edx
	shrl	$6, %esi
	shrl	$7, %eax
	xorl	%edx, %esi
	xorl	%eax, %esi
	xorl	%edi, %esi
	shrl	$9, %edi
	xorl	%esi, %edi
	xorl	$67108859, %edi         # imm = 0x3FFFFFB
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edi, %edx  # imm = 0x3ADE68B3
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movl	%edx, (%rcx,%rax,4)
	movl	pos(%rip), %edx
	leal	-3(%rdx), %ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %edi
	movl	%edx, %ecx
	subl	%r15d, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edi, %ecx
	movl	%ecx, %edi
	shrl	%edi
	leal	-1(%rdx), %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	movl	%ebx, %ebp
	shrl	$2, %ebp
	addl	$-2, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	movl	%edx, %esi
	shrl	$2, %esi
	imull	$30005491, %edi, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %ebp, %ebp   # imm = 0x2FB010F
	imull	$70004807, %esi, %esi   # imm = 0x42C3047
	addl	%edi, %ebp
	leal	-404910215(%rsi,%rbp), %esi
	shrl	$4, %ecx
	shrl	$6, %ebx
	shrl	$7, %edx
	xorl	%ebx, %edx
	xorl	%ecx, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	xorl	$67108858, %esi         # imm = 0x3FFFFFA
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %esi, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %esi  # imm = 0x75BCD17
	addl	%edx, %esi
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rcx
	movl	%esi, (%rcx,%rdx,4)
	movl	pos(%rip), %esi
	leal	-2(%rsi), %edx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %ebx
	leal	-1(%rsi), %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ebx, %edx
	movl	%esi, %ebx
	subl	%r15d, %ebx
	andl	%edi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	movl	%esi, %ebp
	subl	%r11d, %ebp
	andl	%edi, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rax,%rbp), %ebp
	addl	%ebx, %ebp
	subl	%r12d, %esi
	andl	%edi, %esi
	movl	%edx, %edi
	shrl	%edi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %eax
	movl	%ebp, %esi
	shrl	%esi
	imull	$30005491, %edi, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	addl	%edi, %esi
	movl	%eax, %edi
	shrl	$2, %edi
	imull	$70004807, %edi, %edi   # imm = 0x42C3047
	leal	-204907236(%rdi,%rsi), %esi
	shrl	$4, %edx
	shrl	$5, %ebp
	shrl	$7, %eax
	xorl	%edx, %ebp
	xorl	%eax, %ebp
	xorl	%esi, %ebp
	shrl	$9, %esi
	xorl	%ebp, %esi
	xorl	$67108858, %esi         # imm = 0x3FFFFFA
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %esi, %edx  # imm = 0x3ADE68B3
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movl	%edx, (%rcx,%rax,4)
	movl	pos(%rip), %edx
	leal	-2(%rdx), %ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %edi
	leal	-1(%rdx), %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edi, %ecx
	movl	%ecx, %edi
	shrl	$2, %edi
	movl	%edx, %ebx
	subl	%r12d, %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	subl	%r15d, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ebx, %edx
	movl	%edx, %esi
	shrl	$2, %esi
	imull	$30005491, %edi, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	leal	-74909064(%rdi,%rsi), %esi
	shrl	$5, %ecx
	shrl	$6, %edx
	xorl	%ecx, %edx
	xorl	$5, %edx
	orl	$67108864, %edx         # imm = 0x4000000
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%esi, %edx
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%esi, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rcx
	movl	%edx, (%rcx,%rsi,4)
	movl	pos(%rip), %esi
	leal	-2(%rsi), %edx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %ebx
	leal	-1(%rsi), %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ebx, %edx
	movl	%esi, %ebx
	subl	%r15d, %ebx
	andl	%edi, %ebx
	subl	%r11d, %esi
	andl	%edi, %esi
	movl	%edx, %edi
	shrl	%edi
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %eax
	addl	%ebx, %eax
	movl	%eax, %esi
	shrl	%esi
	imull	$30005491, %edi, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	leal	125093915(%rdi,%rsi), %esi
	shrl	$4, %edx
	shrl	$5, %eax
	xorl	%edx, %eax
	xorl	$5, %eax
	orl	$67108864, %eax         # imm = 0x4000000
	xorl	%esi, %eax
	shrl	$9, %esi
	xorl	%esi, %eax
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%edx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%edx, %eax
	movl	%eax, (%rcx,%rdx,4)
	movl	pos(%rip), %edx
	leal	-2(%rdx), %ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %edi
	leal	-1(%rdx), %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edi, %ecx
	movl	%ecx, %edi
	shrl	%edi
	movl	%edx, %ebx
	subl	%r12d, %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	subl	%r9d, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ebx, %edx
	movl	%edx, %esi
	shrl	%esi
	imull	$30005491, %edi, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	leal	325096894(%rdi,%rsi), %esi
	shrl	$4, %ecx
	shrl	$5, %edx
	xorl	%ecx, %edx
	xorl	$6, %edx
	orl	$67108864, %edx         # imm = 0x4000000
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%esi, %edx
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%esi, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rcx
	movl	%edx, (%rcx,%rsi,4)
	movl	pos(%rip), %esi
	leal	-2(%rsi), %edx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %ebp
	leal	-1(%rsi), %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ebp, %edx
	movl	%edx, %ebp
	shrl	%ebp
	movl	%esi, %ebx
	subl	%r13d, %ebx
	andl	%edi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	subl	%r8d, %esi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %eax
	addl	%ebx, %eax
	movl	%eax, %esi
	shrl	%esi
	imull	$30005491, %ebp, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	leal	525099873(%rdi,%rsi), %esi
	shrl	$4, %edx
	shrl	$5, %eax
	xorl	%edx, %eax
	xorl	$6, %eax
	orl	$67108864, %eax         # imm = 0x4000000
	xorl	%esi, %eax
	shrl	$9, %esi
	xorl	%esi, %eax
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%edx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%edx, %eax
	movl	%eax, (%rcx,%rdx,4)
	movl	pos(%rip), %ebp
	movl	%ebp, %ecx
	subl	%r15d, %ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %edi
	movl	%ebp, %ecx
	subl	%r11d, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edi, %ecx
	movl	%ecx, %edi
	shrl	%edi
	movl	%ebp, %edx
	subl	%r12d, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	subl	%r8d, %ebp
	andl	%esi, %ebp
	movslq	%ebp, %rsi
	movzbl	(%rax,%rsi), %esi
	addl	%edx, %esi
	movl	%esi, %edx
	shrl	%edx
	imull	$30005491, %edi, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %edx, %edx   # imm = 0x2FB010F
	leal	725102852(%rdi,%rdx), %edx
	shrl	$4, %ecx
	shrl	$5, %esi
	xorl	%ecx, %esi
	xorl	$6, %esi
	orl	$67108864, %esi         # imm = 0x4000000
	xorl	%edx, %esi
	shrl	$9, %edx
	xorl	%edx, %esi
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %esi, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %esi  # imm = 0x75BCD17
	addl	%edx, %esi
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rcx
	movl	%esi, (%rcx,%rdx,4)
	movl	pos(%rip), %ebp
	movl	%ebp, %edx
	subl	%r12d, %edx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	movl	%ebp, %esi
	subl	%r15d, %esi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	addl	%esi, %edx
	movl	%edx, %ebx
	shrl	%ebx
	subl	%r13d, %ebp
	andl	%edi, %ebp
	movslq	%ebp, %rdi
	movzbl	(%rax,%rdi), %eax
	addl	%esi, %eax
	movl	%eax, %esi
	shrl	%esi
	imull	$30005491, %ebx, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	leal	925105831(%rdi,%rsi), %esi
	shrl	$4, %edx
	shrl	$5, %eax
	xorl	%edx, %eax
	xorl	$6, %eax
	orl	$67108864, %eax         # imm = 0x4000000
	xorl	%esi, %eax
	shrl	$9, %esi
	xorl	%esi, %eax
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%edx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%edx, %eax
	movl	%eax, (%rcx,%rdx,4)
	movl	pos(%rip), %edx
	leal	-1(%rdx), %ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %edi
	movl	%edx, %ecx
	subl	%r12d, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edi, %ecx
	movl	%ecx, %edi
	shrl	%edi
	movl	%edx, %ebp
	subl	%r15d, %ebp
	andl	%esi, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rax,%rbp), %ebp
	subl	%r11d, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ebp, %edx
	movl	%edx, %esi
	shrl	%esi
	imull	$30005491, %edi, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	leal	1125108810(%rdi,%rsi), %esi
	shrl	$4, %ecx
	shrl	$5, %edx
	xorl	%ecx, %edx
	xorl	$7, %edx
	orl	$67108864, %edx         # imm = 0x4000000
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%esi, %edx
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%esi, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rcx
	movl	%edx, (%rcx,%rsi,4)
	movl	pos(%rip), %esi
	leal	-1(%rsi), %edx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %ebp
	movl	%esi, %edx
	subl	%r12d, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ebp, %edx
	movl	%edx, %ebp
	shrl	$2, %ebp
	movl	%esi, %ebx
	subl	%r15d, %ebx
	andl	%edi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	subl	%r13d, %esi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %eax
	addl	%ebx, %eax
	movl	%eax, %esi
	shrl	$2, %esi
	imull	$30005491, %ebp, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %esi, %esi   # imm = 0x2FB010F
	leal	1325111789(%rdi,%rsi), %esi
	shrl	$5, %edx
	shrl	$6, %eax
	xorl	%edx, %eax
	xorl	$7, %eax
	orl	$67108864, %eax         # imm = 0x4000000
	xorl	%esi, %eax
	shrl	$9, %esi
	xorl	%esi, %eax
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%edx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%edx, %eax
	movl	%eax, (%rcx,%rdx,4)
	movl	pos(%rip), %ecx
	leal	-1(%rcx), %edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %edx
	movq	buf+16(%rip), %rax
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	movl	%ecx, %edi
	subl	%r12d, %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%rax,%rdi), %edi
	subl	%edi, %edx
	sarl	%edx
	subl	%r15d, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edx, %ecx
	sarl	$2, %ecx
	imull	$30005491, %ecx, %edx   # imm = 0x1C9D8F3
	addl	$1475110529, %edx       # imm = 0x57EC6681
	shrl	$3, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	xorl	$201326584, %edx        # imm = 0xBFFFFF8
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%esi, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movq	_ZZ9model8bitR5MixeriE2cm+96(%rip), %rcx
	movl	%edx, (%rcx,%rsi,4)
	movl	pos(%rip), %ebp
	movl	%ebp, %esi
	subl	%r12d, %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	movl	%ebp, %edx
	subl	%r15d, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	subl	%edx, %esi
	sarl	%esi
	decl	%ebp
	andl	%edi, %ebp
	movslq	%ebp, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%esi, %edx
	sarl	$2, %edx
	imull	$30005491, %edx, %esi   # imm = 0x1C9D8F3
	addl	$1675113508, %esi       # imm = 0x63D83424
	shrl	$3, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	xorl	$201326584, %esi        # imm = 0xBFFFFF8
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rdx
	leal	1(%rdx), %edi
	movl	%edi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%edx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%edx, %esi
	movl	%esi, (%rcx,%rdx,4)
	movl	pos(%rip), %edx
	leal	-1(%rdx), %esi
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	movl	%edx, %ebp
	subl	%r12d, %ebp
	andl	%edi, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rax,%rbp), %ebp
	subl	%esi, %ebp
	subl	%r15d, %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %eax
	addl	%ebp, %eax
	sarl	$2, %eax
	imull	$30005491, %eax, %edx   # imm = 0x1C9D8F3
	addl	$1875116487, %edx       # imm = 0x6FC401C7
	shrl	$3, %eax
	xorl	%edx, %eax
	shrl	$9, %edx
	xorl	%eax, %edx
	xorl	$201326583, %edx        # imm = 0xBFFFFF7
	movslq	_ZZ9model8bitR5MixeriE2cm+136(%rip), %rax
	leal	1(%rax), %esi
	movl	%esi, _ZZ9model8bitR5MixeriE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movl	%edx, (%rcx,%rax,4)
	movl	pos(%rip), %eax
	leal	-1(%rax), %ecx
	movl	buf(%rip), %r8d
	decl	%r8d
	andl	%r8d, %ecx
	movq	buf+16(%rip), %rbp
	movslq	%ecx, %r9
	movzbl	(%rbp,%r9), %ecx
	movl	%eax, %edx
	subl	%r15d, %edx
	andl	%r8d, %edx
	movslq	%edx, %r15
	movzbl	(%rbp,%r15), %edx
	addl	%ecx, %edx
	shll	$7, %edx
	movl	$65280, %ecx            # imm = 0xFF00
	addl	_ZZ9model8bitR5MixeriE4scm1(%rip), %ecx
	andl	%edx, %ecx
	andl	$65280, %ecx            # imm = 0xFF00
	movl	%ecx, _ZZ9model8bitR5MixeriE4scm1+24(%rip)
	movzbl	(%rbp,%r9), %edx
	movzbl	(%rbp,%r15), %ebx
	addl	%edx, %ebx
	movl	%eax, %edx
	subl	%r13d, %edx
	andl	%r8d, %edx
	movslq	%edx, %rdx
	movzbl	(%rbp,%rdx), %edx
	subl	%edx, %ebx
	shll	$7, %ebx
	movl	$-256, %r13d
	movl	_ZZ9model8bitR5MixeriE4scm2(%rip), %r14d
	addl	%r13d, %r14d
	andl	%ebx, %r14d
	andl	$-256, %r14d
	movl	%r14d, _ZZ9model8bitR5MixeriE4scm2+24(%rip)
	movzbl	(%rbp,%r9), %esi
	addl	%esi, %esi
	leal	-2(%rax), %ebx
	andl	%r8d, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rbp,%rbx), %ebx
	subl	%ebx, %esi
	shll	$7, %esi
	movl	_ZZ9model8bitR5MixeriE4scm3(%rip), %r10d
	addl	%r13d, %r10d
	andl	%esi, %r10d
	andl	$-256, %r10d
	movl	%r10d, _ZZ9model8bitR5MixeriE4scm3+24(%rip)
	movl	%eax, %esi
	subl	%r11d, %esi
	movzbl	(%rbp,%r15), %edx
	addl	%edx, %edx
	andl	%r8d, %esi
	movslq	%esi, %rsi
	movzbl	(%rbp,%rsi), %esi
	subl	%esi, %edx
	shll	$7, %edx
	movl	_ZZ9model8bitR5MixeriE4scm4(%rip), %r11d
	addl	%r13d, %r11d
	andl	%edx, %r11d
	andl	$-256, %r11d
	movl	%r11d, _ZZ9model8bitR5MixeriE4scm4+24(%rip)
	movzbl	(%rbp,%r9), %edx
	movzbl	(%rbp,%r15), %esi
	addl	%edx, %esi
	subl	%r12d, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	andl	%r8d, %eax
	cltq
	movzbl	(%rbp,%rax), %eax
	subl	%eax, %esi
	shll	$7, %esi
	movl	_ZZ9model8bitR5MixeriE4scm5(%rip), %eax
	addl	%r13d, %eax
	andl	%esi, %eax
	andl	$-256, %eax
	movl	%eax, _ZZ9model8bitR5MixeriE4scm5+24(%rip)
	movl	12(%rsp), %eax          # 4-byte Reload
	shrl	$3, %eax
	movl	8(%rsp), %edx           # 4-byte Reload
	addl	%edx, %edx
	andl	$384, %edx              # imm = 0x180
	orl	%eax, %edx
	shll	$8, %edx
	addl	_ZZ9model8bitR5MixeriE4scm6(%rip), %r13d
	andl	%edx, %r13d
	movl	%r13d, _ZZ9model8bitR5MixeriE4scm6+24(%rip)
.LBB34_84:
	movl	y(%rip), %esi
	shll	$16, %esi
	movq	_ZZ9model8bitR5MixeriE4scm1+32(%rip), %rax
	movzwl	(%rax), %edi
	orl	$64, %esi
	movl	%esi, %edx
	subl	%edi, %edx
	shrl	$7, %edx
	addl	%edi, %edx
	movw	%dx, (%rax)
	movl	c0(%rip), %r8d
	addl	%r8d, %ecx
	movq	_ZZ9model8bitR5MixeriE4scm1+16(%rip), %rax
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, _ZZ9model8bitR5MixeriE4scm1+32(%rip)
	movzwl	(%rax,%rcx,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movq	stretch+16(%rip), %rcx
	movzwl	(%rcx,%rax), %edx
	movslq	96(%r12), %rax
	movq	32(%r12), %r15
	movw	%dx, (%r15,%rax,2)
	movq	_ZZ9model8bitR5MixeriE4scm2+32(%rip), %rdx
	movzwl	(%rdx), %edi
	movl	%esi, %ebx
	subl	%edi, %ebx
	shrl	$7, %ebx
	addl	%edi, %ebx
	movw	%bx, (%rdx)
	addl	%r8d, %r14d
	movq	_ZZ9model8bitR5MixeriE4scm2+16(%rip), %rdx
	movslq	%r14d, %rdi
	leaq	(%rdx,%rdi,2), %rbx
	movq	%rbx, _ZZ9model8bitR5MixeriE4scm2+32(%rip)
	movzwl	(%rdx,%rdi,2), %edx
	shrq	$3, %rdx
	andl	$8190, %edx             # imm = 0x1FFE
	movzwl	(%rcx,%rdx), %edx
	movw	%dx, 2(%r15,%rax,2)
	movq	_ZZ9model8bitR5MixeriE4scm3+32(%rip), %rdx
	movzwl	(%rdx), %edi
	movl	%esi, %ebp
	subl	%edi, %ebp
	shrl	$7, %ebp
	addl	%edi, %ebp
	movw	%bp, (%rdx)
	addl	%r8d, %r10d
	movq	_ZZ9model8bitR5MixeriE4scm3+16(%rip), %rdx
	movslq	%r10d, %rdi
	leaq	(%rdx,%rdi,2), %rbp
	movq	%rbp, _ZZ9model8bitR5MixeriE4scm3+32(%rip)
	movzwl	(%rdx,%rdi,2), %edx
	shrq	$3, %rdx
	andl	$8190, %edx             # imm = 0x1FFE
	movzwl	(%rcx,%rdx), %edx
	movw	%dx, 4(%r15,%rax,2)
	movq	_ZZ9model8bitR5MixeriE4scm4+32(%rip), %rdx
	movzwl	(%rdx), %edi
	subl	%edi, %esi
	shrl	$7, %esi
	addl	%edi, %esi
	movw	%si, (%rdx)
	addl	%r8d, %r11d
	movq	_ZZ9model8bitR5MixeriE4scm4+16(%rip), %rdx
	movslq	%r11d, %rsi
	leaq	(%rdx,%rsi,2), %rdi
	movq	%rdi, _ZZ9model8bitR5MixeriE4scm4+32(%rip)
	movzwl	(%rdx,%rsi,2), %edx
	shrq	$3, %rdx
	andl	$8190, %edx             # imm = 0x1FFE
	movzwl	(%rcx,%rdx), %ecx
	movw	%cx, 6(%r15,%rax,2)
	movl	y(%rip), %r9d
	movl	%r9d, %ebx
	shll	$16, %ebx
	movq	_ZZ9model8bitR5MixeriE4scm5+32(%rip), %rcx
	movzwl	(%rcx), %edx
	orl	$64, %ebx
	movl	%ebx, %edi
	subl	%edx, %edi
	shrl	$7, %edi
	addl	%edx, %edi
	movw	%di, (%rcx)
	movl	c0(%rip), %edx
	movl	_ZZ9model8bitR5MixeriE4scm5+24(%rip), %ecx
	addl	%edx, %ecx
	movq	_ZZ9model8bitR5MixeriE4scm5+16(%rip), %rdi
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx,2), %rbp
	movq	%rbp, _ZZ9model8bitR5MixeriE4scm5+32(%rip)
	movzwl	(%rdi,%rcx,2), %ecx
	shrq	$3, %rcx
	andl	$8190, %ecx             # imm = 0x1FFE
	movq	stretch+16(%rip), %rdi
	movzwl	(%rdi,%rcx), %ecx
	movw	%cx, 8(%r15,%rax,2)
	movq	_ZZ9model8bitR5MixeriE4scm6+32(%rip), %rcx
	movzwl	(%rcx), %ebp
	movl	%ebx, %esi
	subl	%ebp, %esi
	shrl	$7, %esi
	addl	%ebp, %esi
	movw	%si, (%rcx)
	movl	_ZZ9model8bitR5MixeriE4scm6+24(%rip), %ecx
	addl	%edx, %ecx
	movq	_ZZ9model8bitR5MixeriE4scm6+16(%rip), %rsi
	movslq	%ecx, %rcx
	leaq	(%rsi,%rcx,2), %rbp
	movq	%rbp, _ZZ9model8bitR5MixeriE4scm6+32(%rip)
	movzwl	(%rsi,%rcx,2), %ecx
	shrq	$3, %rcx
	andl	$8190, %ecx             # imm = 0x1FFE
	movzwl	(%rdi,%rcx), %ecx
	movq	32(%r12), %rsi
	movw	%cx, 10(%rsi,%rax,2)
	movq	_ZZ9model8bitR5MixeriE4scm7+32(%rip), %rcx
	movzwl	(%rcx), %ebp
	subl	%ebp, %ebx
	shrl	$7, %ebx
	addl	%ebp, %ebx
	movw	%bx, (%rcx)
	movl	_ZZ9model8bitR5MixeriE4scm7+24(%rip), %ecx
	addl	%edx, %ecx
	movq	_ZZ9model8bitR5MixeriE4scm7+16(%rip), %rbp
	movslq	%ecx, %rcx
	leaq	(%rbp,%rcx,2), %rbx
	movq	%rbx, _ZZ9model8bitR5MixeriE4scm7+32(%rip)
	movzwl	(%rbp,%rcx,2), %ecx
	shrq	$3, %rcx
	andl	$8190, %ecx             # imm = 0x1FFE
	movzwl	(%rdi,%rcx), %ecx
	leal	7(%rax), %edi
	movl	%edi, 96(%r12)
	movw	%cx, 12(%rsi,%rax,2)
	movl	bpos(%rip), %ecx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	$_ZZ9model8bitR5MixeriE2cm, %edi
	movq	%r12, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN10ContextMap4mix1ER5Mixeriiii # TAILCALL
.LBB34_5:
.Ltmp102:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp103:
# BB#6:                                 # %.noexc
.LBB34_15:
.Ltmp105:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp106:
# BB#16:                                # %.noexc138
.LBB34_25:
.Ltmp108:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp109:
# BB#26:                                # %.noexc144
.LBB34_35:
.Ltmp111:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp112:
# BB#36:                                # %.noexc150
.LBB34_45:
.Ltmp114:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp115:
# BB#46:                                # %.noexc156
.LBB34_55:
.Ltmp117:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp118:
# BB#56:                                # %.noexc162
.LBB34_65:
.Ltmp120:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp121:
# BB#66:                                # %.noexc168
.LBB34_81:
.Ltmp122:
	movq	%rax, %rbx
	movl	$_ZGVZ9model8bitR5MixeriE4scm7, %edi
	jmp	.LBB34_86
.LBB34_80:
.Ltmp119:
	movq	%rax, %rbx
	movl	$_ZGVZ9model8bitR5MixeriE4scm6, %edi
	jmp	.LBB34_86
.LBB34_79:
.Ltmp116:
	movq	%rax, %rbx
	movl	$_ZGVZ9model8bitR5MixeriE4scm5, %edi
	jmp	.LBB34_86
.LBB34_78:
.Ltmp113:
	movq	%rax, %rbx
	movl	$_ZGVZ9model8bitR5MixeriE4scm4, %edi
	jmp	.LBB34_86
.LBB34_77:
.Ltmp110:
	movq	%rax, %rbx
	movl	$_ZGVZ9model8bitR5MixeriE4scm3, %edi
	jmp	.LBB34_86
.LBB34_76:
.Ltmp107:
	movq	%rax, %rbx
	movl	$_ZGVZ9model8bitR5MixeriE4scm2, %edi
	jmp	.LBB34_86
.LBB34_85:
.Ltmp104:
	movq	%rax, %rbx
	movl	$_ZGVZ9model8bitR5MixeriE4scm1, %edi
	jmp	.LBB34_86
.LBB34_82:
.Ltmp125:
	movq	%rax, %rbx
	movl	$_ZGVZ9model8bitR5MixeriE2cm, %edi
.LBB34_86:
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end34:
	.size	_Z9model8bitR5Mixeri, .Lfunc_end34-_Z9model8bitR5Mixeri
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\205\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp123-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin10 #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp102-.Ltmp124       #   Call between .Ltmp124 and .Ltmp102
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin10 #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin10 #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin10 #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin10 #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin10 #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin10 #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin10 # >> Call Site 9 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin10 #     jumps to .Ltmp122
	.byte	0                       #   On action: cleanup
	.long	.Ltmp121-.Lfunc_begin10 # >> Call Site 10 <<
	.long	.Lfunc_end34-.Ltmp121   #   Call between .Ltmp121 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z8pgmModelR5Mixer
	.p2align	4, 0x90
	.type	_Z8pgmModelR5Mixer,@function
_Z8pgmModelR5Mixer:                     # @_Z8pgmModelR5Mixer
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi151:
	.cfi_def_cfa_offset 80
.Lcfi152:
	.cfi_offset %rbx, -48
.Lcfi153:
	.cfi_offset %r12, -40
.Lcfi154:
	.cfi_offset %r13, -32
.Lcfi155:
	.cfi_offset %r14, -24
.Lcfi156:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	cmpl	$0, bpos(%rip)
	je	.LBB35_1
.LBB35_36:                              # %.loopexit.thread
	movl	pos(%rip), %eax
	cmpl	_ZZ8pgmModelR5MixerE3eoi(%rip), %eax
	jle	.LBB35_38
.LBB35_37:
	movl	$0, _ZZ8pgmModelR5MixerE1w(%rip)
	xorl	%eax, %eax
	jmp	.LBB35_39
.LBB35_38:
	movl	_ZZ8pgmModelR5MixerE1w(%rip), %esi
	movq	%r12, %rdi
	callq	_Z9model8bitR5Mixeri
	movl	_ZZ8pgmModelR5MixerE3col(%rip), %eax
	leal	1(%rax), %ecx
	xorl	%edx, %edx
	cmpl	$6, %eax
	cmovlel	%ecx, %edx
	movl	%edx, _ZZ8pgmModelR5MixerE3col(%rip)
	movl	92(%r12), %eax
	addl	$2, %eax
	movslq	88(%r12), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, 88(%r12)
	movq	80(%r12), %rcx
	movl	%eax, (%rcx,%rsi,4)
	movl	92(%r12), %eax
	leal	8(%rax), %esi
	movl	%esi, 92(%r12)
	leal	8(%rdx,%rax), %eax
	movslq	88(%r12), %rdx
	leal	1(%rdx), %esi
	movl	%esi, 88(%r12)
	movl	%eax, (%rcx,%rdx,4)
	movl	92(%r12), %r8d
	leal	8(%r8), %eax
	movl	%eax, 92(%r12)
	movl	_ZZ8pgmModelR5MixerE1w(%rip), %eax
	movl	pos(%rip), %esi
	movl	%esi, %edi
	subl	%eax, %edi
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %edi
	movq	buf+16(%rip), %rdx
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	decl	%esi
	andl	%ebx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %edx
	addl	%edi, %edx
	shrl	$4, %edx
	leal	8(%rdx,%r8), %edx
	movslq	88(%r12), %rsi
	leal	1(%rsi), %edi
	movl	%edi, 88(%r12)
	movl	%edx, (%rcx,%rsi,4)
	movl	92(%r12), %edx
	addl	$32, %edx
	movl	%edx, 92(%r12)
	addl	c0(%rip), %edx
	movslq	88(%r12), %rsi
	leal	1(%rsi), %edi
	movl	%edi, 88(%r12)
	movl	%edx, (%rcx,%rsi,4)
	addl	$256, 92(%r12)          # imm = 0x100
.LBB35_39:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB35_1:
	movl	pos(%rip), %r9d
	leal	-3(%r9), %ecx
	movl	buf(%rip), %edx
	leal	-1(%rdx), %eax
	andl	%eax, %ecx
	movq	buf+16(%rip), %rsi
	movslq	%ecx, %rcx
	cmpb	$80, (%rsi,%rcx)
	jne	.LBB35_6
# BB#2:
	leal	-2(%r9), %ecx
	andl	%eax, %ecx
	movslq	%ecx, %rcx
	cmpb	$53, (%rsi,%rcx)
	jne	.LBB35_6
# BB#3:
	leal	-1(%r9), %ecx
	andl	%ecx, %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	cmpq	$32, %rax
	ja	.LBB35_6
# BB#4:
	movabsq	$4294977024, %rcx       # imm = 0x100002600
	btq	%rax, %rcx
	jae	.LBB35_6
# BB#5:
	movl	%r9d, _ZZ8pgmModelR5MixerE3pgm(%rip)
	movl	$0, _ZZ8pgmModelR5MixerE7pgm_ptr(%rip)
	jmp	.LBB35_37
.LBB35_6:
	movl	_ZZ8pgmModelR5MixerE3pgm(%rip), %edi
	testl	%edi, %edi
	je	.LBB35_36
# BB#7:
	movl	_ZZ8pgmModelR5MixerE7pgm_ptr(%rip), %r8d
	cmpl	$3, %r8d
	je	.LBB35_36
# BB#8:                                 # %.preheader55
	leal	-1(%r9), %eax
	cmpl	%eax, %edi
	jge	.LBB35_36
# BB#9:                                 # %.preheader55
	cmpl	$2, %r8d
	jg	.LBB35_36
# BB#10:                                # %.preheader54.preheader
	movabsq	$4294977024, %r15       # imm = 0x100002600
	movq	%rsp, %r14
	jmp	.LBB35_11
.LBB35_20:                              # %.critedge2.loopexit
                                        #   in Loop: Header=BB35_11 Depth=1
	incl	%r13d
	jmp	.LBB35_27
.LBB35_26:                              # %.critedge49
                                        #   in Loop: Header=BB35_11 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r14, %rdi
	callq	strtol
	movslq	_ZZ8pgmModelR5MixerE7pgm_ptr(%rip), %rcx
	leal	1(%rcx), %r8d
	movl	%r8d, _ZZ8pgmModelR5MixerE7pgm_ptr(%rip)
	movl	%eax, _ZZ8pgmModelR5MixerE7pgm_hdr(,%rcx,4)
	incl	%r13d
	movl	%r13d, _ZZ8pgmModelR5MixerE3pgm(%rip)
	movl	pos(%rip), %r9d
.LBB35_27:                              # %.critedge2
                                        #   in Loop: Header=BB35_11 Depth=1
	movl	%r13d, %edi
.LBB35_28:                              # %.critedge2
                                        #   in Loop: Header=BB35_11 Depth=1
	incl	%edi
	leal	-1(%r9), %eax
	cmpl	%eax, %edi
	jge	.LBB35_31
# BB#29:                                # %.critedge2
                                        #   in Loop: Header=BB35_11 Depth=1
	cmpl	$2, %r8d
	jg	.LBB35_31
# BB#30:                                # %.critedge2..preheader54_crit_edge
                                        #   in Loop: Header=BB35_11 Depth=1
	movl	buf(%rip), %edx
	movq	buf+16(%rip), %rsi
.LBB35_11:                              # %.preheader54
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_12 Depth 2
                                        #     Child Loop BB35_23 Depth 2
                                        #     Child Loop BB35_16 Depth 2
	decl	%edx
	leal	-1(%r9), %ecx
	leal	1(%rdi), %ebx
	jmp	.LBB35_12
	.p2align	4, 0x90
.LBB35_22:                              #   in Loop: Header=BB35_12 Depth=2
	incl	%edi
	incl	%ebx
.LBB35_12:                              #   Parent Loop BB35_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %eax
	andl	%edi, %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	cmpq	$35, %rax
	ja	.LBB35_15
# BB#13:                                #   in Loop: Header=BB35_12 Depth=2
	btq	%rax, %r15
	jae	.LBB35_14
# BB#21:                                # %.thread
                                        #   in Loop: Header=BB35_12 Depth=2
	cmpl	%ecx, %edi
	jl	.LBB35_22
	jmp	.LBB35_31
.LBB35_14:                              #   in Loop: Header=BB35_11 Depth=1
	cmpq	$35, %rax
	jne	.LBB35_15
	.p2align	4, 0x90
.LBB35_23:                              # %.preheader
                                        #   Parent Loop BB35_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %edi
	movl	%edx, %eax
	andl	%edi, %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	cmpb	$10, %al
	je	.LBB35_28
# BB#24:                                # %.preheader
                                        #   in Loop: Header=BB35_23 Depth=2
	cmpb	$13, %al
	je	.LBB35_28
# BB#25:                                #   in Loop: Header=BB35_23 Depth=2
	leal	1(%rdi), %ebx
	cmpl	%ecx, %edi
	jl	.LBB35_23
	jmp	.LBB35_28
.LBB35_15:                              # %.preheader52.preheader
                                        #   in Loop: Header=BB35_11 Depth=1
	movl	$1, %eax
	.p2align	4, 0x90
.LBB35_16:                              # %.preheader52
                                        #   Parent Loop BB35_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %r13d
	movl	%edx, %edi
	andl	%r13d, %edi
	movslq	%edi, %rdi
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, -1(%rsp,%rax)
	leal	1(%r13), %edi
	movl	%edx, %ebx
	andl	%edi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rsi,%rbx), %ebx
	cmpq	$32, %rbx
	ja	.LBB35_18
# BB#17:                                # %.preheader52
                                        #   in Loop: Header=BB35_16 Depth=2
	btq	%rbx, %r15
	jb	.LBB35_26
.LBB35_18:                              #   in Loop: Header=BB35_16 Depth=2
	cmpq	$31, %rax
	jg	.LBB35_20
# BB#19:                                #   in Loop: Header=BB35_16 Depth=2
	incq	%rax
	cmpl	%ecx, %edi
	jl	.LBB35_16
	jmp	.LBB35_20
.LBB35_31:                              # %.loopexit
	cmpl	$3, %r8d
	jne	.LBB35_36
# BB#32:
	cmpl	$255, _ZZ8pgmModelR5MixerE7pgm_hdr+8(%rip)
	jne	.LBB35_36
# BB#33:
	movl	_ZZ8pgmModelR5MixerE7pgm_hdr(%rip), %esi
	testl	%esi, %esi
	jle	.LBB35_36
# BB#34:
	movl	_ZZ8pgmModelR5MixerE7pgm_hdr+4(%rip), %edx
	testl	%edx, %edx
	jle	.LBB35_36
# BB#35:
	movl	%esi, _ZZ8pgmModelR5MixerE1w(%rip)
	movl	%edx, _ZZ8pgmModelR5MixerE1h(%rip)
	movl	%edx, %eax
	imull	%esi, %eax
	addl	%eax, %r9d
	movl	%r9d, _ZZ8pgmModelR5MixerE3eoi(%rip)
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB35_36
.Lfunc_end35:
	.size	_Z8pgmModelR5Mixer, .Lfunc_end35-_Z8pgmModelR5Mixer
	.cfi_endproc

	.globl	_Z9bmpModel8R5Mixer
	.p2align	4, 0x90
	.type	_Z9bmpModel8R5Mixer,@function
_Z9bmpModel8R5Mixer:                    # @_Z9bmpModel8R5Mixer
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi159:
	.cfi_def_cfa_offset 32
.Lcfi160:
	.cfi_offset %rbx, -24
.Lcfi161:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, bpos(%rip)
	jne	.LBB36_8
# BB#1:
	movl	pos(%rip), %eax
	leal	-44(%rax), %esi
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %esi
	movq	buf+16(%rip), %rdx
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-43(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	leal	-42(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$16, %esi
	orl	%edi, %esi
	leal	-41(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r9d
	shll	$24, %r9d
	orl	%esi, %r9d
	cmpl	$1078, %r9d             # imm = 0x436
	ja	.LBB36_6
# BB#2:
	leal	-40(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %r8d
	leal	-39(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$8, %esi
	orl	%r8d, %esi
	leal	-38(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$16, %edi
	orl	%esi, %edi
	leal	-37(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$24, %esi
	orl	%edi, %esi
	cmpl	$40, %esi
	jne	.LBB36_6
# BB#3:
	leal	-24(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-23(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	leal	-22(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$16, %esi
	orl	%edi, %esi
	leal	-21(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$24, %edi
	orl	%esi, %edi
	jne	.LBB36_6
# BB#4:
	leal	-26(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	cmpb	$8, (%rdx,%rsi)
	jne	.LBB36_6
# BB#5:
	leal	-36(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-35(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	leal	-34(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$16, %esi
	orl	%edi, %esi
	leal	-33(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$24, %edi
	orl	%esi, %edi
	movl	%edi, _ZZ9bmpModel8R5MixerE2w1(%rip)
	leal	-32(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-31(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	leal	-30(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$16, %esi
	orl	%edi, %esi
	leal	-29(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rcx
	movzbl	(%rdx,%rcx), %ecx
	shll	$24, %ecx
	orl	%esi, %ecx
	movl	%ecx, _ZZ9bmpModel8R5MixerE1h(%rip)
	leal	-54(%rax,%r9), %ecx
	movl	%ecx, _ZZ9bmpModel8R5MixerE4ibmp(%rip)
.LBB36_6:
	cmpl	%eax, _ZZ9bmpModel8R5MixerE4ibmp(%rip)
	jne	.LBB36_8
# BB#7:
	movl	_ZZ9bmpModel8R5MixerE2w1(%rip), %esi
	movl	%esi, _ZZ9bmpModel8R5MixerE1w(%rip)
	movl	_ZZ9bmpModel8R5MixerE1h(%rip), %edx
	movl	%edx, %ecx
	imull	%esi, %ecx
	addl	%eax, %ecx
	movl	%ecx, _ZZ9bmpModel8R5MixerE3eoi(%rip)
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$0, _ZZ9bmpModel8R5MixerE4ibmp(%rip)
.LBB36_8:
	movl	pos(%rip), %eax
	cmpl	_ZZ9bmpModel8R5MixerE3eoi(%rip), %eax
	jle	.LBB36_10
# BB#9:
	movl	$0, _ZZ9bmpModel8R5MixerE1w(%rip)
	xorl	%eax, %eax
	jmp	.LBB36_11
.LBB36_10:
	movl	_ZZ9bmpModel8R5MixerE1w(%rip), %esi
	movq	%r14, %rdi
	callq	_Z9model8bitR5Mixeri
	movl	_ZZ9bmpModel8R5MixerE3col(%rip), %eax
	leal	1(%rax), %ecx
	xorl	%edx, %edx
	cmpl	$6, %eax
	cmovlel	%ecx, %edx
	movl	%edx, _ZZ9bmpModel8R5MixerE3col(%rip)
	movl	92(%r14), %eax
	addl	$2, %eax
	movslq	88(%r14), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, 88(%r14)
	movq	80(%r14), %rcx
	movl	%eax, (%rcx,%rsi,4)
	movl	92(%r14), %eax
	leal	8(%rax), %esi
	movl	%esi, 92(%r14)
	leal	8(%rdx,%rax), %eax
	movslq	88(%r14), %rdx
	leal	1(%rdx), %esi
	movl	%esi, 88(%r14)
	movl	%eax, (%rcx,%rdx,4)
	movl	92(%r14), %r8d
	leal	8(%r8), %eax
	movl	%eax, 92(%r14)
	movl	_ZZ9bmpModel8R5MixerE1w(%rip), %eax
	movl	pos(%rip), %esi
	movl	%esi, %edi
	subl	%eax, %edi
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %edi
	movq	buf+16(%rip), %rbx
	movslq	%edi, %rdi
	movzbl	(%rbx,%rdi), %edi
	decl	%esi
	andl	%edx, %esi
	movslq	%esi, %rdx
	movzbl	(%rbx,%rdx), %edx
	addl	%edi, %edx
	shrl	$4, %edx
	leal	8(%rdx,%r8), %edx
	movslq	88(%r14), %rsi
	leal	1(%rsi), %edi
	movl	%edi, 88(%r14)
	movl	%edx, (%rcx,%rsi,4)
	movl	92(%r14), %edx
	addl	$32, %edx
	movl	%edx, 92(%r14)
	addl	c0(%rip), %edx
	movslq	88(%r14), %rsi
	leal	1(%rsi), %edi
	movl	%edi, 88(%r14)
	movl	%edx, (%rcx,%rsi,4)
	addl	$256, 92(%r14)          # imm = 0x100
.LBB36_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end36:
	.size	_Z9bmpModel8R5Mixer, .Lfunc_end36-_Z9bmpModel8R5Mixer
	.cfi_endproc

	.globl	_Z9rgbModel8R5Mixer
	.p2align	4, 0x90
	.type	_Z9rgbModel8R5Mixer,@function
_Z9rgbModel8R5Mixer:                    # @_Z9rgbModel8R5Mixer
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 16
.Lcfi163:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpl	$0, bpos(%rip)
	jne	.LBB37_6
# BB#1:
	movl	pos(%rip), %eax
	leal	-507(%rax), %esi
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %esi
	movq	buf+16(%rip), %rdx
	movslq	%esi, %rsi
	cmpb	$1, (%rdx,%rsi)
	jne	.LBB37_6
# BB#2:
	leal	-506(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	cmpb	$-38, (%rdx,%rsi)
	jne	.LBB37_6
# BB#3:
	leal	-505(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	cmpb	$0, (%rdx,%rsi)
	jne	.LBB37_6
# BB#4:
	leal	-496(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-495(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	cmpl	$1, %edi
	jne	.LBB37_6
# BB#5:
	leal	-501(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %edi
	shll	$8, %edi
	leal	-500(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	orl	%edi, %esi
	movl	%esi, _ZZ9rgbModel8R5MixerE1w(%rip)
	leal	-499(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r8d
	shll	$8, %r8d
	leal	-498(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rcx
	movzbl	(%rdx,%rcx), %edx
	orl	%r8d, %edx
	movl	%edx, %ecx
	imull	%esi, %ecx
	addl	%eax, %ecx
	movl	%ecx, _ZZ9rgbModel8R5MixerE3eoi(%rip)
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
.LBB37_6:
	movl	pos(%rip), %eax
	cmpl	_ZZ9rgbModel8R5MixerE3eoi(%rip), %eax
	jle	.LBB37_8
# BB#7:
	movl	$0, _ZZ9rgbModel8R5MixerE1w(%rip)
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB37_8:
	movl	_ZZ9rgbModel8R5MixerE1w(%rip), %esi
	movq	%rbx, %rdi
	callq	_Z9model8bitR5Mixeri
	movl	_ZZ9rgbModel8R5MixerE3col(%rip), %eax
	leal	1(%rax), %ecx
	xorl	%edx, %edx
	cmpl	$6, %eax
	cmovlel	%ecx, %edx
	movl	%edx, _ZZ9rgbModel8R5MixerE3col(%rip)
	movl	92(%rbx), %eax
	addl	$2, %eax
	movslq	88(%rbx), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, 88(%rbx)
	movq	80(%rbx), %r9
	movl	%eax, (%r9,%rsi,4)
	movl	92(%rbx), %eax
	leal	8(%rax), %esi
	movl	%esi, 92(%rbx)
	leal	8(%rdx,%rax), %eax
	movslq	88(%rbx), %rdx
	leal	1(%rdx), %esi
	movl	%esi, 88(%rbx)
	movl	%eax, (%r9,%rdx,4)
	movl	92(%rbx), %r8d
	leal	8(%r8), %eax
	movl	%eax, 92(%rbx)
	movl	_ZZ9rgbModel8R5MixerE1w(%rip), %eax
	movl	pos(%rip), %esi
	movl	%esi, %edi
	subl	%eax, %edi
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %edi
	movq	buf+16(%rip), %rcx
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	decl	%esi
	andl	%edx, %esi
	movslq	%esi, %rdx
	movzbl	(%rcx,%rdx), %ecx
	addl	%edi, %ecx
	shrl	$4, %ecx
	leal	8(%rcx,%r8), %ecx
	movslq	88(%rbx), %rdx
	leal	1(%rdx), %esi
	movl	%esi, 88(%rbx)
	movl	%ecx, (%r9,%rdx,4)
	movl	92(%rbx), %ecx
	addl	$32, %ecx
	movl	%ecx, 92(%rbx)
	addl	c0(%rip), %ecx
	movslq	88(%rbx), %rdx
	leal	1(%rdx), %esi
	movl	%esi, 88(%rbx)
	movl	%ecx, (%r9,%rdx,4)
	addl	$256, 92(%rbx)          # imm = 0x100
	popq	%rbx
	retq
.Lfunc_end37:
	.size	_Z9rgbModel8R5Mixer, .Lfunc_end37-_Z9rgbModel8R5Mixer
	.cfi_endproc

	.globl	_Z4dumpPKci
	.p2align	4, 0x90
	.type	_Z4dumpPKci,@function
_Z4dumpPKci:                            # @_Z4dumpPKci
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 32
.Lcfi167:
	.cfi_offset %rbx, -32
.Lcfi168:
	.cfi_offset %r14, -24
.Lcfi169:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rcx
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	leal	2(%r14), %eax
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %eax
	movq	buf+16(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %eax
	shll	$8, %eax
	leal	3(%r14), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %ebp
	orl	%eax, %ebp
	andl	%r14d, %ecx
	movslq	%ecx, %rax
	movzbl	(%rdx,%rax), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB38_1:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	buf(%rip), %eax
	movq	buf+16(%rip), %rcx
	leal	2(%r14,%rbx), %edx
	decl	%eax
	andl	%edx, %eax
	cltq
	movzbl	(%rcx,%rax), %esi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	incl	%ebx
	cmpl	%ebp, %ebx
	jl	.LBB38_1
# BB#2:
	movl	$10, %edi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	putchar                 # TAILCALL
.Lfunc_end38:
	.size	_Z4dumpPKci, .Lfunc_end38-_Z4dumpPKci
	.cfi_endproc

	.globl	_Z8update_kiiRiS_
	.p2align	4, 0x90
	.type	_Z8update_kiiRiS_,@function
_Z8update_kiiRiS_:                      # @_Z8update_kiiRiS_
	.cfi_startproc
# BB#0:
	movl	(%rdx), %r8d
	leal	-1(%r8), %r9d
	imull	%edi, %r9d
	movl	$9, %eax
	subl	%r8d, %eax
	imull	%esi, %eax
	addl	%r9d, %eax
	movl	%eax, %r9d
	negl	%r9d
	cmovll	%eax, %r9d
	movl	%r8d, %r10d
	imull	%edi, %r10d
	movl	$8, %eax
	subl	%r8d, %eax
	imull	%esi, %eax
	addl	%r10d, %eax
	movl	%eax, %r10d
	negl	%r10d
	cmovll	%eax, %r10d
	movl	%r10d, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%r10d, %eax
	sarl	$3, %eax
	leal	1(%r8), %r10d
	imull	%edi, %r10d
	movl	$7, %edi
	subl	%r8d, %edi
	imull	%esi, %edi
	addl	%r10d, %edi
	movl	%edi, %esi
	negl	%esi
	cmovll	%edi, %esi
	movl	%esi, %edi
	sarl	$31, %edi
	shrl	$29, %edi
	addl	%esi, %edi
	sarl	$3, %edi
	testl	%r8d, %r8d
	je	.LBB39_1
# BB#2:
	movl	%r9d, %esi
	sarl	$31, %esi
	shrl	$29, %esi
	addl	%r9d, %esi
	sarl	$3, %esi
	cmpl	$8, %r8d
	movl	%eax, %r8d
	je	.LBB39_4
# BB#3:                                 # %.fold.split
	movl	%edi, %r8d
.LBB39_4:
	cmpl	%eax, %esi
	jge	.LBB39_7
# BB#5:
	cmpl	%r8d, %esi
	jge	.LBB39_7
# BB#6:
	decl	(%rcx)
	jmp	.LBB39_7
.LBB39_1:
	movl	%edi, %r8d
	movl	%eax, %esi
.LBB39_7:                               # %.thread
	movl	(%rcx), %edi
	cmpl	%eax, %r8d
	jge	.LBB39_10
# BB#8:                                 # %.thread
	cmpl	%esi, %r8d
	jge	.LBB39_10
# BB#9:
	incl	%edi
	movl	%edi, (%rcx)
.LBB39_10:                              # %thread-pre-split
	movl	$-1, %eax
	cmpl	$-2, %edi
	jl	.LBB39_12
# BB#11:
	movl	$1, %eax
	cmpl	$3, %edi
	jl	.LBB39_13
.LBB39_12:                              # %.sink.split
	addl	%eax, (%rdx)
	movl	$0, (%rcx)
.LBB39_13:
	retq
.Lfunc_end39:
	.size	_Z8update_kiiRiS_, .Lfunc_end39-_Z8update_kiiRiS_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI40_0:
	.long	256                     # 0x100
	.long	0                       # 0x0
	.long	256                     # 0x100
	.long	256                     # 0x100
.LCPI40_1:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
.LCPI40_2:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI40_3:
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
.LCPI40_4:
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.text
	.globl	_Z9jpegModelR5Mixer
	.p2align	4, 0x90
	.type	_Z9jpegModelR5Mixer,@function
_Z9jpegModelR5Mixer:                    # @_Z9jpegModelR5Mixer
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%rbp
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi173:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi174:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi176:
	.cfi_def_cfa_offset 112
.Lcfi177:
	.cfi_offset %rbx, -56
.Lcfi178:
	.cfi_offset %r12, -48
.Lcfi179:
	.cfi_offset %r13, -40
.Lcfi180:
	.cfi_offset %r14, -32
.Lcfi181:
	.cfi_offset %r15, -24
.Lcfi182:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movb	_ZGVZ9jpegModelR5MixerE2ht(%rip), %al
	testb	%al, %al
	jne	.LBB40_6
# BB#1:
	movl	$_ZGVZ9jpegModelR5MixerE2ht, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_6
# BB#2:
	movabsq	$34359738376, %rax      # imm = 0x800000008
	movq	%rax, _ZZ9jpegModelR5MixerE2ht(%rip)
	movl	programChecker(%rip), %eax
	addl	$32, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_4
# BB#3:
	movl	%eax, programChecker+4(%rip)
.LBB40_4:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movl	$32, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE2ht+8(%rip)
	testq	%rax, %rax
	je	.LBB40_426
# BB#5:
	movq	%rax, _ZZ9jpegModelR5MixerE2ht+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE2ht, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE2ht, %edi
	callq	__cxa_guard_release
.LBB40_6:
	movb	_ZGVZ9jpegModelR5MixerE3huf(%rip), %al
	testb	%al, %al
	jne	.LBB40_12
# BB#7:
	movl	$_ZGVZ9jpegModelR5MixerE3huf, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_12
# BB#8:
	movabsq	$549755814016, %rax     # imm = 0x8000000080
	movq	%rax, _ZZ9jpegModelR5MixerE3huf(%rip)
	movl	$1536, %eax             # imm = 0x600
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_10
# BB#9:
	movl	%eax, programChecker+4(%rip)
.LBB40_10:                              # %_ZN14ProgramChecker5allocEi.exit.i.i724
	movl	$1536, %edi             # imm = 0x600
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE3huf+8(%rip)
	testq	%rax, %rax
	je	.LBB40_428
# BB#11:
	movq	%rax, _ZZ9jpegModelR5MixerE3huf+16(%rip)
	movl	$_ZN5ArrayI3HUFLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE3huf, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE3huf, %edi
	callq	__cxa_guard_release
.LBB40_12:
	movb	_ZGVZ9jpegModelR5MixerE4hbuf(%rip), %al
	testb	%al, %al
	jne	.LBB40_18
# BB#13:
	movl	$_ZGVZ9jpegModelR5MixerE4hbuf, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_18
# BB#14:
	movabsq	$8796093024256, %rax    # imm = 0x80000000800
	movq	%rax, _ZZ9jpegModelR5MixerE4hbuf(%rip)
	movl	$2048, %eax             # imm = 0x800
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_16
# BB#15:
	movl	%eax, programChecker+4(%rip)
.LBB40_16:                              # %_ZN14ProgramChecker5allocEi.exit.i.i726
	movl	$2048, %edi             # imm = 0x800
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE4hbuf+8(%rip)
	testq	%rax, %rax
	je	.LBB40_430
# BB#17:
	movq	%rax, _ZZ9jpegModelR5MixerE4hbuf+16(%rip)
	movl	$_ZN5ArrayIhLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE4hbuf, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE4hbuf, %edi
	callq	__cxa_guard_release
.LBB40_18:
	movb	_ZGVZ9jpegModelR5MixerE5color(%rip), %al
	testb	%al, %al
	jne	.LBB40_24
# BB#19:
	movl	$_ZGVZ9jpegModelR5MixerE5color, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_24
# BB#20:
	movabsq	$42949672970, %rax      # imm = 0xA0000000A
	movq	%rax, _ZZ9jpegModelR5MixerE5color(%rip)
	movl	programChecker(%rip), %eax
	addl	$40, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_22
# BB#21:
	movl	%eax, programChecker+4(%rip)
.LBB40_22:                              # %_ZN14ProgramChecker5allocEi.exit.i.i728
	movl	$40, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE5color+8(%rip)
	testq	%rax, %rax
	je	.LBB40_432
# BB#23:
	movq	%rax, _ZZ9jpegModelR5MixerE5color+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE5color, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE5color, %edi
	callq	__cxa_guard_release
.LBB40_24:
	movb	_ZGVZ9jpegModelR5MixerE4pred(%rip), %al
	testb	%al, %al
	jne	.LBB40_30
# BB#25:
	movl	$_ZGVZ9jpegModelR5MixerE4pred, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_30
# BB#26:
	movabsq	$17179869188, %rax      # imm = 0x400000004
	movq	%rax, _ZZ9jpegModelR5MixerE4pred(%rip)
	movl	programChecker(%rip), %eax
	addl	$16, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_28
# BB#27:
	movl	%eax, programChecker+4(%rip)
.LBB40_28:                              # %_ZN14ProgramChecker5allocEi.exit.i.i731
	movl	$16, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE4pred+8(%rip)
	testq	%rax, %rax
	je	.LBB40_434
# BB#29:
	movq	%rax, _ZZ9jpegModelR5MixerE4pred+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE4pred, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE4pred, %edi
	callq	__cxa_guard_release
.LBB40_30:
	movb	_ZGVZ9jpegModelR5MixerE4cbuf(%rip), %al
	testb	%al, %al
	jne	.LBB40_36
# BB#31:
	movl	$_ZGVZ9jpegModelR5MixerE4cbuf, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_36
# BB#32:
	movabsq	$562949953552384, %rax  # imm = 0x2000000020000
	movq	%rax, _ZZ9jpegModelR5MixerE4cbuf(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_34
# BB#33:
	movl	%eax, programChecker+4(%rip)
.LBB40_34:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE4cbuf+8(%rip)
	testq	%rax, %rax
	je	.LBB40_436
# BB#35:
	movq	%rax, _ZZ9jpegModelR5MixerE4cbuf+16(%rip)
	movl	$_ZN3BufD2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE4cbuf, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE4cbuf, %edi
	callq	__cxa_guard_release
.LBB40_36:
	movb	_ZGVZ9jpegModelR5MixerE5cbuf2(%rip), %al
	testb	%al, %al
	jne	.LBB40_42
# BB#37:
	movl	$_ZGVZ9jpegModelR5MixerE5cbuf2, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_42
# BB#38:
	movabsq	$562949953552384, %rax  # imm = 0x2000000020000
	movq	%rax, _ZZ9jpegModelR5MixerE5cbuf2(%rip)
	movl	$524288, %eax           # imm = 0x80000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_40
# BB#39:
	movl	%eax, programChecker+4(%rip)
.LBB40_40:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i735
	movl	$524288, %edi           # imm = 0x80000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE5cbuf2+8(%rip)
	testq	%rax, %rax
	je	.LBB40_438
# BB#41:
	movq	%rax, _ZZ9jpegModelR5MixerE5cbuf2+16(%rip)
	movl	$_ZN6IntBufD2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE5cbuf2, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE5cbuf2, %edi
	callq	__cxa_guard_release
.LBB40_42:
	movb	_ZGVZ9jpegModelR5MixerE8adv_pred(%rip), %al
	testb	%al, %al
	jne	.LBB40_48
# BB#43:
	movl	$_ZGVZ9jpegModelR5MixerE8adv_pred, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_48
# BB#44:
	movabsq	$30064771079, %rax      # imm = 0x700000007
	movq	%rax, _ZZ9jpegModelR5MixerE8adv_pred(%rip)
	movl	programChecker(%rip), %eax
	addl	$28, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_46
# BB#45:
	movl	%eax, programChecker+4(%rip)
.LBB40_46:                              # %_ZN14ProgramChecker5allocEi.exit.i.i737
	movl	$28, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE8adv_pred+8(%rip)
	testq	%rax, %rax
	je	.LBB40_440
# BB#47:
	movq	%rax, _ZZ9jpegModelR5MixerE8adv_pred+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE8adv_pred, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE8adv_pred, %edi
	callq	__cxa_guard_release
.LBB40_48:
	movb	_ZGVZ9jpegModelR5MixerE4sumu(%rip), %al
	testb	%al, %al
	jne	.LBB40_54
# BB#49:
	movl	$_ZGVZ9jpegModelR5MixerE4sumu, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_54
# BB#50:
	movabsq	$34359738376, %rax      # imm = 0x800000008
	movq	%rax, _ZZ9jpegModelR5MixerE4sumu(%rip)
	movl	programChecker(%rip), %eax
	addl	$32, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_52
# BB#51:
	movl	%eax, programChecker+4(%rip)
.LBB40_52:                              # %_ZN14ProgramChecker5allocEi.exit.i.i740
	movl	$32, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE4sumu+8(%rip)
	testq	%rax, %rax
	je	.LBB40_442
# BB#53:
	movq	%rax, _ZZ9jpegModelR5MixerE4sumu+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE4sumu, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE4sumu, %edi
	callq	__cxa_guard_release
.LBB40_54:
	movb	_ZGVZ9jpegModelR5MixerE4sumv(%rip), %al
	testb	%al, %al
	jne	.LBB40_60
# BB#55:
	movl	$_ZGVZ9jpegModelR5MixerE4sumv, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_60
# BB#56:
	movabsq	$34359738376, %rax      # imm = 0x800000008
	movq	%rax, _ZZ9jpegModelR5MixerE4sumv(%rip)
	movl	programChecker(%rip), %eax
	addl	$32, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_58
# BB#57:
	movl	%eax, programChecker+4(%rip)
.LBB40_58:                              # %_ZN14ProgramChecker5allocEi.exit.i.i743
	movl	$32, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE4sumv+8(%rip)
	testq	%rax, %rax
	je	.LBB40_444
# BB#59:
	movq	%rax, _ZZ9jpegModelR5MixerE4sumv+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE4sumv, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE4sumv, %edi
	callq	__cxa_guard_release
.LBB40_60:
	movb	_ZGVZ9jpegModelR5MixerE2ls(%rip), %al
	testb	%al, %al
	jne	.LBB40_66
# BB#61:
	movl	$_ZGVZ9jpegModelR5MixerE2ls, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_66
# BB#62:
	movabsq	$42949672970, %rax      # imm = 0xA0000000A
	movq	%rax, _ZZ9jpegModelR5MixerE2ls(%rip)
	movl	programChecker(%rip), %eax
	addl	$40, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_64
# BB#63:
	movl	%eax, programChecker+4(%rip)
.LBB40_64:                              # %_ZN14ProgramChecker5allocEi.exit.i.i746
	movl	$40, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE2ls+8(%rip)
	testq	%rax, %rax
	je	.LBB40_446
# BB#65:
	movq	%rax, _ZZ9jpegModelR5MixerE2ls+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE2ls, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE2ls, %edi
	callq	__cxa_guard_release
.LBB40_66:
	movb	_ZGVZ9jpegModelR5MixerE3lcp(%rip), %al
	testb	%al, %al
	jne	.LBB40_72
# BB#67:
	movl	$_ZGVZ9jpegModelR5MixerE3lcp, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_72
# BB#68:
	movabsq	$17179869188, %rax      # imm = 0x400000004
	movq	%rax, _ZZ9jpegModelR5MixerE3lcp(%rip)
	movl	programChecker(%rip), %eax
	addl	$16, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_70
# BB#69:
	movl	%eax, programChecker+4(%rip)
.LBB40_70:                              # %_ZN14ProgramChecker5allocEi.exit.i.i749
	movl	$16, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE3lcp+8(%rip)
	testq	%rax, %rax
	je	.LBB40_448
# BB#71:
	movq	%rax, _ZZ9jpegModelR5MixerE3lcp+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE3lcp, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE3lcp, %edi
	callq	__cxa_guard_release
.LBB40_72:
	movb	_ZGVZ9jpegModelR5MixerE4zpos(%rip), %al
	testb	%al, %al
	jne	.LBB40_78
# BB#73:
	movl	$_ZGVZ9jpegModelR5MixerE4zpos, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_78
# BB#74:
	movabsq	$274877907008, %rax     # imm = 0x4000000040
	movq	%rax, _ZZ9jpegModelR5MixerE4zpos(%rip)
	movl	$256, %eax              # imm = 0x100
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_76
# BB#75:
	movl	%eax, programChecker+4(%rip)
.LBB40_76:                              # %_ZN14ProgramChecker5allocEi.exit.i.i752
	movl	$256, %edi              # imm = 0x100
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE4zpos+8(%rip)
	testq	%rax, %rax
	je	.LBB40_450
# BB#77:
	movq	%rax, _ZZ9jpegModelR5MixerE4zpos+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE4zpos, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE4zpos, %edi
	callq	__cxa_guard_release
.LBB40_78:
	movb	_ZGVZ9jpegModelR5MixerE4qtab(%rip), %al
	testb	%al, %al
	jne	.LBB40_84
# BB#79:
	movl	$_ZGVZ9jpegModelR5MixerE4qtab, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_84
# BB#80:
	movabsq	$1099511628032, %rax    # imm = 0x10000000100
	movq	%rax, _ZZ9jpegModelR5MixerE4qtab(%rip)
	movl	$256, %eax              # imm = 0x100
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_82
# BB#81:
	movl	%eax, programChecker+4(%rip)
.LBB40_82:                              # %_ZN14ProgramChecker5allocEi.exit.i.i755
	movl	$256, %edi              # imm = 0x100
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE4qtab+8(%rip)
	testq	%rax, %rax
	je	.LBB40_452
# BB#83:
	movq	%rax, _ZZ9jpegModelR5MixerE4qtab+16(%rip)
	movl	$_ZN5ArrayIhLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE4qtab, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE4qtab, %edi
	callq	__cxa_guard_release
.LBB40_84:
	movb	_ZGVZ9jpegModelR5MixerE4qmap(%rip), %al
	testb	%al, %al
	jne	.LBB40_90
# BB#85:
	movl	$_ZGVZ9jpegModelR5MixerE4qmap, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_90
# BB#86:
	movabsq	$42949672970, %rax      # imm = 0xA0000000A
	movq	%rax, _ZZ9jpegModelR5MixerE4qmap(%rip)
	movl	programChecker(%rip), %eax
	addl	$40, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_88
# BB#87:
	movl	%eax, programChecker+4(%rip)
.LBB40_88:                              # %_ZN14ProgramChecker5allocEi.exit.i.i758
	movl	$40, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE4qmap+8(%rip)
	testq	%rax, %rax
	je	.LBB40_454
# BB#89:
	movq	%rax, _ZZ9jpegModelR5MixerE4qmap+16(%rip)
	movl	$_ZN5ArrayIiLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE4qmap, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE4qmap, %edi
	callq	__cxa_guard_release
.LBB40_90:
	movl	bpos(%rip), %ecx
	testl	%ecx, %ecx
	movl	_ZZ9jpegModelR5MixerE4jpeg(%rip), %eax
	je	.LBB40_95
# BB#91:
	je	.LBB40_93
# BB#92:
	testl	%eax, %eax
	je	.LBB40_212
.LBB40_93:                              # %.thread1029
	movl	_ZZ9jpegModelR5MixerE3app(%rip), %ebx
	jmp	.LBB40_94
.LBB40_95:
	xorl	%edx, %edx
	cmpl	$1, %eax
	setg	%dl
	movl	%edx, _ZZ9jpegModelR5MixerE9next_jpeg(%rip)
	movl	_ZZ9jpegModelR5MixerE3app(%rip), %ebx
	testl	%ecx, %ecx
	jne	.LBB40_94
# BB#96:
	testl	%ebx, %ebx
	jle	.LBB40_94
# BB#97:
	decl	%ebx
	movl	%ebx, _ZZ9jpegModelR5MixerE3app(%rip)
	testl	%ebx, %ebx
	jg	.LBB40_212
	jmp	.LBB40_98
.LBB40_94:
	testl	%ebx, %ebx
	jg	.LBB40_212
.LBB40_98:
	testl	%ecx, %ecx
	je	.LBB40_151
# BB#99:                                # %._crit_edge996
	movl	_ZZ9jpegModelR5MixerE4data(%rip), %ebp
.LBB40_100:
	cmpl	%ebp, pos(%rip)
	jne	.LBB40_195
.LBB40_101:
	cmpl	$1, bpos(%rip)
	jne	.LBB40_195
# BB#102:
	movslq	_ZZ9jpegModelR5MixerE6htsize(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB40_211
# BB#103:                               # %.lr.ph922
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	buf(%rip), %ecx
	movq	buf+16(%rip), %r12
	xorl	%r9d, %r9d
	movl	%ecx, %r11d
.LBB40_104:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_108 Depth 2
                                        #       Child Loop BB40_115 Depth 3
                                        #       Child Loop BB40_117 Depth 3
	movq	_ZZ9jpegModelR5MixerE2ht+16(%rip), %rax
	movl	(%rax,%r9,4), %eax
	leal	4(%rax), %r8d
	leal	2(%rax), %esi
	leal	-1(%r11), %edi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%r12,%rsi), %esi
	shll	$8, %esi
	leal	3(%rax), %ebx
	andl	%edi, %ebx
	movslq	%ebx, %rdi
	movzbl	(%r12,%rdi), %edi
	leal	2(%rax,%rsi), %r10d
	addl	%edi, %r10d
	addl	$2104, %eax             # imm = 0x838
	cmpl	%eax, %r10d
	jge	.LBB40_124
# BB#105:                               #   in Loop: Header=BB40_104 Depth=1
	cmpl	%r10d, %r8d
	jge	.LBB40_124
# BB#106:                               #   in Loop: Header=BB40_104 Depth=1
	cmpl	%ebp, %r10d
	jge	.LBB40_124
# BB#107:                               # %.lr.ph1134.preheader
                                        #   in Loop: Header=BB40_104 Depth=1
	xorl	%r15d, %r15d
	movl	%ecx, %esi
	.p2align	4, 0x90
.LBB40_108:                             # %.lr.ph1134
                                        #   Parent Loop BB40_104 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB40_115 Depth 3
                                        #       Child Loop BB40_117 Depth 3
	incl	%r15d
	cmpl	$9, %r15d
	jg	.LBB40_122
# BB#109:                               #   in Loop: Header=BB40_108 Depth=2
	leal	-1(%r11), %eax
	andl	%r8d, %eax
	cltq
	movzbl	(%r12,%rax), %ecx
	cmpl	$31, %ecx
	ja	.LBB40_122
# BB#110:                               #   in Loop: Header=BB40_108 Depth=2
	movl	%ecx, %eax
	andb	$15, %al
	cmpb	$3, %al
	ja	.LBB40_122
# BB#111:                               #   in Loop: Header=BB40_108 Depth=2
	cmpb	$31, %cl
	ja	.LBB40_211
# BB#112:                               #   in Loop: Header=BB40_108 Depth=2
	movl	%ecx, %eax
	andl	$15, %eax
	cmpb	$4, %al
	jae	.LBB40_211
# BB#113:                               #   in Loop: Header=BB40_108 Depth=2
	movl	%ecx, %edi
	shrl	$4, %edi
	movl	%edi, %ebp
	shll	$6, %ebp
	movl	%ecx, %ebx
	shlb	$4, %bl
	movzbl	%bl, %r13d
	addl	%ebp, %r13d
	movq	_ZZ9jpegModelR5MixerE3huf+16(%rip), %r11
	leal	17(%r8), %r14d
	shll	$10, %edi
	shll	$8, %eax
	leaq	(%rax,%rdi), %rbp
	decl	%esi
	andl	%r14d, %esi
	movslq	%esi, %rsi
	movb	(%r12,%rsi), %bl
	movq	_ZZ9jpegModelR5MixerE4hbuf+16(%rip), %rsi
	movb	%bl, (%rsi,%rbp)
	shll	$6, %ecx
	andl	$15360, %ecx            # imm = 0x3C00
	addl	%eax, %ecx
	movl	%r8d, %edi
	xorl	%ebx, %ebx
	jmp	.LBB40_115
	.p2align	4, 0x90
.LBB40_114:                             # %._crit_edge1001.1
                                        #   in Loop: Header=BB40_115 Depth=3
	movl	buf(%rip), %eax
	movq	buf+16(%rip), %rdx
	decl	%eax
	leal	19(%rdi,%rbx), %esi
	andl	%eax, %esi
	movslq	%esi, %rax
	movzbl	(%rdx,%rax), %eax
	movq	_ZZ9jpegModelR5MixerE4hbuf+16(%rip), %rdx
	addq	%rcx, %rdx
	movb	%al, 2(%rbx,%rdx)
	addq	$2, %rbx
.LBB40_115:                             # %._crit_edge1001
                                        #   Parent Loop BB40_104 Depth=1
                                        #     Parent Loop BB40_108 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	buf(%rip), %eax
	movq	buf+16(%rip), %rsi
	decl	%eax
	leal	18(%rdi,%rbx), %edx
	andl	%eax, %edx
	movslq	%edx, %rax
	movzbl	(%rsi,%rax), %eax
	movq	_ZZ9jpegModelR5MixerE4hbuf+16(%rip), %rdx
	addq	%rcx, %rdx
	movb	%al, 1(%rbx,%rdx)
	cmpq	$254, %rbx
	jne	.LBB40_114
# BB#116:                               # %.preheader861
                                        #   in Loop: Header=BB40_108 Depth=2
	leaq	(%r13,%r13,2), %rax
	leaq	(%r11,%rax,4), %rcx
	incl	%r8d
	movl	buf(%rip), %r11d
	movq	buf+16(%rip), %r12
	leal	-1(%r11), %r13d
	xorl	%esi, %esi
	movl	%r8d, %eax
	movl	$8, %ebx
	movl	%r14d, %r8d
	.p2align	4, 0x90
.LBB40_117:                             #   Parent Loop BB40_104 Depth=1
                                        #     Parent Loop BB40_108 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebp, %edx
	movl	%esi, -8(%rcx,%rbx)
	movl	%r13d, %ebp
	andl	%eax, %ebp
	movslq	%ebp, %rbp
	movzbl	(%r12,%rbp), %edi
	addl	%esi, %edi
	movl	%edi, -4(%rcx,%rbx)
	movl	%edx, (%rcx,%rbx)
	movzbl	(%r12,%rbp), %ebp
	addl	%ebp, %r8d
	addl	%edx, %ebp
	addl	%edi, %edi
	addq	$12, %rbx
	incl	%eax
	cmpq	$200, %rbx
	movl	%edi, %esi
	jne	.LBB40_117
# BB#118:                               #   in Loop: Header=BB40_108 Depth=2
	cmpl	$2047, %ebp             # imm = 0x7FF
	ja	.LBB40_211
# BB#119:                               # %..thread795_crit_edge
                                        #   in Loop: Header=BB40_108 Depth=2
	movl	pos(%rip), %ebp
	leal	2100(%r8), %eax
	cmpl	%eax, %r10d
	jge	.LBB40_123
# BB#120:                               # %..thread795_crit_edge
                                        #   in Loop: Header=BB40_108 Depth=2
	cmpl	%r10d, %r8d
	jge	.LBB40_123
# BB#121:                               # %..thread795_crit_edge
                                        #   in Loop: Header=BB40_108 Depth=2
	cmpl	%ebp, %r10d
	movl	%r11d, %esi
	movl	%r11d, %ecx
	jl	.LBB40_108
	jmp	.LBB40_124
.LBB40_122:                             #   in Loop: Header=BB40_104 Depth=1
	movl	%esi, %ecx
	cmpl	%r10d, %r8d
	je	.LBB40_125
	jmp	.LBB40_211
.LBB40_123:                             #   in Loop: Header=BB40_104 Depth=1
	movl	%r11d, %ecx
	.p2align	4, 0x90
.LBB40_124:                             # %.critedge
                                        #   in Loop: Header=BB40_104 Depth=1
	cmpl	%r10d, %r8d
	jne	.LBB40_211
.LBB40_125:                             #   in Loop: Header=BB40_104 Depth=1
	incq	%r9
	cmpq	8(%rsp), %r9            # 8-byte Folded Reload
	jl	.LBB40_104
# BB#126:                               # %._crit_edge923
	movl	$0, _ZZ9jpegModelR5MixerE8huffsize(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffbits(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffcode(%rip)
	movl	$-1, _ZZ9jpegModelR5MixerE2rs(%rip)
	movl	_ZZ9jpegModelR5MixerE3sof(%rip), %r13d
	movl	_ZZ9jpegModelR5MixerE3sos(%rip), %r9d
	testl	%r13d, %r13d
	jne	.LBB40_128
# BB#127:                               # %._crit_edge923
	testl	%r9d, %r9d
	jne	.LBB40_212
.LBB40_128:
	leal	4(%r9), %ecx
	movl	buf(%rip), %eax
	decl	%eax
	andl	%eax, %ecx
	movq	buf+16(%rip), %r14
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %r15d
	cmpl	$4, %r15d
	ja	.LBB40_211
# BB#129:
	leal	9(%r13), %ecx
	andl	%ecx, %eax
	cltq
	movb	(%r14,%rax), %bl
	cmpb	$5, %bl
	jae	.LBB40_211
# BB#130:
	movl	$0, _ZZ9jpegModelR5MixerE7mcusize(%rip)
	testb	%r15b, %r15b
	je	.LBB40_211
# BB#131:                               # %.preheader860.lr.ph
	movzbl	%bl, %ebp
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	movb	%bl, 32(%rsp)           # 1-byte Spill
	movl	%ebp, 48(%rsp)          # 4-byte Spill
.LBB40_132:                             # %.preheader860
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_134 Depth 2
                                        #       Child Loop BB40_138 Depth 3
	testb	%bl, %bl
	je	.LBB40_145
# BB#133:                               # %.lr.ph909
                                        #   in Loop: Header=BB40_132 Depth=1
	leal	5(%r9,%r11,2), %edx
	leal	6(%r9,%r11,2), %r8d
	xorl	%r10d, %r10d
	movl	%edx, 16(%rsp)          # 4-byte Spill
.LBB40_134:                             #   Parent Loop BB40_132 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB40_138 Depth 3
	movl	buf(%rip), %ecx
	decl	%ecx
	movl	%ecx, %eax
	andl	%edx, %eax
	cltq
	movb	(%r14,%rax), %dl
	leal	(%r10,%r10,2), %eax
	leal	10(%r13,%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	cmpb	(%r14,%rsi), %dl
	jne	.LBB40_144
# BB#135:                               #   in Loop: Header=BB40_134 Depth=2
	leal	(%r13,%rax), %edx
	addl	$11, %edx
	andl	%ecx, %edx
	movslq	%edx, %rcx
	movzbl	(%r14,%rcx), %ecx
	movl	%ecx, %edx
	shrl	$4, %edx
	cmpl	%edi, %edx
	cmovgel	%edx, %edi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	andl	$15, %ecx
	imull	%edx, %ecx
	testl	%ecx, %ecx
	je	.LBB40_211
# BB#136:                               #   in Loop: Header=BB40_134 Depth=2
	addl	%ecx, %r12d
	cmpl	$10, %r12d
	jg	.LBB40_211
# BB#137:                               # %.lr.ph906
                                        #   in Loop: Header=BB40_134 Depth=2
	leal	12(%rax,%r13), %r12d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %esi
	cltq
	shlq	$2, %rax
	xorl	%ebp, %ebp
.LBB40_138:                             #   Parent Loop BB40_132 Depth=1
                                        #     Parent Loop BB40_134 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rsi,%rbp), %edx
	cmpl	$10, %edx
	jge	.LBB40_211
# BB#139:                               #   in Loop: Header=BB40_138 Depth=3
	movl	buf(%rip), %edx
	decl	%edx
	andl	%r8d, %edx
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %edi
	movl	%edi, %edx
	shrl	$4, %edx
	movl	%edx, _ZZ9jpegModelR5MixerE6hufsel(%rax,%rbp,4)
	movl	%edi, %edx
	andb	$15, %dl
	movl	%edi, %ebx
	andl	$15, %ebx
	movl	%ebx, _ZZ9jpegModelR5MixerE6hufsel+40(%rax,%rbp,4)
	cmpb	$3, %dl
	ja	.LBB40_211
# BB#140:                               #   in Loop: Header=BB40_138 Depth=3
	cmpb	$64, %dil
	jae	.LBB40_211
# BB#141:                               #   in Loop: Header=BB40_138 Depth=3
	movq	_ZZ9jpegModelR5MixerE5color+16(%rip), %rdx
	addq	%rax, %rdx
	movl	%r11d, (%rdx,%rbp,4)
	movl	buf(%rip), %edx
	decl	%edx
	andl	%r12d, %edx
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %edx
	cmpl	$3, %edx
	ja	.LBB40_211
# BB#142:                               # %.thread799
                                        #   in Loop: Header=BB40_138 Depth=3
	movq	_ZZ9jpegModelR5MixerE4qmap+16(%rip), %rdi
	addq	%rax, %rdi
	movl	%edx, (%rdi,%rbp,4)
	leal	1(%rsi,%rbp), %edx
	movl	%edx, _ZZ9jpegModelR5MixerE7mcusize(%rip)
	incq	%rbp
	cmpl	%ebp, %ecx
	jne	.LBB40_138
# BB#143:                               # %.loopexit859.loopexit
                                        #   in Loop: Header=BB40_134 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	addl	%ebp, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%eax, %r12d
	movq	24(%rsp), %rdi          # 8-byte Reload
	movb	32(%rsp), %bl           # 1-byte Reload
	movl	48(%rsp), %ebp          # 4-byte Reload
.LBB40_144:                             # %.loopexit859
                                        #   in Loop: Header=BB40_134 Depth=2
	incl	%r10d
	cmpl	%ebp, %r10d
	movl	16(%rsp), %edx          # 4-byte Reload
	jl	.LBB40_134
.LBB40_145:                             # %._crit_edge910
                                        #   in Loop: Header=BB40_132 Depth=1
	incl	%r11d
	cmpl	%r15d, %r11d
	jl	.LBB40_132
# BB#146:                               # %._crit_edge914
	movq	%rdi, %r12
	leal	-1(%rdi), %eax
	cmpl	$9, %eax
	ja	.LBB40_211
# BB#147:                               # %.preheader857
	movq	8(%rsp), %rax           # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB40_422
# BB#148:                               # %.lr.ph904
	movq	_ZZ9jpegModelR5MixerE2ls+16(%rip), %r11
	cmpl	$1, %eax
	jne	.LBB40_409
# BB#149:                               # %.lr.ph904.split.preheader
	movq	8(%rsp), %rdx           # 8-byte Reload
	movslq	%edx, %rax
	shll	$6, %edx
	xorl	%ecx, %ecx
.LBB40_150:                             # %.lr.ph904.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%r11,%rcx,4)
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB40_150
	jmp	.LBB40_422
.LBB40_151:
	testl	%eax, %eax
	je	.LBB40_157
.LBB40_152:
	movl	_ZZ9jpegModelR5MixerE4data(%rip), %ebp
	testl	%eax, %eax
	je	.LBB40_155
# BB#153:
	testl	%ebp, %ebp
	je	.LBB40_155
# BB#154:
	movl	pos(%rip), %eax
	leal	-2(%rax), %esi
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %esi
	movq	buf+16(%rip), %rdx
	movslq	%esi, %rsi
	cmpb	$-1, (%rdx,%rsi)
	je	.LBB40_186
.LBB40_155:
	orl	%ebp, %ebx
	movl	pos(%rip), %eax
	je	.LBB40_162
# BB#156:                               # %._crit_edge987
	movl	buf(%rip), %r14d
	movq	buf+16(%rip), %r15
	jmp	.LBB40_166
.LBB40_157:
	movl	pos(%rip), %eax
	leal	-4(%rax), %esi
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %esi
	movq	buf+16(%rip), %rdx
	movslq	%esi, %rsi
	cmpb	$-1, (%rdx,%rsi)
	jne	.LBB40_212
# BB#158:
	leal	-3(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	cmpb	$-40, (%rdx,%rsi)
	jne	.LBB40_212
# BB#159:
	leal	-2(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	cmpb	$-1, (%rdx,%rsi)
	jne	.LBB40_212
# BB#160:
	decl	%eax
	andl	%eax, %ecx
	movslq	%ecx, %rax
	movb	(%rdx,%rax), %al
	andb	$-16, %al
	cmpb	$-32, %al
	jne	.LBB40_212
# BB#161:
	movl	$1, _ZZ9jpegModelR5MixerE4jpeg(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE7mcusize(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE4data(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE6htsize(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE3sof(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE3sos(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE3app(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE4cpos(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE6mcupos(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffsize(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffbits(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffcode(%rip)
	movl	$-1, _ZZ9jpegModelR5MixerE2rs(%rip)
	movq	_ZZ9jpegModelR5MixerE3huf+16(%rip), %rdi
	movslq	_ZZ9jpegModelR5MixerE3huf(%rip), %rax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rdx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	callq	memset
	movq	_ZZ9jpegModelR5MixerE4pred+16(%rip), %rdi
	movslq	_ZZ9jpegModelR5MixerE4pred(%rip), %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movl	$1, %eax
	jmp	.LBB40_152
.LBB40_162:
	leal	-4(%rax), %edi
	movl	buf(%rip), %r14d
	leal	-1(%r14), %esi
	andl	%esi, %edi
	movq	buf+16(%rip), %r15
	movslq	%edi, %rdi
	cmpb	$-1, (%r15,%rdi)
	jne	.LBB40_166
# BB#163:
	leal	-3(%rax), %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movb	(%r15,%rdi), %bl
	cmpb	$-2, %bl
	je	.LBB40_165
# BB#164:
	andb	$-16, %bl
	cmpb	$-32, %bl
	jne	.LBB40_166
.LBB40_165:
	leal	-2(%rax), %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%r15,%rdi), %edi
	shll	$8, %edi
	leal	-1(%rax), %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rsi
	movzbl	(%r15,%rsi), %esi
	leal	2(%rdi,%rsi), %esi
	movl	%esi, _ZZ9jpegModelR5MixerE3app(%rip)
.LBB40_166:
	leal	-5(%rax), %esi
	leal	-1(%r14), %ecx
	movl	%ecx, %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	leal	-4(%rax), %r8d
	movl	%ecx, %edx
	andl	%r8d, %edx
	movslq	%edx, %rbx
	cmpb	$-1, (%r15,%rdi)
	jne	.LBB40_171
# BB#167:
	cmpb	$-38, (%r15,%rbx)
	jne	.LBB40_171
# BB#168:
	leal	-1(%rax), %edx
	andl	%ecx, %edx
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r10d
	movl	%r10d, %edx
	decb	%dl
	cmpb	$3, %dl
	ja	.LBB40_171
# BB#169:
	leal	-3(%rax), %r9d
	movl	%ecx, %edx
	andl	%r9d, %edx
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %edx
	shll	$8, %edx
	leal	-2(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%r15,%rdi), %r11d
	orl	%edx, %r11d
	leal	6(%r10,%r10), %edx
	cmpl	%edx, %r11d
	jne	.LBB40_171
# BB#170:
	movl	%esi, _ZZ9jpegModelR5MixerE3sos(%rip)
	addl	%r11d, %r9d
	movl	%r9d, _ZZ9jpegModelR5MixerE4data(%rip)
	movl	$2, _ZZ9jpegModelR5MixerE4jpeg(%rip)
	movl	%r9d, %ebp
.LBB40_171:                             # %._crit_edge1019
	cmpb	$-1, (%r15,%rbx)
	jne	.LBB40_175
# BB#172:
	leal	-3(%rax), %edx
	andl	%edx, %ecx
	movslq	%ecx, %rcx
	cmpb	$-60, (%r15,%rcx)
	jne	.LBB40_175
# BB#173:
	movl	_ZZ9jpegModelR5MixerE6htsize(%rip), %esi
	cmpl	$7, %esi
	jg	.LBB40_175
# BB#174:
	movslq	%esi, %rax
	incl	%esi
	movl	%esi, _ZZ9jpegModelR5MixerE6htsize(%rip)
	movq	_ZZ9jpegModelR5MixerE2ht+16(%rip), %rcx
	movl	%r8d, (%rcx,%rax,4)
	movl	pos(%rip), %eax
	movl	buf(%rip), %r14d
.LBB40_175:
	leal	-4(%rax), %esi
	leal	-1(%r14), %edi
	movl	%edi, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	cmpb	$-1, (%r15,%rcx)
	jne	.LBB40_180
# BB#176:
	leal	-3(%rax), %ecx
	andl	%edi, %ecx
	movslq	%ecx, %rbx
	cmpb	$-64, (%r15,%rbx)
	jne	.LBB40_178
# BB#177:
	movl	%esi, _ZZ9jpegModelR5MixerE3sof(%rip)
.LBB40_178:
	cmpb	$-37, (%r15,%rbx)
	jne	.LBB40_180
# BB#179:
	leal	-2(%rax), %ecx
	andl	%edi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r15,%rcx), %ecx
	shll	$8, %ecx
	leal	-1(%rax), %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %edx
	leal	-1(%rax,%rcx), %ecx
	addl	%edx, %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE7dqt_end(%rip)
	xorl	%esi, %esi
	jmp	.LBB40_191
.LBB40_180:
	movl	_ZZ9jpegModelR5MixerE9dqt_state(%rip), %ebx
	testl	%ebx, %ebx
	js	.LBB40_192
# BB#181:
	movl	$-1, %esi
	cmpl	_ZZ9jpegModelR5MixerE7dqt_end(%rip), %eax
	jge	.LBB40_191
# BB#182:
	movl	%ebx, %ecx
	movl	$4228890877, %edx       # imm = 0xFC0FC0FD
	imulq	%rcx, %rdx
	shrq	$38, %rdx
	movl	%edx, %ecx
	shll	$6, %ecx
	addl	%edx, %ecx
	leal	-1(%rax), %edx
	andl	%edx, %edi
	movl	%ebx, %esi
	subl	%ecx, %esi
	movslq	%edi, %rcx
	movzbl	(%r15,%rcx), %edi
	je	.LBB40_189
# BB#183:
	testb	%dil, %dil
	je	.LBB40_211
# BB#184:
	movl	_ZZ9jpegModelR5MixerE4qnum(%rip), %eax
	cmpl	$4, %eax
	jae	.LBB40_211
# BB#185:
	decb	%dil
	shll	$6, %eax
	leal	-1(%rsi,%rax), %eax
	movq	_ZZ9jpegModelR5MixerE4qtab+16(%rip), %rcx
	cltq
	movb	%dil, (%rcx,%rax)
	movl	pos(%rip), %eax
	movl	buf(%rip), %r14d
	movq	buf+16(%rip), %r15
	jmp	.LBB40_190
.LBB40_186:
	decl	%eax
	andl	%eax, %ecx
	movslq	%ecx, %rax
	movb	(%rdx,%rax), %al
	testb	%al, %al
	je	.LBB40_155
# BB#187:
	movl	%eax, %ecx
	andb	$-8, %cl
	cmpb	$-48, %cl
	je	.LBB40_155
# BB#188:
	cmpb	$-39, %al
	jmp	.LBB40_211
.LBB40_189:
	movl	%edi, _ZZ9jpegModelR5MixerE4qnum(%rip)
.LBB40_190:
	incl	%ebx
	movl	%ebx, %esi
.LBB40_191:                             # %.sink.split
	movl	%esi, _ZZ9jpegModelR5MixerE9dqt_state(%rip)
.LBB40_192:
	leal	-2(%rax), %ecx
	decl	%r14d
	andl	%r14d, %ecx
	movslq	%ecx, %rcx
	cmpb	$-1, (%r15,%rcx)
	jne	.LBB40_100
# BB#193:
	decl	%eax
	andl	%eax, %r14d
	movslq	%r14d, %rax
	movb	(%r15,%rax), %al
	andb	$-8, %al
	cmpb	$-48, %al
	jne	.LBB40_100
# BB#194:
	movl	$0, _ZZ9jpegModelR5MixerE6mcupos(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffsize(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffbits(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffcode(%rip)
	movl	$-1, _ZZ9jpegModelR5MixerE2rs(%rip)
	movq	_ZZ9jpegModelR5MixerE4pred+16(%rip), %rdi
	movslq	_ZZ9jpegModelR5MixerE4pred(%rip), %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	cmpl	%ebp, pos(%rip)
	je	.LBB40_101
.LBB40_195:                             # %thread-pre-split811
	movl	_ZZ9jpegModelR5MixerE7mcusize(%rip), %r9d
	testl	%r9d, %r9d
	je	.LBB40_335
.LBB40_196:
	cmpl	$1, bpos(%rip)
	movl	$1, %eax
	adcl	$0, %eax
	movl	pos(%rip), %ecx
	subl	%eax, %ecx
	movl	buf(%rip), %eax
	decl	%eax
	andl	%ecx, %eax
	movq	buf+16(%rip), %rcx
	cltq
	cmpb	$-1, (%rcx,%rax)
	je	.LBB40_335
# BB#197:
	movslq	_ZZ9jpegModelR5MixerE8huffbits(%rip), %rdx
	cmpq	$33, %rdx
	jge	.LBB40_211
# BB#198:
	movl	_ZZ9jpegModelR5MixerE8huffcode(%rip), %ebp
	addl	%ebp, %ebp
	addl	y(%rip), %ebp
	movl	%ebp, _ZZ9jpegModelR5MixerE8huffcode(%rip)
	leal	1(%rdx), %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE8huffbits(%rip)
	movl	_ZZ9jpegModelR5MixerE2rs(%rip), %eax
	testl	%eax, %eax
	js	.LBB40_213
.LBB40_199:                             # %.thread820
	movl	%eax, %r10d
	andl	$15, %r10d
	movl	_ZZ9jpegModelR5MixerE8huffsize(%rip), %esi
	addl	%r10d, %esi
	cmpl	%ecx, %esi
	jne	.LBB40_335
# BB#200:
	movl	_ZZ9jpegModelR5MixerE5huff2(%rip), %esi
	movl	%esi, _ZZ9jpegModelR5MixerE5huff3(%rip)
	movl	_ZZ9jpegModelR5MixerE5huff1(%rip), %esi
	movl	%esi, _ZZ9jpegModelR5MixerE5huff2(%rip)
	imull	$200002979, %ebp, %esi  # imm = 0xBEBCDA3
	imull	$30005491, %ecx, %edi   # imm = 0x1C9D8F3
	leal	-230011545(%rsi,%rdi), %esi
	movl	%ebp, %edi
	shrl	$2, %edi
	shrl	$3, %ecx
	xorl	%edi, %ecx
	xorl	%esi, %ecx
	shrl	$9, %esi
	xorl	%ecx, %esi
	xorl	$201326591, %esi        # imm = 0xBFFFFFF
	movl	%esi, _ZZ9jpegModelR5MixerE5huff1(%rip)
	movl	_ZZ9jpegModelR5MixerE3rs2(%rip), %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE3rs3(%rip)
	movl	_ZZ9jpegModelR5MixerE3rs1(%rip), %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE3rs2(%rip)
	movl	%eax, _ZZ9jpegModelR5MixerE3rs1(%rip)
	movl	_ZZ9jpegModelR5MixerE6mcupos(%rip), %r11d
	testb	$63, %r11b
	je	.LBB40_225
# BB#201:
	testl	%eax, %eax
	je	.LBB40_233
# BB#202:
	cmpl	$11, %r10d
	jae	.LBB40_211
# BB#203:
	shrl	$4, %eax
	leal	(%r11,%rax), %ecx
	xorl	%r11d, %ecx
	cmpl	$63, %ecx
	ja	.LBB40_211
# BB#204:
	leal	1(%rax,%r11), %r11d
	movl	%r11d, _ZZ9jpegModelR5MixerE6mcupos(%rip)
	movl	$1, %esi
	movl	%r10d, %ecx
	shll	%cl, %esi
	decl	%esi
	movl	%ebp, %r8d
	andl	%esi, %r8d
	testl	%r10d, %r10d
	je	.LBB40_206
# BB#205:
	leal	-1(%r10), %ecx
	movl	%r8d, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	xorl	%ecx, %ecx
	testl	%edi, %edi
	cmovel	%esi, %ecx
	subl	%ecx, %r8d
.LBB40_206:
	movl	_ZZ9jpegModelR5MixerE4cpos(%rip), %ebx
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %ecx
	decl	%ecx
	andl	%ebx, %ecx
	movslq	%ecx, %rcx
	shlq	$2, %rcx
	addq	_ZZ9jpegModelR5MixerE5cbuf2+16(%rip), %rcx
	testl	%eax, %eax
	je	.LBB40_210
# BB#207:                               # %.lr.ph895.preheader
	leal	1(%rax), %esi
	shll	$4, %eax
.LBB40_208:                             # %.lr.ph895
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rcx)
	movl	%eax, %ecx
	orl	%r10d, %ecx
	movl	_ZZ9jpegModelR5MixerE4cbuf(%rip), %edi
	decl	%edi
	andl	%ebx, %edi
	incl	%ebx
	movq	_ZZ9jpegModelR5MixerE4cbuf+16(%rip), %rdx
	movslq	%edi, %rdi
	movb	%cl, (%rdx,%rdi)
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %ecx
	decl	%ecx
	andl	%ebx, %ecx
	movslq	%ecx, %rcx
	shlq	$2, %rcx
	addq	_ZZ9jpegModelR5MixerE5cbuf2+16(%rip), %rcx
	decl	%esi
	addl	$-16, %eax
	cmpl	$1, %esi
	jg	.LBB40_208
# BB#209:                               # %..thread822_crit_edge
	movl	%ebx, _ZZ9jpegModelR5MixerE4cpos(%rip)
.LBB40_210:                             # %.thread822
	movl	%r8d, (%rcx)
	movl	%r10d, %eax
	shll	$4, %eax
	shll	$2, %ebp
	movl	%r10d, %ecx
	shrl	%cl, %ebp
	andl	$3, %ebp
	orl	%eax, %ebp
	orb	$12, %bpl
	leal	1(%rbx), %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE4cpos(%rip)
	movl	_ZZ9jpegModelR5MixerE4cbuf(%rip), %eax
	decl	%eax
	andl	%ebx, %eax
	movq	_ZZ9jpegModelR5MixerE4cbuf+16(%rip), %rsi
	cltq
	movb	%bpl, (%rsi,%rax)
	addl	_ZZ9jpegModelR5MixerE4ssum(%rip), %r10d
	movl	%r10d, _ZZ9jpegModelR5MixerE4ssum(%rip)
	testl	%r11d, %r11d
	jns	.LBB40_239
	jmp	.LBB40_211
.LBB40_213:
	testl	%edx, %edx
	js	.LBB40_211
# BB#214:
	cmpl	$17, %ecx
	jge	.LBB40_211
# BB#215:
	movl	_ZZ9jpegModelR5MixerE6mcupos(%rip), %esi
	xorl	%eax, %eax
	movl	%esi, %edi
	andl	$63, %edi
	setne	%r8b
	testl	%esi, %esi
	js	.LBB40_211
# BB#216:
	cmpl	$640, %esi              # imm = 0x280
	jae	.LBB40_211
# BB#217:
	xorl	%ebx, %ebx
	testl	%edi, %edi
	setne	%bl
	shrl	$6, %esi
	shlq	$2, %rsi
	leaq	(%rbx,%rbx,4), %rdi
	movl	_ZZ9jpegModelR5MixerE6hufsel(%rsi,%rdi,8), %esi
	cmpl	$4, %esi
	jae	.LBB40_211
# BB#218:
	cmpl	$16, %edx
	jae	.LBB40_211
# BB#219:
	movb	%r8b, %al
	shll	$6, %eax
	shll	$4, %esi
	addl	%eax, %esi
	movslq	%esi, %rax
	leaq	(%rax,%rax,2), %rax
	shlq	$2, %rax
	addq	_ZZ9jpegModelR5MixerE3huf+16(%rip), %rax
	leaq	(%rdx,%rdx,2), %rdi
	movl	(%rax,%rdi,4), %edx
	movl	4(%rax,%rdi,4), %esi
	cmpl	%esi, %edx
	ja	.LBB40_211
# BB#220:
	movl	8(%rax,%rdi,4), %eax
	cmpl	$2048, %eax             # imm = 0x800
	jge	.LBB40_211
# BB#221:
	cmpl	%esi, %ebp
	jae	.LBB40_335
# BB#222:
	cmpl	%edx, %ebp
	jb	.LBB40_211
# BB#223:
	subl	%edx, %eax
	addl	%ebp, %eax
	cmpl	$2047, %eax             # imm = 0x7FF
	ja	.LBB40_211
# BB#224:                               # %.thread831
	movq	_ZZ9jpegModelR5MixerE4hbuf+16(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %eax
	movl	%eax, _ZZ9jpegModelR5MixerE2rs(%rip)
	movl	%ecx, _ZZ9jpegModelR5MixerE8huffsize(%rip)
	jmp	.LBB40_199
.LBB40_225:
	cmpl	$12, %eax
	jge	.LBB40_211
# BB#226:
	leal	1(%r11), %edx
	movl	%edx, _ZZ9jpegModelR5MixerE6mcupos(%rip)
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%esi, %ebp
	testl	%eax, %eax
	je	.LBB40_228
# BB#227:
	leal	-1(%rax), %ecx
	movl	%ebp, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	xorl	%ecx, %ecx
	testl	%edi, %edi
	cmovel	%esi, %ecx
	subl	%ecx, %ebp
.LBB40_228:
	cmpl	$-1, %r11d
	jl	.LBB40_211
# BB#229:
	cmpl	$640, %edx              # imm = 0x280
	jae	.LBB40_211
# BB#230:
	movl	%edx, %ecx
	shrl	$6, %ecx
	movq	_ZZ9jpegModelR5MixerE5color+16(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	cmpq	$4, %rcx
	jae	.LBB40_211
# BB#231:
	movq	_ZZ9jpegModelR5MixerE4pred+16(%rip), %rsi
	addl	(%rsi,%rcx,4), %ebp
	movl	%ebp, (%rsi,%rcx,4)
	movl	%ebp, _ZZ9jpegModelR5MixerE2dc(%rip)
	movl	_ZZ9jpegModelR5MixerE4cpos(%rip), %esi
	testb	$63, %sil
	jne	.LBB40_211
# BB#232:
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %ecx
	decl	%ecx
	andl	%esi, %ecx
	movq	_ZZ9jpegModelR5MixerE5cbuf2+16(%rip), %rdi
	movslq	%ecx, %rcx
	movl	%ebp, (%rdi,%rcx,4)
	addl	$1023, %ebp             # imm = 0x3FF
	shrl	$3, %ebp
	leal	1(%rsi), %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE4cpos(%rip)
	movl	_ZZ9jpegModelR5MixerE4cbuf(%rip), %edi
	decl	%edi
	andl	%esi, %edi
	movq	_ZZ9jpegModelR5MixerE4cbuf+16(%rip), %rsi
	movslq	%edi, %rdi
	movb	%bpl, (%rsi,%rdi)
	movl	_ZZ9jpegModelR5MixerE5ssum2(%rip), %esi
	movl	%esi, _ZZ9jpegModelR5MixerE5ssum3(%rip)
	movl	_ZZ9jpegModelR5MixerE5ssum1(%rip), %esi
	movl	%esi, _ZZ9jpegModelR5MixerE5ssum2(%rip)
	movl	_ZZ9jpegModelR5MixerE4ssum(%rip), %esi
	movl	%esi, _ZZ9jpegModelR5MixerE5ssum1(%rip)
	movl	%eax, _ZZ9jpegModelR5MixerE4ssum(%rip)
	movl	%edx, %r11d
	testl	%r11d, %r11d
	jns	.LBB40_239
	jmp	.LBB40_211
.LBB40_233:
	addl	$63, %r11d
	andl	$-64, %r11d
	movl	%r11d, _ZZ9jpegModelR5MixerE6mcupos(%rip)
	js	.LBB40_211
# BB#234:
	cmpl	$640, %r11d             # imm = 0x280
	jg	.LBB40_211
# BB#235:
	cmpl	%r9d, %r11d
	jg	.LBB40_211
# BB#236:                               # %.preheader854
	movl	_ZZ9jpegModelR5MixerE4cpos(%rip), %ecx
	testb	$63, %cl
	je	.LBB40_239
.LBB40_237:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %eax
	decl	%eax
	andl	%ecx, %eax
	movq	_ZZ9jpegModelR5MixerE5cbuf2+16(%rip), %rdx
	cltq
	movl	$0, (%rdx,%rax,4)
	movl	_ZZ9jpegModelR5MixerE4cbuf(%rip), %eax
	decl	%eax
	andl	%ecx, %eax
	incl	%ecx
	movq	_ZZ9jpegModelR5MixerE4cbuf+16(%rip), %rdx
	cltq
	movb	$0, (%rdx,%rax)
	testb	$63, %cl
	jne	.LBB40_237
# BB#238:                               # %..loopexit855_crit_edge
	movl	%ecx, _ZZ9jpegModelR5MixerE4cpos(%rip)
	testl	%r11d, %r11d
	js	.LBB40_211
.LBB40_239:                             # %.loopexit855.thread
	cmpl	%r9d, %r11d
	jg	.LBB40_211
# BB#240:
	jl	.LBB40_243
# BB#241:
	movl	$0, _ZZ9jpegModelR5MixerE6mcupos(%rip)
	movl	_ZZ9jpegModelR5MixerE6column(%rip), %eax
	incl	%eax
	movl	%eax, _ZZ9jpegModelR5MixerE6column(%rip)
	xorl	%r11d, %r11d
	cmpl	_ZZ9jpegModelR5MixerE5width(%rip), %eax
	jne	.LBB40_243
# BB#242:
	movl	$0, _ZZ9jpegModelR5MixerE6column(%rip)
	incl	_ZZ9jpegModelR5MixerE3row(%rip)
.LBB40_243:
	movabsq	$-34362885646, %r10     # imm = 0xFFFFFFF7FFCFF9F2
	movl	$0, _ZZ9jpegModelR5MixerE8huffbits(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffsize(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE8huffcode(%rip)
	movl	$-1, _ZZ9jpegModelR5MixerE2rs(%rip)
	movl	%r11d, %eax
	sarl	$6, %eax
	movq	_ZZ9jpegModelR5MixerE4qmap+16(%rip), %rdx
	cltq
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	(%rdx,%rax,4), %r14
	shlq	$6, %r14
	movl	%r11d, %eax
	andl	$63, %eax
	movl	%ecx, %edx
	subl	%eax, %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	andl	$63, %r11d
	movq	%r12, 40(%rsp)          # 8-byte Spill
	je	.LBB40_245
# BB#244:
	movslq	%r11d, %rdi
	leal	-1(%r11), %r9d
	movzbl	_ZZ9jpegModelR5MixerE3zzv-1(%rdi), %eax
	movl	%r14d, %esi
	orl	%r11d, %esi
	movslq	%esi, %rsi
	movq	_ZZ9jpegModelR5MixerE4qtab+16(%rip), %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movzbl	-1(%rbp,%rsi), %esi
	incl	%esi
	movl	_ZZ9jpegModelR5MixerE2we(,%rax,4), %ebp
	imull	%esi, %ebp
	decl	%ecx
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %ebx
	decl	%ebx
	andl	%ecx, %ebx
	movq	_ZZ9jpegModelR5MixerE5cbuf2+16(%rip), %rdx
	movslq	%ebx, %rcx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	(%rdx,%rcx,4), %ecx
	imull	%ecx, %ebp
	movzbl	_ZZ9jpegModelR5MixerE3zzu-1(%rdi), %edi
	subl	%ebp, _ZZ9jpegModelR5MixerE5sumu2(,%rdi,4)
	movl	_ZZ9jpegModelR5MixerE2we(,%rdi,4), %ebp
	imull	%esi, %ebp
	imull	%ecx, %ebp
	subl	%ebp, _ZZ9jpegModelR5MixerE5sumv2(,%rax,4)
	movq	$-402702436, %rbp       # imm = 0xE7FF3F9C
	btq	%r9, %rbp
	movl	$256, %r8d              # imm = 0x100
	movl	$181, %ebx
	movl	$181, %ebp
	cmovbl	%r8d, %ebp
	imull	%ecx, %ebp
	imull	%esi, %ebp
	subl	%ebp, _ZZ9jpegModelR5MixerE5sumu3(,%rdi,4)
	btq	%r9, %r10
	cmovbl	%r8d, %ebx
	imull	%ecx, %ebx
	imull	%esi, %ebx
	subl	%ebx, _ZZ9jpegModelR5MixerE5sumv3(,%rax,4)
	jmp	.LBB40_279
.LBB40_245:                             # %.preheader853.preheader
	xorl	%ecx, %ecx
.LBB40_246:                             # %.preheader853
                                        # =>This Inner Loop Header: Depth=1
	movl	_ZZ9jpegModelR5MixerE5sumv2(%rcx), %esi
	movl	_ZZ9jpegModelR5MixerE5sumv3(%rcx), %r8d
	movl	_ZZ9jpegModelR5MixerE2kx(%rcx), %edx
	leal	-1(%rdx), %eax
	imull	%esi, %eax
	movl	$9, %edi
	subl	%edx, %edi
	imull	%r8d, %edi
	addl	%eax, %edi
	movl	%edi, %ebx
	negl	%ebx
	cmovll	%edi, %ebx
	movl	%edx, %eax
	imull	%esi, %eax
	movl	$8, %edi
	subl	%edx, %edi
	imull	%r8d, %edi
	addl	%eax, %edi
	movl	%edi, %ebp
	negl	%ebp
	cmovll	%edi, %ebp
	movl	%ebp, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%ebp, %eax
	sarl	$3, %eax
	leal	1(%rdx), %edi
	imull	%esi, %edi
	movl	$7, %esi
	subl	%edx, %esi
	imull	%r8d, %esi
	addl	%edi, %esi
	movl	%esi, %ebp
	negl	%ebp
	cmovll	%esi, %ebp
	movl	%ebp, %edi
	sarl	$31, %edi
	shrl	$29, %edi
	addl	%ebp, %edi
	sarl	$3, %edi
	testl	%edx, %edx
	je	.LBB40_252
# BB#247:                               # %.preheader853
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	%ebx, %esi
	sarl	$31, %esi
	shrl	$29, %esi
	addl	%ebx, %esi
	sarl	$3, %esi
	cmpl	$8, %edx
	movl	%eax, %ebp
	je	.LBB40_249
# BB#248:                               # %.fold.split.i782
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	%edi, %ebp
.LBB40_249:                             #   in Loop: Header=BB40_246 Depth=1
	cmpl	%eax, %esi
	jge	.LBB40_253
# BB#250:                               #   in Loop: Header=BB40_246 Depth=1
	cmpl	%ebp, %esi
	jge	.LBB40_253
# BB#251:                               #   in Loop: Header=BB40_246 Depth=1
	decl	_ZZ9jpegModelR5MixerE2kx+64(%rcx)
	jmp	.LBB40_253
.LBB40_252:                             #   in Loop: Header=BB40_246 Depth=1
	movl	%edi, %ebp
	movl	%eax, %esi
.LBB40_253:                             # %.thread.i788
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	_ZZ9jpegModelR5MixerE2kx+64(%rcx), %ebx
	cmpl	%eax, %ebp
	jge	.LBB40_256
# BB#254:                               # %.thread.i788
                                        #   in Loop: Header=BB40_246 Depth=1
	cmpl	%esi, %ebp
	jge	.LBB40_256
# BB#255:                               #   in Loop: Header=BB40_246 Depth=1
	incl	%ebx
	movl	%ebx, _ZZ9jpegModelR5MixerE2kx+64(%rcx)
.LBB40_256:                             # %thread-pre-split.i790
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	$-1, %eax
	cmpl	$-2, %ebx
	jl	.LBB40_258
# BB#257:                               #   in Loop: Header=BB40_246 Depth=1
	movl	$1, %eax
	cmpl	$3, %ebx
	jl	.LBB40_259
.LBB40_258:                             # %.sink.split.i792
                                        #   in Loop: Header=BB40_246 Depth=1
	addl	%eax, %edx
	movl	%edx, _ZZ9jpegModelR5MixerE2kx(%rcx)
	movl	$0, _ZZ9jpegModelR5MixerE2kx+64(%rcx)
.LBB40_259:                             # %_Z8update_kiiRiS_.exit793
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	_ZZ9jpegModelR5MixerE5sumu2(%rcx), %esi
	movl	_ZZ9jpegModelR5MixerE5sumu3(%rcx), %r8d
	movl	_ZZ9jpegModelR5MixerE2kx+32(%rcx), %edx
	leal	-1(%rdx), %eax
	imull	%esi, %eax
	movl	$9, %edi
	subl	%edx, %edi
	imull	%r8d, %edi
	addl	%eax, %edi
	movl	%edi, %ebx
	negl	%ebx
	cmovll	%edi, %ebx
	movl	%edx, %eax
	imull	%esi, %eax
	movl	$8, %edi
	subl	%edx, %edi
	imull	%r8d, %edi
	addl	%eax, %edi
	movl	%edi, %ebp
	negl	%ebp
	cmovll	%edi, %ebp
	movl	%ebp, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%ebp, %eax
	sarl	$3, %eax
	leal	1(%rdx), %edi
	imull	%esi, %edi
	movl	$7, %esi
	subl	%edx, %esi
	imull	%r8d, %esi
	addl	%edi, %esi
	movl	%esi, %ebp
	negl	%ebp
	cmovll	%esi, %ebp
	movl	%ebp, %edi
	sarl	$31, %edi
	shrl	$29, %edi
	addl	%ebp, %edi
	sarl	$3, %edi
	testl	%edx, %edx
	je	.LBB40_265
# BB#260:                               # %_Z8update_kiiRiS_.exit793
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	%ebx, %esi
	sarl	$31, %esi
	shrl	$29, %esi
	addl	%ebx, %esi
	sarl	$3, %esi
	cmpl	$8, %edx
	movl	%eax, %ebp
	je	.LBB40_262
# BB#261:                               # %.fold.split.i
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	%edi, %ebp
.LBB40_262:                             #   in Loop: Header=BB40_246 Depth=1
	cmpl	%eax, %esi
	jge	.LBB40_266
# BB#263:                               #   in Loop: Header=BB40_246 Depth=1
	cmpl	%ebp, %esi
	jge	.LBB40_266
# BB#264:                               #   in Loop: Header=BB40_246 Depth=1
	decl	_ZZ9jpegModelR5MixerE2kx+96(%rcx)
	jmp	.LBB40_266
.LBB40_265:                             #   in Loop: Header=BB40_246 Depth=1
	movl	%edi, %ebp
	movl	%eax, %esi
.LBB40_266:                             # %.thread.i
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	_ZZ9jpegModelR5MixerE2kx+96(%rcx), %ebx
	cmpl	%eax, %ebp
	jge	.LBB40_269
# BB#267:                               # %.thread.i
                                        #   in Loop: Header=BB40_246 Depth=1
	cmpl	%esi, %ebp
	jge	.LBB40_269
# BB#268:                               #   in Loop: Header=BB40_246 Depth=1
	incl	%ebx
	movl	%ebx, _ZZ9jpegModelR5MixerE2kx+96(%rcx)
.LBB40_269:                             # %thread-pre-split.i
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	$-1, %eax
	cmpl	$-2, %ebx
	jl	.LBB40_271
# BB#270:                               #   in Loop: Header=BB40_246 Depth=1
	movl	$1, %eax
	cmpl	$3, %ebx
	jl	.LBB40_272
.LBB40_271:                             # %.sink.split.i
                                        #   in Loop: Header=BB40_246 Depth=1
	addl	%eax, %edx
	movl	%edx, _ZZ9jpegModelR5MixerE2kx+32(%rcx)
	movl	$0, _ZZ9jpegModelR5MixerE2kx+96(%rcx)
.LBB40_272:                             # %_Z8update_kiiRiS_.exit
                                        #   in Loop: Header=BB40_246 Depth=1
	movl	$0, _ZZ9jpegModelR5MixerE5sumv3(%rcx)
	movl	$0, _ZZ9jpegModelR5MixerE5sumu3(%rcx)
	movl	$0, _ZZ9jpegModelR5MixerE5sumv2(%rcx)
	movl	$0, _ZZ9jpegModelR5MixerE5sumu2(%rcx)
	addq	$4, %rcx
	cmpq	$32, %rcx
	jne	.LBB40_246
# BB#273:
	movq	_ZZ9jpegModelR5MixerE2ls+16(%rip), %rax
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %esi
	movq	32(%rsp), %rdx          # 8-byte Reload
	subl	(%rax,%rdx,4), %esi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	imull	_ZZ9jpegModelR5MixerE5width(%rip), %r9d
	movl	%ecx, %r13d
	subl	%r9d, %r13d
	movq	_ZZ9jpegModelR5MixerE4qtab+16(%rip), %rax
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %edi
	decl	%edi
	movq	_ZZ9jpegModelR5MixerE5cbuf2+16(%rip), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r14, %r10
	movq	%rax, 8(%rsp)           # 8-byte Spill
	addq	%rax, %r10
	xorl	%ecx, %ecx
.LBB40_274:                             # =>This Inner Loop Header: Depth=1
	movzbl	_ZZ9jpegModelR5MixerE3zzv(%rcx), %eax
	movabsq	$-2978192880766211436, %r9 # imm = 0xD6AB555AA5552A94
	shrq	%cl, %r9
	leal	(%r9,%r9), %edx
	notl	%edx
	andl	$2, %edx
	decl	%edx
	imull	_ZZ9jpegModelR5MixerE2we(,%rax,4), %edx
	movzbl	(%r10,%rcx), %ebx
	incl	%ebx
	leal	(%r13,%rcx), %esi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rsi,4), %r8d
	movl	%ebx, %esi
	imull	%r8d, %esi
	imull	%edx, %esi
	movzbl	_ZZ9jpegModelR5MixerE3zzu(%rcx), %r12d
	addl	%esi, _ZZ9jpegModelR5MixerE5sumu2(,%r12,4)
	movabsq	$-5374292623298483886, %r15 # imm = 0xB56AAD55554AA952
	shrq	%cl, %r15
	leal	(%r15,%r15), %esi
	notl	%esi
	andl	$2, %esi
	decl	%esi
	imull	_ZZ9jpegModelR5MixerE2we(,%r12,4), %esi
	movq	48(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rcx), %edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movl	(%rbp,%rdx,4), %edx
	movl	%ebx, %ebp
	imull	%edx, %ebp
	imull	%esi, %ebp
	addl	%ebp, _ZZ9jpegModelR5MixerE5sumv2(,%rax,4)
	movl	$181, %esi
	movq	$-402702436, %rbp       # imm = 0xE7FF3F9C
	btq	%rcx, %rbp
	movl	$181, %ebp
	jae	.LBB40_276
# BB#275:                               #   in Loop: Header=BB40_274 Depth=1
	shll	$9, %r9d
	notl	%r9d
	andl	$512, %r9d              # imm = 0x200
	addl	$-256, %r9d
	movl	%r9d, %ebp
.LBB40_276:                             #   in Loop: Header=BB40_274 Depth=1
	imull	%ebx, %ebp
	imull	%r8d, %ebp
	addl	%ebp, _ZZ9jpegModelR5MixerE5sumu3(,%r12,4)
	movabsq	$-34362885646, %rbp     # imm = 0xFFFFFFF7FFCFF9F2
	btq	%rcx, %rbp
	jae	.LBB40_278
# BB#277:                               #   in Loop: Header=BB40_274 Depth=1
	shll	$9, %r15d
	notl	%r15d
	andl	$512, %r15d             # imm = 0x200
	addl	$-256, %r15d
	movl	%r15d, %esi
.LBB40_278:                             #   in Loop: Header=BB40_274 Depth=1
	imull	%esi, %ebx
	imull	%edx, %ebx
	addl	%ebx, _ZZ9jpegModelR5MixerE5sumv3(,%rax,4)
	incq	%rcx
	cmpq	$64, %rcx
	jne	.LBB40_274
.LBB40_279:                             # %.preheader852
	movq	_ZZ9jpegModelR5MixerE4sumv+16(%rip), %rsi
	movq	_ZZ9jpegModelR5MixerE4sumu+16(%rip), %rbp
	xorl	%eax, %eax
.LBB40_280:                             # =>This Inner Loop Header: Depth=1
	movl	_ZZ9jpegModelR5MixerE2kx(%rax), %ecx
	movl	$8, %edx
	subl	%ecx, %edx
	imull	_ZZ9jpegModelR5MixerE5sumv2(%rax), %ecx
	imull	_ZZ9jpegModelR5MixerE5sumv3(%rax), %edx
	addl	%ecx, %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	sarl	$3, %ecx
	movl	%ecx, (%rsi,%rax)
	movl	_ZZ9jpegModelR5MixerE2kx+32(%rax), %ecx
	movl	$8, %edx
	subl	%ecx, %edx
	imull	_ZZ9jpegModelR5MixerE5sumu2(%rax), %ecx
	imull	_ZZ9jpegModelR5MixerE5sumu3(%rax), %edx
	addl	%ecx, %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%edx, %ecx
	sarl	$3, %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	cmpq	$32, %rax
	jne	.LBB40_280
# BB#281:                               # %.preheader850.preheader
	xorl	%edi, %edi
.LBB40_282:                             # %.preheader850
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_284 Depth 2
	movl	$2, %r8d
	subl	%edi, %r8d
	xorl	%r9d, %r9d
	jmp	.LBB40_284
.LBB40_283:                             # %.thread1036
                                        #   in Loop: Header=BB40_284 Depth=2
	movq	_ZZ9jpegModelR5MixerE8adv_pred+16(%rip), %rbx
	movl	%ecx, (%rbx,%rdi,4)
	sarq	$36, %rax
	addl	%edx, %eax
	movl	%eax, 16(%rbx,%rdi,4)
	movl	$1, %r9d
.LBB40_284:                             # %.backedge
                                        #   Parent Loop BB40_282 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r9,%r11), %eax
	cmpl	$64, %eax
	movl	$63, %ecx
	cmovgel	%ecx, %eax
	movslq	%eax, %rcx
	movzbl	_ZZ9jpegModelR5MixerE3zzu(%rcx), %eax
	movl	(%rbp,%rax,4), %eax
	imull	%edi, %eax
	movzbl	_ZZ9jpegModelR5MixerE3zzv(%rcx), %edx
	movl	(%rsi,%rdx,4), %edx
	imull	%r8d, %edx
	addl	%eax, %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	leal	(%rcx,%r14), %edx
	movslq	%edx, %rdx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movzbl	(%rbx,%rdx), %edx
	imull	$181, %edx, %ebx
	addl	$181, %ebx
	cltd
	idivl	%ebx
	movl	$10, %edx
	testl	%ecx, %ecx
	jne	.LBB40_286
# BB#285:                               #   in Loop: Header=BB40_284 Depth=2
	movq	_ZZ9jpegModelR5MixerE2ls+16(%rip), %rcx
	movl	24(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	subl	(%rcx,%rbx,4), %edx
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %ecx
	decl	%ecx
	andl	%edx, %ecx
	movslq	%ecx, %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	subl	(%rdx,%rcx,4), %eax
	movl	$14, %edx
.LBB40_286:                             #   in Loop: Header=BB40_284 Depth=2
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	sarl	$31, %eax
	orl	$1, %eax
	imull	%edx, %ecx
	movq	ilog+16(%rip), %r15
	andl	$65534, %ecx            # imm = 0xFFFE
	orl	$1, %ecx
	movzbl	(%r15,%rcx), %ebx
	imull	%eax, %ebx
	movslq	%ebx, %rax
	imulq	$1717986919, %rax, %rax # imm = 0x66666667
	movq	%rax, %rdx
	shrq	$63, %rdx
	movq	%rax, %rcx
	sarq	$34, %rcx
	addl	%edx, %ecx
	testl	%r9d, %r9d
	je	.LBB40_283
# BB#287:                               #   in Loop: Header=BB40_284 Depth=2
	movq	%r14, %r12
	movq	%r11, %r13
	movl	%ecx, %r11d
	negl	%r11d
	cmpl	$-10, %ebx
	cmovgl	%ecx, %r11d
	movq	_ZZ9jpegModelR5MixerE8adv_pred+16(%rip), %rcx
	movl	(%rcx,%rdi,4), %r14d
	movl	%r14d, %r10d
	negl	%r10d
	cmovll	%r14d, %r10d
	incl	%r10d
	cmpl	%r10d, %r11d
	jg	.LBB40_289
# BB#288:                               #   in Loop: Header=BB40_284 Depth=2
	incl	%r9d
	cmpl	$8, %r9d
	movq	%r13, %r11
	movq	%r12, %r14
	jl	.LBB40_284
	jmp	.LBB40_291
.LBB40_289:                             #   in Loop: Header=BB40_282 Depth=1
	xorl	%r8d, %r8d
	cmpl	$9, %ebx
	setg	%r8b
	leal	(%r8,%r9,2), %r8d
	shll	$6, %r8d
	addl	%r8d, %r14d
	movl	%r14d, (%rcx,%rdi,4)
	sarq	$36, %rax
	addl	%edx, %eax
	movl	%eax, %edx
	negl	%edx
	cmpl	$-40, %ebx
	cmovgl	%eax, %edx
	movl	16(%rcx,%rdi,4), %ebx
	movl	%ebx, %eax
	negl	%eax
	cmovll	%ebx, %eax
	incl	%eax
	cmpl	%eax, %edx
	movq	%r13, %r11
	movq	%r12, %r14
	jle	.LBB40_291
# BB#290:                               # %.critedge722
                                        #   in Loop: Header=BB40_282 Depth=1
	addl	%r8d, %ebx
	movl	%ebx, 16(%rcx,%rdi,4)
.LBB40_291:                             # %.critedge723
                                        #   in Loop: Header=BB40_282 Depth=1
	incq	%rdi
	cmpq	$3, %rdi
	jne	.LBB40_282
# BB#292:
	movabsq	$34362885645, %rdi      # imm = 0x80030060D
	movl	%r11d, %r12d
	movzbl	_ZZ9jpegModelR5MixerE3zzu(%r12), %r9d
	movzbl	_ZZ9jpegModelR5MixerE3zzv(%r12), %r10d
	movl	(%rsi,%r10,4), %eax
	addl	(%rbp,%r9,4), %eax
	addl	%eax, %eax
	movq	_ZZ9jpegModelR5MixerE4sumu+16(%rip), %rsi
	movq	_ZZ9jpegModelR5MixerE4sumv+16(%rip), %rdx
	movq	%rdi, %r13
	btq	%r11, %rdi
	jae	.LBB40_294
# BB#293:
	subl	4(%rsi), %eax
.LBB40_294:
	movl	$402702435, %edi        # imm = 0x1800C063
	btq	%r12, %rdi
	movabsq	$120266952479, %rbx     # imm = 0x1C00780F1F
	jae	.LBB40_296
# BB#295:
	subl	4(%rdx), %eax
.LBB40_296:
	btq	%r12, %rbx
	movabsq	$4399053267191, %rdi    # imm = 0x4003C01E0F7
	jae	.LBB40_298
# BB#297:
	subl	8(%rsi), %eax
.LBB40_298:
	btq	%r12, %rdi
	jae	.LBB40_300
# BB#299:
	subl	8(%rdx), %eax
.LBB40_300:
	movabsq	$281741281206207, %rdi  # imm = 0x1003E00FC1FBF
	btq	%r12, %rdi
	jae	.LBB40_302
# BB#301:
	subl	12(%rsi), %eax
.LBB40_302:
	movabsq	$15395276976639, %rdi   # imm = 0xE007E03F1FF
	btq	%r12, %rdi
	jae	.LBB40_304
# BB#303:
	subl	12(%rdx), %eax
.LBB40_304:
	movabsq	$985707912773631, %rdi  # imm = 0x3807F01FE3FFF
	btq	%r12, %rdi
	jae	.LBB40_306
# BB#305:
	subl	16(%rsi), %eax
.LBB40_306:
	movabsq	$9041288393915391, %rdi # imm = 0x201F00FF07FBFF
	btq	%r12, %rdi
	jae	.LBB40_308
# BB#307:
	subl	16(%rdx), %eax
.LBB40_308:
	movabsq	$146297716576583679, %rdi # imm = 0x207C0FF83FF7FFF
	btq	%r12, %rdi
	jae	.LBB40_310
# BB#309:
	subl	20(%rsi), %eax
.LBB40_310:
	movabsq	$31595024962551807, %rdi # imm = 0x703F81FF8FFFFF
	btq	%r12, %rdi
	jae	.LBB40_312
# BB#311:
	subl	20(%rdx), %eax
.LBB40_312:
	movabsq	$508873771604508671, %rdi # imm = 0x70FE1FFC7FFFFFF
	btq	%r12, %rdi
	jae	.LBB40_314
# BB#313:
	subl	24(%rsi), %eax
.LBB40_314:
	movabsq	$1222867778619310079, %rdi # imm = 0x10F87FC3FFDFFFFF
	btq	%r12, %rdi
	jae	.LBB40_316
# BB#315:
	subl	24(%rdx), %eax
.LBB40_316:
	movabsq	$5737572730862043135, %rdi # imm = 0x4F9FF3FFEFFFFFFF
	btq	%r12, %rdi
	jae	.LBB40_318
# BB#317:
	subl	28(%rsi), %eax
.LBB40_318:
	movabsq	$4178495926190473215, %rsi # imm = 0x39FCFFE7FFFFFFFF
	btq	%r12, %rsi
	jae	.LBB40_320
# BB#319:
	subl	28(%rdx), %eax
.LBB40_320:
	movl	%r14d, %edx
	orl	%r11d, %edx
	movslq	%edx, %r8
	movq	8(%rsp), %rdx           # 8-byte Reload
	movzbl	(%rdx,%r8), %edx
	imull	$181, %edx, %edi
	addl	$181, %edi
	cltd
	idivl	%edi
	testl	%r11d, %r11d
	je	.LBB40_322
# BB#321:
	movq	%rbx, %r11
	jmp	.LBB40_323
.LBB40_322:
	movq	%rbx, %r11
	movq	_ZZ9jpegModelR5MixerE2ls+16(%rip), %rdx
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	subl	(%rdx,%rbp,4), %edi
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %edx
	decl	%edx
	andl	%edi, %edx
	movslq	%edx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	subl	(%rsi,%rdx,4), %eax
.LBB40_323:
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	sarl	$31, %eax
	orl	$1, %eax
	addl	%edx, %edx
	leal	(%rdx,%rdx,4), %edx
	andl	$65534, %edx            # imm = 0xFFFE
	orl	$1, %edx
	movzbl	(%r15,%rdx), %edx
	imull	%eax, %edx
	movslq	%edx, %rax
	imulq	$1717986919, %rax, %rax # imm = 0x66666667
	movq	%rax, %rdx
	shrq	$63, %rdx
	sarq	$34, %rax
	addl	%edx, %eax
	movl	%eax, 12(%rcx)
	movl	$255, %ebp
	btq	%r12, %r13
	movl	$255, %eax
	jb	.LBB40_325
# BB#324:
	leaq	(%r9,%r10,8), %rax
	movq	_ZZ9jpegModelR5MixerE4zpos+16(%rip), %rdx
	movl	-4(%rdx,%rax,4), %edx
	leal	(%rdx,%r14), %eax
	cltq
	movq	8(%rsp), %rbx           # 8-byte Reload
	movzbl	(%rbx,%rax), %eax
	incl	%eax
	addl	24(%rsp), %edx          # 4-byte Folded Reload
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %edi
	decl	%edi
	andl	%edx, %edi
	movslq	%edi, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	imull	(%rsi,%rdx,4), %eax
	movzbl	(%rbx,%r8), %edi
	incl	%edi
	cltd
	idivl	%edi
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	sarl	$31, %eax
	orl	$1, %eax
	shll	$3, %edx
	movzwl	%dx, %edx
	movzbl	1(%r15,%rdx), %edx
	imull	%eax, %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%edx, %eax
	sarl	$3, %eax
.LBB40_325:
	movq	_ZZ9jpegModelR5MixerE3lcp+16(%rip), %rdi
	movl	%eax, (%rdi)
	movl	$402702435, %eax        # imm = 0x1800C063
	btq	%r12, %rax
	jb	.LBB40_327
# BB#326:
	leaq	(%r9,%r10,8), %rax
	movq	_ZZ9jpegModelR5MixerE4zpos+16(%rip), %rdx
	movl	-32(%rdx,%rax,4), %edx
	leal	(%rdx,%r14), %eax
	cltq
	movq	8(%rsp), %rbx           # 8-byte Reload
	movzbl	(%rbx,%rax), %eax
	incl	%eax
	addl	24(%rsp), %edx          # 4-byte Folded Reload
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %ebp
	decl	%ebp
	andl	%edx, %ebp
	movslq	%ebp, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	imull	(%rsi,%rdx,4), %eax
	movzbl	(%rbx,%r8), %ebp
	incl	%ebp
	cltd
	idivl	%ebp
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	sarl	$31, %eax
	orl	$1, %eax
	shll	$3, %edx
	movzwl	%dx, %edx
	movzbl	1(%r15,%rdx), %edx
	imull	%eax, %edx
	movl	%edx, %ebp
	sarl	$31, %ebp
	shrl	$29, %ebp
	addl	%edx, %ebp
	sarl	$3, %ebp
.LBB40_327:
	movl	%ebp, 4(%rdi)
	movl	$255, %ebp
	btq	%r12, %r11
	movl	$255, %eax
	jb	.LBB40_329
# BB#328:
	leaq	(%r9,%r10,8), %rax
	movq	_ZZ9jpegModelR5MixerE4zpos+16(%rip), %rdx
	movl	-8(%rdx,%rax,4), %edx
	leal	(%rdx,%r14), %eax
	cltq
	movq	8(%rsp), %rsi           # 8-byte Reload
	movzbl	(%rsi,%rax), %eax
	incl	%eax
	addl	24(%rsp), %edx          # 4-byte Folded Reload
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %ebx
	decl	%ebx
	andl	%edx, %ebx
	movslq	%ebx, %rdx
	movq	16(%rsp), %rbx          # 8-byte Reload
	imull	(%rbx,%rdx,4), %eax
	movzbl	(%rsi,%r8), %ebx
	incl	%ebx
	cltd
	idivl	%ebx
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	sarl	$31, %eax
	orl	$1, %eax
	shll	$3, %edx
	movzwl	%dx, %edx
	movzbl	1(%r15,%rdx), %edx
	imull	%eax, %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%edx, %eax
	sarl	$3, %eax
.LBB40_329:
	movl	%eax, 8(%rdi)
	movabsq	$4399053267191, %rax    # imm = 0x4003C01E0F7
	btq	%r12, %rax
	jb	.LBB40_331
# BB#330:
	leaq	(%r9,%r10,8), %rax
	movq	_ZZ9jpegModelR5MixerE4zpos+16(%rip), %rdx
	movl	-64(%rdx,%rax,4), %edx
	leal	(%rdx,%r14), %eax
	cltq
	movq	8(%rsp), %rsi           # 8-byte Reload
	movzbl	(%rsi,%rax), %eax
	incl	%eax
	addl	24(%rsp), %edx          # 4-byte Folded Reload
	movl	_ZZ9jpegModelR5MixerE5cbuf2(%rip), %ebp
	decl	%ebp
	andl	%edx, %ebp
	movslq	%ebp, %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
	imull	(%rbp,%rdx,4), %eax
	movzbl	(%rsi,%r8), %esi
	incl	%esi
	cltd
	idivl	%esi
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	sarl	$31, %eax
	orl	$1, %eax
	shll	$3, %edx
	movzwl	%dx, %edx
	movzbl	1(%r15,%rdx), %edx
	imull	%eax, %edx
	movl	%edx, %ebp
	sarl	$31, %ebp
	shrl	$29, %ebp
	addl	%edx, %ebp
	sarl	$3, %ebp
.LBB40_331:
	movl	%ebp, 12(%rdi)
	cmpl	$0, _ZZ9jpegModelR5MixerE6column(%rip)
	movq	40(%rsp), %r12          # 8-byte Reload
	jne	.LBB40_333
# BB#332:
	movl	8(%rcx), %eax
	movl	%eax, 4(%rcx)
	movl	$1, (%rcx)
.LBB40_333:
	cmpl	$0, _ZZ9jpegModelR5MixerE3row(%rip)
	jne	.LBB40_335
# BB#334:
	movl	(%rcx), %eax
	movl	%eax, 4(%rcx)
	movl	$1, 8(%rcx)
.LBB40_335:
	cmpl	$0, _ZZ9jpegModelR5MixerE4jpeg(%rip)
	je	.LBB40_212
# BB#336:
	movl	_ZZ9jpegModelR5MixerE4data(%rip), %eax
	testl	%eax, %eax
	je	.LBB40_212
# BB#337:
	cmpl	$1, bpos(%rip)
	movl	$1, %eax
	movl	$1, %ecx
	adcl	$0, %ecx
	movl	pos(%rip), %edx
	subl	%ecx, %edx
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%edx, %ecx
	movq	buf+16(%rip), %rdx
	movslq	%ecx, %rcx
	cmpb	$-1, (%rdx,%rcx)
	je	.LBB40_345
# BB#338:
	movb	_ZGVZ9jpegModelR5MixerE1t(%rip), %al
	testb	%al, %al
	jne	.LBB40_348
# BB#339:
	movl	$_ZGVZ9jpegModelR5MixerE1t, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_348
# BB#340:
	movb	level(%rip), %cl
	movl	$65536, %ebx            # imm = 0x10000
	shll	%cl, %ebx
	leal	(%rbx,%rbx,8), %eax
	movl	%eax, _ZZ9jpegModelR5MixerE1t+4(%rip)
	movl	%eax, _ZZ9jpegModelR5MixerE1t(%rip)
	testl	%ebx, %ebx
	jle	.LBB40_346
# BB#341:
	addl	$64, %eax
	movl	programChecker(%rip), %ecx
	addl	%eax, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB40_343
# BB#342:
	movl	%ecx, programChecker+4(%rip)
.LBB40_343:                             # %_ZN14ProgramChecker5allocEi.exit.i.i.i774
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE1t+8(%rip)
	testq	%rax, %rax
	je	.LBB40_462
# BB#344:
	movl	%eax, %ecx
	andl	$63, %ecx
	negq	%rcx
	leaq	64(%rax,%rcx), %rax
	movq	%rax, _ZZ9jpegModelR5MixerE1t+16(%rip)
	jmp	.LBB40_347
.LBB40_345:
	movslq	96(%r12), %rcx
	leal	1(%rcx), %edx
	movl	%edx, 96(%r12)
	movq	32(%r12), %rdx
	movw	$128, (%rdx,%rcx,2)
	jmp	.LBB40_408
.LBB40_346:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, _ZZ9jpegModelR5MixerE1t+8(%rip)
.LBB40_347:
	decl	%ebx
	movl	%ebx, _ZZ9jpegModelR5MixerE1t+24(%rip)
	movl	$_ZN2BHILi9EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE1t, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE1t, %edi
	callq	__cxa_guard_release
.LBB40_348:
	movb	_ZGVZ9jpegModelR5MixerE3cxt(%rip), %al
	testb	%al, %al
	jne	.LBB40_354
# BB#349:
	movl	$_ZGVZ9jpegModelR5MixerE3cxt, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_354
# BB#350:
	movabsq	$120259084316, %rax     # imm = 0x1C0000001C
	movq	%rax, _ZZ9jpegModelR5MixerE3cxt(%rip)
	movl	programChecker(%rip), %eax
	addl	$112, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_352
# BB#351:
	movl	%eax, programChecker+4(%rip)
.LBB40_352:                             # %_ZN14ProgramChecker5allocEi.exit.i.i772
	movl	$112, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE3cxt+8(%rip)
	testq	%rax, %rax
	je	.LBB40_458
# BB#353:
	movq	%rax, _ZZ9jpegModelR5MixerE3cxt+16(%rip)
	movl	$_ZN5ArrayIjLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE3cxt, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE3cxt, %edi
	callq	__cxa_guard_release
.LBB40_354:
	movb	_ZGVZ9jpegModelR5MixerE2cp(%rip), %al
	testb	%al, %al
	jne	.LBB40_360
# BB#355:
	movl	$_ZGVZ9jpegModelR5MixerE2cp, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_360
# BB#356:
	movabsq	$120259084316, %rax     # imm = 0x1C0000001C
	movq	%rax, _ZZ9jpegModelR5MixerE2cp(%rip)
	movl	$224, %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_358
# BB#357:
	movl	%eax, programChecker+4(%rip)
.LBB40_358:                             # %_ZN14ProgramChecker5allocEi.exit.i.i770
	movl	$224, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE2cp+8(%rip)
	testq	%rax, %rax
	je	.LBB40_460
# BB#359:
	movq	%rax, _ZZ9jpegModelR5MixerE2cp+16(%rip)
	movl	$_ZN5ArrayIPhLi0EED2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE2cp, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE2cp, %edi
	callq	__cxa_guard_release
.LBB40_360:
	movb	_ZGVZ9jpegModelR5MixerE2sm(%rip), %al
	testb	%al, %al
	jne	.LBB40_368
# BB#361:
	movl	$_ZGVZ9jpegModelR5MixerE2sm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_368
# BB#362:                               # %.preheader849.preheader
	movl	programChecker+4(%rip), %r14d
	movl	$1024, %ebx             # imm = 0x400
	addl	programChecker(%rip), %ebx
	xorl	%ebp, %ebp
.LBB40_363:                             # %.preheader849
                                        # =>This Inner Loop Header: Depth=1
	movdqa	.LCPI40_0(%rip), %xmm0  # xmm0 = [256,0,256,256]
	movdqa	%xmm0, _ZZ9jpegModelR5MixerE2sm(%rbp)
	movl	%ebx, programChecker(%rip)
	cmpl	%r14d, %ebx
	jle	.LBB40_365
# BB#364:                               #   in Loop: Header=BB40_363 Depth=1
	movl	%ebx, programChecker+4(%rip)
	movl	%ebx, %r14d
.LBB40_365:                             # %_ZN14ProgramChecker5allocEi.exit.i.i.i765
                                        #   in Loop: Header=BB40_363 Depth=1
	movl	$1024, %edi             # imm = 0x400
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ9jpegModelR5MixerE2sm+16(%rbp)
	testq	%rax, %rax
	je	.LBB40_456
# BB#366:                               # %vector.body1156
                                        #   in Loop: Header=BB40_363 Depth=1
	movq	%rax, _ZZ9jpegModelR5MixerE2sm+24(%rbp)
	movdqa	.LCPI40_1(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	movdqu	%xmm0, (%rax)
	movdqu	%xmm0, 16(%rax)
	movdqu	%xmm0, 32(%rax)
	movdqu	%xmm0, 48(%rax)
	movdqu	%xmm0, 64(%rax)
	movdqu	%xmm0, 80(%rax)
	movdqu	%xmm0, 96(%rax)
	movdqu	%xmm0, 112(%rax)
	movdqu	%xmm0, 128(%rax)
	movdqu	%xmm0, 144(%rax)
	movdqu	%xmm0, 160(%rax)
	movdqu	%xmm0, 176(%rax)
	movdqu	%xmm0, 192(%rax)
	movdqu	%xmm0, 208(%rax)
	movdqu	%xmm0, 224(%rax)
	movdqu	%xmm0, 240(%rax)
	movdqu	%xmm0, 256(%rax)
	movdqu	%xmm0, 272(%rax)
	movdqu	%xmm0, 288(%rax)
	movdqu	%xmm0, 304(%rax)
	movdqu	%xmm0, 320(%rax)
	movdqu	%xmm0, 336(%rax)
	movdqu	%xmm0, 352(%rax)
	movdqu	%xmm0, 368(%rax)
	movdqu	%xmm0, 384(%rax)
	movdqu	%xmm0, 400(%rax)
	movdqu	%xmm0, 416(%rax)
	movdqu	%xmm0, 432(%rax)
	movdqu	%xmm0, 448(%rax)
	movdqu	%xmm0, 464(%rax)
	movdqu	%xmm0, 480(%rax)
	movdqu	%xmm0, 496(%rax)
	movdqu	%xmm0, 512(%rax)
	movdqu	%xmm0, 528(%rax)
	movdqu	%xmm0, 544(%rax)
	movdqu	%xmm0, 560(%rax)
	movdqu	%xmm0, 576(%rax)
	movdqu	%xmm0, 592(%rax)
	movdqu	%xmm0, 608(%rax)
	movdqu	%xmm0, 624(%rax)
	movdqu	%xmm0, 640(%rax)
	movdqu	%xmm0, 656(%rax)
	movdqu	%xmm0, 672(%rax)
	movdqu	%xmm0, 688(%rax)
	movdqu	%xmm0, 704(%rax)
	movdqu	%xmm0, 720(%rax)
	movdqu	%xmm0, 736(%rax)
	movdqu	%xmm0, 752(%rax)
	movdqu	%xmm0, 768(%rax)
	movdqu	%xmm0, 784(%rax)
	movdqu	%xmm0, 800(%rax)
	movdqu	%xmm0, 816(%rax)
	movdqu	%xmm0, 832(%rax)
	movdqu	%xmm0, 848(%rax)
	movdqu	%xmm0, 864(%rax)
	movdqu	%xmm0, 880(%rax)
	movdqu	%xmm0, 896(%rax)
	movdqu	%xmm0, 912(%rax)
	movdqu	%xmm0, 928(%rax)
	movdqu	%xmm0, 944(%rax)
	movdqu	%xmm0, 960(%rax)
	movdqu	%xmm0, 976(%rax)
	movdqu	%xmm0, 992(%rax)
	movdqu	%xmm0, 1008(%rax)
	addq	$32, %rbp
	addl	$1024, %ebx             # imm = 0x400
	cmpq	$896, %rbp              # imm = 0x380
	jne	.LBB40_363
# BB#367:
	movl	$__cxx_global_array_dtor.13, %edi
	xorl	%esi, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE2sm, %edi
	callq	__cxa_guard_release
.LBB40_368:
	movb	_ZGVZ9jpegModelR5MixerE2m1(%rip), %al
	testb	%al, %al
	jne	.LBB40_372
# BB#369:
	movl	$_ZGVZ9jpegModelR5MixerE2m1, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_372
# BB#370:
.Ltmp183:
	movl	$_ZZ9jpegModelR5MixerE2m1, %edi
	movl	$32, %esi
	movl	$770, %edx              # imm = 0x302
	movl	$3, %ecx
	xorl	%r8d, %r8d
	callq	_ZN5MixerC2Eiiii
.Ltmp184:
# BB#371:
	movl	$_ZN5MixerD2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE2m1, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE2m1, %edi
	callq	__cxa_guard_release
.LBB40_372:
	movb	_ZGVZ9jpegModelR5MixerE2a1(%rip), %al
	testb	%al, %al
	jne	.LBB40_376
# BB#373:
	movl	$_ZGVZ9jpegModelR5MixerE2a1, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_376
# BB#374:
.Ltmp186:
	movl	$_ZZ9jpegModelR5MixerE2a1, %edi
	movl	$32768, %esi            # imm = 0x8000
	callq	_ZN3APMC2Ei
.Ltmp187:
# BB#375:
	movl	$_ZN8StateMapD2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE2a1, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE2a1, %edi
	callq	__cxa_guard_release
.LBB40_376:
	movb	_ZGVZ9jpegModelR5MixerE2a2(%rip), %al
	testb	%al, %al
	jne	.LBB40_380
# BB#377:
	movl	$_ZGVZ9jpegModelR5MixerE2a2, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB40_380
# BB#378:
.Ltmp189:
	movl	$_ZZ9jpegModelR5MixerE2a2, %edi
	movl	$65536, %esi            # imm = 0x10000
	callq	_ZN3APMC2Ei
.Ltmp190:
# BB#379:
	movl	$_ZN8StateMapD2Ev, %edi
	movl	$_ZZ9jpegModelR5MixerE2a2, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ9jpegModelR5MixerE2a2, %edi
	callq	__cxa_guard_release
.LBB40_380:
	movq	_ZZ9jpegModelR5MixerE2cp+16(%rip), %rax
	cmpq	$0, 216(%rax)
	je	.LBB40_384
# BB#381:                               # %.preheader845.preheader
	movq	(%rax), %rax
	movzbl	(%rax), %ecx
	movslq	y(%rip), %rdx
	movb	_ZL11State_table(%rdx,%rcx,4), %cl
	movb	%cl, (%rax)
	xorl	%eax, %eax
	jmp	.LBB40_383
	.p2align	4, 0x90
.LBB40_382:                             # %.preheader845..preheader845_crit_edge.1
                                        #   in Loop: Header=BB40_383 Depth=1
	movq	_ZZ9jpegModelR5MixerE2cp+16(%rip), %rcx
	movq	16(%rcx,%rax,8), %rcx
	movzbl	(%rcx), %edx
	movslq	y(%rip), %rsi
	movzbl	_ZL11State_table(%rsi,%rdx,4), %edx
	movb	%dl, (%rcx)
	addq	$2, %rax
.LBB40_383:                             # %.preheader845..preheader845_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	_ZZ9jpegModelR5MixerE2cp+16(%rip), %rcx
	movq	8(%rcx,%rax,8), %rcx
	movzbl	(%rcx), %edx
	movslq	y(%rip), %rsi
	movzbl	_ZL11State_table(%rsi,%rdx,4), %edx
	movb	%dl, (%rcx)
	cmpq	$26, %rax
	jne	.LBB40_382
.LBB40_384:                             # %.loopexit846
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movslq	_ZZ9jpegModelR5MixerE2m1+88(%rip), %r8
	testq	%r8, %r8
	jle	.LBB40_396
# BB#385:                               # %.lr.ph.i
	movl	_ZZ9jpegModelR5MixerE2m1+96(%rip), %r15d
	addl	$7, %r15d
	andl	$-8, %r15d
	testl	%r15d, %r15d
	jle	.LBB40_396
# BB#386:                               # %.lr.ph.i.split.us.preheader
	movl	y(%rip), %r10d
	shll	$12, %r10d
	movq	_ZZ9jpegModelR5MixerE2m1+120(%rip), %r13
	movq	_ZZ9jpegModelR5MixerE2m1+32(%rip), %r9
	movq	_ZZ9jpegModelR5MixerE2m1+80(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movslq	_ZZ9jpegModelR5MixerE2m1(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	_ZZ9jpegModelR5MixerE2m1+56(%rip), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%rax,%r15,2), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	(%r9,%r15,2), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$-32768, %r11d          # imm = 0x8000
	movw	$32767, %r14w           # imm = 0x7FFF
	movdqa	.LCPI40_2(%rip), %xmm8  # xmm8 = [1,1,1,1]
	movdqa	.LCPI40_3(%rip), %xmm9  # xmm9 = [4294934528,4294934528,4294934528,4294934528]
	movdqa	.LCPI40_4(%rip), %xmm2  # xmm2 = [32767,32767,32767,32767]
	.p2align	4, 0x90
.LBB40_387:                             # %.lr.ph.i.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_394 Depth 2
                                        #     Child Loop BB40_392 Depth 2
	movl	%r10d, %eax
	subl	(%r13,%rbx,4), %eax
	je	.LBB40_395
# BB#388:                               # %.lr.ph.preheader.i.i.us
                                        #   in Loop: Header=BB40_387 Depth=1
	leal	(,%rax,8), %ecx
	subl	%eax, %ecx
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	(%rax,%rbx,4), %rax
	imulq	16(%rsp), %rax          # 8-byte Folded Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,2), %rsi
	cmpl	$7, %r15d
	jbe	.LBB40_391
# BB#389:                               # %vector.memcheck
                                        #   in Loop: Header=BB40_387 Depth=1
	cmpq	32(%rsp), %rsi          # 8-byte Folded Reload
	jae	.LBB40_393
# BB#390:                               # %vector.memcheck
                                        #   in Loop: Header=BB40_387 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,2), %rax
	cmpq	%rax, %r9
	jae	.LBB40_393
.LBB40_391:                             #   in Loop: Header=BB40_387 Depth=1
	movq	%r9, %rdx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB40_392:                             # %.lr.ph.i.i.us
                                        #   Parent Loop BB40_387 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rsi), %ebp
	movswl	(%rdx), %edi
	imull	%ecx, %edi
	sarl	$15, %edi
	incl	%edi
	sarl	%edi
	addl	%ebp, %edi
	cmpl	$-32769, %edi           # imm = 0xFFFF7FFF
	cmovlel	%r11d, %edi
	cmpl	$32767, %edi            # imm = 0x7FFF
	cmovgew	%r14w, %di
	movw	%di, (%rsi)
	addq	$2, %rdx
	addq	$2, %rsi
	decq	%rax
	jne	.LBB40_392
	jmp	.LBB40_395
.LBB40_393:                             # %vector.ph1182
                                        #   in Loop: Header=BB40_387 Depth=1
	movd	%ecx, %xmm3
	pshufd	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0]
	movq	%r15, %rcx
	movq	%r9, %r12
	.p2align	4, 0x90
.LBB40_394:                             # %vector.body1169
                                        #   Parent Loop BB40_387 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %xmm4          # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	movq	(%rsi), %xmm5           # xmm5 = mem[0],zero
	punpcklwd	%xmm5, %xmm5    # xmm5 = xmm5[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm5
	movq	8(%r12), %xmm6          # xmm6 = mem[0],zero
	punpcklwd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0],xmm0[1],xmm6[1],xmm0[2],xmm6[2],xmm0[3],xmm6[3]
	psrad	$16, %xmm0
	movq	(%r12), %xmm6           # xmm6 = mem[0],zero
	punpcklwd	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1],xmm7[2],xmm6[2],xmm7[3],xmm6[3]
	psrad	$16, %xmm7
	movdqa	%xmm3, %xmm6
	pmuludq	%xmm7, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm7, %xmm7      # xmm7 = xmm7[1,1,3,3]
	pshufd	$245, %xmm3, %xmm1      # xmm1 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	movdqa	%xmm3, %xmm7
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	psrad	$15, %xmm7
	psrad	$15, %xmm6
	paddd	%xmm8, %xmm6
	paddd	%xmm8, %xmm7
	psrad	$1, %xmm7
	psrad	$1, %xmm6
	paddd	%xmm5, %xmm6
	paddd	%xmm4, %xmm7
	movdqa	%xmm7, %xmm0
	pcmpgtd	%xmm9, %xmm0
	movdqa	%xmm6, %xmm1
	pcmpgtd	%xmm9, %xmm1
	pand	%xmm1, %xmm6
	pandn	%xmm9, %xmm1
	por	%xmm6, %xmm1
	pand	%xmm0, %xmm7
	pandn	%xmm9, %xmm0
	por	%xmm7, %xmm0
	movdqa	%xmm2, %xmm4
	pcmpgtd	%xmm0, %xmm4
	movdqa	%xmm2, %xmm5
	pcmpgtd	%xmm1, %xmm5
	pand	%xmm5, %xmm1
	pandn	%xmm2, %xmm5
	por	%xmm1, %xmm5
	pand	%xmm4, %xmm0
	pandn	%xmm2, %xmm4
	por	%xmm0, %xmm4
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	packssdw	%xmm4, %xmm5
	movdqu	%xmm5, (%rsi)
	addq	$16, %rsi
	addq	$16, %r12
	addq	$-8, %rcx
	jne	.LBB40_394
	.p2align	4, 0x90
.LBB40_395:                             # %_Z5trainPsS_ii.exit.i.us
                                        #   in Loop: Header=BB40_387 Depth=1
	incq	%rbx
	cmpq	%r8, %rbx
	jne	.LBB40_387
.LBB40_396:                             # %_ZN5Mixer6updateEv.exit
	movq	$0, _ZZ9jpegModelR5MixerE2m1+88(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE2m1+96(%rip)
	movl	_ZZ9jpegModelR5MixerE6mcupos(%rip), %eax
	movl	%eax, %ecx
	sarl	$6, %ecx
	movq	_ZZ9jpegModelR5MixerE5color+16(%rip), %rdx
	movslq	%ecx, %rcx
	movl	(%rdx,%rcx,4), %r8d
	andl	$63, %eax
	movl	%r8d, %r14d
	shll	$6, %r14d
	orl	%eax, %r14d
	movl	_ZZ9jpegModelR5MixerE8huffcode(%rip), %r9d
	xorl	%ebp, %ebp
	testl	%r8d, %r8d
	sete	%bl
	movl	_ZZ9jpegModelR5MixerE8huffbits(%rip), %edx
	leal	1(%rdx), %ecx
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	movl	_ZZ9jpegModelR5MixerE7hbcount(%rip), %esi
	leal	1(%rsi), %ecx
	testl	%edx, %edx
	cmovel	%edx, %ecx
	xorl	%edx, %edx
	cmpl	$1, %esi
	cmovgl	%edx, %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE7hbcount(%rip)
	cmpl	$256, %r14d             # imm = 0x100
	jae	.LBB40_211
# BB#397:
	movb	%bl, %bpl
	leal	(%rbp,%r9,2), %r12d
	orl	%edi, %r12d
	testl	%ecx, %ecx
	jne	.LBB40_399
# BB#398:
	movl	%eax, %eax
	movzbl	_ZZ9jpegModelR5MixerE3zzu(%rax), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movzbl	_ZZ9jpegModelR5MixerE3zzv(%rax), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	_ZZ9jpegModelR5MixerE8adv_pred+16(%rip), %rbx
	movl	8(%rbx), %edx
	imull	$30005491, %r12d, %r9d  # imm = 0x1C9D8F3
	imull	$50004239, %r14d, %r13d # imm = 0x2FB010F
	imull	$70004807, %edx, %eax   # imm = 0x42C3047
	addl	%r9d, %r13d
	leal	90000480(%rax,%r13), %esi
	movl	%r12d, 24(%rsp)         # 4-byte Spill
	movl	24(%rsp), %r12d         # 4-byte Reload
	shrl	$3, %r12d
	movl	%r14d, %r15d
	shrl	$4, %r15d
	shrl	$5, %edx
	movl	%r12d, %eax
	xorl	%r15d, %eax
	xorl	$67108863, %eax         # imm = 0x3FFFFFF
	xorl	%eax, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	movq	_ZZ9jpegModelR5MixerE3cxt+16(%rip), %r10
	movl	%esi, (%r10)
	movl	(%rbx), %edx
	imull	$70004807, %edx, %esi   # imm = 0x42C3047
	leal	290003459(%rsi,%r13), %esi
	shrl	$5, %edx
	xorl	%eax, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	movl	%esi, 4(%r10)
	movl	4(%rbx), %edx
	imull	$70004807, %edx, %esi   # imm = 0x42C3047
	leal	490006438(%rsi,%r13), %esi
	shrl	$5, %edx
	xorl	%eax, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	movl	%esi, 8(%r10)
	movl	_ZZ9jpegModelR5MixerE3rs1(%rip), %edx
	movl	8(%rbx), %edi
	imull	$50004239, %edx, %esi   # imm = 0x2FB010F
	imull	$70004807, %edi, %eax   # imm = 0x42C3047
	addl	%r9d, %esi
	leal	690009417(%rax,%rsi), %ebp
	shrl	$4, %edx
	shrl	$5, %edi
	movl	%r12d, %eax
	xorl	$67108862, %eax         # imm = 0x3FFFFFE
	xorl	%eax, %edx
	xorl	%edx, %edi
	xorl	%ebp, %edi
	shrl	$9, %ebp
	xorl	%edi, %ebp
	movl	%ebp, 12(%r10)
	movl	(%rbx), %edi
	imull	$70004807, %edi, %ebp   # imm = 0x42C3047
	leal	890012396(%rbp,%rsi), %ebp
	shrl	$5, %edi
	xorl	%edx, %edi
	xorl	%ebp, %edi
	shrl	$9, %ebp
	xorl	%edi, %ebp
	movl	%ebp, 16(%r10)
	movl	4(%rbx), %edi
	imull	$70004807, %edi, %ebp   # imm = 0x42C3047
	leal	1090015375(%rbp,%rsi), %esi
	shrl	$5, %edi
	xorl	%edx, %edi
	xorl	%esi, %edi
	shrl	$9, %esi
	xorl	%edi, %esi
	movl	%esi, 20(%r10)
	movl	(%rbx), %edx
	movl	8(%rbx), %esi
	imull	$50004239, %esi, %edi   # imm = 0x2FB010F
	imull	$70004807, %edx, %ebp   # imm = 0x42C3047
	addl	%r9d, %edi
	leal	1290018354(%rbp,%rdi), %edi
	shrl	$4, %esi
	shrl	$5, %edx
	xorl	%esi, %edx
	xorl	%eax, %edx
	xorl	%edi, %edx
	shrl	$9, %edi
	xorl	%edx, %edi
	movl	%edi, 24(%r10)
	movl	_ZZ9jpegModelR5MixerE4cpos(%rip), %esi
	movl	_ZZ9jpegModelR5MixerE7mcusize(%rip), %eax
	imull	_ZZ9jpegModelR5MixerE5width(%rip), %eax
	movl	%esi, %edi
	subl	%eax, %edi
	movl	_ZZ9jpegModelR5MixerE4cbuf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %edi
	movq	_ZZ9jpegModelR5MixerE4cbuf+16(%rip), %rdx
	movslq	%edi, %rax
	movzbl	(%rdx,%rax), %eax
	movq	_ZZ9jpegModelR5MixerE8adv_pred+16(%rip), %r10
	movl	12(%r10), %edi
	imull	$50004239, %eax, %ebp   # imm = 0x2FB010F
	imull	$70004807, %edi, %r11d  # imm = 0x42C3047
	addl	%r9d, %ebp
	leal	1490021333(%r11,%rbp), %r11d
	shrl	$4, %eax
	shrl	$5, %edi
	movl	%r12d, %ebp
	xorl	$67108861, %ebp         # imm = 0x3FFFFFD
	xorl	%eax, %edi
	xorl	%ebp, %edi
	xorl	%r11d, %edi
	shrl	$9, %r11d
	xorl	%edi, %r11d
	movq	_ZZ9jpegModelR5MixerE3cxt+16(%rip), %rdi
	movl	%r11d, 28(%rdi)
	movl	_ZZ9jpegModelR5MixerE6mcupos(%rip), %eax
	sarl	$6, %eax
	movq	_ZZ9jpegModelR5MixerE2ls+16(%rip), %r11
	cltq
	subl	(%r11,%rax,4), %esi
	movl	%r12d, %r11d
	xorl	$2, %r11d
	andl	%ebx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %edx
	movl	12(%r10), %esi
	imull	$50004239, %edx, %ebx   # imm = 0x2FB010F
	imull	$70004807, %esi, %eax   # imm = 0x42C3047
	addl	%r9d, %ebx
	leal	1690024312(%rax,%rbx), %eax
	shrl	$4, %edx
	shrl	$5, %esi
	xorl	%ebp, %esi
	xorl	%edx, %esi
	xorl	%eax, %esi
	shrl	$9, %eax
	xorl	%esi, %eax
	movl	%eax, 32(%rdi)
	movq	_ZZ9jpegModelR5MixerE3lcp+16(%rip), %rsi
	movl	(%rsi), %eax
	movl	4(%rsi), %edx
	movl	4(%r10), %r10d
	imull	$50004239, %eax, %ebx   # imm = 0x2FB010F
	addl	%r9d, %ebx
	imull	$70004807, %edx, %ebp   # imm = 0x42C3047
	addl	%ebx, %ebp
	imull	$110002499, %r10d, %ebx # imm = 0x68E8143
	leal	2000029790(%rbx,%rbp), %ebx
	shrl	$4, %eax
	shrl	$5, %edx
	shrl	$6, %r10d
	xorl	%eax, %edx
	xorl	%r10d, %edx
	xorl	%r11d, %edx
	xorl	%ebx, %edx
	shrl	$9, %ebx
	xorl	%edx, %ebx
	movl	%ebx, 36(%rdi)
	movl	(%rsi), %eax
	movl	4(%rsi), %edx
	imull	$50004239, %eax, %ebp   # imm = 0x2FB010F
	imull	$70004807, %edx, %ebx   # imm = 0x42C3047
	imull	$110002499, %r14d, %ecx # imm = 0x68E8143
	addl	%r9d, %ecx
	addl	%ebp, %ecx
	leal	-2094934527(%rbx,%rcx), %ecx
	shrl	$4, %eax
	shrl	$5, %edx
	andl	$67108863, %r8d         # imm = 0x3FFFFFF
	xorl	%eax, %r8d
	xorl	%edx, %r8d
	xorl	%r11d, %r8d
	xorl	%ecx, %r8d
	shrl	$9, %ecx
	xorl	%r8d, %ecx
	movl	%ecx, 40(%rdi)
	movl	(%rsi), %edx
	movslq	8(%rsi), %rax
	imulq	$1431655766, %rax, %rcx # imm = 0x55555556
	movq	%rcx, %rax
	shrq	$63, %rax
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	8(%rsp), %ebx           # 4-byte Reload
	imull	$50004239, %ebx, %r10d  # imm = 0x2FB010F
	imull	$70004807, %edx, %eax   # imm = 0x42C3047
	addl	%r9d, %r10d
	addl	%r10d, %eax
	imull	$110002499, %ecx, %ebp  # imm = 0x68E8143
	leal	-1894931548(%rbp,%rax), %ebp
	shrl	$4, %ebx
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	shrl	$5, %edx
	shrl	$6, %ecx
	movl	%r12d, %eax
	xorl	$3, %eax
	xorl	%ebx, %edx
	xorl	%ecx, %edx
	xorl	%eax, %edx
	xorl	%ebp, %edx
	shrl	$9, %ebp
	xorl	%edx, %ebp
	movl	%ebp, 44(%rdi)
	movl	4(%rsi), %edx
	movslq	12(%rsi), %rcx
	imulq	$1431655766, %rcx, %rcx # imm = 0x55555556
	movq	%rcx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rcx
	addl	%esi, %ecx
	movl	16(%rsp), %ebp          # 4-byte Reload
	imull	$50004239, %ebp, %r11d  # imm = 0x2FB010F
	imull	$70004807, %edx, %esi   # imm = 0x42C3047
	addl	%r9d, %r11d
	addl	%r11d, %esi
	imull	$110002499, %ecx, %edi  # imm = 0x68E8143
	leal	-1694928569(%rdi,%rsi), %esi
	movl	%ebp, %edi
	shrl	$4, %edi
	movl	%edi, 16(%rsp)          # 4-byte Spill
	shrl	$5, %edx
	shrl	$6, %ecx
	xorl	%edi, %edx
	xorl	%ecx, %edx
	xorl	%eax, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	movq	_ZZ9jpegModelR5MixerE3cxt+16(%rip), %rbx
	movl	%esi, 48(%rbx)
	movl	_ZZ9jpegModelR5MixerE6mcupos(%rip), %edi
	movl	%edi, %ecx
	sarl	$2, %ecx
	movl	%edi, %edx
	andl	$63, %edx
	cmpl	$3, %edx
	movl	$3, %esi
	cmovbl	%edx, %esi
	imull	$50004239, %ecx, %edx   # imm = 0x2FB010F
	imull	$70004807, %esi, %esi   # imm = 0x42C3047
	addl	%r9d, %edx
	leal	-1604928089(%rsi,%rdx), %edx
	shrl	$4, %ecx
	xorl	%r12d, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	xorl	$67108860, %edx         # imm = 0x3FFFFFC
	movl	%edx, 52(%rbx)
	movl	_ZZ9jpegModelR5MixerE6column(%rip), %esi
	movl	%esi, %ecx
	sarl	%ecx
	imull	$70004807, %ecx, %edx   # imm = 0x42C3047
	leal	-1404925110(%rdx,%r13), %edx
	shrl	$5, %ecx
	xorl	%r15d, %ecx
	xorl	%eax, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	xorl	$67108863, %edx         # imm = 0x3FFFFFF
	movl	%edx, 56(%rbx)
	sarl	$2, %esi
	movq	_ZZ9jpegModelR5MixerE3lcp+16(%rip), %r15
	movslq	8(%r15), %rax
	imulq	$1431655766, %rax, %rdx # imm = 0x55555556
	movq	%rdx, %rax
	shrq	$63, %rax
	shrq	$32, %rdx
	addl	%eax, %edx
	shll	$8, %edx
	addl	(%r15), %edx
	movslq	12(%r15), %rax
	imulq	$1431655766, %rax, %rbp # imm = 0x55555556
	movq	%rbp, %rax
	shrq	$63, %rax
	shrq	$32, %rbp
	addl	%eax, %ebp
	shll	$8, %ebp
	addl	4(%r15), %ebp
	imull	$50004239, %esi, %eax   # imm = 0x2FB010F
	addl	%r9d, %eax
	imull	$70004807, %edx, %ecx   # imm = 0x42C3047
	addl	%eax, %ecx
	imull	$110002499, %ebp, %eax  # imm = 0x68E8143
	leal	-1094919632(%rax,%rcx), %ecx
	shrl	$4, %esi
	shrl	$5, %edx
	shrl	$6, %ebp
	movl	%r12d, %eax
	xorl	$4, %eax
	xorl	%eax, %esi
	xorl	%edx, %esi
	xorl	%ebp, %esi
	xorl	%ecx, %esi
	shrl	$9, %ecx
	xorl	%esi, %ecx
	movl	%ecx, 60(%rbx)
	movl	_ZZ9jpegModelR5MixerE4ssum(%rip), %ecx
	sarl	$4, %ecx
	imull	$50004239, %ecx, %edx   # imm = 0x2FB010F
	imull	$70004807, %r14d, %esi  # imm = 0x42C3047
	addl	%r9d, %esi
	leal	-1004919152(%rdx,%rsi), %edx
	shrl	$4, %ecx
	movl	%r14d, %ebp
	shrl	$5, %ebp
	xorl	%eax, %ebp
	xorl	$67108863, %ebp         # imm = 0x3FFFFFF
	xorl	%ebp, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	movl	%edx, 64(%rbx)
	movl	_ZZ9jpegModelR5MixerE3rs1(%rip), %ecx
	imull	$50004239, %ecx, %edx   # imm = 0x2FB010F
	leal	-804916173(%rdx,%rsi), %edx
	shrl	$4, %ecx
	xorl	%ebp, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	movl	%edx, 68(%rbx)
	sarl	$3, %edi
	movl	_ZZ9jpegModelR5MixerE5ssum3(%rip), %ecx
	sarl	$3, %ecx
	movq	_ZZ9jpegModelR5MixerE8adv_pred+16(%rip), %r8
	movl	12(%r8), %edx
	imull	$50004239, %edi, %esi   # imm = 0x2FB010F
	addl	%r9d, %esi
	imull	$70004807, %ecx, %ebp   # imm = 0x42C3047
	addl	%esi, %ebp
	imull	$110002499, %edx, %esi  # imm = 0x68E8143
	leal	-494910695(%rsi,%rbp), %ebp
	shrl	$4, %edi
	shrl	$5, %ecx
	shrl	$6, %edx
	xorl	%edi, %ecx
	xorl	%edx, %ecx
	xorl	%eax, %ecx
	xorl	%ebp, %ecx
	shrl	$9, %ebp
	xorl	%ecx, %ebp
	movq	_ZZ9jpegModelR5MixerE3cxt+16(%rip), %r13
	movl	%ebp, 72(%r13)
	movslq	(%r15), %rax
	imulq	$1431655766, %rax, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	movslq	4(%r15), %rcx
	imulq	$1431655766, %rcx, %rdx # imm = 0x55555556
	movq	%rdx, %rcx
	shrq	$63, %rcx
	shrq	$32, %rdx
	addl	%ecx, %edx
	movl	20(%r8), %ecx
	imull	$50004239, %eax, %edi   # imm = 0x2FB010F
	addl	%r9d, %edi
	imull	$70004807, %edx, %ebp   # imm = 0x42C3047
	addl	%edi, %ebp
	imull	$110002499, %ecx, %edi  # imm = 0x68E8143
	leal	-294907716(%rdi,%rbp), %edi
	shrl	$4, %eax
	shrl	$5, %edx
	shrl	$6, %ecx
	xorl	%eax, %edx
	xorl	%r12d, %edx
	xorl	%ecx, %edx
	xorl	%edi, %edx
	shrl	$9, %edi
	xorl	%edx, %edi
	xorl	$5, %edi
	movl	%edi, 76(%r13)
	movl	_ZZ9jpegModelR5MixerE4cpos(%rip), %ebx
	movl	_ZZ9jpegModelR5MixerE7mcusize(%rip), %ecx
	imull	_ZZ9jpegModelR5MixerE5width(%rip), %ecx
	movl	%ebx, %eax
	subl	%ecx, %eax
	movl	_ZZ9jpegModelR5MixerE4cbuf(%rip), %edx
	decl	%edx
	andl	%edx, %eax
	movq	_ZZ9jpegModelR5MixerE4cbuf+16(%rip), %rdi
	cltq
	movzbl	(%rdi,%rax), %eax
	movl	24(%r8), %ecx
	imull	$50004239, %eax, %ebp   # imm = 0x2FB010F
	imull	$70004807, %ecx, %esi   # imm = 0x42C3047
	addl	%r9d, %ebp
	leal	-204907236(%rsi,%rbp), %esi
	shrl	$4, %eax
	shrl	$5, %ecx
	xorl	%eax, %ecx
	movl	%r12d, %ebp
	xorl	$67108858, %ebp         # imm = 0x3FFFFFA
	xorl	%ebp, %ecx
	xorl	%esi, %ecx
	shrl	$9, %esi
	xorl	%ecx, %esi
	movl	%esi, 80(%r13)
	movl	_ZZ9jpegModelR5MixerE6mcupos(%rip), %eax
	sarl	$6, %eax
	movq	_ZZ9jpegModelR5MixerE2ls+16(%rip), %rcx
	cltq
	subl	(%rcx,%rax,4), %ebx
	andl	%edx, %ebx
	movslq	%ebx, %rax
	movzbl	(%rdi,%rax), %eax
	movl	16(%r8), %ecx
	imull	$50004239, %eax, %edx   # imm = 0x2FB010F
	imull	$70004807, %ecx, %esi   # imm = 0x42C3047
	addl	%r9d, %edx
	leal	-4904257(%rsi,%rdx), %edx
	shrl	$4, %eax
	shrl	$5, %ecx
	xorl	%ebp, %ecx
	xorl	%eax, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	movl	%edx, 84(%r13)
	movq	_ZZ9jpegModelR5MixerE8adv_pred+16(%rip), %rdx
	movl	8(%rdx), %ecx
	imull	$50004239, %ecx, %eax   # imm = 0x2FB010F
	leal	125093915(%r9,%rax), %esi
	shrl	$4, %ecx
	movl	%r12d, %eax
	xorl	$67108869, %eax         # imm = 0x4000005
	xorl	%eax, %ecx
	xorl	%esi, %ecx
	shrl	$9, %esi
	xorl	%ecx, %esi
	movl	%esi, 88(%r13)
	movl	(%rdx), %ecx
	imull	$50004239, %ecx, %esi   # imm = 0x2FB010F
	leal	125093915(%r9,%rsi), %edi
	shrl	$4, %ecx
	xorl	%eax, %ecx
	xorl	%edi, %ecx
	shrl	$9, %edi
	xorl	%ecx, %edi
	movq	_ZZ9jpegModelR5MixerE3cxt+16(%rip), %rsi
	movl	%edi, 92(%rsi)
	movl	4(%rdx), %ecx
	imull	$50004239, %ecx, %edi   # imm = 0x2FB010F
	leal	125093915(%r9,%rdi), %edi
	shrl	$4, %ecx
	xorl	%eax, %ecx
	xorl	%edi, %ecx
	shrl	$9, %edi
	xorl	%ecx, %edi
	movl	%edi, 96(%rsi)
	movq	_ZZ9jpegModelR5MixerE3lcp+16(%rip), %rax
	movl	4(%rax), %ecx
	movl	24(%rdx), %edi
	imull	$70004807, %ecx, %ebp   # imm = 0x42C3047
	addl	%r11d, %ebp
	imull	$110002499, %edi, %ebx  # imm = 0x68E8143
	leal	505104200(%rbx,%rbp), %ebp
	shrl	$5, %ecx
	xorl	16(%rsp), %ecx          # 4-byte Folded Reload
	shrl	$6, %edi
	xorl	$6, %r12d
	xorl	%edi, %ecx
	xorl	%r12d, %ecx
	xorl	%ebp, %ecx
	shrl	$9, %ebp
	xorl	%ecx, %ebp
	movl	%ebp, 100(%rsi)
	movl	(%rax), %ecx
	movl	16(%rdx), %edi
	imull	$70004807, %ecx, %ebp   # imm = 0x42C3047
	addl	%r10d, %ebp
	imull	$110002499, %edi, %ebx  # imm = 0x68E8143
	leal	705107179(%rbx,%rbp), %ebp
	shrl	$5, %ecx
	xorl	8(%rsp), %ecx           # 4-byte Folded Reload
	shrl	$6, %edi
	xorl	%edi, %ecx
	xorl	%r12d, %ecx
	xorl	%ebp, %ecx
	shrl	$9, %ebp
	xorl	%ecx, %ebp
	movl	%ebp, 104(%rsi)
	movl	(%rax), %ecx
	movl	4(%rax), %eax
	movl	12(%rdx), %edx
	imull	$50004239, %ecx, %edi   # imm = 0x2FB010F
	addl	%r9d, %edi
	imull	$70004807, %eax, %ebp   # imm = 0x42C3047
	addl	%edi, %ebp
	imull	$110002499, %edx, %edi  # imm = 0x68E8143
	leal	905110158(%rdi,%rbp), %edi
	shrl	$4, %ecx
	shrl	$5, %eax
	xorl	%ecx, %eax
	shrl	$6, %edx
	xorl	%edx, %eax
	xorl	%r12d, %eax
	movl	24(%rsp), %r12d         # 4-byte Reload
	xorl	%edi, %eax
	shrl	$9, %edi
	xorl	%eax, %edi
	movl	%edi, 108(%rsi)
	movl	_ZZ9jpegModelR5MixerE2m1+96(%rip), %edx
	movl	_ZZ9jpegModelR5MixerE7hbcount(%rip), %ecx
.LBB40_399:
	leal	1(%rdx), %eax
	movl	%eax, _ZZ9jpegModelR5MixerE2m1+96(%rip)
	movq	_ZZ9jpegModelR5MixerE2m1+32(%rip), %rax
	movslq	%edx, %rdx
	movw	$128, (%rax,%rdx,2)
	cmpl	$1, %ecx
	je	.LBB40_403
# BB#400:
	testl	%ecx, %ecx
	movq	40(%rsp), %r15          # 8-byte Reload
	jne	.LBB40_405
# BB#401:                               # %.preheader.preheader
	xorl	%ebx, %ebx
.LBB40_402:                             # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	_ZZ9jpegModelR5MixerE3cxt+16(%rip), %rax
	movl	(%rax,%rbx), %esi
	movl	$_ZZ9jpegModelR5MixerE1t, %edi
	callq	_ZN2BHILi9EEixEj
	incq	%rax
	movq	_ZZ9jpegModelR5MixerE2cp+16(%rip), %rcx
	movq	%rax, (%rcx,%rbx,2)
	movq	_ZZ9jpegModelR5MixerE2cp+16(%rip), %rax
	movq	(%rax,%rbx,2), %rax
	movzbl	(%rax), %r8d
	movq	_ZZ9jpegModelR5MixerE2sm+24(,%rbx,8), %rcx
	movslq	_ZZ9jpegModelR5MixerE2sm+4(,%rbx,8), %rdx
	movl	(%rcx,%rdx,4), %esi
	movl	%esi, %edi
	shrl	$10, %edi
	leal	1(%rsi), %ebp
	movl	%esi, %eax
	orl	$1023, %eax             # imm = 0x3FF
	andl	$1023, %esi             # imm = 0x3FF
	cmpl	$1023, %esi             # imm = 0x3FF
	cmovnel	%ebp, %eax
	movl	y(%rip), %ebp
	shll	$22, %ebp
	subl	%edi, %ebp
	sarl	$3, %ebp
	imull	_ZL2dt(,%rsi,4), %ebp
	andl	$-1024, %ebp            # imm = 0xFC00
	addl	%eax, %ebp
	movl	%ebp, (%rcx,%rdx,4)
	movl	%r8d, _ZZ9jpegModelR5MixerE2sm+4(,%rbx,8)
	movl	(%rcx,%r8,4), %eax
	shrl	$20, %eax
	movq	stretch+16(%rip), %rcx
	movzwl	(%rcx,%rax,2), %eax
	movslq	_ZZ9jpegModelR5MixerE2m1+96(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9jpegModelR5MixerE2m1+96(%rip)
	movq	_ZZ9jpegModelR5MixerE2m1+32(%rip), %rdx
	movw	%ax, (%rdx,%rcx,2)
	addq	$4, %rbx
	cmpq	$112, %rbx
	jne	.LBB40_402
	jmp	.LBB40_407
.LBB40_403:
	movl	_ZZ9jpegModelR5MixerE8huffcode(%rip), %r8d
	andl	$1, %r8d
	negl	%r8d
	andl	$3, %r8d
	incl	%r8d
	movq	_ZZ9jpegModelR5MixerE2cp+16(%rip), %rdx
	xorl	%ecx, %ecx
	movq	40(%rsp), %r15          # 8-byte Reload
.LBB40_404:                             # =>This Inner Loop Header: Depth=1
	addq	%r8, (%rdx,%rcx)
	movq	_ZZ9jpegModelR5MixerE2cp+16(%rip), %rdx
	movq	(%rdx,%rcx), %rsi
	movzbl	(%rsi), %r9d
	movq	_ZZ9jpegModelR5MixerE2sm+24(,%rcx,4), %rdi
	movslq	_ZZ9jpegModelR5MixerE2sm+4(,%rcx,4), %r10
	movl	(%rdi,%r10,4), %ebx
	movl	%ebx, %eax
	shrl	$10, %eax
	leal	1(%rbx), %esi
	movl	%ebx, %ebp
	orl	$1023, %ebp             # imm = 0x3FF
	andl	$1023, %ebx             # imm = 0x3FF
	cmpl	$1023, %ebx             # imm = 0x3FF
	cmovnel	%esi, %ebp
	movl	y(%rip), %esi
	shll	$22, %esi
	subl	%eax, %esi
	sarl	$3, %esi
	imull	_ZL2dt(,%rbx,4), %esi
	andl	$-1024, %esi            # imm = 0xFC00
	addl	%ebp, %esi
	movl	%esi, (%rdi,%r10,4)
	movl	%r9d, _ZZ9jpegModelR5MixerE2sm+4(,%rcx,4)
	movl	(%rdi,%r9,4), %eax
	shrl	$20, %eax
	movq	stretch+16(%rip), %rsi
	movzwl	(%rsi,%rax,2), %eax
	movslq	_ZZ9jpegModelR5MixerE2m1+96(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ9jpegModelR5MixerE2m1+96(%rip)
	movq	_ZZ9jpegModelR5MixerE2m1+32(%rip), %rdi
	movw	%ax, (%rdi,%rsi,2)
	addq	$8, %rcx
	cmpq	$224, %rcx
	jne	.LBB40_404
	jmp	.LBB40_407
.LBB40_405:
	movl	_ZZ9jpegModelR5MixerE8huffcode(%rip), %r8d
	andl	$1, %r8d
	incl	%r8d
	movq	_ZZ9jpegModelR5MixerE2cp+16(%rip), %rdx
	xorl	%ecx, %ecx
.LBB40_406:                             # =>This Inner Loop Header: Depth=1
	addq	%r8, (%rdx,%rcx)
	movq	_ZZ9jpegModelR5MixerE2cp+16(%rip), %rdx
	movq	(%rdx,%rcx), %rsi
	movzbl	(%rsi), %r9d
	movq	_ZZ9jpegModelR5MixerE2sm+24(,%rcx,4), %rdi
	movslq	_ZZ9jpegModelR5MixerE2sm+4(,%rcx,4), %r10
	movl	(%rdi,%r10,4), %ebx
	movl	%ebx, %eax
	shrl	$10, %eax
	leal	1(%rbx), %esi
	movl	%ebx, %ebp
	orl	$1023, %ebp             # imm = 0x3FF
	andl	$1023, %ebx             # imm = 0x3FF
	cmpl	$1023, %ebx             # imm = 0x3FF
	cmovnel	%esi, %ebp
	movl	y(%rip), %esi
	shll	$22, %esi
	subl	%eax, %esi
	sarl	$3, %esi
	imull	_ZL2dt(,%rbx,4), %esi
	andl	$-1024, %esi            # imm = 0xFC00
	addl	%ebp, %esi
	movl	%esi, (%rdi,%r10,4)
	movl	%r9d, _ZZ9jpegModelR5MixerE2sm+4(,%rcx,4)
	movl	(%rdi,%r9,4), %eax
	shrl	$20, %eax
	movq	stretch+16(%rip), %rsi
	movzwl	(%rsi,%rax,2), %eax
	movslq	_ZZ9jpegModelR5MixerE2m1+96(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ9jpegModelR5MixerE2m1+96(%rip)
	movq	_ZZ9jpegModelR5MixerE2m1+32(%rip), %rdi
	movw	%ax, (%rdi,%rsi,2)
	addq	$8, %rcx
	cmpq	$224, %rcx
	jne	.LBB40_406
.LBB40_407:                             # %.loopexit
	movl	_ZZ9jpegModelR5MixerE2m1+92(%rip), %eax
	cmpl	$1, _ZZ9jpegModelR5MixerE6column(%rip)
	adcl	$0, %eax
	movslq	_ZZ9jpegModelR5MixerE2m1+88(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ9jpegModelR5MixerE2m1+88(%rip)
	movq	_ZZ9jpegModelR5MixerE2m1+80(%rip), %rdx
	movl	%eax, (%rdx,%rcx,4)
	movl	_ZZ9jpegModelR5MixerE2m1+92(%rip), %eax
	leal	2(%rax), %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE2m1+92(%rip)
	leal	2(%rax,%r14), %eax
	movslq	_ZZ9jpegModelR5MixerE2m1+88(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, _ZZ9jpegModelR5MixerE2m1+88(%rip)
	movl	%eax, (%rdx,%rcx,4)
	movl	_ZZ9jpegModelR5MixerE2m1+92(%rip), %eax
	leal	256(%rax), %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE2m1+92(%rip)
	movl	%r12d, %ebx
	andl	$511, %ebx              # imm = 0x1FF
	leal	256(%rax,%rbx), %eax
	movslq	_ZZ9jpegModelR5MixerE2m1+88(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, _ZZ9jpegModelR5MixerE2m1+88(%rip)
	movl	%eax, (%rdx,%rcx,4)
	addl	$512, _ZZ9jpegModelR5MixerE2m1+92(%rip) # imm = 0x200
	movl	$_ZZ9jpegModelR5MixerE2m1, %edi
	callq	_ZN5Mixer1pEv
	movq	stretch+16(%rip), %r8
	movslq	%eax, %r9
	movzwl	(%r8,%r9,2), %eax
	movslq	96(%r15), %rsi
	leal	1(%rsi), %edi
	movl	%edi, 96(%r15)
	movq	32(%r15), %rdi
	movw	%ax, (%rdi,%rsi,2)
	movq	_ZZ9jpegModelR5MixerE8adv_pred+16(%rip), %rax
	movl	4(%rax), %esi
	andl	$63, %esi
	shll	$9, %esi
	orl	%ebx, %esi
	movq	_ZZ9jpegModelR5MixerE2a1+24(%rip), %rax
	movslq	_ZZ9jpegModelR5MixerE2a1+4(%rip), %rdi
	movl	(%rax,%rdi,4), %ebp
	movl	%ebp, %ebx
	shrl	$10, %ebx
	leal	1(%rbp), %ecx
	movl	%ebp, %edx
	orl	$1023, %edx             # imm = 0x3FF
	andl	$1023, %ebp             # imm = 0x3FF
	cmpl	$1023, %ebp             # imm = 0x3FF
	cmovnel	%ecx, %edx
	movl	y(%rip), %ecx
	shll	$22, %ecx
	subl	%ebx, %ecx
	sarl	$3, %ecx
	imull	_ZL2dt(,%rbp,4), %ecx
	andl	$-1024, %ecx            # imm = 0xFC00
	addl	%edx, %ecx
	movl	%ecx, (%rax,%rdi,4)
	movswl	(%r8,%r9,2), %ecx
	imull	$23, %ecx, %ecx
	addl	$47104, %ecx            # imm = 0xB800
	movl	%ecx, %ebp
	andl	$4095, %ebp             # imm = 0xFFF
	leal	(%rsi,%rsi,2), %edx
	sarl	$12, %ecx
	leal	(%rcx,%rdx,8), %ecx
	movl	%ebp, %edx
	shrl	$11, %edx
	addl	%ecx, %edx
	movl	%edx, _ZZ9jpegModelR5MixerE2a1+4(%rip)
	movslq	%ecx, %rcx
	movl	(%rax,%rcx,4), %esi
	shrl	$13, %esi
	movl	$4096, %r10d            # imm = 0x1000
	movl	$4096, %ebx             # imm = 0x1000
	subl	%ebp, %ebx
	imull	%esi, %ebx
	movl	4(%rax,%rcx,4), %edi
	shrl	$13, %edi
	imull	%ebp, %edi
	addl	%ebx, %edi
	shrl	$19, %edi
	movzbl	%r12b, %eax
	shll	$8, %r14d
	orl	%eax, %r14d
	movq	_ZZ9jpegModelR5MixerE2a2+24(%rip), %rsi
	movslq	_ZZ9jpegModelR5MixerE2a2+4(%rip), %r9
	movl	(%rsi,%r9,4), %ecx
	movl	%ecx, %ebx
	shrl	$10, %ebx
	leal	1(%rcx), %ebp
	movl	%ecx, %edx
	andl	$-1024, %edx            # imm = 0xFC00
	orl	$255, %edx
	andl	$1023, %ecx             # imm = 0x3FF
	cmpl	$255, %ecx
	cmovbl	%ebp, %edx
	movl	y(%rip), %ebp
	shll	$22, %ebp
	subl	%ebx, %ebp
	sarl	$3, %ebp
	imull	_ZL2dt(,%rcx,4), %ebp
	andl	$-1024, %ebp            # imm = 0xFC00
	addl	%edx, %ebp
	movl	%ebp, (%rsi,%r9,4)
	movswl	(%r8,%rdi,2), %ecx
	imull	$23, %ecx, %ecx
	addl	$47104, %ecx            # imm = 0xB800
	movl	%ecx, %edx
	andl	$4095, %edx             # imm = 0xFFF
	leal	(%r14,%r14,2), %edi
	sarl	$12, %ecx
	leal	(%rcx,%rdi,8), %ecx
	movl	%edx, %edi
	shrl	$11, %edi
	addl	%ecx, %edi
	movl	%edi, _ZZ9jpegModelR5MixerE2a2+4(%rip)
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %edi
	shrl	$13, %edi
	subl	%edx, %r10d
	imull	%edi, %r10d
	movl	4(%rsi,%rcx,4), %ecx
	shrl	$13, %ecx
	imull	%edx, %ecx
	addl	%r10d, %ecx
	shrl	$19, %ecx
	movq	stretch+16(%rip), %rdx
	movzwl	(%rdx,%rcx,2), %ecx
	movslq	96(%r15), %rdx
	leal	1(%rdx), %esi
	movl	%esi, 96(%r15)
	movq	32(%r15), %rsi
	movw	%cx, (%rsi,%rdx,2)
	addl	$2, %eax
	jmp	.LBB40_408
.LBB40_409:                             # %.lr.ph904.split.us.preheader
	movq	_ZZ9jpegModelR5MixerE5color+16(%rip), %rbx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebp, %r8d
	movl	%ebp, %r9d
	andl	$1, %r9d
	xorl	%esi, %esi
.LBB40_410:                             # %.lr.ph904.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB40_416 Depth 2
	movl	$0, (%r11,%rsi,4)
	movl	$1, %edi
	testl	%r9d, %r9d
	movl	$0, %r15d
	jne	.LBB40_414
# BB#411:                               #   in Loop: Header=BB40_410 Depth=1
	leal	1(%rsi), %eax
	cltd
	idivl	%ebp
	movslq	%edx, %rax
	movl	(%rbx,%rax,4), %eax
	movl	$2, %edi
	xorl	%r15d, %r15d
	cmpl	(%rbx,%rsi,4), %eax
	jne	.LBB40_413
# BB#412:                               #   in Loop: Header=BB40_410 Depth=1
	movl	$1, (%r11,%rsi,4)
	movl	$1, %r15d
.LBB40_413:                             # %.prol.loopexit
                                        #   in Loop: Header=BB40_410 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB40_414:                             # %.prol.loopexit
                                        #   in Loop: Header=BB40_410 Depth=1
	cmpl	$2, %ebp
	je	.LBB40_421
# BB#415:                               # %.lr.ph904.split.us.new
                                        #   in Loop: Header=BB40_410 Depth=1
	leal	(%rdi,%rsi), %ecx
.LBB40_416:                             #   Parent Loop BB40_410 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %eax
	cltd
	idivl	%ebp
	movslq	%edx, %rax
	movl	(%rbx,%rax,4), %eax
	cmpl	(%rbx,%rsi,4), %eax
	jne	.LBB40_418
# BB#417:                               #   in Loop: Header=BB40_416 Depth=2
	movl	%edi, (%r11,%rsi,4)
	movl	%edi, %r15d
.LBB40_418:                             #   in Loop: Header=BB40_416 Depth=2
	leal	1(%rcx), %eax
	cltd
	idivl	%ebp
	movslq	%edx, %rax
	movl	(%rbx,%rax,4), %eax
	cmpl	(%rbx,%rsi,4), %eax
	jne	.LBB40_420
# BB#419:                               #   in Loop: Header=BB40_416 Depth=2
	leal	1(%rdi), %r15d
	movl	%r15d, (%r11,%rsi,4)
.LBB40_420:                             #   in Loop: Header=BB40_416 Depth=2
	addl	$2, %edi
	addl	$2, %ecx
	cmpl	%edi, %ebp
	jne	.LBB40_416
.LBB40_421:                             # %._crit_edge.us
                                        #   in Loop: Header=BB40_410 Depth=1
	movl	%ebp, %eax
	subl	%r15d, %eax
	shll	$6, %eax
	movl	%eax, (%r11,%rsi,4)
	incq	%rsi
	cmpq	%r8, %rsi
	jne	.LBB40_410
.LBB40_422:                             # %.preheader856
	movq	_ZZ9jpegModelR5MixerE4zpos+16(%rip), %rax
	xorl	%ecx, %ecx
.LBB40_423:                             # =>This Inner Loop Header: Depth=1
	movzbl	_ZZ9jpegModelR5MixerE3zzu(%rcx), %edx
	movzbl	_ZZ9jpegModelR5MixerE3zzv(%rcx), %esi
	leaq	(%rdx,%rsi,8), %rdx
	movl	%ecx, (%rax,%rdx,4)
	movzbl	_ZZ9jpegModelR5MixerE3zzu+1(%rcx), %edx
	movzbl	_ZZ9jpegModelR5MixerE3zzv+1(%rcx), %esi
	leaq	(%rdx,%rsi,8), %rdx
	leal	1(%rcx), %esi
	movl	%esi, (%rax,%rdx,4)
	addq	$2, %rcx
	cmpq	$64, %rcx
	jne	.LBB40_423
# BB#424:
	leal	7(%r13), %eax
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %eax
	cltq
	movzbl	(%r14,%rax), %eax
	shll	$8, %eax
	leal	8(%r13), %edx
	andl	%ecx, %edx
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %esi
	orl	%eax, %esi
	movl	%esi, _ZZ9jpegModelR5MixerE5width(%rip)
	leal	5(%r13), %eax
	andl	%ecx, %eax
	cltq
	movzbl	(%r14,%rax), %eax
	shll	$8, %eax
	addl	$6, %r13d
	andl	%ecx, %r13d
	movslq	%r13d, %rcx
	movzbl	(%r14,%rcx), %edx
	orl	%eax, %edx
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
	movl	_ZZ9jpegModelR5MixerE5width(%rip), %eax
	decl	%eax
	movq	%r12, %rbp
	shll	$3, %ebp
	cltd
	idivl	%ebp
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ9jpegModelR5MixerE5width(%rip)
	testl	%eax, %eax
	js	.LBB40_211
# BB#425:
	movl	_ZZ9jpegModelR5MixerE7mcusize(%rip), %r9d
	shll	$6, %r9d
	movl	%r9d, _ZZ9jpegModelR5MixerE7mcusize(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE6column(%rip)
	movl	$0, _ZZ9jpegModelR5MixerE3row(%rip)
	movq	40(%rsp), %r12          # 8-byte Reload
	testl	%r9d, %r9d
	jne	.LBB40_196
	jmp	.LBB40_335
.LBB40_211:
	movl	$0, _ZZ9jpegModelR5MixerE4jpeg(%rip)
.LBB40_212:                             # %.thread809
	movl	_ZZ9jpegModelR5MixerE9next_jpeg(%rip), %eax
.LBB40_408:                             # %.thread809
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB40_426:
.Ltmp126:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp127:
# BB#427:                               # %.noexc
.LBB40_428:
.Ltmp129:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp130:
# BB#429:                               # %.noexc725
.LBB40_430:
.Ltmp132:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp133:
# BB#431:                               # %.noexc727
.LBB40_432:
.Ltmp135:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp136:
# BB#433:                               # %.noexc729
.LBB40_434:
.Ltmp138:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp139:
# BB#435:                               # %.noexc732
.LBB40_436:
.Ltmp141:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp142:
# BB#437:                               # %.noexc734
.LBB40_438:
.Ltmp144:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp145:
# BB#439:                               # %.noexc736
.LBB40_440:
.Ltmp147:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp148:
# BB#441:                               # %.noexc738
.LBB40_442:
.Ltmp150:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp151:
# BB#443:                               # %.noexc741
.LBB40_444:
.Ltmp153:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp154:
# BB#445:                               # %.noexc744
.LBB40_446:
.Ltmp156:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp157:
# BB#447:                               # %.noexc747
.LBB40_448:
.Ltmp159:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp160:
# BB#449:                               # %.noexc750
.LBB40_450:
.Ltmp162:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp163:
# BB#451:                               # %.noexc753
.LBB40_452:
.Ltmp165:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp166:
# BB#453:                               # %.noexc756
.LBB40_454:
.Ltmp168:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp169:
# BB#455:                               # %.noexc759
.LBB40_456:
.Ltmp180:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp181:
# BB#457:                               # %.noexc769
.LBB40_458:
.Ltmp174:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp175:
# BB#459:                               # %.noexc773
.LBB40_460:
.Ltmp177:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp178:
# BB#461:                               # %.noexc771
.LBB40_462:
.Ltmp171:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp172:
# BB#463:                               # %.noexc775
.LBB40_464:
.Ltmp173:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE1t, %edi
	jmp	.LBB40_490
.LBB40_465:
.Ltmp179:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE2cp, %edi
	jmp	.LBB40_490
.LBB40_466:
.Ltmp176:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE3cxt, %edi
	jmp	.LBB40_490
.LBB40_467:
.Ltmp182:
	movq	%rax, %rbx
	testq	%rbp, %rbp
	je	.LBB40_471
.LBB40_468:                             # %.preheader847
                                        # =>This Inner Loop Header: Depth=1
	movl	_ZZ9jpegModelR5MixerE2sm-24(%rbp), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB40_470
# BB#469:                               #   in Loop: Header=BB40_468 Depth=1
	movl	%eax, programChecker+4(%rip)
.LBB40_470:                             #   in Loop: Header=BB40_468 Depth=1
	movq	_ZZ9jpegModelR5MixerE2sm-16(%rbp), %rdi
	callq	free
	addq	$-32, %rbp
	jne	.LBB40_468
.LBB40_471:                             # %.loopexit848
	movl	$_ZGVZ9jpegModelR5MixerE2sm, %edi
	jmp	.LBB40_490
.LBB40_472:
.Ltmp170:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE4qmap, %edi
	jmp	.LBB40_490
.LBB40_473:
.Ltmp167:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE4qtab, %edi
	jmp	.LBB40_490
.LBB40_474:
.Ltmp164:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE4zpos, %edi
	jmp	.LBB40_490
.LBB40_475:
.Ltmp161:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE3lcp, %edi
	jmp	.LBB40_490
.LBB40_476:
.Ltmp158:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE2ls, %edi
	jmp	.LBB40_490
.LBB40_477:
.Ltmp155:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE4sumv, %edi
	jmp	.LBB40_490
.LBB40_478:
.Ltmp152:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE4sumu, %edi
	jmp	.LBB40_490
.LBB40_479:
.Ltmp149:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE8adv_pred, %edi
	jmp	.LBB40_490
.LBB40_480:
.Ltmp146:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE5cbuf2, %edi
	jmp	.LBB40_490
.LBB40_481:
.Ltmp143:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE4cbuf, %edi
	jmp	.LBB40_490
.LBB40_482:
.Ltmp140:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE4pred, %edi
	jmp	.LBB40_490
.LBB40_483:
.Ltmp137:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE5color, %edi
	jmp	.LBB40_490
.LBB40_484:
.Ltmp134:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE4hbuf, %edi
	jmp	.LBB40_490
.LBB40_485:
.Ltmp131:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE3huf, %edi
	jmp	.LBB40_490
.LBB40_486:
.Ltmp128:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE2ht, %edi
	jmp	.LBB40_490
.LBB40_487:
.Ltmp191:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE2a2, %edi
	jmp	.LBB40_490
.LBB40_488:
.Ltmp188:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE2a1, %edi
	jmp	.LBB40_490
.LBB40_489:
.Ltmp185:
	movq	%rax, %rbx
	movl	$_ZGVZ9jpegModelR5MixerE2m1, %edi
.LBB40_490:
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end40:
	.size	_Z9jpegModelR5Mixer, .Lfunc_end40-_Z9jpegModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\310\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\305\002"              # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp183-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp183
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin11 #     jumps to .Ltmp185
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin11 #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin11 #     jumps to .Ltmp191
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp126-.Ltmp190       #   Call between .Ltmp190 and .Ltmp126
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin11 #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin11 # >> Call Site 7 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin11 #     jumps to .Ltmp131
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin11 # >> Call Site 8 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin11 #     jumps to .Ltmp134
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin11 # >> Call Site 9 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin11 #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin11 # >> Call Site 10 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin11 #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin11 # >> Call Site 11 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin11 #     jumps to .Ltmp143
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin11 # >> Call Site 12 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin11 #     jumps to .Ltmp146
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin11 # >> Call Site 13 <<
	.long	.Ltmp148-.Ltmp147       #   Call between .Ltmp147 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin11 #     jumps to .Ltmp149
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin11 # >> Call Site 14 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp152-.Lfunc_begin11 #     jumps to .Ltmp152
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin11 # >> Call Site 15 <<
	.long	.Ltmp154-.Ltmp153       #   Call between .Ltmp153 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin11 #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin11 # >> Call Site 16 <<
	.long	.Ltmp157-.Ltmp156       #   Call between .Ltmp156 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin11 #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin11 # >> Call Site 17 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp161-.Lfunc_begin11 #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin11 # >> Call Site 18 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin11 #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp165-.Lfunc_begin11 # >> Call Site 19 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin11 #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin11 # >> Call Site 20 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin11 #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin11 # >> Call Site 21 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin11 #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin11 # >> Call Site 22 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin11 #     jumps to .Ltmp176
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin11 # >> Call Site 23 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin11 #     jumps to .Ltmp179
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin11 # >> Call Site 24 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin11 #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp172-.Lfunc_begin11 # >> Call Site 25 <<
	.long	.Lfunc_end40-.Ltmp172   #   Call between .Ltmp172 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN5ArrayI3HUFLi0EED2Ev,"axG",@progbits,_ZN5ArrayI3HUFLi0EED2Ev,comdat
	.weak	_ZN5ArrayI3HUFLi0EED2Ev
	.p2align	4, 0x90
	.type	_ZN5ArrayI3HUFLi0EED2Ev,@function
_ZN5ArrayI3HUFLi0EED2Ev:                # @_ZN5ArrayI3HUFLi0EED2Ev
	.cfi_startproc
# BB#0:
	imull	$-12, (%rdi), %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB41_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB41_2:                               # %_ZN14ProgramChecker5allocEi.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end41:
	.size	_ZN5ArrayI3HUFLi0EED2Ev, .Lfunc_end41-_ZN5ArrayI3HUFLi0EED2Ev
	.cfi_endproc

	.section	.text._ZN6IntBufD2Ev,"axG",@progbits,_ZN6IntBufD2Ev,comdat
	.weak	_ZN6IntBufD2Ev
	.p2align	4, 0x90
	.type	_ZN6IntBufD2Ev,@function
_ZN6IntBufD2Ev:                         # @_ZN6IntBufD2Ev
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB42_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB42_2:                               # %_ZN5ArrayIiLi0EED2Ev.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end42:
	.size	_ZN6IntBufD2Ev, .Lfunc_end42-_ZN6IntBufD2Ev
	.cfi_endproc

	.section	.text._ZN2BHILi9EED2Ev,"axG",@progbits,_ZN2BHILi9EED2Ev,comdat
	.weak	_ZN2BHILi9EED2Ev
	.p2align	4, 0x90
	.type	_ZN2BHILi9EED2Ev,@function
_ZN2BHILi9EED2Ev:                       # @_ZN2BHILi9EED2Ev
	.cfi_startproc
# BB#0:
	movl	$-64, %eax
	subl	(%rdi), %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB43_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB43_2:                               # %_ZN5ArrayIhLi64EED2Ev.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end43:
	.size	_ZN2BHILi9EED2Ev, .Lfunc_end43-_ZN2BHILi9EED2Ev
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	__cxx_global_array_dtor.13,@function
__cxx_global_array_dtor.13:             # @__cxx_global_array_dtor.13
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi183:
	.cfi_def_cfa_offset 16
.Lcfi184:
	.cfi_offset %rbx, -16
	movl	$896, %ebx              # imm = 0x380
	.p2align	4, 0x90
.LBB44_1:                               # =>This Inner Loop Header: Depth=1
	movl	_ZZ9jpegModelR5MixerE2sm-24(%rbx), %ecx
	shll	$2, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB44_3
# BB#2:                                 #   in Loop: Header=BB44_1 Depth=1
	movl	%eax, programChecker+4(%rip)
.LBB44_3:                               #   in Loop: Header=BB44_1 Depth=1
	movq	_ZZ9jpegModelR5MixerE2sm-16(%rbx), %rdi
	callq	free
	addq	$-32, %rbx
	jne	.LBB44_1
# BB#4:
	popq	%rbx
	retq
.Lfunc_end44:
	.size	__cxx_global_array_dtor.13, .Lfunc_end44-__cxx_global_array_dtor.13
	.cfi_endproc

	.section	.text._ZN2BHILi9EEixEj,"axG",@progbits,_ZN2BHILi9EEixEj,comdat
	.weak	_ZN2BHILi9EEixEj
	.p2align	4, 0x90
	.type	_ZN2BHILi9EEixEj,@function
_ZN2BHILi9EEixEj:                       # @_ZN2BHILi9EEixEj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi187:
	.cfi_def_cfa_offset 32
.Lcfi188:
	.cfi_offset %rbx, -24
.Lcfi189:
	.cfi_offset %r14, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r14
	movl	%esi, %eax
	shrl	$16, %eax
	movzwl	%si, %edx
	xorl	%eax, %edx
	shll	$3, %esi
	andl	24(%r14), %esi
	movq	16(%r14), %rdi
	leal	(%rsi,%rsi,8), %eax
	movslq	%eax, %rbx
	leaq	(%rdi,%rbx), %rax
	cmpb	$0, 2(%rdi,%rbx)
	je	.LBB45_6
# BB#1:
	movzwl	(%rax), %ecx
	cmpl	%edx, %ecx
	je	.LBB45_7
# BB#2:
	movl	%esi, %r8d
	orl	$1, %r8d
	leal	(%r8,%r8,8), %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r9
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB45_3
# BB#11:
	movzwl	(%r9), %ecx
	cmpl	%edx, %ecx
	jne	.LBB45_13
# BB#12:
	movl	$1, %esi
	jmp	.LBB45_5
.LBB45_6:                               # %.loopexit
	movw	%dx, (%rax)
.LBB45_7:                               # %.loopexit.thread51
	incq	%rax
	jmp	.LBB45_10
.LBB45_3:
	movl	$1, %esi
	jmp	.LBB45_4
.LBB45_13:
	movl	%esi, %ecx
	orl	$2, %ecx
	leal	(%rcx,%rcx,8), %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r9
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB45_14
# BB#15:
	movzwl	(%r9), %ecx
	cmpl	%edx, %ecx
	jne	.LBB45_17
# BB#16:
	movl	$2, %esi
	jmp	.LBB45_5
.LBB45_14:
	movl	$2, %esi
	jmp	.LBB45_4
.LBB45_17:
	movl	%esi, %ecx
	orl	$3, %ecx
	leal	(%rcx,%rcx,8), %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r9
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB45_18
# BB#19:
	movzwl	(%r9), %ecx
	cmpl	%edx, %ecx
	jne	.LBB45_21
# BB#20:
	movl	$3, %esi
	jmp	.LBB45_5
.LBB45_18:
	movl	$3, %esi
	jmp	.LBB45_4
.LBB45_21:
	movl	%esi, %ecx
	orl	$4, %ecx
	leal	(%rcx,%rcx,8), %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r9
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB45_22
# BB#23:
	movzwl	(%r9), %ecx
	cmpl	%edx, %ecx
	jne	.LBB45_25
# BB#24:
	movl	$4, %esi
	jmp	.LBB45_5
.LBB45_22:
	movl	$4, %esi
	jmp	.LBB45_4
.LBB45_25:
	movl	%esi, %ecx
	orl	$5, %ecx
	leal	(%rcx,%rcx,8), %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r9
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB45_26
# BB#27:
	movzwl	(%r9), %ecx
	cmpl	%edx, %ecx
	jne	.LBB45_29
# BB#28:
	movl	$5, %esi
	jmp	.LBB45_5
.LBB45_26:
	movl	$5, %esi
	jmp	.LBB45_4
.LBB45_29:
	movl	%esi, %ecx
	orl	$6, %ecx
	leal	(%rcx,%rcx,8), %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r9
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB45_30
# BB#31:
	movzwl	(%r9), %ecx
	cmpl	%edx, %ecx
	jne	.LBB45_33
# BB#32:
	movl	$6, %esi
	jmp	.LBB45_5
.LBB45_30:
	movl	$6, %esi
	jmp	.LBB45_4
.LBB45_33:
	orl	$7, %esi
	leal	(%rsi,%rsi,8), %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r9
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB45_34
# BB#35:
	movzwl	(%r9), %ecx
	cmpl	%edx, %ecx
	jne	.LBB45_8
# BB#36:
	movl	$7, %esi
	jmp	.LBB45_5
.LBB45_34:
	movl	$7, %esi
.LBB45_4:                               # %.loopexit.thread54
	movw	%dx, (%r9)
.LBB45_5:                               # %.loopexit.thread
	movb	8(%r9), %cl
	movb	%cl, _ZZN2BHILi9EEixEjE3tmp+8(%rip)
	movq	(%r9), %rcx
	movq	%rcx, _ZZN2BHILi9EEixEjE3tmp(%rip)
.LBB45_9:
	leal	(%r8,%r8,8), %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rdi
	shlq	$32, %rsi
	leaq	(%rsi,%rsi,8), %rdx
	sarq	$32, %rdx
	movq	%rax, %rsi
	callq	memmove
	movq	16(%r14), %rax
	movb	_ZZN2BHILi9EEixEjE3tmp+8(%rip), %cl
	movb	%cl, 8(%rax,%rbx)
	movq	_ZZN2BHILi9EEixEjE3tmp(%rip), %rcx
	movq	%rcx, (%rax,%rbx)
	orl	$1, %ebx
	movslq	%ebx, %rax
	addq	16(%r14), %rax
.LBB45_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB45_8:
	movb	$0, _ZZN2BHILi9EEixEjE3tmp+8(%rip)
	movw	$0, _ZZN2BHILi9EEixEjE3tmp+6(%rip)
	movl	$0, _ZZN2BHILi9EEixEjE3tmp+2(%rip)
	movw	%dx, _ZZN2BHILi9EEixEjE3tmp(%rip)
	leal	2(%rsi,%rsi,8), %ecx
	movslq	%ecx, %rcx
	movb	(%rdi,%rcx), %cl
	leal	-7(%rsi,%rsi,8), %edx
	movslq	%edx, %rdx
	xorl	%esi, %esi
	cmpb	(%rdi,%rdx), %cl
	setbe	%sil
	orq	$6, %rsi
	jmp	.LBB45_9
.Lfunc_end45:
	.size	_ZN2BHILi9EEixEj, .Lfunc_end45-_ZN2BHILi9EEixEj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI46_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI46_1:
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
.LCPI46_2:
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.section	.text._ZN5Mixer1pEv,"axG",@progbits,_ZN5Mixer1pEv,comdat
	.weak	_ZN5Mixer1pEv
	.p2align	4, 0x90
	.type	_ZN5Mixer1pEv,@function
_ZN5Mixer1pEv:                          # @_ZN5Mixer1pEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi190:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi191:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi193:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi194:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi195:
	.cfi_def_cfa_offset 56
.Lcfi196:
	.cfi_offset %rbx, -56
.Lcfi197:
	.cfi_offset %r12, -48
.Lcfi198:
	.cfi_offset %r13, -40
.Lcfi199:
	.cfi_offset %r14, -32
.Lcfi200:
	.cfi_offset %r15, -24
.Lcfi201:
	.cfi_offset %rbp, -16
	movq	stretch+16(%rip), %r9
	movl	$-32768, %r14d          # imm = 0x8000
	movw	$32767, %r15w           # imm = 0x7FFF
	movdqa	.LCPI46_0(%rip), %xmm8  # xmm8 = [1,1,1,1]
	movdqa	.LCPI46_1(%rip), %xmm9  # xmm9 = [4294934528,4294934528,4294934528,4294934528]
	movdqa	.LCPI46_2(%rip), %xmm10 # xmm10 = [32767,32767,32767,32767]
	movq	%r9, -56(%rsp)          # 8-byte Spill
	jmp	.LBB46_1
	.p2align	4, 0x90
.LBB46_19:                              # %._crit_edge28.loopexit
                                        #   in Loop: Header=BB46_1 Depth=1
	movl	88(%r13), %eax
	movl	92(%r13), %ecx
	movl	$-32768, %r14d          # imm = 0x8000
	movw	$32767, %r15w           # imm = 0x7FFF
	jmp	.LBB46_20
	.p2align	4, 0x90
.LBB46_18:                              #   in Loop: Header=BB46_1 Depth=1
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB46_20:                              # %._crit_edge28
                                        #   in Loop: Header=BB46_1 Depth=1
	leal	1(%rax), %edx
	movl	%edx, 88(%r13)
	movq	80(%r13), %rdx
	cltq
	movl	%ecx, (%rdx,%rax,4)
	incl	92(%r13)
	movq	%r13, %rdi
.LBB46_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB46_3 Depth 2
                                        #     Child Loop BB46_8 Depth 2
                                        #       Child Loop BB46_14 Depth 3
                                        #       Child Loop BB46_15 Depth 3
                                        #     Child Loop BB46_22 Depth 2
                                        #       Child Loop BB46_29 Depth 3
                                        #       Child Loop BB46_32 Depth 3
	movl	96(%rdi), %edx
	testb	$7, %dl
	je	.LBB46_5
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB46_1 Depth=1
	movslq	%edx, %rax
	addq	%rax, %rax
	addq	32(%rdi), %rax
	.p2align	4, 0x90
.LBB46_3:                               #   Parent Loop BB46_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	$0, (%rax)
	incl	%edx
	addq	$2, %rax
	testb	$7, %dl
	jne	.LBB46_3
# BB#4:                                 # %._crit_edge
                                        #   in Loop: Header=BB46_1 Depth=1
	movl	%edx, 96(%rdi)
.LBB46_5:                               #   in Loop: Header=BB46_1 Depth=1
	movq	128(%rdi), %r13
	testq	%r13, %r13
	je	.LBB46_37
# BB#6:                                 #   in Loop: Header=BB46_1 Depth=1
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	movslq	88(%r13), %r10
	testq	%r10, %r10
	jle	.LBB46_17
# BB#7:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB46_1 Depth=1
	movl	y(%rip), %r11d
	shll	$12, %r11d
	movq	120(%r13), %r9
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB46_8:                               #   Parent Loop BB46_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB46_14 Depth 3
                                        #       Child Loop BB46_15 Depth 3
	movl	%r11d, %ecx
	subl	(%r9,%rax,4), %ecx
	je	.LBB46_16
# BB#9:                                 #   in Loop: Header=BB46_8 Depth=2
	movl	96(%r13), %esi
	addl	$7, %esi
	andl	$-8, %esi
	testl	%esi, %esi
	jle	.LBB46_16
# BB#10:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB46_8 Depth=2
	leal	(,%rcx,8), %edx
	subl	%ecx, %edx
	movq	32(%r13), %rbx
	movq	56(%r13), %r8
	movq	80(%r13), %rcx
	movslq	(%r13), %rbp
	movslq	(%rcx,%rax,4), %rdi
	imulq	%rbp, %rdi
	leaq	(%r8,%rdi,2), %rcx
	movl	%esi, %ebp
	cmpl	$7, %esi
	jbe	.LBB46_15
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB46_8 Depth=2
	leaq	(%rbx,%rbp,2), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB46_13
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB46_8 Depth=2
	addq	%rbp, %rdi
	leaq	(%r8,%rdi,2), %rsi
	cmpq	%rsi, %rbx
	jae	.LBB46_13
	.p2align	4, 0x90
.LBB46_15:                              # %.lr.ph.i.i
                                        #   Parent Loop BB46_1 Depth=1
                                        #     Parent Loop BB46_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movswl	(%rcx), %esi
	movswl	(%rbx), %edi
	imull	%edx, %edi
	sarl	$15, %edi
	incl	%edi
	sarl	%edi
	addl	%esi, %edi
	cmpl	$-32769, %edi           # imm = 0xFFFF7FFF
	cmovlel	%r14d, %edi
	cmpl	$32767, %edi            # imm = 0x7FFF
	cmovgew	%r15w, %di
	movw	%di, (%rcx)
	addq	$2, %rbx
	addq	$2, %rcx
	decq	%rbp
	jne	.LBB46_15
	jmp	.LBB46_16
.LBB46_13:                              # %vector.ph65
                                        #   in Loop: Header=BB46_8 Depth=2
	movd	%edx, %xmm0
	pshufd	$0, %xmm0, %xmm3        # xmm3 = xmm0[0,0,0,0]
	.p2align	4, 0x90
.LBB46_14:                              # %vector.body53
                                        #   Parent Loop BB46_1 Depth=1
                                        #     Parent Loop BB46_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	psrad	$16, %xmm4
	movq	(%rcx), %xmm0           # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	psrad	$16, %xmm5
	movq	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	movq	(%rbx), %xmm6           # xmm6 = mem[0],zero
	punpcklwd	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1],xmm7[2],xmm6[2],xmm7[3],xmm6[3]
	psrad	$16, %xmm7
	movdqa	%xmm3, %xmm6
	pmuludq	%xmm7, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm7, %xmm7      # xmm7 = xmm7[1,1,3,3]
	pshufd	$245, %xmm3, %xmm1      # xmm1 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	movdqa	%xmm3, %xmm7
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	psrad	$15, %xmm7
	psrad	$15, %xmm6
	paddd	%xmm8, %xmm6
	paddd	%xmm8, %xmm7
	psrad	$1, %xmm7
	psrad	$1, %xmm6
	paddd	%xmm5, %xmm6
	paddd	%xmm4, %xmm7
	movdqa	%xmm7, %xmm0
	pcmpgtd	%xmm9, %xmm0
	movdqa	%xmm6, %xmm1
	pcmpgtd	%xmm9, %xmm1
	pand	%xmm1, %xmm6
	pandn	%xmm9, %xmm1
	por	%xmm6, %xmm1
	pand	%xmm0, %xmm7
	pandn	%xmm9, %xmm0
	por	%xmm7, %xmm0
	movdqa	%xmm10, %xmm4
	pcmpgtd	%xmm0, %xmm4
	movdqa	%xmm10, %xmm5
	pcmpgtd	%xmm1, %xmm5
	pand	%xmm5, %xmm1
	pandn	%xmm10, %xmm5
	por	%xmm1, %xmm5
	pand	%xmm4, %xmm0
	pandn	%xmm10, %xmm4
	por	%xmm0, %xmm4
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	packssdw	%xmm4, %xmm5
	movdqu	%xmm5, (%rcx)
	addq	$16, %rcx
	addq	$16, %rbx
	addq	$-8, %rbp
	jne	.LBB46_14
	.p2align	4, 0x90
.LBB46_16:                              # %_Z5trainPsS_ii.exit.i
                                        #   in Loop: Header=BB46_8 Depth=2
	incq	%rax
	cmpq	%r10, %rax
	jne	.LBB46_8
.LBB46_17:                              # %_ZN5Mixer6updateEv.exit
                                        #   in Loop: Header=BB46_1 Depth=1
	movq	$0, 88(%r13)
	movl	$0, 96(%r13)
	movq	-48(%rsp), %r8          # 8-byte Reload
	cmpl	$0, 88(%r8)
	jle	.LBB46_18
# BB#21:                                # %.lr.ph27
                                        #   in Loop: Header=BB46_1 Depth=1
	movq	32(%r8), %rax
	movq	56(%r8), %r11
	movq	80(%r8), %r10
	movq	120(%r8), %rcx
	movq	%rcx, -8(%rsp)          # 8-byte Spill
	movq	32(%r13), %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	leaq	16(%rax), %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	leaq	16(%r11), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	addq	$2, %r11
	xorl	%r12d, %r12d
	movq	-56(%rsp), %r9          # 8-byte Reload
	movq	%r13, -24(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB46_22:                              #   Parent Loop BB46_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB46_29 Depth 3
                                        #       Child Loop BB46_32 Depth 3
	movl	96(%r8), %ecx
	addl	$7, %ecx
	andl	$-8, %ecx
	testl	%ecx, %ecx
	jle	.LBB46_23
# BB#24:                                # %.lr.ph.preheader.i13
                                        #   in Loop: Header=BB46_22 Depth=2
	movslq	(%r8), %rdx
	movslq	(%r10,%r12,4), %r15
	imulq	%rdx, %r15
	movslq	%ecx, %r13
	leaq	-1(%r13), %r14
	shrq	%r14
	incq	%r14
	cmpq	$8, %r14
	jae	.LBB46_26
# BB#25:                                #   in Loop: Header=BB46_22 Depth=2
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	jmp	.LBB46_31
	.p2align	4, 0x90
.LBB46_23:                              #   in Loop: Header=BB46_22 Depth=2
	xorl	%edx, %edx
	jmp	.LBB46_35
	.p2align	4, 0x90
.LBB46_26:                              # %min.iters.checked
                                        #   in Loop: Header=BB46_22 Depth=2
	movq	%r10, %rsi
	movq	%r14, %rbp
	andq	$-8, %rbp
	movq	%r14, %r10
	andq	$-8, %r10
	je	.LBB46_27
# BB#28:                                # %vector.body.preheader
                                        #   in Loop: Header=BB46_22 Depth=2
	movq	%r11, %rcx
	addq	%rbp, %rbp
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r15,2), %r9
	pxor	%xmm3, %xmm3
	movq	%r10, %r8
	movq	-32(%rsp), %r11         # 8-byte Reload
	pxor	%xmm4, %xmm4
	.p2align	4, 0x90
.LBB46_29:                              # %vector.body
                                        #   Parent Loop BB46_1 Depth=1
                                        #     Parent Loop BB46_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	-16(%r11), %xmm1
	movdqu	(%r11), %xmm12
	movdqu	-16(%r9), %xmm2
	movdqu	(%r9), %xmm11
	pshuflw	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$232, %xmm2, %xmm5      # xmm5 = xmm2[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm5, %xmm6      # xmm6 = xmm5[0,2,2,3]
	movdqa	%xmm6, %xmm5
	pmulhw	%xmm0, %xmm5
	pmullw	%xmm0, %xmm6
	punpcklwd	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1],xmm6[2],xmm5[2],xmm6[3],xmm5[3]
	pshuflw	$232, %xmm12, %xmm0     # xmm0 = xmm12[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm5      # xmm5 = xmm0[0,2,2,3]
	pshuflw	$232, %xmm11, %xmm0     # xmm0 = xmm11[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm7      # xmm7 = xmm0[0,2,2,3]
	movdqa	%xmm7, %xmm0
	pmulhw	%xmm5, %xmm0
	pmullw	%xmm5, %xmm7
	punpcklwd	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1],xmm7[2],xmm0[2],xmm7[3],xmm0[3]
	pshuflw	$231, %xmm1, %xmm0      # xmm0 = xmm1[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$177, %xmm0, %xmm0      # xmm0 = xmm0[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm2, %xmm1      # xmm1 = xmm2[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	movdqa	%xmm1, %xmm2
	pmulhw	%xmm0, %xmm2
	pmullw	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	pshuflw	$231, %xmm12, %xmm0     # xmm0 = xmm12[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$177, %xmm0, %xmm0      # xmm0 = xmm0[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm11, %xmm2     # xmm2 = xmm11[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$177, %xmm2, %xmm2      # xmm2 = xmm2[1,0,3,2,4,5,6,7]
	movdqa	%xmm2, %xmm5
	pmulhw	%xmm0, %xmm5
	pmullw	%xmm0, %xmm2
	punpcklwd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1],xmm2[2],xmm5[2],xmm2[3],xmm5[3]
	paddd	%xmm6, %xmm1
	paddd	%xmm7, %xmm2
	psrad	$8, %xmm1
	psrad	$8, %xmm2
	paddd	%xmm1, %xmm3
	paddd	%xmm2, %xmm4
	addq	$32, %r11
	addq	$32, %r9
	addq	$-8, %r8
	jne	.LBB46_29
# BB#30:                                # %middle.block
                                        #   in Loop: Header=BB46_22 Depth=2
	paddd	%xmm3, %xmm4
	pshufd	$78, %xmm4, %xmm0       # xmm0 = xmm4[2,3,0,1]
	paddd	%xmm4, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edx
	cmpq	%r10, %r14
	movq	-48(%rsp), %r8          # 8-byte Reload
	movq	-56(%rsp), %r9          # 8-byte Reload
	movq	%rcx, %r11
	movq	%rsi, %r10
	jne	.LBB46_31
	jmp	.LBB46_33
.LBB46_27:                              #   in Loop: Header=BB46_22 Depth=2
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	movq	%rsi, %r10
.LBB46_31:                              # %.lr.ph.i18.preheader
                                        #   in Loop: Header=BB46_22 Depth=2
	leaq	(%r11,%r15,2), %rsi
	.p2align	4, 0x90
.LBB46_32:                              # %.lr.ph.i18
                                        #   Parent Loop BB46_1 Depth=1
                                        #     Parent Loop BB46_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movswl	(%rax,%rbp,2), %edi
	movswl	-2(%rsi,%rbp,2), %ebx
	imull	%edi, %ebx
	movswl	2(%rax,%rbp,2), %edi
	movswl	(%rsi,%rbp,2), %ecx
	imull	%edi, %ecx
	addl	%ebx, %ecx
	sarl	$8, %ecx
	addl	%ecx, %edx
	addq	$2, %rbp
	cmpq	%r13, %rbp
	jl	.LBB46_32
.LBB46_33:                              # %_Z11dot_productPsS_i.exit19
                                        #   in Loop: Header=BB46_22 Depth=2
	sarl	$5, %edx
	movl	$4095, %ecx             # imm = 0xFFF
	cmpl	$2047, %edx             # imm = 0x7FF
	movq	-24(%rsp), %r13         # 8-byte Reload
	jg	.LBB46_36
# BB#34:                                #   in Loop: Header=BB46_22 Depth=2
	xorl	%ecx, %ecx
	cmpl	$-2047, %edx            # imm = 0xF801
	jl	.LBB46_36
.LBB46_35:                              # %.thread
                                        #   in Loop: Header=BB46_22 Depth=2
	movl	%edx, %ecx
	andl	$127, %ecx
	sarl	$7, %edx
	movl	$128, %esi
	subl	%ecx, %esi
	movslq	%edx, %rdx
	imull	_ZZ6squashiE1t+64(,%rdx,4), %esi
	imull	_ZZ6squashiE1t+68(,%rdx,4), %ecx
	leal	64(%rsi,%rcx), %ecx
	sarl	$7, %ecx
.LBB46_36:                              # %_Z6squashi.exit12
                                        #   in Loop: Header=BB46_22 Depth=2
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movl	%ecx, (%rdx,%r12,4)
	movslq	%ecx, %rcx
	movzwl	(%r9,%rcx,2), %ecx
	movslq	96(%r13), %rdx
	leal	1(%rdx), %esi
	movl	%esi, 96(%r13)
	movq	-16(%rsp), %rsi         # 8-byte Reload
	movw	%cx, (%rsi,%rdx,2)
	incq	%r12
	movslq	88(%r8), %rcx
	cmpq	%rcx, %r12
	jl	.LBB46_22
	jmp	.LBB46_19
.LBB46_37:
	addl	$7, %edx
	andl	$-8, %edx
	testl	%edx, %edx
	jle	.LBB46_38
# BB#39:                                # %.lr.ph.preheader.i
	movq	32(%rdi), %r11
	movq	56(%rdi), %rcx
	movslq	%edx, %r10
	leaq	-1(%r10), %r9
	shrq	%r9
	incq	%r9
	cmpq	$8, %r9
	jb	.LBB46_40
# BB#41:                                # %min.iters.checked80
	movq	%r9, %rsi
	andq	$-8, %rsi
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB46_40
# BB#42:                                # %vector.body76.preheader
	addq	%rsi, %rsi
	leaq	16(%r11), %rbx
	leaq	16(%rcx), %rbp
	pxor	%xmm0, %xmm0
	movq	%r8, %rdx
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB46_43:                              # %vector.body76
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbx), %xmm6
	movdqu	(%rbx), %xmm9
	movdqu	-16(%rbp), %xmm7
	movdqu	(%rbp), %xmm8
	pshuflw	$232, %xmm6, %xmm2      # xmm2 = xmm6[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm5      # xmm5 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm7, %xmm2      # xmm2 = xmm7[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm3      # xmm3 = xmm2[0,2,2,3]
	movdqa	%xmm3, %xmm2
	pmulhw	%xmm5, %xmm2
	pmullw	%xmm5, %xmm3
	punpcklwd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3]
	pshuflw	$232, %xmm9, %xmm2      # xmm2 = xmm9[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm8, %xmm5      # xmm5 = xmm8[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	movdqa	%xmm4, %xmm5
	pmulhw	%xmm2, %xmm5
	pmullw	%xmm2, %xmm4
	punpcklwd	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1],xmm4[2],xmm5[2],xmm4[3],xmm5[3]
	pshuflw	$231, %xmm6, %xmm2      # xmm2 = xmm6[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$177, %xmm2, %xmm2      # xmm2 = xmm2[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm7, %xmm5      # xmm5 = xmm7[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm5, %xmm5      # xmm5 = xmm5[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshuflw	$177, %xmm5, %xmm5      # xmm5 = xmm5[1,0,3,2,4,5,6,7]
	movdqa	%xmm5, %xmm6
	pmulhw	%xmm2, %xmm6
	pmullw	%xmm2, %xmm5
	punpcklwd	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1],xmm5[2],xmm6[2],xmm5[3],xmm6[3]
	pshuflw	$231, %xmm9, %xmm2      # xmm2 = xmm9[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$177, %xmm2, %xmm2      # xmm2 = xmm2[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm8, %xmm6      # xmm6 = xmm8[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm6, %xmm6      # xmm6 = xmm6[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshuflw	$177, %xmm6, %xmm6      # xmm6 = xmm6[1,0,3,2,4,5,6,7]
	movdqa	%xmm6, %xmm7
	pmulhw	%xmm2, %xmm7
	pmullw	%xmm2, %xmm6
	punpcklwd	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1],xmm6[2],xmm7[2],xmm6[3],xmm7[3]
	paddd	%xmm3, %xmm5
	paddd	%xmm4, %xmm6
	psrad	$8, %xmm5
	psrad	$8, %xmm6
	paddd	%xmm5, %xmm0
	paddd	%xmm6, %xmm1
	addq	$32, %rbx
	addq	$32, %rbp
	addq	$-8, %rdx
	jne	.LBB46_43
# BB#44:                                # %middle.block77
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edx
	cmpq	%r8, %r9
	jne	.LBB46_45
	jmp	.LBB46_46
.LBB46_40:
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB46_45:                              # %.lr.ph.i10
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%r11,%rsi,2), %ebx
	movswl	(%rcx,%rsi,2), %ebp
	imull	%ebx, %ebp
	movswl	2(%r11,%rsi,2), %eax
	movswl	2(%rcx,%rsi,2), %ebx
	imull	%eax, %ebx
	addl	%ebp, %ebx
	sarl	$8, %ebx
	addl	%ebx, %edx
	addq	$2, %rsi
	cmpq	%r10, %rsi
	jl	.LBB46_45
.LBB46_46:                              # %_Z11dot_productPsS_i.exit
	sarl	$8, %edx
	movl	$4095, %eax             # imm = 0xFFF
	cmpl	$2047, %edx             # imm = 0x7FF
	jg	.LBB46_49
# BB#47:
	xorl	%eax, %eax
	cmpl	$-2047, %edx            # imm = 0xF801
	jge	.LBB46_48
	jmp	.LBB46_49
.LBB46_38:
	xorl	%edx, %edx
.LBB46_48:                              # %.thread20
	movl	%edx, %eax
	andl	$127, %eax
	sarl	$7, %edx
	movl	$128, %ecx
	subl	%eax, %ecx
	movslq	%edx, %rdx
	imull	_ZZ6squashiE1t+64(,%rdx,4), %ecx
	imull	_ZZ6squashiE1t+68(,%rdx,4), %eax
	leal	64(%rcx,%rax), %eax
	sarl	$7, %eax
.LBB46_49:                              # %_Z6squashi.exit
	movq	120(%rdi), %rcx
	movl	%eax, (%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end46:
	.size	_ZN5Mixer1pEv, .Lfunc_end46-_ZN5Mixer1pEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI47_0:
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
.LCPI47_1:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI47_2:
	.quad	4607146390002998444     # double 0.99599999999999999
	.text
	.globl	_Z8wavModelR5Mixer
	.p2align	4, 0x90
	.type	_Z8wavModelR5Mixer,@function
_Z8wavModelR5Mixer:                     # @_Z8wavModelR5Mixer
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%rbp
.Lcfi202:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi203:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi204:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi205:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi206:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi207:
	.cfi_def_cfa_offset 56
	subq	$57720, %rsp            # imm = 0xE178
.Lcfi208:
	.cfi_def_cfa_offset 57776
.Lcfi209:
	.cfi_offset %rbx, -56
.Lcfi210:
	.cfi_offset %r12, -48
.Lcfi211:
	.cfi_offset %r13, -40
.Lcfi212:
	.cfi_offset %r14, -32
.Lcfi213:
	.cfi_offset %r15, -24
.Lcfi214:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movb	_ZGVZ8wavModelR5MixerE1K(%rip), %al
	testb	%al, %al
	jne	.LBB47_3
# BB#1:
	movl	$_ZGVZ8wavModelR5MixerE1K, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_3
# BB#2:
	movl	level(%rip), %ecx
	decl	%ecx
	movl	$128, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	%eax, _ZZ8wavModelR5MixerE1K(%rip)
	movl	$_ZGVZ8wavModelR5MixerE1K, %edi
	callq	__cxa_guard_release
.LBB47_3:
	movb	_ZGVZ8wavModelR5MixerE4scm1(%rip), %al
	testb	%al, %al
	jne	.LBB47_13
# BB#4:
	movl	$_ZGVZ8wavModelR5MixerE4scm1, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_13
# BB#5:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8wavModelR5MixerE4scm1(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB47_7
# BB#6:
	movl	%eax, programChecker+4(%rip)
.LBB47_7:                               # %_ZN14ProgramChecker5allocEi.exit.i.i.i
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8wavModelR5MixerE4scm1+8(%rip)
	testq	%rax, %rax
	je	.LBB47_8
# BB#10:                                # %.lr.ph.i
	movq	%rax, _ZZ8wavModelR5MixerE4scm1+16(%rip)
	movl	$0, _ZZ8wavModelR5MixerE4scm1+24(%rip)
	movl	$120, %ecx
	movapd	.LCPI47_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB47_11:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rax,%rcx,2)
	movupd	%xmm0, -224(%rax,%rcx,2)
	movupd	%xmm0, -208(%rax,%rcx,2)
	movupd	%xmm0, -192(%rax,%rcx,2)
	movupd	%xmm0, -176(%rax,%rcx,2)
	movupd	%xmm0, -160(%rax,%rcx,2)
	movupd	%xmm0, -144(%rax,%rcx,2)
	movupd	%xmm0, -128(%rax,%rcx,2)
	movupd	%xmm0, -112(%rax,%rcx,2)
	movupd	%xmm0, -96(%rax,%rcx,2)
	movupd	%xmm0, -80(%rax,%rcx,2)
	movupd	%xmm0, -64(%rax,%rcx,2)
	movupd	%xmm0, -48(%rax,%rcx,2)
	movupd	%xmm0, -32(%rax,%rcx,2)
	movupd	%xmm0, -16(%rax,%rcx,2)
	movupd	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB47_11
# BB#12:                                # %middle.block
	movq	%rax, _ZZ8wavModelR5MixerE4scm1+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8wavModelR5MixerE4scm1, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8wavModelR5MixerE4scm1, %edi
	callq	__cxa_guard_release
.LBB47_13:
	movb	_ZGVZ8wavModelR5MixerE4scm2(%rip), %al
	testb	%al, %al
	jne	.LBB47_23
# BB#14:
	movl	$_ZGVZ8wavModelR5MixerE4scm2, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_23
# BB#15:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8wavModelR5MixerE4scm2(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB47_17
# BB#16:
	movl	%eax, programChecker+4(%rip)
.LBB47_17:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i298
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8wavModelR5MixerE4scm2+8(%rip)
	testq	%rax, %rax
	je	.LBB47_18
# BB#20:                                # %.lr.ph.i299
	movq	%rax, _ZZ8wavModelR5MixerE4scm2+16(%rip)
	movl	$0, _ZZ8wavModelR5MixerE4scm2+24(%rip)
	movl	$120, %ecx
	movapd	.LCPI47_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB47_21:                              # %vector.body785
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rax,%rcx,2)
	movupd	%xmm0, -224(%rax,%rcx,2)
	movupd	%xmm0, -208(%rax,%rcx,2)
	movupd	%xmm0, -192(%rax,%rcx,2)
	movupd	%xmm0, -176(%rax,%rcx,2)
	movupd	%xmm0, -160(%rax,%rcx,2)
	movupd	%xmm0, -144(%rax,%rcx,2)
	movupd	%xmm0, -128(%rax,%rcx,2)
	movupd	%xmm0, -112(%rax,%rcx,2)
	movupd	%xmm0, -96(%rax,%rcx,2)
	movupd	%xmm0, -80(%rax,%rcx,2)
	movupd	%xmm0, -64(%rax,%rcx,2)
	movupd	%xmm0, -48(%rax,%rcx,2)
	movupd	%xmm0, -32(%rax,%rcx,2)
	movupd	%xmm0, -16(%rax,%rcx,2)
	movupd	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB47_21
# BB#22:                                # %middle.block786
	movq	%rax, _ZZ8wavModelR5MixerE4scm2+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8wavModelR5MixerE4scm2, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8wavModelR5MixerE4scm2, %edi
	callq	__cxa_guard_release
.LBB47_23:
	movb	_ZGVZ8wavModelR5MixerE4scm3(%rip), %al
	testb	%al, %al
	jne	.LBB47_33
# BB#24:
	movl	$_ZGVZ8wavModelR5MixerE4scm3, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_33
# BB#25:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8wavModelR5MixerE4scm3(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB47_27
# BB#26:
	movl	%eax, programChecker+4(%rip)
.LBB47_27:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i304
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8wavModelR5MixerE4scm3+8(%rip)
	testq	%rax, %rax
	je	.LBB47_28
# BB#30:                                # %.lr.ph.i305
	movq	%rax, _ZZ8wavModelR5MixerE4scm3+16(%rip)
	movl	$0, _ZZ8wavModelR5MixerE4scm3+24(%rip)
	movl	$120, %ecx
	movapd	.LCPI47_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB47_31:                              # %vector.body798
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rax,%rcx,2)
	movupd	%xmm0, -224(%rax,%rcx,2)
	movupd	%xmm0, -208(%rax,%rcx,2)
	movupd	%xmm0, -192(%rax,%rcx,2)
	movupd	%xmm0, -176(%rax,%rcx,2)
	movupd	%xmm0, -160(%rax,%rcx,2)
	movupd	%xmm0, -144(%rax,%rcx,2)
	movupd	%xmm0, -128(%rax,%rcx,2)
	movupd	%xmm0, -112(%rax,%rcx,2)
	movupd	%xmm0, -96(%rax,%rcx,2)
	movupd	%xmm0, -80(%rax,%rcx,2)
	movupd	%xmm0, -64(%rax,%rcx,2)
	movupd	%xmm0, -48(%rax,%rcx,2)
	movupd	%xmm0, -32(%rax,%rcx,2)
	movupd	%xmm0, -16(%rax,%rcx,2)
	movupd	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB47_31
# BB#32:                                # %middle.block799
	movq	%rax, _ZZ8wavModelR5MixerE4scm3+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8wavModelR5MixerE4scm3, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8wavModelR5MixerE4scm3, %edi
	callq	__cxa_guard_release
.LBB47_33:
	movb	_ZGVZ8wavModelR5MixerE4scm4(%rip), %al
	testb	%al, %al
	jne	.LBB47_43
# BB#34:
	movl	$_ZGVZ8wavModelR5MixerE4scm4, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_43
# BB#35:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8wavModelR5MixerE4scm4(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB47_37
# BB#36:
	movl	%eax, programChecker+4(%rip)
.LBB47_37:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i310
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8wavModelR5MixerE4scm4+8(%rip)
	testq	%rax, %rax
	je	.LBB47_38
# BB#40:                                # %.lr.ph.i311
	movq	%rax, _ZZ8wavModelR5MixerE4scm4+16(%rip)
	movl	$0, _ZZ8wavModelR5MixerE4scm4+24(%rip)
	movl	$120, %ecx
	movapd	.LCPI47_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB47_41:                              # %vector.body811
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rax,%rcx,2)
	movupd	%xmm0, -224(%rax,%rcx,2)
	movupd	%xmm0, -208(%rax,%rcx,2)
	movupd	%xmm0, -192(%rax,%rcx,2)
	movupd	%xmm0, -176(%rax,%rcx,2)
	movupd	%xmm0, -160(%rax,%rcx,2)
	movupd	%xmm0, -144(%rax,%rcx,2)
	movupd	%xmm0, -128(%rax,%rcx,2)
	movupd	%xmm0, -112(%rax,%rcx,2)
	movupd	%xmm0, -96(%rax,%rcx,2)
	movupd	%xmm0, -80(%rax,%rcx,2)
	movupd	%xmm0, -64(%rax,%rcx,2)
	movupd	%xmm0, -48(%rax,%rcx,2)
	movupd	%xmm0, -32(%rax,%rcx,2)
	movupd	%xmm0, -16(%rax,%rcx,2)
	movupd	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB47_41
# BB#42:                                # %middle.block812
	movq	%rax, _ZZ8wavModelR5MixerE4scm4+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8wavModelR5MixerE4scm4, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8wavModelR5MixerE4scm4, %edi
	callq	__cxa_guard_release
.LBB47_43:
	movb	_ZGVZ8wavModelR5MixerE4scm5(%rip), %al
	testb	%al, %al
	jne	.LBB47_53
# BB#44:
	movl	$_ZGVZ8wavModelR5MixerE4scm5, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_53
# BB#45:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8wavModelR5MixerE4scm5(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB47_47
# BB#46:
	movl	%eax, programChecker+4(%rip)
.LBB47_47:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i316
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8wavModelR5MixerE4scm5+8(%rip)
	testq	%rax, %rax
	je	.LBB47_48
# BB#50:                                # %.lr.ph.i317
	movq	%rax, _ZZ8wavModelR5MixerE4scm5+16(%rip)
	movl	$0, _ZZ8wavModelR5MixerE4scm5+24(%rip)
	movl	$120, %ecx
	movapd	.LCPI47_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB47_51:                              # %vector.body824
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rax,%rcx,2)
	movupd	%xmm0, -224(%rax,%rcx,2)
	movupd	%xmm0, -208(%rax,%rcx,2)
	movupd	%xmm0, -192(%rax,%rcx,2)
	movupd	%xmm0, -176(%rax,%rcx,2)
	movupd	%xmm0, -160(%rax,%rcx,2)
	movupd	%xmm0, -144(%rax,%rcx,2)
	movupd	%xmm0, -128(%rax,%rcx,2)
	movupd	%xmm0, -112(%rax,%rcx,2)
	movupd	%xmm0, -96(%rax,%rcx,2)
	movupd	%xmm0, -80(%rax,%rcx,2)
	movupd	%xmm0, -64(%rax,%rcx,2)
	movupd	%xmm0, -48(%rax,%rcx,2)
	movupd	%xmm0, -32(%rax,%rcx,2)
	movupd	%xmm0, -16(%rax,%rcx,2)
	movupd	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB47_51
# BB#52:                                # %middle.block825
	movq	%rax, _ZZ8wavModelR5MixerE4scm5+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8wavModelR5MixerE4scm5, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8wavModelR5MixerE4scm5, %edi
	callq	__cxa_guard_release
.LBB47_53:
	movb	_ZGVZ8wavModelR5MixerE4scm6(%rip), %al
	testb	%al, %al
	jne	.LBB47_63
# BB#54:
	movl	$_ZGVZ8wavModelR5MixerE4scm6, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_63
# BB#55:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8wavModelR5MixerE4scm6(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB47_57
# BB#56:
	movl	%eax, programChecker+4(%rip)
.LBB47_57:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i322
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8wavModelR5MixerE4scm6+8(%rip)
	testq	%rax, %rax
	je	.LBB47_58
# BB#60:                                # %.lr.ph.i323
	movq	%rax, _ZZ8wavModelR5MixerE4scm6+16(%rip)
	movl	$0, _ZZ8wavModelR5MixerE4scm6+24(%rip)
	movl	$120, %ecx
	movapd	.LCPI47_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB47_61:                              # %vector.body837
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rax,%rcx,2)
	movupd	%xmm0, -224(%rax,%rcx,2)
	movupd	%xmm0, -208(%rax,%rcx,2)
	movupd	%xmm0, -192(%rax,%rcx,2)
	movupd	%xmm0, -176(%rax,%rcx,2)
	movupd	%xmm0, -160(%rax,%rcx,2)
	movupd	%xmm0, -144(%rax,%rcx,2)
	movupd	%xmm0, -128(%rax,%rcx,2)
	movupd	%xmm0, -112(%rax,%rcx,2)
	movupd	%xmm0, -96(%rax,%rcx,2)
	movupd	%xmm0, -80(%rax,%rcx,2)
	movupd	%xmm0, -64(%rax,%rcx,2)
	movupd	%xmm0, -48(%rax,%rcx,2)
	movupd	%xmm0, -32(%rax,%rcx,2)
	movupd	%xmm0, -16(%rax,%rcx,2)
	movupd	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB47_61
# BB#62:                                # %middle.block838
	movq	%rax, _ZZ8wavModelR5MixerE4scm6+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8wavModelR5MixerE4scm6, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8wavModelR5MixerE4scm6, %edi
	callq	__cxa_guard_release
.LBB47_63:
	movb	_ZGVZ8wavModelR5MixerE4scm7(%rip), %al
	testb	%al, %al
	jne	.LBB47_73
# BB#64:
	movl	$_ZGVZ8wavModelR5MixerE4scm7, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_73
# BB#65:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8wavModelR5MixerE4scm7(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB47_67
# BB#66:
	movl	%eax, programChecker+4(%rip)
.LBB47_67:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i328
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8wavModelR5MixerE4scm7+8(%rip)
	testq	%rax, %rax
	je	.LBB47_68
# BB#70:                                # %.lr.ph.i329
	movq	%rax, _ZZ8wavModelR5MixerE4scm7+16(%rip)
	movl	$0, _ZZ8wavModelR5MixerE4scm7+24(%rip)
	movl	$120, %ecx
	movapd	.LCPI47_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB47_71:                              # %vector.body850
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rax,%rcx,2)
	movupd	%xmm0, -224(%rax,%rcx,2)
	movupd	%xmm0, -208(%rax,%rcx,2)
	movupd	%xmm0, -192(%rax,%rcx,2)
	movupd	%xmm0, -176(%rax,%rcx,2)
	movupd	%xmm0, -160(%rax,%rcx,2)
	movupd	%xmm0, -144(%rax,%rcx,2)
	movupd	%xmm0, -128(%rax,%rcx,2)
	movupd	%xmm0, -112(%rax,%rcx,2)
	movupd	%xmm0, -96(%rax,%rcx,2)
	movupd	%xmm0, -80(%rax,%rcx,2)
	movupd	%xmm0, -64(%rax,%rcx,2)
	movupd	%xmm0, -48(%rax,%rcx,2)
	movupd	%xmm0, -32(%rax,%rcx,2)
	movupd	%xmm0, -16(%rax,%rcx,2)
	movupd	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB47_71
# BB#72:                                # %middle.block851
	movq	%rax, _ZZ8wavModelR5MixerE4scm7+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8wavModelR5MixerE4scm7, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8wavModelR5MixerE4scm7, %edi
	callq	__cxa_guard_release
.LBB47_73:
	movb	_ZGVZ8wavModelR5MixerE4scm8(%rip), %al
	testb	%al, %al
	jne	.LBB47_83
# BB#74:
	movl	$_ZGVZ8wavModelR5MixerE4scm8, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_83
# BB#75:
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, _ZZ8wavModelR5MixerE4scm8(%rip)
	movl	$131072, %eax           # imm = 0x20000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB47_77
# BB#76:
	movl	%eax, programChecker+4(%rip)
.LBB47_77:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i334
	movl	$131072, %edi           # imm = 0x20000
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8wavModelR5MixerE4scm8+8(%rip)
	testq	%rax, %rax
	je	.LBB47_78
# BB#80:                                # %.lr.ph.i335
	movq	%rax, _ZZ8wavModelR5MixerE4scm8+16(%rip)
	movl	$0, _ZZ8wavModelR5MixerE4scm8+24(%rip)
	movl	$120, %ecx
	movapd	.LCPI47_0(%rip), %xmm0  # xmm0 = [32768,32768,32768,32768,32768,32768,32768,32768]
	.p2align	4, 0x90
.LBB47_81:                              # %vector.body863
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, -240(%rax,%rcx,2)
	movupd	%xmm0, -224(%rax,%rcx,2)
	movupd	%xmm0, -208(%rax,%rcx,2)
	movupd	%xmm0, -192(%rax,%rcx,2)
	movupd	%xmm0, -176(%rax,%rcx,2)
	movupd	%xmm0, -160(%rax,%rcx,2)
	movupd	%xmm0, -144(%rax,%rcx,2)
	movupd	%xmm0, -128(%rax,%rcx,2)
	movupd	%xmm0, -112(%rax,%rcx,2)
	movupd	%xmm0, -96(%rax,%rcx,2)
	movupd	%xmm0, -80(%rax,%rcx,2)
	movupd	%xmm0, -64(%rax,%rcx,2)
	movupd	%xmm0, -48(%rax,%rcx,2)
	movupd	%xmm0, -32(%rax,%rcx,2)
	movupd	%xmm0, -16(%rax,%rcx,2)
	movupd	%xmm0, (%rax,%rcx,2)
	subq	$-128, %rcx
	cmpq	$65656, %rcx            # imm = 0x10078
	jne	.LBB47_81
# BB#82:                                # %middle.block864
	movq	%rax, _ZZ8wavModelR5MixerE4scm8+32(%rip)
	movl	$_ZN25SmallStationaryContextMapD2Ev, %edi
	movl	$_ZZ8wavModelR5MixerE4scm8, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8wavModelR5MixerE4scm8, %edi
	callq	__cxa_guard_release
.LBB47_83:
	movb	_ZGVZ8wavModelR5MixerE2cm(%rip), %al
	testb	%al, %al
	jne	.LBB47_87
# BB#84:
	movl	$_ZGVZ8wavModelR5MixerE2cm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB47_87
# BB#85:
	movb	level(%rip), %cl
	movl	$262144, %esi           # imm = 0x40000
	shll	%cl, %esi
.Ltmp216:
	movl	$_ZZ8wavModelR5MixerE2cm, %edi
	movl	$10, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp217:
# BB#86:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ8wavModelR5MixerE2cm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8wavModelR5MixerE2cm, %edi
	callq	__cxa_guard_release
.LBB47_87:
	cmpl	$0, bpos(%rip)
	jne	.LBB47_141
# BB#88:
	movl	pos(%rip), %eax
	leal	-8(%rax), %edi
	movl	buf(%rip), %ecx
	leal	-1(%rcx), %esi
	andl	%esi, %edi
	movq	buf+16(%rip), %rdx
	movslq	%edi, %rdi
	cmpb	$100, (%rdx,%rdi)
	jne	.LBB47_141
# BB#89:
	leal	-7(%rax), %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	cmpb	$97, (%rdx,%rdi)
	jne	.LBB47_141
# BB#90:
	leal	-6(%rax), %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	cmpb	$116, (%rdx,%rdi)
	jne	.LBB47_141
# BB#91:
	leal	-5(%rax), %edi
	andl	%edi, %esi
	movslq	%esi, %rsi
	cmpb	$97, (%rdx,%rsi)
	jne	.LBB47_141
# BB#92:                                # %.preheader555.preheader
	movl	$32, %ebx
	movl	$-32, %esi
	movq	%r14, 40(%rsp)          # 8-byte Spill
	jmp	.LBB47_93
	.p2align	4, 0x90
.LBB47_140:                             # %..preheader555_crit_edge
                                        #   in Loop: Header=BB47_93 Depth=1
	notl	%ebx
	movl	pos(%rip), %eax
	movl	buf(%rip), %ecx
	movq	buf+16(%rip), %rdx
	movl	%ebx, %esi
	movl	%edi, %ebx
.LBB47_93:                              # %.preheader555
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB47_105 Depth 2
                                        #       Child Loop BB47_124 Depth 3
                                        #       Child Loop BB47_128 Depth 3
                                        #         Child Loop BB47_129 Depth 4
                                        #         Child Loop BB47_131 Depth 4
                                        #     Child Loop BB47_120 Depth 2
                                        #     Child Loop BB47_122 Depth 2
	movl	%eax, %edi
	subl	%ebx, %edi
	decl	%ecx
	andl	%ecx, %edi
	movslq	%edi, %rdi
	cmpb	$102, (%rdx,%rdi)
	jne	.LBB47_139
# BB#94:                                #   in Loop: Header=BB47_93 Depth=1
	leal	1(%rsi,%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	cmpb	$109, (%rdx,%rdi)
	jne	.LBB47_139
# BB#95:                                #   in Loop: Header=BB47_93 Depth=1
	leal	2(%rsi,%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	cmpb	$116, (%rdx,%rdi)
	jne	.LBB47_139
# BB#96:                                #   in Loop: Header=BB47_93 Depth=1
	leal	3(%rsi,%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	cmpb	$32, (%rdx,%rdi)
	jne	.LBB47_139
# BB#97:                                #   in Loop: Header=BB47_93 Depth=1
	leal	8(%rsi,%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	leal	9(%rsi,%rax), %ebp
	andl	%ecx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rdx,%rbp), %ebp
	shll	$8, %ebp
	orl	%edi, %ebp
	movzwl	%bp, %edi
	cmpl	$1, %edi
	je	.LBB47_99
# BB#98:                                #   in Loop: Header=BB47_93 Depth=1
	cmpl	$65534, %edi            # imm = 0xFFFE
	jne	.LBB47_139
.LBB47_99:                              #   in Loop: Header=BB47_93 Depth=1
	leal	22(%rsi,%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %r15d
	movl	%r15d, _ZZ8wavModelR5MixerE4bits(%rip)
	leal	7(%r15), %edi
	shrl	$3, %edi
	movl	%edi, _ZZ8wavModelR5MixerE5bytes(%rip)
	leal	10(%rsi,%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %r14d
	movl	%r14d, _ZZ8wavModelR5MixerE8channels(%rip)
	imull	%r14d, %edi
	movl	%edi, _ZZ8wavModelR5MixerE1w(%rip)
	leal	-4(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	leal	-3(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	leal	-2(%rax), %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$16, %esi
	orl	%edi, %esi
	leal	-1(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rcx
	movzbl	(%rdx,%rcx), %ecx
	shll	$24, %ecx
	orl	%esi, %ecx
	movl	%ecx, _ZZ8wavModelR5MixerE1s(%rip)
	leal	-1(%r14), %edx
	cmpl	$1, %edx
	ja	.LBB47_138
# BB#100:                               #   in Loop: Header=BB47_93 Depth=1
	cmpb	$16, %r15b
	je	.LBB47_102
# BB#101:                               #   in Loop: Header=BB47_93 Depth=1
	cmpb	$8, %r15b
	jne	.LBB47_138
.LBB47_102:                             #   in Loop: Header=BB47_93 Depth=1
	addl	%ecx, %eax
	movl	%eax, _ZZ8wavModelR5MixerE3eof(%rip)
	testb	%r14b, %r14b
	je	.LBB47_134
# BB#103:                               # %.preheader554.lr.ph
                                        #   in Loop: Header=BB47_93 Depth=1
	movl	_ZL1S(%rip), %eax
	movl	_ZL1D(%rip), %ecx
	movl	%ecx, %edx
	addl	%eax, %edx
	js	.LBB47_115
# BB#104:                               # %.preheader554.preheader
                                        #   in Loop: Header=BB47_93 Depth=1
	movslq	%edx, %r12
	leal	1(%rcx,%rax), %r13d
	cmpl	$1, %r14d
	movl	$1, %eax
	cmoval	%r14d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rbp
	movl	$_ZZ8wavModelR5MixerE7counter, %edi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movl	$_ZZ8wavModelR5MixerE1n, %edi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movl	%r13d, %r8d
	andl	$1, %r8d
	leaq	20096(%rsp), %r9
	leaq	19296(%rsp), %r10
	xorl	%r11d, %r11d
.LBB47_105:                             # %.preheader554
                                        #   Parent Loop BB47_93 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB47_124 Depth 3
                                        #       Child Loop BB47_128 Depth 3
                                        #         Child Loop BB47_129 Depth 4
                                        #         Child Loop BB47_131 Depth 4
	testq	%r8, %r8
	jne	.LBB47_123
# BB#106:                               #   in Loop: Header=BB47_105 Depth=2
	xorl	%esi, %esi
	cmpl	$1, %r13d
	jne	.LBB47_127
	jmp	.LBB47_133
.LBB47_123:                             # %.lr.ph612.prol
                                        #   in Loop: Header=BB47_105 Depth=2
	movq	$-1, %rax
	movq	%r10, %rcx
	.p2align	4, 0x90
.LBB47_124:                             #   Parent Loop BB47_93 Depth=1
                                        #     Parent Loop BB47_105 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	$0, (%rcx)
	incq	%rax
	addq	$16, %rcx
	cmpq	%r12, %rax
	jl	.LBB47_124
# BB#125:                               #   in Loop: Header=BB47_105 Depth=2
	movl	$1, %esi
	cmpl	$1, %r13d
	je	.LBB47_133
.LBB47_127:                             # %.preheader554.new
                                        #   in Loop: Header=BB47_105 Depth=2
	leaq	-1(%rsi), %rax
	imulq	$800, %rsi, %rdx        # imm = 0x320
	leaq	(%r10,%rdx), %rcx
	addq	%r9, %rdx
	.p2align	4, 0x90
.LBB47_128:                             # %.lr.ph612
                                        #   Parent Loop BB47_93 Depth=1
                                        #     Parent Loop BB47_105 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB47_129 Depth 4
                                        #         Child Loop BB47_131 Depth 4
	movq	%rcx, %rdi
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB47_129:                             #   Parent Loop BB47_93 Depth=1
                                        #     Parent Loop BB47_105 Depth=2
                                        #       Parent Loop BB47_128 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	$0, (%rdi)
	incq	%rbp
	addq	$16, %rdi
	cmpq	%r12, %rbp
	jl	.LBB47_129
# BB#130:                               #   in Loop: Header=BB47_128 Depth=3
	movq	%rdx, %rbp
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB47_131:                             #   Parent Loop BB47_93 Depth=1
                                        #     Parent Loop BB47_105 Depth=2
                                        #       Parent Loop BB47_128 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	$0, (%rbp)
	incq	%rdi
	addq	$16, %rbp
	cmpq	%r12, %rdi
	jl	.LBB47_131
# BB#132:                               # %._crit_edge613.1
                                        #   in Loop: Header=BB47_128 Depth=3
	addq	$2, %rsi
	addq	$2, %rax
	addq	$1600, %rcx             # imm = 0x640
	addq	$1600, %rdx             # imm = 0x640
	cmpq	%r13, %rsi
	jne	.LBB47_128
.LBB47_133:                             # %._crit_edge615
                                        #   in Loop: Header=BB47_105 Depth=2
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 20080(%rsp,%r11,8)
	incq	%r11
	addq	$8, %r10
	addq	$8, %r9
	cmpq	%r14, %r11
	jl	.LBB47_105
	jmp	.LBB47_134
.LBB47_138:                             #   in Loop: Header=BB47_93 Depth=1
	movl	%eax, _ZZ8wavModelR5MixerE3eof(%rip)
	movq	40(%rsp), %r14          # 8-byte Reload
	jmp	.LBB47_139
.LBB47_115:                             # %.preheader554.us.preheader
                                        #   in Loop: Header=BB47_93 Depth=1
	cmpl	$1, %r14d
	movl	$1, %eax
	cmoval	%r14d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rbp
	movl	$_ZZ8wavModelR5MixerE7counter, %edi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movl	$_ZZ8wavModelR5MixerE1n, %edi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	cmpb	$4, %r14b
	jae	.LBB47_117
# BB#116:                               #   in Loop: Header=BB47_93 Depth=1
	xorl	%eax, %eax
	jmp	.LBB47_122
.LBB47_117:                             # %min.iters.checked879
                                        #   in Loop: Header=BB47_93 Depth=1
	movl	%r14d, %ecx
	andl	$3, %ecx
	movq	%r14, %rax
	subq	%rcx, %rax
	movapd	.LCPI47_1(%rip), %xmm0  # xmm0 = [1.000000e+00,1.000000e+00]
	je	.LBB47_118
# BB#119:                               # %vector.body876.preheader
                                        #   in Loop: Header=BB47_93 Depth=1
	movl	%r14d, %ecx
	andb	$3, %cl
	movq	%rax, %rdx
	leaq	20096(%rsp), %rsi
.LBB47_120:                             # %vector.body876
                                        #   Parent Loop BB47_93 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB47_120
# BB#121:                               # %middle.block877
                                        #   in Loop: Header=BB47_93 Depth=1
	testb	%cl, %cl
	jne	.LBB47_122
	jmp	.LBB47_134
.LBB47_118:                             #   in Loop: Header=BB47_93 Depth=1
	xorl	%eax, %eax
.LBB47_122:                             # %.preheader554.us
                                        #   Parent Loop BB47_93 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, 20080(%rsp,%rax,8)
	incq	%rax
	cmpq	%r14, %rax
	jl	.LBB47_122
.LBB47_134:                             # %._crit_edge617
                                        #   in Loop: Header=BB47_93 Depth=1
	addl	%r15d, %r14d
	movl	%r14d, _ZL5wmode(%rip)
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	printf
	cmpl	$1, _ZZ8wavModelR5MixerE8channels(%rip)
	jne	.LBB47_136
# BB#135:                               #   in Loop: Header=BB47_93 Depth=1
	xorl	%ebp, %ebp
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$48, %eax
	jmp	.LBB47_137
.LBB47_136:                             #   in Loop: Header=BB47_93 Depth=1
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$12, %ebp
	movl	$36, %eax
.LBB47_137:                             #   in Loop: Header=BB47_93 Depth=1
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	%eax, _ZL1S(%rip)
	movl	%ebp, _ZL1D(%rip)
	.p2align	4, 0x90
.LBB47_139:                             #   in Loop: Header=BB47_93 Depth=1
	leal	1(%rbx), %edi
	cmpl	$1001, %edi             # imm = 0x3E9
	jne	.LBB47_140
.LBB47_141:                             # %.loopexit556
	movl	pos(%rip), %ebp
	movl	%ebp, %ecx
	subl	_ZZ8wavModelR5MixerE3eof(%rip), %ecx
	jle	.LBB47_143
# BB#142:
	movl	$0, _ZZ8wavModelR5MixerE8channels(%rip)
	movl	$0, _ZZ8wavModelR5MixerE4bits(%rip)
	xorl	%eax, %eax
	jmp	.LBB47_271
.LBB47_143:
	cmpl	$0, bpos(%rip)
	jne	.LBB47_270
# BB#144:
	addl	_ZZ8wavModelR5MixerE1s(%rip), %ecx
	movl	_ZZ8wavModelR5MixerE5bytes(%rip), %ebx
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%edx, %esi
	movl	_ZZ8wavModelR5MixerE1w(%rip), %r12d
	movl	%ecx, %eax
	cltd
	idivl	%r12d
	movl	%edx, %r8d
	movl	%r8d, %eax
	cltd
	idivl	%ebx
	movl	%eax, 16(%rsp)          # 4-byte Spill
	testl	%esi, %esi
	movq	%r14, 40(%rsp)          # 8-byte Spill
	je	.LBB47_145
# BB#244:
	decl	%ebp
	movl	buf(%rip), %eax
	decl	%eax
	andl	%ebp, %eax
	movq	buf+16(%rip), %r11
	cltq
	movzbl	(%r11,%rax), %ecx
	imull	$30005491, %ecx, %eax   # imm = 0x1C9D8F3
	imull	$50004239, %r8d, %edx   # imm = 0x2FB010F
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leal	19995673(%rdx,%rax), %esi
	shrl	$3, %ecx
	movl	%r8d, %r10d
	shrl	$4, %r10d
	movl	%r10d, %eax
	xorl	$67108864, %eax         # imm = 0x4000000
	xorl	%eax, %ecx
	xorl	%esi, %ecx
	shrl	$9, %esi
	xorl	%ecx, %esi
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %ebp
	movl	%ebp, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%ecx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%ecx, %esi
	movq	_ZZ8wavModelR5MixerE2cm+96(%rip), %r15
	movl	%esi, (%r15,%rcx,4)
	movl	pos(%rip), %ebp
	leal	-1(%rbp), %ecx
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %esi
	shrl	$7, %esi
	leal	-2(%rbp), %ecx
	andl	%ebx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	addl	$-3, %ebp
	andl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%r11,%rbp), %ebp
	shrl	$7, %ebp
	negl	%esi
	andl	$30005491, %esi         # imm = 0x1C9D8F3
	imull	$50004239, %ecx, %ebx   # imm = 0x2FB010F
	negl	%ebp
	andl	$70004807, %ebp         # imm = 0x42C3047
	imull	$110002499, %r8d, %r13d # imm = 0x68E8143
	addl	%r13d, %esi
	addl	%ebx, %esi
	leal	400005958(%rbp,%rsi), %esi
	shrl	$4, %ecx
	movl	%r8d, %r9d
	shrl	$6, %r8d
	xorl	%r8d, %ecx
	xorl	%esi, %ecx
	shrl	$9, %esi
	xorl	%ecx, %esi
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %ebp
	movl	%ebp, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %esi, %esi  # imm = 0x3ADE68B3
	addl	%ecx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %esi  # imm = 0x75BCD17
	addl	%ecx, %esi
	movl	%esi, (%r15,%rcx,4)
	leal	(,%r12,4), %edi
	movl	pos(%rip), %edx
	movl	%edx, %ecx
	subl	%r12d, %ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ebp
	shrl	%ebp
	testl	%r12d, %r12d
	je	.LBB47_246
# BB#245:
	leal	(%r12,%r12), %ecx
	shll	$7, %ebp
	movl	%edx, %ebx
	subl	%ecx, %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	%ecx
	orl	%ebp, %ecx
	shll	$7, %ecx
	imull	$-3, %r12d, %ebp
	addl	%edx, %ebp
	andl	%esi, %ebp
	movslq	%ebp, %rbp
	movzbl	(%r11,%rbp), %ebx
	shrl	%ebx
	orl	%ecx, %ebx
	shll	$7, %ebx
	subl	%edi, %edx
	andl	%esi, %edx
	movslq	%edx, %rcx
	movzbl	(%r11,%rcx), %ebp
	shrl	%ebp
	orl	%ebx, %ebp
.LBB47_246:                             # %_Z1ciiiii.exit430
	imull	$30005491, %ebp, %ecx   # imm = 0x1C9D8F3
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	420001631(%rdx,%rcx), %ecx
	shrl	$3, %ebp
	xorl	%ebp, %eax
	xorl	%ecx, %eax
	shrl	$9, %ecx
	xorl	%eax, %ecx
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%r15,%rax,4)
	movl	pos(%rip), %esi
	movl	%esi, %eax
	subl	%r12d, %eax
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%ebp, %eax
	cltq
	movzbl	(%r11,%rax), %eax
	shrl	$3, %eax
	testl	%r12d, %r12d
	movl	%r10d, 48(%rsp)         # 4-byte Spill
	je	.LBB47_248
# BB#247:
	leal	(%r12,%r12), %ecx
	shll	$5, %eax
	movl	%esi, %edx
	subl	%ecx, %edx
	andl	%ebp, %edx
	movslq	%edx, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	$3, %ecx
	orl	%eax, %ecx
	shll	$5, %ecx
	imull	$-3, %r12d, %eax
	addl	%esi, %eax
	andl	%ebp, %eax
	cltq
	movzbl	(%r11,%rax), %edx
	shrl	$3, %edx
	orl	%ecx, %edx
	shll	$5, %edx
	movl	%esi, %eax
	subl	%edi, %eax
	andl	%ebp, %eax
	cltq
	movzbl	(%r11,%rax), %eax
	shrl	$3, %eax
	orl	%edx, %eax
.LBB47_248:                             # %_Z1ciiiii.exit426
	imull	$-5, %r12d, %r10d
	leal	(%rsi,%r10), %ecx
	andl	%ebp, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	$3, %ecx
	testl	%r12d, %r12d
	movl	%edi, 24(%rsp)          # 4-byte Spill
	je	.LBB47_250
# BB#249:
	shll	$5, %ecx
	imull	$-6, %r12d, %edx
	addl	%edx, %esi
	andl	%esi, %ebp
	movslq	%ebp, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$3, %edx
	orl	%edx, %ecx
.LBB47_250:                             # %_Z1ciiiii.exit422
	imull	$30005491, %eax, %edx   # imm = 0x1C9D8F3
	imull	$50004239, %ecx, %esi   # imm = 0x2FB010F
	imull	$70004807, %r9d, %edi   # imm = 0x42C3047
	addl	%edi, %edx
	leal	690009417(%rsi,%rdx), %edx
	shrl	$3, %eax
	shrl	$4, %ecx
	shrl	$5, %r9d
	xorl	%r9d, %eax
	xorl	%ecx, %eax
	xorl	%edx, %eax
	shrl	$9, %edx
	xorl	%eax, %edx
	xorl	$67108862, %edx         # imm = 0x3FFFFFE
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%r15,%rax,4)
	movl	pos(%rip), %esi
	movl	%esi, %eax
	subl	%r12d, %eax
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %eax
	cltq
	movzbl	(%r11,%rax), %eax
	shrl	$4, %eax
	testl	%r12d, %r12d
	movl	%r9d, 36(%rsp)          # 4-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	je	.LBB47_252
# BB#251:
	leal	(%r12,%r12), %ecx
	shll	$8, %eax
	movl	%esi, %edx
	subl	%ecx, %edx
	andl	%ebx, %edx
	movslq	%edx, %rcx
	movzbl	(%r11,%rcx), %ecx
	andl	$240, %ecx
	orl	%eax, %ecx
	imull	$-3, %r12d, %eax
	addl	%esi, %eax
	andl	%ebx, %eax
	cltq
	movzbl	(%r11,%rax), %edx
	shll	$4, %ecx
	andl	$240, %edx
	orl	%ecx, %edx
	movl	%esi, %eax
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	andl	%ebx, %eax
	cltq
	movzbl	(%r11,%rax), %eax
	shrl	$4, %eax
	orl	%edx, %eax
.LBB47_252:                             # %_Z1ciiiii.exit418
	leal	(,%r12,8), %edi
	leal	(%rsi,%r10), %ecx
	andl	%ebx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ebp
	shrl	$5, %ebp
	testl	%r12d, %r12d
	je	.LBB47_254
# BB#253:
	imull	$-6, %r12d, %ecx
	addl	%esi, %ecx
	andl	%ebx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	$5, %ecx
	leal	(%rcx,%rbp,8), %ecx
	imull	$-7, %r12d, %edx
	addl	%esi, %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$5, %edx
	leal	(%rdx,%rcx,8), %ecx
	movl	%esi, %edx
	subl	%edi, %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$5, %edx
	leal	(%rdx,%rcx,8), %ebp
.LBB47_254:                             # %_Z1ciiiii.exit414
	imull	$-9, %r12d, %r14d
	leal	(%rsi,%r14), %ecx
	andl	%ebx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	$6, %ecx
	testl	%r12d, %r12d
	je	.LBB47_256
# BB#255:
	imull	$-10, %r12d, %edx
	addl	%esi, %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
	imull	$-11, %r12d, %edx
	addl	%esi, %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
	imull	$-12, %r12d, %edx
	addl	%esi, %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
.LBB47_256:                             # %_Z1ciiiii.exit410
	imull	$30005491, %eax, %edx   # imm = 0x1C9D8F3
	imull	$50004239, %ebp, %esi   # imm = 0x2FB010F
	imull	$70004807, %ecx, %ebx   # imm = 0x42C3047
	addl	%r13d, %edx
	addl	%esi, %edx
	leal	1000014895(%rbx,%rdx), %edx
	shrl	$3, %eax
	shrl	$4, %ebp
	shrl	$5, %ecx
	xorl	$1, %r8d
	xorl	%r8d, %eax
	xorl	%ebp, %eax
	xorl	%ecx, %eax
	xorl	%edx, %eax
	shrl	$9, %edx
	xorl	%eax, %edx
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%r15,%rax,4)
	movl	pos(%rip), %eax
	movl	%eax, %ecx
	subl	%r12d, %ecx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %r9d
	shrl	$6, %r9d
	testl	%r12d, %r12d
	je	.LBB47_258
# BB#257:
	leal	(%r12,%r12), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	andl	%esi, %edx
	movslq	%edx, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	$6, %ecx
	leal	(%rcx,%r9,4), %ecx
	imull	$-3, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
	movl	%eax, %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %r9d
.LBB47_258:                             # %_Z1ciiiii.exit406
	addl	%eax, %r10d
	andl	%esi, %r10d
	movslq	%r10d, %rcx
	movzbl	(%r11,%rcx), %r10d
	shrl	$6, %r10d
	testl	%r12d, %r12d
	je	.LBB47_260
# BB#259:
	imull	$-6, %r12d, %ecx
	addl	%eax, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	$6, %ecx
	leal	(%rcx,%r10,4), %ecx
	imull	$-7, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
	movl	%eax, %edx
	subl	%edi, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %r10d
.LBB47_260:                             # %_Z1ciiiii.exit402
	addl	%eax, %r14d
	andl	%esi, %r14d
	movslq	%r14d, %rcx
	movzbl	(%r11,%rcx), %ebx
	shrl	$6, %ebx
	testl	%r12d, %r12d
	je	.LBB47_262
# BB#261:
	imull	$-10, %r12d, %ecx
	addl	%eax, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	$6, %ecx
	leal	(%rcx,%rbx,4), %ecx
	imull	$-11, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
	imull	$-12, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ebx
.LBB47_262:                             # %_Z1ciiiii.exit398
	imull	$-13, %r12d, %ecx
	addl	%eax, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ebp
	shrl	$6, %ebp
	testl	%r12d, %r12d
	je	.LBB47_264
# BB#263:
	movl	%r12d, %ecx
	shll	$4, %ecx
	imull	$-14, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rbp,4), %edx
	imull	$-15, %r12d, %ebp
	addl	%eax, %ebp
	andl	%esi, %ebp
	movslq	%ebp, %rbp
	movzbl	(%r11,%rbp), %ebp
	shrl	$6, %ebp
	leal	(%rbp,%rdx,4), %edx
	movl	%eax, %ebp
	subl	%ecx, %ebp
	andl	%esi, %ebp
	movslq	%ebp, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	$6, %ecx
	leal	(%rcx,%rdx,4), %ebp
.LBB47_264:                             # %_Z1ciiiii.exit394
	shll	$8, %r9d
	shll	$8, %ebx
	imull	$-17, %r12d, %ecx
	addl	%eax, %ecx
	andl	%esi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	shrl	$6, %ecx
	testl	%r12d, %r12d
	je	.LBB47_266
# BB#265:
	imull	$-18, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
	imull	$-19, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
	imull	$-20, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rcx,4), %ecx
.LBB47_266:                             # %_Z1ciiiii.exit390
	orl	%r9d, %r10d
	orl	%ebx, %ebp
	shll	$8, %ecx
	imull	$-21, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %ebx
	shrl	$6, %ebx
	testl	%r12d, %r12d
	je	.LBB47_268
# BB#267:
	imull	$-22, %r12d, %edx
	addl	%eax, %edx
	andl	%esi, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shrl	$6, %edx
	leal	(%rdx,%rbx,4), %edx
	imull	$-23, %r12d, %ebx
	addl	%eax, %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%r11,%rbx), %ebx
	shrl	$6, %ebx
	leal	(%rbx,%rdx,4), %edx
	imull	$-24, %r12d, %ebx
	addl	%eax, %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rax
	movzbl	(%r11,%rax), %eax
	shrl	$6, %eax
	leal	(%rax,%rdx,4), %ebx
.LBB47_268:                             # %_Z1ciiiii.exit
	orl	%ecx, %ebx
	imull	$30005491, %r10d, %eax  # imm = 0x1C9D8F3
	imull	$50004239, %ebp, %ecx   # imm = 0x2FB010F
	addl	%eax, %r13d
	imull	$70004807, %ebx, %eax   # imm = 0x42C3047
	addl	%ecx, %r13d
	leal	1200017874(%rax,%r13), %eax
	shrl	$3, %r10d
	shrl	$4, %ebp
	shrl	$5, %ebx
	xorl	%r10d, %r8d
	xorl	%ebp, %r8d
	xorl	%ebx, %r8d
	xorl	%eax, %r8d
	shrl	$9, %eax
	xorl	%r8d, %eax
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ecx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ecx, %eax
	movl	%eax, (%r15,%rcx,4)
	movslq	16(%rsp), %r8           # 4-byte Folded Reload
	movl	_ZZ8wavModelR5MixerE2pr.0(,%r8,4), %ecx
	movl	%ecx, %eax
	sarl	$8, %eax
	imull	$30005491, %eax, %r9d   # imm = 0x1C9D8F3
	movq	8(%rsp), %rsi           # 8-byte Reload
	leal	1220013547(%rsi,%r9), %edx
	movq	%rsi, %rdi
	shrl	$3, %eax
	movl	48(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %esi
	xorl	%eax, %esi
	xorl	%edx, %esi
	shrl	$9, %edx
	xorl	%esi, %edx
	xorl	$67108865, %edx         # imm = 0x4000001
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rsi
	leal	1(%rsi), %ebp
	movl	%ebp, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%esi, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movl	%edx, (%r15,%rsi,4)
	movl	pos(%rip), %edx
	movl	%r12d, %esi
	notl	%esi
	addl	%edx, %esi
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%ebp, %esi
	movslq	%esi, %rsi
	movzbl	(%r11,%rsi), %esi
	subl	%r12d, %edx
	andl	%ebp, %edx
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %edx
	shll	$8, %edx
	orl	%esi, %edx
	movswl	%dx, %edx
	movl	_ZZ8wavModelR5MixerE2pr.1(,%r8,4), %r11d
	subl	%r11d, %ecx
	addl	%edx, %ecx
	sarl	$8, %ecx
	imull	$30005491, %ecx, %edx   # imm = 0x1C9D8F3
	leal	1420016526(%rdi,%rdx), %edx
	shrl	$3, %ecx
	xorl	$67108866, %r13d        # imm = 0x4000002
	xorl	%r13d, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movq	_ZZ8wavModelR5MixerE2cm+96(%rip), %r10
	movl	%edx, (%r10,%rcx,4)
	movl	_ZZ8wavModelR5MixerE1w(%rip), %r14d
	movl	pos(%rip), %ecx
	movl	%r14d, %esi
	notl	%esi
	leal	(%rcx,%rsi), %edi
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%ebp, %edi
	movq	buf+16(%rip), %rdx
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	movl	%ecx, %ebx
	subl	%r14d, %ebx
	andl	%ebp, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rdx,%rbx), %ebx
	shll	$8, %ebx
	orl	%edi, %ebx
	leal	1(%r14,%r14), %r15d
	movl	%ecx, %edi
	subl	%r15d, %edi
	andl	%ebp, %edi
	incl	%ecx
	subl	%r15d, %ecx
	andl	%ebp, %ecx
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx), %ebp
	shll	$8, %ebp
	orl	%edi, %ebp
	movswl	%bx, %edi
	movl	_ZZ8wavModelR5MixerE2pr.2(,%r8,4), %r12d
	addl	%r12d, %r11d
	subl	%r11d, %edi
	movswl	%bp, %ebp
	addl	%ebp, %edi
	sarl	$9, %edi
	movq	56(%rsp), %rcx          # 8-byte Reload
	addl	%r9d, %ecx
	imull	$50004239, %edi, %ebp   # imm = 0x2FB010F
	leal	1690024312(%rbp,%rcx), %ebp
	shrl	$4, %edi
	movl	36(%rsp), %ecx          # 4-byte Reload
	xorl	%eax, %ecx
	xorl	%edi, %ecx
	xorl	%ebp, %ecx
	shrl	$9, %ebp
	xorl	%ecx, %ebp
	xorl	$67108861, %ebp         # imm = 0x3FFFFFD
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %edi
	movl	%edi, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ebp, %edi  # imm = 0x3ADE68B3
	addl	%eax, %edi
	roll	$16, %edi
	imull	$123456791, %edi, %edi  # imm = 0x75BCD17
	addl	%eax, %edi
	movl	%edi, (%r10,%rax,4)
	movl	_ZZ8wavModelR5MixerE2pr.0(,%r8,4), %eax
	movl	pos(%rip), %ecx
	addl	%ecx, %esi
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%ebp, %esi
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %esi
	movl	%ecx, %edi
	subl	%r14d, %edi
	andl	%ebp, %edi
	movslq	%edi, %rdi
	movzbl	(%rdx,%rdi), %edi
	shll	$8, %edi
	orl	%esi, %edi
	movswl	%di, %esi
	movl	_ZZ8wavModelR5MixerE2pr.1(,%r8,4), %edi
	addl	%edi, %edi
	movl	%ecx, %ebx
	subl	%r15d, %ebx
	andl	%ebp, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rdx,%rbx), %ebx
	incl	%ecx
	subl	%r15d, %ecx
	andl	%ebp, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%ebx, %ecx
	movswl	%cx, %ecx
	subl	%edi, %eax
	addl	%r12d, %eax
	leal	(%rax,%rsi,2), %eax
	subl	%ecx, %eax
	sarl	$8, %eax
	imull	$30005491, %eax, %ecx   # imm = 0x1C9D8F3
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	1820022484(%rdx,%rcx), %ecx
	shrl	$3, %eax
	xorl	%r13d, %eax
	xorl	%ecx, %eax
	shrl	$9, %ecx
	xorl	%eax, %ecx
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movq	_ZZ8wavModelR5MixerE2cm+96(%rip), %rdx
	movl	%ecx, (%rdx,%rax,4)
	movl	pos(%rip), %edi
	leal	-5(%rdi), %eax
	movl	buf(%rip), %ebx
	decl	%ebx
	andl	%ebx, %eax
	movq	buf+16(%rip), %rcx
	cltq
	movzbl	(%rcx,%rax), %eax
	leal	-4(%rdi), %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	shll	$8, %edx
	orl	%eax, %edx
	movswl	%dx, %eax
	leal	-3(%rdi), %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	leal	-2(%rdi), %esi
	andl	%ebx, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	shll	$8, %esi
	orl	%edx, %esi
	movswl	%si, %edx
	leal	-7(%rdi), %esi
	andl	%ebx, %esi
	movslq	%esi, %rsi
	movzbl	(%rcx,%rsi), %esi
	leal	-6(%rdi), %ebp
	andl	%ebx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rcx,%rbp), %ebp
	shll	$8, %ebp
	orl	%esi, %ebp
	movswl	%bp, %esi
	leal	-1(%rdi), %ebp
	andl	%ebx, %ebp
	movslq	%ebp, %r9
	movzbl	(%rcx,%r9), %ebp
	movl	_ZZ8wavModelR5MixerE2pr.0(,%r8,4), %r10d
	subl	%ebp, %eax
	addl	%r10d, %eax
	addl	%edx, %eax
	subl	%esi, %eax
	sarl	$9, %eax
	shll	$8, %eax
	movl	$-256, %r11d
	movl	_ZZ8wavModelR5MixerE4scm1(%rip), %edx
	addl	%r11d, %edx
	andl	%eax, %edx
	movl	%edx, _ZZ8wavModelR5MixerE4scm1+24(%rip)
	movl	_ZZ8wavModelR5MixerE1w(%rip), %r14d
	movl	%edi, %edx
	subl	%r14d, %edx
	movl	%r14d, %ebp
	notl	%ebp
	leal	(%rdi,%rbp), %eax
	andl	%ebx, %eax
	movslq	%eax, %r15
	movzbl	(%rcx,%r15), %esi
	andl	%ebx, %edx
	movslq	%edx, %r12
	movzbl	(%rcx,%r12), %eax
	shll	$8, %eax
	orl	%esi, %eax
	movzbl	(%rcx,%r9), %esi
	movl	%r10d, %edx
	subl	%esi, %edx
	cwtl
	addl	%eax, %edx
	sarl	$9, %edx
	shll	$8, %edx
	movl	_ZZ8wavModelR5MixerE4scm2(%rip), %eax
	addl	%r11d, %eax
	andl	%edx, %eax
	movl	%eax, _ZZ8wavModelR5MixerE4scm2+24(%rip)
	leal	1(%r14,%r14), %r13d
	movzbl	(%rcx,%r15), %edx
	movzbl	(%rcx,%r12), %eax
	shll	$8, %eax
	orl	%edx, %eax
	movl	%edi, %edx
	subl	%r13d, %edx
	andl	%ebx, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	incl	%edi
	subl	%r13d, %edi
	andl	%ebx, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %ebx
	shll	$8, %ebx
	orl	%edx, %ebx
	movl	pos(%rip), %edi
	movl	%edi, %edx
	subl	%r14d, %edx
	leal	(%r14,%r14,2), %esi
	cwtl
	movswl	%bx, %ebx
	movzbl	(%rcx,%r9), %ecx
	subl	%ecx, %r10d
	leal	(%r10,%rax,2), %eax
	subl	%ebx, %eax
	movl	_ZZ8wavModelR5MixerE4scm3(%rip), %ecx
	addl	%r11d, %ecx
	andl	%eax, %ecx
	andl	$-256, %ecx
	movl	%ecx, _ZZ8wavModelR5MixerE4scm3+24(%rip)
	addl	%edi, %ebp
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %ebp
	movq	buf+16(%rip), %rax
	movslq	%ebp, %rbp
	movzbl	(%rax,%rbp), %ebp
	andl	%ecx, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	shll	$8, %edx
	orl	%ebp, %edx
	movswl	%dx, %edx
	movl	%edi, %ebp
	subl	%r13d, %ebp
	andl	%ecx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rax,%rbp), %ebp
	leal	1(%rdi), %ebx
	subl	%r13d, %ebx
	andl	%ecx, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	shll	$8, %ebx
	orl	%ebp, %ebx
	movswl	%bx, %ebp
	subl	%ebp, %edx
	leal	(%rdx,%rdx,2), %ebp
	movl	%edi, %edx
	subl	%esi, %edx
	notl	%esi
	addl	%edi, %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	andl	%ecx, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	shll	$8, %edx
	orl	%esi, %edx
	movswl	%dx, %esi
	leal	-1(%rdi), %edx
	andl	%ecx, %edx
	movslq	%edx, %r9
	movzbl	(%rax,%r9), %ebx
	subl	%ebx, %esi
	addl	%ebp, %esi
	addl	%esi, %esi
	movl	_ZZ8wavModelR5MixerE4scm4(%rip), %ebp
	addl	%r11d, %ebp
	andl	%esi, %ebp
	andl	$-256, %ebp
	movl	%ebp, _ZZ8wavModelR5MixerE4scm4+24(%rip)
	movl	_ZZ8wavModelR5MixerE1w(%rip), %ebp
	movl	$1, %esi
	subl	%ebp, %esi
	addl	%edi, %esi
	andl	%ecx, %esi
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %esi
	movl	$2, %ebx
	subl	%ebp, %ebx
	addl	%edi, %ebx
	andl	%ecx, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rax,%rbx), %ebx
	shll	$8, %ebx
	orl	%esi, %ebx
	movswl	%bx, %esi
	movl	%ebp, %ebx
	notl	%ebx
	leal	(%rdi,%rbx), %edx
	andl	%ecx, %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	subl	%ebp, %edi
	andl	%ecx, %edi
	movslq	%edi, %rcx
	movzbl	(%rax,%rcx), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movswl	%cx, %ecx
	movzbl	(%rax,%r9), %eax
	movl	_ZZ8wavModelR5MixerE2pr.0(,%r8,4), %edx
	subl	%eax, %esi
	leal	(%rsi,%rdx,2), %eax
	addl	%ecx, %eax
	sarl	$10, %eax
	shll	$8, %eax
	movl	_ZZ8wavModelR5MixerE4scm5(%rip), %ecx
	addl	%r11d, %ecx
	andl	%eax, %ecx
	movl	%ecx, _ZZ8wavModelR5MixerE4scm5+24(%rip)
	movl	pos(%rip), %edx
	addl	%edx, %ebx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%esi, %ebx
	movq	buf+16(%rip), %rcx
	movslq	%ebx, %rax
	movzbl	(%rcx,%rax), %eax
	movl	%edx, %edi
	subl	%ebp, %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	shll	$8, %edi
	orl	%eax, %edi
	movswl	%di, %r9d
	leal	1(%rbp,%rbp), %ebx
	movl	%edx, %edi
	subl	%ebx, %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %eax
	leal	1(%rdx), %r10d
	movl	%r10d, %edi
	subl	%ebx, %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	shll	$8, %edi
	orl	%eax, %edi
	movswl	%di, %eax
	imull	$-6, %eax, %eax
	leal	(%rax,%r9,4), %eax
	leal	(%rbp,%rbp,2), %edi
	movl	%edx, %ebx
	subl	%edi, %ebx
	notl	%edi
	addl	%edx, %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	andl	%esi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rcx,%rbx), %ebx
	shll	$8, %ebx
	orl	%edi, %ebx
	movswl	%bx, %r14d
	leal	1(,%rbp,4), %ebp
	movl	%edx, %ebx
	subl	%ebp, %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rcx,%rbx), %ebx
	movl	%r10d, %edi
	subl	%ebp, %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	shll	$8, %edi
	orl	%ebx, %edi
	movswl	%di, %edi
	leal	-1(%rdx), %ebp
	andl	%esi, %ebp
	movslq	%ebp, %r9
	movzbl	(%rcx,%r9), %ebx
	subl	%ebx, %eax
	leal	(%rax,%r14,4), %eax
	subl	%edi, %eax
	addl	%eax, %eax
	addl	_ZZ8wavModelR5MixerE4scm7(%rip), %r11d
	andl	%eax, %r11d
	andl	$-256, %r11d
	movl	%r11d, _ZZ8wavModelR5MixerE4scm7+24(%rip)
	movl	_ZZ8wavModelR5MixerE1w(%rip), %r14d
	movl	%edx, %ebp
	subl	%r14d, %ebp
	leal	1(%r14,%r14), %r11d
	leal	(%r14,%r14,2), %edi
	movl	%r14d, %ebx
	notl	%ebx
	addl	%edx, %ebx
	andl	%esi, %ebx
	movslq	%ebx, %rbx
	movzbl	(%rcx,%rbx), %eax
	andl	%esi, %ebp
	movslq	%ebp, %rbx
	movzbl	(%rcx,%rbx), %r15d
	shll	$8, %r15d
	orl	%eax, %r15d
	movl	%edx, %eax
	subl	%r11d, %eax
	movl	%r10d, %ebp
	subl	%r11d, %ebp
	leal	1(,%r14,4), %ebx
	andl	%esi, %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	andl	%esi, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rcx,%rbp), %ebp
	shll	$8, %ebp
	orl	%eax, %ebp
	movl	%edx, %eax
	subl	%edi, %eax
	notl	%edi
	addl	%edx, %edi
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	andl	%esi, %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	shll	$8, %eax
	orl	%edi, %eax
	movl	%edx, %edi
	subl	%ebx, %edi
	subl	%ebx, %r10d
	leal	(%r14,%r14,4), %r14d
	andl	%esi, %edi
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %r11d
	andl	%esi, %r10d
	movslq	%r10d, %rdi
	movzbl	(%rcx,%rdi), %edi
	shll	$8, %edi
	orl	%r11d, %edi
	movl	%r14d, %ebx
	notl	%ebx
	addl	%edx, %ebx
	subl	%r14d, %edx
	andl	%esi, %ebx
	andl	%esi, %edx
	movslq	%ebx, %rsi
	movzbl	(%rcx,%rsi), %esi
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	shll	$8, %edx
	orl	%esi, %edx
	movzbl	(%rcx,%r9), %ecx
	movswl	%bp, %esi
	cwtl
	subl	%esi, %eax
	movswl	%r15w, %esi
	movswl	%di, %edi
	subl	%edi, %esi
	movl	_ZZ8wavModelR5MixerE2pr.0(,%r8,4), %edi
	subl	%ecx, %edi
	leal	(%rax,%rax,4), %eax
	leal	(%rdi,%rax,2), %eax
	movswl	%dx, %ecx
	leal	(%rsi,%rsi,4), %edx
	addl	%ecx, %eax
	addl	%edx, %eax
	sarl	$9, %eax
	movl	$_ZZ8wavModelR5MixerE4scm8, %ecx
	movl	$_ZZ8wavModelR5MixerE4scm8+24, %edx
	jmp	.LBB47_269
.LBB47_145:                             # %.preheader552
	movl	%r8d, 36(%rsp)          # 4-byte Spill
	movl	_ZL1S(%rip), %eax
	movl	_ZL1D(%rip), %ecx
	movl	%ecx, %edx
	addl	%eax, %edx
	js	.LBB47_146
# BB#147:                               # %.lr.ph608
	movl	16(%rsp), %r12d         # 4-byte Reload
	movslq	%r12d, %r14
	leaq	19296(%rsp,%r14,8), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB47_148:                             # =>This Inner Loop Header: Depth=1
	movslq	_ZZ8wavModelR5MixerE7counter(,%r14,4), %rdx
	cmpq	%rdx, %rbx
	jl	.LBB47_151
# BB#149:                               #   in Loop: Header=BB47_148 Depth=1
	movl	%ebx, %esi
	subl	%eax, %esi
	testl	%esi, %esi
	jle	.LBB47_152
# BB#150:                               #   in Loop: Header=BB47_148 Depth=1
	cmpl	%edx, %esi
	jg	.LBB47_152
.LBB47_151:                             # %._crit_edge717
                                        #   in Loop: Header=BB47_148 Depth=1
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	mulsd	.LCPI47_2(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	movl	$1, %esi
	callq	_Z1Xii
	movl	%eax, %r15d
	movl	$1, %esi
	movl	%ebx, %edi
	callq	_Z1Xii
	imull	%r15d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, (%rbp)
	movl	_ZL1S(%rip), %eax
	movl	_ZL1D(%rip), %ecx
.LBB47_152:                             #   in Loop: Header=BB47_148 Depth=1
	leal	(%rcx,%rax), %edx
	movslq	%edx, %rdx
	addq	$16, %rbp
	cmpq	%rdx, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB47_148
	jmp	.LBB47_153
.LBB47_146:
	movl	16(%rsp), %r12d         # 4-byte Reload
.LBB47_153:                             # %._crit_edge609
	cmpl	$2, _ZZ8wavModelR5MixerE8channels(%rip)
	jne	.LBB47_165
# BB#154:                               # %.preheader551
	testl	%ecx, %ecx
	jle	.LBB47_159
# BB#155:                               # %.lr.ph606
	movslq	%r12d, %rdi
	movslq	%eax, %r12
	leal	1(%r12), %r15d
	movq	%r12, %rdx
	shlq	$4, %rdx
	leaq	19296(%rsp,%rdx), %rdx
	leaq	16(%rdx,%rdi,8), %rbp
	xorl	%r14d, %r14d
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB47_156:                             # =>This Inner Loop Header: Depth=1
	leaq	(%r15,%r14), %rbx
	movl	%ebx, %edx
	subl	%eax, %edx
	cmpl	_ZZ8wavModelR5MixerE7counter(,%rdi,4), %edx
	jg	.LBB47_158
# BB#157:                               #   in Loop: Header=BB47_156 Depth=1
	incl	%eax
	movslq	%eax, %rdi
	imulq	$784, %rdi, %rax        # imm = 0x310
	movsd	(%rbp,%rax), %xmm0      # xmm0 = mem[0],zero
	mulsd	.LCPI47_2(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$1, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	_Z1Xii
	movl	%eax, %r13d
	movl	$1, %esi
	movl	%ebx, %edi
	callq	_Z1Xii
	movq	24(%rsp), %rdi          # 8-byte Reload
	imull	%r13d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movslq	_ZL1S(%rip), %rax
	imulq	$784, %rax, %rcx        # imm = 0x310
	movsd	%xmm0, 784(%rbp,%rcx)
	movl	_ZL1D(%rip), %ecx
.LBB47_158:                             # %.backedge
                                        #   in Loop: Header=BB47_156 Depth=1
	leal	(%rcx,%rax), %edx
	movslq	%edx, %rdx
	addq	$16, %rbp
	leaq	1(%r12,%r14), %rsi
	incq	%r14
	cmpq	%rdx, %rsi
	jl	.LBB47_156
.LBB47_159:                             # %.preheader549
	testl	%eax, %eax
	jle	.LBB47_160
# BB#161:                               # %.lr.ph603
	movl	16(%rsp), %r12d         # 4-byte Reload
	movslq	%r12d, %r14
	leaq	20080(%rsp,%r14,8), %rbp
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB47_162:                             # =>This Inner Loop Header: Depth=1
	movslq	_ZZ8wavModelR5MixerE7counter(,%r14,4), %rcx
	cmpq	%rcx, %rbx
	jge	.LBB47_164
# BB#163:                               #   in Loop: Header=BB47_162 Depth=1
	cltq
	shlq	$4, %rax
	movsd	16(%rbp,%rax), %xmm0    # xmm0 = mem[0],zero
	mulsd	.LCPI47_2(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$1, %esi
	movl	%ebx, %edi
	callq	_Z1Xii
	movl	%eax, %r15d
	movl	_ZL1S(%rip), %edi
	incl	%edi
	movl	$1, %esi
	callq	_Z1Xii
	imull	%r15d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movslq	_ZL1S(%rip), %rax
	movq	%rax, %rcx
	shlq	$4, %rcx
	movsd	%xmm0, 16(%rbp,%rcx)
.LBB47_164:                             #   in Loop: Header=BB47_162 Depth=1
	movslq	%eax, %rcx
	addq	$784, %rbp              # imm = 0x310
	cmpq	%rcx, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB47_162
	jmp	.LBB47_165
.LBB47_160:
	movl	16(%rsp), %r12d         # 4-byte Reload
.LBB47_165:                             # %.loopexit550
	movslq	%r12d, %rdx
	movl	_ZZ8wavModelR5MixerE1n(,%rdx,4), %ecx
	incl	%ecx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%ecx, _ZZ8wavModelR5MixerE1n(,%rdx,4)
	cmpl	_ZZ8wavModelR5MixerE1K(%rip), %ecx
	jne	.LBB47_166
# BB#167:
	movl	_ZL1D(%rip), %r11d
	leal	(%r11,%rax), %ecx
	cmpl	$1, _ZZ8wavModelR5MixerE8channels(%rip)
	jne	.LBB47_168
# BB#177:                               # %.preheader544
	testl	%ecx, %ecx
	jle	.LBB47_183
# BB#178:                               # %.preheader543.preheader
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	19296(%rsp,%rcx,8), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB47_179:                             # %.preheader543
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB47_181 Depth 2
	leal	(%r11,%rax), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %r12
	jg	.LBB47_182
# BB#180:                               # %.lr.ph595
                                        #   in Loop: Header=BB47_179 Depth=1
	leal	-1(%r12), %r14d
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB47_181:                             #   Parent Loop BB47_179 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$1, %esi
	movl	%r14d, %edi
	callq	_Z1Xii
	movl	%eax, %ebx
	movl	$1, %esi
	movl	%ebp, %edi
	callq	_Z1Xii
	imull	%ebx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	.LCPI47_2(%rip), %xmm0  # xmm0 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 800(%r15)
	movl	_ZL1S(%rip), %eax
	movl	_ZL1D(%rip), %r11d
	leal	(%r11,%rax), %ecx
	movslq	%ecx, %rcx
	incq	%rbp
	addq	$16, %r15
	cmpq	%rcx, %rbp
	jl	.LBB47_181
.LBB47_182:                             # %._crit_edge596
                                        #   in Loop: Header=BB47_179 Depth=1
	leal	(%r11,%rax), %ecx
	movslq	%ecx, %rcx
	incq	%r13
	addq	$800, 24(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x320
	cmpq	%rcx, %r12
	leaq	1(%r12), %r12
	jl	.LBB47_179
	jmp	.LBB47_183
.LBB47_166:                             # %.loopexit550..preheader_crit_edge
	movl	_ZL1D(%rip), %r11d
	addl	%eax, %r11d
	jg	.LBB47_241
	jmp	.LBB47_240
.LBB47_168:                             # %.preheader547
	testl	%ecx, %ecx
	jle	.LBB47_183
# BB#169:                               # %.lr.ph601.preheader
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	19296(%rsp,%rcx,8), %r15
	movl	$1, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB47_170:                             # %.lr.ph601
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB47_173 Depth 2
	leal	1(%rax), %ecx
	cmpq	%rcx, %r12
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	je	.LBB47_176
# BB#171:                               # %.preheader545
                                        #   in Loop: Header=BB47_170 Depth=1
	leal	(%r11,%rax), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %r12
	jg	.LBB47_176
# BB#172:                               # %.lr.ph599
                                        #   in Loop: Header=BB47_170 Depth=1
	leal	-1(%r12), %r14d
	.p2align	4, 0x90
.LBB47_173:                             #   Parent Loop BB47_170 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rbp), %r13
	leal	1(%rax), %ecx
	cmpq	%rcx, %r13
	je	.LBB47_175
# BB#174:                               #   in Loop: Header=BB47_173 Depth=2
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	$1, %esi
	movl	%r14d, %edi
	callq	_Z1Xii
	movl	%eax, %ebx
	movl	$1, %esi
	movl	%ebp, %edi
	callq	_Z1Xii
	imull	%ebx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	divsd	.LCPI47_2(%rip), %xmm1
	movsd	%xmm1, 800(%r15)
	movl	_ZL1S(%rip), %eax
	movl	_ZL1D(%rip), %r11d
.LBB47_175:                             #   in Loop: Header=BB47_173 Depth=2
	leal	(%r11,%rax), %ecx
	movslq	%ecx, %rcx
	addq	$16, %r15
	cmpq	%rcx, %r13
	movq	%r13, %rbp
	jl	.LBB47_173
.LBB47_176:                             # %.loopexit546
                                        #   in Loop: Header=BB47_170 Depth=1
	leal	(%r11,%rax), %ecx
	movslq	%ecx, %rcx
	movq	48(%rsp), %rbp          # 8-byte Reload
	incq	%rbp
	movq	24(%rsp), %r15          # 8-byte Reload
	addq	$800, %r15              # imm = 0x320
	cmpq	%rcx, %r12
	leaq	1(%r12), %r12
	jl	.LBB47_170
.LBB47_183:                             # %.preheader542
	movl	%r11d, %ecx
	addl	%eax, %ecx
	jle	.LBB47_184
# BB#192:                               # %.lr.ph589.preheader
	leaq	480(%rsp), %rbx
	leaq	472(%rsp), %r15
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	20112(%rsp,%rcx,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	movl	$2, %ebp
	movl	$1, %r14d
	xorl	%r9d, %r9d
	xorps	%xmm3, %xmm3
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB47_194:                             # %.lr.ph589
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB47_198 Depth 2
                                        #     Child Loop BB47_202 Depth 2
                                        #     Child Loop BB47_217 Depth 2
                                        #     Child Loop BB47_210 Depth 2
                                        #       Child Loop BB47_221 Depth 3
	movq	%r14, %rcx
	shlq	$4, %rcx
	imulq	$784, %r14, %rdx        # imm = 0x310
	leaq	19296(%rsp,%rdx), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	addq	%rdx, %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	$2, %r14
	jl	.LBB47_203
# BB#195:                               # %.lr.ph576.preheader
                                        #   in Loop: Header=BB47_194 Depth=1
	leaq	-1(%r9), %rdx
	testb	$3, %r9b
	je	.LBB47_196
# BB#197:                               # %.lr.ph576.prol.preheader
                                        #   in Loop: Header=BB47_194 Depth=1
	movl	%r9d, %esi
	andl	$3, %esi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB47_198:                             # %.lr.ph576.prol
                                        #   Parent Loop BB47_194 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	incq	%rcx
	addq	$8, %rdi
	cmpq	%rcx, %rsi
	jne	.LBB47_198
# BB#199:                               # %.lr.ph576.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB47_194 Depth=1
	incq	%rcx
	cmpq	$3, %rdx
	jae	.LBB47_201
	jmp	.LBB47_203
.LBB47_196:                             #   in Loop: Header=BB47_194 Depth=1
	movl	$1, %ecx
	cmpq	$3, %rdx
	jb	.LBB47_203
.LBB47_201:                             # %.lr.ph576.preheader.new
                                        #   in Loop: Header=BB47_194 Depth=1
	movq	%r14, %rdx
	subq	%rcx, %rdx
	leaq	(%r15,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB47_202:                             # %.lr.ph576
                                        #   Parent Loop BB47_194 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	movsd	8(%rcx), %xmm2          # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm2
	subsd	%xmm2, %xmm0
	movsd	16(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	movsd	24(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB47_202
.LBB47_203:                             # %._crit_edge577
                                        #   in Loop: Header=BB47_194 Depth=1
	ucomisd	%xmm3, %xmm0
	jbe	.LBB47_204
# BB#205:                               #   in Loop: Header=BB47_194 Depth=1
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	movq	%r9, %r13
	jnp	.LBB47_207
# BB#206:                               # %call.sqrt
                                        #   in Loop: Header=BB47_194 Depth=1
	callq	sqrt
	xorps	%xmm3, %xmm3
	movq	%r13, %r9
	movapd	%xmm0, %xmm1
.LBB47_207:                             # %.split
                                        #   in Loop: Header=BB47_194 Depth=1
	imulq	$392, %r14, %rdi        # imm = 0x188
	leaq	80(%rsp,%rdi), %rdx
	movsd	%xmm1, (%rdx,%r14,8)
	incl	%r12d
	movl	_ZL1S(%rip), %eax
	movl	_ZL1D(%rip), %r11d
	leal	(%r11,%rax), %ecx
	movslq	%ecx, %r8
	cmpq	%r8, %r14
	jge	.LBB47_185
# BB#208:                               # %.lr.ph587
                                        #   in Loop: Header=BB47_194 Depth=1
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	%r12d, 24(%rsp)         # 4-byte Spill
	movl	%ebp, 56(%rsp)          # 4-byte Spill
	movslq	%ebp, %rsi
	imulq	$392, %rsi, %rbp        # imm = 0x188
	leaq	(%rdx,%r14,8), %r10
	cmpq	$2, %r14
	jl	.LBB47_212
# BB#209:                               # %.lr.ph587.split.us.preheader
                                        #   in Loop: Header=BB47_194 Depth=1
	leaq	88(%rsp), %rcx
	addq	%rcx, %rbp
	movl	%r9d, %r15d
	andl	$1, %r15d
	movq	%r9, %rcx
	leaq	88(%rsp,%rdi), %r9
	leaq	1(%rcx), %rdi
	.p2align	4, 0x90
.LBB47_210:                             # %.lr.ph587.split.us
                                        #   Parent Loop BB47_194 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB47_221 Depth 3
	movq	%rsi, %rcx
	shlq	$4, %rcx
	addq	8(%rsp), %rcx           # 8-byte Folded Reload
	testq	%r15, %r15
	movq	16(%rsp), %rdx          # 8-byte Reload
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	jne	.LBB47_218
# BB#211:                               #   in Loop: Header=BB47_210 Depth=2
	movl	$1, %ecx
	cmpq	$1, %r13
	jne	.LBB47_220
	jmp	.LBB47_222
	.p2align	4, 0x90
.LBB47_218:                             #   in Loop: Header=BB47_210 Depth=2
	imulq	$392, %rsi, %rcx        # imm = 0x188
	movsd	88(%rsp,%rcx), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%r9), %xmm1
	subsd	%xmm1, %xmm0
	movl	$2, %ecx
	cmpq	$1, %r13
	je	.LBB47_222
.LBB47_220:                             # %.lr.ph587.split.us.new
                                        #   in Loop: Header=BB47_210 Depth=2
	leaq	(%rbp,%rcx,8), %r12
	.p2align	4, 0x90
.LBB47_221:                             #   Parent Loop BB47_194 Depth=1
                                        #     Parent Loop BB47_210 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%r12), %xmm1         # xmm1 = mem[0],zero
	movsd	(%r12), %xmm2           # xmm2 = mem[0],zero
	mulsd	-8(%rbx,%rcx,8), %xmm1
	subsd	%xmm1, %xmm0
	mulsd	(%rbx,%rcx,8), %xmm2
	subsd	%xmm2, %xmm0
	addq	$2, %rcx
	addq	$16, %r12
	cmpq	%rcx, %rdi
	jne	.LBB47_221
.LBB47_222:                             # %._crit_edge583.us
                                        #   in Loop: Header=BB47_210 Depth=2
	divsd	(%r10), %xmm0
	imulq	$392, %rsi, %rcx        # imm = 0x188
	leaq	80(%rsp,%rcx), %rcx
	movsd	%xmm0, (%rcx,%r14,8)
	addq	$392, %rbp              # imm = 0x188
	cmpq	%r8, %rsi
	leaq	1(%rsi), %rsi
	jl	.LBB47_210
	jmp	.LBB47_193
.LBB47_212:                             # %.lr.ph587.split.preheader
                                        #   in Loop: Header=BB47_194 Depth=1
	movq	%rsi, %rdx
	shlq	$4, %rdx
	addq	8(%rsp), %rdx           # 8-byte Folded Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdx,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	leaq	80(%rsp,%rbp), %rdx
	movsd	%xmm0, (%rdx,%r14,8)
	cmpl	%ecx, %esi
	jge	.LBB47_193
# BB#213:                               # %.lr.ph587.split..lr.ph587.split_crit_edge.preheader
                                        #   in Loop: Header=BB47_194 Depth=1
	movq	%r13, %rdx
	shlq	$32, %rdx
	movabsq	$8589934592, %rdi       # imm = 0x200000000
	addq	%rdi, %rdx
	sarq	$32, %rdx
	subl	%edx, %ecx
	leaq	-1(%r8), %rdi
	testb	$1, %cl
	je	.LBB47_215
# BB#214:                               # %.lr.ph587.split..lr.ph587.split_crit_edge.prol
                                        #   in Loop: Header=BB47_194 Depth=1
	incq	%rsi
	movq	%rsi, %rcx
	shlq	$4, %rcx
	movq	8(%rsp), %rbp           # 8-byte Reload
	addq	%rcx, %rbp
	movq	16(%rsp), %rcx          # 8-byte Reload
	movsd	(%rbp,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	divsd	(%r10), %xmm0
	imulq	$392, %rsi, %rcx        # imm = 0x188
	leaq	80(%rsp,%rcx), %rcx
	movsd	%xmm0, (%rcx,%r14,8)
.LBB47_215:                             # %.lr.ph587.split..lr.ph587.split_crit_edge.prol.loopexit
                                        #   in Loop: Header=BB47_194 Depth=1
	cmpq	%rdx, %rdi
	je	.LBB47_193
# BB#216:                               # %.lr.ph587.split..lr.ph587.split_crit_edge.preheader.new
                                        #   in Loop: Header=BB47_194 Depth=1
	imulq	$392, %rsi, %rcx        # imm = 0x188
	addq	72(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rsi, %rdx
	shlq	$4, %rdx
	addq	64(%rsp), %rdx          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB47_217:                             # %.lr.ph587.split..lr.ph587.split_crit_edge
                                        #   Parent Loop BB47_194 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-16(%rdx), %xmm0        # xmm0 = mem[0],zero
	divsd	(%r10), %xmm0
	movsd	%xmm0, (%rcx)
	addq	$2, %rsi
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	divsd	(%r10), %xmm0
	movsd	%xmm0, 392(%rcx)
	addq	$784, %rcx              # imm = 0x310
	addq	$32, %rdx
	cmpq	%r8, %rsi
	jl	.LBB47_217
	.p2align	4, 0x90
.LBB47_193:                             # %.loopexit541
                                        #   in Loop: Header=BB47_194 Depth=1
	movl	56(%rsp), %ebp          # 4-byte Reload
	incl	%ebp
	movq	%r13, %r9
	incq	%r9
	addq	$392, %rbx              # imm = 0x188
	movq	48(%rsp), %r15          # 8-byte Reload
	addq	$392, %r15              # imm = 0x188
	addq	$8, 72(%rsp)            # 8-byte Folded Spill
	addq	$784, 64(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x310
	cmpq	%r8, %r14
	leaq	1(%r14), %r14
	movl	24(%rsp), %r12d         # 4-byte Reload
	jl	.LBB47_194
	jmp	.LBB47_185
.LBB47_184:
	movl	$1, %r12d
	jmp	.LBB47_185
.LBB47_204:
	movl	%r14d, %r12d
.LBB47_185:                             # %._crit_edge590
	leal	(%r11,%rax), %r9d
	cmpl	%r9d, %r12d
	jle	.LBB47_238
# BB#186:
	leal	1(%rax), %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	cmpl	%ecx, _ZZ8wavModelR5MixerE7counter(,%rdx,4)
	jle	.LBB47_238
# BB#187:
	testl	%r9d, %r9d
	jle	.LBB47_238
# BB#188:                               # %.lr.ph571
	movslq	%r9d, %r10
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	19296(%rsp,%rcx,8), %r8
	leaq	80(%rsp), %r14
	movl	$1, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB47_189:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB47_231 Depth 2
	movq	%rbx, %rcx
	shlq	$4, %rcx
	leaq	19296(%rsp,%rcx), %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	imulq	$784, %rbx, %rdx        # imm = 0x310
	leaq	19296(%rsp,%rdx), %rdx
	leaq	(%rdx,%rdi,8), %rsi
	movq	%rcx, (%rdx,%rdi,8)
	movd	%rcx, %xmm0
	cmpq	$2, %rbx
	jl	.LBB47_232
# BB#190:                               # %.lr.ph568.preheader
                                        #   in Loop: Header=BB47_189 Depth=1
	testb	$1, %r15b
	jne	.LBB47_228
# BB#191:                               #   in Loop: Header=BB47_189 Depth=1
	movl	$1, %edi
	cmpq	$1, %r15
	jne	.LBB47_230
	jmp	.LBB47_232
.LBB47_228:                             # %.lr.ph568.prol
                                        #   in Loop: Header=BB47_189 Depth=1
	imulq	$392, %rbx, %rcx        # imm = 0x188
	movsd	88(%rsp,%rcx), %xmm1    # xmm1 = mem[0],zero
	movq	16(%rsp), %rcx          # 8-byte Reload
	mulsd	20080(%rsp,%rcx,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	movl	$2, %edi
	cmpq	$1, %r15
	je	.LBB47_232
.LBB47_230:                             # %.lr.ph568.preheader.new
                                        #   in Loop: Header=BB47_189 Depth=1
	movq	%r15, %rcx
	subq	%rdi, %rcx
	leaq	(%r14,%rdi,8), %rdx
	imulq	$784, %rdi, %rbp        # imm = 0x310
	addq	%r8, %rbp
	movq	$-1, %rdi
	.p2align	4, 0x90
.LBB47_231:                             # %.lr.ph568
                                        #   Parent Loop BB47_189 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	400(%rdx,%rdi,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	(%rbp), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	movsd	408(%rdx,%rdi,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	784(%rbp), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	addq	$2, %rdi
	addq	$1568, %rbp             # imm = 0x620
	cmpq	%rdi, %rcx
	jne	.LBB47_231
.LBB47_232:                             # %._crit_edge569
                                        #   in Loop: Header=BB47_189 Depth=1
	imulq	$392, %rbx, %rcx        # imm = 0x188
	leaq	80(%rsp,%rcx), %rcx
	divsd	(%rcx,%rbx,8), %xmm0
	movsd	%xmm0, (%rsi)
	incq	%r15
	addq	$392, %r14              # imm = 0x188
	cmpq	%r10, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB47_189
# BB#223:                               # %.preheader539
	testl	%r9d, %r9d
	jle	.LBB47_238
# BB#224:                               # %.preheader538.lr.ph
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	20864(%rsp,%rcx,8), %r8
	leaq	864(%rsp,%r10,8), %r9
	xorl	%r14d, %r14d
	movq	%r10, %rbx
.LBB47_225:                             # %.preheader538
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB47_236 Depth 2
	imulq	$784, %rbx, %rcx        # imm = 0x310
	leaq	19296(%rsp,%rcx), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rsi
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	%r10, %rbx
	jge	.LBB47_237
# BB#226:                               # %.lr.ph563.preheader
                                        #   in Loop: Header=BB47_225 Depth=1
	testb	$1, %r14b
	jne	.LBB47_233
# BB#227:                               #   in Loop: Header=BB47_225 Depth=1
	movq	%rbx, %rdi
	cmpq	$1, %r14
	jne	.LBB47_235
	jmp	.LBB47_237
.LBB47_233:                             # %.lr.ph563.prol
                                        #   in Loop: Header=BB47_225 Depth=1
	leaq	1(%rbx), %rdi
	imulq	$392, %rdi, %rcx        # imm = 0x188
	leaq	80(%rsp,%rcx), %rcx
	movsd	(%rcx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	imulq	$784, %rdi, %rcx        # imm = 0x310
	leaq	19296(%rsp,%rcx), %rcx
	movq	16(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rcx,%rdx,8), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	cmpq	$1, %r14
	je	.LBB47_237
.LBB47_235:                             # %.lr.ph563.preheader.new
                                        #   in Loop: Header=BB47_225 Depth=1
	movq	%r10, %rcx
	subq	%rdi, %rcx
	imulq	$784, %rdi, %rdx        # imm = 0x310
	addq	%r8, %rdx
	imulq	$392, %rdi, %rdi        # imm = 0x188
	movq	%r9, %rbp
	.p2align	4, 0x90
.LBB47_236:                             # %.lr.ph563
                                        #   Parent Loop BB47_225 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-392(%rdi,%rbp), %xmm1  # xmm1 = mem[0],zero
	mulsd	-784(%rdx), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	movsd	(%rdi,%rbp), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rdx), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	addq	$1568, %rdx             # imm = 0x620
	addq	$784, %rbp              # imm = 0x310
	addq	$-2, %rcx
	jne	.LBB47_236
.LBB47_237:                             # %._crit_edge564
                                        #   in Loop: Header=BB47_225 Depth=1
	imulq	$392, %rbx, %rcx        # imm = 0x188
	leaq	80(%rsp,%rcx), %rcx
	divsd	(%rcx,%rbx,8), %xmm0
	movsd	%xmm0, (%rsi)
	incq	%r14
	addq	$-8, %r9
	cmpq	$1, %rbx
	leaq	-1(%rbx), %rbx
	jg	.LBB47_225
.LBB47_238:                             # %.loopexit
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	$0, _ZZ8wavModelR5MixerE1n(,%rcx,4)
	addl	%eax, %r11d
	jle	.LBB47_240
.LBB47_241:                             # %.lr.ph.preheader
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	20080(%rsp,%rax,8), %rbp
	xorpd	%xmm0, %xmm0
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB47_242:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	incq	%rbx
	xorl	%esi, %esi
	movl	%ebx, %edi
	callq	_Z1Xii
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movslq	_ZL1D(%rip), %rax
	movslq	_ZL1S(%rip), %rcx
	addq	%rax, %rcx
	addq	$784, %rbp              # imm = 0x310
	cmpq	%rcx, %rbx
	jl	.LBB47_242
	jmp	.LBB47_243
.LBB47_240:
	xorpd	%xmm0, %xmm0
.LBB47_243:                             # %._crit_edge
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	_ZZ8wavModelR5MixerE2pr.1(,%r14,4), %eax
	movl	%eax, _ZZ8wavModelR5MixerE2pr.2(,%r14,4)
	movl	_ZZ8wavModelR5MixerE2pr.0(,%r14,4), %eax
	movl	%eax, _ZZ8wavModelR5MixerE2pr.1(,%r14,4)
	callq	floor
	cvttsd2si	%xmm0, %eax
	incl	_ZZ8wavModelR5MixerE7counter(,%r14,4)
	movl	%eax, _ZZ8wavModelR5MixerE2pr.0(,%r14,4)
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%eax, %ecx
	movq	buf+16(%rip), %rdi
	movslq	%ecx, %rax
	movzbl	(%rdi,%rax), %eax
	imull	$30005491, %eax, %ecx   # imm = 0x1C9D8F3
	movl	36(%rsp), %ebp          # 4-byte Reload
	imull	$50004239, %ebp, %edx   # imm = 0x2FB010F
	leal	19995673(%rdx,%rcx), %ecx
	movq	%rdx, %r9
	shrl	$3, %eax
	movl	%ebp, %r8d
	shrl	$4, %r8d
	xorl	%r8d, %eax
	xorl	%ecx, %eax
	shrl	$9, %ecx
	xorl	%eax, %ecx
	xorl	$67108864, %ecx         # imm = 0x4000000
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movq	_ZZ8wavModelR5MixerE2cm+96(%rip), %r12
	movl	%ecx, (%r12,%rax,4)
	movl	pos(%rip), %eax
	leal	-1(%rax), %ecx
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	addl	$-2, %eax
	andl	%edx, %eax
	cltq
	movzbl	(%rdi,%rax), %eax
	imull	$30005491, %ecx, %edx   # imm = 0x1C9D8F3
	imull	$50004239, %eax, %esi   # imm = 0x2FB010F
	imull	$70004807, %ebp, %r10d  # imm = 0x42C3047
	addl	%r10d, %edx
	leal	290003459(%rsi,%rdx), %edx
	shrl	$3, %ecx
	shrl	$4, %eax
	movl	%ebp, %r11d
	shrl	$5, %r11d
	xorl	%r11d, %ecx
	xorl	%eax, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	xorl	$67108863, %edx         # imm = 0x3FFFFFF
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%r12,%rax,4)
	movl	pos(%rip), %ecx
	leal	-1(%rcx), %eax
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %eax
	cltq
	movzbl	(%rdi,%rax), %eax
	leal	-2(%rcx), %esi
	andl	%edx, %esi
	movslq	%esi, %rsi
	movzbl	(%rdi,%rsi), %esi
	movl	%esi, %ebx
	shrl	$3, %ebx
	addl	$-3, %ecx
	andl	%edx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	imull	$30005491, %eax, %edx   # imm = 0x1C9D8F3
	imull	$50004239, %ebx, %ebx   # imm = 0x2FB010F
	imull	$70004807, %ecx, %r15d  # imm = 0x42C3047
	imull	$110002499, %ebp, %edi  # imm = 0x68E8143
	addl	%edx, %edi
	addl	%ebx, %edi
	leal	600008937(%r15,%rdi), %edx
	shrl	$3, %eax
	shrl	$7, %esi
	shrl	$5, %ecx
	shrl	$6, %ebp
	xorl	%eax, %ebp
	xorl	%esi, %ecx
	xorl	%ebp, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movl	%ecx, (%r12,%rax,4)
	movl	pos(%rip), %eax
	leal	-4(%rax), %ecx
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %ecx
	movq	buf+16(%rip), %rbx
	movslq	%ecx, %rcx
	movzbl	(%rbx,%rcx), %ecx
	leal	-2(%rax), %esi
	andl	%edx, %esi
	movslq	%esi, %rsi
	movzbl	(%rbx,%rsi), %esi
	addl	%ecx, %esi
	addl	$-6, %eax
	andl	%edx, %eax
	cltq
	movzbl	(%rbx,%rax), %eax
	subl	%eax, %esi
	movzbl	%sil, %eax
	imull	$30005491, %eax, %ecx   # imm = 0x1C9D8F3
	leal	620004610(%r9,%rcx), %edx
	shrl	$3, %eax
	movl	%r8d, %ecx
	xorl	$67108865, %ecx         # imm = 0x4000001
	xorl	%ecx, %eax
	xorl	%edx, %eax
	shrl	$9, %edx
	xorl	%eax, %edx
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %esi
	movl	%esi, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %esi  # imm = 0x75BCD17
	addl	%eax, %esi
	movq	_ZZ8wavModelR5MixerE2cm+96(%rip), %r15
	movl	%esi, (%r15,%rax,4)
	movl	_ZZ8wavModelR5MixerE2pr.0(,%r14,4), %esi
	movzbl	%sil, %edi
	imull	$30005491, %edi, %r13d  # imm = 0x1C9D8F3
	leal	820007589(%r9,%r13), %eax
	movq	%r9, 8(%rsp)            # 8-byte Spill
	shrl	$3, %edi
	movl	%ecx, %ebp
	xorl	%edi, %ebp
	xorl	%eax, %ebp
	shrl	$9, %eax
	xorl	%ebp, %eax
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rbp
	leal	1(%rbp), %edx
	movl	%edx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%ebp, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ebp, %eax
	movl	%eax, (%r15,%rbp,4)
	movl	_ZZ8wavModelR5MixerE1w(%rip), %eax
	movl	pos(%rip), %edx
	subl	%eax, %edx
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%edx, %ebp
	movslq	%ebp, %rdx
	movzbl	(%rbx,%rdx), %edx
	addl	%esi, %edx
	movl	_ZZ8wavModelR5MixerE2pr.1(,%r14,4), %r12d
	subl	%r12d, %edx
	movzbl	%dl, %edx
	imull	$30005491, %edx, %ebx   # imm = 0x1C9D8F3
	leal	1020010568(%r9,%rbx), %ebx
	shrl	$3, %edx
	xorl	%ecx, %edx
	xorl	%ebx, %edx
	shrl	$9, %ebx
	xorl	%edx, %ebx
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ebx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%r15,%rcx,4)
	movl	pos(%rip), %ecx
	movl	%ecx, %ebp
	subl	%eax, %ebp
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %ebp
	movq	buf+16(%rip), %rbx
	movslq	%ebp, %rbp
	movzbl	(%rbx,%rbp), %esi
	movl	$1, %ebp
	subl	%eax, %ebp
	addl	%ecx, %ebp
	andl	%edx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%rbx,%rbp), %ebp
	shll	$8, %ebp
	orl	%esi, %ebp
	subl	%r12d, %ebp
	addl	%eax, %eax
	movl	$1, %esi
	subl	%eax, %esi
	addl	%ecx, %esi
	subl	%eax, %ecx
	andl	%edx, %ecx
	movslq	%ecx, %rax
	movzbl	(%rbx,%rax), %eax
	andl	%edx, %esi
	movslq	%esi, %rcx
	movzbl	(%rbx,%rcx), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movl	_ZZ8wavModelR5MixerE2pr.2(,%r14,4), %r15d
	subl	%r15d, %ebp
	addl	%ecx, %ebp
	shrl	%ebp
	movzbl	%bpl, %ecx
	imull	$50004239, %ecx, %edx   # imm = 0x2FB010F
	addl	%r13d, %r10d
	leal	1290018354(%rdx,%r10), %edx
	shrl	$4, %ecx
	xorl	%r11d, %edi
	xorl	%ecx, %edi
	xorl	%edx, %edi
	shrl	$9, %edx
	xorl	%edi, %edx
	xorl	$67108862, %edx         # imm = 0x3FFFFFE
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %ecx  # imm = 0x3ADE68B3
	addl	%esi, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %edx  # imm = 0x75BCD17
	addl	%esi, %edx
	movq	_ZZ8wavModelR5MixerE2cm+96(%rip), %rcx
	movl	%edx, (%rcx,%rsi,4)
	movl	_ZZ8wavModelR5MixerE2pr.0(,%r14,4), %edx
	movl	_ZZ8wavModelR5MixerE1w(%rip), %r13d
	movl	pos(%rip), %esi
	movl	%esi, %edi
	subl	%r13d, %edi
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%ebp, %edi
	movslq	%edi, %rdi
	movzbl	(%rbx,%rdi), %edi
	movl	_ZZ8wavModelR5MixerE2pr.1(,%r14,4), %eax
	addl	%eax, %eax
	leal	(%r13,%r13), %r9d
	subl	%r9d, %esi
	andl	%ebp, %esi
	movslq	%esi, %rsi
	movzbl	(%rbx,%rsi), %esi
	subl	%eax, %edx
	subl	%esi, %edx
	addl	%r15d, %edx
	leal	(%rdx,%rdi,2), %eax
	movzbl	%al, %eax
	imull	$30005491, %eax, %edx   # imm = 0x1C9D8F3
	movq	8(%rsp), %rsi           # 8-byte Reload
	leal	1420016526(%rsi,%rdx), %edx
	shrl	$3, %eax
	xorl	%r8d, %eax
	xorl	%edx, %eax
	shrl	$9, %edx
	xorl	%eax, %edx
	xorl	$67108866, %edx         # imm = 0x4000002
	movslq	_ZZ8wavModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %esi
	movl	%esi, _ZZ8wavModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movl	%edx, (%rcx,%rax,4)
	movl	pos(%rip), %r15d
	movl	$1, %ecx
	subl	%r13d, %ecx
	addl	%r15d, %ecx
	movl	%r15d, %eax
	movl	$1, %r12d
	subl	%r9d, %r12d
	addl	%r15d, %r12d
	movl	_ZZ8wavModelR5MixerE1w(%rip), %r11d
	movl	%r15d, %r14d
	movl	$1, %esi
	subl	%r11d, %esi
	addl	%r15d, %esi
	movl	%r15d, %r10d
	movl	%r15d, %ebx
	subl	%r13d, %ebx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %ebx
	movq	buf+16(%rip), %rdx
	movslq	%ebx, %r13
	movzbl	(%rdx,%r13), %ebp
	andl	%edi, %ecx
	movslq	%ecx, %r8
	movzbl	(%rdx,%r8), %ebx
	shll	$8, %ebx
	orl	%ebp, %ebx
	shll	$8, %ebx
	movl	$130816, %ebp           # imm = 0x1FF00
	movl	_ZZ8wavModelR5MixerE4scm1(%rip), %ecx
	addl	%ebp, %ecx
	andl	%ebx, %ecx
	andl	$130816, %ecx           # imm = 0x1FF00
	movl	%ecx, _ZZ8wavModelR5MixerE4scm1+24(%rip)
	leal	(%r11,%r11), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	$1, %ebx
	subl	%ecx, %ebx
	addl	%r15d, %ebx
	movzbl	(%rdx,%r13), %r13d
	movzbl	(%rdx,%r8), %ecx
	shll	$8, %ecx
	orl	%r13d, %ecx
	movl	$1, %r8d
	subl	%r9d, %eax
	leal	(%r11,%r11,2), %r13d
	subl	%r13d, %r8d
	addl	%r15d, %r8d
	addl	%ecx, %ecx
	andl	%edi, %eax
	cltq
	movzbl	(%rdx,%rax), %r9d
	andl	%edi, %r12d
	movslq	%r12d, %rax
	movzbl	(%rdx,%rax), %eax
	shll	$8, %eax
	orl	%r9d, %eax
	subl	%eax, %ecx
	shll	$8, %ecx
	addl	_ZZ8wavModelR5MixerE4scm2(%rip), %ebp
	andl	%ecx, %ebp
	andl	$130816, %ebp           # imm = 0x1FF00
	movl	%ebp, _ZZ8wavModelR5MixerE4scm2+24(%rip)
	subl	%r11d, %r14d
	andl	%edi, %r14d
	movslq	%r14d, %rax
	movzbl	(%rdx,%rax), %eax
	andl	%edi, %esi
	movslq	%esi, %rcx
	movzbl	(%rdx,%rcx), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	subl	8(%rsp), %r10d          # 4-byte Folded Reload
	andl	%edi, %r10d
	movslq	%r10d, %rax
	movzbl	(%rdx,%rax), %eax
	andl	%edi, %ebx
	movslq	%ebx, %rsi
	movzbl	(%rdx,%rsi), %esi
	shll	$8, %esi
	orl	%eax, %esi
	subl	%esi, %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%r13d, %r15d
	andl	%edi, %r15d
	movslq	%r15d, %rax
	movzbl	(%rdx,%rax), %esi
	andl	%edi, %r8d
	movslq	%r8d, %rax
	movzbl	(%rdx,%rax), %eax
	shll	$8, %eax
	orl	%esi, %eax
	addl	%ecx, %eax
	andl	$511, %eax              # imm = 0x1FF
	movl	$_ZZ8wavModelR5MixerE4scm3, %ecx
	movl	$_ZZ8wavModelR5MixerE4scm3+24, %edx
.LBB47_269:                             # %.sink.split
	shll	$8, %eax
	movl	$-256, %esi
	addl	(%rcx), %esi
	andl	%eax, %esi
	movl	%esi, (%rdx)
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB47_270:
	movl	y(%rip), %edx
	shll	$16, %edx
	movq	_ZZ8wavModelR5MixerE4scm1+32(%rip), %rax
	movzwl	(%rax), %ecx
	orl	$64, %edx
	movl	%edx, %esi
	subl	%ecx, %esi
	shrl	$7, %esi
	addl	%ecx, %esi
	movw	%si, (%rax)
	movl	c0(%rip), %r9d
	movl	_ZZ8wavModelR5MixerE4scm1+24(%rip), %eax
	addl	%r9d, %eax
	movq	_ZZ8wavModelR5MixerE4scm1+16(%rip), %rsi
	cltq
	leaq	(%rsi,%rax,2), %rdi
	movq	%rdi, _ZZ8wavModelR5MixerE4scm1+32(%rip)
	movzwl	(%rsi,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movq	stretch+16(%rip), %rsi
	movzwl	(%rsi,%rax), %ecx
	movslq	96(%r14), %r8
	movq	32(%r14), %rdi
	movw	%cx, (%rdi,%r8,2)
	movq	_ZZ8wavModelR5MixerE4scm2+32(%rip), %rax
	movzwl	(%rax), %ebx
	movl	%edx, %ecx
	subl	%ebx, %ecx
	shrl	$7, %ecx
	addl	%ebx, %ecx
	movw	%cx, (%rax)
	movl	_ZZ8wavModelR5MixerE4scm2+24(%rip), %eax
	addl	%r9d, %eax
	movq	_ZZ8wavModelR5MixerE4scm2+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbx
	movq	%rbx, _ZZ8wavModelR5MixerE4scm2+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	movw	%ax, 2(%rdi,%r8,2)
	movq	_ZZ8wavModelR5MixerE4scm3+32(%rip), %rax
	movzwl	(%rax), %ecx
	movl	%edx, %ebp
	subl	%ecx, %ebp
	shrl	$7, %ebp
	addl	%ecx, %ebp
	movw	%bp, (%rax)
	movl	_ZZ8wavModelR5MixerE4scm3+24(%rip), %eax
	addl	%r9d, %eax
	movq	_ZZ8wavModelR5MixerE4scm3+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbp
	movq	%rbp, _ZZ8wavModelR5MixerE4scm3+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	movw	%ax, 4(%rdi,%r8,2)
	movq	_ZZ8wavModelR5MixerE4scm4+32(%rip), %rax
	movzwl	(%rax), %ecx
	subl	%ecx, %edx
	shrl	$7, %edx
	addl	%ecx, %edx
	movw	%dx, (%rax)
	addl	_ZZ8wavModelR5MixerE4scm4+24(%rip), %r9d
	movq	_ZZ8wavModelR5MixerE4scm4+16(%rip), %rax
	movslq	%r9d, %rcx
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, _ZZ8wavModelR5MixerE4scm4+32(%rip)
	movzwl	(%rax,%rcx,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rsi,%rax), %eax
	movw	%ax, 6(%rdi,%r8,2)
	movl	y(%rip), %r9d
	shll	$16, %r9d
	movq	_ZZ8wavModelR5MixerE4scm5+32(%rip), %rax
	movzwl	(%rax), %edx
	orl	$64, %r9d
	movl	%r9d, %esi
	subl	%edx, %esi
	shrl	$7, %esi
	addl	%edx, %esi
	movw	%si, (%rax)
	movl	c0(%rip), %edx
	movl	_ZZ8wavModelR5MixerE4scm5+24(%rip), %eax
	addl	%edx, %eax
	movq	_ZZ8wavModelR5MixerE4scm5+16(%rip), %rsi
	cltq
	leaq	(%rsi,%rax,2), %rdi
	movq	%rdi, _ZZ8wavModelR5MixerE4scm5+32(%rip)
	movzwl	(%rsi,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movq	stretch+16(%rip), %rdi
	movzwl	(%rdi,%rax), %eax
	movq	32(%r14), %rsi
	movw	%ax, 8(%rsi,%r8,2)
	movq	_ZZ8wavModelR5MixerE4scm6+32(%rip), %rax
	movzwl	(%rax), %ebp
	movl	%r9d, %ecx
	subl	%ebp, %ecx
	shrl	$7, %ecx
	addl	%ebp, %ecx
	movw	%cx, (%rax)
	movl	_ZZ8wavModelR5MixerE4scm6+24(%rip), %eax
	addl	%edx, %eax
	movq	_ZZ8wavModelR5MixerE4scm6+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbp
	movq	%rbp, _ZZ8wavModelR5MixerE4scm6+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rdi,%rax), %eax
	movw	%ax, 10(%rsi,%r8,2)
	movq	_ZZ8wavModelR5MixerE4scm7+32(%rip), %rax
	movzwl	(%rax), %ecx
	movl	%r9d, %ebx
	subl	%ecx, %ebx
	shrl	$7, %ebx
	addl	%ecx, %ebx
	movw	%bx, (%rax)
	movl	_ZZ8wavModelR5MixerE4scm7+24(%rip), %eax
	addl	%edx, %eax
	movq	_ZZ8wavModelR5MixerE4scm7+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbp
	movq	%rbp, _ZZ8wavModelR5MixerE4scm7+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rdi,%rax), %eax
	movw	%ax, 12(%rsi,%r8,2)
	movq	_ZZ8wavModelR5MixerE4scm8+32(%rip), %rax
	movzwl	(%rax), %ecx
	subl	%ecx, %r9d
	shrl	$7, %r9d
	addl	%ecx, %r9d
	movw	%r9w, (%rax)
	movl	_ZZ8wavModelR5MixerE4scm8+24(%rip), %eax
	addl	%edx, %eax
	movq	_ZZ8wavModelR5MixerE4scm8+16(%rip), %rcx
	cltq
	leaq	(%rcx,%rax,2), %rbp
	movq	%rbp, _ZZ8wavModelR5MixerE4scm8+32(%rip)
	movzwl	(%rcx,%rax,2), %eax
	shrq	$3, %rax
	andl	$8190, %eax             # imm = 0x1FFE
	movzwl	(%rdi,%rax), %eax
	leal	8(%r8), %ecx
	movl	%ecx, 96(%r14)
	movw	%ax, 14(%rsi,%r8,2)
	movl	bpos(%rip), %ecx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ8wavModelR5MixerE2cm, %edi
	movq	%r14, %rsi
	callq	_ZN10ContextMap4mix1ER5Mixeriiii
	movl	_ZZ8wavModelR5MixerE8channels(%rip), %eax
	shll	$8, %eax
	orl	_ZZ8wavModelR5MixerE4bits(%rip), %eax
.LBB47_271:
	addq	$57720, %rsp            # imm = 0xE178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB47_8:
.Ltmp192:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp193:
# BB#9:                                 # %.noexc
.LBB47_18:
.Ltmp195:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp196:
# BB#19:                                # %.noexc302
.LBB47_28:
.Ltmp198:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp199:
# BB#29:                                # %.noexc308
.LBB47_38:
.Ltmp201:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp202:
# BB#39:                                # %.noexc314
.LBB47_48:
.Ltmp204:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp205:
# BB#49:                                # %.noexc320
.LBB47_58:
.Ltmp207:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp208:
# BB#59:                                # %.noexc326
.LBB47_68:
.Ltmp210:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp211:
# BB#69:                                # %.noexc332
.LBB47_78:
.Ltmp213:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp214:
# BB#79:                                # %.noexc338
.LBB47_113:
.Ltmp215:
	movq	%rax, %rbx
	movl	$_ZGVZ8wavModelR5MixerE4scm8, %edi
	jmp	.LBB47_273
.LBB47_112:
.Ltmp212:
	movq	%rax, %rbx
	movl	$_ZGVZ8wavModelR5MixerE4scm7, %edi
	jmp	.LBB47_273
.LBB47_111:
.Ltmp209:
	movq	%rax, %rbx
	movl	$_ZGVZ8wavModelR5MixerE4scm6, %edi
	jmp	.LBB47_273
.LBB47_110:
.Ltmp206:
	movq	%rax, %rbx
	movl	$_ZGVZ8wavModelR5MixerE4scm5, %edi
	jmp	.LBB47_273
.LBB47_109:
.Ltmp203:
	movq	%rax, %rbx
	movl	$_ZGVZ8wavModelR5MixerE4scm4, %edi
	jmp	.LBB47_273
.LBB47_108:
.Ltmp200:
	movq	%rax, %rbx
	movl	$_ZGVZ8wavModelR5MixerE4scm3, %edi
	jmp	.LBB47_273
.LBB47_107:
.Ltmp197:
	movq	%rax, %rbx
	movl	$_ZGVZ8wavModelR5MixerE4scm2, %edi
	jmp	.LBB47_273
.LBB47_272:
.Ltmp194:
	movq	%rax, %rbx
	movl	$_ZGVZ8wavModelR5MixerE4scm1, %edi
	jmp	.LBB47_273
.LBB47_114:
.Ltmp218:
	movq	%rax, %rbx
	movl	$_ZGVZ8wavModelR5MixerE2cm, %edi
.LBB47_273:
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end47:
	.size	_Z8wavModelR5Mixer, .Lfunc_end47-_Z8wavModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table47:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp216-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp217-.Ltmp216       #   Call between .Ltmp216 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin12 #     jumps to .Ltmp218
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp192-.Ltmp217       #   Call between .Ltmp217 and .Ltmp192
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp193-.Ltmp192       #   Call between .Ltmp192 and .Ltmp193
	.long	.Ltmp194-.Lfunc_begin12 #     jumps to .Ltmp194
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin12 #     jumps to .Ltmp197
	.byte	0                       #   On action: cleanup
	.long	.Ltmp198-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp199-.Ltmp198       #   Call between .Ltmp198 and .Ltmp199
	.long	.Ltmp200-.Lfunc_begin12 #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp201-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp202-.Ltmp201       #   Call between .Ltmp201 and .Ltmp202
	.long	.Ltmp203-.Lfunc_begin12 #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp204-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp205-.Ltmp204       #   Call between .Ltmp204 and .Ltmp205
	.long	.Ltmp206-.Lfunc_begin12 #     jumps to .Ltmp206
	.byte	0                       #   On action: cleanup
	.long	.Ltmp207-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp208-.Ltmp207       #   Call between .Ltmp207 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin12 #     jumps to .Ltmp209
	.byte	0                       #   On action: cleanup
	.long	.Ltmp210-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp211-.Ltmp210       #   Call between .Ltmp210 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin12 #     jumps to .Ltmp212
	.byte	0                       #   On action: cleanup
	.long	.Ltmp213-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp215-.Lfunc_begin12 #     jumps to .Ltmp215
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Lfunc_end47-.Ltmp214   #   Call between .Ltmp214 and .Lfunc_end47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._Z1Xii,"axG",@progbits,_Z1Xii,comdat
	.weak	_Z1Xii
	.p2align	4, 0x90
	.type	_Z1Xii,@function
_Z1Xii:                                 # @_Z1Xii
	.cfi_startproc
# BB#0:
	movl	_ZL5wmode(%rip), %eax
	cmpl	$10, %eax
	je	.LBB48_8
# BB#1:
	cmpl	$17, %eax
	je	.LBB48_7
# BB#2:
	cmpl	$18, %eax
	jne	.LBB48_11
# BB#3:
	movl	_ZL1S(%rip), %eax
	addl	%edi, %esi
	cmpl	%edi, %eax
	jge	.LBB48_4
# BB#6:
	subl	%eax, %esi
	shll	$2, %esi
	movl	pos(%rip), %eax
	movl	$2, %ecx
	subl	%esi, %ecx
	addl	%eax, %ecx
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %ecx
	movq	buf+16(%rip), %rdi
	movslq	%ecx, %rcx
	movzbl	(%rdi,%rcx), %r8d
	movl	$3, %ecx
	subl	%esi, %ecx
	addl	%eax, %ecx
	andl	%edx, %ecx
	movslq	%ecx, %rax
	movzbl	(%rdi,%rax), %eax
	shll	$8, %eax
	orl	%r8d, %eax
	cwtl
	retq
.LBB48_8:
	movl	_ZL1S(%rip), %eax
	addl	%edi, %esi
	cmpl	%edi, %eax
	jge	.LBB48_9
# BB#10:
	subl	%esi, %eax
	leal	1(%rax,%rax), %eax
	addl	pos(%rip), %eax
	jmp	.LBB48_13
.LBB48_7:
	addl	%edi, %esi
	addl	%esi, %esi
	jmp	.LBB48_5
.LBB48_11:
	movl	pos(%rip), %eax
	addl	%edi, %esi
	jmp	.LBB48_12
.LBB48_9:
	addl	%esi, %esi
	movl	pos(%rip), %eax
.LBB48_12:
	subl	%esi, %eax
.LBB48_13:
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%eax, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %eax
	retq
.LBB48_4:
	shll	$2, %esi
.LBB48_5:
	movl	pos(%rip), %eax
	movl	$1, %ecx
	subl	%esi, %ecx
	addl	%eax, %ecx
	subl	%esi, %eax
	movl	buf(%rip), %edx
	decl	%edx
	andl	%edx, %eax
	movq	buf+16(%rip), %rsi
	cltq
	movzbl	(%rsi,%rax), %eax
	andl	%edx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movswl	%cx, %eax
	retq
.Lfunc_end48:
	.size	_Z1Xii, .Lfunc_end48-_Z1Xii
	.cfi_endproc

	.text
	.globl	_Z6execxtii
	.p2align	4, 0x90
	.type	_Z6execxtii,@function
_Z6execxtii:                            # @_Z6execxtii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi215:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi216:
	.cfi_def_cfa_offset 24
.Lcfi217:
	.cfi_offset %rbx, -24
.Lcfi218:
	.cfi_offset %rbp, -16
	movl	pos(%rip), %r10d
	movl	$-2, %eax
	subl	%edi, %eax
	addl	%r10d, %eax
	movl	buf(%rip), %r11d
	decl	%r11d
	andl	%r11d, %eax
	movq	buf+16(%rip), %r8
	cltq
	movb	(%r8,%rax), %al
	xorl	%edx, %edx
	cmpb	$15, %al
	sete	%dl
	xorl	%ecx, %ecx
	cmpb	$102, %al
	sete	%cl
	leal	(%rdx,%rcx,2), %r9d
	xorl	%ecx, %ecx
	cmpb	$103, %al
	sete	%cl
	leal	(%rcx,%rcx,2), %ebp
	movl	$-3, %ecx
	subl	%edi, %ecx
	addl	%r10d, %ecx
	andl	%r11d, %ecx
	movslq	%ecx, %rcx
	movb	(%r8,%rcx), %cl
	xorl	%ebx, %ebx
	cmpb	$15, %cl
	sete	%bl
	xorl	%eax, %eax
	cmpb	$102, %cl
	sete	%al
	xorl	%edx, %edx
	cmpb	$103, %cl
	movl	$12, %ecx
	cmovnel	%edx, %ecx
	orl	%ebp, %ecx
	addl	%r9d, %ecx
	leal	(%rcx,%rbx,4), %ecx
	leal	(%rcx,%rax,8), %r9d
	movl	%edi, %eax
	notl	%eax
	addl	%r10d, %eax
	andl	%r11d, %eax
	cltq
	movzbl	(%r8,%rax), %eax
	testl	%edi, %edi
	je	.LBB49_2
# BB#1:
	subl	%edi, %r10d
	andl	%r10d, %r11d
	movslq	%r11d, %rcx
	movzbl	(%r8,%rcx), %edx
	shll	$12, %edx
	andl	$815104, %edx           # imm = 0xC7000
.LBB49_2:
	shll	$4, %eax
	shll	$20, %esi
	orl	%eax, %esi
	orl	%r9d, %esi
	orl	%edx, %esi
	movl	%esi, %eax
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end49:
	.size	_Z6execxtii, .Lfunc_end49-_Z6execxtii
	.cfi_endproc

	.globl	_Z8exeModelR5Mixer
	.p2align	4, 0x90
	.type	_Z8exeModelR5Mixer,@function
_Z8exeModelR5Mixer:                     # @_Z8exeModelR5Mixer
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%rbp
.Lcfi219:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi220:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi221:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi222:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi223:
	.cfi_def_cfa_offset 48
.Lcfi224:
	.cfi_offset %rbx, -48
.Lcfi225:
	.cfi_offset %r12, -40
.Lcfi226:
	.cfi_offset %r14, -32
.Lcfi227:
	.cfi_offset %r15, -24
.Lcfi228:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movb	_ZGVZ8exeModelR5MixerE2cm(%rip), %al
	testb	%al, %al
	jne	.LBB50_4
# BB#1:
	movl	$_ZGVZ8exeModelR5MixerE2cm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB50_4
# BB#2:
	movb	level(%rip), %cl
	movl	$65536, %esi            # imm = 0x10000
	shll	%cl, %esi
.Ltmp219:
	movl	$_ZZ8exeModelR5MixerE2cm, %edi
	movl	$12, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp220:
# BB#3:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ8exeModelR5MixerE2cm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8exeModelR5MixerE2cm, %edi
	callq	__cxa_guard_release
.LBB50_4:
	movl	bpos(%rip), %ecx
	movq	buf+16(%rip), %r10
	testl	%ecx, %ecx
	jne	.LBB50_10
# BB#5:                                 # %.preheader.preheader
	xorl	%r8d, %r8d
	movl	$-3, %esi
	movl	$12, %r9d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB50_6:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	pos(%rip), %r15d
	leal	-1(%r15), %eax
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %eax
	cltq
	movzbl	(%r10,%rax), %ebx
	leal	(%rsi,%r15), %eax
	leal	1(%rsi,%r15), %ebp
	andl	%ecx, %ebp
	movslq	%ebp, %rbp
	movzbl	(%r10,%rbp), %edx
	xorl	%ebp, %ebp
	cmpb	$15, %dl
	sete	%bpl
	xorl	%edi, %edi
	cmpb	$102, %dl
	sete	%dil
	leal	(%rbp,%rdi,2), %r11d
	xorl	%ebp, %ebp
	cmpb	$103, %dl
	sete	%bpl
	leal	(%rbp,%rbp,2), %edx
	andl	%ecx, %eax
	cltq
	movzbl	(%r10,%rax), %eax
	xorl	%ebp, %ebp
	cmpb	$15, %al
	sete	%bpl
	xorl	%edi, %edi
	cmpb	$102, %al
	sete	%dil
	cmpb	$103, %al
	movl	$0, %eax
	cmovel	%r9d, %eax
	orl	%edx, %eax
	addl	%r11d, %eax
	leal	(%rax,%rbp,4), %eax
	leal	(%rax,%rdi,8), %r11d
	leal	2(%rsi,%r15), %eax
	andl	%ecx, %eax
	cltq
	movzbl	(%r10,%rax), %eax
	testl	%r12d, %r12d
	movl	$0, %ebp
	je	.LBB50_8
# BB#7:                                 #   in Loop: Header=BB50_6 Depth=1
	leal	3(%rsi,%r15), %edx
	andl	%edx, %ecx
	movslq	%ecx, %rcx
	movzbl	(%r10,%rcx), %ebp
	shll	$12, %ebp
	andl	$815104, %ebp           # imm = 0xC7000
.LBB50_8:                               # %_Z6execxtii.exit
                                        #   in Loop: Header=BB50_6 Depth=1
	shll	$4, %eax
	shll	$20, %ebx
	cmpl	$4, %r12d
	cmovlel	%r8d, %ebx
	orl	%eax, %ebx
	orl	%r11d, %ebx
	orl	%ebp, %ebx
	movslq	_ZZ8exeModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ8exeModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ebx, %ecx  # imm = 0x3ADE68B3
	addl	%eax, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movq	_ZZ8exeModelR5MixerE2cm+96(%rip), %rdx
	movl	%ecx, (%rdx,%rax,4)
	incl	%r12d
	decl	%esi
	cmpl	$12, %r12d
	jne	.LBB50_6
# BB#9:                                 # %.loopexit.loopexit
	movl	bpos(%rip), %ecx
.LBB50_10:                              # %.loopexit
	movl	c0(%rip), %edx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movslq	%esi, %rax
	movzbl	(%r10,%rax), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ8exeModelR5MixerE2cm, %edi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN10ContextMap4mix1ER5Mixeriiii # TAILCALL
.LBB50_11:
.Ltmp221:
	movq	%rax, %rbx
	movl	$_ZGVZ8exeModelR5MixerE2cm, %edi
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end50:
	.size	_Z8exeModelR5Mixer, .Lfunc_end50-_Z8exeModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table50:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp219-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp220-.Ltmp219       #   Call between .Ltmp219 and .Ltmp220
	.long	.Ltmp221-.Lfunc_begin13 #     jumps to .Ltmp221
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Lfunc_end50-.Ltmp220   #   Call between .Ltmp220 and .Lfunc_end50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z13indirectModelR5Mixer
	.p2align	4, 0x90
	.type	_Z13indirectModelR5Mixer,@function
_Z13indirectModelR5Mixer:               # @_Z13indirectModelR5Mixer
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi229:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi230:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi231:
	.cfi_def_cfa_offset 32
.Lcfi232:
	.cfi_offset %rbx, -24
.Lcfi233:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movb	_ZGVZ13indirectModelR5MixerE2cm(%rip), %al
	testb	%al, %al
	jne	.LBB51_4
# BB#1:
	movl	$_ZGVZ13indirectModelR5MixerE2cm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB51_4
# BB#2:
	movb	level(%rip), %cl
	movl	$65536, %esi            # imm = 0x10000
	shll	%cl, %esi
.Ltmp222:
	movl	$_ZZ13indirectModelR5MixerE2cm, %edi
	movl	$6, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp223:
# BB#3:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ13indirectModelR5MixerE2cm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ13indirectModelR5MixerE2cm, %edi
	callq	__cxa_guard_release
.LBB51_4:
	movl	bpos(%rip), %ecx
	testl	%ecx, %ecx
	jne	.LBB51_6
# BB#5:
	movl	c4(%rip), %ecx
	movzwl	%cx, %r8d
	movzbl	%cl, %edx
	movl	%r8d, %esi
	shrl	$8, %esi
	movl	_ZZ13indirectModelR5MixerE2t1(,%rsi,4), %edi
	shll	$8, %edi
	orl	%edx, %edi
	movl	%edi, _ZZ13indirectModelR5MixerE2t1(,%rsi,4)
	shrl	$8, %ecx
	movzwl	%cx, %ecx
	movzwl	_ZZ13indirectModelR5MixerE2t2(%rcx,%rcx), %esi
	shll	$8, %esi
	orl	%edx, %esi
	movw	%si, _ZZ13indirectModelR5MixerE2t2(%rcx,%rcx)
	movl	_ZZ13indirectModelR5MixerE2t1(,%rdx,4), %esi
	shll	$8, %esi
	orl	%esi, %edx
	movzwl	%dx, %ecx
	movslq	_ZZ13indirectModelR5MixerE2cm+136(%rip), %rdi
	leal	1(%rdi), %ebx
	movl	%ebx, _ZZ13indirectModelR5MixerE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edi, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ebx  # imm = 0x75BCD17
	addl	%edi, %ebx
	movq	_ZZ13indirectModelR5MixerE2cm+96(%rip), %rcx
	movl	%ebx, (%rcx,%rdi,4)
	imull	$987654323, %edx, %edi  # imm = 0x3ADE68B3
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	andl	$16777215, %edx         # imm = 0xFFFFFF
	movslq	_ZZ13indirectModelR5MixerE2cm+136(%rip), %rbx
	leal	1(%rbx), %eax
	movl	%eax, _ZZ13indirectModelR5MixerE2cm+136(%rip)
	imull	$987654323, %edx, %eax  # imm = 0x3ADE68B3
	addl	%ebx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%ebx, %eax
	movl	%eax, (%rcx,%rbx,4)
	movslq	_ZZ13indirectModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %edx
	movl	%edx, _ZZ13indirectModelR5MixerE2cm+136(%rip)
	addl	%eax, %edi
	roll	$16, %edi
	imull	$123456791, %edi, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movl	%edx, (%rcx,%rax,4)
	movzwl	%si, %eax
	movslq	_ZZ13indirectModelR5MixerE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ13indirectModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%edx, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%edx, %eax
	movl	%eax, (%rcx,%rdx,4)
	movzwl	_ZZ13indirectModelR5MixerE2t2(%r8,%r8), %eax
	shll	$16, %eax
	orl	%r8d, %eax
	imull	$987654323, %eax, %edx  # imm = 0x3ADE68B3
	andl	$16777215, %eax         # imm = 0xFFFFFF
	movslq	_ZZ13indirectModelR5MixerE2cm+136(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ13indirectModelR5MixerE2cm+136(%rip)
	imull	$987654323, %eax, %eax  # imm = 0x3ADE68B3
	addl	%esi, %eax
	roll	$16, %eax
	imull	$123456791, %eax, %eax  # imm = 0x75BCD17
	addl	%esi, %eax
	movl	%eax, (%rcx,%rsi,4)
	movslq	_ZZ13indirectModelR5MixerE2cm+136(%rip), %rax
	leal	1(%rax), %esi
	movl	%esi, _ZZ13indirectModelR5MixerE2cm+136(%rip)
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%eax, %edx
	movl	%edx, (%rcx,%rax,4)
	movl	bpos(%rip), %ecx
.LBB51_6:
	movl	c0(%rip), %edx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ13indirectModelR5MixerE2cm, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN10ContextMap4mix1ER5Mixeriiii # TAILCALL
.LBB51_7:
.Ltmp224:
	movq	%rax, %rbx
	movl	$_ZGVZ13indirectModelR5MixerE2cm, %edi
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end51:
	.size	_Z13indirectModelR5Mixer, .Lfunc_end51-_Z13indirectModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table51:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp222-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp223-.Ltmp222       #   Call between .Ltmp222 and .Ltmp223
	.long	.Ltmp224-.Lfunc_begin14 #     jumps to .Ltmp224
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Lfunc_end51-.Ltmp223   #   Call between .Ltmp223 and .Lfunc_end51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI52_0:
	.long	256                     # 0x100
	.long	0                       # 0x0
	.long	256                     # 0x100
	.long	256                     # 0x100
.LCPI52_1:
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.long	2147483648              # 0x80000000
	.text
	.globl	_Z8dmcModelR5Mixer
	.p2align	4, 0x90
	.type	_Z8dmcModelR5Mixer,@function
_Z8dmcModelR5Mixer:                     # @_Z8dmcModelR5Mixer
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi236:
	.cfi_def_cfa_offset 32
.Lcfi237:
	.cfi_offset %rbx, -24
.Lcfi238:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movb	_ZGVZ8dmcModelR5MixerE1t(%rip), %al
	testb	%al, %al
	jne	.LBB52_11
# BB#1:
	movl	$_ZGVZ8dmcModelR5MixerE1t, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB52_11
# BB#2:
	movb	level(%rip), %cl
	movl	$131072, %eax           # imm = 0x20000
	shll	%cl, %eax
	movl	%eax, _ZZ8dmcModelR5MixerE1t+4(%rip)
	movl	%eax, _ZZ8dmcModelR5MixerE1t(%rip)
	testl	%eax, %eax
	jle	.LBB52_3
# BB#4:
	shll	$2, %eax
	leal	(%rax,%rax,2), %eax
	movl	programChecker(%rip), %ecx
	addl	%eax, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB52_6
# BB#5:
	movl	%ecx, programChecker+4(%rip)
.LBB52_6:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8dmcModelR5MixerE1t+8(%rip)
	testq	%rax, %rax
	je	.LBB52_7
# BB#9:
	movq	%rax, _ZZ8dmcModelR5MixerE1t+16(%rip)
	jmp	.LBB52_10
.LBB52_3:
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZZ8dmcModelR5MixerE1t+8(%rip)
.LBB52_10:                              # %_ZN5ArrayI7DMCNodeLi0EEC2Ei.exit
	movl	$_ZN5ArrayI7DMCNodeLi0EED2Ev, %edi
	movl	$_ZZ8dmcModelR5MixerE1t, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8dmcModelR5MixerE1t, %edi
	callq	__cxa_guard_release
.LBB52_11:
	movb	_ZGVZ8dmcModelR5MixerE2sm(%rip), %al
	testb	%al, %al
	jne	.LBB52_19
# BB#12:
	movl	$_ZGVZ8dmcModelR5MixerE2sm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB52_19
# BB#13:
	movaps	.LCPI52_0(%rip), %xmm0  # xmm0 = [256,0,256,256]
	movaps	%xmm0, _ZZ8dmcModelR5MixerE2sm(%rip)
	movl	$1024, %eax             # imm = 0x400
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB52_15
# BB#14:
	movl	%eax, programChecker+4(%rip)
.LBB52_15:                              # %_ZN14ProgramChecker5allocEi.exit.i.i.i
	movl	$1024, %edi             # imm = 0x400
	movl	$1, %esi
	callq	calloc
	movq	%rax, _ZZ8dmcModelR5MixerE2sm+16(%rip)
	testq	%rax, %rax
	je	.LBB52_16
# BB#18:                                # %vector.body
	movq	%rax, _ZZ8dmcModelR5MixerE2sm+24(%rip)
	movaps	.LCPI52_1(%rip), %xmm0  # xmm0 = [2147483648,2147483648,2147483648,2147483648]
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	movups	%xmm0, 672(%rax)
	movups	%xmm0, 688(%rax)
	movups	%xmm0, 704(%rax)
	movups	%xmm0, 720(%rax)
	movups	%xmm0, 736(%rax)
	movups	%xmm0, 752(%rax)
	movups	%xmm0, 768(%rax)
	movups	%xmm0, 784(%rax)
	movups	%xmm0, 800(%rax)
	movups	%xmm0, 816(%rax)
	movups	%xmm0, 832(%rax)
	movups	%xmm0, 848(%rax)
	movups	%xmm0, 864(%rax)
	movups	%xmm0, 880(%rax)
	movups	%xmm0, 896(%rax)
	movups	%xmm0, 912(%rax)
	movups	%xmm0, 928(%rax)
	movups	%xmm0, 944(%rax)
	movups	%xmm0, 960(%rax)
	movups	%xmm0, 976(%rax)
	movups	%xmm0, 992(%rax)
	movups	%xmm0, 1008(%rax)
	movl	$_ZN8StateMapD2Ev, %edi
	movl	$_ZZ8dmcModelR5MixerE2sm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ8dmcModelR5MixerE2sm, %edi
	callq	__cxa_guard_release
.LBB52_19:
	movslq	_ZZ8dmcModelR5MixerE3top(%rip), %r11
	testq	%r11, %r11
	jle	.LBB52_27
# BB#20:
	cmpl	_ZZ8dmcModelR5MixerE1t(%rip), %r11d
	jge	.LBB52_27
# BB#21:
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rdi
	movslq	_ZZ8dmcModelR5MixerE4curr(%rip), %rax
	movslq	y(%rip), %rcx
	leaq	(%rax,%rax,2), %r8
	movzwl	9(%rdi,%r8,4), %edx
	movzbl	11(%rdi,%r8,4), %eax
	shll	$16, %eax
	orl	%edx, %eax
	movl	%eax, %edx
	andl	$4095, %edx             # imm = 0xFFF
	shrl	$12, %eax
	testq	%rcx, %rcx
	cmovel	%edx, %eax
	movl	_ZZ8dmcModelR5MixerE9threshold(%rip), %edx
	leal	(%rdx,%rdx), %ebx
	cmpl	%ebx, %eax
	jl	.LBB52_27
# BB#22:
	leaq	(%rdi,%r8,4), %rbx
	movslq	(%rbx,%rcx,4), %r9
	leaq	(%r9,%r9,2), %rcx
	movzwl	9(%rdi,%rcx,4), %r10d
	movzbl	11(%rdi,%rcx,4), %ebx
	shll	$16, %ebx
	orl	%r10d, %ebx
	movl	%ebx, %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	shrl	$12, %ebx
	addl	%ecx, %ebx
	andl	$16777215, %ebx         # imm = 0xFFFFFF
	movl	%ebx, %esi
	subl	%eax, %esi
	leal	(%rdx,%rdx,2), %edx
	cmpl	%edx, %esi
	jl	.LBB52_27
# BB#23:
	shll	$12, %eax
	andl	$16773120, %eax         # imm = 0xFFF000
	xorl	%edx, %edx
	divl	%ebx
	andl	$16777215, %ecx         # imm = 0xFFFFFF
	imull	%eax, %ecx
	shrl	$12, %ecx
	movq	%r11, %rdx
	shlq	$2, %rdx
	leaq	(%rdx,%rdx,2), %r10
	movzwl	9(%rdi,%r10), %esi
	movzbl	11(%rdi,%r10), %ebx
	shll	$16, %ebx
	orl	%esi, %ebx
	movl	%ecx, %esi
	andl	$4095, %esi             # imm = 0xFFF
	andl	$16773120, %ebx         # imm = 0xFFF000
	orl	%ebx, %esi
	shrl	$16, %ebx
	movb	%bl, 11(%rdi,%r10)
	movw	%si, 9(%rdi,%r10)
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rsi
	shlq	$2, %r9
	leaq	(%r9,%r9,2), %rdi
	movzwl	9(%rsi,%rdi), %r9d
	movzbl	11(%rsi,%rdi), %ebx
	shll	$16, %ebx
	orl	%r9d, %ebx
	movl	%ebx, %edx
	subl	%ecx, %edx
	andl	$4095, %edx             # imm = 0xFFF
	andl	$16773120, %ebx         # imm = 0xFFF000
	orl	%ebx, %edx
	shrl	$16, %ebx
	movb	%bl, 11(%rsi,%rdi)
	movw	%dx, 9(%rsi,%rdi)
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rcx
	movzwl	9(%rcx,%rdi), %edx
	movzbl	11(%rcx,%rdi), %esi
	shll	$16, %esi
	orl	%edx, %esi
	shrl	$12, %esi
	imull	%eax, %esi
	movl	%esi, %eax
	shrl	$12, %eax
	movzwl	9(%rcx,%r10), %edx
	andl	$16773120, %esi         # imm = 0xFFF000
	andl	$4095, %edx             # imm = 0xFFF
	orl	%esi, %edx
	shrl	$16, %esi
	movb	%sil, 11(%rcx,%r10)
	movw	%dx, 9(%rcx,%r10)
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rcx
	movzwl	9(%rcx,%rdi), %edx
	movzbl	11(%rcx,%rdi), %esi
	shll	$16, %esi
	orl	%edx, %esi
	movl	%esi, %edx
	shrl	$12, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	shll	$12, %eax
	andl	$4095, %esi             # imm = 0xFFF
	orl	%eax, %esi
	shrl	$4, %edx
	movb	%dl, 11(%rcx,%rdi)
	movw	%si, 9(%rcx,%rdi)
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rax
	movl	(%rax,%rdi), %ecx
	movl	%ecx, (%rax,%r10)
	movl	4(%rax,%rdi), %ecx
	movl	%ecx, 4(%rax,%r10)
	movb	8(%rax,%rdi), %cl
	movb	%cl, 8(%rax,%r10)
	movslq	y(%rip), %rcx
	leaq	(%rax,%r8,4), %rax
	movl	%r11d, (%rax,%rcx,4)
	leal	1(%r11), %r11d
	movl	%r11d, _ZZ8dmcModelR5MixerE3top(%rip)
	movl	level(%rip), %ecx
	movl	$131072, %eax           # imm = 0x20000
	shll	%cl, %eax
	cmpl	%eax, %r11d
	jne	.LBB52_25
# BB#24:
	movl	$512, _ZZ8dmcModelR5MixerE9threshold(%rip) # imm = 0x200
.LBB52_25:
	movl	$65536, %eax            # imm = 0x10000
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	leal	(%rax,%rax,2), %eax
	cmpl	%eax, %r11d
	jne	.LBB52_27
# BB#26:
	movl	$768, _ZZ8dmcModelR5MixerE9threshold(%rip) # imm = 0x300
.LBB52_27:
	cmpl	_ZZ8dmcModelR5MixerE1t(%rip), %r11d
	jne	.LBB52_36
# BB#28:
	cmpl	$1, bpos(%rip)
	jne	.LBB52_36
# BB#29:                                # %.thread
	movl	$0, _ZZ8dmcModelR5MixerE3top(%rip)
	jmp	.LBB52_30
.LBB52_36:
	testl	%r11d, %r11d
	je	.LBB52_30
# BB#37:                                # %._crit_edge
	movslq	_ZZ8dmcModelR5MixerE4curr(%rip), %rcx
	jmp	.LBB52_38
.LBB52_30:                              # %.preheader.preheader
	movl	$1, %r8d
	xorl	%r9d, %r9d
	movl	$9, %r11d
	.p2align	4, 0x90
.LBB52_31:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB52_42 Depth 2
                                        #     Child Loop BB52_33 Depth 2
	cmpq	$126, %r9
	jg	.LBB52_41
# BB#32:                                # %.preheader.split.us.preheader
                                        #   in Loop: Header=BB52_31 Depth=1
	movl	%r8d, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB52_33:                              # %.preheader.split.us
                                        #   Parent Loop BB52_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rdx
	addq	%r11, %rdx
	movl	%esi, -9(%rdi,%rdx)
	leal	1(%rsi), %ebx
	movl	%ebx, -5(%rdi,%rdx)
	movzwl	(%rdi,%rdx), %ebx
	movzbl	2(%rdi,%rdx), %ecx
	shll	$16, %ecx
	orl	%ebx, %ecx
	andl	$16773120, %ecx         # imm = 0xFFF000
	leal	128(%rcx), %ebx
	shrl	$16, %ecx
	movb	%cl, 2(%rdi,%rdx)
	movw	%bx, (%rdi,%rdx)
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rcx
	addq	%r11, %rcx
	movzwl	(%rdi,%rcx), %edx
	andl	$4095, %edx             # imm = 0xFFF
	movw	%dx, (%rdi,%rcx)
	movb	$8, 2(%rdi,%rcx)
	addq	$3072, %rdi             # imm = 0xC00
	addl	$256, %esi              # imm = 0x100
	cmpq	$786432, %rdi           # imm = 0xC0000
	jne	.LBB52_33
	jmp	.LBB52_34
	.p2align	4, 0x90
.LBB52_41:                              # %.preheader.split.preheader
                                        #   in Loop: Header=BB52_31 Depth=1
	movq	%r9, %rsi
	shlq	$8, %rsi
	leal	256(%rsi), %r10d
	addl	$-32512, %esi           # imm = 0x8100
	movl	$256, %ebx              # imm = 0x100
	movq	%r11, %rdx
	.p2align	4, 0x90
.LBB52_42:                              # %.preheader.split
                                        #   Parent Loop BB52_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rcx
	movl	%esi, -9(%rcx,%rdx)
	movl	%r10d, -5(%rcx,%rdx)
	movzwl	(%rcx,%rdx), %edi
	movzbl	2(%rcx,%rdx), %eax
	shll	$16, %eax
	orl	%edi, %eax
	andl	$16773120, %eax         # imm = 0xFFF000
	leal	128(%rax), %edi
	shrl	$16, %eax
	movb	%al, 2(%rcx,%rdx)
	movw	%di, (%rcx,%rdx)
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rax
	movzwl	(%rax,%rdx), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	movw	%cx, (%rax,%rdx)
	movb	$8, 2(%rax,%rdx)
	addq	$3072, %rdx             # imm = 0xC00
	decq	%rbx
	jne	.LBB52_42
.LBB52_34:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB52_31 Depth=1
	incq	%r9
	addq	$12, %r11
	addl	$2, %r8d
	cmpq	$256, %r9               # imm = 0x100
	jne	.LBB52_31
# BB#35:
	movl	$65536, _ZZ8dmcModelR5MixerE3top(%rip) # imm = 0x10000
	movl	$0, _ZZ8dmcModelR5MixerE4curr(%rip)
	movl	$256, _ZZ8dmcModelR5MixerE9threshold(%rip) # imm = 0x100
	xorl	%ecx, %ecx
.LBB52_38:
	movslq	y(%rip), %rax
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rsi
	leaq	(%rcx,%rcx,2), %rcx
	movzwl	9(%rsi,%rcx,4), %edi
	movzbl	11(%rsi,%rcx,4), %edx
	shll	$16, %edx
	orl	%edi, %edx
	testq	%rax, %rax
	leaq	9(%rsi,%rcx,4), %rsi
	movl	%edx, %edi
	je	.LBB52_43
# BB#39:
	andl	$16777215, %edi         # imm = 0xFFFFFF
	cmpl	$15564799, %edi         # imm = 0xED7FFF
	ja	.LBB52_45
# BB#40:
	addl	$1048576, %edx          # imm = 0x100000
	movw	%dx, (%rsi)
	shrl	$16, %edx
	movb	%dl, 2(%rsi)
	jmp	.LBB52_45
.LBB52_43:
	andl	$4088, %edi             # imm = 0xFF8
	cmpl	$3799, %edi             # imm = 0xED7
	ja	.LBB52_45
# BB#44:
	leal	256(%rdx), %edi
	andl	$4095, %edi             # imm = 0xFFF
	andl	$16773120, %edx         # imm = 0xFFF000
	orl	%edx, %edi
	shrl	$16, %edx
	movb	%dl, 2(%rsi)
	movw	%di, (%rsi)
.LBB52_45:
	movq	_ZZ8dmcModelR5MixerE1t+16(%rip), %rdx
	leaq	(%rdx,%rcx,4), %rsi
	movzbl	8(%rdx,%rcx,4), %edi
	movb	_ZL11State_table(%rax,%rdi,4), %bl
	movb	%bl, 8(%rdx,%rcx,4)
	movslq	(%rsi,%rax,4), %rcx
	movl	%ecx, _ZZ8dmcModelR5MixerE4curr(%rip)
	leaq	(%rcx,%rcx,2), %r11
	movzbl	8(%rdx,%r11,4), %r8d
	movq	_ZZ8dmcModelR5MixerE2sm+24(%rip), %rdi
	movslq	_ZZ8dmcModelR5MixerE2sm+4(%rip), %r9
	movl	(%rdi,%r9,4), %esi
	movl	%esi, %ebx
	shrl	$10, %ebx
	leal	1(%rsi), %r10d
	movl	%esi, %ecx
	orl	$1023, %ecx             # imm = 0x3FF
	andl	$1023, %esi             # imm = 0x3FF
	cmpl	$1023, %esi             # imm = 0x3FF
	cmovnel	%r10d, %ecx
	shll	$22, %eax
	subl	%ebx, %eax
	sarl	$3, %eax
	imull	_ZL2dt(,%rsi,4), %eax
	andl	$-1024, %eax            # imm = 0xFC00
	addl	%ecx, %eax
	movl	%eax, (%rdi,%r9,4)
	movl	%r8d, _ZZ8dmcModelR5MixerE2sm+4(%rip)
	movl	(%rdi,%r8,4), %ecx
	shrl	$20, %ecx
	movzwl	9(%rdx,%r11,4), %eax
	movzbl	11(%rdx,%r11,4), %edx
	shll	$16, %edx
	orl	%eax, %edx
	movl	%edx, %eax
	shrl	$12, %eax
	andl	$4095, %edx             # imm = 0xFFF
	leal	10(%rax,%rdx), %esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	shll	$12, %eax
	addl	$20480, %eax            # imm = 0x5000
	xorl	%edx, %edx
	divl	%esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	stretch+16(%rip), %rdx
	movzwl	(%rdx,%rcx,2), %ecx
	movslq	96(%r14), %rsi
	movq	32(%r14), %rdi
	movw	%cx, (%rdi,%rsi,2)
	movzwl	(%rdx,%rax,2), %eax
	leal	2(%rsi), %ecx
	movl	%ecx, 96(%r14)
	movw	%ax, 2(%rdi,%rsi,2)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB52_16:
.Ltmp228:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp229:
# BB#17:                                # %.noexc79
.LBB52_7:
.Ltmp225:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp226:
# BB#8:                                 # %.noexc
.LBB52_46:
.Ltmp227:
	movq	%rax, %rbx
	movl	$_ZGVZ8dmcModelR5MixerE1t, %edi
	jmp	.LBB52_47
.LBB52_48:
.Ltmp230:
	movq	%rax, %rbx
	movl	$_ZGVZ8dmcModelR5MixerE2sm, %edi
.LBB52_47:
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end52:
	.size	_Z8dmcModelR5Mixer, .Lfunc_end52-_Z8dmcModelR5Mixer
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp228-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin15 #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp225-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp226-.Ltmp225       #   Call between .Ltmp225 and .Ltmp226
	.long	.Ltmp227-.Lfunc_begin15 #     jumps to .Ltmp227
	.byte	0                       #   On action: cleanup
	.long	.Ltmp226-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Lfunc_end52-.Ltmp226   #   Call between .Ltmp226 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN5ArrayI7DMCNodeLi0EED2Ev,"axG",@progbits,_ZN5ArrayI7DMCNodeLi0EED2Ev,comdat
	.weak	_ZN5ArrayI7DMCNodeLi0EED2Ev
	.p2align	4, 0x90
	.type	_ZN5ArrayI7DMCNodeLi0EED2Ev,@function
_ZN5ArrayI7DMCNodeLi0EED2Ev:            # @_ZN5ArrayI7DMCNodeLi0EED2Ev
	.cfi_startproc
# BB#0:
	imull	$-12, (%rdi), %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB53_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB53_2:                               # %_ZN14ProgramChecker5allocEi.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end53:
	.size	_ZN5ArrayI7DMCNodeLi0EED2Ev, .Lfunc_end53-_ZN5ArrayI7DMCNodeLi0EED2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI54_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI54_1:
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
	.long	4294934528              # 0xffff8000
.LCPI54_2:
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
	.long	32767                   # 0x7fff
.LCPI54_3:
	.long	257                     # 0x101
	.long	257                     # 0x101
	.long	257                     # 0x101
	.long	257                     # 0x101
	.text
	.globl	_Z13contextModel2v
	.p2align	4, 0x90
	.type	_Z13contextModel2v,@function
_Z13contextModel2v:                     # @_Z13contextModel2v
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%rbp
.Lcfi239:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi240:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi241:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi242:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi243:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi245:
	.cfi_def_cfa_offset 96
.Lcfi246:
	.cfi_offset %rbx, -56
.Lcfi247:
	.cfi_offset %r12, -48
.Lcfi248:
	.cfi_offset %r13, -40
.Lcfi249:
	.cfi_offset %r14, -32
.Lcfi250:
	.cfi_offset %r15, -24
.Lcfi251:
	.cfi_offset %rbp, -16
	movb	_ZGVZ13contextModel2vE2cm(%rip), %al
	testb	%al, %al
	jne	.LBB54_4
# BB#1:
	movl	$_ZGVZ13contextModel2vE2cm, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB54_4
# BB#2:
	movb	level(%rip), %cl
	movl	$2097152, %esi          # imm = 0x200000
	shll	%cl, %esi
.Ltmp231:
	movl	$_ZZ13contextModel2vE2cm, %edi
	movl	$9, %edx
	callq	_ZN10ContextMapC2Eii
.Ltmp232:
# BB#3:
	movl	$_ZN10ContextMapD2Ev, %edi
	movl	$_ZZ13contextModel2vE2cm, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ13contextModel2vE2cm, %edi
	callq	__cxa_guard_release
.LBB54_4:
	movb	_ZGVZ13contextModel2vE4rcm7(%rip), %al
	testb	%al, %al
	jne	.LBB54_8
# BB#5:
	movl	$_ZGVZ13contextModel2vE4rcm7, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB54_8
# BB#6:
	movb	level(%rip), %cl
	movl	$65536, %esi            # imm = 0x10000
	shll	%cl, %esi
.Ltmp234:
	movl	$_ZZ13contextModel2vE4rcm7, %edi
	callq	_ZN13RunContextMapC2Ei
.Ltmp235:
# BB#7:
	movl	$_ZN13RunContextMapD2Ev, %edi
	movl	$_ZZ13contextModel2vE4rcm7, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ13contextModel2vE4rcm7, %edi
	callq	__cxa_guard_release
.LBB54_8:
	movb	_ZGVZ13contextModel2vE4rcm9(%rip), %al
	testb	%al, %al
	jne	.LBB54_12
# BB#9:
	movl	$_ZGVZ13contextModel2vE4rcm9, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB54_12
# BB#10:
	movb	level(%rip), %cl
	movl	$65536, %esi            # imm = 0x10000
	shll	%cl, %esi
.Ltmp237:
	movl	$_ZZ13contextModel2vE4rcm9, %edi
	callq	_ZN13RunContextMapC2Ei
.Ltmp238:
# BB#11:
	movl	$_ZN13RunContextMapD2Ev, %edi
	movl	$_ZZ13contextModel2vE4rcm9, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ13contextModel2vE4rcm9, %edi
	callq	__cxa_guard_release
.LBB54_12:
	movb	_ZGVZ13contextModel2vE5rcm10(%rip), %al
	testb	%al, %al
	jne	.LBB54_16
# BB#13:
	movl	$_ZGVZ13contextModel2vE5rcm10, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB54_16
# BB#14:
	movb	level(%rip), %cl
	movl	$65536, %esi            # imm = 0x10000
	shll	%cl, %esi
.Ltmp240:
	movl	$_ZZ13contextModel2vE5rcm10, %edi
	callq	_ZN13RunContextMapC2Ei
.Ltmp241:
# BB#15:
	movl	$_ZN13RunContextMapD2Ev, %edi
	movl	$_ZZ13contextModel2vE5rcm10, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ13contextModel2vE5rcm10, %edi
	callq	__cxa_guard_release
.LBB54_16:
	movb	_ZGVZ13contextModel2vE1m(%rip), %al
	testb	%al, %al
	jne	.LBB54_20
# BB#17:
	movl	$_ZGVZ13contextModel2vE1m, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB54_20
# BB#18:
.Ltmp243:
	movl	$_ZZ13contextModel2vE1m, %edi
	movl	$800, %esi              # imm = 0x320
	movl	$3088, %edx             # imm = 0xC10
	movl	$7, %ecx
	movl	$128, %r8d
	callq	_ZN5MixerC2Eiiii
.Ltmp244:
# BB#19:
	movl	$_ZN5MixerD2Ev, %edi
	movl	$_ZZ13contextModel2vE1m, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ13contextModel2vE1m, %edi
	callq	__cxa_guard_release
.LBB54_20:
	cmpl	$0, bpos(%rip)
	jne	.LBB54_26
# BB#21:
	movl	_ZZ13contextModel2vE4size(%rip), %ecx
	leal	-1(%rcx), %eax
	movl	%eax, _ZZ13contextModel2vE4size(%rip)
	testl	%ecx, %ecx
	jne	.LBB54_23
# BB#22:
	movl	pos(%rip), %ecx
	decl	%ecx
	movl	buf(%rip), %edx
	decl	%edx
	andl	%ecx, %edx
	movq	buf+16(%rip), %rcx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %ecx
	movl	%ecx, _ZZ13contextModel2vE8filetype(%rip)
.LBB54_23:
	cmpl	$-5, %eax
	jne	.LBB54_26
# BB#24:
	movl	pos(%rip), %eax
	leal	-4(%rax), %edx
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%ecx, %edx
	movq	buf+16(%rip), %rsi
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %edx
	shll	$24, %edx
	leal	-3(%rax), %edi
	andl	%ecx, %edi
	movslq	%edi, %rdi
	movzbl	(%rsi,%rdi), %edi
	shll	$16, %edi
	orl	%edx, %edi
	leal	-2(%rax), %edx
	andl	%ecx, %edx
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %edx
	shll	$8, %edx
	orl	%edi, %edx
	decl	%eax
	andl	%ecx, %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	orl	%edx, %eax
	movl	%eax, _ZZ13contextModel2vE4size(%rip)
	cmpl	$8, _ZZ13contextModel2vE8filetype(%rip)
	jne	.LBB54_26
# BB#25:
	addl	$8, %eax
	movl	%eax, _ZZ13contextModel2vE4size(%rip)
.LBB54_26:
	movslq	_ZZ13contextModel2vE1m+88(%rip), %r10
	testq	%r10, %r10
	jle	.LBB54_42
# BB#27:                                # %.lr.ph.i
	movl	_ZZ13contextModel2vE1m+96(%rip), %r8d
	addl	$7, %r8d
	andl	$-8, %r8d
	testl	%r8d, %r8d
	jle	.LBB54_42
# BB#28:                                # %.lr.ph.i.split.us.preheader
	movl	y(%rip), %r13d
	shll	$12, %r13d
	movq	_ZZ13contextModel2vE1m+120(%rip), %r12
	movq	_ZZ13contextModel2vE1m+32(%rip), %r9
	movq	_ZZ13contextModel2vE1m+80(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	_ZZ13contextModel2vE1m(%rip), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	_ZZ13contextModel2vE1m+56(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	(%rax,%r8,2), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	(%r9,%r8,2), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebx, %ebx
	movl	$-32768, %r11d          # imm = 0x8000
	movw	$32767, %r14w           # imm = 0x7FFF
	movdqa	.LCPI54_0(%rip), %xmm8  # xmm8 = [1,1,1,1]
	movdqa	.LCPI54_1(%rip), %xmm9  # xmm9 = [4294934528,4294934528,4294934528,4294934528]
	movdqa	.LCPI54_2(%rip), %xmm2  # xmm2 = [32767,32767,32767,32767]
	.p2align	4, 0x90
.LBB54_29:                              # %.lr.ph.i.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB54_40 Depth 2
                                        #     Child Loop BB54_32 Depth 2
	movl	%r13d, %eax
	subl	(%r12,%rbx,4), %eax
	je	.LBB54_41
# BB#30:                                # %.lr.ph.preheader.i.i.us
                                        #   in Loop: Header=BB54_29 Depth=1
	leal	(,%rax,8), %ecx
	subl	%eax, %ecx
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%rbx,4), %rax
	imulq	24(%rsp), %rax          # 8-byte Folded Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,2), %rsi
	cmpl	$7, %r8d
	jbe	.LBB54_31
# BB#37:                                # %vector.memcheck
                                        #   in Loop: Header=BB54_29 Depth=1
	cmpq	8(%rsp), %rsi           # 8-byte Folded Reload
	jae	.LBB54_39
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB54_29 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	leaq	(%rdx,%rax,2), %rax
	cmpq	%rax, %r9
	jae	.LBB54_39
.LBB54_31:                              #   in Loop: Header=BB54_29 Depth=1
	movq	%r9, %rax
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB54_32:                              # %.lr.ph.i.i.us
                                        #   Parent Loop BB54_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rsi), %ebp
	movswl	(%rax), %edi
	imull	%ecx, %edi
	sarl	$15, %edi
	incl	%edi
	sarl	%edi
	addl	%ebp, %edi
	cmpl	$-32769, %edi           # imm = 0xFFFF7FFF
	cmovlel	%r11d, %edi
	cmpl	$32767, %edi            # imm = 0x7FFF
	cmovgew	%r14w, %di
	movw	%di, (%rsi)
	addq	$2, %rax
	addq	$2, %rsi
	decq	%rdx
	jne	.LBB54_32
	jmp	.LBB54_41
.LBB54_39:                              # %vector.ph
                                        #   in Loop: Header=BB54_29 Depth=1
	movd	%ecx, %xmm3
	pshufd	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0]
	movq	%r8, %rcx
	movq	%r9, %r15
	.p2align	4, 0x90
.LBB54_40:                              # %vector.body
                                        #   Parent Loop BB54_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rsi), %xmm4          # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	movq	(%rsi), %xmm5           # xmm5 = mem[0],zero
	punpcklwd	%xmm5, %xmm5    # xmm5 = xmm5[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm5
	movq	8(%r15), %xmm6          # xmm6 = mem[0],zero
	punpcklwd	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0],xmm0[1],xmm6[1],xmm0[2],xmm6[2],xmm0[3],xmm6[3]
	psrad	$16, %xmm0
	movq	(%r15), %xmm6           # xmm6 = mem[0],zero
	punpcklwd	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1],xmm7[2],xmm6[2],xmm7[3],xmm6[3]
	psrad	$16, %xmm7
	movdqa	%xmm3, %xmm6
	pmuludq	%xmm7, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm7, %xmm7      # xmm7 = xmm7[1,1,3,3]
	pshufd	$245, %xmm3, %xmm1      # xmm1 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	movdqa	%xmm3, %xmm7
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	psrad	$15, %xmm7
	psrad	$15, %xmm6
	paddd	%xmm8, %xmm6
	paddd	%xmm8, %xmm7
	psrad	$1, %xmm7
	psrad	$1, %xmm6
	paddd	%xmm5, %xmm6
	paddd	%xmm4, %xmm7
	movdqa	%xmm7, %xmm0
	pcmpgtd	%xmm9, %xmm0
	movdqa	%xmm6, %xmm1
	pcmpgtd	%xmm9, %xmm1
	pand	%xmm1, %xmm6
	pandn	%xmm9, %xmm1
	por	%xmm6, %xmm1
	pand	%xmm0, %xmm7
	pandn	%xmm9, %xmm0
	por	%xmm7, %xmm0
	movdqa	%xmm2, %xmm4
	pcmpgtd	%xmm0, %xmm4
	movdqa	%xmm2, %xmm5
	pcmpgtd	%xmm1, %xmm5
	pand	%xmm5, %xmm1
	pandn	%xmm2, %xmm5
	por	%xmm1, %xmm5
	pand	%xmm4, %xmm0
	pandn	%xmm2, %xmm4
	por	%xmm0, %xmm4
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	packssdw	%xmm4, %xmm5
	movdqu	%xmm5, (%rsi)
	addq	$16, %rsi
	addq	$16, %r15
	addq	$-8, %rcx
	jne	.LBB54_40
	.p2align	4, 0x90
.LBB54_41:                              # %_Z5trainPsS_ii.exit.i.us
                                        #   in Loop: Header=BB54_29 Depth=1
	incq	%rbx
	cmpq	%r10, %rbx
	jne	.LBB54_29
.LBB54_42:                              # %_ZN5Mixer6updateEv.exit
	movq	$0, _ZZ13contextModel2vE1m+88(%rip)
	movl	$1, _ZZ13contextModel2vE1m+96(%rip)
	movq	_ZZ13contextModel2vE1m+32(%rip), %rax
	movw	$256, (%rax)            # imm = 0x100
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z10matchModelR5Mixer
	movq	ilog+16(%rip), %rcx
	movzwl	%ax, %eax
	movzbl	(%rcx,%rax), %r14d
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z8wavModelR5Mixer
	movl	%eax, %ebp
	movl	_ZZ13contextModel2vE8filetype(%rip), %eax
	cmpl	$1, %eax
	jne	.LBB54_45
# BB#43:
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z9jpegModelR5Mixer
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	testl	%eax, %eax
	je	.LBB54_44
# BB#60:
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	incl	%edx
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, _ZZ13contextModel2vE1m+88(%rip)
	movq	_ZZ13contextModel2vE1m+80(%rip), %rcx
	movl	%edx, (%rcx,%rsi,4)
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	leal	8(%rdx), %esi
	movl	%esi, _ZZ13contextModel2vE1m+92(%rip)
	leal	7(%rax,%rdx), %eax
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ13contextModel2vE1m+88(%rip)
	movl	%eax, (%rcx,%rdx,4)
	movl	_ZZ13contextModel2vE1m+92(%rip), %eax
	leal	257(%rax), %edx
	movl	%edx, _ZZ13contextModel2vE1m+92(%rip)
	movl	pos(%rip), %edx
	decl	%edx
	movl	buf(%rip), %esi
	decl	%esi
	andl	%edx, %esi
	movq	buf+16(%rip), %rdx
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %edx
	leal	257(%rdx,%rax), %eax
	jmp	.LBB54_61
.LBB54_44:                              # %..thread_crit_edge
	movl	_ZZ13contextModel2vE8filetype(%rip), %eax
.LBB54_45:                              # %.thread
	movl	%eax, %ecx
	orl	$1, %ecx
	cmpl	$5, %ecx
	jne	.LBB54_48
# BB#46:
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z8bmpModelR5Mixer
	testl	%eax, %eax
	jle	.LBB54_47
# BB#62:
	movl	_ZZ13contextModel2vE3col(%rip), %ecx
	leal	1(%rcx), %edx
	xorl	%esi, %esi
	cmpl	$22, %ecx
	cmovlel	%edx, %esi
	movl	%esi, _ZZ13contextModel2vE3col(%rip)
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	addl	$2, %edx
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rdi
	leal	1(%rdi), %ecx
	movl	%ecx, _ZZ13contextModel2vE1m+88(%rip)
	movq	_ZZ13contextModel2vE1m+80(%rip), %rcx
	movl	%edx, (%rcx,%rdi,4)
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	leal	8(%rdx), %edi
	movl	%edi, _ZZ13contextModel2vE1m+92(%rip)
	leal	8(%rsi,%rdx), %edx
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ13contextModel2vE1m+88(%rip)
	movl	%edx, (%rcx,%rsi,4)
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	leal	24(%rdx), %esi
	movl	%esi, _ZZ13contextModel2vE1m+92(%rip)
	movl	pos(%rip), %esi
	movl	%esi, %edi
	subl	%eax, %edi
	movl	buf(%rip), %eax
	decl	%eax
	andl	%eax, %edi
	movq	buf+16(%rip), %rbp
	movslq	%edi, %rdi
	movzbl	(%rbp,%rdi), %edi
	addl	$-3, %esi
	andl	%eax, %esi
	movslq	%esi, %rax
	movzbl	(%rbp,%rax), %eax
	addl	%edi, %eax
	shrl	$4, %eax
	leal	24(%rax,%rdx), %eax
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ13contextModel2vE1m+88(%rip)
	movl	%eax, (%rcx,%rdx,4)
	movl	_ZZ13contextModel2vE1m+92(%rip), %eax
	addl	$32, %eax
	movl	%eax, _ZZ13contextModel2vE1m+92(%rip)
	addl	c0(%rip), %eax
.LBB54_61:
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ13contextModel2vE1m+88(%rip)
	movl	%eax, (%rcx,%rdx,4)
	addl	$256, _ZZ13contextModel2vE1m+92(%rip) # imm = 0x100
	jmp	.LBB54_96
.LBB54_47:                              # %.thread92
	movl	_ZZ13contextModel2vE8filetype(%rip), %eax
.LBB54_48:
	cmpl	$6, %eax
	jne	.LBB54_51
# BB#49:
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z8pgmModelR5Mixer
	testl	%eax, %eax
	jg	.LBB54_96
# BB#50:                                # %._crit_edge
	movl	_ZZ13contextModel2vE8filetype(%rip), %eax
.LBB54_51:
	cmpl	$3, %eax
	jne	.LBB54_54
# BB#52:
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z9bmpModel8R5Mixer
	testl	%eax, %eax
	jg	.LBB54_96
# BB#53:                                # %thread-pre-split
	movl	_ZZ13contextModel2vE8filetype(%rip), %eax
.LBB54_54:
	cmpl	$7, %eax
	jne	.LBB54_56
# BB#55:
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z9rgbModel8R5Mixer
	testl	%eax, %eax
	jg	.LBB54_96
.LBB54_56:
	testl	%ebp, %ebp
	jle	.LBB54_63
# BB#57:
	movzbl	%bpl, %eax
	shrl	$8, %ebp
	imull	%eax, %ebp
	movl	_ZZ13contextModel2vE3col_0(%rip), %edx
	incl	%edx
	xorl	%ecx, %ecx
	cmpl	%ebp, %edx
	cmovll	%edx, %ecx
	movl	%ecx, _ZZ13contextModel2vE3col_0(%rip)
	cmpl	%eax, %ebp
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	je	.LBB54_59
# BB#58:
	addl	%ecx, %edx
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ13contextModel2vE1m+88(%rip)
	movq	_ZZ13contextModel2vE1m+80(%rip), %rdi
	movl	%edx, (%rdi,%rsi,4)
	addl	_ZZ13contextModel2vE1m+92(%rip), %ebp
	movl	%ebp, _ZZ13contextModel2vE1m+92(%rip)
	movl	%ebp, %edx
.LBB54_59:                              # %._crit_edge103
	addl	%ecx, %edx
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%esi, _ZZ13contextModel2vE1m+88(%rip)
	movq	_ZZ13contextModel2vE1m+80(%rip), %rsi
	movl	%edx, (%rsi,%rcx,4)
	addl	_ZZ13contextModel2vE1m+92(%rip), %eax
	movl	%eax, _ZZ13contextModel2vE1m+92(%rip)
	addl	c0(%rip), %eax
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ13contextModel2vE1m+88(%rip)
	movl	%eax, (%rsi,%rcx,4)
	addl	$256, _ZZ13contextModel2vE1m+92(%rip) # imm = 0x100
	jmp	.LBB54_96
.LBB54_63:
	movl	bpos(%rip), %ecx
	testl	%ecx, %ecx
	jne	.LBB54_80
# BB#64:                                # %.preheader95
	movzbl	c4(%rip), %eax
	movl	_ZZ13contextModel2vE3cxt+56(%rip), %ecx
	movl	%ecx, %edx
	shll	$8, %edx
	addl	%ecx, %edx
	leal	1(%rax,%rdx), %ecx
	movl	%ecx, _ZZ13contextModel2vE3cxt+60(%rip)
	movl	_ZZ13contextModel2vE3cxt+52(%rip), %ecx
	movl	%ecx, %edx
	shll	$8, %edx
	addl	%ecx, %edx
	leal	1(%rax,%rdx), %ecx
	movl	%ecx, _ZZ13contextModel2vE3cxt+56(%rip)
	movl	_ZZ13contextModel2vE3cxt+48(%rip), %ecx
	movl	%ecx, %edx
	shll	$8, %edx
	addl	%ecx, %edx
	leal	1(%rax,%rdx), %ecx
	movl	%ecx, _ZZ13contextModel2vE3cxt+52(%rip)
	movdqa	_ZZ13contextModel2vE3cxt+32(%rip), %xmm0
	movdqa	.LCPI54_3(%rip), %xmm2  # xmm2 = [257,257,257,257]
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movd	%eax, %xmm1
	pshufd	$0, %xmm1, %xmm3        # xmm3 = xmm1[0,0,0,0]
	paddd	.LCPI54_0(%rip), %xmm3
	paddd	%xmm3, %xmm0
	movdqu	%xmm0, _ZZ13contextModel2vE3cxt+36(%rip)
	movdqa	_ZZ13contextModel2vE3cxt+16(%rip), %xmm0
	pshufd	$245, %xmm0, %xmm1      # xmm1 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	paddd	%xmm3, %xmm0
	movdqu	%xmm0, _ZZ13contextModel2vE3cxt+20(%rip)
	movdqa	_ZZ13contextModel2vE3cxt(%rip), %xmm1
	pshufd	$245, %xmm1, %xmm4      # xmm4 = xmm1[1,1,3,3]
	movd	%xmm1, %ecx
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm2      # xmm2 = xmm4[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	paddd	%xmm3, %xmm1
	movdqu	%xmm1, _ZZ13contextModel2vE3cxt+4(%rip)
	movq	_ZZ13contextModel2vE2cm+96(%rip), %rax
	movslq	_ZZ13contextModel2vE2cm+136(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, _ZZ13contextModel2vE2cm+136(%rip)
	imull	$987654323, %ecx, %ecx  # imm = 0x3ADE68B3
	addl	%edx, %ecx
	roll	$16, %ecx
	imull	$123456791, %ecx, %ecx  # imm = 0x75BCD17
	addl	%edx, %ecx
	movl	%ecx, (%rax,%rdx,4)
	movslq	_ZZ13contextModel2vE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ13contextModel2vE2cm+136(%rip)
	movd	%xmm1, %edx
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rax,%rcx,4)
	movslq	_ZZ13contextModel2vE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ13contextModel2vE2cm+136(%rip)
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	movd	%xmm2, %edx
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rax,%rcx,4)
	movslq	_ZZ13contextModel2vE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ13contextModel2vE2cm+136(%rip)
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	movd	%xmm2, %edx
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rax,%rcx,4)
	movslq	_ZZ13contextModel2vE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ13contextModel2vE2cm+136(%rip)
	pshufd	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3]
	movd	%xmm1, %edx
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rax,%rcx,4)
	movslq	_ZZ13contextModel2vE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ13contextModel2vE2cm+136(%rip)
	movd	%xmm0, %edx
	imull	$987654323, %edx, %edx  # imm = 0x3ADE68B3
	addl	%ecx, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rax,%rcx,4)
	movslq	_ZZ13contextModel2vE2cm+136(%rip), %rcx
	leal	1(%rcx), %edx
	imull	$987654323, _ZZ13contextModel2vE3cxt+24(%rip), %esi # imm = 0x3ADE68B3
	movl	%edx, _ZZ13contextModel2vE2cm+136(%rip)
	addl	%ecx, %esi
	roll	$16, %esi
	imull	$123456791, %esi, %edx  # imm = 0x75BCD17
	addl	%ecx, %edx
	movl	%edx, (%rax,%rcx,4)
	movl	_ZZ13contextModel2vE3cxt+28(%rip), %esi
	movq	_ZZ13contextModel2vE4rcm7+32(%rip), %rax
	movb	(%rax), %cl
	testb	%cl, %cl
	je	.LBB54_66
# BB#65:
	movb	1(%rax), %dl
	movl	pos(%rip), %edi
	decl	%edi
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%edi, %ebp
	movq	buf+16(%rip), %rdi
	movslq	%ebp, %rbp
	cmpb	(%rdi,%rbp), %dl
	jne	.LBB54_66
# BB#67:
	cmpb	$-1, %cl
	je	.LBB54_69
# BB#68:
	incb	%cl
	movb	%cl, (%rax)
	jmp	.LBB54_69
.LBB54_66:
	movb	$1, (%rax)
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%eax, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movb	(%rax,%rcx), %al
	movq	_ZZ13contextModel2vE4rcm7+32(%rip), %rcx
	movb	%al, 1(%rcx)
.LBB54_69:                              # %_ZN13RunContextMap3setEj.exit91
	movl	$_ZZ13contextModel2vE4rcm7, %edi
	callq	_ZN2BHILi4EEixEj
	incq	%rax
	movq	%rax, _ZZ13contextModel2vE4rcm7+32(%rip)
	movslq	_ZZ13contextModel2vE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	imull	$987654323, _ZZ13contextModel2vE3cxt+32(%rip), %edx # imm = 0x3ADE68B3
	movl	%ecx, _ZZ13contextModel2vE2cm+136(%rip)
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movq	_ZZ13contextModel2vE2cm+96(%rip), %rdx
	movl	%ecx, (%rdx,%rax,4)
	movl	_ZZ13contextModel2vE3cxt+40(%rip), %esi
	movq	_ZZ13contextModel2vE4rcm9+32(%rip), %rax
	movb	(%rax), %cl
	testb	%cl, %cl
	je	.LBB54_71
# BB#70:
	movb	1(%rax), %dl
	movl	pos(%rip), %edi
	decl	%edi
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%edi, %ebp
	movq	buf+16(%rip), %rdi
	movslq	%ebp, %rbp
	cmpb	(%rdi,%rbp), %dl
	jne	.LBB54_71
# BB#72:
	cmpb	$-1, %cl
	je	.LBB54_74
# BB#73:
	incb	%cl
	movb	%cl, (%rax)
	jmp	.LBB54_74
.LBB54_71:
	movb	$1, (%rax)
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%eax, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movb	(%rax,%rcx), %al
	movq	_ZZ13contextModel2vE4rcm9+32(%rip), %rcx
	movb	%al, 1(%rcx)
.LBB54_74:                              # %_ZN13RunContextMap3setEj.exit90
	movl	$_ZZ13contextModel2vE4rcm9, %edi
	callq	_ZN2BHILi4EEixEj
	incq	%rax
	movq	%rax, _ZZ13contextModel2vE4rcm9+32(%rip)
	movl	_ZZ13contextModel2vE3cxt+48(%rip), %esi
	movq	_ZZ13contextModel2vE5rcm10+32(%rip), %rax
	movb	(%rax), %cl
	testb	%cl, %cl
	je	.LBB54_76
# BB#75:
	movb	1(%rax), %dl
	movl	pos(%rip), %edi
	decl	%edi
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%edi, %ebp
	movq	buf+16(%rip), %rdi
	movslq	%ebp, %rbp
	cmpb	(%rdi,%rbp), %dl
	jne	.LBB54_76
# BB#77:
	cmpb	$-1, %cl
	je	.LBB54_79
# BB#78:
	incb	%cl
	movb	%cl, (%rax)
	jmp	.LBB54_79
.LBB54_76:
	movb	$1, (%rax)
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %ecx
	decl	%ecx
	andl	%eax, %ecx
	movq	buf+16(%rip), %rax
	movslq	%ecx, %rcx
	movb	(%rax,%rcx), %al
	movq	_ZZ13contextModel2vE5rcm10+32(%rip), %rcx
	movb	%al, 1(%rcx)
.LBB54_79:                              # %_ZN13RunContextMap3setEj.exit
	movl	$_ZZ13contextModel2vE5rcm10, %edi
	callq	_ZN2BHILi4EEixEj
	incq	%rax
	movq	%rax, _ZZ13contextModel2vE5rcm10+32(%rip)
	movslq	_ZZ13contextModel2vE2cm+136(%rip), %rax
	leal	1(%rax), %ecx
	imull	$987654323, _ZZ13contextModel2vE3cxt+56(%rip), %edx # imm = 0x3ADE68B3
	movl	%ecx, _ZZ13contextModel2vE2cm+136(%rip)
	addl	%eax, %edx
	roll	$16, %edx
	imull	$123456791, %edx, %ecx  # imm = 0x75BCD17
	addl	%eax, %ecx
	movq	_ZZ13contextModel2vE2cm+96(%rip), %rdx
	movl	%ecx, (%rdx,%rax,4)
	movl	bpos(%rip), %ecx
.LBB54_80:
	movl	c0(%rip), %edx
	movl	pos(%rip), %eax
	decl	%eax
	movl	buf(%rip), %esi
	decl	%esi
	andl	%eax, %esi
	movq	buf+16(%rip), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	movl	y(%rip), %r9d
	movl	$_ZZ13contextModel2vE2cm, %edi
	movl	$_ZZ13contextModel2vE1m, %esi
	callq	_ZN10ContextMap4mix1ER5Mixeriiii
	movl	%eax, %r15d
	movq	_ZZ13contextModel2vE4rcm7+32(%rip), %rbx
	movzbl	1(%rbx), %edi
	leal	256(%rdi), %ebp
	movl	bpos(%rip), %edx
	movl	$8, %eax
	subl	%edx, %eax
	movl	%eax, %ecx
	shrl	%cl, %ebp
	movl	c0(%rip), %esi
	cmpl	%esi, %ebp
	jne	.LBB54_81
# BB#82:
	movl	$7, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	shll	$4, %edi
	andl	$16, %edi
	movzbl	(%rbx), %ecx
	movq	ilog+16(%rip), %rbp
	movzbl	1(%rbp,%rcx), %ecx
	addl	$-8, %edi
	imull	%ecx, %edi
	jmp	.LBB54_83
.LBB54_81:
	xorl	%edi, %edi
.LBB54_83:                              # %_ZN13RunContextMap3mixER5Mixer.exit89
	movslq	_ZZ13contextModel2vE1m+96(%rip), %rcx
	leal	1(%rcx), %ebp
	movl	%ebp, _ZZ13contextModel2vE1m+96(%rip)
	movq	_ZZ13contextModel2vE1m+32(%rip), %rbp
	movw	%di, (%rbp,%rcx,2)
	movq	_ZZ13contextModel2vE4rcm9+32(%rip), %rbx
	movzbl	1(%rbx), %edi
	leal	256(%rdi), %ebp
	movl	%eax, %ecx
	shrl	%cl, %ebp
	cmpl	%esi, %ebp
	jne	.LBB54_84
# BB#85:
	movl	$7, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	shll	$4, %edi
	andl	$16, %edi
	movzbl	(%rbx), %ecx
	movq	ilog+16(%rip), %rbp
	movzbl	1(%rbp,%rcx), %ecx
	addl	$-8, %edi
	imull	%ecx, %edi
	jmp	.LBB54_86
.LBB54_84:
	xorl	%edi, %edi
.LBB54_86:                              # %_ZN13RunContextMap3mixER5Mixer.exit86
	movslq	_ZZ13contextModel2vE1m+96(%rip), %rcx
	leal	1(%rcx), %ebp
	movl	%ebp, _ZZ13contextModel2vE1m+96(%rip)
	movq	_ZZ13contextModel2vE1m+32(%rip), %rbp
	movw	%di, (%rbp,%rcx,2)
	movq	_ZZ13contextModel2vE5rcm10+32(%rip), %rbx
	movzbl	1(%rbx), %edi
	leal	256(%rdi), %ebp
	movl	%eax, %ecx
	shrl	%cl, %ebp
	cmpl	%esi, %ebp
	jne	.LBB54_87
# BB#88:
	movl	$7, %ecx
	subl	%edx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	shll	$4, %edi
	andl	$16, %edi
	movzbl	(%rbx), %eax
	movq	ilog+16(%rip), %rcx
	movzbl	1(%rcx,%rax), %eax
	addl	$-8, %edi
	imull	%eax, %edi
	jmp	.LBB54_89
.LBB54_87:
	xorl	%edi, %edi
.LBB54_89:                              # %_ZN13RunContextMap3mixER5Mixer.exit
	movslq	_ZZ13contextModel2vE1m+96(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, _ZZ13contextModel2vE1m+96(%rip)
	movq	_ZZ13contextModel2vE1m+32(%rip), %rcx
	movw	%di, (%rcx,%rax,2)
	cmpl	$4, level(%rip)
	jl	.LBB54_92
# BB#90:
	movl	$_ZZ13contextModel2vE1m, %edi
	movl	%r14d, %esi
	movl	%r15d, %edx
	callq	_Z11sparseModelR5Mixerii
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z13distanceModelR5Mixer
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z8picModelR5Mixer
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z11recordModelR5Mixer
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z9wordModelR5Mixer
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z13indirectModelR5Mixer
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z8dmcModelR5Mixer
	cmpl	$8, _ZZ13contextModel2vE8filetype(%rip)
	jne	.LBB54_92
# BB#91:
	movl	$_ZZ13contextModel2vE1m, %edi
	callq	_Z8exeModelR5Mixer
.LBB54_92:
	xorl	%eax, %eax
	addl	$-2, %r15d
	cmovsl	%eax, %r15d
	movl	pos(%rip), %ecx
	leal	-1(%rcx), %edx
	movl	buf(%rip), %edi
	decl	%edi
	andl	%edi, %edx
	movq	buf+16(%rip), %rbp
	movslq	%edx, %rdx
	movzbl	(%rbp,%rdx), %r8d
	leal	-2(%rcx), %esi
	andl	%edi, %esi
	movslq	%esi, %rsi
	movzbl	(%rbp,%rsi), %r10d
	addl	$-3, %ecx
	andl	%edi, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rbp,%rcx), %r9d
	movl	_ZZ13contextModel2vE1m+92(%rip), %ecx
	leal	8(%r8,%rcx), %ebp
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rbx
	leal	1(%rbx), %ecx
	movl	%ecx, _ZZ13contextModel2vE1m+88(%rip)
	movq	_ZZ13contextModel2vE1m+80(%rip), %rcx
	movl	%ebp, (%rcx,%rbx,4)
	movl	$264, %ebp              # imm = 0x108
	addl	_ZZ13contextModel2vE1m+92(%rip), %ebp
	movl	%ebp, _ZZ13contextModel2vE1m+92(%rip)
	addl	c0(%rip), %ebp
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rbx
	leal	1(%rbx), %edx
	movl	%edx, _ZZ13contextModel2vE1m+88(%rip)
	movl	%ebp, (%rcx,%rbx,4)
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	leal	256(%rdx), %ebp
	movl	c4(%rip), %ebx
	shrl	$2, %ebx
	andl	$56, %ebx
	xorl	%edi, %edi
	cmpb	%r10b, %r8b
	sete	%dil
	shll	$6, %edi
	xorl	%esi, %esi
	cmpl	$8, _ZZ13contextModel2vE8filetype(%rip)
	movl	%ebp, _ZZ13contextModel2vE1m+92(%rip)
	sete	%sil
	shll	$7, %esi
	addl	%r15d, %edi
	leal	256(%rdx,%rdi), %edx
	addl	%ebx, %edx
	addl	%esi, %edx
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ13contextModel2vE1m+88(%rip)
	movl	%edx, (%rcx,%rsi,4)
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	leal	256(%rdx), %esi
	movl	%esi, _ZZ13contextModel2vE1m+92(%rip)
	leal	256(%rdx,%r10), %edx
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ13contextModel2vE1m+88(%rip)
	movl	%edx, (%rcx,%rsi,4)
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	leal	256(%rdx), %esi
	movl	%esi, _ZZ13contextModel2vE1m+92(%rip)
	leal	256(%rdx,%r9), %edx
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ13contextModel2vE1m+88(%rip)
	movl	%edx, (%rcx,%rsi,4)
	movl	_ZZ13contextModel2vE1m+92(%rip), %edx
	leal	256(%rdx), %esi
	movl	%esi, _ZZ13contextModel2vE1m+92(%rip)
	leal	256(%rdx,%r14), %edx
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rsi
	leal	1(%rsi), %edi
	movl	%edi, _ZZ13contextModel2vE1m+88(%rip)
	movl	%edx, (%rcx,%rsi,4)
	movl	$256, %ebp              # imm = 0x100
	addl	_ZZ13contextModel2vE1m+92(%rip), %ebp
	movl	%ebp, _ZZ13contextModel2vE1m+92(%rip)
	movl	bpos(%rip), %ebx
	testl	%ebx, %ebx
	je	.LBB54_94
# BB#93:
	movl	c0(%rip), %edx
	movl	$8, %ecx
	subl	%ebx, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	shrl	%r9d
	cmpl	$1, %ebx
	cmovel	%r9d, %eax
	addl	%edx, %eax
	cmpl	$6, %ebx
	movl	$5, %edx
	cmovll	%ebx, %edx
	shll	$8, %edx
	shrl	$5, %r8d
	shrl	$5, %r10d
	leal	(%r8,%r10,8), %ecx
	orl	%edx, %ecx
	andl	$192, %eax
	jmp	.LBB54_95
.LBB54_94:
	shrl	$7, %r9d
	movl	c4(%rip), %eax
	shrl	$31, %eax
	shrl	$6, %r10d
	leal	(%r9,%r10,4), %ecx
	leal	(%rcx,%rax,2), %ecx
	andl	$240, %r8d
	movl	%r8d, %eax
.LBB54_95:
	addl	%ecx, %eax
	addl	%ebp, %eax
	movslq	_ZZ13contextModel2vE1m+88(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _ZZ13contextModel2vE1m+88(%rip)
	movq	_ZZ13contextModel2vE1m+80(%rip), %rdx
	movl	%eax, (%rdx,%rcx,4)
	addl	$1536, _ZZ13contextModel2vE1m+92(%rip) # imm = 0x600
.LBB54_96:
	movl	$_ZZ13contextModel2vE1m, %edi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN5Mixer1pEv           # TAILCALL
.LBB54_36:
.Ltmp245:
	movq	%rax, %rbx
	movl	$_ZGVZ13contextModel2vE1m, %edi
	jmp	.LBB54_98
.LBB54_35:
.Ltmp242:
	movq	%rax, %rbx
	movl	$_ZGVZ13contextModel2vE5rcm10, %edi
	jmp	.LBB54_98
.LBB54_34:
.Ltmp239:
	movq	%rax, %rbx
	movl	$_ZGVZ13contextModel2vE4rcm9, %edi
	jmp	.LBB54_98
.LBB54_33:
.Ltmp236:
	movq	%rax, %rbx
	movl	$_ZGVZ13contextModel2vE4rcm7, %edi
	jmp	.LBB54_98
.LBB54_97:
.Ltmp233:
	movq	%rax, %rbx
	movl	$_ZGVZ13contextModel2vE2cm, %edi
.LBB54_98:
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end54:
	.size	_Z13contextModel2v, .Lfunc_end54-_Z13contextModel2v
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table54:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp231-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin16 #     jumps to .Ltmp233
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp235-.Ltmp234       #   Call between .Ltmp234 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin16 #     jumps to .Ltmp236
	.byte	0                       #   On action: cleanup
	.long	.Ltmp237-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp238-.Ltmp237       #   Call between .Ltmp237 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin16 #     jumps to .Ltmp239
	.byte	0                       #   On action: cleanup
	.long	.Ltmp240-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin16 #     jumps to .Ltmp242
	.byte	0                       #   On action: cleanup
	.long	.Ltmp243-.Lfunc_begin16 # >> Call Site 5 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	.Ltmp245-.Lfunc_begin16 #     jumps to .Ltmp245
	.byte	0                       #   On action: cleanup
	.long	.Ltmp244-.Lfunc_begin16 # >> Call Site 6 <<
	.long	.Lfunc_end54-.Ltmp244   #   Call between .Ltmp244 and .Lfunc_end54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13RunContextMapC2Ei,"axG",@progbits,_ZN13RunContextMapC2Ei,comdat
	.weak	_ZN13RunContextMapC2Ei
	.p2align	4, 0x90
	.type	_ZN13RunContextMapC2Ei,@function
_ZN13RunContextMapC2Ei:                 # @_ZN13RunContextMapC2Ei
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%rbp
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi254:
	.cfi_def_cfa_offset 32
.Lcfi255:
	.cfi_offset %rbx, -32
.Lcfi256:
	.cfi_offset %r14, -24
.Lcfi257:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	%esi, %ebp
	sarl	$31, %ebp
	shrl	$30, %ebp
	addl	%esi, %ebp
	movl	%ebp, %eax
	andl	$-4, %eax
	movl	%eax, 4(%rbx)
	movl	%eax, (%rbx)
	cmpl	$3, %esi
	jg	.LBB55_2
# BB#1:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	jmp	.LBB55_6
.LBB55_2:
	addl	$64, %eax
	movl	programChecker(%rip), %ecx
	addl	%eax, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB55_4
# BB#3:
	movl	%ecx, programChecker+4(%rip)
.LBB55_4:                               # %_ZN14ProgramChecker5allocEi.exit.i.i.i
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB55_11
# BB#5:
	movl	%eax, %ecx
	andl	$63, %ecx
	negq	%rcx
	leaq	64(%rax,%rcx), %rax
	movq	%rax, 16(%rbx)
.LBB55_6:                               # %_ZN2BHILi4EEC2Ei.exit
	sarl	$2, %ebp
	decl	%ebp
	movl	%ebp, 24(%rbx)
.Ltmp246:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN2BHILi4EEixEj
.Ltmp247:
# BB#7:
	incq	%rax
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB55_11:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.LBB55_8:
.Ltmp248:
	movq	%rax, %r14
	movl	$-64, %eax
	subl	(%rbx), %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB55_10
# BB#9:
	movl	%eax, programChecker+4(%rip)
.LBB55_10:
	movq	8(%rbx), %rdi
	callq	free
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end55:
	.size	_ZN13RunContextMapC2Ei, .Lfunc_end55-_ZN13RunContextMapC2Ei
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table55:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp246-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp247-.Ltmp246       #   Call between .Ltmp246 and .Ltmp247
	.long	.Ltmp248-.Lfunc_begin17 #     jumps to .Ltmp248
	.byte	0                       #   On action: cleanup
	.long	.Ltmp247-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end55-.Ltmp247   #   Call between .Ltmp247 and .Lfunc_end55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13RunContextMapD2Ev,"axG",@progbits,_ZN13RunContextMapD2Ev,comdat
	.weak	_ZN13RunContextMapD2Ev
	.p2align	4, 0x90
	.type	_ZN13RunContextMapD2Ev,@function
_ZN13RunContextMapD2Ev:                 # @_ZN13RunContextMapD2Ev
	.cfi_startproc
# BB#0:
	movl	$-64, %eax
	subl	(%rdi), %eax
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB56_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB56_2:                               # %_ZN2BHILi4EED2Ev.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end56:
	.size	_ZN13RunContextMapD2Ev, .Lfunc_end56-_ZN13RunContextMapD2Ev
	.cfi_endproc

	.text
	.globl	_ZN9PredictorC2Ev
	.p2align	4, 0x90
	.type	_ZN9PredictorC2Ev,@function
_ZN9PredictorC2Ev:                      # @_ZN9PredictorC2Ev
	.cfi_startproc
# BB#0:
	movl	$2048, (%rdi)           # imm = 0x800
	retq
.Lfunc_end57:
	.size	_ZN9PredictorC2Ev, .Lfunc_end57-_ZN9PredictorC2Ev
	.cfi_endproc

	.globl	_ZN9Predictor6updateEv
	.p2align	4, 0x90
	.type	_ZN9Predictor6updateEv,@function
_ZN9Predictor6updateEv:                 # @_ZN9Predictor6updateEv
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%rbp
.Lcfi258:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi259:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi260:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi261:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi262:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi263:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi264:
	.cfi_def_cfa_offset 64
.Lcfi265:
	.cfi_offset %rbx, -56
.Lcfi266:
	.cfi_offset %r12, -48
.Lcfi267:
	.cfi_offset %r13, -40
.Lcfi268:
	.cfi_offset %r14, -32
.Lcfi269:
	.cfi_offset %r15, -24
.Lcfi270:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movb	_ZGVZN9Predictor6updateEvE1a(%rip), %al
	testb	%al, %al
	jne	.LBB58_4
# BB#1:
	movl	$_ZGVZN9Predictor6updateEvE1a, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB58_4
# BB#2:
.Ltmp249:
	movl	$_ZZN9Predictor6updateEvE1a, %edi
	movl	$256, %esi              # imm = 0x100
	callq	_ZN4APM1C2Ei
.Ltmp250:
# BB#3:
	movl	$_ZN4APM1D2Ev, %edi
	movl	$_ZZN9Predictor6updateEvE1a, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZN9Predictor6updateEvE1a, %edi
	callq	__cxa_guard_release
.LBB58_4:
	movb	_ZGVZN9Predictor6updateEvE2a1(%rip), %al
	testb	%al, %al
	jne	.LBB58_8
# BB#5:
	movl	$_ZGVZN9Predictor6updateEvE2a1, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB58_8
# BB#6:
.Ltmp252:
	movl	$_ZZN9Predictor6updateEvE2a1, %edi
	movl	$65536, %esi            # imm = 0x10000
	callq	_ZN4APM1C2Ei
.Ltmp253:
# BB#7:
	movl	$_ZN4APM1D2Ev, %edi
	movl	$_ZZN9Predictor6updateEvE2a1, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZN9Predictor6updateEvE2a1, %edi
	callq	__cxa_guard_release
.LBB58_8:
	movb	_ZGVZN9Predictor6updateEvE2a2(%rip), %al
	testb	%al, %al
	jne	.LBB58_12
# BB#9:
	movl	$_ZGVZN9Predictor6updateEvE2a2, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB58_12
# BB#10:
.Ltmp255:
	movl	$_ZZN9Predictor6updateEvE2a2, %edi
	movl	$65536, %esi            # imm = 0x10000
	callq	_ZN4APM1C2Ei
.Ltmp256:
# BB#11:
	movl	$_ZN4APM1D2Ev, %edi
	movl	$_ZZN9Predictor6updateEvE2a2, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZN9Predictor6updateEvE2a2, %edi
	callq	__cxa_guard_release
.LBB58_12:
	movb	_ZGVZN9Predictor6updateEvE2a3(%rip), %al
	testb	%al, %al
	jne	.LBB58_16
# BB#13:
	movl	$_ZGVZN9Predictor6updateEvE2a3, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB58_16
# BB#14:
.Ltmp258:
	movl	$_ZZN9Predictor6updateEvE2a3, %edi
	movl	$65536, %esi            # imm = 0x10000
	callq	_ZN4APM1C2Ei
.Ltmp259:
# BB#15:
	movl	$_ZN4APM1D2Ev, %edi
	movl	$_ZZN9Predictor6updateEvE2a3, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZN9Predictor6updateEvE2a3, %edi
	callq	__cxa_guard_release
.LBB58_16:
	movb	_ZGVZN9Predictor6updateEvE2a4(%rip), %al
	testb	%al, %al
	jne	.LBB58_20
# BB#17:
	movl	$_ZGVZN9Predictor6updateEvE2a4, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB58_20
# BB#18:
.Ltmp261:
	movl	$_ZZN9Predictor6updateEvE2a4, %edi
	movl	$65536, %esi            # imm = 0x10000
	callq	_ZN4APM1C2Ei
.Ltmp262:
# BB#19:
	movl	$_ZN4APM1D2Ev, %edi
	movl	$_ZZN9Predictor6updateEvE2a4, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZN9Predictor6updateEvE2a4, %edi
	callq	__cxa_guard_release
.LBB58_20:
	movb	_ZGVZN9Predictor6updateEvE2a5(%rip), %al
	testb	%al, %al
	jne	.LBB58_24
# BB#21:
	movl	$_ZGVZN9Predictor6updateEvE2a5, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB58_24
# BB#22:
.Ltmp264:
	movl	$_ZZN9Predictor6updateEvE2a5, %edi
	movl	$65536, %esi            # imm = 0x10000
	callq	_ZN4APM1C2Ei
.Ltmp265:
# BB#23:
	movl	$_ZN4APM1D2Ev, %edi
	movl	$_ZZN9Predictor6updateEvE2a5, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZN9Predictor6updateEvE2a5, %edi
	callq	__cxa_guard_release
.LBB58_24:
	movb	_ZGVZN9Predictor6updateEvE2a6(%rip), %al
	testb	%al, %al
	jne	.LBB58_28
# BB#25:
	movl	$_ZGVZN9Predictor6updateEvE2a6, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB58_28
# BB#26:
.Ltmp267:
	movl	$_ZZN9Predictor6updateEvE2a6, %edi
	movl	$65536, %esi            # imm = 0x10000
	callq	_ZN4APM1C2Ei
.Ltmp268:
# BB#27:
	movl	$_ZN4APM1D2Ev, %edi
	movl	$_ZZN9Predictor6updateEvE2a6, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZN9Predictor6updateEvE2a6, %edi
	callq	__cxa_guard_release
.LBB58_28:
	movl	c0(%rip), %eax
	addl	%eax, %eax
	addl	y(%rip), %eax
	movl	%eax, c0(%rip)
	cmpl	$256, %eax              # imm = 0x100
	jl	.LBB58_30
# BB#29:
	movl	pos(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, pos(%rip)
	movl	buf(%rip), %edx
	decl	%edx
	andl	%ecx, %edx
	movq	buf+16(%rip), %rcx
	movslq	%edx, %rdx
	movb	%al, (%rcx,%rdx)
	movl	c4(%rip), %eax
	shll	$8, %eax
	movl	c0(%rip), %ecx
	leal	-256(%rcx,%rax), %eax
	movl	%eax, c4(%rip)
	movl	$1, c0(%rip)
.LBB58_30:
	movl	bpos(%rip), %eax
	incl	%eax
	andl	$7, %eax
	movl	%eax, bpos(%rip)
	callq	_Z13contextModel2v
	movslq	c0(%rip), %rsi
	movq	stretch+16(%rip), %r11
	movslq	%eax, %r9
	movswl	(%r11,%r9,2), %eax
	imull	$65662, y(%rip), %ecx   # imm = 0x1007E
	movslq	_ZZN9Predictor6updateEvE1a(%rip), %rbp
	movq	_ZZN9Predictor6updateEvE1a+24(%rip), %rdx
	movzwl	(%rdx,%rbp,2), %ebx
	movl	%ecx, %edi
	subl	%ebx, %edi
	shrl	$7, %edi
	addl	%ebx, %edi
	movw	%di, (%rdx,%rbp,2)
	movzwl	2(%rdx,%rbp,2), %edi
	subl	%edi, %ecx
	shrl	$7, %ecx
	addl	%edi, %ecx
	movw	%cx, 2(%rdx,%rbp,2)
	movl	%eax, %ecx
	andl	$127, %ecx
	addl	$2048, %eax             # imm = 0x800
	sarl	$7, %eax
	movq	%rsi, %rdi
	shlq	$5, %rdi
	addq	%rsi, %rdi
	cltq
	addq	%rdi, %rax
	movl	%eax, _ZZN9Predictor6updateEvE1a(%rip)
	movzwl	(%rdx,%rax,2), %esi
	movl	$128, %edi
	subl	%ecx, %edi
	imull	%esi, %edi
	cltq
	movzwl	2(%rdx,%rax,2), %eax
	imull	%ecx, %eax
	addl	%edi, %eax
	shrl	$11, %eax
	movl	%eax, (%r14)
	movl	c0(%rip), %r8d
	movl	pos(%rip), %r13d
	leal	-1(%r13), %eax
	movl	buf(%rip), %r12d
	decl	%r12d
	andl	%r12d, %eax
	movq	buf+16(%rip), %r15
	movslq	%eax, %r10
	movzbl	(%r15,%r10), %eax
	shll	$8, %eax
	addl	%r8d, %eax
	movswl	(%r11,%r9,2), %esi
	imull	$65662, y(%rip), %r11d  # imm = 0x1007E
	movslq	_ZZN9Predictor6updateEvE2a1(%rip), %rbp
	movq	_ZZN9Predictor6updateEvE2a1+24(%rip), %rcx
	movzwl	(%rcx,%rbp,2), %ebx
	movl	%r11d, %edx
	subl	%ebx, %edx
	shrl	$7, %edx
	addl	%ebx, %edx
	movw	%dx, (%rcx,%rbp,2)
	movzwl	2(%rcx,%rbp,2), %edx
	movl	%r11d, %edi
	subl	%edx, %edi
	shrl	$7, %edi
	addl	%edx, %edi
	movw	%di, 2(%rcx,%rbp,2)
	movl	%esi, %edx
	andl	$127, %edx
	addl	$2048, %esi             # imm = 0x800
	sarl	$7, %esi
	movl	%eax, %edi
	shll	$5, %edi
	addl	%eax, %edi
	addl	%esi, %edi
	movl	%edi, _ZZN9Predictor6updateEvE2a1(%rip)
	movslq	%edi, %rax
	movzwl	(%rcx,%rax,2), %esi
	movl	$128, %edi
	subl	%edx, %edi
	imull	%esi, %edi
	movzwl	2(%rcx,%rax,2), %ecx
	imull	%edx, %ecx
	addl	%edi, %ecx
	shrl	$11, %ecx
	movzbl	(%r15,%r10), %eax
	addl	$-2, %r13d
	andl	%r12d, %r13d
	movslq	%r13d, %rdx
	movzbl	(%r15,%rdx), %edx
	imull	$200002979, %eax, %esi  # imm = 0xBEBCDA3
	imull	$30005491, %edx, %edi   # imm = 0x1C9D8F3
	leal	-230011545(%rsi,%rdi), %esi
	shrl	$2, %eax
	shrl	$3, %edx
	xorl	%eax, %edx
	xorl	%esi, %edx
	shrl	$9, %esi
	xorl	%edx, %esi
	notl	%esi
	movzwl	%si, %r10d
	xorl	%r8d, %r10d
	movq	stretch+16(%rip), %r15
	movswl	(%r15,%r9,2), %edx
	movslq	_ZZN9Predictor6updateEvE2a2(%rip), %rdi
	movq	_ZZN9Predictor6updateEvE2a2+24(%rip), %rsi
	movzwl	(%rsi,%rdi,2), %ebp
	movl	%r11d, %ebx
	subl	%ebp, %ebx
	shrl	$7, %ebx
	addl	%ebp, %ebx
	movw	%bx, (%rsi,%rdi,2)
	movzwl	2(%rsi,%rdi,2), %ebp
	subl	%ebp, %r11d
	shrl	$7, %r11d
	addl	%ebp, %r11d
	movw	%r11w, 2(%rsi,%rdi,2)
	movl	%edx, %edi
	andl	$127, %edi
	addl	$2048, %edx             # imm = 0x800
	sarl	$7, %edx
	movl	%r10d, %eax
	shll	$5, %eax
	addl	%r10d, %eax
	addl	%edx, %eax
	movl	%eax, _ZZN9Predictor6updateEvE2a2(%rip)
	cltq
	movzwl	(%rsi,%rax,2), %edx
	movl	$128, %ebp
	subl	%edi, %ebp
	imull	%edx, %ebp
	movzwl	2(%rsi,%rax,2), %r12d
	imull	%edi, %r12d
	addl	%ebp, %r12d
	shrl	$11, %r12d
	movl	c0(%rip), %r8d
	movl	pos(%rip), %eax
	leal	-1(%rax), %edx
	movl	buf(%rip), %ebp
	decl	%ebp
	andl	%ebp, %edx
	movq	buf+16(%rip), %r11
	movslq	%edx, %r10
	movzbl	(%r11,%r10), %edx
	leal	-2(%rax), %ebx
	andl	%ebp, %ebx
	movslq	%ebx, %rbx
	movzbl	(%r11,%rbx), %ebx
	addl	$-3, %eax
	andl	%ebp, %eax
	cltq
	movzbl	(%r11,%rax), %eax
	imull	$200002979, %edx, %ebp  # imm = 0xBEBCDA3
	imull	$30005491, %ebx, %edi   # imm = 0x1C9D8F3
	imull	$50004239, %eax, %esi   # imm = 0x2FB010F
	addl	%ebp, %edi
	leal	-180007306(%rsi,%rdi), %esi
	shrl	$2, %edx
	shrl	$3, %ebx
	shrl	$4, %eax
	xorl	%edx, %ebx
	xorl	%eax, %ebx
	xorl	%esi, %ebx
	shrl	$9, %esi
	xorl	%ebx, %esi
	movzwl	%si, %r13d
	xorl	%r8d, %r13d
	movswl	(%r15,%r9,2), %edx
	imull	$65662, y(%rip), %r15d  # imm = 0x1007E
	movslq	_ZZN9Predictor6updateEvE2a3(%rip), %rsi
	movq	_ZZN9Predictor6updateEvE2a3+24(%rip), %rbp
	movzwl	(%rbp,%rsi,2), %eax
	movl	%r15d, %ebx
	subl	%eax, %ebx
	shrl	$7, %ebx
	addl	%eax, %ebx
	movw	%bx, (%rbp,%rsi,2)
	movzwl	2(%rbp,%rsi,2), %eax
	movl	%r15d, %ebx
	subl	%eax, %ebx
	shrl	$7, %ebx
	addl	%eax, %ebx
	movw	%bx, 2(%rbp,%rsi,2)
	movl	%edx, %eax
	andl	$127, %eax
	addl	$2048, %edx             # imm = 0x800
	sarl	$7, %edx
	movl	%r13d, %esi
	shll	$5, %esi
	addl	%r13d, %esi
	addl	%edx, %esi
	movl	%esi, _ZZN9Predictor6updateEvE2a3(%rip)
	movslq	%esi, %rdx
	movzwl	(%rbp,%rdx,2), %esi
	movl	$128, %ebx
	subl	%eax, %ebx
	imull	%esi, %ebx
	movzwl	2(%rbp,%rdx,2), %edx
	imull	%eax, %edx
	addl	%ebx, %edx
	shrl	$11, %edx
	addl	%ecx, %r9d
	addl	%r12d, %r9d
	leal	2(%rdx,%r9), %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movzbl	(%r11,%r10), %ecx
	shll	$8, %ecx
	addl	%r8d, %ecx
	movq	stretch+16(%rip), %r8
	movslq	(%r14), %r10
	movswl	(%r8,%r10,2), %esi
	movslq	_ZZN9Predictor6updateEvE2a4(%rip), %rbx
	movq	_ZZN9Predictor6updateEvE2a4+24(%rip), %rax
	movzwl	(%rax,%rbx,2), %edx
	movl	%r15d, %edi
	subl	%edx, %edi
	shrl	$7, %edi
	addl	%edx, %edi
	movw	%di, (%rax,%rbx,2)
	movzwl	2(%rax,%rbx,2), %edx
	subl	%edx, %r15d
	shrl	$7, %r15d
	addl	%edx, %r15d
	movw	%r15w, 2(%rax,%rbx,2)
	movl	%esi, %edx
	andl	$127, %edx
	addl	$2048, %esi             # imm = 0x800
	sarl	$7, %esi
	movl	%ecx, %edi
	shll	$5, %edi
	addl	%ecx, %edi
	addl	%esi, %edi
	movl	%edi, _ZZN9Predictor6updateEvE2a4(%rip)
	movslq	%edi, %rcx
	movzwl	(%rax,%rcx,2), %esi
	movl	$128, %edi
	subl	%edx, %edi
	imull	%esi, %edi
	movzwl	2(%rax,%rcx,2), %r12d
	imull	%edx, %r12d
	addl	%edi, %r12d
	movl	pos(%rip), %ebx
	leal	-1(%rbx), %ecx
	movl	buf(%rip), %r13d
	decl	%r13d
	andl	%r13d, %ecx
	movq	buf+16(%rip), %rsi
	movslq	%ecx, %r11
	movzbl	(%rsi,%r11), %edx
	leal	-2(%rbx), %ecx
	andl	%r13d, %ecx
	movslq	%ecx, %r15
	movzbl	(%rsi,%r15), %edi
	imull	$200002979, %edx, %ecx  # imm = 0xBEBCDA3
	imull	$30005491, %edi, %ebp   # imm = 0x1C9D8F3
	leal	-230011545(%rcx,%rbp), %ecx
	shrl	$2, %edx
	shrl	$3, %edi
	xorl	%edx, %edi
	xorl	%ecx, %edi
	shrl	$9, %ecx
	xorl	%edi, %ecx
	movswl	(%r8,%r10,2), %ebp
	movq	%r14, %r9
	imull	$65662, y(%rip), %r14d  # imm = 0x1007E
	movslq	_ZZN9Predictor6updateEvE2a5(%rip), %rax
	movq	_ZZN9Predictor6updateEvE2a5+24(%rip), %r10
	movzwl	(%r10,%rax,2), %r8d
	movl	%r14d, %edi
	subl	%r8d, %edi
	shrl	$7, %edi
	addl	%r8d, %edi
	movl	c0(%rip), %r8d
	movw	%di, (%r10,%rax,2)
	movzwl	2(%r10,%rax,2), %edi
	movl	%r14d, %edx
	subl	%edi, %edx
	shrl	$7, %edx
	addl	%edi, %edx
	notl	%ecx
	movzwl	%cx, %ecx
	xorl	%r8d, %ecx
	movw	%dx, 2(%r10,%rax,2)
	movl	%ebp, %eax
	andl	$127, %eax
	addl	$2048, %ebp             # imm = 0x800
	sarl	$7, %ebp
	movl	%ecx, %edx
	shll	$5, %edx
	addl	%ecx, %edx
	addl	%ebp, %edx
	movl	%edx, _ZZN9Predictor6updateEvE2a5(%rip)
	movslq	%edx, %rcx
	movzwl	(%r10,%rcx,2), %edx
	movl	$128, %ebp
	subl	%eax, %ebp
	imull	%edx, %ebp
	movzwl	2(%r10,%rcx,2), %edi
	imull	%eax, %edi
	addl	%ebp, %edi
	movslq	(%r9), %r10
	movzbl	(%rsi,%r11), %r11d
	movzbl	(%rsi,%r15), %ecx
	addl	$-3, %ebx
	andl	%r13d, %ebx
	movslq	%ebx, %rax
	movzbl	(%rsi,%rax), %eax
	movslq	_ZZN9Predictor6updateEvE2a6(%rip), %rdx
	movq	_ZZN9Predictor6updateEvE2a6+24(%rip), %rsi
	movzwl	(%rsi,%rdx,2), %ebx
	movl	%r14d, %ebp
	subl	%ebx, %ebp
	shrl	$7, %ebp
	addl	%ebx, %ebp
	movq	stretch+16(%rip), %rbx
	movswl	(%rbx,%r10,2), %ebx
	movw	%bp, (%rsi,%rdx,2)
	movzwl	2(%rsi,%rdx,2), %ebp
	subl	%ebp, %r14d
	shrl	$7, %r14d
	addl	%ebp, %r14d
	movw	%r14w, 2(%rsi,%rdx,2)
	imull	$200002979, %r11d, %edx # imm = 0xBEBCDA3
	imull	$30005491, %ecx, %ebp   # imm = 0x1C9D8F3
	addl	%edx, %ebp
	imull	$50004239, %eax, %edx   # imm = 0x2FB010F
	leal	-180007306(%rdx,%rbp), %edx
	shrl	$2, %r11d
	shrl	$3, %ecx
	xorl	%r11d, %ecx
	shrl	$4, %eax
	xorl	%eax, %ecx
	xorl	%edx, %ecx
	shrl	$9, %edx
	xorl	%ecx, %edx
	movl	$128, %eax
	movzwl	%dx, %ecx
	xorl	%r8d, %ecx
	movl	%ecx, %edx
	shll	$5, %edx
	addl	%ecx, %edx
	movl	%ebx, %ecx
	andl	$127, %ecx
	addl	$2048, %ebx             # imm = 0x800
	sarl	$7, %ebx
	addl	%ebx, %edx
	movl	%edx, _ZZN9Predictor6updateEvE2a6(%rip)
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %ebp
	subl	%ecx, %eax
	imull	%ebp, %eax
	movzwl	2(%rsi,%rdx,2), %edx
	imull	%ecx, %edx
	addl	%eax, %edx
	shrl	$11, %r12d
	shrl	$11, %edi
	addl	%r12d, %edi
	addl	%r10d, %edi
	shrl	$11, %edx
	leal	2(%rdx,%rdi), %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	sarl	$2, %ecx
	sarl	$2, %eax
	leal	1(%rcx,%rax), %eax
	sarl	%eax
	movl	%eax, (%r9)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB58_36:
.Ltmp269:
	movq	%rax, %rbx
	movl	$_ZGVZN9Predictor6updateEvE2a6, %edi
	jmp	.LBB58_38
.LBB58_35:
.Ltmp266:
	movq	%rax, %rbx
	movl	$_ZGVZN9Predictor6updateEvE2a5, %edi
	jmp	.LBB58_38
.LBB58_34:
.Ltmp263:
	movq	%rax, %rbx
	movl	$_ZGVZN9Predictor6updateEvE2a4, %edi
	jmp	.LBB58_38
.LBB58_33:
.Ltmp260:
	movq	%rax, %rbx
	movl	$_ZGVZN9Predictor6updateEvE2a3, %edi
	jmp	.LBB58_38
.LBB58_32:
.Ltmp257:
	movq	%rax, %rbx
	movl	$_ZGVZN9Predictor6updateEvE2a2, %edi
	jmp	.LBB58_38
.LBB58_31:
.Ltmp254:
	movq	%rax, %rbx
	movl	$_ZGVZN9Predictor6updateEvE2a1, %edi
	jmp	.LBB58_38
.LBB58_37:
.Ltmp251:
	movq	%rax, %rbx
	movl	$_ZGVZN9Predictor6updateEvE1a, %edi
.LBB58_38:
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end58:
	.size	_ZN9Predictor6updateEv, .Lfunc_end58-_ZN9Predictor6updateEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table58:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp249-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp250-.Ltmp249       #   Call between .Ltmp249 and .Ltmp250
	.long	.Ltmp251-.Lfunc_begin18 #     jumps to .Ltmp251
	.byte	0                       #   On action: cleanup
	.long	.Ltmp252-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp253-.Ltmp252       #   Call between .Ltmp252 and .Ltmp253
	.long	.Ltmp254-.Lfunc_begin18 #     jumps to .Ltmp254
	.byte	0                       #   On action: cleanup
	.long	.Ltmp255-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp256-.Ltmp255       #   Call between .Ltmp255 and .Ltmp256
	.long	.Ltmp257-.Lfunc_begin18 #     jumps to .Ltmp257
	.byte	0                       #   On action: cleanup
	.long	.Ltmp258-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Ltmp259-.Ltmp258       #   Call between .Ltmp258 and .Ltmp259
	.long	.Ltmp260-.Lfunc_begin18 #     jumps to .Ltmp260
	.byte	0                       #   On action: cleanup
	.long	.Ltmp261-.Lfunc_begin18 # >> Call Site 5 <<
	.long	.Ltmp262-.Ltmp261       #   Call between .Ltmp261 and .Ltmp262
	.long	.Ltmp263-.Lfunc_begin18 #     jumps to .Ltmp263
	.byte	0                       #   On action: cleanup
	.long	.Ltmp264-.Lfunc_begin18 # >> Call Site 6 <<
	.long	.Ltmp265-.Ltmp264       #   Call between .Ltmp264 and .Ltmp265
	.long	.Ltmp266-.Lfunc_begin18 #     jumps to .Ltmp266
	.byte	0                       #   On action: cleanup
	.long	.Ltmp267-.Lfunc_begin18 # >> Call Site 7 <<
	.long	.Ltmp268-.Ltmp267       #   Call between .Ltmp267 and .Ltmp268
	.long	.Ltmp269-.Lfunc_begin18 #     jumps to .Ltmp269
	.byte	0                       #   On action: cleanup
	.long	.Ltmp268-.Lfunc_begin18 # >> Call Site 8 <<
	.long	.Lfunc_end58-.Ltmp268   #   Call between .Ltmp268 and .Lfunc_end58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN4APM1D2Ev,"axG",@progbits,_ZN4APM1D2Ev,comdat
	.weak	_ZN4APM1D2Ev
	.p2align	4, 0x90
	.type	_ZN4APM1D2Ev,@function
_ZN4APM1D2Ev:                           # @_ZN4APM1D2Ev
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %ecx
	addl	%ecx, %ecx
	movl	programChecker(%rip), %eax
	subl	%ecx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB59_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB59_2:                               # %_ZN5ArrayItLi0EED2Ev.exit
	movq	16(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end59:
	.size	_ZN4APM1D2Ev, .Lfunc_end59-_ZN4APM1D2Ev
	.cfi_endproc

	.text
	.globl	_ZN7EncoderC2E4ModeP8_IO_FILE
	.p2align	4, 0x90
	.type	_ZN7EncoderC2E4ModeP8_IO_FILE,@function
_ZN7EncoderC2E4ModeP8_IO_FILE:          # @_ZN7EncoderC2E4ModeP8_IO_FILE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi271:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi272:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi273:
	.cfi_def_cfa_offset 32
.Lcfi274:
	.cfi_offset %rbx, -32
.Lcfi275:
	.cfi_offset %r14, -24
.Lcfi276:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$2048, (%rbx)           # imm = 0x800
	movl	%esi, 4(%rbx)
	movq	%rdx, 8(%rbx)
	movl	$0, 16(%rbx)
	movl	$-1, 20(%rbx)
	movl	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	cmpl	$0, level(%rip)
	jle	.LBB60_2
# BB#1:
	cmpl	$1, %esi
	jne	.LBB60_2
# BB#5:                                 # %.preheader13.preheader
	movq	%rdx, %rdi
	callq	_IO_getc
	movzbl	%al, %r14d
	movl	%r14d, 24(%rbx)
	shll	$8, %r14d
	movq	8(%rbx), %rdi
	callq	_IO_getc
	movzbl	%al, %ebp
	orl	%r14d, %ebp
	movl	%ebp, 24(%rbx)
	shll	$8, %ebp
	movq	8(%rbx), %rdi
	callq	_IO_getc
	movzbl	%al, %r14d
	orl	%ebp, %r14d
	movl	%r14d, 24(%rbx)
	shll	$8, %r14d
	movq	8(%rbx), %rdi
	callq	_IO_getc
	movzbl	%al, %eax
	orl	%r14d, %eax
	movl	%eax, 24(%rbx)
.LBB60_2:                               # %.preheader.preheader
	movq	$-2048, %rcx            # imm = 0xF800
	.p2align	4, 0x90
.LBB60_3:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leal	2051(%rcx), %esi
	movl	$16384, %eax            # imm = 0x4000
	xorl	%edx, %edx
	idivl	%esi
	movl	%eax, _ZL2dt+4096(%rcx,%rcx)
	leal	2053(%rcx), %esi
	movl	$16384, %eax            # imm = 0x4000
	xorl	%edx, %edx
	idivl	%esi
	movl	%eax, _ZL2dt+4100(%rcx,%rcx)
	addq	$4, %rcx
	jne	.LBB60_3
# BB#4:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end60:
	.size	_ZN7EncoderC2E4ModeP8_IO_FILE, .Lfunc_end60-_ZN7EncoderC2E4ModeP8_IO_FILE
	.cfi_endproc

	.globl	_ZN7Encoder5flushEv
	.p2align	4, 0x90
	.type	_ZN7Encoder5flushEv,@function
_ZN7Encoder5flushEv:                    # @_ZN7Encoder5flushEv
	.cfi_startproc
# BB#0:
	cmpl	$0, 4(%rdi)
	jne	.LBB61_2
# BB#1:
	movl	level(%rip), %eax
	testl	%eax, %eax
	jle	.LBB61_2
# BB#3:
	movzbl	19(%rdi), %eax
	movq	8(%rdi), %rsi
	movl	%eax, %edi
	jmp	_IO_putc                # TAILCALL
.LBB61_2:
	retq
.Lfunc_end61:
	.size	_ZN7Encoder5flushEv, .Lfunc_end61-_ZN7Encoder5flushEv
	.cfi_endproc

	.globl	_Z6detectP8_IO_FILEi8Filetype
	.p2align	4, 0x90
	.type	_Z6detectP8_IO_FILEi8Filetype,@function
_Z6detectP8_IO_FILEi8Filetype:          # @_Z6detectP8_IO_FILEi8Filetype
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%rbp
.Lcfi277:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi278:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi279:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi280:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi281:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi282:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi283:
	.cfi_def_cfa_offset 320
.Lcfi284:
	.cfi_offset %rbx, -56
.Lcfi285:
	.cfi_offset %r12, -48
.Lcfi286:
	.cfi_offset %r13, -40
.Lcfi287:
	.cfi_offset %r14, -32
.Lcfi288:
	.cfi_offset %r15, -24
.Lcfi289:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, 156(%rsp)         # 4-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	callq	ftell
	movq	%rax, %r14
	movl	programChecker(%rip), %ebx
	leal	1024(%rbx), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %ebp
	cmpl	%ebp, %eax
	jle	.LBB62_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %ebp
.LBB62_2:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movl	$1024, %edi             # imm = 0x400
	movl	$1, %esi
	callq	calloc
	movq	%rax, 168(%rsp)         # 8-byte Spill
	testq	%rax, %rax
	je	.LBB62_138
# BB#3:                                 # %_ZN5ArrayIiLi0EEC2Ei.exit
	addl	$2048, %ebx             # imm = 0x800
	movl	%ebx, programChecker(%rip)
	cmpl	%ebp, %ebx
	jle	.LBB62_5
# BB#4:
	movl	%ebx, programChecker+4(%rip)
.LBB62_5:                               # %_ZN14ProgramChecker5allocEi.exit.i.i672
	movl	$1024, %edi             # imm = 0x400
	movl	$1, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB62_139
# BB#6:
	cmpl	$0, 156(%rsp)           # 4-byte Folded Reload
	jle	.LBB62_131
# BB#7:                                 # %.lr.ph
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-2(%rax), %eax
	movl	%eax, 188(%rsp)         # 4-byte Spill
	movl	$0, %edi
	movl	$0, %r12d
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
	movl	$0, %r11d
	movl	$0, %ebp
	movl	$-1, %r8d
	movl	$0, %r9d
	movl	$0, 160(%rsp)           # 4-byte Folded Spill
	movl	$0, 108(%rsp)           # 4-byte Folded Spill
	movl	$0, 152(%rsp)           # 4-byte Folded Spill
	movl	$0, 84(%rsp)            # 4-byte Folded Spill
	movl	$0, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	movl	$0, %ecx
	movl	$0, %edx
	movl	$-1, %r13d
	movl	$0, %eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movl	$0, 140(%rsp)           # 4-byte Folded Spill
	movl	$0, %ebx
	xorl	%r10d, %r10d
	movl	$0, 76(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	movl	$0, 56(%rsp)            # 4-byte Folded Spill
.LBB62_8:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, 104(%rsp)         # 4-byte Spill
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	%r11d, 4(%rsp)          # 4-byte Spill
	movl	%edi, %ebp
	movq	%r14, %r15
	movl	%r10d, %r14d
	movl	%r8d, 80(%rsp)          # 4-byte Spill
	movq	%r9, 120(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$-1, %eax
	je	.LBB62_117
# BB#9:                                 #   in Loop: Header=BB62_8 Depth=1
	movl	%ebp, %edi
	movl	%edi, %ecx
	shrl	$24, %ecx
	movl	%ecx, 164(%rsp)         # 4-byte Spill
	shll	$8, %edi
	movq	%rax, 128(%rsp)         # 8-byte Spill
	orl	%eax, %edi
	movl	84(%rsp), %esi          # 4-byte Reload
	testl	%esi, %esi
	sete	%al
	cmpl	$2, %r12d
	setg	%cl
	andb	%al, %cl
	movl	%edi, %eax
	andl	$-16, %eax
	cmpl	$-2555936, %eax         # imm = 0xFFD8FFE0
	sete	%dl
	leal	2(%r12), %eax
	andb	%cl, %dl
	cmovnel	%r12d, %esi
	testb	%dl, %dl
	movl	160(%rsp), %ebp         # 4-byte Reload
	cmovnel	%eax, %ebp
	movl	%esi, 84(%rsp)          # 4-byte Spill
	movl	%ebx, %r9d
	testl	%esi, %esi
	je	.LBB62_16
# BB#10:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	%ebp, %r12d
	movl	$0, %r11d
	movl	%r14d, %r10d
	jne	.LBB62_14
# BB#11:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%edi, %ecx
	andl	$-1048576, %ecx         # imm = 0xFFF00000
	cmpl	$-2097152, %ecx         # imm = 0xFFE00000
	je	.LBB62_13
# BB#12:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%edi, %ecx
	andl	$-65536, %ecx           # imm = 0xFFFF0000
	cmpl	$-131072, %ecx          # imm = 0xFFFE0000
	movl	%r12d, %ebp
	jne	.LBB62_14
.LBB62_13:                              #   in Loop: Header=BB62_8 Depth=1
	movzwl	%di, %ecx
	addl	%ecx, %eax
	movl	%eax, %ebp
.LBB62_14:                              #   in Loop: Header=BB62_8 Depth=1
	movl	%r12d, %eax
	movl	84(%rsp), %esi          # 4-byte Reload
	subl	%esi, %eax
	movl	%edi, %ecx
	andl	$-16776961, %ecx        # imm = 0xFF0000FF
	cmpl	%r12d, %ebp
	movl	%edi, %r8d
	movl	152(%rsp), %edi         # 4-byte Reload
	movl	%edi, %edx
	cmovll	%r12d, %edx
	cmpl	$65536, %eax            # imm = 0x10000
	setl	%bl
	cmovgel	%edi, %edx
	cmpl	$255, 164(%rsp)         # 4-byte Folded Reload
	cmovnel	%edi, %edx
	cmpl	$-1073741816, %ecx      # imm = 0xC0000008
	cmovel	%edx, %edi
	testl	%edi, %edi
	setne	%cl
	cmpl	%esi, %edi
	setg	%al
	andb	%cl, %al
	andb	%bl, %al
	movl	%r12d, %edx
	movl	%edi, 152(%rsp)         # 4-byte Spill
	subl	%edi, %edx
	movl	%r8d, %edi
	cmpl	$4095, %edx             # imm = 0xFFF
	setg	%cl
	cmpl	$4096, %edx             # imm = 0x1000
	setl	%bl
	andb	%al, %bl
	movzwl	%di, %edx
	cmpl	$65498, %edx            # imm = 0xFFDA
	sete	%dl
	setne	%sil
	testb	%bl, %dl
	movl	108(%rsp), %ebx         # 4-byte Reload
	cmovnel	%r12d, %ebx
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	movq	%r15, %r14
	je	.LBB62_17
# BB#15:                                #   in Loop: Header=BB62_8 Depth=1
	xorb	$1, %al
	orb	%sil, %cl
	orb	%al, %cl
	movq	128(%rsp), %r15         # 8-byte Reload
	jne	.LBB62_18
	jmp	.LBB62_120
	.p2align	4, 0x90
.LBB62_16:                              #   in Loop: Header=BB62_8 Depth=1
	xorl	%r11d, %r11d
	movl	108(%rsp), %ebx         # 4-byte Reload
	movl	%r14d, %r10d
	movq	%r15, %r14
.LBB62_17:                              #   in Loop: Header=BB62_8 Depth=1
	movq	128(%rsp), %r15         # 8-byte Reload
.LBB62_18:                              #   in Loop: Header=BB62_8 Depth=1
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	setne	%al
	testl	%ebx, %ebx
	sete	%dl
	movl	%ebx, 108(%rsp)         # 4-byte Spill
	cmpl	%ebx, %r12d
	setle	%cl
	movl	%edi, %esi
	andl	$65280, %esi            # imm = 0xFF00
	cmpl	$65280, %esi            # imm = 0xFF00
	setne	%bl
	movl	%r15d, %esi
	andl	$248, %esi
	cmpl	$208, %esi
	je	.LBB62_21
# BB#19:                                #   in Loop: Header=BB62_8 Depth=1
	movzbl	%r15b, %esi
	testl	%esi, %esi
	je	.LBB62_21
# BB#20:                                #   in Loop: Header=BB62_8 Depth=1
	orb	%dl, %al
	orb	%al, %cl
	orb	%cl, %bl
	je	.LBB62_118
.LBB62_21:                              #   in Loop: Header=BB62_8 Depth=1
	movl	%ebp, 160(%rsp)         # 4-byte Spill
	movzwl	%di, %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	cmpl	$16973, %eax            # imm = 0x424D
	cmovel	%r12d, %r14d
	testl	%r14d, %r14d
	je	.LBB62_25
# BB#22:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	leal	-16(%rcx), %eax
	cmpl	$12, %ecx
	movl	$-1, %r8d
	movl	%r9d, %ebx
	je	.LBB62_26
# BB#23:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$4, %ecx
	movq	120(%rsp), %r9          # 8-byte Reload
	jne	.LBB62_27
# BB#24:                                # %.thread
                                        #   in Loop: Header=BB62_8 Depth=1
	movl	%edi, %ecx
	shrl	$24, %ecx
	movl	%edi, %edx
	shrl	$8, %edx
	andl	$65280, %edx            # imm = 0xFF00
	movl	%edi, %esi
	shll	$8, %esi
	andl	$16711680, %esi         # imm = 0xFF0000
	movl	%r15d, %r10d
	shll	$24, %r10d
	orl	%ecx, %r10d
	orl	%edx, %r10d
	orl	%esi, %r10d
	jmp	.LBB62_27
	.p2align	4, 0x90
.LBB62_25:                              #   in Loop: Header=BB62_8 Depth=1
	xorl	%r14d, %r14d
	movl	$-1, %r8d
	movl	%r9d, %ebx
	movq	120(%rsp), %r9          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB62_42
.LBB62_26:                              #   in Loop: Header=BB62_8 Depth=1
	movl	%edi, %ecx
	shrl	$24, %ecx
	movl	%edi, %edx
	shrl	$8, %edx
	andl	$65280, %edx            # imm = 0xFF00
	movl	%edi, %esi
	shll	$8, %esi
	andl	$16711680, %esi         # imm = 0xFF0000
	movl	%edi, %ebp
	movl	%r15d, %edi
	shll	$24, %edi
	orl	%ecx, %edi
	orl	%edx, %edi
	orl	%esi, %edi
	movq	%rdi, 192(%rsp)         # 8-byte Spill
	movl	%ebp, %edi
	movq	120(%rsp), %r9          # 8-byte Reload
.LBB62_27:                              #   in Loop: Header=BB62_8 Depth=1
	testl	%eax, %eax
	sete	%al
	cmpl	$671088640, %edi        # imm = 0x28000000
	setne	%cl
	andb	%al, %cl
	cmovnel	%r11d, %r14d
	cmovnel	%r11d, %r10d
	cmovnel	%r11d, %ebx
	testb	%cl, %cl
	cmovnel	%r8d, %r13d
	leal	20(%r14), %eax
	cmpl	%r12d, %eax
	jne	.LBB62_29
# BB#28:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%edi, %eax
	shrl	$24, %eax
	movl	%edi, %ecx
	shrl	$8, %ecx
	andl	$65280, %ecx            # imm = 0xFF00
	movl	%edi, %edx
	shll	$8, %edx
	andl	$16711680, %edx         # imm = 0xFF0000
	movl	%r15d, %esi
	shll	$24, %esi
	orl	%eax, %esi
	orl	%ecx, %esi
	orl	%edx, %esi
	movl	%esi, 140(%rsp)         # 4-byte Spill
	cmovel	%r11d, %r14d
	cmovel	%r11d, %r10d
	cmovel	%r11d, %ebx
	cmovel	%r8d, %r13d
.LBB62_29:                              #   in Loop: Header=BB62_8 Depth=1
	leal	24(%r14), %eax
	cmpl	%r12d, %eax
	jne	.LBB62_31
# BB#30:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%edi, %eax
	shrl	$24, %eax
	movl	%edi, %ecx
	shrl	$8, %ecx
	andl	$65280, %ecx            # imm = 0xFF00
	movl	%edi, %edx
	shll	$8, %edx
	andl	$16711680, %edx         # imm = 0xFF0000
	movl	%r15d, %esi
	shll	$24, %esi
	orl	%eax, %esi
	orl	%ecx, %esi
	orl	%edx, %esi
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	cmovel	%r11d, %r14d
	cmovel	%r11d, %r10d
	cmovel	%r11d, %ebx
	cmovel	%r8d, %r13d
.LBB62_31:                              #   in Loop: Header=BB62_8 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	leal	31(%r14), %eax
	leal	27(%r14), %ecx
	cmpl	%r12d, %ecx
	cmovel	%r15d, %ebx
	cmpl	%r12d, %eax
	jne	.LBB62_33
# BB#32:                                #   in Loop: Header=BB62_8 Depth=1
	testl	%edi, %edi
	cmovnel	%r11d, %r14d
	cmovnel	%r11d, %r10d
	cmovnel	%r11d, %ebx
	movl	%edi, %eax
	negl	%eax
	sbbl	%r13d, %r13d
.LBB62_33:                              #   in Loop: Header=BB62_8 Depth=1
	cmpl	$2, 188(%rsp)           # 4-byte Folded Reload
	ja	.LBB62_36
# BB#34:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$4, %ebx
	sete	%al
	movl	%ebx, %ecx
	orl	$16, %ecx
	cmpl	$24, %ecx
	sete	%cl
	testl	%r13d, %r13d
	jne	.LBB62_36
# BB#35:                                #   in Loop: Header=BB62_8 Depth=1
	orb	%cl, %al
	jne	.LBB62_126
.LBB62_36:                              #   in Loop: Header=BB62_8 Depth=1
	cmpl	$4, %ebx
	jne	.LBB62_38
# BB#37:                                #   in Loop: Header=BB62_8 Depth=1
	testl	%r13d, %r13d
	je	.LBB62_121
.LBB62_38:                              #   in Loop: Header=BB62_8 Depth=1
	cmpl	$8, %ebx
	jne	.LBB62_40
# BB#39:                                #   in Loop: Header=BB62_8 Depth=1
	testl	%r13d, %r13d
	je	.LBB62_122
.LBB62_40:                              #   in Loop: Header=BB62_8 Depth=1
	cmpl	$24, %ebx
	jne	.LBB62_42
# BB#41:                                #   in Loop: Header=BB62_8 Depth=1
	testl	%r13d, %r13d
	je	.LBB62_124
.LBB62_42:                              #   in Loop: Header=BB62_8 Depth=1
	movl	%edi, %eax
	andl	$16777215, %eax         # imm = 0xFFFFFF
	cmpl	$5256458, %eax          # imm = 0x50350A
	cmovel	%r12d, %edx
	testl	%edx, %edx
	movl	%r13d, 28(%rsp)         # 4-byte Spill
	je	.LBB62_47
# BB#43:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	leal	1(%rdx), %eax
	cmpl	%r12d, %eax
	movl	64(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %ebx
	movl	$1, %eax
	cmovel	%eax, %ebx
	cmpl	$35, %r15d
	cmovnel	%ecx, %ebx
	movq	208(%rsp), %rbp         # 8-byte Reload
	testl	%ebp, %ebp
	movl	%r10d, 148(%rsp)        # 4-byte Spill
	movl	%edi, 144(%rsp)         # 4-byte Spill
	je	.LBB62_48
# BB#44:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$32, %r15d
	jne	.LBB62_48
# BB#45:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%ebx, %eax
	orl	96(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB62_48
# BB#46:                                # %.thread698
                                        #   in Loop: Header=BB62_8 Depth=1
	movslq	%ebp, %rax
	movb	$0, 224(%rsp,%rax)
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	224(%rsp), %rdi
	callq	strtol
	movq	120(%rsp), %r9          # 8-byte Reload
	xorl	%r11d, %r11d
	movq	%rax, %r15
	testl	%r15d, %r15d
	movl	$0, %ebp
	movl	$0, %esi
	movl	$0, %edx
	movl	$0, %edi
	movl	$0, %r10d
	movl	$0, %r8d
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	jne	.LBB62_59
	jmp	.LBB62_60
	.p2align	4, 0x90
.LBB62_47:                              #   in Loop: Header=BB62_8 Depth=1
	xorl	%edx, %edx
	movl	104(%rsp), %ebp         # 4-byte Reload
	jmp	.LBB62_70
	.p2align	4, 0x90
.LBB62_48:                              #   in Loop: Header=BB62_8 Depth=1
	cmpl	$10, %r15d
	sete	%sil
	testl	%ebp, %ebp
	je	.LBB62_52
# BB#49:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$10, %r15d
	jne	.LBB62_52
# BB#50:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%ebx, %eax
	orl	88(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB62_52
# BB#51:                                #   in Loop: Header=BB62_8 Depth=1
	movslq	%ebp, %rax
	movb	$0, 224(%rsp,%rax)
	movq	%rdx, %r13
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	224(%rsp), %rdi
	callq	strtol
	xorl	%ebp, %ebp
	movq	120(%rsp), %r9          # 8-byte Reload
	xorl	%r11d, %r11d
	movq	%rax, %rcx
	movb	$1, %al
	movq	%rax, 64(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	movl	$0, %esi
	movl	$0, %edx
	movl	$0, %edi
	movl	$0, %r10d
	movl	$0, %r8d
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	jne	.LBB62_59
	jmp	.LBB62_60
.LBB62_52:                              #   in Loop: Header=BB62_8 Depth=1
	testl	%ebp, %ebp
	je	.LBB62_56
# BB#53:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$10, %r15d
	jne	.LBB62_56
# BB#54:                                #   in Loop: Header=BB62_8 Depth=1
	movq	%rdx, %r13
	movl	%ebx, %ecx
	movq	112(%rsp), %rax         # 8-byte Reload
	orl	%eax, %ecx
	jne	.LBB62_57
# BB#55:                                #   in Loop: Header=BB62_8 Depth=1
	movslq	%ebp, %rax
	movb	$0, 224(%rsp,%rax)
	xorl	%esi, %esi
	movl	$10, %edx
	leaq	224(%rsp), %rdi
	callq	strtol
	xorl	%ebp, %ebp
	movq	120(%rsp), %r9          # 8-byte Reload
	xorl	%r11d, %r11d
	movb	$1, %cl
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	jmp	.LBB62_58
.LBB62_56:                              #   in Loop: Header=BB62_8 Depth=1
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rdx, %r13
	jmp	.LBB62_58
.LBB62_57:                              #   in Loop: Header=BB62_8 Depth=1
	movq	%rsi, 64(%rsp)          # 8-byte Spill
.LBB62_58:                              # %.thread700
                                        #   in Loop: Header=BB62_8 Depth=1
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
.LBB62_59:                              # %.thread700
                                        #   in Loop: Header=BB62_8 Depth=1
	testl	%ebx, %ebx
	movl	%eax, %esi
	movl	%ebp, %edx
	movl	%r13d, %edi
	movl	%r15d, %r10d
	movl	%ecx, %r8d
	jne	.LBB62_61
.LBB62_60:                              # %.thread700.thread
                                        #   in Loop: Header=BB62_8 Depth=1
	movslq	%edx, %rbx
	incl	%edx
	movq	128(%rsp), %rax         # 8-byte Reload
	movb	%al, 224(%rsp,%rbx)
	xorl	%ebx, %ebx
	movl	%esi, %eax
	movl	%edi, %r13d
	movl	%edx, %ebp
.LBB62_61:                              #   in Loop: Header=BB62_8 Depth=1
	cmpl	$31, %ebp
	movl	%r13d, %edx
	cmovgl	%r11d, %edx
	cmovgl	%r11d, %ebx
	movl	%r10d, %esi
	cmovgl	%r11d, %esi
	movl	%r8d, %ecx
	cmovgl	%r11d, %ecx
	cmovgl	%r11d, %eax
	testl	%eax, %eax
	je	.LBB62_65
# BB#62:                                #   in Loop: Header=BB62_8 Depth=1
	testl	%ecx, %ecx
	je	.LBB62_65
# BB#63:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$6, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB62_65
# BB#64:                                #   in Loop: Header=BB62_8 Depth=1
	testl	%esi, %esi
	jne	.LBB62_123
	.p2align	4, 0x90
.LBB62_65:                              #   in Loop: Header=BB62_8 Depth=1
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	testl	%ebx, %ebx
	movl	%ebx, %ecx
	cmovnel	%r11d, %ecx
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	cmovel	%ebx, %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	cmpl	$31, %ebp
	cmovgl	%r11d, %ebp
	movq	%rbp, 208(%rsp)         # 8-byte Spill
	movq	%rax, 112(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	je	.LBB62_69
# BB#66:                                #   in Loop: Header=BB62_8 Depth=1
	testl	%esi, %esi
	movl	$-1, %eax
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	148(%rsp), %r10d        # 4-byte Reload
	movl	144(%rsp), %edi         # 4-byte Reload
	movl	104(%rsp), %ebp         # 4-byte Reload
	movl	28(%rsp), %r13d         # 4-byte Reload
	movq	128(%rsp), %r15         # 8-byte Reload
	je	.LBB62_68
# BB#67:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$0, 88(%rsp)            # 4-byte Folded Reload
	jne	.LBB62_125
.LBB62_68:                              #   in Loop: Header=BB62_8 Depth=1
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	jmp	.LBB62_70
	.p2align	4, 0x90
.LBB62_69:                              #   in Loop: Header=BB62_8 Depth=1
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	$-1, %eax
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	148(%rsp), %r10d        # 4-byte Reload
	movl	144(%rsp), %edi         # 4-byte Reload
	movl	104(%rsp), %ebp         # 4-byte Reload
	movl	28(%rsp), %r13d         # 4-byte Reload
	movq	128(%rsp), %r15         # 8-byte Reload
.LBB62_70:                              #   in Loop: Header=BB62_8 Depth=1
	cmpl	$474, 216(%rsp)         # 4-byte Folded Reload
                                        # imm = 0x1DA
	cmovel	%r12d, %r9d
	testl	%r9d, %r9d
	je	.LBB62_75
# BB#71:                                #   in Loop: Header=BB62_8 Depth=1
	leal	1(%r9), %eax
	cmpl	%r12d, %eax
	jne	.LBB62_73
# BB#72:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%r15d, %eax
	orl	$1, %eax
	cmpl	$1, %eax
	cmovnel	%r11d, %r9d
	movl	$-1, %eax
	cmovel	%r15d, %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	cmovnel	%r11d, %ebp
	movl	4(%rsp), %eax           # 4-byte Reload
	cmovnel	%r11d, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB62_73:                              #   in Loop: Header=BB62_8 Depth=1
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	leal	2(%r9), %eax
	movl	%r10d, %r13d
	cmpl	%r12d, %eax
	jne	.LBB62_76
# BB#74:                                #   in Loop: Header=BB62_8 Depth=1
	leal	-1(%r15), %eax
	cmpl	$2, %eax
	cmovael	%r11d, %r9d
	movl	80(%rsp), %edx          # 4-byte Reload
	movl	$-1, %r8d
	cmovael	%r8d, %edx
	cmovael	%r11d, %ebp
	movl	4(%rsp), %esi           # 4-byte Reload
	cmovael	%r11d, %esi
	jmp	.LBB62_77
	.p2align	4, 0x90
.LBB62_75:                              #   in Loop: Header=BB62_8 Depth=1
	xorl	%r9d, %r9d
	movl	80(%rsp), %r8d          # 4-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	4(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB62_99
	.p2align	4, 0x90
.LBB62_76:                              #   in Loop: Header=BB62_8 Depth=1
	movl	80(%rsp), %edx          # 4-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	$-1, %r8d
.LBB62_77:                              #   in Loop: Header=BB62_8 Depth=1
	leal	4(%r9), %eax
	cmpl	%r12d, %eax
	setne	%al
	movq	216(%rsp), %r10         # 8-byte Reload
	leal	-1(%r10), %ecx
	cmpl	$3, %ecx
	setb	%cl
	orb	%al, %cl
	cmovel	%r11d, %r9d
	cmovel	%r8d, %edx
	cmovel	%r11d, %ebp
	cmovel	%r11d, %esi
	leal	6(%r9), %eax
	cmpl	%r12d, %eax
	movl	%edx, %r8d
	movl	%esi, %r11d
	jne	.LBB62_79
# BB#78:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$1, %r10d
	sbbl	%eax, %eax
	orl	%eax, %r8d
	testl	%r10d, %r10d
	cmovel	%r10d, %r9d
	cmovel	%r10d, %ebp
	movl	%r10d, %r11d
.LBB62_79:                              #   in Loop: Header=BB62_8 Depth=1
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	%r10, %rdx
	leal	8(%r9), %eax
	cmpl	%r12d, %eax
	movl	%r13d, %r10d
	jne	.LBB62_82
# BB#80:                                #   in Loop: Header=BB62_8 Depth=1
	testl	%edx, %edx
	movl	28(%rsp), %r13d         # 4-byte Reload
	je	.LBB62_83
# BB#81:                                #   in Loop: Header=BB62_8 Depth=1
	movl	%r11d, %eax
	imull	%edx, %eax
	addl	$512, %eax              # imm = 0x200
	movl	%eax, 60(%rsp)          # 4-byte Spill
	jmp	.LBB62_84
	.p2align	4, 0x90
.LBB62_82:                              #   in Loop: Header=BB62_8 Depth=1
	movl	28(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB62_84
.LBB62_83:                              #   in Loop: Header=BB62_8 Depth=1
	movl	$-1, %r8d
	xorl	%r9d, %r9d
	xorl	%ebp, %ebp
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB62_84:                              #   in Loop: Header=BB62_8 Depth=1
	leal	10(%r9), %eax
	cmpl	%r12d, %eax
	jne	.LBB62_88
# BB#85:                                #   in Loop: Header=BB62_8 Depth=1
	movzwl	%di, %eax
	cmpl	$4, %eax
	ja	.LBB62_87
# BB#86:                                #   in Loop: Header=BB62_8 Depth=1
	movl	$26, %eax
	btl	%edi, %eax
	jb	.LBB62_89
.LBB62_87:                              #   in Loop: Header=BB62_8 Depth=1
	movl	$-1, %r8d
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	jne	.LBB62_90
	jmp	.LBB62_97
	.p2align	4, 0x90
.LBB62_88:                              #   in Loop: Header=BB62_8 Depth=1
	movl	%ebp, %edx
.LBB62_89:                              #   in Loop: Header=BB62_8 Depth=1
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	je	.LBB62_97
.LBB62_90:                              #   in Loop: Header=BB62_8 Depth=1
	movl	%r12d, %eax
	subl	%r9d, %eax
	testl	%eax, %eax
	jle	.LBB62_98
# BB#91:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	60(%rsp), %eax          # 4-byte Folded Reload
	jle	.LBB62_98
# BB#92:                                #   in Loop: Header=BB62_8 Depth=1
	testl	%r8d, %r8d
	jne	.LBB62_95
# BB#93:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$7, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB62_95
# BB#94:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$1, %edx
	je	.LBB62_127
.LBB62_95:                              #   in Loop: Header=BB62_8 Depth=1
	testl	%r8d, %r8d
	jne	.LBB62_98
# BB#96:                                #   in Loop: Header=BB62_8 Depth=1
	cmpl	$1, %edx
	jne	.LBB62_98
	jmp	.LBB62_136
.LBB62_97:                              #   in Loop: Header=BB62_8 Depth=1
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
.LBB62_98:                              #   in Loop: Header=BB62_8 Depth=1
	movl	%edx, %ebp
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB62_99:                              #   in Loop: Header=BB62_8 Depth=1
	movl	164(%rsp), %eax         # 4-byte Reload
	andl	$254, %eax
	cmpl	$232, %eax
	jne	.LBB62_114
# BB#100:                               #   in Loop: Header=BB62_8 Depth=1
	incl	%r15d
	testb	$-2, %r15b
	jne	.LBB62_114
# BB#101:                               #   in Loop: Header=BB62_8 Depth=1
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	%edi, %eax
	shrl	$24, %eax
	leal	(%r12,%rax), %ecx
	movzbl	%cl, %ecx
	movq	168(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx,%rcx,4), %esi
	xorl	%edx, %edx
	cmpl	$6, %esi
	jl	.LBB62_113
# BB#102:                               #   in Loop: Header=BB62_8 Depth=1
	movl	%r11d, 4(%rsp)          # 4-byte Spill
	movl	%ebp, %r15d
	movl	%edi, %r11d
	movl	%r12d, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	subl	(%rdi,%rax,4), %ebp
	movl	%r12d, %edi
	subl	%esi, %edi
	cmpl	%ebp, %edi
	jge	.LBB62_108
# BB#103:                               #   in Loop: Header=BB62_8 Depth=1
	cmpl	$4095, %edi             # imm = 0xFFF
	jg	.LBB62_108
# BB#104:                               #   in Loop: Header=BB62_8 Depth=1
	movq	176(%rsp), %r13         # 8-byte Reload
	leal	1(%r13), %edx
	movl	76(%rsp), %ebp          # 4-byte Reload
	cmpl	%esi, %ebp
	movl	%ebp, %edi
	cmovgl	%esi, %edi
	testl	%ebp, %ebp
	cmovel	%esi, %edi
	movl	%edi, 76(%rsp)          # 4-byte Spill
	cmpl	$8, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB62_109
# BB#105:                               #   in Loop: Header=BB62_8 Depth=1
	cmpl	$3, %r13d
	movl	%r11d, %edi
	movl	%r15d, %ebp
	jl	.LBB62_110
# BB#106:                               #   in Loop: Header=BB62_8 Depth=1
	cmpl	$5, 76(%rsp)            # 4-byte Folded Reload
	movl	28(%rsp), %r13d         # 4-byte Reload
	movl	4(%rsp), %r11d          # 4-byte Reload
	jg	.LBB62_137
# BB#107:                               #   in Loop: Header=BB62_8 Depth=1
	movl	%r12d, %esi
	movl	%esi, 56(%rsp)          # 4-byte Spill
	jmp	.LBB62_113
.LBB62_108:                             #   in Loop: Header=BB62_8 Depth=1
	movl	%r11d, %edi
	movl	%r15d, %ebp
	jmp	.LBB62_112
.LBB62_109:                             #   in Loop: Header=BB62_8 Depth=1
	movl	%r12d, %esi
	movl	%esi, 56(%rsp)          # 4-byte Spill
	movl	%r11d, %edi
	movl	%r15d, %ebp
	jmp	.LBB62_111
.LBB62_110:                             #   in Loop: Header=BB62_8 Depth=1
	movl	%r12d, %esi
	movl	%esi, 56(%rsp)          # 4-byte Spill
.LBB62_111:                             # %.thread716
                                        #   in Loop: Header=BB62_8 Depth=1
	movl	28(%rsp), %r13d         # 4-byte Reload
.LBB62_112:                             # %.thread716
                                        #   in Loop: Header=BB62_8 Depth=1
	movl	4(%rsp), %r11d          # 4-byte Reload
.LBB62_113:                             # %.thread716
                                        #   in Loop: Header=BB62_8 Depth=1
	movq	168(%rsp), %rsi         # 8-byte Reload
	movl	%r12d, (%rsi,%rcx,4)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%r12d, (%rcx,%rax,4)
	movl	%edx, %ecx
	movq	32(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB62_114:                             #   in Loop: Header=BB62_8 Depth=1
	cmpl	$8, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB62_116
# BB#115:                               #   in Loop: Header=BB62_8 Depth=1
	movl	%r12d, %eax
	subl	56(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$4097, %eax             # imm = 0x1001
	jge	.LBB62_119
.LBB62_116:                             #   in Loop: Header=BB62_8 Depth=1
	incl	%r12d
	cmpl	156(%rsp), %r12d        # 4-byte Folded Reload
	jl	.LBB62_8
	jmp	.LBB62_130
.LBB62_117:
	movl	$-1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB62_130
.LBB62_118:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB62_130
.LBB62_119:
	movslq	56(%rsp), %rsi          # 4-byte Folded Reload
	jmp	.LBB62_128
.LBB62_120:
	movslq	84(%rsp), %rax          # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	-3(%rcx,%rax), %rsi
	xorl	%edx, %edx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fseek
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB62_130
.LBB62_121:
	movslq	%r14d, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	-1(%rcx,%rax), %rsi
	xorl	%edx, %edx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fseek
	movl	$2, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB62_130
.LBB62_122:
	movslq	%r14d, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	-1(%rcx,%rax), %rsi
	xorl	%edx, %edx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fseek
	movl	$3, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB62_130
.LBB62_123:
	imull	%r10d, %r8d
	addl	%r13d, %r8d
	xorl	%edx, %edx
	cmpl	$31, %ebp
	movl	$0, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmovgl	%edx, %r8d
	leal	-1(%r8,%r12), %eax
	movslq	%eax, %rsi
	addq	48(%rsp), %rsi          # 8-byte Folded Reload
	jmp	.LBB62_129
.LBB62_124:
	movslq	%r14d, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	-1(%rcx,%rax), %rsi
	xorl	%edx, %edx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fseek
	movl	$4, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB62_130
.LBB62_125:
	movslq	%edx, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	-2(%rcx,%rax), %rsi
	xorl	%edx, %edx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fseek
	movl	$6, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB62_130
.LBB62_126:                             # %._crit_edge
	movq	200(%rsp), %rcx         # 8-byte Reload
	imull	140(%rsp), %ecx         # 4-byte Folded Reload
	movl	%ecx, %eax
	sarl	%eax
	movq	192(%rsp), %rdx         # 8-byte Reload
	addl	%edx, %eax
	cmpl	$4, %ebx
	cmovel	%eax, %r10d
	leal	(%rcx,%rdx), %eax
	cmpl	$8, %ebx
	cmovnel	%r10d, %eax
	leal	(%rcx,%rcx,2), %ecx
	addl	%edx, %ecx
	cmpl	$24, %ebx
	cmovnel	%eax, %ecx
	movslq	%ecx, %rsi
	jmp	.LBB62_128
.LBB62_127:
	movslq	60(%rsp), %rsi          # 4-byte Folded Reload
.LBB62_128:                             # %.thread719
	addq	48(%rsp), %rsi          # 8-byte Folded Reload
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB62_129:                             # %.thread719
	xorl	%edx, %edx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fseek
.LBB62_130:
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB62_131:                             # %.thread719
	movl	$-1024, %ebx            # imm = 0xFC00
	movl	programChecker(%rip), %ecx
	addl	%ebx, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB62_133
# BB#132:
	movl	%ecx, programChecker+4(%rip)
.LBB62_133:                             # %_ZN5ArrayIiLi0EED2Ev.exit675
	movq	%rax, %rdi
	callq	free
	addl	programChecker(%rip), %ebx
	movl	%ebx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ebx
	jle	.LBB62_135
# BB#134:
	movl	%ebx, programChecker+4(%rip)
.LBB62_135:                             # %_ZN5ArrayIiLi0EED2Ev.exit674
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	8(%rsp), %rax           # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB62_136:
	movslq	%r9d, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	-1(%rcx,%rax), %rsi
	xorl	%edx, %edx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fseek
	movl	$7, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB62_130
.LBB62_137:
	movslq	76(%rsp), %rax          # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	-5(%rcx,%rax), %rsi
	xorl	%edx, %edx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	fseek
	movl	$8, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB62_130
.LBB62_138:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.LBB62_139:
.Ltmp270:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp271:
# BB#140:                               # %.noexc
.LBB62_141:
.Ltmp272:
	movq	%rax, %rbx
	movl	$-1024, %eax            # imm = 0xFC00
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB62_143
# BB#142:
	movl	%eax, programChecker+4(%rip)
.LBB62_143:                             # %_ZN5ArrayIiLi0EED2Ev.exit
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end62:
	.size	_Z6detectP8_IO_FILEi8Filetype, .Lfunc_end62-_Z6detectP8_IO_FILEi8Filetype
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table62:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin19-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp270-.Lfunc_begin19 #   Call between .Lfunc_begin19 and .Ltmp270
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp270-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp271-.Ltmp270       #   Call between .Ltmp270 and .Ltmp271
	.long	.Ltmp272-.Lfunc_begin19 #     jumps to .Ltmp272
	.byte	0                       #   On action: cleanup
	.long	.Ltmp271-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Lfunc_end62-.Ltmp271   #   Call between .Ltmp271 and .Lfunc_end62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z14encode_defaultP8_IO_FILES0_i
	.p2align	4, 0x90
	.type	_Z14encode_defaultP8_IO_FILES0_i,@function
_Z14encode_defaultP8_IO_FILES0_i:       # @_Z14encode_defaultP8_IO_FILES0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi290:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi291:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi292:
	.cfi_def_cfa_offset 32
.Lcfi293:
	.cfi_offset %rbx, -32
.Lcfi294:
	.cfi_offset %r14, -24
.Lcfi295:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ebx, %ebx
	je	.LBB63_2
	.p2align	4, 0x90
.LBB63_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	decl	%ebx
	jne	.LBB63_1
.LBB63_2:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end63:
	.size	_Z14encode_defaultP8_IO_FILES0_i, .Lfunc_end63-_Z14encode_defaultP8_IO_FILES0_i
	.cfi_endproc

	.globl	_Z14decode_defaultR7Encoder
	.p2align	4, 0x90
	.type	_Z14decode_defaultR7Encoder,@function
_Z14decode_defaultR7Encoder:            # @_Z14decode_defaultR7Encoder
	.cfi_startproc
# BB#0:
	jmp	_ZN7Encoder10decompressEv # TAILCALL
.Lfunc_end64:
	.size	_Z14decode_defaultR7Encoder, .Lfunc_end64-_Z14decode_defaultR7Encoder
	.cfi_endproc

	.section	.text._ZN7Encoder10decompressEv,"axG",@progbits,_ZN7Encoder10decompressEv,comdat
	.weak	_ZN7Encoder10decompressEv
	.p2align	4, 0x90
	.type	_ZN7Encoder10decompressEv,@function
_ZN7Encoder10decompressEv:              # @_ZN7Encoder10decompressEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi296:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi297:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi298:
	.cfi_def_cfa_offset 32
.Lcfi299:
	.cfi_offset %rbx, -24
.Lcfi300:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpl	$0, 4(%rbx)
	je	.LBB65_1
# BB#2:
	cmpl	$0, level(%rip)
	je	.LBB65_3
# BB#5:                                 # %.loopexit
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN7Encoder4codeEi
	movl	%eax, %r14d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN7Encoder4codeEi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r14,2), %r14d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN7Encoder4codeEi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r14,2), %r14d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN7Encoder4codeEi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r14,2), %r14d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN7Encoder4codeEi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r14,2), %r14d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN7Encoder4codeEi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r14,2), %r14d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN7Encoder4codeEi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r14,2), %r14d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN7Encoder4codeEi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%r14,2), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB65_1:
	addq	$32, %rbx
	jmp	.LBB65_4
.LBB65_3:
	addq	$8, %rbx
.LBB65_4:                               # %.sink.split
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_getc                # TAILCALL
.Lfunc_end65:
	.size	_ZN7Encoder10decompressEv, .Lfunc_end65-_ZN7Encoder10decompressEv
	.cfi_endproc

	.text
	.globl	_Z11encode_jpegP8_IO_FILES0_i
	.p2align	4, 0x90
	.type	_Z11encode_jpegP8_IO_FILES0_i,@function
_Z11encode_jpegP8_IO_FILES0_i:          # @_Z11encode_jpegP8_IO_FILES0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi301:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi302:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi303:
	.cfi_def_cfa_offset 32
.Lcfi304:
	.cfi_offset %rbx, -32
.Lcfi305:
	.cfi_offset %r14, -24
.Lcfi306:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ebx, %ebx
	je	.LBB66_2
	.p2align	4, 0x90
.LBB66_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	decl	%ebx
	jne	.LBB66_1
.LBB66_2:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end66:
	.size	_Z11encode_jpegP8_IO_FILES0_i, .Lfunc_end66-_Z11encode_jpegP8_IO_FILES0_i
	.cfi_endproc

	.globl	_Z11decode_jpegR7Encoder
	.p2align	4, 0x90
	.type	_Z11decode_jpegR7Encoder,@function
_Z11decode_jpegR7Encoder:               # @_Z11decode_jpegR7Encoder
	.cfi_startproc
# BB#0:
	jmp	_ZN7Encoder10decompressEv # TAILCALL
.Lfunc_end67:
	.size	_Z11decode_jpegR7Encoder, .Lfunc_end67-_Z11decode_jpegR7Encoder
	.cfi_endproc

	.globl	_Z10encode_bmpP8_IO_FILES0_i
	.p2align	4, 0x90
	.type	_Z10encode_bmpP8_IO_FILES0_i,@function
_Z10encode_bmpP8_IO_FILES0_i:           # @_Z10encode_bmpP8_IO_FILES0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi307:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi308:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi309:
	.cfi_def_cfa_offset 32
.Lcfi310:
	.cfi_offset %rbx, -32
.Lcfi311:
	.cfi_offset %r14, -24
.Lcfi312:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ebx, %ebx
	je	.LBB68_2
	.p2align	4, 0x90
.LBB68_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	decl	%ebx
	jne	.LBB68_1
.LBB68_2:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end68:
	.size	_Z10encode_bmpP8_IO_FILES0_i, .Lfunc_end68-_Z10encode_bmpP8_IO_FILES0_i
	.cfi_endproc

	.globl	_Z10decode_bmpR7Encoder
	.p2align	4, 0x90
	.type	_Z10decode_bmpR7Encoder,@function
_Z10decode_bmpR7Encoder:                # @_Z10decode_bmpR7Encoder
	.cfi_startproc
# BB#0:
	jmp	_ZN7Encoder10decompressEv # TAILCALL
.Lfunc_end69:
	.size	_Z10decode_bmpR7Encoder, .Lfunc_end69-_Z10decode_bmpR7Encoder
	.cfi_endproc

	.globl	_Z10encode_pgmP8_IO_FILES0_i
	.p2align	4, 0x90
	.type	_Z10encode_pgmP8_IO_FILES0_i,@function
_Z10encode_pgmP8_IO_FILES0_i:           # @_Z10encode_pgmP8_IO_FILES0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi313:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi314:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi315:
	.cfi_def_cfa_offset 32
.Lcfi316:
	.cfi_offset %rbx, -32
.Lcfi317:
	.cfi_offset %r14, -24
.Lcfi318:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ebx, %ebx
	je	.LBB70_2
	.p2align	4, 0x90
.LBB70_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	decl	%ebx
	jne	.LBB70_1
.LBB70_2:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end70:
	.size	_Z10encode_pgmP8_IO_FILES0_i, .Lfunc_end70-_Z10encode_pgmP8_IO_FILES0_i
	.cfi_endproc

	.globl	_Z10decode_pgmR7Encoder
	.p2align	4, 0x90
	.type	_Z10decode_pgmR7Encoder,@function
_Z10decode_pgmR7Encoder:                # @_Z10decode_pgmR7Encoder
	.cfi_startproc
# BB#0:
	jmp	_ZN7Encoder10decompressEv # TAILCALL
.Lfunc_end71:
	.size	_Z10decode_pgmR7Encoder, .Lfunc_end71-_Z10decode_pgmR7Encoder
	.cfi_endproc

	.globl	_Z10encode_rgbP8_IO_FILES0_i
	.p2align	4, 0x90
	.type	_Z10encode_rgbP8_IO_FILES0_i,@function
_Z10encode_rgbP8_IO_FILES0_i:           # @_Z10encode_rgbP8_IO_FILES0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi319:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi320:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi321:
	.cfi_def_cfa_offset 32
.Lcfi322:
	.cfi_offset %rbx, -32
.Lcfi323:
	.cfi_offset %r14, -24
.Lcfi324:
	.cfi_offset %r15, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ebx, %ebx
	je	.LBB72_2
	.p2align	4, 0x90
.LBB72_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	decl	%ebx
	jne	.LBB72_1
.LBB72_2:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end72:
	.size	_Z10encode_rgbP8_IO_FILES0_i, .Lfunc_end72-_Z10encode_rgbP8_IO_FILES0_i
	.cfi_endproc

	.globl	_Z10decode_rgbR7Encoder
	.p2align	4, 0x90
	.type	_Z10decode_rgbR7Encoder,@function
_Z10decode_rgbR7Encoder:                # @_Z10decode_rgbR7Encoder
	.cfi_startproc
# BB#0:
	jmp	_ZN7Encoder10decompressEv # TAILCALL
.Lfunc_end73:
	.size	_Z10decode_rgbR7Encoder, .Lfunc_end73-_Z10decode_rgbR7Encoder
	.cfi_endproc

	.globl	_Z10encode_exeP8_IO_FILES0_ii
	.p2align	4, 0x90
	.type	_Z10encode_exeP8_IO_FILES0_ii,@function
_Z10encode_exeP8_IO_FILES0_ii:          # @_Z10encode_exeP8_IO_FILES0_ii
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%rbp
.Lcfi325:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi326:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi327:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi328:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi329:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi330:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi331:
	.cfi_def_cfa_offset 112
.Lcfi332:
	.cfi_offset %rbx, -56
.Lcfi333:
	.cfi_offset %r12, -48
.Lcfi334:
	.cfi_offset %r13, -40
.Lcfi335:
	.cfi_offset %r14, -32
.Lcfi336:
	.cfi_offset %r15, -24
.Lcfi337:
	.cfi_offset %rbp, -16
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%edx, %ebp
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	$65536, %eax            # imm = 0x10000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB74_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB74_2:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movl	$65536, %edi            # imm = 0x10000
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB74_21
# BB#3:                                 # %_ZN5ArrayIhLi0EEC2Ei.exit
	movl	%ebp, %edx
	sarl	$24, %edx
	movl	%ebp, %ecx
	sarl	$16, %ecx
	movl	%ebp, %r8d
	sarl	$8, %r8d
	xorl	%r12d, %r12d
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movl	%ebp, %r9d
	callq	fprintf
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	%r9d, %edx
	sarl	$24, %edx
	movl	%r9d, %ecx
	sarl	$16, %ecx
	movl	%r9d, %r8d
	sarl	$8, %r8d
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	testl	%ebp, %ebp
	jle	.LBB74_13
# BB#4:                                 # %.lr.ph93.preheader
	movl	%ebp, %eax
	notl	%eax
	decl	12(%rsp)                # 4-byte Folded Spill
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB74_5:                               # %.lr.ph93
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB74_8 Depth 2
	cmpl	$-65538, %eax           # imm = 0xFFFEFFFE
	movl	$-65537, %r15d          # imm = 0xFFFEFFFF
	movl	%eax, 20(%rsp)          # 4-byte Spill
	cmovgl	%eax, %r15d
	movq	%r12, 48(%rsp)          # 8-byte Spill
	subl	%r12d, %ebp
	cmpl	$65537, %ebp            # imm = 0x10001
	movl	$65536, %eax            # imm = 0x10000
	cmovgel	%eax, %ebp
	movslq	%ebp, %r13
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	callq	fread
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	%r13d, %eax
	jne	.LBB74_16
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB74_5 Depth=1
	decl	%ebp
	cmpl	$4, %ebp
	jl	.LBB74_12
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB74_5 Depth=1
	movl	$-6, %r13d
	subl	%r15d, %r13d
	movl	$-5, %eax
	subl	%r15d, %eax
	movl	$-4, %r9d
	subl	%r15d, %r9d
	movl	$-3, %r10d
	subl	%r15d, %r10d
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r11d
	subl	%r15d, %r11d
	movl	$-2, %ecx
	subl	%r15d, %ecx
	movslq	%ecx, %r12
	leaq	(%rbx,%r12), %r15
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB74_8:                               #   Parent Loop BB74_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r13,%rdi), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rbx,%rcx), %ecx
	andb	$-2, %cl
	cmpb	$-24, %cl
	jne	.LBB74_11
# BB#9:                                 #   in Loop: Header=BB74_8 Depth=2
	movzbl	(%r15,%rdi), %esi
	movl	%esi, %ecx
	incb	%cl
	cmpb	$1, %cl
	ja	.LBB74_11
# BB#10:                                #   in Loop: Header=BB74_8 Depth=2
	leal	(%rax,%rdi), %ecx
	movslq	%ecx, %rbp
	movzbl	(%rbx,%rbp), %edx
	leal	(%r9,%rdi), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rbx,%rcx), %r14d
	shll	$8, %r14d
	orl	%edx, %r14d
	leal	(%r10,%rdi), %edx
	movslq	%edx, %r8
	movzbl	(%rbx,%r8), %edx
	shll	$16, %edx
	orl	%r14d, %edx
	shll	$24, %esi
	orl	%edx, %esi
	leaq	(%r11,%rdi), %rdx
	addq	%rsi, %rdx
	movl	%edx, %esi
	shll	$7, %esi
	sarl	$31, %esi
	movb	%sil, (%r15,%rdi)
	movl	%edx, %esi
	shrl	$16, %esi
	movb	%sil, (%rbx,%r8)
	movb	%dh, (%rbx,%rcx)  # NOREX
	movb	%dl, (%rbx,%rbp)
.LBB74_11:                              # %.backedge
                                        #   in Loop: Header=BB74_8 Depth=2
	leaq	-1(%r12,%rdi), %rcx
	decq	%rdi
	cmpq	$3, %rcx
	jg	.LBB74_8
.LBB74_12:                              # %._crit_edge
                                        #   in Loop: Header=BB74_5 Depth=1
	movslq	40(%rsp), %rdx          # 4-byte Folded Reload
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	movq	48(%rsp), %r12          # 8-byte Reload
	addl	$65536, %r12d           # imm = 0x10000
	movl	20(%rsp), %eax          # 4-byte Reload
	addl	$65536, %eax            # imm = 0x10000
	addl	$65536, 12(%rsp)        # 4-byte Folded Spill
                                        # imm = 0x10000
	movl	16(%rsp), %ebp          # 4-byte Reload
	cmpl	%ebp, %r12d
	jl	.LBB74_5
.LBB74_13:                              # %._crit_edge94
	movl	$-65536, %eax           # imm = 0xFFFF0000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB74_15
# BB#14:
	movl	%eax, programChecker+4(%rip)
.LBB74_15:                              # %_ZN5ArrayIhLi0EED2Ev.exit
	movq	%rbx, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB74_16:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.18, (%rax)
.Ltmp273:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp274:
# BB#17:                                # %.noexc
.LBB74_21:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.LBB74_18:
.Ltmp275:
	movq	%rax, %rbp
	movl	$-65536, %eax           # imm = 0xFFFF0000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB74_20
# BB#19:
	movl	%eax, programChecker+4(%rip)
.LBB74_20:                              # %_ZN5ArrayIhLi0EED2Ev.exit65
	movq	%rbx, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end74:
	.size	_Z10encode_exeP8_IO_FILES0_ii, .Lfunc_end74-_Z10encode_exeP8_IO_FILES0_ii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table74:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin20-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp273-.Lfunc_begin20 #   Call between .Lfunc_begin20 and .Ltmp273
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp273-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp274-.Ltmp273       #   Call between .Ltmp273 and .Ltmp274
	.long	.Ltmp275-.Lfunc_begin20 #     jumps to .Ltmp275
	.byte	0                       #   On action: cleanup
	.long	.Ltmp274-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Lfunc_end74-.Ltmp274   #   Call between .Ltmp274 and .Lfunc_end74
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z10decode_exeR7Encoder
	.p2align	4, 0x90
	.type	_Z10decode_exeR7Encoder,@function
_Z10decode_exeR7Encoder:                # @_Z10decode_exeR7Encoder
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi338:
	.cfi_def_cfa_offset 16
.Lcfi339:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	jmp	.LBB75_1
	.p2align	4, 0x90
.LBB75_13:                              # %.lr.ph19
                                        #   in Loop: Header=BB75_1 Depth=1
	movl	$0, _ZZ10decode_exeR7EncoderE6offset(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	shll	$24, %eax
	movl	%eax, _ZZ10decode_exeR7EncoderE4size(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	shll	$16, %eax
	orl	%eax, _ZZ10decode_exeR7EncoderE4size(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	shll	$8, %eax
	orl	%eax, _ZZ10decode_exeR7EncoderE4size(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	orl	%eax, _ZZ10decode_exeR7EncoderE4size(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	shll	$24, %eax
	movl	%eax, _ZZ10decode_exeR7EncoderE5begin(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	shll	$16, %eax
	orl	%eax, _ZZ10decode_exeR7EncoderE5begin(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	shll	$8, %eax
	orl	%eax, _ZZ10decode_exeR7EncoderE5begin(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	orl	%eax, _ZZ10decode_exeR7EncoderE5begin(%rip)
.LBB75_1:                               # =>This Inner Loop Header: Depth=1
	movl	_ZZ10decode_exeR7EncoderE6offset(%rip), %ecx
	movl	_ZZ10decode_exeR7EncoderE4size(%rip), %edx
	movl	_ZZ10decode_exeR7EncoderE1q(%rip), %eax
	cmpl	%edx, %ecx
	jne	.LBB75_3
# BB#2:                                 #   in Loop: Header=BB75_1 Depth=1
	testl	%eax, %eax
	je	.LBB75_13
.LBB75_3:                               # %.preheader
	cmpl	$4, %eax
	jg	.LBB75_7
# BB#4:                                 # %.preheader
	cmpl	%edx, %ecx
	jge	.LBB75_7
	.p2align	4, 0x90
.LBB75_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	_ZZ10decode_exeR7EncoderE1c(%rip), %eax
	movl	%eax, _ZZ10decode_exeR7EncoderE1c+1(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	movb	%al, _ZZ10decode_exeR7EncoderE1c(%rip)
	movl	_ZZ10decode_exeR7EncoderE1q(%rip), %eax
	incl	%eax
	movl	%eax, _ZZ10decode_exeR7EncoderE1q(%rip)
	movl	_ZZ10decode_exeR7EncoderE6offset(%rip), %ecx
	incl	%ecx
	movl	%ecx, _ZZ10decode_exeR7EncoderE6offset(%rip)
	cmpl	$4, %eax
	jg	.LBB75_7
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB75_5 Depth=1
	cmpl	_ZZ10decode_exeR7EncoderE4size(%rip), %ecx
	jl	.LBB75_5
.LBB75_7:                               # %._crit_edge
	cmpl	$5, %eax
	jne	.LBB75_12
# BB#8:
	movb	_ZZ10decode_exeR7EncoderE1c+4(%rip), %dl
	andb	$-2, %dl
	cmpb	$-24, %dl
	jne	.LBB75_12
# BB#9:
	movzbl	_ZZ10decode_exeR7EncoderE1c(%rip), %edx
	movl	%edx, %ebx
	incb	%bl
	cmpb	$1, %bl
	ja	.LBB75_12
# BB#10:
	leal	-1(%rcx), %esi
	leal	-5(%rcx), %edi
	xorl	%esi, %edi
	cmpl	$65535, %edi            # imm = 0xFFFF
	ja	.LBB75_12
# BB#11:
	movzbl	_ZZ10decode_exeR7EncoderE1c+3(%rip), %esi
	movzbl	_ZZ10decode_exeR7EncoderE1c+2(%rip), %edi
	shll	$8, %edi
	orl	%esi, %edi
	movzbl	_ZZ10decode_exeR7EncoderE1c+1(%rip), %esi
	shll	$16, %esi
	orl	%edi, %esi
	shll	$24, %edx
	orl	%esi, %edx
	subl	%ecx, %edx
	subl	_ZZ10decode_exeR7EncoderE5begin(%rip), %edx
	movl	%edx, %ecx
	shll	$7, %ecx
	movb	%dl, _ZZ10decode_exeR7EncoderE1c+3(%rip)
	movb	%dh, _ZZ10decode_exeR7EncoderE1c+2(%rip)  # NOREX
	shrl	$16, %edx
	movb	%dl, _ZZ10decode_exeR7EncoderE1c+1(%rip)
	sarl	$31, %ecx
	movb	%cl, _ZZ10decode_exeR7EncoderE1c(%rip)
.LBB75_12:
	movslq	%eax, %rcx
	decl	%eax
	movl	%eax, _ZZ10decode_exeR7EncoderE1q(%rip)
	movzbl	_ZZ10decode_exeR7EncoderE1c-1(%rcx), %eax
	popq	%rbx
	retq
.Lfunc_end75:
	.size	_Z10decode_exeR7Encoder, .Lfunc_end75-_Z10decode_exeR7Encoder
	.cfi_endproc

	.globl	_Z6encodeP8_IO_FILES0_i
	.p2align	4, 0x90
	.type	_Z6encodeP8_IO_FILES0_i,@function
_Z6encodeP8_IO_FILES0_i:                # @_Z6encodeP8_IO_FILES0_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi340:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi341:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi342:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi343:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi344:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi345:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi346:
	.cfi_def_cfa_offset 80
.Lcfi347:
	.cfi_offset %rbx, -56
.Lcfi348:
	.cfi_offset %r12, -48
.Lcfi349:
	.cfi_offset %r13, -40
.Lcfi350:
	.cfi_offset %r14, -32
.Lcfi351:
	.cfi_offset %r15, -24
.Lcfi352:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %rbp
	callq	ftell
	movq	%rax, %r15
	testl	%ebx, %ebx
	jle	.LBB76_17
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB76_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB76_12 Depth 2
                                        #     Child Loop BB76_10 Depth 2
                                        #     Child Loop BB76_8 Depth 2
                                        #     Child Loop BB76_6 Depth 2
                                        #     Child Loop BB76_15 Depth 2
	movq	%rbp, %rdi
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	_Z6detectP8_IO_FILEi8Filetype
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	%rbp, %rdi
	callq	ftell
	movq	%rax, %rbx
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	fseek
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	%ebx, %r13d
	subl	%r15d, %r13d
	testl	%r13d, %r13d
	jle	.LBB76_16
# BB#3:                                 #   in Loop: Header=BB76_2 Depth=1
	movl	%r13d, %ecx
	shrl	$24, %ecx
	movl	%r13d, %r8d
	shrl	$16, %r8d
	movl	%r13d, %r9d
	shrl	$8, %r9d
	movl	%r13d, (%rsp)
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%r12d, %edx
	callq	fprintf
	decl	%r12d
	cmpl	$7, %r12d
	ja	.LBB76_14
# BB#4:                                 #   in Loop: Header=BB76_2 Depth=1
	jmpq	*.LJTI76_0(,%r12,8)
.LBB76_7:                               # %.lr.ph.i52.preheader
                                        #   in Loop: Header=BB76_2 Depth=1
	movl	%r13d, %ebx
	negl	%ebx
	.p2align	4, 0x90
.LBB76_8:                               # %.lr.ph.i52
                                        #   Parent Loop BB76_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	incl	%ebx
	jne	.LBB76_8
	jmp	.LBB76_16
.LBB76_14:                              # %.lr.ph.i58.preheader
                                        #   in Loop: Header=BB76_2 Depth=1
	movl	%r13d, %ebx
	negl	%ebx
	.p2align	4, 0x90
.LBB76_15:                              # %.lr.ph.i58
                                        #   Parent Loop BB76_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	incl	%ebx
	jne	.LBB76_15
	jmp	.LBB76_16
.LBB76_11:                              # %.lr.ph.i56.preheader
                                        #   in Loop: Header=BB76_2 Depth=1
	movl	%r13d, %ebx
	negl	%ebx
	.p2align	4, 0x90
.LBB76_12:                              # %.lr.ph.i56
                                        #   Parent Loop BB76_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	incl	%ebx
	jne	.LBB76_12
	jmp	.LBB76_16
.LBB76_5:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB76_2 Depth=1
	movl	%r13d, %ebx
	negl	%ebx
	.p2align	4, 0x90
.LBB76_6:                               # %.lr.ph.i
                                        #   Parent Loop BB76_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	incl	%ebx
	jne	.LBB76_6
	jmp	.LBB76_16
.LBB76_9:                               # %.lr.ph.i54.preheader
                                        #   in Loop: Header=BB76_2 Depth=1
	movl	%r13d, %ebx
	negl	%ebx
	.p2align	4, 0x90
.LBB76_10:                              # %.lr.ph.i54
                                        #   Parent Loop BB76_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	incl	%ebx
	jne	.LBB76_10
	jmp	.LBB76_16
.LBB76_13:                              #   in Loop: Header=BB76_2 Depth=1
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	%r13d, %edx
	movl	%r15d, %ecx
	callq	_Z10encode_exeP8_IO_FILES0_ii
	.p2align	4, 0x90
.LBB76_16:                              # %_Z11encode_jpegP8_IO_FILES0_i.exit
                                        #   in Loop: Header=BB76_2 Depth=1
	movl	12(%rsp), %ebx          # 4-byte Reload
	subl	%r13d, %ebx
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, %r12d
	movq	16(%rsp), %r15          # 8-byte Reload
	jg	.LBB76_2
.LBB76_17:                              # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end76:
	.size	_Z6encodeP8_IO_FILES0_i, .Lfunc_end76-_Z6encodeP8_IO_FILES0_i
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI76_0:
	.quad	.LBB76_5
	.quad	.LBB76_7
	.quad	.LBB76_7
	.quad	.LBB76_7
	.quad	.LBB76_14
	.quad	.LBB76_9
	.quad	.LBB76_11
	.quad	.LBB76_13

	.text
	.globl	_Z6decodeR7Encoder
	.p2align	4, 0x90
	.type	_Z6decodeR7Encoder,@function
_Z6decodeR7Encoder:                     # @_Z6decodeR7Encoder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi353:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi354:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi355:
	.cfi_def_cfa_offset 32
.Lcfi356:
	.cfi_offset %rbx, -24
.Lcfi357:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	_ZZ6decodeR7EncoderE3len(%rip), %eax
	testl	%eax, %eax
	jne	.LBB77_3
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB77_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	movl	%eax, _ZZ6decodeR7EncoderE4type(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	shll	$24, %eax
	movl	%eax, _ZZ6decodeR7EncoderE3len(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	shll	$16, %eax
	orl	%eax, _ZZ6decodeR7EncoderE3len(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	shll	$8, %eax
	orl	%eax, _ZZ6decodeR7EncoderE3len(%rip)
	movq	%rbx, %rdi
	callq	_ZN7Encoder10decompressEv
	orl	_ZZ6decodeR7EncoderE3len(%rip), %eax
	cmovsl	%ebp, %eax
	movl	%eax, _ZZ6decodeR7EncoderE3len(%rip)
	testl	%eax, %eax
	je	.LBB77_2
.LBB77_3:                               # %._crit_edge
	decl	%eax
	movl	%eax, _ZZ6decodeR7EncoderE3len(%rip)
	movl	_ZZ6decodeR7EncoderE4type(%rip), %eax
	decl	%eax
	cmpl	$7, %eax
	ja	.LBB77_6
# BB#4:                                 # %._crit_edge
	jmpq	*.LJTI77_0(,%rax,8)
.LBB77_6:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	_ZN7Encoder10decompressEv # TAILCALL
.LBB77_5:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	_Z10decode_exeR7Encoder # TAILCALL
.Lfunc_end77:
	.size	_Z6decodeR7Encoder, .Lfunc_end77-_Z6decodeR7Encoder
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI77_0:
	.quad	.LBB77_6
	.quad	.LBB77_6
	.quad	.LBB77_6
	.quad	.LBB77_6
	.quad	.LBB77_6
	.quad	.LBB77_6
	.quad	.LBB77_6
	.quad	.LBB77_5

	.text
	.globl	_Z11printStatusi
	.p2align	4, 0x90
	.type	_Z11printStatusi,@function
_Z11printStatusi:                       # @_Z11printStatusi
	.cfi_startproc
# BB#0:
	movl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.LBB78_2
# BB#1:
	movl	%ecx, %eax
	andl	$4095, %eax             # imm = 0xFFF
	jne	.LBB78_2
# BB#3:
	pushq	%rax
.Lcfi358:
	.cfi_def_cfa_offset 16
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	popq	%rax
	jmp	fflush                  # TAILCALL
.LBB78_2:
	retq
.Lfunc_end78:
	.size	_Z11printStatusi, .Lfunc_end78-_Z11printStatusi
	.cfi_endproc

	.globl	_Z8compressPKclR7Encoder
	.p2align	4, 0x90
	.type	_Z8compressPKclR7Encoder,@function
_Z8compressPKclR7Encoder:               # @_Z8compressPKclR7Encoder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi359:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi360:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi361:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi362:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi363:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi364:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi365:
	.cfi_def_cfa_offset 128
.Lcfi366:
	.cfi_offset %rbx, -56
.Lcfi367:
	.cfi_offset %r12, -48
.Lcfi368:
	.cfi_offset %r13, -40
.Lcfi369:
	.cfi_offset %r14, -32
.Lcfi370:
	.cfi_offset %r15, -24
.Lcfi371:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$.L.str.21, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB79_1
# BB#3:
	movq	8(%r13), %rdi
	callq	ftell
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbx, %rsi
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	printf
	movb	level(%rip), %cl
	movl	$4194304, %eax          # imm = 0x400000
	shll	%cl, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	testq	%r14, %r14
	jle	.LBB79_26
# BB#4:                                 # %.lr.ph117
	movslq	12(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB79_5
.LBB79_19:                              #   in Loop: Header=BB79_5 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	rewind
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %r12          # 8-byte Reload
	je	.LBB79_25
# BB#20:                                # %.lr.ph113
                                        #   in Loop: Header=BB79_5 Depth=1
	movl	%r12d, %ebp
	.p2align	4, 0x90
.LBB79_21:                              #   Parent Loop BB79_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebp, %ebp
	jle	.LBB79_24
# BB#22:                                #   in Loop: Header=BB79_21 Depth=2
	movl	%ebp, %eax
	andl	$4095, %eax             # imm = 0xFFF
	jne	.LBB79_24
# BB#23:                                #   in Loop: Header=BB79_21 Depth=2
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB79_24:                              # %_Z11printStatusi.exit97
                                        #   in Loop: Header=BB79_21 Depth=2
	movq	%r13, %rdi
	movl	%ebx, %esi
	callq	_ZN7Encoder8compressEi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	movl	%eax, %ebx
	incl	%ebp
	cmpl	$-1, %ebx
	jne	.LBB79_21
	jmp	.LBB79_25
	.p2align	4, 0x90
.LBB79_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB79_9 Depth 2
                                        #     Child Loop BB79_15 Depth 2
                                        #     Child Loop BB79_21 Depth 2
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpq	%r14, %rax
	movl	%eax, %ebp
	cmovgl	%r14d, %ebp
	callq	tmpfile
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB79_6
# BB#7:                                 #   in Loop: Header=BB79_5 Depth=1
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	callq	ftell
	movq	%rax, %r14
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	_Z6encodeP8_IO_FILES0_i
	movq	%rbx, %rdi
	callq	rewind
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%r13)
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movq	%r14, %rsi
	callq	fseek
	movslq	%ebp, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	testl	%eax, %eax
	jle	.LBB79_8
	.p2align	4, 0x90
.LBB79_9:                               # %.lr.ph
                                        #   Parent Loop BB79_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_Z6decodeR7Encoder
	movl	%eax, %r12d
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	%ebx, %r12d
	jne	.LBB79_11
# BB#10:                                #   in Loop: Header=BB79_9 Depth=2
	incq	%r14
	cmpq	24(%rsp), %r14          # 8-byte Folded Reload
	movl	%r12d, %ebx
	jl	.LBB79_9
	jmp	.LBB79_11
	.p2align	4, 0x90
.LBB79_8:                               #   in Loop: Header=BB79_5 Depth=1
	xorl	%ebx, %ebx
.LBB79_11:                              # %._crit_edge
                                        #   in Loop: Header=BB79_5 Depth=1
	cmpq	24(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB79_13
# BB#12:                                #   in Loop: Header=BB79_5 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB79_19
.LBB79_13:                              #   in Loop: Header=BB79_5 Depth=1
	movq	40(%rsp), %r8           # 8-byte Reload
	movslq	%r8d, %rax
	addq	%rax, %r14
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	movl	%ebx, %edx
	movl	%r12d, %ecx
	movq	%r8, %r12
	callq	printf
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	_ZN7Encoder8compressEi
	movl	%ebp, %esi
	sarl	$24, %esi
	movq	%r13, %rdi
	callq	_ZN7Encoder8compressEi
	movl	%ebp, %esi
	sarl	$16, %esi
	movq	%r13, %rdi
	callq	_ZN7Encoder8compressEi
	movl	%ebp, %esi
	sarl	$8, %esi
	movq	%r13, %rdi
	callq	_ZN7Encoder8compressEi
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	_ZN7Encoder8compressEi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	64(%rsp), %rsi          # 8-byte Reload
	callq	fseek
	testl	%ebp, %ebp
	movq	32(%rsp), %r14          # 8-byte Reload
	jle	.LBB79_25
# BB#14:                                # %.lr.ph110.preheader
                                        #   in Loop: Header=BB79_5 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB79_15:                              # %.lr.ph110
                                        #   Parent Loop BB79_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r12,%rbx), %esi
	testl	%esi, %esi
	jle	.LBB79_18
# BB#16:                                # %.lr.ph110
                                        #   in Loop: Header=BB79_15 Depth=2
	movl	%esi, %eax
	andl	$4095, %eax             # imm = 0xFFF
	jne	.LBB79_18
# BB#17:                                #   in Loop: Header=BB79_15 Depth=2
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB79_18:                              # %_Z11printStatusi.exit
                                        #   in Loop: Header=BB79_15 Depth=2
	movq	%r15, %rdi
	callq	_IO_getc
	movq	%r13, %rdi
	movl	%eax, %esi
	callq	_ZN7Encoder8compressEi
	incl	%ebx
	cmpl	%ebx, %ebp
	jne	.LBB79_15
.LBB79_25:                              # %.loopexit
                                        #   in Loop: Header=BB79_5 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	fclose
	addl	12(%rsp), %r12d         # 4-byte Folded Reload
	subq	24(%rsp), %r14          # 8-byte Folded Reload
	jg	.LBB79_5
.LBB79_26:                              # %._crit_edge118
	movq	%r15, %rdi
	callq	fclose
	movq	8(%r13), %rdi
	callq	ftell
	movq	%rax, %rcx
	subq	48(%rsp), %rcx          # 8-byte Folded Reload
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB79_6:
	movl	$.L.str.23, %edi
.LBB79_2:
	callq	perror
	xorl	%edi, %edi
	callq	_Z4quitPKc
.LBB79_1:
	movq	%rbx, %rdi
	jmp	.LBB79_2
.Lfunc_end79:
	.size	_Z8compressPKclR7Encoder, .Lfunc_end79-_Z8compressPKclR7Encoder
	.cfi_endproc

	.section	.text._ZN7Encoder8compressEi,"axG",@progbits,_ZN7Encoder8compressEi,comdat
	.weak	_ZN7Encoder8compressEi
	.p2align	4, 0x90
	.type	_ZN7Encoder8compressEi,@function
_ZN7Encoder8compressEi:                 # @_ZN7Encoder8compressEi
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi372:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi373:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi374:
	.cfi_def_cfa_offset 32
.Lcfi375:
	.cfi_offset %rbx, -24
.Lcfi376:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	cmpl	$0, level(%rip)
	je	.LBB80_2
# BB#1:                                 # %.preheader.preheader
	movl	%ebx, %esi
	shrl	$7, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	_ZN7Encoder4codeEi
	movl	%ebx, %esi
	shrl	$6, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	_ZN7Encoder4codeEi
	movl	%ebx, %esi
	shrl	$5, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	_ZN7Encoder4codeEi
	movl	%ebx, %esi
	shrl	$4, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	_ZN7Encoder4codeEi
	movl	%ebx, %esi
	shrl	$3, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	_ZN7Encoder4codeEi
	movl	%ebx, %esi
	shrl	$2, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	_ZN7Encoder4codeEi
	movl	%ebx, %esi
	shrl	%esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	_ZN7Encoder4codeEi
	andl	$1, %ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN7Encoder4codeEi      # TAILCALL
.LBB80_2:
	movq	8(%r14), %rsi
	movl	%ebx, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.Lfunc_end80:
	.size	_ZN7Encoder8compressEi, .Lfunc_end80-_ZN7Encoder8compressEi
	.cfi_endproc

	.text
	.globl	_Z7makedirPKc
	.p2align	4, 0x90
	.type	_Z7makedirPKc,@function
_Z7makedirPKc:                          # @_Z7makedirPKc
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end81:
	.size	_Z7makedirPKc, .Lfunc_end81-_Z7makedirPKc
	.cfi_endproc

	.globl	_Z10decompressPKclR7Encoder
	.p2align	4, 0x90
	.type	_Z10decompressPKclR7Encoder,@function
_Z10decompressPKclR7Encoder:            # @_Z10decompressPKclR7Encoder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi377:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi378:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi379:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi380:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi381:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi382:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi383:
	.cfi_def_cfa_offset 80
.Lcfi384:
	.cfi_offset %rbx, -56
.Lcfi385:
	.cfi_offset %r12, -48
.Lcfi386:
	.cfi_offset %r13, -40
.Lcfi387:
	.cfi_offset %r14, -32
.Lcfi388:
	.cfi_offset %r15, -24
.Lcfi389:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r12
	movl	$.L.str.21, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB82_17
# BB#1:
	movl	$47, %esi
	movq	%r12, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%r12, %rsi
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	printf
	testq	%r13, %r13
	jle	.LBB82_12
# BB#2:                                 # %.lr.ph90.preheader
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB82_3:                               # %.lr.ph90
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jle	.LBB82_6
# BB#4:                                 # %.lr.ph90
                                        #   in Loop: Header=BB82_3 Depth=1
	movl	%ebx, %eax
	andl	$4095, %eax             # imm = 0xFFF
	jne	.LBB82_6
# BB#5:                                 #   in Loop: Header=BB82_3 Depth=1
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB82_6:                               # %_Z11printStatusi.exit
                                        #   in Loop: Header=BB82_3 Depth=1
	testb	$1, %r12b
	jne	.LBB82_7
# BB#8:                                 #   in Loop: Header=BB82_3 Depth=1
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_Z6decodeR7Encoder
	movl	%eax, %ecx
	cmpl	%ecx, %ebp
	je	.LBB82_10
# BB#9:                                 #   in Loop: Header=BB82_3 Depth=1
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	printf
	movb	$1, %r12b
	jmp	.LBB82_10
	.p2align	4, 0x90
.LBB82_7:                               # %.thread
                                        #   in Loop: Header=BB82_3 Depth=1
	movq	%r14, %rdi
	callq	_Z6decodeR7Encoder
.LBB82_10:                              #   in Loop: Header=BB82_3 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB82_3
# BB#11:                                # %._crit_edge91
	testb	$1, %r12b
	jne	.LBB82_16
.LBB82_12:                              # %._crit_edge91.thread
	movq	%r15, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB82_14
# BB#13:
	movl	$.Lstr.3, %edi
	jmp	.LBB82_15
.LBB82_17:
	movl	$.L.str.30, %esi
	movq	%r12, %rdi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB82_25
# BB#18:
	movq	%rsp, %rdi
	movq	%r12, %rsi
	callq	_ZN6StringC2EPKc
	xorl	%eax, %eax
	jmp	.LBB82_19
	.p2align	4, 0x90
.LBB82_33:                              #   in Loop: Header=BB82_19 Depth=1
	incq	%rax
.LBB82_19:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdx
	movzbl	(%rdx,%rax), %ecx
	cmpb	$47, %cl
	je	.LBB82_32
# BB#20:                                #   in Loop: Header=BB82_19 Depth=1
	cmpb	$92, %cl
	je	.LBB82_32
# BB#21:                                #   in Loop: Header=BB82_19 Depth=1
	testb	%cl, %cl
	jne	.LBB82_33
	jmp	.LBB82_22
	.p2align	4, 0x90
.LBB82_32:                              #   in Loop: Header=BB82_19 Depth=1
	movb	$0, (%rdx,%rax)
	movq	16(%rsp), %rdx
	movb	%cl, (%rdx,%rax)
	jmp	.LBB82_33
.LBB82_14:
	movl	$.Lstr.2, %edi
.LBB82_15:                              # %.critedge
	callq	puts
.LBB82_16:                              # %.critedge
	movq	%r15, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fclose                  # TAILCALL
.LBB82_22:
	movl	$.L.str.30, %esi
	movq	%r12, %rdi
	callq	fopen
	movq	%rax, %r15
	movl	programChecker(%rip), %eax
	subl	(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB82_24
# BB#23:
	movl	%eax, programChecker+4(%rip)
.LBB82_24:
	movq	8(%rsp), %rdi
	callq	free
	testq	%r15, %r15
	je	.LBB82_34
.LBB82_25:                              # %.thread81
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	printf
	testq	%r13, %r13
	jle	.LBB82_31
# BB#26:                                # %.lr.ph85.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB82_27:                              # %.lr.ph85
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jle	.LBB82_30
# BB#28:                                # %.lr.ph85
                                        #   in Loop: Header=BB82_27 Depth=1
	movl	%ebx, %eax
	andl	$4095, %eax             # imm = 0xFFF
	jne	.LBB82_30
# BB#29:                                #   in Loop: Header=BB82_27 Depth=1
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB82_30:                              # %_Z11printStatusi.exit78
                                        #   in Loop: Header=BB82_27 Depth=1
	movq	%r14, %rdi
	callq	_Z6decodeR7Encoder
	movl	%eax, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB82_27
.LBB82_31:                              # %._crit_edge86
	movq	%r15, %rdi
	callq	fclose
	movl	$.Lstr.1, %edi
.LBB82_41:
	callq	puts
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB82_34:
	movq	%r12, %rdi
	callq	perror
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	printf
	testq	%r13, %r13
	jle	.LBB82_40
# BB#35:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
.LBB82_36:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	jle	.LBB82_39
# BB#37:                                # %.lr.ph
                                        #   in Loop: Header=BB82_36 Depth=1
	movl	%ebx, %eax
	andl	$4095, %eax             # imm = 0xFFF
	jne	.LBB82_39
# BB#38:                                #   in Loop: Header=BB82_36 Depth=1
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB82_39:                              # %_Z11printStatusi.exit76
                                        #   in Loop: Header=BB82_36 Depth=1
	movq	%r14, %rdi
	callq	_Z6decodeR7Encoder
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB82_36
.LBB82_40:                              # %._crit_edge
	movl	$.Lstr, %edi
	jmp	.LBB82_41
.Lfunc_end82:
	.size	_Z10decompressPKclR7Encoder, .Lfunc_end82-_Z10decompressPKclR7Encoder
	.cfi_endproc

	.section	.text._ZN6StringC2EPKc,"axG",@progbits,_ZN6StringC2EPKc,comdat
	.weak	_ZN6StringC2EPKc
	.p2align	4, 0x90
	.type	_ZN6StringC2EPKc,@function
_ZN6StringC2EPKc:                       # @_ZN6StringC2EPKc
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%rbp
.Lcfi390:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi391:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi392:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi393:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi394:
	.cfi_def_cfa_offset 48
.Lcfi395:
	.cfi_offset %rbx, -48
.Lcfi396:
	.cfi_offset %r12, -40
.Lcfi397:
	.cfi_offset %r14, -32
.Lcfi398:
	.cfi_offset %r15, -24
.Lcfi399:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, (%r14)
	movl	programChecker(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jl	.LBB83_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB83_2:                               # %_ZN14ProgramChecker5allocEi.exit.i.i
	movl	$1, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.LBB83_19
# BB#3:                                 # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i
	movq	%rax, 16(%r14)
	movl	$0, (%r14)
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB83_4
# BB#5:                                 # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i.preheader
	incq	%rbx
	xorl	%r15d, %r15d
	movl	$1, %ebp
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB83_6:                               # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r12d, %r15d
	jne	.LBB83_9
# BB#7:                                 #   in Loop: Header=BB83_6 Depth=1
	leal	(%r12,%r12), %esi
	testl	%esi, %esi
	cmovlel	%ebp, %esi
.Ltmp276:
	movq	%r14, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
.Ltmp277:
# BB#8:                                 # %.noexc
                                        #   in Loop: Header=BB83_6 Depth=1
	movl	%r12d, (%r14)
	movzbl	-1(%rbx), %eax
.LBB83_9:                               # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i
                                        #   in Loop: Header=BB83_6 Depth=1
	movq	16(%r14), %rcx
	leal	1(%r15), %edx
	movl	%edx, (%r14)
	movslq	%r15d, %rdx
	movb	%al, (%rcx,%rdx)
	movzbl	(%rbx), %eax
	movl	(%r14), %r15d
	movl	4(%r14), %r12d
	incq	%rbx
	testb	%al, %al
	jne	.LBB83_6
# BB#10:                                # %_ZN5ArrayIcLi0EE8pop_backEv.exit._crit_edge.i
	cmpl	%r12d, %r15d
	jne	.LBB83_13
# BB#11:
	leal	(%r12,%r12), %eax
	testl	%eax, %eax
	movl	$1, %esi
	cmovgl	%eax, %esi
.Ltmp279:
	movq	%r14, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
.Ltmp280:
# BB#12:                                # %.noexc3
	movl	%r12d, (%r14)
	jmp	.LBB83_13
.LBB83_4:
	xorl	%r15d, %r15d
.LBB83_13:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit._crit_edge.i.thread
	movq	16(%r14), %rax
	leal	1(%r15), %ecx
	movl	%ecx, (%r14)
	movslq	%r15d, %rcx
	movb	$0, (%rax,%rcx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB83_19:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.LBB83_15:                              # %.loopexit.split-lp
.Ltmp281:
	jmp	.LBB83_16
.LBB83_14:                              # %.loopexit
.Ltmp278:
.LBB83_16:
	movq	%rax, %rbx
	movl	programChecker(%rip), %eax
	subl	(%r14), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB83_18
# BB#17:
	movl	%eax, programChecker+4(%rip)
.LBB83_18:                              # %_ZN5ArrayIcLi0EED2Ev.exit
	movq	8(%r14), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end83:
	.size	_ZN6StringC2EPKc, .Lfunc_end83-_ZN6StringC2EPKc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table83:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp276-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp277-.Ltmp276       #   Call between .Ltmp276 and .Ltmp277
	.long	.Ltmp278-.Lfunc_begin21 #     jumps to .Ltmp278
	.byte	0                       #   On action: cleanup
	.long	.Ltmp279-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp280-.Ltmp279       #   Call between .Ltmp279 and .Ltmp280
	.long	.Ltmp281-.Lfunc_begin21 #     jumps to .Ltmp281
	.byte	0                       #   On action: cleanup
	.long	.Ltmp280-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Lfunc_end83-.Ltmp280   #   Call between .Ltmp280 and .Lfunc_end83
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN5ArrayIcLi0EED2Ev,"axG",@progbits,_ZN5ArrayIcLi0EED2Ev,comdat
	.weak	_ZN5ArrayIcLi0EED2Ev
	.p2align	4, 0x90
	.type	_ZN5ArrayIcLi0EED2Ev,@function
_ZN5ArrayIcLi0EED2Ev:                   # @_ZN5ArrayIcLi0EED2Ev
	.cfi_startproc
# BB#0:
	movl	programChecker(%rip), %eax
	subl	(%rdi), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB84_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB84_2:                               # %_ZN14ProgramChecker5allocEi.exit
	movq	8(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end84:
	.size	_ZN5ArrayIcLi0EED2Ev, .Lfunc_end84-_ZN5ArrayIcLi0EED2Ev
	.cfi_endproc

	.text
	.globl	_Z7getlineP8_IO_FILE
	.p2align	4, 0x90
	.type	_Z7getlineP8_IO_FILE,@function
_Z7getlineP8_IO_FILE:                   # @_Z7getlineP8_IO_FILE
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%rbp
.Lcfi400:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi401:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi402:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi403:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi404:
	.cfi_def_cfa_offset 48
.Lcfi405:
	.cfi_offset %rbx, -40
.Lcfi406:
	.cfi_offset %r14, -32
.Lcfi407:
	.cfi_offset %r15, -24
.Lcfi408:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movb	_ZGVZ7getlineP8_IO_FILEE1s(%rip), %al
	testb	%al, %al
	jne	.LBB85_4
# BB#1:
	movl	$_ZGVZ7getlineP8_IO_FILEE1s, %edi
	callq	__cxa_guard_acquire
	testl	%eax, %eax
	je	.LBB85_4
# BB#2:
.Ltmp282:
	movl	$_ZZ7getlineP8_IO_FILEE1s, %edi
	movl	$.L.str.36, %esi
	callq	_ZN6StringC2EPKc
.Ltmp283:
# BB#3:
	movl	$_ZN5ArrayIcLi0EED2Ev, %edi
	movl	$_ZZ7getlineP8_IO_FILEE1s, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZGVZ7getlineP8_IO_FILEE1s, %edi
	callq	__cxa_guard_release
.LBB85_4:                               # %.outer.preheader
	xorl	%ebp, %ebp
	jmp	.LBB85_5
	.p2align	4, 0x90
.LBB85_11:                              #   in Loop: Header=BB85_5 Depth=1
	movq	_ZZ7getlineP8_IO_FILEE1s+16(%rip), %rax
	movb	%bl, (%rax,%rbp)
	incq	%rbp
.LBB85_5:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB85_6 Depth 2
	leal	1(%rbp,%rbp), %r15d
	.p2align	4, 0x90
.LBB85_6:                               #   Parent Loop BB85_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	leal	1(%rbx), %ecx
	movl	$134215678, %edx        # imm = 0x7FFF7FE
	shrl	%cl, %edx
	andl	$1, %edx
	cmpl	$27, %ecx
	movslq	_ZZ7getlineP8_IO_FILEE1s(%rip), %rax
	ja	.LBB85_8
# BB#7:                                 #   in Loop: Header=BB85_6 Depth=2
	testl	%edx, %edx
	je	.LBB85_12
.LBB85_8:                               #   in Loop: Header=BB85_6 Depth=2
	cmpq	%rax, %rbp
	jl	.LBB85_10
# BB#9:                                 #   in Loop: Header=BB85_6 Depth=2
	movl	$_ZZ7getlineP8_IO_FILEE1s, %edi
	movl	%r15d, %esi
	callq	_ZN5ArrayIcLi0EE6resizeEi
.LBB85_10:                              #   in Loop: Header=BB85_6 Depth=2
	cmpl	$13, %ebx
	je	.LBB85_6
	jmp	.LBB85_11
.LBB85_12:
	cmpq	%rax, %rbp
	jl	.LBB85_14
# BB#13:
	leal	1(%rbp), %esi
	movl	$_ZZ7getlineP8_IO_FILEE1s, %edi
	callq	_ZN5ArrayIcLi0EE6resizeEi
.LBB85_14:
	movq	_ZZ7getlineP8_IO_FILEE1s+16(%rip), %rax
	movslq	%ebp, %rcx
	movb	$0, (%rax,%rcx)
	xorl	%eax, %eax
	cmpl	$-1, %ebx
	je	.LBB85_17
# BB#15:
	cmpl	$26, %ebx
	je	.LBB85_17
# BB#16:
	movq	_ZZ7getlineP8_IO_FILEE1s+16(%rip), %rax
.LBB85_17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB85_18:
.Ltmp284:
	movq	%rax, %rbx
	movl	$_ZGVZ7getlineP8_IO_FILEE1s, %edi
	callq	__cxa_guard_abort
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end85:
	.size	_Z7getlineP8_IO_FILE, .Lfunc_end85-_Z7getlineP8_IO_FILE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table85:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp282-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp283-.Ltmp282       #   Call between .Ltmp282 and .Ltmp283
	.long	.Ltmp284-.Lfunc_begin22 #     jumps to .Ltmp284
	.byte	0                       #   On action: cleanup
	.long	.Ltmp283-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Lfunc_end85-.Ltmp283   #   Call between .Ltmp283 and .Lfunc_end85
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN5ArrayIcLi0EE6resizeEi,"axG",@progbits,_ZN5ArrayIcLi0EE6resizeEi,comdat
	.weak	_ZN5ArrayIcLi0EE6resizeEi
	.p2align	4, 0x90
	.type	_ZN5ArrayIcLi0EE6resizeEi,@function
_ZN5ArrayIcLi0EE6resizeEi:              # @_ZN5ArrayIcLi0EE6resizeEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi409:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi410:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi411:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi412:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi413:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi414:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi415:
	.cfi_def_cfa_offset 64
.Lcfi416:
	.cfi_offset %rbx, -56
.Lcfi417:
	.cfi_offset %r12, -48
.Lcfi418:
	.cfi_offset %r13, -40
.Lcfi419:
	.cfi_offset %r14, -32
.Lcfi420:
	.cfi_offset %r15, -24
.Lcfi421:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	%ebp, 4(%rbx)
	jge	.LBB86_1
# BB#3:
	leaq	8(%rbx), %r13
	movq	8(%rbx), %r14
	movq	16(%rbx), %r15
	movl	(%rbx), %r12d
	movl	%ebp, 4(%rbx)
	movl	%ebp, (%rbx)
	testl	%ebp, %ebp
	jle	.LBB86_4
# BB#5:
	movslq	%ebp, %rdi
	movl	programChecker(%rip), %eax
	addl	%ebp, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB86_7
# BB#6:
	movl	%eax, programChecker+4(%rip)
.LBB86_7:                               # %_ZN14ProgramChecker5allocEi.exit.i
	movl	$1, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	je	.LBB86_14
# BB#8:
	movq	%rax, 16(%rbx)
	testq	%r14, %r14
	jne	.LBB86_10
	jmp	.LBB86_2
.LBB86_1:
	movl	%ebp, (%rbx)
	jmp	.LBB86_2
.LBB86_4:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.LBB86_2
.LBB86_10:
	testq	%r15, %r15
	je	.LBB86_13
# BB#11:
	cmpl	%ebp, %r12d
	cmovgl	%ebp, %r12d
	movslq	%r12d, %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movl	programChecker(%rip), %eax
	subl	%ebp, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB86_13
# BB#12:
	movl	%eax, programChecker+4(%rip)
.LBB86_13:                              # %_ZN14ProgramChecker5allocEi.exit
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB86_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB86_14:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Lfunc_end86:
	.size	_ZN5ArrayIcLi0EE6resizeEi, .Lfunc_end86-_ZN5ArrayIcLi0EE6resizeEi
	.cfi_endproc

	.text
	.globl	_Z7putsizeR6StringS0_PKci
	.p2align	4, 0x90
	.type	_Z7putsizeR6StringS0_PKci,@function
_Z7putsizeR6StringS0_PKci:              # @_Z7putsizeR6StringS0_PKci
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi422:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi423:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi424:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi425:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi426:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi427:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi428:
	.cfi_def_cfa_offset 80
.Lcfi429:
	.cfi_offset %rbx, -56
.Lcfi430:
	.cfi_offset %r12, -48
.Lcfi431:
	.cfi_offset %r13, -40
.Lcfi432:
	.cfi_offset %r14, -32
.Lcfi433:
	.cfi_offset %r15, -24
.Lcfi434:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	$.L.str.21, %esi
	movq	%r12, %rdi
	callq	fopen
	movq	%rax, %rbx
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB87_45
# BB#1:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	fseek
	movq	%rbx, %rdi
	callq	ftell
	movq	%rax, %rcx
	testq	%rcx, %rcx
	js	.LBB87_44
# BB#2:
	movl	$_ZZ7putsizeR6StringS0_PKciE3blk, %edi
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	sprintf
	movl	(%r13), %ebp
	testl	%ebp, %ebp
	jle	.LBB87_4
# BB#3:
	decl	%ebp
	movl	%ebp, (%r13)
.LBB87_4:                               # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movb	_ZZ7putsizeR6StringS0_PKciE3blk(%rip), %al
	testb	%al, %al
	je	.LBB87_9
# BB#5:                                 # %.lr.ph.i
	movl	$_ZZ7putsizeR6StringS0_PKciE3blk+1, %r14d
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB87_6:                               # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	4(%r13), %ebp
	jne	.LBB87_8
# BB#7:                                 #   in Loop: Header=BB87_6 Depth=1
	leal	(%rbp,%rbp), %esi
	testl	%esi, %esi
	cmovlel	%ebx, %esi
	movq	%r13, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebp, (%r13)
	movzbl	-1(%r14), %eax
.LBB87_8:                               # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i
                                        #   in Loop: Header=BB87_6 Depth=1
	movq	16(%r13), %rcx
	leal	1(%rbp), %edx
	movl	%edx, (%r13)
	movslq	%ebp, %rdx
	movb	%al, (%rcx,%rdx)
	movzbl	(%r14), %eax
	movl	(%r13), %ebp
	incq	%r14
	testb	%al, %al
	jne	.LBB87_6
.LBB87_9:                               # %_ZN5ArrayIcLi0EE8pop_backEv.exit._crit_edge.i
	cmpl	4(%r13), %ebp
	jne	.LBB87_11
# BB#10:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %esi
	cmovgl	%eax, %esi
	movq	%r13, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebp, (%r13)
.LBB87_11:                              # %_ZN6StringpLEPKc.exit
	movq	16(%r13), %rax
	leal	1(%rbp), %ecx
	movl	%ecx, (%r13)
	movslq	%ebp, %rcx
	movb	$0, (%rax,%rcx)
	movslq	12(%rsp), %rcx          # 4-byte Folded Reload
	movl	(%r13), %ebp
	testl	%ebp, %ebp
	jle	.LBB87_13
# BB#12:
	decl	%ebp
	movl	%ebp, (%r13)
.LBB87_13:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i22
	movb	(%r12,%rcx), %al
	testb	%al, %al
	je	.LBB87_18
# BB#14:                                # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i25.preheader
	leaq	1(%r12,%rcx), %rbx
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB87_15:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i25
                                        # =>This Inner Loop Header: Depth=1
	cmpl	4(%r13), %ebp
	jne	.LBB87_17
# BB#16:                                #   in Loop: Header=BB87_15 Depth=1
	leal	(%rbp,%rbp), %esi
	testl	%esi, %esi
	cmovlel	%r14d, %esi
	movq	%r13, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebp, (%r13)
	movzbl	-1(%rbx), %eax
.LBB87_17:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i28
                                        #   in Loop: Header=BB87_15 Depth=1
	movq	16(%r13), %rcx
	leal	1(%rbp), %edx
	movl	%edx, (%r13)
	movslq	%ebp, %rdx
	movb	%al, (%rcx,%rdx)
	movzbl	(%rbx), %eax
	movl	(%r13), %ebp
	incq	%rbx
	testb	%al, %al
	jne	.LBB87_15
.LBB87_18:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit._crit_edge.i29
	cmpl	4(%r13), %ebp
	jne	.LBB87_20
# BB#19:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %esi
	cmovgl	%eax, %esi
	movq	%r13, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebp, (%r13)
.LBB87_20:                              # %_ZN6StringpLEPKc.exit30
	movq	16(%r13), %rax
	leal	1(%rbp), %ecx
	movl	%ecx, (%r13)
	movslq	%ebp, %rcx
	movb	$0, (%rax,%rcx)
	movl	(%r13), %ebx
	testl	%ebx, %ebx
	jle	.LBB87_22
# BB#21:
	decl	%ebx
	movl	%ebx, (%r13)
.LBB87_22:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i35.preheader
	cmpl	4(%r13), %ebx
	jne	.LBB87_24
# BB#23:
	leal	(%rbx,%rbx), %eax
	testl	%eax, %eax
	movl	$1, %esi
	cmovgl	%eax, %esi
	movq	%r13, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebx, (%r13)
.LBB87_24:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i38
	movq	16(%r13), %rax
	leal	1(%rbx), %ecx
	movl	%ecx, (%r13)
	movslq	%ebx, %rcx
	movb	$13, (%rax,%rcx)
	movl	(%r13), %ebp
	movslq	%ebp, %rbx
	cmpl	4(%r13), %ebx
	jne	.LBB87_26
# BB#25:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %esi
	cmovgl	%eax, %esi
	movq	%r13, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebp, (%r13)
.LBB87_26:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i38.1
	movq	16(%r13), %rax
	incl	%ebp
	movl	%ebp, (%r13)
	movb	$10, (%rax,%rbx)
	movl	(%r13), %ebp
	movslq	%ebp, %rbx
	cmpl	4(%r13), %ebx
	jne	.LBB87_28
# BB#27:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %esi
	cmovgl	%eax, %esi
	movq	%r13, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebp, (%r13)
.LBB87_28:                              # %_ZN6StringpLEPKc.exit40
	movq	16(%r13), %rax
	incl	%ebp
	movl	%ebp, (%r13)
	movb	$0, (%rax,%rbx)
	movl	(%r15), %ebx
	testl	%ebx, %ebx
	jle	.LBB87_30
# BB#29:
	decl	%ebx
	movl	%ebx, (%r15)
.LBB87_30:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i42
	movb	(%r12), %al
	testb	%al, %al
	je	.LBB87_35
# BB#31:                                # %.lr.ph.i43
	incq	%r12
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB87_32:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i45
                                        # =>This Inner Loop Header: Depth=1
	cmpl	4(%r15), %ebx
	jne	.LBB87_34
# BB#33:                                #   in Loop: Header=BB87_32 Depth=1
	leal	(%rbx,%rbx), %esi
	testl	%esi, %esi
	cmovlel	%ebp, %esi
	movq	%r15, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebx, (%r15)
	movzbl	-1(%r12), %eax
.LBB87_34:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i48
                                        #   in Loop: Header=BB87_32 Depth=1
	movq	16(%r15), %rcx
	leal	1(%rbx), %edx
	movl	%edx, (%r15)
	movslq	%ebx, %rdx
	movb	%al, (%rcx,%rdx)
	movzbl	(%r12), %eax
	movl	(%r15), %ebx
	incq	%r12
	testb	%al, %al
	jne	.LBB87_32
.LBB87_35:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit._crit_edge.i49
	cmpl	4(%r15), %ebx
	jne	.LBB87_37
# BB#36:
	leal	(%rbx,%rbx), %eax
	testl	%eax, %eax
	movl	$1, %esi
	cmovgl	%eax, %esi
	movq	%r15, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebx, (%r15)
.LBB87_37:                              # %_ZN6StringpLEPKc.exit50
	movq	16(%r15), %rax
	leal	1(%rbx), %ecx
	movl	%ecx, (%r15)
	movslq	%ebx, %rcx
	movb	$0, (%rax,%rcx)
	movl	(%r15), %ebx
	testl	%ebx, %ebx
	jle	.LBB87_39
# BB#38:
	decl	%ebx
	movl	%ebx, (%r15)
.LBB87_39:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i55.preheader
	cmpl	4(%r15), %ebx
	jne	.LBB87_41
# BB#40:
	leal	(%rbx,%rbx), %eax
	testl	%eax, %eax
	movl	$1, %esi
	cmovgl	%eax, %esi
	movq	%r15, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebx, (%r15)
.LBB87_41:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i58
	movq	16(%r15), %rax
	leal	1(%rbx), %ecx
	movl	%ecx, (%r15)
	movslq	%ebx, %rcx
	movb	$10, (%rax,%rcx)
	movl	(%r15), %ebp
	movslq	%ebp, %rbx
	cmpl	4(%r15), %ebx
	jne	.LBB87_43
# BB#42:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %esi
	cmovgl	%eax, %esi
	movq	%r15, %rdi
	callq	_ZN5ArrayIcLi0EE6resizeEi
	movl	%ebp, (%r15)
.LBB87_43:                              # %_ZN6StringpLEPKc.exit60
	movq	16(%r15), %rax
	incl	%ebp
	movl	%ebp, (%r15)
	movb	$0, (%rax,%rbx)
	movl	$1, %ebp
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB87_44:
	movq	%rbx, %rdi
	callq	fclose
.LBB87_45:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end87:
	.size	_Z7putsizeR6StringS0_PKci, .Lfunc_end87-_Z7putsizeR6StringS0_PKci
	.cfi_endproc

	.globl	_Z6expandR6StringS0_PKci
	.p2align	4, 0x90
	.type	_Z6expandR6StringS0_PKci,@function
_Z6expandR6StringS0_PKci:               # @_Z6expandR6StringS0_PKci
	.cfi_startproc
# BB#0:
	jmp	_Z7putsizeR6StringS0_PKci # TAILCALL
.Lfunc_end88:
	.size	_Z6expandR6StringS0_PKci, .Lfunc_end88-_Z6expandR6StringS0_PKci
	.cfi_endproc

	.globl	_Z7paqmainiPPc
	.p2align	4, 0x90
	.type	_Z7paqmainiPPc,@function
_Z7paqmainiPPc:                         # @_Z7paqmainiPPc
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%rbp
.Lcfi435:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi436:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi437:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi438:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi439:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi440:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi441:
	.cfi_def_cfa_offset 304
.Lcfi442:
	.cfi_offset %rbx, -56
.Lcfi443:
	.cfi_offset %r12, -48
.Lcfi444:
	.cfi_offset %r13, -40
.Lcfi445:
	.cfi_offset %r14, -32
.Lcfi446:
	.cfi_offset %r15, -24
.Lcfi447:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	movq	(%r14), %rbp
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %r13
	cmoveq	%rbp, %r13
	cmpl	$3, %ebx
	setl	%dl
	cmpl	$2, %ebx
	jl	.LBB89_1
# BB#3:
	movq	8(%r14), %rcx
	cmpb	$45, (%rcx)
	jne	.LBB89_4
# BB#5:
	movsbl	1(%rcx), %eax
	testl	%eax, %eax
	je	.LBB89_4
# BB#6:
	cmpb	$0, 2(%rcx)
	je	.LBB89_7
.LBB89_4:
	movl	%ebx, 136(%rsp)         # 4-byte Spill
	xorl	%r15d, %r15d
.LBB89_18:                              # %.thread
	movl	%edx, 132(%rsp)         # 4-byte Spill
	movl	programChecker(%rip), %ebx
	leal	8(%rbx), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %ebp
	cmpl	%ebp, %eax
	jle	.LBB89_20
# BB#19:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %ebp
.LBB89_20:                              # %_ZN14ProgramChecker5allocEi.exit.i.i
	movl	$8, %edi
	movl	$1, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB89_21
# BB#23:
	movq	%rax, 40(%rsp)          # 8-byte Spill
	addl	$16, %ebx
	movl	%ebx, programChecker(%rip)
	cmpl	%ebp, %ebx
	jle	.LBB89_25
# BB#24:
	movl	%ebx, programChecker+4(%rip)
.LBB89_25:                              # %_ZN14ProgramChecker5allocEi.exit.i.i284
	movl	$8, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB89_26
# BB#28:
	movq	8(%r14), %rbp
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbp, %rsi
.Ltmp290:
	leaq	16(%rsp), %rdi
	callq	_ZN6StringC2EPKc
.Ltmp291:
	movq	%r14, %rbx
# BB#29:
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %r14
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	8(%rbx), %rbp
	movq	%rbp, %rdi
	callq	strlen
	leal	1(%r14), %ecx
	cmpl	%ecx, %eax
	jle	.LBB89_44
# BB#30:
	movl	%eax, %ecx
	subl	%r14d, %ecx
	shlq	$32, %rcx
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	addq	%rcx, %rdx
	sarq	$32, %rdx
	cmpb	$46, (%rbp,%rdx)
	jne	.LBB89_44
# BB#31:
	cltq
	addq	%rax, %rbp
	movslq	%r14d, %rax
	subq	%rax, %rbp
	movq	%r13, %rdx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB89_32:                              # =>This Inner Loop Header: Depth=1
	movsbl	(%rdx), %eax
	testl	%eax, %eax
	movb	(%rbp), %sil
	je	.LBB89_33
# BB#34:                                #   in Loop: Header=BB89_32 Depth=1
	testb	%sil, %sil
	je	.LBB89_35
# BB#487:                               #   in Loop: Header=BB89_32 Depth=1
	movsbl	%sil, %edi
	movl	%eax, %ebx
	addb	$-65, %bl
	leal	32(%rax), %ecx
	cmpb	$26, %bl
	cmovbl	%ecx, %eax
	addb	$-65, %sil
	leal	32(%rdi), %ecx
	cmpb	$26, %sil
	cmovael	%edi, %ecx
	incq	%rdx
	incq	%rbp
	cmpl	%ecx, %eax
	je	.LBB89_32
	jmp	.LBB89_37
.LBB89_44:
	movl	$1, %r12d
	testb	%r15b, %r15b
	je	.LBB89_45
	jmp	.LBB89_109
.LBB89_7:
	movl	%eax, %ecx
	addb	$-48, %cl
	cmpb	$9, %cl
	ja	.LBB89_11
# BB#8:
	addl	$-48, %eax
	movl	%eax, level(%rip)
	xorl	%r15d, %r15d
	jmp	.LBB89_9
.LBB89_33:
	xorl	%eax, %eax
	jmp	.LBB89_36
.LBB89_35:
	xorl	%esi, %esi
.LBB89_36:                              # %.critedge.i
	xorl	%r8d, %r8d
	cmpb	%sil, %al
	sete	%r8b
.LBB89_37:                              # %_Z6equalsPKcS0_.exit
	movl	$1, %r12d
	testb	%r15b, %r15b
	jne	.LBB89_109
# BB#38:                                # %_Z6equalsPKcS0_.exit
	testl	%r8d, %r8d
	jne	.LBB89_109
.LBB89_45:
	movl	16(%rsp), %ebp
	testl	%ebp, %ebp
	jle	.LBB89_47
# BB#46:
	decl	%ebp
	movl	%ebp, 16(%rsp)
.LBB89_47:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i
	cmpl	20(%rsp), %ebp
	jne	.LBB89_61
# BB#48:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %r12d
	cmovgl	%eax, %r12d
	cmpl	%r12d, %ebp
	jge	.LBB89_49
# BB#50:
	movq	24(%rsp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r15
	movl	%r12d, 20(%rsp)
	movl	%r12d, 16(%rsp)
	movl	%r12d, %edi
	movl	programChecker(%rip), %ebx
	leal	(%rbx,%r12), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r14d
	cmpl	%r14d, %eax
	jle	.LBB89_52
# BB#51:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r14d
.LBB89_52:                              # %_ZN14ProgramChecker5allocEi.exit.i.i288
	movl	$1, %esi
	callq	calloc
	movq	%rax, 24(%rsp)
	testq	%rax, %rax
	je	.LBB89_53
# BB#55:
	movq	%rax, 32(%rsp)
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	je	.LBB89_60
# BB#56:
	testq	%r15, %r15
	je	.LBB89_59
# BB#57:
	cmpl	%r12d, %ebp
	cmovlel	%ebp, %r12d
	movslq	%r12d, %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movl	%ebx, programChecker(%rip)
	cmpl	%r14d, %ebx
	jle	.LBB89_59
# BB#58:
	movl	%ebx, programChecker+4(%rip)
.LBB89_59:                              # %_ZN14ProgramChecker5allocEi.exit.i
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB89_60
.LBB89_49:
	movl	%r12d, 16(%rsp)
.LBB89_60:                              # %.noexc286
	movl	%ebp, 16(%rsp)
.LBB89_61:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i
	movq	32(%rsp), %rax
	leal	1(%rbp), %ecx
	movl	%ecx, 16(%rsp)
	movslq	%ebp, %rcx
	movb	$46, (%rax,%rcx)
	movl	16(%rsp), %ebp
	cmpl	20(%rsp), %ebp
	jne	.LBB89_75
# BB#62:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %r12d
	cmovgl	%eax, %r12d
	cmpl	%r12d, %ebp
	jge	.LBB89_63
# BB#64:
	movq	24(%rsp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r15
	movl	%r12d, 20(%rsp)
	movl	%r12d, 16(%rsp)
	movl	%r12d, %edi
	movl	programChecker(%rip), %ebx
	leal	(%rbx,%r12), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r14d
	cmpl	%r14d, %eax
	jle	.LBB89_66
# BB#65:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r14d
.LBB89_66:                              # %_ZN14ProgramChecker5allocEi.exit.i.i309
	movl	$1, %esi
	callq	calloc
	movq	%rax, 24(%rsp)
	testq	%rax, %rax
	je	.LBB89_67
# BB#69:
	movq	%rax, 32(%rsp)
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	je	.LBB89_74
# BB#70:
	testq	%r15, %r15
	je	.LBB89_73
# BB#71:
	cmpl	%r12d, %ebp
	cmovlel	%ebp, %r12d
	movslq	%r12d, %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movl	%ebx, programChecker(%rip)
	cmpl	%r14d, %ebx
	jle	.LBB89_73
# BB#72:
	movl	%ebx, programChecker+4(%rip)
.LBB89_73:                              # %_ZN14ProgramChecker5allocEi.exit.i310
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB89_74
.LBB89_63:
	movl	%r12d, 16(%rsp)
.LBB89_74:                              # %.noexc287
	movl	%ebp, 16(%rsp)
.LBB89_75:
	movslq	%ebp, %rax
	movq	32(%rsp), %rcx
	incl	%ebp
	movl	%ebp, 16(%rsp)
	movb	$0, (%rcx,%rax)
	movl	16(%rsp), %ebp
	testl	%ebp, %ebp
	jle	.LBB89_77
# BB#76:
	decl	%ebp
	movl	%ebp, 16(%rsp)
.LBB89_77:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i291
	movb	(%r13), %al
	testb	%al, %al
	je	.LBB89_94
# BB#78:                                # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i293.preheader
	incq	%r13
	.p2align	4, 0x90
.LBB89_79:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i293
                                        # =>This Inner Loop Header: Depth=1
	cmpl	20(%rsp), %ebp
	jne	.LBB89_93
# BB#80:                                #   in Loop: Header=BB89_79 Depth=1
	leal	(%rbp,%rbp), %ebx
	testl	%ebx, %ebx
	movl	$1, %eax
	cmovlel	%eax, %ebx
	cmpl	%ebx, %ebp
	jge	.LBB89_81
# BB#82:                                #   in Loop: Header=BB89_79 Depth=1
	movq	24(%rsp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	32(%rsp), %r15
	movl	%ebx, 20(%rsp)
	movl	%ebx, 16(%rsp)
	movl	%ebx, %edi
	movl	programChecker(%rip), %r12d
	leal	(%r12,%rbx), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r14d
	cmpl	%r14d, %eax
	jle	.LBB89_84
# BB#83:                                #   in Loop: Header=BB89_79 Depth=1
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r14d
.LBB89_84:                              # %_ZN14ProgramChecker5allocEi.exit.i.i301
                                        #   in Loop: Header=BB89_79 Depth=1
	movl	$1, %esi
	callq	calloc
	movq	%rax, 24(%rsp)
	testq	%rax, %rax
	je	.LBB89_85
# BB#87:                                #   in Loop: Header=BB89_79 Depth=1
	movq	%rax, 32(%rsp)
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	je	.LBB89_92
# BB#88:                                #   in Loop: Header=BB89_79 Depth=1
	testq	%r15, %r15
	je	.LBB89_91
# BB#89:                                #   in Loop: Header=BB89_79 Depth=1
	cmpl	%ebx, %ebp
	cmovlel	%ebp, %ebx
	movslq	%ebx, %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movl	%r12d, programChecker(%rip)
	cmpl	%r14d, %r12d
	jle	.LBB89_91
# BB#90:                                #   in Loop: Header=BB89_79 Depth=1
	movl	%r12d, programChecker+4(%rip)
.LBB89_91:                              # %_ZN14ProgramChecker5allocEi.exit.i302
                                        #   in Loop: Header=BB89_79 Depth=1
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB89_92
	.p2align	4, 0x90
.LBB89_81:                              #   in Loop: Header=BB89_79 Depth=1
	movl	%ebx, 16(%rsp)
.LBB89_92:                              # %.noexc298
                                        #   in Loop: Header=BB89_79 Depth=1
	movl	%ebp, 16(%rsp)
	movzbl	-1(%r13), %eax
.LBB89_93:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i296
                                        #   in Loop: Header=BB89_79 Depth=1
	movq	32(%rsp), %rcx
	leal	1(%rbp), %edx
	movl	%edx, 16(%rsp)
	movslq	%ebp, %rdx
	movb	%al, (%rcx,%rdx)
	movzbl	(%r13), %eax
	movl	16(%rsp), %ebp
	incq	%r13
	testb	%al, %al
	jne	.LBB89_79
.LBB89_94:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit._crit_edge.i297
	cmpl	20(%rsp), %ebp
	jne	.LBB89_108
# BB#95:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %ebx
	cmovgl	%eax, %ebx
	cmpl	%ebx, %ebp
	jge	.LBB89_96
# BB#97:
	movq	24(%rsp), %r15
	movq	32(%rsp), %r14
	movl	%ebx, 20(%rsp)
	movl	%ebx, 16(%rsp)
	movl	%ebx, %edi
	movl	programChecker(%rip), %r12d
	leal	(%r12,%rbx), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r13d
	cmpl	%r13d, %eax
	jle	.LBB89_99
# BB#98:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r13d
.LBB89_99:                              # %_ZN14ProgramChecker5allocEi.exit.i.i305
	movl	$1, %esi
	callq	calloc
	movq	%rax, 24(%rsp)
	testq	%rax, %rax
	je	.LBB89_100
# BB#102:
	movq	%rax, 32(%rsp)
	testq	%r15, %r15
	je	.LBB89_107
# BB#103:
	testq	%r14, %r14
	je	.LBB89_106
# BB#104:
	cmpl	%ebx, %ebp
	cmovlel	%ebp, %ebx
	movslq	%ebx, %rdx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	memcpy
	movl	%r12d, programChecker(%rip)
	cmpl	%r13d, %r12d
	jle	.LBB89_106
# BB#105:
	movl	%r12d, programChecker+4(%rip)
.LBB89_106:                             # %_ZN14ProgramChecker5allocEi.exit.i306
	movq	%r15, %rdi
	callq	free
	jmp	.LBB89_107
.LBB89_96:
	movl	%ebx, 16(%rsp)
.LBB89_107:                             # %.noexc299
	movl	%ebp, 16(%rsp)
.LBB89_108:                             # %_ZN6StringpLEPKc.exit300
	movq	32(%rsp), %rax
	leal	1(%rbp), %ecx
	movl	%ecx, 16(%rsp)
	movslq	%ebp, %rcx
	movb	$0, (%rax,%rcx)
	xorl	%r12d, %r12d
.LBB89_109:
.Ltmp295:
	leaq	184(%rsp), %rdi
	movl	$.L.str.36, %esi
	callq	_ZN6StringC2EPKc
.Ltmp296:
# BB#110:
	testl	%r12d, %r12d
	movl	%r12d, 88(%rsp)         # 4-byte Spill
	je	.LBB89_111
# BB#227:                               # %.thread514
	movq	32(%rsp), %rdi
	movl	$.L.str.47, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB89_228
# BB#230:
	movl	$1, %r14d
.Ltmp298:
	movq	%r15, %rdi
	callq	_Z7getlineP8_IO_FILE
	movq	%rax, %rbx
.Ltmp299:
# BB#231:
	movl	$.L.str.48, %esi
	movl	$7, %edx
	movq	%rbx, %rdi
	callq	strncmp
	testl	%eax, %eax
	jne	.LBB89_232
# BB#237:
	movsbl	7(%rbx), %eax
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	movl	$5, %edx
	cmovgl	%edx, %ecx
	cmpl	$48, %eax
	cmovll	%edx, %ecx
	movl	%ecx, level(%rip)
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	jmp	.LBB89_238
	.p2align	4, 0x90
.LBB89_240:                             #   in Loop: Header=BB89_238 Depth=1
	incl	%ebp
	decq	%r12
	addl	$-8, %r13d
.LBB89_238:                             # =>This Inner Loop Header: Depth=1
.Ltmp303:
	movq	%r15, %rdi
	callq	_Z7getlineP8_IO_FILE
.Ltmp304:
# BB#239:                               #   in Loop: Header=BB89_238 Depth=1
	testq	%rax, %rax
	jne	.LBB89_240
# BB#241:
	movl	level(%rip), %edx
	negq	%r12
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	movq	%r15, %rdi
	callq	ftell
	movq	%rax, %r14
	leaq	4(%r14), %rbx
	cmpl	%ebx, 188(%rsp)
	jge	.LBB89_242
# BB#243:
	movq	%r15, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	192(%rsp), %rbp
	movq	200(%rsp), %rsi
	movl	184(%rsp), %r15d
	movl	%ebx, 188(%rsp)
	movl	%ebx, 184(%rsp)
	testl	%ebx, %ebx
	jle	.LBB89_244
# BB#245:
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movslq	%ebx, %rdi
	movl	programChecker(%rip), %eax
	addl	%ebx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_247
# BB#246:
	movl	%eax, programChecker+4(%rip)
.LBB89_247:                             # %_ZN14ProgramChecker5allocEi.exit.i.i335
	movl	$1, %esi
	callq	calloc
	movq	%rax, 192(%rsp)
	testq	%rax, %rax
	je	.LBB89_248
# BB#250:
	movq	%rax, 200(%rsp)
	movq	72(%rsp), %rsi          # 8-byte Reload
	testq	%rbp, %rbp
	jne	.LBB89_252
	jmp	.LBB89_256
.LBB89_111:
	movl	$1, %r14d
.Ltmp319:
	leaq	208(%rsp), %rdi
	movl	$.L.str.36, %esi
	callq	_ZN6StringC2EPKc
.Ltmp320:
# BB#112:                               # %.preheader554
	cmpl	$2, 136(%rsp)           # 4-byte Folded Reload
	jl	.LBB89_186
# BB#113:                               # %.lr.ph609
	jne	.LBB89_114
# BB#119:                               # %.lr.ph609.split.us
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	8(%rax), %rsi
.Ltmp327:
	leaq	160(%rsp), %rdi
	callq	_ZN6StringC2EPKc
.Ltmp328:
# BB#120:
	movslq	160(%rsp), %rcx
	leal	-1(%rcx), %eax
	testq	%rcx, %rcx
	jle	.LBB89_136
# BB#121:                               # %.lr.ph601.us.preheader
	movl	%ecx, %edx
	testb	$1, %dl
	jne	.LBB89_123
# BB#122:
	xorl	%esi, %esi
	cmpl	$1, %edx
	jne	.LBB89_127
	jmp	.LBB89_132
.LBB89_114:                             # %.lr.ph609.split.preheader
	movslq	136(%rsp), %r14         # 4-byte Folded Reload
	movl	$1, %r13d
	leaq	160(%rsp), %r12
	leaq	184(%rsp), %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB89_115:                             # %.lr.ph609.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB89_167 Depth 2
                                        #     Child Loop BB89_173 Depth 2
                                        #     Child Loop BB89_160 Depth 2
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r13,8), %rsi
.Ltmp321:
	movq	%r12, %rdi
	callq	_ZN6StringC2EPKc
.Ltmp322:
# BB#116:                               #   in Loop: Header=BB89_115 Depth=1
	movslq	160(%rsp), %rcx
	leal	-1(%rcx), %eax
	testq	%rcx, %rcx
	jle	.LBB89_159
# BB#117:                               # %.lr.ph601.preheader
                                        #   in Loop: Header=BB89_115 Depth=1
	movl	%ecx, %edx
	testb	$1, %dl
	jne	.LBB89_154
# BB#118:                               #   in Loop: Header=BB89_115 Depth=1
	xorl	%esi, %esi
	cmpl	$1, %edx
	jne	.LBB89_167
	jmp	.LBB89_158
	.p2align	4, 0x90
.LBB89_154:                             # %.lr.ph601.prol
                                        #   in Loop: Header=BB89_115 Depth=1
	movq	176(%rsp), %rsi
	cmpb	$92, (%rsi)
	jne	.LBB89_156
# BB#155:                               #   in Loop: Header=BB89_115 Depth=1
	movb	$47, (%rsi)
.LBB89_156:                             # %.lr.ph601.prol.loopexit
                                        #   in Loop: Header=BB89_115 Depth=1
	movl	$1, %esi
	cmpl	$1, %edx
	je	.LBB89_158
	.p2align	4, 0x90
.LBB89_167:                             # %.lr.ph601
                                        #   Parent Loop BB89_115 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	176(%rsp), %rdi
	cmpb	$92, (%rdi,%rsi)
	jne	.LBB89_169
# BB#168:                               #   in Loop: Header=BB89_167 Depth=2
	movb	$47, (%rdi,%rsi)
.LBB89_169:                             # %.lr.ph601.1723
                                        #   in Loop: Header=BB89_167 Depth=2
	movq	176(%rsp), %rdi
	cmpb	$92, 1(%rdi,%rsi)
	jne	.LBB89_171
# BB#170:                               #   in Loop: Header=BB89_167 Depth=2
	movb	$47, 1(%rdi,%rsi)
.LBB89_171:                             #   in Loop: Header=BB89_167 Depth=2
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB89_167
.LBB89_158:                             # %.preheader553
                                        #   in Loop: Header=BB89_115 Depth=1
	cmpl	$2, %ecx
	jl	.LBB89_159
	.p2align	4, 0x90
.LBB89_173:                             # %.lr.ph603
                                        #   Parent Loop BB89_115 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	176(%rsp), %rax
	cmpb	$47, -2(%rax,%rcx)
	jne	.LBB89_174
# BB#172:                               #   in Loop: Header=BB89_173 Depth=2
	movb	$0, -2(%rax,%rcx)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB89_173
.LBB89_174:                             # %.lr.ph603..critedge.preheader.loopexit_crit_edge
                                        #   in Loop: Header=BB89_115 Depth=1
	decl	%ecx
	movl	%ecx, %eax
.LBB89_159:                             # %.critedge.preheader
                                        #   in Loop: Header=BB89_115 Depth=1
	movl	%eax, %esi
	movq	176(%rsp), %rdx
	cltq
	leaq	(%rdx,%rax), %rcx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB89_160:                             # %.critedge
                                        #   Parent Loop BB89_115 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rdi), %rbp
	testq	%rbp, %rbp
	jle	.LBB89_175
# BB#161:                               #   in Loop: Header=BB89_160 Depth=2
	leaq	-1(%rdi), %rbp
	cmpb	$47, -1(%rcx,%rdi)
	movq	%rbp, %rdi
	jne	.LBB89_160
# BB#162:                               # %.critedge271.thread.loopexit
                                        #   in Loop: Header=BB89_115 Depth=1
	leal	1(%rsi,%rbp), %ecx
	jmp	.LBB89_178
	.p2align	4, 0x90
.LBB89_175:                             # %.critedge271
                                        #   in Loop: Header=BB89_115 Depth=1
	movq	%rsi, %rcx
	addq	%rdi, %rcx
	cmpl	$2, %esi
	jl	.LBB89_178
# BB#176:                               # %.critedge271
                                        #   in Loop: Header=BB89_115 Depth=1
	testl	%ecx, %ecx
	jne	.LBB89_178
# BB#177:                               #   in Loop: Header=BB89_115 Depth=1
	xorl	%ecx, %ecx
	cmpb	$58, 1(%rdx)
	sete	%cl
	addl	%ecx, %ecx
.LBB89_178:                             # %.critedge271.thread
                                        #   in Loop: Header=BB89_115 Depth=1
.Ltmp324:
	leaq	208(%rsp), %rdi
	movq	%r15, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_Z7putsizeR6StringS0_PKci
	movl	%eax, %ebp
.Ltmp325:
# BB#179:                               # %_Z6expandR6StringS0_PKci.exit
                                        #   in Loop: Header=BB89_115 Depth=1
	testl	%ebp, %ebp
	jne	.LBB89_182
# BB#180:                               # %_Z6expandR6StringS0_PKci.exit
                                        #   in Loop: Header=BB89_115 Depth=1
	cmpq	$2, %r13
	jl	.LBB89_182
# BB#181:                               #   in Loop: Header=BB89_115 Depth=1
	movq	176(%rsp), %rsi
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	printf
.LBB89_182:                             #   in Loop: Header=BB89_115 Depth=1
	movl	programChecker(%rip), %eax
	subl	160(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_184
# BB#183:                               #   in Loop: Header=BB89_115 Depth=1
	movl	%eax, programChecker+4(%rip)
.LBB89_184:                             # %_ZN5ArrayIcLi0EED2Ev.exit
                                        #   in Loop: Header=BB89_115 Depth=1
	movq	168(%rsp), %rdi
	callq	free
	addl	%ebp, %ebx
	incq	%r13
	cmpq	%r14, %r13
	jl	.LBB89_115
	jmp	.LBB89_185
.LBB89_242:
	movl	%ebx, 184(%rsp)
	jmp	.LBB89_257
.LBB89_11:
	movb	$1, %r15b
	cmpb	$100, %al
	jne	.LBB89_12
.LBB89_9:
	xorl	%edx, %edx
	cmpl	$2, %ebx
	jle	.LBB89_10
# BB#17:
	addq	$8, %r14
	decl	%ebx
	movl	%ebx, 136(%rsp)         # 4-byte Spill
	jmp	.LBB89_18
.LBB89_123:                             # %.lr.ph601.us.prol
	movq	176(%rsp), %rsi
	cmpb	$92, (%rsi)
	jne	.LBB89_125
# BB#124:
	movb	$47, (%rsi)
.LBB89_125:                             # %.lr.ph601.us.prol.loopexit
	movl	$1, %esi
	cmpl	$1, %edx
	je	.LBB89_132
	.p2align	4, 0x90
.LBB89_127:                             # %.lr.ph601.us
                                        # =>This Inner Loop Header: Depth=1
	movq	176(%rsp), %rdi
	cmpb	$92, (%rdi,%rsi)
	jne	.LBB89_129
# BB#128:                               #   in Loop: Header=BB89_127 Depth=1
	movb	$47, (%rdi,%rsi)
.LBB89_129:                             # %.lr.ph601.us.1720
                                        #   in Loop: Header=BB89_127 Depth=1
	movq	176(%rsp), %rdi
	cmpb	$92, 1(%rdi,%rsi)
	jne	.LBB89_131
# BB#130:                               #   in Loop: Header=BB89_127 Depth=1
	movb	$47, 1(%rdi,%rsi)
.LBB89_131:                             #   in Loop: Header=BB89_127 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB89_127
.LBB89_132:                             # %.preheader553.us
	cmpl	$2, %ecx
	jl	.LBB89_136
	.p2align	4, 0x90
.LBB89_133:                             # %.lr.ph603.us
                                        # =>This Inner Loop Header: Depth=1
	movq	176(%rsp), %rax
	cmpb	$47, -2(%rax,%rcx)
	jne	.LBB89_135
# BB#134:                               #   in Loop: Header=BB89_133 Depth=1
	movb	$0, -2(%rax,%rcx)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB89_133
.LBB89_135:                             # %.lr.ph603.us..critedge.preheader.us.loopexit_crit_edge
	decl	%ecx
	movl	%ecx, %eax
.LBB89_136:                             # %.critedge.preheader.us
	movl	%eax, %ecx
	movq	176(%rsp), %rdx
	movslq	%eax, %rsi
	leaq	(%rdx,%rsi), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB89_137:                             # %.critedge.us
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rsi,%rbp), %rax
	testq	%rax, %rax
	jle	.LBB89_138
# BB#142:                               #   in Loop: Header=BB89_137 Depth=1
	leaq	-1(%rbp), %rax
	cmpb	$47, -1(%rdi,%rbp)
	movq	%rax, %rbp
	jne	.LBB89_137
# BB#143:                               # %.critedge271.thread.us.loopexit
	leal	1(%rcx,%rax), %ecx
	jmp	.LBB89_144
.LBB89_138:                             # %.critedge271.us
	cmpl	$2, %ecx
	jl	.LBB89_140
# BB#139:                               # %.critedge271.us
	movl	%ecx, %eax
	addl	%ebp, %eax
	jne	.LBB89_140
# BB#141:
	xorl	%ecx, %ecx
	cmpb	$58, 1(%rdx)
	sete	%cl
	addl	%ecx, %ecx
	jmp	.LBB89_144
.LBB89_244:
	leaq	192(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB89_256
.LBB89_252:
	testq	%rsi, %rsi
	je	.LBB89_255
# BB#253:
	cmpl	%ebx, %r15d
	cmovgl	%ebx, %r15d
	movslq	%r15d, %rdx
	movq	%rax, %rdi
	callq	memcpy
	movl	programChecker(%rip), %eax
	subl	%ebx, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_255
# BB#254:
	movl	%eax, programChecker+4(%rip)
.LBB89_255:                             # %_ZN14ProgramChecker5allocEi.exit.i336
	movq	%rbp, %rdi
	callq	free
.LBB89_256:                             # %_ZN5ArrayIcLi0EE6resizeEi.exit338
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
.LBB89_257:                             # %_ZN5ArrayIcLi0EE6resizeEi.exit338
	leaq	200(%rsp), %rbx
	movq	%r15, %rdi
	callq	rewind
	movq	(%rbx), %rdi
	movl	$1, %esi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	fread
	cmpl	$2, %r12d
	jl	.LBB89_258
# BB#259:
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	negl	%eax
	movl	programChecker(%rip), %ecx
	subl	%r13d, %ecx
	movl	%ecx, programChecker(%rip)
	cmpl	programChecker+4(%rip), %ecx
	jle	.LBB89_261
# BB#260:
	movl	%ecx, programChecker+4(%rip)
.LBB89_261:                             # %_ZN14ProgramChecker5allocEi.exit.i.i339
	movslq	%eax, %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB89_262
# BB#264:
	movq	40(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB89_267
# BB#265:
	movq	(%rdi), %rax
	movq	%rax, (%r14)
	movl	programChecker(%rip), %eax
	addl	%r13d, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_267
# BB#266:
	movl	%eax, programChecker+4(%rip)
.LBB89_267:
	callq	free
	movl	programChecker(%rip), %eax
	subl	%r13d, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_269
# BB#268:
	movl	%eax, programChecker+4(%rip)
.LBB89_269:                             # %_ZN14ProgramChecker5allocEi.exit.i.i344
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	calloc
	testq	%rax, %rax
	je	.LBB89_270
# BB#272:
	movq	80(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB89_275
# BB#273:
	movq	(%rdi), %rcx
	movq	%rcx, (%rax)
	addl	programChecker(%rip), %r13d
	movl	%r13d, programChecker(%rip)
	cmpl	programChecker+4(%rip), %r13d
	jle	.LBB89_275
# BB#274:
	movl	%r13d, programChecker+4(%rip)
.LBB89_275:                             # %_ZN14ProgramChecker5allocEi.exit.i346
	movq	%rax, %rbx
	callq	free
	movq	%rbx, %rcx
	movq	72(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB89_276
.LBB89_258:
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
.LBB89_276:                             # %_ZN5ArrayIlLi0EE6resizeEi.exit348
	movq	%rbx, %r13
	movq	(%rbx), %rbx
	.p2align	4, 0x90
.LBB89_277:                             # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	je	.LBB89_279
# BB#278:                               #   in Loop: Header=BB89_277 Depth=1
	cmpb	$13, %al
	jne	.LBB89_277
.LBB89_279:                             # %.preheader540
	testl	%r12d, %r12d
	movq	%r14, 152(%rsp)         # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jle	.LBB89_280
# BB#282:                               # %.lr.ph594
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB89_283:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB89_284 Depth 2
                                        #     Child Loop BB89_287 Depth 2
	leaq	1(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%rbp,8)
	.p2align	4, 0x90
.LBB89_284:                             #   Parent Loop BB89_283 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	je	.LBB89_286
# BB#285:                               #   in Loop: Header=BB89_284 Depth=2
	cmpb	$9, %al
	jne	.LBB89_284
.LBB89_286:                             #   in Loop: Header=BB89_283 Depth=1
	movq	%rbx, (%r14,%rbp,8)
	decq	%rbx
	.p2align	4, 0x90
.LBB89_287:                             #   Parent Loop BB89_283 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx), %eax
	testb	%al, %al
	setne	%cl
	incq	%rbx
	cmpb	$13, %al
	je	.LBB89_289
# BB#288:                               #   in Loop: Header=BB89_287 Depth=2
	testb	%cl, %cl
	jne	.LBB89_287
.LBB89_289:                             #   in Loop: Header=BB89_283 Depth=1
	testb	%al, %al
	je	.LBB89_290
# BB#293:                               #   in Loop: Header=BB89_283 Depth=1
	movb	$0, -1(%rbx)
	incq	%rbp
	cmpq	%r12, %rbp
	jl	.LBB89_283
# BB#294:                               # %.loopexit541.loopexit
	movb	$1, %bl
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %r14d
	movq	72(%rsp), %rax          # 8-byte Reload
	jmp	.LBB89_295
.LBB89_280:                             # %.preheader540..loopexit541_crit_edge
	movb	$1, %bl
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %r14d
	movq	%rcx, %rax
.LBB89_295:                             # %.loopexit541
	movq	%rax, 80(%rsp)          # 8-byte Spill
	jmp	.LBB89_296
.LBB89_140:                             # %.critedge271.us..critedge271.thread.us_crit_edge
	addq	%rbp, %rcx
.LBB89_144:                             # %.critedge271.thread.us
.Ltmp330:
	leaq	208(%rsp), %rdi
	leaq	184(%rsp), %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_Z7putsizeR6StringS0_PKci
	movl	%eax, %ebx
.Ltmp331:
# BB#145:                               # %_Z6expandR6StringS0_PKci.exit.us
	testl	%ebx, %ebx
	jne	.LBB89_147
# BB#146:
	movq	176(%rsp), %rsi
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	printf
.LBB89_147:
	movl	programChecker(%rip), %eax
	subl	160(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_149
# BB#148:
	movl	%eax, programChecker+4(%rip)
.LBB89_149:                             # %._crit_edge610.loopexit
	movq	168(%rsp), %rdi
	callq	free
.LBB89_185:                             # %._crit_edge610
	testl	%ebx, %ebx
	jle	.LBB89_186
# BB#192:
	movq	32(%rsp), %rdi
	movl	$.L.str.44, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB89_193
# BB#195:
	movl	level(%rip), %edx
	movq	224(%rsp), %rcx
	movl	$.L.str.45, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	fprintf
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	cmpl	$2, %ebx
	jl	.LBB89_196
# BB#197:
	movq	%rbp, %r13
	leal	(,%rbx,8), %ebp
	movl	programChecker(%rip), %r12d
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	leal	(%r12,%rbx,8), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r15d
	cmpl	%r15d, %eax
	jle	.LBB89_199
# BB#198:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r15d
.LBB89_199:                             # %_ZN14ProgramChecker5allocEi.exit.i.i321
	movslq	%ebp, %r14
	movl	$1, %esi
	movq	%r14, %rdi
	callq	calloc
	testq	%rax, %rax
	je	.LBB89_200
# BB#202:
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rcx
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	%rcx, (%rax)
	movl	%r12d, programChecker(%rip)
	cmpl	%r15d, %r12d
	jle	.LBB89_204
# BB#203:
	movl	%r12d, programChecker+4(%rip)
.LBB89_204:
	callq	free
	movl	programChecker(%rip), %ebx
	addl	%ebx, %ebp
	movl	%ebp, programChecker(%rip)
	movl	programChecker+4(%rip), %r15d
	cmpl	%r15d, %ebp
	jle	.LBB89_206
# BB#205:
	movl	%ebp, programChecker+4(%rip)
	movl	%ebp, %r15d
.LBB89_206:                             # %_ZN14ProgramChecker5allocEi.exit.i.i324
	movl	$1, %esi
	movq	%r14, %rdi
	callq	calloc
	testq	%rax, %rax
	je	.LBB89_207
# BB#209:
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rcx
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rcx, (%rax)
	movl	%ebx, programChecker(%rip)
	cmpl	%r15d, %ebx
	movq	8(%rsp), %r14           # 8-byte Reload
	jle	.LBB89_211
# BB#210:
	movl	%ebx, programChecker+4(%rip)
.LBB89_211:                             # %_ZN14ProgramChecker5allocEi.exit.i325
	callq	free
	movq	%r13, %rbp
	jmp	.LBB89_212
.LBB89_196:
	movl	$1, %r14d
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB89_212:                             # %_ZN5ArrayIlLi0EE6resizeEi.exit
	movq	200(%rsp), %rbx
	movq	%rbp, %rdi
	callq	rewind
.Ltmp333:
	movq	%rbp, %r12
	movq	%rbp, %rdi
	callq	_Z7getlineP8_IO_FILE
.Ltmp334:
# BB#213:                               # %.lr.ph597
	movq	%r14, %r15
	movq	%r14, %rbp
	movslq	%ebp, %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB89_214:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB89_216 Depth 2
.Ltmp336:
	movq	%r12, %rdi
	callq	_Z7getlineP8_IO_FILE
.Ltmp337:
# BB#215:                               #   in Loop: Header=BB89_214 Depth=1
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%rbp,8)
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rbx, (%rax,%rbp,8)
	.p2align	4, 0x90
.LBB89_216:                             #   Parent Loop BB89_214 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$10, (%rbx)
	leaq	1(%rbx), %rbx
	jne	.LBB89_216
# BB#217:                               #   in Loop: Header=BB89_214 Depth=1
	movb	$0, -1(%rbx)
	incq	%rbp
	cmpq	%r14, %rbp
	jl	.LBB89_214
# BB#218:                               # %._crit_edge598
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	fseek
	movl	programChecker(%rip), %eax
	subl	208(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_220
# BB#219:
	movl	%eax, programChecker+4(%rip)
.LBB89_220:
	movq	216(%rsp), %rdi
	callq	free
	xorl	%ebx, %ebx
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r15, %r14
	movq	%r12, %r15
.LBB89_296:                             # %.loopexit541
	movl	level(%rip), %ecx
	cmpl	$12, %ecx
	movl	88(%rsp), %r12d         # 4-byte Reload
	ja	.LBB89_298
# BB#297:
	movl	$524288, %esi           # imm = 0x80000
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
.Ltmp339:
	movl	$buf, %edi
	callq	_ZN5ArrayIhLi0EE6resizeEi
.Ltmp340:
.LBB89_298:                             # %_ZN3Buf7setsizeEi.exit.preheader
	testl	%r14d, %r14d
	jle	.LBB89_299
# BB#300:                               # %.lr.ph590
	movl	%r14d, %eax
	cmpl	$3, %r14d
	jbe	.LBB89_301
# BB#304:                               # %min.iters.checked
	movl	%r14d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB89_301
# BB#305:                               # %vector.body.preheader
	movq	72(%rsp), %rsi          # 8-byte Reload
	leaq	16(%rsi), %rsi
	pxor	%xmm0, %xmm0
	movq	%rcx, %rdi
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB89_306:                             # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rsi), %xmm2
	movdqu	(%rsi), %xmm3
	paddq	%xmm2, %xmm0
	paddq	%xmm3, %xmm1
	addq	$32, %rsi
	addq	$-4, %rdi
	jne	.LBB89_306
# BB#307:                               # %middle.block
	paddq	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddq	%xmm1, %xmm0
	movd	%xmm0, %rbp
	testl	%edx, %edx
	jne	.LBB89_302
	jmp	.LBB89_308
.LBB89_301:
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
.LBB89_302:                             # %_ZN3Buf7setsizeEi.exit.preheader705
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB89_303:                             # %_ZN3Buf7setsizeEi.exit
                                        # =>This Inner Loop Header: Depth=1
	addq	(%rdx), %rbp
	addq	$8, %rdx
	decq	%rax
	jne	.LBB89_303
	jmp	.LBB89_308
.LBB89_299:
	xorl	%ebp, %ebp
.LBB89_308:                             # %_ZN3Buf7setsizeEi.exit._crit_edge
	movl	$2048, 208(%rsp)        # imm = 0x800
	movl	%r12d, 212(%rsp)
	movq	%r15, 216(%rsp)
	movl	$0, 224(%rsp)
	movl	$-1, 228(%rsp)
	movl	$0, 232(%rsp)
	cmpl	$0, level(%rip)
	movq	$0, 240(%rsp)
	setg	%al
	andb	%al, %bl
	cmpb	$1, %bl
	movq	%r14, 8(%rsp)           # 8-byte Spill
	jne	.LBB89_310
# BB#309:                               # %.preheader13.preheader.i
	movq	%r15, %rdi
	callq	_IO_getc
	movzbl	%al, %r14d
	movl	%r14d, 232(%rsp)
	shll	$8, %r14d
	movq	216(%rsp), %rdi
	callq	_IO_getc
	movzbl	%al, %ebx
	orl	%r14d, %ebx
	movl	%ebx, 232(%rsp)
	shll	$8, %ebx
	movq	216(%rsp), %rdi
	callq	_IO_getc
	movzbl	%al, %r14d
	orl	%ebx, %r14d
	movl	%r14d, 232(%rsp)
	shll	$8, %r14d
	movq	216(%rsp), %rdi
	callq	_IO_getc
	movzbl	%al, %eax
	orl	%r14d, %eax
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	%eax, 232(%rsp)
.LBB89_310:                             # %.preheader.i.preheader
	movq	$-2048, %rcx            # imm = 0xF800
	.p2align	4, 0x90
.LBB89_311:                             # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	leal	2051(%rcx), %esi
	movl	$16384, %eax            # imm = 0x4000
	xorl	%edx, %edx
	idivl	%esi
	movl	%eax, _ZL2dt+4096(%rcx,%rcx)
	leal	2053(%rcx), %esi
	movl	$16384, %eax            # imm = 0x4000
	xorl	%edx, %edx
	idivl	%esi
	movl	%eax, _ZL2dt+4100(%rcx,%rcx)
	addq	$4, %rcx
	jne	.LBB89_311
# BB#312:                               # %_ZN7EncoderC2E4ModeP8_IO_FILE.exit
	testl	%r12d, %r12d
	movl	136(%rsp), %ebx         # 4-byte Reload
	movq	%r15, 96(%rsp)          # 8-byte Spill
	je	.LBB89_313
# BB#322:
	xorl	%eax, %eax
	cmpl	$2, %ebx
	setg	%al
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	8(%rcx,%rax,8), %rsi
.Ltmp342:
	leaq	48(%rsp), %rdi
	callq	_ZN6StringC2EPKc
.Ltmp343:
# BB#323:
	cmpl	$2, %ebx
	jne	.LBB89_351
# BB#324:
	movslq	48(%rsp), %rbx
	movq	%rbx, %rcx
	addq	$-2, %rcx
	testl	%ecx, %ecx
	js	.LBB89_336
# BB#325:                               # %.lr.ph586
	movq	64(%rsp), %rax
	movl	$2, %esi
	subl	%ebx, %esi
	.p2align	4, 0x90
.LBB89_326:                             # =>This Inner Loop Header: Depth=1
	movzbl	(%rax,%rcx), %edx
	cmpb	$47, %dl
	je	.LBB89_331
# BB#327:                               #   in Loop: Header=BB89_326 Depth=1
	cmpb	$92, %dl
	je	.LBB89_331
# BB#328:                               #   in Loop: Header=BB89_326 Depth=1
	cmpl	$-1, %esi
	jne	.LBB89_334
# BB#329:                               #   in Loop: Header=BB89_326 Depth=1
	cmpb	$58, %dl
	je	.LBB89_330
.LBB89_334:                             #   in Loop: Header=BB89_326 Depth=1
	incl	%esi
	testq	%rcx, %rcx
	leaq	-1(%rcx), %rcx
	jg	.LBB89_326
# BB#335:                               # %._crit_edge587.loopexit
	negl	%esi
	movl	%esi, %ecx
.LBB89_336:                             # %._crit_edge587
	cmpl	$-1, %ecx
	jne	.LBB89_351
# BB#337:
	cmpl	$2, 52(%rsp)
	jge	.LBB89_338
# BB#339:
	movq	56(%rsp), %r14
	movq	64(%rsp), %r12
	movabsq	$8589934594, %rax       # imm = 0x200000002
	movq	%rax, 48(%rsp)
	movl	programChecker(%rip), %r15d
	leal	2(%r15), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r13d
	cmpl	%r13d, %eax
	jle	.LBB89_341
# BB#340:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r13d
.LBB89_341:                             # %_ZN14ProgramChecker5allocEi.exit.i.i354
	movl	$2, %edi
	movl	$1, %esi
	callq	calloc
	movq	%rax, 56(%rsp)
	testq	%rax, %rax
	je	.LBB89_342
# BB#344:                               # %_ZN5ArrayIcLi0EE6createEi.exit.i355
	leaq	64(%rsp), %rbp
	movq	%rax, 64(%rsp)
	testq	%r14, %r14
	je	.LBB89_349
# BB#345:
	testq	%r12, %r12
	je	.LBB89_348
# BB#346:
	cmpl	$3, %ebx
	movl	$2, %ecx
	cmovll	%ebx, %ecx
	movslq	%ecx, %rdx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	memcpy
	movl	%r15d, programChecker(%rip)
	cmpl	%r13d, %r15d
	jle	.LBB89_348
# BB#347:
	movl	%r15d, programChecker+4(%rip)
.LBB89_348:                             # %_ZN14ProgramChecker5allocEi.exit.i356
	movq	%r14, %rdi
	callq	free
.LBB89_349:                             # %_ZN6StringaSEPKc.exit
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	jmp	.LBB89_350
.LBB89_313:                             # %.preheader
	testl	%r14d, %r14d
	jle	.LBB89_317
# BB#314:                               # %.lr.ph
	movslq	8(%rsp), %r15           # 4-byte Folded Reload
	xorl	%ebx, %ebx
	leaq	208(%rsp), %r14
	.p2align	4, 0x90
.LBB89_315:                             # =>This Inner Loop Header: Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
.Ltmp366:
	movq	%r14, %rdx
	callq	_Z8compressPKclR7Encoder
.Ltmp367:
# BB#316:                               #   in Loop: Header=BB89_315 Depth=1
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB89_315
.LBB89_317:                             # %._crit_edge
	cmpl	$0, 212(%rsp)
	jne	.LBB89_320
# BB#318:                               # %._crit_edge
	movl	level(%rip), %eax
	testl	%eax, %eax
	jle	.LBB89_320
# BB#319:
	movzbl	227(%rsp), %edi
	movq	216(%rsp), %rsi
	callq	_IO_putc
.LBB89_320:
	movq	216(%rsp), %rdi
	callq	ftell
	movq	%rax, %rcx
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rcx, %rdx
	callq	printf
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	jmp	.LBB89_416
.LBB89_330:
	movl	$2, %ecx
.LBB89_331:                             # %.thread532
	movslq	%ecx, %rcx
	movb	$0, (%rax,%rcx)
	jmp	.LBB89_351
.LBB89_338:
	movl	$2, 48(%rsp)
	leaq	64(%rsp), %rbp
.LBB89_350:                             # %_ZN6StringaSEPKc.exit
	movq	(%rbp), %rax
	movw	$46, (%rax)
.LBB89_351:
	movq	64(%rsp), %r12
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbp
	incq	%rbp
	cmpl	%ebp, 52(%rsp)
	jge	.LBB89_352
# BB#353:
	movq	56(%rsp), %r13
	movl	48(%rsp), %ebx
	movl	%ebp, 52(%rsp)
	movl	%ebp, 48(%rsp)
	testl	%ebp, %ebp
	jle	.LBB89_354
# BB#355:
	movslq	%ebp, %rdi
	movl	programChecker(%rip), %eax
	addl	%ebp, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_357
# BB#356:
	movl	%eax, programChecker+4(%rip)
.LBB89_357:                             # %_ZN14ProgramChecker5allocEi.exit.i.i361
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rdi
	movq	%rdi, 56(%rsp)
	testq	%rdi, %rdi
	je	.LBB89_358
# BB#360:
	movq	%rdi, 64(%rsp)
	testq	%r13, %r13
	jne	.LBB89_362
	jmp	.LBB89_366
.LBB89_352:
	movl	%ebp, 48(%rsp)
	movq	%r12, %rdi
	jmp	.LBB89_366
.LBB89_354:
	leaq	56(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	xorl	%edi, %edi
	testq	%r13, %r13
	je	.LBB89_366
.LBB89_362:
	testq	%r12, %r12
	je	.LBB89_365
# BB#363:
	cmpl	%ebp, %ebx
	cmovgl	%ebp, %ebx
	movslq	%ebx, %rdx
	movq	%r12, %rsi
	callq	memcpy
	movl	programChecker(%rip), %eax
	subl	%ebp, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_365
# BB#364:
	movl	%eax, programChecker+4(%rip)
.LBB89_365:                             # %_ZN14ProgramChecker5allocEi.exit.i363
	movq	%r13, %rdi
	callq	free
	movq	64(%rsp), %rdi
.LBB89_366:
	movq	%r12, %rsi
	callq	strcpy
	movq	64(%rsp), %rbp
	cmpb	$0, (%rbp)
	je	.LBB89_402
# BB#367:
	movl	48(%rsp), %ebx
	cmpl	$3, %ebx
	jne	.LBB89_369
# BB#368:
	cmpb	$58, 1(%rbp)
	jne	.LBB89_370
	jmp	.LBB89_402
.LBB89_369:
	testl	%ebx, %ebx
	jle	.LBB89_371
.LBB89_370:                             # %.thread676
	decl	%ebx
	movl	%ebx, 48(%rsp)
.LBB89_371:                             # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i367
	cmpl	52(%rsp), %ebx
	jne	.LBB89_387
# BB#372:
	leal	(%rbx,%rbx), %eax
	testl	%eax, %eax
	movl	$1, %r14d
	cmovgl	%eax, %r14d
	cmpl	%r14d, %ebx
	jge	.LBB89_373
# BB#374:
	movq	56(%rsp), %r12
	movl	%r14d, 52(%rsp)
	movl	%r14d, 48(%rsp)
	movl	%r14d, %edi
	movl	programChecker(%rip), %r15d
	leal	(%r15,%r14), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r13d
	cmpl	%r13d, %eax
	jle	.LBB89_376
# BB#375:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r13d
.LBB89_376:                             # %_ZN14ProgramChecker5allocEi.exit.i.i378
	movl	$1, %esi
	callq	calloc
	movq	%rax, 56(%rsp)
	testq	%rax, %rax
	je	.LBB89_377
# BB#379:
	movq	%rax, 64(%rsp)
	testq	%r12, %r12
	je	.LBB89_380
# BB#381:
	testq	%rbp, %rbp
	je	.LBB89_384
# BB#382:
	cmpl	%r14d, %ebx
	cmovlel	%ebx, %r14d
	movslq	%r14d, %rdx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	memcpy
	movl	%r15d, programChecker(%rip)
	cmpl	%r13d, %r15d
	jle	.LBB89_384
# BB#383:
	movl	%r15d, programChecker+4(%rip)
.LBB89_384:                             # %_ZN14ProgramChecker5allocEi.exit.i380
	movq	%r12, %rdi
	callq	free
	movq	64(%rsp), %rbp
	jmp	.LBB89_385
.LBB89_373:
	movl	%r14d, 48(%rsp)
	jmp	.LBB89_386
.LBB89_380:
	movq	%rax, %rbp
.LBB89_385:                             # %.noexc375
	movq	96(%rsp), %r15          # 8-byte Reload
.LBB89_386:                             # %.noexc375
	movl	%ebx, 48(%rsp)
	movq	8(%rsp), %r14           # 8-byte Reload
.LBB89_387:                             # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i373
	leal	1(%rbx), %eax
	movl	%eax, 48(%rsp)
	movslq	%ebx, %rax
	movb	$47, (%rbp,%rax)
	movl	48(%rsp), %ebx
	cmpl	52(%rsp), %ebx
	jne	.LBB89_401
# BB#388:
	leal	(%rbx,%rbx), %eax
	testl	%eax, %eax
	movl	$1, %ebp
	cmovgl	%eax, %ebp
	cmpl	%ebp, %ebx
	jge	.LBB89_389
# BB#390:
	movq	56(%rsp), %r14
	movq	64(%rsp), %r12
	movl	%ebp, 52(%rsp)
	movl	%ebp, 48(%rsp)
	movl	%ebp, %edi
	movl	programChecker(%rip), %r15d
	leal	(%r15,%rbp), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r13d
	cmpl	%r13d, %eax
	jle	.LBB89_392
# BB#391:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r13d
.LBB89_392:                             # %_ZN14ProgramChecker5allocEi.exit.i.i427
	movl	$1, %esi
	callq	calloc
	movq	%rax, 56(%rsp)
	testq	%rax, %rax
	je	.LBB89_393
# BB#395:
	movq	%rax, 64(%rsp)
	testq	%r14, %r14
	je	.LBB89_400
# BB#396:
	testq	%r12, %r12
	je	.LBB89_399
# BB#397:
	cmpl	%ebp, %ebx
	cmovlel	%ebx, %ebp
	movslq	%ebp, %rdx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	memcpy
	movl	%r15d, programChecker(%rip)
	cmpl	%r13d, %r15d
	jle	.LBB89_399
# BB#398:
	movl	%r15d, programChecker+4(%rip)
.LBB89_399:                             # %_ZN14ProgramChecker5allocEi.exit.i429
	movq	%r14, %rdi
	callq	free
	jmp	.LBB89_400
.LBB89_389:
	movl	%ebp, 48(%rsp)
.LBB89_400:                             # %.noexc376
	movl	%ebx, 48(%rsp)
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
.LBB89_401:                             # %_ZN6StringpLEPKc.exit377
	movslq	%ebx, %rax
	movq	64(%rsp), %rcx
	incl	%ebx
	movl	%ebx, 48(%rsp)
	movb	$0, (%rcx,%rax)
.LBB89_402:                             # %.preheader539
	testl	%r14d, %r14d
	jle	.LBB89_413
# BB#403:                               # %.lr.ph582
	movslq	%r14d, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB89_404:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB89_409 Depth 2
	movq	64(%rsp), %rsi
.Ltmp355:
	leaq	104(%rsp), %rdi
	callq	_ZN6StringC2EPKc
.Ltmp356:
# BB#405:                               #   in Loop: Header=BB89_404 Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %r14
	movl	104(%rsp), %r15d
	testl	%r15d, %r15d
	jle	.LBB89_407
# BB#406:                               #   in Loop: Header=BB89_404 Depth=1
	decl	%r15d
	movl	%r15d, 104(%rsp)
.LBB89_407:                             # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i386
                                        #   in Loop: Header=BB89_404 Depth=1
	movb	(%r14), %al
	testb	%al, %al
	je	.LBB89_437
# BB#408:                               # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i389.preheader
                                        #   in Loop: Header=BB89_404 Depth=1
	incq	%r14
	.p2align	4, 0x90
.LBB89_409:                             # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i389
                                        #   Parent Loop BB89_404 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	108(%rsp), %r15d
	jne	.LBB89_436
# BB#410:                               #   in Loop: Header=BB89_409 Depth=2
	leal	(%r15,%r15), %r13d
	testl	%r13d, %r13d
	movl	$1, %eax
	cmovlel	%eax, %r13d
	cmpl	%r13d, %r15d
	jge	.LBB89_411
# BB#425:                               #   in Loop: Header=BB89_409 Depth=2
	movq	112(%rsp), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	120(%rsp), %rbp
	movl	%r13d, 108(%rsp)
	movl	%r13d, 104(%rsp)
	movl	%r13d, %edi
	movl	programChecker(%rip), %ebx
	leal	(%rbx,%r13), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r12d
	cmpl	%r12d, %eax
	jle	.LBB89_427
# BB#426:                               #   in Loop: Header=BB89_409 Depth=2
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r12d
.LBB89_427:                             # %_ZN14ProgramChecker5allocEi.exit.i.i397
                                        #   in Loop: Header=BB89_409 Depth=2
	movl	$1, %esi
	callq	calloc
	movq	%rax, 112(%rsp)
	testq	%rax, %rax
	je	.LBB89_428
# BB#430:                               #   in Loop: Header=BB89_409 Depth=2
	movq	%rax, 120(%rsp)
	cmpq	$0, 88(%rsp)            # 8-byte Folded Reload
	je	.LBB89_435
# BB#431:                               #   in Loop: Header=BB89_409 Depth=2
	testq	%rbp, %rbp
	je	.LBB89_434
# BB#432:                               #   in Loop: Header=BB89_409 Depth=2
	cmpl	%r13d, %r15d
	cmovlel	%r15d, %r13d
	movslq	%r13d, %rdx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	memcpy
	movl	%ebx, programChecker(%rip)
	cmpl	%r12d, %ebx
	jle	.LBB89_434
# BB#433:                               #   in Loop: Header=BB89_409 Depth=2
	movl	%ebx, programChecker+4(%rip)
.LBB89_434:                             # %_ZN14ProgramChecker5allocEi.exit.i399
                                        #   in Loop: Header=BB89_409 Depth=2
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB89_435
	.p2align	4, 0x90
.LBB89_411:                             #   in Loop: Header=BB89_409 Depth=2
	movl	%r13d, 104(%rsp)
.LBB89_435:                             # %.noexc394
                                        #   in Loop: Header=BB89_409 Depth=2
	movl	%r15d, 104(%rsp)
	movzbl	-1(%r14), %eax
.LBB89_436:                             # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i392
                                        #   in Loop: Header=BB89_409 Depth=2
	movq	120(%rsp), %rcx
	leal	1(%r15), %edx
	movl	%edx, 104(%rsp)
	movslq	%r15d, %rdx
	movb	%al, (%rcx,%rdx)
	movzbl	(%r14), %eax
	movl	104(%rsp), %r15d
	incq	%r14
	testb	%al, %al
	jne	.LBB89_409
.LBB89_437:                             # %_ZN5ArrayIcLi0EE8pop_backEv.exit._crit_edge.i393
                                        #   in Loop: Header=BB89_404 Depth=1
	cmpl	108(%rsp), %r15d
	jne	.LBB89_451
# BB#438:                               #   in Loop: Header=BB89_404 Depth=1
	leal	(%r15,%r15), %ebp
	testl	%ebp, %ebp
	movl	$1, %eax
	cmovlel	%eax, %ebp
	cmpl	%ebp, %r15d
	jge	.LBB89_439
# BB#440:                               #   in Loop: Header=BB89_404 Depth=1
	movq	112(%rsp), %r14
	movq	120(%rsp), %r12
	movl	%ebp, 108(%rsp)
	movl	%ebp, 104(%rsp)
	movl	%ebp, %edi
	movl	programChecker(%rip), %r13d
	leal	(%r13,%rbp), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %ebx
	cmpl	%ebx, %eax
	jle	.LBB89_442
# BB#441:                               #   in Loop: Header=BB89_404 Depth=1
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %ebx
.LBB89_442:                             # %_ZN14ProgramChecker5allocEi.exit.i.i408
                                        #   in Loop: Header=BB89_404 Depth=1
	movl	$1, %esi
	callq	calloc
	movq	%rax, 112(%rsp)
	testq	%rax, %rax
	je	.LBB89_443
# BB#445:                               #   in Loop: Header=BB89_404 Depth=1
	movq	%rax, 120(%rsp)
	testq	%r14, %r14
	je	.LBB89_450
# BB#446:                               #   in Loop: Header=BB89_404 Depth=1
	testq	%r12, %r12
	je	.LBB89_449
# BB#447:                               #   in Loop: Header=BB89_404 Depth=1
	cmpl	%ebp, %r15d
	cmovlel	%r15d, %ebp
	movslq	%ebp, %rdx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	memcpy
	movl	%r13d, programChecker(%rip)
	cmpl	%ebx, %r13d
	jle	.LBB89_449
# BB#448:                               #   in Loop: Header=BB89_404 Depth=1
	movl	%r13d, programChecker+4(%rip)
.LBB89_449:                             # %_ZN14ProgramChecker5allocEi.exit.i410
                                        #   in Loop: Header=BB89_404 Depth=1
	movq	%r14, %rdi
	callq	free
	jmp	.LBB89_450
	.p2align	4, 0x90
.LBB89_439:                             #   in Loop: Header=BB89_404 Depth=1
	movl	%ebp, 104(%rsp)
.LBB89_450:                             # %.noexc395
                                        #   in Loop: Header=BB89_404 Depth=1
	movl	%r15d, 104(%rsp)
.LBB89_451:                             #   in Loop: Header=BB89_404 Depth=1
	movq	120(%rsp), %rax
	leal	1(%r15), %ecx
	movl	%ecx, 104(%rsp)
	movslq	%r15d, %rcx
	movb	$0, (%rax,%rcx)
	movq	120(%rsp), %rdi
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rsi
.Ltmp360:
	leaq	208(%rsp), %rdx
	callq	_Z10decompressPKclR7Encoder
.Ltmp361:
# BB#452:                               #   in Loop: Header=BB89_404 Depth=1
	movl	programChecker(%rip), %eax
	subl	104(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	movq	8(%rsp), %r14           # 8-byte Reload
	jle	.LBB89_454
# BB#453:                               #   in Loop: Header=BB89_404 Depth=1
	movl	%eax, programChecker+4(%rip)
.LBB89_454:                             # %_ZN5ArrayIcLi0EED2Ev.exit403
                                        #   in Loop: Header=BB89_404 Depth=1
	movq	112(%rsp), %rdi
	callq	free
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	cmpq	136(%rsp), %rcx         # 8-byte Folded Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	jl	.LBB89_404
.LBB89_413:                             # %._crit_edge583
	movl	programChecker(%rip), %eax
	subl	48(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_415
# BB#414:
	movl	%eax, programChecker+4(%rip)
.LBB89_415:                             # %_ZN5ArrayIcLi0EED2Ev.exit384
	movq	56(%rsp), %rdi
	callq	free
.LBB89_416:
	movq	%r15, %rdi
	callq	fclose
	movl	programChecker(%rip), %eax
	subl	184(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_418
# BB#417:
	movl	%eax, programChecker+4(%rip)
.LBB89_418:                             # %_ZN5ArrayIcLi0EED2Ev.exit414
	movq	192(%rsp), %rdi
	callq	free
	movl	programChecker(%rip), %eax
	subl	16(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_420
# BB#419:
	movl	%eax, programChecker+4(%rip)
.LBB89_420:                             # %_ZN5ArrayIcLi0EED2Ev.exit416
	movq	24(%rsp), %rdi
	callq	free
	shll	$3, %r14d
	movl	programChecker(%rip), %eax
	subl	%r14d, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_422
# BB#421:
	movl	%eax, programChecker+4(%rip)
.LBB89_422:                             # %_ZN5ArrayIlLi0EED2Ev.exit
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	programChecker(%rip), %eax
	subl	%r14d, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_424
# BB#423:
	movl	%eax, programChecker+4(%rip)
.LBB89_424:                             # %_ZN5ArrayIPcLi0EED2Ev.exit
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	cmpb	$0, 132(%rsp)           # 1-byte Folded Reload
	jne	.LBB89_484
	jmp	.LBB89_485
.LBB89_428:
.Ltmp358:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp359:
# BB#429:                               # %.noexc400
.LBB89_290:
	movq	32(%rsp), %rsi
	decq	%rbx
	subq	(%r13), %rbx
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	printf
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$0, (%rax)
.Ltmp308:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp309:
# BB#291:                               # %.noexc349
.LBB89_85:
.Ltmp293:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp294:
# BB#86:                                # %.noexc303
.LBB89_1:
	movl	%edx, 132(%rsp)         # 4-byte Spill
.LBB89_2:                               # %.thread501
	movl	$.L.str.40, %edi
	movl	$5, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$0, (%rax)
.Ltmp287:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp288:
# BB#16:                                # %.noexc281
.LBB89_21:
.Ltmp388:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp389:
# BB#22:                                # %.noexc283
.LBB89_26:
.Ltmp385:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp386:
# BB#27:                                # %.noexc285
.LBB89_228:
	movq	32(%rsp), %rdi
	callq	perror
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$0, (%rax)
	movl	$1, %r14d
.Ltmp317:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp318:
# BB#229:                               # %.noexc331
.LBB89_232:
	movq	32(%rsp), %rsi
	movl	$.L.str.49, %edi
	movl	$.L.str.50, %edx
	xorl	%eax, %eax
	callq	printf
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$0, (%rax)
.Ltmp300:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp301:
# BB#233:                               # %.noexc333
.LBB89_186:                             # %._crit_edge610.thread
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.43, (%rax)
	movl	$1, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
.Ltmp375:
	movl	$1, %r12d
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp376:
# BB#191:                               # %.noexc317
.LBB89_443:
.Ltmp363:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp364:
# BB#444:                               # %.noexc411
.LBB89_193:
	movq	32(%rsp), %rdi
	callq	perror
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$0, (%rax)
	movl	$1, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
.Ltmp373:
	movl	$1, %r12d
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp374:
# BB#194:                               # %.noexc319
.LBB89_262:
	movq	%rbp, %rbx
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
.Ltmp314:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp315:
# BB#263:                               # %.noexc342
.LBB89_270:
.Ltmp311:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp312:
# BB#271:                               # %.noexc347
.LBB89_53:
.Ltmp382:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp383:
# BB#54:                                # %.noexc289
.LBB89_67:
.Ltmp380:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp381:
# BB#68:                                # %.noexc311
.LBB89_100:
.Ltmp378:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp379:
# BB#101:                               # %.noexc307
.LBB89_200:
	movl	$1, %r12d
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
.Ltmp371:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp372:
# BB#201:                               # %.noexc323
.LBB89_207:
	movq	8(%rsp), %r12           # 8-byte Reload
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
.Ltmp369:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp370:
# BB#208:                               # %.noexc326
.LBB89_10:
	movl	$0, 132(%rsp)           # 4-byte Folded Spill
	jmp	.LBB89_2
.LBB89_248:
	movl	$1, %ebx
.Ltmp306:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp307:
# BB#249:                               # %.noexc337
.LBB89_358:
.Ltmp348:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp349:
# BB#359:                               # %.noexc364
.LBB89_12:
	movl	%edx, 132(%rsp)         # 4-byte Spill
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.39, (%rax)
.Ltmp285:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp286:
# BB#13:                                # %.noexc
.LBB89_377:
.Ltmp352:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp353:
# BB#378:                               # %.noexc381
.LBB89_393:
.Ltmp350:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp351:
# BB#394:                               # %.noexc430
.LBB89_342:
.Ltmp345:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp346:
# BB#343:                               # %.noexc357
.LBB89_332:
.Ltmp347:
	jmp	.LBB89_333
.LBB89_412:
.Ltmp354:
.LBB89_333:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB89_461
.LBB89_292:
.Ltmp313:
	movq	%rdx, %r13
	movq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %r14d
	jmp	.LBB89_467
.LBB89_281:
.Ltmp316:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movl	$1, %r12d
	movq	%rbx, %r14
	jmp	.LBB89_467
.LBB89_236:                             # %.loopexit.split-lp543
.Ltmp302:
	jmp	.LBB89_235
.LBB89_40:
.Ltmp387:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movl	$1, %r14d
	jmp	.LBB89_476
.LBB89_39:
.Ltmp390:
	jmp	.LBB89_15
.LBB89_165:                             # %.loopexit.split-lp556
.Ltmp377:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB89_166
.LBB89_14:
.Ltmp289:
.LBB89_15:
	movq	%rdx, %r13
	movq	%rax, %rbp
	jmp	.LBB89_479
.LBB89_42:
.Ltmp384:
	jmp	.LBB89_43
.LBB89_488:
.Ltmp310:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %r14d
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 80(%rsp)          # 8-byte Spill
	jmp	.LBB89_467
.LBB89_457:                             # %.loopexit.split-lp
.Ltmp365:
	jmp	.LBB89_458
.LBB89_153:                             # %.us-lcssa.us
.Ltmp332:
	jmp	.LBB89_188
.LBB89_150:                             # %.loopexit555.us-lcssa.us
.Ltmp329:
	jmp	.LBB89_151
.LBB89_222:                             # %.loopexit.split-lp549
.Ltmp335:
	movq	%r14, %r15
	jmp	.LBB89_223
.LBB89_321:
.Ltmp344:
	jmp	.LBB89_465
.LBB89_163:
.Ltmp297:
.LBB89_43:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movl	$1, %r14d
	movl	$1, %r12d
	jmp	.LBB89_470
.LBB89_41:
.Ltmp292:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movl	$1, %r14d
	movl	$1, %r12d
	jmp	.LBB89_473
.LBB89_464:
.Ltmp341:
.LBB89_465:
	movq	%rdx, %r13
	movq	%rax, %rbp
	jmp	.LBB89_466
.LBB89_489:
.Ltmp368:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB89_466
.LBB89_456:                             # %.loopexit
.Ltmp362:
.LBB89_458:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	programChecker(%rip), %eax
	subl	104(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_460
# BB#459:
	movl	%eax, programChecker+4(%rip)
.LBB89_460:                             # %_ZN5ArrayIcLi0EED2Ev.exit405
	movq	112(%rsp), %rdi
	callq	free
	jmp	.LBB89_461
.LBB89_455:
.Ltmp357:
	movq	%rdx, %r13
	movq	%rax, %rbp
.LBB89_461:
	movl	programChecker(%rip), %eax
	subl	48(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_463
# BB#462:
	movl	%eax, programChecker+4(%rip)
.LBB89_463:                             # %_ZN5ArrayIcLi0EED2Ev.exit407
	movq	56(%rsp), %rdi
	callq	free
.LBB89_466:
	movl	%r14d, %r12d
	jmp	.LBB89_467
.LBB89_187:                             # %.us-lcssa
.Ltmp326:
.LBB89_188:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movl	programChecker(%rip), %eax
	subl	160(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_190
# BB#189:
	movl	%eax, programChecker+4(%rip)
.LBB89_190:                             # %_ZN5ArrayIcLi0EED2Ev.exit316
	movq	168(%rsp), %rdi
	callq	free
	jmp	.LBB89_152
.LBB89_164:                             # %.loopexit555.us-lcssa
.Ltmp323:
.LBB89_151:                             # %.loopexit555
	movq	%rdx, %r13
	movq	%rax, %rbp
.LBB89_152:                             # %.loopexit555
	movl	$1, %r14d
	movl	$1, %r12d
.LBB89_166:                             # %.loopexit555
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB89_224
.LBB89_221:                             # %.loopexit548
.Ltmp338:
.LBB89_223:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%r15, %r14
	movl	%r14d, %r12d
.LBB89_224:
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	programChecker(%rip), %eax
	subl	208(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_226
# BB#225:
	movl	%eax, programChecker+4(%rip)
.LBB89_226:                             # %_ZN5ArrayIcLi0EED2Ev.exit330
	movq	216(%rsp), %rdi
	callq	free
	jmp	.LBB89_467
.LBB89_234:                             # %.loopexit542
.Ltmp305:
.LBB89_235:
	movq	%rdx, %r13
	movq	%rax, %rbp
	movl	$1, %r14d
	movl	$1, %r12d
.LBB89_467:
	movl	programChecker(%rip), %eax
	subl	184(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_469
# BB#468:
	movl	%eax, programChecker+4(%rip)
.LBB89_469:                             # %_ZN5ArrayIcLi0EED2Ev.exit420
	movq	192(%rsp), %rdi
	callq	free
.LBB89_470:
	movl	programChecker(%rip), %eax
	subl	16(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB89_472
# BB#471:
	movl	%eax, programChecker+4(%rip)
.LBB89_472:                             # %_ZN5ArrayIcLi0EED2Ev.exit422
	movq	24(%rsp), %rdi
	callq	free
.LBB89_473:
	shll	$3, %r12d
	movl	programChecker(%rip), %eax
	subl	%r12d, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	movq	80(%rsp), %rdi          # 8-byte Reload
	jle	.LBB89_475
# BB#474:
	movl	%eax, programChecker+4(%rip)
.LBB89_475:                             # %_ZN5ArrayIlLi0EED2Ev.exit424
	callq	free
.LBB89_476:
	shll	$3, %r14d
	movl	programChecker(%rip), %eax
	subl	%r14d, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	movq	40(%rsp), %rdi          # 8-byte Reload
	jle	.LBB89_478
# BB#477:
	movl	%eax, programChecker+4(%rip)
.LBB89_478:                             # %_ZN5ArrayIPcLi0EED2Ev.exit426
	callq	free
.LBB89_479:
	movq	%rbp, %rdi
	cmpl	$1, %r13d
	jne	.LBB89_486
# BB#480:
	callq	__cxa_begin_catch
	testq	%rax, %rax
	je	.LBB89_482
# BB#481:
	movq	%rax, %rdi
	callq	puts
.LBB89_482:
	callq	__cxa_end_catch
	cmpb	$0, 132(%rsp)           # 1-byte Folded Reload
	je	.LBB89_485
.LBB89_484:
	movl	$.Lstr.4, %edi
	callq	puts
	movq	stdin(%rip), %rdi
	callq	_IO_getc
.LBB89_485:
	xorl	%eax, %eax
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB89_486:
	callq	_Unwind_Resume
.Lfunc_end89:
	.size	_Z7paqmainiPPc, .Lfunc_end89-_Z7paqmainiPPc
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table89:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\210\005"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\375\004"              # Call site table length
	.long	.Ltmp290-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin23 #     jumps to .Ltmp292
	.byte	3                       #   On action: 2
	.long	.Ltmp291-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp295-.Ltmp291       #   Call between .Ltmp291 and .Ltmp295
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp295-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp296-.Ltmp295       #   Call between .Ltmp295 and .Ltmp296
	.long	.Ltmp297-.Lfunc_begin23 #     jumps to .Ltmp297
	.byte	3                       #   On action: 2
	.long	.Ltmp298-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp341-.Lfunc_begin23 #     jumps to .Ltmp341
	.byte	3                       #   On action: 2
	.long	.Ltmp303-.Lfunc_begin23 # >> Call Site 5 <<
	.long	.Ltmp304-.Ltmp303       #   Call between .Ltmp303 and .Ltmp304
	.long	.Ltmp305-.Lfunc_begin23 #     jumps to .Ltmp305
	.byte	3                       #   On action: 2
	.long	.Ltmp319-.Lfunc_begin23 # >> Call Site 6 <<
	.long	.Ltmp320-.Ltmp319       #   Call between .Ltmp319 and .Ltmp320
	.long	.Ltmp341-.Lfunc_begin23 #     jumps to .Ltmp341
	.byte	3                       #   On action: 2
	.long	.Ltmp327-.Lfunc_begin23 # >> Call Site 7 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin23 #     jumps to .Ltmp329
	.byte	3                       #   On action: 2
	.long	.Ltmp321-.Lfunc_begin23 # >> Call Site 8 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin23 #     jumps to .Ltmp323
	.byte	3                       #   On action: 2
	.long	.Ltmp324-.Lfunc_begin23 # >> Call Site 9 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin23 #     jumps to .Ltmp326
	.byte	3                       #   On action: 2
	.long	.Ltmp325-.Lfunc_begin23 # >> Call Site 10 <<
	.long	.Ltmp330-.Ltmp325       #   Call between .Ltmp325 and .Ltmp330
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin23 # >> Call Site 11 <<
	.long	.Ltmp331-.Ltmp330       #   Call between .Ltmp330 and .Ltmp331
	.long	.Ltmp332-.Lfunc_begin23 #     jumps to .Ltmp332
	.byte	3                       #   On action: 2
	.long	.Ltmp333-.Lfunc_begin23 # >> Call Site 12 <<
	.long	.Ltmp334-.Ltmp333       #   Call between .Ltmp333 and .Ltmp334
	.long	.Ltmp335-.Lfunc_begin23 #     jumps to .Ltmp335
	.byte	3                       #   On action: 2
	.long	.Ltmp336-.Lfunc_begin23 # >> Call Site 13 <<
	.long	.Ltmp337-.Ltmp336       #   Call between .Ltmp336 and .Ltmp337
	.long	.Ltmp338-.Lfunc_begin23 #     jumps to .Ltmp338
	.byte	3                       #   On action: 2
	.long	.Ltmp339-.Lfunc_begin23 # >> Call Site 14 <<
	.long	.Ltmp340-.Ltmp339       #   Call between .Ltmp339 and .Ltmp340
	.long	.Ltmp341-.Lfunc_begin23 #     jumps to .Ltmp341
	.byte	3                       #   On action: 2
	.long	.Ltmp342-.Lfunc_begin23 # >> Call Site 15 <<
	.long	.Ltmp343-.Ltmp342       #   Call between .Ltmp342 and .Ltmp343
	.long	.Ltmp344-.Lfunc_begin23 #     jumps to .Ltmp344
	.byte	3                       #   On action: 2
	.long	.Ltmp343-.Lfunc_begin23 # >> Call Site 16 <<
	.long	.Ltmp366-.Ltmp343       #   Call between .Ltmp343 and .Ltmp366
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp366-.Lfunc_begin23 # >> Call Site 17 <<
	.long	.Ltmp367-.Ltmp366       #   Call between .Ltmp366 and .Ltmp367
	.long	.Ltmp368-.Lfunc_begin23 #     jumps to .Ltmp368
	.byte	3                       #   On action: 2
	.long	.Ltmp367-.Lfunc_begin23 # >> Call Site 18 <<
	.long	.Ltmp355-.Ltmp367       #   Call between .Ltmp367 and .Ltmp355
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp355-.Lfunc_begin23 # >> Call Site 19 <<
	.long	.Ltmp356-.Ltmp355       #   Call between .Ltmp355 and .Ltmp356
	.long	.Ltmp357-.Lfunc_begin23 #     jumps to .Ltmp357
	.byte	3                       #   On action: 2
	.long	.Ltmp356-.Lfunc_begin23 # >> Call Site 20 <<
	.long	.Ltmp360-.Ltmp356       #   Call between .Ltmp356 and .Ltmp360
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp360-.Lfunc_begin23 # >> Call Site 21 <<
	.long	.Ltmp361-.Ltmp360       #   Call between .Ltmp360 and .Ltmp361
	.long	.Ltmp362-.Lfunc_begin23 #     jumps to .Ltmp362
	.byte	3                       #   On action: 2
	.long	.Ltmp358-.Lfunc_begin23 # >> Call Site 22 <<
	.long	.Ltmp359-.Ltmp358       #   Call between .Ltmp358 and .Ltmp359
	.long	.Ltmp365-.Lfunc_begin23 #     jumps to .Ltmp365
	.byte	3                       #   On action: 2
	.long	.Ltmp359-.Lfunc_begin23 # >> Call Site 23 <<
	.long	.Ltmp308-.Ltmp359       #   Call between .Ltmp359 and .Ltmp308
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp308-.Lfunc_begin23 # >> Call Site 24 <<
	.long	.Ltmp309-.Ltmp308       #   Call between .Ltmp308 and .Ltmp309
	.long	.Ltmp310-.Lfunc_begin23 #     jumps to .Ltmp310
	.byte	3                       #   On action: 2
	.long	.Ltmp293-.Lfunc_begin23 # >> Call Site 25 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp384-.Lfunc_begin23 #     jumps to .Ltmp384
	.byte	3                       #   On action: 2
	.long	.Ltmp294-.Lfunc_begin23 # >> Call Site 26 <<
	.long	.Ltmp287-.Ltmp294       #   Call between .Ltmp294 and .Ltmp287
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp287-.Lfunc_begin23 # >> Call Site 27 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin23 #     jumps to .Ltmp289
	.byte	3                       #   On action: 2
	.long	.Ltmp388-.Lfunc_begin23 # >> Call Site 28 <<
	.long	.Ltmp389-.Ltmp388       #   Call between .Ltmp388 and .Ltmp389
	.long	.Ltmp390-.Lfunc_begin23 #     jumps to .Ltmp390
	.byte	3                       #   On action: 2
	.long	.Ltmp385-.Lfunc_begin23 # >> Call Site 29 <<
	.long	.Ltmp386-.Ltmp385       #   Call between .Ltmp385 and .Ltmp386
	.long	.Ltmp387-.Lfunc_begin23 #     jumps to .Ltmp387
	.byte	3                       #   On action: 2
	.long	.Ltmp386-.Lfunc_begin23 # >> Call Site 30 <<
	.long	.Ltmp317-.Ltmp386       #   Call between .Ltmp386 and .Ltmp317
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp317-.Lfunc_begin23 # >> Call Site 31 <<
	.long	.Ltmp318-.Ltmp317       #   Call between .Ltmp317 and .Ltmp318
	.long	.Ltmp341-.Lfunc_begin23 #     jumps to .Ltmp341
	.byte	3                       #   On action: 2
	.long	.Ltmp318-.Lfunc_begin23 # >> Call Site 32 <<
	.long	.Ltmp300-.Ltmp318       #   Call between .Ltmp318 and .Ltmp300
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp300-.Lfunc_begin23 # >> Call Site 33 <<
	.long	.Ltmp301-.Ltmp300       #   Call between .Ltmp300 and .Ltmp301
	.long	.Ltmp302-.Lfunc_begin23 #     jumps to .Ltmp302
	.byte	3                       #   On action: 2
	.long	.Ltmp301-.Lfunc_begin23 # >> Call Site 34 <<
	.long	.Ltmp375-.Ltmp301       #   Call between .Ltmp301 and .Ltmp375
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp375-.Lfunc_begin23 # >> Call Site 35 <<
	.long	.Ltmp376-.Ltmp375       #   Call between .Ltmp375 and .Ltmp376
	.long	.Ltmp377-.Lfunc_begin23 #     jumps to .Ltmp377
	.byte	3                       #   On action: 2
	.long	.Ltmp363-.Lfunc_begin23 # >> Call Site 36 <<
	.long	.Ltmp364-.Ltmp363       #   Call between .Ltmp363 and .Ltmp364
	.long	.Ltmp365-.Lfunc_begin23 #     jumps to .Ltmp365
	.byte	3                       #   On action: 2
	.long	.Ltmp364-.Lfunc_begin23 # >> Call Site 37 <<
	.long	.Ltmp373-.Ltmp364       #   Call between .Ltmp364 and .Ltmp373
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp373-.Lfunc_begin23 # >> Call Site 38 <<
	.long	.Ltmp374-.Ltmp373       #   Call between .Ltmp373 and .Ltmp374
	.long	.Ltmp377-.Lfunc_begin23 #     jumps to .Ltmp377
	.byte	3                       #   On action: 2
	.long	.Ltmp314-.Lfunc_begin23 # >> Call Site 39 <<
	.long	.Ltmp315-.Ltmp314       #   Call between .Ltmp314 and .Ltmp315
	.long	.Ltmp316-.Lfunc_begin23 #     jumps to .Ltmp316
	.byte	3                       #   On action: 2
	.long	.Ltmp311-.Lfunc_begin23 # >> Call Site 40 <<
	.long	.Ltmp312-.Ltmp311       #   Call between .Ltmp311 and .Ltmp312
	.long	.Ltmp313-.Lfunc_begin23 #     jumps to .Ltmp313
	.byte	3                       #   On action: 2
	.long	.Ltmp382-.Lfunc_begin23 # >> Call Site 41 <<
	.long	.Ltmp379-.Ltmp382       #   Call between .Ltmp382 and .Ltmp379
	.long	.Ltmp384-.Lfunc_begin23 #     jumps to .Ltmp384
	.byte	3                       #   On action: 2
	.long	.Ltmp371-.Lfunc_begin23 # >> Call Site 42 <<
	.long	.Ltmp370-.Ltmp371       #   Call between .Ltmp371 and .Ltmp370
	.long	.Ltmp377-.Lfunc_begin23 #     jumps to .Ltmp377
	.byte	3                       #   On action: 2
	.long	.Ltmp306-.Lfunc_begin23 # >> Call Site 43 <<
	.long	.Ltmp307-.Ltmp306       #   Call between .Ltmp306 and .Ltmp307
	.long	.Ltmp316-.Lfunc_begin23 #     jumps to .Ltmp316
	.byte	3                       #   On action: 2
	.long	.Ltmp348-.Lfunc_begin23 # >> Call Site 44 <<
	.long	.Ltmp349-.Ltmp348       #   Call between .Ltmp348 and .Ltmp349
	.long	.Ltmp354-.Lfunc_begin23 #     jumps to .Ltmp354
	.byte	3                       #   On action: 2
	.long	.Ltmp349-.Lfunc_begin23 # >> Call Site 45 <<
	.long	.Ltmp285-.Ltmp349       #   Call between .Ltmp349 and .Ltmp285
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp285-.Lfunc_begin23 # >> Call Site 46 <<
	.long	.Ltmp286-.Ltmp285       #   Call between .Ltmp285 and .Ltmp286
	.long	.Ltmp289-.Lfunc_begin23 #     jumps to .Ltmp289
	.byte	3                       #   On action: 2
	.long	.Ltmp352-.Lfunc_begin23 # >> Call Site 47 <<
	.long	.Ltmp351-.Ltmp352       #   Call between .Ltmp352 and .Ltmp351
	.long	.Ltmp354-.Lfunc_begin23 #     jumps to .Ltmp354
	.byte	3                       #   On action: 2
	.long	.Ltmp345-.Lfunc_begin23 # >> Call Site 48 <<
	.long	.Ltmp346-.Ltmp345       #   Call between .Ltmp345 and .Ltmp346
	.long	.Ltmp347-.Lfunc_begin23 #     jumps to .Ltmp347
	.byte	3                       #   On action: 2
	.long	.Ltmp346-.Lfunc_begin23 # >> Call Site 49 <<
	.long	.Lfunc_end89-.Ltmp346   #   Call between .Ltmp346 and .Lfunc_end89
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	0                       # >> Action Record 1 <<
                                        #   Cleanup
	.byte	0                       #   No further actions
	.byte	1                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 1
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 1
	.p2align	2

	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%rbp
.Lcfi448:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi449:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi450:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi451:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi452:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi453:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi454:
	.cfi_def_cfa_offset 128
.Lcfi455:
	.cfi_offset %rbx, -56
.Lcfi456:
	.cfi_offset %r12, -48
.Lcfi457:
	.cfi_offset %r13, -40
.Lcfi458:
	.cfi_offset %r14, -32
.Lcfi459:
	.cfi_offset %r15, -24
.Lcfi460:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$1, 36(%rsp)
	callq	fork
	cmpl	$-1, %eax
	je	.LBB90_3
# BB#1:
	testl	%eax, %eax
	je	.LBB90_2
# BB#4:
	leaq	36(%rsp), %rdi
	callq	wait
	movl	36(%rsp), %eax
	testl	%eax, %eax
	jne	.LBB90_75
# BB#5:
	movq	(%rbx), %r15
	movq	%r15, 48(%rsp)
	movl	$.L.str.58, %edi
	callq	strdup
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)
	addq	$8, %rbx
	movl	$1, %eax
	subl	%ebp, %eax
	.p2align	4, 0x90
.LBB90_6:                               # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	movq	(%rbx), %rsi
	je	.LBB90_8
# BB#7:                                 #   in Loop: Header=BB90_6 Depth=1
	addq	$8, %rbx
	incl	%eax
	cmpb	$45, (%rsi)
	je	.LBB90_6
.LBB90_8:                               # %.critedge
	movq	%rsp, %rdi
	callq	_ZN6StringC2EPKc
	movl	(%rsp), %ebp
	testl	%ebp, %ebp
	jle	.LBB90_10
# BB#9:
	decl	%ebp
	movl	%ebp, (%rsp)
.LBB90_10:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i
	cmpl	4(%rsp), %ebp
	jne	.LBB90_24
# BB#11:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %ebx
	cmovgl	%eax, %ebx
	cmpl	%ebx, %ebp
	jge	.LBB90_12
# BB#13:
	movq	8(%rsp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r13
	movl	%ebx, 4(%rsp)
	movl	%ebx, (%rsp)
	movl	%ebx, %edi
	movl	programChecker(%rip), %r14d
	leal	(%r14,%rbx), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r12d
	cmpl	%r12d, %eax
	jle	.LBB90_15
# BB#14:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r12d
.LBB90_15:                              # %_ZN14ProgramChecker5allocEi.exit.i.i
	movl	$1, %esi
	callq	calloc
	movq	%rax, 8(%rsp)
	testq	%rax, %rax
	je	.LBB90_16
# BB#18:
	movq	%rax, 16(%rsp)
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB90_23
# BB#19:
	testq	%r13, %r13
	je	.LBB90_22
# BB#20:
	cmpl	%ebx, %ebp
	cmovlel	%ebp, %ebx
	movslq	%ebx, %rdx
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	memcpy
	movl	%r14d, programChecker(%rip)
	cmpl	%r12d, %r14d
	jle	.LBB90_22
# BB#21:
	movl	%r14d, programChecker+4(%rip)
.LBB90_22:                              # %_ZN14ProgramChecker5allocEi.exit.i
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB90_23
.LBB90_12:
	movl	%ebx, (%rsp)
.LBB90_23:                              # %.noexc
	movl	%ebp, (%rsp)
.LBB90_24:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i
	movq	16(%rsp), %rax
	leal	1(%rbp), %ecx
	movl	%ecx, (%rsp)
	movslq	%ebp, %rcx
	movb	$46, (%rax,%rcx)
	movl	(%rsp), %ebp
	cmpl	4(%rsp), %ebp
	jne	.LBB90_38
# BB#25:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %ebx
	cmovgl	%eax, %ebx
	cmpl	%ebx, %ebp
	jge	.LBB90_26
# BB#27:
	movq	8(%rsp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r13
	movl	%ebx, 4(%rsp)
	movl	%ebx, (%rsp)
	movl	%ebx, %edi
	movl	programChecker(%rip), %r14d
	leal	(%r14,%rbx), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r12d
	cmpl	%r12d, %eax
	jle	.LBB90_29
# BB#28:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r12d
.LBB90_29:                              # %_ZN14ProgramChecker5allocEi.exit.i.i38
	movl	$1, %esi
	callq	calloc
	movq	%rax, 8(%rsp)
	testq	%rax, %rax
	je	.LBB90_30
# BB#32:
	movq	%rax, 16(%rsp)
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB90_37
# BB#33:
	testq	%r13, %r13
	je	.LBB90_36
# BB#34:
	cmpl	%ebx, %ebp
	cmovlel	%ebp, %ebx
	movslq	%ebx, %rdx
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	memcpy
	movl	%r14d, programChecker(%rip)
	cmpl	%r12d, %r14d
	jle	.LBB90_36
# BB#35:
	movl	%r14d, programChecker+4(%rip)
.LBB90_36:                              # %_ZN14ProgramChecker5allocEi.exit.i39
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB90_37
.LBB90_26:
	movl	%ebx, (%rsp)
.LBB90_37:                              # %.noexc18
	movl	%ebp, (%rsp)
.LBB90_38:
	movslq	%ebp, %rax
	movq	16(%rsp), %rcx
	incl	%ebp
	movl	%ebp, (%rsp)
	movb	$0, (%rcx,%rax)
	movl	$47, %esi
	movq	%r15, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rbx
	cmoveq	%r15, %rbx
	movl	(%rsp), %ebp
	testl	%ebp, %ebp
	jle	.LBB90_40
# BB#39:
	decl	%ebp
	movl	%ebp, (%rsp)
.LBB90_40:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit.preheader.i21
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB90_57
# BB#41:                                # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i23.preheader
	incq	%rbx
	.p2align	4, 0x90
.LBB90_42:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3._crit_edge.i23
                                        # =>This Inner Loop Header: Depth=1
	cmpl	4(%rsp), %ebp
	jne	.LBB90_56
# BB#43:                                #   in Loop: Header=BB90_42 Depth=1
	leal	(%rbp,%rbp), %r14d
	testl	%r14d, %r14d
	movl	$1, %eax
	cmovlel	%eax, %r14d
	cmpl	%r14d, %ebp
	jge	.LBB90_44
# BB#45:                                #   in Loop: Header=BB90_42 Depth=1
	movq	8(%rsp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r12
	movl	%r14d, 4(%rsp)
	movl	%r14d, (%rsp)
	movl	%r14d, %edi
	movl	programChecker(%rip), %r13d
	leal	(%r13,%r14), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r15d
	cmpl	%r15d, %eax
	jle	.LBB90_47
# BB#46:                                #   in Loop: Header=BB90_42 Depth=1
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r15d
.LBB90_47:                              # %_ZN14ProgramChecker5allocEi.exit.i.i31
                                        #   in Loop: Header=BB90_42 Depth=1
	movl	$1, %esi
	callq	calloc
	movq	%rax, 8(%rsp)
	testq	%rax, %rax
	je	.LBB90_48
# BB#50:                                #   in Loop: Header=BB90_42 Depth=1
	movq	%rax, 16(%rsp)
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB90_55
# BB#51:                                #   in Loop: Header=BB90_42 Depth=1
	testq	%r12, %r12
	je	.LBB90_54
# BB#52:                                #   in Loop: Header=BB90_42 Depth=1
	cmpl	%r14d, %ebp
	cmovlel	%ebp, %r14d
	movslq	%r14d, %rdx
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	memcpy
	movl	%r13d, programChecker(%rip)
	cmpl	%r15d, %r13d
	jle	.LBB90_54
# BB#53:                                #   in Loop: Header=BB90_42 Depth=1
	movl	%r13d, programChecker+4(%rip)
.LBB90_54:                              # %_ZN14ProgramChecker5allocEi.exit.i32
                                        #   in Loop: Header=BB90_42 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB90_55
	.p2align	4, 0x90
.LBB90_44:                              #   in Loop: Header=BB90_42 Depth=1
	movl	%r14d, (%rsp)
.LBB90_55:                              # %.noexc28
                                        #   in Loop: Header=BB90_42 Depth=1
	movl	%ebp, (%rsp)
	movzbl	-1(%rbx), %eax
.LBB90_56:                              # %_ZN5ArrayIcLi0EE9push_backERKc.exit3.i26
                                        #   in Loop: Header=BB90_42 Depth=1
	movq	16(%rsp), %rcx
	leal	1(%rbp), %edx
	movl	%edx, (%rsp)
	movslq	%ebp, %rdx
	movb	%al, (%rcx,%rdx)
	movzbl	(%rbx), %eax
	movl	(%rsp), %ebp
	incq	%rbx
	testb	%al, %al
	jne	.LBB90_42
.LBB90_57:                              # %_ZN5ArrayIcLi0EE8pop_backEv.exit._crit_edge.i27
	cmpl	4(%rsp), %ebp
	jne	.LBB90_71
# BB#58:
	leal	(%rbp,%rbp), %eax
	testl	%eax, %eax
	movl	$1, %ebx
	cmovgl	%eax, %ebx
	cmpl	%ebx, %ebp
	jge	.LBB90_59
# BB#60:
	movq	8(%rsp), %r12
	movq	16(%rsp), %r15
	movl	%ebx, 4(%rsp)
	movl	%ebx, (%rsp)
	movl	%ebx, %edi
	movl	programChecker(%rip), %r14d
	leal	(%r14,%rbx), %eax
	movl	%eax, programChecker(%rip)
	movl	programChecker+4(%rip), %r13d
	cmpl	%r13d, %eax
	jle	.LBB90_62
# BB#61:
	movl	%eax, programChecker+4(%rip)
	movl	%eax, %r13d
.LBB90_62:                              # %_ZN14ProgramChecker5allocEi.exit.i.i42
	movl	$1, %esi
	callq	calloc
	movq	%rax, 8(%rsp)
	testq	%rax, %rax
	je	.LBB90_63
# BB#65:
	movq	%rax, 16(%rsp)
	testq	%r12, %r12
	je	.LBB90_70
# BB#66:
	testq	%r15, %r15
	je	.LBB90_69
# BB#67:
	cmpl	%ebx, %ebp
	cmovlel	%ebp, %ebx
	movslq	%ebx, %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movl	%r14d, programChecker(%rip)
	cmpl	%r13d, %r14d
	jle	.LBB90_69
# BB#68:
	movl	%r14d, programChecker+4(%rip)
.LBB90_69:                              # %_ZN14ProgramChecker5allocEi.exit.i43
	movq	%r12, %rdi
	callq	free
	jmp	.LBB90_70
.LBB90_59:
	movl	%ebx, (%rsp)
.LBB90_70:                              # %.noexc29
	movl	%ebp, (%rsp)
.LBB90_71:
	movq	16(%rsp), %rax
	leal	1(%rbp), %ecx
	movl	%ecx, (%rsp)
	movslq	%ebp, %rcx
	movb	$0, (%rax,%rcx)
	movq	16(%rsp), %rdi
	callq	strdup
	movq	%rax, %rbx
	movq	%rbx, 64(%rsp)
.Ltmp393:
	leaq	48(%rsp), %rsi
	movl	$3, %edi
	callq	_Z7paqmainiPPc
.Ltmp394:
# BB#72:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	unlink
	movq	%rbx, %rdi
	callq	free
	movl	programChecker(%rip), %eax
	subl	(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB90_74
# BB#73:
	movl	%eax, programChecker+4(%rip)
.LBB90_74:                              # %_ZN5ArrayIcLi0EED2Ev.exit37
	movq	8(%rsp), %rdi
	callq	free
	xorl	%eax, %eax
.LBB90_75:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB90_48:
.Ltmp391:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp392:
# BB#49:                                # %.noexc33
.LBB90_3:
	movl	$.L.str.57, %edi
	callq	perror
	movl	$1, %edi
	callq	exit
.LBB90_2:
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	_Z7paqmainiPPc
	xorl	%edi, %edi
	callq	exit
.LBB90_16:
.Ltmp399:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp400:
# BB#17:                                # %.noexc19
.LBB90_30:
.Ltmp397:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp398:
# BB#31:                                # %.noexc40
.LBB90_63:
.Ltmp395:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Ltmp396:
# BB#64:                                # %.noexc44
.LBB90_76:
.Ltmp401:
	movq	%rax, %rbx
	movl	programChecker(%rip), %eax
	subl	(%rsp), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB90_78
# BB#77:
	movl	%eax, programChecker+4(%rip)
.LBB90_78:                              # %_ZN5ArrayIcLi0EED2Ev.exit
	movq	8(%rsp), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end90:
	.size	main, .Lfunc_end90-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table90:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin24-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp393-.Lfunc_begin24 #   Call between .Lfunc_begin24 and .Ltmp393
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp392-.Ltmp393       #   Call between .Ltmp393 and .Ltmp392
	.long	.Ltmp401-.Lfunc_begin24 #     jumps to .Ltmp401
	.byte	0                       #   On action: cleanup
	.long	.Ltmp392-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp399-.Ltmp392       #   Call between .Ltmp392 and .Ltmp399
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp399-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Ltmp396-.Ltmp399       #   Call between .Ltmp399 and .Ltmp396
	.long	.Ltmp401-.Lfunc_begin24 #     jumps to .Ltmp401
	.byte	0                       #   On action: cleanup
	.long	.Ltmp396-.Lfunc_begin24 # >> Call Site 5 <<
	.long	.Lfunc_end90-.Ltmp396   #   Call between .Ltmp396 and .Lfunc_end90
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN2BHILi4EEixEj,"axG",@progbits,_ZN2BHILi4EEixEj,comdat
	.weak	_ZN2BHILi4EEixEj
	.p2align	4, 0x90
	.type	_ZN2BHILi4EEixEj,@function
_ZN2BHILi4EEixEj:                       # @_ZN2BHILi4EEixEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi461:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi462:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi463:
	.cfi_def_cfa_offset 32
.Lcfi464:
	.cfi_offset %rbx, -32
.Lcfi465:
	.cfi_offset %r14, -24
.Lcfi466:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	%ebp, %eax
	shrl	$16, %eax
	movzwl	%bp, %esi
	xorl	%eax, %esi
	shll	$3, %ebp
	andl	24(%rbx), %ebp
	movq	16(%rbx), %rdi
	shll	$2, %ebp
	movslq	%ebp, %r14
	leaq	(%rdi,%r14), %rax
	cmpb	$0, 2(%rdi,%r14)
	je	.LBB91_5
# BB#1:
	movzwl	(%rax), %ecx
	cmpl	%esi, %ecx
	je	.LBB91_6
# BB#2:
	movabsq	$17179869184, %rdx      # imm = 0x400000000
	movl	%ebp, %ecx
	orl	$4, %ecx
	movslq	%ecx, %r8
	leaq	(%rdi,%r8), %r11
	cmpb	$0, 2(%rdi,%r8)
	je	.LBB91_3
# BB#10:
	movzwl	(%r11), %ecx
	cmpl	%esi, %ecx
	je	.LBB91_4
# BB#11:
	movabsq	$34359738368, %rdx      # imm = 0x800000000
	movl	%ebp, %ecx
	orl	$8, %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r11
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB91_3
# BB#12:
	movzwl	(%r11), %ecx
	cmpl	%esi, %ecx
	je	.LBB91_4
# BB#13:
	movabsq	$51539607552, %rdx      # imm = 0xC00000000
	movl	%ebp, %ecx
	orl	$12, %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r11
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB91_3
# BB#14:
	movzwl	(%r11), %ecx
	cmpl	%esi, %ecx
	je	.LBB91_4
# BB#15:
	movabsq	$68719476736, %rdx      # imm = 0x1000000000
	movl	%ebp, %ecx
	orl	$16, %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r11
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB91_3
# BB#16:
	movzwl	(%r11), %ecx
	cmpl	%esi, %ecx
	je	.LBB91_4
# BB#17:
	movabsq	$85899345920, %rdx      # imm = 0x1400000000
	movl	%ebp, %ecx
	orl	$20, %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r11
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB91_3
# BB#18:
	movzwl	(%r11), %ecx
	cmpl	%esi, %ecx
	je	.LBB91_4
# BB#19:
	movabsq	$103079215104, %rdx     # imm = 0x1800000000
	movl	%ebp, %ecx
	orl	$24, %ecx
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r11
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB91_3
# BB#20:
	movzwl	(%r11), %ecx
	cmpl	%esi, %ecx
	je	.LBB91_4
# BB#21:
	movabsq	$120259084288, %r9      # imm = 0x1C00000000
	movl	%ebp, %r10d
	orl	$28, %r10d
	movslq	%r10d, %rcx
	leaq	(%rdi,%rcx), %r11
	cmpb	$0, 2(%rdi,%rcx)
	je	.LBB91_22
# BB#23:
	movzwl	(%r11), %ecx
	cmpl	%esi, %ecx
	jne	.LBB91_7
# BB#24:
	movq	%r9, %rdx
	jmp	.LBB91_4
.LBB91_5:                               # %.loopexit
	movw	%si, (%rax)
.LBB91_6:                               # %.loopexit.thread51
	incq	%rax
	jmp	.LBB91_9
.LBB91_22:
	movq	%r9, %rdx
.LBB91_3:                               # %.loopexit.thread54
	movw	%si, (%r11)
.LBB91_4:                               # %.loopexit.thread
	movl	(%r11), %ecx
	movl	%ecx, _ZZN2BHILi4EEixEjE3tmp(%rip)
.LBB91_8:
	addq	%r8, %rdi
	sarq	$32, %rdx
	movq	%rax, %rsi
	callq	memmove
	movq	16(%rbx), %rax
	movl	_ZZN2BHILi4EEixEjE3tmp(%rip), %ecx
	movl	%ecx, (%rax,%r14)
	orl	$1, %ebp
	movslq	%ebp, %rax
	addq	16(%rbx), %rax
.LBB91_9:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB91_7:
	movl	$0, _ZZN2BHILi4EEixEjE3tmp(%rip)
	movw	%si, _ZZN2BHILi4EEixEjE3tmp(%rip)
	movl	%ebp, %ecx
	orl	$30, %ecx
	movslq	%ecx, %rcx
	movb	(%rdi,%rcx), %cl
	addl	$-2, %r10d
	movslq	%r10d, %rsi
	cmpb	(%rdi,%rsi), %cl
	cmovbeq	%r9, %rdx
	jmp	.LBB91_8
.Lfunc_end91:
	.size	_ZN2BHILi4EEixEj, .Lfunc_end91-_ZN2BHILi4EEixEj
	.cfi_endproc

	.section	.text._ZN7Encoder4codeEi,"axG",@progbits,_ZN7Encoder4codeEi,comdat
	.weak	_ZN7Encoder4codeEi
	.p2align	4, 0x90
	.type	_ZN7Encoder4codeEi,@function
_ZN7Encoder4codeEi:                     # @_ZN7Encoder4codeEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi467:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi468:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi469:
	.cfi_def_cfa_offset 32
.Lcfi470:
	.cfi_offset %rbx, -24
.Lcfi471:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	xorl	%ecx, %ecx
	cmpl	$2048, %eax             # imm = 0x800
	setl	%cl
	addl	%eax, %ecx
	movl	16(%rbx), %edx
	movl	20(%rbx), %eax
	subl	%edx, %eax
	movl	%eax, %edi
	shrl	$12, %edi
	imull	%ecx, %edi
	addl	%edx, %edi
	andl	$4095, %eax             # imm = 0xFFF
	imull	%ecx, %eax
	shrl	$12, %eax
	addl	%edi, %eax
	cmpl	$1, 4(%rbx)
	jne	.LBB92_2
# BB#1:
	xorl	%esi, %esi
	cmpl	%eax, 24(%rbx)
	setbe	%sil
.LBB92_2:
	movl	%esi, y(%rip)
	testl	%esi, %esi
	je	.LBB92_4
# BB#3:
	movl	%eax, 20(%rbx)
	jmp	.LBB92_5
.LBB92_4:
	incl	%eax
	movl	%eax, 16(%rbx)
.LBB92_5:
	movq	%rbx, %rdi
	callq	_ZN9Predictor6updateEv
	movl	16(%rbx), %eax
	movl	20(%rbx), %edi
	movl	%edi, %ecx
	xorl	%eax, %ecx
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB92_11
	.p2align	4, 0x90
.LBB92_6:                               # =>This Inner Loop Header: Depth=1
	movl	4(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.LBB92_8
# BB#7:                                 #   in Loop: Header=BB92_6 Depth=1
	shrl	$24, %edi
	movq	8(%rbx), %rsi
	callq	_IO_putc
	movl	4(%rbx), %ecx
	movl	16(%rbx), %eax
	movl	20(%rbx), %edi
.LBB92_8:                               #   in Loop: Header=BB92_6 Depth=1
	shll	$8, %eax
	movl	%eax, 16(%rbx)
	shll	$8, %edi
	orl	$255, %edi
	movl	%edi, 20(%rbx)
	cmpl	$1, %ecx
	jne	.LBB92_10
# BB#9:                                 #   in Loop: Header=BB92_6 Depth=1
	movl	24(%rbx), %ebp
	shll	$8, %ebp
	movq	8(%rbx), %rdi
	callq	_IO_getc
	movzbl	%al, %eax
	orl	%ebp, %eax
	movl	%eax, 24(%rbx)
	movl	16(%rbx), %eax
	movl	20(%rbx), %edi
.LBB92_10:                              # %.backedge
                                        #   in Loop: Header=BB92_6 Depth=1
	movl	%edi, %ecx
	xorl	%eax, %ecx
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jb	.LBB92_6
.LBB92_11:                              # %._crit_edge
	movl	y(%rip), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end92:
	.size	_ZN7Encoder4codeEi, .Lfunc_end92-_ZN7Encoder4codeEi
	.cfi_endproc

	.section	.text._ZN5ArrayIhLi0EE6resizeEi,"axG",@progbits,_ZN5ArrayIhLi0EE6resizeEi,comdat
	.weak	_ZN5ArrayIhLi0EE6resizeEi
	.p2align	4, 0x90
	.type	_ZN5ArrayIhLi0EE6resizeEi,@function
_ZN5ArrayIhLi0EE6resizeEi:              # @_ZN5ArrayIhLi0EE6resizeEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi472:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi473:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi474:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi475:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi476:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi477:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi478:
	.cfi_def_cfa_offset 64
.Lcfi479:
	.cfi_offset %rbx, -56
.Lcfi480:
	.cfi_offset %r12, -48
.Lcfi481:
	.cfi_offset %r13, -40
.Lcfi482:
	.cfi_offset %r14, -32
.Lcfi483:
	.cfi_offset %r15, -24
.Lcfi484:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	%ebp, 4(%rbx)
	jge	.LBB93_1
# BB#3:
	leaq	8(%rbx), %r13
	movq	8(%rbx), %r14
	movq	16(%rbx), %r15
	movl	(%rbx), %r12d
	movl	%ebp, 4(%rbx)
	movl	%ebp, (%rbx)
	testl	%ebp, %ebp
	jle	.LBB93_4
# BB#5:
	movslq	%ebp, %rdi
	movl	programChecker(%rip), %eax
	addl	%ebp, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB93_7
# BB#6:
	movl	%eax, programChecker+4(%rip)
.LBB93_7:                               # %_ZN14ProgramChecker5allocEi.exit.i
	movl	$1, %esi
	callq	calloc
	movq	%rax, (%r13)
	testq	%rax, %rax
	je	.LBB93_14
# BB#8:
	movq	%rax, 16(%rbx)
	testq	%r14, %r14
	jne	.LBB93_10
	jmp	.LBB93_2
.LBB93_1:
	movl	%ebp, (%rbx)
	jmp	.LBB93_2
.LBB93_4:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.LBB93_2
.LBB93_10:
	testq	%r15, %r15
	je	.LBB93_13
# BB#11:
	cmpl	%ebp, %r12d
	cmovgl	%ebp, %r12d
	movslq	%r12d, %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	memcpy
	movl	programChecker(%rip), %eax
	subl	%ebp, %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB93_13
# BB#12:
	movl	%eax, programChecker+4(%rip)
.LBB93_13:                              # %_ZN14ProgramChecker5allocEi.exit
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB93_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB93_14:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Lfunc_end93:
	.size	_ZN5ArrayIhLi0EE6resizeEi, .Lfunc_end93-_ZN5ArrayIhLi0EE6resizeEi
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_paq8p.ii,@function
_GLOBAL__sub_I_paq8p.ii:                # @_GLOBAL__sub_I_paq8p.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi485:
	.cfi_def_cfa_offset 16
	movq	$0, programChecker(%rip)
	callq	clock
	movq	%rax, programChecker+8(%rip)
	movabsq	$274877907008, %rax     # imm = 0x4000000040
	movq	%rax, rnd(%rip)
	movl	$256, %eax              # imm = 0x100
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB94_2
# BB#1:
	movl	%eax, programChecker+4(%rip)
.LBB94_2:                               # %_ZN14ProgramChecker5allocEi.exit.i.i.i.i
	movl	$256, %edi              # imm = 0x100
	movl	$1, %esi
	callq	calloc
	movq	%rax, rnd+8(%rip)
	testq	%rax, %rax
	je	.LBB94_12
# BB#3:                                 # %_ZN5ArrayIjLi0EEC2Ei.exit.i.i
	movq	%rax, rnd+16(%rip)
	movabsq	$4241943008571542805, %rcx # imm = 0x3ADE68B1075BCD15
	movq	%rcx, (%rax)
	movl	$-1843235223, 8(%rax)   # imm = 0x92227669
	movl	$987654321, %ecx        # imm = 0x3ADE68B1
	movl	$-1843235223, %edx      # imm = 0x92227669
	movl	$4, %esi
	jmp	.LBB94_4
	.p2align	4, 0x90
.LBB94_11:                              # %._crit_edge.i.i.1
                                        #   in Loop: Header=BB94_4 Depth=1
	imull	$11, %ecx, %edi
	imull	$23, %edx, %edx
	shrl	$4, %edx
	addl	%edi, %edx
	movl	%edx, (%rax,%rsi,4)
	addq	$2, %rsi
.LBB94_4:                               # %._crit_edge.i.i
                                        # =>This Inner Loop Header: Depth=1
	imull	$11, %edx, %edi
	imull	$23, %ecx, %ecx
	shrl	$4, %ecx
	addl	%edi, %ecx
	movl	%ecx, -4(%rax,%rsi,4)
	cmpq	$64, %rsi
	jne	.LBB94_11
# BB#5:                                 # %__cxx_global_var_init.1.exit
	movl	$0, rnd+24(%rip)
	movl	$_ZN6RandomD2Ev, %edi
	movl	$rnd, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	xorps	%xmm0, %xmm0
	movups	%xmm0, buf(%rip)
	movq	$0, buf+16(%rip)
	movl	$_ZN3BufD2Ev, %edi
	movl	$buf, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movabsq	$281474976776192, %rax  # imm = 0x1000000010000
	movq	%rax, ilog(%rip)
	movl	$65536, %eax            # imm = 0x10000
	addl	programChecker(%rip), %eax
	movl	%eax, programChecker(%rip)
	cmpl	programChecker+4(%rip), %eax
	jle	.LBB94_7
# BB#6:
	movl	%eax, programChecker+4(%rip)
.LBB94_7:                               # %_ZN14ProgramChecker5allocEi.exit.i.i.i.i1
	movl	$65536, %edi            # imm = 0x10000
	movl	$1, %esi
	callq	calloc
	movq	%rax, ilog+8(%rip)
	testq	%rax, %rax
	je	.LBB94_12
# BB#8:                                 # %_ZN5ArrayIhLi0EEC2Ei.exit.i.i
	movq	%rax, ilog+16(%rip)
	movw	$6416, 2(%rax)          # imm = 0x1910
	movb	$32, 4(%rax)
	movl	$537893024, %ecx        # imm = 0x200F98A0
	movl	$11, %r8d
	xorl	%edi, %edi
	jmp	.LBB94_9
	.p2align	4, 0x90
.LBB94_10:                              # %._crit_edge.i.._crit_edge.i_crit_edge.i.._crit_edge.i.._crit_edge.i_crit_edge.i_crit_edge.1
                                        #   in Loop: Header=BB94_9 Depth=1
	movq	ilog+16(%rip), %r9
	movl	$774541002, %eax        # imm = 0x2E2A8ECA
	xorl	%edx, %edx
	idivl	%r8d
	movl	%eax, %ecx
	addl	%esi, %ecx
	movl	%ecx, %eax
	shrl	$24, %eax
	movb	%al, 6(%r9,%rdi)
	addq	$2, %rdi
	addl	$4, %r8d
.LBB94_9:                               # %._crit_edge.i.._crit_edge.i_crit_edge.i.._crit_edge.i.._crit_edge.i_crit_edge.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	ilog+16(%rip), %r9
	leal	-2(%r8), %esi
	movl	$774541002, %eax        # imm = 0x2E2A8ECA
	xorl	%edx, %edx
	idivl	%esi
	movl	%eax, %esi
	addl	%ecx, %esi
	movl	%esi, %eax
	shrl	$24, %eax
	movb	%al, 5(%r9,%rdi)
	cmpq	$65530, %rdi            # imm = 0xFFFA
	jne	.LBB94_10
# BB#13:                                # %__cxx_global_var_init.3.exit
	movl	$_ZN4IlogD2Ev, %edi
	movl	$ilog, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$stretch, %edi
	callq	_ZN7StretchC2Ev
	movl	$_ZN7StretchD2Ev, %edi
	movl	$stretch, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.LBB94_12:
	movl	$.L.str.59, %edi
	callq	_Z4quitPKc
.Lfunc_end94:
	.size	_GLOBAL__sub_I_paq8p.ii, .Lfunc_end94-_GLOBAL__sub_I_paq8p.ii
	.cfi_endproc

	.type	programChecker,@object  # @programChecker
	.bss
	.globl	programChecker
	.p2align	3
programChecker:
	.zero	16
	.size	programChecker, 16

	.type	rnd,@object             # @rnd
	.globl	rnd
	.p2align	3
rnd:
	.zero	32
	.size	rnd, 32

	.type	pos,@object             # @pos
	.globl	pos
	.p2align	2
pos:
	.long	0                       # 0x0
	.size	pos, 4

	.type	level,@object           # @level
	.data
	.globl	level
	.p2align	2
level:
	.long	5                       # 0x5
	.size	level, 4

	.type	y,@object               # @y
	.bss
	.globl	y
	.p2align	2
y:
	.long	0                       # 0x0
	.size	y, 4

	.type	c0,@object              # @c0
	.data
	.globl	c0
	.p2align	2
c0:
	.long	1                       # 0x1
	.size	c0, 4

	.type	c4,@object              # @c4
	.bss
	.globl	c4
	.p2align	2
c4:
	.long	0                       # 0x0
	.size	c4, 4

	.type	bpos,@object            # @bpos
	.globl	bpos
	.p2align	2
bpos:
	.long	0                       # 0x0
	.size	bpos, 4

	.type	buf,@object             # @buf
	.globl	buf
	.p2align	3
buf:
	.zero	24
	.size	buf, 24

	.type	ilog,@object            # @ilog
	.globl	ilog
	.p2align	3
ilog:
	.zero	24
	.size	ilog, 24

	.type	_ZZ6squashiE1t,@object  # @_ZZ6squashiE1t
	.section	.rodata,"a",@progbits
	.p2align	4
_ZZ6squashiE1t:
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	10                      # 0xa
	.long	16                      # 0x10
	.long	27                      # 0x1b
	.long	45                      # 0x2d
	.long	73                      # 0x49
	.long	120                     # 0x78
	.long	194                     # 0xc2
	.long	310                     # 0x136
	.long	488                     # 0x1e8
	.long	747                     # 0x2eb
	.long	1101                    # 0x44d
	.long	1546                    # 0x60a
	.long	2047                    # 0x7ff
	.long	2549                    # 0x9f5
	.long	2994                    # 0xbb2
	.long	3348                    # 0xd14
	.long	3607                    # 0xe17
	.long	3785                    # 0xec9
	.long	3901                    # 0xf3d
	.long	3975                    # 0xf87
	.long	4022                    # 0xfb6
	.long	4050                    # 0xfd2
	.long	4068                    # 0xfe4
	.long	4079                    # 0xfef
	.long	4085                    # 0xff5
	.long	4089                    # 0xff9
	.long	4092                    # 0xffc
	.long	4093                    # 0xffd
	.long	4094                    # 0xffe
	.size	_ZZ6squashiE1t, 132

	.type	stretch,@object         # @stretch
	.bss
	.globl	stretch
	.p2align	3
stretch:
	.zero	24
	.size	stretch, 24

	.type	_ZL11State_table,@object # @_ZL11State_table
	.section	.rodata,"a",@progbits
	.p2align	4
_ZL11State_table:
	.asciz	"\001\002\000"
	.asciz	"\003\005\001"
	.ascii	"\004\006\000\001"
	.asciz	"\007\n\002"
	.ascii	"\b\f\001\001"
	.ascii	"\t\r\001\001"
	.ascii	"\013\016\000\002"
	.asciz	"\017\023\003"
	.ascii	"\020\027\002\001"
	.ascii	"\021\030\002\001"
	.ascii	"\022\031\002\001"
	.ascii	"\024\033\001\002"
	.ascii	"\025\034\001\002"
	.ascii	"\026\035\001\002"
	.ascii	"\032\036\000\003"
	.asciz	"\037!\004"
	.ascii	" #\003\001"
	.ascii	" #\003\001"
	.ascii	" #\003\001"
	.ascii	" #\003\001"
	.ascii	"\"%\002\002"
	.ascii	"\"%\002\002"
	.ascii	"\"%\002\002"
	.ascii	"\"%\002\002"
	.ascii	"\"%\002\002"
	.ascii	"\"%\002\002"
	.ascii	"$'\001\003"
	.ascii	"$'\001\003"
	.ascii	"$'\001\003"
	.ascii	"$'\001\003"
	.ascii	"&(\000\004"
	.asciz	")+\005"
	.ascii	"*-\004\001"
	.ascii	"*-\004\001"
	.ascii	",/\003\002"
	.ascii	",/\003\002"
	.ascii	".1\002\003"
	.ascii	".1\002\003"
	.ascii	"03\001\004"
	.ascii	"03\001\004"
	.ascii	"24\000\005"
	.asciz	"5+\006"
	.ascii	"69\005\001"
	.ascii	"69\005\001"
	.ascii	"8;\004\002"
	.ascii	"8;\004\002"
	.ascii	":=\003\003"
	.ascii	":=\003\003"
	.ascii	"<?\002\004"
	.ascii	"<?\002\004"
	.ascii	">A\001\005"
	.ascii	">A\001\005"
	.ascii	"2B\000\006"
	.asciz	"C7\007"
	.ascii	"D9\006\001"
	.ascii	"D9\006\001"
	.ascii	"FI\005\002"
	.ascii	"FI\005\002"
	.ascii	"HK\004\003"
	.ascii	"HK\004\003"
	.ascii	"JM\003\004"
	.ascii	"JM\003\004"
	.ascii	"LO\002\005"
	.ascii	"LO\002\005"
	.ascii	">Q\001\006"
	.ascii	">Q\001\006"
	.ascii	"@R\000\007"
	.asciz	"SE\b"
	.ascii	"TG\007\001"
	.ascii	"TG\007\001"
	.ascii	"VI\006\002"
	.ascii	"VI\006\002"
	.ascii	",;\005\003"
	.ascii	",;\005\003"
	.ascii	":=\004\004"
	.ascii	":=\004\004"
	.ascii	"<1\003\005"
	.ascii	"<1\003\005"
	.ascii	"LY\002\006"
	.ascii	"LY\002\006"
	.ascii	"N[\001\007"
	.ascii	"N[\001\007"
	.ascii	"P\\\000\b"
	.asciz	"]E\t"
	.ascii	"^W\b\001"
	.ascii	"^W\b\001"
	.ascii	"`-\007\002"
	.ascii	"`-\007\002"
	.ascii	"0c\002\007"
	.ascii	"0c\002\007"
	.ascii	"Xe\001\b"
	.ascii	"Xe\001\b"
	.ascii	"Pf\000\t"
	.asciz	"gE\n"
	.ascii	"hW\t\001"
	.ascii	"hW\t\001"
	.ascii	"j9\b\002"
	.ascii	"j9\b\002"
	.ascii	">m\002\b"
	.ascii	">m\002\b"
	.ascii	"Xo\001\t"
	.ascii	"Xo\001\t"
	.ascii	"Pp\000\n"
	.asciz	"qU\013"
	.ascii	"rW\n\001"
	.ascii	"rW\n\001"
	.ascii	"t9\t\002"
	.ascii	"t9\t\002"
	.ascii	">w\002\t"
	.ascii	">w\002\t"
	.ascii	"Xy\001\n"
	.ascii	"Xy\001\n"
	.ascii	"Zz\000\013"
	.asciz	"{U\f"
	.ascii	"|a\013\001"
	.ascii	"|a\013\001"
	.ascii	"~9\n\002"
	.ascii	"~9\n\002"
	.ascii	">\201\002\n"
	.ascii	">\201\002\n"
	.ascii	"b\203\001\013"
	.ascii	"b\203\001\013"
	.ascii	"Z\204\000\f"
	.asciz	"\205U\r"
	.ascii	"\206a\f\001"
	.ascii	"\206a\f\001"
	.ascii	"\2109\013\002"
	.ascii	"\2109\013\002"
	.ascii	">\213\002\013"
	.ascii	">\213\002\013"
	.ascii	"b\215\001\f"
	.ascii	"b\215\001\f"
	.ascii	"Z\216\000\r"
	.asciz	"\217_\016"
	.ascii	"\220a\r\001"
	.ascii	"\220a\r\001"
	.ascii	"D9\f\002"
	.ascii	"D9\f\002"
	.ascii	">Q\002\f"
	.ascii	">Q\002\f"
	.ascii	"b\223\001\r"
	.ascii	"b\223\001\r"
	.ascii	"d\224\000\016"
	.asciz	"\225_\017"
	.ascii	"\226k\016\001"
	.ascii	"\226k\016\001"
	.ascii	"l\227\001\016"
	.ascii	"l\227\001\016"
	.ascii	"d\230\000\017"
	.asciz	"\231_\020"
	.ascii	"\232k\017\001"
	.ascii	"l\233\001\017"
	.ascii	"d\234\000\020"
	.asciz	"\235_\021"
	.ascii	"\236k\020\001"
	.ascii	"l\237\001\020"
	.ascii	"d\240\000\021"
	.asciz	"\241i\022"
	.ascii	"\242k\021\001"
	.ascii	"l\243\001\021"
	.ascii	"n\244\000\022"
	.asciz	"\245i\023"
	.ascii	"\246u\022\001"
	.ascii	"v\247\001\022"
	.ascii	"n\250\000\023"
	.asciz	"\251i\024"
	.ascii	"\252u\023\001"
	.ascii	"v\253\001\023"
	.ascii	"n\254\000\024"
	.asciz	"\255i\025"
	.ascii	"\256u\024\001"
	.ascii	"v\257\001\024"
	.ascii	"n\260\000\025"
	.asciz	"\261i\026"
	.ascii	"\262u\025\001"
	.ascii	"v\263\001\025"
	.ascii	"n\264\000\026"
	.asciz	"\265s\027"
	.ascii	"\266u\026\001"
	.ascii	"v\267\001\026"
	.ascii	"x\270\000\027"
	.asciz	"\271s\030"
	.ascii	"\272\177\027\001"
	.ascii	"\200\273\001\027"
	.ascii	"x\274\000\030"
	.asciz	"\275s\031"
	.ascii	"\276\177\030\001"
	.ascii	"\200\277\001\030"
	.ascii	"x\300\000\031"
	.asciz	"\301s\032"
	.ascii	"\302\177\031\001"
	.ascii	"\200\303\001\031"
	.ascii	"x\304\000\032"
	.asciz	"\305s\033"
	.ascii	"\306\177\032\001"
	.ascii	"\200\307\001\032"
	.ascii	"x\310\000\033"
	.asciz	"\311s\034"
	.ascii	"\312\177\033\001"
	.ascii	"\200\313\001\033"
	.ascii	"x\314\000\034"
	.asciz	"\315s\035"
	.ascii	"\316\177\034\001"
	.ascii	"\200\317\001\034"
	.ascii	"x\320\000\035"
	.asciz	"\321}\036"
	.ascii	"\322\177\035\001"
	.ascii	"\200\323\001\035"
	.ascii	"\202\324\000\036"
	.asciz	"\325}\037"
	.ascii	"\326\211\036\001"
	.ascii	"\212\327\001\036"
	.ascii	"\202\330\000\037"
	.asciz	"\331} "
	.ascii	"\332\211\037\001"
	.ascii	"\212\333\001\037"
	.ascii	"\202\334\000 "
	.asciz	"\335}!"
	.ascii	"\336\211 \001"
	.ascii	"\212\337\001 "
	.ascii	"\202\340\000!"
	.asciz	"\341}\""
	.ascii	"\342\211!\001"
	.ascii	"\212\343\001!"
	.ascii	"\202\344\000\""
	.asciz	"\345}#"
	.ascii	"\346\211\"\001"
	.ascii	"\212\347\001\""
	.ascii	"\202\350\000#"
	.asciz	"\351}$"
	.ascii	"\352\211#\001"
	.ascii	"\212\353\001#"
	.ascii	"\202\354\000$"
	.asciz	"\355}%"
	.ascii	"\356\211$\001"
	.ascii	"\212\357\001$"
	.ascii	"\202\360\000%"
	.asciz	"\361}&"
	.ascii	"\362\211%\001"
	.ascii	"\212\363\001%"
	.ascii	"\202\364\000&"
	.asciz	"\365\207'"
	.ascii	"\366\211&\001"
	.ascii	"\212\367\001&"
	.ascii	"\214\370\000'"
	.asciz	"\371\207("
	.ascii	"\372E'\001"
	.ascii	"P\373\001'"
	.ascii	"\214\374\000("
	.asciz	"\371\207)"
	.ascii	"\372E(\001"
	.ascii	"P\373\001("
	.ascii	"\214\374\000)"
	.zero	4
	.zero	4
	.zero	4
	.size	_ZL11State_table, 1024

	.type	_ZZ10matchModelR5MixerE1t,@object # @_ZZ10matchModelR5MixerE1t
	.local	_ZZ10matchModelR5MixerE1t
	.comm	_ZZ10matchModelR5MixerE1t,24,8
	.type	_ZGVZ10matchModelR5MixerE1t,@object # @_ZGVZ10matchModelR5MixerE1t
	.local	_ZGVZ10matchModelR5MixerE1t
	.comm	_ZGVZ10matchModelR5MixerE1t,8,8
	.type	_ZZ10matchModelR5MixerE1h,@object # @_ZZ10matchModelR5MixerE1h
	.local	_ZZ10matchModelR5MixerE1h
	.comm	_ZZ10matchModelR5MixerE1h,4,4
	.type	_ZZ10matchModelR5MixerE3ptr,@object # @_ZZ10matchModelR5MixerE3ptr
	.local	_ZZ10matchModelR5MixerE3ptr
	.comm	_ZZ10matchModelR5MixerE3ptr,4,4
	.type	_ZZ10matchModelR5MixerE3len,@object # @_ZZ10matchModelR5MixerE3len
	.local	_ZZ10matchModelR5MixerE3len
	.comm	_ZZ10matchModelR5MixerE3len,4,4
	.type	_ZZ10matchModelR5MixerE6result,@object # @_ZZ10matchModelR5MixerE6result
	.local	_ZZ10matchModelR5MixerE6result
	.comm	_ZZ10matchModelR5MixerE6result,4,4
	.type	_ZZ10matchModelR5MixerE4scm1,@object # @_ZZ10matchModelR5MixerE4scm1
	.local	_ZZ10matchModelR5MixerE4scm1
	.comm	_ZZ10matchModelR5MixerE4scm1,40,8
	.type	_ZGVZ10matchModelR5MixerE4scm1,@object # @_ZGVZ10matchModelR5MixerE4scm1
	.local	_ZGVZ10matchModelR5MixerE4scm1
	.comm	_ZGVZ10matchModelR5MixerE4scm1,8,8
	.type	_ZZ8picModelR5MixerE2r0,@object # @_ZZ8picModelR5MixerE2r0
	.local	_ZZ8picModelR5MixerE2r0
	.comm	_ZZ8picModelR5MixerE2r0,4,4
	.type	_ZZ8picModelR5MixerE2r1,@object # @_ZZ8picModelR5MixerE2r1
	.local	_ZZ8picModelR5MixerE2r1
	.comm	_ZZ8picModelR5MixerE2r1,4,4
	.type	_ZZ8picModelR5MixerE2r2,@object # @_ZZ8picModelR5MixerE2r2
	.local	_ZZ8picModelR5MixerE2r2
	.comm	_ZZ8picModelR5MixerE2r2,4,4
	.type	_ZZ8picModelR5MixerE2r3,@object # @_ZZ8picModelR5MixerE2r3
	.local	_ZZ8picModelR5MixerE2r3
	.comm	_ZZ8picModelR5MixerE2r3,4,4
	.type	_ZZ8picModelR5MixerE1t,@object # @_ZZ8picModelR5MixerE1t
	.local	_ZZ8picModelR5MixerE1t
	.comm	_ZZ8picModelR5MixerE1t,24,8
	.type	_ZGVZ8picModelR5MixerE1t,@object # @_ZGVZ8picModelR5MixerE1t
	.local	_ZGVZ8picModelR5MixerE1t
	.comm	_ZGVZ8picModelR5MixerE1t,8,8
	.type	_ZZ8picModelR5MixerE3cxt,@object # @_ZZ8picModelR5MixerE3cxt
	.local	_ZZ8picModelR5MixerE3cxt
	.comm	_ZZ8picModelR5MixerE3cxt,12,4
	.type	_ZZ8picModelR5MixerE2sm,@object # @_ZZ8picModelR5MixerE2sm
	.local	_ZZ8picModelR5MixerE2sm
	.comm	_ZZ8picModelR5MixerE2sm,96,16
	.type	_ZGVZ8picModelR5MixerE2sm,@object # @_ZGVZ8picModelR5MixerE2sm
	.local	_ZGVZ8picModelR5MixerE2sm
	.comm	_ZGVZ8picModelR5MixerE2sm,8,8
	.type	_ZZ9wordModelR5MixerE5word0,@object # @_ZZ9wordModelR5MixerE5word0
	.local	_ZZ9wordModelR5MixerE5word0
	.comm	_ZZ9wordModelR5MixerE5word0,4,4
	.type	_ZZ9wordModelR5MixerE5word1,@object # @_ZZ9wordModelR5MixerE5word1
	.local	_ZZ9wordModelR5MixerE5word1
	.comm	_ZZ9wordModelR5MixerE5word1,4,4
	.type	_ZZ9wordModelR5MixerE5word2,@object # @_ZZ9wordModelR5MixerE5word2
	.local	_ZZ9wordModelR5MixerE5word2
	.comm	_ZZ9wordModelR5MixerE5word2,4,4
	.type	_ZZ9wordModelR5MixerE5word3,@object # @_ZZ9wordModelR5MixerE5word3
	.local	_ZZ9wordModelR5MixerE5word3
	.comm	_ZZ9wordModelR5MixerE5word3,4,4
	.type	_ZZ9wordModelR5MixerE5word4,@object # @_ZZ9wordModelR5MixerE5word4
	.local	_ZZ9wordModelR5MixerE5word4
	.comm	_ZZ9wordModelR5MixerE5word4,4,4
	.type	_ZZ9wordModelR5MixerE5word5,@object # @_ZZ9wordModelR5MixerE5word5
	.local	_ZZ9wordModelR5MixerE5word5
	.comm	_ZZ9wordModelR5MixerE5word5,4,4
	.type	_ZZ9wordModelR5MixerE5text0,@object # @_ZZ9wordModelR5MixerE5text0
	.local	_ZZ9wordModelR5MixerE5text0
	.comm	_ZZ9wordModelR5MixerE5text0,4,4
	.type	_ZZ9wordModelR5MixerE2cm,@object # @_ZZ9wordModelR5MixerE2cm
	.local	_ZZ9wordModelR5MixerE2cm
	.comm	_ZZ9wordModelR5MixerE2cm,144,8
	.type	_ZGVZ9wordModelR5MixerE2cm,@object # @_ZGVZ9wordModelR5MixerE2cm
	.local	_ZGVZ9wordModelR5MixerE2cm
	.comm	_ZGVZ9wordModelR5MixerE2cm,8,8
	.type	_ZZ9wordModelR5MixerE3nl1,@object # @_ZZ9wordModelR5MixerE3nl1
	.data
	.p2align	2
_ZZ9wordModelR5MixerE3nl1:
	.long	4294967293              # 0xfffffffd
	.size	_ZZ9wordModelR5MixerE3nl1, 4

	.type	_ZZ9wordModelR5MixerE2nl,@object # @_ZZ9wordModelR5MixerE2nl
	.p2align	2
_ZZ9wordModelR5MixerE2nl:
	.long	4294967294              # 0xfffffffe
	.size	_ZZ9wordModelR5MixerE2nl, 4

	.type	_ZZ11recordModelR5MixerE5cpos1,@object # @_ZZ11recordModelR5MixerE5cpos1
	.local	_ZZ11recordModelR5MixerE5cpos1
	.comm	_ZZ11recordModelR5MixerE5cpos1,1024,16
	.type	_ZZ11recordModelR5MixerE5cpos2,@object # @_ZZ11recordModelR5MixerE5cpos2
	.local	_ZZ11recordModelR5MixerE5cpos2
	.comm	_ZZ11recordModelR5MixerE5cpos2,1024,16
	.type	_ZZ11recordModelR5MixerE5cpos3,@object # @_ZZ11recordModelR5MixerE5cpos3
	.local	_ZZ11recordModelR5MixerE5cpos3
	.comm	_ZZ11recordModelR5MixerE5cpos3,1024,16
	.type	_ZZ11recordModelR5MixerE5cpos4,@object # @_ZZ11recordModelR5MixerE5cpos4
	.local	_ZZ11recordModelR5MixerE5cpos4
	.comm	_ZZ11recordModelR5MixerE5cpos4,1024,16
	.type	_ZZ11recordModelR5MixerE5wpos1,@object # @_ZZ11recordModelR5MixerE5wpos1
	.local	_ZZ11recordModelR5MixerE5wpos1
	.comm	_ZZ11recordModelR5MixerE5wpos1,262144,16
	.type	_ZZ11recordModelR5MixerE4rlen,@object # @_ZZ11recordModelR5MixerE4rlen
	.p2align	2
_ZZ11recordModelR5MixerE4rlen:
	.long	2                       # 0x2
	.size	_ZZ11recordModelR5MixerE4rlen, 4

	.type	_ZZ11recordModelR5MixerE5rlen1,@object # @_ZZ11recordModelR5MixerE5rlen1
	.p2align	2
_ZZ11recordModelR5MixerE5rlen1:
	.long	3                       # 0x3
	.size	_ZZ11recordModelR5MixerE5rlen1, 4

	.type	_ZZ11recordModelR5MixerE5rlen2,@object # @_ZZ11recordModelR5MixerE5rlen2
	.p2align	2
_ZZ11recordModelR5MixerE5rlen2:
	.long	4                       # 0x4
	.size	_ZZ11recordModelR5MixerE5rlen2, 4

	.type	_ZZ11recordModelR5MixerE7rcount1,@object # @_ZZ11recordModelR5MixerE7rcount1
	.local	_ZZ11recordModelR5MixerE7rcount1
	.comm	_ZZ11recordModelR5MixerE7rcount1,4,4
	.type	_ZZ11recordModelR5MixerE7rcount2,@object # @_ZZ11recordModelR5MixerE7rcount2
	.local	_ZZ11recordModelR5MixerE7rcount2
	.comm	_ZZ11recordModelR5MixerE7rcount2,4,4
	.type	_ZZ11recordModelR5MixerE2cm,@object # @_ZZ11recordModelR5MixerE2cm
	.local	_ZZ11recordModelR5MixerE2cm
	.comm	_ZZ11recordModelR5MixerE2cm,144,8
	.type	_ZGVZ11recordModelR5MixerE2cm,@object # @_ZGVZ11recordModelR5MixerE2cm
	.local	_ZGVZ11recordModelR5MixerE2cm
	.comm	_ZGVZ11recordModelR5MixerE2cm,8,8
	.type	_ZZ11recordModelR5MixerE2cn,@object # @_ZZ11recordModelR5MixerE2cn
	.local	_ZZ11recordModelR5MixerE2cn
	.comm	_ZZ11recordModelR5MixerE2cn,144,8
	.type	_ZGVZ11recordModelR5MixerE2cn,@object # @_ZGVZ11recordModelR5MixerE2cn
	.local	_ZGVZ11recordModelR5MixerE2cn
	.comm	_ZGVZ11recordModelR5MixerE2cn,8,8
	.type	_ZZ11recordModelR5MixerE2co,@object # @_ZZ11recordModelR5MixerE2co
	.local	_ZZ11recordModelR5MixerE2co
	.comm	_ZZ11recordModelR5MixerE2co,144,8
	.type	_ZGVZ11recordModelR5MixerE2co,@object # @_ZGVZ11recordModelR5MixerE2co
	.local	_ZGVZ11recordModelR5MixerE2co
	.comm	_ZGVZ11recordModelR5MixerE2co,8,8
	.type	_ZZ11recordModelR5MixerE2cp,@object # @_ZZ11recordModelR5MixerE2cp
	.local	_ZZ11recordModelR5MixerE2cp
	.comm	_ZZ11recordModelR5MixerE2cp,144,8
	.type	_ZGVZ11recordModelR5MixerE2cp,@object # @_ZGVZ11recordModelR5MixerE2cp
	.local	_ZGVZ11recordModelR5MixerE2cp
	.comm	_ZGVZ11recordModelR5MixerE2cp,8,8
	.type	_ZZ11sparseModelR5MixeriiE2cm,@object # @_ZZ11sparseModelR5MixeriiE2cm
	.local	_ZZ11sparseModelR5MixeriiE2cm
	.comm	_ZZ11sparseModelR5MixeriiE2cm,144,8
	.type	_ZGVZ11sparseModelR5MixeriiE2cm,@object # @_ZGVZ11sparseModelR5MixeriiE2cm
	.local	_ZGVZ11sparseModelR5MixeriiE2cm
	.comm	_ZGVZ11sparseModelR5MixeriiE2cm,8,8
	.type	_ZZ11sparseModelR5MixeriiE4mask,@object # @_ZZ11sparseModelR5MixeriiE4mask
	.local	_ZZ11sparseModelR5MixeriiE4mask
	.comm	_ZZ11sparseModelR5MixeriiE4mask,4,4
	.type	_ZZ13distanceModelR5MixerE2cr,@object # @_ZZ13distanceModelR5MixerE2cr
	.local	_ZZ13distanceModelR5MixerE2cr
	.comm	_ZZ13distanceModelR5MixerE2cr,144,8
	.type	_ZGVZ13distanceModelR5MixerE2cr,@object # @_ZGVZ13distanceModelR5MixerE2cr
	.local	_ZGVZ13distanceModelR5MixerE2cr
	.comm	_ZGVZ13distanceModelR5MixerE2cr,8,8
	.type	_ZZ13distanceModelR5MixerE5pos00,@object # @_ZZ13distanceModelR5MixerE5pos00
	.local	_ZZ13distanceModelR5MixerE5pos00
	.comm	_ZZ13distanceModelR5MixerE5pos00,4,4
	.type	_ZZ13distanceModelR5MixerE5pos20,@object # @_ZZ13distanceModelR5MixerE5pos20
	.local	_ZZ13distanceModelR5MixerE5pos20
	.comm	_ZZ13distanceModelR5MixerE5pos20,4,4
	.type	_ZZ13distanceModelR5MixerE5posnl,@object # @_ZZ13distanceModelR5MixerE5posnl
	.local	_ZZ13distanceModelR5MixerE5posnl
	.comm	_ZZ13distanceModelR5MixerE5posnl,4,4
	.type	_ZZ8bmpModelR5MixerE1w,@object # @_ZZ8bmpModelR5MixerE1w
	.local	_ZZ8bmpModelR5MixerE1w
	.comm	_ZZ8bmpModelR5MixerE1w,4,4
	.type	_ZZ8bmpModelR5MixerE3eoi,@object # @_ZZ8bmpModelR5MixerE3eoi
	.local	_ZZ8bmpModelR5MixerE3eoi
	.comm	_ZZ8bmpModelR5MixerE3eoi,4,4
	.type	_ZZ8bmpModelR5MixerE4tiff,@object # @_ZZ8bmpModelR5MixerE4tiff
	.local	_ZZ8bmpModelR5MixerE4tiff
	.comm	_ZZ8bmpModelR5MixerE4tiff,4,4
	.type	_ZZ8bmpModelR5MixerE4scm1,@object # @_ZZ8bmpModelR5MixerE4scm1
	.local	_ZZ8bmpModelR5MixerE4scm1
	.comm	_ZZ8bmpModelR5MixerE4scm1,40,8
	.type	_ZGVZ8bmpModelR5MixerE4scm1,@object # @_ZGVZ8bmpModelR5MixerE4scm1
	.local	_ZGVZ8bmpModelR5MixerE4scm1
	.comm	_ZGVZ8bmpModelR5MixerE4scm1,8,8
	.type	_ZZ8bmpModelR5MixerE4scm2,@object # @_ZZ8bmpModelR5MixerE4scm2
	.local	_ZZ8bmpModelR5MixerE4scm2
	.comm	_ZZ8bmpModelR5MixerE4scm2,40,8
	.type	_ZGVZ8bmpModelR5MixerE4scm2,@object # @_ZGVZ8bmpModelR5MixerE4scm2
	.local	_ZGVZ8bmpModelR5MixerE4scm2
	.comm	_ZGVZ8bmpModelR5MixerE4scm2,8,8
	.type	_ZZ8bmpModelR5MixerE4scm3,@object # @_ZZ8bmpModelR5MixerE4scm3
	.local	_ZZ8bmpModelR5MixerE4scm3
	.comm	_ZZ8bmpModelR5MixerE4scm3,40,8
	.type	_ZGVZ8bmpModelR5MixerE4scm3,@object # @_ZGVZ8bmpModelR5MixerE4scm3
	.local	_ZGVZ8bmpModelR5MixerE4scm3
	.comm	_ZGVZ8bmpModelR5MixerE4scm3,8,8
	.type	_ZZ8bmpModelR5MixerE4scm4,@object # @_ZZ8bmpModelR5MixerE4scm4
	.local	_ZZ8bmpModelR5MixerE4scm4
	.comm	_ZZ8bmpModelR5MixerE4scm4,40,8
	.type	_ZGVZ8bmpModelR5MixerE4scm4,@object # @_ZGVZ8bmpModelR5MixerE4scm4
	.local	_ZGVZ8bmpModelR5MixerE4scm4
	.comm	_ZGVZ8bmpModelR5MixerE4scm4,8,8
	.type	_ZZ8bmpModelR5MixerE4scm5,@object # @_ZZ8bmpModelR5MixerE4scm5
	.local	_ZZ8bmpModelR5MixerE4scm5
	.comm	_ZZ8bmpModelR5MixerE4scm5,40,8
	.type	_ZGVZ8bmpModelR5MixerE4scm5,@object # @_ZGVZ8bmpModelR5MixerE4scm5
	.local	_ZGVZ8bmpModelR5MixerE4scm5
	.comm	_ZGVZ8bmpModelR5MixerE4scm5,8,8
	.type	_ZZ8bmpModelR5MixerE4scm6,@object # @_ZZ8bmpModelR5MixerE4scm6
	.local	_ZZ8bmpModelR5MixerE4scm6
	.comm	_ZZ8bmpModelR5MixerE4scm6,40,8
	.type	_ZGVZ8bmpModelR5MixerE4scm6,@object # @_ZGVZ8bmpModelR5MixerE4scm6
	.local	_ZGVZ8bmpModelR5MixerE4scm6
	.comm	_ZGVZ8bmpModelR5MixerE4scm6,8,8
	.type	_ZZ8bmpModelR5MixerE4scm7,@object # @_ZZ8bmpModelR5MixerE4scm7
	.local	_ZZ8bmpModelR5MixerE4scm7
	.comm	_ZZ8bmpModelR5MixerE4scm7,40,8
	.type	_ZGVZ8bmpModelR5MixerE4scm7,@object # @_ZGVZ8bmpModelR5MixerE4scm7
	.local	_ZGVZ8bmpModelR5MixerE4scm7
	.comm	_ZGVZ8bmpModelR5MixerE4scm7,8,8
	.type	_ZZ8bmpModelR5MixerE4scm8,@object # @_ZZ8bmpModelR5MixerE4scm8
	.local	_ZZ8bmpModelR5MixerE4scm8
	.comm	_ZZ8bmpModelR5MixerE4scm8,40,8
	.type	_ZGVZ8bmpModelR5MixerE4scm8,@object # @_ZGVZ8bmpModelR5MixerE4scm8
	.local	_ZGVZ8bmpModelR5MixerE4scm8
	.comm	_ZGVZ8bmpModelR5MixerE4scm8,8,8
	.type	_ZZ8bmpModelR5MixerE4scm9,@object # @_ZZ8bmpModelR5MixerE4scm9
	.local	_ZZ8bmpModelR5MixerE4scm9
	.comm	_ZZ8bmpModelR5MixerE4scm9,40,8
	.type	_ZGVZ8bmpModelR5MixerE4scm9,@object # @_ZGVZ8bmpModelR5MixerE4scm9
	.local	_ZGVZ8bmpModelR5MixerE4scm9
	.comm	_ZGVZ8bmpModelR5MixerE4scm9,8,8
	.type	_ZZ8bmpModelR5MixerE5scm10,@object # @_ZZ8bmpModelR5MixerE5scm10
	.local	_ZZ8bmpModelR5MixerE5scm10
	.comm	_ZZ8bmpModelR5MixerE5scm10,40,8
	.type	_ZGVZ8bmpModelR5MixerE5scm10,@object # @_ZGVZ8bmpModelR5MixerE5scm10
	.local	_ZGVZ8bmpModelR5MixerE5scm10
	.comm	_ZGVZ8bmpModelR5MixerE5scm10,8,8
	.type	_ZZ8bmpModelR5MixerE2cm,@object # @_ZZ8bmpModelR5MixerE2cm
	.local	_ZZ8bmpModelR5MixerE2cm
	.comm	_ZZ8bmpModelR5MixerE2cm,144,8
	.type	_ZGVZ8bmpModelR5MixerE2cm,@object # @_ZGVZ8bmpModelR5MixerE2cm
	.local	_ZGVZ8bmpModelR5MixerE2cm
	.comm	_ZGVZ8bmpModelR5MixerE2cm,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"BMP %dx%d "
	.size	.L.str, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"TIFF %dx%dx%d "
	.size	.L.str.5, 15

	.type	_ZZ9model8bitR5MixeriE4scm1,@object # @_ZZ9model8bitR5MixeriE4scm1
	.local	_ZZ9model8bitR5MixeriE4scm1
	.comm	_ZZ9model8bitR5MixeriE4scm1,40,8
	.type	_ZGVZ9model8bitR5MixeriE4scm1,@object # @_ZGVZ9model8bitR5MixeriE4scm1
	.local	_ZGVZ9model8bitR5MixeriE4scm1
	.comm	_ZGVZ9model8bitR5MixeriE4scm1,8,8
	.type	_ZZ9model8bitR5MixeriE4scm2,@object # @_ZZ9model8bitR5MixeriE4scm2
	.local	_ZZ9model8bitR5MixeriE4scm2
	.comm	_ZZ9model8bitR5MixeriE4scm2,40,8
	.type	_ZGVZ9model8bitR5MixeriE4scm2,@object # @_ZGVZ9model8bitR5MixeriE4scm2
	.local	_ZGVZ9model8bitR5MixeriE4scm2
	.comm	_ZGVZ9model8bitR5MixeriE4scm2,8,8
	.type	_ZZ9model8bitR5MixeriE4scm3,@object # @_ZZ9model8bitR5MixeriE4scm3
	.local	_ZZ9model8bitR5MixeriE4scm3
	.comm	_ZZ9model8bitR5MixeriE4scm3,40,8
	.type	_ZGVZ9model8bitR5MixeriE4scm3,@object # @_ZGVZ9model8bitR5MixeriE4scm3
	.local	_ZGVZ9model8bitR5MixeriE4scm3
	.comm	_ZGVZ9model8bitR5MixeriE4scm3,8,8
	.type	_ZZ9model8bitR5MixeriE4scm4,@object # @_ZZ9model8bitR5MixeriE4scm4
	.local	_ZZ9model8bitR5MixeriE4scm4
	.comm	_ZZ9model8bitR5MixeriE4scm4,40,8
	.type	_ZGVZ9model8bitR5MixeriE4scm4,@object # @_ZGVZ9model8bitR5MixeriE4scm4
	.local	_ZGVZ9model8bitR5MixeriE4scm4
	.comm	_ZGVZ9model8bitR5MixeriE4scm4,8,8
	.type	_ZZ9model8bitR5MixeriE4scm5,@object # @_ZZ9model8bitR5MixeriE4scm5
	.local	_ZZ9model8bitR5MixeriE4scm5
	.comm	_ZZ9model8bitR5MixeriE4scm5,40,8
	.type	_ZGVZ9model8bitR5MixeriE4scm5,@object # @_ZGVZ9model8bitR5MixeriE4scm5
	.local	_ZGVZ9model8bitR5MixeriE4scm5
	.comm	_ZGVZ9model8bitR5MixeriE4scm5,8,8
	.type	_ZZ9model8bitR5MixeriE4scm6,@object # @_ZZ9model8bitR5MixeriE4scm6
	.local	_ZZ9model8bitR5MixeriE4scm6
	.comm	_ZZ9model8bitR5MixeriE4scm6,40,8
	.type	_ZGVZ9model8bitR5MixeriE4scm6,@object # @_ZGVZ9model8bitR5MixeriE4scm6
	.local	_ZGVZ9model8bitR5MixeriE4scm6
	.comm	_ZGVZ9model8bitR5MixeriE4scm6,8,8
	.type	_ZZ9model8bitR5MixeriE4scm7,@object # @_ZZ9model8bitR5MixeriE4scm7
	.local	_ZZ9model8bitR5MixeriE4scm7
	.comm	_ZZ9model8bitR5MixeriE4scm7,40,8
	.type	_ZGVZ9model8bitR5MixeriE4scm7,@object # @_ZGVZ9model8bitR5MixeriE4scm7
	.local	_ZGVZ9model8bitR5MixeriE4scm7
	.comm	_ZGVZ9model8bitR5MixeriE4scm7,8,8
	.type	_ZZ9model8bitR5MixeriE2cm,@object # @_ZZ9model8bitR5MixeriE2cm
	.local	_ZZ9model8bitR5MixeriE2cm
	.comm	_ZZ9model8bitR5MixeriE2cm,144,8
	.type	_ZGVZ9model8bitR5MixeriE2cm,@object # @_ZGVZ9model8bitR5MixeriE2cm
	.local	_ZGVZ9model8bitR5MixeriE2cm
	.comm	_ZGVZ9model8bitR5MixeriE2cm,8,8
	.type	_ZZ8pgmModelR5MixerE1h,@object # @_ZZ8pgmModelR5MixerE1h
	.local	_ZZ8pgmModelR5MixerE1h
	.comm	_ZZ8pgmModelR5MixerE1h,4,4
	.type	_ZZ8pgmModelR5MixerE1w,@object # @_ZZ8pgmModelR5MixerE1w
	.local	_ZZ8pgmModelR5MixerE1w
	.comm	_ZZ8pgmModelR5MixerE1w,4,4
	.type	_ZZ8pgmModelR5MixerE3eoi,@object # @_ZZ8pgmModelR5MixerE3eoi
	.local	_ZZ8pgmModelR5MixerE3eoi
	.comm	_ZZ8pgmModelR5MixerE3eoi,4,4
	.type	_ZZ8pgmModelR5MixerE3pgm,@object # @_ZZ8pgmModelR5MixerE3pgm
	.local	_ZZ8pgmModelR5MixerE3pgm
	.comm	_ZZ8pgmModelR5MixerE3pgm,4,4
	.type	_ZZ8pgmModelR5MixerE7pgm_hdr,@object # @_ZZ8pgmModelR5MixerE7pgm_hdr
	.local	_ZZ8pgmModelR5MixerE7pgm_hdr
	.comm	_ZZ8pgmModelR5MixerE7pgm_hdr,12,4
	.type	_ZZ8pgmModelR5MixerE7pgm_ptr,@object # @_ZZ8pgmModelR5MixerE7pgm_ptr
	.local	_ZZ8pgmModelR5MixerE7pgm_ptr
	.comm	_ZZ8pgmModelR5MixerE7pgm_ptr,4,4
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"PGM %dx%d"
	.size	.L.str.6, 10

	.type	_ZZ8pgmModelR5MixerE3col,@object # @_ZZ8pgmModelR5MixerE3col
	.local	_ZZ8pgmModelR5MixerE3col
	.comm	_ZZ8pgmModelR5MixerE3col,4,4
	.type	_ZZ9bmpModel8R5MixerE1h,@object # @_ZZ9bmpModel8R5MixerE1h
	.local	_ZZ9bmpModel8R5MixerE1h
	.comm	_ZZ9bmpModel8R5MixerE1h,4,4
	.type	_ZZ9bmpModel8R5MixerE1w,@object # @_ZZ9bmpModel8R5MixerE1w
	.local	_ZZ9bmpModel8R5MixerE1w
	.comm	_ZZ9bmpModel8R5MixerE1w,4,4
	.type	_ZZ9bmpModel8R5MixerE3eoi,@object # @_ZZ9bmpModel8R5MixerE3eoi
	.local	_ZZ9bmpModel8R5MixerE3eoi
	.comm	_ZZ9bmpModel8R5MixerE3eoi,4,4
	.type	_ZZ9bmpModel8R5MixerE3col,@object # @_ZZ9bmpModel8R5MixerE3col
	.local	_ZZ9bmpModel8R5MixerE3col
	.comm	_ZZ9bmpModel8R5MixerE3col,4,4
	.type	_ZZ9bmpModel8R5MixerE4ibmp,@object # @_ZZ9bmpModel8R5MixerE4ibmp
	.local	_ZZ9bmpModel8R5MixerE4ibmp
	.comm	_ZZ9bmpModel8R5MixerE4ibmp,4,4
	.type	_ZZ9bmpModel8R5MixerE2w1,@object # @_ZZ9bmpModel8R5MixerE2w1
	.local	_ZZ9bmpModel8R5MixerE2w1
	.comm	_ZZ9bmpModel8R5MixerE2w1,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"BMP(8-bit) %dx%d"
	.size	.L.str.7, 17

	.type	_ZZ9rgbModel8R5MixerE1w,@object # @_ZZ9rgbModel8R5MixerE1w
	.local	_ZZ9rgbModel8R5MixerE1w
	.comm	_ZZ9rgbModel8R5MixerE1w,4,4
	.type	_ZZ9rgbModel8R5MixerE3eoi,@object # @_ZZ9rgbModel8R5MixerE3eoi
	.local	_ZZ9rgbModel8R5MixerE3eoi
	.comm	_ZZ9rgbModel8R5MixerE3eoi,4,4
	.type	_ZZ9rgbModel8R5MixerE3col,@object # @_ZZ9rgbModel8R5MixerE3col
	.local	_ZZ9rgbModel8R5MixerE3col
	.comm	_ZZ9rgbModel8R5MixerE3col,4,4
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"RGB(8-bit) %dx%d"
	.size	.L.str.8, 17

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%s:"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" %02X"
	.size	.L.str.10, 6

	.type	_ZZ9jpegModelR5MixerE4jpeg,@object # @_ZZ9jpegModelR5MixerE4jpeg
	.local	_ZZ9jpegModelR5MixerE4jpeg
	.comm	_ZZ9jpegModelR5MixerE4jpeg,4,4
	.type	_ZZ9jpegModelR5MixerE9next_jpeg,@object # @_ZZ9jpegModelR5MixerE9next_jpeg
	.local	_ZZ9jpegModelR5MixerE9next_jpeg
	.comm	_ZZ9jpegModelR5MixerE9next_jpeg,4,4
	.type	_ZZ9jpegModelR5MixerE3app,@object # @_ZZ9jpegModelR5MixerE3app
	.local	_ZZ9jpegModelR5MixerE3app
	.comm	_ZZ9jpegModelR5MixerE3app,4,4
	.type	_ZZ9jpegModelR5MixerE3sof,@object # @_ZZ9jpegModelR5MixerE3sof
	.local	_ZZ9jpegModelR5MixerE3sof
	.comm	_ZZ9jpegModelR5MixerE3sof,4,4
	.type	_ZZ9jpegModelR5MixerE3sos,@object # @_ZZ9jpegModelR5MixerE3sos
	.local	_ZZ9jpegModelR5MixerE3sos
	.comm	_ZZ9jpegModelR5MixerE3sos,4,4
	.type	_ZZ9jpegModelR5MixerE4data,@object # @_ZZ9jpegModelR5MixerE4data
	.local	_ZZ9jpegModelR5MixerE4data
	.comm	_ZZ9jpegModelR5MixerE4data,4,4
	.type	_ZZ9jpegModelR5MixerE2ht,@object # @_ZZ9jpegModelR5MixerE2ht
	.local	_ZZ9jpegModelR5MixerE2ht
	.comm	_ZZ9jpegModelR5MixerE2ht,24,8
	.type	_ZGVZ9jpegModelR5MixerE2ht,@object # @_ZGVZ9jpegModelR5MixerE2ht
	.local	_ZGVZ9jpegModelR5MixerE2ht
	.comm	_ZGVZ9jpegModelR5MixerE2ht,8,8
	.type	_ZZ9jpegModelR5MixerE6htsize,@object # @_ZZ9jpegModelR5MixerE6htsize
	.local	_ZZ9jpegModelR5MixerE6htsize
	.comm	_ZZ9jpegModelR5MixerE6htsize,4,4
	.type	_ZZ9jpegModelR5MixerE8huffcode,@object # @_ZZ9jpegModelR5MixerE8huffcode
	.local	_ZZ9jpegModelR5MixerE8huffcode
	.comm	_ZZ9jpegModelR5MixerE8huffcode,4,4
	.type	_ZZ9jpegModelR5MixerE8huffbits,@object # @_ZZ9jpegModelR5MixerE8huffbits
	.local	_ZZ9jpegModelR5MixerE8huffbits
	.comm	_ZZ9jpegModelR5MixerE8huffbits,4,4
	.type	_ZZ9jpegModelR5MixerE8huffsize,@object # @_ZZ9jpegModelR5MixerE8huffsize
	.local	_ZZ9jpegModelR5MixerE8huffsize
	.comm	_ZZ9jpegModelR5MixerE8huffsize,4,4
	.type	_ZZ9jpegModelR5MixerE2rs,@object # @_ZZ9jpegModelR5MixerE2rs
	.data
	.p2align	2
_ZZ9jpegModelR5MixerE2rs:
	.long	4294967295              # 0xffffffff
	.size	_ZZ9jpegModelR5MixerE2rs, 4

	.type	_ZZ9jpegModelR5MixerE6mcupos,@object # @_ZZ9jpegModelR5MixerE6mcupos
	.local	_ZZ9jpegModelR5MixerE6mcupos
	.comm	_ZZ9jpegModelR5MixerE6mcupos,4,4
	.type	_ZZ9jpegModelR5MixerE3huf,@object # @_ZZ9jpegModelR5MixerE3huf
	.local	_ZZ9jpegModelR5MixerE3huf
	.comm	_ZZ9jpegModelR5MixerE3huf,24,8
	.type	_ZGVZ9jpegModelR5MixerE3huf,@object # @_ZGVZ9jpegModelR5MixerE3huf
	.local	_ZGVZ9jpegModelR5MixerE3huf
	.comm	_ZGVZ9jpegModelR5MixerE3huf,8,8
	.type	_ZZ9jpegModelR5MixerE7mcusize,@object # @_ZZ9jpegModelR5MixerE7mcusize
	.local	_ZZ9jpegModelR5MixerE7mcusize
	.comm	_ZZ9jpegModelR5MixerE7mcusize,4,4
	.type	_ZZ9jpegModelR5MixerE6hufsel,@object # @_ZZ9jpegModelR5MixerE6hufsel
	.local	_ZZ9jpegModelR5MixerE6hufsel
	.comm	_ZZ9jpegModelR5MixerE6hufsel,80,16
	.type	_ZZ9jpegModelR5MixerE4hbuf,@object # @_ZZ9jpegModelR5MixerE4hbuf
	.local	_ZZ9jpegModelR5MixerE4hbuf
	.comm	_ZZ9jpegModelR5MixerE4hbuf,24,8
	.type	_ZGVZ9jpegModelR5MixerE4hbuf,@object # @_ZGVZ9jpegModelR5MixerE4hbuf
	.local	_ZGVZ9jpegModelR5MixerE4hbuf
	.comm	_ZGVZ9jpegModelR5MixerE4hbuf,8,8
	.type	_ZZ9jpegModelR5MixerE5color,@object # @_ZZ9jpegModelR5MixerE5color
	.local	_ZZ9jpegModelR5MixerE5color
	.comm	_ZZ9jpegModelR5MixerE5color,24,8
	.type	_ZGVZ9jpegModelR5MixerE5color,@object # @_ZGVZ9jpegModelR5MixerE5color
	.local	_ZGVZ9jpegModelR5MixerE5color
	.comm	_ZGVZ9jpegModelR5MixerE5color,8,8
	.type	_ZZ9jpegModelR5MixerE4pred,@object # @_ZZ9jpegModelR5MixerE4pred
	.local	_ZZ9jpegModelR5MixerE4pred
	.comm	_ZZ9jpegModelR5MixerE4pred,24,8
	.type	_ZGVZ9jpegModelR5MixerE4pred,@object # @_ZGVZ9jpegModelR5MixerE4pred
	.local	_ZGVZ9jpegModelR5MixerE4pred
	.comm	_ZGVZ9jpegModelR5MixerE4pred,8,8
	.type	_ZZ9jpegModelR5MixerE2dc,@object # @_ZZ9jpegModelR5MixerE2dc
	.local	_ZZ9jpegModelR5MixerE2dc
	.comm	_ZZ9jpegModelR5MixerE2dc,4,4
	.type	_ZZ9jpegModelR5MixerE5width,@object # @_ZZ9jpegModelR5MixerE5width
	.local	_ZZ9jpegModelR5MixerE5width
	.comm	_ZZ9jpegModelR5MixerE5width,4,4
	.type	_ZZ9jpegModelR5MixerE3row,@object # @_ZZ9jpegModelR5MixerE3row
	.local	_ZZ9jpegModelR5MixerE3row
	.comm	_ZZ9jpegModelR5MixerE3row,4,4
	.type	_ZZ9jpegModelR5MixerE6column,@object # @_ZZ9jpegModelR5MixerE6column
	.local	_ZZ9jpegModelR5MixerE6column
	.comm	_ZZ9jpegModelR5MixerE6column,4,4
	.type	_ZZ9jpegModelR5MixerE4cbuf,@object # @_ZZ9jpegModelR5MixerE4cbuf
	.local	_ZZ9jpegModelR5MixerE4cbuf
	.comm	_ZZ9jpegModelR5MixerE4cbuf,24,8
	.type	_ZGVZ9jpegModelR5MixerE4cbuf,@object # @_ZGVZ9jpegModelR5MixerE4cbuf
	.local	_ZGVZ9jpegModelR5MixerE4cbuf
	.comm	_ZGVZ9jpegModelR5MixerE4cbuf,8,8
	.type	_ZZ9jpegModelR5MixerE4cpos,@object # @_ZZ9jpegModelR5MixerE4cpos
	.local	_ZZ9jpegModelR5MixerE4cpos
	.comm	_ZZ9jpegModelR5MixerE4cpos,4,4
	.type	_ZZ9jpegModelR5MixerE5huff1,@object # @_ZZ9jpegModelR5MixerE5huff1
	.local	_ZZ9jpegModelR5MixerE5huff1
	.comm	_ZZ9jpegModelR5MixerE5huff1,4,4
	.type	_ZZ9jpegModelR5MixerE5huff2,@object # @_ZZ9jpegModelR5MixerE5huff2
	.local	_ZZ9jpegModelR5MixerE5huff2
	.comm	_ZZ9jpegModelR5MixerE5huff2,4,4
	.type	_ZZ9jpegModelR5MixerE5huff3,@object # @_ZZ9jpegModelR5MixerE5huff3
	.local	_ZZ9jpegModelR5MixerE5huff3
	.comm	_ZZ9jpegModelR5MixerE5huff3,4,4
	.type	_ZZ9jpegModelR5MixerE3rs1,@object # @_ZZ9jpegModelR5MixerE3rs1
	.local	_ZZ9jpegModelR5MixerE3rs1
	.comm	_ZZ9jpegModelR5MixerE3rs1,4,4
	.type	_ZZ9jpegModelR5MixerE3rs2,@object # @_ZZ9jpegModelR5MixerE3rs2
	.local	_ZZ9jpegModelR5MixerE3rs2
	.comm	_ZZ9jpegModelR5MixerE3rs2,4,4
	.type	_ZZ9jpegModelR5MixerE3rs3,@object # @_ZZ9jpegModelR5MixerE3rs3
	.local	_ZZ9jpegModelR5MixerE3rs3
	.comm	_ZZ9jpegModelR5MixerE3rs3,4,4
	.type	_ZZ9jpegModelR5MixerE4ssum,@object # @_ZZ9jpegModelR5MixerE4ssum
	.local	_ZZ9jpegModelR5MixerE4ssum
	.comm	_ZZ9jpegModelR5MixerE4ssum,4,4
	.type	_ZZ9jpegModelR5MixerE5ssum1,@object # @_ZZ9jpegModelR5MixerE5ssum1
	.local	_ZZ9jpegModelR5MixerE5ssum1
	.comm	_ZZ9jpegModelR5MixerE5ssum1,4,4
	.type	_ZZ9jpegModelR5MixerE5ssum2,@object # @_ZZ9jpegModelR5MixerE5ssum2
	.local	_ZZ9jpegModelR5MixerE5ssum2
	.comm	_ZZ9jpegModelR5MixerE5ssum2,4,4
	.type	_ZZ9jpegModelR5MixerE5ssum3,@object # @_ZZ9jpegModelR5MixerE5ssum3
	.local	_ZZ9jpegModelR5MixerE5ssum3
	.comm	_ZZ9jpegModelR5MixerE5ssum3,4,4
	.type	_ZZ9jpegModelR5MixerE5cbuf2,@object # @_ZZ9jpegModelR5MixerE5cbuf2
	.local	_ZZ9jpegModelR5MixerE5cbuf2
	.comm	_ZZ9jpegModelR5MixerE5cbuf2,24,8
	.type	_ZGVZ9jpegModelR5MixerE5cbuf2,@object # @_ZGVZ9jpegModelR5MixerE5cbuf2
	.local	_ZGVZ9jpegModelR5MixerE5cbuf2
	.comm	_ZGVZ9jpegModelR5MixerE5cbuf2,8,8
	.type	_ZZ9jpegModelR5MixerE8adv_pred,@object # @_ZZ9jpegModelR5MixerE8adv_pred
	.local	_ZZ9jpegModelR5MixerE8adv_pred
	.comm	_ZZ9jpegModelR5MixerE8adv_pred,24,8
	.type	_ZGVZ9jpegModelR5MixerE8adv_pred,@object # @_ZGVZ9jpegModelR5MixerE8adv_pred
	.local	_ZGVZ9jpegModelR5MixerE8adv_pred
	.comm	_ZGVZ9jpegModelR5MixerE8adv_pred,8,8
	.type	_ZZ9jpegModelR5MixerE4sumu,@object # @_ZZ9jpegModelR5MixerE4sumu
	.local	_ZZ9jpegModelR5MixerE4sumu
	.comm	_ZZ9jpegModelR5MixerE4sumu,24,8
	.type	_ZGVZ9jpegModelR5MixerE4sumu,@object # @_ZGVZ9jpegModelR5MixerE4sumu
	.local	_ZGVZ9jpegModelR5MixerE4sumu
	.comm	_ZGVZ9jpegModelR5MixerE4sumu,8,8
	.type	_ZZ9jpegModelR5MixerE4sumv,@object # @_ZZ9jpegModelR5MixerE4sumv
	.local	_ZZ9jpegModelR5MixerE4sumv
	.comm	_ZZ9jpegModelR5MixerE4sumv,24,8
	.type	_ZGVZ9jpegModelR5MixerE4sumv,@object # @_ZGVZ9jpegModelR5MixerE4sumv
	.local	_ZGVZ9jpegModelR5MixerE4sumv
	.comm	_ZGVZ9jpegModelR5MixerE4sumv,8,8
	.type	_ZZ9jpegModelR5MixerE2ls,@object # @_ZZ9jpegModelR5MixerE2ls
	.local	_ZZ9jpegModelR5MixerE2ls
	.comm	_ZZ9jpegModelR5MixerE2ls,24,8
	.type	_ZGVZ9jpegModelR5MixerE2ls,@object # @_ZGVZ9jpegModelR5MixerE2ls
	.local	_ZGVZ9jpegModelR5MixerE2ls
	.comm	_ZGVZ9jpegModelR5MixerE2ls,8,8
	.type	_ZZ9jpegModelR5MixerE3lcp,@object # @_ZZ9jpegModelR5MixerE3lcp
	.local	_ZZ9jpegModelR5MixerE3lcp
	.comm	_ZZ9jpegModelR5MixerE3lcp,24,8
	.type	_ZGVZ9jpegModelR5MixerE3lcp,@object # @_ZGVZ9jpegModelR5MixerE3lcp
	.local	_ZGVZ9jpegModelR5MixerE3lcp
	.comm	_ZGVZ9jpegModelR5MixerE3lcp,8,8
	.type	_ZZ9jpegModelR5MixerE4zpos,@object # @_ZZ9jpegModelR5MixerE4zpos
	.local	_ZZ9jpegModelR5MixerE4zpos
	.comm	_ZZ9jpegModelR5MixerE4zpos,24,8
	.type	_ZGVZ9jpegModelR5MixerE4zpos,@object # @_ZGVZ9jpegModelR5MixerE4zpos
	.local	_ZGVZ9jpegModelR5MixerE4zpos
	.comm	_ZGVZ9jpegModelR5MixerE4zpos,8,8
	.type	_ZZ9jpegModelR5MixerE9dqt_state,@object # @_ZZ9jpegModelR5MixerE9dqt_state
	.p2align	2
_ZZ9jpegModelR5MixerE9dqt_state:
	.long	4294967295              # 0xffffffff
	.size	_ZZ9jpegModelR5MixerE9dqt_state, 4

	.type	_ZZ9jpegModelR5MixerE7dqt_end,@object # @_ZZ9jpegModelR5MixerE7dqt_end
	.local	_ZZ9jpegModelR5MixerE7dqt_end
	.comm	_ZZ9jpegModelR5MixerE7dqt_end,4,4
	.type	_ZZ9jpegModelR5MixerE4qnum,@object # @_ZZ9jpegModelR5MixerE4qnum
	.local	_ZZ9jpegModelR5MixerE4qnum
	.comm	_ZZ9jpegModelR5MixerE4qnum,4,4
	.type	_ZZ9jpegModelR5MixerE4qtab,@object # @_ZZ9jpegModelR5MixerE4qtab
	.local	_ZZ9jpegModelR5MixerE4qtab
	.comm	_ZZ9jpegModelR5MixerE4qtab,24,8
	.type	_ZGVZ9jpegModelR5MixerE4qtab,@object # @_ZGVZ9jpegModelR5MixerE4qtab
	.local	_ZGVZ9jpegModelR5MixerE4qtab
	.comm	_ZGVZ9jpegModelR5MixerE4qtab,8,8
	.type	_ZZ9jpegModelR5MixerE4qmap,@object # @_ZZ9jpegModelR5MixerE4qmap
	.local	_ZZ9jpegModelR5MixerE4qmap
	.comm	_ZZ9jpegModelR5MixerE4qmap,24,8
	.type	_ZGVZ9jpegModelR5MixerE4qmap,@object # @_ZGVZ9jpegModelR5MixerE4qmap
	.local	_ZGVZ9jpegModelR5MixerE4qmap
	.comm	_ZGVZ9jpegModelR5MixerE4qmap,8,8
	.type	_ZZ9jpegModelR5MixerE3zzu,@object # @_ZZ9jpegModelR5MixerE3zzu
	.section	.rodata,"a",@progbits
	.p2align	4
_ZZ9jpegModelR5MixerE3zzu:
	.ascii	"\000\001\000\000\001\002\003\002\001\000\000\001\002\003\004\005\004\003\002\001\000\000\001\002\003\004\005\006\007\006\005\004\003\002\001\000\001\002\003\004\005\006\007\007\006\005\004\003\002\003\004\005\006\007\007\006\005\004\005\006\007\007\006\007"
	.size	_ZZ9jpegModelR5MixerE3zzu, 64

	.type	_ZZ9jpegModelR5MixerE3zzv,@object # @_ZZ9jpegModelR5MixerE3zzv
	.p2align	4
_ZZ9jpegModelR5MixerE3zzv:
	.ascii	"\000\000\001\002\001\000\000\001\002\003\004\003\002\001\000\000\001\002\003\004\005\006\005\004\003\002\001\000\000\001\002\003\004\005\006\007\007\006\005\004\003\002\001\002\003\004\005\006\007\007\006\005\004\003\004\005\006\007\007\006\005\006\007\007"
	.size	_ZZ9jpegModelR5MixerE3zzv, 64

	.type	.L.str.12,@object       # @.str.12
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.12:
	.asciz	"JPEG %dx%d "
	.size	.L.str.12, 12

	.type	_ZZ9jpegModelR5MixerE2we,@object # @_ZZ9jpegModelR5MixerE2we
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
_ZZ9jpegModelR5MixerE2we:
	.long	181                     # 0xb5
	.long	282                     # 0x11a
	.long	353                     # 0x161
	.long	456                     # 0x1c8
	.long	568                     # 0x238
	.long	671                     # 0x29f
	.long	742                     # 0x2e6
	.long	767                     # 0x2ff
	.size	_ZZ9jpegModelR5MixerE2we, 32

	.type	_ZZ9jpegModelR5MixerE5sumu2,@object # @_ZZ9jpegModelR5MixerE5sumu2
	.local	_ZZ9jpegModelR5MixerE5sumu2
	.comm	_ZZ9jpegModelR5MixerE5sumu2,32,16
	.type	_ZZ9jpegModelR5MixerE5sumv2,@object # @_ZZ9jpegModelR5MixerE5sumv2
	.local	_ZZ9jpegModelR5MixerE5sumv2
	.comm	_ZZ9jpegModelR5MixerE5sumv2,32,16
	.type	_ZZ9jpegModelR5MixerE5sumu3,@object # @_ZZ9jpegModelR5MixerE5sumu3
	.local	_ZZ9jpegModelR5MixerE5sumu3
	.comm	_ZZ9jpegModelR5MixerE5sumu3,32,16
	.type	_ZZ9jpegModelR5MixerE5sumv3,@object # @_ZZ9jpegModelR5MixerE5sumv3
	.local	_ZZ9jpegModelR5MixerE5sumv3
	.comm	_ZZ9jpegModelR5MixerE5sumv3,32,16
	.type	_ZZ9jpegModelR5MixerE2kx,@object # @_ZZ9jpegModelR5MixerE2kx
	.local	_ZZ9jpegModelR5MixerE2kx
	.comm	_ZZ9jpegModelR5MixerE2kx,128,16
	.type	_ZZ9jpegModelR5MixerE1t,@object # @_ZZ9jpegModelR5MixerE1t
	.local	_ZZ9jpegModelR5MixerE1t
	.comm	_ZZ9jpegModelR5MixerE1t,32,8
	.type	_ZGVZ9jpegModelR5MixerE1t,@object # @_ZGVZ9jpegModelR5MixerE1t
	.local	_ZGVZ9jpegModelR5MixerE1t
	.comm	_ZGVZ9jpegModelR5MixerE1t,8,8
	.type	_ZZ9jpegModelR5MixerE3cxt,@object # @_ZZ9jpegModelR5MixerE3cxt
	.local	_ZZ9jpegModelR5MixerE3cxt
	.comm	_ZZ9jpegModelR5MixerE3cxt,24,8
	.type	_ZGVZ9jpegModelR5MixerE3cxt,@object # @_ZGVZ9jpegModelR5MixerE3cxt
	.local	_ZGVZ9jpegModelR5MixerE3cxt
	.comm	_ZGVZ9jpegModelR5MixerE3cxt,8,8
	.type	_ZZ9jpegModelR5MixerE2cp,@object # @_ZZ9jpegModelR5MixerE2cp
	.local	_ZZ9jpegModelR5MixerE2cp
	.comm	_ZZ9jpegModelR5MixerE2cp,24,8
	.type	_ZGVZ9jpegModelR5MixerE2cp,@object # @_ZGVZ9jpegModelR5MixerE2cp
	.local	_ZGVZ9jpegModelR5MixerE2cp
	.comm	_ZGVZ9jpegModelR5MixerE2cp,8,8
	.type	_ZZ9jpegModelR5MixerE2sm,@object # @_ZZ9jpegModelR5MixerE2sm
	.local	_ZZ9jpegModelR5MixerE2sm
	.comm	_ZZ9jpegModelR5MixerE2sm,896,16
	.type	_ZGVZ9jpegModelR5MixerE2sm,@object # @_ZGVZ9jpegModelR5MixerE2sm
	.local	_ZGVZ9jpegModelR5MixerE2sm
	.comm	_ZGVZ9jpegModelR5MixerE2sm,8,8
	.type	_ZZ9jpegModelR5MixerE2m1,@object # @_ZZ9jpegModelR5MixerE2m1
	.local	_ZZ9jpegModelR5MixerE2m1
	.comm	_ZZ9jpegModelR5MixerE2m1,136,8
	.type	_ZGVZ9jpegModelR5MixerE2m1,@object # @_ZGVZ9jpegModelR5MixerE2m1
	.local	_ZGVZ9jpegModelR5MixerE2m1
	.comm	_ZGVZ9jpegModelR5MixerE2m1,8,8
	.type	_ZZ9jpegModelR5MixerE2a1,@object # @_ZZ9jpegModelR5MixerE2a1
	.local	_ZZ9jpegModelR5MixerE2a1
	.comm	_ZZ9jpegModelR5MixerE2a1,32,8
	.type	_ZGVZ9jpegModelR5MixerE2a1,@object # @_ZGVZ9jpegModelR5MixerE2a1
	.local	_ZGVZ9jpegModelR5MixerE2a1
	.comm	_ZGVZ9jpegModelR5MixerE2a1,8,8
	.type	_ZZ9jpegModelR5MixerE2a2,@object # @_ZZ9jpegModelR5MixerE2a2
	.local	_ZZ9jpegModelR5MixerE2a2
	.comm	_ZZ9jpegModelR5MixerE2a2,32,8
	.type	_ZGVZ9jpegModelR5MixerE2a2,@object # @_ZGVZ9jpegModelR5MixerE2a2
	.local	_ZGVZ9jpegModelR5MixerE2a2
	.comm	_ZGVZ9jpegModelR5MixerE2a2,8,8
	.type	_ZZ9jpegModelR5MixerE7hbcount,@object # @_ZZ9jpegModelR5MixerE7hbcount
	.data
	.p2align	2
_ZZ9jpegModelR5MixerE7hbcount:
	.long	2                       # 0x2
	.size	_ZZ9jpegModelR5MixerE7hbcount, 4

	.type	_ZZ8wavModelR5MixerE8channels,@object # @_ZZ8wavModelR5MixerE8channels
	.local	_ZZ8wavModelR5MixerE8channels
	.comm	_ZZ8wavModelR5MixerE8channels,4,4
	.type	_ZZ8wavModelR5MixerE4bits,@object # @_ZZ8wavModelR5MixerE4bits
	.local	_ZZ8wavModelR5MixerE4bits
	.comm	_ZZ8wavModelR5MixerE4bits,4,4
	.type	_ZZ8wavModelR5MixerE5bytes,@object # @_ZZ8wavModelR5MixerE5bytes
	.local	_ZZ8wavModelR5MixerE5bytes
	.comm	_ZZ8wavModelR5MixerE5bytes,4,4
	.type	_ZZ8wavModelR5MixerE3eof,@object # @_ZZ8wavModelR5MixerE3eof
	.local	_ZZ8wavModelR5MixerE3eof
	.comm	_ZZ8wavModelR5MixerE3eof,4,4
	.type	_ZZ8wavModelR5MixerE1s,@object # @_ZZ8wavModelR5MixerE1s
	.local	_ZZ8wavModelR5MixerE1s
	.comm	_ZZ8wavModelR5MixerE1s,4,4
	.type	_ZZ8wavModelR5MixerE1w,@object # @_ZZ8wavModelR5MixerE1w
	.local	_ZZ8wavModelR5MixerE1w
	.comm	_ZZ8wavModelR5MixerE1w,4,4
	.type	_ZZ8wavModelR5MixerE1K,@object # @_ZZ8wavModelR5MixerE1K
	.local	_ZZ8wavModelR5MixerE1K
	.comm	_ZZ8wavModelR5MixerE1K,4,4
	.type	_ZGVZ8wavModelR5MixerE1K,@object # @_ZGVZ8wavModelR5MixerE1K
	.local	_ZGVZ8wavModelR5MixerE1K
	.comm	_ZGVZ8wavModelR5MixerE1K,8,8
	.type	_ZZ8wavModelR5MixerE1n,@object # @_ZZ8wavModelR5MixerE1n
	.local	_ZZ8wavModelR5MixerE1n
	.comm	_ZZ8wavModelR5MixerE1n,8,4
	.type	_ZZ8wavModelR5MixerE7counter,@object # @_ZZ8wavModelR5MixerE7counter
	.local	_ZZ8wavModelR5MixerE7counter
	.comm	_ZZ8wavModelR5MixerE7counter,8,4
	.type	_ZZ8wavModelR5MixerE4scm1,@object # @_ZZ8wavModelR5MixerE4scm1
	.local	_ZZ8wavModelR5MixerE4scm1
	.comm	_ZZ8wavModelR5MixerE4scm1,40,8
	.type	_ZGVZ8wavModelR5MixerE4scm1,@object # @_ZGVZ8wavModelR5MixerE4scm1
	.local	_ZGVZ8wavModelR5MixerE4scm1
	.comm	_ZGVZ8wavModelR5MixerE4scm1,8,8
	.type	_ZZ8wavModelR5MixerE4scm2,@object # @_ZZ8wavModelR5MixerE4scm2
	.local	_ZZ8wavModelR5MixerE4scm2
	.comm	_ZZ8wavModelR5MixerE4scm2,40,8
	.type	_ZGVZ8wavModelR5MixerE4scm2,@object # @_ZGVZ8wavModelR5MixerE4scm2
	.local	_ZGVZ8wavModelR5MixerE4scm2
	.comm	_ZGVZ8wavModelR5MixerE4scm2,8,8
	.type	_ZZ8wavModelR5MixerE4scm3,@object # @_ZZ8wavModelR5MixerE4scm3
	.local	_ZZ8wavModelR5MixerE4scm3
	.comm	_ZZ8wavModelR5MixerE4scm3,40,8
	.type	_ZGVZ8wavModelR5MixerE4scm3,@object # @_ZGVZ8wavModelR5MixerE4scm3
	.local	_ZGVZ8wavModelR5MixerE4scm3
	.comm	_ZGVZ8wavModelR5MixerE4scm3,8,8
	.type	_ZZ8wavModelR5MixerE4scm4,@object # @_ZZ8wavModelR5MixerE4scm4
	.local	_ZZ8wavModelR5MixerE4scm4
	.comm	_ZZ8wavModelR5MixerE4scm4,40,8
	.type	_ZGVZ8wavModelR5MixerE4scm4,@object # @_ZGVZ8wavModelR5MixerE4scm4
	.local	_ZGVZ8wavModelR5MixerE4scm4
	.comm	_ZGVZ8wavModelR5MixerE4scm4,8,8
	.type	_ZZ8wavModelR5MixerE4scm5,@object # @_ZZ8wavModelR5MixerE4scm5
	.local	_ZZ8wavModelR5MixerE4scm5
	.comm	_ZZ8wavModelR5MixerE4scm5,40,8
	.type	_ZGVZ8wavModelR5MixerE4scm5,@object # @_ZGVZ8wavModelR5MixerE4scm5
	.local	_ZGVZ8wavModelR5MixerE4scm5
	.comm	_ZGVZ8wavModelR5MixerE4scm5,8,8
	.type	_ZZ8wavModelR5MixerE4scm6,@object # @_ZZ8wavModelR5MixerE4scm6
	.local	_ZZ8wavModelR5MixerE4scm6
	.comm	_ZZ8wavModelR5MixerE4scm6,40,8
	.type	_ZGVZ8wavModelR5MixerE4scm6,@object # @_ZGVZ8wavModelR5MixerE4scm6
	.local	_ZGVZ8wavModelR5MixerE4scm6
	.comm	_ZGVZ8wavModelR5MixerE4scm6,8,8
	.type	_ZZ8wavModelR5MixerE4scm7,@object # @_ZZ8wavModelR5MixerE4scm7
	.local	_ZZ8wavModelR5MixerE4scm7
	.comm	_ZZ8wavModelR5MixerE4scm7,40,8
	.type	_ZGVZ8wavModelR5MixerE4scm7,@object # @_ZGVZ8wavModelR5MixerE4scm7
	.local	_ZGVZ8wavModelR5MixerE4scm7
	.comm	_ZGVZ8wavModelR5MixerE4scm7,8,8
	.type	_ZZ8wavModelR5MixerE4scm8,@object # @_ZZ8wavModelR5MixerE4scm8
	.local	_ZZ8wavModelR5MixerE4scm8
	.comm	_ZZ8wavModelR5MixerE4scm8,40,8
	.type	_ZGVZ8wavModelR5MixerE4scm8,@object # @_ZGVZ8wavModelR5MixerE4scm8
	.local	_ZGVZ8wavModelR5MixerE4scm8
	.comm	_ZGVZ8wavModelR5MixerE4scm8,8,8
	.type	_ZZ8wavModelR5MixerE2cm,@object # @_ZZ8wavModelR5MixerE2cm
	.local	_ZZ8wavModelR5MixerE2cm
	.comm	_ZZ8wavModelR5MixerE2cm,144,8
	.type	_ZGVZ8wavModelR5MixerE2cm,@object # @_ZGVZ8wavModelR5MixerE2cm
	.local	_ZGVZ8wavModelR5MixerE2cm
	.comm	_ZGVZ8wavModelR5MixerE2cm,8,8
	.type	_ZL1S,@object           # @_ZL1S
	.local	_ZL1S
	.comm	_ZL1S,4,4
	.type	_ZL1D,@object           # @_ZL1D
	.local	_ZL1D
	.comm	_ZL1D,4,4
	.type	_ZL5wmode,@object       # @_ZL5wmode
	.local	_ZL5wmode
	.comm	_ZL5wmode,4,4
	.type	.L.str.14,@object       # @.str.14
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.14:
	.asciz	"WAV %ibits/"
	.size	.L.str.14, 12

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"mono "
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"stereo "
	.size	.L.str.16, 8

	.type	_ZZ8exeModelR5MixerE2cm,@object # @_ZZ8exeModelR5MixerE2cm
	.local	_ZZ8exeModelR5MixerE2cm
	.comm	_ZZ8exeModelR5MixerE2cm,144,8
	.type	_ZGVZ8exeModelR5MixerE2cm,@object # @_ZGVZ8exeModelR5MixerE2cm
	.local	_ZGVZ8exeModelR5MixerE2cm
	.comm	_ZGVZ8exeModelR5MixerE2cm,8,8
	.type	_ZZ13indirectModelR5MixerE2cm,@object # @_ZZ13indirectModelR5MixerE2cm
	.local	_ZZ13indirectModelR5MixerE2cm
	.comm	_ZZ13indirectModelR5MixerE2cm,144,8
	.type	_ZGVZ13indirectModelR5MixerE2cm,@object # @_ZGVZ13indirectModelR5MixerE2cm
	.local	_ZGVZ13indirectModelR5MixerE2cm
	.comm	_ZGVZ13indirectModelR5MixerE2cm,8,8
	.type	_ZZ13indirectModelR5MixerE2t1,@object # @_ZZ13indirectModelR5MixerE2t1
	.local	_ZZ13indirectModelR5MixerE2t1
	.comm	_ZZ13indirectModelR5MixerE2t1,1024,16
	.type	_ZZ13indirectModelR5MixerE2t2,@object # @_ZZ13indirectModelR5MixerE2t2
	.local	_ZZ13indirectModelR5MixerE2t2
	.comm	_ZZ13indirectModelR5MixerE2t2,131072,16
	.type	_ZZ8dmcModelR5MixerE3top,@object # @_ZZ8dmcModelR5MixerE3top
	.local	_ZZ8dmcModelR5MixerE3top
	.comm	_ZZ8dmcModelR5MixerE3top,4,4
	.type	_ZZ8dmcModelR5MixerE4curr,@object # @_ZZ8dmcModelR5MixerE4curr
	.local	_ZZ8dmcModelR5MixerE4curr
	.comm	_ZZ8dmcModelR5MixerE4curr,4,4
	.type	_ZZ8dmcModelR5MixerE1t,@object # @_ZZ8dmcModelR5MixerE1t
	.local	_ZZ8dmcModelR5MixerE1t
	.comm	_ZZ8dmcModelR5MixerE1t,24,8
	.type	_ZGVZ8dmcModelR5MixerE1t,@object # @_ZGVZ8dmcModelR5MixerE1t
	.local	_ZGVZ8dmcModelR5MixerE1t
	.comm	_ZGVZ8dmcModelR5MixerE1t,8,8
	.type	_ZZ8dmcModelR5MixerE2sm,@object # @_ZZ8dmcModelR5MixerE2sm
	.local	_ZZ8dmcModelR5MixerE2sm
	.comm	_ZZ8dmcModelR5MixerE2sm,32,16
	.type	_ZGVZ8dmcModelR5MixerE2sm,@object # @_ZGVZ8dmcModelR5MixerE2sm
	.local	_ZGVZ8dmcModelR5MixerE2sm
	.comm	_ZGVZ8dmcModelR5MixerE2sm,8,8
	.type	_ZZ8dmcModelR5MixerE9threshold,@object # @_ZZ8dmcModelR5MixerE9threshold
	.data
	.p2align	2
_ZZ8dmcModelR5MixerE9threshold:
	.long	256                     # 0x100
	.size	_ZZ8dmcModelR5MixerE9threshold, 4

	.type	_ZZ13contextModel2vE2cm,@object # @_ZZ13contextModel2vE2cm
	.local	_ZZ13contextModel2vE2cm
	.comm	_ZZ13contextModel2vE2cm,144,8
	.type	_ZGVZ13contextModel2vE2cm,@object # @_ZGVZ13contextModel2vE2cm
	.local	_ZGVZ13contextModel2vE2cm
	.comm	_ZGVZ13contextModel2vE2cm,8,8
	.type	_ZZ13contextModel2vE4rcm7,@object # @_ZZ13contextModel2vE4rcm7
	.local	_ZZ13contextModel2vE4rcm7
	.comm	_ZZ13contextModel2vE4rcm7,40,8
	.type	_ZGVZ13contextModel2vE4rcm7,@object # @_ZGVZ13contextModel2vE4rcm7
	.local	_ZGVZ13contextModel2vE4rcm7
	.comm	_ZGVZ13contextModel2vE4rcm7,8,8
	.type	_ZZ13contextModel2vE4rcm9,@object # @_ZZ13contextModel2vE4rcm9
	.local	_ZZ13contextModel2vE4rcm9
	.comm	_ZZ13contextModel2vE4rcm9,40,8
	.type	_ZGVZ13contextModel2vE4rcm9,@object # @_ZGVZ13contextModel2vE4rcm9
	.local	_ZGVZ13contextModel2vE4rcm9
	.comm	_ZGVZ13contextModel2vE4rcm9,8,8
	.type	_ZZ13contextModel2vE5rcm10,@object # @_ZZ13contextModel2vE5rcm10
	.local	_ZZ13contextModel2vE5rcm10
	.comm	_ZZ13contextModel2vE5rcm10,40,8
	.type	_ZGVZ13contextModel2vE5rcm10,@object # @_ZGVZ13contextModel2vE5rcm10
	.local	_ZGVZ13contextModel2vE5rcm10
	.comm	_ZGVZ13contextModel2vE5rcm10,8,8
	.type	_ZZ13contextModel2vE1m,@object # @_ZZ13contextModel2vE1m
	.local	_ZZ13contextModel2vE1m
	.comm	_ZZ13contextModel2vE1m,136,8
	.type	_ZGVZ13contextModel2vE1m,@object # @_ZGVZ13contextModel2vE1m
	.local	_ZGVZ13contextModel2vE1m
	.comm	_ZGVZ13contextModel2vE1m,8,8
	.type	_ZZ13contextModel2vE3cxt,@object # @_ZZ13contextModel2vE3cxt
	.local	_ZZ13contextModel2vE3cxt
	.comm	_ZZ13contextModel2vE3cxt,64,16
	.type	_ZZ13contextModel2vE8filetype,@object # @_ZZ13contextModel2vE8filetype
	.local	_ZZ13contextModel2vE8filetype
	.comm	_ZZ13contextModel2vE8filetype,4,4
	.type	_ZZ13contextModel2vE4size,@object # @_ZZ13contextModel2vE4size
	.local	_ZZ13contextModel2vE4size
	.comm	_ZZ13contextModel2vE4size,4,4
	.type	_ZZ13contextModel2vE3col,@object # @_ZZ13contextModel2vE3col
	.local	_ZZ13contextModel2vE3col
	.comm	_ZZ13contextModel2vE3col,4,4
	.type	_ZZ13contextModel2vE3col_0,@object # @_ZZ13contextModel2vE3col_0
	.local	_ZZ13contextModel2vE3col_0
	.comm	_ZZ13contextModel2vE3col_0,4,4
	.type	_ZZN9Predictor6updateEvE1a,@object # @_ZZN9Predictor6updateEvE1a
	.local	_ZZN9Predictor6updateEvE1a
	.comm	_ZZN9Predictor6updateEvE1a,32,8
	.type	_ZGVZN9Predictor6updateEvE1a,@object # @_ZGVZN9Predictor6updateEvE1a
	.local	_ZGVZN9Predictor6updateEvE1a
	.comm	_ZGVZN9Predictor6updateEvE1a,8,8
	.type	_ZZN9Predictor6updateEvE2a1,@object # @_ZZN9Predictor6updateEvE2a1
	.local	_ZZN9Predictor6updateEvE2a1
	.comm	_ZZN9Predictor6updateEvE2a1,32,8
	.type	_ZGVZN9Predictor6updateEvE2a1,@object # @_ZGVZN9Predictor6updateEvE2a1
	.local	_ZGVZN9Predictor6updateEvE2a1
	.comm	_ZGVZN9Predictor6updateEvE2a1,8,8
	.type	_ZZN9Predictor6updateEvE2a2,@object # @_ZZN9Predictor6updateEvE2a2
	.local	_ZZN9Predictor6updateEvE2a2
	.comm	_ZZN9Predictor6updateEvE2a2,32,8
	.type	_ZGVZN9Predictor6updateEvE2a2,@object # @_ZGVZN9Predictor6updateEvE2a2
	.local	_ZGVZN9Predictor6updateEvE2a2
	.comm	_ZGVZN9Predictor6updateEvE2a2,8,8
	.type	_ZZN9Predictor6updateEvE2a3,@object # @_ZZN9Predictor6updateEvE2a3
	.local	_ZZN9Predictor6updateEvE2a3
	.comm	_ZZN9Predictor6updateEvE2a3,32,8
	.type	_ZGVZN9Predictor6updateEvE2a3,@object # @_ZGVZN9Predictor6updateEvE2a3
	.local	_ZGVZN9Predictor6updateEvE2a3
	.comm	_ZGVZN9Predictor6updateEvE2a3,8,8
	.type	_ZZN9Predictor6updateEvE2a4,@object # @_ZZN9Predictor6updateEvE2a4
	.local	_ZZN9Predictor6updateEvE2a4
	.comm	_ZZN9Predictor6updateEvE2a4,32,8
	.type	_ZGVZN9Predictor6updateEvE2a4,@object # @_ZGVZN9Predictor6updateEvE2a4
	.local	_ZGVZN9Predictor6updateEvE2a4
	.comm	_ZGVZN9Predictor6updateEvE2a4,8,8
	.type	_ZZN9Predictor6updateEvE2a5,@object # @_ZZN9Predictor6updateEvE2a5
	.local	_ZZN9Predictor6updateEvE2a5
	.comm	_ZZN9Predictor6updateEvE2a5,32,8
	.type	_ZGVZN9Predictor6updateEvE2a5,@object # @_ZGVZN9Predictor6updateEvE2a5
	.local	_ZGVZN9Predictor6updateEvE2a5
	.comm	_ZGVZN9Predictor6updateEvE2a5,8,8
	.type	_ZZN9Predictor6updateEvE2a6,@object # @_ZZN9Predictor6updateEvE2a6
	.local	_ZZN9Predictor6updateEvE2a6
	.comm	_ZZN9Predictor6updateEvE2a6,32,8
	.type	_ZGVZN9Predictor6updateEvE2a6,@object # @_ZGVZN9Predictor6updateEvE2a6
	.local	_ZGVZN9Predictor6updateEvE2a6
	.comm	_ZGVZN9Predictor6updateEvE2a6,8,8
	.type	_ZL2dt,@object          # @_ZL2dt
	.local	_ZL2dt
	.comm	_ZL2dt,4096,16
	.type	.L.str.17,@object       # @.str.17
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.17:
	.asciz	"%c%c%c%c"
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"encode_exe read error"
	.size	.L.str.18, 22

	.type	_ZZ10decode_exeR7EncoderE6offset,@object # @_ZZ10decode_exeR7EncoderE6offset
	.local	_ZZ10decode_exeR7EncoderE6offset
	.comm	_ZZ10decode_exeR7EncoderE6offset,4,4
	.type	_ZZ10decode_exeR7EncoderE1q,@object # @_ZZ10decode_exeR7EncoderE1q
	.local	_ZZ10decode_exeR7EncoderE1q
	.comm	_ZZ10decode_exeR7EncoderE1q,4,4
	.type	_ZZ10decode_exeR7EncoderE4size,@object # @_ZZ10decode_exeR7EncoderE4size
	.local	_ZZ10decode_exeR7EncoderE4size
	.comm	_ZZ10decode_exeR7EncoderE4size,4,4
	.type	_ZZ10decode_exeR7EncoderE5begin,@object # @_ZZ10decode_exeR7EncoderE5begin
	.local	_ZZ10decode_exeR7EncoderE5begin
	.comm	_ZZ10decode_exeR7EncoderE5begin,4,4
	.type	_ZZ10decode_exeR7EncoderE1c,@object # @_ZZ10decode_exeR7EncoderE1c
	.local	_ZZ10decode_exeR7EncoderE1c
	.comm	_ZZ10decode_exeR7EncoderE1c,5,4
	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%c%c%c%c%c"
	.size	.L.str.19, 11

	.type	_ZZ6decodeR7EncoderE4type,@object # @_ZZ6decodeR7EncoderE4type
	.local	_ZZ6decodeR7EncoderE4type
	.comm	_ZZ6decodeR7EncoderE4type,4,4
	.type	_ZZ6decodeR7EncoderE3len,@object # @_ZZ6decodeR7EncoderE3len
	.local	_ZZ6decodeR7EncoderE3len
	.comm	_ZZ6decodeR7EncoderE3len,4,4
	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%12d\b\b\b\b\b\b\b\b\b\b\b\b"
	.size	.L.str.20, 17

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"rb"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%s %ld -> "
	.size	.L.str.22, 11

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"tmpfile"
	.size	.L.str.23, 8

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Transform fails at %ld, input=%d decoded=%d, skipping...\n"
	.size	.L.str.24, 58

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"%-12ld\n"
	.size	.L.str.25, 8

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Comparing %s %ld -> "
	.size	.L.str.26, 21

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"differ at %d: file=%d archive=%d\n"
	.size	.L.str.27, 34

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"wb"
	.size	.L.str.30, 3

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Extracting %s %ld -> "
	.size	.L.str.32, 22

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Skipping %s %ld -> "
	.size	.L.str.34, 20

	.type	_ZZ7getlineP8_IO_FILEE1s,@object # @_ZZ7getlineP8_IO_FILEE1s
	.local	_ZZ7getlineP8_IO_FILEE1s
	.comm	_ZZ7getlineP8_IO_FILEE1s,24,8
	.type	_ZGVZ7getlineP8_IO_FILEE1s,@object # @_ZGVZ7getlineP8_IO_FILEE1s
	.local	_ZGVZ7getlineP8_IO_FILEE1s
	.comm	_ZGVZ7getlineP8_IO_FILEE1s,8,8
	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.zero	1
	.size	.L.str.36, 1

	.type	_ZZ7putsizeR6StringS0_PKciE3blk,@object # @_ZZ7putsizeR6StringS0_PKciE3blk
	.local	_ZZ7putsizeR6StringS0_PKciE3blk
	.comm	_ZZ7putsizeR6StringS0_PKciE3blk,24,16
	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"%ld\t"
	.size	.L.str.37, 5

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Valid options are -0 through -9 or -d\n"
	.size	.L.str.39, 39

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"paq8p archiver (C) 2008, Matt Mahoney et al.\nFree under GPL, http://www.gnu.org/licenses/gpl.txt\n\nTo compress:\n  paq8p -level file               (compresses to file.paq8p)\n  paq8p -level archive files...   (creates archive.paq8p)\n  paq8p file                      (level -%d, pause when done)\nlevel: -0 = store, -1 -2 -3 = faster (uses 35, 48, 59 MB)\n-4 -5 -6 -7 -8 = smaller (uses 133, 233, 435, 837, 1643 MB)\n\nTo extract or compare:\n  paq8p -d dir1/archive.paq8p      (extract to dir1)\n  paq8p -d dir1/archive.paq8p dir2 (extract to dir2)\n  paq8p archive.paq8p              (extract, pause when done)\n\nTo view contents: more < archive.paq8p\n\n"
	.size	.L.str.40, 645

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"%s: not found, skipping...\n"
	.size	.L.str.42, 28

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"Nothing to compress\n"
	.size	.L.str.43, 21

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"wb+"
	.size	.L.str.44, 4

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"paq8p -%d\r\n%s\032"
	.size	.L.str.45, 15

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"Creating archive with %d file(s)...\n"
	.size	.L.str.46, 37

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"rb+"
	.size	.L.str.47, 4

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"paq8p -"
	.size	.L.str.48, 8

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"%s: not a %s file\n"
	.size	.L.str.49, 19

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"paq8p"
	.size	.L.str.50, 6

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Extracting %d file(s) from archive -%d\n"
	.size	.L.str.51, 40

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"%s: header corrupted at %ld\n"
	.size	.L.str.52, 29

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"%ld -> %ld\n"
	.size	.L.str.53, 12

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"fork() failed"
	.size	.L.str.57, 14

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"-d"
	.size	.L.str.58, 3

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"Out of memory"
	.size	.L.str.59, 14

	.type	_ZZN2BHILi4EEixEjE3tmp,@object # @_ZZN2BHILi4EEixEjE3tmp
	.section	.bss._ZZN2BHILi4EEixEjE3tmp,"aGw",@nobits,_ZZN2BHILi4EEixEjE3tmp,comdat
	.weak	_ZZN2BHILi4EEixEjE3tmp
_ZZN2BHILi4EEixEjE3tmp:
	.zero	4
	.size	_ZZN2BHILi4EEixEjE3tmp, 4

	.type	_ZZN2BHILi9EEixEjE3tmp,@object # @_ZZN2BHILi9EEixEjE3tmp
	.section	.bss._ZZN2BHILi9EEixEjE3tmp,"aGw",@nobits,_ZZN2BHILi9EEixEjE3tmp,comdat
	.weak	_ZZN2BHILi9EEixEjE3tmp
_ZZN2BHILi9EEixEjE3tmp:
	.zero	9
	.size	_ZZN2BHILi9EEixEjE3tmp, 9

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_paq8p.ii
	.type	_ZZ8wavModelR5MixerE2pr.0,@object # @_ZZ8wavModelR5MixerE2pr.0
	.local	_ZZ8wavModelR5MixerE2pr.0
	.comm	_ZZ8wavModelR5MixerE2pr.0,8,16
	.type	_ZZ8wavModelR5MixerE2pr.1,@object # @_ZZ8wavModelR5MixerE2pr.1
	.local	_ZZ8wavModelR5MixerE2pr.1
	.comm	_ZZ8wavModelR5MixerE2pr.1,8,8
	.type	_ZZ8wavModelR5MixerE2pr.2,@object # @_ZZ8wavModelR5MixerE2pr.2
	.local	_ZZ8wavModelR5MixerE2pr.2
	.comm	_ZZ8wavModelR5MixerE2pr.2,8,16
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr:
	.asciz	"not extracted"
	.size	.Lstr, 14

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"done        "
	.size	.Lstr.1, 13

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"identical   "
	.size	.Lstr.2, 13

	.type	.Lstr.3,@object         # @str.3
.Lstr.3:
	.asciz	"file is longer"
	.size	.Lstr.3, 15

	.type	.Lstr.4,@object         # @str.4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.4:
	.asciz	"\nClose this window or press ENTER to continue..."
	.size	.Lstr.4, 49


	.globl	_ZN4IlogC1Ev
	.type	_ZN4IlogC1Ev,@function
_ZN4IlogC1Ev = _ZN4IlogC2Ev
	.globl	_ZN7StretchC1Ev
	.type	_ZN7StretchC1Ev,@function
_ZN7StretchC1Ev = _ZN7StretchC2Ev
	.globl	_ZN5MixerD1Ev
	.type	_ZN5MixerD1Ev,@function
_ZN5MixerD1Ev = _ZN5MixerD2Ev
	.globl	_ZN5MixerC1Eiiii
	.type	_ZN5MixerC1Eiiii,@function
_ZN5MixerC1Eiiii = _ZN5MixerC2Eiiii
	.globl	_ZN4APM1C1Ei
	.type	_ZN4APM1C1Ei,@function
_ZN4APM1C1Ei = _ZN4APM1C2Ei
	.globl	_ZN8StateMapC1Ei
	.type	_ZN8StateMapC1Ei,@function
_ZN8StateMapC1Ei = _ZN8StateMapC2Ei
	.globl	_ZN3APMC1Ei
	.type	_ZN3APMC1Ei,@function
_ZN3APMC1Ei = _ZN3APMC2Ei
	.globl	_ZN10ContextMapC1Eii
	.type	_ZN10ContextMapC1Eii,@function
_ZN10ContextMapC1Eii = _ZN10ContextMapC2Eii
	.globl	_ZN10ContextMapD1Ev
	.type	_ZN10ContextMapD1Ev,@function
_ZN10ContextMapD1Ev = _ZN10ContextMapD2Ev
	.globl	_ZN9PredictorC1Ev
	.type	_ZN9PredictorC1Ev,@function
_ZN9PredictorC1Ev = _ZN9PredictorC2Ev
	.globl	_ZN7EncoderC1E4ModeP8_IO_FILE
	.type	_ZN7EncoderC1E4ModeP8_IO_FILE,@function
_ZN7EncoderC1E4ModeP8_IO_FILE = _ZN7EncoderC2E4ModeP8_IO_FILE
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
