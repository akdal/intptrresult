	.text
	.file	"splay2.bc"
	.globl	CHfind
	.p2align	4, 0x90
	.type	CHfind,@function
CHfind:                                 # @CHfind
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 32
	movq	%rdi, %rax
	leaq	32(%rsp), %rcx
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	32(%rsp), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_5
# BB#1:
	ucomisd	%xmm1, %xmm0
	jne	.LBB0_6
	jp	.LBB0_6
# BB#2:
	movsd	8(%rax), %xmm2          # xmm2 = mem[0],zero
	ucomisd	8(%rcx), %xmm2
	ja	.LBB0_5
# BB#3:
	movsd	8(%rax), %xmm2          # xmm2 = mem[0],zero
	ucomisd	8(%rcx), %xmm2
	jne	.LBB0_6
	jp	.LBB0_6
# BB#4:
	movl	16(%rax), %edx
	cmpl	16(%rcx), %edx
	jge	.LBB0_6
.LBB0_5:
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_12
.LBB0_6:                                # %.thread18
	ucomisd	%xmm1, %xmm0
	ja	.LBB0_11
# BB#7:
	jne	.LBB0_13
	jp	.LBB0_13
# BB#8:
	movsd	8(%rcx), %xmm0          # xmm0 = mem[0],zero
	ucomisd	8(%rax), %xmm0
	ja	.LBB0_11
# BB#9:
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	ucomisd	8(%rcx), %xmm0
	jne	.LBB0_13
	jp	.LBB0_13
# BB#10:
	movl	16(%rax), %edx
	cmpl	16(%rcx), %edx
	jle	.LBB0_13
.LBB0_11:
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
.LBB0_12:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rsp)
	movups	(%rcx), %xmm0
	movups	%xmm0, (%rsp)
	callq	CHfind
.LBB0_13:                               # %.thread19
	addq	$24, %rsp
	retq
.Lfunc_end0:
	.size	CHfind, .Lfunc_end0-CHfind
	.cfi_endproc

	.globl	CHrotate
	.p2align	4, 0x90
	.type	CHrotate,@function
CHrotate:                               # @CHrotate
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rcx
	cmpq	%rdi, 40(%rcx)
	je	.LBB1_1
# BB#2:
	leaq	40(%rdi), %rax
	movq	40(%rdi), %rdx
	movq	%rdx, 48(%rcx)
	testq	%rdx, %rdx
	jne	.LBB1_4
	jmp	.LBB1_5
.LBB1_1:
	leaq	48(%rdi), %rax
	movq	48(%rdi), %rdx
	movq	%rdx, 40(%rcx)
	testq	%rdx, %rdx
	je	.LBB1_5
.LBB1_4:
	movq	%rcx, 32(%rdx)
.LBB1_5:
	movq	32(%rdi), %rcx
	movq	%rcx, (%rax)
	movq	32(%rcx), %rax
	movq	%rdi, 32(%rcx)
	testq	%rax, %rax
	je	.LBB1_7
# BB#6:
	leaq	40(%rax), %rcx
	movq	40(%rax), %rdx
	leaq	48(%rax), %rsi
	cmpq	32(%rdi), %rdx
	cmoveq	%rcx, %rsi
	movq	%rdi, (%rsi)
.LBB1_7:
	movq	%rax, 32(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	CHrotate, .Lfunc_end1-CHrotate
	.cfi_endproc

	.globl	CHsplay
	.p2align	4, 0x90
	.type	CHsplay,@function
CHsplay:                                # @CHsplay
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	(%r14), %rdi
	movq	64(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	callq	CHfind
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB2_44
# BB#1:                                 # %.lr.ph
	leaq	48(%rax), %r8
	leaq	40(%rax), %r9
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_24:                               # %.backedge
                                        #   in Loop: Header=BB2_2 Depth=1
	leaq	40(%rsi), %rcx
	movq	40(%rsi), %rdx
	leaq	48(%rsi), %rdi
	cmpq	32(%rax), %rdx
	cmoveq	%rcx, %rdi
	movq	%rax, (%rdi)
	movq	%rsi, 32(%rax)
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	32(%rsi), %rdi
	movq	40(%rsi), %rdx
	leaq	40(%rsi), %rbx
	testq	%rdi, %rdi
	je	.LBB2_3
# BB#9:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpq	%rax, %rdx
	jne	.LBB2_25
# BB#10:                                #   in Loop: Header=BB2_2 Depth=1
	cmpq	%rsi, 40(%rdi)
	je	.LBB2_11
.LBB2_25:                               #   in Loop: Header=BB2_2 Depth=1
	leaq	48(%rsi), %r10
	movq	48(%rsi), %rcx
	cmpq	%rax, %rcx
	jne	.LBB2_34
# BB#26:                                #   in Loop: Header=BB2_2 Depth=1
	cmpq	%rsi, 48(%rdi)
	je	.LBB2_27
.LBB2_34:                               #   in Loop: Header=BB2_2 Depth=1
	cmpq	%rax, %rdx
	je	.LBB2_35
# BB#37:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%r9), %rdx
	movq	%rdx, (%r10)
	testq	%rdx, %rdx
	movq	%r9, %rcx
	je	.LBB2_39
# BB#38:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rsi, 32(%rdx)
	movq	%r9, %rcx
	jmp	.LBB2_39
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_2 Depth=1
	cmpq	%rax, %rdx
	je	.LBB2_4
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%r9), %rdx
	movq	%rdx, 48(%rsi)
	testq	%rdx, %rdx
	movq	%r9, %rcx
	je	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	%rsi, 32(%rdx)
	movq	%r9, %rcx
	jmp	.LBB2_8
.LBB2_35:                               #   in Loop: Header=BB2_2 Depth=1
	movq	(%r8), %rdx
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	movq	%r8, %rcx
	je	.LBB2_39
# BB#36:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rsi, 32(%rdx)
	movq	%r8, %rcx
.LBB2_39:                               #   in Loop: Header=BB2_2 Depth=1
	movq	32(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	32(%rdx), %rcx
	movq	%rax, 32(%rdx)
	leaq	40(%rcx), %rdx
	testq	%rcx, %rcx
	je	.LBB2_41
# BB#40:                                #   in Loop: Header=BB2_2 Depth=1
	movq	40(%rcx), %rsi
	leaq	48(%rcx), %rdi
	cmpq	32(%rax), %rsi
	cmoveq	%rdx, %rdi
	movq	%rax, (%rdi)
.LBB2_41:                               # %CHrotate.exit27
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rcx, 32(%rax)
	cmpq	%rax, 40(%rcx)
	jne	.LBB2_20
# BB#42:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%r8), %rsi
	movq	%rsi, (%rdx)
	jmp	.LBB2_18
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%r8), %rdx
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	movq	%r8, %rcx
	je	.LBB2_8
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	%rsi, 32(%rdx)
	movq	%r8, %rcx
.LBB2_8:                                #   in Loop: Header=BB2_2 Depth=1
	movq	32(%rax), %rdx
	movq	%rdx, (%rcx)
	movq	32(%rdx), %rsi
	movq	%rax, 32(%rdx)
	testq	%rsi, %rsi
	jne	.LBB2_24
	jmp	.LBB2_43
.LBB2_11:                               #   in Loop: Header=BB2_2 Depth=1
	movq	48(%rsi), %rcx
	movq	%rcx, 40(%rdi)
	testq	%rcx, %rcx
	je	.LBB2_13
# BB#12:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rdi, 32(%rcx)
	movq	32(%rsi), %rdi
.LBB2_13:                               # %._crit_edge44
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rdi, 48(%rsi)
	movq	32(%rdi), %rcx
	movq	%rsi, 32(%rdi)
	testq	%rcx, %rcx
	jne	.LBB2_15
	jmp	.LBB2_16
.LBB2_27:                               #   in Loop: Header=BB2_2 Depth=1
	cmpq	%rsi, 40(%rdi)
	je	.LBB2_28
# BB#30:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rdx, 48(%rdi)
	testq	%rdx, %rdx
	je	.LBB2_32
# BB#31:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rdi, 32(%rdx)
.LBB2_32:                               #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, %r10
	jmp	.LBB2_33
.LBB2_28:                               #   in Loop: Header=BB2_2 Depth=1
	movq	%rcx, 40(%rdi)
	testq	%rcx, %rcx
	je	.LBB2_33
# BB#29:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rdi, 32(%rax)
.LBB2_33:                               #   in Loop: Header=BB2_2 Depth=1
	movq	32(%rsi), %rdx
	movq	%rdx, (%r10)
	movq	32(%rdx), %rcx
	movq	%rsi, 32(%rdx)
	testq	%rcx, %rcx
	je	.LBB2_16
.LBB2_15:                               #   in Loop: Header=BB2_2 Depth=1
	leaq	40(%rcx), %rdx
	movq	40(%rcx), %rdi
	leaq	48(%rcx), %rbx
	cmpq	32(%rsi), %rdi
	cmoveq	%rdx, %rbx
	movq	%rsi, (%rbx)
.LBB2_16:                               # %CHrotate.exit43
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rcx, 32(%rsi)
	movq	32(%rax), %rcx
	cmpq	%rax, 40(%rcx)
	je	.LBB2_17
.LBB2_20:                               #   in Loop: Header=BB2_2 Depth=1
	movq	(%r9), %rsi
	movq	%rsi, 48(%rcx)
	testq	%rsi, %rsi
	movq	%r9, %rdx
	je	.LBB2_22
# BB#21:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rcx, 32(%rsi)
	movq	%r9, %rdx
	jmp	.LBB2_22
.LBB2_17:                               #   in Loop: Header=BB2_2 Depth=1
	movq	(%r8), %rsi
	movq	%rsi, 40(%rcx)
.LBB2_18:                               #   in Loop: Header=BB2_2 Depth=1
	testq	%rsi, %rsi
	movq	%r8, %rdx
	je	.LBB2_22
# BB#19:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rcx, 32(%rsi)
	movq	%r8, %rdx
.LBB2_22:                               #   in Loop: Header=BB2_2 Depth=1
	movq	32(%rax), %rcx
	movq	%rcx, (%rdx)
	movq	32(%rcx), %rsi
	movq	%rax, 32(%rcx)
	testq	%rsi, %rsi
	jne	.LBB2_24
.LBB2_43:                               # %._crit_edge.loopexit
	movq	$0, 32(%rax)
.LBB2_44:                               # %._crit_edge
	movq	%rax, (%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	CHsplay, .Lfunc_end2-CHsplay
	.cfi_endproc

	.globl	CHtraverse
	.p2align	4, 0x90
	.type	CHtraverse,@function
CHtraverse:                             # @CHtraverse
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_3
	.p2align	4, 0x90
.LBB3_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdi
	callq	CHtraverse
	movq	24(%rbx), %rax
	movl	4(%rax), %esi
	movl	8(%rax), %edx
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movl	16(%rbx), %ecx
	movl	$.L.str, %edi
	movb	$2, %al
	callq	printf
	movq	48(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_1
.LBB3_3:                                # %tailrecurse._crit_edge
	popq	%rbx
	retq
.Lfunc_end3:
	.size	CHtraverse, .Lfunc_end3-CHtraverse
	.cfi_endproc

	.globl	CHfree_tree
	.p2align	4, 0x90
	.type	CHfree_tree,@function
CHfree_tree:                            # @CHfree_tree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB4_1
# BB#2:
	movq	40(%rbx), %rdi
	callq	CHfree_tree
	movq	48(%rbx), %rdi
	callq	CHfree_tree
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB4_1:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	CHfree_tree, .Lfunc_end4-CHfree_tree
	.cfi_endproc

	.globl	CHcreate_node
	.p2align	4, 0x90
	.type	CHcreate_node,@function
CHcreate_node:                          # @CHcreate_node
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_2
# BB#1:
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 32(%r14)
	movq	$0, 48(%r14)
	movq	%rbx, %rdi
	callq	before
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	next
	movq	4(%r15), %rdi
	movq	4(%rbx), %rsi
	movq	4(%rax), %rdx
	callq	centre
	movq	4(%rbx), %rdi
	callq	radius2
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	before
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	next
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	angle
	movl	(%rbx), %eax
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, (%r14)
	movsd	%xmm0, 8(%r14)
	movl	%eax, 16(%r14)
	movq	%rbx, 24(%r14)
	movq	%r14, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB5_2:
	movl	$.Lstr, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.Lfunc_end5:
	.size	CHcreate_node, .Lfunc_end5-CHcreate_node
	.cfi_endproc

	.globl	CHinit
	.p2align	4, 0x90
	.type	CHinit,@function
CHinit:                                 # @CHinit
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	CHinit, .Lfunc_end6-CHinit
	.cfi_endproc

	.globl	CHinsert
	.p2align	4, 0x90
	.type	CHinsert,@function
CHinsert:                               # @CHinsert
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	%rsi, %rdi
	callq	CHcreate_node
	movq	%rax, %rbx
	cmpq	$0, (%r14)
	je	.LBB7_13
# BB#1:
	movq	16(%rbx), %rax
	movq	%rax, 16(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	CHsplay
	movq	(%r14), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	ucomisd	(%rbx), %xmm0
	ja	.LBB7_6
# BB#2:
	jne	.LBB7_9
	jp	.LBB7_9
# BB#3:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	ucomisd	8(%rax), %xmm0
	ja	.LBB7_6
# BB#4:
	movsd	8(%rax), %xmm0          # xmm0 = mem[0],zero
	ucomisd	8(%rbx), %xmm0
	jne	.LBB7_9
	jp	.LBB7_9
# BB#5:
	movl	16(%rax), %ecx
	cmpl	16(%rbx), %ecx
	jle	.LBB7_9
.LBB7_6:
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rbx)
	testq	%rcx, %rcx
	je	.LBB7_8
# BB#7:
	movq	%rbx, 32(%rcx)
	movq	(%r14), %rax
.LBB7_8:                                # %._crit_edge41
	movq	%rax, 48(%rbx)
	addq	$40, %rax
	jmp	.LBB7_12
.LBB7_9:                                # %.thread45
	movq	48(%rax), %rcx
	movq	%rcx, 48(%rbx)
	testq	%rcx, %rcx
	je	.LBB7_11
# BB#10:
	movq	%rbx, 32(%rcx)
	movq	(%r14), %rax
.LBB7_11:                               # %._crit_edge
	movq	%rax, 40(%rbx)
	addq	$48, %rax
.LBB7_12:
	movq	$0, (%rax)
	movq	(%r14), %rax
	movq	%rbx, 32(%rax)
.LBB7_13:
	movq	%rbx, (%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	CHinsert, .Lfunc_end7-CHinsert
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.quad	5183643170566569984     # double 3.4028234663852886E+38
	.quad	4652007308841189376     # double 1000
	.text
	.globl	CHdelete_max
	.p2align	4, 0x90
	.type	CHdelete_max,@function
CHdelete_max:                           # @CHdelete_max
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 80
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movaps	.LCPI8_0(%rip), %xmm0   # xmm0 = [3.402823e+38,1.000000e+03]
	movaps	%xmm0, 32(%rsp)
	movl	$1000, 48(%rsp)         # imm = 0x3E8
	cmpq	$0, (%rbx)
	je	.LBB8_4
# BB#1:
	movq	48(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	32(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	movq	%rbx, %rdi
	callq	CHsplay
	movq	(%rbx), %rdi
	movq	24(%rdi), %r14
	movq	40(%rdi), %rax
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB8_3
# BB#2:
	movq	$0, 32(%rax)
.LBB8_3:
	callq	free
	jmp	.LBB8_5
.LBB8_4:
	movl	$.Lstr.1, %edi
	callq	puts
	xorl	%r14d, %r14d
.LBB8_5:
	movq	%r14, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	CHdelete_max, .Lfunc_end8-CHdelete_max
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.quad	5183643170566569984     # double 3.4028234663852886E+38
	.quad	4652007308841189376     # double 1000
.LCPI9_1:
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
	.text
	.globl	CHdelete
	.p2align	4, 0x90
	.type	CHdelete,@function
CHdelete:                               # @CHdelete
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 32
	subq	$80, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 112
.Lcfi31:
	.cfi_offset %rbx, -32
.Lcfi32:
	.cfi_offset %r14, -24
.Lcfi33:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	cmpq	$0, (%rbx)
	je	.LBB9_12
# BB#1:
	leaq	112(%rsp), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movups	(%rax), %xmm0
	movups	%xmm0, (%rsp)
	movq	%rbx, %rdi
	callq	CHsplay
	movq	(%rbx), %r14
	movq	40(%r14), %rax
	movq	%rax, 40(%rsp)
	movq	48(%r14), %r15
	movq	%r15, 32(%rsp)
	testq	%rax, %rax
	jne	.LBB9_4
# BB#2:
	testq	%r15, %r15
	jne	.LBB9_4
# BB#3:
	movq	$0, (%rbx)
	jmp	.LBB9_11
.LBB9_12:
	movl	$.Lstr.2, %edi
	callq	puts
	jmp	.LBB9_13
.LBB9_4:
	testq	%rax, %rax
	jne	.LBB9_7
# BB#5:
	testq	%r15, %r15
	je	.LBB9_7
# BB#6:
	movq	%r15, (%rbx)
	movq	$0, 32(%r15)
	jmp	.LBB9_11
.LBB9_7:
	testq	%rax, %rax
	je	.LBB9_10
# BB#8:
	testq	%r15, %r15
	jne	.LBB9_10
# BB#9:
	movq	%rax, (%rbx)
	movq	$0, 32(%rax)
	jmp	.LBB9_11
.LBB9_10:
	movaps	.LCPI9_0(%rip), %xmm0   # xmm0 = [3.402823e+38,1.000000e+03]
	movaps	%xmm0, 48(%rsp)
	movl	$1000, 64(%rsp)         # imm = 0x3E8
	movq	$0, 32(%rax)
	movq	64(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	40(%rsp), %rdi
	callq	CHsplay
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [-1.000000e+00,-1.000000e+00]
	movaps	%xmm0, 48(%rsp)
	movl	$-1, 64(%rsp)
	movq	$0, 32(%r15)
	movq	64(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	32(%rsp), %rdi
	callq	CHsplay
	movq	32(%rsp), %rax
	movq	40(%rsp), %rcx
	movq	%rax, 48(%rcx)
	movq	%rcx, 32(%rax)
	movq	%rcx, (%rbx)
.LBB9_11:
	movq	%r14, %rdi
	callq	free
.LBB9_13:
	addq	$80, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	CHdelete, .Lfunc_end9-CHdelete
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"(%d,%d)  key: (%f,%f,%d)\n"
	.size	.L.str, 26

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Can't create node"
	.size	.Lstr, 18

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"No elements in tree! [CHdelete_max]"
	.size	.Lstr.1, 36

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"No elements in tree! [CHdelete]"
	.size	.Lstr.2, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
