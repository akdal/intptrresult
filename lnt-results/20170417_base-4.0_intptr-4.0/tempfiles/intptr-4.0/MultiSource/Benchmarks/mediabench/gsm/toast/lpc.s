	.text
	.file	"lpc.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
	.short	32768                   # 0x8000
.LCPI0_1:
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
	.short	32767                   # 0x7fff
.LCPI0_2:
	.quad	16384                   # 0x4000
	.quad	16384                   # 0x4000
	.text
	.globl	Gsm_LPC_Analysis
	.p2align	4, 0x90
	.type	Gsm_LPC_Analysis,@function
Gsm_LPC_Analysis:                       # @Gsm_LPC_Analysis
	.cfi_startproc
# BB#0:                                 # %vector.body
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	pxor	%xmm8, %xmm8
	movdqu	(%rbp), %xmm7
	movdqu	16(%rbp), %xmm13
	movdqu	32(%rbp), %xmm10
	movdqu	48(%rbp), %xmm9
	pxor	%xmm14, %xmm14
	pcmpgtw	%xmm7, %xmm14
	pxor	%xmm4, %xmm4
	pcmpgtw	%xmm13, %xmm4
	movdqa	.LCPI0_0(%rip), %xmm12  # xmm12 = [32768,32768,32768,32768,32768,32768,32768,32768]
	movdqa	%xmm7, %xmm6
	pcmpeqw	%xmm12, %xmm6
	movdqa	%xmm13, %xmm5
	pcmpeqw	%xmm12, %xmm5
	pxor	%xmm2, %xmm2
	psubw	%xmm7, %xmm2
	pxor	%xmm0, %xmm0
	psubw	%xmm13, %xmm0
	movdqa	.LCPI0_1(%rip), %xmm11  # xmm11 = [32767,32767,32767,32767,32767,32767,32767,32767]
	movdqa	%xmm11, %xmm1
	pand	%xmm6, %xmm1
	pandn	%xmm2, %xmm6
	por	%xmm1, %xmm6
	movdqa	%xmm11, %xmm1
	pand	%xmm5, %xmm1
	pandn	%xmm0, %xmm5
	por	%xmm1, %xmm5
	pand	%xmm14, %xmm6
	pandn	%xmm7, %xmm14
	por	%xmm6, %xmm14
	pand	%xmm4, %xmm5
	pandn	%xmm13, %xmm4
	por	%xmm5, %xmm4
	pmaxsw	%xmm8, %xmm14
	pmaxsw	%xmm8, %xmm4
	pxor	%xmm5, %xmm5
	pcmpgtw	%xmm10, %xmm5
	pxor	%xmm6, %xmm6
	pcmpgtw	%xmm9, %xmm6
	movdqa	%xmm10, %xmm0
	pcmpeqw	%xmm12, %xmm0
	movdqa	%xmm9, %xmm1
	pcmpeqw	%xmm12, %xmm1
	pxor	%xmm2, %xmm2
	psubw	%xmm10, %xmm2
	pxor	%xmm7, %xmm7
	psubw	%xmm9, %xmm7
	movdqa	%xmm11, %xmm3
	pand	%xmm0, %xmm3
	pandn	%xmm2, %xmm0
	por	%xmm3, %xmm0
	movdqa	%xmm11, %xmm2
	pand	%xmm1, %xmm2
	pandn	%xmm7, %xmm1
	por	%xmm2, %xmm1
	pand	%xmm5, %xmm0
	pandn	%xmm10, %xmm5
	por	%xmm0, %xmm5
	pand	%xmm6, %xmm1
	pandn	%xmm9, %xmm6
	por	%xmm1, %xmm6
	pmaxsw	%xmm14, %xmm5
	pmaxsw	%xmm4, %xmm6
	movdqu	64(%rbp), %xmm10
	movdqu	80(%rbp), %xmm9
	pxor	%xmm13, %xmm13
	pcmpgtw	%xmm10, %xmm13
	pxor	%xmm4, %xmm4
	pcmpgtw	%xmm9, %xmm4
	movdqa	%xmm10, %xmm2
	pcmpeqw	%xmm12, %xmm2
	movdqa	%xmm9, %xmm7
	pcmpeqw	%xmm12, %xmm7
	pxor	%xmm1, %xmm1
	psubw	%xmm10, %xmm1
	pxor	%xmm0, %xmm0
	psubw	%xmm9, %xmm0
	movdqa	%xmm11, %xmm3
	pand	%xmm2, %xmm3
	pandn	%xmm1, %xmm2
	por	%xmm3, %xmm2
	movdqa	%xmm11, %xmm1
	pand	%xmm7, %xmm1
	pandn	%xmm0, %xmm7
	por	%xmm1, %xmm7
	pand	%xmm13, %xmm2
	pandn	%xmm10, %xmm13
	por	%xmm2, %xmm13
	pand	%xmm4, %xmm7
	pandn	%xmm9, %xmm4
	por	%xmm7, %xmm4
	pmaxsw	%xmm5, %xmm13
	pmaxsw	%xmm6, %xmm4
	movdqu	96(%rbp), %xmm10
	movdqu	112(%rbp), %xmm9
	pxor	%xmm5, %xmm5
	pcmpgtw	%xmm10, %xmm5
	pxor	%xmm6, %xmm6
	pcmpgtw	%xmm9, %xmm6
	movdqa	%xmm10, %xmm2
	pcmpeqw	%xmm12, %xmm2
	movdqa	%xmm9, %xmm3
	pcmpeqw	%xmm12, %xmm3
	pxor	%xmm7, %xmm7
	psubw	%xmm10, %xmm7
	pxor	%xmm1, %xmm1
	psubw	%xmm9, %xmm1
	movdqa	%xmm11, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm7, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm11, %xmm0
	pand	%xmm3, %xmm0
	pandn	%xmm1, %xmm3
	por	%xmm0, %xmm3
	pand	%xmm5, %xmm2
	pandn	%xmm10, %xmm5
	por	%xmm2, %xmm5
	pand	%xmm6, %xmm3
	pandn	%xmm9, %xmm6
	por	%xmm3, %xmm6
	pmaxsw	%xmm13, %xmm5
	pmaxsw	%xmm4, %xmm6
	movdqu	128(%rbp), %xmm10
	movdqu	144(%rbp), %xmm9
	pxor	%xmm13, %xmm13
	pcmpgtw	%xmm10, %xmm13
	pxor	%xmm4, %xmm4
	pcmpgtw	%xmm9, %xmm4
	movdqa	%xmm10, %xmm2
	pcmpeqw	%xmm12, %xmm2
	movdqa	%xmm9, %xmm7
	pcmpeqw	%xmm12, %xmm7
	pxor	%xmm1, %xmm1
	psubw	%xmm10, %xmm1
	pxor	%xmm0, %xmm0
	psubw	%xmm9, %xmm0
	movdqa	%xmm11, %xmm3
	pand	%xmm2, %xmm3
	pandn	%xmm1, %xmm2
	por	%xmm3, %xmm2
	movdqa	%xmm11, %xmm1
	pand	%xmm7, %xmm1
	pandn	%xmm0, %xmm7
	por	%xmm1, %xmm7
	pand	%xmm13, %xmm2
	pandn	%xmm10, %xmm13
	por	%xmm2, %xmm13
	pand	%xmm4, %xmm7
	pandn	%xmm9, %xmm4
	por	%xmm7, %xmm4
	pmaxsw	%xmm5, %xmm13
	pmaxsw	%xmm6, %xmm4
	movdqu	160(%rbp), %xmm10
	movdqu	176(%rbp), %xmm9
	pxor	%xmm5, %xmm5
	pcmpgtw	%xmm10, %xmm5
	pxor	%xmm6, %xmm6
	pcmpgtw	%xmm9, %xmm6
	movdqa	%xmm10, %xmm2
	pcmpeqw	%xmm12, %xmm2
	movdqa	%xmm9, %xmm3
	pcmpeqw	%xmm12, %xmm3
	pxor	%xmm7, %xmm7
	psubw	%xmm10, %xmm7
	pxor	%xmm1, %xmm1
	psubw	%xmm9, %xmm1
	movdqa	%xmm11, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm7, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm11, %xmm0
	pand	%xmm3, %xmm0
	pandn	%xmm1, %xmm3
	por	%xmm0, %xmm3
	pand	%xmm5, %xmm2
	pandn	%xmm10, %xmm5
	por	%xmm2, %xmm5
	pand	%xmm6, %xmm3
	pandn	%xmm9, %xmm6
	por	%xmm3, %xmm6
	pmaxsw	%xmm13, %xmm5
	pmaxsw	%xmm4, %xmm6
	movdqu	192(%rbp), %xmm10
	movdqu	208(%rbp), %xmm9
	pxor	%xmm13, %xmm13
	pcmpgtw	%xmm10, %xmm13
	pxor	%xmm4, %xmm4
	pcmpgtw	%xmm9, %xmm4
	movdqa	%xmm10, %xmm2
	pcmpeqw	%xmm12, %xmm2
	movdqa	%xmm9, %xmm7
	pcmpeqw	%xmm12, %xmm7
	pxor	%xmm1, %xmm1
	psubw	%xmm10, %xmm1
	pxor	%xmm0, %xmm0
	psubw	%xmm9, %xmm0
	movdqa	%xmm11, %xmm3
	pand	%xmm2, %xmm3
	pandn	%xmm1, %xmm2
	por	%xmm3, %xmm2
	movdqa	%xmm11, %xmm1
	pand	%xmm7, %xmm1
	pandn	%xmm0, %xmm7
	por	%xmm1, %xmm7
	pand	%xmm13, %xmm2
	pandn	%xmm10, %xmm13
	por	%xmm2, %xmm13
	pand	%xmm4, %xmm7
	pandn	%xmm9, %xmm4
	por	%xmm7, %xmm4
	pmaxsw	%xmm5, %xmm13
	pmaxsw	%xmm6, %xmm4
	movdqu	224(%rbp), %xmm10
	movdqu	240(%rbp), %xmm9
	pxor	%xmm5, %xmm5
	pcmpgtw	%xmm10, %xmm5
	pxor	%xmm6, %xmm6
	pcmpgtw	%xmm9, %xmm6
	movdqa	%xmm10, %xmm2
	pcmpeqw	%xmm12, %xmm2
	movdqa	%xmm9, %xmm3
	pcmpeqw	%xmm12, %xmm3
	pxor	%xmm7, %xmm7
	psubw	%xmm10, %xmm7
	pxor	%xmm1, %xmm1
	psubw	%xmm9, %xmm1
	movdqa	%xmm11, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm7, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm11, %xmm0
	pand	%xmm3, %xmm0
	pandn	%xmm1, %xmm3
	por	%xmm0, %xmm3
	pand	%xmm5, %xmm2
	pandn	%xmm10, %xmm5
	por	%xmm2, %xmm5
	pand	%xmm6, %xmm3
	pandn	%xmm9, %xmm6
	por	%xmm3, %xmm6
	pmaxsw	%xmm13, %xmm5
	pmaxsw	%xmm4, %xmm6
	movdqu	256(%rbp), %xmm10
	movdqu	272(%rbp), %xmm9
	pxor	%xmm13, %xmm13
	pcmpgtw	%xmm10, %xmm13
	pxor	%xmm4, %xmm4
	pcmpgtw	%xmm9, %xmm4
	movdqa	%xmm10, %xmm2
	pcmpeqw	%xmm12, %xmm2
	movdqa	%xmm9, %xmm7
	pcmpeqw	%xmm12, %xmm7
	pxor	%xmm1, %xmm1
	psubw	%xmm10, %xmm1
	pxor	%xmm0, %xmm0
	psubw	%xmm9, %xmm0
	movdqa	%xmm11, %xmm3
	pand	%xmm2, %xmm3
	pandn	%xmm1, %xmm2
	por	%xmm3, %xmm2
	movdqa	%xmm11, %xmm1
	pand	%xmm7, %xmm1
	pandn	%xmm0, %xmm7
	por	%xmm1, %xmm7
	pand	%xmm13, %xmm2
	pandn	%xmm10, %xmm13
	por	%xmm2, %xmm13
	pand	%xmm4, %xmm7
	pandn	%xmm9, %xmm4
	por	%xmm7, %xmm4
	pmaxsw	%xmm5, %xmm13
	pmaxsw	%xmm6, %xmm4
	movdqu	288(%rbp), %xmm0
	movdqu	304(%rbp), %xmm6
	pxor	%xmm5, %xmm5
	pcmpgtw	%xmm0, %xmm5
	pxor	%xmm1, %xmm1
	pcmpgtw	%xmm6, %xmm1
	movdqa	%xmm0, %xmm2
	pcmpeqw	%xmm12, %xmm2
	pcmpeqw	%xmm6, %xmm12
	pxor	%xmm3, %xmm3
	psubw	%xmm0, %xmm3
	psubw	%xmm6, %xmm8
	movdqa	%xmm11, %xmm7
	pand	%xmm2, %xmm7
	pandn	%xmm3, %xmm2
	por	%xmm7, %xmm2
	pand	%xmm12, %xmm11
	pandn	%xmm8, %xmm12
	por	%xmm11, %xmm12
	pand	%xmm5, %xmm2
	pandn	%xmm0, %xmm5
	por	%xmm2, %xmm5
	pand	%xmm1, %xmm12
	pandn	%xmm6, %xmm1
	por	%xmm12, %xmm1
	pmaxsw	%xmm13, %xmm5
	pmaxsw	%xmm4, %xmm1
	pmaxsw	%xmm5, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	pmaxsw	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	pmaxsw	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	movd	%xmm1, %ecx
	pextrw	$1, %xmm1, %eax
	psrld	$16, %xmm1
	pcmpgtw	%xmm1, %xmm0
	movdqa	%xmm0, 160(%rsp)
	testb	$1, 160(%rsp)
	cmovnew	%cx, %ax
	testw	%ax, %ax
	je	.LBB0_1
# BB#2:
	movswq	%ax, %rdi
	shlq	$16, %rdi
	callq	gsm_norm
                                        # kill: %AX<def> %AX<kill> %EAX<def>
	movl	$4, %ecx
	subl	%eax, %ecx
	movswl	%cx, %ecx
	testw	%cx, %cx
	jle	.LBB0_3
# BB#4:
	movb	$1, %al
	movl	%eax, 12(%rsp)          # 4-byte Spill
	leal	-1(%rcx), %eax
	cmpl	$3, %eax
	ja	.LBB0_5
# BB#6:
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_16:                               # %vector.body172.preheader
	movl	$2, %eax
	movdqa	.LCPI0_2(%rip), %xmm0   # xmm0 = [16384,16384]
	.p2align	4, 0x90
.LBB0_17:                               # %vector.body172
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rbp,%rax,2), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movd	(%rbp,%rax,2), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	movdqa	%xmm1, %xmm3
	psrad	$31, %xmm3
	psrad	$16, %xmm1
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	movdqa	%xmm2, %xmm3
	psrad	$31, %xmm3
	psrad	$16, %xmm2
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	psllq	$14, %xmm1
	psllq	$14, %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	psrlq	$15, %xmm1
	psrlq	$15, %xmm2
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, -4(%rbp,%rax,2)
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, (%rbp,%rax,2)
	addq	$4, %rax
	cmpq	$162, %rax
	jne	.LBB0_17
# BB#18:
	movl	$1, %eax
	jmp	.LBB0_19
.LBB0_1:
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
.LBB0_19:                               # %.thread.i
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB0_20
.LBB0_3:
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_20
.LBB0_5:
	movq	%rcx, (%rsp)            # 8-byte Spill
.LBB0_20:                               # %.thread.i
	movswq	(%rbp), %r8
	movq	%r8, %rax
	imulq	%rax, %rax
	movswq	2(%rbp), %r12
	movq	%r12, %rdx
	imulq	%rdx, %rdx
	addq	%rax, %rdx
	movswq	4(%rbp), %r15
	movq	%r15, %rax
	imulq	%rax, %rax
	addq	%rdx, %rax
	movswq	6(%rbp), %rbx
	movq	%rbx, %r14
	imulq	%r14, %r14
	addq	%rax, %r14
	movswq	8(%rbp), %rdx
	movq	%rdx, %r11
	movswq	10(%rbp), %r9
	movswq	12(%rbp), %rdi
	movswq	14(%rbp), %rcx
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	leaq	(%r9,%rbx), %rax
	imulq	%rdx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%rdi,%r15), %r10
	imulq	%rdx, %r10
	leaq	(%rcx,%r12), %rbp
	imulq	%rdx, %rbp
	imulq	%rdx, %rdx
	addq	%r14, %rdx
	movq	%r9, %rax
	movq	%r9, %r14
	movq	%r9, %rsi
	imulq	%rsi, %rsi
	addq	%rdx, %rsi
	leaq	(%r15,%r8), %r13
	imulq	%r12, %r13
	imulq	%r8, %r11
	imulq	%r12, %rax
	addq	%r11, %rax
	imulq	%r8, %r14
	movq	%rdi, %rdx
	imulq	%rdx, %rdx
	addq	%rsi, %rdx
	movq	%rdi, %rsi
	imulq	%r15, %rsi
	addq	%rax, %rsi
	movq	%rdi, %r11
	imulq	%r12, %r11
	addq	%r14, %r11
	movq	%rcx, %rax
	imulq	%rax, %rax
	addq	%rdx, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rbx, %rax
	imulq	%r15, %rax
	addq	%r13, %rax
	leaq	(%rcx,%r9), %rdx
	addq	24(%rsp), %rax          # 8-byte Folded Reload
	movq	%r15, %r14
	imulq	%r8, %r14
	imulq	%rdi, %rdx
	addq	%rdx, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rdi, %rdx
	movq	%rbx, %r13
	imulq	%r12, %r13
	addq	%r14, %r13
	addq	%r10, %r13
	leaq	(%rcx,%rbx), %rax
	imulq	%r9, %rax
	imulq	%r15, %r9
	addq	%rax, %r13
	movq	%rdx, %rax
	addq	%r8, %rdx
	imulq	%rbx, %rdx
	addq	%r9, %rdx
	imulq	%r8, %rax
	addq	%rbp, %rdx
	movq	%r15, %rbp
	imulq	%rcx, %rbx
	addq	%rsi, %rbx
	imulq	%rcx, %rbp
	addq	%r11, %rbp
	movq	%r12, 136(%rsp)         # 8-byte Spill
	movq	%r12, %r9
	imulq	%rcx, %r9
	addq	%rax, %r9
	movq	%rcx, %r12
	movq	%rcx, %r10
	movq	%r8, 128(%rsp)          # 8-byte Spill
	imulq	%r8, %r10
	xorl	%r11d, %r11d
	movl	$8, %r14d
	.p2align	4, 0x90
.LBB0_21:                               # =>This Inner Loop Header: Depth=1
	movq	%r11, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	movswq	(%rax,%r14,2), %rdx
	movswq	%r12w, %r11
	imulq	%rdx, %r11
	movswq	-4(%rax,%r14,2), %rbx
	imulq	%rdx, %rbx
	movswq	-6(%rax,%r14,2), %rbp
	imulq	%rdx, %rbp
	movswq	-8(%rax,%r14,2), %rdi
	imulq	%rdx, %rdi
	movswq	-10(%rax,%r14,2), %rsi
	imulq	%rdx, %rsi
	movq	%r9, %rcx
	movswq	-12(%rax,%r14,2), %r9
	imulq	%rdx, %r9
	movq	%r10, %r8
	movswq	-14(%rax,%r14,2), %r10
	imulq	%rdx, %r10
	movswq	-16(%rax,%r14,2), %r15
	imulq	%rdx, %r15
	movw	%dx, %r12w
	imulq	%rdx, %rdx
	addq	%rdx, 64(%rsp)          # 8-byte Folded Spill
	movq	72(%rsp), %rdx          # 8-byte Reload
	addq	%r11, 112(%rsp)         # 8-byte Folded Spill
	movq	24(%rsp), %r11          # 8-byte Reload
	addq	%rbx, %r13
	addq	%rbp, %rdx
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	152(%rsp), %rbx         # 8-byte Reload
	addq	%rdi, %rbx
	addq	%rsi, %rbp
	addq	%r9, %rcx
	movq	%rcx, %r9
	addq	%r10, %r8
	movq	%r8, %r10
	addq	%r15, %r11
	incq	%r14
	cmpl	$160, %r14d
	jne	.LBB0_21
# BB#22:                                # %.preheader215.i
	movq	112(%rsp), %r14         # 8-byte Reload
	movq	120(%rsp), %r12         # 8-byte Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	addq	%rax, %rax
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	movq	%rax, %rdi
	je	.LBB0_24
# BB#23:                                # %.preheader..preheader_crit_edge.i..preheader..preheader_crit_edge.i_crit_edge
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	128(%rsp), %rax         # 8-byte Reload
	shll	%cl, %eax
	movq	%rdx, %r8
	movq	%r12, %rdx
	movw	%ax, (%rdx)
	movq	136(%rsp), %rax         # 8-byte Reload
	shll	%cl, %eax
	movw	%ax, 2(%rdx)
	movq	12(%rdx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	psrad	$16, %xmm3
	movq	4(%rdx), %xmm0          # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	psrad	$16, %xmm1
	movq	28(%rdx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psrad	$16, %xmm2
	movq	20(%rdx), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	psrad	$16, %xmm4
	movd	%ecx, %xmm0
	pslld	%xmm0, %xmm1
	pslld	%xmm0, %xmm3
	pslld	%xmm0, %xmm4
	pslld	%xmm0, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm3, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm2, %xmm4
	movdqu	%xmm1, 4(%rdx)
	movdqu	%xmm4, 20(%rdx)
	movq	44(%rdx), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	psrad	$16, %xmm3
	movq	36(%rdx), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	60(%rdx), %xmm2         # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	52(%rdx), %xmm4         # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	pslld	%xmm0, %xmm1
	pslld	%xmm0, %xmm3
	pslld	%xmm0, %xmm4
	pslld	%xmm0, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm3, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm2, %xmm4
	movdqu	%xmm1, 36(%rdx)
	movdqu	%xmm4, 52(%rdx)
	movq	76(%rdx), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	psrad	$16, %xmm3
	movq	68(%rdx), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	92(%rdx), %xmm2         # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	84(%rdx), %xmm4         # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	pslld	%xmm0, %xmm1
	pslld	%xmm0, %xmm3
	pslld	%xmm0, %xmm4
	pslld	%xmm0, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm3, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm2, %xmm4
	movdqu	%xmm1, 68(%rdx)
	movdqu	%xmm4, 84(%rdx)
	movq	108(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	psrad	$16, %xmm3
	movq	100(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	124(%rdx), %xmm2        # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	116(%rdx), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	pslld	%xmm0, %xmm1
	pslld	%xmm0, %xmm3
	pslld	%xmm0, %xmm4
	pslld	%xmm0, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm3, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm2, %xmm4
	movdqu	%xmm1, 100(%rdx)
	movdqu	%xmm4, 116(%rdx)
	movq	140(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	psrad	$16, %xmm3
	movq	132(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	156(%rdx), %xmm2        # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	148(%rdx), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	pslld	%xmm0, %xmm1
	pslld	%xmm0, %xmm3
	pslld	%xmm0, %xmm4
	pslld	%xmm0, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm3, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm2, %xmm4
	movdqu	%xmm1, 132(%rdx)
	movdqu	%xmm4, 148(%rdx)
	movq	172(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	psrad	$16, %xmm3
	movq	164(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	188(%rdx), %xmm2        # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	180(%rdx), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	pslld	%xmm0, %xmm1
	pslld	%xmm0, %xmm3
	pslld	%xmm0, %xmm4
	pslld	%xmm0, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm3, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm2, %xmm4
	movdqu	%xmm1, 164(%rdx)
	movdqu	%xmm4, 180(%rdx)
	movq	204(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	psrad	$16, %xmm3
	movq	196(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	220(%rdx), %xmm2        # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	212(%rdx), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	pslld	%xmm0, %xmm1
	pslld	%xmm0, %xmm3
	pslld	%xmm0, %xmm4
	pslld	%xmm0, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm3, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm2, %xmm4
	movdqu	%xmm1, 196(%rdx)
	movdqu	%xmm4, 212(%rdx)
	movq	236(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	psrad	$16, %xmm3
	movq	228(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	252(%rdx), %xmm2        # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	244(%rdx), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	pslld	%xmm0, %xmm1
	pslld	%xmm0, %xmm3
	pslld	%xmm0, %xmm4
	pslld	%xmm0, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm3, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm2, %xmm4
	movdqu	%xmm1, 228(%rdx)
	movdqu	%xmm4, 244(%rdx)
	movq	268(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	psrad	$16, %xmm3
	movq	260(%rdx), %xmm1        # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movq	284(%rdx), %xmm2        # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	276(%rdx), %xmm4        # xmm4 = mem[0],zero
	punpcklwd	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm4
	pslld	%xmm0, %xmm1
	pslld	%xmm0, %xmm3
	pslld	%xmm0, %xmm4
	pslld	%xmm0, %xmm2
	pslld	$16, %xmm3
	psrad	$16, %xmm3
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm3, %xmm1
	pslld	$16, %xmm2
	psrad	$16, %xmm2
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm2, %xmm4
	movdqu	%xmm1, 260(%rdx)
	movdqu	%xmm4, 276(%rdx)
	movswl	292(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 292(%rdx)
	movswl	294(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 294(%rdx)
	movswl	296(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 296(%rdx)
	movswl	298(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 298(%rdx)
	movswl	300(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 300(%rdx)
	movswl	302(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 302(%rdx)
	movswl	304(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 304(%rdx)
	movswl	306(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 306(%rdx)
	movswl	308(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 308(%rdx)
	movswl	310(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 310(%rdx)
	movswl	312(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 312(%rdx)
	movswl	314(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 314(%rdx)
	movswl	316(%rdx), %eax
	shll	%cl, %eax
	movw	%ax, 316(%rdx)
	movswl	318(%rdx), %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	movw	%ax, 318(%rdx)
	movq	%r8, %rdx
.LBB0_24:                               # %Autocorrelation.exit
	testq	%rdi, %rdi
	je	.LBB0_25
# BB#26:                                # %.preheader70.preheader105.i
	addq	%r11, %r11
	movq	%r11, 24(%rsp)          # 8-byte Spill
	addq	%r10, %r10
	movq	%r10, 72(%rsp)          # 8-byte Spill
	addq	%r9, %r9
	addq	%rbp, %rbp
	addq	%rbx, %rbx
	addq	%rdx, %rdx
	addq	%r13, %r13
	addq	%r14, %r14
	movq	%rdx, %r15
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%r9, %r12
	callq	gsm_norm
	movl	%eax, %ecx
	movq	64(%rsp), %rax          # 8-byte Reload
	shlq	%cl, %rax
	shrq	$16, %rax
	movq	%rax, %rsi
	shlq	%cl, %r14
	shrq	$16, %r14
	shlq	%cl, %r13
	shrq	$16, %r13
	shlq	%cl, %r15
	shrq	$16, %r15
	shlq	%cl, %rbx
	shrq	$16, %rbx
	shlq	%cl, %rbp
	shrq	$16, %rbp
	shlq	%cl, %r12
	shrq	$16, %r12
	movq	72(%rsp), %rax          # 8-byte Reload
	shlq	%cl, %rax
	shrq	$16, %rax
	movq	%rax, %rdx
                                        # kill: %CL<def> %CL<kill> %CX<kill>
	movq	24(%rsp), %rax          # 8-byte Reload
	shlq	%cl, %rax
	shrq	$16, %rax
	movw	%r14w, 82(%rsp)
	movw	%r13w, 84(%rsp)
	movw	%r15w, 86(%rsp)
	movw	%bx, 88(%rsp)
	movw	%bp, 90(%rsp)
	movw	%r12w, 92(%rsp)
	movw	%dx, 94(%rsp)
	movw	%si, 32(%rsp)
	movw	%r14w, 34(%rsp)
	movw	%r13w, 36(%rsp)
	movw	%r15w, 38(%rsp)
	movw	%bx, 40(%rsp)
	movw	%bp, 42(%rsp)
	movw	%r12w, 44(%rsp)
	movw	%dx, 46(%rsp)
	movw	%ax, 48(%rsp)
	movl	$1, %ebp
	movl	$6, %r10d
	movabsq	$140737488355328, %r13  # imm = 0x800000000000
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_42:                               # %._crit_edge.i._crit_edge
                                        #   in Loop: Header=BB0_27 Depth=1
	addq	$2, %r12
	movw	34(%rsp), %r14w
	decq	%r10
.LBB0_27:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_39 Depth 2
	testw	%r14w, %r14w
	movw	%r14w, %ax
	jns	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_27 Depth=1
	movzwl	%r14w, %ecx
	movl	%r14d, %eax
	negl	%eax
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	cmovew	%cx, %ax
.LBB0_29:                               #   in Loop: Header=BB0_27 Depth=1
	movswq	32(%rsp), %r15
	cmpw	%ax, %r15w
	jl	.LBB0_30
# BB#36:                                #   in Loop: Header=BB0_27 Depth=1
	movswl	%ax, %edi
	movl	%r15d, %esi
	movq	%r10, %rbx
	callq	gsm_div
	movq	%rbx, %r10
                                        # kill: %AX<def> %AX<kill> %EAX<def>
	movl	%eax, %esi
	negl	%esi
	testw	%r14w, %r14w
	cmovlew	%ax, %si
	movw	%si, (%r12)
	cmpl	$8, %ebp
	je	.LBB0_31
# BB#37:                                #   in Loop: Header=BB0_27 Depth=1
	movswl	%si, %eax
	movswl	%r14w, %ecx
	imull	%eax, %ecx
	shlq	$33, %rcx
	addq	%r13, %rcx
	sarq	$48, %rcx
	leaq	(%rcx,%r15), %rax
	leaq	32768(%rcx,%r15), %rcx
	xorl	%edx, %edx
	testq	%rax, %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%ax, %dx
	movw	%dx, 32(%rsp)
	movl	%ebp, %r9d
	cmpl	$7, %ebp
	jg	.LBB0_41
# BB#38:                                # %.lr.ph78.i.preheader
                                        #   in Loop: Header=BB0_27 Depth=1
	leaq	82(%rsp), %rax
	movq	%r10, %r8
	leaq	36(%rsp), %rdx
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_40:                               # %.lr.ph78..lr.ph78_crit_edge.i
                                        #   in Loop: Header=BB0_39 Depth=2
	movw	(%r12), %si
	addq	$2, %rdx
	decq	%r8
	addq	$2, %rax
.LBB0_39:                               # %.lr.ph78.i
                                        #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rax), %edi
	movslq	%edi, %rbx
	movswl	(%rdx), %ebp
	movslq	%ebp, %rbp
	movswl	%si, %esi
	movl	%ebx, %edi
	imull	%esi, %edi
	shlq	$33, %rdi
	addq	%r13, %rdi
	sarq	$48, %rdi
	leaq	32768(%rdi,%rbp), %rsi
	addq	%rbp, %rdi
	xorl	%ecx, %ecx
	testq	%rdi, %rdi
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rsi            # imm = 0xFFFF
	cmovbew	%di, %cx
	movw	%cx, -2(%rdx)
	movswl	(%r12), %ecx
	imull	%ecx, %ebp
	shlq	$33, %rbp
	addq	%r13, %rbp
	sarq	$48, %rbp
	leaq	(%rbp,%rbx), %rcx
	leaq	32768(%rbp,%rbx), %rsi
	xorl	%edi, %edi
	testq	%rcx, %rcx
	setle	%dil
	addl	$32767, %edi            # imm = 0x7FFF
	cmpq	$65535, %rsi            # imm = 0xFFFF
	cmovbew	%cx, %di
	movw	%di, (%rax)
	testq	%r8, %r8
	jne	.LBB0_40
.LBB0_41:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_27 Depth=1
	movl	%r9d, %ebp
	incl	%ebp
	cmpl	$8, %ebp
	jle	.LBB0_42
	jmp	.LBB0_31
.LBB0_25:                               # %.preheader.preheader.i
	pxor	%xmm0, %xmm0
	movq	16(%rsp), %rax          # 8-byte Reload
	movdqu	%xmm0, (%rax)
	jmp	.LBB0_31
.LBB0_30:                               # %.lr.ph.preheader.i
	movl	$8, %eax
	subl	%ebp, %eax
	leaq	2(%rax,%rax), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
.LBB0_31:                               # %Reflection_coefficients.exit
	movq	16(%rsp), %rbx          # 8-byte Reload
	movw	(%rbx), %ax
	testw	%ax, %ax
	movw	%ax, %cx
	jns	.LBB0_34
# BB#32:
	movzwl	%ax, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	je	.LBB0_45
# BB#33:
	movl	%eax, %ecx
	negl	%ecx
.LBB0_34:
	movswl	%cx, %edx
	cmpl	$22117, %edx            # imm = 0x5665
	jg	.LBB0_43
# BB#35:
	sarw	%cx
	jmp	.LBB0_46
.LBB0_43:
	cmpl	$31129, %edx            # imm = 0x7999
	jg	.LBB0_45
# BB#44:
	addl	$-11059, %ecx           # imm = 0xD4CD
	jmp	.LBB0_46
.LBB0_45:                               # %.thread18.i
	leal	26624(,%rcx,4), %ecx
.LBB0_46:
	movl	%ecx, %edi
	negl	%edi
	testw	%ax, %ax
	cmovnsw	%cx, %di
	movw	%di, (%rbx)
	movw	2(%rbx), %ax
	testw	%ax, %ax
	movw	%ax, %cx
	jns	.LBB0_49
# BB#47:
	movzwl	%ax, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	je	.LBB0_53
# BB#48:
	movl	%eax, %ecx
	negl	%ecx
.LBB0_49:
	movswl	%cx, %edx
	cmpl	$22118, %edx            # imm = 0x5666
	jge	.LBB0_50
# BB#52:
	sarw	%cx
	jmp	.LBB0_54
.LBB0_50:
	cmpl	$31129, %edx            # imm = 0x7999
	jg	.LBB0_53
# BB#51:
	addl	$-11059, %ecx           # imm = 0xD4CD
	jmp	.LBB0_54
.LBB0_53:                               # %.thread18.1.i
	leal	26624(,%rcx,4), %ecx
.LBB0_54:
	movl	%ecx, %r10d
	negl	%r10d
	testw	%ax, %ax
	cmovnsw	%cx, %r10w
	movw	%r10w, 2(%rbx)
	movw	4(%rbx), %ax
	testw	%ax, %ax
	movw	%ax, %cx
	jns	.LBB0_57
# BB#55:
	movzwl	%ax, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	je	.LBB0_61
# BB#56:
	movl	%eax, %ecx
	negl	%ecx
.LBB0_57:
	movswl	%cx, %edx
	cmpl	$22118, %edx            # imm = 0x5666
	jge	.LBB0_58
# BB#60:
	sarw	%cx
	jmp	.LBB0_62
.LBB0_58:
	cmpl	$31129, %edx            # imm = 0x7999
	jg	.LBB0_61
# BB#59:
	addl	$-11059, %ecx           # imm = 0xD4CD
	jmp	.LBB0_62
.LBB0_61:                               # %.thread18.2.i
	leal	26624(,%rcx,4), %ecx
.LBB0_62:
	movl	%ecx, %r12d
	negl	%r12d
	testw	%ax, %ax
	cmovnsw	%cx, %r12w
	movw	%r12w, 4(%rbx)
	movw	6(%rbx), %ax
	testw	%ax, %ax
	movw	%ax, %cx
	jns	.LBB0_65
# BB#63:
	movzwl	%ax, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	movw	$32767, %cx             # imm = 0x7FFF
	je	.LBB0_69
# BB#64:
	movl	%eax, %ecx
	negl	%ecx
.LBB0_65:
	movswl	%cx, %edx
	cmpl	$22118, %edx            # imm = 0x5666
	jge	.LBB0_66
# BB#68:
	sarw	%cx
	jmp	.LBB0_70
.LBB0_66:
	cmpl	$31129, %edx            # imm = 0x7999
	jg	.LBB0_69
# BB#67:
	addl	$-11059, %ecx           # imm = 0xD4CD
	jmp	.LBB0_70
.LBB0_69:                               # %.thread18.3.i
	leal	26624(,%rcx,4), %ecx
.LBB0_70:
	movl	%ecx, %r13d
	negl	%r13d
	testw	%ax, %ax
	cmovnsw	%cx, %r13w
	movw	%r13w, 6(%rbx)
	movw	8(%rbx), %cx
	testw	%cx, %cx
	movw	%cx, %dx
	jns	.LBB0_73
# BB#71:
	movzwl	%cx, %eax
	cmpl	$32768, %eax            # imm = 0x8000
	movw	$32767, %dx             # imm = 0x7FFF
	je	.LBB0_77
# BB#72:
	movl	%ecx, %edx
	negl	%edx
.LBB0_73:
	movswl	%dx, %eax
	cmpl	$22118, %eax            # imm = 0x5666
	jge	.LBB0_74
# BB#76:
	sarw	%dx
	jmp	.LBB0_78
.LBB0_74:
	cmpl	$31129, %eax            # imm = 0x7999
	jg	.LBB0_77
# BB#75:
	addl	$-11059, %edx           # imm = 0xD4CD
	jmp	.LBB0_78
.LBB0_77:                               # %.thread18.4.i
	leal	26624(,%rdx,4), %edx
.LBB0_78:
	movl	%edx, %r9d
	negl	%r9d
	testw	%cx, %cx
	cmovnsw	%dx, %r9w
	movw	%r9w, 8(%rbx)
	movw	10(%rbx), %cx
	testw	%cx, %cx
	movw	%cx, %dx
	jns	.LBB0_81
# BB#79:
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movw	$32767, %dx             # imm = 0x7FFF
	je	.LBB0_85
# BB#80:
	movl	%ecx, %edx
	negl	%edx
.LBB0_81:
	movswl	%dx, %esi
	cmpl	$22118, %esi            # imm = 0x5666
	jge	.LBB0_82
# BB#84:
	sarw	%dx
	jmp	.LBB0_86
.LBB0_82:
	cmpl	$31129, %esi            # imm = 0x7999
	jg	.LBB0_85
# BB#83:
	addl	$-11059, %edx           # imm = 0xD4CD
	jmp	.LBB0_86
.LBB0_85:                               # %.thread18.5.i
	leal	26624(,%rdx,4), %edx
.LBB0_86:
	movl	%edx, %r8d
	negl	%r8d
	testw	%cx, %cx
	cmovnsw	%dx, %r8w
	movw	%r8w, 10(%rbx)
	movw	12(%rbx), %cx
	testw	%cx, %cx
	movw	%cx, %dx
	jns	.LBB0_89
# BB#87:
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movw	$32767, %dx             # imm = 0x7FFF
	je	.LBB0_93
# BB#88:
	movl	%ecx, %edx
	negl	%edx
.LBB0_89:
	movswl	%dx, %esi
	cmpl	$22118, %esi            # imm = 0x5666
	jge	.LBB0_90
# BB#92:
	sarw	%dx
	jmp	.LBB0_94
.LBB0_90:
	cmpl	$31129, %esi            # imm = 0x7999
	jg	.LBB0_93
# BB#91:
	addl	$-11059, %edx           # imm = 0xD4CD
	jmp	.LBB0_94
.LBB0_93:                               # %.thread18.6.i
	leal	26624(,%rdx,4), %edx
.LBB0_94:
	movl	%edx, %r15d
	negl	%r15d
	testw	%cx, %cx
	cmovnsw	%dx, %r15w
	movw	%r15w, 12(%rbx)
	movw	14(%rbx), %cx
	movabsq	$175921860444160, %r14  # imm = 0xA00000000000
	testw	%cx, %cx
	movw	%cx, %dx
	jns	.LBB0_97
# BB#95:
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movw	$32767, %dx             # imm = 0x7FFF
	je	.LBB0_101
# BB#96:
	movl	%ecx, %edx
	negl	%edx
.LBB0_97:
	movswl	%dx, %esi
	cmpl	$22118, %esi            # imm = 0x5666
	jge	.LBB0_98
# BB#100:
	sarw	%dx
	jmp	.LBB0_102
.LBB0_98:
	cmpl	$31129, %esi            # imm = 0x7999
	jg	.LBB0_101
# BB#99:
	addl	$-11059, %edx           # imm = 0xD4CD
	jmp	.LBB0_102
.LBB0_101:                              # %.thread18.7.i
	leal	26624(,%rdx,4), %edx
.LBB0_102:                              # %Transformation_to_Log_Area_Ratios.exit
	movl	%edx, %r11d
	negl	%r11d
	testw	%cx, %cx
	cmovnsw	%dx, %r11w
	movw	%r11w, 14(%rbx)
	movswq	%di, %rcx
	imulq	%r14, %rcx
	sarq	$48, %rcx
	leaq	32768(%rcx), %rsi
	testq	%rcx, %rcx
	movl	$32767, %edi            # imm = 0x7FFF
	movq	$-32768, %rdx           # imm = 0x8000
	cmovleq	%rdx, %rdi
	cmpq	$65535, %rsi            # imm = 0xFFFF
	cmovbeq	%rcx, %rdi
	leal	256(%rdi), %ecx
	cmpq	$-256, %rdi
	leaq	33024(%rdi), %rbp
	movl	$32256, %edi            # imm = 0x7E00
	movl	$32768, %esi            # imm = 0x8000
	movl	$32768, %ebx            # imm = 0x8000
	cmovgl	%edi, %ebx
	cmpq	$65535, %rbp            # imm = 0xFFFF
	cmovbel	%ecx, %ebx
	movswl	%bx, %ebp
	sarl	$9, %ebp
	movw	$63, %cx
	cmpl	$31, %ebp
	movw	$63, %bx
	jg	.LBB0_104
# BB#103:
	leal	32(%rbp), %eax
	xorl	%ebx, %ebx
	cmpl	$-32, %ebp
	cmovgew	%ax, %bx
.LBB0_104:
	movq	16(%rsp), %rax          # 8-byte Reload
	movw	%bx, (%rax)
	movswq	%r10w, %rax
	imulq	%r14, %rax
	sarq	$48, %rax
	leaq	32768(%rax), %rbp
	testq	%rax, %rax
	movl	$32767, %ebx            # imm = 0x7FFF
	cmovgq	%rbx, %rdx
	cmpq	$65535, %rbp            # imm = 0xFFFF
	cmovbeq	%rax, %rdx
	leal	256(%rdx), %eax
	cmpq	$-256, %rdx
	leaq	33024(%rdx), %rdx
	cmovgl	%edi, %esi
	cmpq	$65535, %rdx            # imm = 0xFFFF
	cmovbel	%eax, %esi
	movswl	%si, %edx
	sarl	$9, %edx
	cmpl	$31, %edx
	movabsq	$-72057594037927936, %r10 # imm = 0xFF00000000000000
	jg	.LBB0_106
# BB#105:
	leal	32(%rdx), %eax
	xorl	%ecx, %ecx
	cmpl	$-32, %edx
	cmovgew	%ax, %cx
.LBB0_106:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movw	%cx, 2(%rbx)
	movswq	%r12w, %rax
	imulq	%r14, %rax
	sarq	$48, %rax
	xorl	%ecx, %ecx
	cmpq	$-2047, %rax            # imm = 0xF801
	leaq	2048(%rax), %rdx
	leaq	34816(%rax), %rax
	setl	%cl
	addq	$32767, %rcx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbeq	%rdx, %rcx
	movswq	%cx, %rax
	shlq	$48, %rcx
	leal	256(%rax), %esi
	addq	$33024, %rax            # imm = 0x8100
	cmpq	%r10, %rcx
	movl	$32256, %edx            # imm = 0x7E00
	movl	$32768, %ecx            # imm = 0x8000
	movl	$32768, %edi            # imm = 0x8000
	cmovgl	%edx, %edi
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbel	%esi, %edi
	movswl	%di, %esi
	sarl	$9, %esi
	movw	$31, %bp
	cmpl	$15, %esi
	movw	$31, %di
	jg	.LBB0_108
# BB#107:
	leal	16(%rsi), %eax
	xorl	%edi, %edi
	cmpl	$-16, %esi
	cmovgew	%ax, %di
.LBB0_108:
	movw	%di, 4(%rbx)
	movswq	%r13w, %rax
	imulq	%r14, %rax
	sarq	$48, %rax
	xorl	%esi, %esi
	cmpq	$2561, %rax             # imm = 0xA01
	leaq	62976(%rax), %rdi
	leaq	30208(%rax), %rax
	setl	%sil
	addq	$32767, %rsi            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbeq	%rdi, %rsi
	movswq	%si, %rax
	shlq	$48, %rsi
	leal	256(%rax), %edi
	addq	$33024, %rax            # imm = 0x8100
	cmpq	%r10, %rsi
	cmovgl	%edx, %ecx
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbel	%edi, %ecx
	movswl	%cx, %ecx
	sarl	$9, %ecx
	cmpl	$15, %ecx
	jg	.LBB0_112
# BB#109:
	cmpl	$-16, %ecx
	jge	.LBB0_111
# BB#110:
	xorl	%ebp, %ebp
	jmp	.LBB0_112
.LBB0_111:
	addl	$16, %ecx
	movw	%cx, %bp
.LBB0_112:
	movw	%bp, 6(%rbx)
	movswq	%r9w, %rax
	movabsq	$119949846642688, %rcx  # imm = 0x6D1800000000
	imulq	%rax, %rcx
	sarq	$48, %rcx
	xorl	%eax, %eax
	cmpq	$-93, %rcx
	leaq	94(%rcx), %rdx
	leaq	32862(%rcx), %rcx
	setl	%al
	addq	$32767, %rax            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbeq	%rdx, %rax
	movswq	%ax, %rdx
	shlq	$48, %rax
	leal	256(%rdx), %esi
	addq	$33024, %rdx            # imm = 0x8100
	cmpq	%r10, %rax
	movl	$32256, %ecx            # imm = 0x7E00
	movl	$32768, %ebp            # imm = 0x8000
	movl	$32768, %eax            # imm = 0x8000
	cmovgl	%ecx, %eax
	cmpq	$65535, %rdx            # imm = 0xFFFF
	cmovbel	%esi, %eax
	movswl	%ax, %edx
	sarl	$9, %edx
	movw	$15, %ax
	cmpl	$7, %edx
	movw	$15, %si
	jg	.LBB0_116
# BB#113:
	cmpl	$-8, %edx
	jge	.LBB0_115
# BB#114:
	xorl	%esi, %esi
	jmp	.LBB0_116
.LBB0_115:
	addl	$8, %edx
	movw	%dx, %si
.LBB0_116:
	movw	%si, 8(%rbx)
	movswq	%r8w, %rdx
	movabsq	$131941395333120, %rsi  # imm = 0x780000000000
	imulq	%rdx, %rsi
	sarq	$48, %rsi
	xorl	%edx, %edx
	cmpq	$1793, %rsi             # imm = 0x701
	leaq	63744(%rsi), %rdi
	leaq	30976(%rsi), %rsi
	setl	%dl
	addq	$32767, %rdx            # imm = 0x7FFF
	cmpq	$65535, %rsi            # imm = 0xFFFF
	cmovbeq	%rdi, %rdx
	movswq	%dx, %rsi
	shlq	$48, %rdx
	leal	256(%rsi), %edi
	addq	$33024, %rsi            # imm = 0x8100
	cmpq	%r10, %rdx
	cmovgl	%ecx, %ebp
	cmpq	$65535, %rsi            # imm = 0xFFFF
	cmovbel	%edi, %ebp
	movswl	%bp, %ecx
	sarl	$9, %ecx
	cmpl	$7, %ecx
	jg	.LBB0_120
# BB#117:
	cmpl	$-8, %ecx
	jge	.LBB0_119
# BB#118:
	xorl	%eax, %eax
	jmp	.LBB0_120
.LBB0_119:
	addl	$8, %ecx
	movw	%cx, %ax
.LBB0_120:
	movw	%ax, 10(%rbx)
	movswq	%r15w, %rax
	movabsq	$73306501808128, %rcx   # imm = 0x42AC00000000
	imulq	%rax, %rcx
	sarq	$48, %rcx
	xorl	%eax, %eax
	cmpq	$342, %rcx              # imm = 0x156
	leaq	65195(%rcx), %rdx
	leaq	32427(%rcx), %rcx
	setl	%al
	addq	$32767, %rax            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbeq	%rdx, %rax
	movswq	%ax, %rdx
	shlq	$48, %rax
	leal	256(%rdx), %edi
	addq	$33024, %rdx            # imm = 0x8100
	cmpq	%r10, %rax
	movl	$32256, %ecx            # imm = 0x7E00
	movl	$32768, %esi            # imm = 0x8000
	movl	$32768, %eax            # imm = 0x8000
	cmovgl	%ecx, %eax
	cmpq	$65535, %rdx            # imm = 0xFFFF
	cmovbel	%edi, %eax
	movswl	%ax, %edx
	sarl	$9, %edx
	movw	$7, %ax
	cmpl	$3, %edx
	movw	$7, %di
	jg	.LBB0_124
# BB#121:
	cmpl	$-4, %edx
	jge	.LBB0_123
# BB#122:
	xorl	%edi, %edi
	jmp	.LBB0_124
.LBB0_123:
	addl	$4, %edx
	movw	%dx, %di
.LBB0_124:
	movw	%di, 12(%rbx)
	movswq	%r11w, %rdx
	movabsq	$77618648973312, %rdi   # imm = 0x469800000000
	imulq	%rdx, %rdi
	sarq	$48, %rdi
	xorl	%edx, %edx
	cmpq	$1145, %rdi             # imm = 0x479
	leaq	64392(%rdi), %rbp
	leaq	31624(%rdi), %rdi
	setl	%dl
	addq	$32767, %rdx            # imm = 0x7FFF
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbeq	%rbp, %rdx
	movswq	%dx, %rdi
	shlq	$48, %rdx
	leal	256(%rdi), %ebp
	addq	$33024, %rdi            # imm = 0x8100
	cmpq	%r10, %rdx
	cmovgl	%ecx, %esi
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbel	%ebp, %esi
	movswl	%si, %ecx
	sarl	$9, %ecx
	cmpl	$3, %ecx
	jg	.LBB0_128
# BB#125:
	cmpl	$-4, %ecx
	jge	.LBB0_127
# BB#126:
	xorl	%eax, %eax
	jmp	.LBB0_128
.LBB0_127:
	addl	$4, %ecx
	movw	%cx, %ax
.LBB0_128:                              # %Quantization_and_coding.exit
	movw	%ax, 14(%rbx)
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_13:                               # %vector.body157.preheader
	movl	$2, %eax
	movdqa	.LCPI0_2(%rip), %xmm0   # xmm0 = [16384,16384]
	.p2align	4, 0x90
.LBB0_14:                               # %vector.body157
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rbp,%rax,2), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movd	(%rbp,%rax,2), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	movdqa	%xmm1, %xmm3
	psrad	$31, %xmm3
	psrad	$16, %xmm1
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	movdqa	%xmm2, %xmm3
	psrad	$31, %xmm3
	psrad	$16, %xmm2
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	psllq	$13, %xmm1
	psllq	$13, %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	psrlq	$15, %xmm1
	psrlq	$15, %xmm2
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, -4(%rbp,%rax,2)
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, (%rbp,%rax,2)
	addq	$4, %rax
	cmpq	$162, %rax
	jne	.LBB0_14
# BB#15:
	movl	$2, %eax
	jmp	.LBB0_19
.LBB0_10:                               # %vector.body142.preheader
	movl	$2, %eax
	movdqa	.LCPI0_2(%rip), %xmm0   # xmm0 = [16384,16384]
	.p2align	4, 0x90
.LBB0_11:                               # %vector.body142
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rbp,%rax,2), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movd	(%rbp,%rax,2), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	movdqa	%xmm1, %xmm3
	psrad	$31, %xmm3
	psrad	$16, %xmm1
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	movdqa	%xmm2, %xmm3
	psrad	$31, %xmm3
	psrad	$16, %xmm2
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	psllq	$12, %xmm1
	psllq	$12, %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	psrlq	$15, %xmm1
	psrlq	$15, %xmm2
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, -4(%rbp,%rax,2)
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, (%rbp,%rax,2)
	addq	$4, %rax
	cmpq	$162, %rax
	jne	.LBB0_11
# BB#12:
	movl	$3, %eax
	jmp	.LBB0_19
.LBB0_7:                                # %vector.body127.preheader
	movl	$2, %eax
	movdqa	.LCPI0_2(%rip), %xmm0   # xmm0 = [16384,16384]
	.p2align	4, 0x90
.LBB0_8:                                # %vector.body127
                                        # =>This Inner Loop Header: Depth=1
	movd	-4(%rbp,%rax,2), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movd	(%rbp,%rax,2), %xmm2    # xmm2 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	movdqa	%xmm1, %xmm3
	psrad	$31, %xmm3
	psrad	$16, %xmm1
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	movdqa	%xmm2, %xmm3
	psrad	$31, %xmm3
	psrad	$16, %xmm2
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	psllq	$11, %xmm1
	psllq	$11, %xmm2
	paddq	%xmm0, %xmm1
	paddq	%xmm0, %xmm2
	psrlq	$15, %xmm1
	psrlq	$15, %xmm2
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, -4(%rbp,%rax,2)
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3,4,5,6,7]
	movd	%xmm1, (%rbp,%rax,2)
	addq	$4, %rax
	cmpq	$162, %rax
	jne	.LBB0_8
# BB#9:
	movl	$4, %eax
	jmp	.LBB0_19
.Lfunc_end0:
	.size	Gsm_LPC_Analysis, .Lfunc_end0-Gsm_LPC_Analysis
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_16
	.quad	.LBB0_13
	.quad	.LBB0_10
	.quad	.LBB0_7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
