	.text
	.file	"z14.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	FillObject
	.p2align	4, 0x90
	.type	FillObject,@function
FillObject:                             # @FillObject
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movl	%r8d, 160(%rsp)         # 4-byte Spill
	movl	%ecx, 164(%rsp)         # 4-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	240(%rsp), %rbp
	cmpb	$17, 32(%r14)
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	je	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	168(%rsp), %rdx         # 8-byte Reload
.LBB0_2:
	leaq	32(%r14), %r13
	movl	$0, (%rbp)
	testq	%rdx, %rdx
	je	.LBB0_4
# BB#3:
	xorl	%r15d, %r15d
	movl	$0, 76(%rsp)            # 4-byte Folded Spill
                                        # implicit-def: %EAX
	movl	%eax, 156(%rsp)         # 4-byte Spill
	jmp	.LBB0_9
.LBB0_4:
	movl	4(%rbx), %r15d
	movl	8(%rbx), %eax
	cmpl	%r15d, %eax
	cmovlel	%eax, %r15d
	movb	68(%r14), %al
	andb	$96, %al
	cmpb	$32, %al
	movl	%r15d, 76(%rsp)         # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 156(%rsp)         # 4-byte Spill
	jne	.LBB0_6
# BB#5:
	movl	76(%r14), %edi
	andl	$4095, %edi             # imm = 0xFFF
	movq	%r14, %rsi
	callq	FontSize
	addl	%eax, %eax
	movl	%r15d, %ecx
	movl	%eax, 156(%rsp)         # 4-byte Spill
	subl	%eax, %ecx
	movl	%ecx, 76(%rsp)          # 4-byte Spill
.LBB0_6:
	movl	56(%r14), %eax
	addl	48(%r14), %eax
	cmpl	%r15d, %eax
	jg	.LBB0_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_8:
	movl	76(%r14), %edi
	andl	$4095, %edi             # imm = 0xFFF
	movq	%r14, %rsi
	callq	FontSize
	addl	%eax, %eax
	cmpl	%eax, %r15d
	jle	.LBB0_57
.LBB0_9:
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_11
# BB#10:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_12
.LBB0_11:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_12:
	movb	$1, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movw	$1, 41(%rbx)
	movzwl	44(%rbx), %eax
	andl	$127, %eax
	orl	$52736, %eax            # imm = 0xCE00
	movw	%ax, 44(%rbx)
	movw	$4096, 46(%rbx)         # imm = 0x1000
	movl	$11, %edi
	movl	$.L.str.6, %esi
	movq	%r13, %rdx
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_14
# BB#13:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_15
.LBB0_14:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_15:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_18
# BB#16:
	testq	%rax, %rax
	je	.LBB0_18
# BB#17:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_18:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_21
# BB#19:
	testq	%rax, %rax
	je	.LBB0_21
# BB#20:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_21:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_23
# BB#22:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_24
.LBB0_23:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_24:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_26
# BB#25:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_26:
	testq	%rbx, %rbx
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB0_29
# BB#27:
	testq	%rax, %rax
	je	.LBB0_29
# BB#28:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_29:
	movl	$11, %edi
	movl	$.L.str.4, %esi
	movq	%r13, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movl	$536870912, 40(%rbx)    # imm = 0x20000000
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_31
# BB#30:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_32
.LBB0_31:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_32:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	movq	%r13, 32(%rsp)          # 8-byte Spill
	je	.LBB0_34
# BB#33:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_34:
	movl	%r15d, 72(%rsp)         # 4-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_37
# BB#35:
	testq	%rax, %rax
	je	.LBB0_37
# BB#36:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_37:
	testl	%r12d, %r12d
	je	.LBB0_83
# BB#38:                                # %.critedge.preheader
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB0_46
	.p2align	4, 0x90
.LBB0_39:                               # %.preheader1786
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_40 Depth 2
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB0_40:                               #   Parent Loop BB0_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %r13
	movzbl	32(%r13), %eax
	testb	%al, %al
	je	.LBB0_40
# BB#41:                                #   in Loop: Header=BB0_39 Depth=1
	cmpb	$9, %al
	je	.LBB0_44
# BB#42:                                #   in Loop: Header=BB0_39 Depth=1
	cmpb	$1, %al
	je	.LBB0_45
# BB#43:                                #   in Loop: Header=BB0_39 Depth=1
	addb	$-9, %al
	cmpb	$91, %al
	jae	.LBB0_45
	jmp	.LBB0_48
	.p2align	4, 0x90
.LBB0_44:                               #   in Loop: Header=BB0_39 Depth=1
	movq	%r13, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	jne	.LBB0_48
.LBB0_45:                               # %.critedge.outer
                                        #   in Loop: Header=BB0_39 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.LBB0_39
	jmp	.LBB0_47
.LBB0_46:
                                        # implicit-def: %R13
.LBB0_47:                               # %.thread
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbp, %rbx
.LBB0_48:                               # %.loopexit2491
	movq	8(%rbx), %r14
	cmpq	%rbp, %r14
	je	.LBB0_83
# BB#49:                                # %.preheader1784.lr.ph.preheader
	movl	56(%r13), %r15d
	addl	48(%r13), %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_50:                               # %.preheader1784
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_51 Depth 2
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB0_51:                               #   Parent Loop BB0_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_51
# BB#52:                                #   in Loop: Header=BB0_50 Depth=1
	cmpb	$9, %al
	je	.LBB0_61
# BB#53:                                #   in Loop: Header=BB0_50 Depth=1
	cmpb	$1, %al
	jne	.LBB0_55
# BB#54:                                # %.critedge1.outer
                                        #   in Loop: Header=BB0_50 Depth=1
	movq	8(%r14), %r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	movq	%rbx, %r12
	jne	.LBB0_50
	jmp	.LBB0_83
	.p2align	4, 0x90
.LBB0_61:                               #   in Loop: Header=BB0_50 Depth=1
	movq	%rbx, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB0_56
	jmp	.LBB0_62
	.p2align	4, 0x90
.LBB0_55:                               #   in Loop: Header=BB0_50 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_62
.LBB0_56:                               # %.critedge1.backedge
                                        #   in Loop: Header=BB0_50 Depth=1
	movq	8(%r14), %r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB0_50
	jmp	.LBB0_83
.LBB0_57:
	movl	%r15d, %edi
	callq	EchoLength
	movq	%rax, %rbp
	movl	$14, %edi
	movl	$6, %esi
	movl	$.L.str.3, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r13, %r8
	movq	%rbp, %r9
	callq	Error
	movl	$11, %edi
	movl	$.L.str.4, %esi
	movq	%r13, %rdx
	callq	MakeWord
	movq	%rax, %rbx
	movl	$4095, %ecx             # imm = 0xFFF
	andl	76(%r14), %ecx
	movl	$-4096, %eax            # imm = 0xF000
	andl	40(%rbx), %eax
	orl	%ecx, %eax
	movl	%eax, 40(%rbx)
	movl	$4190208, %ecx          # imm = 0x3FF000
	andl	76(%r14), %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbx)
	movl	$4194304, %ecx          # imm = 0x400000
	andl	76(%r14), %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbx)
	movl	76(%r14), %ecx
	shrl	%ecx
	andl	$528482304, %ecx        # imm = 0x1F800000
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbx)
	movb	68(%r14), %cl
	andb	$3, %cl
	xorl	%edx, %edx
	cmpb	$2, %cl
	sete	%dl
	shll	$31, %edx
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	orl	%edx, %eax
	movl	%eax, 40(%rbx)
	movl	$0, 56(%rbx)
	movl	$0, 48(%rbx)
	movq	%r14, zz_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB0_492
# BB#58:
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r14), %rcx
	movq	%rax, 24(%rcx)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_493
# BB#59:
	testq	%rax, %rax
	je	.LBB0_493
# BB#60:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB0_493
.LBB0_62:
	testq	%r12, %r12
	jne	.LBB0_64
# BB#63:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_64:                               # %.preheader1780
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	je	.LBB0_83
# BB#65:                                # %.lr.ph2175.preheader
	movl	%r15d, 24(%rsp)         # 4-byte Spill
.LBB0_66:                               # %.lr.ph2175
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_72 Depth 2
                                        #       Child Loop BB0_73 Depth 3
	movl	56(%r13), %edi
	movl	48(%rbx), %esi
	movl	56(%rbx), %edx
	addq	$44, %r12
	movq	%r12, %rcx
	callq	MinGap
	addl	24(%rsp), %eax          # 4-byte Folded Reload
	subl	56(%r13), %eax
	addl	48(%rbx), %eax
	cmpl	%r15d, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%eax, %r13d
	jge	.LBB0_70
# BB#67:                                #   in Loop: Header=BB0_66 Depth=1
	movzwl	(%r12), %eax
	movl	%eax, %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	cmpl	$1024, %ecx             # imm = 0x400
	jne	.LBB0_69
# BB#68:                                #   in Loop: Header=BB0_66 Depth=1
	orl	$128, %eax
	movw	%ax, (%r12)
.LBB0_69:                               #   in Loop: Header=BB0_66 Depth=1
	movl	%r15d, %r13d
.LBB0_70:                               #   in Loop: Header=BB0_66 Depth=1
	movq	8(%r14), %r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	je	.LBB0_83
# BB#71:                                # %.preheader1777.lr.ph.preheader
                                        #   in Loop: Header=BB0_66 Depth=1
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_72:                               # %.preheader1777
                                        #   Parent Loop BB0_66 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_73 Depth 3
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB0_73:                               #   Parent Loop BB0_66 Depth=1
                                        #     Parent Loop BB0_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_73
# BB#74:                                #   in Loop: Header=BB0_72 Depth=2
	cmpb	$9, %al
	je	.LBB0_79
# BB#75:                                #   in Loop: Header=BB0_72 Depth=2
	cmpb	$1, %al
	jne	.LBB0_77
# BB#76:                                # %.critedge2.outer
                                        #   in Loop: Header=BB0_72 Depth=2
	movq	8(%r14), %r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	movq	%rbp, %r15
	movq	%rbp, %r12
	jne	.LBB0_72
	jmp	.LBB0_83
	.p2align	4, 0x90
.LBB0_79:                               #   in Loop: Header=BB0_72 Depth=2
	movq	%rbp, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB0_78
	jmp	.LBB0_80
	.p2align	4, 0x90
.LBB0_77:                               #   in Loop: Header=BB0_72 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_80
.LBB0_78:                               # %.critedge2.backedge
                                        #   in Loop: Header=BB0_72 Depth=2
	movq	8(%r14), %r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB0_72
	jmp	.LBB0_83
.LBB0_80:                               #   in Loop: Header=BB0_66 Depth=1
	testq	%r15, %r15
	jne	.LBB0_82
# BB#81:                                #   in Loop: Header=BB0_66 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_82:                               # %.backedge1782
                                        #   in Loop: Header=BB0_66 Depth=1
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	movl	%r13d, %r15d
	movq	%rbx, %r13
	movq	%rbp, %rbx
	jne	.LBB0_66
.LBB0_83:                               # %.loopexit1781
	movq	16(%rsp), %rax          # 8-byte Reload
	testb	$3, 68(%rax)
	jne	.LBB0_85
# BB#84:
	movl	$14, %edi
	movl	$7, %esi
	movl	$.L.str.9, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	32(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB0_85:                               # %.preheader1776
                                        # implicit-def: %EAX
	movl	%eax, 52(%rsp)          # 4-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 68(%rsp)          # 4-byte Spill
                                        # implicit-def: %AL
	movl	%eax, 24(%rsp)          # 4-byte Spill
                                        # implicit-def: %EDI
                                        # implicit-def: %EAX
	movq	%rax, 88(%rsp)          # 8-byte Spill
                                        # implicit-def: %R13D
	movl	$0, 124(%rsp)           # 4-byte Folded Spill
                                        # implicit-def: %R12
                                        # implicit-def: %R15
                                        # implicit-def: %RAX
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jmp	.LBB0_87
	.p2align	4, 0x90
.LBB0_86:                               #   in Loop: Header=BB0_87 Depth=1
	movl	%edi, %ebx
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	Hyphenate
	movl	%ebx, %edi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	240(%rsp), %rax
	movl	$1, (%rax)
	movl	$0, 164(%rsp)           # 4-byte Folded Spill
	movl	$1, 124(%rsp)           # 4-byte Folded Spill
.LBB0_87:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_89 Depth 2
                                        #       Child Loop BB0_90 Depth 3
                                        #     Child Loop BB0_96 Depth 2
                                        #     Child Loop BB0_103 Depth 2
                                        #     Child Loop BB0_106 Depth 2
                                        #       Child Loop BB0_107 Depth 3
                                        #         Child Loop BB0_108 Depth 4
                                        #     Child Loop BB0_154 Depth 2
                                        #       Child Loop BB0_156 Depth 3
                                        #         Child Loop BB0_164 Depth 4
                                        #           Child Loop BB0_165 Depth 5
                                        #         Child Loop BB0_173 Depth 4
                                        #           Child Loop BB0_174 Depth 5
                                        #         Child Loop BB0_253 Depth 4
                                        #         Child Loop BB0_258 Depth 4
                                        #         Child Loop BB0_268 Depth 4
                                        #         Child Loop BB0_204 Depth 4
                                        #         Child Loop BB0_209 Depth 4
                                        #           Child Loop BB0_211 Depth 5
                                        #         Child Loop BB0_220 Depth 4
                                        #         Child Loop BB0_223 Depth 4
                                        #           Child Loop BB0_224 Depth 5
                                        #             Child Loop BB0_225 Depth 6
                                        #         Child Loop BB0_305 Depth 4
                                        #         Child Loop BB0_310 Depth 4
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rbx
	movb	$5, %dl
	cmpq	%rax, %rbx
	je	.LBB0_100
# BB#88:                                # %.preheader1771.lr.ph.preheader
                                        #   in Loop: Header=BB0_87 Depth=1
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB0_89:                               # %.preheader1771
                                        #   Parent Loop BB0_87 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_90 Depth 3
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_90:                               #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_89 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_90
# BB#91:                                #   in Loop: Header=BB0_89 Depth=2
	cmpb	$9, %al
	je	.LBB0_97
# BB#92:                                #   in Loop: Header=BB0_89 Depth=2
	cmpb	$1, %al
	je	.LBB0_98
# BB#93:                                #   in Loop: Header=BB0_89 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB0_98
	jmp	.LBB0_94
	.p2align	4, 0x90
.LBB0_97:                               #   in Loop: Header=BB0_89 Depth=2
	movl	%edi, %r14d
	movq	%rbp, %rdi
	callq	SplitIsDefinite
	movl	%r14d, %edi
	movl	32(%rsp), %r13d         # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB0_94
.LBB0_98:                               # %.critedge3.outer
                                        #   in Loop: Header=BB0_89 Depth=2
	movq	8(%rbx), %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jne	.LBB0_89
# BB#99:                                #   in Loop: Header=BB0_87 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	24(%rsp), %ecx          # 4-byte Reload
	movb	$5, %dl
	jmp	.LBB0_152
	.p2align	4, 0x90
.LBB0_100:                              #   in Loop: Header=BB0_87 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	24(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB0_152
	.p2align	4, 0x90
.LBB0_94:                               #   in Loop: Header=BB0_87 Depth=1
	movq	168(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB0_101
# BB#95:                                #   in Loop: Header=BB0_87 Depth=1
	movq	8(%rax), %rax
	.p2align	4, 0x90
.LBB0_96:                               #   Parent Loop BB0_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_96
	jmp	.LBB0_102
.LBB0_101:                              #   in Loop: Header=BB0_87 Depth=1
	xorl	%eax, %eax
.LBB0_102:                              # %.loopexit1775
                                        #   in Loop: Header=BB0_87 Depth=1
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	48(%rbp), %eax
	movl	56(%rbp), %edx
	leaq	56(%rbp), %rsi
	leaq	16(%rbx), %rcx
	.p2align	4, 0x90
.LBB0_103:                              #   Parent Loop BB0_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	cmpb	$0, 32(%rcx)
	leaq	16(%rcx), %rcx
	je	.LBB0_103
# BB#104:                               # %.critedge12.preheader
                                        #   in Loop: Header=BB0_87 Depth=1
	addl	%eax, %edx
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movq	8(%rbx), %r13
	xorl	%r14d, %r14d
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	je	.LBB0_125
# BB#105:                               # %.preheader1770.lr.ph.preheader
                                        #   in Loop: Header=BB0_87 Depth=1
	xorl	%ebx, %ebx
	movq	%rsi, %r14
	.p2align	4, 0x90
.LBB0_106:                              # %.preheader1770.lr.ph
                                        #   Parent Loop BB0_87 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_107 Depth 3
                                        #         Child Loop BB0_108 Depth 4
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
.LBB0_107:                              # %.preheader1770
                                        #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_106 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_108 Depth 4
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB0_108:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_106 Depth=2
                                        #       Parent Loop BB0_107 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_108
# BB#109:                               #   in Loop: Header=BB0_107 Depth=3
	cmpb	$9, %al
	je	.LBB0_120
# BB#110:                               #   in Loop: Header=BB0_107 Depth=3
	cmpb	$1, %al
	je	.LBB0_123
# BB#111:                               #   in Loop: Header=BB0_107 Depth=3
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB0_121
	jmp	.LBB0_112
	.p2align	4, 0x90
.LBB0_120:                              #   in Loop: Header=BB0_107 Depth=3
	movq	%rbx, %rdi
	callq	SplitIsDefinite
	movq	%r14, %rsi
	testl	%eax, %eax
	jne	.LBB0_112
.LBB0_121:                              # %.critedge12.backedge
                                        #   in Loop: Header=BB0_107 Depth=3
	movq	8(%r13), %r13
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB0_107
	jmp	.LBB0_122
	.p2align	4, 0x90
.LBB0_123:                              # %.critedge12.outer
                                        #   in Loop: Header=BB0_106 Depth=2
	movq	8(%r13), %r13
	cmpq	16(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB0_106
# BB#124:                               #   in Loop: Header=BB0_87 Depth=1
	movq	%rbx, %rbp
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	32(%rsp), %r13d         # 4-byte Reload
	xorl	%r14d, %r14d
	jmp	.LBB0_138
.LBB0_125:                              #   in Loop: Header=BB0_87 Depth=1
	xorl	%ebp, %ebp
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	32(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB0_138
.LBB0_112:                              #   in Loop: Header=BB0_87 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	jne	.LBB0_114
# BB#113:                               #   in Loop: Header=BB0_87 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rsi
.LBB0_114:                              #   in Loop: Header=BB0_87 Depth=1
	movq	24(%rdx), %rax
	cmpq	16(%rdx), %rax
	movl	32(%rsp), %r13d         # 4-byte Reload
	je	.LBB0_116
# BB#115:                               #   in Loop: Header=BB0_87 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rsi
.LBB0_116:                              #   in Loop: Header=BB0_87 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB0_118
# BB#117:                               #   in Loop: Header=BB0_87 Depth=1
	movl	68(%rax), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
.LBB0_118:                              #   in Loop: Header=BB0_87 Depth=1
	leaq	44(%rdx), %rcx
	movzwl	44(%rdx), %eax
	andl	$57344, %eax            # imm = 0xE000
	cmpl	$49152, %eax            # imm = 0xC000
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	jne	.LBB0_126
# BB#119:                               #   in Loop: Header=BB0_87 Depth=1
	leaq	48(%rbx), %r14
	movl	48(%rbx), %esi
	movl	56(%rbx), %edx
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movl	76(%rsp), %r8d          # 4-byte Reload
	callq	ActualGap
	jmp	.LBB0_127
.LBB0_122:                              #   in Loop: Header=BB0_87 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	32(%rsp), %r13d         # 4-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	xorl	%r14d, %r14d
	jmp	.LBB0_138
.LBB0_126:                              #   in Loop: Header=BB0_87 Depth=1
	movl	(%rsi), %edi
	movq	%rsi, %r14
	movl	48(%rbx), %esi
	movl	56(%rbx), %edx
	movl	%r13d, %r9d
	subl	%edi, %r9d
	movl	76(%rsp), %r8d          # 4-byte Reload
	callq	ActualGap
	subl	48(%rbx), %eax
.LBB0_127:                              #   in Loop: Header=BB0_87 Depth=1
	subl	(%r14), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movw	%ax, 52(%rcx)
	movq	%rcx, %r14
	movzwl	44(%r14), %r14d
	movl	%r14d, %eax
	shrl	$13, %eax
	cmpb	$7, %al
	movq	56(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_129
# BB#128:                               #   in Loop: Header=BB0_87 Depth=1
	cmpb	$2, %al
	jne	.LBB0_134
.LBB0_129:                              #   in Loop: Header=BB0_87 Depth=1
	cmpl	$0, 124(%rsp)           # 4-byte Folded Reload
	je	.LBB0_135
# BB#130:                               #   in Loop: Header=BB0_87 Depth=1
	movb	32(%rbp), %al
	addb	$-11, %al
	xorl	%r14d, %r14d
	cmpb	$1, %al
	ja	.LBB0_136
# BB#131:                               #   in Loop: Header=BB0_87 Depth=1
	leaq	64(%rbp), %rdi
	callq	strlen
	cmpb	$45, 63(%rbp,%rax)
	je	.LBB0_136
# BB#132:                               #   in Loop: Header=BB0_87 Depth=1
	movq	FillObject.hyph_word(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_342
# BB#133:                               # %._crit_edge2431
                                        #   in Loop: Header=BB0_87 Depth=1
	movl	40(%rdi), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	76(%rcx), %rcx
	jmp	.LBB0_343
.LBB0_134:                              #   in Loop: Header=BB0_87 Depth=1
	shrl	$7, %r14d
	andl	$1, %r14d
	jmp	.LBB0_136
.LBB0_135:                              #   in Loop: Header=BB0_87 Depth=1
	movl	$1, %r14d
.LBB0_136:                              #   in Loop: Header=BB0_87 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB0_137:                              #   in Loop: Header=BB0_87 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rsi
.LBB0_138:                              # %.thread1644
                                        #   in Loop: Header=BB0_87 Depth=1
	testq	%rbx, %rbx
	movl	72(%rsp), %ecx          # 4-byte Reload
	je	.LBB0_140
# BB#139:                               #   in Loop: Header=BB0_87 Depth=1
	movl	68(%rbx), %ecx
.LBB0_140:                              #   in Loop: Header=BB0_87 Depth=1
	testl	%ecx, %ecx
	jle	.LBB0_143
# BB#141:                               #   in Loop: Header=BB0_87 Depth=1
	movl	%ecx, %eax
	subl	%r13d, %eax
	jle	.LBB0_144
# BB#142:                               #   in Loop: Header=BB0_87 Depth=1
	shll	$9, %eax
	cltd
	idivl	%ecx
	movl	%eax, %edi
	imull	%edi, %edi
	xorl	%edx, %edx
	jmp	.LBB0_147
.LBB0_143:                              #   in Loop: Header=BB0_87 Depth=1
	xorl	%edi, %edi
	testl	%r13d, %r13d
	setne	%dil
	movb	%dil, %dl
	addb	%dl, %dl
	shll	$20, %edi
	jmp	.LBB0_147
.LBB0_144:                              #   in Loop: Header=BB0_87 Depth=1
	jge	.LBB0_146
# BB#145:                               #   in Loop: Header=BB0_87 Depth=1
	movl	$1048576, %edi          # imm = 0x100000
	movb	$3, %dl
	jmp	.LBB0_147
.LBB0_146:                              #   in Loop: Header=BB0_87 Depth=1
	shll	$7, %eax
	cltd
	idivl	%ecx
	movl	%eax, %edi
	imull	%edi, %edi
	movb	$1, %dl
	.p2align	4, 0x90
.LBB0_147:                              #   in Loop: Header=BB0_87 Depth=1
	xorl	%ecx, %ecx
	testl	%r14d, %r14d
	je	.LBB0_149
# BB#148:                               #   in Loop: Header=BB0_87 Depth=1
	movb	$7, %dl
	jmp	.LBB0_151
.LBB0_149:                              #   in Loop: Header=BB0_87 Depth=1
	cmpb	$2, %dl
	jne	.LBB0_151
# BB#150:                               #   in Loop: Header=BB0_87 Depth=1
	movzwl	44(%rbp), %eax
	andl	$57344, %eax            # imm = 0xE000
	cmpl	$49152, %eax            # imm = 0xC000
	sete	%dl
	movl	$1048576, %eax          # imm = 0x100000
	cmovel	%eax, %edi
	orb	$2, %dl
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_151:                              # %.thread1643
                                        #   in Loop: Header=BB0_87 Depth=1
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
.LBB0_152:                              # %.thread1643
                                        #   in Loop: Header=BB0_87 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	leaq	32(%rbp), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	%ecx, %eax
	movb	%al, 15(%rsp)           # 1-byte Spill
	movl	%edx, 120(%rsp)         # 4-byte Spill
	movl	%edx, %ebx
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	%edi, %ecx
	movl	68(%rsp), %eax          # 4-byte Reload
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	movl	%r13d, 100(%rsp)        # 4-byte Spill
	jmp	.LBB0_154
	.p2align	4, 0x90
.LBB0_153:                              #   in Loop: Header=BB0_154 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movzwl	44(%rax), %eax
	andl	$57344, %eax            # imm = 0xE000
	cmpl	$49152, %eax            # imm = 0xC000
	sete	%cl
	movl	$1048576, %eax          # imm = 0x100000
	cmovel	%eax, %edi
	orb	$2, %cl
	movb	15(%rsp), %al           # 1-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%ecx, 120(%rsp)         # 4-byte Spill
	movl	%ecx, %ebx
	movl	%edi, %ecx
	movl	68(%rsp), %eax          # 4-byte Reload
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	32(%rsp), %eax          # 4-byte Reload
	movl	%eax, 100(%rsp)         # 4-byte Spill
.LBB0_154:                              # %.backedge1772
                                        #   Parent Loop BB0_87 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_156 Depth 3
                                        #         Child Loop BB0_164 Depth 4
                                        #           Child Loop BB0_165 Depth 5
                                        #         Child Loop BB0_173 Depth 4
                                        #           Child Loop BB0_174 Depth 5
                                        #         Child Loop BB0_253 Depth 4
                                        #         Child Loop BB0_258 Depth 4
                                        #         Child Loop BB0_268 Depth 4
                                        #         Child Loop BB0_204 Depth 4
                                        #         Child Loop BB0_209 Depth 4
                                        #           Child Loop BB0_211 Depth 5
                                        #         Child Loop BB0_220 Depth 4
                                        #         Child Loop BB0_223 Depth 4
                                        #           Child Loop BB0_224 Depth 5
                                        #             Child Loop BB0_225 Depth 6
                                        #         Child Loop BB0_305 Depth 4
                                        #         Child Loop BB0_310 Depth 4
	movq	88(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 96(%rsp)          # 4-byte Spill
	jmp	.LBB0_156
.LBB0_155:                              #   in Loop: Header=BB0_156 Depth=3
	movb	$6, %bl
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_156:                              # %.backedge1772
                                        #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_164 Depth 4
                                        #           Child Loop BB0_165 Depth 5
                                        #         Child Loop BB0_173 Depth 4
                                        #           Child Loop BB0_174 Depth 5
                                        #         Child Loop BB0_253 Depth 4
                                        #         Child Loop BB0_258 Depth 4
                                        #         Child Loop BB0_268 Depth 4
                                        #         Child Loop BB0_204 Depth 4
                                        #         Child Loop BB0_209 Depth 4
                                        #           Child Loop BB0_211 Depth 5
                                        #         Child Loop BB0_220 Depth 4
                                        #         Child Loop BB0_223 Depth 4
                                        #           Child Loop BB0_224 Depth 5
                                        #             Child Loop BB0_225 Depth 6
                                        #         Child Loop BB0_305 Depth 4
                                        #         Child Loop BB0_310 Depth 4
	movzbl	%bl, %eax
	cmpb	$8, %al
	ja	.LBB0_193
# BB#157:                               # %.backedge1772
                                        #   in Loop: Header=BB0_156 Depth=3
	movl	32(%rsp), %r13d         # 4-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_158:                              #   in Loop: Header=BB0_156 Depth=3
	cmpl	%ecx, %edi
	movl	24(%rsp), %eax          # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	jl	.LBB0_160
# BB#159:                               #   in Loop: Header=BB0_156 Depth=3
	movb	15(%rsp), %al           # 1-byte Reload
	movl	%ecx, %edx
	movl	120(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %ebx
	movl	%edx, %ecx
.LBB0_160:                              #   in Loop: Header=BB0_156 Depth=3
	cmovlel	%edi, %ecx
	movl	108(%rsp), %edx         # 4-byte Reload
	cmovll	68(%rsp), %edx          # 4-byte Folded Reload
	movl	%edx, 108(%rsp)         # 4-byte Spill
	movl	104(%rsp), %edx         # 4-byte Reload
	cmovll	52(%rsp), %edx          # 4-byte Folded Reload
	movl	%edx, 104(%rsp)         # 4-byte Spill
	movq	144(%rsp), %rdx         # 8-byte Reload
	cmovlq	%rbp, %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movq	136(%rsp), %rdx         # 8-byte Reload
	cmovlq	112(%rsp), %rdx         # 8-byte Folded Reload
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rdx         # 8-byte Reload
	cmovlq	56(%rsp), %rdx          # 8-byte Folded Reload
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movl	100(%rsp), %edx         # 4-byte Reload
	cmovll	%r13d, %edx
	movl	%edx, 100(%rsp)         # 4-byte Spill
	movl	96(%rsp), %edx          # 4-byte Reload
	cmovll	88(%rsp), %edx          # 4-byte Folded Reload
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movb	%al, 15(%rsp)           # 1-byte Spill
	movb	%bl, %al
	movl	%eax, 120(%rsp)         # 4-byte Spill
.LBB0_161:                              #   in Loop: Header=BB0_156 Depth=3
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	movq	8(%rbp), %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	je	.LBB0_162
	.p2align	4, 0x90
.LBB0_164:                              # %.preheader1758
                                        #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_165 Depth 5
	movq	%rbx, %r15
	.p2align	4, 0x90
.LBB0_165:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        #         Parent Loop BB0_164 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%r15), %r15
	movzbl	32(%r15), %eax
	testb	%al, %al
	je	.LBB0_165
# BB#166:                               #   in Loop: Header=BB0_164 Depth=4
	cmpb	$9, %al
	jne	.LBB0_169
# BB#167:                               #   in Loop: Header=BB0_164 Depth=4
	movq	%r15, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB0_163
	jmp	.LBB0_170
	.p2align	4, 0x90
.LBB0_169:                              #   in Loop: Header=BB0_164 Depth=4
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB0_170
.LBB0_163:                              # %.critedge78.backedge
                                        #   in Loop: Header=BB0_164 Depth=4
	movq	8(%rbx), %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jne	.LBB0_164
.LBB0_162:                              # %.thread1664
                                        #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rbx
	jmp	.LBB0_171
.LBB0_170:                              #   in Loop: Header=BB0_156 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB0_171:                              # %.critedge79.preheader
                                        #   in Loop: Header=BB0_156 Depth=3
	movq	8(%rbx), %r14
	cmpq	%rax, %r14
	je	.LBB0_181
# BB#172:                               # %.preheader1757.lr.ph.preheader
                                        #   in Loop: Header=BB0_156 Depth=3
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_173:                              # %.preheader1757
                                        #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_174 Depth 5
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB0_174:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        #         Parent Loop BB0_173 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_174
# BB#175:                               #   in Loop: Header=BB0_173 Depth=4
	cmpb	$9, %al
	je	.LBB0_190
# BB#176:                               #   in Loop: Header=BB0_173 Depth=4
	cmpb	$1, %al
	jne	.LBB0_178
# BB#177:                               # %.critedge79.outer
                                        #   in Loop: Header=BB0_173 Depth=4
	movq	8(%r14), %r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	movq	%rbx, %rbp
	movq	%rbx, %r13
	jne	.LBB0_173
	jmp	.LBB0_182
	.p2align	4, 0x90
.LBB0_190:                              #   in Loop: Header=BB0_173 Depth=4
	movq	%rbx, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB0_179
	jmp	.LBB0_191
	.p2align	4, 0x90
.LBB0_178:                              #   in Loop: Header=BB0_173 Depth=4
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_191
.LBB0_179:                              # %.critedge79.backedge
                                        #   in Loop: Header=BB0_173 Depth=4
	movq	8(%r14), %r14
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB0_173
# BB#180:                               #   in Loop: Header=BB0_156 Depth=3
	movq	%rbp, %rbx
	jmp	.LBB0_182
	.p2align	4, 0x90
.LBB0_181:                              #   in Loop: Header=BB0_156 Depth=3
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
.LBB0_182:                              # %.critedge1556
                                        #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %rbp
.LBB0_183:                              #   in Loop: Header=BB0_156 Depth=3
	movl	24(%rsp), %ecx          # 4-byte Reload
	movzwl	44(%r13), %eax
	andl	$57344, %eax            # imm = 0xE000
	cmpl	$49152, %eax            # imm = 0xC000
	jne	.LBB0_187
# BB#184:                               #   in Loop: Header=BB0_156 Depth=3
	testb	%cl, %cl
	je	.LBB0_197
# BB#185:                               #   in Loop: Header=BB0_156 Depth=3
	decb	%cl
	je	.LBB0_245
# BB#186:                               #   in Loop: Header=BB0_156 Depth=3
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	jmp	.LBB0_249
	.p2align	4, 0x90
.LBB0_187:                              #   in Loop: Header=BB0_156 Depth=3
	testb	%cl, %cl
	je	.LBB0_199
# BB#188:                               #   in Loop: Header=BB0_156 Depth=3
	cmpb	$1, %cl
	jne	.LBB0_249
# BB#189:                               #   in Loop: Header=BB0_156 Depth=3
	movswl	52(%r13), %eax
	movl	52(%rsp), %ecx          # 4-byte Reload
	subl	48(%r15), %ecx
	subl	%eax, %ecx
	subl	56(%r15), %ecx
	movl	%ecx, 52(%rsp)          # 4-byte Spill
	movb	$1, %al
	jmp	.LBB0_248
.LBB0_191:                              #   in Loop: Header=BB0_156 Depth=3
	testq	%rbp, %rbp
	jne	.LBB0_183
# BB#192:                               #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
	xorl	%ebp, %ebp
	jmp	.LBB0_183
.LBB0_193:                              #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	%edi, %r14d
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	movl	%ecx, %ebp
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	%r14d, %edi
	movl	%ebp, %ecx
	jmp	.LBB0_156
.LBB0_194:                              #   in Loop: Header=BB0_156 Depth=3
	cmpl	%edi, %ecx
	jg	.LBB0_202
.LBB0_195:                              #   in Loop: Header=BB0_156 Depth=3
	cmpb	$5, 120(%rsp)           # 1-byte Folded Reload
	jne	.LBB0_200
# BB#196:                               #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	movl	%ecx, %ebx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	100(%rsp), %r13d        # 4-byte Reload
	movl	96(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%ebx, %edi
	jmp	.LBB0_201
.LBB0_197:                              #   in Loop: Header=BB0_156 Depth=3
	movq	112(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, 24(%rbp)
	je	.LBB0_247
# BB#198:                               #   in Loop: Header=BB0_156 Depth=3
	movb	$-1, %al
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB0_249
.LBB0_199:                              #   in Loop: Header=BB0_156 Depth=3
	movswl	52(%r13), %eax
	movl	32(%rsp), %ecx          # 4-byte Reload
	subl	48(%r15), %ecx
	subl	%eax, %ecx
	subl	56(%r15), %ecx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movq	88(%rsp), %rcx          # 8-byte Reload
	subl	%eax, %ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_249
.LBB0_200:                              #   in Loop: Header=BB0_156 Depth=3
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	100(%rsp), %r13d        # 4-byte Reload
	movl	96(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%ecx, %edi
.LBB0_201:                              # %.thread1646
                                        #   in Loop: Header=BB0_156 Depth=3
	movb	15(%rsp), %al           # 1-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	108(%rsp), %eax         # 4-byte Reload
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movl	104(%rsp), %eax         # 4-byte Reload
	movl	%eax, 52(%rsp)          # 4-byte Spill
.LBB0_202:                              # %.thread1646
                                        #   in Loop: Header=BB0_156 Depth=3
	movq	%rbp, %rax
	movq	112(%rsp), %rbp         # 8-byte Reload
	movb	$5, %cl
	movl	%ecx, 120(%rsp)         # 4-byte Spill
	movq	16(%rsp), %rsi          # 8-byte Reload
	cmpq	%rsi, %rbp
	movl	24(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	movb	%cl, 15(%rsp)           # 1-byte Spill
	movb	$5, %bl
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movl	%edi, %ecx
	movl	68(%rsp), %edx          # 4-byte Reload
	movl	%edx, 108(%rsp)         # 4-byte Spill
	movl	52(%rsp), %edx          # 4-byte Reload
	movl	%edx, 104(%rsp)         # 4-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	movl	%r13d, 100(%rsp)        # 4-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 96(%rsp)          # 4-byte Spill
	je	.LBB0_156
# BB#203:                               # %.preheader1768.preheader
                                        #   in Loop: Header=BB0_156 Depth=3
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_204:                              # %.preheader1768
                                        #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_204
# BB#205:                               # %.preheader1768
                                        #   in Loop: Header=BB0_156 Depth=3
	cmpb	$1, %al
	movl	32(%rsp), %r13d         # 4-byte Reload
	je	.LBB0_207
# BB#206:                               #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	%edi, %r14d
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	%r14d, %edi
.LBB0_207:                              # %.loopexit1769
                                        #   in Loop: Header=BB0_156 Depth=3
	movl	%edi, 48(%rbx)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 56(%rbx)
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 64(%rbx)
	movzwl	44(%rbx), %eax
	cmpl	$57344, %eax            # imm = 0xE000
	jb	.LBB0_209
# BB#208:                               #   in Loop: Header=BB0_156 Depth=3
	movq	FillObject.hyph_word(%rip), %rax
	subl	48(%rax), %r13d
	subl	56(%rax), %r13d
	addl	$16, %edi
	movl	%edi, 48(%rbx)
	.p2align	4, 0x90
.LBB0_209:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_211 Depth 5
	movq	8(%rbp), %rbp
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	je	.LBB0_215
# BB#210:                               # %.preheader1760
                                        #   in Loop: Header=BB0_209 Depth=4
	movq	%rbp, %r12
	.p2align	4, 0x90
.LBB0_211:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        #         Parent Loop BB0_209 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	16(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB0_211
# BB#212:                               #   in Loop: Header=BB0_209 Depth=4
	cmpb	$9, %al
	jne	.LBB0_214
# BB#213:                               #   in Loop: Header=BB0_209 Depth=4
	movq	%r12, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB0_209
	jmp	.LBB0_216
	.p2align	4, 0x90
.LBB0_214:                              #   in Loop: Header=BB0_209 Depth=4
	addb	$-9, %al
	cmpb	$91, %al
	jae	.LBB0_209
	jmp	.LBB0_216
.LBB0_215:                              # %.thread1657
                                        #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB0_216:                              # %.loopexit2483
                                        #   in Loop: Header=BB0_156 Depth=3
	movzwl	44(%rbx), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	movswl	52(%rbx), %eax
	cmpl	$49152, %ecx            # imm = 0xC000
	jne	.LBB0_218
# BB#217:                               #   in Loop: Header=BB0_156 Depth=3
	movl	24(%rsp), %esi          # 4-byte Reload
	incb	%sil
	movl	%r13d, 52(%rsp)         # 4-byte Spill
	movl	48(%r12), %ecx
	addl	%eax, %ecx
	addl	56(%r12), %ecx
	xorl	%edx, %edx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movl	%esi, %eax
	movb	%al, 15(%rsp)           # 1-byte Spill
	movl	%ecx, %r13d
	jmp	.LBB0_219
.LBB0_218:                              #   in Loop: Header=BB0_156 Depth=3
	addl	48(%r12), %r13d
	addl	%eax, %r13d
	addl	56(%r12), %r13d
	movq	88(%rsp), %rcx          # 8-byte Reload
	addl	%eax, %ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movl	24(%rsp), %eax          # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	movb	%al, 15(%rsp)           # 1-byte Spill
.LBB0_219:                              #   in Loop: Header=BB0_156 Depth=3
	leaq	16(%rbp), %rax
	.p2align	4, 0x90
.LBB0_220:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rax), %rax
	cmpb	$0, 32(%rax)
	leaq	16(%rax), %rax
	je	.LBB0_220
# BB#221:                               # %.critedge45.preheader
                                        #   in Loop: Header=BB0_156 Depth=3
	movq	8(%rbp), %rbx
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	je	.LBB0_246
# BB#222:                               # %.preheader1759.lr.ph.preheader
                                        #   in Loop: Header=BB0_156 Depth=3
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_223:                              # %.preheader1759.lr.ph
                                        #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB0_224 Depth 5
                                        #             Child Loop BB0_225 Depth 6
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%rbp, %r13
.LBB0_224:                              # %.preheader1759
                                        #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        #         Parent Loop BB0_223 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB0_225 Depth 6
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_225:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        #         Parent Loop BB0_223 Depth=4
                                        #           Parent Loop BB0_224 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_225
# BB#226:                               #   in Loop: Header=BB0_224 Depth=5
	cmpb	$9, %al
	je	.LBB0_237
# BB#227:                               #   in Loop: Header=BB0_224 Depth=5
	cmpb	$1, %al
	je	.LBB0_239
# BB#228:                               #   in Loop: Header=BB0_224 Depth=5
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB0_238
	jmp	.LBB0_229
	.p2align	4, 0x90
.LBB0_237:                              #   in Loop: Header=BB0_224 Depth=5
	movq	%rbp, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	jne	.LBB0_229
.LBB0_238:                              # %.critedge45.backedge
                                        #   in Loop: Header=BB0_224 Depth=5
	movq	8(%rbx), %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jne	.LBB0_224
	jmp	.LBB0_241
	.p2align	4, 0x90
.LBB0_239:                              # %.critedge45.outer
                                        #   in Loop: Header=BB0_223 Depth=4
	movq	8(%rbx), %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jne	.LBB0_223
# BB#240:                               #   in Loop: Header=BB0_156 Depth=3
	movq	%rbp, 80(%rsp)          # 8-byte Spill
.LBB0_241:                              #   in Loop: Header=BB0_156 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	32(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB0_242
.LBB0_245:                              #   in Loop: Header=BB0_156 Depth=3
	movswl	52(%r13), %eax
	subl	%eax, 32(%rsp)          # 4-byte Folded Spill
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_249
.LBB0_246:                              #   in Loop: Header=BB0_156 Depth=3
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB0_242:                              #   in Loop: Header=BB0_156 Depth=3
	movq	40(%rsp), %rbp          # 8-byte Reload
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB0_302
	jmp	.LBB0_243
.LBB0_229:                              #   in Loop: Header=BB0_156 Depth=3
	testq	%r13, %r13
	jne	.LBB0_231
# BB#230:                               #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_231:                              #   in Loop: Header=BB0_156 Depth=3
	movq	24(%r13), %rax
	cmpq	16(%r13), %rax
	movq	56(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_233
# BB#232:                               #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_233:                              #   in Loop: Header=BB0_156 Depth=3
	testq	%rbx, %rbx
	je	.LBB0_235
# BB#234:                               #   in Loop: Header=BB0_156 Depth=3
	movl	68(%rbx), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
.LBB0_235:                              #   in Loop: Header=BB0_156 Depth=3
	leaq	44(%r13), %r14
	movzwl	44(%r13), %eax
	andl	$57344, %eax            # imm = 0xE000
	cmpl	$49152, %eax            # imm = 0xC000
	jne	.LBB0_290
# BB#236:                               #   in Loop: Header=BB0_156 Depth=3
	leaq	48(%rbp), %rbx
	movl	48(%rbp), %esi
	movl	56(%rbp), %edx
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	movl	76(%rsp), %r8d          # 4-byte Reload
	callq	ActualGap
	jmp	.LBB0_291
.LBB0_247:                              #   in Loop: Header=BB0_156 Depth=3
	movb	$-1, %al
.LBB0_248:                              # %.thread1668
                                        #   in Loop: Header=BB0_156 Depth=3
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB0_249:                              # %.thread1668
                                        #   in Loop: Header=BB0_156 Depth=3
	movl	80(%rsp), %ecx          # 4-byte Reload
	movq	24(%rbp), %rsi
	movl	$1048577, %edi          # imm = 0x100001
	movb	$8, %bl
	movq	112(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %rsi
	movq	%rax, 40(%rsp)          # 8-byte Spill
	je	.LBB0_156
# BB#250:                               #   in Loop: Header=BB0_156 Depth=3
	movq	64(%r13), %rax
	testq	%rax, %rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	je	.LBB0_254
# BB#251:                               #   in Loop: Header=BB0_156 Depth=3
	movq	24(%rax), %rcx
	movq	8(%rcx), %rcx
	cmpb	$17, 32(%rcx)
	movq	%rax, %rdx
	je	.LBB0_254
# BB#252:                               # %.preheader1762.preheader
                                        #   in Loop: Header=BB0_156 Depth=3
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB0_253:                              # %.preheader1762
                                        #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rdx), %rdx
	cmpb	$0, 32(%rdx)
	je	.LBB0_253
.LBB0_254:                              # %.loopexit1763
                                        #   in Loop: Header=BB0_156 Depth=3
	cmpq	16(%rsp), %rsi          # 8-byte Folded Reload
	movl	24(%rsp), %ecx          # 4-byte Reload
	je	.LBB0_260
# BB#255:                               #   in Loop: Header=BB0_156 Depth=3
	testq	%rdx, %rdx
	movl	76(%rsp), %ebp          # 4-byte Reload
	je	.LBB0_257
# BB#256:                               #   in Loop: Header=BB0_156 Depth=3
	movl	68(%rdx), %ebp
.LBB0_257:                              #   in Loop: Header=BB0_156 Depth=3
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB0_258:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_258
# BB#259:                               #   in Loop: Header=BB0_156 Depth=3
	movl	48(%rax), %r8d
	jmp	.LBB0_263
.LBB0_260:                              #   in Loop: Header=BB0_156 Depth=3
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.LBB0_262
# BB#261:                               #   in Loop: Header=BB0_156 Depth=3
	movl	68(%rdx), %ebp
	jmp	.LBB0_263
.LBB0_262:                              #   in Loop: Header=BB0_156 Depth=3
	movl	72(%rsp), %ebp          # 4-byte Reload
	.p2align	4, 0x90
.LBB0_263:                              #   in Loop: Header=BB0_156 Depth=3
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	testb	%cl, %cl
	je	.LBB0_272
# BB#264:                               #   in Loop: Header=BB0_156 Depth=3
	movq	8(%rsi), %rax
	movq	8(%rax), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_266
# BB#265:                               #   in Loop: Header=BB0_156 Depth=3
	movl	%r8d, 40(%rsp)          # 4-byte Spill
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movq	%rsi, %r14
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	40(%rsp), %r8d          # 4-byte Reload
	movq	%r14, %rsi
.LBB0_266:                              #   in Loop: Header=BB0_156 Depth=3
	addq	$16, %rbx
	movl	80(%rsp), %ecx          # 4-byte Reload
	jmp	.LBB0_268
	.p2align	4, 0x90
.LBB0_267:                              #   in Loop: Header=BB0_268 Depth=4
	addq	$16, %rbx
.LBB0_268:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_267
# BB#269:                               #   in Loop: Header=BB0_156 Depth=3
	cmpb	$1, %al
	jne	.LBB0_273
# BB#270:                               #   in Loop: Header=BB0_156 Depth=3
	movzwl	44(%rbx), %eax
	andl	$64512, %eax            # imm = 0xFC00
	cmpl	$52224, %eax            # imm = 0xCC00
	jne	.LBB0_273
# BB#271:                               #   in Loop: Header=BB0_156 Depth=3
	movzwl	46(%rbx), %eax
	movl	%r8d, %ecx
	subl	$-128, %ecx
	cmpl	$4096, %eax             # imm = 0x1000
	cmovel	%ecx, %r8d
.LBB0_272:                              # %.loopexit1761
                                        #   in Loop: Header=BB0_156 Depth=3
	movl	80(%rsp), %ecx          # 4-byte Reload
.LBB0_273:                              # %.loopexit1761
                                        #   in Loop: Header=BB0_156 Depth=3
	testl	%ebp, %ebp
	jle	.LBB0_278
# BB#274:                               #   in Loop: Header=BB0_156 Depth=3
	movl	$1048576, %edi          # imm = 0x100000
	movl	68(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, 52(%rsp)          # 4-byte Folded Reload
	jle	.LBB0_276
# BB#275:                               #   in Loop: Header=BB0_156 Depth=3
	movb	$4, %bl
	cmpb	$0, 24(%rsp)            # 1-byte Folded Reload
	jne	.LBB0_279
.LBB0_276:                              #   in Loop: Header=BB0_156 Depth=3
	movl	%ebp, %eax
	movl	32(%rsp), %edx          # 4-byte Reload
	subl	%edx, %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rcx), %ecx
	cmpl	%ecx, %eax
	jle	.LBB0_285
# BB#277:                               #   in Loop: Header=BB0_156 Depth=3
	cmpb	$0, 24(%rsp)            # 1-byte Folded Reload
	setne	%bl
	shll	$9, %eax
	cltd
	idivl	%ebp
	movl	%eax, %edi
	imull	%edi, %edi
	jmp	.LBB0_332
.LBB0_278:                              #   in Loop: Header=BB0_156 Depth=3
	xorl	%edi, %edi
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	setne	%dil
	movl	%edi, %ebx
	addb	%bl, %bl
	shll	$20, %edi
.LBB0_279:                              #   in Loop: Header=BB0_156 Depth=3
	addl	%r8d, %edi
	jns	.LBB0_281
	jmp	.LBB0_280
.LBB0_285:                              #   in Loop: Header=BB0_156 Depth=3
	movl	%edx, %ecx
	subl	%ebp, %ecx
	jle	.LBB0_331
# BB#286:                               #   in Loop: Header=BB0_156 Depth=3
	movb	$3, %bl
	cmpl	$0, 160(%rsp)           # 4-byte Folded Reload
	je	.LBB0_332
# BB#287:                               #   in Loop: Header=BB0_156 Depth=3
	movq	BackEnd(%rip), %rdx
	movl	36(%rdx), %edx
	testl	%edx, %edx
	je	.LBB0_332
# BB#288:                               #   in Loop: Header=BB0_156 Depth=3
	shll	$2, %ecx
	cmpl	88(%rsp), %ecx          # 4-byte Folded Reload
	movl	80(%rsp), %ecx          # 4-byte Reload
	jg	.LBB0_279
# BB#289:                               #   in Loop: Header=BB0_156 Depth=3
	shll	$7, %eax
	cltd
	idivl	%ebp
	movl	%eax, %edi
	imull	%edi, %edi
	movb	$2, %bl
	addl	%r8d, %edi
	jns	.LBB0_281
	jmp	.LBB0_280
.LBB0_331:                              #   in Loop: Header=BB0_156 Depth=3
	shll	$7, %eax
	cltd
	idivl	%ebp
	movl	%eax, %edi
	imull	%edi, %edi
	movb	$1, %bl
.LBB0_332:                              #   in Loop: Header=BB0_156 Depth=3
	movl	80(%rsp), %ecx          # 4-byte Reload
	addl	%r8d, %edi
	jns	.LBB0_281
.LBB0_280:                              #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	%edi, %r14d
	movl	$1, %edi
	movq	%rsi, %rbp
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbp, %rsi
	movl	%r14d, %edi
	movl	80(%rsp), %ecx          # 4-byte Reload
.LBB0_281:                              #   in Loop: Header=BB0_156 Depth=3
	movw	44(%r13), %ax
	testb	%al, %al
	js	.LBB0_155
# BB#282:                               #   in Loop: Header=BB0_156 Depth=3
	cmpl	$0, 124(%rsp)           # 4-byte Folded Reload
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_156
# BB#283:                               #   in Loop: Header=BB0_156 Depth=3
	andl	$57344, %eax            # imm = 0xE000
	shrl	$13, %eax
	cmpb	$7, %al
	je	.LBB0_155
# BB#284:                               #   in Loop: Header=BB0_156 Depth=3
	cmpb	$2, %al
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_156
	jmp	.LBB0_155
.LBB0_290:                              #   in Loop: Header=BB0_156 Depth=3
	leaq	56(%r12), %rbx
	movl	56(%r12), %edi
	movl	48(%rbp), %esi
	movl	56(%rbp), %edx
	movl	32(%rsp), %r9d          # 4-byte Reload
	subl	%edi, %r9d
	movq	%r14, %rcx
	movl	76(%rsp), %r8d          # 4-byte Reload
	callq	ActualGap
	subl	48(%rbp), %eax
.LBB0_291:                              #   in Loop: Header=BB0_156 Depth=3
	movq	40(%rsp), %rbp          # 8-byte Reload
	subl	(%rbx), %eax
	movw	%ax, 52(%r13)
	movzwl	44(%r13), %ecx
	movl	%ecx, %eax
	shrl	$13, %eax
	cmpb	$7, %al
	je	.LBB0_293
# BB#292:                               #   in Loop: Header=BB0_156 Depth=3
	cmpb	$2, %al
	jne	.LBB0_298
.LBB0_293:                              #   in Loop: Header=BB0_156 Depth=3
	cmpl	$0, 124(%rsp)           # 4-byte Folded Reload
	je	.LBB0_299
# BB#294:                               #   in Loop: Header=BB0_156 Depth=3
	movb	32(%r12), %al
	addb	$-11, %al
	xorl	%ecx, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmpb	$1, %al
	ja	.LBB0_300
# BB#295:                               #   in Loop: Header=BB0_156 Depth=3
	leaq	64(%r12), %rdi
	callq	strlen
	cmpb	$45, 63(%r12,%rax)
	movq	56(%rsp), %rbx          # 8-byte Reload
	je	.LBB0_301
# BB#296:                               #   in Loop: Header=BB0_156 Depth=3
	movq	FillObject.hyph_word(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_333
# BB#297:                               # %._crit_edge2433
                                        #   in Loop: Header=BB0_156 Depth=3
	movl	40(%rdi), %eax
	jmp	.LBB0_334
.LBB0_298:                              #   in Loop: Header=BB0_156 Depth=3
	shrl	$7, %ecx
	andl	$1, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_300
.LBB0_299:                              #   in Loop: Header=BB0_156 Depth=3
	movl	$1, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB0_300:                              #   in Loop: Header=BB0_156 Depth=3
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB0_301:                              #   in Loop: Header=BB0_156 Depth=3
	movq	24(%r13), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	32(%rsp), %r13d         # 4-byte Reload
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	je	.LBB0_243
.LBB0_302:                              #   in Loop: Header=BB0_156 Depth=3
	testq	%rbx, %rbx
	movq	%rbx, %rax
	movl	76(%rsp), %ebx          # 4-byte Reload
	je	.LBB0_304
# BB#303:                               #   in Loop: Header=BB0_156 Depth=3
	movl	68(%rax), %ebx
.LBB0_304:                              #   in Loop: Header=BB0_156 Depth=3
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB0_305:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_305
# BB#306:                               #   in Loop: Header=BB0_156 Depth=3
	movl	48(%rax), %r14d
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	jne	.LBB0_308
	jmp	.LBB0_315
.LBB0_243:                              #   in Loop: Header=BB0_156 Depth=3
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB0_307
# BB#244:                               #   in Loop: Header=BB0_156 Depth=3
	movl	68(%rbx), %ebx
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	jne	.LBB0_308
	jmp	.LBB0_315
.LBB0_307:                              #   in Loop: Header=BB0_156 Depth=3
	movl	72(%rsp), %ebx          # 4-byte Reload
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	je	.LBB0_315
.LBB0_308:                              #   in Loop: Header=BB0_156 Depth=3
	movq	8(%rbp), %rax
	movq	8(%rax), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB0_310
# BB#309:                               #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
	.p2align	4, 0x90
.LBB0_310:                              #   Parent Loop BB0_87 Depth=1
                                        #     Parent Loop BB0_154 Depth=2
                                        #       Parent Loop BB0_156 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addq	$16, %rbp
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_310
# BB#311:                               #   in Loop: Header=BB0_156 Depth=3
	cmpb	$1, %al
	jne	.LBB0_314
# BB#312:                               #   in Loop: Header=BB0_156 Depth=3
	movzwl	44(%rbp), %eax
	andl	$64512, %eax            # imm = 0xFC00
	cmpl	$52224, %eax            # imm = 0xCC00
	jne	.LBB0_314
# BB#313:                               #   in Loop: Header=BB0_156 Depth=3
	movzwl	46(%rbp), %eax
	movl	%r14d, %ecx
	subl	$-128, %ecx
	cmpl	$4096, %eax             # imm = 0x1000
	cmovel	%ecx, %r14d
.LBB0_314:                              # %.loopexit1766.loopexit
                                        #   in Loop: Header=BB0_156 Depth=3
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB0_315:                              # %.loopexit1766
                                        #   in Loop: Header=BB0_156 Depth=3
	testl	%ebx, %ebx
	movl	%r13d, 32(%rsp)         # 4-byte Spill
	jle	.LBB0_320
# BB#316:                               #   in Loop: Header=BB0_156 Depth=3
	movl	$1048576, %edi          # imm = 0x100000
	movl	68(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, 52(%rsp)          # 4-byte Folded Reload
	jle	.LBB0_318
# BB#317:                               #   in Loop: Header=BB0_156 Depth=3
	movb	$4, %r13b
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	jne	.LBB0_321
.LBB0_318:                              #   in Loop: Header=BB0_156 Depth=3
	movl	%ebx, %eax
	movl	32(%rsp), %edx          # 4-byte Reload
	subl	%edx, %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rcx), %ecx
	cmpl	%ecx, %eax
	jle	.LBB0_325
# BB#319:                               #   in Loop: Header=BB0_156 Depth=3
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	setne	%r13b
	shll	$9, %eax
	cltd
	idivl	%ebx
	movl	%eax, %edi
	imull	%edi, %edi
	addl	%r14d, %edi
	jns	.LBB0_323
	jmp	.LBB0_322
.LBB0_320:                              #   in Loop: Header=BB0_156 Depth=3
	xorl	%edi, %edi
	testl	%r13d, %r13d
	setne	%dil
	movl	%edi, %r13d
	addb	%r13b, %r13b
	shll	$20, %edi
.LBB0_321:                              #   in Loop: Header=BB0_156 Depth=3
	addl	%r14d, %edi
	jns	.LBB0_323
.LBB0_322:                              #   in Loop: Header=BB0_156 Depth=3
	movq	no_fpos(%rip), %r8
	movl	%edi, %ebx
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	%ebx, %edi
.LBB0_323:                              #   in Loop: Header=BB0_156 Depth=3
	movb	$7, %al
	movl	%eax, 120(%rsp)         # 4-byte Spill
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movb	15(%rsp), %al           # 1-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movb	$7, %bl
	movl	%edi, %ecx
	movl	68(%rsp), %eax          # 4-byte Reload
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	32(%rsp), %eax          # 4-byte Reload
	movl	%eax, 100(%rsp)         # 4-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 96(%rsp)          # 4-byte Spill
	jne	.LBB0_156
# BB#324:                               #   in Loop: Header=BB0_156 Depth=3
	cmpb	$2, %r13b
	movb	%r13b, %al
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movb	15(%rsp), %al           # 1-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%r13d, %ebx
	movl	%edi, %ecx
	movl	68(%rsp), %eax          # 4-byte Reload
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	32(%rsp), %eax          # 4-byte Reload
	movl	%eax, 100(%rsp)         # 4-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 96(%rsp)          # 4-byte Spill
	jne	.LBB0_156
	jmp	.LBB0_153
.LBB0_325:                              #   in Loop: Header=BB0_156 Depth=3
	movl	%edx, %ecx
	subl	%ebx, %ecx
	jle	.LBB0_330
# BB#326:                               #   in Loop: Header=BB0_156 Depth=3
	movb	$3, %r13b
	cmpl	$0, 160(%rsp)           # 4-byte Folded Reload
	je	.LBB0_321
# BB#327:                               #   in Loop: Header=BB0_156 Depth=3
	movq	BackEnd(%rip), %rdx
	movl	36(%rdx), %edx
	testl	%edx, %edx
	je	.LBB0_321
# BB#328:                               #   in Loop: Header=BB0_156 Depth=3
	shll	$2, %ecx
	cmpl	88(%rsp), %ecx          # 4-byte Folded Reload
	jg	.LBB0_321
# BB#329:                               #   in Loop: Header=BB0_156 Depth=3
	shll	$7, %eax
	cltd
	idivl	%ebx
	movl	%eax, %edi
	imull	%edi, %edi
	movb	$2, %r13b
	addl	%r14d, %edi
	jns	.LBB0_323
	jmp	.LBB0_322
.LBB0_330:                              #   in Loop: Header=BB0_156 Depth=3
	shll	$7, %eax
	cltd
	idivl	%ebx
	movl	%eax, %edi
	imull	%edi, %edi
	movb	$1, %r13b
	addl	%r14d, %edi
	jns	.LBB0_323
	jmp	.LBB0_322
.LBB0_333:                              #   in Loop: Header=BB0_156 Depth=3
	movl	$11, %edi
	movl	$.L.str.11, %esi
	movq	176(%rsp), %rdx         # 8-byte Reload
	callq	MakeWord
	movq	%rax, %rdi
	movq	%rdi, FillObject.hyph_word(%rip)
	movl	40(%rdi), %eax
	movl	%eax, %ecx
	andl	$-4096, %ecx            # imm = 0xF000
	movl	%ecx, 40(%rdi)
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	76(%rsi), %ecx
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	%edx, %ecx
	andl	$-4194304, %eax         # imm = 0xFFC00000
	orl	%ecx, %eax
	movl	%eax, 40(%rdi)
	movl	76(%rsi), %ecx
	movl	$4194304, %edx          # imm = 0x400000
	andl	%edx, %ecx
	andl	$-4198400, %eax         # imm = 0xFFBFF000
	orl	%ecx, %eax
	movl	%eax, 40(%rdi)
	movl	76(%rsi), %ecx
	shrl	%ecx
	andl	$528482304, %ecx        # imm = 0x1F800000
	andl	$-528486400, %eax       # imm = 0xE07FF000
	orl	%ecx, %eax
	movl	%eax, 40(%rdi)
	movb	68(%rsi), %cl
	andb	$3, %cl
	xorl	%edx, %edx
	cmpb	$2, %cl
	sete	%dl
	shll	$31, %edx
	andl	$2147479552, %eax       # imm = 0x7FFFF000
	orl	%edx, %eax
	movl	%eax, 40(%rdi)
.LBB0_334:                              #   in Loop: Header=BB0_156 Depth=3
	movl	%eax, %edx
	andl	$4095, %edx             # imm = 0xFFF
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	76(%rcx), %ecx
	movl	$4095, %esi             # imm = 0xFFF
	andl	%esi, %ecx
	cmpl	%ecx, %edx
	je	.LBB0_336
# BB#335:                               #   in Loop: Header=BB0_156 Depth=3
	andl	$-4096, %eax            # imm = 0xF000
	orl	%eax, %ecx
	movl	%ecx, 40(%rdi)
	movq	16(%rsp), %rsi          # 8-byte Reload
	movzwl	34(%rsi), %eax
	movw	%ax, 34(%rdi)
	movl	36(%rsi), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rdi), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rdi)
	movl	36(%rsi), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rdi)
	callq	FontWordSize
	movq	FillObject.hyph_word(%rip), %rdi
.LBB0_336:                              #   in Loop: Header=BB0_156 Depth=3
	orb	$-32, 1(%r14)
	movl	32(%rsp), %eax          # 4-byte Reload
	addl	48(%rdi), %eax
	addl	56(%rdi), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	jmp	.LBB0_301
	.p2align	4, 0x90
.LBB0_337:                              #   in Loop: Header=BB0_87 Depth=1
	cmpl	$0, 164(%rsp)           # 4-byte Folded Reload
	je	.LBB0_339
# BB#338:                               #   in Loop: Header=BB0_87 Depth=1
	cmpl	$129, %ecx
	jge	.LBB0_86
	jmp	.LBB0_339
.LBB0_342:                              #   in Loop: Header=BB0_87 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	leaq	32(%rbp), %rdx
	movl	$11, %edi
	movl	$.L.str.11, %esi
	callq	MakeWord
	movq	%rax, %rdi
	movq	%rdi, FillObject.hyph_word(%rip)
	movl	40(%rdi), %eax
	movl	%eax, %ecx
	andl	$-4096, %ecx            # imm = 0xF000
	movl	%ecx, 40(%rdi)
	leaq	76(%rbp), %rcx
	movl	76(%rbp), %edx
	movl	$4190208, %esi          # imm = 0x3FF000
	andl	%esi, %edx
	andl	$-4194304, %eax         # imm = 0xFFC00000
	orl	%edx, %eax
	movl	%eax, 40(%rdi)
	movl	76(%rbp), %edx
	movl	$4194304, %esi          # imm = 0x400000
	andl	%esi, %edx
	andl	$-4198400, %eax         # imm = 0xFFBFF000
	orl	%edx, %eax
	movl	%eax, 40(%rdi)
	movl	76(%rbp), %edx
	shrl	%edx
	andl	$528482304, %edx        # imm = 0x1F800000
	andl	$-528486400, %eax       # imm = 0xE07FF000
	orl	%edx, %eax
	movl	%eax, 40(%rdi)
	movb	68(%rbp), %dl
	andb	$3, %dl
	xorl	%esi, %esi
	cmpb	$2, %dl
	sete	%sil
	shll	$31, %esi
	andl	$2147479552, %eax       # imm = 0x7FFFF000
	orl	%esi, %eax
	movl	%eax, 40(%rdi)
.LBB0_343:                              #   in Loop: Header=BB0_87 Depth=1
	movl	%eax, %edx
	andl	$4095, %edx             # imm = 0xFFF
	movl	(%rcx), %ecx
	movl	$4095, %esi             # imm = 0xFFF
	andl	%esi, %ecx
	cmpl	%ecx, %edx
	movq	40(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_345
# BB#344:                               #   in Loop: Header=BB0_87 Depth=1
	andl	$-4096, %eax            # imm = 0xF000
	orl	%eax, %ecx
	movl	%ecx, 40(%rdi)
	movq	16(%rsp), %rsi          # 8-byte Reload
	movzwl	34(%rsi), %eax
	movw	%ax, 34(%rdi)
	movl	36(%rsi), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rdi), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rdi)
	movl	36(%rsi), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rdi)
	callq	FontWordSize
	movq	FillObject.hyph_word(%rip), %rdi
.LBB0_345:                              #   in Loop: Header=BB0_87 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	orb	$-32, 1(%rax)
	addl	48(%rdi), %r13d
	addl	56(%rdi), %r13d
	jmp	.LBB0_137
.LBB0_339:
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	je	.LBB0_346
# BB#340:
	movzbl	zz_lengths+19(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB0_349
# BB#341:
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_350
.LBB0_346:
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	je	.LBB0_491
# BB#347:
	movzbl	zz_lengths+19(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_495
# BB#348:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_496
.LBB0_349:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB0_350:
	movl	72(%rsp), %r15d         # 4-byte Reload
	movb	$19, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	andb	$-9, 43(%r12)
	movl	$0, 48(%r12)
	movl	%r15d, 56(%r12)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_354
# BB#351:
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rdx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rdx, 24(%rdx)
	movq	%rdx, 16(%rdx)
	movq	%rax, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB0_355
# BB#352:
	testq	%rax, %rax
	je	.LBB0_355
# BB#353:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%r12), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%r12)
	movq	%r12, 24(%rcx)
	jmp	.LBB0_355
.LBB0_354:                              # %.thread2474
	movq	$0, xx_tmp(%rip)
	movq	%r12, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB0_355:                              # %.lr.ph2076
	movq	%r12, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_356:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_402 Depth 2
                                        #     Child Loop BB0_405 Depth 2
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	movq	%rbp, %rbx
	testq	%r13, %r13
	je	.LBB0_358
# BB#357:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_359
	.p2align	4, 0x90
.LBB0_358:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB0_359:                              #   in Loop: Header=BB0_356 Depth=1
	movb	$17, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movq	16(%rsp), %rbp          # 8-byte Reload
	movzwl	42(%rbp), %eax
	andl	$2048, %eax             # imm = 0x800
	movzwl	42(%r13), %ecx
	andl	$63487, %ecx            # imm = 0xF7FF
	orl	%eax, %ecx
	movw	%cx, 42(%r13)
	movzwl	34(%rbp), %eax
	movw	%ax, 34(%r13)
	movl	36(%rbp), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%r13), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r13)
	movl	36(%rbp), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r13)
	movzwl	64(%rbp), %ecx
	andl	$128, %ecx
	movzwl	64(%r13), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 64(%r13)
	movzwl	64(%rbp), %ecx
	andl	$256, %ecx              # imm = 0x100
	movl	%eax, %edx
	andl	$-257, %edx             # imm = 0xFEFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	64(%rbp), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %edx             # imm = 0xFDFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	64(%rbp), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %edx            # imm = 0xE3FF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	64(%rbp), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %edx             # imm = 0x1FFF
	orl	%ecx, %edx
	movw	%dx, 64(%r13)
	movzwl	66(%rbp), %ecx
	movw	%cx, 66(%r13)
	movb	68(%rbp), %cl
	andb	$3, %cl
	movb	68(%r13), %dl
	andb	$-4, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	68(%rbp), %cl
	andb	$12, %cl
	andb	$-13, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	68(%rbp), %cl
	andb	$112, %cl
	andb	$-113, %dl
	orb	%cl, %dl
	movb	%dl, 68(%r13)
	movb	64(%rbp), %cl
	andb	$8, %cl
	andb	$-9, %al
	orb	%cl, %al
	movb	%al, 64(%r13)
	movzwl	68(%rbp), %edx
	andl	$128, %edx
	movzwl	68(%r13), %ecx
	andl	$65407, %ecx            # imm = 0xFF7F
	orl	%edx, %ecx
	movw	%cx, 68(%r13)
	movzwl	68(%rbp), %edx
	andl	$256, %edx              # imm = 0x100
	movl	%ecx, %esi
	andl	$-257, %esi             # imm = 0xFEFF
	orl	%edx, %esi
	movw	%si, 68(%r13)
	movzwl	68(%rbp), %edx
	andl	$512, %edx              # imm = 0x200
	andl	$-513, %esi             # imm = 0xFDFF
	orl	%edx, %esi
	movw	%si, 68(%r13)
	movzwl	68(%rbp), %edx
	andl	$7168, %edx             # imm = 0x1C00
	andl	$-7169, %esi            # imm = 0xE3FF
	orl	%edx, %esi
	movw	%si, 68(%r13)
	movzwl	68(%rbp), %edx
	andl	$57344, %edx            # imm = 0xE000
	andl	$8191, %esi             # imm = 0x1FFF
	orl	%edx, %esi
	movw	%si, 68(%r13)
	movzwl	70(%rbp), %edx
	movw	%dx, 70(%r13)
	movl	76(%rbp), %esi
	movl	$4095, %edx             # imm = 0xFFF
	andl	%edx, %esi
	movl	76(%r13), %edx
	movl	$-4096, %edi            # imm = 0xF000
	andl	%edi, %edx
	orl	%esi, %edx
	movl	%edx, 76(%r13)
	movl	76(%rbp), %esi
	movl	$4190208, %edi          # imm = 0x3FF000
	andl	%edi, %esi
	andl	$-4190209, %edx         # imm = 0xFFC00FFF
	orl	%esi, %edx
	movl	%edx, 76(%r13)
	movl	76(%rbp), %esi
	movl	$12582912, %edi         # imm = 0xC00000
	andl	%edi, %esi
	andl	$-12582913, %edx        # imm = 0xFF3FFFFF
	orl	%esi, %edx
	movl	%edx, 76(%r13)
	movl	76(%rbp), %esi
	movl	$1056964608, %edi       # imm = 0x3F000000
	andl	%edi, %esi
	andl	$-1056964609, %edx      # imm = 0xC0FFFFFF
	orl	%esi, %edx
	movl	%edx, 76(%r13)
	movl	76(%rbp), %esi
	movl	$-2147483648, %edi      # imm = 0x80000000
	andl	%edi, %esi
	andl	$2147483647, %edx       # imm = 0x7FFFFFFF
	orl	%esi, %edx
	movl	%edx, 76(%r13)
	movl	76(%rbp), %esi
	movl	$1073741824, %edi       # imm = 0x40000000
	andl	%edi, %esi
	andl	$-1073741825, %edx      # imm = 0xBFFFFFFF
	orl	%esi, %edx
	movl	%edx, 76(%r13)
	movb	64(%rbp), %dl
	andb	$1, %dl
	andb	$-2, %al
	orb	%dl, %al
	movb	%al, 64(%r13)
	movb	64(%rbp), %dl
	andb	$2, %dl
	andb	$-3, %al
	orb	%dl, %al
	movb	%al, 64(%r13)
	movb	64(%rbp), %dl
	andb	$4, %dl
	andb	$-5, %al
	orb	%dl, %al
	movb	%al, 64(%r13)
	movb	64(%rbp), %dl
	andb	$112, %dl
	andb	$-113, %al
	orb	%dl, %al
	movb	%al, 64(%r13)
	movzwl	72(%rbp), %eax
	movw	%ax, 72(%r13)
	movzwl	74(%rbp), %eax
	movw	%ax, 74(%r13)
	cmpq	%r12, 8(%r12)
	je	.LBB0_362
# BB#360:                               #   in Loop: Header=BB0_356 Depth=1
	movl	%ecx, %eax
	shrb	$4, %al
	andb	$7, %al
	decb	%al
	cmpb	$1, %al
	ja	.LBB0_362
# BB#361:                               #   in Loop: Header=BB0_356 Depth=1
	orb	$112, %cl
	movb	%cl, 68(%r13)
.LBB0_362:                              #   in Loop: Header=BB0_356 Depth=1
	movl	$0, 48(%r13)
	movl	%r15d, 56(%r13)
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	68(%rax), %al
	andb	$96, %al
	cmpb	$32, %al
	jne	.LBB0_394
# BB#363:                               #   in Loop: Header=BB0_356 Depth=1
	movl	$11, %edi
	movl	$.L.str.4, %esi
	movq	176(%rsp), %rdx         # 8-byte Reload
	callq	MakeWord
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r12)
	movl	$536870912, 40(%r12)    # imm = 0x20000000
	movzbl	zz_lengths+26(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB0_365
# BB#364:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_366
.LBB0_365:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB0_366:                              #   in Loop: Header=BB0_356 Depth=1
	movb	$26, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movl	$8388607, 64(%r14)      # imm = 0x7FFFFF
	movl	156(%rsp), %eax         # 4-byte Reload
	movl	%eax, 68(%r14)
	movl	$8388607, 72(%r14)      # imm = 0x7FFFFF
	movl	$0, 48(%r14)
	movl	%eax, 56(%r14)
	movl	40(%r14), %eax
	movl	$-1610612737, %ecx      # imm = 0x9FFFFFFF
	andl	%ecx, %eax
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_368
# BB#367:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_369
.LBB0_368:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_369:                              #   in Loop: Header=BB0_356 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB0_372
# BB#370:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_372
# BB#371:                               #   in Loop: Header=BB0_356 Depth=1
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_372:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB0_375
# BB#373:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_375
# BB#374:                               #   in Loop: Header=BB0_356 Depth=1
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_375:                              #   in Loop: Header=BB0_356 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_377
# BB#376:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_378
.LBB0_377:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_378:                              #   in Loop: Header=BB0_356 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_380
# BB#379:                               #   in Loop: Header=BB0_356 Depth=1
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_380:                              #   in Loop: Header=BB0_356 Depth=1
	testq	%r14, %r14
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB0_383
# BB#381:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_383
# BB#382:                               #   in Loop: Header=BB0_356 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_383:                              #   in Loop: Header=BB0_356 Depth=1
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_385
# BB#384:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_386
.LBB0_385:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB0_386:                              #   in Loop: Header=BB0_356 Depth=1
	movb	$1, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movw	$0, 41(%rbp)
	movzwl	44(%rbp), %eax
	andl	$127, %eax
	orl	$9856, %eax             # imm = 0x2680
	movw	%ax, 44(%rbp)
	movw	$0, 46(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_388
# BB#387:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_389
.LBB0_388:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_389:                              #   in Loop: Header=BB0_356 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_391
# BB#390:                               #   in Loop: Header=BB0_356 Depth=1
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_391:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_394
# BB#392:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_394
# BB#393:                               #   in Loop: Header=BB0_356 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_394:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rbx, %rbp
	movq	8(%rbp), %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	je	.LBB0_401
# BB#395:                               #   in Loop: Header=BB0_356 Depth=1
	cmpb	$0, 32(%rbx)
	je	.LBB0_397
# BB#396:                               #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_397:                              #   in Loop: Header=BB0_356 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	je	.LBB0_399
# BB#398:                               #   in Loop: Header=BB0_356 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_399:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rbx, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB0_401
# BB#400:                               #   in Loop: Header=BB0_356 Depth=1
	movq	(%r13), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbx), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_401:                              # %.preheader1755.preheader
                                        #   in Loop: Header=BB0_356 Depth=1
	leaq	32(%r13), %r12
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_402:                              # %.preheader1755
                                        #   Parent Loop BB0_356 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_402
# BB#403:                               #   in Loop: Header=BB0_356 Depth=1
	movzwl	44(%rbx), %eax
	cmpl	$57344, %eax            # imm = 0xE000
	jb	.LBB0_427
# BB#404:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB0_405:                              #   Parent Loop BB0_356 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rax), %rax
	cmpb	$0, 32(%rax)
	je	.LBB0_405
# BB#406:                               #   in Loop: Header=BB0_356 Depth=1
	movl	40(%rax), %r15d
	movl	$1610612736, %eax       # imm = 0x60000000
	andl	%eax, %r15d
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_408
# BB#407:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_409
.LBB0_408:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB0_409:                              #   in Loop: Header=BB0_356 Depth=1
	movb	$1, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movw	$0, 41(%rbp)
	movl	40(%rbp), %eax
	movl	$-1610612737, %ecx      # imm = 0x9FFFFFFF
	andl	%ecx, %eax
	orl	%r15d, %eax
	movl	%eax, 40(%rbp)
	movzwl	44(%rbp), %eax
	andl	$127, %eax
	orl	$9856, %eax             # imm = 0x2680
	movw	%ax, 44(%rbp)
	movw	$0, 46(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_411
# BB#410:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_412
.LBB0_411:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_412:                              #   in Loop: Header=BB0_356 Depth=1
	testq	%r14, %r14
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB0_415
# BB#413:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_415
# BB#414:                               #   in Loop: Header=BB0_356 Depth=1
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_415:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_418
# BB#416:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_418
# BB#417:                               #   in Loop: Header=BB0_356 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_418:                              #   in Loop: Header=BB0_356 Depth=1
	movl	$11, %edi
	movl	$.L.str.11, %esi
	movq	%r12, %rdx
	callq	MakeWord
	movq	%rax, %r14
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	76(%rdx), %ecx
	movl	$4095, %eax             # imm = 0xFFF
	andl	%eax, %ecx
	movl	40(%r14), %eax
	movl	$-4096, %esi            # imm = 0xF000
	andl	%esi, %eax
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	76(%rdx), %ecx
	movl	$4190208, %esi          # imm = 0x3FF000
	andl	%esi, %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	76(%rdx), %ecx
	movl	$4194304, %esi          # imm = 0x400000
	andl	%esi, %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movl	76(%rdx), %ecx
	shrl	%ecx
	andl	$528482304, %ecx        # imm = 0x1F800000
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%r14)
	movb	68(%rdx), %cl
	andb	$3, %cl
	xorl	%edx, %edx
	cmpb	$2, %cl
	sete	%dl
	shll	$31, %edx
	andl	$536870911, %eax        # imm = 0x1FFFFFFF
	orl	%r15d, %eax
	orl	%edx, %eax
	movl	%eax, 40(%r14)
	movq	%r14, %rdi
	callq	FontWordSize
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_420
# BB#419:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_421
.LBB0_420:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_421:                              #   in Loop: Header=BB0_356 Depth=1
	movl	72(%rsp), %r15d         # 4-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	je	.LBB0_424
# BB#422:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_424
# BB#423:                               #   in Loop: Header=BB0_356 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_424:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB0_427
# BB#425:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_427
# BB#426:                               #   in Loop: Header=BB0_356 Depth=1
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_427:                              #   in Loop: Header=BB0_356 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_429
# BB#428:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_430
	.p2align	4, 0x90
.LBB0_429:                              #   in Loop: Header=BB0_356 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_430:                              #   in Loop: Header=BB0_356 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r12), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_433
# BB#431:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_433
# BB#432:                               #   in Loop: Header=BB0_356 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_433:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB0_436
# BB#434:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_436
# BB#435:                               #   in Loop: Header=BB0_356 Depth=1
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_436:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rbp, xx_link(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB0_438
# BB#437:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rbp
.LBB0_438:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rbp, zz_res(%rip)
	movq	8(%r12), %rax
	movq	%rax, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_441
# BB#439:                               #   in Loop: Header=BB0_356 Depth=1
	testq	%rax, %rax
	je	.LBB0_441
# BB#440:                               #   in Loop: Header=BB0_356 Depth=1
	movq	(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_441:                              #   in Loop: Header=BB0_356 Depth=1
	movw	$256, 41(%rbx)          # imm = 0x100
	movq	16(%rsp), %rdx          # 8-byte Reload
	movzwl	64(%rdx), %ecx
	andl	$128, %ecx
	movzwl	44(%rbx), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	64(%rdx), %ecx
	andl	$256, %ecx              # imm = 0x100
	andl	$-257, %eax             # imm = 0xFEFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	64(%rdx), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %eax             # imm = 0xFDFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	64(%rdx), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %eax            # imm = 0xE3FF
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	64(%rdx), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %eax             # imm = 0x1FFF
	orl	%ecx, %eax
	movw	%ax, 44(%rbx)
	movzwl	66(%rdx), %eax
	movw	%ax, 46(%rbx)
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB0_449
# BB#442:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_444
# BB#443:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_445
	.p2align	4, 0x90
.LBB0_444:                              #   in Loop: Header=BB0_356 Depth=1
	xorl	%ecx, %ecx
.LBB0_445:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_447
# BB#446:                               #   in Loop: Header=BB0_356 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_447:                              #   in Loop: Header=BB0_356 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_449
# BB#448:                               #   in Loop: Header=BB0_356 Depth=1
	callq	DisposeObject
.LBB0_449:                              #   in Loop: Header=BB0_356 Depth=1
	movq	56(%rbx), %rbp
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB0_356
# BB#450:                               # %._crit_edge2077
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_452
# BB#451:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_453
.LBB0_452:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_453:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	8(%r12), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_456
# BB#454:
	testq	%rcx, %rcx
	je	.LBB0_456
# BB#455:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_456:
	movq	%rax, zz_res(%rip)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB0_459
# BB#457:
	testq	%rax, %rax
	je	.LBB0_459
# BB#458:
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_459:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$0, 48(%rax)
	movl	%r15d, 56(%rax)
	movb	68(%rax), %al
	movl	%eax, %ecx
	shrb	$4, %cl
	andb	$7, %cl
	decb	%cl
	cmpb	$1, %cl
	ja	.LBB0_461
# BB#460:
	orb	$112, %al
	movq	16(%rsp), %rcx          # 8-byte Reload
	movb	%al, 68(%rcx)
.LBB0_461:
	movq	(%r12), %rcx
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB0_462:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB0_462
# BB#463:
	movq	(%rbx), %rax
	cmpq	%rax, 8(%rbx)
	je	.LBB0_472
# BB#464:
	leaq	16(%rax), %rcx
	jmp	.LBB0_466
	.p2align	4, 0x90
.LBB0_465:                              #   in Loop: Header=BB0_466 Depth=1
	addq	$16, %rcx
.LBB0_466:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB0_465
# BB#467:
	cmpb	$11, %dl
	jne	.LBB0_469
# BB#468:
	cmpb	$0, 64(%rcx)
	je	.LBB0_470
.LBB0_469:                              # %.loopexit1754
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%rbx), %rax
.LBB0_470:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_474
# BB#471:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_475
.LBB0_472:
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_500
# BB#473:
	movq	%rax, zz_res(%rip)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rcx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
	jmp	.LBB0_501
.LBB0_474:
	xorl	%ecx, %ecx
.LBB0_475:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_477
# BB#476:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_477:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_479
# BB#478:
	callq	DisposeObject
.LBB0_479:
	movq	(%rbx), %rax
	leaq	16(%rax), %rbp
	jmp	.LBB0_481
	.p2align	4, 0x90
.LBB0_480:                              #   in Loop: Header=BB0_481 Depth=1
	addq	$16, %rbp
.LBB0_481:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %ecx
	testb	%cl, %cl
	je	.LBB0_480
# BB#482:
	cmpb	$1, %cl
	je	.LBB0_484
# BB#483:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%rbx), %rax
.LBB0_484:                              # %.loopexit1753
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_486
# BB#485:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_487
.LBB0_486:
	xorl	%ecx, %ecx
.LBB0_487:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_489
# BB#488:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_489:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_517
# BB#490:
	callq	DisposeObject
	jmp	.LBB0_517
.LBB0_491:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	$0, 48(%rcx)
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, 56(%rcx)
	jmp	.LBB0_557
.LBB0_492:                              # %.thread2470
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB0_493:
	movq	%r14, %rdi
	callq	DisposeObject
	jmp	.LBB0_494
.LBB0_495:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_496:
	movb	$19, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	andb	$-9, 43(%rbx)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_509
# BB#497:
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rdx), %rcx
	movq	%rax, 24(%rcx)
	movq	%rdx, 24(%rdx)
	movq	%rdx, 16(%rdx)
	movq	%rax, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
	je	.LBB0_510
# BB#498:
	testq	%rax, %rax
	je	.LBB0_510
# BB#499:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbx)
	movq	%rbx, 24(%rcx)
	jmp	.LBB0_510
.LBB0_500:
	xorl	%eax, %eax
.LBB0_501:
	movq	%rax, xx_tmp(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_503
# BB#502:
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
.LBB0_503:
	movq	%rcx, %rax
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rax), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	addq	$33, %rcx
	cmpb	$2, %dl
	cmovbq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_505
# BB#504:
	callq	DisposeObject
.LBB0_505:
	movq	(%r12), %rax
	cmpq	%rax, 8(%r12)
	jne	.LBB0_507
# BB#506:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r12), %rax
.LBB0_507:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_512
# BB#508:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_513
.LBB0_509:                              # %.thread2472
	movq	$0, xx_tmp(%rip)
	movq	%rbx, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbx, %rbx
	sete	%bpl
.LBB0_510:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_614
# BB#511:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_615
.LBB0_512:
	xorl	%ecx, %ecx
.LBB0_513:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_515
# BB#514:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_515:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
                                        # implicit-def: %RBP
	jne	.LBB0_517
# BB#516:
	callq	DisposeObject
                                        # implicit-def: %RBP
.LBB0_517:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	76(%rax), %eax
	testl	%eax, %eax
	jns	.LBB0_524
# BB#518:
	movq	8(%r12), %rcx
	cmpq	(%r12), %rcx
	je	.LBB0_524
# BB#519:
	movq	8(%rcx), %rbx
	.p2align	4, 0x90
.LBB0_520:                              # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_520
# BB#521:
	cmpb	$1, %al
	je	.LBB0_523
# BB#522:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_523:                              # %.loopexit1752
	orb	$-128, 44(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	76(%rax), %eax
.LBB0_524:
	testl	$1073741824, %eax       # imm = 0x40000000
	je	.LBB0_531
# BB#525:
	movq	(%r12), %rax
	cmpq	%rax, 8(%r12)
	je	.LBB0_531
# BB#526:
	movq	(%rax), %rbx
	.p2align	4, 0x90
.LBB0_527:                              # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_527
# BB#528:
	cmpb	$1, %al
	je	.LBB0_530
# BB#529:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.26, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_530:                              # %.loopexit1751
	orb	$-128, 44(%rbx)
.LBB0_531:
	movq	(%r12), %rax
	cmpq	%r12, %rax
	jne	.LBB0_533
# BB#532:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.27, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r12), %rax
.LBB0_533:
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_534:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax), %r13
	leaq	16(%r13), %rax
	cmpb	$0, 32(%r13)
	je	.LBB0_534
# BB#535:                               # %.critedge123.preheader
	movq	8(%r13), %rbx
	cmpq	%r13, %rbx
	je	.LBB0_536
	.p2align	4, 0x90
.LBB0_539:                              # %.preheader1750
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_540 Depth 2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_540:                              #   Parent Loop BB0_539 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_540
# BB#541:                               #   in Loop: Header=BB0_539 Depth=1
	cmpb	$9, %al
	je	.LBB0_537
# BB#542:                               #   in Loop: Header=BB0_539 Depth=1
	cmpb	$1, %al
	je	.LBB0_538
# BB#543:                               #   in Loop: Header=BB0_539 Depth=1
	addb	$-9, %al
	cmpb	$91, %al
	jae	.LBB0_538
	jmp	.LBB0_544
	.p2align	4, 0x90
.LBB0_537:                              #   in Loop: Header=BB0_539 Depth=1
	movq	%rbp, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	jne	.LBB0_544
.LBB0_538:                              # %.critedge123.outer
                                        #   in Loop: Header=BB0_539 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r13, %rbx
	jne	.LBB0_539
.LBB0_536:                              # %.thread1669
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.28, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%r13, %rbx
.LBB0_544:                              # %.loopexit
	movl	48(%rbp), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	8(%rbx), %r14
	cmpq	%r13, %r14
	je	.LBB0_553
# BB#545:                               # %.preheader1748.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_546:                              # %.preheader1748
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_547 Depth 2
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB0_547:                              #   Parent Loop BB0_546 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_547
# BB#548:                               #   in Loop: Header=BB0_546 Depth=1
	cmpb	$9, %al
	je	.LBB0_596
# BB#549:                               #   in Loop: Header=BB0_546 Depth=1
	cmpb	$1, %al
	jne	.LBB0_551
# BB#550:                               # %.critedge124.outer
                                        #   in Loop: Header=BB0_546 Depth=1
	movq	8(%r14), %r14
	cmpq	%r13, %r14
	movq	%rbx, %r12
	jne	.LBB0_546
	jmp	.LBB0_553
	.p2align	4, 0x90
.LBB0_596:                              #   in Loop: Header=BB0_546 Depth=1
	movq	%rbx, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB0_552
	jmp	.LBB0_597
	.p2align	4, 0x90
.LBB0_551:                              #   in Loop: Header=BB0_546 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_597
.LBB0_552:                              # %.critedge124.backedge
                                        #   in Loop: Header=BB0_546 Depth=1
	movq	8(%r14), %r14
	cmpq	%r13, %r14
	jne	.LBB0_546
.LBB0_553:                              # %.preheader1746.thread
	movl	56(%rbp), %edi
.LBB0_554:                              # %._crit_edge
	movl	16(%rsp), %ecx          # 4-byte Reload
	addl	%edi, %ecx
	cmpl	$8388608, %ecx          # imm = 0x800000
	movl	$8388607, %eax          # imm = 0x7FFFFF
	cmovll	%ecx, %eax
	movl	%eax, 56(%r13)
	addl	48(%r13), %eax
	cmpl	%r15d, %eax
	jle	.LBB0_556
# BB#555:
	orb	$112, 68(%r13)
.LBB0_556:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB0_557
.LBB0_597:
	testq	%r12, %r12
	jne	.LBB0_599
# BB#598:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_599:                              # %.preheader1746
	cmpq	%r13, %r14
	movl	56(%rbp), %edi
	je	.LBB0_554
.LBB0_600:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_602 Depth 2
                                        #       Child Loop BB0_603 Depth 3
	movl	16(%rsp), %ebp          # 4-byte Reload
	movl	48(%rbx), %esi
	movl	56(%rbx), %edx
	addq	$44, %r12
	movq	%r12, %rcx
	callq	MinGap
	addl	%ebp, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	8(%r14), %r14
	cmpq	%r13, %r14
	je	.LBB0_607
# BB#601:                               # %.preheader1744.lr.ph.preheader
                                        #   in Loop: Header=BB0_600 Depth=1
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_602:                              # %.preheader1744
                                        #   Parent Loop BB0_600 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_603 Depth 3
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB0_603:                              #   Parent Loop BB0_600 Depth=1
                                        #     Parent Loop BB0_602 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_603
# BB#604:                               #   in Loop: Header=BB0_602 Depth=2
	cmpb	$9, %al
	je	.LBB0_610
# BB#605:                               #   in Loop: Header=BB0_602 Depth=2
	cmpb	$1, %al
	jne	.LBB0_608
# BB#606:                               # %.critedge125.outer
                                        #   in Loop: Header=BB0_602 Depth=2
	movq	8(%r14), %r14
	cmpq	%r13, %r14
	movq	%rbp, %r15
	movq	%rbp, %r12
	jne	.LBB0_602
	jmp	.LBB0_607
	.p2align	4, 0x90
.LBB0_610:                              #   in Loop: Header=BB0_602 Depth=2
	movq	%rbp, %rdi
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB0_609
	jmp	.LBB0_611
	.p2align	4, 0x90
.LBB0_608:                              #   in Loop: Header=BB0_602 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_611
.LBB0_609:                              # %.critedge125.backedge
                                        #   in Loop: Header=BB0_602 Depth=2
	movq	8(%r14), %r14
	cmpq	%r13, %r14
	jne	.LBB0_602
	jmp	.LBB0_607
.LBB0_611:                              #   in Loop: Header=BB0_600 Depth=1
	testq	%r15, %r15
	jne	.LBB0_613
# BB#612:                               #   in Loop: Header=BB0_600 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_613:                              # %.backedge1747
                                        #   in Loop: Header=BB0_600 Depth=1
	movl	56(%rbx), %edi
	cmpq	%r13, %r14
	movq	%rbp, %rbx
	movl	72(%rsp), %r15d         # 4-byte Reload
	jne	.LBB0_600
	jmp	.LBB0_554
.LBB0_607:                              # %.backedge1747.thread
	movl	56(%rbx), %edi
	movl	72(%rsp), %r15d         # 4-byte Reload
	jmp	.LBB0_554
.LBB0_614:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_615:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %bpl
	jne	.LBB0_617
# BB#616:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_617:
	movq	%rax, zz_res(%rip)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_619
# BB#618:
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_619:
	movq	%rbx, 16(%rsp)          # 8-byte Spill
.LBB0_557:
	movq	240(%rsp), %rax
	cmpl	$0, (%rax)
	je	.LBB0_595
# BB#558:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpb	$19, 32(%rax)
	jne	.LBB0_595
# BB#559:                               # %.preheader1741
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r15
	cmpq	%rax, %r15
	je	.LBB0_595
	.p2align	4, 0x90
.LBB0_560:                              # %.preheader1739
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_561 Depth 2
                                        #     Child Loop BB0_565 Depth 2
                                        #       Child Loop BB0_566 Depth 3
                                        #       Child Loop BB0_571 Depth 3
                                        #       Child Loop BB0_573 Depth 3
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB0_561:                              #   Parent Loop BB0_560 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	testb	%al, %al
	je	.LBB0_561
# BB#562:                               #   in Loop: Header=BB0_560 Depth=1
	cmpb	$17, %al
	jne	.LBB0_594
# BB#563:                               # %.preheader1737
                                        #   in Loop: Header=BB0_560 Depth=1
	movq	8(%rbp), %r12
	cmpq	%rbp, %r12
	je	.LBB0_594
	.p2align	4, 0x90
.LBB0_565:                              # %.preheader
                                        #   Parent Loop BB0_560 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_566 Depth 3
                                        #       Child Loop BB0_571 Depth 3
                                        #       Child Loop BB0_573 Depth 3
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB0_566:                              #   Parent Loop BB0_560 Depth=1
                                        #     Parent Loop BB0_565 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB0_566
# BB#567:                               #   in Loop: Header=BB0_565 Depth=2
	cmpb	$1, %cl
	jne	.LBB0_564
# BB#568:                               #   in Loop: Header=BB0_565 Depth=2
	cmpw	$0, 46(%rax)
	jne	.LBB0_564
# BB#569:                               #   in Loop: Header=BB0_565 Depth=2
	movzwl	44(%rax), %eax
	cmpl	$57344, %eax            # imm = 0xE000
	jb	.LBB0_564
# BB#570:                               #   in Loop: Header=BB0_565 Depth=2
	movq	(%r12), %r14
	.p2align	4, 0x90
.LBB0_571:                              #   Parent Loop BB0_560 Depth=1
                                        #     Parent Loop BB0_565 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r14), %r14
	movzbl	32(%r14), %eax
	testb	%al, %al
	je	.LBB0_571
# BB#572:                               #   in Loop: Header=BB0_565 Depth=2
	leaq	32(%r14), %rcx
	movq	8(%r12), %r13
	.p2align	4, 0x90
.LBB0_573:                              #   Parent Loop BB0_560 Depth=1
                                        #     Parent Loop BB0_565 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r13), %r13
	movzbl	32(%r13), %ebx
	testb	%bl, %bl
	je	.LBB0_573
# BB#574:                               #   in Loop: Header=BB0_565 Depth=2
	movl	%eax, %esi
	addb	$-11, %sil
	movl	%ebx, %edx
	addb	$-11, %dl
	orb	%sil, %dl
	cmpb	$1, %dl
	ja	.LBB0_564
# BB#575:                               #   in Loop: Header=BB0_565 Depth=2
	movl	40(%r13), %edx
	xorl	40(%r14), %edx
	testl	$2147483647, %edx       # imm = 0x7FFFFFFF
	jne	.LBB0_564
# BB#576:                               #   in Loop: Header=BB0_565 Depth=2
	xorl	%edi, %edi
	cmpb	$12, %al
	sete	%dil
	addl	$11, %edi
	cmpb	$12, %bl
	movl	$12, %eax
	cmovel	%eax, %edi
	leaq	64(%r14), %rsi
	leaq	64(%r13), %rdx
	callq	MakeWordTwo
	movq	%rax, %rbx
	movl	40(%r14), %ecx
	movl	$4095, %eax             # imm = 0xFFF
	andl	%eax, %ecx
	movl	40(%rbx), %eax
	movl	$-4096, %edx            # imm = 0xF000
	andl	%edx, %eax
	orl	%ecx, %eax
	movl	%eax, 40(%rbx)
	movl	40(%r14), %ecx
	movl	$4190208, %edx          # imm = 0x3FF000
	andl	%edx, %ecx
	andl	$-4190209, %eax         # imm = 0xFFC00FFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbx)
	movl	40(%r14), %ecx
	movl	$4194304, %edx          # imm = 0x400000
	andl	%edx, %ecx
	andl	$-4194305, %eax         # imm = 0xFFBFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbx)
	movl	40(%r14), %ecx
	movl	$528482304, %edx        # imm = 0x1F800000
	andl	%edx, %ecx
	andl	$-528482305, %eax       # imm = 0xE07FFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbx)
	movl	40(%r14), %ecx
	movl	$-2147483648, %edx      # imm = 0x80000000
	andl	%edx, %ecx
	andl	$2147483647, %eax       # imm = 0x7FFFFFFF
	orl	%ecx, %eax
	movl	%eax, 40(%rbx)
	movq	%rbx, %rdi
	callq	FontWordSize
	movl	40(%r14), %eax
	movl	$1610612736, %ecx       # imm = 0x60000000
	andl	%ecx, %eax
	movl	40(%rbx), %ecx
	movl	$-1610612737, %edx      # imm = 0x9FFFFFFF
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 40(%rbx)
	movq	%r12, xx_link(%rip)
	movq	%r12, zz_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB0_578
# BB#577:                               #   in Loop: Header=BB0_565 Depth=2
	movq	%rax, zz_res(%rip)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r12), %rcx
	movq	%rax, 24(%rcx)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
.LBB0_578:                              #   in Loop: Header=BB0_565 Depth=2
	movq	%r12, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_580
# BB#579:                               #   in Loop: Header=BB0_565 Depth=2
	movq	16(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rbx)
	movq	16(%r12), %rcx
	movq	%rbx, 24(%rcx)
	movq	%rax, 16(%r12)
	movq	%r12, 24(%rax)
.LBB0_580:                              #   in Loop: Header=BB0_565 Depth=2
	movq	24(%r14), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_582
# BB#581:                               #   in Loop: Header=BB0_565 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_583
.LBB0_582:                              #   in Loop: Header=BB0_565 Depth=2
	xorl	%ecx, %ecx
.LBB0_583:                              #   in Loop: Header=BB0_565 Depth=2
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_585
# BB#584:                               #   in Loop: Header=BB0_565 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_585:                              #   in Loop: Header=BB0_565 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_587
# BB#586:                               #   in Loop: Header=BB0_565 Depth=2
	callq	DisposeObject
.LBB0_587:                              #   in Loop: Header=BB0_565 Depth=2
	movq	24(%r13), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_589
# BB#588:                               #   in Loop: Header=BB0_565 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB0_590
.LBB0_589:                              #   in Loop: Header=BB0_565 Depth=2
	xorl	%ecx, %ecx
.LBB0_590:                              #   in Loop: Header=BB0_565 Depth=2
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_592
# BB#591:                               #   in Loop: Header=BB0_565 Depth=2
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_592:                              #   in Loop: Header=BB0_565 Depth=2
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	je	.LBB0_593
	.p2align	4, 0x90
.LBB0_564:                              # %.backedge
                                        #   in Loop: Header=BB0_565 Depth=2
	movq	8(%r12), %r12
	cmpq	%rbp, %r12
	jne	.LBB0_565
	jmp	.LBB0_594
.LBB0_593:                              #   in Loop: Header=BB0_565 Depth=2
	callq	DisposeObject
	movq	8(%r12), %r12
	cmpq	%rbp, %r12
	jne	.LBB0_565
	.p2align	4, 0x90
.LBB0_594:                              # %.backedge1743
                                        #   in Loop: Header=BB0_560 Depth=1
	movq	8(%r15), %r15
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	jne	.LBB0_560
.LBB0_595:
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_494:                              # %.loopexit1742
	movq	%rbx, %rax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	FillObject, .Lfunc_end0-FillObject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_194
	.quad	.LBB0_158
	.quad	.LBB0_158
	.quad	.LBB0_158
	.quad	.LBB0_161
	.quad	.LBB0_337
	.quad	.LBB0_161
	.quad	.LBB0_202
	.quad	.LBB0_195

	.type	FillObject.hyph_word,@object # @FillObject.hyph_word
	.local	FillObject.hyph_word
	.comm	FillObject.hyph_word,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"FillObject: type(x) != ACAT!"
	.size	.L.str.1, 29

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"FillObject: initial size!"
	.size	.L.str.2, 26

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"paragraph deleted (assigned width %s is too narrow)"
	.size	.L.str.3, 52

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.zero	1
	.size	.L.str.4, 1

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"1rt"
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"FillObject/extend_unbreakable:  link == x!"
	.size	.L.str.7, 43

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"NextDefiniteWithGap: g == nilobj!"
	.size	.L.str.8, 34

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"hyphen or nohyphen option missing"
	.size	.L.str.9, 34

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"MoveRightToGap: newg!"
	.size	.L.str.10, 22

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"-"
	.size	.L.str.11, 2

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"SIB: glink!"
	.size	.L.str.12, 12

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"SetIntervalBadness: badness < 0!"
	.size	.L.str.13, 33

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"IntervalShiftRightEnd: AT_END!"
	.size	.L.str.14, 31

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"IntervalShiftRightEnd: type(g)!"
	.size	.L.str.15, 32

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"IntervalShiftRightEnd: rlink == x!"
	.size	.L.str.16, 35

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"IntervalShiftLeftEnd: llink == x!"
	.size	.L.str.18, 34

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"IntervalShiftLeftEnd: tab_count <= 0!"
	.size	.L.str.19, 38

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"FillObject: IntervalClass(I)"
	.size	.L.str.20, 29

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"TransferLinks: start_link!"
	.size	.L.str.21, 27

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"almost empty paragraph!"
	.size	.L.str.22, 24

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"FillObject: last word!"
	.size	.L.str.23, 23

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"FillObject: last gap_obj!"
	.size	.L.str.24, 26

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"FillObject: type(gp) != GAP_OBJ (a)!"
	.size	.L.str.25, 37

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"FillObject: type(gp) != GAP_OBJ (b)!"
	.size	.L.str.26, 37

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"FillObject: empty paragraph!"
	.size	.L.str.27, 29

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"FillObject: last line is empty!"
	.size	.L.str.28, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
