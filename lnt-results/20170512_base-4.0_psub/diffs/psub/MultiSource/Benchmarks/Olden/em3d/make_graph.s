	.text
	.file	"make_graph.bc"
	.globl	make_table
	.p2align	4, 0x90
	.type	make_table,@function
make_table:                             # @make_table
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%edi, %rdi
	shlq	$3, %rdi
	callq	malloc
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	popq	%rcx
	retq
.LBB0_2:
	movl	$.Lstr, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	make_table, .Lfunc_end0-make_table
	.cfi_endproc

	.globl	fill_table
	.p2align	4, 0x90
	.type	fill_table,@function
fill_table:                             # @fill_table
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 80
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, (%rbp)
	callq	gen_uniform_double
	movsd	%xmm0, (%r15)
	movq	%r15, (%rbx)
	movl	$0, 40(%rbx)
	movslq	%r14d, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %ebp
	movl	$8, %r13d
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r14
	leaq	(%r15,%r13), %r12
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	callq	gen_uniform_double
	movsd	%xmm0, (%r15,%rbp,8)
	movq	%r12, (%rbx)
	movl	$0, 40(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rbx, (%rax,%rbp,8)
	movq	%rbx, 8(%r14)
	incq	%rbp
	addq	$8, %r13
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jl	.LBB1_1
# BB#2:                                 # %._crit_edge
	movq	$0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	fill_table, .Lfunc_end1-fill_table
	.cfi_endproc

	.globl	make_neighbors
	.p2align	4, 0x90
	.type	make_neighbors,@function
make_neighbors:                         # @make_neighbors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 80
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, %r12d
	movl	%edx, %r13d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB2_22
# BB#1:                                 # %.lr.ph67
	movslq	%ecx, %rcx
	leaq	(,%rcx,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB2_17
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph67.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_9 Depth 2
                                        #       Child Loop BB2_10 Depth 3
                                        #         Child Loop BB2_14 Depth 4
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	malloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB2_18
# BB#3:                                 # %.preheader49.us.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_9:                                # %.preheader49.us
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_10 Depth 3
                                        #         Child Loop BB2_14 Depth 4
	testq	%r15, %r15
	jle	.LBB2_4
.LBB2_10:                               # %.loopexit.us.us
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_14 Depth 4
	movl	%r13d, %edi
	callq	gen_number
	movl	%eax, %r14d
	movl	%r12d, %edi
	callq	check_percent
	testl	%eax, %eax
	movl	4(%rsp), %edx           # 4-byte Reload
	jne	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_10 Depth=3
	movl	$1, %edi
	callq	gen_signed_number
	xorl	%edx, %edx
.LBB2_12:                               #   in Loop: Header=BB2_10 Depth=3
	movslq	%edx, %rax
	movq	(%rbp,%rax,8), %rax
	movslq	%r14d, %rcx
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB2_19
# BB#13:                                # %.preheader.us.us
                                        #   in Loop: Header=BB2_10 Depth=3
	movq	16(%rbx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_14:                               #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_9 Depth=2
                                        #       Parent Loop BB2_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	(%rcx,%rdx,8), %rax
	leaq	1(%rdx), %rdx
	je	.LBB2_10
# BB#15:                                #   in Loop: Header=BB2_14 Depth=4
	cmpq	%r15, %rdx
	jl	.LBB2_14
	jmp	.LBB2_8
	.p2align	4, 0x90
.LBB2_4:                                # %.preheader49..preheader49.split_crit_edge.us
                                        #   in Loop: Header=BB2_9 Depth=2
	movl	%r13d, %edi
	callq	gen_number
	movl	%eax, %r14d
	movl	%r12d, %edi
	callq	check_percent
	testl	%eax, %eax
	movl	4(%rsp), %edx           # 4-byte Reload
	jne	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_9 Depth=2
	movl	$1, %edi
	callq	gen_signed_number
	xorl	%edx, %edx
.LBB2_6:                                #   in Loop: Header=BB2_9 Depth=2
	movslq	%edx, %rax
	movq	(%rbp,%rax,8), %rax
	movslq	%r14d, %rcx
	movq	(%rax,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB2_19
# BB#7:                                 # %.critedge.us
                                        #   in Loop: Header=BB2_9 Depth=2
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_20
.LBB2_8:                                # %.critedge.us.thread
                                        #   in Loop: Header=BB2_9 Depth=2
	movq	%rax, (%rcx,%r15,8)
	incl	40(%rax)
	incq	%r15
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	jl	.LBB2_9
# BB#16:                                # %._crit_edge.us
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
	jmp	.LBB2_22
	.p2align	4, 0x90
.LBB2_17:                               # %.lr.ph67.split
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	malloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB2_18
# BB#21:                                # %.preheader50
                                        #   in Loop: Header=BB2_17 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_17
.LBB2_22:                               # %._crit_edge68
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_19:                               # %.us-lcssa.us
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movl	$1, %edi
	callq	exit
.LBB2_20:                               # %.us-lcssa70.us
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.LBB2_18:                               # %.us-lcssa69.us
	movl	$.Lstr.1, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.Lfunc_end2:
	.size	make_neighbors, .Lfunc_end2-make_neighbors
	.cfi_endproc

	.globl	update_from_coeffs
	.p2align	4, 0x90
	.type	update_from_coeffs,@function
update_from_coeffs:                     # @update_from_coeffs
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_2
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 24(%rbx)
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 32(%rbx)
	movl	$0, 44(%rbx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB3_6
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	40(%rbx), %r14
	testq	%r14, %r14
	jle	.LBB3_3
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	shlq	$3, %r14
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movl	$160, %r14d
	jmp	.LBB3_5
.LBB3_6:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	update_from_coeffs, .Lfunc_end3-update_from_coeffs
	.cfi_endproc

	.globl	fill_from_fields
	.p2align	4, 0x90
	.type	fill_from_fields,@function
fill_from_fields:                       # @fill_from_fields
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 64
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB4_9
# BB#1:                                 # %.preheader.lr.ph
	testl	%esi, %esi
	jle	.LBB4_8
# BB#2:                                 # %.preheader.us.preheader
	movl	%esi, %r14d
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %r13
	movq	16(%r15), %rax
	movq	(%rax,%rbx,8), %r12
	testq	%r12, %r12
	jne	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=2
	movl	$.Lstr.3, %edi
	callq	puts
.LBB4_6:                                #   in Loop: Header=BB4_4 Depth=2
	movslq	44(%r12), %rbp
	leal	1(%rbp), %eax
	movl	%eax, 44(%r12)
	movq	24(%r12), %rax
	movq	%r13, (%rax,%rbp,8)
	callq	gen_uniform_double
	movq	32(%r12), %rax
	movsd	%xmm0, (%rax,%rbp,8)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB4_4
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB4_3
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_8:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB4_8
.LBB4_9:                                # %._crit_edge36
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	fill_from_fields, .Lfunc_end4-fill_from_fields
	.cfi_endproc

	.globl	localize_local
	.p2align	4, 0x90
	.type	localize_local,@function
localize_local:                         # @localize_local
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB5_3
	.p2align	4, 0x90
.LBB5_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB5_1
.LBB5_3:                                # %._crit_edge
	retq
.Lfunc_end5:
	.size	localize_local, .Lfunc_end5-localize_local
	.cfi_endproc

	.globl	make_tables
	.p2align	4, 0x90
	.type	make_tables,@function
make_tables:                            # @make_tables
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 80
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	%esi, 4(%rsp)           # 4-byte Spill
	imull	$793, %esi, %edi        # imm = 0x319
	callq	init_random
	movslq	n_nodes(%rip), %r13
	leaq	(,%r13,8), %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB6_7
# BB#1:                                 # %make_table.exit
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, (%r12)
	callq	gen_uniform_double
	movsd	%xmm0, (%r14)
	movq	%r14, (%rbx)
	movl	$0, 40(%rbx)
	addq	$8, %r14
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r15
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	callq	gen_uniform_double
	movsd	%xmm0, (%r14)
	movq	%r14, (%rbx)
	movl	$0, 40(%rbx)
	movq	%rbx, (%r12,%rbp,8)
	movq	%rbx, 8(%r15)
	incq	%rbp
	addq	$8, %r14
	cmpq	%r13, %rbp
	jl	.LBB6_2
# BB#3:                                 # %fill_table.exit
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	$0, 8(%rbx)
	movslq	n_nodes(%rip), %r14
	leaq	(,%r14,8), %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB6_7
# BB#4:                                 # %make_table.exit24
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, (%r13)
	callq	gen_uniform_double
	movsd	%xmm0, (%rbp)
	movq	%rbp, (%rbx)
	movl	$0, 40(%rbx)
	addq	$8, %rbp
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph.i30
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r12
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	callq	gen_uniform_double
	movsd	%xmm0, (%rbp)
	movq	%rbp, (%rbx)
	movl	$0, 40(%rbx)
	movq	%rbx, (%r13,%r15,8)
	movq	%rbx, 8(%r12)
	incq	%r15
	addq	$8, %rbp
	cmpq	%r14, %r15
	jl	.LBB6_5
# BB#6:                                 # %fill_table.exit31
	movq	$0, 8(%rbx)
	movslq	4(%rsp), %rax           # 4-byte Folded Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r13, (%rcx,%rax,8)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, 8(%rcx,%rax,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_7:
	movl	$.Lstr, %edi
	callq	puts
	movl	$-1, %edi
	callq	exit
.Lfunc_end6:
	.size	make_tables, .Lfunc_end6-make_tables
	.cfi_endproc

	.globl	make_all_neighbors
	.p2align	4, 0x90
	.type	make_all_neighbors,@function
make_all_neighbors:                     # @make_all_neighbors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 32
.Lcfi61:
	.cfi_offset %rbx, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	imull	$39, %ebp, %edi
	callq	init_random
	movslq	%ebp, %rbp
	movq	8(%rbx,%rbp,8), %rax
	movq	(%rax), %rdi
	movl	n_nodes(%rip), %edx
	movl	d_nodes(%rip), %ecx
	movl	local_p(%rip), %r8d
	movq	%rbx, %rsi
	movl	%ebp, %r9d
	callq	make_neighbors
	movq	(%rbx,%rbp,8), %rax
	leaq	8(%rbx), %rsi
	movq	(%rax), %rdi
	movl	n_nodes(%rip), %edx
	movl	d_nodes(%rip), %ecx
	movl	local_p(%rip), %r8d
	movl	%ebp, %r9d
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	make_neighbors          # TAILCALL
.Lfunc_end7:
	.size	make_all_neighbors, .Lfunc_end7-make_all_neighbors
	.cfi_endproc

	.globl	update_all_from_coeffs
	.p2align	4, 0x90
	.type	update_all_from_coeffs,@function
update_all_from_coeffs:                 # @update_all_from_coeffs
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 48
.Lcfi68:
	.cfi_offset %rbx, -40
.Lcfi69:
	.cfi_offset %r12, -32
.Lcfi70:
	.cfi_offset %r14, -24
.Lcfi71:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	%esi, %r15
	movq	8(%r14,%r15,8), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_2
	jmp	.LBB8_6
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 24(%rbx)
	movq	%r12, %rdi
	callq	malloc
	movq	%rax, 32(%rbx)
	movl	$0, 44(%rbx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB8_6
.LBB8_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	40(%rbx), %r12
	testq	%r12, %r12
	jle	.LBB8_3
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	shlq	$3, %r12
	jmp	.LBB8_5
	.p2align	4, 0x90
.LBB8_3:                                #   in Loop: Header=BB8_2 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	movl	$160, %r12d
	jmp	.LBB8_5
.LBB8_6:                                # %update_from_coeffs.exit
	movq	(%r14,%r15,8), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_8
	jmp	.LBB8_12
	.p2align	4, 0x90
.LBB8_11:                               #   in Loop: Header=BB8_8 Depth=1
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 24(%rbx)
	movq	%r14, %rdi
	callq	malloc
	movq	%rax, 32(%rbx)
	movl	$0, 44(%rbx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB8_12
.LBB8_8:                                # %.lr.ph.i11
                                        # =>This Inner Loop Header: Depth=1
	movslq	40(%rbx), %r14
	testq	%r14, %r14
	jle	.LBB8_9
# BB#10:                                #   in Loop: Header=BB8_8 Depth=1
	shlq	$3, %r14
	jmp	.LBB8_11
	.p2align	4, 0x90
.LBB8_9:                                #   in Loop: Header=BB8_8 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movl	$160, %r14d
	jmp	.LBB8_11
.LBB8_12:                               # %update_from_coeffs.exit13
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	update_all_from_coeffs, .Lfunc_end8-update_all_from_coeffs
	.cfi_endproc

	.globl	fill_all_from_fields
	.p2align	4, 0x90
	.type	fill_all_from_fields,@function
fill_all_from_fields:                   # @fill_all_from_fields
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 80
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	%ebx, %eax
	shll	$4, %eax
	leal	(%rax,%rbx), %edi
	callq	init_random
	movslq	%ebx, %rcx
	movq	8(%rbp,%rcx,8), %rax
	movq	(%rax), %r13
	movl	d_nodes(%rip), %r12d
	testq	%r13, %r13
	je	.LBB9_10
# BB#1:                                 # %.preheader.lr.ph.i
	testl	%r12d, %r12d
	jle	.LBB9_9
# BB#2:                                 # %.preheader.us.preheader.i
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_3:                                # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_4:                                #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %r14
	movq	16(%r13), %rax
	movq	(%rax,%rbx,8), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=2
	movl	$.Lstr.3, %edi
	callq	puts
.LBB9_6:                                #   in Loop: Header=BB9_4 Depth=2
	movslq	44(%rbp), %r15
	leal	1(%r15), %eax
	movl	%eax, 44(%rbp)
	movq	24(%rbp), %rax
	movq	%r14, (%rax,%r15,8)
	callq	gen_uniform_double
	movq	32(%rbp), %rax
	movsd	%xmm0, (%rax,%r15,8)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB9_4
# BB#7:                                 # %._crit_edge.us.i
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	8(%r13), %r13
	testq	%r13, %r13
	jne	.LBB9_3
# BB#8:                                 # %fill_from_fields.exitthread-pre-split.loopexit
	movl	d_nodes(%rip), %r12d
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	jmp	.LBB9_10
	.p2align	4, 0x90
.LBB9_9:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %r13
	testq	%r13, %r13
	jne	.LBB9_9
.LBB9_10:                               # %fill_from_fields.exit
	movq	(%rbp,%rcx,8), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.LBB9_19
# BB#11:                                # %.preheader.lr.ph.i11
	testl	%r12d, %r12d
	jle	.LBB9_18
# BB#12:                                # %.preheader.us.preheader.i13
	movl	%r12d, %r14d
	.p2align	4, 0x90
.LBB9_13:                               # %.preheader.us.i15
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_14 Depth 2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_14:                               #   Parent Loop BB9_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %r12
	movq	16(%r15), %rax
	movq	(%rax,%rbx,8), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_16
# BB#15:                                #   in Loop: Header=BB9_14 Depth=2
	movl	$.Lstr.3, %edi
	callq	puts
.LBB9_16:                               #   in Loop: Header=BB9_14 Depth=2
	movslq	44(%rbp), %r13
	leal	1(%r13), %eax
	movl	%eax, 44(%rbp)
	movq	24(%rbp), %rax
	movq	%r12, (%rax,%r13,8)
	callq	gen_uniform_double
	movq	32(%rbp), %rax
	movsd	%xmm0, (%rax,%r13,8)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB9_14
# BB#17:                                # %._crit_edge.us.i20
                                        #   in Loop: Header=BB9_13 Depth=1
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB9_13
	jmp	.LBB9_19
	.p2align	4, 0x90
.LBB9_18:                               # %.preheader.i22
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB9_18
.LBB9_19:                               # %fill_from_fields.exit23
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	fill_all_from_fields, .Lfunc_end9-fill_all_from_fields
	.cfi_endproc

	.globl	localize
	.p2align	4, 0x90
	.type	localize,@function
localize:                               # @localize
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	localize, .Lfunc_end10-localize
	.cfi_endproc

	.globl	clear_nummiss
	.p2align	4, 0x90
	.type	clear_nummiss,@function
clear_nummiss:                          # @clear_nummiss
	.cfi_startproc
# BB#0:
	movl	$0, NumMisses(%rip)
	retq
.Lfunc_end11:
	.size	clear_nummiss, .Lfunc_end11-clear_nummiss
	.cfi_endproc

	.globl	do_all
	.p2align	4, 0x90
	.type	do_all,@function
do_all:                                 # @do_all
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 64
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%edx, %r13d
	movl	%esi, %r15d
	movq	%rdi, %r12
	cmpl	$2, %r13d
	jl	.LBB12_2
	.p2align	4, 0x90
.LBB12_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %ebp
	shrl	%ebp
	leal	(%rbp,%r15), %esi
	movq	%r12, %rdi
	movl	%ebp, %edx
	movq	%rcx, %rbx
	movl	%r14d, %r8d
	callq	do_all
	movq	%rbx, %rcx
	cmpl	$3, %r13d
	movl	%ebp, %r13d
	ja	.LBB12_1
.LBB12_2:                               # %tailrecurse._crit_edge
	movq	%r12, %rdi
	movl	%r15d, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rcx                   # TAILCALL
.Lfunc_end12:
	.size	do_all, .Lfunc_end12-do_all
	.cfi_endproc

	.globl	initialize_graph
	.p2align	4, 0x90
	.type	initialize_graph,@function
initialize_graph:                       # @initialize_graph
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 96
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r13
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	movl	$.Lstr.4, %edi
	callq	puts
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	make_tables
	movl	$.Lstr.5, %edi
	callq	puts
	xorl	%edi, %edi
	callq	init_random
	leaq	8(%r13), %r15
	movq	8(%r13), %rax
	movq	(%rax), %rdi
	movl	n_nodes(%rip), %edx
	movl	d_nodes(%rip), %ecx
	movl	local_p(%rip), %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rsi
	callq	make_neighbors
	movq	(%r13), %rax
	movq	(%rax), %rdi
	movl	n_nodes(%rip), %edx
	movl	d_nodes(%rip), %ecx
	movl	local_p(%rip), %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rsi
	callq	make_neighbors
	movl	$.Lstr.6, %edi
	callq	puts
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	update_all_from_coeffs
	movl	$.Lstr.7, %edi
	callq	puts
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	fill_all_from_fields
	movl	$.Lstr.8, %edi
	callq	puts
	movl	NumNodes(%rip), %r15d
	leal	1(%r15), %eax
	cmpl	$3, %eax
	cmovael	%ebx, %r15d
	movl	$.Lstr.9, %edi
	callq	puts
	movslq	NumNodes(%rip), %rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rcx, %rcx
	jle	.LBB13_16
# BB#1:                                 # %.lr.ph83
	cmpl	$1, %r15d
	movslq	%r15d, %rdi
	jle	.LBB13_2
# BB#8:                                 # %.lr.ph83.split.us.preheader
	movslq	n_nodes(%rip), %rdx
	movl	%edi, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	(,%rdi,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
	movq	%r13, %r12
	movq	%rdi, %r10
	.p2align	4, 0x90
.LBB13_9:                               # %.lr.ph83.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_14 Depth 2
	movq	%r11, %rax
	imulq	%rdi, %rax
	movq	(%r13,%rax,8), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, (%r14,%r11,8)
	movq	8(%r13,%rax,8), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, 8(%r14,%r11,8)
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB13_10
# BB#11:                                #   in Loop: Header=BB13_9 Depth=1
	movq	(%r13,%rax,8), %rsi
	movq	-8(%rsi,%rdx,8), %rsi
	movq	8(%r13,%rax,8), %rdi
	movq	(%rdi), %rbp
	movq	%rbp, 8(%rsi)
	movq	-8(%rdi,%rdx,8), %rsi
	movq	16(%r13,%rax,8), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%rsi)
	movl	$2, %esi
	cmpl	$2, %r15d
	jne	.LBB13_13
	jmp	.LBB13_15
	.p2align	4, 0x90
.LBB13_10:                              #   in Loop: Header=BB13_9 Depth=1
	movl	$1, %esi
	cmpl	$2, %r15d
	je	.LBB13_15
.LBB13_13:                              # %.lr.ph83.split.us.new
                                        #   in Loop: Header=BB13_9 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	subq	%rsi, %rax
	leaq	(%r12,%rsi,8), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB13_14:                              #   Parent Loop BB13_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi,%rdi,8), %rbp
	movq	-8(%rbp,%rdx,8), %rbp
	movq	(%rsi,%rdi,8), %r8
	movq	(%r8), %r9
	movq	%r9, 8(%rbp)
	movq	-8(%r8,%rdx,8), %rbp
	movq	8(%rsi,%rdi,8), %rbx
	movq	(%rbx), %rbx
	movq	%rbx, 8(%rbp)
	movq	(%rsi,%rdi,8), %rbp
	movq	-8(%rbp,%rdx,8), %rbp
	movq	8(%rsi,%rdi,8), %rbx
	movq	(%rbx), %rcx
	movq	%rcx, 8(%rbp)
	movq	-8(%rbx,%rdx,8), %rcx
	movq	16(%rsi,%rdi,8), %rbp
	movq	(%rbp), %rbp
	movq	%rbp, 8(%rcx)
	addq	$2, %rdi
	cmpq	%rdi, %rax
	jne	.LBB13_14
.LBB13_15:                              # %._crit_edge.us
                                        #   in Loop: Header=BB13_9 Depth=1
	incq	%r11
	addq	24(%rsp), %r12          # 8-byte Folded Reload
	cmpq	8(%rsp), %r11           # 8-byte Folded Reload
	movq	%r10, %rdi
	jl	.LBB13_9
	jmp	.LBB13_16
.LBB13_2:                               # %.lr.ph83.split.preheader
	testb	$1, 8(%rsp)             # 1-byte Folded Reload
	jne	.LBB13_4
# BB#3:
	xorl	%ecx, %ecx
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB13_6
	jmp	.LBB13_16
.LBB13_4:                               # %.lr.ph83.split.prol
	movq	(%r13), %rax
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	8(%r13), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%r14)
	movl	$1, %ecx
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB13_16
.LBB13_6:                               # %.lr.ph83.split.preheader.new
	movq	%rcx, %rdx
	imulq	%rdi, %rdx
	leaq	1(%rcx), %rsi
	imulq	%rdi, %rsi
	shlq	$4, %rdi
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph83.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rdx,8), %rax
	movq	(%rax), %rax
	movq	%rax, (%r14,%rcx,8)
	movq	8(%r13,%rdx,8), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%r14,%rcx,8)
	movq	(%r13,%rsi,8), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%r14,%rcx,8)
	movq	8(%r13,%rsi,8), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r14,%rcx,8)
	addq	$2, %rcx
	addq	%rdi, %r13
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	jl	.LBB13_7
.LBB13_16:                              # %._crit_edge84
	movl	$.Lstr.10, %edi
	callq	puts
	movl	$0, NumMisses(%rip)
	movl	$.Lstr.11, %edi
	callq	puts
	movq	%r14, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	initialize_graph, .Lfunc_end13-initialize_graph
	.cfi_endproc

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"Error! on dest %d @ %d\n"
	.size	.L.str.2, 24

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Help! no from count (from_count=%d) \n"
	.size	.L.str.4, 38

	.type	n_nodes,@object         # @n_nodes
	.comm	n_nodes,4,4
	.type	d_nodes,@object         # @d_nodes
	.comm	d_nodes,4,4
	.type	local_p,@object         # @local_p
	.comm	local_p,4,4
	.type	NumMisses,@object       # @NumMisses
	.comm	NumMisses,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Assertion failure"
	.size	.Lstr, 18

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Uncaught malloc error"
	.size	.Lstr.1, 22

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Error! no to_nodes filed!"
	.size	.Lstr.2, 26

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.3:
	.asciz	"Help!!"
	.size	.Lstr.3, 7

	.type	.Lstr.4,@object         # @str.4
.Lstr.4:
	.asciz	"making tables "
	.size	.Lstr.4, 15

	.type	.Lstr.5,@object         # @str.5
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.5:
	.asciz	"making neighbors"
	.size	.Lstr.5, 17

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"updating from and coeffs"
	.size	.Lstr.6, 25

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"filling from fields"
	.size	.Lstr.7, 20

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"localizing coeffs, from_nodes"
	.size	.Lstr.8, 30

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"cleanup for return now"
	.size	.Lstr.9, 23

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	"Clearing NumMisses"
	.size	.Lstr.10, 19

	.type	.Lstr.11,@object        # @str.11
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.11:
	.asciz	"Returning"
	.size	.Lstr.11, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
