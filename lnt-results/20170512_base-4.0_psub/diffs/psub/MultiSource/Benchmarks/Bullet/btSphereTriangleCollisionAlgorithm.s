	.text
	.file	"btSphereTriangleCollisionAlgorithm.bc"
	.globl	_ZN34btSphereTriangleCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.p2align	4, 0x90
	.type	_ZN34btSphereTriangleCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b,@function
_ZN34btSphereTriangleCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b: # @_ZN34btSphereTriangleCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	%rdx, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV34btSphereTriangleCollisionAlgorithm+16, (%rbx)
	movb	$0, 16(%rbx)
	movq	%rbp, 24(%rbx)
	movb	%r12b, 32(%rbx)
	testq	%rbp, %rbp
	jne	.LBB0_3
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*24(%rax)
.Ltmp1:
# BB#2:
	movq	%rax, 24(%rbx)
	movb	$1, 16(%rbx)
.LBB0_3:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_4:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp4:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_6:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN34btSphereTriangleCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b, .Lfunc_end0-_ZN34btSphereTriangleCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN34btSphereTriangleCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN34btSphereTriangleCollisionAlgorithmD2Ev,@function
_ZN34btSphereTriangleCollisionAlgorithmD2Ev: # @_ZN34btSphereTriangleCollisionAlgorithmD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV34btSphereTriangleCollisionAlgorithm+16, (%rbx)
	cmpb	$0, 16(%rbx)
	je	.LBB2_3
# BB#1:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp6:
	callq	*32(%rax)
.Ltmp7:
.LBB2_3:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN30btActivatingCollisionAlgorithmD2Ev # TAILCALL
.LBB2_4:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp10:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_6:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN34btSphereTriangleCollisionAlgorithmD2Ev, .Lfunc_end2-_ZN34btSphereTriangleCollisionAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN34btSphereTriangleCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN34btSphereTriangleCollisionAlgorithmD0Ev,@function
_ZN34btSphereTriangleCollisionAlgorithmD0Ev: # @_ZN34btSphereTriangleCollisionAlgorithmD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV34btSphereTriangleCollisionAlgorithm+16, (%rbx)
	cmpb	$0, 16(%rbx)
	je	.LBB3_3
# BB#1:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB3_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp12:
	callq	*32(%rax)
.Ltmp13:
.LBB3_3:
.Ltmp18:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp19:
# BB#4:                                 # %_ZN34btSphereTriangleCollisionAlgorithmD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_5:
.Ltmp14:
	movq	%rax, %r14
.Ltmp15:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp16:
	jmp	.LBB3_8
.LBB3_6:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_7:
.Ltmp20:
	movq	%rax, %r14
.LBB3_8:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN34btSphereTriangleCollisionAlgorithmD0Ev, .Lfunc_end3-_ZN34btSphereTriangleCollisionAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN34btSphereTriangleCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN34btSphereTriangleCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN34btSphereTriangleCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN34btSphereTriangleCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 256
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rdx, %rbx
	movq	%rdi, %r12
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB4_7
# BB#1:
	cmpb	$0, 32(%r12)
	movq	%rsi, %r15
	cmovneq	%rbx, %r15
	cmovneq	%rsi, %rbx
	movq	200(%r15), %r13
	movq	200(%rbx), %rbp
	movq	%rdi, 8(%r14)
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	callq	_ZNK20btPersistentManifold27getContactBreakingThresholdEv
	leaq	168(%rsp), %rdi
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	_ZN22SphereTriangleDetectorC1EP13btSphereShapeP15btTriangleShapef
	movq	$0, 152(%rsp)
	movl	$1566444395, 144(%rsp)  # imm = 0x5D5E0B6B
	movups	8(%r15), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	24(%r15), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	40(%r15), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	56(%r15), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	8(%rbx), %xmm0
	movaps	%xmm0, 80(%rsp)
	movups	24(%rbx), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	40(%rbx), %xmm0
	movaps	%xmm0, 112(%rsp)
	movups	56(%rbx), %xmm0
	movaps	%xmm0, 128(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	24(%rax), %rcx
	movzbl	32(%r12), %r8d
.Ltmp21:
	leaq	16(%rsp), %rsi
	leaq	168(%rsp), %rdi
	movq	%r14, %rdx
	callq	_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
.Ltmp22:
# BB#2:
	cmpb	$0, 16(%r12)
	je	.LBB4_7
# BB#3:
	movq	8(%r14), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB4_7
# BB#4:
	movq	712(%rdi), %rax
	cmpq	144(%r14), %rax
	je	.LBB4_6
# BB#5:
	leaq	80(%r14), %rsi
	addq	$16, %r14
.Ltmp23:
	movq	%r14, %rdx
	callq	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
.Ltmp24:
	jmp	.LBB4_7
.LBB4_6:
	leaq	16(%r14), %rsi
	addq	$80, %r14
.Ltmp25:
	movq	%r14, %rdx
	callq	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
.Ltmp26:
.LBB4_7:
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_8:
.Ltmp27:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN34btSphereTriangleCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end4-_ZN34btSphereTriangleCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp21-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp26-.Ltmp21         #   Call between .Ltmp21 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin3   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp26     #   Call between .Ltmp26 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN34btSphereTriangleCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN34btSphereTriangleCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN34btSphereTriangleCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN34btSphereTriangleCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	movss	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end5:
	.size	_ZN34btSphereTriangleCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end5-_ZN34btSphereTriangleCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.text._ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 48
.Lcfi38:
	.cfi_offset %rbx, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB6_18
# BB#1:
	cmpb	$0, 16(%r14)
	je	.LBB6_18
# BB#2:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB6_17
# BB#3:
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%edx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB6_17
# BB#4:
	testl	%ebp, %ebp
	je	.LBB6_5
# BB#6:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB6_8
	jmp	.LBB6_12
.LBB6_5:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB6_12
.LBB6_8:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB6_10
	.p2align	4, 0x90
.LBB6_9:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB6_9
.LBB6_10:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB6_12
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB6_11
.LBB6_12:                               # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_16
# BB#13:
	cmpb	$0, 24(%rbx)
	je	.LBB6_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB6_15:
	movq	$0, 16(%rbx)
.LBB6_16:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
	movq	24(%r14), %rcx
.LBB6_17:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	16(%rbx), %rdx
	cltq
	movq	%rcx, (%rdx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB6_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end6-_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.type	_ZTV34btSphereTriangleCollisionAlgorithm,@object # @_ZTV34btSphereTriangleCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV34btSphereTriangleCollisionAlgorithm
	.p2align	3
_ZTV34btSphereTriangleCollisionAlgorithm:
	.quad	0
	.quad	_ZTI34btSphereTriangleCollisionAlgorithm
	.quad	_ZN34btSphereTriangleCollisionAlgorithmD2Ev
	.quad	_ZN34btSphereTriangleCollisionAlgorithmD0Ev
	.quad	_ZN34btSphereTriangleCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN34btSphereTriangleCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV34btSphereTriangleCollisionAlgorithm, 56

	.type	_ZTS34btSphereTriangleCollisionAlgorithm,@object # @_ZTS34btSphereTriangleCollisionAlgorithm
	.globl	_ZTS34btSphereTriangleCollisionAlgorithm
	.p2align	4
_ZTS34btSphereTriangleCollisionAlgorithm:
	.asciz	"34btSphereTriangleCollisionAlgorithm"
	.size	_ZTS34btSphereTriangleCollisionAlgorithm, 37

	.type	_ZTI34btSphereTriangleCollisionAlgorithm,@object # @_ZTI34btSphereTriangleCollisionAlgorithm
	.globl	_ZTI34btSphereTriangleCollisionAlgorithm
	.p2align	4
_ZTI34btSphereTriangleCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS34btSphereTriangleCollisionAlgorithm
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.size	_ZTI34btSphereTriangleCollisionAlgorithm, 24


	.globl	_ZN34btSphereTriangleCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.type	_ZN34btSphereTriangleCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b,@function
_ZN34btSphereTriangleCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b = _ZN34btSphereTriangleCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.globl	_ZN34btSphereTriangleCollisionAlgorithmD1Ev
	.type	_ZN34btSphereTriangleCollisionAlgorithmD1Ev,@function
_ZN34btSphereTriangleCollisionAlgorithmD1Ev = _ZN34btSphereTriangleCollisionAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
