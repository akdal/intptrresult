	.text
	.file	"RandGen.bc"
	.globl	_ZN16CRandomGenerator4InitEv
	.p2align	4, 0x90
	.type	_ZN16CRandomGenerator4InitEv,@function
_ZN16CRandomGenerator4InitEv:           # @_ZN16CRandomGenerator4InitEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	32(%rsp), %r14
	movq	%r14, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 64(%rsp)
	callq	getpid
	movl	%eax, (%rsp)
	movq	%rsp, %rbp
	movl	$4, %edx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	callq	getppid
	movl	%eax, (%rsp)
	movl	$4, %edx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	xorl	%r15d, %r15d
	leaq	16(%rsp), %r12
	leaq	4(%rsp), %rbp
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	gettimeofday
	testl	%eax, %eax
	jne	.LBB0_3
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	$8, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movl	$8, %edx
	movq	%r14, %rdi
	leaq	24(%rsp), %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%edi, %edi
	callq	time
	movq	%rax, 8(%rsp)
	movl	$8, %edx
	movq	%r14, %rdi
	leaq	8(%rsp), %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	callq	GetTickCount
	movl	%eax, 4(%rsp)
	movl	$4, %edx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movl	$100, %r13d
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	movq	%r14, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 64(%rsp)
	movl	$20, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	decl	%r13d
	jne	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	incl	%r15d
	cmpl	$1000, %r15d            # imm = 0x3E8
	jne	.LBB0_1
# BB#6:
	leaq	32(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	movb	$0, 20(%rbx)
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN16CRandomGenerator4InitEv, .Lfunc_end0-_ZN16CRandomGenerator4InitEv
	.cfi_endproc

	.section	.text._ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,"axG",@progbits,_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,comdat
	.weak	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,@function
_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev: # @_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.cfi_startproc
# BB#0:
	jmp	pthread_mutex_destroy   # TAILCALL
.Lfunc_end1:
	.size	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev, .Lfunc_end1-_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.cfi_endproc

	.text
	.globl	_ZN16CRandomGenerator8GenerateEPhj
	.p2align	4, 0x90
	.type	_ZN16CRandomGenerator8GenerateEPhj,@function
_ZN16CRandomGenerator8GenerateEPhj:     # @_ZN16CRandomGenerator8GenerateEPhj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 208
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_lock
	cmpb	$0, 20(%r14)
	je	.LBB2_2
# BB#1:
	movq	%r14, %rdi
	callq	_ZN16CRandomGenerator4InitEv
.LBB2_2:                                # %.preheader
	testl	%r15d, %r15d
	je	.LBB2_21
# BB#3:                                 # %.lr.ph17
	leaq	48(%rsp), %r12
	leaq	16(%rsp), %rbp
	movq	$-20, %r13
	jmp	.LBB2_4
.LBB2_12:                               # %vector.body.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	leaq	-32(%rax), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_13
# BB#14:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_15:                               # %vector.body.prol
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	16(%rsp,%rdi), %xmm0
	movaps	32(%rsp,%rdi), %xmm1
	movups	%xmm0, (%rbx,%rdi)
	movups	%xmm1, 16(%rbx,%rdi)
	addq	$32, %rdi
	incq	%rsi
	jne	.LBB2_15
	jmp	.LBB2_16
.LBB2_13:                               #   in Loop: Header=BB2_4 Depth=1
	xorl	%edi, %edi
.LBB2_16:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpq	$96, %rdx
	jb	.LBB2_19
# BB#17:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	%rax, %rdx
	subq	%rdi, %rdx
	leaq	128(%rsp), %rsi
	leaq	(%rsi,%rdi), %rsi
	leaq	112(%rbx,%rdi), %rdi
	.p2align	4, 0x90
.LBB2_18:                               # %vector.body
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	-112(%rsi), %xmm0
	movaps	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movaps	-80(%rsi), %xmm0
	movaps	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movaps	-48(%rsi), %xmm0
	movaps	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movaps	-16(%rsi), %xmm0
	movaps	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-128, %rdx
	jne	.LBB2_18
.LBB2_19:                               # %middle.block
                                        #   in Loop: Header=BB2_4 Depth=1
	subl	%eax, %r15d
	addq	%rax, %rbx
	cmpq	%rcx, %rax
	jne	.LBB2_6
	jmp	.LBB2_20
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_15 Depth 2
                                        #     Child Loop BB2_18 Depth 2
                                        #     Child Loop BB2_7 Depth 2
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 80(%rsp)
	movl	$20, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	movq	%r12, %rdi
	callq	_ZN7NCrypto5NSha112CContextBase4InitEv
	movl	$0, 80(%rsp)
	movl	$-160257071, 12(%rsp)   # imm = 0xF672ABD1
	movl	$4, %edx
	movq	%r12, %rdi
	leaq	12(%rsp), %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movl	$20, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN7NCrypto5NSha18CContext6UpdateEPKhm
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	_ZN7NCrypto5NSha18CContext5FinalEPh
	leal	-1(%r15), %edx
	movq	%rdx, %rcx
	notq	%rcx
	cmpq	$-20, %rcx
	cmovbeq	%r13, %rcx
	negq	%rcx
	cmpq	$31, %rcx
	jbe	.LBB2_5
# BB#9:                                 # %min.iters.checked
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	%rcx, %rax
	andq	$32, %rax
	je	.LBB2_5
# BB#10:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_4 Depth=1
	notq	%rdx
	cmpq	$-20, %rdx
	cmovbeq	%r13, %rdx
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rbx
	jae	.LBB2_12
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	%rbx, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rbp
	jae	.LBB2_12
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_4 Depth=1
	xorl	%eax, %eax
.LBB2_6:                                # %.lr.ph.preheader36
                                        #   in Loop: Header=BB2_4 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	movzbl	15(%rsp,%rax), %ecx
	movb	%cl, (%rbx)
	incq	%rbx
	cmpl	$1, %r15d
	leal	-1(%r15), %r15d
	je	.LBB2_20
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_7 Depth=2
	cmpq	$20, %rax
	leaq	1(%rax), %rax
	jb	.LBB2_7
.LBB2_20:                               # %._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	testl	%r15d, %r15d
	jne	.LBB2_4
.LBB2_21:                               # %._crit_edge18
	movl	$_ZL17g_CriticalSection, %edi
	callq	pthread_mutex_unlock
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN16CRandomGenerator8GenerateEPhj, .Lfunc_end2-_ZN16CRandomGenerator8GenerateEPhj
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_RandGen.ii,@function
_GLOBAL__sub_I_RandGen.ii:              # @_GLOBAL__sub_I_RandGen.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	movl	$_ZL17g_CriticalSection, %edi
	callq	CriticalSection_Init
	movl	$_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev, %edi
	movl	$_ZL17g_CriticalSection, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movb	$1, g_RandomGenerator+20(%rip)
	popq	%rax
	retq
.Lfunc_end3:
	.size	_GLOBAL__sub_I_RandGen.ii, .Lfunc_end3-_GLOBAL__sub_I_RandGen.ii
	.cfi_endproc

	.type	_ZL17g_CriticalSection,@object # @_ZL17g_CriticalSection
	.local	_ZL17g_CriticalSection
	.comm	_ZL17g_CriticalSection,40,8
	.type	g_RandomGenerator,@object # @g_RandomGenerator
	.bss
	.globl	g_RandomGenerator
g_RandomGenerator:
	.zero	21
	.size	g_RandomGenerator, 21

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_RandGen.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
