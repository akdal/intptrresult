	.text
	.file	"lists.bc"
	.globl	list_push_tail
	.p2align	4, 0x90
	.type	list_push_tail,@function
list_push_tail:                         # @list_push_tail
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	movq	%rsi, 8(%rax)
	movq	%rdi, 8(%rsi)
	movq	%rsi, 16(%rdi)
	movq	%rax, 16(%rsi)
	incl	(%rdi)
	retq
.Lfunc_end0:
	.size	list_push_tail, .Lfunc_end0-list_push_tail
	.cfi_endproc

	.globl	list_pop_tail
	.p2align	4, 0x90
	.type	list_pop_tail,@function
list_pop_tail:                          # @list_pop_tail
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB1_1
# BB#2:
	movq	16(%rdi), %rax
	movq	16(%rax), %rdx
	movq	%rdi, 8(%rdx)
	movq	%rdx, 16(%rdi)
	decl	%ecx
	movl	%ecx, (%rdi)
	retq
.LBB1_1:
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	list_pop_tail, .Lfunc_end1-list_pop_tail
	.cfi_endproc

	.globl	list_push_head
	.p2align	4, 0x90
	.type	list_push_head,@function
list_push_head:                         # @list_push_head
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movq	%rsi, 8(%rdi)
	movq	%rsi, 16(%rax)
	movq	%rax, 8(%rsi)
	movq	%rdi, 16(%rsi)
	incl	(%rdi)
	retq
.Lfunc_end2:
	.size	list_push_head, .Lfunc_end2-list_push_head
	.cfi_endproc

	.globl	list_pop_head
	.p2align	4, 0x90
	.type	list_pop_head,@function
list_pop_head:                          # @list_pop_head
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB3_1
# BB#2:
	movq	8(%rdi), %rax
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movq	8(%rax), %rdx
	movq	%rdi, 16(%rdx)
	decl	%ecx
	movl	%ecx, (%rdi)
	retq
.LBB3_1:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	list_pop_head, .Lfunc_end3-list_pop_head
	.cfi_endproc

	.globl	list_equal
	.p2align	4, 0x90
	.type	list_equal,@function
list_equal:                             # @list_equal
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movl	(%rdi), %ecx
	cmpl	(%rsi), %ecx
	setne	%dl
	movq	%rsi, %rcx
	cmpq	%rdi, %rax
	jne	.LBB4_2
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	movq	8(%rcx), %rcx
	movl	(%rax), %edx
	movq	8(%rax), %rax
	cmpl	(%rcx), %edx
	setne	%dl
	cmpq	%rdi, %rax
	je	.LBB4_5
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %dl
	je	.LBB4_4
# BB#3:
	xorl	%eax, %eax
	retq
.LBB4_5:                                # %._crit_edge
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.LBB4_7
# BB#6:
	xorl	%eax, %eax
	cmpq	%rsi, 8(%rcx)
	sete	%al
.LBB4_7:                                # %.loopexit
	retq
.Lfunc_end4:
	.size	list_equal, .Lfunc_end4-list_equal
	.cfi_endproc

	.globl	list_print
	.p2align	4, 0x90
	.type	list_print,@function
list_print:                             # @list_print
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	8(%rbx), %r14
	callq	puts
	movl	(%rbx), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%rbx), %rax
	leaq	8(%rax), %r15
	movq	8(%rax), %rcx
	cmpq	%r14, %rcx
	je	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	movl	(%rcx), %ecx
	movq	16(%rax), %rax
	movl	(%rax), %r8d
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	(%r15), %rax
	leaq	8(%rax), %r15
	movq	8(%rax), %rcx
	incl	%ebx
	cmpq	%r14, %rcx
	jne	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movl	$.Lstr, %edi
	callq	puts
	movq	(%r15), %rax
	movl	(%rax), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	printf                  # TAILCALL
.Lfunc_end5:
	.size	list_print, .Lfunc_end5-list_print
	.cfi_endproc

	.globl	list_new
	.p2align	4, 0x90
	.type	list_new,@function
list_new:                               # @list_new
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$24, %edi
	callq	malloc
	movq	%rax, 8(%rax)
	movq	%rax, 16(%rax)
	movl	$0, (%rax)
	popq	%rcx
	retq
.Lfunc_end6:
	.size	list_new, .Lfunc_end6-list_new
	.cfi_endproc

	.globl	list_sequence
	.p2align	4, 0x90
	.type	list_sequence,@function
list_sequence:                          # @list_sequence
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -48
.Lcfi13:
	.cfi_offset %r12, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	cmpl	%esi, %ebx
	movl	%esi, %r12d
	cmovgel	%ebx, %r12d
	cmovgl	%esi, %ebx
	movl	%r12d, %ebp
	subl	%ebx, %ebp
	leal	1(%rbp), %r15d
	movslq	%ebp, %r14
	leaq	(,%r14,8), %rax
	leaq	48(%rax,%rax,2), %rdi
	callq	malloc
	leal	-1(%rbx), %ecx
	cmpl	%ebx, %r12d
	js	.LBB7_8
# BB#1:                                 # %.lr.ph.preheader
	testb	$1, %bpl
	jne	.LBB7_2
# BB#3:                                 # %.lr.ph.prol
	leaq	24(%rax), %rdx
	movq	%rdx, 8(%rax)
	movq	%rax, 40(%rax)
	movl	%ecx, (%rax)
	movl	$2, %edi
	movl	$1, %edx
	movl	%ebx, %ecx
	testl	%ebp, %ebp
	jne	.LBB7_5
	jmp	.LBB7_7
.LBB7_2:
	movl	$1, %edi
	xorl	%edx, %edx
	testl	%ebp, %ebp
	je	.LBB7_7
.LBB7_5:                                # %.lr.ph.preheader.new
	leaq	(,%rdx,8), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	decq	%rdx
	shlq	$3, %rdi
	leaq	(%rdi,%rdi,2), %rdi
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	24(%rbp,%rsi), %rbx
	movq	%rbx, 8(%rbp,%rsi)
	leaq	(%rbp,%rdi), %r8
	leaq	-24(%rbp,%rdi), %rbx
	movq	%rbx, 16(%rbp,%rdi)
	movl	%ecx, (%rbp,%rsi)
	leaq	48(%rbp,%rsi), %rbx
	movq	%rbx, 32(%rbp,%rsi)
	movq	%r8, 40(%rbp,%rdi)
	leal	1(%rcx), %ebx
	movl	%ebx, 24(%rbp,%rsi)
	addq	$48, %rbp
	addq	$2, %rdx
	addl	$2, %ecx
	cmpq	%r14, %rdx
	jl	.LBB7_6
.LBB7_7:
	movl	%r12d, %ecx
.LBB7_8:                                # %._crit_edge
	movslq	%r15d, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	leaq	(%rax,%rsi,8), %rdi
	movq	%rdi, 16(%rax)
	movq	%rax, 8(%rax,%rsi,8)
	leaq	(%r14,%r14,2), %rdi
	leaq	(%rax,%rdi,8), %rdi
	movq	%rdi, 16(%rax,%rsi,8)
	movl	%ecx, (%rax,%rsi,8)
	movl	%edx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	list_sequence, .Lfunc_end7-list_sequence
	.cfi_endproc

	.globl	list_copy
	.p2align	4, 0x90
	.type	list_copy,@function
list_copy:                              # @list_copy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	(%r14), %rbx
	leaq	(%rbx,%rbx,2), %r15
	leaq	24(,%r15,8), %rdi
	callq	malloc
	testq	%rbx, %rbx
	jle	.LBB8_8
# BB#1:                                 # %.lr.ph.preheader
	leaq	24(%rax), %rsi
	movq	%rsi, 8(%rax)
	movq	%rax, 40(%rax)
	movl	%ebx, (%rax)
	cmpl	$1, %ebx
	je	.LBB8_8
# BB#2:                                 # %.lr.ph..lr.ph_crit_edge.preheader
	movl	%ebx, %r9d
	testb	$1, %r9b
	jne	.LBB8_3
# BB#4:                                 # %.lr.ph..lr.ph_crit_edge.prol
	movq	8(%r14), %rdx
	movl	(%rdx), %r8d
	leaq	48(%rax), %rdi
	movq	%rdi, 32(%rax)
	movq	%rsi, 64(%rax)
	movl	%r8d, 24(%rax)
	movl	$2, %edi
	cmpl	$2, %ebx
	jne	.LBB8_6
	jmp	.LBB8_8
.LBB8_3:
	movl	$1, %edi
	movq	%r14, %rdx
	cmpl	$2, %ebx
	je	.LBB8_8
.LBB8_6:                                # %.lr.ph..lr.ph_crit_edge.preheader.new
	subq	%rdi, %r9
	leaq	(,%rdi,8), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$3, %rdi
	leaq	(%rdi,%rdi,2), %r10
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB8_7:                                # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdx), %rdx
	movl	(%rdx), %r8d
	leaq	24(%rbx,%r10), %rcx
	leaq	(%rbx,%rsi), %rdi
	movq	%rcx, 8(%rbx,%rsi)
	movq	%rdi, 40(%rbx,%r10)
	movl	%r8d, (%rbx,%rsi)
	movq	8(%rdx), %rdx
	movl	(%rdx), %r8d
	leaq	48(%rbx,%r10), %rdi
	leaq	24(%rbx,%rsi), %rcx
	movq	%rdi, 32(%rbx,%rsi)
	movq	%rcx, 64(%rbx,%r10)
	movl	%r8d, 24(%rbx,%rsi)
	addq	$48, %rbx
	addq	$-2, %r9
	jne	.LBB8_7
.LBB8_8:                                # %._crit_edge
	leaq	(%rax,%r15,8), %rcx
	movq	%rcx, 16(%rax)
	movq	%rax, 8(%rax,%r15,8)
	movq	16(%r14), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rax,%r15,8)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	list_copy, .Lfunc_end8-list_copy
	.cfi_endproc

	.globl	list_reverse
	.p2align	4, 0x90
	.type	list_reverse,@function
list_reverse:                           # @list_reverse
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB9_1:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	movq	%rdx, 8(%rax)
	movq	%rcx, 16(%rax)
	cmpq	%rdi, %rcx
	movq	%rcx, %rax
	jne	.LBB9_1
# BB#2:
	retq
.Lfunc_end9:
	.size	list_reverse, .Lfunc_end9-list_reverse
	.cfi_endproc

	.globl	test_lists
	.p2align	4, 0x90
	.type	test_lists,@function
test_lists:                             # @test_lists
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r12, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movl	$2424, %edi             # imm = 0x978
	callq	malloc
	movq	%rax, %r14
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB10_1:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r14,%rax), %rdx
	leaq	24(%r14,%rax), %rsi
	movq	%rsi, 8(%r14,%rax)
	movq	%rdx, 40(%r14,%rax)
	movl	%ecx, (%r14,%rax)
	leaq	48(%r14,%rax), %rdx
	movq	%rdx, 32(%r14,%rax)
	movq	%rsi, 64(%r14,%rax)
	leal	1(%rcx), %edx
	movl	%edx, 24(%r14,%rax)
	addq	$48, %rax
	addl	$2, %ecx
	cmpq	$2400, %rax             # imm = 0x960
	jne	.LBB10_1
# BB#2:                                 # %list_sequence.exit
	leaq	2400(%r14), %rax
	movq	%rax, 16(%r14)
	movq	%r14, 2408(%r14)
	leaq	2376(%r14), %rax
	movq	%rax, 2416(%r14)
	movl	$100, 2400(%r14)
	movl	$100, (%r14)
	movl	$2424, %edi             # imm = 0x978
	callq	malloc
	movq	%rax, %rbx
	leaq	24(%rbx), %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, 40(%rbx)
	movl	$100, (%rbx)
	leaq	8(%r14), %r12
	xorl	%eax, %eax
	movq	%r12, %rcx
	jmp	.LBB10_3
	.p2align	4, 0x90
.LBB10_42:                              # %.lr.ph..lr.ph_crit_edge.i.1
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	8(%rcx), %rcx
	movl	(%rcx), %esi
	leaq	72(%rbx,%rax), %rdi
	movq	%rdi, 56(%rbx,%rax)
	movq	%rdx, 88(%rbx,%rax)
	movl	%esi, 48(%rbx,%rax)
	addq	$8, %rcx
	addq	$48, %rax
.LBB10_3:                               # %.lr.ph..lr.ph_crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	movl	(%rcx), %esi
	leaq	48(%rbx,%rax), %rdx
	leaq	24(%rbx,%rax), %rdi
	movq	%rdx, 32(%rbx,%rax)
	movq	%rdi, 64(%rbx,%rax)
	movl	%esi, 24(%rbx,%rax)
	cmpq	$2352, %rax             # imm = 0x930
	jne	.LBB10_42
# BB#4:                                 # %list_copy.exit
	leaq	2400(%rbx), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, 2408(%rbx)
	movl	2400(%r14), %eax
	movl	%eax, 2400(%rbx)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, 8(%r15)
	movq	%r15, 16(%r15)
	movl	$0, (%r15)
	movq	8(%rbx), %rdx
	movl	(%rbx), %edi
	cmpl	$100, %edi
	setne	%al
	cmpq	%rbx, %rdx
	je	.LBB10_5
# BB#6:                                 # %.lr.ph.i52.preheader
	movq	%rdx, %rsi
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_7:                               # %.lr.ph.i52
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %al
	jne	.LBB10_22
# BB#8:                                 #   in Loop: Header=BB10_7 Depth=1
	movq	8(%rcx), %rcx
	movl	(%rsi), %eax
	movq	8(%rsi), %rsi
	cmpl	(%rcx), %eax
	setne	%al
	cmpq	%rbx, %rsi
	jne	.LBB10_7
	jmp	.LBB10_9
.LBB10_5:
	movq	%r14, %rcx
.LBB10_9:                               # %._crit_edge.i
	testb	%al, %al
	jne	.LBB10_22
# BB#10:                                # %list_equal.exit
	cmpq	%r14, 8(%rcx)
	jne	.LBB10_22
# BB#11:                                # %.preheader67
	xorl	%ecx, %ecx
	testl	%edi, %edi
	je	.LBB10_17
# BB#12:                                # %list_pop_head.exit.lr.ph
	movq	8(%rdx), %rax
	movq	%rax, 8(%rbx)
	movq	8(%rdx), %rax
	movq	%rbx, 16(%rax)
	movl	%edi, %esi
	decl	%esi
	movq	16(%r15), %rax
	movq	%rdx, 8(%rax)
	movq	%r15, 8(%rdx)
	movq	%rdx, 16(%r15)
	movq	%rax, 16(%rdx)
	je	.LBB10_16
# BB#13:                                # %list_pop_head.exit.list_pop_head.exit_crit_edge.preheader
	testb	$1, %sil
	je	.LBB10_15
# BB#14:                                # %list_pop_head.exit.list_pop_head.exit_crit_edge.prol
	movq	8(%rbx), %rax
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movq	8(%rax), %rdx
	movq	%rbx, 16(%rdx)
	leal	-2(%rdi), %esi
	movq	16(%r15), %rdx
	movq	%rax, 8(%rdx)
	movq	%r15, 8(%rax)
	movq	%rax, 16(%r15)
	movq	%rdx, 16(%rax)
.LBB10_15:                              # %list_pop_head.exit.list_pop_head.exit_crit_edge.prol.loopexit
	cmpl	$2, %edi
	je	.LBB10_16
	.p2align	4, 0x90
.LBB10_43:                              # %list_pop_head.exit.list_pop_head.exit_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movq	8(%rax), %rdx
	movq	%rbx, 16(%rdx)
	movq	16(%r15), %rdx
	movq	%rax, 8(%rdx)
	movq	%r15, 8(%rax)
	movq	%rax, 16(%r15)
	movq	%rdx, 16(%rax)
	movq	8(%rbx), %rax
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movq	8(%rax), %rdx
	movq	%rbx, 16(%rdx)
	addl	$-2, %esi
	movq	16(%r15), %rdx
	movq	%rax, 8(%rdx)
	movq	%r15, 8(%rax)
	movq	%rax, 16(%r15)
	movq	%rdx, 16(%rax)
	jne	.LBB10_43
.LBB10_16:                              # %.preheader66
	movl	$0, (%rbx)
	movl	%edi, (%r15)
	testl	%edi, %edi
	je	.LBB10_17
# BB#24:                                # %list_pop_tail.exit.lr.ph
	testb	$1, %dil
	movl	%edi, %ecx
	je	.LBB10_26
# BB#25:                                # %list_pop_tail.exit.prol
	movq	16(%r15), %rax
	movq	16(%rax), %rcx
	movq	%r15, 8(%rcx)
	movq	%rcx, 16(%r15)
	leal	-1(%rdi), %ecx
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rbx, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rdx, 16(%rax)
.LBB10_26:                              # %list_pop_tail.exit.prol.loopexit
	cmpl	$1, %edi
	je	.LBB10_27
	.p2align	4, 0x90
.LBB10_28:                              # %list_pop_tail.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movq	16(%rax), %rdx
	movq	%r15, 8(%rdx)
	movq	%rdx, 16(%r15)
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rbx, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rdx, 16(%rax)
	movq	16(%r15), %rax
	movq	16(%rax), %rdx
	movq	%r15, 8(%rdx)
	movq	%rdx, 16(%r15)
	addl	$-2, %ecx
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rbx, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rdx, 16(%rax)
	jne	.LBB10_28
.LBB10_27:                              # %..preheader_crit_edge
	movl	$0, (%r15)
	movl	%edi, (%rbx)
	movl	%edi, %ecx
.LBB10_17:                              # %.preheader.preheader
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB10_18:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rdx
	movq	16(%rax), %rsi
	movq	%rsi, 8(%rax)
	movq	%rdx, 16(%rax)
	cmpq	%r14, %rdx
	movq	%rdx, %rax
	jne	.LBB10_18
# BB#19:                                # %list_reverse.exit
	movq	(%r12), %rax
	movl	(%rax), %edx
	cmpl	$100, %edx
	jne	.LBB10_20
# BB#29:
	movq	16(%r14), %rdx
	movl	(%rdx), %edx
	cmpl	$1, %edx
	jne	.LBB10_30
# BB#31:
	movq	8(%rbx), %rdx
	movl	(%rdx), %edx
	cmpl	$100, %edx
	jne	.LBB10_32
# BB#33:
	movq	16(%rbx), %rdx
	movl	(%rdx), %edx
	cmpl	$1, %edx
	jne	.LBB10_30
# BB#34:
	cmpl	$100, %ecx
	setne	%dl
	movq	%rbx, %rcx
	cmpq	%r14, %rax
	jne	.LBB10_36
	jmp	.LBB10_38
	.p2align	4, 0x90
.LBB10_37:                              #   in Loop: Header=BB10_36 Depth=1
	movq	8(%rcx), %rcx
	movl	(%rax), %edx
	movq	8(%rax), %rax
	cmpl	(%rcx), %edx
	setne	%dl
	cmpq	%r14, %rax
	je	.LBB10_38
.LBB10_36:                              # %.lr.ph.i54
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %dl
	je	.LBB10_37
.LBB10_40:                              # %list_equal.exit59.thread
	movl	$.Lstr.4, %edi
	jmp	.LBB10_23
.LBB10_38:                              # %._crit_edge.i57
	testb	%dl, %dl
	jne	.LBB10_40
# BB#39:                                # %list_equal.exit59
	cmpq	%rbx, 8(%rcx)
	jne	.LBB10_40
# BB#41:
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movl	$100, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB10_22:                              # %list_equal.exit.thread
	movl	$.Lstr.1, %edi
.LBB10_23:                              # %list_equal.exit.thread
	callq	puts
	movl	$1, %edi
	callq	exit
.LBB10_30:
	movl	$.L.str.8, %edi
	jmp	.LBB10_21
.LBB10_20:
	movl	$.L.str.7, %edi
	jmp	.LBB10_21
.LBB10_32:
	movl	$.L.str.9, %edi
.LBB10_21:
	movl	$100, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %edi
	callq	exit
.Lfunc_end10:
	.size	test_lists, .Lfunc_end10-test_lists
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movl	$3000000, %ebx          # imm = 0x2DC6C0
	cmpl	$2, %edi
	jne	.LBB11_2
# BB#1:                                 # %.preheader
	movq	8(%rsi), %rdi
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
	testl	%ebx, %ebx
	je	.LBB11_3
	.p2align	4, 0x90
.LBB11_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	test_lists
	movl	%eax, %r14d
	decl	%ebx
	jne	.LBB11_2
.LBB11_3:                               # %._crit_edge
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	main, .Lfunc_end11-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"length: %d\n"
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"i:%3d  v:%3d  n:%3d  p:%3d\n"
	.size	.L.str.1, 28

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"[val of next of tail is:  %d]\n"
	.size	.L.str.3, 31

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"li1 first value wrong, wanted %d, got %d\n"
	.size	.L.str.7, 42

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"last value wrong, wanted %d, got %d\n"
	.size	.L.str.8, 37

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"li2 first value wrong, wanted %d, got %d\n"
	.size	.L.str.9, 42

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%d\n"
	.size	.L.str.12, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"[last entry points to list head]"
	.size	.Lstr, 33

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"li2 and li1 are not equal"
	.size	.Lstr.1, 26

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"li1 and li2 are not equal"
	.size	.Lstr.4, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
