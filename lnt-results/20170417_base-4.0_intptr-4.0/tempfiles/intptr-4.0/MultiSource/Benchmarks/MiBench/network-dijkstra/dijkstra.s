	.text
	.file	"dijkstra.bc"
	.globl	print_path
	.p2align	4, 0x90
	.type	print_path,@function
print_path:                             # @print_path
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%esi, %ebx
	movslq	%ebx, %rax
	movl	4(%rdi,%rax,8), %esi
	cmpl	$9999, %esi             # imm = 0x270F
	je	.LBB0_2
# BB#1:
	callq	print_path
.LBB0_2:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	popq	%rbx
	jmp	fflush                  # TAILCALL
.Lfunc_end0:
	.size	print_path, .Lfunc_end0-print_path
	.cfi_endproc

	.globl	enqueue
	.p2align	4, 0x90
	.type	enqueue,@function
enqueue:                                # @enqueue
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movl	%edi, %ebx
	movl	$24, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB1_6
# BB#1:
	movq	qHead(%rip), %rdx
	movl	%ebx, (%rax)
	movl	%ebp, 4(%rax)
	movl	%r14d, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.LBB1_2
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB1_3
# BB#4:
	addq	$16, %rcx
	jmp	.LBB1_5
.LBB1_2:
	movl	$qHead, %ecx
.LBB1_5:
	movq	%rax, (%rcx)
	incl	g_qCount(%rip)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB1_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	enqueue, .Lfunc_end1-enqueue
	.cfi_endproc

	.globl	dequeue
	.p2align	4, 0x90
	.type	dequeue,@function
dequeue:                                # @dequeue
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	qHead(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 16
	movl	(%rdi), %ecx
	movl	%ecx, (%rax)
	movl	4(%rdi), %eax
	movl	%eax, (%rsi)
	movl	8(%rdi), %eax
	movl	%eax, (%rdx)
	movq	16(%rdi), %rax
	movq	%rax, qHead(%rip)
	callq	free
	decl	g_qCount(%rip)
	addq	$8, %rsp
.LBB2_2:
	retq
.Lfunc_end2:
	.size	dequeue, .Lfunc_end2-dequeue
	.cfi_endproc

	.globl	qcount
	.p2align	4, 0x90
	.type	qcount,@function
qcount:                                 # @qcount
	.cfi_startproc
# BB#0:
	movl	g_qCount(%rip), %eax
	retq
.Lfunc_end3:
	.size	qcount, .Lfunc_end3-qcount
	.cfi_endproc

	.globl	dijkstra
	.p2align	4, 0x90
	.type	dijkstra,@function
dijkstra:                               # @dijkstra
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 64
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %ebp
	movl	$0, ch(%rip)
	movq	$-800, %rax             # imm = 0xFCE0
	movabsq	$42945378002703, %rcx   # imm = 0x270F0000270F
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, rgnNodes+800(%rax)
	movq	%rcx, rgnNodes+808(%rax)
	movq	%rcx, rgnNodes+816(%rax)
	movq	%rcx, rgnNodes+824(%rax)
	movq	%rcx, rgnNodes+832(%rax)
	addq	$40, %rax
	jne	.LBB4_1
# BB#2:
	movl	$100, ch(%rip)
	cmpl	%ebx, %ebp
	jne	.LBB4_4
# BB#3:
	movl	$.Lstr, %edi
	callq	puts
	jmp	.LBB4_11
.LBB4_4:
	movslq	%ebp, %rax
	movabsq	$42945377992704, %rcx   # imm = 0x270F00000000
	movq	%rcx, rgnNodes(,%rax,8)
	movl	$24, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB4_26
# BB#5:
	movq	qHead(%rip), %rdx
	movl	%ebp, (%rax)
	movl	$0, 4(%rax)
	movl	$9999, 8(%rax)          # imm = 0x270F
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	je	.LBB4_6
	.p2align	4, 0x90
.LBB4_7:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	16(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB4_7
# BB#8:
	addq	$16, %rcx
	jmp	.LBB4_9
.LBB4_6:
	movl	$qHead, %ecx
.LBB4_9:                                # %enqueue.exit
	movq	%rax, (%rcx)
	movl	g_qCount(%rip), %eax
	leal	1(%rax), %r12d
	movl	%r12d, g_qCount(%rip)
	testl	%eax, %eax
	js	.LBB4_10
	.p2align	4, 0x90
.LBB4_13:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_16 Depth 2
                                        #       Child Loop BB4_22 Depth 3
	movq	qHead(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_15
# BB#14:                                #   in Loop: Header=BB4_13 Depth=1
	movl	(%rdi), %eax
	movl	%eax, iNode(%rip)
	movl	4(%rdi), %eax
	movl	%eax, iDist(%rip)
	movl	8(%rdi), %eax
	movl	%eax, iPrev(%rip)
	movq	16(%rdi), %rax
	movq	%rax, qHead(%rip)
	callq	free
	movl	g_qCount(%rip), %r12d
	decl	%r12d
	movl	%r12d, g_qCount(%rip)
.LBB4_15:                               # %dequeue.exit.preheader
                                        #   in Loop: Header=BB4_13 Depth=1
	movl	$0, i(%rip)
	movl	iNode(%rip), %r15d
	movslq	%r15d, %rbx
	xorl	%ebp, %ebp
	movl	iDist(%rip), %r13d
	.p2align	4, 0x90
.LBB4_16:                               #   Parent Loop BB4_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_22 Depth 3
	movslq	%ebp, %rax
	imulq	$400, %rbx, %rcx        # imm = 0x190
	movl	AdjMatrix(%rcx,%rax,4), %r14d
	movl	%r14d, iCost(%rip)
	cmpl	$9999, %r14d            # imm = 0x270F
	je	.LBB4_25
# BB#17:                                #   in Loop: Header=BB4_16 Depth=2
	movl	rgnNodes(,%rax,8), %ecx
	addl	%r13d, %r14d
	cmpl	$9999, %ecx             # imm = 0x270F
	je	.LBB4_19
# BB#18:                                #   in Loop: Header=BB4_16 Depth=2
	cmpl	%r14d, %ecx
	jle	.LBB4_25
.LBB4_19:                               #   in Loop: Header=BB4_16 Depth=2
	movl	%r14d, rgnNodes(,%rax,8)
	movl	%r15d, rgnNodes+4(,%rax,8)
	movl	$24, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB4_26
# BB#20:                                #   in Loop: Header=BB4_16 Depth=2
	movq	qHead(%rip), %rcx
	movl	%ebp, (%rax)
	movl	%r14d, 4(%rax)
	movl	%r15d, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.LBB4_21
	.p2align	4, 0x90
.LBB4_22:                               # %.preheader.i10
                                        #   Parent Loop BB4_13 Depth=1
                                        #     Parent Loop BB4_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rdx
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB4_22
# BB#23:                                #   in Loop: Header=BB4_16 Depth=2
	addq	$16, %rdx
	jmp	.LBB4_24
.LBB4_21:                               #   in Loop: Header=BB4_16 Depth=2
	movl	$qHead, %edx
.LBB4_24:                               # %enqueue.exit12
                                        #   in Loop: Header=BB4_16 Depth=2
	movq	%rax, (%rdx)
	incl	%r12d
	movl	%r12d, g_qCount(%rip)
.LBB4_25:                               # %dequeue.exit
                                        #   in Loop: Header=BB4_16 Depth=2
	incl	%ebp
	movl	%ebp, i(%rip)
	cmpl	$100, %ebp
	jl	.LBB4_16
# BB#12:                                # %thread-pre-split
                                        #   in Loop: Header=BB4_13 Depth=1
	testl	%r12d, %r12d
	jg	.LBB4_13
.LBB4_10:                               # %._crit_edge
	movl	4(%rsp), %ebx           # 4-byte Reload
	movslq	%ebx, %rax
	movl	rgnNodes(,%rax,8), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$rgnNodes, %edi
	movl	%ebx, %esi
	callq	print_path
	movl	$10, %edi
	callq	putchar
.LBB4_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_26:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	dijkstra, .Lfunc_end4-dijkstra
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 64
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	cmpl	$1, %edi
	jle	.LBB5_1
.LBB5_2:
	movq	8(%rbx), %rdi
	movl	$.L.str.8, %esi
	callq	fopen
	movq	%rax, %r13
	movl	$AdjMatrix, %r15d
	leaq	4(%rsp), %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB5_3:                                # %.preheader18
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
	movl	$100, %ebx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB5_4:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r14, %rdx
	callq	fscanf
	movl	4(%rsp), %eax
	movl	%eax, (%rbp)
	addq	$4, %rbp
	decq	%rbx
	jne	.LBB5_4
# BB#5:                                 #   in Loop: Header=BB5_3 Depth=1
	incq	%r12
	addq	$400, %r15              # imm = 0x190
	cmpq	$100, %r12
	jne	.LBB5_3
# BB#6:                                 # %.preheader.preheader
	movl	$50, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_7:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movslq	%r14d, %rsi
	imulq	$1374389535, %rsi, %rax # imm = 0x51EB851F
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$37, %rax
	addl	%ecx, %eax
	imull	$100, %eax, %ebp
	subl	%ebp, %esi
	negl	%ebp
	movl	%ebx, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	dijkstra
	incl	%ebx
	leal	1(%r14,%rbp), %r14d
	cmpl	$100, %ebx
	jne	.LBB5_7
# BB#8:
	xorl	%edi, %edi
	callq	exit
.LBB5_1:
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$40, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB5_2
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc

	.type	qHead,@object           # @qHead
	.bss
	.globl	qHead
	.p2align	3
qHead:
	.quad	0
	.size	qHead, 8

	.type	g_qCount,@object        # @g_qCount
	.globl	g_qCount
	.p2align	2
g_qCount:
	.long	0                       # 0x0
	.size	g_qCount, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" %d"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Out of memory.\n"
	.size	.L.str.1, 16

	.type	ch,@object              # @ch
	.comm	ch,4,4
	.type	rgnNodes,@object        # @rgnNodes
	.comm	rgnNodes,800,16
	.type	iNode,@object           # @iNode
	.comm	iNode,4,4
	.type	iDist,@object           # @iDist
	.comm	iDist,4,4
	.type	iPrev,@object           # @iPrev
	.comm	iPrev,4,4
	.type	i,@object               # @i
	.comm	i,4,4
	.type	AdjMatrix,@object       # @AdjMatrix
	.comm	AdjMatrix,40000,16
	.type	iCost,@object           # @iCost
	.comm	iCost,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Shortest path is %d in cost. "
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Path is: "
	.size	.L.str.4, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Usage: dijkstra <filename>\n"
	.size	.L.str.6, 28

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Only supports matrix size is #define'd.\n"
	.size	.L.str.7, 41

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"r"
	.size	.L.str.8, 2

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%d"
	.size	.L.str.9, 3

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Shortest path is 0 in cost. Just stay where you are."
	.size	.Lstr, 53


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
