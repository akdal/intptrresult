	.text
	.file	"PpmdRegister.bc"
	.p2align	4, 0x90
	.type	_ZL11CreateCodecv,@function
_ZL11CreateCodecv:                      # @_ZL11CreateCodecv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$19376, %edi            # imm = 0x4BB0
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CDecoderC2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZL11CreateCodecv, .Lfunc_end0-_ZL11CreateCodecv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL14CreateCodecOutv,@function
_ZL14CreateCodecOutv:                   # @_ZL14CreateCodecOutv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movl	$19328, %edi            # imm = 0x4B80
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NPpmd8CEncoderC1Ev
.Ltmp4:
# BB#1:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_2:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZL14CreateCodecOutv, .Lfunc_end1-_ZL14CreateCodecOutv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9NCompress5NPpmd8CDecoderC2Ev,"axG",@progbits,_ZN9NCompress5NPpmd8CDecoderC2Ev,comdat
	.weak	_ZN9NCompress5NPpmd8CDecoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NPpmd8CDecoderC2Ev,@function
_ZN9NCompress5NPpmd8CDecoderC2Ev:       # @_ZN9NCompress5NPpmd8CDecoderC2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 40(%rbx)
	movl	$_ZTVN9NCompress5NPpmd8CDecoderE+120, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NPpmd8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN9NCompress5NPpmd8CDecoderE+256, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NPpmd8CDecoderE+184, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	$_ZTVN9NCompress5NPpmd8CDecoderE+320, 32(%rbx)
	movq	$0, 48(%rbx)
	leaq	96(%rbx), %r14
.Ltmp6:
	movq	%r14, %rdi
	callq	_ZN14CByteInBufWrapC1Ev
.Ltmp7:
# BB#1:
	movb	$0, 19345(%rbx)
	movq	$0, 19368(%rbx)
	leaq	56(%rbx), %rdi
.Ltmp9:
	callq	Ppmd7z_RangeDec_CreateVTable
.Ltmp10:
# BB#2:
	movq	%r14, 88(%rbx)
	leaq	160(%rbx), %rdi
.Ltmp11:
	callq	Ppmd7_Construct
.Ltmp12:
# BB#3:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB2_4:
.Ltmp8:
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB2_5:
.Ltmp13:
	movq	%rax, %r15
	movq	19368(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp14:
	callq	*16(%rax)
.Ltmp15:
.LBB2_7:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
.Ltmp16:
	movq	%r14, %rdi
	callq	_ZN14CByteInBufWrap4FreeEv
.Ltmp17:
# BB#8:                                 # %_ZN14CByteInBufWrapD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB2_9:
.Ltmp18:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN9NCompress5NPpmd8CDecoderC2Ev, .Lfunc_end2-_ZN9NCompress5NPpmd8CDecoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin2    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin2   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp12         #   Call between .Ltmp12 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp14         #   Call between .Ltmp14 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin2   #     jumps to .Ltmp18
	.byte	1                       #   On action: 1
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp17     #   Call between .Ltmp17 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_PpmdRegister.ii,@function
_GLOBAL__sub_I_PpmdRegister.ii:         # @_GLOBAL__sub_I_PpmdRegister.ii
	.cfi_startproc
# BB#0:
	movl	$_ZL11g_CodecInfo, %edi
	jmp	_Z13RegisterCodecPK10CCodecInfo # TAILCALL
.Lfunc_end4:
	.size	_GLOBAL__sub_I_PpmdRegister.ii, .Lfunc_end4-_GLOBAL__sub_I_PpmdRegister.ii
	.cfi_endproc

	.type	_ZL11g_CodecInfo,@object # @_ZL11g_CodecInfo
	.data
	.p2align	3
_ZL11g_CodecInfo:
	.quad	_ZL11CreateCodecv
	.quad	_ZL14CreateCodecOutv
	.quad	197633                  # 0x30401
	.quad	.L.str
	.long	1                       # 0x1
	.byte	0                       # 0x0
	.zero	3
	.size	_ZL11g_CodecInfo, 40

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	80                      # 0x50
	.long	80                      # 0x50
	.long	77                      # 0x4d
	.long	68                      # 0x44
	.long	0                       # 0x0
	.size	.L.str, 20

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_PpmdRegister.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
