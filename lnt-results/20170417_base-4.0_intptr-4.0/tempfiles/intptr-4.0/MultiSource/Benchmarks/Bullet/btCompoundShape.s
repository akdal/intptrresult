	.text
	.file	"btCompoundShape.bc"
	.globl	_ZN15btCompoundShapeC2Eb
	.p2align	4, 0x90
	.type	_ZN15btCompoundShapeC2Eb,@function
_ZN15btCompoundShapeC2Eb:               # @_ZN15btCompoundShapeC2Eb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$0, 16(%rbx)
	movq	$_ZTV15btCompoundShape+16, (%rbx)
	movb	$1, 48(%rbx)
	movq	$0, 40(%rbx)
	movl	$0, 28(%rbx)
	movl	$0, 32(%rbx)
	movl	$1566444395, 56(%rbx)   # imm = 0x5D5E0B6B
	movl	$1566444395, 60(%rbx)   # imm = 0x5D5E0B6B
	movl	$1566444395, 64(%rbx)   # imm = 0x5D5E0B6B
	movl	$0, 68(%rbx)
	movl	$-581039253, 72(%rbx)   # imm = 0xDD5E0B6B
	movl	$-581039253, 76(%rbx)   # imm = 0xDD5E0B6B
	movl	$-581039253, 80(%rbx)   # imm = 0xDD5E0B6B
	movl	$0, 84(%rbx)
	movq	$0, 88(%rbx)
	movl	$1, 96(%rbx)
	movl	$0, 100(%rbx)
	movl	$1065353216, 104(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 108(%rbx)  # imm = 0x3F800000
	movl	$1065353216, 112(%rbx)  # imm = 0x3F800000
	movl	$0, 116(%rbx)
	movl	$31, 8(%rbx)
	testb	%sil, %sil
	je	.LBB0_4
# BB#1:
.Ltmp0:
	movl	$64, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
.Ltmp1:
# BB#2:
.Ltmp3:
	movq	%r14, %rdi
	callq	_ZN6btDbvtC1Ev
.Ltmp4:
# BB#3:
	movq	%r14, 88(%rbx)
.LBB0_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_12:
.Ltmp5:
	jmp	.LBB0_6
.LBB0_5:
.Ltmp2:
.LBB0_6:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_10
# BB#7:
	cmpb	$0, 48(%rbx)
	je	.LBB0_9
# BB#8:
.Ltmp6:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp7:
.LBB0_9:                                # %.noexc
	movq	$0, 40(%rbx)
.LBB0_10:
	movb	$1, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 28(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_11:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN15btCompoundShapeC2Eb, .Lfunc_end0-_ZN15btCompoundShapeC2Eb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN15btCompoundShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN15btCompoundShapeD2Ev,@function
_ZN15btCompoundShapeD2Ev:               # @_ZN15btCompoundShapeD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV15btCompoundShape+16, (%rbx)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_3
# BB#1:
.Ltmp9:
	callq	_ZN6btDbvtD1Ev
.Ltmp10:
# BB#2:
	movq	88(%rbx), %rdi
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
.LBB2_3:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_7
# BB#4:
	cmpb	$0, 48(%rbx)
	je	.LBB2_6
# BB#5:
.Ltmp17:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp18:
.LBB2_6:                                # %.noexc4
	movq	$0, 40(%rbx)
.LBB2_7:
	movb	$1, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 28(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB2_13:
.Ltmp19:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_8:
.Ltmp13:
	movq	%rax, %r14
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#9:
	cmpb	$0, 48(%rbx)
	je	.LBB2_11
# BB#10:
.Ltmp14:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp15:
.LBB2_11:                               # %.noexc
	movq	$0, 40(%rbx)
.LBB2_12:                               # %_ZN20btAlignedObjectArrayI20btCompoundShapeChildED2Ev.exit
	movb	$1, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 28(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_15:
.Ltmp16:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN15btCompoundShapeD2Ev, .Lfunc_end2-_ZN15btCompoundShapeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp12-.Ltmp9          #   Call between .Ltmp9 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin1   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp18         #   Call between .Ltmp18 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin1   #     jumps to .Ltmp16
	.byte	1                       #   On action: 1
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end2-.Ltmp15     #   Call between .Ltmp15 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN15btCompoundShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN15btCompoundShapeD0Ev,@function
_ZN15btCompoundShapeD0Ev:               # @_ZN15btCompoundShapeD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp20:
	callq	_ZN15btCompoundShapeD2Ev
.Ltmp21:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB3_2:
.Ltmp22:
	movq	%rax, %r14
.Ltmp23:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp24:
# BB#3:                                 # %_ZN15btCompoundShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_4:
.Ltmp25:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN15btCompoundShapeD0Ev, .Lfunc_end3-_ZN15btCompoundShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp23-.Ltmp21         #   Call between .Ltmp21 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp24     #   Call between .Ltmp24 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape
	.p2align	4, 0x90
	.type	_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape,@function
_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape: # @_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 32
	subq	$160, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 192
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	incl	96(%r14)
	movups	(%r15), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	16(%r15), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	32(%r15), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	48(%r15), %xmm0
	movaps	%xmm0, 80(%rsp)
	movq	%rbx, 96(%rsp)
	movl	8(%rbx), %eax
	movl	%eax, 104(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movss	%xmm0, 108(%rsp)
	movq	(%rbx), %rax
	leaq	16(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	*16(%rax)
	movss	56(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB4_2
# BB#1:
	movss	%xmm0, 56(%r14)
.LBB4_2:
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	72(%r14), %xmm0
	jbe	.LBB4_4
# BB#3:
	movss	%xmm0, 72(%r14)
.LBB4_4:
	movss	60(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB4_6
# BB#5:
	movss	%xmm0, 60(%r14)
.LBB4_6:
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	76(%r14), %xmm0
	jbe	.LBB4_8
# BB#7:
	movss	%xmm0, 76(%r14)
.LBB4_8:
	movss	64(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB4_10
# BB#9:
	movss	%xmm0, 64(%r14)
.LBB4_10:
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	80(%r14), %xmm0
	jbe	.LBB4_12
# BB#11:
	movss	%xmm0, 80(%r14)
.LBB4_12:
	movq	88(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_14
# BB#13:
	movups	16(%rsp), %xmm0
	movaps	%xmm0, 128(%rsp)
	movups	(%rsp), %xmm0
	movaps	%xmm0, 144(%rsp)
	movslq	28(%r14), %rdx
	leaq	128(%rsp), %rsi
	callq	_ZN6btDbvt6insertERK12btDbvtAabbMmPv
	movq	%rax, 112(%rsp)
.LBB4_14:
	addq	$24, %r14
	leaq	32(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_
	addq	$160, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape, .Lfunc_end4-_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_,"axG",@progbits,_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_,comdat
	.weak	_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_,@function
_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_: # @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	4(%r15), %eax
	cmpl	8(%r15), %eax
	jne	.LBB5_13
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB5_13
# BB#2:
	testl	%ebp, %ebp
	je	.LBB5_3
# BB#4:
	movslq	%ebp, %rax
	imulq	$88, %rax, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	4(%r15), %eax
	testl	%eax, %eax
	jg	.LBB5_6
	jmp	.LBB5_8
.LBB5_3:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB5_8
.LBB5_6:                                # %.lr.ph.i.i
	cltq
	movl	$64, %ecx
	.p2align	4, 0x90
.LBB5_7:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	-64(%rdx,%rcx), %xmm0
	movups	%xmm0, -64(%rbx,%rcx)
	movups	-48(%rdx,%rcx), %xmm0
	movups	%xmm0, -48(%rbx,%rcx)
	movups	-32(%rdx,%rcx), %xmm0
	movups	%xmm0, -32(%rbx,%rcx)
	movups	-16(%rdx,%rcx), %xmm0
	movups	%xmm0, -16(%rbx,%rcx)
	movq	16(%rdx,%rcx), %rsi
	movq	%rsi, 16(%rbx,%rcx)
	movups	(%rdx,%rcx), %xmm0
	movups	%xmm0, (%rbx,%rcx)
	addq	$88, %rcx
	decq	%rax
	jne	.LBB5_7
.LBB5_8:                                # %_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4copyEiiPS0_.exit.i
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_12
# BB#9:
	cmpb	$0, 24(%r15)
	je	.LBB5_11
# BB#10:
	callq	_Z21btAlignedFreeInternalPv
.LBB5_11:
	movq	$0, 16(%r15)
.LBB5_12:                               # %_ZN20btAlignedObjectArrayI20btCompoundShapeChildE10deallocateEv.exit.i
	movb	$1, 24(%r15)
	movq	%rbx, 16(%r15)
	movl	%ebp, 8(%r15)
	movl	4(%r15), %eax
.LBB5_13:
	movq	16(%r15), %rcx
	cltq
	imulq	$88, %rax, %rax
	movups	(%r14), %xmm0
	movups	%xmm0, (%rcx,%rax)
	movups	16(%r14), %xmm0
	movups	%xmm0, 16(%rcx,%rax)
	movups	32(%r14), %xmm0
	movups	%xmm0, 32(%rcx,%rax)
	movups	48(%r14), %xmm0
	movups	%xmm0, 48(%rcx,%rax)
	movq	80(%r14), %rdx
	movq	%rdx, 80(%rcx,%rax)
	movups	64(%r14), %xmm0
	movups	%xmm0, 64(%rcx,%rax)
	incl	4(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_, .Lfunc_end5-_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_
	.cfi_endproc

	.text
	.globl	_ZN15btCompoundShape20updateChildTransformEiRK11btTransform
	.p2align	4, 0x90
	.type	_ZN15btCompoundShape20updateChildTransformEiRK11btTransform,@function
_ZN15btCompoundShape20updateChildTransformEiRK11btTransform: # @_ZN15btCompoundShape20updateChildTransformEiRK11btTransform
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 96
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	40(%r14), %rax
	movslq	%esi, %rcx
	imulq	$88, %rcx, %rbx
	movups	(%rdx), %xmm0
	movups	%xmm0, (%rax,%rbx)
	movups	16(%rdx), %xmm0
	movups	%xmm0, 16(%rax,%rbx)
	movups	32(%rdx), %xmm0
	movups	%xmm0, 32(%rax,%rbx)
	movups	48(%rdx), %xmm0
	movups	%xmm0, 48(%rax,%rbx)
	cmpq	$0, 88(%r14)
	je	.LBB6_2
# BB#1:
	movq	40(%r14), %rax
	movq	64(%rax,%rbx), %rdi
	movq	(%rdi), %rax
	leaq	16(%rsp), %r8
	movq	%rsp, %rcx
	movq	%rdx, %rsi
	movq	%r8, %rdx
	callq	*16(%rax)
	movups	16(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	(%rsp), %xmm0
	movaps	%xmm0, 48(%rsp)
	movq	40(%r14), %rax
	movq	88(%r14), %rdi
	movq	80(%rax,%rbx), %rsi
	leaq	32(%rsp), %rdx
	callq	_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm
.LBB6_2:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*104(%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	_ZN15btCompoundShape20updateChildTransformEiRK11btTransform, .Lfunc_end6-_ZN15btCompoundShape20updateChildTransformEiRK11btTransform
	.cfi_endproc

	.globl	_ZN15btCompoundShape23removeChildShapeByIndexEi
	.p2align	4, 0x90
	.type	_ZN15btCompoundShape23removeChildShapeByIndexEi,@function
_ZN15btCompoundShape23removeChildShapeByIndexEi: # @_ZN15btCompoundShape23removeChildShapeByIndexEi
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 24
	subq	$88, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 112
.Lcfi39:
	.cfi_offset %rbx, -24
.Lcfi40:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	incl	96(%rbx)
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_1
# BB#2:
	movq	40(%rbx), %rax
	movslq	%esi, %r14
	imulq	$88, %r14, %rcx
	movq	80(%rax,%rcx), %rsi
	callq	_ZN6btDbvt6removeEP10btDbvtNode
	jmp	.LBB7_3
.LBB7_1:                                # %._crit_edge
	movslq	%esi, %r14
.LBB7_3:
	movslq	28(%rbx), %rax
	imulq	$88, %rax, %rax
	movq	40(%rbx), %rcx
	imulq	$88, %r14, %rdx
	movups	(%rcx,%rdx), %xmm0
	movaps	%xmm0, (%rsp)
	movups	16(%rcx,%rdx), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	32(%rcx,%rdx), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	48(%rcx,%rdx), %xmm0
	movaps	%xmm0, 48(%rsp)
	movq	80(%rcx,%rdx), %rsi
	movq	%rsi, 80(%rsp)
	movups	64(%rcx,%rdx), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	-88(%rcx,%rax), %xmm0
	movups	%xmm0, (%rcx,%rdx)
	movups	-72(%rcx,%rax), %xmm0
	movups	%xmm0, 16(%rcx,%rdx)
	movups	-56(%rcx,%rax), %xmm0
	movups	%xmm0, 32(%rcx,%rdx)
	movups	-40(%rcx,%rax), %xmm0
	movups	%xmm0, 48(%rcx,%rdx)
	movq	-8(%rcx,%rax), %rsi
	movq	%rsi, 80(%rcx,%rdx)
	movups	-24(%rcx,%rax), %xmm0
	movups	%xmm0, 64(%rcx,%rdx)
	movq	40(%rbx), %rcx
	movaps	(%rsp), %xmm0
	movups	%xmm0, -88(%rcx,%rax)
	movaps	16(%rsp), %xmm0
	movups	%xmm0, -72(%rcx,%rax)
	movaps	32(%rsp), %xmm0
	movups	%xmm0, -56(%rcx,%rax)
	movaps	48(%rsp), %xmm0
	movups	%xmm0, -40(%rcx,%rax)
	movq	80(%rsp), %rdx
	movq	%rdx, -8(%rcx,%rax)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, -24(%rcx,%rax)
	decl	28(%rbx)
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	_ZN15btCompoundShape23removeChildShapeByIndexEi, .Lfunc_end7-_ZN15btCompoundShape23removeChildShapeByIndexEi
	.cfi_endproc

	.globl	_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape
	.p2align	4, 0x90
	.type	_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape,@function
_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape: # @_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -48
.Lcfi47:
	.cfi_offset %r12, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	incl	96(%r15)
	movslq	28(%r15), %rax
	testq	%rax, %rax
	jle	.LBB8_4
# BB#1:                                 # %.lr.ph
	imulq	$88, %rax, %r12
	movq	%rax, %rbx
	incq	%rbx
	addq	$-24, %r12
	leal	-1(%rax), %ebp
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	40(%r15), %rax
	cmpq	%r14, (%rax,%r12)
	jne	.LBB8_3
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	_ZN15btCompoundShape23removeChildShapeByIndexEi
.LBB8_3:                                # %.backedge
                                        #   in Loop: Header=BB8_2 Depth=1
	decq	%rbx
	addq	$-88, %r12
	decl	%ebp
	cmpq	$1, %rbx
	jg	.LBB8_2
.LBB8_4:                                # %._crit_edge
	movq	(%r15), %rax
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*104(%rax)              # TAILCALL
.Lfunc_end8:
	.size	_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape, .Lfunc_end8-_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape
	.cfi_endproc

	.globl	_ZN15btCompoundShape20recalculateLocalAabbEv
	.p2align	4, 0x90
	.type	_ZN15btCompoundShape20recalculateLocalAabbEv,@function
_ZN15btCompoundShape20recalculateLocalAabbEv: # @_ZN15btCompoundShape20recalculateLocalAabbEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 80
.Lcfi57:
	.cfi_offset %rbx, -48
.Lcfi58:
	.cfi_offset %r12, -40
.Lcfi59:
	.cfi_offset %r13, -32
.Lcfi60:
	.cfi_offset %r14, -24
.Lcfi61:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$1566444395, 56(%rbx)   # imm = 0x5D5E0B6B
	movl	$1566444395, 60(%rbx)   # imm = 0x5D5E0B6B
	movl	$1566444395, 64(%rbx)   # imm = 0x5D5E0B6B
	movl	$0, 68(%rbx)
	movabsq	$-2495544585613341845, %rax # imm = 0xDD5E0B6BDD5E0B6B
	movq	%rax, 72(%rbx)
	movl	$3713928043, %eax       # imm = 0xDD5E0B6B
	movq	%rax, 80(%rbx)
	cmpl	$0, 28(%rbx)
	jle	.LBB9_15
# BB#1:                                 # %.lr.ph
	xorl	%r12d, %r12d
	leaq	16(%rsp), %r14
	movq	%rsp, %r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rsi
	movq	64(%rsi,%r12), %rdi
	addq	%r12, %rsi
	movq	(%rdi), %rax
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	*16(%rax)
	movss	56(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_4
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movss	%xmm0, 56(%rbx)
.LBB9_4:                                #   in Loop: Header=BB9_2 Depth=1
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	72(%rbx), %xmm0
	jbe	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
	movss	%xmm0, 72(%rbx)
.LBB9_6:                                #   in Loop: Header=BB9_2 Depth=1
	movss	60(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_2 Depth=1
	movss	%xmm0, 60(%rbx)
.LBB9_8:                                #   in Loop: Header=BB9_2 Depth=1
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	76(%rbx), %xmm0
	jbe	.LBB9_10
# BB#9:                                 #   in Loop: Header=BB9_2 Depth=1
	movss	%xmm0, 76(%rbx)
.LBB9_10:                               #   in Loop: Header=BB9_2 Depth=1
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_2 Depth=1
	movss	%xmm0, 64(%rbx)
.LBB9_12:                               #   in Loop: Header=BB9_2 Depth=1
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	ucomiss	80(%rbx), %xmm0
	jbe	.LBB9_14
# BB#13:                                #   in Loop: Header=BB9_2 Depth=1
	movss	%xmm0, 80(%rbx)
.LBB9_14:                               #   in Loop: Header=BB9_2 Depth=1
	incq	%r13
	movslq	28(%rbx), %rax
	addq	$88, %r12
	cmpq	%rax, %r13
	jl	.LBB9_2
.LBB9_15:                               # %._crit_edge
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN15btCompoundShape20recalculateLocalAabbEv, .Lfunc_end9-_ZN15btCompoundShape20recalculateLocalAabbEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI10_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_1:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 40
	subq	$88, %rsp
.Lcfi66:
	.cfi_def_cfa_offset 128
.Lcfi67:
	.cfi_offset %rbx, -40
.Lcfi68:
	.cfi_offset %r12, -32
.Lcfi69:
	.cfi_offset %r14, -24
.Lcfi70:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movss	80(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cmpl	$0, 28(%rbx)
	xorps	%xmm5, %xmm5
	xorps	%xmm4, %xmm4
	je	.LBB10_2
# BB#1:
	movsd	72(%rbx), %xmm5         # xmm5 = mem[0],zero
	movsd	56(%rbx), %xmm2         # xmm2 = mem[0],zero
	movaps	%xmm5, %xmm4
	subps	%xmm2, %xmm4
	movaps	.LCPI10_0(%rip), %xmm3  # xmm3 = <0.5,0.5,u,u>
	mulps	%xmm3, %xmm4
	addps	%xmm2, %xmm5
	mulps	%xmm3, %xmm5
.LBB10_2:
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	xorps	%xmm3, %xmm3
	xorps	%xmm2, %xmm2
	je	.LBB10_4
# BB#3:
	movaps	%xmm0, %xmm3
	subss	%xmm1, %xmm3
	movss	.LCPI10_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm0
	mulss	%xmm2, %xmm3
	mulss	%xmm2, %xmm0
	movaps	%xmm0, %xmm2
.LBB10_4:
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movq	(%rbx), %rax
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	addss	%xmm1, %xmm2
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm0, %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movq	%rbx, %rdi
	callq	*88(%rax)
	addss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movss	16(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%r12), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movaps	.LCPI10_2(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	movaps	%xmm6, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movaps	%xmm3, %xmm10
	andps	%xmm1, %xmm10
	movss	20(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	%xmm2, %xmm9
	andps	%xmm1, %xmm9
	movss	24(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	48(%rsp), %xmm11        # 16-byte Reload
	movaps	%xmm11, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	movaps	%xmm5, %xmm8
	andps	%xmm1, %xmm8
	movaps	%xmm6, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulps	%xmm7, %xmm2
	addps	%xmm4, %xmm2
	movss	32(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	andps	%xmm1, %xmm4
	addps	%xmm2, %xmm3
	movsd	48(%r12), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm3, %xmm2
	movss	36(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	andps	%xmm1, %xmm5
	addss	%xmm6, %xmm7
	movss	40(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	andps	%xmm3, %xmm1
	movaps	%xmm11, %xmm6
	mulss	%xmm3, %xmm6
	addss	%xmm7, %xmm6
	movaps	%xmm6, %xmm11
	movaps	16(%rsp), %xmm12        # 16-byte Reload
	movaps	%xmm12, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm10, %xmm3
	movaps	32(%rsp), %xmm7         # 16-byte Reload
	movaps	%xmm7, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm9, %xmm6
	addps	%xmm3, %xmm6
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm8, %xmm0
	addps	%xmm6, %xmm0
	mulss	%xmm12, %xmm4
	mulss	%xmm7, %xmm5
	movaps	%xmm11, %xmm6
	addss	56(%r12), %xmm6
	addss	%xmm4, %xmm5
	addss	%xmm5, %xmm1
	movaps	%xmm6, %xmm3
	subss	%xmm1, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm3, %xmm4            # xmm4 = xmm3[0],xmm4[1,2,3]
	movaps	%xmm2, %xmm3
	subps	%xmm0, %xmm3
	movlps	%xmm3, (%r15)
	movlps	%xmm4, 8(%r15)
	addps	%xmm2, %xmm0
	addss	%xmm6, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, (%r14)
	movlps	%xmm2, 8(%r14)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end10-_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1056964608              # float 0.5
.LCPI11_1:
	.long	1094713344              # float 12
	.text
	.globl	_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3: # @_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 16
	subq	$112, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 128
.Lcfi73:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movl	$1065353216, 48(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 52(%rsp)
	movl	$1065353216, 68(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 72(%rsp)
	movl	$1065353216, 88(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 92(%rsp)
	movl	$0, 108(%rsp)
	movq	(%rdi), %rax
	leaq	48(%rsp), %rsi
	leaq	32(%rsp), %rdx
	leaq	16(%rsp), %rcx
	callq	*16(%rax)
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	32(%rsp), %xmm0
	subss	36(%rsp), %xmm1
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	40(%rsp), %xmm2
	movss	.LCPI11_0(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm0
	addss	%xmm1, %xmm1
	addss	%xmm2, %xmm2
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	divss	.LCPI11_1(%rip), %xmm4
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	movaps	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm4, %xmm3
	movss	%xmm3, (%rbx)
	mulss	%xmm0, %xmm0
	addss	%xmm0, %xmm2
	mulss	%xmm4, %xmm2
	movss	%xmm2, 4(%rbx)
	addss	%xmm1, %xmm0
	mulss	%xmm4, %xmm0
	movss	%xmm0, 8(%rbx)
	addq	$112, %rsp
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end11-_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	1065353216              # float 1
.LCPI12_1:
	.long	925353388               # float 9.99999974E-6
	.text
	.globl	_ZNK15btCompoundShape31calculatePrincipalAxisTransformEPfR11btTransformR9btVector3
	.p2align	4, 0x90
	.type	_ZNK15btCompoundShape31calculatePrincipalAxisTransformEPfR11btTransformR9btVector3,@function
_ZNK15btCompoundShape31calculatePrincipalAxisTransformEPfR11btTransformR9btVector3: # @_ZNK15btCompoundShape31calculatePrincipalAxisTransformEPfR11btTransformR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi78:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi80:
	.cfi_def_cfa_offset 176
.Lcfi81:
	.cfi_offset %rbx, -56
.Lcfi82:
	.cfi_offset %r12, -48
.Lcfi83:
	.cfi_offset %r13, -40
.Lcfi84:
	.cfi_offset %r14, -32
.Lcfi85:
	.cfi_offset %r15, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	28(%r12), %ebp
	testl	%ebp, %ebp
	jle	.LBB12_1
# BB#2:                                 # %.lr.ph275
	movq	40(%r12), %rax
	addq	$56, %rax
	xorps	%xmm1, %xmm1
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	xorps	%xmm0, %xmm0
	xorps	%xmm6, %xmm6
	xorps	%xmm5, %xmm5
	.p2align	4, 0x90
.LBB12_3:                               # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	-8(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	-4(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm2, %xmm1
	mulss	(%rax), %xmm2
	addss	%xmm3, %xmm5
	addss	%xmm4, %xmm6
	addss	%xmm2, %xmm0
	addq	$4, %rdx
	addq	$88, %rax
	decq	%rcx
	jne	.LBB12_3
	jmp	.LBB12_4
.LBB12_1:
	xorps	%xmm5, %xmm5
	xorps	%xmm6, %xmm6
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
.LBB12_4:                               # %._crit_edge276
	movss	.LCPI12_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm2
	mulss	%xmm2, %xmm5
	movss	%xmm5, 68(%rsp)         # 4-byte Spill
	mulss	%xmm2, %xmm6
	movss	%xmm6, 64(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm2
	movss	%xmm5, 48(%r15)
	movss	%xmm6, 52(%r15)
	movss	%xmm2, 72(%rsp)         # 4-byte Spill
	movss	%xmm2, 56(%r15)
	movl	$0, 60(%r15)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	testl	%ebp, %ebp
	jle	.LBB12_7
# BB#5:                                 # %.lr.ph
	xorl	%r14d, %r14d
	leaq	104(%rsp), %r13
	.p2align	4, 0x90
.LBB12_6:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r12), %rax
	movq	64(%rax,%r14), %rdi
	movq	(%rdi), %rax
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	%r13, %rsi
	callq	*64(%rax)
	movq	40(%r12), %rax
	movss	16(%rax,%r14), %xmm11   # xmm11 = mem[0],zero,zero,zero
	movss	(%rax,%r14), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movss	4(%rax,%r14), %xmm10    # xmm10 = mem[0],zero,zero,zero
	movss	20(%rax,%r14), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	8(%rax,%r14), %xmm14    # xmm14 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	108(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm8
	mulss	%xmm5, %xmm8
	movaps	%xmm11, %xmm13
	mulss	%xmm5, %xmm13
	movaps	%xmm10, %xmm0
	mulss	%xmm7, %xmm0
	movaps	%xmm2, %xmm1
	mulss	%xmm7, %xmm1
	movss	112(%rsp), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm4
	mulss	%xmm9, %xmm4
	movaps	%xmm4, %xmm6
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movaps	%xmm8, %xmm12
	mulss	%xmm3, %xmm12
	movaps	%xmm0, %xmm4
	mulss	%xmm10, %xmm4
	addss	%xmm12, %xmm4
	mulss	%xmm14, %xmm6
	addss	%xmm4, %xmm6
	movss	%xmm6, 92(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm4
	mulss	%xmm3, %xmm4
	movaps	%xmm1, %xmm6
	mulss	%xmm10, %xmm6
	addss	%xmm4, %xmm6
	movss	24(%rax,%r14), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm15
	mulss	%xmm9, %xmm15
	movaps	%xmm15, %xmm12
	mulss	%xmm14, %xmm12
	addss	%xmm6, %xmm12
	movss	32(%rax,%r14), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 76(%rsp)         # 4-byte Spill
	mulss	%xmm6, %xmm5
	movss	36(%rax,%r14), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 84(%rsp)         # 4-byte Spill
	mulss	%xmm6, %xmm7
	mulss	%xmm5, %xmm3
	mulss	%xmm7, %xmm10
	addss	%xmm3, %xmm10
	movss	40(%rax,%r14), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 80(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm9
	mulss	%xmm9, %xmm14
	addss	%xmm10, %xmm14
	movaps	%xmm8, %xmm3
	mulss	%xmm11, %xmm3
	movaps	%xmm0, %xmm6
	mulss	%xmm2, %xmm6
	addss	%xmm3, %xmm6
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	addss	%xmm6, %xmm3
	movss	%xmm3, 88(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm3
	mulss	%xmm11, %xmm3
	movaps	%xmm1, %xmm6
	mulss	%xmm2, %xmm6
	addss	%xmm3, %xmm6
	movaps	%xmm15, %xmm10
	mulss	%xmm4, %xmm10
	addss	%xmm6, %xmm10
	mulss	%xmm5, %xmm11
	mulss	%xmm7, %xmm2
	addss	%xmm11, %xmm2
	mulss	%xmm9, %xmm4
	addss	%xmm2, %xmm4
	movss	76(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm8
	movss	84(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	addss	%xmm8, %xmm0
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	mulss	%xmm6, %xmm13
	mulss	%xmm3, %xmm1
	addss	%xmm13, %xmm1
	movss	48(%rax,%r14), %xmm8    # xmm8 = mem[0],zero,zero,zero
	subss	68(%rsp), %xmm8         # 4-byte Folded Reload
	mulss	%xmm11, %xmm15
	movaps	%xmm11, %xmm0
	addss	%xmm1, %xmm15
	movss	52(%rax,%r14), %xmm2    # xmm2 = mem[0],zero,zero,zero
	subss	64(%rsp), %xmm2         # 4-byte Folded Reload
	mulss	%xmm6, %xmm5
	movss	56(%rax,%r14), %xmm11   # xmm11 = mem[0],zero,zero,zero
	subss	72(%rsp), %xmm11        # 4-byte Folded Reload
	mulss	%xmm3, %xmm7
	addss	%xmm5, %xmm7
	mulss	%xmm0, %xmm9
	addss	%xmm7, %xmm9
	movaps	%xmm8, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm11, %xmm13
	movaps	%xmm8, %xmm6
	mulss	%xmm2, %xmm6
	mulss	%xmm11, %xmm8
	mulss	%xmm2, %xmm11
	movaps	%xmm2, %xmm7
	mulss	%xmm7, %xmm7
	movaps	%xmm0, %xmm3
	addss	%xmm7, %xmm3
	mulss	%xmm13, %xmm13
	addss	%xmm13, %xmm3
	movaps	%xmm3, %xmm2
	subss	%xmm0, %xmm2
	xorps	%xmm1, %xmm1
	subss	%xmm6, %xmm1
	movaps	%xmm3, %xmm5
	subss	%xmm7, %xmm5
	xorps	%xmm6, %xmm6
	subss	%xmm8, %xmm6
	xorps	%xmm0, %xmm0
	subss	%xmm11, %xmm0
	movss	92(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	addss	16(%rsp), %xmm7
	subss	%xmm13, %xmm3
	movss	(%rbx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	addss	%xmm7, %xmm2
	addss	20(%rsp), %xmm12
	addss	24(%rsp), %xmm14
	movss	88(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	addss	32(%rsp), %xmm7
	addss	36(%rsp), %xmm10
	addss	40(%rsp), %xmm4
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	addss	48(%rsp), %xmm11
	addss	52(%rsp), %xmm15
	addss	56(%rsp), %xmm9
	movss	%xmm2, 16(%rsp)
	mulss	%xmm8, %xmm1
	addss	%xmm1, %xmm12
	movss	%xmm12, 20(%rsp)
	mulss	%xmm8, %xmm6
	addss	%xmm6, %xmm14
	movss	%xmm14, 24(%rsp)
	addss	%xmm7, %xmm1
	movss	%xmm1, 32(%rsp)
	mulss	%xmm8, %xmm5
	addss	%xmm10, %xmm5
	movss	%xmm5, 36(%rsp)
	mulss	%xmm8, %xmm0
	addss	%xmm0, %xmm4
	movss	%xmm4, 40(%rsp)
	mulss	%xmm8, %xmm3
	addss	%xmm11, %xmm6
	movss	%xmm6, 48(%rsp)
	addss	%xmm15, %xmm0
	movss	%xmm0, 52(%rsp)
	addss	%xmm9, %xmm3
	movss	%xmm3, 56(%rsp)
	addq	$88, %r14
	addq	$4, %rbx
	decq	%rbp
	jne	.LBB12_6
.LBB12_7:                               # %._crit_edge
	leaq	16(%rsp), %rdi
	movss	.LCPI12_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movl	$20, %edx
	movq	%r15, %rsi
	callq	_ZN11btMatrix3x311diagonalizeERS_fi
	movl	16(%rsp), %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	36(%rsp), %eax
	movl	%eax, 4(%rcx)
	movl	56(%rsp), %eax
	movl	%eax, 8(%rcx)
	movl	$0, 12(%rcx)
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZNK15btCompoundShape31calculatePrincipalAxisTransformEPfR11btTransformR9btVector3, .Lfunc_end12-_ZNK15btCompoundShape31calculatePrincipalAxisTransformEPfR11btTransformR9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI13_6:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_1:
	.long	872415232               # float 1.1920929E-7
.LCPI13_2:
	.long	1285554176              # float 83886080
.LCPI13_3:
	.long	1056964608              # float 0.5
.LCPI13_4:
	.long	1073741824              # float 2
.LCPI13_5:
	.long	1065353216              # float 1
	.section	.text._ZN11btMatrix3x311diagonalizeERS_fi,"axG",@progbits,_ZN11btMatrix3x311diagonalizeERS_fi,comdat
	.weak	_ZN11btMatrix3x311diagonalizeERS_fi
	.p2align	4, 0x90
	.type	_ZN11btMatrix3x311diagonalizeERS_fi,@function
_ZN11btMatrix3x311diagonalizeERS_fi:    # @_ZN11btMatrix3x311diagonalizeERS_fi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi93:
	.cfi_def_cfa_offset 112
.Lcfi94:
	.cfi_offset %rbx, -56
.Lcfi95:
	.cfi_offset %r12, -48
.Lcfi96:
	.cfi_offset %r13, -40
.Lcfi97:
	.cfi_offset %r14, -32
.Lcfi98:
	.cfi_offset %r15, -24
.Lcfi99:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movaps	%xmm0, %xmm8
	movq	%rsi, %r12
	movl	$1065353216, (%r12)     # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 4(%r12)
	movl	$1065353216, 20(%r12)   # imm = 0x3F800000
	movups	%xmm0, 24(%r12)
	movq	$1065353216, 40(%r12)   # imm = 0x3F800000
	testl	%edx, %edx
	jle	.LBB13_15
# BB#1:                                 # %.lr.ph
	movdqa	.LCPI13_0(%rip), %xmm7  # xmm7 = [nan,nan,nan,nan]
	movss	.LCPI13_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movss	.LCPI13_5(%rip), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movss	%xmm8, 12(%rsp)         # 4-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	4(%rdi), %xmm1          # xmm1 = mem[0],zero
	pand	%xmm7, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	ucomiss	%xmm1, %xmm0
	seta	%al
	setbe	%cl
	maxss	%xmm1, %xmm0
	movd	24(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	pand	%xmm7, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB13_3
# BB#4:                                 #   in Loop: Header=BB13_2 Depth=1
	xorl	%ebp, %ebp
	movdqa	%xmm1, %xmm0
	movl	$2, %ebx
	movl	$1, %r14d
	jmp	.LBB13_5
	.p2align	4, 0x90
.LBB13_3:                               #   in Loop: Header=BB13_2 Depth=1
	movb	%cl, %bpl
	incq	%rbp
	movb	%al, %bl
	incq	%rbx
	xorl	%r14d, %r14d
.LBB13_5:                               #   in Loop: Header=BB13_2 Depth=1
	movd	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movd	20(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	pand	%xmm7, %xmm1
	pand	%xmm7, %xmm2
	addss	%xmm1, %xmm2
	movd	40(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	pand	%xmm7, %xmm1
	addss	%xmm2, %xmm1
	mulss	%xmm8, %xmm1
	ucomiss	%xmm0, %xmm1
	jb	.LBB13_7
# BB#6:                                 #   in Loop: Header=BB13_2 Depth=1
	mulss	.LCPI13_1(%rip), %xmm1
	movl	$1, %edx
	ucomiss	%xmm0, %xmm1
	jae	.LBB13_15
.LBB13_7:                               #   in Loop: Header=BB13_2 Depth=1
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rdi, %r13
	leaq	(%r13,%rbx,4), %rcx
	movss	(%r13,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movq	%rbx, %r15
	shlq	$4, %r15
	addq	%rdi, %r15
	leaq	(%r15,%rbx,4), %rax
	movss	(%r15,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	leaq	(%r13,%r14,4), %rsi
	subss	(%r13,%r14,4), %xmm4
	movaps	%xmm3, %xmm0
	addss	%xmm0, %xmm0
	divss	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm6
	jbe	.LBB13_13
# BB#8:                                 #   in Loop: Header=BB13_2 Depth=1
	addss	%xmm9, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB13_10
# BB#9:                                 # %call.sqrt
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movss	%xmm4, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI13_5(%rip), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movss	.LCPI13_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movdqa	.LCPI13_0(%rip), %xmm7  # xmm7 = [nan,nan,nan,nan]
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movss	12(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB13_10:                              # %.split
                                        #   in Loop: Header=BB13_2 Depth=1
	xorps	%xmm0, %xmm0
	cmpless	%xmm4, %xmm0
	movaps	%xmm0, %xmm2
	andps	%xmm1, %xmm2
	xorps	.LCPI13_6(%rip), %xmm1
	andnps	%xmm1, %xmm0
	orps	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	movaps	%xmm9, %xmm5
	divss	%xmm0, %xmm5
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm9, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB13_12
# BB#11:                                # %call.sqrt138
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movss	%xmm5, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI13_5(%rip), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movss	.LCPI13_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movdqa	.LCPI13_0(%rip), %xmm7  # xmm7 = [nan,nan,nan,nan]
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movss	12(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB13_12:                              # %.split.split
                                        #   in Loop: Header=BB13_2 Depth=1
	movaps	%xmm9, %xmm0
	divss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm5, %xmm1
	jmp	.LBB13_14
	.p2align	4, 0x90
.LBB13_13:                              #   in Loop: Header=BB13_2 Depth=1
	movss	.LCPI13_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	divss	%xmm0, %xmm1
	addss	.LCPI13_4(%rip), %xmm1
	mulss	%xmm1, %xmm4
	movaps	%xmm9, %xmm5
	divss	%xmm4, %xmm5
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	mulss	%xmm5, %xmm1
	movaps	%xmm9, %xmm0
	subss	%xmm1, %xmm0
	movaps	%xmm5, %xmm1
	mulss	%xmm0, %xmm1
.LBB13_14:                              #   in Loop: Header=BB13_2 Depth=1
	movl	$0, (%r15,%r14,4)
	movl	$0, (%rcx)
	mulss	%xmm5, %xmm3
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
	movss	%xmm2, (%rsi)
	addss	(%rax), %xmm3
	movss	%xmm3, (%rax)
	movq	%rbp, %rax
	shlq	$4, %rax
	addq	%rdi, %rax
	movss	(%rax,%r14,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	(%rax,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm1, %xmm5
	mulss	%xmm3, %xmm5
	subss	%xmm5, %xmm4
	movss	%xmm4, (%r13,%rbp,4)
	movss	%xmm4, (%rax,%r14,4)
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, (%r15,%rbp,4)
	movss	%xmm2, (%rax,%rbx,4)
	movss	(%r12,%r14,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	(%r12,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm1, %xmm5
	mulss	%xmm3, %xmm5
	subss	%xmm5, %xmm4
	movss	%xmm4, (%r12,%r14,4)
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, (%r12,%rbx,4)
	movss	16(%r12,%r14,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movss	16(%r12,%rbx,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm1, %xmm5
	mulss	%xmm3, %xmm5
	subss	%xmm5, %xmm4
	movss	%xmm4, 16(%r12,%r14,4)
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 16(%r12,%rbx,4)
	movss	32(%r12,%r14,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movss	32(%r12,%rbx,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm1, %xmm5
	mulss	%xmm3, %xmm5
	subss	%xmm5, %xmm4
	movss	%xmm4, 32(%r12,%r14,4)
	mulss	%xmm3, %xmm0
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, 32(%r12,%rbx,4)
	cmpl	$1, %edx
	leal	-1(%rdx), %eax
	movl	%eax, %edx
	jg	.LBB13_2
.LBB13_15:                              # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN11btMatrix3x311diagonalizeERS_fi, .Lfunc_end13-_ZN11btMatrix3x311diagonalizeERS_fi
	.cfi_endproc

	.section	.text._ZN15btCompoundShape15setLocalScalingERK9btVector3,"axG",@progbits,_ZN15btCompoundShape15setLocalScalingERK9btVector3,comdat
	.weak	_ZN15btCompoundShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN15btCompoundShape15setLocalScalingERK9btVector3,@function
_ZN15btCompoundShape15setLocalScalingERK9btVector3: # @_ZN15btCompoundShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 104(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN15btCompoundShape15setLocalScalingERK9btVector3, .Lfunc_end14-_ZN15btCompoundShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.section	.text._ZNK15btCompoundShape15getLocalScalingEv,"axG",@progbits,_ZNK15btCompoundShape15getLocalScalingEv,comdat
	.weak	_ZNK15btCompoundShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK15btCompoundShape15getLocalScalingEv,@function
_ZNK15btCompoundShape15getLocalScalingEv: # @_ZNK15btCompoundShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	104(%rdi), %rax
	retq
.Lfunc_end15:
	.size	_ZNK15btCompoundShape15getLocalScalingEv, .Lfunc_end15-_ZNK15btCompoundShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK15btCompoundShape7getNameEv,"axG",@progbits,_ZNK15btCompoundShape7getNameEv,comdat
	.weak	_ZNK15btCompoundShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK15btCompoundShape7getNameEv,@function
_ZNK15btCompoundShape7getNameEv:        # @_ZNK15btCompoundShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end16:
	.size	_ZNK15btCompoundShape7getNameEv, .Lfunc_end16-_ZNK15btCompoundShape7getNameEv
	.cfi_endproc

	.section	.text._ZN15btCompoundShape9setMarginEf,"axG",@progbits,_ZN15btCompoundShape9setMarginEf,comdat
	.weak	_ZN15btCompoundShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN15btCompoundShape9setMarginEf,@function
_ZN15btCompoundShape9setMarginEf:       # @_ZN15btCompoundShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 100(%rdi)
	retq
.Lfunc_end17:
	.size	_ZN15btCompoundShape9setMarginEf, .Lfunc_end17-_ZN15btCompoundShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK15btCompoundShape9getMarginEv,"axG",@progbits,_ZNK15btCompoundShape9getMarginEv,comdat
	.weak	_ZNK15btCompoundShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK15btCompoundShape9getMarginEv,@function
_ZNK15btCompoundShape9getMarginEv:      # @_ZNK15btCompoundShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	100(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end18:
	.size	_ZNK15btCompoundShape9getMarginEv, .Lfunc_end18-_ZNK15btCompoundShape9getMarginEv
	.cfi_endproc

	.type	_ZTV15btCompoundShape,@object # @_ZTV15btCompoundShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV15btCompoundShape
	.p2align	3
_ZTV15btCompoundShape:
	.quad	0
	.quad	_ZTI15btCompoundShape
	.quad	_ZN15btCompoundShapeD2Ev
	.quad	_ZN15btCompoundShapeD0Ev
	.quad	_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN15btCompoundShape15setLocalScalingERK9btVector3
	.quad	_ZNK15btCompoundShape15getLocalScalingEv
	.quad	_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK15btCompoundShape7getNameEv
	.quad	_ZN15btCompoundShape9setMarginEf
	.quad	_ZNK15btCompoundShape9getMarginEv
	.quad	_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape
	.quad	_ZN15btCompoundShape20recalculateLocalAabbEv
	.size	_ZTV15btCompoundShape, 128

	.type	_ZTS15btCompoundShape,@object # @_ZTS15btCompoundShape
	.globl	_ZTS15btCompoundShape
	.p2align	4
_ZTS15btCompoundShape:
	.asciz	"15btCompoundShape"
	.size	_ZTS15btCompoundShape, 18

	.type	_ZTI15btCompoundShape,@object # @_ZTI15btCompoundShape
	.globl	_ZTI15btCompoundShape
	.p2align	4
_ZTI15btCompoundShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btCompoundShape
	.quad	_ZTI16btCollisionShape
	.size	_ZTI15btCompoundShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Compound"
	.size	.L.str, 9


	.globl	_ZN15btCompoundShapeC1Eb
	.type	_ZN15btCompoundShapeC1Eb,@function
_ZN15btCompoundShapeC1Eb = _ZN15btCompoundShapeC2Eb
	.globl	_ZN15btCompoundShapeD1Ev
	.type	_ZN15btCompoundShapeD1Ev,@function
_ZN15btCompoundShapeD1Ev = _ZN15btCompoundShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
