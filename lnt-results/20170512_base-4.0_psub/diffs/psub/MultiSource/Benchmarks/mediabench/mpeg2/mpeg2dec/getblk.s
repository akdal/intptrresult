	.text
	.file	"getblk.bc"
	.globl	Decode_MPEG1_Intra_Block
	.p2align	4, 0x90
	.type	Decode_MPEG1_Intra_Block,@function
Decode_MPEG1_Intra_Block:               # @Decode_MPEG1_Intra_Block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	ld(%rip), %r14
	movslq	%ebp, %rax
	movq	%rax, %r15
	shlq	$7, %r15
	leaq	3176(%r14,%r15), %r12
	cmpl	$3, %eax
	jg	.LBB0_2
# BB#1:
	callq	Get_Luma_DC_dct_diff
	addl	(%rbx), %eax
	movl	%eax, (%rbx)
	jmp	.LBB0_5
.LBB0_2:
	callq	Get_Chroma_DC_dct_diff
	cmpl	$4, %ebp
	jne	.LBB0_4
# BB#3:
	addl	4(%rbx), %eax
	movl	%eax, 4(%rbx)
	jmp	.LBB0_5
.LBB0_4:
	addl	8(%rbx), %eax
	movl	%eax, 8(%rbx)
.LBB0_5:
	shll	$3, %eax
	movw	%ax, (%r12)
	cmpl	$0, Fault_Flag(%rip)
	jne	.LBB0_30
# BB#6:
	cmpl	$4, picture_coding_type(%rip)
	je	.LBB0_30
# BB#7:                                 # %.preheader.preheader
	movl	$1, %r13d
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_43:                               #   in Loop: Header=BB0_8 Depth=1
	leaq	(%r14,%r15), %rax
	movw	%dx, 3176(%rax,%rcx,2)
	incl	%ebx
	movl	%ebx, %r13d
.LBB0_8:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	Show_Bits
	cmpl	$16384, %eax            # imm = 0x4000
	jb	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	shrl	$12, %eax
	addl	$-4, %eax
	movl	$DCTtabnext, %r12d
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$1024, %eax             # imm = 0x400
	jb	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_8 Depth=1
	shrl	$8, %eax
	addl	$-4, %eax
	movl	$DCTtab0, %r12d
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$512, %eax              # imm = 0x200
	jb	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_8 Depth=1
	shrl	$6, %eax
	addl	$-8, %eax
	movl	$DCTtab1, %r12d
	jmp	.LBB0_24
.LBB0_14:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_8 Depth=1
	shrl	$4, %eax
	addl	$-16, %eax
	movl	$DCTtab2, %r12d
	jmp	.LBB0_24
.LBB0_16:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$128, %eax
	jb	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_8 Depth=1
	shrl	$3, %eax
	addl	$-16, %eax
	movl	$DCTtab3, %r12d
	jmp	.LBB0_24
.LBB0_18:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$64, %eax
	jb	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_8 Depth=1
	shrl	$2, %eax
	addl	$-16, %eax
	movl	$DCTtab4, %r12d
	jmp	.LBB0_24
.LBB0_20:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$32, %eax
	jb	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_8 Depth=1
	shrl	%eax
	addl	$-16, %eax
	movl	$DCTtab5, %r12d
	jmp	.LBB0_24
.LBB0_22:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$16, %eax
	jb	.LBB0_27
# BB#23:                                #   in Loop: Header=BB0_8 Depth=1
	addl	$-16, %eax
	movl	$DCTtab6, %r12d
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_8 Depth=1
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rbp
	movsbl	2(%r12,%rbp), %edi
	callq	Flush_Buffer
	movsbl	(%r12,%rbp), %ebx
	cmpl	$65, %ebx
	je	.LBB0_31
# BB#25:                                #   in Loop: Header=BB0_8 Depth=1
	cmpb	$64, %bl
	je	.LBB0_30
# BB#26:                                #   in Loop: Header=BB0_8 Depth=1
	movsbl	1(%r12,%rbp), %r12d
	movl	$1, %edi
	callq	Get_Bits
	jmp	.LBB0_37
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$6, %edi
	callq	Get_Bits
	movl	%eax, %ebx
	movl	$8, %edi
	callq	Get_Bits
	movl	%eax, %ecx
	cmpl	$128, %ecx
	je	.LBB0_34
# BB#32:                                #   in Loop: Header=BB0_8 Depth=1
	testl	%ecx, %ecx
	jne	.LBB0_35
# BB#33:                                #   in Loop: Header=BB0_8 Depth=1
	movl	$8, %edi
	callq	Get_Bits
	jmp	.LBB0_36
.LBB0_34:                               #   in Loop: Header=BB0_8 Depth=1
	movl	$8, %edi
	callq	Get_Bits
	addl	$-256, %eax
	jmp	.LBB0_36
.LBB0_35:                               #   in Loop: Header=BB0_8 Depth=1
	leal	-256(%rcx), %eax
	cmpl	$128, %ecx
	cmovlel	%ecx, %eax
.LBB0_36:                               #   in Loop: Header=BB0_8 Depth=1
	movl	%eax, %r12d
	negl	%r12d
	cmovll	%eax, %r12d
	shrl	$31, %eax
.LBB0_37:                               #   in Loop: Header=BB0_8 Depth=1
	addl	%r13d, %ebx
	cmpl	$64, %ebx
	jge	.LBB0_38
# BB#40:                                #   in Loop: Header=BB0_8 Depth=1
	movslq	%ebx, %rcx
	movq	ld(%rip), %rdx
	imull	3168(%rdx), %r12d
	movzbl	scan(%rcx), %ecx
	imull	2104(%rdx,%rcx,4), %r12d
	sarl	$3, %r12d
	leal	-1(%r12), %edx
	orl	$1, %edx
	testl	%r12d, %r12d
	cmovel	%r12d, %edx
	testl	%eax, %eax
	je	.LBB0_41
# BB#42:                                #   in Loop: Header=BB0_8 Depth=1
	movl	%edx, %eax
	negl	%eax
	cmpl	$2048, %edx             # imm = 0x800
	movl	$-2048, %edx            # imm = 0xF800
	cmovgl	%edx, %eax
	movl	%eax, %edx
	jmp	.LBB0_43
	.p2align	4, 0x90
.LBB0_41:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$2048, %edx             # imm = 0x800
	movl	$2047, %eax             # imm = 0x7FF
	cmovgel	%eax, %edx
	jmp	.LBB0_43
.LBB0_38:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB0_29
# BB#39:
	movq	stdout(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$42, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_29:
	movl	$1, Fault_Flag(%rip)
.LBB0_30:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_27:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB0_29
# BB#28:
	movl	$.Lstr, %edi
	callq	puts
	jmp	.LBB0_29
.Lfunc_end0:
	.size	Decode_MPEG1_Intra_Block, .Lfunc_end0-Decode_MPEG1_Intra_Block
	.cfi_endproc

	.globl	Decode_MPEG1_Non_Intra_Block
	.p2align	4, 0x90
	.type	Decode_MPEG1_Non_Intra_Block,@function
Decode_MPEG1_Non_Intra_Block:           # @Decode_MPEG1_Non_Intra_Block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movslq	%edi, %r14
	xorl	%r13d, %r13d
	movl	$2047, %r15d            # imm = 0x7FF
	shlq	$7, %r14
	addq	ld(%rip), %r14
	movl	$-2048, %r12d           # imm = 0xF800
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_38:                               #   in Loop: Header=BB1_1 Depth=1
	movw	%dx, 3176(%r14,%rcx,2)
	incl	%ebx
	movl	%ebx, %r13d
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	Show_Bits
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$16384, %eax            # imm = 0x4000
	jb	.LBB1_4
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	shrl	$12, %eax
	addl	$-4, %eax
	testl	%r13d, %r13d
	je	.LBB1_3
# BB#22:                                #   in Loop: Header=BB1_1 Depth=1
	leaq	DCTtabnext(%rax,%rax,2), %rbp
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_4:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$1024, %eax             # imm = 0x400
	jb	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_1 Depth=1
	shrl	$8, %eax
	addl	$-4, %eax
	leaq	DCTtab0(%rax,%rax,2), %rbp
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$512, %eax              # imm = 0x200
	jb	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_1 Depth=1
	shrl	$6, %eax
	addl	$-8, %eax
	leaq	DCTtab1(%rax,%rax,2), %rbp
	jmp	.LBB1_23
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	leaq	DCTtabfirst(%rax,%rax,2), %rbp
	jmp	.LBB1_23
.LBB1_8:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_1 Depth=1
	shrl	$4, %eax
	addl	$-16, %eax
	leaq	DCTtab2(%rax,%rax,2), %rbp
	jmp	.LBB1_23
.LBB1_10:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$128, %eax
	jb	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_1 Depth=1
	shrl	$3, %eax
	addl	$-16, %eax
	leaq	DCTtab3(%rax,%rax,2), %rbp
	jmp	.LBB1_23
.LBB1_12:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$64, %eax
	jb	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_1 Depth=1
	shrl	$2, %eax
	addl	$-16, %eax
	leaq	DCTtab4(%rax,%rax,2), %rbp
	jmp	.LBB1_23
.LBB1_14:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$32, %eax
	jb	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_1 Depth=1
	shrl	%eax
	addl	$-16, %eax
	leaq	DCTtab5(%rax,%rax,2), %rbp
	jmp	.LBB1_23
.LBB1_16:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$16, %eax
	jb	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_1 Depth=1
	addl	$-16, %eax
	leaq	DCTtab6(%rax,%rax,2), %rbp
	.p2align	4, 0x90
.LBB1_23:                               #   in Loop: Header=BB1_1 Depth=1
	movsbl	2(%rbp), %edi
	callq	Flush_Buffer
	movsbl	(%rbp), %ebx
	cmpl	$65, %ebx
	je	.LBB1_26
# BB#24:                                #   in Loop: Header=BB1_1 Depth=1
	cmpb	$64, %bl
	je	.LBB1_21
# BB#25:                                #   in Loop: Header=BB1_1 Depth=1
	movsbl	1(%rbp), %ebp
	movl	$1, %edi
	callq	Get_Bits
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_26:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$6, %edi
	callq	Get_Bits
	movl	%eax, %ebx
	movl	$8, %edi
	callq	Get_Bits
	movl	%eax, %ecx
	cmpl	$128, %ecx
	je	.LBB1_29
# BB#27:                                #   in Loop: Header=BB1_1 Depth=1
	testl	%ecx, %ecx
	jne	.LBB1_30
# BB#28:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$8, %edi
	callq	Get_Bits
	jmp	.LBB1_31
.LBB1_29:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$8, %edi
	callq	Get_Bits
	addl	$-256, %eax
	jmp	.LBB1_31
.LBB1_30:                               #   in Loop: Header=BB1_1 Depth=1
	leal	-256(%rcx), %eax
	cmpl	$128, %ecx
	cmovlel	%ecx, %eax
.LBB1_31:                               #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, %ebp
	negl	%ebp
	cmovll	%eax, %ebp
	shrl	$31, %eax
.LBB1_32:                               #   in Loop: Header=BB1_1 Depth=1
	addl	%r13d, %ebx
	cmpl	$64, %ebx
	jge	.LBB1_33
# BB#35:                                #   in Loop: Header=BB1_1 Depth=1
	movslq	%ebx, %rcx
	leal	1(%rbp,%rbp), %esi
	movq	ld(%rip), %rdx
	imull	3168(%rdx), %esi
	movzbl	scan(%rcx), %ecx
	imull	2360(%rdx,%rcx,4), %esi
	sarl	$4, %esi
	leal	-1(%rsi), %edx
	orl	$1, %edx
	testl	%esi, %esi
	cmovel	%esi, %edx
	testl	%eax, %eax
	je	.LBB1_36
# BB#37:                                #   in Loop: Header=BB1_1 Depth=1
	movl	%edx, %eax
	negl	%eax
	cmpl	$2048, %edx             # imm = 0x800
	cmovgl	%r12d, %eax
	movl	%eax, %edx
	jmp	.LBB1_38
	.p2align	4, 0x90
.LBB1_36:                               #   in Loop: Header=BB1_1 Depth=1
	cmpl	$2048, %edx             # imm = 0x800
	cmovgel	%r15d, %edx
	jmp	.LBB1_38
.LBB1_33:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB1_20
# BB#34:
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$42, %esi
	movl	$1, %edx
	callq	fwrite
.LBB1_20:                               # %.sink.split
	movl	$1, Fault_Flag(%rip)
.LBB1_21:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_18:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB1_20
# BB#19:
	movl	$.Lstr.1, %edi
	callq	puts
	jmp	.LBB1_20
.Lfunc_end1:
	.size	Decode_MPEG1_Non_Intra_Block, .Lfunc_end1-Decode_MPEG1_Non_Intra_Block
	.cfi_endproc

	.globl	Decode_MPEG2_Intra_Block
	.p2align	4, 0x90
	.type	Decode_MPEG2_Intra_Block,@function
Decode_MPEG2_Intra_Block:               # @Decode_MPEG2_Intra_Block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 80
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebx
	movq	ld(%rip), %rax
	cmpl	$1, 3148(%rax)
	movl	$base, %r15d
	cmovneq	%rax, %r15
	cmpl	$1, base+3148(%rip)
	jne	.LBB2_2
# BB#1:
	cmpl	$64, base+3164(%rip)
	movl	$base, %eax
	movl	$enhan, %ecx
	cmovgeq	%rax, %rcx
	movq	%rcx, ld(%rip)
.LBB2_2:
	leaq	2616(%r15), %rcx
	leaq	2104(%r15), %rax
	cmpl	$1, chroma_format(%rip)
	cmoveq	%rax, %rcx
	cmpl	$4, %ebx
	cmovlq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmpl	$3, %ebx
	jg	.LBB2_4
# BB#3:
	callq	Get_Luma_DC_dct_diff
	addl	(%r14), %eax
	movl	%eax, (%r14)
	cmpl	$0, Fault_Flag(%rip)
	jne	.LBB2_33
	jmp	.LBB2_8
.LBB2_4:
	callq	Get_Chroma_DC_dct_diff
	testb	$1, %bl
	jne	.LBB2_6
# BB#5:
	addl	4(%r14), %eax
	movl	%eax, 4(%r14)
	cmpl	$0, Fault_Flag(%rip)
	jne	.LBB2_33
	jmp	.LBB2_8
.LBB2_6:
	addl	8(%r14), %eax
	movl	%eax, 8(%r14)
	cmpl	$0, Fault_Flag(%rip)
	jne	.LBB2_33
.LBB2_8:
	movslq	%ebx, %rcx
	shlq	$7, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	3176(%r15,%rcx), %rdx
	movl	$3, %ecx
	subl	intra_dc_precision(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movw	%ax, (%rdx)
	movl	$1, %r13d
	movl	$1, %r12d
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_48:                               #   in Loop: Header=BB2_9 Depth=1
	incl	%ebx
	incl	%r13d
	movl	%ebx, %r12d
.LBB2_9:                                # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	Show_Bits
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$16384, %eax            # imm = 0x4000
	movl	intra_vlc_format(%rip), %ecx
	jb	.LBB2_12
# BB#10:                                #   in Loop: Header=BB2_9 Depth=1
	testl	%ecx, %ecx
	jne	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_9 Depth=1
	shrl	$12, %eax
	addl	$-4, %eax
	leaq	DCTtabnext(%rax,%rax,2), %rbp
	jmp	.LBB2_35
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_9 Depth=1
	cmpl	$1024, %eax             # imm = 0x400
	jb	.LBB2_15
# BB#13:                                #   in Loop: Header=BB2_9 Depth=1
	shrl	$8, %eax
	addl	$-4, %eax
	testl	%ecx, %ecx
	je	.LBB2_34
# BB#14:                                #   in Loop: Header=BB2_9 Depth=1
	leaq	DCTtab0a(%rax,%rax,2), %rbp
	jmp	.LBB2_35
	.p2align	4, 0x90
.LBB2_15:                               #   in Loop: Header=BB2_9 Depth=1
	cmpl	$512, %eax              # imm = 0x200
	jb	.LBB2_19
# BB#16:                                #   in Loop: Header=BB2_9 Depth=1
	shrl	$6, %eax
	addl	$-8, %eax
	testl	%ecx, %ecx
	je	.LBB2_18
# BB#17:                                #   in Loop: Header=BB2_9 Depth=1
	leaq	DCTtab1a(%rax,%rax,2), %rbp
	jmp	.LBB2_35
.LBB2_19:                               #   in Loop: Header=BB2_9 Depth=1
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_9 Depth=1
	shrl	$4, %eax
	addl	$-16, %eax
	leaq	DCTtab2(%rax,%rax,2), %rbp
	jmp	.LBB2_35
.LBB2_34:                               #   in Loop: Header=BB2_9 Depth=1
	leaq	DCTtab0(%rax,%rax,2), %rbp
	jmp	.LBB2_35
.LBB2_21:                               #   in Loop: Header=BB2_9 Depth=1
	cmpl	$128, %eax
	jb	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_9 Depth=1
	shrl	$3, %eax
	addl	$-16, %eax
	leaq	DCTtab3(%rax,%rax,2), %rbp
	jmp	.LBB2_35
.LBB2_18:                               #   in Loop: Header=BB2_9 Depth=1
	leaq	DCTtab1(%rax,%rax,2), %rbp
	jmp	.LBB2_35
.LBB2_23:                               #   in Loop: Header=BB2_9 Depth=1
	cmpl	$64, %eax
	jb	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_9 Depth=1
	shrl	$2, %eax
	addl	$-16, %eax
	leaq	DCTtab4(%rax,%rax,2), %rbp
	jmp	.LBB2_35
.LBB2_25:                               #   in Loop: Header=BB2_9 Depth=1
	cmpl	$32, %eax
	jb	.LBB2_27
# BB#26:                                #   in Loop: Header=BB2_9 Depth=1
	shrl	%eax
	addl	$-16, %eax
	leaq	DCTtab5(%rax,%rax,2), %rbp
	jmp	.LBB2_35
.LBB2_27:                               #   in Loop: Header=BB2_9 Depth=1
	cmpl	$16, %eax
	jb	.LBB2_29
# BB#28:                                #   in Loop: Header=BB2_9 Depth=1
	addl	$-16, %eax
	leaq	DCTtab6(%rax,%rax,2), %rbp
	.p2align	4, 0x90
.LBB2_35:                               #   in Loop: Header=BB2_9 Depth=1
	movsbl	2(%rbp), %edi
	callq	Flush_Buffer
	movsbl	(%rbp), %ebx
	cmpl	$65, %ebx
	je	.LBB2_38
# BB#36:                                #   in Loop: Header=BB2_9 Depth=1
	cmpb	$64, %bl
	je	.LBB2_33
# BB#37:                                #   in Loop: Header=BB2_9 Depth=1
	movsbl	1(%rbp), %r14d
	movl	$1, %edi
	callq	Get_Bits
	jmp	.LBB2_42
	.p2align	4, 0x90
.LBB2_38:                               #   in Loop: Header=BB2_9 Depth=1
	movl	$6, %edi
	callq	Get_Bits
	movl	%eax, %ebx
	movl	$12, %edi
	callq	Get_Bits
	movl	%eax, %ecx
	testw	$2047, %cx              # imm = 0x7FF
	je	.LBB2_39
# BB#41:                                #   in Loop: Header=BB2_9 Depth=1
	movl	$4096, %r14d            # imm = 0x1000
	subl	%ecx, %r14d
	xorl	%eax, %eax
	cmpl	$2047, %ecx             # imm = 0x7FF
	setg	%al
	cmovlel	%ecx, %r14d
.LBB2_42:                               #   in Loop: Header=BB2_9 Depth=1
	addl	%r12d, %ebx
	cmpl	$64, %ebx
	jge	.LBB2_43
# BB#45:                                #   in Loop: Header=BB2_9 Depth=1
	movslq	3156(%r15), %rcx
	movslq	%ebx, %rdx
	shlq	$6, %rcx
	imull	3168(%r15), %r14d
	movzbl	scan(%rcx,%rdx), %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	imull	(%rdx,%rcx,4), %r14d
	sarl	$4, %r14d
	movl	%r14d, %edx
	negl	%edx
	testl	%eax, %eax
	cmovew	%r14w, %dx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%r15,%rax), %rax
	movw	%dx, 3176(%rax,%rcx,2)
	cmpl	$1, base+3148(%rip)
	jne	.LBB2_48
# BB#46:                                #   in Loop: Header=BB2_9 Depth=1
	movl	base+3164(%rip), %eax
	addl	$-63, %eax
	cmpl	%eax, %r13d
	jne	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_9 Depth=1
	movq	$enhan, ld(%rip)
	jmp	.LBB2_48
.LBB2_43:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB2_32
# BB#44:
	movq	stdout(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB2_32
.LBB2_39:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB2_32
# BB#40:
	movl	$.Lstr.5, %edi
.LBB2_31:
	callq	puts
.LBB2_32:
	movl	$1, Fault_Flag(%rip)
.LBB2_33:                               # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_29:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB2_32
# BB#30:
	movl	$.Lstr.3, %edi
	jmp	.LBB2_31
.Lfunc_end2:
	.size	Decode_MPEG2_Intra_Block, .Lfunc_end2-Decode_MPEG2_Intra_Block
	.cfi_endproc

	.globl	Decode_MPEG2_Non_Intra_Block
	.p2align	4, 0x90
	.type	Decode_MPEG2_Non_Intra_Block,@function
Decode_MPEG2_Non_Intra_Block:           # @Decode_MPEG2_Non_Intra_Block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 80
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	ld(%rip), %rax
	cmpl	$1, 3148(%rax)
	movl	$base, %r14d
	cmovneq	%rax, %r14
	movslq	%edi, %rdx
	cmpl	$1, base+3148(%rip)
	jne	.LBB3_2
# BB#1:
	cmpl	$64, base+3164(%rip)
	movl	$base, %eax
	movl	$enhan, %ecx
	cmovgeq	%rax, %rcx
	movq	%rcx, ld(%rip)
.LBB3_2:
	leaq	2872(%r14), %rcx
	cmpl	$1, chroma_format(%rip)
	leaq	2360(%r14), %rax
	cmoveq	%rax, %rcx
	cmpl	$4, %edx
	cmovlq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	movl	$1, %r13d
	shlq	$7, %rdx
	addq	%r14, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_39:                               #   in Loop: Header=BB3_3 Depth=1
	incl	%ebx
	incl	%r13d
	movl	%ebx, %r15d
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	Show_Bits
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$16384, %eax            # imm = 0x4000
	jb	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	shrl	$12, %eax
	addl	$-4, %eax
	testl	%r15d, %r15d
	je	.LBB3_5
# BB#25:                                #   in Loop: Header=BB3_3 Depth=1
	leaq	DCTtabnext(%rax,%rax,2), %r12
	jmp	.LBB3_26
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	cmpl	$1024, %eax             # imm = 0x400
	jb	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	shrl	$8, %eax
	addl	$-4, %eax
	leaq	DCTtab0(%rax,%rax,2), %r12
	jmp	.LBB3_26
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_3 Depth=1
	cmpl	$512, %eax              # imm = 0x200
	jb	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	shrl	$6, %eax
	addl	$-8, %eax
	leaq	DCTtab1(%rax,%rax,2), %r12
	jmp	.LBB3_26
.LBB3_5:                                #   in Loop: Header=BB3_3 Depth=1
	leaq	DCTtabfirst(%rax,%rax,2), %r12
	jmp	.LBB3_26
.LBB3_10:                               #   in Loop: Header=BB3_3 Depth=1
	cmpl	$256, %eax              # imm = 0x100
	jb	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_3 Depth=1
	shrl	$4, %eax
	addl	$-16, %eax
	leaq	DCTtab2(%rax,%rax,2), %r12
	jmp	.LBB3_26
.LBB3_12:                               #   in Loop: Header=BB3_3 Depth=1
	cmpl	$128, %eax
	jb	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_3 Depth=1
	shrl	$3, %eax
	addl	$-16, %eax
	leaq	DCTtab3(%rax,%rax,2), %r12
	jmp	.LBB3_26
.LBB3_14:                               #   in Loop: Header=BB3_3 Depth=1
	cmpl	$64, %eax
	jb	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_3 Depth=1
	shrl	$2, %eax
	addl	$-16, %eax
	leaq	DCTtab4(%rax,%rax,2), %r12
	jmp	.LBB3_26
.LBB3_16:                               #   in Loop: Header=BB3_3 Depth=1
	cmpl	$32, %eax
	jb	.LBB3_18
# BB#17:                                #   in Loop: Header=BB3_3 Depth=1
	shrl	%eax
	addl	$-16, %eax
	leaq	DCTtab5(%rax,%rax,2), %r12
	jmp	.LBB3_26
.LBB3_18:                               #   in Loop: Header=BB3_3 Depth=1
	cmpl	$16, %eax
	jb	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_3 Depth=1
	addl	$-16, %eax
	leaq	DCTtab6(%rax,%rax,2), %r12
	.p2align	4, 0x90
.LBB3_26:                               #   in Loop: Header=BB3_3 Depth=1
	movsbl	2(%r12), %edi
	callq	Flush_Buffer
	movsbl	(%r12), %ebx
	cmpl	$65, %ebx
	je	.LBB3_29
# BB#27:                                #   in Loop: Header=BB3_3 Depth=1
	cmpb	$64, %bl
	je	.LBB3_24
# BB#28:                                #   in Loop: Header=BB3_3 Depth=1
	movsbl	1(%r12), %ebp
	movl	$1, %edi
	callq	Get_Bits
	jmp	.LBB3_33
	.p2align	4, 0x90
.LBB3_29:                               #   in Loop: Header=BB3_3 Depth=1
	movl	$6, %edi
	callq	Get_Bits
	movl	%eax, %ebx
	movl	$12, %edi
	callq	Get_Bits
	movl	%eax, %ecx
	testw	$2047, %cx              # imm = 0x7FF
	je	.LBB3_30
# BB#32:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$4096, %ebp             # imm = 0x1000
	subl	%ecx, %ebp
	xorl	%eax, %eax
	cmpl	$2047, %ecx             # imm = 0x7FF
	setg	%al
	cmovlel	%ecx, %ebp
.LBB3_33:                               #   in Loop: Header=BB3_3 Depth=1
	addl	%r15d, %ebx
	cmpl	$64, %ebx
	jge	.LBB3_34
# BB#36:                                #   in Loop: Header=BB3_3 Depth=1
	movslq	3156(%r14), %rcx
	movslq	%ebx, %rdx
	shlq	$6, %rcx
	leal	1(%rbp,%rbp), %esi
	imull	3168(%r14), %esi
	movzbl	scan(%rcx,%rdx), %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	imull	(%rdx,%rcx,4), %esi
	sarl	$5, %esi
	movl	%esi, %edx
	negl	%edx
	testl	%eax, %eax
	cmovew	%si, %dx
	movq	16(%rsp), %rax          # 8-byte Reload
	movw	%dx, 3176(%rax,%rcx,2)
	cmpl	$1, base+3148(%rip)
	jne	.LBB3_39
# BB#37:                                #   in Loop: Header=BB3_3 Depth=1
	movl	base+3164(%rip), %eax
	addl	$-63, %eax
	cmpl	%eax, %r13d
	jne	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_3 Depth=1
	movq	$enhan, ld(%rip)
	jmp	.LBB3_39
.LBB3_34:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB3_23
# BB#35:
	movq	stdout(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB3_23
.LBB3_30:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB3_23
# BB#31:
	movl	$.Lstr.5, %edi
.LBB3_22:                               # %.sink.split
	callq	puts
.LBB3_23:                               # %.sink.split
	movl	$1, Fault_Flag(%rip)
.LBB3_24:                               # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_20:
	cmpl	$0, Quiet_Flag(%rip)
	jne	.LBB3_23
# BB#21:
	movl	$.Lstr.4, %edi
	jmp	.LBB3_22
.Lfunc_end3:
	.size	Decode_MPEG2_Non_Intra_Block, .Lfunc_end3-Decode_MPEG2_Non_Intra_Block
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"DCT coeff index (i) out of bounds (intra)\n"
	.size	.L.str.1, 43

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"DCT coeff index (i) out of bounds (inter)\n"
	.size	.L.str.3, 43

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"DCT coeff index (i) out of bounds (intra2)\n"
	.size	.L.str.6, 44

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"DCT coeff index (i) out of bounds (inter2)\n"
	.size	.L.str.8, 44

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"invalid Huffman code in Decode_MPEG1_Intra_Block()"
	.size	.Lstr, 51

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"invalid Huffman code in Decode_MPEG1_Non_Intra_Block()"
	.size	.Lstr.1, 55

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"invalid Huffman code in Decode_MPEG2_Intra_Block()"
	.size	.Lstr.3, 51

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"invalid Huffman code in Decode_MPEG2_Non_Intra_Block()"
	.size	.Lstr.4, 55

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"invalid escape in Decode_MPEG2_Intra_Block()"
	.size	.Lstr.5, 45


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
