	.text
	.file	"shared_sha256.bc"
	.globl	sha256_init
	.p2align	4, 0x90
	.type	sha256_init,@function
sha256_init:                            # @sha256_init
	.cfi_startproc
# BB#0:
	movaps	sha256_init.H0+16(%rip), %xmm0
	movups	%xmm0, 16(%rdi)
	movaps	sha256_init.H0(%rip), %xmm0
	movups	%xmm0, (%rdi)
	movq	$0, 32(%rdi)
	movl	$0, 104(%rdi)
	retq
.Lfunc_end0:
	.size	sha256_init, .Lfunc_end0-sha256_init
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.text
	.globl	sha256_update
	.p2align	4, 0x90
	.type	sha256_update,@function
sha256_update:                          # @sha256_update
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	104(%r14), %eax
	testq	%rax, %rax
	je	.LBB1_10
# BB#1:
	movl	$64, %ebp
	subl	%eax, %ebp
	leaq	40(%r14,%rax), %rdi
	cmpl	%ebx, %ebp
	jbe	.LBB1_3
# BB#2:
	movl	%ebx, %edx
	movq	%r13, %rsi
	callq	memcpy
	addl	104(%r14), %ebx
	jmp	.LBB1_21
.LBB1_3:
	movl	%ebp, %r12d
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	memcpy
	incl	32(%r14)
	jne	.LBB1_5
# BB#4:
	incl	36(%r14)
.LBB1_5:                                # %vector.memcheck
	leaq	104(%r14), %rax
	leaq	32(%rsp), %rcx
	cmpq	%rax, %rcx
	jae	.LBB1_22
# BB#6:                                 # %vector.memcheck
	leaq	96(%rsp), %rax
	leaq	40(%r14), %rcx
	cmpq	%rax, %rcx
	jae	.LBB1_22
# BB#7:                                 # %.preheader.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_8:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	40(%r14,%rax,4), %ecx
	shll	$8, %ecx
	movzbl	41(%r14,%rax,4), %edx
	orl	%ecx, %edx
	shll	$8, %edx
	movzbl	42(%r14,%rax,4), %ecx
	orl	%edx, %ecx
	shll	$8, %ecx
	movzbl	43(%r14,%rax,4), %edx
	orl	%ecx, %edx
	movl	%edx, 32(%rsp,%rax,4)
	incq	%rax
	cmpq	$16, %rax
	jne	.LBB1_8
	jmp	.LBB1_9
.LBB1_22:                               # %vector.body
	movdqu	40(%r14), %xmm1
	movdqa	.LCPI1_0(%rip), %xmm0   # xmm0 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	movdqa	%xmm1, %xmm2
	pand	%xmm0, %xmm2
	pslld	$8, %xmm2
	movdqa	%xmm1, %xmm3
	psrlw	$8, %xmm3
	pand	%xmm0, %xmm3
	por	%xmm2, %xmm3
	pslld	$8, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$16, %xmm2
	pand	%xmm0, %xmm2
	por	%xmm3, %xmm2
	pslld	$8, %xmm2
	psrld	$24, %xmm1
	por	%xmm2, %xmm1
	movdqa	%xmm1, 32(%rsp)
	movdqu	56(%r14), %xmm1
	movdqa	%xmm1, %xmm2
	pand	%xmm0, %xmm2
	pslld	$8, %xmm2
	movdqa	%xmm1, %xmm3
	psrlw	$8, %xmm3
	pand	%xmm0, %xmm3
	por	%xmm2, %xmm3
	pslld	$8, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$16, %xmm2
	pand	%xmm0, %xmm2
	por	%xmm3, %xmm2
	pslld	$8, %xmm2
	psrld	$24, %xmm1
	por	%xmm2, %xmm1
	movdqa	%xmm1, 48(%rsp)
	movdqu	72(%r14), %xmm1
	movdqa	%xmm1, %xmm2
	pand	%xmm0, %xmm2
	pslld	$8, %xmm2
	movdqa	%xmm1, %xmm3
	psrlw	$8, %xmm3
	pand	%xmm0, %xmm3
	por	%xmm2, %xmm3
	pslld	$8, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$16, %xmm2
	pand	%xmm0, %xmm2
	por	%xmm3, %xmm2
	pslld	$8, %xmm2
	psrld	$24, %xmm1
	por	%xmm2, %xmm1
	movdqa	%xmm1, 64(%rsp)
	movdqu	88(%r14), %xmm1
	movdqa	%xmm1, %xmm2
	pand	%xmm0, %xmm2
	pslld	$8, %xmm2
	movdqa	%xmm1, %xmm3
	psrlw	$8, %xmm3
	pand	%xmm0, %xmm3
	por	%xmm2, %xmm3
	pslld	$8, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$16, %xmm2
	pand	%xmm0, %xmm2
	por	%xmm3, %xmm2
	pslld	$8, %xmm2
	psrld	$24, %xmm1
	por	%xmm2, %xmm1
	movdqa	%xmm1, 80(%rsp)
.LBB1_9:                                # %sha256_block.exit
	leaq	32(%rsp), %rsi
	movq	%r14, %rdi
	callq	sha256_transform
	addq	%r12, %r13
	subl	%ebp, %ebx
.LBB1_10:                               # %.preheader
	cmpl	$64, %ebx
	jb	.LBB1_20
# BB#11:                                # %.lr.ph
	leal	-64(%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	andl	$-64, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	64(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	leaq	32(%rsp), %r12
	movq	%r13, %r15
	.p2align	4, 0x90
.LBB1_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_17 Depth 2
	movq	%rbp, %rax
	shlq	$6, %rax
	leaq	64(%r13,%rax), %rcx
	incl	32(%r14)
	jne	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_12 Depth=1
	incl	36(%r14)
.LBB1_14:                               # %vector.memcheck59
                                        #   in Loop: Header=BB1_12 Depth=1
	cmpq	%rcx, %r12
	jae	.LBB1_23
# BB#15:                                # %vector.memcheck59
                                        #   in Loop: Header=BB1_12 Depth=1
	addq	%r13, %rax
	leaq	96(%rsp), %rcx
	cmpq	%rcx, %rax
	jae	.LBB1_23
# BB#16:                                # %.preheader.i31.preheader
                                        #   in Loop: Header=BB1_12 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_17:                               # %.preheader.i31
                                        #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r15,%rax,4), %ecx
	shll	$8, %ecx
	movzbl	1(%r15,%rax,4), %edx
	orl	%ecx, %edx
	shll	$8, %edx
	movzbl	2(%r15,%rax,4), %ecx
	orl	%edx, %ecx
	shll	$8, %ecx
	movzbl	3(%r15,%rax,4), %edx
	orl	%ecx, %edx
	movl	%edx, 32(%rsp,%rax,4)
	incq	%rax
	cmpq	$16, %rax
	jne	.LBB1_17
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_23:                               # %vector.body46
                                        #   in Loop: Header=BB1_12 Depth=1
	movdqu	(%r15), %xmm0
	movdqa	%xmm0, %xmm1
	movdqa	.LCPI1_0(%rip), %xmm3   # xmm3 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	pand	%xmm3, %xmm1
	pslld	$8, %xmm1
	movdqa	%xmm0, %xmm2
	psrlw	$8, %xmm2
	pand	%xmm3, %xmm2
	por	%xmm1, %xmm2
	pslld	$8, %xmm2
	movdqa	%xmm0, %xmm1
	psrld	$16, %xmm1
	pand	%xmm3, %xmm1
	por	%xmm2, %xmm1
	pslld	$8, %xmm1
	psrld	$24, %xmm0
	por	%xmm1, %xmm0
	movdqa	%xmm0, 32(%rsp)
	movdqu	16(%r15), %xmm0
	movdqa	%xmm0, %xmm1
	pand	%xmm3, %xmm1
	pslld	$8, %xmm1
	movdqa	%xmm0, %xmm2
	psrlw	$8, %xmm2
	pand	%xmm3, %xmm2
	por	%xmm1, %xmm2
	pslld	$8, %xmm2
	movdqa	%xmm0, %xmm1
	psrld	$16, %xmm1
	pand	%xmm3, %xmm1
	por	%xmm2, %xmm1
	pslld	$8, %xmm1
	psrld	$24, %xmm0
	por	%xmm1, %xmm0
	movdqa	%xmm0, 48(%rsp)
	movdqu	32(%r15), %xmm0
	movdqa	%xmm0, %xmm1
	pand	%xmm3, %xmm1
	pslld	$8, %xmm1
	movdqa	%xmm0, %xmm2
	psrlw	$8, %xmm2
	pand	%xmm3, %xmm2
	por	%xmm1, %xmm2
	pslld	$8, %xmm2
	movdqa	%xmm0, %xmm1
	psrld	$16, %xmm1
	pand	%xmm3, %xmm1
	por	%xmm2, %xmm1
	pslld	$8, %xmm1
	psrld	$24, %xmm0
	por	%xmm1, %xmm0
	movdqa	%xmm0, 64(%rsp)
	movdqu	48(%r15), %xmm0
	movdqa	%xmm0, %xmm1
	pand	%xmm3, %xmm1
	pslld	$8, %xmm1
	movdqa	%xmm0, %xmm2
	psrlw	$8, %xmm2
	pand	%xmm3, %xmm2
	por	%xmm1, %xmm2
	pslld	$8, %xmm2
	movdqa	%xmm0, %xmm1
	psrld	$16, %xmm1
	pand	%xmm3, %xmm1
	por	%xmm2, %xmm1
	pslld	$8, %xmm1
	psrld	$24, %xmm0
	por	%xmm1, %xmm0
	movdqa	%xmm0, 80(%rsp)
.LBB1_18:                               # %sha256_block.exit32
                                        #   in Loop: Header=BB1_12 Depth=1
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	sha256_transform
	addq	$64, %r15
	addl	$-64, %ebx
	incq	%rbp
	cmpl	$63, %ebx
	ja	.LBB1_12
# BB#19:                                # %._crit_edge.loopexit
	addq	16(%rsp), %r13          # 8-byte Folded Reload
	movl	12(%rsp), %eax          # 4-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %ebx
.LBB1_20:                               # %._crit_edge
	leaq	40(%r14), %rdi
	movl	%ebx, %edx
	movq	%r13, %rsi
	callq	memcpy
.LBB1_21:
	movl	%ebx, 104(%r14)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	sha256_update, .Lfunc_end1-sha256_update
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.text
	.globl	sha256_final
	.p2align	4, 0x90
	.type	sha256_final,@function
sha256_final:                           # @sha256_final
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 96
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	104(%r14), %ecx
	movb	$-128, %al
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	movb	%al, 40(%r14,%rdx)
	incl	%ecx
	xorl	%eax, %eax
	testb	$3, %cl
	jne	.LBB2_1
# BB#2:
	movl	%ecx, %r10d
	shrl	$2, %r10d
	je	.LBB2_12
# BB#3:                                 # %.lr.ph49.preheader
	cmpl	$16, %ecx
	jb	.LBB2_9
# BB#5:                                 # %min.iters.checked
	movl	%r10d, %r9d
	movl	%r10d, %r8d
	andl	$3, %r8d
	subq	%r8, %r9
	je	.LBB2_9
# BB#6:                                 # %vector.body.preheader
	movq	%r10, %rdi
	subq	%r8, %rdi
	leaq	40(%r14), %rsi
	movq	%rsp, %rdx
	movdqa	.LCPI2_0(%rip), %xmm0   # xmm0 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	.p2align	4, 0x90
.LBB2_7:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rsi), %xmm1
	movdqa	%xmm1, %xmm2
	pand	%xmm0, %xmm2
	pslld	$8, %xmm2
	movdqa	%xmm1, %xmm3
	psrlw	$8, %xmm3
	pand	%xmm0, %xmm3
	por	%xmm2, %xmm3
	pslld	$8, %xmm3
	movdqa	%xmm1, %xmm2
	psrld	$16, %xmm2
	pand	%xmm0, %xmm2
	por	%xmm3, %xmm2
	pslld	$8, %xmm2
	psrld	$24, %xmm1
	por	%xmm2, %xmm1
	movdqa	%xmm1, (%rdx)
	addq	$16, %rdx
	addq	$16, %rsi
	addq	$-4, %rdi
	jne	.LBB2_7
# BB#8:                                 # %middle.block
	testl	%r8d, %r8d
	jne	.LBB2_10
	jmp	.LBB2_12
.LBB2_9:
	xorl	%r9d, %r9d
.LBB2_10:                               # %.lr.ph49.preheader59
	leaq	(%rsp,%r9,4), %rdx
	leaq	43(%r14,%r9,4), %rsi
	movq	%r10, %rdi
	subq	%r9, %rdi
	.p2align	4, 0x90
.LBB2_11:                               # %.lr.ph49
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rsi), %eax
	shll	$8, %eax
	movzbl	-2(%rsi), %ebx
	orl	%eax, %ebx
	shll	$8, %ebx
	movzbl	-1(%rsi), %eax
	orl	%ebx, %eax
	shll	$8, %eax
	movzbl	(%rsi), %ebx
	orl	%eax, %ebx
	movl	%ebx, (%rdx)
	addq	$4, %rdx
	addq	$4, %rsi
	decq	%rdi
	jne	.LBB2_11
.LBB2_12:                               # %._crit_edge50
	cmpl	$59, %ecx
	jbe	.LBB2_16
# BB#13:                                # %.preheader
	cmpl	$63, %ecx
	ja	.LBB2_15
# BB#14:                                # %.lr.ph.preheader
	movl	%r10d, %eax
	leaq	(%rsp,%rax,4), %rdi
	leal	1(%r10), %eax
	cmpl	$16, %eax
	movl	$16, %ecx
	cmoval	%eax, %ecx
	decl	%ecx
	subl	%r10d, %ecx
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB2_15:                               # %._crit_edge
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	sha256_transform
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 32(%rsp)
	movdqa	%xmm0, 16(%rsp)
	movdqa	%xmm0, (%rsp)
	movq	$0, 48(%rsp)
	jmp	.LBB2_18
.LBB2_16:                               # %.preheader41
	cmpl	$55, %ecx
	ja	.LBB2_18
# BB#17:                                # %.lr.ph46.preheader
	movl	%r10d, %eax
	leaq	(%rsp,%rax,4), %rdi
	leal	1(%r10), %eax
	cmpl	$14, %eax
	movl	$14, %ecx
	cmoval	%eax, %ecx
	decl	%ecx
	subl	%r10d, %ecx
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB2_18:                               # %.loopexit
	movl	32(%r14), %eax
	movl	36(%r14), %ecx
	shldl	$9, %eax, %ecx
	movl	%ecx, 56(%rsp)
	shll	$9, %eax
	movl	104(%r14), %ecx
	shll	$3, %ecx
	orl	%eax, %ecx
	movl	%ecx, 60(%rsp)
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	sha256_transform
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	sha256_final, .Lfunc_end2-sha256_final
	.cfi_endproc

	.p2align	4, 0x90
	.type	sha256_transform,@function
sha256_transform:                       # @sha256_transform
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	movl	(%rdi), %r12d
	movl	4(%rdi), %r13d
	movl	8(%rdi), %r14d
	movl	12(%rdi), %r9d
	movl	16(%rdi), %ecx
	movl	20(%rdi), %r11d
	movl	24(%rdi), %r10d
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movl	28(%rdi), %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rsi, -48(%rsp)         # 8-byte Spill
	movl	%ecx, %eax
	roll	$26, %eax
	movl	%ecx, %edi
	roll	$21, %edi
	xorl	%eax, %edi
	movl	%ecx, %ebp
	roll	$7, %ebp
	movl	%r11d, %eax
	xorl	%edi, %ebp
	xorl	%r10d, %eax
	andl	%ecx, %eax
	xorl	%r10d, %eax
	addl	%edx, %eax
	addl	%ebp, %eax
	movl	%r12d, %edi
	roll	$30, %edi
	movl	%r12d, %ebp
	roll	$19, %ebp
	addl	K(,%rsi,4), %eax
	xorl	%edi, %ebp
	movl	%r12d, %edi
	roll	$10, %edi
	xorl	%ebp, %edi
	movl	%r12d, %ebp
	movq	-16(%rsp), %rdx         # 8-byte Reload
	addl	(%rdx,%rsi,4), %eax
	andl	%r13d, %ebp
	movl	%r12d, %r8d
	xorl	%r13d, %r8d
	andl	%r14d, %r8d
	xorl	%ebp, %r8d
	addl	%edi, %r8d
	addl	%eax, %r9d
	addl	%eax, %r8d
	movl	%r9d, %edi
	roll	$26, %edi
	movl	%r9d, %ebp
	roll	$21, %ebp
	movl	%r9d, %r15d
	roll	$7, %r15d
	movl	%ecx, %esi
	xorl	%r11d, %esi
	xorl	%edi, %ebp
	andl	%r9d, %esi
	xorl	%r11d, %esi
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+4(,%rax,4), %r10d
	xorl	%ebp, %r15d
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	4(%rdx,%rax,4), %r10d
	movl	%r8d, %edi
	roll	$30, %edi
	movl	%r8d, %ebp
	roll	$19, %ebp
	addl	%esi, %r10d
	xorl	%edi, %ebp
	movl	%r8d, %esi
	roll	$10, %esi
	movl	%r8d, %edi
	andl	%r12d, %edi
	movl	%r8d, %eax
	xorl	%ebp, %esi
	xorl	%r12d, %eax
	andl	%r13d, %eax
	xorl	%edi, %eax
	addl	%r15d, %r10d
	addl	%esi, %eax
	addl	%r10d, %r14d
	addl	%eax, %r10d
	movl	%r14d, %esi
	roll	$26, %esi
	movl	%r14d, %edi
	roll	$21, %edi
	movl	%r14d, %r15d
	roll	$7, %r15d
	movl	%r9d, %ebp
	xorl	%ecx, %ebp
	xorl	%esi, %edi
	andl	%r14d, %ebp
	xorl	%ecx, %ebp
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+8(,%rax,4), %r11d
	xorl	%edi, %r15d
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	8(%rdx,%rax,4), %r11d
	movl	%r10d, %esi
	roll	$30, %esi
	movl	%r10d, %edi
	roll	$19, %edi
	addl	%ebp, %r11d
	xorl	%esi, %edi
	movl	%r10d, %esi
	roll	$10, %esi
	movl	%r10d, %ebp
	andl	%r8d, %ebp
	movl	%r10d, %eax
	xorl	%edi, %esi
	xorl	%r8d, %eax
	andl	%r12d, %eax
	xorl	%ebp, %eax
	addl	%r15d, %r11d
	addl	%esi, %eax
	addl	%r11d, %r13d
	addl	%eax, %r11d
	movl	%r13d, %esi
	roll	$26, %esi
	movl	%r13d, %edi
	roll	$21, %edi
	movl	%r13d, %r15d
	roll	$7, %r15d
	movl	%r14d, %ebp
	xorl	%r9d, %ebp
	xorl	%esi, %edi
	andl	%r13d, %ebp
	xorl	%r9d, %ebp
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+12(,%rax,4), %ecx
	xorl	%edi, %r15d
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	12(%rdx,%rax,4), %ecx
	movl	%r11d, %esi
	roll	$30, %esi
	movl	%r11d, %edi
	roll	$19, %edi
	addl	%ebp, %ecx
	xorl	%esi, %edi
	movl	%r11d, %esi
	roll	$10, %esi
	movl	%r11d, %ebp
	andl	%r10d, %ebp
	movl	%r11d, %eax
	xorl	%edi, %esi
	xorl	%r10d, %eax
	andl	%r8d, %eax
	xorl	%ebp, %eax
	addl	%r15d, %ecx
	addl	%esi, %eax
	addl	%ecx, %r12d
	addl	%eax, %ecx
	movl	%r12d, %eax
	roll	$26, %eax
	movl	%r12d, %esi
	roll	$21, %esi
	movl	%r12d, %edi
	roll	$7, %edi
	movl	%r13d, %ebp
	xorl	%r14d, %ebp
	xorl	%eax, %esi
	andl	%r12d, %ebp
	xorl	%r14d, %ebp
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+16(,%rax,4), %r9d
	xorl	%esi, %edi
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	16(%rdx,%rax,4), %r9d
	movl	%ecx, %eax
	roll	$30, %eax
	movl	%ecx, %esi
	roll	$19, %esi
	addl	%ebp, %r9d
	movl	%r9d, %r15d
	xorl	%eax, %esi
	movl	%ecx, %eax
	roll	$10, %eax
	xorl	%esi, %eax
	movl	%ecx, %esi
	addl	%edi, %r15d
	andl	%r11d, %esi
	movl	%ecx, %r9d
	xorl	%r11d, %r9d
	andl	%r10d, %r9d
	xorl	%esi, %r9d
	addl	%r15d, %r8d
	movl	%r8d, %esi
	roll	$26, %esi
	movl	%r8d, %ebp
	roll	$21, %ebp
	addl	%eax, %r9d
	xorl	%esi, %ebp
	movl	%r8d, %eax
	roll	$7, %eax
	movl	%r12d, %esi
	xorl	%r13d, %esi
	addl	%r15d, %r9d
	andl	%r8d, %esi
	xorl	%r13d, %esi
	movq	%rdx, %rbx
	movq	-48(%rsp), %rdx         # 8-byte Reload
	movl	20(%rbx,%rdx,4), %edi
	xorl	%ebp, %eax
	movq	-48(%rsp), %rdx         # 8-byte Reload
	addl	K+20(,%rdx,4), %edi
	addl	%r14d, %edi
	addl	%esi, %edi
	addl	%eax, %edi
	movl	%r9d, %edx
	roll	$30, %edx
	movl	%r9d, %esi
	roll	$19, %esi
	movl	%r9d, %ebp
	roll	$10, %ebp
	movl	%r9d, %eax
	xorl	%edx, %esi
	movl	%r8d, %edx
	andl	%ecx, %eax
	movl	%r9d, %r14d
	xorl	%ecx, %r14d
	andl	%r11d, %r14d
	xorl	%esi, %ebp
	xorl	%eax, %r14d
	addl	%edi, %r10d
	movl	%r10d, %eax
	roll	$26, %eax
	movl	%r10d, %esi
	addl	%ebp, %r14d
	roll	$21, %esi
	xorl	%eax, %esi
	movl	%r10d, %eax
	roll	$7, %eax
	movl	%edx, %ebp
	addl	%edi, %r14d
	xorl	%r12d, %ebp
	andl	%r10d, %ebp
	xorl	%r12d, %ebp
	xorl	%esi, %eax
	movq	-48(%rsp), %rsi         # 8-byte Reload
	movl	24(%rbx,%rsi,4), %edi
	movq	-48(%rsp), %rsi         # 8-byte Reload
	addl	K+24(,%rsi,4), %edi
	addl	%r13d, %edi
	addl	%ebp, %edi
	addl	%eax, %edi
	movl	%r14d, %eax
	roll	$30, %eax
	movl	%r14d, %esi
	roll	$19, %esi
	movl	%r14d, %ebp
	xorl	%eax, %esi
	roll	$10, %ebp
	movl	%r14d, %eax
	andl	%r9d, %eax
	movl	%r14d, %r13d
	xorl	%r9d, %r13d
	xorl	%esi, %ebp
	andl	%ecx, %r13d
	xorl	%eax, %r13d
	addl	%edi, %r11d
	movl	%r11d, %r15d
	addl	%ebp, %r13d
	roll	$26, %r15d
	movl	%r11d, %esi
	roll	$21, %esi
	movl	%r11d, %ebp
	roll	$7, %ebp
	movl	%r10d, %eax
	xorl	%r15d, %esi
	xorl	%edx, %eax
	andl	%r11d, %eax
	xorl	%edx, %eax
	addl	%edi, %r13d
	movq	-48(%rsp), %rdi         # 8-byte Reload
	movl	28(%rbx,%rdi,4), %edi
	movq	-48(%rsp), %rbx         # 8-byte Reload
	addl	K+28(,%rbx,4), %edi
	addl	%r12d, %edi
	xorl	%esi, %ebp
	addl	%eax, %edi
	addl	%ebp, %edi
	movl	%r13d, %eax
	roll	$30, %eax
	movl	%r13d, %esi
	roll	$19, %esi
	xorl	%eax, %esi
	movl	%r13d, %eax
	roll	$10, %eax
	movl	%r13d, %ebp
	andl	%r14d, %ebp
	movl	%r13d, %r12d
	xorl	%esi, %eax
	movq	-48(%rsp), %rsi         # 8-byte Reload
	xorl	%r14d, %r12d
	andl	%r9d, %r12d
	xorl	%ebp, %r12d
	addl	%eax, %r12d
	addl	%edi, %ecx
	addl	%edi, %r12d
	addq	$8, %rsi
	cmpb	$16, %sil
	jb	.LBB3_1
# BB#2:                                 # %.lr.ph
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r8d
	movl	56(%rax), %r15d
	movl	60(%rax), %esi
	movl	$16, %ebp
	movl	%edx, -40(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	%rbp, -48(%rsp)         # 8-byte Spill
	movl	%ecx, %eax
	roll	$26, %eax
	movl	%ecx, %edx
	roll	$21, %edx
	xorl	%eax, %edx
	movl	%ecx, %ebx
	roll	$7, %ebx
	movl	%r15d, %edi
	roll	$15, %edi
	movl	%r15d, %eax
	xorl	%edx, %ebx
	roll	$13, %eax
	shrl	$10, %r15d
	xorl	%eax, %r15d
	xorl	%edi, %r15d
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	4(%rax), %edi
	movl	%edi, -32(%rsp)         # 4-byte Spill
	movl	%edi, %eax
	roll	$14, %eax
	movl	%edi, %edx
	shrl	$3, %edx
	xorl	%eax, %edx
	movl	%edi, %eax
	roll	$25, %eax
	xorl	%eax, %edx
	movl	%r11d, %eax
	xorl	%r10d, %eax
	andl	%ecx, %eax
	xorl	%r10d, %eax
	movq	-16(%rsp), %rdi         # 8-byte Reload
	addl	36(%rdi), %r8d
	addl	%r15d, %r8d
	addl	%edx, %r8d
	addl	-40(%rsp), %eax         # 4-byte Folded Reload
	addl	%ebx, %eax
	addl	K(,%rbp,4), %eax
	movl	%r12d, %edx
	movl	%r8d, (%rdi)
	movq	%rdi, %r15
	roll	$30, %edx
	movl	%r12d, %edi
	roll	$19, %edi
	movl	%r12d, %ebx
	roll	$10, %ebx
	xorl	%edx, %edi
	xorl	%edi, %ebx
	movl	%r12d, %edx
	andl	%r13d, %edx
	movl	%r12d, %edi
	xorl	%r13d, %edi
	addl	%r8d, %eax
	andl	%r14d, %edi
	xorl	%edx, %edi
	addl	%ebx, %edi
	addl	%eax, %r9d
	addl	%eax, %edi
	movl	%edi, -36(%rsp)         # 4-byte Spill
	movl	%r9d, %eax
	roll	$26, %eax
	movl	%r9d, %edx
	roll	$21, %edx
	movl	%r9d, %r8d
	xorl	%eax, %edx
	roll	$7, %r8d
	movl	%ecx, %ebp
	xorl	%r11d, %ebp
	andl	%r9d, %ebp
	movl	%esi, %eax
	xorl	%r11d, %ebp
	roll	$15, %eax
	movl	%esi, %edi
	roll	$13, %edi
	shrl	$10, %esi
	xorl	%edx, %r8d
	xorl	%edi, %esi
	xorl	%eax, %esi
	movl	8(%r15), %edi
	movl	%edi, %eax
	roll	$14, %eax
	movl	%edi, %edx
	shrl	$3, %edx
	xorl	%eax, %edx
	movl	%edi, %eax
	roll	$25, %eax
	xorl	%eax, %edx
	movq	%r15, %rax
	movl	-32(%rsp), %r15d        # 4-byte Reload
	addl	40(%rax), %r15d
	addl	%esi, %r15d
	addl	%edx, %r15d
	movl	%r15d, 4(%rax)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+4(,%rax,4), %r10d
	movl	-36(%rsp), %ebx         # 4-byte Reload
	movl	%ebx, %eax
	roll	$30, %eax
	movl	%ebx, %edx
	roll	$19, %edx
	addl	%ebp, %r10d
	xorl	%eax, %edx
	movl	%ebx, %eax
	roll	$10, %eax
	xorl	%edx, %eax
	movl	%ebx, %edx
	addl	%r8d, %r10d
	andl	%r12d, %edx
	movl	%ebx, %esi
	xorl	%r12d, %esi
	andl	%r13d, %esi
	addl	%r15d, %r10d
	xorl	%edx, %esi
	addl	%eax, %esi
	addl	%r10d, %r14d
	addl	%r10d, %esi
	movl	%esi, -32(%rsp)         # 4-byte Spill
	movl	%r14d, %eax
	roll	$26, %eax
	movl	%r14d, %edx
	roll	$21, %edx
	movl	%r14d, %r8d
	roll	$7, %r8d
	movl	%r9d, %ebp
	xorl	%eax, %edx
	xorl	%ecx, %ebp
	andl	%r14d, %ebp
	xorl	%ecx, %ebp
	xorl	%edx, %r8d
	movq	-16(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %eax
	movl	12(%rdx), %r10d
	movq	%rdx, %r15
	movl	%eax, %edx
	roll	$15, %edx
	movl	%eax, %ebx
	roll	$13, %ebx
	shrl	$10, %eax
	xorl	%ebx, %eax
	movl	%r10d, %esi
	roll	$14, %esi
	xorl	%edx, %eax
	movl	%r10d, %edx
	shrl	$3, %edx
	xorl	%esi, %edx
	movl	%r10d, %esi
	roll	$25, %esi
	xorl	%esi, %edx
	movq	%r15, %rbx
	addl	44(%rbx), %edi
	addl	%eax, %edi
	addl	%edx, %edi
	movl	%edi, 8(%rbx)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+8(,%rax,4), %r11d
	movl	-32(%rsp), %esi         # 4-byte Reload
	movl	%esi, %eax
	roll	$30, %eax
	movl	%esi, %edx
	roll	$19, %edx
	addl	%edi, %r11d
	xorl	%eax, %edx
	movl	%esi, %eax
	roll	$10, %eax
	xorl	%edx, %eax
	movl	%esi, %edx
	addl	%ebp, %r11d
	movl	-36(%rsp), %edi         # 4-byte Reload
	andl	%edi, %edx
	movl	%esi, %r15d
	xorl	%edi, %r15d
	andl	%r12d, %r15d
	addl	%r8d, %r11d
	xorl	%edx, %r15d
	addl	%eax, %r15d
	addl	%r11d, %r13d
	addl	%r11d, %r15d
	movl	%r13d, %eax
	roll	$26, %eax
	movl	%r13d, %edx
	roll	$21, %edx
	movl	%r13d, %r8d
	roll	$7, %r8d
	movl	%r14d, %ebp
	xorl	%eax, %edx
	xorl	%r9d, %ebp
	andl	%r13d, %ebp
	xorl	%r9d, %ebp
	xorl	%edx, %r8d
	movl	4(%rbx), %eax
	movl	16(%rbx), %edi
	movq	%rbx, %r11
	movl	%eax, %edx
	roll	$15, %edx
	movl	%eax, %esi
	roll	$13, %esi
	shrl	$10, %eax
	xorl	%esi, %eax
	movl	%edi, %esi
	roll	$14, %esi
	xorl	%edx, %eax
	movl	%edi, %edx
	shrl	$3, %edx
	xorl	%esi, %edx
	movl	%edi, %esi
	roll	$25, %esi
	xorl	%esi, %edx
	addl	48(%r11), %r10d
	addl	%eax, %r10d
	addl	%edx, %r10d
	movl	%r10d, 12(%r11)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+12(,%rax,4), %ecx
	movl	%r15d, %eax
	roll	$30, %eax
	movl	%r15d, %edx
	roll	$19, %edx
	addl	%r10d, %ecx
	xorl	%eax, %edx
	movl	%r15d, %eax
	roll	$10, %eax
	xorl	%edx, %eax
	movl	%r15d, %edx
	addl	%ebp, %ecx
	movl	-32(%rsp), %ebx         # 4-byte Reload
	andl	%ebx, %edx
	movl	%r15d, %esi
	xorl	%ebx, %esi
	andl	-36(%rsp), %esi         # 4-byte Folded Reload
	addl	%r8d, %ecx
	xorl	%edx, %esi
	addl	%eax, %esi
	addl	%ecx, %r12d
	addl	%ecx, %esi
	movl	%esi, %r10d
	movl	%r12d, %eax
	roll	$26, %eax
	movl	%r12d, %edx
	roll	$21, %edx
	movl	%r12d, %ecx
	roll	$7, %ecx
	movl	%r13d, %esi
	xorl	%eax, %edx
	xorl	%r14d, %esi
	andl	%r12d, %esi
	xorl	%r14d, %esi
	xorl	%edx, %ecx
	movq	%r11, %rdx
	movl	8(%rdx), %eax
	movl	20(%rdx), %r8d
	movl	%eax, %edx
	roll	$15, %edx
	movl	%eax, %ebx
	roll	$13, %ebx
	shrl	$10, %eax
	xorl	%ebx, %eax
	movl	%r8d, %ebp
	roll	$14, %ebp
	xorl	%edx, %eax
	movl	%r8d, %edx
	shrl	$3, %edx
	xorl	%ebp, %edx
	movl	%r8d, %ebp
	roll	$25, %ebp
	xorl	%ebp, %edx
	addl	52(%r11), %edi
	addl	%eax, %edi
	addl	%edx, %edi
	movl	%edi, 16(%r11)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+16(,%rax,4), %r9d
	movl	%r10d, -40(%rsp)        # 4-byte Spill
	movl	%r10d, %eax
	roll	$30, %eax
	movl	%r10d, %edx
	roll	$19, %edx
	addl	%edi, %r9d
	xorl	%eax, %edx
	movl	%r10d, %eax
	roll	$10, %eax
	xorl	%edx, %eax
	movl	%r10d, %edx
	addl	%esi, %r9d
	andl	%r15d, %edx
	movl	%r10d, %esi
	xorl	%r15d, %esi
	andl	-32(%rsp), %esi         # 4-byte Folded Reload
	addl	%ecx, %r9d
	xorl	%edx, %esi
	addl	%eax, %esi
	movl	-36(%rsp), %edi         # 4-byte Reload
	addl	%r9d, %edi
	addl	%r9d, %esi
	movl	%esi, %r10d
	movl	%edi, %eax
	roll	$26, %eax
	movl	%edi, %ecx
	roll	$21, %ecx
	movl	%edi, %r9d
	roll	$7, %r9d
	movl	%r12d, %esi
	xorl	%eax, %ecx
	xorl	%r13d, %esi
	andl	%edi, %esi
	xorl	%r13d, %esi
	xorl	%ecx, %r9d
	movl	12(%r11), %eax
	movl	24(%r11), %ebx
	movl	%eax, %edx
	roll	$15, %edx
	movl	%eax, %ebp
	roll	$13, %ebp
	shrl	$10, %eax
	xorl	%ebp, %eax
	movl	%ebx, %ecx
	roll	$14, %ecx
	xorl	%edx, %eax
	movl	%ebx, %edx
	shrl	$3, %edx
	xorl	%ecx, %edx
	movl	%ebx, %ecx
	roll	$25, %ecx
	xorl	%ecx, %edx
	addl	56(%r11), %r8d
	addl	%eax, %r8d
	addl	%edx, %r8d
	movl	%r8d, 20(%r11)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+20(,%rax,4), %r14d
	movl	%r10d, %eax
	roll	$30, %eax
	movl	%r10d, %ecx
	roll	$19, %ecx
	addl	%r8d, %r14d
	xorl	%eax, %ecx
	movl	%r10d, %eax
	roll	$10, %eax
	xorl	%ecx, %eax
	movl	%r10d, %ecx
	addl	%esi, %r14d
	movl	-40(%rsp), %esi         # 4-byte Reload
	andl	%esi, %ecx
	movl	%r10d, %edx
	xorl	%esi, %edx
	andl	%r15d, %edx
	addl	%r9d, %r14d
	xorl	%ecx, %edx
	addl	%eax, %edx
	movl	-32(%rsp), %esi         # 4-byte Reload
	addl	%r14d, %esi
	addl	%r14d, %edx
	movl	%edx, %r9d
	movl	%esi, %eax
	roll	$26, %eax
	movl	%esi, %ecx
	roll	$21, %ecx
	movl	%esi, %r8d
	movl	%esi, -32(%rsp)         # 4-byte Spill
	roll	$7, %r8d
	movl	%edi, %ebp
	xorl	%eax, %ecx
	xorl	%r12d, %ebp
	andl	%esi, %ebp
	xorl	%r12d, %ebp
	xorl	%ecx, %r8d
	movl	16(%r11), %eax
	movl	28(%r11), %edx
	movl	%eax, %ecx
	roll	$15, %ecx
	movl	%eax, %esi
	roll	$13, %esi
	shrl	$10, %eax
	xorl	%esi, %eax
	movl	%edx, %esi
	roll	$14, %esi
	xorl	%ecx, %eax
	movl	%edx, %ecx
	shrl	$3, %ecx
	xorl	%esi, %ecx
	movl	%edx, %esi
	roll	$25, %esi
	xorl	%esi, %ecx
	addl	60(%r11), %ebx
	addl	%eax, %ebx
	addl	%ecx, %ebx
	movl	%ebx, 24(%r11)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+24(,%rax,4), %r13d
	movl	%r9d, %eax
	roll	$30, %eax
	movl	%r9d, %ecx
	roll	$19, %ecx
	addl	%ebx, %r13d
	xorl	%eax, %ecx
	movl	%r9d, %eax
	roll	$10, %eax
	xorl	%ecx, %eax
	movl	%r9d, %ecx
	movl	%r9d, %r14d
	addl	%ebp, %r13d
	movl	%r10d, -20(%rsp)        # 4-byte Spill
	andl	%r10d, %ecx
	movl	%r14d, %esi
	xorl	%r10d, %esi
	andl	-40(%rsp), %esi         # 4-byte Folded Reload
	addl	%r8d, %r13d
	xorl	%ecx, %esi
	addl	%eax, %esi
	addl	%r13d, %r15d
	addl	%r13d, %esi
	movl	%esi, %r10d
	movl	%r15d, %eax
	roll	$26, %eax
	movl	%r15d, %ecx
	roll	$21, %ecx
	movl	%r15d, %r8d
	roll	$7, %r8d
	movl	-32(%rsp), %r13d        # 4-byte Reload
	movl	%r13d, %ebp
	xorl	%eax, %ecx
	xorl	%edi, %ebp
	andl	%r15d, %ebp
	xorl	%edi, %ebp
	xorl	%ecx, %r8d
	movl	20(%r11), %eax
	movl	32(%r11), %r9d
	movl	%eax, %ebx
	roll	$15, %ebx
	movl	%eax, %esi
	roll	$13, %esi
	shrl	$10, %eax
	xorl	%esi, %eax
	movl	%r9d, %esi
	roll	$14, %esi
	xorl	%ebx, %eax
	movl	%r9d, %ecx
	shrl	$3, %ecx
	xorl	%esi, %ecx
	movl	%r9d, %esi
	roll	$25, %esi
	xorl	%esi, %ecx
	addl	(%r11), %edx
	addl	%eax, %edx
	addl	%ecx, %edx
	movl	%edx, 28(%r11)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+28(,%rax,4), %r12d
	movl	%r10d, -28(%rsp)        # 4-byte Spill
	movl	%r10d, %eax
	roll	$30, %eax
	movl	%r10d, %ecx
	roll	$19, %ecx
	addl	%edx, %r12d
	xorl	%eax, %ecx
	movl	%r10d, %eax
	roll	$10, %eax
	xorl	%ecx, %eax
	movl	%r10d, %ecx
	addl	%ebp, %r12d
	movl	%r14d, -36(%rsp)        # 4-byte Spill
	andl	%r14d, %ecx
	movl	%r10d, %edx
	xorl	%r14d, %edx
	andl	-20(%rsp), %edx         # 4-byte Folded Reload
	addl	%r8d, %r12d
	xorl	%ecx, %edx
	addl	%eax, %edx
	movl	-40(%rsp), %r8d         # 4-byte Reload
	addl	%r12d, %r8d
	addl	%r12d, %edx
	movl	%edx, %r14d
	movl	%r8d, %eax
	roll	$26, %eax
	movl	%r8d, %ecx
	roll	$21, %ecx
	movl	%r8d, %r10d
	roll	$7, %r10d
	movl	%r15d, %esi
	xorl	%eax, %ecx
	movl	%r13d, %eax
	xorl	%eax, %esi
	andl	%r8d, %esi
	xorl	%eax, %esi
	movl	%eax, %r12d
	xorl	%ecx, %r10d
	movq	%r11, %r13
	movl	24(%r13), %ecx
	movl	36(%r13), %r11d
	movl	%ecx, %ebp
	roll	$15, %ebp
	movl	%ecx, %ebx
	roll	$13, %ebx
	shrl	$10, %ecx
	xorl	%ebx, %ecx
	movl	%r11d, %eax
	roll	$14, %eax
	xorl	%ebp, %ecx
	movl	%r11d, %edx
	shrl	$3, %edx
	xorl	%eax, %edx
	movl	%r11d, %eax
	roll	$25, %eax
	xorl	%eax, %edx
	addl	4(%r13), %r9d
	addl	%ecx, %r9d
	addl	%edx, %r9d
	movl	%r9d, 32(%r13)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+32(,%rax,4), %edi
	movl	%r14d, -24(%rsp)        # 4-byte Spill
	movl	%r14d, %eax
	roll	$30, %eax
	movl	%r14d, %ecx
	roll	$19, %ecx
	addl	%r9d, %edi
	xorl	%eax, %ecx
	movl	%r14d, %eax
	roll	$10, %eax
	xorl	%ecx, %eax
	movl	%r14d, %ecx
	addl	%esi, %edi
	movl	-28(%rsp), %esi         # 4-byte Reload
	andl	%esi, %ecx
	movl	%r14d, %edx
	xorl	%esi, %edx
	andl	-36(%rsp), %edx         # 4-byte Folded Reload
	addl	%r10d, %edi
	xorl	%ecx, %edx
	addl	%eax, %edx
	movl	-20(%rsp), %eax         # 4-byte Reload
	addl	%edi, %eax
	addl	%edi, %edx
	movl	%edx, %r10d
	movl	%eax, %ecx
	roll	$26, %ecx
	movl	%eax, %edx
	roll	$21, %edx
	movl	%eax, %r9d
	roll	$7, %r9d
	movl	%r8d, %esi
	xorl	%ecx, %edx
	xorl	%r15d, %esi
	andl	%eax, %esi
	movl	%eax, %edi
	movl	%edi, -20(%rsp)         # 4-byte Spill
	xorl	%r15d, %esi
	xorl	%edx, %r9d
	movl	28(%r13), %edx
	movl	40(%r13), %r14d
	movl	%edx, %ebp
	roll	$15, %ebp
	movl	%edx, %ebx
	roll	$13, %ebx
	shrl	$10, %edx
	xorl	%ebx, %edx
	movl	%r14d, %ecx
	roll	$14, %ecx
	xorl	%ebp, %edx
	movl	%r14d, %eax
	shrl	$3, %eax
	xorl	%ecx, %eax
	movl	%r14d, %ecx
	roll	$25, %ecx
	xorl	%ecx, %eax
	addl	8(%r13), %r11d
	addl	%edx, %r11d
	addl	%eax, %r11d
	movl	%r11d, 36(%r13)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+36(,%rax,4), %r12d
	movl	%r10d, %edx
	movl	%edx, %eax
	roll	$30, %eax
	movl	%edx, %ecx
	roll	$19, %ecx
	addl	%r11d, %r12d
	xorl	%eax, %ecx
	movl	%edx, %eax
	roll	$10, %eax
	xorl	%ecx, %eax
	movl	%edx, %ecx
	addl	%esi, %r12d
	movl	-24(%rsp), %esi         # 4-byte Reload
	andl	%esi, %ecx
	movl	%edx, %r11d
	xorl	%esi, %r10d
	andl	-28(%rsp), %r10d        # 4-byte Folded Reload
	addl	%r9d, %r12d
	xorl	%ecx, %r10d
	addl	%eax, %r10d
	movl	-36(%rsp), %ecx         # 4-byte Reload
	addl	%r12d, %ecx
	addl	%r12d, %r10d
	movl	%ecx, %eax
	roll	$26, %eax
	movl	%ecx, %edx
	roll	$21, %edx
	movl	%ecx, %r9d
	movl	%ecx, -36(%rsp)         # 4-byte Spill
	roll	$7, %r9d
	movl	%edi, %esi
	xorl	%eax, %edx
	xorl	%r8d, %esi
	andl	%ecx, %esi
	xorl	%r8d, %esi
	movl	%r8d, %edi
	xorl	%edx, %r9d
	movl	32(%r13), %eax
	movl	44(%r13), %r12d
	movl	%eax, %ebp
	roll	$15, %ebp
	movl	%eax, %ebx
	roll	$13, %ebx
	shrl	$10, %eax
	xorl	%ebx, %eax
	movl	%r12d, %edx
	roll	$14, %edx
	xorl	%ebp, %eax
	movl	%r12d, %ecx
	shrl	$3, %ecx
	xorl	%edx, %ecx
	movl	%r12d, %edx
	roll	$25, %edx
	xorl	%edx, %ecx
	addl	12(%r13), %r14d
	addl	%eax, %r14d
	addl	%ecx, %r14d
	movl	%r14d, 40(%r13)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+40(,%rax,4), %r15d
	movl	%r10d, %eax
	roll	$30, %eax
	movl	%r10d, %ecx
	roll	$19, %ecx
	addl	%r14d, %r15d
	xorl	%eax, %ecx
	movl	%r10d, %eax
	roll	$10, %eax
	xorl	%ecx, %eax
	movl	%r10d, %ecx
	addl	%esi, %r15d
	movl	%r11d, %edx
	movl	%edx, -40(%rsp)         # 4-byte Spill
	andl	%edx, %ecx
	movl	%r10d, %r11d
	xorl	%edx, %r11d
	andl	-24(%rsp), %r11d        # 4-byte Folded Reload
	addl	%r9d, %r15d
	xorl	%ecx, %r11d
	addl	%eax, %r11d
	movl	-28(%rsp), %edx         # 4-byte Reload
	addl	%r15d, %edx
	addl	%r15d, %r11d
	movl	%edx, %eax
	roll	$26, %eax
	movl	%edx, %ecx
	roll	$21, %ecx
	movl	%edx, %r14d
	movl	%edx, %r15d
	roll	$7, %r14d
	movl	-36(%rsp), %r9d         # 4-byte Reload
	xorl	%eax, %ecx
	movl	-20(%rsp), %eax         # 4-byte Reload
	xorl	%eax, %r9d
	andl	%r15d, %r9d
	movl	%r15d, -28(%rsp)        # 4-byte Spill
	xorl	%eax, %r9d
	xorl	%ecx, %r14d
	movl	36(%r13), %ecx
	movl	48(%r13), %esi
	movl	%ecx, %ebp
	roll	$15, %ebp
	movl	%ecx, %ebx
	roll	$13, %ebx
	shrl	$10, %ecx
	xorl	%ebx, %ecx
	movl	%esi, %eax
	roll	$14, %eax
	xorl	%ebp, %ecx
	movl	%esi, %edx
	shrl	$3, %edx
	xorl	%eax, %edx
	movl	%esi, %eax
	roll	$25, %eax
	xorl	%eax, %edx
	addl	16(%r13), %r12d
	addl	%ecx, %r12d
	addl	%edx, %r12d
	movl	%r12d, 44(%r13)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+44(,%rax,4), %edi
	movl	%r11d, %eax
	roll	$30, %eax
	movl	%r11d, %ecx
	roll	$19, %ecx
	addl	%r12d, %edi
	xorl	%eax, %ecx
	movl	%r11d, %eax
	roll	$10, %eax
	xorl	%ecx, %eax
	movl	%r11d, %ecx
	addl	%r9d, %edi
	andl	%r10d, %ecx
	movl	%r11d, %r8d
	xorl	%r10d, %r8d
	movl	-40(%rsp), %r12d        # 4-byte Reload
	andl	%r12d, %r8d
	addl	%r14d, %edi
	xorl	%ecx, %r8d
	addl	%eax, %r8d
	movl	-24(%rsp), %eax         # 4-byte Reload
	addl	%edi, %eax
	addl	%edi, %r8d
	movl	%eax, %edx
	roll	$26, %edx
	movl	%eax, %ebx
	roll	$21, %ebx
	movl	%eax, %r14d
	movl	%eax, -24(%rsp)         # 4-byte Spill
	roll	$7, %r14d
	xorl	%edx, %ebx
	movl	-36(%rsp), %edx         # 4-byte Reload
	xorl	%edx, %r15d
	andl	%eax, %r15d
	xorl	%edx, %r15d
	xorl	%ebx, %r14d
	movl	40(%r13), %edx
	movl	52(%r13), %ebx
	movl	%edx, %ebp
	roll	$15, %ebp
	movl	%edx, %eax
	roll	$13, %eax
	shrl	$10, %edx
	xorl	%eax, %edx
	movl	%ebx, %eax
	roll	$14, %eax
	xorl	%ebp, %edx
	movl	%ebx, %ebp
	shrl	$3, %ebp
	xorl	%eax, %ebp
	movl	%ebx, %eax
	roll	$25, %eax
	xorl	%eax, %ebp
	addl	20(%r13), %esi
	addl	%edx, %esi
	addl	%ebp, %esi
	movl	%esi, 48(%r13)
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+48(,%rax,4), %esi
	movl	%r8d, %eax
	roll	$30, %eax
	movl	%r8d, %edx
	roll	$19, %edx
	addl	-20(%rsp), %esi         # 4-byte Folded Reload
	xorl	%eax, %edx
	movl	%r8d, %eax
	roll	$10, %eax
	xorl	%edx, %eax
	movl	%r8d, %edx
	addl	%r15d, %esi
	andl	%r11d, %edx
	movl	%r8d, %r9d
	xorl	%r11d, %r9d
	andl	%r10d, %r9d
	addl	%r14d, %esi
	xorl	%edx, %r9d
	addl	%eax, %r9d
	movl	%r12d, %eax
	addl	%esi, %eax
	addl	%esi, %r9d
	movl	%eax, %r14d
	movl	%eax, %ecx
	movl	44(%r13), %edx
	movl	56(%r13), %r15d
	movl	%edx, %esi
	roll	$15, %esi
	movl	%edx, %ebp
	roll	$26, %r14d
	roll	$13, %ebp
	shrl	$10, %edx
	xorl	%ebp, %edx
	roll	$21, %eax
	xorl	%esi, %edx
	movl	%r15d, %ebp
	roll	$14, %ebp
	movl	%r15d, %edi
	shrl	$3, %edi
	xorl	%r14d, %eax
	movl	%ecx, %esi
	movl	%ecx, %r14d
	movl	%r14d, -40(%rsp)        # 4-byte Spill
	xorl	%ebp, %edi
	movl	%r15d, %ebp
	roll	$25, %ebp
	xorl	%ebp, %edi
	movl	-24(%rsp), %r12d        # 4-byte Reload
	movl	%r12d, %ebp
	roll	$7, %esi
	movl	-28(%rsp), %ecx         # 4-byte Reload
	xorl	%ecx, %ebp
	andl	%r14d, %ebp
	addl	24(%r13), %ebx
	xorl	%ecx, %ebp
	addl	%edx, %ebx
	addl	%edi, %ebx
	movl	%ebx, 52(%r13)
	xorl	%eax, %esi
	movq	-48(%rsp), %rax         # 8-byte Reload
	addl	K+52(,%rax,4), %ebx
	movl	%r9d, %eax
	roll	$30, %eax
	movl	%r9d, %edx
	roll	$19, %edx
	addl	-36(%rsp), %ebx         # 4-byte Folded Reload
	xorl	%eax, %edx
	movl	%r9d, %eax
	roll	$10, %eax
	xorl	%edx, %eax
	movl	%r9d, %edx
	addl	%ebp, %ebx
	movl	%r8d, -32(%rsp)         # 4-byte Spill
	andl	%r8d, %edx
	movl	%r9d, %r14d
	xorl	%r8d, %r14d
	andl	%r11d, %r14d
	addl	%esi, %ebx
	xorl	%edx, %r14d
	addl	%eax, %r14d
	addl	%ebx, %r10d
	addl	%ebx, %r14d
	movl	%r10d, %eax
	movq	%r13, %rcx
	movl	48(%rcx), %edx
	movl	60(%rcx), %esi
	movl	%edx, %edi
	roll	$15, %edi
	movl	%edx, %ebx
	roll	$26, %eax
	movl	%r10d, %ebp
	roll	$13, %ebx
	shrl	$10, %edx
	xorl	%ebx, %edx
	roll	$21, %ebp
	xorl	%edi, %edx
	movl	%esi, %edi
	roll	$14, %edi
	movl	%esi, %ebx
	shrl	$3, %ebx
	xorl	%eax, %ebp
	movl	%r10d, %eax
	xorl	%edi, %ebx
	movl	%esi, %edi
	roll	$25, %edi
	xorl	%edi, %ebx
	movl	-40(%rsp), %edi         # 4-byte Reload
	roll	$7, %eax
	xorl	%r12d, %edi
	andl	%r10d, %edi
	addl	28(%rcx), %r15d
	xorl	%r12d, %edi
	addl	%edx, %r15d
	addl	%ebx, %r15d
	movl	%r15d, 56(%rcx)
	xorl	%ebp, %eax
	movq	-48(%rsp), %rdx         # 8-byte Reload
	movl	K+56(,%rdx,4), %edx
	addl	%r15d, %edx
	movl	%r14d, %ebx
	roll	$30, %ebx
	movl	%r14d, %ebp
	addl	-28(%rsp), %edx         # 4-byte Folded Reload
	roll	$19, %ebp
	xorl	%ebx, %ebp
	movl	%r14d, %ebx
	roll	$10, %ebx
	addl	%edi, %edx
	xorl	%ebp, %ebx
	movl	%r14d, %edi
	andl	%r9d, %edi
	movl	%r14d, %r13d
	xorl	%r9d, %r13d
	addl	%eax, %edx
	andl	-32(%rsp), %r13d        # 4-byte Folded Reload
	xorl	%edi, %r13d
	addl	%ebx, %r13d
	addl	%edx, %r11d
	addl	%edx, %r13d
	movl	%r11d, %edx
	roll	$26, %edx
	movl	%r11d, %edi
	roll	$21, %edi
	movl	%r11d, %eax
	xorl	%edx, %edi
	roll	$7, %eax
	movl	(%rcx), %r8d
	movl	52(%rcx), %edx
	movl	%edx, %ebx
	xorl	%edi, %eax
	movl	%edx, %edi
	roll	$13, %edi
	shrl	$10, %edx
	xorl	%edi, %edx
	movl	%r8d, %edi
	roll	$14, %edi
	movl	%r8d, %ebp
	shrl	$3, %ebp
	xorl	%edi, %ebp
	roll	$15, %ebx
	xorl	%ebx, %edx
	movl	%r8d, %edi
	roll	$25, %edi
	xorl	%edi, %ebp
	addl	32(%rcx), %esi
	addl	%edx, %esi
	movl	%r10d, %edx
	movl	-40(%rsp), %edi         # 4-byte Reload
	xorl	%edi, %edx
	andl	%r11d, %edx
	xorl	%edi, %edx
	addl	%ebp, %esi
	movq	-48(%rsp), %rbp         # 8-byte Reload
	movl	K+60(,%rbp,4), %edi
	addl	%esi, %edi
	addl	%r12d, %edi
	movl	%r13d, %ebx
	addl	%edx, %edi
	roll	$30, %ebx
	movl	%r13d, %edx
	roll	$19, %edx
	xorl	%ebx, %edx
	movl	%r13d, %ebx
	addl	%eax, %edi
	roll	$10, %ebx
	xorl	%edx, %ebx
	movl	%r13d, %eax
	andl	%r14d, %eax
	movl	%r13d, %r12d
	xorl	%r14d, %r12d
	andl	%r9d, %r12d
	xorl	%eax, %r12d
	addl	%ebx, %r12d
	movl	-32(%rsp), %eax         # 4-byte Reload
	addl	%edi, %eax
	movl	%eax, %ecx
	addl	%edi, %r12d
	movq	-16(%rsp), %rax         # 8-byte Reload
	movl	%esi, 60(%rax)
	addq	$16, %rbp
	cmpb	$64, %bpl
	jb	.LBB3_3
# BB#4:                                 # %._crit_edge
	movq	-8(%rsp), %rax          # 8-byte Reload
	movdqu	(%rax), %xmm0
	movd	%r9d, %xmm1
	movd	%r13d, %xmm2
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	%r14d, %xmm1
	movd	%r12d, %xmm3
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	paddd	%xmm0, %xmm3
	movdqu	%xmm3, (%rax)
	movdqu	16(%rax), %xmm0
	movd	-40(%rsp), %xmm1        # 4-byte Folded Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movd	%r11d, %xmm2
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	%r10d, %xmm1
	movd	%ecx, %xmm3
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	paddd	%xmm0, %xmm3
	movdqu	%xmm3, 16(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	sha256_transform, .Lfunc_end3-sha256_transform
	.cfi_endproc

	.globl	sha256_digest
	.p2align	4, 0x90
	.type	sha256_digest,@function
sha256_digest:                          # @sha256_digest
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB4_2
# BB#1:                                 # %.preheader.preheader
	movb	3(%rdi), %al
	movb	%al, (%rsi)
	movb	2(%rdi), %al
	movb	%al, 1(%rsi)
	movb	1(%rdi), %al
	movb	%al, 2(%rsi)
	movb	(%rdi), %al
	movb	%al, 3(%rsi)
	movb	7(%rdi), %al
	movb	%al, 4(%rsi)
	movb	6(%rdi), %al
	movb	%al, 5(%rsi)
	movb	5(%rdi), %al
	movb	%al, 6(%rsi)
	movb	4(%rdi), %al
	movb	%al, 7(%rsi)
	movb	11(%rdi), %al
	movb	%al, 8(%rsi)
	movb	10(%rdi), %al
	movb	%al, 9(%rsi)
	movb	9(%rdi), %al
	movb	%al, 10(%rsi)
	movb	8(%rdi), %al
	movb	%al, 11(%rsi)
	movb	15(%rdi), %al
	movb	%al, 12(%rsi)
	movb	14(%rdi), %al
	movb	%al, 13(%rsi)
	movb	13(%rdi), %al
	movb	%al, 14(%rsi)
	movb	12(%rdi), %al
	movb	%al, 15(%rsi)
	movb	19(%rdi), %al
	movb	%al, 16(%rsi)
	movb	18(%rdi), %al
	movb	%al, 17(%rsi)
	movb	17(%rdi), %al
	movb	%al, 18(%rsi)
	movb	16(%rdi), %al
	movb	%al, 19(%rsi)
	movb	23(%rdi), %al
	movb	%al, 20(%rsi)
	movb	22(%rdi), %al
	movb	%al, 21(%rsi)
	movb	21(%rdi), %al
	movb	%al, 22(%rsi)
	movb	20(%rdi), %al
	movb	%al, 23(%rsi)
	movb	27(%rdi), %al
	movb	%al, 24(%rsi)
	movb	26(%rdi), %al
	movb	%al, 25(%rsi)
	movb	25(%rdi), %al
	movb	%al, 26(%rsi)
	movb	24(%rdi), %al
	movb	%al, 27(%rsi)
	movb	31(%rdi), %al
	movb	%al, 28(%rsi)
	movb	30(%rdi), %al
	movb	%al, 29(%rsi)
	movb	29(%rdi), %al
	movb	%al, 30(%rsi)
	movb	28(%rdi), %al
	movb	%al, 31(%rsi)
.LBB4_2:                                # %.loopexit
	retq
.Lfunc_end4:
	.size	sha256_digest, .Lfunc_end4-sha256_digest
	.cfi_endproc

	.type	sha256_init.H0,@object  # @sha256_init.H0
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
sha256_init.H0:
	.long	1779033703              # 0x6a09e667
	.long	3144134277              # 0xbb67ae85
	.long	1013904242              # 0x3c6ef372
	.long	2773480762              # 0xa54ff53a
	.long	1359893119              # 0x510e527f
	.long	2600822924              # 0x9b05688c
	.long	528734635               # 0x1f83d9ab
	.long	1541459225              # 0x5be0cd19
	.size	sha256_init.H0, 32

	.type	K,@object               # @K
	.section	.rodata,"a",@progbits
	.p2align	4
K:
	.long	1116352408              # 0x428a2f98
	.long	1899447441              # 0x71374491
	.long	3049323471              # 0xb5c0fbcf
	.long	3921009573              # 0xe9b5dba5
	.long	961987163               # 0x3956c25b
	.long	1508970993              # 0x59f111f1
	.long	2453635748              # 0x923f82a4
	.long	2870763221              # 0xab1c5ed5
	.long	3624381080              # 0xd807aa98
	.long	310598401               # 0x12835b01
	.long	607225278               # 0x243185be
	.long	1426881987              # 0x550c7dc3
	.long	1925078388              # 0x72be5d74
	.long	2162078206              # 0x80deb1fe
	.long	2614888103              # 0x9bdc06a7
	.long	3248222580              # 0xc19bf174
	.long	3835390401              # 0xe49b69c1
	.long	4022224774              # 0xefbe4786
	.long	264347078               # 0xfc19dc6
	.long	604807628               # 0x240ca1cc
	.long	770255983               # 0x2de92c6f
	.long	1249150122              # 0x4a7484aa
	.long	1555081692              # 0x5cb0a9dc
	.long	1996064986              # 0x76f988da
	.long	2554220882              # 0x983e5152
	.long	2821834349              # 0xa831c66d
	.long	2952996808              # 0xb00327c8
	.long	3210313671              # 0xbf597fc7
	.long	3336571891              # 0xc6e00bf3
	.long	3584528711              # 0xd5a79147
	.long	113926993               # 0x6ca6351
	.long	338241895               # 0x14292967
	.long	666307205               # 0x27b70a85
	.long	773529912               # 0x2e1b2138
	.long	1294757372              # 0x4d2c6dfc
	.long	1396182291              # 0x53380d13
	.long	1695183700              # 0x650a7354
	.long	1986661051              # 0x766a0abb
	.long	2177026350              # 0x81c2c92e
	.long	2456956037              # 0x92722c85
	.long	2730485921              # 0xa2bfe8a1
	.long	2820302411              # 0xa81a664b
	.long	3259730800              # 0xc24b8b70
	.long	3345764771              # 0xc76c51a3
	.long	3516065817              # 0xd192e819
	.long	3600352804              # 0xd6990624
	.long	4094571909              # 0xf40e3585
	.long	275423344               # 0x106aa070
	.long	430227734               # 0x19a4c116
	.long	506948616               # 0x1e376c08
	.long	659060556               # 0x2748774c
	.long	883997877               # 0x34b0bcb5
	.long	958139571               # 0x391c0cb3
	.long	1322822218              # 0x4ed8aa4a
	.long	1537002063              # 0x5b9cca4f
	.long	1747873779              # 0x682e6ff3
	.long	1955562222              # 0x748f82ee
	.long	2024104815              # 0x78a5636f
	.long	2227730452              # 0x84c87814
	.long	2361852424              # 0x8cc70208
	.long	2428436474              # 0x90befffa
	.long	2756734187              # 0xa4506ceb
	.long	3204031479              # 0xbef9a3f7
	.long	3329325298              # 0xc67178f2
	.size	K, 256


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
