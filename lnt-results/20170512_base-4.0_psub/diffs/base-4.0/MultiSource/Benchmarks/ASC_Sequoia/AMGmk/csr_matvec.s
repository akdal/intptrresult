	.text
	.file	"csr_matvec.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	4604480259023595110     # double 0.69999999999999996
	.text
	.globl	hypre_CSRMatrixMatvec
	.p2align	4, 0x90
	.type	hypre_CSRMatrixMatvec,@function
hypre_CSRMatrixMatvec:                  # @hypre_CSRMatrixMatvec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movapd	%xmm0, %xmm4
	movl	24(%rdi), %r10d
	movl	28(%rdi), %r8d
	movl	8(%rsi), %ecx
	xorl	%ebp, %ebp
	cmpl	%ecx, %r8d
	setne	%bpl
	cmpl	8(%rdx), %r10d
	movl	$2, %ebx
	cmovel	%ebp, %ebx
	movl	$3, %eax
	cmovel	%ebp, %eax
	cmpl	%ecx, %r8d
	movq	(%rdx), %r13
	movl	16(%rsi), %r11d
	cmovel	%ebx, %eax
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm4
	jne	.LBB0_4
	jp	.LBB0_4
# BB#1:                                 # %.preheader
	imull	%r10d, %r11d
	testl	%r11d, %r11d
	jle	.LBB0_75
# BB#2:                                 # %.lr.ph.preheader
	movl	%r11d, %edi
	cmpl	$4, %r11d
	jb	.LBB0_18
# BB#8:                                 # %min.iters.checked391
	andl	$3, %r11d
	movq	%rdi, %rcx
	subq	%r11, %rcx
	je	.LBB0_18
# BB#9:                                 # %vector.ph395
	movaps	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%r13), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB0_10:                               # %vector.body387
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rdx), %xmm2
	movupd	(%rdx), %xmm3
	mulpd	%xmm0, %xmm2
	mulpd	%xmm0, %xmm3
	movupd	%xmm2, -16(%rdx)
	movupd	%xmm3, (%rdx)
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB0_10
# BB#11:                                # %middle.block388
	testl	%r11d, %r11d
	jne	.LBB0_19
	jmp	.LBB0_75
.LBB0_4:
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movq	(%rdi), %rbp
	movq	8(%rdi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rdi), %r14
	movq	40(%rdi), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	48(%rdi), %r8d
	movq	(%rsi), %r15
	movslq	28(%rdx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movslq	24(%rdx), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	28(%rsi), %r12d
	movslq	24(%rsi), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	divsd	%xmm4, %xmm1
	ucomisd	.LCPI0_0(%rip), %xmm1
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%r11, 24(%rsp)          # 8-byte Spill
	movq	%r8, 72(%rsp)           # 8-byte Spill
	jne	.LBB0_5
	jnp	.LBB0_24
.LBB0_5:
	movl	%r11d, %ecx
	imull	%r10d, %ecx
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_12
	jp	.LBB0_12
# BB#6:                                 # %.preheader233
	testl	%ecx, %ecx
	jle	.LBB0_24
# BB#7:                                 # %.lr.ph288.preheader
	decl	%ecx
	leaq	8(,%rcx,8), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movapd	%xmm4, 48(%rsp)         # 16-byte Spill
	callq	memset
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	movapd	48(%rsp), %xmm4         # 16-byte Reload
	jmp	.LBB0_24
.LBB0_18:
	xorl	%ecx, %ecx
.LBB0_19:                               # %.lr.ph.preheader408
	leaq	(%r13,%rcx,8), %rdx
	subq	%rcx, %rdi
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	decq	%rdi
	jne	.LBB0_20
	jmp	.LBB0_75
.LBB0_12:                               # %.preheader235
	testl	%ecx, %ecx
	jle	.LBB0_24
# BB#13:                                # %.lr.ph290.preheader
	movl	%ecx, %eax
	cmpl	$3, %ecx
	jbe	.LBB0_21
# BB#14:                                # %min.iters.checked
	andl	$3, %ecx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	je	.LBB0_21
# BB#15:                                # %vector.ph
	movapd	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%r13), %rsi
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB0_16:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rsi), %xmm2
	movupd	(%rsi), %xmm3
	mulpd	%xmm0, %xmm2
	mulpd	%xmm0, %xmm3
	movupd	%xmm2, -16(%rsi)
	movupd	%xmm3, (%rsi)
	addq	$32, %rsi
	addq	$-4, %rdi
	jne	.LBB0_16
# BB#17:                                # %middle.block
	testl	%ecx, %ecx
	jne	.LBB0_22
	jmp	.LBB0_24
.LBB0_21:
	xorl	%edx, %edx
.LBB0_22:                               # %.lr.ph290.preheader414
	leaq	(%r13,%rdx,8), %rcx
	subq	%rdx, %rax
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph290
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	decq	%rax
	jne	.LBB0_23
.LBB0_24:                               # %.loopexit234
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r10d, %xmm1
	mulsd	.LCPI0_1(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.LBB0_33
# BB#25:                                # %.preheader227
	testl	%r8d, %r8d
	jle	.LBB0_64
# BB#26:                                # %.lr.ph252
	xorl	%eax, %eax
	cmpl	$1, %r11d
	jne	.LBB0_42
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph252.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_31 Depth 2
	movq	80(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%rax,4), %rcx
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	16(%rsp), %rdx          # 8-byte Reload
	movslq	(%rdx,%rcx,4), %rdi
	movslq	4(%rdx,%rcx,4), %rdx
	cmpl	%edx, %edi
	jge	.LBB0_32
# BB#28:                                # %.lr.ph248.us
                                        #   in Loop: Header=BB0_27 Depth=1
	movl	%edx, %esi
	subl	%edi, %esi
	leaq	-1(%rdx), %rbx
	testb	$1, %sil
	jne	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_27 Depth=1
	movq	%rdi, %rsi
	cmpq	%rdi, %rbx
	jne	.LBB0_31
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_27 Depth=1
	movsd	(%rbp,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movslq	(%r14,%rdi,4), %rsi
	mulsd	(%r15,%rsi,8), %xmm1
	addsd	%xmm1, %xmm0
	leaq	1(%rdi), %rsi
	cmpq	%rdi, %rbx
	je	.LBB0_32
	.p2align	4, 0x90
.LBB0_31:                               #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movslq	(%r14,%rsi,4), %rdi
	mulsd	(%r15,%rdi,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	8(%rbp,%rsi,8), %xmm0   # xmm0 = mem[0],zero
	movslq	4(%r14,%rsi,4), %rdi
	mulsd	(%r15,%rdi,8), %xmm0
	addsd	%xmm1, %xmm0
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jl	.LBB0_31
.LBB0_32:                               # %._crit_edge249.us
                                        #   in Loop: Header=BB0_27 Depth=1
	movsd	%xmm0, (%r13,%rcx,8)
	incq	%rax
	cmpq	%r8, %rax
	jne	.LBB0_27
	jmp	.LBB0_64
.LBB0_33:                               # %.preheader231
	testl	%r10d, %r10d
	jle	.LBB0_64
# BB#34:                                # %.lr.ph277
	cmpl	$1, %r11d
	jne	.LBB0_52
# BB#35:                                # %.lr.ph277.split.us.preheader
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_36:                               # %.lr.ph277.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_40 Depth 2
	movq	%r9, %rdx
	movl	%eax, %edi
	movsd	(%r13,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	leaq	1(%rdx), %r9
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%rdx,4), %eax
	cmpl	%eax, %edi
	jge	.LBB0_41
# BB#37:                                # %.lr.ph273.us
                                        #   in Loop: Header=BB0_36 Depth=1
	movslq	%eax, %rsi
	movslq	%edi, %rbx
	movl	%eax, %ecx
	subl	%edi, %ecx
	leaq	-1(%rsi), %r8
	testb	$1, %cl
	jne	.LBB0_39
# BB#38:                                #   in Loop: Header=BB0_36 Depth=1
	movq	%rbx, %rdi
	cmpq	%rbx, %r8
	jne	.LBB0_40
	jmp	.LBB0_41
	.p2align	4, 0x90
.LBB0_39:                               #   in Loop: Header=BB0_36 Depth=1
	movsd	(%rbp,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movslq	(%r14,%rbx,4), %rcx
	mulsd	(%r15,%rcx,8), %xmm1
	addsd	%xmm1, %xmm0
	leaq	1(%rbx), %rdi
	cmpq	%rbx, %r8
	je	.LBB0_41
	.p2align	4, 0x90
.LBB0_40:                               #   Parent Loop BB0_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movslq	(%r14,%rdi,4), %rbx
	mulsd	(%r15,%rbx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	8(%rbp,%rdi,8), %xmm0   # xmm0 = mem[0],zero
	movslq	4(%r14,%rdi,4), %rbx
	mulsd	(%r15,%rbx,8), %xmm0
	addsd	%xmm1, %xmm0
	addq	$2, %rdi
	cmpq	%rsi, %rdi
	jl	.LBB0_40
.LBB0_41:                               # %._crit_edge274.us
                                        #   in Loop: Header=BB0_36 Depth=1
	movsd	%xmm0, (%r13,%rdx,8)
	cmpq	%r10, %r9
	jne	.LBB0_36
	jmp	.LBB0_64
.LBB0_42:                               # %.lr.ph252.split.preheader
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_43:                               # %.lr.ph252.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_46 Depth 2
                                        #       Child Loop BB0_49 Depth 3
	testl	%r11d, %r11d
	jle	.LBB0_51
# BB#44:                                # %.lr.ph244
                                        #   in Loop: Header=BB0_43 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movslq	(%rdx,%rax,4), %rcx
	movslq	4(%rdx,%rax,4), %rdx
	cmpl	%edx, %ecx
	jge	.LBB0_51
# BB#45:                                # %.lr.ph244.split.us.preheader
                                        #   in Loop: Header=BB0_43 Depth=1
	movq	64(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	imull	%eax, %esi
	movslq	%esi, %r10
	movl	%edx, %r8d
	subl	%ecx, %r8d
	leaq	-1(%rdx), %r11
	andl	$1, %r8d
	leaq	1(%rcx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph244.split.us
                                        #   Parent Loop BB0_43 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_49 Depth 3
	movq	%rbx, %rsi
	imulq	96(%rsp), %rsi          # 8-byte Folded Reload
	addq	%r10, %rsi
	movl	%ebx, %eax
	imull	88(%rsp), %eax          # 4-byte Folded Reload
	testq	%r8, %r8
	movsd	(%r13,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	jne	.LBB0_48
# BB#47:                                #   in Loop: Header=BB0_46 Depth=2
	movq	%rcx, %r9
	cmpq	%rcx, %r11
	jne	.LBB0_49
	jmp	.LBB0_50
	.p2align	4, 0x90
.LBB0_48:                               #   in Loop: Header=BB0_46 Depth=2
	movsd	(%rbp,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movl	(%r14,%rcx,4), %edi
	imull	%r12d, %edi
	addl	%eax, %edi
	movslq	%edi, %rdi
	mulsd	(%r15,%rdi,8), %xmm1
	addsd	%xmm1, %xmm0
	movq	48(%rsp), %r9           # 8-byte Reload
	cmpq	%rcx, %r11
	je	.LBB0_50
	.p2align	4, 0x90
.LBB0_49:                               #   Parent Loop BB0_43 Depth=1
                                        #     Parent Loop BB0_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbp,%r9,8), %xmm1     # xmm1 = mem[0],zero
	movl	(%r14,%r9,4), %edi
	imull	%r12d, %edi
	addl	%eax, %edi
	movslq	%edi, %rdi
	mulsd	(%r15,%rdi,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	8(%rbp,%r9,8), %xmm0    # xmm0 = mem[0],zero
	movl	4(%r14,%r9,4), %edi
	imull	%r12d, %edi
	addl	%eax, %edi
	movslq	%edi, %rdi
	mulsd	(%r15,%rdi,8), %xmm0
	addsd	%xmm1, %xmm0
	addq	$2, %r9
	cmpq	%rdx, %r9
	jl	.LBB0_49
.LBB0_50:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_46 Depth=2
	movsd	%xmm0, (%r13,%rsi,8)
	incq	%rbx
	cmpq	24(%rsp), %rbx          # 8-byte Folded Reload
	jne	.LBB0_46
.LBB0_51:                               # %.loopexit226
                                        #   in Loop: Header=BB0_43 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	72(%rsp), %rcx          # 8-byte Folded Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	jne	.LBB0_43
	jmp	.LBB0_64
.LBB0_52:                               # %.lr.ph277.split.preheader
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_53:                               # %.lr.ph277.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_56 Depth 2
                                        #       Child Loop BB0_59 Depth 3
	testl	%r11d, %r11d
	jle	.LBB0_62
# BB#54:                                # %.lr.ph269
                                        #   in Loop: Header=BB0_53 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%r8,4), %rdx
	leaq	1(%r8), %rcx
	movslq	4(%rax,%r8,4), %rsi
	cmpl	%esi, %edx
	jge	.LBB0_63
# BB#55:                                # %.lr.ph269.split.us.preheader
                                        #   in Loop: Header=BB0_53 Depth=1
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	imulq	64(%rsp), %r8           # 8-byte Folded Reload
	movl	%esi, %r11d
	subl	%edx, %r11d
	leaq	-1(%rsi), %r10
	andl	$1, %r11d
	leaq	1(%rdx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_56:                               # %.lr.ph269.split.us
                                        #   Parent Loop BB0_53 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_59 Depth 3
	movq	%rbx, %rcx
	imulq	96(%rsp), %rcx          # 8-byte Folded Reload
	addq	%r8, %rcx
	movl	%ebx, %edi
	imull	88(%rsp), %edi          # 4-byte Folded Reload
	testq	%r11, %r11
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	jne	.LBB0_58
# BB#57:                                #   in Loop: Header=BB0_56 Depth=2
	movq	%rdx, %r9
	cmpq	%rdx, %r10
	jne	.LBB0_59
	jmp	.LBB0_60
	.p2align	4, 0x90
.LBB0_58:                               #   in Loop: Header=BB0_56 Depth=2
	movsd	(%rbp,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movl	(%r14,%rdx,4), %eax
	imull	%r12d, %eax
	addl	%edi, %eax
	cltq
	mulsd	(%r15,%rax,8), %xmm1
	addsd	%xmm1, %xmm0
	movq	48(%rsp), %r9           # 8-byte Reload
	cmpq	%rdx, %r10
	je	.LBB0_60
	.p2align	4, 0x90
.LBB0_59:                               #   Parent Loop BB0_53 Depth=1
                                        #     Parent Loop BB0_56 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbp,%r9,8), %xmm1     # xmm1 = mem[0],zero
	movl	(%r14,%r9,4), %eax
	imull	%r12d, %eax
	addl	%edi, %eax
	cltq
	mulsd	(%r15,%rax,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	8(%rbp,%r9,8), %xmm0    # xmm0 = mem[0],zero
	movl	4(%r14,%r9,4), %eax
	imull	%r12d, %eax
	addl	%edi, %eax
	cltq
	mulsd	(%r15,%rax,8), %xmm0
	addsd	%xmm1, %xmm0
	addq	$2, %r9
	cmpq	%rsi, %r9
	jl	.LBB0_59
.LBB0_60:                               # %._crit_edge266.us
                                        #   in Loop: Header=BB0_56 Depth=2
	movsd	%xmm0, (%r13,%rcx,8)
	incq	%rbx
	cmpq	24(%rsp), %rbx          # 8-byte Folded Reload
	jne	.LBB0_56
# BB#61:                                #   in Loop: Header=BB0_53 Depth=1
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	jmp	.LBB0_63
	.p2align	4, 0x90
.LBB0_62:                               # %.lr.ph277.split..loopexit230_crit_edge
                                        #   in Loop: Header=BB0_53 Depth=1
	incq	%r8
	movq	%r8, %rcx
.LBB0_63:                               # %.loopexit230
                                        #   in Loop: Header=BB0_53 Depth=1
	cmpq	%r10, %rcx
	movq	%rcx, %r8
	jne	.LBB0_53
.LBB0_64:                               # %.loopexit228
	ucomisd	.LCPI0_0(%rip), %xmm4
	movl	44(%rsp), %eax          # 4-byte Reload
	jne	.LBB0_65
	jnp	.LBB0_75
.LBB0_65:                               # %.preheader223
	imull	%r11d, %r10d
	testl	%r10d, %r10d
	jle	.LBB0_75
# BB#66:                                # %.lr.ph239.preheader
	movl	%r10d, %edi
	cmpl	$4, %r10d
	jb	.LBB0_72
# BB#68:                                # %min.iters.checked370
	andl	$3, %r10d
	movq	%rdi, %rcx
	subq	%r10, %rcx
	je	.LBB0_72
# BB#69:                                # %vector.ph374
	movapd	%xmm4, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%r13), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB0_70:                               # %vector.body366
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rdx), %xmm1
	movupd	(%rdx), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, -16(%rdx)
	movupd	%xmm2, (%rdx)
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB0_70
# BB#71:                                # %middle.block367
	testl	%r10d, %r10d
	jne	.LBB0_73
	jmp	.LBB0_75
.LBB0_72:
	xorl	%ecx, %ecx
.LBB0_73:                               # %.lr.ph239.preheader409
	leaq	(%r13,%rcx,8), %rdx
	subq	%rcx, %rdi
	.p2align	4, 0x90
.LBB0_74:                               # %.lr.ph239
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	decq	%rdi
	jne	.LBB0_74
.LBB0_75:                               # %.loopexit
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_CSRMatrixMatvec, .Lfunc_end0-hypre_CSRMatrixMatvec
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	hypre_CSRMatrixMatvecT
	.p2align	4, 0x90
	.type	hypre_CSRMatrixMatvecT,@function
hypre_CSRMatrixMatvecT:                 # @hypre_CSRMatrixMatvecT
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movapd	%xmm0, %xmm4
	movl	24(%rdi), %r13d
	movl	28(%rdi), %r8d
	movl	8(%rsi), %ebx
	xorl	%ecx, %ecx
	cmpl	%ebx, %r13d
	setne	%cl
	cmpl	8(%rdx), %r8d
	movl	$2, %ebp
	cmovel	%ecx, %ebp
	movl	$3, %eax
	cmovel	%ecx, %eax
	cmpl	%ebx, %r13d
	movq	(%rdx), %rbx
	movl	16(%rsi), %r11d
	cmovel	%ebp, %eax
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm4
	jne	.LBB1_4
	jp	.LBB1_4
# BB#1:                                 # %.preheader
	imull	%r8d, %r11d
	testl	%r11d, %r11d
	jle	.LBB1_57
# BB#2:                                 # %.lr.ph.preheader
	movl	%r11d, %edi
	cmpl	$4, %r11d
	jb	.LBB1_18
# BB#8:                                 # %min.iters.checked332
	andl	$3, %r11d
	movq	%rdi, %rcx
	subq	%r11, %rcx
	je	.LBB1_18
# BB#9:                                 # %vector.ph336
	movaps	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%rbx), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB1_10:                               # %vector.body328
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rdx), %xmm2
	movupd	(%rdx), %xmm3
	mulpd	%xmm0, %xmm2
	mulpd	%xmm0, %xmm3
	movupd	%xmm2, -16(%rdx)
	movupd	%xmm3, (%rdx)
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB1_10
# BB#11:                                # %middle.block329
	testl	%r11d, %r11d
	jne	.LBB1_19
	jmp	.LBB1_57
.LBB1_4:
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	(%rdi), %rbp
	movq	8(%rdi), %r9
	movq	16(%rdi), %r14
	movq	(%rsi), %r15
	movl	28(%rdx), %r12d
	movslq	24(%rdx), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movslq	28(%rsi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movslq	24(%rsi), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	divsd	%xmm4, %xmm1
	ucomisd	.LCPI1_0(%rip), %xmm1
	movq	%r11, 8(%rsp)           # 8-byte Spill
	movl	%r8d, 16(%rsp)          # 4-byte Spill
	movq	%r9, (%rsp)             # 8-byte Spill
	jne	.LBB1_5
	jnp	.LBB1_24
.LBB1_5:
	movl	%r11d, %ecx
	imull	%r8d, %ecx
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB1_12
	jp	.LBB1_12
# BB#6:                                 # %.preheader239
	testl	%ecx, %ecx
	jle	.LBB1_24
# BB#7:                                 # %.lr.ph262.preheader
	decl	%ecx
	leaq	8(,%rcx,8), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movapd	%xmm4, 32(%rsp)         # 16-byte Spill
	callq	memset
	movq	(%rsp), %r9             # 8-byte Reload
	movq	8(%rsp), %r11           # 8-byte Reload
	movapd	32(%rsp), %xmm4         # 16-byte Reload
	testl	%r13d, %r13d
	jg	.LBB1_25
	jmp	.LBB1_46
.LBB1_18:
	xorl	%ecx, %ecx
.LBB1_19:                               # %.lr.ph.preheader349
	leaq	(%rbx,%rcx,8), %rdx
	subq	%rcx, %rdi
	.p2align	4, 0x90
.LBB1_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	decq	%rdi
	jne	.LBB1_20
	jmp	.LBB1_57
.LBB1_12:                               # %.preheader240
	testl	%ecx, %ecx
	jle	.LBB1_24
# BB#13:                                # %.lr.ph264.preheader
	movl	%ecx, %eax
	cmpl	$3, %ecx
	jbe	.LBB1_21
# BB#14:                                # %min.iters.checked
	andl	$3, %ecx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	je	.LBB1_21
# BB#15:                                # %vector.ph
	movapd	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%rbx), %rsi
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB1_16:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rsi), %xmm2
	movupd	(%rsi), %xmm3
	mulpd	%xmm0, %xmm2
	mulpd	%xmm0, %xmm3
	movupd	%xmm2, -16(%rsi)
	movupd	%xmm3, (%rsi)
	addq	$32, %rsi
	addq	$-4, %rdi
	jne	.LBB1_16
# BB#17:                                # %middle.block
	testl	%ecx, %ecx
	jne	.LBB1_22
	jmp	.LBB1_24
.LBB1_21:
	xorl	%edx, %edx
.LBB1_22:                               # %.lr.ph264.preheader353
	leaq	(%rbx,%rdx,8), %rcx
	subq	%rdx, %rax
	.p2align	4, 0x90
.LBB1_23:                               # %.lr.ph264
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	decq	%rax
	jne	.LBB1_23
.LBB1_24:                               # %.preheader238
	testl	%r13d, %r13d
	jle	.LBB1_46
.LBB1_25:                               # %.lr.ph253
	cmpl	$1, %r11d
	jne	.LBB1_33
# BB#26:                                # %.lr.ph253.split.us.preheader
	movl	(%r9), %r10d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_27:                               # %.lr.ph253.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_31 Depth 2
	movq	%r8, %rdx
	movl	%r10d, %edi
	leaq	1(%rdx), %r8
	movl	4(%r9,%rdx,4), %r10d
	cmpl	%r10d, %edi
	jge	.LBB1_32
# BB#28:                                # %.lr.ph251.us
                                        #   in Loop: Header=BB1_27 Depth=1
	movslq	%r10d, %rsi
	movslq	%edi, %rcx
	movl	%r10d, %eax
	subl	%edi, %eax
	leaq	-1(%rsi), %r9
	testb	$1, %al
	movq	%rcx, %rdi
	je	.LBB1_30
# BB#29:                                #   in Loop: Header=BB1_27 Depth=1
	movsd	(%rbp,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r15,%rdx,8), %xmm0
	movslq	(%r14,%rcx,4), %rax
	addsd	(%rbx,%rax,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	leaq	1(%rcx), %rdi
.LBB1_30:                               # %.prol.loopexit
                                        #   in Loop: Header=BB1_27 Depth=1
	cmpq	%rcx, %r9
	movq	(%rsp), %r9             # 8-byte Reload
	je	.LBB1_32
	.p2align	4, 0x90
.LBB1_31:                               #   Parent Loop BB1_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbp,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r15,%rdx,8), %xmm0
	movslq	(%r14,%rdi,4), %rcx
	addsd	(%rbx,%rcx,8), %xmm0
	movsd	%xmm0, (%rbx,%rcx,8)
	movsd	8(%rbp,%rdi,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	(%r15,%rdx,8), %xmm0
	movslq	4(%r14,%rdi,4), %rcx
	addsd	(%rbx,%rcx,8), %xmm0
	movsd	%xmm0, (%rbx,%rcx,8)
	addq	$2, %rdi
	cmpq	%rsi, %rdi
	jl	.LBB1_31
.LBB1_32:                               # %.loopexit235.us
                                        #   in Loop: Header=BB1_27 Depth=1
	cmpq	%r13, %r8
	jne	.LBB1_27
	jmp	.LBB1_46
.LBB1_33:                               # %.lr.ph253.split.preheader
	xorl	%r10d, %r10d
	movq	%r13, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_34:                               # %.lr.ph253.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_37 Depth 2
                                        #       Child Loop BB1_40 Depth 3
	testl	%r11d, %r11d
	jle	.LBB1_43
# BB#35:                                # %.lr.ph248
                                        #   in Loop: Header=BB1_34 Depth=1
	movslq	(%r9,%r10,4), %r13
	leaq	1(%r10), %rax
	movslq	4(%r9,%r10,4), %rsi
	cmpl	%esi, %r13d
	jge	.LBB1_44
# BB#36:                                # %.lr.ph248.split.us.preheader
                                        #   in Loop: Header=BB1_34 Depth=1
	movq	%rax, 56(%rsp)          # 8-byte Spill
	imulq	48(%rsp), %r10          # 8-byte Folded Reload
	movl	%esi, %r11d
	subl	%r13d, %r11d
	leaq	-1(%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	andl	$1, %r11d
	leaq	1(%r13), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_37:                               # %.lr.ph248.split.us
                                        #   Parent Loop BB1_34 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_40 Depth 3
	movq	%rdi, %rcx
	imulq	72(%rsp), %rcx          # 8-byte Folded Reload
	addq	%r10, %rcx
	movl	%edi, %r8d
	imull	80(%rsp), %r8d          # 4-byte Folded Reload
	testq	%r11, %r11
	movq	%r13, %rdx
	je	.LBB1_39
# BB#38:                                #   in Loop: Header=BB1_37 Depth=2
	movsd	(%rbp,%r13,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r15,%rcx,8), %xmm0
	movl	(%r14,%r13,4), %edx
	imull	%r12d, %edx
	addl	%r8d, %edx
	movslq	%edx, %rdx
	addsd	(%rbx,%rdx,8), %xmm0
	movsd	%xmm0, (%rbx,%rdx,8)
	movq	64(%rsp), %rdx          # 8-byte Reload
.LBB1_39:                               # %.prol.loopexit355
                                        #   in Loop: Header=BB1_37 Depth=2
	cmpq	%r13, 32(%rsp)          # 8-byte Folded Reload
	je	.LBB1_41
	.p2align	4, 0x90
.LBB1_40:                               #   Parent Loop BB1_34 Depth=1
                                        #     Parent Loop BB1_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rbp,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r15,%rcx,8), %xmm0
	movl	(%r14,%rdx,4), %r9d
	imull	%r12d, %r9d
	addl	%r8d, %r9d
	movslq	%r9d, %rax
	addsd	(%rbx,%rax,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	movsd	8(%rbp,%rdx,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	(%r15,%rcx,8), %xmm0
	movl	4(%r14,%rdx,4), %eax
	imull	%r12d, %eax
	addl	%r8d, %eax
	cltq
	addsd	(%rbx,%rax,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	addq	$2, %rdx
	cmpq	%rsi, %rdx
	jl	.LBB1_40
.LBB1_41:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_37 Depth=2
	incq	%rdi
	cmpq	8(%rsp), %rdi           # 8-byte Folded Reload
	jne	.LBB1_37
# BB#42:                                #   in Loop: Header=BB1_34 Depth=1
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	(%rsp), %r9             # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_45
	.p2align	4, 0x90
.LBB1_43:                               # %.lr.ph253.split..loopexit237_crit_edge
                                        #   in Loop: Header=BB1_34 Depth=1
	incq	%r10
	movq	%r10, %rax
	jmp	.LBB1_45
	.p2align	4, 0x90
.LBB1_44:                               #   in Loop: Header=BB1_34 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB1_45:                               # %.loopexit237
                                        #   in Loop: Header=BB1_34 Depth=1
	cmpq	%r13, %rax
	movq	%rax, %r10
	jne	.LBB1_34
.LBB1_46:                               # %._crit_edge254
	ucomisd	.LCPI1_0(%rip), %xmm4
	movl	20(%rsp), %eax          # 4-byte Reload
	jne	.LBB1_47
	jnp	.LBB1_57
.LBB1_47:                               # %.preheader233
	imull	16(%rsp), %r11d         # 4-byte Folded Reload
	testl	%r11d, %r11d
	jle	.LBB1_57
# BB#48:                                # %.lr.ph244.preheader
	movl	%r11d, %edi
	cmpl	$4, %r11d
	jb	.LBB1_54
# BB#50:                                # %min.iters.checked311
	andl	$3, %r11d
	movq	%rdi, %rcx
	subq	%r11, %rcx
	je	.LBB1_54
# BB#51:                                # %vector.ph315
	movapd	%xmm4, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	leaq	16(%rbx), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB1_52:                               # %vector.body307
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rdx), %xmm1
	movupd	(%rdx), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movupd	%xmm1, -16(%rdx)
	movupd	%xmm2, (%rdx)
	addq	$32, %rdx
	addq	$-4, %rsi
	jne	.LBB1_52
# BB#53:                                # %middle.block308
	testl	%r11d, %r11d
	jne	.LBB1_55
	jmp	.LBB1_57
.LBB1_54:
	xorl	%ecx, %ecx
.LBB1_55:                               # %.lr.ph244.preheader350
	leaq	(%rbx,%rcx,8), %rdx
	subq	%rcx, %rdi
	.p2align	4, 0x90
.LBB1_56:                               # %.lr.ph244
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	decq	%rdi
	jne	.LBB1_56
.LBB1_57:                               # %.loopexit
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_CSRMatrixMatvecT, .Lfunc_end1-hypre_CSRMatrixMatvecT
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	hypre_CSRMatrixMatvec_FF
	.p2align	4, 0x90
	.type	hypre_CSRMatrixMatvec_FF,@function
hypre_CSRMatrixMatvec_FF:               # @hypre_CSRMatrixMatvec_FF
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	24(%rdi), %r10d
	movl	28(%rdi), %r11d
	movl	8(%rsi), %r14d
	xorl	%ebp, %ebp
	cmpl	%r14d, %r11d
	setne	%bpl
	cmpl	8(%rdx), %r10d
	movl	$2, %ebx
	cmovel	%ebp, %ebx
	movl	$3, %eax
	cmovel	%ebp, %eax
	cmpl	%r14d, %r11d
	movq	(%rdx), %r11
	cmovel	%ebx, %eax
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jne	.LBB2_4
	jp	.LBB2_4
# BB#1:                                 # %.preheader
	testl	%r10d, %r10d
	jle	.LBB2_75
# BB#2:                                 # %.lr.ph.preheader
	testb	$1, %r10b
	jne	.LBB2_22
# BB#3:
	xorl	%edx, %edx
	cmpl	$1, %r10d
	jne	.LBB2_25
	jmp	.LBB2_75
.LBB2_4:
	movq	(%rdi), %r12
	movq	8(%rdi), %r15
	movq	16(%rdi), %rdi
	movq	(%rsi), %r13
	divsd	%xmm0, %xmm1
	ucomisd	.LCPI2_0(%rip), %xmm1
	jne	.LBB2_5
	jnp	.LBB2_45
.LBB2_5:
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm1
	jne	.LBB2_31
	jp	.LBB2_31
# BB#6:                                 # %.preheader120
	testl	%r10d, %r10d
	jle	.LBB2_35
# BB#7:                                 # %.lr.ph133.preheader
	leaq	-1(%r10), %rdx
	movq	%r10, %rsi
	xorl	%ebx, %ebx
	andq	$3, %rsi
	je	.LBB2_11
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph133.prol
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, (%rcx,%rbx,4)
	jne	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	movq	$0, (%r11,%rbx,8)
.LBB2_10:                               #   in Loop: Header=BB2_8 Depth=1
	incq	%rbx
	cmpq	%rbx, %rsi
	jne	.LBB2_8
.LBB2_11:                               # %.lr.ph133.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB2_45
# BB#12:                                # %.lr.ph133.preheader.new
	movq	%r10, %rdx
	subq	%rbx, %rdx
	leaq	24(%r11,%rbx,8), %rsi
	leaq	12(%rcx,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph133
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, -12(%rbx)
	jne	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_13 Depth=1
	movq	$0, -24(%rsi)
.LBB2_15:                               # %.lr.ph133.1171
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpl	%r9d, -8(%rbx)
	jne	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_13 Depth=1
	movq	$0, -16(%rsi)
.LBB2_17:                               # %.lr.ph133.2172
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpl	%r9d, -4(%rbx)
	jne	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_13 Depth=1
	movq	$0, -8(%rsi)
.LBB2_19:                               # %.lr.ph133.3173
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpl	%r9d, (%rbx)
	jne	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_13 Depth=1
	movq	$0, (%rsi)
.LBB2_21:                               #   in Loop: Header=BB2_13 Depth=1
	addq	$32, %rsi
	addq	$16, %rbx
	addq	$-4, %rdx
	jne	.LBB2_13
	jmp	.LBB2_45
.LBB2_22:                               # %.lr.ph.prol
	cmpl	%r9d, (%rcx)
	jne	.LBB2_24
# BB#23:
	movsd	(%r11), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%r11)
.LBB2_24:                               # %.lr.ph.prol.loopexit
	movl	$1, %edx
	cmpl	$1, %r10d
	je	.LBB2_75
.LBB2_25:                               # %.lr.ph.preheader.new
	subq	%rdx, %r10
	leaq	4(%rcx,%rdx,4), %rcx
	leaq	8(%r11,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB2_26:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, -4(%rcx)
	jne	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_26 Depth=1
	movsd	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, -8(%rdx)
.LBB2_28:                               # %.lr.ph.1163
                                        #   in Loop: Header=BB2_26 Depth=1
	cmpl	%r9d, (%rcx)
	jne	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_26 Depth=1
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdx)
.LBB2_30:                               #   in Loop: Header=BB2_26 Depth=1
	addq	$8, %rcx
	addq	$16, %rdx
	addq	$-2, %r10
	jne	.LBB2_26
	jmp	.LBB2_75
.LBB2_31:                               # %.preheader121
	testl	%r10d, %r10d
	jle	.LBB2_35
# BB#32:                                # %.lr.ph135.preheader
	testb	$1, %r10b
	jne	.LBB2_36
# BB#33:
	xorl	%ebx, %ebx
	cmpl	$1, %r10d
	jne	.LBB2_39
	jmp	.LBB2_45
.LBB2_35:
	xorl	%edx, %edx
	jmp	.LBB2_62
.LBB2_36:                               # %.lr.ph135.prol
	cmpl	%r9d, (%rcx)
	jne	.LBB2_38
# BB#37:
	movsd	(%r11), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%r11)
.LBB2_38:                               # %.lr.ph135.prol.loopexit
	movl	$1, %ebx
	cmpl	$1, %r10d
	je	.LBB2_45
.LBB2_39:                               # %.lr.ph135.preheader.new
	movq	%r10, %rdx
	subq	%rbx, %rdx
	leaq	4(%rcx,%rbx,4), %rsi
	leaq	8(%r11,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB2_40:                               # %.lr.ph135
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, -4(%rsi)
	jne	.LBB2_42
# BB#41:                                #   in Loop: Header=BB2_40 Depth=1
	movsd	-8(%rbx), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, -8(%rbx)
.LBB2_42:                               # %.lr.ph135.1176
                                        #   in Loop: Header=BB2_40 Depth=1
	cmpl	%r9d, (%rsi)
	jne	.LBB2_44
# BB#43:                                #   in Loop: Header=BB2_40 Depth=1
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx)
.LBB2_44:                               #   in Loop: Header=BB2_40 Depth=1
	addq	$8, %rsi
	addq	$16, %rbx
	addq	$-2, %rdx
	jne	.LBB2_40
.LBB2_45:                               # %.preheader119
	testl	%r10d, %r10d
	setg	%dl
	jle	.LBB2_75
# BB#46:                                # %.lr.ph130.preheader
	movl	%edx, -4(%rsp)          # 4-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_47:                               # %.lr.ph130
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_54 Depth 2
	cmpl	%r9d, (%rcx,%rbp,4)
	jne	.LBB2_60
# BB#48:                                #   in Loop: Header=BB2_47 Depth=1
	movsd	(%r11,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	movslq	(%r15,%rbp,4), %rsi
	movslq	4(%r15,%rbp,4), %rbx
	cmpl	%ebx, %esi
	jge	.LBB2_59
# BB#49:                                # %.lr.ph128
                                        #   in Loop: Header=BB2_47 Depth=1
	movl	%ebx, %edx
	subl	%esi, %edx
	leaq	-1(%rbx), %r14
	testb	$1, %dl
	jne	.LBB2_51
# BB#50:                                #   in Loop: Header=BB2_47 Depth=1
	movq	%rsi, %rdx
	cmpq	%rsi, %r14
	jne	.LBB2_54
	jmp	.LBB2_59
.LBB2_51:                               #   in Loop: Header=BB2_47 Depth=1
	movslq	(%rdi,%rsi,4), %rdx
	cmpl	%r9d, (%r8,%rdx,4)
	jne	.LBB2_53
# BB#52:                                #   in Loop: Header=BB2_47 Depth=1
	movsd	(%r12,%rsi,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	(%r13,%rdx,8), %xmm2
	addsd	%xmm2, %xmm1
.LBB2_53:                               #   in Loop: Header=BB2_47 Depth=1
	leaq	1(%rsi), %rdx
	cmpq	%rsi, %r14
	je	.LBB2_59
	.p2align	4, 0x90
.LBB2_54:                               #   Parent Loop BB2_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rdi,%rdx,4), %rsi
	cmpl	%r9d, (%r8,%rsi,4)
	jne	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_54 Depth=2
	movsd	(%r12,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	(%r13,%rsi,8), %xmm2
	addsd	%xmm2, %xmm1
.LBB2_56:                               #   in Loop: Header=BB2_54 Depth=2
	movslq	4(%rdi,%rdx,4), %rsi
	cmpl	%r9d, (%r8,%rsi,4)
	jne	.LBB2_58
# BB#57:                                #   in Loop: Header=BB2_54 Depth=2
	movsd	8(%r12,%rdx,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	(%r13,%rsi,8), %xmm2
	addsd	%xmm2, %xmm1
.LBB2_58:                               #   in Loop: Header=BB2_54 Depth=2
	addq	$2, %rdx
	cmpq	%rbx, %rdx
	jl	.LBB2_54
.LBB2_59:                               # %._crit_edge
                                        #   in Loop: Header=BB2_47 Depth=1
	movsd	%xmm1, (%r11,%rbp,8)
.LBB2_60:                               #   in Loop: Header=BB2_47 Depth=1
	incq	%rbp
	cmpq	%r10, %rbp
	jne	.LBB2_47
# BB#61:
	movl	-4(%rsp), %edx          # 4-byte Reload
.LBB2_62:                               # %._crit_edge131
	ucomisd	.LCPI2_0(%rip), %xmm0
	jne	.LBB2_63
	jnp	.LBB2_75
.LBB2_63:                               # %._crit_edge131
	testb	%dl, %dl
	je	.LBB2_75
# BB#64:                                # %.lr.ph125.preheader
	testb	$1, %r10b
	jne	.LBB2_66
# BB#65:
	xorl	%edx, %edx
	cmpl	$1, %r10d
	jne	.LBB2_69
	jmp	.LBB2_75
.LBB2_66:                               # %.lr.ph125.prol
	cmpl	%r9d, (%rcx)
	jne	.LBB2_68
# BB#67:
	movsd	(%r11), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%r11)
.LBB2_68:                               # %.lr.ph125.prol.loopexit
	movl	$1, %edx
	cmpl	$1, %r10d
	je	.LBB2_75
.LBB2_69:                               # %.lr.ph125.preheader.new
	subq	%rdx, %r10
	leaq	4(%rcx,%rdx,4), %rcx
	leaq	8(%r11,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB2_70:                               # %.lr.ph125
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, -4(%rcx)
	jne	.LBB2_72
# BB#71:                                #   in Loop: Header=BB2_70 Depth=1
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%rdx)
.LBB2_72:                               # %.lr.ph125.1166
                                        #   in Loop: Header=BB2_70 Depth=1
	cmpl	%r9d, (%rcx)
	jne	.LBB2_74
# BB#73:                                #   in Loop: Header=BB2_70 Depth=1
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx)
.LBB2_74:                               #   in Loop: Header=BB2_70 Depth=1
	addq	$8, %rcx
	addq	$16, %rdx
	addq	$-2, %r10
	jne	.LBB2_70
.LBB2_75:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_CSRMatrixMatvec_FF, .Lfunc_end2-hypre_CSRMatrixMatvec_FF
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
