	.text
	.file	"compress.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI0_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI0_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	compress
	.p2align	4, 0x90
	.type	compress,@function
compress:                               # @compress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1160, %rsp             # imm = 0x488
.Lcfi6:
	.cfi_def_cfa_offset 1216
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	cmpl	$1, %edi
	jle	.LBB0_1
# BB#2:
	movq	8(%rsi), %rbx
	leaq	16(%rsp), %rdx
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	__xstat
	movl	64(%rsp), %eax
	movl	%eax, 12(%rsp)
	movl	$.L.str.1, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, fpi(%rip)
	testq	%rax, %rax
	je	.LBB0_3
# BB#5:
	leaq	160(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%rbp, %rdi
	callq	strlen
	movb	$0, 166(%rsp,%rax)
	movw	$29296, 164(%rsp,%rax)  # imm = 0x7270
	movl	$1836016430, 160(%rsp,%rax) # imm = 0x6D6F632E
	movl	$.L.str.4, %esi
	movq	%rbp, %rdi
	callq	fopen
	movq	%rax, fpo(%rip)
	testq	%rax, %rax
	je	.LBB0_6
# BB#8:
	leaq	12(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movl	12(%rsp), %ebx
	leal	(%rbx,%rbx), %ebp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, in(%rip)
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r15
	movq	%r15, bw(%rip)
	leaq	(,%rbx,4), %rdi
	callq	malloc
	movq	%rax, %r12
	movq	%r12, rot(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %r13
	movq	%r13, rle(%rip)
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, ari(%rip)
	testq	%rax, %rax
	je	.LBB0_13
# BB#9:
	testq	%r14, %r14
	je	.LBB0_13
# BB#10:
	testq	%r15, %r15
	je	.LBB0_13
# BB#11:
	testq	%r12, %r12
	je	.LBB0_13
# BB#12:
	testq	%r13, %r13
	je	.LBB0_13
# BB#14:
	movq	fpi(%rip), %rcx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fread
	cmpq	%rbx, %rax
	jne	.LBB0_64
# BB#15:
	movl	%ebx, size(%rip)
	movq	in(%rip), %rsi
	leaq	(%rsi,%rbx), %rdi
	movq	%rbx, %rdx
	callq	memcpy
	movl	$0, 8(%rsp)
	testl	%ebx, %ebx
	je	.LBB0_16
# BB#17:                                # %.lr.ph10.i
	movq	rot(%rip), %rdi
	cmpl	$8, %ebx
	jae	.LBB0_19
# BB#18:
	xorl	%eax, %eax
	jmp	.LBB0_24
.LBB0_16:                               # %._crit_edge18.i
	movq	rot(%rip), %rdi
	xorl	%ebx, %ebx
	jmp	.LBB0_26
.LBB0_19:                               # %min.iters.checked
	movl	%ebx, %ecx
	andl	$7, %ecx
	movq	%rbx, %rax
	subq	%rcx, %rax
	je	.LBB0_20
# BB#21:                                # %vector.body.preheader
	leaq	16(%rdi), %rdx
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [4,4,4,4]
	movdqa	.LCPI0_2(%rip), %xmm2   # xmm2 = [8,8,8,8]
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB0_22:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm3
	paddd	%xmm1, %xmm3
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm3, (%rdx)
	paddd	%xmm2, %xmm0
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB0_22
# BB#23:                                # %middle.block
	testl	%ecx, %ecx
	jne	.LBB0_24
	jmp	.LBB0_25
.LBB0_20:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_24:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, (%rdi,%rax,4)
	incq	%rax
	cmpq	%rax, %rbx
	jne	.LBB0_24
.LBB0_25:                               # %._crit_edge.i
	movl	%ebx, 8(%rsp)
.LBB0_26:
	movl	$4, %edx
	movl	$compare, %ecx
	movq	%rbx, %rsi
	callq	qsort
	movl	$0, 8(%rsp)
	movl	size(%rip), %ecx
	testq	%rcx, %rcx
	je	.LBB0_27
# BB#28:                                # %.lr.ph6.i
	movq	in(%rip), %rsi
	leal	-1(%rcx), %edi
	testb	$1, %cl
	jne	.LBB0_30
# BB#29:
	xorl	%ebp, %ebp
	cmpl	$1, %ecx
	jne	.LBB0_33
	jmp	.LBB0_32
.LBB0_27:                               # %.preheader.thread.i
	movl	$0, 8(%rsp)
	jmp	.LBB0_38
.LBB0_30:
	movq	rot(%rip), %rax
	movl	(%rax), %eax
	addl	%edi, %eax
	xorl	%edx, %edx
	divl	%ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movb	(%rsi,%rdx), %al
	movq	bw(%rip), %rdx
	movb	%al, (%rdx)
	movl	$1, %ebp
	cmpl	$1, %ecx
	je	.LBB0_32
	.p2align	4, 0x90
.LBB0_33:                               # =>This Inner Loop Header: Depth=1
	movq	rot(%rip), %rax
	movl	(%rax,%rbp,4), %eax
	addl	%edi, %eax
	xorl	%edx, %edx
	divl	%ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movzbl	(%rsi,%rdx), %eax
	movq	bw(%rip), %rdx
	movb	%al, (%rdx,%rbp)
	movq	rot(%rip), %rax
	movl	4(%rax,%rbp,4), %eax
	addl	%edi, %eax
	xorl	%edx, %edx
	divl	%ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movzbl	(%rsi,%rdx), %eax
	movq	bw(%rip), %rdx
	movb	%al, 1(%rdx,%rbp)
	addq	$2, %rbp
	cmpq	%rbp, %rcx
	jne	.LBB0_33
.LBB0_32:                               # %.lr.ph.i
	movl	$0, 8(%rsp)
	xorl	%eax, %eax
	movq	rot(%rip), %rdx
	.p2align	4, 0x90
.LBB0_35:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %esi
	cmpl	$0, (%rdx,%rsi,4)
	je	.LBB0_36
# BB#34:                                #   in Loop: Header=BB0_35 Depth=1
	incl	%eax
	cmpl	%ecx, %eax
	jb	.LBB0_35
# BB#37:                                # %..loopexit_crit_edge.i
	movl	%eax, 8(%rsp)
	jmp	.LBB0_38
.LBB0_36:
	movl	%eax, 8(%rsp)
	movq	fpo(%rip), %rcx
	leaq	8(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
.LBB0_38:                               # %do_bwe.exit
	movq	in(%rip), %rdi
	callq	free
	movq	rot(%rip), %rdi
	callq	free
	movl	size(%rip), %r13d
	testl	%r13d, %r13d
	je	.LBB0_39
# BB#40:                                # %.lr.ph59.i
	leal	(%r13,%r13), %r14d
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_41:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_42 Depth 2
                                        #     Child Loop BB0_58 Depth 2
	movq	bw(%rip), %rax
	movl	%ecx, %ebp
	movb	(%rax,%rbp), %cl
	xorl	%edx, %edx
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_42:                               #   Parent Loop BB0_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rbp,%rdx), %rdi
	cmpl	%r13d, %edi
	jae	.LBB0_43
# BB#44:                                #   in Loop: Header=BB0_42 Depth=2
	movl	%edi, %edi
	cmpb	(%rax,%rdi), %cl
	jne	.LBB0_53
# BB#45:                                #   in Loop: Header=BB0_42 Depth=2
	leal	1(%rsi), %ebx
	leaq	2(%rbp,%rdx), %rdi
	cmpl	%r13d, %edi
	jae	.LBB0_54
# BB#46:                                #   in Loop: Header=BB0_42 Depth=2
	movl	%edi, %edi
	cmpb	(%rax,%rdi), %cl
	jne	.LBB0_54
# BB#47:                                #   in Loop: Header=BB0_42 Depth=2
	leal	2(%rsi), %ebx
	leaq	3(%rbp,%rdx), %rdi
	cmpl	%r13d, %edi
	jae	.LBB0_54
# BB#48:                                #   in Loop: Header=BB0_42 Depth=2
	movl	%edi, %edi
	cmpb	(%rax,%rdi), %cl
	jne	.LBB0_54
# BB#49:                                #   in Loop: Header=BB0_42 Depth=2
	addq	$3, %rsi
	leaq	3(%rdx), %rbx
	addq	$4, %rdx
	cmpq	$126, %rdx
	movq	%rbx, %rdx
	jbe	.LBB0_42
# BB#50:                                # %.critedge.thread.loopexit.i
                                        #   in Loop: Header=BB0_41 Depth=1
	incl	%ebx
	jmp	.LBB0_51
.LBB0_43:                               #   in Loop: Header=BB0_41 Depth=1
	movl	%esi, %ebx
	cmpl	$1, %ebx
	je	.LBB0_55
	jmp	.LBB0_51
.LBB0_53:                               # %..critedge.i_crit_edge
                                        #   in Loop: Header=BB0_41 Depth=1
	incl	%edx
	movl	%edx, %ebx
	.p2align	4, 0x90
.LBB0_54:                               # %.critedge.i
                                        #   in Loop: Header=BB0_41 Depth=1
	cmpl	$1, %ebx
	jne	.LBB0_51
.LBB0_55:                               #   in Loop: Header=BB0_41 Depth=1
	leal	1(%rbp), %edx
	movl	$1, %ebx
	cmpl	%r13d, %edx
	jae	.LBB0_61
# BB#56:                                # %.preheader.i
                                        #   in Loop: Header=BB0_41 Depth=1
	movl	%edx, %edx
	movb	(%rax,%rdx), %dl
	cmpb	%dl, %cl
	je	.LBB0_61
# BB#57:                                # %.lr.ph.i18.preheader
                                        #   in Loop: Header=BB0_41 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_58:                               # %.lr.ph.i18
                                        #   Parent Loop BB0_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	2(%rbp,%rbx), %ecx
	cmpb	(%rax,%rcx), %dl
	je	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_58 Depth=2
	leal	2(%rbp,%rbx), %ecx
	leal	1(%rbp,%rbx), %esi
	movzbl	(%rax,%rcx), %edx
	incl	%ebx
	cmpb	%dl, (%rax,%rsi)
	jne	.LBB0_58
.LBB0_60:                               # %.lr.ph.i18..critedge1.i.loopexit_crit_edge
                                        #   in Loop: Header=BB0_41 Depth=1
	incl	%ebx
.LBB0_61:                               # %.critedge1.i
                                        #   in Loop: Header=BB0_41 Depth=1
	movl	%ebx, %eax
	andb	$127, %al
	movq	rle(%rip), %rcx
	leal	1(%r12), %edi
	movl	%r12d, %edx
	movb	%al, (%rcx,%rdx)
	addq	rle(%rip), %rdi
	movq	bw(%rip), %rsi
	addq	%rbp, %rsi
	movl	%ebx, %edx
	callq	memcpy
	leal	1(%rbx,%r12), %r15d
	jmp	.LBB0_62
	.p2align	4, 0x90
.LBB0_51:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB0_41 Depth=1
	leal	2(%r12), %r15d
	cmpl	%r14d, %r15d
	ja	.LBB0_65
# BB#52:                                #   in Loop: Header=BB0_41 Depth=1
	movl	%ebx, %eax
	orb	$-128, %al
	movq	rle(%rip), %rdx
	movl	%r12d, %esi
	movb	%al, (%rdx,%rsi)
	movq	rle(%rip), %rax
	incl	%r12d
	movb	%cl, (%rax,%r12)
.LBB0_62:                               # %.backedge.i
                                        #   in Loop: Header=BB0_41 Depth=1
	leal	(%rbp,%rbx), %ecx
	cmpl	%r13d, %ecx
	movl	%r15d, %r12d
	jb	.LBB0_41
	jmp	.LBB0_63
.LBB0_39:
	xorl	%r15d, %r15d
.LBB0_63:                               # %do_rle.exit
	movq	bw(%rip), %rdi
	callq	free
	movl	%r15d, %edi
	callq	do_ari
	movl	%eax, %ebx
	movq	rle(%rip), %rdi
	callq	free
	movq	ari(%rip), %rdi
	movl	%ebx, %edx
	movq	fpo(%rip), %rcx
	movl	$1, %esi
	callq	fwrite
	movq	ari(%rip), %rdi
	callq	free
	movq	fpi(%rip), %rdi
	callq	fclose
	movq	fpo(%rip), %rdi
	callq	fclose
	addq	$1160, %rsp             # imm = 0x488
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_65:
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_1:
	movq	stderr(%rip), %rdi
	movq	(%rsi), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	jmp	.LBB0_4
.LBB0_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
.LBB0_4:
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$67, %esi
	jmp	.LBB0_7
.LBB0_13:
	movq	stderr(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$21, %esi
.LBB0_7:
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB0_64:
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	compress, .Lfunc_end0-compress
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare,@function
compare:                                # @compare
	.cfi_startproc
# BB#0:
	movq	in(%rip), %rax
	movl	(%rdi), %edi
	addq	%rax, %rdi
	movl	(%rsi), %esi
	addq	%rax, %rsi
	movl	size(%rip), %edx
	jmp	memcmp                  # TAILCALL
.Lfunc_end1:
	.size	compare, .Lfunc_end1-compare
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"USAGE: %s <FILENAME>\n"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"r"
	.size	.L.str.1, 2

	.type	fpi,@object             # @fpi
	.comm	fpi,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"ERROR: Could not find infile %s\n"
	.size	.L.str.2, 33

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	".compr"
	.size	.L.str.3, 7

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"w"
	.size	.L.str.4, 2

	.type	fpo,@object             # @fpo
	.comm	fpo,8,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ERROR: Could not open outfile (do you have write permission here?)\n"
	.size	.L.str.5, 68

	.type	in,@object              # @in
	.local	in
	.comm	in,8,8
	.type	bw,@object              # @bw
	.comm	bw,8,8
	.type	rot,@object             # @rot
	.comm	rot,8,8
	.type	rle,@object             # @rle
	.comm	rle,8,8
	.type	ari,@object             # @ari
	.comm	ari,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ERROR: Out of memory\n"
	.size	.L.str.6, 22

	.type	size,@object            # @size
	.local	size
	.comm	size,4,4
	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"PANIC: RLE buf larger than %d bytes needed (repeat)\n"
	.size	.L.str.8, 53

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Something is fishy regarding the file size"
	.size	.Lstr, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
