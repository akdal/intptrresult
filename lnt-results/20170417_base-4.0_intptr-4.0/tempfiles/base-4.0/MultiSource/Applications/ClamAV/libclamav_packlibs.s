	.text
	.file	"libclamav_packlibs.bc"
	.globl	cli_unfsg
	.p2align	4, 0x90
	.type	cli_unfsg,@function
cli_unfsg:                              # @cli_unfsg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	testl	%edx, %edx
	movl	$-1, %eax
	jle	.LBB0_149
# BB#1:
	testl	%ecx, %ecx
	jle	.LBB0_149
# BB#2:
	movq	%r9, -16(%rsp)          # 8-byte Spill
	leaq	1(%rdi), %r10
	movb	(%rdi), %al
	leaq	1(%rsi), %r14
	movb	%al, (%rsi)
	movl	%edx, %eax
	leaq	-1(%rdi,%rax), %r11
	movslq	%ecx, %rbx
	addq	%rsi, %rbx
	movslq	%edx, %r9
	addq	%rdi, %r9
	movl	$1, %eax
	xorl	%edx, %edx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	movb	$-128, %r13b
	jmp	.LBB0_4
.LBB0_3:                                # %.outer.backedge
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	-32(%rsp), %rcx         # 8-byte Reload
	leaq	1(%r14,%rcx), %r14
	movq	-24(%rsp), %r8          # 8-byte Reload
	movl	-84(%rsp), %ecx         # 4-byte Reload
	movq	-80(%rsp), %rsi         # 8-byte Reload
	movq	-72(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_4:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_29 Depth 2
                                        #     Child Loop BB0_36 Depth 2
                                        #     Child Loop BB0_49 Depth 2
                                        #     Child Loop BB0_85 Depth 2
                                        #     Child Loop BB0_83 Depth 2
                                        #     Child Loop BB0_99 Depth 2
                                        #     Child Loop BB0_104 Depth 2
                                        #     Child Loop BB0_107 Depth 2
	xorl	%r15d, %r15d
	movl	%r13d, %edx
	movq	%r14, -8(%rsp)          # 8-byte Spill
	testb	$127, %dl
	je	.LBB0_10
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_4 Depth=1
	movl	%edx, %r13d
	addb	%r13b, %r13b
	testb	%dl, %dl
	jns	.LBB0_13
	jmp	.LBB0_18
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_4 Depth=1
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#11:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#12:                                #   in Loop: Header=BB0_4 Depth=1
	movb	(%r10), %dl
	movl	%edx, %r13d
	addb	%r13b, %r13b
	orb	$1, %r13b
	incq	%r10
	testb	%dl, %dl
	js	.LBB0_18
.LBB0_13:                               #   in Loop: Header=BB0_4 Depth=1
	cmpq	%rsi, %r14
	jb	.LBB0_148
# BB#14:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#15:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	%rbx, %r14
	jae	.LBB0_148
# BB#16:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	%r9, %r10
	jae	.LBB0_148
# BB#17:                                #   in Loop: Header=BB0_4 Depth=1
	movb	(%r10), %al
	incq	%r10
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_4 Depth=1
	testb	$127, %r13b
	je	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_4 Depth=1
	movl	%r13d, %ebp
	addb	%bpl, %bpl
	testb	%r13b, %r13b
	js	.LBB0_23
	jmp	.LBB0_35
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_4 Depth=1
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#21:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#22:                                #   in Loop: Header=BB0_4 Depth=1
	movb	(%r10), %r13b
	movl	%r13d, %ebp
	addb	%bpl, %bpl
	orb	$1, %bpl
	incq	%r10
	testb	%r13b, %r13b
	jns	.LBB0_35
.LBB0_23:                               #   in Loop: Header=BB0_4 Depth=1
	testb	$127, %bpl
	je	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_4 Depth=1
	movl	%ebp, %edx
	addb	%dl, %dl
	testb	%bpl, %bpl
	js	.LBB0_28
	jmp	.LBB0_60
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_4 Depth=1
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#26:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#27:                                #   in Loop: Header=BB0_4 Depth=1
	movb	(%r10), %bpl
	movl	%ebp, %edx
	addb	%dl, %dl
	orb	$1, %dl
	incq	%r10
	testb	%bpl, %bpl
	jns	.LBB0_60
.LBB0_28:                               # %.preheader241.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$16, %r12d
	.p2align	4, 0x90
.LBB0_29:                               # %.preheader241
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$127, %dl
	je	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_29 Depth=2
	movl	%edx, %r13d
	addb	%r13b, %r13b
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_29 Depth=2
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#32:                                #   in Loop: Header=BB0_29 Depth=2
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#33:                                #   in Loop: Header=BB0_29 Depth=2
	movzbl	(%r10), %edx
	movl	%edx, %r13d
	addb	%r13b, %r13b
	orb	$1, %r13b
	incq	%r10
.LBB0_34:                               # %doubledl.exit177
                                        #   in Loop: Header=BB0_29 Depth=2
	movzbl	%dl, %eax
	shrl	$7, %eax
	leal	(%rax,%r12,2), %r12d
	cmpl	$256, %r12d             # imm = 0x100
	movl	%r13d, %edx
	jb	.LBB0_29
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	andl	$255, %r12d
	jne	.LBB0_63
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=1
	cmpq	%rbx, %r14
	jae	.LBB0_148
# BB#7:                                 #   in Loop: Header=BB0_4 Depth=1
	xorl	%eax, %eax
.LBB0_8:                                # %.backedge
                                        #   in Loop: Header=BB0_4 Depth=1
	movb	%al, (%r14)
	incq	%r14
	incq	%r15
	movl	$1, %eax
	movl	%r13d, %edx
	testb	$127, %dl
	jne	.LBB0_9
	jmp	.LBB0_10
.LBB0_35:                               # %.preheader239.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, -72(%rsp)         # 8-byte Spill
	movl	%ecx, -84(%rsp)         # 4-byte Spill
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_36:                               # %.preheader239
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$127, %bpl
	je	.LBB0_38
# BB#37:                                #   in Loop: Header=BB0_36 Depth=2
	movl	%ebp, %edx
	addb	%dl, %dl
	movl	%ebp, %ecx
	testb	$127, %dl
	jne	.LBB0_41
	jmp	.LBB0_42
	.p2align	4, 0x90
.LBB0_38:                               #   in Loop: Header=BB0_36 Depth=2
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#39:                                #   in Loop: Header=BB0_36 Depth=2
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#40:                                #   in Loop: Header=BB0_36 Depth=2
	movzbl	(%r10), %ecx
	movl	%ecx, %edx
	addb	%dl, %dl
	orb	$1, %dl
	incq	%r10
	testb	$127, %dl
	je	.LBB0_42
.LBB0_41:                               #   in Loop: Header=BB0_36 Depth=2
	movl	%edx, %ebp
	addb	%bpl, %bpl
	jmp	.LBB0_45
	.p2align	4, 0x90
.LBB0_42:                               #   in Loop: Header=BB0_36 Depth=2
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#43:                                #   in Loop: Header=BB0_36 Depth=2
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#44:                                #   in Loop: Header=BB0_36 Depth=2
	movzbl	(%r10), %edx
	movl	%edx, %ebp
	addb	%bpl, %bpl
	orb	$1, %bpl
	incq	%r10
.LBB0_45:                               # %doubledl.exit169
                                        #   in Loop: Header=BB0_36 Depth=2
	movzbl	%cl, %ecx
	shrl	$7, %ecx
	leal	(%rcx,%rbx,2), %ebx
	testb	%dl, %dl
	js	.LBB0_36
# BB#46:                                #   in Loop: Header=BB0_4 Depth=1
	notl	%eax
	addl	%eax, %ebx
	je	.LBB0_84
# BB#47:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	%r9, %r10
	jae	.LBB0_148
# BB#48:                                #   in Loop: Header=BB0_4 Depth=1
	movzbl	(%r10), %ecx
	shll	$8, %ebx
	leal	-256(%rcx,%rbx), %r12d
	incq	%r10
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_49:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$127, %bpl
	je	.LBB0_51
# BB#50:                                #   in Loop: Header=BB0_49 Depth=2
	movl	%ebp, %edx
	addb	%dl, %dl
	movl	%ebp, %ecx
	testb	$127, %dl
	jne	.LBB0_54
	jmp	.LBB0_55
.LBB0_51:                               #   in Loop: Header=BB0_49 Depth=2
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#52:                                #   in Loop: Header=BB0_49 Depth=2
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#53:                                #   in Loop: Header=BB0_49 Depth=2
	movzbl	(%r10), %ecx
	movl	%ecx, %edx
	addb	%dl, %dl
	orb	$1, %dl
	incq	%r10
	testb	$127, %dl
	je	.LBB0_55
.LBB0_54:                               #   in Loop: Header=BB0_49 Depth=2
	movl	%edx, %ebp
	addb	%bpl, %bpl
	jmp	.LBB0_58
.LBB0_55:                               #   in Loop: Header=BB0_49 Depth=2
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#56:                                #   in Loop: Header=BB0_49 Depth=2
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#57:                                #   in Loop: Header=BB0_49 Depth=2
	movzbl	(%r10), %edx
	movl	%edx, %ebp
	addb	%bpl, %bpl
	orb	$1, %bpl
	incq	%r10
.LBB0_58:                               # %doubledl.exit153
                                        #   in Loop: Header=BB0_49 Depth=2
	movzbl	%cl, %ecx
	shrl	$7, %ecx
	leal	(%rcx,%rbx,2), %ebx
	testb	%dl, %dl
	js	.LBB0_49
# BB#59:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	cmpl	$31999, %r12d           # imm = 0x7CFF
	seta	%cl
	xorl	%edx, %edx
	cmpl	$1279, %r12d            # imm = 0x4FF
	seta	%dl
	addl	%ecx, %edx
	leal	(%rdx,%rbx), %ecx
	cmpl	$128, %r12d
	leal	2(%rbx,%rdx), %esi
	cmovael	%ecx, %esi
	xorl	%eax, %eax
	movl	%ebp, %r13d
	movl	%r12d, %ecx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movl	-84(%rsp), %ecx         # 4-byte Reload
	jmp	.LBB0_64
.LBB0_60:                               #   in Loop: Header=BB0_4 Depth=1
	cmpq	%r9, %r10
	jae	.LBB0_148
# BB#61:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	movzbl	(%r10), %esi
	movl	%esi, %r12d
	shrl	%r12d
	incq	%r10
	testl	%r12d, %r12d
	je	.LBB0_133
# BB#62:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, -72(%rsp)         # 8-byte Spill
	andl	$1, %esi
	orl	$2, %esi
	xorl	%eax, %eax
	movl	%edx, %r13d
	movl	%r12d, %edx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	jmp	.LBB0_64
.LBB0_63:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%rbx, -72(%rsp)         # 8-byte Spill
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	movl	$1, %esi
	movl	$1, %eax
.LBB0_64:                               # %.loopexit237
                                        #   in Loop: Header=BB0_4 Depth=1
	testl	%ecx, %ecx
	setle	%bl
	testl	%esi, %esi
	sete	%dl
	cmpq	-80(%rsp), %r14         # 8-byte Folded Reload
	jb	.LBB0_148
# BB#65:                                # %.loopexit237
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	%ecx, %esi
	ja	.LBB0_148
# BB#66:                                # %.loopexit237
                                        #   in Loop: Header=BB0_4 Depth=1
	orb	%dl, %bl
	jne	.LBB0_148
# BB#67:                                #   in Loop: Header=BB0_4 Depth=1
	movl	%ecx, -84(%rsp)         # 4-byte Spill
	movq	%r8, -24(%rsp)          # 8-byte Spill
	movl	%esi, %ebx
	leaq	(%r14,%rbx), %rcx
	cmpq	-72(%rsp), %rcx         # 8-byte Folded Reload
	ja	.LBB0_148
# BB#68:                                #   in Loop: Header=BB0_4 Depth=1
	testl	%esi, %esi
	je	.LBB0_148
# BB#69:                                #   in Loop: Header=BB0_4 Depth=1
	cmpl	$0, -84(%rsp)           # 4-byte Folded Reload
	jle	.LBB0_148
# BB#70:                                #   in Loop: Header=BB0_4 Depth=1
	movq	-80(%rsp), %rdx         # 8-byte Reload
	cmpq	%rdx, %rcx
	jbe	.LBB0_148
# BB#71:                                #   in Loop: Header=BB0_4 Depth=1
	movl	%r12d, %r12d
	movq	%r14, %rcx
	subq	%r12, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB0_148
# BB#72:                                #   in Loop: Header=BB0_4 Depth=1
	addq	%rcx, %rbx
	cmpq	-72(%rsp), %rbx         # 8-byte Folded Reload
	ja	.LBB0_148
# BB#73:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	-80(%rsp), %rbx         # 8-byte Folded Reload
	jbe	.LBB0_148
# BB#74:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%r12, %rbx
	negq	%rbx
	leal	-1(%rsi), %ebp
	leaq	1(%rbp), %r8
	cmpq	$32, %r8
	movq	%rbp, -32(%rsp)         # 8-byte Spill
	jae	.LBB0_76
# BB#75:                                #   in Loop: Header=BB0_4 Depth=1
	movq	%r14, %rdx
	jmp	.LBB0_102
.LBB0_76:                               # %min.iters.checked
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%r8, -40(%rsp)          # 8-byte Spill
	movabsq	$8589934560, %rdx       # imm = 0x1FFFFFFE0
	andq	%rdx, %r8
	je	.LBB0_80
# BB#77:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rbp, %rdx
	subq	%r12, %rdx
	leaq	1(%r14,%rdx), %rdx
	cmpq	%rdx, %r14
	jae	.LBB0_81
# BB#78:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_4 Depth=1
	leaq	1(%r14,%rbp), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB0_81
.LBB0_80:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%r14, %rdx
.LBB0_102:                              # %.lr.ph.preheader389
                                        #   in Loop: Header=BB0_4 Depth=1
	leal	-1(%rsi), %r8d
	movl	%esi, %ebp
	andl	$7, %ebp
	je	.LBB0_105
# BB#103:                               # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB0_104:                              # %.lr.ph.prol
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%esi
	movzbl	(%rbx,%rdx), %ecx
	movb	%cl, (%rdx)
	incq	%rdx
	incl	%ebp
	jne	.LBB0_104
.LBB0_105:                              # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpl	$7, %r8d
	movl	$-1, %ebp
	jb	.LBB0_3
# BB#106:                               # %.lr.ph.preheader389.new
                                        #   in Loop: Header=BB0_4 Depth=1
	addq	$7, %rdx
	.p2align	4, 0x90
.LBB0_107:                              # %.lr.ph
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rbx,%rdx), %ecx
	movb	%cl, -7(%rdx)
	movzbl	-6(%rbx,%rdx), %ecx
	movb	%cl, -6(%rdx)
	movzbl	-5(%rbx,%rdx), %ecx
	movb	%cl, -5(%rdx)
	movzbl	-4(%rbx,%rdx), %ecx
	movb	%cl, -4(%rdx)
	movzbl	-3(%rbx,%rdx), %ecx
	movb	%cl, -3(%rdx)
	movzbl	-2(%rbx,%rdx), %ecx
	movb	%cl, -2(%rdx)
	movzbl	-1(%rbx,%rdx), %ecx
	movb	%cl, -1(%rdx)
	addl	$-8, %esi
	movzbl	(%rbx,%rdx), %ecx
	movb	%cl, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB0_107
	jmp	.LBB0_3
.LBB0_81:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%r8, -48(%rsp)          # 8-byte Spill
	leaq	-32(%r8), %rcx
	movq	%rcx, -56(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$5, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB0_96
# BB#82:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$16, %r8d
	subq	%r12, %r8
	negq	%rcx
	xorl	%edx, %edx
.LBB0_83:                               # %vector.body.prol
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r8,%rdx), %rbp
	movups	-16(%r14,%rbp), %xmm0
	movups	(%r14,%rbp), %xmm1
	movups	%xmm0, (%r14,%rdx)
	movups	%xmm1, 16(%r14,%rdx)
	addq	$32, %rdx
	incq	%rcx
	jne	.LBB0_83
	jmp	.LBB0_97
.LBB0_84:                               # %.preheader236.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	movl	$1, %esi
	movl	-84(%rsp), %ecx         # 4-byte Reload
.LBB0_85:                               # %.preheader236
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$127, %bpl
	je	.LBB0_87
# BB#86:                                #   in Loop: Header=BB0_85 Depth=2
	movl	%ebp, %edx
	addb	%dl, %dl
	movl	%ebp, %eax
	testb	$127, %dl
	jne	.LBB0_90
	jmp	.LBB0_91
.LBB0_87:                               #   in Loop: Header=BB0_85 Depth=2
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#88:                                #   in Loop: Header=BB0_85 Depth=2
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#89:                                #   in Loop: Header=BB0_85 Depth=2
	movzbl	(%r10), %eax
	movl	%eax, %edx
	addb	%dl, %dl
	orb	$1, %dl
	incq	%r10
	testb	$127, %dl
	je	.LBB0_91
.LBB0_90:                               #   in Loop: Header=BB0_85 Depth=2
	movl	%edx, %ebp
	addb	%bpl, %bpl
	jmp	.LBB0_94
.LBB0_91:                               #   in Loop: Header=BB0_85 Depth=2
	cmpq	%rdi, %r10
	jb	.LBB0_148
# BB#92:                                #   in Loop: Header=BB0_85 Depth=2
	cmpq	%r11, %r10
	jae	.LBB0_148
# BB#93:                                #   in Loop: Header=BB0_85 Depth=2
	movzbl	(%r10), %edx
	movl	%edx, %ebp
	addb	%bpl, %bpl
	orb	$1, %bpl
	incq	%r10
.LBB0_94:                               # %doubledl.exit161
                                        #   in Loop: Header=BB0_85 Depth=2
	movzbl	%al, %eax
	shrl	$7, %eax
	leal	(%rax,%rsi,2), %esi
	testb	%dl, %dl
	js	.LBB0_85
# BB#95:                                #   in Loop: Header=BB0_4 Depth=1
	xorl	%eax, %eax
	movl	%ebp, %r13d
	movq	-64(%rsp), %r12         # 8-byte Reload
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	jmp	.LBB0_64
.LBB0_96:                               #   in Loop: Header=BB0_4 Depth=1
	xorl	%edx, %edx
.LBB0_97:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpq	$96, -56(%rsp)          # 8-byte Folded Reload
	movq	-48(%rsp), %r8          # 8-byte Reload
	jb	.LBB0_100
# BB#98:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%r8, %rcx
	subq	%rdx, %rcx
	movq	-8(%rsp), %rbp          # 8-byte Reload
	addq	%rdx, %rbp
	leaq	112(%r15,%rbp), %rdx
.LBB0_99:                               # %vector.body
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx,%rdx), %xmm0
	movups	-96(%rbx,%rdx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rbx,%rdx), %xmm0
	movups	-64(%rbx,%rdx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rbx,%rdx), %xmm0
	movups	-32(%rbx,%rdx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rbx,%rdx), %xmm0
	movups	(%rbx,%rdx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	addq	$-128, %rcx
	jne	.LBB0_99
.LBB0_100:                              # %middle.block
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpq	%r8, -40(%rsp)          # 8-byte Folded Reload
	movl	$-1, %ecx
	je	.LBB0_3
# BB#101:                               #   in Loop: Header=BB0_4 Depth=1
	movq	%r8, %rdx
	addq	%r14, %rdx
	subl	%r8d, %esi
	jmp	.LBB0_102
.LBB0_148:
	movl	$-1, %eax
.LBB0_149:                              # %doubledl.exit.thread
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_133:
	testq	%r8, %r8
	je	.LBB0_135
# BB#134:
	movq	%r10, (%r8)
.LBB0_135:
	xorl	%eax, %eax
	movq	-16(%rsp), %rcx         # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB0_149
# BB#136:
	movq	%r14, (%rcx)
	jmp	.LBB0_149
.Lfunc_end0:
	.size	cli_unfsg, .Lfunc_end0-cli_unfsg
	.cfi_endproc

	.globl	unmew
	.p2align	4, 0x90
	.type	unmew,@function
unmew:                                  # @unmew
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 144
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r8, 56(%rsp)           # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, %r11
	movq	%rdi, %r12
	leaq	1(%r12), %r9
	movb	(%r12), %al
	leaq	1(%r11), %rsi
	movb	%al, (%r11)
	movl	%edx, %eax
	leaq	-1(%r12,%rax), %rbp
	movq	%rcx, (%rsp)            # 8-byte Spill
	movslq	%ecx, %r10
	movq	%rsi, %rcx
	addq	%r11, %r10
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movslq	%edx, %rdi
	addq	%r12, %rdi
	movl	$1, %r13d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movb	$-128, %r14b
	jmp	.LBB1_2
.LBB1_1:                                # %.outer.backedge
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	1(%rcx,%rbx), %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_2:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_27 Depth 2
                                        #     Child Loop BB1_34 Depth 2
                                        #     Child Loop BB1_47 Depth 2
                                        #     Child Loop BB1_82 Depth 2
                                        #     Child Loop BB1_80 Depth 2
                                        #     Child Loop BB1_96 Depth 2
                                        #     Child Loop BB1_101 Depth 2
                                        #     Child Loop BB1_104 Depth 2
	xorl	%r15d, %r15d
	movl	%r14d, %eax
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	testb	$127, %al
	je	.LBB1_8
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	movl	%eax, %r14d
	addb	%r14b, %r14b
	testb	%al, %al
	jns	.LBB1_11
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$-1, %eax
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#9:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#10:                                #   in Loop: Header=BB1_2 Depth=1
	movb	(%r9), %al
	movl	%eax, %r14d
	addb	%r14b, %r14b
	orb	$1, %r14b
	incq	%r9
	testb	%al, %al
	js	.LBB1_16
.LBB1_11:                               #   in Loop: Header=BB1_2 Depth=1
	cmpq	%r11, %rcx
	jb	.LBB1_105
# BB#12:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	%r12, %r9
	jb	.LBB1_105
# BB#13:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	%r10, %rcx
	jae	.LBB1_105
# BB#14:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	%rdi, %r9
	jae	.LBB1_105
# BB#15:                                #   in Loop: Header=BB1_2 Depth=1
	movb	(%r9), %al
	incq	%r9
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_16:                               #   in Loop: Header=BB1_2 Depth=1
	testb	$127, %r14b
	je	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_2 Depth=1
	movl	%r14d, %esi
	addb	%sil, %sil
	testb	%r14b, %r14b
	js	.LBB1_21
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_2 Depth=1
	movl	$-1, %eax
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#19:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#20:                                #   in Loop: Header=BB1_2 Depth=1
	movb	(%r9), %r14b
	movl	%r14d, %esi
	addb	%sil, %sil
	orb	$1, %sil
	incq	%r9
	testb	%r14b, %r14b
	jns	.LBB1_33
.LBB1_21:                               #   in Loop: Header=BB1_2 Depth=1
	testb	$127, %sil
	je	.LBB1_23
# BB#22:                                #   in Loop: Header=BB1_2 Depth=1
	movl	%esi, %eax
	addb	%al, %al
	testb	%sil, %sil
	js	.LBB1_26
	jmp	.LBB1_58
	.p2align	4, 0x90
.LBB1_23:                               #   in Loop: Header=BB1_2 Depth=1
	movl	$-1, %eax
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#24:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#25:                                #   in Loop: Header=BB1_2 Depth=1
	movb	(%r9), %sil
	movl	%esi, %eax
	addb	%al, %al
	orb	$1, %al
	incq	%r9
	testb	%sil, %sil
	jns	.LBB1_58
.LBB1_26:                               # %.preheader297.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$16, %ebx
	.p2align	4, 0x90
.LBB1_27:                               # %.preheader297
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$127, %al
	je	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_27 Depth=2
	movl	%eax, %r14d
	addb	%r14b, %r14b
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_29:                               #   in Loop: Header=BB1_27 Depth=2
	movl	$-1, %eax
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#30:                                #   in Loop: Header=BB1_27 Depth=2
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#31:                                #   in Loop: Header=BB1_27 Depth=2
	movzbl	(%r9), %eax
	movl	%eax, %r14d
	addb	%r14b, %r14b
	orb	$1, %r14b
	incq	%r9
.LBB1_32:                               # %doubledl.exit230
                                        #   in Loop: Header=BB1_27 Depth=2
	movzbl	%al, %eax
	shrl	$7, %eax
	leal	(%rax,%rbx,2), %ebx
	cmpl	$256, %ebx              # imm = 0x100
	movl	%r14d, %eax
	jb	.LBB1_27
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	andl	$255, %ebx
	jne	.LBB1_62
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpq	%r10, %rcx
	jae	.LBB1_106
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
.LBB1_6:                                # %.backedge
                                        #   in Loop: Header=BB1_2 Depth=1
	movb	%al, (%rcx)
	incq	%rcx
	incq	%r15
	movl	$1, %r13d
	movl	%r14d, %eax
	testb	$127, %al
	jne	.LBB1_7
	jmp	.LBB1_8
.LBB1_33:                               # %.preheader295.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_34:                               # %.preheader295
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$127, %sil
	je	.LBB1_36
# BB#35:                                #   in Loop: Header=BB1_34 Depth=2
	movl	%esi, %eax
	addb	%al, %al
	movl	%esi, %edx
	testb	$127, %al
	jne	.LBB1_39
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_36:                               #   in Loop: Header=BB1_34 Depth=2
	movl	$-1, %eax
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#37:                                #   in Loop: Header=BB1_34 Depth=2
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#38:                                #   in Loop: Header=BB1_34 Depth=2
	movzbl	(%r9), %edx
	movl	%edx, %eax
	addb	%al, %al
	orb	$1, %al
	incq	%r9
	testb	$127, %al
	je	.LBB1_40
.LBB1_39:                               #   in Loop: Header=BB1_34 Depth=2
	movl	%eax, %esi
	addb	%sil, %sil
	jmp	.LBB1_43
	.p2align	4, 0x90
.LBB1_40:                               #   in Loop: Header=BB1_34 Depth=2
	movl	$-1, %eax
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#41:                                #   in Loop: Header=BB1_34 Depth=2
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#42:                                #   in Loop: Header=BB1_34 Depth=2
	movzbl	(%r9), %eax
	movl	%eax, %esi
	addb	%sil, %sil
	orb	$1, %sil
	incq	%r9
.LBB1_43:                               # %doubledl.exit222
                                        #   in Loop: Header=BB1_34 Depth=2
	movzbl	%dl, %edx
	shrl	$7, %edx
	leal	(%rdx,%rbx,2), %ebx
	testb	%al, %al
	js	.LBB1_34
# BB#44:                                #   in Loop: Header=BB1_2 Depth=1
	notl	%r13d
	addl	%r13d, %ebx
	je	.LBB1_81
# BB#45:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$-1, %eax
	cmpq	%rdi, %r9
	jae	.LBB1_107
# BB#46:                                #   in Loop: Header=BB1_2 Depth=1
	movzbl	(%r9), %edx
	shll	$8, %ebx
	leal	-256(%rdx,%rbx), %ebx
	incq	%r9
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB1_47:                               #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$127, %sil
	je	.LBB1_49
# BB#48:                                #   in Loop: Header=BB1_47 Depth=2
	movl	%esi, %edx
	addb	%dl, %dl
	movl	%esi, %r8d
	testb	$127, %dl
	jne	.LBB1_52
	jmp	.LBB1_53
.LBB1_49:                               #   in Loop: Header=BB1_47 Depth=2
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#50:                                #   in Loop: Header=BB1_47 Depth=2
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#51:                                #   in Loop: Header=BB1_47 Depth=2
	movzbl	(%r9), %r8d
	movl	%r8d, %edx
	addb	%dl, %dl
	orb	$1, %dl
	incq	%r9
	testb	$127, %dl
	je	.LBB1_53
.LBB1_52:                               #   in Loop: Header=BB1_47 Depth=2
	movq	%rdi, %r13
	movl	%edx, %esi
	addb	%sil, %sil
	jmp	.LBB1_56
.LBB1_53:                               #   in Loop: Header=BB1_47 Depth=2
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#54:                                #   in Loop: Header=BB1_47 Depth=2
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#55:                                #   in Loop: Header=BB1_47 Depth=2
	movq	%rdi, %r13
	movzbl	(%r9), %edx
	movl	%edx, %esi
	addb	%sil, %sil
	orb	$1, %sil
	incq	%r9
.LBB1_56:                               # %doubledl.exit206
                                        #   in Loop: Header=BB1_47 Depth=2
	movzbl	%r8b, %edi
	shrl	$7, %edi
	leal	(%rdi,%r14,2), %r14d
	testb	%dl, %dl
	movq	%r13, %rdi
	js	.LBB1_47
# BB#57:                                #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	cmpl	$31999, %ebx            # imm = 0x7CFF
	seta	%al
	xorl	%edx, %edx
	cmpl	$1279, %ebx             # imm = 0x4FF
	seta	%dl
	addl	%eax, %edx
	leal	(%rdx,%r14), %eax
	cmpl	$128, %ebx
	leal	2(%r14,%rdx), %r8d
	cmovael	%eax, %r8d
	xorl	%r13d, %r13d
	movl	%esi, %r14d
	jmp	.LBB1_61
.LBB1_58:                               #   in Loop: Header=BB1_2 Depth=1
	cmpq	%rdi, %r9
	jae	.LBB1_106
# BB#59:                                #   in Loop: Header=BB1_2 Depth=1
	movzbl	(%r9), %r8d
	movl	%r8d, %ebx
	shrl	%ebx
	incq	%r9
	testl	%ebx, %ebx
	je	.LBB1_115
# BB#60:                                #   in Loop: Header=BB1_2 Depth=1
	andl	$1, %r8d
	orl	$2, %r8d
	xorl	%r13d, %r13d
	movl	%eax, %r14d
.LBB1_61:                               # %.loopexit293
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	%ebx, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB1_63
.LBB1_62:                               #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %r8d
	movl	$1, %r13d
.LBB1_63:                               # %.loopexit293
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	(%rsp), %rsi            # 8-byte Reload
	testl	%esi, %esi
	setle	%al
	testl	%r8d, %r8d
	sete	%dl
	orb	%al, %dl
	cmpl	%r8d, %esi
	setb	%al
	orb	%dl, %al
	xorl	%edx, %edx
	cmpq	%r11, %rcx
	jb	.LBB1_110
# BB#64:                                # %.loopexit293
                                        #   in Loop: Header=BB1_2 Depth=1
	testb	%al, %al
	jne	.LBB1_110
# BB#65:                                #   in Loop: Header=BB1_2 Depth=1
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movl	%r8d, %edi
	leaq	(%rcx,%rdi), %rsi
	cmpq	%r10, %rsi
	ja	.LBB1_109
# BB#66:                                #   in Loop: Header=BB1_2 Depth=1
	testl	%r8d, %r8d
	je	.LBB1_109
# BB#67:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB1_109
# BB#68:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	%r11, %rsi
	jbe	.LBB1_109
# BB#69:                                #   in Loop: Header=BB1_2 Depth=1
	movl	%ebx, %eax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	cmpq	%r11, %rdx
	jb	.LBB1_109
# BB#70:                                #   in Loop: Header=BB1_2 Depth=1
	addq	%rdx, %rdi
	cmpq	%r10, %rdi
	ja	.LBB1_109
# BB#71:                                #   in Loop: Header=BB1_2 Depth=1
	cmpq	%r11, %rdi
	jbe	.LBB1_109
# BB#72:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, %rsi
	negq	%rsi
	leal	-1(%r8), %ebx
	leaq	1(%rbx), %rdi
	cmpq	$31, %rdi
	jbe	.LBB1_76
# BB#73:                                # %min.iters.checked
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movabsq	$8589934560, %rdi       # imm = 0x1FFFFFFE0
	andq	%rdi, %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	je	.LBB1_77
# BB#74:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	subq	%rax, %rdi
	leaq	1(%rcx,%rdi), %rdi
	cmpq	%rdi, %rcx
	jae	.LBB1_78
# BB#75:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	1(%rcx,%rbx), %rdi
	cmpq	%rdi, %rdx
	jae	.LBB1_78
.LBB1_76:                               #   in Loop: Header=BB1_2 Depth=1
	movq	%rcx, %rdi
	jmp	.LBB1_99
.LBB1_77:                               #   in Loop: Header=BB1_2 Depth=1
	movq	%rcx, %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB1_99
.LBB1_78:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	-32(%rdx), %rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_93
# BB#79:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$16, %ebx
	subq	%rax, %rbx
	negq	%rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_80:                               # %vector.body.prol
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rbx,%rdx), %rax
	movups	-16(%rcx,%rax), %xmm0
	movups	(%rcx,%rax), %xmm1
	movups	%xmm0, (%rcx,%rdx)
	movups	%xmm1, 16(%rcx,%rdx)
	addq	$32, %rdx
	incq	%rdi
	jne	.LBB1_80
	jmp	.LBB1_94
.LBB1_81:                               # %.preheader292.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB1_82:                               # %.preheader292
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$127, %sil
	je	.LBB1_84
# BB#83:                                #   in Loop: Header=BB1_82 Depth=2
	movl	%esi, %eax
	addb	%al, %al
	movl	%esi, %edx
	testb	$127, %al
	jne	.LBB1_87
	jmp	.LBB1_88
.LBB1_84:                               #   in Loop: Header=BB1_82 Depth=2
	movl	$-1, %eax
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#85:                                #   in Loop: Header=BB1_82 Depth=2
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#86:                                #   in Loop: Header=BB1_82 Depth=2
	movzbl	(%r9), %edx
	movl	%edx, %eax
	addb	%al, %al
	orb	$1, %al
	incq	%r9
	testb	$127, %al
	je	.LBB1_88
.LBB1_87:                               #   in Loop: Header=BB1_82 Depth=2
	movl	%eax, %esi
	addb	%sil, %sil
	jmp	.LBB1_91
.LBB1_88:                               #   in Loop: Header=BB1_82 Depth=2
	movl	$-1, %eax
	cmpq	%r12, %r9
	jb	.LBB1_107
# BB#89:                                #   in Loop: Header=BB1_82 Depth=2
	cmpq	%rbp, %r9
	jae	.LBB1_107
# BB#90:                                #   in Loop: Header=BB1_82 Depth=2
	movzbl	(%r9), %eax
	movl	%eax, %esi
	addb	%sil, %sil
	orb	$1, %sil
	incq	%r9
.LBB1_91:                               # %doubledl.exit214
                                        #   in Loop: Header=BB1_82 Depth=2
	movzbl	%dl, %edx
	shrl	$7, %edx
	leal	(%rdx,%r8,2), %r8d
	testb	%al, %al
	js	.LBB1_82
# BB#92:                                #   in Loop: Header=BB1_2 Depth=1
	xorl	%r13d, %r13d
	movl	%esi, %r14d
	movq	24(%rsp), %rbx          # 8-byte Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	jmp	.LBB1_63
.LBB1_93:                               #   in Loop: Header=BB1_2 Depth=1
	xorl	%edx, %edx
.LBB1_94:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpq	$96, 32(%rsp)           # 8-byte Folded Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jb	.LBB1_97
# BB#95:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	subq	%rdx, %rax
	movq	72(%rsp), %rdi          # 8-byte Reload
	addq	%rdx, %rdi
	leaq	112(%r15,%rdi), %rdx
	.p2align	4, 0x90
.LBB1_96:                               # %vector.body
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi,%rdx), %xmm0
	movups	-96(%rsi,%rdx), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi,%rdx), %xmm0
	movups	-64(%rsi,%rdx), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi,%rdx), %xmm0
	movups	-32(%rsi,%rdx), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi,%rdx), %xmm0
	movups	(%rsi,%rdx), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	addq	$-128, %rax
	jne	.LBB1_96
.LBB1_97:                               # %middle.block
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	%rax, 40(%rsp)          # 8-byte Folded Reload
	je	.LBB1_1
# BB#98:                                #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, %rdi
	addq	%rcx, %rdi
	subl	%eax, %r8d
.LBB1_99:                               # %.lr.ph.preheader501
                                        #   in Loop: Header=BB1_2 Depth=1
	leal	-1(%r8), %r15d
	movl	%r8d, %edx
	movq	%rbx, %rax
	andl	$7, %edx
	je	.LBB1_102
# BB#100:                               # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	negl	%edx
	.p2align	4, 0x90
.LBB1_101:                              # %.lr.ph.prol
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%r8d
	movzbl	(%rsi,%rdi), %ebx
	movb	%bl, (%rdi)
	incq	%rdi
	incl	%edx
	jne	.LBB1_101
.LBB1_102:                              # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpl	$7, %r15d
	movq	%rax, %rbx
	jb	.LBB1_1
# BB#103:                               # %.lr.ph.preheader501.new
                                        #   in Loop: Header=BB1_2 Depth=1
	addq	$7, %rdi
	.p2align	4, 0x90
.LBB1_104:                              # %.lr.ph
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rsi,%rdi), %eax
	movb	%al, -7(%rdi)
	movzbl	-6(%rsi,%rdi), %eax
	movb	%al, -6(%rdi)
	movzbl	-5(%rsi,%rdi), %eax
	movb	%al, -5(%rdi)
	movzbl	-4(%rsi,%rdi), %eax
	movb	%al, -4(%rdi)
	movzbl	-3(%rsi,%rdi), %eax
	movb	%al, -3(%rdi)
	movzbl	-2(%rsi,%rdi), %eax
	movb	%al, -2(%rdi)
	movzbl	-1(%rsi,%rdi), %eax
	movb	%al, -1(%rdi)
	addl	$-8, %r8d
	movzbl	(%rsi,%rdi), %eax
	movb	%al, (%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB1_104
	jmp	.LBB1_1
.LBB1_105:
	subq	$8, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	movq	%rdi, %rbx
	movl	$.L.str.1, %edi
	movl	$0, %eax
	movq	%rcx, %rsi
	movq	%r11, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%r10, %r8
	pushq	%rbx
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	callq	cli_dbgmsg
	addq	$32, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset -32
.LBB1_106:                              # %doubledl.exit.thread
	movl	$-1, %eax
.LBB1_107:                              # %doubledl.exit.thread
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_109:
	cmpq	%rsi, %r11
	sbbb	%al, %al
	cmpq	%r10, %rsi
	setbe	%dl
	andb	%al, %dl
	xorl	%eax, %eax
.LBB1_110:                              # %.thread284
	movzbl	%dl, %r9d
	movl	%ebx, %edx
	movq	%rcx, %rbx
	subq	%rdx, %rbx
	xorl	%edx, %edx
	cmpq	%r11, %rbx
	jb	.LBB1_113
# BB#111:                               # %.thread284
	testb	%al, %al
	jne	.LBB1_113
# BB#112:
	movl	%r8d, %eax
	addq	%rbx, %rax
	cmpq	%rax, %r11
	sbbb	%sil, %sil
	cmpq	%r10, %rax
	setbe	%dl
	andb	%sil, %dl
.LBB1_113:
	movzbl	%dl, %ebp
	subq	$8, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str, %edi
	movl	$0, %eax
	movq	%r11, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	pushq	%rbp
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	%r8
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	%rdx
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	callq	cli_dbgmsg
	addq	$48, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB1_106
.LBB1_115:
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%r9, (%rax)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rcx, (%rax)
	xorl	%eax, %eax
	jmp	.LBB1_107
.Lfunc_end1:
	.size	unmew, .Lfunc_end1-unmew
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"MEW: rete: %d %d %d %d %d || %d %d %d %d %d\n"
	.size	.L.str, 45

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"MEW: retf %08x %08x+%08x=%08x, %08x %08x+%08x=%08x\n"
	.size	.L.str.1, 52


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
