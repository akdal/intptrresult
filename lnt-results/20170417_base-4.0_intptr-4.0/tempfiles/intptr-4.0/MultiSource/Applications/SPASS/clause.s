	.text
	.file	"clause.bc"
	.globl	clause_LiteralIsLiteral
	.p2align	4, 0x90
	.type	clause_LiteralIsLiteral,@function
clause_LiteralIsLiteral:                # @clause_LiteralIsLiteral
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB0_1
# BB#2:
	movq	24(%rdi), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB0_4
# BB#3:
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB0_4:                                # %clause_LiteralPredicate.exit
	testl	%eax, %eax
	js	.LBB0_6
# BB#5:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB0_1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB0_6:
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
	retq
.Lfunc_end0:
	.size	clause_LiteralIsLiteral, .Lfunc_end0-clause_LiteralIsLiteral
	.cfi_endproc

	.globl	clause_LiteralComputeWeight
	.p2align	4, 0x90
	.type	clause_LiteralComputeWeight,@function
clause_LiteralComputeWeight:            # @clause_LiteralComputeWeight
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %r8
	xorl	%eax, %eax
	movl	stack_POINTER(%rip), %ecx
	movl	%ecx, %edx
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_1 Depth=1
	leal	-1(%rdx), %r9d
	movq	stack_STACK(,%r9,8), %rdi
	movq	(%rdi), %r10
	movq	8(%rdi), %r8
	movq	%r10, stack_STACK(,%r9,8)
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
	movq	16(%r8), %rdi
	testq	%rdi, %rdi
	je	.LBB1_3
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	movl	180(%rsi), %r8d
	movl	%edx, %r9d
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rdi, stack_STACK(,%r9,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, (%r8)
	jle	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_1 Depth=1
	movl	184(%rsi), %r8d
	jmp	.LBB1_6
.LBB1_5:                                #   in Loop: Header=BB1_1 Depth=1
	movl	180(%rsi), %r8d
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	addl	%r8d, %eax
	cmpl	%ecx, %edx
	je	.LBB1_9
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %edi
	cmpq	$0, stack_STACK(,%rdi,8)
	jne	.LBB1_10
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=2
	movl	%edi, stack_POINTER(%rip)
	cmpl	%edi, %ecx
	movl	%edi, %edx
	jne	.LBB1_7
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_10:                               # %.critedge
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpl	%edx, %ecx
	jne	.LBB1_11
.LBB1_9:                                # %.critedge.thread
	retq
.Lfunc_end1:
	.size	clause_LiteralComputeWeight, .Lfunc_end1-clause_LiteralComputeWeight
	.cfi_endproc

	.globl	clause_LiteralCreate
	.p2align	4, 0x90
	.type	clause_LiteralCreate,@function
clause_LiteralCreate:                   # @clause_LiteralCreate
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rbx, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r14, 16(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	clause_LiteralCreate, .Lfunc_end2-clause_LiteralCreate
	.cfi_endproc

	.globl	clause_LiteralCreateNegative
	.p2align	4, 0x90
	.type	clause_LiteralCreateNegative,@function
clause_LiteralCreateNegative:           # @clause_LiteralCreateNegative
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	$0, 8(%r15)
	movl	fol_NOT(%rip), %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, 24(%rbx)
	movl	$0, 8(%rbx)
	movl	$0, (%rbx)
	movl	$-1, 4(%rbx)
	movq	%r14, 16(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	clause_LiteralCreateNegative, .Lfunc_end3-clause_LiteralCreateNegative
	.cfi_endproc

	.globl	clause_LiteralDelete
	.p2align	4, 0x90
	.type	clause_LiteralDelete,@function
clause_LiteralDelete:                   # @clause_LiteralDelete
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	callq	term_Delete
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	clause_LiteralDelete, .Lfunc_end4-clause_LiteralDelete
	.cfi_endproc

	.globl	clause_LiteralInsertIntoSharing
	.p2align	4, 0x90
	.type	clause_LiteralInsertIntoSharing,@function
clause_LiteralInsertIntoSharing:        # @clause_LiteralInsertIntoSharing
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	24(%rbx), %r14
	movl	fol_NOT(%rip), %ecx
	cmpl	(%r14), %ecx
	jne	.LBB5_2
# BB#1:
	movq	16(%r14), %rcx
	movq	8(%rcx), %r14
.LBB5_2:                                # %clause_LiteralAtom.exit
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	sharing_Insert
	movq	24(%rbx), %rcx
	movl	fol_NOT(%rip), %edx
	cmpl	(%rcx), %edx
	jne	.LBB5_3
# BB#4:
	movq	16(%rcx), %rbx
	addq	$8, %rbx
	jmp	.LBB5_5
.LBB5_3:
	addq	$24, %rbx
.LBB5_5:                                # %clause_LiteralSetAtom.exit
	movq	%rax, (%rbx)
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	term_Delete             # TAILCALL
.Lfunc_end5:
	.size	clause_LiteralInsertIntoSharing, .Lfunc_end5-clause_LiteralInsertIntoSharing
	.cfi_endproc

	.globl	clause_LiteralDeleteFromSharing
	.p2align	4, 0x90
	.type	clause_LiteralDeleteFromSharing,@function
clause_LiteralDeleteFromSharing:        # @clause_LiteralDeleteFromSharing
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	24(%rbx), %rsi
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rsi), %ecx
	jne	.LBB6_2
# BB#1:
	movq	16(%rsi), %rcx
	movq	8(%rcx), %rsi
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	24(%rbx), %rcx
	movq	memory_ARRAY+256(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+256(%rip), %rdx
	movq	%rcx, (%rdx)
.LBB6_2:                                # %clause_LiteralAtom.exit.thread15
	movq	%rbx, %rdi
	movq	%rax, %rdx
	callq	sharing_Delete
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	clause_LiteralDeleteFromSharing, .Lfunc_end6-clause_LiteralDeleteFromSharing
	.cfi_endproc

	.globl	clause_CopyConstraint
	.p2align	4, 0x90
	.type	clause_CopyConstraint,@function
clause_CopyConstraint:                  # @clause_CopyConstraint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -48
.Lcfi29:
	.cfi_offset %r12, -40
.Lcfi30:
	.cfi_offset %r13, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	64(%r14), %r15
	testq	%r15, %r15
	jle	.LBB7_1
# BB#2:                                 # %.lr.ph.i
	decq	%r15
	movq	$-1, %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_3:                                # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB7_5:                                # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB7_3 Depth=1
	callq	term_Copy
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r13, (%rax)
	incq	%rbx
	cmpq	%r15, %rbx
	movq	%rax, %r13
	jl	.LBB7_3
	jmp	.LBB7_6
.LBB7_1:
	xorl	%eax, %eax
.LBB7_6:                                # %clause_CopyLitInterval.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	clause_CopyConstraint, .Lfunc_end7-clause_CopyConstraint
	.cfi_endproc

	.globl	clause_CopyAntecedentExcept
	.p2align	4, 0x90
	.type	clause_CopyAntecedentExcept,@function
clause_CopyAntecedentExcept:            # @clause_CopyAntecedentExcept
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 64
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movslq	64(%r14), %r13
	movl	68(%r14), %eax
	leal	-1(%r13,%rax), %ecx
	cmpl	%r13d, %ecx
	jge	.LBB8_2
# BB#1:
	xorl	%r15d, %r15d
	jmp	.LBB8_8
.LBB8_2:                                # %.lr.ph.i
	movslq	%ecx, %rbp
	incq	%rbp
	incl	%ebx
	subl	%r13d, %ebx
	subl	%eax, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	testl	%ebx, %ebx
	je	.LBB8_7
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	56(%r14), %rax
	movq	-8(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB8_6
# BB#5:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB8_6:                                # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB8_3 Depth=1
	callq	term_Copy
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB8_7:                                #   in Loop: Header=BB8_3 Depth=1
	decq	%rbp
	incl	%ebx
	cmpq	%r13, %rbp
	jg	.LBB8_3
.LBB8_8:                                # %clause_CopyLitIntervalExcept.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	clause_CopyAntecedentExcept, .Lfunc_end8-clause_CopyAntecedentExcept
	.cfi_endproc

	.globl	clause_CopySuccedent
	.p2align	4, 0x90
	.type	clause_CopySuccedent,@function
clause_CopySuccedent:                   # @clause_CopySuccedent
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 48
.Lcfi51:
	.cfi_offset %rbx, -48
.Lcfi52:
	.cfi_offset %r12, -40
.Lcfi53:
	.cfi_offset %r13, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	68(%r14), %eax
	movl	72(%r14), %ecx
	addl	64(%r14), %eax
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	jle	.LBB9_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB9_6
.LBB9_2:                                # %.lr.ph.i
	movslq	%eax, %rbx
	movslq	%ecx, %r12
	decq	%rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB9_5:                                # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB9_3 Depth=1
	callq	term_Copy
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r13, (%rax)
	incq	%rbx
	cmpq	%r12, %rbx
	movq	%rax, %r13
	jl	.LBB9_3
.LBB9_6:                                # %clause_CopyLitInterval.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	clause_CopySuccedent, .Lfunc_end9-clause_CopySuccedent
	.cfi_endproc

	.globl	clause_CopySuccedentExcept
	.p2align	4, 0x90
	.type	clause_CopySuccedentExcept,@function
clause_CopySuccedentExcept:             # @clause_CopySuccedentExcept
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 64
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	movl	64(%r14), %ecx
	movl	68(%r14), %eax
	leal	(%rax,%rcx), %esi
	movl	72(%r14), %edx
	leal	-1(%rdx,%rsi), %edi
	cmpl	%esi, %edi
	jge	.LBB10_2
# BB#1:
	xorl	%r15d, %r15d
	jmp	.LBB10_8
.LBB10_2:                               # %.lr.ph.i
	movslq	%edi, %rbx
	movslq	%esi, %r13
	incq	%rbx
	incl	%ebp
	subl	%ecx, %ebp
	subl	%eax, %ebp
	subl	%edx, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	je	.LBB10_7
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=1
	movq	56(%r14), %rax
	movq	-8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB10_6:                               # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB10_3 Depth=1
	callq	term_Copy
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB10_7:                               #   in Loop: Header=BB10_3 Depth=1
	decq	%rbx
	incl	%ebp
	cmpq	%r13, %rbx
	jg	.LBB10_3
.LBB10_8:                               # %clause_CopyLitIntervalExcept.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	clause_CopySuccedentExcept, .Lfunc_end10-clause_CopySuccedentExcept
	.cfi_endproc

	.globl	clause_IsUnorderedClause
	.p2align	4, 0x90
	.type	clause_IsUnorderedClause,@function
clause_IsUnorderedClause:               # @clause_IsUnorderedClause
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB11_1
# BB#2:
	movl	12(%rdi), %r8d
	testq	%r8, %r8
	je	.LBB11_13
# BB#3:
	movl	24(%rdi), %eax
	movq	16(%rdi), %r9
	decl	%eax
	.p2align	4, 0x90
.LBB11_4:                               # =>This Inner Loop Header: Depth=1
	movslq	%eax, %rdx
	movq	(%r9,%rdx,8), %rsi
	testl	%eax, %eax
	js	.LBB11_6
# BB#5:                                 #   in Loop: Header=BB11_4 Depth=1
	decl	%eax
	testq	%rsi, %rsi
	je	.LBB11_4
.LBB11_6:                               # %.preheader.i.preheader
	movl	$63, %r10d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_7:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	63(%rax), %rcx
	btq	%rcx, %rsi
	jb	.LBB11_11
# BB#8:                                 # %.preheader.i.117
                                        #   in Loop: Header=BB11_7 Depth=1
	decq	%rcx
	btq	%rcx, %rsi
	jb	.LBB11_9
# BB#35:                                # %.preheader.i.218
                                        #   in Loop: Header=BB11_7 Depth=1
	decq	%rcx
	btq	%rcx, %rsi
	jb	.LBB11_36
# BB#37:                                # %.preheader.i.319
                                        #   in Loop: Header=BB11_7 Depth=1
	decq	%rcx
	btq	%rcx, %rsi
	jb	.LBB11_10
# BB#38:                                #   in Loop: Header=BB11_7 Depth=1
	leaq	3(%rcx), %r10
	decq	%rcx
	addq	$-4, %rax
	cmpq	$3, %r10
	movq	%rcx, %r10
	jg	.LBB11_7
# BB#39:
	movl	%ecx, %r10d
	jmp	.LBB11_11
.LBB11_1:
	xorl	%edx, %edx
	movzbl	%dl, %eax
	retq
.LBB11_13:                              # %clause_CheckSplitLevel.exit
	xorl	%edx, %edx
	cmpq	$0, 16(%rdi)
	je	.LBB11_14
.LBB11_34:                              # %clause_LiteralIsLiteral.exit.thread
	movzbl	%dl, %eax
	retq
.LBB11_14:
	xorl	%r9d, %r9d
	cmpl	$0, 68(%rdi)
	jne	.LBB11_18
	jmp	.LBB11_16
.LBB11_9:
	decl	%r10d
	jmp	.LBB11_11
.LBB11_36:
	addl	$-2, %r10d
	jmp	.LBB11_11
.LBB11_10:                              # %.preheader.i.319._crit_edge
	addl	$60, %eax
	movl	%eax, %r10d
.LBB11_11:
	shlq	$6, %rdx
	movslq	%r10d, %rax
	addq	%rdx, %rax
	cmpq	%rax, %r8
	jne	.LBB11_12
# BB#15:
	cmpl	$0, 68(%rdi)
	jne	.LBB11_18
.LBB11_16:
	cmpl	$0, 72(%rdi)
	jne	.LBB11_18
# BB#17:                                # %clause_IsEmptyClause.exit
	cmpl	$0, 64(%rdi)
	je	.LBB11_31
.LBB11_18:                              # %clause_IsEmptyClause.exit.thread
	movq	56(%rdi), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB11_19
# BB#20:
	movq	24(%rax), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB11_22
# BB#21:
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB11_22:                              # %clause_LiteralPredicate.exit.i
	testl	%eax, %eax
	js	.LBB11_24
# BB#23:
	xorl	%edx, %edx
	movzbl	%dl, %eax
	retq
.LBB11_12:
	xorl	%edx, %edx
	movzbl	%dl, %eax
	retq
.LBB11_19:
	xorl	%edx, %edx
	movzbl	%dl, %eax
	retq
.LBB11_24:                              # %clause_LiteralIsLiteral.exit
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	sete	%dl
	jne	.LBB11_34
# BB#25:                                # %clause_LiteralIsLiteral.exit
	testl	%r8d, %r8d
	jne	.LBB11_26
	jmp	.LBB11_34
.LBB11_31:
	testl	%r8d, %r8d
	je	.LBB11_32
.LBB11_26:
	movl	24(%rdi), %eax
	xorl	%edx, %edx
	testl	%eax, %eax
	je	.LBB11_34
# BB#27:
	cmpl	$64, %r8d
	jb	.LBB11_29
# BB#28:                                # %.lr.ph.i.i
	addl	$-64, %r8d
	movl	%r8d, %edx
	shrl	$6, %edx
	movl	%edx, %ecx
	shll	$6, %ecx
	incl	%edx
	subl	%ecx, %r8d
.LBB11_29:                              # %clause_ComputeSplitFieldAddress.exit.i
	cmpl	%eax, %edx
	jae	.LBB11_30
# BB#33:
	movl	%edx, %eax
	movq	(%r9,%rax,8), %rax
	btq	%r8, %rax
	setb	%dl
	movzbl	%dl, %eax
	retq
.LBB11_30:
	xorl	%edx, %edx
	movzbl	%dl, %eax
	retq
.LBB11_32:
	movb	$1, %dl
	movzbl	%dl, %eax
	retq
.Lfunc_end11:
	.size	clause_IsUnorderedClause, .Lfunc_end11-clause_IsUnorderedClause
	.cfi_endproc

	.globl	clause_IsClause
	.p2align	4, 0x90
	.type	clause_IsClause,@function
clause_IsClause:                        # @clause_IsClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 48
.Lcfi74:
	.cfi_offset %rbx, -48
.Lcfi75:
	.cfi_offset %r12, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	callq	clause_IsUnorderedClause
	xorl	%r14d, %r14d
	testl	%eax, %eax
	je	.LBB12_8
# BB#1:
	movl	64(%rbx), %eax
	movl	68(%rbx), %ecx
	movl	72(%rbx), %edx
	leal	(%rax,%rcx), %esi
	leal	-1(%rdx,%rsi), %esi
	cmpl	%esi, %eax
	jg	.LBB12_7
# BB#2:                                 # %.lr.ph
	movslq	%eax, %rbp
	decq	%rbp
	.p2align	4, 0x90
.LBB12_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rsi
	movq	8(%rsi,%rbp,8), %rsi
	movq	24(%rsi), %rsi
	movl	fol_EQUALITY(%rip), %edi
	cmpl	(%rsi), %edi
	jne	.LBB12_6
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
	movq	16(%rsi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	ord_Compare
	cmpl	$1, %eax
	je	.LBB12_8
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB12_3 Depth=1
	movl	64(%rbx), %eax
	movl	68(%rbx), %ecx
	movl	72(%rbx), %edx
.LBB12_6:                               #   in Loop: Header=BB12_3 Depth=1
	leal	(%rax,%rcx), %esi
	leal	-1(%rdx,%rsi), %esi
	movslq	%esi, %rsi
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB12_3
.LBB12_7:
	movl	$1, %r14d
.LBB12_8:                               # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	clause_IsClause, .Lfunc_end12-clause_IsClause
	.cfi_endproc

	.globl	clause_ContainsPositiveEquations
	.p2align	4, 0x90
	.type	clause_ContainsPositiveEquations,@function
clause_ContainsPositiveEquations:       # @clause_ContainsPositiveEquations
	.cfi_startproc
# BB#0:
	movl	72(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.LBB13_7
# BB#1:                                 # %.lr.ph
	movslq	68(%rdi), %rsi
	movslq	64(%rdi), %rcx
	addq	%rsi, %rcx
	addl	%ecx, %edx
	movq	56(%rdi), %r8
	movl	fol_NOT(%rip), %r9d
	movl	fol_EQUALITY(%rip), %edi
	movslq	%edx, %r10
	.p2align	4, 0x90
.LBB13_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rcx,8), %rdx
	movq	24(%rdx), %rdx
	movl	(%rdx), %esi
	cmpl	%esi, %r9d
	jne	.LBB13_5
# BB#4:                                 #   in Loop: Header=BB13_3 Depth=1
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %esi
.LBB13_5:                               # %clause_LiteralIsEquality.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	cmpl	%esi, %edi
	je	.LBB13_6
# BB#2:                                 #   in Loop: Header=BB13_3 Depth=1
	incq	%rcx
	cmpq	%r10, %rcx
	jl	.LBB13_3
	jmp	.LBB13_7
.LBB13_6:
	movl	$1, %eax
.LBB13_7:                               # %clause_LiteralIsEquality.exit._crit_edge
	retq
.Lfunc_end13:
	.size	clause_ContainsPositiveEquations, .Lfunc_end13-clause_ContainsPositiveEquations
	.cfi_endproc

	.globl	clause_ContainsNegativeEquations
	.p2align	4, 0x90
	.type	clause_ContainsNegativeEquations,@function
clause_ContainsNegativeEquations:       # @clause_ContainsNegativeEquations
	.cfi_startproc
# BB#0:
	movl	68(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.LBB14_7
# BB#1:                                 # %.lr.ph
	movslq	64(%rdi), %rcx
	movq	56(%rdi), %r8
	movl	fol_NOT(%rip), %r9d
	movl	fol_EQUALITY(%rip), %r10d
	addl	%ecx, %edx
	movslq	%edx, %rdx
	.p2align	4, 0x90
.LBB14_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rcx,8), %rsi
	movq	24(%rsi), %rdi
	movl	(%rdi), %esi
	cmpl	%esi, %r9d
	jne	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_3 Depth=1
	movq	16(%rdi), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB14_5:                               # %clause_LiteralIsEquality.exit
                                        #   in Loop: Header=BB14_3 Depth=1
	cmpl	%esi, %r10d
	je	.LBB14_6
# BB#2:                                 #   in Loop: Header=BB14_3 Depth=1
	incq	%rcx
	cmpq	%rdx, %rcx
	jl	.LBB14_3
	jmp	.LBB14_7
.LBB14_6:
	movl	$1, %eax
.LBB14_7:                               # %clause_LiteralIsEquality.exit._crit_edge
	retq
.Lfunc_end14:
	.size	clause_ContainsNegativeEquations, .Lfunc_end14-clause_ContainsNegativeEquations
	.cfi_endproc

	.globl	clause_ContainsFolAtom
	.p2align	4, 0x90
	.type	clause_ContainsFolAtom,@function
clause_ContainsFolAtom:                 # @clause_ContainsFolAtom
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 80
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rcx, %r12
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbx
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB15_1:                               # =>This Inner Loop Header: Depth=1
	movslq	68(%rbx), %rax
	movslq	72(%rbx), %rcx
	movslq	64(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %r15
	jge	.LBB15_28
# BB#2:                                 #   in Loop: Header=BB15_1 Depth=1
	cmpl	$0, (%r13)
	je	.LBB15_5
# BB#3:                                 #   in Loop: Header=BB15_1 Depth=1
	cmpl	$0, (%r12)
	je	.LBB15_5
# BB#4:                                 #   in Loop: Header=BB15_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, (%rax)
	jne	.LBB15_28
.LBB15_5:                               # %.critedge2
                                        #   in Loop: Header=BB15_1 Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rdi
	movl	(%rdi), %eax
	movl	fol_NOT(%rip), %edx
	cmpl	%eax, %edx
	movl	%eax, %esi
	jne	.LBB15_7
# BB#6:                                 #   in Loop: Header=BB15_1 Depth=1
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %esi
.LBB15_7:                               # %clause_GetLiteralAtom.exit97
                                        #   in Loop: Header=BB15_1 Depth=1
	negl	%esi
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %esi
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%esi, %rsi
	movq	(%rcx,%rsi,8), %rcx
	movl	16(%rcx), %r14d
	cmpl	%eax, %edx
	jne	.LBB15_9
# BB#8:                                 #   in Loop: Header=BB15_1 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB15_9:                               # %clause_GetLiteralAtom.exit87
                                        #   in Loop: Header=BB15_1 Depth=1
	callq	term_IsGround
	movl	(%r13), %ecx
	orl	%r14d, %ecx
	jne	.LBB15_11
# BB#10:                                #   in Loop: Header=BB15_1 Depth=1
	incl	%ebp
	movl	$1, (%r13)
.LBB15_11:                              #   in Loop: Header=BB15_1 Depth=1
	testl	%eax, %eax
	je	.LBB15_18
# BB#12:                                #   in Loop: Header=BB15_1 Depth=1
	testl	%r14d, %r14d
	jle	.LBB15_18
# BB#13:                                #   in Loop: Header=BB15_1 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jne	.LBB15_18
# BB#14:                                #   in Loop: Header=BB15_1 Depth=1
	movq	56(%rbx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	24(%rcx), %rdx
	movl	(%rdx), %ecx
	cmpl	%ecx, fol_NOT(%rip)
	jne	.LBB15_16
# BB#15:                                #   in Loop: Header=BB15_1 Depth=1
	movq	16(%rdx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB15_16:                              # %clause_GetLiteralAtom.exit77
                                        #   in Loop: Header=BB15_1 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	je	.LBB15_18
# BB#17:                                #   in Loop: Header=BB15_1 Depth=1
	incl	%ebp
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	$1, (%rcx)
.LBB15_18:                              #   in Loop: Header=BB15_1 Depth=1
	cmpl	$1, %r14d
	jne	.LBB15_21
# BB#19:                                #   in Loop: Header=BB15_1 Depth=1
	movl	(%r12), %ecx
	orl	%eax, %ecx
	jne	.LBB15_21
# BB#20:                                #   in Loop: Header=BB15_1 Depth=1
	incl	%ebp
	movl	$1, (%r12)
.LBB15_21:                              #   in Loop: Header=BB15_1 Depth=1
	cmpl	$2, %r14d
	jl	.LBB15_27
# BB#22:                                #   in Loop: Header=BB15_1 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	orl	(%rcx), %eax
	jne	.LBB15_27
# BB#23:                                #   in Loop: Header=BB15_1 Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB15_25
# BB#24:                                #   in Loop: Header=BB15_1 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB15_25:                              # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB15_1 Depth=1
	cmpl	%eax, fol_EQUALITY(%rip)
	je	.LBB15_27
# BB#26:                                #   in Loop: Header=BB15_1 Depth=1
	incl	%ebp
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$1, (%rax)
.LBB15_27:                              #   in Loop: Header=BB15_1 Depth=1
	incq	%r15
	cmpl	$4, %ebp
	jl	.LBB15_1
.LBB15_28:                              # %.critedge
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	clause_ContainsFolAtom, .Lfunc_end15-clause_ContainsFolAtom
	.cfi_endproc

	.globl	clause_ContainsVariables
	.p2align	4, 0x90
	.type	clause_ContainsVariables,@function
clause_ContainsVariables:               # @clause_ContainsVariables
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -24
.Lcfi96:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	68(%r14), %eax
	addl	64(%r14), %eax
	addl	72(%r14), %eax
	jle	.LBB16_7
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB16_4
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB16_4:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB16_2 Depth=1
	callq	term_NumberOfVarOccs
	testl	%eax, %eax
	jne	.LBB16_8
# BB#5:                                 #   in Loop: Header=BB16_2 Depth=1
	incq	%rbx
	movslq	68(%r14), %rax
	movslq	72(%r14), %rcx
	movslq	64(%r14), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB16_2
.LBB16_7:
	xorl	%eax, %eax
	jmp	.LBB16_9
.LBB16_8:
	movl	$1, %eax
.LBB16_9:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	clause_ContainsVariables, .Lfunc_end16-clause_ContainsVariables
	.cfi_endproc

	.globl	clause_ContainsSortRestriction
	.p2align	4, 0x90
	.type	clause_ContainsSortRestriction,@function
clause_ContainsSortRestriction:         # @clause_ContainsSortRestriction
	.cfi_startproc
# BB#0:
	movl	64(%rdi), %eax
	addl	68(%rdi), %eax
	decl	%eax
	js	.LBB17_10
# BB#1:                                 # %.lr.ph
	movq	$-1, %r9
	movl	symbol_TYPESTATBITS(%rip), %ecx
	.p2align	4, 0x90
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rsi)
	je	.LBB17_4
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	cmpl	$0, (%rdx)
	jne	.LBB17_10
.LBB17_4:                               # %.critedge1
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	56(%rdi), %rax
	movq	8(%rax,%r9,8), %rax
	movq	24(%rax), %r8
	movl	(%r8), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB17_6
# BB#5:                                 #   in Loop: Header=BB17_2 Depth=1
	movq	16(%r8), %rax
	movq	8(%rax), %r8
	movl	(%r8), %eax
.LBB17_6:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB17_2 Depth=1
	negl	%eax
	sarl	%cl, %eax
	movq	symbol_SIGNATURE(%rip), %r10
	cltq
	movq	(%r10,%rax,8), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB17_9
# BB#7:                                 #   in Loop: Header=BB17_2 Depth=1
	movl	$1, (%rdx)
	movq	16(%r8), %rax
	movq	8(%rax), %rax
	cmpl	$0, (%rax)
	jle	.LBB17_9
# BB#8:                                 #   in Loop: Header=BB17_2 Depth=1
	movl	$1, (%rsi)
.LBB17_9:                               #   in Loop: Header=BB17_2 Depth=1
	movl	64(%rdi), %r8d
	movl	68(%rdi), %eax
	leal	-1(%r8,%rax), %eax
	cltq
	incq	%r9
	cmpq	%rax, %r9
	jl	.LBB17_2
.LBB17_10:                              # %.critedge
	retq
.Lfunc_end17:
	.size	clause_ContainsSortRestriction, .Lfunc_end17-clause_ContainsSortRestriction
	.cfi_endproc

	.globl	clause_ContainsFunctions
	.p2align	4, 0x90
	.type	clause_ContainsFunctions,@function
clause_ContainsFunctions:               # @clause_ContainsFunctions
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -24
.Lcfi101:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	68(%r14), %eax
	addl	64(%r14), %eax
	addl	72(%r14), %eax
	jle	.LBB18_7
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB18_4
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB18_4:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	callq	term_ContainsFunctions
	testl	%eax, %eax
	jne	.LBB18_8
# BB#5:                                 #   in Loop: Header=BB18_2 Depth=1
	incq	%rbx
	movslq	68(%r14), %rax
	movslq	72(%r14), %rcx
	movslq	64(%r14), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB18_2
.LBB18_7:
	xorl	%eax, %eax
	jmp	.LBB18_9
.LBB18_8:
	movl	$1, %eax
.LBB18_9:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	clause_ContainsFunctions, .Lfunc_end18-clause_ContainsFunctions
	.cfi_endproc

	.globl	clause_ContainsSymbol
	.p2align	4, 0x90
	.type	clause_ContainsSymbol,@function
clause_ContainsSymbol:                  # @clause_ContainsSymbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 32
.Lcfi105:
	.cfi_offset %rbx, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	addl	72(%rbx), %eax
	jle	.LBB19_7
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB19_4
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB19_4:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movl	%r14d, %esi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	jne	.LBB19_8
# BB#5:                                 #   in Loop: Header=BB19_2 Depth=1
	incq	%rbp
	movslq	68(%rbx), %rax
	movslq	72(%rbx), %rcx
	movslq	64(%rbx), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rbp
	jl	.LBB19_2
.LBB19_7:
	xorl	%eax, %eax
	jmp	.LBB19_9
.LBB19_8:
	movl	$1, %eax
.LBB19_9:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end19:
	.size	clause_ContainsSymbol, .Lfunc_end19-clause_ContainsSymbol
	.cfi_endproc

	.globl	clause_NumberOfSymbolOccurrences
	.p2align	4, 0x90
	.type	clause_NumberOfSymbolOccurrences,@function
clause_NumberOfSymbolOccurrences:       # @clause_NumberOfSymbolOccurrences
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi112:
	.cfi_def_cfa_offset 48
.Lcfi113:
	.cfi_offset %rbx, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	addl	72(%r15), %eax
	jle	.LBB20_1
# BB#2:                                 # %.lr.ph
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB20_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB20_5
# BB#4:                                 #   in Loop: Header=BB20_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB20_5:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB20_3 Depth=1
	movl	%r14d, %esi
	callq	term_NumberOfSymbolOccurrences
	addl	%eax, %ebp
	incq	%rbx
	movslq	68(%r15), %rax
	movslq	72(%r15), %rcx
	movslq	64(%r15), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rbx
	jl	.LBB20_3
	jmp	.LBB20_6
.LBB20_1:
	xorl	%ebp, %ebp
.LBB20_6:                               # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	clause_NumberOfSymbolOccurrences, .Lfunc_end20-clause_NumberOfSymbolOccurrences
	.cfi_endproc

	.globl	clause_ImpliesFiniteDomain
	.p2align	4, 0x90
	.type	clause_ImpliesFiniteDomain,@function
clause_ImpliesFiniteDomain:             # @clause_ImpliesFiniteDomain
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 48
.Lcfi122:
	.cfi_offset %rbx, -40
.Lcfi123:
	.cfi_offset %r14, -32
.Lcfi124:
	.cfi_offset %r15, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	64(%r15), %eax
	xorl	%r14d, %r14d
	addl	68(%r15), %eax
	jne	.LBB21_15
# BB#1:                                 # %.preheader
	movl	$1, %r14d
	addl	72(%r15), %eax
	jle	.LBB21_15
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB21_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rbx
	movl	(%rbx), %eax
	cmpl	fol_EQUALITY(%rip), %eax
	jne	.LBB21_14
# BB#4:                                 #   in Loop: Header=BB21_3 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	cmpl	$0, (%rdx)
	movq	8(%rcx), %rdi
	jle	.LBB21_7
# BB#5:                                 #   in Loop: Header=BB21_3 Depth=1
	callq	term_IsGround
	testl	%eax, %eax
	je	.LBB21_14
# BB#6:                                 # %._crit_edge
                                        #   in Loop: Header=BB21_3 Depth=1
	movq	16(%rbx), %rax
	jmp	.LBB21_8
	.p2align	4, 0x90
.LBB21_7:                               #   in Loop: Header=BB21_3 Depth=1
	cmpl	$0, (%rdi)
	jle	.LBB21_14
.LBB21_8:                               #   in Loop: Header=BB21_3 Depth=1
	movq	(%rax), %rcx
	movq	8(%rcx), %rcx
	cmpl	$0, (%rcx)
	jle	.LBB21_10
# BB#9:                                 #   in Loop: Header=BB21_3 Depth=1
	movq	8(%rax), %rdi
	callq	term_IsGround
	testl	%eax, %eax
	je	.LBB21_14
.LBB21_10:                              #   in Loop: Header=BB21_3 Depth=1
	incq	%rbp
	movslq	68(%r15), %rax
	movslq	72(%r15), %rcx
	movslq	64(%r15), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rbp
	jl	.LBB21_3
	jmp	.LBB21_15
.LBB21_14:
	xorl	%r14d, %r14d
.LBB21_15:                              # %.loopexit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	clause_ImpliesFiniteDomain, .Lfunc_end21-clause_ImpliesFiniteDomain
	.cfi_endproc

	.globl	clause_ImpliesNonTrivialDomain
	.p2align	4, 0x90
	.type	clause_ImpliesNonTrivialDomain,@function
clause_ImpliesNonTrivialDomain:         # @clause_ImpliesNonTrivialDomain
	.cfi_startproc
# BB#0:
	movl	68(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB22_9
# BB#1:
	movslq	64(%rdi), %rax
	addl	%eax, %ecx
	addl	72(%rdi), %ecx
	cmpl	$1, %ecx
	jne	.LBB22_9
# BB#2:
	movq	56(%rdi), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %edx
	cmpl	%ecx, %edx
	movl	%ecx, %esi
	jne	.LBB22_4
# BB#3:
	movq	16(%rax), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB22_4:                               # %clause_LiteralIsEquality.exit
	cmpl	%esi, fol_EQUALITY(%rip)
	jne	.LBB22_9
# BB#5:
	movq	16(%rax), %rsi
	movq	8(%rsi), %rdi
	cmpl	%ecx, %edx
	jne	.LBB22_7
# BB#6:
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rdi
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB22_7:                               # %clause_LiteralAtom.exit
	pushq	%rax
.Lcfi126:
	.cfi_def_cfa_offset 16
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	callq	term_Equal
	testl	%eax, %eax
	leaq	8(%rsp), %rsp
	je	.LBB22_8
.LBB22_9:
	xorl	%eax, %eax
	retq
.LBB22_8:
	movl	$1, %eax
	retq
.Lfunc_end22:
	.size	clause_ImpliesNonTrivialDomain, .Lfunc_end22-clause_ImpliesNonTrivialDomain
	.cfi_endproc

	.globl	clause_FiniteMonadicPredicates
	.p2align	4, 0x90
	.type	clause_FiniteMonadicPredicates,@function
clause_FiniteMonadicPredicates:         # @clause_FiniteMonadicPredicates
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi130:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi131:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi133:
	.cfi_def_cfa_offset 80
.Lcfi134:
	.cfi_offset %rbx, -56
.Lcfi135:
	.cfi_offset %r12, -48
.Lcfi136:
	.cfi_offset %r13, -40
.Lcfi137:
	.cfi_offset %r14, -32
.Lcfi138:
	.cfi_offset %r15, -24
.Lcfi139:
	.cfi_offset %rbp, -16
	testq	%rdi, %rdi
	je	.LBB23_1
# BB#2:                                 # %.lr.ph88
	movl	symbol_TYPESTATBITS(%rip), %r9d
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB23_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_16 Depth 2
                                        #       Child Loop BB23_21 Depth 3
                                        #       Child Loop BB23_29 Depth 3
                                        #     Child Loop BB23_5 Depth 2
                                        #       Child Loop BB23_10 Depth 3
	movq	8(%rdi), %r14
	movl	72(%r14), %edx
	testl	%edx, %edx
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	jle	.LBB23_34
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB23_3 Depth=1
	movslq	68(%r14), %rcx
	movslq	64(%r14), %r15
	addq	%rcx, %r15
	addl	%r15d, %edx
	cmpl	$1, %edx
	movslq	%edx, %r10
	movq	%r10, 8(%rsp)           # 8-byte Spill
	jle	.LBB23_16
	.p2align	4, 0x90
.LBB23_5:                               # %.lr.ph.split.us
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB23_10 Depth 3
	movq	56(%r14), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	24(%rcx), %rdi
	movl	(%rdi), %edx
	movl	fol_NOT(%rip), %r8d
	cmpl	%edx, %r8d
	movl	%edx, %ebx
	jne	.LBB23_7
# BB#6:                                 #   in Loop: Header=BB23_5 Depth=2
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ebx
.LBB23_7:                               # %clause_GetLiteralAtom.exit72.us
                                        #   in Loop: Header=BB23_5 Depth=2
	movl	%ebx, %esi
	negl	%esi
	movl	%r9d, %ecx
	sarl	%cl, %esi
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%esi, %rsi
	movq	(%rcx,%rsi,8), %rcx
	cmpl	$1, 16(%rcx)
	jne	.LBB23_15
# BB#8:                                 #   in Loop: Header=BB23_5 Depth=2
	movslq	%ebx, %rbp
	testq	%r13, %r13
	je	.LBB23_12
# BB#9:                                 # %.lr.ph.i58.us.preheader
                                        #   in Loop: Header=BB23_5 Depth=2
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB23_10:                              # %.lr.ph.i58.us
                                        #   Parent Loop BB23_3 Depth=1
                                        #     Parent Loop BB23_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, 8(%rcx)
	je	.LBB23_15
# BB#11:                                #   in Loop: Header=BB23_10 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_10
.LBB23_12:                              # %.loopexit75.us
                                        #   in Loop: Header=BB23_5 Depth=2
	cmpl	%edx, %r8d
	jne	.LBB23_14
# BB#13:                                #   in Loop: Header=BB23_5 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB23_14:                              # %clause_GetLiteralAtom.exit.us
                                        #   in Loop: Header=BB23_5 Depth=2
	callq	term_NumberOfVarOccs
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	%r13, (%rbx)
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, %r12
	movq	%rbx, %r13
	movl	symbol_TYPESTATBITS(%rip), %r9d
	movq	8(%rsp), %r10           # 8-byte Reload
.LBB23_15:                              # %list_PointerMember.exit62.us
                                        #   in Loop: Header=BB23_5 Depth=2
	incq	%r15
	cmpq	%r10, %r15
	jl	.LBB23_5
	jmp	.LBB23_34
	.p2align	4, 0x90
.LBB23_16:                              # %.lr.ph.split
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB23_21 Depth 3
                                        #       Child Loop BB23_29 Depth 3
	movq	56(%r14), %rcx
	movq	(%rcx,%r15,8), %rcx
	movq	24(%rcx), %rdi
	movl	(%rdi), %edx
	movl	fol_NOT(%rip), %r8d
	cmpl	%edx, %r8d
	movl	%edx, %ebx
	jne	.LBB23_18
# BB#17:                                #   in Loop: Header=BB23_16 Depth=2
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ebx
.LBB23_18:                              # %clause_GetLiteralAtom.exit72
                                        #   in Loop: Header=BB23_16 Depth=2
	movl	%ebx, %esi
	negl	%esi
	movl	%r9d, %ecx
	sarl	%cl, %esi
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%esi, %rsi
	movq	(%rcx,%rsi,8), %rcx
	cmpl	$1, 16(%rcx)
	jne	.LBB23_33
# BB#19:                                #   in Loop: Header=BB23_16 Depth=2
	movslq	%ebx, %rbp
	testq	%r13, %r13
	je	.LBB23_23
# BB#20:                                # %.lr.ph.i58.preheader
                                        #   in Loop: Header=BB23_16 Depth=2
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB23_21:                              # %.lr.ph.i58
                                        #   Parent Loop BB23_3 Depth=1
                                        #     Parent Loop BB23_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, 8(%rcx)
	je	.LBB23_33
# BB#22:                                #   in Loop: Header=BB23_21 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_21
.LBB23_23:                              # %.loopexit75
                                        #   in Loop: Header=BB23_16 Depth=2
	cmpl	%edx, %r8d
	jne	.LBB23_25
# BB#24:                                #   in Loop: Header=BB23_16 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB23_25:                              # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB23_16 Depth=2
	callq	term_NumberOfVarOccs
	testl	%eax, %eax
	je	.LBB23_27
# BB#26:                                #   in Loop: Header=BB23_16 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	%r13, (%rbx)
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, %r12
	movq	%rbx, %r13
	jmp	.LBB23_32
.LBB23_27:                              #   in Loop: Header=BB23_16 Depth=2
	testq	%r12, %r12
	je	.LBB23_31
# BB#28:                                # %.lr.ph.i54.preheader
                                        #   in Loop: Header=BB23_16 Depth=2
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB23_29:                              # %.lr.ph.i54
                                        #   Parent Loop BB23_3 Depth=1
                                        #     Parent Loop BB23_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, 8(%rcx)
	je	.LBB23_32
# BB#30:                                #   in Loop: Header=BB23_29 Depth=3
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_29
.LBB23_31:                              # %.loopexit
                                        #   in Loop: Header=BB23_16 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB23_32:                              # %list_PointerMember.exit62
                                        #   in Loop: Header=BB23_16 Depth=2
	movl	symbol_TYPESTATBITS(%rip), %r9d
	movq	8(%rsp), %r10           # 8-byte Reload
.LBB23_33:                              # %list_PointerMember.exit62
                                        #   in Loop: Header=BB23_16 Depth=2
	incq	%r15
	cmpq	%r10, %r15
	jl	.LBB23_16
.LBB23_34:                              # %._crit_edge
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB23_3
# BB#35:                                # %._crit_edge89
	testq	%r13, %r13
	je	.LBB23_37
	.p2align	4, 0x90
.LBB23_36:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %r13
	jne	.LBB23_36
	jmp	.LBB23_37
.LBB23_1:
	xorl	%r12d, %r12d
.LBB23_37:                              # %list_Delete.exit
	movslq	fol_EQUALITY(%rip), %rsi
	movq	%r12, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	list_PointerDeleteElement # TAILCALL
.Lfunc_end23:
	.size	clause_FiniteMonadicPredicates, .Lfunc_end23-clause_FiniteMonadicPredicates
	.cfi_endproc

	.globl	clause_NumberOfVarOccs
	.p2align	4, 0x90
	.type	clause_NumberOfVarOccs,@function
clause_NumberOfVarOccs:                 # @clause_NumberOfVarOccs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi144:
	.cfi_def_cfa_offset 48
.Lcfi145:
	.cfi_offset %rbx, -40
.Lcfi146:
	.cfi_offset %r14, -32
.Lcfi147:
	.cfi_offset %r15, -24
.Lcfi148:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	addl	72(%r15), %eax
	jle	.LBB24_1
# BB#2:                                 # %.lr.ph
	movl	%eax, %r14d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB24_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_NumberOfVarOccs
	addl	%eax, %ebp
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB24_3
	jmp	.LBB24_4
.LBB24_1:
	xorl	%ebp, %ebp
.LBB24_4:                               # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	clause_NumberOfVarOccs, .Lfunc_end24-clause_NumberOfVarOccs
	.cfi_endproc

	.globl	clause_ComputeWeight
	.p2align	4, 0x90
	.type	clause_ComputeWeight,@function
clause_ComputeWeight:                   # @clause_ComputeWeight
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 40
.Lcfi153:
	.cfi_offset %rbx, -40
.Lcfi154:
	.cfi_offset %r14, -32
.Lcfi155:
	.cfi_offset %r15, -24
.Lcfi156:
	.cfi_offset %rbp, -16
	movl	68(%rdi), %eax
	addl	64(%rdi), %eax
	addl	72(%rdi), %eax
	jle	.LBB25_1
# BB#2:                                 # %.lr.ph
	movl	%eax, %r8d
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB25_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_4 Depth 2
                                        #       Child Loop BB25_10 Depth 3
	movq	56(%rdi), %rcx
	movq	(%rcx,%r10,8), %r9
	movq	24(%r9), %r14
	movl	stack_POINTER(%rip), %edx
	movl	%edx, %ebx
	xorl	%r11d, %r11d
	jmp	.LBB25_4
	.p2align	4, 0x90
.LBB25_14:                              # %.critedge.i.i
                                        #   in Loop: Header=BB25_4 Depth=2
	cmpl	%ebx, %edx
	je	.LBB25_12
# BB#15:                                #   in Loop: Header=BB25_4 Depth=2
	movq	(%rbp), %r15
	movq	8(%rbp), %r14
	movq	%r15, stack_STACK(,%rcx,8)
.LBB25_4:                               #   Parent Loop BB25_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_10 Depth 3
	movq	16(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB25_6
# BB#5:                                 #   in Loop: Header=BB25_4 Depth=2
	movl	180(%rsi), %r14d
	movl	%ebx, %ebp
	leal	1(%rbx), %ebx
	movl	%ebx, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rbp,8)
                                        # kill: %EBX<def> %EBX<kill> %RBX<def>
	jmp	.LBB25_9
	.p2align	4, 0x90
.LBB25_6:                               #   in Loop: Header=BB25_4 Depth=2
	cmpl	$0, (%r14)
	jle	.LBB25_8
# BB#7:                                 #   in Loop: Header=BB25_4 Depth=2
	movl	184(%rsi), %r14d
	jmp	.LBB25_9
.LBB25_8:                               #   in Loop: Header=BB25_4 Depth=2
	movl	180(%rsi), %r14d
	.p2align	4, 0x90
.LBB25_9:                               # %.preheader.i.i
                                        #   in Loop: Header=BB25_4 Depth=2
	addl	%r14d, %r11d
	cmpl	%edx, %ebx
	je	.LBB25_12
	.p2align	4, 0x90
.LBB25_10:                              # %.lr.ph.i.i
                                        #   Parent Loop BB25_3 Depth=1
                                        #     Parent Loop BB25_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rbx), %ecx
	movq	stack_STACK(,%rcx,8), %rbp
	testq	%rbp, %rbp
	jne	.LBB25_14
# BB#11:                                #   in Loop: Header=BB25_10 Depth=3
	movl	%ecx, stack_POINTER(%rip)
	cmpl	%ecx, %edx
	movl	%ecx, %ebx
	jne	.LBB25_10
.LBB25_12:                              # %clause_UpdateLiteralWeight.exit
                                        #   in Loop: Header=BB25_3 Depth=1
	movl	%r11d, 4(%r9)
	addl	%r11d, %eax
	incq	%r10
	cmpq	%r8, %r10
	jne	.LBB25_3
	jmp	.LBB25_13
.LBB25_1:
	xorl	%eax, %eax
.LBB25_13:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	clause_ComputeWeight, .Lfunc_end25-clause_ComputeWeight
	.cfi_endproc

	.globl	clause_ComputeTermDepth
	.p2align	4, 0x90
	.type	clause_ComputeTermDepth,@function
clause_ComputeTermDepth:                # @clause_ComputeTermDepth
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 48
.Lcfi162:
	.cfi_offset %rbx, -40
.Lcfi163:
	.cfi_offset %r14, -32
.Lcfi164:
	.cfi_offset %r15, -24
.Lcfi165:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	addl	72(%r15), %eax
	jle	.LBB26_1
# BB#2:                                 # %.lr.ph
	movl	%eax, %r14d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB26_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB26_5
# BB#4:                                 #   in Loop: Header=BB26_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB26_5:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB26_3 Depth=1
	callq	term_Depth
	cmpl	%ebp, %eax
	cmoval	%eax, %ebp
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB26_3
	jmp	.LBB26_6
.LBB26_1:
	xorl	%ebp, %ebp
.LBB26_6:                               # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	clause_ComputeTermDepth, .Lfunc_end26-clause_ComputeTermDepth
	.cfi_endproc

	.globl	clause_MaxTermDepthClauseList
	.p2align	4, 0x90
	.type	clause_MaxTermDepthClauseList,@function
clause_MaxTermDepthClauseList:          # @clause_MaxTermDepthClauseList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi166:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi167:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi168:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi169:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi170:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi172:
	.cfi_def_cfa_offset 64
.Lcfi173:
	.cfi_offset %rbx, -56
.Lcfi174:
	.cfi_offset %r12, -48
.Lcfi175:
	.cfi_offset %r13, -40
.Lcfi176:
	.cfi_offset %r14, -32
.Lcfi177:
	.cfi_offset %r15, -24
.Lcfi178:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	testq	%r15, %r15
	jne	.LBB27_2
	jmp	.LBB27_8
	.p2align	4, 0x90
.LBB27_7:                               # %clause_ComputeTermDepth.exit
                                        #   in Loop: Header=BB27_2 Depth=1
	cmpl	%r14d, %ebx
	cmoval	%ebx, %r14d
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB27_8
.LBB27_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_4 Depth 2
	movq	8(%r15), %r13
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	movl	$0, %ebx
	jle	.LBB27_7
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movl	%eax, %r12d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_4:                               #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB27_6
# BB#5:                                 #   in Loop: Header=BB27_4 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB27_6:                               # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB27_4 Depth=2
	callq	term_Depth
	cmpl	%ebx, %eax
	cmoval	%eax, %ebx
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB27_4
	jmp	.LBB27_7
.LBB27_8:                               # %._crit_edge
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	clause_MaxTermDepthClauseList, .Lfunc_end27-clause_MaxTermDepthClauseList
	.cfi_endproc

	.globl	clause_ComputeSize
	.p2align	4, 0x90
	.type	clause_ComputeSize,@function
clause_ComputeSize:                     # @clause_ComputeSize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi179:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi180:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi181:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi183:
	.cfi_def_cfa_offset 48
.Lcfi184:
	.cfi_offset %rbx, -40
.Lcfi185:
	.cfi_offset %r14, -32
.Lcfi186:
	.cfi_offset %r15, -24
.Lcfi187:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	addl	72(%r15), %eax
	jle	.LBB28_1
# BB#2:                                 # %.lr.ph
	movl	%eax, %r14d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB28_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_ComputeSize
	addl	%eax, %ebp
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB28_3
	jmp	.LBB28_4
.LBB28_1:
	xorl	%ebp, %ebp
.LBB28_4:                               # %._crit_edge
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	clause_ComputeSize, .Lfunc_end28-clause_ComputeSize
	.cfi_endproc

	.globl	clause_WeightCorrect
	.p2align	4, 0x90
	.type	clause_WeightCorrect,@function
clause_WeightCorrect:                   # @clause_WeightCorrect
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi188:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi189:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi190:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi191:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 48
.Lcfi193:
	.cfi_offset %rbx, -48
.Lcfi194:
	.cfi_offset %r12, -40
.Lcfi195:
	.cfi_offset %r14, -32
.Lcfi196:
	.cfi_offset %r15, -24
.Lcfi197:
	.cfi_offset %rbp, -16
	movslq	68(%rdi), %rax
	movslq	72(%rdi), %rcx
	movslq	64(%rdi), %r8
	addq	%rax, %r8
	addq	%rcx, %r8
	testl	%r8d, %r8d
	jle	.LBB29_1
# BB#2:                                 # %.lr.ph
	xorl	%r11d, %r11d
	movl	stack_POINTER(%rip), %edx
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB29_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_4 Depth 2
                                        #       Child Loop BB29_10 Depth 3
	movq	56(%rdi), %rax
	movq	(%rax,%r11,8), %r9
	movq	24(%r9), %r14
	movl	%edx, %eax
	movl	%edx, %ebp
	xorl	%r15d, %r15d
	jmp	.LBB29_4
	.p2align	4, 0x90
.LBB29_12:                              # %.critedge.i
                                        #   in Loop: Header=BB29_4 Depth=2
	cmpl	%ebp, %edx
	je	.LBB29_13
# BB#19:                                #   in Loop: Header=BB29_4 Depth=2
	movq	(%rcx), %r12
	movq	8(%rcx), %r14
	movq	%r12, stack_STACK(,%rbx,8)
.LBB29_4:                               #   Parent Loop BB29_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB29_10 Depth 3
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB29_6
# BB#5:                                 #   in Loop: Header=BB29_4 Depth=2
	movl	180(%rsi), %r14d
	leal	1(%rbp), %eax
	movl	%eax, stack_POINTER(%rip)
	movl	%ebp, %ecx
	movq	%rbx, stack_STACK(,%rcx,8)
	movl	%eax, %ebp
	jmp	.LBB29_9
	.p2align	4, 0x90
.LBB29_6:                               #   in Loop: Header=BB29_4 Depth=2
	cmpl	$0, (%r14)
	jle	.LBB29_8
# BB#7:                                 #   in Loop: Header=BB29_4 Depth=2
	movl	184(%rsi), %r14d
	jmp	.LBB29_9
.LBB29_8:                               #   in Loop: Header=BB29_4 Depth=2
	movl	180(%rsi), %r14d
	.p2align	4, 0x90
.LBB29_9:                               # %.preheader.i
                                        #   in Loop: Header=BB29_4 Depth=2
	addl	%r14d, %r15d
	cmpl	%edx, %ebp
	je	.LBB29_13
	.p2align	4, 0x90
.LBB29_10:                              # %.lr.ph.i
                                        #   Parent Loop BB29_3 Depth=1
                                        #     Parent Loop BB29_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rbp), %ebx
	movq	stack_STACK(,%rbx,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB29_12
# BB#11:                                #   in Loop: Header=BB29_10 Depth=3
	movl	%ebx, stack_POINTER(%rip)
	cmpl	%ebx, %edx
	movl	%ebx, %eax
	movl	%ebx, %ebp
	jne	.LBB29_10
# BB#14:                                # %clause_LiteralComputeWeight.exit
                                        #   in Loop: Header=BB29_3 Depth=1
	cmpl	4(%r9), %r15d
	jne	.LBB29_15
.LBB29_16:                              #   in Loop: Header=BB29_3 Depth=1
	addl	%r15d, %r10d
	incq	%r11
	cmpq	%r8, %r11
	jl	.LBB29_3
	jmp	.LBB29_17
.LBB29_13:                              #   in Loop: Header=BB29_3 Depth=1
	movl	%eax, %edx
	cmpl	4(%r9), %r15d
	je	.LBB29_16
.LBB29_15:
	xorl	%eax, %eax
	jmp	.LBB29_18
.LBB29_1:
	xorl	%r10d, %r10d
.LBB29_17:                              # %._crit_edge
	xorl	%eax, %eax
	cmpl	%r10d, 4(%rdi)
	sete	%al
.LBB29_18:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	clause_WeightCorrect, .Lfunc_end29-clause_WeightCorrect
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_Weight,@function
clause_Weight:                          # @clause_Weight
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %eax
	retq
.Lfunc_end30:
	.size	clause_Weight, .Lfunc_end30-clause_Weight
	.cfi_endproc

	.globl	clause_InsertWeighed
	.p2align	4, 0x90
	.type	clause_InsertWeighed,@function
clause_InsertWeighed:                   # @clause_InsertWeighed
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi198:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi199:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi200:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi201:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi202:
	.cfi_def_cfa_offset 48
.Lcfi203:
	.cfi_offset %rbx, -40
.Lcfi204:
	.cfi_offset %r12, -32
.Lcfi205:
	.cfi_offset %r14, -24
.Lcfi206:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r15, %r15
	je	.LBB31_7
# BB#1:
	movl	4(%r14), %eax
	movq	8(%r15), %rcx
	cmpl	%eax, 4(%rcx)
	jbe	.LBB31_2
.LBB31_7:
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	jmp	.LBB31_6
.LBB31_2:
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB31_3:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r12
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB31_5
# BB#4:                                 #   in Loop: Header=BB31_3 Depth=1
	movq	8(%rbx), %rcx
	cmpl	%eax, 4(%rcx)
	jbe	.LBB31_3
.LBB31_5:                               # %.critedge
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%r12)
.LBB31_6:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end31:
	.size	clause_InsertWeighed, .Lfunc_end31-clause_InsertWeighed
	.cfi_endproc

	.globl	clause_ListSortWeighed
	.p2align	4, 0x90
	.type	clause_ListSortWeighed,@function
clause_ListSortWeighed:                 # @clause_ListSortWeighed
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi209:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi210:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi211:
	.cfi_def_cfa_offset 48
.Lcfi212:
	.cfi_offset %rbx, -48
.Lcfi213:
	.cfi_offset %r12, -40
.Lcfi214:
	.cfi_offset %r13, -32
.Lcfi215:
	.cfi_offset %r14, -24
.Lcfi216:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB32_6
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB32_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r15
	movslq	4(%r15), %r12
	cmpq	$19, %r12
	jg	.LBB32_4
# BB#3:                                 #   in Loop: Header=BB32_2 Depth=1
	movq	clause_SORT(,%r12,8), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, clause_SORT(,%r12,8)
	jmp	.LBB32_5
	.p2align	4, 0x90
.LBB32_4:                               #   in Loop: Header=BB32_2 Depth=1
	movq	clause_SORT+160(%rip), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, clause_SORT+160(%rip)
.LBB32_5:                               #   in Loop: Header=BB32_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB32_2
.LBB32_6:                               # %._crit_edge
	movq	clause_SORT+160(%rip), %rdi
	movl	$clause_Weight, %esi
	callq	list_NumberSort
	movq	$0, clause_SORT+160(%rip)
	movl	$19, %ecx
	.p2align	4, 0x90
.LBB32_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_11 Depth 2
                                        #     Child Loop BB32_20 Depth 2
	movq	clause_SORT(,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB32_8
# BB#9:                                 #   in Loop: Header=BB32_7 Depth=1
	testq	%rax, %rax
	je	.LBB32_13
# BB#10:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB32_7 Depth=1
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB32_11:                              # %.preheader.i
                                        #   Parent Loop BB32_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rsi
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.LBB32_11
# BB#12:                                #   in Loop: Header=BB32_7 Depth=1
	movq	%rax, (%rsi)
	jmp	.LBB32_13
	.p2align	4, 0x90
.LBB32_8:                               #   in Loop: Header=BB32_7 Depth=1
	movq	%rax, %rdx
.LBB32_13:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB32_7 Depth=1
	movq	$0, clause_SORT(,%rcx,8)
	movq	clause_SORT-8(,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB32_14
# BB#18:                                #   in Loop: Header=BB32_7 Depth=1
	testq	%rdx, %rdx
	je	.LBB32_22
# BB#19:                                # %.preheader.i.preheader.1
                                        #   in Loop: Header=BB32_7 Depth=1
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB32_20:                              # %.preheader.i.1
                                        #   Parent Loop BB32_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rsi
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.LBB32_20
# BB#21:                                #   in Loop: Header=BB32_7 Depth=1
	movq	%rdx, (%rsi)
	jmp	.LBB32_22
	.p2align	4, 0x90
.LBB32_14:                              #   in Loop: Header=BB32_7 Depth=1
	movq	%rdx, %rax
.LBB32_22:                              # %list_Nconc.exit.1
                                        #   in Loop: Header=BB32_7 Depth=1
	movq	$0, clause_SORT-8(,%rcx,8)
	cmpq	$1, %rcx
	leaq	-2(%rcx), %rcx
	jg	.LBB32_7
# BB#15:
	testq	%r14, %r14
	je	.LBB32_17
	.p2align	4, 0x90
.LBB32_16:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r14, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r14
	jne	.LBB32_16
.LBB32_17:                              # %list_Delete.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end32:
	.size	clause_ListSortWeighed, .Lfunc_end32-clause_ListSortWeighed
	.cfi_endproc

	.globl	clause_LiteralCopy
	.p2align	4, 0x90
	.type	clause_LiteralCopy,@function
clause_LiteralCopy:                     # @clause_LiteralCopy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi217:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi218:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi219:
	.cfi_def_cfa_offset 32
.Lcfi220:
	.cfi_offset %rbx, -24
.Lcfi221:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	24(%r14), %rdi
	callq	term_Copy
	movq	%rax, 24(%rbx)
	movl	8(%r14), %eax
	movl	%eax, 8(%rbx)
	movl	(%r14), %eax
	movl	%eax, (%rbx)
	movl	4(%r14), %eax
	movl	%eax, 4(%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end33:
	.size	clause_LiteralCopy, .Lfunc_end33-clause_LiteralCopy
	.cfi_endproc

	.globl	clause_Copy
	.p2align	4, 0x90
	.type	clause_Copy,@function
clause_Copy:                            # @clause_Copy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi222:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi223:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi224:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi225:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi226:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi227:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi228:
	.cfi_def_cfa_offset 64
.Lcfi229:
	.cfi_offset %rbx, -56
.Lcfi230:
	.cfi_offset %r12, -48
.Lcfi231:
	.cfi_offset %r13, -40
.Lcfi232:
	.cfi_offset %r14, -32
.Lcfi233:
	.cfi_offset %r15, -24
.Lcfi234:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$80, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movl	(%r14), %eax
	movl	%eax, (%r15)
	movl	52(%r14), %eax
	movl	%eax, 52(%r15)
	movl	48(%r14), %eax
	movl	%eax, 48(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12(%r15)
	movl	12(%r14), %eax
	movl	%eax, 12(%r15)
	movl	24(%r14), %r12d
	testq	%r12, %r12
	je	.LBB34_9
# BB#1:                                 # %.lr.ph.i
	movq	16(%r14), %rbx
	leal	(,%r12,8), %edi
	callq	memory_Malloc
	movq	%rax, 16(%r15)
	movl	%r12d, 24(%r15)
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	cmpl	$1, %r12d
	je	.LBB34_9
# BB#2:                                 # %._crit_edge66.preheader
	leal	3(%r12), %edx
	leaq	-2(%r12), %rcx
	andq	$3, %rdx
	je	.LBB34_3
# BB#4:                                 # %._crit_edge66.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB34_5:                               # %._crit_edge66.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rsi
	movq	8(%rbx,%rax,8), %rdi
	movq	%rdi, 8(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB34_5
# BB#6:                                 # %._crit_edge66.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$3, %rcx
	jae	.LBB34_8
	jmp	.LBB34_9
.LBB34_3:
	movl	$1, %eax
	cmpq	$3, %rcx
	jb	.LBB34_9
	.p2align	4, 0x90
.LBB34_8:                               # %._crit_edge66
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rcx
	movq	(%rbx,%rax,8), %rdx
	movq	%rdx, (%rcx,%rax,8)
	movq	16(%r15), %rcx
	movq	8(%rbx,%rax,8), %rdx
	movq	%rdx, 8(%rcx,%rax,8)
	movq	16(%r15), %rcx
	movq	16(%rbx,%rax,8), %rdx
	movq	%rdx, 16(%rcx,%rax,8)
	movq	16(%r15), %rcx
	movq	24(%rbx,%rax,8), %rdx
	movq	%rdx, 24(%rcx,%rax,8)
	addq	$4, %rax
	cmpq	%rax, %r12
	jne	.LBB34_8
.LBB34_9:                               # %clause_SetSplitField.exit
	movl	8(%r14), %eax
	movl	%eax, 8(%r15)
	movl	4(%r14), %eax
	movl	%eax, 4(%r15)
	movq	32(%r14), %rdi
	callq	list_Copy
	movq	%rax, 32(%r15)
	movq	40(%r14), %rdi
	callq	list_Copy
	movq	%rax, 40(%r15)
	movl	76(%r14), %eax
	movl	%eax, 76(%r15)
	movl	64(%r14), %eax
	movl	%eax, 64(%r15)
	movl	68(%r14), %ebx
	movl	%ebx, 68(%r15)
	movl	72(%r14), %ecx
	movl	%ecx, 72(%r15)
	addl	%eax, %ebx
	addl	%ecx, %ebx
	je	.LBB34_13
# BB#10:                                # %.preheader
	leal	(,%rbx,8), %edi
	callq	memory_Malloc
	movq	%rax, 56(%r15)
	testl	%ebx, %ebx
	jle	.LBB34_13
# BB#11:                                # %.lr.ph
	movl	%ebx, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB34_12:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%r13,8), %rbp
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	24(%rbp), %rdi
	callq	term_Copy
	movq	%rax, 24(%rbx)
	movl	8(%rbp), %eax
	movl	%eax, 8(%rbx)
	movl	(%rbp), %eax
	movl	%eax, (%rbx)
	movl	4(%rbp), %eax
	movl	%eax, 4(%rbx)
	movq	$0, 16(%rbx)
	movq	56(%r15), %rax
	movq	%rbx, (%rax,%r13,8)
	movq	56(%r15), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r15, 16(%rax)
	incq	%r13
	cmpq	%r13, %r12
	jne	.LBB34_12
.LBB34_13:                              # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end34:
	.size	clause_Copy, .Lfunc_end34-clause_Copy
	.cfi_endproc

	.globl	clause_LiteralMaxVar
	.p2align	4, 0x90
	.type	clause_LiteralMaxVar,@function
clause_LiteralMaxVar:                   # @clause_LiteralMaxVar
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %ecx
	movq	24(%rdi), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB35_2
# BB#1:
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB35_2:                               # %clause_LiteralAtom.exit.preheader
	xorl	%eax, %eax
	movl	%ecx, %edx
	jmp	.LBB35_3
	.p2align	4, 0x90
.LBB35_12:                              #   in Loop: Header=BB35_3 Depth=1
	leal	-1(%rdx), %edi
	movq	stack_STACK(,%rdi,8), %rsi
	movq	(%rsi), %r8
	movq	8(%rsi), %rsi
	movq	%r8, stack_STACK(,%rdi,8)
.LBB35_3:                               # %clause_LiteralAtom.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_8 Depth 2
	movq	16(%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB35_5
# BB#4:                                 #   in Loop: Header=BB35_3 Depth=1
	movl	%edx, %esi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rdi, stack_STACK(,%rsi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpl	%ecx, %edx
	jne	.LBB35_8
	jmp	.LBB35_10
	.p2align	4, 0x90
.LBB35_5:                               #   in Loop: Header=BB35_3 Depth=1
	movl	(%rsi), %esi
	testl	%esi, %esi
	jle	.LBB35_7
# BB#6:                                 #   in Loop: Header=BB35_3 Depth=1
	cmpl	%esi, %eax
	cmovll	%esi, %eax
.LBB35_7:                               # %.preheader
                                        #   in Loop: Header=BB35_3 Depth=1
	cmpl	%ecx, %edx
	je	.LBB35_10
	.p2align	4, 0x90
.LBB35_8:                               # %.lr.ph
                                        #   Parent Loop BB35_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %esi
	cmpq	$0, stack_STACK(,%rsi,8)
	jne	.LBB35_11
# BB#9:                                 #   in Loop: Header=BB35_8 Depth=2
	movl	%esi, stack_POINTER(%rip)
	cmpl	%esi, %ecx
	movl	%esi, %edx
	jne	.LBB35_8
	jmp	.LBB35_10
	.p2align	4, 0x90
.LBB35_11:                              # %.critedge
                                        #   in Loop: Header=BB35_3 Depth=1
	cmpl	%edx, %ecx
	jne	.LBB35_12
.LBB35_10:                              # %.critedge.thread
	retq
.Lfunc_end35:
	.size	clause_LiteralMaxVar, .Lfunc_end35-clause_LiteralMaxVar
	.cfi_endproc

	.globl	clause_AtomMaxVar
	.p2align	4, 0x90
	.type	clause_AtomMaxVar,@function
clause_AtomMaxVar:                      # @clause_AtomMaxVar
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	movl	stack_POINTER(%rip), %ecx
	movl	%ecx, %edx
	jmp	.LBB36_1
	.p2align	4, 0x90
.LBB36_10:                              #   in Loop: Header=BB36_1 Depth=1
	leal	-1(%rdx), %esi
	movq	stack_STACK(,%rsi,8), %rdi
	movq	(%rdi), %r8
	movq	8(%rdi), %rdi
	movq	%r8, stack_STACK(,%rsi,8)
.LBB36_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB36_6 Depth 2
	movq	16(%rdi), %rsi
	testq	%rsi, %rsi
	je	.LBB36_3
# BB#2:                                 #   in Loop: Header=BB36_1 Depth=1
	movl	%edx, %edi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rdi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpl	%ecx, %edx
	jne	.LBB36_6
	jmp	.LBB36_8
	.p2align	4, 0x90
.LBB36_3:                               #   in Loop: Header=BB36_1 Depth=1
	movl	(%rdi), %esi
	testl	%esi, %esi
	jle	.LBB36_5
# BB#4:                                 #   in Loop: Header=BB36_1 Depth=1
	cmpl	%esi, %eax
	cmovll	%esi, %eax
.LBB36_5:                               # %.preheader
                                        #   in Loop: Header=BB36_1 Depth=1
	cmpl	%ecx, %edx
	je	.LBB36_8
	.p2align	4, 0x90
.LBB36_6:                               # %.lr.ph
                                        #   Parent Loop BB36_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %esi
	cmpq	$0, stack_STACK(,%rsi,8)
	jne	.LBB36_9
# BB#7:                                 #   in Loop: Header=BB36_6 Depth=2
	movl	%esi, stack_POINTER(%rip)
	cmpl	%esi, %ecx
	movl	%esi, %edx
	jne	.LBB36_6
	jmp	.LBB36_8
	.p2align	4, 0x90
.LBB36_9:                               # %.critedge
                                        #   in Loop: Header=BB36_1 Depth=1
	cmpl	%edx, %ecx
	jne	.LBB36_10
.LBB36_8:                               # %.critedge.thread
	retq
.Lfunc_end36:
	.size	clause_AtomMaxVar, .Lfunc_end36-clause_AtomMaxVar
	.cfi_endproc

	.globl	clause_SetMaxLitFlags
	.p2align	4, 0x90
	.type	clause_SetMaxLitFlags,@function
clause_SetMaxLitFlags:                  # @clause_SetMaxLitFlags
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi235:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi236:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi237:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi238:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi239:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi240:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi241:
	.cfi_def_cfa_offset 128
.Lcfi242:
	.cfi_offset %rbx, -56
.Lcfi243:
	.cfi_offset %r12, -48
.Lcfi244:
	.cfi_offset %r13, -40
.Lcfi245:
	.cfi_offset %r14, -32
.Lcfi246:
	.cfi_offset %r15, -24
.Lcfi247:
	.cfi_offset %rbp, -16
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	movslq	64(%rbp), %rbx
	movl	48(%rbp), %eax
	movl	68(%rbp), %r12d
	movl	72(%rbp), %r15d
	leal	(%r12,%rbx), %r14d
	addl	%r15d, %r14d
	testb	$2, %al
	je	.LBB37_2
# BB#1:
	addl	$-2, %eax
	movl	%eax, 48(%rbp)
.LBB37_2:                               # %clause_RemoveFlag.exit.preheader
	testl	%r14d, %r14d
	jle	.LBB37_7
# BB#3:                                 # %.lr.ph95
	movl	%r14d, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB37_5
	.p2align	4, 0x90
.LBB37_4:                               # %clause_RemoveFlag.exit.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rbp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movl	$0, (%rdi)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB37_4
.LBB37_5:                               # %clause_RemoveFlag.exit.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB37_7
	.p2align	4, 0x90
.LBB37_6:                               # %clause_RemoveFlag.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rbp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	56(%rbp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	56(%rbp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movl	$0, (%rdx)
	movq	56(%rbp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movl	$0, (%rdx)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB37_6
.LBB37_7:                               # %clause_RemoveFlag.exit._crit_edge
	movl	clause_STAMPID(%rip), %edi
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	callq	term_StampOverflow
	movq	8(%rsp), %rbp           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB37_13
# BB#8:                                 # %clause_RemoveFlag.exit._crit_edge
	testl	%r14d, %r14d
	jle	.LBB37_13
# BB#9:                                 # %.lr.ph93
	movl	%r14d, %eax
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB37_11
	.p2align	4, 0x90
.LBB37_10:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rbp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	24(%rdi), %rdi
	movl	$0, 24(%rdi)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB37_10
.LBB37_11:                              # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB37_13
	.p2align	4, 0x90
.LBB37_12:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rbp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	24(%rdx), %rdx
	movl	$0, 24(%rdx)
	movq	56(%rbp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	24(%rdx), %rdx
	movl	$0, 24(%rdx)
	movq	56(%rbp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	24(%rdx), %rdx
	movl	$0, 24(%rdx)
	movq	56(%rbp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	24(%rdx), %rdx
	movl	$0, 24(%rdx)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB37_12
.LBB37_13:                              # %.loopexit
	movl	term_STAMP(%rip), %eax
	incl	%eax
	movl	%eax, term_STAMP(%rip)
	cmpl	%r14d, %ebx
	jge	.LBB37_28
# BB#14:                                # %.lr.ph90.split.us.preheader
	movslq	%r14d, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	addl	%r15d, %r12d
	addl	%ebx, %r12d
	xorl	%edx, %edx
	leaq	1(%rbx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jmp	.LBB37_15
	.p2align	4, 0x90
.LBB37_27:                              # %._crit_edge.us
                                        #   in Loop: Header=BB37_15 Depth=1
	testl	%r13d, %r13d
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	je	.LBB37_16
# BB#18:                                #   in Loop: Header=BB37_15 Depth=1
	movl	(%r14), %eax
	movl	%eax, %ecx
	orl	$1, %ecx
	movl	%ecx, (%r14)
	testl	%r15d, %r15d
	jne	.LBB37_16
# BB#19:                                #   in Loop: Header=BB37_15 Depth=1
	orl	$3, %eax
	movl	%eax, (%r14)
	.p2align	4, 0x90
.LBB37_16:                              #   in Loop: Header=BB37_15 Depth=1
	incq	%rbx
	cmpl	%r12d, %ebx
	je	.LBB37_28
# BB#17:                                # %..lr.ph90.split.us_crit_edge
                                        #   in Loop: Header=BB37_15 Depth=1
	movl	term_STAMP(%rip), %eax
	incl	%edx
.LBB37_15:                              # %.lr.ph90.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_21 Depth 2
	movq	56(%rbp), %rcx
	movq	(%rcx,%rbx,8), %r14
	movq	24(%r14), %rcx
	cmpl	24(%rcx), %eax
	je	.LBB37_16
# BB#20:                                # %.lr.ph.us
                                        #   in Loop: Header=BB37_15 Depth=1
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movl	%edx, %r12d
	movl	$1, %r13d
	xorl	%r15d, %r15d
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB37_21:                              #   Parent Loop BB37_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%r12d, %r12d
	je	.LBB37_25
# BB#22:                                #   in Loop: Header=BB37_21 Depth=2
	movq	56(%rbp), %rax
	movq	-8(%rax,%rbx,8), %rbp
	movq	24(%r14), %rdi
	movl	8(%r14), %esi
	movq	24(%rbp), %rdx
	movl	8(%rbp), %ecx
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	xorl	%r8d, %r8d
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	ord_LiteralCompare
	cmpl	$2, %eax
	movl	$1, %ecx
	cmovel	%ecx, %r15d
	cmpl	$1, %eax
	movl	$0, %ecx
	cmovel	%ecx, %r13d
	cmpl	$3, %eax
	jne	.LBB37_24
# BB#23:                                #   in Loop: Header=BB37_21 Depth=2
	movq	24(%rbp), %rax
	movl	term_STAMP(%rip), %ecx
	movl	%ecx, 24(%rax)
.LBB37_24:                              #   in Loop: Header=BB37_21 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB37_25:                              #   in Loop: Header=BB37_21 Depth=2
	cmpq	64(%rsp), %rbx          # 8-byte Folded Reload
	jge	.LBB37_27
# BB#26:                                #   in Loop: Header=BB37_21 Depth=2
	incq	%rbx
	decl	%r12d
	testl	%r13d, %r13d
	jne	.LBB37_21
	jmp	.LBB37_27
.LBB37_28:                              # %._crit_edge91
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	clause_SetMaxLitFlags, .Lfunc_end37-clause_SetMaxLitFlags
	.cfi_endproc

	.globl	clause_SearchMaxVar
	.p2align	4, 0x90
	.type	clause_SearchMaxVar,@function
clause_SearchMaxVar:                    # @clause_SearchMaxVar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi248:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi249:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi250:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi251:
	.cfi_def_cfa_offset 40
.Lcfi252:
	.cfi_offset %rbx, -40
.Lcfi253:
	.cfi_offset %r14, -32
.Lcfi254:
	.cfi_offset %r15, -24
.Lcfi255:
	.cfi_offset %rbp, -16
	movl	68(%rdi), %eax
	addl	64(%rdi), %eax
	addl	72(%rdi), %eax
	jle	.LBB38_1
# BB#2:                                 # %.lr.ph
	movl	stack_POINTER(%rip), %ebp
	movl	fol_NOT(%rip), %r8d
	movl	%eax, %r9d
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB38_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_6 Depth 2
                                        #       Child Loop BB38_11 Depth 3
	movq	56(%rdi), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	24(%rcx), %r14
	cmpl	(%r14), %r8d
	jne	.LBB38_5
# BB#4:                                 #   in Loop: Header=BB38_3 Depth=1
	movq	16(%r14), %rcx
	movq	8(%rcx), %r14
.LBB38_5:                               # %clause_LiteralAtom.exit.i.preheader
                                        #   in Loop: Header=BB38_3 Depth=1
	xorl	%r10d, %r10d
	movl	%ebp, %ecx
	movl	%ebp, %ebx
	jmp	.LBB38_6
	.p2align	4, 0x90
.LBB38_16:                              # %.critedge.i
                                        #   in Loop: Header=BB38_6 Depth=2
	cmpl	%ebx, %ebp
	je	.LBB38_14
# BB#17:                                #   in Loop: Header=BB38_6 Depth=2
	movq	(%rdx), %r15
	movq	8(%rdx), %r14
	movq	%r15, stack_STACK(,%rsi,8)
.LBB38_6:                               # %clause_LiteralAtom.exit.i
                                        #   Parent Loop BB38_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB38_11 Depth 3
	movq	16(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB38_8
# BB#7:                                 #   in Loop: Header=BB38_6 Depth=2
	leal	1(%rbx), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movl	%ebx, %edx
	movq	%rsi, stack_STACK(,%rdx,8)
	movl	%ecx, %ebx
	cmpl	%ebp, %ebx
	jne	.LBB38_11
	jmp	.LBB38_14
	.p2align	4, 0x90
.LBB38_8:                               #   in Loop: Header=BB38_6 Depth=2
	movl	(%r14), %edx
	testl	%edx, %edx
	jle	.LBB38_10
# BB#9:                                 #   in Loop: Header=BB38_6 Depth=2
	cmpl	%edx, %r10d
	cmovll	%edx, %r10d
.LBB38_10:                              # %.preheader.i
                                        #   in Loop: Header=BB38_6 Depth=2
	cmpl	%ebp, %ebx
	je	.LBB38_14
	.p2align	4, 0x90
.LBB38_11:                              # %.lr.ph.i
                                        #   Parent Loop BB38_3 Depth=1
                                        #     Parent Loop BB38_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rbx), %esi
	movq	stack_STACK(,%rsi,8), %rdx
	testq	%rdx, %rdx
	jne	.LBB38_16
# BB#12:                                #   in Loop: Header=BB38_11 Depth=3
	movl	%esi, stack_POINTER(%rip)
	cmpl	%esi, %ebp
	movl	%esi, %ecx
	movl	%esi, %ebx
	jne	.LBB38_11
# BB#13:                                #   in Loop: Header=BB38_3 Depth=1
	movl	%ebp, %ecx
.LBB38_14:                              # %clause_LiteralMaxVar.exit
                                        #   in Loop: Header=BB38_3 Depth=1
	cmpl	%eax, %r10d
	cmovgel	%r10d, %eax
	incq	%r11
	cmpq	%r9, %r11
	movl	%ecx, %ebp
	jne	.LBB38_3
	jmp	.LBB38_15
.LBB38_1:
	xorl	%eax, %eax
.LBB38_15:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end38:
	.size	clause_SearchMaxVar, .Lfunc_end38-clause_SearchMaxVar
	.cfi_endproc

	.globl	clause_RenameVarsBiggerThan
	.p2align	4, 0x90
	.type	clause_RenameVarsBiggerThan,@function
clause_RenameVarsBiggerThan:            # @clause_RenameVarsBiggerThan
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi256:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi257:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi258:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi259:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi260:
	.cfi_def_cfa_offset 48
.Lcfi261:
	.cfi_offset %rbx, -40
.Lcfi262:
	.cfi_offset %r14, -32
.Lcfi263:
	.cfi_offset %r15, -24
.Lcfi264:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testl	%esi, %esi
	je	.LBB39_4
# BB#1:
	movl	68(%r14), %ebx
	movl	72(%r14), %ebp
	addl	64(%r14), %ebx
	movl	%esi, %edi
	callq	term_StartMaxRenaming
	addl	%ebp, %ebx
	jle	.LBB39_4
# BB#2:                                 # %.lr.ph
	movl	%ebx, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB39_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Rename
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB39_3
.LBB39_4:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end39:
	.size	clause_RenameVarsBiggerThan, .Lfunc_end39-clause_RenameVarsBiggerThan
	.cfi_endproc

	.globl	clause_Normalize
	.p2align	4, 0x90
	.type	clause_Normalize,@function
clause_Normalize:                       # @clause_Normalize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi265:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi266:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi267:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi268:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi269:
	.cfi_def_cfa_offset 48
.Lcfi270:
	.cfi_offset %rbx, -40
.Lcfi271:
	.cfi_offset %r14, -32
.Lcfi272:
	.cfi_offset %r15, -24
.Lcfi273:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	68(%r14), %ebx
	movl	72(%r14), %ebp
	addl	64(%r14), %ebx
	callq	term_StartMinRenaming
	addl	%ebp, %ebx
	jle	.LBB40_3
# BB#1:                                 # %.lr.ph
	movl	%ebx, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB40_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Rename
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB40_2
.LBB40_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end40:
	.size	clause_Normalize, .Lfunc_end40-clause_Normalize
	.cfi_endproc

	.globl	clause_SubstApply
	.p2align	4, 0x90
	.type	clause_SubstApply,@function
clause_SubstApply:                      # @clause_SubstApply
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi274:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi275:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi276:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi277:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi278:
	.cfi_def_cfa_offset 48
.Lcfi279:
	.cfi_offset %rbx, -48
.Lcfi280:
	.cfi_offset %r12, -40
.Lcfi281:
	.cfi_offset %r13, -32
.Lcfi282:
	.cfi_offset %r14, -24
.Lcfi283:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	68(%r12), %eax
	addl	64(%r12), %eax
	addl	72(%r12), %eax
	jle	.LBB41_7
# BB#1:                                 # %.lr.ph
	movl	%eax, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB41_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%r13,8), %rbx
	movq	24(%rbx), %rsi
	addq	$24, %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB41_4
# BB#3:                                 #   in Loop: Header=BB41_2 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB41_4:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	%r14, %rdi
	callq	subst_Apply
	movq	(%rbx), %rcx
	movl	fol_NOT(%rip), %edx
	cmpl	(%rcx), %edx
	jne	.LBB41_6
# BB#5:                                 #   in Loop: Header=BB41_2 Depth=1
	movq	16(%rcx), %rbx
	addq	$8, %rbx
.LBB41_6:                               # %clause_LiteralSetAtom.exit
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	%rax, (%rbx)
	incq	%r13
	cmpq	%r13, %r15
	jne	.LBB41_2
.LBB41_7:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end41:
	.size	clause_SubstApply, .Lfunc_end41-clause_SubstApply
	.cfi_endproc

	.globl	clause_ReplaceVariable
	.p2align	4, 0x90
	.type	clause_ReplaceVariable,@function
clause_ReplaceVariable:                 # @clause_ReplaceVariable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi284:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi285:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi286:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi287:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi288:
	.cfi_def_cfa_offset 48
.Lcfi289:
	.cfi_offset %rbx, -48
.Lcfi290:
	.cfi_offset %r12, -40
.Lcfi291:
	.cfi_offset %r14, -32
.Lcfi292:
	.cfi_offset %r15, -24
.Lcfi293:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r15d
	movq	%rdi, %r12
	movl	68(%r12), %ebp
	addl	64(%r12), %ebp
	addl	72(%r12), %ebp
	movl	%ebp, %eax
	decl	%eax
	js	.LBB42_5
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB42_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB42_4
# BB#3:                                 #   in Loop: Header=BB42_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB42_4:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB42_2 Depth=1
	movl	%r15d, %esi
	movq	%r14, %rdx
	callq	term_ReplaceVariable
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB42_2
.LBB42_5:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end42:
	.size	clause_ReplaceVariable, .Lfunc_end42-clause_ReplaceVariable
	.cfi_endproc

	.globl	clause_UpdateMaxVar
	.p2align	4, 0x90
	.type	clause_UpdateMaxVar,@function
clause_UpdateMaxVar:                    # @clause_UpdateMaxVar
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi294:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi295:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi296:
	.cfi_def_cfa_offset 32
.Lcfi297:
	.cfi_offset %rbx, -32
.Lcfi298:
	.cfi_offset %r14, -24
.Lcfi299:
	.cfi_offset %r15, -16
	movl	68(%rdi), %eax
	addl	64(%rdi), %eax
	addl	72(%rdi), %eax
	jle	.LBB43_1
# BB#2:                                 # %.lr.ph.i
	movl	stack_POINTER(%rip), %ecx
	movl	fol_NOT(%rip), %r8d
	movl	%eax, %r9d
	xorl	%r14d, %r14d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB43_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB43_6 Depth 2
                                        #       Child Loop BB43_11 Depth 3
	movq	56(%rdi), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rbx
	cmpl	(%rbx), %r8d
	jne	.LBB43_5
# BB#4:                                 #   in Loop: Header=BB43_3 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB43_5:                               # %clause_LiteralAtom.exit.i.i.preheader
                                        #   in Loop: Header=BB43_3 Depth=1
	xorl	%r10d, %r10d
	movl	%ecx, %eax
	movl	%ecx, %esi
	jmp	.LBB43_6
	.p2align	4, 0x90
.LBB43_16:                              # %.critedge.i.i
                                        #   in Loop: Header=BB43_6 Depth=2
	cmpl	%esi, %ecx
	je	.LBB43_14
# BB#17:                                #   in Loop: Header=BB43_6 Depth=2
	movq	(%rbx), %r15
	movq	8(%rbx), %rbx
	movq	%r15, stack_STACK(,%rdx,8)
.LBB43_6:                               # %clause_LiteralAtom.exit.i.i
                                        #   Parent Loop BB43_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB43_11 Depth 3
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.LBB43_8
# BB#7:                                 #   in Loop: Header=BB43_6 Depth=2
	leal	1(%rsi), %eax
	movl	%eax, stack_POINTER(%rip)
	movl	%esi, %esi
	movq	%rdx, stack_STACK(,%rsi,8)
	movl	%eax, %esi
	cmpl	%ecx, %esi
	jne	.LBB43_11
	jmp	.LBB43_14
	.p2align	4, 0x90
.LBB43_8:                               #   in Loop: Header=BB43_6 Depth=2
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.LBB43_10
# BB#9:                                 #   in Loop: Header=BB43_6 Depth=2
	cmpl	%edx, %r10d
	cmovll	%edx, %r10d
.LBB43_10:                              # %.preheader.i.i
                                        #   in Loop: Header=BB43_6 Depth=2
	cmpl	%ecx, %esi
	je	.LBB43_14
	.p2align	4, 0x90
.LBB43_11:                              # %.lr.ph.i.i
                                        #   Parent Loop BB43_3 Depth=1
                                        #     Parent Loop BB43_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rsi), %edx
	movq	stack_STACK(,%rdx,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB43_16
# BB#12:                                #   in Loop: Header=BB43_11 Depth=3
	movl	%edx, stack_POINTER(%rip)
	cmpl	%edx, %ecx
	movl	%edx, %eax
	movl	%edx, %esi
	jne	.LBB43_11
# BB#13:                                #   in Loop: Header=BB43_3 Depth=1
	movl	%ecx, %eax
.LBB43_14:                              # %clause_LiteralMaxVar.exit.i
                                        #   in Loop: Header=BB43_3 Depth=1
	cmpl	%r11d, %r10d
	cmovgel	%r10d, %r11d
	incq	%r14
	cmpq	%r9, %r14
	movl	%eax, %ecx
	jne	.LBB43_3
	jmp	.LBB43_15
.LBB43_1:
	xorl	%r11d, %r11d
.LBB43_15:                              # %clause_SearchMaxVar.exit
	movl	%r11d, 52(%rdi)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end43:
	.size	clause_UpdateMaxVar, .Lfunc_end43-clause_UpdateMaxVar
	.cfi_endproc

	.globl	clause_OrientEqualities
	.p2align	4, 0x90
	.type	clause_OrientEqualities,@function
clause_OrientEqualities:                # @clause_OrientEqualities
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi300:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi301:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi302:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi303:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi304:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi305:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi306:
	.cfi_def_cfa_offset 64
.Lcfi307:
	.cfi_offset %rbx, -56
.Lcfi308:
	.cfi_offset %r12, -48
.Lcfi309:
	.cfi_offset %r13, -40
.Lcfi310:
	.cfi_offset %r14, -32
.Lcfi311:
	.cfi_offset %r15, -24
.Lcfi312:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	jle	.LBB44_14
# BB#1:                                 # %.lr.ph
	movl	%eax, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB44_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movq	(%rax,%rbx,8), %rbp
	movq	24(%rbp), %rax
	movl	(%rax), %edx
	movl	fol_NOT(%rip), %esi
	cmpl	%edx, %esi
	movl	%edx, %edi
	jne	.LBB44_4
# BB#3:                                 #   in Loop: Header=BB44_2 Depth=1
	movq	16(%rax), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %edi
.LBB44_4:                               # %clause_LiteralIsEquality.exit
                                        #   in Loop: Header=BB44_2 Depth=1
	xorl	%ecx, %ecx
	cmpl	%edi, fol_EQUALITY(%rip)
	jne	.LBB44_13
# BB#5:                                 #   in Loop: Header=BB44_2 Depth=1
	movq	16(%rax), %rcx
	movq	8(%rcx), %rdi
	cmpl	%edx, %esi
	jne	.LBB44_7
# BB#6:                                 #   in Loop: Header=BB44_2 Depth=1
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rdi
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB44_7:                               # %clause_LiteralAtom.exit38
                                        #   in Loop: Header=BB44_2 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	ord_Compare
	movl	$1, %ecx
	cmpl	$3, %eax
	je	.LBB44_13
# BB#8:                                 # %clause_LiteralAtom.exit38
                                        #   in Loop: Header=BB44_2 Depth=1
	cmpl	$1, %eax
	jne	.LBB44_12
# BB#9:                                 #   in Loop: Header=BB44_2 Depth=1
	movq	24(%rbp), %rax
	movl	fol_NOT(%rip), %edx
	cmpl	(%rax), %edx
	jne	.LBB44_11
# BB#10:                                #   in Loop: Header=BB44_2 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB44_11:                              # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB44_2 Depth=1
	movq	16(%rax), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %rdi
	movq	8(%rsi), %rsi
	movq	%rsi, 8(%rdx)
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	%rdi, 8(%rax)
	jmp	.LBB44_13
.LBB44_12:                              #   in Loop: Header=BB44_2 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB44_13:                              #   in Loop: Header=BB44_2 Depth=1
	movl	%ecx, 8(%rbp)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB44_2
.LBB44_14:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end44:
	.size	clause_OrientEqualities, .Lfunc_end44-clause_OrientEqualities
	.cfi_endproc

	.globl	clause_InsertFlatIntoIndex
	.p2align	4, 0x90
	.type	clause_InsertFlatIntoIndex,@function
clause_InsertFlatIntoIndex:             # @clause_InsertFlatIntoIndex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi313:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi314:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi315:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi316:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi317:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi318:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi319:
	.cfi_def_cfa_offset 64
.Lcfi320:
	.cfi_offset %rbx, -56
.Lcfi321:
	.cfi_offset %r12, -48
.Lcfi322:
	.cfi_offset %r13, -40
.Lcfi323:
	.cfi_offset %r14, -32
.Lcfi324:
	.cfi_offset %r15, -24
.Lcfi325:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	addl	72(%r15), %eax
	jle	.LBB45_5
# BB#1:                                 # %.lr.ph
	movl	%eax, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB45_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%r13,8), %rbp
	movq	24(%rbp), %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB45_4
# BB#3:                                 #   in Loop: Header=BB45_2 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB45_4:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB45_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 8(%rbx)
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rbx, %rdx
	callq	st_EntryCreate
	incq	%r13
	cmpq	%r13, %r12
	jne	.LBB45_2
.LBB45_5:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end45:
	.size	clause_InsertFlatIntoIndex, .Lfunc_end45-clause_InsertFlatIntoIndex
	.cfi_endproc

	.globl	clause_DeleteFlatFromIndex
	.p2align	4, 0x90
	.type	clause_DeleteFlatFromIndex,@function
clause_DeleteFlatFromIndex:             # @clause_DeleteFlatFromIndex
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi326:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi327:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi328:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi329:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi330:
	.cfi_def_cfa_offset 48
.Lcfi331:
	.cfi_offset %rbx, -40
.Lcfi332:
	.cfi_offset %r12, -32
.Lcfi333:
	.cfi_offset %r14, -24
.Lcfi334:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	68(%r12), %eax
	addl	64(%r12), %eax
	addl	72(%r12), %eax
	jle	.LBB46_7
# BB#1:                                 # %.lr.ph
	movl	%eax, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB46_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB46_5 Depth 2
	movq	56(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB46_4
# BB#3:                                 #   in Loop: Header=BB46_2 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB46_4:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB46_2 Depth=1
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.LBB46_6
	.p2align	4, 0x90
.LBB46_5:                               # %.lr.ph.i
                                        #   Parent Loop BB46_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB46_5
.LBB46_6:                               # %list_Delete.exit
                                        #   in Loop: Header=BB46_2 Depth=1
	movq	$0, 8(%rsi)
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r14, %rdi
	movq	%rsi, %rdx
	callq	st_EntryDelete
	incq	%rbx
	cmpq	%r15, %rbx
	jne	.LBB46_2
.LBB46_7:                               # %._crit_edge
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	clause_Delete           # TAILCALL
.Lfunc_end46:
	.size	clause_DeleteFlatFromIndex, .Lfunc_end46-clause_DeleteFlatFromIndex
	.cfi_endproc

	.globl	clause_Delete
	.p2align	4, 0x90
	.type	clause_Delete,@function
clause_Delete:                          # @clause_Delete
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi335:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi336:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi337:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi338:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi339:
	.cfi_def_cfa_offset 48
.Lcfi340:
	.cfi_offset %rbx, -40
.Lcfi341:
	.cfi_offset %r12, -32
.Lcfi342:
	.cfi_offset %r14, -24
.Lcfi343:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	64(%r14), %eax
	movl	68(%r14), %ecx
	movl	72(%r14), %edx
	leal	(%rcx,%rax), %esi
	addl	%edx, %esi
	jle	.LBB47_4
# BB#1:                                 # %.lr.ph
	movl	%esi, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB47_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %r12
	movq	24(%r12), %rdi
	callq	term_Delete
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r12, (%rax)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB47_2
# BB#3:                                 # %._crit_edge.loopexit
	movl	64(%r14), %eax
	movl	68(%r14), %ecx
	movl	72(%r14), %edx
.LBB47_4:                               # %._crit_edge
	addl	%eax, %ecx
	addl	%edx, %ecx
	je	.LBB47_12
# BB#5:
	movq	56(%r14), %rdi
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB47_6
# BB#11:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB47_12
.LBB47_6:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB47_8
# BB#7:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB47_8:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB47_10
# BB#9:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB47_10:
	addq	$-16, %rdi
	callq	free
.LBB47_12:                              # %clause_FreeLitArray.exit
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB47_14
	.p2align	4, 0x90
.LBB47_13:                              # %.lr.ph.i22
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB47_13
.LBB47_14:                              # %list_Delete.exit23
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.LBB47_16
	.p2align	4, 0x90
.LBB47_15:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB47_15
.LBB47_16:                              # %list_Delete.exit
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB47_24
# BB#17:
	movl	24(%r14), %ecx
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB47_18
# BB#23:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB47_24
.LBB47_18:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB47_20
# BB#19:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB47_20:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB47_22
# BB#21:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB47_22:
	addq	$-16, %rdi
	callq	free
.LBB47_24:                              # %memory_Free.exit
	movq	memory_ARRAY+640(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+640(%rip), %rax
	movq	%r14, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end47:
	.size	clause_Delete, .Lfunc_end47-clause_Delete
	.cfi_endproc

	.globl	clause_DeleteClauseListFlatFromIndex
	.p2align	4, 0x90
	.type	clause_DeleteClauseListFlatFromIndex,@function
clause_DeleteClauseListFlatFromIndex:   # @clause_DeleteClauseListFlatFromIndex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi344:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi345:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi346:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi347:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi348:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi349:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi350:
	.cfi_def_cfa_offset 64
.Lcfi351:
	.cfi_offset %rbx, -56
.Lcfi352:
	.cfi_offset %r12, -48
.Lcfi353:
	.cfi_offset %r13, -40
.Lcfi354:
	.cfi_offset %r14, -32
.Lcfi355:
	.cfi_offset %r15, -24
.Lcfi356:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB48_11
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB48_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB48_4 Depth 2
                                        #       Child Loop BB48_7 Depth 3
	movq	8(%r12), %rbx
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	addl	72(%rbx), %eax
	jle	.LBB48_9
# BB#3:                                 # %.lr.ph.i8
                                        #   in Loop: Header=BB48_2 Depth=1
	movl	%eax, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB48_4:                               #   Parent Loop BB48_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB48_7 Depth 3
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB48_6
# BB#5:                                 #   in Loop: Header=BB48_4 Depth=2
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB48_6:                               # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB48_4 Depth=2
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.LBB48_8
	.p2align	4, 0x90
.LBB48_7:                               # %.lr.ph.i.i
                                        #   Parent Loop BB48_2 Depth=1
                                        #     Parent Loop BB48_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB48_7
.LBB48_8:                               # %list_Delete.exit.i
                                        #   in Loop: Header=BB48_4 Depth=2
	movq	$0, 8(%rsi)
	movq	cont_LEFTCONTEXT(%rip), %rcx
	movq	%r15, %rdi
	movq	%rsi, %rdx
	callq	st_EntryDelete
	incq	%rbp
	cmpq	%r13, %rbp
	jne	.LBB48_4
.LBB48_9:                               # %clause_DeleteFlatFromIndex.exit
                                        #   in Loop: Header=BB48_2 Depth=1
	movq	%rbx, %rdi
	callq	clause_Delete
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB48_2
	.p2align	4, 0x90
.LBB48_10:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB48_10
.LBB48_11:                              # %list_Delete.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end48:
	.size	clause_DeleteClauseListFlatFromIndex, .Lfunc_end48-clause_DeleteClauseListFlatFromIndex
	.cfi_endproc

	.globl	clause_MoveBestLiteralToFront
	.p2align	4, 0x90
	.type	clause_MoveBestLiteralToFront,@function
clause_MoveBestLiteralToFront:          # @clause_MoveBestLiteralToFront
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi357:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi358:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi359:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi360:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi361:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi362:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi363:
	.cfi_def_cfa_offset 80
.Lcfi364:
	.cfi_offset %rbx, -56
.Lcfi365:
	.cfi_offset %r12, -48
.Lcfi366:
	.cfi_offset %r13, -40
.Lcfi367:
	.cfi_offset %r14, -32
.Lcfi368:
	.cfi_offset %r15, -24
.Lcfi369:
	.cfi_offset %rbp, -16
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r13d
	movq	%rsi, %rbp
	testq	%rdi, %rdi
	je	.LBB49_25
# BB#1:
	cmpq	$0, (%rdi)
	je	.LBB49_25
# BB#2:
	testq	%rbp, %rbp
	movq	%rdi, (%rsp)            # 8-byte Spill
	je	.LBB49_3
# BB#4:
	leal	4(,%r13,4), %r14d
	movl	%r14d, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movl	%r13d, %eax
	leaq	(%rbx,%rax,4), %rax
	.p2align	4, 0x90
.LBB49_5:                               # %.lr.ph17.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rax)
	addq	$-4, %rax
	cmpq	%rbx, %rax
	jae	.LBB49_5
	.p2align	4, 0x90
.LBB49_6:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rdi
	callq	term_ComputeSize
	movslq	8(%rbp), %rcx
	movl	%eax, (%rbx,%rcx,4)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB49_6
# BB#7:
	movq	%rbx, %r13
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB49_8
.LBB49_3:
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
.LBB49_8:                               # %clause_VarToSizeMap.exit
	movq	8(%rdi), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	movq	%r14, 8(%rsp)           # 8-byte Spill
	jne	.LBB49_10
# BB#9:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB49_10:                              # %clause_LiteralAtom.exit70
	movq	%r13, %rsi
	callq	clause_ComputeTermSize
	movl	%eax, %ebp
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.LBB49_17
# BB#11:                                # %.lr.ph.preheader
	movq	%rdi, %r14
	.p2align	4, 0x90
.LBB49_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB49_14
# BB#13:                                #   in Loop: Header=BB49_12 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB49_14:                              # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB49_12 Depth=1
	movq	%r13, %rsi
	callq	clause_ComputeTermSize
	movl	%eax, %r15d
	movq	8(%r12), %rdi
	movq	8(%r14), %rdx
	movl	%r15d, %esi
	movl	%ebp, %ecx
	callq	*16(%rsp)               # 8-byte Folded Reload
	testl	%eax, %eax
	cmovnel	%r15d, %ebp
	cmovneq	%r12, %r14
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB49_12
# BB#15:                                # %._crit_edge
	movq	(%rsp), %rdi            # 8-byte Reload
	cmpq	%rdi, %r14
	je	.LBB49_17
# BB#16:
	movq	8(%rdi), %rax
	movq	8(%r14), %rcx
	movq	%rcx, 8(%rdi)
	movq	%rax, 8(%r14)
.LBB49_17:                              # %._crit_edge.thread
	testq	%r13, %r13
	je	.LBB49_25
# BB#18:
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpl	$1024, %esi             # imm = 0x400
	jae	.LBB49_19
# BB#24:
	movl	%esi, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rbx, (%rax)
	jmp	.LBB49_25
.LBB49_19:
	movl	memory_ALIGN(%rip), %ecx
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%ecx
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%edx, %ecx
	addl	%esi, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	%rbx, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %rdi
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rsi, %rbp
	movq	%rdi, (%rbp)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB49_21
# BB#20:
	negq	%rax
	movq	-16(%rbx,%rax), %rax
	movq	%rax, (%rdx)
.LBB49_21:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB49_23
# BB#22:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB49_23:
	addq	$-16, %rbx
	movq	%rbx, %rdi
	callq	free
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB49_25:                              # %memory_Free.exit
	movq	%rdi, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end49:
	.size	clause_MoveBestLiteralToFront, .Lfunc_end49-clause_MoveBestLiteralToFront
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_ComputeTermSize,@function
clause_ComputeTermSize:                 # @clause_ComputeTermSize
	.cfi_startproc
# BB#0:
	movl	stack_POINTER(%rip), %ecx
	xorl	%eax, %eax
	movl	%ecx, %edx
	testq	%rsi, %rsi
	jne	.LBB50_1
	jmp	.LBB50_3
	.p2align	4, 0x90
.LBB50_15:                              #   in Loop: Header=BB50_14 Depth=2
	movl	%edi, stack_POINTER(%rip)
	cmpl	%edi, %ecx
	movl	%edi, %edx
	jne	.LBB50_14
	jmp	.LBB50_8
	.p2align	4, 0x90
.LBB50_16:                              # %.critedge
                                        #   in Loop: Header=BB50_1 Depth=1
	cmpl	%edx, %ecx
	je	.LBB50_8
# BB#17:                                #   in Loop: Header=BB50_1 Depth=1
	leal	-1(%rdx), %r8d
	movq	stack_STACK(,%r8,8), %rdi
	movq	(%rdi), %r9
	movq	8(%rdi), %rdi
	movq	%r9, stack_STACK(,%r8,8)
.LBB50_1:                               # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB50_14 Depth 2
	movslq	(%rdi), %r8
	testq	%r8, %r8
	jle	.LBB50_11
# BB#2:                                 #   in Loop: Header=BB50_1 Depth=1
	addl	(%rsi,%r8,4), %eax
	cmpl	%ecx, %edx
	jne	.LBB50_14
	jmp	.LBB50_8
	.p2align	4, 0x90
.LBB50_11:                              #   in Loop: Header=BB50_1 Depth=1
	incl	%eax
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB50_13
# BB#12:                                #   in Loop: Header=BB50_1 Depth=1
	movl	%edx, %r8d
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rdi, stack_STACK(,%r8,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
.LBB50_13:                              # %.preheader
                                        #   in Loop: Header=BB50_1 Depth=1
	cmpl	%ecx, %edx
	je	.LBB50_8
	.p2align	4, 0x90
.LBB50_14:                              # %.lr.ph
                                        #   Parent Loop BB50_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %edi
	cmpq	$0, stack_STACK(,%rdi,8)
	je	.LBB50_15
	jmp	.LBB50_16
	.p2align	4, 0x90
.LBB50_7:                               #   in Loop: Header=BB50_6 Depth=2
	movl	%esi, stack_POINTER(%rip)
	cmpl	%esi, %ecx
	movl	%esi, %edx
	jne	.LBB50_6
	jmp	.LBB50_8
	.p2align	4, 0x90
.LBB50_9:                               # %.critedge.us
                                        #   in Loop: Header=BB50_3 Depth=1
	cmpl	%edx, %ecx
	je	.LBB50_8
# BB#10:                                #   in Loop: Header=BB50_3 Depth=1
	leal	-1(%rdx), %esi
	movq	stack_STACK(,%rsi,8), %rdi
	movq	(%rdi), %r8
	movq	8(%rdi), %rdi
	movq	%r8, stack_STACK(,%rsi,8)
.LBB50_3:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB50_6 Depth 2
	movq	16(%rdi), %rsi
	testq	%rsi, %rsi
	je	.LBB50_5
# BB#4:                                 #   in Loop: Header=BB50_3 Depth=1
	movl	%edx, %edi
	leal	1(%rdx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rsi, stack_STACK(,%rdi,8)
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
.LBB50_5:                               # %.preheader.us
                                        #   in Loop: Header=BB50_3 Depth=1
	incl	%eax
	cmpl	%ecx, %edx
	je	.LBB50_8
	.p2align	4, 0x90
.LBB50_6:                               # %.lr.ph.us
                                        #   Parent Loop BB50_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rdx), %esi
	cmpq	$0, stack_STACK(,%rsi,8)
	je	.LBB50_7
	jmp	.LBB50_9
.LBB50_8:                               # %.us-lcssa.us
	retq
.Lfunc_end50:
	.size	clause_ComputeTermSize, .Lfunc_end50-clause_ComputeTermSize
	.cfi_endproc

	.globl	clause_LiteralPrint
	.p2align	4, 0x90
	.type	clause_LiteralPrint,@function
clause_LiteralPrint:                    # @clause_LiteralPrint
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	jmp	term_PrintPrefix        # TAILCALL
.Lfunc_end51:
	.size	clause_LiteralPrint, .Lfunc_end51-clause_LiteralPrint
	.cfi_endproc

	.globl	clause_LiteralListPrint
	.p2align	4, 0x90
	.type	clause_LiteralListPrint,@function
clause_LiteralListPrint:                # @clause_LiteralListPrint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi370:
	.cfi_def_cfa_offset 16
.Lcfi371:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB52_2
	jmp	.LBB52_3
	.p2align	4, 0x90
.LBB52_1:                               # %.backedge
                                        #   in Loop: Header=BB52_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
.LBB52_2:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rax
	movq	24(%rax), %rdi
	callq	term_PrintPrefix
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB52_1
.LBB52_3:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end52:
	.size	clause_LiteralListPrint, .Lfunc_end52-clause_LiteralListPrint
	.cfi_endproc

	.globl	clause_LiteralPrintUnsigned
	.p2align	4, 0x90
	.type	clause_LiteralPrintUnsigned,@function
clause_LiteralPrintUnsigned:            # @clause_LiteralPrintUnsigned
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB53_2
# BB#1:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB53_2:                               # %clause_LiteralAtom.exit
	pushq	%rax
.Lcfi372:
	.cfi_def_cfa_offset 16
	callq	term_PrintPrefix
	movq	stdout(%rip), %rdi
	popq	%rax
	jmp	fflush                  # TAILCALL
.Lfunc_end53:
	.size	clause_LiteralPrintUnsigned, .Lfunc_end53-clause_LiteralPrintUnsigned
	.cfi_endproc

	.globl	clause_LiteralPrintSigned
	.p2align	4, 0x90
	.type	clause_LiteralPrintSigned,@function
clause_LiteralPrintSigned:              # @clause_LiteralPrintSigned
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi373:
	.cfi_def_cfa_offset 16
	movq	24(%rdi), %rdi
	callq	term_PrintPrefix
	movq	stdout(%rip), %rdi
	popq	%rax
	jmp	fflush                  # TAILCALL
.Lfunc_end54:
	.size	clause_LiteralPrintSigned, .Lfunc_end54-clause_LiteralPrintSigned
	.cfi_endproc

	.globl	clause_LiteralFPrint
	.p2align	4, 0x90
	.type	clause_LiteralFPrint,@function
clause_LiteralFPrint:                   # @clause_LiteralFPrint
	.cfi_startproc
# BB#0:
	movq	24(%rsi), %rsi
	jmp	term_FPrintPrefix       # TAILCALL
.Lfunc_end55:
	.size	clause_LiteralFPrint, .Lfunc_end55-clause_LiteralFPrint
	.cfi_endproc

	.globl	clause_GetLiteralSubSetList
	.p2align	4, 0x90
	.type	clause_GetLiteralSubSetList,@function
clause_GetLiteralSubSetList:            # @clause_GetLiteralSubSetList
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi374:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi375:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi376:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi377:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi378:
	.cfi_def_cfa_offset 48
.Lcfi379:
	.cfi_offset %rbx, -48
.Lcfi380:
	.cfi_offset %r12, -40
.Lcfi381:
	.cfi_offset %r13, -32
.Lcfi382:
	.cfi_offset %r14, -24
.Lcfi383:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	cmpl	%edx, %esi
	jle	.LBB56_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB56_4
.LBB56_2:                               # %.lr.ph
	movslq	%esi, %rbx
	movslq	%edx, %r15
	decq	%rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB56_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r12, (%rax)
	incq	%rbx
	cmpq	%r15, %rbx
	movq	%rax, %r12
	jl	.LBB56_3
.LBB56_4:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end56:
	.size	clause_GetLiteralSubSetList, .Lfunc_end56-clause_GetLiteralSubSetList
	.cfi_endproc

	.globl	clause_ReplaceLiteralSubSet
	.p2align	4, 0x90
	.type	clause_ReplaceLiteralSubSet,@function
clause_ReplaceLiteralSubSet:            # @clause_ReplaceLiteralSubSet
	.cfi_startproc
# BB#0:
	cmpl	%edx, %esi
	jg	.LBB57_3
# BB#1:                                 # %.lr.ph
	movslq	%esi, %rax
	movslq	%edx, %r8
	decq	%rax
	.p2align	4, 0x90
.LBB57_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rsi
	movq	56(%rdi), %rdx
	movq	%rsi, 8(%rdx,%rax,8)
	movq	(%rcx), %rcx
	incq	%rax
	cmpq	%r8, %rax
	jl	.LBB57_2
.LBB57_3:                               # %._crit_edge
	retq
.Lfunc_end57:
	.size	clause_ReplaceLiteralSubSet, .Lfunc_end57-clause_ReplaceLiteralSubSet
	.cfi_endproc

	.globl	clause_FixLiteralOrder
	.p2align	4, 0x90
	.type	clause_FixLiteralOrder,@function
clause_FixLiteralOrder:                 # @clause_FixLiteralOrder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi384:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi385:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi386:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi387:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi388:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi389:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi390:
	.cfi_def_cfa_offset 64
.Lcfi391:
	.cfi_offset %rbx, -56
.Lcfi392:
	.cfi_offset %r12, -48
.Lcfi393:
	.cfi_offset %r13, -40
.Lcfi394:
	.cfi_offset %r14, -32
.Lcfi395:
	.cfi_offset %r15, -24
.Lcfi396:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	64(%r14), %r12
	movl	68(%r14), %eax
	leal	-1(%r12,%rax), %eax
	cmpl	%eax, %r12d
	jle	.LBB58_2
# BB#1:                                 # %clause_GetLiteralSubSetList.exit.thread.i
	xorl	%edi, %edi
	movl	$clause_LiteralsCompare, %esi
	callq	list_Sort
	testq	%rax, %rax
	jne	.LBB58_7
	jmp	.LBB58_8
.LBB58_2:                               # %.lr.ph.i.i
	movslq	%eax, %r15
	leaq	-1(%r12), %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB58_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	incq	%rbx
	cmpq	%r15, %rbx
	movq	%rax, %r13
	jl	.LBB58_3
# BB#4:                                 # %.lr.ph.i14.i
	movl	$clause_LiteralsCompare, %esi
	movq	%rax, %rdi
	callq	list_Sort
	decq	%r12
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB58_5:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	movq	56(%r14), %rsi
	movq	%rdx, 8(%rsi,%r12,8)
	movq	(%rcx), %rcx
	incq	%r12
	cmpq	%r15, %r12
	jl	.LBB58_5
# BB#6:                                 # %clause_ReplaceLiteralSubSet.exit.i
	testq	%rax, %rax
	je	.LBB58_8
	.p2align	4, 0x90
.LBB58_7:                               # %.lr.ph.i18.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB58_7
.LBB58_8:                               # %clause_FixLiteralSubsetOrder.exit
	movl	68(%r14), %eax
	movl	72(%r14), %ecx
	addl	64(%r14), %eax
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	jle	.LBB58_10
# BB#9:                                 # %clause_GetLiteralSubSetList.exit.thread.i26
	xorl	%edi, %edi
	movl	$clause_LiteralsCompare, %esi
	callq	list_Sort
	testq	%rax, %rax
	jne	.LBB58_15
	jmp	.LBB58_16
.LBB58_10:                              # %.lr.ph.i.i28
	movslq	%eax, %r12
	movslq	%ecx, %r15
	leaq	-1(%r12), %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB58_11:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	incq	%rbx
	cmpq	%r15, %rbx
	movq	%rax, %r13
	jl	.LBB58_11
# BB#12:                                # %.lr.ph.i14.i33
	movl	$clause_LiteralsCompare, %esi
	movq	%rax, %rdi
	callq	list_Sort
	decq	%r12
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB58_13:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	movq	56(%r14), %rsi
	movq	%rdx, 8(%rsi,%r12,8)
	movq	(%rcx), %rcx
	incq	%r12
	cmpq	%r15, %r12
	jl	.LBB58_13
# BB#14:                                # %clause_ReplaceLiteralSubSet.exit.i42
	testq	%rax, %rax
	je	.LBB58_16
	.p2align	4, 0x90
.LBB58_15:                              # %.lr.ph.i18.i46
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB58_15
.LBB58_16:                              # %clause_FixLiteralSubsetOrder.exit47
	movslq	64(%r14), %r15
	testq	%r15, %r15
	jle	.LBB58_17
# BB#18:                                # %.lr.ph.i.i50
	decq	%r15
	movq	$-1, %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB58_19:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	incq	%rbx
	cmpq	%r15, %rbx
	movq	%rax, %r12
	jl	.LBB58_19
# BB#20:                                # %.lr.ph.i14.i55
	movl	$clause_LiteralsCompare, %esi
	movq	%rax, %rdi
	callq	list_Sort
	movq	$-1, %rcx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB58_21:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rdx), %rsi
	movq	56(%r14), %rdi
	movq	%rsi, 8(%rdi,%rcx,8)
	movq	(%rdx), %rdx
	incq	%rcx
	cmpq	%r15, %rcx
	jl	.LBB58_21
	jmp	.LBB58_22
.LBB58_17:                              # %clause_GetLiteralSubSetList.exit.thread.i48
	xorl	%edi, %edi
	movl	$clause_LiteralsCompare, %esi
	callq	list_Sort
.LBB58_22:                              # %clause_ReplaceLiteralSubSet.exit.i64
	testq	%rax, %rax
	je	.LBB58_24
	.p2align	4, 0x90
.LBB58_23:                              # %.lr.ph.i18.i68
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB58_23
.LBB58_24:                              # %clause_FixLiteralSubsetOrder.exit69
	movl	68(%r14), %ebx
	movl	72(%r14), %ebp
	addl	64(%r14), %ebx
	callq	term_StartMinRenaming
	addl	%ebp, %ebx
	jle	.LBB58_27
# BB#25:                                # %.lr.ph.i
	movl	%ebx, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB58_26:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Rename
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB58_26
.LBB58_27:                              # %clause_Normalize.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end58:
	.size	clause_FixLiteralOrder, .Lfunc_end58-clause_FixLiteralOrder
	.cfi_endproc

	.globl	clause_CountSymbols
	.p2align	4, 0x90
	.type	clause_CountSymbols,@function
clause_CountSymbols:                    # @clause_CountSymbols
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi397:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi398:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi399:
	.cfi_def_cfa_offset 32
.Lcfi400:
	.cfi_offset %rbx, -32
.Lcfi401:
	.cfi_offset %r14, -24
.Lcfi402:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	64(%r14), %eax
	addl	68(%r14), %eax
	addl	72(%r14), %eax
	decl	%eax
	js	.LBB59_11
# BB#1:                                 # %.lr.ph
	movq	$-1, %rbx
	.p2align	4, 0x90
.LBB59_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	(%rdi), %ebp
	movl	fol_NOT(%rip), %eax
	cmpl	%ebp, %eax
	movl	%ebp, %ecx
	jne	.LBB59_4
# BB#3:                                 #   in Loop: Header=BB59_2 Depth=1
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB59_4:                               # %clause_LiteralIsPredicate.exit
                                        #   in Loop: Header=BB59_2 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	je	.LBB59_8
# BB#5:                                 #   in Loop: Header=BB59_2 Depth=1
	cmpl	%ebp, %eax
	jne	.LBB59_7
# BB#6:                                 #   in Loop: Header=BB59_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %ebp
.LBB59_7:                               # %clause_LiteralPredicate.exit
                                        #   in Loop: Header=BB59_2 Depth=1
	movl	%ebp, %edi
	callq	symbol_GetCount
	leaq	1(%rax), %rsi
	movl	%ebp, %edi
	callq	symbol_SetCount
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	(%rdi), %ebp
	movl	fol_NOT(%rip), %eax
.LBB59_8:                               #   in Loop: Header=BB59_2 Depth=1
	cmpl	%ebp, %eax
	jne	.LBB59_10
# BB#9:                                 #   in Loop: Header=BB59_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB59_10:                              # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB59_2 Depth=1
	callq	term_CountSymbols
	movl	64(%r14), %eax
	movl	72(%r14), %ecx
	addl	68(%r14), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB59_2
.LBB59_11:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end59:
	.size	clause_CountSymbols, .Lfunc_end59-clause_CountSymbols
	.cfi_endproc

	.globl	clause_ListOfPredicates
	.p2align	4, 0x90
	.type	clause_ListOfPredicates,@function
clause_ListOfPredicates:                # @clause_ListOfPredicates
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi403:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi404:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi405:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi406:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi407:
	.cfi_def_cfa_offset 48
.Lcfi408:
	.cfi_offset %rbx, -40
.Lcfi409:
	.cfi_offset %r12, -32
.Lcfi410:
	.cfi_offset %r14, -24
.Lcfi411:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	64(%r15), %r8d
	movl	68(%r15), %r9d
	movl	72(%r15), %esi
	leal	(%r8,%r9), %eax
	addl	%esi, %eax
	decl	%eax
	js	.LBB60_1
# BB#2:                                 # %.lr.ph
	movq	$-1, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB60_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	(%rdi), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %edx
	jne	.LBB60_5
# BB#4:                                 #   in Loop: Header=BB60_3 Depth=1
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
.LBB60_5:                               # %clause_LiteralIsPredicate.exit
                                        #   in Loop: Header=BB60_3 Depth=1
	cmpl	%edx, fol_EQUALITY(%rip)
	je	.LBB60_9
# BB#6:                                 #   in Loop: Header=BB60_3 Depth=1
	cmpl	%eax, %ecx
	jne	.LBB60_8
# BB#7:                                 #   in Loop: Header=BB60_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB60_8:                               # %clause_LiteralPredicate.exit
                                        #   in Loop: Header=BB60_3 Depth=1
	movslq	%eax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r14, (%rax)
	movl	64(%r15), %r8d
	movl	68(%r15), %r9d
	movl	72(%r15), %esi
	movq	%rax, %r14
.LBB60_9:                               #   in Loop: Header=BB60_3 Depth=1
	leal	(%r8,%r9), %eax
	leal	-1(%rsi,%rax), %eax
	cltq
	incq	%rbx
	cmpq	%rax, %rbx
	jl	.LBB60_3
	jmp	.LBB60_10
.LBB60_1:
	xorl	%r14d, %r14d
.LBB60_10:                              # %._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end60:
	.size	clause_ListOfPredicates, .Lfunc_end60-clause_ListOfPredicates
	.cfi_endproc

	.globl	clause_ListOfConstants
	.p2align	4, 0x90
	.type	clause_ListOfConstants,@function
clause_ListOfConstants:                 # @clause_ListOfConstants
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi412:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi413:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi414:
	.cfi_def_cfa_offset 32
.Lcfi415:
	.cfi_offset %rbx, -32
.Lcfi416:
	.cfi_offset %r14, -24
.Lcfi417:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	64(%r14), %eax
	addl	68(%r14), %eax
	addl	72(%r14), %eax
	decl	%eax
	js	.LBB61_1
# BB#2:                                 # %.lr.ph
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB61_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB61_9 Depth 2
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB61_5
# BB#4:                                 #   in Loop: Header=BB61_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB61_5:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB61_3 Depth=1
	callq	term_ListOfConstants
	testq	%rax, %rax
	je	.LBB61_6
# BB#7:                                 #   in Loop: Header=BB61_3 Depth=1
	testq	%r15, %r15
	je	.LBB61_11
# BB#8:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB61_3 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB61_9:                               # %.preheader.i
                                        #   Parent Loop BB61_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB61_9
# BB#10:                                #   in Loop: Header=BB61_3 Depth=1
	movq	%r15, (%rcx)
	jmp	.LBB61_11
	.p2align	4, 0x90
.LBB61_6:                               #   in Loop: Header=BB61_3 Depth=1
	movq	%r15, %rax
.LBB61_11:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB61_3 Depth=1
	movl	64(%r14), %ecx
	movl	72(%r14), %edx
	addl	68(%r14), %ecx
	leal	-1(%rdx,%rcx), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbx
	leaq	1(%rbx), %rbx
	movq	%rax, %r15
	jl	.LBB61_3
	jmp	.LBB61_12
.LBB61_1:
	xorl	%eax, %eax
.LBB61_12:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end61:
	.size	clause_ListOfConstants, .Lfunc_end61-clause_ListOfConstants
	.cfi_endproc

	.globl	clause_ListOfVariables
	.p2align	4, 0x90
	.type	clause_ListOfVariables,@function
clause_ListOfVariables:                 # @clause_ListOfVariables
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi418:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi419:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi420:
	.cfi_def_cfa_offset 32
.Lcfi421:
	.cfi_offset %rbx, -32
.Lcfi422:
	.cfi_offset %r14, -24
.Lcfi423:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	64(%r14), %eax
	addl	68(%r14), %eax
	addl	72(%r14), %eax
	decl	%eax
	js	.LBB62_1
# BB#2:                                 # %.lr.ph
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB62_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB62_9 Depth 2
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB62_5
# BB#4:                                 #   in Loop: Header=BB62_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB62_5:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB62_3 Depth=1
	callq	term_ListOfVariables
	testq	%rax, %rax
	je	.LBB62_6
# BB#7:                                 #   in Loop: Header=BB62_3 Depth=1
	testq	%r15, %r15
	je	.LBB62_11
# BB#8:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB62_3 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB62_9:                               # %.preheader.i
                                        #   Parent Loop BB62_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB62_9
# BB#10:                                #   in Loop: Header=BB62_3 Depth=1
	movq	%r15, (%rcx)
	jmp	.LBB62_11
	.p2align	4, 0x90
.LBB62_6:                               #   in Loop: Header=BB62_3 Depth=1
	movq	%r15, %rax
.LBB62_11:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB62_3 Depth=1
	movl	64(%r14), %ecx
	movl	72(%r14), %edx
	addl	68(%r14), %ecx
	leal	-1(%rdx,%rcx), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbx
	leaq	1(%rbx), %rbx
	movq	%rax, %r15
	jl	.LBB62_3
	jmp	.LBB62_12
.LBB62_1:
	xorl	%eax, %eax
.LBB62_12:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end62:
	.size	clause_ListOfVariables, .Lfunc_end62-clause_ListOfVariables
	.cfi_endproc

	.globl	clause_ListOfFunctions
	.p2align	4, 0x90
	.type	clause_ListOfFunctions,@function
clause_ListOfFunctions:                 # @clause_ListOfFunctions
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi424:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi425:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi426:
	.cfi_def_cfa_offset 32
.Lcfi427:
	.cfi_offset %rbx, -32
.Lcfi428:
	.cfi_offset %r14, -24
.Lcfi429:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	64(%r14), %eax
	addl	68(%r14), %eax
	addl	72(%r14), %eax
	decl	%eax
	js	.LBB63_1
# BB#2:                                 # %.lr.ph
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB63_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB63_9 Depth 2
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB63_5
# BB#4:                                 #   in Loop: Header=BB63_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB63_5:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB63_3 Depth=1
	callq	term_ListOfFunctions
	testq	%rax, %rax
	je	.LBB63_6
# BB#7:                                 #   in Loop: Header=BB63_3 Depth=1
	testq	%r15, %r15
	je	.LBB63_11
# BB#8:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB63_3 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB63_9:                               # %.preheader.i
                                        #   Parent Loop BB63_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB63_9
# BB#10:                                #   in Loop: Header=BB63_3 Depth=1
	movq	%r15, (%rcx)
	jmp	.LBB63_11
	.p2align	4, 0x90
.LBB63_6:                               #   in Loop: Header=BB63_3 Depth=1
	movq	%r15, %rax
.LBB63_11:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB63_3 Depth=1
	movl	64(%r14), %ecx
	movl	72(%r14), %edx
	addl	68(%r14), %ecx
	leal	-1(%rdx,%rcx), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rbx
	leaq	1(%rbx), %rbx
	movq	%rax, %r15
	jl	.LBB63_3
	jmp	.LBB63_12
.LBB63_1:
	xorl	%eax, %eax
.LBB63_12:                              # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end63:
	.size	clause_ListOfFunctions, .Lfunc_end63-clause_ListOfFunctions
	.cfi_endproc

	.globl	clause_CompareAbstract
	.p2align	4, 0x90
	.type	clause_CompareAbstract,@function
clause_CompareAbstract:                 # @clause_CompareAbstract
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi430:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi431:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi432:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi433:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi434:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi435:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi436:
	.cfi_def_cfa_offset 64
.Lcfi437:
	.cfi_offset %rbx, -56
.Lcfi438:
	.cfi_offset %r12, -48
.Lcfi439:
	.cfi_offset %r13, -40
.Lcfi440:
	.cfi_offset %r14, -32
.Lcfi441:
	.cfi_offset %r15, -24
.Lcfi442:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	4(%rbx), %ecx
	xorl	%eax, %eax
	cmpl	4(%r14), %ecx
	seta	%al
	movl	$-1, %ecx
	cmovbl	%ecx, %eax
	testl	%eax, %eax
	jne	.LBB64_120
# BB#1:
	movl	8(%r14), %eax
	cmpl	%eax, 8(%rbx)
	jae	.LBB64_3
# BB#2:
	movl	$-1, %eax
	jmp	.LBB64_120
.LBB64_3:
	movl	$1, %eax
	ja	.LBB64_120
# BB#4:                                 # %clause_CompareByDepth.exit
	movl	52(%r14), %eax
	cmpl	%eax, 52(%rbx)
	movl	$-1, %eax
	jl	.LBB64_120
# BB#5:
	movl	$1, %eax
	jg	.LBB64_120
# BB#6:                                 # %clause_CompareByMaxVar.exit
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	clause_CompareByClausePartSize
	testl	%eax, %eax
	jne	.LBB64_120
# BB#7:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	clause_CompareByLiterals
	testl	%eax, %eax
	jne	.LBB64_120
# BB#8:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	clause_CompareBySymbolOccurences
	testl	%eax, %eax
	jne	.LBB64_120
# BB#9:
	movl	64(%rbx), %r8d
	movl	68(%rbx), %edx
	movl	72(%rbx), %esi
	leal	(%r8,%rdx), %eax
	addl	%esi, %eax
	decl	%eax
	js	.LBB64_10
# BB#11:                                # %.lr.ph.i100
	movq	$-1, %r12
	xorl	%r15d, %r15d
.LBB64_12:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	8(%rax,%r12,8), %rax
	movq	24(%rax), %rdi
	movl	(%rdi), %eax
	movl	fol_NOT(%rip), %ebp
	cmpl	%eax, %ebp
	movl	%eax, %ecx
	jne	.LBB64_14
# BB#13:                                #   in Loop: Header=BB64_12 Depth=1
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB64_14:                              # %clause_LiteralIsPredicate.exit.i
                                        #   in Loop: Header=BB64_12 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	je	.LBB64_18
# BB#15:                                #   in Loop: Header=BB64_12 Depth=1
	cmpl	%eax, %ebp
	jne	.LBB64_17
# BB#16:                                #   in Loop: Header=BB64_12 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB64_17:                              # %clause_LiteralPredicate.exit.i
                                        #   in Loop: Header=BB64_12 Depth=1
	movslq	%eax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movl	64(%rbx), %r8d
	movl	68(%rbx), %edx
	movl	72(%rbx), %esi
	movq	%rax, %r15
.LBB64_18:                              #   in Loop: Header=BB64_12 Depth=1
	leal	(%rsi,%rdx), %eax
	leal	-1(%r8,%rax), %eax
	cltq
	incq	%r12
	cmpq	%rax, %r12
	jl	.LBB64_12
	jmp	.LBB64_19
.LBB64_10:
	xorl	%r15d, %r15d
.LBB64_19:                              # %clause_ListOfPredicates.exit
	movl	64(%r14), %r8d
	movl	68(%r14), %edx
	movl	72(%r14), %esi
	leal	(%r8,%rdx), %eax
	addl	%esi, %eax
	decl	%eax
	js	.LBB64_20
# BB#21:                                # %.lr.ph.i118
	movq	$-1, %r13
	xorl	%r12d, %r12d
.LBB64_22:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%r13,8), %rax
	movq	24(%rax), %rdi
	movl	(%rdi), %eax
	movl	fol_NOT(%rip), %ebp
	cmpl	%eax, %ebp
	movl	%eax, %ecx
	jne	.LBB64_24
# BB#23:                                #   in Loop: Header=BB64_22 Depth=1
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB64_24:                              # %clause_LiteralIsPredicate.exit.i136
                                        #   in Loop: Header=BB64_22 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	je	.LBB64_28
# BB#25:                                #   in Loop: Header=BB64_22 Depth=1
	cmpl	%eax, %ebp
	jne	.LBB64_27
# BB#26:                                #   in Loop: Header=BB64_22 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB64_27:                              # %clause_LiteralPredicate.exit.i147
                                        #   in Loop: Header=BB64_22 Depth=1
	movslq	%eax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movl	64(%r14), %r8d
	movl	68(%r14), %edx
	movl	72(%r14), %esi
	movq	%rax, %r12
.LBB64_28:                              #   in Loop: Header=BB64_22 Depth=1
	leal	(%rsi,%rdx), %eax
	leal	-1(%r8,%rax), %eax
	cltq
	incq	%r13
	cmpq	%rax, %r13
	jl	.LBB64_22
	jmp	.LBB64_29
.LBB64_20:
	xorl	%r12d, %r12d
.LBB64_29:                              # %clause_ListOfPredicates.exit154
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	list_CompareMultisetsByElementDistribution
	testq	%r15, %r15
	je	.LBB64_31
.LBB64_30:                              # %.lr.ph.i76
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r15)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r15, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r15
	jne	.LBB64_30
.LBB64_31:                              # %list_Delete.exit77
	testq	%r12, %r12
	je	.LBB64_33
.LBB64_32:                              # %.lr.ph.i71
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r12, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r12
	jne	.LBB64_32
.LBB64_33:                              # %list_Delete.exit72
	testl	%eax, %eax
	jne	.LBB64_120
# BB#34:
	movl	64(%rbx), %eax
	addl	68(%rbx), %eax
	addl	72(%rbx), %eax
	decl	%eax
	js	.LBB64_35
# BB#36:                                # %.lr.ph.i162
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
.LBB64_37:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_43 Depth 2
	movq	56(%rbx), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB64_39
# BB#38:                                #   in Loop: Header=BB64_37 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB64_39:                              # %clause_GetLiteralAtom.exit.i175
                                        #   in Loop: Header=BB64_37 Depth=1
	callq	term_ListOfConstants
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB64_40
# BB#41:                                #   in Loop: Header=BB64_37 Depth=1
	testq	%r13, %r13
	je	.LBB64_45
# BB#42:                                # %.preheader.i.i179.preheader
                                        #   in Loop: Header=BB64_37 Depth=1
	movq	%r15, %rcx
.LBB64_43:                              # %.preheader.i.i179
                                        #   Parent Loop BB64_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB64_43
# BB#44:                                #   in Loop: Header=BB64_37 Depth=1
	movq	%r13, (%rax)
	jmp	.LBB64_45
.LBB64_40:                              #   in Loop: Header=BB64_37 Depth=1
	movq	%r13, %r15
.LBB64_45:                              # %list_Nconc.exit.i185
                                        #   in Loop: Header=BB64_37 Depth=1
	movl	64(%rbx), %eax
	movl	72(%rbx), %ecx
	addl	68(%rbx), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	cmpq	%rax, %r12
	leaq	1(%r12), %r12
	movq	%r15, %r13
	jl	.LBB64_37
	jmp	.LBB64_46
.LBB64_35:
	xorl	%r15d, %r15d
.LBB64_46:                              # %clause_ListOfConstants.exit
	movl	64(%r14), %eax
	addl	68(%r14), %eax
	addl	72(%r14), %eax
	decl	%eax
	js	.LBB64_47
# BB#48:                                # %.lr.ph.i194
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
.LBB64_49:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_55 Depth 2
	movq	56(%r14), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB64_51
# BB#50:                                #   in Loop: Header=BB64_49 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB64_51:                              # %clause_GetLiteralAtom.exit.i207
                                        #   in Loop: Header=BB64_49 Depth=1
	callq	term_ListOfConstants
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB64_52
# BB#53:                                #   in Loop: Header=BB64_49 Depth=1
	testq	%rbp, %rbp
	je	.LBB64_57
# BB#54:                                # %.preheader.i.i211.preheader
                                        #   in Loop: Header=BB64_49 Depth=1
	movq	%r12, %rcx
.LBB64_55:                              # %.preheader.i.i211
                                        #   Parent Loop BB64_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB64_55
# BB#56:                                #   in Loop: Header=BB64_49 Depth=1
	movq	%rbp, (%rax)
	jmp	.LBB64_57
.LBB64_52:                              #   in Loop: Header=BB64_49 Depth=1
	movq	%rbp, %r12
.LBB64_57:                              # %list_Nconc.exit.i217
                                        #   in Loop: Header=BB64_49 Depth=1
	movl	64(%r14), %eax
	movl	72(%r14), %ecx
	addl	68(%r14), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	cmpq	%rax, %r13
	leaq	1(%r13), %r13
	movq	%r12, %rbp
	jl	.LBB64_49
	jmp	.LBB64_58
.LBB64_47:
	xorl	%r12d, %r12d
.LBB64_58:                              # %clause_ListOfConstants.exit219
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	list_CompareMultisetsByElementDistribution
	testq	%r15, %r15
	je	.LBB64_60
.LBB64_59:                              # %.lr.ph.i86
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r15)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r15, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r15
	jne	.LBB64_59
.LBB64_60:                              # %list_Delete.exit87
	testq	%r12, %r12
	je	.LBB64_62
.LBB64_61:                              # %.lr.ph.i81
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r12, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r12
	jne	.LBB64_61
.LBB64_62:                              # %list_Delete.exit82
	testl	%eax, %eax
	jne	.LBB64_120
# BB#63:
	movl	64(%rbx), %eax
	addl	68(%rbx), %eax
	addl	72(%rbx), %eax
	decl	%eax
	js	.LBB64_64
# BB#65:                                # %.lr.ph.i227
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
.LBB64_66:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_72 Depth 2
	movq	56(%rbx), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB64_68
# BB#67:                                #   in Loop: Header=BB64_66 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB64_68:                              # %clause_GetLiteralAtom.exit.i240
                                        #   in Loop: Header=BB64_66 Depth=1
	callq	term_ListOfFunctions
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB64_69
# BB#70:                                #   in Loop: Header=BB64_66 Depth=1
	testq	%rbp, %rbp
	je	.LBB64_74
# BB#71:                                # %.preheader.i.i244.preheader
                                        #   in Loop: Header=BB64_66 Depth=1
	movq	%r15, %rcx
.LBB64_72:                              # %.preheader.i.i244
                                        #   Parent Loop BB64_66 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB64_72
# BB#73:                                #   in Loop: Header=BB64_66 Depth=1
	movq	%rbp, (%rax)
	jmp	.LBB64_74
.LBB64_69:                              #   in Loop: Header=BB64_66 Depth=1
	movq	%rbp, %r15
.LBB64_74:                              # %list_Nconc.exit.i250
                                        #   in Loop: Header=BB64_66 Depth=1
	movl	64(%rbx), %eax
	movl	72(%rbx), %ecx
	addl	68(%rbx), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	cmpq	%rax, %r12
	leaq	1(%r12), %r12
	movq	%r15, %rbp
	jl	.LBB64_66
	jmp	.LBB64_75
.LBB64_64:
	xorl	%r15d, %r15d
.LBB64_75:                              # %clause_ListOfFunctions.exit
	movl	64(%r14), %eax
	addl	68(%r14), %eax
	addl	72(%r14), %eax
	decl	%eax
	js	.LBB64_76
# BB#77:                                # %.lr.ph.i259
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
.LBB64_78:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_84 Depth 2
	movq	56(%r14), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB64_80
# BB#79:                                #   in Loop: Header=BB64_78 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB64_80:                              # %clause_GetLiteralAtom.exit.i272
                                        #   in Loop: Header=BB64_78 Depth=1
	callq	term_ListOfFunctions
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB64_81
# BB#82:                                #   in Loop: Header=BB64_78 Depth=1
	testq	%rbp, %rbp
	je	.LBB64_86
# BB#83:                                # %.preheader.i.i276.preheader
                                        #   in Loop: Header=BB64_78 Depth=1
	movq	%r12, %rcx
.LBB64_84:                              # %.preheader.i.i276
                                        #   Parent Loop BB64_78 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB64_84
# BB#85:                                #   in Loop: Header=BB64_78 Depth=1
	movq	%rbp, (%rax)
	jmp	.LBB64_86
.LBB64_81:                              #   in Loop: Header=BB64_78 Depth=1
	movq	%rbp, %r12
.LBB64_86:                              # %list_Nconc.exit.i282
                                        #   in Loop: Header=BB64_78 Depth=1
	movl	64(%r14), %eax
	movl	72(%r14), %ecx
	addl	68(%r14), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	cmpq	%rax, %r13
	leaq	1(%r13), %r13
	movq	%r12, %rbp
	jl	.LBB64_78
	jmp	.LBB64_87
.LBB64_76:
	xorl	%r12d, %r12d
.LBB64_87:                              # %clause_ListOfFunctions.exit284
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	list_CompareMultisetsByElementDistribution
	testq	%r15, %r15
	je	.LBB64_89
.LBB64_88:                              # %.lr.ph.i96
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r15)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r15, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r15
	jne	.LBB64_88
.LBB64_89:                              # %list_Delete.exit97
	testq	%r12, %r12
	je	.LBB64_91
.LBB64_90:                              # %.lr.ph.i91
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r12, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r12
	jne	.LBB64_90
.LBB64_91:                              # %list_Delete.exit92
	testl	%eax, %eax
	jne	.LBB64_120
# BB#92:
	movl	64(%rbx), %eax
	addl	68(%rbx), %eax
	addl	72(%rbx), %eax
	decl	%eax
	js	.LBB64_93
# BB#94:                                # %.lr.ph.i42
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
.LBB64_95:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_101 Depth 2
	movq	56(%rbx), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB64_97
# BB#96:                                #   in Loop: Header=BB64_95 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB64_97:                              # %clause_GetLiteralAtom.exit.i55
                                        #   in Loop: Header=BB64_95 Depth=1
	callq	term_ListOfVariables
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB64_98
# BB#99:                                #   in Loop: Header=BB64_95 Depth=1
	testq	%rbp, %rbp
	je	.LBB64_103
# BB#100:                               # %.preheader.i.i59.preheader
                                        #   in Loop: Header=BB64_95 Depth=1
	movq	%r15, %rcx
.LBB64_101:                             # %.preheader.i.i59
                                        #   Parent Loop BB64_95 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB64_101
# BB#102:                               #   in Loop: Header=BB64_95 Depth=1
	movq	%rbp, (%rax)
	jmp	.LBB64_103
.LBB64_98:                              #   in Loop: Header=BB64_95 Depth=1
	movq	%rbp, %r15
.LBB64_103:                             # %list_Nconc.exit.i65
                                        #   in Loop: Header=BB64_95 Depth=1
	movl	64(%rbx), %eax
	movl	72(%rbx), %ecx
	addl	68(%rbx), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	cmpq	%rax, %r12
	leaq	1(%r12), %r12
	movq	%r15, %rbp
	jl	.LBB64_95
	jmp	.LBB64_104
.LBB64_93:
	xorl	%r15d, %r15d
.LBB64_104:                             # %clause_ListOfVariables.exit67
	movl	64(%r14), %eax
	addl	68(%r14), %eax
	addl	72(%r14), %eax
	decl	%eax
	js	.LBB64_105
# BB#106:                               # %.lr.ph.i23
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
.LBB64_107:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB64_113 Depth 2
	movq	56(%r14), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB64_109
# BB#108:                               #   in Loop: Header=BB64_107 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB64_109:                             # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB64_107 Depth=1
	callq	term_ListOfVariables
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB64_110
# BB#111:                               #   in Loop: Header=BB64_107 Depth=1
	testq	%rbp, %rbp
	je	.LBB64_115
# BB#112:                               # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB64_107 Depth=1
	movq	%rbx, %rcx
.LBB64_113:                             # %.preheader.i.i
                                        #   Parent Loop BB64_107 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB64_113
# BB#114:                               #   in Loop: Header=BB64_107 Depth=1
	movq	%rbp, (%rax)
	jmp	.LBB64_115
.LBB64_110:                             #   in Loop: Header=BB64_107 Depth=1
	movq	%rbp, %rbx
.LBB64_115:                             # %list_Nconc.exit.i
                                        #   in Loop: Header=BB64_107 Depth=1
	movl	64(%r14), %eax
	movl	72(%r14), %ecx
	addl	68(%r14), %eax
	leal	-1(%rcx,%rax), %eax
	cltq
	cmpq	%rax, %r12
	leaq	1(%r12), %r12
	movq	%rbx, %rbp
	jl	.LBB64_107
	jmp	.LBB64_116
.LBB64_105:
	xorl	%ebx, %ebx
.LBB64_116:                             # %clause_ListOfVariables.exit
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	list_CompareMultisetsByElementDistribution
	testq	%r15, %r15
	je	.LBB64_118
.LBB64_117:                             # %.lr.ph.i18
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r15)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r15, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r15
	jne	.LBB64_117
.LBB64_118:                             # %list_Delete.exit19
	testq	%rbx, %rbx
	je	.LBB64_120
.LBB64_119:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB64_119
.LBB64_120:                             # %clause_CompareByDepth.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end64:
	.size	clause_CompareAbstract, .Lfunc_end64-clause_CompareAbstract
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_CompareByClausePartSize,@function
clause_CompareByClausePartSize:         # @clause_CompareByClausePartSize
	.cfi_startproc
# BB#0:
	movl	68(%rsi), %eax
	cmpl	%eax, 68(%rdi)
	jge	.LBB65_2
# BB#1:
	movl	$-1, %eax
	retq
.LBB65_2:
	movl	$1, %eax
	jg	.LBB65_8
# BB#3:
	movl	72(%rsi), %ecx
	cmpl	%ecx, 72(%rdi)
	jge	.LBB65_5
# BB#4:
	movl	$-1, %eax
	retq
.LBB65_5:
	jg	.LBB65_8
# BB#6:
	movl	64(%rsi), %eax
	cmpl	%eax, 64(%rdi)
	movl	$-1, %eax
	jl	.LBB65_8
# BB#7:
	setg	%al
	movzbl	%al, %eax
.LBB65_8:
	retq
.Lfunc_end65:
	.size	clause_CompareByClausePartSize, .Lfunc_end65-clause_CompareByClausePartSize
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_CompareByLiterals,@function
clause_CompareByLiterals:               # @clause_CompareByLiterals
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi443:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi444:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi445:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi446:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi447:
	.cfi_def_cfa_offset 48
.Lcfi448:
	.cfi_offset %rbx, -40
.Lcfi449:
	.cfi_offset %r12, -32
.Lcfi450:
	.cfi_offset %r14, -24
.Lcfi451:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	64(%r15), %eax
	movl	72(%r15), %ecx
	addl	68(%r15), %eax
	leal	-1(%rcx,%rax), %r12d
	movl	64(%r14), %eax
	movl	72(%r14), %ecx
	addl	68(%r14), %eax
	leal	-1(%rcx,%rax), %ebx
	movl	%r12d, %eax
	orl	%ebx, %eax
	js	.LBB66_4
# BB#1:                                 # %.lr.ph
	movslq	%r12d, %r12
	movslq	%ebx, %rbx
	.p2align	4, 0x90
.LBB66_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rdi
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rsi
	callq	term_CompareAbstract
	testl	%eax, %eax
	jne	.LBB66_6
# BB#3:                                 #   in Loop: Header=BB66_2 Depth=1
	decq	%r12
	decq	%rbx
	movl	%r12d, %eax
	orl	%ebx, %eax
	jns	.LBB66_2
.LBB66_4:                               # %.loopexit
	movl	$1, %eax
	cmpl	%ebx, %r12d
	jg	.LBB66_6
# BB#5:
	xorl	%ecx, %ecx
	cmpl	%ebx, %r12d
	movl	$-1, %eax
	cmovgel	%ecx, %eax
.LBB66_6:                               # %.critedge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end66:
	.size	clause_CompareByLiterals, .Lfunc_end66-clause_CompareByLiterals
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_CompareBySymbolOccurences,@function
clause_CompareBySymbolOccurences:       # @clause_CompareBySymbolOccurences
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi452:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi453:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi454:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi455:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi456:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi457:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi458:
	.cfi_def_cfa_offset 64
.Lcfi459:
	.cfi_offset %rbx, -56
.Lcfi460:
	.cfi_offset %r12, -48
.Lcfi461:
	.cfi_offset %r13, -40
.Lcfi462:
	.cfi_offset %r14, -32
.Lcfi463:
	.cfi_offset %r15, -24
.Lcfi464:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movl	64(%rdi), %eax
	movl	72(%rdi), %ecx
	movq	%rdi, (%rsp)            # 8-byte Spill
	addl	68(%rdi), %eax
	leal	-1(%rcx,%rax), %esi
	movl	64(%r13), %ecx
	movl	72(%r13), %edx
	addl	68(%r13), %ecx
	leal	-1(%rdx,%rcx), %ecx
	xorl	%eax, %eax
	movl	%esi, %edx
	orl	%ecx, %edx
	js	.LBB67_24
# BB#1:                                 # %.lr.ph
	movslq	%esi, %rbx
	movslq	%ecx, %r12
	decq	%r12
	decq	%rbx
	.p2align	4, 0x90
.LBB67_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%rbx,8), %r14
	movq	56(%r13), %rax
	movq	8(%rax,%r12,8), %r15
	movq	24(%r14), %rdi
	movl	(%rdi), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %esi
	jne	.LBB67_4
# BB#3:                                 #   in Loop: Header=BB67_2 Depth=1
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %esi
.LBB67_4:                               # %clause_LiteralIsPredicate.exit119
                                        #   in Loop: Header=BB67_2 Depth=1
	movl	fol_EQUALITY(%rip), %edx
	cmpl	%esi, %edx
	je	.LBB67_21
# BB#5:                                 #   in Loop: Header=BB67_2 Depth=1
	movq	24(%r15), %rbp
	movl	(%rbp), %esi
	cmpl	%esi, %ecx
	jne	.LBB67_7
# BB#6:                                 #   in Loop: Header=BB67_2 Depth=1
	movq	16(%rbp), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB67_7:                               # %clause_LiteralIsPredicate.exit
                                        #   in Loop: Header=BB67_2 Depth=1
	cmpl	%esi, %edx
	je	.LBB67_21
# BB#8:                                 #   in Loop: Header=BB67_2 Depth=1
	movq	%r13, %rbp
	cmpl	%eax, %ecx
	jne	.LBB67_10
# BB#9:                                 #   in Loop: Header=BB67_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB67_10:                              # %clause_LiteralPredicate.exit103
                                        #   in Loop: Header=BB67_2 Depth=1
	movl	%eax, %edi
	callq	symbol_GetCount
	movq	%rax, %r13
	movq	24(%r15), %rax
	movl	(%rax), %edi
	cmpl	%edi, fol_NOT(%rip)
	jne	.LBB67_12
# BB#11:                                #   in Loop: Header=BB67_2 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edi
.LBB67_12:                              # %clause_LiteralPredicate.exit93
                                        #   in Loop: Header=BB67_2 Depth=1
	callq	symbol_GetCount
	cmpq	%rax, %r13
	jb	.LBB67_13
# BB#14:                                #   in Loop: Header=BB67_2 Depth=1
	movq	24(%r14), %rax
	movl	(%rax), %edi
	cmpl	%edi, fol_NOT(%rip)
	jne	.LBB67_16
# BB#15:                                #   in Loop: Header=BB67_2 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edi
.LBB67_16:                              # %clause_LiteralPredicate.exit83
                                        #   in Loop: Header=BB67_2 Depth=1
	callq	symbol_GetCount
	movq	%rax, %r13
	movq	24(%r15), %rax
	movl	(%rax), %edi
	cmpl	%edi, fol_NOT(%rip)
	jne	.LBB67_18
# BB#17:                                #   in Loop: Header=BB67_2 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edi
.LBB67_18:                              # %clause_LiteralPredicate.exit
                                        #   in Loop: Header=BB67_2 Depth=1
	callq	symbol_GetCount
	cmpq	%rax, %r13
	ja	.LBB67_19
# BB#20:                                # %clause_LiteralPredicate.exit._crit_edge
                                        #   in Loop: Header=BB67_2 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movq	%rbp, %r13
	movq	56(%r13), %rax
	movq	8(%rax,%r12,8), %r15
.LBB67_21:                              #   in Loop: Header=BB67_2 Depth=1
	movq	24(%r15), %rsi
	callq	term_CompareBySymbolOccurences
	testl	%eax, %eax
	jne	.LBB67_24
# BB#22:                                #   in Loop: Header=BB67_2 Depth=1
	movq	%r12, %rax
	decq	%rax
	movq	%rbx, %rcx
	decq	%rcx
	orl	%r12d, %ebx
	movq	%rcx, %rbx
	movq	%rax, %r12
	jns	.LBB67_2
# BB#23:
	xorl	%eax, %eax
	jmp	.LBB67_24
.LBB67_13:
	movl	$-1, %eax
	jmp	.LBB67_24
.LBB67_19:
	movl	$1, %eax
.LBB67_24:                              # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end67:
	.size	clause_CompareBySymbolOccurences, .Lfunc_end67-clause_CompareBySymbolOccurences
	.cfi_endproc

	.globl	clause_Init
	.p2align	4, 0x90
	.type	clause_Init,@function
clause_Init:                            # @clause_Init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi465:
	.cfi_def_cfa_offset 16
	movl	$1, clause_CLAUSECOUNTER(%rip)
	callq	term_GetStampID
	movl	%eax, clause_STAMPID(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, clause_SORT+144(%rip)
	movaps	%xmm0, clause_SORT+128(%rip)
	movaps	%xmm0, clause_SORT+112(%rip)
	movaps	%xmm0, clause_SORT+96(%rip)
	movaps	%xmm0, clause_SORT+80(%rip)
	movaps	%xmm0, clause_SORT+64(%rip)
	movaps	%xmm0, clause_SORT+48(%rip)
	movaps	%xmm0, clause_SORT+32(%rip)
	movaps	%xmm0, clause_SORT+16(%rip)
	movaps	%xmm0, clause_SORT(%rip)
	movq	$0, clause_SORT+160(%rip)
	popq	%rax
	retq
.Lfunc_end68:
	.size	clause_Init, .Lfunc_end68-clause_Init
	.cfi_endproc

	.globl	clause_CreateBody
	.p2align	4, 0x90
	.type	clause_CreateBody,@function
clause_CreateBody:                      # @clause_CreateBody
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi466:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi467:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi468:
	.cfi_def_cfa_offset 32
.Lcfi469:
	.cfi_offset %rbx, -24
.Lcfi470:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$80, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%rbx)
	movl	$0, 52(%rbx)
	movl	$0, 48(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movl	$0, 24(%rbx)
	movl	$-1, 4(%rbx)
	movl	$0, 64(%rbx)
	movl	$0, 68(%rbx)
	movl	$0, 72(%rbx)
	testl	%ebp, %ebp
	movups	%xmm0, 32(%rbx)
	je	.LBB69_2
# BB#1:
	shll	$3, %ebp
	movl	%ebp, %edi
	callq	memory_Malloc
	movq	%rax, 56(%rbx)
.LBB69_2:
	movl	$16, 76(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end69:
	.size	clause_CreateBody, .Lfunc_end69-clause_CreateBody
	.cfi_endproc

	.globl	clause_Create
	.p2align	4, 0x90
	.type	clause_Create,@function
clause_Create:                          # @clause_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi471:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi472:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi473:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi474:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi475:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi476:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi477:
	.cfi_def_cfa_offset 112
.Lcfi478:
	.cfi_offset %rbx, -56
.Lcfi479:
	.cfi_offset %r12, -48
.Lcfi480:
	.cfi_offset %r13, -40
.Lcfi481:
	.cfi_offset %r14, -32
.Lcfi482:
	.cfi_offset %r15, -24
.Lcfi483:
	.cfi_offset %rbp, -16
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	$80, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%r13)
	movl	$0, 48(%r13)
	movl	$0, 8(%r13)
	movl	$-1, 4(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12(%r13)
	movups	%xmm0, 32(%r13)
	movq	%r15, %rdi
	callq	list_Length
	movl	%eax, %r14d
	movl	%r14d, 64(%r13)
	movq	%r12, %rdi
	callq	list_Length
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%eax, 68(%r13)
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	list_Length
	movl	%eax, %ebx
	movl	%ebx, 72(%r13)
	testq	%r13, %r13
	je	.LBB70_3
# BB#1:
	movl	68(%r13), %eax
	orl	%ebx, %eax
	jne	.LBB70_3
# BB#2:                                 # %clause_IsEmptyClause.exit
	cmpl	$0, 64(%r13)
	je	.LBB70_4
.LBB70_3:                               # %clause_IsEmptyClause.exit.thread
	movq	(%rsp), %rax            # 8-byte Reload
	leal	(%rax,%r14), %edi
	addl	%ebx, %edi
	shll	$3, %edi
	callq	memory_Malloc
	movq	%rax, 56(%r13)
.LBB70_4:                               # %.preheader
	testl	%r14d, %r14d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	jle	.LBB70_5
# BB#6:                                 # %.lr.ph78
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%r14d, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB70_7:                               # =>This Inner Loop Header: Depth=1
	movl	fol_NOT(%rip), %r14d
	movq	8(%r15), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rbp, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r13, 16(%rax)
	movq	56(%r13), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	(%r15), %r15
	incq	%rbx
	cmpq	%rbx, 48(%rsp)          # 8-byte Folded Reload
	jne	.LBB70_7
# BB#8:
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB70_9
.LBB70_5:
	xorl	%eax, %eax
.LBB70_9:                               # %._crit_edge79
	movq	(%rsp), %rcx            # 8-byte Reload
	addl	%r14d, %ecx
	cmpl	%ecx, %eax
	jge	.LBB70_13
# BB#10:                                # %.lr.ph74
	movslq	%eax, %rbx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	%ecx, %r14d
	.p2align	4, 0x90
.LBB70_11:                              # =>This Inner Loop Header: Depth=1
	movl	fol_NOT(%rip), %r15d
	movq	8(%r12), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r15d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rbp, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r13, 16(%rax)
	movq	56(%r13), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	(%r12), %r12
	incq	%rbx
	cmpl	%ebx, %r14d
	jne	.LBB70_11
# BB#12:                                # %._crit_edge75.loopexit
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
.LBB70_13:                              # %._crit_edge75
	addl	%ecx, %ebx
	cmpl	%ebx, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	jge	.LBB70_16
# BB#14:                                # %.lr.ph
	movl	%ebx, %ecx
	movslq	%eax, %rbx
	movl	%ecx, %r14d
	.p2align	4, 0x90
.LBB70_15:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r15
	movl	$32, %edi
	callq	memory_Malloc
	movq	%r15, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r13, 16(%rax)
	movq	56(%r13), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	(%rbp), %rbp
	incq	%rbx
	cmpl	%ebx, %r14d
	jne	.LBB70_15
.LBB70_16:                              # %._crit_edge
	movq	%r13, %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	callq	clause_OrientEqualities
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	clause_ReInit
	movl	$16, 76(%r13)
	movq	%r13, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end70:
	.size	clause_Create, .Lfunc_end70-clause_Create
	.cfi_endproc

	.globl	clause_CreateCrude
	.p2align	4, 0x90
	.type	clause_CreateCrude,@function
clause_CreateCrude:                     # @clause_CreateCrude
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi484:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi485:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi486:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi487:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi488:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi489:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi490:
	.cfi_def_cfa_offset 96
.Lcfi491:
	.cfi_offset %rbx, -56
.Lcfi492:
	.cfi_offset %r12, -48
.Lcfi493:
	.cfi_offset %r13, -40
.Lcfi494:
	.cfi_offset %r14, -32
.Lcfi495:
	.cfi_offset %r15, -24
.Lcfi496:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movl	$80, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%r15)
	xorl	%eax, %eax
	testl	%r12d, %r12d
	setne	%al
	shll	$3, %eax
	movl	%eax, 48(%r15)
	movl	$0, 8(%r15)
	movl	$-1, 4(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12(%r15)
	movups	%xmm0, 32(%r15)
	movq	%rbp, %rdi
	callq	list_Length
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%eax, 64(%r15)
	movq	%r14, %rdi
	callq	list_Length
	movl	%eax, %ebx
	movl	%ebx, 68(%r15)
	movq	%r13, %rdi
	callq	list_Length
	movl	%eax, %r12d
	movl	%r12d, 72(%r15)
	testq	%r15, %r15
	je	.LBB71_3
# BB#1:
	movl	68(%r15), %eax
	orl	%r12d, %eax
	jne	.LBB71_3
# BB#2:                                 # %clause_IsEmptyClause.exit
	cmpl	$0, 64(%r15)
	je	.LBB71_4
.LBB71_3:                               # %clause_IsEmptyClause.exit.thread
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%rbx,%rax), %edi
	addl	%r12d, %edi
	shll	$3, %edi
	callq	memory_Malloc
	movq	%rax, 56(%r15)
.LBB71_4:                               # %.preheader
	movq	16(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movq	%rax, %rcx
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	jle	.LBB71_5
# BB#6:                                 # %.lr.ph79
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%rcx, %rbx
	movl	%ecx, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB71_7:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r13
	movl	$32, %edi
	callq	memory_Malloc
	movq	%r13, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r15, 16(%rax)
	movq	56(%r15), %rcx
	movq	%rax, (%rcx,%r12,8)
	movq	(%rbp), %rbp
	incq	%r12
	cmpq	%r12, 32(%rsp)          # 8-byte Folded Reload
	jne	.LBB71_7
# BB#8:
	movq	%rbx, %rcx
	movl	%ecx, %eax
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	4(%rsp), %r12d          # 4-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB71_9
.LBB71_5:
	xorl	%eax, %eax
.LBB71_9:                               # %._crit_edge80
	addl	%ecx, %ebx
	cmpl	%ebx, %eax
	jge	.LBB71_13
# BB#10:                                # %.lr.ph75
	movslq	%eax, %rbp
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	%ebx, %r12d
	.p2align	4, 0x90
.LBB71_11:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rbx
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rbx, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r15, 16(%rax)
	movq	56(%r15), %rcx
	movq	%rax, (%rcx,%rbp,8)
	movq	(%r14), %r14
	incq	%rbp
	cmpl	%ebp, %r12d
	jne	.LBB71_11
# BB#12:                                # %._crit_edge76.loopexit
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, %eax
	movl	4(%rsp), %r12d          # 4-byte Reload
.LBB71_13:                              # %._crit_edge76
	addl	%ebx, %r12d
	cmpl	%r12d, %eax
	jge	.LBB71_16
# BB#14:                                # %.lr.ph
	movslq	%eax, %rbx
	movl	%r12d, %r14d
	.p2align	4, 0x90
.LBB71_15:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rbp
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rbp, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r15, 16(%rax)
	movq	56(%r15), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	(%r13), %r13
	incq	%rbx
	cmpl	%ebx, %r14d
	jne	.LBB71_15
.LBB71_16:                              # %._crit_edge
	movl	$16, 76(%r15)
	movq	%r15, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end71:
	.size	clause_CreateCrude, .Lfunc_end71-clause_CreateCrude
	.cfi_endproc

	.globl	clause_CreateUnnormalized
	.p2align	4, 0x90
	.type	clause_CreateUnnormalized,@function
clause_CreateUnnormalized:              # @clause_CreateUnnormalized
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi497:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi498:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi499:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi500:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi501:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi502:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi503:
	.cfi_def_cfa_offset 96
.Lcfi504:
	.cfi_offset %rbx, -56
.Lcfi505:
	.cfi_offset %r12, -48
.Lcfi506:
	.cfi_offset %r13, -40
.Lcfi507:
	.cfi_offset %r14, -32
.Lcfi508:
	.cfi_offset %r15, -24
.Lcfi509:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	$80, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%r13)
	movl	$0, 48(%r13)
	movl	$0, 8(%r13)
	movl	$-1, 4(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12(%r13)
	movups	%xmm0, 32(%r13)
	movq	%rbp, %rdi
	callq	list_Length
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%eax, 64(%r13)
	movq	%r12, %rdi
	callq	list_Length
	movl	%eax, %ebx
	movl	%ebx, 68(%r13)
	movq	%r15, %rdi
	callq	list_Length
	movl	%eax, %r14d
	movl	%r14d, 72(%r13)
	testq	%r13, %r13
	je	.LBB72_3
# BB#1:
	movl	68(%r13), %eax
	orl	%r14d, %eax
	jne	.LBB72_3
# BB#2:                                 # %clause_IsEmptyClause.exit
	cmpl	$0, 64(%r13)
	je	.LBB72_31
.LBB72_3:                               # %clause_IsEmptyClause.exit.thread
	addl	8(%rsp), %ebx           # 4-byte Folded Reload
	addl	%ebx, %r14d
	leal	(,%r14,8), %edi
	callq	memory_Malloc
	movq	%rax, 56(%r13)
	movl	8(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	jle	.LBB72_4
# BB#14:                                # %.lr.ph84.preheader
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB72_15:                              # %.lr.ph84
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %r15
	movl	fol_NOT(%rip), %r13d
	movq	8(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%r13d, %edi
	movq	%r15, %r13
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rbx, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r13, 16(%rax)
	movq	56(%r13), %rcx
	movq	%rax, (%rcx,%r14,8)
	movq	(%rbp), %rbp
	incq	%r14
	cmpq	%r14, 16(%rsp)          # 8-byte Folded Reload
	jne	.LBB72_15
# BB#16:
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	12(%rsp), %ebx          # 4-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	%ebx, %eax
	jl	.LBB72_6
	jmp	.LBB72_9
.LBB72_4:
	xorl	%eax, %eax
	cmpl	%ebx, %eax
	jge	.LBB72_9
.LBB72_6:                               # %.lr.ph81.preheader
	movslq	%eax, %rbp
	movl	%ebx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB72_7:                               # %.lr.ph81
                                        # =>This Inner Loop Header: Depth=1
	movl	fol_NOT(%rip), %r14d
	movq	8(%r12), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rbx, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r13, 16(%rax)
	movq	56(%r13), %rcx
	movq	%rax, (%rcx,%rbp,8)
	movq	(%r12), %r12
	incq	%rbp
	cmpl	%ebp, 16(%rsp)          # 4-byte Folded Reload
	jne	.LBB72_7
# BB#8:                                 # %.preheader.loopexit
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB72_9:                               # %.preheader
	cmpl	%r14d, %eax
	jge	.LBB72_12
# BB#10:                                # %.lr.ph.preheader
	movslq	%eax, %rbx
	movl	%r14d, %r14d
	.p2align	4, 0x90
.LBB72_11:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rbp
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rbp, 24(%rax)
	movl	$0, 8(%rax)
	movl	$-1, 4(%rax)
	movl	$0, (%rax)
	movq	%r13, 16(%rax)
	movq	56(%r13), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	(%r15), %r15
	incq	%rbx
	cmpl	%ebx, %r14d
	jne	.LBB72_11
.LBB72_12:                              # %._crit_edge
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	jle	.LBB72_13
# BB#17:                                # %.lr.ph.i.i
	movl	stack_POINTER(%rip), %ebx
	movl	fol_NOT(%rip), %r8d
	movl	%eax, %r9d
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB72_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB72_21 Depth 2
                                        #       Child Loop BB72_26 Depth 3
	movq	56(%r13), %rax
	movq	(%rax,%rsi,8), %rax
	movq	24(%rax), %rcx
	cmpl	(%rcx), %r8d
	jne	.LBB72_20
# BB#19:                                #   in Loop: Header=BB72_18 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB72_20:                              # %clause_LiteralAtom.exit.i.i.i.preheader
                                        #   in Loop: Header=BB72_18 Depth=1
	xorl	%r10d, %r10d
	movl	%ebx, %edi
	movl	%ebx, %ebp
	jmp	.LBB72_21
	.p2align	4, 0x90
.LBB72_32:                              # %.critedge.i.i.i
                                        #   in Loop: Header=BB72_21 Depth=2
	cmpl	%ebp, %ebx
	je	.LBB72_29
# BB#33:                                #   in Loop: Header=BB72_21 Depth=2
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	%rdx, stack_STACK(,%rax,8)
.LBB72_21:                              # %clause_LiteralAtom.exit.i.i.i
                                        #   Parent Loop BB72_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB72_26 Depth 3
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.LBB72_23
# BB#22:                                #   in Loop: Header=BB72_21 Depth=2
	leal	1(%rbp), %edi
	movl	%edi, stack_POINTER(%rip)
	movl	%ebp, %ecx
	movq	%rax, stack_STACK(,%rcx,8)
	movl	%edi, %ebp
	cmpl	%ebx, %ebp
	jne	.LBB72_26
	jmp	.LBB72_29
	.p2align	4, 0x90
.LBB72_23:                              #   in Loop: Header=BB72_21 Depth=2
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB72_25
# BB#24:                                #   in Loop: Header=BB72_21 Depth=2
	cmpl	%eax, %r10d
	cmovll	%eax, %r10d
.LBB72_25:                              # %.preheader.i.i.i
                                        #   in Loop: Header=BB72_21 Depth=2
	cmpl	%ebx, %ebp
	je	.LBB72_29
	.p2align	4, 0x90
.LBB72_26:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB72_18 Depth=1
                                        #     Parent Loop BB72_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rbp), %eax
	movq	stack_STACK(,%rax,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB72_32
# BB#27:                                #   in Loop: Header=BB72_26 Depth=3
	movl	%eax, stack_POINTER(%rip)
	cmpl	%eax, %ebx
	movl	%eax, %edi
	movl	%eax, %ebp
	jne	.LBB72_26
# BB#28:                                #   in Loop: Header=BB72_18 Depth=1
	movl	%ebx, %edi
.LBB72_29:                              # %clause_LiteralMaxVar.exit.i.i
                                        #   in Loop: Header=BB72_18 Depth=1
	cmpl	%r11d, %r10d
	cmovgel	%r10d, %r11d
	incq	%rsi
	cmpq	%r9, %rsi
	movl	%edi, %ebx
	jne	.LBB72_18
	jmp	.LBB72_30
.LBB72_13:
	xorl	%r11d, %r11d
.LBB72_30:                              # %clause_UpdateMaxVar.exit
	movl	%r11d, 52(%r13)
.LBB72_31:
	movq	%r13, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end72:
	.size	clause_CreateUnnormalized, .Lfunc_end72-clause_CreateUnnormalized
	.cfi_endproc

	.globl	clause_CreateFromLiterals
	.p2align	4, 0x90
	.type	clause_CreateFromLiterals,@function
clause_CreateFromLiterals:              # @clause_CreateFromLiterals
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi510:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi511:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi512:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi513:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi514:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi515:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi516:
	.cfi_def_cfa_offset 96
.Lcfi517:
	.cfi_offset %rbx, -56
.Lcfi518:
	.cfi_offset %r12, -48
.Lcfi519:
	.cfi_offset %r13, -40
.Lcfi520:
	.cfi_offset %r14, -32
.Lcfi521:
	.cfi_offset %r15, -24
.Lcfi522:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB73_10
# BB#1:                                 # %.lr.ph
	testl	%esi, %esi
	je	.LBB73_11
# BB#2:                                 # %.lr.ph.split.preheader
	movl	symbol_TYPESTATBITS(%rip), %r15d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB73_3:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	movl	(%rbp), %eax
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB73_7
# BB#4:                                 #   in Loop: Header=BB73_3 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
	movl	%r15d, %ecx
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	cmpl	$1, 16(%rcx)
	jne	.LBB73_8
# BB#5:                                 #   in Loop: Header=BB73_3 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	cmpl	$0, (%rax)
	jle	.LBB73_8
# BB#6:                                 #   in Loop: Header=BB73_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	jmp	.LBB73_9
	.p2align	4, 0x90
.LBB73_7:                               #   in Loop: Header=BB73_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, %rcx
	movq	%rax, %r13
	jmp	.LBB73_9
	.p2align	4, 0x90
.LBB73_8:                               #   in Loop: Header=BB73_3 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, %rcx
	movq	%rax, %r14
.LBB73_9:                               #   in Loop: Header=BB73_3 Depth=1
	movq	%rbp, 8(%rax)
	movq	%rcx, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB73_3
	jmp	.LBB73_14
.LBB73_10:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB73_14
.LBB73_11:                              # %.lr.ph.split.us.preheader
	movq	%r12, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB73_12:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rbp
	movl	(%rbp), %r15d
	movl	fol_NOT(%rip), %r12d
	movl	$16, %edi
	callq	memory_Malloc
	cmpl	%r12d, %r15d
	movq	%r13, %rcx
	cmoveq	%r14, %rcx
	cmoveq	%rax, %r14
	cmovneq	%rax, %r13
	movq	%rbp, 8(%rax)
	movq	%rcx, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB73_12
# BB#13:
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB73_14:                              # %._crit_edge
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	list_NReverse
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	list_NReverse
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	list_NReverse
	movq	%rax, %r15
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movl	16(%rsp), %ecx          # 4-byte Reload
	callq	clause_CreateCrude
	movq	%rax, %r14
	testq	%rbp, %rbp
	je	.LBB73_16
	.p2align	4, 0x90
.LBB73_15:                              # %.lr.ph.i60
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB73_15
.LBB73_16:                              # %list_Delete.exit61
	testq	%rbx, %rbx
	je	.LBB73_18
	.p2align	4, 0x90
.LBB73_17:                              # %.lr.ph.i55
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB73_17
.LBB73_18:                              # %list_Delete.exit56
	testq	%r15, %r15
	je	.LBB73_20
	.p2align	4, 0x90
.LBB73_19:                              # %.lr.ph.i51
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB73_19
.LBB73_20:                              # %list_Delete.exit
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB73_22
# BB#21:
	movq	%r14, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	clause_OrientEqualities
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	clause_ReInit
	jmp	.LBB73_44
.LBB73_22:
	movl	68(%r14), %ebx
	movl	72(%r14), %ebp
	addl	64(%r14), %ebx
	callq	term_StartMinRenaming
	addl	%ebp, %ebx
	jle	.LBB73_25
# BB#23:                                # %.lr.ph.i
	movl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB73_24:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	callq	term_Rename
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB73_24
.LBB73_25:                              # %clause_Normalize.exit
	movl	68(%r14), %eax
	addl	64(%r14), %eax
	addl	72(%r14), %eax
	jle	.LBB73_42
# BB#26:                                # %.lr.ph.i.i
	movl	stack_POINTER(%rip), %ebx
	movl	fol_NOT(%rip), %r8d
	movl	%eax, %r9d
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB73_27:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB73_32 Depth 2
                                        #       Child Loop BB73_37 Depth 3
	movq	56(%r14), %rax
	movq	(%rax,%rsi,8), %rax
	movq	24(%rax), %rcx
	cmpl	(%rcx), %r8d
	jne	.LBB73_29
# BB#28:                                #   in Loop: Header=BB73_27 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB73_29:                              # %clause_LiteralAtom.exit.i.i.i.preheader
                                        #   in Loop: Header=BB73_27 Depth=1
	xorl	%r10d, %r10d
	movl	%ebx, %edi
	movl	%ebx, %ebp
	jmp	.LBB73_32
	.p2align	4, 0x90
.LBB73_30:                              # %.critedge.i.i.i
                                        #   in Loop: Header=BB73_32 Depth=2
	cmpl	%ebp, %ebx
	je	.LBB73_40
# BB#31:                                #   in Loop: Header=BB73_32 Depth=2
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	%rdx, stack_STACK(,%rax,8)
.LBB73_32:                              # %clause_LiteralAtom.exit.i.i.i
                                        #   Parent Loop BB73_27 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB73_37 Depth 3
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.LBB73_34
# BB#33:                                #   in Loop: Header=BB73_32 Depth=2
	leal	1(%rbp), %edi
	movl	%edi, stack_POINTER(%rip)
	movl	%ebp, %ecx
	movq	%rax, stack_STACK(,%rcx,8)
	movl	%edi, %ebp
	cmpl	%ebx, %ebp
	jne	.LBB73_37
	jmp	.LBB73_40
	.p2align	4, 0x90
.LBB73_34:                              #   in Loop: Header=BB73_32 Depth=2
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.LBB73_36
# BB#35:                                #   in Loop: Header=BB73_32 Depth=2
	cmpl	%eax, %r10d
	cmovll	%eax, %r10d
.LBB73_36:                              # %.preheader.i.i.i
                                        #   in Loop: Header=BB73_32 Depth=2
	cmpl	%ebx, %ebp
	je	.LBB73_40
	.p2align	4, 0x90
.LBB73_37:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB73_27 Depth=1
                                        #     Parent Loop BB73_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rbp), %eax
	movq	stack_STACK(,%rax,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB73_30
# BB#38:                                #   in Loop: Header=BB73_37 Depth=3
	movl	%eax, stack_POINTER(%rip)
	cmpl	%eax, %ebx
	movl	%eax, %edi
	movl	%eax, %ebp
	jne	.LBB73_37
# BB#39:                                #   in Loop: Header=BB73_27 Depth=1
	movl	%ebx, %edi
.LBB73_40:                              # %clause_LiteralMaxVar.exit.i.i
                                        #   in Loop: Header=BB73_27 Depth=1
	cmpl	%r11d, %r10d
	cmovgel	%r10d, %r11d
	incq	%rsi
	cmpq	%r9, %rsi
	movl	%edi, %ebx
	jne	.LBB73_27
	jmp	.LBB73_43
.LBB73_42:
	xorl	%r11d, %r11d
.LBB73_43:
	movl	%r11d, 52(%r14)
.LBB73_44:
	movq	%r14, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end73:
	.size	clause_CreateFromLiterals, .Lfunc_end73-clause_CreateFromLiterals
	.cfi_endproc

	.globl	clause_SetSortConstraint
	.p2align	4, 0x90
	.type	clause_SetSortConstraint,@function
clause_SetSortConstraint:               # @clause_SetSortConstraint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi523:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi524:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi525:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi526:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi527:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi528:
	.cfi_def_cfa_offset 56
.Lcfi529:
	.cfi_offset %rbx, -56
.Lcfi530:
	.cfi_offset %r12, -48
.Lcfi531:
	.cfi_offset %r13, -40
.Lcfi532:
	.cfi_offset %r14, -32
.Lcfi533:
	.cfi_offset %r15, -24
.Lcfi534:
	.cfi_offset %rbp, -16
	movq	%rcx, %r8
	movl	64(%rdi), %r11d
	movl	68(%rdi), %r9d
	leal	-1(%r11,%r9), %eax
	xorl	%r10d, %r10d
	cmpl	%eax, %r11d
	jle	.LBB74_2
# BB#1:
	movl	%r11d, %r15d
	jmp	.LBB74_18
.LBB74_2:                               # %.lr.ph
	movslq	%r11d, %rax
	leal	-1(%rax), %r14d
	movl	symbol_TYPESTATBITS(%rip), %ecx
	decq	%rax
	xorl	%r10d, %r10d
	movl	%r11d, %r15d
	testl	%esi, %esi
	je	.LBB74_10
	.p2align	4, 0x90
.LBB74_3:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rdi), %rbx
	movq	8(%rbx,%rax,8), %r12
	movq	24(%r12), %rbp
	movl	(%rbp), %esi
	cmpl	%esi, fol_NOT(%rip)
	jne	.LBB74_5
# BB#4:                                 #   in Loop: Header=BB74_3 Depth=1
	movq	16(%rbp), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB74_5:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB74_3 Depth=1
	negl	%esi
	sarl	%cl, %esi
	movq	symbol_SIGNATURE(%rip), %rbp
	movslq	%esi, %rsi
	movq	(%rbp,%rsi,8), %rsi
	cmpl	$1, 16(%rsi)
	jne	.LBB74_9
# BB#6:                                 #   in Loop: Header=BB74_3 Depth=1
	incl	%r14d
	cmpl	%r14d, %r11d
	je	.LBB74_8
# BB#7:                                 #   in Loop: Header=BB74_3 Depth=1
	movslq	%r14d, %rsi
	movq	(%rbx,%rsi,8), %rbp
	movq	%r12, (%rbx,%rsi,8)
	movq	56(%rdi), %rsi
	movq	%rbp, 8(%rsi,%rax,8)
	movl	64(%rdi), %r15d
	movl	68(%rdi), %r9d
.LBB74_8:                               #   in Loop: Header=BB74_3 Depth=1
	incl	%r10d
.LBB74_9:                               #   in Loop: Header=BB74_3 Depth=1
	leal	-1(%r15,%r9), %esi
	movslq	%esi, %rsi
	incq	%rax
	incl	%r11d
	cmpq	%rsi, %rax
	jl	.LBB74_3
	jmp	.LBB74_18
	.p2align	4, 0x90
.LBB74_10:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rdi), %r13
	movq	8(%r13,%rax,8), %r12
	movq	24(%r12), %rsi
	movl	(%rsi), %ebp
	cmpl	%ebp, fol_NOT(%rip)
	jne	.LBB74_12
# BB#11:                                #   in Loop: Header=BB74_10 Depth=1
	movq	16(%rsi), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %ebp
.LBB74_12:                              # %clause_LiteralAtom.exit.us
                                        #   in Loop: Header=BB74_10 Depth=1
	negl	%ebp
	sarl	%cl, %ebp
	movq	symbol_SIGNATURE(%rip), %rbx
	movslq	%ebp, %rbp
	movq	(%rbx,%rbp,8), %rbx
	cmpl	$1, 16(%rbx)
	jne	.LBB74_17
# BB#13:                                #   in Loop: Header=BB74_10 Depth=1
	movq	16(%rsi), %rsi
	movq	8(%rsi), %rsi
	cmpl	$0, (%rsi)
	jle	.LBB74_17
# BB#14:                                #   in Loop: Header=BB74_10 Depth=1
	incl	%r14d
	cmpl	%r14d, %r11d
	je	.LBB74_16
# BB#15:                                #   in Loop: Header=BB74_10 Depth=1
	movslq	%r14d, %rsi
	movq	(%r13,%rsi,8), %rbp
	movq	%r12, (%r13,%rsi,8)
	movq	56(%rdi), %rsi
	movq	%rbp, 8(%rsi,%rax,8)
	movl	64(%rdi), %r15d
	movl	68(%rdi), %r9d
.LBB74_16:                              #   in Loop: Header=BB74_10 Depth=1
	incl	%r10d
.LBB74_17:                              #   in Loop: Header=BB74_10 Depth=1
	leal	-1(%r15,%r9), %esi
	movslq	%esi, %rsi
	incq	%rax
	incl	%r11d
	cmpq	%rsi, %rax
	jl	.LBB74_10
.LBB74_18:                              # %._crit_edge
	addl	%r10d, %r15d
	movl	%r15d, 64(%rdi)
	subl	%r10d, %r9d
	movl	%r9d, 68(%rdi)
	movq	%rdx, %rsi
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	clause_ReInit           # TAILCALL
.Lfunc_end74:
	.size	clause_SetSortConstraint, .Lfunc_end74-clause_SetSortConstraint
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_ReInit,@function
clause_ReInit:                          # @clause_ReInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi535:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi536:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi537:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi538:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi539:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi540:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi541:
	.cfi_def_cfa_offset 64
.Lcfi542:
	.cfi_offset %rbx, -56
.Lcfi543:
	.cfi_offset %r12, -48
.Lcfi544:
	.cfi_offset %r13, -40
.Lcfi545:
	.cfi_offset %r14, -32
.Lcfi546:
	.cfi_offset %r15, -24
.Lcfi547:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	68(%r14), %ebx
	movl	72(%r14), %ebp
	addl	64(%r14), %ebx
	callq	term_StartMinRenaming
	addl	%ebp, %ebx
	jle	.LBB75_3
# BB#1:                                 # %.lr.ph.i
	movl	%ebx, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB75_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Rename
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB75_2
.LBB75_3:                               # %clause_Normalize.exit
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	clause_SetMaxLitFlags
	movl	64(%r14), %edx
	movl	68(%r14), %eax
	movl	72(%r14), %esi
	leal	(%rax,%rdx), %ecx
	xorl	%r10d, %r10d
	addl	%esi, %ecx
	movl	$0, %r11d
	jle	.LBB75_16
# BB#4:                                 # %.lr.ph.i.i
	movl	%ecx, %r8d
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB75_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB75_6 Depth 2
                                        #       Child Loop BB75_12 Depth 3
	movq	56(%r14), %rax
	movq	(%rax,%rsi,8), %r9
	movq	24(%r9), %rdi
	movl	stack_POINTER(%rip), %ebx
	movl	%ebx, %ecx
	xorl	%edx, %edx
	jmp	.LBB75_6
	.p2align	4, 0x90
.LBB75_23:                              # %.critedge.i.i.i.i
                                        #   in Loop: Header=BB75_6 Depth=2
	cmpl	%ecx, %ebx
	je	.LBB75_14
# BB#24:                                #   in Loop: Header=BB75_6 Depth=2
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	%rbp, stack_STACK(,%rax,8)
.LBB75_6:                               #   Parent Loop BB75_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB75_12 Depth 3
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB75_8
# BB#7:                                 #   in Loop: Header=BB75_6 Depth=2
	movl	180(%r12), %edi
	movl	%ecx, %ebp
	leal	1(%rcx), %ecx
	movl	%ecx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rbp,8)
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jmp	.LBB75_11
	.p2align	4, 0x90
.LBB75_8:                               #   in Loop: Header=BB75_6 Depth=2
	cmpl	$0, (%rdi)
	jle	.LBB75_10
# BB#9:                                 #   in Loop: Header=BB75_6 Depth=2
	movl	184(%r12), %edi
	jmp	.LBB75_11
.LBB75_10:                              #   in Loop: Header=BB75_6 Depth=2
	movl	180(%r12), %edi
	.p2align	4, 0x90
.LBB75_11:                              # %.preheader.i.i.i.i
                                        #   in Loop: Header=BB75_6 Depth=2
	addl	%edi, %edx
	cmpl	%ebx, %ecx
	je	.LBB75_14
	.p2align	4, 0x90
.LBB75_12:                              # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB75_5 Depth=1
                                        #     Parent Loop BB75_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rcx), %eax
	movq	stack_STACK(,%rax,8), %rdi
	testq	%rdi, %rdi
	jne	.LBB75_23
# BB#13:                                #   in Loop: Header=BB75_12 Depth=3
	movl	%eax, stack_POINTER(%rip)
	cmpl	%eax, %ebx
	movl	%eax, %ecx
	jne	.LBB75_12
.LBB75_14:                              # %clause_UpdateLiteralWeight.exit.i.i
                                        #   in Loop: Header=BB75_5 Depth=1
	movl	%edx, 4(%r9)
	addl	%edx, %r11d
	incq	%rsi
	cmpq	%r8, %rsi
	jne	.LBB75_5
# BB#15:                                # %clause_UpdateWeight.exit.loopexit
	movl	64(%r14), %edx
	movl	68(%r14), %eax
	movl	72(%r14), %esi
.LBB75_16:                              # %clause_UpdateWeight.exit
	movl	%r11d, 4(%r14)
	addl	%edx, %eax
	addl	%esi, %eax
	jle	.LBB75_32
# BB#17:                                # %.lr.ph.i.i15
	movl	stack_POINTER(%rip), %ecx
	movl	fol_NOT(%rip), %r8d
	movl	%eax, %r9d
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB75_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB75_21 Depth 2
                                        #       Child Loop BB75_28 Depth 3
	movq	56(%r14), %rax
	movq	(%rax,%rsi,8), %rax
	movq	24(%rax), %rax
	cmpl	(%rax), %r8d
	jne	.LBB75_20
# BB#19:                                #   in Loop: Header=BB75_18 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB75_20:                              # %clause_LiteralAtom.exit.i.i.i.preheader
                                        #   in Loop: Header=BB75_18 Depth=1
	xorl	%r11d, %r11d
	movl	%ecx, %edi
	movl	%ecx, %ebx
	jmp	.LBB75_21
	.p2align	4, 0x90
.LBB75_33:                              # %.critedge.i.i.i
                                        #   in Loop: Header=BB75_21 Depth=2
	cmpl	%ebx, %ecx
	je	.LBB75_31
# BB#34:                                #   in Loop: Header=BB75_21 Depth=2
	movq	(%rax), %rbp
	movq	8(%rax), %rax
	movq	%rbp, stack_STACK(,%rdx,8)
.LBB75_21:                              # %clause_LiteralAtom.exit.i.i.i
                                        #   Parent Loop BB75_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB75_28 Depth 3
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB75_25
# BB#22:                                #   in Loop: Header=BB75_21 Depth=2
	leal	1(%rbx), %edi
	movl	%edi, stack_POINTER(%rip)
	movl	%ebx, %eax
	movq	%rdx, stack_STACK(,%rax,8)
	movl	%edi, %ebx
	cmpl	%ecx, %ebx
	jne	.LBB75_28
	jmp	.LBB75_31
	.p2align	4, 0x90
.LBB75_25:                              #   in Loop: Header=BB75_21 Depth=2
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.LBB75_27
# BB#26:                                #   in Loop: Header=BB75_21 Depth=2
	cmpl	%eax, %r11d
	cmovll	%eax, %r11d
.LBB75_27:                              # %.preheader.i.i.i
                                        #   in Loop: Header=BB75_21 Depth=2
	cmpl	%ecx, %ebx
	je	.LBB75_31
	.p2align	4, 0x90
.LBB75_28:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB75_18 Depth=1
                                        #     Parent Loop BB75_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	-1(%rbx), %edx
	movq	stack_STACK(,%rdx,8), %rax
	testq	%rax, %rax
	jne	.LBB75_33
# BB#29:                                #   in Loop: Header=BB75_28 Depth=3
	movl	%edx, stack_POINTER(%rip)
	cmpl	%edx, %ecx
	movl	%edx, %edi
	movl	%edx, %ebx
	jne	.LBB75_28
# BB#30:                                #   in Loop: Header=BB75_18 Depth=1
	movl	%ecx, %edi
.LBB75_31:                              # %clause_LiteralMaxVar.exit.i.i
                                        #   in Loop: Header=BB75_18 Depth=1
	cmpl	%r10d, %r11d
	cmovgel	%r11d, %r10d
	incq	%rsi
	cmpq	%r9, %rsi
	movl	%edi, %ecx
	jne	.LBB75_18
.LBB75_32:                              # %clause_UpdateMaxVar.exit
	movl	%r10d, 52(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end75:
	.size	clause_ReInit, .Lfunc_end75-clause_ReInit
	.cfi_endproc

	.globl	clause_InsertIntoSharing
	.p2align	4, 0x90
	.type	clause_InsertIntoSharing,@function
clause_InsertIntoSharing:               # @clause_InsertIntoSharing
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi548:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi549:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi550:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi551:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi552:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi553:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi554:
	.cfi_def_cfa_offset 64
.Lcfi555:
	.cfi_offset %rbx, -56
.Lcfi556:
	.cfi_offset %r12, -48
.Lcfi557:
	.cfi_offset %r13, -40
.Lcfi558:
	.cfi_offset %r14, -32
.Lcfi559:
	.cfi_offset %r15, -24
.Lcfi560:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	addl	72(%r15), %eax
	jle	.LBB76_8
# BB#1:                                 # %.lr.ph
	movl	%eax, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB76_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	24(%rbx), %r12
	movl	fol_NOT(%rip), %eax
	cmpl	(%r12), %eax
	jne	.LBB76_4
# BB#3:                                 #   in Loop: Header=BB76_2 Depth=1
	movq	16(%r12), %rax
	movq	8(%rax), %r12
.LBB76_4:                               # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB76_2 Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	sharing_Insert
	movq	24(%rbx), %rcx
	movl	fol_NOT(%rip), %edx
	cmpl	(%rcx), %edx
	jne	.LBB76_5
# BB#6:                                 #   in Loop: Header=BB76_2 Depth=1
	movq	16(%rcx), %rbx
	addq	$8, %rbx
	jmp	.LBB76_7
	.p2align	4, 0x90
.LBB76_5:                               #   in Loop: Header=BB76_2 Depth=1
	addq	$24, %rbx
.LBB76_7:                               # %clause_LiteralInsertIntoSharing.exit
                                        #   in Loop: Header=BB76_2 Depth=1
	movq	%rax, (%rbx)
	movq	%r12, %rdi
	callq	term_Delete
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB76_2
.LBB76_8:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end76:
	.size	clause_InsertIntoSharing, .Lfunc_end76-clause_InsertIntoSharing
	.cfi_endproc

	.globl	clause_DeleteFromSharing
	.p2align	4, 0x90
	.type	clause_DeleteFromSharing,@function
clause_DeleteFromSharing:               # @clause_DeleteFromSharing
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi561:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi562:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi563:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi564:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi565:
	.cfi_def_cfa_offset 48
.Lcfi566:
	.cfi_offset %rbx, -48
.Lcfi567:
	.cfi_offset %r12, -40
.Lcfi568:
	.cfi_offset %r13, -32
.Lcfi569:
	.cfi_offset %r14, -24
.Lcfi570:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	64(%r14), %eax
	movl	68(%r14), %ecx
	movl	72(%r14), %edx
	leal	(%rcx,%rax), %esi
	addl	%edx, %esi
	jle	.LBB77_6
# BB#1:                                 # %.lr.ph
	movl	%esi, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB77_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %r13
	movq	24(%r13), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB77_4
# BB#3:                                 #   in Loop: Header=BB77_2 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	24(%r13), %rax
	movq	memory_ARRAY+256(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rax, (%rcx)
.LBB77_4:                               # %clause_LiteralDeleteFromSharing.exit
                                        #   in Loop: Header=BB77_2 Depth=1
	movq	%r13, %rdi
	movq	%r15, %rdx
	callq	sharing_Delete
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r13, (%rax)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB77_2
# BB#5:                                 # %._crit_edge.loopexit
	movl	64(%r14), %eax
	movl	68(%r14), %ecx
	movl	72(%r14), %edx
.LBB77_6:                               # %._crit_edge
	addl	%eax, %ecx
	addl	%edx, %ecx
	je	.LBB77_14
# BB#7:
	movq	56(%r14), %rdi
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB77_8
# BB#13:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB77_14
.LBB77_8:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB77_10
# BB#9:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB77_10:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB77_12
# BB#11:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB77_12:
	addq	$-16, %rdi
	callq	free
.LBB77_14:                              # %clause_FreeLitArray.exit
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB77_16
	.p2align	4, 0x90
.LBB77_15:                              # %.lr.ph.i23
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB77_15
.LBB77_16:                              # %list_Delete.exit24
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.LBB77_18
	.p2align	4, 0x90
.LBB77_17:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB77_17
.LBB77_18:                              # %list_Delete.exit
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB77_26
# BB#19:
	movl	24(%r14), %ecx
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB77_20
# BB#25:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB77_26
.LBB77_20:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB77_22
# BB#21:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB77_22:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB77_24
# BB#23:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB77_24:
	addq	$-16, %rdi
	callq	free
.LBB77_26:                              # %memory_Free.exit
	movq	memory_ARRAY+640(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+640(%rip), %rax
	movq	%r14, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end77:
	.size	clause_DeleteFromSharing, .Lfunc_end77-clause_DeleteFromSharing
	.cfi_endproc

	.globl	clause_MakeUnshared
	.p2align	4, 0x90
	.type	clause_MakeUnshared,@function
clause_MakeUnshared:                    # @clause_MakeUnshared
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi571:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi572:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi573:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi574:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi575:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi576:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi577:
	.cfi_def_cfa_offset 112
.Lcfi578:
	.cfi_offset %rbx, -56
.Lcfi579:
	.cfi_offset %r12, -48
.Lcfi580:
	.cfi_offset %r13, -40
.Lcfi581:
	.cfi_offset %r14, -32
.Lcfi582:
	.cfi_offset %r15, -24
.Lcfi583:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movl	64(%rdi), %ecx
	movl	68(%rdi), %r15d
	movl	72(%rdi), %edx
	leal	(%r15,%rcx), %esi
	leal	(%rsi,%rdx), %ebx
	movl	%esi, %eax
	decl	%eax
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	js	.LBB78_1
# BB#7:                                 # %.lr.ph48
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movl	%esi, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB78_8:                               # =>This Inner Loop Header: Depth=1
	movq	56(%rdi), %rax
	movq	(%rax,%r14,8), %r12
	movq	24(%r12), %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB78_10
# BB#9:                                 #   in Loop: Header=BB78_8 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB78_10:                              # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB78_8 Depth=1
	movq	%rbx, %rdi
	callq	term_Copy
	movq	%rax, %r13
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	sharing_Delete
	movq	24(%r12), %rax
	movq	16(%rax), %rax
	movq	%r13, 8(%rax)
	incq	%r14
	cmpq	%r14, 48(%rsp)          # 8-byte Folded Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	jne	.LBB78_8
# BB#2:                                 # %.preheader.loopexit
	movq	%rbp, %r14
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	12(%rsp), %ebx          # 4-byte Reload
	cmpl	%ebx, %eax
	jl	.LBB78_4
	jmp	.LBB78_6
.LBB78_1:
	movq	%rbp, %r14
	xorl	%eax, %eax
	cmpl	%ebx, %eax
	jge	.LBB78_6
.LBB78_4:                               # %.lr.ph
	movslq	%eax, %rbp
	shlq	$3, %rbp
	addl	%ecx, %r15d
	addl	%edx, %r15d
	subl	%eax, %r15d
	.p2align	4, 0x90
.LBB78_5:                               # =>This Inner Loop Header: Depth=1
	movq	56(%rdi), %rax
	movq	(%rax,%rbp), %rbx
	movq	24(%rbx), %r12
	movq	%r12, %rdi
	callq	term_Copy
	movq	%rax, %r13
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	sharing_Delete
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, 24(%rbx)
	addq	$8, %rbp
	decl	%r15d
	jne	.LBB78_5
.LBB78_6:                               # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end78:
	.size	clause_MakeUnshared, .Lfunc_end78-clause_MakeUnshared
	.cfi_endproc

	.globl	clause_MoveSharedClause
	.p2align	4, 0x90
	.type	clause_MoveSharedClause,@function
clause_MoveSharedClause:                # @clause_MoveSharedClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi584:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi585:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi586:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi587:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi588:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi589:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi590:
	.cfi_def_cfa_offset 80
.Lcfi591:
	.cfi_offset %rbx, -56
.Lcfi592:
	.cfi_offset %r12, -48
.Lcfi593:
	.cfi_offset %r13, -40
.Lcfi594:
	.cfi_offset %r14, -32
.Lcfi595:
	.cfi_offset %r15, -24
.Lcfi596:
	.cfi_offset %rbp, -16
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r12
	movl	68(%r12), %eax
	addl	64(%r12), %eax
	addl	72(%r12), %eax
	jle	.LBB79_8
# BB#1:                                 # %.lr.ph
	movl	%eax, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB79_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%r15,8), %rbx
	movq	24(%rbx), %r13
	movl	fol_NOT(%rip), %eax
	cmpl	(%r13), %eax
	jne	.LBB79_4
# BB#3:                                 #   in Loop: Header=BB79_2 Depth=1
	movq	16(%r13), %rax
	movq	8(%rax), %r13
.LBB79_4:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB79_2 Depth=1
	movq	%r13, %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	sharing_Insert
	movq	24(%rbx), %rcx
	movl	fol_NOT(%rip), %edx
	cmpl	(%rcx), %edx
	jne	.LBB79_5
# BB#6:                                 #   in Loop: Header=BB79_2 Depth=1
	movq	16(%rcx), %rcx
	addq	$8, %rcx
	jmp	.LBB79_7
	.p2align	4, 0x90
.LBB79_5:                               #   in Loop: Header=BB79_2 Depth=1
	leaq	24(%rbx), %rcx
.LBB79_7:                               # %clause_LiteralSetAtom.exit
                                        #   in Loop: Header=BB79_2 Depth=1
	movq	%rax, (%rcx)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	sharing_Delete
	movq	%rbp, %rdi
	callq	term_Delete
	incq	%r15
	cmpq	%r15, %r14
	jne	.LBB79_2
.LBB79_8:                               # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end79:
	.size	clause_MoveSharedClause, .Lfunc_end79-clause_MoveSharedClause
	.cfi_endproc

	.globl	clause_DeleteSharedLiteral
	.p2align	4, 0x90
	.type	clause_DeleteSharedLiteral,@function
clause_DeleteSharedLiteral:             # @clause_DeleteSharedLiteral
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi597:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi598:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi599:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi600:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi601:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi602:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi603:
	.cfi_def_cfa_offset 64
.Lcfi604:
	.cfi_offset %rbx, -56
.Lcfi605:
	.cfi_offset %r12, -48
.Lcfi606:
	.cfi_offset %r13, -40
.Lcfi607:
	.cfi_offset %r14, -32
.Lcfi608:
	.cfi_offset %r15, -24
.Lcfi609:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %r15
	movq	%r14, %rsi
	callq	clause_MakeUnshared
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	clause_DeleteLiteralNN
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	clause_ReInit
	movl	68(%r15), %eax
	addl	64(%r15), %eax
	addl	72(%r15), %eax
	jle	.LBB80_8
# BB#1:                                 # %.lr.ph.i
	movl	%eax, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB80_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	24(%rbx), %r12
	movl	fol_NOT(%rip), %eax
	cmpl	(%r12), %eax
	jne	.LBB80_4
# BB#3:                                 #   in Loop: Header=BB80_2 Depth=1
	movq	16(%r12), %rax
	movq	8(%rax), %r12
.LBB80_4:                               # %clause_LiteralAtom.exit.i.i
                                        #   in Loop: Header=BB80_2 Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	sharing_Insert
	movq	24(%rbx), %rcx
	movl	fol_NOT(%rip), %edx
	cmpl	(%rcx), %edx
	jne	.LBB80_5
# BB#6:                                 #   in Loop: Header=BB80_2 Depth=1
	movq	16(%rcx), %rbx
	addq	$8, %rbx
	jmp	.LBB80_7
	.p2align	4, 0x90
.LBB80_5:                               #   in Loop: Header=BB80_2 Depth=1
	addq	$24, %rbx
.LBB80_7:                               # %clause_LiteralInsertIntoSharing.exit.i
                                        #   in Loop: Header=BB80_2 Depth=1
	movq	%rax, (%rbx)
	movq	%r12, %rdi
	callq	term_Delete
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB80_2
.LBB80_8:                               # %clause_InsertIntoSharing.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end80:
	.size	clause_DeleteSharedLiteral, .Lfunc_end80-clause_DeleteSharedLiteral
	.cfi_endproc

	.globl	clause_DeleteLiteral
	.p2align	4, 0x90
	.type	clause_DeleteLiteral,@function
clause_DeleteLiteral:                   # @clause_DeleteLiteral
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi610:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi611:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi612:
	.cfi_def_cfa_offset 32
.Lcfi613:
	.cfi_offset %rbx, -32
.Lcfi614:
	.cfi_offset %r14, -24
.Lcfi615:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	callq	clause_DeleteLiteralNN
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	clause_ReInit           # TAILCALL
.Lfunc_end81:
	.size	clause_DeleteLiteral, .Lfunc_end81-clause_DeleteLiteral
	.cfi_endproc

	.globl	clause_DeleteClauseList
	.p2align	4, 0x90
	.type	clause_DeleteClauseList,@function
clause_DeleteClauseList:                # @clause_DeleteClauseList
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi616:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi617:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi618:
	.cfi_def_cfa_offset 32
.Lcfi619:
	.cfi_offset %rbx, -24
.Lcfi620:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB82_6
# BB#1:                                 # %.lr.ph.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB82_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB82_4
# BB#3:                                 #   in Loop: Header=BB82_2 Depth=1
	callq	clause_Delete
.LBB82_4:                               #   in Loop: Header=BB82_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB82_2
	.p2align	4, 0x90
.LBB82_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB82_5
.LBB82_6:                               # %list_Delete.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end82:
	.size	clause_DeleteClauseList, .Lfunc_end82-clause_DeleteClauseList
	.cfi_endproc

	.globl	clause_DeleteSharedClauseList
	.p2align	4, 0x90
	.type	clause_DeleteSharedClauseList,@function
clause_DeleteSharedClauseList:          # @clause_DeleteSharedClauseList
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi621:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi622:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi623:
	.cfi_def_cfa_offset 32
.Lcfi624:
	.cfi_offset %rbx, -32
.Lcfi625:
	.cfi_offset %r14, -24
.Lcfi626:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB83_4
# BB#1:                                 # %.lr.ph.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB83_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	callq	clause_DeleteFromSharing
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB83_2
	.p2align	4, 0x90
.LBB83_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB83_3
.LBB83_4:                               # %list_Delete.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end83:
	.size	clause_DeleteSharedClauseList, .Lfunc_end83-clause_DeleteSharedClauseList
	.cfi_endproc

	.globl	clause_DeleteAllIndexedClauses
	.p2align	4, 0x90
	.type	clause_DeleteAllIndexedClauses,@function
clause_DeleteAllIndexedClauses:         # @clause_DeleteAllIndexedClauses
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi627:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi628:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi629:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi630:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi631:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi632:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi633:
	.cfi_def_cfa_offset 64
.Lcfi634:
	.cfi_offset %rbx, -56
.Lcfi635:
	.cfi_offset %r12, -48
.Lcfi636:
	.cfi_offset %r13, -40
.Lcfi637:
	.cfi_offset %r14, -32
.Lcfi638:
	.cfi_offset %r15, -24
.Lcfi639:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	callq	term_CreateStandardVariable
	movq	%rax, %r15
	movl	(%r15), %r14d
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%r12), %rsi
	movq	%r15, %rdx
	callq	st_GetInstance
	movq	%rax, %rbp
	jmp	.LBB84_1
	.p2align	4, 0x90
.LBB84_2:                               # %.lr.ph56
                                        #   in Loop: Header=BB84_1 Depth=1
	movq	8(%rbp), %rdi
	movq	%r12, %rsi
	callq	sharing_GetDataList
	testq	%rax, %rax
	je	.LBB84_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB84_1 Depth=1
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB84_4:                               # %.lr.ph
                                        #   Parent Loop BB84_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rdx
	movq	16(%rdx), %rdx
	movq	%rdx, 8(%rcx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB84_4
.LBB84_5:                               # %._crit_edge
                                        #   in Loop: Header=BB84_1 Depth=1
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB84_8
# BB#6:                                 # %.lr.ph52.preheader
                                        #   in Loop: Header=BB84_1 Depth=1
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB84_7:                               # %.lr.ph52
                                        #   Parent Loop BB84_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	callq	clause_DeleteFromSharing
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB84_7
	.p2align	4, 0x90
.LBB84_8:                               # %.lr.ph.i42
                                        #   Parent Loop BB84_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB84_8
# BB#9:                                 # %list_Delete.exit43
                                        #   in Loop: Header=BB84_1 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%r12), %rsi
	movq	%r15, %rdx
	callq	st_GetInstance
	movq	%rax, %rbp
	testq	%r13, %r13
	je	.LBB84_1
	.p2align	4, 0x90
.LBB84_10:                              # %.lr.ph.i
                                        #   Parent Loop BB84_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB84_10
.LBB84_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB84_4 Depth 2
                                        #     Child Loop BB84_7 Depth 2
                                        #     Child Loop BB84_8 Depth 2
                                        #     Child Loop BB84_10 Depth 2
	testq	%rbp, %rbp
	jne	.LBB84_2
# BB#11:                                # %list_Delete.exit._crit_edge
	movq	%r15, %rdi
	callq	term_Delete
	movl	%r14d, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	symbol_Delete           # TAILCALL
.Lfunc_end84:
	.size	clause_DeleteAllIndexedClauses, .Lfunc_end84-clause_DeleteAllIndexedClauses
	.cfi_endproc

	.globl	clause_PrintAllIndexedClauses
	.p2align	4, 0x90
	.type	clause_PrintAllIndexedClauses,@function
clause_PrintAllIndexedClauses:          # @clause_PrintAllIndexedClauses
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi640:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi641:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi642:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi643:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi644:
	.cfi_def_cfa_offset 48
.Lcfi645:
	.cfi_offset %rbx, -48
.Lcfi646:
	.cfi_offset %r12, -40
.Lcfi647:
	.cfi_offset %r14, -32
.Lcfi648:
	.cfi_offset %r15, -24
.Lcfi649:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	callq	term_CreateStandardVariable
	movq	%rax, %r15
	movl	(%r15), %r14d
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%r12), %rsi
	movq	%r15, %rdx
	callq	st_GetInstance
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB85_11
# BB#1:                                 # %.lr.ph51.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB85_2:                               # %.lr.ph51
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB85_4 Depth 2
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	callq	sharing_GetDataList
	testq	%rax, %rax
	je	.LBB85_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB85_2 Depth=1
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB85_4:                               # %.lr.ph
                                        #   Parent Loop BB85_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rdx
	movq	16(%rdx), %rdx
	movq	%rdx, 8(%rcx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB85_4
.LBB85_5:                               # %._crit_edge
                                        #   in Loop: Header=BB85_2 Depth=1
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	list_NPointerUnion
	movq	%rax, %rbp
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB85_2
# BB#6:                                 # %._crit_edge52
	testq	%rbp, %rbp
	je	.LBB85_11
# BB#7:                                 # %.lr.ph.i36.preheader
	movq	8(%rbp), %rdi
	callq	clause_Print
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB85_10
	.p2align	4, 0x90
.LBB85_8:                               # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB85_8
	.p2align	4, 0x90
.LBB85_10:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB85_10
.LBB85_11:                              # %list_Delete.exit
	movq	%r15, %rdi
	callq	term_Delete
	movl	%r14d, %edi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	symbol_Delete           # TAILCALL
.Lfunc_end85:
	.size	clause_PrintAllIndexedClauses, .Lfunc_end85-clause_PrintAllIndexedClauses
	.cfi_endproc

	.globl	clause_ListPrint
	.p2align	4, 0x90
	.type	clause_ListPrint,@function
clause_ListPrint:                       # @clause_ListPrint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi650:
	.cfi_def_cfa_offset 16
.Lcfi651:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_2
	jmp	.LBB86_3
	.p2align	4, 0x90
.LBB86_1:                               # %.backedge
                                        #   in Loop: Header=BB86_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB86_2:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB86_1
.LBB86_3:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end86:
	.size	clause_ListPrint, .Lfunc_end86-clause_ListPrint
	.cfi_endproc

	.globl	clause_AllIndexedClauses
	.p2align	4, 0x90
	.type	clause_AllIndexedClauses,@function
clause_AllIndexedClauses:               # @clause_AllIndexedClauses
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi652:
	.cfi_def_cfa_offset 16
	callq	sharing_GetAllSuperTerms
	testq	%rax, %rax
	je	.LBB87_3
# BB#1:                                 # %.lr.ph.preheader
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB87_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	movq	16(%rdx), %rdx
	movq	%rdx, 8(%rcx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB87_2
.LBB87_3:                               # %._crit_edge
	movq	%rax, %rdi
	popq	%rax
	jmp	list_PointerDeleteDuplicates # TAILCALL
.Lfunc_end87:
	.size	clause_AllIndexedClauses, .Lfunc_end87-clause_AllIndexedClauses
	.cfi_endproc

	.globl	clause_DeleteLiteralNN
	.p2align	4, 0x90
	.type	clause_DeleteLiteralNN,@function
clause_DeleteLiteralNN:                 # @clause_DeleteLiteralNN
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi653:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi654:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi655:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi656:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi657:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi658:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi659:
	.cfi_def_cfa_offset 80
.Lcfi660:
	.cfi_offset %rbx, -56
.Lcfi661:
	.cfi_offset %r12, -48
.Lcfi662:
	.cfi_offset %r13, -40
.Lcfi663:
	.cfi_offset %r14, -32
.Lcfi664:
	.cfi_offset %r15, -24
.Lcfi665:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	64(%r13), %ecx
	movl	68(%r13), %esi
	movslq	72(%r13), %rax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	leal	(%rsi,%rcx), %ecx
	movslq	%ecx, %rbp
	addq	%rax, %rbp
	cmpl	$1, %ebp
	jle	.LBB88_1
# BB#2:                                 # %.lr.ph
	leal	-8(,%rbp,8), %edi
	callq	memory_Malloc
	movq	%rax, %r12
	decq	%rbp
	leaq	56(%r13), %r15
	movl	%r14d, %eax
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB88_3:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %rax
	cmovel	%edx, %esi
	movq	(%r15), %rdi
	leal	(%rcx,%rsi), %ebx
	movslq	%ebx, %rbx
	movq	(%rdi,%rbx,8), %rdi
	movq	%rdi, (%r12,%rcx,8)
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB88_3
	jmp	.LBB88_4
.LBB88_1:                               # %.._crit_edge_crit_edge
	leaq	56(%r13), %r15
	xorl	%r12d, %r12d
.LBB88_4:                               # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	-1(%rax), %ebp
	movq	56(%r13), %rax
	movslq	%r14d, %rcx
	movq	(%rax,%rcx,8), %rbx
	movq	24(%rbx), %rdi
	callq	term_Delete
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
	movl	68(%r13), %ecx
	addl	64(%r13), %ecx
	addl	72(%r13), %ecx
	je	.LBB88_12
# BB#5:
	movq	(%r15), %rdi
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB88_6
# BB#11:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB88_12
.LBB88_6:
	movl	%ebp, %r8d
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %rbx
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%rbx, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB88_8
# BB#7:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB88_8:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	movl	%r8d, %ebp
	js	.LBB88_10
# BB#9:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB88_10:
	addq	$-16, %rdi
	callq	free
.LBB88_12:                              # %clause_FreeLitArray.exit
	movq	%r12, (%r15)
	cmpl	%r14d, %ebp
	jge	.LBB88_13
# BB#14:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rax,%rcx), %eax
	cmpl	%r14d, %eax
	jge	.LBB88_15
# BB#16:
	decl	72(%r13)
	jmp	.LBB88_17
.LBB88_13:
	decl	64(%r13)
	jmp	.LBB88_17
.LBB88_15:
	decl	68(%r13)
.LBB88_17:
	movl	$-1, 4(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end88:
	.size	clause_DeleteLiteralNN, .Lfunc_end88-clause_DeleteLiteralNN
	.cfi_endproc

	.globl	clause_DeleteLiterals
	.p2align	4, 0x90
	.type	clause_DeleteLiterals,@function
clause_DeleteLiterals:                  # @clause_DeleteLiterals
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi666:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi667:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi668:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi669:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi670:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi671:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi672:
	.cfi_def_cfa_offset 128
.Lcfi673:
	.cfi_offset %rbx, -56
.Lcfi674:
	.cfi_offset %r12, -48
.Lcfi675:
	.cfi_offset %r13, -40
.Lcfi676:
	.cfi_offset %r14, -32
.Lcfi677:
	.cfi_offset %r15, -24
.Lcfi678:
	.cfi_offset %rbp, -16
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movl	64(%r13), %r14d
	movl	68(%r13), %r12d
	leal	(%r12,%r14), %ebp
	addl	72(%r13), %ebp
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rdi
	callq	list_Length
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%ebp, %edi
	subl	%eax, %edi
	je	.LBB89_1
# BB#2:
	shll	$3, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	jmp	.LBB89_3
.LBB89_1:
	xorl	%ebx, %ebx
.LBB89_3:
	testl	%ebp, %ebp
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$0, %r15d
	jle	.LBB89_23
# BB#4:                                 # %.lr.ph
	movl	%r14d, %eax
	decl	%eax
	leal	-1(%r14,%r12), %ecx
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB89_11
# BB#5:                                 # %.lr.ph.split.preheader
	movslq	%ecx, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	cltq
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%ebp, %r12d
	xorl	%ebp, %ebp
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	xorl	%r15d, %r15d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB89_6:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB89_7 Depth 2
	movq	32(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB89_7:                               # %.lr.ph.i
                                        #   Parent Loop BB89_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbp, 8(%rax)
	je	.LBB89_18
# BB#8:                                 #   in Loop: Header=BB89_7 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB89_7
# BB#9:                                 # %.loopexit
                                        #   in Loop: Header=BB89_6 Depth=1
	movq	56(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	movl	28(%rsp), %esi          # 4-byte Reload
	movslq	%esi, %rcx
	incl	%esi
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movq	%rax, (%rbx,%rcx,8)
	cmpq	64(%rsp), %rbp          # 8-byte Folded Reload
	jle	.LBB89_10
# BB#19:                                #   in Loop: Header=BB89_6 Depth=1
	cmpq	56(%rsp), %rbp          # 8-byte Folded Reload
	jle	.LBB89_20
# BB#21:                                #   in Loop: Header=BB89_6 Depth=1
	incl	12(%rsp)                # 4-byte Folded Spill
	jmp	.LBB89_22
	.p2align	4, 0x90
.LBB89_18:                              #   in Loop: Header=BB89_6 Depth=1
	movq	56(%r13), %rax
	movq	%r13, %r14
	movl	%r15d, %r13d
	movq	(%rax,%rbp,8), %r15
	movq	24(%r15), %rdi
	callq	term_Delete
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r15, (%rax)
	movl	%r13d, %r15d
	movq	%r14, %r13
	jmp	.LBB89_22
	.p2align	4, 0x90
.LBB89_10:                              #   in Loop: Header=BB89_6 Depth=1
	incl	%r15d
	jmp	.LBB89_22
.LBB89_20:                              #   in Loop: Header=BB89_6 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB89_22:                              #   in Loop: Header=BB89_6 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jne	.LBB89_6
	jmp	.LBB89_23
.LBB89_11:                              # %.lr.ph.split.us.preheader
	cltq
	movslq	%ecx, %rcx
	movl	%ebp, %edx
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%edi, %edi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB89_12:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdi
	movq	56(%r13), %rsi
	movq	(%rsi,%rdi,8), %rbp
	leaq	1(%rdi), %rsi
	movq	%rbp, (%rbx,%rdi,8)
	cmpq	%rax, %rdi
	jle	.LBB89_13
# BB#14:                                #   in Loop: Header=BB89_12 Depth=1
	cmpq	%rcx, %rdi
	jle	.LBB89_15
# BB#16:                                #   in Loop: Header=BB89_12 Depth=1
	incl	12(%rsp)                # 4-byte Folded Spill
	jmp	.LBB89_17
	.p2align	4, 0x90
.LBB89_13:                              #   in Loop: Header=BB89_12 Depth=1
	incl	%r15d
	jmp	.LBB89_17
	.p2align	4, 0x90
.LBB89_15:                              #   in Loop: Header=BB89_12 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	incl	%edi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
.LBB89_17:                              #   in Loop: Header=BB89_12 Depth=1
	cmpq	%rsi, %rdx
	jne	.LBB89_12
.LBB89_23:                              # %._crit_edge
	movl	68(%r13), %ecx
	addl	64(%r13), %ecx
	addl	72(%r13), %ecx
	je	.LBB89_31
# BB#24:
	movq	56(%r13), %rdi
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB89_25
# BB#30:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB89_31
.LBB89_25:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%r8, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB89_27
# BB#26:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB89_27:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB89_29
# BB#28:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB89_29:
	addq	$-16, %rdi
	callq	free
.LBB89_31:                              # %clause_FreeLitArray.exit
	movq	%rbx, 56(%r13)
	movl	%r15d, 64(%r13)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 68(%r13)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 72(%r13)
	movq	%r13, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	clause_ReInit           # TAILCALL
.Lfunc_end89:
	.size	clause_DeleteLiterals, .Lfunc_end89-clause_DeleteLiterals
	.cfi_endproc

	.globl	clause_IsHornClause
	.p2align	4, 0x90
	.type	clause_IsHornClause,@function
clause_IsHornClause:                    # @clause_IsHornClause
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$2, 72(%rdi)
	setl	%al
	retq
.Lfunc_end90:
	.size	clause_IsHornClause, .Lfunc_end90-clause_IsHornClause
	.cfi_endproc

	.globl	clause_HasTermSortConstraintLits
	.p2align	4, 0x90
	.type	clause_HasTermSortConstraintLits,@function
clause_HasTermSortConstraintLits:       # @clause_HasTermSortConstraintLits
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi679:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi680:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi681:
	.cfi_def_cfa_offset 32
.Lcfi682:
	.cfi_offset %rbx, -32
.Lcfi683:
	.cfi_offset %r14, -24
.Lcfi684:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movslq	64(%r15), %r14
	testq	%r14, %r14
	jle	.LBB91_7
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB91_2:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB91_4
# BB#3:                                 #   in Loop: Header=BB91_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB91_4:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB91_2 Depth=1
	callq	term_AllArgsAreVar
	testl	%eax, %eax
	je	.LBB91_8
# BB#5:                                 #   in Loop: Header=BB91_2 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	jl	.LBB91_2
.LBB91_7:
	xorl	%eax, %eax
	jmp	.LBB91_9
.LBB91_8:
	movl	$1, %eax
.LBB91_9:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end91:
	.size	clause_HasTermSortConstraintLits, .Lfunc_end91-clause_HasTermSortConstraintLits
	.cfi_endproc

	.globl	clause_HasSolvedConstraint
	.p2align	4, 0x90
	.type	clause_HasSolvedConstraint,@function
clause_HasSolvedConstraint:             # @clause_HasSolvedConstraint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi685:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi686:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi687:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi688:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi689:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi690:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi691:
	.cfi_def_cfa_offset 64
.Lcfi692:
	.cfi_offset %rbx, -56
.Lcfi693:
	.cfi_offset %r12, -48
.Lcfi694:
	.cfi_offset %r13, -40
.Lcfi695:
	.cfi_offset %r14, -32
.Lcfi696:
	.cfi_offset %r15, -24
.Lcfi697:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	64(%r15), %ebx
	testq	%rbx, %rbx
	movl	$1, %r14d
	je	.LBB92_24
# BB#1:
	movslq	%ebx, %r13
	testl	%ebx, %ebx
	jle	.LBB92_12
# BB#2:                                 # %.lr.ph.i55
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB92_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB92_5
# BB#4:                                 #   in Loop: Header=BB92_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB92_5:                               # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB92_3 Depth=1
	callq	term_AllArgsAreVar
	testl	%eax, %eax
	je	.LBB92_23
# BB#6:                                 #   in Loop: Header=BB92_3 Depth=1
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB92_3
# BB#7:                                 # %clause_HasTermSortConstraintLits.exit.thread.preheader
	testl	%r13d, %r13d
	jle	.LBB92_12
# BB#8:                                 # %.lr.ph61
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB92_9:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB92_11
# BB#10:                                #   in Loop: Header=BB92_9 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB92_11:                              # %clause_GetLiteralAtom.exit52
                                        #   in Loop: Header=BB92_9 Depth=1
	callq	term_VariableSymbols
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	list_NPointerUnion
	movq	%rax, %r12
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB92_9
	jmp	.LBB92_13
.LBB92_12:                              # %clause_HasTermSortConstraintLits.exit.thread._crit_edge
	xorl	%r12d, %r12d
	testl	%ebx, %ebx
	jne	.LBB92_23
.LBB92_13:                              # %clause_HasTermSortConstraintLits.exit.thread._crit_edge.thread
	movl	64(%r15), %ebp
	addl	68(%r15), %ebp
	addl	72(%r15), %ebp
	cmpl	%ebp, %ebx
	jge	.LBB92_14
# BB#15:                                # %.lr.ph
	shlq	$3, %r13
	subl	%ebx, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB92_16:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%r13), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB92_18
# BB#17:                                #   in Loop: Header=BB92_16 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB92_18:                              # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB92_16 Depth=1
	callq	term_VariableSymbols
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	list_NPointerUnion
	movq	%rax, %rbx
	addq	$8, %r13
	decl	%ebp
	jne	.LBB92_16
# BB#19:                                # %._crit_edge
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	list_NPointerDifference
	testq	%rbx, %rbx
	je	.LBB92_21
	.p2align	4, 0x90
.LBB92_20:                              # %.lr.ph.i41
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB92_20
	jmp	.LBB92_21
.LBB92_14:                              # %._crit_edge.thread
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	list_NPointerDifference
.LBB92_21:                              # %list_Delete.exit42
	testq	%rax, %rax
	je	.LBB92_24
	.p2align	4, 0x90
.LBB92_22:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB92_22
.LBB92_23:
	xorl	%r14d, %r14d
.LBB92_24:                              # %clause_HasTermSortConstraintLits.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end92:
	.size	clause_HasSolvedConstraint, .Lfunc_end92-clause_HasSolvedConstraint
	.cfi_endproc

	.globl	clause_HasSelectedLiteral
	.p2align	4, 0x90
	.type	clause_HasSelectedLiteral,@function
clause_HasSelectedLiteral:              # @clause_HasSelectedLiteral
	.cfi_startproc
# BB#0:
	movslq	64(%rdi), %rcx
	movl	68(%rdi), %eax
	leal	-1(%rcx,%rax), %esi
	xorl	%eax, %eax
	cmpl	%esi, %ecx
	jg	.LBB93_5
# BB#1:                                 # %.lr.ph
	movq	56(%rdi), %rdx
	movslq	%esi, %rsi
	decq	%rcx
	.p2align	4, 0x90
.LBB93_3:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rcx,8), %rdi
	testb	$4, (%rdi)
	jne	.LBB93_4
# BB#2:                                 #   in Loop: Header=BB93_3 Depth=1
	incq	%rcx
	cmpq	%rsi, %rcx
	jl	.LBB93_3
	jmp	.LBB93_5
.LBB93_4:
	movl	$1, %eax
.LBB93_5:                               # %._crit_edge
	retq
.Lfunc_end93:
	.size	clause_HasSelectedLiteral, .Lfunc_end93-clause_HasSelectedLiteral
	.cfi_endproc

	.globl	clause_IsDeclarationClause
	.p2align	4, 0x90
	.type	clause_IsDeclarationClause,@function
clause_IsDeclarationClause:             # @clause_IsDeclarationClause
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi698:
	.cfi_def_cfa_offset 16
.Lcfi699:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	clause_HasSolvedConstraint
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB94_8
# BB#1:
	movl	72(%rbx), %edi
	testl	%edi, %edi
	jle	.LBB94_8
# BB#2:                                 # %.lr.ph
	movslq	68(%rbx), %rcx
	movslq	64(%rbx), %rax
	addq	%rcx, %rax
	addl	%eax, %edi
	movq	56(%rbx), %rdx
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movq	symbol_SIGNATURE(%rip), %r8
	movslq	%edi, %rdi
	.p2align	4, 0x90
.LBB94_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rax,8), %rbx
	testb	$1, (%rbx)
	je	.LBB94_6
# BB#4:                                 #   in Loop: Header=BB94_3 Depth=1
	movq	24(%rbx), %rbx
	xorl	%esi, %esi
	subl	(%rbx), %esi
	sarl	%cl, %esi
	movslq	%esi, %rsi
	movq	(%r8,%rsi,8), %rsi
	cmpl	$1, 16(%rsi)
	je	.LBB94_5
.LBB94_6:                               #   in Loop: Header=BB94_3 Depth=1
	incq	%rax
	cmpq	%rdi, %rax
	jl	.LBB94_3
# BB#7:
	xorl	%eax, %eax
.LBB94_8:                               # %.loopexit
	popq	%rbx
	retq
.LBB94_5:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end94:
	.size	clause_IsDeclarationClause, .Lfunc_end94-clause_IsDeclarationClause
	.cfi_endproc

	.globl	clause_IsSortTheoryClause
	.p2align	4, 0x90
	.type	clause_IsSortTheoryClause,@function
clause_IsSortTheoryClause:              # @clause_IsSortTheoryClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi700:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi701:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi702:
	.cfi_def_cfa_offset 32
.Lcfi703:
	.cfi_offset %rbx, -24
.Lcfi704:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	xorl	%ebp, %ebp
	cmpl	$0, 68(%rbx)
	jg	.LBB95_4
# BB#1:
	cmpl	$1, 72(%rbx)
	jg	.LBB95_4
# BB#2:
	movq	%rbx, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB95_4
# BB#3:
	movslq	68(%rbx), %rax
	movslq	64(%rbx), %rcx
	addq	%rax, %rcx
	movq	56(%rbx), %rax
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	xorl	%ebp, %ebp
	cmpl	$1, 16(%rax)
	sete	%bpl
.LBB95_4:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end95:
	.size	clause_IsSortTheoryClause, .Lfunc_end95-clause_IsSortTheoryClause
	.cfi_endproc

	.globl	clause_IsPotentialSortTheoryClause
	.p2align	4, 0x90
	.type	clause_IsPotentialSortTheoryClause,@function
clause_IsPotentialSortTheoryClause:     # @clause_IsPotentialSortTheoryClause
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi705:
	.cfi_def_cfa_offset 16
.Lcfi706:
	.cfi_offset %rbx, -16
	xorl	%eax, %eax
	cmpl	$1, 72(%rdi)
	jne	.LBB96_12
# BB#1:                                 # %.preheader
	movslq	68(%rdi), %rax
	movslq	64(%rdi), %r10
	addq	%rax, %r10
	testl	%r10d, %r10d
	movq	56(%rdi), %r9
	jle	.LBB96_2
# BB#5:                                 # %.lr.ph
	movl	fol_NOT(%rip), %r11d
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movq	symbol_SIGNATURE(%rip), %r8
	movslq	%r10d, %r10
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB96_6:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdi,8), %rax
	movq	24(%rax), %rdx
	movl	(%rdx), %esi
	cmpl	%esi, %r11d
	movl	%esi, %eax
	jne	.LBB96_8
# BB#7:                                 #   in Loop: Header=BB96_6 Depth=1
	movq	16(%rdx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB96_8:                               # %clause_LiteralAtom.exit34
                                        #   in Loop: Header=BB96_6 Depth=1
	negl	%eax
	sarl	%cl, %eax
	cltq
	movq	(%r8,%rax,8), %rbx
	xorl	%eax, %eax
	cmpl	$1, 16(%rbx)
	jne	.LBB96_12
# BB#9:                                 #   in Loop: Header=BB96_6 Depth=1
	cmpl	%esi, %r11d
	jne	.LBB96_11
# BB#10:                                #   in Loop: Header=BB96_6 Depth=1
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
.LBB96_11:                              # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB96_6 Depth=1
	movq	16(%rdx), %rdx
	movq	8(%rdx), %rdx
	cmpl	$0, (%rdx)
	jle	.LBB96_12
# BB#4:                                 #   in Loop: Header=BB96_6 Depth=1
	incq	%rdi
	cmpq	%r10, %rdi
	jl	.LBB96_6
	jmp	.LBB96_3
.LBB96_12:                              # %.loopexit
	popq	%rbx
	retq
.LBB96_2:                               # %.preheader.._crit_edge_crit_edge
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movq	symbol_SIGNATURE(%rip), %r8
.LBB96_3:                               # %._crit_edge
	movq	(%r9,%r10,8), %rax
	movq	24(%rax), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movslq	%edx, %rax
	movq	(%r8,%rax,8), %rcx
	xorl	%eax, %eax
	cmpl	$1, 16(%rcx)
	sete	%al
	popq	%rbx
	retq
.Lfunc_end96:
	.size	clause_IsPotentialSortTheoryClause, .Lfunc_end96-clause_IsPotentialSortTheoryClause
	.cfi_endproc

	.globl	clause_HasOnlyVarsInConstraint
	.p2align	4, 0x90
	.type	clause_HasOnlyVarsInConstraint,@function
clause_HasOnlyVarsInConstraint:         # @clause_HasOnlyVarsInConstraint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi707:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi708:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi709:
	.cfi_def_cfa_offset 32
.Lcfi710:
	.cfi_offset %rbx, -32
.Lcfi711:
	.cfi_offset %r14, -24
.Lcfi712:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movslq	64(%r15), %r14
	xorl	%ebx, %ebx
	testq	%r14, %r14
	jle	.LBB97_5
	.p2align	4, 0x90
.LBB97_1:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB97_3
# BB#2:                                 #   in Loop: Header=BB97_1 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB97_3:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB97_1 Depth=1
	callq	term_AllArgsAreVar
	testl	%eax, %eax
	je	.LBB97_5
# BB#4:                                 #   in Loop: Header=BB97_1 Depth=1
	incq	%rbx
	cmpq	%r14, %rbx
	jl	.LBB97_1
.LBB97_5:                               # %.critedge
	xorl	%eax, %eax
	cmpl	%r14d, %ebx
	sete	%al
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end97:
	.size	clause_HasOnlyVarsInConstraint, .Lfunc_end97-clause_HasOnlyVarsInConstraint
	.cfi_endproc

	.globl	clause_HasSortInSuccedent
	.p2align	4, 0x90
	.type	clause_HasSortInSuccedent,@function
clause_HasSortInSuccedent:              # @clause_HasSortInSuccedent
	.cfi_startproc
# BB#0:
	movl	72(%rdi), %edx
	testl	%edx, %edx
	jle	.LBB98_1
# BB#2:                                 # %.lr.ph
	movslq	68(%rdi), %rcx
	movslq	64(%rdi), %rax
	addq	%rcx, %rax
	addl	%eax, %edx
	movq	56(%rdi), %r8
	movl	fol_NOT(%rip), %r9d
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movq	symbol_SIGNATURE(%rip), %r10
	movslq	%edx, %r11
	incq	%rax
	.p2align	4, 0x90
.LBB98_3:                               # =>This Inner Loop Header: Depth=1
	movq	-8(%r8,%rax,8), %rdx
	movq	24(%rdx), %rdi
	movl	(%rdi), %esi
	cmpl	%esi, %r9d
	jne	.LBB98_5
# BB#4:                                 #   in Loop: Header=BB98_3 Depth=1
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %esi
.LBB98_5:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB98_3 Depth=1
	negl	%esi
	sarl	%cl, %esi
	movslq	%esi, %rdx
	movq	(%r10,%rdx,8), %rdx
	movl	16(%rdx), %esi
	cmpl	$1, %esi
	setne	%dl
	cmpq	%r11, %rax
	jge	.LBB98_7
# BB#6:                                 # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB98_3 Depth=1
	incq	%rax
	testb	%dl, %dl
	jne	.LBB98_3
.LBB98_7:                               # %.critedge.loopexit
	xorl	%eax, %eax
	cmpl	$1, %esi
	sete	%al
	retq
.LBB98_1:
	xorl	%eax, %eax
	retq
.Lfunc_end98:
	.size	clause_HasSortInSuccedent, .Lfunc_end98-clause_HasSortInSuccedent
	.cfi_endproc

	.globl	clause_LitsHaveCommonVar
	.p2align	4, 0x90
	.type	clause_LitsHaveCommonVar,@function
clause_LitsHaveCommonVar:               # @clause_LitsHaveCommonVar
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi713:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi714:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi715:
	.cfi_def_cfa_offset 32
.Lcfi716:
	.cfi_offset %rbx, -24
.Lcfi717:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	24(%rdi), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB99_2
# BB#1:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB99_2:                               # %clause_LiteralAtom.exit
	callq	term_VariableSymbols
	movq	%rax, %rbx
	movq	24(%r14), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB99_4
# BB#3:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB99_4:                               # %clause_LiteralAtom.exit24
	callq	term_VariableSymbols
	movq	%rax, %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	list_HasIntersection
	testq	%rbx, %rbx
	je	.LBB99_6
	.p2align	4, 0x90
.LBB99_5:                               # %.lr.ph.i15
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB99_5
.LBB99_6:                               # %list_Delete.exit16
	testq	%r14, %r14
	je	.LBB99_8
	.p2align	4, 0x90
.LBB99_7:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%r14)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%r14, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %r14
	jne	.LBB99_7
.LBB99_8:                               # %list_Delete.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end99:
	.size	clause_LitsHaveCommonVar, .Lfunc_end99-clause_LitsHaveCommonVar
	.cfi_endproc

	.globl	clause_Print
	.p2align	4, 0x90
	.type	clause_Print,@function
clause_Print:                           # @clause_Print
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi718:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi719:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi720:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi721:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi722:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi723:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi724:
	.cfi_def_cfa_offset 64
.Lcfi725:
	.cfi_offset %rbx, -56
.Lcfi726:
	.cfi_offset %r12, -48
.Lcfi727:
	.cfi_offset %r13, -40
.Lcfi728:
	.cfi_offset %r14, -32
.Lcfi729:
	.cfi_offset %r15, -24
.Lcfi730:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB100_34
# BB#1:
	movl	(%r12), %esi
	xorl	%r15d, %r15d
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	12(%r12), %esi
	movl	76(%r12), %ebx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rdi
	movq	%r12, %rsi
	callq	clause_FPrintOrigin
	cmpl	$16, %ebx
	je	.LBB100_6
# BB#2:
	movq	stdout(%rip), %rsi
	movl	$58, %edi
	callq	_IO_putc
	movq	32(%r12), %rbp
	testq	%rbp, %rbp
	je	.LBB100_6
# BB#3:
	movq	40(%r12), %rbx
	movl	8(%rbp), %esi
	movl	8(%rbx), %edx
	movl	$.L.str.7, %edi
	jmp	.LBB100_5
	.p2align	4, 0x90
.LBB100_4:                              # %.lr.ph.i
                                        #   in Loop: Header=BB100_5 Depth=1
	movq	(%rbx), %rbx
	movl	8(%rbp), %esi
	movl	8(%rbx), %edx
	movl	$.L.str.8, %edi
.LBB100_5:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	callq	printf
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB100_4
.LBB100_6:                              # %clause_PrintParentClauses.exit
	movq	stdout(%rip), %rsi
	movl	$93, %edi
	callq	_IO_putc
	movl	64(%r12), %ebp
	movl	68(%r12), %r13d
	movslq	72(%r12), %r14
	movq	stdout(%rip), %rax
	testl	%ebp, %ebp
	jle	.LBB100_12
# BB#7:                                 # %.lr.ph93
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB100_8:                              # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	movq	56(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB100_10
# BB#9:                                 #   in Loop: Header=BB100_8 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB100_10:                             # %clause_LiteralPrintUnsigned.exit75
                                        #   in Loop: Header=BB100_8 Depth=1
	callq	term_PrintPrefix
	movq	stdout(%rip), %rdi
	callq	fflush
	incq	%rbx
	movq	stdout(%rip), %rax
	cmpq	%rbx, %rbp
	jne	.LBB100_8
# BB#11:
	movl	%ebp, %r15d
.LBB100_12:                             # %._crit_edge94
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	addl	%ebp, %r13d
	cmpl	%r13d, %r15d
	jge	.LBB100_24
# BB#13:                                # %.lr.ph90
	movslq	%r15d, %rbp
	movslq	%r13d, %r15
	movl	%r13d, 4(%rsp)          # 4-byte Spill
	movl	%r13d, %r13d
	.p2align	4, 0x90
.LBB100_14:                             # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	24(%rbx), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB100_16
# BB#15:                                #   in Loop: Header=BB100_14 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB100_16:                             # %clause_LiteralPrintUnsigned.exit85
                                        #   in Loop: Header=BB100_14 Depth=1
	callq	term_PrintPrefix
	movq	stdout(%rip), %rdi
	callq	fflush
	testb	$1, (%rbx)
	je	.LBB100_19
# BB#17:                                #   in Loop: Header=BB100_14 Depth=1
	movq	stdout(%rip), %rsi
	movl	$42, %edi
	callq	_IO_putc
	cmpl	$0, 8(%rbx)
	je	.LBB100_19
# BB#18:                                #   in Loop: Header=BB100_14 Depth=1
	movq	stdout(%rip), %rsi
	movl	$42, %edi
	callq	_IO_putc
.LBB100_19:                             #   in Loop: Header=BB100_14 Depth=1
	testb	$4, (%rbx)
	je	.LBB100_21
# BB#20:                                #   in Loop: Header=BB100_14 Depth=1
	movq	stdout(%rip), %rsi
	movl	$43, %edi
	callq	_IO_putc
.LBB100_21:                             #   in Loop: Header=BB100_14 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jge	.LBB100_22
# BB#35:                                #   in Loop: Header=BB100_14 Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
.LBB100_22:                             # %.backedge86
                                        #   in Loop: Header=BB100_14 Depth=1
	cmpl	%ebp, %r13d
	jne	.LBB100_14
# BB#23:                                # %._crit_edge91.loopexit
	movl	4(%rsp), %r13d          # 4-byte Reload
	movl	%r13d, %r15d
.LBB100_24:                             # %._crit_edge91
	movq	stdout(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movslq	%r13d, %rax
	addq	%rax, %r14
	cmpl	%r14d, %r15d
	jge	.LBB100_33
# BB#25:                                # %.lr.ph
	movslq	%r15d, %rbp
	movl	%r14d, %r15d
	.p2align	4, 0x90
.LBB100_26:                             # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	24(%rbx), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB100_28
# BB#27:                                #   in Loop: Header=BB100_26 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB100_28:                             # %clause_LiteralPrintUnsigned.exit
                                        #   in Loop: Header=BB100_26 Depth=1
	callq	term_PrintPrefix
	movq	stdout(%rip), %rdi
	callq	fflush
	testb	$1, (%rbx)
	je	.LBB100_31
# BB#29:                                #   in Loop: Header=BB100_26 Depth=1
	movq	stdout(%rip), %rsi
	movl	$42, %edi
	callq	_IO_putc
	cmpl	$0, 8(%rbx)
	je	.LBB100_31
# BB#30:                                #   in Loop: Header=BB100_26 Depth=1
	movq	stdout(%rip), %rsi
	movl	$42, %edi
	callq	_IO_putc
.LBB100_31:                             #   in Loop: Header=BB100_26 Depth=1
	incq	%rbp
	cmpq	%r14, %rbp
	jge	.LBB100_32
# BB#36:                                #   in Loop: Header=BB100_26 Depth=1
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
.LBB100_32:                             # %.backedge
                                        #   in Loop: Header=BB100_26 Depth=1
	cmpl	%ebp, %r15d
	jne	.LBB100_26
.LBB100_33:                             # %._crit_edge
	movq	stdout(%rip), %rsi
	movl	$46, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.LBB100_34:
	movq	stdout(%rip), %rcx
	movl	$.L.str, %edi
	movl	$12, %esi
	movl	$1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end100:
	.size	clause_Print, .Lfunc_end100-clause_Print
	.cfi_endproc

	.globl	clause_PrintOrigin
	.p2align	4, 0x90
	.type	clause_PrintOrigin,@function
clause_PrintOrigin:                     # @clause_PrintOrigin
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	stdout(%rip), %rdi
	movq	%rax, %rsi
	jmp	clause_FPrintOrigin     # TAILCALL
.Lfunc_end101:
	.size	clause_PrintOrigin, .Lfunc_end101-clause_PrintOrigin
	.cfi_endproc

	.globl	clause_PrintParentClauses
	.p2align	4, 0x90
	.type	clause_PrintParentClauses,@function
clause_PrintParentClauses:              # @clause_PrintParentClauses
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi731:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi732:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi733:
	.cfi_def_cfa_offset 32
.Lcfi734:
	.cfi_offset %rbx, -24
.Lcfi735:
	.cfi_offset %r14, -16
	movq	32(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB102_4
# BB#1:
	movq	40(%rdi), %r14
	movl	8(%rbx), %esi
	movl	8(%r14), %edx
	movl	$.L.str.7, %edi
	jmp	.LBB102_3
	.p2align	4, 0x90
.LBB102_2:                              # %.lr.ph
                                        #   in Loop: Header=BB102_3 Depth=1
	movq	(%r14), %r14
	movl	8(%rbx), %esi
	movl	8(%r14), %edx
	movl	$.L.str.8, %edi
.LBB102_3:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB102_2
.LBB102_4:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end102:
	.size	clause_PrintParentClauses, .Lfunc_end102-clause_PrintParentClauses
	.cfi_endproc

	.globl	clause_PrintMaxLitsOnly
	.p2align	4, 0x90
	.type	clause_PrintMaxLitsOnly,@function
clause_PrintMaxLitsOnly:                # @clause_PrintMaxLitsOnly
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi736:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi737:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi738:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi739:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi740:
	.cfi_def_cfa_offset 48
.Lcfi741:
	.cfi_offset %rbx, -48
.Lcfi742:
	.cfi_offset %r12, -40
.Lcfi743:
	.cfi_offset %r14, -32
.Lcfi744:
	.cfi_offset %r15, -24
.Lcfi745:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	64(%rbx), %r12d
	movl	68(%rbx), %r15d
	movl	72(%rbx), %r14d
	testl	%r12d, %r12d
	jle	.LBB103_1
# BB#2:                                 # %.lr.ph85
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB103_3:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movl	(%rax), %ecx
	testb	$1, %cl
	je	.LBB103_5
# BB#4:                                 #   in Loop: Header=BB103_3 Depth=1
	movq	24(%rax), %rdi
	callq	term_PrintPrefix
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movl	(%rax), %ecx
.LBB103_5:                              #   in Loop: Header=BB103_3 Depth=1
	testb	$2, %cl
	je	.LBB103_7
# BB#6:                                 #   in Loop: Header=BB103_3 Depth=1
	movq	24(%rax), %rdi
	callq	term_PrintPrefix
	movq	stdout(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
.LBB103_7:                              #   in Loop: Header=BB103_3 Depth=1
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB103_3
# BB#8:
	movl	%r12d, %ebp
	jmp	.LBB103_9
.LBB103_1:
	xorl	%ebp, %ebp
.LBB103_9:                              # %._crit_edge86
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	addl	%r12d, %r15d
	cmpl	%r15d, %ebp
	jge	.LBB103_17
# BB#10:                                # %.lr.ph81
	movslq	%ebp, %rbp
	movl	%r15d, %r12d
	.p2align	4, 0x90
.LBB103_11:                             # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movl	(%rax), %ecx
	testb	$1, %cl
	je	.LBB103_13
# BB#12:                                #   in Loop: Header=BB103_11 Depth=1
	movq	24(%rax), %rdi
	callq	term_PrintPrefix
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movl	(%rax), %ecx
.LBB103_13:                             #   in Loop: Header=BB103_11 Depth=1
	testb	$2, %cl
	je	.LBB103_15
# BB#14:                                #   in Loop: Header=BB103_11 Depth=1
	movq	24(%rax), %rdi
	callq	term_PrintPrefix
	movq	stdout(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
.LBB103_15:                             #   in Loop: Header=BB103_11 Depth=1
	incq	%rbp
	cmpl	%ebp, %r12d
	jne	.LBB103_11
# BB#16:                                # %._crit_edge82.loopexit
	movl	%r15d, %ebp
.LBB103_17:                             # %._crit_edge82
	movq	stdout(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	addl	%r15d, %r14d
	cmpl	%r14d, %ebp
	jge	.LBB103_24
# BB#18:                                # %.lr.ph
	movslq	%ebp, %rbp
	movl	%r14d, %r14d
	.p2align	4, 0x90
.LBB103_19:                             # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movl	(%rax), %ecx
	testb	$1, %cl
	je	.LBB103_21
# BB#20:                                #   in Loop: Header=BB103_19 Depth=1
	movq	24(%rax), %rdi
	callq	term_PrintPrefix
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movl	(%rax), %ecx
.LBB103_21:                             #   in Loop: Header=BB103_19 Depth=1
	testb	$2, %cl
	je	.LBB103_23
# BB#22:                                #   in Loop: Header=BB103_19 Depth=1
	movq	24(%rax), %rdi
	callq	term_PrintPrefix
	movq	stdout(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
.LBB103_23:                             #   in Loop: Header=BB103_19 Depth=1
	incq	%rbp
	cmpl	%ebp, %r14d
	jne	.LBB103_19
.LBB103_24:                             # %._crit_edge
	movl	$.L.str.6, %edi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	puts                    # TAILCALL
.Lfunc_end103:
	.size	clause_PrintMaxLitsOnly, .Lfunc_end103-clause_PrintMaxLitsOnly
	.cfi_endproc

	.globl	clause_FPrint
	.p2align	4, 0x90
	.type	clause_FPrint,@function
clause_FPrint:                          # @clause_FPrint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi746:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi747:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi748:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi749:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi750:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi751:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi752:
	.cfi_def_cfa_offset 64
.Lcfi753:
	.cfi_offset %rbx, -56
.Lcfi754:
	.cfi_offset %r12, -48
.Lcfi755:
	.cfi_offset %r13, -40
.Lcfi756:
	.cfi_offset %r14, -32
.Lcfi757:
	.cfi_offset %r15, -24
.Lcfi758:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	64(%rbx), %r15d
	movl	68(%rbx), %r12d
	movl	72(%rbx), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	%r15d, %r15d
	jle	.LBB104_1
# BB#2:                                 # %.lr.ph63
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB104_3:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB104_5
# BB#4:                                 #   in Loop: Header=BB104_3 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB104_5:                              # %clause_GetLiteralAtom.exit55
                                        #   in Loop: Header=BB104_3 Depth=1
	movq	%r14, %rdi
	callq	term_FPrint
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB104_3
# BB#6:
	movl	%r15d, %r13d
	jmp	.LBB104_7
.LBB104_1:
	xorl	%r13d, %r13d
.LBB104_7:                              # %._crit_edge64
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	addl	%r15d, %r12d
	cmpl	%r12d, %r13d
	jge	.LBB104_8
# BB#9:                                 # %.lr.ph59
	movslq	%r13d, %rbp
	movl	%r12d, %r13d
	movl	4(%rsp), %r15d          # 4-byte Reload
	.p2align	4, 0x90
.LBB104_10:                             # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB104_12
# BB#11:                                #   in Loop: Header=BB104_10 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB104_12:                             # %clause_GetLiteralAtom.exit45
                                        #   in Loop: Header=BB104_10 Depth=1
	movq	%r14, %rdi
	callq	term_FPrint
	incq	%rbp
	cmpl	%ebp, %r13d
	jne	.LBB104_10
# BB#13:                                # %._crit_edge60.loopexit
	movl	%r12d, %r13d
	jmp	.LBB104_14
.LBB104_8:
	movl	4(%rsp), %r15d          # 4-byte Reload
.LBB104_14:                             # %._crit_edge60
	movq	stdout(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	addl	%r12d, %r15d
	cmpl	%r15d, %r13d
	jge	.LBB104_19
# BB#15:                                # %.lr.ph
	movslq	%r13d, %rbp
	movl	%r15d, %r15d
	.p2align	4, 0x90
.LBB104_16:                             # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB104_18
# BB#17:                                #   in Loop: Header=BB104_16 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB104_18:                             # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB104_16 Depth=1
	movq	%r14, %rdi
	callq	term_FPrint
	incq	%rbp
	cmpl	%ebp, %r15d
	jne	.LBB104_16
.LBB104_19:                             # %._crit_edge
	movl	$46, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.Lfunc_end104:
	.size	clause_FPrint, .Lfunc_end104-clause_FPrint
	.cfi_endproc

	.globl	clause_GetOriginFromString
	.p2align	4, 0x90
	.type	clause_GetOriginFromString,@function
clause_GetOriginFromString:             # @clause_GetOriginFromString
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi759:
	.cfi_def_cfa_offset 16
.Lcfi760:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.9, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_1
# BB#2:
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_3
# BB#4:
	movl	$.L.str.11, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_5
# BB#6:
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_7
# BB#8:
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_9
# BB#10:
	movl	$.L.str.14, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_11
# BB#12:
	movl	$.L.str.15, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_13
# BB#14:
	movl	$.L.str.16, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_15
# BB#16:
	movl	$.L.str.17, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_17
# BB#18:
	movl	$.L.str.18, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_19
# BB#20:
	movl	$.L.str.19, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_21
# BB#22:
	movl	$.L.str.20, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_23
# BB#24:
	movl	$.L.str.21, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_25
# BB#26:
	movl	$.L.str.22, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_27
# BB#28:
	movl	$.L.str.23, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_29
# BB#30:
	movl	$.L.str.24, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_31
# BB#32:
	movl	$.L.str.25, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_33
# BB#34:
	movl	$.L.str.26, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_35
# BB#36:
	movl	$.L.str.27, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_37
# BB#38:
	movl	$.L.str.28, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_39
# BB#40:
	movl	$.L.str.29, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_41
# BB#42:
	movl	$.L.str.30, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_43
# BB#44:
	movl	$.L.str.31, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_45
# BB#46:
	movl	$.L.str.32, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_47
# BB#48:
	movl	$.L.str.33, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_49
# BB#50:
	movl	$.L.str.34, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB105_51
# BB#52:
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	strcmp
	movl	%eax, %ecx
	movl	$26, %eax
	testl	%ecx, %ecx
	jne	.LBB105_54
# BB#53:
	popq	%rbx
	retq
.LBB105_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB105_3:
	movl	$1, %eax
	popq	%rbx
	retq
.LBB105_5:
	movl	$2, %eax
	popq	%rbx
	retq
.LBB105_7:
	movl	$3, %eax
	popq	%rbx
	retq
.LBB105_9:
	movl	$4, %eax
	popq	%rbx
	retq
.LBB105_11:
	movl	$5, %eax
	popq	%rbx
	retq
.LBB105_13:
	movl	$8, %eax
	popq	%rbx
	retq
.LBB105_15:
	movl	$6, %eax
	popq	%rbx
	retq
.LBB105_17:
	movl	$7, %eax
	popq	%rbx
	retq
.LBB105_19:
	movl	$9, %eax
	popq	%rbx
	retq
.LBB105_21:
	movl	$13, %eax
	popq	%rbx
	retq
.LBB105_23:
	movl	$10, %eax
	popq	%rbx
	retq
.LBB105_25:
	movl	$11, %eax
	popq	%rbx
	retq
.LBB105_27:
	movl	$12, %eax
	popq	%rbx
	retq
.LBB105_29:
	movl	$14, %eax
	popq	%rbx
	retq
.LBB105_31:
	movl	$15, %eax
	popq	%rbx
	retq
.LBB105_33:
	movl	$16, %eax
	popq	%rbx
	retq
.LBB105_35:
	movl	$21, %eax
	popq	%rbx
	retq
.LBB105_37:
	movl	$22, %eax
	popq	%rbx
	retq
.LBB105_39:
	movl	$17, %eax
	popq	%rbx
	retq
.LBB105_41:
	movl	$18, %eax
	popq	%rbx
	retq
.LBB105_43:
	movl	$19, %eax
	popq	%rbx
	retq
.LBB105_45:
	movl	$20, %eax
	popq	%rbx
	retq
.LBB105_47:
	movl	$23, %eax
	popq	%rbx
	retq
.LBB105_49:
	movl	$24, %eax
	popq	%rbx
	retq
.LBB105_51:
	movl	$25, %eax
	popq	%rbx
	retq
.LBB105_54:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.36, %esi
	movl	$.L.str.37, %edx
	movl	$3811, %ecx             # imm = 0xEE3
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end105:
	.size	clause_GetOriginFromString, .Lfunc_end105-clause_GetOriginFromString
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi761:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.83, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end106:
	.size	misc_DumpCore, .Lfunc_end106-misc_DumpCore
	.cfi_endproc

	.globl	clause_FPrintOrigin
	.p2align	4, 0x90
	.type	clause_FPrintOrigin,@function
clause_FPrintOrigin:                    # @clause_FPrintOrigin
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movl	76(%rsi), %ecx
	cmpq	$27, %rcx
	ja	.LBB107_32
# BB#1:
	jmpq	*.LJTI107_0(,%rcx,8)
.LBB107_2:
	movl	$.L.str.9, %edi
	jmp	.LBB107_3
.LBB107_5:
	movl	$.L.str.10, %edi
	jmp	.LBB107_3
.LBB107_6:
	movl	$.L.str.11, %edi
	jmp	.LBB107_3
.LBB107_7:
	movl	$.L.str.12, %edi
	jmp	.LBB107_3
.LBB107_8:
	movl	$.L.str.13, %edi
	jmp	.LBB107_3
.LBB107_9:
	movl	$.L.str.14, %edi
	jmp	.LBB107_3
.LBB107_11:
	movl	$.L.str.16, %edi
	jmp	.LBB107_3
.LBB107_12:
	movl	$.L.str.17, %edi
	jmp	.LBB107_3
.LBB107_10:
	movl	$.L.str.15, %edi
	jmp	.LBB107_3
.LBB107_13:
	movl	$.L.str.18, %edi
	jmp	.LBB107_3
.LBB107_15:
	movl	$.L.str.20, %edi
	jmp	.LBB107_3
.LBB107_16:
	movl	$.L.str.21, %edi
	jmp	.LBB107_3
.LBB107_17:
	movl	$.L.str.22, %edi
	jmp	.LBB107_3
.LBB107_14:
	movl	$.L.str.19, %edi
	jmp	.LBB107_3
.LBB107_18:
	movl	$.L.str.23, %edi
	jmp	.LBB107_3
.LBB107_19:
	movl	$.L.str.24, %edi
	jmp	.LBB107_3
.LBB107_20:
	movl	$.L.str.25, %edi
	jmp	.LBB107_3
.LBB107_23:
	movl	$.L.str.28, %edi
	jmp	.LBB107_3
.LBB107_24:
	movl	$.L.str.29, %edi
	jmp	.LBB107_3
.LBB107_25:
	movl	$.L.str.30, %edi
	jmp	.LBB107_3
.LBB107_26:
	movl	$.L.str.31, %edi
	jmp	.LBB107_3
.LBB107_21:
	movl	$.L.str.26, %edi
	jmp	.LBB107_3
.LBB107_22:
	movl	$.L.str.27, %edi
	jmp	.LBB107_3
.LBB107_27:
	movl	$.L.str.32, %edi
	jmp	.LBB107_3
.LBB107_28:
	movl	$.L.str.33, %edi
	jmp	.LBB107_3
.LBB107_29:
	movl	$.L.str.34, %edi
	jmp	.LBB107_3
.LBB107_30:
	movl	$.L.str.35, %edi
.LBB107_3:
	movl	$3, %esi
.LBB107_4:
	movl	$1, %edx
	movq	%rax, %rcx
	jmp	fwrite                  # TAILCALL
.LBB107_31:
	movl	$.L.str.40, %edi
	movl	$9, %esi
	jmp	.LBB107_4
.LBB107_32:
	pushq	%rax
.Lcfi762:
	.cfi_def_cfa_offset 16
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.36, %esi
	movl	$.L.str.37, %edx
	movl	$3859, %ecx             # imm = 0xF13
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end107:
	.size	clause_FPrintOrigin, .Lfunc_end107-clause_FPrintOrigin
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI107_0:
	.quad	.LBB107_2
	.quad	.LBB107_5
	.quad	.LBB107_6
	.quad	.LBB107_7
	.quad	.LBB107_8
	.quad	.LBB107_9
	.quad	.LBB107_11
	.quad	.LBB107_12
	.quad	.LBB107_10
	.quad	.LBB107_13
	.quad	.LBB107_15
	.quad	.LBB107_16
	.quad	.LBB107_17
	.quad	.LBB107_14
	.quad	.LBB107_18
	.quad	.LBB107_19
	.quad	.LBB107_20
	.quad	.LBB107_23
	.quad	.LBB107_24
	.quad	.LBB107_25
	.quad	.LBB107_26
	.quad	.LBB107_21
	.quad	.LBB107_22
	.quad	.LBB107_27
	.quad	.LBB107_28
	.quad	.LBB107_29
	.quad	.LBB107_30
	.quad	.LBB107_31

	.text
	.globl	clause_PrintVerbose
	.p2align	4, 0x90
	.type	clause_PrintVerbose,@function
clause_PrintVerbose:                    # @clause_PrintVerbose
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi763:
	.cfi_def_cfa_offset 16
.Lcfi764:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	64(%rbx), %esi
	movl	68(%rbx), %edx
	movl	72(%rbx), %ecx
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	printf
	movl	4(%rbx), %esi
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	printf
	movl	8(%rbx), %esi
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	printf
	movl	48(%rbx), %eax
	movl	$.L.str.46, %ecx
	movl	$.L.str.47, %esi
	testb	$1, %al
	cmovneq	%rcx, %rsi
	movl	$.L.str.48, %ecx
	movl	$.L.str.49, %edx
	testb	$2, %al
	cmovneq	%rcx, %rdx
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	printf
	movq	%rbx, %rdi
	popq	%rbx
	jmp	clause_Print            # TAILCALL
.Lfunc_end108:
	.size	clause_PrintVerbose, .Lfunc_end108-clause_PrintVerbose
	.cfi_endproc

	.globl	clause_GetNumberedCl
	.p2align	4, 0x90
	.type	clause_GetNumberedCl,@function
clause_GetNumberedCl:                   # @clause_GetNumberedCl
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	jne	.LBB109_2
	jmp	.LBB109_4
	.p2align	4, 0x90
.LBB109_3:                              #   in Loop: Header=BB109_2 Depth=1
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB109_4
.LBB109_2:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsi), %rax
	cmpl	%edi, (%rax)
	jne	.LBB109_3
	jmp	.LBB109_5
.LBB109_4:
	xorl	%eax, %eax
.LBB109_5:                              # %.critedge
	retq
.Lfunc_end109:
	.size	clause_GetNumberedCl, .Lfunc_end109-clause_GetNumberedCl
	.cfi_endproc

	.globl	clause_NumberSort
	.p2align	4, 0x90
	.type	clause_NumberSort,@function
clause_NumberSort:                      # @clause_NumberSort
	.cfi_startproc
# BB#0:
	movl	$clause_NumberLower, %esi
	jmp	list_Sort               # TAILCALL
.Lfunc_end110:
	.size	clause_NumberSort, .Lfunc_end110-clause_NumberSort
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_NumberLower,@function
clause_NumberLower:                     # @clause_NumberLower
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	(%rsi), %ecx
	setl	%al
	retq
.Lfunc_end111:
	.size	clause_NumberLower, .Lfunc_end111-clause_NumberLower
	.cfi_endproc

	.globl	clause_NumberDelete
	.p2align	4, 0x90
	.type	clause_NumberDelete,@function
clause_NumberDelete:                    # @clause_NumberDelete
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi765:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi766:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi767:
	.cfi_def_cfa_offset 32
.Lcfi768:
	.cfi_offset %rbx, -24
.Lcfi769:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	%rbx, %rax
	jmp	.LBB112_1
	.p2align	4, 0x90
.LBB112_4:                              #   in Loop: Header=BB112_1 Depth=1
	movq	%rax, %rdi
	callq	list_PointerDeleteOneElement
.LBB112_1:                              # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB112_2 Depth 2
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB112_2:                              #   Parent Loop BB112_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rcx, %rcx
	je	.LBB112_5
# BB#3:                                 #   in Loop: Header=BB112_2 Depth=2
	movq	(%rcx), %rbx
	movq	8(%rcx), %rsi
	cmpl	%ebp, (%rsi)
	movq	%rbx, %rcx
	jne	.LBB112_2
	jmp	.LBB112_4
.LBB112_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end112:
	.size	clause_NumberDelete, .Lfunc_end112-clause_NumberDelete
	.cfi_endproc

	.globl	clause_NumberOfMaxAntecedentLits
	.p2align	4, 0x90
	.type	clause_NumberOfMaxAntecedentLits,@function
clause_NumberOfMaxAntecedentLits:       # @clause_NumberOfMaxAntecedentLits
	.cfi_startproc
# BB#0:
	movl	64(%rdi), %ecx
	movl	68(%rdi), %eax
	leal	-1(%rcx,%rax), %edx
	xorl	%eax, %eax
	cmpl	%edx, %ecx
	ja	.LBB113_3
# BB#1:                                 # %.lr.ph
	movq	56(%rdi), %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB113_2:                              # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rcx
	movq	(%rsi,%rcx,8), %rdi
	movl	(%rdi), %edi
	andl	$1, %edi
	addl	%edi, %eax
	incl	%ecx
	cmpl	%edx, %ecx
	jbe	.LBB113_2
.LBB113_3:                              # %._crit_edge
	retq
.Lfunc_end113:
	.size	clause_NumberOfMaxAntecedentLits, .Lfunc_end113-clause_NumberOfMaxAntecedentLits
	.cfi_endproc

	.globl	clause_SelectLiteral
	.p2align	4, 0x90
	.type	clause_SelectLiteral,@function
clause_SelectLiteral:                   # @clause_SelectLiteral
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi770:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi771:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi772:
	.cfi_def_cfa_offset 32
.Lcfi773:
	.cfi_offset %rbx, -24
.Lcfi774:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB114_20
# BB#1:
	testb	$2, 48(%r14)
	jne	.LBB114_20
# BB#2:
	movl	68(%r14), %r9d
	testl	%r9d, %r9d
	jle	.LBB114_20
# BB#3:
	movl	152(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB114_6
# BB#4:
	cmpl	$2, %eax
	jne	.LBB114_20
# BB#5:                                 # %._crit_edge
	movl	64(%r14), %eax
	leaq	56(%r14), %r8
	movq	56(%r14), %r11
.LBB114_15:
	leal	-1(%rax,%r9), %ecx
.LBB114_16:                             # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB114_17 Depth 2
	movslq	%eax, %rdi
	movq	(%r11,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB114_17:                             #   Parent Loop BB114_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%eax
	cmpl	%ecx, %eax
	ja	.LBB114_19
# BB#18:                                #   in Loop: Header=BB114_17 Depth=2
	movl	4(%rdi), %ebx
	movq	(%r8), %rdx
	movslq	%eax, %rsi
	movq	(%rdx,%rsi,8), %rdx
	cmpl	4(%rdx), %ebx
	jae	.LBB114_17
	jmp	.LBB114_16
.LBB114_19:
	orb	$4, (%rdi)
	orb	$2, 48(%r14)
	jmp	.LBB114_20
.LBB114_6:
	movl	64(%r14), %eax
	movl	72(%r14), %edi
	leal	(%rax,%r9), %r10d
	addl	%edi, %r10d
	cmpl	%r10d, %eax
	jae	.LBB114_20
# BB#7:                                 # %.lr.ph.i
	movq	56(%r14), %r11
	leal	(%r9,%rdi), %ecx
	leal	-1(%r9,%rdi), %r8d
	andl	$3, %ecx
	je	.LBB114_8
# BB#9:                                 # %.prol.preheader
	negl	%ecx
	xorl	%ebx, %ebx
	movl	%eax, %edi
	.p2align	4, 0x90
.LBB114_10:                             # =>This Inner Loop Header: Depth=1
	movslq	%edi, %rdi
	movq	(%r11,%rdi,8), %rsi
	movl	(%rsi), %esi
	andl	$1, %esi
	addl	%esi, %ebx
	incl	%edi
	incl	%ecx
	jne	.LBB114_10
	jmp	.LBB114_11
.LBB114_8:
	xorl	%ebx, %ebx
	movl	%eax, %edi
.LBB114_11:                             # %.prol.loopexit
	cmpl	$3, %r8d
	jb	.LBB114_13
	.p2align	4, 0x90
.LBB114_12:                             # =>This Inner Loop Header: Depth=1
	movslq	%edi, %rdi
	movq	(%r11,%rdi,8), %rcx
	movl	(%rcx), %ecx
	andl	$1, %ecx
	addl	%ebx, %ecx
	leal	1(%rdi), %esi
	movslq	%esi, %rsi
	movq	(%r11,%rsi,8), %rsi
	movl	(%rsi), %esi
	andl	$1, %esi
	addl	%ecx, %esi
	leal	2(%rdi), %ecx
	movslq	%ecx, %rcx
	movq	(%r11,%rcx,8), %rcx
	movl	(%rcx), %ecx
	andl	$1, %ecx
	addl	%esi, %ecx
	leal	3(%rdi), %esi
	movslq	%esi, %rsi
	movq	(%r11,%rsi,8), %rsi
	movl	(%rsi), %ebx
	andl	$1, %ebx
	addl	%ecx, %ebx
	addl	$4, %edi
	cmpl	%edi, %r10d
	jne	.LBB114_12
.LBB114_13:                             # %clause_NumberOfMaxLits.exit
	cmpl	$2, %ebx
	jb	.LBB114_20
# BB#14:
	leaq	56(%r14), %r8
	jmp	.LBB114_15
.LBB114_20:                             # %clause_NumberOfMaxLits.exit.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end114:
	.size	clause_SelectLiteral, .Lfunc_end114-clause_SelectLiteral
	.cfi_endproc

	.globl	clause_SetSpecialFlags
	.p2align	4, 0x90
	.type	clause_SetSpecialFlags,@function
clause_SetSpecialFlags:                 # @clause_SetSpecialFlags
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi775:
	.cfi_def_cfa_offset 16
.Lcfi776:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testl	%esi, %esi
	je	.LBB115_7
# BB#1:
	cmpl	$0, 68(%rbx)
	jg	.LBB115_7
# BB#2:
	cmpl	$1, 72(%rbx)
	jg	.LBB115_7
# BB#3:
	movq	%rbx, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB115_7
# BB#4:                                 # %clause_IsSortTheoryClause.exit
	movslq	68(%rbx), %rax
	movslq	64(%rbx), %rcx
	addq	%rax, %rcx
	movq	56(%rbx), %rax
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
	movb	symbol_TYPESTATBITS(%rip), %cl
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB115_7
# BB#5:
	testb	$32, 20(%rax)
	je	.LBB115_7
# BB#6:
	orb	$32, 48(%rbx)
.LBB115_7:                              # %clause_IsSortTheoryClause.exit.thread
	popq	%rbx
	retq
.Lfunc_end115:
	.size	clause_SetSpecialFlags, .Lfunc_end115-clause_SetSpecialFlags
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI116_0:
	.zero	16
	.text
	.globl	clause_ContainsPotPredDef
	.p2align	4, 0x90
	.type	clause_ContainsPotPredDef,@function
clause_ContainsPotPredDef:              # @clause_ContainsPotPredDef
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi777:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi778:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi779:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi780:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi781:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi782:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi783:
	.cfi_def_cfa_offset 112
.Lcfi784:
	.cfi_offset %rbx, -56
.Lcfi785:
	.cfi_offset %r12, -48
.Lcfi786:
	.cfi_offset %r13, -40
.Lcfi787:
	.cfi_offset %r14, -32
.Lcfi788:
	.cfi_offset %r15, -24
.Lcfi789:
	.cfi_offset %rbp, -16
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movl	64(%r13), %ebp
	movl	68(%r13), %ecx
	leal	(%rcx,%rbp), %ebx
	movl	72(%r13), %edx
	leal	(%rbx,%rdx), %esi
	xorl	%eax, %eax
	cmpl	%esi, %ebx
	jae	.LBB116_75
# BB#1:                                 # %.lr.ph284
	movl	symbol_TYPEMASK(%rip), %r15d
	movq	%r13, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB116_2:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB116_5 Depth 2
                                        #     Child Loop BB116_10 Depth 2
                                        #     Child Loop BB116_19 Depth 2
                                        #       Child Loop BB116_34 Depth 3
                                        #         Child Loop BB116_35 Depth 4
                                        #       Child Loop BB116_41 Depth 3
                                        #     Child Loop BB116_24 Depth 2
                                        #       Child Loop BB116_27 Depth 3
                                        #     Child Loop BB116_45 Depth 2
                                        #       Child Loop BB116_55 Depth 3
                                        #         Child Loop BB116_56 Depth 4
                                        #       Child Loop BB116_62 Depth 3
                                        #     Child Loop BB116_66 Depth 2
                                        #     Child Loop BB116_69 Depth 2
                                        #     Child Loop BB116_71 Depth 2
	movq	56(%r13), %rsi
	movslq	%ebx, %rdi
	movq	(%rsi,%rdi,8), %rsi
	movq	24(%rsi), %r14
	movl	(%r14), %esi
	testl	%esi, %esi
	jns	.LBB116_73
# BB#3:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB116_2 Depth=1
	negl	%esi
	andl	%r15d, %esi
	cmpl	$2, %esi
	jne	.LBB116_73
# BB#4:                                 #   in Loop: Header=BB116_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movups	%xmm0, (%rax)
	movq	16(%r14), %rax
	testq	%rax, %rax
	jne	.LBB116_5
	jmp	.LBB116_8
	.p2align	4, 0x90
.LBB116_6:                              #   in Loop: Header=BB116_5 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB116_8
.LBB116_5:                              # %.lr.ph
                                        #   Parent Loop BB116_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	movl	(%rcx), %ecx
	decl	%ecx
	cmpl	$2000, %ecx             # imm = 0x7D0
	jb	.LBB116_6
	jmp	.LBB116_70
.LBB116_8:                              # %.preheader222
                                        #   in Loop: Header=BB116_2 Depth=1
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB116_15
# BB#9:                                 # %.lr.ph232
                                        #   in Loop: Header=BB116_2 Depth=1
	movq	56(%r13), %r8
	movl	fol_NOT(%rip), %edx
	movl	(%r14), %edi
	xorl	%esi, %esi
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB116_10:                             #   Parent Loop BB116_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rbx), %ecx
	movslq	%ecx, %rcx
	movq	(%r8,%rcx,8), %rcx
	movq	24(%rcx), %rcx
	movl	(%rcx), %ebp
	cmpl	%ebp, %edx
	jne	.LBB116_12
# BB#11:                                #   in Loop: Header=BB116_10 Depth=2
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ebp
.LBB116_12:                             # %clause_GetLiteralAtom.exit204
                                        #   in Loop: Header=BB116_10 Depth=2
	xorl	%ecx, %ecx
	cmpl	%edi, %ebp
	sete	%cl
	addl	%ecx, %esi
	cmpl	$1, %esi
	ja	.LBB116_14
# BB#13:                                # %clause_GetLiteralAtom.exit204
                                        #   in Loop: Header=BB116_10 Depth=2
	cmpl	%eax, %ebx
	leal	1(%rbx), %ecx
	movl	%ecx, %ebx
	jb	.LBB116_10
.LBB116_14:                             # %._crit_edge
                                        #   in Loop: Header=BB116_2 Depth=1
	cmpl	$1, %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	ja	.LBB116_70
.LBB116_15:                             # %._crit_edge.thread
                                        #   in Loop: Header=BB116_2 Depth=1
	movq	%r14, %rdi
	callq	fol_FreeVariables
	movq	%rax, %r15
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	movl	%eax, %edx
	addl	%ecx, %edx
	movq	%r15, 24(%rsp)          # 8-byte Spill
	jne	.LBB116_17
# BB#16:                                #   in Loop: Header=BB116_2 Depth=1
	xorl	%r12d, %r12d
	movl	$1, %esi
	testl	%esi, %esi
	jne	.LBB116_44
	jmp	.LBB116_32
.LBB116_17:                             # %.lr.ph243
                                        #   in Loop: Header=BB116_2 Depth=1
	movl	$1, %esi
	testq	%r15, %r15
	je	.LBB116_23
# BB#18:                                # %.lr.ph243.split.preheader
                                        #   in Loop: Header=BB116_2 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB116_19:                             # %.lr.ph243.split
                                        #   Parent Loop BB116_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB116_34 Depth 3
                                        #         Child Loop BB116_35 Depth 4
                                        #       Child Loop BB116_41 Depth 3
	movq	56(%r13), %rax
	movslq	%r14d, %r12
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rbp
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbp), %eax
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%r13, %r15
	jne	.LBB116_21
# BB#20:                                #   in Loop: Header=BB116_19 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB116_21:                             # %clause_GetLiteralAtom.exit194
                                        #   in Loop: Header=BB116_19 Depth=2
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	8(%rbx), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, 8(%rbx)
	movq	%r15, %r13
	movq	56(%r13), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rdi
	callq	fol_FreeVariables
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB116_22
# BB#33:                                # %.lr.ph234.split.preheader
                                        #   in Loop: Header=BB116_19 Depth=2
	movq	%rbx, %r13
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB116_34:                             # %.lr.ph234.split
                                        #   Parent Loop BB116_2 Depth=1
                                        #     Parent Loop BB116_19 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB116_35 Depth 4
	movq	8(%r13), %rbp
	movq	%r15, %r12
	.p2align	4, 0x90
.LBB116_35:                             # %.lr.ph.i.i178
                                        #   Parent Loop BB116_2 Depth=1
                                        #     Parent Loop BB116_19 Depth=2
                                        #       Parent Loop BB116_34 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%r12), %rsi
	movq	%rbp, %rdi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB116_38
# BB#36:                                #   in Loop: Header=BB116_35 Depth=4
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB116_35
	jmp	.LBB116_37
	.p2align	4, 0x90
.LBB116_38:                             # %term_ListContainsTerm.exit182
                                        #   in Loop: Header=BB116_34 Depth=3
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB116_34
# BB#39:                                #   in Loop: Header=BB116_19 Depth=2
	movl	4(%rsp), %esi           # 4-byte Reload
	jmp	.LBB116_40
	.p2align	4, 0x90
.LBB116_37:                             #   in Loop: Header=BB116_19 Depth=2
	xorl	%esi, %esi
.LBB116_40:                             # %term_ListContainsTerm.exit182.thread
                                        #   in Loop: Header=BB116_19 Depth=2
	testq	%rbx, %rbx
	movq	32(%rsp), %r13          # 8-byte Reload
	je	.LBB116_42
	.p2align	4, 0x90
.LBB116_41:                             # %.lr.ph.i173
                                        #   Parent Loop BB116_2 Depth=1
                                        #     Parent Loop BB116_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB116_41
	jmp	.LBB116_42
	.p2align	4, 0x90
.LBB116_22:                             #   in Loop: Header=BB116_19 Depth=2
	movq	24(%rsp), %r15          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
.LBB116_42:                             # %list_Delete.exit174
                                        #   in Loop: Header=BB116_19 Depth=2
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	leal	(%rax,%rcx), %r12d
	testl	%esi, %esi
	je	.LBB116_31
# BB#43:                                # %list_Delete.exit174
                                        #   in Loop: Header=BB116_19 Depth=2
	incl	%r14d
	cmpl	%r12d, %r14d
	jb	.LBB116_19
	jmp	.LBB116_31
.LBB116_23:                             # %.lr.ph243.split.us.preheader
                                        #   in Loop: Header=BB116_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB116_24:                             # %.lr.ph243.split.us
                                        #   Parent Loop BB116_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB116_27 Depth 3
	movq	56(%r13), %rax
	movslq	%ebx, %r14
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rbp
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbp), %eax
	movl	%esi, 4(%rsp)           # 4-byte Spill
	jne	.LBB116_26
# BB#25:                                #   in Loop: Header=BB116_24 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB116_26:                             # %clause_GetLiteralAtom.exit194.us
                                        #   in Loop: Header=BB116_24 Depth=2
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	8(%r13), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, 8(%r13)
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	56(%r13), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	callq	fol_FreeVariables
	testq	%rax, %rax
	movl	4(%rsp), %esi           # 4-byte Reload
	je	.LBB116_29
	.p2align	4, 0x90
.LBB116_27:                             # %.lr.ph.i173.us
                                        #   Parent Loop BB116_2 Depth=1
                                        #     Parent Loop BB116_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB116_27
# BB#28:                                #   in Loop: Header=BB116_24 Depth=2
	xorl	%esi, %esi
.LBB116_29:                             # %list_Delete.exit174.us
                                        #   in Loop: Header=BB116_24 Depth=2
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	leal	(%rax,%rcx), %r12d
	testl	%esi, %esi
	je	.LBB116_31
# BB#30:                                # %list_Delete.exit174.us
                                        #   in Loop: Header=BB116_24 Depth=2
	incl	%ebx
	cmpl	%r12d, %ebx
	jb	.LBB116_24
.LBB116_31:                             # %.preheader
                                        #   in Loop: Header=BB116_2 Depth=1
	testl	%esi, %esi
	je	.LBB116_32
.LBB116_44:                             # %.preheader
                                        #   in Loop: Header=BB116_2 Depth=1
	addl	%ecx, %eax
	addl	72(%r13), %eax
	cmpl	%eax, %r12d
	movq	16(%rsp), %rbx          # 8-byte Reload
	jae	.LBB116_65
	.p2align	4, 0x90
.LBB116_45:                             #   Parent Loop BB116_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB116_55 Depth 3
                                        #         Child Loop BB116_56 Depth 4
                                        #       Child Loop BB116_62 Depth 3
	cmpl	%ebx, %r12d
	je	.LBB116_63
# BB#46:                                #   in Loop: Header=BB116_45 Depth=2
	movq	56(%r13), %rax
	movslq	%r12d, %r14
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbx), %eax
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%r13, %r15
	jne	.LBB116_48
# BB#47:                                #   in Loop: Header=BB116_45 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB116_48:                             # %clause_GetLiteralAtom.exit169
                                        #   in Loop: Header=BB116_45 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbp), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, (%rbp)
	movq	%r15, %r13
	movq	56(%r13), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB116_50
# BB#49:                                #   in Loop: Header=BB116_45 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB116_50:                             # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB116_45 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	callq	fol_FreeVariables
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB116_51
# BB#52:                                # %.lr.ph265
                                        #   in Loop: Header=BB116_45 Depth=2
	testq	%r15, %r15
	je	.LBB116_53
# BB#54:                                # %.lr.ph265.split.preheader
                                        #   in Loop: Header=BB116_45 Depth=2
	movq	%r14, %r13
.LBB116_55:                             # %.lr.ph265.split
                                        #   Parent Loop BB116_2 Depth=1
                                        #     Parent Loop BB116_45 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB116_56 Depth 4
	movq	8(%r13), %rbx
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB116_56:                             # %.lr.ph.i.i
                                        #   Parent Loop BB116_2 Depth=1
                                        #     Parent Loop BB116_45 Depth=2
                                        #       Parent Loop BB116_55 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB116_59
# BB#57:                                #   in Loop: Header=BB116_56 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB116_56
	jmp	.LBB116_58
	.p2align	4, 0x90
.LBB116_59:                             # %term_ListContainsTerm.exit
                                        #   in Loop: Header=BB116_55 Depth=3
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB116_55
# BB#60:                                #   in Loop: Header=BB116_45 Depth=2
	movl	4(%rsp), %esi           # 4-byte Reload
	jmp	.LBB116_61
.LBB116_58:                             #   in Loop: Header=BB116_45 Depth=2
	xorl	%esi, %esi
.LBB116_61:                             # %term_ListContainsTerm.exit.thread
                                        #   in Loop: Header=BB116_45 Depth=2
	testq	%r14, %r14
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB116_62
	jmp	.LBB116_63
.LBB116_51:                             #   in Loop: Header=BB116_45 Depth=2
	movl	4(%rsp), %esi           # 4-byte Reload
	testl	%esi, %esi
	jne	.LBB116_64
	jmp	.LBB116_65
.LBB116_53:                             #   in Loop: Header=BB116_45 Depth=2
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB116_62:                             # %.lr.ph.i157
                                        #   Parent Loop BB116_2 Depth=1
                                        #     Parent Loop BB116_45 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB116_62
.LBB116_63:                             # %list_Delete.exit158
                                        #   in Loop: Header=BB116_45 Depth=2
	testl	%esi, %esi
	je	.LBB116_65
.LBB116_64:                             # %list_Delete.exit158
                                        #   in Loop: Header=BB116_45 Depth=2
	incl	%r12d
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	cmpl	%eax, %r12d
	jb	.LBB116_45
	jmp	.LBB116_65
.LBB116_32:                             #   in Loop: Header=BB116_2 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB116_65:                             # %._crit_edge275
                                        #   in Loop: Header=BB116_2 Depth=1
	testq	%r15, %r15
	je	.LBB116_67
	.p2align	4, 0x90
.LBB116_66:                             # %.lr.ph.i152
                                        #   Parent Loop BB116_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB116_66
.LBB116_67:                             # %list_Delete.exit153
                                        #   in Loop: Header=BB116_2 Depth=1
	testl	%esi, %esi
	jne	.LBB116_76
# BB#68:                                # %list_Delete.exit153.thread
                                        #   in Loop: Header=BB116_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rax
	testq	%rax, %rax
	movl	symbol_TYPEMASK(%rip), %r15d
	je	.LBB116_70
	.p2align	4, 0x90
.LBB116_69:                             # %.lr.ph.i147
                                        #   Parent Loop BB116_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB116_69
.LBB116_70:                             # %list_Delete.exit148
                                        #   in Loop: Header=BB116_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB116_72
	.p2align	4, 0x90
.LBB116_71:                             # %.lr.ph.i
                                        #   Parent Loop BB116_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB116_71
.LBB116_72:                             # %.thread208
                                        #   in Loop: Header=BB116_2 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, (%rcx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
	movl	64(%r13), %ebp
	movl	68(%r13), %ecx
	movl	72(%r13), %edx
	.p2align	4, 0x90
.LBB116_73:                             # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB116_2 Depth=1
	incl	%ebx
	leal	(%rcx,%rbp), %esi
	addl	%edx, %esi
	cmpl	%esi, %ebx
	jb	.LBB116_2
# BB#74:
	xorl	%eax, %eax
.LBB116_75:                             # %.loopexit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB116_76:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%ebx, (%rax)
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$1, %eax
	jmp	.LBB116_75
.Lfunc_end116:
	.size	clause_ContainsPotPredDef, .Lfunc_end116-clause_ContainsPotPredDef
	.cfi_endproc

	.globl	clause_IsPartOfDefinition
	.p2align	4, 0x90
	.type	clause_IsPartOfDefinition,@function
clause_IsPartOfDefinition:              # @clause_IsPartOfDefinition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi790:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi791:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi792:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi793:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi794:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi795:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi796:
	.cfi_def_cfa_offset 80
.Lcfi797:
	.cfi_offset %rbx, -56
.Lcfi798:
	.cfi_offset %r12, -48
.Lcfi799:
	.cfi_offset %r13, -40
.Lcfi800:
	.cfi_offset %r14, -32
.Lcfi801:
	.cfi_offset %r15, -24
.Lcfi802:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
	xorl	%ebx, %ebx
	movl	%ecx, %edx
	addl	%eax, %edx
	movl	$0, %r12d
	je	.LBB117_7
# BB#1:                                 # %.lr.ph165
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB117_2:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movslq	%r12d, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB117_4
# BB#3:                                 #   in Loop: Header=BB117_2 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB117_4:                              # %clause_GetLiteralAtom.exit120
                                        #   in Loop: Header=BB117_2 Depth=1
	movq	%rbp, %rdi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB117_5
# BB#6:                                 #   in Loop: Header=BB117_2 Depth=1
	incl	%r12d
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
	leal	(%rcx,%rax), %edx
	cmpl	%edx, %r12d
	jb	.LBB117_2
	jmp	.LBB117_7
.LBB117_5:                              # %clause_GetLiteralAtom.exit120.clause_GetLiteralAtom.exit120._crit_edge.loopexit_crit_edge
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
.LBB117_7:                              # %clause_GetLiteralAtom.exit120._crit_edge
	addl	%eax, %ecx
	cmpl	%ecx, %r12d
	je	.LBB117_52
# BB#8:
	movl	%r12d, (%r15)
	movl	64(%r13), %r8d
	movl	68(%r13), %eax
	movl	%eax, %ecx
	addl	%r8d, %ecx
	movq	%r14, 8(%rsp)           # 8-byte Spill
	jne	.LBB117_17
# BB#9:
	xorl	%r15d, %r15d
	movl	$1, %esi
	jmp	.LBB117_10
.LBB117_17:                             # %.lr.ph158
	xorl	%r14d, %r14d
	movl	$1, %esi
	.p2align	4, 0x90
.LBB117_18:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB117_24 Depth 2
	cmpl	%r12d, %r14d
	je	.LBB117_29
# BB#19:                                #   in Loop: Header=BB117_18 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rbx
	movq	56(%r13), %rcx
	movslq	%r14d, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	24(%rcx), %rbp
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rbp), %ecx
	jne	.LBB117_20
# BB#21:                                #   in Loop: Header=BB117_18 Depth=1
	movl	%esi, %r15d
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rbp
	testq	%rbx, %rbx
	jne	.LBB117_24
	jmp	.LBB117_23
	.p2align	4, 0x90
.LBB117_20:                             #   in Loop: Header=BB117_18 Depth=1
	movl	%esi, %r15d
	testq	%rbx, %rbx
	je	.LBB117_23
	.p2align	4, 0x90
.LBB117_24:                             # %.lr.ph.i.i106
                                        #   Parent Loop BB117_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%rbp, %rdi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB117_25
# BB#26:                                #   in Loop: Header=BB117_24 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB117_24
# BB#27:                                #   in Loop: Header=BB117_18 Depth=1
	xorl	%esi, %esi
	jmp	.LBB117_28
	.p2align	4, 0x90
.LBB117_25:                             #   in Loop: Header=BB117_18 Depth=1
	movl	%r15d, %esi
.LBB117_28:                             # %term_ListContainsTerm.exit110.loopexit
                                        #   in Loop: Header=BB117_18 Depth=1
	movl	64(%r13), %r8d
	movl	68(%r13), %eax
.LBB117_29:                             # %term_ListContainsTerm.exit110
                                        #   in Loop: Header=BB117_18 Depth=1
	leal	(%rax,%r8), %r15d
	testl	%esi, %esi
	je	.LBB117_10
# BB#30:                                # %term_ListContainsTerm.exit110
                                        #   in Loop: Header=BB117_18 Depth=1
	incl	%r14d
	cmpl	%r15d, %r14d
	jb	.LBB117_18
	jmp	.LBB117_10
.LBB117_23:                             # %term_ListContainsTerm.exit110.thread
	leal	(%rax,%r8), %r15d
	xorl	%esi, %esi
.LBB117_10:                             # %.preheader134
	movl	72(%r13), %edx
	testl	%esi, %esi
	setne	%cl
	je	.LBB117_11
# BB#12:                                # %.preheader134
	leal	(%rax,%r8), %edi
	addl	%edx, %edi
	cmpl	%edi, %r15d
	movq	8(%rsp), %r14           # 8-byte Reload
	jae	.LBB117_38
	.p2align	4, 0x90
.LBB117_13:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB117_32 Depth 2
	movq	8(%r14), %rbx
	movq	56(%r13), %rax
	movslq	%r15d, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rbp
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbp), %eax
	jne	.LBB117_15
# BB#14:                                #   in Loop: Header=BB117_13 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB117_15:                             # %clause_GetLiteralAtom.exit102
                                        #   in Loop: Header=BB117_13 Depth=1
	testq	%rbx, %rbx
	je	.LBB117_16
# BB#31:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB117_13 Depth=1
	movl	%esi, 20(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB117_32:                             # %.lr.ph.i.i
                                        #   Parent Loop BB117_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rsi
	movq	%rbp, %rdi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB117_33
# BB#34:                                #   in Loop: Header=BB117_32 Depth=2
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB117_32
# BB#35:                                #   in Loop: Header=BB117_13 Depth=1
	xorl	%esi, %esi
	jmp	.LBB117_36
	.p2align	4, 0x90
.LBB117_33:                             #   in Loop: Header=BB117_13 Depth=1
	movl	20(%rsp), %esi          # 4-byte Reload
.LBB117_36:                             # %term_ListContainsTerm.exit
                                        #   in Loop: Header=BB117_13 Depth=1
	movl	64(%r13), %r8d
	movl	68(%r13), %eax
	movl	72(%r13), %edx
	testl	%esi, %esi
	setne	%cl
	je	.LBB117_38
# BB#37:                                # %term_ListContainsTerm.exit
                                        #   in Loop: Header=BB117_13 Depth=1
	incl	%r15d
	leal	(%rax,%r8), %edi
	addl	%edx, %edi
	cmpl	%edi, %r15d
	jb	.LBB117_13
	jmp	.LBB117_38
.LBB117_16:
	xorl	%ebx, %ebx
	jmp	.LBB117_52
.LBB117_11:
	movq	8(%rsp), %r14           # 8-byte Reload
.LBB117_38:                             # %._crit_edge
	xorl	%ebx, %ebx
	testb	%cl, %cl
	je	.LBB117_52
# BB#39:                                # %.preheader133
	xorl	%ebp, %ebp
	movl	%eax, %ecx
	addl	%r8d, %ecx
	je	.LBB117_47
# BB#40:                                # %.lr.ph144
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB117_41:                             # =>This Inner Loop Header: Depth=1
	cmpl	%ebx, %r12d
	je	.LBB117_45
# BB#42:                                #   in Loop: Header=BB117_41 Depth=1
	movq	(%r14), %rdi
	movq	56(%r13), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB117_44
# BB#43:                                #   in Loop: Header=BB117_41 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB117_44:                             # %clause_GetLiteralAtom.exit91
                                        #   in Loop: Header=BB117_41 Depth=1
	movl	$term_Equal, %edx
	callq	list_DeleteElement
	movq	%rax, (%r14)
	movl	64(%r13), %r8d
	movl	68(%r13), %eax
.LBB117_45:                             #   in Loop: Header=BB117_41 Depth=1
	incl	%ebx
	leal	(%rax,%r8), %ebp
	cmpl	%ebp, %ebx
	jb	.LBB117_41
# BB#46:                                # %.preheader.loopexit
	movl	72(%r13), %edx
.LBB117_47:                             # %.preheader
	addl	%r8d, %eax
	addl	%edx, %eax
	movl	$1, %ebx
	cmpl	%eax, %ebp
	jae	.LBB117_52
# BB#48:                                # %.lr.ph
	movq	8(%r14), %rax
	.p2align	4, 0x90
.LBB117_49:                             # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rcx
	movslq	%ebp, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	24(%rcx), %rsi
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rsi), %ecx
	jne	.LBB117_51
# BB#50:                                #   in Loop: Header=BB117_49 Depth=1
	movq	16(%rsi), %rcx
	movq	8(%rcx), %rsi
.LBB117_51:                             # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB117_49 Depth=1
	movl	$term_Equal, %edx
	movq	%rax, %rdi
	callq	list_DeleteElement
	movq	%rax, 8(%r14)
	incl	%ebp
	movl	68(%r13), %ecx
	addl	64(%r13), %ecx
	addl	72(%r13), %ecx
	cmpl	%ecx, %ebp
	jb	.LBB117_49
.LBB117_52:                             # %.loopexit
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end117:
	.size	clause_IsPartOfDefinition, .Lfunc_end117-clause_IsPartOfDefinition
	.cfi_endproc

	.globl	clause_FPrintRule
	.p2align	4, 0x90
	.type	clause_FPrintRule,@function
clause_FPrintRule:                      # @clause_FPrintRule
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi803:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi804:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi805:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi806:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi807:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi808:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi809:
	.cfi_def_cfa_offset 80
.Lcfi810:
	.cfi_offset %rbx, -56
.Lcfi811:
	.cfi_offset %r12, -48
.Lcfi812:
	.cfi_offset %r13, -40
.Lcfi813:
	.cfi_offset %r14, -32
.Lcfi814:
	.cfi_offset %r15, -24
.Lcfi815:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	68(%rsi), %eax
	addl	64(%rsi), %eax
	movq	%rsi, %rbp
	addl	72(%rsi), %eax
	jle	.LBB118_36
# BB#1:                                 # %.lr.ph127
	movl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB118_2:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rbp), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %r14
	movl	(%r14), %eax
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB118_9
# BB#3:                                 # %fol_Atom.exit100
                                        #   in Loop: Header=BB118_2 Depth=1
	movq	16(%r14), %rax
	movq	8(%rax), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	cmpl	$1, 16(%rcx)
	jne	.LBB118_8
# BB#4:                                 # %fol_Atom.exit92
                                        #   in Loop: Header=BB118_2 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	cmpl	$0, (%rax)
	jle	.LBB118_8
# BB#5:                                 #   in Loop: Header=BB118_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	jmp	.LBB118_10
	.p2align	4, 0x90
.LBB118_9:                              #   in Loop: Header=BB118_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, %rcx
	movq	%rax, %r12
	jmp	.LBB118_10
	.p2align	4, 0x90
.LBB118_8:                              #   in Loop: Header=BB118_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, %rcx
	movq	%rax, %r13
.LBB118_10:                             #   in Loop: Header=BB118_2 Depth=1
	movq	%r14, 8(%rax)
	movq	%rcx, (%rax)
	incq	%r15
	cmpq	%r15, 16(%rsp)          # 8-byte Folded Reload
	jne	.LBB118_2
# BB#6:                                 # %.preheader
	movq	8(%rsp), %rcx           # 8-byte Reload
	testq	%rcx, %rcx
	sete	%al
	movb	$1, %r15b
	je	.LBB118_7
# BB#11:
	movb	%al, 16(%rsp)           # 1-byte Spill
	movq	%rcx, %r14
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB118_12:                             # %.lr.ph117
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movl	(%rsi), %eax
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB118_14
# BB#13:                                #   in Loop: Header=BB118_12 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB118_14:                             # %fol_Atom.exit84
                                        #   in Loop: Header=BB118_12 Depth=1
	movq	%rbx, %rdi
	callq	term_FPrint
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB118_12
# BB#15:
	movq	%r14, %rbp
	jmp	.LBB118_16
.LBB118_36:                             # %list_Delete.exit71.thread
	movl	$.L.str.50, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.51, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.52, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB118_7:
	xorl	%ebp, %ebp
	movb	$1, 16(%rsp)            # 1-byte Folded Spill
.LBB118_16:                             # %._crit_edge118
	movl	$.L.str.50, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	testq	%r13, %r13
	sete	%r14b
	je	.LBB118_17
# BB#18:                                # %.lr.ph114.preheader
	movq	%rbp, %r15
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB118_19:                             # %.lr.ph114
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbp
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	8(%rbp), %rsi
	movl	(%rsi), %eax
	cmpl	fol_NOT(%rip), %eax
	jne	.LBB118_21
# BB#20:                                #   in Loop: Header=BB118_19 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB118_21:                             # %fol_Atom.exit
                                        #   in Loop: Header=BB118_19 Depth=1
	movq	%rbx, %rdi
	callq	term_FPrint
	movq	(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB118_19
# BB#22:                                #   in Loop: Header=BB118_19 Depth=1
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB118_19
# BB#23:
	movq	%r15, %rbp
	movl	%r14d, %r15d
	jmp	.LBB118_24
.LBB118_17:
	xorl	%r13d, %r13d
.LBB118_24:                             # %._crit_edge115
	movl	$.L.str.51, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	testq	%r12, %r12
	sete	%al
	je	.LBB118_25
# BB#26:                                # %.lr.ph.preheader
	movb	%al, 8(%rsp)            # 1-byte Spill
	movl	%r15d, %r14d
	movq	%rbp, %r15
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB118_27:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	callq	term_FPrint
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB118_27
# BB#28:
	movq	%r15, %rbp
	movl	%r14d, %r15d
	movb	8(%rsp), %r14b          # 1-byte Reload
	jmp	.LBB118_29
.LBB118_25:
	xorl	%r12d, %r12d
	movb	$1, %r14b
.LBB118_29:                             # %._crit_edge
	movl	$.L.str.52, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	testb	%r15b, %r15b
	jne	.LBB118_31
	.p2align	4, 0x90
.LBB118_30:                             # %.lr.ph.i75
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB118_30
.LBB118_31:                             # %list_Delete.exit76
	testb	%r14b, %r14b
	jne	.LBB118_33
	.p2align	4, 0x90
.LBB118_32:                             # %.lr.ph.i70
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB118_32
.LBB118_33:                             # %list_Delete.exit71
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	jne	.LBB118_35
	.p2align	4, 0x90
.LBB118_34:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB118_34
.LBB118_35:                             # %list_Delete.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end118:
	.size	clause_FPrintRule, .Lfunc_end118-clause_FPrintRule
	.cfi_endproc

	.globl	clause_FPrintOtter
	.p2align	4, 0x90
	.type	clause_FPrintOtter,@function
clause_FPrintOtter:                     # @clause_FPrintOtter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi816:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi817:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi818:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi819:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi820:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi821:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi822:
	.cfi_def_cfa_offset 64
.Lcfi823:
	.cfi_offset %rbx, -56
.Lcfi824:
	.cfi_offset %r12, -48
.Lcfi825:
	.cfi_offset %r13, -40
.Lcfi826:
	.cfi_offset %r14, -32
.Lcfi827:
	.cfi_offset %r15, -24
.Lcfi828:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	68(%r14), %rcx
	movslq	72(%r14), %rdx
	movslq	64(%r14), %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	testl	%eax, %eax
	je	.LBB119_15
# BB#1:                                 # %.preheader
	jle	.LBB119_14
# BB#2:                                 # %.lr.ph
	movq	%rax, %r12
	addq	$-2, %r12
	movl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB119_3:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbp,8), %r13
	movq	24(%r13), %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB119_5
# BB#4:                                 #   in Loop: Header=BB119_3 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
	movl	$45, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB119_5:                              # %clause_LiteralAtom.exit.thread48
                                        #   in Loop: Header=BB119_3 Depth=1
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB119_10
# BB#6:                                 #   in Loop: Header=BB119_3 Depth=1
	movq	24(%r13), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB119_8
# BB#7:                                 #   in Loop: Header=BB119_3 Depth=1
	movl	$40, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB119_8:                              #   in Loop: Header=BB119_3 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rsi
	movq	%r15, %rdi
	callq	term_FPrintOtterPrefix
	movl	$.L.str.54, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	%r15, %rdi
	callq	term_FPrintOtterPrefix
	movq	24(%r13), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB119_11
# BB#9:                                 #   in Loop: Header=BB119_3 Depth=1
	movl	$41, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	cmpq	%r12, %rbp
	jle	.LBB119_12
	jmp	.LBB119_13
	.p2align	4, 0x90
.LBB119_10:                             #   in Loop: Header=BB119_3 Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	term_FPrintOtterPrefix
.LBB119_11:                             #   in Loop: Header=BB119_3 Depth=1
	cmpq	%r12, %rbp
	jg	.LBB119_13
.LBB119_12:                             #   in Loop: Header=BB119_3 Depth=1
	movl	$.L.str.55, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
.LBB119_13:                             #   in Loop: Header=BB119_3 Depth=1
	incq	%rbp
	cmpq	%rbp, (%rsp)            # 8-byte Folded Reload
	jne	.LBB119_3
	jmp	.LBB119_14
.LBB119_15:
	movl	$.L.str.53, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
.LBB119_14:                             # %.loopexit
	movl	$.L.str.52, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end119:
	.size	clause_FPrintOtter, .Lfunc_end119-clause_FPrintOtter
	.cfi_endproc

	.globl	clause_FPrintCnfDFG
	.p2align	4, 0x90
	.type	clause_FPrintCnfDFG,@function
clause_FPrintCnfDFG:                    # @clause_FPrintCnfDFG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi829:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi830:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi831:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi832:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi833:
	.cfi_def_cfa_offset 48
.Lcfi834:
	.cfi_offset %rbx, -48
.Lcfi835:
	.cfi_offset %r12, -40
.Lcfi836:
	.cfi_offset %r14, -32
.Lcfi837:
	.cfi_offset %r15, -24
.Lcfi838:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %rbp
	movl	%esi, %r15d
	movq	%rdi, %r14
	testq	%rbp, %rbp
	je	.LBB120_11
# BB#1:                                 # %.lr.ph51
	movl	$.L.str.56, %edi
	movl	$30, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testl	%r15d, %r15d
	je	.LBB120_2
	.p2align	4, 0x90
.LBB120_3:                              # %.lr.ph51.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB120_7 Depth 2
	movq	8(%rbp), %rbx
	movq	%rbx, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB120_9
# BB#4:                                 #   in Loop: Header=BB120_3 Depth=1
	movslq	64(%rbx), %rax
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %edx
	cmpl	%edx, %eax
	jg	.LBB120_8
# BB#5:                                 # %.lr.ph.i38
                                        #   in Loop: Header=BB120_3 Depth=1
	movq	56(%rbx), %rcx
	movslq	%edx, %rdx
	decq	%rax
	.p2align	4, 0x90
.LBB120_7:                              #   Parent Loop BB120_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx,%rax,8), %rsi
	testb	$4, (%rsi)
	jne	.LBB120_9
# BB#6:                                 #   in Loop: Header=BB120_7 Depth=2
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB120_7
.LBB120_8:                              # %clause_HasSelectedLiteral.exit45.thread
                                        #   in Loop: Header=BB120_3 Depth=1
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	clause_FPrintDFG
.LBB120_9:                              # %clause_HasSelectedLiteral.exit45
                                        #   in Loop: Header=BB120_3 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB120_3
	jmp	.LBB120_10
	.p2align	4, 0x90
.LBB120_2:                              # %.lr.ph51.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	clause_FPrintDFG
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB120_2
.LBB120_10:                             # %._crit_edge52
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB120_11:
	testq	%r12, %r12
	je	.LBB120_22
# BB#12:                                # %.lr.ph
	movl	$.L.str.58, %edi
	movl	$35, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testl	%r15d, %r15d
	je	.LBB120_13
	.p2align	4, 0x90
.LBB120_14:                             # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB120_18 Depth 2
	movq	8(%r12), %rbx
	movq	%rbx, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB120_20
# BB#15:                                #   in Loop: Header=BB120_14 Depth=1
	movslq	64(%rbx), %rax
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %edx
	cmpl	%edx, %eax
	jg	.LBB120_19
# BB#16:                                # %.lr.ph.i
                                        #   in Loop: Header=BB120_14 Depth=1
	movq	56(%rbx), %rcx
	movslq	%edx, %rdx
	decq	%rax
	.p2align	4, 0x90
.LBB120_18:                             #   Parent Loop BB120_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx,%rax,8), %rsi
	testb	$4, (%rsi)
	jne	.LBB120_20
# BB#17:                                #   in Loop: Header=BB120_18 Depth=2
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB120_18
.LBB120_19:                             # %clause_HasSelectedLiteral.exit.thread
                                        #   in Loop: Header=BB120_14 Depth=1
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	clause_FPrintDFG
.LBB120_20:                             # %clause_HasSelectedLiteral.exit
                                        #   in Loop: Header=BB120_14 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB120_14
	jmp	.LBB120_21
	.p2align	4, 0x90
.LBB120_13:                             # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	clause_FPrintDFG
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB120_13
.LBB120_21:                             # %._crit_edge
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB120_22:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end120:
	.size	clause_FPrintCnfDFG, .Lfunc_end120-clause_FPrintCnfDFG
	.cfi_endproc

	.globl	clause_FPrintDFG
	.p2align	4, 0x90
	.type	clause_FPrintDFG,@function
clause_FPrintDFG:                       # @clause_FPrintDFG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi839:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi840:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi841:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi842:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi843:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi844:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi845:
	.cfi_def_cfa_offset 64
.Lcfi846:
	.cfi_offset %rbx, -56
.Lcfi847:
	.cfi_offset %r12, -48
.Lcfi848:
	.cfi_offset %r13, -40
.Lcfi849:
	.cfi_offset %r14, -32
.Lcfi850:
	.cfi_offset %r15, -24
.Lcfi851:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	68(%r14), %rax
	movslq	72(%r14), %rcx
	movslq	64(%r14), %r12
	addq	%rax, %r12
	addq	%rcx, %r12
	movl	$.L.str.77, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	testl	%r12d, %r12d
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	jle	.LBB121_1
# BB#2:                                 # %.lr.ph91
	movl	%r12d, %r13d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB121_3:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB121_5
# BB#4:                                 #   in Loop: Header=BB121_3 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB121_5:                              # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB121_3 Depth=1
	callq	term_VariableSymbols
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	list_NPointerUnion
	movq	%rax, %rbp
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB121_3
# BB#6:                                 # %._crit_edge92
	testq	%rbp, %rbp
	sete	%al
	je	.LBB121_1
# BB#7:                                 # %.lr.ph86
	movb	%al, 3(%rsp)            # 1-byte Spill
	movl	fol_ALL(%rip), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
	movl	$.L.str.71, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB121_8:                              # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
	cmpq	$0, (%rbx)
	je	.LBB121_10
# BB#9:                                 #   in Loop: Header=BB121_8 Depth=1
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB121_8
.LBB121_10:                             # %._crit_edge87
	movl	$.L.str.72, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	jmp	.LBB121_11
.LBB121_1:
	xorl	%ebp, %ebp
	movb	$1, 3(%rsp)             # 1-byte Folded Spill
.LBB121_11:                             # %._crit_edge92.thread
	movl	fol_OR(%rip), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
	movl	$40, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	testl	%r12d, %r12d
	jle	.LBB121_15
# BB#12:                                # %.lr.ph82
	movl	%r12d, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB121_13:                             # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rsi
	movq	%r15, %rdi
	callq	term_FPrintPrefix
	incq	%r13
	cmpq	%r12, %r13
	jge	.LBB121_14
# BB#27:                                #   in Loop: Header=BB121_13 Depth=1
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB121_14:                             # %.backedge77
                                        #   in Loop: Header=BB121_13 Depth=1
	cmpq	%r13, %rbx
	jne	.LBB121_13
.LBB121_15:                             # %._crit_edge83
	testl	%r12d, %r12d
	jne	.LBB121_17
# BB#16:
	movl	fol_FALSE(%rip), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
.LBB121_17:
	cmpb	$0, 3(%rsp)             # 1-byte Folded Reload
	movl	4(%rsp), %ebx           # 4-byte Reload
	jne	.LBB121_20
	.p2align	4, 0x90
.LBB121_18:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB121_18
# BB#19:                                # %list_Delete.exit
	movl	$41, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB121_20:
	movl	(%r14), %edx
	movl	$.L.str.78, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	testl	%ebx, %ebx
	je	.LBB121_26
# BB#21:
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	clause_FPrintOrigin
	movl	$.L.str.74, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB121_23
	jmp	.LBB121_25
	.p2align	4, 0x90
.LBB121_24:                             # %.backedge
                                        #   in Loop: Header=BB121_23 Depth=1
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB121_25
.LBB121_23:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	cmpq	$0, (%rbx)
	jne	.LBB121_24
.LBB121_25:                             # %._crit_edge
	movl	$93, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movl	12(%r14), %edx
	movl	$.L.str.79, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
.LBB121_26:
	movl	$.L.str.76, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end121:
	.size	clause_FPrintDFG, .Lfunc_end121-clause_FPrintDFG
	.cfi_endproc

	.globl	clause_FPrintCnfDFGProblem
	.p2align	4, 0x90
	.type	clause_FPrintCnfDFGProblem,@function
clause_FPrintCnfDFGProblem:             # @clause_FPrintCnfDFGProblem
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi852:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi853:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi854:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi855:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi856:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi857:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi858:
	.cfi_def_cfa_offset 64
.Lcfi859:
	.cfi_offset %rbx, -56
.Lcfi860:
	.cfi_offset %r12, -48
.Lcfi861:
	.cfi_offset %r13, -40
.Lcfi862:
	.cfi_offset %r14, -32
.Lcfi863:
	.cfi_offset %r15, -24
.Lcfi864:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %rbx
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movl	$.L.str.59, %edi
	movl	$25, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.84, %edi
	movl	$22, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.85, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbp, %rdx
	callq	fprintf
	movl	$.L.str.86, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	fprintf
	movl	$.L.str.87, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	fprintf
	movl	$.L.str.88, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	fprintf
	movl	$.L.str.89, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$10, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movl	$.L.str.60, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	%r14, %rdi
	callq	fol_FPrintDFGSignature
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.56, %edi
	movl	$30, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testq	%r15, %r15
	je	.LBB122_10
# BB#1:                                 # %.lr.ph39.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB122_2:                              # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	testb	$8, 48(%rsi)
	jne	.LBB122_4
# BB#3:                                 #   in Loop: Header=BB122_2 Depth=1
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	clause_FPrintDFG
.LBB122_4:                              #   in Loop: Header=BB122_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB122_2
# BB#5:                                 # %._crit_edge40
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.58, %edi
	movl	$35, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testq	%r15, %r15
	jne	.LBB122_7
	jmp	.LBB122_11
	.p2align	4, 0x90
.LBB122_9:                              #   in Loop: Header=BB122_7 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB122_11
.LBB122_7:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rsi
	testb	$8, 48(%rsi)
	je	.LBB122_9
# BB#8:                                 #   in Loop: Header=BB122_7 Depth=1
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	clause_FPrintDFG
	jmp	.LBB122_9
.LBB122_10:                             # %._crit_edge.critedge
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.58, %edi
	movl	$35, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB122_11:                             # %._crit_edge
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.61, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end122:
	.size	clause_FPrintCnfDFGProblem, .Lfunc_end122-clause_FPrintCnfDFGProblem
	.cfi_endproc

	.globl	clause_FPrintCnfFormulasDFGProblem
	.p2align	4, 0x90
	.type	clause_FPrintCnfFormulasDFGProblem,@function
clause_FPrintCnfFormulasDFGProblem:     # @clause_FPrintCnfFormulasDFGProblem
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi865:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi866:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi867:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi868:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi869:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi870:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi871:
	.cfi_def_cfa_offset 80
.Lcfi872:
	.cfi_offset %rbx, -56
.Lcfi873:
	.cfi_offset %r12, -48
.Lcfi874:
	.cfi_offset %r13, -40
.Lcfi875:
	.cfi_offset %r14, -32
.Lcfi876:
	.cfi_offset %r15, -24
.Lcfi877:
	.cfi_offset %rbp, -16
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %r14
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %rbx
	movq	104(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	88(%rsp), %r15
	movq	80(%rsp), %r12
	movl	$.L.str.59, %edi
	movl	$25, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.84, %edi
	movl	$22, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.85, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	fprintf
	movl	$.L.str.86, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fprintf
	movl	$.L.str.87, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	fprintf
	movl	$.L.str.88, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	fprintf
	movl	$.L.str.89, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.62, %edi
	movl	$18, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	callq	fol_FPrintDFGSignature
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	testq	%r12, %r12
	je	.LBB123_11
# BB#1:                                 # %.lr.ph64
	movl	$.L.str.63, %edi
	movl	$26, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB123_2
	.p2align	4, 0x90
.LBB123_3:                              # %.lr.ph64.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB123_7 Depth 2
	movq	8(%r12), %rbp
	movq	%rbp, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB123_9
# BB#4:                                 #   in Loop: Header=BB123_3 Depth=1
	movslq	64(%rbp), %rax
	movl	68(%rbp), %ecx
	leal	-1(%rax,%rcx), %edx
	cmpl	%edx, %eax
	jg	.LBB123_8
# BB#5:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB123_3 Depth=1
	movq	56(%rbp), %rcx
	movslq	%edx, %rdx
	decq	%rax
	.p2align	4, 0x90
.LBB123_7:                              #   Parent Loop BB123_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx,%rax,8), %rsi
	testb	$4, (%rsi)
	jne	.LBB123_9
# BB#6:                                 #   in Loop: Header=BB123_7 Depth=2
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB123_7
.LBB123_8:                              # %clause_HasSelectedLiteral.exit.thread
                                        #   in Loop: Header=BB123_3 Depth=1
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	clause_FPrintFormulaDFG
.LBB123_9:                              # %clause_HasSelectedLiteral.exit
                                        #   in Loop: Header=BB123_3 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB123_3
	jmp	.LBB123_10
	.p2align	4, 0x90
.LBB123_2:                              # %.lr.ph64.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	clause_FPrintFormulaDFG
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB123_2
.LBB123_10:                             # %._crit_edge65
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB123_11:
	testq	%r15, %r15
	je	.LBB123_22
# BB#12:                                # %.lr.ph
	movl	$.L.str.64, %edi
	movl	$31, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB123_13
	.p2align	4, 0x90
.LBB123_14:                             # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB123_18 Depth 2
	movq	8(%r15), %rbp
	movq	%rbp, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB123_20
# BB#15:                                #   in Loop: Header=BB123_14 Depth=1
	movslq	64(%rbp), %rax
	movl	68(%rbp), %ecx
	leal	-1(%rax,%rcx), %edx
	cmpl	%edx, %eax
	jg	.LBB123_19
# BB#16:                                # %.lr.ph.i51
                                        #   in Loop: Header=BB123_14 Depth=1
	movq	56(%rbp), %rcx
	movslq	%edx, %rdx
	decq	%rax
	.p2align	4, 0x90
.LBB123_18:                             #   Parent Loop BB123_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx,%rax,8), %rsi
	testb	$4, (%rsi)
	jne	.LBB123_20
# BB#17:                                #   in Loop: Header=BB123_18 Depth=2
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB123_18
.LBB123_19:                             # %clause_HasSelectedLiteral.exit58.thread
                                        #   in Loop: Header=BB123_14 Depth=1
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	clause_FPrintFormulaDFG
.LBB123_20:                             # %clause_HasSelectedLiteral.exit58
                                        #   in Loop: Header=BB123_14 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB123_14
	jmp	.LBB123_21
	.p2align	4, 0x90
.LBB123_13:                             # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	clause_FPrintFormulaDFG
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB123_13
.LBB123_21:                             # %._crit_edge
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB123_22:
	movl	$.L.str.65, %edi
	movl	$28, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	fol_FPrintPrecedence
	movl	$.L.str.66, %edi
	movl	$32, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end123:
	.size	clause_FPrintCnfFormulasDFGProblem, .Lfunc_end123-clause_FPrintCnfFormulasDFGProblem
	.cfi_endproc

	.globl	clause_FPrintFormulaDFG
	.p2align	4, 0x90
	.type	clause_FPrintFormulaDFG,@function
clause_FPrintFormulaDFG:                # @clause_FPrintFormulaDFG
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi878:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi879:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi880:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi881:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi882:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi883:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi884:
	.cfi_def_cfa_offset 80
.Lcfi885:
	.cfi_offset %rbx, -56
.Lcfi886:
	.cfi_offset %r12, -48
.Lcfi887:
	.cfi_offset %r13, -40
.Lcfi888:
	.cfi_offset %r14, -32
.Lcfi889:
	.cfi_offset %r15, -24
.Lcfi890:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %r13
	movslq	68(%r14), %rax
	movslq	72(%r14), %rcx
	movslq	64(%r14), %r12
	addq	%rax, %r12
	addq	%rcx, %r12
	movl	$.L.str.80, %edi
	movl	$10, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	testl	%r12d, %r12d
	jle	.LBB124_1
# BB#9:                                 # %.lr.ph92
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movl	%r12d, %r15d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB124_10:                             # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB124_12
# BB#11:                                #   in Loop: Header=BB124_10 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB124_12:                             # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB124_10 Depth=1
	callq	term_VariableSymbols
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	list_NPointerUnion
	movq	%rax, %rbp
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB124_10
# BB#13:                                # %._crit_edge93
	testq	%rbp, %rbp
	sete	11(%rsp)                # 1-byte Folded Spill
	je	.LBB124_14
# BB#15:                                # %.lr.ph87
	movl	fol_ALL(%rip), %esi
	movq	%r13, %rdi
	callq	symbol_FPrint
	movl	$.L.str.71, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB124_16:                             # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %esi
	movq	%r13, %rdi
	callq	symbol_FPrint
	cmpq	$0, (%rbx)
	je	.LBB124_18
# BB#17:                                #   in Loop: Header=BB124_16 Depth=1
	movl	$44, %edi
	movq	%r13, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB124_16
.LBB124_18:                             # %._crit_edge88
	movl	$.L.str.72, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	cmpl	$2, %r12d
	jge	.LBB124_21
	jmp	.LBB124_20
.LBB124_1:
	movb	$1, %bl
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testl	%r12d, %r12d
	jne	.LBB124_4
	jmp	.LBB124_3
.LBB124_14:
	xorl	%ebp, %ebp
	movb	$1, 11(%rsp)            # 1-byte Folded Spill
	cmpl	$2, %r12d
	jl	.LBB124_20
.LBB124_21:
	movl	fol_OR(%rip), %esi
	movq	%r13, %rdi
	callq	symbol_FPrint
	movl	$40, %edi
	movq	%r13, %rsi
	callq	_IO_putc
	movb	$1, %al
	jmp	.LBB124_22
.LBB124_20:
	xorl	%eax, %eax
.LBB124_22:                             # %.preheader
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testl	%r12d, %r12d
	jle	.LBB124_23
# BB#24:                                # %.lr.ph83
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB124_25:                             # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rsi
	movq	%r13, %rdi
	callq	term_FPrintPrefix
	incq	%rbx
	cmpq	%r12, %rbx
	jge	.LBB124_26
# BB#27:                                #   in Loop: Header=BB124_25 Depth=1
	movl	$44, %edi
	movq	%r13, %rsi
	callq	_IO_putc
.LBB124_26:                             # %.backedge79
                                        #   in Loop: Header=BB124_25 Depth=1
	cmpq	%rbx, %r15
	jne	.LBB124_25
.LBB124_23:
	movl	12(%rsp), %r15d         # 4-byte Reload
	movb	11(%rsp), %bl           # 1-byte Reload
	testl	%r12d, %r12d
	jne	.LBB124_4
.LBB124_3:
	movl	fol_FALSE(%rip), %esi
	movq	%r13, %rdi
	callq	symbol_FPrint
.LBB124_4:
	testb	%bl, %bl
	jne	.LBB124_7
	.p2align	4, 0x90
.LBB124_5:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB124_5
# BB#6:                                 # %list_Delete.exit
	movl	$41, %edi
	movq	%r13, %rsi
	callq	_IO_putc
.LBB124_7:
	movl	(%r14), %edx
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	je	.LBB124_28
# BB#8:
	movl	$.L.str.78, %esi
	jmp	.LBB124_29
.LBB124_28:
	movl	$.L.str.79, %esi
.LBB124_29:
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	testl	%r15d, %r15d
	je	.LBB124_35
# BB#30:
	movl	$44, %edi
	movq	%r13, %rsi
	callq	_IO_putc
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	clause_FPrintOrigin
	movl	$.L.str.74, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	callq	fwrite
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB124_32
	jmp	.LBB124_34
	.p2align	4, 0x90
.LBB124_33:                             # %.backedge
                                        #   in Loop: Header=BB124_32 Depth=1
	movl	$44, %edi
	movq	%r13, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB124_34
.LBB124_32:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	cmpq	$0, (%rbx)
	jne	.LBB124_33
.LBB124_34:                             # %._crit_edge
	movl	$93, %edi
	movq	%r13, %rsi
	callq	_IO_putc
	movl	12(%r14), %edx
	movl	$.L.str.79, %esi
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
.LBB124_35:
	movl	$.L.str.76, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r13, %rcx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end124:
	.size	clause_FPrintFormulaDFG, .Lfunc_end124-clause_FPrintFormulaDFG
	.cfi_endproc

	.globl	clause_FPrintCnfOtter
	.p2align	4, 0x90
	.type	clause_FPrintCnfOtter,@function
clause_FPrintCnfOtter:                  # @clause_FPrintCnfOtter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi891:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi892:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi893:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi894:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi895:
	.cfi_def_cfa_offset 48
.Lcfi896:
	.cfi_offset %rbx, -40
.Lcfi897:
	.cfi_offset %r14, -32
.Lcfi898:
	.cfi_offset %r15, -24
.Lcfi899:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r15, %r15
	je	.LBB125_15
# BB#1:                                 # %.lr.ph57
	movl	fol_NOT(%rip), %r10d
	movl	fol_EQUALITY(%rip), %r11d
	movl	$1, %r8d
	movq	%r15, %r9
	.p2align	4, 0x90
.LBB125_2:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB125_4 Depth 2
	movq	8(%r9), %rdi
	movl	64(%rdi), %ebx
	movl	68(%rdi), %esi
	addl	%ebx, %esi
	addl	72(%rdi), %esi
	xorl	%ebp, %ebp
	cmpl	%esi, %ebx
	jge	.LBB125_7
# BB#3:                                 # %.lr.ph53
                                        #   in Loop: Header=BB125_2 Depth=1
	movq	56(%rdi), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB125_4:                              #   Parent Loop BB125_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebx, %rax
	movq	(%rdi,%rax,8), %rax
	movq	24(%rax), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, %r10d
	jne	.LBB125_6
# BB#5:                                 #   in Loop: Header=BB125_4 Depth=2
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB125_6:                              # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB125_4 Depth=2
	cmpl	%eax, %r11d
	cmovel	%esi, %ebx
	cmovel	%r8d, %ebp
	incl	%ebx
	cmpl	%esi, %ebx
	jl	.LBB125_4
.LBB125_7:                              # %._crit_edge54
                                        #   in Loop: Header=BB125_2 Depth=1
	testl	%ebp, %ebp
	jne	.LBB125_9
# BB#8:                                 # %._crit_edge54
                                        #   in Loop: Header=BB125_2 Depth=1
	movq	(%r9), %r9
	testq	%r9, %r9
	jne	.LBB125_2
.LBB125_9:                              # %.critedge
	movl	380(%rdx), %edx
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	fol_FPrintOtterOptions
	testq	%r15, %r15
	je	.LBB125_14
# BB#10:
	movl	$.L.str.67, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	testl	%ebp, %ebp
	je	.LBB125_12
# BB#11:
	movl	$.L.str.68, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	.p2align	4, 0x90
.LBB125_12:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rsi
	movq	%r14, %rdi
	callq	clause_FPrintOtter
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB125_12
# BB#13:                                # %._crit_edge
	movl	$.L.str.57, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB125_15:                             # %.critedge.thread
	movl	380(%rdx), %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fol_FPrintOtterOptions  # TAILCALL
.LBB125_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end125:
	.size	clause_FPrintCnfOtter, .Lfunc_end125-clause_FPrintCnfOtter
	.cfi_endproc

	.globl	clause_FPrintCnfDFGDerivables
	.p2align	4, 0x90
	.type	clause_FPrintCnfDFGDerivables,@function
clause_FPrintCnfDFGDerivables:          # @clause_FPrintCnfDFGDerivables
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi900:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi901:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi902:
	.cfi_def_cfa_offset 32
.Lcfi903:
	.cfi_offset %rbx, -24
.Lcfi904:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB126_8
# BB#1:                                 # %.lr.ph
	testl	%edx, %edx
	je	.LBB126_2
	.p2align	4, 0x90
.LBB126_5:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	testb	$8, 48(%rsi)
	jne	.LBB126_7
# BB#6:                                 #   in Loop: Header=BB126_5 Depth=1
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	clause_FPrintDFG
.LBB126_7:                              #   in Loop: Header=BB126_5 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB126_5
	jmp	.LBB126_8
	.p2align	4, 0x90
.LBB126_2:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	testb	$8, 48(%rsi)
	je	.LBB126_4
# BB#3:                                 #   in Loop: Header=BB126_2 Depth=1
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	clause_FPrintDFG
.LBB126_4:                              #   in Loop: Header=BB126_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB126_2
.LBB126_8:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end126:
	.size	clause_FPrintCnfDFGDerivables, .Lfunc_end126-clause_FPrintCnfDFGDerivables
	.cfi_endproc

	.globl	clause_FPrintDFGStep
	.p2align	4, 0x90
	.type	clause_FPrintDFGStep,@function
clause_FPrintDFGStep:                   # @clause_FPrintDFGStep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi905:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi906:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi907:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi908:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi909:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi910:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi911:
	.cfi_def_cfa_offset 64
.Lcfi912:
	.cfi_offset %rbx, -56
.Lcfi913:
	.cfi_offset %r12, -48
.Lcfi914:
	.cfi_offset %r13, -40
.Lcfi915:
	.cfi_offset %r14, -32
.Lcfi916:
	.cfi_offset %r15, -24
.Lcfi917:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	68(%r14), %rax
	movslq	72(%r14), %rcx
	movslq	64(%r14), %rbx
	addq	%rax, %rbx
	addq	%rcx, %rbx
	movl	$.L.str.69, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movl	(%r14), %edx
	xorl	%r12d, %r12d
	movl	$.L.str.70, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	testl	%ebx, %ebx
	jle	.LBB127_12
# BB#1:                                 # %.lr.ph90
	movl	%ebx, %r13d
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB127_2:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB127_4
# BB#3:                                 #   in Loop: Header=BB127_2 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB127_4:                              # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB127_2 Depth=1
	callq	term_VariableSymbols
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	list_NPointerUnion
	movq	%rax, %r12
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB127_2
# BB#5:                                 # %._crit_edge91
	testq	%r12, %r12
	sete	%al
	je	.LBB127_11
# BB#6:                                 # %.lr.ph85
	movb	%al, 7(%rsp)            # 1-byte Spill
	movq	%rbx, %rbp
	movl	fol_ALL(%rip), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
	movl	$.L.str.71, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB127_7:                              # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
	cmpq	$0, (%rbx)
	je	.LBB127_9
# BB#8:                                 #   in Loop: Header=BB127_7 Depth=1
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB127_7
.LBB127_9:                              # %._crit_edge86
	movl	$.L.str.72, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	%rbp, %rbx
	jmp	.LBB127_13
.LBB127_11:
	xorl	%r12d, %r12d
.LBB127_12:                             # %._crit_edge91.thread
	movb	$1, 7(%rsp)             # 1-byte Folded Spill
.LBB127_13:                             # %._crit_edge91.thread
	movl	fol_OR(%rip), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
	movl	$40, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	testl	%ebx, %ebx
	jle	.LBB127_18
# BB#14:                                # %.lr.ph81
	movl	%ebx, %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB127_15:                             # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rsi
	movq	%r15, %rdi
	callq	term_FPrintPrefix
	incq	%r13
	cmpq	%rbx, %r13
	jge	.LBB127_17
# BB#16:                                #   in Loop: Header=BB127_15 Depth=1
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB127_17:                             # %.backedge76
                                        #   in Loop: Header=BB127_15 Depth=1
	cmpq	%r13, %rbp
	jne	.LBB127_15
.LBB127_18:                             # %._crit_edge82
	testl	%ebx, %ebx
	jne	.LBB127_20
# BB#19:
	movl	fol_FALSE(%rip), %esi
	movq	%r15, %rdi
	callq	symbol_FPrint
.LBB127_20:
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	jne	.LBB127_23
	.p2align	4, 0x90
.LBB127_21:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB127_21
# BB#22:                                # %list_Delete.exit
	movl	$41, %edi
	movq	%r15, %rsi
	callq	_IO_putc
.LBB127_23:
	movl	$.L.str.73, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	clause_FPrintOrigin
	movl	$.L.str.74, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB127_26
	.p2align	4, 0x90
.LBB127_25:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %edx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	cmpq	$0, (%rbx)
	je	.LBB127_26
# BB#24:                                # %.backedge
                                        #   in Loop: Header=BB127_25 Depth=1
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB127_25
.LBB127_26:                             # %._crit_edge
	movl	$93, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movl	12(%r14), %edx
	movl	$.L.str.75, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movl	$.L.str.76, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end127:
	.size	clause_FPrintDFGStep, .Lfunc_end127-clause_FPrintDFGStep
	.cfi_endproc

	.globl	clause_Check
	.p2align	4, 0x90
	.type	clause_Check,@function
clause_Check:                           # @clause_Check
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi918:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi919:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi920:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi921:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi922:
	.cfi_def_cfa_offset 48
.Lcfi923:
	.cfi_offset %rbx, -48
.Lcfi924:
	.cfi_offset %r12, -40
.Lcfi925:
	.cfi_offset %r13, -32
.Lcfi926:
	.cfi_offset %r14, -24
.Lcfi927:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	testq	%r13, %r13
	je	.LBB128_14
# BB#1:
	movq	%r13, %rdi
	callq	clause_IsUnorderedClause
	testl	%eax, %eax
	je	.LBB128_11
# BB#2:
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
	movl	72(%r13), %edx
	leal	(%rax,%rcx), %esi
	leal	-1(%rdx,%rsi), %esi
	cmpl	%esi, %eax
	jg	.LBB128_8
# BB#3:                                 # %.lr.ph.i
	movslq	%eax, %rbx
	decq	%rbx
	.p2align	4, 0x90
.LBB128_4:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rsi
	movq	8(%rsi,%rbx,8), %rsi
	movq	24(%rsi), %rsi
	movl	fol_EQUALITY(%rip), %edi
	cmpl	(%rsi), %edi
	jne	.LBB128_7
# BB#5:                                 #   in Loop: Header=BB128_4 Depth=1
	movq	16(%rsi), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	ord_Compare
	cmpl	$1, %eax
	je	.LBB128_11
# BB#6:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB128_4 Depth=1
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
	movl	72(%r13), %edx
.LBB128_7:                              #   in Loop: Header=BB128_4 Depth=1
	leal	(%rdx,%rcx), %esi
	leal	-1(%rax,%rsi), %esi
	movslq	%esi, %rsi
	incq	%rbx
	cmpq	%rsi, %rbx
	jl	.LBB128_4
.LBB128_8:                              # %clause_IsClause.exit
	movq	%r13, %rdi
	callq	clause_Copy
	movq	%rax, %r12
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	clause_OrientEqualities
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	clause_ReInit
	movl	4(%r13), %eax
	cmpl	4(%r12), %eax
	jne	.LBB128_10
# BB#9:
	movl	52(%r13), %eax
	cmpl	52(%r12), %eax
	jne	.LBB128_10
# BB#13:
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	clause_Delete           # TAILCALL
.LBB128_14:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB128_11:                             # %.loopexit
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.36, %esi
	movl	$.L.str.37, %edx
	movl	$4774, %ecx             # imm = 0x12A6
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.81, %edi
.LBB128_12:                             # %.loopexit
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.39, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.LBB128_10:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.36, %esi
	movl	$.L.str.37, %edx
	movl	$4783, %ecx             # imm = 0x12AF
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.82, %edi
	jmp	.LBB128_12
.Lfunc_end128:
	.size	clause_Check, .Lfunc_end128-clause_Check
	.cfi_endproc

	.globl	clause_PParentsFPrintParentClauses
	.p2align	4, 0x90
	.type	clause_PParentsFPrintParentClauses,@function
clause_PParentsFPrintParentClauses:     # @clause_PParentsFPrintParentClauses
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi928:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi929:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi930:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi931:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi932:
	.cfi_def_cfa_offset 48
.Lcfi933:
	.cfi_offset %rbx, -40
.Lcfi934:
	.cfi_offset %r14, -32
.Lcfi935:
	.cfi_offset %r15, -24
.Lcfi936:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rdi, %r15
	movq	32(%rsi), %rbp
	testq	%rbp, %rbp
	je	.LBB129_10
# BB#1:
	movq	40(%rsi), %rbx
	movq	8(%rbp), %rdx
	testl	%r14d, %r14d
	je	.LBB129_3
# BB#2:
	movl	(%rdx), %edx
.LBB129_3:
	movl	8(%rbx), %ecx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	cmpq	$0, (%rbp)
	je	.LBB129_10
# BB#4:
	movq	%rbp, %rdi
	callq	list_Length
	movl	$44, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movq	(%rbp), %rbp
	movq	(%rbx), %rbx
	movq	8(%rbp), %rdx
	testl	%r14d, %r14d
	je	.LBB129_6
# BB#5:
	movl	(%rdx), %edx
.LBB129_6:
	movl	8(%rbx), %ecx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB129_10
# BB#7:                                 # %.lr.ph
	movq	(%rbx), %rbx
	testl	%r14d, %r14d
	je	.LBB129_9
	.p2align	4, 0x90
.LBB129_8:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rax
	movl	(%rax), %edx
	movl	8(%rbx), %ecx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movq	(%rbp), %rbp
	movq	(%rbx), %rbx
	testq	%rbp, %rbp
	jne	.LBB129_8
	jmp	.LBB129_10
	.p2align	4, 0x90
.LBB129_9:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbp), %edx
	movl	8(%rbx), %ecx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	fprintf
	movq	(%rbp), %rbp
	movq	(%rbx), %rbx
	testq	%rbp, %rbp
	jne	.LBB129_9
.LBB129_10:                             # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end129:
	.size	clause_PParentsFPrintParentClauses, .Lfunc_end129-clause_PParentsFPrintParentClauses
	.cfi_endproc

	.globl	clause_PParentsLiteralFPrintUnsigned
	.p2align	4, 0x90
	.type	clause_PParentsLiteralFPrintUnsigned,@function
clause_PParentsLiteralFPrintUnsigned:   # @clause_PParentsLiteralFPrintUnsigned
	.cfi_startproc
# BB#0:
	movq	24(%rsi), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB130_2
# BB#1:
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB130_2:                              # %clause_LiteralAtom.exit
	pushq	%rax
.Lcfi937:
	.cfi_def_cfa_offset 16
	callq	term_FPrintPrefix
	movq	stdout(%rip), %rdi
	popq	%rax
	jmp	fflush                  # TAILCALL
.Lfunc_end130:
	.size	clause_PParentsLiteralFPrintUnsigned, .Lfunc_end130-clause_PParentsLiteralFPrintUnsigned
	.cfi_endproc

	.globl	clause_PParentsFPrintGen
	.p2align	4, 0x90
	.type	clause_PParentsFPrintGen,@function
clause_PParentsFPrintGen:               # @clause_PParentsFPrintGen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi938:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi939:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi940:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi941:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi942:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi943:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi944:
	.cfi_def_cfa_offset 80
.Lcfi945:
	.cfi_offset %rbx, -56
.Lcfi946:
	.cfi_offset %r12, -48
.Lcfi947:
	.cfi_offset %r13, -40
.Lcfi948:
	.cfi_offset %r14, -32
.Lcfi949:
	.cfi_offset %r15, -24
.Lcfi950:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB131_32
# BB#1:
	movl	(%rbx), %edx
	xorl	%r12d, %r12d
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	12(%rbx), %edx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	clause_FPrintOrigin
	cmpq	$0, 32(%rbx)
	je	.LBB131_3
# BB#2:
	movl	$58, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	clause_PParentsFPrintParentClauses
.LBB131_3:
	movl	$93, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movslq	64(%rbx), %r15
	testq	%r15, %r15
	movl	68(%rbx), %r13d
	movslq	72(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jle	.LBB131_10
# BB#4:                                 # %.lr.ph113
	movl	%r15d, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB131_5:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB131_7
# BB#6:                                 #   in Loop: Header=BB131_5 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB131_7:                              # %clause_PParentsLiteralFPrintUnsigned.exit103
                                        #   in Loop: Header=BB131_5 Depth=1
	movq	%r14, %rdi
	callq	term_FPrintPrefix
	movq	stdout(%rip), %rdi
	callq	fflush
	incq	%rbp
	cmpq	%r15, %rbp
	jge	.LBB131_8
# BB#33:                                #   in Loop: Header=BB131_5 Depth=1
	movl	$32, %edi
	movq	%r14, %rsi
	callq	_IO_putc
.LBB131_8:                              # %.backedge105
                                        #   in Loop: Header=BB131_5 Depth=1
	cmpq	%rbp, %r12
	jne	.LBB131_5
# BB#9:
	movl	%r15d, %r12d
.LBB131_10:                             # %._crit_edge114
	movl	$.L.str.3, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	addl	%r15d, %r13d
	movl	%r13d, %r15d
	cmpl	%r15d, %r12d
	jge	.LBB131_22
# BB#11:                                # %.lr.ph109
	movslq	%r12d, %rbp
	movslq	%r15d, %r12
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movl	%r15d, %r15d
	.p2align	4, 0x90
.LBB131_12:                             # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %r13
	movq	24(%r13), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB131_14
# BB#13:                                #   in Loop: Header=BB131_12 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB131_14:                             # %clause_PParentsLiteralFPrintUnsigned.exit93
                                        #   in Loop: Header=BB131_12 Depth=1
	movq	%r14, %rdi
	callq	term_FPrintPrefix
	movq	stdout(%rip), %rdi
	callq	fflush
	testb	$1, (%r13)
	je	.LBB131_17
# BB#15:                                #   in Loop: Header=BB131_12 Depth=1
	movl	$42, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	cmpl	$0, 8(%r13)
	je	.LBB131_17
# BB#16:                                #   in Loop: Header=BB131_12 Depth=1
	movl	$42, %edi
	movq	%r14, %rsi
	callq	_IO_putc
.LBB131_17:                             #   in Loop: Header=BB131_12 Depth=1
	testb	$4, (%r13)
	je	.LBB131_19
# BB#18:                                #   in Loop: Header=BB131_12 Depth=1
	movl	$43, %edi
	movq	%r14, %rsi
	callq	_IO_putc
.LBB131_19:                             #   in Loop: Header=BB131_12 Depth=1
	incq	%rbp
	cmpq	%r12, %rbp
	jge	.LBB131_20
# BB#34:                                #   in Loop: Header=BB131_12 Depth=1
	movl	$32, %edi
	movq	%r14, %rsi
	callq	_IO_putc
.LBB131_20:                             # %.backedge104
                                        #   in Loop: Header=BB131_12 Depth=1
	cmpl	%ebp, %r15d
	jne	.LBB131_12
# BB#21:                                # %._crit_edge110.loopexit
	movl	12(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %r12d
.LBB131_22:                             # %._crit_edge110
	movl	$.L.str.4, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movslq	%r15d, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	addq	%rax, %rcx
	movq	%rcx, %r13
	cmpl	%ecx, %r12d
	jge	.LBB131_31
# BB#23:                                # %.lr.ph
	movslq	%r12d, %rbp
	movl	%r13d, %r15d
	.p2align	4, 0x90
.LBB131_24:                             # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r12
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	24(%rbx), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB131_26
# BB#25:                                #   in Loop: Header=BB131_24 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB131_26:                             # %clause_PParentsLiteralFPrintUnsigned.exit
                                        #   in Loop: Header=BB131_24 Depth=1
	movq	%r14, %rdi
	callq	term_FPrintPrefix
	movq	stdout(%rip), %rdi
	callq	fflush
	testb	$1, (%rbx)
	je	.LBB131_29
# BB#27:                                #   in Loop: Header=BB131_24 Depth=1
	movl	$42, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	cmpl	$0, 8(%rbx)
	je	.LBB131_29
# BB#28:                                #   in Loop: Header=BB131_24 Depth=1
	movl	$42, %edi
	movq	%r14, %rsi
	callq	_IO_putc
.LBB131_29:                             #   in Loop: Header=BB131_24 Depth=1
	incq	%rbp
	cmpq	%r13, %rbp
	jge	.LBB131_30
# BB#35:                                #   in Loop: Header=BB131_24 Depth=1
	movl	$32, %edi
	movq	%r14, %rsi
	callq	_IO_putc
.LBB131_30:                             # %.backedge
                                        #   in Loop: Header=BB131_24 Depth=1
	cmpl	%ebp, %r15d
	movq	%r12, %rbx
	jne	.LBB131_24
.LBB131_31:                             # %._crit_edge
	movl	$46, %edi
	movq	%r14, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.LBB131_32:
	movl	$.L.str, %edi
	movl	$12, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.Lfunc_end131:
	.size	clause_PParentsFPrintGen, .Lfunc_end131-clause_PParentsFPrintGen
	.cfi_endproc

	.globl	clause_PParentsFPrint
	.p2align	4, 0x90
	.type	clause_PParentsFPrint,@function
clause_PParentsFPrint:                  # @clause_PParentsFPrint
	.cfi_startproc
# BB#0:
	movl	$1, %edx
	jmp	clause_PParentsFPrintGen # TAILCALL
.Lfunc_end132:
	.size	clause_PParentsFPrint, .Lfunc_end132-clause_PParentsFPrint
	.cfi_endproc

	.globl	clause_PParentsListFPrint
	.p2align	4, 0x90
	.type	clause_PParentsListFPrint,@function
clause_PParentsListFPrint:              # @clause_PParentsListFPrint
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi951:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi952:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi953:
	.cfi_def_cfa_offset 32
.Lcfi954:
	.cfi_offset %rbx, -24
.Lcfi955:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB133_3
	.p2align	4, 0x90
.LBB133_1:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	clause_PParentsFPrintGen
	movl	$10, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB133_1
.LBB133_3:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end133:
	.size	clause_PParentsListFPrint, .Lfunc_end133-clause_PParentsListFPrint
	.cfi_endproc

	.globl	clause_PParentsPrint
	.p2align	4, 0x90
	.type	clause_PParentsPrint,@function
clause_PParentsPrint:                   # @clause_PParentsPrint
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	stdout(%rip), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	jmp	clause_PParentsFPrintGen # TAILCALL
.Lfunc_end134:
	.size	clause_PParentsPrint, .Lfunc_end134-clause_PParentsPrint
	.cfi_endproc

	.globl	clause_PParentsListPrint
	.p2align	4, 0x90
	.type	clause_PParentsListPrint,@function
clause_PParentsListPrint:               # @clause_PParentsListPrint
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi956:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi957:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi958:
	.cfi_def_cfa_offset 32
.Lcfi959:
	.cfi_offset %rbx, -24
.Lcfi960:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB135_3
# BB#1:                                 # %.lr.ph.i.preheader
	movq	stdout(%rip), %r14
	.p2align	4, 0x90
.LBB135_2:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	clause_PParentsFPrintGen
	movl	$10, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB135_2
.LBB135_3:                              # %clause_PParentsListFPrint.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end135:
	.size	clause_PParentsListPrint, .Lfunc_end135-clause_PParentsListPrint
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_LiteralsCompare,@function
clause_LiteralsCompare:                 # @clause_LiteralsCompare
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	24(%rsi), %rsi
	jmp	term_CompareAbstractLEQ # TAILCALL
.Lfunc_end136:
	.size	clause_LiteralsCompare, .Lfunc_end136-clause_LiteralsCompare
	.cfi_endproc

	.type	clause_WEIGHTUNDEFINED,@object # @clause_WEIGHTUNDEFINED
	.section	.rodata,"a",@progbits
	.globl	clause_WEIGHTUNDEFINED
	.p2align	2
clause_WEIGHTUNDEFINED:
	.long	4294967295              # 0xffffffff
	.size	clause_WEIGHTUNDEFINED, 4

	.type	clause_SORT,@object     # @clause_SORT
	.comm	clause_SORT,168,16
	.type	clause_STAMPID,@object  # @clause_STAMPID
	.comm	clause_STAMPID,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"(CLAUSE)NULL"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"[%d:"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" || "
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" -> "
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"(strictly)"
	.size	.L.str.5, 11

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"."
	.size	.L.str.6, 2

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%d.%d"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	",%d.%d"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"App"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"EmS"
	.size	.L.str.10, 4

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"SoR"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"EqR"
	.size	.L.str.12, 4

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"EqF"
	.size	.L.str.13, 4

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"MPm"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SpR"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"SPm"
	.size	.L.str.16, 4

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"OPm"
	.size	.L.str.17, 4

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"SpL"
	.size	.L.str.18, 4

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Res"
	.size	.L.str.19, 4

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"SHy"
	.size	.L.str.20, 4

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"OHy"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"URR"
	.size	.L.str.22, 4

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Fac"
	.size	.L.str.23, 4

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Spt"
	.size	.L.str.24, 4

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Inp"
	.size	.L.str.25, 4

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Rew"
	.size	.L.str.26, 4

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"CRw"
	.size	.L.str.27, 4

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Con"
	.size	.L.str.28, 4

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"AED"
	.size	.L.str.29, 4

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Obv"
	.size	.L.str.30, 4

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"SSi"
	.size	.L.str.31, 4

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"MRR"
	.size	.L.str.32, 4

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"UnC"
	.size	.L.str.33, 4

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Def"
	.size	.L.str.34, 4

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Ter"
	.size	.L.str.35, 4

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str.36, 31

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/clause.c"
	.size	.L.str.37, 78

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"\nIn clause_GetOriginFromString: Unknown clause origin."
	.size	.L.str.38, 55

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.39, 133

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Temporary"
	.size	.L.str.40, 10

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"\n In clause_FPrintOrigin: Clause has no origin."
	.size	.L.str.41, 48

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	" c = %d a = %d s = %d"
	.size	.L.str.42, 22

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	" Weight : %d"
	.size	.L.str.43, 13

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	" Depth  : %d"
	.size	.L.str.44, 13

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	" %s %s "
	.size	.L.str.45, 8

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"WorkedOff"
	.size	.L.str.46, 10

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"Usable"
	.size	.L.str.47, 7

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Selected"
	.size	.L.str.48, 9

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"NotSelected"
	.size	.L.str.49, 12

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"||"
	.size	.L.str.50, 3

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"->"
	.size	.L.str.51, 3

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	".\n"
	.size	.L.str.52, 3

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	" $F "
	.size	.L.str.53, 5

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	" = "
	.size	.L.str.54, 4

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	" | "
	.size	.L.str.55, 4

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"list_of_clauses(axioms, cnf).\n"
	.size	.L.str.56, 31

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"end_of_list.\n\n"
	.size	.L.str.57, 15

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"list_of_clauses(conjectures, cnf).\n"
	.size	.L.str.58, 36

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"begin_problem(Unknown).\n\n"
	.size	.L.str.59, 26

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"list_of_symbols.\n"
	.size	.L.str.60, 18

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"\nend_problem.\n\n"
	.size	.L.str.61, 16

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"\nlist_of_symbols.\n"
	.size	.L.str.62, 19

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"list_of_formulae(axioms).\n"
	.size	.L.str.63, 27

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"list_of_formulae(conjectures).\n"
	.size	.L.str.64, 32

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"list_of_settings(SPASS).\n{*\n"
	.size	.L.str.65, 29

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"\n*}\nend_of_list.\n\nend_problem.\n\n"
	.size	.L.str.66, 33

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"list(usable).\n"
	.size	.L.str.67, 15

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"x=x.\n"
	.size	.L.str.68, 6

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"  step("
	.size	.L.str.69, 8

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"%d,"
	.size	.L.str.70, 4

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"(["
	.size	.L.str.71, 3

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"],"
	.size	.L.str.72, 3

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"),"
	.size	.L.str.73, 3

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	",["
	.size	.L.str.74, 3

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	",[splitlevel:%d]"
	.size	.L.str.75, 17

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	").\n"
	.size	.L.str.76, 4

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"  clause("
	.size	.L.str.77, 10

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"),%d"
	.size	.L.str.78, 5

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	",%d"
	.size	.L.str.79, 4

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"  formula("
	.size	.L.str.80, 11

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"\n In clause_Check: Clause not consistent !\n"
	.size	.L.str.81, 44

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"\n In clause_Check: Weight or maximal variable not properly set.\n"
	.size	.L.str.82, 65

	.type	clause_CLAUSECOUNTER,@object # @clause_CLAUSECOUNTER
	.comm	clause_CLAUSECOUNTER,4,4
	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"\n\n"
	.size	.L.str.83, 3

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"list_of_descriptions.\n"
	.size	.L.str.84, 23

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"name(%s).\n"
	.size	.L.str.85, 11

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"author(%s).\n"
	.size	.L.str.86, 13

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"status(%s).\n"
	.size	.L.str.87, 13

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"description(%s).\n"
	.size	.L.str.88, 18

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"end_of_list.\n"
	.size	.L.str.89, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
