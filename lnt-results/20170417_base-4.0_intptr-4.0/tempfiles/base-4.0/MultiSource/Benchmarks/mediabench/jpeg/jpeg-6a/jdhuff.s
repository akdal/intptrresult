	.text
	.file	"jdhuff.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI0_1:
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.text
	.globl	jpeg_make_d_derived_tbl
	.p2align	4, 0x90
	.type	jpeg_make_d_derived_tbl,@function
jpeg_make_d_derived_tbl:                # @jpeg_make_d_derived_tbl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1384, %rsp             # imm = 0x568
.Lcfi6:
	.cfi_def_cfa_offset 1440
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_2
# BB#1:
	movq	8(%rdi), %rax
	movl	$1, %esi
	movl	$1640, %edx             # imm = 0x668
	callq	*(%rax)
	movq	%rax, %rbx
	movq	%rbx, (%r15)
.LBB0_2:
	movq	%r14, 352(%rbx)
	xorl	%ebp, %ebp
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader92
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
                                        #     Child Loop BB0_12 Depth 2
	movzbl	(%r14,%r15), %r12d
	testl	%r12d, %r12d
	je	.LBB0_13
# BB#4:                                 # %.lr.ph116
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	%ebp, %rbp
	leaq	80(%rsp,%rbp), %rdi
	cmpl	$1, %r12d
	movl	$1, %edx
	cmoval	%r12d, %edx
	decl	%edx
	incq	%rdx
	movl	%r15d, %esi
	callq	memset
	cmpb	$4, %r12b
	jb	.LBB0_10
# BB#6:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r12d, %ecx
	andl	$3, %ecx
	movl	%r12d, %eax
	subl	%ecx, %eax
	movl	%r12d, %edx
	subl	%ecx, %edx
	movdqa	.LCPI0_0(%rip), %xmm2   # xmm2 = [1,1]
	je	.LBB0_10
# BB#7:                                 # %vector.ph
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r12d, %ecx
	andb	$3, %cl
	incl	%eax
	movd	%rbp, %xmm1
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_8:                                # %vector.body
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	paddq	%xmm2, %xmm1
	paddq	%xmm2, %xmm0
	addl	$-4, %edx
	jne	.LBB0_8
# BB#9:                                 # %middle.block
                                        #   in Loop: Header=BB0_3 Depth=1
	paddq	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddq	%xmm0, %xmm1
	movd	%xmm1, %rbp
	testb	%cl, %cl
	jne	.LBB0_11
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$1, %eax
.LBB0_11:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	decl	%eax
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rbp
	incl	%eax
	cmpl	%r12d, %eax
	jl	.LBB0_12
.LBB0_13:                               # %._crit_edge117
                                        #   in Loop: Header=BB0_3 Depth=1
	incq	%r15
	cmpq	$17, %r15
	jne	.LBB0_3
# BB#14:
	movslq	%ebp, %rax
	movb	$0, 80(%rsp,%rax)
	movsbl	80(%rsp), %eax
	testl	%eax, %eax
	je	.LBB0_25
# BB#15:                                # %.preheader91.preheader
	xorl	%ecx, %ecx
	movabsq	$4294967296, %r9        # imm = 0x100000000
	movl	%eax, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader91.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_19 Depth 2
                                        #     Child Loop BB0_21 Depth 2
                                        #     Child Loop BB0_23 Depth 2
	movsbl	%sil, %edi
	movl	%edi, %ebp
	subl	%eax, %ebp
	je	.LBB0_22
# BB#17:                                # %._crit_edge108.thread.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	leal	-1(%rdi), %r8d
	subl	%eax, %r8d
	andl	$7, %ebp
	je	.LBB0_20
# BB#18:                                # %._crit_edge108.thread.prol.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB0_19:                               # %._crit_edge108.thread.prol
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	%ecx, %ecx
	incl	%eax
	incl	%ebp
	jne	.LBB0_19
.LBB0_20:                               # %._crit_edge108.thread.prol.loopexit
                                        #   in Loop: Header=BB0_16 Depth=1
	cmpl	$7, %r8d
	jb	.LBB0_22
	.p2align	4, 0x90
.LBB0_21:                               # %._crit_edge108.thread
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shll	$8, %ecx
	addl	$8, %eax
	cmpl	%eax, %edi
	jne	.LBB0_21
.LBB0_22:                               # %.lr.ph107.preheader
                                        #   in Loop: Header=BB0_16 Depth=1
	movl	%ecx, %esi
	movq	%rdx, %rdi
	shlq	$32, %rdi
	movslq	%edx, %rdx
	leal	(%rsi,%rsi), %ecx
	.p2align	4, 0x90
.LBB0_23:                               # %.lr.ph107
                                        #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, 352(%rsp,%rdx,4)
	incl	%esi
	movsbl	81(%rsp,%rdx), %ebp
	incq	%rdx
	addq	%r9, %rdi
	addl	$2, %ecx
	cmpl	%eax, %ebp
	je	.LBB0_23
# BB#24:                                # %._crit_edge108
                                        #   in Loop: Header=BB0_16 Depth=1
	sarq	$32, %rdi
	movb	80(%rsp,%rdi), %sil
	incl	%eax
	testb	%sil, %sil
	jne	.LBB0_16
.LBB0_25:                               # %.preheader90.preheader
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_26:                               # %.preheader90
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 1(%r14,%rax)
	je	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_26 Depth=1
	movl	%ecx, 284(%rbx,%rax,4)
	movslq	%ecx, %rdx
	movl	352(%rsp,%rdx,4), %ecx
	movq	%rcx, 8(%rbx,%rax,8)
	movzbl	1(%r14,%rax), %ecx
	addq	%rdx, %rcx
	movl	348(%rsp,%rcx,4), %edx
	jmp	.LBB0_29
	.p2align	4, 0x90
.LBB0_28:                               #   in Loop: Header=BB0_26 Depth=1
	movq	$-1, %rdx
.LBB0_29:                               #   in Loop: Header=BB0_26 Depth=1
	movq	%rdx, 144(%rbx,%rax,8)
	incq	%rax
	cmpq	$16, %rax
	jne	.LBB0_26
# BB#30:
	movq	$1048575, 272(%rbx)     # imm = 0xFFFFF
	leaq	360(%rbx), %rdi
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	callq	memset
	leaq	364(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	1385(%rbx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %esi
	movdqa	.LCPI0_1(%rip), %xmm0   # xmm0 = [255,255,255,255]
	.p2align	4, 0x90
.LBB0_31:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_33 Depth 2
                                        #       Child Loop BB0_45 Depth 3
                                        #       Child Loop BB0_52 Depth 3
	cmpb	$0, (%r14,%rsi)
	je	.LBB0_54
# BB#32:                                # %.lr.ph97.split.us.preheader
                                        #   in Loop: Header=BB0_31 Depth=1
	movl	$8, %ecx
	subq	%rsi, %rcx
	movl	$1, %r8d
	movl	$1, %edi
	shll	%cl, %edi
	movslq	%r15d, %r15
	movl	$-2, %eax
	roll	%cl, %eax
	cmpl	$-3, %eax
	movl	$-2, %edx
	cmovlel	%edx, %eax
	leal	1(%rdi,%rax), %eax
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,4), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leaq	1(%rax), %rbp
	movq	%rbp, %r12
	movabsq	$8589934584, %rax       # imm = 0x1FFFFFFF8
	andq	%rax, %r12
	leaq	-8(%r12), %rax
	shrq	$3, %rax
	movl	%edi, %edx
	subl	%r12d, %edx
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movd	%esi, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movq	%rax, 56(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_33:                               # %.lr.ph97.split.us
                                        #   Parent Loop BB0_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_45 Depth 3
                                        #       Child Loop BB0_52 Depth 3
	movl	352(%rsp,%r15,4), %eax
	shll	%cl, %eax
	cmpq	$8, %rbp
	leaq	17(%r14,%r15), %r11
	movslq	%eax, %r9
	jae	.LBB0_35
# BB#34:                                #   in Loop: Header=BB0_33 Depth=2
	movl	%edi, %eax
	jmp	.LBB0_51
	.p2align	4, 0x90
.LBB0_35:                               # %min.iters.checked153
                                        #   in Loop: Header=BB0_33 Depth=2
	testq	%r12, %r12
	je	.LBB0_41
# BB#36:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_33 Depth=2
	movq	%rdi, %rcx
	movq	%r14, %r12
	leaq	360(%rbx,%r9,4), %rax
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r9,4), %rbp
	leaq	1384(%rbx,%r9), %rdi
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r9), %r10
	cmpq	%r10, %rax
	sbbb	%dl, %dl
	cmpq	%rbp, %rdi
	sbbb	%r14b, %r14b
	andb	%dl, %r14b
	cmpq	%r11, %rax
	sbbb	%dl, %dl
	cmpq	%rbp, %r11
	sbbb	%r13b, %r13b
	cmpq	%r11, %rdi
	sbbb	%al, %al
	cmpq	%r10, %r11
	sbbb	%r10b, %r10b
	testb	$1, %r14b
	jne	.LBB0_48
# BB#37:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_33 Depth=2
	andb	%r13b, %dl
	andb	$1, %dl
	movq	%r12, %r14
	jne	.LBB0_49
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_33 Depth=2
	andb	%r10b, %al
	andb	$1, %al
	movq	16(%rsp), %r12          # 8-byte Reload
	jne	.LBB0_42
# BB#39:                                # %vector.ph178
                                        #   in Loop: Header=BB0_33 Depth=2
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_43
# BB#40:                                # %vector.body148.prol
                                        #   in Loop: Header=BB0_33 Depth=2
	movdqu	%xmm1, 360(%rbx,%r9,4)
	movdqu	%xmm1, 376(%rbx,%r9,4)
	movzbl	(%r11), %eax
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	pand	%xmm0, %xmm2
	packuswb	%xmm2, %xmm2
	packuswb	%xmm2, %xmm2
	movd	%xmm2, 1384(%rbx,%r9)
	movd	%xmm2, 1388(%rbx,%r9)
	movl	$8, %r10d
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_44
	jmp	.LBB0_46
.LBB0_41:                               #   in Loop: Header=BB0_33 Depth=2
	movl	%edi, %eax
	jmp	.LBB0_51
.LBB0_42:                               #   in Loop: Header=BB0_33 Depth=2
	movq	%rcx, %rdi
	movl	%edi, %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	jmp	.LBB0_51
.LBB0_43:                               #   in Loop: Header=BB0_33 Depth=2
	xorl	%r10d, %r10d
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB0_46
.LBB0_44:                               # %vector.ph178.new
                                        #   in Loop: Header=BB0_33 Depth=2
	movzbl	(%r11), %eax
	movd	%eax, %xmm2
	pshufd	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movq	%r12, %rax
	subq	%r10, %rax
	addq	%r9, %r10
	.p2align	4, 0x90
.LBB0_45:                               # %vector.body148
                                        #   Parent Loop BB0_31 Depth=1
                                        #     Parent Loop BB0_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm1, 360(%rbx,%r10,4)
	movdqu	%xmm1, 376(%rbx,%r10,4)
	movdqa	%xmm2, %xmm3
	pand	%xmm0, %xmm3
	packuswb	%xmm3, %xmm3
	packuswb	%xmm3, %xmm3
	movd	%xmm3, 1384(%rbx,%r10)
	movd	%xmm3, 1388(%rbx,%r10)
	movdqu	%xmm1, 392(%rbx,%r10,4)
	movdqu	%xmm1, 408(%rbx,%r10,4)
	movd	%xmm3, 1392(%rbx,%r10)
	movd	%xmm3, 1396(%rbx,%r10)
	addq	$16, %r10
	addq	$-16, %rax
	jne	.LBB0_45
.LBB0_46:                               # %middle.block149
                                        #   in Loop: Header=BB0_33 Depth=2
	movq	(%rsp), %rbp            # 8-byte Reload
	cmpq	%r12, %rbp
	movq	%rcx, %rdi
	movq	8(%rsp), %rcx           # 8-byte Reload
	je	.LBB0_53
# BB#47:                                #   in Loop: Header=BB0_33 Depth=2
	addq	%r12, %r9
	movl	28(%rsp), %eax          # 4-byte Reload
	jmp	.LBB0_51
.LBB0_48:                               #   in Loop: Header=BB0_33 Depth=2
	movq	%rcx, %rdi
	movl	%edi, %eax
	movq	%r12, %r14
	jmp	.LBB0_50
.LBB0_49:                               #   in Loop: Header=BB0_33 Depth=2
	movq	%rcx, %rdi
	movl	%edi, %eax
.LBB0_50:                               # %scalar.ph150.preheader
                                        #   in Loop: Header=BB0_33 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_51:                               # %scalar.ph150.preheader
                                        #   in Loop: Header=BB0_33 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB0_52:                               # %scalar.ph150
                                        #   Parent Loop BB0_31 Depth=1
                                        #     Parent Loop BB0_33 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%esi, 360(%rbx,%r9,4)
	movzbl	(%r11), %edx
	movb	%dl, 1384(%rbx,%r9)
	incq	%r9
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB0_52
.LBB0_53:                               # %._crit_edge.us
                                        #   in Loop: Header=BB0_33 Depth=2
	incq	%r15
	movzbl	(%r14,%rsi), %eax
	cmpl	%eax, %r8d
	leal	1(%r8), %eax
	movl	%eax, %r8d
	jl	.LBB0_33
.LBB0_54:                               # %._crit_edge98
                                        #   in Loop: Header=BB0_31 Depth=1
	incq	%rsi
	cmpq	$9, %rsi
	jne	.LBB0_31
# BB#55:
	addq	$1384, %rsp             # imm = 0x568
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_make_d_derived_tbl, .Lfunc_end0-jpeg_make_d_derived_tbl
	.cfi_endproc

	.globl	jpeg_fill_bit_buffer
	.p2align	4, 0x90
	.type	jpeg_fill_bit_buffer,@function
jpeg_fill_bit_buffer:                   # @jpeg_fill_bit_buffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	(%r14), %rbp
	movq	8(%r14), %rbx
	cmpl	$24, %r12d
	jg	.LBB1_19
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_9 Depth 2
	cmpl	$0, 16(%r14)
	je	.LBB1_5
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpl	%r15d, %r12d
	jge	.LBB1_19
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movq	48(%r14), %rax
	cmpl	$0, (%rax)
	jne	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_1 Depth=1
	movq	40(%r14), %rdi
	movq	(%rdi), %rax
	movl	$113, 40(%rax)
	movl	$-1, %esi
	callq	*8(%rax)
	movq	48(%r14), %rax
	movl	$1, (%rax)
.LBB1_16:                               # %.loopexit57
                                        #   in Loop: Header=BB1_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_1 Depth=1
	testq	%rbx, %rbx
	jne	.LBB1_8
# BB#6:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	40(%r14), %rdi
	movq	32(%rdi), %rax
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB1_20
# BB#7:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	40(%r14), %rax
	movq	32(%rax), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rbx
.LBB1_8:                                #   in Loop: Header=BB1_1 Depth=1
	decq	%rbx
	movzbl	(%rbp), %eax
	incq	%rbp
	cmpq	$255, %rax
	je	.LBB1_9
.LBB1_17:                               # %.loopexit57
                                        #   in Loop: Header=BB1_1 Depth=1
	shlq	$8, %r13
	orq	%rax, %r13
	addl	$8, %r12d
	cmpl	$25, %r12d
	jl	.LBB1_1
	jmp	.LBB1_19
	.p2align	4, 0x90
.LBB1_9:                                # %.preheader
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	jne	.LBB1_12
# BB#10:                                #   in Loop: Header=BB1_9 Depth=2
	movq	40(%r14), %rdi
	movq	32(%rdi), %rax
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB1_20
# BB#11:                                #   in Loop: Header=BB1_9 Depth=2
	movq	40(%r14), %rax
	movq	32(%rax), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rbx
.LBB1_12:                               #   in Loop: Header=BB1_9 Depth=2
	decq	%rbx
	movzbl	(%rbp), %eax
	incq	%rbp
	cmpl	$255, %eax
	je	.LBB1_9
# BB#13:                                #   in Loop: Header=BB1_1 Depth=1
	testb	%al, %al
	jne	.LBB1_18
# BB#14:                                # %.loopexit57.loopexit
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	$255, %eax
	jmp	.LBB1_17
.LBB1_18:                               #   in Loop: Header=BB1_1 Depth=1
	movl	%eax, 16(%r14)
	cmpl	%r15d, %r12d
	jl	.LBB1_3
.LBB1_19:                               # %._crit_edge
	movq	%rbp, (%r14)
	movq	%rbx, 8(%r14)
	movq	%r13, 24(%r14)
	movl	%r12d, 32(%r14)
	movl	$1, %eax
	jmp	.LBB1_21
.LBB1_20:
	xorl	%eax, %eax
.LBB1_21:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	jpeg_fill_bit_buffer, .Lfunc_end1-jpeg_fill_bit_buffer
	.cfi_endproc

	.globl	jpeg_huff_decode
	.p2align	4, 0x90
	.type	jpeg_huff_decode,@function
jpeg_huff_decode:                       # @jpeg_huff_decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -48
.Lcfi32:
	.cfi_offset %r12, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movq	%rcx, %r14
	movl	%edx, %eax
	movq	%rdi, %r12
	cmpl	%r15d, %eax
	jge	.LBB2_3
# BB#1:
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%r15d, %ecx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB2_13
# BB#2:
	movq	24(%r12), %rsi
	movl	32(%r12), %eax
.LBB2_3:
	subl	%r15d, %eax
	movq	%rsi, %rdx
	movl	%eax, %ecx
	sarq	%cl, %rdx
	movl	$1, %edi
	movl	%r15d, %ecx
	shll	%cl, %edi
	decl	%edi
	andl	%edx, %edi
	movslq	%edi, %rbx
	movslq	%r15d, %rbp
	cmpq	136(%r14,%rbp,8), %rbx
	jle	.LBB2_9
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	jg	.LBB2_7
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=1
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	%eax, %edx
	callq	jpeg_fill_bit_buffer
	testl	%eax, %eax
	je	.LBB2_13
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=1
	movq	24(%r12), %rsi
	movl	32(%r12), %eax
.LBB2_7:                                #   in Loop: Header=BB2_4 Depth=1
	movq	%rbx, %rdx
	addq	%rdx, %rdx
	decl	%eax
	movq	%rsi, %rbx
	movl	%eax, %ecx
	shrq	%cl, %rbx
	andl	$1, %ebx
	orq	%rdx, %rbx
	cmpq	144(%r14,%rbp,8), %rbx
	leaq	1(%rbp), %rbp
	jg	.LBB2_4
# BB#8:
	movl	%ebp, %r15d
.LBB2_9:                                # %._crit_edge
	movq	%rsi, 24(%r12)
	movl	%eax, 32(%r12)
	cmpl	$17, %r15d
	jl	.LBB2_11
# BB#10:
	movq	40(%r12), %rdi
	movq	(%rdi), %rax
	movl	$114, 40(%rax)
	movl	$-1, %esi
	callq	*8(%rax)
	xorl	%eax, %eax
	jmp	.LBB2_14
.LBB2_11:
	movq	352(%r14), %rax
	movslq	280(%r14,%rbp,4), %rcx
	subl	(%r14,%rbp,8), %ebx
	movslq	%ebx, %rdx
	addq	%rcx, %rdx
	movzbl	17(%rax,%rdx), %eax
	jmp	.LBB2_14
.LBB2_13:
	movl	$-1, %eax
.LBB2_14:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	jpeg_huff_decode, .Lfunc_end2-jpeg_huff_decode
	.cfi_endproc

	.globl	jinit_huff_decoder
	.p2align	4, 0x90
	.type	jinit_huff_decoder,@function
jinit_huff_decoder:                     # @jinit_huff_decoder
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 16
.Lcfi37:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$120, %edx
	callq	*(%rax)
	movq	%rax, 576(%rbx)
	movq	$start_pass_huff_decoder, (%rax)
	movq	$decode_mcu, 8(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rax)
	movups	%xmm0, 56(%rax)
	movups	%xmm0, 104(%rax)
	movups	%xmm0, 72(%rax)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	jinit_huff_decoder, .Lfunc_end3-jinit_huff_decoder
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_huff_decoder,@function
start_pass_huff_decoder:                # @start_pass_huff_decoder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 64
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	576(%r15), %r14
	cmpl	$0, 508(%r15)
	jne	.LBB4_4
# BB#1:
	cmpl	$63, 512(%r15)
	jne	.LBB4_4
# BB#2:
	cmpl	$0, 516(%r15)
	jne	.LBB4_4
# BB#3:
	cmpl	$0, 520(%r15)
	je	.LBB4_5
.LBB4_4:
	movq	(%r15), %rax
	movl	$118, 40(%rax)
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	*8(%rax)
.LBB4_5:                                # %.preheader
	cmpl	$0, 416(%r15)
	jle	.LBB4_14
# BB#6:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	movq	424(%r15,%rbx,8), %rax
	movslq	20(%rax), %r12
	cmpq	$3, %r12
	movl	24(%rax), %r13d
	ja	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=1
	cmpq	$0, 224(%r15,%r12,8)
	jne	.LBB4_10
.LBB4_9:                                #   in Loop: Header=BB4_7 Depth=1
	movq	(%r15), %rax
	movl	$49, 40(%rax)
	movl	%r12d, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB4_10:                               #   in Loop: Header=BB4_7 Depth=1
	movslq	%r13d, %rbp
	cmpl	$3, %ebp
	ja	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_7 Depth=1
	cmpq	$0, 256(%r15,%rbp,8)
	jne	.LBB4_13
.LBB4_12:                               # %._crit_edge64
                                        #   in Loop: Header=BB4_7 Depth=1
	movq	(%r15), %rax
	movl	$49, 40(%rax)
	movl	%r13d, 44(%rax)
	movq	%r15, %rdi
	callq	*(%rax)
.LBB4_13:                               #   in Loop: Header=BB4_7 Depth=1
	movq	224(%r15,%r12,8), %rsi
	leaq	56(%r14,%r12,8), %rdx
	movq	%r15, %rdi
	callq	jpeg_make_d_derived_tbl
	movq	256(%r15,%rbp,8), %rsi
	leaq	88(%r14,%rbp,8), %rdx
	movq	%r15, %rdi
	callq	jpeg_make_d_derived_tbl
	movl	$0, 32(%r14,%rbx,4)
	incq	%rbx
	movslq	416(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB4_7
.LBB4_14:                               # %._crit_edge
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r14)
	movl	360(%r15), %eax
	movl	%eax, 48(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	start_pass_huff_decoder, .Lfunc_end4-start_pass_huff_decoder
	.cfi_endproc

	.p2align	4, 0x90
	.type	decode_mcu,@function
decode_mcu:                             # @decode_mcu
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 208
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	movq	576(%rbp), %rbx
	cmpl	$0, 360(%rbp)
	je	.LBB5_7
# BB#1:
	cmpl	$0, 48(%rbx)
	jne	.LBB5_7
# BB#2:
	movl	24(%rbx), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movq	568(%rbp), %rax
	addl	%ecx, 172(%rax)
	movl	$0, 24(%rbx)
	movq	%rbp, %rdi
	callq	*16(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#3:                                 # %.preheader.i
	cmpl	$0, 416(%rbp)
	jle	.LBB5_6
# BB#4:                                 # %.lr.ph.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 32(%rbx,%rax,4)
	incq	%rax
	movslq	416(%rbp), %rcx
	cmpq	%rcx, %rax
	jl	.LBB5_5
.LBB5_6:                                # %process_restart.exit
	movl	360(%rbp), %eax
	movl	%eax, 48(%rbx)
	movl	$0, 28(%rbx)
.LBB5_7:
	movq	%rbp, 56(%rsp)
	movq	32(%rbp), %rax
	movups	(%rax), %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	524(%rbp), %ecx
	movl	%ecx, 32(%rsp)
	movq	16(%rbx), %r14
	movl	24(%rbx), %r12d
	leaq	28(%rbx), %rdx
	movq	%rdx, 64(%rsp)
	movups	32(%rbx), %xmm1
	movaps	%xmm1, 128(%rsp)
	cmpl	$0, 464(%rbp)
	jle	.LBB5_173
# BB#8:                                 # %.lr.ph
	xorl	%r15d, %r15d
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 88(%rsp)          # 8-byte Spill
.LBB5_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_11 Depth 2
                                        #       Child Loop BB5_21 Depth 3
                                        #     Child Loop BB5_38 Depth 2
                                        #       Child Loop BB5_48 Depth 3
                                        #     Child Loop BB5_63 Depth 2
                                        #       Child Loop BB5_65 Depth 3
                                        #         Child Loop BB5_73 Depth 4
                                        #       Child Loop BB5_94 Depth 3
                                        #         Child Loop BB5_104 Depth 4
                                        #     Child Loop BB5_119 Depth 2
                                        #       Child Loop BB5_121 Depth 3
                                        #         Child Loop BB5_129 Depth 4
                                        #       Child Loop BB5_150 Depth 3
                                        #         Child Loop BB5_160 Depth 4
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movslq	468(%rbp,%r15,4), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	424(%rbp,%rax,8), %r13
	movslq	20(%r13), %rax
	movq	56(%rbx,%rax,8), %r9
	movslq	24(%r13), %rax
	movq	88(%rbx,%rax,8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	cmpl	$7, %r12d
	movq	%r15, 80(%rsp)          # 8-byte Spill
	movq	%r13, 72(%rsp)          # 8-byte Spill
	jg	.LBB5_30
# BB#10:                                # %.lr.ph.i237
                                        #   in Loop: Header=BB5_9 Depth=1
	movq	16(%rsp), %rbx
	movq	24(%rsp), %rbp
	movl	32(%rsp), %r15d
	movq	%r9, 8(%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB5_11:                               #   Parent Loop BB5_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_21 Depth 3
	testl	%r15d, %r15d
	je	.LBB5_16
# BB#12:                                #   in Loop: Header=BB5_11 Depth=2
	testl	%r12d, %r12d
	jns	.LBB5_28
.LBB5_13:                               #   in Loop: Header=BB5_11 Depth=2
	movq	64(%rsp), %r13
	cmpl	$0, (%r13)
	jne	.LBB5_15
# BB#14:                                #   in Loop: Header=BB5_11 Depth=2
	movq	56(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$113, 40(%rax)
	movl	$-1, %esi
	callq	*8(%rax)
	movq	8(%rsp), %r9            # 8-byte Reload
	movl	$1, (%r13)
.LBB5_15:                               #   in Loop: Header=BB5_11 Depth=2
	xorl	%eax, %eax
	movq	72(%rsp), %r13          # 8-byte Reload
	jmp	.LBB5_20
	.p2align	4, 0x90
.LBB5_16:                               #   in Loop: Header=BB5_11 Depth=2
	testq	%rbp, %rbp
	jne	.LBB5_19
# BB#17:                                #   in Loop: Header=BB5_11 Depth=2
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#18:                                #   in Loop: Header=BB5_11 Depth=2
	movq	32(%rbp), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rbp
	movq	8(%rsp), %r9            # 8-byte Reload
.LBB5_19:                               #   in Loop: Header=BB5_11 Depth=2
	decq	%rbp
	movzbl	(%rbx), %eax
	incq	%rbx
	xorl	%r15d, %r15d
	cmpq	$255, %rax
	je	.LBB5_21
.LBB5_20:                               # %.loopexit57.i
                                        #   in Loop: Header=BB5_11 Depth=2
	shlq	$8, %r14
	orq	%rax, %r14
	addl	$8, %r12d
	cmpl	$25, %r12d
	jl	.LBB5_11
	jmp	.LBB5_29
	.p2align	4, 0x90
.LBB5_21:                               # %.preheader.i238
                                        #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rbp, %rbp
	jne	.LBB5_24
# BB#22:                                #   in Loop: Header=BB5_21 Depth=3
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#23:                                #   in Loop: Header=BB5_21 Depth=3
	movq	32(%rbp), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rbp
	movq	8(%rsp), %r9            # 8-byte Reload
.LBB5_24:                               #   in Loop: Header=BB5_21 Depth=3
	decq	%rbp
	movzbl	(%rbx), %r15d
	incq	%rbx
	cmpl	$255, %r15d
	je	.LBB5_21
# BB#25:                                #   in Loop: Header=BB5_11 Depth=2
	testb	%r15b, %r15b
	jne	.LBB5_27
# BB#26:                                # %.loopexit57.i.loopexit
                                        #   in Loop: Header=BB5_11 Depth=2
	xorl	%r15d, %r15d
	movl	$255, %eax
	jmp	.LBB5_20
.LBB5_27:                               #   in Loop: Header=BB5_11 Depth=2
	movl	%r15d, 32(%rsp)
	testl	%r12d, %r12d
	js	.LBB5_13
.LBB5_28:                               #   in Loop: Header=BB5_9 Depth=1
	movq	%rbx, 16(%rsp)
	movq	%rbp, 24(%rsp)
	movq	%r14, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movl	$1, %r8d
	cmpl	$8, %r12d
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
	jl	.LBB5_33
	jmp	.LBB5_30
.LBB5_29:                               # %.thread
                                        #   in Loop: Header=BB5_9 Depth=1
	movq	%rbx, 16(%rsp)
	movq	%rbp, 24(%rsp)
	movq	%r14, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
.LBB5_30:                               #   in Loop: Header=BB5_9 Depth=1
	leal	-8(%r12), %ecx
	movq	%r14, %rax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarq	%cl, %rax
	movzbl	%al, %eax
	movl	360(%r9,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB5_32
# BB#31:                                #   in Loop: Header=BB5_9 Depth=1
	subl	%ecx, %r12d
	movzbl	1384(%r9,%rax), %eax
	testl	%eax, %eax
	jne	.LBB5_35
	jmp	.LBB5_59
.LBB5_32:                               #   in Loop: Header=BB5_9 Depth=1
	movl	$9, %r8d
.LBB5_33:                               #   in Loop: Header=BB5_9 Depth=1
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	movl	%r12d, %edx
	movq	%r9, %rcx
	callq	jpeg_huff_decode
	testl	%eax, %eax
	js	.LBB5_189
# BB#34:                                #   in Loop: Header=BB5_9 Depth=1
	movq	40(%rsp), %r14
	movl	48(%rsp), %r12d
	testl	%eax, %eax
	je	.LBB5_59
.LBB5_35:                               #   in Loop: Header=BB5_9 Depth=1
	cmpl	%eax, %r12d
	jge	.LBB5_57
# BB#36:                                #   in Loop: Header=BB5_9 Depth=1
	movq	16(%rsp), %rbp
	movq	24(%rsp), %rbx
	cmpl	$24, %r12d
	jg	.LBB5_56
# BB#37:                                # %.lr.ph.i240.preheader
                                        #   in Loop: Header=BB5_9 Depth=1
	movl	32(%rsp), %r15d
	movl	%eax, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB5_38:                               # %.lr.ph.i240
                                        #   Parent Loop BB5_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_48 Depth 3
	testl	%r15d, %r15d
	je	.LBB5_43
# BB#39:                                #   in Loop: Header=BB5_38 Depth=2
	cmpl	%eax, %r12d
	jge	.LBB5_55
.LBB5_40:                               #   in Loop: Header=BB5_38 Depth=2
	movq	64(%rsp), %r13
	cmpl	$0, (%r13)
	jne	.LBB5_42
# BB#41:                                #   in Loop: Header=BB5_38 Depth=2
	movq	56(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$113, 40(%rax)
	movl	$-1, %esi
	callq	*8(%rax)
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	$1, (%r13)
.LBB5_42:                               #   in Loop: Header=BB5_38 Depth=2
	xorl	%ecx, %ecx
	movq	72(%rsp), %r13          # 8-byte Reload
	jmp	.LBB5_47
	.p2align	4, 0x90
.LBB5_43:                               #   in Loop: Header=BB5_38 Depth=2
	testq	%rbx, %rbx
	jne	.LBB5_46
# BB#44:                                #   in Loop: Header=BB5_38 Depth=2
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#45:                                #   in Loop: Header=BB5_38 Depth=2
	movq	32(%rbp), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rbx
	movl	8(%rsp), %eax           # 4-byte Reload
.LBB5_46:                               #   in Loop: Header=BB5_38 Depth=2
	decq	%rbx
	movzbl	(%rbp), %ecx
	incq	%rbp
	xorl	%r15d, %r15d
	cmpq	$255, %rcx
	je	.LBB5_48
.LBB5_47:                               # %.loopexit57.i257
                                        #   in Loop: Header=BB5_38 Depth=2
	shlq	$8, %r14
	orq	%rcx, %r14
	addl	$8, %r12d
	cmpl	$25, %r12d
	jl	.LBB5_38
	jmp	.LBB5_56
	.p2align	4, 0x90
.LBB5_48:                               # %.preheader.i249
                                        #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rbx, %rbx
	jne	.LBB5_51
# BB#49:                                #   in Loop: Header=BB5_48 Depth=3
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#50:                                #   in Loop: Header=BB5_48 Depth=3
	movq	32(%rbp), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rbx
	movl	8(%rsp), %eax           # 4-byte Reload
.LBB5_51:                               #   in Loop: Header=BB5_48 Depth=3
	decq	%rbx
	movzbl	(%rbp), %r15d
	incq	%rbp
	cmpl	$255, %r15d
	je	.LBB5_48
# BB#52:                                #   in Loop: Header=BB5_38 Depth=2
	testb	%r15b, %r15b
	jne	.LBB5_54
# BB#53:                                # %.loopexit57.i257.loopexit
                                        #   in Loop: Header=BB5_38 Depth=2
	xorl	%r15d, %r15d
	movl	$255, %ecx
	jmp	.LBB5_47
.LBB5_54:                               #   in Loop: Header=BB5_38 Depth=2
	movl	%r15d, 32(%rsp)
	cmpl	%eax, %r12d
	jl	.LBB5_40
.LBB5_55:                               #   in Loop: Header=BB5_9 Depth=1
	movq	72(%rsp), %r13          # 8-byte Reload
.LBB5_56:                               # %.loopexit392
                                        #   in Loop: Header=BB5_9 Depth=1
	movq	%rbp, 16(%rsp)
	movq	%rbx, 24(%rsp)
	movq	%r14, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
.LBB5_57:                               #   in Loop: Header=BB5_9 Depth=1
	subl	%eax, %r12d
	movq	%r14, %rdx
	movl	%r12d, %ecx
	sarq	%cl, %rdx
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	decl	%esi
	andl	%edx, %esi
	movslq	%eax, %rcx
	cmpl	extend_test(,%rcx,4), %esi
	jge	.LBB5_60
# BB#58:                                #   in Loop: Header=BB5_9 Depth=1
	addl	extend_offset(,%rcx,4), %esi
	cmpl	$0, 48(%r13)
	jne	.LBB5_61
	jmp	.LBB5_118
.LBB5_59:                               #   in Loop: Header=BB5_9 Depth=1
	xorl	%esi, %esi
.LBB5_60:                               #   in Loop: Header=BB5_9 Depth=1
	cmpl	$0, 48(%r13)
	je	.LBB5_118
.LBB5_61:                               #   in Loop: Header=BB5_9 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	addl	128(%rsp,%rcx,4), %esi
	movl	%esi, 128(%rsp,%rcx,4)
	movq	104(%rsp), %rcx         # 8-byte Reload
	movw	%si, (%rcx)
	cmpl	$2, 36(%r13)
	jl	.LBB5_118
# BB#62:                                # %.preheader386.preheader
                                        #   in Loop: Header=BB5_9 Depth=1
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
.LBB5_63:                               # %.preheader386
                                        #   Parent Loop BB5_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_65 Depth 3
                                        #         Child Loop BB5_73 Depth 4
                                        #       Child Loop BB5_94 Depth 3
                                        #         Child Loop BB5_104 Depth 4
	cmpl	$7, %r12d
	jg	.LBB5_85
# BB#64:                                # %.lr.ph.i265
                                        #   in Loop: Header=BB5_63 Depth=2
	movq	16(%rsp), %rbx
	movq	24(%rsp), %rbp
	movl	32(%rsp), %r15d
	.p2align	4, 0x90
.LBB5_65:                               #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_63 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_73 Depth 4
	testl	%r15d, %r15d
	je	.LBB5_69
# BB#66:                                #   in Loop: Header=BB5_65 Depth=3
	testl	%r12d, %r12d
	jns	.LBB5_83
.LBB5_67:                               #   in Loop: Header=BB5_65 Depth=3
	movq	64(%rsp), %r13
	cmpl	$0, (%r13)
	jne	.LBB5_80
# BB#79:                                #   in Loop: Header=BB5_65 Depth=3
	movq	56(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$113, 40(%rax)
	movl	$-1, %esi
	callq	*8(%rax)
	movl	$1, (%r13)
.LBB5_80:                               # %.loopexit57.i282
                                        #   in Loop: Header=BB5_65 Depth=3
	xorl	%eax, %eax
	jmp	.LBB5_81
	.p2align	4, 0x90
.LBB5_69:                               #   in Loop: Header=BB5_65 Depth=3
	testq	%rbp, %rbp
	jne	.LBB5_72
# BB#70:                                #   in Loop: Header=BB5_65 Depth=3
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#71:                                #   in Loop: Header=BB5_65 Depth=3
	movq	32(%rbp), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rbp
.LBB5_72:                               #   in Loop: Header=BB5_65 Depth=3
	decq	%rbp
	movzbl	(%rbx), %eax
	incq	%rbx
	xorl	%r15d, %r15d
	cmpq	$255, %rax
	je	.LBB5_73
.LBB5_81:                               # %.loopexit57.i282
                                        #   in Loop: Header=BB5_65 Depth=3
	shlq	$8, %r14
	orq	%rax, %r14
	addl	$8, %r12d
	cmpl	$25, %r12d
	jl	.LBB5_65
	jmp	.LBB5_84
	.p2align	4, 0x90
.LBB5_73:                               # %.preheader.i274
                                        #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_63 Depth=2
                                        #       Parent Loop BB5_65 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rbp, %rbp
	jne	.LBB5_76
# BB#74:                                #   in Loop: Header=BB5_73 Depth=4
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#75:                                #   in Loop: Header=BB5_73 Depth=4
	movq	32(%rbp), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rbp
.LBB5_76:                               #   in Loop: Header=BB5_73 Depth=4
	decq	%rbp
	movzbl	(%rbx), %r15d
	incq	%rbx
	cmpl	$255, %r15d
	je	.LBB5_73
# BB#77:                                #   in Loop: Header=BB5_65 Depth=3
	testb	%r15b, %r15b
	jne	.LBB5_82
# BB#78:                                # %.loopexit57.i282.loopexit
                                        #   in Loop: Header=BB5_65 Depth=3
	xorl	%r15d, %r15d
	movl	$255, %eax
	jmp	.LBB5_81
.LBB5_82:                               #   in Loop: Header=BB5_65 Depth=3
	movl	%r15d, 32(%rsp)
	testl	%r12d, %r12d
	js	.LBB5_67
.LBB5_83:                               #   in Loop: Header=BB5_63 Depth=2
	movq	%rbx, 16(%rsp)
	movq	%rbp, 24(%rsp)
	movq	%r14, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movl	$1, %r8d
	cmpl	$8, %r12d
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	jl	.LBB5_88
	jmp	.LBB5_85
.LBB5_84:                               # %.thread531
                                        #   in Loop: Header=BB5_63 Depth=2
	movq	%rbx, 16(%rsp)
	movq	%rbp, 24(%rsp)
	movq	%r14, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
.LBB5_85:                               #   in Loop: Header=BB5_63 Depth=2
	leal	-8(%r12), %ecx
	movq	%r14, %rax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarq	%cl, %rax
	movzbl	%al, %eax
	movq	112(%rsp), %rdx         # 8-byte Reload
	movl	360(%rdx,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB5_87
# BB#86:                                #   in Loop: Header=BB5_63 Depth=2
	subl	%ecx, %r12d
	movzbl	1384(%rdx,%rax), %eax
	jmp	.LBB5_90
.LBB5_87:                               #   in Loop: Header=BB5_63 Depth=2
	movl	$9, %r8d
.LBB5_88:                               #   in Loop: Header=BB5_63 Depth=2
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	movl	%r12d, %edx
	movq	112(%rsp), %rcx         # 8-byte Reload
	callq	jpeg_huff_decode
	testl	%eax, %eax
	js	.LBB5_189
# BB#89:                                #   in Loop: Header=BB5_63 Depth=2
	movq	40(%rsp), %r14
	movl	48(%rsp), %r12d
.LBB5_90:                               #   in Loop: Header=BB5_63 Depth=2
	movl	%eax, %esi
	sarl	$4, %esi
	andl	$15, %eax
	je	.LBB5_115
# BB#91:                                #   in Loop: Header=BB5_63 Depth=2
	cmpl	%eax, %r12d
	jge	.LBB5_112
# BB#92:                                #   in Loop: Header=BB5_63 Depth=2
	movq	16(%rsp), %rbp
	movq	24(%rsp), %rbx
	cmpl	$24, %r12d
	movl	%esi, 120(%rsp)         # 4-byte Spill
	movl	%eax, 72(%rsp)          # 4-byte Spill
	jg	.LBB5_111
# BB#93:                                # %.lr.ph.i290.preheader
                                        #   in Loop: Header=BB5_63 Depth=2
	movl	32(%rsp), %r13d
	.p2align	4, 0x90
.LBB5_94:                               # %.lr.ph.i290
                                        #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_63 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_104 Depth 4
	testl	%r13d, %r13d
	je	.LBB5_99
# BB#95:                                #   in Loop: Header=BB5_94 Depth=3
	cmpl	72(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB5_111
.LBB5_96:                               #   in Loop: Header=BB5_94 Depth=3
	movq	64(%rsp), %r15
	cmpl	$0, (%r15)
	jne	.LBB5_98
# BB#97:                                #   in Loop: Header=BB5_94 Depth=3
	movq	56(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$113, 40(%rax)
	movl	$-1, %esi
	callq	*8(%rax)
	movl	$1, (%r15)
.LBB5_98:                               #   in Loop: Header=BB5_94 Depth=3
	xorl	%eax, %eax
	movq	80(%rsp), %r15          # 8-byte Reload
	jmp	.LBB5_103
.LBB5_99:                               #   in Loop: Header=BB5_94 Depth=3
	testq	%rbx, %rbx
	jne	.LBB5_102
# BB#100:                               #   in Loop: Header=BB5_94 Depth=3
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#101:                               #   in Loop: Header=BB5_94 Depth=3
	movq	32(%rbp), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rbx
.LBB5_102:                              #   in Loop: Header=BB5_94 Depth=3
	decq	%rbx
	movzbl	(%rbp), %eax
	incq	%rbp
	xorl	%r13d, %r13d
	cmpq	$255, %rax
	je	.LBB5_104
.LBB5_103:                              # %.loopexit57.i307
                                        #   in Loop: Header=BB5_94 Depth=3
	shlq	$8, %r14
	orq	%rax, %r14
	addl	$8, %r12d
	cmpl	$25, %r12d
	jl	.LBB5_94
	jmp	.LBB5_111
	.p2align	4, 0x90
.LBB5_104:                              # %.preheader.i299
                                        #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_63 Depth=2
                                        #       Parent Loop BB5_94 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rbx, %rbx
	jne	.LBB5_107
# BB#105:                               #   in Loop: Header=BB5_104 Depth=4
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#106:                               #   in Loop: Header=BB5_104 Depth=4
	movq	32(%rbp), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rbx
.LBB5_107:                              #   in Loop: Header=BB5_104 Depth=4
	decq	%rbx
	movzbl	(%rbp), %r13d
	incq	%rbp
	cmpl	$255, %r13d
	je	.LBB5_104
# BB#108:                               #   in Loop: Header=BB5_94 Depth=3
	testb	%r13b, %r13b
	jne	.LBB5_110
# BB#109:                               # %.loopexit57.i307.loopexit
                                        #   in Loop: Header=BB5_94 Depth=3
	xorl	%r13d, %r13d
	movl	$255, %eax
	jmp	.LBB5_103
.LBB5_110:                              #   in Loop: Header=BB5_94 Depth=3
	movl	%r13d, 32(%rsp)
	cmpl	72(%rsp), %r12d         # 4-byte Folded Reload
	jl	.LBB5_96
.LBB5_111:                              # %.loopexit380
                                        #   in Loop: Header=BB5_63 Depth=2
	movq	%rbp, 16(%rsp)
	movq	%rbx, 24(%rsp)
	movq	%r14, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movl	120(%rsp), %esi         # 4-byte Reload
	movl	72(%rsp), %eax          # 4-byte Reload
.LBB5_112:                              #   in Loop: Header=BB5_63 Depth=2
	addl	8(%rsp), %esi           # 4-byte Folded Reload
	subl	%eax, %r12d
	movq	%r14, %rdx
	movl	%r12d, %ecx
	sarq	%cl, %rdx
	movl	$1, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	decl	%edi
	andl	%edx, %edi
	movl	%eax, %ecx
	cmpl	extend_test(,%rcx,4), %edi
	jge	.LBB5_114
# BB#113:                               #   in Loop: Header=BB5_63 Depth=2
	addl	extend_offset(,%rcx,4), %edi
.LBB5_114:                              #   in Loop: Header=BB5_63 Depth=2
	movslq	%esi, %rcx
	movslq	jpeg_natural_order(,%rcx,4), %rcx
	movq	104(%rsp), %rdx         # 8-byte Reload
	movw	%di, (%rdx,%rcx,2)
	jmp	.LBB5_117
.LBB5_115:                              #   in Loop: Header=BB5_63 Depth=2
	cmpl	$15, %esi
	jne	.LBB5_171
# BB#116:                               #   in Loop: Header=BB5_63 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	addl	$15, %eax
	movl	%eax, %esi
.LBB5_117:                              #   in Loop: Header=BB5_63 Depth=2
	incl	%esi
	cmpl	$64, %esi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	jl	.LBB5_63
	jmp	.LBB5_171
	.p2align	4, 0x90
.LBB5_118:                              # %.preheader.preheader
                                        #   in Loop: Header=BB5_9 Depth=1
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB5_119:                              # %.preheader
                                        #   Parent Loop BB5_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_121 Depth 3
                                        #         Child Loop BB5_129 Depth 4
                                        #       Child Loop BB5_150 Depth 3
                                        #         Child Loop BB5_160 Depth 4
	cmpl	$7, %r12d
	jg	.LBB5_141
# BB#120:                               # %.lr.ph.i315
                                        #   in Loop: Header=BB5_119 Depth=2
	movq	16(%rsp), %rbx
	movq	24(%rsp), %rbp
	movl	32(%rsp), %r15d
	.p2align	4, 0x90
.LBB5_121:                              #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_119 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_129 Depth 4
	testl	%r15d, %r15d
	je	.LBB5_125
# BB#122:                               #   in Loop: Header=BB5_121 Depth=3
	testl	%r12d, %r12d
	jns	.LBB5_139
.LBB5_123:                              #   in Loop: Header=BB5_121 Depth=3
	movq	64(%rsp), %r13
	cmpl	$0, (%r13)
	jne	.LBB5_136
# BB#135:                               #   in Loop: Header=BB5_121 Depth=3
	movq	56(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$113, 40(%rax)
	movl	$-1, %esi
	callq	*8(%rax)
	movl	$1, (%r13)
.LBB5_136:                              # %.loopexit57.i332
                                        #   in Loop: Header=BB5_121 Depth=3
	xorl	%eax, %eax
	jmp	.LBB5_137
	.p2align	4, 0x90
.LBB5_125:                              #   in Loop: Header=BB5_121 Depth=3
	testq	%rbp, %rbp
	jne	.LBB5_128
# BB#126:                               #   in Loop: Header=BB5_121 Depth=3
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#127:                               #   in Loop: Header=BB5_121 Depth=3
	movq	32(%rbp), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rbp
.LBB5_128:                              #   in Loop: Header=BB5_121 Depth=3
	decq	%rbp
	movzbl	(%rbx), %eax
	incq	%rbx
	xorl	%r15d, %r15d
	cmpq	$255, %rax
	je	.LBB5_129
.LBB5_137:                              # %.loopexit57.i332
                                        #   in Loop: Header=BB5_121 Depth=3
	shlq	$8, %r14
	orq	%rax, %r14
	addl	$8, %r12d
	cmpl	$25, %r12d
	jl	.LBB5_121
	jmp	.LBB5_140
	.p2align	4, 0x90
.LBB5_129:                              # %.preheader.i324
                                        #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_119 Depth=2
                                        #       Parent Loop BB5_121 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rbp, %rbp
	jne	.LBB5_132
# BB#130:                               #   in Loop: Header=BB5_129 Depth=4
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#131:                               #   in Loop: Header=BB5_129 Depth=4
	movq	32(%rbp), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rbp
.LBB5_132:                              #   in Loop: Header=BB5_129 Depth=4
	decq	%rbp
	movzbl	(%rbx), %r15d
	incq	%rbx
	cmpl	$255, %r15d
	je	.LBB5_129
# BB#133:                               #   in Loop: Header=BB5_121 Depth=3
	testb	%r15b, %r15b
	jne	.LBB5_138
# BB#134:                               # %.loopexit57.i332.loopexit
                                        #   in Loop: Header=BB5_121 Depth=3
	xorl	%r15d, %r15d
	movl	$255, %eax
	jmp	.LBB5_137
.LBB5_138:                              #   in Loop: Header=BB5_121 Depth=3
	movl	%r15d, 32(%rsp)
	testl	%r12d, %r12d
	js	.LBB5_123
.LBB5_139:                              #   in Loop: Header=BB5_119 Depth=2
	movq	%rbx, 16(%rsp)
	movq	%rbp, 24(%rsp)
	movq	%r14, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movl	$1, %r8d
	cmpl	$8, %r12d
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	jl	.LBB5_144
	jmp	.LBB5_141
.LBB5_140:                              # %.thread532
                                        #   in Loop: Header=BB5_119 Depth=2
	movq	%rbx, 16(%rsp)
	movq	%rbp, 24(%rsp)
	movq	%r14, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
.LBB5_141:                              #   in Loop: Header=BB5_119 Depth=2
	leal	-8(%r12), %ecx
	movq	%r14, %rax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarq	%cl, %rax
	movzbl	%al, %eax
	movq	112(%rsp), %rdx         # 8-byte Reload
	movl	360(%rdx,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB5_143
# BB#142:                               #   in Loop: Header=BB5_119 Depth=2
	subl	%ecx, %r12d
	movzbl	1384(%rdx,%rax), %r13d
	jmp	.LBB5_146
	.p2align	4, 0x90
.LBB5_143:                              #   in Loop: Header=BB5_119 Depth=2
	movl	$9, %r8d
.LBB5_144:                              #   in Loop: Header=BB5_119 Depth=2
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	movl	%r12d, %edx
	movq	112(%rsp), %rcx         # 8-byte Reload
	callq	jpeg_huff_decode
	movl	%eax, %r13d
	testl	%r13d, %r13d
	js	.LBB5_189
# BB#145:                               #   in Loop: Header=BB5_119 Depth=2
	movq	40(%rsp), %r14
	movl	48(%rsp), %r12d
.LBB5_146:                              #   in Loop: Header=BB5_119 Depth=2
	movl	%r13d, %eax
	sarl	$4, %eax
	andl	$15, %r13d
	je	.LBB5_169
# BB#147:                               #   in Loop: Header=BB5_119 Depth=2
	movq	%rax, 104(%rsp)         # 8-byte Spill
	cmpl	%r13d, %r12d
	jge	.LBB5_168
# BB#148:                               #   in Loop: Header=BB5_119 Depth=2
	movq	16(%rsp), %rbp
	movq	24(%rsp), %rbx
	cmpl	$24, %r12d
	jg	.LBB5_167
# BB#149:                               # %.lr.ph.i340.preheader
                                        #   in Loop: Header=BB5_119 Depth=2
	movl	32(%rsp), %r15d
	movl	%r13d, 72(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB5_150:                              # %.lr.ph.i340
                                        #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_119 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB5_160 Depth 4
	testl	%r15d, %r15d
	je	.LBB5_155
# BB#151:                               #   in Loop: Header=BB5_150 Depth=3
	cmpl	%r13d, %r12d
	jge	.LBB5_167
.LBB5_152:                              #   in Loop: Header=BB5_150 Depth=3
	movq	64(%rsp), %r13
	cmpl	$0, (%r13)
	jne	.LBB5_154
# BB#153:                               #   in Loop: Header=BB5_150 Depth=3
	movq	56(%rsp), %rdi
	movq	(%rdi), %rax
	movl	$113, 40(%rax)
	movl	$-1, %esi
	callq	*8(%rax)
	movl	$1, (%r13)
.LBB5_154:                              #   in Loop: Header=BB5_150 Depth=3
	xorl	%eax, %eax
	movl	72(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB5_159
	.p2align	4, 0x90
.LBB5_155:                              #   in Loop: Header=BB5_150 Depth=3
	testq	%rbx, %rbx
	jne	.LBB5_158
# BB#156:                               #   in Loop: Header=BB5_150 Depth=3
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#157:                               #   in Loop: Header=BB5_150 Depth=3
	movq	32(%rbp), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rbx
.LBB5_158:                              #   in Loop: Header=BB5_150 Depth=3
	decq	%rbx
	movzbl	(%rbp), %eax
	incq	%rbp
	xorl	%r15d, %r15d
	cmpq	$255, %rax
	je	.LBB5_160
.LBB5_159:                              # %.loopexit57.i357
                                        #   in Loop: Header=BB5_150 Depth=3
	shlq	$8, %r14
	orq	%rax, %r14
	addl	$8, %r12d
	cmpl	$25, %r12d
	jl	.LBB5_150
	jmp	.LBB5_167
	.p2align	4, 0x90
.LBB5_160:                              # %.preheader.i349
                                        #   Parent Loop BB5_9 Depth=1
                                        #     Parent Loop BB5_119 Depth=2
                                        #       Parent Loop BB5_150 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testq	%rbx, %rbx
	jne	.LBB5_163
# BB#161:                               #   in Loop: Header=BB5_160 Depth=4
	movq	56(%rsp), %rbp
	movq	32(%rbp), %rax
	movq	%rbp, %rdi
	callq	*24(%rax)
	testl	%eax, %eax
	je	.LBB5_189
# BB#162:                               #   in Loop: Header=BB5_160 Depth=4
	movq	32(%rbp), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rbx
.LBB5_163:                              #   in Loop: Header=BB5_160 Depth=4
	decq	%rbx
	movzbl	(%rbp), %r15d
	incq	%rbp
	cmpl	$255, %r15d
	je	.LBB5_160
# BB#164:                               #   in Loop: Header=BB5_150 Depth=3
	testb	%r15b, %r15b
	jne	.LBB5_166
# BB#165:                               # %.loopexit57.i357.loopexit
                                        #   in Loop: Header=BB5_150 Depth=3
	xorl	%r15d, %r15d
	movl	$255, %eax
	jmp	.LBB5_159
.LBB5_166:                              #   in Loop: Header=BB5_150 Depth=3
	movl	%r15d, 32(%rsp)
	cmpl	%r13d, %r12d
	jl	.LBB5_152
.LBB5_167:                              # %.loopexit
                                        #   in Loop: Header=BB5_119 Depth=2
	movq	%rbp, 16(%rsp)
	movq	%rbx, 24(%rsp)
	movq	%r14, 40(%rsp)
	movl	%r12d, 48(%rsp)
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
.LBB5_168:                              #   in Loop: Header=BB5_119 Depth=2
	subl	%r13d, %r12d
	movq	104(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB5_170
	.p2align	4, 0x90
.LBB5_169:                              #   in Loop: Header=BB5_119 Depth=2
	cmpl	$15, %eax
	movl	$15, %ecx
	jne	.LBB5_171
.LBB5_170:                              #   in Loop: Header=BB5_119 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	1(%rax,%rcx), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpl	$64, %eax
	jl	.LBB5_119
.LBB5_171:                              # %.loopexit384
                                        #   in Loop: Header=BB5_9 Depth=1
	incq	%r15
	movslq	464(%rbp), %rax
	cmpq	%rax, %r15
	jl	.LBB5_9
# BB#172:                               # %._crit_edge.loopexit
	movq	32(%rbp), %rax
	movaps	16(%rsp), %xmm0
	movl	32(%rsp), %ecx
.LBB5_173:                              # %._crit_edge
	leaq	32(%rbx), %rdx
	movups	%xmm0, (%rax)
	movl	%ecx, 524(%rbp)
	movq	%r14, 16(%rbx)
	movl	%r12d, 24(%rbx)
	movaps	128(%rsp), %xmm0
	movups	%xmm0, (%rdx)
	decl	48(%rbx)
	movl	$1, %eax
	jmp	.LBB5_190
.LBB5_189:
	xorl	%eax, %eax
.LBB5_190:                              # %process_restart.exit.thread
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	decode_mcu, .Lfunc_end5-decode_mcu
	.cfi_endproc

	.type	extend_test,@object     # @extend_test
	.section	.rodata,"a",@progbits
	.p2align	4
extend_test:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	32                      # 0x20
	.long	64                      # 0x40
	.long	128                     # 0x80
	.long	256                     # 0x100
	.long	512                     # 0x200
	.long	1024                    # 0x400
	.long	2048                    # 0x800
	.long	4096                    # 0x1000
	.long	8192                    # 0x2000
	.long	16384                   # 0x4000
	.size	extend_test, 64

	.type	extend_offset,@object   # @extend_offset
	.p2align	4
extend_offset:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	4294967293              # 0xfffffffd
	.long	4294967289              # 0xfffffff9
	.long	4294967281              # 0xfffffff1
	.long	4294967265              # 0xffffffe1
	.long	4294967233              # 0xffffffc1
	.long	4294967169              # 0xffffff81
	.long	4294967041              # 0xffffff01
	.long	4294966785              # 0xfffffe01
	.long	4294966273              # 0xfffffc01
	.long	4294965249              # 0xfffff801
	.long	4294963201              # 0xfffff001
	.long	4294959105              # 0xffffe001
	.long	4294950913              # 0xffffc001
	.long	4294934529              # 0xffff8001
	.size	extend_offset, 64


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
