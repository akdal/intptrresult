	.text
	.file	"slice.bc"
	.globl	init_ref_pic_list_reordering
	.p2align	4, 0x90
	.type	init_ref_pic_list_reordering,@function
init_ref_pic_list_reordering:           # @init_ref_pic_list_reordering
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movq	14216(%rax), %rax
	movl	$0, 48(%rax)
	movl	$0, 80(%rax)
	retq
.Lfunc_end0:
	.size	init_ref_pic_list_reordering, .Lfunc_end0-init_ref_pic_list_reordering
	.cfi_endproc

	.globl	start_slice
	.p2align	4, 0x90
	.type	start_slice,@function
start_slice:                            # @start_slice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movq	14208(%rax), %rcx
	movq	14216(%rax), %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	xorl	%edx, %edx
	cmpl	$0, 4(%rcx)
	movq	input(%rip), %rcx
	sete	%dl
	cmpl	$0, 4016(%rcx)
	movl	14248(%rax), %edi
	leaq	1(%rdx,%rdx), %rax
	movl	$1, %r12d
	cmovneq	%rax, %r12
	callq	RTPUpdateTimestamp
	movl	$8, %r13d
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	24(%rax), %rax
	movq	-8(%rax,%r13), %rbp
	movl	$0, 40(%rbp)
	cmpq	$8, %r13
	jne	.LBB1_3
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	xorl	%edi, %edi
	xorl	%eax, %eax
	callq	SliceHeader
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	Partition_BC_Header
.LBB1_4:                                #   in Loop: Header=BB1_1 Depth=1
	addl	%eax, %r14d
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movq	24(%rax), %r15
	addq	%r13, %r15
	movl	4(%rbp), %eax
	cmpl	$8, %eax
	movl	$0, %ecx
	cmovel	%ecx, %eax
	addl	%eax, %r14d
	movq	%rbp, %rdi
	callq	writeVlcByteAlign
	movq	32(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rbp, %rdx
	callq	arienco_start_encoding
	callq	cabac_new_slice
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_1 Depth=1
	callq	CAVLC_init
.LBB1_7:                                #   in Loop: Header=BB1_1 Depth=1
	incq	%rbx
	addq	$104, %r13
	cmpq	%r12, %rbx
	jl	.LBB1_1
# BB#8:
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB1_10
# BB#9:
	callq	init_contexts
.LBB1_10:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_slice, .Lfunc_end1-start_slice
	.cfi_endproc

	.globl	terminate_slice
	.p2align	4, 0x90
	.type	terminate_slice,@function
terminate_slice:                        # @terminate_slice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	movq	img(%rip), %rax
	movq	14216(%rax), %r14
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB2_2
# BB#1:
	movl	$1, %edi
	callq	write_terminating_bit
.LBB2_2:                                # %.preheader
	cmpl	$0, 16(%r14)
	jle	.LBB2_17
# BB#3:                                 # %.lr.ph
	testl	%ebx, %ebx
	je	.LBB2_7
# BB#4:                                 # %.lr.ph.split.preheader
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rbx
	movq	%r14, %r13
	movq	(%rbx,%rbp), %r14
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB2_6
# BB#12:                                #   in Loop: Header=BB2_5 Depth=1
	leaq	8(%rbx,%rbp), %rdi
	callq	arienco_done_encoding
	movl	20(%rbx,%rbp), %eax
	movl	%eax, 4(%r14)
	movb	$0, 8(%r14)
	movl	(%r14), %r15d
	movq	img(%rip), %rax
	addl	%r15d, 15600(%rax)
	movl	16(%r13), %ecx
	decl	%ecx
	cmpq	%rcx, %r12
	jne	.LBB2_15
# BB#13:                                #   in Loop: Header=BB2_5 Depth=1
	movl	15444(%rax), %ecx
	shll	$8, %ecx
	movq	active_sps(%rip), %rdx
	movl	32(%rdx), %edx
	movl	terminate_slice.MbWidthC(,%rdx,4), %esi
	imull	terminate_slice.MbHeightC(,%rdx,4), %esi
	imull	15448(%rax), %esi
	leal	(%rcx,%rsi,2), %ebx
	callq	get_pic_bin_count
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	(%rax,%rax,2), %eax
	shll	$5, %eax
	movq	img(%rip), %rcx
	imull	15348(%rcx), %ebx
	imull	$-3, %ebx, %edx
	leal	(%rax,%rdx), %esi
	leal	1023(%rax,%rdx), %eax
	sarl	$31, %eax
	shrl	$22, %eax
	leal	1023(%rax,%rsi), %r13d
	sarl	$10, %r13d
	subl	15600(%rcx), %r13d
	jle	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_5 Depth=1
	movslq	%r13d, %rax
	imulq	$1431655766, %rax, %rsi # imm = 0x55555556
	movq	%rsi, %rax
	shrq	$63, %rax
	shrq	$32, %rsi
	addl	%eax, %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movl	%r13d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB2_15:                               #   in Loop: Header=BB2_5 Depth=1
	movq	32(%r14), %rdi
	movl	(%r14), %edx
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	xorl	%esi, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	RBSPtoEBSP
	movl	%eax, (%r14)
	subl	%r15d, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_5 Depth=1
	movq	%r14, %rdi
	callq	SODBtoRBSP
	movl	(%r14), %r15d
	movq	32(%r14), %rdi
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	callq	RBSPtoEBSP
	movl	%eax, (%r14)
	subl	%r15d, %eax
	movq	%r13, %r14
.LBB2_16:                               #   in Loop: Header=BB2_5 Depth=1
	shll	$3, %eax
	movq	stats(%rip), %rcx
	movq	2248(%rcx), %rcx
	addl	%eax, (%rcx)
	incq	%r12
	movslq	16(%r14), %rax
	addq	$104, %rbp
	cmpq	%rax, %r12
	jl	.LBB2_5
	jmp	.LBB2_17
.LBB2_7:                                # %.lr.ph.split.us.preheader
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rbp
	movq	(%rbp,%r13), %rbx
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_8 Depth=1
	leaq	8(%rbp,%r13), %rdi
	callq	arienco_done_encoding
	movl	20(%rbp,%r13), %eax
	movl	%eax, 4(%rbx)
	movb	$0, 8(%rbx)
	movl	(%rbx), %r15d
	movq	img(%rip), %rax
	addl	%r15d, 15600(%rax)
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	%r15d, %edx
	movl	%r15d, %ecx
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_10:                               #   in Loop: Header=BB2_8 Depth=1
	movq	%rbx, %rdi
	callq	SODBtoRBSP
	movl	(%rbx), %r15d
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movl	%r15d, %edx
.LBB2_11:                               #   in Loop: Header=BB2_8 Depth=1
	callq	RBSPtoEBSP
	movl	%eax, (%rbx)
	subl	%r15d, %eax
	shll	$3, %eax
	movq	stats(%rip), %rcx
	movq	2248(%rcx), %rcx
	addl	%eax, (%rcx)
	incq	%r12
	movslq	16(%r14), %rax
	addq	$104, %r13
	cmpq	%rax, %r12
	jl	.LBB2_8
.LBB2_17:                               # %._crit_edge
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB2_19
# BB#18:
	callq	store_contexts
.LBB2_19:
	movq	%r14, %rdi
	callq	free_ref_pic_list_reordering_buffer
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	terminate_slice, .Lfunc_end2-terminate_slice
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	9218868437227405311     # double 1.7976931348623157E+308
	.text
	.globl	encode_one_slice
	.p2align	4, 0x90
	.type	encode_one_slice,@function
encode_one_slice:                       # @encode_one_slice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 96
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	$0, 4(%rsp)
	movq	img(%rip), %rax
	movl	$0, 144(%rax)
	callq	FmoGetFirstMacroblockInSlice
	movl	%eax, %ebx
	movq	img(%rip), %rax
	movq	14208(%rax), %r12
	movl	%ebx, 12(%rax)
	movl	(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%r12)
	cmpl	$99, %eax
	jl	.LBB3_2
# BB#1:
	movl	$.L.str.2, %edi
	movl	$-1, %esi
	callq	error
.LBB3_2:
	movq	input(%rip), %rbp
	movl	264(%rbp), %eax
	cmpl	$1, %eax
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	je	.LBB3_5
# BB#3:
	cmpl	$2, %eax
	jne	.LBB3_6
# BB#4:
	movl	268(%rbp), %ebx
	addl	%ebx, %ebx
	jmp	.LBB3_8
.LBB3_5:
	movq	img(%rip), %rax
	movl	15444(%rax), %ecx
	movl	15448(%rax), %eax
	shll	$8, %ecx
	shll	$9, %eax
	leal	128(%rcx,%rax), %ebx
	imull	268(%rbp), %ebx
	jmp	.LBB3_7
.LBB3_6:
	movq	img(%rip), %rax
	movl	15444(%rax), %ecx
	movl	15448(%rax), %edx
	shll	$8, %ecx
	shll	$9, %edx
	leal	128(%rcx,%rdx), %ebx
	imull	15352(%rax), %ebx
.LBB3_7:
	addl	$500, %ebx              # imm = 0x1F4
.LBB3_8:
	movl	$1, %edi
	movl	$144, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB3_10
# BB#9:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
	movq	input(%rip), %rbp
.LBB3_10:
	cmpl	$1, 4008(%rbp)
	jne	.LBB3_12
# BB#11:
	callq	create_contexts_MotionInfo
	movq	%rax, 32(%r15)
	callq	create_contexts_TextureInfo
	movq	%rax, 40(%r15)
	movq	input(%rip), %rbp
.LBB3_12:
	xorl	%eax, %eax
	cmpl	$0, 4016(%rbp)
	setne	%al
	leal	1(%rax,%rax), %ecx
	movq	img(%rip), %rax
	movq	14208(%rax), %rax
	cmpl	$0, 4(%rax)
	movl	$1, %eax
	cmovel	%ecx, %eax
	movl	%eax, 16(%r15)
	movq	$assignSE2partition_NoDP, assignSE2partition(%rip)
	movl	%r14d, 36(%rsp)         # 4-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	je	.LBB3_14
# BB#13:
	movl	$assignSE2partition_NoDP, %ecx
	jmp	.LBB3_15
.LBB3_14:
	cmpl	$1, 4016(%rbp)
	movl	$assignSE2partition_DP, %edx
	movl	$assignSE2partition_NoDP, %ecx
	cmoveq	%rdx, %rcx
.LBB3_15:
	movq	%rcx, assignSE2partition+8(%rip)
	movl	%eax, %r12d
	movl	$104, %esi
	movq	%r12, %rdi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, 24(%r15)
	testq	%rbp, %rbp
	jne	.LBB3_17
# BB#16:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB3_17:                               # %.lr.ph.i.i
	movslq	%ebx, %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_18:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %edi
	movl	$48, %esi
	callq	calloc
	movq	%rax, %r14
	movq	%r14, (%rbp)
	testq	%r14, %r14
	jne	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_18 Depth=1
	movl	$.L.str.5, %edi
	callq	no_mem_exit
	movq	(%rbp), %r14
.LBB3_20:                               #   in Loop: Header=BB3_18 Depth=1
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 32(%r14)
	testq	%rax, %rax
	jne	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_18 Depth=1
	movl	$.L.str.6, %edi
	callq	no_mem_exit
.LBB3_22:                               #   in Loop: Header=BB3_18 Depth=1
	incq	%r13
	addq	$104, %rbp
	cmpq	%r12, %r13
	jl	.LBB3_18
# BB#23:                                # %malloc_slice.exit.i
	movq	24(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx), %rax
	movq	%r15, (%rcx,%rax,8)
	movslq	(%rcx), %rax
	movq	(%rcx,%rax,8), %r14
	movq	img(%rip), %rax
	movq	%r14, 14216(%rax)
	movl	14248(%rax), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$24, %edx
	addl	%ecx, %edx
	andl	$-256, %edx
	subl	%edx, %ecx
	movl	%ecx, (%r14)
	movl	36(%rax), %ecx
	movl	%ecx, 4(%r14)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, 12(%r14)
	movq	$dummy_slice_too_big, 112(%r14)
	movslq	16(%r14), %rcx
	testq	%rcx, %rcx
	jle	.LBB3_30
# BB#24:                                # %.lr.ph119.i
	movq	24(%r14), %rbp
	leaq	-1(%rcx), %r8
	movq	%rcx, %rbx
	xorl	%edi, %edi
	andq	$3, %rbx
	je	.LBB3_27
# BB#25:                                # %.prol.preheader
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB3_26:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rsi
	movl	$8, 4(%rsi)
	movl	$0, (%rsi)
	movb	$0, 8(%rsi)
	incq	%rdi
	addq	$104, %rdx
	cmpq	%rdi, %rbx
	jne	.LBB3_26
.LBB3_27:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB3_30
# BB#28:                                # %.lr.ph119.i.new
	subq	%rdi, %rcx
	imulq	$104, %rdi, %rdx
	addq	%rdx, %rbp
	.p2align	4, 0x90
.LBB3_29:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movl	$8, 4(%rdx)
	movl	$0, (%rdx)
	movb	$0, 8(%rdx)
	movq	104(%rbp), %rdx
	movl	$8, 4(%rdx)
	movl	$0, (%rdx)
	movb	$0, 8(%rdx)
	movq	208(%rbp), %rdx
	movl	$8, 4(%rdx)
	movl	$0, (%rdx)
	movb	$0, 8(%rdx)
	movq	312(%rbp), %rdx
	movl	$8, 4(%rdx)
	movl	$0, (%rdx)
	movb	$0, 8(%rdx)
	addq	$416, %rbp              # imm = 0x1A0
	addq	$-4, %rcx
	jne	.LBB3_29
.LBB3_30:                               # %._crit_edge120.i
	movq	active_pps(%rip), %rcx
	movl	184(%rcx), %edx
	incl	%edx
	movl	%edx, 14456(%rax)
	movl	188(%rcx), %ecx
	incl	%ecx
	movl	%ecx, 14460(%rax)
	movq	input(%rip), %rsi
	cmpl	$0, 5084(%rsi)
	je	.LBB3_34
# BB#31:
	movl	$1, %edx
	cmpl	$0, redundant_coding(%rip)
	jne	.LBB3_33
# BB#32:
	movl	(%rax), %edi
	movl	5768(%rsi), %edx
	cmpl	%edx, %edi
	cmovlel	%edi, %edx
.LBB3_33:
	movl	%edx, 14456(%rax)
.LBB3_34:
	movl	20(%rax), %edi
	cmpl	$3, %edi
	je	.LBB3_36
# BB#35:
	testl	%edi, %edi
	jne	.LBB3_38
.LBB3_36:
	movl	36(%rsi), %ebp
	testl	%ebp, %ebp
	je	.LBB3_38
# BB#37:
	cmpl	$1, 24(%rax)
	movl	$1, %ebx
	sbbl	$-1, %ebx
	imull	%ebp, %ebx
	cmpl	%ebx, %edx
	cmovgl	%ebx, %edx
	movl	%edx, 14456(%rax)
.LBB3_38:
	cmpl	$1, %edi
	jne	.LBB3_43
# BB#39:
	movl	40(%rsi), %ebp
	testl	%ebp, %ebp
	je	.LBB3_41
# BB#40:
	cmpl	$1, 24(%rax)
	movl	$1, %ebx
	sbbl	$-1, %ebx
	imull	%ebp, %ebx
	cmpl	%ebx, %edx
	cmovlel	%edx, %ebx
	movl	%ebx, 14456(%rax)
.LBB3_41:
	movl	44(%rsi), %edx
	testl	%edx, %edx
	je	.LBB3_43
# BB#42:
	cmpl	$1, 24(%rax)
	movl	$1, %esi
	sbbl	$-1, %esi
	imull	%edx, %esi
	cmpl	%esi, %ecx
	cmovlel	%ecx, %esi
	movl	%esi, 14460(%rax)
.LBB3_43:
	movl	24(%rax), %esi
	callq	init_lists
	movl	listXsize(%rip), %ecx
	movq	img(%rip), %rax
	movl	%ecx, 14456(%rax)
	movl	listXsize+4(%rip), %ecx
	movl	%ecx, 14460(%rax)
	cmpl	$0, 15360(%rax)
	je	.LBB3_47
# BB#44:
	movq	input(%rip), %rcx
	cmpl	$0, 4004(%rcx)
	je	.LBB3_47
# BB#45:
	movl	dpb+32(%rip), %ecx
	movq	active_sps(%rip), %rdx
	cmpl	1132(%rdx), %ecx
	jne	.LBB3_47
# BB#46:
	movl	15332(%rax), %edi
	callq	poc_based_ref_management
.LBB3_47:
	movq	input(%rip), %rcx
	cmpl	$0, 1564(%rcx)
	je	.LBB3_60
# BB#48:                                # %.preheader104.i
	movslq	listXsize(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB3_54
# BB#49:                                # %.lr.ph117.i
	movq	listX(%rip), %rdi
	movq	img(%rip), %rax
	movl	15596(%rax), %ebx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_50:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rsi,8), %rbp
	cmpl	%ebx, 4(%rbp)
	jge	.LBB3_52
# BB#51:                                #   in Loop: Header=BB3_50 Depth=1
	cmpl	%ebx, 15328(%rax)
	jg	.LBB3_53
.LBB3_52:                               #   in Loop: Header=BB3_50 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB3_50
	jmp	.LBB3_54
.LBB3_53:
	testl	%esi, %esi
	movl	$1, %edx
	cmovgl	%esi, %edx
	movl	%edx, 14456(%rax)
	movl	%edx, listXsize(%rip)
.LBB3_54:                               # %.preheader102.i
	movslq	listXsize+4(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB3_60
# BB#55:                                # %.lr.ph115.i
	movq	listX+8(%rip), %rdi
	movq	img(%rip), %rax
	movl	15596(%rax), %ebx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_56:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rsi,8), %rbp
	cmpl	%ebx, 4(%rbp)
	jge	.LBB3_58
# BB#57:                                #   in Loop: Header=BB3_56 Depth=1
	cmpl	%ebx, 15328(%rax)
	jg	.LBB3_59
.LBB3_58:                               #   in Loop: Header=BB3_56 Depth=1
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB3_56
	jmp	.LBB3_60
.LBB3_59:
	testl	%esi, %esi
	movl	$1, %edx
	cmovgl	%esi, %edx
	movl	%edx, 14460(%rax)
	movl	%edx, listXsize+4(%rip)
.LBB3_60:                               # %.loopexit103.i
	movq	img(%rip), %rax
	movq	14216(%rax), %rdx
	movl	$0, 48(%rdx)
	movl	$0, 80(%rdx)
	cmpl	$0, 20(%rax)
	je	.LBB3_71
.LBB3_61:
	cmpl	$0, 24(%rax)
	jne	.LBB3_63
.LBB3_62:
	callq	init_mbaff_lists
	movq	img(%rip), %rax
.LBB3_63:
	movl	20(%rax), %eax
	cmpl	$2, %eax
	je	.LBB3_93
# BB#64:
	movq	active_pps(%rip), %rcx
	cmpl	$1, 192(%rcx)
	jne	.LBB3_88
# BB#65:
	cmpl	$3, %eax
	je	.LBB3_67
# BB#66:
	testl	%eax, %eax
	jne	.LBB3_90
.LBB3_67:
	movq	input(%rip), %rax
	cmpl	$0, 1576(%rax)
	je	.LBB3_91
# BB#68:
	cmpl	$0, 2940(%rax)
	je	.LBB3_91
# BB#69:
	movq	enc_picture(%rip), %rax
	cmpq	enc_frame_picture2(%rip), %rax
	je	.LBB3_91
# BB#70:
	movl	$1, %edi
	jmp	.LBB3_92
.LBB3_71:
	cmpl	$0, 4000(%rcx)
	je	.LBB3_61
# BB#72:
	movq	%r14, %rdi
	callq	alloc_ref_pic_list_reordering_buffer
	movq	img(%rip), %rax
	movl	20(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB3_83
# BB#73:
	cmpl	$4, %ecx
	je	.LBB3_83
# BB#74:                                # %.preheader101.i
	movl	14456(%rax), %esi
	testl	%esi, %esi
	js	.LBB3_78
# BB#75:                                # %.lr.ph113.i
	movq	56(%r14), %rcx
	movq	64(%r14), %rdx
	movq	72(%r14), %rdi
	movq	$-1, %rbp
	.p2align	4, 0x90
.LBB3_76:                               # =>This Inner Loop Header: Depth=1
	movl	$3, 4(%rcx,%rbp,4)
	movl	$0, 4(%rdx,%rbp,4)
	movl	$0, 4(%rdi,%rbp,4)
	movslq	14456(%rax), %rsi
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB3_76
# BB#77:                                # %._crit_edge.loopexit.i
	movl	20(%rax), %ecx
.LBB3_78:                               # %._crit_edge.i
	cmpl	$1, %ecx
	jne	.LBB3_83
# BB#79:                                # %.preheader.i
	cmpl	$0, 14460(%rax)
	js	.LBB3_86
# BB#80:                                # %.lr.ph.i
	movq	88(%r14), %rcx
	movq	96(%r14), %rdx
	movq	104(%r14), %rsi
	movq	$-1, %rdi
	.p2align	4, 0x90
.LBB3_81:                               # =>This Inner Loop Header: Depth=1
	movl	$3, 4(%rcx,%rdi,4)
	movl	$0, 4(%rdx,%rdi,4)
	movl	$0, 4(%rsi,%rdi,4)
	movslq	14460(%rax), %rbp
	incq	%rdi
	cmpq	%rbp, %rdi
	jl	.LBB3_81
# BB#82:                                # %.loopexit100.loopexit.i
	movl	20(%rax), %ecx
.LBB3_83:                               # %.loopexit100.i
	cmpl	$2, %ecx
	je	.LBB3_61
# BB#84:                                # %.loopexit100.i
	cmpl	$4, %ecx
	je	.LBB3_61
# BB#85:                                # %.loopexit100.i..loopexit100.thread.i_crit_edge
	movl	14456(%rax), %esi
.LBB3_86:                               # %.loopexit100.thread.i
	movq	listX(%rip), %rdi
	movq	56(%r14), %rdx
	movq	64(%r14), %rcx
	xorl	%r9d, %r9d
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	poc_ref_pic_reorder
	movq	listX(%rip), %rdi
	movq	img(%rip), %rax
	movl	14456(%rax), %edx
	decl	%edx
	movq	56(%r14), %rcx
	movq	64(%r14), %r8
	movq	72(%r14), %r9
	movl	$listXsize, %esi
	callq	reorder_ref_pic_list
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB3_61
# BB#87:
	movl	14460(%rax), %esi
	movq	listX+8(%rip), %rdi
	movq	88(%r14), %rdx
	movq	96(%r14), %rcx
	movl	$1, %r9d
	callq	poc_ref_pic_reorder
	movq	listX+8(%rip), %rdi
	movq	img(%rip), %rax
	movl	14460(%rax), %edx
	decl	%edx
	movq	88(%r14), %rcx
	movq	96(%r14), %r8
	movq	104(%r14), %r9
	movl	$listXsize+4, %esi
	callq	reorder_ref_pic_list
	movq	img(%rip), %rax
	cmpl	$0, 24(%rax)
	jne	.LBB3_63
	jmp	.LBB3_62
.LBB3_88:
	cmpl	$1, %eax
	jne	.LBB3_93
# BB#89:
	movl	196(%rcx), %eax
	testl	%eax, %eax
	je	.LBB3_93
.LBB3_90:                               # %.thread.i
	callq	estimate_weighting_factor_B_slice
	jmp	.LBB3_93
.LBB3_91:
	xorl	%edi, %edi
.LBB3_92:
	callq	estimate_weighting_factor_P_slice
.LBB3_93:
	movslq	listXsize(%rip), %rax
	testq	%rax, %rax
	jle	.LBB3_96
# BB#94:                                # %.lr.ph57.i.i
	movq	listX(%rip), %rcx
	movq	enc_picture(%rip), %rdx
	addq	$24, %rdx
	.p2align	4, 0x90
.LBB3_95:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rsi
	movl	4(%rsi), %edi
	xorl	%ebp, %ebp
	cmpl	$2, (%rsi)
	sete	%bpl
	leal	(%rbp,%rdi,2), %edi
	movslq	%edi, %rdi
	movq	%rdi, (%rdx)
	movslq	16(%rsi), %rdi
	addq	%rdi, %rdi
	movq	%rdi, 1584(%rdx)
	movslq	8(%rsi), %rdi
	addq	%rdi, %rdi
	movq	%rdi, 3168(%rdx)
	movl	12(%rsi), %esi
	leal	1(%rsi,%rsi), %esi
	movslq	%esi, %rsi
	movq	%rsi, 4752(%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	decq	%rax
	jne	.LBB3_95
.LBB3_96:                               # %.preheader49.i.i
	movslq	listXsize+4(%rip), %rax
	testq	%rax, %rax
	jle	.LBB3_99
# BB#97:                                # %.lr.ph53.i.i
	movq	listX+8(%rip), %rcx
	movl	$288, %edx              # imm = 0x120
	addq	enc_picture(%rip), %rdx
	.p2align	4, 0x90
.LBB3_98:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rsi
	movl	4(%rsi), %edi
	xorl	%ebp, %ebp
	cmpl	$2, (%rsi)
	sete	%bpl
	leal	(%rbp,%rdi,2), %edi
	movslq	%edi, %rdi
	movq	%rdi, (%rdx)
	movslq	16(%rsi), %rdi
	addq	%rdi, %rdi
	movq	%rdi, 1584(%rdx)
	movslq	8(%rsi), %rdi
	addq	%rdi, %rdi
	movq	%rdi, 3168(%rdx)
	movl	12(%rsi), %esi
	leal	1(%rsi,%rsi), %esi
	movslq	%esi, %rsi
	movq	%rsi, 4752(%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	decq	%rax
	jne	.LBB3_98
.LBB3_99:                               # %._crit_edge54.i.i
	movq	active_sps(%rip), %rcx
	movq	img(%rip), %r8
	cmpl	$0, 1148(%rcx)
	jne	.LBB3_113
# BB#100:
	cmpl	$0, 24(%r8)
	jne	.LBB3_113
# BB#101:                               # %.preheader48.i.i
	movq	enc_picture(%rip), %rcx
	movslq	listXsize+8(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB3_104
# BB#102:                               # %.lr.ph.i96.i
	movq	listX+16(%rip), %rsi
	leaq	552(%rcx), %rdi
	.p2align	4, 0x90
.LBB3_103:                              # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rbp
	movl	4(%rbp), %ebx
	xorl	%eax, %eax
	cmpl	$2, (%rbp)
	sete	%al
	leal	(%rax,%rbx,2), %eax
	cltq
	movq	%rax, (%rdi)
	movslq	16(%rbp), %rax
	addq	%rax, %rax
	movq	%rax, 1584(%rdi)
	movslq	8(%rbp), %rax
	addq	%rax, %rax
	movq	%rax, 3168(%rdi)
	movl	12(%rbp), %eax
	leal	1(%rax,%rax), %eax
	cltq
	movq	%rax, 4752(%rdi)
	addq	$8, %rdi
	addq	$8, %rsi
	decq	%rdx
	jne	.LBB3_103
.LBB3_104:                              # %._crit_edge.i.i
	movslq	listXsize+12(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB3_107
# BB#105:                               # %.lr.ph.i96.1.i
	movq	listX+24(%rip), %rsi
	leaq	816(%rcx), %rdi
	.p2align	4, 0x90
.LBB3_106:                              # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movl	4(%rax), %ebp
	xorl	%ebx, %ebx
	cmpl	$2, (%rax)
	sete	%bl
	leal	(%rbx,%rbp,2), %ebp
	movslq	%ebp, %rbp
	movq	%rbp, (%rdi)
	movslq	16(%rax), %rbp
	addq	%rbp, %rbp
	movq	%rbp, 1584(%rdi)
	movslq	8(%rax), %rbp
	addq	%rbp, %rbp
	movq	%rbp, 3168(%rdi)
	movl	12(%rax), %eax
	leal	1(%rax,%rax), %eax
	cltq
	movq	%rax, 4752(%rdi)
	addq	$8, %rdi
	addq	$8, %rsi
	decq	%rdx
	jne	.LBB3_106
.LBB3_107:                              # %._crit_edge.i.1.i
	movslq	listXsize+16(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB3_110
# BB#108:                               # %.lr.ph.i96.2.i
	movq	listX+32(%rip), %rsi
	leaq	1080(%rcx), %rdi
	.p2align	4, 0x90
.LBB3_109:                              # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movl	4(%rax), %ebp
	xorl	%ebx, %ebx
	cmpl	$2, (%rax)
	sete	%bl
	leal	(%rbx,%rbp,2), %ebp
	movslq	%ebp, %rbp
	movq	%rbp, (%rdi)
	movslq	16(%rax), %rbp
	addq	%rbp, %rbp
	movq	%rbp, 1584(%rdi)
	movslq	8(%rax), %rbp
	addq	%rbp, %rbp
	movq	%rbp, 3168(%rdi)
	movl	12(%rax), %eax
	leal	1(%rax,%rax), %eax
	cltq
	movq	%rax, 4752(%rdi)
	addq	$8, %rdi
	addq	$8, %rsi
	decq	%rdx
	jne	.LBB3_109
.LBB3_110:                              # %._crit_edge.i.2.i
	movslq	listXsize+20(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB3_113
# BB#111:                               # %.lr.ph.i96.3.i
	movq	listX+40(%rip), %rsi
	addq	$1344, %rcx             # imm = 0x540
	.p2align	4, 0x90
.LBB3_112:                              # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movl	4(%rax), %edi
	xorl	%ebp, %ebp
	cmpl	$2, (%rax)
	sete	%bpl
	leal	(%rbp,%rdi,2), %edi
	movslq	%edi, %rdi
	movq	%rdi, (%rcx)
	movslq	16(%rax), %rdi
	addq	%rdi, %rdi
	movq	%rdi, 1584(%rcx)
	movslq	8(%rax), %rdi
	addq	%rdi, %rdi
	movq	%rdi, 3168(%rcx)
	movl	12(%rax), %eax
	leal	1(%rax,%rax), %eax
	cltq
	movq	%rax, 4752(%rcx)
	addq	$8, %rcx
	addq	$8, %rsi
	decq	%rdx
	jne	.LBB3_112
.LBB3_113:                              # %set_ref_pic_num.exit.i
	movl	20(%r8), %eax
	cmpl	$1, %eax
	jne	.LBB3_115
# BB#114:
	movq	Co_located(%rip), %rdi
	movl	$listX, %esi
	callq	compute_colocated
	movq	img(%rip), %rax
	movl	20(%rax), %eax
.LBB3_115:
	cmpl	$2, %eax
	je	.LBB3_118
# BB#116:
	movq	input(%rip), %rax
	cmpl	$3, 5244(%rax)
	jne	.LBB3_118
# BB#117:
	movq	EPZSCo_located(%rip), %rdi
	movl	$listX, %esi
	callq	EPZSSliceInit
.LBB3_118:
	movq	input(%rip), %rax
	cmpl	$0, 4008(%rax)
	je	.LBB3_122
# BB#119:
	movq	$writeMB_typeInfo_CABAC, writeMB_typeInfo(%rip)
	movq	$writeIntraPredMode_CABAC, writeIntraPredMode(%rip)
	movq	$writeB8_typeInfo_CABAC, writeB8_typeInfo(%rip)
	movl	listXsize(%rip), %eax
	cmpl	$1, %eax
	je	.LBB3_124
# BB#120:
	testl	%eax, %eax
	jne	.LBB3_125
# BB#121:
	movq	$0, writeRefFrame(%rip)
	movl	$writeSE_Dummy, %eax
	jmp	.LBB3_126
.LBB3_122:
	movq	$writeSE_UVLC, writeMB_typeInfo(%rip)
	movq	$writeIntraPredMode_CAVLC, writeIntraPredMode(%rip)
	movq	$writeSE_UVLC, writeB8_typeInfo(%rip)
	movslq	listXsize(%rip), %rax
	cmpq	$2, %rax
	ja	.LBB3_151
# BB#123:                               # %switch.lookup.i
	movq	.Lswitch.table(,%rax,8), %rax
	jmp	.LBB3_152
.LBB3_124:
	movl	$writeSE_Dummy, %eax
	jmp	.LBB3_126
.LBB3_125:
	movl	$writeRefFrame_CABAC, %eax
.LBB3_126:
	movq	%rax, writeRefFrame(%rip)
	movl	listXsize+4(%rip), %eax
	testl	%eax, %eax
	je	.LBB3_129
# BB#127:
	cmpl	$1, %eax
	jne	.LBB3_130
# BB#128:
	movl	$writeSE_Dummy, %eax
	jmp	.LBB3_131
.LBB3_129:
	movq	$0, writeRefFrame+8(%rip)
	movl	$writeSE_Dummy, %eax
	jmp	.LBB3_131
.LBB3_130:
	movl	$writeRefFrame_CABAC, %eax
.LBB3_131:
	movq	%rax, writeRefFrame+8(%rip)
	movl	listXsize+8(%rip), %eax
	cmpl	$1, %eax
	je	.LBB3_134
# BB#132:
	testl	%eax, %eax
	jne	.LBB3_135
# BB#133:
	movq	$0, writeRefFrame+16(%rip)
.LBB3_134:
	movl	$writeSE_Dummy, %eax
	jmp	.LBB3_136
.LBB3_135:
	movl	$writeRefFrame_CABAC, %eax
.LBB3_136:
	movq	%rax, writeRefFrame+16(%rip)
	movl	listXsize+12(%rip), %eax
	cmpl	$1, %eax
	je	.LBB3_139
# BB#137:
	testl	%eax, %eax
	jne	.LBB3_140
# BB#138:
	movq	$0, writeRefFrame+24(%rip)
.LBB3_139:
	movl	$writeSE_Dummy, %eax
	jmp	.LBB3_141
.LBB3_140:
	movl	$writeRefFrame_CABAC, %eax
.LBB3_141:
	movq	%rax, writeRefFrame+24(%rip)
	movl	listXsize+16(%rip), %eax
	cmpl	$1, %eax
	je	.LBB3_144
# BB#142:
	testl	%eax, %eax
	jne	.LBB3_145
# BB#143:
	movq	$0, writeRefFrame+32(%rip)
.LBB3_144:
	movl	$writeSE_Dummy, %eax
	jmp	.LBB3_146
.LBB3_145:
	movl	$writeRefFrame_CABAC, %eax
.LBB3_146:
	movq	%rax, writeRefFrame+32(%rip)
	movl	listXsize+20(%rip), %eax
	cmpl	$1, %eax
	je	.LBB3_149
# BB#147:
	testl	%eax, %eax
	jne	.LBB3_150
# BB#148:
	movq	$0, writeRefFrame+40(%rip)
.LBB3_149:
	movl	$writeMB_transform_size_CABAC, %edx
	movl	$writeFieldModeInfo_CABAC, %eax
	movl	$writeCIPredMode_CABAC, %ecx
	movl	$writeDquant_CABAC, %edi
	movl	$writeCBP_CABAC, %esi
	movl	$writeMVD_CABAC, %ebp
	movl	$writeSE_Dummy, %ebx
	jmp	.LBB3_168
.LBB3_150:
	movl	$writeMB_transform_size_CABAC, %edx
	movl	$writeFieldModeInfo_CABAC, %eax
	movl	$writeCIPredMode_CABAC, %ecx
	movl	$writeDquant_CABAC, %edi
	movl	$writeCBP_CABAC, %esi
	movl	$writeMVD_CABAC, %ebp
	movl	$writeRefFrame_CABAC, %ebx
	jmp	.LBB3_168
.LBB3_151:
	movl	$writeSE_UVLC, %eax
.LBB3_152:
	movq	%rax, writeRefFrame(%rip)
	movslq	listXsize+4(%rip), %rax
	cmpq	$3, %rax
	jae	.LBB3_154
# BB#153:                               # %switch.lookup.1.i
	movq	.Lswitch.table(,%rax,8), %rax
	jmp	.LBB3_155
.LBB3_154:
	movl	$writeSE_UVLC, %eax
.LBB3_155:
	movq	%rax, writeRefFrame+8(%rip)
	movslq	listXsize+8(%rip), %rax
	cmpq	$2, %rax
	ja	.LBB3_157
# BB#156:                               # %switch.lookup.2.i
	movq	.Lswitch.table(,%rax,8), %rax
	jmp	.LBB3_158
.LBB3_157:
	movl	$writeSE_UVLC, %eax
.LBB3_158:
	movq	%rax, writeRefFrame+16(%rip)
	movslq	listXsize+12(%rip), %rax
	cmpq	$2, %rax
	ja	.LBB3_160
# BB#159:                               # %switch.lookup.3.i
	movq	.Lswitch.table(,%rax,8), %rax
	jmp	.LBB3_161
.LBB3_160:
	movl	$writeSE_UVLC, %eax
.LBB3_161:
	movq	%rax, writeRefFrame+24(%rip)
	movslq	listXsize+16(%rip), %rax
	cmpq	$2, %rax
	ja	.LBB3_163
# BB#162:                               # %switch.lookup.4.i
	movq	.Lswitch.table(,%rax,8), %rax
	jmp	.LBB3_164
.LBB3_163:
	movl	$writeSE_UVLC, %eax
.LBB3_164:
	movq	%rax, writeRefFrame+32(%rip)
	movslq	listXsize+20(%rip), %rdx
	movl	$writeSE_Flag, %eax
	cmpq	$2, %rdx
	ja	.LBB3_166
# BB#165:                               # %switch.lookup.5.i
	movl	$writeSE_UVLC, %ecx
	movl	$writeCBP_VLC, %esi
	movl	$writeSE_SVLC, %ebp
	movq	.Lswitch.table(,%rdx,8), %rbx
	movl	$writeSE_SVLC, %edi
	jmp	.LBB3_167
.LBB3_166:
	movl	$writeCBP_VLC, %esi
	movl	$writeSE_SVLC, %ebp
	movl	$writeSE_UVLC, %ebx
	movl	$writeSE_SVLC, %edi
	movl	$writeSE_UVLC, %ecx
.LBB3_167:                              # %init_slice.exit
	movl	$writeSE_Flag, %edx
.LBB3_168:                              # %init_slice.exit
	movq	%rbx, writeRefFrame+40(%rip)
	movq	%rbp, writeMVD(%rip)
	movq	%rsi, writeCBP(%rip)
	movq	%rdi, writeDquant(%rip)
	movq	%rcx, writeCIPredMode(%rip)
	movq	%rax, writeFieldModeInfo(%rip)
	movq	%rdx, writeMB_transform_size(%rip)
	movq	img(%rip), %rax
	movq	14216(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%eax, Bytes_After_Header(%rip)
	callq	SetLagrangianMultipliers
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB3_170
# BB#169:
	callq	SetCtxModelNumber
	movq	input(%rip), %rax
.LBB3_170:
	cmpl	$0, 4168(%rax)
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB3_174
# BB#171:
	cmpl	$0, 4732(%rax)
	je	.LBB3_174
# BB#172:
	movq	img(%rip), %rax
	movl	20(%rax), %eax
	testl	%eax, %eax
	sete	%cl
	cmpl	$3, %eax
	sete	%al
	orb	%cl, %al
	jmp	.LBB3_175
.LBB3_174:
	xorl	%eax, %eax
.LBB3_175:
	movzbl	%al, %eax
	movq	img(%rip), %rcx
	movw	%ax, 15592(%rcx)
	callq	start_slice
	movq	input(%rip), %rcx
	cmpl	$0, 5116(%rcx)
	je	.LBB3_178
# BB#176:
	movq	generic_RC(%rip), %rdx
	addl	%eax, 16(%rdx)
	movq	img(%rip), %rcx
	movl	15404(%rcx), %esi
	cmpl	15352(%rcx), %esi
	jae	.LBB3_179
# BB#177:
	addl	%eax, 24(%rdx)
	jmp	.LBB3_179
.LBB3_178:                              # %._crit_edge
	movq	img(%rip), %rcx
.LBB3_179:
	movq	stats(%rip), %rdx
	addl	%eax, 32(%rdx)
	cltq
	movslq	20(%rcx), %rcx
	addq	%rax, 2040(%rdx,%rcx,8)
	xorl	%ebp, %ebp
	cmpl	$0, 4(%rsp)
	jne	.LBB3_250
# BB#180:                               # %.preheader.preheader
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	leaq	4(%rsp), %r15
	leaq	20(%rsp), %r12
	movl	$rddata_top_field_mb, %r13d
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_181:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rax
	cmpl	$0, 15260(%rax)
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	je	.LBB3_186
# BB#182:                               #   in Loop: Header=BB3_181 Depth=1
	movq	input(%rip), %rcx
	movl	5656(%rcx), %ecx
	testl	%ecx, %ecx
	je	.LBB3_186
# BB#183:                               #   in Loop: Header=BB3_181 Depth=1
	movl	12(%rax), %eax
	cltd
	idivl	%ecx
	testl	%edx, %edx
	jne	.LBB3_186
# BB#184:                               #   in Loop: Header=BB3_181 Depth=1
	callq	CalculateOffsetParam
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB3_186
# BB#185:                               #   in Loop: Header=BB3_181 Depth=1
	callq	CalculateOffset8Param
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_186:                              #   in Loop: Header=BB3_181 Depth=1
	movq	img(%rip), %rsi
	cmpl	$0, 15268(%rsi)
	je	.LBB3_202
# BB#187:                               #   in Loop: Header=BB3_181 Depth=1
	movl	$0, 15408(%rsi)
	movq	input(%rip), %rcx
	movl	4708(%rcx), %eax
	movl	%eax, %edx
	andl	$-2, %edx
	cmpl	$2, %edx
	jne	.LBB3_210
# BB#188:                               #   in Loop: Header=BB3_181 Depth=1
	movl	$0, 20(%rsp)
	movq	$0, 14464(%rsi)
	movq	$0, 15408(%rsi)
	cmpl	$0, 5116(%rcx)
	je	.LBB3_195
# BB#189:                               #   in Loop: Header=BB3_181 Depth=1
	cmpl	$2, %eax
	jne	.LBB3_193
# BB#190:                               #   in Loop: Header=BB3_181 Depth=1
	movl	15388(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB3_193
# BB#191:                               #   in Loop: Header=BB3_181 Depth=1
	xorl	%edx, %edx
	divl	15404(%rsi)
	testl	%edx, %edx
	jne	.LBB3_193
# BB#192:                               #   in Loop: Header=BB3_181 Depth=1
	movq	quadratic_RC_init(%rip), %rdi
	movq	quadratic_RC(%rip), %rsi
	callq	copy_rc_jvt
	movq	input(%rip), %rcx
	cmpl	$0, 5116(%rcx)
	je	.LBB3_195
.LBB3_193:                              # %.thread
                                        #   in Loop: Header=BB3_181 Depth=1
	cmpl	$2, 4708(%rcx)
	jne	.LBB3_195
# BB#194:                               #   in Loop: Header=BB3_181 Depth=1
	movq	generic_RC_init(%rip), %rdi
	movq	generic_RC(%rip), %rsi
	callq	copy_rc_generic
.LBB3_195:                              # %.thread111
                                        #   in Loop: Header=BB3_181 Depth=1
	xorl	%esi, %esi
	movl	%ebx, %edi
	callq	start_macroblock
	movq	$rddata_top_frame_mb, rdopt(%rip)
	callq	*encode_one_macroblock(%rip)
	movq	rdopt(%rip), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movq	img(%rip), %rax
	movl	$1, 15412(%rax)
	movl	$0, 14464(%rax)
	leal	1(%rbx), %edi
	xorl	%esi, %esi
	callq	start_macroblock
	movq	$rddata_bot_frame_mb, rdopt(%rip)
	callq	*encode_one_macroblock(%rip)
	movq	input(%rip), %rcx
	cmpl	$0, 5116(%rcx)
	je	.LBB3_208
# BB#196:                               #   in Loop: Header=BB3_181 Depth=1
	cmpl	$2, 4708(%rcx)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jne	.LBB3_200
# BB#197:                               #   in Loop: Header=BB3_181 Depth=1
	movq	img(%rip), %rsi
	movl	15388(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB3_200
# BB#198:                               #   in Loop: Header=BB3_181 Depth=1
	xorl	%edx, %edx
	divl	15404(%rsi)
	testl	%edx, %edx
	jne	.LBB3_200
# BB#199:                               #   in Loop: Header=BB3_181 Depth=1
	movq	quadratic_RC_best(%rip), %rdi
	movq	quadratic_RC(%rip), %rsi
	callq	copy_rc_jvt
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	input(%rip), %rcx
	cmpl	$0, 5116(%rcx)
	je	.LBB3_209
.LBB3_200:                              # %.thread113
                                        #   in Loop: Header=BB3_181 Depth=1
	cmpl	$2, 4708(%rcx)
	jne	.LBB3_209
# BB#201:                               #   in Loop: Header=BB3_181 Depth=1
	movq	generic_RC_best(%rip), %rdi
	movq	generic_RC(%rip), %rsi
	callq	copy_rc_generic
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	input(%rip), %rcx
	jmp	.LBB3_209
	.p2align	4, 0x90
.LBB3_202:                              #   in Loop: Header=BB3_181 Depth=1
	movl	$0, 20(%rsp)
	movq	$rddata_top_frame_mb, rdopt(%rip)
	xorl	%esi, %esi
	movl	%ebx, %edi
	callq	start_macroblock
	callq	*encode_one_macroblock(%rip)
	movl	$1, %edi
	callq	write_one_macroblock
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	terminate_macroblock
	cmpl	$0, 20(%rsp)
	je	.LBB3_205
# BB#203:                               #   in Loop: Header=BB3_181 Depth=1
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	callq	FmoGetPreviousMBNr
	movq	img(%rip), %rcx
	movl	%eax, 12(%rcx)
	cmpl	$-1, %eax
	jne	.LBB3_248
# BB#204:                               #   in Loop: Header=BB3_181 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
	jmp	.LBB3_248
.LBB3_205:                              #   in Loop: Header=BB3_181 Depth=1
	movl	%ebx, %edi
	callq	FmoGetNextMBNr
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.LBB3_207
# BB#206:                               #   in Loop: Header=BB3_181 Depth=1
	movl	$1, 4(%rsp)
.LBB3_207:                              #   in Loop: Header=BB3_181 Depth=1
	incl	%ebp
	callq	proceed2nextMacroblock
	jmp	.LBB3_248
.LBB3_208:                              #   in Loop: Header=BB3_181 Depth=1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB3_209:                              # %.thread114
                                        #   in Loop: Header=BB3_181 Depth=1
	movq	rdopt(%rip), %rax
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	(%rax), %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movl	4708(%rcx), %eax
.LBB3_210:                              #   in Loop: Header=BB3_181 Depth=1
	leal	-1(%rax), %edx
	cmpl	$1, %edx
	ja	.LBB3_219
# BB#211:                               #   in Loop: Header=BB3_181 Depth=1
	movq	img(%rip), %rsi
	movl	$0, 15412(%rsi)
	movl	$1, 14464(%rsi)
	movl	$1, 14468(%rsi)
	shll	15240(%rsi)
	shll	32(%rcx)
	movl	14456(%rsi), %edx
	leal	1(%rdx,%rdx), %edx
	movl	%edx, 14456(%rsi)
	cmpl	$0, 5116(%rcx)
	je	.LBB3_218
# BB#212:                               #   in Loop: Header=BB3_181 Depth=1
	cmpl	$2, %eax
	jne	.LBB3_216
# BB#213:                               #   in Loop: Header=BB3_181 Depth=1
	movl	15388(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB3_216
# BB#214:                               #   in Loop: Header=BB3_181 Depth=1
	xorl	%edx, %edx
	divl	15404(%rsi)
	testl	%edx, %edx
	jne	.LBB3_216
# BB#215:                               #   in Loop: Header=BB3_181 Depth=1
	movq	quadratic_RC(%rip), %rdi
	movq	quadratic_RC_init(%rip), %rsi
	callq	copy_rc_jvt
	movq	input(%rip), %rcx
	cmpl	$0, 5116(%rcx)
	je	.LBB3_218
.LBB3_216:                              # %.thread116
                                        #   in Loop: Header=BB3_181 Depth=1
	cmpl	$2, 4708(%rcx)
	jne	.LBB3_218
# BB#217:                               #   in Loop: Header=BB3_181 Depth=1
	movq	generic_RC(%rip), %rdi
	movq	generic_RC_init(%rip), %rsi
	callq	copy_rc_generic
.LBB3_218:                              # %.thread117
                                        #   in Loop: Header=BB3_181 Depth=1
	movl	$1, %esi
	movl	%ebx, %edi
	callq	start_macroblock
	movq	$rddata_top_field_mb, rdopt(%rip)
	callq	*encode_one_macroblock(%rip)
	movq	rdopt(%rip), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	img(%rip), %rax
	movl	$1, 15412(%rax)
	movl	$0, 14468(%rax)
	movq	%rbx, %r14
	leal	1(%rbx), %edi
	movl	$1, %esi
	callq	start_macroblock
	movq	$rddata_bot_field_mb, rdopt(%rip)
	callq	*encode_one_macroblock(%rip)
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	rdopt(%rip), %rax
	addsd	(%rax), %xmm0
	movq	input(%rip), %rcx
	movl	4708(%rcx), %eax
	jmp	.LBB3_220
	.p2align	4, 0x90
.LBB3_219:                              #   in Loop: Header=BB3_181 Depth=1
	movq	%rbx, %r14
.LBB3_220:                              #   in Loop: Header=BB3_181 Depth=1
	movq	img(%rip), %rdi
	movl	$0, 15416(%rdi)
	cmpl	$2, %eax
	sete	%dl
	ucomisd	24(%rsp), %xmm0         # 8-byte Folded Reload
	seta	%bl
	cmpl	$3, %eax
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	je	.LBB3_223
# BB#221:                               #   in Loop: Header=BB3_181 Depth=1
	andb	%dl, %bl
	jne	.LBB3_223
# BB#222:                               #   in Loop: Header=BB3_181 Depth=1
	movl	$1, 14464(%rdi)
	movl	$1, MBPairIsField(%rip)
	movl	$1, %esi
	movl	$1, %eax
	movq	%r14, %rbx
	jmp	.LBB3_233
	.p2align	4, 0x90
.LBB3_223:                              #   in Loop: Header=BB3_181 Depth=1
	cmpl	$3, %eax
	movl	$0, 14464(%rdi)
	movl	$0, MBPairIsField(%rip)
	je	.LBB3_225
# BB#224:                               #   in Loop: Header=BB3_181 Depth=1
	sarl	15240(%rdi)
	sarl	32(%rcx)
	movl	14456(%rdi), %edx
	decl	%edx
	sarl	%edx
	movl	%edx, 14456(%rdi)
.LBB3_225:                              #   in Loop: Header=BB3_181 Depth=1
	cmpl	$0, 5116(%rcx)
	movq	%r14, %rbx
	je	.LBB3_232
# BB#226:                               #   in Loop: Header=BB3_181 Depth=1
	cmpl	$2, %eax
	jne	.LBB3_230
# BB#227:                               #   in Loop: Header=BB3_181 Depth=1
	movl	15388(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB3_230
# BB#228:                               #   in Loop: Header=BB3_181 Depth=1
	xorl	%edx, %edx
	divl	15404(%rdi)
	testl	%edx, %edx
	jne	.LBB3_230
# BB#229:                               #   in Loop: Header=BB3_181 Depth=1
	movq	quadratic_RC(%rip), %rdi
	movq	quadratic_RC_best(%rip), %rsi
	callq	copy_rc_jvt
	movq	input(%rip), %rcx
	cmpl	$0, 5116(%rcx)
	je	.LBB3_232
.LBB3_230:                              # %.thread119
                                        #   in Loop: Header=BB3_181 Depth=1
	cmpl	$2, 4708(%rcx)
	jne	.LBB3_232
# BB#231:                               #   in Loop: Header=BB3_181 Depth=1
	movq	generic_RC(%rip), %rdi
	movq	generic_RC_best(%rip), %rsi
	callq	copy_rc_generic
.LBB3_232:                              # %.thread120
                                        #   in Loop: Header=BB3_181 Depth=1
	movq	img(%rip), %rdi
	movl	$1, 15416(%rdi)
	movl	MBPairIsField(%rip), %eax
	movl	14464(%rdi), %esi
.LBB3_233:                              #   in Loop: Header=BB3_181 Depth=1
	movl	$1, 15408(%rdi)
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	movl	%ecx, 14468(%rdi)
	movl	$0, 15412(%rdi)
	movl	%ebx, %edi
	callq	start_macroblock
	movq	img(%rip), %rax
	cmpl	$0, 14464(%rax)
	movl	$rddata_top_frame_mb, %eax
	cmovneq	%r13, %rax
	movq	%rax, rdopt(%rip)
	xorl	%edi, %edi
	callq	copy_rdopt_data
	movl	$1, %edi
	callq	write_one_macroblock
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	terminate_macroblock
	cmpl	$0, 20(%rsp)
	je	.LBB3_235
# BB#234:                               #   in Loop: Header=BB3_181 Depth=1
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	jmp	.LBB3_239
	.p2align	4, 0x90
.LBB3_235:                              #   in Loop: Header=BB3_181 Depth=1
	movl	%ebx, %edi
	callq	FmoGetNextMBNr
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.LBB3_237
# BB#236:                               #   in Loop: Header=BB3_181 Depth=1
	movl	$1, 4(%rsp)
.LBB3_237:                              #   in Loop: Header=BB3_181 Depth=1
	callq	proceed2nextMacroblock
	movq	img(%rip), %rax
	movl	$1, 15412(%rax)
	movl	$0, 14468(%rax)
	movl	14464(%rax), %esi
	movl	%ebx, %edi
	callq	start_macroblock
	movq	img(%rip), %rax
	cmpl	$0, 14464(%rax)
	movl	$rddata_bot_frame_mb, %eax
	movl	$rddata_bot_field_mb, %ecx
	cmovneq	%rcx, %rax
	movq	%rax, rdopt(%rip)
	movl	$1, %edi
	callq	copy_rdopt_data
	xorl	%edi, %edi
	callq	write_one_macroblock
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	terminate_macroblock
	cmpl	$0, 20(%rsp)
	je	.LBB3_241
# BB#238:                               #   in Loop: Header=BB3_181 Depth=1
	incl	%ebp
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	callq	FmoGetPreviousMBNr
	movq	img(%rip), %rcx
	movl	%eax, 12(%rcx)
	movl	%eax, %edi
.LBB3_239:                              #   in Loop: Header=BB3_181 Depth=1
	callq	FmoGetPreviousMBNr
	movq	img(%rip), %rcx
	movl	%eax, 12(%rcx)
	cmpl	$-1, %eax
	jne	.LBB3_244
# BB#240:                               #   in Loop: Header=BB3_181 Depth=1
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	callq	snprintf
	movl	$errortext, %edi
	movl	$300, %esi              # imm = 0x12C
	callq	error
	jmp	.LBB3_244
.LBB3_241:                              #   in Loop: Header=BB3_181 Depth=1
	movl	%ebx, %edi
	callq	FmoGetNextMBNr
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.LBB3_243
# BB#242:                               #   in Loop: Header=BB3_181 Depth=1
	movl	$1, 4(%rsp)
.LBB3_243:                              #   in Loop: Header=BB3_181 Depth=1
	addl	$2, %ebp
	callq	proceed2nextMacroblock
	.p2align	4, 0x90
.LBB3_244:                              #   in Loop: Header=BB3_181 Depth=1
	cmpl	$0, MBPairIsField(%rip)
	movq	img(%rip), %rax
	je	.LBB3_246
# BB#245:                               #   in Loop: Header=BB3_181 Depth=1
	sarl	15240(%rax)
	movq	input(%rip), %rcx
	sarl	32(%rcx)
	movl	14456(%rax), %ecx
	decl	%ecx
	sarl	%ecx
	movl	%ecx, 14456(%rax)
.LBB3_246:                              # %._crit_edge108
                                        #   in Loop: Header=BB3_181 Depth=1
	movq	$0, 14464(%rax)
	cmpl	$0, 4(%rsp)
	jne	.LBB3_250
# BB#247:                               #   in Loop: Header=BB3_181 Depth=1
	movl	%ebx, %edi
	callq	FmoMB2SliceGroup
	movl	%eax, %edi
	callq	FmoGetLastCodedMBOfSliceGroup
	cmpl	%eax, %ebx
	je	.LBB3_249
.LBB3_248:                              #   in Loop: Header=BB3_181 Depth=1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cmpl	$0, 4(%rsp)
	je	.LBB3_181
	jmp	.LBB3_250
.LBB3_249:
	movl	$1, 4(%rsp)
.LBB3_250:                              # %thread-pre-split._crit_edge
	movl	36(%rsp), %ecx          # 4-byte Reload
	addl	%ebp, %ecx
	movq	img(%rip), %rax
	xorl	%edi, %edi
	cmpl	15348(%rax), %ecx
	setge	%dil
	callq	terminate_slice
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	encode_one_slice, .Lfunc_end3-encode_one_slice
	.cfi_endproc

	.globl	free_slice_list
	.p2align	4, 0x90
	.type	free_slice_list,@function
free_slice_list:                        # @free_slice_list
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi45:
	.cfi_def_cfa_offset 64
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, (%r14)
	jle	.LBB4_17
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
	movq	8(%r14,%r12,8), %r15
	testq	%r15, %r15
	je	.LBB4_16
# BB#3:                                 # %.preheader.i
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	24(%r15), %r13
	cmpl	$0, 16(%r15)
	jle	.LBB4_11
# BB#4:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.i
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rax
	addq	%rbx, %rax
	je	.LBB4_10
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=2
	movq	(%r13,%rbx), %rdi
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.LBB4_8
# BB#7:                                 #   in Loop: Header=BB4_5 Depth=2
	movq	%rax, %rdi
	callq	free
	movq	(%r13,%rbx), %rdi
.LBB4_8:                                #   in Loop: Header=BB4_5 Depth=2
	testq	%rdi, %rdi
	je	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_5 Depth=2
	callq	free
.LBB4_10:                               #   in Loop: Header=BB4_5 Depth=2
	incq	%rbp
	movslq	16(%r15), %rax
	movq	24(%r15), %r13
	addq	$104, %rbx
	cmpq	%rax, %rbp
	jl	.LBB4_5
.LBB4_11:                               # %._crit_edge.i
                                        #   in Loop: Header=BB4_2 Depth=1
	testq	%r13, %r13
	je	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_2 Depth=1
	movq	%r13, %rdi
	callq	free
.LBB4_13:                               #   in Loop: Header=BB4_2 Depth=1
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB4_15
# BB#14:                                #   in Loop: Header=BB4_2 Depth=1
	movq	32(%r15), %rdi
	callq	delete_contexts_MotionInfo
	movq	40(%r15), %rdi
	callq	delete_contexts_TextureInfo
.LBB4_15:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB4_16:                               # %free_slice.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	$0, 8(%r14,%r12,8)
	incq	%r12
	movslq	(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB4_2
.LBB4_17:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	free_slice_list, .Lfunc_end4-free_slice_list
	.cfi_endproc

	.globl	poc_ref_pic_reorder
	.p2align	4, 0x90
	.type	poc_ref_pic_reorder,@function
poc_ref_pic_reorder:                    # @poc_ref_pic_reorder
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$744, %rsp              # imm = 0x2E8
.Lcfi58:
	.cfi_def_cfa_offset 800
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	log2_max_frame_num_minus4(%rip), %ecx
	addl	$4, %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movq	img(%rip), %rax
	cmpl	$0, 24(%rax)
	movl	15332(%rax), %r9d
	movq	%rax, 48(%rsp)          # 8-byte Spill
	je	.LBB5_1
# BB#2:
	addl	%ebp, %ebp
	movl	%ebp, (%rsp)            # 4-byte Spill
	leal	1(%r9,%r9), %r9d
	testl	%esi, %esi
	jne	.LBB5_4
	jmp	.LBB5_9
.LBB5_1:
	movl	%ebp, (%rsp)            # 4-byte Spill
	testl	%esi, %esi
	je	.LBB5_9
.LBB5_4:                                # %.lr.ph189.preheader
	movl	%esi, %eax
	leaq	-1(%rax), %r8
	movq	%rax, %rcx
	xorl	%ebx, %ebx
	andq	$3, %rcx
	je	.LBB5_6
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph189.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rbp
	movl	6364(%rbp), %ebp
	movl	%ebp, 352(%rsp,%rbx,4)
	incq	%rbx
	cmpq	%rbx, %rcx
	jne	.LBB5_5
.LBB5_6:                                # %.lr.ph189.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB5_9
# BB#7:                                 # %.lr.ph189.preheader.new
	subq	%rbx, %rax
	leaq	364(%rsp,%rbx,4), %rcx
	leaq	24(%rdi,%rbx,8), %rdi
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph189
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rdi), %rbp
	movl	6364(%rbp), %ebp
	movl	%ebp, -12(%rcx)
	movq	-16(%rdi), %rbp
	movl	6364(%rbp), %ebp
	movl	%ebp, -8(%rcx)
	movq	-8(%rdi), %rbp
	movl	6364(%rbp), %ebp
	movl	%ebp, -4(%rcx)
	movq	(%rdi), %rbp
	movl	6364(%rbp), %ebp
	movl	%ebp, (%rcx)
	addq	$16, %rcx
	addq	$32, %rdi
	addq	$-4, %rax
	jne	.LBB5_8
.LBB5_9:                                # %.preheader156
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	dpb+32(%rip), %esi
	testq	%rsi, %rsi
	je	.LBB5_24
# BB#10:                                # %.lr.ph186
	movq	dpb+8(%rip), %rcx
	movq	enc_picture(%rip), %rdx
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	je	.LBB5_17
# BB#11:                                # %.lr.ph186.split.preheader
	xorl	%eax, %eax
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph186.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rax,8), %rbp
	movq	40(%rbp), %rdi
	movl	6364(%rdi), %ebx
	movl	%ebx, 96(%rsp,%rax,4)
	cmpl	$3, (%rbp)
	jne	.LBB5_16
# BB#13:                                #   in Loop: Header=BB5_12 Depth=1
	cmpl	$0, 6380(%rdi)
	je	.LBB5_16
# BB#14:                                #   in Loop: Header=BB5_12 Depth=1
	cmpl	$0, 6376(%rdi)
	jne	.LBB5_16
# BB#15:                                #   in Loop: Header=BB5_12 Depth=1
	movl	4(%rdi), %ebx
	subl	4(%rdx), %ebx
	movl	$-1, %ebp
	cmovll	%r8d, %ebp
	movl	%ebx, %edi
	negl	%edi
	cmovll	%ebx, %edi
	movl	%edi, 608(%rsp,%rax,4)
	movl	%ebp, 480(%rsp,%rax,4)
	.p2align	4, 0x90
.LBB5_16:                               #   in Loop: Header=BB5_12 Depth=1
	incq	%rax
	cmpq	%rsi, %rax
	jb	.LBB5_12
	jmp	.LBB5_23
.LBB5_17:                               # %.lr.ph186.split.us.preheader
	xorl	%eax, %eax
	movl	$1, %r8d
	.p2align	4, 0x90
.LBB5_18:                               # %.lr.ph186.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rax,8), %rbp
	movq	40(%rbp), %rdi
	movl	6364(%rdi), %ebx
	movl	%ebx, 96(%rsp,%rax,4)
	cmpl	$3, (%rbp)
	jne	.LBB5_22
# BB#19:                                #   in Loop: Header=BB5_18 Depth=1
	cmpl	$0, 6380(%rdi)
	je	.LBB5_22
# BB#20:                                #   in Loop: Header=BB5_18 Depth=1
	cmpl	$0, 6376(%rdi)
	jne	.LBB5_22
# BB#21:                                #   in Loop: Header=BB5_18 Depth=1
	movl	4(%rdi), %ebx
	subl	4(%rdx), %ebx
	movl	$-1, %ebp
	cmovgl	%r8d, %ebp
	movl	%ebx, %edi
	negl	%edi
	cmovll	%ebx, %edi
	movl	%edi, 608(%rsp,%rax,4)
	movl	%ebp, 480(%rsp,%rax,4)
	.p2align	4, 0x90
.LBB5_22:                               #   in Loop: Header=BB5_18 Depth=1
	incq	%rax
	cmpq	%rsi, %rax
	jb	.LBB5_18
.LBB5_23:                               # %.preheader155
	cmpl	$1, %esi
	je	.LBB5_27
.LBB5_24:                               # %.lr.ph184
	leal	-1(%rsi), %eax
	leaq	484(%rsp), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	612(%rsp), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	100(%rsp), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	$1, %r13d
	xorl	%ecx, %ecx
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	%eax, %r14d
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_25:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_37 Depth 2
	leaq	1(%rcx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmpq	%rsi, %rax
	jae	.LBB5_26
# BB#36:                                # %.lr.ph182
                                        #   in Loop: Header=BB5_25 Depth=1
	movl	%r14d, %ebx
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB5_37:                               #   Parent Loop BB5_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	608(%rsp,%rcx,4), %r15d
	movl	(%rsi), %r8d
	cmpl	%r8d, %r15d
	jle	.LBB5_39
# BB#38:                                # %._crit_edge218
                                        #   in Loop: Header=BB5_37 Depth=2
	movl	480(%rsp,%rcx,4), %r9d
	movl	(%r12), %r10d
	movq	%r12, %r11
	jmp	.LBB5_42
	.p2align	4, 0x90
.LBB5_39:                               #   in Loop: Header=BB5_37 Depth=2
	jne	.LBB5_43
# BB#40:                                #   in Loop: Header=BB5_37 Depth=2
	movl	(%r12), %r10d
	movl	480(%rsp,%rcx,4), %r9d
	cmpl	%r9d, %r10d
	jle	.LBB5_43
# BB#41:                                #   in Loop: Header=BB5_37 Depth=2
	leaq	480(%rsp,%rbp,4), %r11
	.p2align	4, 0x90
.LBB5_42:                               #   in Loop: Header=BB5_37 Depth=2
	movl	%r8d, 608(%rsp,%rcx,4)
	movl	%r15d, (%rsi)
	movl	96(%rsp,%rcx,4), %edx
	movl	(%rdi), %eax
	movl	%eax, 96(%rsp,%rcx,4)
	movl	%edx, (%rdi)
	movl	%r10d, 480(%rsp,%rcx,4)
	movl	%r9d, (%r11)
.LBB5_43:                               #   in Loop: Header=BB5_37 Depth=2
	incq	%rbp
	addq	$4, %r12
	addq	$4, %rsi
	addq	$4, %rdi
	decl	%ebx
	jne	.LBB5_37
.LBB5_26:                               # %.loopexit154
                                        #   in Loop: Header=BB5_25 Depth=1
	incq	%r13
	addq	$4, 16(%rsp)            # 8-byte Folded Spill
	addq	$4, 8(%rsp)             # 8-byte Folded Spill
	addq	$4, 56(%rsp)            # 8-byte Folded Spill
	decl	%r14d
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpq	80(%rsp), %rcx          # 8-byte Folded Reload
	movq	88(%rsp), %rsi          # 8-byte Reload
	jb	.LBB5_25
.LBB5_27:                               # %.preheader153
	movq	72(%rsp), %r15          # 8-byte Reload
	testl	%r15d, %r15d
	movq	64(%rsp), %r12          # 8-byte Reload
	je	.LBB5_71
# BB#28:                                # %.lr.ph177.preheader
	movl	%r15d, %r10d
	leaq	-1(%r10), %rcx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %eax
	xorl	%esi, %esi
	andq	$3, %rdi
	je	.LBB5_30
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph177.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	352(%rsp,%rsi,4), %ebp
	cmpl	96(%rsp,%rsi,4), %ebp
	cmovnel	%edx, %eax
	incq	%rsi
	cmpq	%rsi, %rdi
	jne	.LBB5_29
.LBB5_30:                               # %.lr.ph177.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB5_33
# BB#31:                                # %.lr.ph177.preheader.new
	movq	%r10, %rcx
	subq	%rsi, %rcx
	leaq	108(%rsp,%rsi,4), %rdx
	leaq	364(%rsp,%rsi,4), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_32:                               # %.lr.ph177
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rsi), %ebp
	movl	-8(%rsi), %ebx
	cmpl	-12(%rdx), %ebp
	cmovnel	%edi, %eax
	cmpl	-8(%rdx), %ebx
	movl	-4(%rsi), %ebp
	cmovnel	%edi, %eax
	cmpl	-4(%rdx), %ebp
	movl	(%rsi), %ebp
	cmovnel	%edi, %eax
	cmpl	(%rdx), %ebp
	cmovnel	%edi, %eax
	addq	$16, %rdx
	addq	$16, %rsi
	addq	$-4, %rcx
	jne	.LBB5_32
.LBB5_33:                               # %._crit_edge178
	testl	%eax, %eax
	jne	.LBB5_71
# BB#34:                                # %.preheader151
	testl	%r15d, %r15d
	je	.LBB5_35
# BB#44:                                # %.lr.ph173.split.us.preheader
	leal	-1(%r15), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	4(,%rax,4), %r8
	leaq	-1(%r10), %rbx
	xorl	%ebp, %ebp
	leaq	352(%rsp), %r13
	leaq	224(%rsp), %r11
	movq	40(%rsp), %r14          # 8-byte Reload
	jmp	.LBB5_45
	.p2align	4, 0x90
.LBB5_54:                               #   in Loop: Header=BB5_55 Depth=2
	incl	%eax
	cmpl	%r15d, %eax
	jb	.LBB5_55
	jmp	.LBB5_68
.LBB5_48:                               #   in Loop: Header=BB5_45 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp,4), %rdx
	movl	(%rsp), %ecx            # 4-byte Reload
	jmp	.LBB5_49
.LBB5_56:                               # %.lr.ph170.us.preheader
                                        #   in Loop: Header=BB5_45 Depth=1
	movq	%r13, %rdi
	movq	%r11, %rsi
	movq	%r8, %rdx
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	callq	memcpy
	leaq	224(%rsp), %r11
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
.LBB5_45:                               # %.lr.ph173.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_57 Depth 2
                                        #     Child Loop BB5_55 Depth 2
	movl	%r12d, %edx
	movq	%r10, %rax
	subq	%rbp, %rax
	movl	96(%rsp,%rbp,4), %r12d
	movl	%r12d, %ecx
	subl	%edx, %ecx
	jle	.LBB5_47
# BB#46:                                #   in Loop: Header=BB5_45 Depth=1
	movl	$1, (%r14,%rbp,4)
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbp,4), %rdx
.LBB5_49:                               # %.sink.split.us
                                        #   in Loop: Header=BB5_45 Depth=1
	decl	%ecx
	movl	%ecx, (%rdx)
	jmp	.LBB5_50
.LBB5_47:                               #   in Loop: Header=BB5_45 Depth=1
	movl	$0, (%r14,%rbp,4)
	movl	%ecx, %edx
	negl	%edx
	testl	%ecx, %ecx
	cmovel	%ecx, %edx
	leal	-1(%rdx), %ecx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%ecx, (%rsi,%rbp,4)
	testl	%edx, %edx
	jle	.LBB5_48
.LBB5_50:                               # %.lr.ph162.us
                                        #   in Loop: Header=BB5_45 Depth=1
	movl	%r12d, 224(%rsp,%rbp,4)
	testb	$1, %al
	jne	.LBB5_62
# BB#51:                                #   in Loop: Header=BB5_45 Depth=1
	movq	%rbp, %rdx
	movl	%ebp, %eax
	cmpq	%rbp, %rbx
	jne	.LBB5_67
	jmp	.LBB5_52
.LBB5_62:                               #   in Loop: Header=BB5_45 Depth=1
	movl	352(%rsp,%rbp,4), %ecx
	cmpl	%r12d, %ecx
	jne	.LBB5_64
# BB#63:                                #   in Loop: Header=BB5_45 Depth=1
	movl	%ebp, %eax
	jmp	.LBB5_65
.LBB5_64:                               #   in Loop: Header=BB5_45 Depth=1
	leal	1(%rbp), %eax
	movl	%ecx, 224(%rsp,%rax,4)
.LBB5_65:                               #   in Loop: Header=BB5_45 Depth=1
	leaq	1(%rbp), %rdx
	cmpq	%rbp, %rbx
	je	.LBB5_52
.LBB5_67:                               # %.lr.ph162.us.new
                                        #   in Loop: Header=BB5_45 Depth=1
	movq	%r10, %rcx
	subq	%rdx, %rcx
	leaq	356(%rsp), %rsi
	leaq	(%rsi,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB5_57:                               #   Parent Loop BB5_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx), %esi
	cmpl	%r12d, %esi
	je	.LBB5_59
# BB#58:                                #   in Loop: Header=BB5_57 Depth=2
	incl	%eax
	movl	%esi, 224(%rsp,%rax,4)
.LBB5_59:                               #   in Loop: Header=BB5_57 Depth=2
	movl	(%rdx), %esi
	cmpl	%r12d, %esi
	je	.LBB5_61
# BB#60:                                #   in Loop: Header=BB5_57 Depth=2
	incl	%eax
	movl	%esi, 224(%rsp,%rax,4)
.LBB5_61:                               #   in Loop: Header=BB5_57 Depth=2
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB5_57
.LBB5_52:                               # %._crit_edge163.us
                                        #   in Loop: Header=BB5_45 Depth=1
	incq	%rbp
	cmpq	%r10, %rbp
	jae	.LBB5_68
# BB#53:                                # %.lr.ph166.us.preheader
                                        #   in Loop: Header=BB5_45 Depth=1
	movl	%ebp, %eax
	.p2align	4, 0x90
.LBB5_55:                               # %.lr.ph166.us
                                        #   Parent Loop BB5_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ecx
	movl	224(%rsp,%rcx,4), %edx
	cmpl	96(%rsp,%rcx,4), %edx
	je	.LBB5_54
	jmp	.LBB5_56
.LBB5_68:                               # %.critedge
	testl	%r15d, %r15d
	movl	%ebp, %eax
	movl	$3, (%r14,%rax,4)
	movq	48(%rsp), %rbx          # 8-byte Reload
	je	.LBB5_70
# BB#69:                                # %.lr.ph.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	4(,%rax,4), %rdx
	leaq	352(%rsp), %rdi
	leaq	224(%rsp), %rsi
	callq	memcpy
	jmp	.LBB5_70
.LBB5_35:                               # %.critedge.thread
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	$3, (%rax)
	movq	48(%rsp), %rbx          # 8-byte Reload
.LBB5_70:                               # %._crit_edge
	movq	14216(%rbx), %rax
	leaq	80(%rax), %rcx
	addq	$48, %rax
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	cmovneq	%rcx, %rax
	movl	$1, (%rax)
.LBB5_71:                               # %._crit_edge178.thread
	addq	$744, %rsp              # imm = 0x2E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	poc_ref_pic_reorder, .Lfunc_end5-poc_ref_pic_reorder
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4587366580439587226     # double 0.050000000000000003
.LCPI6_1:
	.quad	4602678819172646912     # double 0.5
.LCPI6_2:
	.quad	4607182418800017408     # double 1
.LCPI6_3:
	.quad	-4600427019358961664    # double -12
.LCPI6_4:
	.quad	4606732058837280358     # double 0.94999999999999996
.LCPI6_6:
	.quad	4679240012837945344     # double 65536
.LCPI6_7:
	.quad	4613937818241073152     # double 3
.LCPI6_8:
	.quad	4605831338911806259     # double 0.84999999999999998
.LCPI6_9:
	.quad	4616189618054758400     # double 4
.LCPI6_10:
	.quad	4622945017495814144     # double 12
.LCPI6_11:
	.quad	4608983858650965606     # double 1.3999999999999999
.LCPI6_12:
	.quad	4604300115038500291     # double 0.68000000000000005
.LCPI6_13:
	.quad	4618441417868443648     # double 6
.LCPI6_14:
	.quad	4611686018427387904     # double 2
.LCPI6_16:
	.quad	4605380978949069210     # double 0.80000000000000004
.LCPI6_17:
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI6_18:
	.quad	4600877379321698714     # double 0.40000000000000002
.LCPI6_19:
	.quad	4604418534313441775     # double 0.69314718055994529
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	3
.LCPI6_5:
	.quad	4606732058837280358     # double 0.94999999999999996
	.quad	4607182418800017408     # double 1
.LCPI6_15:
	.quad	4607182418800017408     # double 1
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	SetLagrangianMultipliers
	.p2align	4, 0x90
	.type	SetLagrangianMultipliers,@function
SetLagrangianMultipliers:               # @SetLagrangianMultipliers
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 96
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	input(%rip), %rax
	cmpl	$0, 4168(%rax)
	movq	img(%rip), %r14
	je	.LBB6_107
# BB#1:                                 # %.preheader228.preheader
	cvtsi2sdl	20(%rax), %xmm0
	mulsd	.LCPI6_0(%rip), %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	minsd	.LCPI6_1(%rip), %xmm0
	movsd	.LCPI6_2(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB6_2:                                # %.preheader228
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	movslq	15452(%r14), %rbx
	cmpq	$-51, %rbx
	jl	.LBB6_106
# BB#3:                                 # %.lr.ph243
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	%ebx, %eax
	negq	%rbx
	leaq	(,%rbx,8), %r15
	movl	%ebx, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jmp	.LBB6_4
	.p2align	4, 0x90
.LBB6_105:                              # %.loopexit226._crit_edge
                                        #   in Loop: Header=BB6_4 Depth=2
	movl	15452(%r14), %eax
	incq	%rbp
.LBB6_4:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	input(%rip), %r13
	movl	5288(%r13), %ecx
	cmpl	$2, %ecx
	je	.LBB6_27
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=2
	movq	32(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rbp), %edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm1, %xmm0
	addsd	.LCPI6_3(%rip), %xmm0
	cmpl	$1, %ecx
	jne	.LBB6_58
# BB#6:                                 #   in Loop: Header=BB6_4 Depth=2
	divsd	.LCPI6_7(%rip), %xmm0
	callq	exp2
	movapd	%xmm0, %xmm4
	movsd	5296(%r13,%r12,8), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm4, %xmm1
	movq	15480(%r14), %rax
	movq	(%rax,%r12,8), %rdx
	leaq	(%rdx,%r15), %rax
	movsd	%xmm1, (%rax,%rbp,8)
	cmpl	$2, 5784(%r13)
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	jne	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_4 Depth=2
	xorl	%ecx, %ecx
	cmpl	$2, 5788(%r13)
	sete	%cl
	movsd	.LCPI6_5(,%rcx,8), %xmm0 # xmm0 = mem[0],zero
.LBB6_8:                                # %.thread
                                        #   in Loop: Header=BB6_4 Depth=2
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rax,%rbp,8)
	cmpl	$1, 5780(%r13)
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	je	.LBB6_12
# BB#9:                                 #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_11
# BB#10:                                # %call.sqrt
                                        #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm4, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm4          # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_11:                               # %.split
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	input(%rip), %r13
	movq	15480(%r14), %rax
	movq	(%rax,%r12,8), %rdx
	movapd	%xmm1, %xmm0
.LBB6_12:                               #   in Loop: Header=BB6_4 Depth=2
	movq	15488(%r14), %rax
	movq	(%rax,%r12,8), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rsi
	movsd	%xmm0, (%rsi)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %ebx
	movq	15496(%r14), %rcx
	movq	(%rcx,%r12,8), %rdi
	addq	%r15, %rdi
	movq	(%rdi,%rbp,8), %rdi
	movl	%ebx, (%rdi)
	leaq	(%rdx,%r15), %rbx
	cmpl	$1, 5784(%r13)
	movsd	(%rbx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_16
# BB#13:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_15
# BB#14:                                # %call.sqrt762
                                        #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm4, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm4          # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_15:                               # %.split761
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15480(%r14), %rdx
	movq	15488(%r14), %rax
	movq	(%rax,%r12,8), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rsi
	movq	15496(%r14), %rcx
	movq	(%rcx,%r12,8), %rdi
	addq	%r15, %rdi
	movq	(%rdi,%rbp,8), %rdi
	movq	input(%rip), %r13
	movq	(%rdx,%r12,8), %rdx
	movapd	%xmm1, %xmm0
.LBB6_16:                               #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm0, 8(%rsi)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %ebx
	movl	%ebx, 4(%rdi)
	addq	%r15, %rdx
	cmpl	$1, 5788(%r13)
	movsd	(%rdx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_20
# BB#17:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_19
# BB#18:                                # %call.sqrt764
                                        #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm4, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm4          # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_19:                               # %.split763
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15488(%r14), %rax
	movq	15496(%r14), %rcx
	movq	(%rax,%r12,8), %rdx
	addq	%r15, %rdx
	movq	(%rdx,%rbp,8), %rsi
	movq	(%rcx,%r12,8), %rdx
	addq	%r15, %rdx
	movq	(%rdx,%rbp,8), %rdi
	movapd	%xmm1, %xmm0
.LBB6_20:                               #   in Loop: Header=BB6_4 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	cmpq	$1, %r12
	movsd	%xmm0, 16(%rsi)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %edx
	movl	%edx, 8(%rdi)
	jne	.LBB6_104
# BB#21:                                #   in Loop: Header=BB6_4 Depth=2
	movq	input(%rip), %rdx
	mulsd	5336(%rdx), %xmm4
	movq	15480(%r14), %rsi
	movq	40(%rsi), %rsi
	leaq	(%rsi,%r15), %rdi
	movsd	%xmm4, (%rdi,%rbp,8)
	cmpl	$2, 5784(%rdx)
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	jne	.LBB6_23
# BB#22:                                #   in Loop: Header=BB6_4 Depth=2
	xorl	%ebx, %ebx
	cmpl	$2, 5788(%rdx)
	sete	%bl
	movsd	.LCPI6_5(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB6_23:                               # %.thread214
                                        #   in Loop: Header=BB6_4 Depth=2
	mulsd	%xmm4, %xmm0
	movsd	%xmm0, (%rdi,%rbp,8)
	cmpl	$1, 5780(%rdx)
	je	.LBB6_49
# BB#24:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_26
# BB#25:                                # %call.sqrt744
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_26:                               # %.split743
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15480(%r14), %rsi
	movq	15488(%r14), %rax
	movq	15496(%r14), %rcx
	movq	input(%rip), %rdx
	movq	40(%rsi), %rsi
	movapd	%xmm1, %xmm0
	jmp	.LBB6_49
	.p2align	4, 0x90
.LBB6_27:                               #   in Loop: Header=BB6_4 Depth=2
	movq	5344(%r13,%r12,8), %rcx
	movq	15480(%r14), %rax
	movq	(%rax,%r12,8), %rdx
	leaq	(%rdx,%r15), %rax
	movq	%rcx, (%rax,%rbp,8)
	movd	%rcx, %xmm1
	cmpl	$2, 5784(%r13)
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	jne	.LBB6_29
# BB#28:                                #   in Loop: Header=BB6_4 Depth=2
	xorl	%ecx, %ecx
	cmpl	$2, 5788(%r13)
	sete	%cl
	movsd	.LCPI6_5(,%rcx,8), %xmm0 # xmm0 = mem[0],zero
.LBB6_29:                               # %.thread215
                                        #   in Loop: Header=BB6_4 Depth=2
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rax,%rbp,8)
	cmpl	$1, 5780(%r13)
	je	.LBB6_33
# BB#30:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_32
# BB#31:                                # %call.sqrt746
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_32:                               # %.split745
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	input(%rip), %r13
	movq	15480(%r14), %rax
	movq	(%rax,%r12,8), %rdx
	movapd	%xmm1, %xmm0
.LBB6_33:                               #   in Loop: Header=BB6_4 Depth=2
	movq	15488(%r14), %rax
	movq	(%rax,%r12,8), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rsi
	movsd	%xmm0, (%rsi)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %ebx
	movq	15496(%r14), %rcx
	movq	(%rcx,%r12,8), %rdi
	addq	%r15, %rdi
	movq	(%rdi,%rbp,8), %rdi
	movl	%ebx, (%rdi)
	leaq	(%rdx,%r15), %rbx
	cmpl	$1, 5784(%r13)
	movsd	(%rbx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_37
# BB#34:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_36
# BB#35:                                # %call.sqrt754
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_36:                               # %.split753
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15480(%r14), %rdx
	movq	15488(%r14), %rax
	movq	(%rax,%r12,8), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rsi
	movq	15496(%r14), %rcx
	movq	(%rcx,%r12,8), %rdi
	addq	%r15, %rdi
	movq	(%rdi,%rbp,8), %rdi
	movq	input(%rip), %r13
	movq	(%rdx,%r12,8), %rdx
	movapd	%xmm1, %xmm0
.LBB6_37:                               #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm0, 8(%rsi)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %ebx
	movl	%ebx, 4(%rdi)
	addq	%r15, %rdx
	cmpl	$1, 5788(%r13)
	movsd	(%rdx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_41
# BB#38:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_40
# BB#39:                                # %call.sqrt756
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_40:                               # %.split755
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15488(%r14), %rax
	movq	15496(%r14), %rcx
	movq	(%rax,%r12,8), %rdx
	addq	%r15, %rdx
	movq	(%rdx,%rbp,8), %rsi
	movq	(%rcx,%r12,8), %rdx
	addq	%r15, %rdx
	movq	(%rdx,%rbp,8), %rdi
	movapd	%xmm1, %xmm0
.LBB6_41:                               #   in Loop: Header=BB6_4 Depth=2
	cmpq	$1, %r12
	movsd	%xmm0, 16(%rsi)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %edx
	movl	%edx, 8(%rdi)
	jne	.LBB6_123
# BB#42:                                #   in Loop: Header=BB6_4 Depth=2
	movq	input(%rip), %rdx
	movq	5384(%rdx), %rbx
	movq	15480(%r14), %rsi
	movq	40(%rsi), %rsi
	leaq	(%rsi,%r15), %rdi
	movq	%rbx, (%rdi,%rbp,8)
	movd	%rbx, %xmm1
	cmpl	$2, 5784(%rdx)
	movsd	.LCPI6_4(%rip), %xmm0   # xmm0 = mem[0],zero
	jne	.LBB6_44
# BB#43:                                #   in Loop: Header=BB6_4 Depth=2
	xorl	%ebx, %ebx
	cmpl	$2, 5788(%rdx)
	sete	%bl
	movsd	.LCPI6_5(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
.LBB6_44:                               # %.thread216
                                        #   in Loop: Header=BB6_4 Depth=2
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdi,%rbp,8)
	cmpl	$1, 5780(%rdx)
	je	.LBB6_48
# BB#45:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_47
# BB#46:                                # %call.sqrt748
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_47:                               # %.split747
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15480(%r14), %rsi
	movq	15488(%r14), %rax
	movq	15496(%r14), %rcx
	movq	input(%rip), %rdx
	movq	40(%rsi), %rsi
	movapd	%xmm1, %xmm0
.LBB6_48:                               #   in Loop: Header=BB6_4 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB6_49:                               #   in Loop: Header=BB6_4 Depth=2
	movq	40(%rax), %rax
	addq	%r15, %rax
	movq	(%rax,%rbp,8), %rax
	movsd	%xmm0, (%rax)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %edi
	movq	40(%rcx), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	%edi, (%rcx)
	leaq	(%rsi,%r15), %rdi
	cmpl	$1, 5784(%rdx)
	movsd	(%rdi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_53
# BB#50:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_52
# BB#51:                                # %call.sqrt758
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_52:                               # %.split757
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15480(%r14), %rsi
	movq	15488(%r14), %rax
	movq	40(%rax), %rax
	addq	%r15, %rax
	movq	(%rax,%rbp,8), %rax
	movq	15496(%r14), %rcx
	movq	40(%rcx), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	input(%rip), %rdx
	movq	40(%rsi), %rsi
	movapd	%xmm1, %xmm0
.LBB6_53:                               #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm0, 8(%rax)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %edi
	movl	%edi, 4(%rcx)
	addq	%r15, %rsi
	cmpl	$1, 5788(%rdx)
	movsd	(%rsi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_57
# BB#54:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_56
# BB#55:                                # %call.sqrt760
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_56:                               # %.split759
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15488(%r14), %rax
	movq	15496(%r14), %rcx
	movq	40(%rax), %rax
	addq	%r15, %rax
	movq	(%rax,%rbp,8), %rax
	movq	40(%rcx), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rcx
	movapd	%xmm1, %xmm0
.LBB6_57:                               # %.loopexit226.loopexit247257
                                        #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm0, 16(%rax)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 8(%rcx)
	jmp	.LBB6_104
	.p2align	4, 0x90
.LBB6_58:                               #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	divsd	.LCPI6_7(%rip), %xmm0
	callq	exp2
	cmpl	$0, 2096(%r13)
	jle	.LBB6_63
# BB#59:                                #   in Loop: Header=BB6_4 Depth=2
	mulsd	.LCPI6_12(%rip), %xmm0
	cmpl	$3, %r12d
	je	.LBB6_62
# BB#60:                                #   in Loop: Header=BB6_4 Depth=2
	cmpl	$1, %r12d
	movsd	.LCPI6_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	jne	.LBB6_67
# BB#61:                                #   in Loop: Header=BB6_4 Depth=2
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	.LCPI6_13(%rip), %xmm1
	maxsd	.LCPI6_14(%rip), %xmm1
	minsd	.LCPI6_9(%rip), %xmm1
	jmp	.LBB6_67
.LBB6_123:                              #   in Loop: Header=BB6_4 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB6_104
.LBB6_63:                               #   in Loop: Header=BB6_4 Depth=2
	mulsd	.LCPI6_8(%rip), %xmm0
	cmpl	$1, %r12d
	movsd	.LCPI6_9(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	je	.LBB6_67
# BB#64:                                #   in Loop: Header=BB6_4 Depth=2
	cmpl	$3, %r12d
	jne	.LBB6_66
# BB#65:                                #   in Loop: Header=BB6_4 Depth=2
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	.LCPI6_10(%rip), %xmm1
	maxsd	.LCPI6_11(%rip), %xmm1
	minsd	.LCPI6_7(%rip), %xmm1
	jmp	.LBB6_67
.LBB6_62:                               #   in Loop: Header=BB6_4 Depth=2
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	.LCPI6_10(%rip), %xmm1
	maxsd	.LCPI6_11(%rip), %xmm1
	minsd	.LCPI6_7(%rip), %xmm1
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	jmp	.LBB6_67
.LBB6_66:                               # %.fold.split
                                        #   in Loop: Header=BB6_4 Depth=2
	movsd	.LCPI6_2(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB6_67:                               #   in Loop: Header=BB6_4 Depth=2
	mulsd	%xmm1, %xmm0
	movq	15480(%r14), %rax
	movq	(%rax,%r12,8), %rcx
	leaq	(%rcx,%r15), %rdx
	movsd	%xmm0, (%rdx,%rbp,8)
	cmpl	$2, 5784(%r13)
	movsd	.LCPI6_4(%rip), %xmm1   # xmm1 = mem[0],zero
	jne	.LBB6_69
# BB#68:                                #   in Loop: Header=BB6_4 Depth=2
	xorl	%esi, %esi
	cmpl	$2, 5788(%r13)
	sete	%sil
	movsd	.LCPI6_5(,%rsi,8), %xmm1 # xmm1 = mem[0],zero
.LBB6_69:                               # %.thread217
                                        #   in Loop: Header=BB6_4 Depth=2
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx,%rbp,8)
	cmpq	$1, %r12
	jne	.LBB6_87
# BB#70:                                #   in Loop: Header=BB6_4 Depth=2
	cmpl	$2, 2964(%r13)
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	jne	.LBB6_72
# BB#71:                                #   in Loop: Header=BB6_4 Depth=2
	xorl	%ecx, %ecx
	cmpl	$0, 14364(%r14)
	sete	%cl
	movsd	.LCPI6_15(,%rcx,8), %xmm2 # xmm2 = mem[0],zero
.LBB6_72:                               #   in Loop: Header=BB6_4 Depth=2
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rdx,%rbp,8)
	movq	40(%rax), %rcx
	leaq	(%rcx,%r15), %rdx
	movsd	%xmm2, (%rdx,%rbp,8)
	cmpl	$2, 2968(%r13)
	movsd	.LCPI6_16(%rip), %xmm0  # xmm0 = mem[0],zero
	jne	.LBB6_74
# BB#73:                                #   in Loop: Header=BB6_4 Depth=2
	movq	gop_structure(%rip), %rsi
	movslq	14364(%r14), %rdi
	leaq	(%rdi,%rdi,2), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	-8(%rsi,%rdi,8), %xmm0
	mulsd	.LCPI6_17(%rip), %xmm0
	movsd	.LCPI6_18(%rip), %xmm1  # xmm1 = mem[0],zero
	minsd	%xmm0, %xmm1
	movsd	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
.LBB6_74:                               #   in Loop: Header=BB6_4 Depth=2
	mulsd	%xmm2, %xmm0
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, (%rdx,%rbp,8)
	cmpl	$1, 5780(%r13)
	je	.LBB6_78
# BB#75:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_77
# BB#76:                                # %call.sqrt750
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_77:                               # %.split749
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	input(%rip), %r13
	movq	15480(%r14), %rax
	movq	40(%rax), %rcx
	movapd	%xmm1, %xmm0
.LBB6_78:                               #   in Loop: Header=BB6_4 Depth=2
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movq	15488(%r14), %rdx
	movq	40(%rdx), %rdx
	addq	%r15, %rdx
	movq	(%rdx,%rbp,8), %rdx
	movsd	%xmm0, (%rdx)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %edi
	movq	15496(%r14), %rsi
	movq	40(%rsi), %rsi
	addq	%r15, %rsi
	movq	(%rsi,%rbp,8), %rsi
	movl	%edi, (%rsi)
	leaq	(%rcx,%r15), %rdi
	cmpl	$1, 5784(%r13)
	movsd	(%rdi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_82
# BB#79:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_81
# BB#80:                                # %call.sqrt770
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_81:                               # %.split769
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15480(%r14), %rax
	movq	15488(%r14), %rcx
	movq	40(%rcx), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rdx
	movq	15496(%r14), %rcx
	movq	40(%rcx), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rsi
	movq	input(%rip), %r13
	movq	40(%rax), %rcx
	movapd	%xmm1, %xmm0
.LBB6_82:                               #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm0, 8(%rdx)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %edi
	movl	%edi, 4(%rsi)
	addq	%r15, %rcx
	cmpl	$1, 5788(%r13)
	movsd	(%rcx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_86
# BB#83:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_85
# BB#84:                                # %call.sqrt772
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_85:                               # %.split771
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15480(%r14), %rax
	movq	15488(%r14), %rcx
	movq	40(%rcx), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rdx
	movq	15496(%r14), %rcx
	movq	40(%rcx), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rsi
	movq	input(%rip), %r13
	movapd	%xmm1, %xmm0
.LBB6_86:                               # %.preheader225.loopexit268
                                        #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm0, 16(%rdx)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %ecx
	movl	%ecx, 8(%rsi)
	movq	8(%rax), %rcx
	leaq	(%rcx,%r15), %rax
	movsd	(%rax,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	cmpl	$1, 5780(%r13)
	jne	.LBB6_89
	jmp	.LBB6_92
.LBB6_87:                               #   in Loop: Header=BB6_4 Depth=2
	mulsd	24(%rsp), %xmm1         # 8-byte Folded Reload
	movsd	%xmm1, (%rdx,%rbp,8)
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	cmpl	$1, 5780(%r13)
	je	.LBB6_92
.LBB6_89:                               #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB6_91
# BB#90:                                # %call.sqrt752
                                        #   in Loop: Header=BB6_4 Depth=2
	movapd	%xmm1, %xmm0
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
.LBB6_91:                               # %.split751
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	input(%rip), %r13
	movq	15480(%r14), %rax
	movq	(%rax,%r12,8), %rcx
	movapd	%xmm0, %xmm1
.LBB6_92:                               #   in Loop: Header=BB6_4 Depth=2
	movq	15488(%r14), %rax
	movq	(%rax,%r12,8), %rax
	leaq	(%rax,%r15), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movsd	%xmm1, (%rdx)
	mulsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm1
	cvttsd2si	%xmm1, %edi
	movq	15496(%r14), %rsi
	movq	(%rsi,%r12,8), %rsi
	addq	%r15, %rsi
	movq	(%rsi,%rbp,8), %rsi
	movl	%edi, (%rsi)
	leaq	(%rcx,%r15), %rdi
	cmpl	$1, 5784(%r13)
	movsd	(%rdi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_96
# BB#93:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_95
# BB#94:                                # %call.sqrt774
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_95:                               # %.split773
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15480(%r14), %rcx
	movq	15488(%r14), %rax
	movq	(%rax,%r12,8), %rax
	leaq	(%rax,%r15), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movq	15496(%r14), %rsi
	movq	(%rsi,%r12,8), %rsi
	addq	%r15, %rsi
	movq	(%rsi,%rbp,8), %rsi
	movq	input(%rip), %r13
	movq	(%rcx,%r12,8), %rcx
	movapd	%xmm1, %xmm0
.LBB6_96:                               #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm0, 8(%rdx)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %edi
	movl	%edi, 4(%rsi)
	addq	%r15, %rcx
	cmpl	$1, 5788(%r13)
	movsd	(%rcx,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	je	.LBB6_100
# BB#97:                                #   in Loop: Header=BB6_4 Depth=2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_99
# BB#98:                                # %call.sqrt776
                                        #   in Loop: Header=BB6_4 Depth=2
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_99:                               # %.split775
                                        #   in Loop: Header=BB6_4 Depth=2
	movq	img(%rip), %r14
	movq	15488(%r14), %rax
	movq	15496(%r14), %rcx
	movq	(%rax,%r12,8), %rax
	leaq	(%rax,%r15), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movq	(%rcx,%r12,8), %rcx
	addq	%r15, %rcx
	movq	(%rcx,%rbp,8), %rsi
	movq	input(%rip), %r13
	movapd	%xmm1, %xmm0
.LBB6_100:                              #   in Loop: Header=BB6_4 Depth=2
	movsd	%xmm0, 16(%rdx)
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	cvttsd2si	%xmm0, %ecx
	movl	%ecx, 8(%rsi)
	cmpl	$1, 4172(%r13)
	jne	.LBB6_104
# BB#101:                               #   in Loop: Header=BB6_4 Depth=2
	leaq	(%rbx,%rbp), %rdx
	movl	$-6, %ecx
	cmpq	$32, %rdx
	jl	.LBB6_103
# BB#102:                               #   in Loop: Header=BB6_4 Depth=2
	xorl	%ecx, %ecx
	cmpl	$0, 5116(%r13)
	sete	%cl
	leal	-6(%rcx,%rcx), %ecx
.LBB6_103:                              #   in Loop: Header=BB6_4 Depth=2
	movq	32(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rbp), %edx
	addl	%ecx, %edx
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	movq	(%rax,%rdx,8), %rax
	movsd	16(%rax), %xmm0         # xmm0 = mem[0],zero
	addsd	.LCPI6_2(%rip), %xmm0
	callq	log
	divsd	.LCPI6_19(%rip), %xmm0
	movq	img(%rip), %r14
	movq	15504(%r14), %rax
	movq	(%rax,%r12,8), %rax
	addq	%r15, %rax
	movsd	%xmm0, (%rax,%rbp,8)
	movsd	.LCPI6_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	.p2align	4, 0x90
.LBB6_104:                              # %.loopexit226
                                        #   in Loop: Header=BB6_4 Depth=2
	leaq	1(%rbx,%rbp), %rax
	cmpq	$51, %rax
	jle	.LBB6_105
.LBB6_106:                              # %._crit_edge244
                                        #   in Loop: Header=BB6_2 Depth=1
	incq	%r12
	cmpq	$5, %r12
	jne	.LBB6_2
	jmp	.LBB6_122
.LBB6_107:                              # %.preheader.preheader
	xorl	%r15d, %r15d
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI6_6(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm4   # xmm4 = mem[0],zero
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB6_108:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_110 Depth 2
	movslq	15452(%r14), %rbx
	cmpq	$-51, %rbx
	jl	.LBB6_121
# BB#109:                               # %.lr.ph
                                        #   in Loop: Header=BB6_108 Depth=1
	negq	%rbx
	.p2align	4, 0x90
.LBB6_110:                              #   Parent Loop BB6_108 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-12(%rbx), %eax
	cmpq	$12, %rbx
	cmovleq	%r15, %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	QP2QUANT(,%rax,4), %xmm0
	movq	15480(%r14), %rax
	movq	(%rax,%r12,8), %rdi
	movsd	%xmm0, (%rdi,%rbx,8)
	movq	15488(%r14), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	input(%rip), %rcx
	movq	img(%rip), %r14
	movq	15488(%r14), %rdx
	movq	15496(%r14), %rbp
	movq	(%rdx,%r12,8), %r8
	movq	(%r8,%rbx,8), %rsi
	movq	(%rbp,%r12,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movsd	%xmm0, (%rax)
	cmpl	$1, 5780(%rcx)
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm0
	je	.LBB6_112
# BB#111:                               #   in Loop: Header=BB6_110 Depth=2
	movapd	%xmm2, %xmm0
.LBB6_112:                              #   in Loop: Header=BB6_110 Depth=2
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rsi)
	mulsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm0
	cvttsd2si	%xmm0, %ebp
	movl	%ebp, (%rdx)
	movq	(%rdi,%rbx,8), %rbp
	movq	%rbp, 8(%rax)
	cmpl	$1, 5784(%rcx)
	movsd	8(%rsi), %xmm1          # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm0
	je	.LBB6_114
# BB#113:                               #   in Loop: Header=BB6_110 Depth=2
	movapd	%xmm2, %xmm0
.LBB6_114:                              #   in Loop: Header=BB6_110 Depth=2
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsi)
	mulsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm0
	cvttsd2si	%xmm0, %ebp
	movl	%ebp, 4(%rdx)
	movq	(%rdi,%rbx,8), %rdi
	movq	%rdi, 16(%rax)
	cmpl	$1, 5788(%rcx)
	movsd	16(%rsi), %xmm1         # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm0
	je	.LBB6_116
# BB#115:                               #   in Loop: Header=BB6_110 Depth=2
	movapd	%xmm2, %xmm0
.LBB6_116:                              #   in Loop: Header=BB6_110 Depth=2
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rsi)
	mulsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 8(%rdx)
	cmpl	$1, 4172(%rcx)
	jne	.LBB6_120
# BB#117:                               #   in Loop: Header=BB6_110 Depth=2
	movl	$-6, %eax
	cmpq	$32, %rbx
	jl	.LBB6_119
# BB#118:                               #   in Loop: Header=BB6_110 Depth=2
	xorl	%eax, %eax
	cmpl	$0, 5116(%rcx)
	sete	%al
	leal	-6(%rax,%rax), %eax
.LBB6_119:                              #   in Loop: Header=BB6_110 Depth=2
	addl	%ebx, %eax
	cmovsl	%r15d, %eax
	movq	(%r8,%rax,8), %rax
	movsd	16(%rax), %xmm0         # xmm0 = mem[0],zero
	addsd	%xmm2, %xmm0
	callq	log
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	divsd	.LCPI6_19(%rip), %xmm0
	movq	img(%rip), %r14
	movq	15504(%r14), %rax
	movq	(%rax,%r12,8), %rax
	movsd	%xmm0, (%rax,%rbx,8)
	movsd	.LCPI6_6(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm4   # xmm4 = mem[0],zero
.LBB6_120:                              #   in Loop: Header=BB6_110 Depth=2
	incq	%rbx
	cmpq	$52, %rbx
	jl	.LBB6_110
.LBB6_121:                              # %._crit_edge
                                        #   in Loop: Header=BB6_108 Depth=1
	incq	%r12
	cmpq	$6, %r12
	jne	.LBB6_108
.LBB6_122:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	SetLagrangianMultipliers, .Lfunc_end6-SetLagrangianMultipliers
	.cfi_endproc

	.type	terminate_slice.MbWidthC,@object # @terminate_slice.MbWidthC
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
terminate_slice.MbWidthC:
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	16                      # 0x10
	.size	terminate_slice.MbWidthC, 16

	.type	terminate_slice.MbHeightC,@object # @terminate_slice.MbHeightC
	.p2align	4
terminate_slice.MbHeightC:
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	16                      # 0x10
	.size	terminate_slice.MbHeightC, 16

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"CABAC stuffing words = %6d\n"
	.size	.L.str, 28

	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Error encoding first MB with specified parameter, bits of current MB may be too big"
	.size	.L.str.1, 84

	.type	quadratic_RC_init,@object # @quadratic_RC_init
	.comm	quadratic_RC_init,8,8
	.type	quadratic_RC,@object    # @quadratic_RC
	.comm	quadratic_RC,8,8
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	quadratic_RC_best,@object # @quadratic_RC_best
	.comm	quadratic_RC_best,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	WriteNALU,@object       # @WriteNALU
	.comm	WriteNALU,8,8
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	updateQP,@object        # @updateQP
	.comm	updateQP,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Too many slices per picture, increase MAXSLICEPERPICTURE in global.h."
	.size	.L.str.2, 70

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"malloc_slice: slice structure"
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"malloc_slice: partArr"
	.size	.L.str.4, 22

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"malloc_slice: Bitstream"
	.size	.L.str.5, 24

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"malloc_slice: StreamBuffer"
	.size	.L.str.6, 27

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	0
	.quad	writeSE_Dummy
	.quad	writeSE_invFlag
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
