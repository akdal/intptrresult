	.text
	.file	"getmove.bc"
	.globl	getmove
	.p2align	4, 0x90
	.type	getmove,@function
getmove:                                # @getmove
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_14:                               # %tailrecurse
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	scanf
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_2
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	$.L.str.1, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_4
# BB#7:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_8
# BB#10:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$0, pass(%rip)
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	getij
	testl	%eax, %eax
	je	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_1 Depth=1
	movslq	(%r15), %rdi
	movslq	(%r14), %rsi
	imulq	$19, %rdi, %rax
	cmpb	$0, p(%rax,%rsi)
	jne	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_1 Depth=1
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	suicide
	testl	%eax, %eax
	je	.LBB0_9
.LBB0_13:                               #   in Loop: Header=BB0_1 Depth=1
	movq	stdin(%rip), %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB0_14
# BB#15:
	movl	$1, %edi
	callq	exit
.LBB0_2:                                # %tailrecurse._crit_edge
	movl	$0, play(%rip)
	jmp	.LBB0_9
.LBB0_4:
	movl	$.L.str.2, %edi
	movl	$.L.str.3, %esi
	callq	fopen
	movq	%rax, %r14
	movq	$-361, %rbx             # imm = 0xFE97
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	p+361(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+362(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+363(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+364(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+365(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+366(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+367(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+368(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+369(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+370(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+371(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+372(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+373(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+374(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+375(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+376(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+377(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+378(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	movzbl	p+379(%rbx), %edi
	movq	%r14, %rsi
	callq	fputc
	addq	$19, %rbx
	jne	.LBB0_5
# BB#6:
	movl	mymove(%rip), %edx
	movl	mk(%rip), %ecx
	movl	uk(%rip), %r8d
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	opn(%rip), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	opn+4(%rip), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	opn+8(%rip), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	opn+12(%rip), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	opn+16(%rip), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	opn+20(%rip), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	opn+24(%rip), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	opn+28(%rip), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	opn+32(%rip), %edx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movq	%r14, %rdi
	callq	fclose
	movl	$-1, play(%rip)
	jmp	.LBB0_9
.LBB0_8:
	incl	pass(%rip)
	movl	$-1, (%r15)
.LBB0_9:                                # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	getmove, .Lfunc_end0-getmove
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"stop"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"save"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"gnugo.dat"
	.size	.L.str.2, 10

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"w"
	.size	.L.str.3, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%d %d %d "
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%d "
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"pass"
	.size	.L.str.7, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"your move? "
	.size	.L.str.9, 12

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%s"
	.size	.L.str.10, 3

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"illegal move !"
	.size	.Lstr, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
