	.text
	.file	"StringToInt.bc"
	.globl	_Z21ConvertStringToUInt64PKcPS0_
	.p2align	4, 0x90
	.type	_Z21ConvertStringToUInt64PKcPS0_,@function
_Z21ConvertStringToUInt64PKcPS0_:       # @_Z21ConvertStringToUInt64PKcPS0_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %cl
	movl	%ecx, %eax
	addb	$-48, %al
	cmpb	$9, %al
	jbe	.LBB0_5
# BB#1:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.LBB0_3
	jmp	.LBB0_4
.LBB0_5:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbq	%cl, %rcx
	leaq	(%rax,%rax,4), %rax
	leaq	-48(%rcx,%rax,2), %rax
	movzbl	1(%rdi), %ecx
	incq	%rdi
	movl	%ecx, %edx
	addb	$-48, %dl
	cmpb	$9, %dl
	jbe	.LBB0_6
# BB#2:                                 # %._crit_edge
	testq	%rsi, %rsi
	je	.LBB0_4
.LBB0_3:
	movq	%rdi, (%rsi)
.LBB0_4:
	retq
.Lfunc_end0:
	.size	_Z21ConvertStringToUInt64PKcPS0_, .Lfunc_end0-_Z21ConvertStringToUInt64PKcPS0_
	.cfi_endproc

	.globl	_Z24ConvertOctStringToUInt64PKcPS0_
	.p2align	4, 0x90
	.type	_Z24ConvertOctStringToUInt64PKcPS0_,@function
_Z24ConvertOctStringToUInt64PKcPS0_:    # @_Z24ConvertOctStringToUInt64PKcPS0_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %cl
	movl	%ecx, %eax
	andb	$-8, %al
	cmpb	$48, %al
	jne	.LBB1_1
# BB#5:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbq	%cl, %rcx
	leaq	-48(%rcx,%rax,8), %rax
	movzbl	1(%rdi), %ecx
	incq	%rdi
	movl	%ecx, %edx
	andb	$-8, %dl
	cmpb	$48, %dl
	je	.LBB1_6
	jmp	.LBB1_2
.LBB1_1:
	xorl	%eax, %eax
.LBB1_2:                                # %._crit_edge
	testq	%rsi, %rsi
	je	.LBB1_4
# BB#3:
	movq	%rdi, (%rsi)
.LBB1_4:
	retq
.Lfunc_end1:
	.size	_Z24ConvertOctStringToUInt64PKcPS0_, .Lfunc_end1-_Z24ConvertOctStringToUInt64PKcPS0_
	.cfi_endproc

	.globl	_Z24ConvertHexStringToUInt64PKcPS0_
	.p2align	4, 0x90
	.type	_Z24ConvertHexStringToUInt64PKcPS0_,@function
_Z24ConvertHexStringToUInt64PKcPS0_:    # @_Z24ConvertHexStringToUInt64PKcPS0_
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	movl	$4294967209, %ecx       # imm = 0xFFFFFFA9
	leaq	39(%rcx), %r9
	leaq	32(%rcx), %r8
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_7:                                #   in Loop: Header=BB2_1 Depth=1
	addl	%edx, %ecx
	shlq	$4, %rax
	orq	%rcx, %rax
	incq	%rdi
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movsbq	(%rdi), %rdx
	movl	%edx, %ecx
	addb	$-48, %cl
	cmpb	$10, %cl
	movq	%r9, %rcx
	jb	.LBB2_7
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	%edx, %ecx
	addb	$-65, %cl
	cmpb	$6, %cl
	movq	%r8, %rcx
	jb	.LBB2_7
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	%edx, %ecx
	addb	$-97, %cl
	cmpb	$6, %cl
	movl	$4294967209, %ecx       # imm = 0xFFFFFFA9
	jb	.LBB2_7
# BB#4:
	testq	%rsi, %rsi
	je	.LBB2_6
# BB#5:
	movq	%rdi, (%rsi)
.LBB2_6:
	retq
.Lfunc_end2:
	.size	_Z24ConvertHexStringToUInt64PKcPS0_, .Lfunc_end2-_Z24ConvertHexStringToUInt64PKcPS0_
	.cfi_endproc

	.globl	_Z21ConvertStringToUInt64PKwPS0_
	.p2align	4, 0x90
	.type	_Z21ConvertStringToUInt64PKwPS0_,@function
_Z21ConvertStringToUInt64PKwPS0_:       # @_Z21ConvertStringToUInt64PKwPS0_
	.cfi_startproc
# BB#0:
	movl	(%rdi), %ecx
	addl	$-48, %ecx
	xorl	%eax, %eax
	cmpl	$9, %ecx
	ja	.LBB3_2
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rax,4), %rax
	movslq	%ecx, %rcx
	leaq	(%rcx,%rax,2), %rax
	movl	4(%rdi), %ecx
	addq	$4, %rdi
	addl	$-48, %ecx
	cmpl	$9, %ecx
	jbe	.LBB3_5
.LBB3_2:                                # %._crit_edge
	testq	%rsi, %rsi
	je	.LBB3_4
# BB#3:
	movq	%rdi, (%rsi)
.LBB3_4:
	retq
.Lfunc_end3:
	.size	_Z21ConvertStringToUInt64PKwPS0_, .Lfunc_end3-_Z21ConvertStringToUInt64PKwPS0_
	.cfi_endproc

	.globl	_Z20ConvertStringToInt64PKcPS0_
	.p2align	4, 0x90
	.type	_Z20ConvertStringToInt64PKcPS0_,@function
_Z20ConvertStringToInt64PKcPS0_:        # @_Z20ConvertStringToInt64PKcPS0_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %cl
	cmpb	$45, %cl
	jne	.LBB4_8
# BB#1:
	movb	1(%rdi), %cl
	incq	%rdi
	movl	%ecx, %eax
	addb	$-48, %al
	cmpb	$9, %al
	jbe	.LBB4_6
# BB#2:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.LBB4_4
	jmp	.LBB4_5
.LBB4_8:
	movl	%ecx, %eax
	addb	$-48, %al
	cmpb	$9, %al
	jbe	.LBB4_13
# BB#9:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.LBB4_11
	jmp	.LBB4_12
.LBB4_6:                                # %.lr.ph.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movsbq	%cl, %rcx
	leaq	(%rax,%rax,4), %rax
	leaq	-48(%rcx,%rax,2), %rax
	movzbl	1(%rdi), %ecx
	incq	%rdi
	movl	%ecx, %edx
	addb	$-48, %dl
	cmpb	$9, %dl
	jbe	.LBB4_7
# BB#3:                                 # %._crit_edge.i
	testq	%rsi, %rsi
	je	.LBB4_5
.LBB4_4:
	movq	%rdi, (%rsi)
.LBB4_5:                                # %_Z21ConvertStringToUInt64PKcPS0_.exit
	negq	%rax
	retq
.LBB4_13:                               # %.lr.ph.i11.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph.i11
                                        # =>This Inner Loop Header: Depth=1
	movsbq	%cl, %rcx
	leaq	(%rax,%rax,4), %rax
	leaq	-48(%rcx,%rax,2), %rax
	movzbl	1(%rdi), %ecx
	incq	%rdi
	movl	%ecx, %edx
	addb	$-48, %dl
	cmpb	$9, %dl
	jbe	.LBB4_14
# BB#10:                                # %._crit_edge.i7
	testq	%rsi, %rsi
	je	.LBB4_12
.LBB4_11:
	movq	%rdi, (%rsi)
.LBB4_12:                               # %_Z21ConvertStringToUInt64PKcPS0_.exit12
	retq
.Lfunc_end4:
	.size	_Z20ConvertStringToInt64PKcPS0_, .Lfunc_end4-_Z20ConvertStringToInt64PKcPS0_
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
