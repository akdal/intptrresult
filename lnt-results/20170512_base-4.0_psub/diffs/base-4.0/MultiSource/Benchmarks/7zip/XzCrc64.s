	.text
	.file	"XzCrc64.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
.LCPI0_1:
	.quad	-3932672073523589310    # 0xc96c5795d7870f42
	.quad	-3932672073523589310    # 0xc96c5795d7870f42
.LCPI0_2:
	.quad	2                       # 0x2
	.quad	2                       # 0x2
	.text
	.globl	Crc64GenerateTable
	.p2align	4, 0x90
	.type	Crc64GenerateTable,@function
Crc64GenerateTable:                     # @Crc64GenerateTable
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movq	$-2048, %rax            # imm = 0xF800
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [1,1]
	movdqa	.LCPI0_1(%rip), %xmm2   # xmm2 = [14514072000185962306,14514072000185962306]
	movdqa	.LCPI0_2(%rip), %xmm3   # xmm3 = [2,2]
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	psrlq	$1, %xmm4
	movdqa	%xmm0, %xmm5
	pand	%xmm1, %xmm5
	pxor	%xmm6, %xmm6
	psubq	%xmm5, %xmm6
	pand	%xmm2, %xmm6
	pxor	%xmm4, %xmm6
	psrlq	$1, %xmm6
	pand	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	psubq	%xmm4, %xmm5
	pand	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	psrlq	$1, %xmm5
	pand	%xmm1, %xmm6
	pxor	%xmm4, %xmm4
	psubq	%xmm6, %xmm4
	pand	%xmm2, %xmm4
	pxor	%xmm5, %xmm4
	psrlq	$1, %xmm4
	pand	%xmm1, %xmm5
	pxor	%xmm6, %xmm6
	psubq	%xmm5, %xmm6
	pand	%xmm2, %xmm6
	pxor	%xmm4, %xmm6
	psrlq	$1, %xmm6
	pand	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	psubq	%xmm4, %xmm5
	pand	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	psrlq	$1, %xmm5
	pand	%xmm1, %xmm6
	pxor	%xmm4, %xmm4
	psubq	%xmm6, %xmm4
	pand	%xmm2, %xmm4
	pxor	%xmm5, %xmm4
	psrlq	$1, %xmm4
	pand	%xmm1, %xmm5
	pxor	%xmm6, %xmm6
	psubq	%xmm5, %xmm6
	pand	%xmm2, %xmm6
	pxor	%xmm4, %xmm6
	psrlq	$1, %xmm6
	pand	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	psubq	%xmm4, %xmm5
	pand	%xmm2, %xmm5
	pxor	%xmm6, %xmm5
	movdqa	%xmm5, g_Crc64Table+2048(%rax)
	paddq	%xmm3, %xmm0
	addq	$16, %rax
	jne	.LBB0_1
# BB#2:                                 # %middle.block
	retq
.Lfunc_end0:
	.size	Crc64GenerateTable, .Lfunc_end0-Crc64GenerateTable
	.cfi_endproc

	.globl	Crc64Update
	.p2align	4, 0x90
	.type	Crc64Update,@function
Crc64Update:                            # @Crc64Update
	.cfi_startproc
# BB#0:
	testq	%rdx, %rdx
	je	.LBB1_6
# BB#1:                                 # %.lr.ph.preheader
	testb	$1, %dl
	jne	.LBB1_3
# BB#2:
	movq	%rdx, %rax
	cmpq	$1, %rdx
	jne	.LBB1_5
	jmp	.LBB1_6
.LBB1_3:                                # %.lr.ph.prol
	movzbl	(%rsi), %eax
	movzbl	%dil, %ecx
	xorq	%rax, %rcx
	shrq	$8, %rdi
	xorq	g_Crc64Table(,%rcx,8), %rdi
	leaq	-1(%rdx), %rax
	incq	%rsi
	cmpq	$1, %rdx
	je	.LBB1_6
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %ecx
	movzbl	%dil, %edx
	xorq	%rcx, %rdx
	shrq	$8, %rdi
	xorq	g_Crc64Table(,%rdx,8), %rdi
	movzbl	1(%rsi), %ecx
	movzbl	%dil, %edx
	xorq	%rcx, %rdx
	shrq	$8, %rdi
	xorq	g_Crc64Table(,%rdx,8), %rdi
	addq	$2, %rsi
	addq	$-2, %rax
	jne	.LBB1_5
.LBB1_6:                                # %._crit_edge
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	Crc64Update, .Lfunc_end1-Crc64Update
	.cfi_endproc

	.globl	Crc64Calc
	.p2align	4, 0x90
	.type	Crc64Calc,@function
Crc64Calc:                              # @Crc64Calc
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB2_1
# BB#2:                                 # %.lr.ph.i.preheader
	testb	$1, %sil
	jne	.LBB2_4
# BB#3:
	movq	$-1, %rax
	movq	%rsi, %rcx
	cmpq	$1, %rsi
	jne	.LBB2_6
	jmp	.LBB2_7
.LBB2_1:
	xorl	%eax, %eax
	retq
.LBB2_4:                                # %.lr.ph.i.prol
	movzbl	(%rdi), %ecx
	xorq	$255, %rcx
	movabsq	$72057594037927935, %rax # imm = 0xFFFFFFFFFFFFFF
	xorq	g_Crc64Table(,%rcx,8), %rax
	leaq	-1(%rsi), %rcx
	incq	%rdi
	cmpq	$1, %rsi
	je	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %edx
	movzbl	%al, %esi
	xorq	%rdx, %rsi
	shrq	$8, %rax
	xorq	g_Crc64Table(,%rsi,8), %rax
	movzbl	1(%rdi), %edx
	movzbl	%al, %esi
	xorq	%rdx, %rsi
	shrq	$8, %rax
	xorq	g_Crc64Table(,%rsi,8), %rax
	addq	$2, %rdi
	addq	$-2, %rcx
	jne	.LBB2_6
.LBB2_7:                                # %Crc64Update.exit.loopexit
	notq	%rax
	retq
.Lfunc_end2:
	.size	Crc64Calc, .Lfunc_end2-Crc64Calc
	.cfi_endproc

	.type	g_Crc64Table,@object    # @g_Crc64Table
	.comm	g_Crc64Table,2048,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
