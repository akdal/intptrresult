	.text
	.file	"BwtSort.bc"
	.globl	SortGroup
	.p2align	4, 0x90
	.type	SortGroup,@function
SortGroup:                              # @SortGroup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r11d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r13d
	movl	%edi, %r14d
	xorl	%esi, %esi
	cmpl	$2, %r11d
	jae	.LBB0_3
.LBB0_1:                                # %SetGroupSize.exit
	movl	%esi, %eax
.LBB0_2:                                # %SetGroupSize.exit
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_3:
	movl	152(%rsp), %r10d
	movq	%rdx, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	%edx, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	(%r9,%rax,4), %rbp
	movl	%r11d, %r15d
	movl	%r14d, %edi
	movl	$1, %ebx
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, %ecx
	shll	%cl, %ebx
	movl	(%rbp), %ecx
	leal	(%rcx,%r13), %eax
	xorl	%edx, %edx
	cmpl	%r14d, %eax
	movl	%r14d, %esi
	cmovbl	%edx, %esi
	subl	%esi, %eax
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leaq	(%r9,%rdi,4), %rsi
	movl	%ebx, 16(%rsp)          # 4-byte Spill
	cmpl	%r11d, %ebx
	movl	262144(%rsi,%rax,4), %r8d
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	jb	.LBB0_7
# BB#4:
	cmpl	%r10d, %r11d
	ja	.LBB0_7
# BB#5:                                 # %.lr.ph349.preheader
	movl	%r8d, %eax
	movl	4(%rsp), %ecx           # 4-byte Reload
	shll	%cl, %eax
	movl	%eax, (%rsi)
	testb	$1, %r11b
	movq	%r9, 40(%rsp)           # 8-byte Spill
	jne	.LBB0_14
# BB#6:                                 # %.lr.ph349.prol
	movl	4(%rbp), %eax
	addl	%r13d, %eax
	xorl	%edx, %edx
	cmpl	%r14d, %eax
	cmovael	%r14d, %edx
	subl	%edx, %eax
	movl	262144(%rsi,%rax,4), %eax
	movl	%eax, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	$1, %edx
	movl	%edx, 4(%rsi)
	xorl	%r8d, %eax
	movl	$2, %esi
	jmp	.LBB0_15
.LBB0_7:                                # %.lr.ph330.preheader
	movl	144(%rsp), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph330
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rsi,4), %eax
	addl	%r13d, %eax
	cmpl	%r14d, %eax
	movl	%r14d, %edi
	cmovbl	%edx, %edi
	subl	%edi, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	%r8d, 262144(%rdi,%rax,4)
	jne	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_8 Depth=1
	incq	%rsi
	cmpl	%r11d, %esi
	jb	.LBB0_8
.LBB0_10:                               # %._crit_edge
	cmpl	%r11d, %esi
	jne	.LBB0_43
# BB#11:                                # %.critedge302
	decl	%r11d
	movl	$1, %esi
	je	.LBB0_1
# BB#12:
	movl	%r11d, %eax
	shll	$20, %eax
	andl	$1072693248, %eax       # imm = 0x3FF00000
	orl	%eax, %ecx
	movl	%ecx, %eax
	orl	$-2147483648, %eax      # imm = 0x80000000
	movl	%eax, (%rbp)
	cmpl	$1024, %r11d            # imm = 0x400
	jb	.LBB0_1
# BB#13:
	orl	$-1073741824, %ecx      # imm = 0xC0000000
	movl	%ecx, (%rbp)
	jmp	.LBB0_60
.LBB0_14:
	xorl	%eax, %eax
	movl	$1, %esi
.LBB0_15:                               # %.lr.ph349.prol.loopexit
	movq	%r11, 56(%rsp)          # 8-byte Spill
	cmpl	$2, %r11d
	movq	8(%rsp), %r12           # 8-byte Reload
	movl	4(%rsp), %r11d          # 4-byte Reload
	je	.LBB0_18
# BB#16:                                # %.lr.ph349.preheader.new
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rcx,%rdx,4), %r10
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rcx,%rdx,4), %rdx
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph349
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r10,%rsi,4), %ecx
	addl	%r13d, %ecx
	cmpl	%r14d, %ecx
	movl	%r14d, %ebx
	cmovbl	%r9d, %ebx
	subl	%ebx, %ecx
	movl	262144(%r12,%rcx,4), %edi
	movl	%edi, %ebx
	movl	%r11d, %ecx
	shll	%cl, %ebx
	orl	%esi, %ebx
	movl	%ebx, -4(%rdx,%rsi,4)
	xorl	%r8d, %edi
	orl	%eax, %edi
	movl	(%r10,%rsi,4), %eax
	addl	%r13d, %eax
	cmpl	%r14d, %eax
	movl	%r14d, %ecx
	cmovbl	%r9d, %ecx
	subl	%ecx, %eax
	movl	262144(%r12,%rax,4), %eax
	movl	%eax, %ebx
	movl	%r11d, %ecx
	shll	%cl, %ebx
	leal	1(%rsi), %ecx
	orl	%ebx, %ecx
	movl	%ecx, (%rdx,%rsi,4)
	xorl	%r8d, %eax
	orl	%edi, %eax
	addq	$2, %rsi
	cmpq	%r15, %rsi
	jne	.LBB0_17
.LBB0_18:                               # %._crit_edge350
	testl	%eax, %eax
	je	.LBB0_54
# BB#19:                                # %.lr.ph341.preheader
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	HeapSort
	movl	16(%rsp), %r12d         # 4-byte Reload
	decl	%r12d
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	(%rdx,%rsi,4), %edi
	movl	%edi, %eax
	andl	%r12d, %eax
	movl	(%rbp,%rax,4), %eax
	movl	%eax, (%rdx,%rsi,4)
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	%r8d, %ecx
	shrl	%cl, %edi
	leaq	4(%rdx,%rsi,4), %r10
	movq	80(%rsp), %r14          # 8-byte Reload
	leal	1(%r14), %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	-1(%r15), %r9
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph341
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r10,%rsi,4), %edx
	movl	%edx, %ebx
	movl	%r8d, %ecx
	shrl	%cl, %ebx
	cmpl	%edi, %ebx
	jne	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%edi, %ebx
	movl	$1, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_22:                               #   in Loop: Header=BB0_20 Depth=1
	leaq	1(%rsi), %r14
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi), %r13
	movl	%r11d, %edi
	negl	%edi
	addq	%rsi, %rdi
	testl	%edi, %edi
	je	.LBB0_26
# BB#23:                                #   in Loop: Header=BB0_20 Depth=1
	movl	%r11d, %r15d
	shll	$20, %r11d
	movl	%ebp, %r8d
	subl	%r11d, %r8d
	andl	$1072693248, %r8d       # imm = 0x3FF00000
	movq	8(%rsp), %rax           # 8-byte Reload
	orl	(%rax,%r15,4), %r8d
	movl	%r8d, %ecx
	orl	$-2147483648, %ecx      # imm = 0x80000000
	movl	%ecx, (%rax,%r15,4)
	cmpl	$1023, %edi             # imm = 0x3FF
	jbe	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_20 Depth=1
	orl	$-1073741824, %r8d      # imm = 0xC0000000
	movl	%r8d, (%rax,%r15,4)
	shll	$10, %edi
	andl	$-1048576, %edi         # imm = 0xFFF00000
	orl	%edi, 4(%rax,%r15,4)
.LBB0_25:                               # %SetGroupSize.exit305
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	%r14d, %r11d
	movq	64(%rsp), %r15          # 8-byte Reload
	jmp	.LBB0_27
.LBB0_26:                               # %.SetGroupSize.exit305_crit_edge
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	%r14d, %r11d
.LBB0_27:                               # %SetGroupSize.exit305
                                        #   in Loop: Header=BB0_20 Depth=1
	movl	4(%rsp), %r8d           # 4-byte Reload
	movq	%r13, %r14
.LBB0_28:                               # %SetGroupSize.exit305
                                        #   in Loop: Header=BB0_20 Depth=1
	andl	%r12d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %ecx
	movl	%ecx, (%r10,%rsi,4)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r14d, 262144(%rax,%rcx,4)
	incq	%rsi
	addl	$1048576, %ebp          # imm = 0x100000
	cmpq	%rsi, %r9
	movl	%ebx, %edi
	jne	.LBB0_20
# BB#29:                                # %._crit_edge342
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%r11d, %ecx
	decl	%ecx
	je	.LBB0_32
# BB#30:
	movl	%r11d, %edx
	movl	%ecx, %esi
	shll	$20, %esi
	andl	$1072693248, %esi       # imm = 0x3FF00000
	movq	8(%rsp), %rdi           # 8-byte Reload
	orl	(%rdi,%rdx,4), %esi
	movl	%esi, %eax
	orl	$-2147483648, %eax      # imm = 0x80000000
	movl	%eax, (%rdi,%rdx,4)
	cmpl	$1024, %ecx             # imm = 0x400
	jb	.LBB0_32
# BB#31:
	orl	$-1073741824, %esi      # imm = 0xC0000000
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%esi, (%rax,%rdx,4)
	shll	$10, %ecx
	andl	$-1048576, %ecx         # imm = 0xFFF00000
	orl	%ecx, 4(%rax,%rdx,4)
.LBB0_32:                               # %SetGroupSize.exit306.preheader
	movq	56(%rsp), %r13          # 8-byte Reload
	testl	%r13d, %r13d
	movq	40(%rsp), %rbp          # 8-byte Reload
	je	.LBB0_65
# BB#33:                                # %SetGroupSize.exit306.preheader378
	cmpl	$8, %r15d
	jb	.LBB0_37
# BB#34:                                # %min.iters.checked
	andl	$7, %r13d
	movq	%r15, %rcx
	subq	%r13, %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	je	.LBB0_37
# BB#35:                                # %vector.memcheck
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%r15,%rax), %rax
	leaq	(%rbp,%rax,4), %rax
	cmpq	%rax, 24(%rsp)          # 8-byte Folded Reload
	jae	.LBB0_68
# BB#36:                                # %vector.memcheck
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r15,%rax), %rax
	leaq	(%rbp,%rax,4), %rax
	cmpq	%rax, %rdx
	jae	.LBB0_68
.LBB0_37:
	xorl	%ecx, %ecx
.LBB0_38:                               # %SetGroupSize.exit306.preheader408
	movl	%r15d, %esi
	subl	%ecx, %esi
	leaq	-1(%r15), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_41
# BB#39:                                # %SetGroupSize.exit306.prol.preheader
	negq	%rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_40:                               # %SetGroupSize.exit306.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %eax
	movl	%eax, (%rdi,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_40
.LBB0_41:                               # %SetGroupSize.exit306.prol.loopexit
	cmpq	$7, %rdx
	jae	.LBB0_66
.LBB0_65:
	movl	16(%rsp), %esi          # 4-byte Reload
	jmp	.LBB0_1
.LBB0_43:
	xorl	%ecx, %ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB0_47
.LBB0_54:                               # %.critedge
	movq	56(%rsp), %rax          # 8-byte Reload
	decl	%eax
	movl	$1, %esi
	je	.LBB0_1
# BB#55:
	movl	%eax, %ecx
	shll	$20, %ecx
	andl	$1072693248, %ecx       # imm = 0x3FF00000
	orl	(%rbp), %ecx
	movl	%ecx, %edx
	orl	$-2147483648, %edx      # imm = 0x80000000
	movl	%edx, (%rbp)
	cmpl	$1024, %eax             # imm = 0x400
	jb	.LBB0_1
# BB#56:
	orl	$-1073741824, %ecx      # imm = 0xC0000000
	movl	%ecx, (%rbp)
	shll	$10, %eax
	andl	$-1048576, %eax         # imm = 0xFFF00000
	orl	%eax, 4(%rbp)
	jmp	.LBB0_1
.LBB0_46:                               #   in Loop: Header=BB0_47 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	subl	%r10d, %eax
	movl	%eax, %r10d
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	movq	%r15, 16(%rsp)          # 8-byte Spill
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_44:                               #   in Loop: Header=BB0_47 Depth=1
	testl	%r12d, %r12d
	je	.LBB0_46
.LBB0_45:                               # %.thread312
                                        #   in Loop: Header=BB0_47 Depth=1
	cmpl	%r11d, %r12d
	jne	.LBB0_61
.LBB0_47:                               # %SetGroupSize.exit307.thread315
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_49 Depth 2
                                        #       Child Loop BB0_50 Depth 3
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	cmpl	$1, %r10d
	jbe	.LBB0_57
# BB#48:                                #   in Loop: Header=BB0_47 Depth=1
	movq	%r10, 72(%rsp)          # 8-byte Spill
	leal	1(%r10), %eax
	shrl	%eax
	movq	%rax, %r10
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	(%rax,%rdx), %r15d
	xorl	%r12d, %r12d
	movl	%r11d, %edx
.LBB0_49:                               #   Parent Loop BB0_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_50 Depth 3
	movl	%r12d, %r8d
	movl	(%rbp,%r8,4), %edi
	leal	(%rdi,%r13), %eax
	cmpl	%r14d, %eax
	movl	%r14d, %ebx
	cmovbl	%ecx, %ebx
	subl	%ebx, %eax
	cmpl	%r15d, 262144(%rsi,%rax,4)
	jb	.LBB0_53
	.p2align	4, 0x90
.LBB0_50:                               # %.preheader
                                        #   Parent Loop BB0_47 Depth=1
                                        #     Parent Loop BB0_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%edx
	cmpl	%r12d, %edx
	jbe	.LBB0_44
# BB#51:                                #   in Loop: Header=BB0_50 Depth=3
	movl	%edx, %ebx
	movl	(%rbp,%rbx,4), %eax
	leal	(%rax,%r13), %ebp
	cmpl	%r14d, %ebp
	movl	%r14d, %esi
	cmovbl	%ecx, %esi
	subl	%esi, %ebp
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpl	%r15d, 262144(%rsi,%rbp,4)
	movq	24(%rsp), %rbp          # 8-byte Reload
	jae	.LBB0_50
# BB#52:                                # %.thread
                                        #   in Loop: Header=BB0_49 Depth=2
	movl	%eax, (%rbp,%r8,4)
	movl	%edi, (%rbp,%rbx,4)
.LBB0_53:                               #   in Loop: Header=BB0_49 Depth=2
	incl	%r12d
	cmpl	%edx, %r12d
	jb	.LBB0_49
	jmp	.LBB0_45
.LBB0_57:
	decl	%r11d
	movl	$1, %esi
	je	.LBB0_1
# BB#58:
	movl	%r11d, %ecx
	shll	$20, %ecx
	andl	$1072693248, %ecx       # imm = 0x3FF00000
	orl	(%rbp), %ecx
	movl	%ecx, %eax
	orl	$-2147483648, %eax      # imm = 0x80000000
	movl	%eax, (%rbp)
	cmpl	$1024, %r11d            # imm = 0x400
	jb	.LBB0_1
# BB#59:
	orl	$-1073741824, %ecx      # imm = 0xC0000000
	movl	%ecx, (%rbp)
.LBB0_60:                               # %SetGroupSize.exit
	shll	$10, %r11d
	andl	$-1048576, %r11d        # imm = 0xFFF00000
	orl	%r11d, 4(%rbp)
	jmp	.LBB0_1
.LBB0_61:                               # %SetGroupSize.exit307.preheader
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%r12,%rax), %ebx
	cmpl	%r11d, %r12d
	movq	64(%rsp), %rax          # 8-byte Reload
	jae	.LBB0_76
# BB#62:                                # %.lr.ph
	movl	%r12d, %ecx
	movl	%r11d, %esi
	subl	%r12d, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB0_72
# BB#63:                                # %SetGroupSize.exit307.prol.preheader
	negq	%rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_64:                               # %SetGroupSize.exit307.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx,4), %eax
	movl	%ebx, 262144(%rdi,%rax,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_64
	jmp	.LBB0_73
.LBB0_66:                               # %SetGroupSize.exit306.preheader408.new
	subq	%rcx, %r15
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	%rcx, %rax
	leaq	28(%rbp,%rax,4), %rdx
	movq	48(%rsp), %rax          # 8-byte Reload
	addq	%rcx, %rax
	leaq	28(%rbp,%rax,4), %rcx
	movl	16(%rsp), %esi          # 4-byte Reload
	.p2align	4, 0x90
.LBB0_67:                               # %SetGroupSize.exit306
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %eax
	movl	%eax, -28(%rdx)
	movl	-24(%rcx), %eax
	movl	%eax, -24(%rdx)
	movl	-20(%rcx), %eax
	movl	%eax, -20(%rdx)
	movl	-16(%rcx), %eax
	movl	%eax, -16(%rdx)
	movl	-12(%rcx), %eax
	movl	%eax, -12(%rdx)
	movl	-8(%rcx), %eax
	movl	%eax, -8(%rdx)
	movl	-4(%rcx), %eax
	movl	%eax, -4(%rdx)
	movl	(%rcx), %eax
	movl	%eax, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %r15
	jne	.LBB0_67
	jmp	.LBB0_1
.LBB0_68:                               # %vector.body.preheader
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	16(%rbp,%rax,4), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	16(%rbp,%rax,4), %rsi
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB0_69:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	addq	$32, %rdx
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB0_69
# BB#70:                                # %middle.block
	testl	%r13d, %r13d
	jne	.LBB0_38
	jmp	.LBB0_65
.LBB0_72:
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB0_73:                               # %SetGroupSize.exit307.prol.loopexit
	cmpq	$7, %rdx
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%rdi, %rsi
	jb	.LBB0_76
# BB#74:                                # %.lr.ph.new
	subq	%rcx, %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	%rcx, %rax
	leaq	28(%r9,%rax,4), %rcx
	.p2align	4, 0x90
.LBB0_75:                               # %SetGroupSize.exit307
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %eax
	movl	%ebx, 262144(%rsi,%rax,4)
	movl	-24(%rcx), %eax
	movl	%ebx, 262144(%rsi,%rax,4)
	movl	-20(%rcx), %eax
	movl	%ebx, 262144(%rsi,%rax,4)
	movl	-16(%rcx), %eax
	movl	%ebx, 262144(%rsi,%rax,4)
	movl	-12(%rcx), %eax
	movl	%ebx, 262144(%rsi,%rax,4)
	movl	-8(%rcx), %eax
	movl	%ebx, 262144(%rsi,%rax,4)
	movl	-4(%rcx), %eax
	movl	%ebx, 262144(%rsi,%rax,4)
	movl	(%rcx), %eax
	movl	%ebx, 262144(%rsi,%rax,4)
	addq	$32, %rcx
	addq	$-8, %rdx
	jne	.LBB0_75
.LBB0_76:                               # %SetGroupSize.exit307._crit_edge
	movl	%r14d, %edi
	movl	%r13d, %esi
	movq	80(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r12d, %ecx
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movl	4(%rsp), %r8d           # 4-byte Reload
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r10, %rbx
	pushq	%rbx
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	movq	%r11, %rbp
	callq	SortGroup
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movl	%eax, 8(%rsp)           # 4-byte Spill
	subl	%r12d, %ebp
	movq	72(%rsp), %rax          # 8-byte Reload
	subl	%ebx, %eax
	movl	%r14d, %edi
	movl	%r13d, %esi
	movl	24(%rsp), %edx          # 4-byte Reload
	movl	%ebp, %ecx
	movl	4(%rsp), %r8d           # 4-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	pushq	%rax
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	SortGroup
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	orl	8(%rsp), %eax           # 4-byte Folded Reload
	jmp	.LBB0_2
.Lfunc_end0:
	.size	SortGroup, .Lfunc_end0-SortGroup
	.cfi_endproc

	.globl	BlockSort
	.p2align	4, 0x90
	.type	BlockSort,@function
BlockSort:                              # @BlockSort
	.cfi_startproc
# BB#0:                                 # %.preheader208
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 80
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r12
	movq	%rdi, %r13
	movl	%ebx, %r14d
	leaq	(%r13,%r14,4), %r15
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$262144, %edx           # imm = 0x40000
	movq	%r15, %rdi
	callq	memset
	movq	%rbx, %r11
	movzbl	(%r12), %edx
	movl	%edx, %ecx
	shll	$8, %ecx
	movl	%r11d, %eax
	decl	%eax
	je	.LBB1_8
# BB#1:                                 # %.lr.ph242.preheader
	movl	%eax, %edx
	testb	$1, %dl
	jne	.LBB1_3
# BB#2:
	xorl	%esi, %esi
	cmpl	$1, %eax
	jne	.LBB1_5
	jmp	.LBB1_7
.LBB1_3:                                # %.lr.ph242.prol
	movzbl	1(%r12), %esi
	orl	%ecx, %esi
	incl	(%r15,%rsi,4)
	movzbl	1(%r12), %ecx
	shll	$8, %ecx
	movl	$1, %esi
	cmpl	$1, %eax
	je	.LBB1_7
.LBB1_5:                                # %.lr.ph242.preheader.new
	subq	%rsi, %rdx
	leaq	2(%r12,%rsi), %rsi
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph242
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rsi), %edi
	orl	%ecx, %edi
	incl	(%r15,%rdi,4)
	movzbl	-1(%rsi), %ecx
	shlq	$8, %rcx
	movzbl	(%rsi), %edi
	orq	%rcx, %rdi
	incl	(%r15,%rdi,4)
	movzbl	(%rsi), %ecx
	shll	$8, %ecx
	addq	$2, %rsi
	addq	$-2, %rdx
	jne	.LBB1_6
.LBB1_7:                                # %._crit_edge243.loopexit
	movb	(%r12), %dl
.LBB1_8:                                # %._crit_edge243
	movzbl	%dl, %edx
	orl	%ecx, %edx
	incl	(%r15,%rdx,4)
	leaq	12(%r13,%r14,4), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movl	-12(%rcx,%rdx,4), %esi
	addl	%ebp, %esi
	movl	%ebp, -12(%rcx,%rdx,4)
	movl	-8(%rcx,%rdx,4), %edi
	addl	%esi, %edi
	movl	%esi, -8(%rcx,%rdx,4)
	movl	-4(%rcx,%rdx,4), %esi
	addl	%edi, %esi
	movl	%edi, -4(%rcx,%rdx,4)
	movl	(%rcx,%rdx,4), %ebp
	addl	%esi, %ebp
	movl	%esi, (%rcx,%rdx,4)
	addq	$4, %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jne	.LBB1_9
# BB#10:                                # %.preheader207
	movzbl	(%r12), %ecx
	movl	%ecx, %esi
	shll	$8, %esi
	xorl	%r8d, %r8d
	testl	%eax, %eax
	je	.LBB1_11
# BB#12:                                # %.lr.ph235.preheader
	leal	-2(%r11), %edx
	movl	%eax, %edi
	testb	$1, %dil
	jne	.LBB1_14
# BB#13:
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	jne	.LBB1_16
	jmp	.LBB1_18
.LBB1_11:
	xorl	%edx, %edx
	jmp	.LBB1_19
.LBB1_14:                               # %.lr.ph235.prol
	movzbl	1(%r12), %ecx
	orl	%esi, %ecx
	movl	(%r15,%rcx,4), %ecx
	movl	%ecx, 262144(%r15)
	movzbl	1(%r12), %esi
	shll	$8, %esi
	movl	$1, %ecx
	cmpl	$1, %eax
	je	.LBB1_18
.LBB1_16:                               # %.lr.ph235.preheader.new
	subq	%rcx, %rdi
	leaq	2(%r12,%rcx), %rbp
	addq	%rcx, %r14
	leaq	262148(%r13,%r14,4), %rbx
	.p2align	4, 0x90
.LBB1_17:                               # %.lr.ph235
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rbp), %ecx
	orl	%esi, %ecx
	movl	(%r15,%rcx,4), %ecx
	movl	%ecx, -4(%rbx)
	movzbl	-1(%rbp), %ecx
	shlq	$8, %rcx
	movzbl	(%rbp), %esi
	orq	%rcx, %rsi
	movl	(%r15,%rsi,4), %ecx
	movl	%ecx, (%rbx)
	movzbl	(%rbp), %esi
	shll	$8, %esi
	addq	$2, %rbp
	addq	$8, %rbx
	addq	$-2, %rdi
	jne	.LBB1_17
.LBB1_18:                               # %._crit_edge236.loopexit
	incq	%rdx
	movb	(%r12), %cl
.LBB1_19:                               # %._crit_edge236
	movzbl	%cl, %ecx
	orl	%esi, %ecx
	movl	(%r15,%rcx,4), %ecx
	movl	%ecx, 262144(%r15,%rdx,4)
	movzbl	(%r12), %ecx
	movl	%ecx, %edx
	shll	$8, %edx
	testl	%eax, %eax
	je	.LBB1_26
# BB#20:                                # %.lr.ph230.preheader
	movl	%eax, %ecx
	testb	$1, %cl
	jne	.LBB1_22
# BB#21:
	xorl	%esi, %esi
	cmpl	$1, %eax
	jne	.LBB1_24
	jmp	.LBB1_25
.LBB1_22:                               # %.lr.ph230.prol
	movzbl	1(%r12), %esi
	orl	%edx, %esi
	movl	(%r15,%rsi,4), %edx
	leal	1(%rdx), %edi
	movl	%edi, (%r15,%rsi,4)
	movl	$0, (%r13,%rdx,4)
	movzbl	1(%r12), %edx
	shll	$8, %edx
	movl	$1, %esi
	cmpl	$1, %eax
	je	.LBB1_25
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph230
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%r12,%rsi), %edi
	orl	%edx, %edi
	movl	(%r15,%rdi,4), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r15,%rdi,4)
	movl	%esi, (%r13,%rdx,4)
	movzbl	1(%r12,%rsi), %edx
	shlq	$8, %rdx
	movzbl	2(%r12,%rsi), %edi
	orq	%rdx, %rdi
	movl	(%r15,%rdi,4), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r15,%rdi,4)
	leal	1(%rsi), %edi
	movl	%edi, (%r13,%rdx,4)
	movzbl	2(%r12,%rsi), %edx
	shll	$8, %edx
	addq	$2, %rsi
	cmpq	%rcx, %rsi
	jne	.LBB1_24
.LBB1_25:                               # %._crit_edge231.loopexit
	movb	(%r12), %cl
	movl	%eax, %r8d
.LBB1_26:                               # %._crit_edge231
	leaq	262144(%r15), %r14
	movzbl	%cl, %ecx
	orl	%edx, %ecx
	movl	(%r15,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, (%r15,%rcx,4)
	movl	%r8d, (%r13,%rdx,4)
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_27:                               # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rcx,4), %edx
	subl	%esi, %edx
	je	.LBB1_32
# BB#28:                                #   in Loop: Header=BB1_27 Depth=1
	decl	%edx
	je	.LBB1_31
# BB#29:                                #   in Loop: Header=BB1_27 Depth=1
	movl	%esi, %esi
	movl	%edx, %ebp
	shll	$20, %ebp
	andl	$1072693248, %ebp       # imm = 0x3FF00000
	orl	(%r13,%rsi,4), %ebp
	movl	%ebp, %edi
	orl	$-2147483648, %edi      # imm = 0x80000000
	movl	%edi, (%r13,%rsi,4)
	cmpl	$1024, %edx             # imm = 0x400
	jb	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_27 Depth=1
	orl	$-1073741824, %ebp      # imm = 0xC0000000
	movl	%ebp, (%r13,%rsi,4)
	shll	$10, %edx
	andl	$-1048576, %edx         # imm = 0xFFF00000
	orl	%edx, 4(%r13,%rsi,4)
.LBB1_31:                               # %SetGroupSize.exit
                                        #   in Loop: Header=BB1_27 Depth=1
	movl	(%r15,%rcx,4), %esi
.LBB1_32:                               #   in Loop: Header=BB1_27 Depth=1
	incq	%rcx
	cmpq	$65536, %rcx            # imm = 0x10000
	jne	.LBB1_27
# BB#33:                                # %.preheader206.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_34:                               # %.preheader206
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	shrl	%cl, %edx
	incl	%ecx
	testl	%edx, %edx
	jne	.LBB1_34
# BB#35:
	movl	$33, %eax
	subl	%ecx, %eax
	cmpl	$13, %eax
	movl	$12, %ecx
	cmovll	%eax, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	$2, %r12d
	movq	%r14, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_36:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_39 Depth 2
                                        #       Child Loop BB1_40 Depth 3
	testl	%r11d, %r11d
	je	.LBB1_74
# BB#37:                                # %.lr.ph218.lr.ph
                                        #   in Loop: Header=BB1_36 Depth=1
	cmpl	%r11d, %r12d
	jae	.LBB1_51
# BB#38:                                # %.lr.ph218.us.preheader
                                        #   in Loop: Header=BB1_36 Depth=1
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
.LBB1_39:                               # %.lr.ph218.us
                                        #   Parent Loop BB1_36 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_40 Depth 3
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_40:                               #   Parent Loop BB1_36 Depth=1
                                        #     Parent Loop BB1_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r14d, %ecx
	leaq	(%r13,%rcx,4), %r8
	movl	(%r13,%rcx,4), %esi
	movl	%esi, %ecx
	shrl	$20, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	testl	$1073741824, %esi       # imm = 0x40000000
	je	.LBB1_42
# BB#41:                                #   in Loop: Header=BB1_40 Depth=3
	leal	1(%r14), %edi
	movl	(%r13,%rdi,4), %eax
	movl	%eax, %ebx
	shrl	$10, %ebx
	andl	$4193280, %ebx          # imm = 0x3FFC00
	orl	%ecx, %ebx
	andl	$1048575, %eax          # imm = 0xFFFFF
	movl	%eax, (%r13,%rdi,4)
	movl	%ebx, %ecx
.LBB1_42:                               #   in Loop: Header=BB1_40 Depth=3
	movl	%esi, %eax
	andl	$1048575, %eax          # imm = 0xFFFFF
	leal	1(%rcx), %ebx
	testl	%esi, %esi
	movl	%eax, (%r8)
	jns	.LBB1_46
# BB#43:                                #   in Loop: Header=BB1_40 Depth=3
	testl	%ecx, %ecx
	jne	.LBB1_44
.LBB1_46:                               #   in Loop: Header=BB1_40 Depth=3
	movl	%r14d, %esi
	subl	%edx, %esi
	andl	$1048575, (%r13,%rsi,4) # imm = 0xFFFFF
	cmpl	$2, %edx
	jb	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_40 Depth=3
	incl	%esi
	andl	$1048575, (%r13,%rsi,4) # imm = 0xFFFFF
.LBB1_48:                               #   in Loop: Header=BB1_40 Depth=3
	leal	(%rbx,%rdx), %esi
	addl	%edx, %ecx
	movl	%ecx, %edi
	shll	$20, %edi
	andl	$1072693248, %edi       # imm = 0x3FF00000
	movl	%edx, %edx
	leaq	(,%rdx,4), %rax
	movq	%r8, %rbp
	subq	%rax, %rbp
	orl	(%rbp), %edi
	movl	%edi, (%rbp)
	cmpl	$1025, %esi             # imm = 0x401
	jb	.LBB1_50
# BB#49:                                #   in Loop: Header=BB1_40 Depth=3
	negq	%rdx
	orl	$1073741824, %edi       # imm = 0x40000000
	movl	%edi, (%r8,%rdx,4)
	shll	$10, %ecx
	andl	$-1048576, %ecx         # imm = 0xFFF00000
	orl	%ecx, 4(%r8,%rdx,4)
.LBB1_50:                               #   in Loop: Header=BB1_40 Depth=3
	addl	%r14d, %ebx
	cmpl	%r11d, %ebx
	movl	%ebx, %r14d
	movl	%esi, %edx
	jb	.LBB1_40
	jmp	.LBB1_45
	.p2align	4, 0x90
.LBB1_44:                               # %.outer.us
                                        #   in Loop: Header=BB1_39 Depth=2
	movl	%r11d, %edi
	movl	%r12d, %esi
	movl	%r14d, %edx
	movl	%ebx, %ecx
	movl	12(%rsp), %r8d          # 4-byte Reload
	movq	%r13, %r9
	pushq	%r11
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	movq	%r11, %rbp
	callq	SortGroup
	movq	%rbp, %r11
	addq	$16, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset -16
	addl	%r14d, %ebx
	testl	%eax, %eax
	cmovnel	%ebx, %r15d
	cmpl	%r11d, %ebx
	movl	%ebx, %r14d
	jb	.LBB1_39
	.p2align	4, 0x90
.LBB1_45:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB1_36 Depth=1
	addl	%r12d, %r12d
	testl	%r15d, %r15d
	movq	16(%rsp), %r14          # 8-byte Reload
	jne	.LBB1_36
	jmp	.LBB1_69
.LBB1_51:                               # %.lr.ph218.preheader
	xorl	%ecx, %ecx
.LBB1_52:                               # %.lr.ph218
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_53 Depth 2
                                        #     Child Loop BB1_64 Depth 2
                                        #     Child Loop BB1_67 Depth 2
	movl	%ecx, %r9d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_53:                               #   Parent Loop BB1_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r9d, %r8d
	leaq	(%r13,%r8,4), %r10
	movl	(%r13,%r8,4), %ebx
	movl	%ebx, %edi
	shrl	$20, %edi
	andl	$1023, %edi             # imm = 0x3FF
	testl	$1073741824, %ebx       # imm = 0x40000000
	je	.LBB1_55
# BB#54:                                #   in Loop: Header=BB1_53 Depth=2
	leal	1(%r9), %ecx
	movl	(%r13,%rcx,4), %eax
	movl	%eax, %edx
	shrl	$10, %edx
	andl	$4193280, %edx          # imm = 0x3FFC00
	orl	%edi, %edx
	andl	$1048575, %eax          # imm = 0xFFFFF
	movl	%eax, (%r13,%rcx,4)
	movl	%edx, %edi
.LBB1_55:                               #   in Loop: Header=BB1_53 Depth=2
	movl	%ebx, %eax
	andl	$1048575, %eax          # imm = 0xFFFFF
	leal	1(%rdi), %ecx
	testl	%ebx, %ebx
	movl	%eax, (%r10)
	jns	.LBB1_57
# BB#56:                                #   in Loop: Header=BB1_53 Depth=2
	testl	%edi, %edi
	jne	.LBB1_62
.LBB1_57:                               #   in Loop: Header=BB1_53 Depth=2
	movl	%r9d, %edx
	subl	%ebp, %edx
	andl	$1048575, (%r13,%rdx,4) # imm = 0xFFFFF
	cmpl	$2, %ebp
	jb	.LBB1_59
# BB#58:                                #   in Loop: Header=BB1_53 Depth=2
	incl	%edx
	andl	$1048575, (%r13,%rdx,4) # imm = 0xFFFFF
.LBB1_59:                               #   in Loop: Header=BB1_53 Depth=2
	leal	(%rcx,%rbp), %edx
	addl	%ebp, %edi
	movl	%edi, %ebx
	shll	$20, %ebx
	andl	$1072693248, %ebx       # imm = 0x3FF00000
	movl	%ebp, %ebp
	leaq	(,%rbp,4), %rax
	movq	%r10, %rsi
	subq	%rax, %rsi
	orl	(%rsi), %ebx
	movl	%ebx, (%rsi)
	cmpl	$1025, %edx             # imm = 0x401
	jb	.LBB1_61
# BB#60:                                #   in Loop: Header=BB1_53 Depth=2
	negq	%rbp
	orl	$1073741824, %ebx       # imm = 0x40000000
	movl	%ebx, (%r10,%rbp,4)
	shll	$10, %edi
	andl	$-1048576, %edi         # imm = 0xFFF00000
	orl	%edi, 4(%r10,%rbp,4)
.LBB1_61:                               #   in Loop: Header=BB1_53 Depth=2
	addl	%r9d, %ecx
	cmpl	%r11d, %ecx
	movl	%ecx, %r9d
	movl	%edx, %ebp
	jb	.LBB1_53
	jmp	.LBB1_69
	.p2align	4, 0x90
.LBB1_62:                               # %.preheader205
                                        #   in Loop: Header=BB1_52 Depth=1
	testl	%ecx, %ecx
	je	.LBB1_68
# BB#63:                                # %.lr.ph222.preheader
                                        #   in Loop: Header=BB1_52 Depth=1
	movl	%ecx, %ebx
	leaq	-1(%rbx), %rdi
	movq	%rbx, %rsi
	xorl	%ebp, %ebp
	andq	$3, %rsi
	je	.LBB1_65
	.p2align	4, 0x90
.LBB1_64:                               # %.lr.ph222.prol
                                        #   Parent Loop BB1_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r8,%rbp), %rax
	movl	%eax, %edx
	movl	(%r13,%rdx,4), %edx
	movl	%eax, (%r14,%rdx,4)
	incq	%rbp
	cmpq	%rbp, %rsi
	jne	.LBB1_64
.LBB1_65:                               # %.lr.ph222.prol.loopexit
                                        #   in Loop: Header=BB1_52 Depth=1
	cmpq	$3, %rdi
	jb	.LBB1_68
# BB#66:                                # %.lr.ph222.preheader.new
                                        #   in Loop: Header=BB1_52 Depth=1
	subq	%rbp, %rbx
	leaq	3(%r8,%rbp), %rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_67:                               # %.lr.ph222
                                        #   Parent Loop BB1_52 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-3(%rdx,%rdi), %rax
	movl	%eax, %esi
	movl	(%r13,%rsi,4), %esi
	movl	%eax, (%r14,%rsi,4)
	leaq	-2(%rdx,%rdi), %rax
	movl	%eax, %esi
	movl	(%r13,%rsi,4), %esi
	movl	%eax, (%r14,%rsi,4)
	leaq	-1(%rdx,%rdi), %rax
	movl	%eax, %esi
	movl	(%r13,%rsi,4), %esi
	movl	%eax, (%r14,%rsi,4)
	leaq	(%rdx,%rdi), %rax
	movl	%eax, %esi
	movl	(%r13,%rsi,4), %esi
	movl	%eax, (%r14,%rsi,4)
	addq	$4, %rdi
	cmpq	%rdi, %rbx
	jne	.LBB1_67
.LBB1_68:                               # %.outer
                                        #   in Loop: Header=BB1_52 Depth=1
	addl	%r9d, %ecx
	cmpl	%r11d, %ecx
	jb	.LBB1_52
.LBB1_69:                               # %.preheader
	testl	%r11d, %r11d
	je	.LBB1_74
# BB#70:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_71:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ecx
	movl	(%r13,%rcx,4), %esi
	movl	%esi, %edx
	shrl	$20, %edx
	andl	$1023, %edx             # imm = 0x3FF
	incl	%eax
	testl	$1073741824, %esi       # imm = 0x40000000
	je	.LBB1_73
# BB#72:                                #   in Loop: Header=BB1_71 Depth=1
	movl	%eax, %edi
	movl	(%r13,%rdi,4), %ebx
	movl	%ebx, %ebp
	shrl	$10, %ebp
	andl	$4193280, %ebp          # imm = 0x3FFC00
	orl	%ebp, %edx
	andl	$1048575, %ebx          # imm = 0xFFFFF
	movl	%ebx, (%r13,%rdi,4)
.LBB1_73:                               # %.lr.ph._crit_edge
                                        #   in Loop: Header=BB1_71 Depth=1
	andl	$1048575, %esi          # imm = 0xFFFFF
	movl	%esi, (%r13,%rcx,4)
	addl	%edx, %eax
	cmpl	%r11d, %eax
	jb	.LBB1_71
.LBB1_74:                               # %._crit_edge
	movl	(%r14), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	BlockSort, .Lfunc_end1-BlockSort
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
