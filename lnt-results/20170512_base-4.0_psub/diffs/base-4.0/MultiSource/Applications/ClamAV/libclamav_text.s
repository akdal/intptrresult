	.text
	.file	"libclamav_text.bc"
	.globl	textDestroy
	.p2align	4, 0x90
	.type	textDestroy,@function
textDestroy:                            # @textDestroy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB0_4
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	8(%rbx), %r14
	testq	%rdi, %rdi
	je	.LBB0_3
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	callq	lineUnlink
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	movq	%rbx, %rdi
	callq	free
	testq	%r14, %r14
	movq	%r14, %rbx
	jne	.LBB0_1
.LBB0_4:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	textDestroy, .Lfunc_end0-textDestroy
	.cfi_endproc

	.globl	textAddMessage
	.p2align	4, 0x90
	.type	textAddMessage,@function
textAddMessage:                         # @textAddMessage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	messageGetEncoding
	movq	%rbx, %rdi
	testl	%eax, %eax
	je	.LBB1_8
# BB#1:
	callq	messageToText
	movq	%rax, %rbx
	testq	%r14, %r14
	je	.LBB1_18
# BB#2:
	testq	%rbx, %rbx
	je	.LBB1_39
# BB#3:                                 # %.preheader.i10.preheader
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader.i10
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbp
	movq	8(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB1_4
# BB#5:
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, 8(%rbp)
	testq	%rax, %rax
	je	.LBB1_38
# BB#6:
	movq	(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_31
# BB#7:
	movq	%rcx, (%rax)
	movq	%rbx, %rcx
	jmp	.LBB1_32
.LBB1_8:
	callq	messageGetBody
	movq	%rax, %r12
	testq	%r14, %r14
	je	.LBB1_19
# BB#9:
	testq	%r12, %r12
	je	.LBB1_39
# BB#10:                                # %.preheader.i.preheader
	movl	$-1, %esi
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB1_11:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	movq	8(%rbx), %rax
	incl	%esi
	testq	%rax, %rax
	jne	.LBB1_11
# BB#12:                                # %.lr.ph.preheader.i
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r15
	movq	%r15, 8(%rbx)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_13 Depth=1
	callq	lineLink
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_15:                               #   in Loop: Header=BB1_13 Depth=1
	xorl	%eax, %eax
.LBB1_16:                               #   in Loop: Header=BB1_13 Depth=1
	movq	%rax, (%r15)
	movq	8(%r12), %r12
	testq	%r12, %r12
	movq	%r15, %rbx
	jne	.LBB1_13
# BB#17:                                # %._crit_edge.i
	movq	$0, 8(%r15)
	jmp	.LBB1_39
.LBB1_18:
	movq	%rbx, %r14
	jmp	.LBB1_39
.LBB1_19:
	testq	%r12, %r12
	je	.LBB1_33
# BB#20:                                # %.lr.ph.i.i.preheader
	movb	$1, %bl
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_21:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r14
	testb	$1, %bl
	movq	%r14, %r15
	jne	.LBB1_23
# BB#22:                                #   in Loop: Header=BB1_21 Depth=1
	movq	%r14, 8(%r13)
	movq	%rbp, %r15
.LBB1_23:                               #   in Loop: Header=BB1_21 Depth=1
	testq	%r14, %r14
	je	.LBB1_34
# BB#24:                                #   in Loop: Header=BB1_21 Depth=1
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_21 Depth=1
	callq	lineLink
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_26:                               #   in Loop: Header=BB1_21 Depth=1
	xorl	%eax, %eax
.LBB1_27:                               #   in Loop: Header=BB1_21 Depth=1
	movq	%rax, (%r14)
	movq	8(%r12), %r12
	testq	%r15, %r15
	sete	%bl
	testq	%r12, %r12
	movq	%r14, %r13
	movq	%r15, %rbp
	jne	.LBB1_21
# BB#28:                                # %._crit_edge.i.i
	testq	%r15, %r15
	je	.LBB1_38
# BB#29:
	movq	$0, 8(%r14)
	movq	%r15, %r14
	jmp	.LBB1_39
.LBB1_38:
	xorl	%r14d, %r14d
	jmp	.LBB1_39
.LBB1_31:
	movq	%rax, %rcx
.LBB1_32:
	movq	$0, (%rcx)
	movq	8(%rbx), %rcx
	movq	%rcx, 8(%rax)
	movq	$0, 8(%rbx)
	jmp	.LBB1_39
.LBB1_33:
	xorl	%r14d, %r14d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB1_39
.LBB1_34:
	xorl	%r14d, %r14d
	testq	%r15, %r15
	je	.LBB1_39
	.p2align	4, 0x90
.LBB1_35:                               # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdi
	movq	8(%r15), %rbx
	testq	%rdi, %rdi
	je	.LBB1_37
# BB#36:                                #   in Loop: Header=BB1_35 Depth=1
	callq	lineUnlink
.LBB1_37:                               #   in Loop: Header=BB1_35 Depth=1
	movq	%r15, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %r15
	jne	.LBB1_35
.LBB1_39:                               # %textAdd.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	textAddMessage, .Lfunc_end1-textAddMessage
	.cfi_endproc

	.globl	textMove
	.p2align	4, 0x90
	.type	textMove,@function
textMove:                               # @textMove
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB2_7
# BB#1:
	testq	%r14, %r14
	je	.LBB2_15
# BB#2:                                 # %.preheader.preheader
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB2_3
# BB#4:
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB2_14
# BB#5:
	movq	(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB2_12
# BB#6:
	movq	%rcx, (%rax)
	movq	%r14, %rcx
	jmp	.LBB2_13
.LBB2_7:
	testq	%r14, %r14
	je	.LBB2_11
# BB#8:
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_14
# BB#9:
	movups	(%r14), %xmm0
	movups	%xmm0, (%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	jmp	.LBB2_15
.LBB2_14:
	xorl	%r15d, %r15d
	jmp	.LBB2_15
.LBB2_11:
	xorl	%r15d, %r15d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB2_15
.LBB2_12:
	movq	%rax, %rcx
.LBB2_13:
	movq	$0, (%rcx)
	movq	8(%r14), %rcx
	movq	%rcx, 8(%rax)
	movq	$0, 8(%r14)
.LBB2_15:
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	textMove, .Lfunc_end2-textMove
	.cfi_endproc

	.globl	textToBlob
	.p2align	4, 0x90
	.type	textToBlob,@function
textToBlob:                             # @textToBlob
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 64
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB3_31
# BB#1:                                 # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	callq	lineGetData
	movq	%rax, %rdi
	callq	strlen
	incq	%rax
	jmp	.LBB3_5
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %eax
.LBB3_5:                                # %getLength.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	addq	%rax, %rbp
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_2
# BB#6:                                 # %textIterate.exit
	testq	%rbp, %rbp
	je	.LBB3_13
# BB#7:
	testq	%r12, %r12
	movq	%r12, %r13
	jne	.LBB3_9
# BB#8:
	callq	blobCreate
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB3_31
.LBB3_9:
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	blobGrow
	testl	%eax, %eax
	je	.LBB3_14
# BB#10:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	testq	%r12, %r12
	jne	.LBB3_31
# BB#30:
	movq	%r13, %rdi
	callq	blobDestroy
.LBB3_31:
	xorl	%r13d, %r13d
	jmp	.LBB3_32
.LBB3_13:
	movq	%r12, %r13
	jmp	.LBB3_32
.LBB3_14:
	movq	%r14, %rbx
	testl	%r15d, %r15d
	je	.LBB3_20
	.p2align	4, 0x90
.LBB3_15:                               # %.lr.ph18.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_15 Depth=1
	callq	lineGetData
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	blobAddData
.LBB3_17:                               # %addToBlob.exit23
                                        #   in Loop: Header=BB3_15 Depth=1
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	callq	blobAddData
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_19
# BB#18:                                #   in Loop: Header=BB3_15 Depth=1
	callq	lineUnlink
	movq	$0, (%rbx)
.LBB3_19:                               #   in Loop: Header=BB3_15 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_15
	jmp	.LBB3_23
	.p2align	4, 0x90
.LBB3_20:                               # %.lr.ph.i20
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_20 Depth=1
	callq	lineGetData
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	blobAddData
.LBB3_22:                               # %addToBlob.exit
                                        #   in Loop: Header=BB3_20 Depth=1
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	callq	blobAddData
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_20
.LBB3_23:                               # %textIterate.exit21
	testl	%r15d, %r15d
	je	.LBB3_29
# BB#24:
	movq	8(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB3_29
	.p2align	4, 0x90
.LBB3_25:                               # %.lr.ph.i22
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	8(%rbp), %rbx
	testq	%rdi, %rdi
	je	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_25 Depth=1
	callq	lineUnlink
.LBB3_27:                               #   in Loop: Header=BB3_25 Depth=1
	movq	%rbp, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rbp
	jne	.LBB3_25
# BB#28:                                # %textDestroy.exit
	movq	$0, 8(%r14)
.LBB3_29:
	movq	%r13, %rdi
	callq	blobClose
.LBB3_32:
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	textToBlob, .Lfunc_end3-textToBlob
	.cfi_endproc

	.globl	textToFileblob
	.p2align	4, 0x90
	.type	textToFileblob,@function
textToFileblob:                         # @textToFileblob
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -48
.Lcfi43:
	.cfi_offset %r12, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	%rdi, %r14
	testq	%r12, %r12
	je	.LBB4_1
# BB#3:
	movq	%r12, %rdi
	callq	fileblobGetFilename
	movq	%rax, %rcx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movl	%r15d, %edx
	callq	cli_dbgmsg
	movq	$0, 72(%r12)
	testl	%r15d, %r15d
	jne	.LBB4_5
	jmp	.LBB4_12
.LBB4_1:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	callq	fileblobCreate
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB4_2
# BB#4:
	testl	%r15d, %r15d
	je	.LBB4_12
.LBB4_5:                                # %.preheader14.i
	testq	%r14, %r14
	je	.LBB4_18
# BB#6:
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph18.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=1
	callq	lineGetData
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	fileblobAddData
.LBB4_9:                                # %addToFileblob.exit16
                                        #   in Loop: Header=BB4_7 Depth=1
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	fileblobAddData
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_7 Depth=1
	callq	lineUnlink
	movq	$0, (%rbx)
.LBB4_11:                               #   in Loop: Header=BB4_7 Depth=1
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_7
	jmp	.LBB4_17
.LBB4_12:                               # %.preheader.i
	testq	%r14, %r14
	je	.LBB4_23
# BB#13:
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_16
# BB#15:                                #   in Loop: Header=BB4_14 Depth=1
	callq	lineGetData
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	strlen
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	fileblobAddData
.LBB4_16:                               # %addToFileblob.exit
                                        #   in Loop: Header=BB4_14 Depth=1
	movl	$.L.str.6, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	callq	fileblobAddData
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_14
.LBB4_17:                               # %textIterate.exit
	testl	%r15d, %r15d
	je	.LBB4_23
.LBB4_18:                               # %textIterate.exit.thread17
	movq	8(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB4_23
	.p2align	4, 0x90
.LBB4_19:                               # %.lr.ph.i15
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	8(%rbp), %rbx
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#20:                                #   in Loop: Header=BB4_19 Depth=1
	callq	lineUnlink
.LBB4_21:                               #   in Loop: Header=BB4_19 Depth=1
	movq	%rbp, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rbp
	jne	.LBB4_19
# BB#22:                                # %textDestroy.exit
	movq	$0, 8(%r14)
	jmp	.LBB4_23
.LBB4_2:
	xorl	%r12d, %r12d
.LBB4_23:                               # %textIterate.exit.thread
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	textToFileblob, .Lfunc_end4-textToFileblob
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"textMove fails sanity check\n"
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Couldn't grow the blob: we may be low on memory\n"
	.size	.L.str.1, 49

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"textToFileBlob, destroy = %d\n"
	.size	.L.str.2, 30

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"textToFileBlob to %s, destroy = %d\n"
	.size	.L.str.3, 36

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"textAdd fails sanity check\n"
	.size	.L.str.4, 28

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"textAdd: count = %d\n"
	.size	.L.str.5, 21

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n"
	.size	.L.str.6, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
