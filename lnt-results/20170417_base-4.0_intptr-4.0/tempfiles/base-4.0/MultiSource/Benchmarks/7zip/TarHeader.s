	.text
	.file	"TarHeader.bc"
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"        "
	.size	.L.str, 9

	.type	_ZN8NArchive4NTar11NFileHeader15kCheckSumBlanksE,@object # @_ZN8NArchive4NTar11NFileHeader15kCheckSumBlanksE
	.data
	.globl	_ZN8NArchive4NTar11NFileHeader15kCheckSumBlanksE
	.p2align	3
_ZN8NArchive4NTar11NFileHeader15kCheckSumBlanksE:
	.quad	.L.str
	.size	_ZN8NArchive4NTar11NFileHeader15kCheckSumBlanksE, 8

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"././@LongLink"
	.size	.L.str.1, 14

	.type	_ZN8NArchive4NTar11NFileHeader9kLongLinkE,@object # @_ZN8NArchive4NTar11NFileHeader9kLongLinkE
	.data
	.globl	_ZN8NArchive4NTar11NFileHeader9kLongLinkE
	.p2align	3
_ZN8NArchive4NTar11NFileHeader9kLongLinkE:
	.quad	.L.str.1
	.size	_ZN8NArchive4NTar11NFileHeader9kLongLinkE, 8

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"@LongLink"
	.size	.L.str.2, 10

	.type	_ZN8NArchive4NTar11NFileHeader10kLongLink2E,@object # @_ZN8NArchive4NTar11NFileHeader10kLongLink2E
	.data
	.globl	_ZN8NArchive4NTar11NFileHeader10kLongLink2E
	.p2align	3
_ZN8NArchive4NTar11NFileHeader10kLongLink2E:
	.quad	.L.str.2
	.size	_ZN8NArchive4NTar11NFileHeader10kLongLink2E, 8

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"ustar"
	.size	.L.str.3, 6

	.type	_ZN8NArchive4NTar11NFileHeader6NMagic6kUsTarE,@object # @_ZN8NArchive4NTar11NFileHeader6NMagic6kUsTarE
	.data
	.globl	_ZN8NArchive4NTar11NFileHeader6NMagic6kUsTarE
	.p2align	3
_ZN8NArchive4NTar11NFileHeader6NMagic6kUsTarE:
	.quad	.L.str.3
	.size	_ZN8NArchive4NTar11NFileHeader6NMagic6kUsTarE, 8

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"GNUtar "
	.size	.L.str.4, 8

	.type	_ZN8NArchive4NTar11NFileHeader6NMagic7kGNUTarE,@object # @_ZN8NArchive4NTar11NFileHeader6NMagic7kGNUTarE
	.data
	.globl	_ZN8NArchive4NTar11NFileHeader6NMagic7kGNUTarE
	.p2align	3
_ZN8NArchive4NTar11NFileHeader6NMagic7kGNUTarE:
	.quad	.L.str.4
	.size	_ZN8NArchive4NTar11NFileHeader6NMagic7kGNUTarE, 8

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata,"a",@progbits
.L.str.5:
	.zero	9
	.size	.L.str.5, 9

	.type	_ZN8NArchive4NTar11NFileHeader6NMagic6kEmptyE,@object # @_ZN8NArchive4NTar11NFileHeader6NMagic6kEmptyE
	.data
	.globl	_ZN8NArchive4NTar11NFileHeader6NMagic6kEmptyE
	.p2align	3
_ZN8NArchive4NTar11NFileHeader6NMagic6kEmptyE:
	.quad	.L.str.5
	.size	_ZN8NArchive4NTar11NFileHeader6NMagic6kEmptyE, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
