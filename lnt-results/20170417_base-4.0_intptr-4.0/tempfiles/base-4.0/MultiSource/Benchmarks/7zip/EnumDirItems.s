	.text
	.file	"EnumDirItems.bc"
	.globl	_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE
	.p2align	4, 0x90
	.type	_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE,@function
_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE: # @_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movl	%esi, %r12d
	movl	%edi, %ebp
	leaq	32(%rsp), %r13
	movq	$0, 40(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, 32(%rsp)
	movl	$0, (%rbx)
	movl	$4, 44(%rsp)
	movups	(%r15), %xmm0
	movaps	%xmm0, (%rsp)
	movups	16(%r15), %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	32(%r15), %eax
	movl	%eax, 48(%rsp)
	movl	%ebp, 52(%rsp)
	movl	%r12d, 56(%rsp)
	leaq	40(%r15), %rbp
	cmpq	%r13, %rbp
	je	.LBB0_1
# BB#2:
	movl	$0, 40(%rsp)
	movl	$0, (%rbx)
	movslq	48(%r15), %r12
	incq	%r12
	cmpl	$4, %r12d
	je	.LBB0_5
# BB#3:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp0:
	callq	_Znam
	movq	%rax, %r13
.Ltmp1:
# BB#4:                                 # %._crit_edge16.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	40(%rsp), %rax
	movq	%r13, 32(%rsp)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 44(%rsp)
	movq	%r13, %rbx
.LBB0_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rbp), %rax
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB0_6
# BB#7:                                 # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	48(%r15), %r12d
	movl	%r12d, 40(%rsp)
	jmp	.LBB0_8
.LBB0_1:
	xorl	%r12d, %r12d
.LBB0_8:                                # %_ZN11CStringBaseIwEaSERKS0_.exit
.Ltmp2:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp3:
# BB#9:                                 # %.noexc10
	movaps	(%rsp), %xmm0
	movaps	16(%rsp), %xmm1
	movups	%xmm1, 16(%r15)
	movups	%xmm0, (%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movl	%r12d, %ebp
	incl	%ebp
	je	.LBB0_10
# BB#11:                                # %._crit_edge16.i.i.i.i
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp4:
	callq	_Znam
.Ltmp5:
# BB#12:                                # %.noexc.i
	movq	%rax, 32(%r15)
	movl	$0, (%rax)
	movl	%ebp, 44(%r15)
	jmp	.LBB0_13
.LBB0_10:
	xorl	%eax, %eax
.LBB0_13:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i
	leaq	48(%rsp), %rcx
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB0_14
# BB#15:
	movl	%r12d, 40(%r15)
	movl	8(%rcx), %eax
	movl	%eax, 56(%r15)
	movq	(%rcx), %rax
	movq	%rax, 48(%r15)
.Ltmp7:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp8:
# BB#16:
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movq	%r15, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_18
# BB#17:
	callq	_ZdaPv
.LBB0_18:                               # %_ZN8CDirItemD2Ev.exit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_23:
.Ltmp6:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB0_20
.LBB0_19:
.Ltmp9:
	movq	%rax, %rbx
.LBB0_20:                               # %.body
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_22
# BB#21:
	callq	_ZdaPv
.LBB0_22:                               # %_ZN8CDirItemD2Ev.exit12
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE, .Lfunc_end0-_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp5-.Ltmp4           #   Call between .Ltmp4 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZNK9CDirItems15GetPrefixesPathERK13CRecordVectorIiEiRK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZNK9CDirItems15GetPrefixesPathERK13CRecordVectorIiEiRK11CStringBaseIwE,@function
_ZNK9CDirItems15GetPrefixesPathERK13CRecordVectorIiEiRK11CStringBaseIwE: # @_ZNK9CDirItems15GetPrefixesPathERK13CRecordVectorIiEiRK11CStringBaseIwE
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movl	%ecx, %ebp
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$16, %edi
	callq	_Znam
	movq	%rbx, %r8
	movq	%rax, %rbx
	movq	%rbx, (%r14)
	movl	$0, (%rbx)
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movl	$4, 12(%r14)
	movl	8(%r8), %eax
	testl	%ebp, %ebp
	movl	%eax, %r14d
	js	.LBB2_3
# BB#1:                                 # %.lr.ph51
	movq	16(%r12), %rcx
	movq	16(%r15), %rdx
	movl	%eax, %r14d
	movl	%ebp, %esi
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rsi
	movq	(%rcx,%rsi,8), %rdi
	addl	8(%rdi), %r14d
	movl	(%rdx,%rsi,4), %esi
	testl	%esi, %esi
	jns	.LBB2_2
.LBB2_3:                                # %._crit_edge52
	cmpl	$4, %r14d
	jl	.LBB2_7
# BB#4:
	leal	1(%r14), %r13d
	cmpl	$4, %r13d
	je	.LBB2_7
# BB#5:
	movq	%r8, (%rsp)             # 8-byte Spill
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp10:
	callq	_Znam
	movq	%rax, 16(%rsp)          # 8-byte Spill
.Ltmp11:
# BB#6:                                 # %._crit_edge16.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	(%rsp), %r8             # 8-byte Reload
	movl	8(%r8), %eax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, (%rcx)
	movl	$0, (%rbx)
	movl	%r13d, 12(%rcx)
.LBB2_7:
	movslq	%r14d, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	$0, (%rbx,%rcx,4)
	movl	%r14d, %r13d
	subl	%eax, %r13d
	movslq	%r13d, %rcx
	leaq	(%rbx,%rcx,4), %rdi
	movq	(%r8), %rsi
	movslq	%eax, %rdx
	shlq	$2, %rdx
	callq	memcpy
	testl	%ebp, %ebp
	js	.LBB2_10
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	movslq	%ebp, %rbp
	movq	(%rax,%rbp,8), %rax
	movslq	8(%rax), %rdx
	movslq	%r13d, %r13
	subq	%rdx, %r13
	leaq	(%rbx,%r13,4), %rdi
	movq	(%rax), %rsi
	shlq	$2, %rdx
	callq	memcpy
	movq	16(%r15), %rax
	movl	(%rax,%rbp,4), %ebp
	testl	%ebp, %ebp
	jns	.LBB2_8
.LBB2_10:                               # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, (%rcx,%rdx,4)
	movl	%r14d, 8(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_11:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp12:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZNK9CDirItems15GetPrefixesPathERK13CRecordVectorIiEiRK11CStringBaseIwE, .Lfunc_end2-_ZNK9CDirItems15GetPrefixesPathERK13CRecordVectorIiEiRK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp10-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp11     #   Call between .Ltmp11 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZNK9CDirItems10GetPhyPathEi
	.p2align	4, 0x90
	.type	_ZNK9CDirItems10GetPhyPathEi,@function
_ZNK9CDirItems10GetPhyPathEi:           # @_ZNK9CDirItems10GetPhyPathEi
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 16
.Lcfi27:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	112(%rsi), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %r8
	movl	52(%r8), %ecx
	addq	$32, %r8
	leaq	32(%rsi), %rdx
	callq	_ZNK9CDirItems15GetPrefixesPathERK13CRecordVectorIiEiRK11CStringBaseIwE
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZNK9CDirItems10GetPhyPathEi, .Lfunc_end3-_ZNK9CDirItems10GetPhyPathEi
	.cfi_endproc

	.globl	_ZNK9CDirItems10GetLogPathEi
	.p2align	4, 0x90
	.type	_ZNK9CDirItems10GetLogPathEi,@function
_ZNK9CDirItems10GetLogPathEi:           # @_ZNK9CDirItems10GetLogPathEi
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	112(%rsi), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %r8
	movl	56(%r8), %ecx
	addq	$32, %r8
	leaq	64(%rsi), %rdx
	callq	_ZNK9CDirItems15GetPrefixesPathERK13CRecordVectorIiEiRK11CStringBaseIwE
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZNK9CDirItems10GetLogPathEi, .Lfunc_end4-_ZNK9CDirItems10GetLogPathEi
	.cfi_endproc

	.globl	_ZN9CDirItems11ReserveDownEv
	.p2align	4, 0x90
	.type	_ZN9CDirItems11ReserveDownEv,@function
_ZN9CDirItems11ReserveDownEv:           # @_ZN9CDirItems11ReserveDownEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	32(%rbx), %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	64(%rbx), %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	addq	$96, %rbx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN17CBaseRecordVector11ReserveDownEv # TAILCALL
.Lfunc_end5:
	.size	_ZN9CDirItems11ReserveDownEv, .Lfunc_end5-_ZN9CDirItems11ReserveDownEv
	.cfi_endproc

	.globl	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE,@function
_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE: # @_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	32(%rbx), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	48(%rbx), %rax
	movslq	44(%rbx), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	44(%rbx)
	leaq	64(%rbx), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	80(%rbx), %rax
	movslq	76(%rbx), %rcx
	movl	%r14d, (%rax,%rcx,4)
	incl	76(%rbx)
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movslq	8(%r15), %rbp
	leaq	1(%rbp), %r12
	testl	%r12d, %r12d
	je	.LBB6_1
# BB#2:                                 # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp13:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp14:
# BB#3:                                 # %.noexc.i
	movq	%rcx, (%r14)
	movl	$0, (%rcx)
	movl	%r12d, 12(%r14)
	jmp	.LBB6_4
.LBB6_1:
	xorl	%ecx, %ecx
.LBB6_4:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	(%r15), %rsi
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB6_5
# BB#6:                                 # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
	movl	%ebp, 8(%r14)
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%rbx), %rcx
	movslq	12(%rbx), %rax
	movq	%r14, (%rcx,%rax,8)
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rbx)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_7:
.Ltmp15:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE, .Lfunc_end6-_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp13-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin2   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end6-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9CDirItems16DeleteLastPrefixEv
	.p2align	4, 0x90
	.type	_ZN9CDirItems16DeleteLastPrefixEv,@function
_ZN9CDirItems16DeleteLastPrefixEv:      # @_ZN9CDirItems16DeleteLastPrefixEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	32(%rbx), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	leaq	64(%rbx), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN17CBaseRecordVector10DeleteBackEv # TAILCALL
.Lfunc_end7:
	.size	_ZN9CDirItems16DeleteLastPrefixEv, .Lfunc_end7-_ZN9CDirItems16DeleteLastPrefixEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.zero	16
	.text
	.globl	_ZN9CDirItems18EnumerateDirectoryEiiRK11CStringBaseIwER13CObjectVectorIS1_ER13CRecordVectorIjE
	.p2align	4, 0x90
	.type	_ZN9CDirItems18EnumerateDirectoryEiiRK11CStringBaseIwER13CObjectVectorIS1_ER13CRecordVectorIjE,@function
_ZN9CDirItems18EnumerateDirectoryEiiRK11CStringBaseIwER13CObjectVectorIS1_ER13CRecordVectorIjE: # @_ZN9CDirItems18EnumerateDirectoryEiiRK11CStringBaseIwER13CObjectVectorIS1_ER13CRecordVectorIjE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 288
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movq	%r8, %r13
	movl	%edx, 76(%rsp)          # 4-byte Spill
	movl	%esi, 72(%rsp)          # 4-byte Spill
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	8(%rcx), %ebx
	movslq	%ebx, %r14
	leaq	1(%r14), %r12
	testl	%r12d, %r12d
	je	.LBB8_1
# BB#2:                                 # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
	movq	%rbp, 16(%rsp)
	movl	$0, (%rbp)
	movl	%r12d, 28(%rsp)
	movl	%r12d, %r15d
	jmp	.LBB8_3
.LBB8_1:
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
.LBB8_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB8_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%ebx, 24(%rsp)
	movl	%r15d, %eax
	subl	%ebx, %eax
	cmpl	$1, %eax
	movq	%r13, 112(%rsp)         # 8-byte Spill
	jle	.LBB8_7
# BB#6:
	movq	80(%rsp), %r15          # 8-byte Reload
	jmp	.LBB8_30
.LBB8_7:
	leal	-1(%rax), %ecx
	cmpl	$8, %r15d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r15d
	jl	.LBB8_9
# BB#8:                                 # %select.true.sink
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	sarl	%edx
.LBB8_9:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r15,%rsi), %eax
	cmpl	%r15d, %eax
	jne	.LBB8_11
# BB#10:
	movq	80(%rsp), %r15          # 8-byte Reload
	jmp	.LBB8_30
.LBB8_11:
	movl	%eax, 88(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp16:
	callq	_Znam
	movq	%rax, %r13
.Ltmp17:
# BB#12:                                # %.noexc66
	testl	%r15d, %r15d
	movq	80(%rsp), %r15          # 8-byte Reload
	jle	.LBB8_29
# BB#13:                                # %.preheader.i.i
	testl	%ebx, %ebx
	jle	.LBB8_27
# BB#14:                                # %.lr.ph.i.i
	cmpl	$7, %ebx
	jbe	.LBB8_15
# BB#18:                                # %min.iters.checked
	movq	%r14, %rax
	andq	$-8, %rax
	je	.LBB8_15
# BB#19:                                # %vector.body.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB8_20
# BB#21:                                # %vector.body.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_22:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbp,%rsi,4), %xmm0
	movups	16(%rbp,%rsi,4), %xmm1
	movups	%xmm0, (%r13,%rsi,4)
	movups	%xmm1, 16(%r13,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB8_22
	jmp	.LBB8_23
.LBB8_15:
	xorl	%eax, %eax
.LBB8_16:                               # %scalar.ph.preheader
	movq	%r14, %rcx
	subq	%rax, %rcx
	leaq	(%r13,%rax,4), %rdx
	leaq	(%rbp,%rax,4), %rax
	.p2align	4, 0x90
.LBB8_17:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$4, %rdx
	addq	$4, %rax
	decq	%rcx
	jne	.LBB8_17
	jmp	.LBB8_28
.LBB8_27:                               # %._crit_edge.i.i
	testq	%rbp, %rbp
	jne	.LBB8_28
	jmp	.LBB8_29
.LBB8_20:
	xorl	%esi, %esi
.LBB8_23:                               # %vector.body.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB8_26
# BB#24:                                # %vector.body.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r13,%rsi,4), %rdx
	leaq	112(%rbp,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB8_25:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB8_25
.LBB8_26:                               # %middle.block
	cmpq	%rax, %r14
	jne	.LBB8_16
.LBB8_28:                               # %._crit_edge.thread.i.i
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB8_29:                               # %._crit_edge16.i.i
	movq	%r13, 16(%rsp)
	movl	$0, (%r13,%r14,4)
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	%eax, 28(%rsp)
	movq	%r13, %rbp
	movq	112(%rsp), %r13         # 8-byte Reload
.LBB8_30:                               # %_ZplIwE11CStringBaseIT_ERKS2_S1_.exit
	movl	$42, (%rbp,%r14,4)
	movl	%r12d, 24(%rsp)
	movslq	%r12d, %rax
	movl	$0, (%rbp,%rax,4)
.Ltmp19:
	leaq	120(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE
.Ltmp20:
# BB#31:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_33
# BB#32:
	callq	_ZdaPv
.LBB8_33:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	96(%rax), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jmp	.LBB8_34
.LBB8_69:                               #   in Loop: Header=BB8_34 Depth=1
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
.LBB8_72:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i32
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	216(%rsp), %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB8_73:                               #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_73
# BB#74:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i35
                                        #   in Loop: Header=BB8_34 Depth=1
	movl	%r12d, 56(%rsp)
	movl	%r14d, %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	jg	.LBB8_97
# BB#75:                                #   in Loop: Header=BB8_34 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %r14d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r14d
	jl	.LBB8_77
# BB#76:                                # %select.true.sink486
                                        #   in Loop: Header=BB8_34 Depth=1
	movl	%r14d, %edx
	shrl	$31, %edx
	addl	%r14d, %edx
	sarl	%edx
.LBB8_77:                               # %select.end485
                                        #   in Loop: Header=BB8_34 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r14,%rsi), %eax
	cmpl	%r14d, %eax
	je	.LBB8_97
# BB#78:                                #   in Loop: Header=BB8_34 Depth=1
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	%eax, 108(%rsp)         # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp41:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp42:
# BB#79:                                # %.noexc79
                                        #   in Loop: Header=BB8_34 Depth=1
	testl	%r14d, %r14d
	jle	.LBB8_96
# BB#80:                                # %.preheader.i.i70
                                        #   in Loop: Header=BB8_34 Depth=1
	testl	%r12d, %r12d
	jle	.LBB8_94
# BB#81:                                # %.lr.ph.i.i71
                                        #   in Loop: Header=BB8_34 Depth=1
	cmpl	$7, %r12d
	jbe	.LBB8_82
# BB#85:                                # %min.iters.checked181
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	andq	$-8, %rax
	je	.LBB8_82
# BB#86:                                # %vector.body177.preheader
                                        #   in Loop: Header=BB8_34 Depth=1
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB8_87
# BB#88:                                # %vector.body177.prol.preheader
                                        #   in Loop: Header=BB8_34 Depth=1
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_89:                               # %vector.body177.prol
                                        #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r15,%rsi,4), %xmm0
	movups	16(%r15,%rsi,4), %xmm1
	movups	%xmm0, (%rbp,%rsi,4)
	movups	%xmm1, 16(%rbp,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB8_89
	jmp	.LBB8_90
.LBB8_82:                               #   in Loop: Header=BB8_34 Depth=1
	xorl	%eax, %eax
.LBB8_83:                               # %scalar.ph179.preheader
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	leaq	(%rbp,%rax,4), %rdx
	leaq	(%r15,%rax,4), %rax
	.p2align	4, 0x90
.LBB8_84:                               # %scalar.ph179
                                        #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$4, %rdx
	addq	$4, %rax
	decq	%rcx
	jne	.LBB8_84
	jmp	.LBB8_95
.LBB8_94:                               # %._crit_edge.i.i72
                                        #   in Loop: Header=BB8_34 Depth=1
	testq	%r15, %r15
	jne	.LBB8_95
	jmp	.LBB8_96
.LBB8_87:                               #   in Loop: Header=BB8_34 Depth=1
	xorl	%esi, %esi
.LBB8_90:                               # %vector.body177.prol.loopexit
                                        #   in Loop: Header=BB8_34 Depth=1
	cmpq	$24, %rcx
	jb	.LBB8_93
# BB#91:                                # %vector.body177.preheader.new
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbp,%rsi,4), %rdx
	leaq	112(%r15,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB8_92:                               # %vector.body177
                                        #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB8_92
.LBB8_93:                               # %middle.block178
                                        #   in Loop: Header=BB8_34 Depth=1
	cmpq	%rax, 40(%rsp)          # 8-byte Folded Reload
	jne	.LBB8_83
.LBB8_95:                               # %._crit_edge.thread.i.i77
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB8_96:                               # %._crit_edge16.i.i78
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%rbp, 48(%rsp)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rbp,%rax,4)
	movq	%rax, %rbp
	movl	108(%rsp), %eax         # 4-byte Reload
	movl	%eax, 60(%rsp)
.LBB8_97:                               #   in Loop: Header=BB8_34 Depth=1
	movq	48(%rsp), %rbx
	movl	$47, (%rbx,%rbp,4)
	movl	%r13d, 56(%rsp)
	movslq	%r13d, %rax
	movl	$0, (%rbx,%rax,4)
.Ltmp44:
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	72(%rsp), %esi          # 4-byte Reload
	movl	76(%rsp), %edx          # 4-byte Reload
	leaq	48(%rsp), %rcx
	callq	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
	movl	%eax, %r13d
.Ltmp45:
# BB#98:                                #   in Loop: Header=BB8_34 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	8(%rax), %r14d
	movslq	%r14d, %r12
	leaq	1(%r12), %rbp
	testl	%ebp, %ebp
	je	.LBB8_99
# BB#101:                               # %._crit_edge16.i.i.i41
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp47:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp48:
# BB#102:                               # %.noexc48
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%r15, 16(%rsp)
	movl	$0, (%r15)
	movl	%ebp, 28(%rsp)
	jmp	.LBB8_103
.LBB8_99:                               #   in Loop: Header=BB8_34 Depth=1
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
.LBB8_103:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i42
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB8_104:                              #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB8_104
# BB#105:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i45
                                        #   in Loop: Header=BB8_34 Depth=1
	movl	%r14d, 24(%rsp)
	movl	56(%rsp), %eax
	movl	%ebp, %ecx
	subl	%r14d, %ecx
	cmpl	%eax, %ecx
	jg	.LBB8_134
# BB#106:                               #   in Loop: Header=BB8_34 Depth=1
	decl	%ecx
	cmpl	$8, %ebp
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	jl	.LBB8_108
# BB#107:                               # %select.true.sink525
                                        #   in Loop: Header=BB8_34 Depth=1
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB8_108:                              # %select.end524
                                        #   in Loop: Header=BB8_34 Depth=1
	leal	(%rdx,%rcx), %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	cmpl	%eax, %esi
	cmovgel	%edx, %edi
	leal	1(%rbp,%rdi), %eax
	cmpl	%ebp, %eax
	je	.LBB8_134
# BB#109:                               #   in Loop: Header=BB8_34 Depth=1
	movl	%eax, 40(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp50:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp51:
# BB#110:                               # %.noexc93
                                        #   in Loop: Header=BB8_34 Depth=1
	testl	%ebp, %ebp
	jle	.LBB8_133
# BB#111:                               # %.preheader.i.i84
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	16(%rsp), %rdi
	testl	%r14d, %r14d
	jle	.LBB8_131
# BB#112:                               # %.lr.ph.i.i85
                                        #   in Loop: Header=BB8_34 Depth=1
	cmpl	$7, %r14d
	jbe	.LBB8_113
# BB#120:                               # %min.iters.checked159
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%r12, %rax
	andq	$-8, %rax
	je	.LBB8_113
# BB#121:                               # %vector.memcheck
                                        #   in Loop: Header=BB8_34 Depth=1
	leaq	(%rdi,%r12,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB8_123
# BB#122:                               # %vector.memcheck
                                        #   in Loop: Header=BB8_34 Depth=1
	leaq	(%rbx,%r12,4), %rcx
	cmpq	%rcx, %rdi
	jae	.LBB8_123
.LBB8_113:                              #   in Loop: Header=BB8_34 Depth=1
	xorl	%eax, %eax
.LBB8_114:                              # %scalar.ph157.preheader
                                        #   in Loop: Header=BB8_34 Depth=1
	movl	%r14d, %edx
	subl	%eax, %edx
	leaq	-1(%r12), %rcx
	subq	%rax, %rcx
	andq	$7, %rdx
	je	.LBB8_117
# BB#115:                               # %scalar.ph157.prol.preheader
                                        #   in Loop: Header=BB8_34 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB8_116:                              # %scalar.ph157.prol
                                        #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax,4), %esi
	movl	%esi, (%rbx,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB8_116
.LBB8_117:                              # %scalar.ph157.prol.loopexit
                                        #   in Loop: Header=BB8_34 Depth=1
	cmpq	$7, %rcx
	jb	.LBB8_132
# BB#118:                               # %scalar.ph157.preheader.new
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%r12, %rcx
	subq	%rax, %rcx
	leaq	28(%rbx,%rax,4), %rdx
	leaq	28(%rdi,%rax,4), %rax
	.p2align	4, 0x90
.LBB8_119:                              # %scalar.ph157
                                        #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rax), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rax), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rax), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rax), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rax
	addq	$-8, %rcx
	jne	.LBB8_119
	jmp	.LBB8_132
.LBB8_131:                              # %._crit_edge.i.i86
                                        #   in Loop: Header=BB8_34 Depth=1
	testq	%rdi, %rdi
	je	.LBB8_133
.LBB8_132:                              # %._crit_edge.thread.i.i91
                                        #   in Loop: Header=BB8_34 Depth=1
	callq	_ZdaPv
.LBB8_133:                              # %._crit_edge16.i.i92
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%rbx, 16(%rsp)
	movl	$0, (%rbx,%r12,4)
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%eax, 28(%rsp)
.LBB8_134:                              # %.noexc.i46
                                        #   in Loop: Header=BB8_34 Depth=1
	shlq	$2, %r12
	addq	16(%rsp), %r12
	movq	48(%rsp), %rbx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_135:                              #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rcx), %eax
	movl	%eax, (%r12,%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB8_135
# BB#136:                               #   in Loop: Header=BB8_34 Depth=1
	addl	56(%rsp), %r14d
	movl	%r14d, 24(%rsp)
.Ltmp53:
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	%r13d, %esi
	movl	%r13d, %edx
	leaq	16(%rsp), %rcx
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	%r13, %r8
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	%r15, %r9
	callq	_ZN9CDirItems18EnumerateDirectoryEiiRK11CStringBaseIwER13CObjectVectorIS1_ER13CRecordVectorIjE
.Ltmp54:
# BB#137:                               #   in Loop: Header=BB8_34 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_139
# BB#138:                               #   in Loop: Header=BB8_34 Depth=1
	callq	_ZdaPv
.LBB8_139:                              # %_ZN11CStringBaseIwED2Ev.exit51
                                        #   in Loop: Header=BB8_34 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_141
# BB#140:                               #   in Loop: Header=BB8_34 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB8_141:                              # %_ZN11CStringBaseIwED2Ev.exit52
                                        #   in Loop: Header=BB8_34 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB8_52
.LBB8_123:                              # %vector.body155.preheader
                                        #   in Loop: Header=BB8_34 Depth=1
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB8_124
# BB#125:                               # %vector.body155.prol.preheader
                                        #   in Loop: Header=BB8_34 Depth=1
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_126:                              # %vector.body155.prol
                                        #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rsi,4), %xmm0
	movups	16(%rdi,%rsi,4), %xmm1
	movups	%xmm0, (%rbx,%rsi,4)
	movups	%xmm1, 16(%rbx,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB8_126
	jmp	.LBB8_127
.LBB8_124:                              #   in Loop: Header=BB8_34 Depth=1
	xorl	%esi, %esi
.LBB8_127:                              # %vector.body155.prol.loopexit
                                        #   in Loop: Header=BB8_34 Depth=1
	cmpq	$24, %rcx
	jb	.LBB8_130
# BB#128:                               # %vector.body155.preheader.new
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi,4), %rdx
	leaq	112(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB8_129:                              # %vector.body155
                                        #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB8_129
.LBB8_130:                              # %middle.block156
                                        #   in Loop: Header=BB8_34 Depth=1
	cmpq	%rax, %r12
	jne	.LBB8_114
	jmp	.LBB8_132
	.p2align	4, 0x90
.LBB8_34:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_48 Depth 2
                                        #     Child Loop BB8_73 Depth 2
                                        #     Child Loop BB8_89 Depth 2
                                        #     Child Loop BB8_92 Depth 2
                                        #     Child Loop BB8_84 Depth 2
                                        #     Child Loop BB8_104 Depth 2
                                        #     Child Loop BB8_126 Depth 2
                                        #     Child Loop BB8_129 Depth 2
                                        #     Child Loop BB8_116 Depth 2
                                        #     Child Loop BB8_119 Depth 2
                                        #     Child Loop BB8_135 Depth 2
	leaq	216(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.Ltmp22:
	movl	$16, %edi
	callq	_Znam
.Ltmp23:
# BB#35:                                #   in Loop: Header=BB8_34 Depth=1
	movq	%rax, 216(%rsp)
	movl	$0, (%rax)
	movl	$4, 228(%rsp)
.Ltmp25:
	leaq	120(%rsp), %rdi
	leaq	176(%rsp), %rsi
	leaq	15(%rsp), %rdx
	callq	_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWERb
.Ltmp26:
# BB#36:                                #   in Loop: Header=BB8_34 Depth=1
	testb	%al, %al
	je	.LBB8_37
# BB#65:                                #   in Loop: Header=BB8_34 Depth=1
	cmpb	$0, 15(%rsp)
	je	.LBB8_51
# BB#66:                                #   in Loop: Header=BB8_34 Depth=1
.Ltmp36:
	movl	72(%rsp), %edi          # 4-byte Reload
	movl	76(%rsp), %esi          # 4-byte Reload
	leaq	176(%rsp), %rdx
	movq	88(%rsp), %rcx          # 8-byte Reload
	callq	_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE
.Ltmp37:
# BB#67:                                #   in Loop: Header=BB8_34 Depth=1
	xorl	%r14d, %r14d
	testb	$16, 208(%rsp)
	je	.LBB8_52
# BB#68:                                #   in Loop: Header=BB8_34 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movl	224(%rsp), %r12d
	movslq	%r12d, %rbp
	leaq	1(%rbp), %r13
	testl	%r13d, %r13d
	je	.LBB8_69
# BB#70:                                # %._crit_edge16.i.i.i31
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%r13, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp38:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp39:
# BB#71:                                # %.noexc37
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%rbx, 48(%rsp)
	movl	$0, (%rbx)
	movl	%r13d, 60(%rsp)
	movq	%rbx, %r15
	movl	%r13d, %r14d
	jmp	.LBB8_72
	.p2align	4, 0x90
.LBB8_37:                               #   in Loop: Header=BB8_34 Depth=1
	callq	__errno_location
	movl	(%rax), %ebx
.Ltmp27:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp28:
# BB#38:                                #   in Loop: Header=BB8_34 Depth=1
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	12(%r15)
.Ltmp29:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp30:
# BB#39:                                # %.noexc
                                        #   in Loop: Header=BB8_34 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	64(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB8_40
# BB#45:                                # %._crit_edge16.i.i.i25
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp31:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp32:
# BB#46:                                # %.noexc.i
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB8_47
.LBB8_40:                               #   in Loop: Header=BB8_34 Depth=1
	xorl	%eax, %eax
.LBB8_47:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i26
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	.p2align	4, 0x90
.LBB8_48:                               #   Parent Loop BB8_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB8_48
# BB#49:                                #   in Loop: Header=BB8_34 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp34:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp35:
# BB#50:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
                                        #   in Loop: Header=BB8_34 Depth=1
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r13)
.LBB8_51:                               #   in Loop: Header=BB8_34 Depth=1
	movb	$1, %r14b
.LBB8_52:                               #   in Loop: Header=BB8_34 Depth=1
	movq	216(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_54
# BB#53:                                #   in Loop: Header=BB8_34 Depth=1
	callq	_ZdaPv
.LBB8_54:                               # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit
                                        #   in Loop: Header=BB8_34 Depth=1
	testb	%r14b, %r14b
	je	.LBB8_34
# BB#55:
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_57
# BB#56:
	callq	_ZdaPv
.LBB8_57:                               # %_ZN11CStringBaseIwED2Ev.exit.i58
.Ltmp59:
	leaq	120(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
.Ltmp60:
# BB#58:
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_60
# BB#59:
	callq	_ZdaPv
.LBB8_60:                               # %_ZN11CStringBaseIcED2Ev.exit.i.i
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_62
# BB#61:
	callq	_ZdaPv
.LBB8_62:                               # %_ZN8NWindows5NFile5NFind12CEnumeratorWD2Ev.exit
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_41:
.Ltmp18:
	movq	%rax, %r14
	testq	%rbp, %rbp
	je	.LBB8_44
# BB#42:
	movq	%rbp, %rdi
	jmp	.LBB8_43
.LBB8_153:
.Ltmp61:
	movq	%rax, %r14
	jmp	.LBB8_154
.LBB8_63:
.Ltmp21:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	jmp	.LBB8_157
.LBB8_142:
.Ltmp52:
	movq	%rax, %r14
	testq	%r15, %r15
	je	.LBB8_148
# BB#143:
	movq	%r15, %rdi
	jmp	.LBB8_147
.LBB8_100:
.Ltmp43:
	movq	%rax, %r14
	testq	%r15, %r15
	jne	.LBB8_150
	jmp	.LBB8_151
.LBB8_145:
.Ltmp49:
	movq	%rax, %r14
	jmp	.LBB8_148
.LBB8_146:
.Ltmp55:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_148
.LBB8_147:
	callq	_ZdaPv
.LBB8_148:                              # %_ZN11CStringBaseIwED2Ev.exit53
	movq	48(%rsp), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_150
	jmp	.LBB8_151
.LBB8_144:
.Ltmp46:
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB8_151
.LBB8_150:
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB8_151
.LBB8_167:
.Ltmp33:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB8_151
.LBB8_158:
.Ltmp24:
	movq	%rax, %r14
	jmp	.LBB8_159
.LBB8_64:
.Ltmp40:
	movq	%rax, %r14
.LBB8_151:
	movq	216(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_159
# BB#152:
	callq	_ZdaPv
.LBB8_159:
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_161
# BB#160:
	callq	_ZdaPv
.LBB8_161:                              # %_ZN11CStringBaseIwED2Ev.exit.i59
.Ltmp56:
	leaq	120(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
.Ltmp57:
.LBB8_154:
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_156
# BB#155:
	callq	_ZdaPv
.LBB8_156:                              # %_ZN11CStringBaseIcED2Ev.exit3.i.i
	movq	128(%rsp), %rdi
.LBB8_157:                              # %_ZN11CStringBaseIcED2Ev.exit3.i.i
	testq	%rdi, %rdi
	je	.LBB8_44
.LBB8_43:                               # %unwind_resume
	callq	_ZdaPv
.LBB8_44:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_162:
.Ltmp58:
	movq	%rax, %rbx
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_164
# BB#163:
	callq	_ZdaPv
.LBB8_164:                              # %_ZN11CStringBaseIcED2Ev.exit3.i.i61
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_166
# BB#165:
	callq	_ZdaPv
.LBB8_166:                              # %.body63
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN9CDirItems18EnumerateDirectoryEiiRK11CStringBaseIwER13CObjectVectorIS1_ER13CRecordVectorIjE, .Lfunc_end8-_ZN9CDirItems18EnumerateDirectoryEiiRK11CStringBaseIwER13CObjectVectorIS1_ER13CRecordVectorIjE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\314\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp16-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin3   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin3   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin3   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin3   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin3   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin3   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin3   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin3   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp30-.Ltmp25         #   Call between .Ltmp25 and .Ltmp30
	.long	.Ltmp40-.Lfunc_begin3   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin3   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin3   # >> Call Site 12 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp40-.Lfunc_begin3   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin3   # >> Call Site 13 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin3   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin3   # >> Call Site 14 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin3   #     jumps to .Ltmp58
	.byte	1                       #   On action: 1
	.long	.Ltmp57-.Lfunc_begin3   # >> Call Site 15 <<
	.long	.Lfunc_end8-.Ltmp57     #   Call between .Ltmp57 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.zero	16
	.section	.text._ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE,"axG",@progbits,_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE,comdat
	.weak	_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE,@function
_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE: # @_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 48
.Lcfi62:
	.cfi_offset %rbx, -40
.Lcfi63:
	.cfi_offset %r12, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
	movq	%r15, 8(%rbx)
	movb	$0, (%r15)
	movl	$4, 20(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
.Ltmp62:
	movl	$4, %edi
	callq	_Znam
.Ltmp63:
# BB#1:                                 # %_ZN8NWindows5NFile5NFind9CFindFileC2Ev.exit
	movq	%rax, 24(%rbx)
	movb	$0, (%rax)
	movl	$4, 36(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
	movslq	8(%r14), %r12
	leaq	1(%r12), %r15
	testl	%r15d, %r15d
	je	.LBB9_2
# BB#3:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp65:
	callq	_Znam
.Ltmp66:
# BB#4:                                 # %.noexc
	movq	%rax, 40(%rbx)
	movl	$0, (%rax)
	movl	%r15d, 52(%rbx)
	jmp	.LBB9_5
.LBB9_2:
	xorl	%eax, %eax
.LBB9_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB9_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB9_6
# BB#7:
	movl	%r12d, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB9_8:
.Ltmp67:
	movq	%rax, %r14
.Ltmp68:
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
.Ltmp69:
# BB#9:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_11
# BB#10:
	callq	_ZdaPv
.LBB9_11:                               # %_ZN11CStringBaseIcED2Ev.exit.i3
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB9_18
	jmp	.LBB9_19
.LBB9_12:
.Ltmp70:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_14
# BB#13:
	callq	_ZdaPv
.LBB9_14:                               # %_ZN11CStringBaseIcED2Ev.exit3.i
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_16
# BB#15:
	callq	_ZdaPv
.LBB9_16:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB9_17:                               # %_ZN11CStringBaseIcED2Ev.exit.i
.Ltmp64:
	movq	%rax, %r14
	movq	%r15, %rdi
.LBB9_18:                               # %unwind_resume
	callq	_ZdaPv
.LBB9_19:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE, .Lfunc_end9-_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp62-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin4   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin4   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin4   #     jumps to .Ltmp70
	.byte	1                       #   On action: 1
	.long	.Ltmp69-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Lfunc_end9-.Ltmp69     #   Call between .Ltmp69 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZplIwE11CStringBaseIT_ERKS2_S4_,"axG",@progbits,_ZplIwE11CStringBaseIT_ERKS2_S4_,comdat
	.weak	_ZplIwE11CStringBaseIT_ERKS2_S4_
	.p2align	4, 0x90
	.type	_ZplIwE11CStringBaseIT_ERKS2_S4_,@function
_ZplIwE11CStringBaseIT_ERKS2_S4_:       # @_ZplIwE11CStringBaseIT_ERKS2_S4_
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 48
.Lcfi71:
	.cfi_offset %rbx, -48
.Lcfi72:
	.cfi_offset %r12, -40
.Lcfi73:
	.cfi_offset %r13, -32
.Lcfi74:
	.cfi_offset %r14, -24
.Lcfi75:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%r15), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB10_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r12d, 12(%rbx)
	jmp	.LBB10_3
.LBB10_1:
	xorl	%eax, %eax
.LBB10_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB10_4:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB10_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r13d, 8(%rbx)
	movl	8(%r14), %esi
.Ltmp71:
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp72:
# BB#6:                                 # %.noexc
	movslq	8(%rbx), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%rbx), %rcx
	movq	(%r14), %rsi
	.p2align	4, 0x90
.LBB10_7:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	addq	$4, %rsi
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB10_7
# BB#8:
	movl	8(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB10_9:
.Ltmp73:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_11
# BB#10:
	callq	_ZdaPv
.LBB10_11:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZplIwE11CStringBaseIT_ERKS2_S4_, .Lfunc_end10-_ZplIwE11CStringBaseIT_ERKS2_S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp71-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp71
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin5   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Lfunc_end10-.Ltmp72    #   Call between .Ltmp72 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.zero	16
	.text
	.globl	_ZN9CDirItems18EnumerateDirItems2ERK11CStringBaseIwES3_RK13CObjectVectorIS1_ERS5_R13CRecordVectorIjE
	.p2align	4, 0x90
	.type	_ZN9CDirItems18EnumerateDirItems2ERK11CStringBaseIwES3_RK13CObjectVectorIS1_ERS5_R13CRecordVectorIjE,@function
_ZN9CDirItems18EnumerateDirItems2ERK11CStringBaseIwES3_RK13CObjectVectorIS1_ERS5_R13CRecordVectorIjE: # @_ZN9CDirItems18EnumerateDirItems2ERK11CStringBaseIwES3_RK13CObjectVectorIS1_ERS5_R13CRecordVectorIjE
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi82:
	.cfi_def_cfa_offset 288
.Lcfi83:
	.cfi_offset %rbx, -56
.Lcfi84:
	.cfi_offset %r12, -48
.Lcfi85:
	.cfi_offset %r13, -40
.Lcfi86:
	.cfi_offset %r14, -32
.Lcfi87:
	.cfi_offset %r15, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r8, 120(%rsp)          # 8-byte Spill
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	$-1, 12(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 8(%r15)
	movl	$-1, 44(%rsp)           # 4-byte Folded Spill
	je	.LBB11_2
# BB#1:
	movl	$-1, %esi
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%r15, %rcx
	callq	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
	movl	%eax, 44(%rsp)          # 4-byte Spill
.LBB11_2:
	cmpl	$0, 8(%rbx)
	je	.LBB11_4
# BB#3:
	movl	$-1, %esi
	movl	$-1, %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	callq	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
	movl	%eax, 12(%rsp)          # 4-byte Spill
.LBB11_4:
	cmpl	$0, 12(%rbp)
	jle	.LBB11_139
# BB#5:                                 # %.lr.ph
	leaq	96(%r12), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%r12, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%r15, 104(%rsp)         # 8-byte Spill
	jmp	.LBB11_14
.LBB11_6:                               # %vector.body.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB11_9
# BB#7:                                 # %vector.body.prol.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_8:                               # %vector.body.prol
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%rbp,%rbx,4)
	movups	%xmm1, 16(%rbp,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB11_8
	jmp	.LBB11_10
.LBB11_9:                               #   in Loop: Header=BB11_14 Depth=1
	xorl	%ebx, %ebx
.LBB11_10:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB11_14 Depth=1
	cmpq	$24, %rdx
	jb	.LBB11_13
# BB#11:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rbp,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB11_12:                              # %vector.body
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB11_12
.LBB11_13:                              # %middle.block
                                        #   in Loop: Header=BB11_14 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB11_113
	jmp	.LBB11_120
	.p2align	4, 0x90
.LBB11_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_28 Depth 2
                                        #     Child Loop BB11_19 Depth 2
                                        #     Child Loop BB11_39 Depth 2
                                        #     Child Loop BB11_52 Depth 2
                                        #     Child Loop BB11_64 Depth 2
                                        #     Child Loop BB11_72 Depth 2
                                        #     Child Loop BB11_67 Depth 2
                                        #     Child Loop BB11_82 Depth 2
                                        #     Child Loop BB11_97 Depth 2
                                        #     Child Loop BB11_100 Depth 2
                                        #     Child Loop BB11_8 Depth 2
                                        #     Child Loop BB11_12 Depth 2
                                        #     Child Loop BB11_115 Depth 2
                                        #     Child Loop BB11_118 Depth 2
                                        #     Child Loop BB11_123 Depth 2
	movq	16(%rbp), %rax
	movq	(%rax,%r14,8), %rbx
	movq	$0, 224(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 216(%rsp)
	movl	$0, (%rax)
	movl	$4, 228(%rsp)
.Ltmp74:
	leaq	152(%rsp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	_ZplIwE11CStringBaseIT_ERKS2_S4_
.Ltmp75:
# BB#15:                                #   in Loop: Header=BB11_14 Depth=1
	movq	152(%rsp), %rsi
.Ltmp77:
	leaq	176(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
.Ltmp78:
# BB#16:                                #   in Loop: Header=BB11_14 Depth=1
	testb	%al, %al
	je	.LBB11_21
# BB#17:                                #   in Loop: Header=BB11_14 Depth=1
	movslq	8(%rbx), %rax
	testq	%rax, %rax
	movl	$-1, %ebp
	je	.LBB11_32
# BB#18:                                #   in Loop: Header=BB11_14 Depth=1
	movq	(%rbx), %rcx
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB11_19:                              #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$47, -4(%rcx,%rax)
	je	.LBB11_31
# BB#20:                                #   in Loop: Header=BB11_19 Depth=2
	addq	$-4, %rax
	jne	.LBB11_19
	jmp	.LBB11_32
	.p2align	4, 0x90
.LBB11_21:                              #   in Loop: Header=BB11_14 Depth=1
	callq	__errno_location
	movl	(%rax), %ebx
.Ltmp79:
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp80:
# BB#22:                                #   in Loop: Header=BB11_14 Depth=1
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	12(%rdx)
.Ltmp81:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp82:
# BB#23:                                # %.noexc
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%r12, %r13
	movq	%r14, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	160(%rsp), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB11_26
# BB#24:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp83:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp84:
# BB#25:                                # %.noexc.i
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB11_27
.LBB11_26:                              #   in Loop: Header=BB11_14 Depth=1
	xorl	%eax, %eax
.LBB11_27:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	152(%rsp), %rcx
	.p2align	4, 0x90
.LBB11_28:                              #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB11_28
# BB#29:                                #   in Loop: Header=BB11_14 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp86:
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp87:
	movq	%r12, %r14
	movq	%r13, %r12
	movq	112(%rsp), %rbp         # 8-byte Reload
# BB#30:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	120(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
	jmp	.LBB11_134
.LBB11_31:                              #   in Loop: Header=BB11_14 Depth=1
	leaq	-4(%rcx,%rax), %rbp
	subq	%rcx, %rbp
	shrq	$2, %rbp
.LBB11_32:                              # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
                                        #   in Loop: Header=BB11_14 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
.Ltmp89:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp90:
# BB#33:                                #   in Loop: Header=BB11_14 Depth=1
	movq	%r13, 64(%rsp)
	movl	$0, (%r13)
	movl	$4, 76(%rsp)
	testl	%ebp, %ebp
	movq	%r14, 144(%rsp)         # 8-byte Spill
	js	.LBB11_43
# BB#34:                                #   in Loop: Header=BB11_14 Depth=1
	incl	%ebp
.Ltmp92:
	xorl	%edx, %edx
	leaq	16(%rsp), %rdi
	movq	%rbx, %rsi
	movl	%ebp, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp93:
# BB#35:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	$0, 72(%rsp)
	movl	$0, (%r13)
	movslq	24(%rsp), %rbx
	incq	%rbx
	cmpl	$4, %ebx
	je	.LBB11_38
# BB#36:                                #   in Loop: Header=BB11_14 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp95:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp96:
# BB#37:                                # %._crit_edge16.i.i
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbp, 64(%rsp)
	movl	$0, (%rbp)
	movl	%ebx, 76(%rsp)
	movq	%rbp, %r13
.LBB11_38:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_39:                              #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_39
# BB#40:                                #   in Loop: Header=BB11_14 Depth=1
	movl	24(%rsp), %eax
	movl	%eax, 72(%rsp)
	testq	%rdi, %rdi
	je	.LBB11_42
# BB#41:                                #   in Loop: Header=BB11_14 Depth=1
	callq	_ZdaPv
.LBB11_42:                              # %_ZN11CStringBaseIwED2Ev.exit65
                                        #   in Loop: Header=BB11_14 Depth=1
.Ltmp98:
	movq	%r12, %rdi
	movl	44(%rsp), %esi          # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	leaq	64(%rsp), %rcx
	callq	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
	movl	%eax, %ebx
.Ltmp99:
	jmp	.LBB11_44
.LBB11_43:                              #   in Loop: Header=BB11_14 Depth=1
	movl	44(%rsp), %ebx          # 4-byte Reload
.LBB11_44:                              #   in Loop: Header=BB11_14 Depth=1
.Ltmp100:
	movl	%ebx, %edi
	movl	12(%rsp), %esi          # 4-byte Reload
	leaq	176(%rsp), %rdx
	movq	168(%rsp), %rcx         # 8-byte Reload
	callq	_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE
.Ltmp101:
# BB#45:                                #   in Loop: Header=BB11_14 Depth=1
	testb	$16, 208(%rsp)
	jne	.LBB11_47
# BB#46:                                #   in Loop: Header=BB11_14 Depth=1
	movq	96(%rsp), %r12          # 8-byte Reload
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	144(%rsp), %r14         # 8-byte Reload
	jmp	.LBB11_132
	.p2align	4, 0x90
.LBB11_47:                              #   in Loop: Header=BB11_14 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	224(%rsp), %r14d
	movslq	%r14d, %rdi
	leaq	1(%rdi), %rbp
	testl	%ebp, %ebp
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	je	.LBB11_50
# BB#48:                                # %._crit_edge16.i.i.i67
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rdi, %r15
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp102:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp103:
# BB#49:                                # %.noexc71
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%r12, 16(%rsp)
	movl	$0, (%r12)
	movl	%ebp, 28(%rsp)
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movq	%r15, %rdi
	jmp	.LBB11_51
.LBB11_50:                              #   in Loop: Header=BB11_14 Depth=1
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
.LBB11_51:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i68
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	216(%rsp), %rax
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB11_52:                              #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_52
# BB#53:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	%r14d, 24(%rsp)
	movl	%ebp, %eax
	subl	%r14d, %eax
	cmpl	$1, %eax
	jg	.LBB11_76
# BB#54:                                #   in Loop: Header=BB11_14 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %ebp
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	jl	.LBB11_56
# BB#55:                                # %select.true.sink
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB11_56:                              # %select.end
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbp,%rsi), %r15d
	cmpl	%ebp, %r15d
	je	.LBB11_76
# BB#57:                                #   in Loop: Header=BB11_14 Depth=1
	movq	%rdi, (%rsp)            # 8-byte Spill
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp105:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp106:
# BB#58:                                # %.noexc112
                                        #   in Loop: Header=BB11_14 Depth=1
	testl	%ebp, %ebp
	movq	(%rsp), %rdi            # 8-byte Reload
	jle	.LBB11_75
# BB#59:                                # %.preheader.i.i
                                        #   in Loop: Header=BB11_14 Depth=1
	testl	%r14d, %r14d
	jle	.LBB11_68
# BB#60:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB11_14 Depth=1
	cmpl	$7, %r14d
	jbe	.LBB11_65
# BB#61:                                # %min.iters.checked393
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rdi, %rbp
	andq	$-8, %rbp
	je	.LBB11_65
# BB#62:                                # %vector.body389.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	leaq	-8(%rbp), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB11_69
# BB#63:                                # %vector.body389.prol.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_64:                              # %vector.body389.prol
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r12,%rsi,4), %xmm0
	movups	16(%r12,%rsi,4), %xmm1
	movups	%xmm0, (%rax,%rsi,4)
	movups	%xmm1, 16(%rax,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB11_64
	jmp	.LBB11_70
.LBB11_65:                              #   in Loop: Header=BB11_14 Depth=1
	xorl	%ebp, %ebp
.LBB11_66:                              # %scalar.ph391.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rdi, %rcx
	subq	%rbp, %rcx
	leaq	(%rax,%rbp,4), %rdx
	leaq	(%r12,%rbp,4), %rdi
	.p2align	4, 0x90
.LBB11_67:                              # %scalar.ph391
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %esi
	movl	%esi, (%rdx)
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%rcx
	jne	.LBB11_67
	jmp	.LBB11_74
.LBB11_68:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB11_14 Depth=1
	testq	%r12, %r12
	jne	.LBB11_74
	jmp	.LBB11_75
.LBB11_69:                              #   in Loop: Header=BB11_14 Depth=1
	xorl	%esi, %esi
.LBB11_70:                              # %vector.body389.prol.loopexit
                                        #   in Loop: Header=BB11_14 Depth=1
	cmpq	$24, %rcx
	jb	.LBB11_73
# BB#71:                                # %vector.body389.preheader.new
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rbp, %rcx
	subq	%rsi, %rcx
	leaq	112(%rax,%rsi,4), %rdx
	leaq	112(%r12,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB11_72:                              # %vector.body389
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB11_72
.LBB11_73:                              # %middle.block390
                                        #   in Loop: Header=BB11_14 Depth=1
	cmpq	%rbp, %rdi
	jne	.LBB11_66
.LBB11_74:                              # %._crit_edge.thread.i.i109
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%r12, %rdi
	movq	%rax, %rbp
	callq	_ZdaPv
	movq	%rbp, %rax
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB11_75:                              # %._crit_edge16.i.i110
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rax, 16(%rsp)
	movl	$0, (%rax,%rdi,4)
	movl	%r15d, 28(%rsp)
.LBB11_76:                              #   in Loop: Header=BB11_14 Depth=1
	movq	16(%rsp), %rbp
	movl	$47, (%rbp,%rdi,4)
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	%eax, 24(%rsp)
	cltq
	movl	$0, (%rbp,%rax,4)
.Ltmp108:
	movq	96(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movl	12(%rsp), %edx          # 4-byte Reload
	leaq	16(%rsp), %rcx
	callq	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
	movl	%eax, %r12d
.Ltmp109:
# BB#77:                                #   in Loop: Header=BB11_14 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	movslq	8(%rax), %r14
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	je	.LBB11_80
# BB#78:                                # %._crit_edge16.i.i.i74
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp111:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp112:
# BB#79:                                # %.noexc81
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	$0, (%rbp)
	movq	%rbp, %r15
	jmp	.LBB11_81
.LBB11_80:                              #   in Loop: Header=BB11_14 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
.LBB11_81:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i75
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_82:                              #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rcx), %edx
	movl	%edx, (%r15,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_82
# BB#83:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i78
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	72(%rsp), %eax
	movl	%ebx, %ecx
	subl	%r14d, %ecx
	cmpl	%eax, %ecx
	jle	.LBB11_85
# BB#84:                                #   in Loop: Header=BB11_14 Depth=1
	movq	%rbp, (%rsp)            # 8-byte Spill
	jmp	.LBB11_96
.LBB11_85:                              #   in Loop: Header=BB11_14 Depth=1
	decl	%ecx
	cmpl	$8, %ebx
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB11_87
# BB#86:                                # %select.true.sink788
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB11_87:                              # %select.end787
                                        #   in Loop: Header=BB11_14 Depth=1
	leal	(%rdx,%rcx), %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	cmpl	%eax, %esi
	cmovgel	%edx, %edi
	leal	1(%rbx,%rdi), %eax
	cmpl	%ebx, %eax
	jne	.LBB11_89
# BB#88:                                #   in Loop: Header=BB11_14 Depth=1
	movq	%rbp, (%rsp)            # 8-byte Spill
	jmp	.LBB11_96
.LBB11_89:                              #   in Loop: Header=BB11_14 Depth=1
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp114:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rsp)            # 8-byte Spill
.Ltmp115:
# BB#90:                                # %.noexc126
                                        #   in Loop: Header=BB11_14 Depth=1
	testl	%ebx, %ebx
	jle	.LBB11_95
# BB#91:                                # %.preheader.i.i116
                                        #   in Loop: Header=BB11_14 Depth=1
	testl	%r14d, %r14d
	jle	.LBB11_93
# BB#92:                                # %.lr.ph.i.i117
                                        #   in Loop: Header=BB11_14 Depth=1
	leaq	(,%r14,4), %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbp, %rsi
	callq	memcpy
	jmp	.LBB11_94
.LBB11_93:                              # %._crit_edge.i.i118
                                        #   in Loop: Header=BB11_14 Depth=1
	testq	%r15, %r15
	je	.LBB11_95
.LBB11_94:                              # %._crit_edge.thread.i.i123
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB11_95:                              # %._crit_edge16.i.i124
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	(%rsp), %r15            # 8-byte Reload
	movl	$0, (%r15,%r14,4)
.LBB11_96:                              # %.noexc.i79
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	%r12d, 88(%rsp)         # 4-byte Spill
	leaq	(%r15,%r14,4), %rax
	movq	64(%rsp), %r13
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_97:                              #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB11_97
# BB#98:                                # %._crit_edge16.i.i.i84
                                        #   in Loop: Header=BB11_14 Depth=1
	movslq	72(%rsp), %r12
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	leaq	1(%r12,%r14), %rbp
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp117:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp118:
# BB#99:                                # %.noexc93
                                        #   in Loop: Header=BB11_14 Depth=1
	addq	%r14, %r12
	movq	%rbx, 48(%rsp)
	movl	$0, (%rbx)
	movl	%ebp, 60(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_100:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i85
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r15,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB11_100
# BB#101:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i88
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	%r12d, 56(%rsp)
	movl	24(%rsp), %eax
	testl	%eax, %eax
	movq	144(%rsp), %r14         # 8-byte Reload
	jle	.LBB11_122
# BB#102:                               #   in Loop: Header=BB11_14 Depth=1
	cmpl	$7, %r12d
	movl	$4, %ecx
	movl	$16, %edx
	cmovgl	%edx, %ecx
	cmpl	$64, %r12d
	jl	.LBB11_104
# BB#103:                               # %select.true.sink805
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	%ebp, %ecx
	shrl	$31, %ecx
	addl	%ebp, %ecx
	sarl	%ecx
.LBB11_104:                             # %select.end804
                                        #   in Loop: Header=BB11_14 Depth=1
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
	leal	2(%r12,%rcx), %eax
	cmpl	%ebp, %eax
	je	.LBB11_122
# BB#105:                               #   in Loop: Header=BB11_14 Depth=1
	movl	%eax, 140(%rsp)         # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp120:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp121:
# BB#106:                               # %.noexc141
                                        #   in Loop: Header=BB11_14 Depth=1
	testl	%r12d, %r12d
	js	.LBB11_121
# BB#107:                               # %.preheader.i.i131
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	48(%rsp), %rdi
	je	.LBB11_119
# BB#108:                               # %.lr.ph.i.i132
                                        #   in Loop: Header=BB11_14 Depth=1
	movslq	%r12d, %rax
	cmpl	$7, %r12d
	jbe	.LBB11_112
# BB#109:                               # %min.iters.checked
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB11_112
# BB#110:                               # %vector.memcheck
                                        #   in Loop: Header=BB11_14 Depth=1
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB11_6
# BB#111:                               # %vector.memcheck
                                        #   in Loop: Header=BB11_14 Depth=1
	leaq	(%rbp,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB11_6
.LBB11_112:                             #   in Loop: Header=BB11_14 Depth=1
	xorl	%ecx, %ecx
.LBB11_113:                             # %scalar.ph.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	movl	%r12d, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB11_116
# BB#114:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB11_14 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB11_115:                             # %scalar.ph.prol
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rcx,4), %ebx
	movl	%ebx, (%rbp,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB11_115
.LBB11_116:                             # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB11_14 Depth=1
	cmpq	$7, %rdx
	jb	.LBB11_120
# BB#117:                               # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB11_14 Depth=1
	subq	%rcx, %rax
	leaq	28(%rbp,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB11_118:                             # %scalar.ph
                                        #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB11_118
	jmp	.LBB11_120
.LBB11_119:                             # %._crit_edge.i.i133
                                        #   in Loop: Header=BB11_14 Depth=1
	testq	%rdi, %rdi
	je	.LBB11_121
.LBB11_120:                             # %._crit_edge.thread.i.i138
                                        #   in Loop: Header=BB11_14 Depth=1
	callq	_ZdaPv
.LBB11_121:                             # %._crit_edge16.i.i139
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%rbp, 48(%rsp)
	movslq	%r12d, %rax
	movl	$0, (%rbp,%rax,4)
	movl	140(%rsp), %eax         # 4-byte Reload
	movl	%eax, 60(%rsp)
.LBB11_122:                             # %.noexc.i89
                                        #   in Loop: Header=BB11_14 Depth=1
	movslq	%r12d, %rcx
	shlq	$2, %rcx
	addq	48(%rsp), %rcx
	movq	16(%rsp), %rbx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_123:                             #   Parent Loop BB11_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rsi), %eax
	movl	%eax, (%rcx,%rsi)
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB11_123
# BB#124:                               #   in Loop: Header=BB11_14 Depth=1
	addl	24(%rsp), %r12d
	movl	%r12d, 56(%rsp)
.Ltmp123:
	movq	96(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movl	88(%rsp), %esi          # 4-byte Reload
	movl	%esi, %edx
	leaq	48(%rsp), %rcx
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	128(%rsp), %r9          # 8-byte Reload
	callq	_ZN9CDirItems18EnumerateDirectoryEiiRK11CStringBaseIwER13CObjectVectorIS1_ER13CRecordVectorIjE
.Ltmp124:
# BB#125:                               #   in Loop: Header=BB11_14 Depth=1
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_127
# BB#126:                               #   in Loop: Header=BB11_14 Depth=1
	callq	_ZdaPv
.LBB11_127:                             # %_ZN11CStringBaseIwED2Ev.exit97
                                        #   in Loop: Header=BB11_14 Depth=1
	testq	%r15, %r15
	je	.LBB11_129
# BB#128:                               #   in Loop: Header=BB11_14 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZdaPv
.LBB11_129:                             # %_ZN11CStringBaseIwED2Ev.exit98
                                        #   in Loop: Header=BB11_14 Depth=1
	testq	%rbx, %rbx
	movq	112(%rsp), %rbp         # 8-byte Reload
	je	.LBB11_131
# BB#130:                               #   in Loop: Header=BB11_14 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB11_131:                             #   in Loop: Header=BB11_14 Depth=1
	testq	%r13, %r13
	je	.LBB11_133
.LBB11_132:                             # %.thread
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	%r13, %rdi
	callq	_ZdaPv
.LBB11_133:                             # %_ZN11CStringBaseIwED2Ev.exit103
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	104(%rsp), %r15         # 8-byte Reload
.LBB11_134:                             #   in Loop: Header=BB11_14 Depth=1
	movq	152(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_136
# BB#135:                               #   in Loop: Header=BB11_14 Depth=1
	callq	_ZdaPv
.LBB11_136:                             # %_ZN11CStringBaseIwED2Ev.exit104
                                        #   in Loop: Header=BB11_14 Depth=1
	movq	216(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_138
# BB#137:                               #   in Loop: Header=BB11_14 Depth=1
	callq	_ZdaPv
.LBB11_138:                             # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit106
                                        #   in Loop: Header=BB11_14 Depth=1
	incq	%r14
	movslq	12(%rbp), %rax
	cmpq	%rax, %r14
	jl	.LBB11_14
.LBB11_139:                             # %._crit_edge
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	32(%r12), %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	64(%r12), %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	addq	$96, %r12
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_140:
.Ltmp116:
	movq	%rax, %r14
	testq	%r15, %r15
	je	.LBB11_154
# BB#141:
	movq	%rbp, %rdi
	jmp	.LBB11_153
.LBB11_142:
.Ltmp107:
	movq	%rax, %r14
	testq	%r12, %r12
	je	.LBB11_162
# BB#143:
	movq	%r12, %rdi
	jmp	.LBB11_157
.LBB11_144:
.Ltmp122:
	movq	%rax, %r14
	movq	%rbx, %rdi
	jmp	.LBB11_149
.LBB11_145:
.Ltmp97:
	movq	%rax, %r14
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_163
# BB#146:
	callq	_ZdaPv
	jmp	.LBB11_163
.LBB11_147:
.Ltmp113:
	movq	%rax, %r14
	jmp	.LBB11_154
.LBB11_148:
.Ltmp125:
	movq	%rax, %r14
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_151
.LBB11_149:
	callq	_ZdaPv
	testq	%r15, %r15
	jne	.LBB11_152
	jmp	.LBB11_154
.LBB11_150:
.Ltmp119:
	movq	%rax, %r14
.LBB11_151:                             # %_ZN11CStringBaseIwED2Ev.exit100
	testq	%r15, %r15
	je	.LBB11_154
.LBB11_152:
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB11_153:                             # %_ZN11CStringBaseIwED2Ev.exit101
	callq	_ZdaPv
.LBB11_154:                             # %_ZN11CStringBaseIwED2Ev.exit101
	movq	16(%rsp), %rbp
	testq	%rbp, %rbp
	jne	.LBB11_156
	jmp	.LBB11_162
.LBB11_155:
.Ltmp110:
	movq	%rax, %r14
	testq	%rbp, %rbp
	je	.LBB11_162
.LBB11_156:
	movq	%rbp, %rdi
.LBB11_157:
	callq	_ZdaPv
	testq	%r13, %r13
	jne	.LBB11_163
	jmp	.LBB11_167
.LBB11_158:
.Ltmp85:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB11_167
.LBB11_159:
.Ltmp94:
	movq	%rax, %r14
	jmp	.LBB11_163
.LBB11_160:
.Ltmp91:
	jmp	.LBB11_166
.LBB11_161:
.Ltmp104:
	movq	%rax, %r14
.LBB11_162:
	testq	%r13, %r13
	je	.LBB11_167
.LBB11_163:
	movq	%r13, %rdi
	callq	_ZdaPv
	jmp	.LBB11_167
.LBB11_164:
.Ltmp76:
	movq	%rax, %r14
	jmp	.LBB11_169
.LBB11_165:
.Ltmp88:
.LBB11_166:
	movq	%rax, %r14
.LBB11_167:
	movq	152(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_169
# BB#168:
	callq	_ZdaPv
.LBB11_169:
	movq	216(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_171
# BB#170:
	callq	_ZdaPv
.LBB11_171:                             # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN9CDirItems18EnumerateDirItems2ERK11CStringBaseIwES3_RK13CObjectVectorIS1_ERS5_R13CRecordVectorIjE, .Lfunc_end11-_ZN9CDirItems18EnumerateDirItems2ERK11CStringBaseIwES3_RK13CObjectVectorIS1_ERS5_R13CRecordVectorIjE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\355\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp74-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp74
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin6   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp82-.Ltmp77         #   Call between .Ltmp77 and .Ltmp82
	.long	.Ltmp88-.Lfunc_begin6   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin6   #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin6   # >> Call Site 5 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin6   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp89-.Lfunc_begin6   # >> Call Site 6 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin6   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin6   # >> Call Site 7 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin6   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin6   # >> Call Site 8 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin6   #     jumps to .Ltmp97
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin6   # >> Call Site 9 <<
	.long	.Ltmp103-.Ltmp98        #   Call between .Ltmp98 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin6  #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin6  #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin6  #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin6  #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin6  #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp117-.Ltmp115       #   Call between .Ltmp115 and .Ltmp117
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin6  #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin6  #     jumps to .Ltmp122
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin6  # >> Call Site 17 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin6  #     jumps to .Ltmp125
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Lfunc_end11-.Ltmp124   #   Call between .Ltmp124 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.zero	16
	.text
	.globl	_Z14EnumerateItemsRKN9NWildcard7CCensorER9CDirItemsP20IEnumDirItemCallbackR13CObjectVectorI11CStringBaseIwEER13CRecordVectorIjE
	.p2align	4, 0x90
	.type	_Z14EnumerateItemsRKN9NWildcard7CCensorER9CDirItemsP20IEnumDirItemCallbackR13CObjectVectorI11CStringBaseIwEER13CRecordVectorIjE,@function
_Z14EnumerateItemsRKN9NWildcard7CCensorER9CDirItemsP20IEnumDirItemCallbackR13CObjectVectorI11CStringBaseIwEER13CRecordVectorIjE: # @_Z14EnumerateItemsRKN9NWildcard7CCensorER9CDirItemsP20IEnumDirItemCallbackR13CObjectVectorI11CStringBaseIwEER13CRecordVectorIjE
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi95:
	.cfi_def_cfa_offset 112
.Lcfi96:
	.cfi_offset %rbx, -56
.Lcfi97:
	.cfi_offset %r12, -48
.Lcfi98:
	.cfi_offset %r13, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbx
	cmpl	$0, 12(%rbx)
	jle	.LBB12_15
# BB#1:                                 # %.lr.ph
	xorl	%r12d, %r12d
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB12_3:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax,%r12,8), %r14
	cmpl	$0, 8(%r14)
	movl	$-1, %esi
	je	.LBB12_5
# BB#4:                                 #   in Loop: Header=BB12_3 Depth=1
.Lcfi102:
	.cfi_escape 0x2e, 0x00
	movl	$-1, %esi
	movl	$-1, %edx
	movq	%r13, %rdi
	movq	%r14, %rcx
	callq	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
	movl	%eax, %esi
.LBB12_5:                               #   in Loop: Header=BB12_3 Depth=1
	leaq	16(%r14), %rdi
	leaq	8(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$8, 24(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rsp)
.Ltmp126:
.Lcfi103:
	.cfi_escape 0x2e, 0x20
	movl	$-1, %edx
	movq	%r14, %rcx
	movq	%r15, %r8
	movq	%r13, %r9
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	callq	_ZL17EnumerateDirItemsRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwERK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE
	addq	$32, %rsp
.Lcfi108:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %ebp
.Ltmp127:
# BB#6:                                 #   in Loop: Header=BB12_3 Depth=1
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rsp)
.Ltmp138:
.Lcfi109:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp139:
# BB#7:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
                                        #   in Loop: Header=BB12_3 Depth=1
.Lcfi110:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	testl	%ebp, %ebp
	jne	.LBB12_16
# BB#2:                                 #   in Loop: Header=BB12_3 Depth=1
	incq	%r12
	movslq	12(%rbx), %rax
	cmpq	%rax, %r12
	jl	.LBB12_3
.LBB12_15:                              # %._crit_edge
.Lcfi111:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	32(%r13), %rdi
.Lcfi112:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	leaq	64(%r13), %rdi
.Lcfi113:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	addq	$96, %r13
.Lcfi114:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector11ReserveDownEv
	xorl	%ebp, %ebp
.LBB12_16:                              # %.loopexit
	movl	%ebp, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_11:
.Ltmp128:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rsp)
.Ltmp129:
.Lcfi115:
	.cfi_escape 0x2e, 0x00
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp130:
# BB#12:
.Ltmp135:
.Lcfi116:
	.cfi_escape 0x2e, 0x00
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp136:
	jmp	.LBB12_9
.LBB12_17:
.Ltmp137:
	movq	%rax, %rbx
	jmp	.LBB12_18
.LBB12_13:
.Ltmp131:
	movq	%rax, %rbx
.Ltmp132:
.Lcfi117:
	.cfi_escape 0x2e, 0x00
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp133:
.LBB12_18:                              # %.body
.Lcfi118:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB12_14:
.Ltmp134:
.Lcfi119:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_8:
.Ltmp140:
	movq	%rax, %rbx
.Ltmp141:
.Lcfi120:
	.cfi_escape 0x2e, 0x00
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp142:
.LBB12_9:                               # %unwind_resume
.Lcfi121:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_10:
.Ltmp143:
.Lcfi122:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_Z14EnumerateItemsRKN9NWildcard7CCensorER9CDirItemsP20IEnumDirItemCallbackR13CObjectVectorI11CStringBaseIwEER13CRecordVectorIjE, .Lfunc_end12-_Z14EnumerateItemsRKN9NWildcard7CCensorER9CDirItemsP20IEnumDirItemCallbackR13CObjectVectorI11CStringBaseIwEER13CRecordVectorIjE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp126-.Lfunc_begin7  #   Call between .Lfunc_begin7 and .Ltmp126
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp126-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin7  #     jumps to .Ltmp128
	.byte	0                       #   On action: cleanup
	.long	.Ltmp138-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin7  #     jumps to .Ltmp140
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp129-.Ltmp139       #   Call between .Ltmp139 and .Ltmp129
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin7  #     jumps to .Ltmp131
	.byte	1                       #   On action: 1
	.long	.Ltmp135-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin7  #     jumps to .Ltmp137
	.byte	1                       #   On action: 1
	.long	.Ltmp132-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin7  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp141-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin7  #     jumps to .Ltmp143
	.byte	1                       #   On action: 1
	.long	.Ltmp142-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Lfunc_end12-.Ltmp142   #   Call between .Ltmp142 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.zero	16
	.text
	.p2align	4, 0x90
	.type	_ZL17EnumerateDirItemsRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwERK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE,@function
_ZL17EnumerateDirItemsRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwERK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE: # @_ZL17EnumerateDirItemsRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwERK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi127:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi129:
	.cfi_def_cfa_offset 592
.Lcfi130:
	.cfi_offset %rbx, -56
.Lcfi131:
	.cfi_offset %r12, -48
.Lcfi132:
	.cfi_offset %r13, -40
.Lcfi133:
	.cfi_offset %r14, -32
.Lcfi134:
	.cfi_offset %r15, -24
.Lcfi135:
	.cfi_offset %rbp, -16
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%edx, 44(%rsp)          # 4-byte Spill
	movl	%esi, 40(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movb	592(%rsp), %al
	movb	%al, 11(%rsp)           # 1-byte Spill
	testb	%al, %al
	jne	.LBB13_2
# BB#1:
.Lcfi136:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZNK9NWildcard11CCensorNode16NeedCheckSubDirsEv
	movb	%al, 11(%rsp)           # 1-byte Spill
.LBB13_2:
	cmpq	$0, 600(%rsp)
	je	.LBB13_4
# BB#3:
	movq	600(%rsp), %rdi
	movq	(%rdi), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movslq	12(%rcx), %rsi
	movslq	108(%rcx), %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
.Lcfi137:
	.cfi_escape 0x2e, 0x00
	callq	*(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB13_242
.LBB13_4:
	xorl	%r15d, %r15d
	cmpb	$0, 11(%rsp)            # 1-byte Folded Reload
	movl	$0, %ebp
	movq	%r14, 16(%rsp)          # 8-byte Spill
	jne	.LBB13_131
# BB#5:
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	12(%rax), %eax
	testl	%eax, %eax
	movl	$0, %ebp
	jne	.LBB13_131
# BB#6:                                 # %.preheader480
	movl	68(%r14), %eax
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB13_13
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph704
                                        # =>This Inner Loop Header: Depth=1
	movq	72(%r14), %rcx
	movq	(%rcx,%rbx,8), %rcx
	cmpb	$0, 32(%rcx)
	jne	.LBB13_13
# BB#8:                                 #   in Loop: Header=BB13_7 Depth=1
	cmpl	$1, 12(%rcx)
	jne	.LBB13_13
# BB#9:                                 #   in Loop: Header=BB13_7 Depth=1
	movq	16(%rcx), %rcx
	movq	(%rcx), %rdi
	cmpl	$0, 8(%rdi)
	je	.LBB13_13
# BB#10:                                #   in Loop: Header=BB13_7 Depth=1
.Lcfi138:
	.cfi_escape 0x2e, 0x00
	callq	_Z23DoesNameContainWildCardRK11CStringBaseIwE
	testb	%al, %al
	jne	.LBB13_12
# BB#11:                                #   in Loop: Header=BB13_7 Depth=1
	incq	%rbx
	movslq	68(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB13_7
	jmp	.LBB13_13
.LBB13_12:                              # %..thread.loopexit_crit_edge
	movl	68(%r14), %eax
.LBB13_13:                              # %.thread
	cmpl	%eax, %ebx
	jne	.LBB13_131
# BB#14:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rsp)
	movq	$1, 88(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 64(%rsp)
	testl	%eax, %eax
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jle	.LBB13_86
# BB#15:                                # %.lr.ph701
	leaq	400(%rsp), %r15
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	96(%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB13_34
	.p2align	4, 0x90
.LBB13_16:                              #   in Loop: Header=BB13_34 Depth=1
	incq	%r12
	movslq	68(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB13_34
	jmp	.LBB13_86
.LBB13_17:                              #   in Loop: Header=BB13_34 Depth=1
.Ltmp194:
.Lcfi139:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp195:
# BB#18:                                # %.noexc328
                                        #   in Loop: Header=BB13_34 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%r14), %r15
	leaq	1(%r15), %rbp
	testl	%ebp, %ebp
	je	.LBB13_25
# BB#19:                                # %._crit_edge16.i.i.i323
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp196:
.Lcfi140:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp197:
# BB#20:                                # %.noexc.i324
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB13_26
.LBB13_21:                              # %.lr.ph697.preheader
                                        #   in Loop: Header=BB13_34 Depth=1
	decl	%ebp
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB13_22:                              # %.lr.ph697
                                        #   Parent Loop BB13_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp201:
.Lcfi141:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp202:
# BB#23:                                #   in Loop: Header=BB13_22 Depth=2
	movq	80(%rsp), %rax
	movslq	76(%rsp), %rcx
	movb	$1, (%rax,%rcx)
	leal	1(%rcx), %ecx
	movl	%ecx, 76(%rsp)
	incl	%ebp
	cmpl	%ebx, %ebp
	jl	.LBB13_22
.LBB13_24:                              # %._crit_edge
                                        #   in Loop: Header=BB13_34 Depth=1
	movslq	%ebx, %rcx
	movb	$0, (%rax,%rcx)
	movq	40(%r14), %rax
	movq	(%rax,%rcx,8), %rdi
	jmp	.LBB13_30
.LBB13_25:                              #   in Loop: Header=BB13_34 Depth=1
	xorl	%eax, %eax
.LBB13_26:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i325
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	(%r14), %rcx
	.p2align	4, 0x90
.LBB13_27:                              #   Parent Loop BB13_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB13_27
# BB#28:                                #   in Loop: Header=BB13_34 Depth=1
	movl	%r15d, 8(%rbx)
.Ltmp199:
.Lcfi142:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp200:
	leaq	400(%rsp), %r15
# BB#29:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit332
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	224(%rsp), %rax
	movslq	220(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 220(%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
.LBB13_30:                              #   in Loop: Header=BB13_34 Depth=1
	movq	400(%rsp), %rcx
	movl	408(%rsp), %r8d
.Ltmp204:
.Lcfi143:
	.cfi_escape 0x2e, 0x30
	movl	40(%rsp), %esi          # 4-byte Reload
	movl	44(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	pushq	616(%rsp)
.Lcfi144:
	.cfi_adjust_cfa_offset 8
	pushq	616(%rsp)
.Lcfi145:
	.cfi_adjust_cfa_offset 8
	pushq	616(%rsp)
.Lcfi146:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi147:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi148:
	.cfi_adjust_cfa_offset 8
	leaq	248(%rsp), %rbp
	pushq	%rbp
.Lcfi149:
	.cfi_adjust_cfa_offset 8
	callq	_ZL22EnumerateDirItems_SpecRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwES6_RK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE
	addq	$48, %rsp
.Lcfi150:
	.cfi_adjust_cfa_offset -48
.Ltmp205:
# BB#31:                                #   in Loop: Header=BB13_34 Depth=1
	xorl	%r13d, %r13d
	testl	%eax, %eax
	setne	%bl
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 208(%rsp)
.Ltmp215:
.Lcfi151:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp216:
# BB#32:                                #   in Loop: Header=BB13_34 Depth=1
.Ltmp221:
.Lcfi152:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp222:
# BB#33:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit336
                                        #   in Loop: Header=BB13_34 Depth=1
	movb	%bl, %r13b
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB13_80
	.p2align	4, 0x90
.LBB13_34:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_57 Depth 2
                                        #     Child Loop BB13_62 Depth 2
                                        #     Child Loop BB13_27 Depth 2
                                        #     Child Loop BB13_22 Depth 2
                                        #     Child Loop BB13_76 Depth 2
	movq	72(%r14), %rax
	movq	(%rax,%r12,8), %rbx
	movq	16(%rbx), %rax
	movq	(%rax), %r14
.Ltmp144:
.Lcfi153:
	.cfi_escape 0x2e, 0x00
	leaq	192(%rsp), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	callq	_ZplIwE11CStringBaseIT_ERKS2_S4_
.Ltmp145:
# BB#35:                                #   in Loop: Header=BB13_34 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
.Ltmp147:
.Lcfi154:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp148:
# BB#36:                                #   in Loop: Header=BB13_34 Depth=1
	movq	%rax, 400(%rsp)
	movl	$0, (%rax)
	movl	$4, 412(%rsp)
	movq	192(%rsp), %rsi
.Ltmp150:
.Lcfi155:
	.cfi_escape 0x2e, 0x00
	leaq	360(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
.Ltmp151:
# BB#37:                                #   in Loop: Header=BB13_34 Depth=1
	testb	%al, %al
	je	.LBB13_40
# BB#38:                                #   in Loop: Header=BB13_34 Depth=1
	movl	392(%rsp), %r15d
	andl	$16, %r15d
	je	.LBB13_45
# BB#39:                                #   in Loop: Header=BB13_34 Depth=1
	cmpb	$0, 34(%rbx)
	jne	.LBB13_46
	jmp	.LBB13_50
	.p2align	4, 0x90
.LBB13_40:                              #   in Loop: Header=BB13_34 Depth=1
.Lcfi156:
	.cfi_escape 0x2e, 0x00
	callq	__errno_location
	movl	(%rax), %ebx
.Ltmp152:
.Lcfi157:
	.cfi_escape 0x2e, 0x00
	movq	616(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp153:
# BB#41:                                #   in Loop: Header=BB13_34 Depth=1
	movq	616(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	12(%rdx)
.Ltmp154:
.Lcfi158:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp155:
# BB#42:                                # %.noexc
                                        #   in Loop: Header=BB13_34 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movslq	200(%rsp), %r14
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	je	.LBB13_55
# BB#43:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp156:
.Lcfi159:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp157:
# BB#44:                                # %.noexc.i
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	%rax, (%rbp)
	movl	$0, (%rax)
	movl	%ebx, 12(%rbp)
	jmp	.LBB13_56
	.p2align	4, 0x90
.LBB13_45:                              # %.critedge
                                        #   in Loop: Header=BB13_34 Depth=1
	cmpb	$0, 33(%rbx)
	je	.LBB13_50
.LBB13_46:                              #   in Loop: Header=BB13_34 Depth=1
	xorps	%xmm0, %xmm0
	leaq	168(%rsp), %rax
	movups	%xmm0, (%rax)
	movq	$8, 184(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 160(%rsp)
.Ltmp162:
.Lcfi160:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp163:
# BB#47:                                # %.noexc310
                                        #   in Loop: Header=BB13_34 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	408(%rsp), %r13
	leaq	1(%r13), %rbp
	testl	%ebp, %ebp
	je	.LBB13_60
# BB#48:                                # %._crit_edge16.i.i.i305
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp164:
.Lcfi161:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp165:
# BB#49:                                # %.noexc.i306
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB13_61
.LBB13_50:                              #   in Loop: Header=BB13_34 Depth=1
.Ltmp224:
.Lcfi162:
	.cfi_escape 0x2e, 0x00
	movq	616(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp225:
# BB#51:                                #   in Loop: Header=BB13_34 Depth=1
	movq	616(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movl	$-2147467259, (%rax,%rcx,4) # imm = 0x80004005
	incl	12(%rdx)
.Ltmp226:
.Lcfi163:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp227:
# BB#52:                                # %.noexc300
                                        #   in Loop: Header=BB13_34 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	200(%rsp), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB13_74
# BB#53:                                # %._crit_edge16.i.i.i295
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp228:
.Lcfi164:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp229:
# BB#54:                                # %.noexc.i296
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB13_75
.LBB13_55:                              #   in Loop: Header=BB13_34 Depth=1
	xorl	%eax, %eax
.LBB13_56:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	192(%rsp), %rcx
	.p2align	4, 0x90
.LBB13_57:                              #   Parent Loop BB13_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB13_57
# BB#58:                                #   in Loop: Header=BB13_34 Depth=1
	movl	%r14d, 8(%rbp)
.Ltmp159:
.Lcfi165:
	.cfi_escape 0x2e, 0x00
	movq	608(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp160:
# BB#59:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	608(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
	movl	$7, %r13d
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB13_80
.LBB13_60:                              #   in Loop: Header=BB13_34 Depth=1
	xorl	%eax, %eax
.LBB13_61:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i307
                                        #   in Loop: Header=BB13_34 Depth=1
	leaq	160(%rsp), %rdi
	movq	400(%rsp), %rcx
	.p2align	4, 0x90
.LBB13_62:                              #   Parent Loop BB13_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB13_62
# BB#63:                                #   in Loop: Header=BB13_34 Depth=1
	movl	%r13d, 8(%rbx)
.Ltmp167:
.Lcfi166:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp168:
# BB#64:                                #   in Loop: Header=BB13_34 Depth=1
	movq	176(%rsp), %rax
	movslq	172(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 172(%rsp)
	xorl	%ecx, %ecx
	testl	%r15d, %r15d
	sete	%cl
.Ltmp169:
.Lcfi167:
	.cfi_escape 0x2e, 0x00
	xorl	%esi, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	160(%rsp), %rdx
	callq	_ZNK9NWildcard11CCensorNode15CheckPathToRootEbR13CObjectVectorI11CStringBaseIwEEb
	movl	%eax, %ebx
.Ltmp170:
# BB#65:                                #   in Loop: Header=BB13_34 Depth=1
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 160(%rsp)
.Ltmp180:
.Lcfi168:
	.cfi_escape 0x2e, 0x00
	leaq	160(%rsp), %rbp
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp181:
# BB#66:                                #   in Loop: Header=BB13_34 Depth=1
.Ltmp186:
.Lcfi169:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp187:
# BB#67:                                # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
                                        #   in Loop: Header=BB13_34 Depth=1
	movl	$7, %r13d
	testb	%bl, %bl
	jne	.LBB13_79
# BB#68:                                #   in Loop: Header=BB13_34 Depth=1
.Ltmp189:
.Lcfi170:
	.cfi_escape 0x2e, 0x00
	movl	40(%rsp), %edi          # 4-byte Reload
	movl	44(%rsp), %esi          # 4-byte Reload
	leaq	360(%rsp), %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE
.Ltmp190:
# BB#69:                                #   in Loop: Header=BB13_34 Depth=1
	testl	%r15d, %r15d
	je	.LBB13_79
# BB#70:                                #   in Loop: Header=BB13_34 Depth=1
	xorps	%xmm0, %xmm0
	leaq	216(%rsp), %rax
	movups	%xmm0, (%rax)
	movq	$8, 232(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 208(%rsp)
.Ltmp191:
.Lcfi171:
	.cfi_escape 0x2e, 0x00
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rsi
	callq	_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE
	movl	%eax, %ebx
.Ltmp192:
	leaq	400(%rsp), %r15
# BB#71:                                #   in Loop: Header=BB13_34 Depth=1
	testl	%ebx, %ebx
	js	.LBB13_17
# BB#72:                                #   in Loop: Header=BB13_34 Depth=1
	movl	76(%rsp), %ebp
	cmpl	%ebx, %ebp
	jle	.LBB13_21
# BB#73:                                # %.._crit_edge_crit_edge
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	80(%rsp), %rax
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB13_24
.LBB13_74:                              #   in Loop: Header=BB13_34 Depth=1
	xorl	%eax, %eax
.LBB13_75:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i297
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	192(%rsp), %rcx
	.p2align	4, 0x90
.LBB13_76:                              #   Parent Loop BB13_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB13_76
# BB#77:                                #   in Loop: Header=BB13_34 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp231:
.Lcfi172:
	.cfi_escape 0x2e, 0x00
	movq	608(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp232:
# BB#78:                                # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit304
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	608(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
	movl	$7, %r13d
.LBB13_79:                              #   in Loop: Header=BB13_34 Depth=1
	movq	16(%rsp), %r14          # 8-byte Reload
	leaq	400(%rsp), %r15
.LBB13_80:                              #   in Loop: Header=BB13_34 Depth=1
	movq	400(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_82
# BB#81:                                #   in Loop: Header=BB13_34 Depth=1
.Lcfi173:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_82:                              # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit
                                        #   in Loop: Header=BB13_34 Depth=1
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_84
# BB#83:                                #   in Loop: Header=BB13_34 Depth=1
.Lcfi174:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_84:                              # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB13_34 Depth=1
	movl	%r13d, %eax
	andb	$7, %al
	cmpb	$7, %al
	je	.LBB13_16
# BB#85:                                # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB13_34 Depth=1
	testb	%al, %al
	je	.LBB13_16
	jmp	.LBB13_130
.LBB13_86:                              # %.preheader
	movl	$1, %r13d
	cmpl	$0, 36(%r14)
	jle	.LBB13_129
# BB#87:                                # %.lr.ph
	leaq	520(%rsp), %r12
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB13_88:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_118 Depth 2
                                        #     Child Loop BB13_113 Depth 2
	movslq	76(%rsp), %rax
	cmpq	%rax, %r15
	jge	.LBB13_90
# BB#89:                                #   in Loop: Header=BB13_88 Depth=1
	movq	80(%rsp), %rax
	cmpb	$0, (%rax,%r15)
	je	.LBB13_127
.LBB13_90:                              #   in Loop: Header=BB13_88 Depth=1
	movq	40(%r14), %rax
	movq	(%rax,%r15,8), %rbx
	leaq	8(%rbx), %rdx
.Ltmp234:
.Lcfi175:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	_ZplIwE11CStringBaseIT_ERKS2_S4_
.Ltmp235:
# BB#91:                                #   in Loop: Header=BB13_88 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
.Ltmp237:
.Lcfi176:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp238:
# BB#92:                                #   in Loop: Header=BB13_88 Depth=1
	movq	%rax, 520(%rsp)
	movl	$0, (%rax)
	movl	$4, 532(%rsp)
	movq	144(%rsp), %rsi
.Ltmp240:
.Lcfi177:
	.cfi_escape 0x2e, 0x00
	leaq	480(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
.Ltmp241:
# BB#93:                                #   in Loop: Header=BB13_88 Depth=1
	testb	%al, %al
	je	.LBB13_100
# BB#94:                                #   in Loop: Header=BB13_88 Depth=1
	testb	$16, 512(%rsp)
	jne	.LBB13_107
# BB#95:                                #   in Loop: Header=BB13_88 Depth=1
.Ltmp273:
.Lcfi178:
	.cfi_escape 0x2e, 0x00
	movq	616(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp274:
# BB#96:                                #   in Loop: Header=BB13_88 Depth=1
	movq	616(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movl	$-2147467259, (%rax,%rcx,4) # imm = 0x80004005
	incl	12(%rdx)
.Ltmp275:
.Lcfi179:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp276:
# BB#97:                                # %.noexc366
                                        #   in Loop: Header=BB13_88 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	152(%rsp), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB13_111
# BB#98:                                # %._crit_edge16.i.i.i361
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp277:
.Lcfi180:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp278:
# BB#99:                                # %.noexc.i362
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB13_112
	.p2align	4, 0x90
.LBB13_100:                             #   in Loop: Header=BB13_88 Depth=1
.Ltmp242:
.Lcfi181:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZNK9NWildcard11CCensorNode20AreThereIncludeItemsEv
.Ltmp243:
# BB#101:                               #   in Loop: Header=BB13_88 Depth=1
	movl	$13, %r13d
	testb	%al, %al
	je	.LBB13_121
# BB#102:                               #   in Loop: Header=BB13_88 Depth=1
.Lcfi182:
	.cfi_escape 0x2e, 0x00
	callq	__errno_location
	movl	(%rax), %ebx
.Ltmp244:
.Lcfi183:
	.cfi_escape 0x2e, 0x00
	movq	616(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp245:
# BB#103:                               #   in Loop: Header=BB13_88 Depth=1
	movq	616(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	12(%rdx)
.Ltmp246:
.Lcfi184:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp247:
# BB#104:                               # %.noexc354
                                        #   in Loop: Header=BB13_88 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	152(%rsp), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB13_116
# BB#105:                               # %._crit_edge16.i.i.i349
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp248:
.Lcfi185:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp249:
# BB#106:                               # %.noexc.i350
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB13_117
	.p2align	4, 0x90
.LBB13_107:                             #   in Loop: Header=BB13_88 Depth=1
	leaq	280(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$8, 296(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 272(%rsp)
	movq	520(%rsp), %rcx
	movl	528(%rsp), %r8d
.Ltmp253:
.Lcfi186:
	.cfi_escape 0x2e, 0x30
	movq	%rbx, %rdi
	movl	40(%rsp), %esi          # 4-byte Reload
	movl	44(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	pushq	616(%rsp)
.Lcfi187:
	.cfi_adjust_cfa_offset 8
	pushq	616(%rsp)
.Lcfi188:
	.cfi_adjust_cfa_offset 8
	pushq	616(%rsp)
.Lcfi189:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi190:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi191:
	.cfi_adjust_cfa_offset 8
	leaq	312(%rsp), %rbp
	pushq	%rbp
.Lcfi192:
	.cfi_adjust_cfa_offset 8
	callq	_ZL22EnumerateDirItems_SpecRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwES6_RK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE
	addq	$48, %rsp
.Lcfi193:
	.cfi_adjust_cfa_offset -48
	movl	%eax, %ebx
.Ltmp254:
# BB#108:                               #   in Loop: Header=BB13_88 Depth=1
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 272(%rsp)
.Ltmp264:
.Lcfi194:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp265:
# BB#109:                               #   in Loop: Header=BB13_88 Depth=1
.Ltmp270:
.Lcfi195:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp271:
# BB#110:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit381
                                        #   in Loop: Header=BB13_88 Depth=1
	xorl	%r13d, %r13d
	testl	%ebx, %ebx
	setne	%r13b
	movl	12(%rsp), %eax          # 4-byte Reload
	cmovnel	%ebx, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB13_121
.LBB13_111:                             #   in Loop: Header=BB13_88 Depth=1
	xorl	%eax, %eax
.LBB13_112:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i363
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	144(%rsp), %rcx
	leaq	520(%rsp), %r12
	.p2align	4, 0x90
.LBB13_113:                             #   Parent Loop BB13_88 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB13_113
# BB#114:                               #   in Loop: Header=BB13_88 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp280:
.Lcfi196:
	.cfi_escape 0x2e, 0x00
	movq	608(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp281:
	movq	16(%rsp), %r14          # 8-byte Reload
# BB#115:                               # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit370
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	608(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
	movl	$13, %r13d
	jmp	.LBB13_121
.LBB13_116:                             #   in Loop: Header=BB13_88 Depth=1
	xorl	%eax, %eax
.LBB13_117:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i351
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	144(%rsp), %rcx
	leaq	520(%rsp), %r12
	.p2align	4, 0x90
.LBB13_118:                             #   Parent Loop BB13_88 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB13_118
# BB#119:                               #   in Loop: Header=BB13_88 Depth=1
	movl	%r14d, 8(%rbx)
.Ltmp251:
.Lcfi197:
	.cfi_escape 0x2e, 0x00
	movq	608(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp252:
	movq	16(%rsp), %r14          # 8-byte Reload
# BB#120:                               # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit358
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	608(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
.LBB13_121:                             #   in Loop: Header=BB13_88 Depth=1
	movq	520(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_123
# BB#122:                               #   in Loop: Header=BB13_88 Depth=1
.Lcfi198:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_123:                             # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit387
                                        #   in Loop: Header=BB13_88 Depth=1
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_125
# BB#124:                               #   in Loop: Header=BB13_88 Depth=1
.Lcfi199:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_125:                             # %_ZN11CStringBaseIwED2Ev.exit388
                                        #   in Loop: Header=BB13_88 Depth=1
	movl	%r13d, %eax
	andb	$15, %al
	cmpb	$13, %al
	je	.LBB13_127
# BB#126:                               # %_ZN11CStringBaseIwED2Ev.exit388
                                        #   in Loop: Header=BB13_88 Depth=1
	testb	%al, %al
	jne	.LBB13_130
.LBB13_127:                             #   in Loop: Header=BB13_88 Depth=1
	incq	%r15
	movslq	36(%r14), %rax
	cmpq	%rax, %r15
	jl	.LBB13_88
# BB#128:
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$1, %r13d
	jmp	.LBB13_130
.LBB13_129:
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
.LBB13_130:                             # %.loopexit478
.Lcfi200:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	testl	%r13d, %r13d
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	$0, %r15d
	jne	.LBB13_242
.LBB13_131:                             # %.thread465
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	8(%rax), %r13d
	movslq	%r13d, %rbp
	leaq	1(%rbp), %rbx
	testl	%ebx, %ebx
	je	.LBB13_133
# BB#132:                               # %._crit_edge16.i.i.i392
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Lcfi201:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r12
	movq	%r12, 64(%rsp)
	movl	$0, (%r12)
	movl	%ebx, 76(%rsp)
	movq	%r12, %rdi
	movl	%ebx, %r15d
	jmp	.LBB13_134
.LBB13_133:
	xorl	%r12d, %r12d
	xorl	%edi, %edi
.LBB13_134:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i393
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB13_135:                             # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB13_135
# BB#136:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%r13d, 72(%rsp)
	movl	%r15d, %eax
	subl	%r13d, %eax
	cmpl	$1, %eax
	jg	.LBB13_159
# BB#137:
	leal	-1(%rax), %ecx
	cmpl	$8, %r15d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r15d
	jl	.LBB13_139
# BB#138:                               # %select.true.sink
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	sarl	%edx
.LBB13_139:                             # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r15,%rsi), %ebx
	cmpl	%r15d, %ebx
	je	.LBB13_159
# BB#140:
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	%r15, %r14
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp285:
.Lcfi202:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r15
.Ltmp286:
# BB#141:                               # %.noexc458
	testl	%r14d, %r14d
	jle	.LBB13_158
# BB#142:                               # %.preheader.i.i
	testl	%r13d, %r13d
	jle	.LBB13_151
# BB#143:                               # %.lr.ph.i.i
	cmpl	$7, %r13d
	movq	48(%rsp), %rdi          # 8-byte Reload
	jbe	.LBB13_148
# BB#144:                               # %min.iters.checked
	movq	%rbp, %rax
	andq	$-8, %rax
	je	.LBB13_148
# BB#145:                               # %vector.body.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB13_152
# BB#146:                               # %vector.body.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB13_147:                             # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rsi,4), %xmm0
	movups	16(%r12,%rsi,4), %xmm1
	movups	%xmm0, (%r15,%rsi,4)
	movups	%xmm1, 16(%r15,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB13_147
	jmp	.LBB13_153
.LBB13_148:
	xorl	%eax, %eax
.LBB13_149:                             # %scalar.ph.preheader
	subq	%rax, %rbp
	leaq	(%r15,%rax,4), %rcx
	leaq	(%r12,%rax,4), %rax
	.p2align	4, 0x90
.LBB13_150:                             # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	addq	$4, %rax
	decq	%rbp
	jne	.LBB13_150
	jmp	.LBB13_157
.LBB13_151:                             # %._crit_edge.i.i
	testq	%r12, %r12
	movq	48(%rsp), %rdi          # 8-byte Reload
	jne	.LBB13_157
	jmp	.LBB13_158
.LBB13_152:
	xorl	%esi, %esi
.LBB13_153:                             # %vector.body.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB13_156
# BB#154:                               # %vector.body.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r15,%rsi,4), %rdx
	leaq	112(%r12,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB13_155:                             # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB13_155
.LBB13_156:                             # %middle.block
	cmpq	%rax, %rbp
	jne	.LBB13_149
.LBB13_157:                             # %._crit_edge.thread.i.i
.Lcfi203:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	movl	72(%rsp), %r13d
.LBB13_158:                             # %._crit_edge16.i.i
	movq	%r15, 64(%rsp)
	movslq	%r13d, %rax
	movl	$0, (%r15,%rax,4)
	movl	%ebx, 76(%rsp)
	movq	%r15, %r12
.LBB13_159:                             # %_ZplIwE11CStringBaseIT_ERKS2_S1_.exit
	movslq	%r13d, %rax
	movl	$42, (%r12,%rax,4)
	leal	1(%rax), %ecx
	movl	%ecx, 72(%rsp)
	movl	$0, 4(%r12,%rax,4)
.Ltmp288:
.Lcfi204:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdi
	leaq	64(%rsp), %rsi
	callq	_ZN8NWindows5NFile5NFind12CEnumeratorWC2ERK11CStringBaseIwE
.Ltmp289:
# BB#160:
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_162
# BB#161:
.Lcfi205:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_162:                             # %_ZN11CStringBaseIwED2Ev.exit397
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	96(%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	leaq	304(%rsp), %r14
	leaq	112(%rsp), %r12
	jmp	.LBB13_164
	.p2align	4, 0x90
.LBB13_163:                             #   in Loop: Header=BB13_164 Depth=1
	incl	%r13d
.LBB13_164:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_185 Depth 2
                                        #     Child Loop BB13_191 Depth 2
                                        #     Child Loop BB13_221 Depth 2
	leaq	344(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
.Ltmp291:
.Lcfi206:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znam
.Ltmp292:
# BB#165:                               #   in Loop: Header=BB13_164 Depth=1
	movq	%rax, 344(%rsp)
	movl	$0, (%rax)
	movl	$4, 356(%rsp)
.Ltmp294:
.Lcfi207:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdi
	movq	%r14, %rsi
	leaq	31(%rsp), %rdx
	callq	_ZN8NWindows5NFile5NFind12CEnumeratorW4NextERNS1_10CFileInfoWERb
.Ltmp295:
# BB#166:                               #   in Loop: Header=BB13_164 Depth=1
	testb	%al, %al
	je	.LBB13_173
# BB#167:                               #   in Loop: Header=BB13_164 Depth=1
	cmpb	$0, 31(%rsp)
	je	.LBB13_188
# BB#168:                               #   in Loop: Header=BB13_164 Depth=1
	cmpq	$0, 600(%rsp)
	je	.LBB13_178
# BB#169:                               #   in Loop: Header=BB13_164 Depth=1
	movzbl	%r13b, %eax
	cmpl	$255, %eax
	jne	.LBB13_178
# BB#170:                               #   in Loop: Header=BB13_164 Depth=1
	movq	600(%rsp), %rdi
	movq	(%rdi), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movslq	12(%rcx), %rsi
	movslq	108(%rcx), %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
.Ltmp305:
.Lcfi208:
	.cfi_escape 0x2e, 0x00
	callq	*(%rax)
.Ltmp306:
# BB#171:                               #   in Loop: Header=BB13_164 Depth=1
	testl	%eax, %eax
	je	.LBB13_178
# BB#172:                               #   in Loop: Header=BB13_164 Depth=1
	movl	$1, %ebp
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB13_228
	.p2align	4, 0x90
.LBB13_173:                             #   in Loop: Header=BB13_164 Depth=1
.Lcfi209:
	.cfi_escape 0x2e, 0x00
	callq	__errno_location
	movl	(%rax), %ebx
.Ltmp296:
.Lcfi210:
	.cfi_escape 0x2e, 0x00
	movq	616(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp297:
# BB#174:                               #   in Loop: Header=BB13_164 Depth=1
	movq	616(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	12(%rdx)
.Ltmp298:
.Lcfi211:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp299:
# BB#175:                               # %.noexc407
                                        #   in Loop: Header=BB13_164 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rbp
	leaq	1(%rbp), %r15
	testl	%r15d, %r15d
	je	.LBB13_183
# BB#176:                               # %._crit_edge16.i.i.i402
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	%r15, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp300:
.Lcfi212:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp301:
# BB#177:                               # %.noexc.i403
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r15d, 12(%rbx)
	jmp	.LBB13_184
	.p2align	4, 0x90
.LBB13_178:                             #   in Loop: Header=BB13_164 Depth=1
	leaq	120(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$8, 136(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 112(%rsp)
.Ltmp308:
.Lcfi213:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
.Ltmp309:
# BB#179:                               # %_ZN13CObjectVectorI11CStringBaseIwEEC2ERKS2_.exit
                                        #   in Loop: Header=BB13_164 Depth=1
.Ltmp314:
.Lcfi214:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp315:
# BB#180:                               # %.noexc420
                                        #   in Loop: Header=BB13_164 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	352(%rsp), %r15
	leaq	1(%r15), %rbp
	testl	%ebp, %ebp
	je	.LBB13_189
# BB#181:                               # %._crit_edge16.i.i.i415
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp316:
.Lcfi215:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp317:
# BB#182:                               # %.noexc.i416
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	jmp	.LBB13_190
.LBB13_183:                             #   in Loop: Header=BB13_164 Depth=1
	xorl	%eax, %eax
.LBB13_184:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i404
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	.p2align	4, 0x90
.LBB13_185:                             #   Parent Loop BB13_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB13_185
# BB#186:                               #   in Loop: Header=BB13_164 Depth=1
	movl	%ebp, 8(%rbx)
.Ltmp303:
.Lcfi216:
	.cfi_escape 0x2e, 0x00
	movq	608(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp304:
# BB#187:                               # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit411
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	608(%rsp), %rax
	movq	%rax, %rdx
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
.LBB13_188:                             #   in Loop: Header=BB13_164 Depth=1
	movl	$14, %ebp
	jmp	.LBB13_228
.LBB13_189:                             #   in Loop: Header=BB13_164 Depth=1
	xorl	%eax, %eax
.LBB13_190:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i417
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	344(%rsp), %rcx
	.p2align	4, 0x90
.LBB13_191:                             #   Parent Loop BB13_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB13_191
# BB#192:                               #   in Loop: Header=BB13_164 Depth=1
	movl	%r15d, 8(%rbx)
.Ltmp319:
.Lcfi217:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp320:
	leaq	240(%rsp), %rbp
# BB#193:                               #   in Loop: Header=BB13_164 Depth=1
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 124(%rsp)
	leaq	248(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$8, 264(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 240(%rsp)
.Ltmp321:
.Lcfi218:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
.Ltmp322:
# BB#194:                               #   in Loop: Header=BB13_164 Depth=1
	movl	336(%rsp), %eax
	xorl	%ecx, %ecx
	testb	$16, %al
	sete	%cl
.Ltmp327:
.Lcfi219:
	.cfi_escape 0x2e, 0x00
	xorl	%esi, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	_ZNK9NWildcard11CCensorNode15CheckPathToRootEbR13CObjectVectorI11CStringBaseIwEEb
	movl	%eax, %ebx
.Ltmp328:
# BB#195:                               #   in Loop: Header=BB13_164 Depth=1
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 240(%rsp)
.Ltmp338:
.Lcfi220:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp339:
# BB#196:                               #   in Loop: Header=BB13_164 Depth=1
.Ltmp344:
.Lcfi221:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp345:
# BB#197:                               # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit431
                                        #   in Loop: Header=BB13_164 Depth=1
	movl	$16, %ebp
	testb	%bl, %bl
	jne	.LBB13_226
# BB#198:                               #   in Loop: Header=BB13_164 Depth=1
	movl	336(%rsp), %eax
	xorl	%ecx, %ecx
	testb	$16, %al
	sete	%cl
.Ltmp347:
.Lcfi222:
	.cfi_escape 0x2e, 0x00
	movl	$1, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rdx
	callq	_ZNK9NWildcard11CCensorNode15CheckPathToRootEbR13CObjectVectorI11CStringBaseIwEEb
.Ltmp348:
# BB#199:                               #   in Loop: Header=BB13_164 Depth=1
	testb	%al, %al
	je	.LBB13_202
# BB#200:                               #   in Loop: Header=BB13_164 Depth=1
.Ltmp349:
.Lcfi223:
	.cfi_escape 0x2e, 0x00
	movl	40(%rsp), %edi          # 4-byte Reload
	movl	44(%rsp), %esi          # 4-byte Reload
	movq	%r14, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	_Z14AddDirFileInfoiiRKN8NWindows5NFile5NFind10CFileInfoWER13CObjectVectorI8CDirItemE
.Ltmp350:
# BB#201:                               #   in Loop: Header=BB13_164 Depth=1
	movl	336(%rsp), %ecx
	testb	$16, %cl
	movb	$1, %r15b
	je	.LBB13_203
	jmp	.LBB13_204
.LBB13_202:                             # %._crit_edge753
                                        #   in Loop: Header=BB13_164 Depth=1
	movl	336(%rsp), %ecx
.LBB13_203:                             #   in Loop: Header=BB13_164 Depth=1
	movb	11(%rsp), %r15b         # 1-byte Reload
.LBB13_204:                             #   in Loop: Header=BB13_164 Depth=1
	testb	$16, %cl
	je	.LBB13_226
# BB#205:                               #   in Loop: Header=BB13_164 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 12(%rax)
	je	.LBB13_207
.LBB13_210:                             #   in Loop: Header=BB13_164 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB13_211
.LBB13_207:                             #   in Loop: Header=BB13_164 Depth=1
.Ltmp352:
.Lcfi224:
	.cfi_escape 0x2e, 0x00
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	344(%rsp), %rsi
	callq	_ZNK9NWildcard11CCensorNode11FindSubNodeERK11CStringBaseIwE
.Ltmp353:
# BB#208:                               #   in Loop: Header=BB13_164 Depth=1
	testl	%eax, %eax
	js	.LBB13_210
# BB#209:                               #   in Loop: Header=BB13_164 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	40(%rcx), %rcx
	cltq
	movq	(%rcx,%rax,8), %rbx
.LBB13_211:                             #   in Loop: Header=BB13_164 Depth=1
	andb	$1, %r15b
	sete	%al
	testq	%rbx, %rbx
	jne	.LBB13_213
# BB#212:                               #   in Loop: Header=BB13_164 Depth=1
	testb	%al, %al
	jne	.LBB13_226
.LBB13_213:                             #   in Loop: Header=BB13_164 Depth=1
.Ltmp354:
.Lcfi225:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	movq	104(%rsp), %rsi         # 8-byte Reload
	callq	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
.Ltmp355:
# BB#214:                               #   in Loop: Header=BB13_164 Depth=1
	testq	%rbx, %rbx
	jne	.LBB13_224
# BB#215:                               #   in Loop: Header=BB13_164 Depth=1
.Ltmp356:
.Lcfi226:
	.cfi_escape 0x2e, 0x00
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp357:
# BB#216:                               # %.noexc441
                                        #   in Loop: Header=BB13_164 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	352(%rsp), %rsi
	leaq	1(%rsi), %rbp
	testl	%ebp, %ebp
	je	.LBB13_219
# BB#217:                               # %._crit_edge16.i.i.i436
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	%rsi, 416(%rsp)         # 8-byte Spill
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp358:
.Lcfi227:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	_Znam
.Ltmp359:
# BB#218:                               # %.noexc.i437
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%ebp, 12(%rbx)
	movq	416(%rsp), %rsi         # 8-byte Reload
	jmp	.LBB13_220
.LBB13_219:                             #   in Loop: Header=BB13_164 Depth=1
	xorl	%eax, %eax
.LBB13_220:                             # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i438
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	344(%rsp), %rcx
	.p2align	4, 0x90
.LBB13_221:                             #   Parent Loop BB13_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB13_221
# BB#222:                               #   in Loop: Header=BB13_164 Depth=1
	movl	%esi, 8(%rbx)
.Ltmp361:
.Lcfi228:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp362:
# BB#223:                               # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit445
                                        #   in Loop: Header=BB13_164 Depth=1
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 124(%rsp)
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB13_224:                             #   in Loop: Header=BB13_164 Depth=1
	xorl	%eax, %eax
	testb	%r15b, %r15b
	setne	%al
	movq	344(%rsp), %rcx
	movl	352(%rsp), %r8d
.Ltmp363:
.Lcfi229:
	.cfi_escape 0x2e, 0x30
	movq	%rbx, %rdi
	movl	40(%rsp), %esi          # 4-byte Reload
	movl	44(%rsp), %edx          # 4-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	pushq	616(%rsp)
.Lcfi230:
	.cfi_adjust_cfa_offset 8
	pushq	616(%rsp)
.Lcfi231:
	.cfi_adjust_cfa_offset 8
	pushq	616(%rsp)
.Lcfi232:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi233:
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
.Lcfi234:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi235:
	.cfi_adjust_cfa_offset 8
	callq	_ZL22EnumerateDirItems_SpecRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwES6_RK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE
	addq	$48, %rsp
.Lcfi236:
	.cfi_adjust_cfa_offset -48
.Ltmp364:
# BB#225:                               #   in Loop: Header=BB13_164 Depth=1
	xorl	%ebp, %ebp
	testl	%eax, %eax
	setne	%bpl
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB13_226:                             #   in Loop: Header=BB13_164 Depth=1
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 112(%rsp)
.Ltmp375:
.Lcfi237:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp376:
# BB#227:                               #   in Loop: Header=BB13_164 Depth=1
.Ltmp381:
.Lcfi238:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp382:
.LBB13_228:                             #   in Loop: Header=BB13_164 Depth=1
	movq	344(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_230
# BB#229:                               #   in Loop: Header=BB13_164 Depth=1
.Lcfi239:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_230:                             # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit453
                                        #   in Loop: Header=BB13_164 Depth=1
	testl	%ebp, %ebp
	je	.LBB13_163
# BB#231:                               # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit453
                                        #   in Loop: Header=BB13_164 Depth=1
	cmpl	$16, %ebp
	je	.LBB13_163
# BB#232:                               # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit453
	cmpl	$14, %ebp
	jne	.LBB13_234
# BB#233:                               # %.loopexit.loopexit788
	xorl	%ebp, %ebp
	jmp	.LBB13_235
.LBB13_234:                             # %.loopexit.loopexit
	movl	12(%rsp), %ebp          # 4-byte Reload
.LBB13_235:                             # %.loopexit
	movq	464(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_237
# BB#236:
.Lcfi240:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_237:                             # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp387:
.Lcfi241:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
.Ltmp388:
# BB#238:
	movq	448(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_240
# BB#239:
.Lcfi242:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_240:                             # %_ZN11CStringBaseIcED2Ev.exit.i.i
	movq	432(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_242
# BB#241:
.Lcfi243:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_242:
	movl	%ebp, %eax
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_243:
.Ltmp198:
	movq	%rax, %r15
.Lcfi244:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB13_279
.LBB13_244:
.Ltmp360:
	movq	%rax, %r15
.Lcfi245:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB13_293
.LBB13_245:
.Ltmp223:
	jmp	.LBB13_315
.LBB13_246:
.Ltmp217:
	movq	%rax, %r15
.Ltmp218:
.Lcfi246:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp219:
	jmp	.LBB13_316
.LBB13_247:
.Ltmp220:
.Lcfi247:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_248:
.Ltmp193:
	jmp	.LBB13_278
.LBB13_249:
.Ltmp287:
	movq	%rax, %r15
	testq	%r12, %r12
	je	.LBB13_343
# BB#250:
.Lcfi248:
	.cfi_escape 0x2e, 0x00
	movq	48(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB13_342
.LBB13_251:
.Ltmp279:
	movq	%rax, %r15
.Lcfi249:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB13_253
.LBB13_252:
.Ltmp250:
	movq	%rax, %r15
.Lcfi250:
	.cfi_escape 0x2e, 0x00
.LBB13_253:                             # %.body356
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB13_296
.LBB13_254:
.Ltmp230:
	movq	%rax, %r15
.Lcfi251:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB13_316
.LBB13_255:
.Ltmp206:
	jmp	.LBB13_278
.LBB13_256:
.Ltmp272:
	jmp	.LBB13_295
.LBB13_257:
.Ltmp266:
	movq	%rax, %r15
.Ltmp267:
.Lcfi252:
	.cfi_escape 0x2e, 0x00
	leaq	272(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp268:
	jmp	.LBB13_296
.LBB13_258:
.Ltmp269:
.Lcfi253:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_259:
.Ltmp255:
	movq	%rax, %r15
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 272(%rsp)
.Ltmp256:
.Lcfi254:
	.cfi_escape 0x2e, 0x00
	leaq	272(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp257:
# BB#260:
.Ltmp262:
.Lcfi255:
	.cfi_escape 0x2e, 0x00
	leaq	272(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp263:
	jmp	.LBB13_296
.LBB13_261:
.Ltmp258:
	movq	%rax, %rbx
.Ltmp259:
.Lcfi256:
	.cfi_escape 0x2e, 0x00
	leaq	272(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp260:
	jmp	.LBB13_348
.LBB13_262:
.Ltmp261:
.Lcfi257:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_263:
.Ltmp166:
	movq	%rax, %r15
.Lcfi258:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB13_284
.LBB13_264:
.Ltmp188:
	jmp	.LBB13_315
.LBB13_265:
.Ltmp182:
	movq	%rax, %r15
.Ltmp183:
.Lcfi259:
	.cfi_escape 0x2e, 0x00
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp184:
	jmp	.LBB13_316
.LBB13_266:
.Ltmp185:
.Lcfi260:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_267:
.Ltmp158:
	movq	%rax, %r15
.Lcfi261:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB13_316
.LBB13_268:
.Ltmp389:
	movq	%rax, %r15
	movq	448(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_270
# BB#269:
.Lcfi262:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_270:                             # %_ZN11CStringBaseIcED2Ev.exit3.i.i
	movq	432(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_343
# BB#271:
.Lcfi263:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB13_342
.LBB13_272:
.Ltmp290:
	movq	%rax, %r15
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_343
# BB#273:
.Lcfi264:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB13_342
.LBB13_274:
.Ltmp236:
	jmp	.LBB13_291
.LBB13_275:
.Ltmp239:
	movq	%rax, %r15
	jmp	.LBB13_298
.LBB13_276:
.Ltmp233:
	jmp	.LBB13_315
.LBB13_277:
.Ltmp203:
.LBB13_278:                             # %.body330
	movq	%rax, %r15
.LBB13_279:                             # %.body330
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 208(%rsp)
.Ltmp207:
.Lcfi265:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp208:
# BB#280:
.Ltmp213:
.Lcfi266:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp214:
	jmp	.LBB13_316
.LBB13_281:
.Ltmp209:
	movq	%rax, %rbx
.Ltmp210:
.Lcfi267:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp211:
	jmp	.LBB13_348
.LBB13_282:
.Ltmp212:
.Lcfi268:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_283:
.Ltmp171:
	movq	%rax, %r15
.LBB13_284:                             # %.body312
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 160(%rsp)
.Ltmp172:
.Lcfi269:
	.cfi_escape 0x2e, 0x00
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp173:
# BB#285:
.Ltmp178:
.Lcfi270:
	.cfi_escape 0x2e, 0x00
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp179:
	jmp	.LBB13_316
.LBB13_286:
.Ltmp174:
	movq	%rax, %rbx
.Ltmp175:
.Lcfi271:
	.cfi_escape 0x2e, 0x00
	leaq	160(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp176:
	jmp	.LBB13_348
.LBB13_287:
.Ltmp177:
.Lcfi272:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_288:
.Ltmp365:
	jmp	.LBB13_324
.LBB13_289:
.Ltmp149:
	movq	%rax, %r15
	jmp	.LBB13_318
.LBB13_290:
.Ltmp146:
.LBB13_291:
	movq	%rax, %r15
	jmp	.LBB13_321
.LBB13_292:
.Ltmp318:
	movq	%rax, %r15
.Lcfi273:
	.cfi_escape 0x2e, 0x00
.LBB13_293:
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB13_325
.LBB13_294:
.Ltmp282:
.LBB13_295:
	movq	%rax, %r15
.LBB13_296:
	movq	520(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_298
# BB#297:
.Lcfi274:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_298:
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_321
# BB#299:
.Lcfi275:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB13_320
.LBB13_300:
.Ltmp340:
	movq	%rax, %r15
.Ltmp341:
.Lcfi276:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp342:
	jmp	.LBB13_325
.LBB13_301:
.Ltmp343:
.Lcfi277:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_302:
.Ltmp329:
	movq	%rax, %r15
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 240(%rsp)
.Ltmp330:
.Lcfi278:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp331:
# BB#303:
.Ltmp336:
.Lcfi279:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp337:
	jmp	.LBB13_325
.LBB13_304:
.Ltmp332:
	movq	%rax, %rbx
.Ltmp333:
.Lcfi280:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp334:
	jmp	.LBB13_348
.LBB13_305:
.Ltmp335:
.Lcfi281:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_306:
.Ltmp323:
	movq	%rax, %r15
.Ltmp324:
.Lcfi282:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp325:
	jmp	.LBB13_325
.LBB13_307:
.Ltmp326:
.Lcfi283:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_308:
.Ltmp310:
	movq	%rax, %r15
.Ltmp311:
.Lcfi284:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp312:
	jmp	.LBB13_333
.LBB13_309:
.Ltmp313:
.Lcfi285:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_310:
.Ltmp383:
	jmp	.LBB13_332
.LBB13_311:
.Ltmp377:
	movq	%rax, %r15
.Ltmp378:
.Lcfi286:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp379:
	jmp	.LBB13_333
.LBB13_312:
.Ltmp380:
.Lcfi287:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_313:
.Ltmp346:
	jmp	.LBB13_324
.LBB13_314:
.Ltmp161:
.LBB13_315:
	movq	%rax, %r15
.LBB13_316:
	movq	400(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_318
# BB#317:
.Lcfi288:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_318:
	movq	192(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_321
# BB#319:
.Lcfi289:
	.cfi_escape 0x2e, 0x00
.LBB13_320:
	callq	_ZdaPv
.LBB13_321:
.Ltmp283:
.Lcfi290:
	.cfi_escape 0x2e, 0x00
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp284:
	jmp	.LBB13_343
.LBB13_322:
.Ltmp302:
	movq	%rax, %r15
.Lcfi291:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB13_333
.LBB13_323:
.Ltmp351:
.LBB13_324:
	movq	%rax, %r15
.LBB13_325:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 112(%rsp)
.Ltmp366:
.Lcfi292:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp367:
# BB#326:
.Ltmp372:
.Lcfi293:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp373:
	jmp	.LBB13_333
.LBB13_327:
.Ltmp368:
	movq	%rax, %rbx
.Ltmp369:
.Lcfi294:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp370:
	jmp	.LBB13_348
.LBB13_328:
.Ltmp371:
.Lcfi295:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_329:
.Ltmp374:
	movq	%rax, %rbx
	jmp	.LBB13_348
.LBB13_330:
.Ltmp293:
	movq	%rax, %r15
	jmp	.LBB13_335
.LBB13_331:
.Ltmp307:
.LBB13_332:
	movq	%rax, %r15
.LBB13_333:
	movq	344(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_335
# BB#334:
.Lcfi296:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_335:
	movq	464(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_337
# BB#336:
.Lcfi297:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_337:                             # %_ZN11CStringBaseIwED2Ev.exit.i371
.Ltmp384:
.Lcfi298:
	.cfi_escape 0x2e, 0x00
	leaq	424(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind9CFindFile5CloseEv
.Ltmp385:
# BB#338:
	movq	448(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_340
# BB#339:
.Lcfi299:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_340:                             # %_ZN11CStringBaseIcED2Ev.exit.i.i372
	movq	432(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_343
# BB#341:
.Lcfi300:
	.cfi_escape 0x2e, 0x00
.LBB13_342:                             # %unwind_resume
	callq	_ZdaPv
.LBB13_343:                             # %unwind_resume
.Lcfi301:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB13_344:
.Ltmp386:
	movq	%rax, %rbx
	movq	448(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_346
# BB#345:
.Lcfi302:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_346:                             # %_ZN11CStringBaseIcED2Ev.exit3.i.i373
	movq	432(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_348
# BB#347:
.Lcfi303:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB13_348:                             # %.body319
.Lcfi304:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZL17EnumerateDirItemsRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwERK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE, .Lfunc_end13-_ZL17EnumerateDirItemsRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwERK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\214\b"                # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\203\b"                # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp194-.Lfunc_begin8  #   Call between .Lfunc_begin8 and .Ltmp194
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp195-.Ltmp194       #   Call between .Ltmp194 and .Ltmp195
	.long	.Ltmp206-.Lfunc_begin8  #     jumps to .Ltmp206
	.byte	0                       #   On action: cleanup
	.long	.Ltmp196-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp197-.Ltmp196       #   Call between .Ltmp196 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin8  #     jumps to .Ltmp198
	.byte	0                       #   On action: cleanup
	.long	.Ltmp201-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp202-.Ltmp201       #   Call between .Ltmp201 and .Ltmp202
	.long	.Ltmp203-.Lfunc_begin8  #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp199-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp205-.Ltmp199       #   Call between .Ltmp199 and .Ltmp205
	.long	.Ltmp206-.Lfunc_begin8  #     jumps to .Ltmp206
	.byte	0                       #   On action: cleanup
	.long	.Ltmp215-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp216-.Ltmp215       #   Call between .Ltmp215 and .Ltmp216
	.long	.Ltmp217-.Lfunc_begin8  #     jumps to .Ltmp217
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp222-.Ltmp221       #   Call between .Ltmp221 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin8  #     jumps to .Ltmp223
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin8  #     jumps to .Ltmp146
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin8  # >> Call Site 9 <<
	.long	.Ltmp148-.Ltmp147       #   Call between .Ltmp147 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin8  #     jumps to .Ltmp149
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin8  # >> Call Site 10 <<
	.long	.Ltmp155-.Ltmp150       #   Call between .Ltmp150 and .Ltmp155
	.long	.Ltmp161-.Lfunc_begin8  #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin8  # >> Call Site 11 <<
	.long	.Ltmp157-.Ltmp156       #   Call between .Ltmp156 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin8  #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin8  # >> Call Site 12 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp171-.Lfunc_begin8  #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp164-.Lfunc_begin8  # >> Call Site 13 <<
	.long	.Ltmp165-.Ltmp164       #   Call between .Ltmp164 and .Ltmp165
	.long	.Ltmp166-.Lfunc_begin8  #     jumps to .Ltmp166
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin8  # >> Call Site 14 <<
	.long	.Ltmp227-.Ltmp224       #   Call between .Ltmp224 and .Ltmp227
	.long	.Ltmp233-.Lfunc_begin8  #     jumps to .Ltmp233
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin8  # >> Call Site 15 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin8  #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin8  # >> Call Site 16 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp161-.Lfunc_begin8  #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp167-.Lfunc_begin8  # >> Call Site 17 <<
	.long	.Ltmp170-.Ltmp167       #   Call between .Ltmp167 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin8  #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin8  # >> Call Site 18 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin8  #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin8  # >> Call Site 19 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin8  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin8  # >> Call Site 20 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp233-.Lfunc_begin8  #     jumps to .Ltmp233
	.byte	0                       #   On action: cleanup
	.long	.Ltmp191-.Lfunc_begin8  # >> Call Site 21 <<
	.long	.Ltmp192-.Ltmp191       #   Call between .Ltmp191 and .Ltmp192
	.long	.Ltmp193-.Lfunc_begin8  #     jumps to .Ltmp193
	.byte	0                       #   On action: cleanup
	.long	.Ltmp231-.Lfunc_begin8  # >> Call Site 22 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin8  #     jumps to .Ltmp233
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin8  # >> Call Site 23 <<
	.long	.Ltmp235-.Ltmp234       #   Call between .Ltmp234 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin8  #     jumps to .Ltmp236
	.byte	0                       #   On action: cleanup
	.long	.Ltmp237-.Lfunc_begin8  # >> Call Site 24 <<
	.long	.Ltmp238-.Ltmp237       #   Call between .Ltmp237 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin8  #     jumps to .Ltmp239
	.byte	0                       #   On action: cleanup
	.long	.Ltmp240-.Lfunc_begin8  # >> Call Site 25 <<
	.long	.Ltmp276-.Ltmp240       #   Call between .Ltmp240 and .Ltmp276
	.long	.Ltmp282-.Lfunc_begin8  #     jumps to .Ltmp282
	.byte	0                       #   On action: cleanup
	.long	.Ltmp277-.Lfunc_begin8  # >> Call Site 26 <<
	.long	.Ltmp278-.Ltmp277       #   Call between .Ltmp277 and .Ltmp278
	.long	.Ltmp279-.Lfunc_begin8  #     jumps to .Ltmp279
	.byte	0                       #   On action: cleanup
	.long	.Ltmp242-.Lfunc_begin8  # >> Call Site 27 <<
	.long	.Ltmp247-.Ltmp242       #   Call between .Ltmp242 and .Ltmp247
	.long	.Ltmp282-.Lfunc_begin8  #     jumps to .Ltmp282
	.byte	0                       #   On action: cleanup
	.long	.Ltmp248-.Lfunc_begin8  # >> Call Site 28 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin8  #     jumps to .Ltmp250
	.byte	0                       #   On action: cleanup
	.long	.Ltmp253-.Lfunc_begin8  # >> Call Site 29 <<
	.long	.Ltmp254-.Ltmp253       #   Call between .Ltmp253 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin8  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp264-.Lfunc_begin8  # >> Call Site 30 <<
	.long	.Ltmp265-.Ltmp264       #   Call between .Ltmp264 and .Ltmp265
	.long	.Ltmp266-.Lfunc_begin8  #     jumps to .Ltmp266
	.byte	0                       #   On action: cleanup
	.long	.Ltmp270-.Lfunc_begin8  # >> Call Site 31 <<
	.long	.Ltmp271-.Ltmp270       #   Call between .Ltmp270 and .Ltmp271
	.long	.Ltmp272-.Lfunc_begin8  #     jumps to .Ltmp272
	.byte	0                       #   On action: cleanup
	.long	.Ltmp280-.Lfunc_begin8  # >> Call Site 32 <<
	.long	.Ltmp252-.Ltmp280       #   Call between .Ltmp280 and .Ltmp252
	.long	.Ltmp282-.Lfunc_begin8  #     jumps to .Ltmp282
	.byte	0                       #   On action: cleanup
	.long	.Ltmp252-.Lfunc_begin8  # >> Call Site 33 <<
	.long	.Ltmp285-.Ltmp252       #   Call between .Ltmp252 and .Ltmp285
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp285-.Lfunc_begin8  # >> Call Site 34 <<
	.long	.Ltmp286-.Ltmp285       #   Call between .Ltmp285 and .Ltmp286
	.long	.Ltmp287-.Lfunc_begin8  #     jumps to .Ltmp287
	.byte	0                       #   On action: cleanup
	.long	.Ltmp288-.Lfunc_begin8  # >> Call Site 35 <<
	.long	.Ltmp289-.Ltmp288       #   Call between .Ltmp288 and .Ltmp289
	.long	.Ltmp290-.Lfunc_begin8  #     jumps to .Ltmp290
	.byte	0                       #   On action: cleanup
	.long	.Ltmp291-.Lfunc_begin8  # >> Call Site 36 <<
	.long	.Ltmp292-.Ltmp291       #   Call between .Ltmp291 and .Ltmp292
	.long	.Ltmp293-.Lfunc_begin8  #     jumps to .Ltmp293
	.byte	0                       #   On action: cleanup
	.long	.Ltmp294-.Lfunc_begin8  # >> Call Site 37 <<
	.long	.Ltmp299-.Ltmp294       #   Call between .Ltmp294 and .Ltmp299
	.long	.Ltmp307-.Lfunc_begin8  #     jumps to .Ltmp307
	.byte	0                       #   On action: cleanup
	.long	.Ltmp300-.Lfunc_begin8  # >> Call Site 38 <<
	.long	.Ltmp301-.Ltmp300       #   Call between .Ltmp300 and .Ltmp301
	.long	.Ltmp302-.Lfunc_begin8  #     jumps to .Ltmp302
	.byte	0                       #   On action: cleanup
	.long	.Ltmp308-.Lfunc_begin8  # >> Call Site 39 <<
	.long	.Ltmp309-.Ltmp308       #   Call between .Ltmp308 and .Ltmp309
	.long	.Ltmp310-.Lfunc_begin8  #     jumps to .Ltmp310
	.byte	0                       #   On action: cleanup
	.long	.Ltmp314-.Lfunc_begin8  # >> Call Site 40 <<
	.long	.Ltmp315-.Ltmp314       #   Call between .Ltmp314 and .Ltmp315
	.long	.Ltmp351-.Lfunc_begin8  #     jumps to .Ltmp351
	.byte	0                       #   On action: cleanup
	.long	.Ltmp316-.Lfunc_begin8  # >> Call Site 41 <<
	.long	.Ltmp317-.Ltmp316       #   Call between .Ltmp316 and .Ltmp317
	.long	.Ltmp318-.Lfunc_begin8  #     jumps to .Ltmp318
	.byte	0                       #   On action: cleanup
	.long	.Ltmp303-.Lfunc_begin8  # >> Call Site 42 <<
	.long	.Ltmp304-.Ltmp303       #   Call between .Ltmp303 and .Ltmp304
	.long	.Ltmp307-.Lfunc_begin8  #     jumps to .Ltmp307
	.byte	0                       #   On action: cleanup
	.long	.Ltmp319-.Lfunc_begin8  # >> Call Site 43 <<
	.long	.Ltmp320-.Ltmp319       #   Call between .Ltmp319 and .Ltmp320
	.long	.Ltmp351-.Lfunc_begin8  #     jumps to .Ltmp351
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin8  # >> Call Site 44 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin8  #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin8  # >> Call Site 45 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin8  #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp338-.Lfunc_begin8  # >> Call Site 46 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin8  #     jumps to .Ltmp340
	.byte	0                       #   On action: cleanup
	.long	.Ltmp344-.Lfunc_begin8  # >> Call Site 47 <<
	.long	.Ltmp345-.Ltmp344       #   Call between .Ltmp344 and .Ltmp345
	.long	.Ltmp346-.Lfunc_begin8  #     jumps to .Ltmp346
	.byte	0                       #   On action: cleanup
	.long	.Ltmp347-.Lfunc_begin8  # >> Call Site 48 <<
	.long	.Ltmp350-.Ltmp347       #   Call between .Ltmp347 and .Ltmp350
	.long	.Ltmp351-.Lfunc_begin8  #     jumps to .Ltmp351
	.byte	0                       #   On action: cleanup
	.long	.Ltmp352-.Lfunc_begin8  # >> Call Site 49 <<
	.long	.Ltmp357-.Ltmp352       #   Call between .Ltmp352 and .Ltmp357
	.long	.Ltmp365-.Lfunc_begin8  #     jumps to .Ltmp365
	.byte	0                       #   On action: cleanup
	.long	.Ltmp358-.Lfunc_begin8  # >> Call Site 50 <<
	.long	.Ltmp359-.Ltmp358       #   Call between .Ltmp358 and .Ltmp359
	.long	.Ltmp360-.Lfunc_begin8  #     jumps to .Ltmp360
	.byte	0                       #   On action: cleanup
	.long	.Ltmp361-.Lfunc_begin8  # >> Call Site 51 <<
	.long	.Ltmp364-.Ltmp361       #   Call between .Ltmp361 and .Ltmp364
	.long	.Ltmp365-.Lfunc_begin8  #     jumps to .Ltmp365
	.byte	0                       #   On action: cleanup
	.long	.Ltmp375-.Lfunc_begin8  # >> Call Site 52 <<
	.long	.Ltmp376-.Ltmp375       #   Call between .Ltmp375 and .Ltmp376
	.long	.Ltmp377-.Lfunc_begin8  #     jumps to .Ltmp377
	.byte	0                       #   On action: cleanup
	.long	.Ltmp381-.Lfunc_begin8  # >> Call Site 53 <<
	.long	.Ltmp382-.Ltmp381       #   Call between .Ltmp381 and .Ltmp382
	.long	.Ltmp383-.Lfunc_begin8  #     jumps to .Ltmp383
	.byte	0                       #   On action: cleanup
	.long	.Ltmp387-.Lfunc_begin8  # >> Call Site 54 <<
	.long	.Ltmp388-.Ltmp387       #   Call between .Ltmp387 and .Ltmp388
	.long	.Ltmp389-.Lfunc_begin8  #     jumps to .Ltmp389
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin8  # >> Call Site 55 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin8  #     jumps to .Ltmp220
	.byte	1                       #   On action: 1
	.long	.Ltmp267-.Lfunc_begin8  # >> Call Site 56 <<
	.long	.Ltmp268-.Ltmp267       #   Call between .Ltmp267 and .Ltmp268
	.long	.Ltmp269-.Lfunc_begin8  #     jumps to .Ltmp269
	.byte	1                       #   On action: 1
	.long	.Ltmp256-.Lfunc_begin8  # >> Call Site 57 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp258-.Lfunc_begin8  #     jumps to .Ltmp258
	.byte	1                       #   On action: 1
	.long	.Ltmp262-.Lfunc_begin8  # >> Call Site 58 <<
	.long	.Ltmp263-.Ltmp262       #   Call between .Ltmp262 and .Ltmp263
	.long	.Ltmp374-.Lfunc_begin8  #     jumps to .Ltmp374
	.byte	1                       #   On action: 1
	.long	.Ltmp259-.Lfunc_begin8  # >> Call Site 59 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin8  #     jumps to .Ltmp261
	.byte	1                       #   On action: 1
	.long	.Ltmp183-.Lfunc_begin8  # >> Call Site 60 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin8  #     jumps to .Ltmp185
	.byte	1                       #   On action: 1
	.long	.Ltmp207-.Lfunc_begin8  # >> Call Site 61 <<
	.long	.Ltmp208-.Ltmp207       #   Call between .Ltmp207 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin8  #     jumps to .Ltmp209
	.byte	1                       #   On action: 1
	.long	.Ltmp213-.Lfunc_begin8  # >> Call Site 62 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp374-.Lfunc_begin8  #     jumps to .Ltmp374
	.byte	1                       #   On action: 1
	.long	.Ltmp210-.Lfunc_begin8  # >> Call Site 63 <<
	.long	.Ltmp211-.Ltmp210       #   Call between .Ltmp210 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin8  #     jumps to .Ltmp212
	.byte	1                       #   On action: 1
	.long	.Ltmp172-.Lfunc_begin8  # >> Call Site 64 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin8  #     jumps to .Ltmp174
	.byte	1                       #   On action: 1
	.long	.Ltmp178-.Lfunc_begin8  # >> Call Site 65 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp374-.Lfunc_begin8  #     jumps to .Ltmp374
	.byte	1                       #   On action: 1
	.long	.Ltmp175-.Lfunc_begin8  # >> Call Site 66 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin8  #     jumps to .Ltmp177
	.byte	1                       #   On action: 1
	.long	.Ltmp341-.Lfunc_begin8  # >> Call Site 67 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp343-.Lfunc_begin8  #     jumps to .Ltmp343
	.byte	1                       #   On action: 1
	.long	.Ltmp330-.Lfunc_begin8  # >> Call Site 68 <<
	.long	.Ltmp331-.Ltmp330       #   Call between .Ltmp330 and .Ltmp331
	.long	.Ltmp332-.Lfunc_begin8  #     jumps to .Ltmp332
	.byte	1                       #   On action: 1
	.long	.Ltmp336-.Lfunc_begin8  # >> Call Site 69 <<
	.long	.Ltmp337-.Ltmp336       #   Call between .Ltmp336 and .Ltmp337
	.long	.Ltmp374-.Lfunc_begin8  #     jumps to .Ltmp374
	.byte	1                       #   On action: 1
	.long	.Ltmp333-.Lfunc_begin8  # >> Call Site 70 <<
	.long	.Ltmp334-.Ltmp333       #   Call between .Ltmp333 and .Ltmp334
	.long	.Ltmp335-.Lfunc_begin8  #     jumps to .Ltmp335
	.byte	1                       #   On action: 1
	.long	.Ltmp324-.Lfunc_begin8  # >> Call Site 71 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin8  #     jumps to .Ltmp326
	.byte	1                       #   On action: 1
	.long	.Ltmp311-.Lfunc_begin8  # >> Call Site 72 <<
	.long	.Ltmp312-.Ltmp311       #   Call between .Ltmp311 and .Ltmp312
	.long	.Ltmp313-.Lfunc_begin8  #     jumps to .Ltmp313
	.byte	1                       #   On action: 1
	.long	.Ltmp378-.Lfunc_begin8  # >> Call Site 73 <<
	.long	.Ltmp379-.Ltmp378       #   Call between .Ltmp378 and .Ltmp379
	.long	.Ltmp380-.Lfunc_begin8  #     jumps to .Ltmp380
	.byte	1                       #   On action: 1
	.long	.Ltmp283-.Lfunc_begin8  # >> Call Site 74 <<
	.long	.Ltmp284-.Ltmp283       #   Call between .Ltmp283 and .Ltmp284
	.long	.Ltmp374-.Lfunc_begin8  #     jumps to .Ltmp374
	.byte	1                       #   On action: 1
	.long	.Ltmp366-.Lfunc_begin8  # >> Call Site 75 <<
	.long	.Ltmp367-.Ltmp366       #   Call between .Ltmp366 and .Ltmp367
	.long	.Ltmp368-.Lfunc_begin8  #     jumps to .Ltmp368
	.byte	1                       #   On action: 1
	.long	.Ltmp372-.Lfunc_begin8  # >> Call Site 76 <<
	.long	.Ltmp373-.Ltmp372       #   Call between .Ltmp372 and .Ltmp373
	.long	.Ltmp374-.Lfunc_begin8  #     jumps to .Ltmp374
	.byte	1                       #   On action: 1
	.long	.Ltmp369-.Lfunc_begin8  # >> Call Site 77 <<
	.long	.Ltmp370-.Ltmp369       #   Call between .Ltmp369 and .Ltmp370
	.long	.Ltmp371-.Lfunc_begin8  #     jumps to .Ltmp371
	.byte	1                       #   On action: 1
	.long	.Ltmp384-.Lfunc_begin8  # >> Call Site 78 <<
	.long	.Ltmp385-.Ltmp384       #   Call between .Ltmp384 and .Ltmp385
	.long	.Ltmp386-.Lfunc_begin8  #     jumps to .Ltmp386
	.byte	1                       #   On action: 1
	.long	.Ltmp385-.Lfunc_begin8  # >> Call Site 79 <<
	.long	.Lfunc_end13-.Ltmp385   #   Call between .Ltmp385 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi305:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi306:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi307:
	.cfi_def_cfa_offset 32
.Lcfi308:
	.cfi_offset %rbx, -24
.Lcfi309:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp390:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp391:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB14_2:
.Ltmp392:
	movq	%rax, %r14
.Ltmp393:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp394:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_4:
.Ltmp395:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end14-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp390-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp391-.Ltmp390       #   Call between .Ltmp390 and .Ltmp391
	.long	.Ltmp392-.Lfunc_begin9  #     jumps to .Ltmp392
	.byte	0                       #   On action: cleanup
	.long	.Ltmp391-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp393-.Ltmp391       #   Call between .Ltmp391 and .Ltmp393
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp394-.Ltmp393       #   Call between .Ltmp393 and .Ltmp394
	.long	.Ltmp395-.Lfunc_begin9  #     jumps to .Ltmp395
	.byte	1                       #   On action: 1
	.long	.Ltmp394-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end14-.Ltmp394   #   Call between .Ltmp394 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL22EnumerateDirItems_SpecRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwES6_RK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE,@function
_ZL22EnumerateDirItems_SpecRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwES6_RK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE: # @_ZL22EnumerateDirItems_SpecRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwES6_RK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:                                 # %._crit_edge16.i.i.i
	pushq	%rbp
.Lcfi310:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi311:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi312:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi313:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi314:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi315:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi316:
	.cfi_def_cfa_offset 128
.Lcfi317:
	.cfi_offset %rbx, -56
.Lcfi318:
	.cfi_offset %r12, -48
.Lcfi319:
	.cfi_offset %r13, -40
.Lcfi320:
	.cfi_offset %r14, -32
.Lcfi321:
	.cfi_offset %r15, -24
.Lcfi322:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movl	%r8d, %r15d
	movq	%rcx, %rbx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	leal	1(%r15), %ebp
	movslq	%ebp, %r14
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	movq	$0, 24(%rsp)
.Lcfi323:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r12
	movq	%r12, 16(%rsp)
	movl	$0, (%r12)
	movl	%r14d, 28(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB15_1:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rax), %ecx
	movl	%ecx, (%r12,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB15_1
# BB#2:
	movl	%r15d, 24(%rsp)
	cmpl	$7, %r15d
	movl	$16, %ecx
	movl	$4, %eax
	cmovgl	%ecx, %eax
	cmpl	$64, %r15d
	jl	.LBB15_4
# BB#3:                                 # %select.true.sink
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
.LBB15_4:                               # %select.end
	testl	%eax, %eax
	movl	$1, %ecx
	cmovgl	%eax, %ecx
	leal	2(%r15,%rcx), %ebx
	cmpl	%ebp, %ebx
	je	.LBB15_24
# BB#5:
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp396:
.Lcfi324:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r13
.Ltmp397:
# BB#6:                                 # %.noexc43
	testl	%r15d, %r15d
	js	.LBB15_23
# BB#7:                                 # %.preheader.i.i
	je	.LBB15_22
# BB#8:                                 # %.lr.ph.i.i
	movslq	%r15d, %rax
	cmpl	$8, %r15d
	jae	.LBB15_10
# BB#9:
	xorl	%ecx, %ecx
	jmp	.LBB15_20
.LBB15_10:                              # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB15_11
# BB#12:                                # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB15_13
# BB#14:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB15_15:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rdi,4), %xmm0
	movups	16(%r12,%rdi,4), %xmm1
	movups	%xmm0, (%r13,%rdi,4)
	movups	%xmm1, 16(%r13,%rdi,4)
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB15_15
	jmp	.LBB15_16
.LBB15_11:
	xorl	%ecx, %ecx
	jmp	.LBB15_20
.LBB15_13:
	xorl	%edi, %edi
.LBB15_16:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB15_19
# BB#17:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	112(%r13,%rdi,4), %rsi
	leaq	112(%r12,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB15_18:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdi
	addq	$-32, %rdx
	jne	.LBB15_18
.LBB15_19:                              # %middle.block
	cmpq	%rcx, %rax
	je	.LBB15_22
.LBB15_20:                              # %scalar.ph.preheader
	subq	%rcx, %rax
	leaq	(%r13,%rcx,4), %rdx
	leaq	(%r12,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB15_21:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$4, %rdx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB15_21
.LBB15_22:                              # %._crit_edge.thread.i.i
.Lcfi325:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB15_23:                              # %._crit_edge16.i.i
	movq	%r13, 16(%rsp)
	movslq	%r15d, %rax
	movl	$0, (%r13,%rax,4)
	movl	%ebx, 28(%rsp)
	movq	%r13, %r12
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB15_24:                              # %_ZplIwE11CStringBaseIT_ERKS2_S1_.exit
	movslq	%r15d, %rax
	movl	$47, (%r12,%rax,4)
	movl	%ebp, 24(%rsp)
	movl	$0, (%r12,%r14,4)
.Ltmp399:
.Lcfi326:
	.cfi_escape 0x2e, 0x00
	leaq	16(%rsp), %rcx
	movq	136(%rsp), %rbp
	movq	%rbp, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	callq	_ZN9CDirItems9AddPrefixEiiRK11CStringBaseIwE
.Ltmp400:
# BB#25:
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	108(%rbp), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	%r13, %r15
	movl	8(%r13), %r13d
	movslq	%r13d, %rax
	movq	%rax, %r14
	leaq	1(%rax), %rbx
	testl	%ebx, %ebx
	je	.LBB15_26
# BB#27:                                # %._crit_edge16.i.i.i31
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp402:
.Lcfi327:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r12
.Ltmp403:
# BB#28:                                # %.noexc
	movq	%r12, 32(%rsp)
	movl	$0, (%r12)
	movl	%ebx, 44(%rsp)
	jmp	.LBB15_29
.LBB15_26:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
.LBB15_29:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i32
	movq	(%r15), %rax
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB15_30:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB15_30
# BB#31:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i35
	movl	%r13d, 40(%rsp)
	movl	24(%rsp), %eax
	movl	%ebx, %ecx
	subl	%r13d, %ecx
	cmpl	%eax, %ecx
	movq	%r14, %r15
	jle	.LBB15_33
# BB#32:
	movq	152(%rsp), %rbx
	jmp	.LBB15_56
.LBB15_33:
	decl	%ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	movl	8(%rsp), %r14d          # 4-byte Reload
	jl	.LBB15_35
# BB#34:                                # %select.true.sink158
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB15_35:                              # %select.end157
	leal	(%rdx,%rcx), %esi
	movl	%eax, %edi
	subl	%ecx, %edi
	cmpl	%eax, %esi
	cmovgel	%edx, %edi
	leal	1(%rbx,%rdi), %eax
	cmpl	%ebx, %eax
	jne	.LBB15_37
# BB#36:
	movq	152(%rsp), %rbx
	jmp	.LBB15_57
.LBB15_37:
	movl	%eax, %ebp
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp405:
.Lcfi328:
	.cfi_escape 0x2e, 0x00
	callq	_Znam
	movq	%rax, %r14
.Ltmp406:
# BB#38:                                # %.noexc56
	testl	%ebx, %ebx
	movq	152(%rsp), %rbx
	jle	.LBB15_55
# BB#39:                                # %.preheader.i.i47
	testl	%r13d, %r13d
	jle	.LBB15_53
# BB#40:                                # %.lr.ph.i.i48
	cmpl	$7, %r13d
	jbe	.LBB15_41
# BB#44:                                # %min.iters.checked7
	movq	%r15, %rax
	andq	$-8, %rax
	je	.LBB15_41
# BB#45:                                # %vector.body3.preheader
	leaq	-8(%rax), %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB15_46
# BB#47:                                # %vector.body3.prol.preheader
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_48:                              # %vector.body3.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rsi,4), %xmm0
	movups	16(%r12,%rsi,4), %xmm1
	movups	%xmm0, (%r14,%rsi,4)
	movups	%xmm1, 16(%r14,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB15_48
	jmp	.LBB15_49
.LBB15_41:
	xorl	%eax, %eax
.LBB15_42:                              # %scalar.ph5.preheader
	movq	%r15, %rcx
	subq	%rax, %rcx
	leaq	(%r14,%rax,4), %rdx
	leaq	(%r12,%rax,4), %rax
	.p2align	4, 0x90
.LBB15_43:                              # %scalar.ph5
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$4, %rdx
	addq	$4, %rax
	decq	%rcx
	jne	.LBB15_43
	jmp	.LBB15_54
.LBB15_53:                              # %._crit_edge.i.i49
	testq	%r12, %r12
	jne	.LBB15_54
	jmp	.LBB15_55
.LBB15_46:
	xorl	%esi, %esi
.LBB15_49:                              # %vector.body3.prol.loopexit
	cmpq	$24, %rcx
	jb	.LBB15_52
# BB#50:                                # %vector.body3.preheader.new
	movq	%rax, %rcx
	subq	%rsi, %rcx
	leaq	112(%r14,%rsi,4), %rdx
	leaq	112(%r12,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB15_51:                              # %vector.body3
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB15_51
.LBB15_52:                              # %middle.block4
	cmpq	%rax, %r15
	jne	.LBB15_42
.LBB15_54:                              # %._crit_edge.thread.i.i54
.Lcfi329:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB15_55:                              # %._crit_edge16.i.i55
	movq	%r14, 32(%rsp)
	movl	$0, (%r14,%r15,4)
	movl	%ebp, 44(%rsp)
	movq	%r14, %r12
	movq	136(%rsp), %rbp
.LBB15_56:                              # %.noexc.i
	movl	8(%rsp), %r14d          # 4-byte Reload
.LBB15_57:                              # %.noexc.i
	leaq	(%r12,%r15,4), %rcx
	movq	16(%rsp), %rsi
	.p2align	4, 0x90
.LBB15_58:                              # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB15_58
# BB#59:
	addl	24(%rsp), %r13d
	movl	%r13d, 40(%rsp)
.Ltmp408:
.Lcfi330:
	.cfi_escape 0x2e, 0x20
	movzbl	144(%rsp), %eax
	leaq	32(%rsp), %rcx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%esi, %edx
	movq	128(%rsp), %r8
	movq	%rbp, %r9
	pushq	168(%rsp)
.Lcfi331:
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
.Lcfi332:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi333:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi334:
	.cfi_adjust_cfa_offset 8
	callq	_ZL17EnumerateDirItemsRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwERK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE
	addq	$32, %rsp
.Lcfi335:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %ebx
.Ltmp409:
# BB#60:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_62
# BB#61:
.Lcfi336:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB15_62:                              # %_ZN11CStringBaseIwED2Ev.exit
	cmpl	108(%rbp), %r14d
	jne	.LBB15_66
# BB#63:
	leaq	32(%rbp), %rdi
.Ltmp411:
.Lcfi337:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp412:
# BB#64:                                # %.noexc37
	leaq	64(%rbp), %rdi
.Ltmp413:
.Lcfi338:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp414:
# BB#65:                                # %.noexc38
.Ltmp415:
.Lcfi339:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp416:
.LBB15_66:                              # %_ZN9CDirItems16DeleteLastPrefixEv.exit
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_68
# BB#67:
.Lcfi340:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB15_68:                              # %_ZN11CStringBaseIwED2Ev.exit41
	movl	%ebx, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_69:
.Ltmp407:
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB15_77
# BB#70:
.Lcfi341:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZdaPv
	jmp	.LBB15_77
.LBB15_81:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp398:
	movq	%rax, %rbx
.Lcfi342:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	jmp	.LBB15_79
.LBB15_71:
.Ltmp404:
	jmp	.LBB15_76
.LBB15_72:
.Ltmp410:
	movq	%rax, %rbx
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_77
# BB#73:
.Lcfi343:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
	jmp	.LBB15_77
.LBB15_75:
.Ltmp401:
	jmp	.LBB15_76
.LBB15_74:
.Ltmp417:
.LBB15_76:
	movq	%rax, %rbx
.LBB15_77:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB15_80
# BB#78:
.Lcfi344:
	.cfi_escape 0x2e, 0x00
.LBB15_79:                              # %unwind_resume
	callq	_ZdaPv
.LBB15_80:                              # %unwind_resume
.Lcfi345:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZL22EnumerateDirItems_SpecRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwES6_RK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE, .Lfunc_end15-_ZL22EnumerateDirItems_SpecRKN9NWildcard11CCensorNodeEiiRK11CStringBaseIwES6_RK13CObjectVectorIS4_ER9CDirItemsbP20IEnumDirItemCallbackRS8_R13CRecordVectorIjE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp396-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp396
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp396-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp397-.Ltmp396       #   Call between .Ltmp396 and .Ltmp397
	.long	.Ltmp398-.Lfunc_begin10 #     jumps to .Ltmp398
	.byte	0                       #   On action: cleanup
	.long	.Ltmp399-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp400-.Ltmp399       #   Call between .Ltmp399 and .Ltmp400
	.long	.Ltmp401-.Lfunc_begin10 #     jumps to .Ltmp401
	.byte	0                       #   On action: cleanup
	.long	.Ltmp402-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp403-.Ltmp402       #   Call between .Ltmp402 and .Ltmp403
	.long	.Ltmp404-.Lfunc_begin10 #     jumps to .Ltmp404
	.byte	0                       #   On action: cleanup
	.long	.Ltmp405-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp406-.Ltmp405       #   Call between .Ltmp405 and .Ltmp406
	.long	.Ltmp407-.Lfunc_begin10 #     jumps to .Ltmp407
	.byte	0                       #   On action: cleanup
	.long	.Ltmp408-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp409-.Ltmp408       #   Call between .Ltmp408 and .Ltmp409
	.long	.Ltmp410-.Lfunc_begin10 #     jumps to .Ltmp410
	.byte	0                       #   On action: cleanup
	.long	.Ltmp411-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp416-.Ltmp411       #   Call between .Ltmp411 and .Ltmp416
	.long	.Ltmp417-.Lfunc_begin10 #     jumps to .Ltmp417
	.byte	0                       #   On action: cleanup
	.long	.Ltmp416-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Lfunc_end15-.Ltmp416   #   Call between .Ltmp416 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.zero	16
	.section	.text._ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_,@function
_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_: # @_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%rbp
.Lcfi346:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi347:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi348:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi349:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi350:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi351:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi352:
	.cfi_def_cfa_offset 80
.Lcfi353:
	.cfi_offset %rbx, -56
.Lcfi354:
	.cfi_offset %r12, -48
.Lcfi355:
	.cfi_offset %r13, -40
.Lcfi356:
	.cfi_offset %r14, -32
.Lcfi357:
	.cfi_offset %r15, -24
.Lcfi358:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	12(%rbx), %ebx
	movl	12(%r15), %esi
	addl	%ebx, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	testl	%ebx, %ebx
	jle	.LBB16_9
# BB#1:                                 # %.lr.ph.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_7 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rbp,8), %r13
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movslq	8(%r13), %r14
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	je	.LBB16_3
# BB#4:                                 # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp418:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rcx
.Ltmp419:
# BB#5:                                 # %.noexc.i
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	%rcx, (%r12)
	movl	$0, (%rcx)
	movl	%ebx, 12(%r12)
	jmp	.LBB16_6
	.p2align	4, 0x90
.LBB16_3:                               #   in Loop: Header=BB16_2 Depth=1
	xorl	%ecx, %ecx
.LBB16_6:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	(%r13), %rsi
	.p2align	4, 0x90
.LBB16_7:                               #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB16_7
# BB#8:                                 # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit
                                        #   in Loop: Header=BB16_2 Depth=1
	movl	%r14d, 8(%r12)
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	jne	.LBB16_2
.LBB16_9:                               # %_ZN13CObjectVectorI11CStringBaseIwEEpLERKS2_.exit
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_10:
.Ltmp420:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_, .Lfunc_end16-_ZN13CObjectVectorI11CStringBaseIwEEaSERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp418-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp418
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp418-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp419-.Ltmp418       #   Call between .Ltmp418 and .Ltmp419
	.long	.Ltmp420-.Lfunc_begin11 #     jumps to .Ltmp420
	.byte	0                       #   On action: cleanup
	.long	.Ltmp419-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Lfunc_end16-.Ltmp419   #   Call between .Ltmp419 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIbED0Ev,"axG",@progbits,_ZN13CRecordVectorIbED0Ev,comdat
	.weak	_ZN13CRecordVectorIbED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbED0Ev,@function
_ZN13CRecordVectorIbED0Ev:              # @_ZN13CRecordVectorIbED0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi359:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi360:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi361:
	.cfi_def_cfa_offset 32
.Lcfi362:
	.cfi_offset %rbx, -24
.Lcfi363:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp421:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp422:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB17_2:
.Ltmp423:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end17:
	.size	_ZN13CRecordVectorIbED0Ev, .Lfunc_end17-_ZN13CRecordVectorIbED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp421-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp422-.Ltmp421       #   Call between .Ltmp421 and .Ltmp422
	.long	.Ltmp423-.Lfunc_begin12 #     jumps to .Ltmp423
	.byte	0                       #   On action: cleanup
	.long	.Ltmp422-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Lfunc_end17-.Ltmp422   #   Call between .Ltmp422 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi364:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi365:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi366:
	.cfi_def_cfa_offset 32
.Lcfi367:
	.cfi_offset %rbx, -24
.Lcfi368:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp424:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp425:
# BB#1:
.Ltmp430:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp431:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_5:
.Ltmp432:
	movq	%rax, %r14
	jmp	.LBB18_6
.LBB18_3:
.Ltmp426:
	movq	%rax, %r14
.Ltmp427:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp428:
.LBB18_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_4:
.Ltmp429:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end18-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp424-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp425-.Ltmp424       #   Call between .Ltmp424 and .Ltmp425
	.long	.Ltmp426-.Lfunc_begin13 #     jumps to .Ltmp426
	.byte	0                       #   On action: cleanup
	.long	.Ltmp430-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp431-.Ltmp430       #   Call between .Ltmp430 and .Ltmp431
	.long	.Ltmp432-.Lfunc_begin13 #     jumps to .Ltmp432
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp429-.Lfunc_begin13 #     jumps to .Ltmp429
	.byte	1                       #   On action: 1
	.long	.Ltmp428-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Lfunc_end18-.Ltmp428   #   Call between .Ltmp428 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi369:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi370:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi371:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi372:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi373:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi374:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi375:
	.cfi_def_cfa_offset 64
.Lcfi376:
	.cfi_offset %rbx, -56
.Lcfi377:
	.cfi_offset %r12, -48
.Lcfi378:
	.cfi_offset %r13, -40
.Lcfi379:
	.cfi_offset %r14, -32
.Lcfi380:
	.cfi_offset %r15, -24
.Lcfi381:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB19_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB19_6
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	callq	_ZdaPv
.LBB19_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB19_6:                               #   in Loop: Header=BB19_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB19_2
.LBB19_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end19:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end19-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi382:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi383:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi384:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi385:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi386:
	.cfi_def_cfa_offset 48
.Lcfi387:
	.cfi_offset %rbx, -48
.Lcfi388:
	.cfi_offset %r12, -40
.Lcfi389:
	.cfi_offset %r14, -32
.Lcfi390:
	.cfi_offset %r15, -24
.Lcfi391:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB20_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB20_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB20_3:                               # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB20_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB20_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB20_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB20_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB20_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB20_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB20_17
.LBB20_7:
	xorl	%ecx, %ecx
.LBB20_8:                               # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB20_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB20_10:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB20_10
.LBB20_11:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB20_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB20_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB20_13
	jmp	.LBB20_26
.LBB20_25:                              # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB20_27
.LBB20_26:                              # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB20_27:                              # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB20_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_17:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB20_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB20_20
	jmp	.LBB20_21
.LBB20_18:
	xorl	%ebx, %ebx
.LBB20_21:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB20_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB20_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB20_23
.LBB20_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB20_8
	jmp	.LBB20_26
.Lfunc_end20:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end20-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%rbp
.Lcfi392:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi393:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi394:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi395:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi396:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi397:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi398:
	.cfi_def_cfa_offset 64
.Lcfi399:
	.cfi_offset %rbx, -56
.Lcfi400:
	.cfi_offset %r12, -48
.Lcfi401:
	.cfi_offset %r13, -40
.Lcfi402:
	.cfi_offset %r14, -32
.Lcfi403:
	.cfi_offset %r15, -24
.Lcfi404:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB21_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB21_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB21_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB21_5
.LBB21_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB21_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp433:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp434:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB21_35
# BB#12:
	movq	%rbx, %r13
.LBB21_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB21_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB21_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB21_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB21_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB21_15
.LBB21_14:
	xorl	%esi, %esi
.LBB21_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB21_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB21_16
.LBB21_29:
	movq	%r13, %rbx
.LBB21_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp435:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp436:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB21_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB21_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB21_8
.LBB21_3:
	xorl	%eax, %eax
.LBB21_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB21_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB21_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB21_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB21_30
.LBB21_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB21_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB21_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB21_24
	jmp	.LBB21_25
.LBB21_22:
	xorl	%ecx, %ecx
.LBB21_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB21_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB21_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB21_27
.LBB21_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB21_15
	jmp	.LBB21_29
.LBB21_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp437:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end21-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin14-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp433-.Lfunc_begin14 #   Call between .Lfunc_begin14 and .Ltmp433
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp433-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp436-.Ltmp433       #   Call between .Ltmp433 and .Ltmp436
	.long	.Ltmp437-.Lfunc_begin14 #     jumps to .Ltmp437
	.byte	0                       #   On action: cleanup
	.long	.Ltmp436-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Lfunc_end21-.Ltmp436   #   Call between .Ltmp436 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV13CRecordVectorIbE,@object # @_ZTV13CRecordVectorIbE
	.section	.rodata._ZTV13CRecordVectorIbE,"aG",@progbits,_ZTV13CRecordVectorIbE,comdat
	.weak	_ZTV13CRecordVectorIbE
	.p2align	3
_ZTV13CRecordVectorIbE:
	.quad	0
	.quad	_ZTI13CRecordVectorIbE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIbED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIbE, 40

	.type	_ZTS13CRecordVectorIbE,@object # @_ZTS13CRecordVectorIbE
	.section	.rodata._ZTS13CRecordVectorIbE,"aG",@progbits,_ZTS13CRecordVectorIbE,comdat
	.weak	_ZTS13CRecordVectorIbE
	.p2align	4
_ZTS13CRecordVectorIbE:
	.asciz	"13CRecordVectorIbE"
	.size	_ZTS13CRecordVectorIbE, 19

	.type	_ZTI13CRecordVectorIbE,@object # @_ZTI13CRecordVectorIbE
	.section	.rodata._ZTI13CRecordVectorIbE,"aG",@progbits,_ZTI13CRecordVectorIbE,comdat
	.weak	_ZTI13CRecordVectorIbE
	.p2align	4
_ZTI13CRecordVectorIbE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIbE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIbE, 24

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
