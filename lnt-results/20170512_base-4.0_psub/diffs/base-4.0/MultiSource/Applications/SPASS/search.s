	.text
	.file	"search.bc"
	.globl	prfs_Check
	.p2align	4, 0x90
	.type	prfs_Check,@function
prfs_Check:                             # @prfs_Check
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	56(%r14), %r15
	testq	%r15, %r15
	jne	.LBB0_15
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_15 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB0_2
.LBB0_15:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rbx
	movq	104(%r14), %rdx
	movq	112(%r14), %rsi
	movq	%rbx, %rdi
	callq	clause_IsClause
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB0_30
# BB#16:                                #   in Loop: Header=BB0_15 Depth=1
	testb	$1, 48(%rbx)
	jne	.LBB0_30
# BB#17:                                #   in Loop: Header=BB0_15 Depth=1
	movl	12(%rbx), %ecx
	cmpl	128(%r14), %ecx
	jbe	.LBB0_18
	jmp	.LBB0_30
.LBB0_2:                                # %._crit_edge114
	movq	40(%r14), %r15
	testq	%r15, %r15
	jne	.LBB0_19
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_22:                               #   in Loop: Header=BB0_19 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB0_4
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rbx
	movq	104(%r14), %rdx
	movq	112(%r14), %rsi
	movq	%rbx, %rdi
	callq	clause_IsClause
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB0_30
# BB#20:                                #   in Loop: Header=BB0_19 Depth=1
	testb	$1, 48(%rbx)
	je	.LBB0_30
# BB#21:                                #   in Loop: Header=BB0_19 Depth=1
	movl	12(%rbx), %ecx
	cmpl	128(%r14), %ecx
	jbe	.LBB0_22
	jmp	.LBB0_30
.LBB0_4:                                # %._crit_edge109
	movq	120(%r14), %r8
	testq	%r8, %r8
	je	.LBB0_26
# BB#5:                                 # %.lr.ph104.preheader
	movq	%r8, %rdx
.LBB0_6:                                # %.lr.ph104
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_11 Depth 2
                                        #     Child Loop BB0_24 Depth 2
	movq	8(%rdx), %rsi
	cmpl	$0, 4(%rsi)
	je	.LBB0_25
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	xorl	%eax, %eax
	cmpq	$0, 8(%rsi)
	jne	.LBB0_30
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	cmpq	$0, 16(%rsi)
	jne	.LBB0_30
# BB#9:                                 # %.preheader92
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_13
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	(%rsi), %ebx
	.p2align	4, 0x90
.LBB0_11:                               #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdi), %rcx
	cmpl	%ebx, 12(%rcx)
	je	.LBB0_30
# BB#12:                                #   in Loop: Header=BB0_11 Depth=2
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_11
.LBB0_13:                               # %.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_25
# BB#14:                                # %.lr.ph101
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	(%rsi), %esi
	.p2align	4, 0x90
.LBB0_24:                               #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdi), %rcx
	cmpl	%esi, 12(%rcx)
	je	.LBB0_30
# BB#23:                                #   in Loop: Header=BB0_24 Depth=2
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_24
	.p2align	4, 0x90
.LBB0_25:                               # %.loopexit
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB0_6
.LBB0_26:                               # %._crit_edge
	movl	128(%r14), %edx
	testl	%edx, %edx
	je	.LBB0_27
# BB#28:
	movq	8(%r8), %rcx
	xorl	%eax, %eax
	cmpl	(%rcx), %edx
	je	.LBB0_29
	jmp	.LBB0_30
.LBB0_27:
	xorl	%eax, %eax
	testq	%r8, %r8
	jne	.LBB0_30
.LBB0_29:
	xorl	%eax, %eax
	cmpl	132(%r14), %edx
	setge	%al
.LBB0_30:                               # %.loopexit91
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	prfs_Check, .Lfunc_end0-prfs_Check
	.cfi_endproc

	.globl	prfs_DeleteDocProof
	.p2align	4, 0x90
	.type	prfs_DeleteDocProof,@function
prfs_DeleteDocProof:                    # @prfs_DeleteDocProof
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	88(%rbx), %rsi
	movq	96(%rbx), %rdi
	movq	112(%rbx), %rdx
	movq	104(%rbx), %rcx
	callq	clause_DeleteSharedClauseList
	movq	88(%rbx), %rdi
	leaq	88(%rbx), %rbx
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	sharing_IndexDelete
.LBB1_2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	prfs_DeleteDocProof, .Lfunc_end1-prfs_DeleteDocProof
	.cfi_endproc

	.globl	prfs_Delete
	.p2align	4, 0x90
	.type	prfs_Delete,@function
prfs_Delete:                            # @prfs_Delete
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	prfs_InternalDelete
	movq	32(%rbx), %rdi
	callq	sharing_IndexDelete
	movq	48(%rbx), %rdi
	callq	sharing_IndexDelete
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	callq	sharing_IndexDelete
.LBB2_2:
	movq	112(%rbx), %rax
	movq	memory_ARRAY+3072(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+3072(%rip), %rcx
	movq	%rax, (%rcx)
	movq	104(%rbx), %rdi
	movl	memory_ALIGN(%rip), %ecx
	movl	$16000, %esi            # imm = 0x3E80
	movl	$16000, %eax            # imm = 0x3E80
	xorl	%edx, %edx
	divl	%ecx
	addl	$16000, %ecx            # imm = 0x3E80
	subl	%edx, %ecx
	testl	%edx, %edx
	cmovel	%esi, %ecx
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rdx
	subq	%rax, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %r9
	movl	$memory_BIGBLOCKS, %esi
	cmovneq	%r9, %rsi
	movq	%r8, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_4
# BB#3:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rdx)
.LBB2_4:
	addl	memory_MARKSIZE(%rip), %ecx
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_6
# BB#5:
	leaq	16(%rcx,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_6:                                # %symbol_DeletePrecedence.exit
	addq	$-16, %rdi
	callq	free
	movq	memory_ARRAY+1280(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+1280(%rip), %rax
	movq	%rbx, (%rax)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	prfs_Delete, .Lfunc_end2-prfs_Delete
	.cfi_endproc

	.p2align	4, 0x90
	.type	prfs_InternalDelete,@function
prfs_InternalDelete:                    # @prfs_InternalDelete
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	8(%r14), %rdi
	callq	clause_DeleteClauseList
	movq	(%r14), %rdi
	movl	$def_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_2
	.p2align	4, 0x90
.LBB3_1:                                # %.lr.ph.i48
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB3_1
.LBB3_2:                                # %list_Delete.exit49
	movq	64(%r14), %rdi
	callq	sort_TheoryDelete
	movq	80(%r14), %rdi
	callq	sort_TheoryDelete
	movq	72(%r14), %rdi
	callq	sort_TheoryDelete
	movq	32(%r14), %rsi
	movq	40(%r14), %rdi
	movq	112(%r14), %rdx
	movq	104(%r14), %rcx
	callq	clause_DeleteSharedClauseList
	movq	48(%r14), %rsi
	movq	56(%r14), %rdi
	movq	112(%r14), %rdx
	movq	104(%r14), %rcx
	callq	clause_DeleteSharedClauseList
	movq	88(%r14), %rsi
	movq	96(%r14), %rdi
	movq	112(%r14), %rdx
	movq	104(%r14), %rcx
	callq	clause_DeleteSharedClauseList
	movq	24(%r14), %rdi
	movl	$term_DeleteTermList, %esi
	callq	list_DeleteAssocListWithValues
	movq	$0, 24(%r14)
	movq	120(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB3_8
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r15
	movq	8(%r15), %rdi
	callq	clause_DeleteClauseList
	movq	16(%r15), %rdi
	callq	clause_DeleteClauseList
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	callq	clause_Delete
.LBB3_5:                                # %prfs_SplitDelete.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r15, (%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_3
# BB#6:                                 # %._crit_edge
	movq	120(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_8
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB3_7
.LBB3_8:                                # %list_Delete.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	prfs_InternalDelete, .Lfunc_end3-prfs_InternalDelete
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.long	4294967254              # 0xffffffd6
	.long	4294967254              # 0xffffffd6
	.long	4294967254              # 0xffffffd6
	.long	4294967254              # 0xffffffd6
	.text
	.globl	prfs_Clean
	.p2align	4, 0x90
	.type	prfs_Clean,@function
prfs_Clean:                             # @prfs_Clean
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	prfs_InternalDelete
	movq	$0, 40(%rbx)
	movq	$0, 96(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movups	%xmm0, 72(%rbx)
	movups	%xmm0, 56(%rbx)
	movups	%xmm0, 136(%rbx)
	movups	%xmm0, 120(%rbx)
	movq	$0, 152(%rbx)
	movq	104(%rbx), %rax
	movl	$36, %ecx
	movaps	.LCPI4_0(%rip), %xmm0   # xmm0 = [4294967254,4294967254,4294967254,4294967254]
	.p2align	4, 0x90
.LBB4_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -144(%rax,%rcx,4)
	movups	%xmm0, -128(%rax,%rcx,4)
	movups	%xmm0, -112(%rax,%rcx,4)
	movups	%xmm0, -96(%rax,%rcx,4)
	movups	%xmm0, -80(%rax,%rcx,4)
	movups	%xmm0, -64(%rax,%rcx,4)
	movups	%xmm0, -48(%rax,%rcx,4)
	movups	%xmm0, -32(%rax,%rcx,4)
	movups	%xmm0, -16(%rax,%rcx,4)
	movups	%xmm0, (%rax,%rcx,4)
	addq	$40, %rcx
	cmpq	$4036, %rcx             # imm = 0xFC4
	jne	.LBB4_1
# BB#2:                                 # %symbol_ClearPrecedence.exit
	popq	%rbx
	retq
.Lfunc_end4:
	.size	prfs_Clean, .Lfunc_end4-prfs_Clean
	.cfi_endproc

	.globl	prfs_SwapIndexes
	.p2align	4, 0x90
	.type	prfs_SwapIndexes,@function
prfs_SwapIndexes:                       # @prfs_SwapIndexes
	.cfi_startproc
# BB#0:
	movdqu	32(%rdi), %xmm1
	movdqu	48(%rdi), %xmm0
	movdqu	%xmm0, 32(%rdi)
	movdqu	%xmm1, 48(%rdi)
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %rax
	testq	%rax, %rax
	je	.LBB5_1
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	movl	48(%rcx), %edx
	testb	$1, %dl
	je	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	decl	%edx
	movl	%edx, 48(%rcx)
.LBB5_7:                                # %clause_RemoveFlag.exit
                                        #   in Loop: Header=BB5_5 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB5_5
# BB#8:                                 # %.preheader.loopexit
	movq	40(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB5_2
	jmp	.LBB5_4
.LBB5_1:
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %rax
	testq	%rax, %rax
	je	.LBB5_4
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	orl	$1, 48(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB5_2
.LBB5_4:                                # %._crit_edge
	retq
.Lfunc_end5:
	.size	prfs_SwapIndexes, .Lfunc_end5-prfs_SwapIndexes
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	4294967254              # 0xffffffd6
	.long	4294967254              # 0xffffffd6
	.long	4294967254              # 0xffffffd6
	.long	4294967254              # 0xffffffd6
	.text
	.globl	prfs_Create
	.p2align	4, 0x90
	.type	prfs_Create,@function
prfs_Create:                            # @prfs_Create
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	$160, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movq	$0, 16(%r14)
	callq	sharing_IndexCreate
	movq	%rax, 32(%r14)
	movq	$0, 40(%r14)
	callq	sharing_IndexCreate
	movq	%rax, 48(%r14)
	movq	$0, 24(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%r14)
	movups	%xmm0, 56(%r14)
	movl	$16000, %edi            # imm = 0x3E80
	callq	memory_Malloc
	movl	$36, %ecx
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [4294967254,4294967254,4294967254,4294967254]
	.p2align	4, 0x90
.LBB6_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -144(%rax,%rcx,4)
	movups	%xmm0, -128(%rax,%rcx,4)
	movups	%xmm0, -112(%rax,%rcx,4)
	movups	%xmm0, -96(%rax,%rcx,4)
	movups	%xmm0, -80(%rax,%rcx,4)
	movups	%xmm0, -64(%rax,%rcx,4)
	movups	%xmm0, -48(%rax,%rcx,4)
	movups	%xmm0, -32(%rax,%rcx,4)
	movups	%xmm0, -16(%rax,%rcx,4)
	movups	%xmm0, (%rax,%rcx,4)
	addq	$40, %rcx
	cmpq	$4036, %rcx             # imm = 0xFC4
	jne	.LBB6_1
# BB#2:                                 # %vector.body49
	movq	%rax, 104(%r14)
	movl	$384, %edi              # imm = 0x180
	callq	memory_Malloc
	movq	%rax, %r15
	movd	flag_CLEAN(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%r15)
	movdqu	%xmm0, 16(%r15)
	movdqu	%xmm0, 32(%r15)
	movdqu	%xmm0, 48(%r15)
	movdqu	%xmm0, 64(%r15)
	movdqu	%xmm0, 80(%r15)
	movdqu	%xmm0, 96(%r15)
	movdqu	%xmm0, 112(%r15)
	movdqu	%xmm0, 128(%r15)
	movdqu	%xmm0, 144(%r15)
	movdqu	%xmm0, 160(%r15)
	movdqu	%xmm0, 176(%r15)
	movdqu	%xmm0, 192(%r15)
	movdqu	%xmm0, 208(%r15)
	movdqu	%xmm0, 224(%r15)
	movdqu	%xmm0, 240(%r15)
	movdqu	%xmm0, 256(%r15)
	movdqu	%xmm0, 272(%r15)
	movdqu	%xmm0, 288(%r15)
	movdqu	%xmm0, 304(%r15)
	movdqu	%xmm0, 320(%r15)
	movdqu	%xmm0, 336(%r15)
	movdqu	%xmm0, 352(%r15)
	movdqu	%xmm0, 368(%r15)
	movq	%r15, 112(%r14)
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_3:                                # =>This Inner Loop Header: Depth=1
	callq	flag_DefaultStore
	movl	(%rax,%rbx,4), %ebp
	movl	%ebx, %edi
	callq	flag_Minimum
	cmpl	%ebp, %eax
	jge	.LBB6_4
# BB#6:                                 #   in Loop: Header=BB6_3 Depth=1
	movl	%ebx, %edi
	callq	flag_Maximum
	cmpl	%ebp, %eax
	jle	.LBB6_7
# BB#8:                                 # %flag_SetFlagValue.exit.i
                                        #   in Loop: Header=BB6_3 Depth=1
	movl	%ebp, (%r15,%rbx,4)
	incq	%rbx
	cmpq	$96, %rbx
	jb	.LBB6_3
# BB#9:                                 # %flag_InitStoreByDefaults.exit
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 88(%r14)
	movdqu	%xmm0, 136(%r14)
	movdqu	%xmm0, 120(%r14)
	movq	$0, 152(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_4:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebx, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.24, %edi
	jmp	.LBB6_5
.LBB6_7:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebx, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.25, %edi
.LBB6_5:
	xorl	%eax, %eax
	movl	%ebp, %esi
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end6:
	.size	prfs_Create, .Lfunc_end6-prfs_Create
	.cfi_endproc

	.globl	prfs_CopyIndices
	.p2align	4, 0x90
	.type	prfs_CopyIndices,@function
prfs_CopyIndices:                       # @prfs_CopyIndices
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 48
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r13, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	cmpq	$0, 88(%r14)
	je	.LBB7_3
# BB#1:
	cmpq	$0, 88(%r12)
	jne	.LBB7_3
# BB#2:
	callq	sharing_IndexCreate
	movq	%rax, 88(%r12)
.LBB7_3:
	movq	56(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB7_6
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	clause_Copy
	movq	%rax, %r15
	movq	112(%r12), %rdx
	movq	56(%r12), %rsi
	movq	104(%r12), %rcx
	movq	%r15, %rdi
	callq	clause_InsertWeighed
	movq	%rax, 56(%r12)
	movq	112(%r12), %rdx
	movq	48(%r12), %rsi
	movq	104(%r12), %rcx
	movq	%r15, %rdi
	callq	clause_InsertIntoSharing
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_4
.LBB7_6:                                # %._crit_edge53
	movq	40(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB7_9
	.p2align	4, 0x90
.LBB7_7:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	clause_Copy
	movq	%rax, %r15
	orb	$1, 48(%r15)
	movq	40(%r12), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, 40(%r12)
	movq	112(%r12), %rdx
	movq	32(%r12), %rsi
	movq	104(%r12), %rcx
	movq	%r15, %rdi
	callq	clause_InsertIntoSharing
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	prfs_InsertInSortTheories
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_7
.LBB7_9:                                # %._crit_edge48
	movq	96(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_11
	jmp	.LBB7_15
	.p2align	4, 0x90
.LBB7_13:                               #   in Loop: Header=BB7_11 Depth=1
	movq	96(%r12), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 96(%r12)
	movq	112(%r12), %rdx
	movq	88(%r12), %rsi
	movq	104(%r12), %rcx
	movq	%r14, %rdi
	callq	clause_InsertIntoSharing
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB7_15
.LBB7_11:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	clause_Copy
	movq	%rax, %r14
	cmpq	$0, 88(%r12)
	jne	.LBB7_13
# BB#12:                                #   in Loop: Header=BB7_11 Depth=1
	movq	%r14, %rdi
	callq	clause_Delete
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_11
.LBB7_15:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	prfs_CopyIndices, .Lfunc_end7-prfs_CopyIndices
	.cfi_endproc

	.globl	prfs_InsertUsableClause
	.p2align	4, 0x90
	.type	prfs_InsertUsableClause,@function
prfs_InsertUsableClause:                # @prfs_InsertUsableClause
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	112(%rbx), %rdx
	movq	56(%rbx), %rsi
	movq	104(%rbx), %rcx
	movq	%r14, %rdi
	callq	clause_InsertWeighed
	movq	%rax, 56(%rbx)
	movq	112(%rbx), %rdx
	movq	48(%rbx), %rsi
	movq	104(%rbx), %rcx
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	clause_InsertIntoSharing # TAILCALL
.Lfunc_end8:
	.size	prfs_InsertUsableClause, .Lfunc_end8-prfs_InsertUsableClause
	.cfi_endproc

	.globl	prfs_InsertWorkedOffClause
	.p2align	4, 0x90
	.type	prfs_InsertWorkedOffClause,@function
prfs_InsertWorkedOffClause:             # @prfs_InsertWorkedOffClause
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	orb	$1, 48(%r14)
	movq	40(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 40(%rbx)
	movq	112(%rbx), %rdx
	movq	32(%rbx), %rsi
	movq	104(%rbx), %rcx
	movq	%r14, %rdi
	callq	clause_InsertIntoSharing
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	prfs_InsertInSortTheories # TAILCALL
.Lfunc_end9:
	.size	prfs_InsertWorkedOffClause, .Lfunc_end9-prfs_InsertWorkedOffClause
	.cfi_endproc

	.globl	prfs_InsertDocProofClause
	.p2align	4, 0x90
	.type	prfs_InsertDocProofClause,@function
prfs_InsertDocProofClause:              # @prfs_InsertDocProofClause
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -32
.Lcfi52:
	.cfi_offset %r14, -24
.Lcfi53:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	$0, 88(%rbx)
	je	.LBB10_1
# BB#2:
	movq	96(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 96(%rbx)
	movq	112(%rbx), %rdx
	movq	88(%rbx), %rsi
	movq	104(%rbx), %rcx
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	clause_InsertIntoSharing # TAILCALL
.LBB10_1:
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	clause_Delete           # TAILCALL
.Lfunc_end10:
	.size	prfs_InsertDocProofClause, .Lfunc_end10-prfs_InsertDocProofClause
	.cfi_endproc

	.p2align	4, 0x90
	.type	prfs_InsertInSortTheories,@function
prfs_InsertInSortTheories:              # @prfs_InsertInSortTheories
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 64
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	cmpq	$0, 80(%r14)
	jne	.LBB11_2
# BB#1:
	cmpq	$0, 72(%r14)
	je	.LBB11_17
.LBB11_2:
	movq	%r12, %rdi
	callq	clause_IsDeclarationClause
	testl	%eax, %eax
	je	.LBB11_17
# BB#3:
	movl	72(%r12), %eax
	testl	%eax, %eax
	jle	.LBB11_17
# BB#4:                                 # %.lr.ph78
	movslq	68(%r12), %rcx
	movslq	64(%r12), %rbx
	addq	%rcx, %rbx
	addl	%ebx, %eax
	movl	symbol_TYPESTATBITS(%rip), %r15d
	movslq	%eax, %r13
	.p2align	4, 0x90
.LBB11_5:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_9 Depth 2
                                        #     Child Loop BB11_11 Depth 2
                                        #     Child Loop BB11_15 Depth 2
	movq	56(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	testb	$1, (%rax)
	je	.LBB11_16
# BB#6:                                 #   in Loop: Header=BB11_5 Depth=1
	movq	24(%rax), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
	movl	%r15d, %ecx
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	cmpl	$1, 16(%rax)
	jne	.LBB11_16
# BB#7:                                 #   in Loop: Header=BB11_5 Depth=1
	cmpq	$0, 80(%r14)
	je	.LBB11_13
# BB#8:                                 #   in Loop: Header=BB11_5 Depth=1
	movq	%r12, %rdi
	callq	clause_Copy
	movq	32(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB11_10
	.p2align	4, 0x90
.LBB11_9:                               # %.lr.ph.i72
                                        #   Parent Loop BB11_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB11_9
.LBB11_10:                              # %list_Delete.exit73
                                        #   in Loop: Header=BB11_5 Depth=1
	movq	$0, 32(%rax)
	movq	40(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB11_12
	.p2align	4, 0x90
.LBB11_11:                              # %.lr.ph.i
                                        #   Parent Loop BB11_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB11_11
.LBB11_12:                              # %list_Delete.exit
                                        #   in Loop: Header=BB11_5 Depth=1
	movq	$0, 40(%rax)
	movl	(%r12), %ecx
	movl	%ecx, (%rax)
	movq	80(%r14), %rdi
	movq	56(%rax), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	%r12, %rsi
	movq	%rax, %rdx
	callq	sort_TheoryInsertClause
.LBB11_13:                              #   in Loop: Header=BB11_5 Depth=1
	cmpq	$0, 72(%r14)
	je	.LBB11_16
# BB#14:                                #   in Loop: Header=BB11_5 Depth=1
	movq	104(%r14), %rdx
	movq	112(%r14), %rsi
	movq	%r12, %rdi
	callq	sort_ApproxMaxDeclClauses
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB11_16
	.p2align	4, 0x90
.LBB11_15:                              # %.lr.ph
                                        #   Parent Loop BB11_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdx
	movq	72(%r14), %rdi
	movslq	68(%rdx), %rax
	movslq	64(%rdx), %rcx
	addq	%rax, %rcx
	movq	56(%rdx), %rax
	movq	(%rax,%rcx,8), %rcx
	movq	%r12, %rsi
	callq	sort_TheoryInsertClause
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB11_15
	.p2align	4, 0x90
.LBB11_16:                              # %.loopexit
                                        #   in Loop: Header=BB11_5 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB11_5
.LBB11_17:                              # %.loopexit75
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	prfs_InsertInSortTheories, .Lfunc_end11-prfs_InsertInSortTheories
	.cfi_endproc

	.globl	prfs_MoveUsableWorkedOff
	.p2align	4, 0x90
	.type	prfs_MoveUsableWorkedOff,@function
prfs_MoveUsableWorkedOff:               # @prfs_MoveUsableWorkedOff
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 32
.Lcfi70:
	.cfi_offset %rbx, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	56(%rbx), %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 56(%rbx)
	orb	$1, 48(%r14)
	movq	32(%rbx), %rdx
	movq	48(%rbx), %rsi
	movq	112(%rbx), %rcx
	movq	104(%rbx), %r8
	movq	%r14, %rdi
	callq	clause_MoveSharedClause
	movq	40(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 40(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	prfs_InsertInSortTheories # TAILCALL
.Lfunc_end12:
	.size	prfs_MoveUsableWorkedOff, .Lfunc_end12-prfs_MoveUsableWorkedOff
	.cfi_endproc

	.globl	prfs_MoveWorkedOffDocProof
	.p2align	4, 0x90
	.type	prfs_MoveWorkedOffDocProof,@function
prfs_MoveWorkedOffDocProof:             # @prfs_MoveWorkedOffDocProof
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -32
.Lcfi77:
	.cfi_offset %r14, -24
.Lcfi78:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	callq	clause_IsDeclarationClause
	testl	%eax, %eax
	je	.LBB13_5
# BB#1:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_3
# BB#2:
	movq	%r14, %rsi
	callq	sort_TheoryDeleteClause
.LBB13_3:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_5
# BB#4:
	movq	%r14, %rsi
	callq	sort_TheoryDeleteClause
.LBB13_5:                               # %prfs_DeleteFromSortTheories.exit
	movq	40(%rbx), %rdi
	movq	%r14, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 40(%rbx)
	movl	48(%r14), %eax
	testb	$1, %al
	je	.LBB13_7
# BB#6:
	decl	%eax
	movl	%eax, 48(%r14)
.LBB13_7:                               # %clause_RemoveFlag.exit
	movq	32(%rbx), %rsi
	movq	88(%rbx), %rdx
	movq	112(%rbx), %rcx
	movq	104(%rbx), %r8
	testq	%rdx, %rdx
	je	.LBB13_9
# BB#8:
	movq	%r14, %rdi
	callq	clause_MoveSharedClause
	movq	96(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 96(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB13_9:
	movq	%r14, %rdi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	clause_DeleteFromSharing # TAILCALL
.Lfunc_end13:
	.size	prfs_MoveWorkedOffDocProof, .Lfunc_end13-prfs_MoveWorkedOffDocProof
	.cfi_endproc

	.globl	prfs_MoveUsableDocProof
	.p2align	4, 0x90
	.type	prfs_MoveUsableDocProof,@function
prfs_MoveUsableDocProof:                # @prfs_MoveUsableDocProof
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 32
.Lcfi82:
	.cfi_offset %rbx, -32
.Lcfi83:
	.cfi_offset %r14, -24
.Lcfi84:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	56(%rbx), %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 56(%rbx)
	movq	48(%rbx), %rsi
	movq	88(%rbx), %rdx
	movq	112(%rbx), %rcx
	movq	104(%rbx), %r8
	testq	%rdx, %rdx
	je	.LBB14_2
# BB#1:
	movq	%r14, %rdi
	callq	clause_MoveSharedClause
	movq	96(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 96(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB14_2:
	movq	%r14, %rdi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	clause_DeleteFromSharing # TAILCALL
.Lfunc_end14:
	.size	prfs_MoveUsableDocProof, .Lfunc_end14-prfs_MoveUsableDocProof
	.cfi_endproc

	.globl	prfs_MoveInvalidClausesDocProof
	.p2align	4, 0x90
	.type	prfs_MoveInvalidClausesDocProof,@function
prfs_MoveInvalidClausesDocProof:        # @prfs_MoveInvalidClausesDocProof
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 48
.Lcfi90:
	.cfi_offset %rbx, -40
.Lcfi91:
	.cfi_offset %r12, -32
.Lcfi92:
	.cfi_offset %r14, -24
.Lcfi93:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	40(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB15_7
# BB#1:                                 # %.lr.ph57
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r12
	movl	12(%r12), %eax
	cmpl	128(%r14), %eax
	jbe	.LBB15_4
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB15_4:                               #   in Loop: Header=BB15_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_2
# BB#5:                                 # %.preheader43
	testq	%r15, %r15
	je	.LBB15_7
	.p2align	4, 0x90
.LBB15_6:                               # %.lr.ph51
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rsi
	movq	%r14, %rdi
	callq	prfs_MoveWorkedOffDocProof
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB15_6
.LBB15_7:                               # %._crit_edge52
	movq	56(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB15_17
# BB#8:                                 # %.lr.ph49
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB15_9:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r15
	movl	12(%r15), %eax
	cmpl	128(%r14), %eax
	jbe	.LBB15_11
# BB#10:                                #   in Loop: Header=BB15_9 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB15_11:                              #   in Loop: Header=BB15_9 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_9
# BB#12:                                # %.preheader
	testq	%r12, %r12
	je	.LBB15_17
	.p2align	4, 0x90
.LBB15_13:                              # =>This Inner Loop Header: Depth=1
	movq	8(%r12), %r15
	movq	56(%r14), %rdi
	movq	%r15, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 56(%r14)
	movq	48(%r14), %rsi
	movq	88(%r14), %rdx
	movq	112(%r14), %rcx
	movq	104(%r14), %r8
	testq	%rdx, %rdx
	je	.LBB15_14
# BB#15:                                #   in Loop: Header=BB15_13 Depth=1
	movq	%r15, %rdi
	callq	clause_MoveSharedClause
	movq	96(%r14), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 96(%r14)
	jmp	.LBB15_16
	.p2align	4, 0x90
.LBB15_14:                              #   in Loop: Header=BB15_13 Depth=1
	movq	%r15, %rdi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	callq	clause_DeleteFromSharing
.LBB15_16:                              # %prfs_MoveUsableDocProof.exit
                                        #   in Loop: Header=BB15_13 Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB15_13
.LBB15_17:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	prfs_MoveInvalidClausesDocProof, .Lfunc_end15-prfs_MoveInvalidClausesDocProof
	.cfi_endproc

	.globl	prfs_ExtractWorkedOff
	.p2align	4, 0x90
	.type	prfs_ExtractWorkedOff,@function
prfs_ExtractWorkedOff:                  # @prfs_ExtractWorkedOff
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 32
.Lcfi97:
	.cfi_offset %rbx, -24
.Lcfi98:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	clause_IsDeclarationClause
	testl	%eax, %eax
	je	.LBB16_5
# BB#1:
	movq	80(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB16_3
# BB#2:
	movq	%rbx, %rsi
	callq	sort_TheoryDeleteClause
.LBB16_3:
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB16_5
# BB#4:
	movq	%rbx, %rsi
	callq	sort_TheoryDeleteClause
.LBB16_5:                               # %prfs_DeleteFromSortTheories.exit
	movl	48(%rbx), %eax
	testb	$1, %al
	je	.LBB16_7
# BB#6:
	decl	%eax
	movl	%eax, 48(%rbx)
.LBB16_7:                               # %clause_RemoveFlag.exit
	movq	40(%r14), %rdi
	movq	%rbx, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 40(%r14)
	movq	32(%r14), %rsi
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	clause_MakeUnshared     # TAILCALL
.Lfunc_end16:
	.size	prfs_ExtractWorkedOff, .Lfunc_end16-prfs_ExtractWorkedOff
	.cfi_endproc

	.globl	prfs_ExtractUsable
	.p2align	4, 0x90
	.type	prfs_ExtractUsable,@function
prfs_ExtractUsable:                     # @prfs_ExtractUsable
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	56(%rbx), %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 56(%rbx)
	movq	48(%rbx), %rsi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	clause_MakeUnshared     # TAILCALL
.Lfunc_end17:
	.size	prfs_ExtractUsable, .Lfunc_end17-prfs_ExtractUsable
	.cfi_endproc

	.globl	prfs_ExtractDocProof
	.p2align	4, 0x90
	.type	prfs_ExtractDocProof,@function
prfs_ExtractDocProof:                   # @prfs_ExtractDocProof
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	96(%rbx), %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 96(%rbx)
	movq	88(%rbx), %rsi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	clause_MakeUnshared     # TAILCALL
.Lfunc_end18:
	.size	prfs_ExtractDocProof, .Lfunc_end18-prfs_ExtractDocProof
	.cfi_endproc

	.globl	prfs_DeleteWorkedOff
	.p2align	4, 0x90
	.type	prfs_DeleteWorkedOff,@function
prfs_DeleteWorkedOff:                   # @prfs_DeleteWorkedOff
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -24
.Lcfi113:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	callq	clause_IsDeclarationClause
	testl	%eax, %eax
	je	.LBB19_5
# BB#1:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_3
# BB#2:
	movq	%r14, %rsi
	callq	sort_TheoryDeleteClause
.LBB19_3:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_5
# BB#4:
	movq	%r14, %rsi
	callq	sort_TheoryDeleteClause
.LBB19_5:                               # %prfs_DeleteFromSortTheories.exit
	movq	40(%rbx), %rdi
	movq	%r14, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, 40(%rbx)
	movq	112(%rbx), %rdx
	movq	32(%rbx), %rsi
	movq	104(%rbx), %rcx
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	clause_DeleteFromSharing # TAILCALL
.Lfunc_end19:
	.size	prfs_DeleteWorkedOff, .Lfunc_end19-prfs_DeleteWorkedOff
	.cfi_endproc

	.globl	prfs_DeleteUsable
	.p2align	4, 0x90
	.type	prfs_DeleteUsable,@function
prfs_DeleteUsable:                      # @prfs_DeleteUsable
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 32
.Lcfi117:
	.cfi_offset %rbx, -24
.Lcfi118:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	56(%rbx), %rdi
	callq	list_PointerDeleteElement
	movq	%rax, 56(%rbx)
	movq	112(%rbx), %rdx
	movq	48(%rbx), %rsi
	movq	104(%rbx), %rcx
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	clause_DeleteFromSharing # TAILCALL
.Lfunc_end20:
	.size	prfs_DeleteUsable, .Lfunc_end20-prfs_DeleteUsable
	.cfi_endproc

	.globl	prfs_PrintSplit
	.p2align	4, 0x90
	.type	prfs_PrintSplit,@function
prfs_PrintSplit:                        # @prfs_PrintSplit
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 32
.Lcfi122:
	.cfi_offset %rbx, -24
.Lcfi123:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	(%r14), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	printf
	movq	stdout(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB21_2
# BB#1:
	callq	clause_Print
	jmp	.LBB21_3
.LBB21_2:
	movq	stdout(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
.LBB21_3:
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	cmpl	$0, 4(%r14)
	je	.LBB21_4
# BB#5:
	movl	$.L.str.5, %edi
	jmp	.LBB21_6
.LBB21_4:
	movl	$.L.str.4, %edi
.LBB21_6:
	callq	puts
	movq	stdout(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	movq	stdout(%rip), %rax
	je	.LBB21_8
	.p2align	4, 0x90
.LBB21_7:                               # %.lr.ph28
                                        # =>This Inner Loop Header: Depth=1
	movl	$10, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	movq	stdout(%rip), %rax
	testq	%rbx, %rbx
	jne	.LBB21_7
.LBB21_8:                               # %._crit_edge29
	movl	$.L.str.7, %edi
	movl	$18, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB21_11
	.p2align	4, 0x90
.LBB21_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB21_9
.LBB21_11:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end21:
	.size	prfs_PrintSplit, .Lfunc_end21-prfs_PrintSplit
	.cfi_endproc

	.globl	prfs_PrintSplitStack
	.p2align	4, 0x90
	.type	prfs_PrintSplitStack,@function
prfs_PrintSplitStack:                   # @prfs_PrintSplitStack
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 16
.Lcfi125:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	stdout(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	120(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB22_3
	.p2align	4, 0x90
.LBB22_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	prfs_PrintSplit
	movq	stdout(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_1
.LBB22_3:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end22:
	.size	prfs_PrintSplitStack, .Lfunc_end22-prfs_PrintSplitStack
	.cfi_endproc

	.globl	prfs_Print
	.p2align	4, 0x90
	.type	prfs_Print,@function
prfs_Print:                             # @prfs_Print
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi128:
	.cfi_def_cfa_offset 32
.Lcfi129:
	.cfi_offset %rbx, -24
.Lcfi130:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	128(%r14), %esi
	movl	132(%r14), %edx
	movl	136(%r14), %ecx
	movl	148(%r14), %r8d
	movl	152(%r14), %r9d
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movl	156(%r14), %esi
	testl	%esi, %esi
	je	.LBB23_2
# BB#1:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB23_3
.LBB23_2:
	movq	stdout(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
.LBB23_3:
	movq	stdout(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$16, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	movq	stdout(%rip), %rcx
	je	.LBB23_5
	.p2align	4, 0x90
.LBB23_4:                               # %.lr.ph89
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.14, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rbx), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	movq	stdout(%rip), %rcx
	testq	%rbx, %rbx
	jne	.LBB23_4
.LBB23_5:                               # %._crit_edge90
	movl	$.L.str.15, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	movq	stdout(%rip), %rax
	je	.LBB23_7
	.p2align	4, 0x90
.LBB23_6:                               # %.lr.ph83
                                        # =>This Inner Loop Header: Depth=1
	movl	$10, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	term_Print
	movq	(%rbx), %rbx
	movq	stdout(%rip), %rax
	testq	%rbx, %rbx
	jne	.LBB23_6
.LBB23_7:                               # %._crit_edge84
	movl	$.L.str.16, %edi
	movl	$21, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	40(%r14), %rbx
	testq	%rbx, %rbx
	movq	stdout(%rip), %rax
	je	.LBB23_9
	.p2align	4, 0x90
.LBB23_8:                               # %.lr.ph77
                                        # =>This Inner Loop Header: Depth=1
	movl	$10, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	movq	stdout(%rip), %rax
	testq	%rbx, %rbx
	jne	.LBB23_8
.LBB23_9:                               # %._crit_edge78
	movl	$.L.str.17, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	56(%r14), %rbx
	testq	%rbx, %rbx
	movq	stdout(%rip), %rax
	je	.LBB23_11
	.p2align	4, 0x90
.LBB23_10:                              # %.lr.ph71
                                        # =>This Inner Loop Header: Depth=1
	movl	$10, %edi
	movq	%rax, %rsi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movq	8(%rbx), %rdi
	callq	clause_Print
	movq	(%rbx), %rbx
	movq	stdout(%rip), %rax
	testq	%rbx, %rbx
	jne	.LBB23_10
.LBB23_11:                              # %._crit_edge72
	movl	$.L.str.18, %edi
	movl	$20, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	24(%r14), %rbx
	testq	%rbx, %rbx
	movq	stdout(%rip), %rcx
	je	.LBB23_13
	.p2align	4, 0x90
.LBB23_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.19, %edi
	movl	$3, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rbx), %rax
	movl	8(%rax), %edi
	callq	symbol_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.20, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	callq	term_TermListPrintPrefix
	movq	(%rbx), %rbx
	movq	stdout(%rip), %rcx
	testq	%rbx, %rbx
	jne	.LBB23_12
.LBB23_13:                              # %._crit_edge
	movl	$.L.str.8, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	120(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB23_16
	.p2align	4, 0x90
.LBB23_14:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	prfs_PrintSplit
	movq	stdout(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_14
.LBB23_16:                              # %prfs_PrintSplitStack.exit
	movq	stdout(%rip), %rcx
	movl	$.L.str.21, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
	movq	64(%r14), %rdi
	callq	sort_TheoryPrint
	movq	stdout(%rip), %rcx
	movl	$.L.str.22, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movq	80(%r14), %rdi
	callq	sort_TheoryPrint
	movq	stdout(%rip), %rcx
	movl	$.L.str.23, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movq	72(%r14), %rdi
	callq	sort_TheoryPrint
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_IO_putc                # TAILCALL
.Lfunc_end23:
	.size	prfs_Print, .Lfunc_end23-prfs_Print
	.cfi_endproc

	.globl	prfs_DoSplitting
	.p2align	4, 0x90
	.type	prfs_DoSplitting,@function
prfs_DoSplitting:                       # @prfs_DoSplitting
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi134:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi135:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi137:
	.cfi_def_cfa_offset 144
.Lcfi138:
	.cfi_offset %rbx, -56
.Lcfi139:
	.cfi_offset %r12, -48
.Lcfi140:
	.cfi_offset %r13, -40
.Lcfi141:
	.cfi_offset %r14, -32
.Lcfi142:
	.cfi_offset %r15, -24
.Lcfi143:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %rbx
	decl	136(%rbx)
	incl	128(%rbx)
	movl	$32, %edi
	callq	memory_Malloc
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movl	128(%rbx), %ecx
	movl	%ecx, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rax)
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	list_Length
	movl	64(%r15), %ebx
	subl	%eax, %ebx
	addl	68(%r15), %ebx
	addl	72(%r15), %ebx
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%eax, %edi
	callq	clause_CreateBody
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ebx, %edi
	callq	clause_CreateBody
	movq	%rax, %r13
	decl	clause_CLAUSECOUNTER(%rip)
	movl	$0, (%r13)
	movl	64(%r15), %eax
	movl	68(%r15), %ecx
	movq	%rax, %rdi
	leal	-1(%rax,%rcx), %edx
	movl	72(%r15), %eax
	movl	%edx, %esi
	addl	%eax, %esi
	movl	$0, %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	js	.LBB24_18
# BB#1:                                 # %.lr.ph283
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB24_8
# BB#2:                                 # %.lr.ph283.split.preheader
	leal	-1(%rdi), %esi
	movslq	%edx, %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movslq	%esi, %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	addl	%ecx, %edi
	addl	%eax, %edi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$0, (%rsp)              # 4-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB24_3:                               # %.lr.ph283.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_5 Depth 2
	movq	56(%r15), %rax
	movq	(%rax,%r12,8), %rbx
	movq	%rbx, %rdi
	callq	clause_LiteralCopy
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB24_7
# BB#4:                                 # %.lr.ph.i240.preheader
                                        #   in Loop: Header=BB24_3 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB24_5:                               # %.lr.ph.i240
                                        #   Parent Loop BB24_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, 8(%rcx)
	je	.LBB24_11
# BB#6:                                 #   in Loop: Header=BB24_5 Depth=2
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB24_5
.LBB24_7:                               # %list_PointerMember.exit.thread
                                        #   in Loop: Header=BB24_3 Depth=1
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	movq	56(%r13), %rdx
	movslq	%ecx, %rcx
	movq	%rax, (%rdx,%rcx,8)
	movq	%r13, 16(%rax)
	movslq	(%r15), %rbx
	movq	32(%r13), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 32(%r13)
	movq	40(%r13), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 40(%r13)
	jmp	.LBB24_17
	.p2align	4, 0x90
.LBB24_11:                              # %list_PointerMember.exit
                                        #   in Loop: Header=BB24_3 Depth=1
	decl	32(%rsp)                # 4-byte Folded Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	56(%rsi), %rcx
	movslq	%r14d, %rdx
	incl	%r14d
	movl	%r14d, 20(%rsp)         # 4-byte Spill
	movq	%rax, (%rcx,%rdx,8)
	movq	%rsi, 16(%rax)
	movslq	(%r15), %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	32(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rax, 32(%rbp)
	movq	40(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 40(%rbp)
	cmpq	80(%rsp), %r12          # 8-byte Folded Reload
	jle	.LBB24_12
# BB#13:                                #   in Loop: Header=BB24_3 Depth=1
	cmpq	72(%rsp), %r12          # 8-byte Folded Reload
	jle	.LBB24_14
# BB#15:                                #   in Loop: Header=BB24_3 Depth=1
	incl	4(%rsp)                 # 4-byte Folded Spill
	jmp	.LBB24_16
.LBB24_12:                              #   in Loop: Header=BB24_3 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB24_16
.LBB24_14:                              #   in Loop: Header=BB24_3 Depth=1
	incl	(%rsp)                  # 4-byte Folded Spill
.LBB24_16:                              #   in Loop: Header=BB24_3 Depth=1
	movl	20(%rsp), %r14d         # 4-byte Reload
.LBB24_17:                              #   in Loop: Header=BB24_3 Depth=1
	incq	%r12
	cmpq	64(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB24_3
	jmp	.LBB24_18
.LBB24_8:                               # %.lr.ph283.split.us.preheader
	movq	%rdi, %r12
	addl	%ecx, %r12d
	addl	%eax, %r12d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB24_9:                               # %.lr.ph283.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rdi
	callq	clause_LiteralCopy
	movq	56(%r13), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	%r13, 16(%rax)
	movslq	(%r15), %rbp
	movq	32(%r13), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%r13)
	movq	40(%r13), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 40(%r13)
	incq	%rbx
	cmpq	%rbx, %r12
	jne	.LBB24_9
# BB#10:
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB24_18:                              # %._crit_edge
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	%ecx, 64(%rdx)
	movl	64(%r15), %eax
	subl	%ecx, %eax
	movl	%eax, 64(%r13)
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, 68(%rdx)
	movl	68(%r15), %eax
	subl	%ecx, %eax
	movl	%eax, 68(%r13)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 72(%rdx)
	movl	72(%r15), %eax
	subl	%ecx, %eax
	movl	%eax, 72(%r13)
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	104(%rax), %rbx
	movq	112(%rax), %rbp
	movq	%r13, %rdi
	callq	clause_Normalize
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	clause_SetMaxLitFlags
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r13)
	movq	%r13, %rdi
	callq	clause_UpdateMaxVar
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r14d
	movl	%r14d, 12(%r13)
	cmpl	$64, %r14d
	jb	.LBB24_19
# BB#20:                                # %.lr.ph.i.i242
	addl	$-64, %r14d
	movl	%r14d, %ebp
	shrl	$6, %ebp
	movl	%ebp, %eax
	shll	$6, %eax
	incl	%ebp
	subl	%eax, %r14d
	jmp	.LBB24_21
.LBB24_19:
	xorl	%ebp, %ebp
.LBB24_21:                              # %clause_ComputeSplitFieldAddress.exit.i245
	movl	24(%r13), %ecx
	cmpl	%ecx, %ebp
	jb	.LBB24_31
# BB#22:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB24_30
# BB#23:
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB24_24
# BB#29:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB24_30
.LBB24_24:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rdx, %rbx
	movq	%r8, (%rbx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB24_26
# BB#25:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB24_26:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB24_28
# BB#27:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB24_28:
	addq	$-16, %rdi
	callq	free
.LBB24_30:                              # %memory_Free.exit266
	leal	8(,%rbp,8), %edi
	callq	memory_Malloc
	movq	%rax, 16(%r13)
	leal	1(%rbp), %ecx
	movl	%ecx, 24(%r13)
.LBB24_31:
	testb	$8, 48(%r15)
	je	.LBB24_33
# BB#32:
	orb	$8, 48(%r13)
.LBB24_33:                              # %.preheader29.i249
	cmpl	$0, 24(%r15)
	je	.LBB24_34
# BB#48:                                # %.lr.ph33.i252
	movq	%r13, %rdx
	addq	$16, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_49:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movl	%ecx, %esi
	movq	(%rax,%rsi,8), %rax
	movq	(%rdx), %rdi
	movq	%rax, (%rdi,%rsi,8)
	incl	%ecx
	movl	24(%r15), %eax
	cmpl	%eax, %ecx
	jb	.LBB24_49
# BB#35:                                # %.preheader.loopexit.i254
	movl	24(%r13), %ecx
	jmp	.LBB24_36
.LBB24_34:                              # %.preheader29..preheader_crit_edge.i251
	movq	%r13, %rdx
	addq	$16, %rdx
	xorl	%eax, %eax
.LBB24_36:                              # %.preheader.i257
	movq	(%rdx), %rdx
	cmpl	%ecx, %eax
	jae	.LBB24_38
	.p2align	4, 0x90
.LBB24_37:                              # %.lr.ph.i260
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ecx
	movq	$0, (%rdx,%rcx,8)
	incl	%eax
	movq	16(%r13), %rdx
	cmpl	24(%r13), %eax
	jb	.LBB24_37
.LBB24_38:                              # %clause_UpdateSplitDataFromNewSplitting.exit262
	movl	%ebp, %eax
	movl	$1, %esi
	movl	%r14d, %ecx
	shlq	%cl, %rsi
	orq	%rsi, (%rdx,%rax,8)
	movl	$15, 76(%r13)
	movq	40(%r13), %rdi
	callq	list_NReverse
	movq	%rax, 40(%r13)
	movl	8(%r15), %eax
	incl	%eax
	movl	%eax, 8(%r13)
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	8(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	120(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 120(%rbp)
	movq	104(%rbp), %r14
	movq	112(%rbp), %r12
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	callq	clause_Normalize
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	clause_SetMaxLitFlags
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbp)
	movq	%rbp, %rdi
	callq	clause_UpdateMaxVar
	movl	(%rbx), %r14d
	movl	%r14d, 12(%rbp)
	xorl	%ebx, %ebx
	cmpl	$64, %r14d
	jb	.LBB24_40
# BB#39:                                # %.lr.ph.i.i215
	addl	$-64, %r14d
	movl	%r14d, %ebx
	shrl	$6, %ebx
	movl	%ebx, %eax
	shll	$6, %eax
	incl	%ebx
	subl	%eax, %r14d
.LBB24_40:                              # %clause_ComputeSplitFieldAddress.exit.i218
	movl	24(%rbp), %ecx
	cmpl	%ecx, %ebx
	jb	.LBB24_52
# BB#41:
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB24_51
# BB#42:
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB24_43
# BB#50:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB24_51
.LBB24_43:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB24_45
# BB#44:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB24_45:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB24_47
# BB#46:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB24_47:
	addq	$-16, %rdi
	callq	free
.LBB24_51:                              # %memory_Free.exit239
	leal	8(,%rbx,8), %edi
	callq	memory_Malloc
	movq	%rax, 16(%rbp)
	leal	1(%rbx), %ecx
	movl	%ecx, 24(%rbp)
.LBB24_52:
	testb	$8, 48(%r15)
	je	.LBB24_54
# BB#53:
	orb	$8, 48(%rbp)
.LBB24_54:                              # %.preheader29.i222
	cmpl	$0, 24(%r15)
	je	.LBB24_55
# BB#68:                                # %.lr.ph33.i225
	movq	%rbp, %rdx
	addq	$16, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_69:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movl	%ecx, %esi
	movq	(%rax,%rsi,8), %rax
	movq	(%rdx), %rdi
	movq	%rax, (%rdi,%rsi,8)
	incl	%ecx
	movl	24(%r15), %eax
	cmpl	%eax, %ecx
	jb	.LBB24_69
# BB#56:                                # %.preheader.loopexit.i227
	movl	24(%rbp), %ecx
	jmp	.LBB24_57
.LBB24_55:                              # %.preheader29..preheader_crit_edge.i224
	movq	%rbp, %rdx
	addq	$16, %rdx
	xorl	%eax, %eax
.LBB24_57:                              # %.preheader.i230
	movq	(%rdx), %rdx
	cmpl	%ecx, %eax
	jae	.LBB24_59
	.p2align	4, 0x90
.LBB24_58:                              # %.lr.ph.i233
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ecx
	movq	$0, (%rdx,%rcx,8)
	incl	%eax
	movq	16(%rbp), %rdx
	cmpl	24(%rbp), %eax
	jb	.LBB24_58
.LBB24_59:                              # %clause_UpdateSplitDataFromNewSplitting.exit235
	movl	%ebx, %eax
	movl	$1, %esi
	movl	%r14d, %ecx
	shlq	%cl, %rsi
	orq	%rsi, (%rdx,%rax,8)
	movl	$15, 76(%rbp)
	movq	40(%rbp), %rdi
	callq	list_NReverse
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rbp)
	movl	8(%r15), %eax
	incl	%eax
	movl	%eax, 8(%rbp)
	movl	48(%rbp), %eax
	testb	$1, %al
	je	.LBB24_61
# BB#60:
	decl	%eax
	movl	%eax, 48(%rbp)
.LBB24_61:                              # %clause_RemoveFlag.exit
	cmpl	$0, 52(%rbp)
	jne	.LBB24_93
# BB#62:
	movl	64(%rbp), %ecx
	movl	68(%rbp), %eax
	movq	%rcx, %rdi
	leal	-1(%rcx,%rax), %edx
	movl	72(%rbp), %ecx
	movl	%edx, %esi
	addl	%ecx, %esi
	js	.LBB24_93
# BB#63:                                # %.lr.ph
	movslq	%edx, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	addl	%eax, %edi
	addl	%ecx, %edi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB24_64:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_72 Depth 2
                                        #     Child Loop BB24_95 Depth 2
                                        #     Child Loop BB24_91 Depth 2
	movq	56(%rbp), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB24_66
# BB#65:                                #   in Loop: Header=BB24_64 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB24_66:                              # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB24_64 Depth=1
	callq	term_Copy
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$0, (%rbx)
	cmpq	40(%rsp), %r13          # 8-byte Folded Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	104(%rax), %r8
	movq	112(%rax), %rcx
	jle	.LBB24_67
# BB#70:                                #   in Loop: Header=BB24_64 Depth=1
	xorl	%edi, %edi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	jmp	.LBB24_71
	.p2align	4, 0x90
.LBB24_67:                              #   in Loop: Header=BB24_64 Depth=1
	xorl	%edi, %edi
	xorl	%esi, %esi
	movq	%rbx, %rdx
.LBB24_71:                              #   in Loop: Header=BB24_64 Depth=1
	callq	clause_Create
	movq	%rax, %rbp
	movl	$-1, (%rbp)
	decl	clause_CLAUSECOUNTER(%rip)
	testq	%rbx, %rbx
	je	.LBB24_73
	.p2align	4, 0x90
.LBB24_72:                              # %.lr.ph.i214
                                        #   Parent Loop BB24_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB24_72
.LBB24_73:                              # %list_Delete.exit
                                        #   in Loop: Header=BB24_64 Depth=1
	movl	$15, 76(%rbp)
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r14d
	movl	%r14d, 12(%rbp)
	xorl	%ebx, %ebx
	cmpl	$64, %r14d
	jb	.LBB24_75
# BB#74:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB24_64 Depth=1
	addl	$-64, %r14d
	movl	%r14d, %ebx
	shrl	$6, %ebx
	movl	%ebx, %eax
	shll	$6, %eax
	incl	%ebx
	subl	%eax, %r14d
.LBB24_75:                              # %clause_ComputeSplitFieldAddress.exit.i
                                        #   in Loop: Header=BB24_64 Depth=1
	movl	24(%rbp), %ecx
	cmpl	%ecx, %ebx
	jb	.LBB24_85
# BB#76:                                #   in Loop: Header=BB24_64 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB24_84
# BB#77:                                #   in Loop: Header=BB24_64 Depth=1
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB24_78
# BB#83:                                #   in Loop: Header=BB24_64 Depth=1
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB24_84
.LBB24_78:                              #   in Loop: Header=BB24_64 Depth=1
	xorl	%edx, %edx
	movl	%ecx, %eax
	movl	memory_ALIGN(%rip), %esi
	divl	%esi
	movl	%esi, %eax
	subl	%edx, %eax
	testl	%edx, %edx
	cmovel	%edx, %eax
	addl	%ecx, %eax
	movl	memory_OFFSET(%rip), %r9d
	movq	%rdi, %rdx
	subq	%r9, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ecx
	cmoveq	%rcx, %rsi
	movq	%r8, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB24_80
# BB#79:                                #   in Loop: Header=BB24_64 Depth=1
	negq	%r9
	movq	-16(%rdi,%r9), %rcx
	movq	%rcx, (%rdx)
.LBB24_80:                              #   in Loop: Header=BB24_64 Depth=1
	addl	memory_MARKSIZE(%rip), %eax
	movq	memory_FREEDBYTES(%rip), %rcx
	leaq	16(%rcx,%rax), %rcx
	movq	%rcx, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rcx
	testq	%rcx, %rcx
	js	.LBB24_82
# BB#81:                                #   in Loop: Header=BB24_64 Depth=1
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB24_82:                              #   in Loop: Header=BB24_64 Depth=1
	addq	$-16, %rdi
	callq	free
.LBB24_84:                              # %memory_Free.exit
                                        #   in Loop: Header=BB24_64 Depth=1
	leal	8(,%rbx,8), %edi
	callq	memory_Malloc
	movq	%rax, 16(%rbp)
	leal	1(%rbx), %ecx
	movl	%ecx, 24(%rbp)
.LBB24_85:                              #   in Loop: Header=BB24_64 Depth=1
	testb	$8, 48(%r15)
	je	.LBB24_87
# BB#86:                                #   in Loop: Header=BB24_64 Depth=1
	orb	$8, 48(%rbp)
.LBB24_87:                              # %.preheader29.i
                                        #   in Loop: Header=BB24_64 Depth=1
	cmpl	$0, 24(%r15)
	je	.LBB24_88
# BB#94:                                # %.lr.ph33.i.preheader
                                        #   in Loop: Header=BB24_64 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_95:                              # %.lr.ph33.i
                                        #   Parent Loop BB24_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r15), %rax
	movl	%ecx, %edx
	movq	(%rax,%rdx,8), %rax
	movq	16(%rbp), %rsi
	movq	%rax, (%rsi,%rdx,8)
	incl	%ecx
	movl	24(%r15), %eax
	cmpl	%eax, %ecx
	jb	.LBB24_95
# BB#89:                                # %.preheader.loopexit.i
                                        #   in Loop: Header=BB24_64 Depth=1
	movl	24(%rbp), %ecx
	jmp	.LBB24_90
	.p2align	4, 0x90
.LBB24_88:                              #   in Loop: Header=BB24_64 Depth=1
	xorl	%eax, %eax
.LBB24_90:                              # %.preheader.i
                                        #   in Loop: Header=BB24_64 Depth=1
	movq	16(%rbp), %rdx
	cmpl	%ecx, %eax
	jae	.LBB24_92
	.p2align	4, 0x90
.LBB24_91:                              # %.lr.ph.i
                                        #   Parent Loop BB24_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ecx
	movq	$0, (%rdx,%rcx,8)
	incl	%eax
	movq	16(%rbp), %rdx
	cmpl	24(%rbp), %eax
	jb	.LBB24_91
.LBB24_92:                              # %clause_UpdateSplitDataFromNewSplitting.exit
                                        #   in Loop: Header=BB24_64 Depth=1
	movl	%ebx, %eax
	movl	$1, %esi
	movl	%r14d, %ecx
	shlq	%cl, %rsi
	orq	%rsi, (%rdx,%rax,8)
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	(%rax), %rbx
	movq	32(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%rbp)
	movq	40(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 40(%rbp)
	movslq	(%r15), %rbx
	movq	32(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%rbp)
	movq	32(%rsp), %r12          # 8-byte Reload
	movslq	8(%r12), %rbx
	movq	40(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbp)
	movq	(%r12), %r12
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	8(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rax, 8(%rbx)
	incq	%r13
	cmpq	64(%rsp), %r13          # 8-byte Folded Reload
	jne	.LBB24_64
.LBB24_93:                              # %.loopexit
	movq	%rbp, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	prfs_DoSplitting, .Lfunc_end24-prfs_DoSplitting
	.cfi_endproc

	.globl	prfs_PerformSplitting
	.p2align	4, 0x90
	.type	prfs_PerformSplitting,@function
prfs_PerformSplitting:                  # @prfs_PerformSplitting
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi144:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi145:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi147:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi148:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi150:
	.cfi_def_cfa_offset 96
.Lcfi151:
	.cfi_offset %rbx, -56
.Lcfi152:
	.cfi_offset %r12, -48
.Lcfi153:
	.cfi_offset %r13, -40
.Lcfi154:
	.cfi_offset %r14, -32
.Lcfi155:
	.cfi_offset %r15, -24
.Lcfi156:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	%r15, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB25_67
# BB#1:
	cmpl	$0, 136(%r12)
	je	.LBB25_67
# BB#2:
	movl	72(%r15), %eax
	cmpl	$2, %eax
	jl	.LBB25_67
# BB#3:                                 # %.lr.ph.preheader.i.i
	movslq	68(%r15), %rcx
	movslq	64(%r15), %rbx
	addq	%rcx, %rbx
	addl	%ebx, %eax
	movslq	%eax, %rbp
	.p2align	4, 0x90
.LBB25_4:                               # %.lr.ph.i134.i
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_IsGround
	testl	%eax, %eax
	jne	.LBB25_51
# BB#5:                                 #   in Loop: Header=BB25_4 Depth=1
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB25_4
# BB#6:
	movl	64(%r15), %eax
	movl	72(%r15), %ecx
	addl	68(%r15), %eax
	leal	-1(%rcx,%rax), %eax
	movq	56(%r15), %rcx
	cltq
	movq	(%rcx,%rax,8), %rbp
	movq	24(%rbp), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	movq	%r12, 16(%rsp)          # 8-byte Spill
	jne	.LBB25_8
# BB#7:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB25_8:                               # %clause_LiteralAtom.exit133.i
	callq	term_VariableSymbols
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$0, (%rbx)
	movl	64(%r15), %eax
	movl	68(%r15), %edx
	movl	72(%r15), %esi
	leal	(%rsi,%rdx), %ecx
	addl	%eax, %edx
	leal	-1(%rax,%rcx), %r13d
	testl	%r13d, %r13d
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	jle	.LBB25_31
	.p2align	4, 0x90
.LBB25_9:                               # %.lr.ph12.i.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_10 Depth 2
                                        #       Child Loop BB25_11 Depth 3
                                        #         Child Loop BB25_12 Depth 4
                                        #         Child Loop BB25_18 Depth 4
                                        #       Child Loop BB25_20 Depth 3
                                        #         Child Loop BB25_24 Depth 4
	xorl	%eax, %eax
	movq	%rbx, %r14
.LBB25_10:                              # %.lr.ph12.i
                                        #   Parent Loop BB25_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_11 Depth 3
                                        #         Child Loop BB25_12 Depth 4
                                        #         Child Loop BB25_18 Depth 4
                                        #       Child Loop BB25_20 Depth 3
                                        #         Child Loop BB25_24 Depth 4
	decl	%r13d
	testq	%r14, %r14
	movslq	%r13d, %r13
	movl	%eax, 8(%rsp)           # 4-byte Spill
	je	.LBB25_20
	.p2align	4, 0x90
.LBB25_11:                              # %.lr.ph12.split.i
                                        #   Parent Loop BB25_9 Depth=1
                                        #     Parent Loop BB25_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB25_12 Depth 4
                                        #         Child Loop BB25_18 Depth 4
	movq	56(%r15), %rax
	movq	(%rax,%r13,8), %rbp
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB25_12:                              # %.lr.ph.i121.i
                                        #   Parent Loop BB25_9 Depth=1
                                        #     Parent Loop BB25_10 Depth=2
                                        #       Parent Loop BB25_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbp, 8(%rax)
	je	.LBB25_19
# BB#13:                                #   in Loop: Header=BB25_12 Depth=4
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB25_12
# BB#14:                                # %.loopexit.i
                                        #   in Loop: Header=BB25_11 Depth=3
	movq	24(%rbp), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB25_16
# BB#15:                                #   in Loop: Header=BB25_11 Depth=3
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB25_16:                              # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB25_11 Depth=3
	callq	term_VariableSymbols
	movq	%rax, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	list_HasIntersection
	testl	%eax, %eax
	jne	.LBB25_26
# BB#17:                                #   in Loop: Header=BB25_11 Depth=3
	testq	%rbx, %rbx
	je	.LBB25_19
	.p2align	4, 0x90
.LBB25_18:                              # %.lr.ph.i115.i
                                        #   Parent Loop BB25_9 Depth=1
                                        #     Parent Loop BB25_10 Depth=2
                                        #       Parent Loop BB25_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB25_18
.LBB25_19:                              # %list_PointerMember.exit125.backedge.i
                                        #   in Loop: Header=BB25_11 Depth=3
	testq	%r13, %r13
	leaq	-1(%r13), %r13
	jg	.LBB25_11
	jmp	.LBB25_27
	.p2align	4, 0x90
.LBB25_20:                              # %.lr.ph12.split.us.i
                                        #   Parent Loop BB25_9 Depth=1
                                        #     Parent Loop BB25_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB25_24 Depth 4
	movq	56(%r15), %rax
	movq	(%rax,%r13,8), %rbp
	movq	24(%rbp), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB25_22
# BB#21:                                #   in Loop: Header=BB25_20 Depth=3
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB25_22:                              # %clause_LiteralAtom.exit.us.i
                                        #   in Loop: Header=BB25_20 Depth=3
	callq	term_VariableSymbols
	movq	%rax, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	list_HasIntersection
	testl	%eax, %eax
	jne	.LBB25_26
# BB#23:                                #   in Loop: Header=BB25_20 Depth=3
	testq	%rbx, %rbx
	je	.LBB25_25
	.p2align	4, 0x90
.LBB25_24:                              # %.lr.ph.i115.us.i
                                        #   Parent Loop BB25_9 Depth=1
                                        #     Parent Loop BB25_10 Depth=2
                                        #       Parent Loop BB25_20 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB25_24
.LBB25_25:                              # %list_PointerMember.exit125.backedge.us.i
                                        #   in Loop: Header=BB25_20 Depth=3
	testq	%r13, %r13
	leaq	-1(%r13), %r13
	jg	.LBB25_20
	jmp	.LBB25_28
	.p2align	4, 0x90
.LBB25_26:                              # %list_PointerMember.exit125.outer.i
                                        #   in Loop: Header=BB25_10 Depth=2
	movq	%r12, %rdi
	callq	list_Length
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	list_NPointerUnion
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	%r14, (%rbx)
	movq	%r12, %rdi
	callq	list_Length
	cmpl	%eax, 12(%rsp)          # 4-byte Folded Reload
	movl	$1, %eax
	movl	8(%rsp), %ecx           # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, %eax
	testl	%r13d, %r13d
	movq	%rbx, %r14
	jg	.LBB25_10
	jmp	.LBB25_29
	.p2align	4, 0x90
.LBB25_27:                              #   in Loop: Header=BB25_9 Depth=1
	movq	%r14, %rbx
	movl	8(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB25_30
	jmp	.LBB25_31
	.p2align	4, 0x90
.LBB25_28:                              #   in Loop: Header=BB25_9 Depth=1
	xorl	%ebx, %ebx
	movl	8(%rsp), %eax           # 4-byte Reload
.LBB25_29:                              # %.loopexit2.i
                                        #   in Loop: Header=BB25_9 Depth=1
	testl	%eax, %eax
	je	.LBB25_31
.LBB25_30:                              # %.loopexit2._crit_edge.i
                                        #   in Loop: Header=BB25_9 Depth=1
	movl	64(%r15), %eax
	movl	72(%r15), %ecx
	addl	68(%r15), %ecx
	leal	-1(%rax,%rcx), %r13d
	testl	%r13d, %r13d
	jg	.LBB25_9
.LBB25_31:                              # %.loopexit2.thread.i
	movq	32(%rsp), %rbp          # 8-byte Reload
	addl	24(%rsp), %ebp          # 4-byte Folded Reload
	movq	%rbx, %rdi
	callq	list_Length
	cmpl	%ebp, %eax
	jne	.LBB25_35
# BB#32:
	testq	%rbx, %rbx
	je	.LBB25_34
	.p2align	4, 0x90
.LBB25_33:                              # %.lr.ph.i110.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB25_33
.LBB25_34:
	xorl	%ebx, %ebx
.LBB25_35:                              # %list_Delete.exit111.i
	movslq	68(%r15), %rcx
	movslq	64(%r15), %rax
	addq	%rcx, %rax
	cmpl	%ebp, %eax
	jge	.LBB25_42
# BB#36:                                # %.lr.ph.i
	testq	%rbx, %rbx
	je	.LBB25_44
# BB#37:                                # %.lr.ph.split.preheader.i
	movq	56(%r15), %rcx
	movslq	%ebp, %rdx
.LBB25_38:                              # %.lr.ph.split.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_39 Depth 2
	movq	(%rcx,%rax,8), %rsi
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB25_39:                              # %.lr.ph.i106.i
                                        #   Parent Loop BB25_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, 8(%rdi)
	je	.LBB25_41
# BB#40:                                #   in Loop: Header=BB25_39 Depth=2
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB25_39
	jmp	.LBB25_45
.LBB25_41:                              # %list_PointerMember.exit.i
                                        #   in Loop: Header=BB25_38 Depth=1
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB25_38
.LBB25_42:                              # %.critedge.i
	testq	%rbx, %rbx
	je	.LBB25_44
	.p2align	4, 0x90
.LBB25_43:                              # %.lr.ph.i104.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB25_43
.LBB25_44:
	xorl	%ebx, %ebx
.LBB25_45:                              # %list_Delete.exit105.i
	testq	%r12, %r12
	je	.LBB25_47
	.p2align	4, 0x90
.LBB25_46:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB25_46
.LBB25_47:
	movq	16(%rsp), %r12          # 8-byte Reload
	testq	%rbx, %rbx
	jne	.LBB25_65
	jmp	.LBB25_67
.LBB25_51:                              # %clause_HasGroundSuccLit.exit.i
	movl	72(%r15), %eax
	testl	%eax, %eax
	jle	.LBB25_55
# BB#52:                                # %.lr.ph.i138.i
	movslq	68(%r15), %rcx
	movslq	64(%r15), %rbx
	addq	%rcx, %rbx
	addl	%ebx, %eax
	movslq	%eax, %rbp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB25_53:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_IsGround
	testl	%eax, %eax
	jne	.LBB25_56
# BB#54:                                #   in Loop: Header=BB25_53 Depth=1
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB25_53
	jmp	.LBB25_57
.LBB25_55:
	xorl	%r14d, %r14d
	jmp	.LBB25_57
.LBB25_56:
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %r14
.LBB25_57:                              # %clause_GetGroundSuccLit.exit.i
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	$0, (%rbx)
	movl	64(%r15), %eax
	addl	68(%r15), %eax
	decl	%eax
	js	.LBB25_64
# BB#58:                                # %.lr.ph34.preheader.i
	movslq	%eax, %rbp
	incq	%rbp
	.p2align	4, 0x90
.LBB25_59:                              # %.lr.ph34.i
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	-8(%rax,%rbp,8), %r14
	movq	24(%r14), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB25_61
# BB#60:                                #   in Loop: Header=BB25_59 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB25_61:                              # %clause_LiteralAtom.exit149.i
                                        #   in Loop: Header=BB25_59 Depth=1
	callq	term_IsGround
	testl	%eax, %eax
	je	.LBB25_63
# BB#62:                                #   in Loop: Header=BB25_59 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, %rbx
.LBB25_63:                              #   in Loop: Header=BB25_59 Depth=1
	decq	%rbp
	jg	.LBB25_59
.LBB25_64:                              # %prfs_GetSplitLiterals.exit
	testq	%rbx, %rbx
	je	.LBB25_67
.LBB25_65:
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	prfs_DoSplitting
	.p2align	4, 0x90
.LBB25_66:                              # %.lr.ph.i12
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB25_66
	jmp	.LBB25_68
.LBB25_67:
	xorl	%eax, %eax
.LBB25_68:                              # %list_Delete.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	prfs_PerformSplitting, .Lfunc_end25-prfs_PerformSplitting
	.cfi_endproc

	.globl	prfs_InstallFiniteMonadicPredicates
	.p2align	4, 0x90
	.type	prfs_InstallFiniteMonadicPredicates,@function
prfs_InstallFiniteMonadicPredicates:    # @prfs_InstallFiniteMonadicPredicates
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi157:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi158:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi160:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi161:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 64
.Lcfi164:
	.cfi_offset %rbx, -56
.Lcfi165:
	.cfi_offset %r12, -48
.Lcfi166:
	.cfi_offset %r13, -40
.Lcfi167:
	.cfi_offset %r14, -32
.Lcfi168:
	.cfi_offset %r15, -24
.Lcfi169:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %rbx
	testq	%rbx, %rbx
	movq	%rdi, (%rsp)            # 8-byte Spill
	je	.LBB26_1
# BB#2:                                 # %.lr.ph
	xorl	%r15d, %r15d
	testq	%r12, %r12
	je	.LBB26_10
	.p2align	4, 0x90
.LBB26_3:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_8 Depth 2
	movq	8(%rbx), %rax
	movl	72(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB26_14
# BB#4:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB26_3 Depth=1
	movslq	68(%rax), %rsi
	movslq	64(%rax), %rdx
	addq	%rsi, %rdx
	addl	%edx, %ecx
	cmpl	$1, %ecx
	jne	.LBB26_14
# BB#5:                                 #   in Loop: Header=BB26_3 Depth=1
	movq	56(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	24(%rax), %rbp
	movl	(%rbp), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB26_7
# BB#6:                                 #   in Loop: Header=BB26_3 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
	movl	(%rbp), %eax
.LBB26_7:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB26_3 Depth=1
	movslq	%eax, %rsi
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB26_8:                               # %.lr.ph.i
                                        #   Parent Loop BB26_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, 8(%rax)
	je	.LBB26_11
# BB#9:                                 #   in Loop: Header=BB26_8 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB26_8
	jmp	.LBB26_14
	.p2align	4, 0x90
.LBB26_11:                              # %list_PointerMember.exit
                                        #   in Loop: Header=BB26_3 Depth=1
	movq	%r15, %rdi
	callq	list_AssocListPair
	movq	%rax, %r13
	testq	%r13, %r13
	movq	16(%rbp), %rax
	je	.LBB26_13
# BB#12:                                #   in Loop: Header=BB26_3 Depth=1
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movq	(%r13), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, (%r13)
	jmp	.LBB26_14
.LBB26_13:                              #   in Loop: Header=BB26_3 Depth=1
	movslq	(%rbp), %r14
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r13, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%r14, 8(%r13)
	movq	%rbp, (%r13)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB26_14:                              # %list_PointerMember.exit.thread
                                        #   in Loop: Header=BB26_3 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB26_3
	jmp	.LBB26_15
	.p2align	4, 0x90
.LBB26_10:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB26_10
	jmp	.LBB26_15
.LBB26_1:
	xorl	%r15d, %r15d
.LBB26_15:                              # %._crit_edge
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	24(%rbx), %rdi
	movl	$term_DeleteTermList, %esi
	callq	list_DeleteAssocListWithValues
	movq	%r15, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	prfs_InstallFiniteMonadicPredicates, .Lfunc_end26-prfs_InstallFiniteMonadicPredicates
	.cfi_endproc

	.globl	prfs_GetNumberOfInstances
	.p2align	4, 0x90
	.type	prfs_GetNumberOfInstances,@function
prfs_GetNumberOfInstances:              # @prfs_GetNumberOfInstances
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi172:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi173:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi174:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi176:
	.cfi_def_cfa_offset 80
.Lcfi177:
	.cfi_offset %rbx, -56
.Lcfi178:
	.cfi_offset %r12, -48
.Lcfi179:
	.cfi_offset %r13, -40
.Lcfi180:
	.cfi_offset %r14, -32
.Lcfi181:
	.cfi_offset %r15, -24
.Lcfi182:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r13
	movq	24(%r13), %rbp
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbp), %eax
	jne	.LBB27_2
# BB#1:
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB27_2:                               # %clause_LiteralAtom.exit
	movq	32(%rdi), %rsi
	movq	48(%rdi), %rbx
	movq	%rbp, %rdi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	callq	sharing_GetNumberOfInstances
	movl	%eax, %r15d
	testl	%r14d, %r14d
	je	.LBB27_3
# BB#4:
	movq	%rbp, %rdi
	movq	%rbx, %r12
	movq	%rbx, %rsi
	callq	sharing_GetNumberOfInstances
	addl	%eax, %r15d
	jmp	.LBB27_5
.LBB27_3:
	movq	%rbx, %r12
.LBB27_5:
	movl	(%rbp), %ebx
	cmpl	%ebx, fol_EQUALITY(%rip)
	jne	.LBB27_16
# BB#6:
	movq	16(%rbp), %rdi
	callq	list_Reverse
	movl	%ebx, %edi
	movq	%rax, %rsi
	callq	term_Create
	movl	%r14d, %ebx
	movq	%rax, %r14
	movq	%r14, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	sharing_GetNumberOfInstances
	movl	%eax, %ebp
	addl	%r15d, %ebp
	movl	%ebx, %r15d
	testl	%ebx, %ebx
	je	.LBB27_8
# BB#7:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	sharing_GetNumberOfInstances
	addl	%eax, %ebp
.LBB27_8:
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.LBB27_10
	.p2align	4, 0x90
.LBB27_9:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB27_9
.LBB27_10:                              # %list_Delete.exit
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%r14, (%rax)
	movq	24(%r13), %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB27_12
# BB#11:
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB27_12:                              # %clause_LiteralAtom.exit52
	movl	%r15d, %r14d
	cmpl	$0, 8(%r13)
	je	.LBB27_13
# BB#14:
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	sharing_GetNumberOfInstances
	movl	%eax, %r15d
	addl	%ebp, %r15d
	testl	%r14d, %r14d
	je	.LBB27_16
# BB#15:
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	sharing_GetNumberOfInstances
	addl	%eax, %r15d
	jmp	.LBB27_16
.LBB27_13:
	movl	%ebp, %r15d
.LBB27_16:
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	prfs_GetNumberOfInstances, .Lfunc_end27-prfs_GetNumberOfInstances
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi183:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end28:
	.size	misc_Error, .Lfunc_end28-misc_Error
	.cfi_endproc

	.p2align	4, 0x90
	.type	term_DeleteTermList,@function
term_DeleteTermList:                    # @term_DeleteTermList
	.cfi_startproc
# BB#0:
	movl	$term_Delete, %esi
	jmp	list_DeleteWithElement  # TAILCALL
.Lfunc_end29:
	.size	term_DeleteTermList, .Lfunc_end29-term_DeleteTermList
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n Split: %d %ld"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n Father: "
	.size	.L.str.1, 11

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"No father, unnecessary split."
	.size	.L.str.2, 30

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Split is "
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"unused."
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"used."
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" Blocked clauses:"
	.size	.L.str.6, 18

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n Deleted clauses:"
	.size	.L.str.7, 19

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\n Splitstack:"
	.size	.L.str.8, 14

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\n---------------------"
	.size	.L.str.9, 23

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\n\n Proofsearch: Current Level: %d Last Backtrack Level: %d Splits: %d Loops: %d Backtracked: %d"
	.size	.L.str.10, 96

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n Clause %d implies a non-trivial domain."
	.size	.L.str.11, 42

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\n Potentially trivial domain."
	.size	.L.str.12, 30

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"\n Empty Clauses:"
	.size	.L.str.13, 17

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\n "
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"\n Definitions:"
	.size	.L.str.15, 15

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\n Worked Off Clauses:"
	.size	.L.str.16, 22

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\n Usable Clauses:"
	.size	.L.str.17, 18

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\n Finite predicates:"
	.size	.L.str.18, 21

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\n  "
	.size	.L.str.19, 4

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	": "
	.size	.L.str.20, 3

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\n Static Sort Theory:"
	.size	.L.str.21, 22

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"\n Dynamic Sort Theory:"
	.size	.L.str.22, 23

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"\n Approximated Dynamic Sort Theory:"
	.size	.L.str.23, 36

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\n Error: Flag value %d is too small for flag %s.\n"
	.size	.L.str.24, 50

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"\n Error: Flag value %d is too large for flag %s.\n"
	.size	.L.str.25, 50


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
