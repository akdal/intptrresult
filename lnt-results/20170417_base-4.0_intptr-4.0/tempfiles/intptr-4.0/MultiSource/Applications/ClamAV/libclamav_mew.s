	.text
	.file	"libclamav_mew.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	67109888                # 0x4000400
	.long	67109888                # 0x4000400
	.long	67109888                # 0x4000400
	.long	67109888                # 0x4000400
	.text
	.globl	mew_lzma
	.p2align	4, 0x90
	.type	mew_lzma,@function
mew_lzma:                               # @mew_lzma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.LBB0_1
# BB#2:
	movl	(%rsi), %edi
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	addq	$4, %rsi
	jmp	.LBB0_3
.LBB0_1:
	xorl	%edi, %edi
	movq	%rdi, 152(%rsp)         # 8-byte Spill
.LBB0_3:
	movl	(%rsi), %edi
	subl	%ecx, %edi
	addq	$4, %rsi
	testl	%r8d, %r8d
	movq	%rsi, %r12
	cmovneq	%rax, %r12
	leaq	(%r14,%rdi), %r15
	cmpl	$28267, %edx            # imm = 0x6E6B
	seta	%r9b
	leaq	28268(%r14,%rdi), %rbp
	movl	%edx, 76(%rsp)          # 4-byte Spill
	movl	%edx, %ebx
	addq	%r14, %rbx
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	cmpq	%rbx, %rbp
	setbe	%al
	cmpq	%rbp, %r14
	sbbb	%bl, %bl
	andb	%al, %bl
	leaq	2664(%r14,%rdi), %r10
	leaq	1636(%r14,%rdi), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	leaq	1604(%r14,%rdi), %r11
	andb	%bl, %r9b
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [67109888,67109888,67109888,67109888]
	xorl	%r13d, %r13d
.LBB0_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_15 Depth 2
                                        #     Child Loop BB0_7 Depth 2
                                        #       Child Loop BB0_9 Depth 3
                                        #     Child Loop BB0_18 Depth 2
                                        #       Child Loop BB0_50 Depth 3
                                        #       Child Loop BB0_68 Depth 3
                                        #       Child Loop BB0_78 Depth 3
                                        #       Child Loop BB0_176 Depth 3
                                        #       Child Loop BB0_219 Depth 3
                                        #       Child Loop BB0_237 Depth 3
                                        #       Child Loop BB0_187 Depth 3
	testl	%r8d, %r8d
	je	.LBB0_11
# BB#5:                                 # %.split.us
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	$-1, %ebp
	cmpl	$28268, 76(%rsp)        # 4-byte Folded Reload
                                        # imm = 0x6E6C
	jb	.LBB0_268
# BB#6:                                 # %.split.us.split.us.preheader
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rsi, %rdx
	movabsq	$288234774265332736, %rbp # imm = 0x400040004000400
	.p2align	4, 0x90
.LBB0_7:                                # %.split.us.split.us
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_9 Depth 3
	testb	%bl, %bl
	je	.LBB0_267
# BB#8:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_7 Depth=2
	movq	%r13, 160(%rsp)         # 8-byte Spill
	movl	(%rdx), %r12d
	movl	4(%rdx), %r13d
	subl	%ecx, %r13d
	addq	%r14, %r13
	movl	8(%rdx), %eax
	leaq	13(%rdx,%rax), %rsi
	movl	$28, %eax
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_270:                              # %vector.body.3
                                        #   in Loop: Header=BB0_9 Depth=3
	movups	%xmm0, -16(%r15,%rax,4)
	movups	%xmm0, (%r15,%rax,4)
	addq	$32, %rax
.LBB0_9:                                # %vector.body
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	%xmm0, -112(%r15,%rax,4)
	movups	%xmm0, -96(%r15,%rax,4)
	movups	%xmm0, -80(%r15,%rax,4)
	movups	%xmm0, -64(%r15,%rax,4)
	movups	%xmm0, -48(%r15,%rax,4)
	movups	%xmm0, -32(%r15,%rax,4)
	cmpq	$7068, %rax             # imm = 0x1B9C
	jne	.LBB0_270
# BB#10:                                # %.preheader522.us.us
                                        #   in Loop: Header=BB0_7 Depth=2
	movq	%rbp, 28256(%r15)
	movl	$67109888, 28264(%r15)  # imm = 0x4000400
	movl	14(%rdx), %eax
	bswapl	%eax
	movl	%eax, 28(%rsp)
	movl	$-1, 24(%rsp)
	addq	$18, %rdx
	movq	%rdx, 16(%rsp)
	movl	$0, 44(%rsp)
	movl	%r12d, %eax
	testl	%eax, %eax
	movq	%rsi, %rdx
	movq	%r13, %rdi
	je	.LBB0_7
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_11:                               # %.split
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.LBB0_12
# BB#13:                                #   in Loop: Header=BB0_4 Depth=1
	testb	%r9b, %r9b
	je	.LBB0_267
# BB#14:                                # %min.iters.checked757
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%r13, 160(%rsp)         # 8-byte Spill
	movl	4(%r12), %r13d
	movl	8(%r12), %edx
	subl	%ecx, %r13d
	addq	%r14, %r13
	leaq	13(%r12,%rdx), %rsi
	movl	$28, %edx
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_269:                              # %vector.body754.3
                                        #   in Loop: Header=BB0_15 Depth=2
	movups	%xmm0, -16(%r15,%rdx,4)
	movups	%xmm0, (%r15,%rdx,4)
	addq	$32, %rdx
.LBB0_15:                               # %vector.body754
                                        #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm0, -112(%r15,%rdx,4)
	movups	%xmm0, -96(%r15,%rdx,4)
	movups	%xmm0, -80(%r15,%rdx,4)
	movups	%xmm0, -64(%r15,%rdx,4)
	movups	%xmm0, -48(%r15,%rdx,4)
	movups	%xmm0, -32(%r15,%rdx,4)
	cmpq	$7068, %rdx             # imm = 0x1B9C
	jne	.LBB0_269
# BB#16:                                # %.preheader522
                                        #   in Loop: Header=BB0_4 Depth=1
	movl	%eax, %edx
	movabsq	$288234774265332736, %rax # imm = 0x400040004000400
	movq	%rax, 28256(%r15)
	movl	%edx, %eax
	movl	$67109888, 28264(%r15)  # imm = 0x4000400
	movl	14(%r12), %edx
	bswapl	%edx
	movl	%edx, 28(%rsp)
	movl	$-1, 24(%rsp)
	addq	$18, %r12
	movq	%r12, 16(%rsp)
	movl	$0, 44(%rsp)
	movq	160(%rsp), %rdi         # 8-byte Reload
.LBB0_17:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	%rdi, 184(%rsp)         # 8-byte Spill
	movq	%r11, 192(%rsp)         # 8-byte Spill
	movq	%r10, 144(%rsp)         # 8-byte Spill
	movb	%bl, 102(%rsp)          # 1-byte Spill
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	movb	%r9b, 103(%rsp)         # 1-byte Spill
	movl	%ecx, 176(%rsp)         # 4-byte Spill
	movl	%r8d, 180(%rsp)         # 4-byte Spill
	xorl	%r12d, %r12d
	movl	$.L.str, %edi
	movl	%eax, %ebx
	xorl	%eax, %eax
	movq	%r14, 56(%rsp)          # 8-byte Spill
	callq	cli_dbgmsg
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [67109888,67109888,67109888,67109888]
	movl	76(%rsp), %r11d         # 4-byte Reload
	movq	56(%rsp), %r14          # 8-byte Reload
	movl	%ebx, %r10d
	decl	%ebx
	movl	%ebx, 140(%rsp)         # 4-byte Spill
	movl	$1, 112(%rsp)           # 4-byte Folded Spill
	xorl	%edx, %edx
	movl	$1, 84(%rsp)            # 4-byte Folded Spill
	movl	$1, 80(%rsp)            # 4-byte Folded Spill
	xorl	%r8d, %r8d
	movl	$1, 68(%rsp)            # 4-byte Folded Spill
	xorl	%edi, %edi
	movq	%r10, 120(%rsp)         # 8-byte Spill
	jmp	.LBB0_18
.LBB0_75:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%r12d, %eax
	subl	%edx, %eax
	addl	%eax, %eax
	leaq	1374(%r15,%rax), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rax, 32(%rsp)
	movl	$1, 44(%rsp)
	testl	%ebx, %ebx
	je	.LBB0_271
# BB#76:                                #   in Loop: Header=BB0_18 Depth=2
	movl	%r12d, 136(%rsp)        # 4-byte Spill
	jmp	.LBB0_77
.LBB0_66:                               #   in Loop: Header=BB0_18 Depth=2
	xorl	%ecx, %ecx
.LBB0_74:                               # %.thread
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%r9d, 28(%rsp)
	movl	%edx, 24(%rsp)
	movl	%ecx, 12(%rsp)
	shll	$4, %ecx
	addl	%r12d, %ecx
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	%rax, 32(%rsp)
	movl	$1, 44(%rsp)
	movl	$4, %ebx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ecx, 136(%rsp)         # 4-byte Spill
	movq	%rax, 104(%rsp)         # 8-byte Spill
.LBB0_77:                               # %.preheader.split.us.i.us.preheader
                                        #   in Loop: Header=BB0_18 Depth=2
	xorl	%esi, %esi
	movl	$1, %ebp
	movl	44(%rsp), %r8d
	xorl	%ecx, %ecx
	movl	%ebx, 172(%rsp)         # 4-byte Spill
.LBB0_78:                               # %.preheader.split.us.i.us
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addl	%ebp, %ebp
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp), %rdx
	movq	%rdx, 32(%rsp)
	cmpq	56(%rsp), %rdx          # 8-byte Folded Reload
	jb	.LBB0_266
# BB#79:                                #   in Loop: Header=BB0_78 Depth=3
	leaq	4(%rdx), %rax
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_266
# BB#80:                                #   in Loop: Header=BB0_78 Depth=3
	cmpq	56(%rsp), %rax          # 8-byte Folded Reload
	jbe	.LBB0_266
# BB#81:                                #   in Loop: Header=BB0_78 Depth=3
	movq	16(%rsp), %r9
	cmpq	56(%rsp), %r9           # 8-byte Folded Reload
	jb	.LBB0_266
# BB#82:                                #   in Loop: Header=BB0_78 Depth=3
	leaq	1(%r9), %r12
	cmpq	48(%rsp), %r12          # 8-byte Folded Reload
	ja	.LBB0_266
# BB#83:                                #   in Loop: Header=BB0_78 Depth=3
	cmpq	56(%rsp), %r12          # 8-byte Folded Reload
	jbe	.LBB0_266
# BB#84:                                #   in Loop: Header=BB0_78 Depth=3
	movl	%esi, 88(%rsp)          # 4-byte Spill
	movl	24(%rsp), %r10d
	movl	28(%rsp), %r11d
	movl	%r10d, %esi
	shrl	$11, %esi
	movl	(%rdx), %r8d
	movzwl	%r8w, %edi
	imull	%edi, %esi
	movl	%r11d, %ebx
	subl	%esi, %ebx
	jae	.LBB0_85
# BB#86:                                #   in Loop: Header=BB0_78 Depth=3
	movl	%esi, 24(%rsp)
	movl	$2048, %eax             # imm = 0x800
	subl	%edi, %eax
	shrl	$5, %eax
	addl	%r8d, %eax
	xorl	%r14d, %r14d
	movl	%r11d, %ebx
	jmp	.LBB0_87
.LBB0_85:                               #   in Loop: Header=BB0_78 Depth=3
	subl	%esi, %r10d
	movl	%r10d, 24(%rsp)
	movl	%ebx, 28(%rsp)
	shrl	$5, %edi
	movl	%r8d, %eax
	subl	%edi, %eax
	movl	$1, %r14d
	movl	%r10d, %esi
.LBB0_87:                               #   in Loop: Header=BB0_78 Depth=3
	andl	$-65536, %r8d           # imm = 0xFFFF0000
	movzwl	%ax, %eax
	orl	%r8d, %eax
	movl	%eax, (%rdx)
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	movl	76(%rsp), %r11d         # 4-byte Reload
	movq	120(%rsp), %r10         # 8-byte Reload
	movl	72(%rsp), %edi          # 4-byte Reload
	ja	.LBB0_89
# BB#88:                                #   in Loop: Header=BB0_78 Depth=3
	movq	%r9, 32(%rsp)
	movzbl	(%r9), %eax
	shll	$8, %ebx
	orl	%eax, %ebx
	movq	%r12, 32(%rsp)
	shll	$8, %esi
	movl	%ebx, 28(%rsp)
	movl	%esi, 24(%rsp)
	movq	%r12, 16(%rsp)
.LBB0_89:                               # %lzma_486248.exit.us.i.us
                                        #   in Loop: Header=BB0_78 Depth=3
	orl	%r14d, %ebp
	shll	%cl, %r14d
	movl	88(%rsp), %esi          # 4-byte Reload
	orl	%r14d, %esi
	incl	%ecx
	movl	172(%rsp), %ebx         # 4-byte Reload
	cmpl	%ebx, %ecx
	movl	%ebp, %r8d
	jb	.LBB0_78
# BB#90:                                # %.loopexit516.loopexit
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%ebp, 44(%rsp)
	movq	56(%rsp), %r14          # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	136(%rsp), %r12d        # 4-byte Reload
	jmp	.LBB0_91
.LBB0_271:                              #   in Loop: Header=BB0_18 Depth=2
	xorl	%esi, %esi
.LBB0_91:                               # %.loopexit516
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%esi, 12(%rsp)
	addl	%esi, %r12d
	movl	%r12d, 88(%rsp)         # 4-byte Spill
.LBB0_92:                               #   in Loop: Header=BB0_18 Depth=2
	incl	88(%rsp)                # 4-byte Folded Spill
	jmp	.LBB0_93
	.p2align	4, 0x90
.LBB0_18:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_50 Depth 3
                                        #       Child Loop BB0_68 Depth 3
                                        #       Child Loop BB0_78 Depth 3
                                        #       Child Loop BB0_176 Depth 3
                                        #       Child Loop BB0_219 Depth 3
                                        #       Child Loop BB0_237 Depth 3
                                        #       Child Loop BB0_187 Depth 3
	movl	%edi, 72(%rsp)          # 4-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	andl	$3, %edi
	movl	%edi, 12(%rsp)
	movq	%r8, 128(%rsp)          # 8-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<kill> %R8<def>
	shll	$4, %r8d
	leal	(%rdi,%r8), %ecx
	addl	%ecx, %ecx
	addq	%r15, %rcx
	movq	%rcx, 32(%rsp)
	cmpl	$4, %r11d
	jb	.LBB0_200
# BB#19:                                #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %rcx
	jb	.LBB0_200
# BB#20:                                #   in Loop: Header=BB0_18 Depth=2
	leaq	4(%rcx), %rax
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#21:                                #   in Loop: Header=BB0_18 Depth=2
	testl	%r11d, %r11d
	je	.LBB0_200
# BB#22:                                #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %rax
	jbe	.LBB0_200
# BB#23:                                #   in Loop: Header=BB0_18 Depth=2
	movq	16(%rsp), %r9
	cmpq	%r14, %r9
	jb	.LBB0_200
# BB#24:                                #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%r9), %r11
	cmpq	48(%rsp), %r11          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#25:                                #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %r11
	jbe	.LBB0_200
# BB#26:                                #   in Loop: Header=BB0_18 Depth=2
	movl	%edx, 104(%rsp)         # 4-byte Spill
	movl	24(%rsp), %edx
	movl	28(%rsp), %ebx
	movl	%edx, %r10d
	shrl	$11, %r10d
	movl	(%rcx), %eax
	movzwl	%ax, %ebp
	imull	%ebp, %r10d
	movl	%ebx, %esi
	subl	%r10d, %esi
	jae	.LBB0_28
# BB#27:                                #   in Loop: Header=BB0_18 Depth=2
	movl	%ebx, 88(%rsp)          # 4-byte Spill
	movl	%r10d, 24(%rsp)
	movl	$2048, %ebx             # imm = 0x800
	subl	%ebp, %ebx
	shrl	$5, %ebx
	addl	%eax, %ebx
	xorl	%ebp, %ebp
	jmp	.LBB0_29
	.p2align	4, 0x90
.LBB0_28:                               #   in Loop: Header=BB0_18 Depth=2
	subl	%r10d, %edx
	movl	%edx, 24(%rsp)
	movl	%esi, 28(%rsp)
	shrl	$5, %ebp
	movl	%eax, %ebx
	subl	%ebp, %ebx
	movl	$1, %ebp
	movl	%esi, 88(%rsp)          # 4-byte Spill
	movl	%edx, %r10d
.LBB0_29:                               #   in Loop: Header=BB0_18 Depth=2
	andl	$-65536, %eax           # imm = 0xFFFF0000
	movzwl	%bx, %edx
	orl	%eax, %edx
	movl	%edx, (%rcx)
	cmpl	$16777215, %r10d        # imm = 0xFFFFFF
	movq	56(%rsp), %r14          # 8-byte Reload
	ja	.LBB0_30
# BB#31:                                #   in Loop: Header=BB0_18 Depth=2
	movq	%r9, 32(%rsp)
	movzbl	(%r9), %eax
	movl	88(%rsp), %ecx          # 4-byte Reload
	shll	$8, %ecx
	orl	%eax, %ecx
	movq	%r11, 32(%rsp)
	shll	$8, %r10d
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	movl	%ecx, 28(%rsp)
	movl	%r10d, 24(%rsp)
	movq	%r11, 16(%rsp)
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_18 Depth=2
	movq	%r9, %r11
.LBB0_32:                               # %lzma_486248.exit439
                                        #   in Loop: Header=BB0_18 Depth=2
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	%ebp, 12(%rsp)
	testl	%ebp, %ebp
	je	.LBB0_179
# BB#33:                                #   in Loop: Header=BB0_18 Depth=2
	leal	(%rcx,%rcx), %ecx
	leaq	384(%r15,%rcx), %rbx
	movq	%rbx, 32(%rsp)
	cmpq	%r14, %rbx
	jb	.LBB0_200
# BB#34:                                #   in Loop: Header=BB0_18 Depth=2
	leaq	4(%rbx), %rax
	cmpq	%r14, %rax
	jbe	.LBB0_200
# BB#35:                                #   in Loop: Header=BB0_18 Depth=2
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#36:                                #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %r11
	jb	.LBB0_200
# BB#37:                                #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%r11), %r12
	cmpq	48(%rsp), %r12          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#38:                                #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %r12
	jbe	.LBB0_200
# BB#39:                                #   in Loop: Header=BB0_18 Depth=2
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	%r10d, %ebp
	shrl	$11, %ebp
	movl	(%rbx), %esi
	movzwl	%si, %edx
	imull	%edx, %ebp
	movl	88(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %ecx
	subl	%ebp, %ecx
	jae	.LBB0_41
# BB#40:                                #   in Loop: Header=BB0_18 Depth=2
	movl	%ebp, 24(%rsp)
	movl	$2048, %eax             # imm = 0x800
	subl	%edx, %eax
	shrl	$5, %eax
	addl	%esi, %eax
	xorl	%r9d, %r9d
	jmp	.LBB0_42
	.p2align	4, 0x90
.LBB0_179:                              #   in Loop: Header=BB0_18 Depth=2
	movzbl	%r12b, %eax
	shll	$5, %eax
	andl	$7680, %eax             # imm = 0x1E00
	leal	(%rax,%rax,2), %eax
	movl	%eax, 12(%rsp)
	leaq	3692(%r15,%rax), %r9
	movq	%r9, 32(%rsp)
	xorl	%esi, %esi
	cmpl	$4, %ecx
	movl	72(%rsp), %eax          # 4-byte Reload
	jb	.LBB0_184
# BB#180:                               #   in Loop: Header=BB0_18 Depth=2
	cmpl	$10, %ecx
	jb	.LBB0_182
# BB#181:                               #   in Loop: Header=BB0_18 Depth=2
	addl	$-6, %ecx
	jmp	.LBB0_183
	.p2align	4, 0x90
.LBB0_41:                               #   in Loop: Header=BB0_18 Depth=2
	subl	%ebp, %r10d
	movl	%r10d, 24(%rsp)
	movl	%ecx, 28(%rsp)
	shrl	$5, %edx
	movl	%esi, %eax
	subl	%edx, %eax
	movl	$1, %r9d
	movl	%ecx, %r14d
	movl	%r10d, %ebp
.LBB0_42:                               #   in Loop: Header=BB0_18 Depth=2
	movl	76(%rsp), %ecx          # 4-byte Reload
	andl	$-65536, %esi           # imm = 0xFFFF0000
	movzwl	%ax, %eax
	orl	%esi, %eax
	movl	%eax, (%rbx)
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB0_43
# BB#44:                                #   in Loop: Header=BB0_18 Depth=2
	movq	%r11, 32(%rsp)
	movzbl	(%r11), %eax
	shll	$8, %r14d
	orl	%eax, %r14d
	movq	%r12, 32(%rsp)
	shll	$8, %ebp
	movl	%r14d, 28(%rsp)
	movl	%ebp, 24(%rsp)
	movq	%r12, 16(%rsp)
	jmp	.LBB0_45
	.p2align	4, 0x90
.LBB0_43:                               #   in Loop: Header=BB0_18 Depth=2
	movq	%r11, %r12
.LBB0_45:                               # %lzma_486248.exit491
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%r9d, 12(%rsp)
	testl	%r9d, %r9d
	je	.LBB0_46
# BB#105:                               #   in Loop: Header=BB0_18 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	addq	%r15, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	408(%rax), %rbx
	movq	%rbx, 32(%rsp)
	movq	56(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rbx
	jb	.LBB0_200
# BB#106:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	4(%rbx), %rax
	cmpq	%rcx, %rax
	jbe	.LBB0_200
# BB#107:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#108:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%rcx, %r12
	jb	.LBB0_200
# BB#109:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%r12), %r9
	cmpq	48(%rsp), %r9           # 8-byte Folded Reload
	ja	.LBB0_200
# BB#110:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%rcx, %r9
	jbe	.LBB0_200
# BB#111:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%ebp, %r10d
	shrl	$11, %r10d
	movl	(%rbx), %edx
	movzwl	%dx, %esi
	imull	%esi, %r10d
	movl	%r14d, %ecx
	subl	%r10d, %ecx
	jae	.LBB0_113
# BB#112:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%r10d, 24(%rsp)
	movl	$2048, %eax             # imm = 0x800
	subl	%esi, %eax
	shrl	$5, %eax
	addl	%edx, %eax
	xorl	%r11d, %r11d
	jmp	.LBB0_114
	.p2align	4, 0x90
.LBB0_46:                               #   in Loop: Header=BB0_18 Depth=2
	xorl	%eax, %eax
	cmpl	$6, 128(%rsp)           # 4-byte Folded Reload
	seta	%al
	leal	7(%rax,%rax,2), %eax
	movl	%eax, 128(%rsp)         # 4-byte Spill
	movl	%eax, 12(%rsp)
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	%rax, 32(%rsp)
	movl	%ecx, (%rsp)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	leaq	16(%rsp), %rsi
	leaq	32(%rsp), %rdx
	leaq	44(%rsp), %rcx
	leaq	12(%rsp), %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	callq	lzma_4863da
	movl	%eax, %ecx
	movl	$-1, %ebp
	cmpl	$-1, %ecx
	je	.LBB0_268
# BB#47:                                #   in Loop: Header=BB0_18 Depth=2
	movl	12(%rsp), %eax
	cmpl	$4, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	jb	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_18 Depth=2
	movl	$3, 12(%rsp)
	movl	$3, %eax
.LBB0_49:                               # %.preheader.i475.us.preheader
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	$6, 44(%rsp)
	shll	$7, %eax
	movl	%eax, 12(%rsp)
	leaq	864(%r15,%rax), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rsp)
	movl	$1, %ecx
	movl	$-6, %r10d
	movq	56(%rsp), %r14          # 8-byte Reload
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [67109888,67109888,67109888,67109888]
	.p2align	4, 0x90
.LBB0_50:                               # %.preheader.i475.us
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ecx, %edi
	addl	%edi, %edi
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi), %rbp
	movq	%rbp, 32(%rsp)
	cmpq	%r14, %rbp
	jb	.LBB0_267
# BB#51:                                #   in Loop: Header=BB0_50 Depth=3
	leaq	4(%rbp), %rax
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_267
# BB#52:                                #   in Loop: Header=BB0_50 Depth=3
	cmpq	%r14, %rax
	jbe	.LBB0_267
# BB#53:                                #   in Loop: Header=BB0_50 Depth=3
	movq	16(%rsp), %r12
	cmpq	%r14, %r12
	jb	.LBB0_267
# BB#54:                                #   in Loop: Header=BB0_50 Depth=3
	leaq	1(%r12), %r11
	cmpq	48(%rsp), %r11          # 8-byte Folded Reload
	ja	.LBB0_267
# BB#55:                                #   in Loop: Header=BB0_50 Depth=3
	cmpq	%r14, %r11
	jbe	.LBB0_267
# BB#56:                                #   in Loop: Header=BB0_50 Depth=3
	movl	24(%rsp), %r9d
	movl	28(%rsp), %r8d
	movl	%r9d, %esi
	shrl	$11, %esi
	movl	(%rbp), %ebx
	movzwl	%bx, %ecx
	imull	%ecx, %esi
	movl	%r8d, %edx
	subl	%esi, %edx
	jae	.LBB0_57
# BB#58:                                #   in Loop: Header=BB0_50 Depth=3
	movl	%esi, 24(%rsp)
	movl	$2048, %eax             # imm = 0x800
	subl	%ecx, %eax
	shrl	$5, %eax
	addl	%ebx, %eax
	xorl	%ecx, %ecx
	movl	%r8d, %edx
	jmp	.LBB0_59
	.p2align	4, 0x90
.LBB0_57:                               #   in Loop: Header=BB0_50 Depth=3
	subl	%esi, %r9d
	movl	%r9d, 24(%rsp)
	movl	%edx, 28(%rsp)
	shrl	$5, %ecx
	movl	%ebx, %eax
	subl	%ecx, %eax
	movl	$1, %ecx
	movl	%r9d, %esi
.LBB0_59:                               #   in Loop: Header=BB0_50 Depth=3
	andl	$-65536, %ebx           # imm = 0xFFFF0000
	movzwl	%ax, %eax
	orl	%ebx, %eax
	movl	%eax, (%rbp)
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB0_61
# BB#60:                                #   in Loop: Header=BB0_50 Depth=3
	movq	%r12, 32(%rsp)
	movzbl	(%r12), %eax
	shll	$8, %edx
	orl	%eax, %edx
	movq	%r11, 32(%rsp)
	shll	$8, %esi
	movl	%edx, 28(%rsp)
	movl	%esi, 24(%rsp)
	movq	%r11, 16(%rsp)
.LBB0_61:                               # %lzma_486248.exit484.us
                                        #   in Loop: Header=BB0_50 Depth=3
	orl	%edi, %ecx
	incl	%r10d
	movq	56(%rsp), %r14          # 8-byte Reload
	jne	.LBB0_50
# BB#62:                                # %.loopexit521
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	$64, 44(%rsp)
	addl	$-64, %ecx
	movl	%ecx, 12(%rsp)
	cmpl	$4, %ecx
	jae	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_18 Depth=2
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	movl	76(%rsp), %r11d         # 4-byte Reload
	movq	120(%rsp), %r10         # 8-byte Reload
	movl	72(%rsp), %edi          # 4-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB0_92
.LBB0_113:                              #   in Loop: Header=BB0_18 Depth=2
	subl	%r10d, %ebp
	movl	%ebp, 24(%rsp)
	movl	%ecx, 28(%rsp)
	shrl	$5, %esi
	movl	%edx, %eax
	subl	%esi, %eax
	movl	$1, %r11d
	movl	%ecx, %r14d
	movl	%ebp, %r10d
.LBB0_114:                              #   in Loop: Header=BB0_18 Depth=2
	andl	$-65536, %edx           # imm = 0xFFFF0000
	movzwl	%ax, %eax
	orl	%edx, %eax
	movl	%eax, (%rbx)
	cmpl	$16777215, %r10d        # imm = 0xFFFFFF
	movq	56(%rsp), %rdx          # 8-byte Reload
	ja	.LBB0_115
# BB#116:                               #   in Loop: Header=BB0_18 Depth=2
	movq	%r12, 32(%rsp)
	movzbl	(%r12), %eax
	shll	$8, %r14d
	orl	%eax, %r14d
	movq	%r9, 32(%rsp)
	shll	$8, %r10d
	movl	%r14d, 28(%rsp)
	movl	%r10d, 24(%rsp)
	movq	%r9, 16(%rsp)
	jmp	.LBB0_117
.LBB0_115:                              #   in Loop: Header=BB0_18 Depth=2
	movq	%r12, %r9
.LBB0_117:                              # %lzma_486248.exit467
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%r11d, 12(%rsp)
	testl	%r11d, %r11d
	movl	112(%rsp), %r12d        # 4-byte Reload
	je	.LBB0_147
# BB#118:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%r14d, %r8d
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	432(%rax), %rbp
	movq	%rbp, 32(%rsp)
	cmpq	%rdx, %rbp
	jb	.LBB0_200
# BB#119:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	4(%rbp), %rax
	cmpq	%rdx, %rax
	jbe	.LBB0_200
# BB#120:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#121:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%rdx, %r9
	jb	.LBB0_200
# BB#122:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%r9), %r14
	cmpq	48(%rsp), %r14          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#123:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%rdx, %r14
	jbe	.LBB0_200
# BB#124:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%r10d, %ecx
	shrl	$11, %ecx
	movl	(%rbp), %edx
	movzwl	%dx, %esi
	imull	%esi, %ecx
	movl	%r8d, %eax
	subl	%ecx, %eax
	jae	.LBB0_126
# BB#125:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%ecx, 24(%rsp)
	movl	$2048, %ebx             # imm = 0x800
	subl	%esi, %ebx
	shrl	$5, %ebx
	addl	%edx, %ebx
	xorl	%r11d, %r11d
	jmp	.LBB0_127
.LBB0_147:                              #   in Loop: Header=BB0_18 Depth=2
	addl	$240, %r8d
	orl	%edi, %r8d
	movl	%r8d, 12(%rsp)
	addl	%r8d, %r8d
	addq	%r15, %r8
	movq	%r8, 32(%rsp)
	cmpq	%rdx, %r8
	movq	144(%rsp), %r11         # 8-byte Reload
	jb	.LBB0_200
# BB#148:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	4(%r8), %rax
	cmpq	%rdx, %rax
	jbe	.LBB0_200
# BB#149:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#150:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%rdx, %r9
	jb	.LBB0_200
# BB#151:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%r9), %rcx
	cmpq	48(%rsp), %rcx          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#152:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%rdx, %rcx
	jbe	.LBB0_200
# BB#153:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%r10d, %ebp
	shrl	$11, %ebp
	movl	(%r8), %edx
	movzwl	%dx, %esi
	imull	%esi, %ebp
	movl	%r14d, %ebx
	subl	%ebp, %ebx
	jae	.LBB0_155
# BB#154:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%ebp, 24(%rsp)
	movl	$2048, %eax             # imm = 0x800
	subl	%esi, %eax
	shrl	$5, %eax
	addl	%edx, %eax
	xorl	%esi, %esi
	jmp	.LBB0_156
.LBB0_182:                              #   in Loop: Header=BB0_18 Depth=2
	addl	$-3, %ecx
.LBB0_183:                              #   in Loop: Header=BB0_18 Depth=2
	movl	%ecx, %esi
.LBB0_184:                              #   in Loop: Header=BB0_18 Depth=2
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	movl	%esi, 128(%rsp)         # 4-byte Spill
	je	.LBB0_185
# BB#201:                               #   in Loop: Header=BB0_18 Depth=2
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	subl	68(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 12(%rsp)
	leaq	(%r13,%rax), %rcx
	movl	$-1, %ebp
	cmpq	%r14, %rcx
	jb	.LBB0_268
# BB#202:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%rcx), %rdx
	cmpq	48(%rsp), %rdx          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#203:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %rdx
	jbe	.LBB0_268
# BB#204:                               #   in Loop: Header=BB0_18 Depth=2
	movzbl	(%rcx), %r12d
	andl	$-256, %eax
	orl	%r12d, %eax
	movl	%eax, 12(%rsp)
	movl	%r12d, %edi
	shrl	$7, %edi
	movl	%edi, %eax
	shll	$9, %eax
	leaq	514(%r9,%rax), %rcx
	movq	%rcx, 32(%rsp)
	cmpq	%r14, %rcx
	jb	.LBB0_268
# BB#205:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	4(%rcx), %rax
	cmpq	%r14, %rax
	jbe	.LBB0_268
# BB#206:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#207:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %r11
	jb	.LBB0_268
# BB#208:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%r11), %r8
	cmpq	48(%rsp), %r8           # 8-byte Folded Reload
	ja	.LBB0_268
# BB#209:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %r8
	jbe	.LBB0_268
# BB#210:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%r10d, %ebp
	shrl	$11, %ebp
	movl	(%rcx), %eax
	movzwl	%ax, %esi
	imull	%esi, %ebp
	movl	88(%rsp), %edx          # 4-byte Reload
	subl	%ebp, %edx
	jae	.LBB0_212
# BB#211:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%ebp, 24(%rsp)
	movl	$2048, %edx             # imm = 0x800
	subl	%esi, %edx
	shrl	$5, %edx
	addl	%eax, %edx
	xorl	%esi, %esi
	jmp	.LBB0_213
.LBB0_185:                              # %.split567.us.preheader
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	$2, 12(%rsp)
	leaq	2(%r9), %rcx
	movq	%rcx, 32(%rsp)
	cmpq	%r14, %rcx
	jb	.LBB0_200
# BB#186:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	$2, %eax
	movl	$2, %r12d
	.p2align	4, 0x90
.LBB0_187:                              # %.lr.ph
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	4(%r9,%r12), %rdx
	cmpq	%r14, %rdx
	jbe	.LBB0_200
# BB#188:                               # %.lr.ph
                                        #   in Loop: Header=BB0_187 Depth=3
	cmpq	48(%rsp), %rdx          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#189:                               # %.lr.ph
                                        #   in Loop: Header=BB0_187 Depth=3
	cmpq	%r14, %r11
	jb	.LBB0_200
# BB#190:                               #   in Loop: Header=BB0_187 Depth=3
	leaq	1(%r11), %rdi
	cmpq	48(%rsp), %rdi          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#191:                               #   in Loop: Header=BB0_187 Depth=3
	cmpq	%r14, %rdi
	jbe	.LBB0_200
# BB#192:                               #   in Loop: Header=BB0_187 Depth=3
	movl	%r10d, %esi
	shrl	$11, %esi
	movl	(%rcx), %ebp
	movzwl	%bp, %ebx
	imull	%ebx, %esi
	movl	28(%rsp), %edx
	subl	%esi, %edx
	jae	.LBB0_193
# BB#194:                               #   in Loop: Header=BB0_187 Depth=3
	movl	%esi, 24(%rsp)
	movl	$2048, %edx             # imm = 0x800
	subl	%ebx, %edx
	shrl	$5, %edx
	addl	%ebp, %edx
	xorl	%r12d, %r12d
	jmp	.LBB0_195
	.p2align	4, 0x90
.LBB0_193:                              #   in Loop: Header=BB0_187 Depth=3
	subl	%esi, %r10d
	movl	%r10d, 24(%rsp)
	movl	%edx, 28(%rsp)
	shrl	$5, %ebx
	movl	%ebp, %edx
	subl	%ebx, %edx
	movl	$1, %r12d
	movl	%r10d, %esi
.LBB0_195:                              #   in Loop: Header=BB0_187 Depth=3
	andl	$-65536, %ebp           # imm = 0xFFFF0000
	movzwl	%dx, %edx
	orl	%ebp, %edx
	movl	%edx, (%rcx)
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB0_197
# BB#196:                               #   in Loop: Header=BB0_187 Depth=3
	movq	%r11, 32(%rsp)
	movzbl	(%r11), %ecx
	movl	28(%rsp), %edx
	shll	$8, %edx
	orl	%ecx, %edx
	movq	%rdi, 32(%rsp)
	shll	$8, %esi
	movl	%edx, 28(%rsp)
	movl	%esi, 24(%rsp)
	movq	%rdi, 16(%rsp)
	movq	%rdi, %r11
.LBB0_197:                              #   in Loop: Header=BB0_187 Depth=3
	movl	%esi, %r10d
	orl	%eax, %r12d
	movl	%r12d, 12(%rsp)
	cmpl	$255, %r12d
	movq	56(%rsp), %r14          # 8-byte Reload
	ja	.LBB0_198
# BB#199:                               # %.split567.us
                                        #   in Loop: Header=BB0_187 Depth=3
	movq	%r9, 32(%rsp)
	addl	%r12d, %r12d
	movl	%r12d, 12(%rsp)
	leaq	(%r9,%r12), %rcx
	movq	%rcx, 32(%rsp)
	cmpq	%r14, %rcx
	movl	%r12d, %eax
	jae	.LBB0_187
	jmp	.LBB0_200
.LBB0_212:                              #   in Loop: Header=BB0_18 Depth=2
	subl	%ebp, %r10d
	movl	%r10d, 24(%rsp)
	movl	%edx, 28(%rsp)
	shrl	$5, %esi
	movl	%eax, %edx
	subl	%esi, %edx
	movl	$1, %esi
	movl	%r10d, %ebp
.LBB0_213:                              #   in Loop: Header=BB0_18 Depth=2
	movq	120(%rsp), %r10         # 8-byte Reload
	andl	$-65536, %eax           # imm = 0xFFFF0000
	movzwl	%dx, %edx
	orl	%eax, %edx
	movl	%edx, (%rcx)
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB0_214
# BB#215:                               #   in Loop: Header=BB0_18 Depth=2
	movq	%r11, 32(%rsp)
	movzbl	(%r11), %eax
	movl	28(%rsp), %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	movq	%r8, 32(%rsp)
	shll	$8, %ebp
	movl	%ecx, 28(%rsp)
	movl	%ebp, 24(%rsp)
	movq	%r8, 16(%rsp)
	jmp	.LBB0_216
.LBB0_214:                              #   in Loop: Header=BB0_18 Depth=2
	movq	%r11, %r8
.LBB0_216:                              # %lzma_486248.exit.i
                                        #   in Loop: Header=BB0_18 Depth=2
	leal	2(%rsi), %ecx
	cmpl	%esi, %edi
	jne	.LBB0_217
# BB#218:                               # %.lr.ph85.split.us.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=2
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%r8, %r14
	movq	56(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_219:                              # %.lr.ph85.split.us.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$255, %ecx
	ja	.LBB0_220
# BB#221:                               #   in Loop: Header=BB0_219 Depth=3
	addb	%r12b, %r12b
	movzbl	%r12b, %edi
	shrl	$7, %edi
	movl	%edi, %eax
	shll	$8, %eax
	addl	%ecx, %eax
	leal	512(%rax,%rax), %edx
	addq	%r9, %rdx
	movq	%rdx, 32(%rsp)
	cmpq	%rsi, %rdx
	jb	.LBB0_267
# BB#222:                               #   in Loop: Header=BB0_219 Depth=3
	leaq	4(%rdx), %rax
	cmpq	%rsi, %rax
	jbe	.LBB0_267
# BB#223:                               #   in Loop: Header=BB0_219 Depth=3
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_267
# BB#224:                               #   in Loop: Header=BB0_219 Depth=3
	cmpq	%rsi, %r14
	jb	.LBB0_267
# BB#225:                               #   in Loop: Header=BB0_219 Depth=3
	leaq	1(%r14), %r11
	cmpq	48(%rsp), %r11          # 8-byte Folded Reload
	ja	.LBB0_267
# BB#226:                               #   in Loop: Header=BB0_219 Depth=3
	cmpq	%rsi, %r11
	jbe	.LBB0_267
# BB#227:                               #   in Loop: Header=BB0_219 Depth=3
	movl	%ebp, %ebx
	shrl	$11, %ebx
	movl	(%rdx), %r10d
	movzwl	%r10w, %esi
	imull	%esi, %ebx
	movl	28(%rsp), %eax
	subl	%ebx, %eax
	jae	.LBB0_228
# BB#229:                               #   in Loop: Header=BB0_219 Depth=3
	movl	%ebx, 24(%rsp)
	movl	$2048, %eax             # imm = 0x800
	subl	%esi, %eax
	shrl	$5, %eax
	addl	%r10d, %eax
	xorl	%esi, %esi
	jmp	.LBB0_230
	.p2align	4, 0x90
.LBB0_228:                              #   in Loop: Header=BB0_219 Depth=3
	subl	%ebx, %ebp
	movl	%ebp, 24(%rsp)
	movl	%eax, 28(%rsp)
	shrl	$5, %esi
	movl	%r10d, %eax
	subl	%esi, %eax
	movl	$1, %esi
	movl	%ebp, %ebx
.LBB0_230:                              #   in Loop: Header=BB0_219 Depth=3
	andl	$-65536, %r10d          # imm = 0xFFFF0000
	movzwl	%ax, %eax
	orl	%r10d, %eax
	movl	%eax, (%rdx)
	cmpl	$16777215, %ebx         # imm = 0xFFFFFF
	ja	.LBB0_231
# BB#232:                               #   in Loop: Header=BB0_219 Depth=3
	movq	%r14, 32(%rsp)
	movq	88(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %eax
	movl	28(%rsp), %edx
	shll	$8, %edx
	orl	%eax, %edx
	movq	%r11, 32(%rsp)
	shll	$8, %ebx
	movl	%edx, 28(%rsp)
	movl	%ebx, 24(%rsp)
	movq	%r11, 16(%rsp)
	movq	%r11, 104(%rsp)         # 8-byte Spill
	movq	%r11, 88(%rsp)          # 8-byte Spill
	movq	%r11, %r14
	jmp	.LBB0_233
	.p2align	4, 0x90
.LBB0_231:                              #   in Loop: Header=BB0_219 Depth=3
	movq	%r8, %r11
.LBB0_233:                              # %lzma_486248.exit76.us.i
                                        #   in Loop: Header=BB0_219 Depth=3
	movq	120(%rsp), %r10         # 8-byte Reload
	leal	(%rsi,%rcx,2), %ecx
	cmpl	%esi, %edi
	movq	%r11, %r8
	movl	%ebx, %ebp
	movq	56(%rsp), %rsi          # 8-byte Reload
	je	.LBB0_219
	jmp	.LBB0_234
.LBB0_217:                              #   in Loop: Header=BB0_18 Depth=2
	movq	%r8, %r11
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movl	%ebp, %ebx
.LBB0_234:                              # %.preheader.i
                                        #   in Loop: Header=BB0_18 Depth=2
	cmpl	$255, %ecx
	movl	72(%rsp), %edi          # 4-byte Reload
	ja	.LBB0_235
# BB#236:                               # %.lr.ph.split.us.i.preheader
                                        #   in Loop: Header=BB0_18 Depth=2
	movq	104(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_237:                              # %.lr.ph.split.us.i
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addl	%ecx, %ecx
	leaq	(%r9,%rcx), %rbp
	movq	%rbp, 32(%rsp)
	movq	56(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, %rbp
	jb	.LBB0_267
# BB#238:                               #   in Loop: Header=BB0_237 Depth=3
	leaq	4(%rbp), %rax
	cmpq	%rdx, %rax
	jbe	.LBB0_267
# BB#239:                               #   in Loop: Header=BB0_237 Depth=3
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_267
# BB#240:                               #   in Loop: Header=BB0_237 Depth=3
	cmpq	%rdx, %r14
	jb	.LBB0_267
# BB#241:                               #   in Loop: Header=BB0_237 Depth=3
	leaq	1(%r14), %r8
	cmpq	48(%rsp), %r8           # 8-byte Folded Reload
	ja	.LBB0_267
# BB#242:                               #   in Loop: Header=BB0_237 Depth=3
	cmpq	%rdx, %r8
	jbe	.LBB0_267
# BB#243:                               #   in Loop: Header=BB0_237 Depth=3
	movl	%ebx, %edx
	shrl	$11, %edx
	movl	(%rbp), %edi
	movzwl	%di, %esi
	imull	%esi, %edx
	movl	28(%rsp), %eax
	subl	%edx, %eax
	jae	.LBB0_244
# BB#245:                               #   in Loop: Header=BB0_237 Depth=3
	movl	%edx, 24(%rsp)
	movl	$2048, %eax             # imm = 0x800
	subl	%esi, %eax
	shrl	$5, %eax
	addl	%edi, %eax
	xorl	%esi, %esi
	jmp	.LBB0_246
.LBB0_244:                              #   in Loop: Header=BB0_237 Depth=3
	subl	%edx, %ebx
	movl	%ebx, 24(%rsp)
	movl	%eax, 28(%rsp)
	shrl	$5, %esi
	movl	%edi, %eax
	subl	%esi, %eax
	movl	$1, %esi
	movl	%ebx, %edx
.LBB0_246:                              #   in Loop: Header=BB0_237 Depth=3
	andl	$-65536, %edi           # imm = 0xFFFF0000
	movzwl	%ax, %eax
	orl	%edi, %eax
	movl	%eax, (%rbp)
	cmpl	$16777215, %edx         # imm = 0xFFFFFF
	ja	.LBB0_247
# BB#248:                               #   in Loop: Header=BB0_237 Depth=3
	movq	%r14, 32(%rsp)
	movzbl	(%r11), %eax
	movl	28(%rsp), %edi
	shll	$8, %edi
	orl	%eax, %edi
	movq	%r8, 32(%rsp)
	shll	$8, %edx
	movl	%edi, 28(%rsp)
	movl	%edx, 24(%rsp)
	movq	%r8, 16(%rsp)
	movq	%r8, %r14
	jmp	.LBB0_249
.LBB0_247:                              #   in Loop: Header=BB0_237 Depth=3
	movq	%r11, %r8
.LBB0_249:                              # %lzma_486248.exit69.us.i
                                        #   in Loop: Header=BB0_237 Depth=3
	movl	72(%rsp), %edi          # 4-byte Reload
	orl	%ecx, %esi
	cmpl	$256, %esi              # imm = 0x100
	movq	%r8, %r11
	movl	%edx, %ebx
	movl	%esi, %ecx
	jb	.LBB0_237
	jmp	.LBB0_250
.LBB0_126:                              #   in Loop: Header=BB0_18 Depth=2
	subl	%ecx, %r10d
	movl	%r10d, 24(%rsp)
	movl	%eax, 28(%rsp)
	shrl	$5, %esi
	movl	%edx, %ebx
	subl	%esi, %ebx
	movl	$1, %r11d
	movl	%eax, %r8d
	movl	%r10d, %ecx
.LBB0_127:                              #   in Loop: Header=BB0_18 Depth=2
	andl	$-65536, %edx           # imm = 0xFFFF0000
	movzwl	%bx, %eax
	orl	%edx, %eax
	movl	%eax, (%rbp)
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	movq	56(%rsp), %rdx          # 8-byte Reload
	ja	.LBB0_128
# BB#129:                               #   in Loop: Header=BB0_18 Depth=2
	movq	%r9, 32(%rsp)
	movzbl	(%r9), %eax
	movl	%r8d, %esi
	shll	$8, %esi
	orl	%eax, %esi
	movq	%r14, 32(%rsp)
	shll	$8, %ecx
	movl	%esi, %r8d
	movl	%esi, 28(%rsp)
	movl	%ecx, 24(%rsp)
	movq	%r14, 16(%rsp)
	jmp	.LBB0_130
.LBB0_128:                              #   in Loop: Header=BB0_18 Depth=2
	movq	%r9, %r14
.LBB0_130:                              # %lzma_486248.exit460
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%r11d, 12(%rsp)
	testl	%r11d, %r11d
	movq	104(%rsp), %rbp         # 8-byte Reload
	je	.LBB0_131
# BB#132:                               #   in Loop: Header=BB0_18 Depth=2
	addq	$456, %rbp              # imm = 0x1C8
	movq	%rbp, 32(%rsp)
	cmpq	%rdx, %rbp
	movq	144(%rsp), %r11         # 8-byte Reload
	jb	.LBB0_200
# BB#133:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	4(%rbp), %rax
	cmpq	%rdx, %rax
	jbe	.LBB0_200
# BB#134:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#135:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%rdx, %r14
	jb	.LBB0_200
# BB#136:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%r14), %r10
	cmpq	48(%rsp), %r10          # 8-byte Folded Reload
	ja	.LBB0_200
# BB#137:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%rdx, %r10
	jbe	.LBB0_200
# BB#138:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%r12d, 112(%rsp)        # 4-byte Spill
	movl	%ecx, %esi
	shrl	$11, %esi
	movq	%rbp, %r12
	movl	(%rbp), %ebx
	movzwl	%bx, %edx
	imull	%edx, %esi
	movl	%r8d, %eax
	subl	%esi, %eax
	jae	.LBB0_140
# BB#139:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%esi, 24(%rsp)
	movl	$2048, %ebp             # imm = 0x800
	subl	%edx, %ebp
	shrl	$5, %ebp
	addl	%ebx, %ebp
	xorl	%r9d, %r9d
	jmp	.LBB0_141
.LBB0_235:                              #   in Loop: Header=BB0_18 Depth=2
	movl	%ecx, %esi
	jmp	.LBB0_250
.LBB0_131:                              #   in Loop: Header=BB0_18 Depth=2
	movl	84(%rsp), %ecx          # 4-byte Reload
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movq	144(%rsp), %r11         # 8-byte Reload
	jmp	.LBB0_144
.LBB0_155:                              #   in Loop: Header=BB0_18 Depth=2
	subl	%ebp, %r10d
	movl	%r10d, 24(%rsp)
	movl	%ebx, 28(%rsp)
	shrl	$5, %esi
	movl	%edx, %eax
	subl	%esi, %eax
	movl	$1, %esi
	movl	%ebx, %r14d
	movl	%r10d, %ebp
.LBB0_156:                              #   in Loop: Header=BB0_18 Depth=2
	andl	$-65536, %edx           # imm = 0xFFFF0000
	movzwl	%ax, %eax
	orl	%edx, %eax
	movl	%eax, (%r8)
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	ja	.LBB0_158
# BB#157:                               #   in Loop: Header=BB0_18 Depth=2
	movq	%r9, 32(%rsp)
	movzbl	(%r9), %eax
	shll	$8, %r14d
	orl	%eax, %r14d
	movq	%rcx, 32(%rsp)
	shll	$8, %ebp
	movl	%r14d, 28(%rsp)
	movl	%ebp, 24(%rsp)
	movq	%rcx, 16(%rsp)
.LBB0_158:                              # %lzma_486248.exit446
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%esi, 12(%rsp)
	testl	%esi, %esi
	je	.LBB0_160
# BB#159:                               #   in Loop: Header=BB0_18 Depth=2
	movl	84(%rsp), %ebx          # 4-byte Reload
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movl	68(%rsp), %eax          # 4-byte Reload
	movl	%eax, 88(%rsp)          # 4-byte Spill
	jmp	.LBB0_145
.LBB0_140:                              #   in Loop: Header=BB0_18 Depth=2
	subl	%esi, %ecx
	movl	%ecx, 24(%rsp)
	movl	%eax, 28(%rsp)
	shrl	$5, %edx
	movl	%ebx, %ebp
	subl	%edx, %ebp
	movl	$1, %r9d
	movl	%eax, %r8d
	movl	%ecx, %esi
.LBB0_141:                              #   in Loop: Header=BB0_18 Depth=2
	andl	$-65536, %ebx           # imm = 0xFFFF0000
	movzwl	%bp, %eax
	orl	%ebx, %eax
	movl	%eax, (%r12)
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	ja	.LBB0_143
# BB#142:                               #   in Loop: Header=BB0_18 Depth=2
	movq	%r14, 32(%rsp)
	movzbl	(%r14), %eax
	shll	$8, %r8d
	orl	%eax, %r8d
	movq	%r10, 32(%rsp)
	shll	$8, %esi
	movl	%r8d, 28(%rsp)
	movl	%esi, 24(%rsp)
	movq	%r10, 16(%rsp)
.LBB0_143:                              # %lzma_486248.exit453
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%r9d, 12(%rsp)
	testl	%r9d, %r9d
	movl	112(%rsp), %r12d        # 4-byte Reload
	movl	%r12d, %ecx
	movl	80(%rsp), %eax          # 4-byte Reload
	cmovel	%eax, %ecx
	cmovnel	%eax, %r12d
.LBB0_144:                              #   in Loop: Header=BB0_18 Depth=2
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	movl	%ecx, 12(%rsp)
	movl	68(%rsp), %ebx          # 4-byte Reload
.LBB0_145:                              #   in Loop: Header=BB0_18 Depth=2
	movq	%r11, 32(%rsp)
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rsp)
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	leaq	16(%rsp), %rsi
	leaq	32(%rsp), %rdx
	leaq	44(%rsp), %rcx
	leaq	12(%rsp), %r8
	movq	56(%rsp), %r9           # 8-byte Reload
	callq	lzma_4863da
	cmpl	$-1, %eax
	je	.LBB0_267
# BB#146:                               #   in Loop: Header=BB0_18 Depth=2
	movl	12(%rsp), %ecx
	xorl	%eax, %eax
	cmpl	$6, 128(%rsp)           # 4-byte Folded Reload
	seta	%al
	leal	8(%rax,%rax,2), %eax
	movl	%eax, 128(%rsp)         # 4-byte Spill
	movl	%eax, 12(%rsp)
	movl	%r12d, 80(%rsp)         # 4-byte Spill
	movl	%ebx, 68(%rsp)          # 4-byte Spill
	movq	56(%rsp), %r14          # 8-byte Reload
	movl	76(%rsp), %r11d         # 4-byte Reload
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [67109888,67109888,67109888,67109888]
	movq	120(%rsp), %r10         # 8-byte Reload
	movl	72(%rsp), %edi          # 4-byte Reload
.LBB0_93:                               #   in Loop: Header=BB0_18 Depth=2
	movl	88(%rsp), %edx          # 4-byte Reload
	testl	%edx, %edx
	je	.LBB0_257
# BB#94:                                #   in Loop: Header=BB0_18 Depth=2
	movq	%rcx, %rbx
	leal	2(%rcx), %eax
	movq	%r13, 32(%rsp)
	movl	%edi, 44(%rsp)
	movl	%edi, %ecx
	subl	%edx, %ecx
	movl	%ecx, 12(%rsp)
	movl	%r10d, %edx
	subl	%edi, %edx
	cmpl	%edx, %eax
	jae	.LBB0_167
# BB#95:                                #   in Loop: Header=BB0_18 Depth=2
	testl	%eax, %eax
	sete	%dl
	cmpl	%r11d, %eax
	movl	$-1, %ebp
	ja	.LBB0_268
# BB#96:                                #   in Loop: Header=BB0_18 Depth=2
	testb	%dl, %dl
	jne	.LBB0_268
# BB#97:                                #   in Loop: Header=BB0_18 Depth=2
	movl	%ecx, %esi
	addq	%r13, %rsi
	cmpq	%r14, %rsi
	jb	.LBB0_268
# BB#98:                                #   in Loop: Header=BB0_18 Depth=2
	movl	%eax, %edx
	addq	%rdx, %rsi
	cmpq	48(%rsp), %rsi          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#99:                                #   in Loop: Header=BB0_18 Depth=2
	testl	%eax, %eax
	je	.LBB0_268
# BB#100:                               #   in Loop: Header=BB0_18 Depth=2
	movq	56(%rsp), %r14          # 8-byte Reload
	cmpq	%r14, %rsi
	jbe	.LBB0_268
# BB#101:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%edi, %eax
	addq	%r13, %rax
	cmpq	%r14, %rax
	jb	.LBB0_268
# BB#102:                               #   in Loop: Header=BB0_18 Depth=2
	addq	%rdx, %rax
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#103:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %rax
	jbe	.LBB0_268
# BB#104:                               #   in Loop: Header=BB0_18 Depth=2
	movl	140(%rsp), %eax         # 4-byte Reload
	subl	%edi, %eax
	cmpl	%r11d, %eax
	jb	.LBB0_168
	jmp	.LBB0_268
.LBB0_167:                              #   in Loop: Header=BB0_18 Depth=2
	movl	140(%rsp), %eax         # 4-byte Reload
	subl	%edi, %eax
	cmpl	%r11d, %eax
	jae	.LBB0_267
.LBB0_168:                              #   in Loop: Header=BB0_18 Depth=2
	movl	%ecx, %eax
	leaq	(%r13,%rax), %rdx
	movl	$-1, %ebp
	cmpq	%r14, %rdx
	jb	.LBB0_268
# BB#169:                               #   in Loop: Header=BB0_18 Depth=2
	addq	%r10, %rdx
	movl	%edi, %ecx
	subq	%rcx, %rdx
	cmpq	48(%rsp), %rdx          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#170:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %rdx
	jbe	.LBB0_268
# BB#171:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	(%r13,%rcx), %rdx
	cmpq	%r14, %rdx
	jb	.LBB0_268
# BB#172:                               #   in Loop: Header=BB0_18 Depth=2
	movq	%rcx, %rsi
	negq	%rsi
	addq	%r10, %rdx
	addq	%rsi, %rdx
	cmpq	48(%rsp), %rdx          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#173:                               #   in Loop: Header=BB0_18 Depth=2
	movq	56(%rsp), %r14          # 8-byte Reload
	cmpq	%r14, %rdx
	jbe	.LBB0_268
# BB#174:                               # %.preheader514.preheader
                                        #   in Loop: Header=BB0_18 Depth=2
	movb	(%r13,%rax), %r12b
	movb	%r12b, (%r13,%rcx)
	movl	44(%rsp), %edi
	incl	%edi
	movl	%edi, 44(%rsp)
	movl	12(%rsp), %eax
	incl	%eax
	movl	%eax, 12(%rsp)
	movl	$1, %edx
	cmpl	$-1, %ebx
	jne	.LBB0_176
	jmp	.LBB0_175
	.p2align	4, 0x90
.LBB0_178:                              # %.preheader514..preheader514_crit_edge
                                        #   in Loop: Header=BB0_176 Depth=3
	decl	%ebx
.LBB0_176:                              # %.preheader514.preheader
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r10d, %edi
	jae	.LBB0_175
# BB#177:                               # %.preheader514..preheader514_crit_edge
                                        #   in Loop: Header=BB0_176 Depth=3
	movq	32(%rsp), %rcx
	movl	%eax, %eax
	movb	(%rcx,%rax), %r12b
	movl	%edi, %eax
	movb	%r12b, (%rcx,%rax)
	movl	44(%rsp), %edi
	incl	%edi
	movl	%edi, 44(%rsp)
	movl	12(%rsp), %eax
	incl	%eax
	movl	%eax, 12(%rsp)
	testl	%ebx, %ebx
	jne	.LBB0_178
.LBB0_175:                              #   in Loop: Header=BB0_18 Depth=2
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, 112(%rsp)         # 4-byte Spill
	movl	68(%rsp), %eax          # 4-byte Reload
	movl	84(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	movl	88(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 68(%rsp)          # 4-byte Spill
	jmp	.LBB0_256
.LBB0_198:                              #   in Loop: Header=BB0_18 Depth=2
	movl	76(%rsp), %r11d         # 4-byte Reload
	movq	120(%rsp), %r10         # 8-byte Reload
	movl	72(%rsp), %edi          # 4-byte Reload
	jmp	.LBB0_251
.LBB0_160:                              #   in Loop: Header=BB0_18 Depth=2
	xorl	%eax, %eax
	cmpl	$6, 128(%rsp)           # 4-byte Folded Reload
	seta	%cl
	movl	72(%rsp), %edx          # 4-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	subl	68(%rsp), %edx          # 4-byte Folded Reload
	addq	%r13, %rdx
	movl	$-1, %ebp
	cmpq	56(%rsp), %rdx          # 8-byte Folded Reload
	jb	.LBB0_268
# BB#161:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%rdx), %rsi
	cmpq	48(%rsp), %rsi          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#162:                               #   in Loop: Header=BB0_18 Depth=2
	movq	56(%rsp), %rdi          # 8-byte Reload
	cmpq	%rdi, %rsi
	movq	%rdi, %rsi
	jbe	.LBB0_268
# BB#163:                               #   in Loop: Header=BB0_18 Depth=2
	movb	(%rdx), %r12b
	movl	72(%rsp), %edx          # 4-byte Reload
	movl	%edx, 44(%rsp)
	movl	%edx, %edx
	addq	%r13, %rdx
	cmpq	%rsi, %rdx
	jb	.LBB0_268
# BB#164:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%rdx), %rsi
	cmpq	48(%rsp), %rsi          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#165:                               #   in Loop: Header=BB0_18 Depth=2
	movq	56(%rsp), %r14          # 8-byte Reload
	cmpq	%r14, %rsi
	movl	72(%rsp), %edi          # 4-byte Reload
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [67109888,67109888,67109888,67109888]
	movq	120(%rsp), %r10         # 8-byte Reload
	movl	76(%rsp), %r11d         # 4-byte Reload
	jbe	.LBB0_268
# BB#166:                               #   in Loop: Header=BB0_18 Depth=2
	movb	%cl, %al
	leal	9(%rax,%rax), %eax
	movl	%eax, 128(%rsp)         # 4-byte Spill
	incl	%edi
	movb	%r12b, (%rdx)
	movl	$1, %edx
	jmp	.LBB0_255
.LBB0_64:                               #   in Loop: Header=BB0_18 Depth=2
	movl	%ecx, %eax
	sarl	%eax
	leal	-1(%rax), %ebx
	movl	%ecx, %r12d
	andl	$1, %r12d
	orl	$2, %r12d
	movl	%ecx, %edx
	movl	%ebx, %ecx
	shll	%cl, %r12d
	cmpl	$14, %edx
	movl	76(%rsp), %r11d         # 4-byte Reload
	movq	120(%rsp), %r10         # 8-byte Reload
	movl	72(%rsp), %edi          # 4-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	jb	.LBB0_75
# BB#65:                                #   in Loop: Header=BB0_18 Depth=2
	movl	%eax, %ecx
	addl	$-5, %ecx
	movl	%ecx, 44(%rsp)
	movl	24(%rsp), %edx
	movl	28(%rsp), %r9d
	jle	.LBB0_66
# BB#67:                                # %.preheader.split.i473.preheader
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	$5, %ebp
	subl	%eax, %ebp
	xorl	%ecx, %ecx
	movl	%edx, %esi
	xorl	%r10d, %r10d
.LBB0_68:                               # %.preheader.split.i473
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%esi, %edx
	shrl	%edx
	xorl	%eax, %eax
	cmpl	%edx, %r9d
	movl	%edx, %edi
	cmovbl	%r10d, %edi
	setae	%r8b
	subl	%edi, %r9d
	cmpl	$33554431, %esi         # imm = 0x1FFFFFF
	ja	.LBB0_73
# BB#69:                                #   in Loop: Header=BB0_68 Depth=3
	movq	16(%rsp), %rsi
	cmpq	56(%rsp), %rsi          # 8-byte Folded Reload
	jb	.LBB0_267
# BB#70:                                #   in Loop: Header=BB0_68 Depth=3
	leaq	1(%rsi), %rbx
	cmpq	48(%rsp), %rbx          # 8-byte Folded Reload
	ja	.LBB0_267
# BB#71:                                #   in Loop: Header=BB0_68 Depth=3
	cmpq	56(%rsp), %rbx          # 8-byte Folded Reload
	jbe	.LBB0_267
# BB#72:                                #   in Loop: Header=BB0_68 Depth=3
	shll	$8, %r9d
	shll	$8, %edx
	movzbl	(%rsi), %esi
	orl	%esi, %r9d
	movq	%rbx, 16(%rsp)
.LBB0_73:                               #   in Loop: Header=BB0_68 Depth=3
	movb	%r8b, %al
	leal	(%rax,%rcx,2), %ecx
	incl	%ebp
	movl	%edx, %esi
	jne	.LBB0_68
	jmp	.LBB0_74
.LBB0_220:                              #   in Loop: Header=BB0_18 Depth=2
	movl	%ecx, %esi
	movl	72(%rsp), %edi          # 4-byte Reload
.LBB0_250:                              # %.us-lcssa.us.i
                                        #   in Loop: Header=BB0_18 Depth=2
	movzbl	%sil, %r12d
	movl	%r12d, 12(%rsp)
	movq	56(%rsp), %r14          # 8-byte Reload
	movl	76(%rsp), %r11d         # 4-byte Reload
.LBB0_251:                              # %.loopexit631
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%edi, 44(%rsp)
	movl	%edi, %eax
	addq	%r13, %rax
	movl	$-1, %ebp
	cmpq	%r14, %rax
	jb	.LBB0_268
# BB#252:                               #   in Loop: Header=BB0_18 Depth=2
	leaq	1(%rax), %rcx
	cmpq	48(%rsp), %rcx          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#253:                               #   in Loop: Header=BB0_18 Depth=2
	cmpq	%r14, %rcx
	jbe	.LBB0_268
# BB#254:                               #   in Loop: Header=BB0_18 Depth=2
	incl	%edi
	movb	%r12b, (%rax)
	xorl	%edx, %edx
.LBB0_255:                              # %.loopexit
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	84(%rsp), %eax          # 4-byte Reload
.LBB0_256:                              # %.loopexit
                                        #   in Loop: Header=BB0_18 Depth=2
	movl	%edi, 12(%rsp)
	cmpl	%r10d, %edi
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movl	128(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r8d
	jb	.LBB0_18
.LBB0_257:                              #   in Loop: Header=BB0_4 Depth=1
	movl	180(%rsp), %r8d         # 4-byte Reload
	testl	%r8d, %r8d
	movq	200(%rsp), %rsi         # 8-byte Reload
	movq	%rsi, %r12
	movl	176(%rsp), %ecx         # 4-byte Reload
	movb	103(%rsp), %r9b         # 1-byte Reload
	movb	102(%rsp), %bl          # 1-byte Reload
	movq	144(%rsp), %r10         # 8-byte Reload
	movq	192(%rsp), %r11         # 8-byte Reload
	movq	160(%rsp), %r13         # 8-byte Reload
	je	.LBB0_4
# BB#258:
	movl	44(%rsp), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	152(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %edx
	callq	cli_dbgmsg
	leal	-1(%rbx), %eax
	cmpl	76(%rsp), %eax          # 4-byte Folded Reload
	movl	$-1, %ebp
	jae	.LBB0_268
# BB#259:
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	184(%rsp), %rdx         # 8-byte Reload
	cmpq	%rcx, %rdx
	jb	.LBB0_268
# BB#260:
	movl	%ebx, %eax
	addq	%rdx, %rax
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_268
# BB#261:
	cmpq	%rcx, %rax
	jbe	.LBB0_268
# BB#262:                               # %.preheader.preheader
	xorl	%eax, %eax
	movq	184(%rsp), %rsi         # 8-byte Reload
	xorl	%ebp, %ebp
.LBB0_263:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %eax
	movzbl	(%rsi,%rax), %edx
	andb	$-2, %dl
	movl	$1, %ecx
	cmpb	$-24, %dl
	jne	.LBB0_265
# BB#264:                               #   in Loop: Header=BB0_263 Depth=1
	movl	1(%rsi,%rax), %ecx
	bswapl	%ecx
	movl	%eax, %edx
	notl	%edx
	addl	%ecx, %edx
	movl	%edx, 1(%rsi,%rax)
	movl	$5, %ecx
.LBB0_265:                              #   in Loop: Header=BB0_263 Depth=1
	addl	%ecx, %eax
	cmpl	152(%rsp), %eax         # 4-byte Folded Reload
	jne	.LBB0_263
	jmp	.LBB0_268
.LBB0_200:                              # %lzma_486248.exit439.thread
	movl	$-1, 12(%rsp)
.LBB0_267:                              # %lzma_4862e0.exit.thread
	movl	$-1, %ebp
.LBB0_268:                              # %lzma_4862e0.exit.thread
	movl	%ebp, %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_12:
	xorl	%ebp, %ebp
	jmp	.LBB0_268
.LBB0_266:                              # %lzma_4862e0.exit.thread.loopexit635
	movl	%r8d, 44(%rsp)
	jmp	.LBB0_267
.Lfunc_end0:
	.size	mew_lzma, .Lfunc_end0-mew_lzma
	.cfi_endproc

	.p2align	4, 0x90
	.type	lzma_4863da,@function
lzma_4863da:                            # @lzma_4863da
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movl	56(%rsp), %ebp
	movl	$-1, %eax
	cmpl	$4, %ebp
	jb	.LBB1_69
# BB#1:
	movq	(%rdx), %r11
	cmpq	%r9, %r11
	jae	.LBB1_2
.LBB1_69:                               # %lzma_486248.exit.thread
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_2:
	leaq	4(%r11), %r15
	movl	%ebp, %ebx
	addq	%r9, %rbx
	cmpq	%rbx, %r15
	ja	.LBB1_69
# BB#3:
	testl	%ebp, %ebp
	je	.LBB1_69
# BB#4:
	cmpq	%r9, %r15
	jbe	.LBB1_69
# BB#5:
	movq	(%rsi), %r14
	cmpq	%r9, %r14
	jb	.LBB1_69
# BB#6:
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	%r8, -8(%rsp)           # 8-byte Spill
	leaq	1(%r14), %r8
	cmpq	%rbx, %r8
	ja	.LBB1_69
# BB#7:
	cmpq	%r9, %r8
	jbe	.LBB1_69
# BB#8:
	movq	%rbx, -24(%rsp)         # 8-byte Spill
	movl	8(%rsi), %ecx
	movl	12(%rsi), %r13d
	movl	%ecx, %ebp
	shrl	$11, %ebp
	movl	(%r11), %r12d
	movzwl	%r12w, %r10d
	imull	%r10d, %ebp
	subl	%ebp, %r13d
	jae	.LBB1_10
# BB#9:
	movl	%ebp, 8(%rsi)
	movl	$2048, %ebp             # imm = 0x800
	subl	%r10d, %ebp
	shrl	$5, %ebp
	addl	%r12d, %ebp
	xorl	%r13d, %r13d
	jmp	.LBB1_11
.LBB1_10:
	subl	%ebp, %ecx
	movl	%ecx, 8(%rsi)
	movl	%r13d, 12(%rsi)
	shrl	$5, %r10d
	movl	%r12d, %ebp
	subl	%r10d, %ebp
	movl	$1, %r13d
.LBB1_11:
	andl	$-65536, %r12d          # imm = 0xFFFF0000
	movzwl	%bp, %ecx
	orl	%r12d, %ecx
	movl	%ecx, (%r11)
	movl	8(%rsi), %r12d
	cmpl	$16777215, %r12d        # imm = 0xFFFFFF
	movq	-24(%rsp), %rbx         # 8-byte Reload
	ja	.LBB1_13
# BB#12:
	movq	%r14, (%rdx)
	movq	(%rsi), %rcx
	movzbl	(%rcx), %ecx
	movl	12(%rsi), %ebp
	shll	$8, %ebp
	orl	%ecx, %ebp
	movq	%r8, (%rdx)
	shll	$8, %r12d
	movl	%ebp, 12(%rsi)
	movl	%r12d, 8(%rsi)
	movq	%r8, (%rsi)
.LBB1_13:                               # %lzma_486248.exit
	movq	-32(%rsp), %rcx         # 8-byte Reload
	testl	%r13d, %r13d
	je	.LBB1_53
# BB#14:
	leaq	2(%r11), %rbp
	movq	%rbp, (%rdx)
	cmpq	%r9, %rbp
	jb	.LBB1_69
# BB#15:
	leaq	6(%r11), %rcx
	cmpq	%rbx, %rcx
	ja	.LBB1_69
# BB#16:
	cmpq	%r9, %rcx
	jbe	.LBB1_69
# BB#17:
	movq	(%rsi), %r13
	cmpq	%r9, %r13
	jb	.LBB1_69
# BB#18:
	leaq	1(%r13), %r14
	cmpq	%rbx, %r14
	ja	.LBB1_69
# BB#19:
	cmpq	%r9, %r14
	jbe	.LBB1_69
# BB#20:
	movl	%r12d, %ebx
	shrl	$11, %ebx
	movl	(%rbp), %r15d
	movzwl	%r15w, %r10d
	imull	%r10d, %ebx
	movl	12(%rsi), %ecx
	subl	%ebx, %ecx
	jae	.LBB1_22
# BB#21:
	movl	%ebx, 8(%rsi)
	movl	$2048, %ebx             # imm = 0x800
	subl	%r10d, %ebx
	shrl	$5, %ebx
	addl	%r15d, %ebx
	xorl	%r12d, %r12d
	jmp	.LBB1_23
.LBB1_53:                               # %.preheader.i.us.preheader
	shll	$4, %edi
	movl	$3, (%rcx)
	addq	%rdi, %r15
	movq	%r15, (%rdx)
	movl	$1, %r14d
	movl	$-3, %r11d
.LBB1_54:                               # %.preheader.i.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%r14d, %ebp
	addl	%ebp, %ebp
	leaq	(%r15,%rbp), %rdi
	movq	%rdi, (%rdx)
	cmpq	%r9, %rdi
	jb	.LBB1_69
# BB#55:                                #   in Loop: Header=BB1_54 Depth=1
	leaq	4(%rdi), %rcx
	cmpq	%rbx, %rcx
	ja	.LBB1_69
# BB#56:                                #   in Loop: Header=BB1_54 Depth=1
	cmpq	%r9, %rcx
	jbe	.LBB1_69
# BB#57:                                #   in Loop: Header=BB1_54 Depth=1
	movq	(%rsi), %r13
	cmpq	%r9, %r13
	jb	.LBB1_69
# BB#58:                                #   in Loop: Header=BB1_54 Depth=1
	leaq	1(%r13), %r12
	cmpq	%rbx, %r12
	ja	.LBB1_69
# BB#59:                                #   in Loop: Header=BB1_54 Depth=1
	cmpq	%r9, %r12
	jbe	.LBB1_69
# BB#60:                                #   in Loop: Header=BB1_54 Depth=1
	movl	8(%rsi), %r8d
	movl	12(%rsi), %ebx
	movl	%r8d, %ecx
	shrl	$11, %ecx
	movl	(%rdi), %r10d
	movzwl	%r10w, %r14d
	imull	%r14d, %ecx
	subl	%ecx, %ebx
	jae	.LBB1_61
# BB#62:                                #   in Loop: Header=BB1_54 Depth=1
	movl	%ecx, 8(%rsi)
	movl	$2048, %ebx             # imm = 0x800
	subl	%r14d, %ebx
	shrl	$5, %ebx
	addl	%r10d, %ebx
	xorl	%r14d, %r14d
	jmp	.LBB1_63
.LBB1_61:                               #   in Loop: Header=BB1_54 Depth=1
	subl	%ecx, %r8d
	movl	%r8d, 8(%rsi)
	movl	%ebx, 12(%rsi)
	shrl	$5, %r14d
	movl	%r10d, %ebx
	subl	%r14d, %ebx
	movl	$1, %r14d
.LBB1_63:                               #   in Loop: Header=BB1_54 Depth=1
	andl	$-65536, %r10d          # imm = 0xFFFF0000
	movzwl	%bx, %ecx
	orl	%r10d, %ecx
	movl	%ecx, (%rdi)
	movl	8(%rsi), %edi
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	ja	.LBB1_65
# BB#64:                                #   in Loop: Header=BB1_54 Depth=1
	movq	%r13, (%rdx)
	movq	(%rsi), %rcx
	movzbl	(%rcx), %ecx
	movl	12(%rsi), %ebx
	shll	$8, %ebx
	orl	%ecx, %ebx
	movq	%r12, (%rdx)
	shll	$8, %edi
	movl	%ebx, 12(%rsi)
	movl	%edi, 8(%rsi)
	movq	%r12, (%rsi)
.LBB1_65:                               # %lzma_486248.exit46.us
                                        #   in Loop: Header=BB1_54 Depth=1
	orl	%ebp, %r14d
	incl	%r11d
	movq	-24(%rsp), %rbx         # 8-byte Reload
	jne	.LBB1_54
# BB#66:                                # %lzma_4862e0.exit
	addl	$-8, %r14d
.LBB1_67:                               # %.loopexit
	movl	$8, %eax
.LBB1_68:                               # %.loopexit
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	movq	-8(%rsp), %rax          # 8-byte Reload
	movl	%r14d, (%rax)
	xorl	%eax, %eax
	jmp	.LBB1_69
.LBB1_22:
	subl	%ebx, %r12d
	movl	%r12d, 8(%rsi)
	movl	%ecx, 12(%rsi)
	shrl	$5, %r10d
	movl	%r15d, %ebx
	subl	%r10d, %ebx
	movl	$1, %r12d
.LBB1_23:
	andl	$-65536, %r15d          # imm = 0xFFFF0000
	movzwl	%bx, %ecx
	orl	%r15d, %ecx
	movl	%ecx, (%rbp)
	movl	8(%rsi), %ebp
	cmpl	$16777215, %ebp         # imm = 0xFFFFFF
	movq	-24(%rsp), %r10         # 8-byte Reload
	ja	.LBB1_25
# BB#24:
	movq	%r13, (%rdx)
	movq	(%rsi), %rcx
	movzbl	(%rcx), %ecx
	movl	12(%rsi), %ebx
	shll	$8, %ebx
	orl	%ecx, %ebx
	movq	%r14, (%rdx)
	shll	$8, %ebp
	movl	%ebx, 12(%rsi)
	movl	%ebp, 8(%rsi)
	movq	%r14, (%rsi)
.LBB1_25:                               # %lzma_486248.exit81
	testl	%r12d, %r12d
	je	.LBB1_40
# BB#26:                                # %.preheader.i63.us.preheader
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movl	$8, (%rcx)
	addq	$516, %r11              # imm = 0x204
	movq	%r11, (%rdx)
	movl	$1, %r14d
	movl	$-8, %r8d
.LBB1_27:                               # %.preheader.i63.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%r14d, %edi
	addl	%edi, %edi
	leaq	(%r11,%rdi), %rbx
	movq	%rbx, (%rdx)
	cmpq	%r9, %rbx
	jb	.LBB1_69
# BB#28:                                #   in Loop: Header=BB1_27 Depth=1
	leaq	4(%rbx), %rcx
	cmpq	%r10, %rcx
	ja	.LBB1_69
# BB#29:                                #   in Loop: Header=BB1_27 Depth=1
	cmpq	%r9, %rcx
	jbe	.LBB1_69
# BB#30:                                #   in Loop: Header=BB1_27 Depth=1
	movq	(%rsi), %r13
	cmpq	%r9, %r13
	jb	.LBB1_69
# BB#31:                                #   in Loop: Header=BB1_27 Depth=1
	leaq	1(%r13), %r12
	cmpq	%r10, %r12
	ja	.LBB1_69
# BB#32:                                #   in Loop: Header=BB1_27 Depth=1
	cmpq	%r9, %r12
	jbe	.LBB1_69
# BB#33:                                #   in Loop: Header=BB1_27 Depth=1
	movl	8(%rsi), %ebp
	movl	12(%rsi), %r14d
	movl	%ebp, %ecx
	shrl	$11, %ecx
	movl	(%rbx), %r15d
	movzwl	%r15w, %r10d
	imull	%r10d, %ecx
	subl	%ecx, %r14d
	jae	.LBB1_34
# BB#35:                                #   in Loop: Header=BB1_27 Depth=1
	movl	%ecx, 8(%rsi)
	movl	$2048, %ebp             # imm = 0x800
	subl	%r10d, %ebp
	shrl	$5, %ebp
	addl	%r15d, %ebp
	xorl	%r14d, %r14d
	jmp	.LBB1_36
.LBB1_34:                               #   in Loop: Header=BB1_27 Depth=1
	subl	%ecx, %ebp
	movl	%ebp, 8(%rsi)
	movl	%r14d, 12(%rsi)
	shrl	$5, %r10d
	movl	%r15d, %ebp
	subl	%r10d, %ebp
	movl	$1, %r14d
.LBB1_36:                               #   in Loop: Header=BB1_27 Depth=1
	andl	$-65536, %r15d          # imm = 0xFFFF0000
	movzwl	%bp, %ecx
	orl	%r15d, %ecx
	movl	%ecx, (%rbx)
	movl	8(%rsi), %r15d
	cmpl	$16777215, %r15d        # imm = 0xFFFFFF
	movq	-24(%rsp), %r10         # 8-byte Reload
	ja	.LBB1_38
# BB#37:                                #   in Loop: Header=BB1_27 Depth=1
	movq	%r13, (%rdx)
	movq	(%rsi), %rcx
	movzbl	(%rcx), %ecx
	movl	12(%rsi), %ebx
	shll	$8, %ebx
	orl	%ecx, %ebx
	movq	%r12, (%rdx)
	shll	$8, %r15d
	movl	%ebx, 12(%rsi)
	movl	%r15d, 8(%rsi)
	movq	%r12, (%rsi)
.LBB1_38:                               # %lzma_486248.exit74.us
                                        #   in Loop: Header=BB1_27 Depth=1
	orl	%edi, %r14d
	incl	%r8d
	jne	.LBB1_27
# BB#39:                                # %.loopexit90
	addl	$-240, %r14d
	movl	$256, %eax              # imm = 0x100
	jmp	.LBB1_68
.LBB1_40:                               # %.preheader.i49.us.preheader
	shll	$4, %edi
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movl	$3, (%rcx)
	leaq	260(%r11,%rdi), %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movq	%rcx, (%rdx)
	movl	$1, %r14d
	movl	$-3, %r15d
.LBB1_41:                               # %.preheader.i49.us
                                        # =>This Inner Loop Header: Depth=1
	movl	%r14d, %ebx
	addl	%ebx, %ebx
	movq	-16(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx), %rdi
	movq	%rdi, (%rdx)
	cmpq	%r9, %rdi
	jb	.LBB1_69
# BB#42:                                #   in Loop: Header=BB1_41 Depth=1
	leaq	4(%rdi), %rcx
	cmpq	%r10, %rcx
	ja	.LBB1_69
# BB#43:                                #   in Loop: Header=BB1_41 Depth=1
	cmpq	%r9, %rcx
	jbe	.LBB1_69
# BB#44:                                #   in Loop: Header=BB1_41 Depth=1
	movq	(%rsi), %r13
	cmpq	%r9, %r13
	jb	.LBB1_69
# BB#45:                                #   in Loop: Header=BB1_41 Depth=1
	leaq	1(%r13), %r12
	cmpq	%r10, %r12
	ja	.LBB1_69
# BB#46:                                #   in Loop: Header=BB1_41 Depth=1
	cmpq	%r9, %r12
	jbe	.LBB1_69
# BB#47:                                #   in Loop: Header=BB1_41 Depth=1
	movl	8(%rsi), %ebp
	movl	12(%rsi), %r14d
	movl	%ebp, %ecx
	shrl	$11, %ecx
	movl	(%rdi), %r11d
	movzwl	%r11w, %r10d
	imull	%r10d, %ecx
	subl	%ecx, %r14d
	jae	.LBB1_48
# BB#49:                                #   in Loop: Header=BB1_41 Depth=1
	movl	%ecx, 8(%rsi)
	movl	$2048, %ebp             # imm = 0x800
	subl	%r10d, %ebp
	shrl	$5, %ebp
	addl	%r11d, %ebp
	xorl	%r14d, %r14d
	jmp	.LBB1_50
.LBB1_48:                               #   in Loop: Header=BB1_41 Depth=1
	subl	%ecx, %ebp
	movl	%ebp, 8(%rsi)
	movl	%r14d, 12(%rsi)
	shrl	$5, %r10d
	movl	%r11d, %ebp
	subl	%r10d, %ebp
	movl	$1, %r14d
.LBB1_50:                               #   in Loop: Header=BB1_41 Depth=1
	andl	$-65536, %r11d          # imm = 0xFFFF0000
	movzwl	%bp, %ecx
	orl	%r11d, %ecx
	movl	%ecx, (%rdi)
	movl	8(%rsi), %edi
	cmpl	$16777215, %edi         # imm = 0xFFFFFF
	movq	-24(%rsp), %r10         # 8-byte Reload
	ja	.LBB1_52
# BB#51:                                #   in Loop: Header=BB1_41 Depth=1
	movq	%r13, (%rdx)
	movq	(%rsi), %rcx
	movzbl	(%rcx), %ecx
	movl	12(%rsi), %ebp
	shll	$8, %ebp
	orl	%ecx, %ebp
	movq	%r12, (%rdx)
	shll	$8, %edi
	movl	%ebp, 12(%rsi)
	movl	%edi, 8(%rsi)
	movq	%r12, (%rsi)
.LBB1_52:                               # %lzma_486248.exit60.us
                                        #   in Loop: Header=BB1_41 Depth=1
	orl	%ebx, %r14d
	incl	%r15d
	jne	.LBB1_41
	jmp	.LBB1_67
.Lfunc_end1:
	.size	lzma_4863da, .Lfunc_end1-lzma_4863da
	.cfi_endproc

	.globl	lzma_upack_esi_00
	.p2align	4, 0x90
	.type	lzma_upack_esi_00,@function
lzma_upack_esi_00:                      # @lzma_upack_esi_00
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 16
	movq	%rsi, %r9
	cmpl	$4, %ecx
	setb	%al
	cmpq	%rdx, %r9
	jb	.LBB2_2
# BB#1:
	testb	%al, %al
	jne	.LBB2_2
# BB#3:
	leaq	4(%r9), %rax
	movl	%ecx, %r8d
	addq	%rdx, %r8
	cmpq	%rdx, %rax
	jbe	.LBB2_9
# BB#4:
	cmpl	$4, %ecx
	jb	.LBB2_9
# BB#5:
	cmpq	%r8, %rax
	ja	.LBB2_9
# BB#6:
	movq	(%rdi), %r10
	cmpq	%rdx, %r10
	jb	.LBB2_9
# BB#7:
	leaq	4(%r10), %rsi
	cmpq	%r8, %rsi
	ja	.LBB2_9
# BB#8:
	cmpq	%rdx, %rsi
	jbe	.LBB2_9
# BB#14:
	movl	8(%rdi), %r8d
	movl	%r8d, %edx
	shrl	$11, %edx
	movl	(%r9), %eax
	imull	%eax, %edx
	movl	(%r10), %esi
	bswapl	%esi
	movl	12(%rdi), %ecx
	subl	%ecx, %esi
	cmpl	%edx, %esi
	jae	.LBB2_16
# BB#15:
	movl	%edx, 8(%rdi)
	movl	$2048, %ecx             # imm = 0x800
	subl	%eax, %ecx
	shrl	$5, %ecx
	addl	(%r9), %ecx
	xorl	%eax, %eax
	jmp	.LBB2_17
.LBB2_2:                                # %..thread86_crit_edge
	movl	%ecx, %r8d
	addq	%rdx, %r8
.LBB2_11:                               # %.thread86
	movl	$.L.str.2, %edi
	jmp	.LBB2_12
.LBB2_9:
	cmpq	%rdx, %rax
	jbe	.LBB2_11
# BB#10:
	cmpq	%r8, %rax
	ja	.LBB2_11
# BB#13:
	movq	(%rdi), %r9
	movl	$.L.str.3, %edi
.LBB2_12:
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movl	%ecx, %edx
	movq	%r9, %rcx
	callq	cli_dbgmsg
	movl	$-1, %eax
	popq	%rcx
	retq
.LBB2_16:
	addl	%edx, %ecx
	movl	%ecx, 12(%rdi)
	subl	%edx, %r8d
	movl	%r8d, 8(%rdi)
	shrl	$5, %eax
	movl	(%r9), %ecx
	subl	%eax, %ecx
	movl	$1, %eax
.LBB2_17:
	movl	%ecx, (%r9)
	movl	8(%rdi), %ecx
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB2_19
# BB#18:
	shll	$8, 12(%rdi)
	shll	$8, %ecx
	movl	%ecx, 8(%rdi)
	incq	%r10
	movq	%r10, (%rdi)
.LBB2_19:
	popq	%rcx
	retq
.Lfunc_end2:
	.size	lzma_upack_esi_00, .Lfunc_end2-lzma_upack_esi_00
	.cfi_endproc

	.globl	lzma_upack_esi_50
	.p2align	4, 0x90
	.type	lzma_upack_esi_50,@function
lzma_upack_esi_50:                      # @lzma_upack_esi_50
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%r9, (%rsp)             # 8-byte Spill
	movq	%r8, %r15
	movq	%rcx, %r13
	movl	%edx, %r12d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	64(%rsp), %r14
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	leal	(,%rbp,4), %esi
	addq	%r15, %rsi
	movq	%rsi, (%r13)
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movl	72(%rsp), %ecx
	callq	lzma_upack_esi_00
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$-1, %eax
	je	.LBB3_2
# BB#3:                                 #   in Loop: Header=BB3_1 Depth=1
	leal	(%rax,%rbp,2), %ebp
	movl	%ebp, %eax
	subl	%r12d, %eax
	jb	.LBB3_1
# BB#4:
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%eax, (%rcx)
	xorl	%eax, %eax
	jmp	.LBB3_5
.LBB3_2:
	movl	$-1, %eax
.LBB3_5:                                # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	lzma_upack_esi_50, .Lfunc_end3-lzma_upack_esi_50
	.cfi_endproc

	.globl	lzma_upack_esi_54
	.p2align	4, 0x90
	.type	lzma_upack_esi_54,@function
lzma_upack_esi_54:                      # @lzma_upack_esi_54
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 80
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, %r14
	movq	%rcx, %rbx
	movq	%rdx, %rbp
	movl	%esi, %r15d
	movq	%rdi, %r13
	movl	80(%rsp), %ecx
	movb	$8, (%rbp)
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	callq	lzma_upack_esi_00
	movq	(%rbx), %rsi
	addq	$4, %rsi
	movq	%rsi, (%rbx)
	andl	$-256, %r15d
	testl	%eax, %eax
	movq	%r14, 16(%rsp)          # 8-byte Spill
	je	.LBB4_1
# BB#2:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	80(%rsp), %ecx
	callq	lzma_upack_esi_00
	testl	%eax, %eax
	je	.LBB4_3
# BB#4:
	shll	$5, (%rbp)
	movl	$17, %r15d
	jmp	.LBB4_5
.LBB4_1:
	orl	$1, %r15d
	jmp	.LBB4_5
.LBB4_3:
	orl	$9, %r15d
.LBB4_5:
	movl	80(%rsp), %eax
	movl	(%rbp), %ebp
	movq	%r15, 8(%rsp)           # 8-byte Spill
	leal	(,%r15,4), %r14d
	addq	(%rbx), %r14
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB4_6:                                # =>This Inner Loop Header: Depth=1
	leal	(,%r15,4), %esi
	addq	%r14, %rsi
	movq	%rsi, (%rbx)
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, %ecx
	callq	lzma_upack_esi_00
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$-1, %eax
	je	.LBB4_7
# BB#8:                                 #   in Loop: Header=BB4_6 Depth=1
	leal	(%rax,%r15,2), %r15d
	cmpl	%ebp, %r15d
	movl	80(%rsp), %eax
	jb	.LBB4_6
# BB#9:
	movq	8(%rsp), %rcx           # 8-byte Reload
	subl	%ebp, %ecx
	addl	%r15d, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%ecx, (%rax)
	xorl	%eax, %eax
	jmp	.LBB4_10
.LBB4_7:
	movl	$-1, %eax
.LBB4_10:                               # %lzma_upack_esi_50.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	lzma_upack_esi_54, .Lfunc_end4-lzma_upack_esi_54
	.cfi_endproc

	.globl	unmew11
	.p2align	4, 0x90
	.type	unmew11,@function
unmew11:                                # @unmew11
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 208
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%ecx, %r15d
	movq	%rsi, %r10
	movl	208(%rsp), %esi
	movslq	%r8d, %rax
	addq	%r10, %rax
	movslq	%edx, %rcx
	leaq	12(%rcx,%rax), %rbx
	leal	(%rsi,%r9), %edi
	leal	(%r8,%r15), %edx
	movl	8(%rcx,%rax), %esi
	cmpl	$4, %edx
	jb	.LBB5_24
# BB#1:                                 # %.split.preheader
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movl	4(%rcx,%rax), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	leaq	(%r10,%rdx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$0, %edx
	movl	$0, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$0, %r9d
	movl	$0, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$0, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movl	%r8d, %r14d
	movl	%edi, 20(%rsp)          # 4-byte Spill
	movq	%r10, 120(%rsp)         # 8-byte Spill
.LBB5_2:                                # %.split
                                        # =>This Inner Loop Header: Depth=1
	movq	%r9, 56(%rsp)           # 8-byte Spill
	subl	%edi, %esi
	movq	%rsi, %r13
	addq	%r10, %r13
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rdx, %r12
	movl	%edx, %esi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movq	%r10, %rbp
	callq	cli_dbgmsg
	movq	%rbp, %rcx
	cmpq	%rcx, %rbx
	jb	.LBB5_25
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	leaq	4(%rbx), %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB5_25
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	cmpq	%rcx, %rax
	jbe	.LBB5_25
# BB#5:                                 #   in Loop: Header=BB5_2 Depth=1
	leaq	4(%r13), %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB5_25
# BB#6:                                 #   in Loop: Header=BB5_2 Depth=1
	cmpq	%rcx, %rax
	jbe	.LBB5_25
# BB#7:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%r15d, %edx
	movl	%r14d, %ecx
	leaq	64(%rsp), %r8
	leaq	144(%rsp), %r9
	callq	unmew
	testl	%eax, %eax
	jne	.LBB5_28
# BB#8:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax
	movq	%rbp, %r10
	cmpq	%r10, %rax
	jb	.LBB5_27
# BB#9:                                 #   in Loop: Header=BB5_2 Depth=1
	leaq	4(%rax), %rbp
	cmpq	32(%rsp), %rbp          # 8-byte Folded Reload
	ja	.LBB5_27
# BB#10:                                #   in Loop: Header=BB5_2 Depth=1
	cmpq	%r10, %rbp
	jbe	.LBB5_27
# BB#11:                                #   in Loop: Header=BB5_2 Depth=1
	movq	144(%rsp), %r8
	movl	(%rax), %esi
	cmpl	$0, 216(%rsp)
	je	.LBB5_13
# BB#12:                                # %._crit_edge
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%r12, %rdx
	incq	%rdx
	movq	56(%rsp), %r9           # 8-byte Reload
	movl	20(%rsp), %edi          # 4-byte Reload
	jmp	.LBB5_20
	.p2align	4, 0x90
.LBB5_13:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%r8, %rax
	subq	%r10, %rax
	movq	%rax, %rbx
	sarq	$63, %rbx
	shrq	$52, %rbx
	addl	%eax, %ebx
	shrl	$12, %ebx
	andl	$4095, %eax             # imm = 0xFFF
	cmpq	$1, %rax
	sbbl	$-1, %ebx
	shll	$12, %ebx
	testq	%r12, %r12
	je	.LBB5_15
# BB#14:                                #   in Loop: Header=BB5_2 Depth=1
	leaq	(%r12,%r12,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	8(%rcx,%rax,4), %ebx
	jb	.LBB5_42
.LBB5_15:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	leaq	(%r12,%r12,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	72(,%rax,4), %rsi
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	cli_realloc
	movq	%rax, %rsi
	testq	%rsi, %rsi
	je	.LBB5_32
# BB#16:                                #   in Loop: Header=BB5_2 Depth=1
	movl	$0, 8(%rsi)
	movl	208(%rsp), %eax
	movq	%rax, %rdx
	movl	%edx, (%rsi)
	leaq	1(%r12), %rcx
	leaq	(%rcx,%rcx,8), %rax
	movl	%ebx, 8(%rsi,%rax,4)
	leal	(%rbx,%rdx), %edx
	movl	%edx, (%rsi,%rax,4)
	testq	%r12, %r12
	je	.LBB5_18
# BB#17:                                #   in Loop: Header=BB5_2 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	subl	8(%rsi,%rax,4), %ebx
	movq	8(%rsp), %r8            # 8-byte Reload
	jmp	.LBB5_19
.LBB5_18:                               #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB5_19:                               #   in Loop: Header=BB5_2 Depth=1
	movl	%ebx, 4(%rsi,%rax,4)
	movl	%ebx, 12(%rsi,%rax,4)
	movq	64(%rsp), %rax
	movq	%rcx, %rdx
	movq	%rsi, %r9
	movq	%rsi, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	120(%rsp), %r10         # 8-byte Reload
	movl	20(%rsp), %edi          # 4-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
.LBB5_20:                               #   in Loop: Header=BB5_2 Depth=1
	addl	136(%rsp), %r15d        # 4-byte Folded Reload
	subl	%ebp, %r15d
	addl	%r13d, %r14d
	subl	%r8d, %r14d
	cmpl	$0, (%rax)
	movq	%rbp, %rbx
	jne	.LBB5_2
# BB#21:                                # %.us-lcssa226
	movl	216(%rsp), %ebx
	testl	%ebx, %ebx
	je	.LBB5_29
# BB#22:
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%r10, %r14
	callq	free
	movq	%r14, %rcx
	movslq	%ebx, %rax
	leaq	8(%rcx,%rax), %rbx
	cmpq	%rcx, %rbx
	jae	.LBB5_33
.LBB5_23:
	movl	$.L.str.8, %edi
	jmp	.LBB5_26
.LBB5_24:                               # %.split.us
	subl	%edi, %esi
	addq	%rsi, %r10
	movl	$.L.str.4, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	movq	%r10, %rcx
	callq	cli_dbgmsg
.LBB5_25:                               # %.us-lcssa
	movl	$.L.str.5, %edi
.LBB5_26:                               # %.thread
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB5_45
.LBB5_27:                               # %.us-lcssa223
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB5_44
.LBB5_28:                               # %.us-lcssa222
	movq	24(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB5_44
.LBB5_29:
	movq	80(%rsp), %rcx          # 8-byte Reload
.LBB5_30:
	movl	76(%rsp), %r8d          # 4-byte Reload
	subl	%ecx, %r8d
	movl	$0, %r9d
	movq	%r10, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	240(%rsp), %eax
	pushq	%rax
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	addq	$16, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB5_43
# BB#31:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	$1, %ebp
	jmp	.LBB5_46
.LBB5_32:                               # %.us-lcssa225
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB5_44
.LBB5_33:
	leaq	1(%rbx), %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB5_23
# BB#34:
	cmpq	%rcx, %rax
	jbe	.LBB5_23
# BB#35:
	cmpb	$80, (%rbx)
	movl	$.L.str.10, %eax
	movl	$.L.str.11, %esi
	cmoveq	%rax, %rsi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r14, %rdi
	movq	112(%rsp), %rdx         # 8-byte Reload
	cmpl	$29, %edx
	jb	.LBB5_47
# BB#36:
	movq	64(%rsp), %rax
	leaq	4(%rax), %rsi
	cmpq	%rdi, %rsi
	jb	.LBB5_47
# BB#37:
	addq	$33, %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB5_47
# BB#38:
	cmpq	%rdi, %rax
	jbe	.LBB5_47
# BB#39:
	xorl	%r8d, %r8d
	cmpb	$80, (%rbx)
	sete	%r8b
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	20(%rsp), %ecx          # 4-byte Reload
	callq	mew_lzma
	movl	$-1, %ebp
	testl	%eax, %eax
	jne	.LBB5_46
# BB#40:
	movl	$1, %edi
	movl	$36, %esi
	callq	cli_calloc
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB5_48
# BB#41:
	movl	$0, 8(%rcx)
	movl	208(%rsp), %eax
	movl	%eax, (%rcx)
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%eax, 4(%rcx)
	movl	%eax, 12(%rcx)
	movl	$1, %edx
	movq	%rcx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r10
	jmp	.LBB5_30
.LBB5_42:                               # %.us-lcssa224
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	88(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB5_44
.LBB5_43:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB5_44:                               # %.thread
	callq	free
.LBB5_45:                               # %.thread
	movl	$-1, %ebp
.LBB5_46:                               # %.thread
	movl	%ebp, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_47:
	movl	$.L.str.12, %edi
	jmp	.LBB5_26
.LBB5_48:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB5_46
.Lfunc_end5:
	.size	unmew11, .Lfunc_end5-unmew11
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"MEWlzma: entering do while loop\n"
	.size	.L.str, 33

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"MEWlen: %08x ? %08x\n"
	.size	.L.str.1, 21

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"contain error! %08x %08x ecx: %08x [%08x]\n"
	.size	.L.str.2, 43

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"contain error! %08x %08x p0: %08x [%08x]\n"
	.size	.L.str.3, 42

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"MEW unpacking section %d (%08x->%08x)\n"
	.size	.L.str.4, 39

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Possibly programmer error or hand-crafted PE file, report to clamav team\n"
	.size	.L.str.5, 74

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"MEW: WTF - please report\n"
	.size	.L.str.6, 26

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"MEW: Out of memory\n"
	.size	.L.str.7, 20

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"MEW: couldn't access lzma 'special' tag\n"
	.size	.L.str.8, 41

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"MEW: lzma %swas used, unpacking\n"
	.size	.L.str.9, 33

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"special "
	.size	.L.str.10, 9

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.zero	1
	.size	.L.str.11, 1

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"MEW: lzma initialization data not available!\n"
	.size	.L.str.12, 46

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"MEW: Rebuilding failed\n"
	.size	.L.str.13, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
