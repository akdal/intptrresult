	.text
	.file	"ALACEncoder.bc"
	.globl	_ZN11ALACEncoderC2Ev
	.p2align	4, 0x90
	.type	_ZN11ALACEncoderC2Ev,@function
_ZN11ALACEncoderC2Ev:                   # @_ZN11ALACEncoderC2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV11ALACEncoder+16, (%rdi)
	movw	$0, 8(%rdi)
	movb	$0, 10(%rdi)
	movq	$0, 8272(%rdi)
	movl	$0, 8280(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	movl	$4096, 8284(%rdi)       # imm = 0x1000
	retq
.Lfunc_end0:
	.size	_ZN11ALACEncoderC2Ev, .Lfunc_end0-_ZN11ALACEncoderC2Ev
	.cfi_endproc

	.globl	_ZN11ALACEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN11ALACEncoderD2Ev,@function
_ZN11ALACEncoderD2Ev:                   # @_ZN11ALACEncoderD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV11ALACEncoder+16, (%rbx)
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	free
	movq	$0, 32(%rbx)
.LBB1_2:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	free
	movq	$0, 40(%rbx)
.LBB1_4:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_6
# BB#5:
	callq	free
	movq	$0, 48(%rbx)
.LBB1_6:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_8
# BB#7:
	callq	free
	movq	$0, 56(%rbx)
.LBB1_8:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_10
# BB#9:
	callq	free
	movq	$0, 64(%rbx)
.LBB1_10:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_12
# BB#11:
	callq	free
	movq	$0, 72(%rbx)
.LBB1_12:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN11ALACEncoderD2Ev, .Lfunc_end1-_ZN11ALACEncoderD2Ev
	.cfi_endproc

	.globl	_ZN11ALACEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN11ALACEncoderD0Ev,@function
_ZN11ALACEncoderD0Ev:                   # @_ZN11ALACEncoderD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN11ALACEncoderD2Ev
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN11ALACEncoderD0Ev, .Lfunc_end2-_ZN11ALACEncoderD0Ev
	.cfi_endproc

	.globl	_ZN11ALACEncoder12EncodeStereoEP9BitBufferPvjjj
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder12EncodeStereoEP9BitBufferPvjjj,@function
_ZN11ALACEncoder12EncodeStereoEP9BitBufferPvjjj: # @_ZN11ALACEncoder12EncodeStereoEP9BitBufferPvjjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi10:
	.cfi_def_cfa_offset 320
.Lcfi11:
	.cfi_offset %rbx, -56
.Lcfi12:
	.cfi_offset %r12, -48
.Lcfi13:
	.cfi_offset %r13, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	%rax, 192(%rsp)
	movups	(%rsi), %xmm0
	movaps	%xmm0, 176(%rsp)
	movzwl	8(%rdi), %ecx
	leal	-16(%rcx), %edx
	rolw	$14, %dx
	movl	$-50, %eax
	movzwl	%dx, %edx
	cmpl	$4, %edx
	ja	.LBB3_42
# BB#1:
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	cmpl	$3, %edx
	je	.LBB3_42
# BB#2:
	movswl	%cx, %edx
	movl	%r8d, %edi
	movzwl	%dx, %eax
	xorl	%esi, %esi
	cmpl	$23, %edx
	setg	%sil
	cmpl	$32, %eax
	movl	$2, %eax
	cmovel	%eax, %esi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	leal	(,%rsi,8), %eax
	movl	%eax, 140(%rsp)         # 4-byte Spill
	subl	%eax, %edx
	incl	%edx
	movl	%edx, 52(%rsp)          # 4-byte Spill
	movq	96(%rsp), %rbx          # 8-byte Reload
	movl	8284(%rbx), %eax
	movl	%eax, 168(%rsp)         # 4-byte Spill
	movswl	12(%rbx,%rdi,2), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movl	%r9d, %ebp
	shrl	$3, %ebp
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	movq	%rdi, %rax
	shlq	$9, %rax
	leaq	304(%rbx,%rax), %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	4400(%rbx,%rax), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	$-2147483648, 104(%rsp) # 4-byte Folded Spill
                                        # imm = 0x80000000
	movl	$1, %edx
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	movl	52(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_12:                               # %._crit_edge341
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%esi, 64(%rsp)          # 4-byte Spill
	cmpl	%ecx, %eax
	cmovbl	%eax, %ecx
	movl	%ecx, 104(%rsp)         # 4-byte Spill
	movq	96(%rsp), %rbx          # 8-byte Reload
	movw	8(%rbx), %cx
	incl	%edx
	movl	48(%rsp), %ebp          # 4-byte Reload
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	leal	-1(%rdx), %r15d
	movswl	%cx, %eax
	addl	$-16, %eax
	roll	$30, %eax
	cmpl	$4, %eax
	ja	.LBB3_9
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_5:                                #   in Loop: Header=BB3_3 Depth=1
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rcx
	movl	%r15d, (%rsp)
	movl	$2, %r9d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %r8d
	callq	mix16
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rcx
	movl	%r15d, (%rsp)
	movl	$2, %r9d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %r8d
	callq	mix20
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_7:                                #   in Loop: Header=BB3_3 Depth=1
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rcx
	movq	64(%rbx), %rax
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 16(%rsp)
	movq	%rax, 8(%rsp)
	movl	%r15d, (%rsp)
	movl	$2, %r9d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %r8d
	callq	mix24
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_8:                                #   in Loop: Header=BB3_3 Depth=1
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rcx
	movq	64(%rbx), %rax
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 16(%rsp)
	movq	%rax, 8(%rsp)
	movl	%r15d, (%rsp)
	movl	$2, %r9d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %r8d
	callq	mix32
	.p2align	4, 0x90
.LBB3_9:                                #   in Loop: Header=BB3_3 Depth=1
	movq	72(%rbx), %rsi
	movl	8288(%rbx), %edx
	leaq	240(%rsp), %r13
	movq	%r13, %rdi
	callq	BitBufferInit
	movq	32(%rbx), %rdi
	movq	48(%rbx), %rsi
	movl	$9, (%rsp)
	movl	$8, %r8d
	movl	%ebp, %edx
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	%r14d, %r9d
	callq	pc_block
	movq	40(%rbx), %rdi
	movq	56(%rbx), %rsi
	movl	$9, (%rsp)
	movl	$8, %r8d
	movl	%ebp, %edx
	movq	120(%rsp), %rcx         # 8-byte Reload
	movl	%r14d, %r9d
	callq	pc_block
	movl	$255, (%rsp)
	movl	$10, %esi
	movl	$40, %edx
	movl	$14, %ecx
	leaq	200(%rsp), %r12
	movq	%r12, %rdi
	movl	%ebp, %r8d
	movl	%ebp, %r9d
	callq	set_ag_params
	movq	48(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	movl	%ebp, %ecx
	movl	%r14d, %r8d
	leaq	60(%rsp), %r9
	callq	dyn_comp
	testl	%eax, %eax
	jne	.LBB3_42
# BB#10:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$255, (%rsp)
	movl	$10, %esi
	movl	$40, %edx
	movl	$14, %ecx
	movq	%r12, %rdi
	movl	%ebp, %r8d
	movl	%ebp, %r9d
	callq	set_ag_params
	movq	56(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	movl	%ebp, %ecx
	movl	%r14d, %r8d
	leaq	56(%rsp), %r9
	callq	dyn_comp
	testl	%eax, %eax
	jne	.LBB3_42
# BB#11:                                #   in Loop: Header=BB3_3 Depth=1
	movl	56(%rsp), %eax
	addl	60(%rsp), %eax
	movl	104(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, %eax
	movl	64(%rsp), %esi          # 4-byte Reload
	cmovbl	%r15d, %esi
	movq	72(%rsp), %rdx          # 8-byte Reload
	cmpl	$4, %edx
	jle	.LBB3_12
# BB#13:
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	152(%rsp), %rax         # 8-byte Reload
	movw	%si, 12(%rbx,%rax,2)
	movswl	%si, %r14d
	movswl	8(%rbx), %eax
	addl	$-16, %eax
	roll	$30, %eax
	cmpl	$4, %eax
	movq	112(%rsp), %rbp         # 8-byte Reload
	ja	.LBB3_19
# BB#14:
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_15:
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rcx
	movl	%r14d, (%rsp)
	movl	$2, %r9d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %r8d
	callq	mix16
	jmp	.LBB3_19
.LBB3_16:
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rcx
	movl	%r14d, (%rsp)
	movl	$2, %r9d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %r8d
	callq	mix20
	jmp	.LBB3_19
.LBB3_17:
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rcx
	movq	64(%rbx), %rax
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 16(%rsp)
	movq	%rax, 8(%rsp)
	movl	%r14d, (%rsp)
	movl	$2, %r9d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %r8d
	callq	mix24
	jmp	.LBB3_19
.LBB3_18:
	movq	32(%rbx), %rdx
	movq	40(%rbx), %rcx
	movq	64(%rbx), %rax
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 16(%rsp)
	movq	%rax, 8(%rsp)
	movl	%r14d, (%rsp)
	movl	$2, %r9d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	36(%rsp), %esi          # 4-byte Reload
	movl	%ebp, %r8d
	callq	mix32
.LBB3_19:                               # %.preheader313
	movl	%r14d, 164(%rsp)        # 4-byte Spill
	movl	%ebp, %eax
	shrl	$5, %eax
	movl	%eax, 172(%rsp)         # 4-byte Spill
	movl	$4, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$-2147483648, 128(%rsp) # 4-byte Folded Spill
                                        # imm = 0x80000000
	movl	$4, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	240(%rsp), %r12
	addq	%rbx, 88(%rsp)          # 8-byte Folded Spill
	movl	$-2147483648, %ecx      # imm = 0x80000000
	movl	$4, %r15d
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB3_20:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_21 Depth 2
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	%r15, 120(%rsp)         # 8-byte Spill
	movq	72(%rbp), %rsi
	movl	8288(%rbp), %edx
	movq	%r12, %rdi
	callq	BitBufferInit
	movq	72(%rsp), %rax          # 8-byte Reload
	shlq	$5, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	48(%rax,%rcx), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	leaq	4144(%rax,%rcx), %r15
	movl	$8, %r14d
	movl	172(%rsp), %r12d        # 4-byte Reload
	.p2align	4, 0x90
.LBB3_21:                               #   Parent Loop BB3_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rbp), %rdi
	movq	48(%rbp), %rsi
	movl	$9, (%rsp)
	movl	%r12d, %edx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	movl	52(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %r9d
	callq	pc_block
	movq	40(%rbp), %rdi
	movq	56(%rbp), %rsi
	movl	$9, (%rsp)
	movl	%r12d, %edx
	movq	%r15, %rcx
	movl	%ebx, %r8d
	movl	%r13d, %r9d
	callq	pc_block
	decl	%r14d
	jne	.LBB3_21
# BB#22:                                #   in Loop: Header=BB3_20 Depth=1
	movl	$255, (%rsp)
	movl	$10, %esi
	movl	$40, %edx
	movl	$14, %ecx
	leaq	200(%rsp), %rbx
	movq	%rbx, %rdi
	movl	48(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %r8d
	movl	%r15d, %r9d
	callq	set_ag_params
	movq	48(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	240(%rsp), %r12
	movq	%r12, %rdx
	movl	%r15d, %ecx
	movl	52(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %r8d
	leaq	60(%rsp), %r9
	callq	dyn_comp
	movl	60(%rsp), %eax
	movq	72(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r14d
	shll	$4, %r14d
	leal	(%r14,%rax,8), %eax
	movl	128(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
	cmovbl	%ebx, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	cmovbl	%eax, %edx
	movl	%edx, 128(%rsp)         # 4-byte Spill
	movl	$255, (%rsp)
	movl	$10, %esi
	movl	$40, %edx
	movl	$14, %ecx
	leaq	200(%rsp), %rdi
	movl	%r15d, %r8d
	movl	%r15d, %r9d
	callq	set_ag_params
	movq	56(%rbp), %rsi
	leaq	200(%rsp), %rdi
	movq	%r12, %rdx
	movl	%r15d, %ecx
	movl	%r13d, %r8d
	leaq	56(%rsp), %r9
	callq	dyn_comp
	movl	56(%rsp), %eax
	leal	(%r14,%rax,8), %eax
	movq	152(%rsp), %rcx         # 8-byte Reload
	cmpl	%ecx, %eax
	movq	120(%rsp), %r15         # 8-byte Reload
	cmovbl	%ebx, %r15d
	cmovbl	%eax, %ecx
	addq	$4, %rbx
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	cmpq	$9, %rbx
	jb	.LBB3_20
# BB#23:                                # %._crit_edge342
	xorl	%eax, %eax
	movq	112(%rsp), %r14         # 8-byte Reload
	movl	168(%rsp), %r12d        # 4-byte Reload
	cmpl	%r14d, %r12d
	setne	%al
	shll	$5, %eax
	leal	64(%rax,%rcx), %ecx
	addl	128(%rsp), %ecx         # 4-byte Folded Reload
	leal	(%r14,%r14), %esi
	movl	%esi, %edx
	imull	140(%rsp), %edx         # 4-byte Folded Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	testl	%ebx, %ebx
	cmovel	%ebx, %edx
	addl	%ecx, %edx
	movswl	8(%rbp), %ecx
	imull	%esi, %ecx
	leal	16(%rax,%rcx), %eax
	movl	%edx, 72(%rsp)          # 4-byte Spill
	cmpl	%eax, %edx
	jb	.LBB3_24
.LBB3_41:                               # %.thread344
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	%r14d, %r8d
	callq	_ZN11ALACEncoder18EncodeStereoEscapeEP9BitBufferPvjj
	xorl	%eax, %eax
	jmp	.LBB3_42
.LBB3_24:
	movl	%esi, 48(%rsp)          # 4-byte Spill
	movl	%eax, 104(%rsp)         # 4-byte Spill
	xorl	%ebp, %ebp
	cmpl	%r14d, %r12d
	setne	%bpl
	xorl	%esi, %esi
	movl	$12, %edx
	movq	144(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rdi
	callq	BitBufferWrite
	shll	$3, %ebp
	leal	(%rbx,%rbx), %esi
	orl	%ebp, %esi
	movl	$4, %edx
	movq	%r13, %rdi
	callq	BitBufferWrite
	cmpl	%r14d, %r12d
	je	.LBB3_26
# BB#25:
	movl	$32, %edx
	movq	%r13, %rdi
	movl	%r14d, %esi
	callq	BitBufferWrite
.LBB3_26:
	movl	$2, %esi
	movl	$8, %edx
	movq	%r13, %rdi
	callq	BitBufferWrite
	movl	$8, %edx
	movq	%r13, %rdi
	movl	164(%rsp), %esi         # 4-byte Reload
	callq	BitBufferWrite
	movl	$9, %esi
	movl	$8, %edx
	movq	%r13, %rdi
	callq	BitBufferWrite
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %esi
	orl	$128, %esi
	movl	$8, %edx
	movq	%r13, %rdi
	callq	BitBufferWrite
	testl	%ebx, %ebx
	movq	%r13, %rbx
	je	.LBB3_29
# BB#27:                                # %.lr.ph321
	movq	64(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %eax
	movl	%ecx, %r14d
	shlq	$5, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	80(%rax,%rcx), %rbp
	.p2align	4, 0x90
.LBB3_28:                               # =>This Inner Loop Header: Depth=1
	movswl	(%rbp), %esi
	movl	$16, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	addq	$2, %rbp
	decq	%r14
	jne	.LBB3_28
.LBB3_29:                               # %._crit_edge322
	movl	$9, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	movl	%r15d, %esi
	orl	$128, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	testl	%r15d, %r15d
	je	.LBB3_32
# BB#30:                                # %.lr.ph318
	leal	-1(%r15), %eax
	movl	%r15d, %r14d
	shlq	$5, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	4176(%rax,%rcx), %rbp
	.p2align	4, 0x90
.LBB3_31:                               # =>This Inner Loop Header: Depth=1
	movswl	(%rbp), %esi
	movl	$16, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	addq	$2, %rbp
	decq	%r14
	jne	.LBB3_31
.LBB3_32:                               # %._crit_edge
	movq	%r15, 120(%rsp)         # 8-byte Spill
	movq	80(%rsp), %r12          # 8-byte Reload
	testl	%r12d, %r12d
	sete	%al
	movl	48(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	movl	140(%rsp), %r13d        # 4-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	je	.LBB3_36
# BB#33:                                # %._crit_edge
	testb	%al, %al
	jne	.LBB3_36
# BB#34:                                # %.lr.ph
	shll	$4, %r12d
	movl	%ecx, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_35:                               # =>This Inner Loop Header: Depth=1
	movq	64(%r15), %rax
	movzwl	(%rax,%rbp,2), %edx
	movl	%r13d, %ecx
	shll	%cl, %edx
	movzwl	2(%rax,%rbp,2), %esi
	orl	%edx, %esi
	movq	%rbx, %rdi
	movl	%r12d, %edx
	callq	BitBufferWrite
	addq	$2, %rbp
	cmpq	%r14, %rbp
	jb	.LBB3_35
.LBB3_36:                               # %.loopexit
	movq	32(%r15), %rdi
	movq	48(%r15), %rsi
	movq	64(%rsp), %r8           # 8-byte Reload
	leal	-1(%r8), %eax
	shlq	$5, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	80(%rax,%rcx), %rcx
	movl	$9, (%rsp)
	movq	112(%rsp), %rbp         # 8-byte Reload
	movl	%ebp, %edx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r15, %r14
	movl	52(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %r9d
	callq	pc_block
	movl	$255, (%rsp)
	leaq	200(%rsp), %r12
	movl	$10, %esi
	movl	$40, %edx
	movl	$14, %ecx
	movq	%r12, %rdi
	movl	%ebp, %r8d
	movl	%ebp, %r9d
	callq	set_ag_params
	movq	48(%r14), %rsi
	leaq	60(%rsp), %r9
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	movl	%ebx, %r8d
	callq	dyn_comp
	testl	%eax, %eax
	movq	120(%rsp), %r8          # 8-byte Reload
	movl	104(%rsp), %r15d        # 4-byte Reload
	jne	.LBB3_42
# BB#37:
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	40(%r14), %rdi
	movq	56(%r14), %rsi
	leal	-1(%r8), %eax
	shlq	$5, %rax
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	4176(%rax,%rcx), %rcx
	movl	$9, (%rsp)
	movl	%ebp, %edx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%ebx, %r9d
	callq	pc_block
	movl	$255, (%rsp)
	leaq	200(%rsp), %r12
	movl	$10, %esi
	movl	$40, %edx
	movl	$14, %ecx
	movq	%r12, %rdi
	movl	%ebp, %r8d
	movl	%ebp, %r9d
	callq	set_ag_params
	movq	56(%r14), %rsi
	leaq	56(%rsp), %r9
	movq	%r12, %rdi
	movq	144(%rsp), %rdx         # 8-byte Reload
	movl	%ebp, %ecx
	movl	%ebx, %r8d
	callq	dyn_comp
	testl	%eax, %eax
	je	.LBB3_38
.LBB3_42:                               # %.loopexit314
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_38:
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	callq	BitBufferGetPosition
	movl	%eax, %r14d
	leaq	176(%rsp), %rdi
	callq	BitBufferGetPosition
	subl	%eax, %r14d
	cmpl	%r15d, %r14d
	jae	.LBB3_39
# BB#40:
	xorl	%eax, %eax
	cmpl	%r15d, 72(%rsp)         # 4-byte Folded Reload
	movq	112(%rsp), %r14         # 8-byte Reload
	jae	.LBB3_41
	jmp	.LBB3_42
.LBB3_39:                               # %.thread
	movq	192(%rsp), %rax
	movq	%rax, 16(%rbp)
	movaps	176(%rsp), %xmm0
	movups	%xmm0, (%rbp)
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	%r15d, %edx
	callq	printf
	movq	112(%rsp), %r14         # 8-byte Reload
	jmp	.LBB3_41
.Lfunc_end3:
	.size	_ZN11ALACEncoder12EncodeStereoEP9BitBufferPvjjj, .Lfunc_end3-_ZN11ALACEncoder12EncodeStereoEP9BitBufferPvjjj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_5
	.quad	.LBB3_6
	.quad	.LBB3_7
	.quad	.LBB3_9
	.quad	.LBB3_8
.LJTI3_1:
	.quad	.LBB3_15
	.quad	.LBB3_16
	.quad	.LBB3_17
	.quad	.LBB3_19
	.quad	.LBB3_18

	.text
	.globl	_ZN11ALACEncoder18EncodeStereoEscapeEP9BitBufferPvjj
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder18EncodeStereoEscapeEP9BitBufferPvjj,@function
_ZN11ALACEncoder18EncodeStereoEscapeEP9BitBufferPvjj: # @_ZN11ALACEncoder18EncodeStereoEscapeEP9BitBufferPvjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, %r12d
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movl	8284(%r13), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	xorl	%ebp, %ebp
	cmpl	%r14d, %eax
	setne	%bpl
	xorl	%esi, %esi
	movl	$12, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	leal	1(,%rbp,8), %esi
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	cmpl	%r14d, 4(%rsp)          # 4-byte Folded Reload
	je	.LBB4_2
# BB#1:
	movl	$32, %edx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	BitBufferWrite
.LBB4_2:
	movswl	8(%r13), %eax
	addl	$-16, %eax
	roll	$30, %eax
	cmpl	$4, %eax
	ja	.LBB4_16
# BB#3:
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_4:
	imull	%r12d, %r14d
	testl	%r14d, %r14d
	je	.LBB4_16
# BB#5:                                 # %.lr.ph.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %eax
	movswl	(%r15,%rax,2), %esi
	movl	$16, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	movl	%ebp, %eax
	movswl	(%r15,%rax,2), %esi
	movl	$16, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	leal	-1(%rbp,%r12), %eax
	leal	(%rbp,%r12), %ecx
	cmpl	%r14d, %eax
	movl	%ecx, %ebp
	jb	.LBB4_6
	jmp	.LBB4_16
.LBB4_7:
	movq	32(%r13), %rdx
	movq	40(%r13), %rcx
	subq	$8, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	movl	%r12d, %esi
	movl	%r14d, %r8d
	pushq	$0
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	mix20
	addq	$16, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -16
	testl	%r14d, %r14d
	je	.LBB4_16
# BB#8:                                 # %.lr.ph61.preheader
	movl	%r14d, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph61
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%r13), %rax
	movl	(%rax,%rbp,4), %esi
	movl	$20, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	movq	40(%r13), %rax
	movl	(%rax,%rbp,4), %esi
	movl	$20, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	incq	%rbp
	cmpq	%rbp, %r14
	jne	.LBB4_9
	jmp	.LBB4_16
.LBB4_10:
	movq	32(%r13), %rdx
	movq	40(%r13), %rcx
	subq	$8, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	movl	$0, %r9d
	movq	%r15, %rdi
	movl	%r12d, %esi
	movl	%r14d, %r8d
	pushq	$0
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	64(%r13)
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	mix24
	addq	$32, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -32
	testl	%r14d, %r14d
	je	.LBB4_16
# BB#11:                                # %.lr.ph63.preheader
	movl	%r14d, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_12:                               # %.lr.ph63
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%r13), %rax
	movl	(%rax,%rbp,4), %esi
	movl	$24, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	movq	40(%r13), %rax
	movl	(%rax,%rbp,4), %esi
	movl	$24, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	incq	%rbp
	cmpq	%rbp, %r14
	jne	.LBB4_12
	jmp	.LBB4_16
.LBB4_13:
	imull	%r12d, %r14d
	testl	%r14d, %r14d
	je	.LBB4_16
# BB#14:                                # %.lr.ph65.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB4_15:                               # %.lr.ph65
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %eax
	movl	(%r15,%rax,4), %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	movl	%ebp, %eax
	movl	(%r15,%rax,4), %esi
	movl	$32, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	leal	-1(%rbp,%r12), %eax
	leal	(%rbp,%r12), %ecx
	cmpl	%r14d, %eax
	movl	%ecx, %ebp
	jb	.LBB4_15
.LBB4_16:                               # %.loopexit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN11ALACEncoder18EncodeStereoEscapeEP9BitBufferPvjj, .Lfunc_end4-_ZN11ALACEncoder18EncodeStereoEscapeEP9BitBufferPvjj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_4
	.quad	.LBB4_7
	.quad	.LBB4_10
	.quad	.LBB4_16
	.quad	.LBB4_13

	.text
	.globl	_ZN11ALACEncoder16EncodeStereoFastEP9BitBufferPvjjj
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder16EncodeStereoFastEP9BitBufferPvjjj,@function
_ZN11ALACEncoder16EncodeStereoFastEP9BitBufferPvjjj: # @_ZN11ALACEncoder16EncodeStereoFastEP9BitBufferPvjjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 240
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%ecx, %r15d
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movq	16(%r12), %rax
	movq	%rax, 128(%rsp)
	movups	(%r12), %xmm0
	movaps	%xmm0, 112(%rsp)
	movswl	8(%rbp), %esi
	leal	-16(%rsi), %ecx
	movl	%ecx, %edx
	rolw	$14, %dx
	movl	$-50, %eax
	movzwl	%dx, %edx
	cmpl	$4, %edx
	ja	.LBB5_20
# BB#1:
	cmpl	$3, %edx
	je	.LBB5_20
# BB#2:
	movl	%r8d, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movzwl	%si, %eax
	xorl	%edx, %edx
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	cmpl	$23, %esi
	setg	%dl
	cmpl	$32, %eax
	movl	$2, %r13d
	cmovnel	%edx, %r13d
	leal	(,%r13,8), %ebx
	movl	8284(%rbp), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	roll	$30, %ecx
	cmpl	$4, %ecx
	ja	.LBB5_8
# BB#3:
	jmpq	*.LJTI5_0(,%rcx,8)
.LBB5_4:
	movq	32(%rbp), %rdx
	movq	40(%rbp), %rcx
	movl	$0, (%rsp)
	movl	$2, %r9d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%r14d, %r8d
	callq	mix16
	jmp	.LBB5_8
.LBB5_5:
	movq	32(%rbp), %rdx
	movq	40(%rbp), %rcx
	movl	$0, (%rsp)
	movl	$2, %r9d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%r14d, %r8d
	callq	mix20
	jmp	.LBB5_8
.LBB5_6:
	movq	32(%rbp), %rdx
	movq	40(%rbp), %rcx
	movq	64(%rbp), %rax
	movl	%r13d, 16(%rsp)
	movq	%rax, 8(%rsp)
	movl	$0, (%rsp)
	movl	$2, %r9d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%r14d, %r8d
	callq	mix24
	jmp	.LBB5_8
.LBB5_7:
	movq	32(%rbp), %rdx
	movq	40(%rbp), %rcx
	movq	64(%rbp), %rax
	movl	%r13d, 16(%rsp)
	movq	%rax, 8(%rsp)
	movl	$0, (%rsp)
	movl	$2, %r9d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%r14d, %r8d
	callq	mix32
.LBB5_8:
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	movl	%r15d, 68(%rsp)         # 4-byte Spill
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	movq	56(%rsp), %rbp          # 8-byte Reload
	subl	%ebx, %ebp
	xorl	%ebx, %ebx
	movl	40(%rsp), %r15d         # 4-byte Reload
	cmpl	%r14d, %r15d
	setne	%bl
	xorl	%esi, %esi
	movl	$12, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	shll	$3, %ebx
	leal	(%r13,%r13), %esi
	orl	%ebx, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	cmpl	%r14d, %r15d
	je	.LBB5_10
# BB#9:
	movl	$32, %edx
	movq	%r12, %rdi
	movl	%r14d, %esi
	callq	BitBufferWrite
.LBB5_10:
	movq	%r14, 88(%rsp)          # 8-byte Spill
	incl	%ebp
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	$2, %esi
	movl	$8, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	xorl	%esi, %esi
	movl	$8, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movl	$9, %esi
	movl	$8, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movl	$136, %esi
	movl	$8, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movq	48(%rsp), %rbx          # 8-byte Reload
	shlq	$9, %rbx
	movq	104(%rsp), %rbp         # 8-byte Reload
	leaq	304(%rbp,%rbx), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movswl	304(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	306(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	308(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	310(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	312(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	314(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	316(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	318(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movl	$9, %esi
	movl	$8, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movl	$136, %esi
	movl	$8, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	4400(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	4402(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	4404(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	4406(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	4408(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	4410(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movswl	4412(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movswl	4414(%rbp,%rbx), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movq	%r13, 80(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	movl	44(%rsp), %ebx          # 4-byte Reload
	je	.LBB5_14
# BB#11:                                # %.preheader
	movq	88(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addl	%eax, %eax
	je	.LBB5_14
# BB#12:                                # %.lr.ph
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r15d
	shll	$4, %r15d
	movl	%eax, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_13:                               # =>This Inner Loop Header: Depth=1
	movq	64(%rbp), %rax
	movzwl	(%rax,%r13,2), %edx
	movl	%ebx, %ecx
	shll	%cl, %edx
	movzwl	2(%rax,%r13,2), %esi
	orl	%edx, %esi
	movq	%r12, %rdi
	movl	%r15d, %edx
	callq	BitBufferWrite
	addq	$2, %r13
	cmpq	%r14, %r13
	jb	.LBB5_13
.LBB5_14:                               # %.loopexit
	movq	32(%rbp), %rdi
	movq	48(%rbp), %rsi
	movl	$9, (%rsp)
	movl	$8, %r8d
	movq	88(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %edx
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r9d
	callq	pc_block
	movl	$255, (%rsp)
	leaq	144(%rsp), %r15
	movl	$10, %esi
	movl	$40, %edx
	movl	$14, %ecx
	movq	%r15, %rdi
	movl	%r14d, %r8d
	movl	%r14d, %r9d
	callq	set_ag_params
	movq	48(%rbp), %rsi
	leaq	76(%rsp), %r9
	movq	%r15, %rdi
	movq	%r12, %rdx
	movl	%r14d, %ecx
	movl	%ebx, %r8d
	callq	dyn_comp
	testl	%eax, %eax
	movl	68(%rsp), %r13d         # 4-byte Reload
	jne	.LBB5_20
# BB#15:
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	4400(%rbp,%rax), %rcx
	movq	40(%rbp), %rdi
	movq	56(%rbp), %rsi
	movl	$9, (%rsp)
	movl	$8, %r8d
	movl	%r14d, %edx
	movl	%ebx, %r9d
	callq	pc_block
	movl	$255, (%rsp)
	leaq	144(%rsp), %r15
	movl	$10, %esi
	movl	$40, %edx
	movl	$14, %ecx
	movq	%r15, %rdi
	movl	%r14d, %r8d
	movl	%r14d, %r9d
	callq	set_ag_params
	movq	56(%rbp), %rsi
	leaq	72(%rsp), %r9
	movq	%r15, %rdi
	movq	%r12, %rdx
	movl	%r14d, %ecx
	movl	%ebx, %r8d
	callq	dyn_comp
	testl	%eax, %eax
	jne	.LBB5_20
# BB#16:                                # %._crit_edge
	xorl	%eax, %eax
	cmpl	%r14d, 40(%rsp)         # 4-byte Folded Reload
	setne	%al
	shll	$5, %eax
	leal	320(%rax), %ecx
	addl	76(%rsp), %ecx
	addl	72(%rsp), %ecx
	leal	(%r14,%r14), %edx
	movl	44(%rsp), %esi          # 4-byte Reload
	imull	%edx, %esi
	movq	80(%rsp), %rdi          # 8-byte Reload
	testl	%edi, %edi
	cmovel	%edi, %esi
	addl	%ecx, %esi
	movswl	8(%rbp), %ecx
	imull	%edx, %ecx
	leal	16(%rax,%rcx), %ebx
	cmpl	%ebx, %esi
	jae	.LBB5_19
# BB#17:
	movq	%r12, %rdi
	callq	BitBufferGetPosition
	movl	%eax, %r15d
	leaq	112(%rsp), %rdi
	callq	BitBufferGetPosition
	subl	%eax, %r15d
	xorl	%eax, %eax
	cmpl	%ebx, %r15d
	jb	.LBB5_20
# BB#18:                                # %.thread
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	movl	%ebx, %edx
	callq	printf
.LBB5_19:                               # %.critedge
	movq	128(%rsp), %rax
	movq	%rax, 16(%r12)
	movaps	112(%rsp), %xmm0
	movups	%xmm0, (%r12)
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	%r13d, %ecx
	movl	%r14d, %r8d
	callq	_ZN11ALACEncoder18EncodeStereoEscapeEP9BitBufferPvjj
	xorl	%eax, %eax
.LBB5_20:
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN11ALACEncoder16EncodeStereoFastEP9BitBufferPvjjj, .Lfunc_end5-_ZN11ALACEncoder16EncodeStereoFastEP9BitBufferPvjjj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_4
	.quad	.LBB5_5
	.quad	.LBB5_6
	.quad	.LBB5_8
	.quad	.LBB5_7

	.text
	.globl	_ZN11ALACEncoder10EncodeMonoEP9BitBufferPvjjj
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder10EncodeMonoEP9BitBufferPvjjj,@function
_ZN11ALACEncoder10EncodeMonoEP9BitBufferPvjjj: # @_ZN11ALACEncoder10EncodeMonoEP9BitBufferPvjjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 272
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rsi, %r12
	movq	16(%r12), %rax
	movq	%rax, 144(%rsp)
	movdqu	(%r12), %xmm0
	movdqa	%xmm0, 128(%rsp)
	movswl	8(%rdi), %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leal	-16(%rax), %ebp
	movl	%ebp, %esi
	rolw	$14, %si
	movl	$-50, %eax
	movzwl	%si, %esi
	cmpl	$4, %esi
	ja	.LBB6_80
# BB#1:
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	cmpl	$3, %esi
	je	.LBB6_80
# BB#2:
	movl	%r8d, %ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rsi          # 8-byte Reload
	movzwl	%si, %ecx
	xorl	%edx, %edx
	cmpl	$23, %esi
	setg	%dl
	cmpl	$32, %ecx
	movl	$2, %ecx
	cmovnel	%edx, %ecx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leal	(,%rcx,8), %ebx
	movl	$1, %r15d
	movl	%ebx, %ecx
	shlq	%cl, %r15
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	8284(%r14), %ecx
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	roll	$30, %ebp
	cmpl	$4, %ebp
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	ja	.LBB6_46
# BB#3:
	decl	%r15d
	jmpq	*.LJTI6_0(,%rbp,8)
.LBB6_4:
	testl	%r13d, %r13d
	je	.LBB6_46
# BB#5:                                 # %.lr.ph265
	movq	32(%r14), %r10
	movl	%r13d, %ebp
	cmpl	$8, %r13d
	jb	.LBB6_18
# BB#6:                                 # %min.iters.checked365
	movl	%r13d, %r8d
	andl	$7, %r8d
	movq	%rbp, %rsi
	subq	%r8, %rsi
	je	.LBB6_18
# BB#7:                                 # %vector.scevcheck371
	movq	24(%rsp), %rdx          # 8-byte Reload
	cmpl	$1, %edx
	jne	.LBB6_18
# BB#8:                                 # %vector.body361.preheader
	movl	%esi, %ecx
	imull	%edx, %ecx
	leaq	16(%r10), %rax
	leal	(,%rdx,8), %r9d
	xorl	%edi, %edi
	movq	%rsi, %rbx
	movq	32(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_9:                                # %vector.body361
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %edx
	movq	(%r11,%rdx,2), %xmm0    # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	movq	8(%r11,%rdx,2), %xmm1   # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movdqu	%xmm0, -16(%rax)
	movdqu	%xmm1, (%rax)
	addq	$32, %rax
	addl	%r9d, %edi
	addq	$-8, %rbx
	jne	.LBB6_9
# BB#10:                                # %middle.block362
	testl	%r8d, %r8d
	movl	20(%rsp), %ebx          # 4-byte Reload
	jne	.LBB6_19
	jmp	.LBB6_46
.LBB6_11:
	movq	32(%r14), %rdx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r13d, %ecx
	callq	copy20ToPredictor
	jmp	.LBB6_46
.LBB6_12:
	movq	32(%r14), %rdx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r13d, %ecx
	callq	copy24ToPredictor
	testl	%r13d, %r13d
	je	.LBB6_46
# BB#13:                                # %.lr.ph268
	movq	32(%r14), %r8
	movq	64(%r14), %r9
	movl	%r13d, %eax
	cmpl	$8, %r13d
	jb	.LBB6_34
# BB#25:                                # %min.iters.checked342
	movl	%r13d, %edi
	andl	$7, %edi
	movq	%rax, %rdx
	subq	%rdi, %rdx
	je	.LBB6_34
# BB#26:                                # %vector.ph346
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movd	%ebx, %xmm2
	leaq	16(%r8), %rbp
	leaq	8(%r9), %rcx
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB6_27:                               # %vector.body338
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	movdqa	%xmm2, %xmm4
	pand	%xmm0, %xmm4
	movdqa	%xmm3, %xmm5
	pand	%xmm0, %xmm5
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	movq	%xmm4, -8(%rcx)
	pshuflw	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	movq	%xmm4, (%rcx)
	psrad	%xmm1, %xmm2
	psrad	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rbp)
	movdqu	%xmm3, (%rbp)
	addq	$32, %rbp
	addq	$16, %rcx
	addq	$-8, %rsi
	jne	.LBB6_27
# BB#28:                                # %middle.block339
	testl	%edi, %edi
	jne	.LBB6_35
	jmp	.LBB6_46
.LBB6_15:
	testl	%r13d, %r13d
	je	.LBB6_46
# BB#16:                                # %.lr.ph272
	movq	32(%r14), %rbp
	movq	64(%r14), %r8
	movl	%r13d, %r9d
	cmpl	$8, %r13d
	jae	.LBB6_29
# BB#17:
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.LBB6_41
.LBB6_18:
	xorl	%esi, %esi
	xorl	%ecx, %ecx
.LBB6_19:                               # %scalar.ph363.preheader
	movl	%ebp, %eax
	subl	%esi, %eax
	leaq	-1(%rbp), %r8
	subq	%rsi, %r8
	andq	$3, %rax
	je	.LBB6_22
# BB#20:                                # %scalar.ph363.prol.preheader
	negq	%rax
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_21:                               # %scalar.ph363.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	movswl	(%rdi,%rdx,2), %edx
	movl	%edx, (%r10,%rsi,4)
	incq	%rsi
	addl	%ebx, %ecx
	incq	%rax
	jne	.LBB6_21
.LBB6_22:                               # %scalar.ph363.prol.loopexit
	cmpq	$3, %r8
	movl	20(%rsp), %ebx          # 4-byte Reload
	jb	.LBB6_46
# BB#23:                                # %scalar.ph363.preheader.new
	subq	%rsi, %rbp
	leaq	12(%r10,%rsi,4), %rdx
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rax,2), %r8d
	leal	(%rax,%rax), %r9d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_24:                               # %scalar.ph363
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %eax
	movswl	(%rdi,%rax,2), %eax
	movl	%eax, -12(%rdx)
	leal	(%rcx,%rsi), %eax
	movswl	(%rdi,%rax,2), %ebx
	movl	%ebx, -8(%rdx)
	addl	%esi, %eax
	leal	(%r9,%rcx), %ebx
	movswl	(%rdi,%rbx,2), %ebx
	movl	%ebx, -4(%rdx)
	movl	20(%rsp), %ebx          # 4-byte Reload
	addl	%esi, %eax
	addl	%r8d, %ecx
	movswl	(%rdi,%rcx,2), %ecx
	movl	%ecx, (%rdx)
	addl	%esi, %eax
	addq	$16, %rdx
	addq	$-4, %rbp
	movl	%eax, %ecx
	jne	.LBB6_24
	jmp	.LBB6_46
.LBB6_34:
	xorl	%edx, %edx
.LBB6_35:                               # %scalar.ph340.preheader
	leaq	(%r8,%rdx,4), %rsi
	leaq	(%r9,%rdx,2), %rdi
	subq	%rdx, %rax
	.p2align	4, 0x90
.LBB6_36:                               # %scalar.ph340
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	movl	%edx, %ecx
	andl	%r15d, %ecx
	movw	%cx, (%rdi)
	movl	%ebx, %ecx
	sarl	%cl, %edx
	movl	%edx, (%rsi)
	addq	$4, %rsi
	addq	$2, %rdi
	decq	%rax
	jne	.LBB6_36
	jmp	.LBB6_46
.LBB6_29:                               # %min.iters.checked
	movl	%r13d, %r11d
	andl	$7, %r11d
	xorl	%edi, %edi
	movq	%r9, %r10
	subq	%r11, %r10
	je	.LBB6_37
# BB#30:                                # %min.iters.checked
	cmpl	$1, 24(%rsp)            # 4-byte Folded Reload
	movl	$0, %edx
	jne	.LBB6_41
# BB#31:                                # %vector.memcheck
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	32(%rsp), %r8           # 8-byte Reload
	leaq	(%r8,%r9,4), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB6_38
# BB#32:                                # %vector.memcheck
	leaq	(%rbp,%r9,4), %rcx
	cmpq	%r8, %rcx
	jbe	.LBB6_38
# BB#33:
	xorl	%edi, %edi
	xorl	%edx, %edx
	movq	48(%rsp), %r8           # 8-byte Reload
	jmp	.LBB6_41
.LBB6_37:
	xorl	%edx, %edx
	jmp	.LBB6_41
.LBB6_38:                               # %vector.ph
	movl	%r10d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
	imull	%ecx, %edx
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movd	%ebx, %xmm2
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rdi
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	leaq	16(%rbp), %rsi
	leal	(,%rcx,8), %r14d
	xorl	%ecx, %ecx
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movq	%r10, %rbp
	.p2align	4, 0x90
.LBB6_39:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebx
	movdqu	(%r8,%rbx,4), %xmm2
	movdqu	16(%r8,%rbx,4), %xmm3
	movdqa	%xmm2, %xmm4
	pand	%xmm0, %xmm4
	movdqa	%xmm3, %xmm5
	pand	%xmm0, %xmm5
	pshuflw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	movq	%xmm4, -8(%rdi)
	pshuflw	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	movq	%xmm4, (%rdi)
	psrad	%xmm1, %xmm2
	psrad	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$16, %rdi
	addq	$32, %rsi
	addl	%r14d, %ecx
	addq	$-8, %rbp
	jne	.LBB6_39
# BB#40:                                # %middle.block
	testl	%r11d, %r11d
	movq	%r10, %rdi
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	20(%rsp), %ebx          # 4-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	je	.LBB6_46
.LBB6_41:                               # %scalar.ph.preheader
	movl	%r9d, %ecx
	subl	%edi, %ecx
	leaq	-1(%r9), %rsi
	testb	$1, %cl
	jne	.LBB6_43
# BB#42:
	movq	%rdi, %rcx
	cmpq	%rdi, %rsi
	jne	.LBB6_44
	jmp	.LBB6_46
.LBB6_43:                               # %scalar.ph.prol
	movl	%edx, %ecx
	movq	%rbp, %rax
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%rcx,4), %ebp
	movl	%ebp, %ecx
	andl	%r15d, %ecx
	movw	%cx, (%r8,%rdi,2)
	movl	%ebx, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%rax,%rdi,4)
	movq	%rax, %rbp
	leaq	1(%rdi), %rcx
	addl	24(%rsp), %edx          # 4-byte Folded Reload
	cmpq	%rdi, %rsi
	je	.LBB6_46
.LBB6_44:                               # %scalar.ph.preheader.new
	subq	%rcx, %r9
	leaq	2(%r8,%rcx,2), %rsi
	leaq	4(%rbp,%rcx,4), %rdi
	.p2align	4, 0x90
.LBB6_45:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %ecx
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rcx,4), %ebp
	movl	%ebp, %ecx
	andl	%r15d, %ecx
	movw	%cx, -2(%rsi)
	movl	%ebx, %ecx
	sarl	%cl, %ebp
	movl	%ebp, -4(%rdi)
	movq	24(%rsp), %rbx          # 8-byte Reload
	addl	%ebx, %edx
	movl	(%rax,%rdx,4), %ebp
	movl	%ebp, %ecx
	andl	%r15d, %ecx
	movw	%cx, (%rsi)
	movl	20(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebp
	movl	%ebp, (%rdi)
	addl	%ebx, %edx
	movl	20(%rsp), %ebx          # 4-byte Reload
	addq	$4, %rsi
	addq	$8, %rdi
	addq	$-2, %r9
	jne	.LBB6_45
.LBB6_46:                               # %.preheader238
	movq	%r12, 120(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	subl	%ebx, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	shrl	$5, %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movl	%r13d, 108(%rsp)        # 4-byte Spill
	shrl	$3, %r13d
	movl	%r13d, 60(%rsp)         # 4-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	shlq	$9, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	176(%r14,%rax), %rbx
	movl	$4, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	$-2147483648, %edx      # imm = 0x80000000
	movl	$64, %esi
	movl	$4, %r15d
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	64(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_47:                               # =>This Inner Loop Header: Depth=1
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	72(%r13), %rsi
	movl	8288(%r13), %edx
	leaq	152(%rsp), %rax
	movq	%rax, %rdi
	callq	BitBufferInit
	movq	32(%r13), %rdi
	movq	48(%r13), %rsi
	movl	$9, (%rsp)
	movl	56(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	callq	pc_block
	movq	32(%r13), %rdi
	movq	48(%r13), %rsi
	movl	$9, (%rsp)
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	callq	pc_block
	movq	32(%r13), %rdi
	movq	48(%r13), %rsi
	movl	$9, (%rsp)
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	callq	pc_block
	movq	32(%r13), %rdi
	movq	48(%r13), %rsi
	movl	$9, (%rsp)
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	callq	pc_block
	movq	32(%r13), %rdi
	movq	48(%r13), %rsi
	movl	$9, (%rsp)
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	callq	pc_block
	movq	32(%r13), %rdi
	movq	48(%r13), %rsi
	movl	$9, (%rsp)
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	callq	pc_block
	movq	32(%r13), %rdi
	movq	48(%r13), %rsi
	movl	$9, (%rsp)
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	callq	pc_block
	movq	32(%r13), %rdi
	movq	48(%r13), %rsi
	movl	$9, (%rsp)
	movl	60(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movl	%r14d, %r9d
	callq	pc_block
	movl	$255, (%rsp)
	movl	$10, %esi
	movl	$40, %edx
	movl	$14, %ecx
	leaq	176(%rsp), %r12
	movq	%r12, %rdi
	movl	%ebp, %r8d
	movl	%ebp, %r9d
	callq	set_ag_params
	movq	48(%r13), %rsi
	movq	%r12, %rdi
	leaq	152(%rsp), %rdx
	movl	%ebp, %ecx
	movl	%r14d, %r8d
	leaq	84(%rsp), %r9
	callq	dyn_comp
	testl	%eax, %eax
	jne	.LBB6_80
# BB#48:                                #   in Loop: Header=BB6_47 Depth=1
	movl	84(%rsp), %eax
	movq	96(%rsp), %rsi          # 8-byte Reload
	leal	(%rsi,%rax,8), %eax
	movq	72(%rsp), %rdx          # 8-byte Reload
	cmpl	%edx, %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmovbl	%r15d, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	cmovbl	%eax, %edx
	addq	$4, %r15
	addl	$64, %esi
	subq	$-128, %rbx
	cmpq	$9, %r15
	jb	.LBB6_47
# BB#49:
	xorl	%ebx, %ebx
	movl	108(%rsp), %r13d        # 4-byte Reload
	movl	80(%rsp), %ebp          # 4-byte Reload
	cmpl	%r13d, %ebp
	setne	%bl
	movl	%ebx, %eax
	shll	$5, %eax
	movl	20(%rsp), %ecx          # 4-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	imull	%r13d, %ecx
	movq	112(%rsp), %r14         # 8-byte Reload
	testl	%r14d, %r14d
	cmovel	%r14d, %ecx
	addl	%eax, %ecx
	leal	32(%rdx,%rcx), %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movswl	8(%rcx), %ecx
	imull	%r13d, %ecx
	leal	16(%rax,%rcx), %ecx
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	movq	120(%rsp), %r12         # 8-byte Reload
	jb	.LBB6_51
.LBB6_50:                               # %._crit_edge308
	movl	%eax, %r15d
	shll	$3, %ebx
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill>
	movq	40(%rsp), %r14          # 8-byte Reload
	jmp	.LBB6_63
.LBB6_51:
	movl	%edx, 56(%rsp)          # 4-byte Spill
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	xorl	%esi, %esi
	movl	$12, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	leal	(,%rbx,8), %eax
	leal	(%r14,%r14), %esi
	movl	%eax, 60(%rsp)          # 4-byte Spill
	orl	%eax, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	cmpl	%r13d, %ebp
	je	.LBB6_53
# BB#52:
	movl	$32, %edx
	movq	%r12, %rdi
	movl	%r13d, %esi
	callq	BitBufferWrite
.LBB6_53:
	xorl	%esi, %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movl	$9, %esi
	movl	$8, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %esi
	orl	$128, %esi
	movl	$8, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	testl	%ebx, %ebx
	je	.LBB6_56
# BB#54:                                # %.lr.ph257
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movq	48(%rsp), %rdx          # 8-byte Reload
	leal	-1(%rdx), %ecx
	movl	%edx, %ebx
	shlq	$5, %rcx
	leaq	80(%rcx,%rax), %rbp
	.p2align	4, 0x90
.LBB6_55:                               # =>This Inner Loop Header: Depth=1
	movswl	(%rbp), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	addq	$2, %rbp
	decq	%rbx
	jne	.LBB6_55
.LBB6_56:                               # %._crit_edge
	testl	%r14d, %r14d
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	20(%rsp), %r15d         # 4-byte Reload
	je	.LBB6_60
# BB#57:                                # %._crit_edge
	testl	%r13d, %r13d
	je	.LBB6_60
# BB#58:                                # %.lr.ph255
	movl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_59:                               # =>This Inner Loop Header: Depth=1
	movq	64(%r14), %rax
	movzwl	(%rax,%rbp,2), %esi
	movq	%r12, %rdi
	movl	%r15d, %edx
	callq	BitBufferWrite
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_59
.LBB6_60:                               # %.loopexit237
	movq	32(%r14), %rdi
	movq	48(%r14), %rsi
	movq	48(%rsp), %r8           # 8-byte Reload
	leal	-1(%r8), %eax
	movq	88(%rsp), %rcx          # 8-byte Reload
	addq	%r14, %rcx
	shlq	$5, %rax
	leaq	80(%rax,%rcx), %rcx
	movl	$9, (%rsp)
	movl	%r13d, %edx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %r9d
	callq	pc_block
	leaq	176(%rsp), %rbx
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movl	%r13d, %edx
	callq	set_standard_ag_params
	movq	48(%r14), %rsi
	leaq	84(%rsp), %r9
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movl	%r13d, %ecx
	movl	%ebp, %r8d
	callq	dyn_comp
	movl	%eax, %r15d
	movq	%r12, %rdi
	callq	BitBufferGetPosition
	movl	%eax, %ebx
	leaq	128(%rsp), %rdi
	callq	BitBufferGetPosition
	subl	%eax, %ebx
	movl	72(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %ebx
	movl	80(%rsp), %ebp          # 4-byte Reload
	jae	.LBB6_62
# BB#61:
	cmpl	%edx, 56(%rsp)          # 4-byte Folded Reload
	movl	%r15d, %eax
	movq	96(%rsp), %rbx          # 8-byte Reload
	jae	.LBB6_50
	jmp	.LBB6_80
.LBB6_62:                               # %.thread
	movq	144(%rsp), %rax
	movq	%rax, 16(%r12)
	movdqa	128(%rsp), %xmm0
	movdqu	%xmm0, (%r12)
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	40(%rsp), %r14          # 8-byte Reload
	movl	60(%rsp), %ebx          # 4-byte Reload
.LBB6_63:
	xorl	%esi, %esi
	movl	$12, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	orl	$1, %ebx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	BitBufferWrite
	cmpl	%r13d, %ebp
	je	.LBB6_65
# BB#64:
	movl	$32, %edx
	movq	%r12, %rdi
	movl	%r13d, %esi
	callq	BitBufferWrite
.LBB6_65:
	movswl	8(%r14), %ecx
	addl	$-16, %ecx
	roll	$30, %ecx
	cmpl	$4, %ecx
	movl	%r15d, %eax
	ja	.LBB6_80
# BB#66:
	jmpq	*.LJTI6_1(,%rcx,8)
.LBB6_67:
	movq	24(%rsp), %r14          # 8-byte Reload
	imull	%r14d, %r13d
	testl	%r13d, %r13d
	je	.LBB6_80
# BB#68:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_69:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	movswl	(%rbp,%rax,2), %esi
	movl	$16, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	addl	%r14d, %ebx
	cmpl	%r13d, %ebx
	jb	.LBB6_69
	jmp	.LBB6_79
.LBB6_70:
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	32(%r14), %rdx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r13d, %ecx
	callq	copy20ToPredictor
	testl	%r13d, %r13d
	je	.LBB6_79
# BB#71:                                # %.lr.ph249.preheader
	movl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_72:                               # %.lr.ph249
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rax
	movl	(%rax,%rbp,4), %esi
	movl	$20, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_72
	jmp	.LBB6_79
.LBB6_73:
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	32(%r14), %rdx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r13d, %ecx
	callq	copy24ToPredictor
	testl	%r13d, %r13d
	je	.LBB6_79
# BB#74:                                # %.lr.ph251.preheader
	movl	%r13d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_75:                               # %.lr.ph251
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rax
	movl	(%rax,%rbp,4), %esi
	movl	$24, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_75
	jmp	.LBB6_79
.LBB6_76:
	movq	24(%rsp), %r14          # 8-byte Reload
	imull	%r14d, %r13d
	testl	%r13d, %r13d
	je	.LBB6_80
# BB#77:                                # %.lr.ph253.preheader
	xorl	%ebx, %ebx
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_78:                               # %.lr.ph253
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	movl	(%rbp,%rax,4), %esi
	movl	$32, %edx
	movq	%r12, %rdi
	callq	BitBufferWrite
	addl	%r14d, %ebx
	cmpl	%r13d, %ebx
	jb	.LBB6_78
.LBB6_79:
	movl	%r15d, %eax
.LBB6_80:                               # %.loopexit
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN11ALACEncoder10EncodeMonoEP9BitBufferPvjjj, .Lfunc_end6-_ZN11ALACEncoder10EncodeMonoEP9BitBufferPvjjj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_4
	.quad	.LBB6_11
	.quad	.LBB6_12
	.quad	.LBB6_46
	.quad	.LBB6_15
.LJTI6_1:
	.quad	.LBB6_67
	.quad	.LBB6_70
	.quad	.LBB6_73
	.quad	.LBB6_80
	.quad	.LBB6_76

	.text
	.globl	_ZN11ALACEncoder6EncodeE22AudioFormatDescriptionS0_PhS1_Pi
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder6EncodeE22AudioFormatDescriptionS0_PhS1_Pi,@function
_ZN11ALACEncoder6EncodeE22AudioFormatDescriptionS0_PhS1_Pi: # @_ZN11ALACEncoder6EncodeE22AudioFormatDescriptionS0_PhS1_Pi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 144
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	(%rcx), %eax
	xorl	%edx, %edx
	divl	160(%rsp)
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	8288(%r12), %edx
	leaq	32(%rsp), %rdi
	movq	%rbp, %rsi
	callq	BitBufferInit
	movl	172(%rsp), %r14d
	cmpl	$1, %r14d
	je	.LBB7_7
# BB#1:
	cmpl	$2, %r14d
	jne	.LBB7_8
# BB#2:
	leaq	32(%rsp), %rbp
	movl	$1, %esi
	movl	$3, %edx
	movq	%rbp, %rdi
	callq	BitBufferWrite
	xorl	%esi, %esi
	movl	$4, %edx
	movq	%rbp, %rdi
	callq	BitBufferWrite
	cmpb	$0, 10(%r12)
	je	.LBB7_3
# BB#4:
	leaq	32(%rsp), %rsi
	movl	$2, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%r15, %rdx
	movl	4(%rsp), %r9d           # 4-byte Reload
	callq	_ZN11ALACEncoder16EncodeStereoFastEP9BitBufferPvjjj
	testl	%eax, %eax
	jne	.LBB7_18
	jmp	.LBB7_6
.LBB7_7:
	leaq	32(%rsp), %rbp
	xorl	%esi, %esi
	movl	$3, %edx
	movq	%rbp, %rdi
	callq	BitBufferWrite
	xorl	%esi, %esi
	movl	$4, %edx
	movq	%rbp, %rdi
	callq	BitBufferWrite
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movl	4(%rsp), %r9d           # 4-byte Reload
	callq	_ZN11ALACEncoder10EncodeMonoEP9BitBufferPvjjj
	testl	%eax, %eax
	jne	.LBB7_18
	jmp	.LBB7_6
.LBB7_8:
	movswl	8(%r12), %eax
	leal	7(%rax), %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	leal	7(%rax,%rcx), %eax
	sarl	$3, %eax
	leal	-1(%r14), %ecx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	addl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_9:                                # =>This Inner Loop Header: Depth=1
	cmpl	%r14d, %ebp
	jae	.LBB7_6
# BB#10:                                #   in Loop: Header=BB7_9 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	_ZL12sChannelMaps(,%rax,4), %eax
	leal	(%rbp,%rbp,2), %ecx
	movl	$7, %r12d
	shlq	%cl, %r12
	andq	%rax, %r12
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %r12
	movl	$3, %edx
	leaq	32(%rsp), %rdi
	movl	%r12d, %esi
	callq	BitBufferWrite
	cmpl	$3, %r12d
	je	.LBB7_14
# BB#11:                                #   in Loop: Header=BB7_9 Depth=1
	cmpl	$1, %r12d
	je	.LBB7_16
# BB#12:                                #   in Loop: Header=BB7_9 Depth=1
	testl	%r12d, %r12d
	jne	.LBB7_15
# BB#13:                                #   in Loop: Header=BB7_9 Depth=1
	movl	12(%rsp), %ebx          # 4-byte Reload
	movzbl	%bl, %ebx
	movl	$4, %edx
	leaq	32(%rsp), %r14
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	BitBufferWrite
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r15, %rdx
	movl	%r14d, %ecx
	movl	%ebp, %r8d
	movl	4(%rsp), %r9d           # 4-byte Reload
	callq	_ZN11ALACEncoder10EncodeMonoEP9BitBufferPvjjj
	incl	%ebp
	incb	%bl
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_14:                               #   in Loop: Header=BB7_9 Depth=1
	movzbl	%r13b, %r13d
	movl	$4, %edx
	leaq	32(%rsp), %r14
	movq	%r14, %rdi
	movl	%r13d, %esi
	callq	BitBufferWrite
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r15, %rdx
	movl	%r14d, %ecx
	movl	%ebp, %r8d
	movl	4(%rsp), %r9d           # 4-byte Reload
	callq	_ZN11ALACEncoder10EncodeMonoEP9BitBufferPvjjj
	incl	%ebp
	incb	%r13b
	movq	56(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_16:                               #   in Loop: Header=BB7_9 Depth=1
	movl	8(%rsp), %ebx           # 4-byte Reload
	movzbl	%bl, %ebx
	movl	$4, %edx
	leaq	32(%rsp), %r14
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	BitBufferWrite
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r15, %rdx
	movl	%r14d, %ecx
	movl	%ebp, %r8d
	movl	4(%rsp), %r9d           # 4-byte Reload
	callq	_ZN11ALACEncoder12EncodeStereoEP9BitBufferPvjjj
	addl	$2, %ebp
	incb	%bl
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movq	72(%rsp), %rcx          # 8-byte Reload
.LBB7_17:                               #   in Loop: Header=BB7_9 Depth=1
	addq	%rcx, %r15
	testl	%eax, %eax
	je	.LBB7_9
	jmp	.LBB7_18
.LBB7_3:
	leaq	32(%rsp), %rsi
	movl	$2, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%r15, %rdx
	movl	4(%rsp), %r9d           # 4-byte Reload
	callq	_ZN11ALACEncoder12EncodeStereoEP9BitBufferPvjjj
	testl	%eax, %eax
	jne	.LBB7_18
.LBB7_6:                                # %.loopexit
	leaq	32(%rsp), %rbx
	movl	$7, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	BitBufferWrite
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	BitBufferByteAlign
	movq	%rbx, %rdi
	callq	BitBufferGetPosition
	shrl	$3, %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	addl	%eax, 8272(%r12)
	movl	8280(%r12), %ecx
	cmpl	%eax, %ecx
	cmoval	%ecx, %eax
	movl	%eax, 8280(%r12)
	xorl	%eax, %eax
	jmp	.LBB7_18
.LBB7_15:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	printf
	movl	$-50, %eax
.LBB7_18:                               # %.thread
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN11ALACEncoder6EncodeE22AudioFormatDescriptionS0_PhS1_Pi, .Lfunc_end7-_ZN11ALACEncoder6EncodeE22AudioFormatDescriptionS0_PhS1_Pi
	.cfi_endproc

	.globl	_ZN11ALACEncoder6FinishEv
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder6FinishEv,@function
_ZN11ALACEncoder6FinishEv:              # @_ZN11ALACEncoder6FinishEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	_ZN11ALACEncoder6FinishEv, .Lfunc_end8-_ZN11ALACEncoder6FinishEv
	.cfi_endproc

	.globl	_ZN11ALACEncoder9GetConfigER18ALACSpecificConfig
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder9GetConfigER18ALACSpecificConfig,@function
_ZN11ALACEncoder9GetConfigER18ALACSpecificConfig: # @_ZN11ALACEncoder9GetConfigER18ALACSpecificConfig
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -24
.Lcfi81:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	8284(%r14), %edi
	callq	Swap32NtoB
	movl	%eax, (%rbx)
	movb	$0, 4(%rbx)
	movb	8(%r14), %al
	movb	%al, 5(%rbx)
	movb	$40, 6(%rbx)
	movb	$14, 8(%rbx)
	movb	$10, 7(%rbx)
	movb	8292(%r14), %al
	movb	%al, 9(%rbx)
	movl	$255, %edi
	callq	Swap16NtoB
	movw	%ax, 10(%rbx)
	movl	8280(%r14), %edi
	callq	Swap32NtoB
	movl	%eax, 12(%rbx)
	movl	8276(%r14), %edi
	callq	Swap32NtoB
	movl	%eax, 16(%rbx)
	movl	8296(%r14), %edi
	callq	Swap32NtoB
	movl	%eax, 20(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN11ALACEncoder9GetConfigER18ALACSpecificConfig, .Lfunc_end9-_ZN11ALACEncoder9GetConfigER18ALACSpecificConfig
	.cfi_endproc

	.globl	_ZN11ALACEncoder18GetMagicCookieSizeEj
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder18GetMagicCookieSizeEj,@function
_ZN11ALACEncoder18GetMagicCookieSizeEj: # @_ZN11ALACEncoder18GetMagicCookieSizeEj
	.cfi_startproc
# BB#0:
	cmpl	$2, %esi
	movl	$48, %ecx
	movl	$24, %eax
	cmoval	%ecx, %eax
	retq
.Lfunc_end10:
	.size	_ZN11ALACEncoder18GetMagicCookieSizeEj, .Lfunc_end10-_ZN11ALACEncoder18GetMagicCookieSizeEj
	.cfi_endproc

	.globl	_ZN11ALACEncoder14GetMagicCookieEPvPj
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder14GetMagicCookieEPvPj,@function
_ZN11ALACEncoder14GetMagicCookieEPvPj:  # @_ZN11ALACEncoder14GetMagicCookieEPvPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 64
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	8284(%r15), %edi
	callq	Swap32NtoB
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movb	8(%r15), %al
	movb	%al, 1(%rsp)            # 1-byte Spill
	movzbl	8292(%r15), %r12d
	movl	$255, %edi
	callq	Swap16NtoB
	movw	%ax, 2(%rsp)            # 2-byte Spill
	movl	8280(%r15), %edi
	callq	Swap32NtoB
	movl	%eax, %r13d
	movl	8276(%r15), %edi
	callq	Swap32NtoB
	movl	%eax, %ebp
	movl	8296(%r15), %edi
	callq	Swap32NtoB
	movl	$24, %ecx
	xorl	%esi, %esi
	cmpb	$3, %r12b
	movl	$0, %edi
	jb	.LBB11_2
# BB#1:
	movl	_ZL21ALACChannelLayoutTags-4(,%r12,4), %edi
	movl	$48, %ecx
.LBB11_2:
	cmpl	%ecx, (%r14)
	jb	.LBB11_6
# BB#3:
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%edx, (%rbx)
	movb	$0, 4(%rbx)
	movb	1(%rsp), %dl            # 1-byte Reload
	movb	%dl, 5(%rbx)
	movb	$40, 6(%rbx)
	movb	$10, 7(%rbx)
	movb	$14, 8(%rbx)
	movb	%r12b, 9(%rbx)
	movzwl	2(%rsp), %edx           # 2-byte Folded Reload
	movw	%dx, 10(%rbx)
	movl	%r13d, 12(%rbx)
	movl	%ebp, 16(%rbx)
	movl	%eax, 20(%rbx)
	cmpb	$3, %r12b
	jb	.LBB11_5
# BB#4:
	movb	$0, 26(%rbx)
	movw	$0, 24(%rbx)
	movb	$24, 27(%rbx)
	movq	$1851877475, 28(%rbx)   # imm = 0x6E616863
	movl	%edi, 36(%rbx)
	movq	$0, 40(%rbx)
.LBB11_5:
	movl	%ecx, %esi
.LBB11_6:
	movl	%esi, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN11ALACEncoder14GetMagicCookieEPvPj, .Lfunc_end11-_ZN11ALACEncoder14GetMagicCookieEPvPj
	.cfi_endproc

	.globl	_ZN11ALACEncoder17InitializeEncoderE22AudioFormatDescription
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder17InitializeEncoderE22AudioFormatDescription,@function
_ZN11ALACEncoder17InitializeEncoderE22AudioFormatDescription: # @_ZN11ALACEncoder17InitializeEncoderE22AudioFormatDescription
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi101:
	.cfi_def_cfa_offset 80
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	cvttsd2si	80(%rsp), %rax
	movl	%eax, 8296(%r15)
	movl	108(%rsp), %edx
	movl	%edx, 8292(%r15)
	movl	92(%rsp), %ecx
	decl	%ecx
	cmpl	$3, %ecx
	ja	.LBB12_2
# BB#1:                                 # %switch.lookup
	shlb	$4, %cl
	movabsq	$9007302335266832, %rax # imm = 0x20001800140010
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %rax
	movw	%ax, 8(%r15)
.LBB12_2:                               # %.preheader24.preheader
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12(%r15)
	movl	8284(%r15), %ebx
	movl	%ebx, %eax
	movl	%edx, 4(%rsp)           # 4-byte Spill
	imull	%edx, %eax
	leal	1(%rax,%rax,4), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%eax, 8288(%r15)
	movq	%rbx, %rbp
	shlq	$2, %rbp
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 32(%r15)
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, %r12
	movq	%r12, 40(%r15)
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, %r13
	movq	%r13, 48(%r15)
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, 56(%r15)
	leal	(%rbx,%rbx), %edi
	addq	%rdi, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, %r14
	movq	%r14, 64(%r15)
	movl	$1, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	calloc
	movq	%rax, %rcx
	movq	%rcx, 72(%r15)
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movl	$-108, %eax
	je	.LBB12_14
# BB#3:                                 # %.preheader24.preheader
	testq	%r12, %r12
	je	.LBB12_14
# BB#4:                                 # %.preheader24.preheader
	testq	%r13, %r13
	je	.LBB12_14
# BB#5:                                 # %.preheader24.preheader
	testq	%rbp, %rbp
	je	.LBB12_14
# BB#6:
	testq	%rcx, %rcx
	je	.LBB12_14
# BB#7:
	testq	%r14, %r14
	je	.LBB12_14
# BB#8:                                 # %.preheader23
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jle	.LBB12_13
# BB#9:                                 # %.preheader.preheader
	leaq	4176(%r15), %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB12_10:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_11 Depth 2
	movl	$16, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB12_11:                              #   Parent Loop BB12_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-4096(%rbp), %rdi
	movl	$9, %esi
	movl	$16, %edx
	callq	init_coefs
	movl	$9, %esi
	movl	$16, %edx
	movq	%rbp, %rdi
	callq	init_coefs
	addq	$32, %rbp
	decq	%rbx
	jne	.LBB12_11
# BB#12:                                #   in Loop: Header=BB12_10 Depth=1
	incq	%r12
	movslq	8292(%r15), %rax
	addq	$512, %r14              # imm = 0x200
	cmpq	%rax, %r12
	jl	.LBB12_10
.LBB12_13:
	xorl	%eax, %eax
.LBB12_14:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN11ALACEncoder17InitializeEncoderE22AudioFormatDescription, .Lfunc_end12-_ZN11ALACEncoder17InitializeEncoderE22AudioFormatDescription
	.cfi_endproc

	.globl	_ZN11ALACEncoder15GetSourceFormatEPK22AudioFormatDescriptionPS0_
	.p2align	4, 0x90
	.type	_ZN11ALACEncoder15GetSourceFormatEPK22AudioFormatDescriptionPS0_,@function
_ZN11ALACEncoder15GetSourceFormatEPK22AudioFormatDescriptionPS0_: # @_ZN11ALACEncoder15GetSourceFormatEPK22AudioFormatDescriptionPS0_
	.cfi_startproc
# BB#0:
	cmpl	$1819304813, 8(%rsi)    # imm = 0x6C70636D
	jne	.LBB13_3
# BB#1:
	testb	$1, 12(%rsi)
	jne	.LBB13_3
# BB#2:
	movl	32(%rsi), %eax
	cmpl	$16, %eax
	ja	.LBB13_4
.LBB13_3:
	movw	$16, 8(%rdi)
	retq
.LBB13_4:
	cmpl	$20, %eax
	ja	.LBB13_6
# BB#5:
	movw	$20, 8(%rdi)
	retq
.LBB13_6:
	cmpl	$25, %eax
	movw	$24, %ax
	movw	$32, %cx
	cmovbw	%ax, %cx
	movw	%cx, 8(%rdi)
	retq
.Lfunc_end13:
	.size	_ZN11ALACEncoder15GetSourceFormatEPK22AudioFormatDescriptionPS0_, .Lfunc_end13-_ZN11ALACEncoder15GetSourceFormatEPK22AudioFormatDescriptionPS0_
	.cfi_endproc

	.type	_ZTV11ALACEncoder,@object # @_ZTV11ALACEncoder
	.section	.rodata,"a",@progbits
	.globl	_ZTV11ALACEncoder
	.p2align	3
_ZTV11ALACEncoder:
	.quad	0
	.quad	_ZTI11ALACEncoder
	.quad	_ZN11ALACEncoderD2Ev
	.quad	_ZN11ALACEncoderD0Ev
	.quad	_ZN11ALACEncoder6EncodeE22AudioFormatDescriptionS0_PhS1_Pi
	.quad	_ZN11ALACEncoder6FinishEv
	.quad	_ZN11ALACEncoder17InitializeEncoderE22AudioFormatDescription
	.quad	_ZN11ALACEncoder15GetSourceFormatEPK22AudioFormatDescriptionPS0_
	.size	_ZTV11ALACEncoder, 64

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"compressed frame too big: %u vs. %u \n"
	.size	.L.str, 38

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"compressed frame too big: %u vs. %u\n"
	.size	.L.str.1, 37

	.type	_ZL12sChannelMaps,@object # @_ZL12sChannelMaps
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
_ZL12sChannelMaps:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	520                     # 0x208
	.long	520                     # 0x208
	.long	520                     # 0x208
	.long	33288                   # 0x8208
	.size	_ZL12sChannelMaps, 32

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"That ain't right! (%u)\n"
	.size	.L.str.2, 24

	.type	.L_ZZN11ALACEncoder14GetMagicCookieEPvPjE14theChannelAtom,@object # @_ZZN11ALACEncoder14GetMagicCookieEPvPjE14theChannelAtom
	.section	.rodata,"a",@progbits
.L_ZZN11ALACEncoder14GetMagicCookieEPvPjE14theChannelAtom:
	.asciz	"\000\000\000\000chan\000\000\000"
	.size	.L_ZZN11ALACEncoder14GetMagicCookieEPvPjE14theChannelAtom, 12

	.type	_ZL21ALACChannelLayoutTags,@object # @_ZL21ALACChannelLayoutTags
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
_ZL21ALACChannelLayoutTags:
	.long	6553601                 # 0x640001
	.long	6619138                 # 0x650002
	.long	7405571                 # 0x710003
	.long	7602180                 # 0x740004
	.long	7864325                 # 0x780005
	.long	8126470                 # 0x7c0006
	.long	9306119                 # 0x8e0007
	.long	8323080                 # 0x7f0008
	.size	_ZL21ALACChannelLayoutTags, 32

	.type	_ZTS11ALACEncoder,@object # @_ZTS11ALACEncoder
	.section	.rodata,"a",@progbits
	.globl	_ZTS11ALACEncoder
_ZTS11ALACEncoder:
	.asciz	"11ALACEncoder"
	.size	_ZTS11ALACEncoder, 14

	.type	_ZTI11ALACEncoder,@object # @_ZTI11ALACEncoder
	.globl	_ZTI11ALACEncoder
	.p2align	3
_ZTI11ALACEncoder:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS11ALACEncoder
	.size	_ZTI11ALACEncoder, 16


	.globl	_ZN11ALACEncoderC1Ev
	.type	_ZN11ALACEncoderC1Ev,@function
_ZN11ALACEncoderC1Ev = _ZN11ALACEncoderC2Ev
	.globl	_ZN11ALACEncoderD1Ev
	.type	_ZN11ALACEncoderD1Ev,@function
_ZN11ALACEncoderD1Ev = _ZN11ALACEncoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
