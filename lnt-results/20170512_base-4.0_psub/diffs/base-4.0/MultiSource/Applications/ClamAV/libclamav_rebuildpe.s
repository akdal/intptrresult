	.text
	.file	"libclamav_rebuildpe.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	511                     # 0x1ff
	.long	511                     # 0x1ff
	.long	511                     # 0x1ff
	.long	511                     # 0x1ff
.LCPI0_1:
	.long	512                     # 0x200
	.long	512                     # 0x200
	.long	512                     # 0x200
	.long	512                     # 0x200
.LCPI0_2:
	.long	4294966784              # 0xfffffe00
	.long	4294966784              # 0xfffffe00
	.long	4294966784              # 0xfffffe00
	.long	4294966784              # 0xfffffe00
	.text
	.globl	cli_rebuildpe
	.p2align	4, 0x90
	.type	cli_rebuildpe,@function
cli_rebuildpe:                          # @cli_rebuildpe
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, %r13
	movq	%rdi, %r15
	leal	(,%rdx,8), %esi
	leal	(%rsi,%rsi,4), %eax
	leal	456(%rsi,%rsi,4), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	shrl	$23, %esi
	leal	456(%rsi,%rax), %r14d
	sarl	$9, %r14d
	andl	$504, %edi              # imm = 0x1F8
	cmpl	$1, %edi
	sbbl	$-1, %r14d
	movl	%r14d, %esi
	shll	$9, %esi
	movl	(%r13), %r11d
	shrl	$3, %r14d
	movl	%esi, %edi
	andl	$3584, %edi             # imm = 0xE00
	cmpl	$1, %edi
	sbbl	$-1, %r14d
	shll	$12, %r14d
	xorl	%r12d, %r12d
	cmpl	%r14d, %r11d
	seta	%r12b
	jbe	.LBB0_2
# BB#1:
	leal	496(%rax), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	shrl	$23, %esi
	leal	496(%rax,%rsi), %esi
	shrl	$9, %esi
	andl	$504, %edi              # imm = 0x1F8
	cmpl	$1, %edi
	sbbl	$-1, %esi
	shll	$9, %esi
.LBB0_2:
	movl	%esi, 12(%rsp)          # 4-byte Spill
	addl	%edx, %r12d
	xorl	%ebx, %ebx
	cmpl	$96, %r12d
	jg	.LBB0_22
# BB#3:                                 # %.preheader116
	xorl	%edi, %edi
	testl	%edx, %edx
	jle	.LBB0_13
# BB#4:                                 # %.lr.ph124.preheader
	movl	%edx, %eax
	cmpl	$8, %edx
	jb	.LBB0_5
# BB#6:                                 # %min.iters.checked
	movl	%edx, %ebx
	andl	$7, %ebx
	movq	%rax, %rbp
	subq	%rbx, %rbp
	je	.LBB0_5
# BB#7:                                 # %vector.body.preheader
	movl	%r8d, %r10d
	leaq	264(%r13), %rsi
	pxor	%xmm8, %xmm8
	movdqa	.LCPI0_0(%rip), %xmm9   # xmm9 = [511,511,511,511]
	movdqa	.LCPI0_1(%rip), %xmm3   # xmm3 = [512,512,512,512]
	movdqa	.LCPI0_2(%rip), %xmm4   # xmm4 = [4294966784,4294966784,4294966784,4294966784]
	movq	%rbp, %rdi
	pxor	%xmm5, %xmm5
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB0_8:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movd	-144(%rsi), %xmm6       # xmm6 = mem[0],zero,zero,zero
	movd	-216(%rsi), %xmm7       # xmm7 = mem[0],zero,zero,zero
	punpckldq	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	movd	-180(%rsi), %xmm6       # xmm6 = mem[0],zero,zero,zero
	movd	-252(%rsi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	punpckldq	%xmm6, %xmm0    # xmm0 = xmm0[0],xmm6[0],xmm0[1],xmm6[1]
	punpckldq	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0],xmm0[1],xmm7[1]
	movd	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movd	-72(%rsi), %xmm7        # xmm7 = mem[0],zero,zero,zero
	punpckldq	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	movd	-36(%rsi), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movd	-108(%rsi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	punpckldq	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0],xmm1[1],xmm6[1]
	punpckldq	%xmm7, %xmm1    # xmm1 = xmm1[0],xmm7[0],xmm1[1],xmm7[1]
	movdqa	%xmm0, %xmm6
	pand	%xmm9, %xmm6
	movdqa	%xmm1, %xmm7
	pand	%xmm9, %xmm7
	pcmpeqd	%xmm8, %xmm6
	pcmpeqd	%xmm8, %xmm7
	pandn	%xmm3, %xmm6
	pandn	%xmm3, %xmm7
	paddd	%xmm0, %xmm6
	paddd	%xmm1, %xmm7
	pand	%xmm4, %xmm6
	pand	%xmm4, %xmm7
	paddd	%xmm6, %xmm5
	paddd	%xmm7, %xmm2
	addq	$288, %rsi              # imm = 0x120
	addq	$-8, %rdi
	jne	.LBB0_8
# BB#9:                                 # %middle.block
	paddd	%xmm5, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	paddd	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edi
	testl	%ebx, %ebx
	jne	.LBB0_10
	jmp	.LBB0_12
.LBB0_5:
	movl	%r8d, %r10d
	xorl	%ebp, %ebp
	xorl	%edi, %edi
.LBB0_10:                               # %.lr.ph124.preheader143
	leaq	(%rbp,%rbp,8), %rsi
	leaq	12(%r13,%rsi,4), %rbx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph124
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ebp
	xorl	%esi, %esi
	testw	$511, %bp               # imm = 0x1FF
	setne	%sil
	shll	$9, %esi
	addl	%ebp, %esi
	andl	$-512, %esi             # imm = 0xFE00
	addl	%esi, %edi
	addq	$36, %rbx
	decq	%rax
	jne	.LBB0_11
.LBB0_12:                               # %._crit_edge125
	xorl	%ebx, %ebx
	cmpl	$184549376, %edi        # imm = 0xB000000
	movl	%r10d, %r8d
	ja	.LBB0_22
.LBB0_13:                               # %._crit_edge125.thread
	addl	12(%rsp), %edi          # 4-byte Folded Reload
	movl	$1, %esi
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	%r9d, %ebx
	movl	%r12d, 16(%rsp)         # 4-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	%ecx, %r15d
	movl	%r11d, %ebp
	movl	%r8d, %r12d
	callq	cli_calloc
	movl	%ebp, %r9d
	movl	%ebx, %r10d
	movq	%rax, %rbp
	testq	%rbp, %rbp
	movl	$0, %ebx
	je	.LBB0_22
# BB#14:
	movl	$.L.str, %esi
	movl	$328, %edx              # imm = 0x148
	movq	%rbp, %rdi
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	movl	%r15d, 28(%rsp)         # 4-byte Spill
	movl	%r9d, 36(%rsp)          # 4-byte Spill
	callq	memcpy
	movq	%rbp, %rbx
	xorl	%r15d, %r15d
	movl	12(%rsp), %eax          # 4-byte Reload
	testb	$14, %ah
	setne	%r15b
	shll	$12, %r15d
	addl	%eax, %r15d
	andl	$-4096, %r15d           # imm = 0xF000
	movl	16(%rsp), %ecx          # 4-byte Reload
	movw	%cx, 214(%rbx)
	movl	%r12d, 248(%rbx)
	movl	28(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 260(%rbx)
	movl	%eax, 292(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 440(%rbx)
	movdqu	%xmm0, 424(%rbx)
	movdqu	%xmm0, 408(%rbx)
	movdqu	%xmm0, 392(%rbx)
	movdqu	%xmm0, 376(%rbx)
	movdqu	%xmm0, 360(%rbx)
	movdqu	%xmm0, 344(%rbx)
	movdqu	%xmm0, 328(%rbx)
	movl	32(%rsp), %eax          # 4-byte Reload
	movl	%eax, 344(%rbx)
	movl	112(%rsp), %eax
	movl	%eax, 348(%rbx)
	leaq	456(%rbx), %rbp
	cmpl	%r14d, 36(%rsp)         # 4-byte Folded Reload
	jbe	.LBB0_15
# BB#16:
	movl	$8, %esi
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	snprintf
	movl	(%r13), %eax
	subl	%r15d, %eax
	movl	%eax, 464(%rbx)
	movl	%r15d, 468(%rbx)
	movl	$-1, 492(%rbx)
	movq	%rbx, %r14
	leaq	496(%rbx), %rbp
	movl	(%r13), %eax
	subl	%r15d, %eax
	xorl	%ecx, %ecx
	testw	$4095, %ax              # imm = 0xFFF
	setne	%cl
	shll	$12, %ecx
	addl	%eax, %ecx
	andl	$-4096, %ecx            # imm = 0xF000
	addl	%ecx, %r15d
	jmp	.LBB0_17
.LBB0_15:
	movq	%rbx, %r14
.LBB0_17:                               # %.preheader
	movq	40(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB0_18
# BB#19:                                # %.lr.ph.preheader
	movl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addq	$12, %r13
	xorl	%ebx, %ebx
	movl	12(%rsp), %r12d         # 4-byte Reload
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incq	%rbx
	movl	$8, %esi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movl	%ebx, %ecx
	callq	snprintf
	movl	-8(%r13), %eax
	movl	%eax, 8(%rbp)
	movl	-12(%r13), %eax
	movl	%eax, 12(%rbp)
	movl	(%r13), %eax
	movl	%eax, 16(%rbp)
	movl	%r12d, 20(%rbp)
	movl	$-1, 36(%rbp)
	movl	%r12d, %edi
	addq	%r14, %rdi
	movl	-4(%r13), %esi
	movl	(%r13), %edx
	addq	48(%rsp), %rsi          # 8-byte Folded Reload
	callq	memcpy
	movl	-8(%r13), %eax
	movl	(%r13), %ecx
	xorl	%edx, %edx
	testw	$511, %cx               # imm = 0x1FF
	setne	%dl
	shll	$9, %edx
	addl	%ecx, %edx
	andl	$-512, %edx             # imm = 0xFE00
	addl	%edx, %r12d
	xorl	%ecx, %ecx
	testw	$4095, %ax              # imm = 0xFFF
	setne	%cl
	shll	$12, %ecx
	addl	%eax, %ecx
	andl	$-4096, %ecx            # imm = 0xF000
	addl	%ecx, %r15d
	addq	$36, %r13
	addq	$40, %rbp
	cmpq	%rbx, 16(%rsp)          # 8-byte Folded Reload
	jne	.LBB0_20
	jmp	.LBB0_21
.LBB0_18:
	movl	12(%rsp), %r12d         # 4-byte Reload
.LBB0_21:                               # %._crit_edge
	movl	%r15d, 288(%r14)
	movl	120(%rsp), %edi
	movq	%r14, %rsi
	movl	%r12d, %edx
	callq	cli_writen
	xorl	%ebx, %ebx
	cmpl	$-1, %eax
	setne	%bl
	movq	%r14, %rdi
	callq	free
.LBB0_22:
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_rebuildpe, .Lfunc_end0-cli_rebuildpe
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata,"a",@progbits
.L.str:
	.asciz	"MZ\220\000\002\000\000\000\004\000\017\000\377\377\000\000\260\000\000\000\000\000\000\000@\000\032\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\320\000\000\000\016\037\264\t\272\r\000\315!\264L\315!This file was created by ClamAV for internal use and should not be run.\r\nClamAV - A GPL virus scanner - http://www.clamav.net\r\n$\000\000\000PE\000\000L\001\377\377CLAM\000\000\000\000\000\000\000\000\340\000\203\217\013\001\000\000\000\020\000\000\000\020\000\000\000\000\000\000\377\377\377\377\000\020\000\000\000\020\000\000\377\377\377\377\000\020\000\000\000\002\000\000\001\000\000\000\000\000\000\000\003\000\n\000\000\000\000\000\000\020\000\000\000\004\000\000\000\000\000\000\002\000\000\000\000\000\020\000\000\020\000\000\000\000\020\000\000\020\000\000\000\000\000\000\020\000\000\000"
	.size	.L.str, 329

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"empty"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	".clam%.2d"
	.size	.L.str.2, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
