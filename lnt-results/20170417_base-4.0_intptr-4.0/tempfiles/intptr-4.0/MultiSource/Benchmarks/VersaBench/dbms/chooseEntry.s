	.text
	.file	"chooseEntry.bc"
	.globl	chooseEntry
	.p2align	4, 0x90
	.type	chooseEntry,@function
chooseEntry:                            # @chooseEntry
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$112, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 144
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	8(%rdi), %r15
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	32(%r14), %xmm2
	movups	%xmm2, 80(%rsp)
	movups	%xmm1, 64(%rsp)
	movups	%xmm0, 48(%rsp)
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movups	32(%r15), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	callq	penalty
	movq	40(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB0_3
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	%xmm0, 108(%rsp)        # 4-byte Spill
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	32(%r14), %xmm2
	movups	%xmm2, 80(%rsp)
	movups	%xmm1, 64(%rsp)
	movups	%xmm0, 48(%rsp)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	callq	penalty
	movss	108(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	minss	%xmm1, %xmm0
	cmovaq	%rbx, %r15
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_1
.LBB0_3:                                # %._crit_edge
	movq	%r15, %rax
	addq	$112, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	chooseEntry, .Lfunc_end0-chooseEntry
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
