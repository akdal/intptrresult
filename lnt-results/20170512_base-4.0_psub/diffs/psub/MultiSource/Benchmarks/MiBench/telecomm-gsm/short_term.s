	.text
	.file	"short_term.bc"
	.globl	Gsm_Short_Term_Analysis_Filter
	.p2align	4, 0x90
	.type	Gsm_Short_Term_Analysis_Filter,@function
Gsm_Short_Term_Analysis_Filter:         # @Gsm_Short_Term_Analysis_Filter
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	movswq	628(%r15), %rax
	movq	%rax, %rcx
	shlq	$4, %rcx
	leaq	596(%r15,%rcx), %rbx
	movl	%eax, %ecx
	xorl	$1, %ecx
	movw	%cx, 628(%r15)
	xorq	$1, %rax
	shlq	$4, %rax
	leaq	596(%r15,%rax), %r12
	movq	%rsi, %rdi
	movq	%rbx, %rsi
	callq	Decoding_of_the_coded_Log_Area_Ratios
	movq	%rbx, %rsi
	xorl	%eax, %eax
	movl	$32767, %r8d            # imm = 0x7FFF
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movswl	(%r12,%rax,2), %edx
	movl	%edx, %ecx
	sarl	$2, %ecx
	movswl	(%rsi,%rax,2), %edi
	sarl	$2, %edi
	addl	%ecx, %edi
	movslq	%edi, %rbp
	leaq	32768(%rbp), %rdi
	testl	%ebp, %ebp
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%r8, %rcx
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbeq	%rbp, %rcx
	sarl	%edx
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx), %rdi
	leaq	32768(%rcx,%rdx), %rcx
	xorl	%edx, %edx
	testq	%rdi, %rdi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%di, %dx
	movw	%dx, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB0_1
# BB#2:                                 # %Coefficients_0_12.exit.preheader
	xorl	%eax, %eax
	movw	$32767, %cx             # imm = 0x7FFF
	movl	$32767, %r8d            # imm = 0x7FFF
	jmp	.LBB0_3
.LBB0_13:                               #   in Loop: Header=BB0_3 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rbp
	leaq	58880(%rbp), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB0_17
# BB#14:                                #   in Loop: Header=BB0_3 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rbp           # imm = 0x8000
	cmovgq	%r8, %rbp
	jmp	.LBB0_20
.LBB0_15:                               #   in Loop: Header=BB0_3 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rbp
	leaq	58880(%rbp), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB0_18
# BB#16:                                #   in Loop: Header=BB0_3 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rbp           # imm = 0x8000
	cmovgq	%r8, %rbp
	jmp	.LBB0_19
.LBB0_17:                               #   in Loop: Header=BB0_3 Depth=1
	addq	$26112, %rbp            # imm = 0x6600
	jmp	.LBB0_20
.LBB0_18:                               #   in Loop: Header=BB0_3 Depth=1
	addq	$26112, %rbp            # imm = 0x6600
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_3:                                # %Coefficients_0_12.exit
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rsp,%rax,2), %edi
	testl	%edi, %edi
	movslq	%edi, %rbp
	js	.LBB0_6
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB0_9
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	addq	%rbp, %rbp
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_3 Depth=1
	movzwl	%bp, %edx
	negl	%edi
	cmpl	$32768, %edx            # imm = 0x8000
	cmovew	%cx, %di
	movswl	%di, %edi
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB0_11
# BB#7:                                 #   in Loop: Header=BB0_3 Depth=1
	addl	%edi, %edi
	jmp	.LBB0_8
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_3 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB0_13
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	addq	$11059, %rbp            # imm = 0x2B33
	jmp	.LBB0_20
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_3 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
.LBB0_8:                                #   in Loop: Header=BB0_3 Depth=1
	movslq	%edi, %rbp
.LBB0_19:                               #   in Loop: Header=BB0_3 Depth=1
	negq	%rbp
.LBB0_20:                               #   in Loop: Header=BB0_3 Depth=1
	movw	%bp, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB0_3
# BB#21:                                # %.lr.ph.i80.preheader
	movl	$13, %r8d
	movabsq	$140737488355328, %r13  # imm = 0x800000000000
	movq	%r14, %r9
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph.i80
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_23 Depth 2
	movw	(%r9), %bx
	xorl	%edx, %edx
	movw	%bx, %ax
	.p2align	4, 0x90
.LBB0_23:                               #   Parent Loop BB0_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	580(%r15,%rdx,2), %ecx
	movswl	(%rsp,%rdx,2), %r10d
	movw	%bx, 580(%r15,%rdx,2)
	movswq	%ax, %rdi
	movslq	%ecx, %rax
	movswl	%di, %ecx
	imull	%r10d, %ecx
	shlq	$33, %rcx
	addq	%r13, %rcx
	sarq	$48, %rcx
	leaq	32768(%rcx,%rax), %rbp
	addq	%rax, %rcx
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	setle	%bl
	addl	$32767, %ebx            # imm = 0x7FFF
	cmpq	$65535, %rbp            # imm = 0xFFFF
	cmovbew	%cx, %bx
	imull	%r10d, %eax
	shlq	$33, %rax
	addq	%r13, %rax
	sarq	$48, %rax
	leaq	(%rax,%rdi), %rcx
	leaq	32768(%rax,%rdi), %rdi
	xorl	%eax, %eax
	testq	%rcx, %rcx
	setle	%al
	addl	$32767, %eax            # imm = 0x7FFF
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbew	%cx, %ax
	incq	%rdx
	cmpq	$8, %rdx
	jne	.LBB0_23
# BB#24:                                #   in Loop: Header=BB0_22 Depth=1
	decl	%r8d
	movw	%ax, (%r9)
	leaq	2(%r9), %r9
	jne	.LBB0_22
# BB#25:                                # %Short_term_analysis_filtering.exit86
	movq	%rsp, %rdx
	movq	%r12, %rdi
	movq	%rsi, %rbx
	callq	Coefficients_13_26
	xorl	%eax, %eax
	movw	$32767, %cx             # imm = 0x7FFF
	movl	$32767, %edx            # imm = 0x7FFF
	jmp	.LBB0_26
.LBB0_36:                               #   in Loop: Header=BB0_26 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rbp
	cmpq	$65536, %rbp            # imm = 0x10000
	jb	.LBB0_40
# BB#37:                                #   in Loop: Header=BB0_26 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%rdx, %rsi
	jmp	.LBB0_43
.LBB0_38:                               #   in Loop: Header=BB0_26 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rbp
	cmpq	$65536, %rbp            # imm = 0x10000
	jb	.LBB0_41
# BB#39:                                #   in Loop: Header=BB0_26 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%rdx, %rsi
	jmp	.LBB0_42
.LBB0_40:                               #   in Loop: Header=BB0_26 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB0_43
.LBB0_41:                               #   in Loop: Header=BB0_26 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB0_42
	.p2align	4, 0x90
.LBB0_26:                               # =>This Inner Loop Header: Depth=1
	movswl	(%rsp,%rax,2), %edi
	testl	%edi, %edi
	movslq	%edi, %rsi
	js	.LBB0_29
# BB#27:                                #   in Loop: Header=BB0_26 Depth=1
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB0_32
# BB#28:                                #   in Loop: Header=BB0_26 Depth=1
	addq	%rsi, %rsi
	jmp	.LBB0_43
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_26 Depth=1
	movzwl	%si, %esi
	negl	%edi
	cmpl	$32768, %esi            # imm = 0x8000
	cmovew	%cx, %di
	movswl	%di, %edi
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB0_34
# BB#30:                                #   in Loop: Header=BB0_26 Depth=1
	addl	%edi, %edi
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_32:                               #   in Loop: Header=BB0_26 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB0_36
# BB#33:                                #   in Loop: Header=BB0_26 Depth=1
	addq	$11059, %rsi            # imm = 0x2B33
	jmp	.LBB0_43
.LBB0_34:                               #   in Loop: Header=BB0_26 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB0_38
# BB#35:                                #   in Loop: Header=BB0_26 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
.LBB0_31:                               #   in Loop: Header=BB0_26 Depth=1
	movslq	%edi, %rsi
.LBB0_42:                               #   in Loop: Header=BB0_26 Depth=1
	negq	%rsi
.LBB0_43:                               #   in Loop: Header=BB0_26 Depth=1
	movw	%si, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB0_26
# BB#44:                                # %LARp_to_rp.exit68
	leaq	26(%r14), %r9
	movl	$14, %r8d
	movq	%rbx, %r10
	.p2align	4, 0x90
.LBB0_45:                               # %.lr.ph.i53
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_46 Depth 2
	movw	(%r9), %si
	xorl	%edx, %edx
	movw	%si, %cx
	.p2align	4, 0x90
.LBB0_46:                               #   Parent Loop BB0_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	580(%r15,%rdx,2), %ebp
	movswl	(%rsp,%rdx,2), %ebx
	movw	%si, 580(%r15,%rdx,2)
	movswq	%cx, %rdi
	movslq	%ebp, %rcx
	movswl	%di, %ebp
	imull	%ebx, %ebp
	shlq	$33, %rbp
	addq	%r13, %rbp
	sarq	$48, %rbp
	leaq	32768(%rbp,%rcx), %rax
	addq	%rcx, %rbp
	xorl	%esi, %esi
	testq	%rbp, %rbp
	setle	%sil
	addl	$32767, %esi            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%bp, %si
	imull	%ebx, %ecx
	shlq	$33, %rcx
	addq	%r13, %rcx
	sarq	$48, %rcx
	leaq	(%rcx,%rdi), %rax
	leaq	32768(%rcx,%rdi), %rdi
	xorl	%ecx, %ecx
	testq	%rax, %rax
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbew	%ax, %cx
	incq	%rdx
	cmpq	$8, %rdx
	jne	.LBB0_46
# BB#47:                                #   in Loop: Header=BB0_45 Depth=1
	decl	%r8d
	movw	%cx, (%r9)
	leaq	2(%r9), %r9
	jne	.LBB0_45
# BB#48:                                # %Short_term_analysis_filtering.exit59.preheader
	xorl	%eax, %eax
	movl	$32767, %ecx            # imm = 0x7FFF
	.p2align	4, 0x90
.LBB0_49:                               # %Short_term_analysis_filtering.exit59
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%r12,%rax,2), %edx
	sarl	$2, %edx
	movswl	(%r10,%rax,2), %esi
	movl	%esi, %edi
	sarl	$2, %edi
	addl	%edx, %edi
	movslq	%edi, %rdx
	leaq	32768(%rdx), %rdi
	testl	%edx, %edx
	movq	$-32768, %rbp           # imm = 0x8000
	cmovgq	%rcx, %rbp
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbeq	%rdx, %rbp
	sarl	%esi
	movslq	%esi, %rdx
	leaq	(%rbp,%rdx), %rsi
	leaq	32768(%rbp,%rdx), %rdx
	xorl	%edi, %edi
	testq	%rsi, %rsi
	setle	%dil
	addl	$32767, %edi            # imm = 0x7FFF
	cmpq	$65535, %rdx            # imm = 0xFFFF
	cmovbew	%si, %di
	movw	%di, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB0_49
# BB#50:                                # %Coefficients_27_39.exit.preheader
	xorl	%eax, %eax
	movw	$32767, %cx             # imm = 0x7FFF
	movl	$32767, %edx            # imm = 0x7FFF
	jmp	.LBB0_51
.LBB0_61:                               #   in Loop: Header=BB0_51 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rbp
	cmpq	$65536, %rbp            # imm = 0x10000
	jb	.LBB0_65
# BB#62:                                #   in Loop: Header=BB0_51 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%rdx, %rsi
	jmp	.LBB0_68
.LBB0_63:                               #   in Loop: Header=BB0_51 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rbp
	cmpq	$65536, %rbp            # imm = 0x10000
	jb	.LBB0_66
# BB#64:                                #   in Loop: Header=BB0_51 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%rdx, %rsi
	jmp	.LBB0_67
.LBB0_65:                               #   in Loop: Header=BB0_51 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB0_68
.LBB0_66:                               #   in Loop: Header=BB0_51 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB0_67
	.p2align	4, 0x90
.LBB0_51:                               # %Coefficients_27_39.exit
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rsp,%rax,2), %edi
	testl	%edi, %edi
	movslq	%edi, %rsi
	js	.LBB0_54
# BB#52:                                #   in Loop: Header=BB0_51 Depth=1
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB0_57
# BB#53:                                #   in Loop: Header=BB0_51 Depth=1
	addq	%rsi, %rsi
	jmp	.LBB0_68
	.p2align	4, 0x90
.LBB0_54:                               #   in Loop: Header=BB0_51 Depth=1
	movzwl	%si, %esi
	negl	%edi
	cmpl	$32768, %esi            # imm = 0x8000
	cmovew	%cx, %di
	movswl	%di, %edi
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB0_59
# BB#55:                                #   in Loop: Header=BB0_51 Depth=1
	addl	%edi, %edi
	jmp	.LBB0_56
	.p2align	4, 0x90
.LBB0_57:                               #   in Loop: Header=BB0_51 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB0_61
# BB#58:                                #   in Loop: Header=BB0_51 Depth=1
	addq	$11059, %rsi            # imm = 0x2B33
	jmp	.LBB0_68
.LBB0_59:                               #   in Loop: Header=BB0_51 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB0_63
# BB#60:                                #   in Loop: Header=BB0_51 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
.LBB0_56:                               #   in Loop: Header=BB0_51 Depth=1
	movslq	%edi, %rsi
.LBB0_67:                               #   in Loop: Header=BB0_51 Depth=1
	negq	%rsi
.LBB0_68:                               #   in Loop: Header=BB0_51 Depth=1
	movw	%si, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB0_51
# BB#69:                                # %LARp_to_rp.exit44
	leaq	54(%r14), %r9
	movl	$13, %r8d
	.p2align	4, 0x90
.LBB0_70:                               # %.lr.ph.i29
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_71 Depth 2
	movw	(%r9), %si
	xorl	%edx, %edx
	movw	%si, %cx
	.p2align	4, 0x90
.LBB0_71:                               #   Parent Loop BB0_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	580(%r15,%rdx,2), %ebp
	movswl	(%rsp,%rdx,2), %ebx
	movw	%si, 580(%r15,%rdx,2)
	movswq	%cx, %rdi
	movslq	%ebp, %rcx
	movswl	%di, %ebp
	imull	%ebx, %ebp
	shlq	$33, %rbp
	addq	%r13, %rbp
	sarq	$48, %rbp
	leaq	32768(%rbp,%rcx), %rax
	addq	%rcx, %rbp
	xorl	%esi, %esi
	testq	%rbp, %rbp
	setle	%sil
	addl	$32767, %esi            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%bp, %si
	imull	%ebx, %ecx
	shlq	$33, %rcx
	addq	%r13, %rcx
	sarq	$48, %rcx
	leaq	(%rcx,%rdi), %rax
	leaq	32768(%rcx,%rdi), %rdi
	xorl	%ecx, %ecx
	testq	%rax, %rax
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbew	%ax, %cx
	incq	%rdx
	cmpq	$8, %rdx
	jne	.LBB0_71
# BB#72:                                #   in Loop: Header=BB0_70 Depth=1
	decl	%r8d
	movw	%cx, (%r9)
	leaq	2(%r9), %r9
	jne	.LBB0_70
# BB#73:                                # %Short_term_analysis_filtering.exit35
	movdqu	(%r10), %xmm0
	movdqa	%xmm0, (%rsp)
	movd	%xmm0, %esi
	movl	$1, %eax
	movw	$32767, %cx             # imm = 0x7FFF
	movl	$32767, %edx            # imm = 0x7FFF
	jmp	.LBB0_75
	.p2align	4, 0x90
.LBB0_74:                               # %._crit_edge
                                        #   in Loop: Header=BB0_75 Depth=1
	movw	(%rsp,%rax,2), %si
	incq	%rax
.LBB0_75:                               # =>This Inner Loop Header: Depth=1
	movswl	%si, %edi
	testw	%di, %di
	js	.LBB0_78
# BB#76:                                #   in Loop: Header=BB0_75 Depth=1
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB0_81
# BB#77:                                #   in Loop: Header=BB0_75 Depth=1
	addl	%edi, %edi
	movslq	%edi, %rsi
	jmp	.LBB0_92
	.p2align	4, 0x90
.LBB0_78:                               #   in Loop: Header=BB0_75 Depth=1
	movzwl	%di, %esi
	negl	%edi
	cmpl	$32768, %esi            # imm = 0x8000
	cmovew	%cx, %di
	movswl	%di, %edi
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB0_83
# BB#79:                                #   in Loop: Header=BB0_75 Depth=1
	addl	%edi, %edi
	jmp	.LBB0_80
	.p2align	4, 0x90
.LBB0_81:                               #   in Loop: Header=BB0_75 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB0_85
# BB#82:                                #   in Loop: Header=BB0_75 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
	movslq	%edi, %rsi
	jmp	.LBB0_92
.LBB0_83:                               #   in Loop: Header=BB0_75 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB0_87
# BB#84:                                #   in Loop: Header=BB0_75 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
.LBB0_80:                               #   in Loop: Header=BB0_75 Depth=1
	movslq	%edi, %rsi
.LBB0_91:                               #   in Loop: Header=BB0_75 Depth=1
	negq	%rsi
.LBB0_92:                               #   in Loop: Header=BB0_75 Depth=1
	movw	%si, -2(%rsp,%rax,2)
	cmpl	$8, %eax
	jne	.LBB0_74
	jmp	.LBB0_93
.LBB0_85:                               #   in Loop: Header=BB0_75 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rbp
	cmpq	$65536, %rbp            # imm = 0x10000
	jb	.LBB0_89
# BB#86:                                #   in Loop: Header=BB0_75 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%rdx, %rsi
	jmp	.LBB0_92
.LBB0_87:                               #   in Loop: Header=BB0_75 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rbp
	cmpq	$65536, %rbp            # imm = 0x10000
	jb	.LBB0_90
# BB#88:                                #   in Loop: Header=BB0_75 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%rdx, %rsi
	jmp	.LBB0_91
.LBB0_89:                               #   in Loop: Header=BB0_75 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB0_92
.LBB0_90:                               #   in Loop: Header=BB0_75 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB0_91
.LBB0_93:                               # %LARp_to_rp.exit
	addq	$80, %r14
	movl	$120, %r8d
	.p2align	4, 0x90
.LBB0_94:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_95 Depth 2
	movw	(%r14), %dx
	xorl	%ecx, %ecx
	movw	%dx, %si
	.p2align	4, 0x90
.LBB0_95:                               #   Parent Loop BB0_94 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	580(%r15,%rcx,2), %edi
	movswl	(%rsp,%rcx,2), %ebp
	movw	%dx, 580(%r15,%rcx,2)
	movswq	%si, %rsi
	movslq	%edi, %rdi
	movswl	%si, %ebx
	imull	%ebp, %ebx
	shlq	$33, %rbx
	addq	%r13, %rbx
	sarq	$48, %rbx
	leaq	32768(%rbx,%rdi), %rax
	addq	%rdi, %rbx
	xorl	%edx, %edx
	testq	%rbx, %rbx
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%bx, %dx
	imull	%ebp, %edi
	shlq	$33, %rdi
	addq	%r13, %rdi
	sarq	$48, %rdi
	leaq	(%rdi,%rsi), %rax
	leaq	32768(%rdi,%rsi), %rdi
	xorl	%esi, %esi
	testq	%rax, %rax
	setle	%sil
	addl	$32767, %esi            # imm = 0x7FFF
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbew	%ax, %si
	incq	%rcx
	cmpq	$8, %rcx
	jne	.LBB0_95
# BB#96:                                #   in Loop: Header=BB0_94 Depth=1
	decl	%r8d
	movw	%si, (%r14)
	leaq	2(%r14), %r14
	jne	.LBB0_94
# BB#97:                                # %Short_term_analysis_filtering.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Gsm_Short_Term_Analysis_Filter, .Lfunc_end0-Gsm_Short_Term_Analysis_Filter
	.cfi_endproc

	.p2align	4, 0x90
	.type	Decoding_of_the_coded_Log_Area_Ratios,@function
Decoding_of_the_coded_Log_Area_Ratios:  # @Decoding_of_the_coded_Log_Area_Ratios
	.cfi_startproc
# BB#0:
	movabsq	$140737488355328, %r8   # imm = 0x800000000000
	movabsq	$112588272697344, %r9   # imm = 0x666600000000
	movswq	(%rdi), %rcx
	leal	32(%rcx), %edx
	xorl	%r11d, %r11d
	cmpq	$32, %rcx
	leaq	32736(%rcx), %rcx
	movw	$63, %r10w
	movl	$0, %eax
	cmovgw	%r10w, %ax
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%dx, %ax
	shll	$10, %eax
	movswq	%ax, %rax
	imulq	%r9, %rax
	addq	%r8, %rax
	sarq	$48, %rax
	leaq	32768(%rax,%rax), %rcx
	xorl	%edx, %edx
	testq	%rax, %rax
	leaq	(%rax,%rax), %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%ax, %dx
	movw	%dx, (%rsi)
	movswq	2(%rdi), %rax
	leal	32(%rax), %ecx
	cmpq	$32, %rax
	leaq	32736(%rax), %rax
	movl	$0, %edx
	cmovgw	%r10w, %dx
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%cx, %dx
	shll	$10, %edx
	movswq	%dx, %rax
	imulq	%r9, %rax
	addq	%r8, %rax
	sarq	$48, %rax
	leaq	32768(%rax,%rax), %rcx
	xorl	%edx, %edx
	testq	%rax, %rax
	leaq	(%rax,%rax), %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%ax, %dx
	movw	%dx, 2(%rsi)
	movswq	4(%rdi), %rax
	leal	48(%rax), %ecx
	cmpq	$16, %rax
	leaq	32752(%rax), %rax
	movl	$0, %edx
	cmovgw	%r10w, %dx
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%cx, %dx
	shll	$10, %edx
	movswq	%dx, %rax
	addq	$-4096, %rax            # imm = 0xF000
	cmpq	$-32769, %rax           # imm = 0xFFFF7FFF
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%rax, %rcx
	movswq	%cx, %rcx
	imulq	%r9, %rcx
	addq	%r8, %rcx
	sarq	$48, %rcx
	cmpq	$32766, %rax            # imm = 0x7FFE
	movl	$13107, %eax            # imm = 0x3333
	cmovleq	%rcx, %rax
	leaq	32768(%rax,%rax), %rcx
	xorl	%edx, %edx
	testq	%rax, %rax
	leaq	(%rax,%rax), %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%ax, %dx
	movw	%dx, 4(%rsi)
	movswq	6(%rdi), %rax
	leal	48(%rax), %ecx
	cmpq	$16, %rax
	leaq	32752(%rax), %rax
	cmovgw	%r10w, %r11w
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%cx, %r11w
	shll	$10, %r11d
	movswq	%r11w, %rax
	addq	$5120, %rax             # imm = 0x1400
	cmpq	$32766, %rax            # imm = 0x7FFE
	jle	.LBB1_2
# BB#1:
	movl	$26214, %eax            # imm = 0x6666
	jmp	.LBB1_3
.LBB1_2:
	movswq	%ax, %rax
	imulq	%r9, %rax
	addq	%r8, %rax
	sarq	$48, %rax
	addq	%rax, %rax
.LBB1_3:
	leaq	32768(%rax), %rcx
	xorl	%edx, %edx
	testq	%rax, %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%ax, %dx
	movw	%dx, 6(%rsi)
	movswq	8(%rdi), %rax
	leal	56(%rax), %ecx
	xorl	%edx, %edx
	cmpq	$8, %rax
	leaq	32760(%rax), %rax
	cmovgw	%r10w, %dx
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%cx, %dx
	shll	$10, %edx
	movswq	%dx, %rax
	addq	$-188, %rax
	cmpq	$32766, %rax            # imm = 0x7FFE
	jle	.LBB1_5
# BB#4:
	movl	$38444, %eax            # imm = 0x962C
	jmp	.LBB1_6
.LBB1_5:
	cmpq	$-32769, %rax           # imm = 0xFFFF7FFF
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%rax, %rcx
	movswq	%cx, %rcx
	movabsq	$165124312662016, %rax  # imm = 0x962E00000000
	imulq	%rcx, %rax
	addq	%r8, %rax
	sarq	$48, %rax
	addq	%rax, %rax
.LBB1_6:
	leaq	32768(%rax), %rcx
	xorl	%edx, %edx
	testq	%rax, %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%ax, %dx
	movw	%dx, 8(%rsi)
	movswq	10(%rdi), %rax
	leal	56(%rax), %ecx
	xorl	%edx, %edx
	cmpq	$8, %rax
	leaq	32760(%rax), %rax
	movw	$63, %r9w
	cmovgw	%r9w, %dx
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%cx, %dx
	shll	$10, %edx
	movswq	%dx, %rcx
	addq	$3584, %rcx             # imm = 0xE00
	cmpq	$32766, %rcx            # imm = 0x7FFE
	jle	.LBB1_8
# BB#7:
	movl	$34950, %ecx            # imm = 0x8886
	jmp	.LBB1_9
.LBB1_8:
	movswq	%cx, %rax
	movabsq	$150117696929792, %rcx  # imm = 0x888800000000
	imulq	%rax, %rcx
	addq	%r8, %rcx
	sarq	$48, %rcx
	addq	%rcx, %rcx
.LBB1_9:
	leaq	32768(%rcx), %rax
	xorl	%edx, %edx
	testq	%rcx, %rcx
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%cx, %dx
	movw	%dx, 10(%rsi)
	movswq	12(%rdi), %rax
	leal	60(%rax), %ecx
	xorl	%edx, %edx
	cmpq	$4, %rax
	leaq	32764(%rax), %rax
	cmovgw	%r9w, %dx
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%cx, %dx
	shll	$10, %edx
	movswq	%dx, %rax
	orq	$682, %rax              # imm = 0x2AA
	cmpq	$32766, %rax            # imm = 0x7FFE
	jle	.LBB1_11
# BB#10:
	movl	$62906, %eax            # imm = 0xF5BA
	jmp	.LBB1_12
.LBB1_11:
	cmpq	$-32769, %rax           # imm = 0xFFFF7FFF
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%rax, %rcx
	movabsq	$270187802656768, %rax  # imm = 0xF5BC00000000
	imulq	%rcx, %rax
	addq	%r8, %rax
	sarq	$48, %rax
	addq	%rax, %rax
.LBB1_12:
	leaq	32768(%rax), %rcx
	xorl	%edx, %edx
	testq	%rax, %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%ax, %dx
	movw	%dx, 12(%rsi)
	movswq	14(%rdi), %rax
	leal	60(%rax), %ecx
	xorl	%edx, %edx
	cmpq	$4, %rax
	leaq	32764(%rax), %rax
	movw	$63, %di
	cmovlew	%dx, %di
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%cx, %di
	shll	$10, %edi
	movswq	%di, %rax
	addq	$2288, %rax             # imm = 0x8F0
	cmpq	$32766, %rax            # imm = 0x7FFE
	jle	.LBB1_14
# BB#13:
	movl	$59414, %eax            # imm = 0xE816
	jmp	.LBB1_15
.LBB1_14:
	movswq	%ax, %rcx
	movabsq	$255189776859136, %rax  # imm = 0xE81800000000
	imulq	%rcx, %rax
	addq	%r8, %rax
	sarq	$48, %rax
	addq	%rax, %rax
.LBB1_15:
	leaq	32768(%rax), %rcx
	xorl	%edx, %edx
	testq	%rax, %rax
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%ax, %dx
	movw	%dx, 14(%rsi)
	retq
.Lfunc_end1:
	.size	Decoding_of_the_coded_Log_Area_Ratios, .Lfunc_end1-Decoding_of_the_coded_Log_Area_Ratios
	.cfi_endproc

	.p2align	4, 0x90
	.type	Coefficients_13_26,@function
Coefficients_13_26:                     # @Coefficients_13_26
	.cfi_startproc
# BB#0:
	movswl	(%rdi), %eax
	sarl	%eax
	movswl	(%rsi), %ecx
	sarl	%ecx
	addl	%eax, %ecx
	movslq	%ecx, %rax
	leaq	32768(%rax), %r8
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %r8             # imm = 0xFFFF
	cmovbew	%ax, %cx
	movw	%cx, (%rdx)
	movswl	2(%rdi), %eax
	sarl	%eax
	movswl	2(%rsi), %ecx
	sarl	%ecx
	addl	%eax, %ecx
	movslq	%ecx, %rax
	leaq	32768(%rax), %r8
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %r8             # imm = 0xFFFF
	cmovbew	%ax, %cx
	movw	%cx, 2(%rdx)
	movswl	4(%rdi), %eax
	sarl	%eax
	movswl	4(%rsi), %ecx
	sarl	%ecx
	addl	%eax, %ecx
	movslq	%ecx, %rax
	leaq	32768(%rax), %r8
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %r8             # imm = 0xFFFF
	cmovbew	%ax, %cx
	movw	%cx, 4(%rdx)
	movswl	6(%rdi), %eax
	sarl	%eax
	movswl	6(%rsi), %ecx
	sarl	%ecx
	addl	%eax, %ecx
	movslq	%ecx, %rax
	leaq	32768(%rax), %r8
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %r8             # imm = 0xFFFF
	cmovbew	%ax, %cx
	movw	%cx, 6(%rdx)
	movswl	8(%rdi), %eax
	sarl	%eax
	movswl	8(%rsi), %ecx
	sarl	%ecx
	addl	%eax, %ecx
	movslq	%ecx, %rax
	leaq	32768(%rax), %r8
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %r8             # imm = 0xFFFF
	cmovbew	%ax, %cx
	movw	%cx, 8(%rdx)
	movswl	10(%rdi), %eax
	sarl	%eax
	movswl	10(%rsi), %ecx
	sarl	%ecx
	addl	%eax, %ecx
	movslq	%ecx, %rax
	leaq	32768(%rax), %r8
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %r8             # imm = 0xFFFF
	cmovbew	%ax, %cx
	movw	%cx, 10(%rdx)
	movswl	12(%rdi), %eax
	sarl	%eax
	movswl	12(%rsi), %ecx
	sarl	%ecx
	addl	%eax, %ecx
	movslq	%ecx, %rax
	leaq	32768(%rax), %r8
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %r8             # imm = 0xFFFF
	cmovbew	%ax, %cx
	movw	%cx, 12(%rdx)
	movswl	14(%rdi), %eax
	sarl	%eax
	movswl	14(%rsi), %ecx
	sarl	%ecx
	addl	%eax, %ecx
	movslq	%ecx, %rax
	leaq	32768(%rax), %rcx
	xorl	%esi, %esi
	testl	%eax, %eax
	setle	%sil
	addl	$32767, %esi            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%ax, %si
	movw	%si, 14(%rdx)
	retq
.Lfunc_end2:
	.size	Coefficients_13_26, .Lfunc_end2-Coefficients_13_26
	.cfi_endproc

	.globl	Gsm_Short_Term_Synthesis_Filter
	.p2align	4, 0x90
	.type	Gsm_Short_Term_Synthesis_Filter,@function
Gsm_Short_Term_Synthesis_Filter:        # @Gsm_Short_Term_Synthesis_Filter
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 64
.Lcfi19:
	.cfi_offset %rbx, -48
.Lcfi20:
	.cfi_offset %r12, -40
.Lcfi21:
	.cfi_offset %r13, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movswq	628(%rbx), %rax
	movq	%rax, %rcx
	shlq	$4, %rcx
	leaq	596(%rbx,%rcx), %r12
	movl	%eax, %ecx
	xorl	$1, %ecx
	movw	%cx, 628(%rbx)
	xorq	$1, %rax
	shlq	$4, %rax
	leaq	596(%rbx,%rax), %r13
	movq	%rsi, %rdi
	movq	%r12, %rsi
	callq	Decoding_of_the_coded_Log_Area_Ratios
	xorl	%eax, %eax
	movl	$32767, %r8d            # imm = 0x7FFF
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movswl	(%r13,%rax,2), %edx
	movl	%edx, %esi
	sarl	$2, %esi
	movswl	(%r12,%rax,2), %edi
	sarl	$2, %edi
	addl	%esi, %edi
	movslq	%edi, %rsi
	leaq	32768(%rsi), %rdi
	testl	%esi, %esi
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%r8, %rcx
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbeq	%rsi, %rcx
	sarl	%edx
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx), %rsi
	leaq	32768(%rcx,%rdx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB3_1
# BB#2:                                 # %Coefficients_0_12.exit.preheader
	xorl	%eax, %eax
	movw	$32767, %cx             # imm = 0x7FFF
	movl	$32767, %r8d            # imm = 0x7FFF
	jmp	.LBB3_3
.LBB3_13:                               #   in Loop: Header=BB3_3 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB3_17
# BB#14:                                #   in Loop: Header=BB3_3 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%r8, %rsi
	jmp	.LBB3_20
.LBB3_15:                               #   in Loop: Header=BB3_3 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB3_18
# BB#16:                                #   in Loop: Header=BB3_3 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%r8, %rsi
	jmp	.LBB3_19
.LBB3_17:                               #   in Loop: Header=BB3_3 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB3_20
.LBB3_18:                               #   in Loop: Header=BB3_3 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_3:                                # %Coefficients_0_12.exit
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rsp,%rax,2), %edi
	testl	%edi, %edi
	movslq	%edi, %rsi
	js	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB3_9
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	addq	%rsi, %rsi
	jmp	.LBB3_20
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	movzwl	%si, %esi
	negl	%edi
	cmpl	$32768, %esi            # imm = 0x8000
	cmovew	%cx, %di
	movswl	%di, %edi
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB3_11
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	addl	%edi, %edi
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_9:                                #   in Loop: Header=BB3_3 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB3_13
# BB#10:                                #   in Loop: Header=BB3_3 Depth=1
	addq	$11059, %rsi            # imm = 0x2B33
	jmp	.LBB3_20
.LBB3_11:                               #   in Loop: Header=BB3_3 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB3_15
# BB#12:                                #   in Loop: Header=BB3_3 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
.LBB3_8:                                #   in Loop: Header=BB3_3 Depth=1
	movslq	%edi, %rsi
.LBB3_19:                               #   in Loop: Header=BB3_3 Depth=1
	negq	%rsi
.LBB3_20:                               #   in Loop: Header=BB3_3 Depth=1
	movw	%si, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB3_3
# BB#21:                                # %LARp_to_rp.exit92
	movswq	(%rsp), %r8
	movswq	2(%rsp), %r9
	xorl	%r11d, %r11d
	movl	$32767, %r10d           # imm = 0x7FFF
	.p2align	4, 0x90
.LBB3_22:                               # %.lr.ph.i96
                                        # =>This Inner Loop Header: Depth=1
	movswq	(%r15,%r11,2), %rsi
	movswq	14(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	646(%rbx), %edi
	movswq	%di, %rax
	jne	.LBB3_25
# BB#23:                                # %.lr.ph.i96
                                        #   in Loop: Header=BB3_22 Depth=1
	movzwl	%di, %edi
	cmpl	$32768, %edi            # imm = 0x8000
	jne	.LBB3_25
# BB#24:                                #   in Loop: Header=BB3_22 Depth=1
	movl	$32767, %edi            # imm = 0x7FFF
	jmp	.LBB3_26
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_22 Depth=1
	movl	%eax, %edi
	imull	%ecx, %edi
	addl	$16384, %edi            # imm = 0x4000
	shrl	$15, %edi
	movzwl	%di, %edi
.LBB3_26:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdi
	subq	%rdi, %rsi
	cmpq	$-32769, %rsi           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rsi, %rdi
	cmpq	$32766, %rsi            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	jne	.LBB3_28
# BB#27:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%di, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_29
.LBB3_28:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rsi
	imull	%ecx, %esi
	addl	$16384, %esi            # imm = 0x4000
	shrl	$15, %esi
	movzwl	%si, %esi
.LBB3_29:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rsi
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%si, %cx
	movw	%cx, 648(%rbx)
	movswq	12(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	644(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_32
# BB#30:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%si, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	jne	.LBB3_32
# BB#31:                                #   in Loop: Header=BB3_22 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_33
	.p2align	4, 0x90
.LBB3_32:                               #   in Loop: Header=BB3_22 Depth=1
	movl	%eax, %esi
	imull	%ecx, %esi
	addl	$16384, %esi            # imm = 0x4000
	shrl	$15, %esi
	movzwl	%si, %esi
.LBB3_33:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_35
# BB#34:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_36
.LBB3_35:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_36:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 646(%rbx)
	movswq	10(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	642(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_39
# BB#37:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_22 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_40
	.p2align	4, 0x90
.LBB3_39:                               #   in Loop: Header=BB3_22 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_40:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_42
# BB#41:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_43
.LBB3_42:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_43:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 644(%rbx)
	movswq	8(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	640(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_46
# BB#44:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_46
# BB#45:                                #   in Loop: Header=BB3_22 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_47
	.p2align	4, 0x90
.LBB3_46:                               #   in Loop: Header=BB3_22 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_47:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_49
# BB#48:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_50
.LBB3_49:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_50:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 642(%rbx)
	movswq	6(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	638(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_53
# BB#51:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_53
# BB#52:                                #   in Loop: Header=BB3_22 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_54
	.p2align	4, 0x90
.LBB3_53:                               #   in Loop: Header=BB3_22 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_54:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_56
# BB#55:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_57
.LBB3_56:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_57:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 640(%rbx)
	movswq	4(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	636(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_60
# BB#58:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_22 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_61
	.p2align	4, 0x90
.LBB3_60:                               #   in Loop: Header=BB3_22 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_61:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_63
# BB#62:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_64
.LBB3_63:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_64:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 638(%rbx)
	movzwl	634(%rbx), %ecx
	movswq	%cx, %rax
	movzwl	%r9w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_67
# BB#65:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%cx, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_67
# BB#66:                                #   in Loop: Header=BB3_22 Depth=1
	movl	$32767, %ecx            # imm = 0x7FFF
	jmp	.LBB3_68
	.p2align	4, 0x90
.LBB3_67:                               #   in Loop: Header=BB3_22 Depth=1
	movl	%eax, %ecx
	imull	%r9d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_68:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	movswq	%cx, %rcx
	subq	%rcx, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%r9w, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_70
# BB#69:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%di, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	movl	$32767, %ecx            # imm = 0x7FFF
	je	.LBB3_71
.LBB3_70:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rcx
	imull	%r9d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_71:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%cx, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 636(%rbx)
	movzwl	632(%rbx), %ecx
	movswq	%cx, %rax
	movzwl	%r8w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_74
# BB#72:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%cx, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_74
# BB#73:                                #   in Loop: Header=BB3_22 Depth=1
	movl	$32767, %ecx            # imm = 0x7FFF
	jmp	.LBB3_75
	.p2align	4, 0x90
.LBB3_74:                               #   in Loop: Header=BB3_22 Depth=1
	movl	%eax, %ecx
	imull	%r8d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_75:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%di, %rdx
	movswq	%cx, %rcx
	subq	%rcx, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%rdx, %rcx
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rcx
	movzwl	%r8w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_77
# BB#76:                                #   in Loop: Header=BB3_22 Depth=1
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_78
.LBB3_77:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%cx, %rdx
	imull	%r8d, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_78:                               #   in Loop: Header=BB3_22 Depth=1
	movswq	%si, %rdx
	leaq	(%rdx,%rax), %rsi
	leaq	32768(%rdx,%rax), %rax
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, 634(%rbx)
	movw	%cx, 632(%rbx)
	movw	%cx, (%r14,%r11,2)
	incq	%r11
	cmpl	$13, %r11d
	jne	.LBB3_22
# BB#79:                                # %Short_term_synthesis_filtering.exit107
	movq	%rsp, %rdx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	Coefficients_13_26
	xorl	%eax, %eax
	movw	$32767, %cx             # imm = 0x7FFF
	movl	$32767, %r8d            # imm = 0x7FFF
	jmp	.LBB3_80
.LBB3_90:                               #   in Loop: Header=BB3_80 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB3_94
# BB#91:                                #   in Loop: Header=BB3_80 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%r8, %rsi
	jmp	.LBB3_97
.LBB3_92:                               #   in Loop: Header=BB3_80 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB3_95
# BB#93:                                #   in Loop: Header=BB3_80 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%r8, %rsi
	jmp	.LBB3_96
.LBB3_94:                               #   in Loop: Header=BB3_80 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB3_97
.LBB3_95:                               #   in Loop: Header=BB3_80 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB3_96
	.p2align	4, 0x90
.LBB3_80:                               # =>This Inner Loop Header: Depth=1
	movswl	(%rsp,%rax,2), %edi
	testl	%edi, %edi
	movslq	%edi, %rsi
	js	.LBB3_83
# BB#81:                                #   in Loop: Header=BB3_80 Depth=1
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB3_86
# BB#82:                                #   in Loop: Header=BB3_80 Depth=1
	addq	%rsi, %rsi
	jmp	.LBB3_97
	.p2align	4, 0x90
.LBB3_83:                               #   in Loop: Header=BB3_80 Depth=1
	movzwl	%si, %esi
	negl	%edi
	cmpl	$32768, %esi            # imm = 0x8000
	cmovew	%cx, %di
	movswl	%di, %edi
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB3_88
# BB#84:                                #   in Loop: Header=BB3_80 Depth=1
	addl	%edi, %edi
	jmp	.LBB3_85
	.p2align	4, 0x90
.LBB3_86:                               #   in Loop: Header=BB3_80 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB3_90
# BB#87:                                #   in Loop: Header=BB3_80 Depth=1
	addq	$11059, %rsi            # imm = 0x2B33
	jmp	.LBB3_97
.LBB3_88:                               #   in Loop: Header=BB3_80 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB3_92
# BB#89:                                #   in Loop: Header=BB3_80 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
.LBB3_85:                               #   in Loop: Header=BB3_80 Depth=1
	movslq	%edi, %rsi
.LBB3_96:                               #   in Loop: Header=BB3_80 Depth=1
	negq	%rsi
.LBB3_97:                               #   in Loop: Header=BB3_80 Depth=1
	movw	%si, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB3_80
# BB#98:                                # %LARp_to_rp.exit83
	movswq	(%rsp), %r8
	movswq	2(%rsp), %r9
	xorl	%r11d, %r11d
	movl	$32767, %r10d           # imm = 0x7FFF
	.p2align	4, 0x90
.LBB3_99:                               # %.lr.ph.i63
                                        # =>This Inner Loop Header: Depth=1
	movswq	26(%r15,%r11,2), %rsi
	movswq	14(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	646(%rbx), %edi
	movswq	%di, %rax
	jne	.LBB3_102
# BB#100:                               # %.lr.ph.i63
                                        #   in Loop: Header=BB3_99 Depth=1
	movzwl	%di, %edi
	cmpl	$32768, %edi            # imm = 0x8000
	jne	.LBB3_102
# BB#101:                               #   in Loop: Header=BB3_99 Depth=1
	movl	$32767, %edi            # imm = 0x7FFF
	jmp	.LBB3_103
	.p2align	4, 0x90
.LBB3_102:                              #   in Loop: Header=BB3_99 Depth=1
	movl	%eax, %edi
	imull	%ecx, %edi
	addl	$16384, %edi            # imm = 0x4000
	shrl	$15, %edi
	movzwl	%di, %edi
.LBB3_103:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdi
	subq	%rdi, %rsi
	cmpq	$-32769, %rsi           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rsi, %rdi
	cmpq	$32766, %rsi            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	jne	.LBB3_105
# BB#104:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%di, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_106
.LBB3_105:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rsi
	imull	%ecx, %esi
	addl	$16384, %esi            # imm = 0x4000
	shrl	$15, %esi
	movzwl	%si, %esi
.LBB3_106:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rsi
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%si, %cx
	movw	%cx, 648(%rbx)
	movswq	12(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	644(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_109
# BB#107:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%si, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	jne	.LBB3_109
# BB#108:                               #   in Loop: Header=BB3_99 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_110
	.p2align	4, 0x90
.LBB3_109:                              #   in Loop: Header=BB3_99 Depth=1
	movl	%eax, %esi
	imull	%ecx, %esi
	addl	$16384, %esi            # imm = 0x4000
	shrl	$15, %esi
	movzwl	%si, %esi
.LBB3_110:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_112
# BB#111:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_113
.LBB3_112:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_113:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 646(%rbx)
	movswq	10(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	642(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_116
# BB#114:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_116
# BB#115:                               #   in Loop: Header=BB3_99 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_117
	.p2align	4, 0x90
.LBB3_116:                              #   in Loop: Header=BB3_99 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_117:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_119
# BB#118:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_120
.LBB3_119:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_120:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 644(%rbx)
	movswq	8(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	640(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_123
# BB#121:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_123
# BB#122:                               #   in Loop: Header=BB3_99 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_124
	.p2align	4, 0x90
.LBB3_123:                              #   in Loop: Header=BB3_99 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_124:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_126
# BB#125:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_127
.LBB3_126:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_127:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 642(%rbx)
	movswq	6(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	638(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_130
# BB#128:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_130
# BB#129:                               #   in Loop: Header=BB3_99 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_131
	.p2align	4, 0x90
.LBB3_130:                              #   in Loop: Header=BB3_99 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_131:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_133
# BB#132:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_134
.LBB3_133:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_134:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 640(%rbx)
	movswq	4(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	636(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_137
# BB#135:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_137
# BB#136:                               #   in Loop: Header=BB3_99 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_138
	.p2align	4, 0x90
.LBB3_137:                              #   in Loop: Header=BB3_99 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_138:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_140
# BB#139:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_141
.LBB3_140:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_141:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 638(%rbx)
	movzwl	634(%rbx), %ecx
	movswq	%cx, %rax
	movzwl	%r9w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_144
# BB#142:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%cx, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_144
# BB#143:                               #   in Loop: Header=BB3_99 Depth=1
	movl	$32767, %ecx            # imm = 0x7FFF
	jmp	.LBB3_145
	.p2align	4, 0x90
.LBB3_144:                              #   in Loop: Header=BB3_99 Depth=1
	movl	%eax, %ecx
	imull	%r9d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_145:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	movswq	%cx, %rcx
	subq	%rcx, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%r9w, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_147
# BB#146:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%di, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	movl	$32767, %ecx            # imm = 0x7FFF
	je	.LBB3_148
.LBB3_147:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rcx
	imull	%r9d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_148:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%cx, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 636(%rbx)
	movzwl	632(%rbx), %ecx
	movswq	%cx, %rax
	movzwl	%r8w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_151
# BB#149:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%cx, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_151
# BB#150:                               #   in Loop: Header=BB3_99 Depth=1
	movl	$32767, %ecx            # imm = 0x7FFF
	jmp	.LBB3_152
	.p2align	4, 0x90
.LBB3_151:                              #   in Loop: Header=BB3_99 Depth=1
	movl	%eax, %ecx
	imull	%r8d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_152:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%di, %rdx
	movswq	%cx, %rcx
	subq	%rcx, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%rdx, %rcx
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rcx
	movzwl	%r8w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_154
# BB#153:                               #   in Loop: Header=BB3_99 Depth=1
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_155
.LBB3_154:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%cx, %rdx
	imull	%r8d, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_155:                              #   in Loop: Header=BB3_99 Depth=1
	movswq	%si, %rdx
	leaq	(%rdx,%rax), %rsi
	leaq	32768(%rdx,%rax), %rax
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, 634(%rbx)
	movw	%cx, 632(%rbx)
	movw	%cx, 26(%r14,%r11,2)
	incq	%r11
	cmpl	$14, %r11d
	jne	.LBB3_99
# BB#156:                               # %Short_term_synthesis_filtering.exit74.preheader
	xorl	%eax, %eax
	movl	$32767, %r8d            # imm = 0x7FFF
	.p2align	4, 0x90
.LBB3_157:                              # %Short_term_synthesis_filtering.exit74
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%r13,%rax,2), %edx
	sarl	$2, %edx
	movswl	(%r12,%rax,2), %esi
	movl	%esi, %edi
	sarl	$2, %edi
	addl	%edx, %edi
	movslq	%edi, %rdx
	leaq	32768(%rdx), %rdi
	testl	%edx, %edx
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%r8, %rcx
	cmpq	$65535, %rdi            # imm = 0xFFFF
	cmovbeq	%rdx, %rcx
	sarl	%esi
	movslq	%esi, %rdx
	leaq	(%rcx,%rdx), %rsi
	leaq	32768(%rcx,%rdx), %rcx
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rcx            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB3_157
# BB#158:                               # %Coefficients_27_39.exit.preheader
	xorl	%eax, %eax
	movw	$32767, %cx             # imm = 0x7FFF
	movl	$32767, %r8d            # imm = 0x7FFF
	jmp	.LBB3_159
.LBB3_169:                              #   in Loop: Header=BB3_159 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB3_173
# BB#170:                               #   in Loop: Header=BB3_159 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%r8, %rsi
	jmp	.LBB3_176
.LBB3_171:                              #   in Loop: Header=BB3_159 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB3_174
# BB#172:                               #   in Loop: Header=BB3_159 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%r8, %rsi
	jmp	.LBB3_175
.LBB3_173:                              #   in Loop: Header=BB3_159 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB3_176
.LBB3_174:                              #   in Loop: Header=BB3_159 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB3_175
	.p2align	4, 0x90
.LBB3_159:                              # %Coefficients_27_39.exit
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rsp,%rax,2), %edi
	testl	%edi, %edi
	movslq	%edi, %rsi
	js	.LBB3_162
# BB#160:                               #   in Loop: Header=BB3_159 Depth=1
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB3_165
# BB#161:                               #   in Loop: Header=BB3_159 Depth=1
	addq	%rsi, %rsi
	jmp	.LBB3_176
	.p2align	4, 0x90
.LBB3_162:                              #   in Loop: Header=BB3_159 Depth=1
	movzwl	%si, %esi
	negl	%edi
	cmpl	$32768, %esi            # imm = 0x8000
	cmovew	%cx, %di
	movswl	%di, %edi
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB3_167
# BB#163:                               #   in Loop: Header=BB3_159 Depth=1
	addl	%edi, %edi
	jmp	.LBB3_164
	.p2align	4, 0x90
.LBB3_165:                              #   in Loop: Header=BB3_159 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB3_169
# BB#166:                               #   in Loop: Header=BB3_159 Depth=1
	addq	$11059, %rsi            # imm = 0x2B33
	jmp	.LBB3_176
.LBB3_167:                              #   in Loop: Header=BB3_159 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB3_171
# BB#168:                               #   in Loop: Header=BB3_159 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
.LBB3_164:                              #   in Loop: Header=BB3_159 Depth=1
	movslq	%edi, %rsi
.LBB3_175:                              #   in Loop: Header=BB3_159 Depth=1
	negq	%rsi
.LBB3_176:                              #   in Loop: Header=BB3_159 Depth=1
	movw	%si, (%rsp,%rax,2)
	incq	%rax
	cmpl	$8, %eax
	jne	.LBB3_159
# BB#177:                               # %LARp_to_rp.exit53
	movswq	(%rsp), %r8
	movswq	2(%rsp), %r9
	xorl	%r11d, %r11d
	movl	$32767, %r10d           # imm = 0x7FFF
	.p2align	4, 0x90
.LBB3_178:                              # %.lr.ph.i33
                                        # =>This Inner Loop Header: Depth=1
	movswq	54(%r15,%r11,2), %rsi
	movswq	14(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	646(%rbx), %edi
	movswq	%di, %rax
	jne	.LBB3_181
# BB#179:                               # %.lr.ph.i33
                                        #   in Loop: Header=BB3_178 Depth=1
	movzwl	%di, %edi
	cmpl	$32768, %edi            # imm = 0x8000
	jne	.LBB3_181
# BB#180:                               #   in Loop: Header=BB3_178 Depth=1
	movl	$32767, %edi            # imm = 0x7FFF
	jmp	.LBB3_182
	.p2align	4, 0x90
.LBB3_181:                              #   in Loop: Header=BB3_178 Depth=1
	movl	%eax, %edi
	imull	%ecx, %edi
	addl	$16384, %edi            # imm = 0x4000
	shrl	$15, %edi
	movzwl	%di, %edi
.LBB3_182:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdi
	subq	%rdi, %rsi
	cmpq	$-32769, %rsi           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rsi, %rdi
	cmpq	$32766, %rsi            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	jne	.LBB3_184
# BB#183:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%di, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_185
.LBB3_184:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rsi
	imull	%ecx, %esi
	addl	$16384, %esi            # imm = 0x4000
	shrl	$15, %esi
	movzwl	%si, %esi
.LBB3_185:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rsi
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%si, %cx
	movw	%cx, 648(%rbx)
	movswq	12(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	644(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_188
# BB#186:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%si, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	jne	.LBB3_188
# BB#187:                               #   in Loop: Header=BB3_178 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_189
	.p2align	4, 0x90
.LBB3_188:                              #   in Loop: Header=BB3_178 Depth=1
	movl	%eax, %esi
	imull	%ecx, %esi
	addl	$16384, %esi            # imm = 0x4000
	shrl	$15, %esi
	movzwl	%si, %esi
.LBB3_189:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_191
# BB#190:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_192
.LBB3_191:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_192:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 646(%rbx)
	movswq	10(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	642(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_195
# BB#193:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_195
# BB#194:                               #   in Loop: Header=BB3_178 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_196
	.p2align	4, 0x90
.LBB3_195:                              #   in Loop: Header=BB3_178 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_196:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_198
# BB#197:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_199
.LBB3_198:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_199:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 644(%rbx)
	movswq	8(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	640(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_202
# BB#200:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_202
# BB#201:                               #   in Loop: Header=BB3_178 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_203
	.p2align	4, 0x90
.LBB3_202:                              #   in Loop: Header=BB3_178 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_203:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_205
# BB#204:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_206
.LBB3_205:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_206:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 642(%rbx)
	movswq	6(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	638(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_209
# BB#207:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_209
# BB#208:                               #   in Loop: Header=BB3_178 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_210
	.p2align	4, 0x90
.LBB3_209:                              #   in Loop: Header=BB3_178 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_210:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_212
# BB#211:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_213
.LBB3_212:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_213:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 640(%rbx)
	movswq	4(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	636(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_216
# BB#214:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_216
# BB#215:                               #   in Loop: Header=BB3_178 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_217
	.p2align	4, 0x90
.LBB3_216:                              #   in Loop: Header=BB3_178 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_217:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_219
# BB#218:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_220
.LBB3_219:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_220:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 638(%rbx)
	movzwl	634(%rbx), %ecx
	movswq	%cx, %rax
	movzwl	%r9w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_223
# BB#221:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%cx, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_223
# BB#222:                               #   in Loop: Header=BB3_178 Depth=1
	movl	$32767, %ecx            # imm = 0x7FFF
	jmp	.LBB3_224
	.p2align	4, 0x90
.LBB3_223:                              #   in Loop: Header=BB3_178 Depth=1
	movl	%eax, %ecx
	imull	%r9d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_224:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	movswq	%cx, %rcx
	subq	%rcx, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%r9w, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_226
# BB#225:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%di, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	movl	$32767, %ecx            # imm = 0x7FFF
	je	.LBB3_227
.LBB3_226:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rcx
	imull	%r9d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_227:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%cx, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 636(%rbx)
	movzwl	632(%rbx), %ecx
	movswq	%cx, %rax
	movzwl	%r8w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_230
# BB#228:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%cx, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_230
# BB#229:                               #   in Loop: Header=BB3_178 Depth=1
	movl	$32767, %ecx            # imm = 0x7FFF
	jmp	.LBB3_231
	.p2align	4, 0x90
.LBB3_230:                              #   in Loop: Header=BB3_178 Depth=1
	movl	%eax, %ecx
	imull	%r8d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_231:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%di, %rdx
	movswq	%cx, %rcx
	subq	%rcx, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%rdx, %rcx
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rcx
	movzwl	%r8w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_233
# BB#232:                               #   in Loop: Header=BB3_178 Depth=1
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_234
.LBB3_233:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%cx, %rdx
	imull	%r8d, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_234:                              #   in Loop: Header=BB3_178 Depth=1
	movswq	%si, %rdx
	leaq	(%rdx,%rax), %rsi
	leaq	32768(%rdx,%rax), %rax
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, 634(%rbx)
	movw	%cx, 632(%rbx)
	movw	%cx, 54(%r14,%r11,2)
	incq	%r11
	cmpl	$13, %r11d
	jne	.LBB3_178
# BB#235:                               # %Short_term_synthesis_filtering.exit44
	movdqu	(%r12), %xmm0
	movdqa	%xmm0, (%rsp)
	movd	%xmm0, %esi
	movl	$1, %eax
	movw	$32767, %cx             # imm = 0x7FFF
	movl	$32767, %r8d            # imm = 0x7FFF
	jmp	.LBB3_237
	.p2align	4, 0x90
.LBB3_236:                              # %._crit_edge
                                        #   in Loop: Header=BB3_237 Depth=1
	movw	(%rsp,%rax,2), %si
	incq	%rax
.LBB3_237:                              # =>This Inner Loop Header: Depth=1
	movswl	%si, %edi
	testw	%di, %di
	js	.LBB3_240
# BB#238:                               #   in Loop: Header=BB3_237 Depth=1
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB3_243
# BB#239:                               #   in Loop: Header=BB3_237 Depth=1
	addl	%edi, %edi
	movslq	%edi, %rsi
	jmp	.LBB3_254
	.p2align	4, 0x90
.LBB3_240:                              #   in Loop: Header=BB3_237 Depth=1
	movzwl	%di, %esi
	negl	%edi
	cmpl	$32768, %esi            # imm = 0x8000
	cmovew	%cx, %di
	movswl	%di, %edi
	cmpl	$11058, %edi            # imm = 0x2B32
	jg	.LBB3_245
# BB#241:                               #   in Loop: Header=BB3_237 Depth=1
	addl	%edi, %edi
	jmp	.LBB3_242
	.p2align	4, 0x90
.LBB3_243:                              #   in Loop: Header=BB3_237 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB3_247
# BB#244:                               #   in Loop: Header=BB3_237 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
	movslq	%edi, %rsi
	jmp	.LBB3_254
.LBB3_245:                              #   in Loop: Header=BB3_237 Depth=1
	cmpl	$20069, %edi            # imm = 0x4E65
	jg	.LBB3_249
# BB#246:                               #   in Loop: Header=BB3_237 Depth=1
	addl	$11059, %edi            # imm = 0x2B33
.LBB3_242:                              #   in Loop: Header=BB3_237 Depth=1
	movslq	%edi, %rsi
.LBB3_253:                              #   in Loop: Header=BB3_237 Depth=1
	negq	%rsi
.LBB3_254:                              #   in Loop: Header=BB3_237 Depth=1
	movw	%si, -2(%rsp,%rax,2)
	cmpl	$8, %eax
	jne	.LBB3_236
	jmp	.LBB3_255
.LBB3_247:                              #   in Loop: Header=BB3_237 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB3_251
# BB#248:                               #   in Loop: Header=BB3_237 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%r8, %rsi
	jmp	.LBB3_254
.LBB3_249:                              #   in Loop: Header=BB3_237 Depth=1
	sarl	$2, %edi
	movslq	%edi, %rsi
	leaq	58880(%rsi), %rdx
	cmpq	$65536, %rdx            # imm = 0x10000
	jb	.LBB3_252
# BB#250:                               #   in Loop: Header=BB3_237 Depth=1
	cmpl	$-26112, %edi           # imm = 0x9A00
	movq	$-32768, %rsi           # imm = 0x8000
	cmovgq	%r8, %rsi
	jmp	.LBB3_253
.LBB3_251:                              #   in Loop: Header=BB3_237 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB3_254
.LBB3_252:                              #   in Loop: Header=BB3_237 Depth=1
	addq	$26112, %rsi            # imm = 0x6600
	jmp	.LBB3_253
.LBB3_255:                              # %LARp_to_rp.exit
	movswq	(%rsp), %r8
	movswq	2(%rsp), %r9
	xorl	%r11d, %r11d
	movl	$32767, %r10d           # imm = 0x7FFF
	.p2align	4, 0x90
.LBB3_256:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movswq	80(%r15,%r11,2), %rsi
	movswq	14(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	646(%rbx), %edi
	movswq	%di, %rax
	jne	.LBB3_259
# BB#257:                               # %.lr.ph.i
                                        #   in Loop: Header=BB3_256 Depth=1
	movzwl	%di, %edi
	cmpl	$32768, %edi            # imm = 0x8000
	jne	.LBB3_259
# BB#258:                               #   in Loop: Header=BB3_256 Depth=1
	movl	$32767, %edi            # imm = 0x7FFF
	jmp	.LBB3_260
	.p2align	4, 0x90
.LBB3_259:                              #   in Loop: Header=BB3_256 Depth=1
	movl	%eax, %edi
	imull	%ecx, %edi
	addl	$16384, %edi            # imm = 0x4000
	shrl	$15, %edi
	movzwl	%di, %edi
.LBB3_260:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdi
	subq	%rdi, %rsi
	cmpq	$-32769, %rsi           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rsi, %rdi
	cmpq	$32766, %rsi            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	jne	.LBB3_262
# BB#261:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%di, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_263
.LBB3_262:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rsi
	imull	%ecx, %esi
	addl	$16384, %esi            # imm = 0x4000
	shrl	$15, %esi
	movzwl	%si, %esi
.LBB3_263:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rsi
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%si, %cx
	movw	%cx, 648(%rbx)
	movswq	12(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	644(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_266
# BB#264:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%si, %esi
	cmpl	$32768, %esi            # imm = 0x8000
	jne	.LBB3_266
# BB#265:                               #   in Loop: Header=BB3_256 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_267
	.p2align	4, 0x90
.LBB3_266:                              #   in Loop: Header=BB3_256 Depth=1
	movl	%eax, %esi
	imull	%ecx, %esi
	addl	$16384, %esi            # imm = 0x4000
	shrl	$15, %esi
	movzwl	%si, %esi
.LBB3_267:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_269
# BB#268:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_270
.LBB3_269:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_270:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 646(%rbx)
	movswq	10(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	642(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_273
# BB#271:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_273
# BB#272:                               #   in Loop: Header=BB3_256 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_274
	.p2align	4, 0x90
.LBB3_273:                              #   in Loop: Header=BB3_256 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_274:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_276
# BB#275:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_277
.LBB3_276:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_277:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 644(%rbx)
	movswq	8(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	640(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_280
# BB#278:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_280
# BB#279:                               #   in Loop: Header=BB3_256 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_281
	.p2align	4, 0x90
.LBB3_280:                              #   in Loop: Header=BB3_256 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_281:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_283
# BB#282:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_284
.LBB3_283:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_284:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 642(%rbx)
	movswq	6(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	638(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_287
# BB#285:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_287
# BB#286:                               #   in Loop: Header=BB3_256 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_288
	.p2align	4, 0x90
.LBB3_287:                              #   in Loop: Header=BB3_256 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_288:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_290
# BB#289:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_291
.LBB3_290:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_291:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 640(%rbx)
	movswq	4(%rsp), %rcx
	cmpq	$-32768, %rcx           # imm = 0x8000
	movzwl	636(%rbx), %esi
	movswq	%si, %rax
	jne	.LBB3_294
# BB#292:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%si, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_294
# BB#293:                               #   in Loop: Header=BB3_256 Depth=1
	movl	$32767, %esi            # imm = 0x7FFF
	jmp	.LBB3_295
	.p2align	4, 0x90
.LBB3_294:                              #   in Loop: Header=BB3_256 Depth=1
	movl	%eax, %edx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_295:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	movswq	%si, %rsi
	subq	%rsi, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_297
# BB#296:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%di, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_298
.LBB3_297:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	imull	%ecx, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_298:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%si, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 638(%rbx)
	movzwl	634(%rbx), %ecx
	movswq	%cx, %rax
	movzwl	%r9w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_301
# BB#299:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%cx, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_301
# BB#300:                               #   in Loop: Header=BB3_256 Depth=1
	movl	$32767, %ecx            # imm = 0x7FFF
	jmp	.LBB3_302
	.p2align	4, 0x90
.LBB3_301:                              #   in Loop: Header=BB3_256 Depth=1
	movl	%eax, %ecx
	imull	%r9d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_302:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	movswq	%cx, %rcx
	subq	%rcx, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rdi           # imm = 0x8000
	cmovgq	%rdx, %rdi
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rdi
	movzwl	%r9w, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_304
# BB#303:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%di, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	movl	$32767, %ecx            # imm = 0x7FFF
	je	.LBB3_305
.LBB3_304:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rcx
	imull	%r9d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_305:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%cx, %rcx
	leaq	(%rcx,%rax), %rdx
	leaq	32768(%rcx,%rax), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	setle	%cl
	addl	$32767, %ecx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%dx, %cx
	movw	%cx, 636(%rbx)
	movzwl	632(%rbx), %ecx
	movswq	%cx, %rax
	movzwl	%r8w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_308
# BB#306:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%cx, %ecx
	cmpl	$32768, %ecx            # imm = 0x8000
	jne	.LBB3_308
# BB#307:                               #   in Loop: Header=BB3_256 Depth=1
	movl	$32767, %ecx            # imm = 0x7FFF
	jmp	.LBB3_309
	.p2align	4, 0x90
.LBB3_308:                              #   in Loop: Header=BB3_256 Depth=1
	movl	%eax, %ecx
	imull	%r8d, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	shrl	$15, %ecx
	movzwl	%cx, %ecx
.LBB3_309:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%di, %rdx
	movswq	%cx, %rcx
	subq	%rcx, %rdx
	cmpq	$-32769, %rdx           # imm = 0xFFFF7FFF
	movq	$-32768, %rcx           # imm = 0x8000
	cmovgq	%rdx, %rcx
	cmpq	$32766, %rdx            # imm = 0x7FFE
	cmovgq	%r10, %rcx
	movzwl	%r8w, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	jne	.LBB3_311
# BB#310:                               #   in Loop: Header=BB3_256 Depth=1
	movzwl	%cx, %edx
	cmpl	$32768, %edx            # imm = 0x8000
	movl	$32767, %esi            # imm = 0x7FFF
	je	.LBB3_312
.LBB3_311:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%cx, %rdx
	imull	%r8d, %edx
	addl	$16384, %edx            # imm = 0x4000
	shrl	$15, %edx
	movzwl	%dx, %esi
.LBB3_312:                              #   in Loop: Header=BB3_256 Depth=1
	movswq	%si, %rdx
	leaq	(%rdx,%rax), %rsi
	leaq	32768(%rdx,%rax), %rax
	xorl	%edx, %edx
	testq	%rsi, %rsi
	setle	%dl
	addl	$32767, %edx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%si, %dx
	movw	%dx, 634(%rbx)
	movw	%cx, 632(%rbx)
	movw	%cx, 80(%r14,%r11,2)
	incq	%r11
	cmpl	$120, %r11d
	jne	.LBB3_256
# BB#313:                               # %Short_term_synthesis_filtering.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	Gsm_Short_Term_Synthesis_Filter, .Lfunc_end3-Gsm_Short_Term_Synthesis_Filter
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
