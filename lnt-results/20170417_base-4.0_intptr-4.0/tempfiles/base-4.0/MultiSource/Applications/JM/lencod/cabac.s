	.text
	.file	"cabac.bc"
	.globl	cabac_new_slice
	.p2align	4, 0x90
	.type	cabac_new_slice,@function
cabac_new_slice:                        # @cabac_new_slice
	.cfi_startproc
# BB#0:
	movl	$0, last_dquant(%rip)
	retq
.Lfunc_end0:
	.size	cabac_new_slice, .Lfunc_end0-cabac_new_slice
	.cfi_endproc

	.globl	CheckAvailabilityOfNeighborsCABAC
	.p2align	4, 0x90
	.type	CheckAvailabilityOfNeighborsCABAC,@function
CheckAvailabilityOfNeighborsCABAC:      # @CheckAvailabilityOfNeighborsCABAC
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 80
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	img(%rip), %rax
	movl	12(%rax), %ecx
	movq	14224(%rax), %r14
	movslq	%ecx, %r15
	xorl	%ebx, %ebx
	movq	%rsp, %r8
	movl	$-1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	callq	*getNeighbour(%rip)
	leaq	24(%rsp), %r8
	xorl	%esi, %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	movl	%r15d, %edi
	callq	*getNeighbour(%rip)
	cmpl	$0, 24(%rsp)
	je	.LBB1_2
# BB#1:
	movq	img(%rip), %rax
	movslq	28(%rsp), %rcx
	imulq	$536, %rcx, %rbx        # imm = 0x218
	addq	14224(%rax), %rbx
.LBB1_2:
	imulq	$536, %r15, %rax        # imm = 0x218
	movq	%rbx, 56(%r14,%rax)
	cmpl	$0, (%rsp)
	je	.LBB1_3
# BB#4:
	movq	img(%rip), %rdx
	movslq	4(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	addq	14224(%rdx), %rcx
	jmp	.LBB1_5
.LBB1_3:
	xorl	%ecx, %ecx
.LBB1_5:
	movq	%rcx, 64(%r14,%rax)
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	CheckAvailabilityOfNeighborsCABAC, .Lfunc_end1-CheckAvailabilityOfNeighborsCABAC
	.cfi_endproc

	.globl	create_contexts_MotionInfo
	.p2align	4, 0x90
	.type	create_contexts_MotionInfo,@function
create_contexts_MotionInfo:             # @create_contexts_MotionInfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$1504, %esi             # imm = 0x5E0
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB2_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	create_contexts_MotionInfo, .Lfunc_end2-create_contexts_MotionInfo
	.cfi_endproc

	.globl	create_contexts_TextureInfo
	.p2align	4, 0x90
	.type	create_contexts_TextureInfo,@function
create_contexts_TextureInfo:            # @create_contexts_TextureInfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$12128, %esi            # imm = 0x2F60
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB3_2
# BB#1:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB3_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	create_contexts_TextureInfo, .Lfunc_end3-create_contexts_TextureInfo
	.cfi_endproc

	.globl	delete_contexts_MotionInfo
	.p2align	4, 0x90
	.type	delete_contexts_MotionInfo,@function
delete_contexts_MotionInfo:             # @delete_contexts_MotionInfo
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB4_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB4_1:
	retq
.Lfunc_end4:
	.size	delete_contexts_MotionInfo, .Lfunc_end4-delete_contexts_MotionInfo
	.cfi_endproc

	.globl	delete_contexts_TextureInfo
	.p2align	4, 0x90
	.type	delete_contexts_TextureInfo,@function
delete_contexts_TextureInfo:            # @delete_contexts_TextureInfo
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB5_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB5_1:
	retq
.Lfunc_end5:
	.size	delete_contexts_TextureInfo, .Lfunc_end5-delete_contexts_TextureInfo
	.cfi_endproc

	.globl	writeFieldModeInfo_CABAC
	.p2align	4, 0x90
	.type	writeFieldModeInfo_CABAC,@function
writeFieldModeInfo_CABAC:               # @writeFieldModeInfo_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -48
.Lcfi17:
	.cfi_offset %r12, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	leaq	8(%r12), %rbx
	movq	%rbx, %rdi
	callq	arienco_bits_written
	movl	%eax, %r15d
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movq	14224(%rax), %rsi
	movq	32(%rcx), %r8
	movslq	12(%rax), %rax
	movl	4(%r14), %ecx
	imulq	$536, %rax, %rax        # imm = 0x218
	xorl	%edx, %edx
	cmpl	$0, 452(%rsi,%rax)
	movl	$0, %edi
	je	.LBB6_2
# BB#1:
	movslq	436(%rsi,%rax), %rdi
	imulq	$536, %rdi, %rdi        # imm = 0x218
	movl	424(%rsi,%rdi), %edi
.LBB6_2:
	cmpl	$0, 456(%rsi,%rax)
	je	.LBB6_4
# BB#3:
	movslq	440(%rsi,%rax), %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	movl	424(%rsi,%rax), %edx
.LBB6_4:
	addl	%edi, %edx
	xorl	%esi, %esi
	testl	%ecx, %ecx
	setne	%sil
	movslq	%edx, %rbp
	movq	%rbp, %rax
	shlq	$4, %rax
	leaq	1392(%r8,%rax), %rdx
	movq	%rbx, %rdi
	callq	biari_encode_symbol
	movl	%ebp, 24(%r14)
	movq	(%r12), %rax
	movl	$1, 40(%rax)
	movq	%rbx, %rdi
	callq	arienco_bits_written
	subl	%r15d, %eax
	movl	%eax, 12(%r14)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	writeFieldModeInfo_CABAC, .Lfunc_end6-writeFieldModeInfo_CABAC
	.cfi_endproc

	.globl	writeMB_skip_flagInfo_CABAC
	.p2align	4, 0x90
	.type	writeMB_skip_flagInfo_CABAC,@function
writeMB_skip_flagInfo_CABAC:            # @writeMB_skip_flagInfo_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 80
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leaq	8(%rsi), %r14
	movq	%r14, %rdi
	callq	arienco_bits_written
	movq	img(%rip), %rdx
	movq	14216(%rdx), %rcx
	movq	32(%rcx), %rdi
	movq	14224(%rdx), %r13
	movslq	12(%rdx), %rsi
	movl	4(%rbx), %ecx
	imulq	$536, %rsi, %rbp        # imm = 0x218
	movq	56(%r13,%rbp), %rsi
	cmpl	$1, 20(%rdx)
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jne	.LBB7_13
# BB#1:
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.LBB7_3
# BB#2:
	cmpl	$0, 528(%rsi)
	sete	%dl
.LBB7_3:
	movq	64(%r13,%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB7_4
# BB#5:
	cmpl	$1, 528(%rsi)
	movl	$7, %r15d
	adcl	$0, %r15d
	jmp	.LBB7_6
.LBB7_13:
	xorl	%r15d, %r15d
	testq	%rsi, %rsi
	movl	$0, %edx
	je	.LBB7_15
# BB#14:
	xorl	%edx, %edx
	cmpl	$0, 528(%rsi)
	sete	%dl
.LBB7_15:
	movq	64(%r13,%rbp), %rsi
	testq	%rsi, %rsi
	je	.LBB7_17
# BB#16:
	xorl	%r15d, %r15d
	cmpl	$0, 528(%rsi)
	sete	%r15b
.LBB7_17:
	addl	%edx, %r15d
	movq	%r15, %rdx
	shlq	$4, %rdx
	leaq	176(%rdi,%rdx), %rdx
	testl	%ecx, %ecx
	je	.LBB7_18
# BB#19:
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	jmp	.LBB7_20
.LBB7_4:
	movl	$7, %r15d
.LBB7_6:
	addl	%edx, %r15d
	testl	%ecx, %ecx
	jne	.LBB7_9
# BB#7:
	cmpl	$0, 8(%rbx)
	je	.LBB7_8
.LBB7_9:
	movl	%r15d, %ecx
	shlq	$4, %rcx
	leaq	352(%rdi,%rcx), %rdx
	xorl	%esi, %esi
.LBB7_10:
	movq	%r14, %rdi
	callq	biari_encode_symbol
	cmpl	$0, 4(%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	je	.LBB7_12
# BB#11:
	xorl	%r12d, %r12d
	jmp	.LBB7_21
.LBB7_18:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	movb	$1, %r12b
.LBB7_20:
	movq	8(%rsp), %rcx           # 8-byte Reload
	jmp	.LBB7_21
.LBB7_12:
	cmpl	$0, 8(%rbx)
	sete	%r12b
.LBB7_21:
	movzbl	%r12b, %eax
	movl	%eax, 528(%r13,%rbp)
	movl	%r15d, 24(%rbx)
	movq	(%rcx), %rax
	movl	$1, 40(%rax)
	movq	%r14, %rdi
	callq	arienco_bits_written
	subl	20(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_8:
	movl	%r15d, %ecx
	shlq	$4, %rcx
	leaq	352(%rdi,%rcx), %rdx
	movl	$1, %esi
	jmp	.LBB7_10
.Lfunc_end7:
	.size	writeMB_skip_flagInfo_CABAC, .Lfunc_end7-writeMB_skip_flagInfo_CABAC
	.cfi_endproc

	.globl	writeMB_transform_size_CABAC
	.p2align	4, 0x90
	.type	writeMB_transform_size_CABAC,@function
writeMB_transform_size_CABAC:           # @writeMB_transform_size_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 48
.Lcfi39:
	.cfi_offset %rbx, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	leaq	8(%rbp), %rbx
	movq	%rbx, %rdi
	callq	arienco_bits_written
	movl	%eax, %r15d
	movq	img(%rip), %rax
	movq	14216(%rax), %rdx
	movq	14224(%rax), %rcx
	movq	32(%rdx), %r8
	movslq	12(%rax), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	movq	56(%rcx,%rdx), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	movl	$0, %esi
	je	.LBB8_2
# BB#1:
	movl	472(%rax), %esi
.LBB8_2:
	movq	64(%rcx,%rdx), %rax
	testq	%rax, %rax
	je	.LBB8_4
# BB#3:
	movl	472(%rax), %edi
.LBB8_4:
	addl	%esi, %edi
	xorl	%esi, %esi
	cmpl	$0, 472(%rcx,%rdx)
	movl	%edi, 24(%r14)
	setne	%sil
	movslq	%edi, %rax
	shlq	$4, %rax
	leaq	1456(%r8,%rax), %rdx
	movq	%rbx, %rdi
	callq	biari_encode_symbol
	movq	(%rbp), %rax
	movl	$1, 40(%rax)
	movq	%rbx, %rdi
	callq	arienco_bits_written
	subl	%r15d, %eax
	movl	%eax, 12(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	writeMB_transform_size_CABAC, .Lfunc_end8-writeMB_transform_size_CABAC
	.cfi_endproc

	.globl	writeMB_typeInfo_CABAC
	.p2align	4, 0x90
	.type	writeMB_typeInfo_CABAC,@function
writeMB_typeInfo_CABAC:                 # @writeMB_typeInfo_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 80
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	8(%rbp), %r14
	movq	%r14, %rdi
	callq	arienco_bits_written
	movl	%eax, %r15d
	movq	img(%rip), %rax
	movl	20(%rax), %edx
	movq	14216(%rax), %rcx
	movq	32(%rcx), %r13
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rax
	movl	4(%rbx), %r12d
	cmpl	$1, %edx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB9_9
# BB#1:
	cmpl	$2, %edx
	jne	.LBB9_16
# BB#2:
	imulq	$536, %rax, %rsi        # imm = 0x218
	movq	56(%rcx,%rsi), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movl	$0, %edx
	je	.LBB9_4
# BB#3:
	movl	72(%rdi), %edx
	cmpl	$13, %edx
	movq	%rbx, %rdi
	setne	%bl
	cmpl	$9, %edx
	setne	%dl
	andb	%bl, %dl
	movq	%rdi, %rbx
	movzbl	%dl, %edx
.LBB9_4:
	movq	64(%rcx,%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB9_6
# BB#5:
	movl	72(%rcx), %eax
	cmpl	$13, %eax
	setne	%cl
	cmpl	$9, %eax
	setne	%al
	andb	%cl, %al
	movzbl	%al, %eax
.LBB9_6:
	addl	%edx, %eax
	movl	%eax, 24(%rbx)
	testl	%r12d, %r12d
	je	.LBB9_19
# BB#7:
	movl	%eax, %edx
	shlq	$4, %rdx
	addq	%r13, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	cmpl	$25, %r12d
	jne	.LBB9_21
.LBB9_8:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_final
	jmp	.LBB9_42
.LBB9_9:
	imulq	$536, %rax, %rsi        # imm = 0x218
	movq	56(%rcx,%rsi), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movl	$0, %edx
	je	.LBB9_11
# BB#10:
	xorl	%edx, %edx
	cmpl	$0, 72(%rdi)
	setne	%dl
.LBB9_11:
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	movq	64(%rcx,%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB9_13
# BB#12:
	xorl	%eax, %eax
	cmpl	$0, 72(%rcx)
	setne	%al
.LBB9_13:
	addl	%edx, %eax
	movl	%eax, 24(%rbx)
	leal	-24(%r12), %ecx
	xorl	%edx, %edx
	cmpl	$24, %r12d
	cmovgel	%ecx, %edx
	cmpl	$25, %r12d
	movl	$24, %ebp
	cmovll	%r12d, %ebp
	testl	%ebp, %ebp
	je	.LBB9_20
# BB#14:
	cmpl	$2, %ebp
	jg	.LBB9_23
# BB#15:
	shlq	$4, %rax
	leaq	352(%r13,%rax), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	416(%r13), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	xorl	%esi, %esi
	cmpl	$1, %ebp
	setne	%sil
	addq	$448, %r13              # imm = 0x1C0
	jmp	.LBB9_31
.LBB9_16:
	cmpl	$8, %r12d
	movl	$7, %eax
	cmovll	%r12d, %eax
	cmpl	$7, %eax
	ja	.LBB9_50
# BB#17:
	jmpq	*.LJTI9_0(,%rax,8)
.LBB9_18:
	leaq	240(%r13), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	256(%r13), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	addq	$272, %r13              # imm = 0x110
	movl	$1, %esi
	jmp	.LBB9_41
.LBB9_19:
	movl	%eax, %eax
	shlq	$4, %rax
	addq	%rax, %r13
	jmp	.LBB9_40
.LBB9_20:
	shlq	$4, %rax
	leaq	352(%r13,%rax), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	jmp	.LBB9_33
.LBB9_21:
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	movq	%rbx, %r15
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_final
	decl	%r12d
	movslq	%r12d, %rbp
	imulq	$715827883, %rbp, %rbx  # imm = 0x2AAAAAAB
	movq	%rbx, %rax
	shrq	$63, %rax
	sarq	$33, %rbx
	addl	%eax, %ebx
	leaq	64(%r13), %rdx
	movswl	%bx, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	shll	$2, %ebx
	leal	(%rbx,%rbx,2), %eax
	subl	%eax, %ebp
	leal	3(%rbp), %eax
	leaq	80(%r13), %rdx
	cmpl	$6, %eax
	ja	.LBB9_25
# BB#22:
	xorl	%esi, %esi
	jmp	.LBB9_26
.LBB9_23:
	cmpl	$10, %ebp
	jg	.LBB9_28
# BB#24:
	shlq	$4, %rax
	leaq	352(%r13,%rax), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	416(%r13), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	432(%r13), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	addl	$-3, %ebp
	movl	%ebp, %esi
	shrl	$2, %esi
	addq	$448, %r13              # imm = 0x1C0
	andl	$1, %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	biari_encode_symbol
	movl	%ebp, %esi
	shrl	%esi
	andl	$1, %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	biari_encode_symbol
	jmp	.LBB9_27
.LBB9_25:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	movl	%ebp, %eax
	andl	$-4, %eax
	xorl	%esi, %esi
	cmpl	$4, %eax
	setne	%sil
	leaq	96(%r13), %rdx
.LBB9_26:
	movq	%r14, %rdi
	callq	biari_encode_symbol
	movq	%r15, %rbx
	movl	%ebp, %esi
	shrl	%esi
	leaq	112(%r13), %rdx
	andl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	subq	$-128, %r13
.LBB9_27:                               # %.thread
	andl	$1, %ebp
	movq	%r14, %rdi
	movl	%ebp, %esi
	jmp	.LBB9_32
.LBB9_28:
	cmpl	$22, %ebp
	je	.LBB9_30
# BB#29:
	cmpl	$11, %ebp
	jne	.LBB9_47
.LBB9_30:
	shlq	$4, %rax
	leaq	352(%r13,%rax), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	416(%r13), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	432(%r13), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	addq	$448, %r13              # imm = 0x1C0
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	biari_encode_symbol
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	biari_encode_symbol
	xorl	%esi, %esi
	cmpl	$11, %ebp
	setne	%sil
.LBB9_31:                               # %.thread
	movq	%r14, %rdi
.LBB9_32:                               # %.thread
	movq	%r13, %rdx
.LBB9_33:                               # %.thread
	callq	biari_encode_symbol
.LBB9_34:                               # %.thread
	movl	4(%rsp), %r15d          # 4-byte Reload
	jmp	.LBB9_42
.LBB9_35:
	leaq	240(%r13), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	256(%r13), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	addq	$288, %r13              # imm = 0x120
	movl	$1, %esi
	jmp	.LBB9_41
.LBB9_36:
	leaq	240(%r13), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	256(%r13), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	addq	$272, %r13              # imm = 0x110
	jmp	.LBB9_40
.LBB9_37:
	leaq	240(%r13), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	256(%r13), %rdx
	jmp	.LBB9_39
.LBB9_38:
	leaq	240(%r13), %rdx
.LBB9_39:                               # %.thread
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	addq	$288, %r13              # imm = 0x120
.LBB9_40:                               # %.thread
	xorl	%esi, %esi
.LBB9_41:                               # %.thread
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	biari_encode_symbol
.LBB9_42:                               # %.thread
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	$1, 40(%rax)
	movq	%r14, %rdi
	callq	arienco_bits_written
	subl	%r15d, %eax
	movl	%eax, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_43:                               # %.thread261
	leal	-7(%r12), %eax
	xorl	%ebp, %ebp
	cmpl	$7, %r12d
	cmovgel	%eax, %ebp
	leaq	240(%r13), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	288(%r13), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
.LBB9_44:
	cmpl	$25, %ebp
	movq	%rbp, %rax
	je	.LBB9_8
# BB#45:
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	movq	%rbx, %r12
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r15
	callq	biari_encode_symbol_final
	movslq	%r15d, %rax
	imulq	$715827883, %rax, %rbx  # imm = 0x2AAAAAAB
	movq	%rbx, %rax
	shrq	$63, %rax
	sarq	$33, %rbx
	addl	%eax, %ebx
	leaq	304(%r13), %rdx
	movswl	%bx, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	shll	$2, %ebx
	leal	(%rbx,%rbx,2), %eax
	movl	%r15d, %ebp
	subl	%eax, %ebp
	negl	%eax
	leal	3(%r15,%rax), %eax
	leaq	320(%r13), %r15
	cmpl	$6, %eax
	ja	.LBB9_48
# BB#46:
	xorl	%esi, %esi
	jmp	.LBB9_49
.LBB9_47:
	xorl	%r12d, %r12d
	cmpl	$22, %ebp
	setg	%r12b
	movl	%ebp, %r15d
	subl	%r12d, %r15d
	negl	%r12d
	shlq	$4, %rax
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leaq	352(%r13,%rax), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	416(%r13), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leaq	432(%r13), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	leal	-12(%rbp,%r12), %ebp
	movl	%ebp, %esi
	shrl	$3, %esi
	leaq	448(%r13), %r12
	andl	$1, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	biari_encode_symbol
	movl	%ebp, %esi
	shrl	$2, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	biari_encode_symbol
	movl	%ebp, %esi
	shrl	%esi
	andl	$1, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	biari_encode_symbol
	andl	$1, %ebp
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%r12, %rdx
	callq	biari_encode_symbol
	xorl	%eax, %eax
	cmpl	$21, %r15d
	setg	%al
	addl	%r15d, %eax
	cmpl	$24, %eax
	movl	4(%rsp), %r15d          # 4-byte Reload
	jne	.LBB9_42
	jmp	.LBB9_44
.LBB9_48:
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	biari_encode_symbol
	movl	%ebp, %eax
	andl	$-4, %eax
	xorl	%esi, %esi
	cmpl	$4, %eax
	setne	%sil
.LBB9_49:
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	biari_encode_symbol
	movl	%ebp, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%ebp, %eax
	andl	$-4, %eax
	subl	%eax, %ebp
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	movl	%eax, %ebx
	andl	$65534, %ebx            # imm = 0xFFFE
	shrl	%eax
	addq	$336, %r13              # imm = 0x150
	movswl	%ax, %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	biari_encode_symbol
	subl	%ebx, %ebp
	movswl	%bp, %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	callq	biari_encode_symbol
	movq	%r12, %rbx
	jmp	.LBB9_34
.LBB9_50:
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end9:
	.size	writeMB_typeInfo_CABAC, .Lfunc_end9-writeMB_typeInfo_CABAC
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI9_0:
	.quad	.LBB9_42
	.quad	.LBB9_36
	.quad	.LBB9_35
	.quad	.LBB9_37
	.quad	.LBB9_18
	.quad	.LBB9_18
	.quad	.LBB9_38
	.quad	.LBB9_43

	.text
	.globl	writeB8_typeInfo_CABAC
	.p2align	4, 0x90
	.type	writeB8_typeInfo_CABAC,@function
writeB8_typeInfo_CABAC:                 # @writeB8_typeInfo_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 64
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	leaq	8(%r15), %r13
	movq	%r13, %rdi
	callq	arienco_bits_written
	movq	img(%rip), %rcx
	cmpl	$1, 20(%rcx)
	movl	%eax, %r12d
	movq	14216(%rcx), %rax
	movq	32(%rax), %rbp
	movl	4(%r14), %ebx
	jne	.LBB10_1
# BB#10:
	leaq	672(%rbp), %rdx
	testl	%ebx, %ebx
	je	.LBB10_11
# BB#12:
	movl	$1, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	leaq	688(%rbp), %rdx
	cmpl	$2, %ebx
	jg	.LBB10_14
# BB#13:
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	xorl	%esi, %esi
	cmpl	$1, %ebx
	setne	%sil
	addq	$720, %rbp              # imm = 0x2D0
	movq	%r13, %rdi
	jmp	.LBB10_21
.LBB10_1:
	cmpl	$3, %ebx
	ja	.LBB10_23
# BB#2:
	movl	%ebx, %eax
	jmpq	*.LJTI10_0(,%rax,8)
.LBB10_3:
	addq	$544, %rbp              # imm = 0x220
	jmp	.LBB10_4
.LBB10_11:
	xorl	%esi, %esi
	movq	%r13, %rdi
	jmp	.LBB10_22
.LBB10_14:
	movl	$1, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	leaq	704(%rbp), %rdx
	cmpl	$6, %ebx
	jg	.LBB10_16
# BB#15:
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	addl	$-3, %ebx
	movl	%ebx, %esi
	shrl	%esi
	addq	$720, %rbp              # imm = 0x2D0
	jmp	.LBB10_19
.LBB10_6:
	leaq	544(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	addq	$576, %rbp              # imm = 0x240
	jmp	.LBB10_7
.LBB10_8:
	leaq	544(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	leaq	576(%rbp), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	addq	$592, %rbp              # imm = 0x250
.LBB10_4:
	movl	$1, %esi
	movq	%r13, %rdi
	jmp	.LBB10_21
.LBB10_9:
	leaq	544(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	leaq	576(%rbp), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	addq	$592, %rbp              # imm = 0x250
.LBB10_7:
	xorl	%esi, %esi
	movq	%r13, %rdi
	jmp	.LBB10_21
.LBB10_16:
	movl	$1, %esi
	movq	%r13, %rdi
	callq	biari_encode_symbol
	addl	$-7, %ebx
	addq	$720, %rbp              # imm = 0x2D0
	testb	$4, %bl
	jne	.LBB10_17
# BB#18:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	movl	%ebx, %esi
	shrl	%esi
.LBB10_19:
	andl	$1, %esi
	jmp	.LBB10_20
.LBB10_17:
	movl	$1, %esi
.LBB10_20:
	movq	%r13, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	andl	$1, %ebx
	movq	%r13, %rdi
	movl	%ebx, %esi
.LBB10_21:
	movq	%rbp, %rdx
.LBB10_22:
	callq	biari_encode_symbol
.LBB10_23:
	movq	(%r15), %rax
	movl	$1, 40(%rax)
	movq	%r13, %rdi
	callq	arienco_bits_written
	subl	%r12d, %eax
	movl	%eax, 12(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	writeB8_typeInfo_CABAC, .Lfunc_end10-writeB8_typeInfo_CABAC
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI10_0:
	.quad	.LBB10_3
	.quad	.LBB10_6
	.quad	.LBB10_8
	.quad	.LBB10_9

	.text
	.globl	writeIntraPredMode_CABAC
	.p2align	4, 0x90
	.type	writeIntraPredMode_CABAC,@function
writeIntraPredMode_CABAC:               # @writeIntraPredMode_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 48
.Lcfi74:
	.cfi_offset %rbx, -48
.Lcfi75:
	.cfi_offset %r12, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	leaq	8(%r12), %rbx
	movq	%rbx, %rdi
	callq	arienco_bits_written
	movl	%eax, %r15d
	movq	img(%rip), %rax
	movq	14216(%rax), %rax
	movq	40(%rax), %rbp
	cmpl	$-1, 4(%r14)
	je	.LBB11_1
# BB#2:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	movl	4(%r14), %esi
	addq	$16, %rbp
	andl	$1, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	movl	4(%r14), %esi
	shrl	%esi
	andl	$1, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	movl	4(%r14), %esi
	shrl	$2, %esi
	andl	$1, %esi
	jmp	.LBB11_3
.LBB11_1:
	movl	$1, %esi
.LBB11_3:
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	movq	(%r12), %rax
	movl	$1, 40(%rax)
	movq	%rbx, %rdi
	callq	arienco_bits_written
	subl	%r15d, %eax
	movl	%eax, 12(%r14)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	writeIntraPredMode_CABAC, .Lfunc_end11-writeIntraPredMode_CABAC
	.cfi_endproc

	.globl	writeRefFrame_CABAC
	.p2align	4, 0x90
	.type	writeRefFrame_CABAC,@function
writeRefFrame_CABAC:                    # @writeRefFrame_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi82:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi83:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 144
.Lcfi86:
	.cfi_offset %rbx, -56
.Lcfi87:
	.cfi_offset %r12, -48
.Lcfi88:
	.cfi_offset %r13, -40
.Lcfi89:
	.cfi_offset %r14, -32
.Lcfi90:
	.cfi_offset %r15, -24
.Lcfi91:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	leaq	8(%rsi), %r15
	movq	%r15, %rdi
	callq	arienco_bits_written
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movq	32(%rcx), %r12
	movq	14224(%rax), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	12(%rax), %ecx
	movl	20(%rax), %r14d
	movslq	%ecx, %rbp
	movq	enc_picture(%rip), %rcx
	movq	6488(%rcx), %rcx
	movq	%rbx, %r13
	movslq	8(%rbx), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	96(%rax), %ecx
	movl	100(%rax), %edx
	leal	-1(,%rcx,4), %esi
	shll	$2, %edx
	leaq	56(%rsp), %rcx
	movl	%ebp, %edi
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	96(%rax), %esi
	movl	100(%rax), %eax
	shll	$2, %esi
	leal	-1(,%rax,4), %edx
	leaq	32(%rsp), %rcx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	%ebp, %edi
	callq	getLuma4x4Neighbour
	movl	64(%rsp), %r9d
	movl	68(%rsp), %edx
	xorl	%r8d, %r8d
	cmpl	$0, 32(%rsp)
	je	.LBB12_1
# BB#2:
	movl	40(%rsp), %ebp
	movl	44(%rsp), %ecx
	movq	img(%rip), %rax
	movq	14224(%rax), %rsi
	movslq	36(%rsp), %rdi
	imulq	$536, %rdi, %rdi        # imm = 0x218
	cmpl	$0, 72(%rsi,%rdi)
	je	.LBB12_3
.LBB12_5:
	movl	%r14d, %ebx
	cmpl	$1, %ebx
	movq	%r13, %r14
	jne	.LBB12_7
# BB#6:
	shrl	%ebp
	andl	$1, %ebp
	andl	$2, %ecx
	orl	%ebp, %ecx
	movl	%ecx, %ecx
	leaq	(%rsi,%rdi), %rbp
	movl	376(%rbp,%rcx,4), %ecx
	testl	%ecx, %ecx
	je	.LBB12_13
.LBB12_7:
	cmpl	$0, 15268(%rax)
	je	.LBB12_11
# BB#8:
	imulq	$536, 16(%rsp), %rax    # 8-byte Folded Reload
                                        # imm = 0x218
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 424(%rcx,%rax)
	jne	.LBB12_11
# BB#9:
	cmpl	$1, 424(%rsi,%rdi)
	jne	.LBB12_11
# BB#10:
	movslq	52(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movslq	48(%rsp), %rcx
	xorl	%r8d, %r8d
	cmpb	$1, (%rax,%rcx)
	jmp	.LBB12_12
.LBB12_3:
	cmpl	$1, 20(%rax)
	jne	.LBB12_5
# BB#4:
	movl	giRDOpt_B8OnlyFlag(%rip), %ebx
	testl	%ebx, %ebx
	jne	.LBB12_5
.LBB12_1:
	movl	%r14d, %ebx
	movq	%r13, %r14
	cmpl	$0, 56(%rsp)
	jne	.LBB12_15
	jmp	.LBB12_14
.LBB12_11:
	movslq	52(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movslq	48(%rsp), %rcx
	xorl	%r8d, %r8d
	cmpb	$0, (%rax,%rcx)
.LBB12_12:
	setg	%r8b
	addl	%r8d, %r8d
.LBB12_13:
	cmpl	$0, 56(%rsp)
	je	.LBB12_14
.LBB12_15:
	movq	img(%rip), %rax
	movq	14224(%rax), %rsi
	movslq	60(%rsp), %rcx
	imulq	$536, %rcx, %rdi        # imm = 0x218
	cmpl	$0, 72(%rsi,%rdi)
	je	.LBB12_16
.LBB12_18:
	cmpl	$1, %ebx
	jne	.LBB12_20
# BB#19:
	shrl	%r9d
	andl	$1, %r9d
	andl	$2, %edx
	orl	%r9d, %edx
	movl	%edx, %ecx
	leaq	(%rsi,%rdi), %rdx
	movl	376(%rdx,%rcx,4), %ecx
	testl	%ecx, %ecx
	je	.LBB12_14
.LBB12_20:
	cmpl	$0, 15268(%rax)
	movl	4(%rsp), %ebx           # 4-byte Reload
	je	.LBB12_24
# BB#21:
	imulq	$536, 16(%rsp), %rax    # 8-byte Folded Reload
                                        # imm = 0x218
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 424(%rcx,%rax)
	jne	.LBB12_24
# BB#22:
	cmpl	$1, 424(%rsi,%rdi)
	jne	.LBB12_24
# BB#23:
	movslq	76(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movslq	72(%rsp), %rcx
	cmpb	$1, (%rax,%rcx)
	jmp	.LBB12_25
.LBB12_16:
	cmpl	$1, 20(%rax)
	jne	.LBB12_18
# BB#17:
	movl	giRDOpt_B8OnlyFlag(%rip), %ecx
	testl	%ecx, %ecx
	jne	.LBB12_18
.LBB12_14:
	xorl	%eax, %eax
	movl	4(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB12_26
.LBB12_24:
	movslq	76(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movslq	72(%rsp), %rcx
	cmpb	$0, (%rax,%rcx)
.LBB12_25:
	setg	%al
.LBB12_26:
	movzbl	%al, %eax
	orl	%r8d, %eax
	movl	%eax, 24(%r14)
	movl	4(%r14), %ebp
	shlq	$4, %rax
	leaq	1136(%r12,%rax), %rdx
	testl	%ebp, %ebp
	je	.LBB12_27
# BB#28:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	biari_encode_symbol
	leaq	1200(%r12), %rdx
	cmpl	$1, %ebp
	jne	.LBB12_29
.LBB12_27:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	biari_encode_symbol
	jmp	.LBB12_33
.LBB12_29:
	movl	%ebx, %r13d
	movl	$1, %esi
	movq	%r15, %rdi
	callq	biari_encode_symbol
	addq	$1216, %r12             # imm = 0x4C0
	cmpl	$2, %ebp
	je	.LBB12_32
# BB#30:                                # %.lr.ph.i.preheader
	movl	$2, %ebx
	subl	%ebp, %ebx
	.p2align	4, 0x90
.LBB12_31:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	biari_encode_symbol
	incl	%ebx
	jne	.LBB12_31
.LBB12_32:                              # %._crit_edge.i
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	biari_encode_symbol
	movl	%r13d, %ebx
.LBB12_33:                              # %unary_bin_encode.exit
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	$1, 40(%rax)
	movq	%r15, %rdi
	callq	arienco_bits_written
	subl	%ebx, %eax
	movl	%eax, 12(%r14)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	writeRefFrame_CABAC, .Lfunc_end12-writeRefFrame_CABAC
	.cfi_endproc

	.globl	unary_bin_encode
	.p2align	4, 0x90
	.type	unary_bin_encode,@function
unary_bin_encode:                       # @unary_bin_encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 48
.Lcfi97:
	.cfi_offset %rbx, -48
.Lcfi98:
	.cfi_offset %r12, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
	movq	%rdx, %r15
	movl	%esi, %r12d
	movq	%rdi, %r14
	testl	%r12d, %r12d
	je	.LBB13_1
# BB#3:
	movl	$1, %ebp
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	biari_encode_symbol
	movslq	%ebx, %rbx
	shlq	$4, %rbx
	addq	%r15, %rbx
	cmpl	$1, %r12d
	je	.LBB13_6
# BB#4:                                 # %.lr.ph.preheader
	subl	%r12d, %ebp
	.p2align	4, 0x90
.LBB13_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	biari_encode_symbol
	incl	%ebp
	jne	.LBB13_5
.LBB13_6:                               # %._crit_edge
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	jmp	.LBB13_2
.LBB13_1:
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
.LBB13_2:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	biari_encode_symbol     # TAILCALL
.Lfunc_end13:
	.size	unary_bin_encode, .Lfunc_end13-unary_bin_encode
	.cfi_endproc

	.globl	writeDquant_CABAC
	.p2align	4, 0x90
	.type	writeDquant_CABAC,@function
writeDquant_CABAC:                      # @writeDquant_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 64
.Lcfi109:
	.cfi_offset %rbx, -56
.Lcfi110:
	.cfi_offset %r12, -48
.Lcfi111:
	.cfi_offset %r13, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	leaq	8(%r12), %rbx
	movq	%rbx, %rdi
	callq	arienco_bits_written
	movl	%eax, %r15d
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movq	14224(%rax), %rdx
	movq	32(%rcx), %rbp
	movl	4(%r14), %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	movslq	12(%rax), %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	movl	500(%rdx,%rax), %eax
	movl	%eax, last_dquant(%rip)
	xorl	%edx, %edx
	testl	%ecx, %ecx
	setle	%dl
	leal	(%rdx,%rsi,2), %r13d
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	shlq	$4, %rcx
	leaq	1328(%rbp,%rcx), %rdx
	cmpl	$1, %r13d
	je	.LBB14_1
# BB#2:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	biari_encode_symbol
	leaq	1360(%rbp), %rdx
	cmpl	$2, %r13d
	jne	.LBB14_3
.LBB14_1:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	biari_encode_symbol
	jmp	.LBB14_7
.LBB14_3:
	movl	%r15d, 4(%rsp)          # 4-byte Spill
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	biari_encode_symbol
	addq	$1376, %rbp             # imm = 0x560
	cmpl	$3, %r13d
	je	.LBB14_6
# BB#4:                                 # %.lr.ph.i.preheader
	movl	$3, %r15d
	subl	%r13d, %r15d
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	incl	%r15d
	jne	.LBB14_5
.LBB14_6:                               # %._crit_edge.i
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	movl	4(%rsp), %r15d          # 4-byte Reload
.LBB14_7:                               # %unary_bin_encode.exit
	movq	(%r12), %rax
	movl	$1, 40(%rax)
	movq	%rbx, %rdi
	callq	arienco_bits_written
	subl	%r15d, %eax
	movl	%eax, 12(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	writeDquant_CABAC, .Lfunc_end14-writeDquant_CABAC
	.cfi_endproc

	.globl	writeMVD_CABAC
	.p2align	4, 0x90
	.type	writeMVD_CABAC,@function
writeMVD_CABAC:                         # @writeMVD_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi121:
	.cfi_def_cfa_offset 160
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	leaq	8(%rsi), %rdi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	callq	arienco_bits_written
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	img(%rip), %rax
	movl	12(%rax), %ecx
	movl	96(%rax), %esi
	leal	(,%rsi,4), %ebx
	movl	100(%rax), %r14d
	leal	(,%r14,4), %edx
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movl	8(%rbp), %r12d
	movl	%r12d, %r15d
	andl	$1, %r15d
	sarl	%r12d
	movq	14216(%rax), %rdi
	movq	14224(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	32(%rdi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movslq	%ecx, %r13
	leal	-1(,%rsi,4), %esi
	leaq	80(%rsp), %rcx
	movl	%r13d, %edi
	callq	getLuma4x4Neighbour
	leal	-1(,%r14,4), %edx
	leaq	56(%rsp), %rcx
	movl	%r13d, %edi
	movl	%ebx, %esi
	callq	getLuma4x4Neighbour
	xorl	%eax, %eax
	cmpl	$0, 56(%rsp)
	movl	$0, %edx
	je	.LBB15_8
# BB#1:
	movq	img(%rip), %rdi
	movq	14224(%rdi), %r8
	movslq	60(%rsp), %rdx
	movslq	68(%rsp), %rbx
	movslq	64(%rsp), %rcx
	movslq	%r12d, %rbp
	imulq	$536, %rdx, %r9         # imm = 0x218
	leaq	(%r8,%r9), %rdx
	movq	%r15, %rsi
	shlq	$7, %rsi
	addq	%rdx, %rsi
	shlq	$5, %rbx
	addq	%rsi, %rbx
	leaq	(%rbx,%rcx,8), %rcx
	movl	76(%rcx,%rbp,4), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$1, %ebp
	jne	.LBB15_8
# BB#2:
	movl	15268(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB15_8
# BB#3:
	imulq	$536, %r13, %rcx        # imm = 0x218
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	424(%rsi,%rcx), %edi
	cmpl	$1, %edi
	je	.LBB15_6
# BB#4:
	testl	%edi, %edi
	jne	.LBB15_8
# BB#5:
	cmpl	$1, 424(%r8,%r9)
	sete	%cl
	shll	%cl, %edx
	jmp	.LBB15_8
.LBB15_6:
	cmpl	$0, 424(%r8,%r9)
	jne	.LBB15_8
# BB#7:
	movl	%edx, %ecx
	shrl	$31, %ecx
	addl	%edx, %ecx
	sarl	%ecx
	movl	%ecx, %edx
.LBB15_8:                               # %thread-pre-split.thread
	cmpl	$0, 80(%rsp)
	movq	48(%rsp), %r14          # 8-byte Reload
	je	.LBB15_16
# BB#9:
	movq	img(%rip), %rdi
	movq	14224(%rdi), %r8
	movslq	84(%rsp), %rax
	movslq	92(%rsp), %rbp
	movslq	88(%rsp), %rbx
	movslq	%r12d, %rcx
	imulq	$536, %rax, %rsi        # imm = 0x218
	leaq	(%r8,%rsi), %rax
	shlq	$7, %r15
	addq	%rax, %r15
	shlq	$5, %rbp
	addq	%r15, %rbp
	leaq	(%rbp,%rbx,8), %rax
	movl	76(%rax,%rcx,4), %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	cmpl	$1, %r12d
	jne	.LBB15_16
# BB#10:
	movl	15268(%rdi), %ecx
	testl	%ecx, %ecx
	je	.LBB15_16
# BB#11:
	imulq	$536, %r13, %rcx        # imm = 0x218
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	424(%rdi,%rcx), %edi
	cmpl	$1, %edi
	je	.LBB15_14
# BB#12:
	testl	%edi, %edi
	jne	.LBB15_16
# BB#13:
	cmpl	$1, 424(%r8,%rsi)
	sete	%cl
	shll	%cl, %eax
	jmp	.LBB15_16
.LBB15_14:
	cmpl	$0, 424(%r8,%rsi)
	jne	.LBB15_16
# BB#15:
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movl	%ecx, %eax
.LBB15_16:                              # %thread-pre-split86.thread
	addl	%edx, %eax
	leal	(%r12,%r12,4), %ecx
	cmpl	$2, %eax
	jg	.LBB15_18
# BB#17:
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	jmp	.LBB15_21
.LBB15_18:
	cmpl	$33, %eax
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	jl	.LBB15_20
# BB#19:
	addl	$3, %ecx
	jmp	.LBB15_21
.LBB15_20:
	addl	$2, %ecx
.LBB15_21:
	movl	4(%r14), %ebp
	movl	%ebp, %eax
	sarl	$31, %eax
	leal	(%rbp,%rax), %ebx
	movl	%ecx, 24(%r14)
	movslq	%ecx, %rcx
	shlq	$4, %rcx
	xorl	%eax, %ebx
	leaq	816(%r13,%rcx), %rdx
	je	.LBB15_22
# BB#23:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	biari_encode_symbol
	decl	%ebx
	leal	(%r12,%r12,4), %eax
	cltq
	shlq	$4, %rax
	leaq	976(%r13,%rax), %rdx
	movl	$3, %ecx
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	unary_exp_golomb_mv_encode
	shrl	$31, %ebp
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	biari_encode_symbol_eq_prob
	jmp	.LBB15_24
.LBB15_22:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	biari_encode_symbol
.LBB15_24:
	movl	12(%rsp), %ebx          # 4-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	$1, 40(%rax)
	movq	%r15, %rdi
	callq	arienco_bits_written
	subl	%ebx, %eax
	movl	%eax, 12(%r14)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	writeMVD_CABAC, .Lfunc_end15-writeMVD_CABAC
	.cfi_endproc

	.globl	unary_exp_golomb_mv_encode
	.p2align	4, 0x90
	.type	unary_exp_golomb_mv_encode,@function
unary_exp_golomb_mv_encode:             # @unary_exp_golomb_mv_encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi131:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi132:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 64
.Lcfi135:
	.cfi_offset %rbx, -56
.Lcfi136:
	.cfi_offset %r12, -48
.Lcfi137:
	.cfi_offset %r13, -40
.Lcfi138:
	.cfi_offset %r14, -32
.Lcfi139:
	.cfi_offset %r15, -24
.Lcfi140:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %rbx
	movl	%esi, %r15d
	movq	%rdi, %r14
	testl	%r15d, %r15d
	je	.LBB16_13
# BB#1:
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	biari_encode_symbol
	addq	$16, %rbx
	cmpl	$1, %r15d
	je	.LBB16_13
# BB#2:                                 # %.lr.ph.preheader
	leal	-1(%r15), %r13d
	addl	$-2, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	2(%rbp), %eax
	cmpl	$8, %eax
	ja	.LBB16_5
# BB#4:                                 #   in Loop: Header=BB16_3 Depth=1
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	biari_encode_symbol
	leaq	16(%rbx), %rax
	testl	%ebp, %ebp
	cmovneq	%rbx, %rax
	leaq	16(%rax), %rbx
	cmpl	%ebp, %r12d
	cmovneq	%rax, %rbx
	incl	%ebp
	cmpl	%ebp, %r13d
	jne	.LBB16_3
.LBB16_5:                               # %.critedge
	cmpl	$7, %r15d
	ja	.LBB16_6
.LBB16_13:
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	biari_encode_symbol     # TAILCALL
.LBB16_6:
	addl	$-8, %r15d
	cmpl	$8, %r15d
	jae	.LBB16_7
# BB#14:                                # %._crit_edge14.i.thread
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
	movl	$3, %ebx
	jmp	.LBB16_10
.LBB16_7:                               # %.lr.ph13.i.preheader
	movl	$3, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB16_8:                               # %.lr.ph13.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
	subl	%ebp, %r15d
	incl	%ebx
	movl	$1, %ebp
	movl	%ebx, %ecx
	shll	%cl, %ebp
	cmpl	%ebp, %r15d
	jae	.LBB16_8
# BB#9:                                 # %._crit_edge14.i
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
.LBB16_10:                              # %.lr.ph.i.preheader
	decl	%ebx
	.p2align	4, 0x90
.LBB16_11:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%r15d, %esi
	movl	%ebx, %ecx
	shrl	%cl, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
	decl	%ebx
	cmpl	$-1, %ebx
	jne	.LBB16_11
# BB#12:                                # %exp_golomb_encode_eq_prob.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	unary_exp_golomb_mv_encode, .Lfunc_end16-unary_exp_golomb_mv_encode
	.cfi_endproc

	.globl	writeCIPredMode_CABAC
	.p2align	4, 0x90
	.type	writeCIPredMode_CABAC,@function
writeCIPredMode_CABAC:                  # @writeCIPredMode_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi144:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi145:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi147:
	.cfi_def_cfa_offset 80
.Lcfi148:
	.cfi_offset %rbx, -56
.Lcfi149:
	.cfi_offset %r12, -48
.Lcfi150:
	.cfi_offset %r13, -40
.Lcfi151:
	.cfi_offset %r14, -32
.Lcfi152:
	.cfi_offset %r15, -24
.Lcfi153:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	leaq	8(%r12), %rbx
	movq	%rbx, %rdi
	callq	arienco_bits_written
	movq	img(%rip), %rsi
	movq	14216(%rsi), %rdx
	movq	14224(%rsi), %rcx
	movq	40(%rdx), %rbp
	movslq	12(%rsi), %rdx
	movl	4(%r14), %r15d
	imulq	$536, %rdx, %rdx        # imm = 0x218
	movq	56(%rcx,%rdx), %rsi
	xorl	%edi, %edi
	testq	%rsi, %rsi
	je	.LBB17_2
# BB#1:
	cmpl	$0, 416(%rsi)
	setne	%dil
.LBB17_2:
	movq	64(%rcx,%rdx), %rdx
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.LBB17_4
# BB#3:
	cmpl	$0, 416(%rdx)
	setne	%cl
.LBB17_4:
	addq	%rdi, %rcx
	shlq	$4, %rcx
	leaq	32(%rbp,%rcx), %rdx
	testl	%r15d, %r15d
	movl	%eax, 12(%rsp)          # 4-byte Spill
	je	.LBB17_5
# BB#6:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	biari_encode_symbol
	addq	$80, %rbp
	movl	%r15d, %r13d
	decl	%r13d
	je	.LBB17_11
# BB#7:
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	cmpl	$2, %r15d
	je	.LBB17_11
# BB#8:                                 # %.lr.ph.i.preheader
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	$2, %r14d
	subl	%r15d, %r14d
	.p2align	4, 0x90
.LBB17_9:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	incl	%r14d
	jne	.LBB17_9
# BB#10:                                # %._crit_edge.i
	cmpl	$1, %r13d
	movq	16(%rsp), %r14          # 8-byte Reload
	ja	.LBB17_13
.LBB17_11:                              # %._crit_edge.i.thread
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	jmp	.LBB17_12
.LBB17_5:
	xorl	%esi, %esi
	movq	%rbx, %rdi
.LBB17_12:                              # %unary_bin_max_encode.exit
	callq	biari_encode_symbol
.LBB17_13:                              # %unary_bin_max_encode.exit
	movq	(%r12), %rax
	movl	$1, 40(%rax)
	movq	%rbx, %rdi
	callq	arienco_bits_written
	subl	12(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 12(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	writeCIPredMode_CABAC, .Lfunc_end17-writeCIPredMode_CABAC
	.cfi_endproc

	.globl	unary_bin_max_encode
	.p2align	4, 0x90
	.type	unary_bin_max_encode,@function
unary_bin_max_encode:                   # @unary_bin_max_encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi157:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi158:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi160:
	.cfi_def_cfa_offset 64
.Lcfi161:
	.cfi_offset %rbx, -56
.Lcfi162:
	.cfi_offset %r12, -48
.Lcfi163:
	.cfi_offset %r13, -40
.Lcfi164:
	.cfi_offset %r14, -32
.Lcfi165:
	.cfi_offset %r15, -24
.Lcfi166:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, %ebp
	movq	%rdx, %r12
	movl	%esi, %r15d
	movq	%rdi, %r13
	testl	%r15d, %r15d
	je	.LBB18_1
# BB#3:
	movl	$1, %ebx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	callq	biari_encode_symbol
	movslq	%ebp, %rbp
	shlq	$4, %rbp
	addq	%r12, %rbp
	cmpl	$1, %r15d
	je	.LBB18_6
# BB#4:                                 # %.lr.ph.preheader
	subl	%r15d, %ebx
	.p2align	4, 0x90
.LBB18_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rbp, %rdx
	callq	biari_encode_symbol
	incl	%ebx
	jne	.LBB18_5
.LBB18_6:                               # %._crit_edge
	cmpl	%r14d, %r15d
	jae	.LBB18_8
# BB#7:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rbp, %rdx
	jmp	.LBB18_2
.LBB18_1:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
.LBB18_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	biari_encode_symbol     # TAILCALL
.LBB18_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	unary_bin_max_encode, .Lfunc_end18-unary_bin_max_encode
	.cfi_endproc

	.globl	writeCBP_BIT_CABAC
	.p2align	4, 0x90
	.type	writeCBP_BIT_CABAC,@function
writeCBP_BIT_CABAC:                     # @writeCBP_BIT_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi167:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi168:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi170:
	.cfi_def_cfa_offset 64
.Lcfi171:
	.cfi_offset %rbx, -32
.Lcfi172:
	.cfi_offset %r14, -24
.Lcfi173:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movl	%esi, %ebp
	movl	%edi, %eax
	leal	(%rax,%rax), %esi
	andl	$2, %esi
	andl	$-2, %eax
	je	.LBB19_1
# BB#6:
	movl	%esi, %ecx
	shrl	%ecx
	btl	%ecx, %edx
.LBB19_7:
	setae	%cl
.LBB19_8:
	movzbl	%cl, %ebx
	testl	%esi, %esi
	jne	.LBB19_14
# BB#9:
	movq	img(%rip), %rcx
	movl	12(%rcx), %edi
	shll	$2, %eax
	leaq	8(%rsp), %rcx
	movl	$-1, %esi
	movl	%eax, %edx
	callq	getLuma4x4Neighbour
	cmpl	$0, 8(%rsp)
	je	.LBB19_10
# BB#11:
	movq	img(%rip), %rax
	movq	14224(%rax), %rax
	movslq	12(%rsp), %rcx
	imulq	$536, %rcx, %rcx        # imm = 0x218
	cmpl	$14, 72(%rax,%rcx)
	jne	.LBB19_13
# BB#12:
	xorl	%eax, %eax
	jmp	.LBB19_16
.LBB19_14:
	btl	%eax, %edx
	jmp	.LBB19_15
.LBB19_1:
	movq	56(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB19_2
# BB#3:
	cmpl	$14, 72(%rcx)
	jne	.LBB19_5
# BB#4:
	xorl	%ecx, %ecx
	jmp	.LBB19_8
.LBB19_10:
	xorl	%eax, %eax
	jmp	.LBB19_16
.LBB19_13:
	movl	364(%rax,%rcx), %eax
	movl	20(%rsp), %ecx
	orl	$1, %ecx
	btl	%ecx, %eax
.LBB19_15:
	setae	%al
.LBB19_16:
	movzbl	%al, %eax
	movq	img(%rip), %rcx
	movq	14216(%rcx), %rcx
	shlq	$4, %rax
	addq	40(%rcx), %rax
	shlq	$5, %rbx
	leaq	96(%rbx,%rax), %rdx
	movswl	%bp, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB19_2:
	xorl	%ecx, %ecx
	jmp	.LBB19_8
.LBB19_5:
	movl	364(%rcx), %ecx
	movl	%esi, %edi
	shrl	%edi
	orl	$2, %edi
	btl	%edi, %ecx
	jmp	.LBB19_7
.Lfunc_end19:
	.size	writeCBP_BIT_CABAC, .Lfunc_end19-writeCBP_BIT_CABAC
	.cfi_endproc

	.globl	writeCBP_CABAC
	.p2align	4, 0x90
	.type	writeCBP_CABAC,@function
writeCBP_CABAC:                         # @writeCBP_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi174:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi175:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi176:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi177:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi178:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi180:
	.cfi_def_cfa_offset 80
.Lcfi181:
	.cfi_offset %rbx, -56
.Lcfi182:
	.cfi_offset %r12, -48
.Lcfi183:
	.cfi_offset %r13, -40
.Lcfi184:
	.cfi_offset %r14, -32
.Lcfi185:
	.cfi_offset %r15, -24
.Lcfi186:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbp
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	callq	arienco_bits_written
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movq	14224(%rax), %r15
	movq	40(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	12(%rax), %rax
	imulq	$536, %rax, %r14        # imm = 0x218
	leaq	(%r15,%r14), %rbx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	4(%rbp), %ebp
	movl	%ebp, %esi
	andl	$1, %esi
	xorl	%edi, %edi
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movq	%r13, %r9
	callq	writeCBP_BIT_CABAC
	movl	%ebp, %esi
	andl	$2, %esi
	movl	$1, %edi
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movq	%r13, %r9
	callq	writeCBP_BIT_CABAC
	movl	%ebp, %esi
	andl	$4, %esi
	movl	$2, %edi
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movq	%r13, %r9
	callq	writeCBP_BIT_CABAC
	movl	%ebp, %esi
	andl	$8, %esi
	movl	$3, %edi
	movl	%ebp, %edx
	movq	%rbx, %rcx
	movq	%r13, %r9
	callq	writeCBP_BIT_CABAC
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB20_27
# BB#1:
	movq	56(%r15,%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB20_4
# BB#2:
	cmpl	$14, 72(%rcx)
	jne	.LBB20_5
# BB#3:
	movl	$2, %eax
	jmp	.LBB20_6
.LBB20_4:
	xorl	%eax, %eax
	jmp	.LBB20_6
.LBB20_5:
	xorl	%eax, %eax
	cmpl	$15, 364(%rcx)
	setg	%al
	addq	%rax, %rax
.LBB20_6:
	movq	64(%r15,%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB20_9
# BB#7:
	cmpl	$14, 72(%rdx)
	jne	.LBB20_10
# BB#8:
	movl	$1, %ecx
	jmp	.LBB20_11
.LBB20_9:
	xorl	%ecx, %ecx
	jmp	.LBB20_11
.LBB20_10:
	xorl	%ecx, %ecx
	cmpl	$15, 364(%rdx)
	setg	%cl
.LBB20_11:
	orq	%rax, %rcx
	xorl	%esi, %esi
	cmpl	$15, %ebp
	setg	%sil
	shlq	$4, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	160(%rax,%rcx), %rdx
	movq	%r13, %rdi
	callq	biari_encode_symbol
	cmpl	$16, %ebp
	jl	.LBB20_27
# BB#12:
	leaq	56(%r15,%r14), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB20_18
# BB#13:
	cmpl	$14, 72(%rax)
	jne	.LBB20_16
# BB#14:
	movl	$2, %eax
	jmp	.LBB20_19
.LBB20_16:
	movl	364(%rax), %edx
	cmpl	$16, %edx
	jl	.LBB20_18
# BB#17:
	andl	$-16, %edx
	xorl	%eax, %eax
	cmpl	$32, %edx
	sete	%al
	addq	%rax, %rax
	jmp	.LBB20_19
.LBB20_18:
	xorl	%eax, %eax
.LBB20_19:
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB20_25
# BB#20:
	cmpl	$14, 72(%rcx)
	jne	.LBB20_23
# BB#21:
	movl	$1, %ecx
	jmp	.LBB20_26
.LBB20_23:
	movl	364(%rcx), %edx
	cmpl	$16, %edx
	jl	.LBB20_25
# BB#24:
	andl	$-16, %edx
	xorl	%ecx, %ecx
	cmpl	$32, %edx
	sete	%cl
	jmp	.LBB20_26
.LBB20_25:
	xorl	%ecx, %ecx
.LBB20_26:
	orq	%rax, %rcx
	andl	$-16, %ebp
	xorl	%esi, %esi
	cmpl	$32, %ebp
	sete	%sil
	shlq	$4, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	224(%rax,%rcx), %rdx
	movq	%r13, %rdi
	callq	biari_encode_symbol
.LBB20_27:
	movq	(%r12), %rax
	movl	$1, 40(%rax)
	movq	%r13, %rdi
	callq	arienco_bits_written
	subl	4(%rsp), %eax           # 4-byte Folded Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 12(%rcx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	writeCBP_CABAC, .Lfunc_end20-writeCBP_CABAC
	.cfi_endproc

	.globl	write_and_store_CBP_block_bit
	.p2align	4, 0x90
	.type	write_and_store_CBP_block_bit,@function
write_and_store_CBP_block_bit:          # @write_and_store_CBP_block_bit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi187:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi188:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi189:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi190:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi191:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi193:
	.cfi_def_cfa_offset 160
.Lcfi194:
	.cfi_offset %rbx, -56
.Lcfi195:
	.cfi_offset %r12, -48
.Lcfi196:
	.cfi_offset %r13, -40
.Lcfi197:
	.cfi_offset %r14, -32
.Lcfi198:
	.cfi_offset %r15, -24
.Lcfi199:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	cmpl	$7, %edx
	jne	.LBB21_1
# BB#2:
	movq	img(%rip), %rax
	cmpl	$0, 108(%rax)
	sete	%r14b
	setne	%r15b
	jmp	.LBB21_3
.LBB21_1:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
.LBB21_3:
	leal	-1(%rdx), %ebp
	leal	-6(%rdx), %esi
	movl	%esi, %ecx
	andb	$15, %cl
	movb	$13, %al
	shrb	%cl, %al
	xorl	%r13d, %r13d
	cmpl	$3, %esi
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%rdx, %rsi
	ja	.LBB21_4
# BB#5:
	andb	$1, %al
	movl	$0, %r12d
	movl	$0, %edx
	je	.LBB21_7
# BB#6:
	movq	img(%rip), %rax
	cmpl	$0, 108(%rax)
	sete	%r12b
	setne	%dl
	jmp	.LBB21_7
.LBB21_4:
	xorl	%r12d, %r12d
	xorl	%edx, %edx
.LBB21_7:                               # %.thread146
	cmpl	$5, %ebp
	setb	%cl
	orb	%r14b, %cl
	orb	%r15b, %cl
	movq	img(%rip), %rax
	cmpb	$1, %cl
	movl	$0, %ebx
	jne	.LBB21_9
# BB#8:
	movl	96(%rax), %ebx
	movl	100(%rax), %r13d
.LBB21_9:                               # %.thread147
	movl	%edx, 20(%rsp)          # 4-byte Spill
	movl	%r12d, 16(%rsp)         # 4-byte Spill
	movq	%rsi, %rcx
	testl	%ecx, %ecx
	sete	13(%rsp)                # 1-byte Folded Spill
	cmpl	$5, %ebp
	setb	15(%rsp)                # 1-byte Folded Spill
	cmpl	$0, 104(%rax)
	setne	14(%rsp)                # 1-byte Folded Spill
	movl	12(%rax), %edi
	leal	(,%rbx,4), %r12d
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	leal	-1(,%rbx,4), %esi
	shll	$2, %r13d
	movq	%rcx, %rbx
	cmpl	$5, %ecx
	ja	.LBB21_11
# BB#10:
	leaq	72(%rsp), %rcx
	movl	%r13d, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leal	-1(%r13), %edx
	leaq	48(%rsp), %rcx
	movl	%r12d, %esi
	callq	getLuma4x4Neighbour
	xorl	%r8d, %r8d
	cmpl	$4, %ebp
	movl	$0, %r11d
	movl	16(%rsp), %r12d         # 4-byte Reload
	movl	20(%rsp), %r10d         # 4-byte Reload
	movb	13(%rsp), %al           # 1-byte Reload
	movl	%ebp, %ecx
	movq	%rbx, %r15
	jbe	.LBB21_12
	jmp	.LBB21_16
.LBB21_11:
	leaq	72(%rsp), %rcx
	movl	%r13d, %edx
	callq	getChroma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leal	-1(%r13), %edx
	leaq	48(%rsp), %rcx
	movl	%r12d, %esi
	callq	getChroma4x4Neighbour
	orb	%r14b, %r15b
	xorl	%r8d, %r8d
	cmpb	$1, %r15b
	movl	$0, %r11d
	movl	16(%rsp), %r12d         # 4-byte Reload
	movl	20(%rsp), %r10d         # 4-byte Reload
	movb	13(%rsp), %al           # 1-byte Reload
	movl	%ebp, %ecx
	movq	%rbx, %r15
	jne	.LBB21_16
.LBB21_12:
	xorl	%r11d, %r11d
	cmpl	$0, 72(%rsp)
	movl	$0, %r8d
	je	.LBB21_14
# BB#13:
	movl	84(%rsp), %r8d
	shll	$2, %r8d
	addl	80(%rsp), %r8d
.LBB21_14:
	cmpl	$0, 48(%rsp)
	je	.LBB21_16
# BB#15:
	movl	60(%rsp), %r11d
	shll	$2, %r11d
	addl	56(%rsp), %r11d
.LBB21_16:
	orb	15(%rsp), %al           # 1-byte Folded Reload
	testl	%r15d, %r15d
	movl	$0, %esi
	je	.LBB21_17
# BB#18:
	cmpl	$4, %ecx
	ja	.LBB21_20
# BB#19:
	orl	$1, %r13d
	addl	24(%rsp), %r13d         # 4-byte Folded Reload
	movl	%r13d, %edx
	jmp	.LBB21_24
.LBB21_17:
	xorl	%edx, %edx
	jmp	.LBB21_24
.LBB21_20:
	cmpb	$1, %r12b
	movl	$17, %edx
	adcl	$0, %edx
	movl	%r12d, %ecx
	orb	%r10b, %cl
	jne	.LBB21_24
# BB#21:
	testb	%r14b, %r14b
	je	.LBB21_23
# BB#22:
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	19(%r13,%rcx), %edx
	jmp	.LBB21_24
.LBB21_23:
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	35(%r13,%rcx), %edx
.LBB21_24:
	orb	%al, %r12b
	movl	44(%rsp), %r9d          # 4-byte Reload
	testl	%r9d, %r9d
	je	.LBB21_33
# BB#25:
	cmpl	$3, %r15d
	je	.LBB21_28
# BB#26:
	cmpl	$2, %r15d
	jne	.LBB21_29
# BB#27:                                # %.thread151
	movl	$1, %eax
	movl	$1, %esi
	movl	%edx, %ecx
	shlq	%cl, %rsi
	leal	1(%rdx), %ecx
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rdi
	leal	4(%rdx), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rbp
	addl	$5, %edx
	movl	%edx, %ecx
	shlq	%cl, %rax
	orq	%rsi, %rdi
	orq	%rbp, %rax
	orq	%rdi, %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	orq	%rax, 408(%rcx)
	jmp	.LBB21_41
.LBB21_33:
	xorl	%ecx, %ecx
	testl	%r15d, %r15d
	setne	%cl
	testb	%al, %al
	movl	$17, %edi
	cmovnel	%ecx, %edi
	testb	%r14b, %r14b
	movl	$19, %ecx
	movl	$35, %ebp
	cmovnel	%ecx, %ebp
	testb	%r10b, %r10b
	movl	$18, %edx
	cmovel	%ebp, %edx
	testb	%r12b, %r12b
	cmovnel	%edi, %edx
	cmpl	$2, %r15d
	jne	.LBB21_34
	jmp	.LBB21_41
.LBB21_28:
	movl	$1, %edi
	movl	%edx, %ecx
	shlq	%cl, %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	orq	408(%rbp), %rdi
	addq	$408, %rbp              # imm = 0x198
	movl	$1, %ecx
	jmp	.LBB21_31
.LBB21_29:
	movl	$1, %edi
	movl	%edx, %ecx
	shlq	%cl, %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	orq	408(%rbp), %rdi
	movq	%rdi, 408(%rbp)
	cmpl	$4, %r15d
	jne	.LBB21_32
# BB#30:
	addq	$408, %rbp              # imm = 0x198
	movl	$4, %ecx
.LBB21_31:                              # %.thread.sink.split
	addl	%edx, %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rdx
	orq	%rdi, %rdx
	movq	%rdx, (%rbp)
.LBB21_32:                              # %.thread
	xorl	%ecx, %ecx
	testl	%r15d, %r15d
	setne	%cl
	testb	%al, %al
	movl	$17, %edi
	cmovnel	%ecx, %edi
	testb	%r14b, %r14b
	movl	$19, %ecx
	movl	$35, %ebp
	cmovnel	%ecx, %ebp
	testb	%r10b, %r10b
	movl	$18, %edx
	cmovel	%ebp, %edx
	testb	%r12b, %r12b
	cmovnel	%edi, %edx
.LBB21_34:
	movb	14(%rsp), %cl           # 1-byte Reload
	movb	%cl, %sil
	cmpl	$0, 48(%rsp)
	movl	%esi, %edi
	je	.LBB21_37
# BB#35:
	movq	img(%rip), %rcx
	movq	14224(%rcx), %rbp
	movslq	52(%rsp), %rcx
	imulq	$536, %rcx, %rbx        # imm = 0x218
	movl	$1, %edi
	cmpl	$14, 72(%rbp,%rbx)
	je	.LBB21_37
# BB#36:
	addl	%edx, %r11d
	movl	$1, %edi
	movl	%r11d, %ecx
	shlq	%cl, %rdi
	andq	408(%rbp,%rbx), %rdi
	sarq	%cl, %rdi
.LBB21_37:
	cmpl	$0, 72(%rsp)
	movq	img(%rip), %rax
	je	.LBB21_40
# BB#38:
	movq	14224(%rax), %rbp
	movslq	76(%rsp), %rcx
	imulq	$536, %rcx, %rbx        # imm = 0x218
	movl	$1, %esi
	cmpl	$14, 72(%rbp,%rbx)
	je	.LBB21_40
# BB#39:
	addl	%r8d, %edx
	movl	$1, %esi
	movl	%edx, %ecx
	shlq	%cl, %rsi
	andq	408(%rbp,%rbx), %rsi
	sarq	%cl, %rsi
.LBB21_40:                              # %._crit_edge
	leal	(%rsi,%rdi,2), %ecx
	movq	14216(%rax), %rax
	movslq	%r15d, %rdx
	movslq	type2ctx_bcbp(,%rdx,4), %rdx
	movslq	%ecx, %rcx
	shlq	$6, %rdx
	addq	40(%rax), %rdx
	shlq	$4, %rcx
	leaq	288(%rcx,%rdx), %rdx
	movswl	%r9w, %esi
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	biari_encode_symbol
.LBB21_41:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	write_and_store_CBP_block_bit, .Lfunc_end21-write_and_store_CBP_block_bit
	.cfi_endproc

	.globl	write_significance_map
	.p2align	4, 0x90
	.type	write_significance_map,@function
write_significance_map:                 # @write_significance_map
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi200:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi201:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi202:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi203:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi204:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi205:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi206:
	.cfi_def_cfa_offset 96
.Lcfi207:
	.cfi_offset %rbx, -56
.Lcfi208:
	.cfi_offset %r12, -48
.Lcfi209:
	.cfi_offset %r13, -40
.Lcfi210:
	.cfi_offset %r14, -32
.Lcfi211:
	.cfi_offset %r15, -24
.Lcfi212:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movslq	%edx, %rbp
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movq	40(%rcx), %rsi
	movb	$1, %cl
	cmpl	$0, 24(%rax)
	jne	.LBB22_2
# BB#1:
	cmpl	$0, 424(%rdi)
	setne	%cl
.LBB22_2:
	movl	maxpos(,%rbp,4), %r8d
	leaq	928(%rsi), %r13
	leaq	7328(%rsi), %rdi
	leaq	3328(%rsi), %rax
	leaq	9728(%rsi), %rsi
	testb	%cl, %cl
	cmovneq	%rdi, %r13
	cmoveq	%rax, %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	cmpl	$7, %ebp
	je	.LBB22_5
# BB#3:
	cmpl	$1, %edx
	jne	.LBB22_4
.LBB22_5:
	addq	$-4, 32(%rsp)           # 8-byte Folded Spill
	movl	$1, %r12d
	jmp	.LBB22_6
.LBB22_4:
	decl	%r8d
	xorl	%r12d, %r12d
.LBB22_6:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movslq	type2ctx_last(,%rbp,4), %rdx
	testb	%cl, %cl
	je	.LBB22_7
# BB#12:                                # %.preheader
	cmpl	%r8d, %r12d
	jge	.LBB22_17
# BB#13:                                # %.lr.ph
	movslq	%r8d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	pos2ctx_map_int(,%rax,8), %rbp
	imulq	$240, %rdx, %rax
	addq	%rax, %r13
	addq	%rax, 8(%rsp)           # 8-byte Folded Spill
	.p2align	4, 0x90
.LBB22_14:                              # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r12,4), %r14d
	xorl	%esi, %esi
	cmpl	$0, %r14d
	setne	%sil
	movslq	(%rbp,%r12,4), %rdx
	shlq	$4, %rdx
	addq	%r13, %rdx
	movq	%r15, %rdi
	callq	biari_encode_symbol
	cmpl	$0, %r14d
	je	.LBB22_16
# BB#15:                                #   in Loop: Header=BB22_14 Depth=1
	xorl	%esi, %esi
	movl	%ebx, %eax
	decl	%eax
	sete	%sil
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	pos2ctx_last(,%rax,8), %rax
	movslq	(%rax,%r12,4), %rdx
	shlq	$4, %rdx
	addq	8(%rsp), %rdx           # 8-byte Folded Reload
	movq	%r15, %rdi
	callq	biari_encode_symbol
	decl	%ebx
	je	.LBB22_17
.LBB22_16:                              #   in Loop: Header=BB22_14 Depth=1
	incq	%r12
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB22_14
	jmp	.LBB22_17
.LBB22_7:                               # %.preheader81
	cmpl	%r8d, %r12d
	jge	.LBB22_17
# BB#8:                                 # %.lr.ph88
	movslq	%r8d, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	pos2ctx_map(,%rax,8), %r14
	imulq	$240, %rdx, %rax
	addq	%rax, %r13
	addq	%rax, 8(%rsp)           # 8-byte Folded Spill
	.p2align	4, 0x90
.LBB22_9:                               # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r12,4), %ebp
	xorl	%esi, %esi
	cmpl	$0, %ebp
	setne	%sil
	movslq	(%r14,%r12,4), %rdx
	shlq	$4, %rdx
	addq	%r13, %rdx
	movq	%r15, %rdi
	callq	biari_encode_symbol
	cmpl	$0, %ebp
	je	.LBB22_11
# BB#10:                                #   in Loop: Header=BB22_9 Depth=1
	xorl	%esi, %esi
	movl	%ebx, %eax
	decl	%eax
	sete	%sil
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	pos2ctx_last(,%rax,8), %rax
	movslq	(%rax,%r12,4), %rdx
	shlq	$4, %rdx
	addq	8(%rsp), %rdx           # 8-byte Folded Reload
	movq	%r15, %rdi
	callq	biari_encode_symbol
	decl	%ebx
	je	.LBB22_17
.LBB22_11:                              #   in Loop: Header=BB22_9 Depth=1
	incq	%r12
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	jl	.LBB22_9
.LBB22_17:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	write_significance_map, .Lfunc_end22-write_significance_map
	.cfi_endproc

	.globl	write_significant_coefficients
	.p2align	4, 0x90
	.type	write_significant_coefficients,@function
write_significant_coefficients:         # @write_significant_coefficients
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi213:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi214:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi215:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi216:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi217:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi218:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi219:
	.cfi_def_cfa_offset 80
.Lcfi220:
	.cfi_offset %rbx, -56
.Lcfi221:
	.cfi_offset %r12, -48
.Lcfi222:
	.cfi_offset %r13, -40
.Lcfi223:
	.cfi_offset %r14, -32
.Lcfi224:
	.cfi_offset %r15, -24
.Lcfi225:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rsi, %r15
	movslq	%edx, %r14
	movl	maxpos(,%r14,4), %ebp
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$1, %r13d
	jmp	.LBB23_1
	.p2align	4, 0x90
.LBB23_4:                               #   in Loop: Header=BB23_1 Depth=1
	movl	%eax, %r12d
	negl	%r12d
	cmovll	%eax, %r12d
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setle	%cl
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	xorl	%esi, %esi
	cmpl	$1, %r12d
	setg	%sil
	cmpl	$5, %r13d
	movl	$4, %eax
	cmovll	%r13d, %eax
	movq	img(%rip), %rcx
	movq	14216(%rcx), %rcx
	movslq	type2ctx_abs(,%r14,4), %rdx
	cltq
	shlq	$4, %rax
	leaq	(%rdx,%rdx,4), %rdx
	shlq	$4, %rdx
	addq	40(%rcx), %rdx
	leaq	5728(%rax,%rdx), %rdx
	movq	%r15, %rdi
	callq	biari_encode_symbol
	cmpl	$2, %r12d
	movq	%r14, 16(%rsp)          # 8-byte Spill
	jl	.LBB23_6
# BB#5:                                 #   in Loop: Header=BB23_1 Depth=1
	movl	max_c2(,%r14,4), %eax
	movl	8(%rsp), %r13d          # 4-byte Reload
	cmpl	%eax, %r13d
	cmovlel	%r13d, %eax
	addl	$-2, %r12d
	movq	img(%rip), %rcx
	movq	14216(%rcx), %rcx
	movslq	type2ctx_abs(,%r14,4), %rdx
	cltq
	leaq	(%rdx,%rdx,4), %rdx
	shlq	$4, %rdx
	addq	40(%rcx), %rdx
	shlq	$4, %rax
	leaq	6528(%rax,%rdx), %rdx
	movq	%r15, %rdi
	movl	%r12d, %esi
	callq	unary_exp_golomb_level_encode
	incl	%r13d
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	jmp	.LBB23_7
	.p2align	4, 0x90
.LBB23_6:                               #   in Loop: Header=BB23_1 Depth=1
	leal	1(%r13), %eax
	testl	%r13d, %r13d
	cmovel	%r13d, %eax
	movl	%eax, %r13d
.LBB23_7:                               #   in Loop: Header=BB23_1 Depth=1
	movzwl	12(%rsp), %esi          # 2-byte Folded Reload
	movq	%r15, %rdi
	callq	biari_encode_symbol_eq_prob
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB23_1:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_2 Depth 2
	movslq	%ebp, %rbp
	.p2align	4, 0x90
.LBB23_2:                               #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbp, %rbp
	jle	.LBB23_8
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=2
	movl	-4(%rbx,%rbp,4), %eax
	decq	%rbp
	testl	%eax, %eax
	je	.LBB23_2
	jmp	.LBB23_4
.LBB23_8:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	write_significant_coefficients, .Lfunc_end23-write_significant_coefficients
	.cfi_endproc

	.globl	unary_exp_golomb_level_encode
	.p2align	4, 0x90
	.type	unary_exp_golomb_level_encode,@function
unary_exp_golomb_level_encode:          # @unary_exp_golomb_level_encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi226:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi227:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi228:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi229:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi230:
	.cfi_def_cfa_offset 48
.Lcfi231:
	.cfi_offset %rbx, -48
.Lcfi232:
	.cfi_offset %r12, -40
.Lcfi233:
	.cfi_offset %r14, -32
.Lcfi234:
	.cfi_offset %r15, -24
.Lcfi235:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r12d
	movq	%rdi, %r14
	testl	%r12d, %r12d
	je	.LBB24_11
# BB#1:
	movl	$1, %ebx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	biari_encode_symbol
	cmpl	$1, %r12d
	je	.LBB24_11
	.p2align	4, 0x90
.LBB24_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebx
	cmpl	$13, %ebx
	ja	.LBB24_4
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=1
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	biari_encode_symbol
	cmpl	%ebx, %r12d
	jne	.LBB24_2
.LBB24_4:                               # %.critedge
	cmpl	$12, %r12d
	ja	.LBB24_5
.LBB24_11:
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	biari_encode_symbol     # TAILCALL
.LBB24_5:
	addl	$-13, %r12d
	je	.LBB24_12
# BB#6:                                 # %.lr.ph13.i.preheader
	movl	$1, %ebp
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB24_7:                               # %.lr.ph13.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
	subl	%ebp, %r12d
	leal	2(%rbx), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	incl	%ebx
	cmpl	%ebp, %r12d
	jae	.LBB24_7
# BB#8:                                 # %.lr.ph.i.preheader
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
	.p2align	4, 0x90
.LBB24_9:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%r12d, %esi
	movl	%ebx, %ecx
	shrl	%cl, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
	decl	%ebx
	cmpl	$-1, %ebx
	jne	.LBB24_9
# BB#10:                                # %exp_golomb_encode_eq_prob.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_12:                              # %._crit_edge14.i.thread
	xorl	%esi, %esi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	biari_encode_symbol_eq_prob # TAILCALL
.Lfunc_end24:
	.size	unary_exp_golomb_level_encode, .Lfunc_end24-unary_exp_golomb_level_encode
	.cfi_endproc

	.globl	writeRunLevel_CABAC
	.p2align	4, 0x90
	.type	writeRunLevel_CABAC,@function
writeRunLevel_CABAC:                    # @writeRunLevel_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi236:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi237:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi238:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi239:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi240:
	.cfi_def_cfa_offset 48
.Lcfi241:
	.cfi_offset %rbx, -48
.Lcfi242:
	.cfi_offset %r12, -40
.Lcfi243:
	.cfi_offset %r14, -32
.Lcfi244:
	.cfi_offset %r15, -24
.Lcfi245:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbx
	leaq	8(%r12), %r14
	movq	%r14, %rdi
	callq	arienco_bits_written
	movl	%eax, %r15d
	movl	4(%rbx), %eax
	testl	%eax, %eax
	je	.LBB25_2
# BB#1:
	movslq	writeRunLevel_CABAC.pos(%rip), %rcx
	movslq	8(%rbx), %rdx
	addq	%rcx, %rdx
	leal	1(%rdx), %ecx
	movl	%ecx, writeRunLevel_CABAC.pos(%rip)
	movl	%eax, writeRunLevel_CABAC.coeff(,%rdx,4)
	incl	writeRunLevel_CABAC.coeff_ctr(%rip)
	jmp	.LBB25_6
.LBB25_2:
	movq	img(%rip), %rax
	movslq	12(%rax), %rcx
	imulq	$536, %rcx, %rbp        # imm = 0x218
	addq	14224(%rax), %rbp
	movl	24(%rbx), %edx
	cmpl	$0, writeRunLevel_CABAC.coeff_ctr(%rip)
	jle	.LBB25_4
# BB#3:
	movl	$1, %ecx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	write_and_store_CBP_block_bit
	movl	24(%rbx), %edx
	movl	writeRunLevel_CABAC.coeff_ctr(%rip), %r8d
	movl	$writeRunLevel_CABAC.coeff, %ecx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	write_significance_map
	movl	24(%rbx), %edx
	movl	$writeRunLevel_CABAC.coeff, %ecx
	movq	%r14, %rsi
	callq	write_significant_coefficients
	jmp	.LBB25_5
.LBB25_4:
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	write_and_store_CBP_block_bit
.LBB25_5:
	movl	$0, writeRunLevel_CABAC.coeff_ctr(%rip)
	movl	$0, writeRunLevel_CABAC.pos(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, writeRunLevel_CABAC.coeff+240(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+224(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+208(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+192(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+176(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+160(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+144(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+128(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+112(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+96(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+80(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+64(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+48(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+32(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff+16(%rip)
	movaps	%xmm0, writeRunLevel_CABAC.coeff(%rip)
.LBB25_6:
	movq	(%r12), %rax
	movl	$1, 40(%rax)
	movq	%r14, %rdi
	callq	arienco_bits_written
	subl	%r15d, %eax
	movl	%eax, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	writeRunLevel_CABAC, .Lfunc_end25-writeRunLevel_CABAC
	.cfi_endproc

	.globl	exp_golomb_encode_eq_prob
	.p2align	4, 0x90
	.type	exp_golomb_encode_eq_prob,@function
exp_golomb_encode_eq_prob:              # @exp_golomb_encode_eq_prob
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi246:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi247:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi248:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi249:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi250:
	.cfi_def_cfa_offset 48
.Lcfi251:
	.cfi_offset %rbx, -40
.Lcfi252:
	.cfi_offset %r14, -32
.Lcfi253:
	.cfi_offset %r15, -24
.Lcfi254:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %r15d
	movq	%rdi, %r14
	movl	$1, %ebp
	movl	%ebx, %ecx
	shll	%cl, %ebp
	cmpl	%r15d, %ebp
	ja	.LBB26_2
	.p2align	4, 0x90
.LBB26_1:                               # %.lr.ph13
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
	subl	%ebp, %r15d
	incl	%ebx
	movl	$1, %ebp
	movl	%ebx, %ecx
	shll	%cl, %ebp
	cmpl	%ebp, %r15d
	jae	.LBB26_1
.LBB26_2:                               # %._crit_edge14
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
	testl	%ebx, %ebx
	je	.LBB26_5
# BB#3:                                 # %.lr.ph.preheader
	decl	%ebx
	.p2align	4, 0x90
.LBB26_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r15d, %esi
	movl	%ebx, %ecx
	shrl	%cl, %esi
	andl	$1, %esi
	movq	%r14, %rdi
	callq	biari_encode_symbol_eq_prob
	decl	%ebx
	cmpl	$-1, %ebx
	jne	.LBB26_4
.LBB26_5:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	exp_golomb_encode_eq_prob, .Lfunc_end26-exp_golomb_encode_eq_prob
	.cfi_endproc

	.type	last_dquant,@object     # @last_dquant
	.bss
	.globl	last_dquant
	.p2align	2
last_dquant:
	.long	0                       # 0x0
	.size	last_dquant, 4

	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"create_contexts_MotionInfo: enco_ctx"
	.size	.L.str, 37

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"create_contexts_TextureInfo: enco_ctx"
	.size	.L.str.1, 38

	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	type2ctx_bcbp,@object   # @type2ctx_bcbp
	.section	.rodata,"a",@progbits
	.p2align	4
type2ctx_bcbp:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	5                       # 0x5
	.size	type2ctx_bcbp, 40

	.type	maxpos,@object          # @maxpos
	.p2align	4
maxpos:
	.long	16                      # 0x10
	.long	15                      # 0xf
	.long	64                      # 0x40
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	16                      # 0x10
	.long	4                       # 0x4
	.long	15                      # 0xf
	.long	8                       # 0x8
	.long	16                      # 0x10
	.size	maxpos, 40

	.type	type2ctx_last,@object   # @type2ctx_last
	.p2align	4
type2ctx_last:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	6                       # 0x6
	.size	type2ctx_last, 40

	.type	pos2ctx_map,@object     # @pos2ctx_map
	.p2align	4
pos2ctx_map:
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map8x8
	.quad	pos2ctx_map8x4
	.quad	pos2ctx_map8x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map2x4c
	.quad	pos2ctx_map4x4c
	.size	pos2ctx_map, 80

	.type	pos2ctx_last,@object    # @pos2ctx_last
	.p2align	4
pos2ctx_last:
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last8x8
	.quad	pos2ctx_last8x4
	.quad	pos2ctx_last8x4
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last4x4
	.quad	pos2ctx_last2x4c
	.quad	pos2ctx_last4x4c
	.size	pos2ctx_last, 80

	.type	pos2ctx_map_int,@object # @pos2ctx_map_int
	.p2align	4
pos2ctx_map_int:
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map8x8i
	.quad	pos2ctx_map8x4i
	.quad	pos2ctx_map4x8i
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map4x4
	.quad	pos2ctx_map2x4c
	.quad	pos2ctx_map4x4c
	.size	pos2ctx_map_int, 80

	.type	max_c2,@object          # @max_c2
	.p2align	4
max_c2:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.size	max_c2, 40

	.type	type2ctx_abs,@object    # @type2ctx_abs
	.p2align	4
type2ctx_abs:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	5                       # 0x5
	.size	type2ctx_abs, 40

	.type	writeRunLevel_CABAC.coeff,@object # @writeRunLevel_CABAC.coeff
	.local	writeRunLevel_CABAC.coeff
	.comm	writeRunLevel_CABAC.coeff,256,16
	.type	writeRunLevel_CABAC.coeff_ctr,@object # @writeRunLevel_CABAC.coeff_ctr
	.local	writeRunLevel_CABAC.coeff_ctr
	.comm	writeRunLevel_CABAC.coeff_ctr,4,4
	.type	writeRunLevel_CABAC.pos,@object # @writeRunLevel_CABAC.pos
	.local	writeRunLevel_CABAC.pos
	.comm	writeRunLevel_CABAC.pos,4,4
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	pos2ctx_map4x4,@object  # @pos2ctx_map4x4
	.p2align	4
pos2ctx_map4x4:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map4x4, 64

	.type	pos2ctx_map8x8,@object  # @pos2ctx_map8x8
	.p2align	4
pos2ctx_map8x8:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	11                      # 0xb
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	11                      # 0xb
	.long	6                       # 0x6
	.long	9                       # 0x9
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	10                      # 0xa
	.long	12                      # 0xc
	.long	14                      # 0xe
	.size	pos2ctx_map8x8, 256

	.type	pos2ctx_map8x4,@object  # @pos2ctx_map8x4
	.p2align	4
pos2ctx_map8x4:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map8x4, 128

	.type	pos2ctx_map2x4c,@object # @pos2ctx_map2x4c
	.p2align	4
pos2ctx_map2x4c:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	pos2ctx_map2x4c, 64

	.type	pos2ctx_map4x4c,@object # @pos2ctx_map4x4c
	.p2align	4
pos2ctx_map4x4c:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	pos2ctx_map4x4c, 64

	.type	pos2ctx_last4x4,@object # @pos2ctx_last4x4
	.p2align	4
pos2ctx_last4x4:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	15                      # 0xf
	.size	pos2ctx_last4x4, 64

	.type	pos2ctx_last8x8,@object # @pos2ctx_last8x8
	.p2align	4
pos2ctx_last8x8:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.size	pos2ctx_last8x8, 256

	.type	pos2ctx_last8x4,@object # @pos2ctx_last8x4
	.p2align	4
pos2ctx_last8x4:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.size	pos2ctx_last8x4, 128

	.type	pos2ctx_last2x4c,@object # @pos2ctx_last2x4c
	.p2align	4
pos2ctx_last2x4c:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	pos2ctx_last2x4c, 64

	.type	pos2ctx_last4x4c,@object # @pos2ctx_last4x4c
	.p2align	4
pos2ctx_last4x4c:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	pos2ctx_last4x4c, 64

	.type	pos2ctx_map8x8i,@object # @pos2ctx_map8x8i
	.p2align	4
pos2ctx_map8x8i:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	11                      # 0xb
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	9                       # 0x9
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map8x8i, 256

	.type	pos2ctx_map8x4i,@object # @pos2ctx_map8x4i
	.p2align	4
pos2ctx_map8x4i:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	12                      # 0xc
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map8x4i, 128

	.type	pos2ctx_map4x8i,@object # @pos2ctx_map4x8i
	.p2align	4
pos2ctx_map4x8i:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	2                       # 0x2
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	11                      # 0xb
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	14                      # 0xe
	.size	pos2ctx_map4x8i, 128

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Unsupported MB-MODE in writeMB_typeInfo_CABAC!"
	.size	.Lstr, 47


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
