	.text
	.file	"libclamav_line.bc"
	.globl	lineCreate
	.p2align	4, 0x90
	.type	lineCreate,@function
lineCreate:                             # @lineCreate
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	callq	strlen
	movq	%rax, %r15
	leaq	2(%r15), %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movb	$1, (%rbx)
	movq	%rbx, %rdi
	incq	%rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	movb	$0, 1(%rbx,%r15)
	jmp	.LBB0_3
.LBB0_1:
	xorl	%ebx, %ebx
.LBB0_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	lineCreate, .Lfunc_end0-lineCreate
	.cfi_endproc

	.globl	lineLink
	.p2align	4, 0x90
	.type	lineLink,@function
lineLink:                               # @lineLink
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movb	(%rbx), %al
	cmpb	$-1, %al
	je	.LBB1_1
# BB#4:
	incb	%al
	movb	%al, (%rbx)
	jmp	.LBB1_5
.LBB1_1:
	incq	%rbx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	leaq	2(%r14), %rdi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_2
# BB#3:
	movb	$1, (%r15)
	movq	%r15, %rdi
	incq	%rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	memcpy
	movb	$0, 1(%r15,%r14)
	movq	%r15, %rbx
	jmp	.LBB1_5
.LBB1_2:
	xorl	%ebx, %ebx
.LBB1_5:                                # %lineCreate.exit
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	lineLink, .Lfunc_end1-lineLink
	.cfi_endproc

	.globl	lineGetData
	.p2align	4, 0x90
	.type	lineGetData,@function
lineGetData:                            # @lineGetData
	.cfi_startproc
# BB#0:
	leaq	1(%rdi), %rax
	testq	%rdi, %rdi
	cmoveq	%rdi, %rax
	retq
.Lfunc_end2:
	.size	lineGetData, .Lfunc_end2-lineGetData
	.cfi_endproc

	.globl	lineUnlink
	.p2align	4, 0x90
	.type	lineUnlink,@function
lineUnlink:                             # @lineUnlink
	.cfi_startproc
# BB#0:
	decb	(%rdi)
	jne	.LBB3_2
# BB#1:
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 16
	callq	free
	xorl	%edi, %edi
	addq	$8, %rsp
.LBB3_2:
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	lineUnlink, .Lfunc_end3-lineUnlink
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"lineLink: linkcount too large (%s)\n"
	.size	.L.str, 36


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
