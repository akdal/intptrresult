; ModuleID = 'yacr2.base-4.0/vcg.bc'
source_filename = "vcg.i"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._nodeVCGType = type { %struct._constraintVCGType*, i64, i64, i64, %struct._constraintVCGType*, i64, i64, i64 }
%struct._constraintVCGType = type { i64, i64, i64, i64 }
%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }

@channelNets = external local_unnamed_addr global i64, align 8
@VCG = common local_unnamed_addr global %struct._nodeVCGType* null, align 8
@storageRootVCG = common local_unnamed_addr global %struct._constraintVCGType* null, align 8
@storageVCG = common local_unnamed_addr global %struct._constraintVCGType* null, align 8
@storageLimitVCG = common local_unnamed_addr global i64 0, align 8
@SCC = common local_unnamed_addr global i64* null, align 8
@perSCC = common local_unnamed_addr global i64* null, align 8
@removeVCG = common local_unnamed_addr global %struct._constraintVCGType** null, align 8
@channelColumns = external local_unnamed_addr global i64, align 8
@TOP = external local_unnamed_addr global i64*, align 8
@BOT = external local_unnamed_addr global i64*, align 8
@.str = private unnamed_addr constant [6 x i8] c"[%d]\0A\00", align 1
@.str.1 = private unnamed_addr constant [8 x i8] c"above: \00", align 1
@.str.2 = private unnamed_addr constant [4 x i8] c"%d \00", align 1
@.str.4 = private unnamed_addr constant [8 x i8] c"below: \00", align 1
@totalSCC = common local_unnamed_addr global i64 0, align 8
@.str.6 = private unnamed_addr constant [6 x i8] c"[%d]\09\00", align 1
@.str.7 = private unnamed_addr constant [5 x i8] c"<%d>\00", align 1
@removeTotalVCG = common local_unnamed_addr global i64 0, align 8
@.str.10 = private unnamed_addr constant [27 x i8] c"*** VC's removed (%d) ***\0A\00", align 1
@stdout = external local_unnamed_addr global %struct._IO_FILE*, align 8
@cardBotNotPref = external local_unnamed_addr global i64, align 8
@channelTracks = external local_unnamed_addr global i64, align 8
@tracksBotNotPref = external local_unnamed_addr global i64*, align 8
@cardTopNotPref = external local_unnamed_addr global i64, align 8
@tracksTopNotPref = external local_unnamed_addr global i64*, align 8
@tracksNotPref = external local_unnamed_addr global i64*, align 8
@cardNotPref = external local_unnamed_addr global i64, align 8
@str = private unnamed_addr constant [2 x i8] c"\0A\00"
@str.1 = private unnamed_addr constant [26 x i8] c"\0A*** Input is cyclic! ***\00"
@str.2 = private unnamed_addr constant [27 x i8] c"\0A*** Input is acyclic! ***\00"

; Function Attrs: nounwind uwtable
define void @AllocVCG() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %add = shl i64 %0, 6
  %mul = add i64 %add, 64
  %call = tail call noalias i8* @malloc(i64 %mul) #4
  store i8* %call, i8** bitcast (%struct._nodeVCGType** @VCG to i8**), align 8, !tbaa !5
  %add1 = add i64 %0, 1
  %mul3 = shl i64 %add1, 5
  %mul4 = mul i64 %mul3, %add1
  %call5 = tail call noalias i8* @malloc(i64 %mul4) #4
  store i8* %call5, i8** bitcast (%struct._constraintVCGType** @storageRootVCG to i8**), align 8, !tbaa !5
  store i8* %call5, i8** bitcast (%struct._constraintVCGType** @storageVCG to i8**), align 8, !tbaa !5
  %mul8 = mul i64 %add1, %add1
  store i64 %mul8, i64* @storageLimitVCG, align 8, !tbaa !1
  %add9 = shl i64 %0, 3
  %mul10 = add i64 %add9, 8
  %call11 = tail call noalias i8* @malloc(i64 %mul10) #4
  store i8* %call11, i8** bitcast (i64** @SCC to i8**), align 8, !tbaa !5
  %call14 = tail call noalias i8* @malloc(i64 %mul10) #4
  store i8* %call14, i8** bitcast (i64** @perSCC to i8**), align 8, !tbaa !5
  %mul17 = shl i64 %add1, 3
  %mul18 = mul i64 %mul17, %add1
  %call19 = tail call noalias i8* @malloc(i64 %mul18) #4
  store i8* %call19, i8** bitcast (%struct._constraintVCGType*** @removeVCG to i8**), align 8, !tbaa !5
  ret void
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define void @FreeVCG() local_unnamed_addr #0 {
entry:
  %0 = load i8*, i8** bitcast (%struct._nodeVCGType** @VCG to i8**), align 8, !tbaa !5
  tail call void @free(i8* %0) #4
  %1 = load i8*, i8** bitcast (%struct._constraintVCGType** @storageRootVCG to i8**), align 8, !tbaa !5
  tail call void @free(i8* %1) #4
  store i64 0, i64* @storageLimitVCG, align 8, !tbaa !1
  %2 = load i8*, i8** bitcast (i64** @SCC to i8**), align 8, !tbaa !5
  tail call void @free(i8* %2) #4
  %3 = load i8*, i8** bitcast (i64** @perSCC to i8**), align 8, !tbaa !5
  tail call void @free(i8* %3) #4
  %4 = load i8*, i8** bitcast (%struct._constraintVCGType*** @removeVCG to i8**), align 8, !tbaa !5
  tail call void @free(i8* %4) #4
  ret void
}

; Function Attrs: nounwind
declare void @free(i8* nocapture) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define void @BuildVCG() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %add.i = shl i64 %0, 6
  %mul.i = add i64 %add.i, 64
  %call.i = tail call noalias i8* @malloc(i64 %mul.i) #4
  store i8* %call.i, i8** bitcast (%struct._nodeVCGType** @VCG to i8**), align 8, !tbaa !5
  %add1.i = add i64 %0, 1
  %mul3.i = shl i64 %add1.i, 5
  %mul4.i = mul i64 %mul3.i, %add1.i
  %call5.i = tail call noalias i8* @malloc(i64 %mul4.i) #4
  store i8* %call5.i, i8** bitcast (%struct._constraintVCGType** @storageRootVCG to i8**), align 8, !tbaa !5
  store i8* %call5.i, i8** bitcast (%struct._constraintVCGType** @storageVCG to i8**), align 8, !tbaa !5
  %mul8.i = mul i64 %add1.i, %add1.i
  store i64 %mul8.i, i64* @storageLimitVCG, align 8, !tbaa !1
  %add9.i = shl i64 %0, 3
  %mul10.i = add i64 %add9.i, 8
  %call11.i = tail call noalias i8* @malloc(i64 %mul10.i) #4
  store i8* %call11.i, i8** bitcast (i64** @SCC to i8**), align 8, !tbaa !5
  %call14.i = tail call noalias i8* @malloc(i64 %mul10.i) #4
  store i8* %call14.i, i8** bitcast (i64** @perSCC to i8**), align 8, !tbaa !5
  %mul17.i = shl i64 %add1.i, 3
  %mul18.i = mul i64 %mul17.i, %add1.i
  %call19.i = tail call noalias i8* @malloc(i64 %mul18.i) #4
  store i8* %call19.i, i8** bitcast (%struct._constraintVCGType*** @removeVCG to i8**), align 8, !tbaa !5
  %cmp75 = icmp eq i64 %0, 0
  br i1 %cmp75, label %for.end102, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  %1 = bitcast i8* %call.i to %struct._nodeVCGType*
  %2 = ptrtoint i8* %call5.i to i64
  %.pre = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.end98
  %3 = phi i64 [ %39, %for.end98 ], [ %.pre, %for.body.preheader ]
  %4 = phi i64 [ %40, %for.end98 ], [ %.pre, %for.body.preheader ]
  %5 = phi %struct._nodeVCGType* [ %21, %for.end98 ], [ %1, %for.body.preheader ]
  %6 = phi i64 [ %41, %for.end98 ], [ %2, %for.body.preheader ]
  %net.076 = phi i64 [ %inc101, %for.end98 ], [ 1, %for.body.preheader ]
  %arrayidx = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %5, i64 %net.076
  %7 = bitcast %struct._nodeVCGType* %arrayidx to i64*
  store i64 %6, i64* %7, align 8, !tbaa !7
  %cmp264 = icmp eq i64 %4, 0
  br i1 %cmp264, label %for.end42, label %for.body3.preheader

for.body3.preheader:                              ; preds = %for.body
  %.pre78 = load i64*, i64** @TOP, align 8, !tbaa !5
  br label %for.body3

for.body3:                                        ; preds = %for.body3.preheader, %for.inc40
  %8 = phi i64 [ %17, %for.inc40 ], [ %3, %for.body3.preheader ]
  %9 = phi i64 [ %18, %for.inc40 ], [ %4, %for.body3.preheader ]
  %constraint.066 = phi i64 [ %constraint.1, %for.inc40 ], [ 0, %for.body3.preheader ]
  %col.065 = phi i64 [ %inc41, %for.inc40 ], [ 1, %for.body3.preheader ]
  %arrayidx4 = getelementptr inbounds i64, i64* %.pre78, i64 %col.065
  %10 = load i64, i64* %arrayidx4, align 8, !tbaa !1
  %cmp5 = icmp eq i64 %10, %net.076
  br i1 %cmp5, label %land.lhs.true, label %for.inc40

land.lhs.true:                                    ; preds = %for.body3
  %11 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx6 = getelementptr inbounds i64, i64* %11, i64 %col.065
  %12 = load i64, i64* %arrayidx6, align 8, !tbaa !1
  %cmp7 = icmp eq i64 %12, %net.076
  %cmp10 = icmp eq i64 %12, 0
  %or.cond = or i1 %cmp7, %cmp10
  br i1 %or.cond, label %for.inc40, label %for.cond11.preheader

for.cond11.preheader:                             ; preds = %land.lhs.true
  %cmp1262 = icmp eq i64 %constraint.066, 0
  %.pre79 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %netsAboveHook23.phi.trans.insert = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %.pre79, i64 %net.076, i32 0
  %.pre80 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsAboveHook23.phi.trans.insert, align 8, !tbaa !7
  br i1 %cmp1262, label %if.then20.critedge, label %for.body13.preheader

for.body13.preheader:                             ; preds = %for.cond11.preheader
  br label %for.body13

for.cond11:                                       ; preds = %for.body13
  %cmp12 = icmp ult i64 %inc, %constraint.066
  br i1 %cmp12, label %for.body13, label %if.then20.critedge.loopexit

for.body13:                                       ; preds = %for.body13.preheader, %for.cond11
  %check.063 = phi i64 [ %inc, %for.cond11 ], [ 0, %for.body13.preheader ]
  %bot = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre80, i64 %check.063, i32 1
  %13 = load i64, i64* %bot, align 8, !tbaa !9
  %cmp18 = icmp eq i64 %13, %12
  %inc = add nuw i64 %check.063, 1
  br i1 %cmp18, label %for.inc40.loopexit, label %for.cond11

if.then20.critedge.loopexit:                      ; preds = %for.cond11
  br label %if.then20.critedge

if.then20.critedge:                               ; preds = %if.then20.critedge.loopexit, %for.cond11.preheader
  %top = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre80, i64 %constraint.066, i32 0
  store i64 %net.076, i64* %top, align 8, !tbaa !11
  %14 = load i64, i64* %arrayidx6, align 8, !tbaa !1
  %bot29 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre80, i64 %constraint.066, i32 1
  store i64 %14, i64* %bot29, align 8, !tbaa !9
  %col33 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre80, i64 %constraint.066, i32 2
  store i64 %col.065, i64* %col33, align 8, !tbaa !12
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre80, i64 %constraint.066, i32 3
  store i64 0, i64* %removed, align 8, !tbaa !13
  %15 = load %struct._constraintVCGType*, %struct._constraintVCGType** @storageVCG, align 8, !tbaa !5
  %incdec.ptr = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %15, i64 1
  store %struct._constraintVCGType* %incdec.ptr, %struct._constraintVCGType** @storageVCG, align 8, !tbaa !5
  %16 = load i64, i64* @storageLimitVCG, align 8, !tbaa !1
  %dec = add i64 %16, -1
  store i64 %dec, i64* @storageLimitVCG, align 8, !tbaa !1
  %inc37 = add i64 %constraint.066, 1
  %.pre81 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.inc40

for.inc40.loopexit:                               ; preds = %for.body13
  br label %for.inc40

for.inc40:                                        ; preds = %for.inc40.loopexit, %land.lhs.true, %for.body3, %if.then20.critedge
  %17 = phi i64 [ %.pre81, %if.then20.critedge ], [ %8, %land.lhs.true ], [ %8, %for.body3 ], [ %8, %for.inc40.loopexit ]
  %18 = phi i64 [ %.pre81, %if.then20.critedge ], [ %9, %land.lhs.true ], [ %9, %for.body3 ], [ %9, %for.inc40.loopexit ]
  %constraint.1 = phi i64 [ %inc37, %if.then20.critedge ], [ %constraint.066, %land.lhs.true ], [ %constraint.066, %for.body3 ], [ %constraint.066, %for.inc40.loopexit ]
  %inc41 = add i64 %col.065, 1
  %cmp2 = icmp ugt i64 %inc41, %18
  br i1 %cmp2, label %for.end42.loopexit, label %for.body3

for.end42.loopexit:                               ; preds = %for.inc40
  br label %for.end42

for.end42:                                        ; preds = %for.end42.loopexit, %for.body
  %19 = phi i64 [ %3, %for.body ], [ %17, %for.end42.loopexit ]
  %20 = phi i64 [ 0, %for.body ], [ %18, %for.end42.loopexit ]
  %constraint.0.lcssa = phi i64 [ 0, %for.body ], [ %constraint.1, %for.end42.loopexit ]
  %21 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %netsAbove = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %21, i64 %net.076, i32 1
  store i64 %constraint.0.lcssa, i64* %netsAbove, align 8, !tbaa !14
  %22 = load i64, i64* bitcast (%struct._constraintVCGType** @storageVCG to i64*), align 8, !tbaa !5
  %netsBelowHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %21, i64 %net.076, i32 4
  %23 = bitcast %struct._constraintVCGType** %netsBelowHook to i64*
  store i64 %22, i64* %23, align 8, !tbaa !15
  %cmp4670 = icmp eq i64 %20, 0
  %24 = inttoptr i64 %22 to %struct._constraintVCGType*
  br i1 %cmp4670, label %for.end98, label %for.body47.lr.ph

for.body47.lr.ph:                                 ; preds = %for.end42
  %25 = load i64*, i64** @BOT, align 8
  %26 = load i64*, i64** @TOP, align 8
  br label %for.body47

for.body47:                                       ; preds = %for.body47.lr.ph, %for.inc96
  %27 = phi i64 [ %19, %for.body47.lr.ph ], [ %36, %for.inc96 ]
  %28 = phi %struct._constraintVCGType* [ %24, %for.body47.lr.ph ], [ %37, %for.inc96 ]
  %29 = phi i64 [ %22, %for.body47.lr.ph ], [ %38, %for.inc96 ]
  %constraint.272 = phi i64 [ 0, %for.body47.lr.ph ], [ %constraint.3, %for.inc96 ]
  %col.171 = phi i64 [ 1, %for.body47.lr.ph ], [ %inc97, %for.inc96 ]
  %arrayidx48 = getelementptr inbounds i64, i64* %25, i64 %col.171
  %30 = load i64, i64* %arrayidx48, align 8, !tbaa !1
  %cmp49 = icmp eq i64 %30, %net.076
  br i1 %cmp49, label %land.lhs.true50, label %for.inc96

land.lhs.true50:                                  ; preds = %for.body47
  %arrayidx51 = getelementptr inbounds i64, i64* %26, i64 %col.171
  %31 = load i64, i64* %arrayidx51, align 8, !tbaa !1
  %cmp52 = icmp eq i64 %31, %net.076
  %cmp55 = icmp eq i64 %31, 0
  %or.cond61 = or i1 %cmp52, %cmp55
  br i1 %or.cond61, label %for.inc96, label %for.cond57.preheader

for.cond57.preheader:                             ; preds = %land.lhs.true50
  %cmp5868 = icmp eq i64 %constraint.272, 0
  br i1 %cmp5868, label %if.then72.critedge, label %for.body59.preheader

for.body59.preheader:                             ; preds = %for.cond57.preheader
  br label %for.body59

for.cond57:                                       ; preds = %for.body59
  %cmp58 = icmp ult i64 %inc69, %constraint.272
  br i1 %cmp58, label %for.body59, label %if.then72.critedge.loopexit

for.body59:                                       ; preds = %for.body59.preheader, %for.cond57
  %check.169 = phi i64 [ %inc69, %for.cond57 ], [ 0, %for.body59.preheader ]
  %top63 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %24, i64 %check.169, i32 0
  %32 = load i64, i64* %top63, align 8, !tbaa !11
  %cmp65 = icmp eq i64 %32, %31
  %inc69 = add nuw i64 %check.169, 1
  br i1 %cmp65, label %for.inc96.loopexit, label %for.cond57

if.then72.critedge.loopexit:                      ; preds = %for.cond57
  br label %if.then72.critedge

if.then72.critedge:                               ; preds = %if.then72.critedge.loopexit, %for.cond57.preheader
  %top77 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %24, i64 %constraint.272, i32 0
  store i64 %31, i64* %top77, align 8, !tbaa !11
  %33 = load i64, i64* %arrayidx48, align 8, !tbaa !1
  %bot82 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %24, i64 %constraint.272, i32 1
  store i64 %33, i64* %bot82, align 8, !tbaa !9
  %col86 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %24, i64 %constraint.272, i32 2
  store i64 %col.171, i64* %col86, align 8, !tbaa !12
  %removed90 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %24, i64 %constraint.272, i32 3
  store i64 0, i64* %removed90, align 8, !tbaa !13
  %incdec.ptr91 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %28, i64 1
  store %struct._constraintVCGType* %incdec.ptr91, %struct._constraintVCGType** @storageVCG, align 8, !tbaa !5
  %34 = load i64, i64* @storageLimitVCG, align 8, !tbaa !1
  %dec92 = add i64 %34, -1
  store i64 %dec92, i64* @storageLimitVCG, align 8, !tbaa !1
  %inc93 = add i64 %constraint.272, 1
  %35 = ptrtoint %struct._constraintVCGType* %incdec.ptr91 to i64
  %.pre82 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.inc96

for.inc96.loopexit:                               ; preds = %for.body59
  br label %for.inc96

for.inc96:                                        ; preds = %for.inc96.loopexit, %land.lhs.true50, %for.body47, %if.then72.critedge
  %36 = phi i64 [ %.pre82, %if.then72.critedge ], [ %27, %land.lhs.true50 ], [ %27, %for.body47 ], [ %27, %for.inc96.loopexit ]
  %37 = phi %struct._constraintVCGType* [ %incdec.ptr91, %if.then72.critedge ], [ %28, %land.lhs.true50 ], [ %28, %for.body47 ], [ %28, %for.inc96.loopexit ]
  %38 = phi i64 [ %35, %if.then72.critedge ], [ %29, %land.lhs.true50 ], [ %29, %for.body47 ], [ %29, %for.inc96.loopexit ]
  %constraint.3 = phi i64 [ %inc93, %if.then72.critedge ], [ %constraint.272, %land.lhs.true50 ], [ %constraint.272, %for.body47 ], [ %constraint.272, %for.inc96.loopexit ]
  %inc97 = add i64 %col.171, 1
  %cmp46 = icmp ugt i64 %inc97, %36
  br i1 %cmp46, label %for.end98.loopexit, label %for.body47

for.end98.loopexit:                               ; preds = %for.inc96
  br label %for.end98

for.end98:                                        ; preds = %for.end98.loopexit, %for.end42
  %39 = phi i64 [ %19, %for.end42 ], [ %36, %for.end98.loopexit ]
  %40 = phi i64 [ 0, %for.end42 ], [ %36, %for.end98.loopexit ]
  %41 = phi i64 [ %22, %for.end42 ], [ %38, %for.end98.loopexit ]
  %constraint.2.lcssa = phi i64 [ 0, %for.end42 ], [ %constraint.3, %for.end98.loopexit ]
  %netsBelow = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %21, i64 %net.076, i32 5
  store i64 %constraint.2.lcssa, i64* %netsBelow, align 8, !tbaa !16
  %inc101 = add i64 %net.076, 1
  %42 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc101, %42
  br i1 %cmp, label %for.end102.loopexit, label %for.body

for.end102.loopexit:                              ; preds = %for.end98
  br label %for.end102

for.end102:                                       ; preds = %for.end102.loopexit, %entry
  ret void
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #2

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #2

; Function Attrs: norecurse nounwind uwtable
define void @DFSClearVCG(%struct._nodeVCGType* nocapture %VCG) local_unnamed_addr #3 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp10 = icmp eq i64 %0, 0
  br i1 %cmp10, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %net.011 = phi i64 [ %inc, %for.body ], [ 1, %for.body.preheader ]
  %netsAboveLabel = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.011, i32 2
  %netsBelowLabel = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.011, i32 6
  %inc = add i64 %net.011, 1
  %cmp = icmp ugt i64 %inc, %0
  %1 = bitcast i64* %netsAboveLabel to i8*
  call void @llvm.memset.p0i8.i64(i8* %1, i8 0, i64 16, i32 8, i1 false)
  %2 = bitcast i64* %netsBelowLabel to i8*
  call void @llvm.memset.p0i8.i64(i8* %2, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.body
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  ret void
}

; Function Attrs: nounwind uwtable
define void @DumpVCG(%struct._nodeVCGType* nocapture readonly %VCG) local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp27 = icmp eq i64 %0, 0
  br i1 %cmp27, label %for.end33, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.end29
  %net.028 = phi i64 [ %inc32, %for.end29 ], [ 1, %for.body.preheader ]
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i64 0, i64 0), i64 %net.028)
  %call1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.1, i64 0, i64 0))
  %netsAbove = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.028, i32 1
  %1 = load i64, i64* %netsAbove, align 8, !tbaa !14
  %cmp323 = icmp eq i64 %1, 0
  br i1 %cmp323, label %for.end, label %for.body4.lr.ph

for.body4.lr.ph:                                  ; preds = %for.body
  %netsAboveHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.028, i32 0
  br label %for.body4

for.body4:                                        ; preds = %for.body4.lr.ph, %for.inc
  %2 = phi i64 [ %1, %for.body4.lr.ph ], [ %6, %for.inc ]
  %which.024 = phi i64 [ 0, %for.body4.lr.ph ], [ %inc, %for.inc ]
  %3 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsAboveHook, align 8, !tbaa !7
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %3, i64 %which.024, i32 3
  %4 = load i64, i64* %removed, align 8, !tbaa !13
  %tobool = icmp eq i64 %4, 0
  br i1 %tobool, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body4
  %bot = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %3, i64 %which.024, i32 1
  %5 = load i64, i64* %bot, align 8, !tbaa !9
  %call10 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i64 0, i64 0), i64 %5)
  %.pre = load i64, i64* %netsAbove, align 8, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %for.body4, %if.then
  %6 = phi i64 [ %2, %for.body4 ], [ %.pre, %if.then ]
  %inc = add i64 %which.024, 1
  %cmp3 = icmp ult i64 %inc, %6
  br i1 %cmp3, label %for.body4, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %for.body
  %putchar = tail call i32 @putchar(i32 10) #4
  %call12 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.4, i64 0, i64 0))
  %netsBelow = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.028, i32 5
  %7 = load i64, i64* %netsBelow, align 8, !tbaa !16
  %cmp1525 = icmp eq i64 %7, 0
  br i1 %cmp1525, label %for.end29, label %for.body16.lr.ph

for.body16.lr.ph:                                 ; preds = %for.end
  %netsBelowHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.028, i32 4
  br label %for.body16

for.body16:                                       ; preds = %for.body16.lr.ph, %for.inc27
  %8 = phi i64 [ %7, %for.body16.lr.ph ], [ %12, %for.inc27 ]
  %which.126 = phi i64 [ 0, %for.body16.lr.ph ], [ %inc28, %for.inc27 ]
  %9 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsBelowHook, align 8, !tbaa !15
  %removed19 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %9, i64 %which.126, i32 3
  %10 = load i64, i64* %removed19, align 8, !tbaa !13
  %tobool20 = icmp eq i64 %10, 0
  br i1 %tobool20, label %if.then21, label %for.inc27

if.then21:                                        ; preds = %for.body16
  %top = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %9, i64 %which.126, i32 0
  %11 = load i64, i64* %top, align 8, !tbaa !11
  %call25 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i64 0, i64 0), i64 %11)
  %.pre29 = load i64, i64* %netsBelow, align 8, !tbaa !16
  br label %for.inc27

for.inc27:                                        ; preds = %for.body16, %if.then21
  %12 = phi i64 [ %8, %for.body16 ], [ %.pre29, %if.then21 ]
  %inc28 = add i64 %which.126, 1
  %cmp15 = icmp ult i64 %inc28, %12
  br i1 %cmp15, label %for.body16, label %for.end29.loopexit

for.end29.loopexit:                               ; preds = %for.inc27
  br label %for.end29

for.end29:                                        ; preds = %for.end29.loopexit, %for.end
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @str, i64 0, i64 0))
  %inc32 = add i64 %net.028, 1
  %13 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc32, %13
  br i1 %cmp, label %for.end33.loopexit, label %for.body

for.end33.loopexit:                               ; preds = %for.end29
  br label %for.end33

for.end33:                                        ; preds = %for.end33.loopexit, %entry
  ret void
}

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define void @DFSAboveVCG(%struct._nodeVCGType* nocapture %VCG, i64 %net) local_unnamed_addr #0 {
entry:
  %netsAboveReached = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 3
  store i64 1, i64* %netsAboveReached, align 8, !tbaa !17
  %netsAbove = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 1
  %0 = load i64, i64* %netsAbove, align 8, !tbaa !14
  %cmp15 = icmp eq i64 %0, 0
  br i1 %cmp15, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %netsAboveHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 0
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %1 = phi i64 [ %0, %for.body.lr.ph ], [ %6, %for.inc ]
  %s.016 = phi i64 [ 0, %for.body.lr.ph ], [ %inc, %for.inc ]
  %2 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsAboveHook, align 8, !tbaa !7
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.016, i32 3
  %3 = load i64, i64* %removed, align 8, !tbaa !13
  %tobool = icmp eq i64 %3, 0
  br i1 %tobool, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %bot = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.016, i32 1
  %4 = load i64, i64* %bot, align 8, !tbaa !9
  %netsAboveReached8 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %4, i32 3
  %5 = load i64, i64* %netsAboveReached8, align 8, !tbaa !17
  %tobool9 = icmp eq i64 %5, 0
  br i1 %tobool9, label %if.then10, label %for.inc

if.then10:                                        ; preds = %if.then
  tail call void @DFSAboveVCG(%struct._nodeVCGType* nonnull %VCG, i64 %4)
  %.pre = load i64, i64* %netsAbove, align 8, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %if.then, %for.body, %if.then10
  %6 = phi i64 [ %1, %if.then ], [ %1, %for.body ], [ %.pre, %if.then10 ]
  %inc = add i64 %s.016, 1
  %cmp = icmp ult i64 %inc, %6
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  ret void
}

; Function Attrs: nounwind uwtable
define void @DFSBelowVCG(%struct._nodeVCGType* nocapture %VCG, i64 %net) local_unnamed_addr #0 {
entry:
  %netsBelowReached = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 7
  store i64 1, i64* %netsBelowReached, align 8, !tbaa !18
  %netsBelow = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 5
  %0 = load i64, i64* %netsBelow, align 8, !tbaa !16
  %cmp15 = icmp eq i64 %0, 0
  br i1 %cmp15, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %netsBelowHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 4
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %1 = phi i64 [ %0, %for.body.lr.ph ], [ %6, %for.inc ]
  %s.016 = phi i64 [ 0, %for.body.lr.ph ], [ %inc, %for.inc ]
  %2 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsBelowHook, align 8, !tbaa !15
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.016, i32 3
  %3 = load i64, i64* %removed, align 8, !tbaa !13
  %tobool = icmp eq i64 %3, 0
  br i1 %tobool, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %top = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.016, i32 0
  %4 = load i64, i64* %top, align 8, !tbaa !11
  %netsBelowReached8 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %4, i32 7
  %5 = load i64, i64* %netsBelowReached8, align 8, !tbaa !18
  %tobool9 = icmp eq i64 %5, 0
  br i1 %tobool9, label %if.then10, label %for.inc

if.then10:                                        ; preds = %if.then
  tail call void @DFSBelowVCG(%struct._nodeVCGType* nonnull %VCG, i64 %4)
  %.pre = load i64, i64* %netsBelow, align 8, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %if.then, %for.body, %if.then10
  %6 = phi i64 [ %1, %if.then ], [ %1, %for.body ], [ %.pre, %if.then10 ]
  %inc = add i64 %s.016, 1
  %cmp = icmp ult i64 %inc, %6
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  ret void
}

; Function Attrs: nounwind uwtable
define void @SCCofVCG(%struct._nodeVCGType* nocapture %VCG, i64* nocapture %SCC, i64* nocapture %perSCC) local_unnamed_addr #0 {
entry:
  %label = alloca i64, align 8
  %0 = bitcast i64* %label to i8*
  call void @llvm.lifetime.start(i64 8, i8* nonnull %0) #4
  store i64 0, i64* %label, align 8, !tbaa !1
  %1 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp55 = icmp eq i64 %1, 0
  br i1 %cmp55, label %do.body.preheader, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %2 = phi i64 [ %4, %for.inc ], [ %1, %for.body.preheader ]
  %net.056 = phi i64 [ %inc, %for.inc ], [ 1, %for.body.preheader ]
  %netsAboveReached = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.056, i32 3
  %3 = load i64, i64* %netsAboveReached, align 8, !tbaa !17
  %tobool = icmp eq i64 %3, 0
  br i1 %tobool, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  call void @SCC_DFSAboveVCG(%struct._nodeVCGType* nonnull %VCG, i64 %net.056, i64* nonnull %label)
  %.pre = load i64, i64* @channelNets, align 8, !tbaa !1
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then
  %4 = phi i64 [ %2, %for.body ], [ %.pre, %if.then ]
  %inc = add i64 %net.056, 1
  %cmp = icmp ugt i64 %inc, %4
  br i1 %cmp, label %do.body.preheader.loopexit, label %for.body

do.body.preheader.loopexit:                       ; preds = %for.inc
  br label %do.body.preheader

do.body.preheader:                                ; preds = %do.body.preheader.loopexit, %entry
  %.ph = phi i64 [ 0, %entry ], [ %4, %do.body.preheader.loopexit ]
  %cmp24867 = icmp eq i64 %.ph, 0
  br i1 %cmp24867, label %do.end.thread, label %for.body3.preheader.preheader

for.body3.preheader.preheader:                    ; preds = %do.body.preheader
  br label %for.body3.preheader

for.body3.preheader:                              ; preds = %for.body3.preheader.preheader, %do.cond
  %cmp24869 = phi i1 [ %cmp248, %do.cond ], [ %cmp24867, %for.body3.preheader.preheader ]
  %which.068 = phi i64 [ %inc19, %do.cond ], [ 0, %for.body3.preheader.preheader ]
  %5 = phi i64 [ %.pre57, %do.cond ], [ %.ph, %for.body3.preheader.preheader ]
  br label %for.body3

do.end.thread.loopexit:                           ; preds = %do.cond
  br label %do.end.thread

do.end.thread:                                    ; preds = %do.end.thread.loopexit, %do.body.preheader
  store i64 0, i64* @totalSCC, align 8, !tbaa !1
  br label %for.end52

for.body3:                                        ; preds = %for.body3.preheader, %for.inc14
  %done.052 = phi i64 [ %done.1, %for.inc14 ], [ 1, %for.body3.preheader ]
  %large.051 = phi i64 [ %large.1, %for.inc14 ], [ 0, %for.body3.preheader ]
  %choose.050 = phi i64 [ %choose.1, %for.inc14 ], [ 0, %for.body3.preheader ]
  %net.149 = phi i64 [ %inc15, %for.inc14 ], [ 1, %for.body3.preheader ]
  %netsBelowReached = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.149, i32 7
  %6 = load i64, i64* %netsBelowReached, align 8, !tbaa !18
  %tobool5 = icmp eq i64 %6, 0
  br i1 %tobool5, label %if.then6, label %for.inc14

if.then6:                                         ; preds = %for.body3
  %netsAboveLabel = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.149, i32 2
  %7 = load i64, i64* %netsAboveLabel, align 8, !tbaa !19
  %cmp8 = icmp ugt i64 %7, %large.051
  %net.1.choose.0 = select i1 %cmp8, i64 %net.149, i64 %choose.050
  %.large.0 = select i1 %cmp8, i64 %7, i64 %large.051
  %.done.0 = select i1 %cmp8, i64 0, i64 %done.052
  br label %for.inc14

for.inc14:                                        ; preds = %if.then6, %for.body3
  %choose.1 = phi i64 [ %choose.050, %for.body3 ], [ %net.1.choose.0, %if.then6 ]
  %large.1 = phi i64 [ %large.051, %for.body3 ], [ %.large.0, %if.then6 ]
  %done.1 = phi i64 [ %done.052, %for.body3 ], [ %.done.0, %if.then6 ]
  %inc15 = add i64 %net.149, 1
  %cmp2 = icmp ugt i64 %inc15, %5
  br i1 %cmp2, label %for.end16, label %for.body3

for.end16:                                        ; preds = %for.inc14
  %tobool17 = icmp eq i64 %done.1, 0
  br i1 %tobool17, label %do.cond, label %do.end

do.cond:                                          ; preds = %for.end16
  %inc19 = add i64 %which.068, 1
  tail call void @SCC_DFSBelowVCG(%struct._nodeVCGType* nonnull %VCG, i64 %choose.1, i64 %inc19)
  %.pre57 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp248 = icmp eq i64 %.pre57, 0
  br i1 %cmp248, label %do.end.thread.loopexit, label %for.body3.preheader

do.end:                                           ; preds = %for.end16
  store i64 0, i64* @totalSCC, align 8, !tbaa !1
  br i1 %cmp24869, label %for.end52, label %for.body24.preheader

for.body24.preheader:                             ; preds = %do.end
  br label %for.body24

for.cond35.preheader:                             ; preds = %for.inc32
  %cmp3644 = icmp eq i64 %10, 0
  br i1 %cmp3644, label %for.end52, label %for.cond38.preheader.preheader

for.cond38.preheader.preheader:                   ; preds = %for.cond35.preheader
  br label %for.cond38.preheader

for.body24:                                       ; preds = %for.body24.preheader, %for.inc32
  %net.247 = phi i64 [ %inc33, %for.inc32 ], [ 1, %for.body24.preheader ]
  %netsBelowLabel = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.247, i32 6
  %8 = load i64, i64* %netsBelowLabel, align 8, !tbaa !20
  %arrayidx26 = getelementptr inbounds i64, i64* %SCC, i64 %net.247
  store i64 %8, i64* %arrayidx26, align 8, !tbaa !1
  %9 = load i64, i64* @totalSCC, align 8, !tbaa !1
  %cmp28 = icmp ugt i64 %8, %9
  br i1 %cmp28, label %if.then29, label %for.inc32

if.then29:                                        ; preds = %for.body24
  store i64 %8, i64* @totalSCC, align 8, !tbaa !1
  br label %for.inc32

for.inc32:                                        ; preds = %for.body24, %if.then29
  %10 = phi i64 [ %9, %for.body24 ], [ %8, %if.then29 ]
  %inc33 = add i64 %net.247, 1
  %11 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp23 = icmp ugt i64 %inc33, %11
  br i1 %cmp23, label %for.cond35.preheader, label %for.body24

for.cond38.preheader:                             ; preds = %for.cond38.preheader.preheader, %for.end48.for.cond38.preheader_crit_edge
  %12 = phi i64 [ %.pre58, %for.end48.for.cond38.preheader_crit_edge ], [ %11, %for.cond38.preheader.preheader ]
  %scc.045 = phi i64 [ %inc51, %for.end48.for.cond38.preheader_crit_edge ], [ 1, %for.cond38.preheader.preheader ]
  %cmp3941 = icmp eq i64 %12, 0
  br i1 %cmp3941, label %for.end48, label %for.body40.preheader

for.body40.preheader:                             ; preds = %for.cond38.preheader
  br label %for.body40

for.body40:                                       ; preds = %for.body40.preheader, %for.body40
  %per.043 = phi i64 [ %inc44.per.0, %for.body40 ], [ 0, %for.body40.preheader ]
  %net.342 = phi i64 [ %inc47, %for.body40 ], [ 1, %for.body40.preheader ]
  %arrayidx41 = getelementptr inbounds i64, i64* %SCC, i64 %net.342
  %13 = load i64, i64* %arrayidx41, align 8, !tbaa !1
  %cmp42 = icmp eq i64 %13, %scc.045
  %inc44 = zext i1 %cmp42 to i64
  %inc44.per.0 = add i64 %inc44, %per.043
  %inc47 = add i64 %net.342, 1
  %cmp39 = icmp ugt i64 %inc47, %12
  br i1 %cmp39, label %for.end48.loopexit, label %for.body40

for.end48.loopexit:                               ; preds = %for.body40
  br label %for.end48

for.end48:                                        ; preds = %for.end48.loopexit, %for.cond38.preheader
  %per.0.lcssa = phi i64 [ 0, %for.cond38.preheader ], [ %inc44.per.0, %for.end48.loopexit ]
  %arrayidx49 = getelementptr inbounds i64, i64* %perSCC, i64 %scc.045
  store i64 %per.0.lcssa, i64* %arrayidx49, align 8, !tbaa !1
  %inc51 = add i64 %scc.045, 1
  %14 = load i64, i64* @totalSCC, align 8, !tbaa !1
  %cmp36 = icmp ugt i64 %inc51, %14
  br i1 %cmp36, label %for.end52.loopexit, label %for.end48.for.cond38.preheader_crit_edge

for.end48.for.cond38.preheader_crit_edge:         ; preds = %for.end48
  %.pre58 = load i64, i64* @channelNets, align 8, !tbaa !1
  br label %for.cond38.preheader

for.end52.loopexit:                               ; preds = %for.end48
  br label %for.end52

for.end52:                                        ; preds = %for.end52.loopexit, %do.end.thread, %do.end, %for.cond35.preheader
  call void @llvm.lifetime.end(i64 8, i8* nonnull %0) #4
  ret void
}

; Function Attrs: nounwind uwtable
define void @SCC_DFSAboveVCG(%struct._nodeVCGType* nocapture %VCG, i64 %net, i64* nocapture %label) local_unnamed_addr #0 {
entry:
  %netsAboveReached = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 3
  store i64 1, i64* %netsAboveReached, align 8, !tbaa !17
  %netsAbove = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 1
  %0 = load i64, i64* %netsAbove, align 8, !tbaa !14
  %cmp19 = icmp eq i64 %0, 0
  br i1 %cmp19, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %netsAboveHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 0
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %1 = phi i64 [ %0, %for.body.lr.ph ], [ %6, %for.inc ]
  %s.020 = phi i64 [ 0, %for.body.lr.ph ], [ %inc, %for.inc ]
  %2 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsAboveHook, align 8, !tbaa !7
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.020, i32 3
  %3 = load i64, i64* %removed, align 8, !tbaa !13
  %tobool = icmp eq i64 %3, 0
  br i1 %tobool, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %bot = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.020, i32 1
  %4 = load i64, i64* %bot, align 8, !tbaa !9
  %netsAboveReached8 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %4, i32 3
  %5 = load i64, i64* %netsAboveReached8, align 8, !tbaa !17
  %tobool9 = icmp eq i64 %5, 0
  br i1 %tobool9, label %if.then10, label %for.inc

if.then10:                                        ; preds = %if.then
  tail call void @SCC_DFSAboveVCG(%struct._nodeVCGType* nonnull %VCG, i64 %4, i64* %label)
  %.pre = load i64, i64* %netsAbove, align 8, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %if.then, %for.body, %if.then10
  %6 = phi i64 [ %1, %if.then ], [ %1, %for.body ], [ %.pre, %if.then10 ]
  %inc = add i64 %s.020, 1
  %cmp = icmp ult i64 %inc, %6
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %7 = load i64, i64* %label, align 8, !tbaa !1
  %inc12 = add i64 %7, 1
  store i64 %inc12, i64* %label, align 8, !tbaa !1
  %netsAboveLabel = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 2
  store i64 %inc12, i64* %netsAboveLabel, align 8, !tbaa !19
  ret void
}

; Function Attrs: nounwind uwtable
define void @SCC_DFSBelowVCG(%struct._nodeVCGType* nocapture %VCG, i64 %net, i64 %label) local_unnamed_addr #0 {
entry:
  %netsBelowReached = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 7
  store i64 1, i64* %netsBelowReached, align 8, !tbaa !18
  %netsBelow = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 5
  %0 = load i64, i64* %netsBelow, align 8, !tbaa !16
  %cmp18 = icmp eq i64 %0, 0
  br i1 %cmp18, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %netsBelowHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 4
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %1 = phi i64 [ %0, %for.body.lr.ph ], [ %6, %for.inc ]
  %s.019 = phi i64 [ 0, %for.body.lr.ph ], [ %inc, %for.inc ]
  %2 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsBelowHook, align 8, !tbaa !15
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.019, i32 3
  %3 = load i64, i64* %removed, align 8, !tbaa !13
  %tobool = icmp eq i64 %3, 0
  br i1 %tobool, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %top = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.019, i32 0
  %4 = load i64, i64* %top, align 8, !tbaa !11
  %netsBelowReached8 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %4, i32 7
  %5 = load i64, i64* %netsBelowReached8, align 8, !tbaa !18
  %tobool9 = icmp eq i64 %5, 0
  br i1 %tobool9, label %if.then10, label %for.inc

if.then10:                                        ; preds = %if.then
  tail call void @SCC_DFSBelowVCG(%struct._nodeVCGType* nonnull %VCG, i64 %4, i64 %label)
  %.pre = load i64, i64* %netsBelow, align 8, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %if.then, %for.body, %if.then10
  %6 = phi i64 [ %1, %if.then ], [ %1, %for.body ], [ %.pre, %if.then10 ]
  %inc = add i64 %s.019, 1
  %cmp = icmp ult i64 %inc, %6
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %netsBelowLabel = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 6
  store i64 %label, i64* %netsBelowLabel, align 8, !tbaa !20
  ret void
}

; Function Attrs: nounwind uwtable
define void @DumpSCC(i64* nocapture readonly %SCC, i64* nocapture readonly %perSCC) local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @totalSCC, align 8, !tbaa !1
  %cmp13 = icmp eq i64 %0, 0
  br i1 %cmp13, label %for.end11, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.end
  %scc.014 = phi i64 [ %inc10, %for.end ], [ 1, %for.body.preheader ]
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.6, i64 0, i64 0), i64 %scc.014)
  %1 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp211 = icmp eq i64 %1, 0
  br i1 %cmp211, label %for.end, label %for.body3.preheader

for.body3.preheader:                              ; preds = %for.body
  br label %for.body3

for.body3:                                        ; preds = %for.body3.preheader, %for.inc
  %2 = phi i64 [ %4, %for.inc ], [ %1, %for.body3.preheader ]
  %net.012 = phi i64 [ %inc, %for.inc ], [ 1, %for.body3.preheader ]
  %arrayidx = getelementptr inbounds i64, i64* %SCC, i64 %net.012
  %3 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %cmp4 = icmp eq i64 %3, %scc.014
  br i1 %cmp4, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body3
  %call5 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i64 0, i64 0), i64 %net.012)
  %.pre = load i64, i64* @channelNets, align 8, !tbaa !1
  br label %for.inc

for.inc:                                          ; preds = %for.body3, %if.then
  %4 = phi i64 [ %2, %for.body3 ], [ %.pre, %if.then ]
  %inc = add i64 %net.012, 1
  %cmp2 = icmp ugt i64 %inc, %4
  br i1 %cmp2, label %for.end.loopexit, label %for.body3

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %for.body
  %arrayidx6 = getelementptr inbounds i64, i64* %perSCC, i64 %scc.014
  %5 = load i64, i64* %arrayidx6, align 8, !tbaa !1
  %call7 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.7, i64 0, i64 0), i64 %5)
  %putchar10 = tail call i32 @putchar(i32 10) #4
  %inc10 = add i64 %scc.014, 1
  %6 = load i64, i64* @totalSCC, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc10, %6
  br i1 %cmp, label %for.end11.loopexit, label %for.body

for.end11.loopexit:                               ; preds = %for.end
  br label %for.end11

for.end11:                                        ; preds = %for.end11.loopexit, %entry
  %putchar = tail call i32 @putchar(i32 10) #4
  ret void
}

; Function Attrs: nounwind uwtable
define void @AcyclicVCG() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp112 = icmp eq i64 %0, 0
  br i1 %cmp112, label %for.end18, label %for.cond1.preheader.lr.ph

for.cond1.preheader.lr.ph:                        ; preds = %entry
  %1 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8
  br label %for.cond1.preheader

for.cond1.preheader:                              ; preds = %for.cond1.preheader.lr.ph, %for.inc16
  %net.0113 = phi i64 [ 1, %for.cond1.preheader.lr.ph ], [ %inc17, %for.inc16 ]
  %netsAbove106 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %1, i64 %net.0113, i32 1
  %2 = load i64, i64* %netsAbove106, align 8, !tbaa !14
  %cmp2107 = icmp eq i64 %2, 0
  br i1 %cmp2107, label %for.cond6.preheader, label %for.body3.lr.ph

for.body3.lr.ph:                                  ; preds = %for.cond1.preheader
  %netsAboveHook.phi.trans.insert = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %1, i64 %net.0113, i32 0
  %.pre = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsAboveHook.phi.trans.insert, align 8, !tbaa !7
  %3 = add i64 %2, -1
  %xtraiter = and i64 %2, 7
  %lcmp.mod = icmp eq i64 %xtraiter, 0
  br i1 %lcmp.mod, label %for.body3.prol.loopexit, label %for.body3.prol.preheader

for.body3.prol.preheader:                         ; preds = %for.body3.lr.ph
  br label %for.body3.prol

for.body3.prol:                                   ; preds = %for.body3.prol, %for.body3.prol.preheader
  %which.0108.prol = phi i64 [ 0, %for.body3.prol.preheader ], [ %inc.prol, %for.body3.prol ]
  %prol.iter = phi i64 [ %xtraiter, %for.body3.prol.preheader ], [ %prol.iter.sub, %for.body3.prol ]
  %removed.prol = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre, i64 %which.0108.prol, i32 3
  store i64 0, i64* %removed.prol, align 8, !tbaa !13
  %inc.prol = add nuw i64 %which.0108.prol, 1
  %prol.iter.sub = add i64 %prol.iter, -1
  %prol.iter.cmp = icmp eq i64 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %for.body3.prol.loopexit.unr-lcssa, label %for.body3.prol, !llvm.loop !21

for.body3.prol.loopexit.unr-lcssa:                ; preds = %for.body3.prol
  br label %for.body3.prol.loopexit

for.body3.prol.loopexit:                          ; preds = %for.body3.lr.ph, %for.body3.prol.loopexit.unr-lcssa
  %which.0108.unr = phi i64 [ 0, %for.body3.lr.ph ], [ %inc.prol, %for.body3.prol.loopexit.unr-lcssa ]
  %4 = icmp ult i64 %3, 7
  br i1 %4, label %for.cond6.preheader.loopexit, label %for.body3.lr.ph.new

for.body3.lr.ph.new:                              ; preds = %for.body3.prol.loopexit
  br label %for.body3

for.cond6.preheader.loopexit.unr-lcssa:           ; preds = %for.body3
  br label %for.cond6.preheader.loopexit

for.cond6.preheader.loopexit:                     ; preds = %for.body3.prol.loopexit, %for.cond6.preheader.loopexit.unr-lcssa
  br label %for.cond6.preheader

for.cond6.preheader:                              ; preds = %for.cond6.preheader.loopexit, %for.cond1.preheader
  %netsBelow109 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %1, i64 %net.0113, i32 5
  %5 = load i64, i64* %netsBelow109, align 8, !tbaa !16
  %cmp8110 = icmp eq i64 %5, 0
  br i1 %cmp8110, label %for.inc16, label %for.body9.lr.ph

for.body9.lr.ph:                                  ; preds = %for.cond6.preheader
  %netsBelowHook.phi.trans.insert = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %1, i64 %net.0113, i32 4
  %.pre129 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsBelowHook.phi.trans.insert, align 8, !tbaa !15
  %6 = add i64 %5, -1
  %xtraiter156 = and i64 %5, 7
  %lcmp.mod157 = icmp eq i64 %xtraiter156, 0
  br i1 %lcmp.mod157, label %for.body9.prol.loopexit, label %for.body9.prol.preheader

for.body9.prol.preheader:                         ; preds = %for.body9.lr.ph
  br label %for.body9.prol

for.body9.prol:                                   ; preds = %for.body9.prol, %for.body9.prol.preheader
  %which.1111.prol = phi i64 [ 0, %for.body9.prol.preheader ], [ %inc14.prol, %for.body9.prol ]
  %prol.iter158 = phi i64 [ %xtraiter156, %for.body9.prol.preheader ], [ %prol.iter158.sub, %for.body9.prol ]
  %removed12.prol = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre129, i64 %which.1111.prol, i32 3
  store i64 0, i64* %removed12.prol, align 8, !tbaa !13
  %inc14.prol = add nuw i64 %which.1111.prol, 1
  %prol.iter158.sub = add i64 %prol.iter158, -1
  %prol.iter158.cmp = icmp eq i64 %prol.iter158.sub, 0
  br i1 %prol.iter158.cmp, label %for.body9.prol.loopexit.unr-lcssa, label %for.body9.prol, !llvm.loop !23

for.body9.prol.loopexit.unr-lcssa:                ; preds = %for.body9.prol
  br label %for.body9.prol.loopexit

for.body9.prol.loopexit:                          ; preds = %for.body9.lr.ph, %for.body9.prol.loopexit.unr-lcssa
  %which.1111.unr = phi i64 [ 0, %for.body9.lr.ph ], [ %inc14.prol, %for.body9.prol.loopexit.unr-lcssa ]
  %7 = icmp ult i64 %6, 7
  br i1 %7, label %for.inc16.loopexit, label %for.body9.lr.ph.new

for.body9.lr.ph.new:                              ; preds = %for.body9.prol.loopexit
  br label %for.body9

for.body3:                                        ; preds = %for.body3, %for.body3.lr.ph.new
  %which.0108 = phi i64 [ %which.0108.unr, %for.body3.lr.ph.new ], [ %inc.7, %for.body3 ]
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre, i64 %which.0108, i32 3
  store i64 0, i64* %removed, align 8, !tbaa !13
  %inc = add nuw i64 %which.0108, 1
  %removed.1 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre, i64 %inc, i32 3
  store i64 0, i64* %removed.1, align 8, !tbaa !13
  %inc.1 = add i64 %which.0108, 2
  %removed.2 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre, i64 %inc.1, i32 3
  store i64 0, i64* %removed.2, align 8, !tbaa !13
  %inc.2 = add i64 %which.0108, 3
  %removed.3 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre, i64 %inc.2, i32 3
  store i64 0, i64* %removed.3, align 8, !tbaa !13
  %inc.3 = add i64 %which.0108, 4
  %removed.4 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre, i64 %inc.3, i32 3
  store i64 0, i64* %removed.4, align 8, !tbaa !13
  %inc.4 = add i64 %which.0108, 5
  %removed.5 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre, i64 %inc.4, i32 3
  store i64 0, i64* %removed.5, align 8, !tbaa !13
  %inc.5 = add i64 %which.0108, 6
  %removed.6 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre, i64 %inc.5, i32 3
  store i64 0, i64* %removed.6, align 8, !tbaa !13
  %inc.6 = add i64 %which.0108, 7
  %removed.7 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre, i64 %inc.6, i32 3
  store i64 0, i64* %removed.7, align 8, !tbaa !13
  %inc.7 = add i64 %which.0108, 8
  %cmp2.7 = icmp ult i64 %inc.7, %2
  br i1 %cmp2.7, label %for.body3, label %for.cond6.preheader.loopexit.unr-lcssa

for.body9:                                        ; preds = %for.body9, %for.body9.lr.ph.new
  %which.1111 = phi i64 [ %which.1111.unr, %for.body9.lr.ph.new ], [ %inc14.7, %for.body9 ]
  %removed12 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre129, i64 %which.1111, i32 3
  store i64 0, i64* %removed12, align 8, !tbaa !13
  %inc14 = add nuw i64 %which.1111, 1
  %removed12.1 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre129, i64 %inc14, i32 3
  store i64 0, i64* %removed12.1, align 8, !tbaa !13
  %inc14.1 = add i64 %which.1111, 2
  %removed12.2 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre129, i64 %inc14.1, i32 3
  store i64 0, i64* %removed12.2, align 8, !tbaa !13
  %inc14.2 = add i64 %which.1111, 3
  %removed12.3 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre129, i64 %inc14.2, i32 3
  store i64 0, i64* %removed12.3, align 8, !tbaa !13
  %inc14.3 = add i64 %which.1111, 4
  %removed12.4 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre129, i64 %inc14.3, i32 3
  store i64 0, i64* %removed12.4, align 8, !tbaa !13
  %inc14.4 = add i64 %which.1111, 5
  %removed12.5 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre129, i64 %inc14.4, i32 3
  store i64 0, i64* %removed12.5, align 8, !tbaa !13
  %inc14.5 = add i64 %which.1111, 6
  %removed12.6 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre129, i64 %inc14.5, i32 3
  store i64 0, i64* %removed12.6, align 8, !tbaa !13
  %inc14.6 = add i64 %which.1111, 7
  %removed12.7 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre129, i64 %inc14.6, i32 3
  store i64 0, i64* %removed12.7, align 8, !tbaa !13
  %inc14.7 = add i64 %which.1111, 8
  %cmp8.7 = icmp ult i64 %inc14.7, %5
  br i1 %cmp8.7, label %for.body9, label %for.inc16.loopexit.unr-lcssa

for.inc16.loopexit.unr-lcssa:                     ; preds = %for.body9
  br label %for.inc16.loopexit

for.inc16.loopexit:                               ; preds = %for.body9.prol.loopexit, %for.inc16.loopexit.unr-lcssa
  br label %for.inc16

for.inc16:                                        ; preds = %for.inc16.loopexit, %for.cond6.preheader
  %inc17 = add i64 %net.0113, 1
  %cmp = icmp ugt i64 %inc17, %0
  br i1 %cmp, label %for.end18.loopexit, label %for.cond1.preheader

for.end18.loopexit:                               ; preds = %for.inc16
  br label %for.end18

for.end18:                                        ; preds = %for.end18.loopexit, %entry
  store i64 0, i64* @removeTotalVCG, align 8, !tbaa !1
  br label %do.body

do.body:                                          ; preds = %if.then27, %for.end18
  %8 = phi i64 [ %0, %for.end18 ], [ %.pre130, %if.then27 ]
  %acyclic.0 = phi i64 [ 1, %for.end18 ], [ 0, %if.then27 ]
  %9 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %cmp.i100 = icmp eq i64 %8, 0
  br i1 %cmp.i100, label %DFSClearVCG.exit, label %for.body.i.preheader

for.body.i.preheader:                             ; preds = %do.body
  br label %for.body.i

for.body.i:                                       ; preds = %for.body.i.preheader, %for.body.i
  %net.0.i101 = phi i64 [ %inc.i, %for.body.i ], [ 1, %for.body.i.preheader ]
  %netsAboveLabel.i = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %9, i64 %net.0.i101, i32 2
  %netsBelowLabel.i = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %9, i64 %net.0.i101, i32 6
  %inc.i = add i64 %net.0.i101, 1
  %cmp.i = icmp ugt i64 %inc.i, %8
  %10 = bitcast i64* %netsAboveLabel.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %10, i8 0, i64 16, i32 8, i1 false)
  %11 = bitcast i64* %netsBelowLabel.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %11, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp.i, label %DFSClearVCG.exit.loopexit, label %for.body.i

DFSClearVCG.exit.loopexit:                        ; preds = %for.body.i
  br label %DFSClearVCG.exit

DFSClearVCG.exit:                                 ; preds = %DFSClearVCG.exit.loopexit, %do.body
  %12 = load i64*, i64** @SCC, align 8, !tbaa !5
  %13 = load i64*, i64** @perSCC, align 8, !tbaa !5
  tail call void @SCCofVCG(%struct._nodeVCGType* %9, i64* %12, i64* %13)
  %14 = load i64, i64* @totalSCC, align 8, !tbaa !1
  %cmp20102 = icmp eq i64 %14, 0
  br i1 %cmp20102, label %do.end.loopexit149, label %for.body21.lr.ph

for.body21.lr.ph:                                 ; preds = %DFSClearVCG.exit
  %15 = load i64*, i64** @perSCC, align 8, !tbaa !5
  br label %for.body21

for.cond19:                                       ; preds = %for.body21
  %cmp20 = icmp ugt i64 %inc25, %14
  br i1 %cmp20, label %do.end.loopexit, label %for.body21

for.body21:                                       ; preds = %for.body21.lr.ph, %for.cond19
  %scc.0103 = phi i64 [ 1, %for.body21.lr.ph ], [ %inc25, %for.cond19 ]
  %arrayidx22 = getelementptr inbounds i64, i64* %15, i64 %scc.0103
  %16 = load i64, i64* %arrayidx22, align 8, !tbaa !1
  %cmp23 = icmp ugt i64 %16, 1
  %inc25 = add i64 %scc.0103, 1
  br i1 %cmp23, label %if.then27, label %for.cond19

if.then27:                                        ; preds = %for.body21
  %17 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %18 = load i64*, i64** @SCC, align 8, !tbaa !5
  %19 = load %struct._constraintVCGType**, %struct._constraintVCGType*** @removeVCG, align 8, !tbaa !5
  tail call void @RemoveConstraintVCG(%struct._nodeVCGType* %17, i64* %18, i64* nonnull %15, %struct._constraintVCGType** %19)
  %.pre130 = load i64, i64* @channelNets, align 8, !tbaa !1
  br label %do.body

do.end.loopexit:                                  ; preds = %for.cond19
  br label %do.end

do.end.loopexit149:                               ; preds = %DFSClearVCG.exit
  br label %do.end

do.end:                                           ; preds = %do.end.loopexit149, %do.end.loopexit
  %20 = load i64, i64* @removeTotalVCG, align 8, !tbaa !1
  %cmp3196 = icmp eq i64 %20, 0
  br i1 %cmp3196, label %for.end128, label %for.body32.preheader

for.body32.preheader:                             ; preds = %do.end
  br label %for.body32

for.body32:                                       ; preds = %for.body32.preheader, %for.inc126
  %total.098 = phi i64 [ %total.1, %for.inc126 ], [ %20, %for.body32.preheader ]
  %rep.097 = phi i64 [ %inc127, %for.inc126 ], [ 0, %for.body32.preheader ]
  %21 = load %struct._constraintVCGType**, %struct._constraintVCGType*** @removeVCG, align 8, !tbaa !5
  %arrayidx33 = getelementptr inbounds %struct._constraintVCGType*, %struct._constraintVCGType** %21, i64 %rep.097
  %22 = load %struct._constraintVCGType*, %struct._constraintVCGType** %arrayidx33, align 8, !tbaa !5
  %top34 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %22, i64 0, i32 0
  %23 = load i64, i64* %top34, align 8, !tbaa !11
  %bot36 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %22, i64 0, i32 1
  %24 = load i64, i64* %bot36, align 8, !tbaa !9
  %25 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %netsAbove3980 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %25, i64 %23, i32 1
  %26 = load i64, i64* %netsAbove3980, align 8, !tbaa !14
  %cmp4081 = icmp eq i64 %26, 0
  br i1 %cmp4081, label %for.cond56.preheader, label %for.body41.lr.ph

for.body41.lr.ph:                                 ; preds = %for.body32
  %netsAboveHook43.phi.trans.insert = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %25, i64 %23, i32 0
  %.pre131 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsAboveHook43.phi.trans.insert, align 8, !tbaa !7
  br label %for.body41

for.cond56.preheader.loopexit:                    ; preds = %for.inc53
  br label %for.cond56.preheader

for.cond56.preheader:                             ; preds = %for.cond56.preheader.loopexit, %for.body32, %if.then47
  %netsBelow5883 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %25, i64 %24, i32 5
  %27 = load i64, i64* %netsBelow5883, align 8, !tbaa !16
  %cmp5984 = icmp eq i64 %27, 0
  br i1 %cmp5984, label %for.end74, label %for.body60.lr.ph

for.body60.lr.ph:                                 ; preds = %for.cond56.preheader
  %netsBelowHook62.phi.trans.insert = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %25, i64 %24, i32 4
  %.pre132 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsBelowHook62.phi.trans.insert, align 8, !tbaa !15
  br label %for.body60

for.body41:                                       ; preds = %for.body41.lr.ph, %for.inc53
  %which.282 = phi i64 [ 0, %for.body41.lr.ph ], [ %inc54, %for.inc53 ]
  %bot45 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre131, i64 %which.282, i32 1
  %28 = load i64, i64* %bot45, align 8, !tbaa !9
  %cmp46 = icmp eq i64 %28, %24
  br i1 %cmp46, label %if.then47, label %for.inc53

if.then47:                                        ; preds = %for.body41
  %removed51 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre131, i64 %which.282, i32 3
  store i64 0, i64* %removed51, align 8, !tbaa !13
  br label %for.cond56.preheader

for.inc53:                                        ; preds = %for.body41
  %inc54 = add i64 %which.282, 1
  %cmp40 = icmp ult i64 %inc54, %26
  br i1 %cmp40, label %for.body41, label %for.cond56.preheader.loopexit

for.body60:                                       ; preds = %for.body60.lr.ph, %for.inc72
  %which.385 = phi i64 [ 0, %for.body60.lr.ph ], [ %inc73, %for.inc72 ]
  %top64 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre132, i64 %which.385, i32 0
  %29 = load i64, i64* %top64, align 8, !tbaa !11
  %cmp65 = icmp eq i64 %29, %23
  br i1 %cmp65, label %if.then66, label %for.inc72

if.then66:                                        ; preds = %for.body60
  %removed70 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre132, i64 %which.385, i32 3
  store i64 0, i64* %removed70, align 8, !tbaa !13
  br label %for.end74

for.inc72:                                        ; preds = %for.body60
  %inc73 = add i64 %which.385, 1
  %cmp59 = icmp ult i64 %inc73, %27
  br i1 %cmp59, label %for.body60, label %for.end74.loopexit

for.end74.loopexit:                               ; preds = %for.inc72
  br label %for.end74

for.end74:                                        ; preds = %for.end74.loopexit, %for.cond56.preheader, %if.then66
  %30 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp.i6386 = icmp eq i64 %30, 0
  br i1 %cmp.i6386, label %DFSClearVCG.exit71, label %for.body.i70.preheader

for.body.i70.preheader:                           ; preds = %for.end74
  br label %for.body.i70

for.body.i70:                                     ; preds = %for.body.i70.preheader, %for.body.i70
  %net.0.i6287 = phi i64 [ %inc.i69, %for.body.i70 ], [ 1, %for.body.i70.preheader ]
  %netsAboveLabel.i65 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %25, i64 %net.0.i6287, i32 2
  %netsBelowLabel.i67 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %25, i64 %net.0.i6287, i32 6
  %inc.i69 = add i64 %net.0.i6287, 1
  %cmp.i63 = icmp ugt i64 %inc.i69, %30
  %31 = bitcast i64* %netsAboveLabel.i65 to i8*
  call void @llvm.memset.p0i8.i64(i8* %31, i8 0, i64 16, i32 8, i1 false)
  %32 = bitcast i64* %netsBelowLabel.i67 to i8*
  call void @llvm.memset.p0i8.i64(i8* %32, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp.i63, label %DFSClearVCG.exit71.loopexit, label %for.body.i70

DFSClearVCG.exit71.loopexit:                      ; preds = %for.body.i70
  br label %DFSClearVCG.exit71

DFSClearVCG.exit71:                               ; preds = %DFSClearVCG.exit71.loopexit, %for.end74
  %33 = load i64*, i64** @SCC, align 8, !tbaa !5
  %34 = load i64*, i64** @perSCC, align 8, !tbaa !5
  tail call void @SCCofVCG(%struct._nodeVCGType* %25, i64* %33, i64* %34)
  %35 = load i64, i64* @totalSCC, align 8, !tbaa !1
  %cmp7688 = icmp eq i64 %35, 0
  br i1 %cmp7688, label %if.else, label %for.body77.lr.ph

for.body77.lr.ph:                                 ; preds = %DFSClearVCG.exit71
  %36 = load i64*, i64** @perSCC, align 8, !tbaa !5
  br label %for.body77

for.cond75:                                       ; preds = %for.body77
  %cmp76 = icmp ugt i64 %inc83, %35
  br i1 %cmp76, label %if.else.loopexit, label %for.body77

for.body77:                                       ; preds = %for.body77.lr.ph, %for.cond75
  %scc.189 = phi i64 [ 1, %for.body77.lr.ph ], [ %inc83, %for.cond75 ]
  %arrayidx78 = getelementptr inbounds i64, i64* %36, i64 %scc.189
  %37 = load i64, i64* %arrayidx78, align 8, !tbaa !1
  %cmp79 = icmp ugt i64 %37, 1
  %inc83 = add i64 %scc.189, 1
  br i1 %cmp79, label %for.cond87.preheader, label %for.cond75

for.cond87.preheader:                             ; preds = %for.body77
  %38 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %netsAbove8990 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %38, i64 %23, i32 1
  %39 = load i64, i64* %netsAbove8990, align 8, !tbaa !14
  %cmp9091 = icmp eq i64 %39, 0
  br i1 %cmp9091, label %for.cond106.preheader, label %for.body91.lr.ph

for.body91.lr.ph:                                 ; preds = %for.cond87.preheader
  %netsAboveHook93.phi.trans.insert = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %38, i64 %23, i32 0
  %.pre133 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsAboveHook93.phi.trans.insert, align 8, !tbaa !7
  br label %for.body91

for.cond106.preheader.loopexit:                   ; preds = %for.inc103
  br label %for.cond106.preheader

for.cond106.preheader:                            ; preds = %for.cond106.preheader.loopexit, %for.cond87.preheader, %if.then97
  %netsBelow10893 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %38, i64 %24, i32 5
  %40 = load i64, i64* %netsBelow10893, align 8, !tbaa !16
  %cmp10994 = icmp eq i64 %40, 0
  br i1 %cmp10994, label %for.inc126, label %for.body110.lr.ph

for.body110.lr.ph:                                ; preds = %for.cond106.preheader
  %netsBelowHook112.phi.trans.insert = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %38, i64 %24, i32 4
  %.pre134 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsBelowHook112.phi.trans.insert, align 8, !tbaa !15
  br label %for.body110

for.body91:                                       ; preds = %for.body91.lr.ph, %for.inc103
  %which.492 = phi i64 [ 0, %for.body91.lr.ph ], [ %inc104, %for.inc103 ]
  %bot95 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre133, i64 %which.492, i32 1
  %41 = load i64, i64* %bot95, align 8, !tbaa !9
  %cmp96 = icmp eq i64 %41, %24
  br i1 %cmp96, label %if.then97, label %for.inc103

if.then97:                                        ; preds = %for.body91
  %removed101 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre133, i64 %which.492, i32 3
  store i64 1, i64* %removed101, align 8, !tbaa !13
  br label %for.cond106.preheader

for.inc103:                                       ; preds = %for.body91
  %inc104 = add i64 %which.492, 1
  %cmp90 = icmp ult i64 %inc104, %39
  br i1 %cmp90, label %for.body91, label %for.cond106.preheader.loopexit

for.body110:                                      ; preds = %for.body110.lr.ph, %for.inc122
  %which.595 = phi i64 [ 0, %for.body110.lr.ph ], [ %inc123, %for.inc122 ]
  %top114 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre134, i64 %which.595, i32 0
  %42 = load i64, i64* %top114, align 8, !tbaa !11
  %cmp115 = icmp eq i64 %42, %23
  br i1 %cmp115, label %if.then116, label %for.inc122

if.then116:                                       ; preds = %for.body110
  %removed120 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %.pre134, i64 %which.595, i32 3
  store i64 1, i64* %removed120, align 8, !tbaa !13
  br label %for.inc126

for.inc122:                                       ; preds = %for.body110
  %inc123 = add i64 %which.595, 1
  %cmp109 = icmp ult i64 %inc123, %40
  br i1 %cmp109, label %for.body110, label %for.inc126.loopexit

if.else.loopexit:                                 ; preds = %for.cond75
  br label %if.else

if.else:                                          ; preds = %if.else.loopexit, %DFSClearVCG.exit71
  %dec = add i64 %total.098, -1
  br label %for.inc126

for.inc126.loopexit:                              ; preds = %for.inc122
  br label %for.inc126

for.inc126:                                       ; preds = %for.inc126.loopexit, %for.cond106.preheader, %if.else, %if.then116
  %total.1 = phi i64 [ %total.098, %if.then116 ], [ %dec, %if.else ], [ %total.098, %for.cond106.preheader ], [ %total.098, %for.inc126.loopexit ]
  %inc127 = add i64 %rep.097, 1
  %43 = load i64, i64* @removeTotalVCG, align 8, !tbaa !1
  %cmp31 = icmp ult i64 %inc127, %43
  br i1 %cmp31, label %for.body32, label %for.end128.loopexit

for.end128.loopexit:                              ; preds = %for.inc126
  br label %for.end128

for.end128:                                       ; preds = %for.end128.loopexit, %do.end
  %total.0.lcssa = phi i64 [ 0, %do.end ], [ %total.1, %for.end128.loopexit ]
  %tobool129 = icmp eq i64 %acyclic.0, 0
  br i1 %tobool129, label %if.else131, label %if.then130

if.then130:                                       ; preds = %for.end128
  %puts61 = tail call i32 @puts(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @str.2, i64 0, i64 0))
  br label %if.end134

if.else131:                                       ; preds = %for.end128
  %puts = tail call i32 @puts(i8* getelementptr inbounds ([26 x i8], [26 x i8]* @str.1, i64 0, i64 0))
  %call133 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.10, i64 0, i64 0), i64 %total.0.lcssa)
  br label %if.end134

if.end134:                                        ; preds = %if.else131, %if.then130
  ret void
}

; Function Attrs: nounwind uwtable
define void @RemoveConstraintVCG(%struct._nodeVCGType* nocapture readonly %VCG, i64* nocapture readonly %SCC, i64* nocapture readonly %perSCC, %struct._constraintVCGType** nocapture %removeVCG) local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @totalSCC, align 8, !tbaa !1
  %cmp86 = icmp eq i64 %0, 0
  br i1 %cmp86, label %for.end154, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.inc152
  %scc.087 = phi i64 [ %inc153, %for.inc152 ], [ 1, %for.body.preheader ]
  %arrayidx = getelementptr inbounds i64, i64* %perSCC, i64 %scc.087
  %1 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %cmp1 = icmp ugt i64 %1, 1
  br i1 %cmp1, label %for.cond2.preheader, label %for.inc152

for.cond2.preheader:                              ; preds = %for.body
  %2 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp379 = icmp eq i64 %2, 0
  br i1 %cmp379, label %for.end128, label %for.body4.lr.ph

for.body4.lr.ph:                                  ; preds = %for.cond2.preheader
  %3 = load i64*, i64** @TOP, align 8
  %4 = load i64*, i64** @BOT, align 8
  %5 = load i64, i64* @channelColumns, align 8
  %arrayidx28 = getelementptr inbounds i64, i64* %3, i64 2
  %arrayidx40 = getelementptr inbounds i64, i64* %4, i64 2
  %arrayidx32 = getelementptr inbounds i64, i64* %4, i64 2
  br label %for.body4

for.body4:                                        ; preds = %for.body4.lr.ph, %for.inc126
  %remove.082 = phi %struct._constraintVCGType* [ null, %for.body4.lr.ph ], [ %remove.3, %for.inc126 ]
  %net.081 = phi i64 [ 1, %for.body4.lr.ph ], [ %inc127, %for.inc126 ]
  %best.080 = phi i64 [ 7, %for.body4.lr.ph ], [ %best.3, %for.inc126 ]
  %arrayidx5 = getelementptr inbounds i64, i64* %SCC, i64 %net.081
  %6 = load i64, i64* %arrayidx5, align 8, !tbaa !1
  %cmp6 = icmp eq i64 %6, %scc.087
  br i1 %cmp6, label %for.cond8.preheader, label %for.inc126

for.cond8.preheader:                              ; preds = %for.body4
  %netsAbove = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.081, i32 1
  %7 = load i64, i64* %netsAbove, align 8, !tbaa !14
  %cmp1073 = icmp eq i64 %7, 0
  br i1 %cmp1073, label %for.inc126, label %for.body11.lr.ph

for.body11.lr.ph:                                 ; preds = %for.cond8.preheader
  %netsAboveHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.081, i32 0
  %8 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsAboveHook, align 8, !tbaa !7
  br label %for.body11

for.body11:                                       ; preds = %for.body11.lr.ph, %for.inc
  %remove.177 = phi %struct._constraintVCGType* [ %remove.082, %for.body11.lr.ph ], [ %remove.2, %for.inc ]
  %best.176 = phi i64 [ %best.080, %for.body11.lr.ph ], [ %best.2, %for.inc ]
  %which.074 = phi i64 [ 0, %for.body11.lr.ph ], [ %inc, %for.inc ]
  %bot14 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %8, i64 %which.074, i32 1
  %9 = load i64, i64* %bot14, align 8, !tbaa !9
  %arrayidx15 = getelementptr inbounds i64, i64* %SCC, i64 %9
  %10 = load i64, i64* %arrayidx15, align 8, !tbaa !1
  %cmp16 = icmp eq i64 %10, %scc.087
  br i1 %cmp16, label %land.lhs.true, label %for.inc

land.lhs.true:                                    ; preds = %for.body11
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %8, i64 %which.074, i32 3
  %11 = load i64, i64* %removed, align 8, !tbaa !13
  %tobool = icmp eq i64 %11, 0
  br i1 %tobool, label %if.then20, label %for.inc

if.then20:                                        ; preds = %land.lhs.true
  %col24 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %8, i64 %which.074, i32 2
  %12 = load i64, i64* %col24, align 8, !tbaa !12
  %cmp25 = icmp eq i64 %12, 1
  br i1 %cmp25, label %if.then26, label %if.else46

if.then26:                                        ; preds = %if.then20
  %13 = load i64, i64* %arrayidx28, align 8, !tbaa !1
  %tobool29 = icmp eq i64 %13, 0
  br i1 %tobool29, label %lor.lhs.false, label %land.lhs.true30

land.lhs.true30:                                  ; preds = %if.then26
  %14 = load i64, i64* %arrayidx32, align 8, !tbaa !1
  %tobool33 = icmp eq i64 %14, 0
  br i1 %tobool33, label %if.else43, label %if.end117

lor.lhs.false:                                    ; preds = %if.then26
  %15 = load i64, i64* %arrayidx40, align 8, !tbaa !1
  %tobool41 = icmp eq i64 %15, 0
  br i1 %tobool41, label %if.end117, label %if.else43

if.else43:                                        ; preds = %land.lhs.true30, %lor.lhs.false
  br label %if.end117

if.else46:                                        ; preds = %if.then20
  %cmp47 = icmp eq i64 %12, %5
  %sub = add i64 %12, -1
  %arrayidx50 = getelementptr inbounds i64, i64* %3, i64 %sub
  %16 = load i64, i64* %arrayidx50, align 8, !tbaa !1
  %tobool51 = icmp eq i64 %16, 0
  %arrayidx64 = getelementptr inbounds i64, i64* %4, i64 %sub
  %17 = load i64, i64* %arrayidx64, align 8, !tbaa !1
  %tobool65 = icmp eq i64 %17, 0
  br i1 %cmp47, label %if.then48, label %if.else71

if.then48:                                        ; preds = %if.else46
  br i1 %tobool51, label %lor.lhs.false62, label %land.lhs.true52

land.lhs.true52:                                  ; preds = %if.then48
  br i1 %tobool65, label %if.else67, label %if.end117

lor.lhs.false62:                                  ; preds = %if.then48
  br i1 %tobool65, label %if.end117, label %if.else67

if.else67:                                        ; preds = %land.lhs.true52, %lor.lhs.false62
  br label %if.end117

if.else71:                                        ; preds = %if.else46
  br i1 %tobool51, label %lor.lhs.false85, label %land.lhs.true75

land.lhs.true75:                                  ; preds = %if.else71
  br i1 %tobool65, label %if.else90, label %if.end93

lor.lhs.false85:                                  ; preds = %if.else71
  br i1 %tobool65, label %if.end93, label %if.else90

if.else90:                                        ; preds = %land.lhs.true75, %lor.lhs.false85
  br label %if.end93

if.end93:                                         ; preds = %land.lhs.true75, %lor.lhs.false85, %if.else90
  %weight.0 = phi i64 [ 2, %if.else90 ], [ 0, %lor.lhs.false85 ], [ 3, %land.lhs.true75 ]
  %add94 = add i64 %12, 1
  %arrayidx95 = getelementptr inbounds i64, i64* %3, i64 %add94
  %18 = load i64, i64* %arrayidx95, align 8, !tbaa !1
  %tobool96 = icmp eq i64 %18, 0
  %arrayidx109 = getelementptr inbounds i64, i64* %4, i64 %add94
  %19 = load i64, i64* %arrayidx109, align 8, !tbaa !1
  %tobool110 = icmp eq i64 %19, 0
  br i1 %tobool96, label %lor.lhs.false107, label %land.lhs.true97

land.lhs.true97:                                  ; preds = %if.end93
  br i1 %tobool110, label %if.else112, label %if.then101

if.then101:                                       ; preds = %land.lhs.true97
  %add102 = add nuw nsw i64 %weight.0, 3
  br label %if.end117

lor.lhs.false107:                                 ; preds = %if.end93
  br i1 %tobool110, label %if.end117, label %if.else112

if.else112:                                       ; preds = %land.lhs.true97, %lor.lhs.false107
  %add113 = add nuw nsw i64 %weight.0, 2
  br label %if.end117

if.end117:                                        ; preds = %land.lhs.true52, %land.lhs.true30, %lor.lhs.false, %lor.lhs.false62, %lor.lhs.false107, %if.else67, %if.else112, %if.then101, %if.else43
  %weight.1 = phi i64 [ 5, %if.else43 ], [ 3, %lor.lhs.false ], [ 5, %if.else67 ], [ 3, %lor.lhs.false62 ], [ %add102, %if.then101 ], [ %add113, %if.else112 ], [ %weight.0, %lor.lhs.false107 ], [ 6, %land.lhs.true30 ], [ 6, %land.lhs.true52 ]
  %cmp118 = icmp ult i64 %weight.1, %best.176
  %arrayidx122 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %8, i64 %which.074
  %weight.1.best.176 = select i1 %cmp118, i64 %weight.1, i64 %best.176
  %arrayidx122.remove.177 = select i1 %cmp118, %struct._constraintVCGType* %arrayidx122, %struct._constraintVCGType* %remove.177
  br label %for.inc

for.inc:                                          ; preds = %if.end117, %land.lhs.true, %for.body11
  %best.2 = phi i64 [ %best.176, %land.lhs.true ], [ %best.176, %for.body11 ], [ %weight.1.best.176, %if.end117 ]
  %remove.2 = phi %struct._constraintVCGType* [ %remove.177, %land.lhs.true ], [ %remove.177, %for.body11 ], [ %arrayidx122.remove.177, %if.end117 ]
  %inc = add nuw i64 %which.074, 1
  %cmp10 = icmp ult i64 %inc, %7
  br i1 %cmp10, label %for.body11, label %for.inc126.loopexit

for.inc126.loopexit:                              ; preds = %for.inc
  br label %for.inc126

for.inc126:                                       ; preds = %for.inc126.loopexit, %for.cond8.preheader, %for.body4
  %best.3 = phi i64 [ %best.080, %for.body4 ], [ %best.080, %for.cond8.preheader ], [ %best.2, %for.inc126.loopexit ]
  %remove.3 = phi %struct._constraintVCGType* [ %remove.082, %for.body4 ], [ %remove.082, %for.cond8.preheader ], [ %remove.2, %for.inc126.loopexit ]
  %inc127 = add i64 %net.081, 1
  %cmp3 = icmp ugt i64 %inc127, %2
  br i1 %cmp3, label %for.end128.loopexit, label %for.body4

for.end128.loopexit:                              ; preds = %for.inc126
  br label %for.end128

for.end128:                                       ; preds = %for.end128.loopexit, %for.cond2.preheader
  %remove.0.lcssa = phi %struct._constraintVCGType* [ null, %for.cond2.preheader ], [ %remove.3, %for.end128.loopexit ]
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 8, !tbaa !5
  %call = tail call i32 @fflush(%struct._IO_FILE* %20)
  %21 = load i64, i64* @removeTotalVCG, align 8, !tbaa !1
  %arrayidx129 = getelementptr inbounds %struct._constraintVCGType*, %struct._constraintVCGType** %removeVCG, i64 %21
  store %struct._constraintVCGType* %remove.0.lcssa, %struct._constraintVCGType** %arrayidx129, align 8, !tbaa !5
  %inc130 = add i64 %21, 1
  store i64 %inc130, i64* @removeTotalVCG, align 8, !tbaa !1
  %top131 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %remove.0.lcssa, i64 0, i32 0
  %22 = load i64, i64* %top131, align 8, !tbaa !11
  %bot132 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %remove.0.lcssa, i64 0, i32 1
  %23 = load i64, i64* %bot132, align 8, !tbaa !9
  %removed133 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %remove.0.lcssa, i64 0, i32 3
  store i64 1, i64* %removed133, align 8, !tbaa !13
  %netsBelow = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %23, i32 5
  %24 = load i64, i64* %netsBelow, align 8, !tbaa !16
  %cmp13684 = icmp eq i64 %24, 0
  br i1 %cmp13684, label %for.inc152, label %for.body137.lr.ph

for.body137.lr.ph:                                ; preds = %for.end128
  %netsBelowHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %23, i32 4
  %25 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsBelowHook, align 8, !tbaa !15
  br label %for.body137

for.body137:                                      ; preds = %for.body137.lr.ph, %for.inc148
  %which.185 = phi i64 [ 0, %for.body137.lr.ph ], [ %inc149, %for.inc148 ]
  %top140 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %25, i64 %which.185, i32 0
  %26 = load i64, i64* %top140, align 8, !tbaa !11
  %cmp141 = icmp eq i64 %26, %22
  br i1 %cmp141, label %if.then142, label %for.inc148

if.then142:                                       ; preds = %for.body137
  %removed146 = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %25, i64 %which.185, i32 3
  store i64 1, i64* %removed146, align 8, !tbaa !13
  br label %for.inc152

for.inc148:                                       ; preds = %for.body137
  %inc149 = add i64 %which.185, 1
  %cmp136 = icmp ult i64 %inc149, %24
  br i1 %cmp136, label %for.body137, label %for.inc152.loopexit

for.inc152.loopexit:                              ; preds = %for.inc148
  br label %for.inc152

for.inc152:                                       ; preds = %for.inc152.loopexit, %for.end128, %for.body, %if.then142
  %inc153 = add i64 %scc.087, 1
  %27 = load i64, i64* @totalSCC, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc153, %27
  br i1 %cmp, label %for.end154.loopexit, label %for.body

for.end154.loopexit:                              ; preds = %for.inc152
  br label %for.end154

for.end154:                                       ; preds = %for.end154.loopexit, %entry
  ret void
}

; Function Attrs: nounwind
declare i32 @fflush(%struct._IO_FILE* nocapture) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define i64 @ExistPathAboveVCG(%struct._nodeVCGType* nocapture %VCG, i64 %above, i64 %below) local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp.i3 = icmp eq i64 %0, 0
  br i1 %cmp.i3, label %DFSClearVCG.exit, label %for.body.i.preheader

for.body.i.preheader:                             ; preds = %entry
  br label %for.body.i

for.body.i:                                       ; preds = %for.body.i.preheader, %for.body.i
  %net.0.i4 = phi i64 [ %inc.i, %for.body.i ], [ 1, %for.body.i.preheader ]
  %netsAboveLabel.i = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i4, i32 2
  %netsBelowLabel.i = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i4, i32 6
  %inc.i = add i64 %net.0.i4, 1
  %cmp.i = icmp ugt i64 %inc.i, %0
  %1 = bitcast i64* %netsAboveLabel.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %1, i8 0, i64 16, i32 8, i1 false)
  %2 = bitcast i64* %netsBelowLabel.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %2, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp.i, label %DFSClearVCG.exit.loopexit, label %for.body.i

DFSClearVCG.exit.loopexit:                        ; preds = %for.body.i
  br label %DFSClearVCG.exit

DFSClearVCG.exit:                                 ; preds = %DFSClearVCG.exit.loopexit, %entry
  tail call void @DFSAboveVCG(%struct._nodeVCGType* %VCG, i64 %above)
  %netsAboveReached = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %below, i32 3
  %3 = load i64, i64* %netsAboveReached, align 8, !tbaa !17
  ret i64 %3
}

; Function Attrs: nounwind uwtable
define void @LongestPathVCG(%struct._nodeVCGType* nocapture %VCG, i64 %net) local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp.i44 = icmp eq i64 %0, 0
  br i1 %cmp.i44, label %DFSClearVCG.exit, label %for.body.i.preheader

for.body.i.preheader:                             ; preds = %entry
  br label %for.body.i

for.body.i:                                       ; preds = %for.body.i.preheader, %for.body.i
  %net.0.i45 = phi i64 [ %inc.i, %for.body.i ], [ 1, %for.body.i.preheader ]
  %netsAboveLabel.i = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i45, i32 2
  %netsBelowLabel.i = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i45, i32 6
  %inc.i = add i64 %net.0.i45, 1
  %cmp.i = icmp ugt i64 %inc.i, %0
  %1 = bitcast i64* %netsAboveLabel.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %1, i8 0, i64 16, i32 8, i1 false)
  %2 = bitcast i64* %netsBelowLabel.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %2, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp.i, label %DFSClearVCG.exit.loopexit, label %for.body.i

DFSClearVCG.exit.loopexit:                        ; preds = %for.body.i
  br label %DFSClearVCG.exit

DFSClearVCG.exit:                                 ; preds = %DFSClearVCG.exit.loopexit, %entry
  %call = tail call i64 @DFSAboveLongestPathVCG(%struct._nodeVCGType* %VCG, i64 %net)
  %sub = add i64 %call, -1
  store i64 %sub, i64* @cardBotNotPref, align 8, !tbaa !1
  %3 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp41 = icmp eq i64 %3, 0
  br i1 %cmp41, label %for.cond.i25.preheader, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %DFSClearVCG.exit
  %4 = load i64*, i64** @tracksBotNotPref, align 8, !tbaa !5
  %xtraiter = and i64 %3, 1
  %lcmp.mod = icmp eq i64 %xtraiter, 0
  br i1 %lcmp.mod, label %for.body.prol.loopexit, label %for.body.prol.preheader

for.body.prol.preheader:                          ; preds = %for.body.lr.ph
  br label %for.body.prol

for.body.prol:                                    ; preds = %for.body.prol.preheader
  %cmp1.prol = icmp eq i64 %sub, 0
  %arrayidx.prol = getelementptr inbounds i64, i64* %4, i64 %3
  br i1 %cmp1.prol, label %if.else.prol, label %if.then.prol

if.then.prol:                                     ; preds = %for.body.prol
  store i64 1, i64* %arrayidx.prol, align 8, !tbaa !1
  %dec.prol = add i64 %call, -2
  br label %for.inc.prol

if.else.prol:                                     ; preds = %for.body.prol
  store i64 0, i64* %arrayidx.prol, align 8, !tbaa !1
  br label %for.inc.prol

for.inc.prol:                                     ; preds = %if.else.prol, %if.then.prol
  %bot.1.prol = phi i64 [ %dec.prol, %if.then.prol ], [ 0, %if.else.prol ]
  %dec3.prol = add i64 %3, -1
  br label %for.body.prol.loopexit

for.body.prol.loopexit:                           ; preds = %for.body.lr.ph, %for.inc.prol
  %bot.043.unr = phi i64 [ %sub, %for.body.lr.ph ], [ %bot.1.prol, %for.inc.prol ]
  %track.042.unr = phi i64 [ %3, %for.body.lr.ph ], [ %dec3.prol, %for.inc.prol ]
  %5 = icmp eq i64 %3, 1
  br i1 %5, label %for.cond.i25.preheader.loopexit, label %for.body.lr.ph.new

for.body.lr.ph.new:                               ; preds = %for.body.prol.loopexit
  br label %for.body

for.cond.i25.preheader.loopexit.unr-lcssa:        ; preds = %for.inc.1
  br label %for.cond.i25.preheader.loopexit

for.cond.i25.preheader.loopexit:                  ; preds = %for.body.prol.loopexit, %for.cond.i25.preheader.loopexit.unr-lcssa
  br label %for.cond.i25.preheader

for.cond.i25.preheader:                           ; preds = %for.cond.i25.preheader.loopexit, %DFSClearVCG.exit
  %6 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp.i2439 = icmp eq i64 %6, 0
  br i1 %cmp.i2439, label %DFSClearVCG.exit32, label %for.body.i31.preheader

for.body.i31.preheader:                           ; preds = %for.cond.i25.preheader
  br label %for.body.i31

for.body:                                         ; preds = %for.inc.1, %for.body.lr.ph.new
  %bot.043 = phi i64 [ %bot.043.unr, %for.body.lr.ph.new ], [ %bot.1.1, %for.inc.1 ]
  %track.042 = phi i64 [ %track.042.unr, %for.body.lr.ph.new ], [ %dec3.1, %for.inc.1 ]
  %cmp1 = icmp eq i64 %bot.043, 0
  %arrayidx = getelementptr inbounds i64, i64* %4, i64 %track.042
  br i1 %cmp1, label %if.else, label %if.then

if.then:                                          ; preds = %for.body
  store i64 1, i64* %arrayidx, align 8, !tbaa !1
  %dec = add i64 %bot.043, -1
  br label %for.inc

if.else:                                          ; preds = %for.body
  store i64 0, i64* %arrayidx, align 8, !tbaa !1
  br label %for.inc

for.inc:                                          ; preds = %if.then, %if.else
  %bot.1 = phi i64 [ %dec, %if.then ], [ 0, %if.else ]
  %dec3 = add i64 %track.042, -1
  %cmp1.1 = icmp eq i64 %bot.1, 0
  %arrayidx.1 = getelementptr inbounds i64, i64* %4, i64 %dec3
  br i1 %cmp1.1, label %if.else.1, label %if.then.1

for.body.i31:                                     ; preds = %for.body.i31.preheader, %for.body.i31
  %net.0.i2340 = phi i64 [ %inc.i30, %for.body.i31 ], [ 1, %for.body.i31.preheader ]
  %netsAboveLabel.i26 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i2340, i32 2
  %netsBelowLabel.i28 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i2340, i32 6
  %inc.i30 = add i64 %net.0.i2340, 1
  %cmp.i24 = icmp ugt i64 %inc.i30, %6
  %7 = bitcast i64* %netsAboveLabel.i26 to i8*
  call void @llvm.memset.p0i8.i64(i8* %7, i8 0, i64 16, i32 8, i1 false)
  %8 = bitcast i64* %netsBelowLabel.i28 to i8*
  call void @llvm.memset.p0i8.i64(i8* %8, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp.i24, label %DFSClearVCG.exit32.loopexit, label %for.body.i31

DFSClearVCG.exit32.loopexit:                      ; preds = %for.body.i31
  br label %DFSClearVCG.exit32

DFSClearVCG.exit32:                               ; preds = %DFSClearVCG.exit32.loopexit, %for.cond.i25.preheader
  %call4 = tail call i64 @DFSBelowLongestPathVCG(%struct._nodeVCGType* %VCG, i64 %net)
  %sub5 = add i64 %call4, -1
  store i64 %sub5, i64* @cardTopNotPref, align 8, !tbaa !1
  %9 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp736 = icmp eq i64 %9, 0
  br i1 %cmp736, label %for.end32, label %for.body8.lr.ph

for.body8.lr.ph:                                  ; preds = %DFSClearVCG.exit32
  %10 = load i64*, i64** @tracksTopNotPref, align 8, !tbaa !5
  br label %for.body8

for.cond18.preheader:                             ; preds = %for.inc16
  %phitmp = icmp eq i64 %14, 0
  br i1 %phitmp, label %for.end32, label %for.body20.lr.ph

for.body20.lr.ph:                                 ; preds = %for.cond18.preheader
  %11 = load i64*, i64** @tracksTopNotPref, align 8, !tbaa !5
  %12 = load i64*, i64** @tracksBotNotPref, align 8
  %13 = load i64*, i64** @tracksNotPref, align 8
  br label %for.body20

for.body8:                                        ; preds = %for.body8.lr.ph, %for.inc16
  %top.038 = phi i64 [ %sub5, %for.body8.lr.ph ], [ %top.1, %for.inc16 ]
  %track.137 = phi i64 [ 1, %for.body8.lr.ph ], [ %inc, %for.inc16 ]
  %cmp9 = icmp eq i64 %top.038, 0
  %arrayidx11 = getelementptr inbounds i64, i64* %10, i64 %track.137
  br i1 %cmp9, label %if.else13, label %if.then10

if.then10:                                        ; preds = %for.body8
  store i64 1, i64* %arrayidx11, align 8, !tbaa !1
  %dec12 = add i64 %top.038, -1
  br label %for.inc16

if.else13:                                        ; preds = %for.body8
  store i64 0, i64* %arrayidx11, align 8, !tbaa !1
  br label %for.inc16

for.inc16:                                        ; preds = %if.then10, %if.else13
  %top.1 = phi i64 [ %dec12, %if.then10 ], [ 0, %if.else13 ]
  %inc = add i64 %track.137, 1
  %14 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp7 = icmp ugt i64 %inc, %14
  br i1 %cmp7, label %for.cond18.preheader, label %for.body8

for.body20:                                       ; preds = %for.body20.lr.ph, %for.inc30
  %not.035 = phi i64 [ 0, %for.body20.lr.ph ], [ %not.1, %for.inc30 ]
  %track.234 = phi i64 [ 1, %for.body20.lr.ph ], [ %inc31, %for.inc30 ]
  %arrayidx21 = getelementptr inbounds i64, i64* %11, i64 %track.234
  %15 = load i64, i64* %arrayidx21, align 8, !tbaa !1
  %tobool = icmp eq i64 %15, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then24

lor.lhs.false:                                    ; preds = %for.body20
  %arrayidx22 = getelementptr inbounds i64, i64* %12, i64 %track.234
  %16 = load i64, i64* %arrayidx22, align 8, !tbaa !1
  %tobool23 = icmp eq i64 %16, 0
  br i1 %tobool23, label %if.else27, label %if.then24

if.then24:                                        ; preds = %lor.lhs.false, %for.body20
  %arrayidx25 = getelementptr inbounds i64, i64* %13, i64 %track.234
  store i64 1, i64* %arrayidx25, align 8, !tbaa !1
  %inc26 = add i64 %not.035, 1
  br label %for.inc30

if.else27:                                        ; preds = %lor.lhs.false
  %arrayidx28 = getelementptr inbounds i64, i64* %13, i64 %track.234
  store i64 0, i64* %arrayidx28, align 8, !tbaa !1
  br label %for.inc30

for.inc30:                                        ; preds = %if.then24, %if.else27
  %not.1 = phi i64 [ %inc26, %if.then24 ], [ %not.035, %if.else27 ]
  %inc31 = add i64 %track.234, 1
  %17 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp19 = icmp ugt i64 %inc31, %17
  br i1 %cmp19, label %for.end32.loopexit, label %for.body20

for.end32.loopexit:                               ; preds = %for.inc30
  br label %for.end32

for.end32:                                        ; preds = %for.end32.loopexit, %DFSClearVCG.exit32, %for.cond18.preheader
  %not.0.lcssa = phi i64 [ 0, %for.cond18.preheader ], [ 0, %DFSClearVCG.exit32 ], [ %not.1, %for.end32.loopexit ]
  store i64 %not.0.lcssa, i64* @cardNotPref, align 8, !tbaa !1
  ret void

if.then.1:                                        ; preds = %for.inc
  store i64 1, i64* %arrayidx.1, align 8, !tbaa !1
  %dec.1 = add i64 %bot.1, -1
  br label %for.inc.1

if.else.1:                                        ; preds = %for.inc
  store i64 0, i64* %arrayidx.1, align 8, !tbaa !1
  br label %for.inc.1

for.inc.1:                                        ; preds = %if.else.1, %if.then.1
  %bot.1.1 = phi i64 [ %dec.1, %if.then.1 ], [ 0, %if.else.1 ]
  %dec3.1 = add i64 %track.042, -2
  %cmp.1 = icmp eq i64 %dec3.1, 0
  br i1 %cmp.1, label %for.cond.i25.preheader.loopexit.unr-lcssa, label %for.body
}

; Function Attrs: nounwind uwtable
define i64 @DFSAboveLongestPathVCG(%struct._nodeVCGType* nocapture %VCG, i64 %net) local_unnamed_addr #0 {
entry:
  %netsAboveReached = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 3
  store i64 1, i64* %netsAboveReached, align 8, !tbaa !17
  %netsAbove = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 1
  %0 = load i64, i64* %netsAbove, align 8, !tbaa !14
  %cmp19 = icmp eq i64 %0, 0
  br i1 %cmp19, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %netsAboveHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 0
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %1 = phi i64 [ %0, %for.body.lr.ph ], [ %6, %for.inc ]
  %longest.021 = phi i64 [ 0, %for.body.lr.ph ], [ %longest.1, %for.inc ]
  %s.020 = phi i64 [ 0, %for.body.lr.ph ], [ %inc, %for.inc ]
  %2 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsAboveHook, align 8, !tbaa !7
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.020, i32 3
  %3 = load i64, i64* %removed, align 8, !tbaa !13
  %tobool = icmp eq i64 %3, 0
  br i1 %tobool, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %bot = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.020, i32 1
  %4 = load i64, i64* %bot, align 8, !tbaa !9
  %netsAboveReached8 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %4, i32 3
  %5 = load i64, i64* %netsAboveReached8, align 8, !tbaa !17
  %tobool9 = icmp eq i64 %5, 0
  br i1 %tobool9, label %if.then10, label %for.inc

if.then10:                                        ; preds = %if.then
  %call = tail call i64 @DFSAboveLongestPathVCG(%struct._nodeVCGType* nonnull %VCG, i64 %4)
  %cmp11 = icmp ugt i64 %call, %longest.021
  %call.longest.0 = select i1 %cmp11, i64 %call, i64 %longest.021
  %.pre = load i64, i64* %netsAbove, align 8, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %if.then10, %if.then, %for.body
  %6 = phi i64 [ %1, %for.body ], [ %1, %if.then ], [ %.pre, %if.then10 ]
  %longest.1 = phi i64 [ %longest.021, %for.body ], [ %longest.021, %if.then ], [ %call.longest.0, %if.then10 ]
  %inc = add i64 %s.020, 1
  %cmp = icmp ult i64 %inc, %6
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %longest.0.lcssa = phi i64 [ 0, %entry ], [ %longest.1, %for.end.loopexit ]
  %add = add i64 %longest.0.lcssa, 1
  ret i64 %add
}

; Function Attrs: nounwind uwtable
define i64 @DFSBelowLongestPathVCG(%struct._nodeVCGType* nocapture %VCG, i64 %net) local_unnamed_addr #0 {
entry:
  %netsBelowReached = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 7
  store i64 1, i64* %netsBelowReached, align 8, !tbaa !18
  %netsBelow = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 5
  %0 = load i64, i64* %netsBelow, align 8, !tbaa !16
  %cmp19 = icmp eq i64 %0, 0
  br i1 %cmp19, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %netsBelowHook = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net, i32 4
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %1 = phi i64 [ %0, %for.body.lr.ph ], [ %6, %for.inc ]
  %longest.021 = phi i64 [ 0, %for.body.lr.ph ], [ %longest.1, %for.inc ]
  %s.020 = phi i64 [ 0, %for.body.lr.ph ], [ %inc, %for.inc ]
  %2 = load %struct._constraintVCGType*, %struct._constraintVCGType** %netsBelowHook, align 8, !tbaa !15
  %removed = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.020, i32 3
  %3 = load i64, i64* %removed, align 8, !tbaa !13
  %tobool = icmp eq i64 %3, 0
  br i1 %tobool, label %if.then, label %for.inc

if.then:                                          ; preds = %for.body
  %top = getelementptr inbounds %struct._constraintVCGType, %struct._constraintVCGType* %2, i64 %s.020, i32 0
  %4 = load i64, i64* %top, align 8, !tbaa !11
  %netsBelowReached8 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %4, i32 7
  %5 = load i64, i64* %netsBelowReached8, align 8, !tbaa !18
  %tobool9 = icmp eq i64 %5, 0
  br i1 %tobool9, label %if.then10, label %for.inc

if.then10:                                        ; preds = %if.then
  %call = tail call i64 @DFSBelowLongestPathVCG(%struct._nodeVCGType* nonnull %VCG, i64 %4)
  %cmp11 = icmp ugt i64 %call, %longest.021
  %call.longest.0 = select i1 %cmp11, i64 %call, i64 %longest.021
  %.pre = load i64, i64* %netsBelow, align 8, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %if.then10, %if.then, %for.body
  %6 = phi i64 [ %1, %for.body ], [ %1, %if.then ], [ %.pre, %if.then10 ]
  %longest.1 = phi i64 [ %longest.021, %for.body ], [ %longest.021, %if.then ], [ %call.longest.0, %if.then10 ]
  %inc = add i64 %s.020, 1
  %cmp = icmp ult i64 %inc, %6
  br i1 %cmp, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %longest.0.lcssa = phi i64 [ 0, %entry ], [ %longest.1, %for.end.loopexit ]
  %add = add i64 %longest.0.lcssa, 1
  ret i64 %add
}

; Function Attrs: nounwind uwtable
define i64 @VCV(%struct._nodeVCGType* %VCG, i64 %check, i64 %track, i64* nocapture readonly %assign) local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp63 = icmp eq i64 %0, 0
  br i1 %cmp63, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %netsAboveReached.i42 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %check, i32 3
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %1 = phi i64 [ %0, %for.body.lr.ph ], [ %16, %for.inc ]
  %vcv.067 = phi i64 [ 0, %for.body.lr.ph ], [ %vcv.1, %for.inc ]
  %net.064 = phi i64 [ 1, %for.body.lr.ph ], [ %inc25, %for.inc ]
  %arrayidx = getelementptr inbounds i64, i64* %assign, i64 %net.064
  %2 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %tobool = icmp eq i64 %2, 0
  br i1 %tobool, label %for.inc, label %if.then

if.then:                                          ; preds = %for.body
  %cmp2 = icmp ult i64 %2, %track
  br i1 %cmp2, label %for.cond.i.i.preheader, label %if.else

for.cond.i.i.preheader:                           ; preds = %if.then
  %cmp.i.i61 = icmp eq i64 %1, 0
  br i1 %cmp.i.i61, label %ExistPathAboveVCG.exit, label %for.body.i.i.preheader

for.body.i.i.preheader:                           ; preds = %for.cond.i.i.preheader
  br label %for.body.i.i

for.body.i.i:                                     ; preds = %for.body.i.i.preheader, %for.body.i.i
  %net.0.i.i62 = phi i64 [ %inc.i.i, %for.body.i.i ], [ 1, %for.body.i.i.preheader ]
  %netsAboveLabel.i.i = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i.i62, i32 2
  %netsBelowLabel.i.i = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i.i62, i32 6
  %inc.i.i = add i64 %net.0.i.i62, 1
  %cmp.i.i = icmp ugt i64 %inc.i.i, %1
  %3 = bitcast i64* %netsAboveLabel.i.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %3, i8 0, i64 16, i32 8, i1 false)
  %4 = bitcast i64* %netsBelowLabel.i.i to i8*
  call void @llvm.memset.p0i8.i64(i8* %4, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp.i.i, label %ExistPathAboveVCG.exit.loopexit, label %for.body.i.i

ExistPathAboveVCG.exit.loopexit:                  ; preds = %for.body.i.i
  br label %ExistPathAboveVCG.exit

ExistPathAboveVCG.exit:                           ; preds = %ExistPathAboveVCG.exit.loopexit, %for.cond.i.i.preheader
  tail call void @DFSAboveVCG(%struct._nodeVCGType* %VCG, i64 %net.064) #4
  %5 = load i64, i64* %netsAboveReached.i42, align 8, !tbaa !17
  %not.tobool4 = icmp ne i64 %5, 0
  %inc = zext i1 %not.tobool4 to i64
  %vcv.0.inc = add i64 %inc, %vcv.067
  br label %for.inc

if.else:                                          ; preds = %if.then
  %cmp7 = icmp ugt i64 %2, %track
  %cmp.i.i2359 = icmp eq i64 %1, 0
  br i1 %cmp7, label %for.cond.i.i24.preheader, label %for.cond.i.i35.preheader

for.cond.i.i35.preheader:                         ; preds = %if.else
  br i1 %cmp.i.i2359, label %ExistPathAboveVCG.exit43, label %for.body.i.i41.preheader

for.body.i.i41.preheader:                         ; preds = %for.cond.i.i35.preheader
  br label %for.body.i.i41

for.cond.i.i24.preheader:                         ; preds = %if.else
  br i1 %cmp.i.i2359, label %ExistPathAboveVCG.exit32, label %for.body.i.i30.preheader

for.body.i.i30.preheader:                         ; preds = %for.cond.i.i24.preheader
  br label %for.body.i.i30

for.body.i.i30:                                   ; preds = %for.body.i.i30.preheader, %for.body.i.i30
  %net.0.i.i2260 = phi i64 [ %inc.i.i29, %for.body.i.i30 ], [ 1, %for.body.i.i30.preheader ]
  %netsAboveLabel.i.i25 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i.i2260, i32 2
  %netsBelowLabel.i.i27 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i.i2260, i32 6
  %inc.i.i29 = add i64 %net.0.i.i2260, 1
  %cmp.i.i23 = icmp ugt i64 %inc.i.i29, %1
  %6 = bitcast i64* %netsAboveLabel.i.i25 to i8*
  call void @llvm.memset.p0i8.i64(i8* %6, i8 0, i64 16, i32 8, i1 false)
  %7 = bitcast i64* %netsBelowLabel.i.i27 to i8*
  call void @llvm.memset.p0i8.i64(i8* %7, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp.i.i23, label %ExistPathAboveVCG.exit32.loopexit, label %for.body.i.i30

ExistPathAboveVCG.exit32.loopexit:                ; preds = %for.body.i.i30
  br label %ExistPathAboveVCG.exit32

ExistPathAboveVCG.exit32:                         ; preds = %ExistPathAboveVCG.exit32.loopexit, %for.cond.i.i24.preheader
  tail call void @DFSAboveVCG(%struct._nodeVCGType* %VCG, i64 %check) #4
  %netsAboveReached.i31 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.064, i32 3
  %8 = load i64, i64* %netsAboveReached.i31, align 8, !tbaa !17
  %not.tobool10 = icmp ne i64 %8, 0
  %inc12 = zext i1 %not.tobool10 to i64
  %vcv.0.inc12 = add i64 %inc12, %vcv.067
  br label %for.inc

for.body.i.i41:                                   ; preds = %for.body.i.i41.preheader, %for.body.i.i41
  %net.0.i.i3356 = phi i64 [ %inc.i.i40, %for.body.i.i41 ], [ 1, %for.body.i.i41.preheader ]
  %netsAboveLabel.i.i36 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i.i3356, i32 2
  %netsBelowLabel.i.i38 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i.i3356, i32 6
  %inc.i.i40 = add i64 %net.0.i.i3356, 1
  %cmp.i.i34 = icmp ugt i64 %inc.i.i40, %1
  %9 = bitcast i64* %netsAboveLabel.i.i36 to i8*
  call void @llvm.memset.p0i8.i64(i8* %9, i8 0, i64 16, i32 8, i1 false)
  %10 = bitcast i64* %netsBelowLabel.i.i38 to i8*
  call void @llvm.memset.p0i8.i64(i8* %10, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp.i.i34, label %ExistPathAboveVCG.exit43.loopexit, label %for.body.i.i41

ExistPathAboveVCG.exit43.loopexit:                ; preds = %for.body.i.i41
  br label %ExistPathAboveVCG.exit43

ExistPathAboveVCG.exit43:                         ; preds = %ExistPathAboveVCG.exit43.loopexit, %for.cond.i.i35.preheader
  tail call void @DFSAboveVCG(%struct._nodeVCGType* %VCG, i64 %net.064) #4
  %11 = load i64, i64* %netsAboveReached.i42, align 8, !tbaa !17
  %tobool16 = icmp eq i64 %11, 0
  br i1 %tobool16, label %for.cond.i.i46.preheader, label %if.then19

for.cond.i.i46.preheader:                         ; preds = %ExistPathAboveVCG.exit43
  %12 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp.i.i4557 = icmp eq i64 %12, 0
  br i1 %cmp.i.i4557, label %ExistPathAboveVCG.exit54, label %for.body.i.i52.preheader

for.body.i.i52.preheader:                         ; preds = %for.cond.i.i46.preheader
  br label %for.body.i.i52

for.body.i.i52:                                   ; preds = %for.body.i.i52.preheader, %for.body.i.i52
  %net.0.i.i4458 = phi i64 [ %inc.i.i51, %for.body.i.i52 ], [ 1, %for.body.i.i52.preheader ]
  %netsAboveLabel.i.i47 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i.i4458, i32 2
  %netsBelowLabel.i.i49 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.0.i.i4458, i32 6
  %inc.i.i51 = add i64 %net.0.i.i4458, 1
  %cmp.i.i45 = icmp ugt i64 %inc.i.i51, %12
  %13 = bitcast i64* %netsAboveLabel.i.i47 to i8*
  call void @llvm.memset.p0i8.i64(i8* %13, i8 0, i64 16, i32 8, i1 false)
  %14 = bitcast i64* %netsBelowLabel.i.i49 to i8*
  call void @llvm.memset.p0i8.i64(i8* %14, i8 0, i64 16, i32 8, i1 false)
  br i1 %cmp.i.i45, label %ExistPathAboveVCG.exit54.loopexit, label %for.body.i.i52

ExistPathAboveVCG.exit54.loopexit:                ; preds = %for.body.i.i52
  br label %ExistPathAboveVCG.exit54

ExistPathAboveVCG.exit54:                         ; preds = %ExistPathAboveVCG.exit54.loopexit, %for.cond.i.i46.preheader
  tail call void @DFSAboveVCG(%struct._nodeVCGType* nonnull %VCG, i64 %check) #4
  %netsAboveReached.i53 = getelementptr inbounds %struct._nodeVCGType, %struct._nodeVCGType* %VCG, i64 %net.064, i32 3
  %15 = load i64, i64* %netsAboveReached.i53, align 8, !tbaa !17
  %tobool18 = icmp eq i64 %15, 0
  br i1 %tobool18, label %for.inc, label %if.then19

if.then19:                                        ; preds = %ExistPathAboveVCG.exit54, %ExistPathAboveVCG.exit43
  %inc20 = add i64 %vcv.067, 1
  br label %for.inc

for.inc:                                          ; preds = %ExistPathAboveVCG.exit32, %ExistPathAboveVCG.exit, %ExistPathAboveVCG.exit54, %for.body, %if.then19
  %vcv.1 = phi i64 [ %inc20, %if.then19 ], [ %vcv.067, %ExistPathAboveVCG.exit54 ], [ %vcv.067, %for.body ], [ %vcv.0.inc, %ExistPathAboveVCG.exit ], [ %vcv.0.inc12, %ExistPathAboveVCG.exit32 ]
  %inc25 = add i64 %net.064, 1
  %16 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc25, %16
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %vcv.0.lcssa = phi i64 [ 0, %entry ], [ %vcv.1, %for.end.loopexit ]
  ret i64 %vcv.0.lcssa
}

declare i32 @putchar(i32)

; Function Attrs: nounwind
declare i32 @puts(i8* nocapture readonly) #4

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i32, i1) #2

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind }
attributes #3 = { norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"long", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"any pointer", !3, i64 0}
!7 = !{!8, !6, i64 0}
!8 = !{!"_nodeVCGType", !6, i64 0, !2, i64 8, !2, i64 16, !2, i64 24, !6, i64 32, !2, i64 40, !2, i64 48, !2, i64 56}
!9 = !{!10, !2, i64 8}
!10 = !{!"_constraintVCGType", !2, i64 0, !2, i64 8, !2, i64 16, !2, i64 24}
!11 = !{!10, !2, i64 0}
!12 = !{!10, !2, i64 16}
!13 = !{!10, !2, i64 24}
!14 = !{!8, !2, i64 8}
!15 = !{!8, !6, i64 32}
!16 = !{!8, !2, i64 40}
!17 = !{!8, !2, i64 24}
!18 = !{!8, !2, i64 56}
!19 = !{!8, !2, i64 16}
!20 = !{!8, !2, i64 48}
!21 = distinct !{!21, !22}
!22 = !{!"llvm.loop.unroll.disable"}
!23 = distinct !{!23, !22}
