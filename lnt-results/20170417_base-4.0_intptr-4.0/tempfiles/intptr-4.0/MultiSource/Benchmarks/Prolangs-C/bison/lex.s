	.text
	.file	"lex.bc"
	.globl	init_lex
	.p2align	4, 0x90
	.type	init_lex,@function
init_lex:                               # @init_lex
	.cfi_startproc
# BB#0:
	movl	$-1, unlexed(%rip)
	retq
.Lfunc_end0:
	.size	init_lex, .Lfunc_end0-init_lex
	.cfi_endproc

	.globl	skip_white_space
	.p2align	4, 0x90
	.type	skip_white_space,@function
skip_white_space:                       # @skip_white_space
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	jmp	.LBB1_1
.LBB1_15:                               #   in Loop: Header=BB1_1 Depth=1
	incl	lineno(%rip)
	.p2align	4, 0x90
.LBB1_1:                                # %.loopexit14.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
                                        #       Child Loop BB1_12 Depth 3
	movq	finput(%rip), %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-9(%rax), %ecx
	cmpl	$38, %ecx
	ja	.LBB1_16
# BB#2:                                 # %.loopexit14.sink.split
                                        #   in Loop: Header=BB1_1 Depth=1
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %eax
	je	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_1 Depth=1
	subq	$8, %rsp
.Lcfi1:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%eax, %esi
	pushq	$0
.Lcfi2:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi3:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi4:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi5:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_13:                               #   in Loop: Header=BB1_5 Depth=2
	incl	lineno(%rip)
.LBB1_5:                                # %.lr.ph
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_12 Depth 3
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB1_7
.LBB1_14:                               #   in Loop: Header=BB1_5 Depth=2
	movl	$.L.str.1, %edi
	callq	fatal
	movl	$-1, %eax
.LBB1_6:                                # %.backedge
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpl	$-1, %eax
	je	.LBB1_14
.LBB1_7:                                # %.backedge
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpl	$10, %eax
	je	.LBB1_13
# BB#8:                                 # %.backedge
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpl	$42, %eax
	jne	.LBB1_5
# BB#9:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB1_5 Depth=2
	movl	$42, %eax
	cmpl	$42, %eax
	jne	.LBB1_11
	.p2align	4, 0x90
.LBB1_12:                               #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$42, %eax
	je	.LBB1_12
.LBB1_11:                               # %.preheader
                                        #   in Loop: Header=BB1_5 Depth=2
	cmpl	$47, %eax
	je	.LBB1_1
	jmp	.LBB1_6
.LBB1_16:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rcx
	retq
.Lfunc_end1:
	.size	skip_white_space, .Lfunc_end1-skip_white_space
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_1
	.quad	.LBB1_15
	.quad	.LBB1_16
	.quad	.LBB1_1
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_1
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_16
	.quad	.LBB1_3

	.text
	.globl	unlex
	.p2align	4, 0x90
	.type	unlex,@function
unlex:                                  # @unlex
	.cfi_startproc
# BB#0:
	movl	%edi, unlexed(%rip)
	movq	symval(%rip), %rax
	movq	%rax, unlexed_symval(%rip)
	retq
.Lfunc_end2:
	.size	unlex, .Lfunc_end2-unlex
	.cfi_endproc

	.globl	lex
	.p2align	4, 0x90
	.type	lex,@function
lex:                                    # @lex
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -40
.Lcfi12:
	.cfi_offset %r12, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movl	unlexed(%rip), %eax
	testl	%eax, %eax
	js	.LBB3_2
# BB#1:
	movq	unlexed_symval(%rip), %rcx
	movq	%rcx, symval(%rip)
	movl	$-1, unlexed(%rip)
	jmp	.LBB3_12
.LBB3_2:
	callq	skip_white_space
	movl	%eax, %r15d
	leal	1(%r15), %ecx
	cmpl	$125, %ecx
	ja	.LBB3_25
# BB#3:
	xorl	%eax, %eax
	jmpq	*.LJTI3_0(,%rcx,8)
.LBB3_4:                                # %.preheader
	movl	$token_buffer, %ebx
	callq	__ctype_b_loc
	movq	%rax, %r14
	movl	$token_buffer+1024, %r12d
	jmp	.LBB3_6
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_6 Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %r15d
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movslq	%r15d, %rcx
	testb	$8, (%rax,%rcx,2)
	jne	.LBB3_9
# BB#7:                                 # %switch.early.test
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpl	$95, %r15d
	je	.LBB3_9
# BB#8:                                 # %switch.early.test
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpl	$46, %r15d
	jne	.LBB3_11
.LBB3_9:                                #   in Loop: Header=BB3_6 Depth=1
	cmpq	%r12, %rbx
	jae	.LBB3_5
# BB#10:                                #   in Loop: Header=BB3_6 Depth=1
	movb	%r15b, (%rbx)
	incq	%rbx
	jmp	.LBB3_5
.LBB3_11:
	movb	$0, (%rbx)
	movq	finput(%rip), %rsi
	movl	%r15d, %edi
	callq	ungetc
	movl	$token_buffer, %edi
	xorl	%eax, %eax
	callq	getsym
	movq	%rax, symval(%rip)
	movl	$1, %eax
.LBB3_12:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB3_13:                               #   in Loop: Header=BB3_14 Depth=1
	incl	lineno(%rip)
.LBB3_14:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	cmpl	$31, %eax
	jg	.LBB3_17
# BB#15:                                # %.critedge
                                        #   in Loop: Header=BB3_14 Depth=1
	cmpl	$10, %eax
	je	.LBB3_13
# BB#16:                                # %.critedge
                                        #   in Loop: Header=BB3_14 Depth=1
	cmpl	$9, %eax
	je	.LBB3_14
	jmp	.LBB3_46
.LBB3_17:                               # %.critedge
                                        #   in Loop: Header=BB3_14 Depth=1
	cmpl	$32, %eax
	je	.LBB3_14
# BB#18:                                # %.critedge
	cmpl	$123, %eax
	jne	.LBB3_46
.LBB3_19:                               # %.loopexit.loopexit
	movl	$6, %eax
	jmp	.LBB3_12
.LBB3_20:
	movl	$0, numval(%rip)
	callq	__ctype_b_loc
	movq	%rax, %r14
	movq	(%r14), %rax
	movslq	%r15d, %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB3_24
# BB#21:                                # %.lr.ph.preheader
	addl	$-48, %r15d
	movl	%r15d, numval(%rip)
	jmp	.LBB3_23
	.p2align	4, 0x90
.LBB3_22:                               # %.lr.ph..lr.ph_crit_edge
                                        #   in Loop: Header=BB3_23 Depth=1
	movl	numval(%rip), %eax
	leal	(%rax,%rax,4), %eax
	leal	-48(%r15,%rax,2), %eax
	movl	%eax, numval(%rip)
.LBB3_23:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %r15d
	movq	(%r14), %rax
	movslq	%r15d, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB3_22
.LBB3_24:                               # %._crit_edge
	movq	finput(%rip), %rsi
	movl	%r15d, %edi
	callq	ungetc
	movl	$22, %eax
	jmp	.LBB3_12
.LBB3_25:
	movl	$24, %eax
	jmp	.LBB3_12
.LBB3_26:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	parse_percent_token     # TAILCALL
.LBB3_27:
	movl	$-1, translations(%rip)
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$92, %r14d
	jne	.LBB3_60
# BB#28:
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpl	$48, %eax
	jne	.LBB3_47
# BB#29:                                # %.lr.ph112.preheader
	xorl	%r14d, %r14d
.LBB3_30:                               # %.lr.ph112
                                        # =>This Inner Loop Header: Depth=1
	leal	-48(%rbx,%r14,8), %r14d
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	andl	$-8, %eax
	cmpl	$48, %eax
	je	.LBB3_30
# BB#31:                                # %._crit_edge113
	cmpl	$128, %r14d
	jb	.LBB3_60
# BB#32:
	subq	$8, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.2, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%r14d, %esi
	pushq	$0
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset -32
	cmpl	$39, %ebx
	jne	.LBB3_61
	jmp	.LBB3_62
.LBB3_33:
	movl	$2, %eax
	jmp	.LBB3_12
.LBB3_34:
	movl	$3, %eax
	jmp	.LBB3_12
.LBB3_35:
	movl	$4, %eax
	jmp	.LBB3_12
.LBB3_36:                               # %.preheader107.preheader
	movl	$token_buffer, %r14d
	movl	$token_buffer+1023, %r15d
	jmp	.LBB3_38
.LBB3_37:                               #   in Loop: Header=BB3_38 Depth=1
	movb	%bl, (%r14)
	incq	%r14
.LBB3_38:                               # %.preheader107
                                        # =>This Inner Loop Header: Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB3_41
# BB#39:                                # %.preheader107
                                        #   in Loop: Header=BB3_38 Depth=1
	cmpl	$10, %ebx
	je	.LBB3_41
# BB#40:                                # %.preheader107
                                        #   in Loop: Header=BB3_38 Depth=1
	cmpl	$62, %ebx
	jne	.LBB3_42
	jmp	.LBB3_45
.LBB3_41:                               #   in Loop: Header=BB3_38 Depth=1
	movl	$.L.str.5, %edi
	callq	fatal
.LBB3_42:                               #   in Loop: Header=BB3_38 Depth=1
	cmpq	%r15, %r14
	jb	.LBB3_37
# BB#43:                                #   in Loop: Header=BB3_38 Depth=1
	subq	$8, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.6, %edi
	movl	$1023, %esi             # imm = 0x3FF
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB3_37
.LBB3_44:
	movl	$5, %eax
	jmp	.LBB3_12
.LBB3_45:
	movb	$0, (%r14)
	movl	$21, %eax
	jmp	.LBB3_12
.LBB3_46:
	movq	finput(%rip), %rsi
	movl	%eax, %edi
	callq	ungetc
	movl	$24, %eax
	jmp	.LBB3_12
.LBB3_47:
	leal	-92(%rbx), %eax
	cmpl	$24, %eax
	ja	.LBB3_50
# BB#48:
	movl	$9, %r14d
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_54:
	movl	$8, %r14d
	jmp	.LBB3_59
.LBB3_50:
	cmpl	$34, %ebx
	je	.LBB3_58
# BB#51:
	cmpl	$39, %ebx
	jne	.LBB3_53
.LBB3_58:
	movl	%ebx, %r14d
	jmp	.LBB3_59
.LBB3_53:
	subq	$8, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	xorl	%r14d, %r14d
	movl	$.L.str.3, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%ebx, %esi
	pushq	$0
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	callq	fatals
	addq	$32, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB3_59
.LBB3_55:
	movl	$12, %r14d
	jmp	.LBB3_59
.LBB3_56:
	movl	$10, %r14d
	jmp	.LBB3_59
.LBB3_57:
	movl	$13, %r14d
.LBB3_59:
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
.LBB3_60:
	cmpl	$39, %ebx
	je	.LBB3_62
.LBB3_61:
	movl	$.L.str.4, %edi
	callq	fatal
.LBB3_62:
	movb	$39, token_buffer(%rip)
	cmpl	$39, %r14d
	je	.LBB3_65
# BB#63:
	cmpl	$92, %r14d
	jne	.LBB3_66
# BB#64:
	movw	$23644, token_buffer+1(%rip) # imm = 0x5C5C
	movl	$token_buffer+3, %eax
	jmp	.LBB3_69
.LBB3_65:
	movw	$10076, token_buffer+1(%rip) # imm = 0x275C
	movl	$token_buffer+3, %eax
	jmp	.LBB3_69
.LBB3_66:
	cmpl	$32, %r14d
	jl	.LBB3_71
# BB#67:
	cmpl	$127, %r14d
	je	.LBB3_71
# BB#68:
	movb	%r14b, token_buffer+1(%rip)
	movl	$token_buffer+2, %eax
.LBB3_69:
	movw	$39, (%rax)
	movl	$token_buffer, %edi
	xorl	%eax, %eax
	callq	getsym
	movq	%rax, %rcx
	movq	%rcx, symval(%rip)
	movb	$1, 40(%rcx)
	movl	$1, %eax
	cmpw	$0, 38(%rcx)
	jne	.LBB3_12
# BB#70:
	movw	%r14w, 38(%rcx)
	jmp	.LBB3_12
.LBB3_71:
	leal	-8(%r14), %eax
	cmpl	$5, %eax
	ja	.LBB3_76
# BB#72:
	jmpq	*.LJTI3_2(,%rax,8)
.LBB3_73:
	movw	$25180, token_buffer+1(%rip) # imm = 0x625C
	movl	$token_buffer+3, %eax
	jmp	.LBB3_69
.LBB3_74:
	movw	$29788, token_buffer+1(%rip) # imm = 0x745C
	movl	$token_buffer+3, %eax
	jmp	.LBB3_69
.LBB3_75:
	movw	$28252, token_buffer+1(%rip) # imm = 0x6E5C
	movl	$token_buffer+3, %eax
	jmp	.LBB3_69
.LBB3_76:
	movl	%r14d, %eax
	sarl	$31, %eax
	movl	%eax, %ecx
	shrl	$26, %ecx
	addl	%r14d, %ecx
	shrl	$6, %ecx
	addl	$48, %ecx
	movb	%cl, token_buffer+1(%rip)
	shrl	$29, %eax
	addl	%r14d, %eax
	shrb	$3, %al
	andb	$7, %al
	orb	$48, %al
	movb	%al, token_buffer+2(%rip)
	movl	%r14d, %eax
	andb	$7, %al
	orb	$48, %al
	movb	%al, token_buffer+3(%rip)
	movl	$token_buffer+4, %eax
	jmp	.LBB3_69
.LBB3_77:
	movw	$26204, token_buffer+1(%rip) # imm = 0x665C
	movl	$token_buffer+3, %eax
	jmp	.LBB3_69
.LBB3_78:
	movw	$29276, token_buffer+1(%rip) # imm = 0x725C
	movl	$token_buffer+3, %eax
	jmp	.LBB3_69
.Lfunc_end3:
	.size	lex, .Lfunc_end3-lex
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_12
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_26
	.quad	.LBB3_25
	.quad	.LBB3_27
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_33
	.quad	.LBB3_25
	.quad	.LBB3_4
	.quad	.LBB3_25
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_20
	.quad	.LBB3_34
	.quad	.LBB3_35
	.quad	.LBB3_36
	.quad	.LBB3_14
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_25
	.quad	.LBB3_4
	.quad	.LBB3_25
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_4
	.quad	.LBB3_19
	.quad	.LBB3_44
.LJTI3_1:
	.quad	.LBB3_58
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_54
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_55
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_56
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_57
	.quad	.LBB3_53
	.quad	.LBB3_59
.LJTI3_2:
	.quad	.LBB3_73
	.quad	.LBB3_74
	.quad	.LBB3_75
	.quad	.LBB3_76
	.quad	.LBB3_77
	.quad	.LBB3_78

	.text
	.globl	parse_percent_token
	.p2align	4, 0x90
	.type	parse_percent_token,@function
parse_percent_token:                    # @parse_percent_token
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 48
.Lcfi35:
	.cfi_offset %rbx, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	leal	-37(%rbx), %eax
	cmpl	$25, %eax
	ja	.LBB4_3
# BB#1:
	movl	$7, %r14d
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_2:
	movl	$9, %r14d
	jmp	.LBB4_30
.LBB4_3:
	cmpl	$123, %ebx
	jne	.LBB4_5
# BB#4:
	movl	$8, %r14d
	jmp	.LBB4_30
.LBB4_5:
	callq	__ctype_b_loc
	movq	%rax, %r15
	movq	(%r15), %rax
	movslq	%ebx, %rcx
	movl	$24, %r14d
	testb	$4, 1(%rax,%rcx,2)
	je	.LBB4_30
# BB#6:                                 # %.lr.ph.preheader
	movl	$token_buffer, %ebp
	movl	$token_buffer+1024, %r14d
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r14, %rbp
	jae	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=1
	movb	%bl, (%rbp)
	incq	%rbp
.LBB4_9:                                #   in Loop: Header=BB4_7 Depth=1
	movq	finput(%rip), %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	(%r15), %rax
	movslq	%ebx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$1024, %eax             # imm = 0x400
	cmpl	$95, %ecx
	je	.LBB4_7
# BB#10:                                #   in Loop: Header=BB4_7 Depth=1
	testw	%ax, %ax
	jne	.LBB4_7
# BB#11:                                # %._crit_edge
	movq	finput(%rip), %rsi
	movl	%ebx, %edi
	callq	ungetc
	movb	$0, (%rbp)
	movl	$token_buffer, %edi
	movl	$.L.str.7, %esi
	callq	strcmp
	movl	$9, %r14d
	testl	%eax, %eax
	je	.LBB4_30
# BB#12:
	movl	$token_buffer, %edi
	movl	$.L.str.8, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_30
# BB#13:
	movl	$token_buffer, %edi
	movl	$.L.str.9, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_32
# BB#14:
	movl	$token_buffer, %edi
	movl	$.L.str.10, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_33
# BB#15:
	movl	$token_buffer, %edi
	movl	$.L.str.11, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_34
# BB#16:
	movl	$token_buffer, %edi
	movl	$.L.str.12, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_35
# BB#17:
	movl	$token_buffer, %edi
	movl	$.L.str.13, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_36
# BB#18:
	movl	$token_buffer, %edi
	movl	$.L.str.14, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_37
# BB#19:
	movl	$token_buffer, %edi
	movl	$.L.str.15, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_26
# BB#20:
	movl	$token_buffer, %edi
	movl	$.L.str.16, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_29
# BB#21:
	movl	$token_buffer, %edi
	movl	$.L.str.17, %esi
	callq	strcmp
	movl	$17, %r14d
	testl	%eax, %eax
	je	.LBB4_30
# BB#22:
	movl	$token_buffer, %edi
	movl	$.L.str.18, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_30
# BB#23:
	movl	$token_buffer, %edi
	movl	$.L.str.19, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_39
# BB#24:
	movl	$token_buffer, %edi
	movl	$.L.str.20, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB4_40
# BB#25:
	movl	$token_buffer, %edi
	movl	$.L.str.21, %esi
	callq	strcmp
	testl	%eax, %eax
	movl	$18, %ecx
	movl	$24, %eax
	cmovel	%ecx, %eax
	jmp	.LBB4_31
.LBB4_26:
	movl	$15, %r14d
	jmp	.LBB4_30
.LBB4_29:
	movl	$16, %r14d
	jmp	.LBB4_30
.LBB4_27:
	movl	$17, %r14d
	jmp	.LBB4_30
.LBB4_28:
	movl	$18, %r14d
.LBB4_30:
	movl	%r14d, %eax
.LBB4_31:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_32:
	movl	$10, %r14d
	jmp	.LBB4_30
.LBB4_33:
	movl	$12, %r14d
	jmp	.LBB4_30
.LBB4_34:
	movl	$11, %r14d
	jmp	.LBB4_30
.LBB4_35:
	movl	$13, %r14d
	jmp	.LBB4_30
.LBB4_36:
	movl	$23, %r14d
	jmp	.LBB4_30
.LBB4_37:
	movl	$14, %r14d
	jmp	.LBB4_30
.LBB4_39:
	movl	$19, %r14d
	jmp	.LBB4_30
.LBB4_40:
	movl	$20, %r14d
	jmp	.LBB4_30
.Lfunc_end4:
	.size	parse_percent_token, .Lfunc_end4-parse_percent_token
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_30
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_2
	.quad	.LBB4_5
	.quad	.LBB4_27
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_5
	.quad	.LBB4_26
	.quad	.LBB4_28
	.quad	.LBB4_29

	.type	unlexed,@object         # @unlexed
	.local	unlexed
	.comm	unlexed,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"unexpected '/%c' found"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"unterminated comment"
	.size	.L.str.1, 21

	.type	symval,@object          # @symval
	.comm	symval,8,8
	.type	unlexed_symval,@object  # @unlexed_symval
	.local	unlexed_symval
	.comm	unlexed_symval,8,8
	.type	token_buffer,@object    # @token_buffer
	.comm	token_buffer,1025,16
	.type	numval,@object          # @numval
	.comm	numval,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"malformatted literal token '\\%03o'"
	.size	.L.str.2, 35

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"invalid literal token '\\%c'"
	.size	.L.str.3, 28

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"multicharacter literal tokens NOT supported"
	.size	.L.str.4, 44

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"unterminated type name"
	.size	.L.str.5, 23

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"type name too long (%d max)"
	.size	.L.str.6, 28

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"token"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"term"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"nterm"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"type"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"guard"
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"union"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"expect"
	.size	.L.str.13, 7

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"start"
	.size	.L.str.14, 6

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"left"
	.size	.L.str.15, 5

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"right"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"nonassoc"
	.size	.L.str.17, 9

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"binary"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"semantic_parser"
	.size	.L.str.19, 16

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"pure_parser"
	.size	.L.str.20, 12

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"prec"
	.size	.L.str.21, 5


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
