	.text
	.file	"jdtrans.bc"
	.globl	jpeg_read_coefficients
	.p2align	4, 0x90
	.type	jpeg_read_coefficients,@function
jpeg_read_coefficients:                 # @jpeg_read_coefficients
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$202, %eax
	je	.LBB0_3
# BB#1:
	cmpl	$209, %eax
	je	.LBB0_16
# BB#2:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
	jmp	.LBB0_16
.LBB0_3:
	cmpl	$0, 308(%rbx)
	je	.LBB0_5
# BB#4:
	movq	(%rbx), %rax
	movl	$1, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	jmp	.LBB0_8
.LBB0_5:
	cmpl	$0, 304(%rbx)
	je	.LBB0_7
# BB#6:
	movq	%rbx, %rdi
	callq	jinit_phuff_decoder
	jmp	.LBB0_8
.LBB0_7:
	movq	%rbx, %rdi
	callq	jinit_huff_decoder
.LBB0_8:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	jinit_d_coef_controller
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	560(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_15
# BB#9:
	cmpl	$0, 304(%rbx)
	je	.LBB0_11
# BB#10:
	movl	48(%rbx), %ecx
	leal	2(%rcx,%rcx,2), %ecx
	jmp	.LBB0_14
.LBB0_11:
	movq	560(%rbx), %rcx
	cmpl	$0, 32(%rcx)
	je	.LBB0_12
# BB#13:
	movl	48(%rbx), %ecx
	jmp	.LBB0_14
.LBB0_12:
	movl	$1, %ecx
.LBB0_14:
	movq	$0, 8(%rax)
	movl	400(%rbx), %edx
	movslq	%ecx, %rcx
	imulq	%rdx, %rcx
	movq	%rcx, 16(%rax)
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	movq	%rcx, 24(%rax)
.LBB0_15:                               # %transdecode_master_selection.exit
	movl	$209, 28(%rbx)
.LBB0_16:                               # %.thread.preheader
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_18
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_19 Depth=1
	movl	400(%rbx), %edx
	addq	%rax, %rdx
	movq	%rdx, 16(%rcx)
.LBB0_17:                               # %.thread
                                        #   in Loop: Header=BB0_19 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_19
.LBB0_18:
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	movq	560(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	testl	%eax, %eax
	je	.LBB0_26
# BB#20:                                #   in Loop: Header=BB0_19 Depth=1
	cmpl	$2, %eax
	je	.LBB0_25
# BB#21:                                #   in Loop: Header=BB0_19 Depth=1
	movq	16(%rbx), %rcx
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB0_17
# BB#22:                                #   in Loop: Header=BB0_19 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_17
# BB#23:                                #   in Loop: Header=BB0_19 Depth=1
	movq	8(%rcx), %rdx
	incq	%rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rcx), %rax
	cmpq	%rax, %rdx
	jl	.LBB0_17
	jmp	.LBB0_24
.LBB0_26:                               # %.thread29.loopexit
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB0_25:
	movl	$210, 28(%rbx)
	movq	544(%rbx), %rax
	movq	32(%rax), %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	jpeg_read_coefficients, .Lfunc_end0-jpeg_read_coefficients
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
