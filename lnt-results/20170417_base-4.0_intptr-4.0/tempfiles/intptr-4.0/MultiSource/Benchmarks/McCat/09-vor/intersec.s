	.text
	.file	"intersec.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	midpoint
	.p2align	4, 0x90
	.type	midpoint,@function
midpoint:                               # @midpoint
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%edi, %xmm1
	shrq	$32, %rdi
	cvtsi2sdl	%esi, %xmm0
	shrq	$32, %rsi
	addsd	%xmm1, %xmm0
	movsd	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm0
	cvtsi2sdl	%edi, %xmm3
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	addsd	%xmm3, %xmm1
	mulsd	%xmm2, %xmm1
	retq
.Lfunc_end0:
	.size	midpoint, .Lfunc_end0-midpoint
	.cfi_endproc

	.globl	vector
	.p2align	4, 0x90
	.type	vector,@function
vector:                                 # @vector
	.cfi_startproc
# BB#0:
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	movl	%esi, %eax
	subl	%edi, %eax
	andq	%rcx, %rdi
	subq	%rdi, %rsi
	andq	%rsi, %rcx
	orq	%rcx, %rax
	retq
.Lfunc_end1:
	.size	vector, .Lfunc_end1-vector
	.cfi_endproc

	.globl	length2
	.p2align	4, 0x90
	.type	length2,@function
length2:                                # @length2
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	subl	%edi, %esi
	shrq	$32, %rdi
	shrq	$32, %rax
	imull	%esi, %esi
	subl	%edi, %eax
	imull	%eax, %eax
	addl	%eax, %esi
	movl	%esi, %eax
	retq
.Lfunc_end2:
	.size	length2, .Lfunc_end2-length2
	.cfi_endproc

	.globl	calculate_c
	.p2align	4, 0x90
	.type	calculate_c,@function
calculate_c:                            # @calculate_c
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%edi, %xmm2
	shrq	$32, %rdi
	mulsd	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	retq
.Lfunc_end3:
	.size	calculate_c, .Lfunc_end3-calculate_c
	.cfi_endproc

	.globl	intersect
	.p2align	4, 0x90
	.type	intersect,@function
intersect:                              # @intersect
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%edi, %xmm3
	shrq	$32, %rdi
	cvtsi2sdl	%esi, %xmm4
	shrq	$32, %rsi
	cvtsi2sdl	%esi, %xmm5
	movapd	%xmm5, %xmm2
	mulsd	%xmm0, %xmm2
	cvtsi2sdl	%edi, %xmm6
	movapd	%xmm6, %xmm7
	mulsd	%xmm1, %xmm7
	subsd	%xmm7, %xmm2
	mulsd	%xmm3, %xmm5
	mulsd	%xmm4, %xmm6
	subsd	%xmm6, %xmm5
	divsd	%xmm5, %xmm2
	mulsd	%xmm3, %xmm1
	mulsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm1
	divsd	%xmm5, %xmm1
	movapd	%xmm2, %xmm0
	retq
.Lfunc_end4:
	.size	intersect, .Lfunc_end4-intersect
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	centre
	.p2align	4, 0x90
	.type	centre,@function
centre:                                 # @centre
	.cfi_startproc
# BB#0:
	movabsq	$-4294967296, %r10      # imm = 0xFFFFFFFF00000000
	movl	%esi, %r8d
	subl	%edi, %r8d
	movq	%rdi, %r9
	cvtsi2sdl	%edi, %xmm0
	movq	%rdi, %r11
	andq	%r10, %r11
	andq	%rsi, %r10
	movq	%rdx, %rdi
	movq	%rdx, %rcx
	cvtsi2sdl	%edx, %xmm3
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	subl	%esi, %edx
	movq	%rsi, %rax
	cvtsi2sdl	%esi, %xmm1
	subq	%r11, %rsi
	subq	%r10, %rdi
	shrq	$32, %r9
	shrq	$32, %rax
	addsd	%xmm1, %xmm0
	movsd	.LCPI5_0(%rip), %xmm4   # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm0
	cvtsi2sdl	%r9d, %xmm2
	cvtsi2sdl	%eax, %xmm5
	addsd	%xmm5, %xmm2
	mulsd	%xmm4, %xmm2
	shrq	$32, %rsi
	cvtsi2sdl	%r8d, %xmm6
	mulsd	%xmm6, %xmm0
	cvtsi2sdl	%esi, %xmm7
	mulsd	%xmm7, %xmm2
	addsd	%xmm0, %xmm2
	shrq	$32, %rcx
	addsd	%xmm1, %xmm3
	mulsd	%xmm4, %xmm3
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	addsd	%xmm5, %xmm1
	mulsd	%xmm4, %xmm1
	shrq	$32, %rdi
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%edx, %xmm4
	mulsd	%xmm4, %xmm3
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	movapd	%xmm7, %xmm3
	mulsd	%xmm1, %xmm3
	mulsd	%xmm6, %xmm1
	mulsd	%xmm0, %xmm6
	mulsd	%xmm2, %xmm0
	subsd	%xmm3, %xmm0
	mulsd	%xmm4, %xmm7
	subsd	%xmm7, %xmm6
	divsd	%xmm6, %xmm0
	mulsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm1
	divsd	%xmm6, %xmm1
	retq
.Lfunc_end5:
	.size	centre, .Lfunc_end5-centre
	.cfi_endproc

	.globl	radius2
	.p2align	4, 0x90
	.type	radius2,@function
radius2:                                # @radius2
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%edi, %xmm2
	shrq	$32, %rdi
	subsd	%xmm0, %xmm2
	mulsd	%xmm2, %xmm2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	subsd	%xmm1, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	retq
.Lfunc_end6:
	.size	radius2, .Lfunc_end6-radius2
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
