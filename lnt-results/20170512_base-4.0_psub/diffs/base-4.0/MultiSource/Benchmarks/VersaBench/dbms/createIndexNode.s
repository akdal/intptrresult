	.text
	.file	"createIndexNode.bc"
	.globl	createIndexNode
	.p2align	4, 0x90
	.type	createIndexNode,@function
createIndexNode:                        # @createIndexNode
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	js	.LBB0_1
# BB#2:
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_3
# BB#5:
	movq	%rbx, (%r14)
	jmp	.LBB0_6
.LBB0_1:
	xorl	%r14d, %r14d
	movl	$.L.str, %edi
	jmp	.LBB0_4
.LBB0_3:
	movl	$.L.str.1, %edi
.LBB0_4:
	xorl	%esi, %esi
	callq	errorMessage
	movl	$createIndexNode.name, %edi
	movl	$1, %esi
	callq	errorMessage
.LBB0_6:
	movq	$0, 8(%r14)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	createIndexNode, .Lfunc_end0-createIndexNode
	.cfi_endproc

	.type	createIndexNode.name,@object # @createIndexNode.name
	.data
	.p2align	4
createIndexNode.name:
	.asciz	"createIndexNode"
	.size	createIndexNode.name, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"invalid level specified"
	.size	.L.str, 24

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"allocation failure"
	.size	.L.str.1, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
