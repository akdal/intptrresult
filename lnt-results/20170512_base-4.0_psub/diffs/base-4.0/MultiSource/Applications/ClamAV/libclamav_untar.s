	.text
	.file	"libclamav_untar.bc"
	.globl	cli_untar
	.p2align	4, 0x90
	.type	cli_untar,@function
cli_untar:                              # @cli_untar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$968, %rsp              # imm = 0x3C8
.Lcfi6:
	.cfi_def_cfa_offset 1024
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movl	%esi, %ebx
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	movl	$.L.str.1, %esi
	cmovneq	%rbp, %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	cli_dbgmsg
	leaq	192(%rsp), %rsi
	movl	$512, %edx              # imm = 0x200
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movl	%ebx, %edi
	callq	cli_readn
	testl	%eax, %eax
	je	.LBB0_44
# BB#1:                                 # %.lr.ph
	movq	%rbp, 48(%rsp)          # 8-byte Spill
                                        # implicit-def: %ECX
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_2 Depth=1
	testl	%r13d, %r13d
	je	.LBB0_7
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	cmpl	$513, %r12d             # imm = 0x201
	movl	$512, %ebp              # imm = 0x200
	cmovll	%r12d, %ebp
	movslq	%ebp, %rbx
	movl	$1, %esi
	leaq	192(%rsp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	fwrite
	movq	%rax, %rcx
	cmpl	%ebx, %ecx
	jne	.LBB0_51
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	subl	%ebp, %r12d
.LBB0_6:                                # %.thread160
                                        #   in Loop: Header=BB0_2 Depth=1
	testl	%r12d, %r12d
	cmovel	%r12d, %r13d
	jmp	.LBB0_43
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%r14, %r14
	je	.LBB0_10
# BB#8:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	%r14, %rdi
	callq	fclose
	testl	%eax, %eax
	je	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_2 Depth=1
	xorl	%r13d, %r13d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	leaq	704(%rsp), %rsi
	callq	cli_errmsg
	movl	$-123, 12(%rsp)         # 4-byte Folded Spill
	movl	$1, %ebx
	testl	%ebx, %ebx
	jne	.LBB0_42
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_2 Depth=1
	cmpb	$0, 192(%rsp)
	je	.LBB0_17
# BB#11:                                #   in Loop: Header=BB0_2 Depth=1
	testq	%r15, %r15
	je	.LBB0_14
# BB#12:                                #   in Loop: Header=BB0_2 Depth=1
	movl	4(%r15), %esi
	leal	-1(%rsi), %eax
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jae	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_2 Depth=1
	xorl	%r14d, %r14d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movl	$1, %ebx
	xorl	%r13d, %r13d
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	testl	%ebx, %ebx
	jne	.LBB0_42
	jmp	.LBB0_6
.LBB0_14:                               #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB0_19
# BB#15:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$5, %edx
	leaq	41(%rsp), %rbx
	movq	%rbx, %rdi
	leaq	449(%rsp), %rsi
	callq	strncpy
	movb	$0, 46(%rsp)
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_19
# BB#16:                                #   in Loop: Header=BB0_2 Depth=1
	xorl	%r14d, %r14d
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	leaq	41(%rsp), %rsi
	callq	cli_dbgmsg
	movl	$-124, 12(%rsp)         # 4-byte Folded Spill
	movl	$1, %ebx
	jmp	.LBB0_18
.LBB0_17:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%r14d, %r14d
	movl	$2, %ebx
.LBB0_18:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%r13d, %r13d
	testl	%ebx, %ebx
	jne	.LBB0_42
	jmp	.LBB0_6
.LBB0_19:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$3, %ebx
	movsbl	348(%rsp), %esi
	cmpl	$120, %esi
	ja	.LBB0_32
# BB#20:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$1, %eax
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmpq	*.LJTI0_0(,%rsi,8)
.LBB0_21:                               #   in Loop: Header=BB0_2 Depth=1
	incl	28(%rsp)                # 4-byte Folded Spill
	xorl	%eax, %eax
.LBB0_22:                               #   in Loop: Header=BB0_2 Depth=1
	movl	%eax, %r14d
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movl	$12, %edx
	leaq	67(%rsp), %rbp
	movq	%rbp, %rdi
	leaq	316(%rsp), %rsi
	callq	strncpy
	movb	$0, 79(%rsp)
	movl	$.L.str.19, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	leaq	80(%rsp), %rdx
	callq	sscanf
	cmpl	$1, %eax
	movl	80(%rsp), %r12d
	movl	$-1, %r15d
	cmovel	%r12d, %r15d
	testl	%r15d, %r15d
	js	.LBB0_29
# BB#23:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB0_27
# BB#24:                                #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.LBB0_27
# BB#25:                                #   in Loop: Header=BB0_2 Depth=1
	movl	%r15d, %ecx
	cmpq	%rax, %rcx
	jbe	.LBB0_27
# BB#26:                                # %.thread
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_28
.LBB0_27:                               #   in Loop: Header=BB0_2 Depth=1
	testl	%r14d, %r14d
	je	.LBB0_30
.LBB0_28:                               #   in Loop: Header=BB0_2 Depth=1
	movl	%r15d, %eax
	andl	$511, %eax              # imm = 0x1FF
	leal	512(%r15), %ecx
	subl	%eax, %ecx
	movl	%r15d, %eax
	andl	$511, %eax              # imm = 0x1FF
	movl	%ecx, %ebp
	cmovel	%r15d, %ebp
	testl	%r15d, %r15d
	cmovel	%ecx, %ebp
	xorl	%r14d, %r14d
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movslq	%ebp, %rsi
	movl	$1, %edx
	movl	32(%rsp), %edi          # 4-byte Reload
	callq	lseek
.LBB0_39:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB0_40
.LBB0_29:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%r14d, %r14d
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-124, 12(%rsp)         # 4-byte Folded Spill
	movl	$1, %ebx
	xorl	%r13d, %r13d
	movl	%r15d, %r12d
	jmp	.LBB0_40
.LBB0_30:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$100, %edx
	leaq	80(%rsp), %rbx
	movq	%rbx, %rdi
	leaq	192(%rsp), %rsi
	callq	strncpy
	movb	$0, 180(%rsp)
	movq	%rbx, %rdi
	callq	sanitiseName
	movl	$46, %esi
	movq	%rbx, %rdi
	callq	strrchr
	testq	%rax, %rax
	je	.LBB0_33
# BB#31:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%rax, %rdi
	callq	strlen
	cmpq	$4, %rax
	movl	$0, %ecx
	cmovbeq	%rax, %rcx
	jmp	.LBB0_34
.LBB0_32:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_warnmsg
	jmp	.LBB0_21
.LBB0_33:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%ecx, %ecx
.LBB0_34:                               #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	704(%rsp), %r13
	movl	$256, %r14d             # imm = 0x100
	subq	%rcx, %r14
	movl	$248, %ebx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	subl	%ecx, %ebx
	movq	%rbp, %rdi
	callq	strlen
	subl	%eax, %ebx
	movl	$.L.str.12, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rcx
	movl	%ebx, %r8d
	leaq	80(%rsp), %r9
	callq	snprintf
	movq	%r13, %rdi
	callq	mkstemp
	movl	%eax, %r15d
	testl	%r15d, %r15d
	js	.LBB0_38
# BB#35:                                #   in Loop: Header=BB0_2 Depth=1
	xorl	%ebx, %ebx
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_dbgmsg
	movl	$.L.str.16, %esi
	movl	%r15d, %edi
	callq	fdopen
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB0_37
# BB#36:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	cli_errmsg
	movl	%r15d, %edi
	callq	close
	movl	$-112, 12(%rsp)         # 4-byte Folded Spill
	movl	$1, %ebx
	xorl	%r14d, %r14d
.LBB0_37:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$1, %r13d
.LBB0_40:                               #   in Loop: Header=BB0_2 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_41:                               #   in Loop: Header=BB0_2 Depth=1
	testl	%ebx, %ebx
	je	.LBB0_6
.LBB0_42:                               #   in Loop: Header=BB0_2 Depth=1
	andb	$3, %bl
	cmpb	$3, %bl
	jne	.LBB0_55
.LBB0_43:                               # %.backedge
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$512, %edx              # imm = 0x200
	movl	32(%rsp), %edi          # 4-byte Reload
	leaq	192(%rsp), %rsi
	callq	cli_readn
	movl	%eax, %ecx
	orl	%r13d, %ecx
	jne	.LBB0_2
	jmp	.LBB0_45
.LBB0_38:                               #   in Loop: Header=BB0_2 Depth=1
	callq	__errno_location
	movl	(%rax), %edi
	callq	strerror
	movq	%rax, %rcx
	xorl	%r14d, %r14d
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%rcx, %rdx
	callq	cli_errmsg
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	$.L.str.14, %edi
	movl	$257, %edx              # imm = 0x101
	xorl	%eax, %eax
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	cli_dbgmsg
	movl	$-112, 12(%rsp)         # 4-byte Folded Spill
	movl	$1, %ebx
	jmp	.LBB0_39
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	testl	%eax, %eax
	jns	.LBB0_3
# BB#48:
	testq	%r14, %r14
	je	.LBB0_50
# BB#49:
	movq	%r14, %rdi
	callq	fclose
.LBB0_50:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB0_53
.LBB0_44:
	xorl	%r14d, %r14d
.LBB0_45:                               # %.loopexit164
	testq	%r14, %r14
	je	.LBB0_47
# BB#46:
	movq	%r14, %rdi
	callq	fclose
	jmp	.LBB0_54
.LBB0_47:
	xorl	%eax, %eax
	jmp	.LBB0_54
.LBB0_51:
	leaq	704(%rsp), %rdx
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	cli_errmsg
	testq	%r14, %r14
	je	.LBB0_53
# BB#52:
	movq	%r14, %rdi
	callq	fclose
.LBB0_53:                               # %.thread159
	movl	$-123, %eax
.LBB0_54:                               # %.loopexit
	addq	$968, %rsp              # imm = 0x3C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_55:
	cmpb	$2, %bl
	je	.LBB0_45
# BB#56:                                # %.loopexit.loopexit
	movl	12(%rsp), %eax          # 4-byte Reload
	jmp	.LBB0_54
.Lfunc_end0:
	.size	cli_untar, .Lfunc_end0-cli_untar
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_21
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_21
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_41
	.quad	.LBB0_21
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_22
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_22
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_22
	.quad	.LBB0_32
	.quad	.LBB0_22
	.quad	.LBB0_22
	.quad	.LBB0_21
	.quad	.LBB0_22
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_41
	.quad	.LBB0_32
	.quad	.LBB0_22
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_22
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_32
	.quad	.LBB0_22

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"In untar(%s, %d)\n"
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.zero	1
	.size	.L.str.1, 1

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"cli_untar: block read error\n"
	.size	.L.str.2, 29

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"cli_untar: cannot close file %s\n"
	.size	.L.str.3, 33

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"cli_untar: number of files exceeded %u\n"
	.size	.L.str.4, 40

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ustar"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Incorrect magic string '%s' in tar header\n"
	.size	.L.str.6, 43

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"cli_untar: unknown type flag %c\n"
	.size	.L.str.7, 33

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Invalid size in tar header\n"
	.size	.L.str.8, 28

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cli_untar: size = %d\n"
	.size	.L.str.9, 22

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cli_untar: size exceeded %d bytes\n"
	.size	.L.str.10, 35

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"cli_untar: skipping entry\n"
	.size	.L.str.11, 27

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%s/%.*sXXXXXX"
	.size	.L.str.12, 14

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Can't create temporary file %s: %s\n"
	.size	.L.str.13, 36

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%lu %lu %lu\n"
	.size	.L.str.14, 13

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"cli_untar: extracting %s\n"
	.size	.L.str.15, 26

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"wb"
	.size	.L.str.16, 3

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"cli_untar: cannot create file %s\n"
	.size	.L.str.17, 34

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"cli_untar: only wrote %d bytes to file %s (out of disc space?)\n"
	.size	.L.str.18, 64

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%o"
	.size	.L.str.19, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
