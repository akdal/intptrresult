	.text
	.file	"lvm.bc"
	.hidden	luaV_tonumber
	.globl	luaV_tonumber
	.p2align	4, 0x90
	.type	luaV_tonumber,@function
luaV_tonumber:                          # @luaV_tonumber
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	8(%rdi), %eax
	cmpl	$3, %eax
	je	.LBB0_1
# BB#2:
	cmpl	$4, %eax
	jne	.LBB0_5
# BB#3:
	movq	(%rdi), %rdi
	addq	$24, %rdi
	leaq	8(%rsp), %rsi
	callq	luaO_str2d
	testl	%eax, %eax
	je	.LBB0_5
# BB#4:
	movq	8(%rsp), %rax
	movq	%rax, (%rbx)
	movl	$3, 8(%rbx)
	jmp	.LBB0_6
.LBB0_5:
	xorl	%ebx, %ebx
	jmp	.LBB0_6
.LBB0_1:
	movq	%rdi, %rbx
.LBB0_6:
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	luaV_tonumber, .Lfunc_end0-luaV_tonumber
	.cfi_endproc

	.hidden	luaV_tostring
	.globl	luaV_tostring
	.p2align	4, 0x90
	.type	luaV_tostring,@function
luaV_tostring:                          # @luaV_tostring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorl	%eax, %eax
	cmpl	$3, 8(%rbx)
	jne	.LBB1_2
# BB#1:
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movq	%rsp, %r15
	movl	$.L.str, %esi
	movb	$1, %al
	movq	%r15, %rdi
	callq	sprintf
	movq	%r15, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rax, (%rbx)
	movl	$4, 8(%rbx)
	movl	$1, %eax
.LBB1_2:
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	luaV_tostring, .Lfunc_end1-luaV_tostring
	.cfi_endproc

	.hidden	luaV_gettable
	.globl	luaV_gettable
	.p2align	4, 0x90
	.type	luaV_gettable,@function
luaV_gettable:                          # @luaV_gettable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %r13
	movq	%rdi, %r12
	movl	8(%r13), %eax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$5, %eax
	jne	.LBB2_8
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	luaH_get
	movq	%rax, %rbp
	cmpl	$0, 8(%rbp)
	jne	.LBB2_7
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_7
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	testb	$1, 10(%rdi)
	jne	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	32(%r12), %rax
	movq	296(%rax), %rdx
	xorl	%esi, %esi
	callq	luaT_gettm
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_10
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	luaT_gettmbyobj
	movq	%rax, %rbx
	cmpl	$0, 8(%rbx)
	jne	.LBB2_10
# BB#9:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.1, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	luaG_typeerror
	.p2align	4, 0x90
.LBB2_10:                               # %.thread47
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	8(%rbx), %eax
	cmpl	$6, %eax
	je	.LBB2_11
# BB#1:                                 #   in Loop: Header=BB2_2 Depth=1
	incl	%r14d
	cmpl	$99, %r14d
	movq	%rbx, %r13
	jle	.LBB2_2
# BB#15:
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaG_runerror           # TAILCALL
.LBB2_7:                                # %.thread
	movq	(%rbp), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rax, (%rcx)
	movl	8(%rbp), %eax
	movl	%eax, 8(%rcx)
	jmp	.LBB2_14
.LBB2_11:
	movq	(%rsp), %rbp            # 8-byte Reload
	subq	64(%r12), %rbp
	movq	16(%r12), %rax
	movq	(%rbx), %rcx
	movq	%rcx, (%rax)
	movl	8(%rbx), %ecx
	movl	%ecx, 8(%rax)
	movq	16(%r12), %rax
	movq	(%r13), %rcx
	movq	%rcx, 16(%rax)
	movl	8(%r13), %ecx
	movl	%ecx, 24(%rax)
	movq	16(%r12), %rax
	movq	(%r15), %rcx
	movq	%rcx, 32(%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 40(%rax)
	movq	16(%r12), %rsi
	movq	56(%r12), %rax
	subq	%rsi, %rax
	cmpq	$48, %rax
	jg	.LBB2_13
# BB#12:
	movl	$3, %esi
	movq	%r12, %rdi
	callq	luaD_growstack
	movq	16(%r12), %rsi
.LBB2_13:                               # %callTMres.exit
	leaq	48(%rsi), %rax
	movq	%rax, 16(%r12)
	movl	$1, %edx
	movq	%r12, %rdi
	callq	luaD_call
	movq	16(%r12), %rax
	movq	64(%r12), %rcx
	leaq	-16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	-16(%rax), %rdx
	movq	%rdx, (%rcx,%rbp)
	movl	-8(%rax), %eax
	movl	%eax, 8(%rcx,%rbp)
.LBB2_14:                               # %.thread50
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	luaV_gettable, .Lfunc_end2-luaV_gettable
	.cfi_endproc

	.hidden	luaV_settable
	.globl	luaV_settable
	.p2align	4, 0x90
	.type	luaV_settable,@function
luaV_settable:                          # @luaV_settable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	8(%rbx), %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$5, %eax
	jne	.LBB3_11
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	luaH_set
	movq	%rax, %r14
	cmpl	$0, 8(%r14)
	jne	.LBB3_7
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB3_7
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	testb	$2, 10(%rdi)
	jne	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	32(%r12), %rax
	movq	304(%rax), %rdx
	movl	$1, %esi
	callq	luaT_gettm
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB3_13
	jmp	.LBB3_7
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	luaT_gettmbyobj
	movq	%rax, %rbp
	cmpl	$0, 8(%rbp)
	jne	.LBB3_13
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.1, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	luaG_typeerror
	.p2align	4, 0x90
.LBB3_13:                               #   in Loop: Header=BB3_2 Depth=1
	movl	8(%rbp), %eax
	cmpl	$6, %eax
	je	.LBB3_14
# BB#1:                                 #   in Loop: Header=BB3_2 Depth=1
	incl	%r15d
	cmpl	$99, %r15d
	movq	%rbp, %rbx
	jle	.LBB3_2
# BB#17:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaG_runerror           # TAILCALL
.LBB3_7:                                # %.thread
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx), %rax
	movq	%rax, (%r14)
	movl	8(%rcx), %eax
	movl	%eax, 8(%r14)
	cmpl	$4, 8(%rcx)
	jl	.LBB3_18
# BB#8:
	movq	(%rcx), %rax
	testb	$3, 9(%rax)
	je	.LBB3_18
# BB#9:
	testb	$4, 9(%r13)
	jne	.LBB3_10
.LBB3_18:                               # %.thread57
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_14:
	movq	16(%r12), %rax
	movq	(%rbp), %rcx
	movq	%rcx, (%rax)
	movl	8(%rbp), %ecx
	movl	%ecx, 8(%rax)
	movq	16(%r12), %rax
	movq	(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movl	8(%rbx), %ecx
	movl	%ecx, 24(%rax)
	movq	16(%r12), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	8(%rdx), %ecx
	movl	%ecx, 40(%rax)
	movq	16(%r12), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, 48(%rax)
	movl	8(%rdx), %ecx
	movl	%ecx, 56(%rax)
	movq	16(%r12), %rsi
	movq	56(%r12), %rax
	subq	%rsi, %rax
	cmpq	$64, %rax
	jg	.LBB3_16
# BB#15:
	movl	$4, %esi
	movq	%r12, %rdi
	callq	luaD_growstack
	movq	16(%r12), %rsi
.LBB3_16:                               # %callTM.exit
	leaq	64(%rsi), %rax
	movq	%rax, 16(%r12)
	xorl	%edx, %edx
	movq	%r12, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaD_call               # TAILCALL
.LBB3_10:
	movq	%r12, %rdi
	movq	%r13, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaC_barrierback        # TAILCALL
.Lfunc_end3:
	.size	luaV_settable, .Lfunc_end3-luaV_settable
	.cfi_endproc

	.hidden	luaV_lessthan
	.globl	luaV_lessthan
	.p2align	4, 0x90
	.type	luaV_lessthan,@function
luaV_lessthan:                          # @luaV_lessthan
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 48
.Lcfi41:
	.cfi_offset %rbx, -48
.Lcfi42:
	.cfi_offset %r12, -40
.Lcfi43:
	.cfi_offset %r13, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	8(%rbx), %eax
	cmpl	8(%r15), %eax
	jne	.LBB4_14
# BB#1:
	cmpl	$4, %eax
	je	.LBB4_4
# BB#2:
	cmpl	$3, %eax
	jne	.LBB4_7
# BB#3:
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	xorl	%eax, %eax
	ucomisd	(%rbx), %xmm0
	seta	%al
	jmp	.LBB4_6
.LBB4_4:
	movq	(%rbx), %r13
	movq	(%r15), %rbx
	leaq	24(%r13), %r15
	leaq	24(%rbx), %r14
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	strcoll
	testl	%eax, %eax
	je	.LBB4_15
.LBB4_5:                                # %l_strcmp.exit
	shrl	$31, %eax
.LBB4_6:                                # %call_orderTM.exit.thread19
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB4_7:
	movl	$13, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaT_gettmbyobj
	movq	%rax, %r12
	cmpl	$0, 8(%r12)
	je	.LBB4_14
# BB#8:
	movl	$13, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	luaT_gettmbyobj
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	luaO_rawequalObj
	testl	%eax, %eax
	je	.LBB4_14
# BB#9:
	movq	16(%r14), %rax
	movq	%rax, %r13
	subq	64(%r14), %r13
	movq	(%r12), %rcx
	movq	%rcx, (%rax)
	movl	8(%r12), %ecx
	movl	%ecx, 8(%rax)
	movq	16(%r14), %rax
	movq	(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movl	8(%rbx), %ecx
	movl	%ecx, 24(%rax)
	movq	16(%r14), %rax
	movq	(%r15), %rcx
	movq	%rcx, 32(%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 40(%rax)
	movq	16(%r14), %rsi
	movq	56(%r14), %rax
	subq	%rsi, %rax
	cmpq	$48, %rax
	jg	.LBB4_11
# BB#10:
	movl	$3, %esi
	movq	%r14, %rdi
	callq	luaD_growstack
	movq	16(%r14), %rsi
.LBB4_11:                               # %callTMres.exit
	leaq	48(%rsi), %rax
	movq	%rax, 16(%r14)
	movl	$1, %edx
	movq	%r14, %rdi
	callq	luaD_call
	movq	16(%r14), %rax
	movq	64(%r14), %rcx
	leaq	-16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	-16(%rax), %rdx
	movq	%rdx, (%rcx,%r13)
	movl	-8(%rax), %eax
	movl	%eax, 8(%rcx,%r13)
	movq	16(%r14), %rcx
	movl	8(%rcx), %eax
	testl	%eax, %eax
	je	.LBB4_19
# BB#12:                                # %callTMres.exit
	cmpl	$1, %eax
	jne	.LBB4_20
# BB#13:                                # %call_orderTM.exit
	xorl	%eax, %eax
	cmpl	$0, (%rcx)
	setne	%al
	jmp	.LBB4_6
.LBB4_14:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	luaG_ordererror         # TAILCALL
.LBB4_15:                               # %.lr.ph.preheader.i
	movq	16(%rbx), %r12
	movq	16(%r13), %rbx
	.p2align	4, 0x90
.LBB4_16:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	strlen
	cmpq	%r12, %rax
	je	.LBB4_21
# BB#17:                                #   in Loop: Header=BB4_16 Depth=1
	cmpq	%rbx, %rax
	je	.LBB4_22
# BB#18:                                #   in Loop: Header=BB4_16 Depth=1
	leaq	1(%rax), %rcx
	leaq	1(%r15,%rax), %r15
	subq	%rcx, %rbx
	leaq	1(%r14,%rax), %r14
	subq	%rcx, %r12
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	strcoll
	testl	%eax, %eax
	je	.LBB4_16
	jmp	.LBB4_5
.LBB4_19:
	xorl	%eax, %eax
	jmp	.LBB4_6
.LBB4_20:                               # %.fold.split.i
	movl	$1, %eax
	jmp	.LBB4_6
.LBB4_21:
	xorl	%eax, %eax
	jmp	.LBB4_5
.LBB4_22:
	movl	$-1, %eax
	jmp	.LBB4_5
.Lfunc_end4:
	.size	luaV_lessthan, .Lfunc_end4-luaV_lessthan
	.cfi_endproc

	.hidden	luaV_equalval
	.globl	luaV_equalval
	.p2align	4, 0x90
	.type	luaV_equalval,@function
luaV_equalval:                          # @luaV_equalval
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 64
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	8(%rbx), %eax
	cmpq	$7, %rax
	ja	.LBB5_16
# BB#1:
	movb	$1, %r12b
	jmpq	*.LJTI5_0(,%rax,8)
.LBB5_2:
	movq	(%rbx), %rcx
	movq	(%r15), %rax
	cmpq	%rax, %rcx
	je	.LBB5_27
# BB#3:
	addq	$16, %rcx
	addq	$16, %rax
	movq	(%rcx), %rbp
	testq	%rbp, %rbp
	je	.LBB5_26
# BB#4:
	testb	$16, 10(%rbp)
	jne	.LBB5_26
# BB#5:
	movq	(%rax), %r13
	movq	32(%r14), %rax
	movq	328(%rax), %rdx
	movl	$4, %esi
	movq	%rbp, %rdi
	callq	luaT_gettm
	movq	%rax, %rdx
	testq	%rdx, %rdx
	je	.LBB5_26
# BB#6:
	cmpq	%r13, %rbp
	je	.LBB5_11
# BB#7:
	testq	%r13, %r13
	je	.LBB5_26
# BB#8:
	testb	$16, 10(%r13)
	jne	.LBB5_26
# BB#9:
	movq	%rdx, %rbp
	movq	32(%r14), %rax
	movq	328(%rax), %rdx
	movl	$4, %esi
	movq	%r13, %rdi
	callq	luaT_gettm
	testq	%rax, %rax
	je	.LBB5_26
# BB#10:
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	luaO_rawequalObj
	movq	%rbp, %rdx
	testl	%eax, %eax
	je	.LBB5_26
.LBB5_11:                               # %get_compTM.exit
	movq	16(%r14), %rax
	movq	%rax, %rbp
	subq	64(%r14), %rbp
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	8(%rdx), %ecx
	movl	%ecx, 8(%rax)
	movq	16(%r14), %rax
	movq	(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movl	8(%rbx), %ecx
	movl	%ecx, 24(%rax)
	movq	16(%r14), %rax
	movq	(%r15), %rcx
	movq	%rcx, 32(%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 40(%rax)
	movq	16(%r14), %rsi
	movq	56(%r14), %rax
	subq	%rsi, %rax
	cmpq	$48, %rax
	jg	.LBB5_13
# BB#12:
	movl	$3, %esi
	movq	%r14, %rdi
	callq	luaD_growstack
	movq	16(%r14), %rsi
.LBB5_13:                               # %callTMres.exit
	leaq	48(%rsi), %rax
	movq	%rax, 16(%r14)
	movl	$1, %edx
	movq	%r14, %rdi
	callq	luaD_call
	movq	16(%r14), %rax
	movq	64(%r14), %rcx
	leaq	-16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	-16(%rax), %rdx
	movq	%rdx, (%rcx,%rbp)
	movl	-8(%rax), %eax
	movl	%eax, 8(%rcx,%rbp)
	movq	16(%r14), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB5_26
# BB#14:                                # %callTMres.exit
	cmpl	$1, %ecx
	jne	.LBB5_27
# BB#15:
	cmpl	$0, (%rax)
	setne	%r12b
	jmp	.LBB5_27
.LBB5_16:
	movq	(%rbx), %rax
	cmpq	(%r15), %rax
	sete	%r12b
	jmp	.LBB5_27
.LBB5_26:
	xorl	%r12d, %r12d
	jmp	.LBB5_27
.LBB5_17:
	movl	(%rbx), %eax
	cmpl	(%r15), %eax
	sete	%r12b
	jmp	.LBB5_27
.LBB5_18:
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	cmpeqsd	(%r15), %xmm0
	movd	%xmm0, %r12
	andl	$1, %r12d
.LBB5_27:                               # %get_compTM.exit.thread
	movzbl	%r12b, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	luaV_equalval, .Lfunc_end5-luaV_equalval
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_27
	.quad	.LBB5_17
	.quad	.LBB5_16
	.quad	.LBB5_18
	.quad	.LBB5_16
	.quad	.LBB5_2
	.quad	.LBB5_16
	.quad	.LBB5_2

	.text
	.hidden	luaV_concat
	.globl	luaV_concat
	.p2align	4, 0x90
	.type	luaV_concat,@function
luaV_concat:                            # @luaV_concat
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 160
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rdi, %rbp
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
                                        #     Child Loop BB6_26 Depth 2
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	24(%rbp), %rcx
	movslq	%r13d, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	shlq	$4, %rax
	leaq	(%rcx,%rax), %rbx
	leaq	16(%rcx,%rax), %r14
	leaq	-16(%rcx,%rax), %r12
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	-8(%rcx,%rax), %eax
	addl	$-3, %eax
	cmpl	$1, %eax
	ja	.LBB6_16
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	movl	8(%rbx), %eax
	cmpl	$4, %eax
	je	.LBB6_5
# BB#3:                                 #   in Loop: Header=BB6_1 Depth=1
	cmpl	$3, %eax
	jne	.LBB6_16
# BB#4:                                 # %luaV_tostring.exit
                                        #   in Loop: Header=BB6_1 Depth=1
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str, %esi
	movb	$1, %al
	movq	%r14, %r15
	leaq	64(%rsp), %r14
	movq	%r14, %rdi
	callq	sprintf
	movq	%r14, %rdi
	callq	strlen
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r15, %r14
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rax, (%rbx)
	movl	$4, 8(%rbx)
.LBB6_5:                                #   in Loop: Header=BB6_1 Depth=1
	movq	(%rbx), %rax
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB6_23
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	$1, %r14d
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	$2, %eax
	jl	.LBB6_14
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	movslq	%eax, %r15
	movl	$1, %r14d
	movq	%r15, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%r12), %eax
	cmpl	$4, %eax
	je	.LBB6_11
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_8 Depth=2
	cmpl	$3, %eax
	jne	.LBB6_14
# BB#10:                                # %luaV_tostring.exit85
                                        #   in Loop: Header=BB6_8 Depth=2
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str, %esi
	movb	$1, %al
	movl	%r13d, %r15d
	leaq	64(%rsp), %r13
	movq	%r13, %rdi
	callq	sprintf
	movq	%r13, %rdi
	callq	strlen
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	%r15d, %r13d
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rax, (%r12)
	movl	$4, 8(%r12)
.LBB6_11:                               # %.critedge1
                                        #   in Loop: Header=BB6_8 Depth=2
	movq	(%r12), %rax
	movq	16(%rax), %rbp
	movq	$-3, %rax
	subq	%rbx, %rax
	cmpq	%rax, %rbp
	jb	.LBB6_13
# BB#12:                                #   in Loop: Header=BB6_8 Depth=2
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	luaG_runerror
.LBB6_13:                               #   in Loop: Header=BB6_8 Depth=2
	addq	%rbp, %rbx
	incq	%r14
	addq	$-16, %r12
	cmpq	%r15, %r14
	movq	24(%rsp), %rbp          # 8-byte Reload
	jl	.LBB6_8
.LBB6_14:                               # %.critedge
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	movq	32(%rbp), %rsi
	addq	$88, %rsi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	luaZ_openspace
	movq	%rax, %rbx
	movslq	%r14d, %rax
	testl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jle	.LBB6_15
# BB#25:                                # %.lr.ph94.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	leaq	1(%rax), %r12
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	%rax, %rcx
	shlq	$4, %rcx
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax,%rcx), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_26:                               # %.lr.ph94
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rsi
	movq	16(%rsi), %r15
	leaq	(%rbx,%rbp), %rdi
	addq	$24, %rsi
	movq	%r15, %rdx
	callq	memcpy
	addq	%r15, %rbp
	decq	%r12
	addq	$16, %r13
	cmpq	$1, %r12
	jg	.LBB6_26
	jmp	.LBB6_27
	.p2align	4, 0x90
.LBB6_16:                               # %luaV_tostring.exit.thread
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	$15, %edx
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	luaT_gettmbyobj
	cmpl	$0, 8(%rax)
	jne	.LBB6_18
# BB#17:                                #   in Loop: Header=BB6_1 Depth=1
	movl	$15, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	luaT_gettmbyobj
	cmpl	$0, 8(%rax)
	je	.LBB6_21
.LBB6_18:                               # %.thread.i
                                        #   in Loop: Header=BB6_1 Depth=1
	subq	64(%rbp), %r12
	movq	16(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rcx)
	movq	16(%rbp), %rax
	movq	-32(%r14), %rcx
	movq	%rcx, 16(%rax)
	movl	-24(%r14), %ecx
	movl	%ecx, 24(%rax)
	movq	16(%rbp), %rax
	movq	-16(%r14), %rcx
	movq	%rcx, 32(%rax)
	movl	-8(%r14), %ecx
	movl	%ecx, 40(%rax)
	movq	16(%rbp), %rsi
	movq	56(%rbp), %rax
	subq	%rsi, %rax
	cmpq	$48, %rax
	jg	.LBB6_20
# BB#19:                                #   in Loop: Header=BB6_1 Depth=1
	movl	$3, %esi
	movq	%rbp, %rdi
	callq	luaD_growstack
	movq	16(%rbp), %rsi
.LBB6_20:                               # %call_binTM.exit
                                        #   in Loop: Header=BB6_1 Depth=1
	leaq	48(%rsi), %rax
	movq	%rax, 16(%rbp)
	movl	$1, %edx
	movq	%rbp, %rdi
	callq	luaD_call
	movq	16(%rbp), %rax
	movq	64(%rbp), %rcx
	leaq	-16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	-16(%rax), %rdx
	movq	%rdx, (%rcx,%r12)
	movl	-8(%rax), %eax
	movl	%eax, 8(%rcx,%r12)
	movl	$2, %r14d
	jmp	.LBB6_28
.LBB6_23:                               #   in Loop: Header=BB6_1 Depth=1
	movl	$2, %r14d
	cmpl	$3, 8(%r12)
	jne	.LBB6_28
# BB#24:                                #   in Loop: Header=BB6_1 Depth=1
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str, %esi
	movb	$1, %al
	leaq	64(%rsp), %rbx
	movq	%rbx, %rdi
	callq	sprintf
	movq	%rbx, %rdi
	callq	strlen
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rax, (%r12)
	movl	$4, 8(%r12)
	jmp	.LBB6_28
.LBB6_21:                               #   in Loop: Header=BB6_1 Depth=1
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	luaG_concaterror
	movl	$2, %r14d
	jmp	.LBB6_28
.LBB6_15:                               #   in Loop: Header=BB6_1 Depth=1
	xorl	%ebp, %ebp
.LBB6_27:                               # %._crit_edge
                                        #   in Loop: Header=BB6_1 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	shlq	$4, %rax
	movq	40(%rsp), %r15          # 8-byte Reload
	subq	%rax, %r15
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%rdi, %rbp
	callq	luaS_newlstr
	movq	%rax, (%r15)
	movl	$4, 8(%r15)
	movl	16(%rsp), %r13d         # 4-byte Reload
.LBB6_28:                               # %luaV_tostring.exit83
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	12(%rsp), %esi          # 4-byte Reload
	decl	%r14d
	subl	%r14d, %esi
	subl	%r14d, %r13d
	cmpl	$1, %esi
	jg	.LBB6_1
# BB#29:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	luaV_concat, .Lfunc_end6-luaV_concat
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI7_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
.LCPI7_2:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.hidden	luaV_execute
	.globl	luaV_execute
	.p2align	4, 0x90
	.type	luaV_execute,@function
luaV_execute:                           # @luaV_execute
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 128
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movl	%esi, 60(%rsp)          # 4-byte Spill
	movq	%rdi, (%rsp)            # 8-byte Spill
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_1:                                #   in Loop: Header=BB7_2 Depth=1
	movq	40(%rdi), %rax
	movq	16(%rax), %rax
	movq	%rax, 16(%rdi)
.LBB7_2:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
                                        #       Child Loop BB7_8 Depth 3
                                        #         Child Loop BB7_171 Depth 4
                                        #         Child Loop BB7_138 Depth 4
                                        #         Child Loop BB7_128 Depth 4
                                        #         Child Loop BB7_219 Depth 4
                                        #         Child Loop BB7_31 Depth 4
                                        #       Child Loop BB7_237 Depth 3
	movq	40(%rdi), %rax
	movq	48(%rdi), %r10
	jmp	.LBB7_4
.LBB7_3:                                # %._crit_edge1135
                                        #   in Loop: Header=BB7_4 Depth=2
	movq	%rbp, 16(%rdi)
	movq	%rbp, -24(%r14)
	movq	48(%rdi), %r10
	movq	%r10, -16(%r14)
	incl	-4(%r14)
	movq	40(%rdi), %rax
	addq	$-40, %rax
	movq	%rax, 40(%rdi)
	.p2align	4, 0x90
.LBB7_4:                                #   Parent Loop BB7_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_8 Depth 3
                                        #         Child Loop BB7_171 Depth 4
                                        #         Child Loop BB7_138 Depth 4
                                        #         Child Loop BB7_128 Depth 4
                                        #         Child Loop BB7_219 Depth 4
                                        #         Child Loop BB7_31 Depth 4
                                        #       Child Loop BB7_237 Depth 3
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	24(%rdi), %r11
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	32(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_7:                                # %.thread1078
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	24(%rdi), %r11
	jmp	.LBB7_8
.LBB7_5:                                #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	$11, %r8d
	movq	%r12, %rsi
	movq	%rdx, %rcx
.LBB7_6:                                # %.thread1078
                                        #   in Loop: Header=BB7_8 Depth=3
	callq	Arith
	jmp	.LBB7_7
	.p2align	4, 0x90
.LBB7_8:                                # %.thread1078
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_171 Depth 4
                                        #         Child Loop BB7_138 Depth 4
                                        #         Child Loop BB7_128 Depth 4
                                        #         Child Loop BB7_219 Depth 4
                                        #         Child Loop BB7_31 Depth 4
	movq	%r10, %r15
	leaq	4(%r15), %r10
	movl	(%r15), %ebx
	movzbl	100(%rdi), %ebp
	testb	$12, %bpl
	movq	%r10, 8(%rsp)           # 8-byte Spill
	je	.LBB7_25
# BB#9:                                 #   in Loop: Header=BB7_8 Depth=3
	movl	108(%rdi), %eax
	decl	%eax
	movl	%eax, 108(%rdi)
	setne	%cl
	testb	$4, %bpl
	jne	.LBB7_11
# BB#10:                                #   in Loop: Header=BB7_8 Depth=3
	testb	%cl, %cl
	jne	.LBB7_25
.LBB7_11:                               #   in Loop: Header=BB7_8 Depth=3
	movq	48(%rdi), %r14
	movq	%r10, 48(%rdi)
	testl	%eax, %eax
	setne	%al
	testb	$8, %bpl
	je	.LBB7_14
# BB#12:                                #   in Loop: Header=BB7_8 Depth=3
	testb	%al, %al
	jne	.LBB7_14
# BB#13:                                #   in Loop: Header=BB7_8 Depth=3
	movl	104(%rdi), %eax
	movl	%eax, 108(%rdi)
	movl	$3, %esi
	movl	$-1, %edx
	callq	luaD_callhook
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_14:                               #   in Loop: Header=BB7_8 Depth=3
	testb	$4, %bpl
	je	.LBB7_23
# BB#15:                                #   in Loop: Header=BB7_8 Depth=3
	movq	40(%rdi), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rax
	movq	32(%rax), %rax
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%r10, %rsi
	subq	%rcx, %rsi
	shrq	$2, %rsi
	decl	%esi
	testq	%rax, %rax
	je	.LBB7_17
# BB#16:                                #   in Loop: Header=BB7_8 Depth=3
	movslq	%esi, %rdx
	movl	(%rax,%rdx,4), %edx
	cmpq	%r10, %r14
	jb	.LBB7_18
	jmp	.LBB7_22
.LBB7_17:                               #   in Loop: Header=BB7_8 Depth=3
	xorl	%edx, %edx
	cmpq	%r10, %r14
	jae	.LBB7_22
.LBB7_18:                               #   in Loop: Header=BB7_8 Depth=3
	testl	%esi, %esi
	je	.LBB7_22
# BB#19:                                #   in Loop: Header=BB7_8 Depth=3
	testq	%rax, %rax
	je	.LBB7_21
# BB#20:                                #   in Loop: Header=BB7_8 Depth=3
	subq	%rcx, %r14
	shlq	$30, %r14
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rcx, %r14
	sarq	$30, %r14
	movl	(%rax,%r14), %eax
	cmpl	%eax, %edx
	jne	.LBB7_22
	jmp	.LBB7_23
.LBB7_21:                               #   in Loop: Header=BB7_8 Depth=3
	xorl	%eax, %eax
	cmpl	%eax, %edx
	je	.LBB7_23
.LBB7_22:                               #   in Loop: Header=BB7_8 Depth=3
	movl	$2, %esi
	callq	luaD_callhook
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_23:                               # %traceexec.exit
                                        #   in Loop: Header=BB7_8 Depth=3
	cmpb	$1, 10(%rdi)
	je	.LBB7_246
# BB#24:                                #   in Loop: Header=BB7_8 Depth=3
	movq	24(%rdi), %r11
.LBB7_25:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	andb	$63, %al
	cmpb	$37, %al
	ja	.LBB7_8
# BB#26:                                #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$6, %eax
	movzbl	%al, %r14d
	movq	%r14, %r13
	shlq	$4, %r13
	leaq	(%r11,%r13), %r12
	movl	%ebx, %eax
	andl	$63, %eax
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_27:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$23, %ebx
	shlq	$4, %rbx
	movq	(%r11,%rbx), %rax
	movq	%rax, (%r12)
	movl	8(%r11,%rbx), %eax
	movl	%eax, 8(%r11,%r13)
	jmp	.LBB7_8
.LBB7_28:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$14, %ebx
	shlq	$4, %rbx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rbx), %rax
	movq	%rax, (%r12)
	movl	8(%rcx,%rbx), %eax
	movl	%eax, 8(%r11,%r13)
	jmp	.LBB7_8
.LBB7_29:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	movl	%eax, (%r12)
	movl	$1, 8(%r11,%r13)
	addq	$8, %r15
	testl	$8372224, %ebx          # imm = 0x7FC000
	cmovneq	%r15, %r10
	jmp	.LBB7_8
.LBB7_30:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$23, %ebx
	shlq	$4, %rbx
	addq	%r11, %rbx
	.p2align	4, 0x90
.LBB7_31:                               #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	$0, 8(%rbx)
	addq	$-16, %rbx
	cmpq	%r12, %rbx
	jae	.LBB7_31
	jmp	.LBB7_8
.LBB7_32:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$23, %ebx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	40(%rax,%rbx,8), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	%rcx, (%r12)
	movl	8(%rax), %eax
	movl	%eax, 8(%r11,%r13)
	jmp	.LBB7_8
.LBB7_33:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$14, %ebx
	shlq	$4, %rbx
	addq	16(%rsp), %rbx          # 8-byte Folded Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	%rax, 40(%rsp)
	movl	$5, 48(%rsp)
	movq	%r10, 48(%rdi)
	leaq	40(%rsp), %rsi
	movq	%rbx, %rdx
	jmp	.LBB7_47
.LBB7_34:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	%ebx, %esi
	shrl	$23, %esi
	shlq	$4, %rsi
	addq	%r11, %rsi
	jmp	.LBB7_46
.LBB7_35:                               #   in Loop: Header=BB7_8 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	%rax, 40(%rsp)
	movl	$5, 48(%rsp)
	movq	%r10, 48(%rdi)
	shrl	$14, %ebx
	shlq	$4, %rbx
	addq	16(%rsp), %rbx          # 8-byte Folded Reload
	leaq	40(%rsp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	jmp	.LBB7_41
.LBB7_36:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$23, %ebx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	40(%rax,%rbx,8), %rsi
	movq	16(%rsi), %rax
	movq	(%r12), %rcx
	movq	%rcx, (%rax)
	movl	8(%r11,%r13), %ecx
	movl	%ecx, 8(%rax)
	cmpl	$4, 8(%r11,%r13)
	jl	.LBB7_8
# BB#37:                                #   in Loop: Header=BB7_8 Depth=3
	movq	(%r12), %rdx
	testb	$3, 9(%rdx)
	je	.LBB7_8
# BB#38:                                #   in Loop: Header=BB7_8 Depth=3
	testb	$4, 9(%rsi)
	je	.LBB7_8
# BB#39:                                #   in Loop: Header=BB7_8 Depth=3
	movq	%r11, %rbx
	callq	luaC_barrierf
	jmp	.LBB7_135
.LBB7_40:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	%ebx, %eax
	shrl	$23, %eax
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	movzbl	%al, %edx
	cmovel	%eax, %edx
	testl	%ecx, %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %rax
	cmoveq	%r11, %rax
	shlq	$4, %rdx
	addq	%rax, %rdx
	shrl	$14, %ebx
	movl	%ebx, %ecx
	andl	$256, %ecx              # imm = 0x100
	cmovneq	%rsi, %r11
	xorl	$511, %ecx              # imm = 0x1FF
	andl	%ebx, %ecx
	shlq	$4, %rcx
	addq	%r11, %rcx
	movq	%r12, %rsi
.LBB7_41:                               # %.thread1078
                                        #   in Loop: Header=BB7_8 Depth=3
	callq	luaV_settable
	jmp	.LBB7_7
.LBB7_42:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %edi
	shrl	$23, %edi
	shrl	$14, %ebx
	andl	$511, %ebx              # imm = 0x1FF
	movq	%r11, %rbp
	callq	luaO_fb2int
	movl	%eax, %r14d
	movl	%ebx, %edi
	callq	luaO_fb2int
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r14d, %esi
	movl	%eax, %edx
	callq	luaH_new
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, (%r12)
	movl	$5, 8(%rbp,%r13)
	movq	%r10, 48(%rdi)
	movq	32(%rdi), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB7_44
# BB#43:                                #   in Loop: Header=BB7_8 Depth=3
	callq	luaC_step
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_44:                               #   in Loop: Header=BB7_8 Depth=3
	movq	24(%rdi), %r11
	jmp	.LBB7_8
.LBB7_45:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	shlq	$4, %rax
	leaq	(%r11,%rax), %rsi
	movq	(%r11,%rax), %rcx
	movq	%rcx, 16(%r12)
	movl	8(%r11,%rax), %eax
	movl	%eax, 24(%r12)
	movq	%r10, 48(%rdi)
.LBB7_46:                               # %.thread1078
                                        #   in Loop: Header=BB7_8 Depth=3
	shrl	$14, %ebx
	movl	%ebx, %edx
	andl	$256, %edx              # imm = 0x100
	cmovneq	16(%rsp), %r11          # 8-byte Folded Reload
	xorl	$511, %edx              # imm = 0x1FF
	andl	%ebx, %edx
	shlq	$4, %rdx
	addq	%r11, %rdx
.LBB7_47:                               # %.thread1078
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	%r12, %rcx
	callq	luaV_gettable
	jmp	.LBB7_7
.LBB7_48:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	movzbl	%al, %ebp
	cmovel	%eax, %ebp
	testl	%ecx, %ecx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r8
	cmoveq	%r11, %r8
	shlq	$4, %rbp
	leaq	(%r8,%rbp), %rdx
	shrl	$14, %ebx
	movl	%ebx, %eax
	andl	$256, %eax              # imm = 0x100
	movq	%rcx, %rsi
	cmoveq	%r11, %rsi
	xorl	$511, %eax              # imm = 0x1FF
	andl	%ebx, %eax
	shlq	$4, %rax
	leaq	(%rsi,%rax), %rcx
	cmpl	$3, 8(%r8,%rbp)
	jne	.LBB7_145
# BB#49:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$3, 8(%rsi,%rax)
	jne	.LBB7_145
# BB#50:                                #   in Loop: Header=BB7_8 Depth=3
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rcx), %xmm0
	jmp	.LBB7_66
.LBB7_51:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	movzbl	%al, %ebp
	cmovel	%eax, %ebp
	testl	%ecx, %ecx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r8
	cmoveq	%r11, %r8
	shlq	$4, %rbp
	leaq	(%r8,%rbp), %rdx
	shrl	$14, %ebx
	movl	%ebx, %eax
	andl	$256, %eax              # imm = 0x100
	movq	%rcx, %rsi
	cmoveq	%r11, %rsi
	xorl	$511, %eax              # imm = 0x1FF
	andl	%ebx, %eax
	shlq	$4, %rax
	leaq	(%rsi,%rax), %rcx
	cmpl	$3, 8(%r8,%rbp)
	jne	.LBB7_146
# BB#52:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$3, 8(%rsi,%rax)
	jne	.LBB7_146
# BB#53:                                #   in Loop: Header=BB7_8 Depth=3
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	subsd	(%rcx), %xmm0
	jmp	.LBB7_66
.LBB7_54:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	movzbl	%al, %ebp
	cmovel	%eax, %ebp
	testl	%ecx, %ecx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r8
	cmoveq	%r11, %r8
	shlq	$4, %rbp
	leaq	(%r8,%rbp), %rdx
	shrl	$14, %ebx
	movl	%ebx, %eax
	andl	$256, %eax              # imm = 0x100
	movq	%rcx, %rsi
	cmoveq	%r11, %rsi
	xorl	$511, %eax              # imm = 0x1FF
	andl	%ebx, %eax
	shlq	$4, %rax
	leaq	(%rsi,%rax), %rcx
	cmpl	$3, 8(%r8,%rbp)
	jne	.LBB7_147
# BB#55:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$3, 8(%rsi,%rax)
	jne	.LBB7_147
# BB#56:                                #   in Loop: Header=BB7_8 Depth=3
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rcx), %xmm0
	jmp	.LBB7_66
.LBB7_57:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	movzbl	%al, %ebp
	cmovel	%eax, %ebp
	testl	%ecx, %ecx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r8
	cmoveq	%r11, %r8
	shlq	$4, %rbp
	leaq	(%r8,%rbp), %rdx
	shrl	$14, %ebx
	movl	%ebx, %eax
	andl	$256, %eax              # imm = 0x100
	movq	%rcx, %rsi
	cmoveq	%r11, %rsi
	xorl	$511, %eax              # imm = 0x1FF
	andl	%ebx, %eax
	shlq	$4, %rax
	leaq	(%rsi,%rax), %rcx
	cmpl	$3, 8(%r8,%rbp)
	jne	.LBB7_148
# BB#58:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$3, 8(%rsi,%rax)
	jne	.LBB7_148
# BB#59:                                #   in Loop: Header=BB7_8 Depth=3
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	divsd	(%rcx), %xmm0
	jmp	.LBB7_66
.LBB7_60:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	movzbl	%al, %ebp
	cmovel	%eax, %ebp
	testl	%ecx, %ecx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r8
	cmoveq	%r11, %r8
	shlq	$4, %rbp
	leaq	(%r8,%rbp), %rdx
	shrl	$14, %ebx
	movl	%ebx, %eax
	andl	$256, %eax              # imm = 0x100
	movq	%rcx, %rsi
	cmoveq	%r11, %rsi
	xorl	$511, %eax              # imm = 0x1FF
	andl	%ebx, %eax
	shlq	$4, %rax
	leaq	(%rsi,%rax), %rcx
	cmpl	$3, 8(%r8,%rbp)
	jne	.LBB7_149
# BB#61:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$3, 8(%rsi,%rax)
	jne	.LBB7_149
# BB#62:                                #   in Loop: Header=BB7_8 Depth=3
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	movsd	%xmm1, 64(%rsp)         # 8-byte Spill
	divsd	%xmm1, %xmm0
	movq	%r11, %rbx
	callq	floor
	movq	%rbx, %r11
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	mulsd	64(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%r12)
	movl	$3, 8(%r11,%r13)
	jmp	.LBB7_8
.LBB7_63:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	movzbl	%al, %ebp
	cmovel	%eax, %ebp
	testl	%ecx, %ecx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %r8
	cmoveq	%r11, %r8
	shlq	$4, %rbp
	leaq	(%r8,%rbp), %rdx
	shrl	$14, %ebx
	movl	%ebx, %eax
	andl	$256, %eax              # imm = 0x100
	movq	%rcx, %rsi
	cmoveq	%r11, %rsi
	xorl	$511, %eax              # imm = 0x1FF
	andl	%ebx, %eax
	shlq	$4, %rax
	leaq	(%rsi,%rax), %rcx
	cmpl	$3, 8(%r8,%rbp)
	jne	.LBB7_150
# BB#64:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$3, 8(%rsi,%rax)
	jne	.LBB7_150
# BB#65:                                #   in Loop: Header=BB7_8 Depth=3
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	movq	%r11, %rbx
	callq	pow
	movq	%rbx, %r11
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_66:                               # %.thread1078
                                        #   in Loop: Header=BB7_8 Depth=3
	movsd	%xmm0, (%r12)
	movl	$3, 8(%r11,%r13)
	jmp	.LBB7_8
.LBB7_67:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$23, %ebx
	shlq	$4, %rbx
	leaq	(%r11,%rbx), %rdx
	cmpl	$3, 8(%r11,%rbx)
	jne	.LBB7_5
# BB#68:                                #   in Loop: Header=BB7_8 Depth=3
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	xorpd	.LCPI7_2(%rip), %xmm0
	movlpd	%xmm0, (%r12)
	movl	$3, 8(%r11,%r13)
	jmp	.LBB7_8
.LBB7_69:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$23, %ebx
	shlq	$4, %rbx
	movl	8(%r11,%rbx), %eax
	testl	%eax, %eax
	je	.LBB7_177
# BB#70:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$1, %eax
	jne	.LBB7_178
# BB#71:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$0, (%r11,%rbx)
	sete	%al
	jmp	.LBB7_179
.LBB7_72:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$23, %ebx
	shlq	$4, %rbx
	leaq	(%r11,%rbx), %r15
	movl	8(%r11,%rbx), %eax
	cmpl	$4, %eax
	je	.LBB7_180
# BB#73:                                #   in Loop: Header=BB7_8 Depth=3
	movq	%r11, %r14
	cmpl	$5, %eax
	jne	.LBB7_181
# BB#74:                                #   in Loop: Header=BB7_8 Depth=3
	movq	(%r15), %rdi
	callq	luaH_getn
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%r12)
	movq	%r14, %r11
	movl	$3, 8(%r11,%r13)
	jmp	.LBB7_8
.LBB7_75:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %ebp
	shrl	$23, %ebp
	shrl	$14, %ebx
	andl	$511, %ebx              # imm = 0x1FF
	movq	%r10, 48(%rdi)
	movl	$1, %esi
	subl	%ebp, %esi
	addl	%ebx, %esi
	movl	%ebx, %edx
	callq	luaV_concat
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	32(%rdi), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB7_77
# BB#76:                                #   in Loop: Header=BB7_8 Depth=3
	callq	luaC_step
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_77:                               #   in Loop: Header=BB7_8 Depth=3
	movq	24(%rdi), %r11
	movl	%ebp, %eax
	shlq	$4, %rax
	movq	(%r11,%rax), %rcx
	movq	%rcx, (%r11,%r13)
	movl	8(%r11,%rax), %eax
	movl	%eax, 8(%r11,%r13)
	movq	8(%rsp), %r10           # 8-byte Reload
	jmp	.LBB7_8
.LBB7_78:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %ecx
	shrl	$23, %ecx
	movl	%ecx, %edx
	andl	$256, %edx              # imm = 0x100
	movzbl	%cl, %eax
	cmovel	%ecx, %eax
	testl	%edx, %edx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rsi
	cmoveq	%r11, %rsi
	shrl	$14, %ebx
	movl	%ebx, %ecx
	andl	$256, %ecx              # imm = 0x100
	cmovneq	%rdx, %r11
	xorl	$511, %ecx              # imm = 0x1FF
	andl	%ebx, %ecx
	movq	%r10, 48(%rdi)
	shlq	$4, %rax
	movl	8(%rsi,%rax), %edx
	shlq	$4, %rcx
	cmpl	8(%r11,%rcx), %edx
	jne	.LBB7_152
# BB#79:                                #   in Loop: Header=BB7_8 Depth=3
	addq	%rax, %rsi
	addq	%rcx, %r11
	movq	%r11, %rdx
	callq	luaV_equalval
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	testl	%eax, %eax
	setne	%al
	jmp	.LBB7_153
.LBB7_80:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	%ebx, %eax
	shrl	$23, %eax
	movl	%eax, %ecx
	andl	$256, %ecx              # imm = 0x100
	movzbl	%al, %esi
	cmovel	%eax, %esi
	testl	%ecx, %ecx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rax
	cmoveq	%r11, %rax
	shlq	$4, %rsi
	addq	%rax, %rsi
	shrl	$14, %ebx
	movl	%ebx, %edx
	andl	$256, %edx              # imm = 0x100
	cmovneq	%rcx, %r11
	xorl	$511, %edx              # imm = 0x1FF
	andl	%ebx, %edx
	shlq	$4, %rdx
	addq	%r11, %rdx
	movq	%r10, %rbx
	callq	luaV_lessthan
	cmpl	%r14d, %eax
	jne	.LBB7_82
# BB#81:                                #   in Loop: Header=BB7_8 Depth=3
	movl	(%rbx), %eax
	shrl	$14, %eax
	leaq	-524284(%rbx,%rax,4), %rbx
.LBB7_82:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%rbx, %r10
	jmp	.LBB7_228
.LBB7_83:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	%ebx, %ecx
	shrl	$23, %ecx
	movl	%ecx, %edx
	andl	$256, %edx              # imm = 0x100
	movzbl	%cl, %eax
	cmovel	%ecx, %eax
	testl	%edx, %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %rcx
	cmoveq	%r11, %rcx
	shlq	$4, %rax
	leaq	(%rcx,%rax), %rbp
	shrl	$14, %ebx
	movl	%ebx, %edx
	andl	$256, %edx              # imm = 0x100
	cmovneq	%rsi, %r11
	xorl	$511, %edx              # imm = 0x1FF
	andl	%ebx, %edx
	shlq	$4, %rdx
	leaq	(%r11,%rdx), %rbx
	movl	8(%rcx,%rax), %esi
	cmpl	8(%r11,%rdx), %esi
	jne	.LBB7_155
# BB#84:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$4, %esi
	je	.LBB7_202
# BB#85:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$3, %esi
	jne	.LBB7_204
# BB#86:                                #   in Loop: Header=BB7_8 Depth=3
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	xorl	%ebx, %ebx
	ucomisd	(%rbp), %xmm0
	setae	%bl
	cmpl	%r14d, %ebx
	je	.LBB7_227
	jmp	.LBB7_228
.LBB7_87:                               #   in Loop: Header=BB7_8 Depth=3
	movl	8(%r11,%r13), %eax
	testl	%eax, %eax
	je	.LBB7_187
# BB#88:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$1, %eax
	jne	.LBB7_188
# BB#89:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$0, (%r12)
	sete	%al
	jmp	.LBB7_189
.LBB7_90:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	shlq	$4, %rax
	leaq	(%r11,%rax), %rcx
	movl	8(%r11,%rax), %eax
	testl	%eax, %eax
	je	.LBB7_192
# BB#91:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$1, %eax
	jne	.LBB7_193
# BB#92:                                #   in Loop: Header=BB7_8 Depth=3
	cmpl	$0, (%rcx)
	sete	%dl
	jmp	.LBB7_194
.LBB7_93:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shrl	$23, %eax
	shrl	$14, %ebx
	andl	$511, %ebx              # imm = 0x1FF
	leal	-1(%rbx), %edx
	testl	%eax, %eax
	je	.LBB7_95
# BB#94:                                #   in Loop: Header=BB7_8 Depth=3
	movl	%eax, %eax
	shlq	$4, %rax
	addq	%r12, %rax
	movq	%rax, 16(%rdi)
.LBB7_95:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movq	%r12, %rsi
	callq	luaD_precall
	cmpl	$1, %eax
	jne	.LBB7_244
# BB#96:                                #   in Loop: Header=BB7_8 Depth=3
	testl	%ebx, %ebx
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB7_186
# BB#97:                                #   in Loop: Header=BB7_8 Depth=3
	movq	40(%rdi), %rax
	movq	16(%rax), %rax
	movq	%rax, 16(%rdi)
	jmp	.LBB7_186
.LBB7_98:                               #   in Loop: Header=BB7_8 Depth=3
	shrl	$23, %ebx
	je	.LBB7_100
# BB#99:                                #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %eax
	shlq	$4, %rax
	addq	%r12, %rax
	movq	%rax, 16(%rdi)
.LBB7_100:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%rdi, %rbx
	callq	luaD_precall
	cmpl	$1, %eax
	jne	.LBB7_233
# BB#101:                               #   in Loop: Header=BB7_8 Depth=3
	movq	24(%rbx), %r11
	movq	%rbx, %rdi
	movq	8(%rsp), %r10           # 8-byte Reload
	jmp	.LBB7_8
.LBB7_102:                              #   in Loop: Header=BB7_8 Depth=3
	movsd	16(%r12), %xmm1         # xmm1 = mem[0],zero
	movsd	32(%r12), %xmm2         # xmm2 = mem[0],zero
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	addsd	%xmm2, %xmm0
	xorps	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm2
	jbe	.LBB7_156
# BB#103:                               #   in Loop: Header=BB7_8 Depth=3
	ucomisd	%xmm0, %xmm1
	jb	.LBB7_8
	jmp	.LBB7_157
.LBB7_104:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	leaq	8(%r11,%r13), %r14
	movl	8(%r11,%r13), %eax
	cmpl	$3, %eax
	je	.LBB7_108
# BB#105:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r11, %rbp
	cmpl	$4, %eax
	jne	.LBB7_176
# BB#106:                               #   in Loop: Header=BB7_8 Depth=3
	movq	(%r12), %rdi
	addq	$24, %rdi
	leaq	40(%rsp), %rsi
	callq	luaO_str2d
	movq	(%rsp), %rdi            # 8-byte Reload
	testl	%eax, %eax
	je	.LBB7_176
# BB#107:                               # %luaV_tonumber.exit.thread
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	40(%rsp), %rax
	movq	%rax, (%r12)
	movl	$3, (%r14)
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	%rbp, %r11
.LBB7_108:                              #   in Loop: Header=BB7_8 Depth=3
	movl	24(%r12), %eax
	cmpl	$3, %eax
	je	.LBB7_112
# BB#109:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r11, %rbp
	cmpl	$4, %eax
	jne	.LBB7_197
# BB#110:                               #   in Loop: Header=BB7_8 Depth=3
	movq	16(%r12), %rdi
	addq	$24, %rdi
	leaq	40(%rsp), %rsi
	callq	luaO_str2d
	movq	(%rsp), %rdi            # 8-byte Reload
	testl	%eax, %eax
	je	.LBB7_197
# BB#111:                               # %luaV_tonumber.exit1074.thread
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	40(%rsp), %rax
	movq	%rax, 16(%r12)
	movl	$3, 24(%r12)
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	%rbp, %r11
.LBB7_112:                              #   in Loop: Header=BB7_8 Depth=3
	movl	40(%r12), %eax
	cmpl	$3, %eax
	je	.LBB7_200
# BB#113:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r11, %rbp
	cmpl	$4, %eax
	jne	.LBB7_248
# BB#114:                               #   in Loop: Header=BB7_8 Depth=3
	movq	32(%r12), %rdi
	addq	$24, %rdi
	leaq	40(%rsp), %rsi
	callq	luaO_str2d
	testl	%eax, %eax
	je	.LBB7_248
# BB#115:                               # %luaV_tonumber.exit1076.thread
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	40(%rsp), %rax
	movq	%rax, 32(%r12)
	movl	$3, 40(%r12)
	jmp	.LBB7_199
.LBB7_116:                              #   in Loop: Header=BB7_8 Depth=3
	leaq	48(%r12), %rsi
	movq	32(%r12), %rax
	movq	%rax, 80(%r12)
	movl	40(%r12), %eax
	movl	%eax, 88(%r12)
	movq	16(%r12), %rax
	movq	%rax, 64(%r12)
	movl	24(%r12), %eax
	movl	%eax, 72(%r12)
	movq	(%r12), %rax
	movq	%rax, 48(%r12)
	movl	8(%r11,%r13), %eax
	movl	%eax, 56(%r12)
	addq	$96, %r12
	movq	%r12, 16(%rdi)
	movq	%r10, 48(%rdi)
	shrl	$14, %ebx
	andl	$511, %ebx              # imm = 0x1FF
	movl	%ebx, %edx
	movq	%r10, %rbx
	callq	luaD_call
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	24(%rdi), %r11
	movq	40(%rdi), %rax
	movq	16(%rax), %rax
	movq	%rax, 16(%rdi)
	movl	56(%r11,%r13), %eax
	testl	%eax, %eax
	je	.LBB7_118
# BB#117:                               #   in Loop: Header=BB7_8 Depth=3
	addq	%r11, %r13
	movq	48(%r13), %rcx
	movq	%rcx, 32(%r13)
	movl	%eax, 40(%r13)
	movl	(%rbx), %eax
	shrl	$14, %eax
	leaq	-524284(%rbx,%rax,4), %rbx
.LBB7_118:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%rbx, %r10
	addq	$4, %r10
	jmp	.LBB7_8
.LBB7_119:                              #   in Loop: Header=BB7_8 Depth=3
	movl	%ebx, %ecx
	shrl	$23, %ecx
	shrl	$14, %ebx
	andl	$511, %ebx              # imm = 0x1FF
	testl	%ecx, %ecx
	jne	.LBB7_121
# BB#120:                               #   in Loop: Header=BB7_8 Depth=3
	movq	16(%rdi), %rcx
	movq	40(%rdi), %rax
	subq	%r12, %rcx
	shrq	$4, %rcx
	decl	%ecx
	movq	16(%rax), %rax
	movq	%rax, 16(%rdi)
.LBB7_121:                              #   in Loop: Header=BB7_8 Depth=3
	testl	%ebx, %ebx
	jne	.LBB7_123
# BB#122:                               #   in Loop: Header=BB7_8 Depth=3
	movl	4(%r15), %ebx
	addq	$8, %r15
	movq	%r15, %r10
.LBB7_123:                              #   in Loop: Header=BB7_8 Depth=3
	cmpl	$5, 8(%r11,%r13)
	jne	.LBB7_8
# BB#124:                               #   in Loop: Header=BB7_8 Depth=3
	movq	(%r12), %r15
	imull	$50, %ebx, %eax
	leal	-50(%rcx,%rax), %r13d
	cmpl	56(%r15), %r13d
	jle	.LBB7_126
# BB#125:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r15, %rsi
	movl	%r13d, %edx
	movq	%r10, %rbx
	movq	%r11, %r12
	movq	%rcx, %rbp
	callq	luaH_resizearray
	movq	%rbp, %rcx
	movq	%r12, %r11
	movq	%rbx, %r10
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_126:                              # %.preheader
                                        #   in Loop: Header=BB7_8 Depth=3
	testl	%ecx, %ecx
	jle	.LBB7_8
# BB#127:                               # %.lr.ph1131
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, %r12
	movslq	%ecx, %rbp
	shlq	$2, %r14
	leaq	(%r14,%rbp,4), %rax
	incq	%rbp
	movq	%r11, %r14
	leaq	8(%r11,%rax,4), %rbx
	.p2align	4, 0x90
.LBB7_128:                              #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r15, %rsi
	movl	%r13d, %edx
	callq	luaH_setnum
	movq	-8(%rbx), %rcx
	movq	%rcx, (%rax)
	movl	(%rbx), %ecx
	movl	%ecx, 8(%rax)
	cmpl	$4, (%rbx)
	jl	.LBB7_132
# BB#129:                               #   in Loop: Header=BB7_128 Depth=4
	movq	-8(%rbx), %rax
	testb	$3, 9(%rax)
	je	.LBB7_132
# BB#130:                               #   in Loop: Header=BB7_128 Depth=4
	testb	$4, 9(%r15)
	je	.LBB7_132
# BB#131:                               #   in Loop: Header=BB7_128 Depth=4
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r15, %rsi
	callq	luaC_barrierback
	.p2align	4, 0x90
.LBB7_132:                              #   in Loop: Header=BB7_128 Depth=4
	decl	%r13d
	decq	%rbp
	addq	$-16, %rbx
	cmpq	$1, %rbp
	movq	(%rsp), %rdi            # 8-byte Reload
	jg	.LBB7_128
# BB#133:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r12, %r10
	movq	%r14, %r11
	jmp	.LBB7_8
.LBB7_134:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r12, %rsi
	movq	%r11, %rbx
	callq	luaF_close
.LBB7_135:                              # %.thread1078
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	%rbx, %r11
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB7_8
.LBB7_136:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r11, %r14
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rdx
	movq	32(%rax), %rax
	movq	32(%rax), %rax
	shrl	$14, %ebx
	movq	(%rax,%rbx,8), %rbx
	movzbl	112(%rbx), %ebp
	movl	%ebp, %esi
	callq	luaF_newLclosure
	testl	%ebp, %ebp
	movq	%rbx, 32(%rax)
	je	.LBB7_158
# BB#137:                               # %.lr.ph1128
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rax, %rbx
	addq	$40, %rbx
	movq	%rbp, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %r15
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	%r10, %rbp
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB7_138:                              #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%rbp), %esi
	movl	%esi, %eax
	andl	$63, %eax
	shrl	$23, %esi
	cmpl	$4, %eax
	jne	.LBB7_140
# BB#139:                               #   in Loop: Header=BB7_138 Depth=4
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	40(%rax,%rsi,8), %rax
	jmp	.LBB7_141
	.p2align	4, 0x90
.LBB7_140:                              #   in Loop: Header=BB7_138 Depth=4
	shlq	$4, %rsi
	addq	%rcx, %rsi
	callq	luaF_findupval
	movq	%r14, %rcx
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_141:                              #   in Loop: Header=BB7_138 Depth=4
	movq	%rax, (%rbx)
	addq	$4, %rbp
	addq	$8, %rbx
	decq	%r15
	jne	.LBB7_138
# BB#142:                               # %._crit_edge.loopexit
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r10,%rax,4), %r10
	movq	64(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_159
.LBB7_143:                              #   in Loop: Header=BB7_8 Depth=3
	shrl	$23, %ebx
	movq	40(%rdi), %r14
	movq	(%r14), %rax
	subq	8(%r14), %rax
	shrq	$4, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rcx), %rcx
	movzbl	113(%rcx), %ecx
	movslq	%eax, %r15
	subq	%rcx, %r15
	leaq	-1(%r15), %rbp
	testl	%ebx, %ebx
	je	.LBB7_162
# BB#144:                               #   in Loop: Header=BB7_8 Depth=3
	decl	%ebx
	testl	%ebx, %ebx
	jle	.LBB7_8
	jmp	.LBB7_165
.LBB7_145:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	$5, %r8d
	jmp	.LBB7_151
.LBB7_146:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	$6, %r8d
	jmp	.LBB7_151
.LBB7_147:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	$7, %r8d
	jmp	.LBB7_151
.LBB7_148:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	$8, %r8d
	jmp	.LBB7_151
.LBB7_149:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	$9, %r8d
	jmp	.LBB7_151
.LBB7_150:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	$10, %r8d
.LBB7_151:                              # %.thread1078
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	%r12, %rsi
	jmp	.LBB7_6
.LBB7_152:                              #   in Loop: Header=BB7_8 Depth=3
	xorl	%eax, %eax
.LBB7_153:                              #   in Loop: Header=BB7_8 Depth=3
	movzbl	%al, %eax
	cmpl	%r14d, %eax
	jne	.LBB7_229
# BB#154:                               #   in Loop: Header=BB7_8 Depth=3
	movl	(%r10), %eax
	shrl	$14, %eax
	leaq	-524284(%r10,%rax,4), %r10
	jmp	.LBB7_229
.LBB7_155:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	luaG_ordererror
	movq	8(%rsp), %r10           # 8-byte Reload
	movl	%eax, %ebx
	cmpl	%r14d, %ebx
	je	.LBB7_227
	jmp	.LBB7_228
.LBB7_156:                              #   in Loop: Header=BB7_8 Depth=3
	ucomisd	%xmm1, %xmm0
	jb	.LBB7_8
.LBB7_157:                              #   in Loop: Header=BB7_8 Depth=3
	shrl	$14, %ebx
	leaq	-524284(%r10,%rbx,4), %r10
	movsd	%xmm0, (%r12)
	movl	$3, 8(%r11,%r13)
	movsd	%xmm0, 48(%r12)
	movl	$3, 56(%r12)
	jmp	.LBB7_8
.LBB7_158:                              #   in Loop: Header=BB7_8 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	%r14, %rcx
.LBB7_159:                              # %._crit_edge
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	%rax, (%r12)
	movl	$6, 8(%rcx,%r13)
	movq	%r10, 48(%rdi)
	movq	32(%rdi), %rax
	movq	120(%rax), %rcx
	cmpq	112(%rax), %rcx
	jb	.LBB7_161
# BB#160:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, %rbx
	callq	luaC_step
	movq	%rbx, %r10
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_161:                              #   in Loop: Header=BB7_8 Depth=3
	movq	24(%rdi), %r11
	jmp	.LBB7_8
.LBB7_162:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movq	56(%rdi), %rax
	subq	16(%rdi), %rax
	movq	%rbp, %rcx
	shlq	$4, %rcx
	cmpq	%rcx, %rax
	jg	.LBB7_164
# BB#163:                               #   in Loop: Header=BB7_8 Depth=3
	movl	%ebp, %esi
	callq	luaD_growstack
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_164:                              #   in Loop: Header=BB7_8 Depth=3
	movq	24(%rdi), %r11
	addq	%r11, %r13
	movslq	%ebp, %rax
	shlq	$4, %rax
	addq	%r13, %rax
	movq	%rax, 16(%rdi)
	movl	%ebp, %ebx
	movq	%r13, %r12
	testl	%ebx, %ebx
	jle	.LBB7_8
.LBB7_165:                              # %.lr.ph
                                        #   in Loop: Header=BB7_8 Depth=3
	movslq	%ebp, %r9
	movl	%ebx, %r8d
	xorl	%edx, %edx
	testb	$1, %r8b
	je	.LBB7_169
# BB#166:                               #   in Loop: Header=BB7_8 Depth=3
	cmpl	$2, %r15d
	jl	.LBB7_168
# BB#167:                               #   in Loop: Header=BB7_8 Depth=3
	movq	%r9, %rdx
	negq	%rdx
	movq	(%r14), %rsi
	shlq	$4, %rdx
	movq	(%rsi,%rdx), %rax
	movq	%rax, (%r12)
	movl	8(%rsi,%rdx), %edx
.LBB7_168:                              #   in Loop: Header=BB7_8 Depth=3
	movl	%edx, 8(%r12)
	movl	$1, %edx
.LBB7_169:                              # %.prol.loopexit
                                        #   in Loop: Header=BB7_8 Depth=3
	cmpl	$1, %ebx
	je	.LBB7_8
# BB#170:                               # %.lr.ph.new
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	%rdx, %rsi
	shlq	$4, %rsi
	leaq	24(%r12,%rsi), %rcx
	movq	%r9, %rax
	shlq	$4, %rax
	subq	%rax, %rsi
	.p2align	4, 0x90
.LBB7_171:                              #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	xorl	%ebp, %ebp
	cmpq	%r9, %rdx
	movl	$0, %ebx
	jge	.LBB7_173
# BB#172:                               #   in Loop: Header=BB7_171 Depth=4
	movq	(%r14), %rax
	movq	(%rax,%rsi), %rbx
	movq	%rbx, -24(%rcx)
	movl	8(%rax,%rsi), %ebx
.LBB7_173:                              #   in Loop: Header=BB7_171 Depth=4
	movl	%ebx, -16(%rcx)
	incq	%rdx
	cmpq	%r9, %rdx
	jge	.LBB7_175
# BB#174:                               #   in Loop: Header=BB7_171 Depth=4
	movq	(%r14), %rax
	movq	16(%rax,%rsi), %rbp
	movq	%rbp, -8(%rcx)
	movl	24(%rax,%rsi), %ebp
.LBB7_175:                              #   in Loop: Header=BB7_171 Depth=4
	movl	%ebp, (%rcx)
	addq	$32, %rcx
	addq	$32, %rsi
	incq	%rdx
	cmpq	%r8, %rdx
	jne	.LBB7_171
	jmp	.LBB7_8
.LBB7_176:                              #   in Loop: Header=BB7_8 Depth=3
	movl	$.L.str.6, %esi
	jmp	.LBB7_198
.LBB7_177:                              #   in Loop: Header=BB7_8 Depth=3
	movb	$1, %al
	jmp	.LBB7_179
.LBB7_178:                              # %.fold.split
                                        #   in Loop: Header=BB7_8 Depth=3
	xorl	%eax, %eax
.LBB7_179:                              #   in Loop: Header=BB7_8 Depth=3
	movzbl	%al, %eax
	movl	%eax, (%r12)
	movl	$1, 8(%r11,%r13)
	jmp	.LBB7_8
.LBB7_180:                              #   in Loop: Header=BB7_8 Depth=3
	movq	(%r15), %rax
	movq	16(%rax), %xmm0         # xmm0 = mem[0],zero
	punpckldq	.LCPI7_0(%rip), %xmm0 # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	.LCPI7_1(%rip), %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	addpd	%xmm0, %xmm1
	movlpd	%xmm1, (%r12)
	movl	$3, 8(%r11,%r13)
	jmp	.LBB7_8
.LBB7_181:                              #   in Loop: Header=BB7_8 Depth=3
	movq	%r10, 48(%rdi)
	movl	$12, %edx
	movq	%r15, %rsi
	movq	%rdi, %rbp
	callq	luaT_gettmbyobj
	cmpl	$0, 8(%rax)
	jne	.LBB7_183
# BB#182:                               #   in Loop: Header=BB7_8 Depth=3
	movl	$luaO_nilobject_, %esi
	movl	$12, %edx
	movq	%rbp, %rdi
	callq	luaT_gettmbyobj
	cmpl	$0, 8(%rax)
	je	.LBB7_223
.LBB7_183:                              # %.thread.i
                                        #   in Loop: Header=BB7_8 Depth=3
	leaq	8(%r14,%rbx), %rcx
	subq	64(%rbp), %r12
	movq	16(%rbp), %rdx
	movq	(%rax), %rsi
	movq	%rsi, (%rdx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movq	16(%rbp), %rax
	movq	(%r15), %rdx
	movq	%rdx, 16(%rax)
	movl	(%rcx), %ecx
	movl	%ecx, 24(%rax)
	movq	16(%rbp), %rax
	movq	luaO_nilobject_(%rip), %rcx
	movq	%rcx, 32(%rax)
	movl	luaO_nilobject_+8(%rip), %ecx
	movl	%ecx, 40(%rax)
	movq	16(%rbp), %rsi
	movq	56(%rbp), %rax
	subq	%rsi, %rax
	cmpq	$48, %rax
	movq	%rbp, %rdi
	jg	.LBB7_185
# BB#184:                               #   in Loop: Header=BB7_8 Depth=3
	movl	$3, %esi
	callq	luaD_growstack
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	16(%rdi), %rsi
.LBB7_185:                              # %call_binTM.exit
                                        #   in Loop: Header=BB7_8 Depth=3
	leaq	48(%rsi), %rax
	movq	%rax, 16(%rdi)
	movl	$1, %edx
	callq	luaD_call
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	16(%rdi), %rax
	movq	64(%rdi), %rcx
	leaq	-16(%rax), %rdx
	movq	%rdx, 16(%rdi)
	movq	-16(%rax), %rdx
	movq	%rdx, (%rcx,%r12)
	movl	-8(%rax), %eax
	movl	%eax, 8(%rcx,%r12)
.LBB7_186:                              #   in Loop: Header=BB7_8 Depth=3
	movq	24(%rdi), %r11
	movq	8(%rsp), %r10           # 8-byte Reload
	jmp	.LBB7_8
.LBB7_187:                              #   in Loop: Header=BB7_8 Depth=3
	movb	$1, %al
	jmp	.LBB7_189
.LBB7_188:                              # %.fold.split1066
                                        #   in Loop: Header=BB7_8 Depth=3
	xorl	%eax, %eax
.LBB7_189:                              #   in Loop: Header=BB7_8 Depth=3
	movzbl	%al, %eax
	shrl	$14, %ebx
	andl	$511, %ebx              # imm = 0x1FF
	cmpl	%ebx, %eax
	je	.LBB7_191
# BB#190:                               #   in Loop: Header=BB7_8 Depth=3
	movl	(%r10), %eax
	shrl	$14, %eax
	leaq	-524284(%r10,%rax,4), %r10
.LBB7_191:                              #   in Loop: Header=BB7_8 Depth=3
	addq	$4, %r10
	jmp	.LBB7_8
.LBB7_192:                              #   in Loop: Header=BB7_8 Depth=3
	movb	$1, %dl
	jmp	.LBB7_194
.LBB7_193:                              # %.fold.split1067
                                        #   in Loop: Header=BB7_8 Depth=3
	xorl	%edx, %edx
.LBB7_194:                              #   in Loop: Header=BB7_8 Depth=3
	movzbl	%dl, %edx
	shrl	$14, %ebx
	andl	$511, %ebx              # imm = 0x1FF
	cmpl	%ebx, %edx
	je	.LBB7_196
# BB#195:                               #   in Loop: Header=BB7_8 Depth=3
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movl	%eax, 8(%r11,%r13)
	movl	(%r10), %eax
	shrl	$14, %eax
	leaq	-524284(%r10,%rax,4), %r10
.LBB7_196:                              #   in Loop: Header=BB7_8 Depth=3
	addq	$4, %r10
	jmp	.LBB7_8
.LBB7_197:                              #   in Loop: Header=BB7_8 Depth=3
	movl	$.L.str.7, %esi
.LBB7_198:                              #   in Loop: Header=BB7_8 Depth=3
	xorl	%eax, %eax
	callq	luaG_runerror
.LBB7_199:                              #   in Loop: Header=BB7_8 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	%rbp, %r11
.LBB7_200:                              #   in Loop: Header=BB7_8 Depth=3
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	subsd	32(%r12), %xmm0
	movsd	%xmm0, (%r12)
	movl	$3, (%r14)
.LBB7_201:                              #   in Loop: Header=BB7_8 Depth=3
	shrl	$14, %ebx
	leaq	-524284(%r10,%rbx,4), %r10
	jmp	.LBB7_8
.LBB7_202:                              #   in Loop: Header=BB7_8 Depth=3
	movq	(%rbp), %rbp
	movq	(%rbx), %rbx
	leaq	24(%rbp), %r12
	leaq	24(%rbx), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	strcoll
	testl	%eax, %eax
	je	.LBB7_218
.LBB7_203:                              # %l_strcmp.exit.i
                                        #   in Loop: Header=BB7_8 Depth=3
	xorl	%ebx, %ebx
	testl	%eax, %eax
	setle	%bl
	jmp	.LBB7_225
.LBB7_204:                              #   in Loop: Header=BB7_8 Depth=3
	leaq	8(%rcx,%rax), %r13
	leaq	8(%r11,%rdx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$14, %edx
	movq	%rbp, %rsi
	movq	%rdi, %r12
	callq	luaT_gettmbyobj
	movq	%rax, %r15
	cmpl	$0, 8(%r15)
	je	.LBB7_211
# BB#205:                               #   in Loop: Header=BB7_8 Depth=3
	movl	$14, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	luaT_gettmbyobj
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	luaO_rawequalObj
	testl	%eax, %eax
	je	.LBB7_211
# BB#206:                               #   in Loop: Header=BB7_8 Depth=3
	movq	16(%r12), %rax
	movq	%r13, %rdx
	movq	%rax, %r13
	subq	64(%r12), %r13
	movq	(%r15), %rcx
	movq	%rcx, (%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rax)
	movq	16(%r12), %rax
	movq	(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movl	(%rdx), %ecx
	movl	%ecx, 24(%rax)
	movq	16(%r12), %rax
	movq	(%rbx), %rcx
	movq	%rcx, 32(%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	movl	%ecx, 40(%rax)
	movq	16(%r12), %rsi
	movq	56(%r12), %rax
	subq	%rsi, %rax
	cmpq	$48, %rax
	movq	%r12, %rdi
	jg	.LBB7_208
# BB#207:                               #   in Loop: Header=BB7_8 Depth=3
	movl	$3, %esi
	callq	luaD_growstack
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	16(%rdi), %rsi
.LBB7_208:                              # %callTMres.exit.i
                                        #   in Loop: Header=BB7_8 Depth=3
	leaq	48(%rsi), %rax
	movq	%rax, 16(%rdi)
	movl	$1, %edx
	callq	luaD_call
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsi), %rax
	movq	64(%rsi), %rcx
	leaq	-16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	-16(%rax), %rdx
	movq	%rdx, (%rcx,%r13)
	movl	-8(%rax), %eax
	movl	%eax, 8(%rcx,%r13)
	movq	16(%rsi), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB7_224
# BB#209:                               # %callTMres.exit.i
                                        #   in Loop: Header=BB7_8 Depth=3
	cmpl	$1, %ecx
	movq	8(%rsp), %r10           # 8-byte Reload
	jne	.LBB7_230
# BB#210:                               # %call_orderTM.exit.i
                                        #   in Loop: Header=BB7_8 Depth=3
	xorl	%ebx, %ebx
	cmpl	$0, (%rax)
	setne	%bl
	cmpl	%r14d, %ebx
	je	.LBB7_227
	jmp	.LBB7_228
.LBB7_211:                              # %call_orderTM.exit.thread.i
                                        #   in Loop: Header=BB7_8 Depth=3
	movl	$13, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	luaT_gettmbyobj
	movq	%rax, %r15
	cmpl	$0, 8(%r15)
	je	.LBB7_222
# BB#212:                               #   in Loop: Header=BB7_8 Depth=3
	movl	$13, %edx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	luaT_gettmbyobj
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	luaO_rawequalObj
	testl	%eax, %eax
	je	.LBB7_222
# BB#213:                               #   in Loop: Header=BB7_8 Depth=3
	movq	16(%r12), %rax
	movq	%r13, %rdx
	movq	%rax, %r13
	subq	64(%r12), %r13
	movq	(%r15), %rcx
	movq	%rcx, (%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rax)
	movq	16(%r12), %rax
	movq	(%rbx), %rcx
	movq	%rcx, 16(%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	movl	%ecx, 24(%rax)
	movq	16(%r12), %rax
	movq	(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movl	(%rdx), %ecx
	movl	%ecx, 40(%rax)
	movq	16(%r12), %rsi
	movq	56(%r12), %rax
	subq	%rsi, %rax
	cmpq	$48, %rax
	movq	%r12, %rdi
	jg	.LBB7_215
# BB#214:                               #   in Loop: Header=BB7_8 Depth=3
	movl	$3, %esi
	callq	luaD_growstack
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	16(%rdi), %rsi
.LBB7_215:                              # %callTMres.exit27.i
                                        #   in Loop: Header=BB7_8 Depth=3
	leaq	48(%rsi), %rax
	movq	%rax, 16(%rdi)
	movl	$1, %ebx
	movl	$1, %edx
	movq	%rdi, %rbp
	callq	luaD_call
	movq	16(%rbp), %rax
	movq	64(%rbp), %rcx
	leaq	-16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	-16(%rax), %rdx
	movq	%rdx, (%rcx,%r13)
	movl	-8(%rax), %eax
	movl	%eax, 8(%rcx,%r13)
	movq	16(%rbp), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	movq	8(%rsp), %r10           # 8-byte Reload
	je	.LBB7_226
# BB#216:                               # %callTMres.exit27.i
                                        #   in Loop: Header=BB7_8 Depth=3
	xorl	%ebx, %ebx
	cmpl	$1, %ecx
	jne	.LBB7_226
# BB#217:                               # %call_orderTM.exit25.i
                                        #   in Loop: Header=BB7_8 Depth=3
	cmpl	$0, (%rax)
	sete	%bl
	cmpl	%r14d, %ebx
	je	.LBB7_227
	jmp	.LBB7_228
.LBB7_218:                              # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	16(%rbx), %rbx
	movq	16(%rbp), %rbp
	.p2align	4, 0x90
.LBB7_219:                              # %.lr.ph.i.i
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        #       Parent Loop BB7_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rcx
	cmpq	%rbx, %rcx
	je	.LBB7_231
# BB#220:                               #   in Loop: Header=BB7_219 Depth=4
	cmpq	%rbp, %rcx
	je	.LBB7_232
# BB#221:                               #   in Loop: Header=BB7_219 Depth=4
	leaq	1(%rcx), %rax
	leaq	1(%r12,%rcx), %r12
	subq	%rax, %rbp
	leaq	1(%r15,%rcx), %r15
	subq	%rax, %rbx
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	strcoll
	testl	%eax, %eax
	je	.LBB7_219
	jmp	.LBB7_203
.LBB7_222:                              # %call_orderTM.exit25.thread.i
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	luaG_ordererror
	movl	%eax, %ebx
	jmp	.LBB7_225
.LBB7_223:                              #   in Loop: Header=BB7_8 Depth=3
	movl	$.L.str.5, %edx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	luaG_typeerror
	movq	24(%rbp), %r11
	movq	%rbp, %rdi
	movq	8(%rsp), %r10           # 8-byte Reload
	jmp	.LBB7_8
.LBB7_224:                              #   in Loop: Header=BB7_8 Depth=3
	xorl	%ebx, %ebx
.LBB7_225:                              # %lessequal.exit
                                        #   in Loop: Header=BB7_8 Depth=3
	movq	8(%rsp), %r10           # 8-byte Reload
.LBB7_226:                              # %lessequal.exit
                                        #   in Loop: Header=BB7_8 Depth=3
	cmpl	%r14d, %ebx
	jne	.LBB7_228
.LBB7_227:                              #   in Loop: Header=BB7_8 Depth=3
	movl	(%r10), %eax
	shrl	$14, %eax
	leaq	-524284(%r10,%rax,4), %r10
.LBB7_228:                              #   in Loop: Header=BB7_8 Depth=3
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_229:                              #   in Loop: Header=BB7_8 Depth=3
	movq	24(%rdi), %r11
	addq	$4, %r10
	jmp	.LBB7_8
.LBB7_230:                              # %.fold.split.i.i
                                        #   in Loop: Header=BB7_8 Depth=3
	movl	$1, %ebx
	cmpl	%r14d, %ebx
	je	.LBB7_227
	jmp	.LBB7_228
.LBB7_231:                              #   in Loop: Header=BB7_8 Depth=3
	xorl	%eax, %eax
	cmpq	%rbp, %rcx
	setne	%al
	jmp	.LBB7_203
.LBB7_232:                              #   in Loop: Header=BB7_8 Depth=3
	movl	$-1, %eax
	jmp	.LBB7_203
.LBB7_233:                              #   in Loop: Header=BB7_4 Depth=2
	testl	%eax, %eax
	jne	.LBB7_247
# BB#234:                               #   in Loop: Header=BB7_4 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	40(%rdi), %r14
	movq	-32(%r14), %rbp
	movq	8(%r14), %rbx
	cmpq	$0, 152(%rdi)
	movq	%rbp, %rax
	je	.LBB7_236
# BB#235:                               #   in Loop: Header=BB7_4 Depth=2
	movq	-40(%r14), %rsi
	callq	luaF_close
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	-32(%r14), %rax
.LBB7_236:                              # %._crit_edge1181
                                        #   in Loop: Header=BB7_4 Depth=2
	movq	(%r14), %rcx
	subq	%rbx, %rcx
	addq	%rax, %rcx
	movq	%rcx, -40(%r14)
	movq	%rcx, 24(%rdi)
	cmpq	16(%rdi), %rbx
	jae	.LBB7_3
	.p2align	4, 0x90
.LBB7_237:                              # %.lr.ph1134
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rax
	movq	%rax, (%rbp)
	movl	8(%rbx), %eax
	movl	%eax, 8(%rbp)
	addq	$16, %rbp
	addq	$16, %rbx
	cmpq	16(%rdi), %rbx
	jb	.LBB7_237
	jmp	.LBB7_3
	.p2align	4, 0x90
.LBB7_238:                              #   in Loop: Header=BB7_2 Depth=1
	shrl	$23, %ebx
	je	.LBB7_240
# BB#239:                               #   in Loop: Header=BB7_2 Depth=1
	movl	%ebx, %eax
	shlq	$4, %rax
	leaq	-16(%r12,%rax), %rax
	movq	%rax, 16(%rdi)
.LBB7_240:                              #   in Loop: Header=BB7_2 Depth=1
	cmpq	$0, 152(%rdi)
	je	.LBB7_242
# BB#241:                               #   in Loop: Header=BB7_2 Depth=1
	movq	%r11, %rsi
	callq	luaF_close
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB7_242:                              #   in Loop: Header=BB7_2 Depth=1
	movq	%r10, 48(%rdi)
	movq	%r12, %rsi
	callq	luaD_poscall
	decl	60(%rsp)                # 4-byte Folded Spill
	je	.LBB7_247
# BB#243:                               #   in Loop: Header=BB7_2 Depth=1
	testl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB7_2
	jmp	.LBB7_1
.LBB7_244:                              #   in Loop: Header=BB7_2 Depth=1
	testl	%eax, %eax
	jne	.LBB7_247
# BB#245:                               #   in Loop: Header=BB7_2 Depth=1
	incl	60(%rsp)                # 4-byte Folded Spill
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB7_2
.LBB7_246:
	movq	%r15, 48(%rdi)
.LBB7_247:                              # %.loopexit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_248:
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	luaG_runerror
.Lfunc_end7:
	.size	luaV_execute, .Lfunc_end7-luaV_execute
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_27
	.quad	.LBB7_28
	.quad	.LBB7_29
	.quad	.LBB7_30
	.quad	.LBB7_32
	.quad	.LBB7_33
	.quad	.LBB7_34
	.quad	.LBB7_35
	.quad	.LBB7_36
	.quad	.LBB7_40
	.quad	.LBB7_42
	.quad	.LBB7_45
	.quad	.LBB7_48
	.quad	.LBB7_51
	.quad	.LBB7_54
	.quad	.LBB7_57
	.quad	.LBB7_60
	.quad	.LBB7_63
	.quad	.LBB7_67
	.quad	.LBB7_69
	.quad	.LBB7_72
	.quad	.LBB7_75
	.quad	.LBB7_201
	.quad	.LBB7_78
	.quad	.LBB7_80
	.quad	.LBB7_83
	.quad	.LBB7_87
	.quad	.LBB7_90
	.quad	.LBB7_93
	.quad	.LBB7_98
	.quad	.LBB7_238
	.quad	.LBB7_102
	.quad	.LBB7_104
	.quad	.LBB7_116
	.quad	.LBB7_119
	.quad	.LBB7_134
	.quad	.LBB7_136
	.quad	.LBB7_143

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.p2align	4, 0x90
	.type	Arith,@function
Arith:                                  # @Arith
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 128
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	8(%r12), %eax
	cmpl	$3, %eax
	movq	%r12, %rbp
	je	.LBB8_4
# BB#1:
	cmpl	$4, %eax
	jne	.LBB8_18
# BB#2:
	movq	(%r12), %rdi
	addq	$24, %rdi
	leaq	24(%rsp), %rsi
	callq	luaO_str2d
	testl	%eax, %eax
	je	.LBB8_18
# BB#3:
	movq	24(%rsp), %rax
	movq	%rax, 56(%rsp)
	movl	$3, 64(%rsp)
	leaq	56(%rsp), %rbp
.LBB8_4:
	movl	8(%r15), %eax
	cmpl	$3, %eax
	je	.LBB8_8
# BB#5:
	cmpl	$4, %eax
	jne	.LBB8_18
# BB#6:
	movq	(%r15), %rdi
	addq	$24, %rdi
	leaq	48(%rsp), %rsi
	callq	luaO_str2d
	testl	%eax, %eax
	je	.LBB8_18
# BB#7:
	movq	48(%rsp), %rax
	movq	%rax, 24(%rsp)
	movl	$3, 32(%rsp)
	leaq	24(%rsp), %r15
.LBB8_8:
	addl	$-5, %r13d
	cmpl	$6, %r13d
	ja	.LBB8_24
# BB#9:
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	movsd	(%r15), %xmm1           # xmm1 = mem[0],zero
	jmpq	*.LJTI8_0(,%r13,8)
.LBB8_10:
	addsd	%xmm1, %xmm0
	jmp	.LBB8_17
.LBB8_18:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	%r13d, %edx
	callq	luaT_gettmbyobj
	cmpl	$0, 8(%rax)
	jne	.LBB8_20
# BB#19:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movl	%r13d, %edx
	callq	luaT_gettmbyobj
	cmpl	$0, 8(%rax)
	je	.LBB8_23
.LBB8_20:                               # %.thread.i
	subq	64(%rbx), %r14
	movq	16(%rbx), %rcx
	movq	(%rax), %rdx
	movq	%rdx, (%rcx)
	movl	8(%rax), %eax
	movl	%eax, 8(%rcx)
	movq	16(%rbx), %rax
	movq	(%r12), %rcx
	movq	%rcx, 16(%rax)
	movl	8(%r12), %ecx
	movl	%ecx, 24(%rax)
	movq	16(%rbx), %rax
	movq	(%r15), %rcx
	movq	%rcx, 32(%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 40(%rax)
	movq	16(%rbx), %rsi
	movq	56(%rbx), %rax
	subq	%rsi, %rax
	cmpq	$48, %rax
	jg	.LBB8_22
# BB#21:
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	luaD_growstack
	movq	16(%rbx), %rsi
.LBB8_22:                               # %call_binTM.exit
	leaq	48(%rsi), %rax
	movq	%rax, 16(%rbx)
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	luaD_call
	movq	16(%rbx), %rax
	movq	64(%rbx), %rcx
	leaq	-16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	-16(%rax), %rdx
	movq	%rdx, (%rcx,%r14)
	movl	-8(%rax), %eax
	movl	%eax, 8(%rcx,%r14)
	jmp	.LBB8_24
.LBB8_23:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	luaG_aritherror
	jmp	.LBB8_24
.LBB8_11:
	subsd	%xmm1, %xmm0
	jmp	.LBB8_17
.LBB8_12:
	mulsd	%xmm1, %xmm0
	jmp	.LBB8_17
.LBB8_13:
	divsd	%xmm1, %xmm0
	jmp	.LBB8_17
.LBB8_14:
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	movapd	(%rsp), %xmm0           # 16-byte Reload
	divsd	%xmm1, %xmm0
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	callq	floor
	mulsd	40(%rsp), %xmm0         # 8-byte Folded Reload
	movapd	(%rsp), %xmm1           # 16-byte Reload
	subsd	%xmm0, %xmm1
	movapd	%xmm1, (%rsp)           # 16-byte Spill
	movapd	(%rsp), %xmm0           # 16-byte Reload
	jmp	.LBB8_17
.LBB8_15:
	callq	pow
	jmp	.LBB8_17
.LBB8_16:
	xorpd	.LCPI8_0(%rip), %xmm0
.LBB8_17:                               # %.sink.split
	movsd	%xmm0, (%r14)
	movl	$3, 8(%r14)
.LBB8_24:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	Arith, .Lfunc_end8-Arith
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_10
	.quad	.LBB8_11
	.quad	.LBB8_12
	.quad	.LBB8_13
	.quad	.LBB8_14
	.quad	.LBB8_15
	.quad	.LBB8_16

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%.14g"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"index"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"loop in gettable"
	.size	.L.str.2, 17

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"loop in settable"
	.size	.L.str.3, 17

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"string length overflow"
	.size	.L.str.4, 23

	.hidden	luaO_nilobject_
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"get length of"
	.size	.L.str.5, 14

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"'for' initial value must be a number"
	.size	.L.str.6, 37

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"'for' limit must be a number"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"'for' step must be a number"
	.size	.L.str.8, 28

	.hidden	luaO_str2d
	.hidden	luaS_newlstr
	.hidden	luaH_get
	.hidden	luaT_gettm
	.hidden	luaT_gettmbyobj
	.hidden	luaG_typeerror
	.hidden	luaG_runerror
	.hidden	luaH_set
	.hidden	luaC_barrierback
	.hidden	luaG_ordererror
	.hidden	luaG_concaterror
	.hidden	luaZ_openspace
	.hidden	luaC_barrierf
	.hidden	luaH_new
	.hidden	luaO_fb2int
	.hidden	luaC_step
	.hidden	luaH_getn
	.hidden	luaD_precall
	.hidden	luaF_close
	.hidden	luaD_poscall
	.hidden	luaD_call
	.hidden	luaH_resizearray
	.hidden	luaH_setnum
	.hidden	luaF_newLclosure
	.hidden	luaF_findupval
	.hidden	luaD_growstack
	.hidden	luaO_rawequalObj
	.hidden	luaD_callhook
	.hidden	luaG_aritherror

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
