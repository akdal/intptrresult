	.text
	.file	"output.bc"
	.globl	O_cleanup
	.p2align	4, 0x90
	.type	O_cleanup,@function
O_cleanup:                              # @O_cleanup
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	O_cleanup, .Lfunc_end0-O_cleanup
	.cfi_endproc

	.globl	O_output
	.p2align	4, 0x90
	.type	O_output,@function
O_output:                               # @O_output
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, 12(%rsp)          # 4-byte Spill
	testq	%rdi, %rdi
	je	.LBB1_118
# BB#1:                                 # %.lr.ph212.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph212
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rax
	movq	%rdi, %r15
	decl	12(%r15)
	decl	16(%r15)
	movq	(%r15), %rdi
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	jne	.LBB1_2
# BB#3:                                 # %.lr.ph208
	andl	$8, 12(%rsp)            # 4-byte Folded Spill
	movl	$-1, %ebp
	jmp	.LBB1_4
.LBB1_10:                               # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	decq	%rbp
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph.split
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	_K_ato+8(,%rbp,8), %r14
	movq	16(%r14), %rbx
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$1, %rax
	jne	.LBB1_19
# BB#12:                                #   in Loop: Header=BB1_11 Depth=2
	movsbl	(%rbx), %eax
	cmpl	$9, %eax
	je	.LBB1_16
# BB#13:                                #   in Loop: Header=BB1_11 Depth=2
	cmpl	$32, %eax
	je	.LBB1_17
# BB#14:                                #   in Loop: Header=BB1_11 Depth=2
	cmpl	$10, %eax
	jne	.LBB1_19
# BB#15:                                #   in Loop: Header=BB1_11 Depth=2
	movabsq	$4994009629030960700, %rax # imm = 0x454E494C57454E3C
	movq	%rax, _O_convert.spacetext(%rip)
	movw	$62, _O_convert.spacetext+8(%rip)
	jmp	.LBB1_18
.LBB1_16:                               #   in Loop: Header=BB1_11 Depth=2
	movw	$62, _O_convert.spacetext+4(%rip)
	movl	$1111577660, _O_convert.spacetext(%rip) # imm = 0x4241543C
	jmp	.LBB1_18
.LBB1_17:                               #   in Loop: Header=BB1_11 Depth=2
	movabsq	$17527603716969276, %rax # imm = 0x3E45434150533C
	movq	%rax, _O_convert.spacetext(%rip)
.LBB1_18:                               # %_O_convert.exit.i167
                                        #   in Loop: Header=BB1_11 Depth=2
	movl	$_O_convert.spacetext, %ebx
.LBB1_19:                               # %_O_convert.exit.i167
                                        #   in Loop: Header=BB1_11 Depth=2
	movslq	(%r14), %rax
	movl	_L_atlindex(,%rax,4), %ecx
	incl	%ecx
	movl	4(%r14), %r8d
	incl	%r8d
	movl	$_O_get_text.buf, %edi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	sprintf
	movl	$.L.str.7, %edi
	movl	$_O_get_text.buf, %esi
	xorl	%eax, %eax
	callq	printf
	incq	%rbp
	cmpq	%r12, %rbp
	jl	.LBB1_11
	jmp	.LBB1_98
	.p2align	4, 0x90
.LBB1_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_21 Depth 2
                                        #     Child Loop BB1_7 Depth 2
                                        #     Child Loop BB1_11 Depth 2
                                        #     Child Loop BB1_96 Depth 2
                                        #     Child Loop BB1_108 Depth 2
                                        #     Child Loop BB1_106 Depth 2
	movl	12(%r15), %r12d
	cmpl	$1, 8(%r15)
	jne	.LBB1_20
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	movl	%r12d, %edx
	movl	$1, %esi
.LBB1_6:                                #   in Loop: Header=BB1_4 Depth=1
	movl	16(%r15), %r14d
	movl	%r14d, %eax
	.p2align	4, 0x90
.LBB1_7:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %r13d
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB1_25
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=2
	cmpl	$1, 8(%r15)
	jne	.LBB1_30
# BB#9:                                 #   in Loop: Header=BB1_7 Depth=2
	leal	1(%r13), %eax
	cmpl	%edx, 12(%r15)
	je	.LBB1_7
	jmp	.LBB1_30
	.p2align	4, 0x90
.LBB1_20:                               # %.preheader189.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%r12d, %ecx
	.p2align	4, 0x90
.LBB1_21:                               # %.preheader189
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.LBB1_26
# BB#22:                                #   in Loop: Header=BB1_21 Depth=2
	movl	8(%r15), %edx
	cmpl	$2, %edx
	jne	.LBB1_27
# BB#23:                                #   in Loop: Header=BB1_21 Depth=2
	movl	12(%r15), %edx
	incl	%ecx
	cmpl	%ecx, %edx
	movl	%edx, %ecx
	je	.LBB1_21
	jmp	.LBB1_24
.LBB1_25:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB1_30
.LBB1_26:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB1_29
.LBB1_27:                               #   in Loop: Header=BB1_4 Depth=1
	cmpl	$1, %edx
	jne	.LBB1_24
# BB#28:                                #   in Loop: Header=BB1_4 Depth=1
	movl	12(%r15), %edx
	movl	12(%rax), %ecx
	movl	$3, %esi
	cmpl	%ecx, %edx
	movl	%edx, %ebp
	je	.LBB1_6
	jmp	.LBB1_29
	.p2align	4, 0x90
.LBB1_24:                               # %.critedge..loopexit190_crit_edge
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	12(%rax), %ecx
.LBB1_29:                               # %.loopexit190
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	16(%rax), %r14d
	movl	$2, %esi
	movl	$-1, %r13d
	movl	%ecx, %ebp
.LBB1_30:                               # %.critedge2
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	%esi, 8(%rsp)           # 4-byte Spill
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB1_32
# BB#31:                                # %.thread
                                        #   in Loop: Header=BB1_4 Depth=1
	leal	1(%r12), %esi
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	%r13d, %edx
	movl	%r14d, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%ebp, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%r12d, %ebx
	jmp	.LBB1_43
	.p2align	4, 0x90
.LBB1_32:                               #   in Loop: Header=BB1_4 Depth=1
	testl	%r12d, %r12d
	movl	%r12d, %ebx
	js	.LBB1_34
# BB#33:                                #   in Loop: Header=BB1_4 Depth=1
	movslq	%r12d, %rax
	movq	_K_ato(,%rax,8), %rax
	movl	(%rax), %ebx
.LBB1_34:                               #   in Loop: Header=BB1_4 Depth=1
	testl	%ebp, %ebp
	movl	%ebp, %eax
	js	.LBB1_36
# BB#35:                                #   in Loop: Header=BB1_4 Depth=1
	movslq	%ebp, %rax
	movq	_K_ato(,%rax,8), %rax
	movl	(%rax), %eax
.LBB1_36:                               #   in Loop: Header=BB1_4 Depth=1
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	movl	%r14d, %ecx
	js	.LBB1_38
# BB#37:                                #   in Loop: Header=BB1_4 Depth=1
	movslq	%r14d, %rax
	movq	_K_bto(,%rax,8), %rax
	movl	(%rax), %ecx
.LBB1_38:                               #   in Loop: Header=BB1_4 Depth=1
	testl	%r13d, %r13d
	movl	%r13d, %edx
	js	.LBB1_40
# BB#39:                                #   in Loop: Header=BB1_4 Depth=1
	movslq	%r13d, %rax
	movq	_K_bto(,%rax,8), %rax
	movl	(%rax), %edx
.LBB1_40:                               #   in Loop: Header=BB1_4 Depth=1
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movb	$1, %al
	movl	%eax, 4(%rsp)           # 4-byte Spill
	testl	%ebx, %ebx
	js	.LBB1_42
# BB#41:                                #   in Loop: Header=BB1_4 Depth=1
	movslq	%ebx, %rax
	movl	_L_atlindex(,%rax,4), %esi
	incl	%esi
	jmp	.LBB1_43
.LBB1_42:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_43:                               # %_O_con_line.exit
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	$3, %eax
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	movl	%r14d, 52(%rsp)         # 4-byte Spill
	je	.LBB1_49
# BB#44:                                # %_O_con_line.exit
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpl	$2, %eax
	je	.LBB1_53
# BB#45:                                # %_O_con_line.exit
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpl	$1, %eax
	jne	.LBB1_57
# BB#46:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	movl	%ebx, 28(%rsp)          # 4-byte Spill
	je	.LBB1_58
# BB#47:                                #   in Loop: Header=BB1_4 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %rbx
	testl	%ecx, %ecx
	js	.LBB1_61
# BB#48:                                #   in Loop: Header=BB1_4 Depth=1
	movslq	%ecx, %rax
	movl	_L_btlindex(,%rax,4), %esi
	incl	%esi
	jmp	.LBB1_62
	.p2align	4, 0x90
.LBB1_49:                               #   in Loop: Header=BB1_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	%ebx, %eax
	jle	.LBB1_73
# BB#50:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB1_59
# BB#51:                                #   in Loop: Header=BB1_4 Depth=1
	testl	%eax, %eax
	js	.LBB1_71
# BB#52:                                #   in Loop: Header=BB1_4 Depth=1
	cltq
	movl	_L_atlindex(,%rax,4), %esi
	incl	%esi
	jmp	.LBB1_72
	.p2align	4, 0x90
.LBB1_53:                               #   in Loop: Header=BB1_4 Depth=1
	cmpl	%ebx, 32(%rsp)          # 4-byte Folded Reload
	jle	.LBB1_85
# BB#54:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	je	.LBB1_60
# BB#55:                                #   in Loop: Header=BB1_4 Depth=1
	testl	%eax, %eax
	js	.LBB1_83
# BB#56:                                #   in Loop: Header=BB1_4 Depth=1
	cltq
	movl	_L_atlindex(,%rax,4), %esi
	incl	%esi
	jmp	.LBB1_84
	.p2align	4, 0x90
.LBB1_57:                               #   in Loop: Header=BB1_4 Depth=1
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	jmp	.LBB1_91
.LBB1_58:                               #   in Loop: Header=BB1_4 Depth=1
	movq	%r12, %rbx
	movq	16(%rsp), %r12          # 8-byte Reload
	leal	1(%r12), %esi
	jmp	.LBB1_63
.LBB1_59:                               #   in Loop: Header=BB1_4 Depth=1
	leal	1(%rax), %esi
	jmp	.LBB1_72
.LBB1_60:                               #   in Loop: Header=BB1_4 Depth=1
	leal	1(%rax), %esi
	jmp	.LBB1_84
.LBB1_61:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%esi, %esi
.LBB1_62:                               # %_O_con_line.exit158
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%rcx, %r12
.LBB1_63:                               # %_O_con_line.exit158
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	%r12d, 40(%rsp)         # 4-byte Folded Reload
	movq	%rbx, %r12
	movl	28(%rsp), %ebx          # 4-byte Reload
	jle	.LBB1_70
# BB#64:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB1_67
# BB#65:                                #   in Loop: Header=BB1_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jns	.LBB1_81
	jmp	.LBB1_66
.LBB1_67:                               #   in Loop: Header=BB1_4 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	jmp	.LBB1_68
.LBB1_71:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%esi, %esi
.LBB1_72:                               # %_O_con_line.exit171
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_73:                               #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ebx, 28(%rsp)          # 4-byte Spill
	je	.LBB1_76
# BB#74:                                #   in Loop: Header=BB1_4 Depth=1
	testl	%ecx, %ecx
	js	.LBB1_77
# BB#75:                                #   in Loop: Header=BB1_4 Depth=1
	movslq	%ecx, %rax
	movl	_L_btlindex(,%rax,4), %esi
	incl	%esi
	jmp	.LBB1_78
.LBB1_76:                               #   in Loop: Header=BB1_4 Depth=1
	leal	1(%rcx), %esi
	jmp	.LBB1_78
.LBB1_77:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_78:                               # %_O_con_line.exit173
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	%rcx, %rbx
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	%ebx, %eax
	movl	28(%rsp), %ebx          # 4-byte Reload
	jle	.LBB1_70
# BB#79:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB1_68
# BB#80:                                #   in Loop: Header=BB1_4 Depth=1
	testl	%eax, %eax
	js	.LBB1_66
.LBB1_81:                               #   in Loop: Header=BB1_4 Depth=1
	cltq
	movl	_L_btlindex(,%rax,4), %esi
	incl	%esi
	jmp	.LBB1_69
.LBB1_68:                               # %_O_con_line.exit161
                                        #   in Loop: Header=BB1_4 Depth=1
	leal	1(%rax), %esi
	jmp	.LBB1_69
.LBB1_66:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_69:                               # %_O_con_line.exit161
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_70:                               #   in Loop: Header=BB1_4 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	jmp	.LBB1_91
.LBB1_83:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%esi, %esi
.LBB1_84:                               # %_O_con_line.exit163
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_85:                               #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB1_88
# BB#86:                                #   in Loop: Header=BB1_4 Depth=1
	testl	%eax, %eax
	js	.LBB1_89
# BB#87:                                #   in Loop: Header=BB1_4 Depth=1
	cltq
	movl	_L_btlindex(,%rax,4), %esi
	incl	%esi
	jmp	.LBB1_90
.LBB1_88:                               #   in Loop: Header=BB1_4 Depth=1
	leal	1(%rax), %esi
	jmp	.LBB1_90
.LBB1_89:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_90:                               # %_O_con_line.exit165
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_91:                               #   in Loop: Header=BB1_4 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	orl	$1, %eax
	cmpl	$3, %eax
	jne	.LBB1_98
# BB#92:                                #   in Loop: Header=BB1_4 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB1_97
# BB#93:                                # %.preheader187
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpl	32(%rsp), %ebx          # 4-byte Folded Reload
	jg	.LBB1_98
# BB#94:                                # %.lr.ph
                                        #   in Loop: Header=BB1_4 Depth=1
	movslq	%ebx, %rbp
	movslq	32(%rsp), %r12          # 4-byte Folded Reload
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB1_10
# BB#95:                                # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	decq	%rbp
	.p2align	4, 0x90
.LBB1_96:                               # %.lr.ph.split.us
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	_L_atlindex+4(,%rbp,4), %rax
	movslq	_L_aclindex(,%rax,4), %rax
	movq	_L_al(,%rax,8), %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbp
	cmpq	%r12, %rbp
	jl	.LBB1_96
	jmp	.LBB1_98
	.p2align	4, 0x90
.LBB1_97:                               #   in Loop: Header=BB1_4 Depth=1
	xorl	%edx, %edx
	movl	%r12d, %edi
	movl	48(%rsp), %esi          # 4-byte Reload
	callq	_O_do_lines
.LBB1_98:                               # %.loopexit188
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	$3, %eax
	jne	.LBB1_101
# BB#99:                                # %.thread186
                                        #   in Loop: Header=BB1_4 Depth=1
	movl	$.Lstr, %edi
	callq	puts
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_103
	jmp	.LBB1_100
	.p2align	4, 0x90
.LBB1_101:                              #   in Loop: Header=BB1_4 Depth=1
	orl	$2, %eax
	cmpl	$3, %eax
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	jne	.LBB1_117
# BB#102:                               #   in Loop: Header=BB1_4 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB1_100
.LBB1_103:                              # %.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	cmpl	%ecx, %eax
	jg	.LBB1_117
# BB#104:                               # %.lr.ph205
                                        #   in Loop: Header=BB1_4 Depth=1
	movslq	%eax, %rbp
	movslq	%ecx, %r14
	cmpb	$0, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB1_107
# BB#105:                               # %.lr.ph205.split.us.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	decq	%rbp
	.p2align	4, 0x90
.LBB1_106:                              # %.lr.ph205.split.us
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	_L_btlindex+4(,%rbp,4), %rax
	movslq	_L_bclindex(,%rax,4), %rax
	movq	_L_bl(,%rax,8), %rsi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbp
	cmpq	%r14, %rbp
	jl	.LBB1_106
	jmp	.LBB1_117
	.p2align	4, 0x90
.LBB1_100:                              #   in Loop: Header=BB1_4 Depth=1
	movl	$1, %edx
	movl	52(%rsp), %edi          # 4-byte Reload
	movl	%r13d, %esi
	callq	_O_do_lines
	jmp	.LBB1_117
.LBB1_107:                              # %.lr.ph205.split.preheader
                                        #   in Loop: Header=BB1_4 Depth=1
	decq	%rbp
	.p2align	4, 0x90
.LBB1_108:                              # %.lr.ph205.split
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	_K_bto+8(,%rbp,8), %rbx
	movq	16(%rbx), %r12
	movq	%r12, %rdi
	callq	strlen
	cmpq	$1, %rax
	jne	.LBB1_116
# BB#109:                               #   in Loop: Header=BB1_108 Depth=2
	movsbl	(%r12), %eax
	cmpl	$9, %eax
	je	.LBB1_113
# BB#110:                               #   in Loop: Header=BB1_108 Depth=2
	cmpl	$32, %eax
	je	.LBB1_114
# BB#111:                               #   in Loop: Header=BB1_108 Depth=2
	cmpl	$10, %eax
	jne	.LBB1_116
# BB#112:                               #   in Loop: Header=BB1_108 Depth=2
	movabsq	$4994009629030960700, %rax # imm = 0x454E494C57454E3C
	movq	%rax, _O_convert.spacetext(%rip)
	movw	$62, _O_convert.spacetext+8(%rip)
	jmp	.LBB1_115
.LBB1_113:                              #   in Loop: Header=BB1_108 Depth=2
	movw	$62, _O_convert.spacetext+4(%rip)
	movl	$1111577660, _O_convert.spacetext(%rip) # imm = 0x4241543C
	jmp	.LBB1_115
.LBB1_114:                              #   in Loop: Header=BB1_108 Depth=2
	movabsq	$17527603716969276, %rax # imm = 0x3E45434150533C
	movq	%rax, _O_convert.spacetext(%rip)
.LBB1_115:                              # %_O_convert.exit.i
                                        #   in Loop: Header=BB1_108 Depth=2
	movl	$_O_convert.spacetext, %r12d
.LBB1_116:                              # %_O_convert.exit.i
                                        #   in Loop: Header=BB1_108 Depth=2
	movslq	(%rbx), %rax
	movl	_L_btlindex(,%rax,4), %ecx
	incl	%ecx
	movl	4(%rbx), %r8d
	incl	%r8d
	movl	$_O_get_text.buf, %edi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	sprintf
	movl	$.L.str.9, %edi
	movl	$_O_get_text.buf, %esi
	xorl	%eax, %eax
	callq	printf
	incq	%rbp
	cmpq	%r14, %rbp
	jl	.LBB1_108
	.p2align	4, 0x90
.LBB1_117:                              # %.backedge
                                        #   in Loop: Header=BB1_4 Depth=1
	testq	%r15, %r15
	movl	48(%rsp), %ebp          # 4-byte Reload
	jne	.LBB1_4
.LBB1_118:                              # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	O_output, .Lfunc_end1-O_output
	.cfi_endproc

	.p2align	4, 0x90
	.type	_O_do_lines,@function
_O_do_lines:                            # @_O_do_lines
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$1112, %rsp             # imm = 0x458
.Lcfi19:
	.cfi_def_cfa_offset 1168
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edx, 12(%rsp)          # 4-byte Spill
	cmpl	%esi, %edi
	jg	.LBB2_32
# BB#1:                                 # %.lr.ph75
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movl	$_K_bto, %eax
	movl	$_K_ato, %r13d
	cmovneq	%rax, %r13
	movl	$_L_btlindex, %eax
	movl	$_L_atlindex, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	$_L_bclindex, %eax
	movl	$_L_aclindex, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	$_L_bl, %eax
	movl	$_L_al, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movslq	%edi, %r15
	movslq	%esi, %r14
	leaq	-1(%r15), %rcx
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
                                        #     Child Loop BB2_16 Depth 2
                                        #     Child Loop BB2_20 Depth 2
                                        #     Child Loop BB2_29 Depth 2
	movq	(%r13,%r15,8), %rbp
	movl	(%rbp), %r12d
	cmpl	%r12d, %eax
	je	.LBB2_31
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movslq	%r12d, %rbx
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB2_4
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.15, %edi
	jmp	.LBB2_6
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.14, %edi
.LBB2_6:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%eax, %eax
	callq	printf
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	(%rax,%rbx,4), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx,%rax,4), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%rax,8), %rsi
	leaq	80(%rsp), %rdi
	callq	strcpy
	movq	%rbp, (%rsp)            # 8-byte Spill
	movl	4(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.LBB2_9
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	leaq	80(%rsp), %rbp
	.p2align	4, 0x90
.LBB2_8:                                # %.lr.ph.i
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rbp), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%rbp
	decq	%rbx
	jne	.LBB2_8
.LBB2_9:                                # %_O_pchars.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movb	_O_need_init(%rip), %al
	testb	%al, %al
	jne	.LBB2_15
# BB#10:                                #   in Loop: Header=BB2_2 Depth=1
	movl	$1, %edi
	callq	isatty
	testl	%eax, %eax
	je	.LBB2_14
# BB#11:                                #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.16, %edi
	callq	getenv
	movq	%rax, _O_st_tmp(%rip)
	testq	%rax, %rax
	je	.LBB2_12
# BB#13:                                #   in Loop: Header=BB2_2 Depth=1
	leaq	48(%rsp), %rdi
	movq	%rax, %rsi
	callq	strcpy
	jmp	.LBB2_14
.LBB2_12:                               #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	Z_complain
.LBB2_14:                               # %_O_st_init.exit.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movb	$1, _O_need_init(%rip)
.LBB2_15:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %eax
	.p2align	4, 0x90
.LBB2_16:                               # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r13,%rcx,8), %rdx
	cmpl	(%rdx), %r12d
	jne	.LBB2_18
# BB#17:                                # %_O_start_standout.exit
                                        #   in Loop: Header=BB2_16 Depth=2
	incl	%eax
	incq	%rcx
	cmpq	%r14, %rcx
	jl	.LBB2_16
.LBB2_18:                               # %.critedge
                                        #   in Loop: Header=BB2_2 Depth=1
	cltq
	movq	-8(%r13,%rax,8), %rax
	movl	4(%rax), %ebx
	movq	16(%rax), %rdi
	callq	strlen
	movq	%rax, %rbp
	addq	%rbx, %rbp
	movq	(%rsp), %rax            # 8-byte Reload
	movslq	4(%rax), %rax
	cmpl	%ebp, %eax
	jge	.LBB2_27
# BB#19:                                # %.lr.ph.preheader.i53
                                        #   in Loop: Header=BB2_2 Depth=1
	leaq	80(%rsp,%rax), %rbx
	movq	%rbp, (%rsp)            # 8-byte Spill
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
	subl	%eax, %ebp
	.p2align	4, 0x90
.LBB2_20:                               # %.lr.ph.i58
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rbx), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%rbx
	decl	%ebp
	jne	.LBB2_20
# BB#21:                                # %_O_pchars.exit59
                                        #   in Loop: Header=BB2_2 Depth=1
	movb	_O_need_init(%rip), %al
	testb	%al, %al
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB2_27
# BB#22:                                #   in Loop: Header=BB2_2 Depth=1
	movl	$1, %edi
	callq	isatty
	testl	%eax, %eax
	je	.LBB2_26
# BB#23:                                #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.16, %edi
	callq	getenv
	movq	%rax, _O_st_tmp(%rip)
	testq	%rax, %rax
	je	.LBB2_24
# BB#25:                                #   in Loop: Header=BB2_2 Depth=1
	leaq	48(%rsp), %rdi
	movq	%rax, %rsi
	callq	strcpy
	jmp	.LBB2_26
.LBB2_24:                               #   in Loop: Header=BB2_2 Depth=1
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	Z_complain
.LBB2_26:                               # %_O_st_init.exit.i61
                                        #   in Loop: Header=BB2_2 Depth=1
	movb	$1, _O_need_init(%rip)
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB2_27:                               # %_O_end_standout.exit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%r15, (%rsp)            # 8-byte Spill
	leaq	80(%rsp), %rdi
	callq	strlen
	movq	%rax, %rbx
	cmpl	%ebx, %ebp
	jge	.LBB2_30
# BB#28:                                # %.lr.ph.preheader.i62
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	%ebp, %rax
	leaq	80(%rsp,%rax), %r15
	subl	%ebp, %ebx
	.p2align	4, 0x90
.LBB2_29:                               # %.lr.ph.i67
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%r15), %edi
	movq	stdout(%rip), %rsi
	callq	_IO_putc
	incq	%r15
	decl	%ebx
	jne	.LBB2_29
.LBB2_30:                               # %_O_pchars.exit68
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	%r12d, %eax
	movq	(%rsp), %r15            # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB2_31:                               #   in Loop: Header=BB2_2 Depth=1
	incq	%rcx
	cmpq	%r14, %r15
	leaq	1(%r15), %r15
	jl	.LBB2_2
.LBB2_32:                               # %._crit_edge
	addq	$1112, %rsp             # imm = 0x458
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_O_do_lines, .Lfunc_end2-_O_do_lines
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"a%d"
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	",%d"
	.size	.L.str.2, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"d%d\n"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"c%d"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"type in O_output wasn't set\n"
	.size	.L.str.6, 29

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"< %s"
	.size	.L.str.7, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"> %s"
	.size	.L.str.9, 5

	.type	_O_get_text.buf,@object # @_O_get_text.buf
	.local	_O_get_text.buf
	.comm	_O_get_text.buf,2048,16
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%s -- line %d, character %d\n"
	.size	.L.str.10, 29

	.type	_O_convert.spacetext,@object # @_O_convert.spacetext
	.local	_O_convert.spacetext
	.comm	_O_convert.spacetext,20,16
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"<NEWLINE>"
	.size	.L.str.11, 10

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"<TAB>"
	.size	.L.str.12, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"< "
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"> "
	.size	.L.str.15, 3

	.type	_O_need_init,@object    # @_O_need_init
	.local	_O_need_init
	.comm	_O_need_init,1,4
	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"TERM"
	.size	.L.str.16, 5

	.type	_O_st_tmp,@object       # @_O_st_tmp
	.local	_O_st_tmp
	.comm	_O_st_tmp,8,8
	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"can't find TERM entry in environment\n"
	.size	.L.str.17, 38

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"---"
	.size	.Lstr, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
