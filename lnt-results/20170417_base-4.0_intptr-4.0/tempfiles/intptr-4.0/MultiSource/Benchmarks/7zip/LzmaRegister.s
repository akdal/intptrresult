	.text
	.file	"LzmaRegister.bc"
	.p2align	4, 0x90
	.type	_ZL11CreateCodecv,@function
_ZL11CreateCodecv:                      # @_ZL11CreateCodecv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$280, %edi              # imm = 0x118
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NLzma8CDecoderC1Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZL11CreateCodecv, .Lfunc_end0-_ZL11CreateCodecv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL14CreateCodecOutv,@function
_ZL14CreateCodecOutv:                   # @_ZL14CreateCodecOutv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NLzma8CEncoderC1Ev
.Ltmp4:
# BB#1:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_2:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZL14CreateCodecOutv, .Lfunc_end1-_ZL14CreateCodecOutv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_LzmaRegister.ii,@function
_GLOBAL__sub_I_LzmaRegister.ii:         # @_GLOBAL__sub_I_LzmaRegister.ii
	.cfi_startproc
# BB#0:
	movl	$_ZL11g_CodecInfo, %edi
	jmp	_Z13RegisterCodecPK10CCodecInfo # TAILCALL
.Lfunc_end2:
	.size	_GLOBAL__sub_I_LzmaRegister.ii, .Lfunc_end2-_GLOBAL__sub_I_LzmaRegister.ii
	.cfi_endproc

	.type	_ZL11g_CodecInfo,@object # @_ZL11g_CodecInfo
	.data
	.p2align	3
_ZL11g_CodecInfo:
	.quad	_ZL11CreateCodecv
	.quad	_ZL14CreateCodecOutv
	.quad	196865                  # 0x30101
	.quad	.L.str
	.long	1                       # 0x1
	.byte	0                       # 0x0
	.zero	3
	.size	_ZL11g_CodecInfo, 40

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	76                      # 0x4c
	.long	90                      # 0x5a
	.long	77                      # 0x4d
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str, 20

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_LzmaRegister.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
