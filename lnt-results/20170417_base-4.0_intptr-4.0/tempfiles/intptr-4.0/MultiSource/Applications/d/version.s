	.text
	.file	"version.bc"
	.globl	d_version
	.p2align	4, 0x90
	.type	d_version,@function
d_version:                              # @d_version
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str, %esi
	movl	$1, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	sprintf
	cltq
	addq	%rax, %rbx
	movl	$.L.str.1, %esi
	movl	$5725, %edx             # imm = 0x165D
	xorl	%eax, %eax
	movq	%rbx, %rdi
	popq	%rbx
	jmp	sprintf                 # TAILCALL
.Lfunc_end0:
	.size	d_version, .Lfunc_end0-d_version
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d.%d"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	".%d"
	.size	.L.str.1, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
