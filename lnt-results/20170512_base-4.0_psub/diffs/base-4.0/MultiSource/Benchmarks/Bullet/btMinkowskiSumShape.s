	.text
	.file	"btMinkowskiSumShape.bc"
	.globl	_ZN19btMinkowskiSumShapeC2EPK13btConvexShapeS2_
	.p2align	4, 0x90
	.type	_ZN19btMinkowskiSumShapeC2EPK13btConvexShapeS2_,@function
_ZN19btMinkowskiSumShapeC2EPK13btConvexShapeS2_: # @_ZN19btMinkowskiSumShapeC2EPK13btConvexShapeS2_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV19btMinkowskiSumShape+16, (%rbx)
	movq	%r15, 192(%rbx)
	movq	%r14, 200(%rbx)
	movl	$16, 8(%rbx)
	movl	$1065353216, 64(%rbx)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 68(%rbx)
	movl	$1065353216, 84(%rbx)   # imm = 0x3F800000
	movups	%xmm0, 88(%rbx)
	movl	$1065353216, 104(%rbx)  # imm = 0x3F800000
	movups	%xmm0, 108(%rbx)
	movl	$0, 124(%rbx)
	movl	$1065353216, 128(%rbx)  # imm = 0x3F800000
	movups	%xmm0, 132(%rbx)
	movl	$1065353216, 148(%rbx)  # imm = 0x3F800000
	movups	%xmm0, 152(%rbx)
	movl	$1065353216, 168(%rbx)  # imm = 0x3F800000
	movups	%xmm0, 172(%rbx)
	movl	$0, 188(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN19btMinkowskiSumShapeC2EPK13btConvexShapeS2_, .Lfunc_end0-_ZN19btMinkowskiSumShapeC2EPK13btConvexShapeS2_
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZNK19btMinkowskiSumShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK19btMinkowskiSumShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK19btMinkowskiSumShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK19btMinkowskiSumShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 80
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	192(%rbx), %rdi
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movsd	64(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movsd	80(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm4, %xmm5
	movsd	96(%rbx), %xmm3         # xmm3 = mem[0],zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm5, %xmm4
	mulss	72(%rbx), %xmm0
	mulss	88(%rbx), %xmm1
	addss	%xmm0, %xmm1
	mulss	104(%rbx), %xmm2
	addss	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	leaq	16(%rsp), %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	80(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	64(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	68(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	84(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm0, %xmm6
	addps	%xmm2, %xmm6
	movss	104(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	88(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	72(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	mulps	%xmm1, %xmm4
	addps	%xmm6, %xmm4
	movsd	112(%rbx), %xmm1        # xmm1 = mem[0],zero
	addps	%xmm4, %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movss	100(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	addss	%xmm1, %xmm5
	addss	120(%rbx), %xmm5
	movss	%xmm5, 12(%rsp)         # 4-byte Spill
	movq	200(%rbx), %rdi
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	movd	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movd	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movdqa	.LCPI2_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm2, %xmm0
	movdqa	%xmm1, %xmm3
	pxor	%xmm2, %xmm3
	movd	8(%r14), %xmm4          # xmm4 = mem[0],zero,zero,zero
	pxor	%xmm4, %xmm2
	movsd	128(%rbx), %xmm5        # xmm5 = mem[0],zero
	pshufd	$224, %xmm0, %xmm6      # xmm6 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm6
	movsd	144(%rbx), %xmm5        # xmm5 = mem[0],zero
	pshufd	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm6, %xmm3
	movsd	160(%rbx), %xmm5        # xmm5 = mem[0],zero
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	addps	%xmm3, %xmm2
	movss	136(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	152(%rbx), %xmm1
	subss	%xmm1, %xmm3
	mulss	168(%rbx), %xmm4
	subss	%xmm4, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm2, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	leaq	16(%rsp), %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	movss	160(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	144(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	128(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	132(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	148(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm0, %xmm6
	addps	%xmm2, %xmm6
	movss	168(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	152(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	136(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm1, %xmm5
	addps	%xmm6, %xmm5
	movsd	176(%rbx), %xmm1        # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	movss	164(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm2
	addss	184(%rbx), %xmm2
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	subps	%xmm1, %xmm0
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	_ZNK19btMinkowskiSumShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end2-_ZNK19btMinkowskiSumShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.globl	_ZNK19btMinkowskiSumShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK19btMinkowskiSumShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK19btMinkowskiSumShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK19btMinkowskiSumShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r12, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	testl	%ecx, %ecx
	jle	.LBB3_3
# BB#1:                                 # %.lr.ph
	movl	%ecx, %r12d
	addq	$8, %rbx
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	*104(%rax)
	movlps	%xmm0, -8(%rbx)
	movlps	%xmm1, (%rbx)
	addq	$16, %r14
	addq	$16, %rbx
	decq	%r12
	jne	.LBB3_2
.LBB3_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZNK19btMinkowskiSumShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end3-_ZNK19btMinkowskiSumShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.globl	_ZNK19btMinkowskiSumShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK19btMinkowskiSumShape9getMarginEv,@function
_ZNK19btMinkowskiSumShape9getMarginEv:  # @_ZNK19btMinkowskiSumShape9getMarginEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	192(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	200(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	addss	12(%rsp), %xmm0         # 4-byte Folded Reload
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZNK19btMinkowskiSumShape9getMarginEv, .Lfunc_end4-_ZNK19btMinkowskiSumShape9getMarginEv
	.cfi_endproc

	.globl	_ZNK19btMinkowskiSumShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK19btMinkowskiSumShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK19btMinkowskiSumShape21calculateLocalInertiaEfR9btVector3: # @_ZNK19btMinkowskiSumShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end5:
	.size	_ZNK19btMinkowskiSumShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end5-_ZNK19btMinkowskiSumShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZN19btMinkowskiSumShapeD0Ev,"axG",@progbits,_ZN19btMinkowskiSumShapeD0Ev,comdat
	.weak	_ZN19btMinkowskiSumShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN19btMinkowskiSumShapeD0Ev,@function
_ZN19btMinkowskiSumShapeD0Ev:           # @_ZN19btMinkowskiSumShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB6_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN19btMinkowskiSumShapeD0Ev, .Lfunc_end6-_ZN19btMinkowskiSumShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,"axG",@progbits,_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,comdat
	.weak	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end7:
	.size	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end7-_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape15getLocalScalingEv,"axG",@progbits,_ZNK21btConvexInternalShape15getLocalScalingEv,comdat
	.weak	_ZNK21btConvexInternalShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape15getLocalScalingEv,@function
_ZNK21btConvexInternalShape15getLocalScalingEv: # @_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	24(%rdi), %rax
	retq
.Lfunc_end8:
	.size	_ZNK21btConvexInternalShape15getLocalScalingEv, .Lfunc_end8-_ZNK21btConvexInternalShape15getLocalScalingEv
	.cfi_endproc

	.section	.text._ZNK19btMinkowskiSumShape7getNameEv,"axG",@progbits,_ZNK19btMinkowskiSumShape7getNameEv,comdat
	.weak	_ZNK19btMinkowskiSumShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK19btMinkowskiSumShape7getNameEv,@function
_ZNK19btMinkowskiSumShape7getNameEv:    # @_ZNK19btMinkowskiSumShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end9:
	.size	_ZNK19btMinkowskiSumShape7getNameEv, .Lfunc_end9-_ZNK19btMinkowskiSumShape7getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end10-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end11-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end12:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end12-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.type	_ZTV19btMinkowskiSumShape,@object # @_ZTV19btMinkowskiSumShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV19btMinkowskiSumShape
	.p2align	3
_ZTV19btMinkowskiSumShape:
	.quad	0
	.quad	_ZTI19btMinkowskiSumShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN19btMinkowskiSumShapeD0Ev
	.quad	_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btConvexInternalShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btConvexInternalShape15getLocalScalingEv
	.quad	_ZNK19btMinkowskiSumShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK19btMinkowskiSumShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK19btMinkowskiSumShape9getMarginEv
	.quad	_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK19btMinkowskiSumShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK19btMinkowskiSumShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV19btMinkowskiSumShape, 160

	.type	_ZTS19btMinkowskiSumShape,@object # @_ZTS19btMinkowskiSumShape
	.globl	_ZTS19btMinkowskiSumShape
	.p2align	4
_ZTS19btMinkowskiSumShape:
	.asciz	"19btMinkowskiSumShape"
	.size	_ZTS19btMinkowskiSumShape, 22

	.type	_ZTI19btMinkowskiSumShape,@object # @_ZTI19btMinkowskiSumShape
	.globl	_ZTI19btMinkowskiSumShape
	.p2align	4
_ZTI19btMinkowskiSumShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19btMinkowskiSumShape
	.quad	_ZTI21btConvexInternalShape
	.size	_ZTI19btMinkowskiSumShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"MinkowskiSum"
	.size	.L.str, 13


	.globl	_ZN19btMinkowskiSumShapeC1EPK13btConvexShapeS2_
	.type	_ZN19btMinkowskiSumShapeC1EPK13btConvexShapeS2_,@function
_ZN19btMinkowskiSumShapeC1EPK13btConvexShapeS2_ = _ZN19btMinkowskiSumShapeC2EPK13btConvexShapeS2_
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
