	.text
	.file	"cholesky.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4652218415073722368     # double 1024
.LCPI6_1:
	.quad	4607182418800017408     # double 1
.LCPI6_3:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_84
# BB#1:
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_84
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8192, %edx             # imm = 0x2000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_84
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_84
# BB#4:                                 # %polybench_alloc_data.exit26
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_84
# BB#5:                                 # %polybench_alloc_data.exit26
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_84
# BB#6:                                 # %polybench_alloc_data.exit28
	xorl	%eax, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
	leal	1024(%rax), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	%xmm0, (%r14,%rax,8)
	movq	%rcx, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_8:                                #   Parent Loop BB6_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1024(%rsi), %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	movsd	%xmm0, -8(%r15,%rdx,8)
	movsd	%xmm0, -8(%rbx,%rdx,8)
	leal	1025(%rsi), %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	movsd	%xmm0, (%r15,%rdx,8)
	movsd	%xmm0, (%rbx,%rdx,8)
	addq	$2, %rsi
	addq	$2, %rdx
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB6_8
# BB#9:                                 #   in Loop: Header=BB6_7 Depth=1
	incq	%rax
	addq	$1024, %rcx             # imm = 0x400
	cmpq	$1024, %rax             # imm = 0x400
	jne	.LBB6_7
# BB#10:                                # %init_array.exit.preheader
	leaq	8192(%r15), %r9
	leaq	8(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r10d
	xorl	%r12d, %r12d
	movsd	.LCPI6_0(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movq	%r15, %r13
	jmp	.LBB6_14
	.p2align	4, 0x90
.LBB6_11:                               # %.loopexit.i
                                        #   in Loop: Header=BB6_14 Depth=1
	cmpq	$1024, %r8              # imm = 0x400
	je	.LBB6_43
# BB#12:                                #   in Loop: Header=BB6_14 Depth=1
	incq	%r10
	jmp	.LBB6_13
.LBB6_28:                               # %.lr.ph11.split.i.preheader
                                        #   in Loop: Header=BB6_14 Depth=1
	mulsd	(%rbp,%r10,8), %xmm0
	movq	%r10, %rax
	shlq	$13, %rax
	addq	%r15, %rax
	movsd	%xmm0, (%rax,%r12,8)
	leaq	1(%r10), %rcx
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_30
# BB#29:                                #   in Loop: Header=BB6_14 Depth=1
	movl	$1024, %r10d            # imm = 0x400
	jmp	.LBB6_13
.LBB6_30:                               # %.lr.ph11.split..lr.ph11.split_crit_edge.i.preheader
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	$1023, %eax             # imm = 0x3FF
	subl	%r10d, %eax
	testb	$1, %al
	je	.LBB6_32
# BB#31:                                # %.lr.ph11.split..lr.ph11.split_crit_edge.i.prol
                                        #   in Loop: Header=BB6_14 Depth=1
	movsd	(%r14,%r12,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rbp,%rcx,8), %xmm0
	shlq	$13, %rcx
	addq	%r15, %rcx
	movsd	%xmm0, (%rcx,%r12,8)
	leaq	2(%r10), %rcx
.LBB6_32:                               # %.lr.ph11.split..lr.ph11.split_crit_edge.i.prol.loopexit
                                        #   in Loop: Header=BB6_14 Depth=1
	cmpq	$1022, %r10             # imm = 0x3FE
	je	.LBB6_11
# BB#33:                                # %.lr.ph11.split..lr.ph11.split_crit_edge.i.preheader.new
                                        #   in Loop: Header=BB6_14 Depth=1
	movq	%rcx, %rax
	shlq	$13, %rax
	addq	%r9, %rax
	.p2align	4, 0x90
.LBB6_34:                               # %.lr.ph11.split..lr.ph11.split_crit_edge.i
                                        #   Parent Loop BB6_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r14,%r12,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r13,%rcx,8), %xmm0
	movsd	%xmm0, -8192(%rax)
	movsd	(%r14,%r12,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	8(%r13,%rcx,8), %xmm0
	movsd	%xmm0, (%rax)
	addq	$2, %rcx
	addq	$16384, %rax            # imm = 0x4000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_34
	jmp	.LBB6_11
	.p2align	4, 0x90
.LBB6_13:                               # %init_array.exit.backedge
                                        #   in Loop: Header=BB6_14 Depth=1
	addq	$8192, %r13             # imm = 0x2000
	addq	$8, %r9
	movq	%r8, %r12
.LBB6_14:                               # %init_array.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
                                        #     Child Loop BB6_20 Depth 2
                                        #     Child Loop BB6_34 Depth 2
                                        #     Child Loop BB6_26 Depth 2
                                        #       Child Loop BB6_38 Depth 3
	movq	%r12, %rbp
	shlq	$13, %rbp
	addq	%r15, %rbp
	movsd	(%rbp,%r12,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	testq	%r12, %r12
	jle	.LBB6_21
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB6_14 Depth=1
	leaq	-1(%r12), %rcx
	testb	$3, %r12b
	je	.LBB6_16
# BB#17:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	%r12d, %edx
	andl	$3, %edx
	movq	%r13, %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_18:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB6_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	incq	%rax
	addq	$8, %rsi
	cmpq	%rax, %rdx
	jne	.LBB6_18
	jmp	.LBB6_19
	.p2align	4, 0x90
.LBB6_16:                               #   in Loop: Header=BB6_14 Depth=1
	xorl	%eax, %eax
.LBB6_19:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB6_14 Depth=1
	cmpq	$3, %rcx
	jb	.LBB6_21
	.p2align	4, 0x90
.LBB6_20:                               # %.lr.ph.i
                                        #   Parent Loop BB6_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r13,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	movsd	8(%r13,%rax,8), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	movsd	16(%r13,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	movsd	24(%r13,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	addq	$4, %rax
	cmpq	%rax, %r12
	jne	.LBB6_20
.LBB6_21:                               # %._crit_edge.i
                                        #   in Loop: Header=BB6_14 Depth=1
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_23
# BB#22:                                # %call.sqrt
                                        #   in Loop: Header=BB6_14 Depth=1
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	callq	sqrt
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movsd	.LCPI6_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI6_0(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_23:                               # %._crit_edge.i.split
                                        #   in Loop: Header=BB6_14 Depth=1
	movapd	%xmm4, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm0, (%r14,%r12,8)
	leaq	1(%r12), %r8
	cmpq	$1023, %r8              # imm = 0x3FF
	jg	.LBB6_11
# BB#24:                                # %.lr.ph11.i
                                        #   in Loop: Header=BB6_14 Depth=1
	testq	%r12, %r12
	jle	.LBB6_28
# BB#25:                                # %.lr.ph11.split.us.i.preheader
                                        #   in Loop: Header=BB6_14 Depth=1
	movl	%r12d, %ecx
	andl	$1, %ecx
	movq	%r10, %rdx
	shlq	$13, %rdx
	addq	16(%rsp), %rdx          # 8-byte Folded Reload
	movq	%r10, %rsi
	.p2align	4, 0x90
.LBB6_26:                               # %.lr.ph11.split.us.i
                                        #   Parent Loop BB6_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_38 Depth 3
	testq	%rcx, %rcx
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	jne	.LBB6_35
# BB#27:                                #   in Loop: Header=BB6_26 Depth=2
	xorl	%edi, %edi
	cmpq	$1, %r12
	jne	.LBB6_37
	jmp	.LBB6_39
	.p2align	4, 0x90
.LBB6_35:                               #   in Loop: Header=BB6_26 Depth=2
	movq	%rsi, %rax
	shlq	$13, %rax
	movsd	(%r15,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rbp), %xmm1
	subsd	%xmm1, %xmm0
	movl	$1, %edi
	cmpq	$1, %r12
	je	.LBB6_39
.LBB6_37:                               # %.lr.ph11.split.us.i.new
                                        #   in Loop: Header=BB6_26 Depth=2
	leaq	(%rdx,%rdi,8), %rax
	.p2align	4, 0x90
.LBB6_38:                               #   Parent Loop BB6_14 Depth=1
                                        #     Parent Loop BB6_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	mulsd	(%r13,%rdi,8), %xmm1
	subsd	%xmm1, %xmm0
	mulsd	8(%r13,%rdi,8), %xmm2
	subsd	%xmm2, %xmm0
	addq	$2, %rdi
	addq	$16, %rax
	cmpq	%rdi, %r12
	jne	.LBB6_38
.LBB6_39:                               # %._crit_edge7.us.i
                                        #   in Loop: Header=BB6_26 Depth=2
	mulsd	(%r14,%r12,8), %xmm0
	movq	%rsi, %rax
	shlq	$13, %rax
	addq	%r15, %rax
	movsd	%xmm0, (%rax,%r12,8)
	incq	%rsi
	addq	$8192, %rdx             # imm = 0x2000
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB6_26
	jmp	.LBB6_11
.LBB6_43:                               # %kernel_cholesky.exit.preheader
	leaq	8192(%rbx), %r9
	leaq	8(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r10d
	xorl	%r12d, %r12d
	movq	%rbx, %r13
	jmp	.LBB6_44
	.p2align	4, 0x90
.LBB6_40:                               # %.loopexit.i34
                                        #   in Loop: Header=BB6_44 Depth=1
	cmpq	$1024, %r8              # imm = 0x400
	je	.LBB6_70
# BB#41:                                #   in Loop: Header=BB6_44 Depth=1
	incq	%r10
	jmp	.LBB6_42
.LBB6_58:                               # %.lr.ph11.split.i58.preheader
                                        #   in Loop: Header=BB6_44 Depth=1
	mulsd	(%rbp,%r10,8), %xmm0
	movq	%r10, %rax
	shlq	$13, %rax
	addq	%rbx, %rax
	movsd	%xmm0, (%rax,%r12,8)
	leaq	1(%r10), %rcx
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_60
# BB#59:                                #   in Loop: Header=BB6_44 Depth=1
	movl	$1024, %r10d            # imm = 0x400
	jmp	.LBB6_42
.LBB6_60:                               # %.lr.ph11.split..lr.ph11.split_crit_edge.i60.preheader
                                        #   in Loop: Header=BB6_44 Depth=1
	movl	$1023, %eax             # imm = 0x3FF
	subl	%r10d, %eax
	testb	$1, %al
	je	.LBB6_62
# BB#61:                                # %.lr.ph11.split..lr.ph11.split_crit_edge.i60.prol
                                        #   in Loop: Header=BB6_44 Depth=1
	movsd	(%r14,%r12,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rbp,%rcx,8), %xmm0
	shlq	$13, %rcx
	addq	%rbx, %rcx
	movsd	%xmm0, (%rcx,%r12,8)
	leaq	2(%r10), %rcx
.LBB6_62:                               # %.lr.ph11.split..lr.ph11.split_crit_edge.i60.prol.loopexit
                                        #   in Loop: Header=BB6_44 Depth=1
	cmpq	$1022, %r10             # imm = 0x3FE
	je	.LBB6_40
# BB#63:                                # %.lr.ph11.split..lr.ph11.split_crit_edge.i60.preheader.new
                                        #   in Loop: Header=BB6_44 Depth=1
	movq	%rcx, %rax
	shlq	$13, %rax
	addq	%r9, %rax
	.p2align	4, 0x90
.LBB6_64:                               # %.lr.ph11.split..lr.ph11.split_crit_edge.i60
                                        #   Parent Loop BB6_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r14,%r12,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r13,%rcx,8), %xmm0
	movsd	%xmm0, -8192(%rax)
	movsd	(%r14,%r12,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	8(%r13,%rcx,8), %xmm0
	movsd	%xmm0, (%rax)
	addq	$2, %rcx
	addq	$16384, %rax            # imm = 0x4000
	cmpq	$1024, %rcx             # imm = 0x400
	jne	.LBB6_64
	jmp	.LBB6_40
	.p2align	4, 0x90
.LBB6_42:                               # %kernel_cholesky.exit.backedge
                                        #   in Loop: Header=BB6_44 Depth=1
	addq	$8192, %r13             # imm = 0x2000
	addq	$8, %r9
	movq	%r8, %r12
.LBB6_44:                               # %kernel_cholesky.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_48 Depth 2
                                        #     Child Loop BB6_50 Depth 2
                                        #     Child Loop BB6_64 Depth 2
                                        #     Child Loop BB6_56 Depth 2
                                        #       Child Loop BB6_68 Depth 3
	movq	%r12, %rbp
	shlq	$13, %rbp
	addq	%rbx, %rbp
	movsd	(%rbp,%r12,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	testq	%r12, %r12
	jle	.LBB6_51
# BB#45:                                # %.lr.ph.i41.preheader
                                        #   in Loop: Header=BB6_44 Depth=1
	leaq	-1(%r12), %rcx
	testb	$3, %r12b
	je	.LBB6_46
# BB#47:                                # %.lr.ph.i41.prol.preheader
                                        #   in Loop: Header=BB6_44 Depth=1
	movl	%r12d, %edx
	andl	$3, %edx
	movq	%r13, %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_48:                               # %.lr.ph.i41.prol
                                        #   Parent Loop BB6_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	incq	%rax
	addq	$8, %rsi
	cmpq	%rax, %rdx
	jne	.LBB6_48
	jmp	.LBB6_49
	.p2align	4, 0x90
.LBB6_46:                               #   in Loop: Header=BB6_44 Depth=1
	xorl	%eax, %eax
.LBB6_49:                               # %.lr.ph.i41.prol.loopexit
                                        #   in Loop: Header=BB6_44 Depth=1
	cmpq	$3, %rcx
	jb	.LBB6_51
	.p2align	4, 0x90
.LBB6_50:                               # %.lr.ph.i41
                                        #   Parent Loop BB6_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r13,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	movsd	8(%r13,%rax,8), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	movsd	16(%r13,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	movsd	24(%r13,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	addq	$4, %rax
	cmpq	%rax, %r12
	jne	.LBB6_50
.LBB6_51:                               # %._crit_edge.i44
                                        #   in Loop: Header=BB6_44 Depth=1
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_53
# BB#52:                                # %call.sqrt292
                                        #   in Loop: Header=BB6_44 Depth=1
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	callq	sqrt
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movsd	.LCPI6_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI6_0(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB6_53:                               # %._crit_edge.i44.split
                                        #   in Loop: Header=BB6_44 Depth=1
	movapd	%xmm4, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm0, (%r14,%r12,8)
	leaq	1(%r12), %r8
	cmpq	$1023, %r8              # imm = 0x3FF
	jg	.LBB6_40
# BB#54:                                # %.lr.ph11.i45
                                        #   in Loop: Header=BB6_44 Depth=1
	testq	%r12, %r12
	jle	.LBB6_58
# BB#55:                                # %.lr.ph11.split.us.i47.preheader
                                        #   in Loop: Header=BB6_44 Depth=1
	movl	%r12d, %ecx
	andl	$1, %ecx
	movq	%r10, %rdx
	shlq	$13, %rdx
	addq	16(%rsp), %rdx          # 8-byte Folded Reload
	movq	%r10, %rsi
	.p2align	4, 0x90
.LBB6_56:                               # %.lr.ph11.split.us.i47
                                        #   Parent Loop BB6_44 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_68 Depth 3
	testq	%rcx, %rcx
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	jne	.LBB6_65
# BB#57:                                #   in Loop: Header=BB6_56 Depth=2
	xorl	%edi, %edi
	cmpq	$1, %r12
	jne	.LBB6_67
	jmp	.LBB6_69
	.p2align	4, 0x90
.LBB6_65:                               #   in Loop: Header=BB6_56 Depth=2
	movq	%rsi, %rax
	shlq	$13, %rax
	movsd	(%rbx,%rax), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%rbp), %xmm1
	subsd	%xmm1, %xmm0
	movl	$1, %edi
	cmpq	$1, %r12
	je	.LBB6_69
.LBB6_67:                               # %.lr.ph11.split.us.i47.new
                                        #   in Loop: Header=BB6_56 Depth=2
	leaq	(%rdx,%rdi,8), %rax
	.p2align	4, 0x90
.LBB6_68:                               #   Parent Loop BB6_44 Depth=1
                                        #     Parent Loop BB6_56 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	mulsd	(%r13,%rdi,8), %xmm1
	subsd	%xmm1, %xmm0
	mulsd	8(%r13,%rdi,8), %xmm2
	subsd	%xmm2, %xmm0
	addq	$2, %rdi
	addq	$16, %rax
	cmpq	%rdi, %r12
	jne	.LBB6_68
.LBB6_69:                               # %._crit_edge7.us.i54
                                        #   in Loop: Header=BB6_56 Depth=2
	mulsd	(%r14,%r12,8), %xmm0
	movq	%rsi, %rax
	shlq	$13, %rax
	addq	%rbx, %rax
	movsd	%xmm0, (%rax,%r12,8)
	incq	%rsi
	addq	$8192, %rdx             # imm = 0x2000
	cmpq	$1024, %rsi             # imm = 0x400
	jne	.LBB6_56
	jmp	.LBB6_40
.LBB6_70:                               # %.preheader.i.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_2(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_71:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_72 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_72:                               #   Parent Loop BB6_71 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r15,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%rbx,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_73
# BB#75:                                #   in Loop: Header=BB6_72 Depth=2
	movsd	(%r15,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbx,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_74
# BB#76:                                #   in Loop: Header=BB6_72 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$1024, %rcx             # imm = 0x400
	movq	%rdi, %rcx
	jl	.LBB6_72
# BB#77:                                #   in Loop: Header=BB6_71 Depth=1
	incq	%rdx
	addq	$1024, %rax             # imm = 0x400
	cmpq	$1024, %rdx             # imm = 0x400
	jl	.LBB6_71
# BB#78:                                # %check_FP.exit
	movl	$16385, %edi            # imm = 0x4001
	callq	malloc
	movq	%rax, %r12
	movb	$0, 16384(%r12)
	xorl	%r13d, %r13d
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB6_79:                               # %.preheader.i65
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_80 Depth 2
	movq	%rbp, %rsi
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_80:                               #   Parent Loop BB6_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdx
	movl	%edx, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -15(%r12,%rcx)
	movb	%al, -14(%r12,%rcx)
	movq	%rdx, %rax
	shrq	$8, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -13(%r12,%rcx)
	movb	%al, -12(%r12,%rcx)
	movq	%rdx, %rax
	shrq	$16, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -11(%r12,%rcx)
	movb	%al, -10(%r12,%rcx)
	movl	%edx, %eax
	shrl	$24, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -9(%r12,%rcx)
	movb	%al, -8(%r12,%rcx)
	movq	%rdx, %rax
	shrq	$32, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -7(%r12,%rcx)
	movb	%al, -6(%r12,%rcx)
	movq	%rdx, %rax
	shrq	$40, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -5(%r12,%rcx)
	movb	%al, -4(%r12,%rcx)
	movq	%rdx, %rax
	shrq	$48, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -3(%r12,%rcx)
	movb	%al, -2(%r12,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r12,%rcx)
	movb	%dl, (%r12,%rcx)
	addq	$16, %rcx
	addq	$8, %rsi
	cmpq	$16399, %rcx            # imm = 0x400F
	jne	.LBB6_80
# BB#81:                                #   in Loop: Header=BB6_79 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r12, %rdi
	callq	fputs
	incq	%r13
	addq	$8192, %rbp             # imm = 0x2000
	cmpq	$1024, %r13             # imm = 0x400
	jne	.LBB6_79
# BB#82:                                # %print_array.exit
	movq	%r12, %rdi
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_83
.LBB6_73:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_74:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_3(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_83:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_84:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
