	.text
	.file	"util.bc"
	.globl	_ZN2kc7f_addedEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc7f_addedEPNS_7impl_IDE,@function
_ZN2kc7f_addedEPNS_7impl_IDE:           # @_ZN2kc7f_addedEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB0_2
# BB#1:
	movq	40(%rbx), %rdi
	callq	_ZN2kc22AddedPhylumdeclarationEPNS_11impl_uniqIDE
	cmpb	$0, 8(%rax)
	setne	%al
	jmp	.LBB0_3
.LBB0_2:
	movl	$.L.str, %edi
	movl	$74, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
.LBB0_3:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN2kc7f_addedEPNS_7impl_IDE, .Lfunc_end0-_ZN2kc7f_addedEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc5v_addEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc5v_addEPNS_7impl_IDE,@function
_ZN2kc5v_addEPNS_7impl_IDE:             # @_ZN2kc5v_addEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB1_2
# BB#1:
	movq	40(%rbx), %rdi
	callq	_ZN2kc22AddedPhylumdeclarationEPNS_11impl_uniqIDE
	movb	$1, 8(%rax)
	popq	%rbx
	retq
.LBB1_2:
	movl	$.L.str.2, %edi
	movl	$90, %esi
	movl	$.L.str.1, %edx
	popq	%rbx
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.Lfunc_end1:
	.size	_ZN2kc5v_addEPNS_7impl_IDE, .Lfunc_end1-_ZN2kc5v_addEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc11v_freeaddedEv
	.p2align	4, 0x90
	.type	_ZN2kc11v_freeaddedEv,@function
_ZN2kc11v_freeaddedEv:                  # @_ZN2kc11v_freeaddedEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movq	pl_addedphylumdeclarations(%rip), %rdi
	movl	$1, %esi
	callq	_ZN2kc20impl_abstract_phylum4freeEb
	movq	$0, pl_addedphylumdeclarations(%rip)
	popq	%rax
	retq
.Lfunc_end2:
	.size	_ZN2kc11v_freeaddedEv, .Lfunc_end2-_ZN2kc11v_freeaddedEv
	.cfi_endproc

	.globl	_ZN2kc10f_getcountEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc10f_getcountEPNS_7impl_IDE,@function
_ZN2kc10f_getcountEPNS_7impl_IDE:       # @_ZN2kc10f_getcountEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	$7, %ecx
	jne	.LBB3_2
# BB#1:
	movq	40(%rbx), %rdi
	callq	_ZN2kc24CountedPhylumdeclarationEPNS_11impl_uniqIDE
	movq	%rax, %rcx
	movl	8(%rcx), %eax
	incl	%eax
	movl	%eax, 8(%rcx)
.LBB3_2:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN2kc10f_getcountEPNS_7impl_IDE, .Lfunc_end3-_ZN2kc10f_getcountEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc12v_resetcountEv
	.p2align	4, 0x90
	.type	_ZN2kc12v_resetcountEv,@function
_ZN2kc12v_resetcountEv:                 # @_ZN2kc12v_resetcountEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	pl_countedphylumdeclarations(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_2
	jmp	.LBB4_3
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	8(%rbx), %rax
	movl	$0, 8(%rax)
	movq	16(%rbx), %rbx
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$331, %eax              # imm = 0x14B
	je	.LBB4_1
.LBB4_3:                                # %.loopexit
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN2kc12v_resetcountEv, .Lfunc_end4-_ZN2kc12v_resetcountEv
	.cfi_endproc

	.globl	_ZN2kc11v_freecountEv
	.p2align	4, 0x90
	.type	_ZN2kc11v_freecountEv,@function
_ZN2kc11v_freecountEv:                  # @_ZN2kc11v_freecountEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	pl_countedphylumdeclarations(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_2
	jmp	.LBB5_4
	.p2align	4, 0x90
.LBB5_1:                                # %.lr.ph.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rbx), %rax
	movl	$0, 8(%rax)
	movq	16(%rbx), %rbx
.LBB5_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$331, %eax              # imm = 0x14B
	je	.LBB5_1
# BB#3:                                 # %_ZN2kc12v_resetcountEv.exit
	movq	pl_countedphylumdeclarations(%rip), %rdi
	movl	$1, %esi
	callq	_ZN2kc20impl_abstract_phylum4freeEb
	movq	$0, pl_countedphylumdeclarations(%rip)
.LBB5_4:
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_ZN2kc11v_freecountEv, .Lfunc_end5-_ZN2kc11v_freecountEv
	.cfi_endproc

	.globl	_ZN2kc12is_uview_varEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc12is_uview_varEPNS_7impl_IDE,@function
_ZN2kc12is_uview_varEPNS_7impl_IDE:     # @_ZN2kc12is_uview_varEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	The_current_unparseitems(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB6_7
# BB#1:                                 # %.preheader
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$147, %eax
	jne	.LBB6_7
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$152, %eax
	jne	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	24(%r15), %rdi
	movq	%r14, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB6_8
.LBB6_4:                                # %.thread37
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	16(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$147, %eax
	je	.LBB6_2
.LBB6_7:
	xorl	%eax, %eax
.LBB6_9:                                # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB6_8:
	callq	_ZN2kc10ITUViewVarEv
	movq	%rax, 8(%r14)
	movb	$1, %al
	jmp	.LBB6_9
.Lfunc_end6:
	.size	_ZN2kc12is_uview_varEPNS_7impl_IDE, .Lfunc_end6-_ZN2kc12is_uview_varEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc16f_DvIsDisallowedEPNS_20impl_dollarvarstatusE
	.p2align	4, 0x90
	.type	_ZN2kc16f_DvIsDisallowedEPNS_20impl_dollarvarstatusE,@function
_ZN2kc16f_DvIsDisallowedEPNS_20impl_dollarvarstatusE: # @_ZN2kc16f_DvIsDisallowedEPNS_20impl_dollarvarstatusE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 16
.Lcfi18:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$194, %ecx
	je	.LBB7_4
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$193, %eax
	je	.LBB7_3
# BB#2:
	movl	$.L.str.3, %edi
	movl	$206, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB7_3:
	xorl	%eax, %eax
.LBB7_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN2kc16f_DvIsDisallowedEPNS_20impl_dollarvarstatusE, .Lfunc_end7-_ZN2kc16f_DvIsDisallowedEPNS_20impl_dollarvarstatusE
	.cfi_endproc

	.globl	_ZN2kc19f_listelementphylumEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc19f_listelementphylumEPNS_7impl_IDE,@function
_ZN2kc19f_listelementphylumEPNS_7impl_IDE: # @_ZN2kc19f_listelementphylumEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB8_7
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB8_5
# BB#2:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB8_5
# BB#3:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$22, %eax
	jne	.LBB8_5
# BB#4:
	movq	48(%rbx), %rax
	movq	16(%rax), %rax
	popq	%rbx
	retq
.LBB8_7:
	movl	$.L.str.4, %edi
	movl	$243, %esi
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB8_5:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rax
	testq	%rax, %rax
	je	.LBB8_6
# BB#8:                                 # %_ZN2kc9f_emptyIdEv.exit23
	popq	%rbx
	retq
.LBB8_6:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
	popq	%rbx
	retq
.Lfunc_end8:
	.size	_ZN2kc19f_listelementphylumEPNS_7impl_IDE, .Lfunc_end8-_ZN2kc19f_listelementphylumEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc9f_emptyIdEv
	.p2align	4, 0x90
	.type	_ZN2kc9f_emptyIdEv,@function
_ZN2kc9f_emptyIdEv:                     # @_ZN2kc9f_emptyIdEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 16
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rax
	testq	%rax, %rax
	jne	.LBB9_2
# BB#1:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB9_2:
	popq	%rcx
	retq
.Lfunc_end9:
	.size	_ZN2kc9f_emptyIdEv, .Lfunc_end9-_ZN2kc9f_emptyIdEv
	.cfi_endproc

	.globl	_ZN2kc25f_listelementconsoperatorEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc25f_listelementconsoperatorEPNS_7impl_IDE,@function
_ZN2kc25f_listelementconsoperatorEPNS_7impl_IDE: # @_ZN2kc25f_listelementconsoperatorEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB10_9
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB10_7
# BB#2:
	movq	8(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB10_7
# BB#3:
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$22, %eax
	jne	.LBB10_7
# BB#4:
	movq	48(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB10_7
# BB#5:
	movq	48(%rbx), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB10_7
# BB#6:
	movq	48(%rbx), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	movq	40(%rax), %rax
	popq	%rbx
	retq
.LBB10_9:
	movl	$.L.str.5, %edi
	movl	$280, %esi              # imm = 0x118
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB10_7:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rax
	testq	%rax, %rax
	je	.LBB10_8
# BB#10:                                # %_ZN2kc9f_emptyIdEv.exit25
	popq	%rbx
	retq
.LBB10_8:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
	popq	%rbx
	retq
.Lfunc_end10:
	.size	_ZN2kc25f_listelementconsoperatorEPNS_7impl_IDE, .Lfunc_end10-_ZN2kc25f_listelementconsoperatorEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc18f_operatorinphylumEPNS_7impl_IDES1_
	.p2align	4, 0x90
	.type	_ZN2kc18f_operatorinphylumEPNS_7impl_IDES1_,@function
_ZN2kc18f_operatorinphylumEPNS_7impl_IDES1_: # @_ZN2kc18f_operatorinphylumEPNS_7impl_IDES1_
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB11_3
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB11_4
# BB#2:
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZNK2kc20impl_abstract_phylum2eqEPKS0_ # TAILCALL
.LBB11_3:
	movl	$.L.str.7, %edi
	movl	$314, %esi              # imm = 0x13A
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB11_4:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	_ZN2kc18f_operatorinphylumEPNS_7impl_IDES1_, .Lfunc_end11-_ZN2kc18f_operatorinphylumEPNS_7impl_IDES1_
	.cfi_endproc

	.globl	_ZN2kc10f_isphylumEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc10f_isphylumEPNS_7impl_IDE,@function
_ZN2kc10f_isphylumEPNS_7impl_IDE:       # @_ZN2kc10f_isphylumEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
.Lcfi30:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB12_3
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$174, %ecx
	je	.LBB12_4
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	sete	%al
	jmp	.LBB12_4
.LBB12_3:
	movl	$.L.str.8, %edi
	movl	$340, %esi              # imm = 0x154
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
.LBB12_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end12:
	.size	_ZN2kc10f_isphylumEPNS_7impl_IDE, .Lfunc_end12-_ZN2kc10f_isphylumEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE,@function
_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE: # @_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
.Lcfi32:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB13_5
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB13_3
# BB#2:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB13_5:
	movl	$.L.str.9, %edi
	movl	$368, %esi              # imm = 0x170
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB13_6:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB13_3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB13_6
# BB#4:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.Lfunc_end13:
	.size	_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE, .Lfunc_end13-_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc20f_ispredefinedphylumEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc20f_ispredefinedphylumEPNS_7impl_IDE,@function
_ZN2kc20f_ispredefinedphylumEPNS_7impl_IDE: # @_ZN2kc20f_ispredefinedphylumEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 16
.Lcfi34:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB14_2
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$173, %eax
	sete	%al
	jmp	.LBB14_3
.LBB14_2:
	movl	$.L.str.10, %edi
	movl	$391, %esi              # imm = 0x187
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
.LBB14_3:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end14:
	.size	_ZN2kc20f_ispredefinedphylumEPNS_7impl_IDE, .Lfunc_end14-_ZN2kc20f_ispredefinedphylumEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE,@function
_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE: # @_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
.Lcfi36:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB15_3
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB15_4
# BB#2:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB15_3:
	movl	$.L.str.11, %edi
	movl	$415, %esi              # imm = 0x19F
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB15_4:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE, .Lfunc_end15-_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc21f_argumentsofoperatorEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc21f_argumentsofoperatorEPNS_7impl_IDE,@function
_ZN2kc21f_argumentsofoperatorEPNS_7impl_IDE: # @_ZN2kc21f_argumentsofoperatorEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
.Lcfi38:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB16_5
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB16_4
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB16_4
# BB#3:
	movq	8(%rbx), %rax
	movq	48(%rax), %rax
	popq	%rbx
	retq
.LBB16_5:
	movl	$.L.str.12, %edi
	movl	$439, %esi              # imm = 0x1B7
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB16_4:
	popq	%rbx
	jmp	_ZN2kc12NilargumentsEv  # TAILCALL
.Lfunc_end16:
	.size	_ZN2kc21f_argumentsofoperatorEPNS_7impl_IDE, .Lfunc_end16-_ZN2kc21f_argumentsofoperatorEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE,@function
_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE: # @_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 16
.Lcfi40:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB17_7
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB17_3
# BB#2:
	movq	16(%rbx), %rax
	popq	%rbx
	retq
.LBB17_7:
	movl	$.L.str.13, %edi
	movl	$467, %esi              # imm = 0x1D3
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
.LBB17_8:                               # %_ZN2kc9f_emptyIdEv.exit
	popq	%rbx
	retq
.LBB17_3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	jne	.LBB17_5
# BB#4:
	movq	16(%rbx), %rax
	popq	%rbx
	retq
.LBB17_5:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rax
	testq	%rax, %rax
	jne	.LBB17_8
# BB#6:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
	popq	%rbx
	retq
.Lfunc_end17:
	.size	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE, .Lfunc_end17-_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc19f_phylumofpatternIDEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc19f_phylumofpatternIDEPNS_7impl_IDE,@function
_ZN2kc19f_phylumofpatternIDEPNS_7impl_IDE: # @_ZN2kc19f_phylumofpatternIDEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 16
.Lcfi42:
	.cfi_offset %rbx, -16
	movq	8(%rdi), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB18_2
# BB#1:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB18_2:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rax
	testq	%rax, %rax
	jne	.LBB18_4
# BB#3:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB18_4:                               # %_ZN2kc9f_emptyIdEv.exit
	popq	%rbx
	retq
.Lfunc_end18:
	.size	_ZN2kc19f_phylumofpatternIDEPNS_7impl_IDE, .Lfunc_end18-_ZN2kc19f_phylumofpatternIDEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc25f_phylumofpatternvariableEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc25f_phylumofpatternvariableEPNS_7impl_IDE,@function
_ZN2kc25f_phylumofpatternvariableEPNS_7impl_IDE: # @_ZN2kc25f_phylumofpatternvariableEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 16
.Lcfi44:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB19_5
# BB#1:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB19_3
# BB#2:
	movq	8(%rbx), %rax
	popq	%rbx
	retq
.LBB19_5:
	movl	$.L.str.14, %edi
	movl	$506, %esi              # imm = 0x1FA
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB19_3:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rax
	testq	%rax, %rax
	je	.LBB19_4
# BB#6:                                 # %_ZN2kc9f_emptyIdEv.exit
	popq	%rbx
	retq
.LBB19_4:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
	popq	%rbx
	retq
.Lfunc_end19:
	.size	_ZN2kc25f_phylumofpatternvariableEPNS_7impl_IDE, .Lfunc_end19-_ZN2kc25f_phylumofpatternvariableEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc23v_syn_type_attribute_IDEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc23v_syn_type_attribute_IDEPNS_7impl_IDE,@function
_ZN2kc23v_syn_type_attribute_IDEPNS_7impl_IDE: # @_ZN2kc23v_syn_type_attribute_IDEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -24
.Lcfi49:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB20_4
# BB#1:
	movq	40(%rbx), %r14
	movq	8(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB20_3
# BB#2:
	movq	8(%r14), %rax
	movq	%rax, 8(%rbx)
.LBB20_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB20_4:
	movl	$.L.str.15, %edi
	movl	$529, %esi              # imm = 0x211
	movl	$.L.str.1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.Lfunc_end20:
	.size	_ZN2kc23v_syn_type_attribute_IDEPNS_7impl_IDE, .Lfunc_end20-_ZN2kc23v_syn_type_attribute_IDEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc14f_NilargumentsEPNS_14impl_argumentsE
	.p2align	4, 0x90
	.type	_ZN2kc14f_NilargumentsEPNS_14impl_argumentsE,@function
_ZN2kc14f_NilargumentsEPNS_14impl_argumentsE: # @_ZN2kc14f_NilargumentsEPNS_14impl_argumentsE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 16
.Lcfi51:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$29, %eax
	je	.LBB21_3
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	%eax, %ecx
	movb	$1, %al
	cmpl	$28, %ecx
	je	.LBB21_4
# BB#2:
	movl	$.L.str.16, %edi
	movl	$543, %esi              # imm = 0x21F
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB21_3:
	xorl	%eax, %eax
.LBB21_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end21:
	.size	_ZN2kc14f_NilargumentsEPNS_14impl_argumentsE, .Lfunc_end21-_ZN2kc14f_NilargumentsEPNS_14impl_argumentsE
	.cfi_endproc

	.globl	_ZN2kc14f_hd_argumentsEPNS_14impl_argumentsE
	.p2align	4, 0x90
	.type	_ZN2kc14f_hd_argumentsEPNS_14impl_argumentsE,@function
_ZN2kc14f_hd_argumentsEPNS_14impl_argumentsE: # @_ZN2kc14f_hd_argumentsEPNS_14impl_argumentsE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 16
.Lcfi53:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$29, %eax
	jne	.LBB22_2
# BB#1:
	movq	16(%rbx), %rax
	popq	%rbx
	retq
.LBB22_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$28, %eax
	je	.LBB22_5
# BB#3:
	movl	$.L.str.18, %edi
	movl	$562, %esi              # imm = 0x232
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB22_5:
	movl	$.L.str.1, %edi
	movl	$558, %esi              # imm = 0x22E
	movl	$.L.str.17, %edx
	callq	_ZN2kc24kc_assertionReasonFailedEPKciS1_
.Lfunc_end22:
	.size	_ZN2kc14f_hd_argumentsEPNS_14impl_argumentsE, .Lfunc_end22-_ZN2kc14f_hd_argumentsEPNS_14impl_argumentsE
	.cfi_endproc

	.globl	_ZN2kc14f_tl_argumentsEPNS_14impl_argumentsE
	.p2align	4, 0x90
	.type	_ZN2kc14f_tl_argumentsEPNS_14impl_argumentsE,@function
_ZN2kc14f_tl_argumentsEPNS_14impl_argumentsE: # @_ZN2kc14f_tl_argumentsEPNS_14impl_argumentsE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 16
.Lcfi55:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$29, %eax
	jne	.LBB23_2
# BB#1:
	movq	24(%rbx), %rax
	popq	%rbx
	retq
.LBB23_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$28, %eax
	je	.LBB23_5
# BB#3:
	movl	$.L.str.20, %edi
	movl	$581, %esi              # imm = 0x245
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB23_5:
	movl	$.L.str.1, %edi
	movl	$577, %esi              # imm = 0x241
	movl	$.L.str.19, %edx
	callq	_ZN2kc24kc_assertionReasonFailedEPKciS1_
.Lfunc_end23:
	.size	_ZN2kc14f_tl_argumentsEPNS_14impl_argumentsE, .Lfunc_end23-_ZN2kc14f_tl_argumentsEPNS_14impl_argumentsE
	.cfi_endproc

	.globl	_ZN2kc40v_check_dollarvar_attribute_in_operatorsEPNS_8impl_INTEPNS_18impl_unpattributesEPNS_14impl_operatorsE
	.p2align	4, 0x90
	.type	_ZN2kc40v_check_dollarvar_attribute_in_operatorsEPNS_8impl_INTEPNS_18impl_unpattributesEPNS_14impl_operatorsE,@function
_ZN2kc40v_check_dollarvar_attribute_in_operatorsEPNS_8impl_INTEPNS_18impl_unpattributesEPNS_14impl_operatorsE: # @_ZN2kc40v_check_dollarvar_attribute_in_operatorsEPNS_8impl_INTEPNS_18impl_unpattributesEPNS_14impl_operatorsE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 64
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$188, %eax
	jne	.LBB24_16
# BB#1:
	movq	8(%rbx), %r12
	movq	16(%rbx), %r13
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE
	movq	%rax, %r15
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rsi
	testq	%rsi, %rsi
	jne	.LBB24_3
# BB#2:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rsi
	movq	%rsi, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB24_3:                               # %_ZN2kc9f_emptyIdEv.exit
	movq	%r15, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB24_4
# BB#19:
	movq	16(%r14), %rdi
	movl	8(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.21, %edi
	movl	$.L.str.22, %edx
	movq	%r14, %rsi
	movq	%r12, %rcx
	callq	_ZN2kc18Problem1S1INT1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc8v_reportEPNS_10impl_errorE # TAILCALL
.LBB24_16:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$187, %eax
	je	.LBB24_18
# BB#17:
	movl	$.L.str.23, %edi
	movl	$607, %esi              # imm = 0x25F
	jmp	.LBB24_15
.LBB24_4:
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc31f_check_unpattributes_in_phylumEPNS_18impl_unpattributesEPNS_7impl_IDE
	jmp	.LBB24_5
	.p2align	4, 0x90
.LBB24_10:                              # %tailrecurse.backedge.i
                                        #   in Loop: Header=BB24_5 Depth=1
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB24_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$188, %eax
	jne	.LBB24_13
# BB#6:                                 #   in Loop: Header=BB24_5 Depth=1
	movq	8(%r13), %r12
	movq	16(%r13), %r13
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE
	movq	%rax, %rbx
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rsi
	testq	%rsi, %rsi
	jne	.LBB24_8
# BB#7:                                 #   in Loop: Header=BB24_5 Depth=1
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rsi
	movq	%rsi, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB24_8:                               # %_ZN2kc9f_emptyIdEv.exit.i
                                        #   in Loop: Header=BB24_5 Depth=1
	movq	%rbx, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB24_11
# BB#9:                                 #   in Loop: Header=BB24_5 Depth=1
	movq	16(%r14), %rdi
	movl	8(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.21, %edi
	movl	$.L.str.22, %edx
	movq	%r14, %rsi
	movq	%r12, %rcx
	callq	_ZN2kc18Problem1S1INT1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDE
	movq	%rbx, %rdi
	jmp	.LBB24_10
	.p2align	4, 0x90
.LBB24_11:                              #   in Loop: Header=BB24_5 Depth=1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB24_5
# BB#12:                                #   in Loop: Header=BB24_5 Depth=1
	movq	16(%r14), %rdi
	movl	8(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbp
	movl	$.L.str.46, %edi
	movl	$.L.str.47, %edx
	movl	$.L.str.48, %r8d
	movq	%r14, %rsi
	movq	%r15, %rcx
	movq	%rbx, %r9
	callq	_ZN2kc23Problem1S1INT1S1ID1S1IDEPKcPNS_8impl_INTES1_PNS_7impl_IDES1_S5_
	movq	%rbp, %rdi
	jmp	.LBB24_10
.LBB24_13:                              # %tailrecurse._crit_edge.i
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$187, %eax
	jne	.LBB24_14
.LBB24_18:                              # %_ZN2kcL33v_do_check_dollarvar_in_operatorsEPNS_8impl_INTEPNS_14impl_operatorsEPNS_7impl_IDE.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_14:
	movl	$.L.str.49, %edi
	movl	$670, %esi              # imm = 0x29E
.LBB24_15:
	movl	$.L.str.1, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.Lfunc_end24:
	.size	_ZN2kc40v_check_dollarvar_attribute_in_operatorsEPNS_8impl_INTEPNS_18impl_unpattributesEPNS_14impl_operatorsE, .Lfunc_end24-_ZN2kc40v_check_dollarvar_attribute_in_operatorsEPNS_8impl_INTEPNS_18impl_unpattributesEPNS_14impl_operatorsE
	.cfi_endproc

	.globl	_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE
	.p2align	4, 0x90
	.type	_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE,@function
_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE: # @_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 32
.Lcfi72:
	.cfi_offset %rbx, -24
.Lcfi73:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$8, %eax
	jne	.LBB25_9
# BB#1:
	movq	24(%r14), %rax
	cmpl	$0, 8(%rax)
	je	.LBB25_10
# BB#2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB25_7
# BB#3:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB25_6
# BB#4:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB25_6
# BB#5:
	movq	8(%rbx), %rax
	movq	48(%rax), %rdi
	jmp	.LBB25_8
.LBB25_9:
	movl	$.L.str.26, %edi
	movl	$766, %esi              # imm = 0x2FE
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB25_10:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE # TAILCALL
.LBB25_7:
	movl	$.L.str.12, %edi
	movl	$439, %esi              # imm = 0x1B7
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%edi, %edi
	jmp	.LBB25_8
.LBB25_6:
	callq	_ZN2kc12NilargumentsEv
	movq	%rax, %rdi
.LBB25_8:                               # %_ZN2kc21f_argumentsofoperatorEPNS_7impl_IDE.exit
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc11f_subphylumEPNS_14impl_argumentsEPNS_8impl_INTE # TAILCALL
.Lfunc_end25:
	.size	_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE, .Lfunc_end25-_ZN2kc21f_subphylumofoperatorEPNS_7impl_IDEPNS_8impl_INTE
	.cfi_endproc

	.globl	_ZN2kc31f_check_unpattributes_in_phylumEPNS_18impl_unpattributesEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc31f_check_unpattributes_in_phylumEPNS_18impl_unpattributesEPNS_7impl_IDE,@function
_ZN2kc31f_check_unpattributes_in_phylumEPNS_18impl_unpattributesEPNS_7impl_IDE: # @_ZN2kc31f_check_unpattributes_in_phylumEPNS_18impl_unpattributesEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 48
.Lcfi79:
	.cfi_offset %rbx, -48
.Lcfi80:
	.cfi_offset %r12, -40
.Lcfi81:
	.cfi_offset %r13, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %r15, -16
	movq	%rsi, %r13
	movq	(%rdi), %rax
	callq	*96(%rax)
	movq	%rax, %r14
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB26_1:                               # %tailrecurse.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %r15
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$159, %eax
	jne	.LBB26_7
# BB#2:                                 #   in Loop: Header=BB26_1 Depth=1
	movq	8(%rbx), %r12
	movq	16(%rbx), %rbx
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc28f_typeof_attribute_in_phylymEPNS_7impl_IDES1_
	movq	%rax, %r13
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rsi
	testq	%rsi, %rsi
	jne	.LBB26_4
# BB#3:                                 #   in Loop: Header=BB26_1 Depth=1
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rsi
	movq	%rsi, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB26_4:                               # %_ZN2kc9f_emptyIdEv.exit.i
                                        #   in Loop: Header=BB26_1 Depth=1
	movq	%r13, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB26_1
# BB#5:
	movq	32(%r12), %rdi
	movl	24(%r12), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.43, %edi
	movl	$.L.str.44, %edx
	movq	%r12, %rsi
	movq	%r15, %rcx
	callq	_ZN2kc17Problem1S1ID1S1IDEPKcPNS_7impl_IDES1_S3_
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %r15
	testq	%r15, %r15
	jne	.LBB26_9
# BB#6:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
	movq	%r15, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
	jmp	.LBB26_9
.LBB26_7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$158, %eax
	je	.LBB26_9
# BB#8:
	movl	$.L.str.45, %edi
	movl	$641, %esi              # imm = 0x281
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r15d, %r15d
.LBB26_9:                               # %_ZN2kcL34f_do_check_unpattributes_in_phylumEPNS_18impl_unpattributesEPNS_7impl_IDE.exit
	movq	%r14, %rdi
	callq	_ZN2kc18impl_abstract_list8freelistEv
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	_ZN2kc31f_check_unpattributes_in_phylumEPNS_18impl_unpattributesEPNS_7impl_IDE, .Lfunc_end26-_ZN2kc31f_check_unpattributes_in_phylumEPNS_18impl_unpattributesEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc21f_attribute_in_phylymEPNS_7impl_IDES1_
	.p2align	4, 0x90
	.type	_ZN2kc21f_attribute_in_phylymEPNS_7impl_IDES1_,@function
_ZN2kc21f_attribute_in_phylymEPNS_7impl_IDES1_: # @_ZN2kc21f_attribute_in_phylymEPNS_7impl_IDES1_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 16
.Lcfi85:
	.cfi_offset %rbx, -16
	callq	_ZN2kc28f_typeof_attribute_in_phylymEPNS_7impl_IDES1_
	movq	%rax, %rbx
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rsi
	testq	%rsi, %rsi
	jne	.LBB27_2
# BB#1:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rsi
	movq	%rsi, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB27_2:                               # %_ZN2kc9f_emptyIdEv.exit
	movq	%rbx, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	xorb	$1, %al
	popq	%rbx
	retq
.Lfunc_end27:
	.size	_ZN2kc21f_attribute_in_phylymEPNS_7impl_IDES1_, .Lfunc_end27-_ZN2kc21f_attribute_in_phylymEPNS_7impl_IDES1_
	.cfi_endproc

	.globl	_ZN2kc28f_typeof_attribute_in_phylymEPNS_7impl_IDES1_
	.p2align	4, 0x90
	.type	_ZN2kc28f_typeof_attribute_in_phylymEPNS_7impl_IDES1_,@function
_ZN2kc28f_typeof_attribute_in_phylymEPNS_7impl_IDES1_: # @_ZN2kc28f_typeof_attribute_in_phylymEPNS_7impl_IDES1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 48
.Lcfi91:
	.cfi_offset %rbx, -48
.Lcfi92:
	.cfi_offset %r12, -40
.Lcfi93:
	.cfi_offset %r13, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	_ZN2kc12f_lookupdeclEPNS_7impl_IDE
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB28_1
# BB#4:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB28_21
# BB#5:
	movq	56(%r12), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$31, %eax
	jne	.LBB28_21
# BB#6:
	movq	56(%r12), %rax
	movq	8(%rax), %rbx
	jmp	.LBB28_7
	.p2align	4, 0x90
.LBB28_10:                              # %.thread
                                        #   in Loop: Header=BB28_7 Depth=1
	movq	16(%rbx), %rbx
.LBB28_7:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$33, %eax
	jne	.LBB28_11
# BB#8:                                 #   in Loop: Header=BB28_7 Depth=1
	movq	8(%rbx), %r13
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$34, %eax
	jne	.LBB28_10
# BB#9:                                 #   in Loop: Header=BB28_7 Depth=1
	movq	8(%r13), %r15
	movq	16(%r13), %rsi
	movq	%r14, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB28_10
	jmp	.LBB28_22
.LBB28_21:
	movl	$.L.str.25, %edi
	movl	$748, %esi              # imm = 0x2EC
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r15d, %r15d
	jmp	.LBB28_22
.LBB28_1:
	movq	32(%rbx), %rdi
	movl	24(%rbx), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %r14
	movl	$.L.str.24, %edi
	movq	%rbx, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
	jmp	.LBB28_2
.LBB28_11:                              # %._crit_edge94
	movq	16(%r12), %rbx
	jmp	.LBB28_12
	.p2align	4, 0x90
.LBB28_20:                              # %.thread82
                                        #   in Loop: Header=BB28_12 Depth=1
	movq	16(%rbx), %rbx
.LBB28_12:                              # %._crit_edge94
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	jne	.LBB28_2
# BB#13:                                #   in Loop: Header=BB28_12 Depth=1
	movq	8(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$110, %eax
	jne	.LBB28_20
# BB#14:                                #   in Loop: Header=BB28_12 Depth=1
	movq	40(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$236, %eax
	jne	.LBB28_20
# BB#15:                                #   in Loop: Header=BB28_12 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$238, %eax
	jne	.LBB28_20
# BB#16:                                #   in Loop: Header=BB28_12 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$246, %eax
	jne	.LBB28_20
# BB#17:                                #   in Loop: Header=BB28_12 Depth=1
	movq	48(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB28_20
# BB#18:                                #   in Loop: Header=BB28_12 Depth=1
	movq	48(%r15), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$260, %eax              # imm = 0x104
	jne	.LBB28_20
# BB#19:                                #   in Loop: Header=BB28_12 Depth=1
	movq	40(%r15), %rax
	movq	48(%r15), %rcx
	movq	8(%rax), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %r15
	movq	24(%rcx), %rax
	movq	16(%rax), %rsi
	movq	%r14, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB28_20
	jmp	.LBB28_22
.LBB28_2:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %r15
	testq	%r15, %r15
	jne	.LBB28_22
# BB#3:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
	movq	%r15, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB28_22:                              # %_ZN2kc9f_emptyIdEv.exit
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end28:
	.size	_ZN2kc28f_typeof_attribute_in_phylymEPNS_7impl_IDES1_, .Lfunc_end28-_ZN2kc28f_typeof_attribute_in_phylymEPNS_7impl_IDES1_
	.cfi_endproc

	.globl	_ZN2kc11f_subphylumEPNS_14impl_argumentsEPNS_8impl_INTE
	.p2align	4, 0x90
	.type	_ZN2kc11f_subphylumEPNS_14impl_argumentsEPNS_8impl_INTE,@function
_ZN2kc11f_subphylumEPNS_14impl_argumentsEPNS_8impl_INTE: # @_ZN2kc11f_subphylumEPNS_14impl_argumentsEPNS_8impl_INTE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 48
.Lcfi101:
	.cfi_offset %rbx, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movl	%eax, %ebp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$29, %eax
	jne	.LBB29_6
# BB#1:                                 # %.lr.ph.i
	incl	%ebp
	.p2align	4, 0x90
.LBB29_3:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %r15
	movq	24(%rbx), %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$8, %eax
	jne	.LBB29_5
# BB#4:                                 #   in Loop: Header=BB29_3 Depth=1
	movq	24(%r14), %rax
	decl	%ebp
	cmpl	8(%rax), %ebp
	je	.LBB29_11
# BB#2:                                 # %tailrecurse.i
                                        #   in Loop: Header=BB29_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$29, %eax
	je	.LBB29_3
.LBB29_6:                               # %tailrecurse._crit_edge.i
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$28, %eax
	jne	.LBB29_9
# BB#7:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %r15
	testq	%r15, %r15
	jne	.LBB29_11
# BB#8:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %r15
	movq	%r15, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
	jmp	.LBB29_11
.LBB29_9:
	movl	$.L.str.50, %edi
	movl	$804, %esi              # imm = 0x324
	jmp	.LBB29_10
.LBB29_5:
	movl	$.L.str.50, %edi
	movl	$796, %esi              # imm = 0x31C
.LBB29_10:                              # %_ZN2kcL14f_do_subphylumEPNS_14impl_argumentsEPNS_8impl_INTEi.exit
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r15d, %r15d
.LBB29_11:                              # %_ZN2kcL14f_do_subphylumEPNS_14impl_argumentsEPNS_8impl_INTEi.exit
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	_ZN2kc11f_subphylumEPNS_14impl_argumentsEPNS_8impl_INTE, .Lfunc_end29-_ZN2kc11f_subphylumEPNS_14impl_argumentsEPNS_8impl_INTE
	.cfi_endproc

	.globl	_ZN2kc20f_argumentofoperatorEPNS_7impl_IDEPNS_8impl_INTE
	.p2align	4, 0x90
	.type	_ZN2kc20f_argumentofoperatorEPNS_7impl_IDEPNS_8impl_INTE,@function
_ZN2kc20f_argumentofoperatorEPNS_7impl_IDEPNS_8impl_INTE: # @_ZN2kc20f_argumentofoperatorEPNS_7impl_IDEPNS_8impl_INTE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -24
.Lcfi109:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$8, %eax
	jne	.LBB30_10
# BB#1:
	movq	24(%r14), %rax
	cmpl	$0, 8(%rax)
	je	.LBB30_2
# BB#3:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB30_8
# BB#4:
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB30_7
# BB#5:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB30_7
# BB#6:
	movq	8(%rbx), %rax
	movq	48(%rax), %rdi
	jmp	.LBB30_9
.LBB30_10:
	movl	$.L.str.27, %edi
	movl	$822, %esi              # imm = 0x336
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	jmp	.LBB30_11
.LBB30_2:
	movq	%rbx, %rdi
	callq	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE
	movq	%rax, %rbx
	xorl	%edi, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8ArgumentEPNS_7impl_IDEPNS_17impl_integer__IntE
.LBB30_11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB30_8:
	movl	$.L.str.12, %edi
	movl	$439, %esi              # imm = 0x1B7
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%edi, %edi
	jmp	.LBB30_9
.LBB30_7:
	callq	_ZN2kc12NilargumentsEv
	movq	%rax, %rdi
.LBB30_9:                               # %_ZN2kc21f_argumentsofoperatorEPNS_7impl_IDE.exit
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc10f_argumentEPNS_14impl_argumentsEPNS_8impl_INTE # TAILCALL
.Lfunc_end30:
	.size	_ZN2kc20f_argumentofoperatorEPNS_7impl_IDEPNS_8impl_INTE, .Lfunc_end30-_ZN2kc20f_argumentofoperatorEPNS_7impl_IDEPNS_8impl_INTE
	.cfi_endproc

	.globl	_ZN2kc10f_argumentEPNS_14impl_argumentsEPNS_8impl_INTE
	.p2align	4, 0x90
	.type	_ZN2kc10f_argumentEPNS_14impl_argumentsEPNS_8impl_INTE,@function
_ZN2kc10f_argumentEPNS_14impl_argumentsEPNS_8impl_INTE: # @_ZN2kc10f_argumentEPNS_14impl_argumentsEPNS_8impl_INTE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 48
.Lcfi115:
	.cfi_offset %rbx, -48
.Lcfi116:
	.cfi_offset %r12, -40
.Lcfi117:
	.cfi_offset %r14, -32
.Lcfi118:
	.cfi_offset %r15, -24
.Lcfi119:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movl	%eax, %ebp
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$29, %eax
	jne	.LBB31_1
# BB#9:                                 # %.lr.ph.i
	incl	%ebp
	.p2align	4, 0x90
.LBB31_10:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %r12
	movq	24(%r15), %rbx
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$8, %eax
	jne	.LBB31_13
# BB#11:                                #   in Loop: Header=BB31_10 Depth=1
	movq	24(%r14), %rax
	decl	%ebp
	cmpl	8(%rax), %ebp
	je	.LBB31_12
# BB#2:                                 # %tailrecurse.i
                                        #   in Loop: Header=BB31_10 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$29, %eax
	movq	%rbx, %r15
	je	.LBB31_10
# BB#3:
	movq	%rbx, %r15
	jmp	.LBB31_4
.LBB31_1:
	movq	%r15, %rbx
.LBB31_4:                               # %tailrecurse._crit_edge.i
	movq	(%r15), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$28, %eax
	jne	.LBB31_14
# BB#5:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_7
# BB#6:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rbx
	movq	%rbx, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB31_7:                               # %_ZN2kc9f_emptyIdEv.exit.i
	xorl	%edi, %edi
	callq	_ZN2kc9mkintegerEi
	movq	%rbx, %rdi
	jmp	.LBB31_8
.LBB31_14:
	movl	$.L.str.51, %edi
	movl	$860, %esi              # imm = 0x35C
	jmp	.LBB31_15
.LBB31_13:
	movl	$.L.str.51, %edi
	movl	$852, %esi              # imm = 0x354
.LBB31_15:                              # %_ZN2kcL13f_do_argumentEPNS_14impl_argumentsEPNS_8impl_INTEi.exit
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	jmp	.LBB31_16
.LBB31_12:
	movl	8(%r15), %edi
	callq	_ZN2kc9mkintegerEi
	movq	%r12, %rdi
.LBB31_8:                               # %_ZN2kcL13f_do_argumentEPNS_14impl_argumentsEPNS_8impl_INTEi.exit
	movq	%rax, %rsi
	callq	_ZN2kc8ArgumentEPNS_7impl_IDEPNS_17impl_integer__IntE
.LBB31_16:                              # %_ZN2kcL13f_do_argumentEPNS_14impl_argumentsEPNS_8impl_INTEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	_ZN2kc10f_argumentEPNS_14impl_argumentsEPNS_8impl_INTE, .Lfunc_end31-_ZN2kc10f_argumentEPNS_14impl_argumentsEPNS_8impl_INTE
	.cfi_endproc

	.globl	_ZN2kc24f_phylumofoutmostpatternEPNS_19impl_outmostpatternE
	.p2align	4, 0x90
	.type	_ZN2kc24f_phylumofoutmostpatternEPNS_19impl_outmostpatternE,@function
_ZN2kc24f_phylumofoutmostpatternEPNS_19impl_outmostpatternE: # @_ZN2kc24f_phylumofoutmostpatternEPNS_19impl_outmostpatternE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 16
.Lcfi121:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	jmp	.LBB32_1
	.p2align	4, 0x90
.LBB32_6:                               # %tailrecurse
                                        #   in Loop: Header=BB32_1 Depth=1
	movq	40(%rbx), %rbx
.LBB32_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$83, %eax
	je	.LBB32_2
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB32_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$84, %eax
	je	.LBB32_2
# BB#5:                                 #   in Loop: Header=BB32_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$82, %eax
	je	.LBB32_6
# BB#7:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$81, %eax
	je	.LBB32_11
# BB#8:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$80, %eax
	jne	.LBB32_9
.LBB32_11:
	movq	32(%rbx), %rdi
	popq	%rbx
	jmp	_ZN2kc18f_phylumofoperatorEPNS_7impl_IDE # TAILCALL
.LBB32_2:                               # %tailrecurse._crit_edge
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rax
	testq	%rax, %rax
	je	.LBB32_3
# BB#10:                                # %_ZN2kc9f_emptyIdEv.exit
	popq	%rbx
	retq
.LBB32_3:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
	popq	%rbx
	retq
.LBB32_9:
	movl	$.L.str.28, %edi
	movl	$887, %esi              # imm = 0x377
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end32:
	.size	_ZN2kc24f_phylumofoutmostpatternEPNS_19impl_outmostpatternE, .Lfunc_end32-_ZN2kc24f_phylumofoutmostpatternEPNS_19impl_outmostpatternE
	.cfi_endproc

	.globl	_ZN2kc18f_operatorofphylumEPNS_7impl_IDEi
	.p2align	4, 0x90
	.type	_ZN2kc18f_operatorofphylumEPNS_7impl_IDEi,@function
_ZN2kc18f_operatorofphylumEPNS_7impl_IDEi: # @_ZN2kc18f_operatorofphylumEPNS_7impl_IDEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi126:
	.cfi_def_cfa_offset 48
.Lcfi127:
	.cfi_offset %rbx, -40
.Lcfi128:
	.cfi_offset %r14, -32
.Lcfi129:
	.cfi_offset %r15, -24
.Lcfi130:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r14
	callq	_ZN2kc12f_lookupdeclEPNS_7impl_IDE
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB33_1
# BB#5:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$15, %eax
	jne	.LBB33_38
# BB#6:
	movq	48(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$24, %eax
	jne	.LBB33_18
# BB#7:
	movq	8(%rbx), %rbx
	movq	%rbx, %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movl	%eax, %r15d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB33_11
# BB#8:                                 # %.lr.ph.i.i.preheader
	decl	%ebp
	subl	%r15d, %ebp
	.p2align	4, 0x90
.LBB33_9:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	je	.LBB33_13
# BB#10:                                # %tailrecurse.i.i
                                        #   in Loop: Header=BB33_9 Depth=1
	movq	16(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	je	.LBB33_9
	jmp	.LBB33_11
.LBB33_1:
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.24, %edi
	jmp	.LBB33_2
.LBB33_38:
	movl	$.L.str.30, %edi
	movl	$983, %esi              # imm = 0x3D7
.LBB33_39:                              # %_ZN2kc9f_emptyIdEv.exit
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	jmp	.LBB33_40
.LBB33_18:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$23, %eax
	jne	.LBB33_27
# BB#19:
	movq	8(%rbx), %rbx
	movq	%rbx, %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movl	%eax, %r15d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB33_11
# BB#20:                                # %.lr.ph.i.i81.preheader
	decl	%ebp
	subl	%r15d, %ebp
	.p2align	4, 0x90
.LBB33_21:                              # %.lr.ph.i.i81
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	je	.LBB33_23
# BB#22:                                # %tailrecurse.i.i82
                                        #   in Loop: Header=BB33_21 Depth=1
	movq	16(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	je	.LBB33_21
	jmp	.LBB33_11
.LBB33_27:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$22, %eax
	jne	.LBB33_36
# BB#28:
	movq	8(%rbx), %rbx
	movq	%rbx, %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movl	%eax, %r15d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB33_11
# BB#29:                                # %.lr.ph.i.i91.preheader
	decl	%ebp
	subl	%r15d, %ebp
	.p2align	4, 0x90
.LBB33_30:                              # %.lr.ph.i.i91
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	je	.LBB33_32
# BB#31:                                # %tailrecurse.i.i92
                                        #   in Loop: Header=BB33_30 Depth=1
	movq	16(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	je	.LBB33_30
.LBB33_11:                              # %tailrecurse._crit_edge.i.i
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$25, %eax
	je	.LBB33_14
# BB#12:
	movl	$.L.str.52, %edi
	movl	$1011, %esi             # imm = 0x3F3
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB33_14:                              # %_ZN2kc13f_alternativeEPNS_17impl_alternativesEi.exit.thread
	movq	32(%r14), %rdi
	movl	24(%r14), %esi
	callq	_ZN2kc8FileLineEPNS_20impl_casestring__StrEi
	movq	%rax, %rbx
	movl	$.L.str.29, %edi
.LBB33_2:
	movq	%r14, %rsi
	callq	_ZN2kc12Problem1S1IDEPKcPNS_7impl_IDE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	_ZN2kc8NonFatalEPNS_13impl_filelineEPNS_12impl_problemE
	movq	%rax, %rdi
	callq	_ZN2kc8v_reportEPNS_10impl_errorE
.LBB33_3:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rax
	testq	%rax, %rax
	jne	.LBB33_40
# BB#4:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB33_40:                              # %_ZN2kc9f_emptyIdEv.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB33_13:                              # %_ZN2kc13f_alternativeEPNS_17impl_alternativesEi.exit
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB33_14
# BB#15:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB33_17
# BB#16:
	movq	40(%rbx), %rax
	jmp	.LBB33_40
.LBB33_36:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$21, %eax
	je	.LBB33_3
# BB#37:
	movl	$.L.str.30, %edi
	movl	$978, %esi              # imm = 0x3D2
	jmp	.LBB33_39
.LBB33_23:                              # %_ZN2kc13f_alternativeEPNS_17impl_alternativesEi.exit87
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB33_14
# BB#24:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB33_26
# BB#25:
	movq	40(%rbx), %rax
	jmp	.LBB33_40
.LBB33_17:
	movl	$.L.str.30, %edi
	movl	$926, %esi              # imm = 0x39E
	jmp	.LBB33_39
.LBB33_32:                              # %_ZN2kc13f_alternativeEPNS_17impl_alternativesEi.exit97
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB33_14
# BB#33:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$27, %eax
	jne	.LBB33_35
# BB#34:
	movq	40(%rbx), %rax
	jmp	.LBB33_40
.LBB33_26:
	movl	$.L.str.30, %edi
	movl	$948, %esi              # imm = 0x3B4
	jmp	.LBB33_39
.LBB33_35:
	movl	$.L.str.30, %edi
	movl	$970, %esi              # imm = 0x3CA
	jmp	.LBB33_39
.Lfunc_end33:
	.size	_ZN2kc18f_operatorofphylumEPNS_7impl_IDEi, .Lfunc_end33-_ZN2kc18f_operatorofphylumEPNS_7impl_IDEi
	.cfi_endproc

	.globl	_ZN2kc13f_alternativeEPNS_17impl_alternativesEi
	.p2align	4, 0x90
	.type	_ZN2kc13f_alternativeEPNS_17impl_alternativesEi,@function
_ZN2kc13f_alternativeEPNS_17impl_alternativesEi: # @_ZN2kc13f_alternativeEPNS_17impl_alternativesEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 32
.Lcfi134:
	.cfi_offset %rbx, -32
.Lcfi135:
	.cfi_offset %r14, -24
.Lcfi136:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movl	%eax, %r14d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	jne	.LBB34_5
# BB#1:                                 # %.lr.ph.i.preheader
	decl	%ebp
	subl	%r14d, %ebp
	.p2align	4, 0x90
.LBB34_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	je	.LBB34_3
# BB#4:                                 # %tailrecurse.i
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	16(%rbx), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$26, %eax
	je	.LBB34_2
.LBB34_5:                               # %tailrecurse._crit_edge.i
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$25, %eax
	je	.LBB34_7
# BB#6:
	movl	$.L.str.52, %edi
	movl	$1011, %esi             # imm = 0x3F3
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB34_7:                               # %_ZN2kcL16f_do_alternativeEPNS_17impl_alternativesEii.exit
	xorl	%eax, %eax
	jmp	.LBB34_8
.LBB34_3:
	movq	8(%rbx), %rax
.LBB34_8:                               # %_ZN2kcL16f_do_alternativeEPNS_17impl_alternativesEii.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end34:
	.size	_ZN2kc13f_alternativeEPNS_17impl_alternativesEi, .Lfunc_end34-_ZN2kc13f_alternativeEPNS_17impl_alternativesEi
	.cfi_endproc

	.globl	_ZN2kc31v_reset_phylumdeclaration_marksEv
	.p2align	4, 0x90
	.type	_ZN2kc31v_reset_phylumdeclaration_marksEv,@function
_ZN2kc31v_reset_phylumdeclaration_marksEv: # @_ZN2kc31v_reset_phylumdeclaration_marksEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 16
.Lcfi138:
	.cfi_offset %rbx, -16
	movq	Thephylumdeclarations(%rip), %rbx
	jmp	.LBB35_2
	.p2align	4, 0x90
.LBB35_1:                               # %.lr.ph
                                        #   in Loop: Header=BB35_2 Depth=1
	movq	8(%rbx), %rax
	movl	$0, 8(%rax)
	movq	16(%rbx), %rbx
.LBB35_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$12, %eax
	je	.LBB35_1
# BB#3:                                 # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end35:
	.size	_ZN2kc31v_reset_phylumdeclaration_marksEv, .Lfunc_end35-_ZN2kc31v_reset_phylumdeclaration_marksEv
	.cfi_endproc

	.globl	_ZN2kc22v_reset_variables_typeEPNS_14impl_variablesE
	.p2align	4, 0x90
	.type	_ZN2kc22v_reset_variables_typeEPNS_14impl_variablesE,@function
_ZN2kc22v_reset_variables_typeEPNS_14impl_variablesE: # @_ZN2kc22v_reset_variables_typeEPNS_14impl_variablesE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi143:
	.cfi_def_cfa_offset 48
.Lcfi144:
	.cfi_offset %rbx, -40
.Lcfi145:
	.cfi_offset %r12, -32
.Lcfi146:
	.cfi_offset %r14, -24
.Lcfi147:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	jmp	.LBB36_1
	.p2align	4, 0x90
.LBB36_26:                              #   in Loop: Header=BB36_1 Depth=1
	movq	16(%r15), %r15
.LBB36_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$192, %eax
	jne	.LBB36_27
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB36_1 Depth=1
	movq	8(%r15), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB36_24
# BB#3:                                 #   in Loop: Header=BB36_1 Depth=1
	movq	40(%rbx), %rbx
	movq	8(%rbx), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$185, %eax
	jne	.LBB36_11
# BB#4:                                 #   in Loop: Header=BB36_1 Depth=1
	movq	16(%r14), %r12
	movq	32(%rbx), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$170, %eax
	jne	.LBB36_8
# BB#5:                                 #   in Loop: Header=BB36_1 Depth=1
	movq	8(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$171, %eax
	jne	.LBB36_8
# BB#6:                                 #   in Loop: Header=BB36_1 Depth=1
	movq	8(%r14), %rax
	cmpq	%r12, 8(%rax)
	jne	.LBB36_26
# BB#7:                                 #   in Loop: Header=BB36_1 Depth=1
	movq	16(%rax), %rcx
	movq	24(%rax), %rdx
	movq	16(%r14), %rsi
	movq	32(%rax), %rax
	movq	%rcx, 8(%rbx)
	movq	%rdx, 24(%rbx)
	movl	8(%rax), %eax
	movl	%eax, 16(%rbx)
	movq	%rsi, 32(%rbx)
	movq	16(%r15), %r15
	jmp	.LBB36_1
	.p2align	4, 0x90
.LBB36_24:                              #   in Loop: Header=BB36_1 Depth=1
	movl	$.L.str.31, %edi
	movl	$1135, %esi             # imm = 0x46F
.LBB36_25:                              #   in Loop: Header=BB36_1 Depth=1
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	jmp	.LBB36_26
	.p2align	4, 0x90
.LBB36_11:                              #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$183, %eax
	je	.LBB36_26
# BB#12:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$182, %eax
	je	.LBB36_26
# BB#13:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$180, %eax
	je	.LBB36_26
# BB#14:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$179, %eax
	je	.LBB36_26
# BB#15:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$184, %eax
	je	.LBB36_26
# BB#16:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$178, %eax
	je	.LBB36_26
# BB#17:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$177, %eax
	je	.LBB36_26
# BB#18:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	je	.LBB36_26
# BB#19:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$175, %eax
	je	.LBB36_26
# BB#20:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	je	.LBB36_26
# BB#21:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	je	.LBB36_26
# BB#22:                                #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$172, %eax
	je	.LBB36_26
# BB#23:                                #   in Loop: Header=BB36_1 Depth=1
	movl	$.L.str.31, %edi
	movl	$1131, %esi             # imm = 0x46B
	jmp	.LBB36_25
.LBB36_8:                               #   in Loop: Header=BB36_1 Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$169, %eax
	jne	.LBB36_10
# BB#9:                                 #   in Loop: Header=BB36_1 Depth=1
	callq	_ZN2kc9ITUnknownEv
	movq	%rax, 8(%rbx)
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, 24(%rbx)
	movl	$0, 16(%rbx)
	movq	16(%r15), %r15
	jmp	.LBB36_1
.LBB36_10:                              #   in Loop: Header=BB36_1 Depth=1
	movl	$.L.str.31, %edi
	movl	$1091, %esi             # imm = 0x443
	jmp	.LBB36_25
.LBB36_27:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end36:
	.size	_ZN2kc22v_reset_variables_typeEPNS_14impl_variablesE, .Lfunc_end36-_ZN2kc22v_reset_variables_typeEPNS_14impl_variablesE
	.cfi_endproc

	.globl	_ZN2kc19v_add_to_uviewnamesEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc19v_add_to_uviewnamesEPNS_7impl_IDE,@function
_ZN2kc19v_add_to_uviewnamesEPNS_7impl_IDE: # @_ZN2kc19v_add_to_uviewnamesEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi150:
	.cfi_def_cfa_offset 32
.Lcfi151:
	.cfi_offset %rbx, -24
.Lcfi152:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	Theuviewnames(%rip), %rbx
	jmp	.LBB37_1
	.p2align	4, 0x90
.LBB37_3:                               #   in Loop: Header=BB37_1 Depth=1
	movq	24(%rbx), %rbx
.LBB37_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$145, %eax
	jne	.LBB37_2
# BB#4:                                 #   in Loop: Header=BB37_1 Depth=1
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB37_3
	jmp	.LBB37_5
.LBB37_2:                               # %._crit_edge
	movq	Theuviewnames(%rip), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc13ConsviewnamesEPNS_7impl_IDEPNS_14impl_viewnamesE
	movq	%rax, Theuviewnames(%rip)
.LBB37_5:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end37:
	.size	_ZN2kc19v_add_to_uviewnamesEPNS_7impl_IDE, .Lfunc_end37-_ZN2kc19v_add_to_uviewnamesEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc23v_add_to_uviewnames_extEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc23v_add_to_uviewnames_extEPNS_7impl_IDE,@function
_ZN2kc23v_add_to_uviewnames_extEPNS_7impl_IDE: # @_ZN2kc23v_add_to_uviewnames_extEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 32
.Lcfi156:
	.cfi_offset %rbx, -32
.Lcfi157:
	.cfi_offset %r14, -24
.Lcfi158:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	Theuviewnames(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB38_3
# BB#1:
	xorl	%esi, %esi
	jmp	.LBB38_8
	.p2align	4, 0x90
.LBB38_2:                               # %.thread28
                                        #   in Loop: Header=BB38_3 Depth=1
	movl	$.L.str.32, %edi
	movl	$1199, %esi             # imm = 0x4AF
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB38_3:                               # %.lr.ph.split.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$144, %eax
	je	.LBB38_7
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB38_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$145, %eax
	jne	.LBB38_2
# BB#5:                                 # %.us-lcssa.us
                                        #   in Loop: Header=BB38_3 Depth=1
	movq	16(%rbx), %rdi
	movq	24(%rbx), %r15
	movq	%r14, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB38_9
# BB#6:                                 # %.thread28.outer.backedge
                                        #   in Loop: Header=BB38_3 Depth=1
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB38_3
.LBB38_7:                               # %.thread28.outer._crit_edge.loopexit
	movq	Theuviewnames(%rip), %rsi
.LBB38_8:                               # %.thread28.outer._crit_edge
	movq	%r14, %rdi
	callq	_ZN2kc13ConsviewnamesEPNS_7impl_IDEPNS_14impl_viewnamesE
	movq	%rax, %rbx
	movq	%rbx, Theuviewnames(%rip)
.LBB38_10:                              # %.sink.split
	addq	$8, %rbx
	movb	$1, (%rbx)
.LBB38_11:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB38_9:
	cmpb	$0, 8(%rbx)
	jne	.LBB38_11
	jmp	.LBB38_10
.Lfunc_end38:
	.size	_ZN2kc23v_add_to_uviewnames_extEPNS_7impl_IDE, .Lfunc_end38-_ZN2kc23v_add_to_uviewnames_extEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc19v_add_to_rviewnamesEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc19v_add_to_rviewnamesEPNS_7impl_IDE,@function
_ZN2kc19v_add_to_rviewnamesEPNS_7impl_IDE: # @_ZN2kc19v_add_to_rviewnamesEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	Therviewnames(%rip), %rbx
	jmp	.LBB39_1
	.p2align	4, 0x90
.LBB39_3:                               #   in Loop: Header=BB39_1 Depth=1
	movq	24(%rbx), %rbx
.LBB39_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$145, %eax
	jne	.LBB39_2
# BB#4:                                 #   in Loop: Header=BB39_1 Depth=1
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB39_3
	jmp	.LBB39_5
.LBB39_2:                               # %._crit_edge
	movq	Therviewnames(%rip), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc13ConsviewnamesEPNS_7impl_IDEPNS_14impl_viewnamesE
	movq	%rax, Therviewnames(%rip)
.LBB39_5:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end39:
	.size	_ZN2kc19v_add_to_rviewnamesEPNS_7impl_IDE, .Lfunc_end39-_ZN2kc19v_add_to_rviewnamesEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc23v_add_to_rviewnames_extEPNS_7impl_IDE
	.p2align	4, 0x90
	.type	_ZN2kc23v_add_to_rviewnames_extEPNS_7impl_IDE,@function
_ZN2kc23v_add_to_rviewnames_extEPNS_7impl_IDE: # @_ZN2kc23v_add_to_rviewnames_extEPNS_7impl_IDE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 32
.Lcfi167:
	.cfi_offset %rbx, -32
.Lcfi168:
	.cfi_offset %r14, -24
.Lcfi169:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	Therviewnames(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB40_3
# BB#1:
	xorl	%esi, %esi
	jmp	.LBB40_8
	.p2align	4, 0x90
.LBB40_2:                               # %.thread28
                                        #   in Loop: Header=BB40_3 Depth=1
	movl	$.L.str.33, %edi
	movl	$1257, %esi             # imm = 0x4E9
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB40_3:                               # %.lr.ph.split.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$144, %eax
	je	.LBB40_7
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$145, %eax
	jne	.LBB40_2
# BB#5:                                 # %.us-lcssa.us
                                        #   in Loop: Header=BB40_3 Depth=1
	movq	16(%rbx), %rdi
	movq	24(%rbx), %r15
	movq	%r14, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB40_9
# BB#6:                                 # %.thread28.outer.backedge
                                        #   in Loop: Header=BB40_3 Depth=1
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB40_3
.LBB40_7:                               # %.thread28.outer._crit_edge.loopexit
	movq	Therviewnames(%rip), %rsi
.LBB40_8:                               # %.thread28.outer._crit_edge
	movq	%r14, %rdi
	callq	_ZN2kc13ConsviewnamesEPNS_7impl_IDEPNS_14impl_viewnamesE
	movq	%rax, %rbx
	movq	%rbx, Therviewnames(%rip)
.LBB40_10:                              # %.sink.split
	addq	$8, %rbx
	movb	$1, (%rbx)
.LBB40_11:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB40_9:
	cmpb	$0, 8(%rbx)
	jne	.LBB40_11
	jmp	.LBB40_10
.Lfunc_end40:
	.size	_ZN2kc23v_add_to_rviewnames_extEPNS_7impl_IDE, .Lfunc_end40-_ZN2kc23v_add_to_rviewnames_extEPNS_7impl_IDE
	.cfi_endproc

	.globl	_ZN2kc23v_add_to_storageclassesEPNS_7impl_IDES1_
	.p2align	4, 0x90
	.type	_ZN2kc23v_add_to_storageclassesEPNS_7impl_IDES1_,@function
_ZN2kc23v_add_to_storageclassesEPNS_7impl_IDES1_: # @_ZN2kc23v_add_to_storageclassesEPNS_7impl_IDES1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi172:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi173:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi174:
	.cfi_def_cfa_offset 48
.Lcfi175:
	.cfi_offset %rbx, -48
.Lcfi176:
	.cfi_offset %r12, -40
.Lcfi177:
	.cfi_offset %r13, -32
.Lcfi178:
	.cfi_offset %r14, -24
.Lcfi179:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	Thestorageclasses(%rip), %r15
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB41_1:                               # %tailrecurse.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r13
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$20, %eax
	jne	.LBB41_7
# BB#2:                                 #   in Loop: Header=BB41_1 Depth=1
	movq	16(%r13), %rdi
	movq	24(%r13), %rbx
	movq	%r12, %rsi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	je	.LBB41_1
# BB#3:
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rsi
	testq	%rsi, %rsi
	jne	.LBB41_5
# BB#4:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rsi
	movq	%rsi, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB41_5:                               # %_ZN2kc9f_emptyIdEv.exit.i
	movq	%r14, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB41_13
# BB#6:
	movq	8(%r13), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, 8(%r13)
	jmp	.LBB41_13
.LBB41_7:
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$19, %eax
	jne	.LBB41_12
# BB#8:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc18ConsstorageclassesEPNS_7impl_IDEPNS_19impl_storageclassesE
	movq	%rax, %r15
	callq	_ZN2kc14NilphylumnamesEv
	movq	%rax, 8(%r15)
	movq	_ZZN2kc9f_emptyIdEvE7emptyID(%rip), %rsi
	testq	%rsi, %rsi
	jne	.LBB41_10
# BB#9:
	movl	$.L.str.6, %edi
	movl	$-1, %esi
	callq	_ZN2kc12mkcasestringEPKci
	movq	%rax, %rdi
	callq	_ZN2kc3StrEPNS_20impl_casestring__StrE
	movq	%rax, %rdi
	callq	_ZN2kc2IdEPNS_11impl_uniqIDE
	movq	%rax, %rsi
	movq	%rsi, _ZZN2kc9f_emptyIdEvE7emptyID(%rip)
.LBB41_10:                              # %_ZN2kc9f_emptyIdEv.exit32.i
	movq	%r14, %rdi
	callq	_ZNK2kc20impl_abstract_phylum2eqEPKS0_
	testb	%al, %al
	jne	.LBB41_13
# BB#11:
	movq	8(%r15), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE
	movq	%rax, 8(%r15)
	jmp	.LBB41_13
.LBB41_12:
	movl	$.L.str.53, %edi
	movl	$1298, %esi             # imm = 0x512
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%r15d, %r15d
.LBB41_13:                              # %_ZN2kcL24do_add_to_storageclassesEPNS_7impl_IDES1_PNS_19impl_storageclassesES3_.exit
	movq	%r15, Thestorageclasses(%rip)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end41:
	.size	_ZN2kc23v_add_to_storageclassesEPNS_7impl_IDES1_, .Lfunc_end41-_ZN2kc23v_add_to_storageclassesEPNS_7impl_IDES1_
	.cfi_endproc

	.globl	_ZN2kc15collect_stringsEv
	.p2align	4, 0x90
	.type	_ZN2kc15collect_stringsEv,@function
_ZN2kc15collect_stringsEv:              # @_ZN2kc15collect_stringsEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi180:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi181:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi182:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi183:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi184:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi185:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi186:
	.cfi_def_cfa_offset 80
.Lcfi187:
	.cfi_offset %rbx, -56
.Lcfi188:
	.cfi_offset %r12, -48
.Lcfi189:
	.cfi_offset %r13, -40
.Lcfi190:
	.cfi_offset %r14, -32
.Lcfi191:
	.cfi_offset %r15, -24
.Lcfi192:
	.cfi_offset %rbp, -16
	callq	_ZN2kc12last_text_nrEv
	movq	%rax, %r13
	leaq	1(%r13), %r14
	movq	Thelanguages(%rip), %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, (%rsp)            # 8-byte Spill
	callq	_ZN2kc15NilunparseitemsEv
	movq	%rax, %r12
	movq	%r12, _ZL19The_Nilunparseitems(%rip)
	movl	$8, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rbp
	cmovoq	%rbp, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, _ZL17string_collection(%rip)
	testq	%r13, %r13
	js	.LBB42_13
# BB#1:                                 # %.lr.ph22
	movq	(%rsp), %rsi            # 8-byte Reload
	movslq	%esi, %rcx
	movl	$8, %edx
	movq	%rcx, %rax
	mulq	%rdx
	cmovoq	%rbp, %rax
	testl	%ecx, %ecx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jle	.LBB42_12
# BB#2:                                 # %.lr.ph22.split.us.preheader
	movl	%esi, %ebp
	leal	7(%rsi), %r15d
	leaq	-2(%rbp), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	andl	$7, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB42_3:                               # %.lr.ph22.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB42_7 Depth 2
                                        #     Child Loop BB42_10 Depth 2
	movq	%rax, %rdi
	callq	_Znam
	cmpl	$1, (%rsp)              # 4-byte Folded Reload
	movq	%rax, (%rbx,%r13,8)
	movq	%r12, (%rax)
	je	.LBB42_11
# BB#4:                                 # %._crit_edge.preheader
                                        #   in Loop: Header=BB42_3 Depth=1
	testq	%r15, %r15
	je	.LBB42_5
# BB#6:                                 # %._crit_edge.prol.preheader
                                        #   in Loop: Header=BB42_3 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB42_7:                               # %._crit_edge.prol
                                        #   Parent Loop BB42_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r13,8), %rcx
	movq	%r12, 8(%rcx,%rax,8)
	incq	%rax
	cmpq	%rax, %r15
	jne	.LBB42_7
# BB#8:                                 # %._crit_edge.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB42_3 Depth=1
	incq	%rax
	cmpq	$7, 16(%rsp)            # 8-byte Folded Reload
	jae	.LBB42_10
	jmp	.LBB42_11
.LBB42_5:                               #   in Loop: Header=BB42_3 Depth=1
	movl	$1, %eax
	cmpq	$7, 16(%rsp)            # 8-byte Folded Reload
	jb	.LBB42_11
	.p2align	4, 0x90
.LBB42_10:                              # %._crit_edge
                                        #   Parent Loop BB42_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%r13,8), %rcx
	movq	%r12, (%rcx,%rax,8)
	movq	(%rbx,%r13,8), %rcx
	movq	%r12, 8(%rcx,%rax,8)
	movq	(%rbx,%r13,8), %rcx
	movq	%r12, 16(%rcx,%rax,8)
	movq	(%rbx,%r13,8), %rcx
	movq	%r12, 24(%rcx,%rax,8)
	movq	(%rbx,%r13,8), %rcx
	movq	%r12, 32(%rcx,%rax,8)
	movq	(%rbx,%r13,8), %rcx
	movq	%r12, 40(%rcx,%rax,8)
	movq	(%rbx,%r13,8), %rcx
	movq	%r12, 48(%rcx,%rax,8)
	movq	(%rbx,%r13,8), %rcx
	movq	%r12, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %rbp
	jne	.LBB42_10
.LBB42_11:                              # %._crit_edge.us
                                        #   in Loop: Header=BB42_3 Depth=1
	incq	%r13
	cmpq	%r14, %r13
	movq	8(%rsp), %rax           # 8-byte Reload
	jne	.LBB42_3
	jmp	.LBB42_13
	.p2align	4, 0x90
.LBB42_12:                              # %.lr.ph22.split
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_Znam
	movq	%rax, (%rbx)
	addq	$8, %rbx
	decq	%r14
	jne	.LBB42_12
.LBB42_13:                              # %._crit_edge23
	movq	Theunparsedeclarations(%rip), %rdi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	movl	$v_null_printer, %esi
	movl	$_ZN2kc20view_collect_stringsE, %edx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end42:
	.size	_ZN2kc15collect_stringsEv, .Lfunc_end42-_ZN2kc15collect_stringsEv
	.cfi_endproc

	.globl	_ZN2kc24add_string_to_collectionEPNS_16impl_unparseitemE
	.p2align	4, 0x90
	.type	_ZN2kc24add_string_to_collectionEPNS_16impl_unparseitemE,@function
_ZN2kc24add_string_to_collectionEPNS_16impl_unparseitemE: # @_ZN2kc24add_string_to_collectionEPNS_16impl_unparseitemE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi195:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi196:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi197:
	.cfi_def_cfa_offset 48
.Lcfi198:
	.cfi_offset %rbx, -48
.Lcfi199:
	.cfi_offset %r12, -40
.Lcfi200:
	.cfi_offset %r13, -32
.Lcfi201:
	.cfi_offset %r14, -24
.Lcfi202:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*(%rax)
	cmpl	$148, %eax
	jne	.LBB43_14
# BB#1:
	movq	16(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$163, %eax
	jne	.LBB43_14
# BB#2:
	movq	16(%r14), %rax
	movslq	8(%r14), %r13
	movq	8(%rax), %r12
	jmp	.LBB43_3
	.p2align	4, 0x90
.LBB43_13:                              #   in Loop: Header=BB43_3 Depth=1
	movq	16(%r12), %r12
.LBB43_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$165, %eax
	jne	.LBB43_19
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB43_3 Depth=1
	movq	8(%r12), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB43_13
# BB#5:                                 #   in Loop: Header=BB43_3 Depth=1
	movq	40(%rbx), %rax
	movq	8(%rax), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$186, %eax
	jne	.LBB43_13
# BB#6:                                 #   in Loop: Header=BB43_3 Depth=1
	movq	8(%rbx), %rbx
	movq	_ZL17string_collection(%rip), %rax
	movq	(%rax,%r13,8), %rax
	movslq	8(%rbx), %rcx
	movq	(%rax,%rcx,8), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$147, %eax
	jne	.LBB43_11
# BB#7:                                 #   in Loop: Header=BB43_3 Depth=1
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$148, %eax
	jne	.LBB43_11
# BB#8:                                 #   in Loop: Header=BB43_3 Depth=1
	movq	8(%r15), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$162, %eax
	jne	.LBB43_11
# BB#9:                                 #   in Loop: Header=BB43_3 Depth=1
	movq	16(%r15), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$146, %eax
	jne	.LBB43_11
# BB#10:                                #   in Loop: Header=BB43_3 Depth=1
	movq	_ZL19The_Nilunparseitems(%rip), %r15
	jmp	.LBB43_12
	.p2align	4, 0x90
.LBB43_11:                              #   in Loop: Header=BB43_3 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$146, %eax
	cmoveq	_ZL19The_Nilunparseitems(%rip), %r15
.LBB43_12:                              #   in Loop: Header=BB43_3 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN2kc16ConsunparseitemsEPNS_16impl_unparseitemEPNS_17impl_unparseitemsE
	movq	_ZL17string_collection(%rip), %rcx
	movq	(%rcx,%r13,8), %rcx
	movslq	8(%rbx), %rdx
	movq	%rax, (%rcx,%rdx,8)
	jmp	.LBB43_13
.LBB43_14:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$148, %eax
	jne	.LBB43_20
# BB#15:
	movq	16(%r14), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$162, %eax
	jne	.LBB43_20
# BB#16:
	movq	Thelanguages(%rip), %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	testl	%eax, %eax
	jle	.LBB43_19
# BB#17:                                # %.lr.ph84.preheader
	movslq	8(%r14), %r12
	movq	_ZL17string_collection(%rip), %rcx
	movl	%eax, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB43_18:                              # %.lr.ph84
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%r12,8), %rax
	movq	(%rax,%rbx,8), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc16ConsunparseitemsEPNS_16impl_unparseitemEPNS_17impl_unparseitemsE
	movq	_ZL17string_collection(%rip), %rcx
	movq	(%rcx,%r12,8), %rdx
	movq	%rax, (%rdx,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB43_18
.LBB43_19:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB43_20:
	movl	$.L.str.34, %edi
	movl	$1393, %esi             # imm = 0x571
	movl	$.L.str.1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_ZN2kc21kc_no_default_in_withEPKciS1_ # TAILCALL
.Lfunc_end43:
	.size	_ZN2kc24add_string_to_collectionEPNS_16impl_unparseitemE, .Lfunc_end43-_ZN2kc24add_string_to_collectionEPNS_16impl_unparseitemE
	.cfi_endproc

	.globl	_ZN2kc25unparse_string_collectionEv
	.p2align	4, 0x90
	.type	_ZN2kc25unparse_string_collectionEv,@function
_ZN2kc25unparse_string_collectionEv:    # @_ZN2kc25unparse_string_collectionEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi203:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi204:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi205:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi206:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi207:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi208:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi209:
	.cfi_def_cfa_offset 64
.Lcfi210:
	.cfi_offset %rbx, -56
.Lcfi211:
	.cfi_offset %r12, -48
.Lcfi212:
	.cfi_offset %r13, -40
.Lcfi213:
	.cfi_offset %r14, -32
.Lcfi214:
	.cfi_offset %r15, -24
.Lcfi215:
	.cfi_offset %rbp, -16
	movq	Thelanguages(%rip), %rdi
	callq	_ZNK2kc18impl_abstract_list6lengthEv
	movl	%eax, %ebx
	callq	_ZN2kc12last_text_nrEv
	movq	%rax, %r15
	movq	Thelanguages(%rip), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$165, %eax
	jne	.LBB44_6
# BB#1:                                 # %.lr.ph39
	testq	%r15, %r15
	js	.LBB44_10
# BB#2:                                 # %.lr.ph39.split.preheader
	movslq	%ebx, %r13
	incq	%r15
	.p2align	4, 0x90
.LBB44_3:                               # %.lr.ph39.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB44_4 Depth 2
	movq	8(%r14), %rdi
	leaq	-1(%r13), %rbp
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc22view_output_collectionE, %edx
	callq	*72(%rax)
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB44_4:                               #   Parent Loop BB44_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	_ZL17string_collection(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movq	-8(%rax,%r13,8), %r12
	movl	$_ZZN2kc25unparse_string_collectionEvE3buf, %edi
	movl	$.L.str.35, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	sprintf
	movl	$v_ccfile_printer, %edi
	movl	$_ZZN2kc25unparse_string_collectionEvE3buf, %esi
	movl	$_ZN2kc10base_uviewE, %edx
	callq	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE
	movq	(%r12), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc22view_output_collectionE, %edx
	movq	%r12, %rdi
	callq	*72(%rax)
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.36, %esi
	movl	$_ZN2kc10base_uviewE, %edx
	callq	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB44_4
# BB#5:                                 # %._crit_edge
                                        #   in Loop: Header=BB44_3 Depth=1
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.37, %esi
	movl	$_ZN2kc10base_uviewE, %edx
	callq	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE
	movq	16(%r14), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$165, %eax
	movq	%rbp, %r13
	je	.LBB44_3
	jmp	.LBB44_6
	.p2align	4, 0x90
.LBB44_10:                              # %.lr.ph39.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc22view_output_collectionE, %edx
	callq	*72(%rax)
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.37, %esi
	movl	$_ZN2kc10base_uviewE, %edx
	callq	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE
	movq	16(%r14), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$165, %eax
	je	.LBB44_10
.LBB44_6:                               # %._crit_edge40
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.38, %esi
	movl	$_ZN2kc10base_uviewE, %edx
	callq	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE
	movq	Thelanguages(%rip), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$165, %eax
	jne	.LBB44_8
# BB#7:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$v_ccfile_printer, %esi
	movl	$_ZN2kc10base_uviewE, %edx
	callq	*72(%rax)
	jmp	.LBB44_9
.LBB44_8:
	movl	$.L.str.39, %edi
	movl	$1444, %esi             # imm = 0x5A4
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB44_9:
	movl	$v_ccfile_printer, %edi
	movl	$.L.str.40, %esi
	movl	$_ZN2kc10base_uviewE, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN14kc_filePrinterclEPKcRN2kc11uview_classE # TAILCALL
.Lfunc_end44:
	.size	_ZN2kc25unparse_string_collectionEv, .Lfunc_end44-_ZN2kc25unparse_string_collectionEv
	.cfi_endproc

	.globl	_ZN2kc30f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE
	.p2align	4, 0x90
	.type	_ZN2kc30f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE,@function
_ZN2kc30f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE: # @_ZN2kc30f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE
	.cfi_startproc
# BB#0:
	jmp	_ZN2kc32t_f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE # TAILCALL
.Lfunc_end45:
	.size	_ZN2kc30f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE, .Lfunc_end45-_ZN2kc30f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE
	.cfi_endproc

	.globl	_ZN2kc32t_f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE
	.p2align	4, 0x90
	.type	_ZN2kc32t_f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE,@function
_ZN2kc32t_f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE: # @_ZN2kc32t_f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi216:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi217:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi218:
	.cfi_def_cfa_offset 32
.Lcfi219:
	.cfi_offset %rbx, -24
.Lcfi220:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$55, %eax
	jne	.LBB46_2
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$56, %eax
	jne	.LBB46_2
# BB#4:
	movq	8(%rbx), %rax
	movq	16(%rbx), %r14
	movq	16(%rax), %rdi
	callq	_ZN2kc19f_listelementphylumEPNS_7impl_IDE
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZN2kc32t_f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE # TAILCALL
.LBB46_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$54, %eax
	jne	.LBB46_3
# BB#5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN2kc14NilphylumnamesEv # TAILCALL
.LBB46_3:
	movl	$.L.str.41, %edi
	movl	$1471, %esi             # imm = 0x5BF
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end46:
	.size	_ZN2kc32t_f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE, .Lfunc_end46-_ZN2kc32t_f_phylumnames_foreachwith_varsEPNS_19impl_idCexpressionsE
	.cfi_endproc

	.globl	_ZN2kc34f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	.p2align	4, 0x90
	.type	_ZN2kc34f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE,@function
_ZN2kc34f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE: # @_ZN2kc34f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	.cfi_startproc
# BB#0:
	jmp	_ZN2kc36t_f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE # TAILCALL
.Lfunc_end47:
	.size	_ZN2kc34f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE, .Lfunc_end47-_ZN2kc34f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	.cfi_endproc

	.globl	_ZN2kc36t_f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	.p2align	4, 0x90
	.type	_ZN2kc36t_f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE,@function
_ZN2kc36t_f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE: # @_ZN2kc36t_f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi221:
	.cfi_def_cfa_offset 16
.Lcfi222:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$55, %eax
	jne	.LBB48_2
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$56, %eax
	jne	.LBB48_2
# BB#4:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdi
	movq	16(%rax), %rbx
	callq	_ZN2kc36t_f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	jmp	_ZN2kc15ConsphylumnamesEPNS_7impl_IDEPNS_16impl_phylumnamesE # TAILCALL
.LBB48_2:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$54, %eax
	jne	.LBB48_3
# BB#5:
	popq	%rbx
	jmp	_ZN2kc14NilphylumnamesEv # TAILCALL
.LBB48_3:
	movl	$.L.str.42, %edi
	movl	$1498, %esi             # imm = 0x5DA
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end48:
	.size	_ZN2kc36t_f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE, .Lfunc_end48-_ZN2kc36t_f_phylumnames_foreachwith_listvarsEPNS_19impl_idCexpressionsE
	.cfi_endproc

	.globl	_ZN2kc17f_collect_membersEPNS_19impl_fndeclarationsE
	.p2align	4, 0x90
	.type	_ZN2kc17f_collect_membersEPNS_19impl_fndeclarationsE,@function
_ZN2kc17f_collect_membersEPNS_19impl_fndeclarationsE: # @_ZN2kc17f_collect_membersEPNS_19impl_fndeclarationsE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi223:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi224:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi225:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi226:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi227:
	.cfi_def_cfa_offset 48
.Lcfi228:
	.cfi_offset %rbx, -40
.Lcfi229:
	.cfi_offset %r12, -32
.Lcfi230:
	.cfi_offset %r14, -24
.Lcfi231:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	jmp	.LBB49_1
	.p2align	4, 0x90
.LBB49_99:                              #   in Loop: Header=BB49_1 Depth=1
	movq	16(%r14), %r14
.LBB49_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*(%rax)
	cmpl	$108, %eax
	jne	.LBB49_100
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r14), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB49_22
# BB#3:                                 #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB49_22
# BB#4:                                 #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	jne	.LBB49_22
# BB#5:                                 #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$273, %eax              # imm = 0x111
	jne	.LBB49_22
# BB#6:                                 #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$272, %eax              # imm = 0x110
	jne	.LBB49_22
# BB#7:                                 #   in Loop: Header=BB49_1 Depth=1
	movq	88(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$113, %eax
	jne	.LBB49_22
# BB#8:                                 #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_13
# BB#9:                                 #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB49_11
# BB#10:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_15
	.p2align	4, 0x90
.LBB49_22:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB49_41
# BB#23:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB49_41
# BB#24:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	jne	.LBB49_41
# BB#25:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$255, %eax
	jne	.LBB49_41
# BB#26:                                #   in Loop: Header=BB49_1 Depth=1
	movq	88(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$115, %eax
	jne	.LBB49_41
# BB#27:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_32
# BB#28:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB49_30
# BB#29:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_34
	.p2align	4, 0x90
.LBB49_41:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB49_60
# BB#42:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB49_60
# BB#43:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	jne	.LBB49_60
# BB#44:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$255, %eax
	jne	.LBB49_60
# BB#45:                                #   in Loop: Header=BB49_1 Depth=1
	movq	88(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$114, %eax
	jne	.LBB49_60
# BB#46:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_51
# BB#47:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB49_49
# BB#48:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_53
	.p2align	4, 0x90
.LBB49_60:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$109, %eax
	jne	.LBB49_79
# BB#61:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB49_79
# BB#62:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	jne	.LBB49_79
# BB#63:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$261, %eax              # imm = 0x105
	jne	.LBB49_79
# BB#64:                                #   in Loop: Header=BB49_1 Depth=1
	movq	88(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$116, %eax
	jne	.LBB49_79
# BB#65:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_70
# BB#66:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB49_68
# BB#67:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_72
	.p2align	4, 0x90
.LBB49_79:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$110, %eax
	jne	.LBB49_99
# BB#80:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB49_99
# BB#81:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$260, %eax              # imm = 0x104
	jne	.LBB49_99
# BB#82:                                #   in Loop: Header=BB49_1 Depth=1
	movq	48(%rbx), %rax
	movq	24(%rax), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_87
# BB#83:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB49_85
# BB#84:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_89
.LBB49_87:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.9, %edi
	movl	$368, %esi              # imm = 0x170
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	jmp	.LBB49_88
.LBB49_85:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB49_88
# BB#86:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_89
.LBB49_88:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%r12d, %r12d
.LBB49_89:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit
                                        #   in Loop: Header=BB49_1 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_92
# BB#90:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB49_93
# BB#91:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r15), %rax
	testq	%r12, %r12
	jne	.LBB49_95
	jmp	.LBB49_96
.LBB49_92:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.11, %edi
	movl	$415, %esi              # imm = 0x19F
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB49_93:                              # %_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE.exit
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.LBB49_96
.LBB49_95:                              #   in Loop: Header=BB49_1 Depth=1
	addq	$16, %r12
	jmp	.LBB49_98
.LBB49_32:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.9, %edi
	movl	$368, %esi              # imm = 0x170
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB49_33:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit154
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%r12d, %r12d
.LBB49_34:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit154
                                        #   in Loop: Header=BB49_1 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_37
# BB#35:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB49_38
# BB#36:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r15), %rax
	testq	%r12, %r12
	jne	.LBB49_40
	jmp	.LBB49_96
.LBB49_37:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.11, %edi
	movl	$415, %esi              # imm = 0x19F
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB49_38:                              # %_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE.exit156
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.LBB49_96
.LBB49_40:                              #   in Loop: Header=BB49_1 Depth=1
	addq	$16, %r12
	jmp	.LBB49_98
.LBB49_51:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.9, %edi
	movl	$368, %esi              # imm = 0x170
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB49_52:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit160
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%r12d, %r12d
.LBB49_53:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit160
                                        #   in Loop: Header=BB49_1 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_56
# BB#54:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB49_57
# BB#55:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r15), %rax
	testq	%r12, %r12
	jne	.LBB49_59
	jmp	.LBB49_96
.LBB49_56:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.11, %edi
	movl	$415, %esi              # imm = 0x19F
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB49_57:                              # %_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE.exit158
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.LBB49_96
.LBB49_59:                              #   in Loop: Header=BB49_1 Depth=1
	addq	$16, %r12
	jmp	.LBB49_98
.LBB49_70:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.9, %edi
	movl	$368, %esi              # imm = 0x170
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	jmp	.LBB49_71
.LBB49_13:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.9, %edi
	movl	$368, %esi              # imm = 0x170
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB49_14:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit150
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%r12d, %r12d
.LBB49_15:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit150
                                        #   in Loop: Header=BB49_1 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_18
# BB#16:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB49_19
# BB#17:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r15), %rax
	testq	%r12, %r12
	jne	.LBB49_21
	jmp	.LBB49_96
.LBB49_30:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB49_33
# BB#31:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_34
.LBB49_18:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.11, %edi
	movl	$415, %esi              # imm = 0x19F
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB49_19:                              # %_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE.exit152
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.LBB49_96
.LBB49_21:                              #   in Loop: Header=BB49_1 Depth=1
	addq	$16, %r12
	jmp	.LBB49_98
.LBB49_49:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB49_52
# BB#50:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_53
.LBB49_68:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB49_71
# BB#69:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_72
.LBB49_71:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit148
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%r12d, %r12d
.LBB49_72:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit148
                                        #   in Loop: Header=BB49_1 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB49_75
# BB#73:                                #   in Loop: Header=BB49_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB49_76
# BB#74:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r15), %rax
	testq	%r12, %r12
	jne	.LBB49_78
	jmp	.LBB49_96
.LBB49_75:                              #   in Loop: Header=BB49_1 Depth=1
	movl	$.L.str.11, %edi
	movl	$415, %esi              # imm = 0x19F
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB49_76:                              # %_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE.exit146
                                        #   in Loop: Header=BB49_1 Depth=1
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.LBB49_96
.LBB49_78:                              #   in Loop: Header=BB49_1 Depth=1
	addq	$16, %r12
	jmp	.LBB49_98
.LBB49_96:                              #   in Loop: Header=BB49_1 Depth=1
	testq	%rax, %rax
	je	.LBB49_99
# BB#97:                                #   in Loop: Header=BB49_1 Depth=1
	addq	$24, %rax
	movq	%rax, %r12
.LBB49_98:                              # %.sink.split139
                                        #   in Loop: Header=BB49_1 Depth=1
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	callq	_ZN2kc18ConsfndeclarationsEPNS_18impl_fndeclarationEPNS_19impl_fndeclarationsE
	movq	%rax, (%r12)
	jmp	.LBB49_99
.LBB49_11:                              #   in Loop: Header=BB49_1 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB49_14
# BB#12:                                #   in Loop: Header=BB49_1 Depth=1
	movq	8(%r12), %r12
	jmp	.LBB49_15
.LBB49_100:                             # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end49:
	.size	_ZN2kc17f_collect_membersEPNS_19impl_fndeclarationsE, .Lfunc_end49-_ZN2kc17f_collect_membersEPNS_19impl_fndeclarationsE
	.cfi_endproc

	.globl	_ZN2kc22f_id_of_ctor_dtor_declEPNS_18impl_ac_declaratorE
	.p2align	4, 0x90
	.type	_ZN2kc22f_id_of_ctor_dtor_declEPNS_18impl_ac_declaratorE,@function
_ZN2kc22f_id_of_ctor_dtor_declEPNS_18impl_ac_declaratorE: # @_ZN2kc22f_id_of_ctor_dtor_declEPNS_18impl_ac_declaratorE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi232:
	.cfi_def_cfa_offset 16
.Lcfi233:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*(%rax)
	cmpl	$254, %eax
	jne	.LBB50_1
# BB#2:
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$259, %eax              # imm = 0x103
	jne	.LBB50_3
# BB#4:
	movq	24(%rbx), %rax
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	cmpl	$255, %eax
	jne	.LBB50_5
# BB#6:
	movq	24(%rbx), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	popq	%rbx
	retq
.LBB50_1:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB50_3:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB50_5:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end50:
	.size	_ZN2kc22f_id_of_ctor_dtor_declEPNS_18impl_ac_declaratorE, .Lfunc_end50-_ZN2kc22f_id_of_ctor_dtor_declEPNS_18impl_ac_declaratorE
	.cfi_endproc

	.globl	_ZN2kc20prepare_base_classesEPNS_27impl_baseclass_declarationsE
	.p2align	4, 0x90
	.type	_ZN2kc20prepare_base_classesEPNS_27impl_baseclass_declarationsE,@function
_ZN2kc20prepare_base_classesEPNS_27impl_baseclass_declarationsE: # @_ZN2kc20prepare_base_classesEPNS_27impl_baseclass_declarationsE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi236:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi237:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi238:
	.cfi_def_cfa_offset 48
.Lcfi239:
	.cfi_offset %rbx, -48
.Lcfi240:
	.cfi_offset %r12, -40
.Lcfi241:
	.cfi_offset %r13, -32
.Lcfi242:
	.cfi_offset %r14, -24
.Lcfi243:
	.cfi_offset %r15, -16
	movq	%rdi, %r13
	jmp	.LBB51_1
	.p2align	4, 0x90
.LBB51_20:                              #   in Loop: Header=BB51_1 Depth=1
	movq	16(%r13), %r13
.LBB51_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	%r13, %rdi
	callq	*(%rax)
	cmpl	$302, %eax              # imm = 0x12E
	jne	.LBB51_21
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB51_1 Depth=1
	movq	8(%r13), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$303, %eax              # imm = 0x12F
	jne	.LBB51_20
# BB#3:                                 #   in Loop: Header=BB51_1 Depth=1
	movq	8(%rbx), %r15
	movq	16(%rbx), %r14
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB51_8
# BB#4:                                 #   in Loop: Header=BB51_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r12
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$174, %eax
	jne	.LBB51_6
# BB#5:                                 #   in Loop: Header=BB51_1 Depth=1
	movq	8(%r12), %rbx
	jmp	.LBB51_10
	.p2align	4, 0x90
.LBB51_8:                               #   in Loop: Header=BB51_1 Depth=1
	movl	$.L.str.9, %edi
	movl	$368, %esi              # imm = 0x170
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
	jmp	.LBB51_9
.LBB51_6:                               #   in Loop: Header=BB51_1 Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*(%rax)
	cmpl	$173, %eax
	jne	.LBB51_9
# BB#7:                                 #   in Loop: Header=BB51_1 Depth=1
	movq	8(%r12), %rbx
	jmp	.LBB51_10
	.p2align	4, 0x90
.LBB51_9:                               # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit
                                        #   in Loop: Header=BB51_1 Depth=1
	xorl	%ebx, %ebx
.LBB51_10:                              # %_ZN2kc16f_phylumdeclofidEPNS_7impl_IDE.exit
                                        #   in Loop: Header=BB51_1 Depth=1
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$7, %eax
	jne	.LBB51_13
# BB#11:                                #   in Loop: Header=BB51_1 Depth=1
	movq	40(%r15), %rax
	movq	8(%rax), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*(%rax)
	cmpl	$176, %eax
	jne	.LBB51_14
# BB#12:                                #   in Loop: Header=BB51_1 Depth=1
	movq	8(%r15), %rax
	testq	%rbx, %rbx
	jne	.LBB51_16
	jmp	.LBB51_17
	.p2align	4, 0x90
.LBB51_13:                              #   in Loop: Header=BB51_1 Depth=1
	movl	$.L.str.11, %edi
	movl	$415, %esi              # imm = 0x19F
	movl	$.L.str.1, %edx
	callq	_ZN2kc21kc_no_default_in_withEPKciS1_
.LBB51_14:                              # %_ZN2kc23f_alternativeofoperatorEPNS_7impl_IDE.exit
                                        #   in Loop: Header=BB51_1 Depth=1
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB51_17
.LBB51_16:                              #   in Loop: Header=BB51_1 Depth=1
	addq	$24, %rbx
	jmp	.LBB51_19
.LBB51_17:                              #   in Loop: Header=BB51_1 Depth=1
	testq	%rax, %rax
	je	.LBB51_20
# BB#18:                                #   in Loop: Header=BB51_1 Depth=1
	addq	$32, %rax
	movq	%rax, %rbx
.LBB51_19:                              # %.sink.split
                                        #   in Loop: Header=BB51_1 Depth=1
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN2kc6concatEPKNS_19impl_baseclass_listES2_
	movq	%rax, (%rbx)
	jmp	.LBB51_20
.LBB51_21:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end51:
	.size	_ZN2kc20prepare_base_classesEPNS_27impl_baseclass_declarationsE, .Lfunc_end51-_ZN2kc20prepare_base_classesEPNS_27impl_baseclass_declarationsE
	.cfi_endproc

	.type	pl_addedphylumdeclarations,@object # @pl_addedphylumdeclarations
	.bss
	.globl	pl_addedphylumdeclarations
	.p2align	3
pl_addedphylumdeclarations:
	.quad	0
	.size	pl_addedphylumdeclarations, 8

	.type	pl_countedphylumdeclarations,@object # @pl_countedphylumdeclarations
	.globl	pl_countedphylumdeclarations
	.p2align	3
pl_countedphylumdeclarations:
	.quad	0
	.size	pl_countedphylumdeclarations, 8

	.type	The_current_unparseitems,@object # @The_current_unparseitems
	.globl	The_current_unparseitems
	.p2align	3
The_current_unparseitems:
	.quad	0
	.size	The_current_unparseitems, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"f_added"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/kimwitu++/util.cc"
	.size	.L.str.1, 81

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"v_add"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"f_DvIsDisallowed"
	.size	.L.str.3, 17

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"f_listelementphylum"
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"f_listelementconsoperator"
	.size	.L.str.5, 26

	.type	_ZZN2kc9f_emptyIdEvE7emptyID,@object # @_ZZN2kc9f_emptyIdEvE7emptyID
	.local	_ZZN2kc9f_emptyIdEvE7emptyID
	.comm	_ZZN2kc9f_emptyIdEvE7emptyID,8,8
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.zero	1
	.size	.L.str.6, 1

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"f_operatorinphylum"
	.size	.L.str.7, 19

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"f_isphylum"
	.size	.L.str.8, 11

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"f_phylumdeclofid"
	.size	.L.str.9, 17

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"f_ispredefinedphylum"
	.size	.L.str.10, 21

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"f_alternativeofoperator"
	.size	.L.str.11, 24

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"f_argumentsofoperator"
	.size	.L.str.12, 22

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"f_phylumofoperator"
	.size	.L.str.13, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"f_phylumofpatternvariable"
	.size	.L.str.14, 26

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"v_syn_type_attribute_ID"
	.size	.L.str.15, 24

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"f_Nilarguments"
	.size	.L.str.16, 15

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Head of empty argument list requested"
	.size	.L.str.17, 38

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"f_hd_arguments"
	.size	.L.str.18, 15

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Tail of empty argument list requested"
	.size	.L.str.19, 38

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"f_tl_arguments"
	.size	.L.str.20, 15

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"illegal dollar variable"
	.size	.L.str.21, 24

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"not that many subterms in operator"
	.size	.L.str.22, 35

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"v_check_dollarvar_attribute_in_operators"
	.size	.L.str.23, 41

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"internal error: could not find declaration of phylum:"
	.size	.L.str.24, 54

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"f_typeof_attribute_in_phylym"
	.size	.L.str.25, 29

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"f_subphylumofoperator"
	.size	.L.str.26, 22

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"f_argumentofoperator"
	.size	.L.str.27, 21

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"f_phylumofoutmostpattern"
	.size	.L.str.28, 25

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"internal error: could not find operators of phylum:"
	.size	.L.str.29, 52

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"f_operatorofphylum"
	.size	.L.str.30, 19

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"v_reset_variables_type"
	.size	.L.str.31, 23

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"v_add_to_uviewnames_ext"
	.size	.L.str.32, 24

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"v_add_to_rviewnames_ext"
	.size	.L.str.33, 24

	.type	_ZL19The_Nilunparseitems,@object # @_ZL19The_Nilunparseitems
	.local	_ZL19The_Nilunparseitems
	.comm	_ZL19The_Nilunparseitems,8,8
	.type	_ZL17string_collection,@object # @_ZL17string_collection
	.local	_ZL17string_collection
	.comm	_ZL17string_collection,8,8
	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"add_string_to_collection"
	.size	.L.str.34, 25

	.type	_ZZN2kc25unparse_string_collectionEvE3buf,@object # @_ZZN2kc25unparse_string_collectionEvE3buf
	.local	_ZZN2kc25unparse_string_collectionEvE3buf
	.comm	_ZZN2kc25unparse_string_collectionEvE3buf,30,16
	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"/*%ld*/"
	.size	.L.str.35, 8

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	",\n"
	.size	.L.str.36, 3

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"};\n\n"
	.size	.L.str.37, 5

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"char **kc_language=kc_language_"
	.size	.L.str.38, 32

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"unparse_string_collection"
	.size	.L.str.39, 26

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	";\n\n"
	.size	.L.str.40, 4

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"t_f_phylumnames_foreachwith_vars"
	.size	.L.str.41, 33

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"t_f_phylumnames_foreachwith_listvars"
	.size	.L.str.42, 37

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"attribute"
	.size	.L.str.43, 10

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"not defined in phylum"
	.size	.L.str.44, 22

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"f_do_check_unpattributes_in_phylum"
	.size	.L.str.45, 35

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	" type mismatch for dollar varariable:"
	.size	.L.str.46, 38

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"; old type"
	.size	.L.str.47, 11

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"; new type"
	.size	.L.str.48, 11

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"v_do_check_dollarvar_in_operators"
	.size	.L.str.49, 34

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"f_do_subphylum"
	.size	.L.str.50, 15

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"f_do_argument"
	.size	.L.str.51, 14

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"f_do_alternative"
	.size	.L.str.52, 17

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"do_add_to_storageclasses"
	.size	.L.str.53, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
