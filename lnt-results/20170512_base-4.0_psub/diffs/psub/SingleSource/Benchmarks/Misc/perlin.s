	.text
	.file	"perlin.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	-4555905281166241956    # double -11352.57
.LCPI0_1:
	.quad	-4578575351656820507    # double -346.12349999999998
.LCPI0_2:
	.quad	4627233148028512633     # double 23.234500000000001
.LCPI0_3:
	.quad	4618441417868443648     # double 6
.LCPI0_4:
	.quad	-4598738169498697728    # double -15
.LCPI0_5:
	.quad	4621819117588971520     # double 10
.LCPI0_6:
	.quad	-4616189618054758400    # double -1
.LCPI0_7:
	.quad	4605065726975153152     # double 0.76499999999998636
.LCPI0_9:
	.quad	-4625737249264783360    # double -0.23500000000001364
.LCPI0_10:
	.quad	4606386795240182783     # double 0.91166804049373684
.LCPI0_11:
	.quad	4612699328343546266     # double 2.4500000000000002
.LCPI0_12:
	.quad	4609130225638855148     # double 1.4325000000000001
.LCPI0_13:
	.quad	4638434866939178254     # double 124.124
.LCPI0_14:
	.quad	4593563533526849028     # double 0.1235
.LCPI0_15:
	.quad	4672205769001748398     # double 23561.57
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_8:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	$-1024, %rax            # imm = 0xFC00
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movaps	permutation+1024(%rax), %xmm0
	movaps	%xmm0, p+1024(%rax)
	movaps	%xmm0, p+2048(%rax)
	movaps	permutation+1040(%rax), %xmm0
	movaps	%xmm0, p+1040(%rax)
	movaps	%xmm0, p+2064(%rax)
	movaps	permutation+1056(%rax), %xmm0
	movaps	%xmm0, p+1056(%rax)
	movaps	%xmm0, p+2080(%rax)
	movaps	permutation+1072(%rax), %xmm0
	movaps	%xmm0, p+1072(%rax)
	movaps	%xmm0, p+2096(%rax)
	addq	$64, %rax
	jne	.LBB0_1
# BB#2:                                 # %.preheader21.preheader
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 80
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader21
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #       Child Loop BB0_6 Depth 3
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_6 Depth 3
	movsd	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB0_79
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_4 Depth=2
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	floor
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cvttsd2si	%xmm0, %eax
	movzbl	%al, %eax
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	subsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	mulsd	%xmm2, %xmm0
	movapd	%xmm2, %xmm10
	mulsd	.LCPI0_3(%rip), %xmm10
	addsd	.LCPI0_4(%rip), %xmm10
	mulsd	%xmm2, %xmm10
	addsd	.LCPI0_5(%rip), %xmm10
	mulsd	%xmm0, %xmm10
	movslq	p(,%rax,4), %r14
	movslq	p+4(,%rax,4), %r15
	movsd	%xmm2, 32(%rsp)         # 8-byte Spill
	movapd	%xmm2, %xmm4
	addsd	.LCPI0_6(%rip), %xmm4
	movsd	%xmm10, 40(%rsp)        # 8-byte Spill
	movsd	%xmm4, 24(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_6:                                #   Parent Loop BB0_3 Depth=1
                                        #     Parent Loop BB0_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movapd	%xmm1, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movzbl	%al, %eax
	movsd	8(%rsp), %xmm8          # 8-byte Reload
                                        # xmm8 = mem[0],zero
	subsd	%xmm0, %xmm8
	leaq	(%rax,%r14), %rcx
	movslq	p(,%rcx,4), %rsi
	movslq	%ecx, %rcx
	movslq	p+4(,%rcx,4), %rcx
	addq	%r15, %rax
	movslq	p(,%rax,4), %rdx
	cltq
	movslq	p+4(,%rax,4), %rax
	movl	p+396(,%rsi,4), %ebx
	movl	%ebx, %edi
	andl	$15, %edi
	cmpl	$8, %edi
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm13
	jb	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm8, %xmm13
.LBB0_8:                                #   in Loop: Header=BB0_6 Depth=3
	cmpl	$4, %edi
	movapd	%xmm8, %xmm4
	movapd	.LCPI0_8(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00]
	movsd	24(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
	jb	.LBB0_11
# BB#9:                                 #   in Loop: Header=BB0_6 Depth=3
	orl	$2, %edi
	cmpl	$14, %edi
	movapd	%xmm1, %xmm4
	je	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_6 Depth=3
	movsd	.LCPI0_7(%rip), %xmm4   # xmm4 = mem[0],zero
.LBB0_11:                               # %grad.exit7.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$1, %bl
	je	.LBB0_13
# BB#12:                                # %grad.exit7.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm13
.LBB0_13:                               # %grad.exit7.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$2, %bl
	je	.LBB0_15
# BB#14:                                # %grad.exit7.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm4
.LBB0_15:                               # %grad.exit7.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movl	p+396(,%rdx,4), %ebx
	movl	%ebx, %edi
	andl	$15, %edi
	cmpl	$8, %edi
	movapd	%xmm7, %xmm15
	jb	.LBB0_17
# BB#16:                                # %grad.exit7.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm8, %xmm15
.LBB0_17:                               # %grad.exit7.i
                                        #   in Loop: Header=BB0_6 Depth=3
	cmpl	$4, %edi
	movapd	%xmm8, %xmm9
	jb	.LBB0_20
# BB#18:                                #   in Loop: Header=BB0_6 Depth=3
	orl	$2, %edi
	cmpl	$14, %edi
	movapd	%xmm7, %xmm9
	je	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_6 Depth=3
	movsd	.LCPI0_7(%rip), %xmm9   # xmm9 = mem[0],zero
.LBB0_20:                               # %grad.exit6.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$1, %bl
	je	.LBB0_22
# BB#21:                                # %grad.exit6.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm15
.LBB0_22:                               # %grad.exit6.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$2, %bl
	je	.LBB0_24
# BB#23:                                # %grad.exit6.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm9
.LBB0_24:                               # %grad.exit6.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movl	p+396(,%rcx,4), %ebx
	movapd	%xmm8, %xmm2
	addsd	.LCPI0_6(%rip), %xmm2
	movl	%ebx, %edi
	andl	$15, %edi
	cmpl	$8, %edi
	movapd	%xmm1, %xmm3
	jb	.LBB0_26
# BB#25:                                # %grad.exit6.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm2, %xmm3
.LBB0_26:                               # %grad.exit6.i
                                        #   in Loop: Header=BB0_6 Depth=3
	cmpl	$4, %edi
	movapd	%xmm2, %xmm10
	jb	.LBB0_29
# BB#27:                                #   in Loop: Header=BB0_6 Depth=3
	orl	$2, %edi
	cmpl	$14, %edi
	movapd	%xmm1, %xmm10
	je	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_6 Depth=3
	movsd	.LCPI0_7(%rip), %xmm10  # xmm10 = mem[0],zero
.LBB0_29:                               # %grad.exit5.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$1, %bl
	je	.LBB0_31
# BB#30:                                # %grad.exit5.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm3
.LBB0_31:                               # %grad.exit5.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$2, %bl
	je	.LBB0_33
# BB#32:                                # %grad.exit5.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm10
.LBB0_33:                               # %grad.exit5.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movl	p+396(,%rax,4), %ebx
	movl	%ebx, %edi
	andl	$15, %edi
	cmpl	$8, %edi
	movapd	%xmm7, %xmm5
	jb	.LBB0_35
# BB#34:                                # %grad.exit5.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm2, %xmm5
.LBB0_35:                               # %grad.exit5.i
                                        #   in Loop: Header=BB0_6 Depth=3
	cmpl	$4, %edi
	movapd	%xmm2, %xmm12
	jb	.LBB0_38
# BB#36:                                #   in Loop: Header=BB0_6 Depth=3
	orl	$2, %edi
	cmpl	$14, %edi
	movapd	%xmm7, %xmm12
	je	.LBB0_38
# BB#37:                                #   in Loop: Header=BB0_6 Depth=3
	movsd	.LCPI0_7(%rip), %xmm12  # xmm12 = mem[0],zero
.LBB0_38:                               # %grad.exit4.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$1, %bl
	je	.LBB0_40
# BB#39:                                # %grad.exit4.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm5
.LBB0_40:                               # %grad.exit4.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$2, %bl
	je	.LBB0_42
# BB#41:                                # %grad.exit4.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm12
.LBB0_42:                               # %grad.exit4.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movl	p+400(,%rsi,4), %esi
	movl	%esi, %edi
	andl	$15, %edi
	cmpl	$8, %edi
	movapd	%xmm1, %xmm6
	jb	.LBB0_44
# BB#43:                                # %grad.exit4.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm8, %xmm6
.LBB0_44:                               # %grad.exit4.i
                                        #   in Loop: Header=BB0_6 Depth=3
	cmpl	$4, %edi
	movapd	%xmm8, %xmm11
	jb	.LBB0_47
# BB#45:                                #   in Loop: Header=BB0_6 Depth=3
	orl	$2, %edi
	cmpl	$14, %edi
	movapd	%xmm1, %xmm11
	je	.LBB0_47
# BB#46:                                #   in Loop: Header=BB0_6 Depth=3
	movsd	.LCPI0_9(%rip), %xmm11  # xmm11 = mem[0],zero
.LBB0_47:                               # %grad.exit3.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$1, %sil
	je	.LBB0_49
# BB#48:                                # %grad.exit3.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm6
.LBB0_49:                               # %grad.exit3.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$2, %sil
	je	.LBB0_51
# BB#50:                                # %grad.exit3.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm11
.LBB0_51:                               # %grad.exit3.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movl	p+400(,%rdx,4), %edx
	movl	%edx, %esi
	andl	$15, %esi
	cmpl	$8, %esi
	jb	.LBB0_53
# BB#52:                                # %grad.exit3.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm8, %xmm7
.LBB0_53:                               # %grad.exit3.i
                                        #   in Loop: Header=BB0_6 Depth=3
	cmpl	$4, %esi
	movapd	%xmm8, %xmm14
	jb	.LBB0_56
# BB#54:                                #   in Loop: Header=BB0_6 Depth=3
	orl	$2, %esi
	cmpl	$14, %esi
	movsd	24(%rsp), %xmm14        # 8-byte Reload
                                        # xmm14 = mem[0],zero
	je	.LBB0_56
# BB#55:                                #   in Loop: Header=BB0_6 Depth=3
	movsd	.LCPI0_9(%rip), %xmm14  # xmm14 = mem[0],zero
.LBB0_56:                               # %grad.exit2.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$1, %dl
	je	.LBB0_58
# BB#57:                                # %grad.exit2.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm7
.LBB0_58:                               # %grad.exit2.i
                                        #   in Loop: Header=BB0_6 Depth=3
	testb	$2, %dl
	je	.LBB0_60
# BB#59:                                # %grad.exit2.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	%xmm0, %xmm14
.LBB0_60:                               # %grad.exit2.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movl	p+400(,%rcx,4), %ecx
	movl	%ecx, %edx
	andl	$15, %edx
	cmpl	$8, %edx
	movapd	%xmm1, %xmm0
	jb	.LBB0_62
# BB#61:                                # %grad.exit2.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm2, %xmm0
.LBB0_62:                               # %grad.exit2.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm8, %xmm1
	mulsd	.LCPI0_3(%rip), %xmm1
	addsd	%xmm4, %xmm13
	addsd	%xmm9, %xmm15
	addsd	%xmm10, %xmm3
	addsd	%xmm12, %xmm5
	cmpl	$4, %edx
	movapd	%xmm2, %xmm9
	movsd	40(%rsp), %xmm10        # 8-byte Reload
                                        # xmm10 = mem[0],zero
	jb	.LBB0_65
# BB#63:                                #   in Loop: Header=BB0_6 Depth=3
	orl	$2, %edx
	cmpl	$14, %edx
	movsd	32(%rsp), %xmm9         # 8-byte Reload
                                        # xmm9 = mem[0],zero
	je	.LBB0_65
# BB#64:                                #   in Loop: Header=BB0_6 Depth=3
	movsd	.LCPI0_9(%rip), %xmm9   # xmm9 = mem[0],zero
.LBB0_65:                               # %grad.exit1.i
                                        #   in Loop: Header=BB0_6 Depth=3
	addsd	.LCPI0_4(%rip), %xmm1
	subsd	%xmm13, %xmm15
	subsd	%xmm3, %xmm5
	testb	$1, %cl
	je	.LBB0_67
# BB#66:                                # %grad.exit1.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	.LCPI0_8(%rip), %xmm0
.LBB0_67:                               # %grad.exit1.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm8, %xmm4
	mulsd	%xmm4, %xmm4
	mulsd	%xmm8, %xmm1
	mulsd	%xmm10, %xmm15
	mulsd	%xmm10, %xmm5
	testb	$2, %cl
	je	.LBB0_69
# BB#68:                                # %grad.exit1.i
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	.LCPI0_8(%rip), %xmm9
.LBB0_69:                               # %grad.exit1.i
                                        #   in Loop: Header=BB0_6 Depth=3
	mulsd	%xmm4, %xmm8
	addsd	.LCPI0_5(%rip), %xmm1
	addsd	%xmm15, %xmm13
	addsd	%xmm5, %xmm3
	addsd	%xmm11, %xmm6
	addsd	%xmm14, %xmm7
	movl	p+400(,%rax,4), %eax
	movl	%eax, %ecx
	andl	$15, %ecx
	cmpl	$8, %ecx
	movsd	24(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm5
	jb	.LBB0_71
# BB#70:                                # %grad.exit1.i
                                        #   in Loop: Header=BB0_6 Depth=3
	movapd	%xmm2, %xmm5
.LBB0_71:                               # %grad.exit1.i
                                        #   in Loop: Header=BB0_6 Depth=3
	mulsd	%xmm1, %xmm8
	subsd	%xmm13, %xmm3
	subsd	%xmm6, %xmm7
	cmpl	$4, %ecx
	jb	.LBB0_74
# BB#72:                                #   in Loop: Header=BB0_6 Depth=3
	orl	$2, %ecx
	cmpl	$14, %ecx
	movapd	%xmm4, %xmm2
	je	.LBB0_74
# BB#73:                                #   in Loop: Header=BB0_6 Depth=3
	movsd	.LCPI0_9(%rip), %xmm2   # xmm2 = mem[0],zero
.LBB0_74:                               # %noise.exit
                                        #   in Loop: Header=BB0_6 Depth=3
	mulsd	%xmm8, %xmm3
	mulsd	%xmm10, %xmm7
	testb	$1, %al
	je	.LBB0_76
# BB#75:                                # %noise.exit
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	.LCPI0_8(%rip), %xmm5
.LBB0_76:                               # %noise.exit
                                        #   in Loop: Header=BB0_6 Depth=3
	addsd	%xmm3, %xmm13
	addsd	%xmm7, %xmm6
	addsd	%xmm9, %xmm0
	testb	$2, %al
	je	.LBB0_78
# BB#77:                                # %noise.exit
                                        #   in Loop: Header=BB0_6 Depth=3
	xorpd	.LCPI0_8(%rip), %xmm2
.LBB0_78:                               # %noise.exit
                                        #   in Loop: Header=BB0_6 Depth=3
	addsd	%xmm2, %xmm5
	subsd	%xmm0, %xmm5
	mulsd	%xmm10, %xmm5
	addsd	%xmm0, %xmm5
	subsd	%xmm6, %xmm5
	mulsd	%xmm5, %xmm8
	addsd	%xmm6, %xmm8
	subsd	%xmm13, %xmm8
	mulsd	.LCPI0_10(%rip), %xmm8
	addsd	%xmm13, %xmm8
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	%xmm8, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI0_11(%rip), %xmm1
	movsd	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	ja	.LBB0_6
.LBB0_79:                               # %._crit_edge
                                        #   in Loop: Header=BB0_4 Depth=2
	addsd	.LCPI0_12(%rip), %xmm1
	movsd	.LCPI0_13(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	ja	.LBB0_4
# BB#80:                                # %init.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI0_14(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	.LCPI0_15(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB0_3
# BB#81:
	movl	$.L.str, %edi
	movb	$1, %al
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	printf
	xorl	%eax, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%e\n"
	.size	.L.str, 4

	.type	permutation,@object     # @permutation
	.section	.rodata,"a",@progbits
	.p2align	4
permutation:
	.long	151                     # 0x97
	.long	160                     # 0xa0
	.long	137                     # 0x89
	.long	91                      # 0x5b
	.long	90                      # 0x5a
	.long	15                      # 0xf
	.long	131                     # 0x83
	.long	13                      # 0xd
	.long	201                     # 0xc9
	.long	95                      # 0x5f
	.long	96                      # 0x60
	.long	53                      # 0x35
	.long	194                     # 0xc2
	.long	233                     # 0xe9
	.long	7                       # 0x7
	.long	225                     # 0xe1
	.long	140                     # 0x8c
	.long	36                      # 0x24
	.long	103                     # 0x67
	.long	30                      # 0x1e
	.long	69                      # 0x45
	.long	142                     # 0x8e
	.long	8                       # 0x8
	.long	99                      # 0x63
	.long	37                      # 0x25
	.long	240                     # 0xf0
	.long	21                      # 0x15
	.long	10                      # 0xa
	.long	23                      # 0x17
	.long	190                     # 0xbe
	.long	6                       # 0x6
	.long	148                     # 0x94
	.long	247                     # 0xf7
	.long	120                     # 0x78
	.long	234                     # 0xea
	.long	75                      # 0x4b
	.long	0                       # 0x0
	.long	26                      # 0x1a
	.long	197                     # 0xc5
	.long	62                      # 0x3e
	.long	94                      # 0x5e
	.long	252                     # 0xfc
	.long	219                     # 0xdb
	.long	203                     # 0xcb
	.long	117                     # 0x75
	.long	35                      # 0x23
	.long	11                      # 0xb
	.long	32                      # 0x20
	.long	57                      # 0x39
	.long	177                     # 0xb1
	.long	33                      # 0x21
	.long	88                      # 0x58
	.long	237                     # 0xed
	.long	149                     # 0x95
	.long	56                      # 0x38
	.long	87                      # 0x57
	.long	174                     # 0xae
	.long	20                      # 0x14
	.long	125                     # 0x7d
	.long	136                     # 0x88
	.long	171                     # 0xab
	.long	168                     # 0xa8
	.long	68                      # 0x44
	.long	175                     # 0xaf
	.long	74                      # 0x4a
	.long	165                     # 0xa5
	.long	71                      # 0x47
	.long	134                     # 0x86
	.long	139                     # 0x8b
	.long	48                      # 0x30
	.long	27                      # 0x1b
	.long	166                     # 0xa6
	.long	77                      # 0x4d
	.long	146                     # 0x92
	.long	158                     # 0x9e
	.long	231                     # 0xe7
	.long	83                      # 0x53
	.long	111                     # 0x6f
	.long	229                     # 0xe5
	.long	122                     # 0x7a
	.long	60                      # 0x3c
	.long	211                     # 0xd3
	.long	133                     # 0x85
	.long	230                     # 0xe6
	.long	220                     # 0xdc
	.long	105                     # 0x69
	.long	92                      # 0x5c
	.long	41                      # 0x29
	.long	55                      # 0x37
	.long	46                      # 0x2e
	.long	245                     # 0xf5
	.long	40                      # 0x28
	.long	244                     # 0xf4
	.long	102                     # 0x66
	.long	143                     # 0x8f
	.long	54                      # 0x36
	.long	65                      # 0x41
	.long	25                      # 0x19
	.long	63                      # 0x3f
	.long	161                     # 0xa1
	.long	1                       # 0x1
	.long	216                     # 0xd8
	.long	80                      # 0x50
	.long	73                      # 0x49
	.long	209                     # 0xd1
	.long	76                      # 0x4c
	.long	132                     # 0x84
	.long	187                     # 0xbb
	.long	208                     # 0xd0
	.long	89                      # 0x59
	.long	18                      # 0x12
	.long	169                     # 0xa9
	.long	200                     # 0xc8
	.long	196                     # 0xc4
	.long	135                     # 0x87
	.long	130                     # 0x82
	.long	116                     # 0x74
	.long	188                     # 0xbc
	.long	159                     # 0x9f
	.long	86                      # 0x56
	.long	164                     # 0xa4
	.long	100                     # 0x64
	.long	109                     # 0x6d
	.long	198                     # 0xc6
	.long	173                     # 0xad
	.long	186                     # 0xba
	.long	3                       # 0x3
	.long	64                      # 0x40
	.long	52                      # 0x34
	.long	217                     # 0xd9
	.long	226                     # 0xe2
	.long	250                     # 0xfa
	.long	124                     # 0x7c
	.long	123                     # 0x7b
	.long	5                       # 0x5
	.long	202                     # 0xca
	.long	38                      # 0x26
	.long	147                     # 0x93
	.long	118                     # 0x76
	.long	126                     # 0x7e
	.long	255                     # 0xff
	.long	82                      # 0x52
	.long	85                      # 0x55
	.long	212                     # 0xd4
	.long	207                     # 0xcf
	.long	206                     # 0xce
	.long	59                      # 0x3b
	.long	227                     # 0xe3
	.long	47                      # 0x2f
	.long	16                      # 0x10
	.long	58                      # 0x3a
	.long	17                      # 0x11
	.long	182                     # 0xb6
	.long	189                     # 0xbd
	.long	28                      # 0x1c
	.long	42                      # 0x2a
	.long	223                     # 0xdf
	.long	183                     # 0xb7
	.long	170                     # 0xaa
	.long	213                     # 0xd5
	.long	119                     # 0x77
	.long	248                     # 0xf8
	.long	152                     # 0x98
	.long	2                       # 0x2
	.long	44                      # 0x2c
	.long	154                     # 0x9a
	.long	163                     # 0xa3
	.long	70                      # 0x46
	.long	221                     # 0xdd
	.long	153                     # 0x99
	.long	101                     # 0x65
	.long	155                     # 0x9b
	.long	167                     # 0xa7
	.long	43                      # 0x2b
	.long	172                     # 0xac
	.long	9                       # 0x9
	.long	129                     # 0x81
	.long	22                      # 0x16
	.long	39                      # 0x27
	.long	253                     # 0xfd
	.long	19                      # 0x13
	.long	98                      # 0x62
	.long	108                     # 0x6c
	.long	110                     # 0x6e
	.long	79                      # 0x4f
	.long	113                     # 0x71
	.long	224                     # 0xe0
	.long	232                     # 0xe8
	.long	178                     # 0xb2
	.long	185                     # 0xb9
	.long	112                     # 0x70
	.long	104                     # 0x68
	.long	218                     # 0xda
	.long	246                     # 0xf6
	.long	97                      # 0x61
	.long	228                     # 0xe4
	.long	251                     # 0xfb
	.long	34                      # 0x22
	.long	242                     # 0xf2
	.long	193                     # 0xc1
	.long	238                     # 0xee
	.long	210                     # 0xd2
	.long	144                     # 0x90
	.long	12                      # 0xc
	.long	191                     # 0xbf
	.long	179                     # 0xb3
	.long	162                     # 0xa2
	.long	241                     # 0xf1
	.long	81                      # 0x51
	.long	51                      # 0x33
	.long	145                     # 0x91
	.long	235                     # 0xeb
	.long	249                     # 0xf9
	.long	14                      # 0xe
	.long	239                     # 0xef
	.long	107                     # 0x6b
	.long	49                      # 0x31
	.long	192                     # 0xc0
	.long	214                     # 0xd6
	.long	31                      # 0x1f
	.long	181                     # 0xb5
	.long	199                     # 0xc7
	.long	106                     # 0x6a
	.long	157                     # 0x9d
	.long	184                     # 0xb8
	.long	84                      # 0x54
	.long	204                     # 0xcc
	.long	176                     # 0xb0
	.long	115                     # 0x73
	.long	121                     # 0x79
	.long	50                      # 0x32
	.long	45                      # 0x2d
	.long	127                     # 0x7f
	.long	4                       # 0x4
	.long	150                     # 0x96
	.long	254                     # 0xfe
	.long	138                     # 0x8a
	.long	236                     # 0xec
	.long	205                     # 0xcd
	.long	93                      # 0x5d
	.long	222                     # 0xde
	.long	114                     # 0x72
	.long	67                      # 0x43
	.long	29                      # 0x1d
	.long	24                      # 0x18
	.long	72                      # 0x48
	.long	243                     # 0xf3
	.long	141                     # 0x8d
	.long	128                     # 0x80
	.long	195                     # 0xc3
	.long	78                      # 0x4e
	.long	66                      # 0x42
	.long	215                     # 0xd7
	.long	61                      # 0x3d
	.long	156                     # 0x9c
	.long	180                     # 0xb4
	.size	permutation, 1024

	.type	p,@object               # @p
	.local	p
	.comm	p,2048,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
