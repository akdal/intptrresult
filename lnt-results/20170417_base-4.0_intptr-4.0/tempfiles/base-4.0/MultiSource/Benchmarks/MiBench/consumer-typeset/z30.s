	.text
	.file	"z30.bc"
	.globl	InsertUses
	.p2align	4, 0x90
	.type	InsertUses,@function
InsertUses:                             # @InsertUses
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpb	$-113, 32(%r14)
	jne	.LBB0_9
# BB#1:
	cmpb	$-113, 32(%rbx)
	jne	.LBB0_9
# BB#2:
	cmpw	$0, 120(%rbx)
	jne	.LBB0_9
# BB#3:
	movl	$2, zz_size(%rip)
	movq	zz_free+16(%rip), %rax
	testq	%rax, %rax
	je	.LBB0_4
# BB#5:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free+16(%rip)
	jmp	.LBB0_6
.LBB0_4:
	movq	no_fpos(%rip), %rsi
	movl	$2, %edi
	callq	GetMemory
.LBB0_6:
	movq	%rbx, (%rax)
	movq	64(%r14), %rcx
	testq	%rcx, %rcx
	movq	%rax, %rdx
	je	.LBB0_8
# BB#7:
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rcx, %rdx
.LBB0_8:
	movq	%rax, 8(%rdx)
	movq	%rax, 64(%r14)
.LBB0_9:                                # %._crit_edge
	movb	32(%rbx), %al
	addb	$112, %al
	cmpb	$2, %al
	ja	.LBB0_13
# BB#10:
	movq	48(%rbx), %rax
	cmpq	%r14, %rax
	setne	%cl
	addb	124(%rbx), %cl
	incb	%cl
	movb	%cl, 124(%rbx)
	cmpb	$1, %cl
	ja	.LBB0_12
# BB#11:
	movzwl	41(%rbx), %ecx
	testw	%cx, %cx
	jns	.LBB0_16
.LBB0_12:
	orb	$-128, 42(%rax)
	jmp	.LBB0_16
.LBB0_13:
	cmpq	$0, 56(%rbx)
	je	.LBB0_15
# BB#14:
	cmpw	$0, 41(%rbx)
	jns	.LBB0_16
.LBB0_15:
	orb	$-128, 42(%r14)
.LBB0_16:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	InsertUses, .Lfunc_end0-InsertUses
	.cfi_endproc

	.globl	FlattenUses
	.p2align	4, 0x90
	.type	FlattenUses,@function
FlattenUses:                            # @FlattenUses
	.cfi_startproc
# BB#0:
	movq	StartSym(%rip), %rdi
	jmp	GatherAllUses           # TAILCALL
.Lfunc_end1:
	.size	FlattenUses, .Lfunc_end1-FlattenUses
	.cfi_endproc

	.p2align	4, 0x90
	.type	GatherAllUses,@function
GatherAllUses:                          # @GatherAllUses
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	8(%r14), %r15
	cmpq	%r14, %r15
	jne	.LBB2_2
	jmp	.LBB2_7
	.p2align	4, 0x90
.LBB2_6:                                # %.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, %rdi
	callq	GatherAllUses
	movq	8(%r15), %r15
	cmpq	%r14, %r15
	je	.LBB2_7
.LBB2_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_3
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpb	$-113, %al
	jne	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	callq	GatherUses
	jmp	.LBB2_6
.LBB2_7:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	GatherAllUses, .Lfunc_end2-GatherAllUses
	.cfi_endproc

	.globl	SearchUses
	.p2align	4, 0x90
	.type	SearchUses,@function
SearchUses:                             # @SearchUses
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.LBB3_6
# BB#1:
	movq	72(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB3_5
# BB#2:
	movq	8(%rcx), %rdx
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, (%rdx)
	je	.LBB3_6
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	8(%rdx), %rdx
	cmpq	8(%rcx), %rdx
	jne	.LBB3_3
.LBB3_5:
	xorl	%eax, %eax
.LBB3_6:                                # %.loopexit
	retq
.Lfunc_end3:
	.size	SearchUses, .Lfunc_end3-SearchUses
	.cfi_endproc

	.globl	FirstExternTarget
	.p2align	4, 0x90
	.type	FirstExternTarget,@function
FirstExternTarget:                      # @FirstExternTarget
	.cfi_startproc
# BB#0:
	movq	$0, (%rsi)
	movzwl	41(%rdi), %eax
	testb	$16, %ah
	jne	.LBB4_1
# BB#2:
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.LBB4_6
# BB#3:
	movq	8(%rax), %rcx
	movq	%rcx, (%rsi)
	.p2align	4, 0x90
.LBB4_4:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rax
	movzwl	41(%rax), %edx
	testb	$16, %dh
	jne	.LBB4_7
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=1
	movq	8(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	72(%rdi), %rax
	cmpq	8(%rax), %rcx
	jne	.LBB4_4
.LBB4_6:
	xorl	%eax, %eax
.LBB4_7:                                # %.loopexit
	retq
.LBB4_1:
	movq	%rdi, %rax
	retq
.Lfunc_end4:
	.size	FirstExternTarget, .Lfunc_end4-FirstExternTarget
	.cfi_endproc

	.globl	NextExternTarget
	.p2align	4, 0x90
	.type	NextExternTarget,@function
NextExternTarget:                       # @NextExternTarget
	.cfi_startproc
# BB#0:
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.LBB5_1
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	72(%rdi), %rax
	cmpq	8(%rax), %rcx
	je	.LBB5_3
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	(%rcx), %rax
	movzwl	41(%rax), %edx
	testb	$16, %dh
	je	.LBB5_2
# BB#5:                                 # %.loopexit
	retq
.LBB5_1:
	xorl	%eax, %eax
	retq
.LBB5_3:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	NextExternTarget, .Lfunc_end5-NextExternTarget
	.cfi_endproc

	.p2align	4, 0x90
	.type	GatherUses,@function
GatherUses:                             # @GatherUses
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r12, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	64(%r14), %rax
	testq	%rax, %rax
	je	.LBB6_16
# BB#1:
	movq	8(%rax), %rbx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %r12
	cmpq	%r15, 80(%r12)
	je	.LBB6_15
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpq	%r15, %r12
	je	.LBB6_14
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	%r15, 80(%r12)
	movl	$2, zz_size(%rip)
	movq	zz_free+16(%rip), %rax
	testq	%rax, %rax
	je	.LBB6_5
# BB#6:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free+16(%rip)
	jmp	.LBB6_7
	.p2align	4, 0x90
.LBB6_14:                               #   in Loop: Header=BB6_2 Depth=1
	orb	$4, 42(%r15)
	jmp	.LBB6_15
.LBB6_5:                                #   in Loop: Header=BB6_2 Depth=1
	movq	no_fpos(%rip), %rsi
	movl	$2, %edi
	callq	GetMemory
.LBB6_7:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%r12, (%rax)
	movq	72(%r15), %rcx
	testq	%rcx, %rcx
	movq	%rax, %rdx
	je	.LBB6_9
# BB#8:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	%rcx, %rdx
.LBB6_9:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%rax, 8(%rdx)
	movq	%rax, 72(%r15)
	movzwl	41(%r12), %ecx
	movzbl	43(%r12), %eax
	shll	$16, %eax
	orl	%ecx, %eax
	testb	$2, %ah
	je	.LBB6_11
# BB#10:                                #   in Loop: Header=BB6_2 Depth=1
	orb	$2, 42(%r15)
	movzwl	41(%r12), %ecx
	movzbl	43(%r12), %eax
	shll	$16, %eax
	orl	%ecx, %eax
.LBB6_11:                               #   in Loop: Header=BB6_2 Depth=1
	testb	$8, %ah
	je	.LBB6_13
# BB#12:                                #   in Loop: Header=BB6_2 Depth=1
	orb	$8, 42(%r15)
.LBB6_13:                               #   in Loop: Header=BB6_2 Depth=1
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	GatherUses
.LBB6_15:                               #   in Loop: Header=BB6_2 Depth=1
	movq	8(%rbx), %rbx
	movq	64(%r14), %rax
	cmpq	8(%rax), %rbx
	jne	.LBB6_2
.LBB6_16:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	GatherUses, .Lfunc_end6-GatherUses
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
