	.text
	.file	"rtp.bc"
	.globl	ComposeRTPPacket
	.p2align	4, 0x90
	.type	ComposeRTPPacket,@function
ComposeRTPPacket:                       # @ComposeRTPPacket
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	(%rbx), %eax
	movl	4(%rbx), %ecx
	shll	$6, %eax
	shll	$5, %ecx
	andl	$32, %ecx
	orl	%eax, %ecx
	movl	8(%rbx), %eax
	shll	$4, %eax
	andl	$16, %eax
	orl	%ecx, %eax
	movl	12(%rbx), %ecx
	andl	$15, %ecx
	orl	%eax, %ecx
	movq	56(%rbx), %rax
	movb	%cl, (%rax)
	movl	16(%rbx), %eax
	movl	20(%rbx), %ecx
	shll	$7, %eax
	andl	$127, %ecx
	orl	%eax, %ecx
	movq	56(%rbx), %rax
	movb	%cl, 1(%rax)
	movzwl	24(%rbx), %eax
	rolw	$8, %ax
	movq	56(%rbx), %rcx
	movw	%ax, 2(%rcx)
	movl	28(%rbx), %eax
	bswapl	%eax
	movq	56(%rbx), %rcx
	movl	%eax, 4(%rcx)
	movl	32(%rbx), %eax
	bswapl	%eax
	movq	56(%rbx), %rcx
	movl	%eax, 8(%rcx)
	movq	56(%rbx), %rdi
	addq	$12, %rdi
	movq	40(%rbx), %rsi
	movl	48(%rbx), %edx
	callq	memcpy
	movl	48(%rbx), %eax
	addl	$12, %eax
	movl	%eax, 64(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	ComposeRTPPacket, .Lfunc_end0-ComposeRTPPacket
	.cfi_endproc

	.globl	WriteRTPPacket
	.p2align	4, 0x90
	.type	WriteRTPPacket,@function
WriteRTPPacket:                         # @WriteRTPPacket
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$-1, 12(%rsp)
	leaq	64(%rbx), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$-1, %ebp
	cmpq	$1, %rax
	jne	.LBB1_3
# BB#1:
	leaq	12(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB1_3
# BB#2:
	movq	56(%rbx), %rdi
	movl	64(%rbx), %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	xorl	%ecx, %ecx
	cmpq	$1, %rax
	movl	$-1, %ebp
	cmovel	%ecx, %ebp
.LBB1_3:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	WriteRTPPacket, .Lfunc_end1-WriteRTPPacket
	.cfi_endproc

	.globl	WriteRTPNALU
	.p2align	4, 0x90
	.type	WriteRTPNALU,@function
WriteRTPNALU:                           # @WriteRTPNALU
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 80
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	16(%r14), %eax
	movl	20(%r14), %ecx
	shll	$7, %ecx
	shll	$5, %eax
	orl	%ecx, %eax
	orl	12(%r14), %eax
	movq	24(%r14), %rcx
	movb	%al, (%rcx)
	movl	$72, %edi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbp, %rbx
	testq	%rbp, %rbp
	jne	.LBB2_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB2_2:
	movl	$65508, %edi            # imm = 0xFFE4
	callq	malloc
	movq	%rax, 56(%rbx)
	testq	%rax, %rax
	jne	.LBB2_4
# BB#3:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB2_4:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	$65508, %edi            # imm = 0xFFE4
	callq	malloc
	movq	%rax, %r12
	movq	%r12, 40(%rbx)
	testq	%r12, %r12
	jne	.LBB2_6
# BB#5:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB2_6:
	movl	$2, %eax
	movd	%eax, %xmm0
	movdqu	%xmm0, (%rbx)
	xorl	%eax, %eax
	cmpl	$4, (%r14)
	sete	%al
	movl	%eax, 16(%rbx)
	movl	$105, 20(%rbx)
	movl	CurrentRTPSequenceNumber(%rip), %ebp
	leal	1(%rbp), %eax
	movl	%eax, CurrentRTPSequenceNumber(%rip)
	movl	%ebp, 24(%rbx)
	movl	CurrentRTPTimestamp(%rip), %r15d
	movl	%r15d, 28(%rbx)
	movl	$305419896, 32(%rbx)    # imm = 0x12345678
	movl	4(%r14), %r13d
	movl	%r13d, 48(%rbx)
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	callq	memcpy
	movl	(%rbx), %eax
	movl	4(%rbx), %ecx
	shll	$6, %eax
	shll	$5, %ecx
	andl	$32, %ecx
	orl	%eax, %ecx
	movl	8(%rbx), %eax
	shll	$4, %eax
	andl	$16, %eax
	orl	%ecx, %eax
	movl	12(%rbx), %ecx
	andl	$15, %ecx
	orl	%eax, %ecx
	movq	56(%rbx), %rax
	movb	%cl, (%rax)
	movl	16(%rbx), %eax
	movl	20(%rbx), %ecx
	shll	$7, %eax
	andl	$127, %ecx
	orl	%eax, %ecx
	movq	56(%rbx), %rdi
	movb	%cl, 1(%rdi)
	rolw	$8, %bp
	movw	%bp, 2(%rdi)
	bswapl	%r15d
	movl	%r15d, 4(%rdi)
	movl	$2018915346, 8(%rdi)    # imm = 0x78563412
	addq	$12, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	memcpy
	leal	12(%r13), %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	addq	$64, %rbp
	movl	%eax, 64(%rbx)
	movq	f(%rip), %r12
	movl	$-1, 12(%rsp)
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbp, %rdi
	movq	%r12, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB2_8
# BB#7:
	leaq	12(%rsp), %rdi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r12, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB2_8
# BB#10:                                # %WriteRTPPacket.exit
	movq	56(%rbx), %r13
	movl	64(%rbx), %r15d
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB2_9
# BB#11:
	movq	%r13, %rdi
	callq	free
	movq	40(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movl	4(%r14), %eax
	shll	$3, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_8:                                # %WriteRTPPacket.exit.thread
	movl	(%rbp), %r15d
.LBB2_9:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end2:
	.size	WriteRTPNALU, .Lfunc_end2-WriteRTPNALU
	.cfi_endproc

	.globl	RTPUpdateTimestamp
	.p2align	4, 0x90
	.type	RTPUpdateTimestamp,@function
RTPUpdateTimestamp:                     # @RTPUpdateTimestamp
	.cfi_startproc
# BB#0:
	movl	RTPUpdateTimestamp.oldtr(%rip), %eax
	cmpl	$-1, %eax
	je	.LBB3_1
# BB#2:
	movl	%edi, %ecx
	subl	%eax, %ecx
	leal	256(%rcx), %eax
	cmpl	$-10, %ecx
	cmovgel	%ecx, %eax
	imull	$1000, %eax, %eax       # imm = 0x3E8
	addl	CurrentRTPTimestamp(%rip), %eax
	jmp	.LBB3_3
.LBB3_1:
	xorl	%eax, %eax
	xorl	%edi, %edi
.LBB3_3:
	movl	%eax, CurrentRTPTimestamp(%rip)
	movl	%edi, RTPUpdateTimestamp.oldtr(%rip)
	retq
.Lfunc_end3:
	.size	RTPUpdateTimestamp, .Lfunc_end3-RTPUpdateTimestamp
	.cfi_endproc

	.globl	OpenRTPFile
	.p2align	4, 0x90
	.type	OpenRTPFile,@function
OpenRTPFile:                            # @OpenRTPFile
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.5, %esi
	callq	fopen64
	movq	%rax, f(%rip)
	testq	%rax, %rax
	je	.LBB4_2
# BB#1:
	popq	%rbx
	retq
.LBB4_2:
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movl	$-1, %edi
	callq	exit
.Lfunc_end4:
	.size	OpenRTPFile, .Lfunc_end4-OpenRTPFile
	.cfi_endproc

	.globl	CloseRTPFile
	.p2align	4, 0x90
	.type	CloseRTPFile,@function
CloseRTPFile:                           # @CloseRTPFile
	.cfi_startproc
# BB#0:
	movq	f(%rip), %rdi
	jmp	fclose                  # TAILCALL
.Lfunc_end5:
	.size	CloseRTPFile, .Lfunc_end5-CloseRTPFile
	.cfi_endproc

	.type	CurrentRTPTimestamp,@object # @CurrentRTPTimestamp
	.bss
	.globl	CurrentRTPTimestamp
	.p2align	2
CurrentRTPTimestamp:
	.long	0                       # 0x0
	.size	CurrentRTPTimestamp, 4

	.type	CurrentRTPSequenceNumber,@object # @CurrentRTPSequenceNumber
	.globl	CurrentRTPSequenceNumber
	.p2align	2
CurrentRTPSequenceNumber:
	.long	0                       # 0x0
	.size	CurrentRTPSequenceNumber, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"RTPWriteNALU-1"
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"RTPWriteNALU-2"
	.size	.L.str.1, 15

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"RTPWriteNALU-3"
	.size	.L.str.2, 15

	.type	f,@object               # @f
	.comm	f,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Cannot write %d bytes of RTP packet to outfile, exit\n"
	.size	.L.str.4, 54

	.type	RTPUpdateTimestamp.oldtr,@object # @RTPUpdateTimestamp.oldtr
	.data
	.p2align	2
RTPUpdateTimestamp.oldtr:
	.long	4294967295              # 0xffffffff
	.size	RTPUpdateTimestamp.oldtr, 4

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"wb"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Fatal: cannot open bitstream file '%s', exit (-1)\n"
	.size	.L.str.6, 51

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	WriteNALU,@object       # @WriteNALU
	.comm	WriteNALU,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
