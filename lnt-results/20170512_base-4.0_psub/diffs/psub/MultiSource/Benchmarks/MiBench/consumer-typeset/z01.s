	.text
	.file	"z01.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1123024896              # float 120
.LCPI0_1:
	.long	1101004800              # float 20
.LCPI0_2:
	.long	1152647168              # float 1440
.LCPI0_3:
	.long	1141751808              # float 567
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$2152, %rsp             # imm = 0x868
.Lcfi6:
	.cfi_def_cfa_offset 2208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, 48(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	$.L.str, %edi
	callq	getenv
	testq	%rax, %rax
	movl	$.L.str.1, %r14d
	cmovneq	%rax, %r14
	movl	$5, %edi
	movl	$.L.str.2, %esi
	callq	setlocale
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$6, %esi
	movl	$.L.str.3, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
	movl	$.L.str.4, %ebp
.LBB0_2:
	leaq	1632(%rsp), %rbx
	movl	$.L.str.5, %esi
	movl	$.L.str.6, %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%rbp, %r8
	movq	%rbp, %r9
	callq	sprintf
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	catopen
	movq	%rax, MsgCat(%rip)
	movl	$0, TotalWordCount(%rip)
	movq	PS_BackEnd(%rip), %rax
	movq	%rax, BackEnd(%rip)
	movl	$144, PlainCharWidth(%rip)
	movl	$240, PlainCharHeight(%rip)
	movl	$0, PlainFormFeed(%rip)
	movl	$0, InitializeAll(%rip)
	movl	$1, UseCollate(%rip)
	movl	$1, AllowCrossDb(%rip)
	movl	$1, InMemoryDbIndexes(%rip)
	movl	$0, Encapsulated(%rip)
	movl	$0, SafeExecution(%rip)
	movl	$1, Kern(%rip)
	callq	MemInit
	callq	InitSym
	callq	LexInit
	callq	InitFiles
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.2, %esi
	callq	MakeWord
	xorl	%edi, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.2, %esi
	callq	MakeWord
	movl	$3, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movl	$.L.str.2, %esi
	callq	MakeWord
	movl	$1, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movl	$0, AltErrorFormat(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_4
# BB#3:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_5
.LBB0_4:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_5:
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, CommandOptions(%rip)
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$2, %ecx
	movq	48(%rsp), %rbx          # 8-byte Reload
	jl	.LBB0_254
# BB#6:                                 # %.lr.ph
	leal	-1(%rcx), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$.L.str.8, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$.L.str.7, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	jmp	.LBB0_223
.LBB0_7:                                #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 2(%r14)
	je	.LBB0_68
# BB#8:                                 #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %r14
	movq	%r14, %rbp
	movl	4(%rsp), %r14d          # 4-byte Reload
	jmp	.LBB0_178
.LBB0_9:                                #   in Loop: Header=BB0_223 Depth=1
	cmpb	$80, %al
	jne	.LBB0_13
# BB#10:                                #   in Loop: Header=BB0_223 Depth=1
	cmpb	$68, 2(%r14)
	jne	.LBB0_13
# BB#11:                                #   in Loop: Header=BB0_223 Depth=1
	cmpb	$70, 3(%r14)
	jne	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 4(%r14)
	je	.LBB0_20
.LBB0_13:                               # %.thread507
                                        #   in Loop: Header=BB0_223 Depth=1
	movl	$1, PlainFormFeed(%rip)
.LBB0_14:                               #   in Loop: Header=BB0_223 Depth=1
	movq	Plain_BackEnd(%rip), %rax
	movq	%rax, BackEnd(%rip)
	movq	(%rbx,%r15,8), %rdi
	cmpb	$0, 2(%rdi)
	je	.LBB0_67
# BB#15:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %rdi
	leaq	32(%rsp), %rdx
	leaq	27(%rsp), %rcx
	leaq	28(%rsp), %r8
	leaq	26(%rsp), %r9
	movl	$.L.str.57, %esi
	xorl	%eax, %eax
	callq	sscanf
	cmpl	$4, %eax
	je	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movq	(%rbx,%r15,8), %rax
	movsbl	1(%rax), %r9d
	movl	$1, %edi
	movl	$19, %esi
	movl	$.L.str.58, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_17:                               #   in Loop: Header=BB0_223 Depth=1
	movb	27(%rsp), %al
	addb	$-99, %al
	cmpb	$13, %al
	ja	.LBB0_164
# BB#18:                                #   in Loop: Header=BB0_223 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI0_1(,%rax,8)
.LBB0_19:                               #   in Loop: Header=BB0_223 Depth=1
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_3(%rip), %xmm0
	jmp	.LBB0_156
.LBB0_20:                               #   in Loop: Header=BB0_223 Depth=1
	movq	PDF_BackEnd(%rip), %rax
	movq	%rax, BackEnd(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_21:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$0, InMemoryDbIndexes(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_22:                               #   in Loop: Header=BB0_223 Depth=1
	movl	%r13d, 76(%rsp)         # 4-byte Spill
	addq	$2, %r14
	leaq	608(%rsp), %rdx
	leaq	1120(%rsp), %r13
	movl	$.L.str.64, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r13, %rcx
	callq	sscanf
	cmpl	$2, %eax
	jne	.LBB0_25
# BB#23:                                #   in Loop: Header=BB0_223 Depth=1
	leaq	608(%rsp), %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_223 Depth=1
	leaq	1120(%rsp), %rdi
	callq	strlen
	testq	%rax, %rax
	jne	.LBB0_26
.LBB0_25:                               #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movq	(%rbx,%r15,8), %r9
	addq	$2, %r9
	movl	$1, %edi
	movl	$24, %esi
	movl	$.L.str.65, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_26:                               #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	leaq	608(%rsp), %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_71
# BB#27:                                #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_72
.LBB0_28:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$69, %al
	jne	.LBB0_32
# BB#29:                                #   in Loop: Header=BB0_223 Depth=1
	cmpb	$80, 2(%r14)
	jne	.LBB0_32
# BB#30:                                #   in Loop: Header=BB0_223 Depth=1
	cmpb	$83, 3(%r14)
	jne	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 4(%r14)
	je	.LBB0_33
.LBB0_32:                               # %.thread503
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$10, %esi
	movl	$.L.str.16, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB0_33:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$1, Encapsulated(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_34:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 2(%r14)
	je	.LBB0_125
# BB#35:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_183
.LBB0_36:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 2(%r14)
	je	.LBB0_128
# BB#37:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_186
.LBB0_38:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$1, SafeExecution(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_39:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$0, SafeExecution(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_40:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 2(%r14)
	je	.LBB0_131
# BB#41:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_189
.LBB0_42:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 2(%r14)
	je	.LBB0_134
# BB#43:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_198
.LBB0_44:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 2(%r14)
	je	.LBB0_137
# BB#45:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_201
.LBB0_46:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$1, UseCollate(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_47:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 2(%r14)
	je	.LBB0_140
# BB#48:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_204
.LBB0_49:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$7, %edi
	callq	FirstFile
	testw	%ax, %ax
	je	.LBB0_51
# BB#50:                                #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$17, %esi
	movl	$.L.str.23, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_51:                               #   in Loop: Header=BB0_223 Depth=1
	movq	(%rbx,%r15,8), %rbx
	cmpb	$0, 2(%rbx)
	je	.LBB0_143
# BB#52:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %rbx
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_208
.LBB0_53:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$0, Kern(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_54:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$1, AltErrorFormat(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_55:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 2(%r14)
	je	.LBB0_146
# BB#56:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_211
.LBB0_57:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 2(%r14)
	je	.LBB0_149
# BB#58:                                #   in Loop: Header=BB0_223 Depth=1
	addq	$2, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	movq	%r14, 40(%rsp)          # 8-byte Spill
	jmp	.LBB0_238
.LBB0_59:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$0, UseCollate(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_60:                               #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$4, %esi
	movl	$.L.str.60, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r9
	jmp	.LBB0_65
.LBB0_61:                               #   in Loop: Header=BB0_223 Depth=1
	leaq	88(%rsp), %rdx
	movl	$.L.str.61, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sscanf
	movq	88(%rsp), %rdx
	movq	%rdx, MemCheck(%rip)
	movq	stderr(%rip), %rdi
	movl	$.L.str.62, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB0_66
.LBB0_62:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$1, InitializeAll(%rip)
.LBB0_63:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$0, AllowCrossDb(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_64:                               #   in Loop: Header=BB0_223 Depth=1
	movq	stderr(%rip), %rdi
	callq	PrintUsage
	movq	no_fpos(%rip), %r8
	movq	(%rbx,%r15,8), %r9
	movl	$1, %edi
	movl	$26, %esi
	movl	$.L.str.66, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
.LBB0_65:                               # %GetArg.exit361.thread414
                                        #   in Loop: Header=BB0_223 Depth=1
	callq	Error
.LBB0_66:                               # %GetArg.exit361.thread414
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB0_67:                               #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_68:                               #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_176
# BB#69:                                #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rbx,%r15,8), %rbp
	cmpb	$45, (%rbp)
	jne	.LBB0_212
.LBB0_176:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %r14d          # 4-byte Reload
.LBB0_177:                              # %GetArg.exit.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%ebp, %ebp
	movl	$1, %edi
	movl	$7, %esi
	movl	$.L.str.10, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_178:                              # %GetArg.exit.thread411
                                        #   in Loop: Header=BB0_223 Depth=1
	movl	$.L.str.11, %esi
	movq	%rbp, %rdi
	callq	StringEndsWith
	testl	%eax, %eax
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	je	.LBB0_180
# BB#179:                               #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$28, %esi
	movl	$.L.str.12, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbp, %r9
	pushq	$.L.str.11
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
.LBB0_180:                              #   in Loop: Header=BB0_223 Depth=1
	movl	%r14d, %ebp
	jmp	.LBB0_237
.LBB0_71:                               #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_72:                               #   in Loop: Header=BB0_223 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	CommandOptions(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_75
# BB#73:                                #   in Loop: Header=BB0_223 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_75
# BB#74:                                #   in Loop: Header=BB0_223 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_75:                               #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_78
# BB#76:                                #   in Loop: Header=BB0_223 Depth=1
	testq	%rax, %rax
	je	.LBB0_78
# BB#77:                                #   in Loop: Header=BB0_223 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_78:                               #   in Loop: Header=BB0_223 Depth=1
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_80
# BB#79:                                #   in Loop: Header=BB0_223 Depth=1
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_81
.LBB0_80:                               #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_81:                               #   in Loop: Header=BB0_223 Depth=1
	movb	$17, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_83
# BB#82:                                #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_84
.LBB0_83:                               #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_84:                               #   in Loop: Header=BB0_223 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	CommandOptions(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_87
# BB#85:                                #   in Loop: Header=BB0_223 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_87
# BB#86:                                #   in Loop: Header=BB0_223 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_87:                               #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_90
# BB#88:                                #   in Loop: Header=BB0_223 Depth=1
	testq	%rax, %rax
	je	.LBB0_90
# BB#89:                                #   in Loop: Header=BB0_223 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_90:                               # %.preheader
                                        #   in Loop: Header=BB0_223 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB0_92
	.p2align	4, 0x90
.LBB0_91:                               #   in Loop: Header=BB0_92 Depth=2
	incq	%r13
.LBB0_92:                               #   Parent Loop BB0_223 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r13), %eax
	cmpb	$31, %al
	jle	.LBB0_96
# BB#93:                                #   in Loop: Header=BB0_92 Depth=2
	cmpb	$32, %al
	je	.LBB0_97
# BB#94:                                #   in Loop: Header=BB0_92 Depth=2
	cmpb	$125, %al
	je	.LBB0_97
# BB#95:                                #   in Loop: Header=BB0_92 Depth=2
	cmpb	$123, %al
	je	.LBB0_97
	jmp	.LBB0_121
	.p2align	4, 0x90
.LBB0_96:                               #   in Loop: Header=BB0_92 Depth=2
	movl	%eax, %edx
	addb	$-9, %dl
	cmpb	$2, %dl
	jae	.LBB0_120
.LBB0_97:                               #   in Loop: Header=BB0_92 Depth=2
	testl	%ecx, %ecx
	jle	.LBB0_91
# BB#98:                                #   in Loop: Header=BB0_92 Depth=2
	movslq	%ecx, %rax
	movb	$0, 96(%rsp,%rax)
	cmpq	%rbx, 8(%rbx)
	je	.LBB0_111
# BB#99:                                #   in Loop: Header=BB0_92 Depth=2
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_101
# BB#100:                               #   in Loop: Header=BB0_92 Depth=2
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_102
.LBB0_101:                              #   in Loop: Header=BB0_92 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB0_102:                              #   in Loop: Header=BB0_92 Depth=2
	movb	$1, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movw	$1, 41(%rbp)
	movq	no_fpos(%rip), %rsi
	movzwl	2(%rsi), %eax
	movw	%ax, 34(%rbp)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	4(%rsi), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbp), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbp)
	andl	4(%rsi), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_104
# BB#103:                               #   in Loop: Header=BB0_92 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_105
.LBB0_104:                              #   in Loop: Header=BB0_92 Depth=2
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_105:                              #   in Loop: Header=BB0_92 Depth=2
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB0_108
# BB#106:                               #   in Loop: Header=BB0_92 Depth=2
	testq	%rax, %rax
	je	.LBB0_108
# BB#107:                               #   in Loop: Header=BB0_92 Depth=2
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_108:                              #   in Loop: Header=BB0_92 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_111
# BB#109:                               #   in Loop: Header=BB0_92 Depth=2
	testq	%rax, %rax
	je	.LBB0_111
# BB#110:                               #   in Loop: Header=BB0_92 Depth=2
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_111:                              #   in Loop: Header=BB0_92 Depth=2
	movq	no_fpos(%rip), %rdx
	leaq	96(%rsp), %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_113
# BB#112:                               #   in Loop: Header=BB0_92 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_114
.LBB0_113:                              #   in Loop: Header=BB0_92 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_114:                              #   in Loop: Header=BB0_92 Depth=2
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB0_117
# BB#115:                               #   in Loop: Header=BB0_92 Depth=2
	testq	%rax, %rax
	je	.LBB0_117
# BB#116:                               #   in Loop: Header=BB0_92 Depth=2
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_117:                              #   in Loop: Header=BB0_92 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	je	.LBB0_91
# BB#118:                               #   in Loop: Header=BB0_92 Depth=2
	testq	%rax, %rax
	je	.LBB0_91
# BB#119:                               #   in Loop: Header=BB0_92 Depth=2
	movq	16(%rbp), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rbp)
	movq	16(%rax), %rsi
	movq	%rbp, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	jmp	.LBB0_91
.LBB0_120:                              #   in Loop: Header=BB0_92 Depth=2
	testb	%al, %al
	je	.LBB0_122
.LBB0_121:                              #   in Loop: Header=BB0_92 Depth=2
	movslq	%ecx, %rdx
	incl	%ecx
	movb	%al, 96(%rsp,%rdx)
	incq	%r13
	jmp	.LBB0_92
.LBB0_122:                              #   in Loop: Header=BB0_223 Depth=1
	testl	%ecx, %ecx
	movl	76(%rsp), %r13d         # 4-byte Reload
	jle	.LBB0_173
# BB#123:                               #   in Loop: Header=BB0_223 Depth=1
	movslq	%ecx, %rax
	movb	$0, 96(%rsp,%rax)
	movq	no_fpos(%rip), %rdx
	leaq	96(%rsp), %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_166
# BB#124:                               #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_167
.LBB0_125:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_181
# BB#126:                               #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rbx,%r15,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB0_213
.LBB0_181:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_182:                              # %GetArg.exit373.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%r14d, %r14d
	movl	$1, %edi
	movl	$13, %esi
	movl	$.L.str.19, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_183:                              # %GetArg.exit373.thread434
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%r14, %rsi
	callq	MakeWord
	movl	$5, %edi
	jmp	.LBB0_205
.LBB0_128:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_184
# BB#129:                               #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rbx,%r15,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB0_214
.LBB0_184:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_185:                              # %GetArg.exit376.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%r14d, %r14d
	movl	$1, %edi
	movl	$14, %esi
	movl	$.L.str.20, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_186:                              # %GetArg.exit376.thread439
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%r14, %rsi
	callq	MakeWord
	movl	$6, %edi
	jmp	.LBB0_205
.LBB0_131:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_187
# BB#132:                               #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rbx,%r15,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB0_215
.LBB0_187:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_188:                              # %GetArg.exit382.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%r14d, %r14d
	movl	$1, %edi
	movl	$16, %esi
	movl	$.L.str.22, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_189:                              # %GetArg.exit382.thread449
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	%r14, %rdi
	callq	strlen
	movl	$4294967293, %ecx       # imm = 0xFFFFFFFD
	addq	%rcx, %rax
	testl	%eax, %eax
	js	.LBB0_195
# BB#190:                               #   in Loop: Header=BB0_223 Depth=1
	cltq
	cmpb	$46, (%r14,%rax)
	jne	.LBB0_195
# BB#191:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$108, 1(%r14,%rax)
	jne	.LBB0_195
# BB#192:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$116, 2(%r14,%rax)
	jne	.LBB0_195
# BB#193:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 3(%r14,%rax)
	jne	.LBB0_195
# BB#194:                               #   in Loop: Header=BB0_223 Depth=1
	movb	$0, (%r14,%rax)
.LBB0_195:                              # %.thread505
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$.L.str.2, %esi
	xorl	%ecx, %ecx
	movl	$2, %r8d
	movq	%r14, %rdi
	callq	DefineFile
	jmp	.LBB0_237
.LBB0_134:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_196
# BB#135:                               #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rbx,%r15,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB0_216
.LBB0_196:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_197:                              # %GetArg.exit370.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%r14d, %r14d
	movl	$1, %edi
	movl	$12, %esi
	movl	$.L.str.18, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_198:                              # %GetArg.exit370.thread429
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%r14, %rsi
	callq	MakeWord
	movl	$7, %edi
	jmp	.LBB0_205
.LBB0_137:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_199
# BB#138:                               #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rbx,%r15,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB0_217
.LBB0_199:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_200:                              # %GetArg.exit367.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%r14d, %r14d
	movl	$1, %edi
	movl	$11, %esi
	movl	$.L.str.17, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_201:                              # %GetArg.exit367.thread424
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%r14, %rsi
	callq	MakeWord
	movl	$3, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%r14, %rsi
	callq	MakeWord
	movl	$4, %edi
	jmp	.LBB0_205
.LBB0_140:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_202
# BB#141:                               #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rbx,%r15,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB0_218
.LBB0_202:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_203:                              # %GetArg.exit379.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%r14d, %r14d
	movl	$1, %edi
	movl	$15, %esi
	movl	$.L.str.21, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_204:                              # %GetArg.exit379.thread444
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%r14, %rsi
	callq	MakeWord
	movl	$1, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%r14, %rsi
	callq	MakeWord
	movl	$2, %edi
.LBB0_205:                              # %GetArg.exit361.thread414
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, %rsi
	callq	AddToPath
	jmp	.LBB0_237
.LBB0_143:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_206
# BB#144:                               #   in Loop: Header=BB0_223 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	8(%rax,%r15,8), %rbx
	cmpb	$45, (%rbx)
	jne	.LBB0_219
.LBB0_206:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_207:                              # %GetArg.exit385.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%ebx, %ebx
	movl	$1, %edi
	movl	$18, %esi
	movl	$.L.str.24, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_208:                              # %GetArg.exit385.thread454
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$.L.str.2, %esi
	movl	$7, %ecx
	movl	$1, %r8d
	movq	%rbx, %rdi
	callq	DefineFile
	movq	no_fpos(%rip), %rdx
	movl	$.L.str.25, %esi
	movl	$8, %ecx
	movl	$1, %r8d
	movq	%rbx, %rdi
	callq	DefineFile
	movq	48(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_237
.LBB0_146:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_209
# BB#147:                               #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rbx,%r15,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB0_220
.LBB0_209:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_210:                              # %GetArg.exit364.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%r14d, %r14d
	movl	$1, %edi
	movl	$9, %esi
	movl	$.L.str.14, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_211:                              # %GetArg.exit364.thread419
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	%r14, %rdi
	callq	ErrorInit
	jmp	.LBB0_237
.LBB0_149:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	20(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB0_151
# BB#150:                               #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rbx,%r15,8), %rax
	cmpb	$45, (%rax)
	jne	.LBB0_221
.LBB0_151:                              #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_152:                              # %GetArg.exit361.thread
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	$8, %esi
	movl	$.L.str.13, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB0_237
.LBB0_153:                              #   in Loop: Header=BB0_223 Depth=1
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_0(%rip), %xmm0
	jmp	.LBB0_156
.LBB0_154:                              #   in Loop: Header=BB0_223 Depth=1
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_2(%rip), %xmm0
	jmp	.LBB0_156
.LBB0_155:                              #   in Loop: Header=BB0_223 Depth=1
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_1(%rip), %xmm0
.LBB0_156:                              #   in Loop: Header=BB0_223 Depth=1
	cvttss2si	%xmm0, %eax
	movl	%eax, PlainCharWidth(%rip)
.LBB0_157:                              #   in Loop: Header=BB0_223 Depth=1
	movb	26(%rsp), %al
	addb	$-99, %al
	cmpb	$13, %al
	movq	8(%rsp), %rcx           # 8-byte Reload
	ja	.LBB0_165
# BB#158:                               #   in Loop: Header=BB0_223 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI0_2(,%rax,8)
.LBB0_159:                              #   in Loop: Header=BB0_223 Depth=1
	movss	28(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_3(%rip), %xmm0
	jmp	.LBB0_163
.LBB0_160:                              #   in Loop: Header=BB0_223 Depth=1
	movss	28(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_0(%rip), %xmm0
	jmp	.LBB0_163
.LBB0_161:                              #   in Loop: Header=BB0_223 Depth=1
	movss	28(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_2(%rip), %xmm0
	jmp	.LBB0_163
.LBB0_162:                              #   in Loop: Header=BB0_223 Depth=1
	movss	28(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI0_1(%rip), %xmm0
.LBB0_163:                              #   in Loop: Header=BB0_223 Depth=1
	cvttss2si	%xmm0, %eax
	movl	%eax, PlainCharHeight(%rip)
	movl	4(%rsp), %ebp           # 4-byte Reload
	jmp	.LBB0_238
.LBB0_164:                              #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movq	(%rbx,%r15,8), %rax
	movsbl	1(%rax), %r9d
	movl	$1, %edi
	movl	$20, %esi
	movl	$.L.str.59, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB0_157
.LBB0_165:                              #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movq	(%rbx,%r15,8), %rax
	movsbl	1(%rax), %r9d
	movl	$1, %edi
	movl	$21, %esi
	movl	$.L.str.59, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB0_66
.LBB0_166:                              #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_167:                              #   in Loop: Header=BB0_223 Depth=1
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB0_170
# BB#168:                               #   in Loop: Header=BB0_223 Depth=1
	testq	%rax, %rax
	je	.LBB0_170
# BB#169:                               #   in Loop: Header=BB0_223 Depth=1
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_170:                              #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_173
# BB#171:                               #   in Loop: Header=BB0_223 Depth=1
	testq	%rax, %rax
	je	.LBB0_173
# BB#172:                               #   in Loop: Header=BB0_223 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_173:                              #   in Loop: Header=BB0_223 Depth=1
	cmpq	%rbx, 8(%rbx)
	je	.LBB0_175
# BB#174:                               #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_237
.LBB0_175:                              #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%r15,8), %r9
	addq	$2, %r9
	movl	$1, %edi
	movl	$25, %esi
	movl	$.L.str.65, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB0_236
.LBB0_212:                              # %GetArg.exit
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%rbp, %rbp
	movl	4(%rsp), %r14d          # 4-byte Reload
	jne	.LBB0_178
	jmp	.LBB0_177
.LBB0_213:                              # %GetArg.exit373
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%r14, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jne	.LBB0_183
	jmp	.LBB0_182
.LBB0_214:                              # %GetArg.exit376
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%r14, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jne	.LBB0_186
	jmp	.LBB0_185
.LBB0_215:                              # %GetArg.exit382
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%r14, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jne	.LBB0_189
	jmp	.LBB0_188
.LBB0_216:                              # %GetArg.exit370
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%r14, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jne	.LBB0_198
	jmp	.LBB0_197
.LBB0_217:                              # %GetArg.exit367
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%r14, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jne	.LBB0_201
	jmp	.LBB0_200
.LBB0_218:                              # %GetArg.exit379
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%r14, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jne	.LBB0_204
	jmp	.LBB0_203
.LBB0_219:                              # %GetArg.exit385
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%rbx, %rbx
	movl	4(%rsp), %ebp           # 4-byte Reload
	jne	.LBB0_208
	jmp	.LBB0_207
.LBB0_220:                              # %GetArg.exit364
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%r14, %r14
	movl	4(%rsp), %ebp           # 4-byte Reload
	jne	.LBB0_211
	jmp	.LBB0_210
.LBB0_221:                              # %GetArg.exit361
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	testq	%rax, %rax
	movl	4(%rsp), %ebp           # 4-byte Reload
	je	.LBB0_152
# BB#222:                               #   in Loop: Header=BB0_223 Depth=1
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jmp	.LBB0_238
	.p2align	4, 0x90
.LBB0_223:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_92 Depth 2
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movslq	%r12d, %r15
	movq	(%rbx,%r15,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB0_229
# BB#224:                               #   in Loop: Header=BB0_223 Depth=1
	movsbl	1(%r14), %eax
	cmpl	$120, %eax
	ja	.LBB0_64
# BB#225:                               #   in Loop: Header=BB0_223 Depth=1
	movl	$1, %ebp
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_226:                              #   in Loop: Header=BB0_223 Depth=1
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	je	.LBB0_228
# BB#227:                               #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$23, %esi
	movl	$.L.str.63, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_228:                              #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$.L.str.8, %edi
	movl	$.L.str.2, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	callq	DefineFile
	movl	$1, 60(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_236
	.p2align	4, 0x90
.LBB0_229:                              #   in Loop: Header=BB0_223 Depth=1
	movq	%r14, %rdi
	callq	strlen
	movl	$4294967293, %ecx       # imm = 0xFFFFFFFD
	addq	%rcx, %rax
	testl	%eax, %eax
	js	.LBB0_235
# BB#230:                               #   in Loop: Header=BB0_223 Depth=1
	cltq
	cmpb	$46, (%r14,%rax)
	jne	.LBB0_235
# BB#231:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$108, 1(%r14,%rax)
	jne	.LBB0_235
# BB#232:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$116, 2(%r14,%rax)
	jne	.LBB0_235
# BB#233:                               #   in Loop: Header=BB0_223 Depth=1
	cmpb	$0, 3(%r14,%rax)
	jne	.LBB0_235
# BB#234:                               #   in Loop: Header=BB0_223 Depth=1
	movb	$0, (%r14,%rax)
	movq	(%rbx,%r15,8), %r14
	.p2align	4, 0x90
.LBB0_235:                              # %.thread509
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$.L.str.2, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	callq	DefineFile
	incl	%r13d
.LBB0_236:                              # %GetArg.exit361.thread414
                                        #   in Loop: Header=BB0_223 Depth=1
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB0_237:                              # %GetArg.exit361.thread414
                                        #   in Loop: Header=BB0_223 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB0_238:                              # %GetArg.exit361.thread414
                                        #   in Loop: Header=BB0_223 Depth=1
	incl	%r12d
	cmpl	%ecx, %r12d
	movl	%ebp, %eax
	jl	.LBB0_223
# BB#239:                               # %._crit_edge
	cmpl	$0, UseCollate(%rip)
	je	.LBB0_242
.LBB0_240:
	movl	$3, %edi
	movl	$.L.str.2, %esi
	callq	setlocale
	testq	%rax, %rax
	jne	.LBB0_242
# BB#241:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$30, %esi
	movl	$.L.str.67, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_242:
	movq	64(%rsp), %rbx          # 8-byte Reload
	cmpb	$45, (%rbx)
	movq	80(%rsp), %r14          # 8-byte Reload
	jne	.LBB0_245
# BB#243:
	cmpb	$0, 1(%rbx)
	jne	.LBB0_245
# BB#244:
	movq	stdout(%rip), %r15
	jmp	.LBB0_247
.LBB0_245:                              # %.thread
	movl	$.L.str.68, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB0_247
# BB#246:
	movq	no_fpos(%rip), %r8
	xorl	%r15d, %r15d
	movl	$1, %edi
	movl	$27, %esi
	movl	$.L.str.69, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r9
	callq	Error
.LBB0_247:
	callq	ColourInit
	callq	LanguageInit
	movq	BackEnd(%rip), %rax
	movq	%r15, %rdi
	callq	*48(%rax)
	movl	$.L.str.39, %esi
	movl	$.L.str.70, %edx
	movq	%r14, %rdi
	callq	MakeWordThree
	movl	$5, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movl	$.L.str.39, %esi
	movl	$.L.str.71, %edx
	movq	%r14, %rdi
	callq	MakeWordThree
	movl	$6, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movl	$.L.str.39, %esi
	movl	$.L.str.72, %edx
	movq	%r14, %rdi
	callq	MakeWordThree
	movl	$7, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movl	$.L.str.39, %esi
	movl	$.L.str.42, %edx
	movq	%r14, %rdi
	callq	MakeWordThree
	movl	$4, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movl	$.L.str.39, %esi
	movl	$.L.str.42, %edx
	movq	%r14, %rdi
	callq	MakeWordThree
	movl	$3, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movl	$.L.str.39, %esi
	movl	$.L.str.40, %edx
	movq	%r14, %rdi
	callq	MakeWordThree
	movl	$2, %edi
	movq	%rax, %rsi
	callq	AddToPath
	movl	$.L.str.39, %esi
	movl	$.L.str.40, %edx
	movq	%r14, %rdi
	callq	MakeWordThree
	movl	$1, %edi
	movq	%rax, %rsi
	callq	AddToPath
	testl	%r13d, %r13d
	jne	.LBB0_249
# BB#248:
	movq	no_fpos(%rip), %rdx
	movl	$.L.str.8, %edi
	movl	$.L.str.2, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	callq	DefineFile
.LBB0_249:
	movq	$0, StartSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.73, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$1, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset -32
	movq	%rax, StartSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.74, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$1, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi25:
	.cfi_adjust_cfa_offset -32
	movq	%rax, GalleySym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.75, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$1, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset -32
	movq	%rax, ForceGalleySym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.76, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$1, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi35:
	.cfi_adjust_cfa_offset -32
	movq	%rax, InputSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.77, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$1, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -32
	movq	%rax, PrintSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.78, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi45:
	.cfi_adjust_cfa_offset -32
	movq	%rax, FilterInSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.79, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset -32
	movq	%rax, FilterOutSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.80, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset -32
	movq	%rax, FilterErrSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.81, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi65:
	.cfi_adjust_cfa_offset -32
	movq	%rbx, OptGallSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.82, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	$53
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi70:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset -32
	movq	%rbx, VerbatimSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.83, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	$54
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi80:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi85:
	.cfi_adjust_cfa_offset -32
	movq	%rbx, RawVerbatimSym(%rip)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.84, %edi
	movl	$143, %esi
	movl	$1, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	pushq	$104
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.85, %edi
	movl	$143, %esi
	movl	$2, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	pushq	$105
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi95:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.86, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	pushq	$82
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi100:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.87, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi103:
	.cfi_adjust_cfa_offset 8
	pushq	$83
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi105:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.88, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	pushq	$84
.Lcfi109:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi110:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.89, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	pushq	$85
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi115:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi116:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.90, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	pushq	$86
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi120:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.91, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	pushq	$87
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi125:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.92, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	pushq	$88
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi130:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi131:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.93, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi132:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	pushq	$89
.Lcfi134:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi135:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi136:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.94, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi137:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi138:
	.cfi_adjust_cfa_offset 8
	pushq	$90
.Lcfi139:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi140:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.95, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi143:
	.cfi_adjust_cfa_offset 8
	pushq	$91
.Lcfi144:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi145:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi146:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.96, %edi
	movl	$143, %esi
	movl	$3, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi147:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi148:
	.cfi_adjust_cfa_offset 8
	pushq	$102
.Lcfi149:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi150:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi151:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.97, %edi
	movl	$143, %esi
	movl	$4, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi152:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi153:
	.cfi_adjust_cfa_offset 8
	pushq	$103
.Lcfi154:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi155:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi156:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.98, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi157:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi158:
	.cfi_adjust_cfa_offset 8
	pushq	$112
.Lcfi159:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi160:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi161:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.99, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi162:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi163:
	.cfi_adjust_cfa_offset 8
	pushq	$113
.Lcfi164:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi165:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.100, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi168:
	.cfi_adjust_cfa_offset 8
	pushq	$114
.Lcfi169:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi170:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi171:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.101, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi172:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	pushq	$115
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi175:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi176:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.102, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi177:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi178:
	.cfi_adjust_cfa_offset 8
	pushq	$116
.Lcfi179:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi180:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi181:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.103, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi182:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi183:
	.cfi_adjust_cfa_offset 8
	pushq	$117
.Lcfi184:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi185:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi186:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.104, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi187:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi188:
	.cfi_adjust_cfa_offset 8
	pushq	$106
.Lcfi189:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi190:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi191:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.105, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi192:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi193:
	.cfi_adjust_cfa_offset 8
	pushq	$107
.Lcfi194:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi195:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi196:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.106, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi197:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi198:
	.cfi_adjust_cfa_offset 8
	pushq	$52
.Lcfi199:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi200:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi201:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi202:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi203:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi204:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi205:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi206:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi207:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi208:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi209:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi210:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi211:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.107, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi212:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi213:
	.cfi_adjust_cfa_offset 8
	pushq	$55
.Lcfi214:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi215:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi216:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi217:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi218:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi219:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi220:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi221:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi222:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi223:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi224:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi225:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi226:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.108, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi227:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi228:
	.cfi_adjust_cfa_offset 8
	pushq	$56
.Lcfi229:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi230:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi231:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.109, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi232:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi233:
	.cfi_adjust_cfa_offset 8
	pushq	$58
.Lcfi234:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi235:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi236:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi237:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi238:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi239:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi240:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi241:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.110, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi242:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi243:
	.cfi_adjust_cfa_offset 8
	pushq	$59
.Lcfi244:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi245:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi246:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi247:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi248:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi249:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi250:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi251:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi252:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi253:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi254:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi255:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi256:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.111, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi257:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi258:
	.cfi_adjust_cfa_offset 8
	pushq	$60
.Lcfi259:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi260:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi261:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi262:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi263:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi264:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi265:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi266:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi267:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi268:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi269:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi270:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi271:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.112, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi272:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi273:
	.cfi_adjust_cfa_offset 8
	pushq	$61
.Lcfi274:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi275:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi276:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi277:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi278:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi279:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi280:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi281:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi282:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi283:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi284:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi285:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi286:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.113, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi287:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi288:
	.cfi_adjust_cfa_offset 8
	pushq	$62
.Lcfi289:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi290:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi291:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi292:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi293:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi294:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi295:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi296:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi297:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi298:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi299:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi300:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi301:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.114, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi302:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi303:
	.cfi_adjust_cfa_offset 8
	pushq	$63
.Lcfi304:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi305:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi306:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi307:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi308:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi309:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi310:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi311:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi312:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi313:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi314:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi315:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi316:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.115, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi317:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi318:
	.cfi_adjust_cfa_offset 8
	pushq	$64
.Lcfi319:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi320:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi321:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi322:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi323:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi324:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi325:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi326:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.116, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi327:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi328:
	.cfi_adjust_cfa_offset 8
	pushq	$65
.Lcfi329:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi330:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi331:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi332:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi333:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi334:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi335:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi336:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi337:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi338:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi339:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi340:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi341:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.117, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi342:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi343:
	.cfi_adjust_cfa_offset 8
	pushq	$65
.Lcfi344:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi345:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi346:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi347:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi348:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi349:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi350:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi351:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi352:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi353:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi354:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi355:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi356:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.118, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi357:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi358:
	.cfi_adjust_cfa_offset 8
	pushq	$66
.Lcfi359:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi360:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi361:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi362:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi363:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi364:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi365:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi366:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.119, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi367:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi368:
	.cfi_adjust_cfa_offset 8
	pushq	$67
.Lcfi369:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi370:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi371:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi372:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi373:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi374:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi375:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi376:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi377:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi378:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi379:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi380:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi381:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.120, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi382:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi383:
	.cfi_adjust_cfa_offset 8
	pushq	$68
.Lcfi384:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi385:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi386:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.121, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi387:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi388:
	.cfi_adjust_cfa_offset 8
	pushq	$69
.Lcfi389:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi390:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi391:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.122, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi392:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi393:
	.cfi_adjust_cfa_offset 8
	pushq	$70
.Lcfi394:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi395:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi396:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.123, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi397:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi398:
	.cfi_adjust_cfa_offset 8
	pushq	$71
.Lcfi399:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi400:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi401:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.124, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi402:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi403:
	.cfi_adjust_cfa_offset 8
	pushq	$72
.Lcfi404:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi405:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi406:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.125, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi407:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi408:
	.cfi_adjust_cfa_offset 8
	pushq	$73
.Lcfi409:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi410:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi411:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi412:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi413:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi414:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi415:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi416:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi417:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi418:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi419:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi420:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi421:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.126, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi422:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi423:
	.cfi_adjust_cfa_offset 8
	pushq	$74
.Lcfi424:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi425:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi426:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi427:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi428:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi429:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi430:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi431:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi432:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi433:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi434:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi435:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi436:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.127, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi437:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi438:
	.cfi_adjust_cfa_offset 8
	pushq	$75
.Lcfi439:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi440:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi441:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi442:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi443:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi444:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi445:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi446:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi447:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi448:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi449:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi450:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi451:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.128, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi452:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi453:
	.cfi_adjust_cfa_offset 8
	pushq	$76
.Lcfi454:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi455:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi456:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi457:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi458:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi459:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi460:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi461:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi462:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi463:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi464:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi465:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi466:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.129, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi467:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi468:
	.cfi_adjust_cfa_offset 8
	pushq	$77
.Lcfi469:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi470:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi471:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi472:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi473:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi474:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi475:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi476:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.130, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi477:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi478:
	.cfi_adjust_cfa_offset 8
	pushq	$78
.Lcfi479:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi480:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi481:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi482:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi483:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi484:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi485:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi486:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.131, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi487:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi488:
	.cfi_adjust_cfa_offset 8
	pushq	$79
.Lcfi489:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi490:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi491:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi492:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi493:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi494:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi495:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi496:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi497:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi498:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi499:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi500:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi501:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.132, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi502:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi503:
	.cfi_adjust_cfa_offset 8
	pushq	$80
.Lcfi504:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi505:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi506:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi507:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi508:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi509:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi510:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi511:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi512:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi513:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi514:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi515:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi516:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.133, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi517:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi518:
	.cfi_adjust_cfa_offset 8
	pushq	$92
.Lcfi519:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi520:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi521:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi522:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi523:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi524:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi525:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi526:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi527:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi528:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi529:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi530:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi531:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.134, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi532:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi533:
	.cfi_adjust_cfa_offset 8
	pushq	$93
.Lcfi534:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi535:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi536:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi537:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi538:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi539:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi540:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi541:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi542:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi543:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi544:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi545:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi546:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.135, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi547:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi548:
	.cfi_adjust_cfa_offset 8
	pushq	$26
.Lcfi549:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi550:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi551:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi552:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi553:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi554:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi555:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi556:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi557:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi558:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi559:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi560:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi561:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.136, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi562:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi563:
	.cfi_adjust_cfa_offset 8
	pushq	$27
.Lcfi564:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi565:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi566:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi567:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi568:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi569:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi570:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi571:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi572:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi573:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi574:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi575:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi576:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.137, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi577:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi578:
	.cfi_adjust_cfa_offset 8
	pushq	$28
.Lcfi579:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi580:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi581:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi582:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi583:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi584:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi585:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi586:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi587:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi588:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi589:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi590:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi591:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.138, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi592:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi593:
	.cfi_adjust_cfa_offset 8
	pushq	$29
.Lcfi594:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi595:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi596:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi597:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi598:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi599:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi600:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi601:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi602:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi603:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi604:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi605:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi606:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.139, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi607:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi608:
	.cfi_adjust_cfa_offset 8
	pushq	$20
.Lcfi609:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi610:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi611:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi612:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi613:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi614:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi615:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi616:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi617:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi618:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi619:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi620:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi621:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.140, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi622:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi623:
	.cfi_adjust_cfa_offset 8
	pushq	$21
.Lcfi624:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi625:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi626:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.141, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi627:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi628:
	.cfi_adjust_cfa_offset 8
	pushq	$22
.Lcfi629:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi630:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi631:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi632:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi633:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi634:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi635:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi636:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi637:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi638:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi639:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi640:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi641:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.142, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi642:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi643:
	.cfi_adjust_cfa_offset 8
	pushq	$23
.Lcfi644:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi645:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi646:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.143, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi647:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi648:
	.cfi_adjust_cfa_offset 8
	pushq	$24
.Lcfi649:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi650:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi651:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi652:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi653:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi654:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi655:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi656:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.144, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi657:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi658:
	.cfi_adjust_cfa_offset 8
	pushq	$25
.Lcfi659:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi660:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi661:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi662:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi663:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi664:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi665:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi666:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.145, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi667:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi668:
	.cfi_adjust_cfa_offset 8
	pushq	$30
.Lcfi669:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi670:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi671:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi672:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi673:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi674:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi675:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi676:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.146, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi677:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi678:
	.cfi_adjust_cfa_offset 8
	pushq	$31
.Lcfi679:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi680:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi681:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi682:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi683:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi684:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi685:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi686:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.147, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi687:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi688:
	.cfi_adjust_cfa_offset 8
	pushq	$32
.Lcfi689:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi690:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi691:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi692:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi693:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi694:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi695:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi696:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.148, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi697:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi698:
	.cfi_adjust_cfa_offset 8
	pushq	$33
.Lcfi699:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi700:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi701:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi702:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi703:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi704:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi705:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi706:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.149, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi707:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi708:
	.cfi_adjust_cfa_offset 8
	pushq	$35
.Lcfi709:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi710:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi711:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi712:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi713:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi714:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi715:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi716:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi717:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi718:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi719:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi720:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi721:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.150, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi722:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi723:
	.cfi_adjust_cfa_offset 8
	pushq	$34
.Lcfi724:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi725:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi726:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi727:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi728:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi729:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi730:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi731:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi732:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi733:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi734:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi735:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi736:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.151, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi737:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi738:
	.cfi_adjust_cfa_offset 8
	pushq	$36
.Lcfi739:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi740:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi741:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi742:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi743:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi744:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi745:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi746:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.152, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi747:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi748:
	.cfi_adjust_cfa_offset 8
	pushq	$37
.Lcfi749:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi750:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi751:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi752:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi753:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi754:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi755:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi756:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.153, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi757:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi758:
	.cfi_adjust_cfa_offset 8
	pushq	$38
.Lcfi759:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi760:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi761:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi762:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi763:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi764:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi765:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi766:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.154, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi767:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi768:
	.cfi_adjust_cfa_offset 8
	pushq	$39
.Lcfi769:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi770:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi771:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi772:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi773:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi774:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi775:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi776:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.155, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi777:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi778:
	.cfi_adjust_cfa_offset 8
	pushq	$40
.Lcfi779:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi780:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi781:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi782:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi783:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi784:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi785:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi786:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.156, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi787:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi788:
	.cfi_adjust_cfa_offset 8
	pushq	$41
.Lcfi789:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi790:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi791:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi792:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi793:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi794:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi795:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi796:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.157, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi797:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi798:
	.cfi_adjust_cfa_offset 8
	pushq	$44
.Lcfi799:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi800:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi801:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi802:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi803:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi804:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi805:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi806:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.158, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi807:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi808:
	.cfi_adjust_cfa_offset 8
	pushq	$42
.Lcfi809:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi810:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi811:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi812:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi813:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi814:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi815:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi816:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.159, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi817:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi818:
	.cfi_adjust_cfa_offset 8
	pushq	$43
.Lcfi819:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi820:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi821:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi822:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi823:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi824:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi825:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi826:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.160, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi827:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi828:
	.cfi_adjust_cfa_offset 8
	pushq	$45
.Lcfi829:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi830:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi831:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.161, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi832:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi833:
	.cfi_adjust_cfa_offset 8
	pushq	$46
.Lcfi834:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi835:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi836:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.162, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi837:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi838:
	.cfi_adjust_cfa_offset 8
	pushq	$47
.Lcfi839:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi840:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi841:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi842:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi843:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi844:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi845:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi846:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.163, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi847:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi848:
	.cfi_adjust_cfa_offset 8
	pushq	$48
.Lcfi849:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi850:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi851:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi852:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi853:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi854:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi855:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi856:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.164, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi857:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi858:
	.cfi_adjust_cfa_offset 8
	pushq	$49
.Lcfi859:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi860:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi861:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi862:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi863:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi864:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi865:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi866:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.165, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi867:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi868:
	.cfi_adjust_cfa_offset 8
	pushq	$50
.Lcfi869:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi870:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi871:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi872:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi873:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi874:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi875:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi876:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi877:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi878:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi879:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi880:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi881:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.166, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi882:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi883:
	.cfi_adjust_cfa_offset 8
	pushq	$51
.Lcfi884:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi885:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi886:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi887:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi888:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi889:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi890:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi891:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi892:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi893:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi894:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi895:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi896:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.167, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi897:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi898:
	.cfi_adjust_cfa_offset 8
	pushq	$94
.Lcfi899:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi900:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi901:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi902:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi903:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi904:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi905:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi906:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.168, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi907:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi908:
	.cfi_adjust_cfa_offset 8
	pushq	$95
.Lcfi909:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi910:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi911:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi912:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi913:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi914:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi915:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi916:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.169, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi917:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi918:
	.cfi_adjust_cfa_offset 8
	pushq	$96
.Lcfi919:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi920:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi921:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi922:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi923:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi924:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi925:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi926:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi927:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi928:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi929:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi930:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi931:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.170, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi932:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi933:
	.cfi_adjust_cfa_offset 8
	pushq	$97
.Lcfi934:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi935:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi936:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi937:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi938:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi939:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi940:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi941:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi942:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi943:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi944:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi945:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi946:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.171, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi947:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi948:
	.cfi_adjust_cfa_offset 8
	pushq	$98
.Lcfi949:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi950:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi951:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi952:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi953:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi954:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi955:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi956:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi957:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi958:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi959:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi960:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi961:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.172, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi962:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi963:
	.cfi_adjust_cfa_offset 8
	pushq	$99
.Lcfi964:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi965:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi966:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi967:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi968:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi969:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi970:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi971:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi972:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi973:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi974:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi975:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi976:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.173, %edi
	movl	$143, %esi
	movl	$101, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi977:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi978:
	.cfi_adjust_cfa_offset 8
	pushq	$6
.Lcfi979:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi980:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi981:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi982:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi983:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi984:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi985:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi986:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi987:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi988:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi989:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi990:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi991:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.174, %edi
	movl	$143, %esi
	movl	$101, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi992:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi993:
	.cfi_adjust_cfa_offset 8
	pushq	$7
.Lcfi994:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi995:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi996:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi997:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi998:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi999:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1000:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1001:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1002:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1003:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1004:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1005:
	.cfi_adjust_cfa_offset -32
	orb	$16, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1006:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.175, %edi
	movl	$143, %esi
	movl	$0, %ecx
	movl	$1, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1007:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1008:
	.cfi_adjust_cfa_offset 8
	pushq	$5
.Lcfi1009:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1010:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1011:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.176, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$1, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1012:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1013:
	.cfi_adjust_cfa_offset 8
	pushq	$4
.Lcfi1014:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1015:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1016:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1017:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi1018:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1019:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1020:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1021:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.177, %edi
	movl	$143, %esi
	movl	$5, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1022:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1023:
	.cfi_adjust_cfa_offset 8
	pushq	$19
.Lcfi1024:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1025:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1026:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1027:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1028:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1029:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1030:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1031:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1032:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1033:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1034:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1035:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	andl	$16383983, %ecx         # imm = 0xF9FFEF
	leal	16(%rcx), %eax
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	movw	%ax, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1036:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.178, %edi
	movl	$143, %esi
	movl	$5, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1037:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1038:
	.cfi_adjust_cfa_offset 8
	pushq	$19
.Lcfi1039:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1040:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1041:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1042:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1043:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1044:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1045:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1046:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1047:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1048:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1049:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1050:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	andl	$16383983, %ecx         # imm = 0xF9FFEF
	orl	$131088, %ecx           # imm = 0x20010
	movw	%cx, 41(%rbx)
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1051:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.39, %edi
	movl	$143, %esi
	movl	$5, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1052:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1053:
	.cfi_adjust_cfa_offset 8
	pushq	$19
.Lcfi1054:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1055:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1056:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1057:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1058:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1059:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1060:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1061:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1062:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1063:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1064:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1065:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	andl	$16383983, %ecx         # imm = 0xF9FFEF
	orl	$262160, %ecx           # imm = 0x40010
	movw	%cx, 41(%rbx)
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1066:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.179, %edi
	movl	$143, %esi
	movl	$5, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1067:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1068:
	.cfi_adjust_cfa_offset 8
	pushq	$19
.Lcfi1069:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1070:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1071:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1072:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1073:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1074:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1075:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1076:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1077:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1078:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1079:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1080:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	orl	$393232, %ecx           # imm = 0x60010
	movw	%cx, 41(%rbx)
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1081:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.180, %edi
	movl	$143, %esi
	movl	$6, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1082:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1083:
	.cfi_adjust_cfa_offset 8
	pushq	$18
.Lcfi1084:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1085:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1086:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1087:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1088:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1089:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1090:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1091:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1092:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1093:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1094:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1095:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	andl	$16383983, %ecx         # imm = 0xF9FFEF
	leal	16(%rcx), %eax
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	movw	%ax, 41(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1096:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.181, %edi
	movl	$143, %esi
	movl	$6, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1097:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1098:
	.cfi_adjust_cfa_offset 8
	pushq	$18
.Lcfi1099:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1100:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1101:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1102:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1103:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1104:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1105:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1106:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1107:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1108:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1109:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1110:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	andl	$16383983, %ecx         # imm = 0xF9FFEF
	orl	$131088, %ecx           # imm = 0x20010
	movw	%cx, 41(%rbx)
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1111:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.182, %edi
	movl	$143, %esi
	movl	$6, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1112:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1113:
	.cfi_adjust_cfa_offset 8
	pushq	$18
.Lcfi1114:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1115:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1116:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1117:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1118:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1119:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1120:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1121:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1122:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1123:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1124:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1125:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	andl	$16383983, %ecx         # imm = 0xF9FFEF
	orl	$262160, %ecx           # imm = 0x40010
	movw	%cx, 41(%rbx)
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1126:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.183, %edi
	movl	$143, %esi
	movl	$6, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1127:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1128:
	.cfi_adjust_cfa_offset 8
	pushq	$18
.Lcfi1129:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1130:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1131:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1132:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1133:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1134:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1135:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1136:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1137:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1138:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1139:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1140:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	orl	$393232, %ecx           # imm = 0x60010
	movw	%cx, 41(%rbx)
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1141:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.184, %edi
	movl	$143, %esi
	movl	$7, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1142:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1143:
	.cfi_adjust_cfa_offset 8
	pushq	$17
.Lcfi1144:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1145:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1146:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1147:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1148:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1149:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1150:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1151:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1152:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1153:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1154:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1155:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	andl	$16383983, %ecx         # imm = 0xF9FFEF
	orl	$262160, %ecx           # imm = 0x40010
	movw	%cx, 41(%rbx)
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1156:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.185, %edi
	movl	$143, %esi
	movl	$7, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1157:
	.cfi_adjust_cfa_offset 8
	pushq	StartSym(%rip)
.Lcfi1158:
	.cfi_adjust_cfa_offset 8
	pushq	$17
.Lcfi1159:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1160:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1161:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.214, %edi
	movl	$144, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1162:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1163:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1164:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1165:
	.cfi_adjust_cfa_offset -32
	movq	no_fpos(%rip), %rdx
	subq	$8, %rsp
.Lcfi1166:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.215, %edi
	movl	$146, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	pushq	$0
.Lcfi1167:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi1168:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi1169:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi1170:
	.cfi_adjust_cfa_offset -32
	movzwl	41(%rbx), %eax
	movzbl	43(%rbx), %ecx
	shll	$16, %ecx
	orl	%eax, %ecx
	orl	$393232, %ecx           # imm = 0x60010
	movw	%cx, 41(%rbx)
	shrl	$16, %ecx
	movb	%cl, 43(%rbx)
	callq	FontInit
	callq	InitTime
	callq	FilterInit
	callq	EnvInit
	movq	StartSym(%rip), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	PushScope
	xorl	%edi, %edi
	callq	FirstFile
	movzwl	%ax, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	callq	LexPush
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	InitParser
	movq	no_fpos(%rip), %rsi
	movq	StartSym(%rip), %r9
	movl	$104, %edi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$1, %r8d
	callq	NewToken
	movq	%rax, 32(%rsp)
	movq	StartSym(%rip), %rsi
	leaq	32(%rsp), %rdi
	movl	$1, %edx
	movl	$1, %ecx
	callq	Parse
	movq	%rax, %rbx
	movq	CommandOptions(%rip), %rdi
	callq	DisposeObject
	movq	%rbx, %rdi
	callq	TransferEnd
	callq	TransferClose
	movq	BackEnd(%rip), %rax
	callq	*104(%rax)
	movq	BackEnd(%rip), %rcx
	xorl	%eax, %eax
	callq	*224(%rcx)
	callq	CrossClose
	callq	CloseFiles
	movl	$1, %edi
	callq	FilterScavenge
	testl	%ebp, %ebp
	je	.LBB0_251
# BB#250:
	movq	no_fpos(%rip), %r8
	movl	TotalWordCount(%rip), %r9d
	movl	$1, %edi
	movl	$29, %esi
	movl	$.L.str.186, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_251:
	callq	CheckErrorBlocks
	movq	MsgCat(%rip), %rdi
	callq	catclose
	xorl	%edi, %edi
	callq	exit
.LBB0_252:
	movq	stderr(%rip), %rdi
	callq	PrintUsage
	xorl	%edi, %edi
	callq	exit
.LBB0_253:
	movq	stderr(%rip), %rdi
	movl	$.L.str.26, %esi
	movl	$.L.str.27, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.28, %esi
	movl	$.L.str.29, %edx
	movl	$.L.str.30, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.28, %esi
	movl	$.L.str.31, %edx
	movl	$.L.str.32, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.33, %esi
	movl	$.L.str.34, %edx
	movl	$.L.str.35, %ecx
	movl	$.L.str.36, %r8d
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.37, %esi
	movl	$.L.str.38, %edx
	movl	$.L.str.39, %r8d
	movl	$.L.str.40, %r9d
	xorl	%eax, %eax
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.37, %esi
	movl	$.L.str.41, %edx
	movl	$.L.str.39, %r8d
	movl	$.L.str.42, %r9d
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.43, %esi
	movl	$.L.str.44, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.45, %esi
	movl	$.L.str.46, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.47, %esi
	movl	$.L.str.44, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.48, %esi
	movl	$.L.str.46, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.49, %esi
	movl	$.L.str.46, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stderr(%rip), %rcx
	movl	$.L.str.51, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.52, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.53, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.54, %edi
	movl	$48, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.55, %edi
	movl	$49, %esi
	movl	$1, %edx
	callq	fwrite
	xorl	%edi, %edi
	callq	exit
.LBB0_254:
	movl	$.L.str.7, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$.L.str.8, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	cmpl	$0, UseCollate(%rip)
	jne	.LBB0_240
	jmp	.LBB0_242
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_226
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_22
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_42
	.quad	.LBB0_44
	.quad	.LBB0_28
	.quad	.LBB0_34
	.quad	.LBB0_64
	.quad	.LBB0_36
	.quad	.LBB0_47
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_46
	.quad	.LBB0_21
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_9
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_38
	.quad	.LBB0_64
	.quad	.LBB0_39
	.quad	.LBB0_253
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_20
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_54
	.quad	.LBB0_64
	.quad	.LBB0_57
	.quad	.LBB0_60
	.quad	.LBB0_55
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_49
	.quad	.LBB0_40
	.quad	.LBB0_64
	.quad	.LBB0_53
	.quad	.LBB0_59
	.quad	.LBB0_61
	.quad	.LBB0_64
	.quad	.LBB0_7
	.quad	.LBB0_14
	.quad	.LBB0_64
	.quad	.LBB0_64
	.quad	.LBB0_63
	.quad	.LBB0_64
	.quad	.LBB0_252
	.quad	.LBB0_64
	.quad	.LBB0_238
	.quad	.LBB0_62
.LJTI0_1:
	.quad	.LBB0_19
	.quad	.LBB0_164
	.quad	.LBB0_164
	.quad	.LBB0_164
	.quad	.LBB0_164
	.quad	.LBB0_164
	.quad	.LBB0_154
	.quad	.LBB0_164
	.quad	.LBB0_164
	.quad	.LBB0_164
	.quad	.LBB0_153
	.quad	.LBB0_164
	.quad	.LBB0_164
	.quad	.LBB0_155
.LJTI0_2:
	.quad	.LBB0_159
	.quad	.LBB0_165
	.quad	.LBB0_165
	.quad	.LBB0_165
	.quad	.LBB0_165
	.quad	.LBB0_165
	.quad	.LBB0_161
	.quad	.LBB0_165
	.quad	.LBB0_165
	.quad	.LBB0_165
	.quad	.LBB0_160
	.quad	.LBB0_165
	.quad	.LBB0_165
	.quad	.LBB0_162

	.text
	.p2align	4, 0x90
	.type	PrintUsage,@function
PrintUsage:                             # @PrintUsage
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1171:
	.cfi_def_cfa_offset 16
.Lcfi1172:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.26, %esi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.187, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.188, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.189, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.190, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.191, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.192, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.193, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.194, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.195, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.196, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.197, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.198, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.199, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.200, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.201, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.202, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.203, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.204, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.205, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.206, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.207, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.208, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.209, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.210, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.211, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.212, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.213, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.26, %esi
	movl	$.L.str.2, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fprintf                 # TAILCALL
.Lfunc_end1:
	.size	PrintUsage, .Lfunc_end1-PrintUsage
	.cfi_endproc

	.type	MemCheck,@object        # @MemCheck
	.bss
	.globl	MemCheck
	.p2align	3
MemCheck:
	.quad	0
	.size	MemCheck, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"LOUTLIB"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"lout.lib"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.zero	1
	.size	.L.str.2, 1

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"unable to initialize locale"
	.size	.L.str.3, 28

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"C"
	.size	.L.str.4, 2

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s/%s/%s/LC_MESSAGES/errors.%s"
	.size	.L.str.5, 31

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"locale"
	.size	.L.str.6, 7

	.type	MsgCat,@object          # @MsgCat
	.comm	MsgCat,8,8
	.type	TotalWordCount,@object  # @TotalWordCount
	.comm	TotalWordCount,4,4
	.type	BackEnd,@object         # @BackEnd
	.comm	BackEnd,8,8
	.type	InitializeAll,@object   # @InitializeAll
	.comm	InitializeAll,4,4
	.type	UseCollate,@object      # @UseCollate
	.comm	UseCollate,4,4
	.type	AllowCrossDb,@object    # @AllowCrossDb
	.comm	AllowCrossDb,4,4
	.type	InMemoryDbIndexes,@object # @InMemoryDbIndexes
	.comm	InMemoryDbIndexes,4,4
	.type	SafeExecution,@object   # @SafeExecution
	.comm	SafeExecution,4,4
	.type	Kern,@object            # @Kern
	.comm	Kern,4,4
	.type	AltErrorFormat,@object  # @AltErrorFormat
	.comm	AltErrorFormat,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"lout"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"-"
	.size	.L.str.8, 2

	.type	CommandOptions,@object  # @CommandOptions
	.comm	CommandOptions,8,8
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"usage: -o <filename>"
	.size	.L.str.10, 21

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	".lt"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"-o: output file name %s ends with %s"
	.size	.L.str.12, 37

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"usage: -c <filename>"
	.size	.L.str.13, 21

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"usage: -e <filename>"
	.size	.L.str.14, 21

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"EPS"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"usage: -EPS"
	.size	.L.str.16, 12

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"usage: -D <directoryname>"
	.size	.L.str.17, 26

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"usage: -C <directoryname>"
	.size	.L.str.18, 26

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"usage: -F <directoryname>"
	.size	.L.str.19, 26

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"usage: -H <directoryname>"
	.size	.L.str.20, 26

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"usage: -I <directoryname>"
	.size	.L.str.21, 26

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"usage: -i <filename>"
	.size	.L.str.22, 21

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"two -h options illegal"
	.size	.L.str.23, 23

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"usage: -h <filename>"
	.size	.L.str.24, 21

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	".lh"
	.size	.L.str.25, 4

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"%s\n"
	.size	.L.str.26, 4

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Basser Lout Version 3.24 (October 2000)"
	.size	.L.str.27, 40

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"%-28s %s\n"
	.size	.L.str.28, 10

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Basser Lout written by:"
	.size	.L.str.29, 24

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Jeffrey H. Kingston (jeff@cs.usyd.edu.au)"
	.size	.L.str.30, 42

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Free source available from:"
	.size	.L.str.31, 28

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"ftp://ftp.cs.usyd.edu.au/jeff/lout"
	.size	.L.str.32, 35

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%-28s %s %s\n"
	.size	.L.str.33, 13

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"This executable compiled:"
	.size	.L.str.34, 26

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"00:00:00"
	.size	.L.str.35, 9

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Jan 01 1970"
	.size	.L.str.36, 12

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"%-28s %s%s%s\n"
	.size	.L.str.37, 14

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"System include directory:"
	.size	.L.str.38, 26

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"/"
	.size	.L.str.39, 2

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"include"
	.size	.L.str.40, 8

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"System database directory:"
	.size	.L.str.41, 27

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"data"
	.size	.L.str.42, 5

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"Database index files created afresh automatically:%s\n"
	.size	.L.str.43, 54

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	" yes"
	.size	.L.str.44, 5

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"Safe execution (disabling system()) is default:%s\n"
	.size	.L.str.45, 51

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	" no"
	.size	.L.str.46, 4

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"strcoll() used for sorting by default:%s\n"
	.size	.L.str.47, 42

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"PDF compression on:%s\n"
	.size	.L.str.48, 23

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"Debugging (-d, -dd, -ddd flags) available:%s\n"
	.size	.L.str.49, 46

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Basser Lout comes with ABSOLUTELY NO WARRANTY.\n"
	.size	.L.str.51, 48

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"This is free software, and you are welcome to\n"
	.size	.L.str.52, 47

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"redistribute it under certain conditions.  For\n"
	.size	.L.str.53, 48

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"details on both points, consult the GNU General\n"
	.size	.L.str.54, 49

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"Public License (distributed with this software).\n"
	.size	.L.str.55, 50

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"PDF"
	.size	.L.str.56, 4

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"%f%c%f%c"
	.size	.L.str.57, 9

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"usage: lout -%c<length><length>"
	.size	.L.str.58, 32

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"lout -%c: units must be c, i, p, or m"
	.size	.L.str.59, 38

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"%s - debug flags not implemented"
	.size	.L.str.60, 33

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"-m%ld"
	.size	.L.str.61, 6

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"checking memory location %ld\n"
	.size	.L.str.62, 30

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"standard input specified twice"
	.size	.L.str.63, 31

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"%[^{ ] { %[^}] }"
	.size	.L.str.64, 17

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"error in command-line option %s"
	.size	.L.str.65, 32

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"unknown command line flag %s"
	.size	.L.str.66, 29

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"unable to initialize collation"
	.size	.L.str.67, 31

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"w"
	.size	.L.str.68, 2

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"cannot open output file %s"
	.size	.L.str.69, 27

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"font"
	.size	.L.str.70, 5

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"hyph"
	.size	.L.str.71, 5

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"maps"
	.size	.L.str.72, 5

	.type	StartSym,@object        # @StartSym
	.comm	StartSym,8,8
	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"\\Start"
	.size	.L.str.73, 7

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"@Galley"
	.size	.L.str.74, 8

	.type	GalleySym,@object       # @GalleySym
	.comm	GalleySym,8,8
	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"@ForceGalley"
	.size	.L.str.75, 13

	.type	ForceGalleySym,@object  # @ForceGalleySym
	.comm	ForceGalleySym,8,8
	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"@LInput"
	.size	.L.str.76, 8

	.type	InputSym,@object        # @InputSym
	.comm	InputSym,8,8
	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"\\Print"
	.size	.L.str.77, 7

	.type	PrintSym,@object        # @PrintSym
	.comm	PrintSym,8,8
	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"@FilterIn"
	.size	.L.str.78, 10

	.type	FilterInSym,@object     # @FilterInSym
	.comm	FilterInSym,8,8
	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"@FilterOut"
	.size	.L.str.79, 11

	.type	FilterOutSym,@object    # @FilterOutSym
	.comm	FilterOutSym,8,8
	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"@FilterErr"
	.size	.L.str.80, 11

	.type	FilterErrSym,@object    # @FilterErrSym
	.comm	FilterErrSym,8,8
	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"@OptGall"
	.size	.L.str.81, 9

	.type	OptGallSym,@object      # @OptGallSym
	.comm	OptGallSym,8,8
	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"@Verbatim"
	.size	.L.str.82, 10

	.type	VerbatimSym,@object     # @VerbatimSym
	.comm	VerbatimSym,8,8
	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"@RawVerbatim"
	.size	.L.str.83, 13

	.type	RawVerbatimSym,@object  # @RawVerbatimSym
	.comm	RawVerbatimSym,8,8
	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"@Begin"
	.size	.L.str.84, 7

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"@End"
	.size	.L.str.85, 5

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"@LEnv"
	.size	.L.str.86, 6

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"@@A"
	.size	.L.str.87, 4

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"@@B"
	.size	.L.str.88, 4

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"@@C"
	.size	.L.str.89, 4

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"@@D"
	.size	.L.str.90, 4

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"@@E"
	.size	.L.str.91, 4

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"@LClos"
	.size	.L.str.92, 7

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"@@V"
	.size	.L.str.93, 4

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"@LUse"
	.size	.L.str.94, 6

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"@LEO"
	.size	.L.str.95, 5

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"{"
	.size	.L.str.96, 2

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"}"
	.size	.L.str.97, 2

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"@Include"
	.size	.L.str.98, 9

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"@SysInclude"
	.size	.L.str.99, 12

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"@PrependGraphic"
	.size	.L.str.100, 16

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"@SysPrependGraphic"
	.size	.L.str.101, 19

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"@Database"
	.size	.L.str.102, 10

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"@SysDatabase"
	.size	.L.str.103, 13

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"@Use"
	.size	.L.str.104, 5

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"@NotRevealed"
	.size	.L.str.105, 13

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"@Case"
	.size	.L.str.106, 6

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"@Yield"
	.size	.L.str.107, 7

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"@BackEnd"
	.size	.L.str.108, 9

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"@Char"
	.size	.L.str.109, 6

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"@Font"
	.size	.L.str.110, 6

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"@Space"
	.size	.L.str.111, 7

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"@YUnit"
	.size	.L.str.112, 7

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"@ZUnit"
	.size	.L.str.113, 7

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"@Break"
	.size	.L.str.114, 7

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"@Underline"
	.size	.L.str.115, 11

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"@SetColour"
	.size	.L.str.116, 11

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"@SetColor"
	.size	.L.str.117, 10

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"@Outline"
	.size	.L.str.118, 9

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"@Language"
	.size	.L.str.119, 10

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"@CurrLang"
	.size	.L.str.120, 10

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"@CurrFamily"
	.size	.L.str.121, 12

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"@CurrFace"
	.size	.L.str.122, 10

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"@CurrYUnit"
	.size	.L.str.123, 11

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"@CurrZUnit"
	.size	.L.str.124, 11

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"@Common"
	.size	.L.str.125, 8

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"@Rump"
	.size	.L.str.126, 6

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"@Meld"
	.size	.L.str.127, 6

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"@Insert"
	.size	.L.str.128, 8

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"@OneOf"
	.size	.L.str.129, 7

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"@Next"
	.size	.L.str.130, 6

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"@Plus"
	.size	.L.str.131, 6

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"@Minus"
	.size	.L.str.132, 7

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"@Open"
	.size	.L.str.133, 6

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"@Tagged"
	.size	.L.str.134, 8

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"@Wide"
	.size	.L.str.135, 6

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"@High"
	.size	.L.str.136, 6

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"@HShift"
	.size	.L.str.137, 8

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"@VShift"
	.size	.L.str.138, 8

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"@BeginHeaderComponent"
	.size	.L.str.139, 22

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"@EndHeaderComponent"
	.size	.L.str.140, 20

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"@SetHeaderComponent"
	.size	.L.str.141, 20

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"@ClearHeaderComponent"
	.size	.L.str.142, 22

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"@OneCol"
	.size	.L.str.143, 8

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"@OneRow"
	.size	.L.str.144, 8

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"@HScale"
	.size	.L.str.145, 8

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"@VScale"
	.size	.L.str.146, 8

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"@HCover"
	.size	.L.str.147, 8

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"@VCover"
	.size	.L.str.148, 8

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"@KernShrink"
	.size	.L.str.149, 12

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"@Scale"
	.size	.L.str.150, 7

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"@HContract"
	.size	.L.str.151, 11

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"@VContract"
	.size	.L.str.152, 11

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"@HLimited"
	.size	.L.str.153, 10

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"@VLimited"
	.size	.L.str.154, 10

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"@HExpand"
	.size	.L.str.155, 9

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"@VExpand"
	.size	.L.str.156, 9

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"@StartHVSpan"
	.size	.L.str.157, 13

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"@StartHSpan"
	.size	.L.str.158, 12

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"@StartVSpan"
	.size	.L.str.159, 12

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"@HSpan"
	.size	.L.str.160, 7

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"@VSpan"
	.size	.L.str.161, 7

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"@PAdjust"
	.size	.L.str.162, 9

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"@HAdjust"
	.size	.L.str.163, 9

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"@VAdjust"
	.size	.L.str.164, 9

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"@Rotate"
	.size	.L.str.165, 8

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"@Background"
	.size	.L.str.166, 12

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"@IncludeGraphic"
	.size	.L.str.167, 16

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"@SysIncludeGraphic"
	.size	.L.str.168, 19

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"@PlainGraphic"
	.size	.L.str.169, 14

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"@Graphic"
	.size	.L.str.170, 9

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"@LinkSource"
	.size	.L.str.171, 12

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"@LinkDest"
	.size	.L.str.172, 10

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"&&"
	.size	.L.str.173, 3

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"&&&"
	.size	.L.str.174, 4

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"@Null"
	.size	.L.str.175, 6

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"@PageLabel"
	.size	.L.str.176, 11

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"//"
	.size	.L.str.177, 3

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"^//"
	.size	.L.str.178, 4

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"^/"
	.size	.L.str.179, 3

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"||"
	.size	.L.str.180, 3

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"^||"
	.size	.L.str.181, 4

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"|"
	.size	.L.str.182, 2

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"^|"
	.size	.L.str.183, 3

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"&"
	.size	.L.str.184, 2

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"^&"
	.size	.L.str.185, 3

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"total of all words printed: %d"
	.size	.L.str.186, 31

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"usage:  lout options files"
	.size	.L.str.187, 27

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"  -s              suppress access to cross reference database"
	.size	.L.str.188, 62

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"  -EPS            EPS (Encapsulated PostScript) output"
	.size	.L.str.189, 55

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"  -PDF or -Z      PDF (Adobe Portable Document Format) output"
	.size	.L.str.190, 62

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"  -p              plain text output instead of PostScript"
	.size	.L.str.191, 58

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"  -P              like -p but with form-feed char between pages"
	.size	.L.str.192, 64

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"  -S              safe execution (disable calls to system(3))"
	.size	.L.str.193, 62

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"  -U              unsafe execution (allow calls to system(3))"
	.size	.L.str.194, 62

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"  -l              ASCII collation order when sorting indexes etc."
	.size	.L.str.195, 66

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"  -L              locale collation order when sorting indexes etc."
	.size	.L.str.196, 67

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"  -o file         output to file instead of stdout"
	.size	.L.str.197, 51

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"  -e file         error messages to file instead of stderr"
	.size	.L.str.198, 59

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"  -a              alternative error format:  file:line:col ..."
	.size	.L.str.199, 63

	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"  -w              print total number of words in output"
	.size	.L.str.200, 56

	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"  -i file         like @SysInclude { file }; not recommended"
	.size	.L.str.201, 61

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"  -I directory    add directory to include file search path"
	.size	.L.str.202, 60

	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"  -C directory    add directory to LCM file search path"
	.size	.L.str.203, 56

	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"  -F directory    add directory to font metrics file search path"
	.size	.L.str.204, 65

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"  -H directory    add directory to hyphenation file search path"
	.size	.L.str.205, 64

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"  -D directory    add directory to database file search path"
	.size	.L.str.206, 61

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"  --option{value} set option e.g. --'@InitialFont{Times Base 10p}'"
	.size	.L.str.207, 67

	.type	.L.str.208,@object      # @.str.208
.L.str.208:
	.asciz	"  -c file         use file.li instead of lout.li for crossrefs"
	.size	.L.str.208, 63

	.type	.L.str.209,@object      # @.str.209
.L.str.209:
	.asciz	"  -M              save memory (don't read in database indexes)"
	.size	.L.str.209, 63

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"  -x              initializing run, not for ordinary use"
	.size	.L.str.210, 57

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"  -u              print this usage message on stderr and exit"
	.size	.L.str.211, 62

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"  -V              print version and configuration information"
	.size	.L.str.212, 62

	.type	.L.str.213,@object      # @.str.213
.L.str.213:
	.asciz	"  -               a file name denoting standard input"
	.size	.L.str.213, 54

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"pa"
	.size	.L.str.214, 3

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	"pb"
	.size	.L.str.215, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
