	.text
	.file	"CabIn.bc"
	.globl	_ZN8NArchive4NCab10CInArchive5Read8Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab10CInArchive5Read8Ev,@function
_ZN8NArchive4NCab10CInArchive5Read8Ev:  # @_ZN8NArchive4NCab10CInArchive5Read8Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jb	.LBB0_3
# BB#1:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB0_4
# BB#2:                                 # %._crit_edge.i
	movq	(%rbx), %rax
.LBB0_3:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movb	(%rax), %al
	popq	%rbx
	retq
.LBB0_4:                                # %_ZN9CInBuffer8ReadByteERh.exit
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end0:
	.size	_ZN8NArchive4NCab10CInArchive5Read8Ev, .Lfunc_end0-_ZN8NArchive4NCab10CInArchive5Read8Ev
	.cfi_endproc

	.globl	_ZN8NArchive4NCab10CInArchive6Read16Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab10CInArchive6Read16Ev,@function
_ZN8NArchive4NCab10CInArchive6Read16Ev: # @_ZN8NArchive4NCab10CInArchive6Read16Ev
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movq	8(%rbx), %rcx
	cmpq	%rcx, %rax
	jb	.LBB1_3
# BB#1:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB1_7
# BB#2:                                 # %._crit_edge.i.i
	movq	(%rbx), %rax
	movq	8(%rbx), %rcx
.LBB1_3:                                # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit
	leaq	1(%rax), %rdx
	movq	%rdx, (%rbx)
	movzbl	(%rax), %ebp
	cmpq	%rcx, %rdx
	jb	.LBB1_6
# BB#4:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB1_7
# BB#5:                                 # %._crit_edge.i.i.1
	movq	(%rbx), %rdx
.LBB1_6:                                # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.1
	leaq	1(%rdx), %rax
	movq	%rax, (%rbx)
	movzbl	(%rdx), %ecx
	shll	$8, %ecx
	movzwl	%bp, %eax
	orl	%ecx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB1_7:                                # %_ZN9CInBuffer8ReadByteERh.exit.i
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end1:
	.size	_ZN8NArchive4NCab10CInArchive6Read16Ev, .Lfunc_end1-_ZN8NArchive4NCab10CInArchive6Read16Ev
	.cfi_endproc

	.globl	_ZN8NArchive4NCab10CInArchive6Read32Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab10CInArchive6Read32Ev,@function
_ZN8NArchive4NCab10CInArchive6Read32Ev: # @_ZN8NArchive4NCab10CInArchive6Read32Ev
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 48
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rcx
	movq	8(%rbx), %rax
	cmpq	%rax, %rcx
	jb	.LBB2_3
# BB#1:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB2_13
# BB#2:                                 # %._crit_edge.i.i
	movq	(%rbx), %rcx
	movq	8(%rbx), %rax
.LBB2_3:                                # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit
	leaq	1(%rcx), %rdx
	movq	%rdx, (%rbx)
	movzbl	(%rcx), %r14d
	cmpq	%rax, %rdx
	jb	.LBB2_6
# BB#4:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB2_13
# BB#5:                                 # %._crit_edge.i.i.1
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
.LBB2_6:                                # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.1
	leaq	1(%rdx), %rsi
	movq	%rsi, (%rbx)
	movzbl	(%rdx), %r15d
	cmpq	%rax, %rsi
	jb	.LBB2_9
# BB#7:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB2_13
# BB#8:                                 # %._crit_edge.i.i.2
	movq	(%rbx), %rsi
	movq	8(%rbx), %rax
.LBB2_9:                                # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.2
	leaq	1(%rsi), %rcx
	movq	%rcx, (%rbx)
	movzbl	(%rsi), %ebp
	cmpq	%rax, %rcx
	jb	.LBB2_12
# BB#10:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB2_13
# BB#11:                                # %._crit_edge.i.i.3
	movq	(%rbx), %rcx
.LBB2_12:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.3
	shll	$8, %r15d
	orl	%r14d, %r15d
	shll	$16, %ebp
	orl	%r15d, %ebp
	leaq	1(%rcx), %rax
	movq	%rax, (%rbx)
	movzbl	(%rcx), %eax
	shll	$24, %eax
	orl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_13:                               # %_ZN9CInBuffer8ReadByteERh.exit.i
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end2:
	.size	_ZN8NArchive4NCab10CInArchive6Read32Ev, .Lfunc_end2-_ZN8NArchive4NCab10CInArchive6Read32Ev
	.cfi_endproc

	.globl	_ZN8NArchive4NCab10CInArchive12SafeReadNameEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab10CInArchive12SafeReadNameEv,@function
_ZN8NArchive4NCab10CInArchive12SafeReadNameEv: # @_ZN8NArchive4NCab10CInArchive12SafeReadNameEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, (%r14)
	movb	$0, (%rax)
	movl	$4, 12(%r14)
	.p2align	4, 0x90
.LBB3_1:                                # %.thread
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jb	.LBB3_5
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp1:
# BB#3:                                 # %.noexc
                                        #   in Loop: Header=BB3_1 Depth=1
	testb	%al, %al
	je	.LBB3_7
# BB#4:                                 # %._crit_edge.i.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movq	(%rbx), %rax
.LBB3_5:                                #   in Loop: Header=BB3_1 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.LBB3_11
# BB#6:                                 #   in Loop: Header=BB3_1 Depth=1
.Ltmp6:
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZN11CStringBaseIcEpLEc
.Ltmp7:
	jmp	.LBB3_1
.LBB3_11:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_7:                                # %_ZN9CInBuffer8ReadByteERh.exit.i
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
.Ltmp3:
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp4:
# BB#8:                                 # %.noexc10
.LBB3_9:                                # %.loopexit.split-lp
.Ltmp5:
	jmp	.LBB3_13
.LBB3_12:                               # %.loopexit
.Ltmp2:
	jmp	.LBB3_13
.LBB3_10:
.Ltmp8:
.LBB3_13:
	movq	%rax, %rbx
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_15
# BB#14:
	callq	_ZdaPv
.LBB3_15:                               # %_ZN11CStringBaseIcED2Ev.exit11
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN8NArchive4NCab10CInArchive12SafeReadNameEv, .Lfunc_end3-_ZN8NArchive4NCab10CInArchive12SafeReadNameEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp3-.Ltmp7           #   Call between .Ltmp7 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Lfunc_end3-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIcEpLEc,"axG",@progbits,_ZN11CStringBaseIcEpLEc,comdat
	.weak	_ZN11CStringBaseIcEpLEc
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIcEpLEc,@function
_ZN11CStringBaseIcEpLEc:                # @_ZN11CStringBaseIcEpLEc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 64
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r13
	movl	8(%r13), %ebp
	movl	12(%r13), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	$1, %eax
	jg	.LBB4_28
# BB#1:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB4_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB4_3:                                # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rsi,%rbx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB4_28
# BB#4:
	addl	%ebx, %esi
	movslq	%r15d, %rax
	cmpl	$-1, %esi
	movq	$-1, %rdi
	cmovgeq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB4_27
# BB#5:                                 # %.preheader.i.i
	movq	(%r13), %rdi
	testl	%ebp, %ebp
	jle	.LBB4_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	movslq	%ebp, %rax
	cmpl	$31, %ebp
	jbe	.LBB4_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB4_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r12
	jae	.LBB4_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB4_17
.LBB4_7:
	xorl	%ecx, %ecx
.LBB4_8:                                # %.lr.ph.i.i.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB4_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r12,%rcx)
	incq	%rcx
	incq	%rbp
	jne	.LBB4_10
.LBB4_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB4_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rax
	leaq	7(%r12,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB4_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB4_13
	jmp	.LBB4_26
.LBB4_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB4_27
.LBB4_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movl	8(%r13), %ebp
.LBB4_27:
	movq	%r12, (%r13)
	movslq	%ebp, %rax
	movb	$0, (%r12,%rax)
	movl	%r15d, 12(%r13)
.LBB4_28:                               # %_ZN11CStringBaseIcE10GrowLengthEi.exit
	movq	(%r13), %rax
	movslq	%ebp, %rcx
	movb	%r14b, (%rax,%rcx)
	movq	(%r13), %rax
	movslq	8(%r13), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%r13)
	movb	$0, 1(%rax,%rcx)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB4_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r12,%rbx)
	movups	%xmm1, 16(%r12,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB4_20
	jmp	.LBB4_21
.LBB4_18:
	xorl	%ebx, %ebx
.LBB4_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB4_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
	.p2align	4, 0x90
.LBB4_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB4_23
.LBB4_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB4_8
	jmp	.LBB4_26
.Lfunc_end4:
	.size	_ZN11CStringBaseIcEpLEc, .Lfunc_end4-_ZN11CStringBaseIcEpLEc
	.cfi_endproc

	.text
	.globl	_ZN8NArchive4NCab10CInArchive16ReadOtherArchiveERNS0_13COtherArchiveE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab10CInArchive16ReadOtherArchiveERNS0_13COtherArchiveE,@function
_ZN8NArchive4NCab10CInArchive16ReadOtherArchiveERNS0_13COtherArchiveE: # @_ZN8NArchive4NCab10CInArchive16ReadOtherArchiveERNS0_13COtherArchiveE
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 80
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r15
	leaq	8(%rsp), %r14
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NCab10CInArchive12SafeReadNameEv
	cmpq	%r12, %r14
	je	.LBB5_31
# BB#1:
	movl	$0, 8(%r12)
	movq	(%r12), %rax
	movb	$0, (%rax)
	movslq	16(%rsp), %rax
	leaq	1(%rax), %r13
	movl	12(%r12), %ebp
	cmpl	%ebp, %r13d
	jne	.LBB5_3
# BB#2:                                 # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	(%r12), %rbx
	jmp	.LBB5_28
.LBB5_3:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r13, %rdi
	cmovlq	%rax, %rdi
.Ltmp9:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp10:
# BB#4:                                 # %.noexc
	testl	%ebp, %ebp
	jle	.LBB5_27
# BB#5:                                 # %.preheader.i.i
	movslq	8(%r12), %rbp
	testq	%rbp, %rbp
	movq	(%r12), %rdi
	jle	.LBB5_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	cmpl	$31, %ebp
	jbe	.LBB5_7
# BB#14:                                # %min.iters.checked
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB5_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB5_17
# BB#16:                                # %vector.memcheck
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_17
.LBB5_7:
	xorl	%ecx, %ecx
.LBB5_8:                                # %.lr.ph.i.i.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB5_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB5_10
.LBB5_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB5_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB5_13
	jmp	.LBB5_26
.LBB5_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB5_27
.LBB5_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB5_27:                               # %._crit_edge17.i.i
	movq	%rbx, (%r12)
	movslq	8(%r12), %rax
	movb	$0, (%rbx,%rax)
	movl	%r13d, 12(%r12)
.LBB5_28:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	8(%rsp), %rax
	.p2align	4, 0x90
.LBB5_29:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB5_29
# BB#30:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
	movl	16(%rsp), %eax
	movl	%eax, 8(%r12)
.LBB5_31:                               # %_ZN11CStringBaseIcEaSERKS0_.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_33
# BB#32:
	callq	_ZdaPv
.LBB5_33:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN8NArchive4NCab10CInArchive12SafeReadNameEv
	leaq	16(%r12), %rax
	cmpq	%rax, %r14
	je	.LBB5_64
# BB#34:
	movl	$0, 24(%r12)
	movq	16(%r12), %rcx
	movb	$0, (%rcx)
	movslq	16(%rsp), %rcx
	leaq	1(%rcx), %r14
	movl	28(%r12), %ebp
	cmpl	%ebp, %r14d
	jne	.LBB5_36
# BB#35:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i6
	movq	(%rax), %rbx
	jmp	.LBB5_61
.LBB5_36:
	cmpl	$-1, %ecx
	movq	$-1, %rax
	movq	%r14, %rdi
	cmovlq	%rax, %rdi
.Ltmp12:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp13:
# BB#37:                                # %.noexc20
	testl	%ebp, %ebp
	jle	.LBB5_60
# BB#38:                                # %.preheader.i.i7
	movslq	24(%r12), %rbp
	testq	%rbp, %rbp
	movq	16(%r12), %rdi
	jle	.LBB5_58
# BB#39:                                # %.lr.ph.preheader.i.i8
	cmpl	$31, %ebp
	jbe	.LBB5_40
# BB#47:                                # %min.iters.checked34
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB5_40
# BB#48:                                # %vector.memcheck45
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB5_50
# BB#49:                                # %vector.memcheck45
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB5_50
.LBB5_40:
	xorl	%ecx, %ecx
.LBB5_41:                               # %.lr.ph.i.i13.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB5_44
# BB#42:                                # %.lr.ph.i.i13.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB5_43:                               # %.lr.ph.i.i13.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB5_43
.LBB5_44:                               # %.lr.ph.i.i13.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB5_59
# BB#45:                                # %.lr.ph.i.i13.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB5_46:                               # %.lr.ph.i.i13
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB5_46
	jmp	.LBB5_59
.LBB5_58:                               # %._crit_edge.i.i9
	testq	%rdi, %rdi
	je	.LBB5_60
.LBB5_59:                               # %._crit_edge.thread.i.i14
	callq	_ZdaPv
.LBB5_60:                               # %._crit_edge17.i.i15
	movq	%rbx, 16(%r12)
	movslq	24(%r12), %rax
	movb	$0, (%rbx,%rax)
	movl	%r14d, 28(%r12)
.LBB5_61:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i16
	movq	8(%rsp), %rax
	.p2align	4, 0x90
.LBB5_62:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB5_62
# BB#63:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i19
	movl	16(%rsp), %eax
	movl	%eax, 24(%r12)
.LBB5_64:                               # %_ZN11CStringBaseIcEaSERKS0_.exit21
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_66
# BB#65:
	callq	_ZdaPv
.LBB5_66:                               # %_ZN11CStringBaseIcED2Ev.exit22
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB5_20
	jmp	.LBB5_21
.LBB5_50:                               # %vector.body30.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB5_51
# BB#52:                                # %vector.body30.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_53:                               # %vector.body30.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB5_53
	jmp	.LBB5_54
.LBB5_18:
	xorl	%edx, %edx
.LBB5_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB5_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB5_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB5_23
.LBB5_24:                               # %middle.block
	cmpq	%rcx, %rbp
	jne	.LBB5_8
	jmp	.LBB5_26
.LBB5_51:
	xorl	%edx, %edx
.LBB5_54:                               # %vector.body30.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB5_57
# BB#55:                                # %vector.body30.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB5_56:                               # %vector.body30
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB5_56
.LBB5_57:                               # %middle.block31
	cmpq	%rcx, %rbp
	jne	.LBB5_41
	jmp	.LBB5_59
.LBB5_68:
.Ltmp14:
	jmp	.LBB5_69
.LBB5_67:
.Ltmp11:
.LBB5_69:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_71
# BB#70:
	callq	_ZdaPv
.LBB5_71:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN8NArchive4NCab10CInArchive16ReadOtherArchiveERNS0_13COtherArchiveE, .Lfunc_end5-_ZN8NArchive4NCab10CInArchive16ReadOtherArchiveERNS0_13COtherArchiveE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp9-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp10         #   Call between .Ltmp10 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end5-.Ltmp13     #   Call between .Ltmp13 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive4NCab10CInArchive4SkipEj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab10CInArchive4SkipEj,@function
_ZN8NArchive4NCab10CInArchive4SkipEj:   # @_ZN8NArchive4NCab10CInArchive4SkipEj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -24
.Lcfi51:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	testl	%ebx, %ebx
	je	.LBB6_6
# BB#1:                                 # %.lr.ph
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	8(%r14), %rax
	jb	.LBB6_5
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB6_7
# BB#4:                                 # %._crit_edge.i.i
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	(%r14), %rax
.LBB6_5:                                # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit
                                        #   in Loop: Header=BB6_2 Depth=1
	decl	%ebx
	incq	%rax
	movq	%rax, (%r14)
	testl	%ebx, %ebx
	jne	.LBB6_2
.LBB6_6:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_7:                                # %_ZN9CInBuffer8ReadByteERh.exit.i
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end6:
	.size	_ZN8NArchive4NCab10CInArchive4SkipEj, .Lfunc_end6-_ZN8NArchive4NCab10CInArchive4SkipEj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NCab10CInArchive4OpenEPKyRNS0_11CDatabaseExE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab10CInArchive4OpenEPKyRNS0_11CDatabaseExE,@function
_ZN8NArchive4NCab10CInArchive4OpenEPKyRNS0_11CDatabaseExE: # @_ZN8NArchive4NCab10CInArchive4OpenEPKyRNS0_11CDatabaseExE
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 144
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	160(%rbx), %r12
	movw	$0, 20(%rbx)
	movw	$0, 22(%rbx)
	leaq	96(%rbx), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%rbx, %r15
	subq	$-128, %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	(%r12), %rax
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB7_88
# BB#1:
	movl	$_ZN8NArchive4NCab7NHeader7kMarkerE, %esi
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	movq	%rbx, %r8
	callq	_Z21FindSignatureInStreamP19ISequentialInStreamPKhjPKyRy
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB7_88
# BB#2:
	movq	(%r12), %rax
	movq	(%rbx), %rsi
	addq	$8, %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB7_88
# BB#3:
	movl	$131072, %esi           # imm = 0x20000
	movq	%r14, %rdi
	callq	_ZN9CInBuffer6CreateEj
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB7_88
# BB#4:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	movq	%r14, %rdi
	callq	_ZN9CInBuffer4InitEv
	movq	%r14, %rdi
	callq	_ZN8NArchive4NCab10CInArchive6Read32Ev
	movl	%eax, 88(%rbx)
	movq	%r14, %rdi
	callq	_ZN8NArchive4NCab10CInArchive6Read32Ev
	movl	$1, %ebp
	testl	%eax, %eax
	jne	.LBB7_88
# BB#5:
	movq	%r14, %rdi
	callq	_ZN8NArchive4NCab10CInArchive6Read32Ev
	movl	%eax, 92(%rbx)
	movq	%r14, %rdi
	callq	_ZN8NArchive4NCab10CInArchive6Read32Ev
	testl	%eax, %eax
	jne	.LBB7_88
# BB#6:
	movq	(%r14), %rdx
	movq	8(%r14), %rax
	cmpq	%rax, %rdx
	jb	.LBB7_9
# BB#7:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#8:                                 # %._crit_edge.i.i
	movq	(%r14), %rdx
	movq	8(%r14), %rax
.LBB7_9:                                # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movb	(%rdx), %dl
	movb	%dl, 8(%rbx)
	cmpq	%rax, %rcx
	jb	.LBB7_12
# BB#10:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#11:                                # %._crit_edge.i.i113
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_12:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit115
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movb	(%rcx), %cl
	movb	%cl, 9(%rbx)
	cmpq	%rax, %rdx
	jb	.LBB7_15
# BB#13:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#14:                                # %._crit_edge.i.i.i
	movq	(%r14), %rdx
	movq	8(%r14), %rax
.LBB7_15:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i
	leaq	1(%rdx), %rsi
	movq	%rsi, (%r14)
	movzbl	(%rdx), %r13d
	cmpq	%rax, %rsi
	jb	.LBB7_18
# BB#16:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#17:                                # %._crit_edge.i.i.1.i
	movq	(%r14), %rsi
	movq	8(%r14), %rax
.LBB7_18:                               # %_ZN8NArchive4NCab10CInArchive6Read16Ev.exit
	leaq	1(%rsi), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rsi), %edx
	shll	$8, %edx
	movzwl	%r13w, %esi
	orl	%edx, %esi
	movw	%si, 10(%rbx)
	cmpq	%rax, %rcx
	jb	.LBB7_21
# BB#19:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#20:                                # %._crit_edge.i.i.i118
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_21:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i120
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %r13d
	cmpq	%rax, %rdx
	jb	.LBB7_24
# BB#22:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#23:                                # %._crit_edge.i.i.1.i122
	movq	(%r14), %rdx
	movq	8(%r14), %rax
.LBB7_24:                               # %_ZN8NArchive4NCab10CInArchive6Read16Ev.exit123
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rdx), %edx
	shll	$8, %edx
	movzwl	%r13w, %esi
	orl	%edx, %esi
	movw	%si, 12(%rbx)
	cmpq	%rax, %rcx
	jb	.LBB7_27
# BB#25:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#26:                                # %._crit_edge.i.i.i126
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_27:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i128
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %r13d
	cmpq	%rax, %rdx
	jb	.LBB7_30
# BB#28:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#29:                                # %._crit_edge.i.i.1.i130
	movq	(%r14), %rdx
.LBB7_30:                               # %_ZN8NArchive4NCab10CInArchive6Read16Ev.exit131
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rdx), %eax
	shll	$8, %eax
	movzwl	%r13w, %edx
	orl	%eax, %edx
	movw	%dx, 14(%rbx)
	movzwl	%dx, %eax
	cmpl	$7, %eax
	ja	.LBB7_88
# BB#31:
	movq	8(%r14), %rax
	cmpq	%rax, %rcx
	jb	.LBB7_34
# BB#32:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#33:                                # %._crit_edge.i.i.i134
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_34:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i136
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %ebp
	cmpq	%rax, %rdx
	jb	.LBB7_37
# BB#35:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#36:                                # %._crit_edge.i.i.1.i138
	movq	(%r14), %rdx
	movq	8(%r14), %rax
.LBB7_37:                               # %_ZN8NArchive4NCab10CInArchive6Read16Ev.exit139
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rdx), %edx
	shll	$8, %edx
	movzwl	%bp, %esi
	orl	%edx, %esi
	movw	%si, 16(%rbx)
	cmpq	%rax, %rcx
	jb	.LBB7_40
# BB#38:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#39:                                # %._crit_edge.i.i.i142
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_40:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i144
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %ebp
	cmpq	%rax, %rdx
	jb	.LBB7_43
# BB#41:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#42:                                # %._crit_edge.i.i.1.i146
	movq	(%r14), %rdx
.LBB7_43:                               # %_ZN8NArchive4NCab10CInArchive6Read16Ev.exit147
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rdx), %eax
	shll	$8, %eax
	movzwl	%bp, %edx
	orl	%eax, %edx
	movw	%dx, 18(%rbx)
	testb	$4, 14(%rbx)
	je	.LBB7_62
# BB#44:
	movq	8(%r14), %rax
	cmpq	%rax, %rcx
	jb	.LBB7_47
# BB#45:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#46:                                # %._crit_edge.i.i.i150
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_47:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i152
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %ebp
	cmpq	%rax, %rdx
	jb	.LBB7_50
# BB#48:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#49:                                # %._crit_edge.i.i.1.i154
	movq	(%r14), %rdx
	movq	8(%r14), %rax
.LBB7_50:                               # %_ZN8NArchive4NCab10CInArchive6Read16Ev.exit155
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rdx), %edx
	shll	$8, %edx
	movzwl	%bp, %esi
	orl	%edx, %esi
	movw	%si, 20(%rbx)
	cmpq	%rax, %rcx
	jb	.LBB7_53
# BB#51:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#52:                                # %._crit_edge.i.i157
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_53:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit159
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movb	(%rcx), %cl
	movb	%cl, 22(%rbx)
	cmpq	%rax, %rdx
	jb	.LBB7_56
# BB#54:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#55:                                # %._crit_edge.i.i161
	movq	(%r14), %rdx
.LBB7_56:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit163
	leaq	1(%rdx), %rax
	movq	%rax, (%r14)
	movb	(%rdx), %cl
	movb	%cl, 23(%rbx)
	movzwl	20(%rbx), %ebp
	testl	%ebp, %ebp
	je	.LBB7_62
# BB#57:                                # %.lr.ph.i
	negl	%ebp
.LBB7_58:                               # =>This Inner Loop Header: Depth=1
	cmpq	8(%r14), %rax
	jb	.LBB7_61
# BB#59:                                #   in Loop: Header=BB7_58 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#60:                                # %._crit_edge.i.i.i166
                                        #   in Loop: Header=BB7_58 Depth=1
	movq	(%r14), %rax
.LBB7_61:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i168
                                        #   in Loop: Header=BB7_58 Depth=1
	incq	%rax
	movq	%rax, (%r14)
	incl	%ebp
	jne	.LBB7_58
.LBB7_62:                               # %_ZN8NArchive4NCab10CInArchive4SkipEj.exit
	movzwl	14(%rbx), %eax
	testb	$1, %al
	je	.LBB7_64
# BB#63:
	leaq	24(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN8NArchive4NCab10CInArchive16ReadOtherArchiveERNS0_13COtherArchiveE
	movw	14(%rbx), %ax
.LBB7_64:
	testb	$2, %al
	je	.LBB7_66
# BB#65:
	leaq	56(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN8NArchive4NCab10CInArchive16ReadOtherArchiveERNS0_13COtherArchiveE
.LBB7_66:                               # %.preheader
	cmpw	$0, 10(%rbx)
	je	.LBB7_87
# BB#67:                                # %.lr.ph308
	xorl	%ebp, %ebp
.LBB7_68:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_82 Depth 2
	movq	%r14, %rdi
	callq	_ZN8NArchive4NCab10CInArchive6Read32Ev
	movl	%eax, %r13d
	movq	(%r14), %rcx
	movq	8(%r14), %rax
	cmpq	%rax, %rcx
	jb	.LBB7_71
# BB#69:                                #   in Loop: Header=BB7_68 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#70:                                # %._crit_edge.i.i.i171
                                        #   in Loop: Header=BB7_68 Depth=1
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_71:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i173
                                        #   in Loop: Header=BB7_68 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmpq	%rax, %rdx
	jb	.LBB7_74
# BB#72:                                #   in Loop: Header=BB7_68 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#73:                                # %._crit_edge.i.i.1.i175
                                        #   in Loop: Header=BB7_68 Depth=1
	movq	(%r14), %rdx
	movq	8(%r14), %rax
.LBB7_74:                               # %_ZN8NArchive4NCab10CInArchive6Read16Ev.exit176
                                        #   in Loop: Header=BB7_68 Depth=1
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rdx), %r13d
	cmpq	%rax, %rcx
	jb	.LBB7_77
# BB#75:                                #   in Loop: Header=BB7_68 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#76:                                # %._crit_edge.i.i178
                                        #   in Loop: Header=BB7_68 Depth=1
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_77:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit180
                                        #   in Loop: Header=BB7_68 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	cmpq	%rax, %rdx
	jb	.LBB7_80
# BB#78:                                #   in Loop: Header=BB7_68 Depth=1
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#79:                                # %._crit_edge.i.i182
                                        #   in Loop: Header=BB7_68 Depth=1
	movq	(%r14), %rdx
.LBB7_80:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit184
                                        #   in Loop: Header=BB7_68 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	leaq	1(%rdx), %rax
	movq	%rax, (%r14)
	movzbl	(%rdx), %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movzbl	22(%rbx), %ebp
	testl	%ebp, %ebp
	je	.LBB7_86
# BB#81:                                # %.lr.ph.i186.preheader
                                        #   in Loop: Header=BB7_68 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB7_82:                               # %.lr.ph.i186
                                        #   Parent Loop BB7_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	8(%r14), %rax
	jb	.LBB7_85
# BB#83:                                #   in Loop: Header=BB7_82 Depth=2
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB7_143
# BB#84:                                # %._crit_edge.i.i.i189
                                        #   in Loop: Header=BB7_82 Depth=2
	movq	(%r14), %rax
.LBB7_85:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i191
                                        #   in Loop: Header=BB7_82 Depth=2
	incq	%rax
	movq	%rax, (%r14)
	incl	%ebp
	jne	.LBB7_82
.LBB7_86:                               # %_ZN8NArchive4NCab10CInArchive4SkipEj.exit192
                                        #   in Loop: Header=BB7_68 Depth=1
	shlq	$8, %r13
	addq	16(%rsp), %r13          # 8-byte Folded Reload
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	shlq	$56, %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	shlq	$48, %rcx
	shlq	$32, %r13
	movl	8(%rsp), %eax           # 4-byte Reload
	orq	%r13, %rax
	orq	%rcx, %rax
	orq	%rdx, %rax
	movq	%rax, (%rbp)
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	112(%rbx), %rax
	movslq	108(%rbx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 108(%rbx)
	movl	12(%rsp), %ebp          # 4-byte Reload
	incl	%ebp
	movzwl	10(%rbx), %eax
	cmpl	%eax, %ebp
	jl	.LBB7_68
.LBB7_87:                               # %._crit_edge
	movq	(%r12), %rax
	movl	92(%rbx), %esi
	addq	(%rbx), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB7_89
.LBB7_88:                               # %.loopexit
	movl	%ebp, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_89:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	movq	%r14, %rdi
	callq	_ZN9CInBuffer4InitEv
	cmpw	$0, 12(%rbx)
	movl	$0, %ebp
	je	.LBB7_88
# BB#90:                                # %.lr.ph
	xorl	%ecx, %ecx
.LBB7_91:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_130 Depth 2
                                        #     Child Loop BB7_139 Depth 2
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r12
	movb	$0, (%r12)
.Ltmp15:
	movq	%r14, %rdi
	callq	_ZN8NArchive4NCab10CInArchive6Read32Ev
	movl	%eax, %r13d
.Ltmp16:
# BB#92:                                #   in Loop: Header=BB7_91 Depth=1
.Ltmp17:
	movq	%r14, %rdi
	callq	_ZN8NArchive4NCab10CInArchive6Read32Ev
	movl	%eax, %ebp
.Ltmp18:
# BB#93:                                #   in Loop: Header=BB7_91 Depth=1
	movq	(%r14), %rcx
	movq	8(%r14), %rax
	cmpq	%rax, %rcx
	jb	.LBB7_97
# BB#94:                                #   in Loop: Header=BB7_91 Depth=1
.Ltmp19:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp20:
# BB#95:                                # %.noexc
                                        #   in Loop: Header=BB7_91 Depth=1
	testb	%al, %al
	je	.LBB7_148
# BB#96:                                # %._crit_edge.i.i.i195
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_97:                               # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i197
                                        #   in Loop: Header=BB7_91 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	cmpq	%rax, %rdx
	jb	.LBB7_101
# BB#98:                                #   in Loop: Header=BB7_91 Depth=1
.Ltmp21:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp22:
# BB#99:                                # %.noexc201
                                        #   in Loop: Header=BB7_91 Depth=1
	testb	%al, %al
	je	.LBB7_148
# BB#100:                               # %._crit_edge.i.i.1.i199
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	(%r14), %rdx
	movq	8(%r14), %rax
.LBB7_101:                              #   in Loop: Header=BB7_91 Depth=1
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rdx), %edx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	cmpq	%rax, %rcx
	jb	.LBB7_105
# BB#102:                               #   in Loop: Header=BB7_91 Depth=1
.Ltmp25:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp26:
# BB#103:                               # %.noexc210
                                        #   in Loop: Header=BB7_91 Depth=1
	testb	%al, %al
	je	.LBB7_144
# BB#104:                               # %._crit_edge.i.i.i205
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_105:                              # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i207
                                        #   in Loop: Header=BB7_91 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %ecx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	cmpq	%rax, %rdx
	jb	.LBB7_109
# BB#106:                               #   in Loop: Header=BB7_91 Depth=1
.Ltmp27:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp28:
# BB#107:                               # %.noexc212
                                        #   in Loop: Header=BB7_91 Depth=1
	testb	%al, %al
	je	.LBB7_144
# BB#108:                               # %._crit_edge.i.i.1.i209
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	(%r14), %rdx
	movq	8(%r14), %rax
.LBB7_109:                              #   in Loop: Header=BB7_91 Depth=1
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rdx), %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	cmpq	%rax, %rcx
	jb	.LBB7_113
# BB#110:                               #   in Loop: Header=BB7_91 Depth=1
.Ltmp33:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp34:
# BB#111:                               # %.noexc221
                                        #   in Loop: Header=BB7_91 Depth=1
	testb	%al, %al
	je	.LBB7_146
# BB#112:                               # %._crit_edge.i.i.i216
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_113:                              # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i218
                                        #   in Loop: Header=BB7_91 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %ecx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	cmpq	%rax, %rdx
	jb	.LBB7_117
# BB#114:                               #   in Loop: Header=BB7_91 Depth=1
.Ltmp35:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp36:
# BB#115:                               # %.noexc223
                                        #   in Loop: Header=BB7_91 Depth=1
	testb	%al, %al
	je	.LBB7_146
# BB#116:                               # %._crit_edge.i.i.1.i220
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	(%r14), %rdx
	movq	8(%r14), %rax
.LBB7_117:                              #   in Loop: Header=BB7_91 Depth=1
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movzbl	(%rdx), %edx
	movl	%edx, 40(%rsp)          # 4-byte Spill
	cmpq	%rax, %rcx
	jb	.LBB7_121
# BB#118:                               #   in Loop: Header=BB7_91 Depth=1
.Ltmp41:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp42:
# BB#119:                               # %.noexc232
                                        #   in Loop: Header=BB7_91 Depth=1
	testb	%al, %al
	je	.LBB7_150
# BB#120:                               # %._crit_edge.i.i.i227
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	(%r14), %rcx
	movq	8(%r14), %rax
.LBB7_121:                              # %_ZN8NArchive4NCab10CInArchive5Read8Ev.exit.i229
                                        #   in Loop: Header=BB7_91 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r14)
	movzbl	(%rcx), %ecx
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	cmpq	%rax, %rdx
	jb	.LBB7_125
# BB#122:                               #   in Loop: Header=BB7_91 Depth=1
.Ltmp43:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp44:
# BB#123:                               # %.noexc234
                                        #   in Loop: Header=BB7_91 Depth=1
	testb	%al, %al
	je	.LBB7_150
# BB#124:                               # %._crit_edge.i.i.1.i231
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	(%r14), %rdx
.LBB7_125:                              #   in Loop: Header=BB7_91 Depth=1
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	movl	%r13d, 56(%rsp)         # 4-byte Spill
	leaq	1(%rdx), %rax
	movq	%rax, (%r14)
	movzbl	(%rdx), %r13d
.Ltmp49:
	leaq	64(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NArchive4NCab10CInArchive12SafeReadNameEv
.Ltmp50:
# BB#126:                               #   in Loop: Header=BB7_91 Depth=1
	movb	$0, (%r12)
	movslq	72(%rsp), %rax
	leaq	1(%rax), %rdi
	cmpl	$4, %edi
	je	.LBB7_129
# BB#127:                               #   in Loop: Header=BB7_91 Depth=1
	cmpl	$-1, %eax
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp52:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp53:
# BB#128:                               # %._crit_edge17.i.i
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	%r12, %rdi
	callq	_ZdaPv
	movb	$0, (%rbp)
	movq	%rbp, %r12
.LBB7_129:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB7_91 Depth=1
	movl	24(%rsp), %ebp          # 4-byte Reload
	movl	8(%rsp), %eax           # 4-byte Reload
	shll	$8, %eax
	movzwl	16(%rsp), %esi          # 2-byte Folded Reload
	orl	%eax, %esi
	movl	12(%rsp), %eax          # 4-byte Reload
	shll	$8, %eax
	addl	32(%rsp), %eax          # 4-byte Folded Reload
	movl	40(%rsp), %ecx          # 4-byte Reload
	shll	$8, %ecx
	shll	$16, %eax
	orl	%eax, %ebp
	orl	%ecx, %ebp
	shll	$8, %r13d
	movzwl	60(%rsp), %eax          # 2-byte Folded Reload
	orl	%r13d, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	64(%rsp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_130:                              #   Parent Loop BB7_91 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax,%rcx), %edx
	movb	%dl, (%r12,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB7_130
# BB#131:                               #   in Loop: Header=BB7_91 Depth=1
	movl	72(%rsp), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_133
# BB#132:                               #   in Loop: Header=BB7_91 Depth=1
	movl	%esi, %r13d
	callq	_ZdaPv
	movl	%r13d, %esi
.LBB7_133:                              # %_ZN11CStringBaseIcED2Ev.exit
                                        #   in Loop: Header=BB7_91 Depth=1
	movl	108(%rbx), %eax
	movl	%esi, %edx
	andl	$65533, %edx            # imm = 0xFFFD
	xorl	%ecx, %ecx
	cmpl	$65533, %edx            # imm = 0xFFFD
	je	.LBB7_135
# BB#134:                               #   in Loop: Header=BB7_91 Depth=1
	movzwl	%si, %ecx
	cmpl	$65533, %ecx            # imm = 0xFFFD
	ja	.LBB7_136
.LBB7_135:                              # %_ZNK8NArchive4NCab5CItem14GetFolderIndexEi.exit
                                        #   in Loop: Header=BB7_91 Depth=1
	cmpl	%eax, %ecx
	jge	.LBB7_142
.LBB7_136:                              # %_ZNK8NArchive4NCab5CItem14GetFolderIndexEi.exit.thread
                                        #   in Loop: Header=BB7_91 Depth=1
.Ltmp55:
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movl	%ebp, 24(%rsp)          # 4-byte Spill
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp56:
# BB#137:                               # %.noexc240
                                        #   in Loop: Header=BB7_91 Depth=1
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	leaq	1(%rax), %r13
	cmpl	$-1, %eax
	movq	%r13, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp57:
	callq	_Znam
.Ltmp58:
# BB#138:                               # %.noexc.i
                                        #   in Loop: Header=BB7_91 Depth=1
	movq	%rax, (%rbp)
	movb	$0, (%rax)
	movl	%r13d, 12(%rbp)
	xorl	%ecx, %ecx
	movl	32(%rsp), %esi          # 4-byte Reload
	.p2align	4, 0x90
.LBB7_139:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i.i
                                        #   Parent Loop BB7_91 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12,%rcx), %edx
	movb	%dl, (%rax,%rcx)
	incq	%rcx
	testb	%dl, %dl
	jne	.LBB7_139
# BB#140:                               #   in Loop: Header=BB7_91 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%rbp)
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, 16(%rbp)
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, 20(%rbp)
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, 24(%rbp)
	movw	%si, 28(%rbp)
	movl	8(%rsp), %eax           # 4-byte Reload
	movw	%ax, 32(%rbp)
.Ltmp60:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp61:
# BB#141:                               #   in Loop: Header=BB7_91 Depth=1
	movq	144(%rbx), %rax
	movslq	140(%rbx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 140(%rbx)
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	80(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movzwl	12(%rbx), %eax
	xorl	%ebp, %ebp
	cmpl	%eax, %ecx
	jl	.LBB7_91
	jmp	.LBB7_88
.LBB7_142:                              # %_ZN8NArchive4NCab5CItemD2Ev.exit
	movq	%r12, %rdi
	callq	_ZdaPv
	movl	$1, %ebp
	jmp	.LBB7_88
.LBB7_143:                              # %_ZN9CInBuffer8ReadByteERh.exit.i
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB7_144:                              # %_ZN9CInBuffer8ReadByteERh.exit.i.i206
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
.Ltmp30:
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp31:
# BB#145:                               # %.noexc211
.LBB7_146:                              # %_ZN9CInBuffer8ReadByteERh.exit.i.i217
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
.Ltmp38:
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp39:
# BB#147:                               # %.noexc222
.LBB7_148:                              # %_ZN9CInBuffer8ReadByteERh.exit.i.i196
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
.Ltmp23:
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp24:
# BB#149:                               # %.noexc200
.LBB7_150:                              # %_ZN9CInBuffer8ReadByteERh.exit.i.i228
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$2, (%rax)
.Ltmp46:
	movl	$_ZTIN8NArchive4NCab19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp47:
# BB#151:                               # %.noexc233
.LBB7_152:                              # %.loopexit.split-lp282
.Ltmp48:
	jmp	.LBB7_163
.LBB7_153:                              # %.loopexit.split-lp277
.Ltmp40:
	jmp	.LBB7_163
.LBB7_154:                              # %.loopexit.split-lp
.Ltmp32:
	jmp	.LBB7_163
.LBB7_155:
.Ltmp54:
	movq	%rax, %rbx
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_164
# BB#156:
	callq	_ZdaPv
	jmp	.LBB7_164
.LBB7_157:
.Ltmp59:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB7_164
.LBB7_158:                              # %.loopexit276
.Ltmp37:
	jmp	.LBB7_163
.LBB7_159:
.Ltmp51:
	jmp	.LBB7_163
.LBB7_160:                              # %.loopexit281
.Ltmp45:
	jmp	.LBB7_163
.LBB7_161:
.Ltmp62:
	jmp	.LBB7_163
.LBB7_162:                              # %.loopexit275
.Ltmp29:
.LBB7_163:
	movq	%rax, %rbx
.LBB7_164:
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN8NArchive4NCab10CInArchive4OpenEPKyRNS0_11CDatabaseExE, .Lfunc_end7-_ZN8NArchive4NCab10CInArchive4OpenEPKyRNS0_11CDatabaseExE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\355\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp15-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp28-.Ltmp15         #   Call between .Ltmp15 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin2   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp36-.Ltmp33         #   Call between .Ltmp33 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin2   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp44-.Ltmp41         #   Call between .Ltmp41 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin2   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin2   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp62-.Lfunc_begin2   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin2   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin2   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp30-.Ltmp61         #   Call between .Ltmp61 and .Ltmp30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin2   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp38-.Ltmp31         #   Call between .Ltmp31 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin2   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin2   # >> Call Site 14 <<
	.long	.Ltmp23-.Ltmp39         #   Call between .Ltmp39 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 15 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp32-.Lfunc_begin2   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 16 <<
	.long	.Ltmp46-.Ltmp24         #   Call between .Ltmp24 and .Ltmp46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin2   # >> Call Site 17 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin2   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin2   # >> Call Site 18 <<
	.long	.Lfunc_end7-.Ltmp47     #   Call between .Ltmp47 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive4NCab13CMvDatabaseEx13AreItemsEqualEii
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab13CMvDatabaseEx13AreItemsEqualEii,@function
_ZN8NArchive4NCab13CMvDatabaseEx13AreItemsEqualEii: # @_ZN8NArchive4NCab13CMvDatabaseEx13AreItemsEqualEii
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 32
.Lcfi68:
	.cfi_offset %rbx, -32
.Lcfi69:
	.cfi_offset %r14, -24
.Lcfi70:
	.cfi_offset %r15, -16
	movq	16(%rdi), %rax
	movq	48(%rdi), %rbx
	movslq	%esi, %rsi
	movslq	%edx, %rcx
	movslq	(%rbx,%rsi,8), %rdx
	movq	(%rax,%rdx,8), %r14
	movslq	(%rbx,%rcx,8), %r15
	movq	(%rax,%r15,8), %r8
	movq	144(%r14), %rax
	movslq	4(%rbx,%rsi,8), %rsi
	movq	(%rax,%rsi,8), %r9
	movq	144(%r8), %rax
	movslq	4(%rbx,%rcx,8), %rcx
	movq	(%rax,%rcx,8), %r10
	movq	80(%rdi), %rax
	movl	(%rax,%rdx,4), %r11d
	movzwl	28(%r9), %ebx
	movl	%ebx, %ecx
	andl	$65533, %ecx            # imm = 0xFFFD
	xorl	%esi, %esi
	cmpl	$65533, %ecx            # imm = 0xFFFD
	movl	$0, %edi
	je	.LBB8_4
# BB#1:
	cmpl	$65534, %ebx            # imm = 0xFFFE
	jb	.LBB8_3
# BB#2:
	movl	108(%r14), %edi
	decl	%edi
	jmp	.LBB8_4
.LBB8_3:
	movl	%ebx, %edi
.LBB8_4:                                # %_ZNK8NArchive4NCab13CMvDatabaseEx14GetFolderIndexEPKNS0_7CMvItemE.exit28
	addl	%r11d, %edi
	movl	(%rax,%r15,4), %eax
	movzwl	28(%r10), %ecx
	movl	%ecx, %edx
	andl	$65533, %edx            # imm = 0xFFFD
	cmpl	$65533, %edx            # imm = 0xFFFD
	je	.LBB8_8
# BB#5:
	cmpl	$65534, %ecx            # imm = 0xFFFE
	jb	.LBB8_7
# BB#6:
	movl	108(%r8), %esi
	decl	%esi
	jmp	.LBB8_8
.LBB8_7:
	movl	%ecx, %esi
.LBB8_8:                                # %_ZNK8NArchive4NCab13CMvDatabaseEx14GetFolderIndexEPKNS0_7CMvItemE.exit
	addl	%eax, %esi
	cmpl	%esi, %edi
	jne	.LBB8_14
# BB#9:
	movl	16(%r9), %eax
	cmpl	16(%r10), %eax
	jne	.LBB8_14
# BB#10:
	movl	20(%r9), %eax
	cmpl	20(%r10), %eax
	jne	.LBB8_14
# BB#11:
	movq	(%r9), %rdi
	movq	(%r10), %rsi
	callq	_Z15MyStringComparePKcS0_
	testl	%eax, %eax
	sete	%al
	jmp	.LBB8_15
.LBB8_14:
	xorl	%eax, %eax
.LBB8_15:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN8NArchive4NCab13CMvDatabaseEx13AreItemsEqualEii, .Lfunc_end8-_ZN8NArchive4NCab13CMvDatabaseEx13AreItemsEqualEii
	.cfi_endproc

	.globl	_ZN8NArchive4NCab13CMvDatabaseEx17FillSortAndShrinkEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab13CMvDatabaseEx17FillSortAndShrinkEv,@function
_ZN8NArchive4NCab13CMvDatabaseEx17FillSortAndShrinkEv: # @_ZN8NArchive4NCab13CMvDatabaseEx17FillSortAndShrinkEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi73:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi74:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi75:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 96
.Lcfi78:
	.cfi_offset %rbx, -56
.Lcfi79:
	.cfi_offset %r12, -48
.Lcfi80:
	.cfi_offset %r13, -40
.Lcfi81:
	.cfi_offset %r14, -32
.Lcfi82:
	.cfi_offset %r15, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	leaq	32(%r12), %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	64(%r12), %r15
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	96(%r12), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
	cmpl	$0, 12(%r12)
	jle	.LBB9_6
# BB#1:                                 # %.lr.ph64
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
                                        #     Child Loop BB9_23 Depth 2
                                        #     Child Loop BB9_27 Depth 2
	movq	16(%r12), %rax
	movq	(%rax,%r14,8), %rdi
	movslq	140(%rdi), %rax
	testq	%rax, %rax
	movl	%ebx, %ebp
	jle	.LBB9_18
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	144(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_4:                                #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	movzwl	28(%rsi), %esi
	andl	$65533, %esi            # imm = 0xFFFD
	cmpl	$65533, %esi            # imm = 0xFFFD
	je	.LBB9_5
# BB#16:                                #   in Loop: Header=BB9_4 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB9_4
# BB#17:                                #   in Loop: Header=BB9_2 Depth=1
	movl	%ebx, %ebp
	jmp	.LBB9_18
	.p2align	4, 0x90
.LBB9_5:                                #   in Loop: Header=BB9_2 Depth=1
	leal	-1(%rbx), %ebp
.LBB9_18:                               # %_ZNK8NArchive4NCab9CDatabase17IsTherePrevFolderEv.exit.thread
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	80(%r12), %rax
	movslq	76(%r12), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	76(%r12)
	movl	108(%rdx), %edi
	movslq	140(%rdx), %rax
	testq	%rax, %rax
	jle	.LBB9_19
# BB#22:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	144(%rdx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_23:                               #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	movzwl	28(%rsi), %esi
	andl	$65533, %esi            # imm = 0xFFFD
	cmpl	$65533, %esi            # imm = 0xFFFD
	je	.LBB9_24
# BB#21:                                #   in Loop: Header=BB9_23 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB9_23
	jmp	.LBB9_25
	.p2align	4, 0x90
.LBB9_19:                               # %_ZNK8NArchive4NCab9CDatabase21GetNumberOfNewFoldersEv.exit.thread
                                        #   in Loop: Header=BB9_2 Depth=1
	addl	%ebx, %edi
	jmp	.LBB9_20
	.p2align	4, 0x90
.LBB9_24:                               #   in Loop: Header=BB9_2 Depth=1
	decl	%edi
.LBB9_25:                               # %_ZNK8NArchive4NCab9CDatabase21GetNumberOfNewFoldersEv.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	addl	%ebx, %edi
	testl	%eax, %eax
	jle	.LBB9_20
# BB#26:                                # %.lr.ph58.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	%edi, 4(%rsp)           # 4-byte Spill
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_27:                               # %.lr.ph58
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %r15
	orq	%r14, %r15
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	48(%r12), %rax
	movslq	44(%r12), %rcx
	movq	%r15, (%rax,%rcx,8)
	incl	44(%r12)
	movabsq	$4294967296, %rax       # imm = 0x100000000
	addq	%rax, %rbx
	incl	%ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	140(%rax), %ebp
	jl	.LBB9_27
# BB#28:                                #   in Loop: Header=BB9_2 Depth=1
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	4(%rsp), %edi           # 4-byte Reload
.LBB9_20:                               # %._crit_edge59
                                        #   in Loop: Header=BB9_2 Depth=1
	incq	%r14
	movslq	12(%r12), %rax
	cmpq	%rax, %r14
	movl	%edi, %ebx
	jl	.LBB9_2
.LBB9_6:                                # %._crit_edge65
	movl	$_ZN8NArchive4NCabL14CompareMvItemsEPKNS0_7CMvItemES3_Pv, %esi
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE4SortEPFiPKS2_S5_PvES6_
	movl	$1, %ebx
	cmpl	$2, 44(%r12)
	jl	.LBB9_11
# BB#7:                                 # %.lr.ph54
	movl	$1, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_8:                                # =>This Inner Loop Header: Depth=1
	leal	1(%rbp), %esi
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	_ZN8NArchive4NCab13CMvDatabaseEx13AreItemsEqualEii
	testb	%al, %al
	jne	.LBB9_10
# BB#9:                                 #   in Loop: Header=BB9_8 Depth=1
	movq	48(%r12), %rax
	movslq	%ebx, %rcx
	incl	%ebx
	movq	8(%rax,%rbp,8), %rdx
	movq	%rdx, (%rax,%rcx,8)
.LBB9_10:                               #   in Loop: Header=BB9_8 Depth=1
	movslq	44(%r12), %rax
	leaq	1(%rbp), %rcx
	addq	$2, %rbp
	cmpq	%rax, %rbp
	movq	%rcx, %rbp
	jl	.LBB9_8
.LBB9_11:                               # %._crit_edge55
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	_ZN17CBaseRecordVector10DeleteFromEi
	movl	44(%r12), %eax
	testl	%eax, %eax
	movq	24(%rsp), %rbx          # 8-byte Reload
	jle	.LBB9_33
# BB#12:                                # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_13:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rcx
	movq	48(%r12), %rsi
	movslq	(%rsi,%rbp,8), %rdi
	movq	(%rcx,%rdi,8), %rdx
	movq	80(%r12), %rcx
	movl	(%rcx,%rdi,4), %ecx
	movq	144(%rdx), %rdi
	movslq	4(%rsi,%rbp,8), %rsi
	movq	(%rdi,%rsi,8), %rsi
	movzwl	28(%rsi), %edi
	movl	%edi, %esi
	andl	$65533, %esi            # imm = 0xFFFD
	cmpl	$65533, %esi            # imm = 0xFFFD
	movl	$0, %esi
	je	.LBB9_30
# BB#14:                                #   in Loop: Header=BB9_13 Depth=1
	cmpl	$65534, %edi            # imm = 0xFFFE
	jb	.LBB9_29
# BB#15:                                #   in Loop: Header=BB9_13 Depth=1
	movl	108(%rdx), %esi
	decl	%esi
	jmp	.LBB9_30
	.p2align	4, 0x90
.LBB9_29:                               #   in Loop: Header=BB9_13 Depth=1
	movl	%edi, %esi
.LBB9_30:                               # %_ZNK8NArchive4NCab13CMvDatabaseEx14GetFolderIndexEPKNS0_7CMvItemE.exit
                                        #   in Loop: Header=BB9_13 Depth=1
	addl	%ecx, %esi
	cmpl	108(%r12), %esi
	jl	.LBB9_32
# BB#31:                                #   in Loop: Header=BB9_13 Depth=1
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	112(%r12), %rax
	movslq	108(%r12), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	108(%r12)
	movl	44(%r12), %eax
.LBB9_32:                               #   in Loop: Header=BB9_13 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB9_13
.LBB9_33:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZN8NArchive4NCab13CMvDatabaseEx17FillSortAndShrinkEv, .Lfunc_end9-_ZN8NArchive4NCab13CMvDatabaseEx17FillSortAndShrinkEv
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE4SortEPFiPKS2_S5_PvES6_,"axG",@progbits,_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE4SortEPFiPKS2_S5_PvES6_,comdat
	.weak	_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE4SortEPFiPKS2_S5_PvES6_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE4SortEPFiPKS2_S5_PvES6_,@function
_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE4SortEPFiPKS2_S5_PvES6_: # @_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE4SortEPFiPKS2_S5_PvES6_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 96
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%rsi, %r8
	movslq	12(%rdi), %rsi
	cmpq	$2, %rsi
	jl	.LBB10_22
# BB#1:
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	16(%rdi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-8(%rax), %r12
	movl	%esi, %edx
	shrl	%edx
	movq	%r8, (%rsp)             # 8-byte Spill
	.p2align	4, 0x90
.LBB10_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_5 Depth 2
	movq	(%r12,%rdx,8), %rax
	movq	%rax, 8(%rsp)
	leal	(%rdx,%rdx), %ebx
	cmpl	%esi, %ebx
	jle	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movl	%edx, %r14d
	jmp	.LBB10_12
	.p2align	4, 0x90
.LBB10_4:                               # %.lr.ph.i29.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r15d
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph.i29
                                        #   Parent Loop BB10_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %r13
	cmpl	%esi, %ebx
	jge	.LBB10_6
# BB#7:                                 #   in Loop: Header=BB10_5 Depth=2
	movslq	%ebx, %rax
	leaq	(%r12,%rax,8), %rsi
	leaq	8(%r12,%rax,8), %rdi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	*(%rsp)                 # 8-byte Folded Reload
	movq	(%rsp), %r8             # 8-byte Reload
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setg	%r14b
	orl	%ebx, %r14d
	jmp	.LBB10_8
	.p2align	4, 0x90
.LBB10_6:                               #   in Loop: Header=BB10_5 Depth=2
	movl	%ebx, %r14d
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB10_8:                               #   in Loop: Header=BB10_5 Depth=2
	movslq	%r14d, %rax
	leaq	(%r12,%rax,8), %rbx
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	*%r8
	testl	%eax, %eax
	jns	.LBB10_9
# BB#10:                                #   in Loop: Header=BB10_5 Depth=2
	movslq	%r15d, %rax
	movq	(%rbx), %rcx
	movq	%rcx, (%r12,%rax,8)
	leal	(%r14,%r14), %ebx
	movq	%r13, %rsi
	cmpl	%esi, %ebx
	movl	%r14d, %r15d
	movq	(%rsp), %r8             # 8-byte Reload
	jle	.LBB10_5
	jmp	.LBB10_11
	.p2align	4, 0x90
.LBB10_9:                               #   in Loop: Header=BB10_2 Depth=1
	movl	%r15d, %r14d
	movq	(%rsp), %r8             # 8-byte Reload
	movq	%r13, %rsi
.LBB10_11:                              # %._crit_edge.loopexit.i34
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	8(%rsp), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB10_12:                              # %_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE11SortRefDownEPS2_iiPFiPKS2_S6_PvES7_.exit36
                                        #   in Loop: Header=BB10_2 Depth=1
	movslq	%r14d, %rcx
	movq	%rax, (%r12,%rcx,8)
	decq	%rdx
	testl	%edx, %edx
	jne	.LBB10_2
# BB#13:                                # %.preheader
	movq	24(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB10_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_16 Depth 2
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	-8(%rdx,%rsi,8), %rax
	movq	(%rdx), %rcx
	movq	%rcx, -8(%rdx,%rsi,8)
	movq	%rax, (%rdx)
	movq	%rax, 8(%rsp)
	cmpq	$3, %rsi
	jl	.LBB10_23
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB10_14 Depth=1
	leaq	-1(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r15d
	movl	$2, %eax
	.p2align	4, 0x90
.LBB10_16:                              # %.lr.ph.i
                                        #   Parent Loop BB10_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %r13
	movslq	%eax, %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	movl	%eax, %ebp
	jge	.LBB10_18
# BB#17:                                #   in Loop: Header=BB10_16 Depth=2
	leaq	(%r12,%rbx,8), %rsi
	leaq	8(%r12,%rbx,8), %rdi
	movq	%r14, %rdx
	callq	*(%rsp)                 # 8-byte Folded Reload
	movq	(%rsp), %r8             # 8-byte Reload
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setg	%cl
	orl	%ecx, %ebx
	movl	%ebx, %ebp
.LBB10_18:                              #   in Loop: Header=BB10_16 Depth=2
	movslq	%ebp, %rax
	leaq	(%r12,%rax,8), %rbx
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	*%r8
	testl	%eax, %eax
	jns	.LBB10_19
# BB#20:                                #   in Loop: Header=BB10_16 Depth=2
	movslq	%r15d, %rax
	movq	(%rbx), %rcx
	movq	%rcx, (%r12,%rax,8)
	leal	(%rbp,%rbp), %eax
	movslq	%eax, %rcx
	movq	%r13, %rsi
	cmpq	%rsi, %rcx
	movl	%ebp, %r15d
	movq	(%rsp), %r8             # 8-byte Reload
	jl	.LBB10_16
	jmp	.LBB10_21
	.p2align	4, 0x90
.LBB10_19:                              #   in Loop: Header=BB10_14 Depth=1
	movl	%r15d, %ebp
	movq	(%rsp), %r8             # 8-byte Reload
.LBB10_21:                              # %_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE11SortRefDownEPS2_iiPFiPKS2_S6_PvES7_.exit
                                        #   in Loop: Header=BB10_14 Depth=1
	movq	8(%rsp), %rax
	movslq	%ebp, %rcx
	movq	%rax, (%r12,%rcx,8)
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	$1, %rax
	movq	%rax, %rsi
	jg	.LBB10_14
	jmp	.LBB10_22
.LBB10_23:                              # %_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE11SortRefDownEPS2_iiPFiPKS2_S6_PvES7_.exit.thread
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
.LBB10_22:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE4SortEPFiPKS2_S5_PvES6_, .Lfunc_end10-_ZN13CRecordVectorIN8NArchive4NCab7CMvItemEE4SortEPFiPKS2_S5_PvES6_
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCabL14CompareMvItemsEPKNS0_7CMvItemES3_Pv,@function
_ZN8NArchive4NCabL14CompareMvItemsEPKNS0_7CMvItemES3_Pv: # @_ZN8NArchive4NCabL14CompareMvItemsEPKNS0_7CMvItemES3_Pv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 48
.Lcfi102:
	.cfi_offset %rbx, -48
.Lcfi103:
	.cfi_offset %r12, -40
.Lcfi104:
	.cfi_offset %r14, -32
.Lcfi105:
	.cfi_offset %r15, -24
.Lcfi106:
	.cfi_offset %rbp, -16
	movq	16(%rdx), %rax
	movslq	(%rdi), %r8
	movq	(%rax,%r8,8), %rbx
	movslq	(%rsi), %r11
	movq	(%rax,%r11,8), %r12
	movslq	4(%rdi), %r9
	movq	144(%rbx), %rax
	movq	(%rax,%r9,8), %r15
	movslq	4(%rsi), %r10
	movq	144(%r12), %rax
	movq	(%rax,%r10,8), %rsi
	movzwl	32(%r15), %ebp
	andw	$16, %bp
	movzwl	32(%rsi), %edi
	sete	%cl
	andw	$16, %di
	jne	.LBB11_2
# BB#1:
	movl	$-1, %eax
	testb	%cl, %cl
	je	.LBB11_17
.LBB11_2:
	testw	%di, %di
	sete	%cl
	testw	%bp, %bp
	jne	.LBB11_4
# BB#3:
	movl	$1, %eax
	testb	%cl, %cl
	je	.LBB11_17
.LBB11_4:
	movq	80(%rdx), %rax
	movl	(%rax,%r8,4), %r14d
	movzwl	28(%r15), %edi
	movl	%edi, %ecx
	andl	$65533, %ecx            # imm = 0xFFFD
	xorl	%edx, %edx
	cmpl	$65533, %ecx            # imm = 0xFFFD
	movl	$0, %ebp
	je	.LBB11_8
# BB#5:
	cmpl	$65534, %edi            # imm = 0xFFFE
	jb	.LBB11_7
# BB#6:
	movl	108(%rbx), %ebp
	decl	%ebp
	jmp	.LBB11_8
.LBB11_7:
	movl	%edi, %ebp
.LBB11_8:                               # %_ZNK8NArchive4NCab13CMvDatabaseEx14GetFolderIndexEPKNS0_7CMvItemE.exit80
	addl	%r14d, %ebp
	movl	(%rax,%r11,4), %eax
	movzwl	28(%rsi), %edi
	movl	%edi, %ecx
	andl	$65533, %ecx            # imm = 0xFFFD
	cmpl	$65533, %ecx            # imm = 0xFFFD
	je	.LBB11_12
# BB#9:
	cmpl	$65534, %edi            # imm = 0xFFFE
	jb	.LBB11_11
# BB#10:
	movl	108(%r12), %edx
	decl	%edx
	jmp	.LBB11_12
.LBB11_11:
	movl	%edi, %edx
.LBB11_12:                              # %_ZNK8NArchive4NCab13CMvDatabaseEx14GetFolderIndexEPKNS0_7CMvItemE.exit
	addl	%eax, %edx
	xorl	%eax, %eax
	cmpl	%edx, %ebp
	setne	%al
	movl	$-1, %ecx
	cmovll	%ecx, %eax
	testl	%eax, %eax
	jne	.LBB11_17
# BB#13:
	movl	16(%r15), %edx
	xorl	%eax, %eax
	cmpl	16(%rsi), %edx
	setne	%al
	cmovbl	%ecx, %eax
	testl	%eax, %eax
	jne	.LBB11_17
# BB#14:
	movl	20(%r15), %ecx
	xorl	%eax, %eax
	cmpl	20(%rsi), %ecx
	setne	%al
	movl	$-1, %ecx
	cmovbl	%ecx, %eax
	testl	%eax, %eax
	jne	.LBB11_17
# BB#15:
	xorl	%eax, %eax
	cmpl	%r11d, %r8d
	setne	%al
	cmovll	%ecx, %eax
	testl	%eax, %eax
	jne	.LBB11_17
# BB#16:
	xorl	%ecx, %ecx
	cmpl	%r10d, %r9d
	setne	%cl
	movl	$-1, %eax
	cmovgel	%ecx, %eax
.LBB11_17:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN8NArchive4NCabL14CompareMvItemsEPKNS0_7CMvItemES3_Pv, .Lfunc_end11-_ZN8NArchive4NCabL14CompareMvItemsEPKNS0_7CMvItemES3_Pv
	.cfi_endproc

	.globl	_ZN8NArchive4NCab13CMvDatabaseEx5CheckEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NCab13CMvDatabaseEx5CheckEv,@function
_ZN8NArchive4NCab13CMvDatabaseEx5CheckEv: # @_ZN8NArchive4NCab13CMvDatabaseEx5CheckEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi110:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi111:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 56
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movslq	12(%rdi), %r8
	cmpq	$2, %r8
	jl	.LBB12_5
# BB#1:                                 # %.lr.ph116
	movq	16(%rdi), %r9
	movl	$1, %edx
	.p2align	4, 0x90
.LBB12_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_11 Depth 2
	movq	(%r9,%rdx,8), %rsi
	movslq	140(%rsi), %rbp
	testq	%rbp, %rbp
	jle	.LBB12_4
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	144(%rsi), %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB12_11:                              #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rax,8), %rcx
	movzwl	28(%rcx), %ecx
	andl	$65533, %ecx            # imm = 0xFFFD
	cmpl	$65533, %ecx            # imm = 0xFFFD
	je	.LBB12_12
# BB#10:                                #   in Loop: Header=BB12_11 Depth=2
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB12_11
	jmp	.LBB12_4
	.p2align	4, 0x90
.LBB12_12:                              # %_ZNK8NArchive4NCab9CDatabase17IsTherePrevFolderEv.exit
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	-8(%r9,%rdx,8), %rax
	movslq	108(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB12_32
# BB#13:                                #   in Loop: Header=BB12_2 Depth=1
	cmpl	$0, 108(%rsi)
	je	.LBB12_32
# BB#14:                                #   in Loop: Header=BB12_2 Depth=1
	movq	112(%rax), %rax
	movq	-8(%rax,%rbp,8), %rax
	movq	112(%rsi), %rcx
	movq	(%rcx), %rsi
	movb	6(%rax), %cl
	cmpb	6(%rsi), %cl
	jne	.LBB12_32
# BB#15:                                #   in Loop: Header=BB12_2 Depth=1
	movb	7(%rax), %al
	cmpb	7(%rsi), %al
	jne	.LBB12_32
.LBB12_4:                               # %_ZNK8NArchive4NCab9CDatabase17IsTherePrevFolderEv.exit.thread
                                        #   in Loop: Header=BB12_2 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jl	.LBB12_2
.LBB12_5:                               # %.preheader
	movslq	44(%rdi), %r15
	testq	%r15, %r15
	movb	$1, %al
	jle	.LBB12_33
# BB#6:                                 # %.lr.ph
	movq	16(%rdi), %r8
	movq	48(%rdi), %r12
	movq	80(%rdi), %r9
	movl	108(%rdi), %r11d
	movl	$-2, %eax
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB12_7:                               # =>This Inner Loop Header: Depth=1
	movslq	(%r12,%rdx,8), %rcx
	movq	(%r8,%rcx,8), %rsi
	movl	(%r9,%rcx,4), %ebp
	movq	144(%rsi), %rcx
	movslq	4(%r12,%rdx,8), %rdi
	movq	(%rcx,%rdi,8), %rdi
	movl	108(%rsi), %ecx
	movzwl	28(%rdi), %r10d
	movl	%r10d, %esi
	andl	$65533, %esi            # imm = 0xFFFD
	cmpl	$65533, %esi            # imm = 0xFFFD
	movl	$0, %ebx
	je	.LBB12_17
# BB#8:                                 #   in Loop: Header=BB12_7 Depth=1
	cmpl	$65534, %r10d           # imm = 0xFFFE
	jb	.LBB12_16
# BB#9:                                 #   in Loop: Header=BB12_7 Depth=1
	leal	-1(%rcx), %ebx
	jmp	.LBB12_17
	.p2align	4, 0x90
.LBB12_16:                              #   in Loop: Header=BB12_7 Depth=1
	movl	%r10d, %ebx
.LBB12_17:                              # %_ZNK8NArchive4NCab13CMvDatabaseEx14GetFolderIndexEPKNS0_7CMvItemE.exit91
                                        #   in Loop: Header=BB12_7 Depth=1
	addl	%ebp, %ebx
	cmpl	%r11d, %ebx
	jge	.LBB12_32
# BB#18:                                #   in Loop: Header=BB12_7 Depth=1
	testb	$16, 32(%rdi)
	jne	.LBB12_26
# BB#19:                                #   in Loop: Header=BB12_7 Depth=1
	movzwl	%si, %ebx
	xorl	%esi, %esi
	cmpl	$65533, %ebx            # imm = 0xFFFD
	je	.LBB12_23
# BB#20:                                #   in Loop: Header=BB12_7 Depth=1
	cmpl	$65534, %r10d           # imm = 0xFFFE
	jb	.LBB12_22
# BB#21:                                #   in Loop: Header=BB12_7 Depth=1
	decl	%ecx
	movl	%ecx, %esi
	jmp	.LBB12_23
.LBB12_22:                              #   in Loop: Header=BB12_7 Depth=1
	movl	%r10d, %esi
.LBB12_23:                              # %_ZNK8NArchive4NCab13CMvDatabaseEx14GetFolderIndexEPKNS0_7CMvItemE.exit
                                        #   in Loop: Header=BB12_7 Depth=1
	addl	%ebp, %esi
	movl	16(%rdi), %ecx
	cmpl	%eax, %esi
	jne	.LBB12_24
# BB#28:                                #   in Loop: Header=BB12_7 Depth=1
	cmpq	%r13, %rcx
	jae	.LBB12_29
# BB#30:                                #   in Loop: Header=BB12_7 Depth=1
	cmpl	%r14d, %ecx
	jne	.LBB12_32
# BB#31:                                #   in Loop: Header=BB12_7 Depth=1
	movl	20(%rdi), %esi
	addq	%rcx, %rsi
	cmpq	%r13, %rsi
	je	.LBB12_25
	jmp	.LBB12_32
	.p2align	4, 0x90
.LBB12_24:                              #   in Loop: Header=BB12_7 Depth=1
	movl	%ecx, %r14d
	movl	%esi, %eax
	jmp	.LBB12_25
.LBB12_29:                              #   in Loop: Header=BB12_7 Depth=1
	movl	%ecx, %r14d
.LBB12_25:                              # %_ZNK8NArchive4NCab13CMvDatabaseEx14GetFolderIndexEPKNS0_7CMvItemE.exit._crit_edge
                                        #   in Loop: Header=BB12_7 Depth=1
	movl	%r14d, %ecx
	movl	20(%rdi), %r13d
	addq	%rcx, %r13
.LBB12_26:                              #   in Loop: Header=BB12_7 Depth=1
	incq	%rdx
	cmpq	%r15, %rdx
	jl	.LBB12_7
# BB#27:
	movb	$1, %al
	jmp	.LBB12_33
.LBB12_32:
	xorl	%eax, %eax
.LBB12_33:                              # %.thread
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN8NArchive4NCab13CMvDatabaseEx5CheckEv, .Lfunc_end12-_ZN8NArchive4NCab13CMvDatabaseEx5CheckEv
	.cfi_endproc

	.type	_ZTSN8NArchive4NCab19CInArchiveExceptionE,@object # @_ZTSN8NArchive4NCab19CInArchiveExceptionE
	.section	.rodata._ZTSN8NArchive4NCab19CInArchiveExceptionE,"aG",@progbits,_ZTSN8NArchive4NCab19CInArchiveExceptionE,comdat
	.weak	_ZTSN8NArchive4NCab19CInArchiveExceptionE
	.p2align	4
_ZTSN8NArchive4NCab19CInArchiveExceptionE:
	.asciz	"N8NArchive4NCab19CInArchiveExceptionE"
	.size	_ZTSN8NArchive4NCab19CInArchiveExceptionE, 38

	.type	_ZTIN8NArchive4NCab19CInArchiveExceptionE,@object # @_ZTIN8NArchive4NCab19CInArchiveExceptionE
	.section	.rodata._ZTIN8NArchive4NCab19CInArchiveExceptionE,"aG",@progbits,_ZTIN8NArchive4NCab19CInArchiveExceptionE,comdat
	.weak	_ZTIN8NArchive4NCab19CInArchiveExceptionE
	.p2align	3
_ZTIN8NArchive4NCab19CInArchiveExceptionE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NArchive4NCab19CInArchiveExceptionE
	.size	_ZTIN8NArchive4NCab19CInArchiveExceptionE, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
