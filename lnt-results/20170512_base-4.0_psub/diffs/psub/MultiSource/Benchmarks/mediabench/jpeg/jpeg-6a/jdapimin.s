	.text
	.file	"jdapimin.bc"
	.globl	jpeg_CreateDecompress
	.p2align	4, 0x90
	.type	jpeg_CreateDecompress,@function
jpeg_CreateDecompress:                  # @jpeg_CreateDecompress
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	cmpl	$61, %esi
	je	.LBB0_2
# BB#1:
	movq	(%rbx), %rax
	movl	$10, 40(%rax)
	movl	$61, 44(%rax)
	movl	%esi, 48(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_2:
	cmpq	$616, %r14              # imm = 0x268
	je	.LBB0_4
# BB#3:
	movq	(%rbx), %rax
	movl	$19, 40(%rax)
	movl	$616, 44(%rax)          # imm = 0x268
	movl	%r14d, 48(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_4:                                # %.preheader.preheader
	movq	(%rbx), %r14
	leaq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$608, %edx              # imm = 0x260
	callq	memset
	movq	%r14, (%rbx)
	movl	$1, 24(%rbx)
	movq	%rbx, %rdi
	callq	jinit_memory_mgr
	movq	$0, 16(%rbx)
	movq	$0, 32(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 272(%rbx)
	movups	%xmm0, 256(%rbx)
	movups	%xmm0, 240(%rbx)
	movups	%xmm0, 224(%rbx)
	movups	%xmm0, 208(%rbx)
	movups	%xmm0, 192(%rbx)
	movq	%rbx, %rdi
	callq	jinit_marker_reader
	movq	%rbx, %rdi
	callq	jinit_input_controller
	movl	$200, 28(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	jpeg_CreateDecompress, .Lfunc_end0-jpeg_CreateDecompress
	.cfi_endproc

	.globl	jpeg_destroy_decompress
	.p2align	4, 0x90
	.type	jpeg_destroy_decompress,@function
jpeg_destroy_decompress:                # @jpeg_destroy_decompress
	.cfi_startproc
# BB#0:
	jmp	jpeg_destroy            # TAILCALL
.Lfunc_end1:
	.size	jpeg_destroy_decompress, .Lfunc_end1-jpeg_destroy_decompress
	.cfi_endproc

	.globl	jpeg_abort_decompress
	.p2align	4, 0x90
	.type	jpeg_abort_decompress,@function
jpeg_abort_decompress:                  # @jpeg_abort_decompress
	.cfi_startproc
# BB#0:
	jmp	jpeg_abort              # TAILCALL
.Lfunc_end2:
	.size	jpeg_abort_decompress, .Lfunc_end2-jpeg_abort_decompress
	.cfi_endproc

	.globl	jpeg_set_marker_processor
	.p2align	4, 0x90
	.type	jpeg_set_marker_processor,@function
jpeg_set_marker_processor:              # @jpeg_set_marker_processor
	.cfi_startproc
# BB#0:
	cmpl	$254, %esi
	jne	.LBB3_2
# BB#1:
	movq	568(%rdi), %rax
	movq	%rdx, 24(%rax)
	retq
.LBB3_2:
	movl	%esi, %eax
	andl	$-16, %eax
	cmpl	$224, %eax
	jne	.LBB3_5
# BB#3:
	movq	568(%rdi), %rax
	movslq	%esi, %rcx
	movq	%rdx, -1760(%rax,%rcx,8)
	retq
.LBB3_5:
	movq	(%rdi), %rax
	movl	$67, 40(%rax)
	movl	%esi, 44(%rax)
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end3:
	.size	jpeg_set_marker_processor, .Lfunc_end3-jpeg_set_marker_processor
	.cfi_endproc

	.globl	jpeg_read_header
	.p2align	4, 0x90
	.type	jpeg_read_header,@function
jpeg_read_header:                       # @jpeg_read_header
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	movl	%eax, %ecx
	andl	$-2, %ecx
	cmpl	$200, %ecx
	je	.LBB4_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB4_2:
	movq	%rbx, %rdi
	callq	jpeg_consume_input
	cmpl	$2, %eax
	je	.LBB4_5
# BB#3:
	cmpl	$1, %eax
	jne	.LBB4_8
# BB#4:
	movl	$1, %eax
	jmp	.LBB4_8
.LBB4_5:
	testl	%ebp, %ebp
	je	.LBB4_7
# BB#6:
	movq	(%rbx), %rax
	movl	$50, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB4_7:
	movq	%rbx, %rdi
	callq	jpeg_abort
	movl	$2, %eax
.LBB4_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	jpeg_read_header, .Lfunc_end4-jpeg_read_header
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
.LCPI5_1:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	1                       # 0x1
	.text
	.globl	jpeg_consume_input
	.p2align	4, 0x90
	.type	jpeg_consume_input,@function
jpeg_consume_input:                     # @jpeg_consume_input
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %ecx
	leal	-200(%rcx), %edx
	cmpl	$10, %edx
	ja	.LBB5_9
# BB#1:
	movl	$1, %eax
	jmpq	*.LJTI5_0(,%rdx,8)
.LBB5_2:
	movq	560(%rbx), %rax
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.LBB5_3:
	movq	560(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	32(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	movl	$201, 28(%rbx)
.LBB5_4:
	movq	560(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$1, %eax
	jne	.LBB5_32
# BB#5:
	movl	48(%rbx), %eax
	cmpl	$4, %eax
	je	.LBB5_10
# BB#6:
	cmpl	$3, %eax
	je	.LBB5_15
# BB#7:
	xorl	%ebp, %ebp
	cmpl	$1, %eax
	movl	$0, %r14d
	jne	.LBB5_31
# BB#8:
	movl	$1, %ebp
	movl	$1, %r14d
	jmp	.LBB5_31
.LBB5_9:
	movq	(%rbx), %rax
	movl	$18, 40(%rax)
	movl	%ecx, 44(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	xorl	%eax, %eax
	jmp	.LBB5_32
.LBB5_10:
	movl	$4, %r14d
	cmpl	$0, 376(%rbx)
	je	.LBB5_21
# BB#11:
	movzbl	380(%rbx), %eax
	testl	%eax, %eax
	je	.LBB5_21
# BB#12:
	cmpb	$2, %al
	je	.LBB5_14
# BB#13:
	movq	(%rbx), %rcx
	movl	$110, 40(%rcx)
	movl	%eax, 44(%rcx)
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	*8(%rcx)
.LBB5_14:                               # %default_decompress_parms.exit
	movl	$5, %ebp
	jmp	.LBB5_31
.LBB5_15:
	movl	$2, %r14d
	movl	$3, %ebp
	cmpl	$0, 364(%rbx)
	jne	.LBB5_31
# BB#16:
	cmpl	$0, 376(%rbx)
	je	.LBB5_22
# BB#17:
	movzbl	380(%rbx), %eax
	testl	%eax, %eax
	movl	$2, %r14d
	je	.LBB5_29
# BB#18:
	cmpb	$1, %al
	je	.LBB5_31
# BB#19:
	movq	(%rbx), %rcx
	movl	$110, 40(%rcx)
	movl	%eax, 44(%rcx)
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	*8(%rcx)
	jmp	.LBB5_31
.LBB5_21:
	movl	$4, %ebp
.LBB5_31:                               # %default_decompress_parms.exit
	movl	%ebp, 52(%rbx)
	movl	%r14d, 56(%rbx)
	movl	$1, 60(%rbx)
	movl	$1, 64(%rbx)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 72(%rbx)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [0,0,0,1]
	movups	%xmm0, 80(%rbx)
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1,0,2,1]
	movups	%xmm0, 96(%rbx)
	movl	$256, 112(%rbx)         # imm = 0x100
	movq	$0, 152(%rbx)
	movl	$0, 116(%rbx)
	movl	$0, 120(%rbx)
	movl	$0, 124(%rbx)
	movl	$202, 28(%rbx)
	movl	$1, %eax
.LBB5_32:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB5_22:
	movq	296(%rbx), %rdx
	movl	(%rdx), %eax
	movl	96(%rdx), %ecx
	movl	192(%rdx), %edx
	cmpl	$1, %eax
	jne	.LBB5_25
# BB#23:
	cmpl	$2, %ecx
	jne	.LBB5_25
# BB#24:
	cmpl	$3, %edx
	je	.LBB5_31
.LBB5_25:
	movl	$2, %r14d
	cmpl	$82, %eax
	jne	.LBB5_30
# BB#26:
	cmpl	$71, %ecx
	jne	.LBB5_30
# BB#27:
	cmpl	$66, %edx
	jne	.LBB5_30
.LBB5_29:
	movl	$2, %ebp
	jmp	.LBB5_31
.LBB5_30:
	movq	(%rbx), %r8
	movl	%eax, 44(%r8)
	movl	%ecx, 48(%r8)
	movl	%edx, 52(%r8)
	movl	$107, 40(%r8)
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*8(%r8)
	jmp	.LBB5_31
.Lfunc_end5:
	.size	jpeg_consume_input, .Lfunc_end5-jpeg_consume_input
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_3
	.quad	.LBB5_4
	.quad	.LBB5_32
	.quad	.LBB5_2
	.quad	.LBB5_2
	.quad	.LBB5_2
	.quad	.LBB5_2
	.quad	.LBB5_2
	.quad	.LBB5_2
	.quad	.LBB5_9
	.quad	.LBB5_2

	.text
	.globl	jpeg_input_complete
	.p2align	4, 0x90
	.type	jpeg_input_complete,@function
jpeg_input_complete:                    # @jpeg_input_complete
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	leal	-200(%rax), %ecx
	cmpl	$11, %ecx
	jb	.LBB6_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB6_2:
	movq	560(%rbx), %rax
	movl	36(%rax), %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	jpeg_input_complete, .Lfunc_end6-jpeg_input_complete
	.cfi_endproc

	.globl	jpeg_has_multiple_scans
	.p2align	4, 0x90
	.type	jpeg_has_multiple_scans,@function
jpeg_has_multiple_scans:                # @jpeg_has_multiple_scans
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	leal	-202(%rax), %ecx
	cmpl	$9, %ecx
	jb	.LBB7_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB7_2:
	movq	560(%rbx), %rax
	movl	32(%rax), %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	jpeg_has_multiple_scans, .Lfunc_end7-jpeg_has_multiple_scans
	.cfi_endproc

	.globl	jpeg_finish_decompress
	.p2align	4, 0x90
	.type	jpeg_finish_decompress,@function
jpeg_finish_decompress:                 # @jpeg_finish_decompress
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
.Lcfi21:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	leal	-205(%rax), %ecx
	cmpl	$1, %ecx
	ja	.LBB8_5
# BB#1:
	cmpl	$0, 80(%rbx)
	je	.LBB8_2
.LBB8_5:                                # %thread-pre-split
	cmpl	$210, %eax
	je	.LBB8_9
# BB#6:                                 # %thread-pre-split
	cmpl	$207, %eax
	jne	.LBB8_8
# BB#7:
	movl	$210, 28(%rbx)
	jmp	.LBB8_9
.LBB8_8:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
	jmp	.LBB8_9
.LBB8_2:
	movl	160(%rbx), %eax
	cmpl	132(%rbx), %eax
	jae	.LBB8_4
# BB#3:
	movq	(%rbx), %rax
	movl	$66, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB8_4:
	movq	528(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movl	$210, 28(%rbx)
	.p2align	4, 0x90
.LBB8_9:                                # =>This Inner Loop Header: Depth=1
	movq	560(%rbx), %rax
	cmpl	$0, 36(%rax)
	jne	.LBB8_12
# BB#10:                                #   in Loop: Header=BB8_9 Depth=1
	movq	%rbx, %rdi
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB8_9
# BB#11:
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB8_12:
	movq	32(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	%rbx, %rdi
	callq	jpeg_abort
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	jpeg_finish_decompress, .Lfunc_end8-jpeg_finish_decompress
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
