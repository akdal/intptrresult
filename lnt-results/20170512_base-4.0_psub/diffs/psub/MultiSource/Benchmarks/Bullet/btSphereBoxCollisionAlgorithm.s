	.text
	.file	"btSphereBoxCollisionAlgorithm.bc"
	.globl	_ZN29btSphereBoxCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.p2align	4, 0x90
	.type	_ZN29btSphereBoxCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b,@function
_ZN29btSphereBoxCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b: # @_ZN29btSphereBoxCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movq	%r8, %r14
	movq	%rcx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rdx, %rsi
	movq	%rbp, %rdx
	movq	%r14, %rcx
	callq	_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	movq	$_ZTV29btSphereBoxCollisionAlgorithm+16, (%rbx)
	movb	$0, 16(%rbx)
	movq	%r12, 24(%rbx)
	movb	%r15b, 32(%rbx)
	testb	%r15b, %r15b
	movq	%rbp, %r15
	cmovneq	%r14, %r15
	cmovneq	%rbp, %r14
	testq	%r12, %r12
	jne	.LBB0_5
# BB#1:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp0:
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*48(%rax)
.Ltmp1:
# BB#2:
	testb	%al, %al
	je	.LBB0_5
# BB#3:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp2:
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	*24(%rax)
.Ltmp3:
# BB#4:
	movq	%rax, 24(%rbx)
	movb	$1, 16(%rbx)
.LBB0_5:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_6:
.Ltmp4:
	movq	%rax, %r14
.Ltmp5:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp6:
# BB#7:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_8:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN29btSphereBoxCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b, .Lfunc_end0-_ZN29btSphereBoxCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN29btSphereBoxCollisionAlgorithmD2Ev
	.p2align	4, 0x90
	.type	_ZN29btSphereBoxCollisionAlgorithmD2Ev,@function
_ZN29btSphereBoxCollisionAlgorithmD2Ev: # @_ZN29btSphereBoxCollisionAlgorithmD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV29btSphereBoxCollisionAlgorithm+16, (%rbx)
	cmpb	$0, 16(%rbx)
	je	.LBB2_3
# BB#1:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp8:
	callq	*32(%rax)
.Ltmp9:
.LBB2_3:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN30btActivatingCollisionAlgorithmD2Ev # TAILCALL
.LBB2_4:
.Ltmp10:
	movq	%rax, %r14
.Ltmp11:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp12:
# BB#5:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_6:
.Ltmp13:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN29btSphereBoxCollisionAlgorithmD2Ev, .Lfunc_end2-_ZN29btSphereBoxCollisionAlgorithmD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp9          #   Call between .Ltmp9 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN29btSphereBoxCollisionAlgorithmD0Ev
	.p2align	4, 0x90
	.type	_ZN29btSphereBoxCollisionAlgorithmD0Ev,@function
_ZN29btSphereBoxCollisionAlgorithmD0Ev: # @_ZN29btSphereBoxCollisionAlgorithmD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV29btSphereBoxCollisionAlgorithm+16, (%rbx)
	cmpb	$0, 16(%rbx)
	je	.LBB3_3
# BB#1:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB3_3
# BB#2:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp14:
	callq	*32(%rax)
.Ltmp15:
.LBB3_3:
.Ltmp20:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp21:
# BB#4:                                 # %_ZN29btSphereBoxCollisionAlgorithmD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_5:
.Ltmp16:
	movq	%rax, %r14
.Ltmp17:
	movq	%rbx, %rdi
	callq	_ZN30btActivatingCollisionAlgorithmD2Ev
.Ltmp18:
	jmp	.LBB3_8
.LBB3_6:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_7:
.Ltmp22:
	movq	%rax, %r14
.LBB3_8:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN29btSphereBoxCollisionAlgorithmD0Ev, .Lfunc_end3-_ZN29btSphereBoxCollisionAlgorithmD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin2   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin2   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	872415232               # float 1.1920929E-7
.LCPI4_1:
	.long	1065353216              # float 1
	.text
	.globl	_ZN29btSphereBoxCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN29btSphereBoxCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN29btSphereBoxCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN29btSphereBoxCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	subq	$104, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 128
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%r8, %r14
	movq	%rdi, %rbx
	cmpq	$0, 24(%rbx)
	je	.LBB4_12
# BB#1:
	cmpb	$0, 32(%rbx)
	movq	%rsi, %rax
	cmovneq	%rdx, %rax
	cmovneq	%rsi, %rdx
	movq	200(%rax), %rcx
	movups	56(%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movss	40(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	24(%rcx), %xmm0
	leaq	16(%rsp), %rax
	leaq	48(%rsp), %rcx
	leaq	80(%rsp), %r8
	movq	%rbx, %rdi
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	_ZN29btSphereBoxCollisionAlgorithm17getSphereDistanceEP17btCollisionObjectR9btVector3S3_RKS2_f
	movaps	%xmm0, %xmm2
	movq	24(%rbx), %rax
	movq	%rax, 8(%r14)
	movss	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB4_5
# BB#2:
	movsd	16(%rsp), %xmm3         # xmm3 = mem[0],zero
	movsd	48(%rsp), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm0, %xmm3
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	24(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	56(%rsp), %xmm4
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB4_4
# BB#3:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movaps	%xmm3, 64(%rsp)         # 16-byte Spill
	movss	%xmm4, 8(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm3         # 16-byte Reload
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
.LBB4_4:                                # %.split
	movss	.LCPI4_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm4
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm3, 32(%rsp)
	movlps	%xmm0, 40(%rsp)
	movq	(%r14), %rax
	leaq	32(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%r14, %rdi
	movaps	%xmm2, %xmm0
	callq	*32(%rax)
.LBB4_5:
	cmpb	$0, 16(%rbx)
	je	.LBB4_12
# BB#6:
	movq	24(%rbx), %rax
	cmpl	$0, 728(%rax)
	je	.LBB4_12
# BB#7:
	movq	8(%r14), %rdi
	cmpl	$0, 728(%rdi)
	je	.LBB4_12
# BB#8:
	movq	712(%rdi), %rax
	cmpq	144(%r14), %rax
	je	.LBB4_10
# BB#9:
	leaq	80(%r14), %rsi
	addq	$16, %r14
	jmp	.LBB4_11
.LBB4_10:
	leaq	16(%r14), %rsi
	addq	$80, %r14
.LBB4_11:                               # %_ZN16btManifoldResult20refreshContactPointsEv.exit
	movq	%r14, %rdx
	callq	_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_
.LBB4_12:
	addq	$104, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZN29btSphereBoxCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end4-_ZN29btSphereBoxCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_1:
	.long	1065353216              # float 1
.LCPI5_2:
	.long	1259902592              # float 1.0E+7
.LCPI5_3:
	.long	872415232               # float 1.1920929E-7
	.text
	.globl	_ZN29btSphereBoxCollisionAlgorithm17getSphereDistanceEP17btCollisionObjectR9btVector3S3_RKS2_f
	.p2align	4, 0x90
	.type	_ZN29btSphereBoxCollisionAlgorithm17getSphereDistanceEP17btCollisionObjectR9btVector3S3_RKS2_f,@function
_ZN29btSphereBoxCollisionAlgorithm17getSphereDistanceEP17btCollisionObjectR9btVector3S3_RKS2_f: # @_ZN29btSphereBoxCollisionAlgorithm17getSphereDistanceEP17btCollisionObjectR9btVector3S3_RKS2_f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi31:
	.cfi_def_cfa_offset 368
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movq	%r8, %r12
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	200(%rbx), %rdi
	movsd	40(%rdi), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm1
	movss	48(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	xorps	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	leaq	32(%rsp), %r13
	movups	40(%rdi), %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	(%rdi), %rax
	callq	*88(%rax)
	xorps	%xmm7, %xmm7
	movaps	%xmm0, %xmm8
	movaps	16(%rsp), %xmm0
	movaps	%xmm0, 96(%rsp)
	leaq	112(%rsp), %rbp
	movaps	32(%rsp), %xmm0
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm8, %xmm0
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	subss	%xmm1, %xmm0
	movss	%xmm0, 16(%rsp)
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	movaps	%xmm8, %xmm1
	subss	%xmm0, %xmm1
	movss	%xmm1, 20(%rsp)
	movaps	%xmm8, %xmm0
	subss	48(%rsp), %xmm0         # 16-byte Folded Reload
	movss	%xmm0, 24(%rsp)
	movss	32(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm0
	movss	%xmm0, 32(%rsp)
	movss	36(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm0
	movss	%xmm0, 36(%rsp)
	movss	40(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	%xmm8, %xmm0
	movss	%xmm0, 40(%rsp)
	movl	$-1082130432, 208(%rsp) # imm = 0xBF800000
	movups	%xmm7, 212(%rsp)
	movl	$-1082130432, 228(%rsp) # imm = 0xBF800000
	movups	%xmm7, 232(%rsp)
	movl	$-1082130432, 248(%rsp) # imm = 0xBF800000
	movl	$0, 252(%rsp)
	movl	$1065353216, 256(%rsp)  # imm = 0x3F800000
	movups	%xmm7, 260(%rsp)
	movl	$1065353216, 276(%rsp)  # imm = 0x3F800000
	movups	%xmm7, 280(%rsp)
	movq	$1065353216, 296(%rsp)  # imm = 0x3F800000
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	subss	56(%rbx), %xmm1
	subss	60(%rbx), %xmm0
	movss	8(%r12), %xmm10         # xmm10 = mem[0],zero,zero,zero
	subss	64(%rbx), %xmm10
	movsd	8(%rbx), %xmm2          # xmm2 = mem[0],zero
	movsd	24(%rbx), %xmm3         # xmm3 = mem[0],zero
	movsd	40(%rbx), %xmm4         # xmm4 = mem[0],zero
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm2, %xmm5
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	addps	%xmm5, %xmm2
	movaps	%xmm10, %xmm9
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm4, %xmm9
	addps	%xmm2, %xmm9
	mulss	16(%rbx), %xmm1
	mulss	32(%rbx), %xmm0
	addss	%xmm1, %xmm0
	mulss	48(%rbx), %xmm10
	addss	%xmm0, %xmm10
	movss	%xmm10, %xmm7           # xmm7 = xmm10[0],xmm7[1,2,3]
	leaq	216(%rsp), %rax
	xorl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	xorl	%edx, %edx
	movaps	%xmm9, %xmm6
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	cmpq	$2, %rcx
	setg	%sil
	shlq	$4, %rsi
	movss	16(%rsp,%rsi), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	subss	%xmm1, %xmm3
	movaps	%xmm6, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	subss	20(%rsp,%rsi), %xmm4
	movaps	%xmm7, %xmm1
	subss	24(%rsp,%rsi), %xmm1
	movaps	-8(%rax), %xmm2
	movaps	%xmm2, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm2, %xmm3
	mulss	%xmm4, %xmm5
	addss	%xmm3, %xmm5
	movss	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm5, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB5_3
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm2
	subps	%xmm2, %xmm6
	subss	%xmm3, %xmm7
	xorps	%xmm1, %xmm1
	movss	%xmm7, %xmm1            # xmm1 = xmm7[0],xmm1[1,2,3]
	movb	$1, %dl
	movaps	%xmm1, %xmm7
.LBB5_3:                                #   in Loop: Header=BB5_1 Depth=1
	incq	%rcx
	addq	$16, %rax
	cmpq	$6, %rcx
	jne	.LBB5_1
# BB#4:
	testb	$1, %dl
	jne	.LBB5_5
# BB#13:
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movq	%r13, (%rsp)
	leaq	16(%rsp), %r9
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	%r12, %r8
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	callq	_ZN29btSphereBoxCollisionAlgorithm20getSpherePenetrationEP17btCollisionObjectR9btVector3S3_RKS2_fS5_S5_
	movaps	96(%rsp), %xmm1
	movaps	%xmm1, 16(%rsp)
	movaps	(%rbp), %xmm1
	movaps	%xmm1, (%r13)
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jae	.LBB5_15
# BB#14:
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB5_16
.LBB5_5:
	movaps	96(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
	movaps	(%rbp), %xmm0
	movaps	%xmm0, (%r13)
	movaps	%xmm9, %xmm4
	subps	%xmm6, %xmm4
	movaps	%xmm4, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm10, %xmm12
	subss	%xmm7, %xmm12
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_7
# BB#6:                                 # %call.sqrt
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	movaps	%xmm10, 192(%rsp)       # 16-byte Spill
	movaps	%xmm9, 176(%rsp)        # 16-byte Spill
	movaps	%xmm6, 160(%rsp)        # 16-byte Spill
	movaps	%xmm4, 144(%rsp)        # 16-byte Spill
	movaps	%xmm12, 128(%rsp)       # 16-byte Spill
	callq	sqrtf
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movaps	144(%rsp), %xmm4        # 16-byte Reload
	movaps	160(%rsp), %xmm6        # 16-byte Reload
	movaps	176(%rsp), %xmm9        # 16-byte Reload
	movaps	192(%rsp), %xmm10       # 16-byte Reload
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB5_7:                                # %.split
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	divss	%xmm1, %xmm2
	mulss	%xmm2, %xmm12
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm2, %xmm4
	movaps	%xmm8, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	mulss	%xmm12, %xmm8
	addps	%xmm1, %xmm6
	addss	%xmm7, %xmm8
	xorps	%xmm11, %xmm11
	xorps	%xmm2, %xmm2
	movss	%xmm8, %xmm2            # xmm2 = xmm8[0],xmm2[1,2,3]
	movlps	%xmm6, (%r15)
	movlps	%xmm2, 8(%r15)
	movaps	%xmm12, %xmm2
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm3, %xmm2
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	subps	%xmm3, %xmm9
	subss	%xmm2, %xmm10
	movaps	%xmm9, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm10, %xmm2           # xmm2 = xmm10[0],xmm2[1,2,3]
	movlps	%xmm9, (%r14)
	movlps	%xmm2, 8(%r14)
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r15), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm9
	subss	%xmm2, %xmm5
	movss	8(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm10
	mulss	%xmm4, %xmm9
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm5, %xmm4
	addss	%xmm9, %xmm4
	mulss	%xmm12, %xmm10
	addss	%xmm4, %xmm10
	xorps	%xmm5, %xmm5
	ucomiss	%xmm5, %xmm10
	ja	.LBB5_16
# BB#8:
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	24(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm0, %xmm6
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	28(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	mulps	%xmm0, %xmm7
	addps	%xmm6, %xmm7
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	32(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm0, %xmm6
	addps	%xmm7, %xmm6
	movsd	56(%rbx), %xmm0         # xmm0 = mem[0],zero
	addps	%xmm6, %xmm0
	mulss	40(%rbx), %xmm1
	mulss	44(%rbx), %xmm2
	addss	%xmm1, %xmm2
	mulss	48(%rbx), %xmm3
	addss	%xmm2, %xmm3
	addss	64(%rbx), %xmm3
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movlps	%xmm0, (%r15)
	movlps	%xmm2, 8(%r15)
	movss	(%r14), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	12(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	movss	28(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movaps	%xmm2, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	addps	%xmm4, %xmm5
	movss	32(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	addps	%xmm5, %xmm4
	movsd	56(%rbx), %xmm5         # xmm5 = mem[0],zero
	addps	%xmm4, %xmm5
	mulss	40(%rbx), %xmm3
	mulss	44(%rbx), %xmm2
	addss	%xmm3, %xmm2
	mulss	48(%rbx), %xmm0
	addss	%xmm2, %xmm0
	addss	64(%rbx), %xmm0
	movss	%xmm0, %xmm11           # xmm11 = xmm0[0],xmm11[1,2,3]
	movlps	%xmm5, (%r14)
	movlps	%xmm11, 8(%r14)
	movss	(%r15), %xmm2           # xmm2 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm2
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movss	4(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm3
	movss	8(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	mulss	%xmm2, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	ucomiss	.LCPI5_3(%rip), %xmm1
	jbe	.LBB5_9
# BB#10:
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_12
# BB#11:                                # %call.sqrt335
	movaps	%xmm1, %xmm0
	callq	sqrtf
.LBB5_12:                               # %.split334
	xorps	.LCPI5_0(%rip), %xmm0
	jmp	.LBB5_16
.LBB5_15:
	subss	48(%rsp), %xmm0         # 16-byte Folded Reload
	jmp	.LBB5_16
.LBB5_9:
	movss	.LCPI5_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
.LBB5_16:
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN29btSphereBoxCollisionAlgorithm17getSphereDistanceEP17btCollisionObjectR9btVector3S3_RKS2_f, .Lfunc_end5-_ZN29btSphereBoxCollisionAlgorithm17getSphereDistanceEP17btCollisionObjectR9btVector3S3_RKS2_f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN29btSphereBoxCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.p2align	4, 0x90
	.type	_ZN29btSphereBoxCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult,@function
_ZN29btSphereBoxCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult: # @_ZN29btSphereBoxCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_startproc
# BB#0:
	movss	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end6:
	.size	_ZN29btSphereBoxCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult, .Lfunc_end6-_ZN29btSphereBoxCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	3407386240              # float -1.0E+7
.LCPI7_1:
	.long	1065353216              # float 1
.LCPI7_2:
	.long	0                       # float 0
	.text
	.globl	_ZN29btSphereBoxCollisionAlgorithm20getSpherePenetrationEP17btCollisionObjectR9btVector3S3_RKS2_fS5_S5_
	.p2align	4, 0x90
	.type	_ZN29btSphereBoxCollisionAlgorithm20getSpherePenetrationEP17btCollisionObjectR9btVector3S3_RKS2_fS5_S5_,@function
_ZN29btSphereBoxCollisionAlgorithm20getSpherePenetrationEP17btCollisionObjectR9btVector3S3_RKS2_fS5_S5_: # @_ZN29btSphereBoxCollisionAlgorithm20getSpherePenetrationEP17btCollisionObjectR9btVector3S3_RKS2_fS5_S5_
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 16
	movaps	%xmm0, %xmm15
	movq	16(%rsp), %rax
	movups	(%r9), %xmm0
	movaps	%xmm0, -128(%rsp)
	movups	(%rax), %xmm0
	movaps	%xmm0, -112(%rsp)
	movl	$-1082130432, -96(%rsp) # imm = 0xBF800000
	xorps	%xmm11, %xmm11
	movups	%xmm11, -92(%rsp)
	movl	$-1082130432, -76(%rsp) # imm = 0xBF800000
	movups	%xmm11, -72(%rsp)
	movl	$-1082130432, -56(%rsp) # imm = 0xBF800000
	movl	$0, -52(%rsp)
	movl	$1065353216, -48(%rsp)  # imm = 0x3F800000
	movups	%xmm11, -44(%rsp)
	movl	$1065353216, -28(%rsp)  # imm = 0x3F800000
	movups	%xmm11, -24(%rsp)
	movq	$1065353216, -8(%rsp)   # imm = 0x3F800000
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm0           # xmm0 = mem[0],zero,zero,zero
	subss	56(%rsi), %xmm3
	subss	60(%rsi), %xmm0
	movss	8(%r8), %xmm1           # xmm1 = mem[0],zero,zero,zero
	subss	64(%rsi), %xmm1
	movsd	8(%rsi), %xmm2          # xmm2 = mem[0],zero
	movsd	24(%rsi), %xmm4         # xmm4 = mem[0],zero
	movsd	40(%rsi), %xmm5         # xmm5 = mem[0],zero
	movaps	%xmm3, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm2, %xmm6
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm6, %xmm2
	movaps	%xmm1, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm5, %xmm8
	addps	%xmm2, %xmm8
	movaps	%xmm8, %xmm9
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	mulss	16(%rsi), %xmm3
	mulss	32(%rsi), %xmm0
	addss	%xmm3, %xmm0
	mulss	48(%rsi), %xmm1
	addss	%xmm0, %xmm1
	leaq	-88(%rsp), %rax
	movss	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	xorl	%r8d, %r8d
	xorps	%xmm12, %xmm12
	xorps	%xmm13, %xmm13
	xorps	%xmm14, %xmm14
	xorps	%xmm10, %xmm10
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	cmpq	$2, %r8
	setg	%dil
	shlq	$4, %rdi
	movss	-128(%rsp,%rdi), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movss	-124(%rsp,%rdi), %xmm5  # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm4
	subss	%xmm3, %xmm4
	movaps	%xmm9, %xmm6
	subss	%xmm5, %xmm6
	movss	-120(%rsp,%rdi), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	subss	%xmm7, %xmm2
	mulss	-8(%rax), %xmm4
	mulss	-4(%rax), %xmm6
	addss	%xmm4, %xmm6
	mulss	(%rax), %xmm2
	addss	%xmm6, %xmm2
	subss	%xmm15, %xmm2
	ucomiss	.LCPI7_2, %xmm2
	ja	.LBB7_2
# BB#3:                                 #   in Loop: Header=BB7_1 Depth=1
	ucomiss	%xmm0, %xmm2
	jbe	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_1 Depth=1
	movaps	-8(%rax), %xmm10
	movsd	(%rax), %xmm11          # xmm11 = mem[0],zero
	movaps	%xmm2, %xmm0
	movaps	%xmm3, %xmm14
	movaps	%xmm5, %xmm13
	movaps	%xmm7, %xmm12
.LBB7_5:                                #   in Loop: Header=BB7_1 Depth=1
	incq	%r8
	addq	$16, %rax
	cmpq	$6, %r8
	jl	.LBB7_1
# BB#6:
	movaps	%xmm8, %xmm2
	subss	%xmm14, %xmm2
	subss	%xmm13, %xmm9
	movaps	%xmm1, %xmm3
	subss	%xmm12, %xmm3
	mulss	%xmm10, %xmm2
	movaps	%xmm10, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm9, %xmm4
	addss	%xmm2, %xmm4
	mulss	%xmm11, %xmm3
	addss	%xmm4, %xmm3
	movaps	%xmm0, %xmm2
	mulss	%xmm11, %xmm2
	mulss	%xmm3, %xmm11
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm10, %xmm3
	subps	%xmm3, %xmm8
	subss	%xmm11, %xmm1
	xorps	%xmm5, %xmm5
	xorps	%xmm3, %xmm3
	movss	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1,2,3]
	movlps	%xmm8, (%rdx)
	movlps	%xmm3, 8(%rdx)
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm10, %xmm3
	addps	%xmm8, %xmm3
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm3, (%rcx)
	movlps	%xmm2, 8(%rcx)
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	24(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	12(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	movss	28(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	movaps	%xmm3, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm7, %xmm6
	addps	%xmm1, %xmm6
	movss	32(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1]
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm7, %xmm1
	addps	%xmm6, %xmm1
	movsd	56(%rsi), %xmm6         # xmm6 = mem[0],zero
	addps	%xmm1, %xmm6
	mulss	40(%rsi), %xmm4
	mulss	44(%rsi), %xmm3
	addss	%xmm4, %xmm3
	mulss	48(%rsi), %xmm2
	addss	%xmm3, %xmm2
	addss	64(%rsi), %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm6, (%rdx)
	movlps	%xmm1, 8(%rdx)
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	24(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	12(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	movss	28(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	movaps	%xmm3, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm7, %xmm6
	addps	%xmm1, %xmm6
	movss	32(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	16(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1]
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm7, %xmm1
	addps	%xmm6, %xmm1
	movsd	56(%rsi), %xmm6         # xmm6 = mem[0],zero
	addps	%xmm1, %xmm6
	mulss	40(%rsi), %xmm4
	mulss	44(%rsi), %xmm3
	addss	%xmm4, %xmm3
	mulss	48(%rsi), %xmm2
	addss	%xmm3, %xmm2
	addss	64(%rsi), %xmm2
	movss	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1,2,3]
	movlps	%xmm6, (%rcx)
	movlps	%xmm5, 8(%rcx)
	popq	%rax
	retq
.LBB7_2:
	movss	.LCPI7_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	popq	%rax
	retq
.Lfunc_end7:
	.size	_ZN29btSphereBoxCollisionAlgorithm20getSpherePenetrationEP17btCollisionObjectR9btVector3S3_RKS2_fS5_S5_, .Lfunc_end7-_ZN29btSphereBoxCollisionAlgorithm20getSpherePenetrationEP17btCollisionObjectR9btVector3S3_RKS2_fS5_S5_
	.cfi_endproc

	.section	.text._ZN29btSphereBoxCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,"axG",@progbits,_ZN29btSphereBoxCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,comdat
	.weak	_ZN29btSphereBoxCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.p2align	4, 0x90
	.type	_ZN29btSphereBoxCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE,@function
_ZN29btSphereBoxCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE: # @_ZN29btSphereBoxCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB8_18
# BB#1:
	cmpb	$0, 16(%r14)
	je	.LBB8_18
# BB#2:
	movl	4(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB8_17
# BB#3:
	leal	(%rax,%rax), %edx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%edx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB8_17
# BB#4:
	testl	%ebp, %ebp
	je	.LBB8_5
# BB#6:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB8_8
	jmp	.LBB8_12
.LBB8_5:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB8_12
.LBB8_8:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB8_10
	.p2align	4, 0x90
.LBB8_9:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB8_9
.LBB8_10:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB8_12
	.p2align	4, 0x90
.LBB8_11:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	16(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB8_11
.LBB8_12:                               # %_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_.exit.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_16
# BB#13:
	cmpb	$0, 24(%rbx)
	je	.LBB8_15
# BB#14:
	callq	_Z21btAlignedFreeInternalPv
	movl	4(%rbx), %eax
.LBB8_15:
	movq	$0, 16(%rbx)
.LBB8_16:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv.exit.i.i
	movb	$1, 24(%rbx)
	movq	%r15, 16(%rbx)
	movl	%ebp, 8(%rbx)
	movq	24(%r14), %rcx
.LBB8_17:                               # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_.exit
	movq	16(%rbx), %rdx
	cltq
	movq	%rcx, (%rdx,%rax,8)
	incl	%eax
	movl	%eax, 4(%rbx)
.LBB8_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN29btSphereBoxCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE, .Lfunc_end8-_ZN29btSphereBoxCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.cfi_endproc

	.type	_ZTV29btSphereBoxCollisionAlgorithm,@object # @_ZTV29btSphereBoxCollisionAlgorithm
	.section	.rodata,"a",@progbits
	.globl	_ZTV29btSphereBoxCollisionAlgorithm
	.p2align	3
_ZTV29btSphereBoxCollisionAlgorithm:
	.quad	0
	.quad	_ZTI29btSphereBoxCollisionAlgorithm
	.quad	_ZN29btSphereBoxCollisionAlgorithmD2Ev
	.quad	_ZN29btSphereBoxCollisionAlgorithmD0Ev
	.quad	_ZN29btSphereBoxCollisionAlgorithm16processCollisionEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN29btSphereBoxCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult
	.quad	_ZN29btSphereBoxCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE
	.size	_ZTV29btSphereBoxCollisionAlgorithm, 56

	.type	_ZTS29btSphereBoxCollisionAlgorithm,@object # @_ZTS29btSphereBoxCollisionAlgorithm
	.globl	_ZTS29btSphereBoxCollisionAlgorithm
	.p2align	4
_ZTS29btSphereBoxCollisionAlgorithm:
	.asciz	"29btSphereBoxCollisionAlgorithm"
	.size	_ZTS29btSphereBoxCollisionAlgorithm, 32

	.type	_ZTI29btSphereBoxCollisionAlgorithm,@object # @_ZTI29btSphereBoxCollisionAlgorithm
	.globl	_ZTI29btSphereBoxCollisionAlgorithm
	.p2align	4
_ZTI29btSphereBoxCollisionAlgorithm:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29btSphereBoxCollisionAlgorithm
	.quad	_ZTI30btActivatingCollisionAlgorithm
	.size	_ZTI29btSphereBoxCollisionAlgorithm, 24


	.globl	_ZN29btSphereBoxCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.type	_ZN29btSphereBoxCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b,@function
_ZN29btSphereBoxCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b = _ZN29btSphereBoxCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	.globl	_ZN29btSphereBoxCollisionAlgorithmD1Ev
	.type	_ZN29btSphereBoxCollisionAlgorithmD1Ev,@function
_ZN29btSphereBoxCollisionAlgorithmD1Ev = _ZN29btSphereBoxCollisionAlgorithmD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
