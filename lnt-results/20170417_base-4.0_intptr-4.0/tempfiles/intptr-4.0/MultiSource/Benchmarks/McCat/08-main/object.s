	.text
	.file	"object.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.text
	.globl	Oalloc
	.p2align	4, 0x90
	.type	Oalloc,@function
Oalloc:                                 # @Oalloc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$272, %edi              # imm = 0x110
	movl	$1, %esi
	callq	calloc
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	movups	%xmm0, 104(%rbx)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 120(%rbx)
	movq	$0, 128(%rbx)
	xorps	%xmm1, %xmm1
	movups	%xmm1, 160(%rbx)
	movq	$0, 176(%rbx)
	movups	%xmm1, 236(%rbx)
	movups	%xmm1, 224(%rbx)
	movups	%xmm1, 208(%rbx)
	movups	%xmm0, 184(%rbx)
	movq	%rax, 200(%rbx)
	movups	%xmm1, 136(%rbx)
	movq	$0, 152(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	Oalloc, .Lfunc_end0-Oalloc
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI1_1:
	.quad	-8775337516792518218    # x86_fp80 1.00000000000000000004E-6
	.short	16363
	.zero	6
	.text
	.globl	InsertPoint
	.p2align	4, 0x90
	.type	InsertPoint,@function
InsertPoint:                            # @InsertPoint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 96
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	228(%r14), %r15d
	testl	%r15d, %r15d
	je	.LBB1_1
# BB#2:
	movq	64(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_3
# BB#4:                                 # %.sink.split64.split.us.preheader.preheader
	movapd	.LCPI1_0(%rip), %xmm4   # xmm4 = [nan,nan]
	fldt	.LCPI1_1(%rip)
.LBB1_5:                                # %.sink.split64.split.us.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
	movq	%rax, %rbp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_6:                                # %.sink.split64.split.us
                                        #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rax, %rax
	jne	.LBB1_13
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=2
	movq	(%rbp), %rax
	movsd	(%rax), %xmm3           # xmm3 = mem[0],zero
	subsd	%xmm0, %xmm3
	andpd	%xmm4, %xmm3
	movlpd	%xmm3, 48(%rsp)
	fldl	48(%rsp)
	fxch	%st(1)
	fucomi	%st(1)
	fstp	%st(1)
	jbe	.LBB1_10
# BB#8:                                 #   in Loop: Header=BB1_6 Depth=2
	movsd	8(%rax), %xmm3          # xmm3 = mem[0],zero
	subsd	%xmm1, %xmm3
	andpd	%xmm4, %xmm3
	movlpd	%xmm3, 40(%rsp)
	fldl	40(%rsp)
	fxch	%st(1)
	fucomi	%st(1)
	fstp	%st(1)
	jbe	.LBB1_10
# BB#9:                                 #   in Loop: Header=BB1_6 Depth=2
	movsd	16(%rax), %xmm3         # xmm3 = mem[0],zero
	subsd	%xmm2, %xmm3
	andpd	%xmm4, %xmm3
	movlpd	%xmm3, 32(%rsp)
	fldl	32(%rsp)
	fxch	%st(1)
	fucomi	%st(1)
	fstp	%st(1)
	ja	.LBB1_6
.LBB1_10:                               # %.us-lcssa68.us
                                        #   in Loop: Header=BB1_5 Depth=1
	movq	8(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB1_5
	jmp	.LBB1_11
.LBB1_1:
	movl	$24, %edi
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 64(%r14)
	movl	$48, %edi
	callq	malloc
	movq	%rax, (%rbx)
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	%xmm2, (%rax)
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, 8(%rax)
	movq	(%rbx), %rax
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rax)
	movsd	%xmm2, 24(%rax)
	movsd	%xmm1, 32(%rax)
	movsd	%xmm0, 40(%rax)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 8(%rbx)
	jmp	.LBB1_12
.LBB1_3:
	xorl	%ebp, %ebp
	fldz
.LBB1_11:                               # %.us-lcssa.us
	fstp	%st(0)
	movl	$24, %edi
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, 8(%rbp)
	movl	$48, %edi
	callq	malloc
	movq	%rax, (%rbx)
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	%xmm2, (%rax)
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, 8(%rax)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rax)
	movsd	%xmm2, 24(%rax)
	movsd	%xmm1, 32(%rax)
	movsd	%xmm0, 40(%rax)
	movq	%rbp, 16(%rbx)
	movq	$0, 8(%rbx)
.LBB1_12:                               # %.sink.split
	incl	%r15d
	movl	%r15d, 228(%r14)
	fldz
.LBB1_13:                               # %.us-lcssa.us.thread
	fstp	%st(0)
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	InsertPoint, .Lfunc_end1-InsertPoint
	.cfi_endproc

	.globl	InsertPoly3
	.p2align	4, 0x90
	.type	InsertPoly3,@function
InsertPoly3:                            # @InsertPoly3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%r15)
	movups	%xmm0, 24(%r15)
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbx), %xmm2         # xmm2 = mem[0],zero
	movq	%r14, %rdi
	callq	InsertPoint
	movq	%rax, (%r15)
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rbx), %xmm2         # xmm2 = mem[0],zero
	movq	%r14, %rdi
	callq	InsertPoint
	movq	%rax, 8(%r15)
	movsd	48(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	56(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	64(%rbx), %xmm2         # xmm2 = mem[0],zero
	movq	%r14, %rdi
	callq	InsertPoint
	movq	%rax, 16(%r15)
	movq	72(%r14), %rax
	testq	%rax, %rax
	je	.LBB2_2
# BB#1:
	movq	%r15, 48(%rax)
	movq	%rax, 40(%r15)
.LBB2_2:
	movq	%r15, 72(%r14)
	incl	232(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	InsertPoly3, .Lfunc_end2-InsertPoly3
	.cfi_endproc

	.globl	InsertPoly4
	.p2align	4, 0x90
	.type	InsertPoly4,@function
InsertPoly4:                            # @InsertPoly4
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$64, %edi
	callq	malloc
	movq	%rax, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r15)
	movups	%xmm0, 32(%r15)
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbx), %xmm2         # xmm2 = mem[0],zero
	movq	%r14, %rdi
	callq	InsertPoint
	movq	%rax, (%r15)
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rbx), %xmm2         # xmm2 = mem[0],zero
	movq	%r14, %rdi
	callq	InsertPoint
	movq	%rax, 8(%r15)
	movsd	48(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	56(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	64(%rbx), %xmm2         # xmm2 = mem[0],zero
	movq	%r14, %rdi
	callq	InsertPoint
	movq	%rax, 16(%r15)
	movsd	72(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	80(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	88(%rbx), %xmm2         # xmm2 = mem[0],zero
	movq	%r14, %rdi
	callq	InsertPoint
	movq	%rax, 24(%r15)
	movq	80(%r14), %rax
	testq	%rax, %rax
	je	.LBB3_2
# BB#1:
	movq	%r15, 56(%rax)
	movq	%rax, 48(%r15)
.LBB3_2:
	movq	%r15, 80(%r14)
	incl	236(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	InsertPoly4, .Lfunc_end3-InsertPoly4
	.cfi_endproc

	.globl	ArrayToPoly3
	.p2align	4, 0x90
	.type	ArrayToPoly3,@function
ArrayToPoly3:                           # @ArrayToPoly3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
	subq	$80, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 128
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r13, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testl	%edx, %edx
	jle	.LBB4_5
# BB#1:                                 # %.lr.ph
	movslq	%edx, %r12
	addq	$64, %rbx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movsd	-64(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	-56(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	-48(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movsd	-40(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movsd	-32(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movsd	-24(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	-16(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	movsd	-8(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movl	$56, %edi
	callq	malloc
	movq	%rax, %r15
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 40(%r15)
	movupd	%xmm0, 24(%r15)
	movq	%r14, %rdi
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	callq	InsertPoint
	movq	%rax, (%r15)
	movq	%r14, %rdi
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	callq	InsertPoint
	movq	%rax, 8(%r15)
	movq	%r14, %rdi
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	56(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	callq	InsertPoint
	movq	%rax, 16(%r15)
	movq	72(%r14), %rax
	testq	%rax, %rax
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	%r15, 48(%rax)
	movq	%rax, 40(%r15)
.LBB4_4:                                # %InsertPoly3.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%r15, 72(%r14)
	incl	232(%r14)
	addq	$3, %r13
	addq	$72, %rbx
	cmpq	%r12, %r13
	jl	.LBB4_2
.LBB4_5:                                # %._crit_edge
	movq	%r14, %rax
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	ArrayToPoly3, .Lfunc_end4-ArrayToPoly3
	.cfi_endproc

	.globl	ArrayToPoly4
	.p2align	4, 0x90
	.type	ArrayToPoly4,@function
ArrayToPoly4:                           # @ArrayToPoly4
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 48
	subq	$96, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 144
.Lcfi43:
	.cfi_offset %rbx, -48
.Lcfi44:
	.cfi_offset %r12, -40
.Lcfi45:
	.cfi_offset %r13, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testl	%edx, %edx
	jle	.LBB5_3
# BB#1:                                 # %.lr.ph
	movslq	%edx, %r12
	addq	$80, %rbx
	xorl	%r13d, %r13d
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movups	-80(%rbx), %xmm0
	movaps	%xmm0, (%rsp)
	movups	-64(%rbx), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	-48(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	-32(%rbx), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	-16(%rbx), %xmm0
	movaps	%xmm0, 64(%rsp)
	movups	(%rbx), %xmm0
	movaps	%xmm0, 80(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	InsertPoly4
	addq	$4, %r13
	addq	$96, %rbx
	cmpq	%r12, %r13
	jl	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movq	%r14, %rax
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	ArrayToPoly4, .Lfunc_end5-ArrayToPoly4
	.cfi_endproc

	.globl	PrintPoints
	.p2align	4, 0x90
	.type	PrintPoints,@function
PrintPoints:                            # @PrintPoints
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	64(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB6_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str, %edi
	movb	$3, %al
	movl	%ebp, %esi
	callq	printf
	movq	(%rbx), %rax
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$3, %al
	callq	printf
	incl	%ebp
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_2
.LBB6_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	PrintPoints, .Lfunc_end6-PrintPoints
	.cfi_endproc

	.globl	PrintPoly3s
	.p2align	4, 0x90
	.type	PrintPoly3s,@function
PrintPoly3s:                            # @PrintPoly3s
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 16
.Lcfi54:
	.cfi_offset %rbx, -16
	movq	72(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB7_3
	.p2align	4, 0x90
.LBB7_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.2, %edi
	xorl	%esi, %esi
	movb	$3, %al
	callq	printf
	movq	(%rbx), %rax
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$3, %al
	callq	printf
	movq	8(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.2, %edi
	movl	$1, %esi
	movb	$3, %al
	callq	printf
	movq	8(%rbx), %rax
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$3, %al
	callq	printf
	movq	16(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.2, %edi
	movl	$2, %esi
	movb	$3, %al
	callq	printf
	movq	16(%rbx), %rax
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$3, %al
	callq	printf
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB7_1
.LBB7_3:                                # %.loopexit
	popq	%rbx
	retq
.Lfunc_end7:
	.size	PrintPoly3s, .Lfunc_end7-PrintPoly3s
	.cfi_endproc

	.globl	PrintPoly4s
	.p2align	4, 0x90
	.type	PrintPoly4s,@function
PrintPoly4s:                            # @PrintPoly4s
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 16
.Lcfi56:
	.cfi_offset %rbx, -16
	movq	80(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB8_3
	.p2align	4, 0x90
.LBB8_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.4, %edi
	xorl	%esi, %esi
	movb	$3, %al
	callq	printf
	movq	(%rbx), %rax
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$3, %al
	callq	printf
	movq	8(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.4, %edi
	movl	$1, %esi
	movb	$3, %al
	callq	printf
	movq	8(%rbx), %rax
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$3, %al
	callq	printf
	movq	16(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.4, %edi
	movl	$2, %esi
	movb	$3, %al
	callq	printf
	movq	16(%rbx), %rax
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$3, %al
	callq	printf
	movq	24(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.4, %edi
	movl	$3, %esi
	movb	$3, %al
	callq	printf
	movq	24(%rbx), %rax
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.3, %edi
	movb	$3, %al
	callq	printf
	movq	48(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_1
.LBB8_3:                                # %.loopexit
	popq	%rbx
	retq
.Lfunc_end8:
	.size	PrintPoly4s, .Lfunc_end8-PrintPoly4s
	.cfi_endproc

	.globl	PrintObject
	.p2align	4, 0x90
	.type	PrintObject,@function
PrintObject:                            # @PrintObject
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 32
.Lcfi60:
	.cfi_offset %rbx, -32
.Lcfi61:
	.cfi_offset %r14, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	xorl	%ebp, %ebp
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB9_3
	.p2align	4, 0x90
.LBB9_1:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str, %edi
	movb	$3, %al
	movl	%ebp, %esi
	callq	printf
	movq	(%rbx), %rax
	movsd	24(%rax), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%rax), %xmm1         # xmm1 = mem[0],zero
	movsd	40(%rax), %xmm2         # xmm2 = mem[0],zero
	movl	$.L.str.1, %edi
	movb	$3, %al
	callq	printf
	incl	%ebp
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_1
.LBB9_3:                                # %PrintPoints.exit
	movq	%r14, %rdi
	callq	PrintPoly3s
	movq	%r14, %rdi
	callq	PrintPoly4s
	movsd	184(%r14), %xmm0        # xmm0 = mem[0],zero
	movsd	192(%r14), %xmm1        # xmm1 = mem[0],zero
	movsd	200(%r14), %xmm2        # xmm2 = mem[0],zero
	movl	$.L.str.6, %edi
	movb	$3, %al
	callq	printf
	movsd	136(%r14), %xmm0        # xmm0 = mem[0],zero
	movsd	144(%r14), %xmm1        # xmm1 = mem[0],zero
	movsd	152(%r14), %xmm2        # xmm2 = mem[0],zero
	movl	$.L.str.7, %edi
	movb	$3, %al
	callq	printf
	movsd	160(%r14), %xmm0        # xmm0 = mem[0],zero
	movsd	168(%r14), %xmm1        # xmm1 = mem[0],zero
	movsd	176(%r14), %xmm2        # xmm2 = mem[0],zero
	movl	$.L.str.8, %edi
	movb	$3, %al
	callq	printf
	movsd	104(%r14), %xmm0        # xmm0 = mem[0],zero
	movsd	112(%r14), %xmm1        # xmm1 = mem[0],zero
	movsd	120(%r14), %xmm2        # xmm2 = mem[0],zero
	movl	$.L.str.9, %edi
	movb	$3, %al
	callq	printf
	movl	132(%r14), %eax
	testl	%eax, %eax
	jne	.LBB9_5
# BB#4:
	movl	$.Lstr.2, %edi
	callq	puts
	movl	132(%r14), %eax
.LBB9_5:
	cmpl	$1, %eax
	jne	.LBB9_7
# BB#6:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	132(%r14), %eax
.LBB9_7:
	cmpl	$2, %eax
	jne	.LBB9_8
# BB#9:
	movl	$.Lstr, %edi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	puts                    # TAILCALL
.LBB9_8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end9:
	.size	PrintObject, .Lfunc_end9-PrintObject
	.cfi_endproc

	.globl	InsertChild
	.p2align	4, 0x90
	.type	InsertChild,@function
InsertChild:                            # @InsertChild
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB10_4
# BB#1:
	movq	208(%rdi), %rax
	testq	%rax, %rax
	je	.LBB10_3
# BB#2:
	movq	%rsi, 256(%rax)
	movq	%rax, 264(%rsi)
	movq	$0, 256(%rsi)
.LBB10_3:                               # %.sink.split
	movq	%rsi, 208(%rdi)
.LBB10_4:
	retq
.Lfunc_end10:
	.size	InsertChild, .Lfunc_end10-InsertChild
	.cfi_endproc

	.globl	CalcObjectChildren
	.p2align	4, 0x90
	.type	CalcObjectChildren,@function
CalcObjectChildren:                     # @CalcObjectChildren
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 48
	subq	$176, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 224
.Lcfi69:
	.cfi_offset %rbx, -48
.Lcfi70:
	.cfi_offset %r12, -40
.Lcfi71:
	.cfi_offset %r13, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %r15, -16
	movsd	%xmm7, 104(%rsp)        # 8-byte Spill
	movsd	%xmm6, 96(%rsp)         # 8-byte Spill
	movsd	%xmm5, 88(%rsp)         # 8-byte Spill
	movsd	%xmm4, 80(%rsp)         # 8-byte Spill
	movsd	%xmm3, 72(%rsp)         # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB11_13
# BB#1:
	movq	208(%rdi), %r14
	testq	%r14, %r14
	je	.LBB11_13
# BB#2:                                 # %.lr.ph
	leaq	144(%rsp), %r15
	leaq	112(%rsp), %r12
	movsd	%xmm2, 64(%rsp)         # 8-byte Spill
	movsd	%xmm1, 56(%rsp)         # 8-byte Spill
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB11_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_4 Depth 2
                                        #     Child Loop BB11_7 Depth 2
                                        #     Child Loop BB11_10 Depth 2
	callq	ScaleMatrix
	movq	%rax, %r13
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB11_6
	.p2align	4, 0x90
.LBB11_4:                               # %.lr.ph.i
                                        #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movupd	32(%rax), %xmm2
	movupd	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	callq	TPointToHPoint
	movups	144(%rsp), %xmm0
	movupd	160(%rsp), %xmm1
	movupd	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	MultMatrixHPoint
	movapd	112(%rsp), %xmm0
	movq	128(%rsp), %rax
	movq	(%rbx), %rcx
	movupd	%xmm0, 24(%rcx)
	movq	%rax, 40(%rcx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_4
.LBB11_6:                               # %.loopexit94
                                        #   in Loop: Header=BB11_3 Depth=1
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	callq	RotateMatrix
	movq	%rax, %r13
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB11_9
	.p2align	4, 0x90
.LBB11_7:                               # %.lr.ph.i28
                                        #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	callq	TPointToHPoint
	movups	144(%rsp), %xmm0
	movups	160(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	MultMatrixHPoint
	movaps	112(%rsp), %xmm0
	movq	128(%rsp), %rax
	movq	(%rbx), %rcx
	movups	%xmm0, 24(%rcx)
	movq	%rax, 40(%rcx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_7
.LBB11_9:                               # %.loopexit
                                        #   in Loop: Header=BB11_3 Depth=1
	movsd	96(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	104(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	224(%rsp), %xmm2        # xmm2 = mem[0],zero
	callq	TranslateMatrix
	movq	%rax, %r13
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB11_12
	.p2align	4, 0x90
.LBB11_10:                              # %.lr.ph.i32
                                        #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	callq	TPointToHPoint
	movups	144(%rsp), %xmm0
	movups	160(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	MultMatrixHPoint
	movaps	112(%rsp), %xmm0
	movq	128(%rsp), %rax
	movq	(%rbx), %rcx
	movups	%xmm0, 24(%rcx)
	movq	%rax, 40(%rcx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_10
.LBB11_12:                              # %TranslateObjectAdd.exit
                                        #   in Loop: Header=BB11_3 Depth=1
	movsd	224(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)
	movq	%r14, %rdi
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	56(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	64(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	72(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	80(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	88(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	96(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	104(%rsp), %xmm7        # 8-byte Reload
                                        # xmm7 = mem[0],zero
	callq	CalcObjectChildren
	movsd	48(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	56(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	64(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	256(%r14), %r14
	testq	%r14, %r14
	jne	.LBB11_3
.LBB11_13:                              # %.loopexit95
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	CalcObjectChildren, .Lfunc_end11-CalcObjectChildren
	.cfi_endproc

	.globl	ScaleObjectAdd
	.p2align	4, 0x90
	.type	ScaleObjectAdd,@function
ScaleObjectAdd:                         # @ScaleObjectAdd
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 192
.Lcfi79:
	.cfi_offset %rbx, -40
.Lcfi80:
	.cfi_offset %r12, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	callq	ScaleMatrix
	movq	%rax, %r12
	testq	%r14, %r14
	je	.LBB12_4
# BB#1:
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB12_5
# BB#2:                                 # %.lr.ph
	leaq	88(%rsp), %r14
	leaq	120(%rsp), %r15
	.p2align	4, 0x90
.LBB12_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	TPointToHPoint
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	MultMatrixHPoint
	movups	120(%rsp), %xmm0
	movups	136(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	(%rbx), %rax
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 24(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 40(%rax)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB12_3
.LBB12_5:                               # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB12_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB12_5
.Lfunc_end12:
	.size	ScaleObjectAdd, .Lfunc_end12-ScaleObjectAdd
	.cfi_endproc

	.globl	RotateObjectAdd
	.p2align	4, 0x90
	.type	RotateObjectAdd,@function
RotateObjectAdd:                        # @RotateObjectAdd
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi87:
	.cfi_def_cfa_offset 192
.Lcfi88:
	.cfi_offset %rbx, -40
.Lcfi89:
	.cfi_offset %r12, -32
.Lcfi90:
	.cfi_offset %r14, -24
.Lcfi91:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB13_4
# BB#1:
	callq	RotateMatrix
	movq	%rax, %r12
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB13_5
# BB#2:                                 # %.lr.ph
	leaq	88(%rsp), %r14
	leaq	120(%rsp), %r15
	.p2align	4, 0x90
.LBB13_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	TPointToHPoint
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	MultMatrixHPoint
	movups	120(%rsp), %xmm0
	movups	136(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	(%rbx), %rax
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 24(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 40(%rax)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_3
.LBB13_5:                               # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB13_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB13_5
.Lfunc_end13:
	.size	RotateObjectAdd, .Lfunc_end13-RotateObjectAdd
	.cfi_endproc

	.globl	TranslateObjectAdd
	.p2align	4, 0x90
	.type	TranslateObjectAdd,@function
TranslateObjectAdd:                     # @TranslateObjectAdd
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi96:
	.cfi_def_cfa_offset 192
.Lcfi97:
	.cfi_offset %rbx, -40
.Lcfi98:
	.cfi_offset %r12, -32
.Lcfi99:
	.cfi_offset %r14, -24
.Lcfi100:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB14_4
# BB#1:
	callq	TranslateMatrix
	movq	%rax, %r12
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB14_5
# BB#2:                                 # %.lr.ph
	leaq	88(%rsp), %r14
	leaq	120(%rsp), %r15
	.p2align	4, 0x90
.LBB14_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	TPointToHPoint
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	MultMatrixHPoint
	movups	120(%rsp), %xmm0
	movups	136(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	(%rbx), %rax
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 24(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 40(%rax)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_3
.LBB14_5:                               # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB14_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB14_5
.Lfunc_end14:
	.size	TranslateObjectAdd, .Lfunc_end14-TranslateObjectAdd
	.cfi_endproc

	.globl	CalcObject
	.p2align	4, 0x90
	.type	CalcObject,@function
CalcObject:                             # @CalcObject
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 48
	subq	$112, %rsp
.Lcfi106:
	.cfi_def_cfa_offset 160
.Lcfi107:
	.cfi_offset %rbx, -48
.Lcfi108:
	.cfi_offset %r12, -40
.Lcfi109:
	.cfi_offset %r13, -32
.Lcfi110:
	.cfi_offset %r14, -24
.Lcfi111:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB15_13
# BB#1:
	movq	208(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB15_4
	.p2align	4, 0x90
.LBB15_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	CalcObject
	movq	256(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_2
.LBB15_4:                               # %._crit_edge
	movsd	184(%r14), %xmm0        # xmm0 = mem[0],zero
	movsd	192(%r14), %xmm1        # xmm1 = mem[0],zero
	movsd	200(%r14), %xmm2        # xmm2 = mem[0],zero
	movsd	160(%r14), %xmm3        # xmm3 = mem[0],zero
	movsd	168(%r14), %xmm4        # xmm4 = mem[0],zero
	movsd	176(%r14), %xmm5        # xmm5 = mem[0],zero
	movsd	136(%r14), %xmm6        # xmm6 = mem[0],zero
	movsd	144(%r14), %xmm7        # xmm7 = mem[0],zero
	movsd	152(%r14), %xmm8        # xmm8 = mem[0],zero
	movsd	%xmm8, (%rsp)
	movq	%r14, %rdi
	callq	CalcObjectChildren
	movsd	184(%r14), %xmm0        # xmm0 = mem[0],zero
	movsd	192(%r14), %xmm1        # xmm1 = mem[0],zero
	movsd	200(%r14), %xmm2        # xmm2 = mem[0],zero
	callq	ScaleMatrix
	movq	%rax, %r13
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB15_7
# BB#5:                                 # %.lr.ph.i29.preheader
	leaq	80(%rsp), %r15
	leaq	48(%rsp), %r12
	.p2align	4, 0x90
.LBB15_6:                               # %.lr.ph.i29
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	callq	PointToHPoint
	movups	80(%rsp), %xmm0
	movups	96(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	MultMatrixHPoint
	movaps	48(%rsp), %xmm0
	movq	64(%rsp), %rax
	movq	(%rbx), %rcx
	movups	%xmm0, 24(%rcx)
	movq	%rax, 40(%rcx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_6
.LBB15_7:                               # %.loopexit95
	movsd	160(%r14), %xmm0        # xmm0 = mem[0],zero
	movsd	168(%r14), %xmm1        # xmm1 = mem[0],zero
	movsd	176(%r14), %xmm2        # xmm2 = mem[0],zero
	callq	RotateMatrix
	movq	%rax, %r13
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB15_10
# BB#8:                                 # %.lr.ph.i33.preheader
	leaq	80(%rsp), %r15
	leaq	48(%rsp), %r12
	.p2align	4, 0x90
.LBB15_9:                               # %.lr.ph.i33
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	callq	TPointToHPoint
	movups	80(%rsp), %xmm0
	movups	96(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	MultMatrixHPoint
	movaps	48(%rsp), %xmm0
	movq	64(%rsp), %rax
	movq	(%rbx), %rcx
	movups	%xmm0, 24(%rcx)
	movq	%rax, 40(%rcx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_9
.LBB15_10:                              # %.loopexit
	movsd	136(%r14), %xmm0        # xmm0 = mem[0],zero
	movsd	144(%r14), %xmm1        # xmm1 = mem[0],zero
	movsd	152(%r14), %xmm2        # xmm2 = mem[0],zero
	callq	TranslateMatrix
	movq	%rax, %r12
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB15_13
# BB#11:                                # %.lr.ph.i.preheader
	leaq	80(%rsp), %r14
	leaq	48(%rsp), %r15
	.p2align	4, 0x90
.LBB15_12:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	TPointToHPoint
	movups	80(%rsp), %xmm0
	movups	96(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	MultMatrixHPoint
	movaps	48(%rsp), %xmm0
	movq	64(%rsp), %rax
	movq	(%rbx), %rcx
	movups	%xmm0, 24(%rcx)
	movq	%rax, 40(%rcx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_12
.LBB15_13:
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	CalcObject, .Lfunc_end15-CalcObject
	.cfi_endproc

	.globl	ScaleObjectOverwrite
	.p2align	4, 0x90
	.type	ScaleObjectOverwrite,@function
ScaleObjectOverwrite:                   # @ScaleObjectOverwrite
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi116:
	.cfi_def_cfa_offset 192
.Lcfi117:
	.cfi_offset %rbx, -40
.Lcfi118:
	.cfi_offset %r12, -32
.Lcfi119:
	.cfi_offset %r14, -24
.Lcfi120:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	callq	ScaleMatrix
	movq	%rax, %r12
	testq	%r14, %r14
	je	.LBB16_4
# BB#1:
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB16_5
# BB#2:                                 # %.lr.ph
	leaq	88(%rsp), %r14
	leaq	120(%rsp), %r15
	.p2align	4, 0x90
.LBB16_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	PointToHPoint
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	MultMatrixHPoint
	movups	120(%rsp), %xmm0
	movups	136(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	(%rbx), %rax
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 24(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 40(%rax)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB16_3
.LBB16_5:                               # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB16_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB16_5
.Lfunc_end16:
	.size	ScaleObjectOverwrite, .Lfunc_end16-ScaleObjectOverwrite
	.cfi_endproc

	.globl	TranslateObjectOverwrite
	.p2align	4, 0x90
	.type	TranslateObjectOverwrite,@function
TranslateObjectOverwrite:               # @TranslateObjectOverwrite
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi125:
	.cfi_def_cfa_offset 192
.Lcfi126:
	.cfi_offset %rbx, -40
.Lcfi127:
	.cfi_offset %r12, -32
.Lcfi128:
	.cfi_offset %r14, -24
.Lcfi129:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB17_4
# BB#1:
	callq	TranslateMatrix
	movq	%rax, %r12
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB17_5
# BB#2:                                 # %.lr.ph
	leaq	88(%rsp), %r14
	leaq	120(%rsp), %r15
	.p2align	4, 0x90
.LBB17_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	PointToHPoint
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	MultMatrixHPoint
	movups	120(%rsp), %xmm0
	movups	136(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	(%rbx), %rax
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 24(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 40(%rax)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB17_3
.LBB17_5:                               # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB17_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB17_5
.Lfunc_end17:
	.size	TranslateObjectOverwrite, .Lfunc_end17-TranslateObjectOverwrite
	.cfi_endproc

	.globl	RotateObjectOverwrite
	.p2align	4, 0x90
	.type	RotateObjectOverwrite,@function
RotateObjectOverwrite:                  # @RotateObjectOverwrite
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi134:
	.cfi_def_cfa_offset 192
.Lcfi135:
	.cfi_offset %rbx, -40
.Lcfi136:
	.cfi_offset %r12, -32
.Lcfi137:
	.cfi_offset %r14, -24
.Lcfi138:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	callq	RotateMatrix
	movq	%rax, %r12
	testq	%r14, %r14
	je	.LBB18_4
# BB#1:
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB18_5
# BB#2:                                 # %.lr.ph
	leaq	88(%rsp), %r14
	leaq	120(%rsp), %r15
	.p2align	4, 0x90
.LBB18_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	32(%rax), %xmm2
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	callq	PointToHPoint
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movups	88(%rsp), %xmm0
	movups	104(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	MultMatrixHPoint
	movups	120(%rsp), %xmm0
	movups	136(%rsp), %xmm1
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	(%rbx), %rax
	movaps	48(%rsp), %xmm0
	movups	%xmm0, 24(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 40(%rax)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB18_3
.LBB18_5:                               # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB18_4:
	movq	stderr(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB18_5
.Lfunc_end18:
	.size	RotateObjectOverwrite, .Lfunc_end18-RotateObjectOverwrite
	.cfi_endproc

	.globl	SetObjectColor
	.p2align	4, 0x90
	.type	SetObjectColor,@function
SetObjectColor:                         # @SetObjectColor
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB19_2
# BB#1:
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 104(%rdi)
.LBB19_2:
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	cvtps2pd	%xmm1, %xmm0
	movups	%xmm0, 112(%rdi)
	retq
.Lfunc_end19:
	.size	SetObjectColor, .Lfunc_end19-SetObjectColor
	.cfi_endproc

	.globl	Draw_Children
	.p2align	4, 0x90
	.type	Draw_Children,@function
Draw_Children:                          # @Draw_Children
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 16
.Lcfi140:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB20_4
# BB#1:
	movq	%rbx, %rdi
	callq	PrintObject
	movq	208(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB20_4
	.p2align	4, 0x90
.LBB20_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	Draw_Children
	movq	256(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB20_2
.LBB20_4:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end20:
	.size	Draw_Children, .Lfunc_end20-Draw_Children
	.cfi_endproc

	.globl	Draw_Object
	.p2align	4, 0x90
	.type	Draw_Object,@function
Draw_Object:                            # @Draw_Object
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 16
.Lcfi142:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB21_1
# BB#2:
	movq	%rbx, %rdi
	callq	PrintObject
	movq	208(%rbx), %rdi
	popq	%rbx
	jmp	Draw_Children           # TAILCALL
.LBB21_1:
	popq	%rbx
	retq
.Lfunc_end21:
	.size	Draw_Object, .Lfunc_end21-Draw_Object
	.cfi_endproc

	.globl	Draw_All_Nexts
	.p2align	4, 0x90
	.type	Draw_All_Nexts,@function
Draw_All_Nexts:                         # @Draw_All_Nexts
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 16
.Lcfi144:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB22_4
# BB#1:                                 # %.preheader
	movq	256(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB22_4
	.p2align	4, 0x90
.LBB22_2:                               # %Draw_Object.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	CalcObject
	movq	%rbx, %rdi
	callq	PrintObject
	movq	208(%rbx), %rdi
	callq	Draw_Children
	movq	256(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_2
.LBB22_4:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end22:
	.size	Draw_All_Nexts, .Lfunc_end22-Draw_All_Nexts
	.cfi_endproc

	.globl	Draw_All_Prevs
	.p2align	4, 0x90
	.type	Draw_All_Prevs,@function
Draw_All_Prevs:                         # @Draw_All_Prevs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 16
.Lcfi146:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB23_4
# BB#1:                                 # %.preheader
	movq	264(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB23_4
	.p2align	4, 0x90
.LBB23_2:                               # %Draw_Object.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	CalcObject
	movq	%rbx, %rdi
	callq	PrintObject
	movq	208(%rbx), %rdi
	callq	Draw_Children
	movq	264(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_2
.LBB23_4:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end23:
	.size	Draw_All_Prevs, .Lfunc_end23-Draw_All_Prevs
	.cfi_endproc

	.globl	Draw_All
	.p2align	4, 0x90
	.type	Draw_All,@function
Draw_All:                               # @Draw_All
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi149:
	.cfi_def_cfa_offset 32
.Lcfi150:
	.cfi_offset %rbx, -24
.Lcfi151:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB24_7
# BB#1:                                 # %.preheader.i
	movq	%r14, %rdi
	callq	CalcObject
	movq	264(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB24_4
	.p2align	4, 0x90
.LBB24_2:                               # %Draw_Object.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	CalcObject
	movq	%rbx, %rdi
	callq	PrintObject
	movq	208(%rbx), %rdi
	callq	Draw_Children
	movq	264(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_2
.LBB24_4:                               # %.preheader.i7
	movq	%r14, %rdi
	callq	PrintObject
	movq	208(%r14), %rdi
	callq	Draw_Children
	movq	256(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB24_7
	.p2align	4, 0x90
.LBB24_5:                               # %Draw_Object.exit.i11
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	CalcObject
	movq	%rbx, %rdi
	callq	PrintObject
	movq	208(%rbx), %rdi
	callq	Draw_Children
	movq	256(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB24_5
.LBB24_7:                               # %Draw_All_Nexts.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end24:
	.size	Draw_All, .Lfunc_end24-Draw_All
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Point[%i] = (%.2f, %.2f, %.2f)"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" -> (%.2f, %.2f, %.2f)\n"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"PrintPoly3s[%i] = (%.2f, %.2f, %.2f)"
	.size	.L.str.2, 37

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"-> (%.2f, %.2f, %.2f)\n"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"PrintPoly4s[%i] = %.2f, %.2f, %.2f"
	.size	.L.str.4, 35

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Name: %s\n"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Scale    : (%.2f,%.2f,%.2f)\n"
	.size	.L.str.6, 29

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Origin   : (%.2f,%.2f,%.2f)\n"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Rotation : (%.2f,%.2f,%.2f)\n"
	.size	.L.str.8, 29

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Color    : (%.2f,%.2f,%.2f)\n"
	.size	.L.str.9, 29

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Cannot Translate NULL-object\n"
	.size	.L.str.13, 30

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Cannot Rotate NULL-object\n"
	.size	.L.str.14, 27

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Cannot Scale NULL-object\n"
	.size	.L.str.15, 26

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Style     : NONE"
	.size	.Lstr, 17

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"Style     : HALF"
	.size	.Lstr.1, 17

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Style     : FULL"
	.size	.Lstr.2, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
