	.text
	.file	"TarHandler.bc"
	.globl	_ZN8NArchive4NTar8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive4NTar8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive4NTar8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$9, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN8NArchive4NTar8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end0-_ZN8NArchive4NTar8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NTar8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive4NTar8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive4NTar8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$8, %esi
	ja	.LBB1_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive4NTarL6kPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive4NTarL6kPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB1_2:
	retq
.Lfunc_end1:
	.size	_ZN8NArchive4NTar8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end1-_ZN8NArchive4NTar8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive4NTar8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive4NTar8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive4NTar8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$2, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN8NArchive4NTar8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end2-_ZN8NArchive4NTar8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive4NTar8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive4NTar8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive4NTar8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$1, %esi
	ja	.LBB3_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive4NTarL9kArcPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive4NTarL9kArcPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB3_2:
	retq
.Lfunc_end3:
	.size	_ZN8NArchive4NTar8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end3-_ZN8NArchive4NTar8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive4NTar8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive4NTar8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive4NTar8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	movl	$0, (%rsp)
	cmpl	$55, %esi
	je	.LBB4_7
# BB#1:
	cmpl	$45, %esi
	je	.LBB4_5
# BB#2:
	cmpl	$44, %esi
	jne	.LBB4_9
# BB#3:
	cmpb	$0, 240(%rdi)
	je	.LBB4_9
# BB#4:
	movq	224(%rdi), %rsi
.Ltmp4:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp5:
	jmp	.LBB4_9
.LBB4_5:
	cmpb	$0, 240(%rdi)
	je	.LBB4_9
# BB#6:
	movq	232(%rdi), %rsi
.Ltmp2:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp3:
	jmp	.LBB4_9
.LBB4_7:
	cmpl	$0, 256(%rdi)
	je	.LBB4_9
# BB#8:
	movq	248(%rdi), %rsi
.Ltmp0:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp1:
.LBB4_9:
.Ltmp6:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp7:
# BB#10:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB4_11:
.Ltmp8:
	movq	%rax, %rbx
.Ltmp9:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp10:
# BB#12:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_13:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive4NTar8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end4-_ZN8NArchive4NTar8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp4           #   Call between .Ltmp4 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end5:
	.size	__clang_call_terminate, .Lfunc_end5-__clang_call_terminate

	.text
	.globl	_ZN8NArchive4NTar8CHandler9ReadItem2EP19ISequentialInStreamRbRNS0_7CItemExE
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler9ReadItem2EP19ISequentialInStreamRbRNS0_7CItemExE,@function
_ZN8NArchive4NTar8CHandler9ReadItem2EP19ISequentialInStreamRbRNS0_7CItemExE: # @_ZN8NArchive4NTar8CHandler9ReadItem2EP19ISequentialInStreamRbRNS0_7CItemExE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -24
.Lcfi7:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	movq	224(%rbx), %rax
	movq	%rax, 112(%r14)
	leaq	248(%rbx), %rcx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%r14, %rdx
	callq	_ZN8NArchive4NTar8ReadItemEP19ISequentialInStreamRbRNS0_7CItemExER11CStringBaseIcE
	testl	%eax, %eax
	jne	.LBB6_2
# BB#1:
	movl	120(%r14), %eax
	movdqu	224(%rbx), %xmm0
	movd	%rax, %xmm1
	pshufd	$68, %xmm1, %xmm1       # xmm1 = xmm1[0,1,0,1]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 224(%rbx)
	xorl	%eax, %eax
.LBB6_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	_ZN8NArchive4NTar8CHandler9ReadItem2EP19ISequentialInStreamRbRNS0_7CItemExE, .Lfunc_end6-_ZN8NArchive4NTar8CHandler9ReadItem2EP19ISequentialInStreamRbRNS0_7CItemExE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.byte	85                      # 0x55
	.byte	110                     # 0x6e
	.byte	101                     # 0x65
	.byte	120                     # 0x78
	.byte	112                     # 0x70
	.byte	101                     # 0x65
	.byte	99                      # 0x63
	.byte	116                     # 0x74
	.byte	101                     # 0x65
	.byte	100                     # 0x64
	.byte	32                      # 0x20
	.byte	101                     # 0x65
	.byte	110                     # 0x6e
	.byte	100                     # 0x64
	.byte	32                      # 0x20
	.byte	111                     # 0x6f
	.text
	.globl	_ZN8NArchive4NTar8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback,@function
_ZN8NArchive4NTar8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback: # @_ZN8NArchive4NTar8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 272
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	$0, 48(%rsp)
	movq	(%r12), %rax
	leaq	48(%rsp), %rcx
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r12, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB7_66
# BB#1:
	movq	(%r12), %rax
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB7_66
# BB#2:
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movb	$1, 240(%rbx)
	leaq	224(%rbx), %r13
	leaq	248(%rbx), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	leaq	40(%rbx), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	jmp	.LBB7_11
.LBB7_3:                                # %vector.body.preheader
                                        #   in Loop: Header=BB7_11 Depth=1
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB7_6
# BB#4:                                 # %vector.body.prol.preheader
                                        #   in Loop: Header=BB7_11 Depth=1
	negq	%rsi
	xorl	%edx, %edx
.LBB7_5:                                # %vector.body.prol
                                        #   Parent Loop BB7_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rdx), %xmm0
	movdqu	16(%rdi,%rdx), %xmm1
	movdqu	%xmm0, (%r15,%rdx)
	movdqu	%xmm1, 16(%r15,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB7_5
	jmp	.LBB7_7
.LBB7_6:                                #   in Loop: Header=BB7_11 Depth=1
	xorl	%edx, %edx
.LBB7_7:                                # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB7_11 Depth=1
	cmpq	$96, %r8
	jb	.LBB7_10
# BB#8:                                 # %vector.body.preheader.new
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%r15,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB7_9:                                # %vector.body
                                        #   Parent Loop BB7_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rdx), %xmm0
	movdqu	(%rdx), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB7_9
.LBB7_10:                               # %middle.block
                                        #   in Loop: Header=BB7_11 Depth=1
	cmpq	%rcx, %rbp
	jne	.LBB7_40
	jmp	.LBB7_50
	.p2align	4, 0x90
.LBB7_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
                                        #     Child Loop BB7_9 Depth 2
                                        #     Child Loop BB7_42 Depth 2
                                        #     Child Loop BB7_45 Depth 2
	leaq	88(%rsp), %rbx
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NTar5CItemC2Ev
	movq	(%r13), %rax
	movq	%rax, 200(%rsp)
.Ltmp12:
	movq	%r12, %rdi
	leaq	56(%rsp), %rsi
	movq	%rbx, %rdx
	movq	72(%rsp), %rcx          # 8-byte Reload
	callq	_ZN8NArchive4NTar8ReadItemEP19ISequentialInStreamRbRNS0_7CItemExER11CStringBaseIcE
	movl	%eax, %ebx
.Ltmp13:
# BB#12:                                # %.noexc
                                        #   in Loop: Header=BB7_11 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_14
# BB#13:                                #   in Loop: Header=BB7_11 Depth=1
	movl	$1, %r14d
	jmp	.LBB7_53
	.p2align	4, 0x90
.LBB7_14:                               #   in Loop: Header=BB7_11 Depth=1
	movl	208(%rsp), %eax
	movdqu	(%r13), %xmm0
	movd	%rax, %xmm1
	pshufd	$68, %xmm1, %xmm1       # xmm1 = xmm1[0,1,0,1]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, (%r13)
	movl	$2, %r14d
	cmpb	$0, 56(%rsp)
	je	.LBB7_31
# BB#15:                                #   in Loop: Header=BB7_11 Depth=1
.Ltmp14:
	movl	$128, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp15:
# BB#16:                                # %.noexc83
                                        #   in Loop: Header=BB7_11 Depth=1
.Ltmp16:
	movq	%rbp, %rdi
	leaq	88(%rsp), %rsi
	callq	_ZN8NArchive4NTar5CItemC2ERKS1_
.Ltmp17:
# BB#17:                                #   in Loop: Header=BB7_11 Depth=1
	leaq	200(%rsp), %rax
	movq	%rax, %rcx
	movl	8(%rcx), %eax
	movl	%eax, 120(%rbp)
	movq	(%rcx), %rax
	movq	%rax, 112(%rbp)
.Ltmp19:
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp20:
# BB#18:                                #   in Loop: Header=BB7_11 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	56(%rdx), %rax
	movslq	52(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 52(%rdx)
	movq	(%r12), %rax
	movq	104(%rsp), %rsi
	movl	$511, %ecx              # imm = 0x1FF
	addq	%rcx, %rsi
	andq	$-512, %rsi             # imm = 0xFE00
.Ltmp21:
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%r13, %rcx
	callq	*48(%rax)
.Ltmp22:
# BB#19:                                #   in Loop: Header=BB7_11 Depth=1
	testl	%eax, %eax
	movl	%eax, %ebx
	cmovel	%r15d, %ebx
	je	.LBB7_21
# BB#20:                                #   in Loop: Header=BB7_11 Depth=1
	movl	$1, %r14d
	movl	%eax, %ebx
	jmp	.LBB7_53
.LBB7_21:                               #   in Loop: Header=BB7_11 Depth=1
	movq	(%r13), %rax
	cmpq	48(%rsp), %rax
	jbe	.LBB7_24
# BB#22:                                # %_Z11MyStringLenIcEiPKT_.exit.i
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	$0, 256(%rcx)
	movq	248(%rcx), %rax
	movb	$0, (%rax)
	movl	260(%rcx), %ebp
	cmpl	$26, %ebp
	jne	.LBB7_32
# BB#23:                                # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	movq	8(%rsp), %rcx           # 8-byte Reload
	jmp	.LBB7_52
.LBB7_24:                               #   in Loop: Header=BB7_11 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB7_30
# BB#25:                                #   in Loop: Header=BB7_11 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	52(%rax), %eax
	cmpl	$1, %eax
	jne	.LBB7_29
# BB#26:                                #   in Loop: Header=BB7_11 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp23:
	xorl	%esi, %esi
	leaq	48(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %ebx
.Ltmp24:
# BB#27:                                #   in Loop: Header=BB7_11 Depth=1
	testl	%ebx, %ebx
	movl	$1, %r14d
	jne	.LBB7_53
# BB#28:                                # %._crit_edge
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	52(%rax), %eax
.LBB7_29:                               #   in Loop: Header=BB7_11 Depth=1
	cltq
	imulq	$1374389535, %rax, %rcx # imm = 0x51EB851F
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$37, %rcx
	addl	%edx, %ecx
	imull	$100, %ecx, %ecx
	cmpl	%ecx, %eax
	je	.LBB7_46
.LBB7_30:                               #   in Loop: Header=BB7_11 Depth=1
	xorl	%r14d, %r14d
.LBB7_31:                               # %_ZN8NArchive4NTar8CHandler9ReadItem2EP19ISequentialInStreamRbRNS0_7CItemExE.exit
                                        #   in Loop: Header=BB7_11 Depth=1
	movl	%r15d, %ebx
	jmp	.LBB7_53
.LBB7_32:                               #   in Loop: Header=BB7_11 Depth=1
.Ltmp28:
	movl	$26, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp29:
# BB#33:                                # %.noexc85
                                        #   in Loop: Header=BB7_11 Depth=1
	testl	%ebp, %ebp
	jle	.LBB7_51
# BB#34:                                # %.preheader.i.i
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	256(%rax), %rbp
	testq	%rbp, %rbp
	movq	248(%rax), %rdi
	jle	.LBB7_49
# BB#35:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB7_11 Depth=1
	cmpl	$31, %ebp
	jbe	.LBB7_39
# BB#36:                                # %min.iters.checked
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB7_39
# BB#37:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_11 Depth=1
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %r15
	jae	.LBB7_3
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB7_11 Depth=1
	leaq	(%r15,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB7_3
.LBB7_39:                               #   in Loop: Header=BB7_11 Depth=1
	xorl	%ecx, %ecx
.LBB7_40:                               # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB7_11 Depth=1
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %r8
	subq	%rcx, %r8
	andq	$7, %rsi
	je	.LBB7_43
# BB#41:                                # %.lr.ph.i.i.prol.preheader
                                        #   in Loop: Header=BB7_11 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB7_42:                               # %.lr.ph.i.i.prol
                                        #   Parent Loop BB7_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r15,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB7_42
.LBB7_43:                               # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB7_11 Depth=1
	cmpq	$7, %r8
	jb	.LBB7_50
# BB#44:                                # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB7_11 Depth=1
	subq	%rcx, %rbp
	leaq	7(%r15,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB7_45:                               # %.lr.ph.i.i
                                        #   Parent Loop BB7_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB7_45
	jmp	.LBB7_50
.LBB7_46:                               #   in Loop: Header=BB7_11 Depth=1
	movq	%rax, 16(%rsp)
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp25:
	leaq	16(%rsp), %rsi
	movq	%r13, %rdx
	callq	*48(%rax)
	movl	%eax, %ebx
.Ltmp26:
# BB#47:                                #   in Loop: Header=BB7_11 Depth=1
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	setne	%al
	je	.LBB7_30
# BB#48:                                #   in Loop: Header=BB7_11 Depth=1
	movb	%al, %r14b
	jmp	.LBB7_53
.LBB7_49:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB7_11 Depth=1
	testq	%rdi, %rdi
	je	.LBB7_51
.LBB7_50:                               # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB7_11 Depth=1
	callq	_ZdaPv
.LBB7_51:                               # %._crit_edge17.i.i
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r15, 248(%rcx)
	movslq	256(%rcx), %rax
	movb	$0, (%r15,%rax)
	movl	$26, 260(%rcx)
.LBB7_52:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.preheader
                                        #   in Loop: Header=BB7_11 Depth=1
	movdqa	.LCPI7_0(%rip), %xmm0   # xmm0 = [85,110,101,120,112,101,99,116,101,100,32,101,110,100,32,111]
	movdqu	%xmm0, (%r15)
	movb	$102, 16(%r15)
	movb	$32, 17(%r15)
	movb	$97, 18(%r15)
	movb	$114, 19(%r15)
	movb	$99, 20(%r15)
	movb	$104, 21(%r15)
	movb	$105, 22(%r15)
	movb	$118, 23(%r15)
	movb	$101, 24(%r15)
	movb	$0, 25(%r15)
	movl	$25, 256(%rcx)
	.p2align	4, 0x90
.LBB7_53:                               # %_ZN8NArchive4NTar8CHandler9ReadItem2EP19ISequentialInStreamRbRNS0_7CItemExE.exit
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	168(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_55
# BB#54:                                #   in Loop: Header=BB7_11 Depth=1
	callq	_ZdaPv
.LBB7_55:                               # %_ZN11CStringBaseIcED2Ev.exit.i
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	152(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_57
# BB#56:                                #   in Loop: Header=BB7_11 Depth=1
	callq	_ZdaPv
.LBB7_57:                               # %_ZN11CStringBaseIcED2Ev.exit1.i
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_59
# BB#58:                                #   in Loop: Header=BB7_11 Depth=1
	callq	_ZdaPv
.LBB7_59:                               # %_ZN11CStringBaseIcED2Ev.exit2.i
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_61
# BB#60:                                #   in Loop: Header=BB7_11 Depth=1
	callq	_ZdaPv
.LBB7_61:                               # %_ZN8NArchive4NTar5CItemD2Ev.exit
                                        #   in Loop: Header=BB7_11 Depth=1
	andb	$3, %r14b
	movl	%ebx, %r15d
	je	.LBB7_11
# BB#62:                                # %_ZN8NArchive4NTar5CItemD2Ev.exit
	cmpb	$2, %r14b
	jne	.LBB7_65
# BB#63:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 52(%rax)
	movq	32(%rsp), %rdi          # 8-byte Reload
	je	.LBB7_67
.LBB7_64:
	xorl	%ebp, %ebp
	jmp	.LBB7_66
.LBB7_65:                               # %.loopexit.loopexit
	movl	%ebx, %ebp
.LBB7_66:                               # %.loopexit
	movl	%ebp, %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_67:
	testq	%rdi, %rdi
	movq	$0, 40(%rsp)
	je	.LBB7_93
# BB#68:
	movq	(%rdi), %rax
.Ltmp31:
	leaq	40(%rsp), %rdx
	movl	$IID_IArchiveOpenVolumeCallback, %esi
	callq	*(%rax)
.Ltmp32:
# BB#69:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_93
# BB#70:
	movl	$0, 88(%rsp)
	movq	(%rdi), %rax
.Ltmp34:
	leaq	88(%rsp), %rdx
	movl	$4, %esi
	callq	*40(%rax)
.Ltmp35:
# BB#71:
	movl	$1, %r14d
	testl	%eax, %eax
	jne	.LBB7_94
# BB#72:
	movzwl	88(%rsp), %eax
	cmpl	$8, %eax
	movl	$1, %ebp
	jne	.LBB7_95
# BB#73:
	movq	96(%rsp), %rbp
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 16(%rsp)
	xorl	%r14d, %r14d
	movq	%rbp, %rax
	xorl	%r15d, %r15d
.LBB7_74:                               # =>This Inner Loop Header: Depth=1
	incl	%r15d
	decl	%r14d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB7_74
# BB#75:                                # %_Z11MyStringLenIwEiPKT_.exit.i
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp36:
	callq	_Znam
.Ltmp37:
# BB#76:                                # %.noexc94
	leal	-1(%r15), %edx
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	%r15d, 28(%rsp)
.LBB7_77:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB7_77
# BB#78:
	movl	%edx, 24(%rsp)
	cmpl	$4, %edx
	movl	$4, %ecx
	cmovll	%edx, %ecx
	cmpl	$-6, %r14d
	movl	$-5, %edx
	cmovgl	%r14d, %edx
	addl	%r15d, %edx
.Ltmp39:
	leaq	56(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp40:
# BB#79:                                # %_ZNK11CStringBaseIwE5RightEi.exit
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbp
	movl	$0, (%rbp)
	movslq	64(%rsp), %r14
	incq	%r14
	movl	28(%rsp), %r12d
	cmpl	%r12d, %r14d
	je	.LBB7_85
# BB#80:
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp42:
	callq	_Znam
	movq	%rax, %r15
.Ltmp43:
# BB#81:                                # %.noexc101
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB7_84
# BB#82:                                # %.noexc101
	testl	%r12d, %r12d
	jle	.LBB7_84
# BB#83:                                # %._crit_edge.thread.i.i97
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
.LBB7_84:                               # %._crit_edge16.i.i
	movq	%r15, 16(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%r14d, 28(%rsp)
	movq	%r15, %rbp
.LBB7_85:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i98
	movq	56(%rsp), %rdi
	xorl	%eax, %eax
.LBB7_86:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB7_86
# BB#87:
	movl	64(%rsp), %eax
	movl	%eax, 24(%rsp)
	testq	%rdi, %rdi
	je	.LBB7_89
# BB#88:
	callq	_ZdaPv
	movq	16(%rsp), %rbp
.LBB7_89:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp45:
	movl	$.L.str, %esi
	movq	%rbp, %rdi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp46:
# BB#90:                                # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r15b
	movl	$1, %ebp
	cmovel	%ebx, %ebp
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_92
# BB#91:
	callq	_ZdaPv
.LBB7_92:                               # %_ZN11CStringBaseIwED2Ev.exit103
	movb	%r15b, %r14b
	jmp	.LBB7_95
.LBB7_93:                               # %_ZN9CMyComPtrI26IArchiveOpenVolumeCallbackED2Ev.exit.thread
	movl	$1, %ebp
	jmp	.LBB7_66
.LBB7_94:
	movl	$1, %ebp
.LBB7_95:
.Ltmp50:
	leaq	88(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp51:
# BB#96:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_98
# BB#97:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB7_98:                               # %_ZN9CMyComPtrI26IArchiveOpenVolumeCallbackED2Ev.exit
	testl	%r14d, %r14d
	jne	.LBB7_66
	jmp	.LBB7_64
.LBB7_99:
.Ltmp44:
	movq	%rax, %rbx
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_104
# BB#100:
	callq	_ZdaPv
	jmp	.LBB7_104
.LBB7_101:
.Ltmp47:
	jmp	.LBB7_103
.LBB7_102:
.Ltmp41:
.LBB7_103:
	movq	%rax, %rbx
.LBB7_104:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_108
# BB#105:
	callq	_ZdaPv
	jmp	.LBB7_108
.LBB7_106:
.Ltmp52:
	jmp	.LBB7_110
.LBB7_107:
.Ltmp38:
	movq	%rax, %rbx
.LBB7_108:
.Ltmp48:
	leaq	88(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp49:
	jmp	.LBB7_111
.LBB7_109:
.Ltmp33:
.LBB7_110:
	movq	%rax, %rbx
.LBB7_111:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_126
# BB#112:
	movq	(%rdi), %rax
.Ltmp53:
	callq	*16(%rax)
.Ltmp54:
	jmp	.LBB7_126
.LBB7_113:
.Ltmp55:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_114:
.Ltmp27:
	jmp	.LBB7_117
.LBB7_115:
.Ltmp18:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB7_118
.LBB7_116:
.Ltmp30:
.LBB7_117:
	movq	%rax, %rbx
.LBB7_118:
	movq	168(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_120
# BB#119:
	callq	_ZdaPv
.LBB7_120:                              # %_ZN11CStringBaseIcED2Ev.exit.i86
	movq	152(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_122
# BB#121:
	callq	_ZdaPv
.LBB7_122:                              # %_ZN11CStringBaseIcED2Ev.exit1.i87
	movq	136(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_124
# BB#123:
	callq	_ZdaPv
.LBB7_124:                              # %_ZN11CStringBaseIcED2Ev.exit2.i88
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_126
# BB#125:
	callq	_ZdaPv
.LBB7_126:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN8NArchive4NTar8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback, .Lfunc_end7-_ZN8NArchive4NTar8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp12-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp15-.Ltmp12         #   Call between .Ltmp12 and .Ltmp15
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp29-.Ltmp19         #   Call between .Ltmp19 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp37-.Ltmp34         #   Call between .Ltmp34 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin1   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin1   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin1   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin1   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin1   #     jumps to .Ltmp52
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp48-.Ltmp51         #   Call between .Ltmp51 and .Ltmp48
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp54-.Ltmp48         #   Call between .Ltmp48 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin1   #     jumps to .Ltmp55
	.byte	1                       #   On action: 1
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Lfunc_end7-.Ltmp54     #   Call between .Ltmp54 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NTar8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive4NTar8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive4NTar8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
.Ltmp56:
	callq	*48(%rax)
.Ltmp57:
# BB#1:
.Ltmp58:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZN8NArchive4NTar8CHandler5Open2EP9IInStreamP20IArchiveOpenCallback
.Ltmp59:
# BB#2:
	testl	%eax, %eax
	jne	.LBB8_12
# BB#3:
	testq	%r14, %r14
	je	.LBB8_5
# BB#4:
	movq	(%r14), %rax
.Ltmp60:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp61:
.LBB8_5:                                # %.noexc
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp62:
	callq	*16(%rax)
.Ltmp63:
.LBB8_7:                                # %_ZN9CMyComPtrI9IInStreamEaSEPS0_.exit
	movq	%r14, 72(%rbx)
	xorl	%eax, %eax
.LBB8_12:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB8_8:
.Ltmp64:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %r14
	cmpl	$2, %ebx
	je	.LBB8_9
# BB#11:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB8_12
.LBB8_9:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%r14, (%rax)
.Ltmp65:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp66:
# BB#13:
.LBB8_10:
.Ltmp67:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8NArchive4NTar8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end8-_ZN8NArchive4NTar8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\302\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp56-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp63-.Ltmp56         #   Call between .Ltmp56 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin2   #     jumps to .Ltmp64
	.byte	3                       #   On action: 2
	.long	.Ltmp63-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp65-.Ltmp63         #   Call between .Ltmp63 and .Ltmp65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin2   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp66     #   Call between .Ltmp66 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZN8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream: # @_ZN8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -24
.Lcfi31:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*48(%rax)
	testq	%r14, %r14
	je	.LBB9_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB9_2:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB9_4:                                # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%r14, 80(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end9-_ZN8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn8_N8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZThn8_N8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream: # @_ZThn8_N8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	-8(%rbx), %rax
	addq	$-8, %rbx
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%r14, %r14
	je	.LBB10_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB10_2:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB10_4:                               # %_ZN8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream.exit
	movq	%r14, 80(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZThn8_N8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end10-_ZThn8_N8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN8NArchive4NTar8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler5CloseEv,@function
_ZN8NArchive4NTar8CHandler5CloseEv:     # @_ZN8NArchive4NTar8CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
.Lcfi38:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$0, 256(%rbx)
	movq	248(%rbx), %rax
	movb	$0, (%rax)
	movl	$0, 88(%rbx)
	movb	$0, 92(%rbx)
	leaq	40(%rbx), %rdi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 224(%rbx)
	movb	$0, 240(%rbx)
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 80(%rbx)
.LBB11_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 72(%rbx)
.LBB11_4:                               # %_ZN9CMyComPtrI9IInStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZN8NArchive4NTar8CHandler5CloseEv, .Lfunc_end11-_ZN8NArchive4NTar8CHandler5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive4NTar8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive4NTar8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive4NTar8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	cmpq	$0, 72(%rdi)
	je	.LBB12_1
# BB#2:
	movl	52(%rdi), %eax
	jmp	.LBB12_3
.LBB12_1:
	movl	$-1, %eax
.LBB12_3:
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	_ZN8NArchive4NTar8CHandler16GetNumberOfItemsEPj, .Lfunc_end12-_ZN8NArchive4NTar8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NTar8CHandlerC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandlerC2Ev,@function
_ZN8NArchive4NTar8CHandlerC2Ev:         # @_ZN8NArchive4NTar8CHandlerC2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -48
.Lcfi45:
	.cfi_offset %r12, -40
.Lcfi46:
	.cfi_offset %r13, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	$0, 32(%r12)
	movl	$_ZTVN8NArchive4NTar8CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NTar8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r12)
	movl	$_ZTVN8NArchive4NTar8CHandlerE+312, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NTar8CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%r12)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 48(%r12)
	movq	$8, 64(%r12)
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE+16, 40(%r12)
	leaq	96(%r12), %r14
	movdqu	%xmm0, 72(%r12)
.Ltmp68:
	movq	%r14, %rdi
	callq	_ZN8NArchive4NTar5CItemC2Ev
.Ltmp69:
# BB#1:                                 # %_ZN8NArchive4NTar7CItemExC2Ev.exit
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 248(%r12)
.Ltmp71:
	movl	$4, %edi
	callq	_Znam
.Ltmp72:
# BB#2:
	leaq	248(%r12), %r13
	movq	%rax, 248(%r12)
	movb	$0, (%rax)
	movl	$4, 260(%r12)
	movq	$0, 272(%r12)
.Ltmp74:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp75:
# BB#3:
	movl	$0, 16(%rbx)
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
	movq	%rbx, 264(%r12)
.Ltmp76:
	movq	%rbx, %rdi
	callq	*_ZTVN9NCompress10CCopyCoderE+24(%rip)
.Ltmp77:
# BB#4:                                 # %.noexc9
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB13_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp78:
	callq	*16(%rax)
.Ltmp79:
.LBB13_6:
	movq	%rbx, 272(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB13_8:
.Ltmp73:
	movq	%rax, %r15
	jmp	.LBB13_13
.LBB13_7:
.Ltmp70:
	movq	%rax, %r15
	jmp	.LBB13_21
.LBB13_9:
.Ltmp80:
	movq	%rax, %r15
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB13_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp81:
	callq	*16(%rax)
.Ltmp82:
.LBB13_11:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB13_13
# BB#12:
	callq	_ZdaPv
.LBB13_13:                              # %_ZN11CStringBaseIcED2Ev.exit
	movq	176(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB13_15
# BB#14:
	callq	_ZdaPv
.LBB13_15:                              # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	160(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB13_17
# BB#16:
	callq	_ZdaPv
.LBB13_17:                              # %_ZN11CStringBaseIcED2Ev.exit1.i
	movq	144(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB13_19
# BB#18:
	callq	_ZdaPv
.LBB13_19:                              # %_ZN11CStringBaseIcED2Ev.exit2.i
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB13_21
# BB#20:
	callq	_ZdaPv
.LBB13_21:                              # %_ZN8NArchive4NTar5CItemD2Ev.exit
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB13_23
# BB#22:
	movq	(%rdi), %rax
.Ltmp83:
	callq	*16(%rax)
.Ltmp84:
.LBB13_23:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	leaq	72(%r12), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB13_25
# BB#24:
	movq	(%rdi), %rax
.Ltmp85:
	callq	*16(%rax)
.Ltmp86:
.LBB13_25:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	addq	$40, %r12
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE+16, (%r12)
.Ltmp87:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp88:
# BB#26:
.Ltmp93:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp94:
# BB#27:                                # %_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB13_28:
.Ltmp89:
	movq	%rax, %rbx
.Ltmp90:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp91:
	jmp	.LBB13_31
.LBB13_29:
.Ltmp92:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB13_30:
.Ltmp95:
	movq	%rax, %rbx
.LBB13_31:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN8NArchive4NTar8CHandlerC2Ev, .Lfunc_end13-_ZN8NArchive4NTar8CHandlerC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp68-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin3   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin3   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp79-.Ltmp74         #   Call between .Ltmp74 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin3   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp86-.Ltmp81         #   Call between .Ltmp81 and .Ltmp86
	.long	.Ltmp95-.Lfunc_begin3   #     jumps to .Ltmp95
	.byte	1                       #   On action: 1
	.long	.Ltmp87-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin3   #     jumps to .Ltmp89
	.byte	1                       #   On action: 1
	.long	.Ltmp93-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin3   #     jumps to .Ltmp95
	.byte	1                       #   On action: 1
	.long	.Ltmp94-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp90-.Ltmp94         #   Call between .Ltmp94 and .Ltmp90
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin3   #     jumps to .Ltmp92
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE+16, (%rbx)
.Ltmp96:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp97:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB14_2:
.Ltmp98:
	movq	%rax, %r14
.Ltmp99:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp100:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_4:
.Ltmp101:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev, .Lfunc_end14-_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp96-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin4   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp99-.Ltmp97         #   Call between .Ltmp97 and .Ltmp99
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin4  #     jumps to .Ltmp101
	.byte	1                       #   On action: 1
	.long	.Ltmp100-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Lfunc_end14-.Ltmp100   #   Call between .Ltmp100 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.byte	85                      # 0x55
	.byte	110                     # 0x6e
	.byte	101                     # 0x65
	.byte	120                     # 0x78
	.byte	112                     # 0x70
	.byte	101                     # 0x65
	.byte	99                      # 0x63
	.byte	116                     # 0x74
	.byte	101                     # 0x65
	.byte	100                     # 0x64
	.byte	32                      # 0x20
	.byte	101                     # 0x65
	.byte	110                     # 0x6e
	.byte	100                     # 0x64
	.byte	32                      # 0x20
	.byte	111                     # 0x6f
	.text
	.globl	_ZN8NArchive4NTar8CHandler6SkipToEj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler6SkipToEj,@function
_ZN8NArchive4NTar8CHandler6SkipToEj:    # @_ZN8NArchive4NTar8CHandler6SkipToEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 80
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leaq	96(%rbx), %r14
	leaq	248(%rbx), %r15
	leaq	16(%rsp), %r12
	leaq	15(%rsp), %r13
	jmp	.LBB15_1
	.p2align	4, 0x90
.LBB15_39:                              # %.thread27
                                        #   in Loop: Header=BB15_1 Depth=1
	movb	$1, 92(%rbx)
.LBB15_1:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, 88(%rbx)
	movzbl	92(%rbx), %eax
	jae	.LBB15_2
# BB#4:                                 # %.critedge
                                        #   in Loop: Header=BB15_1 Depth=1
	testb	%al, %al
	je	.LBB15_35
# BB#5:                                 #   in Loop: Header=BB15_1 Depth=1
	movq	112(%rbx), %rax
	movl	$511, %ecx              # imm = 0x1FF
	addq	%rcx, %rax
	andq	$-512, %rax             # imm = 0xFE00
	movq	%rax, 16(%rsp)
	movq	80(%rbx), %rsi
	movq	264(%rbx), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	movq	%r12, %r8
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB15_38
# BB#6:                                 #   in Loop: Header=BB15_1 Depth=1
	movq	264(%rbx), %rax
	movq	32(%rax), %rax
	addq	%rax, 224(%rbx)
	cmpq	16(%rsp), %rax
	jne	.LBB15_7
# BB#34:                                # %.thread
                                        #   in Loop: Header=BB15_1 Depth=1
	movb	$0, 92(%rbx)
	incl	88(%rbx)
	jmp	.LBB15_1
	.p2align	4, 0x90
.LBB15_2:                               #   in Loop: Header=BB15_1 Depth=1
	testb	%al, %al
	jne	.LBB15_3
.LBB15_35:                              # %.critedge.thread
                                        #   in Loop: Header=BB15_1 Depth=1
	movq	80(%rbx), %rdi
	movq	224(%rbx), %rax
	movq	%rax, 208(%rbx)
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	_ZN8NArchive4NTar8ReadItemEP19ISequentialInStreamRbRNS0_7CItemExER11CStringBaseIcE
	testl	%eax, %eax
	jne	.LBB15_38
# BB#36:                                #   in Loop: Header=BB15_1 Depth=1
	movl	216(%rbx), %eax
	movdqu	224(%rbx), %xmm0
	movd	%rax, %xmm1
	pshufd	$68, %xmm1, %xmm1       # xmm1 = xmm1[0,1,0,1]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 224(%rbx)
	cmpb	$0, 15(%rsp)
	jne	.LBB15_39
# BB#37:
	movb	$1, 240(%rbx)
	movl	$-2147024809, %eax      # imm = 0x80070057
	jmp	.LBB15_38
.LBB15_3:
	xorl	%eax, %eax
	jmp	.LBB15_38
.LBB15_7:                               # %_Z11MyStringLenIcEiPKT_.exit.i
	movl	$0, 256(%rbx)
	movq	248(%rbx), %rax
	movb	$0, (%rax)
	movl	260(%rbx), %ebp
	cmpl	$26, %ebp
	jne	.LBB15_9
# BB#8:                                 # %_Z11MyStringLenIcEiPKT_.exit._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	248(%rbx), %r14
	jmp	.LBB15_33
.LBB15_9:
	movl	$26, %edi
	callq	_Znam
	movq	%rax, %r14
	testl	%ebp, %ebp
	jle	.LBB15_32
# BB#10:                                # %.preheader.i.i
	movslq	256(%rbx), %r8
	testq	%r8, %r8
	movq	248(%rbx), %rdi
	jle	.LBB15_30
# BB#11:                                # %.lr.ph.preheader.i.i
	cmpl	$31, %r8d
	jbe	.LBB15_12
# BB#19:                                # %min.iters.checked
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB15_12
# BB#20:                                # %vector.memcheck
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %r14
	jae	.LBB15_22
# BB#21:                                # %vector.memcheck
	leaq	(%r14,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB15_22
.LBB15_12:
	xorl	%ecx, %ecx
.LBB15_13:                              # %.lr.ph.i.i.preheader
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB15_16
# BB#14:                                # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB15_15:                              # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r14,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB15_15
.LBB15_16:                              # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rbp
	jb	.LBB15_31
# BB#17:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %r8
	leaq	7(%r14,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB15_18:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB15_18
	jmp	.LBB15_31
.LBB15_30:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB15_32
.LBB15_31:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB15_32:                              # %._crit_edge17.i.i
	movq	%r14, 248(%rbx)
	movslq	256(%rbx), %rax
	movb	$0, (%r14,%rax)
	movl	$26, 260(%rbx)
.LBB15_33:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.preheader
	movdqa	.LCPI15_0(%rip), %xmm0  # xmm0 = [85,110,101,120,112,101,99,116,101,100,32,101,110,100,32,111]
	movdqu	%xmm0, (%r14)
	movb	$102, 16(%r14)
	movb	$32, 17(%r14)
	movb	$97, 18(%r14)
	movb	$114, 19(%r14)
	movb	$99, 20(%r14)
	movb	$104, 21(%r14)
	movb	$105, 22(%r14)
	movb	$118, 23(%r14)
	movb	$101, 24(%r14)
	movb	$0, 25(%r14)
	movl	$25, 256(%rbx)
	movl	$1, %eax
.LBB15_38:                              # %.loopexit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_22:                              # %vector.body.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB15_23
# BB#24:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
.LBB15_25:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp), %xmm0
	movdqu	16(%rdi,%rbp), %xmm1
	movups	%xmm0, (%r14,%rbp)
	movdqu	%xmm1, 16(%r14,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB15_25
	jmp	.LBB15_26
.LBB15_23:
	xorl	%ebp, %ebp
.LBB15_26:                              # %vector.body.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB15_29
# BB#27:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r14,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
.LBB15_28:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB15_28
.LBB15_29:                              # %middle.block
	cmpq	%rcx, %r8
	jne	.LBB15_13
	jmp	.LBB15_31
.Lfunc_end15:
	.size	_ZN8NArchive4NTar8CHandler6SkipToEj, .Lfunc_end15-_ZN8NArchive4NTar8CHandler6SkipToEj
	.cfi_endproc

	.globl	_ZN8NArchive4NTar8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive4NTar8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive4NTar8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 40
	subq	$104, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 144
.Lcfi72:
	.cfi_offset %rbx, -40
.Lcfi73:
	.cfi_offset %r14, -32
.Lcfi74:
	.cfi_offset %r15, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebx
	movq	%rdi, %r15
	movl	$0, 8(%rsp)
	cmpq	$0, 72(%r15)
	je	.LBB16_11
# BB#1:
	movq	56(%r15), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %r15
.LBB16_2:
	addl	$-3, %ebx
	cmpl	$51, %ebx
	ja	.LBB16_44
# BB#3:
	jmpq	*.LJTI16_0(,%rbx,8)
.LBB16_4:
.Ltmp134:
	leaq	24(%rsp), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp135:
# BB#5:                                 # %_ZN8NArchive4NTarL18TarStringToUnicodeERK11CStringBaseIcE.exit
.Ltmp137:
	leaq	88(%rsp), %rdi
	leaq	24(%rsp), %rsi
	callq	_ZN8NArchive9NItemName10GetOSName2ERK11CStringBaseIwE
.Ltmp138:
# BB#6:
	movq	88(%rsp), %rsi
.Ltmp140:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp141:
# BB#7:
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_9
# BB#8:
	callq	_ZdaPv
.LBB16_9:                               # %_ZN11CStringBaseIwED2Ev.exit41
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_44
# BB#10:
	callq	_ZdaPv
	jmp	.LBB16_44
.LBB16_11:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	cmpl	%esi, 88(%r15)
	ja	.LBB16_45
# BB#12:
.Ltmp102:
	movq	%r15, %rdi
	callq	_ZN8NArchive4NTar8CHandler6SkipToEj
	movl	%eax, %ebp
.Ltmp103:
# BB#13:
	testl	%ebp, %ebp
	jne	.LBB16_45
# BB#14:
	addq	$96, %r15
	jmp	.LBB16_2
.LBB16_15:
	movq	16(%r15), %rsi
	cmpb	$50, 104(%r15)
	jne	.LBB16_18
# BB#16:
	testq	%rsi, %rsi
	jne	.LBB16_18
# BB#17:
	movslq	56(%r15), %rsi
.LBB16_18:                              # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit
.Ltmp128:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp129:
	jmp	.LBB16_44
.LBB16_19:
	addq	$64, %r15
.Ltmp114:
	leaq	72(%rsp), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp115:
# BB#20:                                # %_ZN8NArchive4NTarL18TarStringToUnicodeERK11CStringBaseIcE.exit49
	movq	72(%rsp), %rsi
.Ltmp116:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp117:
# BB#21:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_44
# BB#22:
	callq	_ZdaPv
	jmp	.LBB16_44
.LBB16_23:
	movsbl	104(%r15), %ecx
	cmpl	$52, %ecx
	jg	.LBB16_40
# BB#24:
	testl	%ecx, %ecx
	je	.LBB16_26
# BB#25:
	cmpl	$48, %ecx
	jne	.LBB16_42
.LBB16_26:
.Ltmp130:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj
                                        # kill: %AL<def> %AL<kill> %EAX<def>
.Ltmp131:
	jmp	.LBB16_43
.LBB16_27:
	addq	$80, %r15
.Ltmp109:
	leaq	56(%rsp), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp110:
# BB#28:                                # %_ZN8NArchive4NTarL18TarStringToUnicodeERK11CStringBaseIcE.exit54
	movq	56(%rsp), %rsi
.Ltmp111:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp112:
# BB#29:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_44
# BB#30:
	callq	_ZdaPv
	jmp	.LBB16_44
.LBB16_31:
	movl	24(%r15), %esi
.Ltmp119:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp120:
	jmp	.LBB16_44
.LBB16_32:
	movq	16(%r15), %rsi
	addq	$511, %rsi              # imm = 0x1FF
	andq	$-512, %rsi             # imm = 0xFE00
.Ltmp126:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp127:
	jmp	.LBB16_44
.LBB16_33:
	movl	36(%r15), %edi
	testl	%edi, %edi
	je	.LBB16_44
# BB#34:
.Ltmp121:
	leaq	24(%rsp), %rsi
	callq	_ZN8NWindows5NTime18UnixTimeToFileTimeEjR9_FILETIME
.Ltmp122:
# BB#35:
.Ltmp123:
	leaq	8(%rsp), %rdi
	leaq	24(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSERK9_FILETIME
.Ltmp124:
	jmp	.LBB16_44
.LBB16_36:
	addq	$48, %r15
.Ltmp104:
	leaq	40(%rsp), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	callq	_Z24MultiByteToUnicodeStringRK11CStringBaseIcEj
.Ltmp105:
# BB#37:                                # %_ZN8NArchive4NTarL18TarStringToUnicodeERK11CStringBaseIcE.exit47
	movq	40(%rsp), %rsi
.Ltmp106:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp107:
# BB#38:
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_44
# BB#39:
	callq	_ZdaPv
	jmp	.LBB16_44
.LBB16_40:
	movb	$1, %al
	cmpl	$53, %ecx
	je	.LBB16_43
# BB#41:
	cmpl	$68, %ecx
	je	.LBB16_43
.LBB16_42:
	xorl	%eax, %eax
.LBB16_43:                              # %_ZNK8NArchive4NTar5CItem5IsDirEv.exit
.Ltmp132:
	movzbl	%al, %esi
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEb
.Ltmp133:
.LBB16_44:                              # %_ZN11CStringBaseIwED2Ev.exit51
	xorl	%ebp, %ebp
.Ltmp143:
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp144:
.LBB16_45:
.Ltmp149:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp150:
.LBB16_46:
	movl	%ebp, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_47:
.Ltmp142:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	88(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_49
# BB#48:
	callq	_ZdaPv
	jmp	.LBB16_49
.LBB16_50:
.Ltmp108:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB16_54
	jmp	.LBB16_60
.LBB16_51:
.Ltmp113:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB16_54
	jmp	.LBB16_60
.LBB16_52:
.Ltmp139:
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB16_49:                              # %_ZN11CStringBaseIwED2Ev.exit43
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB16_54
	jmp	.LBB16_60
.LBB16_53:
.Ltmp118:
	movq	%rdx, %r14
	movq	%rax, %rbx
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_60
.LBB16_54:
	callq	_ZdaPv
	jmp	.LBB16_60
.LBB16_55:
.Ltmp136:
	jmp	.LBB16_59
.LBB16_56:
.Ltmp125:
	jmp	.LBB16_59
.LBB16_57:
.Ltmp151:
	movq	%rdx, %r14
	movq	%rax, %rbx
	jmp	.LBB16_61
.LBB16_58:
.Ltmp145:
.LBB16_59:                              # %_ZN11CStringBaseIwED2Ev.exit52
	movq	%rdx, %r14
	movq	%rax, %rbx
.LBB16_60:                              # %_ZN11CStringBaseIwED2Ev.exit52
.Ltmp146:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp147:
.LBB16_61:                              # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB16_63
# BB#62:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB16_46
.LBB16_63:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp152:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp153:
# BB#64:
.LBB16_65:
.Ltmp154:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB16_66:
.Ltmp148:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN8NArchive4NTar8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end16-_ZN8NArchive4NTar8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI16_0:
	.quad	.LBB16_4
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_23
	.quad	.LBB16_15
	.quad	.LBB16_32
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_33
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_19
	.quad	.LBB16_27
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_44
	.quad	.LBB16_31
	.quad	.LBB16_36
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\354\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\335\001"              # Call site table length
	.long	.Ltmp134-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp135-.Ltmp134       #   Call between .Ltmp134 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin5  #     jumps to .Ltmp136
	.byte	3                       #   On action: 2
	.long	.Ltmp137-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp138-.Ltmp137       #   Call between .Ltmp137 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin5  #     jumps to .Ltmp139
	.byte	3                       #   On action: 2
	.long	.Ltmp140-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp142-.Lfunc_begin5  #     jumps to .Ltmp142
	.byte	3                       #   On action: 2
	.long	.Ltmp102-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp115-.Ltmp102       #   Call between .Ltmp102 and .Ltmp115
	.long	.Ltmp145-.Lfunc_begin5  #     jumps to .Ltmp145
	.byte	3                       #   On action: 2
	.long	.Ltmp116-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin5  #     jumps to .Ltmp118
	.byte	3                       #   On action: 2
	.long	.Ltmp130-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp110-.Ltmp130       #   Call between .Ltmp130 and .Ltmp110
	.long	.Ltmp145-.Lfunc_begin5  #     jumps to .Ltmp145
	.byte	3                       #   On action: 2
	.long	.Ltmp111-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin5  #     jumps to .Ltmp113
	.byte	3                       #   On action: 2
	.long	.Ltmp119-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp127-.Ltmp119       #   Call between .Ltmp119 and .Ltmp127
	.long	.Ltmp145-.Lfunc_begin5  #     jumps to .Ltmp145
	.byte	3                       #   On action: 2
	.long	.Ltmp121-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp124-.Ltmp121       #   Call between .Ltmp121 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin5  #     jumps to .Ltmp125
	.byte	3                       #   On action: 2
	.long	.Ltmp104-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp145-.Lfunc_begin5  #     jumps to .Ltmp145
	.byte	3                       #   On action: 2
	.long	.Ltmp106-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin5  #     jumps to .Ltmp108
	.byte	3                       #   On action: 2
	.long	.Ltmp132-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp144-.Ltmp132       #   Call between .Ltmp132 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin5  #     jumps to .Ltmp145
	.byte	3                       #   On action: 2
	.long	.Ltmp149-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp150-.Ltmp149       #   Call between .Ltmp149 and .Ltmp150
	.long	.Ltmp151-.Lfunc_begin5  #     jumps to .Ltmp151
	.byte	3                       #   On action: 2
	.long	.Ltmp146-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp147-.Ltmp146       #   Call between .Ltmp146 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin5  #     jumps to .Ltmp148
	.byte	1                       #   On action: 1
	.long	.Ltmp147-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp152-.Ltmp147       #   Call between .Ltmp147 and .Ltmp152
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp153-.Ltmp152       #   Call between .Ltmp152 and .Ltmp153
	.long	.Ltmp154-.Lfunc_begin5  #     jumps to .Ltmp154
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Lfunc_end16-.Ltmp153   #   Call between .Ltmp153 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NTar8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive4NTar8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive4NTar8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi79:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi82:
	.cfi_def_cfa_offset 192
.Lcfi83:
	.cfi_offset %rbx, -56
.Lcfi84:
	.cfi_offset %r12, -48
.Lcfi85:
	.cfi_offset %r13, -40
.Lcfi86:
	.cfi_offset %r14, -32
.Lcfi87:
	.cfi_offset %r15, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	72(%rdi), %rax
	testq	%rax, %rax
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	80(%rdi), %r13
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmovneq	%rax, %r13
	cmpl	$-1, %edx
	movl	%edx, 68(%rsp)          # 4-byte Spill
	movl	%edx, %ebx
	jne	.LBB17_2
# BB#1:
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	52(%rax), %ebx
.LBB17_2:
	testl	%ebx, %ebx
	jne	.LBB17_4
# BB#3:
	xorl	%r12d, %r12d
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB17_143
.LBB17_4:                               # %.preheader
	testl	%ebx, %ebx
	je	.LBB17_5
# BB#6:                                 # %.lr.ph309
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	cmpl	$-1, 68(%rsp)           # 4-byte Folded Reload
	je	.LBB17_7
# BB#20:                                # %.lr.ph309.split.preheader
	movl	%ecx, %r12d
	movl	%ebx, %ecx
	xorl	%esi, %esi
	movq	112(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB17_21:                              # %.lr.ph309.split
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rdi), %rdx
	movq	(%rax,%rdx,8), %rbp
	movq	16(%rbp), %rdx
	cmpb	$50, 104(%rbp)
	jne	.LBB17_24
# BB#22:                                # %.lr.ph309.split
                                        #   in Loop: Header=BB17_21 Depth=1
	testq	%rdx, %rdx
	jne	.LBB17_24
# BB#23:                                #   in Loop: Header=BB17_21 Depth=1
	movslq	56(%rbp), %rdx
.LBB17_24:                              # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit
                                        #   in Loop: Header=BB17_21 Depth=1
	addq	%rdx, %rsi
	addq	$4, %rdi
	decq	%rcx
	jne	.LBB17_21
	jmp	.LBB17_25
.LBB17_5:
	movl	%ecx, %r12d
	xorl	%esi, %esi
	jmp	.LBB17_25
.LBB17_7:                               # %.lr.ph309.split.us.preheader
	movl	%ecx, %r12d
	testb	$1, %bl
	jne	.LBB17_9
# BB#8:
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	cmpl	$1, %ebx
	jne	.LBB17_13
	jmp	.LBB17_25
.LBB17_9:                               # %.lr.ph309.split.us.prol
	movq	(%rax), %rdx
	movq	16(%rdx), %rsi
	movl	$1, %ecx
	cmpb	$50, 104(%rdx)
	jne	.LBB17_12
# BB#10:                                # %.lr.ph309.split.us.prol
	testq	%rsi, %rsi
	jne	.LBB17_12
# BB#11:
	movslq	56(%rdx), %rsi
.LBB17_12:                              # %.lr.ph309.split.us.prol.loopexit
	cmpl	$1, %ebx
	je	.LBB17_25
	.p2align	4, 0x90
.LBB17_13:                              # %.lr.ph309.split.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ecx, %rdx
	movq	(%rax,%rdx,8), %rdi
	movq	16(%rdi), %rdx
	cmpb	$50, 104(%rdi)
	jne	.LBB17_16
# BB#14:                                # %.lr.ph309.split.us
                                        #   in Loop: Header=BB17_13 Depth=1
	testq	%rdx, %rdx
	jne	.LBB17_16
# BB#15:                                #   in Loop: Header=BB17_13 Depth=1
	movslq	56(%rdi), %rdx
.LBB17_16:                              # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit.us
                                        #   in Loop: Header=BB17_13 Depth=1
	addq	%rsi, %rdx
	leal	1(%rcx), %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rdi
	movq	16(%rdi), %rsi
	cmpb	$50, 104(%rdi)
	jne	.LBB17_19
# BB#17:                                # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit.us
                                        #   in Loop: Header=BB17_13 Depth=1
	testq	%rsi, %rsi
	jne	.LBB17_19
# BB#18:                                #   in Loop: Header=BB17_13 Depth=1
	movslq	56(%rdi), %rsi
.LBB17_19:                              # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit.us.1
                                        #   in Loop: Header=BB17_13 Depth=1
	addq	%rdx, %rsi
	addl	$2, %ecx
	cmpl	%ecx, %ebx
	jne	.LBB17_13
.LBB17_25:                              # %._crit_edge
	movl	%ebx, 64(%rsp)          # 4-byte Spill
	movq	(%r15), %rax
.Ltmp155:
	movq	%r15, %rdi
	callq	*40(%rax)
.Ltmp156:
# BB#26:
.Ltmp158:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp159:
# BB#27:
.Ltmp161:
	movq	%r14, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp162:
# BB#28:
	movq	(%r14), %rax
.Ltmp164:
	movq	%r14, %rdi
	callq	*8(%rax)
.Ltmp165:
# BB#29:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp167:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp168:
# BB#30:
.Ltmp169:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp170:
# BB#31:
	movl	$0, 8(%rbx)
	movq	$_ZTV26CLimitedSequentialInStream+16, (%rbx)
	movq	$0, 16(%rbx)
.Ltmp172:
	movq	%rbx, %rdi
	callq	*_ZTV26CLimitedSequentialInStream+24(%rip)
.Ltmp173:
# BB#32:                                # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit
	testq	%r13, %r13
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	je	.LBB17_34
# BB#33:
	movq	(%r13), %rax
.Ltmp175:
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp176:
.LBB17_34:                              # %.noexc260
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_36
# BB#35:
	movq	(%rdi), %rax
.Ltmp177:
	callq	*16(%rax)
.Ltmp178:
.LBB17_36:
	movq	%r13, 16(%rbx)
.Ltmp179:
	movl	$40, %edi
	callq	_Znwm
.Ltmp180:
# BB#37:
	movl	%r12d, 60(%rsp)         # 4-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	$0, 8(%rax)
	movq	$_ZTV27CLimitedSequentialOutStream+16, (%rax)
	movq	$0, 16(%rax)
.Ltmp182:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, %rdi
	callq	*_ZTV27CLimitedSequentialOutStream+24(%rip)
.Ltmp183:
# BB#38:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit.preheader
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB17_40
# BB#39:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit.preheader
	xorl	%r12d, %r12d
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	jne	.LBB17_40
.LBB17_128:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit264._crit_edge
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp226:
	callq	*16(%rax)
.Ltmp227:
# BB#129:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit255
	movq	(%rbx), %rax
.Ltmp231:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp232:
# BB#130:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit253
	movq	(%r14), %rax
.Ltmp237:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp238:
	jmp	.LBB17_143
.LBB17_40:                              # %.lr.ph
	xorl	%eax, %eax
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	setne	%al
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	96(%rax), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	$1, %eax
                                        # implicit-def: %R13D
	xorl	%ecx, %ecx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	jmp	.LBB17_41
	.p2align	4, 0x90
.LBB17_55:                              #   in Loop: Header=BB17_41 Depth=1
	leal	-1(%r15), %ebp
	cmpl	$-1, 68(%rsp)           # 4-byte Folded Reload
	movq	$0, 40(%rsp)
	je	.LBB17_57
# BB#56:                                #   in Loop: Header=BB17_41 Depth=1
	movl	%ebp, %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %ebp
.LBB17_57:                              #   in Loop: Header=BB17_41 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB17_58
# BB#66:                                #   in Loop: Header=BB17_41 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rbx
.LBB17_67:                              # %.thread287
                                        #   in Loop: Header=BB17_41 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp190:
	movl	%ebp, %esi
	leaq	40(%rsp), %rdx
	movl	20(%rsp), %ecx          # 4-byte Reload
	callq	*56(%rax)
.Ltmp191:
# BB#68:                                #   in Loop: Header=BB17_41 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	movl	$1, %r12d
	je	.LBB17_70
# BB#69:                                #   in Loop: Header=BB17_41 Depth=1
	movl	%eax, %r13d
	jmp	.LBB17_119
	.p2align	4, 0x90
.LBB17_58:                              #   in Loop: Header=BB17_41 Depth=1
.Ltmp188:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	callq	_ZN8NArchive4NTar8CHandler6SkipToEj
	movl	%eax, %ecx
.Ltmp189:
# BB#59:                                #   in Loop: Header=BB17_41 Depth=1
	cmpl	$-2147024809, %ecx      # imm = 0x80070057
	je	.LBB17_60
# BB#64:                                #   in Loop: Header=BB17_41 Depth=1
	testl	%ecx, %ecx
	movq	128(%rsp), %rbx         # 8-byte Reload
	je	.LBB17_67
# BB#65:                                #   in Loop: Header=BB17_41 Depth=1
	movl	$1, %r12d
	movl	%ecx, %r13d
	jmp	.LBB17_119
	.p2align	4, 0x90
.LBB17_70:                              #   in Loop: Header=BB17_41 Depth=1
	movsbl	104(%rbx), %eax
	cmpl	$50, %eax
	movq	16(%rbx), %rcx
	movq	%rcx, %rbp
	jne	.LBB17_73
# BB#71:                                #   in Loop: Header=BB17_41 Depth=1
	testq	%rcx, %rcx
	movq	%rcx, %rbp
	jne	.LBB17_73
# BB#72:                                #   in Loop: Header=BB17_41 Depth=1
	movslq	56(%rbx), %rbp
.LBB17_73:                              # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit267
                                        #   in Loop: Header=BB17_41 Depth=1
	addq	%rbp, 104(%rsp)         # 8-byte Folded Spill
	addq	$511, %rcx              # imm = 0x1FF
	andq	$-512, %rcx             # imm = 0xFE00
	addq	%rcx, 96(%rsp)          # 8-byte Folded Spill
	cmpl	$52, %eax
	jg	.LBB17_80
# BB#74:                                # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit267
                                        #   in Loop: Header=BB17_41 Depth=1
	testl	%eax, %eax
	je	.LBB17_76
# BB#75:                                # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit267
                                        #   in Loop: Header=BB17_41 Depth=1
	cmpl	$48, %eax
	jne	.LBB17_78
.LBB17_76:                              #   in Loop: Header=BB17_41 Depth=1
.Ltmp193:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN8NArchive9NItemName12HasTailSlashERK11CStringBaseIcEj
.Ltmp194:
# BB#77:                                # %_ZNK8NArchive4NTar5CItem5IsDirEv.exit
                                        #   in Loop: Header=BB17_41 Depth=1
	testb	%al, %al
	jne	.LBB17_82
	jmp	.LBB17_78
.LBB17_80:                              # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit267
                                        #   in Loop: Header=BB17_41 Depth=1
	cmpl	$53, %eax
	je	.LBB17_82
# BB#81:                                # %_ZNK8NArchive4NTar5CItem13GetUnpackSizeEv.exit267
                                        #   in Loop: Header=BB17_41 Depth=1
	cmpl	$68, %eax
	jne	.LBB17_78
.LBB17_82:                              # %_ZNK8NArchive4NTar5CItem5IsDirEv.exit.thread
                                        #   in Loop: Header=BB17_41 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp195:
	movl	20(%rsp), %esi          # 4-byte Reload
	callq	*64(%rax)
.Ltmp196:
# BB#83:                                #   in Loop: Header=BB17_41 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	je	.LBB17_86
# BB#84:                                #   in Loop: Header=BB17_41 Depth=1
	movl	%eax, %r13d
	jmp	.LBB17_119
.LBB17_60:                              #   in Loop: Header=BB17_41 Depth=1
	movl	$5, %r12d
	jmp	.LBB17_119
.LBB17_78:                              # %_ZNK8NArchive4NTar5CItem5IsDirEv.exit.thread291
                                        #   in Loop: Header=BB17_41 Depth=1
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	je	.LBB17_88
# BB#79:                                #   in Loop: Header=BB17_41 Depth=1
	xorl	%eax, %eax
	movl	20(%rsp), %esi          # 4-byte Reload
.LBB17_90:                              #   in Loop: Header=BB17_41 Depth=1
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp200:
	callq	*64(%rax)
.Ltmp201:
	movq	120(%rsp), %rbp         # 8-byte Reload
# BB#91:                                #   in Loop: Header=BB17_41 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	je	.LBB17_93
# BB#92:                                #   in Loop: Header=BB17_41 Depth=1
	movl	%eax, %r13d
	jmp	.LBB17_119
.LBB17_86:                              #   in Loop: Header=BB17_41 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp197:
	xorl	%esi, %esi
	callq	*72(%rax)
.Ltmp198:
# BB#87:                                #   in Loop: Header=BB17_41 Depth=1
	xorl	%r12d, %r12d
	testl	%eax, %eax
	setne	%r12b
	cmovnel	%eax, %r13d
	movl	$7, %eax
	cmovel	%eax, %r12d
	.p2align	4, 0x90
.LBB17_119:                             # %.threadthread-pre-split
                                        #   in Loop: Header=BB17_41 Depth=1
	movq	40(%rsp), %rdi
	movl	%r12d, %ebp
.LBB17_120:                             # %.thread
                                        #   in Loop: Header=BB17_41 Depth=1
	movq	72(%rsp), %rbx          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB17_122
# BB#121:                               #   in Loop: Header=BB17_41 Depth=1
	movq	(%rdi), %rax
.Ltmp221:
	callq	*16(%rax)
.Ltmp222:
.LBB17_122:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit264
                                        #   in Loop: Header=BB17_41 Depth=1
	xorl	%r12d, %r12d
	testl	%ebp, %ebp
	je	.LBB17_126
# BB#123:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit264
                                        #   in Loop: Header=BB17_41 Depth=1
	cmpl	$7, %ebp
	jne	.LBB17_124
.LBB17_126:                             # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
                                        #   in Loop: Header=BB17_41 Depth=1
	leal	1(%r15), %eax
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB17_41
# BB#127:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
                                        #   in Loop: Header=BB17_41 Depth=1
	cmpl	64(%rsp), %r15d         # 4-byte Folded Reload
	jb	.LBB17_41
	jmp	.LBB17_128
.LBB17_88:                              #   in Loop: Header=BB17_41 Depth=1
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	movl	$2, %esi
	cmovnel	20(%rsp), %esi          # 4-byte Folded Reload
	sete	%al
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB17_90
# BB#89:                                #   in Loop: Header=BB17_41 Depth=1
	movl	$7, %ebp
	testq	%rdi, %rdi
	je	.LBB17_120
	jmp	.LBB17_90
.LBB17_93:                              #   in Loop: Header=BB17_41 Depth=1
	movq	40(%rsp), %rcx
	testq	%rcx, %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	je	.LBB17_95
# BB#94:                                #   in Loop: Header=BB17_41 Depth=1
	movq	(%rcx), %rax
.Ltmp202:
	movq	%rcx, %rdi
	callq	*8(%rax)
	movq	88(%rsp), %rcx          # 8-byte Reload
.Ltmp203:
.LBB17_95:                              # %.noexc270
                                        #   in Loop: Header=BB17_41 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB17_97
# BB#96:                                #   in Loop: Header=BB17_41 Depth=1
	movq	(%rdi), %rax
.Ltmp204:
	callq	*16(%rax)
	movq	88(%rsp), %rcx          # 8-byte Reload
.Ltmp205:
.LBB17_97:                              #   in Loop: Header=BB17_41 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rcx, 16(%rax)
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_100
# BB#98:                                #   in Loop: Header=BB17_41 Depth=1
	movq	(%rdi), %rax
.Ltmp206:
	callq	*16(%rax)
.Ltmp207:
# BB#99:                                # %.noexc269
                                        #   in Loop: Header=BB17_41 Depth=1
	movq	$0, 40(%rsp)
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB17_100:                             # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
                                        #   in Loop: Header=BB17_41 Depth=1
	cmpb	$0, 84(%rsp)            # 1-byte Folded Reload
	movl	$0, %ecx
	cmovneq	%rcx, %rbp
	movq	%rbp, 24(%rax)
	movw	$256, 32(%rax)          # imm = 0x100
	cmpb	$50, 104(%rbx)
	jne	.LBB17_105
# BB#101:                               # %_ZNK8NArchive4NTar5CItem6IsLinkEv.exit
                                        #   in Loop: Header=BB17_41 Depth=1
	cmpq	$0, 16(%rbx)
	je	.LBB17_102
.LBB17_105:                             # %_ZNK8NArchive4NTar5CItem6IsLinkEv.exit.thread
                                        #   in Loop: Header=BB17_41 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB17_109
# BB#106:                               #   in Loop: Header=BB17_41 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	72(%rax), %rdi
	movq	(%rdi), %rax
	movl	120(%rbx), %esi
	addq	112(%rbx), %rsi
.Ltmp208:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
.Ltmp209:
# BB#107:                               #   in Loop: Header=BB17_41 Depth=1
	testl	%eax, %eax
	je	.LBB17_109
# BB#108:                               #   in Loop: Header=BB17_41 Depth=1
	movl	%eax, %r13d
	jmp	.LBB17_119
.LBB17_109:                             #   in Loop: Header=BB17_41 Depth=1
	movq	16(%rbx), %rax
	movl	$511, %ecx              # imm = 0x1FF
	addq	%rcx, %rax
	andq	$-512, %rax             # imm = 0xFE00
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	%rax, 24(%rsi)
	movq	$0, 32(%rsi)
	movb	$0, 40(%rsi)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	272(%rax), %rdi
	movq	(%rdi), %rax
.Ltmp210:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r14, %r9
	callq	*40(%rax)
.Ltmp211:
# BB#110:                               #   in Loop: Header=BB17_41 Depth=1
	testl	%eax, %eax
	je	.LBB17_112
# BB#111:                               #   in Loop: Header=BB17_41 Depth=1
	movl	%eax, %r13d
	jmp	.LBB17_119
.LBB17_102:                             #   in Loop: Header=BB17_41 Depth=1
	movq	48(%rbx), %rsi
	movslq	56(%rbx), %rdx
.Ltmp212:
	movq	%rax, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
.Ltmp213:
# BB#103:                               #   in Loop: Header=BB17_41 Depth=1
	testl	%eax, %eax
	je	.LBB17_112
# BB#104:                               #   in Loop: Header=BB17_41 Depth=1
	movl	%eax, %r13d
	jmp	.LBB17_119
.LBB17_112:                             #   in Loop: Header=BB17_41 Depth=1
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB17_114
# BB#113:                               #   in Loop: Header=BB17_41 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	$0, 92(%rax)
	incl	88(%rax)
.LBB17_114:                             #   in Loop: Header=BB17_41 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_117
# BB#115:                               #   in Loop: Header=BB17_41 Depth=1
	movq	(%rdi), %rax
.Ltmp214:
	callq	*16(%rax)
.Ltmp215:
# BB#116:                               # %.noexc265
                                        #   in Loop: Header=BB17_41 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	$0, 16(%rcx)
.LBB17_117:                             # %_ZN27CLimitedSequentialOutStream13ReleaseStreamEv.exit
                                        #   in Loop: Header=BB17_41 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	xorl	%esi, %esi
	cmpq	$0, 24(%rcx)
	setne	%sil
	addl	%esi, %esi
.Ltmp216:
	callq	*72(%rax)
.Ltmp217:
# BB#118:                               #   in Loop: Header=BB17_41 Depth=1
	xorl	%r12d, %r12d
	testl	%eax, %eax
	setne	%r12b
	cmovnel	%eax, %r13d
	jmp	.LBB17_119
	.p2align	4, 0x90
.LBB17_41:                              # =>This Inner Loop Header: Depth=1
	movl	%eax, %r15d
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, 48(%r14)
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, 56(%r14)
.Ltmp185:
	movq	%r14, %rdi
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp186:
# BB#42:                                #   in Loop: Header=BB17_41 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	je	.LBB17_55
# BB#43:
	movl	%eax, %r13d
	movl	%r13d, %r12d
	jmp	.LBB17_128
.LBB17_124:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit264
	cmpl	$5, %ebp
	je	.LBB17_128
# BB#125:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit264._crit_edge.loopexit318
	movl	%r13d, %r12d
	jmp	.LBB17_128
.LBB17_148:
.Ltmp239:
	jmp	.LBB17_45
.LBB17_147:
.Ltmp233:
	jmp	.LBB17_50
.LBB17_132:
.Ltmp228:
	jmp	.LBB17_53
.LBB17_54:
.Ltmp184:
	jmp	.LBB17_53
.LBB17_51:
.Ltmp174:
	jmp	.LBB17_50
.LBB17_48:
.Ltmp166:
	jmp	.LBB17_45
.LBB17_47:
.Ltmp163:
	movq	%rdx, %r15
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	jmp	.LBB17_139
.LBB17_46:
.Ltmp160:
	jmp	.LBB17_45
.LBB17_44:
.Ltmp157:
.LBB17_45:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB17_139
.LBB17_49:
.Ltmp171:
.LBB17_50:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB17_138
.LBB17_52:
.Ltmp181:
.LBB17_53:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB17_137
.LBB17_146:
.Ltmp218:
	jmp	.LBB17_62
.LBB17_85:
.Ltmp199:
	jmp	.LBB17_62
.LBB17_131:
.Ltmp223:
	jmp	.LBB17_134
.LBB17_133:
.Ltmp187:
.LBB17_134:
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB17_135
.LBB17_61:
.Ltmp192:
.LBB17_62:
	movq	%rdx, %r15
	movq	%rax, %rbx
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB17_135
# BB#63:
	movq	(%rdi), %rax
.Ltmp219:
	callq	*16(%rax)
.Ltmp220:
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB17_136
.LBB17_135:
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB17_136:
	movq	(%rdi), %rax
.Ltmp224:
	callq	*16(%rax)
.Ltmp225:
.LBB17_137:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp229:
	callq	*16(%rax)
.Ltmp230:
.LBB17_138:                             # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	(%r14), %rax
.Ltmp234:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp235:
.LBB17_139:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r15d
	je	.LBB17_140
# BB#142:
	callq	__cxa_end_catch
	movl	$-2147024882, %r12d     # imm = 0x8007000E
.LBB17_143:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit250
	movl	%r12d, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_140:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp240:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp241:
# BB#145:
.LBB17_141:
.Ltmp242:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB17_144:
.Ltmp236:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN8NArchive4NTar8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end17-_ZN8NArchive4NTar8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\255\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\236\002"              # Call site table length
	.long	.Ltmp155-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin6  #     jumps to .Ltmp157
	.byte	3                       #   On action: 2
	.long	.Ltmp158-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp159-.Ltmp158       #   Call between .Ltmp158 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin6  #     jumps to .Ltmp160
	.byte	3                       #   On action: 2
	.long	.Ltmp161-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp162-.Ltmp161       #   Call between .Ltmp161 and .Ltmp162
	.long	.Ltmp163-.Lfunc_begin6  #     jumps to .Ltmp163
	.byte	3                       #   On action: 2
	.long	.Ltmp164-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp165-.Ltmp164       #   Call between .Ltmp164 and .Ltmp165
	.long	.Ltmp166-.Lfunc_begin6  #     jumps to .Ltmp166
	.byte	3                       #   On action: 2
	.long	.Ltmp167-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp170-.Ltmp167       #   Call between .Ltmp167 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin6  #     jumps to .Ltmp171
	.byte	3                       #   On action: 2
	.long	.Ltmp172-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin6  #     jumps to .Ltmp174
	.byte	3                       #   On action: 2
	.long	.Ltmp175-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp180-.Ltmp175       #   Call between .Ltmp175 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin6  #     jumps to .Ltmp181
	.byte	3                       #   On action: 2
	.long	.Ltmp182-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin6  #     jumps to .Ltmp184
	.byte	3                       #   On action: 2
	.long	.Ltmp226-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp228-.Lfunc_begin6  #     jumps to .Ltmp228
	.byte	3                       #   On action: 2
	.long	.Ltmp231-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin6  #     jumps to .Ltmp233
	.byte	3                       #   On action: 2
	.long	.Ltmp237-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp238-.Ltmp237       #   Call between .Ltmp237 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin6  #     jumps to .Ltmp239
	.byte	3                       #   On action: 2
	.long	.Ltmp190-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp189-.Ltmp190       #   Call between .Ltmp190 and .Ltmp189
	.long	.Ltmp192-.Lfunc_begin6  #     jumps to .Ltmp192
	.byte	3                       #   On action: 2
	.long	.Ltmp193-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp196-.Ltmp193       #   Call between .Ltmp193 and .Ltmp196
	.long	.Ltmp199-.Lfunc_begin6  #     jumps to .Ltmp199
	.byte	3                       #   On action: 2
	.long	.Ltmp200-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp201-.Ltmp200       #   Call between .Ltmp200 and .Ltmp201
	.long	.Ltmp218-.Lfunc_begin6  #     jumps to .Ltmp218
	.byte	3                       #   On action: 2
	.long	.Ltmp197-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp198-.Ltmp197       #   Call between .Ltmp197 and .Ltmp198
	.long	.Ltmp199-.Lfunc_begin6  #     jumps to .Ltmp199
	.byte	3                       #   On action: 2
	.long	.Ltmp221-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp222-.Ltmp221       #   Call between .Ltmp221 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin6  #     jumps to .Ltmp223
	.byte	3                       #   On action: 2
	.long	.Ltmp202-.Lfunc_begin6  # >> Call Site 17 <<
	.long	.Ltmp217-.Ltmp202       #   Call between .Ltmp202 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin6  #     jumps to .Ltmp218
	.byte	3                       #   On action: 2
	.long	.Ltmp185-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Ltmp186-.Ltmp185       #   Call between .Ltmp185 and .Ltmp186
	.long	.Ltmp187-.Lfunc_begin6  #     jumps to .Ltmp187
	.byte	3                       #   On action: 2
	.long	.Ltmp219-.Lfunc_begin6  # >> Call Site 19 <<
	.long	.Ltmp235-.Ltmp219       #   Call between .Ltmp219 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin6  #     jumps to .Ltmp236
	.byte	1                       #   On action: 1
	.long	.Ltmp235-.Lfunc_begin6  # >> Call Site 20 <<
	.long	.Ltmp240-.Ltmp235       #   Call between .Ltmp235 and .Ltmp240
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp240-.Lfunc_begin6  # >> Call Site 21 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin6  #     jumps to .Ltmp242
	.byte	0                       #   On action: cleanup
	.long	.Ltmp241-.Lfunc_begin6  # >> Call Site 22 <<
	.long	.Lfunc_end17-.Ltmp241   #   Call between .Ltmp241 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream,@function
_ZN8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream: # @_ZN8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 48
.Lcfi94:
	.cfi_offset %rbx, -40
.Lcfi95:
	.cfi_offset %r12, -32
.Lcfi96:
	.cfi_offset %r14, -24
.Lcfi97:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rdi, %r15
	movq	56(%r15), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rbx
	movq	16(%rbx), %rdx
	cmpb	$50, 104(%rbx)
	jne	.LBB18_14
# BB#1:
	testq	%rdx, %rdx
	jne	.LBB18_14
# BB#2:
.Ltmp245:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp246:
# BB#3:
	movl	$0, 8(%r12)
	movq	$_ZTV12CBufInStream+16, (%r12)
	movq	$0, 40(%r12)
.Ltmp248:
	movq	%r12, %rdi
	callq	*_ZTV12CBufInStream+24(%rip)
.Ltmp249:
# BB#4:
	movq	48(%rbx), %rax
	movslq	56(%rbx), %rcx
	movq	%rax, 16(%r12)
	movq	%rcx, 32(%r12)
	movq	$0, 24(%r12)
	movq	(%r15), %rax
.Ltmp251:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp252:
# BB#5:                                 # %.noexc33
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB18_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp253:
	callq	*16(%rax)
.Ltmp254:
.LBB18_7:
	movq	%r15, 40(%r12)
	movq	%r12, (%r14)
	xorl	%eax, %eax
	jmp	.LBB18_17
.LBB18_14:                              # %_ZNK8NArchive4NTar5CItem6IsLinkEv.exit.thread
	movq	72(%r15), %rdi
	movl	120(%rbx), %esi
	addq	112(%rbx), %rsi
.Ltmp243:
	movq	%r14, %rcx
	callq	_Z21CreateLimitedInStreamP9IInStreamyyPP19ISequentialInStream
.Ltmp244:
.LBB18_17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB18_12:
.Ltmp250:
	jmp	.LBB18_9
.LBB18_13:
.Ltmp255:
	movq	%rdx, %r14
	movq	%rax, %r15
	movq	(%r12), %rax
.Ltmp256:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp257:
	jmp	.LBB18_10
.LBB18_18:
.Ltmp258:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_8:
.Ltmp247:
.LBB18_9:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	%rdx, %r14
	movq	%rax, %r15
.LBB18_10:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	%r15, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB18_11
# BB#16:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB18_17
.LBB18_11:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp259:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp260:
# BB#19:
.LBB18_15:
.Ltmp261:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end18:
	.size	_ZN8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream, .Lfunc_end18-_ZN8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\366\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp245-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp246-.Ltmp245       #   Call between .Ltmp245 and .Ltmp246
	.long	.Ltmp247-.Lfunc_begin7  #     jumps to .Ltmp247
	.byte	3                       #   On action: 2
	.long	.Ltmp248-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin7  #     jumps to .Ltmp250
	.byte	3                       #   On action: 2
	.long	.Ltmp251-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp254-.Ltmp251       #   Call between .Ltmp251 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin7  #     jumps to .Ltmp255
	.byte	3                       #   On action: 2
	.long	.Ltmp243-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	.Ltmp247-.Lfunc_begin7  #     jumps to .Ltmp247
	.byte	3                       #   On action: 2
	.long	.Ltmp256-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp258-.Lfunc_begin7  #     jumps to .Ltmp258
	.byte	1                       #   On action: 1
	.long	.Ltmp257-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp259-.Ltmp257       #   Call between .Ltmp257 and .Ltmp259
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp259-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin7  #     jumps to .Ltmp261
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Lfunc_end18-.Ltmp260   #   Call between .Ltmp260 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn16_N8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream,@function
_ZThn16_N8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream: # @_ZThn16_N8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream # TAILCALL
.Lfunc_end19:
	.size	_ZThn16_N8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream, .Lfunc_end19-_ZThn16_N8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream
	.cfi_endproc

	.section	.text._ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB20_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB20_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB20_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB20_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB20_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB20_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB20_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB20_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB20_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB20_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB20_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB20_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB20_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB20_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB20_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB20_16
.LBB20_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB20_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInArchive+1(%rip), %al
	jne	.LBB20_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInArchive+2(%rip), %al
	jne	.LBB20_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInArchive+3(%rip), %al
	jne	.LBB20_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInArchive+4(%rip), %al
	jne	.LBB20_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInArchive+5(%rip), %al
	jne	.LBB20_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInArchive+6(%rip), %al
	jne	.LBB20_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInArchive+7(%rip), %al
	jne	.LBB20_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInArchive+8(%rip), %al
	jne	.LBB20_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInArchive+9(%rip), %al
	jne	.LBB20_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInArchive+10(%rip), %al
	jne	.LBB20_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInArchive+11(%rip), %al
	jne	.LBB20_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInArchive+12(%rip), %al
	jne	.LBB20_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInArchive+13(%rip), %al
	jne	.LBB20_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInArchive+14(%rip), %al
	jne	.LBB20_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %al
	cmpb	IID_IInArchive+15(%rip), %al
	jne	.LBB20_33
.LBB20_16:
	movq	%rdi, (%rdx)
	jmp	.LBB20_85
.LBB20_33:                              # %_ZeqRK4GUIDS1_.exit12.thread
	cmpb	IID_IArchiveOpenSeq(%rip), %cl
	jne	.LBB20_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+1(%rip), %al
	jne	.LBB20_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+2(%rip), %al
	jne	.LBB20_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+3(%rip), %al
	jne	.LBB20_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+4(%rip), %al
	jne	.LBB20_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+5(%rip), %al
	jne	.LBB20_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+6(%rip), %al
	jne	.LBB20_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+7(%rip), %al
	jne	.LBB20_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+8(%rip), %al
	jne	.LBB20_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+9(%rip), %al
	jne	.LBB20_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+10(%rip), %al
	jne	.LBB20_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+11(%rip), %al
	jne	.LBB20_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+12(%rip), %al
	jne	.LBB20_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+13(%rip), %al
	jne	.LBB20_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+14(%rip), %al
	jne	.LBB20_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit16
	movb	15(%rsi), %al
	cmpb	IID_IArchiveOpenSeq+15(%rip), %al
	jne	.LBB20_50
# BB#49:
	leaq	8(%rdi), %rax
	jmp	.LBB20_84
.LBB20_50:                              # %_ZeqRK4GUIDS1_.exit16.thread
	cmpb	IID_IInArchiveGetStream(%rip), %cl
	jne	.LBB20_67
# BB#51:
	movb	1(%rsi), %al
	cmpb	IID_IInArchiveGetStream+1(%rip), %al
	jne	.LBB20_67
# BB#52:
	movb	2(%rsi), %al
	cmpb	IID_IInArchiveGetStream+2(%rip), %al
	jne	.LBB20_67
# BB#53:
	movb	3(%rsi), %al
	cmpb	IID_IInArchiveGetStream+3(%rip), %al
	jne	.LBB20_67
# BB#54:
	movb	4(%rsi), %al
	cmpb	IID_IInArchiveGetStream+4(%rip), %al
	jne	.LBB20_67
# BB#55:
	movb	5(%rsi), %al
	cmpb	IID_IInArchiveGetStream+5(%rip), %al
	jne	.LBB20_67
# BB#56:
	movb	6(%rsi), %al
	cmpb	IID_IInArchiveGetStream+6(%rip), %al
	jne	.LBB20_67
# BB#57:
	movb	7(%rsi), %al
	cmpb	IID_IInArchiveGetStream+7(%rip), %al
	jne	.LBB20_67
# BB#58:
	movb	8(%rsi), %al
	cmpb	IID_IInArchiveGetStream+8(%rip), %al
	jne	.LBB20_67
# BB#59:
	movb	9(%rsi), %al
	cmpb	IID_IInArchiveGetStream+9(%rip), %al
	jne	.LBB20_67
# BB#60:
	movb	10(%rsi), %al
	cmpb	IID_IInArchiveGetStream+10(%rip), %al
	jne	.LBB20_67
# BB#61:
	movb	11(%rsi), %al
	cmpb	IID_IInArchiveGetStream+11(%rip), %al
	jne	.LBB20_67
# BB#62:
	movb	12(%rsi), %al
	cmpb	IID_IInArchiveGetStream+12(%rip), %al
	jne	.LBB20_67
# BB#63:
	movb	13(%rsi), %al
	cmpb	IID_IInArchiveGetStream+13(%rip), %al
	jne	.LBB20_67
# BB#64:
	movb	14(%rsi), %al
	cmpb	IID_IInArchiveGetStream+14(%rip), %al
	jne	.LBB20_67
# BB#65:                                # %_ZeqRK4GUIDS1_.exit18
	movb	15(%rsi), %al
	cmpb	IID_IInArchiveGetStream+15(%rip), %al
	jne	.LBB20_67
# BB#66:
	leaq	16(%rdi), %rax
	jmp	.LBB20_84
.LBB20_67:                              # %_ZeqRK4GUIDS1_.exit18.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IOutArchive(%rip), %cl
	jne	.LBB20_86
# BB#68:
	movb	1(%rsi), %cl
	cmpb	IID_IOutArchive+1(%rip), %cl
	jne	.LBB20_86
# BB#69:
	movb	2(%rsi), %cl
	cmpb	IID_IOutArchive+2(%rip), %cl
	jne	.LBB20_86
# BB#70:
	movb	3(%rsi), %cl
	cmpb	IID_IOutArchive+3(%rip), %cl
	jne	.LBB20_86
# BB#71:
	movb	4(%rsi), %cl
	cmpb	IID_IOutArchive+4(%rip), %cl
	jne	.LBB20_86
# BB#72:
	movb	5(%rsi), %cl
	cmpb	IID_IOutArchive+5(%rip), %cl
	jne	.LBB20_86
# BB#73:
	movb	6(%rsi), %cl
	cmpb	IID_IOutArchive+6(%rip), %cl
	jne	.LBB20_86
# BB#74:
	movb	7(%rsi), %cl
	cmpb	IID_IOutArchive+7(%rip), %cl
	jne	.LBB20_86
# BB#75:
	movb	8(%rsi), %cl
	cmpb	IID_IOutArchive+8(%rip), %cl
	jne	.LBB20_86
# BB#76:
	movb	9(%rsi), %cl
	cmpb	IID_IOutArchive+9(%rip), %cl
	jne	.LBB20_86
# BB#77:
	movb	10(%rsi), %cl
	cmpb	IID_IOutArchive+10(%rip), %cl
	jne	.LBB20_86
# BB#78:
	movb	11(%rsi), %cl
	cmpb	IID_IOutArchive+11(%rip), %cl
	jne	.LBB20_86
# BB#79:
	movb	12(%rsi), %cl
	cmpb	IID_IOutArchive+12(%rip), %cl
	jne	.LBB20_86
# BB#80:
	movb	13(%rsi), %cl
	cmpb	IID_IOutArchive+13(%rip), %cl
	jne	.LBB20_86
# BB#81:
	movb	14(%rsi), %cl
	cmpb	IID_IOutArchive+14(%rip), %cl
	jne	.LBB20_86
# BB#82:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %cl
	cmpb	IID_IOutArchive+15(%rip), %cl
	jne	.LBB20_86
# BB#83:
	leaq	24(%rdi), %rax
.LBB20_84:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	%rax, (%rdx)
.LBB20_85:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB20_86:                              # %_ZeqRK4GUIDS1_.exit14.thread
	popq	%rcx
	retq
.Lfunc_end20:
	.size	_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end20-_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive4NTar8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive4NTar8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive4NTar8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler6AddRefEv,@function
_ZN8NArchive4NTar8CHandler6AddRefEv:    # @_ZN8NArchive4NTar8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	32(%rdi), %eax
	incl	%eax
	movl	%eax, 32(%rdi)
	retq
.Lfunc_end21:
	.size	_ZN8NArchive4NTar8CHandler6AddRefEv, .Lfunc_end21-_ZN8NArchive4NTar8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NTar8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive4NTar8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive4NTar8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandler7ReleaseEv,@function
_ZN8NArchive4NTar8CHandler7ReleaseEv:   # @_ZN8NArchive4NTar8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 16
	movl	32(%rdi), %eax
	decl	%eax
	movl	%eax, 32(%rdi)
	jne	.LBB22_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB22_2:
	popq	%rcx
	retq
.Lfunc_end22:
	.size	_ZN8NArchive4NTar8CHandler7ReleaseEv, .Lfunc_end22-_ZN8NArchive4NTar8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive4NTar8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive4NTar8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive4NTar8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandlerD2Ev,@function
_ZN8NArchive4NTar8CHandlerD2Ev:         # @_ZN8NArchive4NTar8CHandlerD2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -24
.Lcfi104:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive4NTar8CHandlerE+184, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NTar8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN8NArchive4NTar8CHandlerE+312, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive4NTar8CHandlerE+248, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rbx)
	movq	272(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp262:
	callq	*16(%rax)
.Ltmp263:
.LBB23_2:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	248(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_4
# BB#3:
	callq	_ZdaPv
.LBB23_4:                               # %_ZN11CStringBaseIcED2Ev.exit
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_6
# BB#5:
	callq	_ZdaPv
.LBB23_6:                               # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_8
# BB#7:
	callq	_ZdaPv
.LBB23_8:                               # %_ZN11CStringBaseIcED2Ev.exit1.i
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_10
# BB#9:
	callq	_ZdaPv
.LBB23_10:                              # %_ZN11CStringBaseIcED2Ev.exit2.i
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_12
# BB#11:
	callq	_ZdaPv
.LBB23_12:                              # %_ZN8NArchive4NTar5CItemD2Ev.exit
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_14
# BB#13:
	movq	(%rdi), %rax
.Ltmp267:
	callq	*16(%rax)
.Ltmp268:
.LBB23_14:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_16
# BB#15:
	movq	(%rdi), %rax
.Ltmp272:
	callq	*16(%rax)
.Ltmp273:
.LBB23_16:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE+16, 40(%rbx)
	addq	$40, %rbx
.Ltmp284:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp285:
# BB#17:
.Ltmp290:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp291:
# BB#18:                                # %_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB23_37:
.Ltmp274:
	movq	%rax, %r14
	jmp	.LBB23_38
.LBB23_33:
.Ltmp269:
	movq	%rax, %r14
	jmp	.LBB23_34
.LBB23_21:
.Ltmp264:
	movq	%rax, %r14
	movq	248(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_23
# BB#22:
	callq	_ZdaPv
.LBB23_23:                              # %_ZN11CStringBaseIcED2Ev.exit12
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_25
# BB#24:
	callq	_ZdaPv
.LBB23_25:                              # %_ZN11CStringBaseIcED2Ev.exit.i13
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_27
# BB#26:
	callq	_ZdaPv
.LBB23_27:                              # %_ZN11CStringBaseIcED2Ev.exit1.i14
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_29
# BB#28:
	callq	_ZdaPv
.LBB23_29:                              # %_ZN11CStringBaseIcED2Ev.exit2.i15
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_31
# BB#30:
	callq	_ZdaPv
.LBB23_31:                              # %_ZN8NArchive4NTar5CItemD2Ev.exit16
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_34
# BB#32:
	movq	(%rdi), %rax
.Ltmp265:
	callq	*16(%rax)
.Ltmp266:
.LBB23_34:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit18
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_38
# BB#35:
	movq	(%rdi), %rax
.Ltmp270:
	callq	*16(%rax)
.Ltmp271:
.LBB23_38:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit20
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE+16, 40(%rbx)
	addq	$40, %rbx
.Ltmp275:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp276:
# BB#39:
.Ltmp281:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp282:
	jmp	.LBB23_40
.LBB23_41:
.Ltmp277:
	movq	%rax, %r14
.Ltmp278:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp279:
	jmp	.LBB23_44
.LBB23_42:
.Ltmp280:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB23_43:
.Ltmp283:
	movq	%rax, %r14
.LBB23_44:                              # %.body22
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB23_36:
.Ltmp292:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_19:
.Ltmp286:
	movq	%rax, %r14
.Ltmp287:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp288:
.LBB23_40:                              # %_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev.exit24
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB23_20:
.Ltmp289:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end23:
	.size	_ZN8NArchive4NTar8CHandlerD2Ev, .Lfunc_end23-_ZN8NArchive4NTar8CHandlerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp262-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp263-.Ltmp262       #   Call between .Ltmp262 and .Ltmp263
	.long	.Ltmp264-.Lfunc_begin8  #     jumps to .Ltmp264
	.byte	0                       #   On action: cleanup
	.long	.Ltmp267-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp268-.Ltmp267       #   Call between .Ltmp267 and .Ltmp268
	.long	.Ltmp269-.Lfunc_begin8  #     jumps to .Ltmp269
	.byte	0                       #   On action: cleanup
	.long	.Ltmp272-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp273-.Ltmp272       #   Call between .Ltmp272 and .Ltmp273
	.long	.Ltmp274-.Lfunc_begin8  #     jumps to .Ltmp274
	.byte	0                       #   On action: cleanup
	.long	.Ltmp284-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin8  #     jumps to .Ltmp286
	.byte	0                       #   On action: cleanup
	.long	.Ltmp290-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin8  #     jumps to .Ltmp292
	.byte	0                       #   On action: cleanup
	.long	.Ltmp265-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp271-.Ltmp265       #   Call between .Ltmp265 and .Ltmp271
	.long	.Ltmp283-.Lfunc_begin8  #     jumps to .Ltmp283
	.byte	1                       #   On action: 1
	.long	.Ltmp275-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp276-.Ltmp275       #   Call between .Ltmp275 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin8  #     jumps to .Ltmp277
	.byte	1                       #   On action: 1
	.long	.Ltmp281-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin8  #     jumps to .Ltmp283
	.byte	1                       #   On action: 1
	.long	.Ltmp278-.Lfunc_begin8  # >> Call Site 9 <<
	.long	.Ltmp279-.Ltmp278       #   Call between .Ltmp278 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin8  #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.long	.Ltmp279-.Lfunc_begin8  # >> Call Site 10 <<
	.long	.Ltmp287-.Ltmp279       #   Call between .Ltmp279 and .Ltmp287
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp287-.Lfunc_begin8  # >> Call Site 11 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin8  #     jumps to .Ltmp289
	.byte	1                       #   On action: 1
	.long	.Ltmp288-.Lfunc_begin8  # >> Call Site 12 <<
	.long	.Lfunc_end23-.Ltmp288   #   Call between .Ltmp288 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive4NTar8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive4NTar8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive4NTar8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar8CHandlerD0Ev,@function
_ZN8NArchive4NTar8CHandlerD0Ev:         # @_ZN8NArchive4NTar8CHandlerD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -24
.Lcfi109:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp293:
	callq	_ZN8NArchive4NTar8CHandlerD2Ev
.Ltmp294:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB24_2:
.Ltmp295:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end24:
	.size	_ZN8NArchive4NTar8CHandlerD0Ev, .Lfunc_end24-_ZN8NArchive4NTar8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp293-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin9  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp294-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end24-.Ltmp294   #   Call between .Ltmp294 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end25:
	.size	_ZThn8_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end25-_ZThn8_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NTar8CHandler6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive4NTar8CHandler6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive4NTar8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NTar8CHandler6AddRefEv,@function
_ZThn8_N8NArchive4NTar8CHandler6AddRefEv: # @_ZThn8_N8NArchive4NTar8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end26:
	.size	_ZThn8_N8NArchive4NTar8CHandler6AddRefEv, .Lfunc_end26-_ZThn8_N8NArchive4NTar8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NTar8CHandler7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive4NTar8CHandler7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive4NTar8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NTar8CHandler7ReleaseEv,@function
_ZThn8_N8NArchive4NTar8CHandler7ReleaseEv: # @_ZThn8_N8NArchive4NTar8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB27_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB27_2:                               # %_ZN8NArchive4NTar8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end27:
	.size	_ZThn8_N8NArchive4NTar8CHandler7ReleaseEv, .Lfunc_end27-_ZThn8_N8NArchive4NTar8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NTar8CHandlerD1Ev,"axG",@progbits,_ZThn8_N8NArchive4NTar8CHandlerD1Ev,comdat
	.weak	_ZThn8_N8NArchive4NTar8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NTar8CHandlerD1Ev,@function
_ZThn8_N8NArchive4NTar8CHandlerD1Ev:    # @_ZThn8_N8NArchive4NTar8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive4NTar8CHandlerD2Ev # TAILCALL
.Lfunc_end28:
	.size	_ZThn8_N8NArchive4NTar8CHandlerD1Ev, .Lfunc_end28-_ZThn8_N8NArchive4NTar8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive4NTar8CHandlerD0Ev,"axG",@progbits,_ZThn8_N8NArchive4NTar8CHandlerD0Ev,comdat
	.weak	_ZThn8_N8NArchive4NTar8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NTar8CHandlerD0Ev,@function
_ZThn8_N8NArchive4NTar8CHandlerD0Ev:    # @_ZThn8_N8NArchive4NTar8CHandlerD0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi113:
	.cfi_def_cfa_offset 32
.Lcfi114:
	.cfi_offset %rbx, -24
.Lcfi115:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp296:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NTar8CHandlerD2Ev
.Ltmp297:
# BB#1:                                 # %_ZN8NArchive4NTar8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB29_2:
.Ltmp298:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end29:
	.size	_ZThn8_N8NArchive4NTar8CHandlerD0Ev, .Lfunc_end29-_ZThn8_N8NArchive4NTar8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp296-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp297-.Ltmp296       #   Call between .Ltmp296 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin10 #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp297-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end29-.Ltmp297   #   Call between .Ltmp297 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn16_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end30:
	.size	_ZThn16_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end30-_ZThn16_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NTar8CHandler6AddRefEv,"axG",@progbits,_ZThn16_N8NArchive4NTar8CHandler6AddRefEv,comdat
	.weak	_ZThn16_N8NArchive4NTar8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NTar8CHandler6AddRefEv,@function
_ZThn16_N8NArchive4NTar8CHandler6AddRefEv: # @_ZThn16_N8NArchive4NTar8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end31:
	.size	_ZThn16_N8NArchive4NTar8CHandler6AddRefEv, .Lfunc_end31-_ZThn16_N8NArchive4NTar8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NTar8CHandler7ReleaseEv,"axG",@progbits,_ZThn16_N8NArchive4NTar8CHandler7ReleaseEv,comdat
	.weak	_ZThn16_N8NArchive4NTar8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NTar8CHandler7ReleaseEv,@function
_ZThn16_N8NArchive4NTar8CHandler7ReleaseEv: # @_ZThn16_N8NArchive4NTar8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB32_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB32_2:                               # %_ZN8NArchive4NTar8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end32:
	.size	_ZThn16_N8NArchive4NTar8CHandler7ReleaseEv, .Lfunc_end32-_ZThn16_N8NArchive4NTar8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NTar8CHandlerD1Ev,"axG",@progbits,_ZThn16_N8NArchive4NTar8CHandlerD1Ev,comdat
	.weak	_ZThn16_N8NArchive4NTar8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NTar8CHandlerD1Ev,@function
_ZThn16_N8NArchive4NTar8CHandlerD1Ev:   # @_ZThn16_N8NArchive4NTar8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive4NTar8CHandlerD2Ev # TAILCALL
.Lfunc_end33:
	.size	_ZThn16_N8NArchive4NTar8CHandlerD1Ev, .Lfunc_end33-_ZThn16_N8NArchive4NTar8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn16_N8NArchive4NTar8CHandlerD0Ev,"axG",@progbits,_ZThn16_N8NArchive4NTar8CHandlerD0Ev,comdat
	.weak	_ZThn16_N8NArchive4NTar8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NTar8CHandlerD0Ev,@function
_ZThn16_N8NArchive4NTar8CHandlerD0Ev:   # @_ZThn16_N8NArchive4NTar8CHandlerD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi119:
	.cfi_def_cfa_offset 32
.Lcfi120:
	.cfi_offset %rbx, -24
.Lcfi121:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp299:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NTar8CHandlerD2Ev
.Ltmp300:
# BB#1:                                 # %_ZN8NArchive4NTar8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB34_2:
.Ltmp301:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end34:
	.size	_ZThn16_N8NArchive4NTar8CHandlerD0Ev, .Lfunc_end34-_ZThn16_N8NArchive4NTar8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp299-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp300-.Ltmp299       #   Call between .Ltmp299 and .Ltmp300
	.long	.Ltmp301-.Lfunc_begin11 #     jumps to .Ltmp301
	.byte	0                       #   On action: cleanup
	.long	.Ltmp300-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Lfunc_end34-.Ltmp300   #   Call between .Ltmp300 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn24_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn24_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn24_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn24_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn24_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end35:
	.size	_ZThn24_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end35-_ZThn24_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive4NTar8CHandler6AddRefEv,"axG",@progbits,_ZThn24_N8NArchive4NTar8CHandler6AddRefEv,comdat
	.weak	_ZThn24_N8NArchive4NTar8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NTar8CHandler6AddRefEv,@function
_ZThn24_N8NArchive4NTar8CHandler6AddRefEv: # @_ZThn24_N8NArchive4NTar8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end36:
	.size	_ZThn24_N8NArchive4NTar8CHandler6AddRefEv, .Lfunc_end36-_ZThn24_N8NArchive4NTar8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive4NTar8CHandler7ReleaseEv,"axG",@progbits,_ZThn24_N8NArchive4NTar8CHandler7ReleaseEv,comdat
	.weak	_ZThn24_N8NArchive4NTar8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NTar8CHandler7ReleaseEv,@function
_ZThn24_N8NArchive4NTar8CHandler7ReleaseEv: # @_ZThn24_N8NArchive4NTar8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi122:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB37_2
# BB#1:
	addq	$-24, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB37_2:                               # %_ZN8NArchive4NTar8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end37:
	.size	_ZThn24_N8NArchive4NTar8CHandler7ReleaseEv, .Lfunc_end37-_ZThn24_N8NArchive4NTar8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive4NTar8CHandlerD1Ev,"axG",@progbits,_ZThn24_N8NArchive4NTar8CHandlerD1Ev,comdat
	.weak	_ZThn24_N8NArchive4NTar8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NTar8CHandlerD1Ev,@function
_ZThn24_N8NArchive4NTar8CHandlerD1Ev:   # @_ZThn24_N8NArchive4NTar8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-24, %rdi
	jmp	_ZN8NArchive4NTar8CHandlerD2Ev # TAILCALL
.Lfunc_end38:
	.size	_ZThn24_N8NArchive4NTar8CHandlerD1Ev, .Lfunc_end38-_ZThn24_N8NArchive4NTar8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn24_N8NArchive4NTar8CHandlerD0Ev,"axG",@progbits,_ZThn24_N8NArchive4NTar8CHandlerD0Ev,comdat
	.weak	_ZThn24_N8NArchive4NTar8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn24_N8NArchive4NTar8CHandlerD0Ev,@function
_ZThn24_N8NArchive4NTar8CHandlerD0Ev:   # @_ZThn24_N8NArchive4NTar8CHandlerD0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi125:
	.cfi_def_cfa_offset 32
.Lcfi126:
	.cfi_offset %rbx, -24
.Lcfi127:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-24, %rbx
.Ltmp302:
	movq	%rbx, %rdi
	callq	_ZN8NArchive4NTar8CHandlerD2Ev
.Ltmp303:
# BB#1:                                 # %_ZN8NArchive4NTar8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB39_2:
.Ltmp304:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end39:
	.size	_ZThn24_N8NArchive4NTar8CHandlerD0Ev, .Lfunc_end39-_ZThn24_N8NArchive4NTar8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp302-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp303-.Ltmp302       #   Call between .Ltmp302 and .Ltmp303
	.long	.Ltmp304-.Lfunc_begin12 #     jumps to .Ltmp304
	.byte	0                       #   On action: cleanup
	.long	.Ltmp303-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Lfunc_end39-.Ltmp303   #   Call between .Ltmp303 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI40_0:
	.zero	16
	.section	.text._ZN8NArchive4NTar5CItemC2Ev,"axG",@progbits,_ZN8NArchive4NTar5CItemC2Ev,comdat
	.weak	_ZN8NArchive4NTar5CItemC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar5CItemC2Ev,@function
_ZN8NArchive4NTar5CItemC2Ev:            # @_ZN8NArchive4NTar5CItemC2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi130:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi132:
	.cfi_def_cfa_offset 48
.Lcfi133:
	.cfi_offset %rbx, -40
.Lcfi134:
	.cfi_offset %r12, -32
.Lcfi135:
	.cfi_offset %r14, -24
.Lcfi136:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, (%rbx)
	movb	$0, (%rax)
	movl	$4, 12(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
.Ltmp305:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp306:
# BB#1:
	movq	%r15, 48(%rbx)
	movb	$0, (%r15)
	movl	$4, 60(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rbx)
.Ltmp308:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp309:
# BB#2:
	leaq	48(%rbx), %r15
	movq	%r12, 64(%rbx)
	movb	$0, (%r12)
	movl	$4, 76(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
.Ltmp311:
	movl	$4, %edi
	callq	_Znam
.Ltmp312:
# BB#3:
	movq	%rax, 80(%rbx)
	movb	$0, (%rax)
	movl	$4, 92(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB40_6:                               # %_ZN11CStringBaseIcED2Ev.exit
.Ltmp313:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB40_7
	jmp	.LBB40_8
.LBB40_5:                               # %_ZN11CStringBaseIcED2Ev.exit.thread
.Ltmp310:
	movq	%rax, %r14
.LBB40_7:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB40_8
.LBB40_4:
.Ltmp307:
	movq	%rax, %r14
.LBB40_8:                               # %_ZN11CStringBaseIcED2Ev.exit6
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB40_10
# BB#9:
	callq	_ZdaPv
.LBB40_10:                              # %_ZN11CStringBaseIcED2Ev.exit7
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end40:
	.size	_ZN8NArchive4NTar5CItemC2Ev, .Lfunc_end40-_ZN8NArchive4NTar5CItemC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp305-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp305
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp305-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp306-.Ltmp305       #   Call between .Ltmp305 and .Ltmp306
	.long	.Ltmp307-.Lfunc_begin13 #     jumps to .Ltmp307
	.byte	0                       #   On action: cleanup
	.long	.Ltmp308-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp309-.Ltmp308       #   Call between .Ltmp308 and .Ltmp309
	.long	.Ltmp310-.Lfunc_begin13 #     jumps to .Ltmp310
	.byte	0                       #   On action: cleanup
	.long	.Ltmp311-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp312-.Ltmp311       #   Call between .Ltmp311 and .Ltmp312
	.long	.Ltmp313-.Lfunc_begin13 #     jumps to .Ltmp313
	.byte	0                       #   On action: cleanup
	.long	.Ltmp312-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Lfunc_end40-.Ltmp312   #   Call between .Ltmp312 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NTar7CItemExEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi139:
	.cfi_def_cfa_offset 32
.Lcfi140:
	.cfi_offset %rbx, -24
.Lcfi141:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE+16, (%rbx)
.Ltmp314:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp315:
# BB#1:
.Ltmp320:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp321:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB41_5:
.Ltmp322:
	movq	%rax, %r14
	jmp	.LBB41_6
.LBB41_3:
.Ltmp316:
	movq	%rax, %r14
.Ltmp317:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp318:
.LBB41_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB41_4:
.Ltmp319:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end41:
	.size	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED0Ev, .Lfunc_end41-_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table41:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp314-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp315-.Ltmp314       #   Call between .Ltmp314 and .Ltmp315
	.long	.Ltmp316-.Lfunc_begin14 #     jumps to .Ltmp316
	.byte	0                       #   On action: cleanup
	.long	.Ltmp320-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp321-.Ltmp320       #   Call between .Ltmp320 and .Ltmp321
	.long	.Ltmp322-.Lfunc_begin14 #     jumps to .Ltmp322
	.byte	0                       #   On action: cleanup
	.long	.Ltmp317-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp318-.Ltmp317       #   Call between .Ltmp317 and .Ltmp318
	.long	.Ltmp319-.Lfunc_begin14 #     jumps to .Ltmp319
	.byte	1                       #   On action: 1
	.long	.Ltmp318-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end41-.Ltmp318   #   Call between .Ltmp318 and .Lfunc_end41
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NTar7CItemExEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NTar7CItemExEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NTar7CItemExEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NTar7CItemExEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi146:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi148:
	.cfi_def_cfa_offset 64
.Lcfi149:
	.cfi_offset %rbx, -56
.Lcfi150:
	.cfi_offset %r12, -48
.Lcfi151:
	.cfi_offset %r13, -40
.Lcfi152:
	.cfi_offset %r14, -32
.Lcfi153:
	.cfi_offset %r15, -24
.Lcfi154:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB42_13
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB42_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB42_12
# BB#3:                                 #   in Loop: Header=BB42_2 Depth=1
	movq	80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB42_5
# BB#4:                                 #   in Loop: Header=BB42_2 Depth=1
	callq	_ZdaPv
.LBB42_5:                               # %_ZN11CStringBaseIcED2Ev.exit.i
                                        #   in Loop: Header=BB42_2 Depth=1
	movq	64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB42_7
# BB#6:                                 #   in Loop: Header=BB42_2 Depth=1
	callq	_ZdaPv
.LBB42_7:                               # %_ZN11CStringBaseIcED2Ev.exit1.i
                                        #   in Loop: Header=BB42_2 Depth=1
	movq	48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB42_9
# BB#8:                                 #   in Loop: Header=BB42_2 Depth=1
	callq	_ZdaPv
.LBB42_9:                               # %_ZN11CStringBaseIcED2Ev.exit2.i
                                        #   in Loop: Header=BB42_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB42_11
# BB#10:                                #   in Loop: Header=BB42_2 Depth=1
	callq	_ZdaPv
.LBB42_11:                              # %_ZN8NArchive4NTar5CItemD2Ev.exit
                                        #   in Loop: Header=BB42_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB42_12:                              #   in Loop: Header=BB42_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB42_2
.LBB42_13:                              # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end42:
	.size	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEE6DeleteEii, .Lfunc_end42-_ZN13CObjectVectorIN8NArchive4NTar7CItemExEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN8NArchive4NTar5CItemC2ERKS1_,"axG",@progbits,_ZN8NArchive4NTar5CItemC2ERKS1_,comdat
	.weak	_ZN8NArchive4NTar5CItemC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar5CItemC2ERKS1_,@function
_ZN8NArchive4NTar5CItemC2ERKS1_:        # @_ZN8NArchive4NTar5CItemC2ERKS1_
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%rbp
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi156:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi157:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi158:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi159:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 64
.Lcfi162:
	.cfi_offset %rbx, -56
.Lcfi163:
	.cfi_offset %r12, -48
.Lcfi164:
	.cfi_offset %r13, -40
.Lcfi165:
	.cfi_offset %r14, -32
.Lcfi166:
	.cfi_offset %r15, -24
.Lcfi167:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movslq	8(%rbp), %rax
	leaq	1(%rax), %rbx
	testl	%ebx, %ebx
	je	.LBB43_1
# BB#2:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%rbx, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r14)
	movb	$0, (%rax)
	movl	%ebx, 12(%r14)
	jmp	.LBB43_3
.LBB43_1:
	xorl	%eax, %eax
.LBB43_3:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	(%rbp), %rcx
	.p2align	4, 0x90
.LBB43_4:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB43_4
# BB#5:                                 # %_ZN11CStringBaseIcEC2ERKS0_.exit
	movl	8(%rbp), %eax
	movl	%eax, 8(%r14)
	movups	16(%rbp), %xmm0
	movups	32(%rbp), %xmm1
	movups	%xmm1, 32(%r14)
	movups	%xmm0, 16(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r14)
	movslq	56(%rbp), %rax
	leaq	1(%rax), %r15
	testl	%r15d, %r15d
	je	.LBB43_6
# BB#7:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r15, %rdi
	cmovlq	%rax, %rdi
.Ltmp323:
	callq	_Znam
.Ltmp324:
# BB#8:                                 # %.noexc
	movq	%rax, 48(%r14)
	movb	$0, (%rax)
	movl	%r15d, 60(%r14)
	jmp	.LBB43_9
.LBB43_6:
	xorl	%eax, %eax
.LBB43_9:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i10
	leaq	48(%r14), %r15
	movq	48(%rbp), %rcx
	.p2align	4, 0x90
.LBB43_10:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB43_10
# BB#11:
	movl	56(%rbp), %eax
	movl	%eax, 56(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%r14)
	movslq	72(%rbp), %rax
	leaq	1(%rax), %r12
	testl	%r12d, %r12d
	je	.LBB43_12
# BB#13:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
.Ltmp326:
	callq	_Znam
.Ltmp327:
# BB#14:                                # %.noexc17
	movq	%rax, 64(%r14)
	movb	$0, (%rax)
	movl	%r12d, 76(%r14)
	jmp	.LBB43_15
.LBB43_12:
	xorl	%eax, %eax
.LBB43_15:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i14
	leaq	64(%r14), %r12
	movq	64(%rbp), %rcx
	.p2align	4, 0x90
.LBB43_16:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB43_16
# BB#17:
	movl	72(%rbp), %eax
	movl	%eax, 72(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 80(%r14)
	movslq	88(%rbp), %rax
	leaq	1(%rax), %r13
	testl	%r13d, %r13d
	je	.LBB43_18
# BB#19:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r13, %rdi
	cmovlq	%rax, %rdi
.Ltmp329:
	callq	_Znam
.Ltmp330:
# BB#20:                                # %.noexc22
	movq	%rax, 80(%r14)
	movb	$0, (%rax)
	movl	%r13d, 92(%r14)
	jmp	.LBB43_21
.LBB43_18:
	xorl	%eax, %eax
.LBB43_21:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i19
	movq	80(%rbp), %rcx
	.p2align	4, 0x90
.LBB43_22:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB43_22
# BB#23:
	movl	88(%rbp), %eax
	movl	%eax, 88(%r14)
	movb	106(%rbp), %al
	movb	%al, 106(%r14)
	movzwl	104(%rbp), %eax
	movw	%ax, 104(%r14)
	movq	96(%rbp), %rax
	movq	%rax, 96(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB43_26:
.Ltmp331:
	movq	%rax, %rbx
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB43_28
# BB#27:
	callq	_ZdaPv
	jmp	.LBB43_28
.LBB43_25:
.Ltmp328:
	movq	%rax, %rbx
.LBB43_28:                              # %_ZN11CStringBaseIcED2Ev.exit
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB43_30
# BB#29:
	callq	_ZdaPv
	jmp	.LBB43_30
.LBB43_24:
.Ltmp325:
	movq	%rax, %rbx
.LBB43_30:                              # %_ZN11CStringBaseIcED2Ev.exit24
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB43_32
# BB#31:
	callq	_ZdaPv
.LBB43_32:                              # %_ZN11CStringBaseIcED2Ev.exit25
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end43:
	.size	_ZN8NArchive4NTar5CItemC2ERKS1_, .Lfunc_end43-_ZN8NArchive4NTar5CItemC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin15-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp323-.Lfunc_begin15 #   Call between .Lfunc_begin15 and .Ltmp323
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp323-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin15 #     jumps to .Ltmp325
	.byte	0                       #   On action: cleanup
	.long	.Ltmp326-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp328-.Lfunc_begin15 #     jumps to .Ltmp328
	.byte	0                       #   On action: cleanup
	.long	.Ltmp329-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp331-.Lfunc_begin15 #     jumps to .Ltmp331
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin15 # >> Call Site 5 <<
	.long	.Lfunc_end43-.Ltmp330   #   Call between .Ltmp330 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%rbp
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi171:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi172:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi174:
	.cfi_def_cfa_offset 64
.Lcfi175:
	.cfi_offset %rbx, -56
.Lcfi176:
	.cfi_offset %r12, -48
.Lcfi177:
	.cfi_offset %r13, -40
.Lcfi178:
	.cfi_offset %r14, -32
.Lcfi179:
	.cfi_offset %r15, -24
.Lcfi180:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB44_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB44_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB44_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB44_5
.LBB44_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB44_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp332:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp333:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB44_35
# BB#12:
	movq	%rbx, %r13
.LBB44_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB44_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB44_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB44_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB44_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB44_15
.LBB44_14:
	xorl	%esi, %esi
.LBB44_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB44_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB44_16
.LBB44_29:
	movq	%r13, %rbx
.LBB44_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp334:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp335:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB44_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB44_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB44_8
.LBB44_3:
	xorl	%eax, %eax
.LBB44_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB44_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB44_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB44_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB44_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB44_30
.LBB44_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB44_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB44_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB44_24
	jmp	.LBB44_25
.LBB44_22:
	xorl	%ecx, %ecx
.LBB44_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB44_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB44_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB44_27
.LBB44_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB44_15
	jmp	.LBB44_29
.LBB44_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp336:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end44:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end44-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table44:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin16-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp332-.Lfunc_begin16 #   Call between .Lfunc_begin16 and .Ltmp332
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp332-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp335-.Ltmp332       #   Call between .Ltmp332 and .Ltmp335
	.long	.Ltmp336-.Lfunc_begin16 #     jumps to .Ltmp336
	.byte	0                       #   On action: cleanup
	.long	.Ltmp335-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Lfunc_end44-.Ltmp335   #   Call between .Ltmp335 and .Lfunc_end44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZN8NArchive4NTarL6kPropsE,@object # @_ZN8NArchive4NTarL6kPropsE
	.section	.rodata,"a",@progbits
	.p2align	4
_ZN8NArchive4NTarL6kPropsE:
	.quad	0
	.long	3                       # 0x3
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	6                       # 0x6
	.short	11                      # 0xb
	.zero	2
	.quad	0
	.long	7                       # 0x7
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	8                       # 0x8
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	12                      # 0xc
	.short	64                      # 0x40
	.zero	2
	.quad	0
	.long	53                      # 0x35
	.short	19                      # 0x13
	.zero	2
	.quad	0
	.long	25                      # 0x19
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	26                      # 0x1a
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	54                      # 0x36
	.short	8                       # 0x8
	.zero	2
	.size	_ZN8NArchive4NTarL6kPropsE, 144

	.type	_ZN8NArchive4NTarL9kArcPropsE,@object # @_ZN8NArchive4NTarL9kArcPropsE
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
_ZN8NArchive4NTarL9kArcPropsE:
	.quad	0
	.long	44                      # 0x2c
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	45                      # 0x2d
	.short	21                      # 0x15
	.zero	2
	.size	_ZN8NArchive4NTarL9kArcPropsE, 32

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	46                      # 0x2e
	.long	116                     # 0x74
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	0                       # 0x0
	.size	.L.str, 20

	.type	_ZTVN8NArchive4NTar8CHandlerE,@object # @_ZTVN8NArchive4NTar8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive4NTar8CHandlerE
	.p2align	3
_ZTVN8NArchive4NTar8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive4NTar8CHandlerE
	.quad	_ZN8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive4NTar8CHandler6AddRefEv
	.quad	_ZN8NArchive4NTar8CHandler7ReleaseEv
	.quad	_ZN8NArchive4NTar8CHandlerD2Ev
	.quad	_ZN8NArchive4NTar8CHandlerD0Ev
	.quad	_ZN8NArchive4NTar8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive4NTar8CHandler5CloseEv
	.quad	_ZN8NArchive4NTar8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive4NTar8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive4NTar8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive4NTar8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive4NTar8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive4NTar8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive4NTar8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive4NTar8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZN8NArchive4NTar8CHandler15GetFileTimeTypeEPj
	.quad	_ZN8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream
	.quad	_ZN8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream
	.quad	-8
	.quad	_ZTIN8NArchive4NTar8CHandlerE
	.quad	_ZThn8_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive4NTar8CHandler6AddRefEv
	.quad	_ZThn8_N8NArchive4NTar8CHandler7ReleaseEv
	.quad	_ZThn8_N8NArchive4NTar8CHandlerD1Ev
	.quad	_ZThn8_N8NArchive4NTar8CHandlerD0Ev
	.quad	_ZThn8_N8NArchive4NTar8CHandler7OpenSeqEP19ISequentialInStream
	.quad	-16
	.quad	_ZTIN8NArchive4NTar8CHandlerE
	.quad	_ZThn16_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N8NArchive4NTar8CHandler6AddRefEv
	.quad	_ZThn16_N8NArchive4NTar8CHandler7ReleaseEv
	.quad	_ZThn16_N8NArchive4NTar8CHandlerD1Ev
	.quad	_ZThn16_N8NArchive4NTar8CHandlerD0Ev
	.quad	_ZThn16_N8NArchive4NTar8CHandler9GetStreamEjPP19ISequentialInStream
	.quad	-24
	.quad	_ZTIN8NArchive4NTar8CHandlerE
	.quad	_ZThn24_N8NArchive4NTar8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn24_N8NArchive4NTar8CHandler6AddRefEv
	.quad	_ZThn24_N8NArchive4NTar8CHandler7ReleaseEv
	.quad	_ZThn24_N8NArchive4NTar8CHandlerD1Ev
	.quad	_ZThn24_N8NArchive4NTar8CHandlerD0Ev
	.quad	_ZThn24_N8NArchive4NTar8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.quad	_ZThn24_N8NArchive4NTar8CHandler15GetFileTimeTypeEPj
	.size	_ZTVN8NArchive4NTar8CHandlerE, 368

	.type	_ZTSN8NArchive4NTar8CHandlerE,@object # @_ZTSN8NArchive4NTar8CHandlerE
	.globl	_ZTSN8NArchive4NTar8CHandlerE
	.p2align	4
_ZTSN8NArchive4NTar8CHandlerE:
	.asciz	"N8NArchive4NTar8CHandlerE"
	.size	_ZTSN8NArchive4NTar8CHandlerE, 26

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS15IArchiveOpenSeq,@object # @_ZTS15IArchiveOpenSeq
	.section	.rodata._ZTS15IArchiveOpenSeq,"aG",@progbits,_ZTS15IArchiveOpenSeq,comdat
	.weak	_ZTS15IArchiveOpenSeq
	.p2align	4
_ZTS15IArchiveOpenSeq:
	.asciz	"15IArchiveOpenSeq"
	.size	_ZTS15IArchiveOpenSeq, 18

	.type	_ZTI15IArchiveOpenSeq,@object # @_ZTI15IArchiveOpenSeq
	.section	.rodata._ZTI15IArchiveOpenSeq,"aG",@progbits,_ZTI15IArchiveOpenSeq,comdat
	.weak	_ZTI15IArchiveOpenSeq
	.p2align	4
_ZTI15IArchiveOpenSeq:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15IArchiveOpenSeq
	.quad	_ZTI8IUnknown
	.size	_ZTI15IArchiveOpenSeq, 24

	.type	_ZTS19IInArchiveGetStream,@object # @_ZTS19IInArchiveGetStream
	.section	.rodata._ZTS19IInArchiveGetStream,"aG",@progbits,_ZTS19IInArchiveGetStream,comdat
	.weak	_ZTS19IInArchiveGetStream
	.p2align	4
_ZTS19IInArchiveGetStream:
	.asciz	"19IInArchiveGetStream"
	.size	_ZTS19IInArchiveGetStream, 22

	.type	_ZTI19IInArchiveGetStream,@object # @_ZTI19IInArchiveGetStream
	.section	.rodata._ZTI19IInArchiveGetStream,"aG",@progbits,_ZTI19IInArchiveGetStream,comdat
	.weak	_ZTI19IInArchiveGetStream
	.p2align	4
_ZTI19IInArchiveGetStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19IInArchiveGetStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19IInArchiveGetStream, 24

	.type	_ZTS11IOutArchive,@object # @_ZTS11IOutArchive
	.section	.rodata._ZTS11IOutArchive,"aG",@progbits,_ZTS11IOutArchive,comdat
	.weak	_ZTS11IOutArchive
_ZTS11IOutArchive:
	.asciz	"11IOutArchive"
	.size	_ZTS11IOutArchive, 14

	.type	_ZTI11IOutArchive,@object # @_ZTI11IOutArchive
	.section	.rodata._ZTI11IOutArchive,"aG",@progbits,_ZTI11IOutArchive,comdat
	.weak	_ZTI11IOutArchive
	.p2align	4
_ZTI11IOutArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11IOutArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI11IOutArchive, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive4NTar8CHandlerE,@object # @_ZTIN8NArchive4NTar8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive4NTar8CHandlerE
	.p2align	4
_ZTIN8NArchive4NTar8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive4NTar8CHandlerE
	.long	1                       # 0x1
	.long	5                       # 0x5
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI15IArchiveOpenSeq
	.quad	2050                    # 0x802
	.quad	_ZTI19IInArchiveGetStream
	.quad	4098                    # 0x1002
	.quad	_ZTI11IOutArchive
	.quad	6146                    # 0x1802
	.quad	_ZTI13CMyUnknownImp
	.quad	8194                    # 0x2002
	.size	_ZTIN8NArchive4NTar8CHandlerE, 104

	.type	_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE,@object # @_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NTar7CItemExEE
	.quad	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NTar7CItemExEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NTar7CItemExEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NTar7CItemExEE,@object # @_ZTS13CObjectVectorIN8NArchive4NTar7CItemExEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NTar7CItemExEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NTar7CItemExEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NTar7CItemExEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NTar7CItemExEE:
	.asciz	"13CObjectVectorIN8NArchive4NTar7CItemExEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NTar7CItemExEE, 42

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive4NTar7CItemExEE,@object # @_ZTI13CObjectVectorIN8NArchive4NTar7CItemExEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NTar7CItemExEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NTar7CItemExEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NTar7CItemExEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NTar7CItemExEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NTar7CItemExEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NTar7CItemExEE, 24


	.globl	_ZN8NArchive4NTar8CHandlerC1Ev
	.type	_ZN8NArchive4NTar8CHandlerC1Ev,@function
_ZN8NArchive4NTar8CHandlerC1Ev = _ZN8NArchive4NTar8CHandlerC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
