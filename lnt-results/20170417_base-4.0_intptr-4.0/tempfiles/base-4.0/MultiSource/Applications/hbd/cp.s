	.text
	.file	"cp.bc"
	.globl	_ZN9ConstPool4readEP9ClassfilePt
	.p2align	4, 0x90
	.type	_ZN9ConstPool4readEP9ClassfilePt,@function
_ZN9ConstPool4readEP9ClassfilePt:       # @_ZN9ConstPool4readEP9ClassfilePt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movw	%ax, (%r12)
	movzwl	%ax, %ebx
	movq	%rbx, %rdi
	shlq	$4, %rdi
	callq	_Znam
	movq	%rax, %r15
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%r15, 8(%r12)
	movb	$0, (%r15)
	movl	%ebx, %r13d
	decl	%r13d
	je	.LBB0_39
# BB#1:                                 # %.lr.ph
	addl	$-2, %ebx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	$1, %ebx
	movq	%r14, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_2:                                # %.backedge._crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	-1(%r13), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r15
	movl	%ebp, %ebx
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %r12
	movq	(%r14), %rdi
	callq	_IO_getc
	shlq	$4, %r12
	movb	%al, (%r15,%r12)
	cmpl	$0, debugon(%rip)
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	leaq	(%r15,%r12), %rbp
	movq	8(%r14), %rdi
	movl	16(%r14), %edx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movl	%ebx, %ecx
	callq	fprintf
	movb	(%rbp), %al
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	leal	1(%rbx), %ebp
	decb	%al
	cmpb	$11, %al
	ja	.LBB0_34
# BB#6:                                 #   in Loop: Header=BB0_3 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_7:                                #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movl	$4, %edi
	callq	_Znwm
	movq	%rax, 8(%r15,%r12)
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movq	8(%r15,%r12), %rcx
	movw	%ax, (%rcx)
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movq	8(%r15,%r12), %rcx
	movw	%ax, 2(%rcx)
	cmpl	$0, debugon(%rip)
	je	.LBB0_32
# BB#8:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	8(%r14), %rdi
	movzwl	(%rcx), %edx
	movzwl	%ax, %ecx
	movl	$.L.str.10, %esi
	jmp	.LBB0_31
.LBB0_9:                                #   in Loop: Header=BB0_3 Depth=1
	movq	(%r14), %r13
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r13, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %eax
	orl	%ebx, %eax
	movzwl	%ax, %ebx
	leal	1(%rbx), %edi
	callq	_Znam
	movq	%rax, 8(%r15,%r12)
	addl	%ebx, 16(%r14)
	movq	(%r14), %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	fread
	movq	8(%r15,%r12), %rax
	movb	$0, (%rax,%rbx)
	cmpl	$0, debugon(%rip)
	je	.LBB0_35
# BB#10:                                #   in Loop: Header=BB0_3 Depth=1
	leaq	8(%r15,%r12), %rax
	movq	8(%r14), %rdi
	movq	(%rax), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB0_35
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=1
	cmpl	$0, debugon(%rip)
	je	.LBB0_35
# BB#12:                                #   in Loop: Header=BB0_3 Depth=1
	movq	8(%r14), %rcx
	movl	$.L.str.2, %edi
	movl	$8, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB0_35
.LBB0_13:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	(%r14), %r13
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r13, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %eax
	movzwl	%bx, %ebx
	orq	%rax, %rbx
	movq	%r13, %rdi
	callq	_IO_getc
	movq	%r14, %rbp
	movl	%eax, %r14d
	movq	%r13, %rdi
	callq	_IO_getc
	shll	$8, %r14d
	movzbl	%al, %eax
	movzwl	%r14w, %ecx
	orq	%rax, %rcx
	shlq	$16, %rbx
	orq	%rcx, %rbx
	movq	%rbx, 8(%r15,%r12)
	cmpl	$0, debugon(%rip)
	je	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_3 Depth=1
	movq	8(%rbp), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
.LBB0_15:                               #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	movq	%rbp, %r14
	jmp	.LBB0_33
.LBB0_16:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%r14, %r13
	movq	(%r13), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %eax
	movzwl	%bx, %ebp
	orq	%rax, %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebx
	movzbl	%al, %eax
	movzwl	%bx, %ecx
	orq	%rax, %rcx
	shlq	$16, %rbp
	orq	%rcx, %rbp
	movq	%rbp, 8(%r15,%r12)
	cmpl	$0, debugon(%rip)
	je	.LBB0_36
# BB#17:                                #   in Loop: Header=BB0_3 Depth=1
	movd	%ebp, %xmm0
	movq	8(%r13), %rdi
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.4, %esi
	movb	$1, %al
	callq	fprintf
	movq	%r13, %r14
	jmp	.LBB0_32
.LBB0_18:                               #   in Loop: Header=BB0_3 Depth=1
	movq	(%r14), %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	callq	_IO_getc
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movl	%eax, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %r14d
	orq	%rax, %r14
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	movl	%eax, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ecx
	orq	%rax, %rcx
	shlq	$16, %r14
	orq	%rcx, %r14
	movq	%r14, 8(%r15,%r12)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	12(%rsp), %r12          # 4-byte Folded Reload
	shlq	$4, %r12
	movb	$0, (%rax,%r12)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r14, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ebp
	orq	%rax, %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r15d
	movq	%r14, %rdi
	movq	24(%rsp), %r14          # 8-byte Reload
	callq	_IO_getc
	shll	$8, %r15d
	movzbl	%al, %eax
	movzwl	%r15w, %ecx
	orq	%rax, %rcx
	movq	16(%rsp), %rax          # 8-byte Reload
	shlq	$16, %rbp
	orq	%rcx, %rbp
	movq	%rbp, 8(%rax,%r12)
	cmpl	$0, debugon(%rip)
	je	.LBB0_38
# BB#19:                                #   in Loop: Header=BB0_3 Depth=1
	addq	%r12, %rax
	movq	-8(%rax), %rdx
	testq	%rdx, %rdx
	movq	8(%r14), %rdi
	je	.LBB0_37
# BB#20:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbp, %rcx
	callq	fprintf
	jmp	.LBB0_38
.LBB0_21:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	(%r14), %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	callq	_IO_getc
	movl	%eax, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %r14d
	orq	%rax, %r14
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	movl	%eax, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ecx
	orq	%rax, %rcx
	shlq	$16, %r14
	orq	%rcx, %r14
	movq	%r14, 16(%r15,%r12)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbp
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	movq	%rbp, %rdi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	callq	_IO_getc
	shll	$8, %r14d
	movzbl	%al, %eax
	movzwl	%r14w, %r14d
	orq	%rax, %r14
	movq	%rbp, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	movzwl	%bp, %ecx
	orq	%rax, %rcx
	shlq	$16, %r14
	orq	%rcx, %r14
	movq	%r14, 8(%r15,%r12)
	movq	24(%rsp), %r15          # 8-byte Reload
	cmpl	$0, debugon(%rip)
	je	.LBB0_23
# BB#22:                                #   in Loop: Header=BB0_3 Depth=1
	movd	%r14, %xmm0
	movq	8(%r15), %rdi
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.7, %esi
	movb	$1, %al
	callq	fprintf
.LBB0_23:                               #   in Loop: Header=BB0_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	addl	$2, %ebx
	movslq	12(%rsp), %rcx          # 4-byte Folded Reload
	shlq	$4, %rcx
	movb	$0, (%rax,%rcx)
	addl	$-2, %r13d
	movl	%ebx, %ebp
	movq	%r15, %r14
	testl	%r13d, %r13d
	jne	.LBB0_2
	jmp	.LBB0_39
.LBB0_24:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movzwl	%ax, %edx
	movq	%rdx, 8(%r15,%r12)
	cmpl	$0, debugon(%rip)
	je	.LBB0_32
# BB#25:                                #   in Loop: Header=BB0_3 Depth=1
	movq	8(%r14), %rdi
	movl	$.L.str.8, %esi
	jmp	.LBB0_28
.LBB0_26:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movzwl	%ax, %edx
	movq	%rdx, 8(%r15,%r12)
	cmpl	$0, debugon(%rip)
	je	.LBB0_32
# BB#27:                                #   in Loop: Header=BB0_3 Depth=1
	movq	8(%r14), %rdi
	movl	$.L.str.9, %esi
.LBB0_28:                               # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	jmp	.LBB0_32
.LBB0_29:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movl	$4, %edi
	callq	_Znwm
	movq	%rax, 8(%r15,%r12)
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movq	8(%r15,%r12), %rcx
	movw	%ax, (%rcx)
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
	shll	$8, %ebp
	movzbl	%al, %eax
	orl	%ebp, %eax
	movq	8(%r15,%r12), %rcx
	movw	%ax, 2(%rcx)
	cmpl	$0, debugon(%rip)
	je	.LBB0_32
# BB#30:                                #   in Loop: Header=BB0_3 Depth=1
	movq	8(%r14), %rdi
	movzwl	(%rcx), %edx
	movzwl	%ax, %ecx
	movl	$.L.str.11, %esi
.LBB0_31:                               # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
	callq	fprintf
.LBB0_32:                               #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
.LBB0_33:                               #   in Loop: Header=BB0_3 Depth=1
	movl	12(%rsp), %ebp          # 4-byte Reload
	testl	%r13d, %r13d
	jne	.LBB0_2
	jmp	.LBB0_39
.LBB0_34:                               #   in Loop: Header=BB0_3 Depth=1
	movq	stderr(%rip), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	(%rax), %ecx
	movl	16(%r14), %r8d
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	fprintf
	movl	$6, %edi
	xorl	%eax, %eax
	callq	_Z10fatalerroriz
.LBB0_35:                               # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	testl	%r13d, %r13d
	jne	.LBB0_2
	jmp	.LBB0_39
.LBB0_36:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%r13, %r14
	jmp	.LBB0_32
.LBB0_37:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
.LBB0_38:                               #   in Loop: Header=BB0_3 Depth=1
	addl	$2, %ebx
	addl	$-2, %r13d
	movl	%ebx, %ebp
	testl	%r13d, %r13d
	jne	.LBB0_2
.LBB0_39:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN9ConstPool4readEP9ClassfilePt, .Lfunc_end0-_ZN9ConstPool4readEP9ClassfilePt
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_9
	.quad	.LBB0_11
	.quad	.LBB0_13
	.quad	.LBB0_16
	.quad	.LBB0_18
	.quad	.LBB0_21
	.quad	.LBB0_24
	.quad	.LBB0_26
	.quad	.LBB0_7
	.quad	.LBB0_7
	.quad	.LBB0_7
	.quad	.LBB0_29

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\npos: 0x%05X\tindex: %4d\t"
	.size	.L.str, 25

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"UTF8: %s\t"
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Unicode\t"
	.size	.L.str.2, 9

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"32-bit int: 0x%8lX\t"
	.size	.L.str.3, 20

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"32-bit float: %.25G\t"
	.size	.L.str.4, 21

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"64-bit int: 0x%lX%08lX"
	.size	.L.str.5, 23

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"64-bit int: 0x%lX"
	.size	.L.str.6, 18

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"64-bit float: %.25G\t"
	.size	.L.str.7, 21

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Class: name = index %d\t"
	.size	.L.str.8, 24

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"String: index %d\t"
	.size	.L.str.9, 18

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Ref: class_index %d, n&t_index %d\t"
	.size	.L.str.10, 35

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Name&Type: name_index %d, sig_index %d\t"
	.size	.L.str.11, 40

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Error reading constant pool entry %d of %d at file pos 0x%08x!\n"
	.size	.L.str.12, 64


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
