	.text
	.file	"HuffEnc.bc"
	.globl	Huffman_Generate
	.p2align	4, 0x90
	.type	Huffman_Generate,@function
Huffman_Generate:                       # @Huffman_Generate
	.cfi_startproc
# BB#0:                                 # %.preheader189
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi6:
	.cfi_def_cfa_offset 416
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %r14
	movq	%rsi, %r15
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 336(%rsp)
	movaps	%xmm0, 320(%rsp)
	movaps	%xmm0, 304(%rsp)
	movaps	%xmm0, 288(%rsp)
	movaps	%xmm0, 272(%rsp)
	movaps	%xmm0, 256(%rsp)
	movaps	%xmm0, 240(%rsp)
	movaps	%xmm0, 224(%rsp)
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm0, 176(%rsp)
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	testl	%r12d, %r12d
	je	.LBB0_1
# BB#7:                                 # %.lr.ph212.preheader
	movl	%r12d, %eax
	testb	$1, %al
	jne	.LBB0_9
# BB#8:
	xorl	%ecx, %ecx
	cmpl	$1, %r12d
	jne	.LBB0_11
	jmp	.LBB0_1
.LBB0_9:                                # %.lr.ph212.prol
	movl	(%rdi), %ecx
	cmpl	$63, %ecx
	movl	$63, %edx
	cmovbl	%ecx, %edx
	incl	96(%rsp,%rdx,4)
	movl	$1, %ecx
	cmpl	$1, %r12d
	je	.LBB0_1
.LBB0_11:                               # %.lr.ph212.preheader.new
	subq	%rcx, %rax
	leaq	4(%rdi,%rcx,4), %rcx
	movl	$63, %edx
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph212
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx), %esi
	cmpl	$63, %esi
	cmovael	%edx, %esi
	incl	96(%rsp,%rsi,4)
	movl	(%rcx), %esi
	cmpl	$63, %esi
	cmovael	%edx, %esi
	incl	96(%rsp,%rsi,4)
	addq	$8, %rcx
	addq	$-2, %rax
	jne	.LBB0_12
.LBB0_1:                                # %.preheader188.preheader
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader188
                                        # =>This Inner Loop Header: Depth=1
	movl	100(%rsp,%rax,4), %ecx
	addl	%ebp, %ecx
	movl	%ebp, 100(%rsp,%rax,4)
	movl	104(%rsp,%rax,4), %edx
	addl	%ecx, %edx
	movl	%ecx, 104(%rsp,%rax,4)
	movl	108(%rsp,%rax,4), %ebp
	addl	%edx, %ebp
	movl	%edx, 108(%rsp,%rax,4)
	addq	$3, %rax
	cmpq	$63, %rax
	jne	.LBB0_2
# BB#3:                                 # %.preheader187
	testl	%r12d, %r12d
	je	.LBB0_15
# BB#4:                                 # %.lr.ph207.preheader
	movl	%r12d, %r9d
	xorl	%ecx, %ecx
	movl	$63, %r8d
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph207
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	testl	%esi, %esi
	je	.LBB0_6
# BB#13:                                #   in Loop: Header=BB0_5 Depth=1
	movl	%esi, %edx
	shll	$10, %edx
	movl	%ecx, %eax
	orl	%edx, %eax
	cmpl	$63, %esi
	cmovael	%r8d, %esi
	movl	96(%rsp,%rsi,4), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, 96(%rsp,%rsi,4)
	movl	%eax, (%r15,%rdx,4)
	jmp	.LBB0_14
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_5 Depth=1
	movb	$0, (%r14,%rcx)
.LBB0_14:                               #   in Loop: Header=BB0_5 Depth=1
	incq	%rcx
	cmpq	%rcx, %r9
	jne	.LBB0_5
.LBB0_15:                               # %._crit_edge208
	movl	$0, 96(%rsp)
	movl	344(%rsp), %eax
	movl	348(%rsp), %esi
	leaq	(%r15,%rax,4), %rdi
	subl	%eax, %esi
	callq	HeapSort
	cmpl	$2, %ebp
	jae	.LBB0_16
# BB#21:
	cmpl	$1, %ebp
	jne	.LBB0_22
# BB#23:
	movl	$1023, %ecx             # imm = 0x3FF
	xorl	%eax, %eax
	andl	(%r15), %ecx
	sete	%al
	addl	%ecx, %eax
	jmp	.LBB0_24
.LBB0_16:                               # %.preheader186.preheader
	movl	%r12d, 8(%rsp)          # 4-byte Spill
	leal	-2(%rbp), %r10d
	leal	-1(%rbp), %r9d
	xorl	%edx, %edx
	movl	$1023, %r8d             # imm = 0x3FF
	xorl	%edi, %edi
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_17:                               # %.preheader186
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %r12d
	je	.LBB0_25
# BB#18:                                #   in Loop: Header=BB0_17 Depth=1
	movl	%r11d, %ecx
	cmpq	%rcx, %rdi
	je	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_17 Depth=1
	movl	%r12d, %esi
	movl	(%r15,%rsi,4), %esi
	shrl	$10, %esi
	movl	(%r15,%rcx,4), %ecx
	shrl	$10, %ecx
	cmpl	%ecx, %esi
	jbe	.LBB0_20
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_17 Depth=1
	leal	1(%r11), %r13d
	movl	%r12d, %esi
	movl	%r11d, %r12d
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_17 Depth=1
	leal	1(%r12), %esi
	movl	%r11d, %r13d
.LBB0_26:                               #   in Loop: Header=BB0_17 Depth=1
	movl	%r12d, %eax
	movl	(%r15,%rax,4), %ecx
	movl	%ecx, %ebx
	andl	$-1024, %ebx            # imm = 0xFC00
	andl	$1023, %ecx             # imm = 0x3FF
	orl	%edx, %ecx
	movl	%ecx, (%r15,%rax,4)
	cmpl	%ebp, %esi
	je	.LBB0_30
# BB#27:                                #   in Loop: Header=BB0_17 Depth=1
	movl	%r13d, %eax
	cmpq	%rax, %rdi
	je	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_17 Depth=1
	movl	%esi, %ecx
	movl	(%r15,%rcx,4), %ecx
	shrl	$10, %ecx
	movl	(%r15,%rax,4), %eax
	shrl	$10, %eax
	cmpl	%eax, %ecx
	jbe	.LBB0_29
	.p2align	4, 0x90
.LBB0_30:                               #   in Loop: Header=BB0_17 Depth=1
	leal	1(%r13), %r11d
	movl	%esi, %r12d
	movl	%r13d, %esi
	jmp	.LBB0_31
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_17 Depth=1
	leal	1(%rsi), %r12d
	movl	%r13d, %r11d
.LBB0_31:                               #   in Loop: Header=BB0_17 Depth=1
	movl	%esi, %ecx
	movl	(%r15,%rcx,4), %esi
	movl	%esi, %eax
	andl	$-1024, %eax            # imm = 0xFC00
	addl	%ebx, %eax
	andl	$1023, %esi             # imm = 0x3FF
	orl	%edx, %esi
	movl	%esi, (%r15,%rcx,4)
	movl	(%r15,%rdi,4), %ecx
	andl	%r8d, %ecx
	orl	%eax, %ecx
	movl	%ecx, (%r15,%rdi,4)
	incq	%rdi
	addl	$1024, %edx             # imm = 0x400
	cmpq	%rdi, %r9
	jne	.LBB0_17
# BB#32:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movl	$0, 160(%rsp)
	andl	$1023, (%r15,%r10,4)    # imm = 0x3FF
	movl	$2, 100(%rsp)
	testl	%r10d, %r10d
	movl	8(%rsp), %r9d           # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	je	.LBB0_33
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph204
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_48 Depth 2
	leal	-1(%r10), %eax
	movl	(%r15,%rax,4), %ecx
	movl	%ecx, %edx
	shrl	$10, %edx
	movl	(%r15,%rdx,4), %edx
	shrl	$10, %edx
	incl	%edx
	andl	$1023, %ecx             # imm = 0x3FF
	movl	%edx, %esi
	shll	$10, %esi
	orl	%ecx, %esi
	movl	%esi, (%r15,%rax,4)
	cmpl	%edi, %edx
	movl	%edi, %ecx
	jae	.LBB0_48
# BB#47:                                # %.lr.ph204..loopexit_crit_edge
                                        #   in Loop: Header=BB0_46 Depth=1
	movl	%edx, %ecx
	movl	96(%rsp,%rcx,4), %esi
	movl	%edx, %ecx
	jmp	.LBB0_49
	.p2align	4, 0x90
.LBB0_48:                               # %.preheader185
                                        #   Parent Loop BB0_46 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ecx
	movl	96(%rsp,%rcx,4), %esi
	testl	%esi, %esi
	je	.LBB0_48
.LBB0_49:                               # %.loopexit
                                        #   in Loop: Header=BB0_46 Depth=1
	movl	%ecx, %edx
	decl	%esi
	movl	%esi, 96(%rsp,%rdx,4)
	incl	%ecx
	addl	$2, 96(%rsp,%rcx,4)
	decq	%r10
	testl	%eax, %eax
	jne	.LBB0_46
.LBB0_33:                               # %.preheader184
	testl	%edi, %edi
	je	.LBB0_43
# BB#34:                                # %.lr.ph200.preheader
	movl	%edi, %eax
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_35:                               # %.lr.ph200
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_38 Depth 2
                                        #     Child Loop BB0_40 Depth 2
	movl	96(%rsp,%rax,4), %edx
	testl	%edx, %edx
	je	.LBB0_42
# BB#36:                                # %.lr.ph196
                                        #   in Loop: Header=BB0_35 Depth=1
	leal	-1(%rdx), %ebp
	movl	%edx, %ecx
	andl	$3, %ecx
	movl	%edx, %esi
	movl	%r8d, %edi
	je	.LBB0_39
# BB#37:                                # %.prol.preheader
                                        #   in Loop: Header=BB0_35 Depth=1
	negl	%ecx
	movl	%edx, %esi
	movl	%r8d, %edi
	.p2align	4, 0x90
.LBB0_38:                               #   Parent Loop BB0_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ebx
	incl	%edi
	movl	(%r15,%rbx,4), %ebx
	andl	$1023, %ebx             # imm = 0x3FF
	movb	%al, (%r14,%rbx)
	decl	%esi
	incl	%ecx
	jne	.LBB0_38
.LBB0_39:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_35 Depth=1
	cmpl	$3, %ebp
	jb	.LBB0_41
	.p2align	4, 0x90
.LBB0_40:                               #   Parent Loop BB0_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%rdi), %ecx
	movl	%edi, %ebp
	movl	(%r15,%rbp,4), %ebp
	andl	$1023, %ebp             # imm = 0x3FF
	movb	%al, (%r14,%rbp)
	leal	2(%rdi), %ebp
	movl	(%r15,%rcx,4), %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	movb	%al, (%r14,%rcx)
	leal	3(%rdi), %ecx
	movl	(%r15,%rbp,4), %ebp
	andl	$1023, %ebp             # imm = 0x3FF
	movb	%al, (%r14,%rbp)
	addl	$4, %edi
	movl	(%r15,%rcx,4), %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	movb	%al, (%r14,%rcx)
	addl	$-4, %esi
	jne	.LBB0_40
.LBB0_41:                               # %._crit_edge197.loopexit
                                        #   in Loop: Header=BB0_35 Depth=1
	addl	%edx, %r8d
.LBB0_42:                               # %._crit_edge197
                                        #   in Loop: Header=BB0_35 Depth=1
	movq	%rax, %rcx
	decq	%rcx
	cmpl	$1, %eax
	movq	%rcx, %rax
	jne	.LBB0_35
.LBB0_43:                               # %._crit_edge201
	movl	96(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 20(%rsp)
	addl	100(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 24(%rsp)
	addl	104(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 28(%rsp)
	addl	108(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 32(%rsp)
	addl	112(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 36(%rsp)
	addl	116(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 40(%rsp)
	addl	120(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 44(%rsp)
	addl	124(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 48(%rsp)
	addl	128(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 52(%rsp)
	addl	132(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 56(%rsp)
	addl	136(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 60(%rsp)
	addl	140(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 64(%rsp)
	addl	144(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 68(%rsp)
	addl	148(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 72(%rsp)
	addl	152(%rsp), %eax
	addl	%eax, %eax
	movl	%eax, 76(%rsp)
	addl	156(%rsp), %eax
	addl	%eax, %eax
	testl	%r9d, %r9d
	movl	%eax, 80(%rsp)
	je	.LBB0_54
# BB#44:                                # %.lr.ph.preheader
	movl	%r9d, %eax
	testb	$1, %al
	jne	.LBB0_50
# BB#45:
	xorl	%edx, %edx
	cmpl	$1, %r9d
	jne	.LBB0_52
	jmp	.LBB0_54
.LBB0_22:
	movl	$1, %eax
.LBB0_24:
	movl	$0, (%r15)
	movl	$1, (%r15,%rax,4)
	movb	$1, (%r14,%rax)
	movb	$1, (%r14)
	jmp	.LBB0_54
.LBB0_50:                               # %.lr.ph.prol
	movzbl	(%r14), %ecx
	movl	16(%rsp,%rcx,4), %edx
	leal	1(%rdx), %esi
	movl	%esi, 16(%rsp,%rcx,4)
	movl	%edx, (%r15)
	movl	$1, %edx
	cmpl	$1, %r9d
	je	.LBB0_54
.LBB0_52:                               # %.lr.ph.preheader.new
	subq	%rdx, %rax
	leaq	1(%r14,%rdx), %rcx
	leaq	4(%r15,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB0_53:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rcx), %esi
	movl	16(%rsp,%rsi,4), %edi
	leal	1(%rdi), %ebp
	movl	%ebp, 16(%rsp,%rsi,4)
	movl	%edi, -4(%rdx)
	movzbl	(%rcx), %esi
	movl	16(%rsp,%rsi,4), %edi
	leal	1(%rdi), %ebp
	movl	%ebp, 16(%rsp,%rsi,4)
	movl	%edi, (%rdx)
	addq	$2, %rcx
	addq	$8, %rdx
	addq	$-2, %rax
	jne	.LBB0_53
.LBB0_54:
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Huffman_Generate, .Lfunc_end0-Huffman_Generate
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
