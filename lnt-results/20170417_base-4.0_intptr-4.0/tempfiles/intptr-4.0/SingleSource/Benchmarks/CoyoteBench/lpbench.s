	.text
	.file	"lpbench.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4467570830353633249     # double 4.65661287525E-10
	.text
	.globl	matgen
	.p2align	4, 0x90
	.type	matgen,@function
matgen:                                 # @matgen
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	seed(%rip), %rcx
	xorl	%esi, %esi
	movabsq	$4730756183288445817, %r8 # imm = 0x41A705AF1FE3FB79
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_1:                                # %.preheader26
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_2:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorq	$123459876, %rcx        # imm = 0x75BD924
	movq	%rcx, %rax
	imulq	%r8
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$-127773, %rdx, %rax    # imm = 0xFFFE0CE3
	addq	%rcx, %rax
	imulq	$16807, %rax, %rax      # imm = 0x41A7
	imulq	$-2836, %rdx, %rdx      # imm = 0xF4EC
	leaq	2147483647(%rax,%rdx), %rcx
	addq	%rdx, %rax
	cmovnsq	%rax, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	mulsd	%xmm0, %xmm1
	xorq	$123459876, %rcx        # imm = 0x75BD924
	movq	(%r14,%rdi,8), %rax
	movsd	%xmm1, (%rax,%rsi,8)
	incq	%rdi
	cmpq	$2000, %rdi             # imm = 0x7D0
	jne	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	incq	%rsi
	cmpq	$2000, %rsi             # imm = 0x7D0
	jne	.LBB0_1
# BB#4:                                 # %.preheader25
	movq	%rcx, seed(%rip)
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$16000, %edx            # imm = 0x3E80
	movq	%rbx, %rdi
	callq	memset
	leaq	16000(%rbx), %rax
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
                                        #     Child Loop BB0_10 Depth 2
	movq	(%r14,%r15,8), %rcx
	leaq	16000(%rcx), %rdx
	cmpq	%rbx, %rdx
	jbe	.LBB0_7
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	cmpq	%rax, %rcx
	jae	.LBB0_7
# BB#9:                                 # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	$4, %edx
	.p2align	4, 0x90
.LBB0_10:                               # %scalar.ph
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-32(%rcx,%rdx,8), %xmm0 # xmm0 = mem[0],zero
	addsd	-32(%rbx,%rdx,8), %xmm0
	movsd	%xmm0, -32(%rbx,%rdx,8)
	movsd	-24(%rcx,%rdx,8), %xmm0 # xmm0 = mem[0],zero
	addsd	-24(%rbx,%rdx,8), %xmm0
	movsd	%xmm0, -24(%rbx,%rdx,8)
	movsd	-16(%rcx,%rdx,8), %xmm0 # xmm0 = mem[0],zero
	addsd	-16(%rbx,%rdx,8), %xmm0
	movsd	%xmm0, -16(%rbx,%rdx,8)
	movsd	-8(%rcx,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	addsd	-8(%rbx,%rdx,8), %xmm0
	movsd	%xmm0, -8(%rbx,%rdx,8)
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rbx,%rdx,8), %xmm0
	movsd	%xmm0, (%rbx,%rdx,8)
	addq	$5, %rdx
	cmpq	$2004, %rdx             # imm = 0x7D4
	jne	.LBB0_10
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_7:                                # %vector.body.preheader
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	$6, %edx
	.p2align	4, 0x90
.LBB0_8:                                # %vector.body
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rcx,%rdx,8), %xmm0
	movupd	-32(%rcx,%rdx,8), %xmm1
	movupd	-48(%rbx,%rdx,8), %xmm2
	movupd	-32(%rbx,%rdx,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, -48(%rbx,%rdx,8)
	movupd	%xmm3, -32(%rbx,%rdx,8)
	movupd	-16(%rcx,%rdx,8), %xmm0
	movupd	(%rcx,%rdx,8), %xmm1
	movupd	-16(%rbx,%rdx,8), %xmm2
	movupd	(%rbx,%rdx,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rbx,%rdx,8)
	movupd	%xmm3, (%rbx,%rdx,8)
	addq	$8, %rdx
	cmpq	$2006, %rdx             # imm = 0x7D6
	jne	.LBB0_8
.LBB0_11:                               # %middle.block
                                        #   in Loop: Header=BB0_5 Depth=1
	incq	%r15
	cmpq	$2000, %r15             # imm = 0x7D0
	jne	.LBB0_5
# BB#12:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	matgen, .Lfunc_end0-matgen
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	idamax
	.p2align	4, 0x90
	.type	idamax,@function
idamax:                                 # @idamax
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	testl	%edi, %edi
	jle	.LBB1_1
# BB#2:
	xorl	%eax, %eax
	cmpl	$1, %edi
	je	.LBB1_16
# BB#3:
	movslq	%edx, %r8
	movsd	(%rsi,%r8,8), %xmm0     # xmm0 = mem[0],zero
	andpd	.LCPI1_0(%rip), %xmm0
	cmpl	$1, %ecx
	jne	.LBB1_10
# BB#4:                                 # %.lr.ph.preheader
	testb	$1, %dil
	jne	.LBB1_5
# BB#6:                                 # %.lr.ph.prol
	movsd	8(%rsi,%r8,8), %xmm1    # xmm1 = mem[0],zero
	andpd	.LCPI1_0(%rip), %xmm1
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm1
	seta	%al
	maxsd	%xmm0, %xmm1
	movl	$2, %ecx
	movapd	%xmm1, %xmm0
	cmpl	$2, %edi
	jne	.LBB1_8
	jmp	.LBB1_16
.LBB1_1:
	movl	$-1, %eax
	retq
.LBB1_10:                               # %.lr.ph54.preheader
	leal	1(%rcx), %eax
	movslq	%eax, %r9
	movslq	%ecx, %r10
	testb	$1, %dil
	jne	.LBB1_11
# BB#12:                                # %.lr.ph54.prol
	leaq	(%r9,%r8), %rax
	movsd	(%rsi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	andpd	.LCPI1_0(%rip), %xmm1
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm1
	seta	%al
	maxsd	%xmm0, %xmm1
	addq	%r10, %r9
	movl	$2, %edx
	movapd	%xmm1, %xmm0
	cmpl	$2, %edi
	jne	.LBB1_14
	jmp	.LBB1_16
.LBB1_5:
	xorl	%eax, %eax
	movl	$1, %ecx
	cmpl	$2, %edi
	je	.LBB1_16
.LBB1_8:                                # %.lr.ph.preheader.new
	movl	%edi, %edx
	leaq	8(%rsi,%r8,8), %rsi
	movapd	.LCPI1_0(%rip), %xmm1   # xmm1 = [nan,nan]
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%rsi,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	andpd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	maxsd	%xmm0, %xmm2
	cmoval	%ecx, %eax
	movsd	(%rsi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	andpd	%xmm1, %xmm0
	leal	1(%rcx), %edi
	ucomisd	%xmm2, %xmm0
	maxsd	%xmm2, %xmm0
	cmoval	%edi, %eax
	addq	$2, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB1_9
	jmp	.LBB1_16
.LBB1_11:
	movl	$1, %edx
	xorl	%eax, %eax
	cmpl	$2, %edi
	je	.LBB1_16
.LBB1_14:                               # %.lr.ph54.preheader.new
	leaq	(%r9,%r10), %r11
	shlq	$4, %r10
	leaq	(%r9,%r8), %r9
	addq	%r8, %r11
	movapd	.LCPI1_0(%rip), %xmm1   # xmm1 = [nan,nan]
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph54
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rsi,%r9,8), %xmm2     # xmm2 = mem[0],zero
	andpd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	maxsd	%xmm0, %xmm2
	cmoval	%edx, %eax
	movsd	(%rsi,%r11,8), %xmm0    # xmm0 = mem[0],zero
	andpd	%xmm1, %xmm0
	leal	1(%rdx), %ecx
	ucomisd	%xmm2, %xmm0
	maxsd	%xmm2, %xmm0
	cmoval	%ecx, %eax
	addq	%r10, %rsi
	addl	$2, %edx
	cmpl	%edi, %edx
	jne	.LBB1_15
.LBB1_16:                               # %.loopexit
	retq
.Lfunc_end1:
	.size	idamax, .Lfunc_end1-idamax
	.cfi_endproc

	.globl	dscal
	.p2align	4, 0x90
	.type	dscal,@function
dscal:                                  # @dscal
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB2_13
# BB#1:
	cmpl	$1, %ecx
	jne	.LBB2_10
# BB#2:                                 # %.lr.ph.preheader
	movslq	%edx, %r8
	movl	%edi, %eax
	cmpl	$3, %edi
	jbe	.LBB2_3
# BB#6:                                 # %min.iters.checked
	andl	$3, %edi
	movq	%rax, %r9
	subq	%rdi, %r9
	je	.LBB2_3
# BB#7:                                 # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	16(%rsi,%r8,8), %rcx
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB2_8:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rcx), %xmm2
	movupd	(%rcx), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rcx)
	movupd	%xmm3, (%rcx)
	addq	$32, %rcx
	addq	$-4, %rdx
	jne	.LBB2_8
# BB#9:                                 # %middle.block
	testl	%edi, %edi
	jne	.LBB2_4
	jmp	.LBB2_13
.LBB2_10:
	imull	%ecx, %edi
	testl	%edi, %edi
	jle	.LBB2_13
# BB#11:                                # %.lr.ph24.preheader
	movslq	%ecx, %rax
	movslq	%edi, %rcx
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,8), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_12:                               # %.lr.ph24
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdx,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdx,%rsi,8)
	addq	%rax, %rsi
	cmpq	%rcx, %rsi
	jl	.LBB2_12
	jmp	.LBB2_13
.LBB2_3:
	xorl	%r9d, %r9d
.LBB2_4:                                # %.lr.ph.preheader34
	addq	%r9, %r8
	leaq	(%rsi,%r8,8), %rcx
	subq	%r9, %rax
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rcx)
	addq	$8, %rcx
	decq	%rax
	jne	.LBB2_5
.LBB2_13:                               # %.loopexit
	retq
.Lfunc_end2:
	.size	dscal, .Lfunc_end2-dscal
	.cfi_endproc

	.globl	daxpy
	.p2align	4, 0x90
	.type	daxpy,@function
daxpy:                                  # @daxpy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	jle	.LBB3_36
# BB#1:
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB3_2
	jnp	.LBB3_36
.LBB3_2:
	movl	56(%rsp), %ebp
	cmpl	$1, %ecx
	jne	.LBB3_14
# BB#3:
	cmpl	$1, %ebp
	jne	.LBB3_14
# BB#4:                                 # %.lr.ph50.preheader
	movslq	%r9d, %r9
	movslq	%edx, %r10
	movl	%edi, %eax
	cmpl	$3, %edi
	jbe	.LBB3_8
# BB#5:                                 # %min.iters.checked
	andl	$3, %edi
	movq	%rax, %rdx
	subq	%rdi, %rdx
	je	.LBB3_8
# BB#6:                                 # %vector.memcheck
	leaq	(%r8,%r9,8), %rcx
	leaq	(%r10,%rax), %rbp
	leaq	(%rsi,%rbp,8), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB3_23
# BB#7:                                 # %vector.memcheck
	leaq	(%r9,%rax), %rcx
	leaq	(%r8,%rcx,8), %rcx
	leaq	(%rsi,%r10,8), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB3_23
.LBB3_8:
	xorl	%edx, %edx
.LBB3_9:                                # %.lr.ph50.preheader132
	movl	%eax, %edi
	subl	%edx, %edi
	leaq	-1(%rax), %rcx
	testb	$1, %dil
	movq	%rdx, %rdi
	je	.LBB3_11
# BB#10:                                # %.lr.ph50.prol
	leaq	(%rdx,%r10), %rdi
	movsd	(%rsi,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	leaq	(%rdx,%r9), %rdi
	addsd	(%r8,%rdi,8), %xmm1
	movsd	%xmm1, (%r8,%rdi,8)
	leaq	1(%rdx), %rdi
.LBB3_11:                               # %.lr.ph50.prol.loopexit
	cmpq	%rdx, %rcx
	je	.LBB3_36
# BB#12:                                # %.lr.ph50.preheader132.new
	subq	%rdi, %rax
	addq	%rdi, %r10
	leaq	8(%rsi,%r10,8), %rdx
	addq	%rdi, %r9
	leaq	8(%r8,%r9,8), %rcx
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph50
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%rdx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	-8(%rcx), %xmm1
	movsd	%xmm1, -8(%rcx)
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rcx), %xmm1
	movsd	%xmm1, (%rcx)
	addq	$16, %rdx
	addq	$16, %rcx
	addq	$-2, %rax
	jne	.LBB3_13
	jmp	.LBB3_36
.LBB3_14:                               # %.lr.ph.preheader
	movl	$1, %eax
	subl	%edi, %eax
	movl	%eax, %ebx
	imull	%ebp, %ebx
	imull	%ecx, %eax
	xorl	%r14d, %r14d
	testl	%ebp, %ebp
	movslq	%ebx, %r11
	cmovnsq	%r14, %r11
	testl	%ecx, %ecx
	movslq	%ebp, %r10
	movslq	%r9d, %rbx
	movslq	%eax, %r13
	cmovnsq	%r14, %r13
	movslq	%ecx, %rax
	movslq	%edx, %rdx
	leal	-1(%rdi), %r15d
	leaq	1(%r15), %r12
	cmpq	$4, %r12
	jae	.LBB3_16
# BB#15:
	xorl	%r14d, %r14d
	jmp	.LBB3_31
.LBB3_16:                               # %min.iters.checked76
	movabsq	$8589934588, %r14       # imm = 0x1FFFFFFFC
	andq	%r12, %r14
	je	.LBB3_22
# BB#17:                                # %vector.scevcheck
	cmpl	$1, %ebp
	jne	.LBB3_27
# BB#18:                                # %vector.scevcheck
	cmpl	$1, %ecx
	jne	.LBB3_27
# BB#19:                                # %vector.memcheck95
	movq	%rbx, -8(%rsp)          # 8-byte Spill
	leaq	(%r11,%rbx), %rcx
	leaq	(%r8,%rcx,8), %rbp
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	leaq	(%r13,%rdx), %rdx
	leaq	(%rdx,%r15), %rbx
	leaq	8(%rsi,%rbx,8), %rbx
	cmpq	%rbx, %rbp
	jae	.LBB3_28
# BB#20:                                # %vector.memcheck95
	leaq	(%rcx,%r15), %rbp
	leaq	8(%r8,%rbp,8), %rbp
	leaq	(%rsi,%rdx,8), %rbx
	cmpq	%rbp, %rbx
	jae	.LBB3_28
# BB#21:
	xorl	%r14d, %r14d
	movq	-8(%rsp), %rbx          # 8-byte Reload
	movq	-16(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB3_31
.LBB3_22:
	xorl	%r14d, %r14d
	jmp	.LBB3_31
.LBB3_23:                               # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	16(%rsi,%r10,8), %rbp
	leaq	16(%r8,%r9,8), %rcx
	movq	%rdx, %rbx
	.p2align	4, 0x90
.LBB3_24:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rbp), %xmm2
	movupd	(%rbp), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	-16(%rcx), %xmm4
	movupd	(%rcx), %xmm5
	addpd	%xmm2, %xmm4
	addpd	%xmm3, %xmm5
	movupd	%xmm4, -16(%rcx)
	movupd	%xmm5, (%rcx)
	addq	$32, %rbp
	addq	$32, %rcx
	addq	$-4, %rbx
	jne	.LBB3_24
# BB#25:                                # %middle.block
	testl	%edi, %edi
	jne	.LBB3_9
	jmp	.LBB3_36
.LBB3_27:
	xorl	%r14d, %r14d
	jmp	.LBB3_31
.LBB3_28:                               # %vector.ph96
	movq	%r14, %rbp
	imulq	%rax, %rbp
	addq	%rbp, %r13
	movq	%r14, %rbp
	imulq	%r10, %rbp
	addq	%rbp, %r11
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	16(%r8,%rcx,8), %rcx
	movq	%r10, %rbp
	shlq	$5, %rbp
	leaq	16(%rsi,%rdx,8), %rbx
	movq	%rax, %r9
	shlq	$5, %r9
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB3_29:                               # %vector.body72
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rbx), %xmm2
	movupd	(%rbx), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	-16(%rcx), %xmm4
	movupd	(%rcx), %xmm5
	addpd	%xmm2, %xmm4
	addpd	%xmm3, %xmm5
	movupd	%xmm4, -16(%rcx)
	movupd	%xmm5, (%rcx)
	addq	%rbp, %rcx
	addq	%r9, %rbx
	addq	$-4, %rdx
	jne	.LBB3_29
# BB#30:                                # %middle.block73
	cmpq	%r14, %r12
	movq	-8(%rsp), %rbx          # 8-byte Reload
	movq	-16(%rsp), %rdx         # 8-byte Reload
	je	.LBB3_36
.LBB3_31:                               # %.lr.ph.preheader131
	movl	%edi, %ecx
	subl	%r14d, %ecx
	testb	$1, %cl
	jne	.LBB3_33
# BB#32:
	movl	%r14d, %r9d
	cmpl	%r14d, %r15d
	jne	.LBB3_34
	jmp	.LBB3_36
.LBB3_33:                               # %.lr.ph.prol
	leaq	(%r13,%rdx), %rcx
	movsd	(%rsi,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	leaq	(%r11,%rbx), %rcx
	addsd	(%r8,%rcx,8), %xmm1
	movsd	%xmm1, (%r8,%rcx,8)
	addq	%rax, %r13
	addq	%r10, %r11
	leal	1(%r14), %r9d
	cmpl	%r14d, %r15d
	je	.LBB3_36
.LBB3_34:                               # %.lr.ph.preheader131.new
	leaq	(%r13,%rax), %rcx
	shlq	$4, %rax
	movq	%rdx, %rbp
	leaq	(%r13,%rbp), %rdx
	addq	%rbp, %rcx
	movq	%rbx, %r14
	leaq	(%r11,%r10), %rbx
	shlq	$4, %r10
	leaq	(%r11,%r14), %rbp
	addq	%r14, %rbx
	subl	%r9d, %edi
	.p2align	4, 0x90
.LBB3_35:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rsi,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%r8,%rbp,8), %xmm1
	movsd	%xmm1, (%r8,%rbp,8)
	movsd	(%rsi,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%r8,%rbx,8), %xmm1
	movsd	%xmm1, (%r8,%rbx,8)
	addq	%rax, %rsi
	addq	%r10, %r8
	addl	$-2, %edi
	jne	.LBB3_35
.LBB3_36:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	daxpy, .Lfunc_end3-daxpy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_1:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	dgefa
	.p2align	4, 0x90
	.type	dgefa,@function
dgefa:                                  # @dgefa
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	%rsi, -32(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movl	$1999, %eax             # imm = 0x7CF
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	$5, %eax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movl	$7, %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	movl	$2, %eax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movapd	.LCPI4_0(%rip), %xmm9   # xmm9 = [nan,nan]
	xorps	%xmm1, %xmm1
	movsd	.LCPI4_1(%rip), %xmm8   # xmm8 = mem[0],zero
	movapd	.LCPI4_0(%rip), %xmm3   # xmm3 = [nan,nan]
	.p2align	4, 0x90
.LBB4_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #     Child Loop BB4_18 Depth 2
                                        #     Child Loop BB4_21 Depth 2
                                        #     Child Loop BB4_45 Depth 2
                                        #       Child Loop BB4_57 Depth 3
                                        #       Child Loop BB4_63 Depth 3
                                        #     Child Loop BB4_24 Depth 2
                                        #       Child Loop BB4_36 Depth 3
                                        #       Child Loop BB4_42 Depth 3
	movq	%rcx, %rax
	movl	$1999, %r13d            # imm = 0x7CF
	subq	%rax, %r13
	movq	(%rdi,%rax,8), %rbx
	movsd	(%rbx,%rax,8), %xmm4    # xmm4 = mem[0],zero
	andpd	%xmm9, %xmm4
	testb	$1, %r13b
	jne	.LBB4_3
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	$1, %ecx
	xorl	%r8d, %r8d
	cmpq	$1998, %rax             # imm = 0x7CE
	jne	.LBB4_4
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph.i.prol
                                        #   in Loop: Header=BB4_1 Depth=1
	movsd	8(%rbx,%rax,8), %xmm5   # xmm5 = mem[0],zero
	andpd	%xmm9, %xmm5
	xorl	%r8d, %r8d
	ucomisd	%xmm4, %xmm5
	seta	%r8b
	maxsd	%xmm4, %xmm5
	movl	$2, %ecx
	movapd	%xmm5, %xmm4
	cmpq	$1998, %rax             # imm = 0x7CE
	je	.LBB4_6
.LBB4_4:                                # %.new
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	$2000, %edx             # imm = 0x7D0
	subq	%rax, %rdx
	leaq	(%rbx,%rax,8), %rsi
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.i
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi,%rcx,8), %xmm5    # xmm5 = mem[0],zero
	andpd	%xmm3, %xmm5
	ucomisd	%xmm4, %xmm5
	maxsd	%xmm4, %xmm5
	cmoval	%ecx, %r8d
	movsd	8(%rsi,%rcx,8), %xmm4   # xmm4 = mem[0],zero
	andpd	%xmm3, %xmm4
	leal	1(%rcx), %ebp
	ucomisd	%xmm5, %xmm4
	maxsd	%xmm5, %xmm4
	cmoval	%ebp, %r8d
	addq	$2, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB4_5
.LBB4_6:                                # %idamax.exit
                                        #   in Loop: Header=BB4_1 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	leal	(%rax,%r8), %ecx
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	%ecx, (%rdx,%rax,4)
	movslq	%ecx, %r14
	movsd	(%rbx,%r14,8), %xmm5    # xmm5 = mem[0],zero
	ucomisd	%xmm1, %xmm5
	jne	.LBB4_7
	jnp	.LBB4_65
.LBB4_7:                                #   in Loop: Header=BB4_1 Depth=1
	movl	$1995, %ebp             # imm = 0x7CB
	subq	%rax, %rbp
	testl	%r8d, %r8d
	je	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	(%rbx,%rax,8), %rcx
	movq	%rcx, (%rbx,%r14,8)
	movsd	%xmm5, (%rbx,%rax,8)
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_9:                                # %._crit_edge
                                        #   in Loop: Header=BB4_1 Depth=1
	movsd	(%rbx,%rax,8), %xmm5    # xmm5 = mem[0],zero
.LBB4_10:                               #   in Loop: Header=BB4_1 Depth=1
	movq	-88(%rsp), %r9          # 8-byte Reload
	shrq	$2, %rbp
	movq	-16(%rsp), %r15         # 8-byte Reload
	andq	$-4, %r15
	movapd	%xmm8, %xmm4
	divsd	%xmm5, %xmm4
	cmpq	$4, %r13
	jae	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_1 Depth=1
	xorl	%esi, %esi
	jmp	.LBB4_20
	.p2align	4, 0x90
.LBB4_12:                               # %min.iters.checked152
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	%r13, %rsi
	andq	$-4, %rsi
	je	.LBB4_15
# BB#13:                                # %vector.ph156
                                        #   in Loop: Header=BB4_1 Depth=1
	movapd	%xmm4, %xmm5
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	testb	$1, %bpl
	jne	.LBB4_16
# BB#14:                                # %vector.body148.prol
                                        #   in Loop: Header=BB4_1 Depth=1
	movupd	8(%rbx,%rax,8), %xmm6
	movupd	24(%rbx,%rax,8), %xmm7
	mulpd	%xmm5, %xmm6
	mulpd	%xmm5, %xmm7
	movupd	%xmm6, 8(%rbx,%rax,8)
	movupd	%xmm7, 24(%rbx,%rax,8)
	movl	$4, %edx
	testq	%rbp, %rbp
	jne	.LBB4_17
	jmp	.LBB4_19
.LBB4_15:                               #   in Loop: Header=BB4_1 Depth=1
	xorl	%esi, %esi
	jmp	.LBB4_20
.LBB4_16:                               #   in Loop: Header=BB4_1 Depth=1
	xorl	%edx, %edx
	testq	%rbp, %rbp
	je	.LBB4_19
.LBB4_17:                               # %vector.ph156.new
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	%r15, %rcx
	subq	%rdx, %rcx
	addq	-24(%rsp), %rdx         # 8-byte Folded Reload
	leaq	16(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB4_18:                               # %vector.body148
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rdx), %xmm6
	movupd	-32(%rdx), %xmm7
	mulpd	%xmm5, %xmm6
	mulpd	%xmm5, %xmm7
	movupd	%xmm6, -48(%rdx)
	movupd	%xmm7, -32(%rdx)
	movupd	-16(%rdx), %xmm6
	movupd	(%rdx), %xmm7
	mulpd	%xmm5, %xmm6
	mulpd	%xmm5, %xmm7
	movupd	%xmm6, -16(%rdx)
	movupd	%xmm7, (%rdx)
	addq	$64, %rdx
	addq	$-8, %rcx
	jne	.LBB4_18
.LBB4_19:                               # %middle.block149
                                        #   in Loop: Header=BB4_1 Depth=1
	cmpq	%rsi, %r13
	je	.LBB4_22
	.p2align	4, 0x90
.LBB4_20:                               # %.lr.ph.i71.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	-72(%rsp), %rcx         # 8-byte Reload
	leaq	(%rbx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB4_21:                               # %.lr.ph.i71
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rcx,%rsi,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	movsd	%xmm5, (%rcx,%rsi,8)
	leaq	1(%rax,%rsi), %rdx
	incq	%rsi
	cmpq	$1999, %rdx             # imm = 0x7CF
	jne	.LBB4_21
.LBB4_22:                               # %.lr.ph
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	$1998, %r12d            # imm = 0x7CE
	subq	%rax, %r12
	testl	%r8d, %r8d
	movq	%rbp, -8(%rsp)          # 8-byte Spill
	leaq	(%rbx,%r9,8), %rcx
	je	.LBB4_44
# BB#23:                                # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	leaq	16000(%rbx), %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movq	%r13, %r9
	andq	$-4, %r9
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	andl	$1, %ebp
	movq	%rbp, -64(%rsp)         # 8-byte Spill
	movq	-80(%rsp), %rcx         # 8-byte Reload
	leaq	(%rbx,%rcx,8), %r8
	movq	-72(%rsp), %r10         # 8-byte Reload
	jmp	.LBB4_24
.LBB4_32:                               # %vector.ph
                                        #   in Loop: Header=BB4_24 Depth=2
	movapd	%xmm4, %xmm5
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	cmpq	$0, -64(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_34
# BB#33:                                # %vector.body.prol
                                        #   in Loop: Header=BB4_24 Depth=2
	movupd	8(%rbx,%rax,8), %xmm6
	movupd	24(%rbx,%rax,8), %xmm7
	mulpd	%xmm5, %xmm6
	mulpd	%xmm5, %xmm7
	movupd	8(%r11,%rax,8), %xmm2
	movupd	24(%r11,%rax,8), %xmm0
	addpd	%xmm6, %xmm2
	addpd	%xmm7, %xmm0
	movupd	%xmm2, 8(%r11,%rax,8)
	movupd	%xmm0, 24(%r11,%rax,8)
	movl	$4, %ebp
	cmpq	$0, -8(%rsp)            # 8-byte Folded Reload
	jne	.LBB4_35
	jmp	.LBB4_37
.LBB4_34:                               #   in Loop: Header=BB4_24 Depth=2
	xorl	%ebp, %ebp
	cmpq	$0, -8(%rsp)            # 8-byte Folded Reload
	je	.LBB4_37
.LBB4_35:                               # %vector.ph.new
                                        #   in Loop: Header=BB4_24 Depth=2
	movq	-80(%rsp), %rcx         # 8-byte Reload
	leaq	(%r11,%rcx,8), %rsi
	.p2align	4, 0x90
.LBB4_36:                               # %vector.body
                                        #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-48(%r8,%rbp,8), %xmm0
	movupd	-32(%r8,%rbp,8), %xmm2
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm2
	movupd	-48(%rsi,%rbp,8), %xmm6
	movupd	-32(%rsi,%rbp,8), %xmm7
	addpd	%xmm0, %xmm6
	addpd	%xmm2, %xmm7
	movupd	%xmm6, -48(%rsi,%rbp,8)
	movupd	%xmm7, -32(%rsi,%rbp,8)
	movupd	-16(%r8,%rbp,8), %xmm0
	movupd	(%r8,%rbp,8), %xmm2
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm2
	movupd	-16(%rsi,%rbp,8), %xmm6
	movupd	(%rsi,%rbp,8), %xmm7
	addpd	%xmm0, %xmm6
	addpd	%xmm2, %xmm7
	movupd	%xmm6, -16(%rsi,%rbp,8)
	movupd	%xmm7, (%rsi,%rbp,8)
	addq	$8, %rbp
	cmpq	%rbp, %r15
	jne	.LBB4_36
.LBB4_37:                               # %middle.block
                                        #   in Loop: Header=BB4_24 Depth=2
	cmpq	%r9, %r13
	movq	%r9, %rcx
	je	.LBB4_43
	jmp	.LBB4_38
	.p2align	4, 0x90
.LBB4_24:                               # %.lr.ph.split.us
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_36 Depth 3
                                        #       Child Loop BB4_42 Depth 3
	movq	(%rdi,%r10,8), %r11
	movsd	(%r11,%r14,8), %xmm4    # xmm4 = mem[0],zero
	movq	(%r11,%rax,8), %rcx
	movq	%rcx, (%r11,%r14,8)
	movsd	%xmm4, (%r11,%rax,8)
	ucomisd	%xmm1, %xmm4
	jne	.LBB4_25
	jnp	.LBB4_43
.LBB4_25:                               # %.lr.ph50.i.us.preheader
                                        #   in Loop: Header=BB4_24 Depth=2
	cmpq	$4, %r13
	jae	.LBB4_27
# BB#26:                                #   in Loop: Header=BB4_24 Depth=2
	xorl	%ecx, %ecx
	jmp	.LBB4_38
	.p2align	4, 0x90
.LBB4_27:                               # %min.iters.checked
                                        #   in Loop: Header=BB4_24 Depth=2
	testq	%r9, %r9
	je	.LBB4_31
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_24 Depth=2
	movq	-88(%rsp), %rcx         # 8-byte Reload
	leaq	(%r11,%rcx,8), %rcx
	cmpq	-48(%rsp), %rcx         # 8-byte Folded Reload
	jae	.LBB4_32
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_24 Depth=2
	leaq	16000(%r11), %rcx
	cmpq	%rcx, -56(%rsp)         # 8-byte Folded Reload
	jae	.LBB4_32
.LBB4_31:                               #   in Loop: Header=BB4_24 Depth=2
	xorl	%ecx, %ecx
.LBB4_38:                               # %.lr.ph50.i.us.preheader169
                                        #   in Loop: Header=BB4_24 Depth=2
	movl	%r13d, %edx
	subl	%ecx, %edx
	testb	$1, %dl
	movq	%rcx, %rsi
	je	.LBB4_40
# BB#39:                                # %.lr.ph50.i.us.prol
                                        #   in Loop: Header=BB4_24 Depth=2
	movq	-88(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx), %rdx
	movsd	(%rbx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	(%r11,%rdx,8), %xmm0
	movsd	%xmm0, (%r11,%rdx,8)
	movq	%rcx, %rsi
	orq	$1, %rsi
.LBB4_40:                               # %.lr.ph50.i.us.prol.loopexit
                                        #   in Loop: Header=BB4_24 Depth=2
	cmpq	%rcx, %r12
	je	.LBB4_43
# BB#41:                                # %.lr.ph50.i.us.preheader169.new
                                        #   in Loop: Header=BB4_24 Depth=2
	addq	-40(%rsp), %rsi         # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB4_42:                               # %.lr.ph50.i.us
                                        #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbx,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	-8(%r11,%rsi,8), %xmm0
	movsd	%xmm0, -8(%r11,%rsi,8)
	movsd	(%rbx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	(%r11,%rsi,8), %xmm0
	movsd	%xmm0, (%r11,%rsi,8)
	addq	$2, %rsi
	cmpq	$2001, %rsi             # imm = 0x7D1
	jne	.LBB4_42
.LBB4_43:                               # %daxpy.exit.us
                                        #   in Loop: Header=BB4_24 Depth=2
	incq	%r10
	cmpq	$2000, %r10             # imm = 0x7D0
	jne	.LBB4_24
	jmp	.LBB4_65
	.p2align	4, 0x90
.LBB4_44:                               # %.lr.ph.split.split.us.preheader
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	leaq	16000(%rbx), %r9
	movq	%r13, %r10
	andq	$-4, %r10
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	andl	$1, %ebp
	movq	%rbp, -64(%rsp)         # 8-byte Spill
	movq	-80(%rsp), %rcx         # 8-byte Reload
	leaq	(%rbx,%rcx,8), %rsi
	movq	-72(%rsp), %r8          # 8-byte Reload
	movq	%r9, -56(%rsp)          # 8-byte Spill
	jmp	.LBB4_45
.LBB4_53:                               # %vector.ph133
                                        #   in Loop: Header=BB4_45 Depth=2
	movaps	%xmm4, %xmm5
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	cmpq	$0, -64(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_55
# BB#54:                                # %vector.body111.prol
                                        #   in Loop: Header=BB4_45 Depth=2
	movupd	8(%rbx,%rax,8), %xmm0
	movupd	24(%rbx,%rax,8), %xmm2
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm2
	movupd	8(%rbp,%rax,8), %xmm6
	movupd	24(%rbp,%rax,8), %xmm7
	addpd	%xmm0, %xmm6
	addpd	%xmm2, %xmm7
	movupd	%xmm6, 8(%rbp,%rax,8)
	movupd	%xmm7, 24(%rbp,%rax,8)
	movl	$4, %r11d
	cmpq	$0, -8(%rsp)            # 8-byte Folded Reload
	jne	.LBB4_56
	jmp	.LBB4_58
.LBB4_55:                               #   in Loop: Header=BB4_45 Depth=2
	xorl	%r11d, %r11d
	cmpq	$0, -8(%rsp)            # 8-byte Folded Reload
	je	.LBB4_58
.LBB4_56:                               # %vector.ph133.new
                                        #   in Loop: Header=BB4_45 Depth=2
	movq	-80(%rsp), %rcx         # 8-byte Reload
	leaq	(%rbp,%rcx,8), %r9
	.p2align	4, 0x90
.LBB4_57:                               # %vector.body111
                                        #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_45 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-48(%rsi,%r11,8), %xmm0
	movupd	-32(%rsi,%r11,8), %xmm2
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm2
	movupd	-48(%r9,%r11,8), %xmm6
	movupd	-32(%r9,%r11,8), %xmm7
	addpd	%xmm0, %xmm6
	addpd	%xmm2, %xmm7
	movupd	%xmm6, -48(%r9,%r11,8)
	movupd	%xmm7, -32(%r9,%r11,8)
	movupd	-16(%rsi,%r11,8), %xmm0
	movupd	(%rsi,%r11,8), %xmm2
	mulpd	%xmm5, %xmm0
	mulpd	%xmm5, %xmm2
	movupd	-16(%r9,%r11,8), %xmm6
	movupd	(%r9,%r11,8), %xmm7
	addpd	%xmm0, %xmm6
	addpd	%xmm2, %xmm7
	movupd	%xmm6, -16(%r9,%r11,8)
	movupd	%xmm7, (%r9,%r11,8)
	addq	$8, %r11
	cmpq	%r11, %r15
	jne	.LBB4_57
.LBB4_58:                               # %middle.block112
                                        #   in Loop: Header=BB4_45 Depth=2
	cmpq	%r10, %r13
	movq	%r10, %rdx
	movq	-56(%rsp), %r9          # 8-byte Reload
	je	.LBB4_64
	jmp	.LBB4_59
	.p2align	4, 0x90
.LBB4_45:                               # %.lr.ph.split.split.us
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_57 Depth 3
                                        #       Child Loop BB4_63 Depth 3
	movq	(%rdi,%r8,8), %rbp
	movsd	(%rbp,%r14,8), %xmm4    # xmm4 = mem[0],zero
	ucomisd	%xmm1, %xmm4
	jne	.LBB4_46
	jnp	.LBB4_64
.LBB4_46:                               # %.lr.ph50.i.us76.preheader
                                        #   in Loop: Header=BB4_45 Depth=2
	cmpq	$4, %r13
	jae	.LBB4_48
# BB#47:                                #   in Loop: Header=BB4_45 Depth=2
	xorl	%edx, %edx
	jmp	.LBB4_59
	.p2align	4, 0x90
.LBB4_48:                               # %min.iters.checked115
                                        #   in Loop: Header=BB4_45 Depth=2
	testq	%r10, %r10
	je	.LBB4_52
# BB#49:                                # %vector.memcheck132
                                        #   in Loop: Header=BB4_45 Depth=2
	movq	-88(%rsp), %rcx         # 8-byte Reload
	leaq	(%rbp,%rcx,8), %rcx
	cmpq	%r9, %rcx
	jae	.LBB4_53
# BB#50:                                # %vector.memcheck132
                                        #   in Loop: Header=BB4_45 Depth=2
	leaq	16000(%rbp), %rcx
	cmpq	%rcx, -48(%rsp)         # 8-byte Folded Reload
	jae	.LBB4_53
.LBB4_52:                               #   in Loop: Header=BB4_45 Depth=2
	xorl	%edx, %edx
.LBB4_59:                               # %.lr.ph50.i.us76.preheader170
                                        #   in Loop: Header=BB4_45 Depth=2
	movl	%r13d, %ecx
	subl	%edx, %ecx
	testb	$1, %cl
	movq	%rdx, %rcx
	je	.LBB4_61
# BB#60:                                # %.lr.ph50.i.us76.prol
                                        #   in Loop: Header=BB4_45 Depth=2
	movq	-88(%rsp), %rcx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rcx
	movsd	(%rbx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	(%rbp,%rcx,8), %xmm0
	movsd	%xmm0, (%rbp,%rcx,8)
	movq	%rdx, %rcx
	orq	$1, %rcx
.LBB4_61:                               # %.lr.ph50.i.us76.prol.loopexit
                                        #   in Loop: Header=BB4_45 Depth=2
	cmpq	%rdx, %r12
	je	.LBB4_64
# BB#62:                                # %.lr.ph50.i.us76.preheader170.new
                                        #   in Loop: Header=BB4_45 Depth=2
	addq	-40(%rsp), %rcx         # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB4_63:                               # %.lr.ph50.i.us76
                                        #   Parent Loop BB4_1 Depth=1
                                        #     Parent Loop BB4_45 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rbx,%rcx,8), %xmm0  # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	-8(%rbp,%rcx,8), %xmm0
	movsd	%xmm0, -8(%rbp,%rcx,8)
	movsd	(%rbx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	(%rbp,%rcx,8), %xmm0
	movsd	%xmm0, (%rbp,%rcx,8)
	addq	$2, %rcx
	cmpq	$2001, %rcx             # imm = 0x7D1
	jne	.LBB4_63
.LBB4_64:                               # %daxpy.exit.us80
                                        #   in Loop: Header=BB4_45 Depth=2
	incq	%r8
	cmpq	$2000, %r8              # imm = 0x7D0
	jne	.LBB4_45
.LBB4_65:                               # %.backedge
                                        #   in Loop: Header=BB4_1 Depth=1
	incq	-72(%rsp)               # 8-byte Folded Spill
	decq	-16(%rsp)               # 8-byte Folded Spill
	incq	-24(%rsp)               # 8-byte Folded Spill
	incq	-80(%rsp)               # 8-byte Folded Spill
	incq	-40(%rsp)               # 8-byte Folded Spill
	movq	-88(%rsp), %rcx         # 8-byte Reload
	cmpq	$1999, %rcx             # imm = 0x7CF
	jne	.LBB4_1
# BB#66:
	movq	-32(%rsp), %rax         # 8-byte Reload
	movl	$1999, 7996(%rax)       # imm = 0x7CF
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	dgefa, .Lfunc_end4-dgefa
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	-9223372036854775808    # double -0
	.text
	.globl	dgesl
	.p2align	4, 0x90
	.type	dgesl,@function
dgesl:                                  # @dgesl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	leaq	16000(%rdx), %r9
	leaq	16(%rdx), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	xorl	%r11d, %r11d
	movl	$1999, %r14d            # imm = 0x7CF
	movl	$5, %r10d
	movl	$2, %r12d
	xorps	%xmm0, %xmm0
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	jmp	.LBB5_1
.LBB5_14:                               # %vector.ph
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	$1995, %eax             # imm = 0x7CB
	subq	%rcx, %rax
	shrq	$2, %rax
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	testb	$1, %al
	jne	.LBB5_15
# BB#16:                                # %vector.body.prol
                                        #   in Loop: Header=BB5_1 Depth=1
	movupd	8(%rbx,%rcx,8), %xmm3
	movupd	24(%rbx,%rcx,8), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	8(%rdx,%rcx,8), %xmm5
	movupd	24(%rdx,%rcx,8), %xmm6
	addpd	%xmm3, %xmm5
	addpd	%xmm4, %xmm6
	movupd	%xmm5, 8(%rdx,%rcx,8)
	movupd	%xmm6, 24(%rdx,%rcx,8)
	movl	$4, %r8d
	testq	%rax, %rax
	jne	.LBB5_18
	jmp	.LBB5_20
.LBB5_15:                               #   in Loop: Header=BB5_1 Depth=1
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.LBB5_20
.LBB5_18:                               # %vector.ph.new
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%r14, %rax
	andq	$-4, %rax
	subq	%r8, %rax
	addq	%r10, %r8
	leaq	16(%rbx,%r8,8), %rbp
	movq	%r9, %rdi
	movq	-16(%rsp), %r9          # 8-byte Reload
	leaq	(%r9,%r8,8), %r8
	movq	%rdi, %r9
	.p2align	4, 0x90
.LBB5_19:                               # %vector.body
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rbp), %xmm3
	movupd	-32(%rbp), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	-48(%r8), %xmm5
	movupd	-32(%r8), %xmm6
	addpd	%xmm3, %xmm5
	addpd	%xmm4, %xmm6
	movupd	%xmm5, -48(%r8)
	movupd	%xmm6, -32(%r8)
	movupd	-16(%rbp), %xmm3
	movupd	(%rbp), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	-16(%r8), %xmm5
	movupd	(%r8), %xmm6
	addpd	%xmm3, %xmm5
	addpd	%xmm4, %xmm6
	movupd	%xmm5, -16(%r8)
	movupd	%xmm6, (%r8)
	addq	$64, %rbp
	addq	$64, %r8
	addq	$-8, %rax
	jne	.LBB5_19
.LBB5_20:                               # %middle.block
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpq	%r15, %r13
	movq	-8(%rsp), %rdi          # 8-byte Reload
	jne	.LBB5_6
	jmp	.LBB5_21
	.p2align	4, 0x90
.LBB5_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_19 Depth 2
                                        #     Child Loop BB5_10 Depth 2
	movq	%r11, %rcx
	movl	(%rsi,%rcx,4), %ebp
	movslq	%ebp, %rax
	movsd	(%rdx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	cmpq	%rcx, %rbp
	je	.LBB5_3
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	movq	(%rdx,%rcx,8), %rbp
	movq	%rbp, (%rdx,%rax,8)
	movsd	%xmm1, (%rdx,%rcx,8)
.LBB5_3:                                #   in Loop: Header=BB5_1 Depth=1
	leaq	1(%rcx), %r11
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_4
	jnp	.LBB5_21
.LBB5_4:                                # %.lr.ph50.i.preheader
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	$1999, %r13d            # imm = 0x7CF
	subq	%rcx, %r13
	movq	(%rdi,%rcx,8), %rbx
	cmpq	$3, %r13
	jbe	.LBB5_5
# BB#11:                                # %min.iters.checked
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%r13, %r15
	andq	$-4, %r15
	je	.LBB5_5
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_1 Depth=1
	leaq	8(%rdx,%rcx,8), %rax
	leaq	16000(%rbx), %rbp
	cmpq	%rbp, %rax
	jae	.LBB5_14
# BB#13:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_1 Depth=1
	leaq	(%rbx,%r11,8), %rax
	cmpq	%r9, %rax
	jae	.LBB5_14
	.p2align	4, 0x90
.LBB5_5:                                #   in Loop: Header=BB5_1 Depth=1
	xorl	%r15d, %r15d
.LBB5_6:                                # %.lr.ph50.i.preheader106
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	$1998, %ebp             # imm = 0x7CE
	subq	%rcx, %rbp
	subl	%r15d, %r13d
	testb	$1, %r13b
	movq	%r15, %rax
	je	.LBB5_8
# BB#7:                                 # %.lr.ph50.i.prol
                                        #   in Loop: Header=BB5_1 Depth=1
	leaq	(%r15,%r11), %rax
	movsd	(%rbx,%rax,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	(%rdx,%rax,8), %xmm2
	movsd	%xmm2, (%rdx,%rax,8)
	movq	%r15, %rax
	orq	$1, %rax
.LBB5_8:                                # %.lr.ph50.i.prol.loopexit
                                        #   in Loop: Header=BB5_1 Depth=1
	cmpq	%r15, %rbp
	je	.LBB5_21
# BB#9:                                 # %.lr.ph50.i.preheader106.new
                                        #   in Loop: Header=BB5_1 Depth=1
	addq	%r12, %rax
	.p2align	4, 0x90
.LBB5_10:                               # %.lr.ph50.i
                                        #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rbx,%rax,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	-8(%rdx,%rax,8), %xmm2
	movsd	%xmm2, -8(%rdx,%rax,8)
	movsd	(%rbx,%rax,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	(%rdx,%rax,8), %xmm2
	movsd	%xmm2, (%rdx,%rax,8)
	addq	$2, %rax
	cmpq	$2001, %rax             # imm = 0x7D1
	jne	.LBB5_10
.LBB5_21:                               # %daxpy.exit.backedge
                                        #   in Loop: Header=BB5_1 Depth=1
	decq	%r14
	incq	%r10
	incq	%r12
	cmpq	$1999, %r11             # imm = 0x7CF
	jne	.LBB5_1
# BB#22:                                # %daxpy.exit53.preheader.preheader
	leaq	48(%rdx), %r8
	xorl	%r9d, %r9d
	movl	$1999, %r10d            # imm = 0x7CF
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	jmp	.LBB5_23
.LBB5_35:                               # %vector.ph90
                                        #   in Loop: Header=BB5_23 Depth=1
	movl	$1995, %esi             # imm = 0x7CB
	subq	%r11, %rsi
	shrq	$2, %rsi
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	testb	$1, %sil
	jne	.LBB5_36
# BB#37:                                # %vector.body72.prol
                                        #   in Loop: Header=BB5_23 Depth=1
	movupd	(%rax), %xmm3
	movupd	16(%rax), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	(%rdx), %xmm5
	movupd	16(%rdx), %xmm6
	subpd	%xmm3, %xmm5
	subpd	%xmm4, %xmm6
	movupd	%xmm5, (%rdx)
	movupd	%xmm6, 16(%rdx)
	movl	$4, %ebx
	testq	%rsi, %rsi
	jne	.LBB5_39
	jmp	.LBB5_41
.LBB5_36:                               #   in Loop: Header=BB5_23 Depth=1
	xorl	%ebx, %ebx
	testq	%rsi, %rsi
	je	.LBB5_41
.LBB5_39:                               # %vector.ph90.new
                                        #   in Loop: Header=BB5_23 Depth=1
	movq	%r10, %rsi
	andq	$-4, %rsi
	subq	%rbx, %rsi
	leaq	48(%rax,%rbx,8), %rbp
	leaq	(%r8,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB5_40:                               # %vector.body72
                                        #   Parent Loop BB5_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rbp), %xmm3
	movupd	-32(%rbp), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	-48(%rbx), %xmm5
	movupd	-32(%rbx), %xmm6
	subpd	%xmm3, %xmm5
	subpd	%xmm4, %xmm6
	movupd	%xmm5, -48(%rbx)
	movupd	%xmm6, -32(%rbx)
	movupd	-16(%rbp), %xmm3
	movupd	(%rbp), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	-16(%rbx), %xmm5
	movupd	(%rbx), %xmm6
	subpd	%xmm3, %xmm5
	subpd	%xmm4, %xmm6
	movupd	%xmm5, -16(%rbx)
	movupd	%xmm6, (%rbx)
	addq	$64, %rbp
	addq	$64, %rbx
	addq	$-8, %rsi
	jne	.LBB5_40
.LBB5_41:                               # %middle.block73
                                        #   in Loop: Header=BB5_23 Depth=1
	cmpq	%rcx, %r14
	jne	.LBB5_27
	jmp	.LBB5_42
	.p2align	4, 0x90
.LBB5_23:                               # %daxpy.exit53.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_40 Depth 2
                                        #     Child Loop BB5_29 Depth 2
                                        #     Child Loop BB5_31 Depth 2
	movq	%r9, %r11
	movl	$1999, %r14d            # imm = 0x7CF
	subq	%r11, %r14
	leaq	1(%r11), %r9
	movq	(%rdi,%r14,8), %rax
	movsd	(%rdx,%r14,8), %xmm1    # xmm1 = mem[0],zero
	divsd	(%rax,%r14,8), %xmm1
	movsd	%xmm1, (%rdx,%r14,8)
	testq	%r14, %r14
	jle	.LBB5_42
# BB#24:                                # %daxpy.exit53.preheader
                                        #   in Loop: Header=BB5_23 Depth=1
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_25
	jnp	.LBB5_42
.LBB5_25:                               # %.lr.ph50.i52.preheader
                                        #   in Loop: Header=BB5_23 Depth=1
	cmpq	$3, %r14
	jbe	.LBB5_26
# BB#32:                                # %min.iters.checked76
                                        #   in Loop: Header=BB5_23 Depth=1
	movq	%r14, %rcx
	andq	$-4, %rcx
	je	.LBB5_26
# BB#33:                                # %vector.memcheck89
                                        #   in Loop: Header=BB5_23 Depth=1
	leaq	(%rax,%r14,8), %rsi
	cmpq	%rdx, %rsi
	jbe	.LBB5_35
# BB#34:                                # %vector.memcheck89
                                        #   in Loop: Header=BB5_23 Depth=1
	leaq	(%rdx,%r14,8), %rsi
	cmpq	%rsi, %rax
	jae	.LBB5_35
	.p2align	4, 0x90
.LBB5_26:                               #   in Loop: Header=BB5_23 Depth=1
	xorl	%ecx, %ecx
.LBB5_27:                               # %.lr.ph50.i52.preheader105
                                        #   in Loop: Header=BB5_23 Depth=1
	movl	$1998, %esi             # imm = 0x7CE
	subq	%r11, %rsi
	subl	%ecx, %r14d
	subq	%rcx, %rsi
	testb	$3, %r14b
	je	.LBB5_30
# BB#28:                                # %.lr.ph50.i52.prol.preheader
                                        #   in Loop: Header=BB5_23 Depth=1
	movl	%r10d, %ebx
	subl	%ecx, %ebx
	andl	$3, %ebx
	negq	%rbx
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph50.i52.prol
                                        #   Parent Loop BB5_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	(%rdx,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	subsd	%xmm2, %xmm3
	movsd	%xmm3, (%rdx,%rcx,8)
	incq	%rcx
	incq	%rbx
	jne	.LBB5_29
.LBB5_30:                               # %.lr.ph50.i52.prol.loopexit
                                        #   in Loop: Header=BB5_23 Depth=1
	cmpq	$3, %rsi
	jb	.LBB5_42
	.p2align	4, 0x90
.LBB5_31:                               # %.lr.ph50.i52
                                        #   Parent Loop BB5_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	(%rdx,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	subsd	%xmm2, %xmm3
	movsd	%xmm3, (%rdx,%rcx,8)
	movsd	8(%rax,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	8(%rdx,%rcx,8), %xmm3   # xmm3 = mem[0],zero
	subsd	%xmm2, %xmm3
	movsd	%xmm3, 8(%rdx,%rcx,8)
	movsd	16(%rax,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	16(%rdx,%rcx,8), %xmm3  # xmm3 = mem[0],zero
	subsd	%xmm2, %xmm3
	movsd	%xmm3, 16(%rdx,%rcx,8)
	movsd	24(%rax,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	24(%rdx,%rcx,8), %xmm3  # xmm3 = mem[0],zero
	subsd	%xmm2, %xmm3
	movsd	%xmm3, 24(%rdx,%rcx,8)
	leaq	4(%r11,%rcx), %rsi
	addq	$4, %rcx
	cmpq	$1999, %rsi             # imm = 0x7CF
	jne	.LBB5_31
.LBB5_42:                               # %daxpy.exit53.backedge
                                        #   in Loop: Header=BB5_23 Depth=1
	decq	%r10
	cmpq	$2000, %r9              # imm = 0x7D0
	jne	.LBB5_23
# BB#43:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	dgesl, .Lfunc_end5-dgesl
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4467570830353633249     # double 4.65661287525E-10
.LCPI6_1:
	.quad	-9223372036854775808    # double -0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 80
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jl	.LBB6_8
# BB#1:                                 # %.lr.ph
	movq	8(%rsi), %rax
	movb	(%rax), %cl
	movl	$1, %edx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	cmpb	$45, %cl
	jne	.LBB6_6
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpb	$103, 1(%rax)
	jne	.LBB6_6
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpb	$97, 2(%rax)
	jne	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpb	$0, 3(%rax)
	je	.LBB6_55
	.p2align	4, 0x90
.LBB6_6:                                # %.thread
                                        #   in Loop: Header=BB6_2 Depth=1
	incl	%edx
	cmpl	%edi, %edx
	jl	.LBB6_2
.LBB6_8:
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
.LBB6_9:                                # %.loopexit
	movl	$16000, %edi            # imm = 0x3E80
	callq	malloc
	movq	%rax, %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_10:                               # =>This Inner Loop Header: Depth=1
	movl	$16008, %edi            # imm = 0x3E88
	callq	malloc
	movq	%rax, (%r14,%rbx,8)
	incq	%rbx
	cmpq	$2000, %rbx             # imm = 0x7D0
	jne	.LBB6_10
# BB#11:
	movl	$16000, %edi            # imm = 0x3E80
	callq	malloc
	movq	%rax, %rbx
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %r15
	xorl	%esi, %esi
	movq	seed(%rip), %rcx
	movabsq	$4730756183288445817, %rdi # imm = 0x41A705AF1FE3FB79
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_12:                               # %.preheader26.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_13 Depth 2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_13:                               #   Parent Loop BB6_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorq	$123459876, %rcx        # imm = 0x75BD924
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$-127773, %rdx, %rax    # imm = 0xFFFE0CE3
	addq	%rcx, %rax
	imulq	$16807, %rax, %rax      # imm = 0x41A7
	imulq	$-2836, %rdx, %rdx      # imm = 0xF4EC
	leaq	2147483647(%rax,%rdx), %rcx
	addq	%rdx, %rax
	cmovnsq	%rax, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	mulsd	%xmm0, %xmm1
	xorq	$123459876, %rcx        # imm = 0x75BD924
	movq	(%r14,%rbp,8), %rax
	movsd	%xmm1, (%rax,%rsi,8)
	incq	%rbp
	cmpq	$2000, %rbp             # imm = 0x7D0
	jne	.LBB6_13
# BB#14:                                #   in Loop: Header=BB6_12 Depth=1
	incq	%rsi
	cmpq	$2000, %rsi             # imm = 0x7D0
	jne	.LBB6_12
# BB#15:                                # %.preheader25.i
	movq	%rcx, seed(%rip)
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$16000, %edx            # imm = 0x3E80
	movq	%rbx, %rdi
	callq	memset
	.p2align	4, 0x90
.LBB6_16:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_17 Depth 2
	movq	(%r14,%rbp,8), %rax
	movl	$6, %ecx
	.p2align	4, 0x90
.LBB6_17:                               # %vector.body
                                        #   Parent Loop BB6_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rax,%rcx,8), %xmm0
	movupd	-32(%rax,%rcx,8), %xmm1
	movupd	-48(%rbx,%rcx,8), %xmm2
	movupd	-32(%rbx,%rcx,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, -48(%rbx,%rcx,8)
	movupd	%xmm3, -32(%rbx,%rcx,8)
	movupd	-16(%rax,%rcx,8), %xmm0
	movupd	(%rax,%rcx,8), %xmm1
	movupd	-16(%rbx,%rcx,8), %xmm2
	movupd	(%rbx,%rcx,8), %xmm3
	addpd	%xmm0, %xmm2
	addpd	%xmm1, %xmm3
	movupd	%xmm2, -16(%rbx,%rcx,8)
	movupd	%xmm3, (%rbx,%rcx,8)
	addq	$8, %rcx
	cmpq	$2006, %rcx             # imm = 0x7D6
	jne	.LBB6_17
# BB#18:                                # %middle.block
                                        #   in Loop: Header=BB6_16 Depth=1
	incq	%rbp
	cmpq	$2000, %rbp             # imm = 0x7D0
	jne	.LBB6_16
# BB#19:                                # %matgen.exit
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	dgefa
	leaq	16(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r10d, %r10d
	movl	$1999, %r8d             # imm = 0x7CF
	movl	$5, %r13d
	movl	$1, %esi
	xorpd	%xmm0, %xmm0
	jmp	.LBB6_24
.LBB6_20:                               #   in Loop: Header=BB6_24 Depth=1
	xorl	%r12d, %r12d
	testq	%rcx, %rcx
	je	.LBB6_23
.LBB6_21:                               # %vector.ph88.new
                                        #   in Loop: Header=BB6_24 Depth=1
	movq	%r8, %rcx
	andq	$-4, %rcx
	subq	%r12, %rcx
	addq	%r13, %r12
	leaq	16(%r9,%r12,8), %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%r12,8), %r12
	.p2align	4, 0x90
.LBB6_22:                               # %vector.body84
                                        #   Parent Loop BB6_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rdx), %xmm3
	movupd	-32(%rdx), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	-48(%r12), %xmm5
	movupd	-32(%r12), %xmm6
	addpd	%xmm3, %xmm5
	addpd	%xmm4, %xmm6
	movupd	%xmm5, -48(%r12)
	movupd	%xmm6, -32(%r12)
	movupd	-16(%rdx), %xmm3
	movupd	(%rdx), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	-16(%r12), %xmm5
	movupd	(%r12), %xmm6
	addpd	%xmm3, %xmm5
	addpd	%xmm4, %xmm6
	movupd	%xmm5, -16(%r12)
	movupd	%xmm6, (%r12)
	addq	$64, %rdx
	addq	$64, %r12
	addq	$-8, %rcx
	jne	.LBB6_22
.LBB6_23:                               # %middle.block85
                                        #   in Loop: Header=BB6_24 Depth=1
	cmpq	%rax, %r11
	jne	.LBB6_32
	jmp	.LBB6_34
	.p2align	4, 0x90
.LBB6_24:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_22 Depth 2
                                        #     Child Loop BB6_33 Depth 2
	movq	%r10, %rdi
	movl	(%r15,%rdi,4), %ecx
	movslq	%ecx, %rax
	movsd	(%rbx,%rax,8), %xmm1    # xmm1 = mem[0],zero
	cmpq	%rdi, %rcx
	je	.LBB6_26
# BB#25:                                #   in Loop: Header=BB6_24 Depth=1
	movq	(%rbx,%rdi,8), %rcx
	movq	%rcx, (%rbx,%rax,8)
	movsd	%xmm1, (%rbx,%rdi,8)
.LBB6_26:                               #   in Loop: Header=BB6_24 Depth=1
	leaq	1(%rdi), %r10
	ucomisd	%xmm0, %xmm1
	jne	.LBB6_27
	jnp	.LBB6_34
.LBB6_27:                               # %.lr.ph50.i.i.preheader
                                        #   in Loop: Header=BB6_24 Depth=1
	movl	$1999, %r11d            # imm = 0x7CF
	subq	%rdi, %r11
	movq	(%r14,%rdi,8), %r9
	cmpq	$3, %r11
	jbe	.LBB6_31
# BB#28:                                # %min.iters.checked87
                                        #   in Loop: Header=BB6_24 Depth=1
	movq	%r11, %rax
	andq	$-4, %rax
	je	.LBB6_31
# BB#29:                                # %vector.ph88
                                        #   in Loop: Header=BB6_24 Depth=1
	movl	$1995, %ecx             # imm = 0x7CB
	subq	%rdi, %rcx
	shrq	$2, %rcx
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	testb	$1, %cl
	jne	.LBB6_20
# BB#30:                                # %vector.body84.prol
                                        #   in Loop: Header=BB6_24 Depth=1
	movupd	8(%r9,%rdi,8), %xmm3
	movupd	24(%r9,%rdi,8), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	8(%rbx,%rdi,8), %xmm5
	movupd	24(%rbx,%rdi,8), %xmm6
	addpd	%xmm3, %xmm5
	addpd	%xmm4, %xmm6
	movupd	%xmm5, 8(%rbx,%rdi,8)
	movupd	%xmm6, 24(%rbx,%rdi,8)
	movl	$4, %r12d
	testq	%rcx, %rcx
	jne	.LBB6_21
	jmp	.LBB6_23
	.p2align	4, 0x90
.LBB6_31:                               #   in Loop: Header=BB6_24 Depth=1
	xorl	%eax, %eax
.LBB6_32:                               # %.lr.ph50.i.i.preheader128
                                        #   in Loop: Header=BB6_24 Depth=1
	leaq	(%rax,%rsi), %rcx
	leaq	(%rbx,%rcx,8), %rcx
	leaq	(%r9,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB6_33:                               # %.lr.ph50.i.i
                                        #   Parent Loop BB6_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdx,%rax,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	(%rcx), %xmm2
	movsd	%xmm2, (%rcx)
	addq	$8, %rcx
	leaq	1(%rdi,%rax), %rbp
	incq	%rax
	cmpq	$1999, %rbp             # imm = 0x7CF
	jne	.LBB6_33
.LBB6_34:                               # %daxpy.exit.backedge.i
                                        #   in Loop: Header=BB6_24 Depth=1
	decq	%r8
	incq	%r13
	incq	%rsi
	cmpq	$1999, %r10             # imm = 0x7CF
	jne	.LBB6_24
# BB#35:                                # %daxpy.exit53.preheader.i.preheader
	movq	%rbx, %r8
	addq	$48, %r8
	xorl	%r9d, %r9d
	movl	$1999, %r10d            # imm = 0x7CF
	movsd	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero
	jmp	.LBB6_40
.LBB6_36:                               #   in Loop: Header=BB6_40 Depth=1
	xorl	%r12d, %r12d
	testq	%rax, %rax
	je	.LBB6_39
.LBB6_37:                               # %vector.ph111.new
                                        #   in Loop: Header=BB6_40 Depth=1
	movq	%r10, %rcx
	andq	$-4, %rcx
	subq	%r12, %rcx
	leaq	48(%rdi,%r12,8), %rdx
	leaq	(%r8,%r12,8), %rax
	.p2align	4, 0x90
.LBB6_38:                               # %vector.body103
                                        #   Parent Loop BB6_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rdx), %xmm3
	movupd	-32(%rdx), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	-48(%rax), %xmm5
	movupd	-32(%rax), %xmm6
	subpd	%xmm3, %xmm5
	subpd	%xmm4, %xmm6
	movupd	%xmm5, -48(%rax)
	movupd	%xmm6, -32(%rax)
	movupd	-16(%rdx), %xmm3
	movupd	(%rdx), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	-16(%rax), %xmm5
	movupd	(%rax), %xmm6
	subpd	%xmm3, %xmm5
	subpd	%xmm4, %xmm6
	movupd	%xmm5, -16(%rax)
	movupd	%xmm6, (%rax)
	addq	$64, %rdx
	addq	$64, %rax
	addq	$-8, %rcx
	jne	.LBB6_38
.LBB6_39:                               # %middle.block104
                                        #   in Loop: Header=BB6_40 Depth=1
	cmpq	%rbp, %r11
	jne	.LBB6_47
	jmp	.LBB6_48
	.p2align	4, 0x90
.LBB6_40:                               # %daxpy.exit53.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_38 Depth 2
                                        #     Child Loop BB6_47 Depth 2
	movq	%r9, %rsi
	movl	$1999, %r11d            # imm = 0x7CF
	subq	%rsi, %r11
	leaq	1(%rsi), %r9
	movq	(%r14,%r11,8), %rdi
	movsd	(%rbx,%r11,8), %xmm1    # xmm1 = mem[0],zero
	divsd	(%rdi,%r11,8), %xmm1
	movsd	%xmm1, (%rbx,%r11,8)
	testq	%r11, %r11
	jle	.LBB6_48
# BB#41:                                # %daxpy.exit53.preheader.i
                                        #   in Loop: Header=BB6_40 Depth=1
	ucomisd	%xmm0, %xmm1
	jne	.LBB6_42
	jnp	.LBB6_48
.LBB6_42:                               # %.lr.ph50.i52.i.preheader
                                        #   in Loop: Header=BB6_40 Depth=1
	cmpq	$3, %r11
	jbe	.LBB6_46
# BB#43:                                # %min.iters.checked107
                                        #   in Loop: Header=BB6_40 Depth=1
	movq	%r11, %rbp
	andq	$-4, %rbp
	je	.LBB6_46
# BB#44:                                # %vector.ph111
                                        #   in Loop: Header=BB6_40 Depth=1
	movl	$1995, %eax             # imm = 0x7CB
	subq	%rsi, %rax
	shrq	$2, %rax
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	testb	$1, %al
	jne	.LBB6_36
# BB#45:                                # %vector.body103.prol
                                        #   in Loop: Header=BB6_40 Depth=1
	movupd	(%rdi), %xmm3
	movupd	16(%rdi), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	(%rbx), %xmm5
	movupd	16(%rbx), %xmm6
	subpd	%xmm3, %xmm5
	subpd	%xmm4, %xmm6
	movupd	%xmm5, (%rbx)
	movupd	%xmm6, 16(%rbx)
	movl	$4, %r12d
	testq	%rax, %rax
	jne	.LBB6_37
	jmp	.LBB6_39
	.p2align	4, 0x90
.LBB6_46:                               #   in Loop: Header=BB6_40 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_47:                               # %.lr.ph50.i52.i
                                        #   Parent Loop BB6_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	(%rbx,%rbp,8), %xmm3    # xmm3 = mem[0],zero
	subsd	%xmm2, %xmm3
	movsd	%xmm3, (%rbx,%rbp,8)
	leaq	1(%rsi,%rbp), %rax
	incq	%rbp
	cmpq	$1999, %rax             # imm = 0x7CF
	jne	.LBB6_47
.LBB6_48:                               # %daxpy.exit53.backedge.i
                                        #   in Loop: Header=BB6_40 Depth=1
	decq	%r10
	cmpq	$2000, %r9              # imm = 0x7D0
	jne	.LBB6_40
# BB#49:                                # %dgesl.exit
	movq	%r15, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_50:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rdi
	callq	free
	incq	%rbx
	cmpq	$2000, %rbx             # imm = 0x7D0
	jne	.LBB6_50
# BB#51:
	movq	%r14, %rdi
	callq	free
	movq	stdout(%rip), %rdi
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB6_53
# BB#52:
	movl	$.L.str.1, %esi
	jmp	.LBB6_54
.LBB6_53:
	movl	$.L.str.2, %esi
.LBB6_54:
	xorps	%xmm0, %xmm0
	movb	$1, %al
	callq	fprintf
	movq	stdout(%rip), %rdi
	callq	fflush
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_55:
	movb	$1, %al
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB6_9
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"-ga"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%f"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\nlpbench (Std. C) run time: %f\n\n"
	.size	.L.str.2, 33

	.type	seed,@object            # @seed
	.data
	.p2align	3
seed:
	.quad	1325                    # 0x52d
	.size	seed, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
