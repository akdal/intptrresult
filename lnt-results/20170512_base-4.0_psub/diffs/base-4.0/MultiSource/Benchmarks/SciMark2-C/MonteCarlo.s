	.text
	.file	"MonteCarlo.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4616189618054758400     # double 4
	.text
	.globl	MonteCarlo_num_flops
	.p2align	4, 0x90
	.type	MonteCarlo_num_flops,@function
MonteCarlo_num_flops:                   # @MonteCarlo_num_flops
	.cfi_startproc
# BB#0:
	cvtsi2sdl	%edi, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	retq
.Lfunc_end0:
	.size	MonteCarlo_num_flops, .Lfunc_end0-MonteCarlo_num_flops
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
.LCPI1_1:
	.quad	4616189618054758400     # double 4
	.text
	.globl	MonteCarlo_integrate
	.p2align	4, 0x90
	.type	MonteCarlo_integrate,@function
MonteCarlo_integrate:                   # @MonteCarlo_integrate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movl	$113, %edi
	callq	new_Random_seed
	movq	%rax, %r15
	testl	%r14d, %r14d
	jle	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	movl	%r14d, %ebx
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	Random_nextDouble
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%r15, %rdi
	callq	Random_nextDouble
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorl	%eax, %eax
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	setae	%al
	addl	%eax, %ebp
	decl	%ebx
	jne	.LBB1_3
# BB#4:                                 # %._crit_edge.loopexit
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	jmp	.LBB1_5
.LBB1_1:
	xorpd	%xmm0, %xmm0
.LBB1_5:                                # %._crit_edge
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%r15, %rdi
	callq	Random_delete
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r14d, %xmm1
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	mulsd	.LCPI1_1(%rip), %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	MonteCarlo_integrate, .Lfunc_end1-MonteCarlo_integrate
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
