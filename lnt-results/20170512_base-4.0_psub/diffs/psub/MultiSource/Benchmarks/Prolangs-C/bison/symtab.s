	.text
	.file	"symtab.bc"
	.globl	hash
	.p2align	4, 0x90
	.type	hash,@function
hash:                                   # @hash
	.cfi_startproc
# BB#0:
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB0_1
# BB#2:                                 # %.lr.ph.preheader
	incq	%rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addl	%eax, %eax
	movsbl	%cl, %ecx
	xorl	%ecx, %eax
	andl	$16383, %eax            # imm = 0x3FFF
	movzbl	(%rdi), %ecx
	incq	%rdi
	testb	%cl, %cl
	jne	.LBB0_3
# BB#4:                                 # %._crit_edge.loopexit
	movl	%eax, %ecx
	imulq	$63849861, %rcx, %rcx   # imm = 0x3CE4585
	shrq	$32, %rcx
	movl	%eax, %edx
	subl	%ecx, %edx
	shrl	%edx
	addl	%ecx, %edx
	shrl	$9, %edx
	imull	$1009, %edx, %ecx       # imm = 0x3F1
	subl	%ecx, %eax
	retq
.LBB0_1:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	hash, .Lfunc_end0-hash
	.cfi_endproc

	.globl	copys
	.p2align	4, 0x90
	.type	copys,@function
copys:                                  # @copys
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpb	$0, (%rbx)
	je	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	movl	$1, %edi
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rbx,%rdi)
	leaq	1(%rdi), %rdi
	jne	.LBB1_3
	jmp	.LBB1_4
.LBB1_1:
	movl	$1, %edi
.LBB1_4:                                # %._crit_edge
	xorl	%eax, %eax
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	mallocate
	movq	%rax, %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	copys, .Lfunc_end1-copys
	.cfi_endproc

	.globl	tabinit
	.p2align	4, 0x90
	.type	tabinit,@function
tabinit:                                # @tabinit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movl	$8072, %edi             # imm = 0x1F88
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, symtab(%rip)
	movq	$0, firstsymbol(%rip)
	movq	$0, lastsymbol(%rip)
	popq	%rax
	retq
.Lfunc_end2:
	.size	tabinit, .Lfunc_end2-tabinit
	.cfi_endproc

	.globl	getsym
	.p2align	4, 0x90
	.type	getsym,@function
getsym:                                 # @getsym
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -40
.Lcfi12:
	.cfi_offset %r12, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movb	(%r14), %cl
	xorl	%ebx, %ebx
	testb	%cl, %cl
	je	.LBB3_4
# BB#1:                                 # %.lr.ph.i.preheader
	leaq	1(%r14), %rax
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	addl	%ebx, %ebx
	movsbl	%cl, %ecx
	xorl	%ecx, %ebx
	andl	$16383, %ebx            # imm = 0x3FFF
	movzbl	(%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	jne	.LBB3_2
# BB#3:                                 # %._crit_edge.loopexit.i
	movl	%ebx, %eax
	imulq	$63849861, %rax, %rax   # imm = 0x3CE4585
	shrq	$32, %rax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	shrl	%ecx
	addl	%eax, %ecx
	shrl	$9, %ecx
	imull	$1009, %ecx, %eax       # imm = 0x3F1
	subl	%eax, %ebx
.LBB3_4:                                # %hash.exit
	movq	symtab(%rip), %rax
	movq	(%rax,%rbx,8), %r12
	testq	%r12, %r12
	jne	.LBB3_6
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_7:                                # %.us-lcssa32.us
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.LBB3_8
.LBB3_6:                                # %.sink.split.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rsi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB3_7
	jmp	.LBB3_13
.LBB3_8:                                # %.sink.split._crit_edge
	incl	nsyms(%rip)
	movl	$48, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r12
	movq	symtab(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	cmpb	$0, (%r14)
	je	.LBB3_9
# BB#10:                                # %.lr.ph.i26.preheader
	movl	$1, %edi
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph.i26
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%r14,%rdi)
	leaq	1(%rdi), %rdi
	jne	.LBB3_11
	jmp	.LBB3_12
.LBB3_9:
	movl	$1, %edi
.LBB3_12:                               # %copys.exit
	xorl	%eax, %eax
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	mallocate
	movq	%rax, %r15
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	%r15, 16(%r12)
	movb	$0, 40(%r12)
	movl	$firstsymbol, %eax
	movq	lastsymbol(%rip), %rcx
	addq	$8, %rcx
	cmpq	$0, firstsymbol(%rip)
	cmoveq	%rax, %rcx
	movq	%r12, (%rcx)
	movq	%r12, lastsymbol(%rip)
	movq	symtab(%rip), %rax
	movq	%r12, (%rax,%rbx,8)
.LBB3_13:                               # %.us-lcssa.us
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	getsym, .Lfunc_end3-getsym
	.cfi_endproc

	.globl	free_symtab
	.p2align	4, 0x90
	.type	free_symtab,@function
free_symtab:                            # @free_symtab
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_2 Depth 2
	movq	symtab(%rip), %rax
	movq	(%rax,%r14,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_3
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        #   Parent Loop BB4_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_2
.LBB4_3:                                # %._crit_edge
                                        #   in Loop: Header=BB4_1 Depth=1
	incq	%r14
	cmpq	$1009, %r14             # imm = 0x3F1
	jne	.LBB4_1
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	free_symtab, .Lfunc_end4-free_symtab
	.cfi_endproc

	.type	symtab,@object          # @symtab
	.comm	symtab,8,8
	.type	firstsymbol,@object     # @firstsymbol
	.comm	firstsymbol,8,8
	.type	lastsymbol,@object      # @lastsymbol
	.comm	lastsymbol,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
