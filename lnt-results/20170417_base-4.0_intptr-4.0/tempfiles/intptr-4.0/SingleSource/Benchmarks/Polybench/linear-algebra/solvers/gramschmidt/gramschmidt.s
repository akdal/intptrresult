	.text
	.file	"gramschmidt.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4647714815446351872     # double 512
.LCPI6_1:
	.quad	4566650022153682944     # double 0.001953125
.LCPI6_3:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 128
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, 16(%rsp)
	leaq	16(%rsp), %rdi
	movl	$32, %esi
	movl	$2097152, %edx          # imm = 0x200000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_105
# BB#1:
	movq	16(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_105
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 16(%rsp)
	leaq	16(%rsp), %rdi
	movl	$32, %esi
	movl	$2097152, %edx          # imm = 0x200000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_105
# BB#3:                                 # %polybench_alloc_data.exit
	movq	16(%rsp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_105
# BB#4:                                 # %polybench_alloc_data.exit59
	movq	$0, 16(%rsp)
	leaq	16(%rsp), %rdi
	movl	$32, %esi
	movl	$2097152, %edx          # imm = 0x200000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_105
# BB#5:                                 # %polybench_alloc_data.exit59
	movq	16(%rsp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_105
# BB#6:                                 # %polybench_alloc_data.exit61
	movq	$0, 16(%rsp)
	leaq	16(%rsp), %rdi
	movl	$32, %esi
	movl	$2097152, %edx          # imm = 0x200000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_105
# BB#7:                                 # %polybench_alloc_data.exit61
	movq	16(%rsp), %r15
	testq	%r15, %r15
	je	.LBB6_105
# BB#8:                                 # %polybench_alloc_data.exit63
	movq	$0, 16(%rsp)
	leaq	16(%rsp), %rdi
	movl	$32, %esi
	movl	$2097152, %edx          # imm = 0x200000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_105
# BB#9:                                 # %polybench_alloc_data.exit63
	movq	16(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_105
# BB#10:                                # %polybench_alloc_data.exit65
	movq	$0, 16(%rsp)
	leaq	16(%rsp), %rdi
	movl	$32, %esi
	movl	$2097152, %edx          # imm = 0x200000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_105
# BB#11:                                # %polybench_alloc_data.exit65
	movq	16(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_105
# BB#12:                                # %polybench_alloc_data.exit67
	xorl	%eax, %eax
	movl	$1, %ecx
	movsd	.LCPI6_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movq	56(%rsp), %r11          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_13:                               # %.preheader2.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_14 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%rcx, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_14:                               #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, -8(%r12,%rdx,8)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, -8(%r10,%rdx,8)
	movsd	%xmm1, (%r12,%rdx,8)
	addq	$2, %rsi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, (%r10,%rdx,8)
	addq	$2, %rdx
	cmpq	$512, %rsi              # imm = 0x200
	jne	.LBB6_14
# BB#15:                                #   in Loop: Header=BB6_13 Depth=1
	incq	%rax
	addq	$512, %rcx              # imm = 0x200
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB6_13
# BB#16:                                # %.preheader.i.preheader
	leaq	8(%r11), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	$-512, %rdx             # imm = 0xFE00
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_18:                               #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	514(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, -8(%rsi)
	leal	515(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, (%rsi)
	addq	$16, %rsi
	addq	$2, %rdx
	jne	.LBB6_18
# BB#19:                                #   in Loop: Header=BB6_17 Depth=1
	incq	%rcx
	addq	$4096, %rax             # imm = 0x1000
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB6_17
# BB#20:                                # %.preheader1.i.preheader
	leaq	12288(%r12), %rcx
	leaq	4096(%r10), %r13
	leaq	4104(%r12), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	movl	$8192, %ebx             # imm = 0x2000
	.p2align	4, 0x90
.LBB6_22:                               # %.preheader1.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_23 Depth 2
                                        #     Child Loop BB6_27 Depth 2
                                        #     Child Loop BB6_30 Depth 2
                                        #       Child Loop BB6_31 Depth 3
                                        #       Child Loop BB6_33 Depth 3
	movq	%r8, 8(%rsp)            # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movl	$512, %eax              # imm = 0x200
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_23:                               #   Parent Loop BB6_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-12288(%rcx), %xmm1     # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movsd	-8192(%rcx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movsd	-4096(%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	addq	$16384, %rcx            # imm = 0x4000
	addq	$-4, %rax
	jne	.LBB6_23
# BB#24:                                #   in Loop: Header=BB6_22 Depth=1
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_26
# BB#25:                                # %call.sqrt
                                        #   in Loop: Header=BB6_22 Depth=1
	callq	sqrt
	movsd	.LCPI6_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movsd	.LCPI6_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	movapd	%xmm0, %xmm1
.LBB6_26:                               # %.split
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, %rax
	shlq	$12, %rax
	addq	%r11, %rax
	leaq	(%rax,%rdx,8), %rcx
	movsd	%xmm1, (%rax,%rdx,8)
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	movsd	%xmm0, (%r10,%rdx,8)
	movl	$510, %edx              # imm = 0x1FE
	movq	%rbx, %rsi
	jmp	.LBB6_27
	.p2align	4, 0x90
.LBB6_103:                              # %._crit_edge.i.1
                                        #   in Loop: Header=BB6_27 Depth=2
	movsd	(%r12,%rsi), %xmm0      # xmm0 = mem[0],zero
	divsd	(%rcx), %xmm0
	movsd	%xmm0, (%r10,%rsi)
	addq	$8192, %rsi             # imm = 0x2000
	addq	$-2, %rdx
.LBB6_27:                               # %._crit_edge.i
                                        #   Parent Loop BB6_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-4096(%r12,%rsi), %xmm0 # xmm0 = mem[0],zero
	divsd	(%rcx), %xmm0
	movsd	%xmm0, -4096(%r10,%rsi)
	testq	%rdx, %rdx
	jne	.LBB6_103
# BB#28:                                #   in Loop: Header=BB6_22 Depth=1
	movq	%rbx, %r9
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	1(%rcx), %r8
	cmpq	$511, %r8               # imm = 0x1FF
	jg	.LBB6_21
# BB#29:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_30:                               # %.lr.ph.i
                                        #   Parent Loop BB6_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_31 Depth 3
                                        #       Child Loop BB6_33 Depth 3
	leaq	(%rax,%rsi,8), %rdi
	movq	$0, (%rax,%rsi,8)
	xorpd	%xmm0, %xmm0
	movl	$512, %ecx              # imm = 0x200
	movq	%rbp, %rdx
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB6_31:                               #   Parent Loop BB6_22 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-4096(%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	-4096(%rdx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdi)
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rdx), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdi)
	addq	$8192, %rbx             # imm = 0x2000
	addq	$8192, %rdx             # imm = 0x2000
	addq	$-2, %rcx
	jne	.LBB6_31
# BB#32:                                # %.preheader.i71.preheader
                                        #   in Loop: Header=BB6_30 Depth=2
	movsd	(%r12,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movq	8(%rsp), %rcx           # 8-byte Reload
	mulsd	(%r10,%rcx,8), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%r12,%rsi,8)
	xorl	%ecx, %ecx
	jmp	.LBB6_33
	.p2align	4, 0x90
.LBB6_104:                              # %.preheader..preheader_crit_edge.i.1
                                        #   in Loop: Header=BB6_33 Depth=3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	4096(%rbp,%rcx), %xmm1  # xmm1 = mem[0],zero
	mulsd	4096(%r13,%rcx), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 4096(%rbp,%rcx)
	addq	$8192, %rcx             # imm = 0x2000
.LBB6_33:                               # %.preheader..preheader_crit_edge.i
                                        #   Parent Loop BB6_22 Depth=1
                                        #     Parent Loop BB6_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rbp,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%r13,%rcx), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbp,%rcx)
	cmpq	$2088960, %rcx          # imm = 0x1FE000
	jne	.LBB6_104
# BB#34:                                #   in Loop: Header=BB6_30 Depth=2
	incq	%rsi
	addq	$8, %rbp
	cmpq	$512, %rsi              # imm = 0x200
	jne	.LBB6_30
.LBB6_21:                               # %.loopexit.i
                                        #   in Loop: Header=BB6_22 Depth=1
	incq	24(%rsp)                # 8-byte Folded Spill
	movq	64(%rsp), %rcx          # 8-byte Reload
	addq	$8, %rcx
	movq	%r9, %rbx
	addq	$8, %rbx
	addq	$8, %r13
	addq	$8, 32(%rsp)            # 8-byte Folded Spill
	cmpq	$512, %r8               # imm = 0x200
	movq	48(%rsp), %r9           # 8-byte Reload
	jne	.LBB6_22
# BB#35:                                # %kernel_gramschmidt.exit
	xorl	%eax, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_36:                               # %.preheader2.i73
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_37 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movq	%rcx, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_37:                               #   Parent Loop BB6_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, -8(%r15,%rdx,8)
	movl	%esi, %edi
	orl	$1, %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, -8(%r14,%rdx,8)
	movsd	%xmm1, (%r15,%rdx,8)
	addq	$2, %rsi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, (%r14,%rdx,8)
	addq	$2, %rdx
	cmpq	$512, %rsi              # imm = 0x200
	jne	.LBB6_37
# BB#38:                                #   in Loop: Header=BB6_36 Depth=1
	incq	%rax
	addq	$512, %rcx              # imm = 0x200
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB6_36
# BB#39:                                # %.preheader.i80.preheader
	leaq	8(%r9), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_40:                               # %.preheader.i80
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_41 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	$-512, %rdx             # imm = 0xFE00
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_41:                               #   Parent Loop BB6_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	514(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, -8(%rsi)
	leal	515(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm1
	movsd	%xmm1, (%rsi)
	addq	$16, %rsi
	addq	$2, %rdx
	jne	.LBB6_41
# BB#42:                                #   in Loop: Header=BB6_40 Depth=1
	incq	%rcx
	addq	$4096, %rax             # imm = 0x1000
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB6_40
# BB#43:                                # %.preheader1.i92.preheader
	leaq	12288(%r15), %rcx
	leaq	4096(%r14), %r13
	leaq	4104(%r15), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	movl	$8192, %ebx             # imm = 0x2000
	.p2align	4, 0x90
.LBB6_44:                               # %.preheader1.i92
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_45 Depth 2
                                        #     Child Loop BB6_49 Depth 2
                                        #     Child Loop BB6_58 Depth 2
                                        #       Child Loop BB6_59 Depth 3
                                        #       Child Loop BB6_61 Depth 3
	movq	%r8, 8(%rsp)            # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movl	$512, %eax              # imm = 0x200
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_45:                               #   Parent Loop BB6_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-12288(%rcx), %xmm1     # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movsd	-8192(%rcx), %xmm0      # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movsd	-4096(%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	addq	$16384, %rcx            # imm = 0x4000
	addq	$-4, %rax
	jne	.LBB6_45
# BB#46:                                #   in Loop: Header=BB6_44 Depth=1
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB6_48
# BB#47:                                # %call.sqrt498
                                        #   in Loop: Header=BB6_44 Depth=1
	callq	sqrt
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movapd	%xmm0, %xmm1
.LBB6_48:                               # %.split497
                                        #   in Loop: Header=BB6_44 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, %rax
	shlq	$12, %rax
	addq	%r9, %rax
	leaq	(%rax,%rdx,8), %rcx
	movsd	%xmm1, (%rax,%rdx,8)
	movsd	(%r15,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	movsd	%xmm0, (%r14,%rdx,8)
	movl	$510, %edx              # imm = 0x1FE
	movq	%rbx, %rsi
	jmp	.LBB6_49
	.p2align	4, 0x90
.LBB6_101:                              # %._crit_edge.i101.1
                                        #   in Loop: Header=BB6_49 Depth=2
	movsd	(%r15,%rsi), %xmm0      # xmm0 = mem[0],zero
	divsd	(%rcx), %xmm0
	movsd	%xmm0, (%r14,%rsi)
	addq	$8192, %rsi             # imm = 0x2000
	addq	$-2, %rdx
.LBB6_49:                               # %._crit_edge.i101
                                        #   Parent Loop BB6_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-4096(%r15,%rsi), %xmm0 # xmm0 = mem[0],zero
	divsd	(%rcx), %xmm0
	movsd	%xmm0, -4096(%r14,%rsi)
	testq	%rdx, %rdx
	jne	.LBB6_101
# BB#50:                                #   in Loop: Header=BB6_44 Depth=1
	movq	%rbx, %r11
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	1(%rcx), %r8
	cmpq	$511, %r8               # imm = 0x1FF
	jg	.LBB6_51
# BB#57:                                # %.lr.ph.i104.preheader
                                        #   in Loop: Header=BB6_44 Depth=1
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_58:                               # %.lr.ph.i104
                                        #   Parent Loop BB6_44 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_59 Depth 3
                                        #       Child Loop BB6_61 Depth 3
	leaq	(%rax,%rsi,8), %rdi
	movq	$0, (%rax,%rsi,8)
	xorpd	%xmm0, %xmm0
	movl	$512, %ecx              # imm = 0x200
	movq	%rbx, %rdx
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB6_59:                               #   Parent Loop BB6_44 Depth=1
                                        #     Parent Loop BB6_58 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-4096(%rbp), %xmm1      # xmm1 = mem[0],zero
	mulsd	-4096(%rdx), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rdi)
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rdx), %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, (%rdi)
	addq	$8192, %rbp             # imm = 0x2000
	addq	$8192, %rdx             # imm = 0x2000
	addq	$-2, %rcx
	jne	.LBB6_59
# BB#60:                                # %.preheader.i111.preheader
                                        #   in Loop: Header=BB6_58 Depth=2
	movsd	(%r15,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movq	8(%rsp), %rcx           # 8-byte Reload
	mulsd	(%r14,%rcx,8), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%r15,%rsi,8)
	xorl	%ecx, %ecx
	jmp	.LBB6_61
	.p2align	4, 0x90
.LBB6_102:                              # %.preheader..preheader_crit_edge.i113.1
                                        #   in Loop: Header=BB6_61 Depth=3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	4096(%rbx,%rcx), %xmm1  # xmm1 = mem[0],zero
	mulsd	4096(%r13,%rcx), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, 4096(%rbx,%rcx)
	addq	$8192, %rcx             # imm = 0x2000
.LBB6_61:                               # %.preheader..preheader_crit_edge.i113
                                        #   Parent Loop BB6_44 Depth=1
                                        #     Parent Loop BB6_58 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rbx,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	(%r13,%rcx), %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx,%rcx)
	cmpq	$2088960, %rcx          # imm = 0x1FE000
	jne	.LBB6_102
# BB#62:                                #   in Loop: Header=BB6_58 Depth=2
	incq	%rsi
	addq	$8, %rbx
	cmpq	$512, %rsi              # imm = 0x200
	jne	.LBB6_58
.LBB6_51:                               # %.loopexit.i89
                                        #   in Loop: Header=BB6_44 Depth=1
	incq	24(%rsp)                # 8-byte Folded Spill
	movq	64(%rsp), %rcx          # 8-byte Reload
	addq	$8, %rcx
	movq	%r11, %rbx
	addq	$8, %rbx
	addq	$8, %r13
	addq	$8, 32(%rsp)            # 8-byte Folded Spill
	cmpq	$512, %r8               # imm = 0x200
	movq	56(%rsp), %r11          # 8-byte Reload
	jne	.LBB6_44
# BB#52:                                # %.preheader.i116.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_2(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_53:                               # %.preheader.i116
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_54 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_54:                               #   Parent Loop BB6_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r12,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r15,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_55
# BB#63:                                #   in Loop: Header=BB6_54 Depth=2
	movsd	(%r12,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_56
# BB#64:                                #   in Loop: Header=BB6_54 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$512, %rcx              # imm = 0x200
	movq	%rdi, %rcx
	jl	.LBB6_54
# BB#65:                                #   in Loop: Header=BB6_53 Depth=1
	incq	%rdx
	addq	$512, %rax              # imm = 0x200
	cmpq	$512, %rdx              # imm = 0x200
	jl	.LBB6_53
# BB#66:                                # %.preheader.i121.preheader
	xorl	%edx, %edx
	movl	$1, %eax
.LBB6_67:                               # %.preheader.i121
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_68 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
.LBB6_68:                               #   Parent Loop BB6_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r11,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r9,%rsi,8), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_55
# BB#69:                                #   in Loop: Header=BB6_68 Depth=2
	movsd	(%r11,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r9,%rsi,8), %xmm1     # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_56
# BB#70:                                #   in Loop: Header=BB6_68 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$512, %rcx              # imm = 0x200
	movq	%rdi, %rcx
	jl	.LBB6_68
# BB#71:                                #   in Loop: Header=BB6_67 Depth=1
	incq	%rdx
	addq	$512, %rax              # imm = 0x200
	cmpq	$512, %rdx              # imm = 0x200
	jl	.LBB6_67
# BB#72:                                # %.preheader.i130.preheader
	xorl	%edx, %edx
	movl	$1, %eax
.LBB6_73:                               # %.preheader.i130
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_74 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
.LBB6_74:                               #   Parent Loop BB6_73 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r10,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r14,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_55
# BB#75:                                #   in Loop: Header=BB6_74 Depth=2
	movsd	(%r10,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r14,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_56
# BB#76:                                #   in Loop: Header=BB6_74 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$512, %rcx              # imm = 0x200
	movq	%rdi, %rcx
	jl	.LBB6_74
# BB#77:                                #   in Loop: Header=BB6_73 Depth=1
	incq	%rdx
	addq	$512, %rax              # imm = 0x200
	cmpq	$512, %rdx              # imm = 0x200
	jl	.LBB6_73
# BB#78:                                # %.preheader17.i.preheader
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r15, %rbp
.LBB6_79:                               # %.preheader17.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_81 Depth 2
                                        #     Child Loop BB6_83 Depth 2
	movslq	8(%rsp), %rax           # 4-byte Folded Reload
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB6_80
# BB#82:                                # %.preheader17.split.us.i.preheader
                                        #   in Loop: Header=BB6_79 Depth=1
	xorl	%ebx, %ebx
.LBB6_83:                               # %.preheader17.split.us.i
                                        #   Parent Loop BB6_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stderr(%rip), %rdi
	movsd	(%rbp,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incq	%rbx
	cmpq	$512, %rbx              # imm = 0x200
	jne	.LBB6_83
	jmp	.LBB6_84
.LBB6_80:                               # %.preheader17.split.i.preheader
                                        #   in Loop: Header=BB6_79 Depth=1
	movq	%rbp, %rbx
	movl	$512, %r13d             # imm = 0x200
.LBB6_81:                               # %.preheader17.split.i
                                        #   Parent Loop BB6_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stderr(%rip), %rdi
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	addq	$8, %rbx
	decq	%r13
	jne	.LBB6_81
.LBB6_84:                               # %.us-lcssa24.us.i
                                        #   in Loop: Header=BB6_79 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	addq	$4096, %rbp             # imm = 0x1000
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB6_79
# BB#85:
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB6_86:                               # %.preheader16.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_88 Depth 2
                                        #     Child Loop BB6_90 Depth 2
	movslq	8(%rsp), %rax           # 4-byte Folded Reload
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB6_87
# BB#89:                                # %.preheader16.split.us.i.preheader
                                        #   in Loop: Header=BB6_86 Depth=1
	xorl	%ebx, %ebx
.LBB6_90:                               # %.preheader16.split.us.i
                                        #   Parent Loop BB6_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stderr(%rip), %rdi
	movsd	(%rbp,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incq	%rbx
	cmpq	$512, %rbx              # imm = 0x200
	jne	.LBB6_90
	jmp	.LBB6_91
.LBB6_87:                               # %.preheader16.split.i.preheader
                                        #   in Loop: Header=BB6_86 Depth=1
	movq	%rbp, %rbx
	movl	$512, %r13d             # imm = 0x200
.LBB6_88:                               # %.preheader16.split.i
                                        #   Parent Loop BB6_86 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stderr(%rip), %rdi
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	addq	$8, %rbx
	decq	%r13
	jne	.LBB6_88
.LBB6_91:                               # %.us-lcssa21.us.i
                                        #   in Loop: Header=BB6_86 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	addq	$4096, %rbp             # imm = 0x1000
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB6_86
# BB#92:
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r14, %rbp
.LBB6_93:                               # %.preheader.i138
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_95 Depth 2
                                        #     Child Loop BB6_97 Depth 2
	movslq	8(%rsp), %rax           # 4-byte Folded Reload
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$35, %rcx
	addl	%edx, %ecx
	shll	$2, %ecx
	leal	(%rcx,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.LBB6_94
# BB#96:                                # %.preheader.split.us.i.preheader
                                        #   in Loop: Header=BB6_93 Depth=1
	xorl	%ebx, %ebx
.LBB6_97:                               # %.preheader.split.us.i
                                        #   Parent Loop BB6_93 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stderr(%rip), %rdi
	movsd	(%rbp,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	incq	%rbx
	cmpq	$512, %rbx              # imm = 0x200
	jne	.LBB6_97
	jmp	.LBB6_98
.LBB6_94:                               # %.preheader.split.i.preheader
                                        #   in Loop: Header=BB6_93 Depth=1
	movq	%rbp, %rbx
	movl	$512, %r13d             # imm = 0x200
.LBB6_95:                               # %.preheader.split.i
                                        #   Parent Loop BB6_93 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stderr(%rip), %rdi
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str.3, %esi
	movb	$1, %al
	callq	fprintf
	addq	$8, %rbx
	decq	%r13
	jne	.LBB6_95
.LBB6_98:                               # %.us-lcssa.us.i
                                        #   in Loop: Header=BB6_93 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	addq	$4096, %rbp             # imm = 0x1000
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB6_93
# BB#99:                                # %print_array.exit
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	%r12, %rdi
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_100
.LBB6_55:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_56:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_3(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_100:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_105:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%0.2lf "
	.size	.L.str.3, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
