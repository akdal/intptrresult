	.text
	.file	"bisect_test.bc"
	.globl	test_matrix
	.p2align	4, 0x90
	.type	test_matrix,@function
test_matrix:                            # @test_matrix
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB0_6
# BB#1:                                 # %.lr.ph.preheader
	movl	%edi, %eax
	testb	$1, %al
	jne	.LBB0_3
# BB#2:
	xorl	%ecx, %ecx
	cmpl	$1, %edi
	jne	.LBB0_5
	jmp	.LBB0_6
.LBB0_3:                                # %.lr.ph.prol
	movq	$0, (%rdx)
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, (%rsi)
	movl	$1, %ecx
	cmpl	$1, %edi
	je	.LBB0_6
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, (%rdx,%rcx,8)
	leal	1(%rcx), %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	%xmm1, %xmm1
	movsd	%xmm1, (%rsi,%rcx,8)
	movsd	%xmm0, 8(%rdx,%rcx,8)
	leaq	2(%rcx), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	mulsd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsi,%rcx,8)
	cmpq	%rax, %rdi
	movq	%rdi, %rcx
	jne	.LBB0_5
.LBB0_6:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	test_matrix, .Lfunc_end0-test_matrix
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	xorl	%r13d, %r13d
	leaq	12(%rsp), %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	scanf
	leaq	8(%rsp), %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	scanf
	leaq	64(%rsp), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	scanf
	movl	8(%rsp), %edi
	leaq	56(%rsp), %rsi
	callq	dallocvector
	movl	8(%rsp), %edi
	leaq	48(%rsp), %rsi
	callq	dallocvector
	movl	8(%rsp), %edi
	leaq	40(%rsp), %rsi
	callq	dallocvector
	movl	8(%rsp), %edi
	leaq	24(%rsp), %rsi
	callq	dallocvector
	cmpl	$0, 12(%rsp)
	jle	.LBB1_5
# BB#1:                                 # %.lr.ph21.preheader
	xorps	%xmm2, %xmm2
	movabsq	$4607182418800017408, %r14 # imm = 0x3FF0000000000000
	leaq	36(%rsp), %r15
	leaq	16(%rsp), %r12
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph21
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_11 Depth 2
                                        #     Child Loop BB1_28 Depth 2
                                        #     Child Loop BB1_30 Depth 2
	movslq	8(%rsp), %r9
	testq	%r9, %r9
	movl	%r9d, %ecx
	movq	56(%rsp), %rdi
	movq	48(%rsp), %rsi
	jle	.LBB1_13
# BB#3:                                 # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB1_2 Depth=1
	testb	$1, %cl
	jne	.LBB1_9
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	cmpl	$1, %r9d
	jne	.LBB1_11
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph.i.prol
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	$0, (%rsi)
	movq	%r14, (%rdi)
	movl	$1, %eax
	cmpl	$1, %r9d
	je	.LBB1_12
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph.i
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rsi,%rax,8)
	leal	1(%rax), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	mulsd	%xmm1, %xmm1
	movsd	%xmm1, (%rdi,%rax,8)
	movsd	%xmm0, 8(%rsi,%rax,8)
	leaq	2(%rax), %rdx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rdi,%rax,8)
	cmpq	%rcx, %rdx
	movq	%rdx, %rax
	jne	.LBB1_11
.LBB1_12:                               # %test_matrix.exit.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	testl	%ecx, %ecx
	jle	.LBB1_13
# BB#14:                                # %.lr.ph18
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	40(%rsp), %rdx
	movq	24(%rsp), %r10
	cmpl	$4, %r9d
	jae	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_30
	.p2align	4, 0x90
.LBB1_13:                               # %test_matrix.exit.preheader.test_matrix.exit._crit_edge_crit_edge
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	40(%rsp), %rdx
	jmp	.LBB1_31
.LBB1_16:                               # %min.iters.checked
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%r9, %r8
	andq	$-4, %r8
	je	.LBB1_17
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%r15, %r12
	leaq	(%rdx,%r9,8), %rax
	leaq	(%r10,%r9,8), %r11
	leaq	(%rsi,%r9,8), %rbp
	cmpq	%r11, %rdx
	sbbb	%bl, %bl
	cmpq	%rax, %r10
	sbbb	%r15b, %r15b
	andb	%bl, %r15b
	cmpq	%rbp, %rdx
	sbbb	%bl, %bl
	cmpq	%rax, %rsi
	sbbb	%r14b, %r14b
	cmpq	%rbp, %r10
	sbbb	%al, %al
	cmpq	%r11, %rsi
	sbbb	%r11b, %r11b
	testb	$1, %r15b
	jne	.LBB1_19
# BB#20:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_2 Depth=1
	andb	%r14b, %bl
	andb	$1, %bl
	movq	%r12, %r15
	jne	.LBB1_21
# BB#22:                                # %vector.memcheck
                                        #   in Loop: Header=BB1_2 Depth=1
	andb	%r11b, %al
	andb	$1, %al
	movl	$0, %eax
	movabsq	$4607182418800017408, %r14 # imm = 0x3FF0000000000000
	leaq	16(%rsp), %r12
	jne	.LBB1_30
# BB#23:                                # %vector.body.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	-4(%r8), %rax
	movq	%rax, %rbx
	shrq	$2, %rbx
	btl	$2, %eax
	jb	.LBB1_24
# BB#25:                                # %vector.body.prol
                                        #   in Loop: Header=BB1_2 Depth=1
	movupd	(%rsi), %xmm0
	movupd	16(%rsi), %xmm1
	mulpd	%xmm0, %xmm0
	mulpd	%xmm1, %xmm1
	movupd	%xmm0, (%rdx)
	movupd	%xmm1, 16(%rdx)
	movups	%xmm2, (%r10)
	movups	%xmm2, 16(%r10)
	movl	$4, %ebp
	testq	%rbx, %rbx
	jne	.LBB1_27
	jmp	.LBB1_29
.LBB1_17:                               #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_30
.LBB1_24:                               #   in Loop: Header=BB1_2 Depth=1
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB1_29
.LBB1_27:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%r8, %r11
	subq	%rbp, %r11
	leaq	48(%rsi,%rbp,8), %rax
	leaq	48(%rdx,%rbp,8), %rbx
	leaq	48(%r10,%rbp,8), %rbp
	.p2align	4, 0x90
.LBB1_28:                               # %vector.body
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rax), %xmm0
	movupd	-32(%rax), %xmm1
	mulpd	%xmm0, %xmm0
	mulpd	%xmm1, %xmm1
	movupd	%xmm0, -48(%rbx)
	movupd	%xmm1, -32(%rbx)
	movups	%xmm2, -48(%rbp)
	movups	%xmm2, -32(%rbp)
	movupd	-16(%rax), %xmm0
	movupd	(%rax), %xmm1
	mulpd	%xmm0, %xmm0
	mulpd	%xmm1, %xmm1
	movupd	%xmm0, -16(%rbx)
	movupd	%xmm1, (%rbx)
	movups	%xmm2, -16(%rbp)
	movups	%xmm2, (%rbp)
	addq	$64, %rax
	addq	$64, %rbx
	addq	$64, %rbp
	addq	$-8, %r11
	jne	.LBB1_28
.LBB1_29:                               # %middle.block
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpq	%r8, %r9
	movq	%r8, %rax
	jne	.LBB1_30
	jmp	.LBB1_31
.LBB1_19:                               #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	movabsq	$4607182418800017408, %r14 # imm = 0x3FF0000000000000
	movq	%r12, %r15
	leaq	16(%rsp), %r12
	jmp	.LBB1_30
.LBB1_21:                               #   in Loop: Header=BB1_2 Depth=1
	xorl	%eax, %eax
	movabsq	$4607182418800017408, %r14 # imm = 0x3FF0000000000000
	leaq	16(%rsp), %r12
	.p2align	4, 0x90
.LBB1_30:                               # %test_matrix.exit
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rsi,%rax,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, (%rdx,%rax,8)
	movq	$0, (%r10,%rax,8)
	incq	%rax
	cmpq	%r9, %rax
	jl	.LBB1_30
.LBB1_31:                               # %test_matrix.exit._crit_edge
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	$0, (%rdx)
	movq	$0, (%rsi)
	movsd	64(%rsp), %xmm0         # xmm0 = mem[0],zero
	movq	24(%rsp), %rax
	addq	$-8, %rax
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	$1, %r8d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	dbisect
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	incl	%r13d
	cmpl	12(%rsp), %r13d
	xorps	%xmm2, %xmm2
	jl	.LBB1_2
.LBB1_5:                                # %.preheader
	cmpl	$2, 8(%rsp)
	jl	.LBB1_8
# BB#6:                                 # %.lr.ph.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	incq	%rbx
	movl	$.L.str.2, %edi
	movb	$1, %al
	movl	%ebx, %esi
	callq	printf
	movslq	8(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_7
.LBB1_8:                                # %._crit_edge
	movsd	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	36(%rsp), %esi
	movl	$.L.str.3, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%lf"
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%5d %.15e\n"
	.size	.L.str.2, 11

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"eps2 = %e,  k = %d\n"
	.size	.L.str.3, 20


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
