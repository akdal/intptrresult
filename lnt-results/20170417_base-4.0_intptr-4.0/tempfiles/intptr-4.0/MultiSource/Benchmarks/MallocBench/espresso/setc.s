	.text
	.file	"setc.bc"
	.globl	full_row
	.p2align	4, 0x90
	.type	full_row,@function
full_row:                               # @full_row
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	movq	cube+88(%rip), %rcx
	andl	$1023, %eax             # imm = 0x3FF
	incq	%rax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi,%rax,4), %edx
	orl	-4(%rdi,%rax,4), %edx
	cmpl	-4(%rcx,%rax,4), %edx
	jne	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB0_1
# BB#4:
	movl	$1, %eax
	retq
.LBB0_2:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	full_row, .Lfunc_end0-full_row
	.cfi_endproc

	.globl	cdist0
	.p2align	4, 0x90
	.type	cdist0,@function
cdist0:                                 # @cdist0
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movslq	cube+108(%rip), %r8
	cmpq	$-1, %r8
	je	.LBB1_6
# BB#1:
	movl	(%rsi,%r8,4), %eax
	andl	(%rdi,%r8,4), %eax
	movl	%eax, %edx
	shrl	%edx
	orl	%eax, %edx
	notl	%edx
	xorl	%eax, %eax
	testl	cube+104(%rip), %edx
	jne	.LBB1_14
# BB#2:                                 # %.preheader
	cmpl	$2, %r8d
	jl	.LBB1_6
# BB#3:                                 # %.lr.ph58.preheader
	movl	$1, %edx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph58
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rdx,4), %ecx
	andl	(%rdi,%rdx,4), %ecx
	movl	%ecx, %ebx
	shrl	%ebx
	orl	%ecx, %ebx
	andl	$1431655765, %ebx       # imm = 0x55555555
	cmpl	$1431655765, %ebx       # imm = 0x55555555
	jne	.LBB1_14
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jl	.LBB1_4
.LBB1_6:                                # %.loopexit
	movslq	cube+8(%rip), %r14
	movslq	cube+4(%rip), %r11
	movl	$1, %eax
	cmpl	%r11d, %r14d
	jge	.LBB1_14
# BB#7:                                 # %.lr.ph56
	movq	cube+72(%rip), %r8
	movq	cube+48(%rip), %r9
	movq	cube+40(%rip), %r10
.LBB1_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_10 Depth 2
	movslq	(%r9,%r14,4), %rdx
	movslq	(%r10,%r14,4), %rbx
	cmpl	%edx, %ebx
	jg	.LBB1_13
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_8 Depth=1
	movq	(%r8,%r14,8), %rcx
	decq	%rbx
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph
                                        #   Parent Loop BB1_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rsi,%rbx,4), %ebp
	andl	4(%rdi,%rbx,4), %ebp
	testl	4(%rcx,%rbx,4), %ebp
	jne	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_10 Depth=2
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB1_10
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_12:                               #   in Loop: Header=BB1_8 Depth=1
	incq	%r14
	cmpq	%r11, %r14
	jl	.LBB1_8
	jmp	.LBB1_14
.LBB1_13:
	xorl	%eax, %eax
.LBB1_14:                               # %.critedge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	cdist0, .Lfunc_end1-cdist0
	.cfi_endproc

	.globl	cdist01
	.p2align	4, 0x90
	.type	cdist01,@function
cdist01:                                # @cdist01
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movslq	cube+108(%rip), %r8
	cmpq	$-1, %r8
	je	.LBB2_1
# BB#2:
	movl	(%rsi,%r8,4), %ecx
	andl	(%rdi,%r8,4), %ecx
	movl	%ecx, %eax
	shrl	%eax
	orl	%ecx, %eax
	notl	%eax
	andl	cube+104(%rip), %eax
	je	.LBB2_3
# BB#4:
	movzbl	%al, %ecx
	movzbl	%ah, %edx  # NOREX
	movl	bit_count(,%rdx,4), %edx
	addl	bit_count(,%rcx,4), %edx
	movl	%eax, %ecx
	shrl	$16, %ecx
	movzbl	%cl, %ecx
	addl	bit_count(,%rcx,4), %edx
	shrl	$24, %eax
	addl	bit_count(,%rax,4), %edx
	movl	$2, %eax
	cmpl	$1, %edx
	jle	.LBB2_5
	jmp	.LBB2_21
.LBB2_1:
	xorl	%edx, %edx
	jmp	.LBB2_11
.LBB2_3:
	xorl	%edx, %edx
.LBB2_5:                                # %.preheader
	cmpl	$2, %r8d
	jl	.LBB2_11
# BB#6:                                 # %.lr.ph83.preheader
	movl	$1, %r9d
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph83
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%r9,4), %eax
	andl	(%rdi,%r9,4), %eax
	movl	%eax, %ecx
	shrl	%ecx
	orl	%eax, %ecx
	andl	$1431655765, %ecx       # imm = 0x55555555
	cmpl	$1431655765, %ecx       # imm = 0x55555555
	je	.LBB2_10
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	movl	$2, %eax
	cmpl	$1, %edx
	je	.LBB2_21
# BB#9:                                 #   in Loop: Header=BB2_7 Depth=1
	xorl	$1431655765, %ecx       # imm = 0x55555555
	movl	%ecx, %r10d
	andl	$85, %r10d
	movl	%ecx, %r11d
	shrl	$8, %r11d
	andl	$85, %r11d
	movl	%ecx, %ebx
	shrl	$16, %ebx
	andl	$85, %ebx
	shrl	$24, %ecx
	addl	bit_count(,%r10,4), %edx
	addl	bit_count(,%r11,4), %edx
	addl	bit_count(,%rbx,4), %edx
	addl	bit_count(,%rcx,4), %edx
	cmpl	$1, %edx
	jg	.LBB2_21
.LBB2_10:                               #   in Loop: Header=BB2_7 Depth=1
	incq	%r9
	cmpq	%r8, %r9
	jl	.LBB2_7
.LBB2_11:                               # %.loopexit70
	movslq	cube+8(%rip), %r14
	movslq	cube+4(%rip), %r8
	cmpl	%r8d, %r14d
	jge	.LBB2_12
# BB#13:                                # %.lr.ph78
	movq	cube+72(%rip), %r9
	movq	cube+48(%rip), %r10
	movq	cube+40(%rip), %r11
	movl	%edx, %eax
	.p2align	4, 0x90
.LBB2_14:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_17 Depth 2
	movslq	(%r10,%r14,4), %rdx
	movslq	(%r11,%r14,4), %rbx
	cmpl	%edx, %ebx
	jg	.LBB2_18
# BB#15:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_14 Depth=1
	movq	(%r9,%r14,8), %rcx
	decq	%rbx
	.p2align	4, 0x90
.LBB2_17:                               # %.lr.ph
                                        #   Parent Loop BB2_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rsi,%rbx,4), %ebp
	andl	4(%rdi,%rbx,4), %ebp
	testl	4(%rcx,%rbx,4), %ebp
	jne	.LBB2_20
# BB#16:                                #   in Loop: Header=BB2_17 Depth=2
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB2_17
.LBB2_18:                               # %._crit_edge
                                        #   in Loop: Header=BB2_14 Depth=1
	testl	%eax, %eax
	jg	.LBB2_22
# BB#19:                                #   in Loop: Header=BB2_14 Depth=1
	incl	%eax
.LBB2_20:                               # %.loopexit
                                        #   in Loop: Header=BB2_14 Depth=1
	incq	%r14
	cmpq	%r8, %r14
	jl	.LBB2_14
	jmp	.LBB2_21
.LBB2_12:
	movl	%edx, %eax
	jmp	.LBB2_21
.LBB2_22:
	movl	$2, %eax
.LBB2_21:                               # %.loopexit69
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cdist01, .Lfunc_end2-cdist01
	.cfi_endproc

	.globl	cdist
	.p2align	4, 0x90
	.type	cdist,@function
cdist:                                  # @cdist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movslq	cube+108(%rip), %rcx
	cmpq	$-1, %rcx
	je	.LBB3_1
# BB#2:
	movl	(%rsi,%rcx,4), %eax
	andl	(%rdi,%rcx,4), %eax
	movl	%eax, %edx
	shrl	%edx
	orl	%eax, %edx
	notl	%edx
	andl	cube+104(%rip), %edx
	je	.LBB3_3
# BB#4:
	movzbl	%dl, %ebx
	movzbl	%dh, %eax  # NOREX
	movl	bit_count(,%rax,4), %eax
	addl	bit_count(,%rbx,4), %eax
	movl	%edx, %ebx
	shrl	$16, %ebx
	movzbl	%bl, %ebx
	addl	bit_count(,%rbx,4), %eax
	shrl	$24, %edx
	addl	bit_count(,%rdx,4), %eax
	cmpl	$2, %ecx
	jge	.LBB3_6
	jmp	.LBB3_10
.LBB3_1:
	xorl	%eax, %eax
	jmp	.LBB3_10
.LBB3_3:
	xorl	%eax, %eax
	cmpl	$2, %ecx
	jl	.LBB3_10
.LBB3_6:                                # %.lr.ph67.preheader
	movl	%ecx, %r8d
	leaq	4(%rdi), %r9
	leaq	4(%rsi), %r11
	decq	%r8
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph67
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r11), %ebx
	andl	(%r9), %ebx
	movl	%ebx, %edx
	shrl	%edx
	orl	%ebx, %edx
	andl	$1431655765, %edx       # imm = 0x55555555
	cmpl	$1431655765, %edx       # imm = 0x55555555
	je	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=1
	xorl	$1431655765, %edx       # imm = 0x55555555
	movl	%edx, %r10d
	andl	$85, %r10d
	movl	%edx, %ebx
	shrl	$8, %ebx
	andl	$85, %ebx
	movl	%edx, %ecx
	shrl	$16, %ecx
	andl	$85, %ecx
	shrl	$24, %edx
	addl	bit_count(,%r10,4), %eax
	addl	bit_count(,%rbx,4), %eax
	addl	bit_count(,%rcx,4), %eax
	addl	bit_count(,%rdx,4), %eax
.LBB3_9:                                #   in Loop: Header=BB3_7 Depth=1
	addq	$4, %r9
	addq	$4, %r11
	decq	%r8
	jne	.LBB3_7
.LBB3_10:                               # %.loopexit57
	movslq	cube+8(%rip), %r14
	movslq	cube+4(%rip), %r10
	cmpl	%r10d, %r14d
	jge	.LBB3_18
# BB#11:                                # %.lr.ph63
	movq	cube+72(%rip), %r8
	movq	cube+48(%rip), %r9
	movq	cube+40(%rip), %r11
	.p2align	4, 0x90
.LBB3_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_15 Depth 2
	movslq	(%r9,%r14,4), %rdx
	movslq	(%r11,%r14,4), %rbx
	cmpl	%edx, %ebx
	jg	.LBB3_16
# BB#13:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_12 Depth=1
	movq	(%r8,%r14,8), %rcx
	decq	%rbx
	.p2align	4, 0x90
.LBB3_15:                               # %.lr.ph
                                        #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rsi,%rbx,4), %ebp
	andl	4(%rdi,%rbx,4), %ebp
	testl	4(%rcx,%rbx,4), %ebp
	jne	.LBB3_17
# BB#14:                                #   in Loop: Header=BB3_15 Depth=2
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB3_15
.LBB3_16:                               # %._crit_edge
                                        #   in Loop: Header=BB3_12 Depth=1
	incl	%eax
.LBB3_17:                               # %.loopexit
                                        #   in Loop: Header=BB3_12 Depth=1
	incq	%r14
	cmpq	%r10, %r14
	jl	.LBB3_12
.LBB3_18:                               # %._crit_edge64
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	cdist, .Lfunc_end3-cdist
	.cfi_endproc

	.globl	force_lower
	.p2align	4, 0x90
	.type	force_lower,@function
force_lower:                            # @force_lower
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movslq	cube+108(%rip), %rax
	cmpq	$-1, %rax
	je	.LBB4_8
# BB#1:
	movl	(%rsi,%rax,4), %ecx
	movl	(%rdx,%rax,4), %ebx
	andl	%ecx, %ebx
	movl	%ebx, %ebp
	shrl	%ebp
	orl	%ebx, %ebp
	notl	%ebp
	andl	cube+104(%rip), %ebp
	je	.LBB4_3
# BB#2:
	leal	(%rbp,%rbp), %ebx
	orl	%ebp, %ebx
	andl	%ecx, %ebx
	orl	%ebx, (%rdi,%rax,4)
.LBB4_3:                                # %.preheader
	cmpl	$2, %eax
	jl	.LBB4_8
# BB#4:                                 # %.lr.ph77.preheader
	movl	%eax, %r9d
	leaq	4(%rsi), %rcx
	leaq	4(%rdx), %rbp
	leaq	4(%rdi), %r10
	decq	%r9
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph77
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %r8d
	movl	(%rbp), %ebx
	andl	%r8d, %ebx
	movl	%ebx, %eax
	shrl	%eax
	orl	%ebx, %eax
	andl	$1431655765, %eax       # imm = 0x55555555
	cmpl	$1431655765, %eax       # imm = 0x55555555
	je	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	xorl	$1431655765, %eax       # imm = 0x55555555
	leal	(%rax,%rax), %ebx
	orl	%eax, %ebx
	andl	%r8d, %ebx
	orl	%ebx, (%r10)
.LBB4_7:                                #   in Loop: Header=BB4_5 Depth=1
	addq	$4, %rcx
	addq	$4, %rbp
	addq	$4, %r10
	decq	%r9
	jne	.LBB4_5
.LBB4_8:                                # %.loopexit67
	movslq	cube+8(%rip), %r13
	movl	cube+4(%rip), %r10d
	cmpl	%r10d, %r13d
	jge	.LBB4_27
# BB#9:                                 # %.lr.ph74
	movq	cube+72(%rip), %r11
	movq	cube+48(%rip), %r9
	movq	cube+40(%rip), %r12
	leaq	4(%rdi), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	4(%rsi), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	leaq	16(%rdi), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leaq	16(%rsi), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	%r11, -56(%rsp)         # 8-byte Spill
	movq	%r9, -64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_13 Depth 2
                                        #     Child Loop BB4_21 Depth 2
                                        #     Child Loop BB4_24 Depth 2
	movslq	(%r9,%r13,4), %rbx
	movslq	(%r12,%r13,4), %rcx
	cmpl	%ebx, %ecx
	jg	.LBB4_26
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_10 Depth=1
	movq	(%r11,%r13,8), %r8
	leaq	-1(%rcx), %rbp
	.p2align	4, 0x90
.LBB4_13:                               # %.lr.ph
                                        #   Parent Loop BB4_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rdx,%rbp,4), %eax
	andl	4(%rsi,%rbp,4), %eax
	testl	4(%r8,%rbp,4), %eax
	jne	.LBB4_26
# BB#12:                                #   in Loop: Header=BB4_13 Depth=2
	incq	%rbp
	cmpq	%rbx, %rbp
	jl	.LBB4_13
# BB#14:                                # %._crit_edge
                                        #   in Loop: Header=BB4_10 Depth=1
	cmpl	%ebx, %ecx
	jg	.LBB4_26
# BB#15:                                # %.lr.ph71.preheader
                                        #   in Loop: Header=BB4_10 Depth=1
	cmpq	%rbx, %rcx
	movq	%rbx, %rbp
	cmovgeq	%rcx, %rbp
	incq	%rbp
	subq	%rcx, %rbp
	cmpq	$8, %rbp
	jb	.LBB4_23
# BB#16:                                # %min.iters.checked
                                        #   in Loop: Header=BB4_10 Depth=1
	movq	%rbp, %rax
	andq	$-8, %rax
	je	.LBB4_23
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_10 Depth=1
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rcx,4), %r10
	cmpq	%rbx, %rcx
	movq	%rbx, %r9
	cmovgeq	%rcx, %r9
	movq	-16(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9,4), %r11
	leaq	(%rsi,%rcx,4), %r14
	movq	-24(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9,4), %r15
	leaq	(%r8,%rcx,4), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	leaq	4(%r8,%r9,4), %rax
	cmpq	%r15, %r10
	sbbb	%r15b, %r15b
	cmpq	%r11, %r14
	sbbb	%r14b, %r14b
	andb	%r15b, %r14b
	cmpq	%rax, %r10
	sbbb	%r10b, %r10b
	cmpq	%r11, -8(%rsp)          # 8-byte Folded Reload
	sbbb	%r11b, %r11b
	testb	$1, %r14b
	jne	.LBB4_18
# BB#19:                                # %vector.memcheck
                                        #   in Loop: Header=BB4_10 Depth=1
	andb	%r11b, %r10b
	andb	$1, %r10b
	jne	.LBB4_18
# BB#20:                                # %vector.body.preheader
                                        #   in Loop: Header=BB4_10 Depth=1
	movq	-40(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,4), %r15
	leaq	16(%r8,%rcx,4), %r10
	movq	-48(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,4), %r11
	movq	-32(%rsp), %rax         # 8-byte Reload
	leaq	(%rcx,%rax), %rcx
	movq	%rax, %r14
	movq	-64(%rsp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_21:                               # %vector.body
                                        #   Parent Loop BB4_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%r11), %xmm0
	movups	(%r11), %xmm1
	movups	-16(%r10), %xmm2
	movups	(%r10), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	-16(%r15), %xmm0
	movups	(%r15), %xmm1
	orps	%xmm2, %xmm0
	orps	%xmm3, %xmm1
	movups	%xmm0, -16(%r15)
	movups	%xmm1, (%r15)
	addq	$32, %r15
	addq	$32, %r10
	addq	$32, %r11
	addq	$-8, %r14
	jne	.LBB4_21
# BB#22:                                # %middle.block
                                        #   in Loop: Header=BB4_10 Depth=1
	cmpq	%rax, %rbp
	movq	-56(%rsp), %r11         # 8-byte Reload
	jne	.LBB4_23
	jmp	.LBB4_25
.LBB4_18:                               #   in Loop: Header=BB4_10 Depth=1
	movq	-56(%rsp), %r11         # 8-byte Reload
	movq	-64(%rsp), %r9          # 8-byte Reload
.LBB4_23:                               # %.lr.ph71.preheader111
                                        #   in Loop: Header=BB4_10 Depth=1
	decq	%rcx
	.p2align	4, 0x90
.LBB4_24:                               # %.lr.ph71
                                        #   Parent Loop BB4_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%r8,%rcx,4), %eax
	andl	4(%rsi,%rcx,4), %eax
	orl	%eax, 4(%rdi,%rcx,4)
	incq	%rcx
	cmpq	%rbx, %rcx
	jl	.LBB4_24
.LBB4_25:                               # %.loopexit.loopexit
                                        #   in Loop: Header=BB4_10 Depth=1
	movl	cube+4(%rip), %r10d
	.p2align	4, 0x90
.LBB4_26:                               # %.loopexit
                                        #   in Loop: Header=BB4_10 Depth=1
	incq	%r13
	movslq	%r10d, %rax
	cmpq	%rax, %r13
	jl	.LBB4_10
.LBB4_27:                               # %._crit_edge75
	movq	%rdi, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	force_lower, .Lfunc_end4-force_lower
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.zero	16,85
	.text
	.globl	consensus
	.p2align	4, 0x90
	.type	consensus,@function
consensus:                              # @consensus
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 208
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	cube(%rip), %ecx
	movl	$1, %eax
	cmpl	$33, %ecx
	jl	.LBB5_2
# BB#1:
	decl	%ecx
	sarl	$5, %ecx
	incl	%ecx
	movl	%ecx, %eax
.LBB5_2:
	movl	%eax, (%r15)
	movslq	%eax, %rcx
	movl	%ecx, %edx
	notl	%edx
	cmpl	$-3, %edx
	movl	$-2, %esi
	cmovgl	%edx, %esi
	leal	1(%rax,%rsi), %eax
	subq	%rax, %rcx
	leaq	(%r15,%rcx,4), %rdi
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movslq	cube+108(%rip), %rax
	cmpq	$-1, %rax
	je	.LBB5_27
# BB#3:
	movl	(%r14,%rax,4), %ecx
	andl	(%rbx,%rax,4), %ecx
	movl	%ecx, (%r15,%rax,4)
	movl	%ecx, %edx
	shrl	%edx
	orl	%ecx, %edx
	notl	%edx
	andl	cube+104(%rip), %edx
	je	.LBB5_5
# BB#4:
	leal	(%rdx,%rdx), %esi
	orl	%edx, %esi
	movl	(%r14,%rax,4), %edx
	orl	(%rbx,%rax,4), %edx
	andl	%esi, %edx
	orl	%ecx, %edx
	movl	%edx, (%r15,%rax,4)
.LBB5_5:                                # %.preheader
	cmpl	$2, %eax
	jl	.LBB5_27
# BB#6:                                 # %.lr.ph104.preheader
	movl	%eax, %r12d
	leaq	-1(%r12), %r9
	cmpq	$4, %r9
	jb	.LBB5_22
# BB#8:                                 # %min.iters.checked
	movq	%r9, %r8
	andq	$-4, %r8
	je	.LBB5_22
# BB#9:                                 # %vector.memcheck
	leaq	4(%r15), %rdi
	leaq	(%r15,%r12,4), %rsi
	leaq	4(%rbx), %rbp
	leaq	(%rbx,%r12,4), %rcx
	leaq	4(%r14), %rdx
	leaq	(%r14,%r12,4), %r10
	cmpq	%rcx, %rdi
	sbbb	%r11b, %r11b
	cmpq	%rsi, %rbp
	sbbb	%cl, %cl
	andb	%r11b, %cl
	cmpq	%r10, %rdi
	sbbb	%r11b, %r11b
	cmpq	%rsi, %rdx
	sbbb	%r10b, %r10b
	movl	$1, %esi
	testb	$1, %cl
	jne	.LBB5_23
# BB#10:                                # %vector.memcheck
	andb	%r10b, %r11b
	andb	$1, %r11b
	jne	.LBB5_23
# BB#11:                                # %vector.body.preheader
	movq	%r8, %rsi
	orq	$1, %rsi
	pcmpeqd	%xmm0, %xmm0
	movdqa	.LCPI5_0(%rip), %xmm1   # xmm1 = [1431655765,1431655765,1431655765,1431655765]
	pxor	%xmm2, %xmm2
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB5_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rbp), %xmm3
	movdqu	(%rdx), %xmm5
	pand	%xmm3, %xmm5
	movdqu	%xmm5, (%rdi)
	movdqa	%xmm5, %xmm3
	psrld	$1, %xmm3
	por	%xmm5, %xmm3
	pandn	%xmm1, %xmm3
	movdqa	%xmm3, %xmm6
	paddd	%xmm6, %xmm6
	por	%xmm3, %xmm6
	pcmpeqd	%xmm2, %xmm3
	pxor	%xmm0, %xmm3
	movdqu	(%rbp), %xmm7
	movdqu	(%rdx), %xmm4
	por	%xmm7, %xmm4
	pand	%xmm6, %xmm4
	por	%xmm5, %xmm4
	movdqa	%xmm3, 128(%rsp)
	testb	$1, 128(%rsp)
	je	.LBB5_14
# BB#13:                                # %pred.store.if
                                        #   in Loop: Header=BB5_12 Depth=1
	movd	%xmm4, (%rdi)
.LBB5_14:                               # %pred.store.continue
                                        #   in Loop: Header=BB5_12 Depth=1
	movdqa	%xmm3, 112(%rsp)
	testb	$1, 116(%rsp)
	je	.LBB5_16
# BB#15:                                # %pred.store.if132
                                        #   in Loop: Header=BB5_12 Depth=1
	pshufd	$229, %xmm4, %xmm5      # xmm5 = xmm4[1,1,2,3]
	movd	%xmm5, 4(%rdi)
.LBB5_16:                               # %pred.store.continue133
                                        #   in Loop: Header=BB5_12 Depth=1
	movdqa	%xmm3, 96(%rsp)
	testb	$1, 104(%rsp)
	je	.LBB5_18
# BB#17:                                # %pred.store.if134
                                        #   in Loop: Header=BB5_12 Depth=1
	pshufd	$78, %xmm4, %xmm5       # xmm5 = xmm4[2,3,0,1]
	movd	%xmm5, 8(%rdi)
.LBB5_18:                               # %pred.store.continue135
                                        #   in Loop: Header=BB5_12 Depth=1
	movdqa	%xmm3, 80(%rsp)
	testb	$1, 92(%rsp)
	je	.LBB5_20
# BB#19:                                # %pred.store.if136
                                        #   in Loop: Header=BB5_12 Depth=1
	pshufd	$231, %xmm4, %xmm3      # xmm3 = xmm4[3,1,2,3]
	movd	%xmm3, 12(%rdi)
.LBB5_20:                               # %pred.store.continue137
                                        #   in Loop: Header=BB5_12 Depth=1
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rdx
	addq	$-4, %rcx
	jne	.LBB5_12
# BB#21:                                # %middle.block
	cmpq	%r8, %r9
	jne	.LBB5_23
	jmp	.LBB5_27
.LBB5_22:
	movl	$1, %esi
.LBB5_23:                               # %.lr.ph104.preheader202
	leaq	(%rbx,%rsi,4), %rcx
	leaq	(%r14,%rsi,4), %rdx
	leaq	(%r15,%rsi,4), %rdi
	subq	%rsi, %r12
	.p2align	4, 0x90
.LBB5_24:                               # %.lr.ph104
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %esi
	andl	(%rcx), %esi
	movl	%esi, (%rdi)
	movl	%esi, %ebp
	shrl	%ebp
	orl	%esi, %ebp
	andl	$1431655765, %ebp       # imm = 0x55555555
	cmpl	$1431655765, %ebp       # imm = 0x55555555
	je	.LBB5_26
# BB#25:                                #   in Loop: Header=BB5_24 Depth=1
	xorl	$1431655765, %ebp       # imm = 0x55555555
	leal	(%rbp,%rbp), %eax
	orl	%ebp, %eax
	movl	(%rdx), %ebp
	orl	(%rcx), %ebp
	andl	%eax, %ebp
	orl	%esi, %ebp
	movl	%ebp, (%rdi)
.LBB5_26:                               #   in Loop: Header=BB5_24 Depth=1
	addq	$4, %rcx
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%r12
	jne	.LBB5_24
.LBB5_27:                               # %.loopexit93
	movslq	cube+8(%rip), %rax
	cmpl	cube+4(%rip), %eax
	jge	.LBB5_48
# BB#28:                                # %.lr.ph101
	movq	cube+72(%rip), %r8
	movq	cube+48(%rip), %r9
	movq	cube+40(%rip), %r13
	leaq	4(%r15), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	4(%rbx), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	4(%r14), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	16(%r15), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	16(%r14), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	16(%rbx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%r13, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB5_29:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_31 Depth 2
                                        #     Child Loop BB5_42 Depth 2
                                        #     Child Loop BB5_46 Depth 2
	movq	(%r8,%rax,8), %rdi
	movslq	(%r9,%rax,4), %rbp
	movslq	(%r13,%rax,4), %rcx
	cmpl	%ebp, %ecx
	jg	.LBB5_35
# BB#30:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_29 Depth=1
	decq	%rcx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB5_31:                               # %.lr.ph
                                        #   Parent Loop BB5_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%r14,%rcx,4), %esi
	andl	4(%rbx,%rcx,4), %esi
	andl	4(%rdi,%rcx,4), %esi
	je	.LBB5_33
# BB#32:                                #   in Loop: Header=BB5_31 Depth=2
	orl	%esi, 4(%r15,%rcx,4)
	xorl	%edx, %edx
.LBB5_33:                               #   in Loop: Header=BB5_31 Depth=2
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB5_31
# BB#34:                                # %._crit_edge
                                        #   in Loop: Header=BB5_29 Depth=1
	testl	%edx, %edx
	je	.LBB5_47
.LBB5_35:                               # %._crit_edge.thread
                                        #   in Loop: Header=BB5_29 Depth=1
	movslq	(%r13,%rax,4), %r10
	cmpl	%ebp, %r10d
	jg	.LBB5_47
# BB#36:                                # %.lr.ph98.preheader
                                        #   in Loop: Header=BB5_29 Depth=1
	cmpq	%rbp, %r10
	movq	%rbp, %r11
	cmovgeq	%r10, %r11
	incq	%r11
	subq	%r10, %r11
	cmpq	$8, %r11
	jb	.LBB5_45
# BB#37:                                # %min.iters.checked143
                                        #   in Loop: Header=BB5_29 Depth=1
	movq	%r11, %r12
	andq	$-8, %r12
	je	.LBB5_45
# BB#38:                                # %vector.memcheck181
                                        #   in Loop: Header=BB5_29 Depth=1
	leaq	(%r15,%r10,4), %r13
	cmpq	%rbp, %r10
	movq	%rbp, %rsi
	cmovgeq	%r10, %rsi
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rsi,4), %r8
	leaq	(%rdi,%r10,4), %r9
	leaq	4(%rdi,%rsi,4), %rdx
	cmpq	%rdx, %r13
	sbbb	%cl, %cl
	cmpq	%r8, %r9
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rsi,4), %rdx
	sbbb	%r9b, %r9b
	andb	%cl, %r9b
	cmpq	%rdx, %r13
	leaq	(%rbx,%r10,4), %rcx
	sbbb	%dl, %dl
	cmpq	%r8, %rcx
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rsi
	sbbb	%cl, %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	cmpq	%rsi, %r13
	leaq	(%r14,%r10,4), %rsi
	sbbb	%cl, %cl
	cmpq	%r8, %rsi
	sbbb	%r8b, %r8b
	testb	$1, %r9b
	jne	.LBB5_44
# BB#39:                                # %vector.memcheck181
                                        #   in Loop: Header=BB5_29 Depth=1
	andb	7(%rsp), %dl            # 1-byte Folded Reload
	andb	$1, %dl
	jne	.LBB5_44
# BB#40:                                # %vector.memcheck181
                                        #   in Loop: Header=BB5_29 Depth=1
	andb	%r8b, %cl
	andb	$1, %cl
	jne	.LBB5_44
# BB#41:                                # %vector.body138.preheader
                                        #   in Loop: Header=BB5_29 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r10,4), %r8
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r10,4), %r9
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r10,4), %r13
	leaq	16(%rdi,%r10,4), %rcx
	leaq	(%r10,%r12), %r10
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_42:                               # %vector.body138
                                        #   Parent Loop BB5_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rcx,%rdx,4), %xmm0
	movdqu	(%rcx,%rdx,4), %xmm1
	movdqu	-16(%r13,%rdx,4), %xmm2
	movdqu	(%r13,%rdx,4), %xmm3
	movdqu	-16(%r9,%rdx,4), %xmm4
	movdqu	(%r9,%rdx,4), %xmm5
	por	%xmm2, %xmm4
	por	%xmm3, %xmm5
	pand	%xmm0, %xmm4
	pand	%xmm1, %xmm5
	movdqu	-16(%r8,%rdx,4), %xmm0
	movdqu	(%r8,%rdx,4), %xmm1
	por	%xmm4, %xmm0
	por	%xmm5, %xmm1
	movdqu	%xmm0, -16(%r8,%rdx,4)
	movdqu	%xmm1, (%r8,%rdx,4)
	addq	$8, %rdx
	cmpq	%rdx, %r12
	jne	.LBB5_42
# BB#43:                                # %middle.block139
                                        #   in Loop: Header=BB5_29 Depth=1
	cmpq	%r12, %r11
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	jne	.LBB5_45
	jmp	.LBB5_47
.LBB5_44:                               #   in Loop: Header=BB5_29 Depth=1
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_45:                               # %.lr.ph98.preheader201
                                        #   in Loop: Header=BB5_29 Depth=1
	decq	%r10
	.p2align	4, 0x90
.LBB5_46:                               # %.lr.ph98
                                        #   Parent Loop BB5_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%r14,%r10,4), %ecx
	orl	4(%rbx,%r10,4), %ecx
	andl	4(%rdi,%r10,4), %ecx
	orl	%ecx, 4(%r15,%r10,4)
	incq	%r10
	cmpq	%rbp, %r10
	jl	.LBB5_46
.LBB5_47:                               # %.loopexit
                                        #   in Loop: Header=BB5_29 Depth=1
	incq	%rax
	movslq	cube+4(%rip), %rcx
	cmpq	%rcx, %rax
	jl	.LBB5_29
.LBB5_48:                               # %._crit_edge102
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	consensus, .Lfunc_end5-consensus
	.cfi_endproc

	.globl	cactive
	.p2align	4, 0x90
	.type	cactive,@function
cactive:                                # @cactive
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 48
.Lcfi48:
	.cfi_offset %rbx, -48
.Lcfi49:
	.cfi_offset %r12, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movslq	cube+108(%rip), %r14
	cmpq	$-1, %r14
	je	.LBB6_4
# BB#1:
	movl	(%r15,%r14,4), %eax
	movl	%eax, %ecx
	shrl	%ecx
	andl	%eax, %ecx
	notl	%ecx
	andl	cube+104(%rip), %ecx
	je	.LBB6_5
# BB#2:
	movzbl	%cl, %eax
	movzbl	%ch, %edx  # NOREX
	movl	bit_count(,%rdx,4), %ebp
	addl	bit_count(,%rax,4), %ebp
	movl	%ecx, %eax
	shrl	$16, %eax
	movzbl	%al, %eax
	addl	bit_count(,%rax,4), %ebp
	movl	%ecx, %eax
	shrl	$24, %eax
	addl	bit_count(,%rax,4), %ebp
	movl	$-1, %eax
	cmpl	$1, %ebp
	jg	.LBB6_22
# BB#3:
	movl	%r14d, %ebx
	shll	$4, %ebx
	xorl	%eax, %eax
	movl	%ecx, %edi
	callq	bit_index
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	leal	-16(%rbx,%rcx), %eax
	cmpl	$2, %r14d
	jge	.LBB6_6
	jmp	.LBB6_11
.LBB6_4:
	xorl	%ebp, %ebp
	movl	$-1, %eax
	jmp	.LBB6_11
.LBB6_5:
	xorl	%ebp, %ebp
	movl	$-1, %eax
	cmpl	$2, %r14d
	jl	.LBB6_11
.LBB6_6:                                # %.lr.ph91.preheader
	movl	$1, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB6_7:                                # %.lr.ph91
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rbx,4), %ecx
	movl	%ecx, %edi
	shrl	%edi
	andl	%ecx, %edi
	andl	$1431655765, %edi       # imm = 0x55555555
	cmpl	$1431655765, %edi       # imm = 0x55555555
	je	.LBB6_10
# BB#8:                                 #   in Loop: Header=BB6_7 Depth=1
	xorl	$1431655765, %edi       # imm = 0x55555555
	movl	%edi, %eax
	andl	$85, %eax
	movl	%edi, %ecx
	shrl	$8, %ecx
	andl	$85, %ecx
	movl	%edi, %edx
	shrl	$16, %edx
	andl	$85, %edx
	movl	%edi, %esi
	shrl	$24, %esi
	addl	bit_count(,%rax,4), %ebp
	addl	bit_count(,%rcx,4), %ebp
	addl	bit_count(,%rdx,4), %ebp
	addl	bit_count(,%rsi,4), %ebp
	cmpl	$1, %ebp
	jg	.LBB6_21
# BB#9:                                 #   in Loop: Header=BB6_7 Depth=1
	xorl	%eax, %eax
	callq	bit_index
	movl	%eax, %ecx
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	addl	%r12d, %eax
.LBB6_10:                               #   in Loop: Header=BB6_7 Depth=1
	incq	%rbx
	addl	$16, %r12d
	cmpq	%r14, %rbx
	jl	.LBB6_7
.LBB6_11:                               # %.loopexit77
	movslq	cube+8(%rip), %rcx
	movslq	cube+4(%rip), %r11
	cmpl	%r11d, %ecx
	jge	.LBB6_22
# BB#12:                                # %.lr.ph85
	movq	cube+72(%rip), %r8
	movq	cube+48(%rip), %r9
	movq	cube+40(%rip), %r10
	.p2align	4, 0x90
.LBB6_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_15 Depth 2
	movslq	(%r9,%rcx,4), %rsi
	movslq	(%r10,%rcx,4), %rdi
	cmpl	%esi, %edi
	jg	.LBB6_19
# BB#14:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_13 Depth=1
	movq	(%r8,%rcx,8), %rbx
	decq	%rdi
	.p2align	4, 0x90
.LBB6_15:                               # %.lr.ph
                                        #   Parent Loop BB6_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%r15,%rdi,4), %edx
	notl	%edx
	testl	4(%rbx,%rdi,4), %edx
	jne	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_15 Depth=2
	incq	%rdi
	cmpq	%rsi, %rdi
	jl	.LBB6_15
	jmp	.LBB6_19
	.p2align	4, 0x90
.LBB6_17:                               #   in Loop: Header=BB6_13 Depth=1
	testl	%ebp, %ebp
	jg	.LBB6_21
# BB#18:                                #   in Loop: Header=BB6_13 Depth=1
	incl	%ebp
	movl	%ecx, %eax
.LBB6_19:                               # %.loopexit
                                        #   in Loop: Header=BB6_13 Depth=1
	incq	%rcx
	cmpq	%r11, %rcx
	jl	.LBB6_13
	jmp	.LBB6_22
.LBB6_21:
	movl	$-1, %eax
.LBB6_22:                               # %.loopexit76
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	cactive, .Lfunc_end6-cactive
	.cfi_endproc

	.globl	ccommon
	.p2align	4, 0x90
	.type	ccommon,@function
ccommon:                                # @ccommon
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movslq	cube+108(%rip), %r8
	cmpq	$-1, %r8
	je	.LBB7_6
# BB#1:
	movl	(%rdx,%r8,4), %eax
	movl	(%rdi,%r8,4), %ecx
	orl	%eax, %ecx
	orl	(%rsi,%r8,4), %eax
	movl	%ecx, %ebx
	shrl	%ebx
	andl	%ecx, %ebx
	movl	%eax, %ecx
	shrl	%ecx
	andl	%eax, %ecx
	orl	%ebx, %ecx
	notl	%ecx
	movl	$1, %eax
	testl	cube+104(%rip), %ecx
	jne	.LBB7_16
# BB#2:                                 # %.preheader
	cmpl	$2, %r8d
	jl	.LBB7_6
# BB#3:                                 # %.lr.ph87.preheader
	movl	$1, %r11d
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph87
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx,%r11,4), %r10d
	movl	(%rdi,%r11,4), %r9d
	orl	%r10d, %r9d
	orl	(%rsi,%r11,4), %r10d
	movl	%r9d, %ecx
	shrl	%ecx
	andl	%r9d, %ecx
	movl	%r10d, %ebx
	shrl	%ebx
	andl	%r10d, %ebx
	orl	%ecx, %ebx
	andl	$1431655765, %ebx       # imm = 0x55555555
	cmpl	$1431655765, %ebx       # imm = 0x55555555
	jne	.LBB7_16
# BB#4:                                 #   in Loop: Header=BB7_5 Depth=1
	incq	%r11
	cmpq	%r8, %r11
	jl	.LBB7_5
.LBB7_6:                                # %.loopexit79
	movslq	cube+8(%rip), %r14
	movslq	cube+4(%rip), %r8
	xorl	%eax, %eax
	cmpl	%r8d, %r14d
	jge	.LBB7_16
# BB#7:                                 # %.lr.ph85
	movq	cube+72(%rip), %r9
	movq	cube+48(%rip), %r10
	movq	cube+40(%rip), %r11
	.p2align	4, 0x90
.LBB7_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_11 Depth 2
                                        #     Child Loop BB7_13 Depth 2
	movslq	(%r10,%r14,4), %r15
	movslq	(%r11,%r14,4), %r13
	cmpl	%r15d, %r13d
	jg	.LBB7_15
# BB#9:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_8 Depth=1
	movq	(%r9,%r14,8), %r12
	decq	%r13
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph
                                        #   Parent Loop BB7_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rdi,%rcx,4), %ebp
	notl	%ebp
	andl	4(%r12,%rcx,4), %ebp
	movl	4(%rdx,%rcx,4), %ebx
	notl	%ebx
	testl	%ebx, %ebp
	jne	.LBB7_13
# BB#10:                                #   in Loop: Header=BB7_11 Depth=2
	incq	%rcx
	cmpq	%r15, %rcx
	jl	.LBB7_11
	jmp	.LBB7_15
	.p2align	4, 0x90
.LBB7_13:                               # %.lr.ph83
                                        #   Parent Loop BB7_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%rsi,%r13,4), %ecx
	notl	%ecx
	andl	4(%r12,%r13,4), %ecx
	movl	4(%rdx,%r13,4), %ebx
	notl	%ebx
	testl	%ebx, %ecx
	jne	.LBB7_14
# BB#12:                                #   in Loop: Header=BB7_13 Depth=2
	incq	%r13
	cmpq	%r15, %r13
	jl	.LBB7_13
	.p2align	4, 0x90
.LBB7_15:                               # %.loopexit
                                        #   in Loop: Header=BB7_8 Depth=1
	incq	%r14
	cmpq	%r8, %r14
	jl	.LBB7_8
	jmp	.LBB7_16
.LBB7_14:
	movl	$1, %eax
.LBB7_16:                               # %.critedge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	ccommon, .Lfunc_end7-ccommon
	.cfi_endproc

	.globl	descend
	.p2align	4, 0x90
	.type	descend,@function
descend:                                # @descend
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r8
	movq	(%rsi), %rdx
	movl	(%r8), %esi
	movl	%esi, %edi
	shrl	$16, %edi
	movl	(%rdx), %ecx
	shrl	$16, %ecx
	movl	$-1, %eax
	cmpl	%ecx, %edi
	jbe	.LBB8_1
.LBB8_9:                                # %.thread
	retq
.LBB8_1:
	jae	.LBB8_3
# BB#2:
	movl	$1, %eax
	retq
.LBB8_3:
	andl	$1023, %esi             # imm = 0x3FF
	incq	%rsi
	.p2align	4, 0x90
.LBB8_4:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rsi,4), %ecx
	cmpl	%ecx, -4(%r8,%rsi,4)
	ja	.LBB8_9
# BB#5:                                 #   in Loop: Header=BB8_4 Depth=1
	jb	.LBB8_6
# BB#7:                                 #   in Loop: Header=BB8_4 Depth=1
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB8_4
# BB#8:
	xorl	%eax, %eax
	retq
.LBB8_6:
	movl	$1, %eax
	retq
.Lfunc_end8:
	.size	descend, .Lfunc_end8-descend
	.cfi_endproc

	.globl	ascend
	.p2align	4, 0x90
	.type	ascend,@function
ascend:                                 # @ascend
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r8
	movq	(%rsi), %rdx
	movl	(%r8), %esi
	movl	%esi, %edi
	shrl	$16, %edi
	movl	(%rdx), %ecx
	shrl	$16, %ecx
	movl	$1, %eax
	cmpl	%ecx, %edi
	jbe	.LBB9_1
.LBB9_9:                                # %.thread
	retq
.LBB9_1:
	jae	.LBB9_3
# BB#2:
	movl	$-1, %eax
	retq
.LBB9_3:
	andl	$1023, %esi             # imm = 0x3FF
	incq	%rsi
	.p2align	4, 0x90
.LBB9_4:                                # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rsi,4), %ecx
	cmpl	%ecx, -4(%r8,%rsi,4)
	ja	.LBB9_9
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=1
	jb	.LBB9_6
# BB#7:                                 #   in Loop: Header=BB9_4 Depth=1
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB9_4
# BB#8:
	xorl	%eax, %eax
	retq
.LBB9_6:
	movl	$-1, %eax
	retq
.Lfunc_end9:
	.size	ascend, .Lfunc_end9-ascend
	.cfi_endproc

	.globl	lex_order
	.p2align	4, 0x90
	.type	lex_order,@function
lex_order:                              # @lex_order
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	(%rsi), %rcx
	movl	(%rax), %edx
	andl	$1023, %edx             # imm = 0x3FF
	incq	%rdx
	.p2align	4, 0x90
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx,%rdx,4), %esi
	cmpl	%esi, -4(%rax,%rdx,4)
	ja	.LBB10_2
# BB#3:                                 #   in Loop: Header=BB10_1 Depth=1
	jb	.LBB10_4
# BB#5:                                 #   in Loop: Header=BB10_1 Depth=1
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB10_1
# BB#6:
	xorl	%eax, %eax
	retq
.LBB10_2:
	movl	$-1, %eax
	retq
.LBB10_4:
	movl	$1, %eax
	retq
.Lfunc_end10:
	.size	lex_order, .Lfunc_end10-lex_order
	.cfi_endproc

	.globl	d1_order
	.p2align	4, 0x90
	.type	d1_order,@function
d1_order:                               # @d1_order
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	(%rsi), %r8
	movq	cube+80(%rip), %rdx
	movq	(%rdx), %rdx
	movl	(%rax), %esi
	andl	$1023, %esi             # imm = 0x3FF
	incq	%rsi
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rdx,%rsi,4), %edi
	movl	-4(%rax,%rsi,4), %ecx
	orl	%edi, %ecx
	orl	-4(%r8,%rsi,4), %edi
	cmpl	%edi, %ecx
	ja	.LBB11_2
# BB#3:                                 #   in Loop: Header=BB11_1 Depth=1
	jb	.LBB11_4
# BB#5:                                 #   in Loop: Header=BB11_1 Depth=1
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB11_1
# BB#6:
	xorl	%eax, %eax
	retq
.LBB11_2:
	movl	$-1, %eax
	retq
.LBB11_4:
	movl	$1, %eax
	retq
.Lfunc_end11:
	.size	d1_order, .Lfunc_end11-d1_order
	.cfi_endproc

	.globl	desc1
	.p2align	4, 0x90
	.type	desc1,@function
desc1:                                  # @desc1
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB12_1
# BB#2:
	movl	$-1, %eax
	testq	%rsi, %rsi
	je	.LBB12_12
# BB#3:
	movl	(%rdi), %ecx
	movl	%ecx, %r8d
	shrl	$16, %r8d
	movl	(%rsi), %edx
	shrl	$16, %edx
	cmpl	%edx, %r8d
	jbe	.LBB12_4
.LBB12_12:                              # %.thread
	retq
.LBB12_1:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	setne	%al
	retq
.LBB12_4:
	jae	.LBB12_6
# BB#5:
	movl	$1, %eax
	retq
.LBB12_6:
	andl	$1023, %ecx             # imm = 0x3FF
	incq	%rcx
	.p2align	4, 0x90
.LBB12_7:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi,%rcx,4), %edx
	cmpl	%edx, -4(%rdi,%rcx,4)
	ja	.LBB12_12
# BB#8:                                 #   in Loop: Header=BB12_7 Depth=1
	jb	.LBB12_9
# BB#10:                                #   in Loop: Header=BB12_7 Depth=1
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB12_7
# BB#11:
	xorl	%eax, %eax
	retq
.LBB12_9:
	movl	$1, %eax
	retq
.Lfunc_end12:
	.size	desc1, .Lfunc_end12-desc1
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
