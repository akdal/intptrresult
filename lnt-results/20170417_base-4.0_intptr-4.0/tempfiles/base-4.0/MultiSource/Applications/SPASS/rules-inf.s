	.text
	.file	"rules-inf.bc"
	.globl	inf_EqualityResolution
	.p2align	4, 0x90
	.type	inf_EqualityResolution,@function
inf_EqualityResolution:                 # @inf_EqualityResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movl	%esi, %r13d
	movq	%rdi, %rbp
	cmpl	$0, 68(%rbp)
	je	.LBB0_3
# BB#1:
	movq	%rbp, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB0_3
# BB#2:
	movslq	64(%rbp), %rbx
	movl	68(%rbp), %eax
	leal	-1(%rbx,%rax), %eax
	cmpl	%eax, %ebx
	jle	.LBB0_5
.LBB0_3:
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB0_4:                                # %.loopexit
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:                                # %.lr.ph118
	movslq	%eax, %rsi
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%r15, 56(%rsp)          # 8-byte Spill
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movl	%r13d, 36(%rsp)         # 4-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_23 Depth 2
                                        #     Child Loop BB0_32 Depth 2
	movq	56(%rbp), %rax
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	(%rax,%rbx,8), %r14
	movq	24(%r14), %rbx
	movl	(%rbx), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %edx
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movq	16(%rbx), %rdx
	movq	8(%rdx), %rdx
	movl	(%rdx), %edx
.LBB0_8:                                # %clause_LiteralIsEquality.exit
                                        #   in Loop: Header=BB0_6 Depth=1
	cmpl	%edx, fol_EQUALITY(%rip)
	jne	.LBB0_14
# BB#9:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	(%r14), %edx
	testb	$4, %dl
	jne	.LBB0_15
# BB#10:                                #   in Loop: Header=BB0_6 Depth=1
	testb	$2, 48(%rbp)
	jne	.LBB0_28
# BB#11:                                #   in Loop: Header=BB0_6 Depth=1
	testl	%r13d, %r13d
	je	.LBB0_15
# BB#12:                                #   in Loop: Header=BB0_6 Depth=1
	andl	$1, %edx
	jne	.LBB0_15
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_6 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_6 Depth=1
	cmpl	%eax, %ecx
	jne	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_6 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB0_17:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB0_6 Depth=1
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	8(%rcx), %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB0_29
# BB#18:                                #   in Loop: Header=BB0_6 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	24(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	testl	%r13d, %r13d
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB0_21
# BB#19:                                #   in Loop: Header=BB0_6 Depth=1
	movl	(%r14), %eax
	andl	$4, %eax
	jne	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_6 Depth=1
	movq	24(%rsp), %rcx
	movq	%r15, (%rsp)
	movl	$-1, %edx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r12, %r9
	callq	inf_LitMax
	testl	%eax, %eax
	je	.LBB0_27
.LBB0_21:                               #   in Loop: Header=BB0_6 Depth=1
	movl	64(%rbp), %eax
	movl	72(%rbp), %ecx
	addl	68(%rbp), %eax
	leal	-1(%rcx,%rax), %edi
	callq	clause_CreateBody
	movq	%rax, %r14
	movl	64(%rbp), %eax
	movl	%eax, 64(%r14)
	movl	68(%rbp), %eax
	decl	%eax
	movl	%eax, 68(%r14)
	movl	72(%rbp), %eax
	movl	%eax, 72(%r14)
	movl	68(%rbp), %ebx
	addl	64(%rbp), %ebx
	addl	72(%rbp), %ebx
	movl	%ebx, %eax
	decl	%eax
	js	.LBB0_26
# BB#22:                                # %.lr.ph
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	8(%rsp), %r12d          # 4-byte Reload
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_23:                               #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r13, %r12
	je	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_23 Depth=2
	movq	24(%rsp), %r15
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	clause_LiteralCreate
	movq	56(%r14), %rcx
	movslq	%ebp, %rbp
	movq	%rax, (%rcx,%rbp,8)
	incl	%ebp
.LBB0_25:                               #   in Loop: Header=BB0_23 Depth=2
	incq	%r13
	cmpq	%r13, %rbx
	jne	.LBB0_23
.LBB0_26:                               # %._crit_edge
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	%r14, %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, %edx
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r15, %r8
	callq	clause_SetDataFromFather
	movl	$3, 76(%r14)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	36(%rsp), %r13d         # 4-byte Reload
.LBB0_27:                               #   in Loop: Header=BB0_6 Depth=1
	movq	24(%rsp), %rdi
	callq	subst_Delete
	jmp	.LBB0_30
.LBB0_28:                               #   in Loop: Header=BB0_6 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB0_34
.LBB0_29:                               #   in Loop: Header=BB0_6 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB0_30:                               #   in Loop: Header=BB0_6 Depth=1
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	movq	64(%rsp), %rsi          # 8-byte Reload
	xorps	%xmm0, %xmm0
	je	.LBB0_33
# BB#31:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph.i
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB0_32
.LBB0_33:                               # %cont_Reset.exit
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB0_34:                               #   in Loop: Header=BB0_6 Depth=1
	cmpq	%rsi, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB0_6
	jmp	.LBB0_4
.Lfunc_end0:
	.size	inf_EqualityResolution, .Lfunc_end0-inf_EqualityResolution
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_LitMax,@function
inf_LitMax:                             # @inf_LitMax
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 96
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%edx, %ebp
	movl	%esi, %ebx
	movq	%rdi, %r13
	movq	56(%r13), %rax
	movslq	%ebx, %r15
	movq	(%rax,%r15,8), %rsi
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testb	$1, %dl
	je	.LBB1_23
# BB#1:
	testl	%r14d, %r14d
	je	.LBB1_3
# BB#2:
	andl	$2, %edx
	je	.LBB1_23
.LBB1_3:
	testq	%rcx, %rcx
	movl	$1, %eax
	je	.LBB1_23
# BB#4:
	movl	68(%r13), %edi
	addl	72(%r13), %edi
	cmpl	$1, %edi
	je	.LBB1_23
# BB#5:
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movl	64(%r13), %edx
	leal	-1(%rdx,%rdi), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	24(%rsi), %rdi
	movq	%rcx, %r12
	callq	term_Copy
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	64(%r13), %rax
	cmpl	%ecx, %eax
	jg	.LBB1_22
# BB#6:                                 # %.lr.ph
	testl	%r14d, %r14d
	movslq	%ecx, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, %r12
	je	.LBB1_7
# BB#13:                                # %.lr.ph.split.us.preheader
	decq	%r12
	subl	%eax, %ebp
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	%ebx, %ebx
	je	.LBB1_19
# BB#15:                                # %.lr.ph.split.us
                                        #   in Loop: Header=BB1_14 Depth=1
	testl	%ebp, %ebp
	je	.LBB1_19
# BB#16:                                #   in Loop: Header=BB1_14 Depth=1
	movq	56(%r13), %rax
	movq	8(%rax,%r12,8), %rax
	testb	$1, (%rax)
	je	.LBB1_19
# BB#17:                                #   in Loop: Header=BB1_14 Depth=1
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r14
	movq	56(%r13), %rax
	movq	(%rax,%r15,8), %rcx
	movl	8(%rcx), %esi
	movq	8(%rax,%r12,8), %rax
	movl	8(%rax), %ecx
	movq	96(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$1, %r8d
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	callq	ord_LiteralCompare
	decl	%eax
	cmpl	$2, %eax
	jb	.LBB1_12
# BB#18:                                #   in Loop: Header=BB1_14 Depth=1
	movq	%r14, %rdi
	callq	term_Delete
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB1_19:                               #   in Loop: Header=BB1_14 Depth=1
	incq	%r12
	decl	%ebp
	decl	%ebx
	cmpq	%rcx, %r12
	jl	.LBB1_14
	jmp	.LBB1_22
.LBB1_7:                                # %.lr.ph.split.preheader
	decq	%r12
	subl	%eax, %ebp
	subl	%eax, %ebx
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	testl	%ebx, %ebx
	je	.LBB1_21
# BB#9:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB1_8 Depth=1
	testl	%ebp, %ebp
	je	.LBB1_21
# BB#10:                                #   in Loop: Header=BB1_8 Depth=1
	movq	56(%r13), %rax
	movq	8(%rax,%r12,8), %rax
	testb	$1, (%rax)
	je	.LBB1_21
# BB#11:                                #   in Loop: Header=BB1_8 Depth=1
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r14
	movq	56(%r13), %rax
	movq	(%rax,%r15,8), %rcx
	movl	8(%rcx), %esi
	movq	8(%rax,%r12,8), %rax
	movl	8(%rax), %ecx
	movq	96(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$1, %r8d
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %rdx
	movq	32(%rsp), %r9           # 8-byte Reload
	callq	ord_LiteralCompare
	cmpl	$1, %eax
	je	.LBB1_12
# BB#20:                                #   in Loop: Header=BB1_8 Depth=1
	movq	%r14, %rdi
	callq	term_Delete
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB1_21:                               #   in Loop: Header=BB1_8 Depth=1
	incq	%r12
	decl	%ebp
	decl	%ebx
	cmpq	%rcx, %r12
	jl	.LBB1_8
.LBB1_22:                               # %._crit_edge
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movl	$1, %eax
.LBB1_23:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_12:                               # %.us-lcssa.us
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movq	%r14, %rdi
	callq	term_Delete
	xorl	%eax, %eax
	jmp	.LBB1_23
.Lfunc_end1:
	.size	inf_LitMax, .Lfunc_end1-inf_LitMax
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_SetDataFromFather,@function
clause_SetDataFromFather:               # @clause_SetDataFromFather
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -48
.Lcfi32:
	.cfi_offset %r12, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %rbp
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	clause_OrientEqualities
	movq	%rbx, %rdi
	callq	clause_Normalize
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	clause_SetMaxLitFlags
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbx)
	movq	%rbx, %rdi
	callq	clause_UpdateMaxVar
	testb	$8, 48(%r15)
	je	.LBB2_2
# BB#1:
	orb	$8, 48(%rbx)
.LBB2_2:
	movl	12(%r15), %eax
	movl	%eax, 12(%rbx)
	movq	16(%r15), %rbp
	movl	24(%r15), %r12d
	movl	24(%rbx), %ecx
	cmpl	%r12d, %ecx
	je	.LBB2_15
# BB#3:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_11
# BB#4:
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB2_5
# BB#10:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	testl	%r12d, %r12d
	jne	.LBB2_13
	jmp	.LBB2_12
.LBB2_5:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_7
# BB#6:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB2_7:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_9
# BB#8:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_9:
	addq	$-16, %rdi
	callq	free
.LBB2_11:                               # %memory_Free.exit.i.i
	testl	%r12d, %r12d
	je	.LBB2_12
.LBB2_13:
	leal	(,%r12,8), %edi
	callq	memory_Malloc
	jmp	.LBB2_14
.LBB2_12:
	xorl	%eax, %eax
.LBB2_14:
	movq	%rax, 16(%rbx)
	movl	%r12d, 24(%rbx)
.LBB2_15:                               # %.preheader.i.i
	testl	%r12d, %r12d
	je	.LBB2_20
# BB#16:                                # %.lr.ph.i.i
	leaq	-1(%r12), %rcx
	movq	%r12, %rdx
	xorl	%eax, %eax
	andq	$3, %rdx
	je	.LBB2_18
	.p2align	4, 0x90
.LBB2_17:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rax,8), %rsi
	movq	16(%rbx), %rdi
	movq	%rsi, (%rdi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB2_17
.LBB2_18:                               # %.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB2_20
	.p2align	4, 0x90
.LBB2_19:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rax,8), %rcx
	movq	16(%rbx), %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	8(%rbp,%rax,8), %rcx
	movq	16(%rbx), %rdx
	movq	%rcx, 8(%rdx,%rax,8)
	movq	16(%rbp,%rax,8), %rcx
	movq	16(%rbx), %rdx
	movq	%rcx, 16(%rdx,%rax,8)
	movq	24(%rbp,%rax,8), %rcx
	movq	16(%rbx), %rdx
	movq	%rcx, 24(%rdx,%rax,8)
	addq	$4, %rax
	cmpq	%rax, %r12
	jne	.LBB2_19
.LBB2_20:                               # %clause_SetSplitDataFromFather.exit
	movl	8(%r15), %eax
	incl	%eax
	movl	%eax, 8(%rbx)
	movslq	(%r15), %rbp
	movq	32(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 32(%rbx)
	movslq	%r14d, %rbp
	movq	40(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	clause_SetDataFromFather, .Lfunc_end2-clause_SetDataFromFather
	.cfi_endproc

	.globl	inf_EqualityFactoring
	.p2align	4, 0x90
	.type	inf_EqualityFactoring,@function
inf_EqualityFactoring:                  # @inf_EqualityFactoring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 160
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	cmpl	$0, 72(%r15)
	je	.LBB3_1
# BB#2:
	testb	$2, 48(%r15)
	jne	.LBB3_1
# BB#3:
	movq	%r15, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB3_1
# BB#4:
	movl	68(%r15), %eax
	movl	72(%r15), %ecx
	addl	64(%r15), %eax
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	jle	.LBB3_5
.LBB3_1:
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB3_53:                               # %.loopexit179
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_5:                                # %.lr.ph185
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movslq	%ecx, %rdi
	movslq	%eax, %r12
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r14, 72(%rsp)          # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_14 Depth 2
                                        #       Child Loop BB3_26 Depth 3
                                        #       Child Loop BB3_33 Depth 3
                                        #       Child Loop BB3_42 Depth 3
                                        #       Child Loop BB3_49 Depth 3
	movq	56(%r15), %rax
	movq	(%rax,%r12,8), %rbp
	testb	$1, (%rbp)
	je	.LBB3_52
# BB#7:                                 #   in Loop: Header=BB3_6 Depth=1
	movq	24(%rbp), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %edx
	cmpl	%ecx, %edx
	movl	%ecx, %esi
	jne	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_6 Depth=1
	movq	16(%rax), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB3_9:                                # %clause_LiteralIsEquality.exit
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpl	%esi, fol_EQUALITY(%rip)
	jne	.LBB3_52
# BB#10:                                #   in Loop: Header=BB3_6 Depth=1
	cmpl	%ecx, %edx
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	jne	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_6 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB3_12:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB3_6 Depth=1
	movslq	68(%r15), %rcx
	movslq	64(%r15), %rbp
	addq	%rcx, %rbp
	cmpl	92(%rsp), %ebp          # 4-byte Folded Reload
	jg	.LBB3_52
# BB#13:                                # %.lr.ph
                                        #   in Loop: Header=BB3_6 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %r13
	movq	8(%rcx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_14:                               #   Parent Loop BB3_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_26 Depth 3
                                        #       Child Loop BB3_33 Depth 3
                                        #       Child Loop BB3_42 Depth 3
                                        #       Child Loop BB3_49 Depth 3
	cmpl	%ebp, %r12d
	je	.LBB3_51
# BB#15:                                #   in Loop: Header=BB3_14 Depth=2
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %edx
	cmpl	%ecx, %edx
	movl	%ecx, %esi
	jne	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_14 Depth=2
	movq	16(%rax), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB3_17:                               # %clause_LiteralIsEquality.exit174
                                        #   in Loop: Header=BB3_14 Depth=2
	cmpl	%esi, fol_EQUALITY(%rip)
	jne	.LBB3_51
# BB#18:                                #   in Loop: Header=BB3_14 Depth=2
	cmpl	%ecx, %edx
	jne	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_14 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB3_20:                               # %clause_LiteralAtom.exit164
                                        #   in Loop: Header=BB3_14 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	8(%rcx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r13, %rsi
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB3_24
# BB#21:                                #   in Loop: Header=BB3_14 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	8(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	movq	8(%rsp), %r8
	subq	$8, %rsp
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdi
	movl	%r12d, %esi
	movq	%r13, %rdx
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	%r13, %r9
	pushq	%r14
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	callq	inf_EqualityFactoringApplicable
	addq	$16, %rsp
.Lcfi51:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB3_23
# BB#22:                                #   in Loop: Header=BB3_14 Depth=2
	movq	8(%rsp), %r9
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	%r12d, %ecx
	movl	%ebp, %r8d
	pushq	%r14
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyEqualityFactoring
	addq	$16, %rsp
.Lcfi54:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB3_23:                               #   in Loop: Header=BB3_14 Depth=2
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	8(%rsp), %rdi
	callq	subst_Delete
.LBB3_24:                               #   in Loop: Header=BB3_14 Depth=2
	xorps	%xmm0, %xmm0
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_27
# BB#25:                                # %.lr.ph.preheader.i152
                                        #   in Loop: Header=BB3_14 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB3_26:                               # %.lr.ph.i155
                                        #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB3_26
.LBB3_27:                               # %cont_Reset.exit156
                                        #   in Loop: Header=BB3_14 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r13, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB3_31
# BB#28:                                #   in Loop: Header=BB3_14 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	8(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	movq	8(%rsp), %r8
	subq	$8, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdi
	movl	%r12d, %esi
	movq	%r13, %rdx
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	%r14, %r13
	movq	%r15, %r14
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r15, %r9
	pushq	%r13
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	callq	inf_EqualityFactoringApplicable
	addq	$16, %rsp
.Lcfi57:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB3_30
# BB#29:                                #   in Loop: Header=BB3_14 Depth=2
	movq	8(%rsp), %r9
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	%r12d, %ecx
	movl	%ebp, %r8d
	pushq	%r13
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyEqualityFactoring
	addq	$16, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB3_30:                               #   in Loop: Header=BB3_14 Depth=2
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	%r14, %r15
	movq	8(%rsp), %rdi
	callq	subst_Delete
.LBB3_31:                               #   in Loop: Header=BB3_14 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	xorps	%xmm0, %xmm0
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_34
# BB#32:                                # %.lr.ph.preheader.i146
                                        #   in Loop: Header=BB3_14 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB3_33:                               # %.lr.ph.i149
                                        #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB3_33
.LBB3_34:                               # %cont_Reset.exit150
                                        #   in Loop: Header=BB3_14 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	96(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 8(%rax)
	movq	72(%rsp), %r14          # 8-byte Reload
	jne	.LBB3_51
# BB#35:                                #   in Loop: Header=BB3_14 Depth=2
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB3_36
# BB#37:                                #   in Loop: Header=BB3_14 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	8(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	movq	8(%rsp), %r8
	subq	$8, %rsp
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r15, %r9
	pushq	%r14
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	callq	inf_EqualityFactoringApplicable
	addq	$16, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_14 Depth=2
	movq	8(%rsp), %r9
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	%r12d, %ecx
	movl	%ebp, %r8d
	pushq	%r14
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyEqualityFactoring
	addq	$16, %rsp
.Lcfi66:
	.cfi_adjust_cfa_offset -16
	movq	%rax, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB3_39:                               #   in Loop: Header=BB3_14 Depth=2
	movq	8(%rsp), %rdi
	callq	subst_Delete
	xorps	%xmm0, %xmm0
	movq	24(%rsp), %r13          # 8-byte Reload
	jmp	.LBB3_40
.LBB3_36:                               #   in Loop: Header=BB3_14 Depth=2
	xorps	%xmm0, %xmm0
.LBB3_40:                               #   in Loop: Header=BB3_14 Depth=2
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_43
# BB#41:                                # %.lr.ph.preheader.i140
                                        #   in Loop: Header=BB3_14 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB3_42:                               # %.lr.ph.i143
                                        #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB3_42
.LBB3_43:                               # %cont_Reset.exit144
                                        #   in Loop: Header=BB3_14 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB3_47
# BB#44:                                #   in Loop: Header=BB3_14 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	8(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	movq	8(%rsp), %r8
	subq	$8, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdi
	movl	%r12d, %esi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r9
	pushq	%r14
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	callq	inf_EqualityFactoringApplicable
	addq	$16, %rsp
.Lcfi69:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB3_46
# BB#45:                                #   in Loop: Header=BB3_14 Depth=2
	movq	8(%rsp), %r9
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	%r12d, %ecx
	movl	%ebp, %r8d
	pushq	%r14
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyEqualityFactoring
	addq	$16, %rsp
.Lcfi72:
	.cfi_adjust_cfa_offset -16
	movq	%r14, %rbx
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, %r14
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB3_46:                               #   in Loop: Header=BB3_14 Depth=2
	movq	8(%rsp), %rdi
	callq	subst_Delete
.LBB3_47:                               #   in Loop: Header=BB3_14 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	xorps	%xmm0, %xmm0
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB3_50
# BB#48:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB3_14 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB3_49:                               # %.lr.ph.i
                                        #   Parent Loop BB3_6 Depth=1
                                        #     Parent Loop BB3_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB3_49
.LBB3_50:                               # %cont_Reset.exit
                                        #   in Loop: Header=BB3_14 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	.p2align	4, 0x90
.LBB3_51:                               #   in Loop: Header=BB3_14 Depth=2
	cmpq	%rdi, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB3_14
	.p2align	4, 0x90
.LBB3_52:                               # %.loopexit
                                        #   in Loop: Header=BB3_6 Depth=1
	cmpq	%rdi, %r12
	leaq	1(%r12), %r12
	jl	.LBB3_6
	jmp	.LBB3_53
.Lfunc_end3:
	.size	inf_EqualityFactoring, .Lfunc_end3-inf_EqualityFactoring
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_EqualityFactoringApplicable,@function
inf_EqualityFactoringApplicable:        # @inf_EqualityFactoringApplicable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 80
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rcx, %r12
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	56(%rbx), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rax
	cmpl	$0, 8(%rax)
	jne	.LBB4_3
# BB#1:
	movq	80(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rdi
	callq	term_Copy
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r13
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%r12, %rdi
	callq	term_Copy
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r12
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	ord_Compare
	movl	%eax, %r13d
	decl	%r13d
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	term_Delete
	movq	%r12, %rdi
	callq	term_Delete
	cmpl	$2, %r13d
	jae	.LBB4_3
# BB#2:                                 # %.critedge
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_3:
	movl	$-1, %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rcx
	movq	%r14, %r9
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	inf_LitMax              # TAILCALL
.Lfunc_end4:
	.size	inf_EqualityFactoringApplicable, .Lfunc_end4-inf_EqualityFactoringApplicable
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_ApplyEqualityFactoring,@function
inf_ApplyEqualityFactoring:             # @inf_ApplyEqualityFactoring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 112
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movl	%r8d, 32(%rsp)          # 4-byte Spill
	movl	%ecx, %r13d
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	68(%rbx), %edi
	addl	64(%rbx), %edi
	addl	72(%rbx), %edi
	callq	clause_CreateBody
	movq	%rax, %rbp
	movl	64(%rbx), %r14d
	movl	%r14d, 64(%rbp)
	movl	64(%rbx), %eax
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %edx
	incl	%ecx
	movl	%ecx, 68(%rbp)
	movl	64(%rbx), %ecx
	movl	72(%rbx), %esi
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	addl	68(%rbx), %ecx
	movl	%esi, %eax
	decl	%eax
	movl	%eax, 72(%rbp)
	testl	%r14d, %r14d
	jle	.LBB5_1
# BB#11:                                # %.lr.ph100
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movl	%edx, 4(%rsp)           # 4-byte Spill
	xorl	%ebx, %ebx
	movq	%r12, %r15
	movq	24(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_12:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	clause_LiteralCreate
	movq	56(%rbp), %rcx
	movq	%rax, (%rcx,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %r14
	jne	.LBB5_12
# BB#13:
	movq	%r15, %r12
	movl	4(%rsp), %edx           # 4-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB5_2
.LBB5_1:
	xorl	%r14d, %r14d
.LBB5_2:                                # %.preheader
	leal	-1(%rsi,%rcx), %eax
	cmpl	%edx, %r14d
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jg	.LBB5_5
# BB#3:                                 # %.lr.ph96
	movslq	%r14d, %r14
	movslq	%edx, %r15
	movq	24(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_4:                                # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	clause_LiteralCreate
	movq	56(%rbp), %rcx
	movq	%rax, (%rcx,%r14,8)
	cmpq	%r15, %r14
	leaq	1(%r14), %r14
	jl	.LBB5_4
.LBB5_5:                                # %._crit_edge97.loopexit
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	fol_EQUALITY(%rip), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	callq	term_Copy
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	$0, (%r15)
	movl	$16, %edi
	callq	memory_Malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%r15, (%rax)
	movl	8(%rsp), %edi           # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	movl	fol_NOT(%rip), %r15d
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%r15d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	clause_LiteralCreate
	movq	56(%rbp), %rcx
	movslq	%r14d, %rbx
	movq	%rax, (%rcx,%rbx,8)
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	%eax, %ebx
	jg	.LBB5_10
# BB#6:                                 # %.lr.ph
	movslq	%eax, %r15
	decq	%rbx
	movl	$1, %r12d
	movl	%r13d, 36(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB5_7:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	cmpl	%r14d, %r13d
	je	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_7 Depth=1
	leal	(%r12,%r14), %r13d
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	clause_LiteralCreate
	movq	56(%rbp), %rcx
	movslq	%r13d, %rdx
	movl	36(%rsp), %r13d         # 4-byte Reload
	movq	%rax, (%rcx,%rdx,8)
	movl	%r12d, %eax
.LBB5_9:                                #   in Loop: Header=BB5_7 Depth=1
	incq	%rbx
	incl	%r14d
	cmpq	%r15, %rbx
	movl	%eax, %r12d
	jl	.LBB5_7
.LBB5_10:                               # %._crit_edge
	movq	24(%rsp), %r15          # 8-byte Reload
	movslq	(%r15), %rbx
	movq	32(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%rbp)
	movslq	32(%rsp), %rbx          # 4-byte Folded Reload
	movq	40(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbp)
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movl	%r13d, %edx
	movq	112(%rsp), %rcx
	movq	120(%rsp), %r8
	callq	clause_SetDataFromFather
	movl	$4, 76(%rbp)
	movq	%rbp, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	inf_ApplyEqualityFactoring, .Lfunc_end5-inf_ApplyEqualityFactoring
	.cfi_endproc

	.globl	inf_GenSuperpositionRight
	.p2align	4, 0x90
	.type	inf_GenSuperpositionRight,@function
inf_GenSuperpositionRight:              # @inf_GenSuperpositionRight
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi103:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi105:
	.cfi_def_cfa_offset 240
.Lcfi106:
	.cfi_offset %rbx, -56
.Lcfi107:
	.cfi_offset %r12, -48
.Lcfi108:
	.cfi_offset %r13, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movq	%r9, 40(%rsp)           # 8-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 80(%rsp)           # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	testb	$2, 48(%r12)
	jne	.LBB6_6
# BB#1:
	cmpl	$0, 72(%r12)
	je	.LBB6_6
# BB#2:
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB6_6
# BB#3:
	movq	%r12, %rdi
	callq	clause_Copy
	movq	%rax, %rdi
	movl	68(%rdi), %eax
	movl	72(%rdi), %ecx
	addl	64(%rdi), %eax
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	jle	.LBB6_7
# BB#4:
	xorl	%r13d, %r13d
	jmp	.LBB6_102
.LBB6_6:
	xorl	%r13d, %r13d
.LBB6_103:
	movq	%r13, %rax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_7:                                # %.lr.ph
	movslq	%eax, %rbx
	movslq	%ecx, %r14
	xorl	%r15d, %r15d
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_17 Depth 2
                                        #     Child Loop BB6_26 Depth 2
                                        #     Child Loop BB6_40 Depth 2
                                        #       Child Loop BB6_41 Depth 3
                                        #       Child Loop BB6_45 Depth 3
                                        #         Child Loop BB6_47 Depth 4
                                        #           Child Loop BB6_50 Depth 5
                                        #             Child Loop BB6_51 Depth 6
                                        #             Child Loop BB6_65 Depth 6
                                        #     Child Loop BB6_90 Depth 2
                                        #     Child Loop BB6_99 Depth 2
	testl	%r10d, %r10d
	movq	56(%rdi), %rcx
	movq	(%rcx,%rbx,8), %rbp
	movq	24(%rbp), %r13
	je	.LBB6_11
# BB#9:                                 #   in Loop: Header=BB6_8 Depth=1
	testb	$2, (%rbp)
	jne	.LBB6_11
# BB#10:                                #   in Loop: Header=BB6_8 Depth=1
	movq	%r15, %r13
	jmp	.LBB6_101
	.p2align	4, 0x90
.LBB6_11:                               #   in Loop: Header=BB6_8 Depth=1
	movl	fol_EQUALITY(%rip), %ecx
	cmpl	(%r13), %ecx
	jne	.LBB6_31
# BB#12:                                #   in Loop: Header=BB6_8 Depth=1
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	je	.LBB6_14
# BB#13:                                #   in Loop: Header=BB6_8 Depth=1
	movl	68(%r12), %ecx
	addl	64(%r12), %ecx
	addl	72(%r12), %ecx
	cmpl	$1, %ecx
	jne	.LBB6_31
.LBB6_14:                               #   in Loop: Header=BB6_8 Depth=1
	movq	16(%r13), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	8(%rcx), %rdx
	subq	$8, %rsp
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	movl	%ebx, %ecx
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	248(%rsp)
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenLitSPRight
	addq	$32, %rsp
.Lcfi116:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB6_19
# BB#15:                                #   in Loop: Header=BB6_8 Depth=1
	testq	%r15, %r15
	je	.LBB6_20
# BB#16:                                # %.preheader.i85.preheader
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader.i85
                                        #   Parent Loop BB6_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_17
# BB#18:                                #   in Loop: Header=BB6_8 Depth=1
	movq	%r15, (%rax)
	jmp	.LBB6_20
.LBB6_19:                               #   in Loop: Header=BB6_8 Depth=1
	movq	%r15, %r14
.LBB6_20:                               # %list_Nconc.exit87
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB6_23
# BB#21:                                #   in Loop: Header=BB6_8 Depth=1
	cmpl	$0, 8(%rbp)
	je	.LBB6_23
# BB#22:                                #   in Loop: Header=BB6_8 Depth=1
	movq	%r14, %r15
	jmp	.LBB6_30
.LBB6_23:                               #   in Loop: Header=BB6_8 Depth=1
	movq	16(%r13), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	8(%rcx), %rsi
	subq	$8, %rsp
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	movl	%ebx, %ecx
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	248(%rsp)
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenLitSPRight
	addq	$32, %rsp
.Lcfi121:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB6_28
# BB#24:                                #   in Loop: Header=BB6_8 Depth=1
	testq	%r14, %r14
	je	.LBB6_29
# BB#25:                                # %.preheader.i91.preheader
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB6_26:                               # %.preheader.i91
                                        #   Parent Loop BB6_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_26
# BB#27:                                #   in Loop: Header=BB6_8 Depth=1
	movq	%r14, (%rdx)
	jmp	.LBB6_29
.LBB6_28:                               #   in Loop: Header=BB6_8 Depth=1
	movq	%r14, %r15
.LBB6_29:                               # %list_Nconc.exit93
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB6_30:                               # %list_Nconc.exit93
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
.LBB6_31:                               # %list_Nconc.exit93
                                        #   in Loop: Header=BB6_8 Depth=1
	testb	$32, 48(%rdi)
	jne	.LBB6_38
# BB#32:                                #   in Loop: Header=BB6_8 Depth=1
	movq	56(%rdi), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	24(%rcx), %rdx
	movl	(%rdx), %ecx
	cmpl	%ecx, fol_NOT(%rip)
	jne	.LBB6_34
# BB#33:                                #   in Loop: Header=BB6_8 Depth=1
	movq	16(%rdx), %rcx
	movq	8(%rcx), %rdx
	movl	(%rdx), %ecx
.LBB6_34:                               # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	%r15, 56(%rsp)          # 8-byte Spill
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB6_39
# BB#35:                                # %list_Nconc.exit41.i
                                        #   in Loop: Header=BB6_8 Depth=1
	subq	$8, %rsp
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r10d, %r9d
	pushq	248(%rsp)
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)               # 8-byte Folded Reload
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenSPRightEqToGiven
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	addq	$32, %rsp
.Lcfi126:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r15
	testl	%r10d, %r10d
	je	.LBB6_87
# BB#36:                                #   in Loop: Header=BB6_8 Depth=1
	movq	56(%rdi), %rax
	movq	(%rax,%rbx,8), %rax
	cmpl	$0, 8(%rax)
	je	.LBB6_87
# BB#37:                                #   in Loop: Header=BB6_8 Depth=1
	movq	%r15, %r13
	jmp	.LBB6_94
	.p2align	4, 0x90
.LBB6_38:                               #   in Loop: Header=BB6_8 Depth=1
	movq	%r15, %r13
	jmp	.LBB6_101
.LBB6_39:                               #   in Loop: Header=BB6_8 Depth=1
	movl	stack_POINTER(%rip), %ebp
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	16(%rdx), %rdi
	callq	sharing_PushListOnStack
	xorl	%r13d, %r13d
.LBB6_40:                               # %.outer.i.i
                                        #   Parent Loop BB6_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_41 Depth 3
                                        #       Child Loop BB6_45 Depth 3
                                        #         Child Loop BB6_47 Depth 4
                                        #           Child Loop BB6_50 Depth 5
                                        #             Child Loop BB6_51 Depth 6
                                        #             Child Loop BB6_65 Depth 6
	movl	stack_POINTER(%rip), %eax
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_41:                               #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%eax, %ebp
	je	.LBB6_86
# BB#42:                                #   in Loop: Header=BB6_41 Depth=3
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %r14
	cmpl	$0, (%r14)
	jg	.LBB6_41
# BB#43:                                #   in Loop: Header=BB6_40 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%r14, %rcx
	callq	st_GetUnifier
	testq	%rax, %rax
	je	.LBB6_40
# BB#44:                                # %.lr.ph149.i.i.preheader
                                        #   in Loop: Header=BB6_40 Depth=2
	movq	%r12, 152(%rsp)         # 8-byte Spill
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movl	%ebp, 140(%rsp)         # 4-byte Spill
	movq	%r14, 160(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB6_45:                               # %.lr.ph149.i.i
                                        #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_47 Depth 4
                                        #           Child Loop BB6_50 Depth 5
                                        #             Child Loop BB6_51 Depth 6
                                        #             Child Loop BB6_65 Depth 6
	movq	%r13, 120(%rsp)         # 8-byte Spill
	movq	8(%rax), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	8(%rcx), %r13
	testq	%r13, %r13
	je	.LBB6_85
# BB#46:                                # %.lr.ph144.i.i.preheader
                                        #   in Loop: Header=BB6_45 Depth=3
	movq	%rax, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_47:                               # %.lr.ph144.i.i
                                        #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_45 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB6_50 Depth 5
                                        #             Child Loop BB6_51 Depth 6
                                        #             Child Loop BB6_65 Depth 6
	movq	8(%r13), %rdi
	movl	fol_EQUALITY(%rip), %ecx
	cmpl	(%rdi), %ecx
	jne	.LBB6_84
# BB#48:                                #   in Loop: Header=BB6_47 Depth=4
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB6_83
# BB#49:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB6_47 Depth=4
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB6_50:                               #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_45 Depth=3
                                        #         Parent Loop BB6_47 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB6_51 Depth 6
                                        #             Child Loop BB6_65 Depth 6
	movq	8(%rbp), %rbx
	movq	16(%rbx), %r12
	movq	56(%r12), %rcx
	movl	$-1, %r15d
	.p2align	4, 0x90
.LBB6_51:                               #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_45 Depth=3
                                        #         Parent Loop BB6_47 Depth=4
                                        #           Parent Loop BB6_50 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	incl	%r15d
	cmpq	%rbx, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB6_51
# BB#52:                                # %clause_LiteralGetIndex.exit.i.i
                                        #   in Loop: Header=BB6_50 Depth=5
	testb	$2, 48(%r12)
	jne	.LBB6_82
# BB#53:                                #   in Loop: Header=BB6_50 Depth=5
	testl	%edx, %edx
	je	.LBB6_55
# BB#54:                                #   in Loop: Header=BB6_50 Depth=5
	testb	$2, (%rbx)
	je	.LBB6_82
.LBB6_55:                               #   in Loop: Header=BB6_50 Depth=5
	testl	%esi, %esi
	je	.LBB6_58
# BB#56:                                #   in Loop: Header=BB6_50 Depth=5
	movq	16(%r9), %rcx
	movq	128(%rsp), %rdi         # 8-byte Reload
	cmpq	8(%rcx), %rdi
	je	.LBB6_58
# BB#57:                                #   in Loop: Header=BB6_50 Depth=5
	cmpl	$0, 8(%rbx)
	jne	.LBB6_82
	.p2align	4, 0x90
.LBB6_58:                               #   in Loop: Header=BB6_50 Depth=5
	movq	24(%rbx), %rdi
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rdi), %ecx
	je	.LBB6_82
# BB#59:                                #   in Loop: Header=BB6_50 Depth=5
	movl	(%r12), %ecx
	cmpl	(%r8), %ecx
	je	.LBB6_82
# BB#60:                                #   in Loop: Header=BB6_50 Depth=5
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	je	.LBB6_62
# BB#61:                                #   in Loop: Header=BB6_50 Depth=5
	movl	68(%r12), %ecx
	addl	64(%r12), %ecx
	addl	72(%r12), %ecx
	cmpl	$1, %ecx
	jne	.LBB6_82
.LBB6_62:                               #   in Loop: Header=BB6_50 Depth=5
	movq	%r12, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB6_81
# BB#63:                                #   in Loop: Header=BB6_50 Depth=5
	movl	52(%r12), %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%r14, %rsi
	movq	128(%rsp), %rcx         # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	104(%rsp), %rsi
	leaq	72(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB6_66
# BB#64:                                # %.lr.ph.preheader.i.i.i
                                        #   in Loop: Header=BB6_50 Depth=5
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB6_65:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB6_8 Depth=1
                                        #     Parent Loop BB6_40 Depth=2
                                        #       Parent Loop BB6_45 Depth=3
                                        #         Parent Loop BB6_47 Depth=4
                                        #           Parent Loop BB6_50 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB6_65
.LBB6_66:                               # %cont_Reset.exit.i.i
                                        #   in Loop: Header=BB6_50 Depth=5
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB6_71
# BB#67:                                #   in Loop: Header=BB6_50 Depth=5
	movq	72(%rsp), %r9
	movq	8(%rsp), %rdx           # 8-byte Reload
	testb	$2, 48(%rdx)
	jne	.LBB6_69
# BB#68:                                #   in Loop: Header=BB6_50 Depth=5
	movq	104(%rsp), %rcx
	movl	64(%rdx), %eax
	movl	68(%rdx), %edx
	leal	-1(%rax,%rdx), %eax
	cltq
	xorl	%r8d, %r8d
	movq	112(%rsp), %rsi         # 8-byte Reload
	cmpq	%rsi, %rax
	setl	%r8b
	subq	$8, %rsp
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	16(%rsp), %rdi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	48(%rsp), %r9           # 8-byte Reload
	pushq	248(%rsp)
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	movq	48(%rsp), %r9           # 8-byte Reload
	addq	$16, %rsp
.Lcfi129:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB6_80
.LBB6_69:                               #   in Loop: Header=BB6_50 Depth=5
	testb	$2, 48(%r12)
	jne	.LBB6_71
# BB#70:                                #   in Loop: Header=BB6_50 Depth=5
	movl	64(%r12), %eax
	movl	68(%r12), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	cmpl	%r15d, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi130:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	%r12, %rdi
	movl	%r15d, %esi
	movq	%r9, %rcx
	movq	48(%rsp), %r9           # 8-byte Reload
	pushq	248(%rsp)
.Lcfi131:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	addq	$16, %rsp
.Lcfi132:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB6_80
.LBB6_71:                               # %inf_LiteralsMax.exit.i.i
                                        #   in Loop: Header=BB6_50 Depth=5
	movq	72(%rsp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	128(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	callq	term_Copy
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	movq	72(%rsp), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	cmpq	%rdi, %r14
	jne	.LBB6_73
# BB#72:                                #   in Loop: Header=BB6_50 Depth=5
	movq	(%rax), %rax
	movq	8(%rax), %rdi
.LBB6_73:                               #   in Loop: Header=BB6_50 Depth=5
	callq	term_Copy
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 32(%rsp)          # 8-byte Spill
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	160(%rsp), %r14         # 8-byte Reload
	je	.LBB6_76
# BB#74:                                #   in Loop: Header=BB6_50 Depth=5
	cmpl	$0, 8(%rbx)
	jne	.LBB6_76
# BB#75:                                #   in Loop: Header=BB6_50 Depth=5
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	240(%rsp), %rcx
	callq	ord_Compare
	cmpl	$1, %eax
	je	.LBB6_79
.LBB6_76:                               #   in Loop: Header=BB6_50 Depth=5
	movq	104(%rsp), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rdi         # 8-byte Reload
	callq	term_Copy
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	jne	.LBB6_78
# BB#77:                                #   in Loop: Header=BB6_50 Depth=5
	movq	%rbx, %rdi
	callq	term_Delete
	xorl	%ebx, %ebx
.LBB6_78:                               # %inf_AllTermsRplac.exit.i.i
                                        #   in Loop: Header=BB6_50 Depth=5
	movq	72(%rsp), %rdx
	movq	104(%rsp), %r9
	movq	%r12, %rdi
	movl	%r15d, %esi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	112(%rsp), %r8          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	pushq	240(%rsp)
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi134:
	.cfi_adjust_cfa_offset 8
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi135:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi136:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi137:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi138:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyGenSuperposition
	addq	$48, %rsp
.Lcfi139:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 120(%rsp)         # 8-byte Spill
.LBB6_79:                               #   in Loop: Header=BB6_50 Depth=5
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	term_Delete
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
.LBB6_80:                               # %inf_LiteralsMax.exit.thread.i.i
                                        #   in Loop: Header=BB6_50 Depth=5
	movq	104(%rsp), %rdi
	callq	subst_Delete
	movq	72(%rsp), %rdi
	callq	subst_Delete
.LBB6_81:                               #   in Loop: Header=BB6_50 Depth=5
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB6_82:                               #   in Loop: Header=BB6_50 Depth=5
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB6_50
	jmp	.LBB6_84
.LBB6_83:                               #   in Loop: Header=BB6_47 Depth=4
	movq	88(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_84:                               # %.loopexit.i.i
                                        #   in Loop: Header=BB6_47 Depth=4
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB6_47
.LBB6_85:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB6_45 Depth=3
	movq	(%rax), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rax
	movq	152(%rsp), %r12         # 8-byte Reload
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	120(%rsp), %r13         # 8-byte Reload
	movl	140(%rsp), %ebp         # 4-byte Reload
	jne	.LBB6_45
	jmp	.LBB6_40
.LBB6_86:                               #   in Loop: Header=BB6_8 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
	testq	%r13, %r13
	jne	.LBB6_97
	jmp	.LBB6_95
.LBB6_87:                               #   in Loop: Header=BB6_8 Depth=1
	subq	$8, %rsp
.Lcfi140:
	.cfi_adjust_cfa_offset 8
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r10d, %r9d
	pushq	248(%rsp)
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)               # 8-byte Folded Reload
.Lcfi143:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenSPRightEqToGiven
	addq	$32, %rsp
.Lcfi144:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB6_92
# BB#88:                                #   in Loop: Header=BB6_8 Depth=1
	testq	%r15, %r15
	je	.LBB6_96
# BB#89:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB6_90:                               # %.preheader.i.i
                                        #   Parent Loop BB6_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_90
# BB#91:                                #   in Loop: Header=BB6_8 Depth=1
	movq	%r15, (%rax)
	jmp	.LBB6_93
.LBB6_92:                               #   in Loop: Header=BB6_8 Depth=1
	movq	%r15, %r13
.LBB6_93:                               # %inf_GenSPRightToGiven.exit
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB6_94:                               # %inf_GenSPRightToGiven.exit
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	testq	%r13, %r13
	jne	.LBB6_97
.LBB6_95:                               #   in Loop: Header=BB6_8 Depth=1
	movq	%rdx, %r13
	jmp	.LBB6_101
.LBB6_96:                               #   in Loop: Header=BB6_8 Depth=1
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_97:                               # %inf_GenSPRightToGiven.exit.thread
                                        #   in Loop: Header=BB6_8 Depth=1
	testq	%rdx, %rdx
	je	.LBB6_101
# BB#98:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB6_8 Depth=1
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB6_99:                               # %.preheader.i
                                        #   Parent Loop BB6_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB6_99
# BB#100:                               #   in Loop: Header=BB6_8 Depth=1
	movq	%rdx, (%rax)
	.p2align	4, 0x90
.LBB6_101:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB6_8 Depth=1
	cmpq	%r14, %rbx
	leaq	1(%rbx), %rbx
	movq	%r13, %r15
	jl	.LBB6_8
.LBB6_102:                              # %._crit_edge
	callq	clause_Delete
	jmp	.LBB6_103
.Lfunc_end6:
	.size	inf_GenSuperpositionRight, .Lfunc_end6-inf_GenSuperpositionRight
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_GenLitSPRight,@function
inf_GenLitSPRight:                      # @inf_GenLitSPRight
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi151:
	.cfi_def_cfa_offset 224
.Lcfi152:
	.cfi_offset %rbx, -56
.Lcfi153:
	.cfi_offset %r12, -48
.Lcfi154:
	.cfi_offset %r13, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movl	%ecx, %ebp
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	%rsi, %rax
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r8, 144(%rsp)          # 8-byte Spill
	movq	(%r8), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	callq	st_GetUnifier
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB7_65
# BB#1:                                 # %.lr.ph185
	movslq	%ebp, %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	testl	%ebx, %ebx
	sete	15(%rsp)                # 1-byte Folded Spill
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%ebp, 92(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB7_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_7 Depth 2
                                        #       Child Loop BB7_10 Depth 3
                                        #       Child Loop BB7_20 Depth 3
	movq	8(%r14), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.LBB7_64
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	jns	.LBB7_5
# BB#4:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB7_2 Depth=1
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	je	.LBB7_64
.LBB7_5:                                # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rsi         # 8-byte Reload
	callq	sharing_GetDataList
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB7_64
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	%r14, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_7:                                # %.lr.ph
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_10 Depth 3
                                        #       Child Loop BB7_20 Depth 3
	movq	8(%r12), %rbx
	movq	24(%rbx), %r15
	movl	(%r15), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	jne	.LBB7_9
# BB#8:                                 #   in Loop: Header=BB7_7 Depth=2
	movq	16(%r15), %rdx
	movq	8(%rdx), %r15
.LBB7_9:                                # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB7_7 Depth=2
	movq	16(%rbx), %r13
	movq	56(%r13), %rdx
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB7_10:                               #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ebp
	cmpq	%rbx, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB7_10
# BB#11:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB7_7 Depth=2
	movl	48(%r13), %edx
	testb	$2, %dl
	jne	.LBB7_63
# BB#12:                                #   in Loop: Header=BB7_7 Depth=2
	movl	224(%rsp), %esi
	testl	%esi, %esi
	je	.LBB7_15
# BB#13:                                #   in Loop: Header=BB7_7 Depth=2
	testb	$32, %dl
	jne	.LBB7_63
# BB#14:                                #   in Loop: Header=BB7_7 Depth=2
	movl	(%rbx), %edx
	andl	$2, %edx
	jne	.LBB7_16
	jmp	.LBB7_63
.LBB7_15:                               #   in Loop: Header=BB7_7 Depth=2
	testb	$32, %dl
	jne	.LBB7_63
.LBB7_16:                               #   in Loop: Header=BB7_7 Depth=2
	cmpl	%eax, %ecx
	je	.LBB7_63
# BB#17:                                #   in Loop: Header=BB7_7 Depth=2
	movq	%r13, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB7_63
# BB#18:                                #   in Loop: Header=BB7_7 Depth=2
	movl	52(%r13), %esi
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	80(%rsp), %rsi
	leaq	32(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB7_21
# BB#19:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB7_7 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB7_20:                               # %.lr.ph.i
                                        #   Parent Loop BB7_2 Depth=1
                                        #     Parent Loop BB7_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB7_20
.LBB7_21:                               # %cont_Reset.exit
                                        #   in Loop: Header=BB7_7 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movl	224(%rsp), %eax
	testl	%eax, %eax
	je	.LBB7_26
# BB#22:                                #   in Loop: Header=BB7_7 Depth=2
	movq	32(%rsp), %r14
	movq	72(%rsp), %rax          # 8-byte Reload
	testb	$2, 48(%rax)
	jne	.LBB7_24
# BB#23:                                #   in Loop: Header=BB7_7 Depth=2
	movq	80(%rsp), %rcx
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	64(%rdi), %eax
	movl	68(%rdi), %edx
	leal	-1(%rax,%rdx), %eax
	xorl	%r8d, %r8d
	movl	92(%rsp), %esi          # 4-byte Reload
	cmpl	%esi, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi158:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	240(%rsp), %r9
	pushq	248(%rsp)
.Lcfi159:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	addq	$16, %rsp
.Lcfi160:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB7_43
.LBB7_24:                               #   in Loop: Header=BB7_7 Depth=2
	testb	$2, 48(%r13)
	jne	.LBB7_26
# BB#25:                                #   in Loop: Header=BB7_7 Depth=2
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	cmpl	%ebp, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi161:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	%r13, %rdi
	movl	%ebp, %esi
	movq	%r14, %rcx
	movq	240(%rsp), %r9
	pushq	248(%rsp)
.Lcfi162:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	addq	$16, %rsp
.Lcfi163:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB7_43
.LBB7_26:                               # %inf_LiteralsMax.exit
                                        #   in Loop: Header=BB7_7 Depth=2
	movq	80(%rsp), %r14
	movq	160(%rsp), %rdi         # 8-byte Reload
	callq	term_Copy
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movb	$1, %sil
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	je	.LBB7_31
# BB#27:                                #   in Loop: Header=BB7_7 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	cmpl	$0, 8(%rax)
	movq	112(%rsp), %rdx         # 8-byte Reload
	jne	.LBB7_32
# BB#28:                                #   in Loop: Header=BB7_7 Depth=2
	movq	80(%rsp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	112(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdi
	callq	term_Copy
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdx
	cmpq	%r14, %rdx
	sete	%sil
	movl	%esi, %eax
	orb	15(%rsp), %al           # 1-byte Folded Reload
	jne	.LBB7_32
# BB#29:                                #   in Loop: Header=BB7_7 Depth=2
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movq	%rdx, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	232(%rsp), %rdx
	movq	240(%rsp), %rcx
	callq	ord_Compare
	cmpl	$1, %eax
	jne	.LBB7_48
# BB#30:                                #   in Loop: Header=BB7_7 Depth=2
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	jmp	.LBB7_60
.LBB7_31:                               #   in Loop: Header=BB7_7 Depth=2
	movq	112(%rsp), %rdx         # 8-byte Reload
.LBB7_32:                               # %.thread
                                        #   in Loop: Header=BB7_7 Depth=2
	movl	224(%rsp), %eax
	testl	%eax, %eax
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movl	%esi, 64(%rsp)          # 4-byte Spill
	je	.LBB7_39
# BB#33:                                #   in Loop: Header=BB7_7 Depth=2
	movq	24(%rbx), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB7_35
# BB#34:                                #   in Loop: Header=BB7_7 Depth=2
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB7_35:                               # %clause_LiteralIsPredicate.exit
                                        #   in Loop: Header=BB7_7 Depth=2
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB7_39
# BB#36:                                #   in Loop: Header=BB7_7 Depth=2
	cmpl	$0, 8(%rbx)
	movq	32(%rsp), %r14
	je	.LBB7_44
# BB#37:                                #   in Loop: Header=BB7_7 Depth=2
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r14, 128(%rsp)         # 8-byte Spill
	movq	%r14, %rcx
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB7_47
# BB#38:                                #   in Loop: Header=BB7_7 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	128(%rsp), %rcx         # 8-byte Reload
	callq	inf_NAllTermsRplac
	jmp	.LBB7_57
.LBB7_39:                               #   in Loop: Header=BB7_7 Depth=2
	movq	32(%rsp), %r14
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	je	.LBB7_41
# BB#40:                                #   in Loop: Header=BB7_7 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	testq	%rbx, %rbx
	jne	.LBB7_58
	jmp	.LBB7_59
.LBB7_43:                               #   in Loop: Header=BB7_7 Depth=2
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB7_62
.LBB7_41:                               #   in Loop: Header=BB7_7 Depth=2
	movq	%rbx, %rdi
	callq	term_Delete
	movq	16(%rsp), %r14          # 8-byte Reload
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	je	.LBB7_60
	jmp	.LBB7_61
.LBB7_44:                               #   in Loop: Header=BB7_7 Depth=2
	movq	16(%r15), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movq	32(%rsp), %r14
	movq	16(%r15), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	232(%rsp), %rdx
	movq	240(%rsp), %rcx
	callq	ord_Compare
	cmpl	$3, %eax
	movq	16(%rsp), %r14          # 8-byte Reload
	je	.LBB7_49
# BB#45:                                #   in Loop: Header=BB7_7 Depth=2
	cmpl	$1, %eax
	jne	.LBB7_52
# BB#46:                                #   in Loop: Header=BB7_7 Depth=2
	movq	32(%rsp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rdi
	jmp	.LBB7_50
.LBB7_47:                               #   in Loop: Header=BB7_7 Depth=2
	movq	%rbx, %rdi
	callq	term_Delete
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	je	.LBB7_60
	jmp	.LBB7_61
.LBB7_48:                               #   in Loop: Header=BB7_7 Depth=2
	xorl	%esi, %esi
	movq	136(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB7_32
.LBB7_49:                               #   in Loop: Header=BB7_7 Depth=2
	movq	32(%rsp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
.LBB7_50:                               #   in Loop: Header=BB7_7 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	je	.LBB7_55
# BB#51:                                #   in Loop: Header=BB7_7 Depth=2
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	callq	inf_NAllTermsRplac
	jmp	.LBB7_56
.LBB7_52:                               #   in Loop: Header=BB7_7 Depth=2
	movq	32(%rsp), %r14
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	jne	.LBB7_54
# BB#53:                                #   in Loop: Header=BB7_7 Depth=2
	movq	%rbx, %rdi
	callq	term_Delete
	xorl	%ebx, %ebx
.LBB7_54:                               # %inf_AllTermsRightRplac.exit
                                        #   in Loop: Header=BB7_7 Depth=2
	movq	16(%rsp), %r14          # 8-byte Reload
	jmp	.LBB7_56
.LBB7_55:                               #   in Loop: Header=BB7_7 Depth=2
	movq	%rbx, %rdi
	callq	term_Delete
	xorl	%ebx, %ebx
.LBB7_56:                               # %inf_AllTermsRightRplac.exit
                                        #   in Loop: Header=BB7_7 Depth=2
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	term_Delete
.LBB7_57:                               # %inf_AllTermsRplac.exit163
                                        #   in Loop: Header=BB7_7 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	testq	%rbx, %rbx
	je	.LBB7_59
.LBB7_58:                               #   in Loop: Header=BB7_7 Depth=2
	movq	80(%rsp), %rdx
	movq	32(%rsp), %r9
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	92(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rcx
	movl	%ebp, %r8d
	pushq	240(%rsp)
.Lcfi164:
	.cfi_adjust_cfa_offset 8
	pushq	240(%rsp)
.Lcfi165:
	.cfi_adjust_cfa_offset 8
	movl	240(%rsp), %ebp
	pushq	%rbp
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi168:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi169:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyGenSuperposition
	addq	$48, %rsp
.Lcfi170:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	movq	%rax, 96(%rsp)          # 8-byte Spill
	jne	.LBB7_61
	jmp	.LBB7_60
.LBB7_59:                               #   in Loop: Header=BB7_7 Depth=2
	cmpb	$0, 64(%rsp)            # 1-byte Folded Reload
	jne	.LBB7_61
.LBB7_60:                               # %.thread187
                                        #   in Loop: Header=BB7_7 Depth=2
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	term_Delete
.LBB7_61:                               #   in Loop: Header=BB7_7 Depth=2
	movq	%r15, %rdi
	callq	term_Delete
.LBB7_62:                               # %inf_LiteralsMax.exit.thread
                                        #   in Loop: Header=BB7_7 Depth=2
	movq	80(%rsp), %rdi
	callq	subst_Delete
	movq	32(%rsp), %rdi
	callq	subst_Delete
	.p2align	4, 0x90
.LBB7_63:                               #   in Loop: Header=BB7_7 Depth=2
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB7_7
	.p2align	4, 0x90
.LBB7_64:                               # %.loopexit
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB7_2
	jmp	.LBB7_66
.LBB7_65:
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
.LBB7_66:                               # %._crit_edge
	movq	96(%rsp), %rax          # 8-byte Reload
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	inf_GenLitSPRight, .Lfunc_end7-inf_GenLitSPRight
	.cfi_endproc

	.globl	inf_MergingParamodulation
	.p2align	4, 0x90
	.type	inf_MergingParamodulation,@function
inf_MergingParamodulation:              # @inf_MergingParamodulation
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi175:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi177:
	.cfi_def_cfa_offset 80
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, %rbx
	testb	$2, 48(%rbx)
	jne	.LBB8_6
# BB#1:
	cmpl	$0, 72(%rbx)
	je	.LBB8_6
# BB#2:
	movq	%rbx, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB8_6
# BB#3:
	movq	%rbx, %rdi
	callq	clause_Copy
	movq	%rax, %r13
	movl	68(%r13), %eax
	movl	72(%r13), %ecx
	addl	64(%r13), %eax
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_7
# BB#4:
	xorl	%ebp, %ebp
	jmp	.LBB8_36
.LBB8_6:
	xorl	%ebp, %ebp
.LBB8_37:
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_7:                                # %.lr.ph
	movslq	%eax, %rbx
	movslq	%ecx, %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_13 Depth 2
                                        #     Child Loop BB8_19 Depth 2
                                        #     Child Loop BB8_26 Depth 2
                                        #     Child Loop BB8_32 Depth 2
	movq	56(%r13), %rax
	movq	(%rax,%rbx,8), %r12
	testb	$2, (%r12)
	je	.LBB8_35
# BB#9:                                 #   in Loop: Header=BB8_8 Depth=1
	movq	24(%r12), %rax
	movl	fol_EQUALITY(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB8_35
# BB#10:                                #   in Loop: Header=BB8_8 Depth=1
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	inf_LitMParamod
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB8_15
# BB#11:                                #   in Loop: Header=BB8_8 Depth=1
	testq	%rbp, %rbp
	je	.LBB8_16
# BB#12:                                # %.preheader.i80.preheader
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB8_13:                               # %.preheader.i80
                                        #   Parent Loop BB8_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB8_13
# BB#14:                                #   in Loop: Header=BB8_8 Depth=1
	movq	%rbp, (%rax)
	jmp	.LBB8_16
.LBB8_15:                               #   in Loop: Header=BB8_8 Depth=1
	movq	%rbp, %r14
.LBB8_16:                               # %list_Nconc.exit82
                                        #   in Loop: Header=BB8_8 Depth=1
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	inf_MParamodLitToGiven
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB8_21
# BB#17:                                #   in Loop: Header=BB8_8 Depth=1
	testq	%r14, %r14
	je	.LBB8_22
# BB#18:                                # %.preheader.i74.preheader
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB8_19:                               # %.preheader.i74
                                        #   Parent Loop BB8_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB8_19
# BB#20:                                #   in Loop: Header=BB8_8 Depth=1
	movq	%r14, (%rax)
	cmpl	$0, 8(%r12)
	jne	.LBB8_35
	jmp	.LBB8_23
.LBB8_21:                               #   in Loop: Header=BB8_8 Depth=1
	movq	%r14, %rbp
.LBB8_22:                               # %list_Nconc.exit76
                                        #   in Loop: Header=BB8_8 Depth=1
	cmpl	$0, 8(%r12)
	jne	.LBB8_35
.LBB8_23:                               #   in Loop: Header=BB8_8 Depth=1
	movl	$1, %edx
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	inf_LitMParamod
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB8_28
# BB#24:                                #   in Loop: Header=BB8_8 Depth=1
	testq	%rbp, %rbp
	je	.LBB8_29
# BB#25:                                # %.preheader.i68.preheader
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB8_26:                               # %.preheader.i68
                                        #   Parent Loop BB8_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB8_26
# BB#27:                                #   in Loop: Header=BB8_8 Depth=1
	movq	%rbp, (%rax)
	jmp	.LBB8_29
.LBB8_28:                               #   in Loop: Header=BB8_8 Depth=1
	movq	%rbp, %r14
.LBB8_29:                               # %list_Nconc.exit70
                                        #   in Loop: Header=BB8_8 Depth=1
	movl	$1, %edx
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	inf_MParamodLitToGiven
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB8_34
# BB#30:                                #   in Loop: Header=BB8_8 Depth=1
	testq	%r14, %r14
	je	.LBB8_35
# BB#31:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB8_32:                               # %.preheader.i
                                        #   Parent Loop BB8_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB8_32
# BB#33:                                #   in Loop: Header=BB8_8 Depth=1
	movq	%r14, (%rax)
	jmp	.LBB8_35
.LBB8_34:                               #   in Loop: Header=BB8_8 Depth=1
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB8_35:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB8_8 Depth=1
	cmpq	%r15, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB8_8
.LBB8_36:                               # %._crit_edge
	movq	%r13, %rdi
	callq	clause_Delete
	jmp	.LBB8_37
.Lfunc_end8:
	.size	inf_MergingParamodulation, .Lfunc_end8-inf_MergingParamodulation
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_LitMParamod,@function
inf_LitMParamod:                        # @inf_LitMParamod
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi187:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi188:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi190:
	.cfi_def_cfa_offset 176
.Lcfi191:
	.cfi_offset %rbx, -56
.Lcfi192:
	.cfi_offset %r12, -48
.Lcfi193:
	.cfi_offset %r13, -40
.Lcfi194:
	.cfi_offset %r14, -32
.Lcfi195:
	.cfi_offset %r15, -24
.Lcfi196:
	.cfi_offset %rbp, -16
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	56(%rdi), %rax
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	8(%rsi), %rbp
	testl	%edx, %edx
	movq	%rax, %rdx
	cmoveq	%rbp, %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	cmoveq	%rax, %rbp
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	(%rcx), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rbp, %rcx
	callq	st_GetUnifier
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.LBB9_44
# BB#1:                                 # %.lr.ph169
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_7 Depth 2
                                        #       Child Loop BB9_10 Depth 3
                                        #       Child Loop BB9_21 Depth 3
                                        #       Child Loop BB9_29 Depth 3
                                        #       Child Loop BB9_38 Depth 3
	movq	8(%r15), %rbx
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB9_43
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	jns	.LBB9_5
# BB#4:                                 # %term_IsAtom.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	negl	%eax
	andl	symbol_TYPEMASK(%rip), %eax
	cmpl	$2, %eax
	je	.LBB9_43
.LBB9_5:                                # %term_IsAtom.exit.thread
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rbx, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	callq	sharing_GetDataList
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB9_43
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_7:                                # %.lr.ph
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_10 Depth 3
                                        #       Child Loop BB9_21 Depth 3
                                        #       Child Loop BB9_29 Depth 3
                                        #       Child Loop BB9_38 Depth 3
	movq	8(%r13), %r12
	movq	16(%r12), %rbx
	movq	24(%r12), %rbp
	movl	(%rbp), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	jne	.LBB9_9
# BB#8:                                 #   in Loop: Header=BB9_7 Depth=2
	movq	16(%rbp), %rdx
	movq	8(%rdx), %rbp
.LBB9_9:                                # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB9_7 Depth=2
	movq	56(%rbx), %rdx
	movl	$-1, %r14d
	.p2align	4, 0x90
.LBB9_10:                               #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%r14d
	cmpq	%r12, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB9_10
# BB#11:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB9_7 Depth=2
	testb	$2, 48(%rbx)
	jne	.LBB9_42
# BB#12:                                #   in Loop: Header=BB9_7 Depth=2
	testb	$2, (%r12)
	je	.LBB9_42
# BB#13:                                #   in Loop: Header=BB9_7 Depth=2
	cmpl	%eax, %ecx
	je	.LBB9_42
# BB#14:                                #   in Loop: Header=BB9_7 Depth=2
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB9_42
# BB#15:                                #   in Loop: Header=BB9_7 Depth=2
	cmpl	$2, 72(%rbx)
	jl	.LBB9_42
# BB#16:                                #   in Loop: Header=BB9_7 Depth=2
	movq	%rbx, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB9_42
# BB#17:                                #   in Loop: Header=BB9_7 Depth=2
	movq	16(%rbp), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rdi
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	term_HasPointerSubterm
	testl	%eax, %eax
	jne	.LBB9_19
# BB#18:                                #   in Loop: Header=BB9_7 Depth=2
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	jne	.LBB9_42
.LBB9_19:                               #   in Loop: Header=BB9_7 Depth=2
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	52(%rbx), %esi
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	24(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB9_22
# BB#20:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB9_7 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB9_21:                               # %.lr.ph.i
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB9_21
.LBB9_22:                               # %cont_Reset.exit
                                        #   in Loop: Header=BB9_7 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	24(%rsp), %rbp
	movq	72(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rbp
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbp
	movl	$3, %eax
	cmpl	$0, 8(%r12)
	movq	104(%rsp), %r12         # 8-byte Reload
	jne	.LBB9_24
# BB#23:                                #   in Loop: Header=BB9_7 Depth=2
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	ord_Compare
.LBB9_24:                               #   in Loop: Header=BB9_7 Depth=2
	movl	36(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	je	.LBB9_32
# BB#25:                                #   in Loop: Header=BB9_7 Depth=2
	cmpl	$3, %eax
	jne	.LBB9_32
# BB#26:                                #   in Loop: Header=BB9_7 Depth=2
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movl	32(%rsp), %edx          # 4-byte Reload
	movl	%r14d, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi197:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi198:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)
.Lcfi199:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi200:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi201:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi202:
	.cfi_adjust_cfa_offset 8
	callq	inf_Lit2MParamod
	addq	$48, %rsp
.Lcfi203:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB9_41
# BB#27:                                #   in Loop: Header=BB9_7 Depth=2
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB9_40
# BB#28:                                # %.preheader.i147.preheader
                                        #   in Loop: Header=BB9_7 Depth=2
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB9_29:                               # %.preheader.i147
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB9_29
# BB#30:                                #   in Loop: Header=BB9_7 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	84(%rsp), %eax          # 4-byte Reload
	cmpl	$1, %eax
	je	.LBB9_33
	jmp	.LBB9_40
.LBB9_32:                               #   in Loop: Header=BB9_7 Depth=2
	movq	(%rsp), %rbp            # 8-byte Reload
	cmpl	$1, %eax
	jne	.LBB9_40
.LBB9_33:                               #   in Loop: Header=BB9_7 Depth=2
	testl	%ecx, %ecx
	je	.LBB9_35
# BB#34:                                #   in Loop: Header=BB9_7 Depth=2
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	term_HasPointerSubterm
	testl	%eax, %eax
	je	.LBB9_40
.LBB9_35:                               #   in Loop: Header=BB9_7 Depth=2
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movl	32(%rsp), %edx          # 4-byte Reload
	movl	%r14d, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi204:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi205:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)
.Lcfi206:
	.cfi_adjust_cfa_offset 8
	pushq	32(%rsp)                # 8-byte Folded Reload
.Lcfi207:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)               # 8-byte Folded Reload
.Lcfi208:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi209:
	.cfi_adjust_cfa_offset 8
	callq	inf_Lit2MParamod
	addq	$48, %rsp
.Lcfi210:
	.cfi_adjust_cfa_offset -48
	testq	%rax, %rax
	je	.LBB9_40
# BB#36:                                #   in Loop: Header=BB9_7 Depth=2
	testq	%rbp, %rbp
	movq	%rax, (%rsp)            # 8-byte Spill
	je	.LBB9_41
# BB#37:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB9_7 Depth=2
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB9_38:                               # %.preheader.i
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB9_38
# BB#39:                                #   in Loop: Header=BB9_7 Depth=2
	movq	%rbp, (%rax)
	jmp	.LBB9_41
.LBB9_40:                               #   in Loop: Header=BB9_7 Depth=2
	movq	%rbp, (%rsp)            # 8-byte Spill
.LBB9_41:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB9_7 Depth=2
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%r12, %rdi
	callq	term_Delete
	movq	%rbx, %rdi
	callq	term_Delete
	movq	24(%rsp), %rdi
	callq	subst_Delete
	.p2align	4, 0x90
.LBB9_42:                               #   in Loop: Header=BB9_7 Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB9_7
	.p2align	4, 0x90
.LBB9_43:                               # %.loopexit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB9_2
	jmp	.LBB9_45
.LBB9_44:
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB9_45:                               # %._crit_edge
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	inf_LitMParamod, .Lfunc_end9-inf_LitMParamod
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_MParamodLitToGiven,@function
inf_MParamodLitToGiven:                 # @inf_MParamodLitToGiven
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi211:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi212:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi213:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi214:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi215:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi216:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi217:
	.cfi_def_cfa_offset 176
.Lcfi218:
	.cfi_offset %rbx, -56
.Lcfi219:
	.cfi_offset %r12, -48
.Lcfi220:
	.cfi_offset %r13, -40
.Lcfi221:
	.cfi_offset %r14, -32
.Lcfi222:
	.cfi_offset %r15, -24
.Lcfi223:
	.cfi_offset %rbp, -16
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movl	%esi, %ebx
	cmpl	$2, 72(%rdi)
	jl	.LBB10_2
# BB#1:
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	56(%rdi), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	8(%rcx), %rcx
	testl	%edx, %edx
	movq	%rax, %rdi
	cmoveq	%rcx, %rdi
	cmoveq	%rax, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	stack_POINTER(%rip), %ebp
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	callq	sharing_PushReverseOnStack
	movl	stack_POINTER(%rip), %eax
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	cmpl	%ebp, %eax
	jne	.LBB10_3
.LBB10_2:
	xorl	%ebp, %ebp
	jmp	.LBB10_52
.LBB10_3:                               # %.lr.ph179
	movl	%ebx, 52(%rsp)          # 4-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_5 Depth 2
                                        #       Child Loop BB10_6 Depth 3
                                        #         Child Loop BB10_9 Depth 4
                                        #           Child Loop BB10_10 Depth 5
                                        #           Child Loop BB10_26 Depth 5
                                        #           Child Loop BB10_37 Depth 5
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %rcx
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	callq	st_GetUnifier
	testq	%rax, %rax
	je	.LBB10_50
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph175
                                        #   Parent Loop BB10_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_6 Depth 3
                                        #         Child Loop BB10_9 Depth 4
                                        #           Child Loop BB10_10 Depth 5
                                        #           Child Loop BB10_26 Depth 5
                                        #           Child Loop BB10_37 Depth 5
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rax), %r14
	testq	%r14, %r14
	je	.LBB10_48
	.p2align	4, 0x90
.LBB10_6:                               # %.lr.ph170
                                        #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_5 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_9 Depth 4
                                        #           Child Loop BB10_10 Depth 5
                                        #           Child Loop BB10_26 Depth 5
                                        #           Child Loop BB10_37 Depth 5
	movq	8(%r14), %rbp
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%rbp), %eax
	jne	.LBB10_46
# BB#7:                                 #   in Loop: Header=BB10_6 Depth=3
	movq	%rbp, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB10_46
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB10_6 Depth=3
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	jmp	.LBB10_9
	.p2align	4, 0x90
.LBB10_15:                              #   in Loop: Header=BB10_9 Depth=4
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %edx
	cmpl	(%rcx), %edx
	jne	.LBB10_17
# BB#16:                                #   in Loop: Header=BB10_9 Depth=4
	movq	%rbx, %rbp
	jmp	.LBB10_44
.LBB10_17:                              #   in Loop: Header=BB10_9 Depth=4
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	16(%rbp), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	cmpq	8(%rcx), %rdx
	je	.LBB10_20
# BB#18:                                #   in Loop: Header=BB10_9 Depth=4
	cmpl	$0, 8(%rax)
	je	.LBB10_20
# BB#19:                                #   in Loop: Header=BB10_9 Depth=4
	movq	%rbx, %rbp
	jmp	.LBB10_44
.LBB10_20:                              #   in Loop: Header=BB10_9 Depth=4
	movq	%r12, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB10_23
# BB#21:                                #   in Loop: Header=BB10_9 Depth=4
	movl	(%r12), %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jne	.LBB10_24
.LBB10_23:                              #   in Loop: Header=BB10_9 Depth=4
	movq	%rbx, %rbp
	jmp	.LBB10_44
.LBB10_24:                              #   in Loop: Header=BB10_9 Depth=4
	movl	52(%r12), %esi
	movq	%rcx, %rdi
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	8(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB10_27
# BB#25:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB10_9 Depth=4
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB10_26:                              # %.lr.ph.i
                                        #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_5 Depth=2
                                        #       Parent Loop BB10_6 Depth=3
                                        #         Parent Loop BB10_9 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB10_26
.LBB10_27:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB10_9 Depth=4
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	112(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 8(%rax)
	je	.LBB10_29
# BB#28:                                #   in Loop: Header=BB10_9 Depth=4
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r10d, %r10d
	jmp	.LBB10_30
.LBB10_29:                              #   in Loop: Header=BB10_9 Depth=4
	movq	8(%rsp), %rbp
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rbp
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rbp, %rdi
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rax, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	callq	ord_Compare
	movq	16(%rsp), %r10          # 8-byte Reload
	cmpl	$3, %eax
	jne	.LBB10_41
.LBB10_30:                              # %.thread
                                        #   in Loop: Header=BB10_9 Depth=4
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
	cmpq	%rbp, 24(%rsp)          # 8-byte Folded Reload
	jne	.LBB10_32
# BB#31:                                #   in Loop: Header=BB10_9 Depth=4
	movq	(%rax), %rax
	movq	8(%rax), %rbp
.LBB10_32:                              #   in Loop: Header=BB10_9 Depth=4
	testq	%r10, %r10
	jne	.LBB10_34
# BB#33:                                #   in Loop: Header=BB10_9 Depth=4
	movq	8(%rsp), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB10_34:                              #   in Loop: Header=BB10_9 Depth=4
	movq	%r12, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, %edx
	movl	52(%rsp), %ecx          # 4-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	%rbp, %r9
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi224:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi225:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rsp)
.Lcfi226:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %r12
	pushq	%r10
.Lcfi227:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi228:
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)               # 8-byte Folded Reload
.Lcfi229:
	.cfi_adjust_cfa_offset 8
	callq	inf_Lit2MParamod
	addq	$48, %rsp
.Lcfi230:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB10_39
# BB#35:                                #   in Loop: Header=BB10_9 Depth=4
	testq	%rbx, %rbx
	je	.LBB10_40
# BB#36:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB10_9 Depth=4
	movq	%rbp, %rcx
	movq	%r12, %r10
	.p2align	4, 0x90
.LBB10_37:                              # %.preheader.i
                                        #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_5 Depth=2
                                        #       Parent Loop BB10_6 Depth=3
                                        #         Parent Loop BB10_9 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB10_37
# BB#38:                                #   in Loop: Header=BB10_9 Depth=4
	movq	%rbx, (%rax)
	testq	%r10, %r10
	jne	.LBB10_42
	jmp	.LBB10_43
.LBB10_39:                              #   in Loop: Header=BB10_9 Depth=4
	movq	%rbx, %rbp
.LBB10_40:                              #   in Loop: Header=BB10_9 Depth=4
	movq	%r12, %r10
	testq	%r10, %r10
	jne	.LBB10_42
	jmp	.LBB10_43
.LBB10_41:                              #   in Loop: Header=BB10_9 Depth=4
	movq	%rbx, %rbp
	testq	%r10, %r10
	je	.LBB10_43
.LBB10_42:                              #   in Loop: Header=BB10_9 Depth=4
	movq	%r10, %rdi
	callq	term_Delete
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	term_Delete
.LBB10_43:                              #   in Loop: Header=BB10_9 Depth=4
	movq	8(%rsp), %rdi
	callq	subst_Delete
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	clause_Normalize
	jmp	.LBB10_44
	.p2align	4, 0x90
.LBB10_9:                               #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_5 Depth=2
                                        #       Parent Loop BB10_6 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB10_10 Depth 5
                                        #           Child Loop BB10_26 Depth 5
                                        #           Child Loop BB10_37 Depth 5
	movq	8(%r15), %rax
	movq	16(%rax), %r12
	movq	56(%r12), %rcx
	movl	$-1, %r13d
	.p2align	4, 0x90
.LBB10_10:                              #   Parent Loop BB10_4 Depth=1
                                        #     Parent Loop BB10_5 Depth=2
                                        #       Parent Loop BB10_6 Depth=3
                                        #         Parent Loop BB10_9 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%r13d
	cmpq	%rax, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB10_10
# BB#11:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB10_9 Depth=4
	testb	$2, 48(%r12)
	jne	.LBB10_14
# BB#12:                                #   in Loop: Header=BB10_9 Depth=4
	testb	$2, (%rax)
	jne	.LBB10_15
.LBB10_14:                              #   in Loop: Header=BB10_9 Depth=4
	movq	%rbx, %rbp
.LBB10_44:                              #   in Loop: Header=BB10_9 Depth=4
	movq	(%r15), %r15
	testq	%r15, %r15
	movq	%rbp, %rbx
	jne	.LBB10_9
	jmp	.LBB10_47
	.p2align	4, 0x90
.LBB10_46:                              #   in Loop: Header=BB10_6 Depth=3
	movq	%rbx, %rbp
.LBB10_47:                              # %.loopexit
                                        #   in Loop: Header=BB10_6 Depth=3
	movq	(%r14), %r14
	testq	%r14, %r14
	movq	%rbp, %rbx
	jne	.LBB10_6
	jmp	.LBB10_49
	.p2align	4, 0x90
.LBB10_48:                              #   in Loop: Header=BB10_5 Depth=2
	movq	%rbx, %rbp
.LBB10_49:                              # %._crit_edge
                                        #   in Loop: Header=BB10_5 Depth=2
	movq	104(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rbp, %rbx
	jne	.LBB10_5
	jmp	.LBB10_51
	.p2align	4, 0x90
.LBB10_50:                              #   in Loop: Header=BB10_4 Depth=1
	movq	%rbx, %rbp
.LBB10_51:                              # %.loopexit163
                                        #   in Loop: Header=BB10_4 Depth=1
	movl	stack_POINTER(%rip), %eax
	cmpl	48(%rsp), %eax          # 4-byte Folded Reload
	movq	%rbp, %rbx
	jne	.LBB10_4
.LBB10_52:                              # %.loopexit164
	movq	%rbp, %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	inf_MParamodLitToGiven, .Lfunc_end10-inf_MParamodLitToGiven
	.cfi_endproc

	.globl	inf_GeneralResolution
	.p2align	4, 0x90
	.type	inf_GeneralResolution,@function
inf_GeneralResolution:                  # @inf_GeneralResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi231:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi232:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi233:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi234:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi235:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi236:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi237:
	.cfi_def_cfa_offset 192
.Lcfi238:
	.cfi_offset %rbx, -56
.Lcfi239:
	.cfi_offset %r12, -48
.Lcfi240:
	.cfi_offset %r13, -40
.Lcfi241:
	.cfi_offset %r14, -32
.Lcfi242:
	.cfi_offset %r15, -24
.Lcfi243:
	.cfi_offset %rbp, -16
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movl	%ecx, %ebp
	movl	%edx, 48(%rsp)          # 4-byte Spill
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	%rdi, %r14
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB11_1
# BB#2:
	movq	%r14, %rdi
	callq	clause_Copy
	testb	$2, 48(%rax)
	movslq	64(%rax), %rsi
	movl	68(%rax), %edx
	jne	.LBB11_3
# BB#4:
	leal	-1(%rsi,%rdx), %ecx
	movl	72(%rax), %edx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	jmp	.LBB11_5
.LBB11_1:
	xorl	%eax, %eax
	jmp	.LBB11_8
.LBB11_3:
	leal	-1(%rsi), %ecx
.LBB11_5:
	addl	%ecx, %edx
	cmpl	%edx, %esi
	jle	.LBB11_9
# BB#6:
	xorl	%ecx, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jmp	.LBB11_7
.LBB11_9:                               # %.lr.ph193
	movslq	%edx, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	movq	%r14, 120(%rsp)         # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB11_10:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_21 Depth 2
                                        #     Child Loop BB11_27 Depth 2
                                        #       Child Loop BB11_28 Depth 3
                                        #         Child Loop BB11_31 Depth 4
                                        #           Child Loop BB11_32 Depth 5
                                        #           Child Loop BB11_40 Depth 5
                                        #           Child Loop BB11_55 Depth 5
	movq	56(%rax), %rax
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	(%rax,%rsi,8), %r12
	movq	24(%r12), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB11_11
# BB#12:                                #   in Loop: Header=BB11_10 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jne	.LBB11_16
	jmp	.LBB11_14
	.p2align	4, 0x90
.LBB11_11:                              #   in Loop: Header=BB11_10 Depth=1
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jne	.LBB11_16
.LBB11_14:                              #   in Loop: Header=BB11_10 Depth=1
	movl	fol_EQUALITY(%rip), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	je	.LBB11_15
.LBB11_16:                              #   in Loop: Header=BB11_10 Depth=1
	movl	(%r12), %eax
	testb	$4, %al
	jne	.LBB11_24
# BB#17:                                #   in Loop: Header=BB11_10 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	testb	$2, 48(%rcx)
	jne	.LBB11_15
# BB#18:                                #   in Loop: Header=BB11_10 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB11_25
# BB#19:                                #   in Loop: Header=BB11_10 Depth=1
	testb	$1, %al
	jne	.LBB11_20
	jmp	.LBB11_15
	.p2align	4, 0x90
.LBB11_24:                              #   in Loop: Header=BB11_10 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB11_25
.LBB11_20:                              # %.thread
                                        #   in Loop: Header=BB11_10 Depth=1
	movq	16(%r12), %rdx
	movq	56(%rdx), %rsi
	movl	$-1, %ecx
	.p2align	4, 0x90
.LBB11_21:                              #   Parent Loop BB11_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ecx
	cmpq	%r12, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB11_21
# BB#22:                                # %clause_LiteralIsFromAntecedent.exit165
                                        #   in Loop: Header=BB11_10 Depth=1
	movl	64(%rdx), %esi
	movl	68(%rdx), %edx
	leal	-1(%rsi,%rdx), %edx
	cmpl	%esi, %ecx
	setl	%bl
	cmpl	%edx, %ecx
	setg	%cl
	orb	%bl, %cl
	movb	$1, %dl
	movl	%edx, 12(%rsp)          # 4-byte Spill
	cmpb	$1, %cl
	jne	.LBB11_26
# BB#23:                                # %clause_LiteralIsFromAntecedent.exit165
                                        #   in Loop: Header=BB11_10 Depth=1
	andl	$2, %eax
	je	.LBB11_15
	jmp	.LBB11_26
	.p2align	4, 0x90
.LBB11_25:                              #   in Loop: Header=BB11_10 Depth=1
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
.LBB11_26:                              # %.thread166.preheader
                                        #   in Loop: Header=BB11_10 Depth=1
	movl	$0, 52(%rsp)            # 4-byte Folded Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB11_27
	.p2align	4, 0x90
.LBB11_68:                              # %._crit_edge
                                        #   in Loop: Header=BB11_27 Depth=2
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	jne	.LBB11_69
# BB#70:                                #   in Loop: Header=BB11_27 Depth=2
	movl	fol_EQUALITY(%rip), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jne	.LBB11_69
# BB#71:                                #   in Loop: Header=BB11_27 Depth=2
	movq	16(%rcx), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	8(%rsi), %rsi
	movq	%rsi, 8(%rax)
	movq	16(%rcx), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	$1, 52(%rsp)            # 4-byte Folded Spill
.LBB11_27:                              # %.thread166
                                        #   Parent Loop BB11_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_28 Depth 3
                                        #         Child Loop BB11_31 Depth 4
                                        #           Child Loop BB11_32 Depth 5
                                        #           Child Loop BB11_40 Depth 5
                                        #           Child Loop BB11_55 Depth 5
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	callq	st_GetUnifier
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB11_68
	.p2align	4, 0x90
.LBB11_28:                              # %.lr.ph187
                                        #   Parent Loop BB11_10 Depth=1
                                        #     Parent Loop BB11_27 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB11_31 Depth 4
                                        #           Child Loop BB11_32 Depth 5
                                        #           Child Loop BB11_40 Depth 5
                                        #           Child Loop BB11_55 Depth 5
	movq	8(%r14), %rdi
	cmpl	$0, (%rdi)
	jg	.LBB11_67
# BB#29:                                #   in Loop: Header=BB11_28 Depth=3
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB11_67
# BB#30:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB11_28 Depth=3
	movq	%r14, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB11_31:                              # %.lr.ph
                                        #   Parent Loop BB11_10 Depth=1
                                        #     Parent Loop BB11_27 Depth=2
                                        #       Parent Loop BB11_28 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB11_32 Depth 5
                                        #           Child Loop BB11_40 Depth 5
                                        #           Child Loop BB11_55 Depth 5
	movq	8(%rbp), %r13
	movq	16(%r13), %r15
	movq	56(%r15), %rax
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB11_32:                              #   Parent Loop BB11_10 Depth=1
                                        #     Parent Loop BB11_27 Depth=2
                                        #       Parent Loop BB11_28 Depth=3
                                        #         Parent Loop BB11_31 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%ebx
	cmpq	%r13, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB11_32
# BB#33:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB11_31 Depth=4
	movq	24(%r13), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %eax
	movq	24(%r12), %rdx
	movl	(%rdx), %edx
	cmpl	%ecx, %eax
	jne	.LBB11_35
# BB#34:                                #   in Loop: Header=BB11_31 Depth=4
	cmpl	%ecx, %edx
	movl	%ecx, %edx
	jne	.LBB11_37
.LBB11_35:                              # %clause_LiteralsAreComplementary.exit
                                        #   in Loop: Header=BB11_31 Depth=4
	cmpl	%ecx, %eax
	je	.LBB11_66
# BB#36:                                # %clause_LiteralsAreComplementary.exit
                                        #   in Loop: Header=BB11_31 Depth=4
	cmpl	%edx, %eax
	jne	.LBB11_66
.LBB11_37:                              # %clause_LiteralsAreComplementary.exit.thread
                                        #   in Loop: Header=BB11_31 Depth=4
	movq	%r15, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB11_66
# BB#38:                                #   in Loop: Header=BB11_31 Depth=4
	movq	24(%r13), %rax
	movl	(%rax), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	jne	.LBB11_43
# BB#39:                                #   in Loop: Header=BB11_31 Depth=4
	movq	16(%r13), %rsi
	movq	56(%rsi), %rdi
	movl	$-1, %edx
	.p2align	4, 0x90
.LBB11_40:                              #   Parent Loop BB11_10 Depth=1
                                        #     Parent Loop BB11_27 Depth=2
                                        #       Parent Loop BB11_28 Depth=3
                                        #         Parent Loop BB11_31 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%edx
	cmpq	%r13, (%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB11_40
# BB#41:                                # %clause_LiteralIsFromAntecedent.exit
                                        #   in Loop: Header=BB11_31 Depth=4
	movl	64(%rsi), %edi
	cmpl	%edi, %edx
	jl	.LBB11_66
# BB#42:                                # %clause_LiteralIsFromAntecedent.exit
                                        #   in Loop: Header=BB11_31 Depth=4
	movl	68(%rsi), %esi
	leal	-1(%rdi,%rsi), %esi
	cmpl	%esi, %edx
	jg	.LBB11_66
.LBB11_43:                              #   in Loop: Header=BB11_31 Depth=4
	movl	(%r13), %edx
	testb	$4, %dl
	jne	.LBB11_47
# BB#44:                                #   in Loop: Header=BB11_31 Depth=4
	testb	$2, 48(%r15)
	jne	.LBB11_66
# BB#45:                                #   in Loop: Header=BB11_31 Depth=4
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB11_50
# BB#46:                                #   in Loop: Header=BB11_31 Depth=4
	testb	$1, %dl
	jne	.LBB11_48
	jmp	.LBB11_66
.LBB11_47:                              #   in Loop: Header=BB11_31 Depth=4
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB11_50
.LBB11_48:                              # %.thread167
                                        #   in Loop: Header=BB11_31 Depth=4
	cmpl	%eax, %ecx
	je	.LBB11_51
# BB#49:                                #   in Loop: Header=BB11_31 Depth=4
	testb	$2, %dl
	jne	.LBB11_52
	jmp	.LBB11_66
.LBB11_50:                              # %.critedge
                                        #   in Loop: Header=BB11_31 Depth=4
	cmpl	%eax, %ecx
	jne	.LBB11_52
.LBB11_51:                              # %.critedge.thread
                                        #   in Loop: Header=BB11_31 Depth=4
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	cmpl	(%r15), %eax
	je	.LBB11_66
.LBB11_52:                              # %.critedge.thread196
                                        #   in Loop: Header=BB11_31 Depth=4
	movl	52(%r15), %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	128(%rsp), %rcx         # 8-byte Reload
	callq	unify_UnifyNoOC
	testl	%eax, %eax
	je	.LBB11_72
# BB#53:                                #   in Loop: Header=BB11_31 Depth=4
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	88(%rsp), %rsi
	leaq	80(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB11_56
# BB#54:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB11_31 Depth=4
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB11_55:                              # %.lr.ph.i
                                        #   Parent Loop BB11_10 Depth=1
                                        #     Parent Loop BB11_27 Depth=2
                                        #       Parent Loop BB11_28 Depth=3
                                        #         Parent Loop BB11_31 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB11_55
.LBB11_56:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB11_31 Depth=4
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB11_61
# BB#57:                                #   in Loop: Header=BB11_31 Depth=4
	movq	80(%rsp), %r9
	movq	16(%rsp), %rdi          # 8-byte Reload
	testb	$2, 48(%rdi)
	jne	.LBB11_59
# BB#58:                                #   in Loop: Header=BB11_31 Depth=4
	movq	88(%rsp), %rcx
	movl	64(%rdi), %eax
	movl	68(%rdi), %edx
	leal	-1(%rax,%rdx), %eax
	cltq
	xorl	%r8d, %r8d
	movq	24(%rsp), %rsi          # 8-byte Reload
	cmpq	%rsi, %rax
	setl	%r8b
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movl	$-1, %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r12, %r14
	movq	%r9, %r12
	movq	56(%rsp), %r9           # 8-byte Reload
	callq	inf_LitMax
	movq	%r12, %r9
	movq	%r14, %r12
	movq	112(%rsp), %r14         # 8-byte Reload
	testl	%eax, %eax
	je	.LBB11_65
.LBB11_59:                              #   in Loop: Header=BB11_31 Depth=4
	testb	$2, 48(%r15)
	jne	.LBB11_61
# BB#60:                                #   in Loop: Header=BB11_31 Depth=4
	movl	64(%r15), %eax
	movl	68(%r15), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	cmpl	%ebx, %eax
	setl	%r8b
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movl	$-1, %edx
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	%r9, %rcx
	movq	56(%rsp), %r9           # 8-byte Reload
	callq	inf_LitMax
	testl	%eax, %eax
	je	.LBB11_65
.LBB11_61:                              # %inf_LiteralsMax.exit
                                        #   in Loop: Header=BB11_31 Depth=4
	movq	24(%r13), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB11_63
# BB#62:                                #   in Loop: Header=BB11_31 Depth=4
	movq	88(%rsp), %rdx
	movq	80(%rsp), %rcx
	movq	%r12, %rdi
	movq	%r13, %rsi
	jmp	.LBB11_64
.LBB11_63:                              #   in Loop: Header=BB11_31 Depth=4
	movq	80(%rsp), %rdx
	movq	88(%rsp), %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
.LBB11_64:                              # %.sink.split
                                        #   in Loop: Header=BB11_31 Depth=4
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	64(%rsp), %r9           # 8-byte Reload
	callq	inf_ApplyGenRes
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB11_65:                              # %inf_LiteralsMax.exit.thread
                                        #   in Loop: Header=BB11_31 Depth=4
	movq	88(%rsp), %rdi
	callq	subst_Delete
	movq	80(%rsp), %rdi
	callq	subst_Delete
	.p2align	4, 0x90
.LBB11_66:                              #   in Loop: Header=BB11_31 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB11_31
.LBB11_67:                              # %.loopexit
                                        #   in Loop: Header=BB11_28 Depth=3
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB11_28
	jmp	.LBB11_68
	.p2align	4, 0x90
.LBB11_69:                              #   in Loop: Header=BB11_10 Depth=1
	movl	44(%rsp), %ebp          # 4-byte Reload
.LBB11_15:                              #   in Loop: Header=BB11_10 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	cmpq	96(%rsp), %rsi          # 8-byte Folded Reload
	leaq	1(%rsi), %rsi
	jl	.LBB11_10
.LBB11_7:                               # %._crit_edge194
	movq	%rax, %rdi
	callq	clause_Delete
	movq	72(%rsp), %rax          # 8-byte Reload
.LBB11_8:
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_72:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$2382, %ecx             # imm = 0x94E
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end11:
	.size	inf_GeneralResolution, .Lfunc_end11-inf_GeneralResolution
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_DumpCore,@function
misc_DumpCore:                          # @misc_DumpCore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi244:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end12:
	.size	misc_DumpCore, .Lfunc_end12-misc_DumpCore
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_ApplyGenRes,@function
inf_ApplyGenRes:                        # @inf_ApplyGenRes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi245:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi246:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi247:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi248:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi249:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi250:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi251:
	.cfi_def_cfa_offset 208
.Lcfi252:
	.cfi_offset %rbx, -56
.Lcfi253:
	.cfi_offset %r12, -48
.Lcfi254:
	.cfi_offset %r13, -40
.Lcfi255:
	.cfi_offset %r14, -32
.Lcfi256:
	.cfi_offset %r15, -24
.Lcfi257:
	.cfi_offset %rbp, -16
	movq	%r9, 128(%rsp)          # 8-byte Spill
	movq	%r8, 136(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rbx
	movq	16(%rdi), %r11
	movl	64(%rbx), %ebp
	movl	68(%rbx), %r9d
	movl	72(%rbx), %r8d
	leal	-1(%rbp), %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	leal	-1(%rbp,%r9), %r10d
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	56(%rbx), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_1:                               # =>This Inner Loop Header: Depth=1
	movq	%rbp, %r15
	leaq	1(%r15), %rbp
	cmpq	%rsi, (%rbx,%r15,8)
	jne	.LBB13_1
# BB#2:                                 # %clause_LiteralGetIndex.exit206
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%r10, 120(%rsp)         # 8-byte Spill
	leal	(%r10,%r8), %ecx
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	movl	64(%r11), %eax
	movl	68(%r11), %edx
	movl	72(%r11), %ebx
	leal	-1(%rax), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	leal	-1(%rax,%rdx), %r13d
	movq	%r11, 24(%rsp)          # 8-byte Spill
	movq	56(%r11), %rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_3:                               # =>This Inner Loop Header: Depth=1
	movq	%rbp, %r14
	leaq	1(%r14), %rbp
	cmpq	%rdi, (%rsi,%r14,8)
	jne	.LBB13_3
# BB#4:                                 # %clause_LiteralGetIndex.exit
	movq	%rax, 32(%rsp)          # 8-byte Spill
	addl	%eax, %edx
	addl	%ebx, %edx
	addl	40(%rsp), %edx          # 4-byte Folded Reload
	addl	%r9d, %edx
	leal	-2(%r8,%rdx), %edi
	callq	clause_CreateBody
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	cmpl	84(%rsp), %r15d         # 4-byte Folded Reload
	setle	%bpl
	movl	$-1, %esi
	movl	$0, %edi
	cmovgl	%esi, %edi
	cmovgl	%r12d, %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
	addl	64(%rcx), %esi
	movq	56(%rsp), %rdx          # 8-byte Reload
	addl	64(%rdx), %esi
	movl	%esi, 64(%rax)
	movl	68(%rcx), %esi
	movl	%edi, 108(%rsp)         # 4-byte Spill
	addl	%edi, %esi
	addl	68(%rdx), %esi
	movl	%esi, 68(%rax)
	movl	72(%rcx), %esi
	movl	72(%rdx), %ecx
	leal	-1(%rsi,%rcx), %ecx
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%ecx, 72(%rax)
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movq	%r15, 112(%rsp)         # 8-byte Spill
	js	.LBB13_8
# BB#5:                                 # %.lr.ph232
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	movl	%ebx, 52(%rsp)          # 4-byte Spill
	xorl	%ebp, %ebp
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB13_6:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	clause_LiteralCreate
	movq	56(%rbx), %rcx
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB13_6
# BB#7:
	movl	%r15d, %r12d
	movl	52(%rsp), %ebx          # 4-byte Reload
	movl	16(%rsp), %r13d         # 4-byte Reload
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
.LBB13_8:                               # %._crit_edge233
	addl	%r13d, %ebx
	movq	56(%rsp), %rax          # 8-byte Reload
	movslq	64(%rax), %rax
	subq	%rbp, %rax
	cmpl	%r13d, %r12d
	movl	%ebx, 52(%rsp)          # 4-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jg	.LBB13_11
# BB#9:                                 # %.lr.ph228
	movslq	%r12d, %rbx
	movslq	%r13d, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	decq	%rbx
	leaq	(,%rax,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB13_10:                              # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	clause_LiteralCreate
	movq	56(%r13), %rcx
	addq	72(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rax, 8(%rcx,%rbx,8)
	incq	%rbx
	incl	%r12d
	cmpq	32(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB13_10
.LBB13_11:                              # %._crit_edge229
	movl	52(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, %r12d
	movq	16(%rsp), %rdx          # 8-byte Reload
	jg	.LBB13_17
# BB#12:                                # %.lr.ph224
	addl	108(%rsp), %edx         # 4-byte Folded Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	addl	68(%rax), %edx
	movslq	%r12d, %r13
	movslq	%ecx, %rcx
	decq	%r13
	movl	%r12d, %ebp
	movq	$-1, %rbx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB13_13:                              # =>This Inner Loop Header: Depth=1
	leal	1(%rbp,%rbx), %eax
	cmpl	%eax, %r14d
	jne	.LBB13_14
# BB#15:                                #   in Loop: Header=BB13_13 Depth=1
	decl	%edx
	jmp	.LBB13_16
	.p2align	4, 0x90
.LBB13_14:                              #   in Loop: Header=BB13_13 Depth=1
	leal	(%rbp,%rbx), %eax
	leal	1(%rdx,%rax), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%r13,8), %rax
	movq	24(%rax), %rdi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	callq	term_Copy
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	clause_LiteralCreate
	movq	56(%r12), %rcx
	movslq	32(%rsp), %rdx          # 4-byte Folded Reload
	movq	%rax, (%rcx,%rdx,8)
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB13_16:                              #   in Loop: Header=BB13_13 Depth=1
	incq	%r13
	incq	%rbx
	cmpq	%rcx, %r13
	jl	.LBB13_13
.LBB13_17:                              # %._crit_edge225
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	64(%rax), %ebp
	cmpl	$0, 84(%rsp)            # 4-byte Folded Reload
	js	.LBB13_18
# BB#19:                                # %.lr.ph217
	movl	%r15d, %eax
	xorl	%ebx, %ebx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB13_20:                              # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %rax
	jne	.LBB13_21
# BB#22:                                #   in Loop: Header=BB13_20 Depth=1
	decl	%ebp
	jmp	.LBB13_23
	.p2align	4, 0x90
.LBB13_21:                              #   in Loop: Header=BB13_20 Depth=1
	leal	(%rbx,%rbp), %r15d
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	clause_LiteralCreate
	movq	56(%r13), %rcx
	movslq	%r15d, %rdx
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	%rax, (%rcx,%rdx,8)
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB13_23:                              #   in Loop: Header=BB13_20 Depth=1
	incq	%rbx
	cmpq	%rbx, 40(%rsp)          # 8-byte Folded Reload
	jne	.LBB13_20
# BB#24:                                # %._crit_edge218.loopexit
	movq	40(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB13_25
.LBB13_18:
	xorl	%ecx, %ecx
.LBB13_25:                              # %._crit_edge218
	movq	24(%rsp), %rax          # 8-byte Reload
	addl	68(%rax), %ebp
	movq	120(%rsp), %rax         # 8-byte Reload
	cmpl	%eax, %ecx
	jg	.LBB13_32
# BB#26:                                # %.lr.ph211
	movslq	%ecx, %rbx
	movslq	%eax, %rdx
	decq	%rbx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%ecx, %ecx
	movq	$-1, %r13
	movq	%r14, 144(%rsp)         # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB13_27:                              # =>This Inner Loop Header: Depth=1
	leal	1(%rcx,%r13), %eax
	cmpl	%eax, %r15d
	jne	.LBB13_28
# BB#29:                                #   in Loop: Header=BB13_27 Depth=1
	decl	%ebp
	jmp	.LBB13_30
	.p2align	4, 0x90
.LBB13_28:                              #   in Loop: Header=BB13_27 Depth=1
	leal	(%rcx,%r13), %eax
	leal	1(%rbp,%rax), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movq	%rcx, %r14
	callq	term_Copy
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	clause_LiteralCreate
	movq	56(%r12), %rcx
	movslq	16(%rsp), %rdx          # 4-byte Folded Reload
	movq	%rax, (%rcx,%rdx,8)
	movq	%r14, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	144(%rsp), %r14         # 8-byte Reload
.LBB13_30:                              #   in Loop: Header=BB13_27 Depth=1
	incq	%rbx
	incq	%r13
	cmpq	%rdx, %rbx
	jl	.LBB13_27
# BB#31:                                # %._crit_edge212.loopexit
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	1(%rax,%r13), %eax
	movq	%rax, %rcx
.LBB13_32:                              # %._crit_edge212
	movl	80(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %ecx
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	jg	.LBB13_35
# BB#33:                                # %.lr.ph
	movl	%eax, %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	72(%rax), %eax
	movslq	%ecx, %rbx
	movslq	%edx, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	decq	%rbx
	addl	%ecx, %ebp
	leal	-1(%rax,%rbp), %ebp
	.p2align	4, 0x90
.LBB13_34:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	clause_LiteralCreate
	movq	56(%r13), %rcx
	movslq	%ebp, %rbp
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbx
	incl	%ebp
	cmpq	40(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB13_34
.LBB13_35:                              # %._crit_edge
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, %rdi
	movq	%r12, %rsi
	movl	%r15d, %edx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%r14d, %r8d
	movq	136(%rsp), %r9          # 8-byte Reload
	callq	clause_SetDataFromParents
	movl	$13, 76(%r13)
	movq	%r13, %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	inf_ApplyGenRes, .Lfunc_end13-inf_ApplyGenRes
	.cfi_endproc

	.globl	inf_UnitResolution
	.p2align	4, 0x90
	.type	inf_UnitResolution,@function
inf_UnitResolution:                     # @inf_UnitResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi258:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi259:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi260:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi261:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi262:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi263:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi264:
	.cfi_def_cfa_offset 160
.Lcfi265:
	.cfi_offset %rbx, -56
.Lcfi266:
	.cfi_offset %r12, -48
.Lcfi267:
	.cfi_offset %r13, -40
.Lcfi268:
	.cfi_offset %r14, -32
.Lcfi269:
	.cfi_offset %r15, -24
.Lcfi270:
	.cfi_offset %rbp, -16
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%edx, 44(%rsp)          # 4-byte Spill
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	callq	clause_HasSolvedConstraint
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB14_43
# BB#1:
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	clause_Copy
	movslq	64(%rax), %rsi
	movl	72(%rax), %edx
	movl	68(%rax), %r13d
	addl	%esi, %r13d
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testb	$2, 48(%rax)
	movl	$0, %ecx
	cmovel	%edx, %ecx
	leal	-1(%rcx,%r13), %ecx
	cmpl	%ecx, %esi
	jg	.LBB14_42
# BB#2:                                 # %.lr.ph146
	addl	%edx, %r13d
	movslq	%ecx, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB14_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_10 Depth 2
                                        #       Child Loop BB14_11 Depth 3
                                        #         Child Loop BB14_14 Depth 4
                                        #           Child Loop BB14_23 Depth 5
                                        #           Child Loop BB14_31 Depth 5
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rsi,8), %r15
	movq	24(%r15), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_3 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB14_5:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB14_3 Depth=1
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	jne	.LBB14_7
# BB#6:                                 #   in Loop: Header=BB14_3 Depth=1
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%rcx), %eax
	je	.LBB14_41
.LBB14_7:                               #   in Loop: Header=BB14_3 Depth=1
	testb	$4, (%r15)
	jne	.LBB14_9
# BB#8:                                 #   in Loop: Header=BB14_3 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	testb	$2, 48(%rax)
	jne	.LBB14_41
.LBB14_9:                               # %.preheader
                                        #   in Loop: Header=BB14_3 Depth=1
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jmp	.LBB14_10
	.p2align	4, 0x90
.LBB14_38:                              # %._crit_edge
                                        #   in Loop: Header=BB14_10 Depth=2
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	jne	.LBB14_40
# BB#39:                                #   in Loop: Header=BB14_10 Depth=2
	movl	fol_EQUALITY(%rip), %eax
	movq	72(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jne	.LBB14_40
# BB#45:                                #   in Loop: Header=BB14_10 Depth=2
	movq	16(%rcx), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	8(%rsi), %rsi
	movq	%rsi, 8(%rax)
	movq	16(%rcx), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
.LBB14_10:                              #   Parent Loop BB14_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_11 Depth 3
                                        #         Child Loop BB14_14 Depth 4
                                        #           Child Loop BB14_23 Depth 5
                                        #           Child Loop BB14_31 Depth 5
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	callq	st_GetUnifier
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB14_38
	.p2align	4, 0x90
.LBB14_11:                              # %.lr.ph141
                                        #   Parent Loop BB14_3 Depth=1
                                        #     Parent Loop BB14_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB14_14 Depth 4
                                        #           Child Loop BB14_23 Depth 5
                                        #           Child Loop BB14_31 Depth 5
	movq	8(%r14), %rdi
	cmpl	$0, (%rdi)
	jg	.LBB14_37
# BB#12:                                #   in Loop: Header=BB14_11 Depth=3
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	callq	sharing_NAtomDataList
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB14_14
	jmp	.LBB14_37
	.p2align	4, 0x90
.LBB14_36:                              #   in Loop: Header=BB14_14 Depth=4
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB14_37
.LBB14_14:                              # %.lr.ph
                                        #   Parent Loop BB14_3 Depth=1
                                        #     Parent Loop BB14_10 Depth=2
                                        #       Parent Loop BB14_11 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB14_23 Depth 5
                                        #           Child Loop BB14_31 Depth 5
	cmpl	$1, %r13d
	movq	8(%rbx), %r12
	movq	16(%r12), %rbp
	je	.LBB14_16
# BB#15:                                #   in Loop: Header=BB14_14 Depth=4
	movl	68(%rbp), %eax
	addl	64(%rbp), %eax
	addl	72(%rbp), %eax
	cmpl	$1, %eax
	jne	.LBB14_36
.LBB14_16:                              #   in Loop: Header=BB14_14 Depth=4
	movq	24(%r12), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %eax
	movq	24(%r15), %rdx
	movl	(%rdx), %edx
	cmpl	%ecx, %eax
	jne	.LBB14_18
# BB#17:                                #   in Loop: Header=BB14_14 Depth=4
	cmpl	%ecx, %edx
	movl	%ecx, %edx
	jne	.LBB14_20
.LBB14_18:                              # %clause_LiteralsAreComplementary.exit
                                        #   in Loop: Header=BB14_14 Depth=4
	cmpl	%ecx, %eax
	je	.LBB14_36
# BB#19:                                # %clause_LiteralsAreComplementary.exit
                                        #   in Loop: Header=BB14_14 Depth=4
	cmpl	%edx, %eax
	jne	.LBB14_36
.LBB14_20:                              # %clause_LiteralsAreComplementary.exit.thread
                                        #   in Loop: Header=BB14_14 Depth=4
	movq	%rbp, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB14_36
# BB#21:                                #   in Loop: Header=BB14_14 Depth=4
	movq	24(%r12), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB14_26
# BB#22:                                #   in Loop: Header=BB14_14 Depth=4
	movq	16(%r12), %rcx
	movq	56(%rcx), %rdx
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB14_23:                              #   Parent Loop BB14_3 Depth=1
                                        #     Parent Loop BB14_10 Depth=2
                                        #       Parent Loop BB14_11 Depth=3
                                        #         Parent Loop BB14_14 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%eax
	cmpq	%r12, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB14_23
# BB#24:                                # %clause_LiteralIsFromAntecedent.exit
                                        #   in Loop: Header=BB14_14 Depth=4
	movl	64(%rcx), %edx
	cmpl	%edx, %eax
	jl	.LBB14_36
# BB#25:                                # %clause_LiteralIsFromAntecedent.exit
                                        #   in Loop: Header=BB14_14 Depth=4
	movl	68(%rcx), %ecx
	leal	-1(%rdx,%rcx), %ecx
	cmpl	%ecx, %eax
	jg	.LBB14_36
.LBB14_26:                              #   in Loop: Header=BB14_14 Depth=4
	testb	$4, (%r12)
	jne	.LBB14_28
# BB#27:                                #   in Loop: Header=BB14_14 Depth=4
	testb	$2, 48(%rbp)
	jne	.LBB14_36
.LBB14_28:                              #   in Loop: Header=BB14_14 Depth=4
	movl	52(%rbp), %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	testl	%eax, %eax
	je	.LBB14_44
# BB#29:                                #   in Loop: Header=BB14_14 Depth=4
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	32(%rsp), %rsi
	leaq	24(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB14_32
# BB#30:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB14_14 Depth=4
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB14_31:                              # %.lr.ph.i
                                        #   Parent Loop BB14_3 Depth=1
                                        #     Parent Loop BB14_10 Depth=2
                                        #       Parent Loop BB14_11 Depth=3
                                        #         Parent Loop BB14_14 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB14_31
.LBB14_32:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB14_14 Depth=4
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	24(%r12), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB14_34
# BB#33:                                #   in Loop: Header=BB14_14 Depth=4
	movq	32(%rsp), %rdx
	movq	24(%rsp), %rcx
	movq	%r15, %rdi
	movq	%r12, %rsi
	jmp	.LBB14_35
.LBB14_34:                              #   in Loop: Header=BB14_14 Depth=4
	movq	24(%rsp), %rdx
	movq	32(%rsp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rsi
.LBB14_35:                              #   in Loop: Header=BB14_14 Depth=4
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	64(%rsp), %r9           # 8-byte Reload
	callq	inf_ApplyGenRes
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r12, 8(%rbp)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movq	32(%rsp), %rdi
	callq	subst_Delete
	movq	24(%rsp), %rdi
	callq	subst_Delete
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jmp	.LBB14_36
	.p2align	4, 0x90
.LBB14_37:                              # %.loopexit
                                        #   in Loop: Header=BB14_11 Depth=3
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB14_11
	jmp	.LBB14_38
	.p2align	4, 0x90
.LBB14_40:                              #   in Loop: Header=BB14_3 Depth=1
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB14_41:                              # %.loopexit133
                                        #   in Loop: Header=BB14_3 Depth=1
	cmpq	80(%rsp), %rsi          # 8-byte Folded Reload
	leaq	1(%rsi), %rsi
	jl	.LBB14_3
.LBB14_42:                              # %._crit_edge147
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	clause_Delete
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB14_43:
	movq	%rcx, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_44:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$2525, %ecx             # imm = 0x9DD
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end14:
	.size	inf_UnitResolution, .Lfunc_end14-inf_UnitResolution
	.cfi_endproc

	.globl	inf_BoundedDepthUnitResolution
	.p2align	4, 0x90
	.type	inf_BoundedDepthUnitResolution,@function
inf_BoundedDepthUnitResolution:         # @inf_BoundedDepthUnitResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi271:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi272:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi273:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi274:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi275:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi276:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi277:
	.cfi_def_cfa_offset 160
.Lcfi278:
	.cfi_offset %rbx, -56
.Lcfi279:
	.cfi_offset %r12, -48
.Lcfi280:
	.cfi_offset %r13, -40
.Lcfi281:
	.cfi_offset %r14, -32
.Lcfi282:
	.cfi_offset %r15, -24
.Lcfi283:
	.cfi_offset %rbp, -16
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	callq	clause_Copy
	movq	%rax, %rbx
	movl	64(%rbx), %ebp
	addl	68(%rbx), %ebp
	addl	72(%rbx), %ebp
	movq	%rbx, %rdi
	callq	clause_ComputeTermDepth
	movl	%eax, 32(%rsp)          # 4-byte Spill
	decl	%ebp
	js	.LBB15_1
# BB#2:                                 # %.lr.ph129
	movslq	%ebp, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB15_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_6 Depth 2
                                        #       Child Loop BB15_8 Depth 3
                                        #         Child Loop BB15_11 Depth 4
                                        #           Child Loop BB15_24 Depth 5
	movq	56(%rbx), %rax
	movq	(%rax,%rdx,8), %r14
	movq	24(%r14), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	jne	.LBB15_5
# BB#4:                                 #   in Loop: Header=BB15_3 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB15_5:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB15_3 Depth=1
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	jmp	.LBB15_6
	.p2align	4, 0x90
.LBB15_36:                              #   in Loop: Header=BB15_6 Depth=2
	movq	16(%rcx), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	8(%rsi), %rsi
	movq	%rsi, 8(%rax)
	movq	16(%rcx), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	$1, 4(%rsp)             # 4-byte Folded Spill
.LBB15_6:                               #   Parent Loop BB15_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_8 Depth 3
                                        #         Child Loop BB15_11 Depth 4
                                        #           Child Loop BB15_24 Depth 5
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	callq	st_GetUnifier
	testq	%rax, %rax
	jne	.LBB15_8
	jmp	.LBB15_34
	.p2align	4, 0x90
.LBB15_33:                              # %.loopexit
                                        #   in Loop: Header=BB15_8 Depth=3
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	je	.LBB15_34
.LBB15_8:                               # %.lr.ph124
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB15_11 Depth 4
                                        #           Child Loop BB15_24 Depth 5
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	8(%rax), %rdi
	cmpl	$0, (%rdi)
	jg	.LBB15_33
# BB#9:                                 #   in Loop: Header=BB15_8 Depth=3
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	callq	sharing_NAtomDataList
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB15_11
	jmp	.LBB15_33
	.p2align	4, 0x90
.LBB15_32:                              #   in Loop: Header=BB15_11 Depth=4
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB15_33
.LBB15_11:                              # %.lr.ph
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_8 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB15_24 Depth 5
	movq	8(%r15), %rbp
	movq	16(%rbp), %r13
	movq	24(%rbp), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %eax
	movq	24(%r14), %rdx
	movl	(%rdx), %edx
	cmpl	%ecx, %eax
	jne	.LBB15_13
# BB#12:                                #   in Loop: Header=BB15_11 Depth=4
	cmpl	%ecx, %edx
	movl	%ecx, %edx
	jne	.LBB15_15
.LBB15_13:                              # %clause_LiteralsAreComplementary.exit
                                        #   in Loop: Header=BB15_11 Depth=4
	cmpl	%ecx, %eax
	je	.LBB15_32
# BB#14:                                # %clause_LiteralsAreComplementary.exit
                                        #   in Loop: Header=BB15_11 Depth=4
	cmpl	%edx, %eax
	jne	.LBB15_32
.LBB15_15:                              # %clause_LiteralsAreComplementary.exit.thread
                                        #   in Loop: Header=BB15_11 Depth=4
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	addl	72(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB15_17
# BB#16:                                #   in Loop: Header=BB15_11 Depth=4
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	cmpl	$1, %eax
	jne	.LBB15_32
.LBB15_17:                              #   in Loop: Header=BB15_11 Depth=4
	testb	$8, 48(%rbx)
	jne	.LBB15_19
# BB#18:                                #   in Loop: Header=BB15_11 Depth=4
	testb	$8, 48(%r13)
	je	.LBB15_32
.LBB15_19:                              #   in Loop: Header=BB15_11 Depth=4
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB15_21
# BB#20:                                #   in Loop: Header=BB15_11 Depth=4
	testb	$8, 48(%r13)
	je	.LBB15_32
.LBB15_21:                              #   in Loop: Header=BB15_11 Depth=4
	movq	%r13, %rdi
	callq	clause_ComputeTermDepth
	movl	32(%rsp), %edi          # 4-byte Reload
	movl	%eax, %esi
	callq	misc_Max
	movl	%eax, %r12d
	movl	52(%r13), %esi
	movq	%rbx, %rdi
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	testl	%eax, %eax
	je	.LBB15_39
# BB#22:                                #   in Loop: Header=BB15_11 Depth=4
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	24(%rsp), %rsi
	leaq	16(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB15_25
# BB#23:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB15_11 Depth=4
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB15_24:                              # %.lr.ph.i
                                        #   Parent Loop BB15_3 Depth=1
                                        #     Parent Loop BB15_6 Depth=2
                                        #       Parent Loop BB15_8 Depth=3
                                        #         Parent Loop BB15_11 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB15_24
.LBB15_25:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB15_11 Depth=4
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	24(%rbp), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB15_27
# BB#26:                                #   in Loop: Header=BB15_11 Depth=4
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rcx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	jmp	.LBB15_28
.LBB15_27:                              #   in Loop: Header=BB15_11 Depth=4
	movq	16(%rsp), %rdx
	movq	24(%rsp), %rcx
	movq	%rbp, %rdi
	movq	%r14, %rsi
.LBB15_28:                              #   in Loop: Header=BB15_11 Depth=4
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	inf_ApplyGenRes
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	clause_ComputeTermDepth
	cmpl	%r12d, %eax
	jbe	.LBB15_30
# BB#29:                                #   in Loop: Header=BB15_11 Depth=4
	movq	%rbp, %rdi
	callq	clause_Delete
	jmp	.LBB15_31
.LBB15_30:                              #   in Loop: Header=BB15_11 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB15_31:                              #   in Loop: Header=BB15_11 Depth=4
	movq	24(%rsp), %rdi
	callq	subst_Delete
	movq	16(%rsp), %rdi
	callq	subst_Delete
	jmp	.LBB15_32
	.p2align	4, 0x90
.LBB15_34:                              # %._crit_edge
                                        #   in Loop: Header=BB15_6 Depth=2
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	jne	.LBB15_37
# BB#35:                                #   in Loop: Header=BB15_6 Depth=2
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%rcx), %eax
	je	.LBB15_36
.LBB15_37:                              #   in Loop: Header=BB15_3 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	cmpq	64(%rsp), %rdx          # 8-byte Folded Reload
	leaq	1(%rdx), %rdx
	jl	.LBB15_3
	jmp	.LBB15_38
.LBB15_1:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB15_38:                              # %._crit_edge130
	movq	%rbx, %rdi
	callq	clause_Delete
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_39:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$2645, %ecx             # imm = 0xA55
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end15:
	.size	inf_BoundedDepthUnitResolution, .Lfunc_end15-inf_BoundedDepthUnitResolution
	.cfi_endproc

	.globl	inf_GeneralFactoring
	.p2align	4, 0x90
	.type	inf_GeneralFactoring,@function
inf_GeneralFactoring:                   # @inf_GeneralFactoring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi284:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi285:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi286:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi287:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi288:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi289:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi290:
	.cfi_def_cfa_offset 144
.Lcfi291:
	.cfi_offset %rbx, -56
.Lcfi292:
	.cfi_offset %r12, -48
.Lcfi293:
	.cfi_offset %r13, -40
.Lcfi294:
	.cfi_offset %r14, -32
.Lcfi295:
	.cfi_offset %r15, -24
.Lcfi296:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movl	%ecx, 76(%rsp)          # 4-byte Spill
	movl	%edx, %ebp
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movq	%rdi, %rbx
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB16_1
# BB#2:
	testb	$2, 48(%rbx)
	movq	%r12, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %r8
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%ebp, 60(%rsp)          # 4-byte Spill
	jne	.LBB16_3
# BB#4:
	movl	68(%r8), %eax
	movl	72(%r8), %ecx
	addl	64(%r8), %eax
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	jle	.LBB16_5
.LBB16_3:
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_45
	jmp	.LBB16_92
.LBB16_1:
	xorl	%eax, %eax
	jmp	.LBB16_93
.LBB16_5:                               # %.lr.ph287
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movslq	%ecx, %r13
	movslq	%eax, %r15
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB16_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_15 Depth 2
                                        #       Child Loop BB16_29 Depth 3
                                        #       Child Loop BB16_40 Depth 3
	movq	56(%r8), %rax
	movq	(%rax,%r15,8), %rax
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB16_8
# BB#7:                                 #   in Loop: Header=BB16_6 Depth=1
	testb	$1, (%rax)
	je	.LBB16_43
.LBB16_8:                               #   in Loop: Header=BB16_6 Depth=1
	movq	24(%rax), %r14
	movl	(%r14), %eax
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_12
# BB#9:                                 #   in Loop: Header=BB16_6 Depth=1
	cmpl	%eax, fol_NOT(%rip)
	movl	%eax, %ecx
	jne	.LBB16_11
# BB#10:                                #   in Loop: Header=BB16_6 Depth=1
	movq	16(%r14), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB16_11:                              # %clause_LiteralIsEquality.exit
                                        #   in Loop: Header=BB16_6 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	je	.LBB16_43
.LBB16_12:                              # %._crit_edge
                                        #   in Loop: Header=BB16_6 Depth=1
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB16_14
# BB#13:                                #   in Loop: Header=BB16_6 Depth=1
	movq	16(%r14), %rax
	movq	8(%rax), %r14
.LBB16_14:                              # %clause_LiteralAtom.exit242
                                        #   in Loop: Header=BB16_6 Depth=1
	movslq	68(%r8), %rax
	movslq	64(%r8), %rbx
	addq	%rax, %rbx
	cmpl	64(%rsp), %ebx          # 4-byte Folded Reload
	jg	.LBB16_43
	.p2align	4, 0x90
.LBB16_15:                              #   Parent Loop BB16_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_29 Depth 3
                                        #       Child Loop BB16_40 Depth 3
	cmpl	%ebx, %r15d
	je	.LBB16_42
# BB#16:                                #   in Loop: Header=BB16_15 Depth=2
	movq	56(%r8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rbp
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rbp), %ecx
	jne	.LBB16_18
# BB#17:                                #   in Loop: Header=BB16_15 Depth=2
	movq	16(%rbp), %rcx
	movq	8(%rcx), %rbp
.LBB16_18:                              # %clause_LiteralAtom.exit256
                                        #   in Loop: Header=BB16_15 Depth=2
	cmpq	%r15, %rbx
	jg	.LBB16_21
# BB#19:                                #   in Loop: Header=BB16_15 Depth=2
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB16_42
# BB#20:                                #   in Loop: Header=BB16_15 Depth=2
	testb	$1, (%rax)
	jne	.LBB16_42
.LBB16_21:                              #   in Loop: Header=BB16_15 Depth=2
	movl	(%r14), %eax
	cmpl	(%rbp), %eax
	jne	.LBB16_42
# BB#22:                                #   in Loop: Header=BB16_15 Depth=2
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB16_27
# BB#23:                                #   in Loop: Header=BB16_15 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	32(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB16_25
# BB#24:                                #   in Loop: Header=BB16_15 Depth=2
	movq	32(%rsp), %rcx
	movq	%r12, (%rsp)
	xorl	%r8d, %r8d
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%ebx, %edx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	inf_LitMax
	testl	%eax, %eax
	je	.LBB16_26
.LBB16_25:                              #   in Loop: Header=BB16_15 Depth=2
	movq	32(%rsp), %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%ebx, %edx
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%r12, %r9
	callq	inf_ApplyGeneralFactoring
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB16_26:                              #   in Loop: Header=BB16_15 Depth=2
	movq	32(%rsp), %rdi
	callq	subst_Delete
.LBB16_27:                              #   in Loop: Header=BB16_15 Depth=2
	movq	16(%rsp), %r8           # 8-byte Reload
	xorps	%xmm0, %xmm0
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB16_30
# BB#28:                                # %.lr.ph.preheader.i244
                                        #   in Loop: Header=BB16_15 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB16_29:                              # %.lr.ph.i247
                                        #   Parent Loop BB16_6 Depth=1
                                        #     Parent Loop BB16_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB16_29
.LBB16_30:                              # %cont_Reset.exit248
                                        #   in Loop: Header=BB16_15 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%r14), %eax
	jne	.LBB16_38
# BB#31:                                #   in Loop: Header=BB16_15 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	16(%rbp), %rax
	movq	8(%rax), %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB16_37
# BB#32:                                #   in Loop: Header=BB16_15 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	16(%r14), %rax
	movq	8(%rax), %rsi
	movq	16(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB16_37
# BB#33:                                #   in Loop: Header=BB16_15 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	32(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB16_35
# BB#34:                                #   in Loop: Header=BB16_15 Depth=2
	movq	32(%rsp), %rcx
	movq	%r12, (%rsp)
	xorl	%r8d, %r8d
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%ebx, %edx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	inf_LitMax
	testl	%eax, %eax
	je	.LBB16_36
.LBB16_35:                              #   in Loop: Header=BB16_15 Depth=2
	movq	32(%rsp), %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%ebx, %edx
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%r12, %r9
	callq	inf_ApplyGeneralFactoring
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB16_36:                              #   in Loop: Header=BB16_15 Depth=2
	movq	32(%rsp), %rdi
	callq	subst_Delete
.LBB16_37:                              #   in Loop: Header=BB16_15 Depth=2
	movq	16(%rsp), %r8           # 8-byte Reload
	xorps	%xmm0, %xmm0
.LBB16_38:                              #   in Loop: Header=BB16_15 Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB16_41
# BB#39:                                # %.lr.ph.preheader.i230
                                        #   in Loop: Header=BB16_15 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB16_40:                              # %.lr.ph.i233
                                        #   Parent Loop BB16_6 Depth=1
                                        #     Parent Loop BB16_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB16_40
.LBB16_41:                              # %cont_Reset.exit234
                                        #   in Loop: Header=BB16_15 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	.p2align	4, 0x90
.LBB16_42:                              #   in Loop: Header=BB16_15 Depth=2
	cmpq	%r13, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB16_15
.LBB16_43:                              # %.loopexit272
                                        #   in Loop: Header=BB16_6 Depth=1
	cmpq	%r13, %r15
	leaq	1(%r15), %r15
	jl	.LBB16_6
# BB#44:                                # %.loopexit273
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	je	.LBB16_92
.LBB16_45:
	movslq	64(%r8), %r15
	movl	68(%r8), %eax
	leal	-1(%r15,%rax), %eax
	cmpl	%eax, %r15d
	jg	.LBB16_92
# BB#46:                                # %.lr.ph279
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movslq	%eax, %r13
	.p2align	4, 0x90
.LBB16_47:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_58 Depth 2
                                        #       Child Loop BB16_76 Depth 3
                                        #       Child Loop BB16_88 Depth 3
	movq	56(%r8), %rax
	movq	(%rax,%r15,8), %rdx
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_51
# BB#48:                                #   in Loop: Header=BB16_47 Depth=1
	movq	24(%rdx), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB16_50
# BB#49:                                #   in Loop: Header=BB16_47 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB16_50:                              # %clause_LiteralIsEquality.exit228
                                        #   in Loop: Header=BB16_47 Depth=1
	cmpl	%eax, fol_EQUALITY(%rip)
	je	.LBB16_91
.LBB16_51:                              #   in Loop: Header=BB16_47 Depth=1
	movl	(%rdx), %eax
	testb	$4, %al
	jne	.LBB16_55
# BB#52:                                #   in Loop: Header=BB16_47 Depth=1
	testb	$2, 48(%r8)
	jne	.LBB16_91
# BB#53:                                #   in Loop: Header=BB16_47 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB16_55
# BB#54:                                #   in Loop: Header=BB16_47 Depth=1
	andl	$1, %eax
	je	.LBB16_91
	.p2align	4, 0x90
.LBB16_55:                              #   in Loop: Header=BB16_47 Depth=1
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	24(%rdx), %r14
	movl	fol_NOT(%rip), %eax
	cmpl	(%r14), %eax
	jne	.LBB16_57
# BB#56:                                #   in Loop: Header=BB16_47 Depth=1
	movq	16(%r14), %rax
	movq	8(%rax), %r14
.LBB16_57:                              # %clause_LiteralAtom.exit218
                                        #   in Loop: Header=BB16_47 Depth=1
	movslq	64(%r8), %rbp
	cmpl	60(%rsp), %ebp          # 4-byte Folded Reload
	jg	.LBB16_91
	.p2align	4, 0x90
.LBB16_58:                              #   Parent Loop BB16_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB16_76 Depth 3
                                        #       Child Loop BB16_88 Depth 3
	cmpl	%ebp, %r15d
	je	.LBB16_90
# BB#59:                                #   in Loop: Header=BB16_58 Depth=2
	movq	56(%r8), %rax
	movq	(%rax,%rbp,8), %rdx
	movq	24(%rdx), %rbx
	movl	(%rbx), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movq	%rbx, %rsi
	jne	.LBB16_61
# BB#60:                                #   in Loop: Header=BB16_58 Depth=2
	movq	16(%rbx), %rsi
	movq	8(%rsi), %rsi
.LBB16_61:                              # %clause_LiteralAtom.exit210
                                        #   in Loop: Header=BB16_58 Depth=2
	cmpq	%r15, %rbp
	jg	.LBB16_65
# BB#62:                                #   in Loop: Header=BB16_58 Depth=2
	movq	64(%rsp), %rdi          # 8-byte Reload
	testb	$4, (%rdi)
	jne	.LBB16_65
# BB#63:                                #   in Loop: Header=BB16_58 Depth=2
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB16_90
# BB#64:                                #   in Loop: Header=BB16_58 Depth=2
	testb	$1, (%rdx)
	jne	.LBB16_90
	.p2align	4, 0x90
.LBB16_65:                              #   in Loop: Header=BB16_58 Depth=2
	movl	(%r14), %edx
	cmpl	(%rsi), %edx
	jne	.LBB16_90
# BB#66:                                #   in Loop: Header=BB16_58 Depth=2
	cmpl	%eax, %ecx
	jne	.LBB16_68
# BB#67:                                #   in Loop: Header=BB16_58 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB16_68:                              # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB16_58 Depth=2
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB16_74
# BB#69:                                #   in Loop: Header=BB16_58 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	32(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB16_72
# BB#70:                                #   in Loop: Header=BB16_58 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	testb	$4, (%rax)
	jne	.LBB16_72
# BB#71:                                #   in Loop: Header=BB16_58 Depth=2
	movq	32(%rsp), %rcx
	movq	%r12, (%rsp)
	xorl	%r8d, %r8d
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	inf_LitMax
	testl	%eax, %eax
	je	.LBB16_73
.LBB16_72:                              #   in Loop: Header=BB16_58 Depth=2
	movq	32(%rsp), %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%r12, %r9
	callq	inf_ApplyGeneralFactoring
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB16_73:                              #   in Loop: Header=BB16_58 Depth=2
	movq	32(%rsp), %rdi
	callq	subst_Delete
.LBB16_74:                              #   in Loop: Header=BB16_58 Depth=2
	movq	16(%rsp), %r8           # 8-byte Reload
	xorps	%xmm0, %xmm0
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB16_77
# BB#75:                                # %.lr.ph.preheader.i198
                                        #   in Loop: Header=BB16_58 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB16_76:                              # %.lr.ph.i201
                                        #   Parent Loop BB16_47 Depth=1
                                        #     Parent Loop BB16_58 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB16_76
.LBB16_77:                              # %cont_Reset.exit202
                                        #   in Loop: Header=BB16_58 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%r14), %eax
	jne	.LBB16_86
# BB#78:                                #   in Loop: Header=BB16_58 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	16(%rbx), %rax
	movq	8(%rax), %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB16_85
# BB#79:                                #   in Loop: Header=BB16_58 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	16(%r14), %rax
	movq	8(%rax), %rsi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB16_85
# BB#80:                                #   in Loop: Header=BB16_58 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	leaq	32(%rsp), %rsi
	callq	subst_ExtractUnifierCom
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB16_83
# BB#81:                                #   in Loop: Header=BB16_58 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	testb	$4, (%rax)
	jne	.LBB16_83
# BB#82:                                #   in Loop: Header=BB16_58 Depth=2
	movq	32(%rsp), %rcx
	movq	%r12, (%rsp)
	xorl	%r8d, %r8d
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	inf_LitMax
	testl	%eax, %eax
	je	.LBB16_84
.LBB16_83:                              #   in Loop: Header=BB16_58 Depth=2
	movq	32(%rsp), %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%r15d, %esi
	movl	%ebp, %edx
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%r12, %r9
	callq	inf_ApplyGeneralFactoring
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB16_84:                              #   in Loop: Header=BB16_58 Depth=2
	movq	32(%rsp), %rdi
	callq	subst_Delete
.LBB16_85:                              #   in Loop: Header=BB16_58 Depth=2
	movq	16(%rsp), %r8           # 8-byte Reload
	xorps	%xmm0, %xmm0
.LBB16_86:                              #   in Loop: Header=BB16_58 Depth=2
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB16_89
# BB#87:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB16_58 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB16_88:                              # %.lr.ph.i
                                        #   Parent Loop BB16_47 Depth=1
                                        #     Parent Loop BB16_58 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB16_88
.LBB16_89:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB16_58 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
.LBB16_90:                              #   in Loop: Header=BB16_58 Depth=2
	cmpq	%r13, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB16_58
.LBB16_91:                              # %.loopexit
                                        #   in Loop: Header=BB16_47 Depth=1
	cmpq	%r13, %r15
	leaq	1(%r15), %r15
	jl	.LBB16_47
.LBB16_92:                              # %.loopexit271
	callq	cont_Check
	movq	40(%rsp), %rax          # 8-byte Reload
.LBB16_93:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	inf_GeneralFactoring, .Lfunc_end16-inf_GeneralFactoring
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_ApplyGeneralFactoring,@function
inf_ApplyGeneralFactoring:              # @inf_ApplyGeneralFactoring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi297:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi298:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi299:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi300:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi301:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi302:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi303:
	.cfi_def_cfa_offset 80
.Lcfi304:
	.cfi_offset %rbx, -56
.Lcfi305:
	.cfi_offset %r12, -48
.Lcfi306:
	.cfi_offset %r13, -40
.Lcfi307:
	.cfi_offset %r14, -32
.Lcfi308:
	.cfi_offset %r15, -24
.Lcfi309:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, %rbp
	movq	%rcx, %r14
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movl	%esi, %r15d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	callq	clause_Copy
	movq	%rax, %r13
	movq	%r13, %rbx
	movl	$0, 48(%r13)
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	clause_SubstApply
	movq	%r13, %rdi
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	movl	%r15d, %esi
	movq	%rbp, %r15
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	clause_DeleteLiteral
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.LBB17_2
	.p2align	4, 0x90
.LBB17_1:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB17_1
.LBB17_2:                               # %list_Delete.exit
	addq	$32, %r13
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB17_4
	.p2align	4, 0x90
.LBB17_3:                               # %.lr.ph.i29
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB17_3
.LBB17_4:                               # %list_Delete.exit30
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	movq	%rbx, %rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	movl	8(%rsp), %edx           # 4-byte Reload
	movq	%r15, %rcx
	movq	%r12, %r8
	callq	clause_SetDataFromFather
	movl	$14, 76(%rbx)
	movslq	(%rbp), %rbp
	movq	32(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%rbx)
	movslq	12(%rsp), %rbp          # 4-byte Folded Reload
	movq	40(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbx)
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%rbx)
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	inf_ApplyGeneralFactoring, .Lfunc_end17-inf_ApplyGeneralFactoring
	.cfi_endproc

	.globl	inf_GenSuperpositionLeft
	.p2align	4, 0x90
	.type	inf_GenSuperpositionLeft,@function
inf_GenSuperpositionLeft:               # @inf_GenSuperpositionLeft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi310:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi311:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi312:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi313:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi314:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi315:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi316:
	.cfi_def_cfa_offset 224
.Lcfi317:
	.cfi_offset %rbx, -56
.Lcfi318:
	.cfi_offset %r12, -48
.Lcfi319:
	.cfi_offset %r13, -40
.Lcfi320:
	.cfi_offset %r14, -32
.Lcfi321:
	.cfi_offset %r15, -24
.Lcfi322:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movl	%r8d, %r13d
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 16(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB18_25
# BB#1:
	movq	%rbx, %rdi
	callq	clause_Copy
	movq	%rax, %rdi
	testb	$2, 48(%rdi)
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	jne	.LBB18_26
# BB#2:
	movl	68(%rdi), %eax
	movl	72(%rdi), %ecx
	addl	64(%rdi), %eax
	leal	(%rax,%rcx), %edx
	testl	%r13d, %r13d
	setne	%bl
	cmpl	$1, %edx
	setne	%dl
	xorl	%r14d, %r14d
	testb	%bl, %dl
	jne	.LBB18_108
# BB#3:
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	movq	16(%rsp), %r10          # 8-byte Reload
	jg	.LBB18_27
# BB#4:                                 # %.lr.ph134
	testl	%r10d, %r10d
	movslq	%eax, %rbp
	movslq	%ecx, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	je	.LBB18_109
# BB#5:                                 # %.lr.ph134.split.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB18_6:                               # %.lr.ph134.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_11 Depth 2
                                        #     Child Loop BB18_20 Depth 2
	movq	56(%rdi), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	24(%rbx), %r12
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%r12), %eax
	jne	.LBB18_24
# BB#7:                                 #   in Loop: Header=BB18_6 Depth=1
	testb	$2, (%rbx)
	je	.LBB18_24
# BB#8:                                 #   in Loop: Header=BB18_6 Depth=1
	movq	16(%r12), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	8(%rcx), %rdx
	subq	$8, %rsp
.Lcfi323:
	.cfi_adjust_cfa_offset 8
	movl	%ebp, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	232(%rsp)
.Lcfi324:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi325:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi326:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenLitSPLeft
	addq	$32, %rsp
.Lcfi327:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB18_13
# BB#9:                                 #   in Loop: Header=BB18_6 Depth=1
	testq	%r14, %r14
	je	.LBB18_14
# BB#10:                                # %.preheader.i104.preheader
                                        #   in Loop: Header=BB18_6 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB18_11:                              # %.preheader.i104
                                        #   Parent Loop BB18_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB18_11
# BB#12:                                #   in Loop: Header=BB18_6 Depth=1
	movq	%r14, (%rax)
	jmp	.LBB18_14
.LBB18_13:                              #   in Loop: Header=BB18_6 Depth=1
	movq	%r14, %r15
.LBB18_14:                              # %list_Nconc.exit106
                                        #   in Loop: Header=BB18_6 Depth=1
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB18_17
# BB#15:                                #   in Loop: Header=BB18_6 Depth=1
	cmpl	$0, 8(%rbx)
	je	.LBB18_17
# BB#16:                                #   in Loop: Header=BB18_6 Depth=1
	movq	%r15, %r14
	movq	40(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB18_24
.LBB18_17:                              #   in Loop: Header=BB18_6 Depth=1
	movq	16(%r12), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	8(%rcx), %rsi
	subq	$8, %rsp
.Lcfi328:
	.cfi_adjust_cfa_offset 8
	movl	%ebp, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	232(%rsp)
.Lcfi329:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi330:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi331:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenLitSPLeft
	addq	$32, %rsp
.Lcfi332:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	testq	%rax, %rax
	je	.LBB18_22
# BB#18:                                #   in Loop: Header=BB18_6 Depth=1
	testq	%r15, %r15
	movq	40(%rsp), %rdx          # 8-byte Reload
	je	.LBB18_23
# BB#19:                                # %.preheader.i110.preheader
                                        #   in Loop: Header=BB18_6 Depth=1
	movq	%r14, %rcx
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB18_20:                              # %.preheader.i110
                                        #   Parent Loop BB18_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB18_20
# BB#21:                                #   in Loop: Header=BB18_6 Depth=1
	movq	%r15, (%rax)
	jmp	.LBB18_24
.LBB18_22:                              #   in Loop: Header=BB18_6 Depth=1
	movq	%r15, %r14
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB18_24
.LBB18_23:                              #   in Loop: Header=BB18_6 Depth=1
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB18_24:                              # %list_Nconc.exit112
                                        #   in Loop: Header=BB18_6 Depth=1
	cmpq	%rdx, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB18_6
	jmp	.LBB18_27
.LBB18_25:
	xorl	%r14d, %r14d
	jmp	.LBB18_107
.LBB18_26:
	xorl	%r14d, %r14d
	movq	16(%rsp), %r10          # 8-byte Reload
.LBB18_27:                              # %.loopexit123
	testb	$32, 48(%rdi)
	jne	.LBB18_106
.LBB18_28:                              # %.loopexit123
	movl	64(%rdi), %eax
	movl	68(%rdi), %ecx
	leal	-1(%rax,%rcx), %ecx
	cmpl	%ecx, %eax
	movq	%r14, %r15
	jg	.LBB18_105
# BB#29:                                # %.lr.ph
	movslq	%eax, %rbx
	movslq	%ecx, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	jmp	.LBB18_31
.LBB18_30:                              #   in Loop: Header=BB18_31 Depth=1
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rdx, %r14
	testq	%r15, %r15
	jne	.LBB18_100
	jmp	.LBB18_104
	.p2align	4, 0x90
.LBB18_31:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_47 Depth 2
                                        #       Child Loop BB18_48 Depth 3
                                        #       Child Loop BB18_52 Depth 3
                                        #         Child Loop BB18_53 Depth 4
                                        #           Child Loop BB18_58 Depth 5
                                        #             Child Loop BB18_59 Depth 6
                                        #             Child Loop BB18_76 Depth 6
                                        #     Child Loop BB18_44 Depth 2
                                        #     Child Loop BB18_101 Depth 2
	movq	56(%rdi), %rax
	movq	(%rax,%rbx,8), %rax
	movl	(%rax), %ecx
	testb	$4, %cl
	jne	.LBB18_36
# BB#32:                                #   in Loop: Header=BB18_31 Depth=1
	testb	$2, 48(%rdi)
	jne	.LBB18_103
# BB#33:                                #   in Loop: Header=BB18_31 Depth=1
	testl	%r10d, %r10d
	je	.LBB18_36
# BB#34:                                #   in Loop: Header=BB18_31 Depth=1
	andl	$1, %ecx
	je	.LBB18_103
	.p2align	4, 0x90
.LBB18_36:                              #   in Loop: Header=BB18_31 Depth=1
	movq	24(%rax), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB18_38
# BB#37:                                #   in Loop: Header=BB18_31 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
	movl	(%rcx), %eax
.LBB18_38:                              # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB18_31 Depth=1
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB18_46
# BB#39:                                # %list_Nconc.exit41.i
                                        #   in Loop: Header=BB18_31 Depth=1
	subq	$8, %rsp
.Lcfi333:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edx
	movl	%ebx, %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r10d, %r9d
	pushq	232(%rsp)
.Lcfi334:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi335:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi336:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenSPLeftEqToGiven
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	addq	$32, %rsp
.Lcfi337:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	testl	%r10d, %r10d
	je	.LBB18_41
# BB#40:                                #   in Loop: Header=BB18_31 Depth=1
	movq	56(%rdi), %rax
	movq	(%rax,%rbx,8), %rax
	cmpl	$0, 8(%rax)
	jne	.LBB18_98
.LBB18_41:                              #   in Loop: Header=BB18_31 Depth=1
	subq	$8, %rsp
.Lcfi338:
	.cfi_adjust_cfa_offset 8
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r10d, %r9d
	pushq	232(%rsp)
.Lcfi339:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi340:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi341:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenSPLeftEqToGiven
	addq	$32, %rsp
.Lcfi342:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.LBB18_97
# BB#42:                                #   in Loop: Header=BB18_31 Depth=1
	testq	%r14, %r14
	je	.LBB18_30
# BB#43:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB18_31 Depth=1
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB18_44:                              # %.preheader.i.i
                                        #   Parent Loop BB18_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB18_44
# BB#45:                                #   in Loop: Header=BB18_31 Depth=1
	movq	%r14, (%rax)
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rdx, %r14
	testq	%r14, %r14
	jne	.LBB18_99
	jmp	.LBB18_103
	.p2align	4, 0x90
.LBB18_46:                              #   in Loop: Header=BB18_31 Depth=1
	movl	stack_POINTER(%rip), %ebp
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	16(%rcx), %rdi
	callq	sharing_PushListOnStack
	xorl	%r14d, %r14d
.LBB18_47:                              # %.outer.i.i
                                        #   Parent Loop BB18_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_48 Depth 3
                                        #       Child Loop BB18_52 Depth 3
                                        #         Child Loop BB18_53 Depth 4
                                        #           Child Loop BB18_58 Depth 5
                                        #             Child Loop BB18_59 Depth 6
                                        #             Child Loop BB18_76 Depth 6
	movl	stack_POINTER(%rip), %eax
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB18_48:                              #   Parent Loop BB18_31 Depth=1
                                        #     Parent Loop BB18_47 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%eax, %ebp
	je	.LBB18_98
# BB#49:                                #   in Loop: Header=BB18_48 Depth=3
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %r12
	cmpl	$0, (%r12)
	jg	.LBB18_48
# BB#50:                                #   in Loop: Header=BB18_47 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%r12, %rcx
	callq	st_GetUnifier
	testq	%rax, %rax
	je	.LBB18_47
# BB#51:                                # %.lr.ph161.i.i.preheader
                                        #   in Loop: Header=BB18_47 Depth=2
	movq	%r12, 112(%rsp)         # 8-byte Spill
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movq	%r15, 120(%rsp)         # 8-byte Spill
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movl	%ebp, 108(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB18_52:                              # %.lr.ph161.i.i
                                        #   Parent Loop BB18_31 Depth=1
                                        #     Parent Loop BB18_47 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB18_53 Depth 4
                                        #           Child Loop BB18_58 Depth 5
                                        #             Child Loop BB18_59 Depth 6
                                        #             Child Loop BB18_76 Depth 6
	movq	8(%rax), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%rax, 128(%rsp)         # 8-byte Spill
	je	.LBB18_95
	.p2align	4, 0x90
.LBB18_53:                              # %.lr.ph156.i.i
                                        #   Parent Loop BB18_31 Depth=1
                                        #     Parent Loop BB18_47 Depth=2
                                        #       Parent Loop BB18_52 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB18_58 Depth 5
                                        #             Child Loop BB18_59 Depth 6
                                        #             Child Loop BB18_76 Depth 6
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rdi
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB18_94
# BB#54:                                #   in Loop: Header=BB18_53 Depth=4
	callq	sharing_NAtomDataList
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB18_94
# BB#55:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB18_53 Depth=4
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	.p2align	4, 0x90
.LBB18_58:                              # %.lr.ph.i.i
                                        #   Parent Loop BB18_31 Depth=1
                                        #     Parent Loop BB18_47 Depth=2
                                        #       Parent Loop BB18_52 Depth=3
                                        #         Parent Loop BB18_53 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB18_59 Depth 6
                                        #             Child Loop BB18_76 Depth 6
	movq	8(%r14), %rbx
	movq	16(%rbx), %rbp
	movq	56(%rbp), %rax
	movl	$-1, %r12d
	.p2align	4, 0x90
.LBB18_59:                              #   Parent Loop BB18_31 Depth=1
                                        #     Parent Loop BB18_47 Depth=2
                                        #       Parent Loop BB18_52 Depth=3
                                        #         Parent Loop BB18_53 Depth=4
                                        #           Parent Loop BB18_58 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	incl	%r12d
	cmpq	%rbx, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB18_59
# BB#60:                                # %clause_LiteralGetIndex.exit.i.i
                                        #   in Loop: Header=BB18_58 Depth=5
	movq	24(%rbx), %r15
	movl	(%r15), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	jne	.LBB18_62
# BB#61:                                #   in Loop: Header=BB18_58 Depth=5
	movq	16(%r15), %rdx
	movq	8(%rdx), %r15
.LBB18_62:                              # %clause_LiteralAtom.exit.i.i
                                        #   in Loop: Header=BB18_58 Depth=5
	testb	$2, 48(%rbp)
	jne	.LBB18_93
# BB#63:                                #   in Loop: Header=BB18_58 Depth=5
	testl	%esi, %esi
	je	.LBB18_65
# BB#64:                                #   in Loop: Header=BB18_58 Depth=5
	testb	$2, (%rbx)
	je	.LBB18_93
.LBB18_65:                              #   in Loop: Header=BB18_58 Depth=5
	testl	%r9d, %r9d
	je	.LBB18_69
# BB#66:                                #   in Loop: Header=BB18_58 Depth=5
	movq	16(%r15), %rdx
	movq	40(%rsp), %rdi          # 8-byte Reload
	cmpq	8(%rdx), %rdi
	je	.LBB18_69
# BB#67:                                #   in Loop: Header=BB18_58 Depth=5
	cmpl	%eax, %ecx
	je	.LBB18_93
# BB#68:                                #   in Loop: Header=BB18_58 Depth=5
	movl	8(%rbx), %eax
	testl	%eax, %eax
	je	.LBB18_70
	jmp	.LBB18_93
	.p2align	4, 0x90
.LBB18_69:                              #   in Loop: Header=BB18_58 Depth=5
	cmpl	%eax, %ecx
	je	.LBB18_93
.LBB18_70:                              #   in Loop: Header=BB18_58 Depth=5
	movl	(%rbp), %eax
	cmpl	(%r8), %eax
	je	.LBB18_93
# BB#71:                                #   in Loop: Header=BB18_58 Depth=5
	testl	%r13d, %r13d
	je	.LBB18_73
# BB#72:                                #   in Loop: Header=BB18_58 Depth=5
	movl	68(%rbp), %eax
	addl	64(%rbp), %eax
	addl	72(%rbp), %eax
	cmpl	$1, %eax
	jne	.LBB18_93
.LBB18_73:                              #   in Loop: Header=BB18_58 Depth=5
	movq	%rbp, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB18_92
# BB#74:                                #   in Loop: Header=BB18_58 Depth=5
	movl	52(%rbp), %esi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	80(%rsp), %rsi
	leaq	64(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB18_77
# BB#75:                                # %.lr.ph.preheader.i.i.i
                                        #   in Loop: Header=BB18_58 Depth=5
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB18_76:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB18_31 Depth=1
                                        #     Parent Loop BB18_47 Depth=2
                                        #       Parent Loop BB18_52 Depth=3
                                        #         Parent Loop BB18_53 Depth=4
                                        #           Parent Loop BB18_58 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB18_76
.LBB18_77:                              # %cont_Reset.exit.i.i
                                        #   in Loop: Header=BB18_58 Depth=5
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB18_82
# BB#78:                                #   in Loop: Header=BB18_58 Depth=5
	movq	64(%rsp), %r13
	movq	8(%rsp), %rax           # 8-byte Reload
	testb	$2, 48(%rax)
	jne	.LBB18_80
# BB#79:                                #   in Loop: Header=BB18_58 Depth=5
	movq	80(%rsp), %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	64(%rdi), %eax
	movl	68(%rdi), %edx
	leal	-1(%rax,%rdx), %eax
	cltq
	xorl	%r8d, %r8d
	movq	88(%rsp), %rsi          # 8-byte Reload
	cmpq	%rsi, %rax
	setl	%r8b
	subq	$8, %rsp
.Lcfi343:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	40(%rsp), %r9           # 8-byte Reload
	pushq	232(%rsp)
.Lcfi344:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	addq	$16, %rsp
.Lcfi345:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB18_57
.LBB18_80:                              #   in Loop: Header=BB18_58 Depth=5
	testb	$2, 48(%rbp)
	jne	.LBB18_82
# BB#81:                                #   in Loop: Header=BB18_58 Depth=5
	movl	64(%rbp), %eax
	movl	68(%rbp), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	cmpl	%r12d, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi346:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r13, %rcx
	movq	40(%rsp), %r9           # 8-byte Reload
	pushq	232(%rsp)
.Lcfi347:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	addq	$16, %rsp
.Lcfi348:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB18_57
.LBB18_82:                              # %inf_LiteralsMax.exit.i.i
                                        #   in Loop: Header=BB18_58 Depth=5
	movq	64(%rsp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	callq	term_Copy
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	16(%r15), %rax
	movq	8(%rax), %rdi
	movq	64(%rsp), %r15
	cmpq	%rdi, %r13
	jne	.LBB18_84
# BB#83:                                #   in Loop: Header=BB18_58 Depth=5
	movq	(%rax), %rax
	movq	8(%rax), %rdi
.LBB18_84:                              #   in Loop: Header=BB18_58 Depth=5
	callq	term_Copy
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	152(%rsp), %r15         # 8-byte Reload
	je	.LBB18_87
# BB#85:                                #   in Loop: Header=BB18_58 Depth=5
	cmpl	$0, 8(%rbx)
	jne	.LBB18_87
# BB#86:                                #   in Loop: Header=BB18_58 Depth=5
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	224(%rsp), %rcx
	callq	ord_Compare
	cmpl	$1, %eax
	je	.LBB18_90
.LBB18_87:                              #   in Loop: Header=BB18_58 Depth=5
	movq	80(%rsp), %rbx
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	term_Copy
	movq	%rax, %r13
	movq	%r13, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rcx
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	jne	.LBB18_89
# BB#88:                                #   in Loop: Header=BB18_58 Depth=5
	movq	%r13, %rdi
	callq	term_Delete
	xorl	%r13d, %r13d
.LBB18_89:                              # %inf_AllTermsRplac.exit.i.i
                                        #   in Loop: Header=BB18_58 Depth=5
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	64(%rsp), %rdx
	movq	80(%rsp), %r9
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	88(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	pushq	224(%rsp)
.Lcfi349:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi350:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi351:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi352:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi353:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi354:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyGenSuperposition
	addq	$48, %rsp
.Lcfi355:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 96(%rsp)          # 8-byte Spill
.LBB18_90:                              #   in Loop: Header=BB18_58 Depth=5
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	%r15, %rdi
	callq	term_Delete
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	jmp	.LBB18_91
.LBB18_57:                              #   in Loop: Header=BB18_58 Depth=5
	movq	72(%rsp), %r13          # 8-byte Reload
.LBB18_91:                              # %inf_LiteralsMax.exit.thread.i.i
                                        #   in Loop: Header=BB18_58 Depth=5
	movq	80(%rsp), %rdi
	callq	subst_Delete
	movq	64(%rsp), %rdi
	callq	subst_Delete
.LBB18_92:                              #   in Loop: Header=BB18_58 Depth=5
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	.p2align	4, 0x90
.LBB18_93:                              #   in Loop: Header=BB18_58 Depth=5
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB18_58
.LBB18_94:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB18_53 Depth=4
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB18_53
.LBB18_95:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB18_52 Depth=3
	movq	128(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	120(%rsp), %r15         # 8-byte Reload
	movq	88(%rsp), %rbx          # 8-byte Reload
	movq	96(%rsp), %r14          # 8-byte Reload
	movl	108(%rsp), %ebp         # 4-byte Reload
	jne	.LBB18_52
	jmp	.LBB18_47
.LBB18_97:                              #   in Loop: Header=BB18_31 Depth=1
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB18_98:                              # %inf_GenSPLeftToGiven.exit
                                        #   in Loop: Header=BB18_31 Depth=1
	testq	%r14, %r14
	je	.LBB18_103
.LBB18_99:                              # %inf_GenSPLeftToGiven.exit.thread
                                        #   in Loop: Header=BB18_31 Depth=1
	testq	%r15, %r15
	je	.LBB18_104
.LBB18_100:                             # %.preheader.i.preheader
                                        #   in Loop: Header=BB18_31 Depth=1
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB18_101:                             # %.preheader.i
                                        #   Parent Loop BB18_31 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB18_101
# BB#102:                               #   in Loop: Header=BB18_31 Depth=1
	movq	%r15, (%rax)
	jmp	.LBB18_104
	.p2align	4, 0x90
.LBB18_103:                             #   in Loop: Header=BB18_31 Depth=1
	movq	%r15, %r14
.LBB18_104:                             # %list_Nconc.exit
                                        #   in Loop: Header=BB18_31 Depth=1
	cmpq	136(%rsp), %rbx         # 8-byte Folded Reload
	leaq	1(%rbx), %rbx
	movq	%r14, %r15
	jl	.LBB18_31
	jmp	.LBB18_106
.LBB18_105:
	movq	%r15, %r14
.LBB18_106:                             # %.loopexit
	callq	clause_Delete
.LBB18_107:
	movq	%r14, %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_108:
	movq	16(%rsp), %r10          # 8-byte Reload
	testb	$32, 48(%rdi)
	je	.LBB18_28
	jmp	.LBB18_106
.LBB18_109:                             # %.lr.ph134.split.us.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB18_110:                             # %.lr.ph134.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_114 Depth 2
                                        #     Child Loop BB18_123 Depth 2
	movq	56(%rdi), %rax
	movq	(%rax,%rbp,8), %r12
	movq	24(%r12), %rbx
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB18_127
# BB#111:                               #   in Loop: Header=BB18_110 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	8(%rcx), %rdx
	subq	$8, %rsp
.Lcfi356:
	.cfi_adjust_cfa_offset 8
	movl	%ebp, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	232(%rsp)
.Lcfi357:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi358:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi359:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenLitSPLeft
	addq	$32, %rsp
.Lcfi360:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB18_116
# BB#112:                               #   in Loop: Header=BB18_110 Depth=1
	testq	%r14, %r14
	je	.LBB18_117
# BB#113:                               # %.preheader.i104.us.preheader
                                        #   in Loop: Header=BB18_110 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB18_114:                             # %.preheader.i104.us
                                        #   Parent Loop BB18_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB18_114
# BB#115:                               #   in Loop: Header=BB18_110 Depth=1
	movq	%r14, (%rax)
	jmp	.LBB18_117
.LBB18_116:                             #   in Loop: Header=BB18_110 Depth=1
	movq	%r14, %r15
.LBB18_117:                             # %list_Nconc.exit106.us
                                        #   in Loop: Header=BB18_110 Depth=1
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	testl	%r9d, %r9d
	je	.LBB18_120
# BB#118:                               #   in Loop: Header=BB18_110 Depth=1
	cmpl	$0, 8(%r12)
	je	.LBB18_120
# BB#119:                               #   in Loop: Header=BB18_110 Depth=1
	movq	%r15, %r14
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB18_127
	.p2align	4, 0x90
.LBB18_120:                             #   in Loop: Header=BB18_110 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	8(%rcx), %rsi
	subq	$8, %rsp
.Lcfi361:
	.cfi_adjust_cfa_offset 8
	movl	%ebp, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	232(%rsp)
.Lcfi362:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi363:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi364:
	.cfi_adjust_cfa_offset 8
	callq	inf_GenLitSPLeft
	addq	$32, %rsp
.Lcfi365:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	testq	%rax, %rax
	je	.LBB18_125
# BB#121:                               #   in Loop: Header=BB18_110 Depth=1
	testq	%r15, %r15
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	je	.LBB18_126
# BB#122:                               # %.preheader.i110.us.preheader
                                        #   in Loop: Header=BB18_110 Depth=1
	movq	%r14, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB18_123:                             # %.preheader.i110.us
                                        #   Parent Loop BB18_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB18_123
# BB#124:                               #   in Loop: Header=BB18_110 Depth=1
	movq	%r15, (%rax)
	jmp	.LBB18_127
.LBB18_125:                             #   in Loop: Header=BB18_110 Depth=1
	movq	%r15, %r14
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB18_127
.LBB18_126:                             #   in Loop: Header=BB18_110 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	.p2align	4, 0x90
.LBB18_127:                             # %list_Nconc.exit112.us
                                        #   in Loop: Header=BB18_110 Depth=1
	cmpq	%rdx, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB18_110
	jmp	.LBB18_27
.Lfunc_end18:
	.size	inf_GenSuperpositionLeft, .Lfunc_end18-inf_GenSuperpositionLeft
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_GenLitSPLeft,@function
inf_GenLitSPLeft:                       # @inf_GenLitSPLeft
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi366:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi367:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi368:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi369:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi370:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi371:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi372:
	.cfi_def_cfa_offset 208
.Lcfi373:
	.cfi_offset %rbx, -56
.Lcfi374:
	.cfi_offset %r12, -48
.Lcfi375:
	.cfi_offset %r13, -40
.Lcfi376:
	.cfi_offset %r14, -32
.Lcfi377:
	.cfi_offset %r15, -24
.Lcfi378:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movl	%ecx, %ebp
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movq	%rsi, %rax
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r8, 120(%rsp)          # 8-byte Spill
	movq	(%r8), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	callq	st_GetUnifier
	testq	%rax, %rax
	je	.LBB19_1
# BB#2:                                 # %.lr.ph189
	movl	%ebp, (%rsp)            # 4-byte Spill
	movslq	%ebp, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	testl	%ebx, %ebx
	sete	7(%rsp)                 # 1-byte Folded Spill
	xorl	%ecx, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB19_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_7 Depth 2
                                        #       Child Loop BB19_10 Depth 3
                                        #       Child Loop BB19_20 Depth 3
	movq	8(%rax), %rdi
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	movq	%rax, 144(%rsp)         # 8-byte Spill
	jg	.LBB19_61
# BB#4:                                 #   in Loop: Header=BB19_3 Depth=1
	jns	.LBB19_6
# BB#5:                                 # %symbol_IsPredicate.exit
                                        #   in Loop: Header=BB19_3 Depth=1
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	je	.LBB19_61
.LBB19_6:                               # %symbol_IsPredicate.exit.thread
                                        #   in Loop: Header=BB19_3 Depth=1
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	120(%rsp), %rsi         # 8-byte Reload
	callq	sharing_GetDataList
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB19_7
	jmp	.LBB19_61
.LBB19_44:                              #   in Loop: Header=BB19_7 Depth=2
	movq	16(%r15), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r14
	movq	8(%rsp), %rbx
	movq	16(%r15), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	216(%rsp), %rdx
	movq	224(%rsp), %rcx
	callq	ord_Compare
	cmpl	$3, %eax
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	je	.LBB19_49
# BB#45:                                #   in Loop: Header=BB19_7 Depth=2
	cmpl	$1, %eax
	jne	.LBB19_51
# BB#46:                                #   in Loop: Header=BB19_7 Depth=2
	movq	8(%rsp), %rbx
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %r15
	movq	16(%r15), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rdi
	jmp	.LBB19_47
.LBB19_32:                              #   in Loop: Header=BB19_7 Depth=2
	xorl	%esi, %esi
	movq	112(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB19_33
.LBB19_49:                              #   in Loop: Header=BB19_7 Depth=2
	movq	8(%rsp), %rbx
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %r15
	movq	16(%r15), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
.LBB19_47:                              #   in Loop: Header=BB19_7 Depth=2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rbx, %rcx
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	movl	(%rsp), %ebx            # 4-byte Reload
	je	.LBB19_50
# BB#48:                                #   in Loop: Header=BB19_7 Depth=2
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movq	96(%rsp), %rcx          # 8-byte Reload
	callq	inf_NAllTermsRplac
	jmp	.LBB19_54
.LBB19_51:                              #   in Loop: Header=BB19_7 Depth=2
	movq	8(%rsp), %rbx
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %r15
	movq	%r15, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	jne	.LBB19_53
# BB#52:                                #   in Loop: Header=BB19_7 Depth=2
	movq	%r15, %rdi
	callq	term_Delete
	xorl	%r15d, %r15d
.LBB19_53:                              # %inf_AllTermsRightRplac.exit
                                        #   in Loop: Header=BB19_7 Depth=2
	movl	(%rsp), %ebx            # 4-byte Reload
	jmp	.LBB19_54
.LBB19_50:                              #   in Loop: Header=BB19_7 Depth=2
	movq	%r15, %rdi
	callq	term_Delete
	xorl	%r15d, %r15d
.LBB19_54:                              # %inf_AllTermsRightRplac.exit
                                        #   in Loop: Header=BB19_7 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	term_Delete
	movq	72(%rsp), %rax          # 8-byte Reload
	testq	%r15, %r15
	jne	.LBB19_40
	jmp	.LBB19_56
	.p2align	4, 0x90
.LBB19_7:                               # %.lr.ph
                                        #   Parent Loop BB19_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_10 Depth 3
                                        #       Child Loop BB19_20 Depth 3
	movq	8(%r12), %r14
	movq	24(%r14), %r15
	movl	(%r15), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	jne	.LBB19_9
# BB#8:                                 #   in Loop: Header=BB19_7 Depth=2
	movq	16(%r15), %rdx
	movq	8(%rdx), %r15
.LBB19_9:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB19_7 Depth=2
	movq	16(%r14), %r13
	movq	56(%r13), %rdx
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB19_10:                              #   Parent Loop BB19_3 Depth=1
                                        #     Parent Loop BB19_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ebp
	cmpq	%r14, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB19_10
# BB#11:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB19_7 Depth=2
	movl	(%r14), %edx
	testb	$4, %dl
	jne	.LBB19_15
# BB#12:                                #   in Loop: Header=BB19_7 Depth=2
	testb	$2, 48(%r13)
	jne	.LBB19_60
# BB#13:                                #   in Loop: Header=BB19_7 Depth=2
	movl	208(%rsp), %esi
	testl	%esi, %esi
	setne	%bl
	testb	$1, %dl
	sete	%dl
	cmpl	%eax, %ecx
	setne	%al
	testb	%dl, %bl
	jne	.LBB19_60
# BB#14:                                #   in Loop: Header=BB19_7 Depth=2
	testb	%al, %al
	je	.LBB19_16
	jmp	.LBB19_60
	.p2align	4, 0x90
.LBB19_15:                              #   in Loop: Header=BB19_7 Depth=2
	cmpl	%eax, %ecx
	jne	.LBB19_60
.LBB19_16:                              #   in Loop: Header=BB19_7 Depth=2
	testb	$32, 48(%r13)
	jne	.LBB19_60
# BB#17:                                #   in Loop: Header=BB19_7 Depth=2
	movq	%r13, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB19_60
# BB#18:                                #   in Loop: Header=BB19_7 Depth=2
	movl	52(%r13), %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	56(%rsp), %rsi
	leaq	8(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB19_21
# BB#19:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB19_7 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB19_20:                              # %.lr.ph.i
                                        #   Parent Loop BB19_3 Depth=1
                                        #     Parent Loop BB19_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB19_20
.LBB19_21:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB19_7 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movl	208(%rsp), %eax
	testl	%eax, %eax
	je	.LBB19_26
# BB#22:                                #   in Loop: Header=BB19_7 Depth=2
	movq	8(%rsp), %rbx
	movq	48(%rsp), %rax          # 8-byte Reload
	testb	$2, 48(%rax)
	jne	.LBB19_24
# BB#23:                                #   in Loop: Header=BB19_7 Depth=2
	movq	56(%rsp), %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	64(%rdi), %eax
	movl	68(%rdi), %edx
	leal	-1(%rax,%rdx), %eax
	xorl	%r8d, %r8d
	movl	(%rsp), %esi            # 4-byte Reload
	cmpl	%esi, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi379:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	224(%rsp), %r9
	pushq	232(%rsp)
.Lcfi380:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	addq	$16, %rsp
.Lcfi381:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB19_59
.LBB19_24:                              #   in Loop: Header=BB19_7 Depth=2
	testb	$2, 48(%r13)
	jne	.LBB19_26
# BB#25:                                #   in Loop: Header=BB19_7 Depth=2
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	cmpl	%ebp, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi382:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	%r13, %rdi
	movl	%ebp, %esi
	movq	%rbx, %rcx
	movq	224(%rsp), %r9
	pushq	232(%rsp)
.Lcfi383:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	addq	$16, %rsp
.Lcfi384:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB19_59
.LBB19_26:                              # %inf_LiteralsMax.exit
                                        #   in Loop: Header=BB19_7 Depth=2
	movq	56(%rsp), %rbx
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movb	$1, %sil
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	je	.LBB19_27
# BB#28:                                #   in Loop: Header=BB19_7 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	cmpl	$0, 8(%rax)
	movq	80(%rsp), %rdx          # 8-byte Reload
	jne	.LBB19_33
# BB#29:                                #   in Loop: Header=BB19_7 Depth=2
	movq	56(%rsp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	term_Copy
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdx
	cmpq	%rbx, %rdx
	sete	%sil
	movl	%esi, %eax
	orb	7(%rsp), %al            # 1-byte Folded Reload
	jne	.LBB19_33
# BB#30:                                #   in Loop: Header=BB19_7 Depth=2
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%rdx, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	216(%rsp), %rdx
	movq	224(%rsp), %rcx
	callq	ord_Compare
	cmpl	$1, %eax
	jne	.LBB19_32
# BB#31:                                #   in Loop: Header=BB19_7 Depth=2
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB19_57
.LBB19_27:                              #   in Loop: Header=BB19_7 Depth=2
	movq	80(%rsp), %rdx          # 8-byte Reload
.LBB19_33:                              # %.thread
                                        #   in Loop: Header=BB19_7 Depth=2
	movl	208(%rsp), %eax
	testl	%eax, %eax
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movl	%esi, 40(%rsp)          # 4-byte Spill
	je	.LBB19_37
# BB#34:                                #   in Loop: Header=BB19_7 Depth=2
	movq	24(%r14), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB19_36
# BB#35:                                #   in Loop: Header=BB19_7 Depth=2
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB19_36:                              # %clause_LiteralIsPredicate.exit
                                        #   in Loop: Header=BB19_7 Depth=2
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB19_37
# BB#41:                                #   in Loop: Header=BB19_7 Depth=2
	cmpl	$0, 8(%r14)
	movq	8(%rsp), %rbx
	je	.LBB19_44
# BB#42:                                #   in Loop: Header=BB19_7 Depth=2
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %r15
	movq	16(%r15), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rcx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	je	.LBB19_55
# BB#43:                                #   in Loop: Header=BB19_7 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	callq	inf_NAllTermsRplac
	jmp	.LBB19_38
.LBB19_37:                              #   in Loop: Header=BB19_7 Depth=2
	movq	8(%rsp), %rbx
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %r15
	movq	%r15, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	je	.LBB19_55
.LBB19_38:                              #   in Loop: Header=BB19_7 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rsp), %ebx            # 4-byte Reload
	testq	%r15, %r15
	je	.LBB19_56
.LBB19_40:                              #   in Loop: Header=BB19_7 Depth=2
	movq	56(%rsp), %rdx
	movq	8(%rsp), %r9
	movq	48(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movq	%r13, %rcx
	movl	%ebp, %r8d
	pushq	224(%rsp)
.Lcfi385:
	.cfi_adjust_cfa_offset 8
	pushq	224(%rsp)
.Lcfi386:
	.cfi_adjust_cfa_offset 8
	movl	224(%rsp), %ebp
	pushq	%rbp
.Lcfi387:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi388:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi389:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi390:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyGenSuperposition
	addq	$48, %rsp
.Lcfi391:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	cmpb	$0, 40(%rsp)            # 1-byte Folded Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	je	.LBB19_57
	jmp	.LBB19_58
.LBB19_55:                              #   in Loop: Header=BB19_7 Depth=2
	movq	%r15, %rdi
	callq	term_Delete
.LBB19_56:                              #   in Loop: Header=BB19_7 Depth=2
	cmpb	$0, 40(%rsp)            # 1-byte Folded Reload
	jne	.LBB19_58
.LBB19_57:                              # %.thread191
                                        #   in Loop: Header=BB19_7 Depth=2
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	term_Delete
.LBB19_58:                              #   in Loop: Header=BB19_7 Depth=2
	movq	%r14, %rdi
	callq	term_Delete
.LBB19_59:                              # %inf_LiteralsMax.exit.thread
                                        #   in Loop: Header=BB19_7 Depth=2
	movq	56(%rsp), %rdi
	callq	subst_Delete
	movq	8(%rsp), %rdi
	callq	subst_Delete
	.p2align	4, 0x90
.LBB19_60:                              #   in Loop: Header=BB19_7 Depth=2
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB19_7
	.p2align	4, 0x90
.LBB19_61:                              # %.loopexit
                                        #   in Loop: Header=BB19_3 Depth=1
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	jne	.LBB19_3
	jmp	.LBB19_62
.LBB19_1:
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
.LBB19_62:                              # %._crit_edge
	movq	64(%rsp), %rax          # 8-byte Reload
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	inf_GenLitSPLeft, .Lfunc_end19-inf_GenLitSPLeft
	.cfi_endproc

	.globl	inf_ApplyDefinition
	.p2align	4, 0x90
	.type	inf_ApplyDefinition,@function
inf_ApplyDefinition:                    # @inf_ApplyDefinition
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi392:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi393:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi394:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi395:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi396:
	.cfi_def_cfa_offset 48
.Lcfi397:
	.cfi_offset %rbx, -48
.Lcfi398:
	.cfi_offset %r12, -40
.Lcfi399:
	.cfi_offset %r13, -32
.Lcfi400:
	.cfi_offset %r14, -24
.Lcfi401:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB20_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB20_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_7 Depth 2
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	def_ApplyDefToClauseOnce
	testq	%rax, %rax
	je	.LBB20_4
# BB#5:                                 #   in Loop: Header=BB20_3 Depth=1
	testq	%r13, %r13
	je	.LBB20_9
# BB#6:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB20_7:                               # %.preheader.i
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB20_7
# BB#8:                                 #   in Loop: Header=BB20_3 Depth=1
	movq	%r13, (%rcx)
	jmp	.LBB20_9
	.p2align	4, 0x90
.LBB20_4:                               #   in Loop: Header=BB20_3 Depth=1
	movq	%r13, %rax
.LBB20_9:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rax, %r13
	jne	.LBB20_3
	jmp	.LBB20_10
.LBB20_1:
	xorl	%eax, %eax
.LBB20_10:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	inf_ApplyDefinition, .Lfunc_end20-inf_ApplyDefinition
	.cfi_endproc

	.globl	inf_GeneralHyperResolution
	.p2align	4, 0x90
	.type	inf_GeneralHyperResolution,@function
inf_GeneralHyperResolution:             # @inf_GeneralHyperResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi402:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi403:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi404:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi405:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi406:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi407:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi408:
	.cfi_def_cfa_offset 224
.Lcfi409:
	.cfi_offset %rbx, -56
.Lcfi410:
	.cfi_offset %r12, -48
.Lcfi411:
	.cfi_offset %r13, -40
.Lcfi412:
	.cfi_offset %r14, -32
.Lcfi413:
	.cfi_offset %r15, -24
.Lcfi414:
	.cfi_offset %rbp, -16
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB21_4
# BB#1:
	movl	68(%r14), %eax
	testl	%eax, %eax
	je	.LBB21_5
# BB#2:
	movslq	64(%r14), %rbx
	leal	-1(%rax,%rbx), %eax
	cmpl	%eax, %ebx
	jle	.LBB21_6
# BB#3:
	xorl	%ebp, %ebp
	jmp	.LBB21_8
.LBB21_4:
	xorl	%eax, %eax
	jmp	.LBB21_65
.LBB21_5:
	xorl	%eax, %eax
	cmpl	$0, 72(%r14)
	jne	.LBB21_12
	jmp	.LBB21_65
.LBB21_6:                               # %.lr.ph.i16.i
	movslq	%eax, %r15
	decq	%rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB21_7:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	8(%rax,%rbx,8), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r13, 8(%rbp)
	movq	%r12, (%rbp)
	incq	%rbx
	cmpq	%r15, %rbx
	movq	%rbp, %r12
	jl	.LBB21_7
.LBB21_8:                               # %inf_GetAntecedentLiterals.exit.i24
	movl	52(%r14), %ecx
	subq	$8, %rsp
.Lcfi415:
	.cfi_adjust_cfa_offset 8
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	%rbp, %rdx
	movl	16(%rsp), %r9d          # 4-byte Reload
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi416:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi417:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi418:
	.cfi_adjust_cfa_offset 8
	callq	inf_HyperResolvents
	addq	$32, %rsp
.Lcfi419:
	.cfi_adjust_cfa_offset -32
	testq	%rbp, %rbp
	je	.LBB21_10
	.p2align	4, 0x90
.LBB21_9:                               # %.lr.ph.i.i28
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rbp
	jne	.LBB21_9
.LBB21_10:                              # %inf_ForwardHyperResolution.exit
	cmpl	$0, 68(%r14)
	jne	.LBB21_65
# BB#11:                                # %inf_ForwardHyperResolution.exit.thread
	cmpl	$0, 72(%r14)
	je	.LBB21_65
.LBB21_12:
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	callq	clause_Copy
	movq	%rax, %r15
	movl	64(%r15), %eax
	movl	72(%r15), %ecx
	addl	68(%r15), %eax
	leal	-1(%rcx,%rax), %eax
	movslq	68(%r14), %rcx
	movslq	64(%r14), %rdx
	addq	%rcx, %rdx
	cmpl	%eax, %edx
	jle	.LBB21_15
# BB#13:                                # %inf_BackwardHyperResolution.exit.thread33
	movq	%r15, %rdi
	callq	clause_Delete
	jmp	.LBB21_14
.LBB21_15:                              # %.lr.ph142.i
	cltq
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	movq	%r15, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB21_16:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_25 Depth 2
                                        #       Child Loop BB21_26 Depth 3
                                        #         Child Loop BB21_28 Depth 4
                                        #           Child Loop BB21_31 Depth 5
                                        #           Child Loop BB21_40 Depth 5
                                        #           Child Loop BB21_44 Depth 5
                                        #           Child Loop BB21_48 Depth 5
                                        #           Child Loop BB21_52 Depth 5
	movq	56(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB21_18
# BB#17:                                #   in Loop: Header=BB21_16 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB21_18:                              # %clause_LiteralAtom.exit127.i
                                        #   in Loop: Header=BB21_16 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB21_20
# BB#19:                                #   in Loop: Header=BB21_16 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	testb	$2, (%rax)
	je	.LBB21_58
.LBB21_20:                              # %.preheader.i13
                                        #   in Loop: Header=BB21_16 Depth=1
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB21_25
	.p2align	4, 0x90
.LBB21_21:                              #   in Loop: Header=BB21_25 Depth=2
	movq	%r12, %r14
.LBB21_22:                              # %._crit_edge.i
                                        #   in Loop: Header=BB21_25 Depth=2
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB21_57
# BB#23:                                #   in Loop: Header=BB21_25 Depth=2
	movl	fol_EQUALITY(%rip), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx), %eax
	jne	.LBB21_57
# BB#24:                                #   in Loop: Header=BB21_25 Depth=2
	movq	16(%rcx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	16(%rcx), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	movq	%r14, %r12
.LBB21_25:                              #   Parent Loop BB21_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB21_26 Depth 3
                                        #         Child Loop BB21_28 Depth 4
                                        #           Child Loop BB21_31 Depth 5
                                        #           Child Loop BB21_40 Depth 5
                                        #           Child Loop BB21_44 Depth 5
                                        #           Child Loop BB21_48 Depth 5
                                        #           Child Loop BB21_52 Depth 5
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	callq	st_GetUnifier
	testq	%rax, %rax
	je	.LBB21_21
	.p2align	4, 0x90
.LBB21_26:                              # %.lr.ph137.i
                                        #   Parent Loop BB21_16 Depth=1
                                        #     Parent Loop BB21_25 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB21_28 Depth 4
                                        #           Child Loop BB21_31 Depth 5
                                        #           Child Loop BB21_40 Depth 5
                                        #           Child Loop BB21_44 Depth 5
                                        #           Child Loop BB21_48 Depth 5
                                        #           Child Loop BB21_52 Depth 5
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	8(%rax), %rdi
	cmpl	$0, (%rdi)
	jle	.LBB21_27
.LBB21_55:                              #   in Loop: Header=BB21_26 Depth=3
	movq	%r12, %r14
	jmp	.LBB21_56
	.p2align	4, 0x90
.LBB21_27:                              #   in Loop: Header=BB21_26 Depth=3
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB21_55
	.p2align	4, 0x90
.LBB21_28:                              # %.lr.ph.i
                                        #   Parent Loop BB21_16 Depth=1
                                        #     Parent Loop BB21_25 Depth=2
                                        #       Parent Loop BB21_26 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB21_31 Depth 5
                                        #           Child Loop BB21_40 Depth 5
                                        #           Child Loop BB21_44 Depth 5
                                        #           Child Loop BB21_48 Depth 5
                                        #           Child Loop BB21_52 Depth 5
	movq	8(%rbp), %r13
	movq	24(%r13), %rbx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB21_30
# BB#29:                                #   in Loop: Header=BB21_28 Depth=4
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB21_30:                              # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB21_28 Depth=4
	movq	16(%r13), %r14
	movq	56(%r14), %rcx
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB21_31:                              #   Parent Loop BB21_16 Depth=1
                                        #     Parent Loop BB21_25 Depth=2
                                        #       Parent Loop BB21_26 Depth=3
                                        #         Parent Loop BB21_28 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%eax
	cmpq	%r13, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB21_31
# BB#32:                                # %clause_LiteralIsFromAntecedent.exit.i
                                        #   in Loop: Header=BB21_28 Depth=4
	movl	64(%r14), %ecx
	cmpl	%ecx, %eax
	jl	.LBB21_38
# BB#33:                                # %clause_LiteralIsFromAntecedent.exit.i
                                        #   in Loop: Header=BB21_28 Depth=4
	movl	68(%r14), %edx
	leal	-1(%rcx,%rdx), %ecx
	cmpl	%ecx, %eax
	jg	.LBB21_38
# BB#34:                                #   in Loop: Header=BB21_28 Depth=4
	movq	%r14, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB21_38
# BB#35:                                #   in Loop: Header=BB21_28 Depth=4
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	52(%r14), %ebp
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	clause_RenameVarsBiggerThan
	movq	%r15, %rdi
	callq	clause_SearchMaxVar
	cmpl	%eax, %ebp
	cmovgel	%ebp, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movslq	64(%r14), %rbp
	movl	68(%r14), %eax
	leal	-1(%rbp,%rax), %eax
	cmpl	%eax, %ebp
	jle	.LBB21_39
# BB#36:                                #   in Loop: Header=BB21_28 Depth=4
	xorl	%eax, %eax
	jmp	.LBB21_41
	.p2align	4, 0x90
.LBB21_38:                              #   in Loop: Header=BB21_28 Depth=4
	movq	%r12, %r14
	jmp	.LBB21_54
.LBB21_39:                              # %.lr.ph.i115.i
                                        #   in Loop: Header=BB21_28 Depth=4
	movslq	%eax, %r12
	decq	%rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB21_40:                              #   Parent Loop BB21_16 Depth=1
                                        #     Parent Loop BB21_25 Depth=2
                                        #       Parent Loop BB21_26 Depth=3
                                        #         Parent Loop BB21_28 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	56(%r14), %rax
	movq	8(%rax,%rbp,8), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbx, (%rax)
	incq	%rbp
	cmpq	%r12, %rbp
	movq	%rax, %rbx
	jl	.LBB21_40
.LBB21_41:                              # %inf_GetAntecedentLiterals.exit.i
                                        #   in Loop: Header=BB21_28 Depth=4
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, %r12
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	136(%rsp), %rsi         # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	testl	%eax, %eax
	je	.LBB21_66
# BB#42:                                #   in Loop: Header=BB21_28 Depth=4
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	96(%rsp), %rsi
	leaq	88(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB21_45
# BB#43:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB21_28 Depth=4
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB21_44:                              # %.lr.ph.i112.i
                                        #   Parent Loop BB21_16 Depth=1
                                        #     Parent Loop BB21_25 Depth=2
                                        #       Parent Loop BB21_26 Depth=3
                                        #         Parent Loop BB21_28 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB21_44
.LBB21_45:                              # %cont_Reset.exit.i
                                        #   in Loop: Header=BB21_28 Depth=4
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	%r13, 144(%rsp)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 152(%rsp)
	movq	88(%rsp), %rax
	movq	%rax, 160(%rsp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	leaq	144(%rsp), %rax
	movq	%rax, 8(%rbx)
	movq	$0, (%rbx)
	movq	96(%rsp), %rsi
	subq	$8, %rsp
.Lcfi420:
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rdi
	movq	%r12, %rdx
	movl	60(%rsp), %ecx          # 4-byte Reload
	movq	%rbx, %r8
	movl	16(%rsp), %r9d          # 4-byte Reload
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi421:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi422:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi423:
	.cfi_adjust_cfa_offset 8
	callq	inf_HyperResolvents
	addq	$32, %rsp
.Lcfi424:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	testq	%r14, %r14
	movq	120(%rsp), %r15         # 8-byte Reload
	je	.LBB21_50
# BB#46:                                #   in Loop: Header=BB21_28 Depth=4
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB21_51
# BB#47:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB21_28 Depth=4
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB21_48:                              # %.preheader.i.i
                                        #   Parent Loop BB21_16 Depth=1
                                        #     Parent Loop BB21_25 Depth=2
                                        #       Parent Loop BB21_26 Depth=3
                                        #         Parent Loop BB21_28 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB21_48
# BB#49:                                #   in Loop: Header=BB21_28 Depth=4
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	jmp	.LBB21_51
.LBB21_50:                              #   in Loop: Header=BB21_28 Depth=4
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB21_51:                              # %list_Nconc.exit.i
                                        #   in Loop: Header=BB21_28 Depth=4
	movq	96(%rsp), %rdi
	callq	subst_Delete
	movq	88(%rsp), %rdi
	callq	subst_Delete
	testq	%r12, %r12
	je	.LBB21_53
	.p2align	4, 0x90
.LBB21_52:                              # %.lr.ph.i.i
                                        #   Parent Loop BB21_16 Depth=1
                                        #     Parent Loop BB21_25 Depth=2
                                        #       Parent Loop BB21_26 Depth=3
                                        #         Parent Loop BB21_28 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB21_52
.LBB21_53:                              # %list_Delete.exit.i
                                        #   in Loop: Header=BB21_28 Depth=4
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	128(%rsp), %rbp         # 8-byte Reload
.LBB21_54:                              #   in Loop: Header=BB21_28 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%r14, %r12
	jne	.LBB21_28
.LBB21_56:                              # %.loopexit.i
                                        #   in Loop: Header=BB21_26 Depth=3
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%r14, %r12
	jne	.LBB21_26
	jmp	.LBB21_22
	.p2align	4, 0x90
.LBB21_57:                              #   in Loop: Header=BB21_16 Depth=1
	movq	%r14, %r12
	movq	56(%rsp), %rdx          # 8-byte Reload
.LBB21_58:                              # %.loopexit129.i
                                        #   in Loop: Header=BB21_16 Depth=1
	cmpq	104(%rsp), %rdx         # 8-byte Folded Reload
	leaq	1(%rdx), %rdx
	jl	.LBB21_16
# BB#59:                                # %inf_BackwardHyperResolution.exit
	movq	%r15, %rdi
	callq	clause_Delete
	testq	%r12, %r12
	je	.LBB21_14
# BB#60:
	movq	16(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB21_64
# BB#61:                                # %.preheader.i.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB21_62:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB21_62
# BB#63:
	movq	%rdx, (%rax)
.LBB21_64:                              # %list_Nconc.exit
	movq	%r12, %rax
	jmp	.LBB21_65
.LBB21_14:
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB21_65:                              # %list_Nconc.exit
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_66:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$4001, %ecx             # imm = 0xFA1
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.Lfunc_end21:
	.size	inf_GeneralHyperResolution, .Lfunc_end21-inf_GeneralHyperResolution
	.cfi_endproc

	.globl	inf_DerivableClauses
	.p2align	4, 0x90
	.type	inf_DerivableClauses,@function
inf_DerivableClauses:                   # @inf_DerivableClauses
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi425:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi426:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi427:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi428:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi429:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi430:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi431:
	.cfi_def_cfa_offset 96
.Lcfi432:
	.cfi_offset %rbx, -56
.Lcfi433:
	.cfi_offset %r12, -48
.Lcfi434:
	.cfi_offset %r13, -40
.Lcfi435:
	.cfi_offset %r14, -32
.Lcfi436:
	.cfi_offset %r15, -24
.Lcfi437:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	112(%rbx), %r14
	movq	104(%rbx), %r13
	movq	32(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	80(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB22_4
# BB#1:
	movq	%r15, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB22_5
# BB#2:
	cmpl	$0, 236(%r14)
	je	.LBB22_8
# BB#3:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%rbp, %rdx
	movq	%r14, %r8
	movq	%r13, %r9
	callq	inf_BackwardEmptySort
	jmp	.LBB22_9
.LBB22_4:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	cmpl	$0, 244(%r14)
	jne	.LBB22_18
	jmp	.LBB22_23
.LBB22_5:
	movq	%r15, %rdi
	callq	clause_HasTermSortConstraintLits
	testl	%eax, %eax
	je	.LBB22_202
# BB#6:
	cmpl	$0, 240(%r14)
	je	.LBB22_205
# BB#7:
	movq	(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%rbp, %rdx
	movq	%r14, %r8
	movq	%r13, %r9
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	inf_ForwardSortResolution # TAILCALL
.LBB22_8:
	xorl	%eax, %eax
.LBB22_9:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	cmpl	$0, 240(%r14)
	je	.LBB22_15
# BB#10:
	movq	%rax, %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%rbp, %rdx
	movq	%r14, %r8
	movq	%r13, %r9
	callq	inf_BackwardSortResolution
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB22_16
# BB#11:
	testq	%rbx, %rbx
	je	.LBB22_17
# BB#12:                                # %.preheader.i263.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB22_13:                              # %.preheader.i263
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_13
# BB#14:
	movq	%rbx, (%rax)
	cmpl	$0, 244(%r14)
	jne	.LBB22_18
	jmp	.LBB22_23
.LBB22_15:
	movq	%rax, %r12
	cmpl	$0, 244(%r14)
	jne	.LBB22_18
	jmp	.LBB22_23
.LBB22_16:
	movq	%rbx, %r12
.LBB22_17:                              # %.critedge
	cmpl	$0, 244(%r14)
	je	.LBB22_23
.LBB22_18:
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, %rcx
	callq	inf_EqualityResolution
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB22_24
# BB#19:
	testq	%r12, %r12
	je	.LBB22_25
# BB#20:                                # %.preheader.i281.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB22_21:                              # %.preheader.i281
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_21
# BB#22:
	movq	%r12, (%rax)
	cmpl	$0, 248(%r14)
	jne	.LBB22_26
	jmp	.LBB22_31
.LBB22_23:
	movq	%r12, %rbp
	cmpl	$0, 248(%r14)
	jne	.LBB22_26
	jmp	.LBB22_31
.LBB22_24:
	movq	%r12, %rbp
.LBB22_25:                              # %list_Nconc.exit283
	cmpl	$0, 248(%r14)
	je	.LBB22_31
.LBB22_26:
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r13, %rcx
	callq	inf_EqualityResolution
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB22_32
# BB#27:
	testq	%rbp, %rbp
	je	.LBB22_33
# BB#28:                                # %.preheader.i293.preheader
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB22_29:                              # %.preheader.i293
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_29
# BB#30:
	movq	%rbp, (%rax)
	cmpl	$0, 256(%r14)
	jne	.LBB22_34
	jmp	.LBB22_39
.LBB22_31:
	movq	%rbp, %rbx
	cmpl	$0, 256(%r14)
	jne	.LBB22_34
	jmp	.LBB22_39
.LBB22_32:
	movq	%rbp, %rbx
.LBB22_33:                              # %list_Nconc.exit295
	cmpl	$0, 256(%r14)
	je	.LBB22_39
.LBB22_34:
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %rdx
	movq	%r13, %rcx
	callq	inf_MergingParamodulation
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB22_40
# BB#35:
	testq	%rbx, %rbx
	je	.LBB22_41
# BB#36:                                # %.preheader.i311.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB22_37:                              # %.preheader.i311
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_37
# BB#38:
	movq	%rbx, (%rax)
	cmpl	$0, 252(%r14)
	jne	.LBB22_42
	jmp	.LBB22_48
.LBB22_39:
	movq	%rbx, %rbp
	cmpl	$0, 252(%r14)
	jne	.LBB22_42
	jmp	.LBB22_48
.LBB22_40:
	movq	%rbx, %rbp
.LBB22_41:                              # %list_Nconc.exit313
	cmpl	$0, 252(%r14)
	je	.LBB22_48
.LBB22_42:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	inf_EqualityFactoring
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB22_48
# BB#43:
	testq	%rbp, %rbp
	je	.LBB22_49
# BB#44:                                # %.preheader.i305.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB22_45:                              # %.preheader.i305
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_45
# BB#46:
	movq	%rbp, (%rax)
	jmp	.LBB22_49
.LBB22_48:
	movq	%rbp, %r12
.LBB22_49:                              # %list_Nconc.exit307
	movl	296(%r14), %eax
	testl	%eax, %eax
	movq	%r13, 16(%rsp)          # 8-byte Spill
	je	.LBB22_56
# BB#50:                                # %list_Nconc.exit307
	cmpl	$2, %eax
	je	.LBB22_57
# BB#51:                                # %list_Nconc.exit307
	cmpl	$1, %eax
	jne	.LBB22_207
# BB#52:
	movl	$1, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r14, %r8
	movq	%r13, %r9
	callq	inf_GeneralFactoring
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB22_56
# BB#53:
	testq	%r12, %r12
	je	.LBB22_64
# BB#54:                                # %.preheader.i299.preheader
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB22_55:                              # %.preheader.i299
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_55
	jmp	.LBB22_61
.LBB22_56:
	movq	%r12, %r13
	cmpl	$0, 300(%r14)
	jne	.LBB22_65
	jmp	.LBB22_71
.LBB22_57:
	movl	$1, %esi
	movl	$1, %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r14, %r8
	movq	%r13, %r9
	callq	inf_GeneralFactoring
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB22_63
# BB#58:
	testq	%r12, %r12
	je	.LBB22_64
# BB#59:                                # %.preheader.i287.preheader
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB22_60:                              # %.preheader.i287
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_60
.LBB22_61:
	movq	%r12, (%rax)
	cmpl	$0, 300(%r14)
	jne	.LBB22_65
	jmp	.LBB22_71
.LBB22_63:
	movq	%r12, %r13
.LBB22_64:                              # %list_Nconc.exit301
	cmpl	$0, 300(%r14)
	je	.LBB22_71
.LBB22_65:
	xorl	%esi, %esi
	movl	$1, %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	%r14, %r8
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	inf_GeneralFactoring
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB22_71
# BB#66:
	testq	%r13, %r13
	je	.LBB22_72
# BB#67:                                # %.preheader.i275.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB22_68:                              # %.preheader.i275
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_68
# BB#69:
	movq	%r13, (%rax)
	jmp	.LBB22_72
.LBB22_71:
	movq	%r13, %rbp
.LBB22_72:                              # %list_Nconc.exit277
	cmpl	$0, 260(%r14)
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB22_78
# BB#73:
	movq	%rbx, (%rsp)
	movl	$1, %edx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %r9
	callq	inf_GenSuperpositionRight
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB22_79
# BB#74:
	testq	%rbp, %rbp
	je	.LBB22_80
# BB#75:                                # %.preheader.i269.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB22_76:                              # %.preheader.i269
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_76
# BB#77:
	movq	%rbp, (%rax)
	cmpl	$0, 268(%r14)
	jne	.LBB22_81
	jmp	.LBB22_92
.LBB22_78:
	movq	%rbp, %r12
	cmpl	$0, 268(%r14)
	jne	.LBB22_81
	jmp	.LBB22_92
.LBB22_79:
	movq	%rbp, %r12
.LBB22_80:                              # %list_Nconc.exit271
	cmpl	$0, 268(%r14)
	je	.LBB22_92
.LBB22_81:
	movq	%rbx, (%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r14, %r9
	callq	inf_GenSuperpositionLeft
	movq	%rax, %r13
	movq	%rbx, (%rsp)
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %r9
	callq	inf_GenSuperpositionRight
	testq	%r13, %r13
	je	.LBB22_87
# BB#82:
	testq	%rax, %rax
	je	.LBB22_88
# BB#83:                                # %.preheader.i.i257.preheader
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB22_84:                              # %.preheader.i.i257
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB22_84
# BB#85:
	movq	%rax, (%rcx)
	testq	%r13, %r13
	jne	.LBB22_88
	jmp	.LBB22_92
.LBB22_87:
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB22_92
.LBB22_88:                              # %inf_Paramodulation.exit.thread
	testq	%r12, %r12
	je	.LBB22_93
# BB#89:                                # %.preheader.i251.preheader
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB22_90:                              # %.preheader.i251
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_90
# BB#91:
	movq	%r12, (%rax)
	jmp	.LBB22_93
.LBB22_92:
	movq	%r12, %r13
.LBB22_93:                              # %list_Nconc.exit253
	cmpl	$0, 264(%r14)
	movq	%r15, 32(%rsp)          # 8-byte Spill
	je	.LBB22_105
# BB#94:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, (%rsp)
	movl	$1, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rsi
	movq	%r14, %r9
	callq	inf_GenSuperpositionLeft
	movq	%r15, %rdi
	movq	%rax, %r15
	movq	%rbx, (%rsp)
	movl	$1, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbp, %rsi
	movq	%r14, %r9
	callq	inf_GenSuperpositionRight
	testq	%r15, %r15
	je	.LBB22_100
# BB#95:
	testq	%rax, %rax
	je	.LBB22_101
# BB#96:                                # %.preheader.i.i243.preheader
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB22_97:                              # %.preheader.i.i243
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB22_97
# BB#98:
	movq	%rax, (%rcx)
	testq	%r15, %r15
	jne	.LBB22_101
	jmp	.LBB22_105
.LBB22_100:
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB22_105
.LBB22_101:                             # %inf_OrderedParamodulation.exit.thread
	testq	%r13, %r13
	je	.LBB22_106
# BB#102:                               # %.preheader.i237.preheader
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB22_103:                             # %.preheader.i237
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_103
# BB#104:
	movq	%r13, (%rax)
	jmp	.LBB22_106
.LBB22_105:
	movq	%r13, %r15
.LBB22_106:                             # %list_Nconc.exit239
	cmpl	$0, 272(%r14)
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB22_113
# BB#107:
	movq	%rbx, (%rsp)
	movl	$1, %edx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %r9
	callq	inf_GenSuperpositionLeft
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB22_113
# BB#108:
	testq	%r15, %r15
	je	.LBB22_114
# BB#109:                               # %.preheader.i231.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB22_110:                             # %.preheader.i231
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_110
# BB#111:
	movq	%r15, (%rax)
	jmp	.LBB22_114
.LBB22_113:
	movq	%r15, %r12
.LBB22_114:                             # %list_Nconc.exit233
	movl	276(%r14), %eax
	testl	%eax, %eax
	movq	32(%rsp), %r15          # 8-byte Reload
	je	.LBB22_128
# BB#115:                               # %list_Nconc.exit233
	cmpl	$2, %eax
	je	.LBB22_122
# BB#116:                               # %list_Nconc.exit233
	cmpl	$1, %eax
	jne	.LBB22_208
# BB#117:
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	inf_GeneralResolution
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB22_128
# BB#118:
	testq	%r12, %r12
	je	.LBB22_129
# BB#119:                               # %.preheader.i225.preheader
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB22_120:                             # %.preheader.i225
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_120
	jmp	.LBB22_126
.LBB22_122:
	movl	$1, %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	inf_GeneralResolution
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB22_128
# BB#123:
	testq	%r12, %r12
	je	.LBB22_129
# BB#124:                               # %.preheader.i217.preheader
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB22_125:                             # %.preheader.i217
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_125
.LBB22_126:
	movq	%r12, (%rax)
	jmp	.LBB22_129
.LBB22_128:
	movq	%r12, %r13
.LBB22_129:                             # %list_Nconc.exit227
	movl	280(%r14), %eax
	testl	%eax, %eax
	je	.LBB22_143
# BB#130:                               # %list_Nconc.exit227
	cmpl	$2, %eax
	je	.LBB22_137
# BB#131:                               # %list_Nconc.exit227
	cmpl	$1, %eax
	jne	.LBB22_209
# BB#132:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %r8
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	inf_GeneralResolution
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB22_143
# BB#133:
	testq	%r13, %r13
	je	.LBB22_144
# BB#134:                               # %.preheader.i211.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB22_135:                             # %.preheader.i211
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_135
	jmp	.LBB22_141
.LBB22_137:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %r8
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	inf_GeneralResolution
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB22_143
# BB#138:
	testq	%r13, %r13
	je	.LBB22_144
# BB#139:                               # %.preheader.i203.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB22_140:                             # %.preheader.i203
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_140
.LBB22_141:
	movq	%r13, (%rax)
	jmp	.LBB22_144
.LBB22_143:
	movq	%r13, %r12
.LBB22_144:                             # %list_Nconc.exit213
	cmpl	$0, 304(%r14)
	movq	16(%rsp), %r13          # 8-byte Reload
	je	.LBB22_150
# BB#145:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	inf_UnitResolution
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB22_151
# BB#146:
	testq	%r12, %r12
	je	.LBB22_152
# BB#147:                               # %.preheader.i197.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB22_148:                             # %.preheader.i197
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_148
# BB#149:
	movq	%r12, (%rax)
	cmpl	$0, 308(%r14)
	jne	.LBB22_153
	jmp	.LBB22_158
.LBB22_150:
	movq	%r12, %rbp
	cmpl	$0, 308(%r14)
	jne	.LBB22_153
	jmp	.LBB22_158
.LBB22_151:
	movq	%r12, %rbp
.LBB22_152:                             # %list_Nconc.exit199
	cmpl	$0, 308(%r14)
	je	.LBB22_158
.LBB22_153:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	inf_BoundedDepthUnitResolution
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB22_159
# BB#154:
	testq	%rbp, %rbp
	je	.LBB22_160
# BB#155:                               # %.preheader.i191.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB22_156:                             # %.preheader.i191
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_156
# BB#157:
	movq	%rbp, (%rax)
	cmpl	$0, 284(%r14)
	jne	.LBB22_161
	jmp	.LBB22_166
.LBB22_158:
	movq	%rbp, %r12
	cmpl	$0, 284(%r14)
	jne	.LBB22_161
	jmp	.LBB22_166
.LBB22_159:
	movq	%rbp, %r12
.LBB22_160:                             # %list_Nconc.exit193
	cmpl	$0, 284(%r14)
	je	.LBB22_166
.LBB22_161:
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	inf_GeneralHyperResolution
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB22_167
# BB#162:
	testq	%r12, %r12
	je	.LBB22_168
# BB#163:                               # %.preheader.i185.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB22_164:                             # %.preheader.i185
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_164
# BB#165:
	movq	%r12, (%rax)
	cmpl	$0, 288(%r14)
	jne	.LBB22_169
	jmp	.LBB22_174
.LBB22_166:
	movq	%r12, %rbp
	cmpl	$0, 288(%r14)
	jne	.LBB22_169
	jmp	.LBB22_174
.LBB22_167:
	movq	%r12, %rbp
.LBB22_168:                             # %list_Nconc.exit187
	cmpl	$0, 288(%r14)
	je	.LBB22_174
.LBB22_169:
	movl	$1, %edx
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %rcx
	movq	%r13, %r8
	callq	inf_GeneralHyperResolution
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB22_175
# BB#170:
	testq	%rbp, %rbp
	je	.LBB22_176
# BB#171:                               # %.preheader.i179.preheader
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB22_172:                             # %.preheader.i179
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_172
# BB#173:
	movq	%rbp, (%rax)
	cmpl	$0, 292(%r14)
	jne	.LBB22_177
	jmp	.LBB22_182
.LBB22_174:
	movq	%rbp, %rbx
	cmpl	$0, 292(%r14)
	jne	.LBB22_177
	jmp	.LBB22_182
.LBB22_175:
	movq	%rbp, %rbx
.LBB22_176:                             # %list_Nconc.exit181
	cmpl	$0, 292(%r14)
	je	.LBB22_182
.LBB22_177:
	movq	%r15, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r14, %rdx
	movq	%r13, %rcx
	callq	inf_URResolution
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB22_183
# BB#178:
	testq	%rbx, %rbx
	je	.LBB22_184
# BB#179:                               # %.preheader.i173.preheader
	movq	%r12, %rcx
	.p2align	4, 0x90
.LBB22_180:                             # %.preheader.i173
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_180
# BB#181:
	movq	%rbx, (%rax)
	cmpl	$0, 312(%r14)
	jne	.LBB22_185
	jmp	.LBB22_199
.LBB22_182:
	movq	%rbx, %r12
	cmpl	$0, 312(%r14)
	jne	.LBB22_185
	jmp	.LBB22_199
.LBB22_183:
	movq	%rbx, %r12
.LBB22_184:                             # %list_Nconc.exit175
	cmpl	$0, 312(%r14)
	je	.LBB22_199
.LBB22_185:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB22_201
# BB#186:                               # %.lr.ph.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB22_187:                             # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_190 Depth 2
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r13, %rcx
	callq	def_ApplyDefToClauseOnce
	testq	%rax, %rax
	je	.LBB22_193
# BB#188:                               #   in Loop: Header=BB22_187 Depth=1
	testq	%rbp, %rbp
	je	.LBB22_192
# BB#189:                               # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB22_187 Depth=1
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB22_190:                             # %.preheader.i.i
                                        #   Parent Loop BB22_187 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB22_190
# BB#191:                               #   in Loop: Header=BB22_187 Depth=1
	movq	%rbp, (%rcx)
.LBB22_192:                             # %list_Nconc.exit.i
                                        #   in Loop: Header=BB22_187 Depth=1
	movq	%rax, %rbp
.LBB22_193:                             # %list_Nconc.exit.i
                                        #   in Loop: Header=BB22_187 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_187
# BB#194:                               # %inf_ApplyDefinition.exit
	testq	%rbp, %rbp
	je	.LBB22_201
# BB#195:
	testq	%r12, %r12
	je	.LBB22_206
# BB#196:                               # %.preheader.i.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB22_197:                             # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB22_197
# BB#198:
	movq	%r12, (%rax)
	jmp	.LBB22_206
.LBB22_201:
	movq	%r12, %rbp
	jmp	.LBB22_206
.LBB22_199:
	movq	%r12, %rbp
	jmp	.LBB22_206
.LBB22_202:
	cmpl	$0, 236(%r14)
	je	.LBB22_205
# BB#203:
	movq	(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%rbp, %rdx
	movq	%r14, %r8
	movq	%r13, %r9
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	inf_ForwardEmptySort    # TAILCALL
.LBB22_205:
	xorl	%ebp, %ebp
.LBB22_206:                             # %list_Nconc.exit207
	movq	%rbp, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_207:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.6, %edi
	jmp	.LBB22_210
.LBB22_208:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.7, %edi
	jmp	.LBB22_210
.LBB22_209:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.8, %edi
.LBB22_210:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end22:
	.size	inf_DerivableClauses, .Lfunc_end22-inf_DerivableClauses
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi438:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end23:
	.size	misc_Error, .Lfunc_end23-misc_Error
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_ApplyGenSuperposition,@function
inf_ApplyGenSuperposition:              # @inf_ApplyGenSuperposition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi439:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi440:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi441:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi442:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi443:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi444:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi445:
	.cfi_def_cfa_offset 160
.Lcfi446:
	.cfi_offset %rbx, -56
.Lcfi447:
	.cfi_offset %r12, -48
.Lcfi448:
	.cfi_offset %r13, -40
.Lcfi449:
	.cfi_offset %r14, -32
.Lcfi450:
	.cfi_offset %r15, -24
.Lcfi451:
	.cfi_offset %rbp, -16
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	movq	%rcx, %r14
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movl	%esi, 60(%rsp)          # 4-byte Spill
	movq	%rdi, %r15
	movl	64(%r14), %r13d
	movl	68(%r14), %eax
	leal	-1(%r13,%rax), %eax
	movl	72(%r14), %ecx
	movl	%eax, 84(%rsp)          # 4-byte Spill
	addl	%eax, %ecx
	movl	64(%r15), %ebx
	movl	68(%r15), %eax
	movl	72(%r15), %ebp
	leal	-1(%rbx), %edx
	movl	%edx, 48(%rsp)          # 4-byte Spill
	leal	-1(%rbx,%rax), %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leal	(%rcx,%rbx), %edi
	addl	%eax, %edi
	addl	%ebp, %edi
	callq	clause_CreateBody
	movq	%rax, %r12
	movl	64(%r14), %eax
	addl	64(%r15), %eax
	movl	%eax, 64(%r12)
	movl	68(%r14), %eax
	addl	68(%r15), %eax
	movl	%eax, 68(%r12)
	movl	72(%r15), %eax
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movl	72(%r14), %ecx
	leal	-1(%rax,%rcx), %eax
	movl	%eax, 72(%r12)
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	js	.LBB24_1
# BB#2:                                 # %.lr.ph212
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	xorl	%ebp, %ebp
	movq	72(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB24_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	clause_LiteralCreate
	movq	56(%r12), %rcx
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB24_3
# BB#4:
	movl	40(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB24_5
.LBB24_1:
	xorl	%ebx, %ebx
.LBB24_5:                               # %._crit_edge213
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movl	24(%rsp), %ecx          # 4-byte Reload
	addl	%ecx, %ebp
	movl	%ebp, 40(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	64(%rax), %rax
	cmpl	%ecx, %ebx
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jg	.LBB24_8
# BB#6:                                 # %.lr.ph207
	movslq	%ebx, %rbp
	movslq	%ecx, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	decq	%rbp
	leaq	(,%rax,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB24_7:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	clause_LiteralCreate
	movq	56(%r12), %rcx
	addq	48(%rsp), %rcx          # 8-byte Folded Reload
	movq	%rax, 8(%rcx,%rbp,8)
	incq	%rbp
	incl	%ebx
	cmpq	24(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB24_7
.LBB24_8:                               # %._crit_edge208
	leal	-1(%r13), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	40(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, %ebx
	movq	88(%rsp), %r14          # 8-byte Reload
	jg	.LBB24_14
# BB#9:                                 # %.lr.ph203
	movq	32(%rsp), %rax          # 8-byte Reload
	addl	68(%rax), %r14d
	movslq	%ebx, %r15
	movslq	%ecx, %rax
	decq	%r15
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB24_10:                              # =>This Inner Loop Header: Depth=1
	cmpl	%ebx, 60(%rsp)          # 4-byte Folded Reload
	jne	.LBB24_11
# BB#12:                                #   in Loop: Header=BB24_10 Depth=1
	decl	%r14d
	jmp	.LBB24_13
	.p2align	4, 0x90
.LBB24_11:                              #   in Loop: Header=BB24_10 Depth=1
	leal	(%r14,%rbx), %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%r15,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	clause_LiteralCreate
	movq	56(%r12), %rcx
	movslq	%ebp, %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	24(%rsp), %rax          # 8-byte Reload
.LBB24_13:                              #   in Loop: Header=BB24_10 Depth=1
	incq	%r15
	incl	%ebx
	cmpq	%rax, %r15
	jl	.LBB24_10
.LBB24_14:                              # %._crit_edge204
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	64(%rax), %rdx
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	js	.LBB24_15
# BB#16:                                # %.lr.ph197
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	leaq	(,%rdx,8), %r15
	xorl	%ebx, %ebx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB24_17:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	clause_LiteralCreate
	movq	56(%r12), %rcx
	addq	%r15, %rcx
	movq	%rax, (%rcx,%rbx,8)
	incq	%rbx
	cmpq	%rbx, %r13
	jne	.LBB24_17
# BB#18:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB24_19
.LBB24_15:
	xorl	%r13d, %r13d
.LBB24_19:                              # %._crit_edge198
	movl	84(%rsp), %ecx          # 4-byte Reload
	cmpl	%ecx, %r13d
	movq	64(%rsp), %rbx          # 8-byte Reload
	jg	.LBB24_25
# BB#20:                                # %.lr.ph193
	movslq	68(%rax), %rbp
	addq	%rdx, %rbp
	movslq	%r13d, %r14
	movslq	%ecx, %r15
	decq	%r14
	shlq	$3, %rbp
	.p2align	4, 0x90
.LBB24_21:                              # =>This Inner Loop Header: Depth=1
	cmpl	%r13d, 44(%rsp)         # 4-byte Folded Reload
	jne	.LBB24_22
# BB#23:                                #   in Loop: Header=BB24_21 Depth=1
	movl	fol_NOT(%rip), %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	160(%rsp), %rcx
	movq	%rcx, 8(%rax)
	movq	$0, (%rax)
	movl	%ebx, %edi
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rax, %rsi
	callq	term_Create
	jmp	.LBB24_24
	.p2align	4, 0x90
.LBB24_22:                              #   in Loop: Header=BB24_21 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
.LBB24_24:                              #   in Loop: Header=BB24_21 Depth=1
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	clause_LiteralCreate
	movq	56(%r12), %rcx
	addq	%rbp, %rcx
	movq	%rax, 8(%rcx,%r14,8)
	incq	%r14
	incl	%r13d
	cmpq	%r15, %r14
	jl	.LBB24_21
.LBB24_25:                              # %._crit_edge194
	movq	96(%rsp), %rdx          # 8-byte Reload
	cmpl	%edx, %r13d
	jg	.LBB24_30
# BB#26:                                # %.lr.ph
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	72(%rcx), %eax
	movslq	%r13d, %rbx
	movslq	%edx, %r15
	decq	%rbx
	movl	44(%rsp), %r14d         # 4-byte Reload
	subl	%r13d, %r14d
	addl	64(%rcx), %r13d
	addl	68(%rcx), %r13d
	leal	-1(%rax,%r13), %ebp
	.p2align	4, 0x90
.LBB24_27:                              # =>This Inner Loop Header: Depth=1
	testl	%r14d, %r14d
	movq	160(%rsp), %rdi
	je	.LBB24_29
# BB#28:                                #   in Loop: Header=BB24_27 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
.LBB24_29:                              #   in Loop: Header=BB24_27 Depth=1
	movq	%r12, %rsi
	callq	clause_LiteralCreate
	movq	56(%r12), %rcx
	movslq	%ebp, %rbp
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbx
	incl	%ebp
	decl	%r14d
	cmpq	%r15, %rbx
	jl	.LBB24_27
.LBB24_30:                              # %._crit_edge
	movq	200(%rsp), %rax
	movq	192(%rsp), %r9
	movl	184(%rsp), %edx
	movl	176(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB24_35
# BB#31:                                # %._crit_edge
	testl	%edx, %edx
	je	.LBB24_35
# BB#32:
	movl	168(%rsp), %ecx
	testl	%ecx, %ecx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	je	.LBB24_34
# BB#33:
	movl	$8, 76(%r12)
	jmp	.LBB24_39
.LBB24_35:
	testl	%ecx, %ecx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	je	.LBB24_38
# BB#36:
	testl	%edx, %edx
	jne	.LBB24_38
# BB#37:
	movl	$7, 76(%r12)
	jmp	.LBB24_39
.LBB24_38:
	movl	$6, 76(%r12)
	jmp	.LBB24_39
.LBB24_34:
	movl	$9, 76(%r12)
.LBB24_39:
	movq	%rax, (%rsp)
	movq	%r12, %rdi
	movl	44(%rsp), %edx          # 4-byte Reload
	movl	60(%rsp), %r8d          # 4-byte Reload
	callq	clause_SetDataFromParents
	movq	%r12, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	inf_ApplyGenSuperposition, .Lfunc_end24-inf_ApplyGenSuperposition
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_NAllTermsRplac,@function
inf_NAllTermsRplac:                     # @inf_NAllTermsRplac
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi452:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi453:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi454:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi455:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi456:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi457:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi458:
	.cfi_def_cfa_offset 64
.Lcfi459:
	.cfi_offset %rbx, -56
.Lcfi460:
	.cfi_offset %r12, -48
.Lcfi461:
	.cfi_offset %r13, -40
.Lcfi462:
	.cfi_offset %r14, -32
.Lcfi463:
	.cfi_offset %r15, -24
.Lcfi464:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbp
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB25_2
# BB#1:
	movl	(%r15), %eax
	movl	%eax, (%rbp)
	movq	16(%rbp), %rbx
	movq	16(%r15), %rdi
	movl	$term_Copy, %esi
	callq	list_CopyWithElement
	movq	%rax, 16(%rbp)
	movl	$term_Delete, %esi
	movq	%rbx, %rdi
	callq	list_DeleteWithElement
	movl	$1, %r13d
	jmp	.LBB25_17
.LBB25_2:
	cmpl	$0, (%rbp)
	jle	.LBB25_4
# BB#3:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	subst_Apply
.LBB25_4:
	movq	16(%rbp), %rcx
	testq	%rcx, %rcx
	je	.LBB25_5
# BB#6:                                 # %.lr.ph47.preheader
	movq	%rbx, (%rsp)            # 8-byte Spill
	movl	stack_POINTER(%rip), %ebp
	leal	1(%rbp), %eax
	movl	%eax, stack_POINTER(%rip)
	movq	%rcx, stack_STACK(,%rbp,8)
	xorl	%r13d, %r13d
.LBB25_8:                               # %.lr.ph47
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_15 Depth 2
	decl	%eax
	movq	stack_STACK(,%rax,8), %r14
	movq	(%r14), %rcx
	movq	8(%r14), %rbx
	movq	%rcx, stack_STACK(,%rax,8)
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB25_10
# BB#9:                                 #   in Loop: Header=BB25_8 Depth=1
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, 8(%r14)
	movq	%rbx, %rdi
	callq	term_Delete
	movl	$1, %r13d
	jmp	.LBB25_14
.LBB25_10:                              #   in Loop: Header=BB25_8 Depth=1
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB25_12
# BB#11:                                #   in Loop: Header=BB25_8 Depth=1
	movl	stack_POINTER(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, stack_POINTER(%rip)
	movq	%rax, stack_STACK(,%rcx,8)
	jmp	.LBB25_14
.LBB25_12:                              #   in Loop: Header=BB25_8 Depth=1
	cmpl	$0, (%rbx)
	jle	.LBB25_14
# BB#13:                                #   in Loop: Header=BB25_8 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	callq	subst_Apply
	.p2align	4, 0x90
.LBB25_14:                              # %.preheader
                                        #   in Loop: Header=BB25_8 Depth=1
	movl	stack_POINTER(%rip), %eax
	cmpl	%ebp, %eax
	je	.LBB25_17
	.p2align	4, 0x90
.LBB25_15:                              # %.lr.ph
                                        #   Parent Loop BB25_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-1(%rax), %ecx
	cmpq	$0, stack_STACK(,%rcx,8)
	jne	.LBB25_7
# BB#16:                                #   in Loop: Header=BB25_15 Depth=2
	movl	%ecx, stack_POINTER(%rip)
	cmpl	%ecx, %ebp
	movl	%ecx, %eax
	jne	.LBB25_15
	jmp	.LBB25_17
	.p2align	4, 0x90
.LBB25_7:                               # %.critedge.loopexit
                                        #   in Loop: Header=BB25_8 Depth=1
	cmpl	%eax, %ebp
	jne	.LBB25_8
	jmp	.LBB25_17
.LBB25_5:
	xorl	%r13d, %r13d
.LBB25_17:                              # %.loopexit
	movl	%r13d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	inf_NAllTermsRplac, .Lfunc_end25-inf_NAllTermsRplac
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_SetDataFromParents,@function
clause_SetDataFromParents:              # @clause_SetDataFromParents
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi465:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi466:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi467:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi468:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi469:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi470:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi471:
	.cfi_def_cfa_offset 64
.Lcfi472:
	.cfi_offset %rbx, -56
.Lcfi473:
	.cfi_offset %r12, -48
.Lcfi474:
	.cfi_offset %r13, -40
.Lcfi475:
	.cfi_offset %r14, -32
.Lcfi476:
	.cfi_offset %r15, -24
.Lcfi477:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movq	%rcx, %r12
	movl	%edx, %r15d
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	64(%rsp), %r14
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	clause_OrientEqualities
	movq	%rbx, %rdi
	callq	clause_Normalize
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	clause_SetMaxLitFlags
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbx)
	movq	%rbx, %rdi
	callq	clause_UpdateMaxVar
	testb	$8, 48(%r12)
	jne	.LBB26_2
# BB#1:
	testb	$8, 48(%r13)
	je	.LBB26_3
.LBB26_2:
	orb	$8, 48(%rbx)
.LBB26_3:
	movl	12(%r12), %eax
	movl	12(%r13), %ecx
	movl	%ecx, %edx
	orl	%eax, %edx
	je	.LBB26_32
# BB#4:                                 # %._crit_edge.i
	cmpl	%eax, %ecx
	movq	%r12, %rax
	cmovaq	%r13, %rax
	movl	12(%rax), %eax
	movl	%eax, 12(%rbx)
	movl	24(%r13), %eax
	movq	16(%rbx), %rdi
	cmpl	24(%r12), %eax
	jbe	.LBB26_20
# BB#5:
	testq	%rdi, %rdi
	je	.LBB26_13
# BB#6:
	movl	24(%rbx), %ecx
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB26_7
# BB#12:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB26_13
.LBB26_20:
	testq	%rdi, %rdi
	je	.LBB26_28
# BB#21:
	movl	24(%rbx), %ecx
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB26_22
# BB#27:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB26_28
.LBB26_7:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%r8, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB26_9
# BB#8:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB26_9:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB26_11
# BB#10:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB26_11:
	addq	$-16, %rdi
	callq	free
.LBB26_13:                              # %memory_Free.exit66.i
	movl	24(%r13), %edi
	shll	$3, %edi
	callq	memory_Malloc
	movq	%rax, 16(%rbx)
	movl	24(%r13), %ecx
	movl	%ecx, 24(%rbx)
	cmpl	$0, 24(%r12)
	je	.LBB26_14
# BB#17:                                # %.lr.ph73.i
	movq	16(%r13), %rcx
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	orq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	24(%r12), %eax
	cmpl	$2, %eax
	jb	.LBB26_15
# BB#18:                                # %._crit_edge84.i.preheader
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB26_19:                              # %._crit_edge84.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	16(%r13), %rdx
	movl	%ecx, %esi
	movq	16(%r12), %rdi
	movq	(%rdi,%rsi,8), %rdi
	orq	(%rdx,%rsi,8), %rdi
	movq	%rdi, (%rax,%rsi,8)
	incl	%ecx
	movl	24(%r12), %eax
	cmpl	%eax, %ecx
	jb	.LBB26_19
	jmp	.LBB26_15
.LBB26_14:
	xorl	%eax, %eax
	cmpl	24(%r13), %eax
	jae	.LBB26_32
	.p2align	4, 0x90
.LBB26_16:
	movq	16(%r13), %rcx
	movl	%eax, %edx
	movq	(%rcx,%rdx,8), %rcx
	movq	16(%rbx), %rsi
	movq	%rcx, (%rsi,%rdx,8)
	incl	%eax
.LBB26_15:                              # %.preheader.i
	cmpl	24(%r13), %eax
	jb	.LBB26_16
	jmp	.LBB26_32
.LBB26_22:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%r8, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB26_24
# BB#23:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB26_24:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB26_26
# BB#25:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB26_26:
	addq	$-16, %rdi
	callq	free
.LBB26_28:                              # %memory_Free.exit.i
	movl	24(%r12), %edi
	shll	$3, %edi
	callq	memory_Malloc
	movq	%rax, 16(%rbx)
	movl	24(%r12), %ecx
	movl	%ecx, 24(%rbx)
	cmpl	$0, 24(%r13)
	je	.LBB26_29
# BB#33:                                # %.lr.ph77.i
	movq	16(%r13), %rcx
	movq	16(%r12), %rdx
	movq	(%rdx), %rdx
	orq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	24(%r13), %eax
	cmpl	$2, %eax
	jb	.LBB26_31
# BB#34:                                # %._crit_edge83.i.preheader
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB26_35:                              # %._crit_edge83.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	16(%r13), %rdx
	movl	%ecx, %esi
	movq	16(%r12), %rdi
	movq	(%rdi,%rsi,8), %rdi
	orq	(%rdx,%rsi,8), %rdi
	movq	%rdi, (%rax,%rsi,8)
	incl	%ecx
	movl	24(%r13), %eax
	cmpl	%eax, %ecx
	jb	.LBB26_35
	jmp	.LBB26_31
.LBB26_29:
	xorl	%eax, %eax
	cmpl	24(%r12), %eax
	jae	.LBB26_32
	.p2align	4, 0x90
.LBB26_30:
	movq	16(%r12), %rcx
	movl	%eax, %edx
	movq	(%rcx,%rdx,8), %rcx
	movq	16(%rbx), %rsi
	movq	%rcx, (%rsi,%rdx,8)
	incl	%eax
.LBB26_31:
	cmpl	24(%r12), %eax
	jb	.LBB26_30
.LBB26_32:                              # %clause_SetSplitDataFromParents.exit
	movl	8(%r13), %edi
	movl	8(%r12), %esi
	callq	misc_Max
	incl	%eax
	movl	%eax, 8(%rbx)
	movslq	(%r13), %rbp
	movq	32(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%rbx)
	movslq	%r15d, %rbp
	movq	40(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbx)
	movslq	(%r12), %rbp
	movq	32(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%rbx)
	movslq	4(%rsp), %rbp           # 4-byte Folded Reload
	movq	40(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	clause_SetDataFromParents, .Lfunc_end26-clause_SetDataFromParents
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_GenSPRightEqToGiven,@function
inf_GenSPRightEqToGiven:                # @inf_GenSPRightEqToGiven
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi478:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi479:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi480:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi481:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi482:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi483:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi484:
	.cfi_def_cfa_offset 224
.Lcfi485:
	.cfi_offset %rbx, -56
.Lcfi486:
	.cfi_offset %r12, -48
.Lcfi487:
	.cfi_offset %r13, -40
.Lcfi488:
	.cfi_offset %r14, -32
.Lcfi489:
	.cfi_offset %r15, -24
.Lcfi490:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%r9, 40(%rsp)           # 8-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %rbx
	movl	%edx, 76(%rsp)          # 4-byte Spill
	movq	56(%rdi), %rax
	movl	%esi, 108(%rsp)         # 4-byte Spill
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB27_2
# BB#1:
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB27_2:                               # %clause_LiteralAtom.exit
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	stack_POINTER(%rip), %eax
	movl	%eax, 140(%rsp)         # 4-byte Spill
	movq	16(%rcx), %rax
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	jne	.LBB27_4
# BB#3:
	movq	(%rax), %rax
.LBB27_4:                               # %.preheader
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	callq	sharing_PushListOnStack
	xorl	%eax, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
.LBB27_5:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_6 Depth 2
                                        #     Child Loop BB27_10 Depth 2
                                        #       Child Loop BB27_12 Depth 3
                                        #         Child Loop BB27_16 Depth 4
                                        #           Child Loop BB27_17 Depth 5
                                        #           Child Loop BB27_31 Depth 5
	movl	stack_POINTER(%rip), %eax
	.p2align	4, 0x90
.LBB27_6:                               #   Parent Loop BB27_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%eax, 140(%rsp)         # 4-byte Folded Reload
	je	.LBB27_70
# BB#7:                                 #   in Loop: Header=BB27_6 Depth=2
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %rbp
	cmpl	$0, (%rbp)
	jg	.LBB27_6
# BB#8:                                 #   in Loop: Header=BB27_5 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%rbx), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rbp, %rcx
	callq	st_GetUnifier
	testq	%rax, %rax
	je	.LBB27_5
# BB#9:                                 # %.lr.ph235.preheader
                                        #   in Loop: Header=BB27_5 Depth=1
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB27_10:                              # %.lr.ph235
                                        #   Parent Loop BB27_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_12 Depth 3
                                        #         Child Loop BB27_16 Depth 4
                                        #           Child Loop BB27_17 Depth 5
                                        #           Child Loop BB27_31 Depth 5
	movq	8(%rax), %r9
	movq	8(%r9), %r12
	testq	%r12, %r12
	je	.LBB27_69
# BB#11:                                # %.lr.ph230.preheader
                                        #   in Loop: Header=BB27_10 Depth=2
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%r9, 48(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB27_12:                              # %.lr.ph230
                                        #   Parent Loop BB27_5 Depth=1
                                        #     Parent Loop BB27_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB27_16 Depth 4
                                        #           Child Loop BB27_17 Depth 5
                                        #           Child Loop BB27_31 Depth 5
	movq	8(%r12), %rdi
	movl	fol_EQUALITY(%rip), %ecx
	cmpl	(%rdi), %ecx
	jne	.LBB27_68
# BB#13:                                #   in Loop: Header=BB27_12 Depth=3
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB27_14
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB27_12 Depth=3
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_16:                              #   Parent Loop BB27_5 Depth=1
                                        #     Parent Loop BB27_10 Depth=2
                                        #       Parent Loop BB27_12 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB27_17 Depth 5
                                        #           Child Loop BB27_31 Depth 5
	movq	8(%rbp), %r15
	movq	16(%r15), %r13
	movq	56(%r13), %rcx
	movl	$-1, %r14d
	.p2align	4, 0x90
.LBB27_17:                              #   Parent Loop BB27_5 Depth=1
                                        #     Parent Loop BB27_10 Depth=2
                                        #       Parent Loop BB27_12 Depth=3
                                        #         Parent Loop BB27_16 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%r14d
	cmpq	%r15, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB27_17
# BB#18:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB27_16 Depth=4
	testb	$2, 48(%r13)
	jne	.LBB27_67
# BB#19:                                #   in Loop: Header=BB27_16 Depth=4
	testl	%edx, %edx
	je	.LBB27_21
# BB#20:                                #   in Loop: Header=BB27_16 Depth=4
	testb	$2, (%r15)
	je	.LBB27_67
.LBB27_21:                              #   in Loop: Header=BB27_16 Depth=4
	testl	%esi, %esi
	je	.LBB27_24
# BB#22:                                #   in Loop: Header=BB27_16 Depth=4
	movq	16(%rdi), %rcx
	cmpq	8(%rcx), %r9
	je	.LBB27_24
# BB#23:                                #   in Loop: Header=BB27_16 Depth=4
	cmpl	$0, 8(%r15)
	jne	.LBB27_67
	.p2align	4, 0x90
.LBB27_24:                              #   in Loop: Header=BB27_16 Depth=4
	movq	24(%r15), %rbx
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rbx), %ecx
	je	.LBB27_67
# BB#25:                                #   in Loop: Header=BB27_16 Depth=4
	movl	(%r13), %ecx
	cmpl	(%r8), %ecx
	je	.LBB27_67
# BB#26:                                #   in Loop: Header=BB27_16 Depth=4
	cmpl	$0, 224(%rsp)
	je	.LBB27_28
# BB#27:                                #   in Loop: Header=BB27_16 Depth=4
	movl	68(%r13), %ecx
	addl	64(%r13), %ecx
	addl	72(%r13), %ecx
	cmpl	$1, %ecx
	jne	.LBB27_67
.LBB27_28:                              #   in Loop: Header=BB27_16 Depth=4
	movq	%r13, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB27_66
# BB#29:                                #   in Loop: Header=BB27_16 Depth=4
	movl	52(%r13), %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	88(%rsp), %rsi
	leaq	80(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB27_32
# BB#30:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB27_16 Depth=4
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB27_31:                              # %.lr.ph.i
                                        #   Parent Loop BB27_5 Depth=1
                                        #     Parent Loop BB27_10 Depth=2
                                        #       Parent Loop BB27_12 Depth=3
                                        #         Parent Loop BB27_16 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB27_31
.LBB27_32:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB27_16 Depth=4
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	40(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	je	.LBB27_37
# BB#33:                                #   in Loop: Header=BB27_16 Depth=4
	movq	80(%rsp), %r9
	testb	$2, 48(%rdi)
	jne	.LBB27_35
# BB#34:                                #   in Loop: Header=BB27_16 Depth=4
	movq	88(%rsp), %rcx
	movl	64(%rdi), %eax
	movl	68(%rdi), %edx
	leal	-1(%rax,%rdx), %eax
	xorl	%r8d, %r8d
	movl	108(%rsp), %esi         # 4-byte Reload
	cmpl	%esi, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi491:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	%r9, %rbx
	movq	240(%rsp), %r9
	pushq	248(%rsp)
.Lcfi492:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	movq	%rbx, %r9
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	$16, %rsp
.Lcfi493:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB27_65
.LBB27_35:                              #   in Loop: Header=BB27_16 Depth=4
	testb	$2, 48(%r13)
	jne	.LBB27_37
# BB#36:                                #   in Loop: Header=BB27_16 Depth=4
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	cmpl	%r14d, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi494:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	%r13, %rdi
	movl	%r14d, %esi
	movq	%r9, %rcx
	movq	240(%rsp), %r9
	pushq	248(%rsp)
.Lcfi495:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	$16, %rsp
.Lcfi496:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB27_65
.LBB27_37:                              # %inf_LiteralsMax.exit
                                        #   in Loop: Header=BB27_16 Depth=4
	testl	%edx, %edx
	je	.LBB27_38
# BB#39:                                #   in Loop: Header=BB27_16 Depth=4
	cmpl	$0, 8(%r15)
	je	.LBB27_40
.LBB27_38:                              #   in Loop: Header=BB27_16 Depth=4
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	movl	$1, %r15d
	testl	%ecx, %ecx
	jne	.LBB27_44
	jmp	.LBB27_47
.LBB27_40:                              #   in Loop: Header=BB27_16 Depth=4
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
	cmpq	%rbx, %rsi
	jne	.LBB27_42
# BB#41:                                #   in Loop: Header=BB27_16 Depth=4
	movq	(%rax), %rax
	movq	8(%rax), %rbx
.LBB27_42:                              #   in Loop: Header=BB27_16 Depth=4
	movq	80(%rsp), %r15
	movq	%rsi, %rdi
	callq	term_Copy
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r15
	movq	80(%rsp), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	term_Copy
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %rsi
	movq	232(%rsp), %rdx
	movq	240(%rsp), %rcx
	callq	ord_Compare
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	cmpl	$1, %eax
	setne	%r15b
	movq	40(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	je	.LBB27_47
.LBB27_44:                              #   in Loop: Header=BB27_16 Depth=4
	testl	%r15d, %r15d
	je	.LBB27_47
# BB#45:                                #   in Loop: Header=BB27_16 Depth=4
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 8(%rax)
	jne	.LBB27_47
# BB#46:                                #   in Loop: Header=BB27_16 Depth=4
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	%rax, %rdx
	cmovneq	%rcx, %rdx
	cmovneq	%rax, %rcx
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rdi
	movq	8(%rdx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	88(%rsp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	callq	term_Copy
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	term_Copy
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	movq	232(%rsp), %rdx
	movq	240(%rsp), %rcx
	callq	ord_Compare
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	setne	%cl
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movq	96(%rsp), %rdi          # 8-byte Reload
.LBB27_47:                              #   in Loop: Header=BB27_16 Depth=4
	testl	%r15d, %r15d
	je	.LBB27_61
# BB#48:                                #   in Loop: Header=BB27_16 Depth=4
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB27_61
# BB#49:                                #   in Loop: Header=BB27_16 Depth=4
	testq	%rbx, %rbx
	jne	.LBB27_53
# BB#50:                                #   in Loop: Header=BB27_16 Depth=4
	movq	%rdi, %r15
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	cmpq	%rdi, 48(%rsp)          # 8-byte Folded Reload
	jne	.LBB27_52
# BB#51:                                #   in Loop: Header=BB27_16 Depth=4
	movq	(%rax), %rax
	movq	8(%rax), %rdi
.LBB27_52:                              #   in Loop: Header=BB27_16 Depth=4
	movq	80(%rsp), %rbx
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movq	%r15, %rdi
.LBB27_53:                              #   in Loop: Header=BB27_16 Depth=4
	movq	%rbx, %r15
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	term_Copy
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	je	.LBB27_57
# BB#54:                                #   in Loop: Header=BB27_16 Depth=4
	movq	8(%rax), %rdi
	movq	8(%rcx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	je	.LBB27_59
# BB#55:                                #   in Loop: Header=BB27_16 Depth=4
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	jmp	.LBB27_56
.LBB27_57:                              #   in Loop: Header=BB27_16 Depth=4
	movq	8(%rcx), %rdi
	movq	8(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	je	.LBB27_59
# BB#58:                                #   in Loop: Header=BB27_16 Depth=4
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
.LBB27_56:                              # %inf_AllTermsLeftRplac.exit
                                        #   in Loop: Header=BB27_16 Depth=4
	movq	%r15, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	inf_NAllTermsRplac
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r15, %rbx
	movq	24(%rsp), %r10          # 8-byte Reload
	jmp	.LBB27_60
.LBB27_59:                              #   in Loop: Header=BB27_16 Depth=4
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	xorl	%r10d, %r10d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r15, %rbx
.LBB27_60:                              # %inf_AllTermsLeftRplac.exit
                                        #   in Loop: Header=BB27_16 Depth=4
	movq	80(%rsp), %rdx
	movq	88(%rsp), %r9
	movq	%r13, %rdi
	movl	%r14d, %esi
	movl	108(%rsp), %r8d         # 4-byte Reload
	pushq	240(%rsp)
.Lcfi497:
	.cfi_adjust_cfa_offset 8
	pushq	240(%rsp)
.Lcfi498:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi499:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi500:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi501:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi502:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyGenSuperposition
	addq	$48, %rsp
.Lcfi503:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rdi          # 8-byte Reload
.LBB27_61:                              #   in Loop: Header=BB27_16 Depth=4
	testq	%rdi, %rdi
	je	.LBB27_63
# BB#62:                                #   in Loop: Header=BB27_16 Depth=4
	callq	term_Delete
.LBB27_63:                              #   in Loop: Header=BB27_16 Depth=4
	testq	%rbx, %rbx
	je	.LBB27_65
# BB#64:                                #   in Loop: Header=BB27_16 Depth=4
	movq	%rbx, %rdi
	callq	term_Delete
.LBB27_65:                              # %inf_LiteralsMax.exit.thread
                                        #   in Loop: Header=BB27_16 Depth=4
	movq	88(%rsp), %rdi
	callq	subst_Delete
	movq	80(%rsp), %rdi
	callq	subst_Delete
.LBB27_66:                              #   in Loop: Header=BB27_16 Depth=4
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_67:                              #   in Loop: Header=BB27_16 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB27_16
	jmp	.LBB27_68
.LBB27_14:                              #   in Loop: Header=BB27_12 Depth=3
	movq	120(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB27_68:                              # %.loopexit
                                        #   in Loop: Header=BB27_12 Depth=3
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB27_12
.LBB27_69:                              # %._crit_edge
                                        #   in Loop: Header=BB27_10 Depth=2
	movq	(%rax), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rax
	movq	152(%rsp), %rbx         # 8-byte Reload
	jne	.LBB27_10
	jmp	.LBB27_5
.LBB27_70:
	movq	128(%rsp), %rax         # 8-byte Reload
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	inf_GenSPRightEqToGiven, .Lfunc_end27-inf_GenSPRightEqToGiven
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_Lit2MParamod,@function
inf_Lit2MParamod:                       # @inf_Lit2MParamod
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi504:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi505:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi506:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi507:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi508:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi509:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi510:
	.cfi_def_cfa_offset 160
.Lcfi511:
	.cfi_offset %rbx, -56
.Lcfi512:
	.cfi_offset %r12, -48
.Lcfi513:
	.cfi_offset %r13, -40
.Lcfi514:
	.cfi_offset %r14, -32
.Lcfi515:
	.cfi_offset %r15, -24
.Lcfi516:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	68(%r14), %eax
	movl	72(%r14), %ecx
	addl	64(%r14), %eax
	leal	-1(%rcx,%rax), %ecx
	cmpl	%ecx, %eax
	jle	.LBB28_2
# BB#1:
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB28_56
.LBB28_2:                               # %.lr.ph
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movslq	%edx, %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movslq	%eax, %r15
	movslq	%ecx, %rcx
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB28_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB28_18 Depth 2
                                        #     Child Loop BB28_27 Depth 2
                                        #     Child Loop BB28_41 Depth 2
                                        #     Child Loop BB28_53 Depth 2
	cmpl	%ebp, %r15d
	je	.LBB28_55
# BB#4:                                 #   in Loop: Header=BB28_3 Depth=1
	movq	56(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rbx
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB28_55
# BB#5:                                 #   in Loop: Header=BB28_3 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	184(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r14
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	176(%rsp), %rsi
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movq	%r13, %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB28_8
# BB#6:                                 #   in Loop: Header=BB28_3 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rsp, %rsi
	callq	subst_ExtractUnifierCom
	movq	%r12, %rbp
	movq	56(%rbp), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	cmpl	$0, 8(%rax)
	movq	%r14, 80(%rsp)          # 8-byte Spill
	je	.LBB28_9
# BB#7:                                 #   in Loop: Header=BB28_3 Depth=1
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	jmp	.LBB28_11
.LBB28_8:                               #   in Loop: Header=BB28_3 Depth=1
	xorps	%xmm0, %xmm0
	movq	%r14, %r13
	movq	48(%rsp), %r14          # 8-byte Reload
	jmp	.LBB28_25
.LBB28_9:                               #   in Loop: Header=BB28_3 Depth=1
	movq	(%rsp), %rbx
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	184(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movq	(%rsp), %r13
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rbp, %rdi
	movq	%r12, %rbp
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r13
	movq	%rbx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	192(%rsp), %rdx
	movq	200(%rsp), %rcx
	callq	ord_Compare
	decl	%eax
	cmpl	$2, %eax
	jae	.LBB28_11
# BB#10:                                #   in Loop: Header=BB28_3 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
	jmp	.LBB28_22
.LBB28_11:                              #   in Loop: Header=BB28_3 Depth=1
	movq	(%rsp), %rbx
	testb	$2, 48(%rbp)
	movq	48(%rsp), %r14          # 8-byte Reload
	jne	.LBB28_13
# BB#12:                                #   in Loop: Header=BB28_3 Depth=1
	movl	64(%rbp), %eax
	movl	68(%rbp), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	movl	36(%rsp), %esi          # 4-byte Reload
	cmpl	%esi, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi517:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	192(%rsp), %rcx
	movq	200(%rsp), %r9
	pushq	208(%rsp)
.Lcfi518:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMaxWith2Subst
	addq	$16, %rsp
.Lcfi519:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB28_22
.LBB28_13:                              #   in Loop: Header=BB28_3 Depth=1
	testb	$2, 48(%r14)
	jne	.LBB28_15
# BB#14:                                #   in Loop: Header=BB28_3 Depth=1
	movl	64(%r14), %eax
	movl	68(%r14), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	movl	12(%rsp), %esi          # 4-byte Reload
	cmpl	%esi, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi520:
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	192(%rsp), %rcx
	movq	200(%rsp), %r9
	pushq	208(%rsp)
.Lcfi521:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMaxWith2Subst
	addq	$16, %rsp
.Lcfi522:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB28_22
.LBB28_15:                              # %inf_LiteralsMaxWith2Subst.exit170
                                        #   in Loop: Header=BB28_3 Depth=1
	movq	(%rsp), %rbx
	movq	80(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r15d, %r8d
	movq	176(%rsp), %r9
	pushq	200(%rsp)
.Lcfi523:
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
.Lcfi524:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)
.Lcfi525:
	.cfi_adjust_cfa_offset 8
	pushq	208(%rsp)
.Lcfi526:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi527:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi528:
	.cfi_adjust_cfa_offset 8
	pushq	208(%rsp)
.Lcfi529:
	.cfi_adjust_cfa_offset 8
	pushq	224(%rsp)
.Lcfi530:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyMParamod
	addq	$64, %rsp
.Lcfi531:
	.cfi_adjust_cfa_offset -64
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB28_20
# BB#16:                                #   in Loop: Header=BB28_3 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB28_21
# BB#17:                                # %.preheader.i152.preheader
                                        #   in Loop: Header=BB28_3 Depth=1
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB28_18:                              # %.preheader.i152
                                        #   Parent Loop BB28_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB28_18
# BB#19:                                #   in Loop: Header=BB28_3 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	jmp	.LBB28_21
.LBB28_20:                              #   in Loop: Header=BB28_3 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB28_21:                              # %list_Nconc.exit154
                                        #   in Loop: Header=BB28_3 Depth=1
	movq	%rbx, %rdi
	callq	term_Delete
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB28_22:                              # %inf_LiteralsMaxWith2Subst.exit170.thread
                                        #   in Loop: Header=BB28_3 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %r12
	testq	%rdi, %rdi
	je	.LBB28_24
# BB#23:                                #   in Loop: Header=BB28_3 Depth=1
	callq	term_Delete
	movq	%r13, %rdi
	callq	term_Delete
.LBB28_24:                              #   in Loop: Header=BB28_3 Depth=1
	movq	(%rsp), %rdi
	callq	subst_Delete
	xorps	%xmm0, %xmm0
	movq	80(%rsp), %r13          # 8-byte Reload
.LBB28_25:                              #   in Loop: Header=BB28_3 Depth=1
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB28_28
# BB#26:                                # %.lr.ph.preheader.i144
                                        #   in Loop: Header=BB28_3 Depth=1
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB28_27:                              # %.lr.ph.i147
                                        #   Parent Loop BB28_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB28_27
.LBB28_28:                              # %cont_Reset.exit148
                                        #   in Loop: Header=BB28_3 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	176(%rsp), %rsi
	movq	%r13, %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB28_31
# BB#29:                                #   in Loop: Header=BB28_3 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rsp, %rsi
	callq	subst_ExtractUnifierCom
	movq	56(%r12), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	cmpl	$0, 8(%rax)
	je	.LBB28_32
# BB#30:                                #   in Loop: Header=BB28_3 Depth=1
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movl	12(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB28_34
.LBB28_31:                              #   in Loop: Header=BB28_3 Depth=1
	xorps	%xmm0, %xmm0
	movl	12(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB28_51
.LBB28_32:                              #   in Loop: Header=BB28_3 Depth=1
	movq	(%rsp), %rbx
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	184(%rsp), %r12
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r14
	movq	(%rsp), %rbx
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%r12, %rdi
	movl	12(%rsp), %ebp          # 4-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	192(%rsp), %rdx
	movq	200(%rsp), %rcx
	callq	ord_Compare
	decl	%eax
	cmpl	$2, %eax
	jae	.LBB28_34
# BB#33:                                #   in Loop: Header=BB28_3 Depth=1
	movq	40(%rsp), %r12          # 8-byte Reload
	testq	%r14, %r14
	jne	.LBB28_49
	jmp	.LBB28_50
.LBB28_34:                              #   in Loop: Header=BB28_3 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	(%rsp), %r12
	movq	40(%rsp), %rbx          # 8-byte Reload
	testb	$2, 48(%rbx)
	jne	.LBB28_36
# BB#35:                                #   in Loop: Header=BB28_3 Depth=1
	movl	64(%rbx), %eax
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	movl	36(%rsp), %esi          # 4-byte Reload
	cmpl	%esi, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi532:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	192(%rsp), %rcx
	movq	200(%rsp), %r9
	pushq	208(%rsp)
.Lcfi533:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMaxWith2Subst
	addq	$16, %rsp
.Lcfi534:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB28_44
.LBB28_36:                              #   in Loop: Header=BB28_3 Depth=1
	movq	48(%rsp), %rbx          # 8-byte Reload
	testb	$2, 48(%rbx)
	jne	.LBB28_38
# BB#37:                                #   in Loop: Header=BB28_3 Depth=1
	movl	64(%rbx), %eax
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	cmpl	%ebp, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi535:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	192(%rsp), %rcx
	movq	200(%rsp), %r9
	pushq	208(%rsp)
.Lcfi536:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMaxWith2Subst
	addq	$16, %rsp
.Lcfi537:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB28_46
.LBB28_38:                              # %inf_LiteralsMaxWith2Subst.exit
                                        #   in Loop: Header=BB28_3 Depth=1
	movq	(%rsp), %r12
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r12
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	%ebp, %ecx
	movl	%r15d, %r8d
	movq	176(%rsp), %r9
	pushq	200(%rsp)
.Lcfi538:
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
.Lcfi539:
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)
.Lcfi540:
	.cfi_adjust_cfa_offset 8
	pushq	208(%rsp)
.Lcfi541:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi542:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi543:
	.cfi_adjust_cfa_offset 8
	pushq	208(%rsp)
.Lcfi544:
	.cfi_adjust_cfa_offset 8
	pushq	224(%rsp)
.Lcfi545:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyMParamod
	addq	$64, %rsp
.Lcfi546:
	.cfi_adjust_cfa_offset -64
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB28_43
# BB#39:                                #   in Loop: Header=BB28_3 Depth=1
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB28_47
# BB#40:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB28_3 Depth=1
	movq	%rbp, %rcx
	movq	16(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB28_41:                              # %.preheader.i
                                        #   Parent Loop BB28_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB28_41
# BB#42:                                #   in Loop: Header=BB28_3 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	jmp	.LBB28_48
.LBB28_43:                              #   in Loop: Header=BB28_3 Depth=1
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB28_47:                              #   in Loop: Header=BB28_3 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB28_48:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB28_3 Depth=1
	movq	%r12, %rdi
	callq	term_Delete
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r12          # 8-byte Reload
	movl	12(%rsp), %ebp          # 4-byte Reload
	testq	%r14, %r14
	jne	.LBB28_49
	jmp	.LBB28_50
.LBB28_44:                              #   in Loop: Header=BB28_3 Depth=1
	movq	%rbx, %r12
	jmp	.LBB28_45
.LBB28_46:                              #   in Loop: Header=BB28_3 Depth=1
	movq	40(%rsp), %r12          # 8-byte Reload
.LBB28_45:                              #   in Loop: Header=BB28_3 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%r14, %r14
	je	.LBB28_50
.LBB28_49:                              #   in Loop: Header=BB28_3 Depth=1
	movq	%r14, %rdi
	callq	term_Delete
	movq	%rbx, %rdi
	callq	term_Delete
.LBB28_50:                              #   in Loop: Header=BB28_3 Depth=1
	movq	(%rsp), %rdi
	callq	subst_Delete
	movq	48(%rsp), %r14          # 8-byte Reload
	xorps	%xmm0, %xmm0
.LBB28_51:                              #   in Loop: Header=BB28_3 Depth=1
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	je	.LBB28_54
# BB#52:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB28_3 Depth=1
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB28_53:                              # %.lr.ph.i
                                        #   Parent Loop BB28_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB28_53
.LBB28_54:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB28_3 Depth=1
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movq	%r13, %rdi
	callq	term_Delete
	movq	96(%rsp), %rcx          # 8-byte Reload
.LBB28_55:                              #   in Loop: Header=BB28_3 Depth=1
	cmpq	%rcx, %r15
	leaq	1(%r15), %r15
	jl	.LBB28_3
.LBB28_56:                              # %._crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	inf_Lit2MParamod, .Lfunc_end28-inf_Lit2MParamod
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_ApplyMParamod,@function
inf_ApplyMParamod:                      # @inf_ApplyMParamod
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi547:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi548:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi549:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi550:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi551:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi552:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi553:
	.cfi_def_cfa_offset 160
.Lcfi554:
	.cfi_offset %rbx, -56
.Lcfi555:
	.cfi_offset %r12, -48
.Lcfi556:
	.cfi_offset %r13, -40
.Lcfi557:
	.cfi_offset %r14, -32
.Lcfi558:
	.cfi_offset %r15, -24
.Lcfi559:
	.cfi_offset %rbp, -16
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movl	%r8d, 52(%rsp)          # 4-byte Spill
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movl	%edx, 64(%rsp)          # 4-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	192(%rsp), %r13
	movl	64(%r14), %r15d
	movl	68(%r14), %eax
	leal	-1(%r15,%rax), %eax
	movl	72(%r14), %ecx
	movl	%eax, 84(%rsp)          # 4-byte Spill
	addl	%eax, %ecx
	movl	64(%r12), %ebx
	movl	68(%r12), %eax
	movl	72(%r12), %edx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leal	(%rcx,%rbx), %edi
	addl	%eax, %edi
	leal	-1(%rbx,%rax), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	addl	%edx, %edi
	leal	-1(%rbx), %ebp
	callq	clause_CreateBody
	movl	64(%r14), %ecx
	addl	64(%r12), %ecx
	movl	%ecx, 64(%rax)
	movl	68(%r14), %ecx
	addl	68(%r12), %ecx
	movl	%ecx, 68(%rax)
	movl	72(%r12), %edx
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movl	72(%r14), %ecx
	leal	-1(%rdx,%rcx), %ecx
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%ecx, 72(%rax)
	testl	%ebp, %ebp
	movq	%r12, 56(%rsp)          # 8-byte Spill
	js	.LBB29_1
# BB#2:                                 # %.lr.ph226
	xorl	%ebp, %ebp
	movq	40(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB29_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	200(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	clause_LiteralCreate
	movq	56(%r14), %rcx
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB29_3
	jmp	.LBB29_4
.LBB29_1:
	xorl	%ebx, %ebx
.LBB29_4:
	movl	8(%rsp), %eax           # 4-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	addl	%edx, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movslq	64(%rcx), %rax
	cmpl	%edx, %ebx
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jg	.LBB29_7
# BB#5:                                 # %.lr.ph221
	movslq	%ebx, %rbp
	movslq	%edx, %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	decq	%rbp
	leaq	(,%rax,8), %r13
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB29_6:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	192(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	200(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	clause_LiteralCreate
	movq	56(%r14), %rcx
	addq	%r13, %rcx
	movq	%rax, 8(%rcx,%rbp,8)
	incq	%rbp
	incl	%ebx
	cmpq	24(%rsp), %rbp          # 8-byte Folded Reload
	jl	.LBB29_6
.LBB29_7:                               # %._crit_edge222
	leal	-1(%r15), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	8(%rsp), %ecx           # 4-byte Reload
	cmpl	%ecx, %ebx
	movq	88(%rsp), %r13          # 8-byte Reload
	jg	.LBB29_13
# BB#8:                                 # %.lr.ph217
	movq	32(%rsp), %rax          # 8-byte Reload
	addl	68(%rax), %r13d
	movslq	%ebx, %r14
	movslq	%ecx, %rax
	decq	%r14
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB29_9:                               # =>This Inner Loop Header: Depth=1
	cmpl	%ebx, 64(%rsp)          # 4-byte Folded Reload
	jne	.LBB29_10
# BB#11:                                #   in Loop: Header=BB29_9 Depth=1
	decl	%r13d
	jmp	.LBB29_12
	.p2align	4, 0x90
.LBB29_10:                              #   in Loop: Header=BB29_9 Depth=1
	leal	(%r13,%rbx), %r12d
	movq	%rax, %r15
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	192(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	200(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	clause_LiteralCreate
	movq	56(%rbp), %rcx
	movslq	%r12d, %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	%r15, %rax
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB29_12:                              #   in Loop: Header=BB29_9 Depth=1
	incq	%r14
	incl	%ebx
	cmpq	%rax, %r14
	jl	.LBB29_9
.LBB29_13:                              # %._crit_edge218
	movq	56(%rsp), %rdx          # 8-byte Reload
	movslq	64(%rdx), %rax
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	js	.LBB29_14
# BB#15:                                # %.lr.ph211
	leaq	(,%rax,8), %r12
	xorl	%ebp, %ebp
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	192(%rsp), %r14
	.p2align	4, 0x90
.LBB29_16:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	200(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	clause_LiteralCreate
	movq	56(%rbx), %rcx
	addq	%r12, %rcx
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB29_16
# BB#17:
	movl	84(%rsp), %ecx          # 4-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB29_18
.LBB29_14:
	xorl	%r15d, %r15d
	movl	84(%rsp), %ecx          # 4-byte Reload
.LBB29_18:                              # %._crit_edge212
	movl	68(%rdx), %edx
	cmpl	%ecx, %r15d
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	jle	.LBB29_20
# BB#19:
	movq	200(%rsp), %r12
	jmp	.LBB29_22
.LBB29_20:                              # %.lr.ph207
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rdx,%rax), %eax
	movslq	%r15d, %rbx
	movslq	%eax, %r14
	movslq	%ecx, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	decq	%rbx
	shlq	$3, %r14
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	200(%rsp), %r12
	.p2align	4, 0x90
.LBB29_21:                              # =>This Inner Loop Header: Depth=1
	movq	56(%rbp), %rax
	movq	8(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	192(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	clause_LiteralCreate
	movq	56(%r13), %rcx
	addq	%r14, %rcx
	movq	%rax, 8(%rcx,%rbx,8)
	incq	%rbx
	incl	%r15d
	cmpq	64(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB29_21
.LBB29_22:                              # %._crit_edge208
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	72(%rax), %ebx
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, %r15d
	jg	.LBB29_40
# BB#23:                                # %.lr.ph
	movq	184(%rsp), %rcx
	movq	160(%rsp), %rdx
	cmpq	168(%rsp), %rdx
	movslq	%r15d, %r14
	cltq
	movq	%rax, 64(%rsp)          # 8-byte Spill
	je	.LBB29_27
# BB#24:                                # %.lr.ph.split.preheader
	decq	%r14
	movq	16(%rsp), %rax          # 8-byte Reload
	addl	%r15d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	leal	-1(%rbx,%rax), %ebx
	movl	52(%rsp), %r12d         # 4-byte Reload
	subl	%r15d, %r12d
	movl	48(%rsp), %r13d         # 4-byte Reload
	subl	%r15d, %r13d
	movq	%rcx, %r15
	.p2align	4, 0x90
.LBB29_25:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	testl	%r13d, %r13d
	je	.LBB29_26
# BB#35:                                #   in Loop: Header=BB29_25 Depth=1
	testl	%r12d, %r12d
	je	.LBB29_36
# BB#38:                                #   in Loop: Header=BB29_25 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	192(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	200(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	jmp	.LBB29_39
	.p2align	4, 0x90
.LBB29_26:                              #   in Loop: Header=BB29_25 Depth=1
	movq	160(%rsp), %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movq	168(%rsp), %rsi
	movq	176(%rsp), %rdx
	callq	term_ReplaceSubtermBy
	movl	fol_EQUALITY(%rip), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	192(%rsp), %rdi
	movq	%rbp, %rsi
	callq	subst_Apply
	movq	200(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	jmp	.LBB29_37
	.p2align	4, 0x90
.LBB29_36:                              #   in Loop: Header=BB29_25 Depth=1
	movl	fol_EQUALITY(%rip), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r15, %rdi
	callq	term_Copy
.LBB29_37:                              #   in Loop: Header=BB29_25 Depth=1
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbp, (%rax)
	movl	16(%rsp), %edi          # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
.LBB29_39:                              #   in Loop: Header=BB29_25 Depth=1
	movq	%rax, %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	callq	clause_LiteralCreate
	movq	56(%rbp), %rcx
	movslq	%ebx, %rbx
	movq	%rax, (%rcx,%rbx,8)
	incq	%r14
	incl	%ebx
	decl	%r12d
	decl	%r13d
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB29_25
	jmp	.LBB29_40
.LBB29_27:                              # %.lr.ph.split.us.preheader
	decq	%r14
	movq	16(%rsp), %rax          # 8-byte Reload
	addl	%r15d, %eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	leal	-1(%rbx,%rax), %ebx
	movl	52(%rsp), %r12d         # 4-byte Reload
	subl	%r15d, %r12d
	movl	48(%rsp), %r13d         # 4-byte Reload
	subl	%r15d, %r13d
	.p2align	4, 0x90
.LBB29_28:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	testl	%r13d, %r13d
	je	.LBB29_32
# BB#29:                                #   in Loop: Header=BB29_28 Depth=1
	testl	%r12d, %r12d
	je	.LBB29_31
# BB#30:                                #   in Loop: Header=BB29_28 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	192(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	200(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	jmp	.LBB29_34
	.p2align	4, 0x90
.LBB29_32:                              #   in Loop: Header=BB29_28 Depth=1
	movq	176(%rsp), %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movl	fol_EQUALITY(%rip), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	192(%rsp), %rdi
	movq	%rbp, %rsi
	callq	subst_Apply
	movq	200(%rsp), %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	jmp	.LBB29_33
	.p2align	4, 0x90
.LBB29_31:                              #   in Loop: Header=BB29_28 Depth=1
	movl	fol_EQUALITY(%rip), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	term_Copy
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	184(%rsp), %rdi
	callq	term_Copy
.LBB29_33:                              #   in Loop: Header=BB29_28 Depth=1
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movl	%r12d, %r15d
	movq	%rax, %r12
	movq	%rbp, 8(%r12)
	movq	$0, (%r12)
	movl	$16, %edi
	callq	memory_Malloc
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%r12, (%rax)
	movl	%r15d, %r12d
	movl	16(%rsp), %edi          # 4-byte Reload
	movq	%rax, %rsi
	callq	term_Create
.LBB29_34:                              #   in Loop: Header=BB29_28 Depth=1
	movq	%rax, %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	callq	clause_LiteralCreate
	movq	56(%rbp), %rcx
	movslq	%ebx, %rbx
	movq	%rax, (%rcx,%rbx,8)
	incq	%r14
	incl	%ebx
	decl	%r12d
	decl	%r13d
	cmpq	64(%rsp), %r14          # 8-byte Folded Reload
	jl	.LBB29_28
.LBB29_40:                              # %._crit_edge
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	$5, 76(%rbp)
	movq	32(%rsp), %r15          # 8-byte Reload
	movslq	(%r15), %rbx
	movq	32(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%rbp)
	movl	52(%rsp), %ebx          # 4-byte Reload
	movslq	%ebx, %r14
	movq	40(%rbp), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, 40(%rbp)
	movq	216(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movl	48(%rsp), %edx          # 4-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ebx, %r8d
	movq	208(%rsp), %r9
	callq	clause_SetDataFromParents
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	inf_ApplyMParamod, .Lfunc_end29-inf_ApplyMParamod
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_LitMaxWith2Subst,@function
inf_LitMaxWith2Subst:                   # @inf_LitMaxWith2Subst
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi560:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi561:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi562:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi563:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi564:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi565:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi566:
	.cfi_def_cfa_offset 112
.Lcfi567:
	.cfi_offset %rbx, -56
.Lcfi568:
	.cfi_offset %r12, -48
.Lcfi569:
	.cfi_offset %r13, -40
.Lcfi570:
	.cfi_offset %r14, -32
.Lcfi571:
	.cfi_offset %r15, -24
.Lcfi572:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movq	%rcx, %rbp
	movq	%rdx, %r13
	movq	%rdi, %r15
	movq	56(%r15), %rax
	movslq	%esi, %rbx
	movq	(%rax,%rbx,8), %rdi
	movl	(%rdi), %ecx
	xorl	%eax, %eax
	testb	$1, %cl
	je	.LBB30_23
# BB#1:
	testl	%r14d, %r14d
	je	.LBB30_3
# BB#2:
	andl	$2, %ecx
	je	.LBB30_23
.LBB30_3:
	movl	68(%r15), %ecx
	movl	72(%r15), %r8d
	leal	(%r8,%rcx), %edx
	movl	$1, %eax
	cmpl	$1, %edx
	je	.LBB30_23
# BB#4:
	movq	%r13, %rdx
	orq	%rbp, %rdx
	je	.LBB30_23
# BB#5:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%r9, 48(%rsp)           # 8-byte Spill
	addl	64(%r15), %ecx
	leal	-1(%r8,%rcx), %ebx
	movq	24(%rdi), %rdi
	movl	%esi, %r12d
	callq	term_Copy
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movl	%r12d, %ecx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movslq	64(%r15), %r12
	cmpl	%ebx, %r12d
	jg	.LBB30_22
# BB#6:                                 # %.lr.ph
	testl	%r14d, %r14d
	movslq	%ebx, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	je	.LBB30_7
# BB#13:                                # %.lr.ph.split.us.preheader
	movq	%r12, %r13
	decq	%r13
	negl	%r12d
	negl	%ecx
	.p2align	4, 0x90
.LBB30_14:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r12d, %ecx
	je	.LBB30_19
# BB#15:                                # %.lr.ph.split.us
                                        #   in Loop: Header=BB30_14 Depth=1
	cmpl	$1, %r12d
	je	.LBB30_19
# BB#16:                                #   in Loop: Header=BB30_14 Depth=1
	movq	56(%r15), %rax
	movq	8(%rax,%r13,8), %rax
	testb	$1, (%rax)
	je	.LBB30_19
# BB#17:                                #   in Loop: Header=BB30_14 Depth=1
	movl	%ecx, %ebx
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r14
	movq	56(%r15), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movl	8(%rcx), %esi
	movq	8(%rax,%r13,8), %rax
	movl	8(%rax), %ecx
	movq	112(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$1, %r8d
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rdx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	ord_LiteralCompare
	decl	%eax
	cmpl	$2, %eax
	jb	.LBB30_12
# BB#18:                                #   in Loop: Header=BB30_14 Depth=1
	movq	%r14, %rdi
	callq	term_Delete
	movl	%ebx, %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB30_19:                              #   in Loop: Header=BB30_14 Depth=1
	incq	%r13
	decl	%r12d
	cmpq	%rdx, %r13
	jl	.LBB30_14
	jmp	.LBB30_22
.LBB30_7:                               # %.lr.ph.split.preheader
	movq	%r12, %rbp
	decq	%rbp
	negl	%r12d
	negl	%ecx
	.p2align	4, 0x90
.LBB30_8:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	cmpl	%r12d, %ecx
	je	.LBB30_21
# BB#9:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB30_8 Depth=1
	cmpl	$1, %r12d
	je	.LBB30_21
# BB#10:                                #   in Loop: Header=BB30_8 Depth=1
	movq	56(%r15), %rax
	movq	8(%rax,%rbp,8), %rax
	testb	$1, (%rax)
	je	.LBB30_21
# BB#11:                                #   in Loop: Header=BB30_8 Depth=1
	movl	%ecx, %ebx
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r14
	movq	56(%r15), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movl	8(%rcx), %esi
	movq	8(%rax,%rbp,8), %rax
	movl	8(%rax), %ecx
	movq	112(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$1, %r8d
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rdx
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	ord_LiteralCompare
	cmpl	$1, %eax
	je	.LBB30_12
# BB#20:                                #   in Loop: Header=BB30_8 Depth=1
	movq	%r14, %rdi
	callq	term_Delete
	movl	%ebx, %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB30_21:                              #   in Loop: Header=BB30_8 Depth=1
	incq	%rbp
	decl	%r12d
	cmpq	%rdx, %rbp
	jl	.LBB30_8
.LBB30_22:                              # %._crit_edge
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	term_Delete
	movl	$1, %eax
.LBB30_23:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB30_12:                              # %.us-lcssa.us
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	term_Delete
	movq	%r14, %rdi
	callq	term_Delete
	xorl	%eax, %eax
	jmp	.LBB30_23
.Lfunc_end30:
	.size	inf_LitMaxWith2Subst, .Lfunc_end30-inf_LitMaxWith2Subst
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_GenSPLeftEqToGiven,@function
inf_GenSPLeftEqToGiven:                 # @inf_GenSPLeftEqToGiven
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi573:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi574:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi575:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi576:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi577:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi578:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi579:
	.cfi_def_cfa_offset 224
.Lcfi580:
	.cfi_offset %rbx, -56
.Lcfi581:
	.cfi_offset %r12, -48
.Lcfi582:
	.cfi_offset %r13, -40
.Lcfi583:
	.cfi_offset %r14, -32
.Lcfi584:
	.cfi_offset %r15, -24
.Lcfi585:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%r9, 40(%rsp)           # 8-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rcx, %rbx
	movl	%edx, 76(%rsp)          # 4-byte Spill
	movq	56(%rdi), %rax
	movl	%esi, 108(%rsp)         # 4-byte Spill
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB31_2
# BB#1:
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB31_2:                               # %clause_LiteralAtom.exit
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	stack_POINTER(%rip), %eax
	movl	%eax, 140(%rsp)         # 4-byte Spill
	movq	16(%rcx), %rax
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	jne	.LBB31_4
# BB#3:
	movq	(%rax), %rax
.LBB31_4:                               # %.preheader
	movq	8(%rax), %rdi
	callq	sharing_PushOnStack
	xorl	%eax, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
.LBB31_5:                               # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_6 Depth 2
                                        #     Child Loop BB31_10 Depth 2
                                        #       Child Loop BB31_12 Depth 3
                                        #         Child Loop BB31_16 Depth 4
                                        #           Child Loop BB31_17 Depth 5
                                        #           Child Loop BB31_31 Depth 5
	movl	stack_POINTER(%rip), %eax
	.p2align	4, 0x90
.LBB31_6:                               #   Parent Loop BB31_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%eax, 140(%rsp)         # 4-byte Folded Reload
	je	.LBB31_70
# BB#7:                                 #   in Loop: Header=BB31_6 Depth=2
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %rbp
	cmpl	$0, (%rbp)
	jg	.LBB31_6
# BB#8:                                 #   in Loop: Header=BB31_5 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%rbx), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rbp, %rcx
	callq	st_GetUnifier
	testq	%rax, %rax
	je	.LBB31_5
# BB#9:                                 # %.lr.ph233.preheader
                                        #   in Loop: Header=BB31_5 Depth=1
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB31_10:                              # %.lr.ph233
                                        #   Parent Loop BB31_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_12 Depth 3
                                        #         Child Loop BB31_16 Depth 4
                                        #           Child Loop BB31_17 Depth 5
                                        #           Child Loop BB31_31 Depth 5
	movq	8(%rax), %r9
	movq	8(%r9), %r12
	testq	%r12, %r12
	je	.LBB31_69
# BB#11:                                # %.lr.ph228.preheader
                                        #   in Loop: Header=BB31_10 Depth=2
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%r9, 48(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB31_12:                              # %.lr.ph228
                                        #   Parent Loop BB31_5 Depth=1
                                        #     Parent Loop BB31_10 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB31_16 Depth 4
                                        #           Child Loop BB31_17 Depth 5
                                        #           Child Loop BB31_31 Depth 5
	movq	8(%r12), %rdi
	movl	fol_EQUALITY(%rip), %ecx
	cmpl	(%rdi), %ecx
	jne	.LBB31_68
# BB#13:                                #   in Loop: Header=BB31_12 Depth=3
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB31_14
# BB#15:                                # %.lr.ph
                                        #   in Loop: Header=BB31_12 Depth=3
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB31_16:                              #   Parent Loop BB31_5 Depth=1
                                        #     Parent Loop BB31_10 Depth=2
                                        #       Parent Loop BB31_12 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB31_17 Depth 5
                                        #           Child Loop BB31_31 Depth 5
	movq	8(%rbp), %r15
	movq	16(%r15), %r13
	movq	56(%r13), %rcx
	movl	$-1, %r14d
	.p2align	4, 0x90
.LBB31_17:                              #   Parent Loop BB31_5 Depth=1
                                        #     Parent Loop BB31_10 Depth=2
                                        #       Parent Loop BB31_12 Depth=3
                                        #         Parent Loop BB31_16 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%r14d
	cmpq	%r15, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB31_17
# BB#18:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB31_16 Depth=4
	testb	$2, 48(%r13)
	jne	.LBB31_67
# BB#19:                                #   in Loop: Header=BB31_16 Depth=4
	testl	%edx, %edx
	je	.LBB31_21
# BB#20:                                #   in Loop: Header=BB31_16 Depth=4
	testb	$2, (%r15)
	je	.LBB31_67
.LBB31_21:                              #   in Loop: Header=BB31_16 Depth=4
	testl	%esi, %esi
	je	.LBB31_24
# BB#22:                                #   in Loop: Header=BB31_16 Depth=4
	movq	16(%rdi), %rcx
	cmpq	8(%rcx), %r9
	je	.LBB31_24
# BB#23:                                #   in Loop: Header=BB31_16 Depth=4
	cmpl	$0, 8(%r15)
	jne	.LBB31_67
	.p2align	4, 0x90
.LBB31_24:                              #   in Loop: Header=BB31_16 Depth=4
	movq	24(%r15), %rbx
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rbx), %ecx
	je	.LBB31_67
# BB#25:                                #   in Loop: Header=BB31_16 Depth=4
	movl	(%r13), %ecx
	cmpl	(%r8), %ecx
	je	.LBB31_67
# BB#26:                                #   in Loop: Header=BB31_16 Depth=4
	cmpl	$0, 224(%rsp)
	je	.LBB31_28
# BB#27:                                #   in Loop: Header=BB31_16 Depth=4
	movl	68(%r13), %ecx
	addl	64(%r13), %ecx
	addl	72(%r13), %ecx
	cmpl	$1, %ecx
	jne	.LBB31_67
.LBB31_28:                              #   in Loop: Header=BB31_16 Depth=4
	movq	%r13, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB31_66
# BB#29:                                #   in Loop: Header=BB31_16 Depth=4
	movl	52(%r13), %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	clause_RenameVarsBiggerThan
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	unify_UnifyNoOC
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	88(%rsp), %rsi
	leaq	80(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB31_32
# BB#30:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB31_16 Depth=4
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB31_31:                              # %.lr.ph.i
                                        #   Parent Loop BB31_5 Depth=1
                                        #     Parent Loop BB31_10 Depth=2
                                        #       Parent Loop BB31_12 Depth=3
                                        #         Parent Loop BB31_16 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB31_31
.LBB31_32:                              # %cont_Reset.exit
                                        #   in Loop: Header=BB31_16 Depth=4
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	40(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	je	.LBB31_37
# BB#33:                                #   in Loop: Header=BB31_16 Depth=4
	movq	80(%rsp), %r9
	testb	$2, 48(%rdi)
	jne	.LBB31_35
# BB#34:                                #   in Loop: Header=BB31_16 Depth=4
	movq	88(%rsp), %rcx
	movl	64(%rdi), %eax
	movl	68(%rdi), %edx
	leal	-1(%rax,%rdx), %eax
	xorl	%r8d, %r8d
	movl	108(%rsp), %esi         # 4-byte Reload
	cmpl	%esi, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi586:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	%r9, %rbx
	movq	240(%rsp), %r9
	pushq	248(%rsp)
.Lcfi587:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	movq	%rbx, %r9
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	$16, %rsp
.Lcfi588:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB31_65
.LBB31_35:                              #   in Loop: Header=BB31_16 Depth=4
	testb	$2, 48(%r13)
	jne	.LBB31_37
# BB#36:                                #   in Loop: Header=BB31_16 Depth=4
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
	leal	-1(%rax,%rcx), %eax
	xorl	%r8d, %r8d
	cmpl	%r14d, %eax
	setl	%r8b
	subq	$8, %rsp
.Lcfi589:
	.cfi_adjust_cfa_offset 8
	movl	$-1, %edx
	movq	%r13, %rdi
	movl	%r14d, %esi
	movq	%r9, %rcx
	movq	240(%rsp), %r9
	pushq	248(%rsp)
.Lcfi590:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMax
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	$16, %rsp
.Lcfi591:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB31_65
.LBB31_37:                              # %inf_LiteralsMax.exit
                                        #   in Loop: Header=BB31_16 Depth=4
	testl	%edx, %edx
	je	.LBB31_38
# BB#39:                                #   in Loop: Header=BB31_16 Depth=4
	cmpl	$0, 8(%r15)
	je	.LBB31_40
.LBB31_38:                              #   in Loop: Header=BB31_16 Depth=4
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	movl	$1, %r15d
	testl	%ecx, %ecx
	jne	.LBB31_44
	jmp	.LBB31_47
.LBB31_40:                              #   in Loop: Header=BB31_16 Depth=4
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
	cmpq	%rbx, %rsi
	jne	.LBB31_42
# BB#41:                                #   in Loop: Header=BB31_16 Depth=4
	movq	(%rax), %rax
	movq	8(%rax), %rbx
.LBB31_42:                              #   in Loop: Header=BB31_16 Depth=4
	movq	80(%rsp), %r15
	movq	%rsi, %rdi
	callq	term_Copy
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %r15
	movq	80(%rsp), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	term_Copy
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, %rsi
	movq	232(%rsp), %rdx
	movq	240(%rsp), %rcx
	callq	ord_Compare
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	cmpl	$1, %eax
	setne	%r15b
	movq	40(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	je	.LBB31_47
.LBB31_44:                              #   in Loop: Header=BB31_16 Depth=4
	testl	%r15d, %r15d
	je	.LBB31_47
# BB#45:                                #   in Loop: Header=BB31_16 Depth=4
	movq	160(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 8(%rax)
	jne	.LBB31_47
# BB#46:                                #   in Loop: Header=BB31_16 Depth=4
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	%rax, %rdx
	cmovneq	%rcx, %rdx
	cmovneq	%rax, %rcx
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rdi
	movq	8(%rdx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	88(%rsp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	callq	term_Copy
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	term_Copy
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	movq	232(%rsp), %rdx
	movq	240(%rsp), %rcx
	callq	ord_Compare
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	setne	%cl
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movq	96(%rsp), %rdi          # 8-byte Reload
.LBB31_47:                              #   in Loop: Header=BB31_16 Depth=4
	testl	%r15d, %r15d
	je	.LBB31_61
# BB#48:                                #   in Loop: Header=BB31_16 Depth=4
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB31_61
# BB#49:                                #   in Loop: Header=BB31_16 Depth=4
	testq	%rbx, %rbx
	jne	.LBB31_53
# BB#50:                                #   in Loop: Header=BB31_16 Depth=4
	movq	%rdi, %r15
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	8(%rax), %rdi
	cmpq	%rdi, 48(%rsp)          # 8-byte Folded Reload
	jne	.LBB31_52
# BB#51:                                #   in Loop: Header=BB31_16 Depth=4
	movq	(%rax), %rax
	movq	8(%rax), %rdi
.LBB31_52:                              #   in Loop: Header=BB31_16 Depth=4
	movq	80(%rsp), %rbx
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movq	%r15, %rdi
.LBB31_53:                              #   in Loop: Header=BB31_16 Depth=4
	movq	%rbx, %r15
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	144(%rsp), %rdi         # 8-byte Reload
	callq	term_Copy
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	je	.LBB31_57
# BB#54:                                #   in Loop: Header=BB31_16 Depth=4
	movq	8(%rax), %rdi
	movq	8(%rcx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	je	.LBB31_59
# BB#55:                                #   in Loop: Header=BB31_16 Depth=4
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	jmp	.LBB31_56
.LBB31_57:                              #   in Loop: Header=BB31_16 Depth=4
	movq	8(%rcx), %rdi
	movq	8(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	%r15, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	inf_NAllTermsRplac
	testl	%eax, %eax
	je	.LBB31_59
# BB#58:                                #   in Loop: Header=BB31_16 Depth=4
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
.LBB31_56:                              # %inf_AllTermsLeftRplac.exit
                                        #   in Loop: Header=BB31_16 Depth=4
	movq	%r15, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	inf_NAllTermsRplac
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r15, %rbx
	movq	24(%rsp), %r10          # 8-byte Reload
	jmp	.LBB31_60
.LBB31_59:                              #   in Loop: Header=BB31_16 Depth=4
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	xorl	%r10d, %r10d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r15, %rbx
.LBB31_60:                              # %inf_AllTermsLeftRplac.exit
                                        #   in Loop: Header=BB31_16 Depth=4
	movq	80(%rsp), %rdx
	movq	88(%rsp), %r9
	movq	%r13, %rdi
	movl	%r14d, %esi
	movl	108(%rsp), %r8d         # 4-byte Reload
	pushq	240(%rsp)
.Lcfi592:
	.cfi_adjust_cfa_offset 8
	pushq	240(%rsp)
.Lcfi593:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi594:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi595:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi596:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi597:
	.cfi_adjust_cfa_offset 8
	callq	inf_ApplyGenSuperposition
	addq	$48, %rsp
.Lcfi598:
	.cfi_adjust_cfa_offset -48
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rdi          # 8-byte Reload
.LBB31_61:                              #   in Loop: Header=BB31_16 Depth=4
	testq	%rdi, %rdi
	je	.LBB31_63
# BB#62:                                #   in Loop: Header=BB31_16 Depth=4
	callq	term_Delete
.LBB31_63:                              #   in Loop: Header=BB31_16 Depth=4
	testq	%rbx, %rbx
	je	.LBB31_65
# BB#64:                                #   in Loop: Header=BB31_16 Depth=4
	movq	%rbx, %rdi
	callq	term_Delete
.LBB31_65:                              # %inf_LiteralsMax.exit.thread
                                        #   in Loop: Header=BB31_16 Depth=4
	movq	88(%rsp), %rdi
	callq	subst_Delete
	movq	80(%rsp), %rdi
	callq	subst_Delete
.LBB31_66:                              #   in Loop: Header=BB31_16 Depth=4
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB31_67:                              #   in Loop: Header=BB31_16 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB31_16
	jmp	.LBB31_68
.LBB31_14:                              #   in Loop: Header=BB31_12 Depth=3
	movq	120(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB31_68:                              # %.loopexit
                                        #   in Loop: Header=BB31_12 Depth=3
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB31_12
.LBB31_69:                              # %._crit_edge
                                        #   in Loop: Header=BB31_10 Depth=2
	movq	(%rax), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rax
	movq	152(%rsp), %rbx         # 8-byte Reload
	jne	.LBB31_10
	jmp	.LBB31_5
.LBB31_70:
	movq	128(%rsp), %rax         # 8-byte Reload
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	inf_GenSPLeftEqToGiven, .Lfunc_end31-inf_GenSPLeftEqToGiven
	.cfi_endproc

	.p2align	4, 0x90
	.type	inf_HyperResolvents,@function
inf_HyperResolvents:                    # @inf_HyperResolvents
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi599:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi600:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi601:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi602:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi603:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi604:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi605:
	.cfi_def_cfa_offset 208
.Lcfi606:
	.cfi_offset %rbx, -56
.Lcfi607:
	.cfi_offset %r12, -48
.Lcfi608:
	.cfi_offset %r13, -40
.Lcfi609:
	.cfi_offset %r14, -32
.Lcfi610:
	.cfi_offset %r15, -24
.Lcfi611:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movl	%ecx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movabsq	$4294967296, %r15       # imm = 0x100000000
	testq	%rdx, %rdx
	movl	%r12d, 52(%rsp)         # 4-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	je	.LBB32_1
# BB#79:
	movq	%rdx, %rdi
	callq	list_Copy
	movl	$clause_HyperLiteralIsBetter, %ecx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %edx
	callq	clause_MoveBestLiteralToFront
	movq	(%rax), %rsi
	movq	8(%rax), %rdi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	24(%rdi), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	jne	.LBB32_81
# BB#80:
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB32_81:                              # %clause_LiteralAtom.exit149
	movl	%ebp, 88(%rsp)          # 4-byte Spill
	callq	term_Copy
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$0, 56(%rsp)            # 4-byte Folded Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jmp	.LBB32_82
	.p2align	4, 0x90
.LBB32_83:                              #   in Loop: Header=BB32_82 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB32_131
	.p2align	4, 0x90
.LBB32_111:                             #   in Loop: Header=BB32_82 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	52(%rsp), %r12d         # 4-byte Reload
.LBB32_131:                             # %inf_GetHyperResolutionPartnerLits.exit._crit_edge
                                        #   in Loop: Header=BB32_82 Depth=1
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movq	72(%rsp), %rcx          # 8-byte Reload
	jne	.LBB32_134
# BB#132:                               #   in Loop: Header=BB32_82 Depth=1
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB32_134
# BB#133:                               #   in Loop: Header=BB32_82 Depth=1
	movq	16(%rcx), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	8(%rsi), %rsi
	movq	%rsi, 8(%rax)
	movq	16(%rcx), %rax
	movq	(%rax), %rax
	movq	%rdx, 8(%rax)
	movl	$1, 56(%rsp)            # 4-byte Folded Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
.LBB32_82:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_85 Depth 2
                                        #       Child Loop BB32_88 Depth 3
                                        #         Child Loop BB32_89 Depth 4
                                        #     Child Loop BB32_98 Depth 2
                                        #       Child Loop BB32_101 Depth 3
                                        #         Child Loop BB32_102 Depth 4
                                        #     Child Loop BB32_113 Depth 2
                                        #       Child Loop BB32_114 Depth 3
                                        #       Child Loop BB32_123 Depth 3
                                        #       Child Loop BB32_128 Depth 3
	movq	208(%rsp), %rax
	movq	(%rax), %rsi
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	callq	st_GetUnifier
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB32_83
# BB#84:                                # %.lr.ph6.i
                                        #   in Loop: Header=BB32_82 Depth=1
	xorl	%r14d, %r14d
	testl	%r12d, %r12d
	je	.LBB32_98
	.p2align	4, 0x90
.LBB32_85:                              # %.lr.ph6.i.split
                                        #   Parent Loop BB32_82 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB32_88 Depth 3
                                        #         Child Loop BB32_89 Depth 4
	movq	8(%r13), %rdi
	cmpl	$0, (%rdi)
	jg	.LBB32_97
# BB#86:                                #   in Loop: Header=BB32_85 Depth=2
	callq	sharing_NAtomDataList
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB32_88
	jmp	.LBB32_97
	.p2align	4, 0x90
.LBB32_96:                              #   in Loop: Header=BB32_88 Depth=3
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.LBB32_97
.LBB32_88:                              # %.lr.ph.split.i
                                        #   Parent Loop BB32_82 Depth=1
                                        #     Parent Loop BB32_85 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB32_89 Depth 4
	movq	8(%r12), %rbx
	movq	16(%rbx), %rbp
	movq	56(%rbp), %rcx
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB32_89:                              #   Parent Loop BB32_82 Depth=1
                                        #     Parent Loop BB32_85 Depth=2
                                        #       Parent Loop BB32_88 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%eax
	cmpq	%rbx, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB32_89
# BB#90:                                # %clause_LiteralIsFromSuccedent.exit.i
                                        #   in Loop: Header=BB32_88 Depth=3
	movl	68(%rbp), %ecx
	addl	64(%rbp), %ecx
	cmpl	%ecx, %eax
	jl	.LBB32_96
# BB#91:                                # %clause_LiteralIsFromSuccedent.exit.i
                                        #   in Loop: Header=BB32_88 Depth=3
	movl	72(%rbp), %edx
	leal	-1(%rdx,%rcx), %ecx
	cmpl	%ecx, %eax
	jg	.LBB32_96
# BB#92:                                #   in Loop: Header=BB32_88 Depth=3
	testb	$2, (%rbx)
	je	.LBB32_96
# BB#93:                                #   in Loop: Header=BB32_88 Depth=3
	movq	%rbp, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB32_96
# BB#94:                                #   in Loop: Header=BB32_88 Depth=3
	cmpl	$0, 68(%rbp)
	jne	.LBB32_96
# BB#95:                                #   in Loop: Header=BB32_88 Depth=3
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
	jmp	.LBB32_96
	.p2align	4, 0x90
.LBB32_97:                              # %.loopexit.i
                                        #   in Loop: Header=BB32_85 Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB32_85
	jmp	.LBB32_110
	.p2align	4, 0x90
.LBB32_98:                              # %.lr.ph6.i.split.us
                                        #   Parent Loop BB32_82 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB32_101 Depth 3
                                        #         Child Loop BB32_102 Depth 4
	movq	8(%r13), %rdi
	cmpl	$0, (%rdi)
	jg	.LBB32_109
# BB#99:                                #   in Loop: Header=BB32_98 Depth=2
	callq	sharing_NAtomDataList
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB32_101
	jmp	.LBB32_109
	.p2align	4, 0x90
.LBB32_108:                             #   in Loop: Header=BB32_101 Depth=3
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.LBB32_109
.LBB32_101:                             # %.lr.ph.split.us.i.us
                                        #   Parent Loop BB32_82 Depth=1
                                        #     Parent Loop BB32_98 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB32_102 Depth 4
	movq	8(%r12), %rbx
	movq	16(%rbx), %rbp
	movq	56(%rbp), %rcx
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB32_102:                             #   Parent Loop BB32_82 Depth=1
                                        #     Parent Loop BB32_98 Depth=2
                                        #       Parent Loop BB32_101 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%eax
	cmpq	%rbx, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB32_102
# BB#103:                               # %clause_LiteralIsFromSuccedent.exit.us.i.us
                                        #   in Loop: Header=BB32_101 Depth=3
	movl	68(%rbp), %ecx
	addl	64(%rbp), %ecx
	cmpl	%ecx, %eax
	jl	.LBB32_108
# BB#104:                               # %clause_LiteralIsFromSuccedent.exit.us.i.us
                                        #   in Loop: Header=BB32_101 Depth=3
	movl	72(%rbp), %edx
	leal	-1(%rdx,%rcx), %ecx
	cmpl	%ecx, %eax
	jg	.LBB32_108
# BB#105:                               #   in Loop: Header=BB32_101 Depth=3
	movq	%rbp, %rdi
	callq	clause_HasSolvedConstraint
	testl	%eax, %eax
	je	.LBB32_108
# BB#106:                               #   in Loop: Header=BB32_101 Depth=3
	cmpl	$0, 68(%rbp)
	jne	.LBB32_108
# BB#107:                               #   in Loop: Header=BB32_101 Depth=3
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
	jmp	.LBB32_108
	.p2align	4, 0x90
.LBB32_109:                             # %.loopexit.i.us
                                        #   in Loop: Header=BB32_98 Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB32_98
.LBB32_110:                             # %inf_GetHyperResolutionPartnerLits.exit.preheader
                                        #   in Loop: Header=BB32_82 Depth=1
	testq	%r14, %r14
	je	.LBB32_111
# BB#112:                               # %.lr.ph168.preheader
                                        #   in Loop: Header=BB32_82 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	.p2align	4, 0x90
.LBB32_113:                             # %.lr.ph168
                                        #   Parent Loop BB32_82 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB32_114 Depth 3
                                        #       Child Loop BB32_123 Depth 3
                                        #       Child Loop BB32_128 Depth 3
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%r14), %rax
	movq	16(%rax), %rdi
	movq	56(%rdi), %rcx
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB32_114:                             #   Parent Loop BB32_82 Depth=1
                                        #     Parent Loop BB32_113 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	%r15, %rbx
	cmpq	%rax, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB32_114
# BB#115:                               # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB32_113 Depth=2
	callq	clause_Copy
	movq	%rax, %r13
	movq	%r13, %rdi
	movl	88(%rsp), %esi          # 4-byte Reload
	callq	clause_RenameVarsBiggerThan
	movq	56(%r13), %rax
	sarq	$29, %rbx
	movq	(%rax,%rbx), %r12
	movq	24(%r12), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB32_117
# BB#116:                               #   in Loop: Header=BB32_113 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB32_117:                             # %clause_LiteralAtom.exit119
                                        #   in Loop: Header=BB32_113 Depth=2
	callq	term_MaxVar
	movl	%eax, %ebp
	movl	88(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %ebp
	cmovll	%eax, %ebp
	callq	cont_Check
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	24(%r12), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB32_119
# BB#118:                               #   in Loop: Header=BB32_113 Depth=2
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB32_119:                             # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB32_113 Depth=2
	movq	72(%rsp), %rsi          # 8-byte Reload
	callq	unify_UnifyNoOC
	testl	%eax, %eax
	je	.LBB32_120
# BB#121:                               #   in Loop: Header=BB32_113 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	leaq	24(%rsp), %rsi
	leaq	104(%rsp), %rcx
	callq	subst_ExtractUnifier
	movq	cont_LASTBINDING(%rip), %rax
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	je	.LBB32_124
# BB#122:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB32_113 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	decl	%ecx
	.p2align	4, 0x90
.LBB32_123:                             # %.lr.ph.i110
                                        #   Parent Loop BB32_82 Depth=1
                                        #     Parent Loop BB32_113 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	movl	%ecx, cont_BINDINGS(%rip)
	movq	cont_LASTBINDING(%rip), %rax
	decl	%ecx
	testq	%rax, %rax
	jne	.LBB32_123
.LBB32_124:                             # %cont_Reset.exit
                                        #   in Loop: Header=BB32_113 Depth=2
	movl	$0, cont_BINDINGS(%rip)
	movl	$1, cont_STACKPOINTER(%rip)
	movl	$2000, cont_INDEXVARSCANNER(%rip) # imm = 0x7D0
	movq	24(%rsp), %rbx
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	subst_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Compose
	movq	%rax, 24(%rsp)
	movq	%rbx, %rdi
	callq	subst_Delete
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 112(%rsp)
	movq	%r12, 120(%rsp)
	movq	104(%rsp), %rax
	movq	%rax, 128(%rsp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	leaq	112(%rsp), %rax
	movq	%rax, 8(%rbx)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movq	24(%rsp), %rsi
	subq	$8, %rsp
.Lcfi612:
	.cfi_adjust_cfa_offset 8
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movl	%ebp, %ecx
	movq	%rbx, %r8
	movl	60(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %r9d
	pushq	232(%rsp)
.Lcfi613:
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
.Lcfi614:
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
.Lcfi615:
	.cfi_adjust_cfa_offset 8
	callq	inf_HyperResolvents
	addq	$32, %rsp
.Lcfi616:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB32_125
# BB#126:                               #   in Loop: Header=BB32_113 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB32_130
# BB#127:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB32_113 Depth=2
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB32_128:                             # %.preheader.i
                                        #   Parent Loop BB32_82 Depth=1
                                        #     Parent Loop BB32_113 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB32_128
# BB#129:                               #   in Loop: Header=BB32_113 Depth=2
	movq	%rdx, (%rax)
	jmp	.LBB32_130
	.p2align	4, 0x90
.LBB32_125:                             #   in Loop: Header=BB32_113 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB32_130:                             # %list_Nconc.exit
                                        #   in Loop: Header=BB32_113 Depth=2
	movq	24(%rsp), %rdi
	callq	subst_Delete
	movq	104(%rsp), %rdi
	callq	subst_Delete
	movq	%r13, %rdi
	callq	clause_Delete
	movq	(%rbx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	movq	%rbp, %rax
	jne	.LBB32_113
	jmp	.LBB32_131
.LBB32_134:
	movq	80(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB32_136
	.p2align	4, 0x90
.LBB32_135:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rdi
	movslq	32(%rdi), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rdi), %rdx
	movq	%rdx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rsi, (%rdx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB32_135
.LBB32_136:                             # %list_Delete.exit
	movq	%rcx, %rdi
	callq	term_Delete
	jmp	.LBB32_137
.LBB32_1:
	testl	%r12d, %r12d
	je	.LBB32_8
# BB#2:
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB32_8
# BB#3:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	movq	64(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB32_4:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_5 Depth 2
	movq	8(%rbx), %rax
	movq	8(%rax), %rcx
	movq	16(%rcx), %rdi
	movq	56(%rdi), %rdx
	movl	$-1, %esi
	.p2align	4, 0x90
.LBB32_5:                               #   Parent Loop BB32_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	cmpq	%rcx, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB32_5
# BB#6:                                 # %clause_LiteralGetIndex.exit133
                                        #   in Loop: Header=BB32_4 Depth=1
	movq	16(%rax), %rcx
	subq	$8, %rsp
.Lcfi617:
	.cfi_adjust_cfa_offset 8
	movl	$1, %r8d
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	224(%rsp), %r9
	pushq	232(%rsp)
.Lcfi618:
	.cfi_adjust_cfa_offset 8
	callq	inf_LitMaxWith2Subst
	addq	$16, %rsp
.Lcfi619:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB32_137
# BB#7:                                 #   in Loop: Header=BB32_4 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB32_4
.LBB32_8:                               # %.loopexit
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	$0, (%rax)
	movq	$0, 24(%rsp)
	movq	$0, 112(%rsp)
	movl	64(%r14), %esi
	testl	%esi, %esi
	jle	.LBB32_9
# BB#10:                                # %.lr.ph185.i
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	movq	%rsi, %r12
	.p2align	4, 0x90
.LBB32_11:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB32_13
# BB#12:                                #   in Loop: Header=BB32_11 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB32_13:                              # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB32_11 Depth=1
	callq	term_Copy
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbx, 8(%r13)
	movq	%r14, (%r13)
	incq	%rbp
	movq	%r12, %rax
	cmpq	%rbp, %rax
	movq	%r13, %r14
	jne	.LBB32_11
# BB#14:                                # %._crit_edge186.loopexit.i
	movq	%r13, 112(%rsp)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	64(%rcx), %esi
	jmp	.LBB32_15
.LBB32_9:
	xorl	%r13d, %r13d
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB32_15:                              # %._crit_edge186.i
	movl	68(%rcx), %eax
	movl	72(%rcx), %edx
	leal	(%rax,%rsi), %ecx
	leal	-1(%rdx,%rcx), %edx
	cmpl	%edx, %ecx
	jle	.LBB32_17
# BB#16:
	xorl	%r12d, %r12d
	movq	16(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB32_22
.LBB32_17:                              # %.lr.ph181.i
	movslq	%ecx, %rbp
	movslq	%edx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	decq	%rbp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB32_18:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%rbp,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB32_20
# BB#19:                                #   in Loop: Header=BB32_18 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB32_20:                              # %clause_GetLiteralAtom.exit146.i
                                        #   in Loop: Header=BB32_18 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbx, 8(%r12)
	movq	%r14, (%r12)
	incq	%rbp
	cmpq	8(%rsp), %rbp           # 8-byte Folded Reload
	movq	%r12, %r14
	jl	.LBB32_18
# BB#21:                                # %._crit_edge182.loopexit.i
	movq	%r12, 24(%rsp)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	64(%rcx), %esi
	movl	68(%rcx), %eax
.LBB32_22:                              # %._crit_edge182.i
	movl	8(%rcx), %ebp
	leal	-1(%rax,%rsi), %eax
	cmpl	%eax, %esi
	jle	.LBB32_24
# BB#23:
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB32_46
.LBB32_24:                              # %.lr.ph173.i
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB32_31
# BB#25:                                # %.lr.ph173.i.split.preheader
	cltq
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movslq	%esi, %rbp
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB32_26:                              # %.lr.ph173.i.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_27 Depth 2
                                        #     Child Loop BB32_34 Depth 2
                                        #     Child Loop BB32_36 Depth 2
                                        #     Child Loop BB32_39 Depth 2
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	56(%rbx), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB32_27:                              # %.lr.ph.i135
                                        #   Parent Loop BB32_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rdx), %rax
	movq	(%rax), %rsi
	cmpq	%rcx, %rsi
	je	.LBB32_29
# BB#28:                                #   in Loop: Header=BB32_27 Depth=2
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB32_27
.LBB32_29:                              # %._crit_edge163.i
                                        #   in Loop: Header=BB32_26 Depth=1
	cmpq	%rcx, %rsi
	setne	%cl
	testq	%rax, %rax
	je	.LBB32_31
# BB#30:                                # %._crit_edge163.i
                                        #   in Loop: Header=BB32_26 Depth=1
	testb	%cl, %cl
	jne	.LBB32_31
# BB#33:                                #   in Loop: Header=BB32_26 Depth=1
	movq	8(%rax), %r12
	movq	16(%rax), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	16(%r12), %r14
	movl	8(%r14), %esi
	movl	48(%rsp), %edi          # 4-byte Reload
	callq	misc_Max
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%r13, (%rax)
	movslq	(%rbx), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, 8(%rbx)
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r13
	movq	%rbp, 8(%r13)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movslq	(%r14), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rax, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rbx, (%rax)
	movq	16(%r12), %rax
	movq	56(%rax), %rax
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB32_34:                              #   Parent Loop BB32_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r15, %rbx
	cmpq	%r12, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB32_34
# BB#35:                                # %clause_LiteralGetIndex.exit136.i
                                        #   in Loop: Header=BB32_26 Depth=1
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	sarq	$32, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rax, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r13, (%rax)
	movq	16(%r12), %rax
	movq	56(%rax), %rcx
	movq	$-1, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB32_36:                              #   Parent Loop BB32_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %eax
	leal	1(%rax), %esi
	cmpq	%r12, 8(%rcx,%rdx,8)
	leaq	1(%rdx), %rdx
	jne	.LBB32_36
# BB#37:                                # %clause_LiteralGetIndex.exit.i
                                        #   in Loop: Header=BB32_26 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	64(%rcx), %edx
	movl	68(%rcx), %r13d
	movl	72(%rcx), %ecx
	leal	-1(%rdx,%r13), %esi
	addl	%ecx, %esi
	js	.LBB32_44
# BB#38:                                # %.lr.ph.i123.i
                                        #   in Loop: Header=BB32_26 Depth=1
	leal	-1(%rdx), %esi
	movslq	%esi, %rsi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	addl	%edx, %r13d
	addl	%ecx, %r13d
	movl	%eax, %eax
	xorl	%r14d, %r14d
	movq	%rax, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB32_39:                              #   Parent Loop BB32_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r14, %rax
	je	.LBB32_43
# BB#40:                                # %.sink.split.i.i
                                        #   in Loop: Header=BB32_39 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%r14,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB32_42
# BB#41:                                #   in Loop: Header=BB32_39 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB32_42:                              # %clause_GetLiteralAtom.exit.i.i
                                        #   in Loop: Header=BB32_39 Depth=2
	movq	40(%rsp), %rbx          # 8-byte Reload
	callq	term_Copy
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	subst_Apply
	movq	%rax, %rbx
	cmpq	72(%rsp), %r14          # 8-byte Folded Reload
	leaq	112(%rsp), %r12
	leaq	24(%rsp), %rax
	cmovgq	%rax, %r12
	movq	(%r12), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, (%r12)
	movq	80(%rsp), %rax          # 8-byte Reload
.LBB32_43:                              #   in Loop: Header=BB32_39 Depth=2
	incq	%r14
	cmpq	%r14, %r13
	jne	.LBB32_39
.LBB32_44:                              # %inf_CopyHyperElectron.exit.i
                                        #   in Loop: Header=BB32_26 Depth=1
	movq	144(%rsp), %rbp         # 8-byte Reload
	cmpq	136(%rsp), %rbp         # 8-byte Folded Reload
	leaq	1(%rbp), %rbp
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	jl	.LBB32_26
# BB#45:                                # %._crit_edge174.i
	movq	112(%rsp), %r13
	movq	24(%rsp), %r12
	movq	96(%rsp), %r15          # 8-byte Reload
	movl	48(%rsp), %ebp          # 4-byte Reload
.LBB32_46:
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	216(%rsp), %rcx
	movq	224(%rsp), %r8
	callq	clause_Create
	movq	%rax, %rbx
	xorl	%eax, %eax
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	setne	%al
	orl	$10, %eax
	movl	%eax, 76(%rbx)
	incl	%ebp
	movl	%ebp, 8(%rbx)
	movl	24(%rbx), %ecx
	movq	56(%rsp), %r14          # 8-byte Reload
	testq	%r14, %r14
	je	.LBB32_63
# BB#47:                                # %.lr.ph60.i.i
	movl	%ecx, %ebp
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB32_48:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rdx
	testb	$8, 48(%rdx)
	je	.LBB32_50
# BB#49:                                #   in Loop: Header=BB32_48 Depth=1
	orb	$8, 48(%rbx)
.LBB32_50:                              #   in Loop: Header=BB32_48 Depth=1
	movl	12(%rdx), %esi
	cmpl	12(%rbx), %esi
	movq	%rbx, %rsi
	cmovaq	%rdx, %rsi
	movl	12(%rsi), %esi
	movl	%esi, 12(%rbx)
	movl	24(%rdx), %edx
	cmpl	%edx, %ebp
	cmovbl	%edx, %ebp
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB32_48
# BB#51:                                # %._crit_edge61.i.i
	cmpl	%ecx, %ebp
	jbe	.LBB32_63
# BB#52:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_60
# BB#53:
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB32_54
# BB#59:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB32_60
.LBB32_63:                              # %.preheader51.i.i
	testl	%ecx, %ecx
	jne	.LBB32_61
	jmp	.LBB32_64
.LBB32_54:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB32_56
# BB#55:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB32_56:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB32_58
# BB#57:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB32_58:
	addq	$-16, %rdi
	callq	free
.LBB32_60:                              # %.preheader51.thread.i.i
	leal	(,%rbp,8), %edi
	callq	memory_Malloc
	movq	%rax, 16(%rbx)
	movl	%ebp, 24(%rbx)
.LBB32_61:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB32_62:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rcx
	movl	%eax, %edx
	movq	$0, (%rcx,%rdx,8)
	incl	%eax
	cmpl	24(%rbx), %eax
	jb	.LBB32_62
.LBB32_64:                              # %.preheader.i.i
	testq	%r14, %r14
	je	.LBB32_72
# BB#65:                                # %.lr.ph54.i.i
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB32_67:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_69 Depth 2
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	cmpl	$0, 24(%rcx)
	je	.LBB32_66
# BB#68:                                # %.lr.ph.i121.i
                                        #   in Loop: Header=BB32_67 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB32_69:                              #   Parent Loop BB32_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rsi
	movl	%edx, %edi
	movq	16(%rcx), %rbp
	movq	(%rbp,%rdi,8), %rbp
	orq	%rbp, (%rsi,%rdi,8)
	incl	%edx
	cmpl	24(%rcx), %edx
	jb	.LBB32_69
.LBB32_66:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB32_67 Depth=1
	testq	%rax, %rax
	jne	.LBB32_67
# BB#70:                                # %.lr.ph.i119.preheader.i
	movq	%r15, %rdi
	callq	list_NReverse
	movq	%rax, 32(%rbx)
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	list_NReverse
	movq	%rax, 40(%rbx)
	.p2align	4, 0x90
.LBB32_71:                              # %.lr.ph.i119.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB32_71
	jmp	.LBB32_73
.LBB32_72:                              # %list_Delete.exit120.critedge.i
	movq	%r15, %rdi
	callq	list_NReverse
	movq	%rax, 32(%rbx)
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	list_NReverse
	movq	%rax, 40(%rbx)
.LBB32_73:                              # %list_Delete.exit120.i
	testq	%r13, %r13
	je	.LBB32_76
	.p2align	4, 0x90
.LBB32_74:                              # %.lr.ph.i114.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB32_74
# BB#75:                                # %list_Delete.exit115.loopexit.i
	movq	24(%rsp), %r12
.LBB32_76:                              # %list_Delete.exit115.i
	testq	%r12, %r12
	je	.LBB32_78
	.p2align	4, 0x90
.LBB32_77:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB32_77
.LBB32_78:                              # %inf_BuildHyperResolvent.exit
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$0, (%rbp)
.LBB32_137:                             # %.loopexit154
	movq	%rbp, %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB32_120:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$3815, %ecx             # imm = 0xEE7
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.10, %edi
.LBB32_32:                              # %.thread.i.split
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	callq	misc_DumpCore
.LBB32_31:                              # %.thread.i.split
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$.L.str.1, %edx
	movl	$3637, %ecx             # imm = 0xE35
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.11, %edi
	jmp	.LBB32_32
.Lfunc_end32:
	.size	inf_HyperResolvents, .Lfunc_end32-inf_HyperResolvents
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_HyperLiteralIsBetter,@function
clause_HyperLiteralIsBetter:            # @clause_HyperLiteralIsBetter
	.cfi_startproc
# BB#0:
	cmpl	%esi, %ecx
	sbbl	%eax, %eax
	andl	$1, %eax
	retq
.Lfunc_end33:
	.size	clause_HyperLiteralIsBetter, .Lfunc_end33-clause_HyperLiteralIsBetter
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str, 31

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/rules-inf.c"
	.size	.L.str.1, 81

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n In inf_GeneralResolution: Unification failed."
	.size	.L.str.2, 48

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.3, 133

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\n In inf_UnitResolution: Unification failed."
	.size	.L.str.4, 45

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n In inf_BoundedDepthUnitResolution: Unification failed."
	.size	.L.str.5, 57

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n Error: Flag \"IOFC\" has invalid value.\n"
	.size	.L.str.6, 41

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\n Error: Flag \"IORE\" has invalid value.\n"
	.size	.L.str.7, 41

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\n Error: Flag \"ISRE\" has invalid value.\n"
	.size	.L.str.8, 41

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\n\n"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\n In inf_HyperResolvents: Unification failed."
	.size	.L.str.10, 46

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"\n In inf_BuildHyperResolvent: Map entry not found."
	.size	.L.str.11, 51

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\n In inf_BackwardHyperResolution: Unification failed."
	.size	.L.str.12, 54


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
