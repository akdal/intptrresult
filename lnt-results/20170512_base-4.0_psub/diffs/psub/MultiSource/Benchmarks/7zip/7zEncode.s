	.text
	.file	"7zEncode.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
	.byte	255                     # 0xff
.LCPI0_1:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z8CEncoder16CreateMixerCoderEPKy
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CEncoder16CreateMixerCoderEPKy,@function
_ZN8NArchive3N7z8CEncoder16CreateMixerCoderEPKy: # @_ZN8NArchive3N7z8CEncoder16CreateMixerCoderEPKy
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	$224, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	$0, 16(%rbp)
	movl	$_ZTVN11NCoderMixer14CCoderMixer2MTE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN11NCoderMixer14CCoderMixer2MTE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 32(%rbp)
	movq	$8, 48(%rbp)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE+16, 24(%rbp)
	movdqu	%xmm0, 64(%rbp)
	movq	$8, 80(%rbp)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE+16, 56(%rbp)
	movdqu	%xmm0, 96(%rbp)
	movq	$4, 112(%rbp)
	movq	$_ZTV13CRecordVectorIjE+16, 88(%rbp)
	movdqu	%xmm0, 128(%rbp)
	movq	$4, 144(%rbp)
	movq	$_ZTV13CRecordVectorIjE+16, 120(%rbp)
	movdqu	%xmm0, 160(%rbp)
	movq	$8, 176(%rbp)
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, 152(%rbp)
	movdqu	%xmm0, 200(%rbp)
	movq	$8, 216(%rbp)
	movq	$_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE+16, 192(%rbp)
	movq	%rbp, (%r12)
	movq	%rbp, %rdi
	callq	*_ZTVN11NCoderMixer14CCoderMixer2MTE+24(%rip)
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_2:                                # %_ZN9CMyComPtrI15ICompressCoder2EaSEPS0_.exit
	movq	%rbp, 8(%r12)
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	leaq	136(%r12), %rsi
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB0_6
# BB#3:                                 # %.preheader
	xorl	%eax, %eax
	movl	$2, %ecx
	cmpl	$0, 60(%r12)
	jle	.LBB0_4
# BB#9:                                 # %.lr.ph247
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	leaq	16(%r12), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movq	%r12, 40(%rsp)          # 8-byte Spill
	jmp	.LBB0_10
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_10 Depth=1
	incq	%r13
	movq	40(%rsp), %r12          # 8-byte Reload
	movslq	60(%r12), %rax
	cmpq	%rax, %r13
	jl	.LBB0_10
	jmp	.LBB0_8
.LBB0_47:                               # %vector.body.preheader
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	-4(%rcx), %rsi
	movq	%rsi, %rdx
	shrq	$2, %rdx
	btl	$2, %esi
	jb	.LBB0_48
# BB#49:                                # %vector.body.prol
                                        #   in Loop: Header=BB0_10 Depth=1
	movdqu	(%rbx), %xmm0
	pshuflw	$232, %xmm0, %xmm1      # xmm1 = xmm0[0,2,2,3,4,5,6,7]
	psrld	$8, %xmm0
	pshufhw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pand	%xmm3, %xmm1
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$216, %xmm0, %xmm0      # xmm0 = xmm0[0,2,1,3,4,5,6,7]
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$216, %xmm0, %xmm0      # xmm0 = xmm0[0,2,1,3,4,5,6,7]
	movdqa	%xmm3, %xmm2
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	movq	%rax, %rsi
	movq	%xmm2, (%rsi)
	movl	$4, %r8d
	testq	%rdx, %rdx
	jne	.LBB0_51
	jmp	.LBB0_53
.LBB0_48:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.LBB0_53
.LBB0_51:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%rcx, %rdx
	subq	%r8, %rdx
	leaq	16(%rbx,%r8,4), %rsi
	movq	%rax, %rdi
	leaq	8(%rdi,%r8,2), %rdi
.LBB0_52:                               # %vector.body
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rsi), %xmm0
	pshuflw	$232, %xmm0, %xmm1      # xmm1 = xmm0[0,2,2,3,4,5,6,7]
	psrld	$8, %xmm0
	pshufhw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pand	%xmm3, %xmm1
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$216, %xmm0, %xmm0      # xmm0 = xmm0[0,2,1,3,4,5,6,7]
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$216, %xmm0, %xmm0      # xmm0 = xmm0[0,2,1,3,4,5,6,7]
	movdqa	%xmm3, %xmm2
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	movq	%xmm2, -8(%rdi)
	movdqu	(%rsi), %xmm0
	pshuflw	$232, %xmm0, %xmm1      # xmm1 = xmm0[0,2,2,3,4,5,6,7]
	psrld	$8, %xmm0
	pshufhw	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pand	%xmm3, %xmm1
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$216, %xmm0, %xmm0      # xmm0 = xmm0[0,2,1,3,4,5,6,7]
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$216, %xmm0, %xmm0      # xmm0 = xmm0[0,2,1,3,4,5,6,7]
	movdqa	%xmm3, %xmm2
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	movq	%xmm2, (%rdi)
	addq	$32, %rsi
	addq	$16, %rdi
	addq	$-8, %rdx
	jne	.LBB0_52
.LBB0_53:                               # %middle.block
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpq	%rcx, %rbp
	jne	.LBB0_43
	jmp	.LBB0_54
	.p2align	4, 0x90
.LBB0_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_52 Depth 2
                                        #     Child Loop BB0_43 Depth 2
	movq	64(%r12), %rax
	movq	(%rax,%r13,8), %rbp
.Ltmp0:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp1:
# BB#11:                                #   in Loop: Header=BB0_10 Depth=1
	movq	$0, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 8(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rbx)
	movq	$0, 32(%rbx)
.Ltmp2:
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp3:
# BB#12:                                # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	32(%r12), %rax
	movslq	28(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 28(%r12)
	movq	32(%r12), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rbp), %rdi
	movq	%rdi, (%rax)
	movq	$0, 16(%rsp)
	movq	$0, 8(%rsp)
.Ltmp5:
	movl	$1, %ecx
	leaq	16(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderERS_I15ICompressCoder2Eb
	movl	%eax, %r15d
.Ltmp6:
# BB#13:                                #   in Loop: Header=BB0_10 Depth=1
	testl	%r15d, %r15d
	cmovnel	%r15d, %r14d
	je	.LBB0_15
# BB#14:                                #   in Loop: Header=BB0_10 Depth=1
	movl	$1, %r12d
	jmp	.LBB0_70
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_10 Depth=1
	movq	16(%rsp), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_18
# BB#16:                                #   in Loop: Header=BB0_10 Depth=1
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB0_17
.LBB0_18:                               #   in Loop: Header=BB0_10 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_10 Depth=1
	movq	(%rbx), %rax
.Ltmp7:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp8:
.LBB0_20:                               # %_ZN9CMyComPtrI8IUnknownEC2EPS0_.exit
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	$0, 32(%rsp)
	movq	(%rbx), %rax
.Ltmp10:
	movl	$IID_ICompressSetCoderMt, %esi
	movq	%rbx, %rdi
	leaq	32(%rsp), %rdx
	movq	%rbx, (%rsp)            # 8-byte Spill
	callq	*(%rax)
.Ltmp11:
# BB#21:                                # %_ZNK9CMyComPtrI8IUnknownE14QueryInterfaceI19ICompressSetCoderMtEEiRK4GUIDPPT_.exit
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_27
# BB#22:                                #   in Loop: Header=BB0_10 Depth=1
	movq	(%rdi), %rax
	movl	112(%r12), %esi
.Ltmp12:
	callq	*40(%rax)
.Ltmp13:
# BB#23:                                #   in Loop: Header=BB0_10 Depth=1
	testl	%eax, %eax
	je	.LBB0_27
# BB#24:                                #   in Loop: Header=BB0_10 Depth=1
	movl	$1, %ebx
	movl	%eax, %r14d
	jmp	.LBB0_28
.LBB0_27:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%ebx, %ebx
.LBB0_28:                               #   in Loop: Header=BB0_10 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_30
# BB#29:                                #   in Loop: Header=BB0_10 Depth=1
	movq	(%rdi), %rax
.Ltmp17:
	callq	*16(%rax)
.Ltmp18:
.LBB0_30:                               # %_ZN9CMyComPtrI19ICompressSetCoderMtED2Ev.exit124
                                        #   in Loop: Header=BB0_10 Depth=1
	movl	$1, %r12d
	testl	%ebx, %ebx
	je	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_10 Depth=1
	movl	%r14d, %r15d
	movq	(%rsp), %rbx            # 8-byte Reload
	testq	%rbx, %rbx
	jne	.LBB0_69
	jmp	.LBB0_70
.LBB0_32:                               #   in Loop: Header=BB0_10 Depth=1
.Ltmp20:
	movq	%rbp, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%rbx, %rdx
	callq	_Z19SetMethodPropertiesRK7CMethodPKyP8IUnknown
	movl	%eax, %ebp
.Ltmp21:
# BB#33:                                #   in Loop: Header=BB0_10 Depth=1
	movl	%ebp, %r15d
	testl	%ebp, %ebp
	cmovel	%r14d, %ebp
	jne	.LBB0_68
# BB#34:                                #   in Loop: Header=BB0_10 Depth=1
	movq	$0, 24(%rsp)
	movq	(%rbx), %rax
.Ltmp23:
	movl	$IID_ICryptoSetPassword, %esi
	movq	%rbx, %rdi
	leaq	24(%rsp), %rdx
	callq	*(%rax)
.Ltmp24:
# BB#35:                                # %_ZNK9CMyComPtrI8IUnknownE14QueryInterfaceI18ICryptoSetPasswordEEiRK4GUIDPPT_.exit
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	24(%rsp), %r15
	testq	%r15, %r15
	je	.LBB0_62
# BB#36:                                #   in Loop: Header=BB0_10 Depth=1
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	movq	40(%rsp), %rbx          # 8-byte Reload
	movslq	128(%rbx), %rbp
	leal	(%rbp,%rbp), %r14d
	testq	%rbp, %rbp
	je	.LBB0_37
# BB#39:                                #   in Loop: Header=BB0_10 Depth=1
	movl	%r14d, %edi
.Ltmp25:
	callq	_Znam
.Ltmp26:
# BB#40:                                # %_ZN7CBufferIhE11SetCapacityEm.exit
                                        #   in Loop: Header=BB0_10 Depth=1
	testl	%ebp, %ebp
	movdqa	.LCPI0_0(%rip), %xmm3   # xmm3 = [255,0,255,0,255,0,255,0,255,255,255,255,255,255,255,255]
	jle	.LBB0_54
# BB#41:                                # %.lr.ph
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	120(%rbx), %rbx
	cmpl	$3, %ebp
	jbe	.LBB0_42
# BB#44:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	%rbp, %rcx
	andq	$-4, %rcx
	je	.LBB0_42
# BB#45:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	(%rbx,%rbp,4), %rdx
	cmpq	%rdx, %rax
	jae	.LBB0_47
# BB#46:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_10 Depth=1
	leaq	(%rax,%rbp,2), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB0_47
.LBB0_42:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_43:                               # %scalar.ph
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rcx,4), %edx
	movb	%dl, (%rax,%rcx,2)
	movb	%dh, 1(%rax,%rcx,2)  # NOREX
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB0_43
	jmp	.LBB0_54
.LBB0_17:                               #   in Loop: Header=BB0_10 Depth=1
	movl	$1, %r12d
	movl	$-2147467259, %r14d     # imm = 0x80004005
	jmp	.LBB0_73
.LBB0_37:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%eax, %eax
.LBB0_54:                               # %._crit_edge
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	(%r15), %rcx
.Ltmp28:
	movq	%r15, %rdi
	movq	%rax, %r15
	movq	%r15, %rsi
	movl	%r14d, %edx
	callq	*40(%rcx)
	movl	%eax, %ebp
.Ltmp29:
# BB#55:                                #   in Loop: Header=BB0_10 Depth=1
	xorl	%r12d, %r12d
	testl	%ebp, %ebp
	setne	%bl
	testq	%r15, %r15
	je	.LBB0_57
# BB#56:                                #   in Loop: Header=BB0_10 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB0_57:                               # %_ZN7CBufferIhED2Ev.exit
                                        #   in Loop: Header=BB0_10 Depth=1
	testl	%ebp, %ebp
	movl	52(%rsp), %r14d         # 4-byte Reload
	je	.LBB0_62
# BB#58:                                #   in Loop: Header=BB0_10 Depth=1
	movb	%bl, %r12b
	movl	%ebp, %r14d
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB0_65
.LBB0_62:                               #   in Loop: Header=BB0_10 Depth=1
	movq	16(%rsp), %rsi
	testq	%rsi, %rsi
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	je	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_10 Depth=1
	xorl	%r12d, %r12d
.Ltmp31:
	movq	(%rsp), %rbx            # 8-byte Reload
	callq	_ZN11NCoderMixer14CCoderMixer2MT8AddCoderEP14ICompressCoder
.Ltmp32:
	jmp	.LBB0_65
.LBB0_64:                               #   in Loop: Header=BB0_10 Depth=1
	xorl	%r12d, %r12d
	movq	8(%rsp), %rsi
.Ltmp33:
	movq	(%rsp), %rbx            # 8-byte Reload
	callq	_ZN11NCoderMixer14CCoderMixer2MT9AddCoder2EP15ICompressCoder2
.Ltmp34:
.LBB0_65:                               #   in Loop: Header=BB0_10 Depth=1
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_67
# BB#66:                                #   in Loop: Header=BB0_10 Depth=1
	movq	(%rdi), %rax
.Ltmp38:
	callq	*16(%rax)
.Ltmp39:
.LBB0_67:                               # %_ZN9CMyComPtrI18ICryptoSetPasswordED2Ev.exit119
                                        #   in Loop: Header=BB0_10 Depth=1
	movl	%r14d, %r15d
	.p2align	4, 0x90
.LBB0_68:                               #   in Loop: Header=BB0_10 Depth=1
	testq	%rbx, %rbx
	je	.LBB0_70
.LBB0_69:                               #   in Loop: Header=BB0_10 Depth=1
	movq	(%rbx), %rax
.Ltmp43:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp44:
.LBB0_70:                               # %_ZN9CMyComPtrI8IUnknownED2Ev.exit116
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_71
# BB#72:                                #   in Loop: Header=BB0_10 Depth=1
	movq	(%rdi), %rax
.Ltmp48:
	callq	*16(%rax)
.Ltmp49:
	movl	%r15d, %r14d
	jmp	.LBB0_73
	.p2align	4, 0x90
.LBB0_71:                               #   in Loop: Header=BB0_10 Depth=1
	movl	%r15d, %r14d
.LBB0_73:                               # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit114
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_75
# BB#74:                                #   in Loop: Header=BB0_10 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_75:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit112
                                        #   in Loop: Header=BB0_10 Depth=1
	testl	%r12d, %r12d
	je	.LBB0_7
# BB#76:
	movl	%r12d, %ecx
	xorl	%eax, %eax
	jmp	.LBB0_5
.LBB0_4:
	xorl	%r14d, %r14d
	jmp	.LBB0_5
.LBB0_8:
	xorl	%eax, %eax
	movl	$2, %ecx
.LBB0_5:                                # %._crit_edge248
	cmpl	$2, %ecx
	cmovel	%eax, %r14d
	movl	%r14d, %eax
.LBB0_6:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_78:                               # %.thread159
.Ltmp27:
	jmp	.LBB0_79
.LBB0_77:
.Ltmp40:
	jmp	.LBB0_84
.LBB0_60:
.Ltmp30:
	movq	%rax, %rbp
	movq	%r15, %rdi
	testq	%r15, %r15
	je	.LBB0_80
# BB#61:
	callq	_ZdaPv
	jmp	.LBB0_80
.LBB0_59:
.Ltmp35:
.LBB0_79:                               # %_ZN7CBufferIhED2Ev.exit121
	movq	%rax, %rbp
.LBB0_80:                               # %_ZN7CBufferIhED2Ev.exit121
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_85
# BB#81:
	movq	(%rdi), %rax
.Ltmp36:
	callq	*16(%rax)
.Ltmp37:
	jmp	.LBB0_85
.LBB0_83:
.Ltmp22:
	jmp	.LBB0_84
.LBB0_82:
.Ltmp45:
	jmp	.LBB0_89
.LBB0_38:
.Ltmp19:
.LBB0_84:
	movq	%rax, %rbp
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	jne	.LBB0_86
	jmp	.LBB0_90
.LBB0_25:
.Ltmp14:
	movq	%rax, %rbp
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_85
# BB#26:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB0_85:
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB0_90
.LBB0_86:
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp41:
	callq	*16(%rax)
.Ltmp42:
	jmp	.LBB0_90
.LBB0_87:
.Ltmp50:
	movq	%rax, %rbp
	jmp	.LBB0_92
.LBB0_88:
.Ltmp9:
.LBB0_89:                               # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
	movq	%rax, %rbp
.LBB0_90:                               # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_92
# BB#91:
	movq	(%rdi), %rax
.Ltmp46:
	callq	*16(%rax)
.Ltmp47:
.LBB0_92:                               # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_95
# BB#93:
	movq	(%rdi), %rax
.Ltmp51:
	callq	*16(%rax)
.Ltmp52:
	jmp	.LBB0_95
.LBB0_96:
.Ltmp53:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB0_94:
.Ltmp4:
	movq	%rax, %rbp
.LBB0_95:
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZN8NArchive3N7z8CEncoder16CreateMixerCoderEPKy, .Lfunc_end0-_ZN8NArchive3N7z8CEncoder16CreateMixerCoderEPKy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\331\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp13-.Ltmp10         #   Call between .Ltmp10 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp35-.Lfunc_begin0   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp34-.Ltmp31         #   Call between .Ltmp31 and .Ltmp34
	.long	.Ltmp35-.Lfunc_begin0   #     jumps to .Ltmp35
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin0   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin0   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin0   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp36-.Ltmp49         #   Call between .Ltmp49 and .Ltmp36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp52-.Ltmp36         #   Call between .Ltmp36 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	1                       #   On action: 1
	.long	.Ltmp52-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Lfunc_end0-.Ltmp52     #   Call between .Ltmp52 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end2-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z8CEncoder6EncodeEP19ISequentialInStreamPKyS5_RNS0_7CFolderEP20ISequentialOutStreamR13CRecordVectorIyEP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CEncoder6EncodeEP19ISequentialInStreamPKyS5_RNS0_7CFolderEP20ISequentialOutStreamR13CRecordVectorIyEP21ICompressProgressInfo,@function
_ZN8NArchive3N7z8CEncoder6EncodeEP19ISequentialInStreamPKyS5_RNS0_7CFolderEP20ISequentialOutStreamR13CRecordVectorIyEP21ICompressProgressInfo: # @_ZN8NArchive3N7z8CEncoder6EncodeEP19ISequentialInStreamPKyS5_RNS0_7CFolderEP20ISequentialOutStreamR13CRecordVectorIyEP21ICompressProgressInfo
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$1480, %rsp             # imm = 0x5C8
.Lcfi19:
	.cfi_def_cfa_offset 1536
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r8, %r15
	movq	%rcx, %rbx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, %r14
.Lcfi26:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7z8CEncoder13EncoderConstrEv
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_143
.LBB3_1:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	(%rdi), %rax
.Lcfi27:
	.cfi_escape 0x2e, 0x00
	callq	*56(%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 32(%rsp)
	movq	$8, 48(%rsp)
	movq	$_ZTV13CObjectVectorI16CInOutTempBufferE+16, 24(%rsp)
	movdqu	%xmm0, 160(%rsp)
	movq	$8, 176(%rsp)
	movq	$_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE+16, 152(%rsp)
	movdqu	%xmm0, 96(%rsp)
	movq	$8, 112(%rsp)
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 88(%rsp)
	movslq	148(%r14), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	cmpl	$2, 244(%r14)
	jl	.LBB3_22
# BB#2:                                 # %.lr.ph439
	movl	$1, %ebp
	leaq	312(%rsp), %r15
	leaq	24(%rsp), %r12
	.p2align	4, 0x90
.LBB3_3:                                # =>This Inner Loop Header: Depth=1
.Ltmp54:
.Lcfi28:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZN16CInOutTempBufferC1Ev
.Ltmp55:
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
.Ltmp56:
.Lcfi29:
	.cfi_escape 0x2e, 0x00
	movl	$1168, %edi             # imm = 0x490
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp57:
# BB#5:                                 # %.noexc
                                        #   in Loop: Header=BB3_3 Depth=1
.Ltmp58:
.Lcfi30:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZN16CInOutTempBufferC2ERKS_
.Ltmp59:
# BB#6:                                 #   in Loop: Header=BB3_3 Depth=1
.Ltmp61:
.Lcfi31:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp62:
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	40(%rsp), %rax
	movslq	36(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 36(%rsp)
.Ltmp66:
.Lcfi32:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZN16CInOutTempBufferD1Ev
.Ltmp67:
# BB#8:                                 #   in Loop: Header=BB3_3 Depth=1
	movslq	36(%rsp), %rax
	movq	40(%rsp), %rcx
	movq	-8(%rcx,%rax,8), %rdi
.Ltmp69:
.Lcfi33:
	.cfi_escape 0x2e, 0x00
	callq	_ZN16CInOutTempBuffer6CreateEv
.Ltmp70:
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	movslq	36(%rsp), %rax
	movq	40(%rsp), %rcx
	movq	-8(%rcx,%rax,8), %rdi
.Ltmp71:
.Lcfi34:
	.cfi_escape 0x2e, 0x00
	callq	_ZN16CInOutTempBuffer11InitWritingEv
.Ltmp72:
# BB#10:                                #   in Loop: Header=BB3_3 Depth=1
	incl	%ebp
	movl	244(%r14), %eax
	cmpl	%eax, %ebp
	jl	.LBB3_3
# BB#11:                                # %.preheader346
	cmpl	$2, %eax
	jl	.LBB3_22
# BB#12:                                # %.lr.ph436
	movl	$1, %r12d
	leaq	152(%rsp), %rbp
	.p2align	4, 0x90
.LBB3_13:                               # =>This Inner Loop Header: Depth=1
.Ltmp74:
.Lcfi35:
	.cfi_escape 0x2e, 0x00
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp75:
# BB#14:                                #   in Loop: Header=BB3_13 Depth=1
	movl	$0, 8(%r13)
	movq	$_ZTV27CSequentialOutTempBufferImp+16, (%r13)
.Ltmp77:
.Lcfi36:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*_ZTV27CSequentialOutTempBufferImp+24(%rip)
.Ltmp78:
# BB#15:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
                                        #   in Loop: Header=BB3_13 Depth=1
	movq	40(%rsp), %rax
	movq	-8(%rax,%r12,8), %rax
	movq	%rax, 16(%r13)
.Ltmp80:
.Lcfi37:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp81:
# BB#16:                                #   in Loop: Header=BB3_13 Depth=1
	movq	%r13, (%r15)
	movq	(%r13), %rax
.Ltmp82:
.Lcfi38:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp83:
# BB#17:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2ERKS1_.exit.i
                                        #   in Loop: Header=BB3_13 Depth=1
.Ltmp85:
.Lcfi39:
	.cfi_escape 0x2e, 0x00
	leaq	88(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp86:
# BB#18:                                #   in Loop: Header=BB3_13 Depth=1
	movq	104(%rsp), %rax
	movslq	100(%rsp), %rcx
	movq	%r15, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 100(%rsp)
.Ltmp87:
.Lcfi40:
	.cfi_escape 0x2e, 0x00
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp88:
# BB#19:                                # %.noexc201
                                        #   in Loop: Header=BB3_13 Depth=1
	movq	%r13, (%rbx)
.Ltmp89:
.Lcfi41:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp90:
# BB#20:                                #   in Loop: Header=BB3_13 Depth=1
	movq	168(%rsp), %rax
	movslq	164(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 164(%rsp)
	movq	(%r13), %rax
.Ltmp94:
.Lcfi42:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp95:
# BB#21:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
                                        #   in Loop: Header=BB3_13 Depth=1
	incq	%r12
	movslq	244(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB3_13
.LBB3_22:                               # %.preheader340
	movq	192(%rsp), %rbp         # 8-byte Reload
	testl	%ebp, %ebp
	movq	8(%rsp), %r13           # 8-byte Reload
	jle	.LBB3_26
# BB#23:                                # %.lr.ph433.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_24:                               # %.lr.ph433
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp97:
.Lcfi43:
	.cfi_escape 0x2e, 0x00
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %esi
	callq	*64(%rax)
.Ltmp98:
# BB#25:                                #   in Loop: Header=BB3_24 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jl	.LBB3_24
.LBB3_26:                               # %._crit_edge434
	cmpl	$0, 212(%r14)
	je	.LBB3_145
# BB#27:
	movl	148(%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB3_31
# BB#28:                                # %.lr.ph.i
	movq	152(%r14), %rax
	movq	216(%r14), %rdx
	movl	(%rdx), %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_29:                               # =>This Inner Loop Header: Depth=1
	movslq	%r12d, %rbp
	movl	%ebx, %edx
	subl	(%rax,%rbp,8), %edx
	jb	.LBB3_33
# BB#30:                                #   in Loop: Header=BB3_29 Depth=1
	incl	%r12d
	cmpl	%ecx, %r12d
	movl	%edx, %ebx
	jb	.LBB3_29
.LBB3_31:                               # %._crit_edge.i
.Lcfi44:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp236:
.Lcfi45:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp237:
# BB#32:                                # %.noexc206
.LBB3_33:                               # %_ZNK11NCoderMixer9CBindInfo12FindInStreamEjRjS1_.exit
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB3_41
# BB#34:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 240(%rsp)
	movq	$8, 256(%rsp)
	movq	$_ZTV13CRecordVectorIPKyE+16, 232(%rsp)
	xorl	%r15d, %r15d
	cmpl	$0, (%rax,%rbp,8)
	je	.LBB3_39
# BB#35:                                # %.lr.ph430
	leaq	232(%rsp), %r13
	.p2align	4, 0x90
.LBB3_36:                               # =>This Inner Loop Header: Depth=1
.Ltmp100:
.Lcfi46:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp101:
# BB#37:                                # %_ZN13CRecordVectorIPKyE3AddES1_.exit209
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpl	%r15d, %ebx
	movl	$0, %eax
	cmoveq	64(%rsp), %rax          # 8-byte Folded Reload
	movq	248(%rsp), %rcx
	movslq	244(%rsp), %rdx
	movq	%rax, (%rcx,%rdx,8)
	leal	1(%rdx), %eax
	movl	%eax, 244(%rsp)
	incl	%r15d
	movq	152(%r14), %rax
	cmpl	(%rax,%rbp,8), %r15d
	jb	.LBB3_36
# BB#38:                                # %._crit_edge431.loopexit
	movq	248(%rsp), %r15
	movq	8(%rsp), %r13           # 8-byte Reload
.LBB3_39:                               # %._crit_edge431
	movq	(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp103:
.Lcfi47:
	.cfi_escape 0x2e, 0x00
	xorl	%ecx, %ecx
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	*64(%rax)
.Ltmp104:
# BB#40:
.Ltmp108:
.Lcfi48:
	.cfi_escape 0x2e, 0x00
	leaq	232(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp109:
.LBB3_41:
.Ltmp111:
.Lcfi49:
	.cfi_escape 0x2e, 0x00
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp112:
# BB#42:
	movl	$0, 16(%rbp)
	movl	$_ZTV29CSequentialInStreamSizeCount2+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTV29CSequentialInStreamSizeCount2+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbp)
.Ltmp113:
.Lcfi50:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*_ZTV29CSequentialInStreamSizeCount2+24(%rip)
.Ltmp114:
# BB#43:                                # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit
.Ltmp116:
.Lcfi51:
	.cfi_escape 0x2e, 0x00
	movl	$32, %edi
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp117:
# BB#44:
	movl	$0, 8(%rbx)
	movq	$_ZTV29CSequentialOutStreamSizeCount+16, (%rbx)
	movq	$0, 16(%rbx)
.Ltmp119:
.Lcfi52:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*_ZTV29CSequentialOutStreamSizeCount+24(%rip)
.Ltmp120:
# BB#45:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit213
	testq	%r13, %r13
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	je	.LBB3_47
# BB#46:
	movq	(%r13), %rax
.Ltmp122:
.Lcfi53:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*8(%rax)
.Ltmp123:
.LBB3_47:                               # %.noexc214
	leaq	24(%rbp), %r15
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_49
# BB#48:
	movq	(%rdi), %rax
.Ltmp124:
.Lcfi54:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp125:
.LBB3_49:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit.i
	movq	%r13, 24(%rbp)
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_52
# BB#50:
	movq	(%rdi), %rax
.Ltmp126:
.Lcfi55:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp127:
# BB#51:                                # %.noexc216
	movq	(%r15), %r13
.LBB3_52:                               # %_ZN9CMyComPtrI25ICompressGetSubStreamSizeEaSEPS0_.exit.i
	leaq	32(%rbp), %rdx
	movq	$0, (%rdx)
	movq	(%r13), %rax
.Ltmp128:
.Lcfi56:
	.cfi_escape 0x2e, 0x00
	movl	$IID_ICompressGetSubStreamSize, %esi
	movq	%r13, %rdi
	callq	*(%rax)
.Ltmp129:
# BB#53:
	movq	$0, 40(%rbp)
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	je	.LBB3_55
# BB#54:
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp130:
.Lcfi57:
	.cfi_escape 0x2e, 0x00
	callq	*8(%rax)
.Ltmp131:
.LBB3_55:                               # %.noexc218
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_57
# BB#56:
	movq	(%rdi), %rax
.Ltmp132:
.Lcfi58:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp133:
.LBB3_57:
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rbx)
	movq	$0, 24(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 208(%rsp)
	movq	$8, 224(%rsp)
	movq	$_ZTV13CRecordVectorIP19ISequentialInStreamE+16, 200(%rsp)
	movdqu	%xmm0, 128(%rsp)
	movq	$8, 144(%rsp)
	movq	$_ZTV13CRecordVectorIP20ISequentialOutStreamE+16, 120(%rsp)
.Ltmp135:
.Lcfi59:
	.cfi_escape 0x2e, 0x00
	leaq	200(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp136:
# BB#58:
	movq	216(%rsp), %rax
	movslq	212(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 212(%rsp)
.Ltmp137:
.Lcfi60:
	.cfi_escape 0x2e, 0x00
	leaq	120(%rsp), %rdi
	movq	%rdi, 184(%rsp)         # 8-byte Spill
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp138:
# BB#59:                                # %_ZN13CRecordVectorIP20ISequentialOutStreamE3AddES1_.exit
	movq	136(%rsp), %rax
	movslq	132(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 132(%rsp)
	cmpl	$2, 244(%r14)
	jl	.LBB3_63
# BB#60:                                # %.lr.ph427
	movl	$1, %ebx
	leaq	120(%rsp), %r15
	.p2align	4, 0x90
.LBB3_61:                               # =>This Inner Loop Header: Depth=1
	movq	104(%rsp), %rax
	movq	-8(%rax,%rbx,8), %rax
	movq	(%rax), %rbp
.Ltmp140:
.Lcfi61:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp141:
# BB#62:                                #   in Loop: Header=BB3_61 Depth=1
	movq	136(%rsp), %rax
	movslq	132(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 132(%rsp)
	incq	%rbx
	movslq	244(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_61
.LBB3_63:                               # %.preheader334
	movl	28(%r14), %eax
	testl	%eax, %eax
	jle	.LBB3_84
# BB#64:                                # %.lr.ph425
	xorl	%ebx, %ebx
	leaq	16(%rsp), %rbp
	.p2align	4, 0x90
.LBB3_65:                               # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rax
	movq	(%rax,%rbx,8), %r15
	movq	$0, 72(%rsp)
	movq	(%r14), %rax
	movq	208(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	240(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_67
# BB#66:                                #   in Loop: Header=BB3_65 Depth=1
	movq	248(%rax), %rdi
.LBB3_67:                               #   in Loop: Header=BB3_65 Depth=1
	movq	(%rdi), %rax
.Ltmp143:
.Lcfi62:
	.cfi_escape 0x2e, 0x00
	movl	$IID_ICryptoResetInitVector, %esi
	leaq	72(%rsp), %rdx
	callq	*(%rax)
.Ltmp144:
# BB#68:                                # %_ZNK11NCoderMixer11CCoderInfo214QueryInterfaceERK4GUIDPPv.exit
                                        #   in Loop: Header=BB3_65 Depth=1
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_70
# BB#69:                                #   in Loop: Header=BB3_65 Depth=1
	movq	(%rdi), %rax
.Ltmp145:
.Lcfi63:
	.cfi_escape 0x2e, 0x00
	callq	*40(%rax)
.Ltmp146:
.LBB3_70:                               #   in Loop: Header=BB3_65 Depth=1
	movq	$0, 16(%rsp)
	movq	(%r14), %rax
	movq	208(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	240(%rax), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_72
# BB#71:                                #   in Loop: Header=BB3_65 Depth=1
	movq	248(%rax), %rdi
.LBB3_72:                               #   in Loop: Header=BB3_65 Depth=1
	movq	(%rdi), %rax
.Ltmp148:
.Lcfi64:
	.cfi_escape 0x2e, 0x00
	movl	$IID_ICompressWriteCoderProperties, %esi
	movq	%rbp, %rdx
	callq	*(%rax)
.Ltmp149:
# BB#73:                                # %_ZNK11NCoderMixer11CCoderInfo214QueryInterfaceERK4GUIDPPv.exit237
                                        #   in Loop: Header=BB3_65 Depth=1
	cmpq	$0, 16(%rsp)
	je	.LBB3_81
# BB#74:                                #   in Loop: Header=BB3_65 Depth=1
.Ltmp150:
.Lcfi65:
	.cfi_escape 0x2e, 0x00
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp151:
# BB#75:                                #   in Loop: Header=BB3_65 Depth=1
	movl	$0, 8(%r13)
	movq	$_ZTV19CDynBufSeqOutStream+16, (%r13)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%r13)
	movq	$0, 32(%r13)
.Ltmp153:
.Lcfi66:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*_ZTV19CDynBufSeqOutStream+24(%rip)
.Ltmp154:
# BB#76:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit240
                                        #   in Loop: Header=BB3_65 Depth=1
	movq	$0, 32(%r13)
	movq	16(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp156:
.Lcfi67:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rsi
	callq	*40(%rax)
.Ltmp157:
# BB#77:                                #   in Loop: Header=BB3_65 Depth=1
	addq	$8, %r15
.Ltmp158:
.Lcfi68:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	_ZNK19CDynBufSeqOutStream12CopyToBufferER7CBufferIhE
.Ltmp159:
# BB#78:                                #   in Loop: Header=BB3_65 Depth=1
	movq	(%r13), %rax
.Ltmp163:
.Lcfi69:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp164:
# BB#79:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit248
                                        #   in Loop: Header=BB3_65 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_81
# BB#80:                                #   in Loop: Header=BB3_65 Depth=1
	movq	(%rdi), %rax
.Ltmp168:
.Lcfi70:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp169:
.LBB3_81:                               # %_ZN9CMyComPtrI29ICompressWriteCoderPropertiesED2Ev.exit
                                        #   in Loop: Header=BB3_65 Depth=1
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_83
# BB#82:                                #   in Loop: Header=BB3_65 Depth=1
	movq	(%rdi), %rax
.Ltmp173:
.Lcfi71:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp174:
.LBB3_83:                               # %_ZN9CMyComPtrI22ICryptoResetInitVectorED2Ev.exit
                                        #   in Loop: Header=BB3_65 Depth=1
	incq	%rbx
	movslq	28(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_65
.LBB3_84:                               # %.preheader333
	movq	8(%rsp), %r13           # 8-byte Reload
	addq	$40, %r13
	cltq
	xorl	%ecx, %ecx
	movslq	%ecx, %rsi
	jmp	.LBB3_86
.LBB3_85:                               # %.outer.loopexit
                                        #   in Loop: Header=BB3_86 Depth=1
	movl	%ecx, %r12d
	movslq	%ecx, %rsi
	.p2align	4, 0x90
.LBB3_86:                               # =>This Inner Loop Header: Depth=1
	leaq	1(%rsi), %rcx
	cmpq	%rax, %rcx
	jge	.LBB3_90
# BB#87:                                #   in Loop: Header=BB3_86 Depth=1
	movq	32(%r14), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rdx), %rdx
	cmpq	$3, %rdx
	je	.LBB3_85
# BB#88:                                #   in Loop: Header=BB3_86 Depth=1
	cmpq	$50528539, %rdx         # imm = 0x303011B
	je	.LBB3_85
# BB#89:                                #   in Loop: Header=BB3_86 Depth=1
	cmpq	$50528515, %rdx         # imm = 0x3030103
	movq	%rcx, %rsi
	jne	.LBB3_86
	jmp	.LBB3_85
.LBB3_90:
	movq	(%r14), %rax
	movl	%r12d, 184(%rax)
	movq	8(%r14), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	216(%rsp), %rsi
	movq	136(%rsp), %r8
	movl	132(%rsp), %ebp
.Ltmp176:
.Lcfi72:
	.cfi_escape 0x2e, 0x10
	movl	$0, %edx
	movl	$1, %ecx
	movl	$0, %r9d
	pushq	1544(%rsp)
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$16, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset -16
.Ltmp177:
	movq	(%rsp), %rbx            # 8-byte Reload
# BB#91:
	testl	%eax, %eax
	jne	.LBB3_139
# BB#92:
	leaq	400(%r14), %rsi
.Ltmp178:
.Lcfi76:
	.cfi_escape 0x2e, 0x00
	leaq	280(%rsp), %rdi
	callq	_ZN13CRecordVectorIyEC2ERKS0_
.Ltmp179:
# BB#93:
.Ltmp180:
.Lcfi77:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp181:
# BB#94:                                # %.noexc270
	leaq	64(%rbx), %rdi
.Ltmp182:
.Lcfi78:
	.cfi_escape 0x2e, 0x00
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp183:
# BB#95:                                # %.noexc271
	leaq	32(%rbx), %r15
.Ltmp184:
.Lcfi79:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp185:
# BB#96:                                # %.noexc272
	cmpl	$0, 308(%r14)
	jle	.LBB3_100
# BB#97:                                # %.lr.ph54.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_98:                               # =>This Inner Loop Header: Depth=1
	movq	312(%r14), %rax
	movl	(%rax,%rbx,8), %r12d
	movl	4(%rax,%rbx,8), %ebp
.Ltmp187:
.Lcfi80:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp188:
# BB#99:                                # %.noexc273
                                        #   in Loop: Header=BB3_98 Depth=1
	shlq	$32, %rbp
	orq	%r12, %rbp
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	48(%rdx), %rax
	movslq	44(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	incl	44(%rdx)
	incq	%rbx
	movslq	308(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_98
.LBB3_100:                              # %.preheader49.i
	movq	%r13, 264(%rsp)         # 8-byte Spill
	cmpl	$0, 276(%r14)
	jle	.LBB3_105
# BB#101:                               # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit35.lr.ph.i
	xorl	%r12d, %r12d
	movq	%r14, 272(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_102:                              # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit35.i
                                        # =>This Inner Loop Header: Depth=1
	movq	280(%r14), %rax
	movl	(%rax,%r12,8), %r13d
	movl	4(%rax,%r12,8), %ebp
	movq	296(%rsp), %rax
	movq	(%rax,%r12,8), %r15
.Ltmp190:
.Lcfi81:
	.cfi_escape 0x2e, 0x00
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp191:
	movq	(%rsp), %r14            # 8-byte Reload
# BB#103:                               # %.noexc274
                                        #   in Loop: Header=BB3_102 Depth=1
	shlq	$32, %rbp
	orq	%r13, %rbp
	movq	%r15, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 8(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rbx)
	movq	%rbp, 32(%rbx)
.Ltmp192:
.Lcfi82:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp193:
# BB#104:                               # %.noexc275
                                        #   in Loop: Header=BB3_102 Depth=1
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	incq	%r12
	movq	272(%rsp), %r14         # 8-byte Reload
	movslq	276(%r14), %rax
	cmpq	%rax, %r12
	jl	.LBB3_102
.LBB3_105:                              # %.preheader.i
	cmpl	$0, 340(%r14)
	movq	%r14, %r15
	movq	(%rsp), %r14            # 8-byte Reload
	movq	1536(%rsp), %r12
	movq	264(%rsp), %r13         # 8-byte Reload
	jle	.LBB3_109
# BB#106:                               # %.lr.ph.i266
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_107:                              # =>This Inner Loop Header: Depth=1
	movq	344(%r15), %rax
	movl	(%rax,%rbx,4), %ebp
.Ltmp195:
.Lcfi83:
	.cfi_escape 0x2e, 0x00
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp196:
# BB#108:                               # %.noexc276
                                        #   in Loop: Header=BB3_107 Depth=1
	movq	80(%r14), %rax
	movslq	76(%r14), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	76(%r14)
	incq	%rbx
	movslq	340(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_107
.LBB3_109:                              # %_ZN8NArchive3N7zL31ConvertBindInfoToFolderItemInfoERKN11NCoderMixer9CBindInfoE13CRecordVectorIyERNS0_7CFolderE.exit
.Ltmp200:
.Lcfi84:
	.cfi_escape 0x2e, 0x00
	leaq	280(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp201:
	movq	%r15, %r14
# BB#110:
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rbx
.Ltmp202:
.Lcfi85:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp203:
# BB#111:                               # %_ZN13CRecordVectorIyE3AddEy.exit265
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	cmpl	$2, 244(%r14)
	jl	.LBB3_117
# BB#112:                               # %.lr.ph423
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB3_113:                              # =>This Inner Loop Header: Depth=1
	movq	40(%rsp), %rax
	movq	-8(%rax,%rbp,8), %rbx
.Ltmp205:
.Lcfi86:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	_ZN16CInOutTempBuffer13WriteToStreamEP20ISequentialOutStream
.Ltmp206:
# BB#114:                               #   in Loop: Header=BB3_113 Depth=1
	testl	%eax, %eax
	jne	.LBB3_139
# BB#115:                               #   in Loop: Header=BB3_113 Depth=1
	movq	1152(%rbx), %rbx
.Ltmp207:
.Lcfi87:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp208:
# BB#116:                               # %.thread
                                        #   in Loop: Header=BB3_113 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbp
	movslq	244(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB3_113
.LBB3_117:                              # %.preheader
	movq	392(%r14), %rax
	cmpl	$0, 232(%rax)
	jle	.LBB3_130
# BB#118:                               # %.lr.ph420
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	96(%rcx), %r12
	xorl	%ebx, %ebx
	movl	$176, %r15d
	.p2align	4, 0x90
.LBB3_119:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_121 Depth 2
	movslq	180(%r14), %rcx
	testq	%rcx, %rcx
	jle	.LBB3_127
# BB#120:                               # %.lr.ph.i260
                                        #   in Loop: Header=BB3_119 Depth=1
	movq	256(%rax), %rax
	movl	(%rax,%rbx,4), %edx
	movq	184(%r14), %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_121:                              #   Parent Loop BB3_119 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%edx, (%rsi,%rax,8)
	je	.LBB3_125
# BB#122:                               #   in Loop: Header=BB3_121 Depth=2
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB3_121
	jmp	.LBB3_127
	.p2align	4, 0x90
.LBB3_125:                              # %_ZNK11NCoderMixer9CBindInfo21FindBinderForInStreamEj.exit
                                        #   in Loop: Header=BB3_119 Depth=1
	testl	%eax, %eax
	js	.LBB3_127
# BB#126:                               #   in Loop: Header=BB3_119 Depth=1
	movq	(%r14), %rcx
	movq	168(%rcx), %rcx
	movq	(%rcx,%rax,8), %rcx
	addq	%r15, %rcx
	jmp	.LBB3_128
	.p2align	4, 0x90
.LBB3_127:                              #   in Loop: Header=BB3_119 Depth=1
	movq	%r13, %rcx
.LBB3_128:                              # %_ZNK11NCoderMixer9CBindInfo21FindBinderForInStreamEj.exit.thread
                                        #   in Loop: Header=BB3_119 Depth=1
	movq	(%rcx), %rbp
.Ltmp210:
.Lcfi88:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp211:
# BB#129:                               #   in Loop: Header=BB3_119 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	112(%rdx), %rax
	movslq	108(%rdx), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 108(%rdx)
	incq	%rbx
	movq	392(%r14), %rax
	movslq	232(%rax), %rcx
	cmpq	%rcx, %rbx
	jl	.LBB3_119
.LBB3_130:                              # %._crit_edge
	xorl	%eax, %eax
	movq	192(%rsp), %rcx         # 8-byte Reload
	testl	%ecx, %ecx
	jle	.LBB3_139
# BB#131:                               # %.lr.ph
	movq	%rcx, %rbp
	subq	%rbp, %rbp
	shlq	$3, %rbp
	.p2align	4, 0x90
.LBB3_132:                              # =>This Inner Loop Header: Depth=1
	movq	32(%r14), %rax
	movq	%rcx, %r15
	movq	-8(%rax,%rcx,8), %r12
	movq	(%rsp), %rax            # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax,%rbp), %rbx
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_134
# BB#133:                               #   in Loop: Header=BB3_132 Depth=1
.Lcfi89:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB3_134:                              # %_ZN7CBufferIhE4FreeEv.exit.i
                                        #   in Loop: Header=BB3_132 Depth=1
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rbx)
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.LBB3_137
# BB#135:                               # %_ZN7CBufferIhE11SetCapacityEm.exit.i
                                        #   in Loop: Header=BB3_132 Depth=1
.Ltmp213:
.Lcfi90:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_Znam
.Ltmp214:
# BB#136:                               # %.noexc258
                                        #   in Loop: Header=BB3_132 Depth=1
	movq	%rax, 24(%rbx)
	movq	%r13, 16(%rbx)
	movq	16(%r12), %rdx
	movq	24(%r12), %rsi
.Lcfi91:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	memmove
.LBB3_137:                              # %_ZN7CBufferIhEaSERKS0_.exit
                                        #   in Loop: Header=BB3_132 Depth=1
	movq	%r15, %rcx
	decq	%rcx
	addq	$8, %rbp
	testq	%rcx, %rcx
	jg	.LBB3_132
# BB#138:
	xorl	%eax, %eax
.LBB3_139:                              # %.loopexit
.Ltmp218:
	movl	%eax, %r14d
.Lcfi92:
	.cfi_escape 0x2e, 0x00
	leaq	120(%rsp), %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp219:
# BB#140:
.Ltmp223:
.Lcfi93:
	.cfi_escape 0x2e, 0x00
	leaq	200(%rsp), %rdi
	movq	80(%rsp), %rbp          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp224:
# BB#141:
	movq	(%rbp), %rax
.Ltmp228:
.Lcfi94:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp229:
# BB#142:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit246
	movq	(%rbx), %rax
.Ltmp233:
.Lcfi95:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp234:
	jmp	.LBB3_146
.LBB3_143:
.Lcfi96:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NArchive3N7z8CEncoder16CreateMixerCoderEPKy
	testl	%eax, %eax
	jne	.LBB3_152
# BB#144:                               # %._crit_edge485
	movq	(%r14), %rdi
	jmp	.LBB3_1
.LBB3_145:
	movl	$-2147467259, %r14d     # imm = 0x80004005
.LBB3_146:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit244
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 88(%rsp)
.Ltmp247:
.Lcfi97:
	.cfi_escape 0x2e, 0x00
	leaq	88(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp248:
# BB#147:
.Ltmp253:
.Lcfi98:
	.cfi_escape 0x2e, 0x00
	leaq	88(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp254:
# BB#148:                               # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit235
	movq	$_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE+16, 152(%rsp)
.Ltmp264:
.Lcfi99:
	.cfi_escape 0x2e, 0x00
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp265:
# BB#149:
.Ltmp270:
.Lcfi100:
	.cfi_escape 0x2e, 0x00
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp271:
# BB#150:                               # %_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev.exit225
	movq	$_ZTV13CObjectVectorI16CInOutTempBufferE+16, 24(%rsp)
.Ltmp282:
.Lcfi101:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp283:
# BB#151:                               # %_ZN13CObjectVectorI16CInOutTempBufferED2Ev.exit211
.Lcfi102:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movl	%r14d, %eax
.LBB3_152:
	addq	$1480, %rsp             # imm = 0x5C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_153:
.Ltmp110:
	jmp	.LBB3_217
.LBB3_154:
.Ltmp105:
	movq	%rax, %r14
	leaq	232(%rsp), %r13
	jmp	.LBB3_188
.LBB3_155:
.Ltmp235:
	jmp	.LBB3_217
.LBB3_156:
.Ltmp230:
	jmp	.LBB3_161
.LBB3_157:
.Ltmp225:
	jmp	.LBB3_177
.LBB3_158:
.Ltmp220:
	movq	%rax, %r14
	jmp	.LBB3_208
.LBB3_159:
.Ltmp121:
	jmp	.LBB3_161
.LBB3_160:
.Ltmp118:
.LBB3_161:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit242
	movq	%rax, %r14
	jmp	.LBB3_210
.LBB3_162:
.Ltmp115:
	jmp	.LBB3_217
.LBB3_163:
.Ltmp238:
	jmp	.LBB3_217
.LBB3_164:                              # %.loopexit.split-lp323.loopexit.split-lp.loopexit.split-lp
.Ltmp186:
	jmp	.LBB3_194
.LBB3_165:
.Ltmp284:
	movq	%rax, %r14
.Ltmp285:
.Lcfi103:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp286:
	jmp	.LBB3_224
.LBB3_166:
.Ltmp287:
.Lcfi104:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_167:
.Ltmp272:
	movq	%rax, %r14
	jmp	.LBB3_222
.LBB3_168:
.Ltmp266:
	movq	%rax, %r14
.Ltmp267:
.Lcfi105:
	.cfi_escape 0x2e, 0x00
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp268:
	jmp	.LBB3_222
.LBB3_169:
.Ltmp269:
.Lcfi106:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_170:
.Ltmp255:
	movq	%rax, %r14
	jmp	.LBB3_220
.LBB3_171:
.Ltmp249:
	movq	%rax, %r14
.Ltmp250:
.Lcfi107:
	.cfi_escape 0x2e, 0x00
	leaq	88(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp251:
	jmp	.LBB3_220
.LBB3_172:
.Ltmp252:
.Lcfi108:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_173:                              # %.loopexit.split-lp336
.Ltmp139:
	movq	%rax, %r14
	leaq	120(%rsp), %r15
	movq	%r15, 184(%rsp)         # 8-byte Spill
	jmp	.LBB3_207
.LBB3_174:                              # %.loopexit.split-lp
.Ltmp204:
	jmp	.LBB3_184
.LBB3_175:                              # %.loopexit320
.Ltmp215:
	jmp	.LBB3_184
.LBB3_176:
.Ltmp134:
.LBB3_177:
	movq	%rax, %r14
	jmp	.LBB3_209
.LBB3_178:
.Ltmp212:
	jmp	.LBB3_184
.LBB3_179:
.Ltmp209:
	jmp	.LBB3_184
.LBB3_180:                              # %.loopexit322
.Ltmp197:
	jmp	.LBB3_194
.LBB3_181:                              # %.loopexit.split-lp323.loopexit.split-lp.loopexit
.Ltmp189:
	jmp	.LBB3_194
.LBB3_182:
.Ltmp170:
	jmp	.LBB3_204
.LBB3_183:
.Ltmp175:
.LBB3_184:
	movq	%rax, %r14
	jmp	.LBB3_207
.LBB3_185:
.Ltmp165:
	jmp	.LBB3_200
.LBB3_186:
.Ltmp155:
	jmp	.LBB3_200
.LBB3_187:
.Ltmp102:
	movq	%rax, %r14
.LBB3_188:
.Ltmp106:
.Lcfi109:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp107:
	jmp	.LBB3_218
.LBB3_189:                              # %.loopexit.split-lp342.loopexit
.Ltmp76:
	jmp	.LBB3_217
.LBB3_190:
.Ltmp79:
	jmp	.LBB3_217
.LBB3_191:
.Ltmp84:
	movq	%rax, %r14
.Lcfi110:
	.cfi_escape 0x2e, 0x00
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB3_212
.LBB3_192:
.Ltmp96:
	jmp	.LBB3_217
.LBB3_193:                              # %.loopexit.split-lp323.loopexit
.Ltmp194:
.LBB3_194:                              # %.loopexit.split-lp323
	movq	%rax, %r14
.Ltmp198:
.Lcfi111:
	.cfi_escape 0x2e, 0x00
	leaq	280(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp199:
	jmp	.LBB3_207
.LBB3_195:                              # %.loopexit335
.Ltmp142:
	movq	%rax, %r14
	movq	%r15, 184(%rsp)         # 8-byte Spill
	jmp	.LBB3_207
.LBB3_196:
.Ltmp160:
	movq	%rax, %r14
	movq	(%r13), %rax
.Ltmp161:
.Lcfi112:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp162:
	jmp	.LBB3_201
.LBB3_197:
.Ltmp60:
	movq	%rax, %r14
.Lcfi113:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB3_214
.LBB3_198:                              # %.loopexit341
.Ltmp99:
	jmp	.LBB3_217
.LBB3_199:
.Ltmp152:
.LBB3_200:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit250
	movq	%rax, %r14
.LBB3_201:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit250
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_205
# BB#202:
	movq	(%rdi), %rax
.Ltmp166:
.Lcfi114:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp167:
	jmp	.LBB3_205
.LBB3_203:
.Ltmp147:
.LBB3_204:
	movq	%rax, %r14
.LBB3_205:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_207
# BB#206:
	movq	(%rdi), %rax
.Ltmp171:
.Lcfi115:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp172:
.LBB3_207:
.Ltmp216:
.Lcfi116:
	.cfi_escape 0x2e, 0x00
	movq	184(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp217:
.LBB3_208:
.Ltmp221:
.Lcfi117:
	.cfi_escape 0x2e, 0x00
	leaq	200(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp222:
.LBB3_209:
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp226:
.Lcfi118:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp227:
.LBB3_210:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit242
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp231:
.Lcfi119:
	.cfi_escape 0x2e, 0x00
	callq	*16(%rax)
.Ltmp232:
	jmp	.LBB3_218
.LBB3_211:
.Ltmp91:
	movq	%rax, %r14
.LBB3_212:
	movq	(%r13), %rax
.Ltmp92:
.Lcfi120:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	*16(%rax)
.Ltmp93:
	jmp	.LBB3_218
.LBB3_213:
.Ltmp63:
	movq	%rax, %r14
.LBB3_214:                              # %.body191
.Ltmp64:
.Lcfi121:
	.cfi_escape 0x2e, 0x00
	leaq	312(%rsp), %rdi
	callq	_ZN16CInOutTempBufferD1Ev
.Ltmp65:
	jmp	.LBB3_218
.LBB3_215:
.Ltmp68:
	jmp	.LBB3_217
.LBB3_216:                              # %.loopexit.split-lp342.loopexit.split-lp
.Ltmp73:
.LBB3_217:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit205
	movq	%rax, %r14
.LBB3_218:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit205
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 88(%rsp)
.Ltmp239:
.Lcfi122:
	.cfi_escape 0x2e, 0x00
	leaq	88(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp240:
# BB#219:
.Ltmp245:
.Lcfi123:
	.cfi_escape 0x2e, 0x00
	leaq	88(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp246:
.LBB3_220:
	movq	$_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE+16, 152(%rsp)
.Ltmp256:
.Lcfi124:
	.cfi_escape 0x2e, 0x00
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp257:
# BB#221:
.Ltmp262:
.Lcfi125:
	.cfi_escape 0x2e, 0x00
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp263:
.LBB3_222:
	movq	$_ZTV13CObjectVectorI16CInOutTempBufferE+16, 24(%rsp)
.Ltmp273:
.Lcfi126:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp274:
# BB#223:
.Ltmp279:
.Lcfi127:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp280:
.LBB3_224:                              # %unwind_resume
.Lcfi128:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_225:
.Ltmp241:
	movq	%rax, %rbx
.Ltmp242:
.Lcfi129:
	.cfi_escape 0x2e, 0x00
	leaq	88(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp243:
	jmp	.LBB3_232
.LBB3_226:
.Ltmp244:
.Lcfi130:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_227:
.Ltmp258:
	movq	%rax, %rbx
.Ltmp259:
.Lcfi131:
	.cfi_escape 0x2e, 0x00
	leaq	152(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp260:
	jmp	.LBB3_232
.LBB3_228:
.Ltmp261:
.Lcfi132:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_229:
.Ltmp275:
	movq	%rax, %rbx
.Ltmp276:
.Lcfi133:
	.cfi_escape 0x2e, 0x00
	leaq	24(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp277:
	jmp	.LBB3_232
.LBB3_230:
.Ltmp278:
.Lcfi134:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_231:
.Ltmp281:
	movq	%rax, %rbx
.LBB3_232:                              # %.body
.Lcfi135:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN8NArchive3N7z8CEncoder6EncodeEP19ISequentialInStreamPKyS5_RNS0_7CFolderEP20ISequentialOutStreamR13CRecordVectorIyEP21ICompressProgressInfo, .Lfunc_end3-_ZN8NArchive3N7z8CEncoder6EncodeEP19ISequentialInStreamPKyS5_RNS0_7CFolderEP20ISequentialOutStreamR13CRecordVectorIyEP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\375\206\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\364\006"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp54-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp68-.Lfunc_begin1   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp63-.Lfunc_begin1   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin1   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin1   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin1   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp72-.Ltmp69         #   Call between .Ltmp69 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin1   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin1   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin1   #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp91-.Lfunc_begin1   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin1   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp90-.Ltmp85         #   Call between .Ltmp85 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin1   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin1   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin1   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Ltmp236-.Ltmp98        #   Call between .Ltmp98 and .Ltmp236
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin1  # >> Call Site 16 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin1  #     jumps to .Ltmp238
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin1  # >> Call Site 17 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin1  #     jumps to .Ltmp102
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin1  # >> Call Site 18 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin1  #     jumps to .Ltmp105
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin1  # >> Call Site 19 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin1  #     jumps to .Ltmp110
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin1  # >> Call Site 20 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp238-.Lfunc_begin1  #     jumps to .Ltmp238
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin1  # >> Call Site 21 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin1  #     jumps to .Ltmp115
	.byte	0                       #   On action: cleanup
	.long	.Ltmp116-.Lfunc_begin1  # >> Call Site 22 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin1  #     jumps to .Ltmp118
	.byte	0                       #   On action: cleanup
	.long	.Ltmp119-.Lfunc_begin1  # >> Call Site 23 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin1  #     jumps to .Ltmp121
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin1  # >> Call Site 24 <<
	.long	.Ltmp133-.Ltmp122       #   Call between .Ltmp122 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin1  #     jumps to .Ltmp134
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin1  # >> Call Site 25 <<
	.long	.Ltmp138-.Ltmp135       #   Call between .Ltmp135 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin1  #     jumps to .Ltmp139
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin1  # >> Call Site 26 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp142-.Lfunc_begin1  #     jumps to .Ltmp142
	.byte	0                       #   On action: cleanup
	.long	.Ltmp143-.Lfunc_begin1  # >> Call Site 27 <<
	.long	.Ltmp146-.Ltmp143       #   Call between .Ltmp143 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin1  #     jumps to .Ltmp147
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin1  # >> Call Site 28 <<
	.long	.Ltmp151-.Ltmp148       #   Call between .Ltmp148 and .Ltmp151
	.long	.Ltmp152-.Lfunc_begin1  #     jumps to .Ltmp152
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin1  # >> Call Site 29 <<
	.long	.Ltmp154-.Ltmp153       #   Call between .Ltmp153 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin1  #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin1  # >> Call Site 30 <<
	.long	.Ltmp159-.Ltmp156       #   Call between .Ltmp156 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin1  #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin1  # >> Call Site 31 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin1  #     jumps to .Ltmp165
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin1  # >> Call Site 32 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin1  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp173-.Lfunc_begin1  # >> Call Site 33 <<
	.long	.Ltmp174-.Ltmp173       #   Call between .Ltmp173 and .Ltmp174
	.long	.Ltmp175-.Lfunc_begin1  #     jumps to .Ltmp175
	.byte	0                       #   On action: cleanup
	.long	.Ltmp176-.Lfunc_begin1  # >> Call Site 34 <<
	.long	.Ltmp179-.Ltmp176       #   Call between .Ltmp176 and .Ltmp179
	.long	.Ltmp204-.Lfunc_begin1  #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin1  # >> Call Site 35 <<
	.long	.Ltmp185-.Ltmp180       #   Call between .Ltmp180 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin1  #     jumps to .Ltmp186
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin1  # >> Call Site 36 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin1  #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp190-.Lfunc_begin1  # >> Call Site 37 <<
	.long	.Ltmp193-.Ltmp190       #   Call between .Ltmp190 and .Ltmp193
	.long	.Ltmp194-.Lfunc_begin1  #     jumps to .Ltmp194
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin1  # >> Call Site 38 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin1  #     jumps to .Ltmp197
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin1  # >> Call Site 39 <<
	.long	.Ltmp203-.Ltmp200       #   Call between .Ltmp200 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin1  #     jumps to .Ltmp204
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin1  # >> Call Site 40 <<
	.long	.Ltmp208-.Ltmp205       #   Call between .Ltmp205 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin1  #     jumps to .Ltmp209
	.byte	0                       #   On action: cleanup
	.long	.Ltmp210-.Lfunc_begin1  # >> Call Site 41 <<
	.long	.Ltmp211-.Ltmp210       #   Call between .Ltmp210 and .Ltmp211
	.long	.Ltmp212-.Lfunc_begin1  #     jumps to .Ltmp212
	.byte	0                       #   On action: cleanup
	.long	.Ltmp213-.Lfunc_begin1  # >> Call Site 42 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp215-.Lfunc_begin1  #     jumps to .Ltmp215
	.byte	0                       #   On action: cleanup
	.long	.Ltmp214-.Lfunc_begin1  # >> Call Site 43 <<
	.long	.Ltmp218-.Ltmp214       #   Call between .Ltmp214 and .Ltmp218
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin1  # >> Call Site 44 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin1  #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin1  # >> Call Site 45 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin1  #     jumps to .Ltmp225
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin1  # >> Call Site 46 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin1  #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin1  # >> Call Site 47 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin1  #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin1  # >> Call Site 48 <<
	.long	.Ltmp247-.Ltmp234       #   Call between .Ltmp234 and .Ltmp247
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp247-.Lfunc_begin1  # >> Call Site 49 <<
	.long	.Ltmp248-.Ltmp247       #   Call between .Ltmp247 and .Ltmp248
	.long	.Ltmp249-.Lfunc_begin1  #     jumps to .Ltmp249
	.byte	0                       #   On action: cleanup
	.long	.Ltmp253-.Lfunc_begin1  # >> Call Site 50 <<
	.long	.Ltmp254-.Ltmp253       #   Call between .Ltmp253 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin1  #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp264-.Lfunc_begin1  # >> Call Site 51 <<
	.long	.Ltmp265-.Ltmp264       #   Call between .Ltmp264 and .Ltmp265
	.long	.Ltmp266-.Lfunc_begin1  #     jumps to .Ltmp266
	.byte	0                       #   On action: cleanup
	.long	.Ltmp270-.Lfunc_begin1  # >> Call Site 52 <<
	.long	.Ltmp271-.Ltmp270       #   Call between .Ltmp270 and .Ltmp271
	.long	.Ltmp272-.Lfunc_begin1  #     jumps to .Ltmp272
	.byte	0                       #   On action: cleanup
	.long	.Ltmp282-.Lfunc_begin1  # >> Call Site 53 <<
	.long	.Ltmp283-.Ltmp282       #   Call between .Ltmp282 and .Ltmp283
	.long	.Ltmp284-.Lfunc_begin1  #     jumps to .Ltmp284
	.byte	0                       #   On action: cleanup
	.long	.Ltmp283-.Lfunc_begin1  # >> Call Site 54 <<
	.long	.Ltmp285-.Ltmp283       #   Call between .Ltmp283 and .Ltmp285
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp285-.Lfunc_begin1  # >> Call Site 55 <<
	.long	.Ltmp286-.Ltmp285       #   Call between .Ltmp285 and .Ltmp286
	.long	.Ltmp287-.Lfunc_begin1  #     jumps to .Ltmp287
	.byte	1                       #   On action: 1
	.long	.Ltmp267-.Lfunc_begin1  # >> Call Site 56 <<
	.long	.Ltmp268-.Ltmp267       #   Call between .Ltmp267 and .Ltmp268
	.long	.Ltmp269-.Lfunc_begin1  #     jumps to .Ltmp269
	.byte	1                       #   On action: 1
	.long	.Ltmp250-.Lfunc_begin1  # >> Call Site 57 <<
	.long	.Ltmp251-.Ltmp250       #   Call between .Ltmp250 and .Ltmp251
	.long	.Ltmp252-.Lfunc_begin1  #     jumps to .Ltmp252
	.byte	1                       #   On action: 1
	.long	.Ltmp106-.Lfunc_begin1  # >> Call Site 58 <<
	.long	.Ltmp65-.Ltmp106        #   Call between .Ltmp106 and .Ltmp65
	.long	.Ltmp281-.Lfunc_begin1  #     jumps to .Ltmp281
	.byte	1                       #   On action: 1
	.long	.Ltmp239-.Lfunc_begin1  # >> Call Site 59 <<
	.long	.Ltmp240-.Ltmp239       #   Call between .Ltmp239 and .Ltmp240
	.long	.Ltmp241-.Lfunc_begin1  #     jumps to .Ltmp241
	.byte	1                       #   On action: 1
	.long	.Ltmp245-.Lfunc_begin1  # >> Call Site 60 <<
	.long	.Ltmp246-.Ltmp245       #   Call between .Ltmp245 and .Ltmp246
	.long	.Ltmp281-.Lfunc_begin1  #     jumps to .Ltmp281
	.byte	1                       #   On action: 1
	.long	.Ltmp256-.Lfunc_begin1  # >> Call Site 61 <<
	.long	.Ltmp257-.Ltmp256       #   Call between .Ltmp256 and .Ltmp257
	.long	.Ltmp258-.Lfunc_begin1  #     jumps to .Ltmp258
	.byte	1                       #   On action: 1
	.long	.Ltmp262-.Lfunc_begin1  # >> Call Site 62 <<
	.long	.Ltmp263-.Ltmp262       #   Call between .Ltmp262 and .Ltmp263
	.long	.Ltmp281-.Lfunc_begin1  #     jumps to .Ltmp281
	.byte	1                       #   On action: 1
	.long	.Ltmp273-.Lfunc_begin1  # >> Call Site 63 <<
	.long	.Ltmp274-.Ltmp273       #   Call between .Ltmp273 and .Ltmp274
	.long	.Ltmp275-.Lfunc_begin1  #     jumps to .Ltmp275
	.byte	1                       #   On action: 1
	.long	.Ltmp279-.Lfunc_begin1  # >> Call Site 64 <<
	.long	.Ltmp280-.Ltmp279       #   Call between .Ltmp279 and .Ltmp280
	.long	.Ltmp281-.Lfunc_begin1  #     jumps to .Ltmp281
	.byte	1                       #   On action: 1
	.long	.Ltmp280-.Lfunc_begin1  # >> Call Site 65 <<
	.long	.Ltmp242-.Ltmp280       #   Call between .Ltmp280 and .Ltmp242
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp242-.Lfunc_begin1  # >> Call Site 66 <<
	.long	.Ltmp243-.Ltmp242       #   Call between .Ltmp242 and .Ltmp243
	.long	.Ltmp244-.Lfunc_begin1  #     jumps to .Ltmp244
	.byte	1                       #   On action: 1
	.long	.Ltmp259-.Lfunc_begin1  # >> Call Site 67 <<
	.long	.Ltmp260-.Ltmp259       #   Call between .Ltmp259 and .Ltmp260
	.long	.Ltmp261-.Lfunc_begin1  #     jumps to .Ltmp261
	.byte	1                       #   On action: 1
	.long	.Ltmp276-.Lfunc_begin1  # >> Call Site 68 <<
	.long	.Ltmp277-.Ltmp276       #   Call between .Ltmp276 and .Ltmp277
	.long	.Ltmp278-.Lfunc_begin1  #     jumps to .Ltmp278
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z8CEncoder13EncoderConstrEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CEncoder13EncoderConstrEv,@function
_ZN8NArchive3N7z8CEncoder13EncoderConstrEv: # @_ZN8NArchive3N7z8CEncoder13EncoderConstrEv
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi139:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi140:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi142:
	.cfi_def_cfa_offset 208
.Lcfi143:
	.cfi_offset %rbx, -56
.Lcfi144:
	.cfi_offset %r12, -48
.Lcfi145:
	.cfi_offset %r13, -40
.Lcfi146:
	.cfi_offset %r14, -32
.Lcfi147:
	.cfi_offset %r15, -24
.Lcfi148:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	cmpb	$0, 432(%r13)
	jne	.LBB4_97
# BB#1:
	leaq	48(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	60(%r13), %eax
	testl	%eax, %eax
	je	.LBB4_4
# BB#2:                                 # %.preheader179
	jle	.LBB4_3
# BB#18:                                # %.lr.ph223
	leaq	168(%r13), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	136(%r13), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	232(%r13), %rbx
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB4_19:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_25 Depth 2
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	64(%r13), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	40(%rcx), %r12d
	movl	44(%rcx), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	cmpl	$0, 92(%r13)
	jne	.LBB4_26
# BB#20:                                #   in Loop: Header=BB4_19 Depth=1
	decl	%eax
	cltq
	cmpq	%rax, %rbp
	jge	.LBB4_22
# BB#21:                                #   in Loop: Header=BB4_19 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %r14d
	movq	8(%rsp), %rax           # 8-byte Reload
	shlq	$32, %rax
	orq	%rax, %r14
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	184(%r13), %rax
	movslq	180(%r13), %rcx
	movq	%r14, (%rax,%rcx,8)
	incl	180(%r13)
	cmpl	$2, %r12d
	jae	.LBB4_24
	jmp	.LBB4_26
.LBB4_22:                               #   in Loop: Header=BB4_19 Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
	movq	248(%r13), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, (%rax)
	cmpl	$2, %r12d
	jb	.LBB4_26
.LBB4_24:                               # %.lr.ph217.preheader
                                        #   in Loop: Header=BB4_19 Depth=1
	leal	-1(%r12), %r15d
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	1(%rax), %r14d
	.p2align	4, 0x90
.LBB4_25:                               # %.lr.ph217
                                        #   Parent Loop BB4_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	248(%r13), %rax
	movslq	244(%r13), %rcx
	movl	%r14d, (%rax,%rcx,4)
	incl	244(%r13)
	incl	%r14d
	decl	%r15d
	jne	.LBB4_25
	.p2align	4, 0x90
.LBB4_26:                               # %.loopexit178
                                        #   in Loop: Header=BB4_19 Depth=1
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	addl	%ecx, %r15d
	movq	8(%rsp), %rax           # 8-byte Reload
	addl	%r12d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	shlq	$32, %r12
	orq	%rcx, %r12
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	152(%r13), %rax
	movslq	148(%r13), %rcx
	movq	%r12, (%rax,%rcx,8)
	incl	148(%r13)
	incq	%rbp
	movslq	60(%r13), %rax
	cmpq	%rax, %rbp
	jl	.LBB4_19
	jmp	.LBB4_27
.LBB4_4:
	cmpb	$0, 116(%r13)
	je	.LBB4_70
# BB#5:
	cmpl	$0, 92(%r13)
	jne	.LBB4_70
# BB#6:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rsp)
	movq	$8, 88(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 64(%rsp)
	movl	$1, 96(%rsp)
	movl	$1, 100(%rsp)
	movq	$116459265, 56(%rsp)    # imm = 0x6F10701
.Ltmp309:
	leaq	56(%rsp), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
.Ltmp310:
# BB#7:
	leaq	136(%r13), %rdi
.Ltmp311:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp312:
# BB#8:
	movq	152(%r13), %rax
	movslq	148(%r13), %rcx
	movabsq	$4294967297, %rdx       # imm = 0x100000001
	movq	%rdx, (%rax,%rcx,8)
	incl	148(%r13)
	leaq	200(%r13), %rdi
.Ltmp313:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp314:
# BB#9:
	movq	216(%r13), %rax
	movslq	212(%r13), %rcx
	movl	$0, (%rax,%rcx,4)
	incl	212(%r13)
	leaq	232(%r13), %rdi
.Ltmp315:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp316:
# BB#10:
	movq	248(%r13), %rax
	movslq	244(%r13), %rcx
	movl	$0, (%rax,%rcx,4)
	incl	244(%r13)
	leaq	64(%rsp), %rbp
	movq	$_ZTV13CObjectVectorI5CPropE+16, 64(%rsp)
.Ltmp327:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp328:
# BB#11:                                # %_ZN7CMethodD2Ev.exit133
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	jmp	.LBB4_92
.LBB4_3:                                # %.preheader179.._crit_edge224_crit_edge
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
.LBB4_27:                               # %._crit_edge224
	movl	92(%r13), %eax
	testl	%eax, %eax
	je	.LBB4_33
# BB#28:                                # %.preheader176
	jle	.LBB4_39
# BB#29:                                # %.lr.ph215
	leaq	168(%r13), %r12
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_30:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_46 Depth 2
                                        #     Child Loop BB4_48 Depth 2
                                        #     Child Loop BB4_54 Depth 2
                                        #     Child Loop BB4_56 Depth 2
	movq	96(%r13), %r8
	movq	%r14, %r9
	shlq	$4, %r9
	movl	(%r8,%r9), %ecx
	testl	%ecx, %ecx
	movl	$0, %edx
	je	.LBB4_49
# BB#31:                                # %.lr.ph.i
                                        #   in Loop: Header=BB4_30 Depth=1
	movq	152(%r13), %rdi
	leal	-1(%rcx), %r10d
	testb	$3, %cl
	je	.LBB4_32
# BB#45:                                # %.prol.preheader
                                        #   in Loop: Header=BB4_30 Depth=1
	movl	%ecx, %esi
	andl	$3, %esi
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_46:                               #   Parent Loop BB4_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	(%rdi,%rax,8), %edx
	incq	%rax
	cmpl	%eax, %esi
	jne	.LBB4_46
	jmp	.LBB4_47
	.p2align	4, 0x90
.LBB4_32:                               #   in Loop: Header=BB4_30 Depth=1
	xorl	%eax, %eax
	xorl	%edx, %edx
.LBB4_47:                               # %.prol.loopexit
                                        #   in Loop: Header=BB4_30 Depth=1
	cmpl	$3, %r10d
	jb	.LBB4_49
	.p2align	4, 0x90
.LBB4_48:                               #   Parent Loop BB4_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cltq
	addl	(%rdi,%rax,8), %edx
	leal	1(%rax), %esi
	movslq	%esi, %rsi
	addl	(%rdi,%rsi,8), %edx
	leal	2(%rax), %esi
	movslq	%esi, %rsi
	addl	(%rdi,%rsi,8), %edx
	leal	3(%rax), %esi
	movslq	%esi, %rsi
	addl	(%rdi,%rsi,8), %edx
	addl	$4, %eax
	cmpl	%eax, %ecx
	jne	.LBB4_48
.LBB4_49:                               # %_ZNK11NCoderMixer9CBindInfo21GetCoderInStreamIndexEj.exit
                                        #   in Loop: Header=BB4_30 Depth=1
	addl	4(%r8,%r9), %edx
	movl	8(%r8,%r9), %ebp
	testl	%ebp, %ebp
	je	.LBB4_50
# BB#51:                                # %.lr.ph.i140
                                        #   in Loop: Header=BB4_30 Depth=1
	movq	152(%r13), %rdi
	leal	-1(%rbp), %r10d
	testb	$3, %bpl
	je	.LBB4_52
# BB#53:                                # %.prol.preheader292
                                        #   in Loop: Header=BB4_30 Depth=1
	movl	%ebp, %esi
	andl	$3, %esi
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_54:                               #   Parent Loop BB4_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	4(%rdi,%rcx,8), %eax
	incq	%rcx
	cmpl	%ecx, %esi
	jne	.LBB4_54
	jmp	.LBB4_55
	.p2align	4, 0x90
.LBB4_50:                               #   in Loop: Header=BB4_30 Depth=1
	xorl	%eax, %eax
	jmp	.LBB4_57
	.p2align	4, 0x90
.LBB4_52:                               #   in Loop: Header=BB4_30 Depth=1
	xorl	%ecx, %ecx
	xorl	%eax, %eax
.LBB4_55:                               # %.prol.loopexit293
                                        #   in Loop: Header=BB4_30 Depth=1
	cmpl	$3, %r10d
	jb	.LBB4_57
	.p2align	4, 0x90
.LBB4_56:                               #   Parent Loop BB4_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ecx, %rcx
	addl	4(%rdi,%rcx,8), %eax
	leal	1(%rcx), %esi
	movslq	%esi, %rsi
	addl	4(%rdi,%rsi,8), %eax
	leal	2(%rcx), %esi
	movslq	%esi, %rsi
	addl	4(%rdi,%rsi,8), %eax
	leal	3(%rcx), %esi
	movslq	%esi, %rsi
	addl	4(%rdi,%rsi,8), %eax
	addl	$4, %ecx
	cmpl	%ecx, %ebp
	jne	.LBB4_56
.LBB4_57:                               # %_ZNK11NCoderMixer9CBindInfo22GetCoderOutStreamIndexEj.exit
                                        #   in Loop: Header=BB4_30 Depth=1
	addl	12(%r8,%r9), %eax
	shlq	$32, %rax
	movl	%edx, %ebp
	orq	%rax, %rbp
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	184(%r13), %rax
	movslq	180(%r13), %rcx
	movq	%rbp, (%rax,%rcx,8)
	incl	180(%r13)
	incq	%r14
	movslq	92(%r13), %rax
	cmpq	%rax, %r14
	jl	.LBB4_30
.LBB4_39:                               # %.preheader175
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB4_33
# BB#40:                                # %.lr.ph213
	leaq	232(%r13), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_41:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_43 Depth 2
	movslq	180(%r13), %rax
	testq	%rax, %rax
	jle	.LBB4_59
# BB#42:                                # %.lr.ph.i146
                                        #   in Loop: Header=BB4_41 Depth=1
	movq	184(%r13), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_43:                               #   Parent Loop BB4_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, 4(%rdx,%rcx,8)
	je	.LBB4_58
# BB#44:                                #   in Loop: Header=BB4_43 Depth=2
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB4_43
	jmp	.LBB4_59
	.p2align	4, 0x90
.LBB4_58:                               # %_ZNK11NCoderMixer9CBindInfo22FindBinderForOutStreamEj.exit
                                        #   in Loop: Header=BB4_41 Depth=1
	cmpl	$-1, %ecx
	jne	.LBB4_60
	.p2align	4, 0x90
.LBB4_59:                               # %_ZNK11NCoderMixer9CBindInfo22FindBinderForOutStreamEj.exit.thread
                                        #   in Loop: Header=BB4_41 Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	248(%r13), %rax
	movslq	244(%r13), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	244(%r13)
.LBB4_60:                               #   in Loop: Header=BB4_41 Depth=1
	incl	%ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jne	.LBB4_41
.LBB4_33:                               # %.preheader
	testl	%r15d, %r15d
	jle	.LBB4_64
# BB#34:                                # %.lr.ph209
	leaq	200(%r13), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_35:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_37 Depth 2
	movslq	180(%r13), %rax
	testq	%rax, %rax
	jle	.LBB4_62
# BB#36:                                # %.lr.ph.i149
                                        #   in Loop: Header=BB4_35 Depth=1
	movq	184(%r13), %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_37:                               #   Parent Loop BB4_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, (%rdx,%rcx,8)
	je	.LBB4_61
# BB#38:                                #   in Loop: Header=BB4_37 Depth=2
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB4_37
	jmp	.LBB4_62
	.p2align	4, 0x90
.LBB4_61:                               # %_ZNK11NCoderMixer9CBindInfo21FindBinderForInStreamEj.exit
                                        #   in Loop: Header=BB4_35 Depth=1
	cmpl	$-1, %ecx
	jne	.LBB4_63
	.p2align	4, 0x90
.LBB4_62:                               # %_ZNK11NCoderMixer9CBindInfo21FindBinderForInStreamEj.exit.thread
                                        #   in Loop: Header=BB4_35 Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	216(%r13), %rax
	movslq	212(%r13), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	212(%r13)
.LBB4_63:                               #   in Loop: Header=BB4_35 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	jne	.LBB4_35
.LBB4_64:                               # %._crit_edge210
	cmpl	$0, 212(%r13)
	je	.LBB4_70
# BB#65:
	movl	148(%r13), %eax
	testl	%eax, %eax
	je	.LBB4_70
# BB#66:                                # %.lr.ph.i152.lr.ph
	movq	152(%r13), %rcx
	movq	216(%r13), %rdx
.LBB4_67:                               # %.lr.ph.i152
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_68 Depth 2
                                        #     Child Loop BB4_76 Depth 2
                                        #     Child Loop BB4_78 Depth 2
                                        #     Child Loop BB4_81 Depth 2
	movl	(%rdx), %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_68:                               #   Parent Loop BB4_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rbp
	subl	(%rcx,%rbp,8), %edi
	jb	.LBB4_71
# BB#69:                                #   in Loop: Header=BB4_68 Depth=2
	incl	%edx
	incb	%sil
	cmpl	%eax, %edx
	jb	.LBB4_68
	jmp	.LBB4_70
	.p2align	4, 0x90
.LBB4_71:                               # %_ZNK11NCoderMixer9CBindInfo12FindInStreamEjRjS1_.exit
                                        #   in Loop: Header=BB4_67 Depth=1
	testl	%edx, %edx
	je	.LBB4_72
# BB#73:                                # %.lr.ph.i153.preheader
                                        #   in Loop: Header=BB4_67 Depth=1
	leal	-1(%rdx), %r8d
	testb	$3, %dl
	je	.LBB4_74
# BB#75:                                # %.lr.ph.i153.prol.preheader
                                        #   in Loop: Header=BB4_67 Depth=1
	andb	$3, %sil
	movzbl	%sil, %edi
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_76:                               # %.lr.ph.i153.prol
                                        #   Parent Loop BB4_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addl	4(%rcx,%rsi,8), %ebp
	incq	%rsi
	cmpl	%esi, %edi
	jne	.LBB4_76
	jmp	.LBB4_77
.LBB4_72:                               #   in Loop: Header=BB4_67 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB4_79
.LBB4_74:                               #   in Loop: Header=BB4_67 Depth=1
	xorl	%esi, %esi
	xorl	%ebp, %ebp
.LBB4_77:                               # %.lr.ph.i153.prol.loopexit
                                        #   in Loop: Header=BB4_67 Depth=1
	cmpl	$3, %r8d
	jb	.LBB4_79
	.p2align	4, 0x90
.LBB4_78:                               # %.lr.ph.i153
                                        #   Parent Loop BB4_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	addl	4(%rcx,%rsi,8), %ebp
	leal	1(%rsi), %edi
	movslq	%edi, %rdi
	addl	4(%rcx,%rdi,8), %ebp
	leal	2(%rsi), %edi
	movslq	%edi, %rdi
	addl	4(%rcx,%rdi,8), %ebp
	leal	3(%rsi), %edi
	movslq	%edi, %rdi
	addl	4(%rcx,%rdi,8), %ebp
	addl	$4, %esi
	cmpl	%esi, %edx
	jne	.LBB4_78
.LBB4_79:                               # %_ZNK11NCoderMixer9CBindInfo22GetCoderOutStreamIndexEj.exit159
                                        #   in Loop: Header=BB4_67 Depth=1
	movslq	180(%r13), %rsi
	testq	%rsi, %rsi
	jle	.LBB4_84
# BB#80:                                # %.lr.ph.i160
                                        #   in Loop: Header=BB4_67 Depth=1
	movq	184(%r13), %rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_81:                               #   Parent Loop BB4_67 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebp, 4(%rdx)
	je	.LBB4_83
# BB#82:                                #   in Loop: Header=BB4_81 Depth=2
	incq	%rdi
	addq	$8, %rdx
	cmpq	%rsi, %rdi
	jl	.LBB4_81
	jmp	.LBB4_84
	.p2align	4, 0x90
.LBB4_83:                               # %_ZNK11NCoderMixer9CBindInfo22FindBinderForOutStreamEj.exit164
                                        #   in Loop: Header=BB4_67 Depth=1
	testl	%edi, %edi
	jns	.LBB4_67
.LBB4_84:                               # %_ZNK11NCoderMixer9CBindInfo22FindBinderForOutStreamEj.exit164.thread.preheader
	leaq	232(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	244(%r13), %rax
	testq	%rax, %rax
	jle	.LBB4_89
# BB#85:                                # %.lr.ph205
	movq	248(%r13), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_86:                               # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, (%rcx,%rsi,4)
	je	.LBB4_87
# BB#88:                                # %_ZNK11NCoderMixer9CBindInfo22FindBinderForOutStreamEj.exit164.thread
                                        #   in Loop: Header=BB4_86 Depth=1
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB4_86
	jmp	.LBB4_89
.LBB4_87:
	movl	$1, %edx
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN17CBaseRecordVector6DeleteEii
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
	movq	248(%r13), %rax
	movl	%ebp, (%rax)
.LBB4_89:                               # %.loopexit172
	cmpb	$0, 116(%r13)
	je	.LBB4_92
# BB#90:
	movl	244(%r13), %r12d
	testl	%r12d, %r12d
	jle	.LBB4_91
# BB#98:                                # %.lr.ph202
	leaq	168(%r13), %r14
	movl	%r15d, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_99:                               # =>This Inner Loop Header: Depth=1
	leal	(%r15,%rbp), %ebx
	movq	248(%r13), %rax
	movl	(%rax,%rbp,4), %eax
	shlq	$32, %rax
	orq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	184(%r13), %rax
	movslq	180(%r13), %rcx
	movq	%rbx, (%rax,%rcx,8)
	incl	180(%r13)
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB4_99
# BB#100:                               # %._crit_edge203
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector5ClearEv
	testl	%r12d, %r12d
	jle	.LBB4_92
# BB#101:                               # %.lr.ph199
	leaq	112(%rsp), %rbp
	leaq	136(%r13), %rbx
	xorl	%r14d, %r14d
	leaq	104(%rsp), %r15
	.p2align	4, 0x90
.LBB4_102:                              # =>This Inner Loop Header: Depth=1
	leaq	120(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$8, 136(%rsp)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 112(%rsp)
	movl	$1, 144(%rsp)
	movl	$1, 148(%rsp)
	movq	$116459265, 104(%rsp)   # imm = 0x6F10701
.Ltmp288:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	callq	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
.Ltmp289:
# BB#103:                               #   in Loop: Header=BB4_102 Depth=1
.Ltmp290:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp291:
# BB#104:                               #   in Loop: Header=BB4_102 Depth=1
	movq	152(%r13), %rax
	movslq	148(%r13), %rcx
	movabsq	$4294967297, %rdx       # imm = 0x100000001
	movq	%rdx, (%rax,%rcx,8)
	incl	148(%r13)
.Ltmp292:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp293:
# BB#105:                               #   in Loop: Header=BB4_102 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rax,%r14), %eax
	movq	248(%r13), %rcx
	movslq	244(%r13), %rdx
	movl	%eax, (%rcx,%rdx,4)
	incl	244(%r13)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 112(%rsp)
.Ltmp303:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp304:
# BB#106:                               # %_ZN7CMethodD2Ev.exit148
                                        #   in Loop: Header=BB4_102 Depth=1
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	incl	%r14d
	cmpl	%r12d, %r14d
	jl	.LBB4_102
	jmp	.LBB4_92
.LBB4_91:                               # %._crit_edge203.thread
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector5ClearEv
.LBB4_92:                               # %.loopexit
	movslq	60(%r13), %rbp
	testq	%rbp, %rbp
	jle	.LBB4_95
# BB#93:                                # %.lr.ph
	leaq	400(%r13), %r14
	incq	%rbp
	.p2align	4, 0x90
.LBB4_94:                               # =>This Inner Loop Header: Depth=1
	movq	64(%r13), %rax
	movq	-16(%rax,%rbp,8), %rax
	movq	(%rax), %rbx
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	416(%r13), %rax
	movslq	412(%r13), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 412(%r13)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB4_94
.LBB4_95:                               # %._crit_edge
	movl	$272, %edi              # imm = 0x110
	callq	_Znwm
	movq	%rax, %rbp
	leaq	136(%r13), %rsi
.Ltmp333:
	movq	%rbp, %rdi
	callq	_ZN11NCoderMixer21CBindReverseConverterC1ERKNS_9CBindInfoE
.Ltmp334:
# BB#96:
	movq	%rbp, 392(%r13)
	leaq	264(%r13), %rsi
	movq	%rbp, %rdi
	callq	_ZN11NCoderMixer21CBindReverseConverter21CreateReverseBindInfoERNS_9CBindInfoE
	movb	$1, 432(%r13)
.LBB4_97:
	xorl	%eax, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_70:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB4_12:
.Ltmp329:
	movq	%rax, %rbx
.Ltmp330:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp331:
	jmp	.LBB4_116
.LBB4_13:
.Ltmp332:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_115:
.Ltmp335:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_16:
.Ltmp317:
	movq	%rax, %rbx
	leaq	64(%rsp), %rbp
	movq	$_ZTV13CObjectVectorI5CPropE+16, 64(%rsp)
.Ltmp318:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp319:
# BB#17:                                # %_ZN13CObjectVectorI5CPropED2Ev.exit.i134
.Ltmp324:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp325:
	jmp	.LBB4_116
.LBB4_14:
.Ltmp320:
	movq	%rax, %rbx
.Ltmp321:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp322:
	jmp	.LBB4_114
.LBB4_15:
.Ltmp323:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_107:
.Ltmp305:
	movq	%rax, %rbx
.Ltmp306:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp307:
	jmp	.LBB4_116
.LBB4_108:
.Ltmp308:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_111:
.Ltmp294:
	movq	%rax, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, 112(%rsp)
.Ltmp295:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp296:
# BB#112:                               # %_ZN13CObjectVectorI5CPropED2Ev.exit.i
.Ltmp301:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp302:
.LBB4_116:                              # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_109:
.Ltmp297:
	movq	%rax, %rbx
.Ltmp298:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp299:
	jmp	.LBB4_114
.LBB4_110:
.Ltmp300:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_113:
.Ltmp326:
	movq	%rax, %rbx
.LBB4_114:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive3N7z8CEncoder13EncoderConstrEv, .Lfunc_end4-_ZN8NArchive3N7z8CEncoder13EncoderConstrEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\200\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp309-.Lfunc_begin2  #   Call between .Lfunc_begin2 and .Ltmp309
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp309-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp316-.Ltmp309       #   Call between .Ltmp309 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin2  #     jumps to .Ltmp317
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp328-.Ltmp327       #   Call between .Ltmp327 and .Ltmp328
	.long	.Ltmp329-.Lfunc_begin2  #     jumps to .Ltmp329
	.byte	0                       #   On action: cleanup
	.long	.Ltmp328-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Ltmp288-.Ltmp328       #   Call between .Ltmp328 and .Ltmp288
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp288-.Lfunc_begin2  # >> Call Site 5 <<
	.long	.Ltmp293-.Ltmp288       #   Call between .Ltmp288 and .Ltmp293
	.long	.Ltmp294-.Lfunc_begin2  #     jumps to .Ltmp294
	.byte	0                       #   On action: cleanup
	.long	.Ltmp303-.Lfunc_begin2  # >> Call Site 6 <<
	.long	.Ltmp304-.Ltmp303       #   Call between .Ltmp303 and .Ltmp304
	.long	.Ltmp305-.Lfunc_begin2  #     jumps to .Ltmp305
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin2  # >> Call Site 7 <<
	.long	.Ltmp333-.Ltmp304       #   Call between .Ltmp304 and .Ltmp333
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp333-.Lfunc_begin2  # >> Call Site 8 <<
	.long	.Ltmp334-.Ltmp333       #   Call between .Ltmp333 and .Ltmp334
	.long	.Ltmp335-.Lfunc_begin2  #     jumps to .Ltmp335
	.byte	0                       #   On action: cleanup
	.long	.Ltmp334-.Lfunc_begin2  # >> Call Site 9 <<
	.long	.Ltmp330-.Ltmp334       #   Call between .Ltmp334 and .Ltmp330
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin2  # >> Call Site 10 <<
	.long	.Ltmp331-.Ltmp330       #   Call between .Ltmp330 and .Ltmp331
	.long	.Ltmp332-.Lfunc_begin2  #     jumps to .Ltmp332
	.byte	1                       #   On action: 1
	.long	.Ltmp331-.Lfunc_begin2  # >> Call Site 11 <<
	.long	.Ltmp318-.Ltmp331       #   Call between .Ltmp331 and .Ltmp318
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp318-.Lfunc_begin2  # >> Call Site 12 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin2  #     jumps to .Ltmp320
	.byte	1                       #   On action: 1
	.long	.Ltmp324-.Lfunc_begin2  # >> Call Site 13 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin2  #     jumps to .Ltmp326
	.byte	1                       #   On action: 1
	.long	.Ltmp321-.Lfunc_begin2  # >> Call Site 14 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin2  #     jumps to .Ltmp323
	.byte	1                       #   On action: 1
	.long	.Ltmp306-.Lfunc_begin2  # >> Call Site 15 <<
	.long	.Ltmp307-.Ltmp306       #   Call between .Ltmp306 and .Ltmp307
	.long	.Ltmp308-.Lfunc_begin2  #     jumps to .Ltmp308
	.byte	1                       #   On action: 1
	.long	.Ltmp295-.Lfunc_begin2  # >> Call Site 16 <<
	.long	.Ltmp296-.Ltmp295       #   Call between .Ltmp295 and .Ltmp296
	.long	.Ltmp297-.Lfunc_begin2  #     jumps to .Ltmp297
	.byte	1                       #   On action: 1
	.long	.Ltmp301-.Lfunc_begin2  # >> Call Site 17 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp326-.Lfunc_begin2  #     jumps to .Ltmp326
	.byte	1                       #   On action: 1
	.long	.Ltmp302-.Lfunc_begin2  # >> Call Site 18 <<
	.long	.Ltmp298-.Ltmp302       #   Call between .Ltmp302 and .Ltmp298
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp298-.Lfunc_begin2  # >> Call Site 19 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin2  #     jumps to .Ltmp300
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIyEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIyEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIyEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyEC2ERKS0_,@function
_ZN13CRecordVectorIyEC2ERKS0_:          # @_ZN13CRecordVectorIyEC2ERKS0_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 48
.Lcfi154:
	.cfi_offset %rbx, -48
.Lcfi155:
	.cfi_offset %r12, -40
.Lcfi156:
	.cfi_offset %r13, -32
.Lcfi157:
	.cfi_offset %r14, -24
.Lcfi158:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIyE+16, (%r12)
.Ltmp336:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp337:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp338:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp339:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB5_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_4:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp341:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp342:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB5_4
.LBB5_6:                                # %_ZN13CRecordVectorIyEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_8:                                # %.loopexit.split-lp
.Ltmp340:
	jmp	.LBB5_9
.LBB5_7:                                # %.loopexit
.Ltmp343:
.LBB5_9:
	movq	%rax, %r14
.Ltmp344:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp345:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_11:
.Ltmp346:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN13CRecordVectorIyEC2ERKS0_, .Lfunc_end5-_ZN13CRecordVectorIyEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp336-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp339-.Ltmp336       #   Call between .Ltmp336 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin3  #     jumps to .Ltmp340
	.byte	0                       #   On action: cleanup
	.long	.Ltmp341-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp343-.Lfunc_begin3  #     jumps to .Ltmp343
	.byte	0                       #   On action: cleanup
	.long	.Ltmp344-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp345-.Ltmp344       #   Call between .Ltmp344 and .Ltmp345
	.long	.Ltmp346-.Lfunc_begin3  #     jumps to .Ltmp346
	.byte	1                       #   On action: 1
	.long	.Ltmp345-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp345    #   Call between .Ltmp345 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, (%rbx)
.Ltmp347:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp348:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB6_2:
.Ltmp349:
	movq	%rax, %r14
.Ltmp350:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp351:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_4:
.Ltmp352:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev, .Lfunc_end6-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp347-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp348-.Ltmp347       #   Call between .Ltmp347 and .Ltmp348
	.long	.Ltmp349-.Lfunc_begin4  #     jumps to .Ltmp349
	.byte	0                       #   On action: cleanup
	.long	.Ltmp348-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp350-.Ltmp348       #   Call between .Ltmp348 and .Ltmp350
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp350-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin4  #     jumps to .Ltmp352
	.byte	1                       #   On action: 1
	.long	.Ltmp351-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp351    #   Call between .Ltmp351 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev,"axG",@progbits,_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev,comdat
	.weak	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev,@function
_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev: # @_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 32
.Lcfi167:
	.cfi_offset %rbx, -24
.Lcfi168:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE+16, (%rbx)
.Ltmp353:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp354:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB7_2:
.Ltmp355:
	movq	%rax, %r14
.Ltmp356:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp357:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_4:
.Ltmp358:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev, .Lfunc_end7-_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp353-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp354-.Ltmp353       #   Call between .Ltmp353 and .Ltmp354
	.long	.Ltmp355-.Lfunc_begin5  #     jumps to .Ltmp355
	.byte	0                       #   On action: cleanup
	.long	.Ltmp354-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp356-.Ltmp354       #   Call between .Ltmp354 and .Ltmp356
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp356-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp357-.Ltmp356       #   Call between .Ltmp356 and .Ltmp357
	.long	.Ltmp358-.Lfunc_begin5  #     jumps to .Ltmp358
	.byte	1                       #   On action: 1
	.long	.Ltmp357-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Lfunc_end7-.Ltmp357    #   Call between .Ltmp357 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI16CInOutTempBufferED2Ev,"axG",@progbits,_ZN13CObjectVectorI16CInOutTempBufferED2Ev,comdat
	.weak	_ZN13CObjectVectorI16CInOutTempBufferED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI16CInOutTempBufferED2Ev,@function
_ZN13CObjectVectorI16CInOutTempBufferED2Ev: # @_ZN13CObjectVectorI16CInOutTempBufferED2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi171:
	.cfi_def_cfa_offset 32
.Lcfi172:
	.cfi_offset %rbx, -24
.Lcfi173:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI16CInOutTempBufferE+16, (%rbx)
.Ltmp359:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp360:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB8_2:
.Ltmp361:
	movq	%rax, %r14
.Ltmp362:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp363:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_4:
.Ltmp364:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN13CObjectVectorI16CInOutTempBufferED2Ev, .Lfunc_end8-_ZN13CObjectVectorI16CInOutTempBufferED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp359-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin6  #     jumps to .Ltmp361
	.byte	0                       #   On action: cleanup
	.long	.Ltmp360-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp362-.Ltmp360       #   Call between .Ltmp360 and .Ltmp362
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp362-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin6  #     jumps to .Ltmp364
	.byte	1                       #   On action: 1
	.long	.Ltmp363-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp363    #   Call between .Ltmp363 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z8CEncoderC2ERKNS0_22CCompressionMethodModeE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CEncoderC2ERKNS0_22CCompressionMethodModeE,@function
_ZN8NArchive3N7z8CEncoderC2ERKNS0_22CCompressionMethodModeE: # @_ZN8NArchive3N7z8CEncoderC2ERKNS0_22CCompressionMethodModeE
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi174:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi175:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi176:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi177:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi178:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi180:
	.cfi_def_cfa_offset 64
.Lcfi181:
	.cfi_offset %rbx, -56
.Lcfi182:
	.cfi_offset %r12, -48
.Lcfi183:
	.cfi_offset %r13, -40
.Lcfi184:
	.cfi_offset %r14, -32
.Lcfi185:
	.cfi_offset %r15, -24
.Lcfi186:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	$0, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	$8, 40(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, 16(%rbx)
	leaq	48(%rbx), %r15
.Ltmp365:
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
.Ltmp366:
# BB#1:
	leaq	136(%rbx), %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movq	$8, 160(%rbx)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE+16, 136(%rbx)
	movups	%xmm0, 176(%rbx)
	movq	$8, 192(%rbx)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE+16, 168(%rbx)
	movups	%xmm0, 208(%rbx)
	movq	$4, 224(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 200(%rbx)
	movups	%xmm0, 240(%rbx)
	movq	$4, 256(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 232(%rbx)
	leaq	264(%rbx), %r12
	movups	%xmm0, 272(%rbx)
	movq	$8, 288(%rbx)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE+16, 264(%rbx)
	movups	%xmm0, 304(%rbx)
	movq	$8, 320(%rbx)
	movq	$_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE+16, 296(%rbx)
	movups	%xmm0, 336(%rbx)
	movq	$4, 352(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 328(%rbx)
	movups	%xmm0, 368(%rbx)
	movq	$4, 384(%rbx)
	movq	$_ZTV13CRecordVectorIjE+16, 360(%rbx)
	movq	$0, 392(%rbx)
	leaq	400(%rbx), %rbp
	movups	%xmm0, 408(%rbx)
	movq	$8, 424(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 400(%rbx)
	movb	$0, 432(%rbx)
	cmpl	$0, 12(%r13)
	jne	.LBB9_10
# BB#2:                                 # %_ZNK8NArchive3N7z22CCompressionMethodMode7IsEmptyEv.exit
	cmpb	$0, 68(%r13)
	je	.LBB9_3
.LBB9_10:                               # %_ZNK8NArchive3N7z22CCompressionMethodMode7IsEmptyEv.exit.thread
.Ltmp368:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_
.Ltmp369:
# BB#11:
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_3:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp370:
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp371:
# BB#38:
.LBB9_12:
.Ltmp372:
	movq	%rax, %r13
.Ltmp373:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp374:
# BB#13:
	leaq	360(%rbx), %rdi
.Ltmp375:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp376:
# BB#14:
	leaq	328(%rbx), %rdi
.Ltmp380:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp381:
# BB#15:
	leaq	296(%rbx), %rdi
.Ltmp385:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp386:
# BB#16:
.Ltmp391:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp392:
# BB#17:                                # %_ZN11NCoderMixer9CBindInfoD2Ev.exit
	leaq	232(%rbx), %rdi
.Ltmp393:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp394:
# BB#18:
	leaq	200(%rbx), %rdi
.Ltmp398:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp399:
# BB#19:
	leaq	168(%rbx), %rdi
.Ltmp403:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp404:
# BB#20:
.Ltmp409:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp410:
# BB#21:                                # %_ZN11NCoderMixer9CBindInfoD2Ev.exit19
.Ltmp411:
	movq	%r15, %rdi
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp412:
	jmp	.LBB9_5
.LBB9_29:
.Ltmp405:
	movq	%rax, %r15
	jmp	.LBB9_32
.LBB9_30:
.Ltmp400:
	movq	%rax, %r15
	jmp	.LBB9_31
.LBB9_28:
.Ltmp395:
	movq	%rax, %r15
	leaq	200(%rbx), %rdi
.Ltmp396:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp397:
.LBB9_31:
	addq	$168, %rbx
.Ltmp401:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp402:
.LBB9_32:
.Ltmp406:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp407:
	jmp	.LBB9_37
.LBB9_33:
.Ltmp408:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_23:
.Ltmp387:
	movq	%rax, %r15
	jmp	.LBB9_26
.LBB9_24:
.Ltmp382:
	movq	%rax, %r15
	jmp	.LBB9_25
.LBB9_22:
.Ltmp377:
	movq	%rax, %r15
	leaq	328(%rbx), %rdi
.Ltmp378:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp379:
.LBB9_25:
	addq	$296, %rbx              # imm = 0x128
.Ltmp383:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp384:
.LBB9_26:
.Ltmp388:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp389:
	jmp	.LBB9_37
.LBB9_27:
.Ltmp390:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_4:
.Ltmp367:
	movq	%rax, %r13
.LBB9_5:
	leaq	16(%rbx), %rbp
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbp)
.Ltmp413:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp414:
# BB#6:
.Ltmp419:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp420:
# BB#7:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp421:
	callq	*16(%rax)
.Ltmp422:
.LBB9_9:                                # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	movq	%r13, %rdi
	callq	_Unwind_Resume
.LBB9_34:
.Ltmp415:
	movq	%rax, %r15
.Ltmp416:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp417:
	jmp	.LBB9_37
.LBB9_35:
.Ltmp418:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_36:
.Ltmp423:
	movq	%rax, %r15
.LBB9_37:                               # %.body
	movq	%r15, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN8NArchive3N7z8CEncoderC2ERKNS0_22CCompressionMethodModeE, .Lfunc_end9-_ZN8NArchive3N7z8CEncoderC2ERKNS0_22CCompressionMethodModeE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\200\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\367\001"              # Call site table length
	.long	.Ltmp365-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp366-.Ltmp365       #   Call between .Ltmp365 and .Ltmp366
	.long	.Ltmp367-.Lfunc_begin7  #     jumps to .Ltmp367
	.byte	0                       #   On action: cleanup
	.long	.Ltmp368-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp369-.Ltmp368       #   Call between .Ltmp368 and .Ltmp369
	.long	.Ltmp372-.Lfunc_begin7  #     jumps to .Ltmp372
	.byte	0                       #   On action: cleanup
	.long	.Ltmp369-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp370-.Ltmp369       #   Call between .Ltmp369 and .Ltmp370
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp370-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp371-.Ltmp370       #   Call between .Ltmp370 and .Ltmp371
	.long	.Ltmp372-.Lfunc_begin7  #     jumps to .Ltmp372
	.byte	0                       #   On action: cleanup
	.long	.Ltmp373-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp374-.Ltmp373       #   Call between .Ltmp373 and .Ltmp374
	.long	.Ltmp423-.Lfunc_begin7  #     jumps to .Ltmp423
	.byte	1                       #   On action: 1
	.long	.Ltmp375-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp376-.Ltmp375       #   Call between .Ltmp375 and .Ltmp376
	.long	.Ltmp377-.Lfunc_begin7  #     jumps to .Ltmp377
	.byte	1                       #   On action: 1
	.long	.Ltmp380-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp381-.Ltmp380       #   Call between .Ltmp380 and .Ltmp381
	.long	.Ltmp382-.Lfunc_begin7  #     jumps to .Ltmp382
	.byte	1                       #   On action: 1
	.long	.Ltmp385-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp386-.Ltmp385       #   Call between .Ltmp385 and .Ltmp386
	.long	.Ltmp387-.Lfunc_begin7  #     jumps to .Ltmp387
	.byte	1                       #   On action: 1
	.long	.Ltmp391-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp392-.Ltmp391       #   Call between .Ltmp391 and .Ltmp392
	.long	.Ltmp423-.Lfunc_begin7  #     jumps to .Ltmp423
	.byte	1                       #   On action: 1
	.long	.Ltmp393-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Ltmp394-.Ltmp393       #   Call between .Ltmp393 and .Ltmp394
	.long	.Ltmp395-.Lfunc_begin7  #     jumps to .Ltmp395
	.byte	1                       #   On action: 1
	.long	.Ltmp398-.Lfunc_begin7  # >> Call Site 11 <<
	.long	.Ltmp399-.Ltmp398       #   Call between .Ltmp398 and .Ltmp399
	.long	.Ltmp400-.Lfunc_begin7  #     jumps to .Ltmp400
	.byte	1                       #   On action: 1
	.long	.Ltmp403-.Lfunc_begin7  # >> Call Site 12 <<
	.long	.Ltmp404-.Ltmp403       #   Call between .Ltmp403 and .Ltmp404
	.long	.Ltmp405-.Lfunc_begin7  #     jumps to .Ltmp405
	.byte	1                       #   On action: 1
	.long	.Ltmp409-.Lfunc_begin7  # >> Call Site 13 <<
	.long	.Ltmp412-.Ltmp409       #   Call between .Ltmp409 and .Ltmp412
	.long	.Ltmp423-.Lfunc_begin7  #     jumps to .Ltmp423
	.byte	1                       #   On action: 1
	.long	.Ltmp396-.Lfunc_begin7  # >> Call Site 14 <<
	.long	.Ltmp407-.Ltmp396       #   Call between .Ltmp396 and .Ltmp407
	.long	.Ltmp408-.Lfunc_begin7  #     jumps to .Ltmp408
	.byte	1                       #   On action: 1
	.long	.Ltmp378-.Lfunc_begin7  # >> Call Site 15 <<
	.long	.Ltmp389-.Ltmp378       #   Call between .Ltmp378 and .Ltmp389
	.long	.Ltmp390-.Lfunc_begin7  #     jumps to .Ltmp390
	.byte	1                       #   On action: 1
	.long	.Ltmp413-.Lfunc_begin7  # >> Call Site 16 <<
	.long	.Ltmp414-.Ltmp413       #   Call between .Ltmp413 and .Ltmp414
	.long	.Ltmp415-.Lfunc_begin7  #     jumps to .Ltmp415
	.byte	1                       #   On action: 1
	.long	.Ltmp419-.Lfunc_begin7  # >> Call Site 17 <<
	.long	.Ltmp422-.Ltmp419       #   Call between .Ltmp419 and .Ltmp422
	.long	.Ltmp423-.Lfunc_begin7  #     jumps to .Ltmp423
	.byte	1                       #   On action: 1
	.long	.Ltmp422-.Lfunc_begin7  # >> Call Site 18 <<
	.long	.Ltmp416-.Ltmp422       #   Call between .Ltmp422 and .Ltmp416
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp416-.Lfunc_begin7  # >> Call Site 19 <<
	.long	.Ltmp417-.Ltmp416       #   Call between .Ltmp416 and .Ltmp417
	.long	.Ltmp418-.Lfunc_begin7  #     jumps to .Ltmp418
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeC2Ev,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeC2Ev,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev,@function
_ZN8NArchive3N7z22CCompressionMethodModeC2Ev: # @_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r15
.Lcfi187:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi188:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 32
.Lcfi190:
	.cfi_offset %rbx, -32
.Lcfi191:
	.cfi_offset %r14, -24
.Lcfi192:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$8, 24(%rbx)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
	leaq	32(%rbx), %r15
	movups	%xmm0, 40(%rbx)
	movq	$16, 56(%rbx)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE+16, 32(%rbx)
	movl	$1, 64(%rbx)
	movb	$0, 68(%rbx)
	movups	%xmm0, 72(%rbx)
.Ltmp424:
	movl	$16, %edi
	callq	_Znam
.Ltmp425:
# BB#1:
	movq	%rax, 72(%rbx)
	movl	$0, (%rax)
	movl	$4, 84(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB10_2:
.Ltmp426:
	movq	%rax, %r14
.Ltmp427:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp428:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp429:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp430:
# BB#4:
.Ltmp435:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp436:
# BB#5:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB10_6:
.Ltmp431:
	movq	%rax, %r14
.Ltmp432:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp433:
	jmp	.LBB10_9
.LBB10_7:
.Ltmp434:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_8:
.Ltmp437:
	movq	%rax, %r14
.LBB10_9:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeC2Ev, .Lfunc_end10-_ZN8NArchive3N7z22CCompressionMethodModeC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp424-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp425-.Ltmp424       #   Call between .Ltmp424 and .Ltmp425
	.long	.Ltmp426-.Lfunc_begin8  #     jumps to .Ltmp426
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp437-.Lfunc_begin8  #     jumps to .Ltmp437
	.byte	1                       #   On action: 1
	.long	.Ltmp429-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp430-.Ltmp429       #   Call between .Ltmp429 and .Ltmp430
	.long	.Ltmp431-.Lfunc_begin8  #     jumps to .Ltmp431
	.byte	1                       #   On action: 1
	.long	.Ltmp435-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp436-.Ltmp435       #   Call between .Ltmp435 and .Ltmp436
	.long	.Ltmp437-.Lfunc_begin8  #     jumps to .Ltmp437
	.byte	1                       #   On action: 1
	.long	.Ltmp436-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp432-.Ltmp436       #   Call between .Ltmp436 and .Ltmp432
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp432-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp433-.Ltmp432       #   Call between .Ltmp432 and .Ltmp433
	.long	.Ltmp434-.Lfunc_begin8  #     jumps to .Ltmp434
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_,@function
_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_: # @_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi193:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi194:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi195:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi196:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi197:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi198:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi199:
	.cfi_def_cfa_offset 80
.Lcfi200:
	.cfi_offset %rbx, -56
.Lcfi201:
	.cfi_offset %r12, -48
.Lcfi202:
	.cfi_offset %r13, -40
.Lcfi203:
	.cfi_offset %r14, -32
.Lcfi204:
	.cfi_offset %r15, -24
.Lcfi205:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	12(%r14), %ebx
	movl	12(%r15), %esi
	addl	%ebx, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%ebx, %ebx
	jle	.LBB11_3
# BB#1:                                 # %.lr.ph.i.i
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbp,8), %rsi
	movq	%r15, %rdi
	callq	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB11_2
.LBB11_3:                               # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEEaSERKS3_.exit
	leaq	32(%r15), %r12
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	44(%r14), %r13d
	movl	44(%r15), %esi
	addl	%r13d, %esi
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r13d, %r13d
	jle	.LBB11_6
# BB#4:                                 # %.lr.ph.i.i6
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_5:                               # =>This Inner Loop Header: Depth=1
	movq	48(%r14), %rax
	movups	(%rax,%rbx), %xmm0
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	48(%r15), %rax
	movslq	44(%r15), %rcx
	shlq	$4, %rcx
	movaps	(%rsp), %xmm0           # 16-byte Reload
	movups	%xmm0, (%rax,%rcx)
	incl	44(%r15)
	addq	$16, %rbx
	decq	%r13
	jne	.LBB11_5
.LBB11_6:                               # %_ZN13CRecordVectorIN8NArchive3N7z5CBindEEaSERKS3_.exit
	movb	68(%r14), %al
	movb	%al, 68(%r15)
	movl	64(%r14), %eax
	movl	%eax, 64(%r15)
	cmpq	%r15, %r14
	je	.LBB11_15
# BB#7:
	movl	$0, 80(%r15)
	movq	72(%r15), %rbx
	movl	$0, (%rbx)
	movslq	80(%r14), %r12
	incq	%r12
	movl	84(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB11_12
# BB#8:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB11_11
# BB#9:
	testl	%ebp, %ebp
	jle	.LBB11_11
# BB#10:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	80(%r15), %rax
.LBB11_11:                              # %._crit_edge16.i.i
	movq	%r13, 72(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 84(%r15)
	movq	%r13, %rbx
.LBB11_12:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	72(%r14), %rax
	.p2align	4, 0x90
.LBB11_13:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB11_13
# BB#14:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	80(%r14), %eax
	movl	%eax, 80(%r15)
.LBB11_15:                              # %_ZN11CStringBaseIwEaSERKS0_.exit
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_, .Lfunc_end11-_ZN8NArchive3N7z22CCompressionMethodModeaSERKS1_
	.cfi_endproc

	.section	.text._ZN8NArchive3N7z22CCompressionMethodModeD2Ev,"axG",@progbits,_ZN8NArchive3N7z22CCompressionMethodModeD2Ev,comdat
	.weak	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev,@function
_ZN8NArchive3N7z22CCompressionMethodModeD2Ev: # @_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi206:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi207:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi208:
	.cfi_def_cfa_offset 32
.Lcfi209:
	.cfi_offset %rbx, -24
.Lcfi210:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_2
# BB#1:
	callq	_ZdaPv
.LBB12_2:                               # %_ZN11CStringBaseIwED2Ev.exit
	leaq	32(%rbx), %rdi
.Ltmp438:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp439:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp450:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp451:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB12_5:
.Ltmp452:
	movq	%rax, %r14
.Ltmp453:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp454:
	jmp	.LBB12_6
.LBB12_7:
.Ltmp455:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB12_8:
.Ltmp440:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp441:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp442:
# BB#9:
.Ltmp447:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp448:
.LBB12_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB12_12:
.Ltmp449:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB12_10:
.Ltmp443:
	movq	%rax, %r14
.Ltmp444:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp445:
# BB#13:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB12_11:
.Ltmp446:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev, .Lfunc_end12-_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp438-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp439-.Ltmp438       #   Call between .Ltmp438 and .Ltmp439
	.long	.Ltmp440-.Lfunc_begin9  #     jumps to .Ltmp440
	.byte	0                       #   On action: cleanup
	.long	.Ltmp450-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp451-.Ltmp450       #   Call between .Ltmp450 and .Ltmp451
	.long	.Ltmp452-.Lfunc_begin9  #     jumps to .Ltmp452
	.byte	0                       #   On action: cleanup
	.long	.Ltmp451-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp453-.Ltmp451       #   Call between .Ltmp451 and .Ltmp453
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp453-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp454-.Ltmp453       #   Call between .Ltmp453 and .Ltmp454
	.long	.Ltmp455-.Lfunc_begin9  #     jumps to .Ltmp455
	.byte	1                       #   On action: 1
	.long	.Ltmp441-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp442-.Ltmp441       #   Call between .Ltmp441 and .Ltmp442
	.long	.Ltmp443-.Lfunc_begin9  #     jumps to .Ltmp443
	.byte	1                       #   On action: 1
	.long	.Ltmp447-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Ltmp448-.Ltmp447       #   Call between .Ltmp447 and .Ltmp448
	.long	.Ltmp449-.Lfunc_begin9  #     jumps to .Ltmp449
	.byte	1                       #   On action: 1
	.long	.Ltmp448-.Lfunc_begin9  # >> Call Site 7 <<
	.long	.Ltmp444-.Ltmp448       #   Call between .Ltmp448 and .Ltmp444
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp444-.Lfunc_begin9  # >> Call Site 8 <<
	.long	.Ltmp445-.Ltmp444       #   Call between .Ltmp444 and .Ltmp445
	.long	.Ltmp446-.Lfunc_begin9  #     jumps to .Ltmp446
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi211:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi212:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi213:
	.cfi_def_cfa_offset 32
.Lcfi214:
	.cfi_offset %rbx, -24
.Lcfi215:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp456:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp457:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB13_2:
.Ltmp458:
	movq	%rax, %r14
.Ltmp459:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp460:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB13_4:
.Ltmp461:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev, .Lfunc_end13-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp456-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp457-.Ltmp456       #   Call between .Ltmp456 and .Ltmp457
	.long	.Ltmp458-.Lfunc_begin10 #     jumps to .Ltmp458
	.byte	0                       #   On action: cleanup
	.long	.Ltmp457-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp459-.Ltmp457       #   Call between .Ltmp457 and .Ltmp459
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp459-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp460-.Ltmp459       #   Call between .Ltmp459 and .Ltmp460
	.long	.Ltmp461-.Lfunc_begin10 #     jumps to .Ltmp461
	.byte	1                       #   On action: 1
	.long	.Ltmp460-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end13-.Ltmp460   #   Call between .Ltmp460 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r15
.Lcfi216:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi217:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi218:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi219:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi220:
	.cfi_def_cfa_offset 48
.Lcfi221:
	.cfi_offset %rbx, -40
.Lcfi222:
	.cfi_offset %r12, -32
.Lcfi223:
	.cfi_offset %r14, -24
.Lcfi224:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	(%r15), %rax
	movq	%rax, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	$8, 32(%rbx)
	movq	$_ZTV13CObjectVectorI5CPropE+16, 8(%rbx)
	leaq	8(%rbx), %r12
.Ltmp462:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp463:
# BB#1:                                 # %.noexc.i.i.i
	leaq	8(%r15), %rsi
.Ltmp464:
	movq	%r12, %rdi
	callq	_ZN13CObjectVectorI5CPropEpLERKS1_
.Ltmp465:
# BB#2:
	movq	40(%r15), %rax
	movq	%rax, 40(%rbx)
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rcx
	movslq	12(%r14), %rax
	movq	%rbx, (%rcx,%rax,8)
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r14)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB14_3:
.Ltmp466:
	movq	%rax, %r14
.Ltmp467:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp468:
# BB#4:                                 # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_5:
.Ltmp469:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_, .Lfunc_end14-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE3AddERKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp462-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp462
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp462-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp465-.Ltmp462       #   Call between .Ltmp462 and .Ltmp465
	.long	.Ltmp466-.Lfunc_begin11 #     jumps to .Ltmp466
	.byte	0                       #   On action: cleanup
	.long	.Ltmp465-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp467-.Ltmp465       #   Call between .Ltmp465 and .Ltmp467
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp467-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp468-.Ltmp467       #   Call between .Ltmp467 and .Ltmp468
	.long	.Ltmp469-.Lfunc_begin11 #     jumps to .Ltmp469
	.byte	1                       #   On action: 1
	.long	.Ltmp468-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Lfunc_end14-.Ltmp468   #   Call between .Ltmp468 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z8CEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CEncoderD2Ev,@function
_ZN8NArchive3N7z8CEncoderD2Ev:          # @_ZN8NArchive3N7z8CEncoderD2Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r15
.Lcfi225:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi227:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi228:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi229:
	.cfi_def_cfa_offset 48
.Lcfi230:
	.cfi_offset %rbx, -40
.Lcfi231:
	.cfi_offset %r12, -32
.Lcfi232:
	.cfi_offset %r14, -24
.Lcfi233:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movq	392(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB15_3
# BB#1:
.Ltmp470:
	movq	%rbx, %rdi
	callq	_ZN11NCoderMixer21CBindReverseConverterD2Ev
.Ltmp471:
# BB#2:
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB15_3:
	leaq	400(%r12), %rdi
.Ltmp475:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp476:
# BB#4:
	leaq	264(%r12), %r15
	leaq	360(%r12), %rdi
.Ltmp496:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp497:
# BB#5:
	leaq	328(%r12), %rdi
.Ltmp501:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp502:
# BB#6:
	leaq	296(%r12), %rdi
.Ltmp506:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp507:
# BB#7:
.Ltmp512:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp513:
# BB#8:                                 # %_ZN11NCoderMixer9CBindInfoD2Ev.exit
	leaq	136(%r12), %r15
	leaq	232(%r12), %rdi
.Ltmp533:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp534:
# BB#9:
	leaq	200(%r12), %rdi
.Ltmp538:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp539:
# BB#10:
	leaq	168(%r12), %rdi
.Ltmp543:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp544:
# BB#11:
.Ltmp549:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp550:
# BB#12:                                # %_ZN11NCoderMixer9CBindInfoD2Ev.exit13
	leaq	48(%r12), %rdi
.Ltmp554:
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp555:
# BB#13:
	leaq	16(%r12), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, 16(%r12)
.Ltmp565:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp566:
# BB#14:
.Ltmp571:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp572:
# BB#15:                                # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB15_31
# BB#16:
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmpq	*16(%rax)               # TAILCALL
.LBB15_31:                              # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB15_32:
.Ltmp472:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	leaq	400(%r12), %rdi
.Ltmp473:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp474:
	jmp	.LBB15_35
.LBB15_57:
.Ltmp573:
	movq	%rax, %r14
	jmp	.LBB15_61
.LBB15_29:
.Ltmp567:
	movq	%rax, %r14
.Ltmp568:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp569:
	jmp	.LBB15_61
.LBB15_30:
.Ltmp570:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_58:
.Ltmp556:
	movq	%rax, %r14
	jmp	.LBB15_59
.LBB15_50:
.Ltmp551:
	movq	%rax, %r14
	jmp	.LBB15_43
.LBB15_24:
.Ltmp545:
	movq	%rax, %r14
	jmp	.LBB15_27
.LBB15_25:
.Ltmp540:
	movq	%rax, %r14
	jmp	.LBB15_26
.LBB15_23:
.Ltmp535:
	movq	%rax, %r14
	leaq	200(%r12), %rdi
.Ltmp536:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp537:
.LBB15_26:
	leaq	168(%r12), %rdi
.Ltmp541:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp542:
.LBB15_27:
.Ltmp546:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp547:
	jmp	.LBB15_43
.LBB15_28:
.Ltmp548:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_33:
.Ltmp514:
	movq	%rax, %r14
	jmp	.LBB15_39
.LBB15_18:
.Ltmp508:
	movq	%rax, %r14
	jmp	.LBB15_21
.LBB15_19:
.Ltmp503:
	movq	%rax, %r14
	jmp	.LBB15_20
.LBB15_17:
.Ltmp498:
	movq	%rax, %r14
	leaq	328(%r12), %rdi
.Ltmp499:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp500:
.LBB15_20:
	leaq	296(%r12), %rdi
.Ltmp504:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp505:
.LBB15_21:
.Ltmp509:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp510:
	jmp	.LBB15_39
.LBB15_22:
.Ltmp511:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_34:
.Ltmp477:
	movq	%rax, %r14
.LBB15_35:
	leaq	264(%r12), %r15
	leaq	360(%r12), %rdi
.Ltmp478:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp479:
# BB#36:
	leaq	328(%r12), %rdi
.Ltmp483:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp484:
# BB#37:
	leaq	296(%r12), %rdi
.Ltmp488:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp489:
# BB#38:
.Ltmp494:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp495:
.LBB15_39:                              # %_ZN11NCoderMixer9CBindInfoD2Ev.exit22
	leaq	136(%r12), %r15
	leaq	232(%r12), %rdi
.Ltmp515:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp516:
# BB#40:
	leaq	200(%r12), %rdi
.Ltmp520:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp521:
# BB#41:
	leaq	168(%r12), %rdi
.Ltmp525:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp526:
# BB#42:
.Ltmp531:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp532:
.LBB15_43:                              # %_ZN11NCoderMixer9CBindInfoD2Ev.exit29
	leaq	48(%r12), %rdi
.Ltmp552:
	callq	_ZN8NArchive3N7z22CCompressionMethodModeD2Ev
.Ltmp553:
.LBB15_59:
	leaq	16(%r12), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, 16(%r12)
.Ltmp557:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp558:
# BB#60:
.Ltmp563:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp564:
.LBB15_61:                              # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit32
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB15_63
# BB#62:
	movq	(%rdi), %rax
.Ltmp574:
	callq	*16(%rax)
.Ltmp575:
.LBB15_63:                              # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit33
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_45:
.Ltmp490:
	movq	%rax, %r14
	jmp	.LBB15_48
.LBB15_46:
.Ltmp485:
	movq	%rax, %r14
	jmp	.LBB15_47
.LBB15_44:
.Ltmp480:
	movq	%rax, %r14
	leaq	328(%r12), %rdi
.Ltmp481:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp482:
.LBB15_47:
	addq	$296, %r12              # imm = 0x128
.Ltmp486:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp487:
.LBB15_48:
.Ltmp491:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp492:
	jmp	.LBB15_67
.LBB15_49:
.Ltmp493:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_52:
.Ltmp527:
	movq	%rax, %r14
	jmp	.LBB15_55
.LBB15_53:
.Ltmp522:
	movq	%rax, %r14
	jmp	.LBB15_54
.LBB15_51:
.Ltmp517:
	movq	%rax, %r14
	leaq	200(%r12), %rdi
.Ltmp518:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp519:
.LBB15_54:
	addq	$168, %r12
.Ltmp523:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp524:
.LBB15_55:
.Ltmp528:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp529:
	jmp	.LBB15_67
.LBB15_56:
.Ltmp530:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_64:
.Ltmp559:
	movq	%rax, %r14
.Ltmp560:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp561:
	jmp	.LBB15_67
.LBB15_65:
.Ltmp562:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_66:
.Ltmp576:
	movq	%rax, %r14
.LBB15_67:                              # %.body20
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN8NArchive3N7z8CEncoderD2Ev, .Lfunc_end15-_ZN8NArchive3N7z8CEncoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\251\203\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\240\003"              # Call site table length
	.long	.Ltmp470-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp471-.Ltmp470       #   Call between .Ltmp470 and .Ltmp471
	.long	.Ltmp472-.Lfunc_begin12 #     jumps to .Ltmp472
	.byte	0                       #   On action: cleanup
	.long	.Ltmp475-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp476-.Ltmp475       #   Call between .Ltmp475 and .Ltmp476
	.long	.Ltmp477-.Lfunc_begin12 #     jumps to .Ltmp477
	.byte	0                       #   On action: cleanup
	.long	.Ltmp496-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp497-.Ltmp496       #   Call between .Ltmp496 and .Ltmp497
	.long	.Ltmp498-.Lfunc_begin12 #     jumps to .Ltmp498
	.byte	0                       #   On action: cleanup
	.long	.Ltmp501-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp502-.Ltmp501       #   Call between .Ltmp501 and .Ltmp502
	.long	.Ltmp503-.Lfunc_begin12 #     jumps to .Ltmp503
	.byte	0                       #   On action: cleanup
	.long	.Ltmp506-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp507-.Ltmp506       #   Call between .Ltmp506 and .Ltmp507
	.long	.Ltmp508-.Lfunc_begin12 #     jumps to .Ltmp508
	.byte	0                       #   On action: cleanup
	.long	.Ltmp512-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp513-.Ltmp512       #   Call between .Ltmp512 and .Ltmp513
	.long	.Ltmp514-.Lfunc_begin12 #     jumps to .Ltmp514
	.byte	0                       #   On action: cleanup
	.long	.Ltmp533-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp534-.Ltmp533       #   Call between .Ltmp533 and .Ltmp534
	.long	.Ltmp535-.Lfunc_begin12 #     jumps to .Ltmp535
	.byte	0                       #   On action: cleanup
	.long	.Ltmp538-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp539-.Ltmp538       #   Call between .Ltmp538 and .Ltmp539
	.long	.Ltmp540-.Lfunc_begin12 #     jumps to .Ltmp540
	.byte	0                       #   On action: cleanup
	.long	.Ltmp543-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp544-.Ltmp543       #   Call between .Ltmp543 and .Ltmp544
	.long	.Ltmp545-.Lfunc_begin12 #     jumps to .Ltmp545
	.byte	0                       #   On action: cleanup
	.long	.Ltmp549-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp550-.Ltmp549       #   Call between .Ltmp549 and .Ltmp550
	.long	.Ltmp551-.Lfunc_begin12 #     jumps to .Ltmp551
	.byte	0                       #   On action: cleanup
	.long	.Ltmp554-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Ltmp555-.Ltmp554       #   Call between .Ltmp554 and .Ltmp555
	.long	.Ltmp556-.Lfunc_begin12 #     jumps to .Ltmp556
	.byte	0                       #   On action: cleanup
	.long	.Ltmp565-.Lfunc_begin12 # >> Call Site 12 <<
	.long	.Ltmp566-.Ltmp565       #   Call between .Ltmp565 and .Ltmp566
	.long	.Ltmp567-.Lfunc_begin12 #     jumps to .Ltmp567
	.byte	0                       #   On action: cleanup
	.long	.Ltmp571-.Lfunc_begin12 # >> Call Site 13 <<
	.long	.Ltmp572-.Ltmp571       #   Call between .Ltmp571 and .Ltmp572
	.long	.Ltmp573-.Lfunc_begin12 #     jumps to .Ltmp573
	.byte	0                       #   On action: cleanup
	.long	.Ltmp572-.Lfunc_begin12 # >> Call Site 14 <<
	.long	.Ltmp473-.Ltmp572       #   Call between .Ltmp572 and .Ltmp473
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp473-.Lfunc_begin12 # >> Call Site 15 <<
	.long	.Ltmp474-.Ltmp473       #   Call between .Ltmp473 and .Ltmp474
	.long	.Ltmp576-.Lfunc_begin12 #     jumps to .Ltmp576
	.byte	1                       #   On action: 1
	.long	.Ltmp568-.Lfunc_begin12 # >> Call Site 16 <<
	.long	.Ltmp569-.Ltmp568       #   Call between .Ltmp568 and .Ltmp569
	.long	.Ltmp570-.Lfunc_begin12 #     jumps to .Ltmp570
	.byte	1                       #   On action: 1
	.long	.Ltmp536-.Lfunc_begin12 # >> Call Site 17 <<
	.long	.Ltmp547-.Ltmp536       #   Call between .Ltmp536 and .Ltmp547
	.long	.Ltmp548-.Lfunc_begin12 #     jumps to .Ltmp548
	.byte	1                       #   On action: 1
	.long	.Ltmp499-.Lfunc_begin12 # >> Call Site 18 <<
	.long	.Ltmp510-.Ltmp499       #   Call between .Ltmp499 and .Ltmp510
	.long	.Ltmp511-.Lfunc_begin12 #     jumps to .Ltmp511
	.byte	1                       #   On action: 1
	.long	.Ltmp478-.Lfunc_begin12 # >> Call Site 19 <<
	.long	.Ltmp479-.Ltmp478       #   Call between .Ltmp478 and .Ltmp479
	.long	.Ltmp480-.Lfunc_begin12 #     jumps to .Ltmp480
	.byte	1                       #   On action: 1
	.long	.Ltmp483-.Lfunc_begin12 # >> Call Site 20 <<
	.long	.Ltmp484-.Ltmp483       #   Call between .Ltmp483 and .Ltmp484
	.long	.Ltmp485-.Lfunc_begin12 #     jumps to .Ltmp485
	.byte	1                       #   On action: 1
	.long	.Ltmp488-.Lfunc_begin12 # >> Call Site 21 <<
	.long	.Ltmp489-.Ltmp488       #   Call between .Ltmp488 and .Ltmp489
	.long	.Ltmp490-.Lfunc_begin12 #     jumps to .Ltmp490
	.byte	1                       #   On action: 1
	.long	.Ltmp494-.Lfunc_begin12 # >> Call Site 22 <<
	.long	.Ltmp495-.Ltmp494       #   Call between .Ltmp494 and .Ltmp495
	.long	.Ltmp576-.Lfunc_begin12 #     jumps to .Ltmp576
	.byte	1                       #   On action: 1
	.long	.Ltmp515-.Lfunc_begin12 # >> Call Site 23 <<
	.long	.Ltmp516-.Ltmp515       #   Call between .Ltmp515 and .Ltmp516
	.long	.Ltmp517-.Lfunc_begin12 #     jumps to .Ltmp517
	.byte	1                       #   On action: 1
	.long	.Ltmp520-.Lfunc_begin12 # >> Call Site 24 <<
	.long	.Ltmp521-.Ltmp520       #   Call between .Ltmp520 and .Ltmp521
	.long	.Ltmp522-.Lfunc_begin12 #     jumps to .Ltmp522
	.byte	1                       #   On action: 1
	.long	.Ltmp525-.Lfunc_begin12 # >> Call Site 25 <<
	.long	.Ltmp526-.Ltmp525       #   Call between .Ltmp525 and .Ltmp526
	.long	.Ltmp527-.Lfunc_begin12 #     jumps to .Ltmp527
	.byte	1                       #   On action: 1
	.long	.Ltmp531-.Lfunc_begin12 # >> Call Site 26 <<
	.long	.Ltmp553-.Ltmp531       #   Call between .Ltmp531 and .Ltmp553
	.long	.Ltmp576-.Lfunc_begin12 #     jumps to .Ltmp576
	.byte	1                       #   On action: 1
	.long	.Ltmp557-.Lfunc_begin12 # >> Call Site 27 <<
	.long	.Ltmp558-.Ltmp557       #   Call between .Ltmp557 and .Ltmp558
	.long	.Ltmp559-.Lfunc_begin12 #     jumps to .Ltmp559
	.byte	1                       #   On action: 1
	.long	.Ltmp563-.Lfunc_begin12 # >> Call Site 28 <<
	.long	.Ltmp575-.Ltmp563       #   Call between .Ltmp563 and .Ltmp575
	.long	.Ltmp576-.Lfunc_begin12 #     jumps to .Ltmp576
	.byte	1                       #   On action: 1
	.long	.Ltmp575-.Lfunc_begin12 # >> Call Site 29 <<
	.long	.Ltmp481-.Ltmp575       #   Call between .Ltmp575 and .Ltmp481
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp481-.Lfunc_begin12 # >> Call Site 30 <<
	.long	.Ltmp492-.Ltmp481       #   Call between .Ltmp481 and .Ltmp492
	.long	.Ltmp493-.Lfunc_begin12 #     jumps to .Ltmp493
	.byte	1                       #   On action: 1
	.long	.Ltmp518-.Lfunc_begin12 # >> Call Site 31 <<
	.long	.Ltmp529-.Ltmp518       #   Call between .Ltmp518 and .Ltmp529
	.long	.Ltmp530-.Lfunc_begin12 #     jumps to .Ltmp530
	.byte	1                       #   On action: 1
	.long	.Ltmp560-.Lfunc_begin12 # >> Call Site 32 <<
	.long	.Ltmp561-.Ltmp560       #   Call between .Ltmp560 and .Ltmp561
	.long	.Ltmp562-.Lfunc_begin12 #     jumps to .Ltmp562
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11NCoderMixer21CBindReverseConverterD2Ev,"axG",@progbits,_ZN11NCoderMixer21CBindReverseConverterD2Ev,comdat
	.weak	_ZN11NCoderMixer21CBindReverseConverterD2Ev
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer21CBindReverseConverterD2Ev,@function
_ZN11NCoderMixer21CBindReverseConverterD2Ev: # @_ZN11NCoderMixer21CBindReverseConverterD2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r15
.Lcfi234:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi235:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi236:
	.cfi_def_cfa_offset 32
.Lcfi237:
	.cfi_offset %rbx, -32
.Lcfi238:
	.cfi_offset %r14, -24
.Lcfi239:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	240(%rbx), %rdi
.Ltmp577:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp578:
# BB#1:
	leaq	200(%rbx), %rdi
.Ltmp582:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp583:
# BB#2:
	leaq	168(%rbx), %rdi
.Ltmp587:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp588:
# BB#3:
	leaq	136(%rbx), %rdi
.Ltmp592:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp593:
# BB#4:
	leaq	8(%rbx), %r15
	leaq	104(%rbx), %rdi
.Ltmp614:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp615:
# BB#5:
	leaq	72(%rbx), %rdi
.Ltmp619:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp620:
# BB#6:
	addq	$40, %rbx
.Ltmp624:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp625:
# BB#7:                                 # %_ZN11NCoderMixer9CBindInfoD2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB16_9:
.Ltmp626:
	movq	%rax, %r14
	jmp	.LBB16_12
.LBB16_10:
.Ltmp621:
	movq	%rax, %r14
	jmp	.LBB16_11
.LBB16_8:
.Ltmp616:
	movq	%rax, %r14
	leaq	72(%rbx), %rdi
.Ltmp617:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp618:
.LBB16_11:
	addq	$40, %rbx
.Ltmp622:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp623:
.LBB16_12:
.Ltmp627:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp628:
	jmp	.LBB16_13
.LBB16_14:
.Ltmp629:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB16_18:
.Ltmp594:
	movq	%rax, %r14
	jmp	.LBB16_21
.LBB16_19:
.Ltmp589:
	movq	%rax, %r14
	jmp	.LBB16_20
.LBB16_16:
.Ltmp584:
	movq	%rax, %r14
	jmp	.LBB16_17
.LBB16_15:
.Ltmp579:
	movq	%rax, %r14
	leaq	200(%rbx), %rdi
.Ltmp580:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp581:
.LBB16_17:
	leaq	168(%rbx), %rdi
.Ltmp585:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp586:
.LBB16_20:
	leaq	136(%rbx), %rdi
.Ltmp590:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp591:
.LBB16_21:
	leaq	8(%rbx), %r15
	leaq	104(%rbx), %rdi
.Ltmp595:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp596:
# BB#22:
	leaq	72(%rbx), %rdi
.Ltmp600:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp601:
# BB#23:
	addq	$40, %rbx
.Ltmp605:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp606:
# BB#24:
.Ltmp611:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp612:
.LBB16_13:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_26:
.Ltmp607:
	movq	%rax, %r14
	jmp	.LBB16_29
.LBB16_27:
.Ltmp602:
	movq	%rax, %r14
	jmp	.LBB16_28
.LBB16_25:
.Ltmp597:
	movq	%rax, %r14
	leaq	72(%rbx), %rdi
.Ltmp598:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp599:
.LBB16_28:
	addq	$40, %rbx
.Ltmp603:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp604:
.LBB16_29:
.Ltmp608:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp609:
	jmp	.LBB16_32
.LBB16_30:
.Ltmp610:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB16_31:
.Ltmp613:
	movq	%rax, %r14
.LBB16_32:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN11NCoderMixer21CBindReverseConverterD2Ev, .Lfunc_end16-_ZN11NCoderMixer21CBindReverseConverterD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\331\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Ltmp577-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp578-.Ltmp577       #   Call between .Ltmp577 and .Ltmp578
	.long	.Ltmp579-.Lfunc_begin13 #     jumps to .Ltmp579
	.byte	0                       #   On action: cleanup
	.long	.Ltmp582-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp583-.Ltmp582       #   Call between .Ltmp582 and .Ltmp583
	.long	.Ltmp584-.Lfunc_begin13 #     jumps to .Ltmp584
	.byte	0                       #   On action: cleanup
	.long	.Ltmp587-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp588-.Ltmp587       #   Call between .Ltmp587 and .Ltmp588
	.long	.Ltmp589-.Lfunc_begin13 #     jumps to .Ltmp589
	.byte	0                       #   On action: cleanup
	.long	.Ltmp592-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp593-.Ltmp592       #   Call between .Ltmp592 and .Ltmp593
	.long	.Ltmp594-.Lfunc_begin13 #     jumps to .Ltmp594
	.byte	0                       #   On action: cleanup
	.long	.Ltmp614-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Ltmp615-.Ltmp614       #   Call between .Ltmp614 and .Ltmp615
	.long	.Ltmp616-.Lfunc_begin13 #     jumps to .Ltmp616
	.byte	0                       #   On action: cleanup
	.long	.Ltmp619-.Lfunc_begin13 # >> Call Site 6 <<
	.long	.Ltmp620-.Ltmp619       #   Call between .Ltmp619 and .Ltmp620
	.long	.Ltmp621-.Lfunc_begin13 #     jumps to .Ltmp621
	.byte	0                       #   On action: cleanup
	.long	.Ltmp624-.Lfunc_begin13 # >> Call Site 7 <<
	.long	.Ltmp625-.Ltmp624       #   Call between .Ltmp624 and .Ltmp625
	.long	.Ltmp626-.Lfunc_begin13 #     jumps to .Ltmp626
	.byte	0                       #   On action: cleanup
	.long	.Ltmp625-.Lfunc_begin13 # >> Call Site 8 <<
	.long	.Ltmp617-.Ltmp625       #   Call between .Ltmp625 and .Ltmp617
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp617-.Lfunc_begin13 # >> Call Site 9 <<
	.long	.Ltmp628-.Ltmp617       #   Call between .Ltmp617 and .Ltmp628
	.long	.Ltmp629-.Lfunc_begin13 #     jumps to .Ltmp629
	.byte	1                       #   On action: 1
	.long	.Ltmp580-.Lfunc_begin13 # >> Call Site 10 <<
	.long	.Ltmp591-.Ltmp580       #   Call between .Ltmp580 and .Ltmp591
	.long	.Ltmp613-.Lfunc_begin13 #     jumps to .Ltmp613
	.byte	1                       #   On action: 1
	.long	.Ltmp595-.Lfunc_begin13 # >> Call Site 11 <<
	.long	.Ltmp596-.Ltmp595       #   Call between .Ltmp595 and .Ltmp596
	.long	.Ltmp597-.Lfunc_begin13 #     jumps to .Ltmp597
	.byte	1                       #   On action: 1
	.long	.Ltmp600-.Lfunc_begin13 # >> Call Site 12 <<
	.long	.Ltmp601-.Ltmp600       #   Call between .Ltmp600 and .Ltmp601
	.long	.Ltmp602-.Lfunc_begin13 #     jumps to .Ltmp602
	.byte	1                       #   On action: 1
	.long	.Ltmp605-.Lfunc_begin13 # >> Call Site 13 <<
	.long	.Ltmp606-.Ltmp605       #   Call between .Ltmp605 and .Ltmp606
	.long	.Ltmp607-.Lfunc_begin13 #     jumps to .Ltmp607
	.byte	1                       #   On action: 1
	.long	.Ltmp611-.Lfunc_begin13 # >> Call Site 14 <<
	.long	.Ltmp612-.Ltmp611       #   Call between .Ltmp611 and .Ltmp612
	.long	.Ltmp613-.Lfunc_begin13 #     jumps to .Ltmp613
	.byte	1                       #   On action: 1
	.long	.Ltmp612-.Lfunc_begin13 # >> Call Site 15 <<
	.long	.Ltmp598-.Ltmp612       #   Call between .Ltmp612 and .Ltmp598
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp598-.Lfunc_begin13 # >> Call Site 16 <<
	.long	.Ltmp609-.Ltmp598       #   Call between .Ltmp598 and .Ltmp609
	.long	.Ltmp610-.Lfunc_begin13 #     jumps to .Ltmp610
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI13CStreamBinderED2Ev,"axG",@progbits,_ZN13CObjectVectorI13CStreamBinderED2Ev,comdat
	.weak	_ZN13CObjectVectorI13CStreamBinderED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI13CStreamBinderED2Ev,@function
_ZN13CObjectVectorI13CStreamBinderED2Ev: # @_ZN13CObjectVectorI13CStreamBinderED2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi240:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi241:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi242:
	.cfi_def_cfa_offset 32
.Lcfi243:
	.cfi_offset %rbx, -24
.Lcfi244:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, (%rbx)
.Ltmp630:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp631:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB17_2:
.Ltmp632:
	movq	%rax, %r14
.Ltmp633:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp634:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB17_4:
.Ltmp635:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN13CObjectVectorI13CStreamBinderED2Ev, .Lfunc_end17-_ZN13CObjectVectorI13CStreamBinderED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp630-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp631-.Ltmp630       #   Call between .Ltmp630 and .Ltmp631
	.long	.Ltmp632-.Lfunc_begin14 #     jumps to .Ltmp632
	.byte	0                       #   On action: cleanup
	.long	.Ltmp631-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp633-.Ltmp631       #   Call between .Ltmp631 and .Ltmp633
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp633-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp634-.Ltmp633       #   Call between .Ltmp633 and .Ltmp634
	.long	.Ltmp635-.Lfunc_begin14 #     jumps to .Ltmp635
	.byte	1                       #   On action: 1
	.long	.Ltmp634-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end17-.Ltmp634   #   Call between .Ltmp634 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI13CStreamBinderED0Ev,"axG",@progbits,_ZN13CObjectVectorI13CStreamBinderED0Ev,comdat
	.weak	_ZN13CObjectVectorI13CStreamBinderED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI13CStreamBinderED0Ev,@function
_ZN13CObjectVectorI13CStreamBinderED0Ev: # @_ZN13CObjectVectorI13CStreamBinderED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi245:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi246:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi247:
	.cfi_def_cfa_offset 32
.Lcfi248:
	.cfi_offset %rbx, -24
.Lcfi249:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI13CStreamBinderE+16, (%rbx)
.Ltmp636:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp637:
# BB#1:
.Ltmp642:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp643:
# BB#2:                                 # %_ZN13CObjectVectorI13CStreamBinderED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB18_5:
.Ltmp644:
	movq	%rax, %r14
	jmp	.LBB18_6
.LBB18_3:
.Ltmp638:
	movq	%rax, %r14
.Ltmp639:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp640:
.LBB18_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_4:
.Ltmp641:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN13CObjectVectorI13CStreamBinderED0Ev, .Lfunc_end18-_ZN13CObjectVectorI13CStreamBinderED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp636-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp637-.Ltmp636       #   Call between .Ltmp636 and .Ltmp637
	.long	.Ltmp638-.Lfunc_begin15 #     jumps to .Ltmp638
	.byte	0                       #   On action: cleanup
	.long	.Ltmp642-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp643-.Ltmp642       #   Call between .Ltmp642 and .Ltmp643
	.long	.Ltmp644-.Lfunc_begin15 #     jumps to .Ltmp644
	.byte	0                       #   On action: cleanup
	.long	.Ltmp639-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp640-.Ltmp639       #   Call between .Ltmp639 and .Ltmp640
	.long	.Ltmp641-.Lfunc_begin15 #     jumps to .Ltmp641
	.byte	1                       #   On action: 1
	.long	.Ltmp640-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end18-.Ltmp640   #   Call between .Ltmp640 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI13CStreamBinderE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI13CStreamBinderE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii,@function
_ZN13CObjectVectorI13CStreamBinderE6DeleteEii: # @_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi250:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi251:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi252:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi253:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi254:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi256:
	.cfi_def_cfa_offset 80
.Lcfi257:
	.cfi_offset %rbx, -56
.Lcfi258:
	.cfi_offset %r12, -48
.Lcfi259:
	.cfi_offset %r13, -40
.Lcfi260:
	.cfi_offset %r14, -32
.Lcfi261:
	.cfi_offset %r15, -24
.Lcfi262:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%esi, %edi
	cmpl	%ecx, %eax
	cmovlel	%edx, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB19_10
# BB#1:                                 # %.lr.ph
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	movslq	12(%rsp), %r14          # 4-byte Folded Reload
	shlq	$3, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%r13, %rax
	movq	(%rax,%r15,8), %rbp
	testq	%rbp, %rbp
	je	.LBB19_9
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	152(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB19_7
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	cmpb	$0, 88(%rbx)
	je	.LBB19_6
# BB#5:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rdi
	callq	pthread_mutex_destroy
	leaq	40(%rbx), %rdi
	callq	pthread_cond_destroy
.LBB19_6:                               #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB19_7:                               #   in Loop: Header=BB19_2 Depth=1
	movq	$0, 152(%rbp)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 128(%rbp)
	movq	$0, 136(%rbp)
	leaq	24(%rbp), %rdi
.Ltmp645:
	callq	Event_Close
.Ltmp646:
# BB#8:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB19_9:                               #   in Loop: Header=BB19_2 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jl	.LBB19_2
.LBB19_10:                              # %._crit_edge
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	12(%rsp), %edx          # 4-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB19_11:                              # %.body
.Ltmp647:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii, .Lfunc_end19-_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp645-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp646-.Ltmp645       #   Call between .Ltmp645 and .Ltmp646
	.long	.Ltmp647-.Lfunc_begin16 #     jumps to .Ltmp647
	.byte	0                       #   On action: cleanup
	.long	.Ltmp646-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Lfunc_end19-.Ltmp646   #   Call between .Ltmp646 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,"axG",@progbits,_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,comdat
	.weak	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,@function
_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv: # @_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_startproc
# BB#0:
	cmpb	$0, 17(%rdi)
	je	.LBB20_1
# BB#2:
	movb	$1, %al
	cmpb	$0, 16(%rdi)
	jne	.LBB20_4
# BB#3:
	movb	$0, 17(%rdi)
.LBB20_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB20_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end20:
	.size	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv, .Lfunc_end20-_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev,"axG",@progbits,_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev,comdat
	.weak	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev,@function
_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev: # @_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi263:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi264:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi265:
	.cfi_def_cfa_offset 32
.Lcfi266:
	.cfi_offset %rbx, -24
.Lcfi267:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE+16, (%rbx)
.Ltmp648:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp649:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB21_2:
.Ltmp650:
	movq	%rax, %r14
.Ltmp651:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp652:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_4:
.Ltmp653:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev, .Lfunc_end21-_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp648-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp649-.Ltmp648       #   Call between .Ltmp648 and .Ltmp649
	.long	.Ltmp650-.Lfunc_begin17 #     jumps to .Ltmp650
	.byte	0                       #   On action: cleanup
	.long	.Ltmp649-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp651-.Ltmp649       #   Call between .Ltmp649 and .Ltmp651
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp651-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp652-.Ltmp651       #   Call between .Ltmp651 and .Ltmp652
	.long	.Ltmp653-.Lfunc_begin17 #     jumps to .Ltmp653
	.byte	1                       #   On action: 1
	.long	.Ltmp652-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end21-.Ltmp652   #   Call between .Ltmp652 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev,"axG",@progbits,_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev,comdat
	.weak	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev,@function
_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev: # @_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi268:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi269:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi270:
	.cfi_def_cfa_offset 32
.Lcfi271:
	.cfi_offset %rbx, -24
.Lcfi272:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE+16, (%rbx)
.Ltmp654:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp655:
# BB#1:
.Ltmp660:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp661:
# BB#2:                                 # %_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_5:
.Ltmp662:
	movq	%rax, %r14
	jmp	.LBB22_6
.LBB22_3:
.Ltmp656:
	movq	%rax, %r14
.Ltmp657:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp658:
.LBB22_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_4:
.Ltmp659:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev, .Lfunc_end22-_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp654-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp655-.Ltmp654       #   Call between .Ltmp654 and .Ltmp655
	.long	.Ltmp656-.Lfunc_begin18 #     jumps to .Ltmp656
	.byte	0                       #   On action: cleanup
	.long	.Ltmp660-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp661-.Ltmp660       #   Call between .Ltmp660 and .Ltmp661
	.long	.Ltmp662-.Lfunc_begin18 #     jumps to .Ltmp662
	.byte	0                       #   On action: cleanup
	.long	.Ltmp657-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp658-.Ltmp657       #   Call between .Ltmp657 and .Ltmp658
	.long	.Ltmp659-.Lfunc_begin18 #     jumps to .Ltmp659
	.byte	1                       #   On action: 1
	.long	.Ltmp658-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end22-.Ltmp658   #   Call between .Ltmp658 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii,@function
_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii: # @_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi273:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi274:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi275:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi276:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi277:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi278:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi279:
	.cfi_def_cfa_offset 64
.Lcfi280:
	.cfi_offset %rbx, -56
.Lcfi281:
	.cfi_offset %r12, -48
.Lcfi282:
	.cfi_offset %r13, -40
.Lcfi283:
	.cfi_offset %r14, -32
.Lcfi284:
	.cfi_offset %r15, -24
.Lcfi285:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB23_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB23_5
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=1
.Ltmp663:
	movq	%rbp, %rdi
	callq	_ZN11NCoderMixer7CCoder2D2Ev
.Ltmp664:
# BB#4:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB23_5:                               #   in Loop: Header=BB23_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB23_2
.LBB23_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB23_7:
.Ltmp665:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii, .Lfunc_end23-_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp663-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp664-.Ltmp663       #   Call between .Ltmp663 and .Ltmp664
	.long	.Ltmp665-.Lfunc_begin19 #     jumps to .Ltmp665
	.byte	0                       #   On action: cleanup
	.long	.Ltmp664-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end23-.Ltmp664   #   Call between .Ltmp664 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11NCoderMixer7CCoder2D2Ev,"axG",@progbits,_ZN11NCoderMixer7CCoder2D2Ev,comdat
	.weak	_ZN11NCoderMixer7CCoder2D2Ev
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer7CCoder2D2Ev,@function
_ZN11NCoderMixer7CCoder2D2Ev:           # @_ZN11NCoderMixer7CCoder2D2Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r15
.Lcfi286:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi287:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi288:
	.cfi_def_cfa_offset 32
.Lcfi289:
	.cfi_offset %rbx, -32
.Lcfi290:
	.cfi_offset %r14, -24
.Lcfi291:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	$_ZTVN11NCoderMixer7CCoder2E+16, (%r15)
	leaq	496(%r15), %rdi
.Ltmp666:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp667:
# BB#1:
	leaq	464(%r15), %rdi
.Ltmp671:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp672:
# BB#2:
	leaq	432(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 432(%r15)
.Ltmp682:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp683:
# BB#3:
.Ltmp688:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp689:
# BB#4:                                 # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit
	leaq	400(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 400(%r15)
.Ltmp699:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp700:
# BB#5:
.Ltmp705:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp706:
# BB#6:                                 # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit
.Ltmp710:
	movq	%r15, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp711:
# BB#7:
	addq	$240, %r15
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN11NCoderMixer11CCoderInfo2D2Ev # TAILCALL
.LBB24_25:
.Ltmp712:
	movq	%rax, %r14
	jmp	.LBB24_26
.LBB24_22:
.Ltmp707:
	movq	%rax, %r14
	jmp	.LBB24_19
.LBB24_10:
.Ltmp701:
	movq	%rax, %r14
.Ltmp702:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp703:
	jmp	.LBB24_19
.LBB24_11:
.Ltmp704:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_13:
.Ltmp690:
	movq	%rax, %r14
	jmp	.LBB24_17
.LBB24_8:
.Ltmp684:
	movq	%rax, %r14
.Ltmp685:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp686:
	jmp	.LBB24_17
.LBB24_9:
.Ltmp687:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_14:
.Ltmp673:
	movq	%rax, %r14
	jmp	.LBB24_15
.LBB24_12:
.Ltmp668:
	movq	%rax, %r14
	leaq	464(%r15), %rdi
.Ltmp669:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp670:
.LBB24_15:
	leaq	432(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, 432(%r15)
.Ltmp674:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp675:
# BB#16:
.Ltmp680:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp681:
.LBB24_17:                              # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit10
	leaq	400(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, 400(%r15)
.Ltmp691:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp692:
# BB#18:
.Ltmp697:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp698:
.LBB24_19:                              # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit13
.Ltmp708:
	movq	%r15, %rdi
	callq	_ZN11CVirtThreadD2Ev
.Ltmp709:
.LBB24_26:
	addq	$240, %r15
.Ltmp713:
	movq	%r15, %rdi
	callq	_ZN11NCoderMixer11CCoderInfo2D2Ev
.Ltmp714:
# BB#27:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB24_20:
.Ltmp676:
	movq	%rax, %r14
.Ltmp677:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp678:
	jmp	.LBB24_29
.LBB24_21:
.Ltmp679:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_23:
.Ltmp693:
	movq	%rax, %r14
.Ltmp694:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp695:
	jmp	.LBB24_29
.LBB24_24:
.Ltmp696:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_28:
.Ltmp715:
	movq	%rax, %r14
.LBB24_29:                              # %.body8
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end24:
	.size	_ZN11NCoderMixer7CCoder2D2Ev, .Lfunc_end24-_ZN11NCoderMixer7CCoder2D2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp666-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp667-.Ltmp666       #   Call between .Ltmp666 and .Ltmp667
	.long	.Ltmp668-.Lfunc_begin20 #     jumps to .Ltmp668
	.byte	0                       #   On action: cleanup
	.long	.Ltmp671-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp672-.Ltmp671       #   Call between .Ltmp671 and .Ltmp672
	.long	.Ltmp673-.Lfunc_begin20 #     jumps to .Ltmp673
	.byte	0                       #   On action: cleanup
	.long	.Ltmp682-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Ltmp683-.Ltmp682       #   Call between .Ltmp682 and .Ltmp683
	.long	.Ltmp684-.Lfunc_begin20 #     jumps to .Ltmp684
	.byte	0                       #   On action: cleanup
	.long	.Ltmp688-.Lfunc_begin20 # >> Call Site 4 <<
	.long	.Ltmp689-.Ltmp688       #   Call between .Ltmp688 and .Ltmp689
	.long	.Ltmp690-.Lfunc_begin20 #     jumps to .Ltmp690
	.byte	0                       #   On action: cleanup
	.long	.Ltmp699-.Lfunc_begin20 # >> Call Site 5 <<
	.long	.Ltmp700-.Ltmp699       #   Call between .Ltmp699 and .Ltmp700
	.long	.Ltmp701-.Lfunc_begin20 #     jumps to .Ltmp701
	.byte	0                       #   On action: cleanup
	.long	.Ltmp705-.Lfunc_begin20 # >> Call Site 6 <<
	.long	.Ltmp706-.Ltmp705       #   Call between .Ltmp705 and .Ltmp706
	.long	.Ltmp707-.Lfunc_begin20 #     jumps to .Ltmp707
	.byte	0                       #   On action: cleanup
	.long	.Ltmp710-.Lfunc_begin20 # >> Call Site 7 <<
	.long	.Ltmp711-.Ltmp710       #   Call between .Ltmp710 and .Ltmp711
	.long	.Ltmp712-.Lfunc_begin20 #     jumps to .Ltmp712
	.byte	0                       #   On action: cleanup
	.long	.Ltmp711-.Lfunc_begin20 # >> Call Site 8 <<
	.long	.Ltmp702-.Ltmp711       #   Call between .Ltmp711 and .Ltmp702
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp702-.Lfunc_begin20 # >> Call Site 9 <<
	.long	.Ltmp703-.Ltmp702       #   Call between .Ltmp702 and .Ltmp703
	.long	.Ltmp704-.Lfunc_begin20 #     jumps to .Ltmp704
	.byte	1                       #   On action: 1
	.long	.Ltmp685-.Lfunc_begin20 # >> Call Site 10 <<
	.long	.Ltmp686-.Ltmp685       #   Call between .Ltmp685 and .Ltmp686
	.long	.Ltmp687-.Lfunc_begin20 #     jumps to .Ltmp687
	.byte	1                       #   On action: 1
	.long	.Ltmp669-.Lfunc_begin20 # >> Call Site 11 <<
	.long	.Ltmp670-.Ltmp669       #   Call between .Ltmp669 and .Ltmp670
	.long	.Ltmp715-.Lfunc_begin20 #     jumps to .Ltmp715
	.byte	1                       #   On action: 1
	.long	.Ltmp674-.Lfunc_begin20 # >> Call Site 12 <<
	.long	.Ltmp675-.Ltmp674       #   Call between .Ltmp674 and .Ltmp675
	.long	.Ltmp676-.Lfunc_begin20 #     jumps to .Ltmp676
	.byte	1                       #   On action: 1
	.long	.Ltmp680-.Lfunc_begin20 # >> Call Site 13 <<
	.long	.Ltmp681-.Ltmp680       #   Call between .Ltmp680 and .Ltmp681
	.long	.Ltmp715-.Lfunc_begin20 #     jumps to .Ltmp715
	.byte	1                       #   On action: 1
	.long	.Ltmp691-.Lfunc_begin20 # >> Call Site 14 <<
	.long	.Ltmp692-.Ltmp691       #   Call between .Ltmp691 and .Ltmp692
	.long	.Ltmp693-.Lfunc_begin20 #     jumps to .Ltmp693
	.byte	1                       #   On action: 1
	.long	.Ltmp697-.Lfunc_begin20 # >> Call Site 15 <<
	.long	.Ltmp714-.Ltmp697       #   Call between .Ltmp697 and .Ltmp714
	.long	.Ltmp715-.Lfunc_begin20 #     jumps to .Ltmp715
	.byte	1                       #   On action: 1
	.long	.Ltmp714-.Lfunc_begin20 # >> Call Site 16 <<
	.long	.Ltmp677-.Ltmp714       #   Call between .Ltmp714 and .Ltmp677
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp677-.Lfunc_begin20 # >> Call Site 17 <<
	.long	.Ltmp678-.Ltmp677       #   Call between .Ltmp677 and .Ltmp678
	.long	.Ltmp679-.Lfunc_begin20 #     jumps to .Ltmp679
	.byte	1                       #   On action: 1
	.long	.Ltmp694-.Lfunc_begin20 # >> Call Site 18 <<
	.long	.Ltmp695-.Ltmp694       #   Call between .Ltmp694 and .Ltmp695
	.long	.Ltmp696-.Lfunc_begin20 #     jumps to .Ltmp696
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi293:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi294:
	.cfi_def_cfa_offset 32
.Lcfi295:
	.cfi_offset %rbx, -24
.Lcfi296:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, (%rbx)
.Ltmp716:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp717:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB25_2:
.Ltmp718:
	movq	%rax, %r14
.Ltmp719:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp720:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_4:
.Ltmp721:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev, .Lfunc_end25-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp716-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp717-.Ltmp716       #   Call between .Ltmp716 and .Ltmp717
	.long	.Ltmp718-.Lfunc_begin21 #     jumps to .Ltmp718
	.byte	0                       #   On action: cleanup
	.long	.Ltmp717-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp719-.Ltmp717       #   Call between .Ltmp717 and .Ltmp719
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp719-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Ltmp720-.Ltmp719       #   Call between .Ltmp719 and .Ltmp720
	.long	.Ltmp721-.Lfunc_begin21 #     jumps to .Ltmp721
	.byte	1                       #   On action: 1
	.long	.Ltmp720-.Lfunc_begin21 # >> Call Site 4 <<
	.long	.Lfunc_end25-.Ltmp720   #   Call between .Ltmp720 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN11NCoderMixer11CCoderInfo2D2Ev,"axG",@progbits,_ZN11NCoderMixer11CCoderInfo2D2Ev,comdat
	.weak	_ZN11NCoderMixer11CCoderInfo2D2Ev
	.p2align	4, 0x90
	.type	_ZN11NCoderMixer11CCoderInfo2D2Ev,@function
_ZN11NCoderMixer11CCoderInfo2D2Ev:      # @_ZN11NCoderMixer11CCoderInfo2D2Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi297:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi298:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi299:
	.cfi_def_cfa_offset 32
.Lcfi300:
	.cfi_offset %rbx, -24
.Lcfi301:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	120(%rbx), %rdi
.Ltmp722:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp723:
# BB#1:
	leaq	88(%rbx), %rdi
.Ltmp727:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp728:
# BB#2:
	leaq	56(%rbx), %rdi
.Ltmp732:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp733:
# BB#3:
	leaq	24(%rbx), %rdi
.Ltmp737:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp738:
# BB#4:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp742:
	callq	*16(%rax)
.Ltmp743:
.LBB26_6:                               # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_7
# BB#21:
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*16(%rax)               # TAILCALL
.LBB26_7:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB26_13:
.Ltmp744:
	movq	%rax, %r14
	jmp	.LBB26_17
.LBB26_14:
.Ltmp739:
	movq	%rax, %r14
	jmp	.LBB26_15
.LBB26_11:
.Ltmp734:
	movq	%rax, %r14
	jmp	.LBB26_12
.LBB26_9:
.Ltmp729:
	movq	%rax, %r14
	jmp	.LBB26_10
.LBB26_8:
.Ltmp724:
	movq	%rax, %r14
	leaq	88(%rbx), %rdi
.Ltmp725:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp726:
.LBB26_10:
	leaq	56(%rbx), %rdi
.Ltmp730:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp731:
.LBB26_12:
	leaq	24(%rbx), %rdi
.Ltmp735:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp736:
.LBB26_15:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_17
# BB#16:
	movq	(%rdi), %rax
.Ltmp740:
	callq	*16(%rax)
.Ltmp741:
.LBB26_17:                              # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit7
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_19
# BB#18:
	movq	(%rdi), %rax
.Ltmp745:
	callq	*16(%rax)
.Ltmp746:
.LBB26_19:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit9
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB26_20:
.Ltmp747:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end26:
	.size	_ZN11NCoderMixer11CCoderInfo2D2Ev, .Lfunc_end26-_ZN11NCoderMixer11CCoderInfo2D2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp722-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp723-.Ltmp722       #   Call between .Ltmp722 and .Ltmp723
	.long	.Ltmp724-.Lfunc_begin22 #     jumps to .Ltmp724
	.byte	0                       #   On action: cleanup
	.long	.Ltmp727-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Ltmp728-.Ltmp727       #   Call between .Ltmp727 and .Ltmp728
	.long	.Ltmp729-.Lfunc_begin22 #     jumps to .Ltmp729
	.byte	0                       #   On action: cleanup
	.long	.Ltmp732-.Lfunc_begin22 # >> Call Site 3 <<
	.long	.Ltmp733-.Ltmp732       #   Call between .Ltmp732 and .Ltmp733
	.long	.Ltmp734-.Lfunc_begin22 #     jumps to .Ltmp734
	.byte	0                       #   On action: cleanup
	.long	.Ltmp737-.Lfunc_begin22 # >> Call Site 4 <<
	.long	.Ltmp738-.Ltmp737       #   Call between .Ltmp737 and .Ltmp738
	.long	.Ltmp739-.Lfunc_begin22 #     jumps to .Ltmp739
	.byte	0                       #   On action: cleanup
	.long	.Ltmp742-.Lfunc_begin22 # >> Call Site 5 <<
	.long	.Ltmp743-.Ltmp742       #   Call between .Ltmp742 and .Ltmp743
	.long	.Ltmp744-.Lfunc_begin22 #     jumps to .Ltmp744
	.byte	0                       #   On action: cleanup
	.long	.Ltmp743-.Lfunc_begin22 # >> Call Site 6 <<
	.long	.Ltmp725-.Ltmp743       #   Call between .Ltmp743 and .Ltmp725
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp725-.Lfunc_begin22 # >> Call Site 7 <<
	.long	.Ltmp746-.Ltmp725       #   Call between .Ltmp725 and .Ltmp746
	.long	.Ltmp747-.Lfunc_begin22 #     jumps to .Ltmp747
	.byte	1                       #   On action: 1
	.long	.Ltmp746-.Lfunc_begin22 # >> Call Site 8 <<
	.long	.Lfunc_end26-.Ltmp746   #   Call between .Ltmp746 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r14
.Lcfi302:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi303:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi304:
	.cfi_def_cfa_offset 32
.Lcfi305:
	.cfi_offset %rbx, -24
.Lcfi306:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE+16, (%rbx)
.Ltmp748:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp749:
# BB#1:
.Ltmp754:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp755:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB27_5:
.Ltmp756:
	movq	%rax, %r14
	jmp	.LBB27_6
.LBB27_3:
.Ltmp750:
	movq	%rax, %r14
.Ltmp751:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp752:
.LBB27_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_4:
.Ltmp753:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev, .Lfunc_end27-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp748-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp749-.Ltmp748       #   Call between .Ltmp748 and .Ltmp749
	.long	.Ltmp750-.Lfunc_begin23 #     jumps to .Ltmp750
	.byte	0                       #   On action: cleanup
	.long	.Ltmp754-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp755-.Ltmp754       #   Call between .Ltmp754 and .Ltmp755
	.long	.Ltmp756-.Lfunc_begin23 #     jumps to .Ltmp756
	.byte	0                       #   On action: cleanup
	.long	.Ltmp751-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp752-.Ltmp751       #   Call between .Ltmp751 and .Ltmp752
	.long	.Ltmp753-.Lfunc_begin23 #     jumps to .Ltmp753
	.byte	1                       #   On action: 1
	.long	.Ltmp752-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Lfunc_end27-.Ltmp752   #   Call between .Ltmp752 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi307:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi308:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi309:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi310:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi311:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi312:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi313:
	.cfi_def_cfa_offset 64
.Lcfi314:
	.cfi_offset %rbx, -56
.Lcfi315:
	.cfi_offset %r12, -48
.Lcfi316:
	.cfi_offset %r13, -40
.Lcfi317:
	.cfi_offset %r14, -32
.Lcfi318:
	.cfi_offset %r15, -24
.Lcfi319:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB28_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB28_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB28_6
# BB#3:                                 #   in Loop: Header=BB28_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB28_5
# BB#4:                                 #   in Loop: Header=BB28_2 Depth=1
	movq	(%rdi), %rax
.Ltmp757:
	callq	*16(%rax)
.Ltmp758:
.LBB28_5:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB28_6:                               #   in Loop: Header=BB28_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB28_2
.LBB28_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB28_8:
.Ltmp759:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii, .Lfunc_end28-_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp757-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp758-.Ltmp757       #   Call between .Ltmp757 and .Ltmp758
	.long	.Ltmp759-.Lfunc_begin24 #     jumps to .Ltmp759
	.byte	0                       #   On action: cleanup
	.long	.Ltmp758-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp758   #   Call between .Ltmp758 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r14
.Lcfi320:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi321:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi322:
	.cfi_def_cfa_offset 32
.Lcfi323:
	.cfi_offset %rbx, -24
.Lcfi324:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp760:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp761:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB29_2:
.Ltmp762:
	movq	%rax, %r14
.Ltmp763:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp764:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB29_4:
.Ltmp765:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end29:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev, .Lfunc_end29-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp760-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp761-.Ltmp760       #   Call between .Ltmp760 and .Ltmp761
	.long	.Ltmp762-.Lfunc_begin25 #     jumps to .Ltmp762
	.byte	0                       #   On action: cleanup
	.long	.Ltmp761-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp763-.Ltmp761       #   Call between .Ltmp761 and .Ltmp763
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp763-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp764-.Ltmp763       #   Call between .Ltmp763 and .Ltmp764
	.long	.Ltmp765-.Lfunc_begin25 #     jumps to .Ltmp765
	.byte	1                       #   On action: 1
	.long	.Ltmp764-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Lfunc_end29-.Ltmp764   #   Call between .Ltmp764 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%r14
.Lcfi325:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi326:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi327:
	.cfi_def_cfa_offset 32
.Lcfi328:
	.cfi_offset %rbx, -24
.Lcfi329:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE+16, (%rbx)
.Ltmp766:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp767:
# BB#1:
.Ltmp772:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp773:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB30_5:
.Ltmp774:
	movq	%rax, %r14
	jmp	.LBB30_6
.LBB30_3:
.Ltmp768:
	movq	%rax, %r14
.Ltmp769:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp770:
.LBB30_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB30_4:
.Ltmp771:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev, .Lfunc_end30-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp766-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp767-.Ltmp766       #   Call between .Ltmp766 and .Ltmp767
	.long	.Ltmp768-.Lfunc_begin26 #     jumps to .Ltmp768
	.byte	0                       #   On action: cleanup
	.long	.Ltmp772-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp773-.Ltmp772       #   Call between .Ltmp772 and .Ltmp773
	.long	.Ltmp774-.Lfunc_begin26 #     jumps to .Ltmp774
	.byte	0                       #   On action: cleanup
	.long	.Ltmp769-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp770-.Ltmp769       #   Call between .Ltmp769 and .Ltmp770
	.long	.Ltmp771-.Lfunc_begin26 #     jumps to .Ltmp771
	.byte	1                       #   On action: 1
	.long	.Ltmp770-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Lfunc_end30-.Ltmp770   #   Call between .Ltmp770 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi330:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi331:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi332:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi333:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi334:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi335:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi336:
	.cfi_def_cfa_offset 80
.Lcfi337:
	.cfi_offset %rbx, -56
.Lcfi338:
	.cfi_offset %r12, -48
.Lcfi339:
	.cfi_offset %r13, -40
.Lcfi340:
	.cfi_offset %r14, -32
.Lcfi341:
	.cfi_offset %r15, -24
.Lcfi342:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	subl	%esi, %edi
	cmpl	%ecx, %eax
	cmovlel	%edx, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	jle	.LBB31_7
# BB#1:                                 # %.lr.ph
	movslq	16(%rsp), %r13          # 4-byte Folded Reload
	movslq	12(%rsp), %r14          # 4-byte Folded Reload
	shlq	$3, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB31_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%r13, %rax
	movq	(%rax,%r15,8), %rbp
	testq	%rbp, %rbp
	je	.LBB31_6
# BB#3:                                 #   in Loop: Header=BB31_2 Depth=1
	leaq	8(%rbp), %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, 8(%rbp)
.Ltmp775:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp776:
# BB#4:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit.i
                                        #   in Loop: Header=BB31_2 Depth=1
.Ltmp781:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp782:
# BB#5:                                 # %_ZN7CMethodD2Ev.exit
                                        #   in Loop: Header=BB31_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB31_6:                               #   in Loop: Header=BB31_2 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jl	.LBB31_2
.LBB31_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	12(%rsp), %edx          # 4-byte Reload
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB31_8:
.Ltmp777:
	movq	%rax, %r14
.Ltmp778:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp779:
	jmp	.LBB31_11
.LBB31_9:
.Ltmp780:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB31_10:
.Ltmp783:
	movq	%rax, %r14
.LBB31_11:                              # %.body
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end31:
	.size	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii, .Lfunc_end31-_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp775-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp776-.Ltmp775       #   Call between .Ltmp775 and .Ltmp776
	.long	.Ltmp777-.Lfunc_begin27 #     jumps to .Ltmp777
	.byte	0                       #   On action: cleanup
	.long	.Ltmp781-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Ltmp782-.Ltmp781       #   Call between .Ltmp781 and .Ltmp782
	.long	.Ltmp783-.Lfunc_begin27 #     jumps to .Ltmp783
	.byte	0                       #   On action: cleanup
	.long	.Ltmp782-.Lfunc_begin27 # >> Call Site 3 <<
	.long	.Ltmp778-.Ltmp782       #   Call between .Ltmp782 and .Ltmp778
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp778-.Lfunc_begin27 # >> Call Site 4 <<
	.long	.Ltmp779-.Ltmp778       #   Call between .Ltmp778 and .Ltmp779
	.long	.Ltmp780-.Lfunc_begin27 #     jumps to .Ltmp780
	.byte	1                       #   On action: 1
	.long	.Ltmp779-.Lfunc_begin27 # >> Call Site 5 <<
	.long	.Lfunc_end31-.Ltmp779   #   Call between .Ltmp779 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:
	pushq	%r14
.Lcfi343:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi344:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi345:
	.cfi_def_cfa_offset 32
.Lcfi346:
	.cfi_offset %rbx, -24
.Lcfi347:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp784:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp785:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB32_2:
.Ltmp786:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev, .Lfunc_end32-_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp784-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp785-.Ltmp784       #   Call between .Ltmp784 and .Ltmp785
	.long	.Ltmp786-.Lfunc_begin28 #     jumps to .Ltmp786
	.byte	0                       #   On action: cleanup
	.long	.Ltmp785-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Lfunc_end32-.Ltmp785   #   Call between .Ltmp785 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev,@function
_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev: # @_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:
	pushq	%r14
.Lcfi348:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi349:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi350:
	.cfi_def_cfa_offset 32
.Lcfi351:
	.cfi_offset %rbx, -24
.Lcfi352:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp787:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp788:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB33_2:
.Ltmp789:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end33:
	.size	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev, .Lfunc_end33-_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp787-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp788-.Ltmp787       #   Call between .Ltmp787 and .Ltmp788
	.long	.Ltmp789-.Lfunc_begin29 #     jumps to .Ltmp789
	.byte	0                       #   On action: cleanup
	.long	.Ltmp788-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Lfunc_end33-.Ltmp788   #   Call between .Ltmp788 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev,@function
_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev: # @_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:
	pushq	%r14
.Lcfi353:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi354:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi355:
	.cfi_def_cfa_offset 32
.Lcfi356:
	.cfi_offset %rbx, -24
.Lcfi357:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp790:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp791:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB34_2:
.Ltmp792:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end34:
	.size	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev, .Lfunc_end34-_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp790-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp791-.Ltmp790       #   Call between .Ltmp790 and .Ltmp791
	.long	.Ltmp792-.Lfunc_begin30 #     jumps to .Ltmp792
	.byte	0                       #   On action: cleanup
	.long	.Ltmp791-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Lfunc_end34-.Ltmp791   #   Call between .Ltmp791 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin31:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception31
# BB#0:
	pushq	%r14
.Lcfi358:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi359:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi360:
	.cfi_def_cfa_offset 32
.Lcfi361:
	.cfi_offset %rbx, -24
.Lcfi362:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp793:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp794:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB35_2:
.Ltmp795:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end35:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end35-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception31:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp793-.Lfunc_begin31 # >> Call Site 1 <<
	.long	.Ltmp794-.Ltmp793       #   Call between .Ltmp793 and .Ltmp794
	.long	.Ltmp795-.Lfunc_begin31 #     jumps to .Ltmp795
	.byte	0                       #   On action: cleanup
	.long	.Ltmp794-.Lfunc_begin31 # >> Call Site 2 <<
	.long	.Lfunc_end35-.Ltmp794   #   Call between .Ltmp794 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED2Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED2Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED2Ev,@function
_ZN13CObjectVectorI5CPropED2Ev:         # @_ZN13CObjectVectorI5CPropED2Ev
.Lfunc_begin32:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception32
# BB#0:
	pushq	%r14
.Lcfi363:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi364:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi365:
	.cfi_def_cfa_offset 32
.Lcfi366:
	.cfi_offset %rbx, -24
.Lcfi367:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp796:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp797:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB36_2:
.Ltmp798:
	movq	%rax, %r14
.Ltmp799:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp800:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB36_4:
.Ltmp801:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end36:
	.size	_ZN13CObjectVectorI5CPropED2Ev, .Lfunc_end36-_ZN13CObjectVectorI5CPropED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception32:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp796-.Lfunc_begin32 # >> Call Site 1 <<
	.long	.Ltmp797-.Ltmp796       #   Call between .Ltmp796 and .Ltmp797
	.long	.Ltmp798-.Lfunc_begin32 #     jumps to .Ltmp798
	.byte	0                       #   On action: cleanup
	.long	.Ltmp797-.Lfunc_begin32 # >> Call Site 2 <<
	.long	.Ltmp799-.Ltmp797       #   Call between .Ltmp797 and .Ltmp799
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp799-.Lfunc_begin32 # >> Call Site 3 <<
	.long	.Ltmp800-.Ltmp799       #   Call between .Ltmp799 and .Ltmp800
	.long	.Ltmp801-.Lfunc_begin32 #     jumps to .Ltmp801
	.byte	1                       #   On action: 1
	.long	.Ltmp800-.Lfunc_begin32 # >> Call Site 4 <<
	.long	.Lfunc_end36-.Ltmp800   #   Call between .Ltmp800 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropED0Ev,"axG",@progbits,_ZN13CObjectVectorI5CPropED0Ev,comdat
	.weak	_ZN13CObjectVectorI5CPropED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropED0Ev,@function
_ZN13CObjectVectorI5CPropED0Ev:         # @_ZN13CObjectVectorI5CPropED0Ev
.Lfunc_begin33:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception33
# BB#0:
	pushq	%r14
.Lcfi368:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi369:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi370:
	.cfi_def_cfa_offset 32
.Lcfi371:
	.cfi_offset %rbx, -24
.Lcfi372:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI5CPropE+16, (%rbx)
.Ltmp802:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp803:
# BB#1:
.Ltmp808:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp809:
# BB#2:                                 # %_ZN13CObjectVectorI5CPropED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB37_5:
.Ltmp810:
	movq	%rax, %r14
	jmp	.LBB37_6
.LBB37_3:
.Ltmp804:
	movq	%rax, %r14
.Ltmp805:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp806:
.LBB37_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB37_4:
.Ltmp807:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end37:
	.size	_ZN13CObjectVectorI5CPropED0Ev, .Lfunc_end37-_ZN13CObjectVectorI5CPropED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception33:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp802-.Lfunc_begin33 # >> Call Site 1 <<
	.long	.Ltmp803-.Ltmp802       #   Call between .Ltmp802 and .Ltmp803
	.long	.Ltmp804-.Lfunc_begin33 #     jumps to .Ltmp804
	.byte	0                       #   On action: cleanup
	.long	.Ltmp808-.Lfunc_begin33 # >> Call Site 2 <<
	.long	.Ltmp809-.Ltmp808       #   Call between .Ltmp808 and .Ltmp809
	.long	.Ltmp810-.Lfunc_begin33 #     jumps to .Ltmp810
	.byte	0                       #   On action: cleanup
	.long	.Ltmp805-.Lfunc_begin33 # >> Call Site 3 <<
	.long	.Ltmp806-.Ltmp805       #   Call between .Ltmp805 and .Ltmp806
	.long	.Ltmp807-.Lfunc_begin33 #     jumps to .Ltmp807
	.byte	1                       #   On action: 1
	.long	.Ltmp806-.Lfunc_begin33 # >> Call Site 4 <<
	.long	.Lfunc_end37-.Ltmp806   #   Call between .Ltmp806 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI5CPropE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI5CPropE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropE6DeleteEii,@function
_ZN13CObjectVectorI5CPropE6DeleteEii:   # @_ZN13CObjectVectorI5CPropE6DeleteEii
.Lfunc_begin34:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception34
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi373:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi374:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi375:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi376:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi377:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi378:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi379:
	.cfi_def_cfa_offset 64
.Lcfi380:
	.cfi_offset %rbx, -56
.Lcfi381:
	.cfi_offset %r12, -48
.Lcfi382:
	.cfi_offset %r13, -40
.Lcfi383:
	.cfi_offset %r14, -32
.Lcfi384:
	.cfi_offset %r15, -24
.Lcfi385:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB38_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB38_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB38_5
# BB#3:                                 #   in Loop: Header=BB38_2 Depth=1
	leaq	8(%rbp), %rdi
.Ltmp811:
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp812:
# BB#4:                                 # %_ZN5CPropD2Ev.exit
                                        #   in Loop: Header=BB38_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB38_5:                               #   in Loop: Header=BB38_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB38_2
.LBB38_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB38_7:
.Ltmp813:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZN13CObjectVectorI5CPropE6DeleteEii, .Lfunc_end38-_ZN13CObjectVectorI5CPropE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception34:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp811-.Lfunc_begin34 # >> Call Site 1 <<
	.long	.Ltmp812-.Ltmp811       #   Call between .Ltmp811 and .Ltmp812
	.long	.Ltmp813-.Lfunc_begin34 #     jumps to .Ltmp813
	.byte	0                       #   On action: cleanup
	.long	.Ltmp812-.Lfunc_begin34 # >> Call Site 2 <<
	.long	.Lfunc_end38-.Ltmp812   #   Call between .Ltmp812 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi386:
	.cfi_def_cfa_offset 16
.Lcfi387:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB39_2
# BB#1:
	callq	_ZdaPv
.LBB39_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end39:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end39-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
.Lfunc_begin35:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception35
# BB#0:
	pushq	%r14
.Lcfi388:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi389:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi390:
	.cfi_def_cfa_offset 32
.Lcfi391:
	.cfi_offset %rbx, -24
.Lcfi392:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp814:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp815:
# BB#1:
.Ltmp820:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp821:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB40_5:
.Ltmp822:
	movq	%rax, %r14
	jmp	.LBB40_6
.LBB40_3:
.Ltmp816:
	movq	%rax, %r14
.Ltmp817:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp818:
.LBB40_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB40_4:
.Ltmp819:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev, .Lfunc_end40-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception35:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp814-.Lfunc_begin35 # >> Call Site 1 <<
	.long	.Ltmp815-.Ltmp814       #   Call between .Ltmp814 and .Ltmp815
	.long	.Ltmp816-.Lfunc_begin35 #     jumps to .Ltmp816
	.byte	0                       #   On action: cleanup
	.long	.Ltmp820-.Lfunc_begin35 # >> Call Site 2 <<
	.long	.Ltmp821-.Ltmp820       #   Call between .Ltmp820 and .Ltmp821
	.long	.Ltmp822-.Lfunc_begin35 #     jumps to .Ltmp822
	.byte	0                       #   On action: cleanup
	.long	.Ltmp817-.Lfunc_begin35 # >> Call Site 3 <<
	.long	.Ltmp818-.Ltmp817       #   Call between .Ltmp817 and .Ltmp818
	.long	.Ltmp819-.Lfunc_begin35 #     jumps to .Ltmp819
	.byte	1                       #   On action: 1
	.long	.Ltmp818-.Lfunc_begin35 # >> Call Site 4 <<
	.long	.Lfunc_end40-.Ltmp818   #   Call between .Ltmp818 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi393:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi394:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi395:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi396:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi397:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi398:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi399:
	.cfi_def_cfa_offset 64
.Lcfi400:
	.cfi_offset %rbx, -56
.Lcfi401:
	.cfi_offset %r12, -48
.Lcfi402:
	.cfi_offset %r13, -40
.Lcfi403:
	.cfi_offset %r14, -32
.Lcfi404:
	.cfi_offset %r15, -24
.Lcfi405:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB41_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB41_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB41_6
# BB#3:                                 #   in Loop: Header=BB41_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB41_5
# BB#4:                                 #   in Loop: Header=BB41_2 Depth=1
	callq	_ZdaPv
.LBB41_5:                               # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB41_6:                               #   in Loop: Header=BB41_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB41_2
.LBB41_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end41:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii, .Lfunc_end41-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin36:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception36
# BB#0:
	pushq	%r14
.Lcfi406:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi407:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi408:
	.cfi_def_cfa_offset 32
.Lcfi409:
	.cfi_offset %rbx, -24
.Lcfi410:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp823:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp824:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB42_2:
.Ltmp825:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end42:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end42-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception36:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp823-.Lfunc_begin36 # >> Call Site 1 <<
	.long	.Ltmp824-.Ltmp823       #   Call between .Ltmp823 and .Ltmp824
	.long	.Ltmp825-.Lfunc_begin36 #     jumps to .Ltmp825
	.byte	0                       #   On action: cleanup
	.long	.Ltmp824-.Lfunc_begin36 # >> Call Site 2 <<
	.long	.Lfunc_end42-.Ltmp824   #   Call between .Ltmp824 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI16CInOutTempBufferED0Ev,"axG",@progbits,_ZN13CObjectVectorI16CInOutTempBufferED0Ev,comdat
	.weak	_ZN13CObjectVectorI16CInOutTempBufferED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI16CInOutTempBufferED0Ev,@function
_ZN13CObjectVectorI16CInOutTempBufferED0Ev: # @_ZN13CObjectVectorI16CInOutTempBufferED0Ev
.Lfunc_begin37:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception37
# BB#0:
	pushq	%r14
.Lcfi411:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi412:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi413:
	.cfi_def_cfa_offset 32
.Lcfi414:
	.cfi_offset %rbx, -24
.Lcfi415:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI16CInOutTempBufferE+16, (%rbx)
.Ltmp826:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp827:
# BB#1:
.Ltmp832:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp833:
# BB#2:                                 # %_ZN13CObjectVectorI16CInOutTempBufferED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_5:
.Ltmp834:
	movq	%rax, %r14
	jmp	.LBB43_6
.LBB43_3:
.Ltmp828:
	movq	%rax, %r14
.Ltmp829:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp830:
.LBB43_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB43_4:
.Ltmp831:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end43:
	.size	_ZN13CObjectVectorI16CInOutTempBufferED0Ev, .Lfunc_end43-_ZN13CObjectVectorI16CInOutTempBufferED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception37:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp826-.Lfunc_begin37 # >> Call Site 1 <<
	.long	.Ltmp827-.Ltmp826       #   Call between .Ltmp826 and .Ltmp827
	.long	.Ltmp828-.Lfunc_begin37 #     jumps to .Ltmp828
	.byte	0                       #   On action: cleanup
	.long	.Ltmp832-.Lfunc_begin37 # >> Call Site 2 <<
	.long	.Ltmp833-.Ltmp832       #   Call between .Ltmp832 and .Ltmp833
	.long	.Ltmp834-.Lfunc_begin37 #     jumps to .Ltmp834
	.byte	0                       #   On action: cleanup
	.long	.Ltmp829-.Lfunc_begin37 # >> Call Site 3 <<
	.long	.Ltmp830-.Ltmp829       #   Call between .Ltmp829 and .Ltmp830
	.long	.Ltmp831-.Lfunc_begin37 #     jumps to .Ltmp831
	.byte	1                       #   On action: 1
	.long	.Ltmp830-.Lfunc_begin37 # >> Call Site 4 <<
	.long	.Lfunc_end43-.Ltmp830   #   Call between .Ltmp830 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI16CInOutTempBufferE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI16CInOutTempBufferE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI16CInOutTempBufferE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI16CInOutTempBufferE6DeleteEii,@function
_ZN13CObjectVectorI16CInOutTempBufferE6DeleteEii: # @_ZN13CObjectVectorI16CInOutTempBufferE6DeleteEii
.Lfunc_begin38:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception38
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi416:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi417:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi418:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi419:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi420:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi421:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi422:
	.cfi_def_cfa_offset 64
.Lcfi423:
	.cfi_offset %rbx, -56
.Lcfi424:
	.cfi_offset %r12, -48
.Lcfi425:
	.cfi_offset %r13, -40
.Lcfi426:
	.cfi_offset %r14, -32
.Lcfi427:
	.cfi_offset %r15, -24
.Lcfi428:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB44_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB44_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB44_5
# BB#3:                                 #   in Loop: Header=BB44_2 Depth=1
.Ltmp835:
	movq	%rbp, %rdi
	callq	_ZN16CInOutTempBufferD1Ev
.Ltmp836:
# BB#4:                                 #   in Loop: Header=BB44_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB44_5:                               #   in Loop: Header=BB44_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB44_2
.LBB44_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB44_7:
.Ltmp837:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end44:
	.size	_ZN13CObjectVectorI16CInOutTempBufferE6DeleteEii, .Lfunc_end44-_ZN13CObjectVectorI16CInOutTempBufferE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table44:
.Lexception38:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp835-.Lfunc_begin38 # >> Call Site 1 <<
	.long	.Ltmp836-.Ltmp835       #   Call between .Ltmp835 and .Ltmp836
	.long	.Ltmp837-.Lfunc_begin38 #     jumps to .Ltmp837
	.byte	0                       #   On action: cleanup
	.long	.Ltmp836-.Lfunc_begin38 # >> Call Site 2 <<
	.long	.Lfunc_end44-.Ltmp836   #   Call between .Ltmp836 and .Lfunc_end44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIP27CSequentialOutTempBufferImpED0Ev,"axG",@progbits,_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED0Ev,comdat
	.weak	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED0Ev,@function
_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED0Ev: # @_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED0Ev
.Lfunc_begin39:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception39
# BB#0:
	pushq	%r14
.Lcfi429:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi430:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi431:
	.cfi_def_cfa_offset 32
.Lcfi432:
	.cfi_offset %rbx, -24
.Lcfi433:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE+16, (%rbx)
.Ltmp838:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp839:
# BB#1:
.Ltmp844:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp845:
# BB#2:                                 # %_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB45_5:
.Ltmp846:
	movq	%rax, %r14
	jmp	.LBB45_6
.LBB45_3:
.Ltmp840:
	movq	%rax, %r14
.Ltmp841:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp842:
.LBB45_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB45_4:
.Ltmp843:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end45:
	.size	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED0Ev, .Lfunc_end45-_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception39:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp838-.Lfunc_begin39 # >> Call Site 1 <<
	.long	.Ltmp839-.Ltmp838       #   Call between .Ltmp838 and .Ltmp839
	.long	.Ltmp840-.Lfunc_begin39 #     jumps to .Ltmp840
	.byte	0                       #   On action: cleanup
	.long	.Ltmp844-.Lfunc_begin39 # >> Call Site 2 <<
	.long	.Ltmp845-.Ltmp844       #   Call between .Ltmp844 and .Ltmp845
	.long	.Ltmp846-.Lfunc_begin39 #     jumps to .Ltmp846
	.byte	0                       #   On action: cleanup
	.long	.Ltmp841-.Lfunc_begin39 # >> Call Site 3 <<
	.long	.Ltmp842-.Ltmp841       #   Call between .Ltmp841 and .Ltmp842
	.long	.Ltmp843-.Lfunc_begin39 #     jumps to .Ltmp843
	.byte	1                       #   On action: 1
	.long	.Ltmp842-.Lfunc_begin39 # >> Call Site 4 <<
	.long	.Lfunc_end45-.Ltmp842   #   Call between .Ltmp842 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIP27CSequentialOutTempBufferImpE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIP27CSequentialOutTempBufferImpE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpE6DeleteEii,@function
_ZN13CObjectVectorIP27CSequentialOutTempBufferImpE6DeleteEii: # @_ZN13CObjectVectorIP27CSequentialOutTempBufferImpE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi434:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi435:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi436:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi437:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi438:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi439:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi440:
	.cfi_def_cfa_offset 64
.Lcfi441:
	.cfi_offset %rbx, -56
.Lcfi442:
	.cfi_offset %r12, -48
.Lcfi443:
	.cfi_offset %r13, -40
.Lcfi444:
	.cfi_offset %r14, -32
.Lcfi445:
	.cfi_offset %r15, -24
.Lcfi446:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r14d
	movq	%rdi, %r12
	leal	(%rdx,%r14), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	subl	%r14d, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB46_5
# BB#1:                                 # %.lr.ph
	movslq	%r14d, %rbp
	movslq	%r15d, %r13
	shlq	$3, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB46_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbp, %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB46_4
# BB#3:                                 #   in Loop: Header=BB46_2 Depth=1
	callq	_ZdlPv
.LBB46_4:                               #   in Loop: Header=BB46_2 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB46_2
.LBB46_5:                               # %._crit_edge
	movq	%r12, %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end46:
	.size	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpE6DeleteEii, .Lfunc_end46-_ZN13CObjectVectorIP27CSequentialOutTempBufferImpE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
.Lfunc_begin40:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception40
# BB#0:
	pushq	%r14
.Lcfi447:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi448:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi449:
	.cfi_def_cfa_offset 32
.Lcfi450:
	.cfi_offset %rbx, -24
.Lcfi451:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE+16, (%rbx)
.Ltmp847:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp848:
# BB#1:
.Ltmp853:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp854:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB47_5:
.Ltmp855:
	movq	%rax, %r14
	jmp	.LBB47_6
.LBB47_3:
.Ltmp849:
	movq	%rax, %r14
.Ltmp850:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp851:
.LBB47_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB47_4:
.Ltmp852:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end47:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev, .Lfunc_end47-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table47:
.Lexception40:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp847-.Lfunc_begin40 # >> Call Site 1 <<
	.long	.Ltmp848-.Ltmp847       #   Call between .Ltmp847 and .Ltmp848
	.long	.Ltmp849-.Lfunc_begin40 #     jumps to .Ltmp849
	.byte	0                       #   On action: cleanup
	.long	.Ltmp853-.Lfunc_begin40 # >> Call Site 2 <<
	.long	.Ltmp854-.Ltmp853       #   Call between .Ltmp853 and .Ltmp854
	.long	.Ltmp855-.Lfunc_begin40 #     jumps to .Ltmp855
	.byte	0                       #   On action: cleanup
	.long	.Ltmp850-.Lfunc_begin40 # >> Call Site 3 <<
	.long	.Ltmp851-.Ltmp850       #   Call between .Ltmp850 and .Ltmp851
	.long	.Ltmp852-.Lfunc_begin40 #     jumps to .Ltmp852
	.byte	1                       #   On action: 1
	.long	.Ltmp851-.Lfunc_begin40 # >> Call Site 4 <<
	.long	.Lfunc_end47-.Ltmp851   #   Call between .Ltmp851 and .Lfunc_end47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
.Lfunc_begin41:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception41
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi452:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi453:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi454:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi455:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi456:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi457:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi458:
	.cfi_def_cfa_offset 64
.Lcfi459:
	.cfi_offset %rbx, -56
.Lcfi460:
	.cfi_offset %r12, -48
.Lcfi461:
	.cfi_offset %r13, -40
.Lcfi462:
	.cfi_offset %r14, -32
.Lcfi463:
	.cfi_offset %r15, -24
.Lcfi464:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB48_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB48_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB48_6
# BB#3:                                 #   in Loop: Header=BB48_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB48_5
# BB#4:                                 #   in Loop: Header=BB48_2 Depth=1
	movq	(%rdi), %rax
.Ltmp856:
	callq	*16(%rax)
.Ltmp857:
.LBB48_5:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
                                        #   in Loop: Header=BB48_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB48_6:                               #   in Loop: Header=BB48_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB48_2
.LBB48_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB48_8:
.Ltmp858:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end48:
	.size	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii, .Lfunc_end48-_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table48:
.Lexception41:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp856-.Lfunc_begin41 # >> Call Site 1 <<
	.long	.Ltmp857-.Ltmp856       #   Call between .Ltmp856 and .Ltmp857
	.long	.Ltmp858-.Lfunc_begin41 #     jumps to .Ltmp858
	.byte	0                       #   On action: cleanup
	.long	.Ltmp857-.Lfunc_begin41 # >> Call Site 2 <<
	.long	.Lfunc_end48-.Ltmp857   #   Call between .Ltmp857 and .Lfunc_end48
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN16CInOutTempBufferC2ERKS_,"axG",@progbits,_ZN16CInOutTempBufferC2ERKS_,comdat
	.weak	_ZN16CInOutTempBufferC2ERKS_
	.p2align	4, 0x90
	.type	_ZN16CInOutTempBufferC2ERKS_,@function
_ZN16CInOutTempBufferC2ERKS_:           # @_ZN16CInOutTempBufferC2ERKS_
.Lfunc_begin42:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception42
# BB#0:
	pushq	%r15
.Lcfi465:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi466:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi467:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi468:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi469:
	.cfi_def_cfa_offset 48
.Lcfi470:
	.cfi_offset %rbx, -48
.Lcfi471:
	.cfi_offset %r12, -40
.Lcfi472:
	.cfi_offset %r13, -32
.Lcfi473:
	.cfi_offset %r14, -24
.Lcfi474:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movb	(%r14), %al
	movb	%al, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movslq	16(%r14), %r12
	leaq	1(%r12), %r15
	testl	%r15d, %r15d
	je	.LBB49_1
# BB#2:                                 # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movl	$0, (%rax)
	movl	%r15d, 20(%rbx)
	jmp	.LBB49_3
.LBB49_1:
	xorl	%eax, %eax
.LBB49_3:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	8(%r14), %rcx
	.p2align	4, 0x90
.LBB49_4:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB49_4
# BB#5:                                 # %_ZN8NWindows5NFile10NDirectory9CTempFileC2ERKS2_.exit
	movl	%r12d, 16(%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 24(%rbx)
	movl	32(%r14), %eax
	movl	%eax, 32(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
	movslq	48(%r14), %rax
	leaq	1(%rax), %r15
	testl	%r15d, %r15d
	je	.LBB49_6
# BB#7:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r15, %rdi
	cmovlq	%rax, %rdi
.Ltmp859:
	callq	_Znam
.Ltmp860:
# BB#8:                                 # %.noexc
	movq	%rax, 40(%rbx)
	movb	$0, (%rax)
	movl	%r15d, 52(%rbx)
	jmp	.LBB49_9
.LBB49_6:
	xorl	%eax, %eax
.LBB49_9:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i.i
	leaq	24(%rbx), %r15
	movq	40(%r14), %rcx
	.p2align	4, 0x90
.LBB49_10:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB49_10
# BB#11:
	movl	48(%r14), %eax
	movl	%eax, 48(%rbx)
	leaq	56(%rbx), %rdi
	leaq	56(%r14), %rsi
	movl	$1052, %edx             # imm = 0x41C
	callq	memcpy
	movq	$_ZTVN8NWindows5NFile3NIO8COutFileE+16, 24(%rbx)
	movl	1120(%r14), %eax
	movl	%eax, 1120(%rbx)
	movq	1112(%r14), %rax
	movq	%rax, 1112(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 1128(%rbx)
	movslq	1136(%r14), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB49_12
# BB#13:                                # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp862:
	callq	_Znam
.Ltmp863:
# BB#14:                                # %.noexc8
	movq	%rax, 1128(%rbx)
	movl	$0, (%rax)
	movl	%r12d, 1140(%rbx)
	jmp	.LBB49_15
.LBB49_12:
	xorl	%eax, %eax
.LBB49_15:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	1128(%r14), %rcx
	.p2align	4, 0x90
.LBB49_16:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB49_16
# BB#17:
	movl	%r13d, 1136(%rbx)
	movl	1160(%r14), %eax
	movl	%eax, 1160(%rbx)
	movups	1144(%r14), %xmm0
	movups	%xmm0, 1144(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB49_19:
.Ltmp864:
	movq	%rax, %r14
.Ltmp865:
	movq	%r15, %rdi
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp866:
	jmp	.LBB49_20
.LBB49_26:
.Ltmp867:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB49_18:
.Ltmp861:
	movq	%rax, %r14
.LBB49_20:
.Ltmp868:
	movq	%rbx, %rdi
	callq	_ZN8NWindows5NFile10NDirectory9CTempFile6RemoveEv
.Ltmp869:
# BB#21:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB49_23
# BB#22:
	callq	_ZdaPv
.LBB49_23:                              # %_ZN8NWindows5NFile10NDirectory9CTempFileD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB49_24:
.Ltmp870:
	movq	%rax, %r14
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB49_25
# BB#27:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB49_25:
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end49:
	.size	_ZN16CInOutTempBufferC2ERKS_, .Lfunc_end49-_ZN16CInOutTempBufferC2ERKS_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table49:
.Lexception42:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin42-.Lfunc_begin42 # >> Call Site 1 <<
	.long	.Ltmp859-.Lfunc_begin42 #   Call between .Lfunc_begin42 and .Ltmp859
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp859-.Lfunc_begin42 # >> Call Site 2 <<
	.long	.Ltmp860-.Ltmp859       #   Call between .Ltmp859 and .Ltmp860
	.long	.Ltmp861-.Lfunc_begin42 #     jumps to .Ltmp861
	.byte	0                       #   On action: cleanup
	.long	.Ltmp860-.Lfunc_begin42 # >> Call Site 3 <<
	.long	.Ltmp862-.Ltmp860       #   Call between .Ltmp860 and .Ltmp862
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp862-.Lfunc_begin42 # >> Call Site 4 <<
	.long	.Ltmp863-.Ltmp862       #   Call between .Ltmp862 and .Ltmp863
	.long	.Ltmp864-.Lfunc_begin42 #     jumps to .Ltmp864
	.byte	0                       #   On action: cleanup
	.long	.Ltmp865-.Lfunc_begin42 # >> Call Site 5 <<
	.long	.Ltmp866-.Ltmp865       #   Call between .Ltmp865 and .Ltmp866
	.long	.Ltmp867-.Lfunc_begin42 #     jumps to .Ltmp867
	.byte	1                       #   On action: 1
	.long	.Ltmp868-.Lfunc_begin42 # >> Call Site 6 <<
	.long	.Ltmp869-.Ltmp868       #   Call between .Ltmp868 and .Ltmp869
	.long	.Ltmp870-.Lfunc_begin42 #     jumps to .Ltmp870
	.byte	1                       #   On action: 1
	.long	.Ltmp869-.Lfunc_begin42 # >> Call Site 7 <<
	.long	.Lfunc_end49-.Ltmp869   #   Call between .Ltmp869 and .Lfunc_end49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO8COutFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO8COutFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFileD0Ev,@function
_ZN8NWindows5NFile3NIO8COutFileD0Ev:    # @_ZN8NWindows5NFile3NIO8COutFileD0Ev
.Lfunc_begin43:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception43
# BB#0:
	pushq	%r14
.Lcfi475:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi476:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi477:
	.cfi_def_cfa_offset 32
.Lcfi478:
	.cfi_offset %rbx, -24
.Lcfi479:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp871:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp872:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB50_2:
.Ltmp873:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end50:
	.size	_ZN8NWindows5NFile3NIO8COutFileD0Ev, .Lfunc_end50-_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table50:
.Lexception43:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp871-.Lfunc_begin43 # >> Call Site 1 <<
	.long	.Ltmp872-.Ltmp871       #   Call between .Ltmp871 and .Ltmp872
	.long	.Ltmp873-.Lfunc_begin43 #     jumps to .Ltmp873
	.byte	0                       #   On action: cleanup
	.long	.Ltmp872-.Lfunc_begin43 # >> Call Site 2 <<
	.long	.Lfunc_end50-.Ltmp872   #   Call between .Ltmp872 and .Lfunc_end50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIPKyED0Ev,"axG",@progbits,_ZN13CRecordVectorIPKyED0Ev,comdat
	.weak	_ZN13CRecordVectorIPKyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIPKyED0Ev,@function
_ZN13CRecordVectorIPKyED0Ev:            # @_ZN13CRecordVectorIPKyED0Ev
.Lfunc_begin44:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception44
# BB#0:
	pushq	%r14
.Lcfi480:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi481:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi482:
	.cfi_def_cfa_offset 32
.Lcfi483:
	.cfi_offset %rbx, -24
.Lcfi484:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp874:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp875:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB51_2:
.Ltmp876:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end51:
	.size	_ZN13CRecordVectorIPKyED0Ev, .Lfunc_end51-_ZN13CRecordVectorIPKyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table51:
.Lexception44:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp874-.Lfunc_begin44 # >> Call Site 1 <<
	.long	.Ltmp875-.Ltmp874       #   Call between .Ltmp874 and .Ltmp875
	.long	.Ltmp876-.Lfunc_begin44 #     jumps to .Ltmp876
	.byte	0                       #   On action: cleanup
	.long	.Ltmp875-.Lfunc_begin44 # >> Call Site 2 <<
	.long	.Lfunc_end51-.Ltmp875   #   Call between .Ltmp875 and .Lfunc_end51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIP19ISequentialInStreamED0Ev,"axG",@progbits,_ZN13CRecordVectorIP19ISequentialInStreamED0Ev,comdat
	.weak	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev,@function
_ZN13CRecordVectorIP19ISequentialInStreamED0Ev: # @_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
.Lfunc_begin45:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception45
# BB#0:
	pushq	%r14
.Lcfi485:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi486:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi487:
	.cfi_def_cfa_offset 32
.Lcfi488:
	.cfi_offset %rbx, -24
.Lcfi489:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp877:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp878:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB52_2:
.Ltmp879:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end52:
	.size	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev, .Lfunc_end52-_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception45:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp877-.Lfunc_begin45 # >> Call Site 1 <<
	.long	.Ltmp878-.Ltmp877       #   Call between .Ltmp877 and .Ltmp878
	.long	.Ltmp879-.Lfunc_begin45 #     jumps to .Ltmp879
	.byte	0                       #   On action: cleanup
	.long	.Ltmp878-.Lfunc_begin45 # >> Call Site 2 <<
	.long	.Lfunc_end52-.Ltmp878   #   Call between .Ltmp878 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIP20ISequentialOutStreamED0Ev,"axG",@progbits,_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev,comdat
	.weak	_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev,@function
_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev: # @_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev
.Lfunc_begin46:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception46
# BB#0:
	pushq	%r14
.Lcfi490:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi491:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi492:
	.cfi_def_cfa_offset 32
.Lcfi493:
	.cfi_offset %rbx, -24
.Lcfi494:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp880:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp881:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB53_2:
.Ltmp882:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end53:
	.size	_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev, .Lfunc_end53-_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table53:
.Lexception46:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp880-.Lfunc_begin46 # >> Call Site 1 <<
	.long	.Ltmp881-.Ltmp880       #   Call between .Ltmp880 and .Ltmp881
	.long	.Ltmp882-.Lfunc_begin46 #     jumps to .Ltmp882
	.byte	0                       #   On action: cleanup
	.long	.Ltmp881-.Lfunc_begin46 # >> Call Site 2 <<
	.long	.Lfunc_end53-.Ltmp881   #   Call between .Ltmp881 and .Lfunc_end53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI5CPropEpLERKS1_,"axG",@progbits,_ZN13CObjectVectorI5CPropEpLERKS1_,comdat
	.weak	_ZN13CObjectVectorI5CPropEpLERKS1_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI5CPropEpLERKS1_,@function
_ZN13CObjectVectorI5CPropEpLERKS1_:     # @_ZN13CObjectVectorI5CPropEpLERKS1_
.Lfunc_begin47:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception47
# BB#0:
	pushq	%rbp
.Lcfi495:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi496:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi497:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi498:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi499:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi500:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi501:
	.cfi_def_cfa_offset 64
.Lcfi502:
	.cfi_offset %rbx, -56
.Lcfi503:
	.cfi_offset %r12, -48
.Lcfi504:
	.cfi_offset %r13, -40
.Lcfi505:
	.cfi_offset %r14, -32
.Lcfi506:
	.cfi_offset %r15, -24
.Lcfi507:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movslq	12(%r14), %r13
	movl	12(%r15), %esi
	addl	%r13d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testq	%r13, %r13
	jle	.LBB54_4
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB54_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbp,8), %rbx
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r12
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	leaq	8(%r12), %rdi
	addq	$8, %rbx
.Ltmp883:
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantC1ERKS1_
.Ltmp884:
# BB#3:                                 # %_ZN13CObjectVectorI5CPropE3AddERKS0_.exit
                                        #   in Loop: Header=BB54_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB54_2
.LBB54_4:                               # %._crit_edge
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB54_5:
.Ltmp885:
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end54:
	.size	_ZN13CObjectVectorI5CPropEpLERKS1_, .Lfunc_end54-_ZN13CObjectVectorI5CPropEpLERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table54:
.Lexception47:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin47-.Lfunc_begin47 # >> Call Site 1 <<
	.long	.Ltmp883-.Lfunc_begin47 #   Call between .Lfunc_begin47 and .Ltmp883
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp883-.Lfunc_begin47 # >> Call Site 2 <<
	.long	.Ltmp884-.Ltmp883       #   Call between .Ltmp883 and .Ltmp884
	.long	.Ltmp885-.Lfunc_begin47 #     jumps to .Ltmp885
	.byte	0                       #   On action: cleanup
	.long	.Ltmp884-.Lfunc_begin47 # >> Call Site 3 <<
	.long	.Lfunc_end54-.Ltmp884   #   Call between .Ltmp884 and .Lfunc_end54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV13CObjectVectorI13CStreamBinderE,@object # @_ZTV13CObjectVectorI13CStreamBinderE
	.section	.rodata._ZTV13CObjectVectorI13CStreamBinderE,"aG",@progbits,_ZTV13CObjectVectorI13CStreamBinderE,comdat
	.weak	_ZTV13CObjectVectorI13CStreamBinderE
	.p2align	3
_ZTV13CObjectVectorI13CStreamBinderE:
	.quad	0
	.quad	_ZTI13CObjectVectorI13CStreamBinderE
	.quad	_ZN13CObjectVectorI13CStreamBinderED2Ev
	.quad	_ZN13CObjectVectorI13CStreamBinderED0Ev
	.quad	_ZN13CObjectVectorI13CStreamBinderE6DeleteEii
	.size	_ZTV13CObjectVectorI13CStreamBinderE, 40

	.type	_ZTS13CObjectVectorI13CStreamBinderE,@object # @_ZTS13CObjectVectorI13CStreamBinderE
	.section	.rodata._ZTS13CObjectVectorI13CStreamBinderE,"aG",@progbits,_ZTS13CObjectVectorI13CStreamBinderE,comdat
	.weak	_ZTS13CObjectVectorI13CStreamBinderE
	.p2align	4
_ZTS13CObjectVectorI13CStreamBinderE:
	.asciz	"13CObjectVectorI13CStreamBinderE"
	.size	_ZTS13CObjectVectorI13CStreamBinderE, 33

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI13CStreamBinderE,@object # @_ZTI13CObjectVectorI13CStreamBinderE
	.section	.rodata._ZTI13CObjectVectorI13CStreamBinderE,"aG",@progbits,_ZTI13CObjectVectorI13CStreamBinderE,comdat
	.weak	_ZTI13CObjectVectorI13CStreamBinderE
	.p2align	4
_ZTI13CObjectVectorI13CStreamBinderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI13CStreamBinderE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI13CStreamBinderE, 24

	.type	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE:
	.asciz	"N8NWindows16NSynchronization14CBaseEventWFMOE"
	.size	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE, 46

	.type	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.asciz	"N8NWindows16NSynchronization15CBaseHandleWFMOE"
	.size	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE, 47

	.type	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	3
_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE, 16

	.type	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE,@object # @_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE
	.section	.rodata._ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE,"aG",@progbits,_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE,comdat
	.weak	_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE
	.p2align	3
_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE
	.quad	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED2Ev
	.quad	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EED0Ev
	.quad	_ZN13CObjectVectorIN11NCoderMixer7CCoder2EE6DeleteEii
	.size	_ZTV13CObjectVectorIN11NCoderMixer7CCoder2EE, 40

	.type	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE,@object # @_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE
	.section	.rodata._ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE,"aG",@progbits,_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE,comdat
	.weak	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE
	.p2align	4
_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE:
	.asciz	"13CObjectVectorIN11NCoderMixer7CCoder2EE"
	.size	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE, 41

	.type	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE,@object # @_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE
	.section	.rodata._ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE,"aG",@progbits,_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE,comdat
	.weak	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE
	.p2align	4
_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN11NCoderMixer7CCoder2EE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN11NCoderMixer7CCoder2EE, 24

	.type	_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE:
	.asciz	"13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE, 51

	.type	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI19ISequentialInStreamEE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z11CMethodFullEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z11CMethodFullEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 46

	.type	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z11CMethodFullEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z11CMethodFullEE, 24

	.type	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z5CBindEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z5CBindEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z5CBindEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE, 39

	.type	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z5CBindEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z5CBindEE, 24

	.type	_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,@object # @_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.section	.rodata._ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,"aG",@progbits,_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,comdat
	.weak	_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.p2align	3
_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE, 40

	.type	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,@object # @_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.section	.rodata._ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,"aG",@progbits,_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,comdat
	.weak	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.p2align	4
_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE:
	.asciz	"13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE"
	.size	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE, 52

	.type	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,@object # @_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.section	.rodata._ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,"aG",@progbits,_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE,comdat
	.weak	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.p2align	4
_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN11NCoderMixer17CCoderStreamsInfoEE, 24

	.type	_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE,@object # @_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE
	.section	.rodata._ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE,"aG",@progbits,_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE,comdat
	.weak	_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE
	.p2align	3
_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN11NCoderMixer9CBindPairEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN11NCoderMixer9CBindPairEE, 40

	.type	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE,@object # @_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE
	.section	.rodata._ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE,"aG",@progbits,_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE,comdat
	.weak	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE
	.p2align	4
_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE:
	.asciz	"13CRecordVectorIN11NCoderMixer9CBindPairEE"
	.size	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE, 43

	.type	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE,@object # @_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE
	.section	.rodata._ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE,"aG",@progbits,_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE,comdat
	.weak	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE
	.p2align	4
_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN11NCoderMixer9CBindPairEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN11NCoderMixer9CBindPairEE, 24

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CObjectVectorI5CPropE,@object # @_ZTV13CObjectVectorI5CPropE
	.section	.rodata._ZTV13CObjectVectorI5CPropE,"aG",@progbits,_ZTV13CObjectVectorI5CPropE,comdat
	.weak	_ZTV13CObjectVectorI5CPropE
	.p2align	3
_ZTV13CObjectVectorI5CPropE:
	.quad	0
	.quad	_ZTI13CObjectVectorI5CPropE
	.quad	_ZN13CObjectVectorI5CPropED2Ev
	.quad	_ZN13CObjectVectorI5CPropED0Ev
	.quad	_ZN13CObjectVectorI5CPropE6DeleteEii
	.size	_ZTV13CObjectVectorI5CPropE, 40

	.type	_ZTS13CObjectVectorI5CPropE,@object # @_ZTS13CObjectVectorI5CPropE
	.section	.rodata._ZTS13CObjectVectorI5CPropE,"aG",@progbits,_ZTS13CObjectVectorI5CPropE,comdat
	.weak	_ZTS13CObjectVectorI5CPropE
	.p2align	4
_ZTS13CObjectVectorI5CPropE:
	.asciz	"13CObjectVectorI5CPropE"
	.size	_ZTS13CObjectVectorI5CPropE, 24

	.type	_ZTI13CObjectVectorI5CPropE,@object # @_ZTI13CObjectVectorI5CPropE
	.section	.rodata._ZTI13CObjectVectorI5CPropE,"aG",@progbits,_ZTI13CObjectVectorI5CPropE,comdat
	.weak	_ZTI13CObjectVectorI5CPropE
	.p2align	4
_ZTI13CObjectVectorI5CPropE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI5CPropE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI5CPropE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z10CCoderInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 45

	.type	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CObjectVectorI16CInOutTempBufferE,@object # @_ZTV13CObjectVectorI16CInOutTempBufferE
	.section	.rodata._ZTV13CObjectVectorI16CInOutTempBufferE,"aG",@progbits,_ZTV13CObjectVectorI16CInOutTempBufferE,comdat
	.weak	_ZTV13CObjectVectorI16CInOutTempBufferE
	.p2align	3
_ZTV13CObjectVectorI16CInOutTempBufferE:
	.quad	0
	.quad	_ZTI13CObjectVectorI16CInOutTempBufferE
	.quad	_ZN13CObjectVectorI16CInOutTempBufferED2Ev
	.quad	_ZN13CObjectVectorI16CInOutTempBufferED0Ev
	.quad	_ZN13CObjectVectorI16CInOutTempBufferE6DeleteEii
	.size	_ZTV13CObjectVectorI16CInOutTempBufferE, 40

	.type	_ZTS13CObjectVectorI16CInOutTempBufferE,@object # @_ZTS13CObjectVectorI16CInOutTempBufferE
	.section	.rodata._ZTS13CObjectVectorI16CInOutTempBufferE,"aG",@progbits,_ZTS13CObjectVectorI16CInOutTempBufferE,comdat
	.weak	_ZTS13CObjectVectorI16CInOutTempBufferE
	.p2align	4
_ZTS13CObjectVectorI16CInOutTempBufferE:
	.asciz	"13CObjectVectorI16CInOutTempBufferE"
	.size	_ZTS13CObjectVectorI16CInOutTempBufferE, 36

	.type	_ZTI13CObjectVectorI16CInOutTempBufferE,@object # @_ZTI13CObjectVectorI16CInOutTempBufferE
	.section	.rodata._ZTI13CObjectVectorI16CInOutTempBufferE,"aG",@progbits,_ZTI13CObjectVectorI16CInOutTempBufferE,comdat
	.weak	_ZTI13CObjectVectorI16CInOutTempBufferE
	.p2align	4
_ZTI13CObjectVectorI16CInOutTempBufferE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI16CInOutTempBufferE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI16CInOutTempBufferE, 24

	.type	_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE,@object # @_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE
	.section	.rodata._ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE,"aG",@progbits,_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE,comdat
	.weak	_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE
	.p2align	3
_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE:
	.quad	0
	.quad	_ZTI13CObjectVectorIP27CSequentialOutTempBufferImpE
	.quad	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED2Ev
	.quad	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpED0Ev
	.quad	_ZN13CObjectVectorIP27CSequentialOutTempBufferImpE6DeleteEii
	.size	_ZTV13CObjectVectorIP27CSequentialOutTempBufferImpE, 40

	.type	_ZTS13CObjectVectorIP27CSequentialOutTempBufferImpE,@object # @_ZTS13CObjectVectorIP27CSequentialOutTempBufferImpE
	.section	.rodata._ZTS13CObjectVectorIP27CSequentialOutTempBufferImpE,"aG",@progbits,_ZTS13CObjectVectorIP27CSequentialOutTempBufferImpE,comdat
	.weak	_ZTS13CObjectVectorIP27CSequentialOutTempBufferImpE
	.p2align	4
_ZTS13CObjectVectorIP27CSequentialOutTempBufferImpE:
	.asciz	"13CObjectVectorIP27CSequentialOutTempBufferImpE"
	.size	_ZTS13CObjectVectorIP27CSequentialOutTempBufferImpE, 48

	.type	_ZTI13CObjectVectorIP27CSequentialOutTempBufferImpE,@object # @_ZTI13CObjectVectorIP27CSequentialOutTempBufferImpE
	.section	.rodata._ZTI13CObjectVectorIP27CSequentialOutTempBufferImpE,"aG",@progbits,_ZTI13CObjectVectorIP27CSequentialOutTempBufferImpE,comdat
	.weak	_ZTI13CObjectVectorIP27CSequentialOutTempBufferImpE
	.p2align	4
_ZTI13CObjectVectorIP27CSequentialOutTempBufferImpE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIP27CSequentialOutTempBufferImpE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIP27CSequentialOutTempBufferImpE, 24

	.type	_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE:
	.asciz	"13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE, 52

	.type	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI20ISequentialOutStreamEE, 24

	.type	_ZTVN8NWindows5NFile3NIO8COutFileE,@object # @_ZTVN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO8COutFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO8COutFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO8COutFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO8COutFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO8COutFileE,@object # @_ZTSN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO8COutFileE:
	.asciz	"N8NWindows5NFile3NIO8COutFileE"
	.size	_ZTSN8NWindows5NFile3NIO8COutFileE, 31

	.type	_ZTIN8NWindows5NFile3NIO8COutFileE,@object # @_ZTIN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO8COutFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO8COutFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO8COutFileE, 24

	.type	_ZTV13CRecordVectorIPKyE,@object # @_ZTV13CRecordVectorIPKyE
	.section	.rodata._ZTV13CRecordVectorIPKyE,"aG",@progbits,_ZTV13CRecordVectorIPKyE,comdat
	.weak	_ZTV13CRecordVectorIPKyE
	.p2align	3
_ZTV13CRecordVectorIPKyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIPKyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIPKyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIPKyE, 40

	.type	_ZTS13CRecordVectorIPKyE,@object # @_ZTS13CRecordVectorIPKyE
	.section	.rodata._ZTS13CRecordVectorIPKyE,"aG",@progbits,_ZTS13CRecordVectorIPKyE,comdat
	.weak	_ZTS13CRecordVectorIPKyE
	.p2align	4
_ZTS13CRecordVectorIPKyE:
	.asciz	"13CRecordVectorIPKyE"
	.size	_ZTS13CRecordVectorIPKyE, 21

	.type	_ZTI13CRecordVectorIPKyE,@object # @_ZTI13CRecordVectorIPKyE
	.section	.rodata._ZTI13CRecordVectorIPKyE,"aG",@progbits,_ZTI13CRecordVectorIPKyE,comdat
	.weak	_ZTI13CRecordVectorIPKyE
	.p2align	4
_ZTI13CRecordVectorIPKyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPKyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPKyE, 24

	.type	_ZTV13CRecordVectorIP19ISequentialInStreamE,@object # @_ZTV13CRecordVectorIP19ISequentialInStreamE
	.section	.rodata._ZTV13CRecordVectorIP19ISequentialInStreamE,"aG",@progbits,_ZTV13CRecordVectorIP19ISequentialInStreamE,comdat
	.weak	_ZTV13CRecordVectorIP19ISequentialInStreamE
	.p2align	3
_ZTV13CRecordVectorIP19ISequentialInStreamE:
	.quad	0
	.quad	_ZTI13CRecordVectorIP19ISequentialInStreamE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIP19ISequentialInStreamED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIP19ISequentialInStreamE, 40

	.type	_ZTS13CRecordVectorIP19ISequentialInStreamE,@object # @_ZTS13CRecordVectorIP19ISequentialInStreamE
	.section	.rodata._ZTS13CRecordVectorIP19ISequentialInStreamE,"aG",@progbits,_ZTS13CRecordVectorIP19ISequentialInStreamE,comdat
	.weak	_ZTS13CRecordVectorIP19ISequentialInStreamE
	.p2align	4
_ZTS13CRecordVectorIP19ISequentialInStreamE:
	.asciz	"13CRecordVectorIP19ISequentialInStreamE"
	.size	_ZTS13CRecordVectorIP19ISequentialInStreamE, 40

	.type	_ZTI13CRecordVectorIP19ISequentialInStreamE,@object # @_ZTI13CRecordVectorIP19ISequentialInStreamE
	.section	.rodata._ZTI13CRecordVectorIP19ISequentialInStreamE,"aG",@progbits,_ZTI13CRecordVectorIP19ISequentialInStreamE,comdat
	.weak	_ZTI13CRecordVectorIP19ISequentialInStreamE
	.p2align	4
_ZTI13CRecordVectorIP19ISequentialInStreamE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIP19ISequentialInStreamE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIP19ISequentialInStreamE, 24

	.type	_ZTV13CRecordVectorIP20ISequentialOutStreamE,@object # @_ZTV13CRecordVectorIP20ISequentialOutStreamE
	.section	.rodata._ZTV13CRecordVectorIP20ISequentialOutStreamE,"aG",@progbits,_ZTV13CRecordVectorIP20ISequentialOutStreamE,comdat
	.weak	_ZTV13CRecordVectorIP20ISequentialOutStreamE
	.p2align	3
_ZTV13CRecordVectorIP20ISequentialOutStreamE:
	.quad	0
	.quad	_ZTI13CRecordVectorIP20ISequentialOutStreamE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIP20ISequentialOutStreamED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIP20ISequentialOutStreamE, 40

	.type	_ZTS13CRecordVectorIP20ISequentialOutStreamE,@object # @_ZTS13CRecordVectorIP20ISequentialOutStreamE
	.section	.rodata._ZTS13CRecordVectorIP20ISequentialOutStreamE,"aG",@progbits,_ZTS13CRecordVectorIP20ISequentialOutStreamE,comdat
	.weak	_ZTS13CRecordVectorIP20ISequentialOutStreamE
	.p2align	4
_ZTS13CRecordVectorIP20ISequentialOutStreamE:
	.asciz	"13CRecordVectorIP20ISequentialOutStreamE"
	.size	_ZTS13CRecordVectorIP20ISequentialOutStreamE, 41

	.type	_ZTI13CRecordVectorIP20ISequentialOutStreamE,@object # @_ZTI13CRecordVectorIP20ISequentialOutStreamE
	.section	.rodata._ZTI13CRecordVectorIP20ISequentialOutStreamE,"aG",@progbits,_ZTI13CRecordVectorIP20ISequentialOutStreamE,comdat
	.weak	_ZTI13CRecordVectorIP20ISequentialOutStreamE
	.p2align	4
_ZTI13CRecordVectorIP20ISequentialOutStreamE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIP20ISequentialOutStreamE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIP20ISequentialOutStreamE, 24


	.globl	_ZN8NArchive3N7z8CEncoderC1ERKNS0_22CCompressionMethodModeE
	.type	_ZN8NArchive3N7z8CEncoderC1ERKNS0_22CCompressionMethodModeE,@function
_ZN8NArchive3N7z8CEncoderC1ERKNS0_22CCompressionMethodModeE = _ZN8NArchive3N7z8CEncoderC2ERKNS0_22CCompressionMethodModeE
	.globl	_ZN8NArchive3N7z8CEncoderD1Ev
	.type	_ZN8NArchive3N7z8CEncoderD1Ev,@function
_ZN8NArchive3N7z8CEncoderD1Ev = _ZN8NArchive3N7z8CEncoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
