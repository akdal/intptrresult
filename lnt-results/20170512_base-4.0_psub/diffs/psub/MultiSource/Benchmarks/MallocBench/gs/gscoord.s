	.text
	.file	"gscoord.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1166016512              # float 4096
	.text
	.globl	gs_initmatrix
	.p2align	4, 0x90
	.type	gs_initmatrix,@function
gs_initmatrix:                          # @gs_initmatrix
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	448(%rbx), %rax
	movq	(%rax), %rdi
	movq	8(%rdi), %rax
	leaq	24(%rbx), %rsi
	callq	*8(%rax)
	movss	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	88(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %rax
	movq	%rax, 120(%rbx)
	mulss	104(%rbx), %xmm0
	cvttss2si	%xmm0, %rax
	movq	%rax, 128(%rbx)
	movl	$0, 232(%rbx)
	movl	$0, 432(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	gs_initmatrix, .Lfunc_end0-gs_initmatrix
	.cfi_endproc

	.globl	gs_defaultmatrix
	.p2align	4, 0x90
	.type	gs_defaultmatrix,@function
gs_defaultmatrix:                       # @gs_defaultmatrix
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movq	448(%rdi), %rax
	movq	(%rax), %rdi
	movq	8(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	gs_defaultmatrix, .Lfunc_end1-gs_defaultmatrix
	.cfi_endproc

	.globl	gs_currentmatrix
	.p2align	4, 0x90
	.type	gs_currentmatrix,@function
gs_currentmatrix:                       # @gs_currentmatrix
	.cfi_startproc
# BB#0:
	movups	104(%rdi), %xmm0
	movups	%xmm0, 80(%rsi)
	movups	88(%rdi), %xmm0
	movups	%xmm0, 64(%rsi)
	movups	24(%rdi), %xmm0
	movups	40(%rdi), %xmm1
	movups	56(%rdi), %xmm2
	movups	72(%rdi), %xmm3
	movups	%xmm3, 48(%rsi)
	movups	%xmm2, 32(%rsi)
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	gs_currentmatrix, .Lfunc_end2-gs_currentmatrix
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1166016512              # float 4096
	.text
	.globl	gs_setmatrix
	.p2align	4, 0x90
	.type	gs_setmatrix,@function
gs_setmatrix:                           # @gs_setmatrix
	.cfi_startproc
# BB#0:
	movups	80(%rsi), %xmm0
	movups	%xmm0, 104(%rdi)
	movups	64(%rsi), %xmm0
	movups	%xmm0, 88(%rdi)
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	32(%rsi), %xmm2
	movups	48(%rsi), %xmm3
	movups	%xmm3, 72(%rdi)
	movups	%xmm2, 56(%rdi)
	movups	%xmm1, 40(%rdi)
	movups	%xmm0, 24(%rdi)
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	88(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %rax
	movq	%rax, 120(%rdi)
	mulss	104(%rdi), %xmm0
	cvttss2si	%xmm0, %rax
	movq	%rax, 128(%rdi)
	movl	$0, 232(%rdi)
	movl	$0, 432(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	gs_setmatrix, .Lfunc_end3-gs_setmatrix
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1166016512              # float 4096
	.text
	.globl	gs_translate
	.p2align	4, 0x90
	.type	gs_translate,@function
gs_translate:                           # @gs_translate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	leaq	8(%rsp), %rsi
	callq	gs_distance_transform
	testl	%eax, %eax
	js	.LBB4_2
# BB#1:
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	88(%rbx), %xmm0
	movss	%xmm0, 88(%rbx)
	movss	12(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	104(%rbx), %xmm1
	movss	%xmm1, 104(%rbx)
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	cvttss2si	%xmm0, %rax
	movq	%rax, 120(%rbx)
	mulss	%xmm2, %xmm1
	cvttss2si	%xmm1, %rax
	movq	%rax, 128(%rbx)
	movl	$0, 232(%rbx)
	xorl	%eax, %eax
.LBB4_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end4:
	.size	gs_translate, .Lfunc_end4-gs_translate
	.cfi_endproc

	.globl	gs_scale
	.p2align	4, 0x90
	.type	gs_scale,@function
gs_scale:                               # @gs_scale
	.cfi_startproc
# BB#0:
	movss	24(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movss	%xmm2, 24(%rdi)
	movss	40(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm2, %xmm0
	movss	%xmm0, 40(%rdi)
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 56(%rdi)
	movss	72(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 72(%rdi)
	movl	$0, 232(%rdi)
	movl	$0, 432(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	gs_scale, .Lfunc_end5-gs_scale
	.cfi_endproc

	.globl	gs_rotate
	.p2align	4, 0x90
	.type	gs_rotate,@function
gs_rotate:                              # @gs_rotate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	movq	%rdi, %rsi
	callq	gs_matrix_rotate
	movl	$0, 232(%rbx)
	movl	$0, 432(%rbx)
	popq	%rbx
	retq
.Lfunc_end6:
	.size	gs_rotate, .Lfunc_end6-gs_rotate
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	1166016512              # float 4096
	.text
	.globl	gs_concat
	.p2align	4, 0x90
	.type	gs_concat,@function
gs_concat:                              # @gs_concat
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	movq	%rax, %rdx
	callq	gs_matrix_multiply
	movss	.LCPI7_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	88(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %rcx
	movq	%rcx, 120(%rbx)
	mulss	104(%rbx), %xmm0
	cvttss2si	%xmm0, %rcx
	movq	%rcx, 128(%rbx)
	movl	$0, 232(%rbx)
	movl	$0, 432(%rbx)
	popq	%rbx
	retq
.Lfunc_end7:
	.size	gs_concat, .Lfunc_end7-gs_concat
	.cfi_endproc

	.globl	gs_transform
	.p2align	4, 0x90
	.type	gs_transform,@function
gs_transform:                           # @gs_transform
	.cfi_startproc
# BB#0:
	addq	$24, %rdi
	jmp	gs_point_transform      # TAILCALL
.Lfunc_end8:
	.size	gs_transform, .Lfunc_end8-gs_transform
	.cfi_endproc

	.globl	gs_dtransform
	.p2align	4, 0x90
	.type	gs_dtransform,@function
gs_dtransform:                          # @gs_dtransform
	.cfi_startproc
# BB#0:
	addq	$24, %rdi
	jmp	gs_distance_transform   # TAILCALL
.Lfunc_end9:
	.size	gs_dtransform, .Lfunc_end9-gs_dtransform
	.cfi_endproc

	.globl	gs_itransform
	.p2align	4, 0x90
	.type	gs_itransform,@function
gs_itransform:                          # @gs_itransform
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpl	$0, 232(%rbx)
	je	.LBB10_2
# BB#1:                                 # %._crit_edge
	addq	$136, %rbx
	movq	%rbx, %r15
	jmp	.LBB10_4
.LBB10_2:
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	leaq	24(%rbx), %rdi
	leaq	136(%rbx), %r15
	movq	%r15, %rsi
	callq	gs_matrix_invert
	testl	%eax, %eax
	js	.LBB10_5
# BB#3:                                 # %.critedge
	movl	$1, 232(%rbx)
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB10_4:
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movq	%r15, %rdi
	movq	%r14, %rsi
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	gs_point_transform      # TAILCALL
.LBB10_5:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	gs_itransform, .Lfunc_end10-gs_itransform
	.cfi_endproc

	.globl	gs_idtransform
	.p2align	4, 0x90
	.type	gs_idtransform,@function
gs_idtransform:                         # @gs_idtransform
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -32
.Lcfi22:
	.cfi_offset %r14, -24
.Lcfi23:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpl	$0, 232(%rbx)
	je	.LBB11_2
# BB#1:                                 # %._crit_edge
	addq	$136, %rbx
	movq	%rbx, %r15
	jmp	.LBB11_4
.LBB11_2:
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	leaq	24(%rbx), %rdi
	leaq	136(%rbx), %r15
	movq	%r15, %rsi
	callq	gs_matrix_invert
	testl	%eax, %eax
	js	.LBB11_5
# BB#3:                                 # %.critedge
	movl	$1, 232(%rbx)
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB11_4:
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	cvtss2sd	%xmm1, %xmm1
	movq	%r15, %rdi
	movq	%r14, %rsi
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	gs_distance_transform   # TAILCALL
.LBB11_5:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	gs_idtransform, .Lfunc_end11-gs_idtransform
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4553139223271571456     # double 2.44140625E-4
	.text
	.globl	gs_translate_to_fixed
	.p2align	4, 0x90
	.type	gs_translate_to_fixed,@function
gs_translate_to_fixed:                  # @gs_translate_to_fixed
	.cfi_startproc
# BB#0:
	movq	%rsi, 120(%rdi)
	cvtsi2sdq	%rsi, %xmm0
	movsd	.LCPI12_0(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 88(%rdi)
	movq	%rdx, 128(%rdi)
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	mulsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 104(%rdi)
	movl	$0, 232(%rdi)
	movl	$1, 432(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end12:
	.size	gs_translate_to_fixed, .Lfunc_end12-gs_translate_to_fixed
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
