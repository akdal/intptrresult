	.text
	.file	"me_fullfast.bc"
	.globl	InitializeFastFullIntegerSearch
	.p2align	4, 0x90
	.type	InitializeFastFullIntegerSearch,@function
InitializeFastFullIntegerSearch:        # @InitializeFastFullIntegerSearch
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	input(%rip), %rax
	movl	28(%rax), %r15d
	leal	1(%r15,%r15), %ebx
	imull	%ebx, %ebx
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r14
	movq	%r14, BlockSAD(%rip)
	testq	%r14, %r14
	jne	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
	movq	BlockSAD(%rip), %r14
.LBB0_2:                                # %.preheader67
	movl	%ebx, %r12d
	movq	img(%rip), %rax
	movslq	32(%rax), %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, (%r14)
	testq	%rax, %rax
	jne	.LBB0_4
# BB#3:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_4:                                # %.preheader66
	shlq	$2, %r12
	movq	img(%rip), %rbx
	movl	32(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB0_17
# BB#5:                                 # %.lr.ph79.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph79
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
                                        #       Child Loop BB0_12 Depth 3
	movl	$64, %edi
	callq	malloc
	movq	BlockSAD(%rip), %rcx
	movq	(%rcx), %rcx
	movq	%rax, (%rcx,%r14,8)
	testq	%rax, %rax
	jne	.LBB0_8
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_8:                                # %.preheader65.preheader
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB0_9:                                # %.preheader65
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_12 Depth 3
	movl	$128, %edi
	callq	malloc
	movq	BlockSAD(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	%rax, (%rcx,%r13,8)
	testq	%rax, %rax
	jne	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_9 Depth=2
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_11:                               # %.preheader64.preheader
                                        #   in Loop: Header=BB0_9 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_12:                               # %.preheader64
                                        #   Parent Loop BB0_6 Depth=1
                                        #     Parent Loop BB0_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r12, %rdi
	callq	malloc
	movq	BlockSAD(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	%rax, (%rcx,%rbx,8)
	testq	%rax, %rax
	jne	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_12 Depth=3
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_14:                               #   in Loop: Header=BB0_12 Depth=3
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB0_12
# BB#15:                                #   in Loop: Header=BB0_9 Depth=2
	incq	%r13
	cmpq	$8, %r13
	jne	.LBB0_9
# BB#16:                                #   in Loop: Header=BB0_6 Depth=1
	incq	%r14
	movq	img(%rip), %rbx
	movslq	32(%rbx), %rax
	cmpq	%rax, %r14
	jl	.LBB0_6
.LBB0_17:                               # %._crit_edge80
	movslq	%eax, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	BlockSAD(%rip), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB0_19
# BB#18:
	movl	$.L.str, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbx
.LBB0_19:                               # %.preheader66.1
	cmpl	$0, 32(%rbx)
	jle	.LBB0_32
# BB#20:                                # %.lr.ph79.1.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_21:                               # %.lr.ph79.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_24 Depth 2
                                        #       Child Loop BB0_27 Depth 3
	movl	$64, %edi
	callq	malloc
	movq	BlockSAD(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, (%rcx,%r14,8)
	testq	%rax, %rax
	jne	.LBB0_23
# BB#22:                                #   in Loop: Header=BB0_21 Depth=1
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_23:                               # %.preheader65.1.preheader
                                        #   in Loop: Header=BB0_21 Depth=1
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB0_24:                               # %.preheader65.1
                                        #   Parent Loop BB0_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_27 Depth 3
	movl	$128, %edi
	callq	malloc
	movq	BlockSAD(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	%rax, (%rcx,%r13,8)
	testq	%rax, %rax
	jne	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_24 Depth=2
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_26:                               # %.preheader64.1.preheader
                                        #   in Loop: Header=BB0_24 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_27:                               # %.preheader64.1
                                        #   Parent Loop BB0_21 Depth=1
                                        #     Parent Loop BB0_24 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r12, %rdi
	callq	malloc
	movq	BlockSAD(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx,%r13,8), %rcx
	movq	%rax, (%rcx,%rbx,8)
	testq	%rax, %rax
	jne	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_27 Depth=3
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_29:                               #   in Loop: Header=BB0_27 Depth=3
	incq	%rbx
	cmpq	$16, %rbx
	jne	.LBB0_27
# BB#30:                                #   in Loop: Header=BB0_24 Depth=2
	incq	%r13
	cmpq	$8, %r13
	jne	.LBB0_24
# BB#31:                                #   in Loop: Header=BB0_21 Depth=1
	incq	%r14
	movq	img(%rip), %rax
	movslq	32(%rax), %rax
	cmpq	%rax, %r14
	jl	.LBB0_21
.LBB0_32:                               # %._crit_edge80.1
	movl	$16, %edi
	callq	malloc
	movq	%rax, search_setup_done(%rip)
	testq	%rax, %rax
	jne	.LBB0_34
# BB#33:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB0_34:
	movl	$16, %edi
	callq	malloc
	movq	%rax, search_center_x(%rip)
	testq	%rax, %rax
	jne	.LBB0_36
# BB#35:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB0_36:
	movl	$16, %edi
	callq	malloc
	movq	%rax, search_center_y(%rip)
	testq	%rax, %rax
	jne	.LBB0_38
# BB#37:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
.LBB0_38:
	movl	$16, %edi
	callq	malloc
	movq	%rax, pos_00(%rip)
	testq	%rax, %rax
	jne	.LBB0_40
# BB#39:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB0_40:
	movl	$16, %edi
	callq	malloc
	movq	%rax, max_search_range(%rip)
	testq	%rax, %rax
	jne	.LBB0_42
# BB#41:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
.LBB0_42:                               # %.preheader63.preheader
	movq	img(%rip), %rax
	movslq	32(%rax), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	search_setup_done(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB0_44
# BB#43:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB0_44:
	movq	img(%rip), %rax
	movslq	32(%rax), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	search_center_x(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB0_46
# BB#45:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB0_46:
	movq	img(%rip), %rax
	movslq	32(%rax), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	search_center_y(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB0_48
# BB#47:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
.LBB0_48:
	movq	img(%rip), %rax
	movslq	32(%rax), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	pos_00(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB0_50
# BB#49:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
.LBB0_50:
	movq	img(%rip), %rax
	movslq	32(%rax), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	max_search_range(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rax, %rax
	jne	.LBB0_52
# BB#51:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
.LBB0_52:                               # %.preheader63.191
	movq	img(%rip), %rbx
	movslq	32(%rbx), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	search_setup_done(%rip), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB0_54
# BB#53:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbx
.LBB0_54:
	movslq	32(%rbx), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	search_center_x(%rip), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB0_56
# BB#55:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbx
.LBB0_56:
	movslq	32(%rbx), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	search_center_y(%rip), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB0_58
# BB#57:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbx
.LBB0_58:
	movslq	32(%rbx), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	pos_00(%rip), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB0_60
# BB#59:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbx
.LBB0_60:
	movslq	32(%rbx), %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	max_search_range(%rip), %rcx
	movq	%rax, 8(%rcx)
	testq	%rax, %rax
	jne	.LBB0_62
# BB#61:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
.LBB0_62:
	movq	input(%rip), %rax
	cmpl	$2, 4140(%rax)
	jne	.LBB0_63
# BB#69:                                # %.preheader60
	movq	img(%rip), %rax
	cmpl	$0, 32(%rax)
	jle	.LBB0_75
# BB#70:                                # %.lr.ph
	movq	max_search_range(%rip), %rcx
	movq	(%rcx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_71:                               # =>This Inner Loop Header: Depth=1
	movl	%r15d, (%rdx,%rsi,4)
	incq	%rsi
	movslq	32(%rax), %rdi
	cmpq	%rdi, %rsi
	jl	.LBB0_71
# BB#72:                                # %._crit_edge
	testl	%edi, %edi
	jle	.LBB0_75
# BB#73:                                # %.lr.ph.1
	movq	8(%rcx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_74:                               # =>This Inner Loop Header: Depth=1
	movl	%r15d, (%rcx,%rdx,4)
	incq	%rdx
	movslq	32(%rax), %rsi
	cmpq	%rsi, %rdx
	jl	.LBB0_74
	jmp	.LBB0_75
.LBB0_63:                               # %.preheader61
	movq	max_search_range(%rip), %rdx
	movq	img(%rip), %rax
	movl	%r15d, %ecx
	shrl	$31, %ecx
	addl	%r15d, %ecx
	sarl	%ecx
	movq	(%rdx), %rsi
	movl	%r15d, (%rsi)
	cmpl	$2, 32(%rax)
	jl	.LBB0_66
# BB#64:                                # %.lr.ph72.preheader
	movl	$1, %edi
	.p2align	4, 0x90
.LBB0_65:                               # %.lr.ph72
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%rsi,%rdi,4)
	incq	%rdi
	movslq	32(%rax), %rbx
	cmpq	%rbx, %rdi
	jl	.LBB0_65
.LBB0_66:                               # %._crit_edge73
	movq	8(%rdx), %rdx
	movl	%r15d, (%rdx)
	cmpl	$2, 32(%rax)
	jl	.LBB0_75
# BB#67:                                # %.lr.ph72.1.preheader
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_68:                               # %.lr.ph72.1
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, (%rdx,%rsi,4)
	incq	%rsi
	movslq	32(%rax), %rdi
	cmpq	%rdi, %rsi
	jl	.LBB0_68
.LBB0_75:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	InitializeFastFullIntegerSearch, .Lfunc_end0-InitializeFastFullIntegerSearch
	.cfi_endproc

	.globl	ClearFastFullIntegerSearch
	.p2align	4, 0x90
	.type	ClearFastFullIntegerSearch,@function
ClearFastFullIntegerSearch:             # @ClearFastFullIntegerSearch
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_1:                                # %.preheader33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
                                        #       Child Loop BB1_4 Depth 3
	movq	img(%rip), %rax
	cmpl	$0, 32(%rax)
	jle	.LBB1_6
# BB#2:                                 # %.preheader32.preheader
                                        #   in Loop: Header=BB1_1 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader32
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_4 Depth 3
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader
                                        #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	16(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	24(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	32(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	40(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	48(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	56(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	64(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	72(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	80(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	88(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	96(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	104(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	112(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rax
	movq	120(%rax), %rdi
	callq	free
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	8(%rax,%rbx), %rdi
	callq	free
	addq	$8, %rbx
	cmpq	$56, %rbx
	jne	.LBB1_4
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=2
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rdi
	callq	free
	incq	%r15
	movq	img(%rip), %rax
	movslq	32(%rax), %rax
	cmpq	%rax, %r15
	jl	.LBB1_3
.LBB1_6:                                # %._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	BlockSAD(%rip), %rax
	movq	(%rax,%r14,8), %rdi
	callq	free
	incq	%r14
	cmpq	$2, %r14
	jne	.LBB1_1
# BB#7:
	movq	BlockSAD(%rip), %rdi
	callq	free
	movq	search_setup_done(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	search_center_x(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	search_center_y(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	pos_00(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	max_search_range(%rip), %rax
	movq	(%rax), %rdi
	callq	free
	movq	search_setup_done(%rip), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	search_center_x(%rip), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	search_center_y(%rip), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	pos_00(%rip), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	max_search_range(%rip), %rax
	movq	8(%rax), %rdi
	callq	free
	movq	search_setup_done(%rip), %rdi
	callq	free
	movq	search_center_x(%rip), %rdi
	callq	free
	movq	search_center_y(%rip), %rdi
	callq	free
	movq	pos_00(%rip), %rdi
	callq	free
	movq	max_search_range(%rip), %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.Lfunc_end1:
	.size	ClearFastFullIntegerSearch, .Lfunc_end1-ClearFastFullIntegerSearch
	.cfi_endproc

	.globl	ResetFastFullIntegerSearch
	.p2align	4, 0x90
	.type	ResetFastFullIntegerSearch,@function
ResetFastFullIntegerSearch:             # @ResetFastFullIntegerSearch
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	search_setup_done(%rip), %rbx
	movq	(%rbx), %rdi
	movq	img(%rip), %rax
	movslq	32(%rax), %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	8(%rbx), %rdi
	movq	img(%rip), %rax
	movslq	32(%rax), %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	popq	%rbx
	jmp	memset                  # TAILCALL
.Lfunc_end2:
	.size	ResetFastFullIntegerSearch, .Lfunc_end2-ResetFastFullIntegerSearch
	.cfi_endproc

	.globl	SetupLargerBlocks
	.p2align	4, 0x90
	.type	SetupLargerBlocks,@function
SetupLargerBlocks:                      # @SetupLargerBlocks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 112
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	jle	.LBB3_418
# BB#1:                                 # %.lr.ph494.preheader
	movq	BlockSAD(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	48(%rax), %rcx
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	56(%rax), %rax
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	movq	(%rcx), %r15
	movq	(%rax), %r8
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	32(%rax), %r9
	movl	%edx, %ebx
	cmpl	$8, %edx
	movq	%rdx, %r11
	jae	.LBB3_3
# BB#2:
	xorl	%eax, %eax
	jmp	.LBB3_11
.LBB3_3:                                # %min.iters.checked
	movl	%r11d, %ecx
	andl	$7, %ecx
	movq	%rbx, %r10
	subq	%rcx, %r10
	je	.LBB3_9
# BB#4:                                 # %vector.memcheck
	leaq	(%r15,%rbx,4), %rax
	leaq	(%r8,%rbx,4), %rsi
	movq	%rbx, %rbp
	leaq	(%r9,%rbx,4), %rdi
	cmpq	%rsi, %r15
	sbbb	%bl, %bl
	cmpq	%rax, %r8
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rdi, %r15
	sbbb	%bl, %bl
	cmpq	%rax, %r9
	sbbb	%sil, %sil
	xorl	%eax, %eax
	testb	$1, %dl
	jne	.LBB3_10
# BB#5:                                 # %vector.memcheck
	andb	%sil, %bl
	andb	$1, %bl
	movq	%rbp, %rbx
	jne	.LBB3_11
# BB#6:                                 # %vector.body.preheader
	leaq	16(%r8), %rax
	leaq	16(%r9), %rdi
	leaq	16(%r15), %rbp
	movq	%r10, %rsi
	.p2align	4, 0x90
.LBB3_7:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movdqu	-16(%rdi), %xmm2
	movdqu	(%rdi), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rbp)
	movdqu	%xmm3, (%rbp)
	addq	$32, %rax
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rsi
	jne	.LBB3_7
# BB#8:                                 # %middle.block
	testl	%ecx, %ecx
	movq	%r10, %rax
	jne	.LBB3_11
	jmp	.LBB3_17
.LBB3_9:
	xorl	%eax, %eax
	jmp	.LBB3_11
.LBB3_10:
	movq	%rbp, %rbx
.LBB3_11:                               # %.lr.ph494.preheader1760
	movl	%ebx, %edx
	subl	%eax, %edx
	leaq	-1(%rbx), %rcx
	subq	%rax, %rcx
	andq	$3, %rdx
	je	.LBB3_14
# BB#12:                                # %.lr.ph494.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph494.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r9,%rax,4), %esi
	addl	(%r8,%rax,4), %esi
	movl	%esi, (%r15,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB3_13
.LBB3_14:                               # %.lr.ph494.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB3_17
# BB#15:                                # %.lr.ph494.preheader1760.new
	movq	%rbx, %rcx
	subq	%rax, %rcx
	leaq	12(%r15,%rax,4), %rdx
	leaq	12(%r9,%rax,4), %rdi
	leaq	12(%r8,%rax,4), %rax
	.p2align	4, 0x90
.LBB3_16:                               # %.lr.ph494
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdi), %esi
	addl	-12(%rax), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rdi), %esi
	addl	-8(%rax), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rdi), %esi
	addl	-4(%rax), %esi
	movl	%esi, -4(%rdx)
	movl	(%rdi), %esi
	addl	(%rax), %esi
	movl	%esi, (%rdx)
	addq	$16, %rdx
	addq	$16, %rdi
	addq	$16, %rax
	addq	$-4, %rcx
	jne	.LBB3_16
.LBB3_17:                               # %._crit_edge495
	movq	%rbx, %rdx
	testl	%r11d, %r11d
	movq	%r11, %rax
	jle	.LBB3_418
# BB#18:                                # %.lr.ph490.preheader
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movq	8(%rcx), %rbx
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	8(%rcx), %r14
	movq	40(%rcx), %r11
	cmpl	$8, %eax
	movq	%rax, %r10
	jae	.LBB3_20
# BB#19:
	xorl	%ecx, %ecx
	movq	%rdx, %r12
	jmp	.LBB3_28
.LBB3_20:                               # %min.iters.checked852
	movl	%r10d, %ebp
	andl	$7, %ebp
	movq	%rdx, %r12
	movq	%r12, %r13
	subq	%rbp, %r13
	je	.LBB3_26
# BB#21:                                # %vector.memcheck871
	leaq	(%rbx,%r12,4), %rcx
	leaq	(%r14,%r12,4), %rdx
	movq	%r12, %rdi
	leaq	(%r11,%r12,4), %rsi
	cmpq	%rdx, %rbx
	sbbb	%dl, %dl
	cmpq	%rcx, %r14
	sbbb	%al, %al
	andb	%dl, %al
	cmpq	%rsi, %rbx
	sbbb	%dl, %dl
	cmpq	%rcx, %r11
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %al
	jne	.LBB3_27
# BB#22:                                # %vector.memcheck871
	andb	%sil, %dl
	andb	$1, %dl
	movq	%rdi, %r12
	jne	.LBB3_28
# BB#23:                                # %vector.body848.preheader
	leaq	16(%r14), %rcx
	leaq	16(%r11), %rdi
	leaq	16(%rbx), %rsi
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB3_24:                               # %vector.body848
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rdi), %xmm2
	movdqu	(%rdi), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-8, %rdx
	jne	.LBB3_24
# BB#25:                                # %middle.block849
	testl	%ebp, %ebp
	movq	%r13, %rcx
	jne	.LBB3_28
	jmp	.LBB3_34
.LBB3_26:
	xorl	%ecx, %ecx
	jmp	.LBB3_28
.LBB3_27:
	movq	%rdi, %r12
.LBB3_28:                               # %.lr.ph490.preheader1759
	movl	%r12d, %edx
	subl	%ecx, %edx
	leaq	-1(%r12), %rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_31
# BB#29:                                # %.lr.ph490.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB3_30:                               # %.lr.ph490.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r11,%rcx,4), %esi
	addl	(%r14,%rcx,4), %esi
	movl	%esi, (%rbx,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_30
.LBB3_31:                               # %.lr.ph490.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_34
# BB#32:                                # %.lr.ph490.preheader1759.new
	movq	%r12, %rdx
	subq	%rcx, %rdx
	leaq	12(%rbx,%rcx,4), %rdi
	leaq	12(%r11,%rcx,4), %rbp
	leaq	12(%r14,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_33:                               # %.lr.ph490
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_33
.LBB3_34:                               # %._crit_edge491
	movq	%r12, %rsi
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	testl	%r10d, %r10d
	movq	%r10, %rax
	jle	.LBB3_418
# BB#35:                                # %.lr.ph486.preheader
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movq	16(%rcx), %r12
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	16(%rcx), %rbx
	movq	48(%rcx), %rbp
	cmpl	$8, %eax
	movq	%rax, %r13
	movq	%rbp, -80(%rsp)         # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, -128(%rsp)        # 8-byte Spill
	jae	.LBB3_37
# BB#36:
	xorl	%ecx, %ecx
	movq	%rsi, %r10
	jmp	.LBB3_45
.LBB3_37:                               # %min.iters.checked889
	movl	%r13d, %edx
	andl	$7, %edx
	movq	%rsi, %r10
	movq	%r10, %rdi
	subq	%rdx, %rdi
	je	.LBB3_43
# BB#38:                                # %vector.memcheck908
	leaq	(%r12,%r10,4), %rcx
	leaq	(%rbx,%r10,4), %rax
	leaq	(%rbp,%r10,4), %rsi
	cmpq	%rax, %r12
	sbbb	%al, %al
	cmpq	%rcx, %rbx
	movq	%rbp, %r10
	movq	%r12, %rbp
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%rsi, %rbp
	sbbb	%al, %al
	cmpq	%rcx, %r10
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %bl
	jne	.LBB3_44
# BB#39:                                # %vector.memcheck908
	andb	%sil, %al
	andb	$1, %al
	movq	-128(%rsp), %rbx        # 8-byte Reload
	movq	%rbx, %r10
	movq	40(%rsp), %rbx          # 8-byte Reload
	jne	.LBB3_45
# BB#40:                                # %vector.body885.preheader
	leaq	16(%rbx), %rcx
	movq	-80(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rbp
	leaq	16(%r12), %rsi
	movq	%rdi, %rax
	.p2align	4, 0x90
.LBB3_41:                               # %vector.body885
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rax
	jne	.LBB3_41
# BB#42:                                # %middle.block886
	testl	%edx, %edx
	movq	%rdi, %rcx
	jne	.LBB3_45
	jmp	.LBB3_51
.LBB3_43:
	xorl	%ecx, %ecx
	jmp	.LBB3_45
.LBB3_44:
	movq	-128(%rsp), %rbx        # 8-byte Reload
	movq	%rbx, %r10
	movq	40(%rsp), %rbx          # 8-byte Reload
.LBB3_45:                               # %.lr.ph486.preheader1758
	movl	%r10d, %edx
	subl	%ecx, %edx
	leaq	-1(%r10), %rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_48
# BB#46:                                # %.lr.ph486.prol.preheader
	negq	%rdx
	movq	-80(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_47:                               # %.lr.ph486.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	addl	(%rbx,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_47
.LBB3_48:                               # %.lr.ph486.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_51
# BB#49:                                # %.lr.ph486.preheader1758.new
	movq	%r10, %rdx
	subq	%rcx, %rdx
	leaq	12(%r12,%rcx,4), %rdi
	movq	-80(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rbp
	leaq	12(%rbx,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_50:                               # %.lr.ph486
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_50
.LBB3_51:                               # %._crit_edge487
	testl	%r13d, %r13d
	jle	.LBB3_418
# BB#52:                                # %.lr.ph482.preheader
	movq	%r12, -96(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rax        # 8-byte Reload
	movq	24(%rax), %r10
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	24(%rax), %rbp
	movq	56(%rax), %rbx
	cmpl	$8, %r13d
	movq	%rbp, -16(%rsp)         # 8-byte Spill
	movq	%r13, -112(%rsp)        # 8-byte Spill
	jae	.LBB3_54
# BB#53:
	xorl	%ecx, %ecx
	jmp	.LBB3_62
.LBB3_54:                               # %min.iters.checked926
	movl	%r13d, %r12d
	andl	$7, %r12d
	movq	-128(%rsp), %rax        # 8-byte Reload
	movq	%rax, %r13
	subq	%r12, %r13
	je	.LBB3_60
# BB#55:                                # %vector.memcheck945
	leaq	(%r10,%rax,4), %rcx
	leaq	(%rbp,%rax,4), %rsi
	leaq	(%rbx,%rax,4), %rdi
	cmpq	%rsi, %r10
	movq	%r10, %rsi
	sbbb	%dl, %dl
	cmpq	%rcx, %rbp
	sbbb	%al, %al
	andb	%dl, %al
	movq	%rsi, %rbp
	cmpq	%rdi, %rsi
	sbbb	%dl, %dl
	cmpq	%rcx, %rbx
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %al
	jne	.LBB3_61
# BB#56:                                # %vector.memcheck945
	andb	%sil, %dl
	andb	$1, %dl
	movq	%rbp, %r10
	jne	.LBB3_62
# BB#57:                                # %vector.body922.preheader
	movq	-16(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rcx
	leaq	16(%rbx), %rbp
	leaq	16(%r10), %rsi
	movq	%r13, %rdi
	.p2align	4, 0x90
.LBB3_58:                               # %vector.body922
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB3_58
# BB#59:                                # %middle.block923
	testl	%r12d, %r12d
	movq	%r13, %rcx
	jne	.LBB3_62
	jmp	.LBB3_68
.LBB3_60:
	xorl	%ecx, %ecx
	jmp	.LBB3_62
.LBB3_61:
	movq	%rbp, %r10
.LBB3_62:                               # %.lr.ph482.preheader1757
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_65
# BB#63:                                # %.lr.ph482.prol.preheader
	negq	%rdx
	movq	-16(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_64:                               # %.lr.ph482.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %esi
	addl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r10,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_64
.LBB3_65:                               # %.lr.ph482.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_68
# BB#66:                                # %.lr.ph482.preheader1757.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	leaq	12(%r10,%rcx,4), %rdi
	leaq	12(%rbx,%rcx,4), %rbp
	movq	-16(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_67:                               # %.lr.ph482
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_67
.LBB3_68:                               # %._crit_edge483
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	-112(%rsp), %rax        # 8-byte Reload
	testl	%eax, %eax
	movq	-96(%rsp), %r12         # 8-byte Reload
	jle	.LBB3_418
# BB#69:                                # %.lr.ph478.preheader
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movq	64(%rcx), %rsi
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	64(%rcx), %rbx
	movq	96(%rcx), %rbp
	cmpl	$8, %eax
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	movq	%rbx, -48(%rsp)         # 8-byte Spill
	movq	%r10, -8(%rsp)          # 8-byte Spill
	jae	.LBB3_71
# BB#70:
	xorl	%ecx, %ecx
	jmp	.LBB3_80
.LBB3_71:                               # %min.iters.checked963
	movl	%eax, %edx
	andl	$7, %edx
	movq	-128(%rsp), %rbx        # 8-byte Reload
	movq	%rbx, %r13
	subq	%rdx, %r13
	je	.LBB3_77
# BB#72:                                # %vector.memcheck982
	leaq	(%rsi,%rbx,4), %rcx
	movq	-48(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rbx,4), %rax
	leaq	(%rbp,%rbx,4), %r12
	cmpq	%rax, %rsi
	sbbb	%al, %al
	cmpq	%rcx, %rdi
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r12, %rsi
	sbbb	%al, %al
	cmpq	%rcx, %rbp
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %bl
	jne	.LBB3_78
# BB#73:                                # %vector.memcheck982
	andb	%sil, %al
	andb	$1, %al
	movq	-96(%rsp), %r12         # 8-byte Reload
	movq	-48(%rsp), %rbx         # 8-byte Reload
	jne	.LBB3_80
# BB#74:                                # %vector.body959.preheader
	leaq	16(%rbx), %rcx
	movq	-88(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rbp
	movq	-24(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rsi
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB3_75:                               # %vector.body959
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rax
	jne	.LBB3_75
# BB#76:                                # %middle.block960
	testl	%edx, %edx
	movq	%r13, %rcx
	jne	.LBB3_80
	jmp	.LBB3_86
.LBB3_77:
	xorl	%ecx, %ecx
	jmp	.LBB3_79
.LBB3_78:
	movq	-96(%rsp), %r12         # 8-byte Reload
.LBB3_79:                               # %.lr.ph478.preheader1756
	movq	-48(%rsp), %rbx         # 8-byte Reload
.LBB3_80:                               # %.lr.ph478.preheader1756
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_83
# BB#81:                                # %.lr.ph478.prol.preheader
	negq	%rdx
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movq	-88(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_82:                               # %.lr.ph478.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx,4), %esi
	addl	(%rbx,%rcx,4), %esi
	movl	%esi, (%rdi,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_82
.LBB3_83:                               # %.lr.ph478.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_86
# BB#84:                                # %.lr.ph478.preheader1756.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	movq	-24(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rdi
	movq	-88(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rbp
	leaq	12(%rbx,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_85:                               # %.lr.ph478
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_85
.LBB3_86:                               # %._crit_edge479
	movq	-112(%rsp), %rax        # 8-byte Reload
	testl	%eax, %eax
	movq	%rax, %r13
	jle	.LBB3_418
# BB#87:                                # %.lr.ph474.preheader
	movq	-104(%rsp), %rax        # 8-byte Reload
	movq	72(%rax), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	72(%rcx), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movq	104(%rcx), %rbx
	cmpl	$8, %r13d
	jae	.LBB3_89
# BB#88:
	xorl	%ecx, %ecx
	jmp	.LBB3_97
.LBB3_89:                               # %min.iters.checked1000
	movl	%r13d, %edi
	andl	$7, %edi
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movq	%rsi, %r10
	subq	%rdi, %r10
	je	.LBB3_95
# BB#90:                                # %vector.memcheck1019
	movq	-56(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,4), %rcx
	movq	-32(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi,4), %rax
	leaq	(%rbx,%rsi,4), %rsi
	cmpq	%rax, %rbp
	sbbb	%al, %al
	cmpq	%rcx, %rdx
	sbbb	%dl, %dl
	andb	%al, %dl
	cmpq	%rsi, %rbp
	sbbb	%al, %al
	cmpq	%rcx, %rbx
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_96
# BB#91:                                # %vector.memcheck1019
	andb	%sil, %al
	andb	$1, %al
	movq	-96(%rsp), %r12         # 8-byte Reload
	jne	.LBB3_97
# BB#92:                                # %vector.body996.preheader
	movq	-32(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rcx
	leaq	16(%rbx), %rbp
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rsi
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB3_93:                               # %vector.body996
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rax
	jne	.LBB3_93
# BB#94:                                # %middle.block997
	testl	%edi, %edi
	movq	%r10, %rcx
	jne	.LBB3_97
	jmp	.LBB3_103
.LBB3_95:
	xorl	%ecx, %ecx
	jmp	.LBB3_97
.LBB3_96:
	movq	-96(%rsp), %r12         # 8-byte Reload
.LBB3_97:                               # %.lr.ph474.preheader1755
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_100
# BB#98:                                # %.lr.ph474.prol.preheader
	negq	%rdx
	movq	-56(%rsp), %rdi         # 8-byte Reload
	movq	-32(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_99:                               # %.lr.ph474.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %esi
	addl	(%rbp,%rcx,4), %esi
	movl	%esi, (%rdi,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_99
.LBB3_100:                              # %.lr.ph474.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_103
# BB#101:                               # %.lr.ph474.preheader1755.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rdi
	leaq	12(%rbx,%rcx,4), %rbp
	movq	-32(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_102:                              # %.lr.ph474
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_102
.LBB3_103:                              # %._crit_edge475
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	jle	.LBB3_418
# BB#104:                               # %.lr.ph470.preheader
	movq	-104(%rsp), %rax        # 8-byte Reload
	movq	80(%rax), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	80(%rcx), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movq	112(%rcx), %rbx
	cmpl	$8, %r13d
	jae	.LBB3_106
# BB#105:
	xorl	%ecx, %ecx
	jmp	.LBB3_114
.LBB3_106:                              # %min.iters.checked1037
	movl	%r13d, %edi
	andl	$7, %edi
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movq	%rsi, %r10
	subq	%rdi, %r10
	je	.LBB3_112
# BB#107:                               # %vector.memcheck1056
	movq	-64(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,4), %rcx
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi,4), %rax
	leaq	(%rbx,%rsi,4), %rsi
	cmpq	%rax, %rbp
	sbbb	%al, %al
	cmpq	%rcx, %rdx
	sbbb	%dl, %dl
	andb	%al, %dl
	cmpq	%rsi, %rbp
	sbbb	%al, %al
	cmpq	%rcx, %rbx
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_113
# BB#108:                               # %vector.memcheck1056
	andb	%sil, %al
	andb	$1, %al
	movq	-96(%rsp), %r12         # 8-byte Reload
	jne	.LBB3_114
# BB#109:                               # %vector.body1033.preheader
	movq	-40(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rcx
	leaq	16(%rbx), %rbp
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rsi
	movq	%r10, %rax
.LBB3_110:                              # %vector.body1033
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rax
	jne	.LBB3_110
# BB#111:                               # %middle.block1034
	testl	%edi, %edi
	movq	%r10, %rcx
	jne	.LBB3_114
	jmp	.LBB3_120
.LBB3_112:
	xorl	%ecx, %ecx
	jmp	.LBB3_114
.LBB3_113:
	movq	-96(%rsp), %r12         # 8-byte Reload
.LBB3_114:                              # %.lr.ph470.preheader1754
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_117
# BB#115:                               # %.lr.ph470.prol.preheader
	negq	%rdx
	movq	-64(%rsp), %rdi         # 8-byte Reload
	movq	-40(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_116:                              # %.lr.ph470.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %esi
	addl	(%rbp,%rcx,4), %esi
	movl	%esi, (%rdi,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_116
.LBB3_117:                              # %.lr.ph470.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_120
# BB#118:                               # %.lr.ph470.preheader1754.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rdi
	leaq	12(%rbx,%rcx,4), %rbp
	movq	-40(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_119:                              # %.lr.ph470
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_119
.LBB3_120:                              # %._crit_edge471
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	testl	%r13d, %r13d
	jle	.LBB3_418
# BB#121:                               # %.lr.ph466.preheader
	movq	-104(%rsp), %rax        # 8-byte Reload
	movq	88(%rax), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	88(%rax), %rcx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	movq	120(%rax), %rbx
	cmpl	$8, %r13d
	jae	.LBB3_123
# BB#122:
	xorl	%ecx, %ecx
	jmp	.LBB3_131
.LBB3_123:                              # %min.iters.checked1074
	movl	%r13d, %edi
	andl	$7, %edi
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movq	%rsi, %r10
	subq	%rdi, %r10
	je	.LBB3_129
# BB#124:                               # %vector.memcheck1093
	movq	-72(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,4), %rcx
	movq	-104(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi,4), %rax
	leaq	(%rbx,%rsi,4), %rsi
	cmpq	%rax, %rbp
	sbbb	%al, %al
	cmpq	%rcx, %rdx
	sbbb	%dl, %dl
	andb	%al, %dl
	cmpq	%rsi, %rbp
	sbbb	%al, %al
	cmpq	%rcx, %rbx
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_130
# BB#125:                               # %vector.memcheck1093
	andb	%sil, %al
	andb	$1, %al
	movq	-96(%rsp), %r12         # 8-byte Reload
	jne	.LBB3_131
# BB#126:                               # %vector.body1070.preheader
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	16(%rax), %rcx
	leaq	16(%rbx), %rbp
	movq	-72(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rsi
	movq	%r10, %rax
.LBB3_127:                              # %vector.body1070
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rax
	jne	.LBB3_127
# BB#128:                               # %middle.block1071
	testl	%edi, %edi
	movq	%r10, %rcx
	jne	.LBB3_131
	jmp	.LBB3_137
.LBB3_129:
	xorl	%ecx, %ecx
	jmp	.LBB3_131
.LBB3_130:
	movq	-96(%rsp), %r12         # 8-byte Reload
.LBB3_131:                              # %.lr.ph466.preheader1753
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_134
# BB#132:                               # %.lr.ph466.prol.preheader
	negq	%rdx
	movq	-72(%rsp), %rdi         # 8-byte Reload
	movq	-104(%rsp), %rbp        # 8-byte Reload
	.p2align	4, 0x90
.LBB3_133:                              # %.lr.ph466.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %esi
	addl	(%rbp,%rcx,4), %esi
	movl	%esi, (%rdi,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_133
.LBB3_134:                              # %.lr.ph466.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_137
# BB#135:                               # %.lr.ph466.preheader1753.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	movq	-72(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rdi
	leaq	12(%rbx,%rcx,4), %rbp
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_136:                              # %.lr.ph466
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_136
.LBB3_137:                              # %._crit_edge467
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	testl	%r13d, %r13d
	movq	%r13, %rax
	jle	.LBB3_418
# BB#138:                               # %.lr.ph462.preheader
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	40(%rcx), %rcx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movq	(%rcx), %r13
	cmpl	$8, %eax
	movq	%rax, %r10
	jae	.LBB3_140
# BB#139:
	xorl	%ecx, %ecx
	jmp	.LBB3_148
.LBB3_140:                              # %min.iters.checked1111
	movl	%r10d, %edx
	andl	$7, %edx
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	je	.LBB3_146
# BB#141:                               # %vector.memcheck1130
	leaq	(%r13,%rsi,4), %rcx
	leaq	(%r8,%rsi,4), %rax
	leaq	(%r14,%rsi,4), %rsi
	cmpq	%rax, %r13
	sbbb	%al, %al
	cmpq	%rcx, %r8
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%rsi, %r13
	sbbb	%al, %al
	cmpq	%rcx, %r14
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %bl
	jne	.LBB3_147
# BB#142:                               # %vector.memcheck1130
	andb	%sil, %al
	andb	$1, %al
	movq	-96(%rsp), %r12         # 8-byte Reload
	jne	.LBB3_148
# BB#143:                               # %vector.body1107.preheader
	leaq	16(%r8), %rcx
	leaq	16(%r14), %rbp
	leaq	16(%r13), %rsi
	movq	%rdi, %rax
.LBB3_144:                              # %vector.body1107
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rax
	jne	.LBB3_144
# BB#145:                               # %middle.block1108
	testl	%edx, %edx
	movq	%rdi, %rcx
	jne	.LBB3_148
	jmp	.LBB3_154
.LBB3_146:
	xorl	%ecx, %ecx
	jmp	.LBB3_148
.LBB3_147:
	movq	-96(%rsp), %r12         # 8-byte Reload
.LBB3_148:                              # %.lr.ph462.preheader1752
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_151
# BB#149:                               # %.lr.ph462.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB3_150:                              # %.lr.ph462.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r13,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_150
.LBB3_151:                              # %.lr.ph462.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_154
# BB#152:                               # %.lr.ph462.preheader1752.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	leaq	12(%r13,%rcx,4), %rdi
	leaq	12(%r14,%rcx,4), %rbp
	leaq	12(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_153:                              # %.lr.ph462
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_153
.LBB3_154:                              # %._crit_edge463
	testl	%r10d, %r10d
	movq	%r10, %r13
	jle	.LBB3_418
# BB#155:                               # %.lr.ph458.preheader
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	16(%rax), %r8
	cmpl	$8, %r13d
	movq	%r13, %r14
	jae	.LBB3_157
# BB#156:
	xorl	%ecx, %ecx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	-16(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB3_165
.LBB3_157:                              # %min.iters.checked1148
	movl	%r14d, %edi
	andl	$7, %edi
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movq	%rsi, %r10
	subq	%rdi, %r10
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	-16(%rsp), %rbp         # 8-byte Reload
	je	.LBB3_163
# BB#158:                               # %vector.memcheck1167
	movq	%r12, %r13
	leaq	(%r8,%rsi,4), %rcx
	leaq	(%rbx,%rsi,4), %rax
	leaq	(%rbp,%rsi,4), %rsi
	cmpq	%rax, %r8
	sbbb	%al, %al
	cmpq	%rcx, %rbx
	sbbb	%dl, %dl
	andb	%al, %dl
	cmpq	%rsi, %r8
	sbbb	%al, %al
	cmpq	%rcx, %rbp
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_164
# BB#159:                               # %vector.memcheck1167
	andb	%sil, %al
	andb	$1, %al
	movq	%r13, %r12
	jne	.LBB3_165
# BB#160:                               # %vector.body1144.preheader
	leaq	16(%rbx), %rcx
	leaq	16(%rbp), %rbp
	leaq	16(%r8), %rsi
	movq	%r10, %rax
.LBB3_161:                              # %vector.body1144
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rax
	jne	.LBB3_161
# BB#162:                               # %middle.block1145
	testl	%edi, %edi
	movq	%r10, %rcx
	movq	-16(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_165
	jmp	.LBB3_171
.LBB3_163:
	xorl	%ecx, %ecx
	jmp	.LBB3_165
.LBB3_164:
	movq	%r13, %r12
.LBB3_165:                              # %.lr.ph458.preheader1751
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_168
# BB#166:                               # %.lr.ph458.prol.preheader
	negq	%rdx
.LBB3_167:                              # %.lr.ph458.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx,4), %esi
	addl	(%rbx,%rcx,4), %esi
	movl	%esi, (%r8,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_167
.LBB3_168:                              # %.lr.ph458.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_171
# BB#169:                               # %.lr.ph458.preheader1751.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	leaq	12(%r8,%rcx,4), %rdi
	leaq	12(%rbp,%rcx,4), %rbp
	leaq	12(%rbx,%rcx,4), %rcx
.LBB3_170:                              # %.lr.ph458
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_170
.LBB3_171:                              # %._crit_edge459
	testl	%r14d, %r14d
	jle	.LBB3_418
# BB#172:                               # %.lr.ph454.preheader
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	32(%rax), %r8
	cmpl	$8, %r14d
	movq	%r14, %r10
	jae	.LBB3_174
# BB#173:
	xorl	%ecx, %ecx
	jmp	.LBB3_182
.LBB3_174:                              # %min.iters.checked1185
	movl	%r10d, %edx
	andl	$7, %edx
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	je	.LBB3_180
# BB#175:                               # %vector.memcheck1204
	movq	%r12, %r14
	leaq	(%r8,%rsi,4), %rcx
	leaq	(%r9,%rsi,4), %rax
	leaq	(%r11,%rsi,4), %rsi
	cmpq	%rax, %r8
	sbbb	%al, %al
	cmpq	%rcx, %r9
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%rsi, %r8
	sbbb	%al, %al
	cmpq	%rcx, %r11
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %bl
	jne	.LBB3_181
# BB#176:                               # %vector.memcheck1204
	andb	%sil, %al
	andb	$1, %al
	movq	%r14, %r12
	jne	.LBB3_182
# BB#177:                               # %vector.body1181.preheader
	leaq	16(%r9), %rcx
	leaq	16(%r11), %rbp
	leaq	16(%r8), %rsi
	movq	%rdi, %rax
.LBB3_178:                              # %vector.body1181
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rax
	jne	.LBB3_178
# BB#179:                               # %middle.block1182
	testl	%edx, %edx
	movq	%rdi, %rcx
	jne	.LBB3_182
	jmp	.LBB3_188
.LBB3_180:
	xorl	%ecx, %ecx
	jmp	.LBB3_182
.LBB3_181:
	movq	%r14, %r12
.LBB3_182:                              # %.lr.ph454.preheader1750
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edx
	subl	%ecx, %edx
	decq	%rax
	subq	%rcx, %rax
	andq	$3, %rdx
	je	.LBB3_185
# BB#183:                               # %.lr.ph454.prol.preheader
	negq	%rdx
.LBB3_184:                              # %.lr.ph454.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r11,%rcx,4), %esi
	addl	(%r9,%rcx,4), %esi
	movl	%esi, (%r8,%rcx,4)
	incq	%rcx
	incq	%rdx
	jne	.LBB3_184
.LBB3_185:                              # %.lr.ph454.prol.loopexit
	cmpq	$3, %rax
	jb	.LBB3_188
# BB#186:                               # %.lr.ph454.preheader1750.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	leaq	12(%r8,%rcx,4), %rdi
	leaq	12(%r11,%rcx,4), %rax
	leaq	12(%r9,%rcx,4), %rcx
.LBB3_187:                              # %.lr.ph454
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rax), %esi
	addl	-12(%rcx), %esi
	movl	%esi, -12(%rdi)
	movl	-8(%rax), %esi
	addl	-8(%rcx), %esi
	movl	%esi, -8(%rdi)
	movl	-4(%rax), %esi
	addl	-4(%rcx), %esi
	movl	%esi, -4(%rdi)
	movl	(%rax), %esi
	addl	(%rcx), %esi
	movl	%esi, (%rdi)
	addq	$16, %rdi
	addq	$16, %rax
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_187
.LBB3_188:                              # %._crit_edge455
	testl	%r10d, %r10d
	jle	.LBB3_418
# BB#189:                               # %.lr.ph450.preheader
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	48(%rax), %rax
	cmpl	$8, %r10d
	jb	.LBB3_196
# BB#190:                               # %min.iters.checked1222
	movl	%r10d, %r9d
	andl	$7, %r9d
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movq	%rdx, %r8
	subq	%r9, %r8
	je	.LBB3_196
# BB#191:                               # %vector.memcheck1241
	movq	%r12, %r11
	leaq	(%rax,%rdx,4), %rcx
	movq	-80(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdx,4), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rdx,4), %r12
	cmpq	%rsi, %rax
	sbbb	%bl, %bl
	cmpq	%rcx, %rbp
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%r12, %rax
	sbbb	%bl, %bl
	cmpq	%rcx, %rdi
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_214
# BB#192:                               # %vector.memcheck1241
	andb	%sil, %bl
	andb	$1, %bl
	movq	%r11, %r12
	movq	-80(%rsp), %rbp         # 8-byte Reload
	movq	-48(%rsp), %rbx         # 8-byte Reload
	jne	.LBB3_198
# BB#193:                               # %vector.body1218.preheader
	leaq	16(%rbp), %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %rbp
	leaq	16(%rax), %rsi
	movq	%r8, %rdi
.LBB3_194:                              # %vector.body1218
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB3_194
# BB#195:                               # %middle.block1219
	testl	%r9d, %r9d
	movq	%r8, %rcx
	movq	-80(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_198
	jmp	.LBB3_204
.LBB3_196:
	xorl	%ecx, %ecx
.LBB3_197:                              # %.lr.ph450.preheader1749
	movq	-80(%rsp), %rbp         # 8-byte Reload
.LBB3_198:                              # %.lr.ph450.preheader1749
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movl	%edx, %esi
	subl	%ecx, %esi
	decq	%rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB3_201
# BB#199:                               # %.lr.ph450.prol.preheader
	negq	%rsi
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB3_200:                              # %.lr.ph450.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %edi
	addl	(%rbp,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_200
.LBB3_201:                              # %.lr.ph450.prol.loopexit
	cmpq	$3, %rdx
	movq	-48(%rsp), %rbx         # 8-byte Reload
	jb	.LBB3_204
# BB#202:                               # %.lr.ph450.preheader1749.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	leaq	12(%rax,%rcx,4), %rax
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	12(%rsi,%rcx,4), %rdi
	leaq	12(%rbp,%rcx,4), %rcx
.LBB3_203:                              # %.lr.ph450
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdi), %esi
	addl	-12(%rcx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdi), %esi
	addl	-8(%rcx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdi), %esi
	addl	-4(%rcx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdi), %esi
	addl	(%rcx), %esi
	movl	%esi, (%rax)
	addq	$16, %rax
	addq	$16, %rdi
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_203
.LBB3_204:                              # %._crit_edge451
	testl	%r10d, %r10d
	jle	.LBB3_418
# BB#205:                               # %.lr.ph446.preheader
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	64(%rax), %rax
	cmpl	$8, %r10d
	jae	.LBB3_207
# BB#206:
	xorl	%ecx, %ecx
	movq	-32(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB3_216
.LBB3_207:                              # %min.iters.checked1259
	movl	%r10d, %r9d
	andl	$7, %r9d
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movq	%rdx, %r8
	subq	%r9, %r8
	movq	-32(%rsp), %rbp         # 8-byte Reload
	je	.LBB3_213
# BB#208:                               # %vector.memcheck1278
	movq	%r12, %r11
	leaq	(%rax,%rdx,4), %rcx
	leaq	(%rbx,%rdx,4), %rsi
	leaq	(%rbp,%rdx,4), %rdi
	cmpq	%rsi, %rax
	movq	%rbx, %rdx
	sbbb	%bl, %bl
	cmpq	%rcx, %rdx
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rdi, %rax
	sbbb	%bl, %bl
	cmpq	%rcx, %rbp
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_215
# BB#209:                               # %vector.memcheck1278
	andb	%sil, %bl
	andb	$1, %bl
	movq	%r11, %r12
	movq	-48(%rsp), %rbx         # 8-byte Reload
	jne	.LBB3_216
# BB#210:                               # %vector.body1255.preheader
	leaq	16(%rbx), %rcx
	leaq	16(%rbp), %rbp
	leaq	16(%rax), %rsi
	movq	%r8, %rdi
.LBB3_211:                              # %vector.body1255
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB3_211
# BB#212:                               # %middle.block1256
	testl	%r9d, %r9d
	movq	%r8, %rcx
	movq	-32(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_216
	jmp	.LBB3_222
.LBB3_213:
	xorl	%ecx, %ecx
	jmp	.LBB3_216
.LBB3_214:
	movq	%r11, %r12
	jmp	.LBB3_197
.LBB3_215:
	movq	%r11, %r12
	movq	-48(%rsp), %rbx         # 8-byte Reload
.LBB3_216:                              # %.lr.ph446.preheader1748
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movl	%edx, %esi
	subl	%ecx, %esi
	decq	%rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB3_219
# BB#217:                               # %.lr.ph446.prol.preheader
	negq	%rsi
.LBB3_218:                              # %.lr.ph446.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx,4), %edi
	addl	(%rbx,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_218
.LBB3_219:                              # %.lr.ph446.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_222
# BB#220:                               # %.lr.ph446.preheader1748.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	leaq	12(%rax,%rcx,4), %rax
	leaq	12(%rbp,%rcx,4), %rdi
	leaq	12(%rbx,%rcx,4), %rcx
.LBB3_221:                              # %.lr.ph446
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdi), %esi
	addl	-12(%rcx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdi), %esi
	addl	-8(%rcx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdi), %esi
	addl	-4(%rcx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdi), %esi
	addl	(%rcx), %esi
	movl	%esi, (%rax)
	addq	$16, %rax
	addq	$16, %rdi
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_221
.LBB3_222:                              # %._crit_edge447
	testl	%r10d, %r10d
	jle	.LBB3_418
# BB#223:                               # %.lr.ph442.preheader
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	80(%rax), %rax
	cmpl	$8, %r10d
	jae	.LBB3_225
# BB#224:
	xorl	%ecx, %ecx
	movq	-40(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB3_233
.LBB3_225:                              # %min.iters.checked1296
	movl	%r10d, %r9d
	andl	$7, %r9d
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movq	%rdx, %r8
	subq	%r9, %r8
	movq	-40(%rsp), %rbp         # 8-byte Reload
	je	.LBB3_231
# BB#226:                               # %vector.memcheck1315
	movq	%r12, %r11
	leaq	(%rax,%rdx,4), %rcx
	leaq	(%rbp,%rdx,4), %rsi
	movq	-104(%rsp), %rdi        # 8-byte Reload
	leaq	(%rdi,%rdx,4), %r14
	cmpq	%rsi, %rax
	sbbb	%bl, %bl
	cmpq	%rcx, %rbp
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%r14, %rax
	sbbb	%bl, %bl
	cmpq	%rcx, %rdi
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_232
# BB#227:                               # %vector.memcheck1315
	andb	%sil, %bl
	andb	$1, %bl
	movq	%r11, %r12
	jne	.LBB3_233
# BB#228:                               # %vector.body1292.preheader
	leaq	16(%rbp), %rcx
	movq	-104(%rsp), %rdx        # 8-byte Reload
	leaq	16(%rdx), %rbp
	leaq	16(%rax), %rsi
	movq	%r8, %rdi
.LBB3_229:                              # %vector.body1292
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB3_229
# BB#230:                               # %middle.block1293
	testl	%r9d, %r9d
	movq	%r8, %rcx
	movq	-40(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_233
	jmp	.LBB3_239
.LBB3_231:
	xorl	%ecx, %ecx
	jmp	.LBB3_233
.LBB3_232:
	movq	%r11, %r12
.LBB3_233:                              # %.lr.ph442.preheader1747
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movl	%edx, %esi
	subl	%ecx, %esi
	decq	%rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB3_236
# BB#234:                               # %.lr.ph442.prol.preheader
	negq	%rsi
	movq	-104(%rsp), %rbx        # 8-byte Reload
.LBB3_235:                              # %.lr.ph442.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %edi
	addl	(%rbp,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_235
.LBB3_236:                              # %.lr.ph442.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_239
# BB#237:                               # %.lr.ph442.preheader1747.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	leaq	12(%rax,%rcx,4), %rax
	movq	-104(%rsp), %rsi        # 8-byte Reload
	leaq	12(%rsi,%rcx,4), %rdi
	leaq	12(%rbp,%rcx,4), %rcx
.LBB3_238:                              # %.lr.ph442
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdi), %esi
	addl	-12(%rcx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdi), %esi
	addl	-8(%rcx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdi), %esi
	addl	-4(%rcx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdi), %esi
	addl	(%rcx), %esi
	movl	%esi, (%rax)
	addq	$16, %rax
	addq	$16, %rdi
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_238
.LBB3_239:                              # %._crit_edge443
	testl	%r10d, %r10d
	jle	.LBB3_418
# BB#240:                               # %.lr.ph438.preheader
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	96(%rax), %rax
	cmpl	$8, %r10d
	jb	.LBB3_247
# BB#241:                               # %min.iters.checked1333
	movl	%r10d, %r9d
	andl	$7, %r9d
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movq	%rdx, %r8
	subq	%r9, %r8
	je	.LBB3_247
# BB#242:                               # %vector.memcheck1352
	movq	%r12, %r11
	leaq	(%rax,%rdx,4), %rcx
	movq	-88(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdx,4), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rdx,4), %r12
	cmpq	%rsi, %rax
	sbbb	%bl, %bl
	cmpq	%rcx, %rbp
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%r12, %rax
	sbbb	%bl, %bl
	cmpq	%rcx, %rdi
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_265
# BB#243:                               # %vector.memcheck1352
	andb	%sil, %bl
	andb	$1, %bl
	movq	%r11, %r12
	movq	-88(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_249
# BB#244:                               # %vector.body1329.preheader
	leaq	16(%rbp), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	16(%rdx), %rbp
	leaq	16(%rax), %rsi
	movq	%r8, %rdi
.LBB3_245:                              # %vector.body1329
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB3_245
# BB#246:                               # %middle.block1330
	testl	%r9d, %r9d
	movq	%r8, %rcx
	movq	-88(%rsp), %rbp         # 8-byte Reload
	jne	.LBB3_249
	jmp	.LBB3_255
.LBB3_247:
	xorl	%ecx, %ecx
.LBB3_248:                              # %.lr.ph438.preheader1746
	movq	-88(%rsp), %rbp         # 8-byte Reload
.LBB3_249:                              # %.lr.ph438.preheader1746
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movl	%edx, %esi
	subl	%ecx, %esi
	decq	%rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB3_252
# BB#250:                               # %.lr.ph438.prol.preheader
	negq	%rsi
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB3_251:                              # %.lr.ph438.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %edi
	addl	(%rbp,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_251
.LBB3_252:                              # %.lr.ph438.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_255
# BB#253:                               # %.lr.ph438.preheader1746.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	leaq	12(%rax,%rcx,4), %rax
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	12(%rsi,%rcx,4), %rdi
	leaq	12(%rbp,%rcx,4), %rcx
.LBB3_254:                              # %.lr.ph438
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdi), %esi
	addl	-12(%rcx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdi), %esi
	addl	-8(%rcx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdi), %esi
	addl	-4(%rcx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdi), %esi
	addl	(%rcx), %esi
	movl	%esi, (%rax)
	addq	$16, %rax
	addq	$16, %rdi
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_254
.LBB3_255:                              # %._crit_edge439
	testl	%r10d, %r10d
	jle	.LBB3_418
# BB#256:                               # %.lr.ph434.preheader
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	112(%rax), %rax
	cmpl	$8, %r10d
	jae	.LBB3_258
# BB#257:
	xorl	%ecx, %ecx
	jmp	.LBB3_267
.LBB3_258:                              # %min.iters.checked1370
	movl	%r10d, %r9d
	andl	$7, %r9d
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movq	%rdx, %r8
	subq	%r9, %r8
	je	.LBB3_264
# BB#259:                               # %vector.memcheck1389
	movq	%r12, %r11
	leaq	(%rax,%rdx,4), %rcx
	movq	16(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdx,4), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	leaq	(%rdi,%rdx,4), %r12
	cmpq	%rsi, %rax
	sbbb	%bl, %bl
	cmpq	%rcx, %rbp
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%r12, %rax
	sbbb	%bl, %bl
	cmpq	%rcx, %rdi
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_266
# BB#260:                               # %vector.memcheck1389
	andb	%sil, %bl
	andb	$1, %bl
	movq	%r11, %r12
	jne	.LBB3_267
# BB#261:                               # %vector.body1366.preheader
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	16(%rcx), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	16(%rdx), %rbp
	leaq	16(%rax), %rsi
	movq	%r8, %rdi
.LBB3_262:                              # %vector.body1366
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB3_262
# BB#263:                               # %middle.block1367
	testl	%r9d, %r9d
	movq	%r8, %rcx
	jne	.LBB3_267
	jmp	.LBB3_273
.LBB3_264:
	xorl	%ecx, %ecx
	jmp	.LBB3_267
.LBB3_265:
	movq	%r11, %r12
	jmp	.LBB3_248
.LBB3_266:
	movq	%r11, %r12
.LBB3_267:                              # %.lr.ph434.preheader1745
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movl	%edx, %esi
	subl	%ecx, %esi
	decq	%rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB3_270
# BB#268:                               # %.lr.ph434.prol.preheader
	negq	%rsi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB3_269:                              # %.lr.ph434.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rcx,4), %edi
	addl	(%rbp,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_269
.LBB3_270:                              # %.lr.ph434.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_273
# BB#271:                               # %.lr.ph434.preheader1745.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rcx, %rdx
	leaq	12(%rax,%rcx,4), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	12(%rsi,%rcx,4), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	12(%rsi,%rcx,4), %rcx
.LBB3_272:                              # %.lr.ph434
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdi), %esi
	addl	-12(%rcx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdi), %esi
	addl	-8(%rcx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdi), %esi
	addl	-4(%rcx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdi), %esi
	addl	(%rcx), %esi
	movl	%esi, (%rax)
	addq	$16, %rax
	addq	$16, %rdi
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_272
.LBB3_273:                              # %._crit_edge435
	testl	%r10d, %r10d
	jle	.LBB3_418
# BB#274:                               # %.lr.ph430.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	movq	32(%rax), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	(%rax), %r13
	cmpl	$8, %r10d
	movq	-8(%rsp), %r8           # 8-byte Reload
	movq	%r12, %r14
	jae	.LBB3_276
# BB#275:
	movq	%r10, %r11
	movq	-128(%rsp), %r10        # 8-byte Reload
	jmp	.LBB3_283
.LBB3_276:                              # %min.iters.checked1407
	movq	%r10, %r11
	movl	%r10d, %edx
	andl	$7, %edx
	movq	-128(%rsp), %rax        # 8-byte Reload
	movq	%rax, %r9
	subq	%rdx, %r9
	je	.LBB3_282
# BB#277:                               # %vector.memcheck1426
	leaq	(%r13,%rax,4), %rcx
	leaq	(%r15,%rax,4), %rsi
	movq	%rax, %r10
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rax,4), %rdi
	cmpq	%rsi, %r13
	sbbb	%bl, %bl
	cmpq	%rcx, %r15
	sbbb	%al, %al
	andb	%bl, %al
	cmpq	%rdi, %r13
	sbbb	%bl, %bl
	cmpq	%rcx, %rbp
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %al
	jne	.LBB3_284
# BB#278:                               # %vector.memcheck1426
	andb	%sil, %bl
	andb	$1, %bl
	jne	.LBB3_284
# BB#279:                               # %vector.body1403.preheader
	leaq	16(%r15), %rcx
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax), %rbp
	leaq	16(%r13), %rsi
	movq	%r9, %rdi
.LBB3_280:                              # %vector.body1403
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rsi)
	movdqu	%xmm3, (%rsi)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB3_280
# BB#281:                               # %middle.block1404
	testl	%edx, %edx
	movq	%r9, %rcx
	movq	-8(%rsp), %r8           # 8-byte Reload
	jne	.LBB3_284
	jmp	.LBB3_290
.LBB3_282:
	movq	%rax, %r10
.LBB3_283:                              # %.lr.ph430.preheader1744
	xorl	%ecx, %ecx
.LBB3_284:                              # %.lr.ph430.preheader1744
	movl	%r10d, %esi
	subl	%ecx, %esi
	leaq	-1(%r10), %rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB3_287
# BB#285:                               # %.lr.ph430.prol.preheader
	negq	%rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
.LBB3_286:                              # %.lr.ph430.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %eax
	addl	(%r15,%rcx,4), %eax
	movl	%eax, (%r13,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_286
.LBB3_287:                              # %.lr.ph430.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_290
# BB#288:                               # %.lr.ph430.preheader1744.new
	movq	%r10, %rdx
	subq	%rcx, %rdx
	leaq	12(%r13,%rcx,4), %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rbx
	leaq	12(%r15,%rcx,4), %rcx
.LBB3_289:                              # %.lr.ph430
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbx), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbx), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbx), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbx), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbx
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_289
.LBB3_290:                              # %._crit_edge431
	testl	%r11d, %r11d
	movq	%r10, %rdx
	jle	.LBB3_418
# BB#291:                               # %.lr.ph426.preheader
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	16(%rax), %rsi
	cmpl	$8, %r11d
	jb	.LBB3_298
# BB#292:                               # %min.iters.checked1444
	movq	-112(%rsp), %r10        # 8-byte Reload
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	andl	$7, %r10d
	movq	%rdx, %r9
	subq	%r10, %r9
	je	.LBB3_298
# BB#293:                               # %vector.memcheck1463
	leaq	(%rsi,%rdx,4), %rax
	leaq	(%r14,%rdx,4), %rcx
	movq	%rdx, %r11
	leaq	(%r8,%rdx,4), %rdi
	cmpq	%rcx, %rsi
	sbbb	%cl, %cl
	movq	%r14, %r12
	cmpq	%rax, %r14
	sbbb	%dl, %dl
	andb	%cl, %dl
	cmpq	%rdi, %rsi
	sbbb	%bl, %bl
	cmpq	%rax, %r8
	sbbb	%dil, %dil
	xorl	%ecx, %ecx
	testb	$1, %dl
	jne	.LBB3_330
# BB#294:                               # %vector.memcheck1463
	andb	%dil, %bl
	andb	$1, %bl
	movq	%r12, %r14
	jne	.LBB3_299
# BB#295:                               # %vector.body1440.preheader
	leaq	16(%r14), %rcx
	movq	-8(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax), %rbp
	leaq	16(%rsi), %rbx
	movq	%r9, %rdi
.LBB3_296:                              # %vector.body1440
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rbx)
	movdqu	%xmm3, (%rbx)
	addq	$32, %rcx
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$-8, %rdi
	jne	.LBB3_296
# BB#297:                               # %middle.block1441
	testl	%r10d, %r10d
	movq	%r9, %rcx
	movq	%r12, %r14
	jne	.LBB3_299
	jmp	.LBB3_305
.LBB3_298:
	movq	%rdx, %r11
	xorl	%ecx, %ecx
.LBB3_299:                              # %.lr.ph426.preheader1743
	movl	%r11d, %edi
	subl	%ecx, %edi
	leaq	-1(%r11), %rdx
	subq	%rcx, %rdx
	andq	$3, %rdi
	je	.LBB3_302
# BB#300:                               # %.lr.ph426.prol.preheader
	negq	%rdi
	movq	-8(%rsp), %rbp          # 8-byte Reload
.LBB3_301:                              # %.lr.ph426.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rcx,4), %eax
	addl	(%r14,%rcx,4), %eax
	movl	%eax, (%rsi,%rcx,4)
	incq	%rcx
	incq	%rdi
	jne	.LBB3_301
.LBB3_302:                              # %.lr.ph426.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_305
# BB#303:                               # %.lr.ph426.preheader1743.new
	movq	%r11, %rdx
	subq	%rcx, %rdx
	leaq	12(%rsi,%rcx,4), %rdi
	movq	-8(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax,%rcx,4), %rbx
	leaq	12(%r14,%rcx,4), %rcx
.LBB3_304:                              # %.lr.ph426
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbx), %eax
	addl	-12(%rcx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbx), %eax
	addl	-8(%rcx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbx), %eax
	addl	-4(%rcx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbx), %eax
	addl	(%rcx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbx
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_304
.LBB3_305:                              # %._crit_edge427
	movq	-112(%rsp), %rdx        # 8-byte Reload
	testl	%edx, %edx
	movq	%r11, %rax
	jle	.LBB3_418
# BB#306:                               # %.lr.ph422.preheader
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	64(%rcx), %r15
	cmpl	$8, %edx
	jae	.LBB3_308
# BB#307:
	movq	%rax, %r11
	jmp	.LBB3_314
.LBB3_308:                              # %min.iters.checked1481
	movq	-112(%rsp), %r10        # 8-byte Reload
                                        # kill: %R10D<def> %R10D<kill> %R10<kill> %R10<def>
	andl	$7, %r10d
	movq	%rax, %r11
	movq	%rax, %r9
	subq	%r10, %r9
	je	.LBB3_314
# BB#309:                               # %vector.memcheck1500
	movq	%r11, %rdi
	leaq	(%r15,%rdi,4), %rax
	movq	-24(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rdi,4), %rdx
	movq	-56(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rdi
	cmpq	%rdx, %r15
	sbbb	%dl, %dl
	cmpq	%rax, %rbx
	sbbb	%bl, %bl
	andb	%dl, %bl
	cmpq	%rdi, %r15
	sbbb	%dl, %dl
	cmpq	%rax, %rcx
	sbbb	%al, %al
	xorl	%edi, %edi
	testb	$1, %bl
	jne	.LBB3_315
# BB#310:                               # %vector.memcheck1500
	andb	%al, %dl
	andb	$1, %dl
	jne	.LBB3_315
# BB#311:                               # %vector.body1477.preheader
	movq	-24(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rdi
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rbp
	leaq	16(%r15), %rbx
	movq	%r9, %rdx
.LBB3_312:                              # %vector.body1477
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rbx)
	movdqu	%xmm3, (%rbx)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$-8, %rdx
	jne	.LBB3_312
# BB#313:                               # %middle.block1478
	testl	%r10d, %r10d
	movq	%r9, %rdi
	jne	.LBB3_315
	jmp	.LBB3_321
.LBB3_314:
	xorl	%edi, %edi
.LBB3_315:                              # %.lr.ph422.preheader1742
	movl	%r11d, %ebp
	subl	%edi, %ebp
	leaq	-1(%r11), %rdx
	subq	%rdi, %rdx
	andq	$3, %rbp
	je	.LBB3_318
# BB#316:                               # %.lr.ph422.prol.preheader
	negq	%rbp
	movq	-24(%rsp), %rbx         # 8-byte Reload
	movq	-56(%rsp), %rcx         # 8-byte Reload
.LBB3_317:                              # %.lr.ph422.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rdi,4), %eax
	addl	(%rbx,%rdi,4), %eax
	movl	%eax, (%r15,%rdi,4)
	incq	%rdi
	incq	%rbp
	jne	.LBB3_317
.LBB3_318:                              # %.lr.ph422.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_321
# BB#319:                               # %.lr.ph422.preheader1742.new
	movq	%r11, %rdx
	subq	%rdi, %rdx
	leaq	12(%r15,%rdi,4), %rbx
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rdi,4), %rbp
	movq	-24(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rdi,4), %rdi
.LBB3_320:                              # %.lr.ph422
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rdi), %eax
	movl	%eax, -12(%rbx)
	movl	-8(%rbp), %eax
	addl	-8(%rdi), %eax
	movl	%eax, -8(%rbx)
	movl	-4(%rbp), %eax
	addl	-4(%rdi), %eax
	movl	%eax, -4(%rbx)
	movl	(%rbp), %eax
	addl	(%rdi), %eax
	movl	%eax, (%rbx)
	addq	$16, %rbx
	addq	$16, %rbp
	addq	$16, %rdi
	addq	$-4, %rdx
	jne	.LBB3_320
.LBB3_321:                              # %._crit_edge423
	movq	-112(%rsp), %rax        # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB3_418
# BB#322:                               # %.lr.ph418.preheader
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	80(%rcx), %r14
	cmpl	$8, %eax
	jb	.LBB3_331
# BB#324:                               # %min.iters.checked1518
	movq	-112(%rsp), %r9         # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill> %R9<def>
	andl	$7, %r9d
	movq	-128(%rsp), %r8         # 8-byte Reload
	subq	%r9, %r8
	je	.LBB3_331
# BB#325:                               # %vector.memcheck1537
	movq	-128(%rsp), %rdi        # 8-byte Reload
	leaq	(%r14,%rdi,4), %rax
	movq	-64(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rdx
	movq	-72(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,4), %rdi
	cmpq	%rdx, %r14
	sbbb	%dl, %dl
	cmpq	%rax, %rcx
	sbbb	%bl, %bl
	andb	%dl, %bl
	cmpq	%rdi, %r14
	sbbb	%dl, %dl
	cmpq	%rax, %rbp
	sbbb	%al, %al
	xorl	%edi, %edi
	testb	$1, %bl
	jne	.LBB3_332
# BB#326:                               # %vector.memcheck1537
	andb	%al, %dl
	andb	$1, %dl
	jne	.LBB3_332
# BB#327:                               # %vector.body1514.preheader
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rdi
	movq	-72(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax), %rbp
	leaq	16(%r14), %rbx
	movq	%r8, %rdx
.LBB3_328:                              # %vector.body1514
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rbx)
	movdqu	%xmm3, (%rbx)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$-8, %rdx
	jne	.LBB3_328
# BB#329:                               # %middle.block1515
	testl	%r9d, %r9d
	movq	%r8, %rdi
	jne	.LBB3_332
	jmp	.LBB3_338
.LBB3_331:
	xorl	%edi, %edi
.LBB3_332:                              # %.lr.ph418.preheader1741
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %ebp
	subl	%edi, %ebp
	leaq	-1(%rax), %rdx
	subq	%rdi, %rdx
	andq	$3, %rbp
	je	.LBB3_335
# BB#333:                               # %.lr.ph418.prol.preheader
	negq	%rbp
	movq	-64(%rsp), %rcx         # 8-byte Reload
	movq	-72(%rsp), %rbx         # 8-byte Reload
.LBB3_334:                              # %.lr.ph418.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%rdi,4), %eax
	addl	(%rcx,%rdi,4), %eax
	movl	%eax, (%r14,%rdi,4)
	incq	%rdi
	incq	%rbp
	jne	.LBB3_334
.LBB3_335:                              # %.lr.ph418.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_338
# BB#336:                               # %.lr.ph418.preheader1741.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rdi, %rdx
	leaq	12(%r14,%rdi,4), %rbx
	movq	-72(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rdi,4), %rbp
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	12(%rax,%rdi,4), %rdi
.LBB3_337:                              # %.lr.ph418
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rdi), %eax
	movl	%eax, -12(%rbx)
	movl	-8(%rbp), %eax
	addl	-8(%rdi), %eax
	movl	%eax, -8(%rbx)
	movl	-4(%rbp), %eax
	addl	-4(%rdi), %eax
	movl	%eax, -4(%rbx)
	movl	(%rbp), %eax
	addl	(%rdi), %eax
	movl	%eax, (%rbx)
	addq	$16, %rbx
	addq	$16, %rbp
	addq	$16, %rdi
	addq	$-4, %rdx
	jne	.LBB3_337
.LBB3_338:                              # %._crit_edge419
	movq	-112(%rsp), %rax        # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB3_418
# BB#339:                               # %.lr.ph414.preheader
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	24(%rdx), %r8
	movq	(%r8), %r10
	cmpl	$8, %eax
	jb	.LBB3_347
# BB#341:                               # %min.iters.checked1555
	movq	-112(%rsp), %r11        # 8-byte Reload
                                        # kill: %R11D<def> %R11D<kill> %R11<kill> %R11<def>
	andl	$7, %r11d
	movq	-128(%rsp), %r9         # 8-byte Reload
	subq	%r11, %r9
	je	.LBB3_347
# BB#342:                               # %vector.memcheck1574
	movq	-128(%rsp), %rax        # 8-byte Reload
	leaq	(%r10,%rax,4), %rbx
	leaq	(%r13,%rax,4), %rdx
	leaq	(%r15,%rax,4), %rdi
	cmpq	%rdx, %r10
	sbbb	%dl, %dl
	cmpq	%rbx, %r13
	sbbb	%al, %al
	andb	%dl, %al
	cmpq	%rdi, %r10
	sbbb	%dl, %dl
	cmpq	%rbx, %r15
	sbbb	%dil, %dil
	xorl	%ebx, %ebx
	testb	$1, %al
	jne	.LBB3_348
# BB#343:                               # %vector.memcheck1574
	andb	%dil, %dl
	andb	$1, %dl
	jne	.LBB3_348
# BB#344:                               # %vector.body1551.preheader
	leaq	16(%r13), %rbp
	leaq	16(%r15), %rbx
	leaq	16(%r10), %rdi
	movq	%r9, %rdx
.LBB3_345:                              # %vector.body1551
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	-16(%rbx), %xmm2
	movdqu	(%rbx), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rdi)
	movdqu	%xmm3, (%rdi)
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB3_345
# BB#346:                               # %middle.block1552
	testl	%r11d, %r11d
	movq	%r9, %rbx
	jne	.LBB3_348
	jmp	.LBB3_354
.LBB3_347:
	xorl	%ebx, %ebx
.LBB3_348:                              # %.lr.ph414.preheader1740
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edi
	subl	%ebx, %edi
	leaq	-1(%rax), %rdx
	subq	%rbx, %rdx
	andq	$3, %rdi
	je	.LBB3_351
# BB#349:                               # %.lr.ph414.prol.preheader
	negq	%rdi
.LBB3_350:                              # %.lr.ph414.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rbx,4), %eax
	addl	(%r13,%rbx,4), %eax
	movl	%eax, (%r10,%rbx,4)
	incq	%rbx
	incq	%rdi
	jne	.LBB3_350
.LBB3_351:                              # %.lr.ph414.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_354
# BB#352:                               # %.lr.ph414.preheader1740.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rbx, %rdx
	leaq	12(%r10,%rbx,4), %rdi
	leaq	12(%r15,%rbx,4), %rbp
	leaq	12(%r13,%rbx,4), %rbx
.LBB3_353:                              # %.lr.ph414
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rbx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rbx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rbx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rbx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rbx
	addq	$-4, %rdx
	jne	.LBB3_353
.LBB3_354:                              # %._crit_edge415
	movq	-112(%rsp), %rax        # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB3_418
# BB#355:                               # %.lr.ph410.preheader
	movq	16(%r8), %r9
	cmpl	$8, %eax
	jb	.LBB3_363
# BB#357:                               # %min.iters.checked1592
	movq	-112(%rsp), %r11        # 8-byte Reload
                                        # kill: %R11D<def> %R11D<kill> %R11<kill> %R11<def>
	andl	$7, %r11d
	movq	-128(%rsp), %r8         # 8-byte Reload
	subq	%r11, %r8
	je	.LBB3_363
# BB#358:                               # %vector.memcheck1611
	movq	-128(%rsp), %rax        # 8-byte Reload
	leaq	(%r9,%rax,4), %rbx
	leaq	(%rsi,%rax,4), %rdx
	leaq	(%r14,%rax,4), %rdi
	cmpq	%rdx, %r9
	sbbb	%dl, %dl
	cmpq	%rbx, %rsi
	sbbb	%al, %al
	andb	%dl, %al
	cmpq	%rdi, %r9
	sbbb	%dl, %dl
	cmpq	%rbx, %r14
	sbbb	%dil, %dil
	xorl	%ebx, %ebx
	testb	$1, %al
	jne	.LBB3_364
# BB#359:                               # %vector.memcheck1611
	andb	%dil, %dl
	andb	$1, %dl
	jne	.LBB3_364
# BB#360:                               # %vector.body1588.preheader
	leaq	16(%rsi), %rbp
	leaq	16(%r14), %rbx
	leaq	16(%r9), %rdi
	movq	%r8, %rdx
.LBB3_361:                              # %vector.body1588
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	-16(%rbx), %xmm2
	movdqu	(%rbx), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rdi)
	movdqu	%xmm3, (%rdi)
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB3_361
# BB#362:                               # %middle.block1589
	testl	%r11d, %r11d
	movq	%r8, %rbx
	jne	.LBB3_364
	jmp	.LBB3_370
.LBB3_363:
	xorl	%ebx, %ebx
.LBB3_364:                              # %.lr.ph410.preheader1739
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %edi
	subl	%ebx, %edi
	leaq	-1(%rax), %rdx
	subq	%rbx, %rdx
	andq	$3, %rdi
	je	.LBB3_367
# BB#365:                               # %.lr.ph410.prol.preheader
	negq	%rdi
.LBB3_366:                              # %.lr.ph410.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rbx,4), %eax
	addl	(%rsi,%rbx,4), %eax
	movl	%eax, (%r9,%rbx,4)
	incq	%rbx
	incq	%rdi
	jne	.LBB3_366
.LBB3_367:                              # %.lr.ph410.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_370
# BB#368:                               # %.lr.ph410.preheader1739.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rbx, %rdx
	leaq	12(%r9,%rbx,4), %rdi
	leaq	12(%r14,%rbx,4), %rbp
	leaq	12(%rsi,%rbx,4), %rbx
.LBB3_369:                              # %.lr.ph410
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	addl	-12(%rbx), %eax
	movl	%eax, -12(%rdi)
	movl	-8(%rbp), %eax
	addl	-8(%rbx), %eax
	movl	%eax, -8(%rdi)
	movl	-4(%rbp), %eax
	addl	-4(%rbx), %eax
	movl	%eax, -4(%rdi)
	movl	(%rbp), %eax
	addl	(%rbx), %eax
	movl	%eax, (%rdi)
	addq	$16, %rdi
	addq	$16, %rbp
	addq	$16, %rbx
	addq	$-4, %rdx
	jne	.LBB3_369
.LBB3_370:                              # %._crit_edge411
	movq	-112(%rsp), %rax        # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB3_418
# BB#371:                               # %.lr.ph406.preheader
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	16(%rdx), %rcx
	movq	(%rcx), %r12
	cmpl	$8, %eax
	jb	.LBB3_379
# BB#373:                               # %min.iters.checked1629
	movq	-112(%rsp), %r8         # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill> %R8<def>
	andl	$7, %r8d
	movq	-128(%rsp), %r11        # 8-byte Reload
	subq	%r8, %r11
	je	.LBB3_379
# BB#374:                               # %vector.memcheck1648
	movq	-128(%rsp), %rdi        # 8-byte Reload
	leaq	(%r12,%rdi,4), %rax
	leaq	(%r13,%rdi,4), %rdx
	leaq	(%rsi,%rdi,4), %rdi
	cmpq	%rdx, %r12
	sbbb	%dl, %dl
	cmpq	%rax, %r13
	sbbb	%bl, %bl
	andb	%dl, %bl
	cmpq	%rdi, %r12
	sbbb	%dl, %dl
	cmpq	%rax, %rsi
	sbbb	%al, %al
	xorl	%edi, %edi
	testb	$1, %bl
	jne	.LBB3_380
# BB#375:                               # %vector.memcheck1648
	andb	%al, %dl
	andb	$1, %dl
	jne	.LBB3_380
# BB#376:                               # %vector.body1625.preheader
	leaq	16(%r13), %rdi
	leaq	16(%rsi), %rbp
	leaq	16(%r12), %rdx
	movq	%r11, %rbx
.LBB3_377:                              # %vector.body1625
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rdi), %xmm0
	movdqu	(%rdi), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rdx)
	movdqu	%xmm3, (%rdx)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$32, %rdx
	addq	$-8, %rbx
	jne	.LBB3_377
# BB#378:                               # %middle.block1626
	testl	%r8d, %r8d
	movq	%r11, %rdi
	jne	.LBB3_380
	jmp	.LBB3_386
.LBB3_330:
	movq	%r12, %r14
	jmp	.LBB3_299
.LBB3_379:
	xorl	%edi, %edi
.LBB3_380:                              # %.lr.ph406.preheader1738
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	%eax, %ebp
	subl	%edi, %ebp
	leaq	-1(%rax), %rdx
	subq	%rdi, %rdx
	andq	$3, %rbp
	je	.LBB3_383
# BB#381:                               # %.lr.ph406.prol.preheader
	negq	%rbp
.LBB3_382:                              # %.lr.ph406.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rdi,4), %eax
	addl	(%r13,%rdi,4), %eax
	movl	%eax, (%r12,%rdi,4)
	incq	%rdi
	incq	%rbp
	jne	.LBB3_382
.LBB3_383:                              # %.lr.ph406.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_386
# BB#384:                               # %.lr.ph406.preheader1738.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rdi, %rdx
	leaq	12(%r12,%rdi,4), %rbx
	leaq	12(%rsi,%rdi,4), %rsi
	leaq	12(%r13,%rdi,4), %rax
.LBB3_385:                              # %.lr.ph406
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rsi), %edi
	addl	-12(%rax), %edi
	movl	%edi, -12(%rbx)
	movl	-8(%rsi), %edi
	addl	-8(%rax), %edi
	movl	%edi, -8(%rbx)
	movl	-4(%rsi), %edi
	addl	-4(%rax), %edi
	movl	%edi, -4(%rbx)
	movl	(%rsi), %edi
	addl	(%rax), %edi
	movl	%edi, (%rbx)
	addq	$16, %rbx
	addq	$16, %rsi
	addq	$16, %rax
	addq	$-4, %rdx
	jne	.LBB3_385
.LBB3_386:                              # %._crit_edge407
	movq	-112(%rsp), %rdx        # 8-byte Reload
	testl	%edx, %edx
	jle	.LBB3_418
# BB#387:                               # %.lr.ph402.preheader
	movq	64(%rcx), %rax
	cmpl	$8, %edx
	jb	.LBB3_395
# BB#389:                               # %min.iters.checked1666
	movq	-112(%rsp), %r11        # 8-byte Reload
                                        # kill: %R11D<def> %R11D<kill> %R11<kill> %R11<def>
	andl	$7, %r11d
	movq	-128(%rsp), %r8         # 8-byte Reload
	subq	%r11, %r8
	je	.LBB3_395
# BB#390:                               # %vector.memcheck1685
	movq	-128(%rsp), %rdx        # 8-byte Reload
	leaq	(%rax,%rdx,4), %rsi
	leaq	(%r15,%rdx,4), %rdi
	leaq	(%r14,%rdx,4), %rbp
	cmpq	%rdi, %rax
	sbbb	%bl, %bl
	cmpq	%rsi, %r15
	sbbb	%dl, %dl
	andb	%bl, %dl
	cmpq	%rbp, %rax
	sbbb	%bl, %bl
	cmpq	%rsi, %r14
	sbbb	%dil, %dil
	xorl	%esi, %esi
	testb	$1, %dl
	jne	.LBB3_396
# BB#391:                               # %vector.memcheck1685
	andb	%dil, %bl
	andb	$1, %bl
	jne	.LBB3_396
# BB#392:                               # %vector.body1662.preheader
	leaq	16(%r15), %rsi
	leaq	16(%r14), %rbp
	leaq	16(%rax), %rbx
	movq	%r8, %rdi
.LBB3_393:                              # %vector.body1662
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	-16(%rbp), %xmm2
	movdqu	(%rbp), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rbx)
	movdqu	%xmm3, (%rbx)
	addq	$32, %rsi
	addq	$32, %rbp
	addq	$32, %rbx
	addq	$-8, %rdi
	jne	.LBB3_393
# BB#394:                               # %middle.block1663
	testl	%r11d, %r11d
	movq	%r8, %rsi
	jne	.LBB3_396
	jmp	.LBB3_402
.LBB3_395:
	xorl	%esi, %esi
.LBB3_396:                              # %.lr.ph402.preheader1737
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movl	%edx, %edi
	subl	%esi, %edi
	decq	%rdx
	subq	%rsi, %rdx
	andq	$3, %rdi
	je	.LBB3_399
# BB#397:                               # %.lr.ph402.prol.preheader
	negq	%rdi
.LBB3_398:                              # %.lr.ph402.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rsi,4), %ebp
	addl	(%r15,%rsi,4), %ebp
	movl	%ebp, (%rax,%rsi,4)
	incq	%rsi
	incq	%rdi
	jne	.LBB3_398
.LBB3_399:                              # %.lr.ph402.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB3_402
# BB#400:                               # %.lr.ph402.preheader1737.new
	movq	-128(%rsp), %rdx        # 8-byte Reload
	subq	%rsi, %rdx
	leaq	12(%rax,%rsi,4), %rax
	leaq	12(%r14,%rsi,4), %rdi
	leaq	12(%r15,%rsi,4), %rcx
.LBB3_401:                              # %.lr.ph402
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdi), %esi
	addl	-12(%rcx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdi), %esi
	addl	-8(%rcx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdi), %esi
	addl	-4(%rcx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdi), %esi
	addl	(%rcx), %esi
	movl	%esi, (%rax)
	addq	$16, %rax
	addq	$16, %rdi
	addq	$16, %rcx
	addq	$-4, %rdx
	jne	.LBB3_401
.LBB3_402:                              # %._crit_edge403
	movq	-112(%rsp), %rcx        # 8-byte Reload
	testl	%ecx, %ecx
	jle	.LBB3_418
# BB#403:                               # %.lr.ph.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax), %rax
	movq	(%rax), %rax
	cmpl	$8, %ecx
	jb	.LBB3_410
# BB#404:                               # %min.iters.checked1703
	movq	-112(%rsp), %r15        # 8-byte Reload
	andl	$7, %r15d
	movq	-128(%rsp), %rbp        # 8-byte Reload
	movq	%r15, %r8
	subq	%r15, %rbp
	je	.LBB3_410
# BB#405:                               # %vector.memcheck1722
	movq	-128(%rsp), %rsi        # 8-byte Reload
	leaq	(%rax,%rsi,4), %rcx
	leaq	(%r10,%rsi,4), %rdx
	leaq	(%r9,%rsi,4), %rsi
	cmpq	%rdx, %rax
	sbbb	%dl, %dl
	cmpq	%rcx, %r10
	sbbb	%bl, %bl
	andb	%dl, %bl
	cmpq	%rsi, %rax
	sbbb	%dl, %dl
	cmpq	%rcx, %r9
	sbbb	%sil, %sil
	xorl	%ecx, %ecx
	testb	$1, %bl
	jne	.LBB3_411
# BB#406:                               # %vector.memcheck1722
	andb	%sil, %dl
	andb	$1, %dl
	movq	-128(%rsp), %rdx        # 8-byte Reload
	jne	.LBB3_412
# BB#407:                               # %vector.body1699.preheader
	leaq	16(%r10), %rcx
	leaq	16(%r9), %rsi
	leaq	16(%rax), %rdi
	movq	%rbp, %rdx
.LBB3_408:                              # %vector.body1699
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movdqu	-16(%rsi), %xmm2
	movdqu	(%rsi), %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	%xmm2, -16(%rdi)
	movdqu	%xmm3, (%rdi)
	addq	$32, %rcx
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB3_408
# BB#409:                               # %middle.block1700
	testl	%r8d, %r8d
	movq	%rbp, %rcx
	movq	-128(%rsp), %rdx        # 8-byte Reload
	jne	.LBB3_412
	jmp	.LBB3_418
.LBB3_410:
	xorl	%ecx, %ecx
.LBB3_411:                              # %.lr.ph.preheader1736
	movq	-128(%rsp), %rdx        # 8-byte Reload
.LBB3_412:                              # %.lr.ph.preheader1736
	movl	%edx, %esi
	subl	%ecx, %esi
	decq	%rdx
	subq	%rcx, %rdx
	andq	$3, %rsi
	je	.LBB3_415
# BB#413:                               # %.lr.ph.prol.preheader
	negq	%rsi
.LBB3_414:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r9,%rcx,4), %edi
	addl	(%r10,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB3_414
.LBB3_415:                              # %.lr.ph.prol.loopexit
	cmpq	$3, %rdx
	movq	-128(%rsp), %r15        # 8-byte Reload
	jb	.LBB3_418
# BB#416:                               # %.lr.ph.preheader1736.new
	subq	%rcx, %r15
	leaq	12(%rax,%rcx,4), %rax
	leaq	12(%r9,%rcx,4), %rdx
	leaq	12(%r10,%rcx,4), %rcx
.LBB3_417:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdx), %esi
	addl	-12(%rcx), %esi
	movl	%esi, -12(%rax)
	movl	-8(%rdx), %esi
	addl	-8(%rcx), %esi
	movl	%esi, -8(%rax)
	movl	-4(%rdx), %esi
	addl	-4(%rcx), %esi
	movl	%esi, -4(%rax)
	movl	(%rdx), %esi
	addl	(%rcx), %esi
	movl	%esi, (%rax)
	addq	$16, %rax
	addq	$16, %rdx
	addq	$16, %rcx
	addq	$-4, %r15
	jne	.LBB3_417
.LBB3_418:                              # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	SetupLargerBlocks, .Lfunc_end3-SetupLargerBlocks
	.cfi_endproc

	.globl	SetupFastFullPelSearch
	.p2align	4, 0x90
	.type	SetupFastFullPelSearch,@function
SetupFastFullPelSearch:                 # @SetupFastFullPelSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 272
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	BlockSAD(%rip), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%edi, %rbp
	movq	(%rax,%rbp,8), %rax
	movq	56(%rax), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	max_search_range(%rip), %rax
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movl	(%rax,%rbp,4), %r14d
	movq	img(%rip), %rcx
	movq	14224(%rcx), %rax
	movslq	12(%rcx), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	movslq	432(%rax,%rdx), %rax
	movq	active_pps(%rip), %rdx
	cmpl	$0, 192(%rdx)
	je	.LBB4_3
# BB#1:
	movl	20(%rcx), %esi
	testl	%esi, %esi
	je	.LBB4_5
# BB#2:
	cmpl	$3, %esi
	je	.LBB4_5
.LBB4_3:
	cmpl	$0, 196(%rdx)
	je	.LBB4_7
# BB#4:
	cmpl	$1, 20(%rcx)
	jne	.LBB4_7
.LBB4_5:
	movq	input(%rip), %rdx
	cmpl	$0, 2936(%rdx)
	setne	%r15b
	jmp	.LBB4_8
.LBB4_7:
	xorl	%r15d, %r15d
.LBB4_8:
	movq	input(%rip), %rdx
	addq	$14232, %rcx            # imm = 0x3798
	cmpl	$0, 5780(%rdx)
	movl	$byte_abs, %edx
	cmovneq	%rcx, %rdx
	movq	(%rdx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	addq	136(%rsp), %rax         # 8-byte Folded Reload
	movq	listX(,%rax,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	$0, ref_access_method(%rip)
	movq	6448(%rcx), %rdx
	movq	%rdx, ref_pic_sub(%rip)
	movl	6392(%rcx), %edx
	movl	%edx, 88(%rsp)          # 4-byte Spill
	movl	6396(%rcx), %edx
	movl	%edx, 92(%rsp)          # 4-byte Spill
	movl	6408(%rcx), %edx
	movl	%edx, width_pad(%rip)
	movl	6412(%rcx), %edx
	movl	%edx, height_pad(%rip)
	testb	%r15b, %r15b
	je	.LBB4_10
# BB#9:
	movq	wp_weight(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movl	(%rdx), %edx
	movl	%edx, weight_luma(%rip)
	movq	wp_offset(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movl	(%rdx), %edx
	movl	%edx, offset_luma(%rip)
.LBB4_10:
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB4_13
# BB#11:
	movq	6464(%rcx), %rdx
	movq	(%rdx), %rsi
	movq	%rsi, ref_pic_sub+8(%rip)
	movq	8(%rdx), %rdx
	movq	%rdx, ref_pic_sub+16(%rip)
	movl	6416(%rcx), %edx
	movl	%edx, width_pad_cr(%rip)
	movl	6420(%rcx), %ecx
	movl	%ecx, height_pad_cr(%rip)
	testb	%r15b, %r15b
	je	.LBB4_13
# BB#12:
	movq	wp_weight(%rip), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movl	4(%rcx), %edx
	movl	%edx, weight_cr(%rip)
	movl	8(%rcx), %ecx
	movl	%ecx, weight_cr+4(%rip)
	movq	wp_offset(%rip), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	(%rax,%rbp,8), %rax
	movl	4(%rax), %ecx
	movl	%ecx, offset_cr(%rip)
	movl	8(%rax), %eax
	movl	%eax, offset_cr+4(%rip)
.LBB4_13:
	leal	1(%r14,%r14), %r12d
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rcx
	movq	6512(%rax), %rax
	movq	136(%rsp), %rbx         # 8-byte Reload
	movq	(%rcx,%rbx,8), %rsi
	movq	(%rax,%rbx,8), %rdx
	subq	$8, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	movswl	%di, %ecx
	leaq	140(%rsp), %rdi
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	movl	$0, %r9d
	movl	%ecx, 188(%rsp)         # 4-byte Spill
	movl	%ebx, %r8d
	pushq	$16
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	movq	184(%rsp), %r9          # 8-byte Reload
	addq	$32, %rsp
.Lcfi48:
	.cfi_adjust_cfa_offset -32
	movzwl	132(%rsp), %eax
	movw	%ax, %cx
	sarw	$15, %cx
	andl	$49152, %ecx            # imm = 0xC000
	shrl	$14, %ecx
	addl	%eax, %ecx
	sarw	$2, %cx
	movswl	%cx, %eax
	movq	search_center_x(%rip), %rcx
	movq	(%rcx,%rbx,8), %rdx
	movl	%eax, (%rdx,%r9,4)
	movzwl	134(%rsp), %eax
	movw	%ax, %cx
	sarw	$15, %cx
	andl	$49152, %ecx            # imm = 0xC000
	shrl	$14, %ecx
	addl	%eax, %ecx
	sarw	$2, %cx
	movswl	%cx, %eax
	movq	search_center_y(%rip), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movl	%eax, (%rsi,%r9,4)
	movq	input(%rip), %r8
	cmpl	$0, 4168(%r8)
	jne	.LBB4_15
# BB#14:
	movl	%r14d, %eax
	negl	%eax
	movl	(%rdx,%r9,4), %edi
	cmpl	%eax, %edi
	cmovll	%eax, %edi
	cmpl	%r14d, %edi
	cmovgl	%r14d, %edi
	movl	%edi, (%rdx,%r9,4)
	movl	(%rsi,%r9,4), %edi
	cmpl	%eax, %edi
	cmovgel	%edi, %eax
	cmpl	%r14d, %eax
	cmovgl	%r14d, %eax
	movl	%eax, (%rsi,%r9,4)
.LBB4_15:                               # %.lr.ph736.preheader
	imull	%r12d, %r12d
	addl	$-17, 88(%rsp)          # 4-byte Folded Spill
	addl	$-17, 92(%rsp)          # 4-byte Folded Spill
	leal	-2047(%r14), %eax
	movl	$2047, %edi             # imm = 0x7FF
	subl	%r14d, %edi
	movl	(%rdx,%r9,4), %ebp
	cmpl	%eax, %ebp
	cmovgel	%ebp, %eax
	cmpl	%edi, %eax
	cmovgl	%edi, %eax
	movl	%eax, (%rdx,%r9,4)
	movq	img(%rip), %rax
	movslq	8(%rax), %rdi
	shlq	$3, %rdi
	movl	LEVELMVLIMIT(%rdi,%rdi,2), %ebp
	addl	%r14d, %ebp
	movl	LEVELMVLIMIT+4(%rdi,%rdi,2), %edi
	subl	%r14d, %edi
	movl	(%rsi,%r9,4), %ebx
	cmpl	%ebp, %ebx
	cmovgel	%ebx, %ebp
	cmpl	%edi, %ebp
	cmovgl	%edi, %ebp
	movl	%ebp, (%rsi,%r9,4)
	movl	192(%rax), %edi
	addl	%edi, (%rdx,%r9,4)
	movl	(%rsi,%r9,4), %r10d
	addl	196(%rax), %r10d
	movl	%r10d, (%rsi,%r9,4)
	movl	(%rdx,%r9,4), %r11d
	movslq	196(%rax), %rdx
	movq	imgY_org(%rip), %rsi
	movl	$SetupFastFullPelSearch.orig_pels, %ebx
	decq	%rdx
	.p2align	4, 0x90
.LBB4_16:                               # %.lr.ph736
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsi,%rdx,8), %rdi
	movslq	192(%rax), %rbp
	movups	16(%rdi,%rbp,2), %xmm0
	movups	%xmm0, 16(%rbx)
	movdqu	(%rdi,%rbp,2), %xmm0
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	movslq	196(%rax), %rdi
	addq	$15, %rdi
	incq	%rdx
	cmpq	%rdi, %rdx
	jl	.LBB4_16
# BB#17:                                # %._crit_edge737
	cmpl	$0, ChromaMEEnable(%rip)
	movl	%r10d, 128(%rsp)        # 4-byte Spill
	movl	%r11d, 124(%rsp)        # 4-byte Spill
	movl	%r12d, 84(%rsp)         # 4-byte Spill
	je	.LBB4_25
# BB#18:                                # %.preheader592.preheader
	cmpl	$0, 15548(%rax)
	jle	.LBB4_24
# BB#19:                                # %.lr.ph728
	movslq	204(%rax), %rbp
	movl	15544(%rax), %ecx
	.p2align	4, 0x90
.LBB4_20:                               # =>This Inner Loop Header: Depth=1
	movq	imgUV_org(%rip), %rdx
	movq	(%rdx), %rdx
	movslq	200(%rax), %rsi
	addq	%rsi, %rsi
	addq	(%rdx,%rbp,8), %rsi
	movslq	%ecx, %rdx
	addq	%rdx, %rdx
	movq	%rbx, %rdi
	callq	memcpy
	movq	img(%rip), %rax
	movslq	15544(%rax), %rcx
	leaq	(%rbx,%rcx,2), %rbx
	incq	%rbp
	movl	15548(%rax), %edx
	movl	204(%rax), %esi
	addl	%edx, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %rbp
	jl	.LBB4_20
# BB#21:                                # %._crit_edge729
	testl	%edx, %edx
	jle	.LBB4_24
# BB#22:                                # %.lr.ph728.1
	movslq	204(%rax), %rbp
	movl	15544(%rax), %ecx
	.p2align	4, 0x90
.LBB4_23:                               # =>This Inner Loop Header: Depth=1
	movq	imgUV_org(%rip), %rdx
	movq	8(%rdx), %rdx
	movslq	200(%rax), %rsi
	addq	%rsi, %rsi
	addq	(%rdx,%rbp,8), %rsi
	movslq	%ecx, %rdx
	addq	%rdx, %rdx
	movq	%rbx, %rdi
	callq	memcpy
	movq	img(%rip), %rax
	movslq	15544(%rax), %rcx
	leaq	(%rbx,%rcx,2), %rbx
	incq	%rbp
	movslq	15548(%rax), %rdx
	movslq	204(%rax), %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rbp
	jl	.LBB4_23
.LBB4_24:                               # %._crit_edge729.1
	movq	input(%rip), %r8
	movq	152(%rsp), %r9          # 8-byte Reload
	movl	128(%rsp), %r10d        # 4-byte Reload
	movl	124(%rsp), %r11d        # 4-byte Reload
.LBB4_25:                               # %.loopexit593
	cmpl	%r14d, %r11d
	setl	%bl
	movl	88(%rsp), %edx          # 4-byte Reload
	subl	%r14d, %edx
	cmpl	%edx, %r11d
	setg	%cl
	cmpl	%r14d, %r10d
	setl	%dl
	orb	%bl, %dl
	orb	%cl, %dl
	movl	92(%rsp), %ecx          # 4-byte Reload
	subl	%r14d, %ecx
	cmpl	%ecx, %r10d
	setg	7(%rsp)                 # 1-byte Folded Spill
	cmpl	$0, 4168(%r8)
	je	.LBB4_27
# BB#26:                                # %.loopexit593..preheader591.preheader_crit_edge
	movl	84(%rsp), %eax          # 4-byte Reload
	movq	%rax, 160(%rsp)         # 8-byte Spill
	jmp	.LBB4_32
.LBB4_27:
	movl	192(%rax), %ecx
	movl	196(%rax), %eax
	subl	%r11d, %ecx
	subl	%r10d, %eax
	movq	spiral_search_x(%rip), %rdi
	movq	spiral_search_y(%rip), %rbp
	movl	84(%rsp), %esi          # 4-byte Reload
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_28:                               # =>This Inner Loop Header: Depth=1
	movswl	(%rdi,%rsi,2), %ebx
	cmpl	%ebx, %ecx
	jne	.LBB4_30
# BB#29:                                #   in Loop: Header=BB4_28 Depth=1
	movswl	(%rbp,%rsi,2), %ebx
	cmpl	%ebx, %eax
	je	.LBB4_31
.LBB4_30:                               #   in Loop: Header=BB4_28 Depth=1
	incq	%rsi
	cmpq	160(%rsp), %rsi         # 8-byte Folded Reload
	jl	.LBB4_28
	jmp	.LBB4_32
.LBB4_31:
	movq	pos_00(%rip), %rax
	movq	136(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movl	%esi, (%rax,%r9,4)
.LBB4_32:                               # %.preheader591.preheader
	orb	%dl, 7(%rsp)            # 1-byte Folded Spill
	xorl	%r9d, %r9d
	movl	%r15d, 184(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB4_33:                               # %.preheader591
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_72 Depth 2
                                        #       Child Loop BB4_73 Depth 3
                                        #     Child Loop BB4_77 Depth 2
                                        #       Child Loop BB4_78 Depth 3
                                        #         Child Loop BB4_103 Depth 4
                                        #         Child Loop BB4_107 Depth 4
                                        #         Child Loop BB4_81 Depth 4
                                        #           Child Loop BB4_84 Depth 5
                                        #           Child Loop BB4_88 Depth 5
                                        #           Child Loop BB4_93 Depth 5
                                        #           Child Loop BB4_98 Depth 5
                                        #     Child Loop BB4_42 Depth 2
                                        #       Child Loop BB4_43 Depth 3
                                        #     Child Loop BB4_47 Depth 2
                                        #       Child Loop BB4_48 Depth 3
                                        #         Child Loop BB4_50 Depth 4
                                        #           Child Loop BB4_52 Depth 5
                                        #           Child Loop BB4_55 Depth 5
                                        #           Child Loop BB4_58 Depth 5
                                        #           Child Loop BB4_61 Depth 5
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	spiral_search_y(%rip), %rax
	movswl	(%rax,%r9,2), %edx
	addl	%r10d, %edx
	movq	spiral_search_x(%rip), %rax
	movswl	(%rax,%r9,2), %ecx
	addl	%r11d, %ecx
	leal	80(,%rdx,4), %eax
	movl	%eax, 100(%rsp)         # 4-byte Spill
	leal	80(,%rcx,4), %eax
	movl	%eax, 96(%rsp)          # 4-byte Spill
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB4_39
# BB#34:                                #   in Loop: Header=BB4_33 Depth=1
	movl	$1, %eax
	testl	%edx, %edx
	js	.LBB4_38
# BB#35:                                #   in Loop: Header=BB4_33 Depth=1
	cmpl	92(%rsp), %edx          # 4-byte Folded Reload
	jg	.LBB4_38
# BB#36:                                #   in Loop: Header=BB4_33 Depth=1
	testl	%ecx, %ecx
	js	.LBB4_38
# BB#37:                                #   in Loop: Header=BB4_33 Depth=1
	xorl	%eax, %eax
	cmpl	88(%rsp), %ecx          # 4-byte Folded Reload
	setg	%al
.LBB4_38:                               #   in Loop: Header=BB4_33 Depth=1
	movl	%eax, ref_access_method(%rip)
	jmp	.LBB4_40
	.p2align	4, 0x90
.LBB4_39:                               # %.preheader591._crit_edge
                                        #   in Loop: Header=BB4_33 Depth=1
	movl	ref_access_method(%rip), %eax
.LBB4_40:                               #   in Loop: Header=BB4_33 Depth=1
	cltq
	movq	ref_pic_sub(%rip), %rdi
	movl	100(%rsp), %esi         # 4-byte Reload
	movl	96(%rsp), %edx          # 4-byte Reload
	callq	*get_line(,%rax,8)
	movq	%rax, %r12
	testb	%r15b, %r15b
	je	.LBB4_71
# BB#41:                                # %.preheader587
                                        #   in Loop: Header=BB4_33 Depth=1
	xorl	%r14d, %r14d
	movq	img(%rip), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	$SetupFastFullPelSearch.orig_pels, %r15d
	xorl	%r11d, %r11d
	pxor	%xmm13, %xmm13
	.p2align	4, 0x90
.LBB4_42:                               # %.preheader584
                                        #   Parent Loop BB4_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_43 Depth 3
	movslq	img_padded_size_x(%rip), %rcx
	movq	%r15, %r10
	subq	$-128, %r10
	leaq	-64(,%rcx,4), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movd	weight_luma(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm11       # xmm11 = xmm0[0,0,0,0]
	movd	wp_luma_round(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm9        # xmm9 = xmm0[0,0,0,0]
	movss	luma_log_weight_denom(%rip), %xmm8 # xmm8 = mem[0],zero,zero,zero
	movd	offset_luma(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm10       # xmm10 = xmm0[0,0,0,0]
	movq	32(%rsp), %rdx          # 8-byte Reload
	movd	15520(%rdx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm12       # xmm12 = xmm0[0,0,0,0]
	leaq	-32(%rcx,%rcx), %r13
	addq	$32, %r13
	movq	%r12, 8(%rsp)           # 8-byte Spill
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_43:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_42 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm2, %xmm2
	movss	%xmm8, %xmm2            # xmm2 = xmm8[0],xmm2[1,2,3]
	movdqu	(%r12), %xmm4
	movdqu	16(%r12), %xmm1
	movdqa	%xmm4, %xmm3
	punpcklwd	%xmm13, %xmm3   # xmm3 = xmm3[0],xmm13[0],xmm3[1],xmm13[1],xmm3[2],xmm13[2],xmm3[3],xmm13[3]
	punpckhwd	%xmm13, %xmm4   # xmm4 = xmm4[4],xmm13[4],xmm4[5],xmm13[5],xmm4[6],xmm13[6],xmm4[7],xmm13[7]
	movdqa	%xmm1, %xmm6
	punpcklwd	%xmm13, %xmm6   # xmm6 = xmm6[0],xmm13[0],xmm6[1],xmm13[1],xmm6[2],xmm13[2],xmm6[3],xmm13[3]
	punpckhwd	%xmm13, %xmm1   # xmm1 = xmm1[4],xmm13[4],xmm1[5],xmm13[5],xmm1[6],xmm13[6],xmm1[7],xmm13[7]
	pshufd	$245, %xmm1, %xmm7      # xmm7 = xmm1[1,1,3,3]
	pmuludq	%xmm11, %xmm1
	pshufd	$232, %xmm1, %xmm5      # xmm5 = xmm1[0,2,2,3]
	pshufd	$245, %xmm11, %xmm0     # xmm0 = xmm11[1,1,3,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm1      # xmm1 = xmm7[0,2,2,3]
	punpckldq	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm11, %xmm6
	pshufd	$232, %xmm6, %xmm1      # xmm1 = xmm6[0,2,2,3]
	pmuludq	%xmm0, %xmm7
	pshufd	$232, %xmm7, %xmm6      # xmm6 = xmm7[0,2,2,3]
	punpckldq	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0],xmm1[1],xmm6[1]
	pshufd	$245, %xmm4, %xmm6      # xmm6 = xmm4[1,1,3,3]
	pmuludq	%xmm11, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm0, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm4    # xmm4 = xmm4[0],xmm6[0],xmm4[1],xmm6[1]
	pshufd	$245, %xmm3, %xmm6      # xmm6 = xmm3[1,1,3,3]
	pmuludq	%xmm11, %xmm3
	pshufd	$232, %xmm3, %xmm7      # xmm7 = xmm3[0,2,2,3]
	pmuludq	%xmm0, %xmm6
	pshufd	$232, %xmm6, %xmm0      # xmm0 = xmm6[0,2,2,3]
	punpckldq	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	paddd	%xmm9, %xmm7
	paddd	%xmm9, %xmm4
	paddd	%xmm9, %xmm1
	paddd	%xmm9, %xmm5
	psrad	%xmm2, %xmm5
	psrad	%xmm2, %xmm1
	psrad	%xmm2, %xmm4
	psrad	%xmm2, %xmm7
	paddd	%xmm10, %xmm7
	paddd	%xmm10, %xmm4
	paddd	%xmm10, %xmm1
	paddd	%xmm10, %xmm5
	movdqa	%xmm5, %xmm2
	pcmpgtd	%xmm13, %xmm2
	movdqa	%xmm1, %xmm6
	pcmpgtd	%xmm13, %xmm6
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm13, %xmm0
	movdqa	%xmm7, %xmm3
	pcmpgtd	%xmm13, %xmm3
	pand	%xmm7, %xmm3
	pand	%xmm4, %xmm0
	pand	%xmm1, %xmm6
	pand	%xmm5, %xmm2
	movdqa	%xmm12, %xmm5
	pcmpgtd	%xmm2, %xmm5
	movdqa	%xmm12, %xmm1
	pcmpgtd	%xmm6, %xmm1
	movdqa	%xmm12, %xmm7
	pcmpgtd	%xmm0, %xmm7
	movdqa	%xmm12, %xmm4
	pcmpgtd	%xmm3, %xmm4
	pand	%xmm4, %xmm3
	pandn	%xmm12, %xmm4
	por	%xmm3, %xmm4
	pand	%xmm7, %xmm0
	pandn	%xmm12, %xmm7
	por	%xmm0, %xmm7
	pand	%xmm1, %xmm6
	pandn	%xmm12, %xmm1
	por	%xmm6, %xmm1
	pand	%xmm5, %xmm2
	pandn	%xmm12, %xmm5
	por	%xmm2, %xmm5
	movdqu	(%r15,%r8), %xmm0
	movdqu	16(%r15,%r8), %xmm2
	movdqa	%xmm0, %xmm6
	punpcklwd	%xmm13, %xmm6   # xmm6 = xmm6[0],xmm13[0],xmm6[1],xmm13[1],xmm6[2],xmm13[2],xmm6[3],xmm13[3]
	punpckhwd	%xmm13, %xmm0   # xmm0 = xmm0[4],xmm13[4],xmm0[5],xmm13[5],xmm0[6],xmm13[6],xmm0[7],xmm13[7]
	movdqa	%xmm2, %xmm3
	punpcklwd	%xmm13, %xmm3   # xmm3 = xmm3[0],xmm13[0],xmm3[1],xmm13[1],xmm3[2],xmm13[2],xmm3[3],xmm13[3]
	punpckhwd	%xmm13, %xmm2   # xmm2 = xmm2[4],xmm13[4],xmm2[5],xmm13[5],xmm2[6],xmm13[6],xmm2[7],xmm13[7]
	psubd	%xmm2, %xmm5
	pshufd	$78, %xmm5, %xmm2       # xmm2 = xmm5[2,3,0,1]
	psubd	%xmm3, %xmm1
	pshufd	$78, %xmm1, %xmm3       # xmm3 = xmm1[2,3,0,1]
	psubd	%xmm0, %xmm7
	pshufd	$78, %xmm7, %xmm0       # xmm0 = xmm7[2,3,0,1]
	psubd	%xmm6, %xmm4
	pshufd	$78, %xmm4, %xmm6       # xmm6 = xmm4[2,3,0,1]
	movd	%xmm4, %rdi
	movslq	%edi, %r9
	addl	(%rax,%r9,4), %ecx
	movd	%xmm6, %rsi
	sarq	$32, %rdi
	addl	(%rax,%rdi,4), %ecx
	movslq	%esi, %rdi
	addl	(%rax,%rdi,4), %ecx
	movd	%xmm7, %rdi
	sarq	$32, %rsi
	addl	(%rax,%rsi,4), %ecx
	movslq	%edi, %rsi
	addl	(%rax,%rsi,4), %ebp
	movd	%xmm0, %rsi
	sarq	$32, %rdi
	addl	(%rax,%rdi,4), %ebp
	movslq	%esi, %rdi
	addl	(%rax,%rdi,4), %ebp
	movd	%xmm1, %rdi
	sarq	$32, %rsi
	addl	(%rax,%rsi,4), %ebp
	movslq	%edi, %rsi
	addl	(%rax,%rsi,4), %ebx
	movd	%xmm3, %rsi
	sarq	$32, %rdi
	addl	(%rax,%rdi,4), %ebx
	movslq	%esi, %rdi
	addl	(%rax,%rdi,4), %ebx
	movd	%xmm5, %rdi
	sarq	$32, %rsi
	addl	(%rax,%rsi,4), %ebx
	movslq	%edi, %rsi
	addl	(%rax,%rsi,4), %edx
	movd	%xmm2, %rsi
	sarq	$32, %rdi
	addl	(%rax,%rdi,4), %edx
	movslq	%esi, %rdi
	addl	(%rax,%rdi,4), %edx
	sarq	$32, %rsi
	addl	(%rax,%rsi,4), %edx
	addq	$32, %r8
	addq	%r13, %r12
	cmpl	$128, %r8d
	jne	.LBB4_43
# BB#44:                                #   in Loop: Header=BB4_42 Depth=2
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	128(%r12,%rsi,2), %r12
	movq	%r14, %r9
	orq	$1, %r9
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%r14,8), %rsi
	movq	104(%rsp), %r8          # 8-byte Reload
	movl	%ecx, (%rsi,%r8,4)
	movq	%r14, %rcx
	orq	$2, %rcx
	movq	(%rdi,%r9,8), %rsi
	movl	%ebp, (%rsi,%r8,4)
	movq	%r14, %rsi
	orq	$3, %rsi
	movq	(%rdi,%rcx,8), %rcx
	movl	%ebx, (%rcx,%r8,4)
	addq	$4, %r14
	movq	(%rdi,%rsi,8), %rcx
	movl	%edx, (%rcx,%r8,4)
	incl	%r11d
	cmpl	$4, %r11d
	movq	%r10, %r15
	jne	.LBB4_42
# BB#45:                                #   in Loop: Header=BB4_33 Depth=1
	cmpl	$0, ChromaMEEnable(%rip)
	movq	104(%rsp), %r9          # 8-byte Reload
	je	.LBB4_111
# BB#46:                                # %.preheader586.preheader
                                        #   in Loop: Header=BB4_33 Depth=1
	xorl	%r12d, %r12d
	movl	$SetupFastFullPelSearch.orig_pels+512, %ebp
	.p2align	4, 0x90
.LBB4_47:                               # %.preheader586
                                        #   Parent Loop BB4_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_48 Depth 3
                                        #         Child Loop BB4_50 Depth 4
                                        #           Child Loop BB4_52 Depth 5
                                        #           Child Loop BB4_55 Depth 5
                                        #           Child Loop BB4_58 Depth 5
                                        #           Child Loop BB4_61 Depth 5
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movslq	ref_access_method(%rip), %rax
	movq	ref_pic_sub+8(,%r12,8), %rdi
	movl	100(%rsp), %esi         # 4-byte Reload
	movl	96(%rsp), %edx          # 4-byte Reload
	callq	*get_crline(,%rax,8)
	movq	48(%rsp), %r12          # 8-byte Reload
	xorl	%edi, %edi
	movq	img(%rip), %r13
	xorl	%ecx, %ecx
	movq	%r13, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_48:                               # %.preheader582
                                        #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_47 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_50 Depth 4
                                        #           Child Loop BB4_52 Depth 5
                                        #           Child Loop BB4_55 Depth 5
                                        #           Child Loop BB4_58 Depth 5
                                        #           Child Loop BB4_61 Depth 5
	movl	%ecx, 188(%rsp)         # 4-byte Spill
	movq	%rdi, 192(%rsp)         # 8-byte Spill
	movl	15548(%r13), %ecx
	movl	%ecx, 168(%rsp)         # 4-byte Spill
	testl	%ecx, %ecx
	jle	.LBB4_68
# BB#49:                                # %.preheader577.lr.ph
                                        #   in Loop: Header=BB4_48 Depth=3
	movslq	15544(%r13), %r15
	cmpq	$4, %r15
	movslq	img_cr_padded_size_x(%rip), %rdx
	movl	wp_chroma_round(%rip), %r8d
	movl	chroma_log_weight_denom(%rip), %ecx
	movl	$4, %edi
	cmovgl	%r15d, %edi
	decl	%edi
	shrl	$2, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	subq	%r15, %rdx
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	movl	%r8d, 64(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB4_50:                               # %.preheader577
                                        #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_47 Depth=2
                                        #       Parent Loop BB4_48 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_52 Depth 5
                                        #           Child Loop BB4_55 Depth 5
                                        #           Child Loop BB4_58 Depth 5
                                        #           Child Loop BB4_61 Depth 5
	testl	%r15d, %r15d
	jle	.LBB4_67
# BB#51:                                # %.lr.ph673
                                        #   in Loop: Header=BB4_50 Depth=4
	movl	%esi, 112(%rsp)         # 4-byte Spill
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movl	%edx, 44(%rsp)          # 4-byte Spill
	movl	weight_cr(,%r12,4), %r11d
	movl	offset_cr(,%r12,4), %edi
	movl	15524(%r13), %edx
	xorl	%esi, %esi
	leaq	2(%rbp), %rbx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r15, %r10
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_52:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_47 Depth=2
                                        #       Parent Loop BB4_48 Depth=3
                                        #         Parent Loop BB4_50 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rax), %r13d
	addq	$2, %rax
	imull	%r11d, %r13d
	addl	%r8d, %r13d
	sarl	%cl, %r13d
	addl	%edi, %r13d
	movl	$0, %ebx
	cmovsl	%ebx, %r13d
	cmpl	%edx, %r13d
	cmovgl	%edx, %r13d
	movzwl	(%rbp), %r12d
	addq	$2, %rbp
	movslq	%r13d, %rbx
	subq	%r12, %rbx
	addl	(%r15,%rbx,4), %r9d
	addl	$4, %esi
	cmpl	%r10d, %esi
	jl	.LBB4_52
# BB#53:                                # %.preheader576
                                        #   in Loop: Header=BB4_50 Depth=4
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	2(%rax,%rdx,2), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rdx,2), %rbp
	testl	%r10d, %r10d
	movq	%r10, %r15
	jle	.LBB4_64
# BB#54:                                # %.lr.ph681
                                        #   in Loop: Header=BB4_50 Depth=4
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	weight_cr(,%rdx,4), %r12d
	movl	offset_cr(,%rdx,4), %r8d
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	15524(%rdx), %esi
	xorl	%r11d, %r11d
	leaq	2(%rbp), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r15, %r13
	movq	16(%rsp), %r15          # 8-byte Reload
	xorl	%r10d, %r10d
	movl	64(%rsp), %edx          # 4-byte Reload
	.p2align	4, 0x90
.LBB4_55:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_47 Depth=2
                                        #       Parent Loop BB4_48 Depth=3
                                        #         Parent Loop BB4_50 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rax), %ebx
	addq	$2, %rax
	imull	%r12d, %ebx
	addl	%edx, %ebx
	sarl	%cl, %ebx
	addl	%r8d, %ebx
	cmovsl	%r10d, %ebx
	cmpl	%esi, %ebx
	cmovgl	%esi, %ebx
	movzwl	(%rbp), %edi
	addq	$2, %rbp
	movslq	%ebx, %rbx
	subq	%rdi, %rbx
	addl	(%r15,%rbx,4), %r14d
	addl	$4, %r11d
	cmpl	%r13d, %r11d
	jl	.LBB4_55
# BB#56:                                # %.preheader575
                                        #   in Loop: Header=BB4_50 Depth=4
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	2(%rax,%rdx,2), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rdx,2), %rbp
	testl	%r13d, %r13d
	movq	%r13, %r15
	jle	.LBB4_63
# BB#57:                                # %.lr.ph689
                                        #   in Loop: Header=BB4_50 Depth=4
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	weight_cr(,%rdx,4), %r12d
	movl	offset_cr(,%rdx,4), %r8d
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	15524(%rdx), %esi
	xorl	%r11d, %r11d
	leaq	2(%rbp), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r15, %r13
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	32(%rsp), %r10d         # 4-byte Reload
	movl	64(%rsp), %edx          # 4-byte Reload
	.p2align	4, 0x90
.LBB4_58:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_47 Depth=2
                                        #       Parent Loop BB4_48 Depth=3
                                        #         Parent Loop BB4_50 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rax), %ebx
	addq	$2, %rax
	imull	%r12d, %ebx
	addl	%edx, %ebx
	sarl	%cl, %ebx
	addl	%r8d, %ebx
	movl	$0, %edi
	cmovsl	%edi, %ebx
	cmpl	%esi, %ebx
	cmovgl	%esi, %ebx
	movzwl	(%rbp), %edi
	addq	$2, %rbp
	movslq	%ebx, %rbx
	subq	%rdi, %rbx
	addl	(%r15,%rbx,4), %r10d
	addl	$4, %r11d
	cmpl	%r13d, %r11d
	jl	.LBB4_58
# BB#59:                                # %.preheader
                                        #   in Loop: Header=BB4_50 Depth=4
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	2(%rax,%rdx,2), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%rsi,%rdx,2), %rbp
	testl	%r13d, %r13d
	movq	%r13, %r15
	jle	.LBB4_63
# BB#60:                                # %.lr.ph697
                                        #   in Loop: Header=BB4_50 Depth=4
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	weight_cr(,%rdx,4), %esi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	offset_cr(,%rdx,4), %r8d
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	15524(%rdx), %esi
	xorl	%r11d, %r11d
	leaq	2(%rbp), %rdx
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	44(%rsp), %r10d         # 4-byte Reload
	movl	64(%rsp), %r12d         # 4-byte Reload
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_61:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_47 Depth=2
                                        #       Parent Loop BB4_48 Depth=3
                                        #         Parent Loop BB4_50 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rax), %ebx
	addq	$2, %rax
	imull	8(%rsp), %ebx           # 4-byte Folded Reload
	addl	%r12d, %ebx
	sarl	%cl, %ebx
	addl	%r8d, %ebx
	cmovsl	%r13d, %ebx
	cmpl	%esi, %ebx
	cmovgl	%esi, %ebx
	movzwl	(%rbp), %edi
	addq	$2, %rbp
	movslq	%ebx, %rbx
	subq	%rdi, %rbx
	addl	(%rdx,%rbx,4), %r10d
	addl	$4, %r11d
	cmpl	%r15d, %r11d
	jl	.LBB4_61
# BB#62:                                # %._crit_edge698.loopexit
                                        #   in Loop: Header=BB4_50 Depth=4
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	2(%rax,%rdx,2), %rax
	movq	200(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,2), %rbp
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%r10d, %edx
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	%r12d, %r8d
	movq	%rsi, %r12
	jmp	.LBB4_66
	.p2align	4, 0x90
.LBB4_63:                               #   in Loop: Header=BB4_50 Depth=4
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	jmp	.LBB4_65
	.p2align	4, 0x90
.LBB4_64:                               #   in Loop: Header=BB4_50 Depth=4
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB4_65:                               # %._crit_edge698
                                        #   in Loop: Header=BB4_50 Depth=4
	movl	44(%rsp), %edx          # 4-byte Reload
	movl	32(%rsp), %ebx          # 4-byte Reload
	movl	64(%rsp), %r8d          # 4-byte Reload
.LBB4_66:                               # %._crit_edge698
                                        #   in Loop: Header=BB4_50 Depth=4
	movl	112(%rsp), %esi         # 4-byte Reload
.LBB4_67:                               # %._crit_edge698
                                        #   in Loop: Header=BB4_50 Depth=4
	movq	208(%rsp), %rdi         # 8-byte Reload
	leaq	(%rax,%rdi,2), %rax
	addl	$4, %esi
	cmpl	168(%rsp), %esi         # 4-byte Folded Reload
	jl	.LBB4_50
	jmp	.LBB4_69
	.p2align	4, 0x90
.LBB4_68:                               #   in Loop: Header=BB4_48 Depth=3
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
.LBB4_69:                               # %._crit_edge710
                                        #   in Loop: Header=BB4_48 Depth=3
	movq	%rax, %r8
	movq	192(%rsp), %rdi         # 8-byte Reload
	movq	%rdi, %rax
	orq	$1, %rax
	movq	144(%rsp), %r11         # 8-byte Reload
	movq	(%r11,%rdi,8), %rcx
	movq	104(%rsp), %r10         # 8-byte Reload
	addl	%r9d, (%rcx,%r10,4)
	movq	%r10, %r9
	movq	%rdi, %rcx
	orq	$2, %rcx
	movq	(%r11,%rax,8), %rax
	addl	%r14d, (%rax,%r9,4)
	movq	%rdi, %rax
	orq	$3, %rax
	movq	(%r11,%rcx,8), %rcx
	addl	%ebx, (%rcx,%r9,4)
	addq	$4, %rdi
	movq	(%r11,%rax,8), %rax
	addl	%edx, (%rax,%r9,4)
	movq	%r8, %rax
	movl	188(%rsp), %ecx         # 4-byte Reload
	incl	%ecx
	cmpl	$4, %ecx
	jne	.LBB4_48
# BB#70:                                #   in Loop: Header=BB4_47 Depth=2
	incq	%r12
	cmpq	$2, %r12
	jne	.LBB4_47
	jmp	.LBB4_111
	.p2align	4, 0x90
.LBB4_71:                               # %.preheader585.preheader
                                        #   in Loop: Header=BB4_33 Depth=1
	xorl	%r11d, %r11d
	movl	$SetupFastFullPelSearch.orig_pels, %r13d
	xorl	%r10d, %r10d
	pxor	%xmm8, %xmm8
	.p2align	4, 0x90
.LBB4_72:                               # %.preheader585
                                        #   Parent Loop BB4_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_73 Depth 3
	movslq	img_padded_size_x(%rip), %rcx
	movq	%r13, %r9
	subq	$-128, %r9
	leaq	-64(,%rcx,4), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leaq	-32(%rcx,%rcx), %r14
	addq	$32, %r14
	movq	%r12, %r8
	movq	%r12, %r15
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movq	16(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_73:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_72 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	(%r15), %xmm3
	movdqu	16(%r15), %xmm0
	movdqa	%xmm3, %xmm5
	punpcklwd	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1],xmm5[2],xmm8[2],xmm5[3],xmm8[3]
	punpckhwd	%xmm8, %xmm3    # xmm3 = xmm3[4],xmm8[4],xmm3[5],xmm8[5],xmm3[6],xmm8[6],xmm3[7],xmm8[7]
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm8, %xmm2    # xmm2 = xmm2[0],xmm8[0],xmm2[1],xmm8[1],xmm2[2],xmm8[2],xmm2[3],xmm8[3]
	punpckhwd	%xmm8, %xmm0    # xmm0 = xmm0[4],xmm8[4],xmm0[5],xmm8[5],xmm0[6],xmm8[6],xmm0[7],xmm8[7]
	movdqu	(%r13,%r12), %xmm6
	movdqu	16(%r13,%r12), %xmm1
	movdqa	%xmm6, %xmm7
	punpcklwd	%xmm8, %xmm7    # xmm7 = xmm7[0],xmm8[0],xmm7[1],xmm8[1],xmm7[2],xmm8[2],xmm7[3],xmm8[3]
	punpckhwd	%xmm8, %xmm6    # xmm6 = xmm6[4],xmm8[4],xmm6[5],xmm8[5],xmm6[6],xmm8[6],xmm6[7],xmm8[7]
	movdqa	%xmm1, %xmm4
	punpcklwd	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1],xmm4[2],xmm8[2],xmm4[3],xmm8[3]
	punpckhwd	%xmm8, %xmm1    # xmm1 = xmm1[4],xmm8[4],xmm1[5],xmm8[5],xmm1[6],xmm8[6],xmm1[7],xmm8[7]
	psubd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	psubd	%xmm4, %xmm2
	pshufd	$78, %xmm2, %xmm4       # xmm4 = xmm2[2,3,0,1]
	psubd	%xmm6, %xmm3
	pshufd	$78, %xmm3, %xmm6       # xmm6 = xmm3[2,3,0,1]
	psubd	%xmm7, %xmm5
	pshufd	$78, %xmm5, %xmm7       # xmm7 = xmm5[2,3,0,1]
	movd	%xmm5, %rax
	movslq	%eax, %rdx
	addl	(%rdi,%rdx,4), %ecx
	movd	%xmm7, %rdx
	sarq	$32, %rax
	addl	(%rdi,%rax,4), %ecx
	movslq	%edx, %rax
	addl	(%rdi,%rax,4), %ecx
	movd	%xmm3, %rax
	sarq	$32, %rdx
	addl	(%rdi,%rdx,4), %ecx
	movslq	%eax, %rdx
	addl	(%rdi,%rdx,4), %esi
	movd	%xmm6, %rdx
	sarq	$32, %rax
	addl	(%rdi,%rax,4), %esi
	movslq	%edx, %rax
	addl	(%rdi,%rax,4), %esi
	movd	%xmm2, %rax
	sarq	$32, %rdx
	addl	(%rdi,%rdx,4), %esi
	movslq	%eax, %rdx
	addl	(%rdi,%rdx,4), %ebx
	movd	%xmm4, %rdx
	sarq	$32, %rax
	addl	(%rdi,%rax,4), %ebx
	movslq	%edx, %rax
	addl	(%rdi,%rax,4), %ebx
	movd	%xmm0, %rax
	sarq	$32, %rdx
	addl	(%rdi,%rdx,4), %ebx
	movslq	%eax, %rdx
	addl	(%rdi,%rdx,4), %ebp
	movd	%xmm1, %rdx
	sarq	$32, %rax
	addl	(%rdi,%rax,4), %ebp
	movslq	%edx, %rax
	addl	(%rdi,%rax,4), %ebp
	sarq	$32, %rdx
	addl	(%rdi,%rdx,4), %ebp
	addq	$32, %r12
	addq	%r14, %r15
	cmpl	$128, %r12d
	jne	.LBB4_73
# BB#74:                                #   in Loop: Header=BB4_72 Depth=2
	movq	%r8, %r12
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	128(%r12,%rdx,2), %r12
	movq	%r11, %r14
	orq	$1, %r14
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%r11,8), %rdx
	movq	104(%rsp), %r8          # 8-byte Reload
	movl	%ecx, (%rdx,%r8,4)
	movq	%r11, %rcx
	orq	$2, %rcx
	movq	(%rdi,%r14,8), %rdx
	movl	%esi, (%rdx,%r8,4)
	movq	%r11, %rdx
	orq	$3, %rdx
	movq	(%rdi,%rcx,8), %rcx
	movl	%ebx, (%rcx,%r8,4)
	addq	$4, %r11
	movq	(%rdi,%rdx,8), %rcx
	movl	%ebp, (%rcx,%r8,4)
	incl	%r10d
	cmpl	$4, %r10d
	movq	%r9, %r13
	jne	.LBB4_72
# BB#75:                                #   in Loop: Header=BB4_33 Depth=1
	cmpl	$0, ChromaMEEnable(%rip)
	movq	104(%rsp), %r9          # 8-byte Reload
	je	.LBB4_111
# BB#76:                                # %.preheader588.preheader
                                        #   in Loop: Header=BB4_33 Depth=1
	xorl	%ecx, %ecx
	movl	$SetupFastFullPelSearch.orig_pels+512, %r14d
	.p2align	4, 0x90
.LBB4_77:                               # %.preheader588
                                        #   Parent Loop BB4_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_78 Depth 3
                                        #         Child Loop BB4_103 Depth 4
                                        #         Child Loop BB4_107 Depth 4
                                        #         Child Loop BB4_81 Depth 4
                                        #           Child Loop BB4_84 Depth 5
                                        #           Child Loop BB4_88 Depth 5
                                        #           Child Loop BB4_93 Depth 5
                                        #           Child Loop BB4_98 Depth 5
	movslq	ref_access_method(%rip), %rax
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movq	ref_pic_sub+8(,%rcx,8), %rdi
	movl	100(%rsp), %esi         # 4-byte Reload
	movl	96(%rsp), %edx          # 4-byte Reload
	callq	*get_crline(,%rax,8)
	xorl	%ebp, %ebp
	movq	img(%rip), %rcx
	xorl	%edx, %edx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_78:                               # %.preheader583
                                        #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_77 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_103 Depth 4
                                        #         Child Loop BB4_107 Depth 4
                                        #         Child Loop BB4_81 Depth 4
                                        #           Child Loop BB4_84 Depth 5
                                        #           Child Loop BB4_88 Depth 5
                                        #           Child Loop BB4_93 Depth 5
                                        #           Child Loop BB4_98 Depth 5
	movl	%edx, 44(%rsp)          # 4-byte Spill
	movl	15548(%rcx), %ebx
	testl	%ebx, %ebx
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	jle	.LBB4_100
# BB#79:                                # %.preheader581.lr.ph
                                        #   in Loop: Header=BB4_78 Depth=3
	movl	15544(%rcx), %r10d
	testl	%r10d, %r10d
	movl	img_cr_padded_size_x(%rip), %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	jle	.LBB4_101
# BB#80:                                # %.preheader581.us.preheader
                                        #   in Loop: Header=BB4_78 Depth=3
	cmpl	$3, %r10d
	movl	$4, %edi
	cmovgl	%r10d, %edi
	decl	%edi
	shrl	$2, %edi
	movq	%rdi, %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	addl	%edi, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	subl	%r10d, %ecx
	movslq	%ecx, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leal	-1(%r10), %ecx
	shrl	$2, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leal	1(%rcx), %ecx
	andl	$1, %ecx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	xorl	%r13d, %r13d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_81:                               # %.preheader581.us
                                        #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_77 Depth=2
                                        #       Parent Loop BB4_78 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB4_84 Depth 5
                                        #           Child Loop BB4_88 Depth 5
                                        #           Child Loop BB4_93 Depth 5
                                        #           Child Loop BB4_98 Depth 5
	leaq	2(%r14), %r9
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_83
# BB#82:                                #   in Loop: Header=BB4_81 Depth=4
	movq	%r14, %rdi
	movq	%rax, %rbp
	xorl	%esi, %esi
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_84
	jmp	.LBB4_85
	.p2align	4, 0x90
.LBB4_83:                               #   in Loop: Header=BB4_81 Depth=4
	leaq	2(%rax), %rbp
	movzwl	(%rax), %esi
	movzwl	(%r14), %edi
	subq	%rdi, %rsi
	addl	(%rdx,%rsi,4), %r8d
	movl	$4, %esi
	movq	%r9, %rdi
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB4_85
	.p2align	4, 0x90
.LBB4_84:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_77 Depth=2
                                        #       Parent Loop BB4_78 Depth=3
                                        #         Parent Loop BB4_81 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rbp), %ebx
	movzwl	(%rdi), %edx
	subq	%rdx, %rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
	addl	(%rdx,%rbx,4), %r8d
	movzwl	2(%rbp), %edx
	movzwl	2(%rdi), %ebx
	subq	%rbx, %rdx
	movq	16(%rsp), %rbx          # 8-byte Reload
	addl	(%rbx,%rdx,4), %r8d
	movq	16(%rsp), %rdx          # 8-byte Reload
	addl	$8, %esi
	addq	$4, %rbp
	addq	$4, %rdi
	cmpl	%r10d, %esi
	jl	.LBB4_84
.LBB4_85:                               # %.lr.ph617.us
                                        #   in Loop: Header=BB4_81 Depth=4
	xorl	%ebp, %ebp
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%r9,%rsi,2), %r11
	leaq	2(%rax,%rsi,2), %rdi
	je	.LBB4_87
# BB#86:                                #   in Loop: Header=BB4_81 Depth=4
	movzwl	(%rdi), %edx
	addq	$2, %rdi
	movzwl	(%r11), %esi
	addq	$2, %r11
	subq	%rsi, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	addl	(%rsi,%rdx,4), %ecx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	$4, %ebp
.LBB4_87:                               # %.prol.loopexit982
                                        #   in Loop: Header=BB4_81 Depth=4
	addq	$4, %r14
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB4_89
	.p2align	4, 0x90
.LBB4_88:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_77 Depth=2
                                        #       Parent Loop BB4_78 Depth=3
                                        #         Parent Loop BB4_81 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rdi), %esi
	movzwl	(%r11), %ebx
	subq	%rbx, %rsi
	addl	(%rdx,%rsi,4), %ecx
	movzwl	2(%rdi), %esi
	movzwl	2(%r11), %ebx
	subq	%rbx, %rsi
	addl	(%rdx,%rsi,4), %ecx
	addl	$8, %ebp
	addq	$4, %rdi
	addq	$4, %r11
	cmpl	%r10d, %ebp
	jl	.LBB4_88
.LBB4_89:                               # %.lr.ph625.us
                                        #   in Loop: Header=BB4_81 Depth=4
	xorl	%ebp, %ebp
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rax,%rsi,2), %r9
	leaq	(%r14,%rsi,2), %rdi
	jne	.LBB4_91
# BB#90:                                #   in Loop: Header=BB4_81 Depth=4
	movq	%r9, %rax
	jmp	.LBB4_92
	.p2align	4, 0x90
.LBB4_91:                               #   in Loop: Header=BB4_81 Depth=4
	leaq	2(%r9), %rax
	movzwl	(%r9), %edx
	movzwl	(%rdi), %esi
	addq	$2, %rdi
	subq	%rsi, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	addl	(%rsi,%rdx,4), %r15d
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	$4, %ebp
.LBB4_92:                               # %.prol.loopexit986
                                        #   in Loop: Header=BB4_81 Depth=4
	movq	72(%rsp), %rsi          # 8-byte Reload
	leaq	2(%r14,%rsi,2), %r11
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB4_94
	.p2align	4, 0x90
.LBB4_93:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_77 Depth=2
                                        #       Parent Loop BB4_78 Depth=3
                                        #         Parent Loop BB4_81 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rax), %esi
	movzwl	(%rdi), %ebx
	subq	%rbx, %rsi
	addl	(%rdx,%rsi,4), %r15d
	movzwl	2(%rax), %esi
	movzwl	2(%rdi), %ebx
	subq	%rbx, %rsi
	addl	(%rdx,%rsi,4), %r15d
	addl	$8, %ebp
	addq	$4, %rax
	addq	$4, %rdi
	cmpl	%r10d, %ebp
	jl	.LBB4_93
.LBB4_94:                               # %.lr.ph633.us
                                        #   in Loop: Header=BB4_81 Depth=4
	xorl	%ebp, %ebp
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	2(%r9,%rax,2), %r9
	leaq	(%r11,%rax,2), %rdi
	jne	.LBB4_96
# BB#95:                                #   in Loop: Header=BB4_81 Depth=4
	movq	%r9, %rax
	jmp	.LBB4_97
	.p2align	4, 0x90
.LBB4_96:                               #   in Loop: Header=BB4_81 Depth=4
	leaq	2(%r9), %rax
	movzwl	(%r9), %edx
	movzwl	(%rdi), %esi
	addq	$2, %rdi
	subq	%rsi, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	addl	(%rsi,%rdx,4), %r12d
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	$4, %ebp
.LBB4_97:                               # %.prol.loopexit990
                                        #   in Loop: Header=BB4_81 Depth=4
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	2(%r11,%rsi,2), %r11
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB4_99
	.p2align	4, 0x90
.LBB4_98:                               #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_77 Depth=2
                                        #       Parent Loop BB4_78 Depth=3
                                        #         Parent Loop BB4_81 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movzwl	(%rax), %esi
	movzwl	(%rdi), %ebx
	subq	%rbx, %rsi
	addl	(%rdx,%rsi,4), %r12d
	movzwl	2(%rax), %esi
	movzwl	2(%rdi), %ebx
	subq	%rbx, %rsi
	addl	(%rdx,%rsi,4), %r12d
	addl	$8, %ebp
	addq	$4, %rax
	addq	$4, %rdi
	cmpl	%r10d, %ebp
	jl	.LBB4_98
.LBB4_99:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_81 Depth=4
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%r9,%rsi,2), %rax
	leaq	(%r11,%rsi,2), %r14
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	2(%rax,%rsi,2), %rax
	addl	$4, %r13d
	movq	48(%rsp), %rbx          # 8-byte Reload
	cmpl	%ebx, %r13d
	jl	.LBB4_81
	jmp	.LBB4_109
	.p2align	4, 0x90
.LBB4_100:                              #   in Loop: Header=BB4_78 Depth=3
	xorl	%r15d, %r15d
	jmp	.LBB4_108
	.p2align	4, 0x90
.LBB4_101:                              # %._crit_edge.preheader
                                        #   in Loop: Header=BB4_78 Depth=3
	subl	%r10d, %ecx
	movslq	%ecx, %rcx
	leal	-1(%rbx), %esi
	movl	%esi, %edi
	shrl	$2, %edi
	incl	%edi
	xorl	%r15d, %r15d
	andl	$7, %edi
	je	.LBB4_104
# BB#102:                               # %._crit_edge.prol.preheader
                                        #   in Loop: Header=BB4_78 Depth=3
	leaq	(%rcx,%rcx), %rbp
	negl	%edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_103:                              # %._crit_edge.prol
                                        #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_77 Depth=2
                                        #       Parent Loop BB4_78 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addl	$4, %edx
	addq	%rbp, %rax
	incl	%edi
	jne	.LBB4_103
	jmp	.LBB4_105
.LBB4_104:                              #   in Loop: Header=BB4_78 Depth=3
	xorl	%edx, %edx
.LBB4_105:                              # %._crit_edge.prol.loopexit
                                        #   in Loop: Header=BB4_78 Depth=3
	cmpl	$28, %esi
	jb	.LBB4_108
# BB#106:                               # %._crit_edge.preheader.new
                                        #   in Loop: Header=BB4_78 Depth=3
	shlq	$4, %rcx
	.p2align	4, 0x90
.LBB4_107:                              # %._crit_edge
                                        #   Parent Loop BB4_33 Depth=1
                                        #     Parent Loop BB4_77 Depth=2
                                        #       Parent Loop BB4_78 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addl	$32, %edx
	addq	%rcx, %rax
	cmpl	%ebx, %edx
	jl	.LBB4_107
	.p2align	4, 0x90
.LBB4_108:                              #   in Loop: Header=BB4_78 Depth=3
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
.LBB4_109:                              # %._crit_edge645
                                        #   in Loop: Header=BB4_78 Depth=3
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	orq	$1, %rdx
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%rbp,8), %rsi
	movq	104(%rsp), %r9          # 8-byte Reload
	addl	%r8d, (%rsi,%r9,4)
	movq	%rbp, %rsi
	orq	$2, %rsi
	movq	(%rdi,%rdx,8), %rdx
	addl	%ecx, (%rdx,%r9,4)
	movq	%rbp, %rcx
	orq	$3, %rcx
	movq	(%rdi,%rsi,8), %rdx
	addl	%r15d, (%rdx,%r9,4)
	addq	$4, %rbp
	movq	(%rdi,%rcx,8), %rcx
	addl	%r12d, (%rcx,%r9,4)
	movl	44(%rsp), %edx          # 4-byte Reload
	incl	%edx
	cmpl	$4, %edx
	movq	112(%rsp), %rcx         # 8-byte Reload
	jne	.LBB4_78
# BB#110:                               #   in Loop: Header=BB4_77 Depth=2
	movq	168(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpq	$2, %rcx
	jne	.LBB4_77
.LBB4_111:                              # %.loopexit
                                        #   in Loop: Header=BB4_33 Depth=1
	incq	%r9
	cmpq	160(%rsp), %r9          # 8-byte Folded Reload
	movl	184(%rsp), %r15d        # 4-byte Reload
	movl	128(%rsp), %r10d        # 4-byte Reload
	movl	124(%rsp), %r11d        # 4-byte Reload
	jl	.LBB4_33
# BB#112:
	movq	136(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %edi
	movl	180(%rsp), %esi         # 4-byte Reload
	movl	84(%rsp), %edx          # 4-byte Reload
	callq	SetupLargerBlocks
	movq	search_setup_done(%rip), %rax
	movq	(%rax,%rbx,8), %rax
	movq	152(%rsp), %rcx         # 8-byte Reload
	movl	$1, (%rax,%rcx,4)
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	SetupFastFullPelSearch, .Lfunc_end4-SetupFastFullPelSearch
	.cfi_endproc

	.globl	FastFullPelBlockMotionSearch
	.p2align	4, 0x90
	.type	FastFullPelBlockMotionSearch,@function
FastFullPelBlockMotionSearch:           # @FastFullPelBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 64
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	96(%rsp), %eax
	leal	1(%rax,%rax), %r15d
	movq	img(%rip), %rax
	movl	192(%rax), %r11d
	movl	196(%rax), %r10d
	subl	%r10d, %r8d
	subl	%r11d, %ecx
	sarl	$2, %ecx
	addl	%r8d, %ecx
	movq	BlockSAD(%rip), %rax
	movslq	%edx, %r14
	movq	(%rax,%r14,8), %rax
	movslq	%esi, %rbx
	movq	(%rax,%rbx,8), %rax
	movslq	%r9d, %rdi
	movq	(%rax,%rdi,8), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %r13
	movq	search_setup_done(%rip), %rax
	movq	(%rax,%r14,8), %rax
	cmpl	$0, (%rax,%rbx,4)
	jne	.LBB5_2
# BB#1:
	movswl	%si, %edi
	movl	%edx, %esi
	callq	SetupFastFullPelSearch
	movq	img(%rip), %rax
	movl	192(%rax), %r11d
	movl	196(%rax), %r10d
.LBB5_2:
	movl	112(%rsp), %ebp
	movl	104(%rsp), %eax
	movzwl	72(%rsp), %ecx
	movzwl	64(%rsp), %r8d
	imull	%r15d, %r15d
	movq	search_center_x(%rip), %rdx
	movq	(%rdx,%r14,8), %rdx
	movl	(%rdx,%rbx,4), %edx
	movl	%edx, (%rsp)            # 4-byte Spill
	movq	search_center_y(%rip), %rdx
	movq	(%rdx,%r14,8), %rdx
	movl	(%rdx,%rbx,4), %edx
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	input(%rip), %rdx
	cmpl	$0, 4168(%rdx)
	je	.LBB5_4
# BB#3:                                 # %..preheader_crit_edge
	xorl	%r9d, %r9d
	movq	mvbits(%rip), %r12
	jmp	.LBB5_5
.LBB5_4:
	movq	pos_00(%rip), %rdx
	movq	(%rdx,%r14,8), %rdx
	movslq	(%rdx,%rbx,4), %rdx
	movq	mvbits(%rip), %r12
	movswq	%r8w, %rsi
	shlq	$2, %rsi
	negq	%rsi
	movswq	%cx, %rbx
	shlq	$2, %rbx
	negq	%rbx
	movl	(%r12,%rbx), %ebx
	addl	(%r12,%rsi), %ebx
	imull	%ebp, %ebx
	sarl	$16, %ebx
	addl	(%r13,%rdx,4), %ebx
	xorl	%r9d, %r9d
	cmpl	%eax, %ebx
	cmovll	%edx, %r9d
	cmovlel	%ebx, %eax
.LBB5_5:                                # %.preheader
	subl	%r11d, (%rsp)           # 4-byte Folded Spill
	subl	%r10d, 4(%rsp)          # 4-byte Folded Spill
	movq	spiral_search_x(%rip), %r14
	movq	spiral_search_y(%rip), %r10
	movswl	%r8w, %r8d
	movswl	%cx, %r11d
	movl	%r15d, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rsi,4), %ebx
	cmpl	%eax, %ebx
	jge	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_6 Depth=1
	movswl	(%r14,%rsi,2), %edx
	addl	(%rsp), %edx            # 4-byte Folded Reload
	shll	$2, %edx
	movswl	(%r10,%rsi,2), %r15d
	addl	4(%rsp), %r15d          # 4-byte Folded Reload
	shll	$2, %r15d
	subl	%r8d, %edx
	movslq	%edx, %rdx
	subl	%r11d, %r15d
	movslq	%r15d, %rdi
	movl	(%r12,%rdi,4), %edi
	addl	(%r12,%rdx,4), %edi
	imull	%ebp, %edi
	sarl	$16, %edi
	addl	%ebx, %edi
	cmpl	%eax, %edi
	cmovll	%esi, %r9d
	cmovlel	%edi, %eax
.LBB5_8:                                #   in Loop: Header=BB5_6 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB5_6
# BB#9:
	movslq	%r9d, %rcx
	movzwl	(%r14,%rcx,2), %edx
	addl	(%rsp), %edx            # 4-byte Folded Reload
	movq	80(%rsp), %rsi
	movw	%dx, (%rsi)
	movzwl	(%r10,%rcx,2), %ecx
	addl	4(%rsp), %ecx           # 4-byte Folded Reload
	movq	88(%rsp), %rdx
	movw	%cx, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	FastFullPelBlockMotionSearch, .Lfunc_end5-FastFullPelBlockMotionSearch
	.cfi_endproc

	.type	BlockSAD,@object        # @BlockSAD
	.local	BlockSAD
	.comm	BlockSAD,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"InitializeFastFullIntegerSearch: BlockSAD"
	.size	.L.str, 42

	.type	search_setup_done,@object # @search_setup_done
	.local	search_setup_done
	.comm	search_setup_done,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"InitializeFastFullIntegerSearch: search_setup_done"
	.size	.L.str.1, 51

	.type	search_center_x,@object # @search_center_x
	.local	search_center_x
	.comm	search_center_x,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"InitializeFastFullIntegerSearch: search_center_x"
	.size	.L.str.2, 49

	.type	search_center_y,@object # @search_center_y
	.local	search_center_y
	.comm	search_center_y,8,8
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"InitializeFastFullIntegerSearch: search_center_y"
	.size	.L.str.3, 49

	.type	pos_00,@object          # @pos_00
	.local	pos_00
	.comm	pos_00,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"InitializeFastFullIntegerSearch: pos_00"
	.size	.L.str.4, 40

	.type	max_search_range,@object # @max_search_range
	.local	max_search_range
	.comm	max_search_range,8,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"InitializeFastFullIntegerSearch: max_search_range"
	.size	.L.str.5, 50

	.type	SetupFastFullPelSearch.orig_pels,@object # @SetupFastFullPelSearch.orig_pels
	.local	SetupFastFullPelSearch.orig_pels
	.comm	SetupFastFullPelSearch.orig_pels,1536,16
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	width_pad,@object       # @width_pad
	.comm	width_pad,4,4
	.type	height_pad,@object      # @height_pad
	.comm	height_pad,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	width_pad_cr,@object    # @width_pad_cr
	.comm	width_pad_cr,4,4
	.type	height_pad_cr,@object   # @height_pad_cr
	.comm	height_pad_cr,4,4
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
