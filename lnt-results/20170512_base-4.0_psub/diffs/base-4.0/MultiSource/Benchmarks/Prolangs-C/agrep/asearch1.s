	.text
	.file	"asearch1.bc"
	.globl	asearch1
	.p2align	4, 0x90
	.type	asearch1,@function
asearch1:                               # @asearch1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$98696, %rsp            # imm = 0x18188
.Lcfi6:
	.cfi_def_cfa_offset 98752
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r14d
	movl	I(%rip), %eax
	testl	%eax, %eax
	jne	.LBB0_2
# BB#1:
	movl	$-1, Init1(%rip)
.LBB0_2:
	cmpl	%edx, DD(%rip)
	jbe	.LBB0_4
# BB#3:
	leal	1(%rdx), %ecx
	movl	%ecx, DD(%rip)
.LBB0_4:
	cmpl	%edx, %eax
	jbe	.LBB0_6
# BB#5:
	leal	1(%rdx), %eax
	movl	%eax, I(%rip)
.LBB0_6:
	cmpl	%edx, S(%rip)
	jbe	.LBB0_8
# BB#7:
	leal	1(%rdx), %eax
	movl	%eax, S(%rip)
.LBB0_8:
	movq	%rdi, 360(%rsp)         # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	callq	strlen
	movq	48(%rsp), %rdi          # 8-byte Reload
	movb	$10, 49535(%rsp)
	movl	NO_ERR_MASK(%rip), %r13d
	movl	D_endpos(%rip), %edx
	movq	%rax, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpl	$2, %eax
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movl	%edx, %ebp
	jb	.LBB0_16
# BB#9:                                 # %.lr.ph265.preheader
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	7(%rax), %edx
	leal	-2(%rax), %eax
	andl	$7, %edx
	je	.LBB0_13
# BB#10:                                # %.lr.ph265.prol.preheader
	xorl	%ecx, %ecx
	movl	24(%rsp), %esi          # 4-byte Reload
	movl	%esi, %ebp
	.p2align	4, 0x90
.LBB0_11:                               # %.lr.ph265.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rbp), %esi
	orl	%esi, %ebp
	incl	%ecx
	cmpl	%ecx, %edx
	jne	.LBB0_11
# BB#12:                                # %.lr.ph265.prol.loopexit.unr-lcssa
	incl	%ecx
	cmpl	$7, %eax
	jae	.LBB0_14
	jmp	.LBB0_16
.LBB0_13:
	movl	$1, %ecx
	movl	24(%rsp), %edx          # 4-byte Reload
	movl	%edx, %ebp
	cmpl	$7, %eax
	jb	.LBB0_16
.LBB0_14:                               # %.lr.ph265.preheader.new
	movq	40(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%ecx, %eax
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph265
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rbp), %ecx
	orl	%ebp, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	orl	%ecx, %edx
	leal	(%rdx,%rdx), %ecx
	orl	%edx, %ecx
	leal	(%rcx,%rcx), %ebp
	orl	%ecx, %ebp
	addl	$-8, %eax
	jne	.LBB0_15
.LBB0_16:                               # %._crit_edge266
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	leal	(%rdi,%rdi), %ebx
	testl	%edi, %edi
	je	.LBB0_18
# BB#17:                                # %.lr.ph261.preheader
	leal	-1(%rdi), %eax
	leaq	4(,%rax,4), %rbp
	leaq	192(%rsp), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	leaq	272(%rsp), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	48(%rsp), %rdi          # 8-byte Reload
.LBB0_18:                               # %.preheader224
	cmpl	%edi, %ebx
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	jb	.LBB0_32
# BB#19:                                # %.lr.ph258
	movl	Init(%rip), %eax
	movl	48(%rsp), %r11d         # 4-byte Reload
	movl	%ebx, %ecx
	orl	$1, %ecx
	movq	%rcx, %rsi
	subq	%r11, %rsi
	cmpq	$7, %rsi
	jbe	.LBB0_30
# BB#20:                                # %min.iters.checked
	movq	%rsi, %r8
	andq	$-8, %r8
	movq	%rsi, %r9
	andq	$-8, %r9
	je	.LBB0_30
# BB#21:                                # %vector.ph
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	-8(%r9), %r10
	movl	%r10d, %ebx
	shrl	$3, %ebx
	incl	%ebx
	andq	$3, %rbx
	je	.LBB0_24
# BB#22:                                # %vector.body.prol.preheader
	leaq	288(%rsp,%r11,4), %rbp
	leaq	208(%rsp,%r11,4), %rdx
	negq	%rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_23:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rdx,%rdi,4)
	movdqu	%xmm0, (%rdx,%rdi,4)
	movdqu	%xmm0, -16(%rbp,%rdi,4)
	movdqu	%xmm0, (%rbp,%rdi,4)
	addq	$8, %rdi
	incq	%rbx
	jne	.LBB0_23
	jmp	.LBB0_25
.LBB0_24:
	xorl	%edi, %edi
.LBB0_25:                               # %vector.body.prol.loopexit
	cmpq	$24, %r10
	jb	.LBB0_28
# BB#26:                                # %vector.ph.new
	movq	%r9, %rbp
	subq	%rdi, %rbp
	addq	%r11, %rdi
	leaq	384(%rsp,%rdi,4), %rbx
	leaq	304(%rsp,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB0_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -112(%rbx)
	movdqu	%xmm0, -96(%rbx)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -80(%rbx)
	movdqu	%xmm0, -64(%rbx)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -48(%rbx)
	movdqu	%xmm0, -32(%rbx)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rdi
	addq	$-32, %rbp
	jne	.LBB0_27
.LBB0_28:                               # %middle.block
	cmpq	%r9, %rsi
	movl	12(%rsp), %ebx          # 4-byte Reload
	je	.LBB0_32
# BB#29:
	addq	%r8, %r11
.LBB0_30:                               # %scalar.ph.preheader
	leaq	192(%rsp,%r11,4), %rsi
	leaq	272(%rsp,%r11,4), %rdi
	subq	%r11, %rcx
	.p2align	4, 0x90
.LBB0_31:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, (%rsi)
	movl	%eax, (%rdi)
	addq	$4, %rsi
	addq	$4, %rdi
	decq	%rcx
	jne	.LBB0_31
.LBB0_32:                               # %.preheader222
	leaq	49536(%rsp), %rsi
	movl	$49152, %edx            # imm = 0xC000
	movl	%r14d, %edi
	callq	fill_buf
	movl	%ebx, %r9d
	movl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB0_110
# BB#33:                                # %.lr.ph256
	movq	104(%rsp), %rax         # 8-byte Reload
	notl	%eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rsi          # 8-byte Reload
	leal	1(%rsi), %r11d
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	%eax, %rdx
	movq	%rdx, 352(%rsp)         # 8-byte Spill
	movl	%esi, %r15d
	movl	%r9d, %edx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	notl	%eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movl	%r11d, %eax
	movl	%r9d, %edx
	orl	$1, %edx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	subq	%r15, %rdx
	leaq	-8(%rdx), %rsi
	movq	%rsi, 168(%rsp)         # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	shrl	$3, %esi
	incl	%esi
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	andq	$-8, %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	leaq	(%r15,%rdx), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	andl	$3, %esi
	leaq	192(%rsp,%rax,4), %rdx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	leaq	272(%rsp,%rax,4), %r12
	leaq	288(%rsp,%r15,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	negq	%rsi
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	leaq	208(%rsp,%r15,4), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	$49152, 8(%rsp)         # 4-byte Folded Spill
                                        # imm = 0xC000
	movl	%r11d, 16(%rsp)         # 4-byte Spill
	movl	%r14d, 140(%rsp)        # 4-byte Spill
	movl	%r13d, 20(%rsp)         # 4-byte Spill
.LBB0_34:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_37 Depth 2
                                        #       Child Loop BB0_39 Depth 3
                                        #       Child Loop BB0_58 Depth 3
                                        #       Child Loop BB0_62 Depth 3
                                        #       Child Loop BB0_65 Depth 3
                                        #       Child Loop BB0_70 Depth 3
                                        #       Child Loop BB0_74 Depth 3
                                        #       Child Loop BB0_92 Depth 3
                                        #       Child Loop BB0_96 Depth 3
                                        #       Child Loop BB0_99 Depth 3
                                        #       Child Loop BB0_103 Depth 3
	leal	49152(%rcx), %ebp
	cmpl	$49151, %ecx            # imm = 0xBFFF
	movl	%ebp, %eax
	movq	%rcx, 368(%rsp)         # 8-byte Spill
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	jg	.LBB0_36
# BB#35:                                # %.preheader
                                        #   in Loop: Header=BB0_34 Depth=1
	movslq	%ecx, %rdi
	leaq	49536(%rsp), %rax
	addq	%rax, %rdi
	movq	360(%rsp), %rsi         # 8-byte Reload
	movq	352(%rsp), %rdx         # 8-byte Reload
	movl	%r9d, %ebx
	movl	%r11d, %r14d
	callq	strncpy
	movl	%r14d, %r11d
	movl	%ebx, %r9d
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rbp,%rax), %eax
	movb	$0, 384(%rsp,%rax)
	cmpl	$49153, %eax            # imm = 0xC001
	jb	.LBB0_106
.LBB0_36:                               # %.lr.ph251.preheader
                                        #   in Loop: Header=BB0_34 Depth=1
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movl	$49152, %ebp            # imm = 0xC000
	movl	Init1(%rip), %r8d
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph251
                                        #   Parent Loop BB0_34 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_39 Depth 3
                                        #       Child Loop BB0_58 Depth 3
                                        #       Child Loop BB0_62 Depth 3
                                        #       Child Loop BB0_65 Depth 3
                                        #       Child Loop BB0_70 Depth 3
                                        #       Child Loop BB0_74 Depth 3
                                        #       Child Loop BB0_92 Depth 3
                                        #       Child Loop BB0_96 Depth 3
                                        #       Child Loop BB0_99 Depth 3
                                        #       Child Loop BB0_103 Depth 3
	movl	%ebp, %ecx
	movsbq	384(%rsp,%rcx), %rcx
	movl	Mask(,%rcx,4), %eax
	movl	192(%rsp,%r15,4), %esi
	movl	%esi, %ecx
	andl	%r8d, %ecx
	movl	%esi, %edx
	shrl	%edx
	movl	%eax, 4(%rsp)           # 4-byte Spill
	andl	%eax, %edx
	orl	%ecx, %edx
	cmpl	%r9d, %r11d
	movl	%edx, 272(%rsp,%r15,4)
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	ja	.LBB0_41
# BB#38:                                # %.lr.ph
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	%r11d, %r9d
	subl	S(%rip), %r9d
	movl	%r11d, %r10d
	subl	DD(%rip), %r10d
                                        # kill: %R11D<def> %R11D<kill> %R11<def>
	subl	I(%rip), %r11d
	xorl	%edi, %edi
	movl	20(%rsp), %r13d         # 4-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	movl	4(%rsp), %r14d          # 4-byte Reload
	.p2align	4, 0x90
.LBB0_39:                               #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rsi,%rdi,4), %ebp
	movl	%r8d, %eax
	andl	%ebp, %eax
	shrl	%ebp
	andl	%r14d, %ebp
	leal	(%r11,%rdi), %ebx
	leal	(%r10,%rdi), %ecx
	leal	(%r9,%rdi), %edx
	movl	192(%rsp,%rdx,4), %edx
	orl	272(%rsp,%rcx,4), %edx
	shrl	%edx
	andl	%r13d, %edx
	orl	%eax, %ebp
	orl	192(%rsp,%rbx,4), %ebp
	orl	%edx, %ebp
	movl	%ebp, (%r12,%rdi,4)
	incq	%rdi
	cmpl	%edi, %r15d
	jne	.LBB0_39
# BB#40:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	272(%rsp,%r15,4), %edx
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	16(%rsp), %r11d         # 4-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
.LBB0_41:                               # %._crit_edge
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	%ebp, %r14d
	orl	$1, %r14d
	testl	24(%rsp), %edx          # 4-byte Folded Reload
	je	.LBB0_72
# BB#42:                                #   in Loop: Header=BB0_37 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	AND(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_46
# BB#43:                                #   in Loop: Header=BB0_37 Depth=2
	cmpl	$1, %eax
	jne	.LBB0_47
# BB#44:                                #   in Loop: Header=BB0_37 Depth=2
	movl	endposition(%rip), %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	272(%rsp,%rcx,4), %ecx
	andl	%eax, %ecx
	cmpl	%eax, %ecx
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	movl	4(%rsp), %r10d          # 4-byte Reload
	je	.LBB0_50
# BB#45:                                #   in Loop: Header=BB0_37 Depth=2
	xorl	%eax, %eax
	jmp	.LBB0_49
	.p2align	4, 0x90
.LBB0_46:                               #   in Loop: Header=BB0_37 Depth=2
	movl	endposition(%rip), %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	testl	272(%rsp,%rcx,4), %eax
	setne	%al
	jmp	.LBB0_48
	.p2align	4, 0x90
.LBB0_47:                               #   in Loop: Header=BB0_37 Depth=2
	xorl	%eax, %eax
.LBB0_48:                               # %thread-pre-split.thread
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	movl	4(%rsp), %r10d          # 4-byte Reload
.LBB0_49:                               # %thread-pre-split.thread
                                        #   in Loop: Header=BB0_37 Depth=2
	movzbl	%al, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB0_53
.LBB0_50:                               #   in Loop: Header=BB0_37 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB0_109
# BB#51:                                #   in Loop: Header=BB0_37 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	%eax, 120(%rsp)         # 4-byte Folded Reload
	jle	.LBB0_53
# BB#52:                                #   in Loop: Header=BB0_37 Depth=2
	movq	176(%rsp), %rax         # 8-byte Reload
	leal	(%r14,%rax), %edx
	leaq	384(%rsp), %rdi
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	8(%rsp), %esi           # 4-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r9d, %r13d
	movq	%r14, 128(%rsp)         # 8-byte Spill
	movl	%r11d, %r14d
	callq	output
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	%r14d, %r11d
	movq	128(%rsp), %r14         # 8-byte Reload
	movl	%r13d, %r9d
	movl	4(%rsp), %r10d          # 4-byte Reload
.LBB0_53:                               #   in Loop: Header=BB0_37 Depth=2
	cmpl	48(%rsp), %r9d          # 4-byte Folded Reload
	movl	$0, TRUNCATE(%rip)
	jb	.LBB0_67
# BB#54:                                # %.lr.ph232
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpq	$8, 88(%rsp)            # 8-byte Folded Reload
	movl	Init(%rip), %eax
	movq	%r15, %rdi
	jb	.LBB0_64
# BB#55:                                # %min.iters.checked327
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	movq	%r15, %rdi
	je	.LBB0_64
# BB#56:                                # %vector.ph331
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpq	$0, 160(%rsp)           # 8-byte Folded Reload
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	je	.LBB0_59
# BB#57:                                # %vector.body323.prol.preheader
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	144(%rsp), %rcx         # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_58:                               # %vector.body323.prol
                                        #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -16(%rbp,%rsi,4)
	movdqu	%xmm0, (%rbp,%rsi,4)
	movdqu	%xmm0, -16(%rbx,%rsi,4)
	movdqu	%xmm0, (%rbx,%rsi,4)
	addq	$8, %rsi
	incq	%rcx
	jne	.LBB0_58
	jmp	.LBB0_60
.LBB0_59:                               #   in Loop: Header=BB0_37 Depth=2
	xorl	%esi, %esi
.LBB0_60:                               # %vector.body323.prol.loopexit
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpq	$24, 168(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_63
# BB#61:                                # %vector.ph331.new
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	addq	%r15, %rsi
	leaq	384(%rsp), %rdx
	leaq	(%rdx,%rsi,4), %rdx
	leaq	304(%rsp), %rdi
	leaq	(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB0_62:                               # %vector.body323
                                        #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB0_62
.LBB0_63:                               # %middle.block324
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, 88(%rsp)          # 8-byte Folded Reload
	movq	152(%rsp), %rdi         # 8-byte Reload
	je	.LBB0_66
	.p2align	4, 0x90
.LBB0_64:                               # %scalar.ph325.preheader
                                        #   in Loop: Header=BB0_37 Depth=2
	leaq	192(%rsp,%rdi,4), %rcx
	leaq	272(%rsp,%rdi,4), %rdx
	movq	184(%rsp), %rsi         # 8-byte Reload
	subq	%rdi, %rsi
	.p2align	4, 0x90
.LBB0_65:                               # %scalar.ph325
                                        #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, (%rcx)
	movl	%eax, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	decq	%rsi
	jne	.LBB0_65
.LBB0_66:                               # %._crit_edge233.loopexit
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	192(%rsp,%r15,4), %esi
.LBB0_67:                               # %._crit_edge233
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	%r14d, %eax
	subl	40(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	Init1(%rip), %r8d
	movl	%esi, %eax
	andl	%r8d, %eax
	shrl	%esi
	andl	%r10d, %esi
	orl	%eax, %esi
	movl	%esi, %edx
	andl	104(%rsp), %edx         # 4-byte Folded Reload
	cmpl	%r9d, %r11d
	movl	%edx, 272(%rsp,%r15,4)
	jbe	.LBB0_69
# BB#68:                                #   in Loop: Header=BB0_37 Depth=2
	movq	64(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_72
	.p2align	4, 0x90
.LBB0_69:                               # %.lr.ph236
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	%r14, 128(%rsp)         # 8-byte Spill
	movl	%r11d, %r9d
	subl	S(%rip), %r9d
	movl	%r11d, %r10d
	subl	DD(%rip), %r10d
                                        # kill: %R11D<def> %R11D<kill> %R11<def>
	subl	I(%rip), %r11d
	xorl	%edi, %edi
	movq	%r12, %r14
	movl	20(%rsp), %r12d         # 4-byte Reload
	movq	112(%rsp), %r13         # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	.p2align	4, 0x90
.LBB0_70:                               #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r13,%rdi,4), %ebp
	movl	%r8d, %eax
	andl	%ebp, %eax
	shrl	%ebp
	andl	%esi, %ebp
	leal	(%r11,%rdi), %ebx
	leal	(%r10,%rdi), %ecx
	leal	(%r9,%rdi), %edx
	movl	192(%rsp,%rdx,4), %edx
	orl	272(%rsp,%rcx,4), %edx
	shrl	%edx
	andl	%r12d, %edx
	orl	%eax, %ebp
	orl	192(%rsp,%rbx,4), %ebp
	orl	%edx, %ebp
	movl	%ebp, (%r14,%rdi,4)
	incq	%rdi
	cmpl	%edi, %r15d
	jne	.LBB0_70
# BB#71:                                # %.loopexit221.loopexit
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	272(%rsp,%r15,4), %edx
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	16(%rsp), %r11d         # 4-byte Reload
	movq	%r14, %r12
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	128(%rsp), %r14         # 8-byte Reload
.LBB0_72:                               # %.loopexit221
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	%r14d, %eax
	movsbq	384(%rsp,%rax), %rax
	movl	Mask(,%rax,4), %r14d
	movl	%r8d, %eax
	andl	%edx, %eax
	movl	%edx, %ecx
	shrl	%ecx
	andl	%r14d, %ecx
	orl	%eax, %ecx
	cmpl	%r9d, %r11d
	movl	%ecx, 192(%rsp,%r15,4)
	ja	.LBB0_76
# BB#73:                                # %.lr.ph239
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%r11d, %r9d
	subl	S(%rip), %r9d
	movl	%r11d, %r10d
	subl	DD(%rip), %r10d
	movl	%r14d, %r13d
	movq	%r15, %r14
	movl	%r11d, %r15d
	subl	I(%rip), %r15d
	xorl	%edi, %edi
	movl	20(%rsp), %r11d         # 4-byte Reload
	movq	%r12, %rsi
	movq	112(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_74:                               #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rsi,%rdi,4), %ebx
	movl	%r8d, %eax
	andl	%ebx, %eax
	shrl	%ebx
	andl	%r13d, %ebx
	leal	(%r15,%rdi), %ebp
	leal	(%r10,%rdi), %ecx
	leal	(%r9,%rdi), %edx
	movl	272(%rsp,%rdx,4), %edx
	orl	192(%rsp,%rcx,4), %edx
	shrl	%edx
	andl	%r11d, %edx
	orl	%eax, %ebx
	orl	272(%rsp,%rbp,4), %ebx
	orl	%edx, %ebx
	movl	%ebx, (%r12,%rdi,4)
	incq	%rdi
	cmpl	%edi, %r14d
	jne	.LBB0_74
# BB#75:                                # %._crit_edge240.loopexit
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	192(%rsp,%r14,4), %ecx
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	16(%rsp), %r11d         # 4-byte Reload
	movq	%rsi, %r12
	movq	%r14, %r15
	movq	64(%rsp), %rbp          # 8-byte Reload
	movl	%r13d, %r14d
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB0_76:                               # %._crit_edge240
                                        #   in Loop: Header=BB0_37 Depth=2
	addl	$2, %ebp
	testl	24(%rsp), %ecx          # 4-byte Folded Reload
	je	.LBB0_105
# BB#77:                                #   in Loop: Header=BB0_37 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	AND(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_81
# BB#78:                                #   in Loop: Header=BB0_37 Depth=2
	cmpl	$1, %eax
	jne	.LBB0_82
# BB#79:                                #   in Loop: Header=BB0_37 Depth=2
	movl	endposition(%rip), %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	192(%rsp,%rcx,4), %ecx
	andl	%eax, %ecx
	cmpl	%eax, %ecx
	je	.LBB0_84
.LBB0_82:                               #   in Loop: Header=BB0_37 Depth=2
	xorl	%eax, %eax
	jmp	.LBB0_83
	.p2align	4, 0x90
.LBB0_81:                               #   in Loop: Header=BB0_37 Depth=2
	movl	endposition(%rip), %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	testl	192(%rsp,%rcx,4), %eax
	setne	%al
.LBB0_83:                               # %thread-pre-split219.thread
                                        #   in Loop: Header=BB0_37 Depth=2
	movzbl	%al, %eax
	cmpl	INVERSE(%rip), %eax
	je	.LBB0_87
.LBB0_84:                               #   in Loop: Header=BB0_37 Depth=2
	cmpl	$0, FILENAMEONLY(%rip)
	jne	.LBB0_109
# BB#85:                                #   in Loop: Header=BB0_37 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	120(%rsp), %eax         # 4-byte Folded Reload
	jge	.LBB0_87
# BB#86:                                #   in Loop: Header=BB0_37 Depth=2
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	%edx, 4(%rsp)           # 4-byte Spill
	leal	(%rbp,%rax), %edx
	leaq	384(%rsp), %rdi
	movl	8(%rsp), %esi           # 4-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r9d, %ebx
	movl	%r14d, %r13d
	movl	%r11d, %r14d
	callq	output
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%r14d, %r11d
	movl	%r13d, %r14d
	movl	%ebx, %r9d
.LBB0_87:                               #   in Loop: Header=BB0_37 Depth=2
	cmpl	48(%rsp), %r9d          # 4-byte Folded Reload
	movl	$0, TRUNCATE(%rip)
	jb	.LBB0_101
# BB#88:                                # %.lr.ph243
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpq	$8, 88(%rsp)            # 8-byte Folded Reload
	movl	Init(%rip), %eax
	movq	%r15, %rdi
	jb	.LBB0_98
# BB#89:                                # %min.iters.checked306
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	movq	%r15, %rdi
	je	.LBB0_98
# BB#90:                                # %vector.ph310
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpq	$0, 160(%rsp)           # 8-byte Folded Reload
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	je	.LBB0_93
# BB#91:                                # %vector.body302.prol.preheader
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	144(%rsp), %rcx         # 8-byte Reload
	xorl	%esi, %esi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_92:                               # %vector.body302.prol
                                        #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -16(%rdi,%rsi,4)
	movdqu	%xmm0, (%rdi,%rsi,4)
	movdqu	%xmm0, -16(%rdx,%rsi,4)
	movdqu	%xmm0, (%rdx,%rsi,4)
	addq	$8, %rsi
	incq	%rcx
	jne	.LBB0_92
	jmp	.LBB0_94
.LBB0_93:                               #   in Loop: Header=BB0_37 Depth=2
	xorl	%esi, %esi
.LBB0_94:                               # %vector.body302.prol.loopexit
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpq	$24, 168(%rsp)          # 8-byte Folded Reload
	jb	.LBB0_97
# BB#95:                                # %vector.ph310.new
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	subq	%rsi, %rcx
	addq	%r15, %rsi
	leaq	384(%rsp), %rdx
	leaq	(%rdx,%rsi,4), %rdx
	leaq	304(%rsp), %rdi
	leaq	(%rdi,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB0_96:                               # %vector.body302
                                        #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB0_96
.LBB0_97:                               # %middle.block303
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, 88(%rsp)          # 8-byte Folded Reload
	movq	152(%rsp), %rdi         # 8-byte Reload
	je	.LBB0_100
	.p2align	4, 0x90
.LBB0_98:                               # %scalar.ph304.preheader
                                        #   in Loop: Header=BB0_37 Depth=2
	leaq	192(%rsp,%rdi,4), %rcx
	leaq	272(%rsp,%rdi,4), %rdx
	movq	184(%rsp), %rsi         # 8-byte Reload
	subq	%rdi, %rsi
	.p2align	4, 0x90
.LBB0_99:                               # %scalar.ph304
                                        #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%eax, (%rcx)
	movl	%eax, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	decq	%rsi
	jne	.LBB0_99
.LBB0_100:                              # %._crit_edge244.loopexit
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	272(%rsp,%r15,4), %edx
.LBB0_101:                              # %._crit_edge244
                                        #   in Loop: Header=BB0_37 Depth=2
	movl	%ebp, %eax
	subl	40(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	Init1(%rip), %r8d
	movl	%edx, %eax
	andl	%r8d, %eax
	shrl	%edx
	andl	%r14d, %edx
	orl	%eax, %edx
	andl	104(%rsp), %edx         # 4-byte Folded Reload
	cmpl	%r9d, %r11d
	movl	%edx, 192(%rsp,%r15,4)
	ja	.LBB0_105
# BB#102:                               # %.lr.ph247
                                        #   in Loop: Header=BB0_37 Depth=2
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movl	%r11d, %r9d
	subl	S(%rip), %r9d
	movl	%r11d, %r10d
	subl	DD(%rip), %r10d
                                        # kill: %R11D<def> %R11D<kill> %R11<def>
	subl	I(%rip), %r11d
	xorl	%edi, %edi
	movl	20(%rsp), %r13d         # 4-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_103:                              #   Parent Loop BB0_34 Depth=1
                                        #     Parent Loop BB0_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r12,%rdi,4), %ebx
	movl	%r8d, %eax
	andl	%ebx, %eax
	shrl	%ebx
	andl	%r14d, %ebx
	leal	(%r11,%rdi), %ebp
	leal	(%r10,%rdi), %ecx
	leal	(%r9,%rdi), %edx
	movl	272(%rsp,%rdx,4), %edx
	orl	192(%rsp,%rcx,4), %edx
	shrl	%edx
	andl	%r13d, %edx
	orl	%eax, %ebx
	orl	272(%rsp,%rbp,4), %ebx
	orl	%edx, %ebx
	movl	%ebx, (%rsi,%rdi,4)
	incq	%rdi
	cmpl	%edi, %r15d
	jne	.LBB0_103
# BB#104:                               #   in Loop: Header=BB0_37 Depth=2
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	16(%rsp), %r11d         # 4-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
.LBB0_105:                              # %.backedge
                                        #   in Loop: Header=BB0_37 Depth=2
	cmpl	376(%rsp), %ebp         # 4-byte Folded Reload
	jb	.LBB0_37
.LBB0_106:                              # %._crit_edge252
                                        #   in Loop: Header=BB0_34 Depth=1
	movl	%r9d, %ebx
	movq	120(%rsp), %rax         # 8-byte Reload
	subl	8(%rsp), %eax           # 4-byte Folded Reload
	cmpl	$49153, %eax            # imm = 0xC001
	jl	.LBB0_108
# BB#107:                               #   in Loop: Header=BB0_34 Depth=1
	movl	$1, TRUNCATE(%rip)
	movl	$49152, %eax            # imm = 0xC000
.LBB0_108:                              #   in Loop: Header=BB0_34 Depth=1
	movslq	%eax, %r14
	leaq	49536(%rsp), %rbp
	movq	%rbp, %rdi
	subq	%r14, %rdi
	movslq	8(%rsp), %rax           # 4-byte Folded Reload
	leaq	384(%rsp,%rax), %rsi
	movq	%r14, %rdx
	callq	strncpy
	movl	$49152, %ecx            # imm = 0xC000
	subl	%r14d, %ecx
	movl	$1, %eax
	cmovsl	%eax, %ecx
	cmpl	$49152, 368(%rsp)       # 4-byte Folded Reload
                                        # imm = 0xC000
	movl	$49152, %eax            # imm = 0xC000
	cmovll	%eax, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	$49152, %edx            # imm = 0xC000
	movl	140(%rsp), %r14d        # 4-byte Reload
	movl	%r14d, %edi
	movq	%rbp, %rsi
	callq	fill_buf
	movl	%eax, %ecx
	testl	%ecx, %ecx
	movl	16(%rsp), %r11d         # 4-byte Reload
	movl	%ebx, %r9d
	jg	.LBB0_34
	jmp	.LBB0_110
.LBB0_109:
	incl	num_of_matched(%rip)
	movl	$CurrentFileName, %edi
	callq	puts
.LBB0_110:                              # %.loopexit223
	addq	$98696, %rsp            # imm = 0x18188
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	asearch1, .Lfunc_end0-asearch1
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
