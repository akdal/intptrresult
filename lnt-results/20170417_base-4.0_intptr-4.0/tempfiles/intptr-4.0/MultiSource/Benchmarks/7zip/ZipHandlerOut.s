	.text
	.file	"ZipHandlerOut.bc"
	.globl	_ZN8NArchive4NZip8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler15GetFileTimeTypeEPj,@function
_ZN8NArchive4NZip8CHandler15GetFileTimeTypeEPj: # @_ZN8NArchive4NZip8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$2, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN8NArchive4NZip8CHandler15GetFileTimeTypeEPj, .Lfunc_end0-_ZN8NArchive4NZip8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.globl	_ZThn8_N8NArchive4NZip8CHandler15GetFileTimeTypeEPj
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NZip8CHandler15GetFileTimeTypeEPj,@function
_ZThn8_N8NArchive4NZip8CHandler15GetFileTimeTypeEPj: # @_ZThn8_N8NArchive4NZip8CHandler15GetFileTimeTypeEPj
	.cfi_startproc
# BB#0:
	movl	$2, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	_ZThn8_N8NArchive4NZip8CHandler15GetFileTimeTypeEPj, .Lfunc_end1-_ZThn8_N8NArchive4NZip8CHandler15GetFileTimeTypeEPj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZN8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZN8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi6:
	.cfi_def_cfa_offset 496
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, %r13d
	movq	%rsi, 432(%rsp)         # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	movups	%xmm0, 312(%rsp)
	movq	$8, 328(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE+16, 304(%rsp)
	xorl	%r15d, %r15d
	testl	%r13d, %r13d
	je	.LBB2_1
# BB#11:                                # %.lr.ph774
	leaq	88(%rsp), %rbp
	leaq	28(%rsp), %r12
                                        # implicit-def: %R14D
	xorl	%eax, %eax
	movq	%rax, 272(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movl	%r13d, 24(%rsp)         # 4-byte Spill
	jmp	.LBB2_13
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_13 Depth=1
	incl	%r15d
	cmpl	%r13d, %r15d
	jb	.LBB2_13
	jmp	.LBB2_2
.LBB2_52:                               #   in Loop: Header=BB2_13 Depth=1
	xorl	%eax, %eax
.LBB2_55:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rbp, (%rsp)
	movl	$0, (%rbp,%rax,4)
	movl	%r13d, 12(%rsp)
	movq	%rbp, %rbx
.LBB2_56:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	decl	%r13d
	.p2align	4, 0x90
.LBB2_57:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12), %eax
	addq	$4, %r12
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB2_57
# BB#58:                                # %_ZN11CStringBaseIwEaSEPKw.exit
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	%r13d, 8(%rsp)
	xorl	%ebx, %ebx
	jmp	.LBB2_59
.LBB2_109:                              #   in Loop: Header=BB2_13 Depth=1
	movl	%ebx, %ebp
	movq	$0, 144(%rsp)
	movl	112(%rsp), %eax
	orl	116(%rsp), %eax
	leaq	144(%rsp), %rbx
	je	.LBB2_112
# BB#110:                               #   in Loop: Header=BB2_13 Depth=1
.Ltmp67:
	leaq	112(%rsp), %rdi
	movq	%rbx, %rsi
	callq	FileTimeToLocalFileTime
.Ltmp68:
# BB#111:                               #   in Loop: Header=BB2_13 Depth=1
	testl	%eax, %eax
	je	.LBB2_129
.LBB2_112:                              #   in Loop: Header=BB2_13 Depth=1
.Ltmp69:
	movq	%rbx, %rdi
	leaq	84(%rsp), %rsi
	callq	_ZN8NWindows5NTime17FileTimeToDosTimeERK9_FILETIMERj
.Ltmp70:
# BB#113:                               #   in Loop: Header=BB2_13 Depth=1
.Ltmp72:
	movq	%rbx, %rdi
	movq	%rsp, %rsi
	callq	_ZN8NArchive9NItemName13MakeLegalNameERK11CStringBaseIwE
.Ltmp73:
# BB#114:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$0, 8(%rsp)
	movq	(%rsp), %r12
	movl	$0, (%r12)
	movslq	152(%rsp), %r14
	incq	%r14
	movl	12(%rsp), %ebx
	cmpl	%ebx, %r14d
	je	.LBB2_133
# BB#115:                               #   in Loop: Header=BB2_13 Depth=1
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp75:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp76:
# BB#116:                               # %.noexc334
                                        #   in Loop: Header=BB2_13 Depth=1
	testq	%r12, %r12
	je	.LBB2_117
# BB#130:                               # %.noexc334
                                        #   in Loop: Header=BB2_13 Depth=1
	testl	%ebx, %ebx
	movl	$0, %ecx
	jle	.LBB2_132
# BB#131:                               # %._crit_edge.thread.i.i329
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%r12, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	8(%rsp), %rcx
	jmp	.LBB2_132
.LBB2_129:                              #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	jmp	.LBB2_226
.LBB2_117:                              #   in Loop: Header=BB2_13 Depth=1
	xorl	%ecx, %ecx
.LBB2_132:                              # %._crit_edge16.i.i330
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rax, (%rsp)
	movl	$0, (%rax,%rcx,4)
	movl	%r14d, 12(%rsp)
	movq	%rax, %r12
.LBB2_133:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i331
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	144(%rsp), %rdi
	xorl	%eax, %eax
.LBB2_134:                              #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%r12,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_134
# BB#135:                               #   in Loop: Header=BB2_13 Depth=1
	movl	152(%rsp), %r12d
	movl	%r12d, 8(%rsp)
	testq	%rdi, %rdi
	je	.LBB2_137
# BB#136:                               #   in Loop: Header=BB2_13 Depth=1
	callq	_ZdaPv
	movl	8(%rsp), %r12d
.LBB2_137:                              # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB2_13 Depth=1
	movb	66(%rsp), %al
	testl	%r12d, %r12d
	je	.LBB2_145
# BB#138:                               #   in Loop: Header=BB2_13 Depth=1
	movq	(%rsp), %rcx
	movslq	%r12d, %rdx
	cmpl	$47, -4(%rcx,%rdx,4)
	jne	.LBB2_145
# BB#139:                               #   in Loop: Header=BB2_13 Depth=1
	testb	%al, %al
	movq	32(%rsp), %rax          # 8-byte Reload
	jne	.LBB2_177
# BB#140:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	jmp	.LBB2_226
.LBB2_145:                              #   in Loop: Header=BB2_13 Depth=1
	testb	%al, %al
	je	.LBB2_176
# BB#146:                               #   in Loop: Header=BB2_13 Depth=1
	movl	12(%rsp), %ebx
	movl	%ebx, %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	jg	.LBB2_175
# BB#147:                               #   in Loop: Header=BB2_13 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %ebx
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %ebx
	jl	.LBB2_149
# BB#148:                               # %select.true.sink
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	%ebx, %edx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
.LBB2_149:                              # %select.end
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbx,%rsi), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	cmpl	%ebx, %eax
	je	.LBB2_175
# BB#150:                               #   in Loop: Header=BB2_13 Depth=1
	movslq	44(%rsp), %rax          # 4-byte Folded Reload
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp78:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp79:
# BB#151:                               # %.noexc341
                                        #   in Loop: Header=BB2_13 Depth=1
	testl	%ebx, %ebx
	jle	.LBB2_174
# BB#152:                               # %.preheader.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	(%rsp), %rdi
	testl	%r12d, %r12d
	jle	.LBB2_172
# BB#153:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movslq	%r12d, %rax
	cmpl	$7, %r12d
	jbe	.LBB2_154
# BB#161:                               # %min.iters.checked802
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB2_154
# BB#162:                               # %vector.memcheck814
                                        #   in Loop: Header=BB2_13 Depth=1
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r14
	jae	.LBB2_164
# BB#163:                               # %vector.memcheck814
                                        #   in Loop: Header=BB2_13 Depth=1
	leaq	(%r14,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_164
.LBB2_154:                              #   in Loop: Header=BB2_13 Depth=1
	xorl	%ecx, %ecx
.LBB2_155:                              # %scalar.ph800.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	subl	%ecx, %r12d
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %r12
	je	.LBB2_158
# BB#156:                               # %scalar.ph800.prol.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	negq	%r12
.LBB2_157:                              # %scalar.ph800.prol
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r14,%rcx,4)
	incq	%rcx
	incq	%r12
	jne	.LBB2_157
.LBB2_158:                              # %scalar.ph800.prol.loopexit
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpq	$7, %rdx
	jb	.LBB2_173
# BB#159:                               # %scalar.ph800.preheader.new
                                        #   in Loop: Header=BB2_13 Depth=1
	subq	%rcx, %rax
	leaq	28(%r14,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
.LBB2_160:                              # %scalar.ph800
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB2_160
	jmp	.LBB2_173
.LBB2_172:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	testq	%rdi, %rdi
	je	.LBB2_174
.LBB2_173:                              # %._crit_edge.thread.i.i338
                                        #   in Loop: Header=BB2_13 Depth=1
	callq	_ZdaPv
	movl	8(%rsp), %r12d
.LBB2_174:                              # %._crit_edge16.i.i339
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%r14, (%rsp)
	movslq	%r12d, %rax
	movl	$0, (%r14,%rax,4)
	movl	44(%rsp), %eax          # 4-byte Reload
	movl	%eax, 12(%rsp)
.LBB2_175:                              # %_ZN11CStringBaseIwEpLEw.exit
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	(%rsp), %rax
	movslq	%r12d, %rcx
	movl	$47, (%rax,%rcx,4)
	leal	1(%rcx), %r12d
	movl	%r12d, 8(%rsp)
	movl	$0, 4(%rax,%rcx,4)
.LBB2_176:                              # %.thread
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
.LBB2_177:                              # %.thread
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpb	$0, 249(%rax)
	je	.LBB2_178
.LBB2_179:                              #   in Loop: Header=BB2_13 Depth=1
.Ltmp81:
	movl	$1, %edx
	leaq	144(%rsp), %rdi
	movq	%rsp, %rsi
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp82:
# BB#180:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$0, 104(%rsp)
	movq	96(%rsp), %rax
	movb	$0, (%rax)
	movslq	152(%rsp), %rax
	leaq	1(%rax), %r12
	movl	108(%rsp), %ebx
	cmpl	%ebx, %r12d
	jne	.LBB2_182
# BB#181:                               # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	96(%rsp), %r14
	jmp	.LBB2_207
.LBB2_178:                              #   in Loop: Header=BB2_13 Depth=1
	cmpb	$0, 250(%rax)
	jne	.LBB2_213
	jmp	.LBB2_179
.LBB2_182:                              #   in Loop: Header=BB2_13 Depth=1
	cmpl	$-1, %eax
	movq	%r12, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp84:
	callq	_Znam
	movq	%rax, %r14
.Ltmp85:
# BB#183:                               # %.noexc352
                                        #   in Loop: Header=BB2_13 Depth=1
	testl	%ebx, %ebx
	jle	.LBB2_206
# BB#184:                               # %.preheader.i.i343
                                        #   in Loop: Header=BB2_13 Depth=1
	movslq	104(%rsp), %rax
	testq	%rax, %rax
	movq	96(%rsp), %rdi
	jle	.LBB2_204
# BB#185:                               # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpl	$31, %eax
	jbe	.LBB2_186
# BB#193:                               # %min.iters.checked
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB2_186
# BB#194:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_13 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r14
	jae	.LBB2_196
# BB#195:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_13 Depth=1
	leaq	(%r14,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_196
.LBB2_186:                              #   in Loop: Header=BB2_13 Depth=1
	xorl	%ecx, %ecx
.LBB2_187:                              # %.lr.ph.i.i348.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB2_190
# BB#188:                               # %.lr.ph.i.i348.prol.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	negq	%rsi
.LBB2_189:                              # %.lr.ph.i.i348.prol
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%r14,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_189
.LBB2_190:                              # %.lr.ph.i.i348.prol.loopexit
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpq	$7, %rdx
	jb	.LBB2_205
# BB#191:                               # %.lr.ph.i.i348.preheader.new
                                        #   in Loop: Header=BB2_13 Depth=1
	subq	%rcx, %rax
	leaq	7(%r14,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB2_192:                              # %.lr.ph.i.i348
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB2_192
	jmp	.LBB2_205
.LBB2_204:                              # %._crit_edge.i.i344
                                        #   in Loop: Header=BB2_13 Depth=1
	testq	%rdi, %rdi
	je	.LBB2_206
.LBB2_205:                              # %._crit_edge.thread.i.i349
                                        #   in Loop: Header=BB2_13 Depth=1
	callq	_ZdaPv
.LBB2_206:                              # %._crit_edge17.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%r14, 96(%rsp)
	movslq	104(%rsp), %rax
	movb	$0, (%r14,%rax)
	movl	%r12d, 108(%rsp)
.LBB2_207:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	144(%rsp), %rax
.LBB2_208:                              #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%r14)
	incq	%r14
	testb	%cl, %cl
	jne	.LBB2_208
# BB#209:                               #   in Loop: Header=BB2_13 Depth=1
	movl	152(%rsp), %eax
	movl	%eax, 104(%rsp)
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_211
# BB#210:                               #   in Loop: Header=BB2_13 Depth=1
	callq	_ZdaPv
.LBB2_211:                              #   in Loop: Header=BB2_13 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpb	$1, 249(%rax)
	jne	.LBB2_212
# BB#224:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	cmpl	$65535, 104(%rsp)       # imm = 0xFFFF
	jle	.LBB2_225
	jmp	.LBB2_226
.LBB2_212:                              # %..thread446.preheader_crit_edge
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	8(%rsp), %r12d
.LBB2_213:                              # %.thread446.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	testl	%r12d, %r12d
	jle	.LBB2_214
# BB#218:                               # %.lr.ph
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	(%rsp), %rax
	movl	8(%rsp), %r12d
	movslq	%r12d, %rdx
	xorl	%ecx, %ecx
.LBB2_219:                              #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$127, (%rax,%rcx,4)
	ja	.LBB2_221
# BB#220:                               # %.thread446
                                        #   in Loop: Header=BB2_219 Depth=2
	incq	%rcx
	cmpq	%rdx, %rcx
	jl	.LBB2_219
	jmp	.LBB2_221
.LBB2_214:                              #   in Loop: Header=BB2_13 Depth=1
	xorl	%ecx, %ecx
.LBB2_221:                              # %.critedge
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpl	%r12d, %ecx
	setne	68(%rsp)
.Ltmp87:
	movq	%rsp, %rdi
	leaq	96(%rsp), %rsi
	callq	_Z20ConvertUnicodeToUTF8RK11CStringBaseIwERS_IcE
.Ltmp88:
# BB#222:                               #   in Loop: Header=BB2_13 Depth=1
	cmpl	$65535, 104(%rsp)       # imm = 0xFFFF
	movl	$-2147024809, %r12d     # imm = 0x80070057
	jg	.LBB2_226
# BB#223:                               #   in Loop: Header=BB2_13 Depth=1
	xorb	$1, %al
	jne	.LBB2_226
.LBB2_225:                              #   in Loop: Header=BB2_13 Depth=1
	movl	%r15d, 76(%rsp)
	xorl	%r13d, %r13d
	movl	%ebp, %r12d
	jmp	.LBB2_226
.LBB2_196:                              # %vector.body.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_197
# BB#198:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	negq	%rsi
	xorl	%ebx, %ebx
.LBB2_199:                              # %vector.body.prol
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%r14,%rbx)
	movups	%xmm1, 16(%r14,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB2_199
	jmp	.LBB2_200
.LBB2_197:                              #   in Loop: Header=BB2_13 Depth=1
	xorl	%ebx, %ebx
.LBB2_200:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpq	$96, %rdx
	jb	.LBB2_203
# BB#201:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r14,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
.LBB2_202:                              # %vector.body
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB2_202
.LBB2_203:                              # %middle.block
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB2_187
	jmp	.LBB2_205
.LBB2_164:                              # %vector.body798.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_165
# BB#166:                               # %vector.body798.prol.preheader
                                        #   in Loop: Header=BB2_13 Depth=1
	negq	%rsi
	xorl	%ebx, %ebx
.LBB2_167:                              # %vector.body798.prol
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r14,%rbx,4)
	movups	%xmm1, 16(%r14,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB2_167
	jmp	.LBB2_168
.LBB2_165:                              #   in Loop: Header=BB2_13 Depth=1
	xorl	%ebx, %ebx
.LBB2_168:                              # %vector.body798.prol.loopexit
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpq	$24, %rdx
	jb	.LBB2_171
# BB#169:                               # %vector.body798.preheader.new
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r14,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
.LBB2_170:                              # %vector.body798
                                        #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB2_170
.LBB2_171:                              # %middle.block799
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB2_155
	jmp	.LBB2_173
	.p2align	4, 0x90
.LBB2_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_48 Depth 2
                                        #     Child Loop BB2_57 Depth 2
                                        #     Child Loop BB2_134 Depth 2
                                        #     Child Loop BB2_167 Depth 2
                                        #     Child Loop BB2_170 Depth 2
                                        #     Child Loop BB2_157 Depth 2
                                        #     Child Loop BB2_160 Depth 2
                                        #     Child Loop BB2_199 Depth 2
                                        #     Child Loop BB2_202 Depth 2
                                        #     Child Loop BB2_189 Depth 2
                                        #     Child Loop BB2_192 Depth 2
                                        #     Child Loop BB2_208 Depth 2
                                        #     Child Loop BB2_219 Depth 2
                                        #     Child Loop BB2_249 Depth 2
	movw	$0, 67(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	$0, 16(%rbp)
.Ltmp0:
	movl	$4, %edi
	callq	_Znam
.Ltmp1:
# BB#14:                                #   in Loop: Header=BB2_13 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movq	%rax, 96(%rsp)
	movb	$0, (%rax)
	movl	$4, 108(%rsp)
	movl	$1, %ebx
	je	.LBB2_15
# BB#16:                                #   in Loop: Header=BB2_13 Depth=1
	movq	(%rcx), %rax
.Ltmp3:
	movq	%rcx, %rdi
	movl	%r15d, %esi
	movq	%r12, %rdx
	leaq	300(%rsp), %rcx
	leaq	296(%rsp), %r8
	callq	*56(%rax)
.Ltmp4:
# BB#17:                                #   in Loop: Header=BB2_13 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r14d
	je	.LBB2_19
# BB#18:                                #   in Loop: Header=BB2_13 Depth=1
	movl	%eax, %r14d
	jmp	.LBB2_253
	.p2align	4, 0x90
.LBB2_15:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147467259, %r14d     # imm = 0x80004005
	jmp	.LBB2_253
	.p2align	4, 0x90
.LBB2_19:                               #   in Loop: Header=BB2_13 Depth=1
	movl	300(%rsp), %ecx
	testl	%ecx, %ecx
	setne	65(%rsp)
	movl	28(%rsp), %eax
	testl	%eax, %eax
	setne	64(%rsp)
	movslq	296(%rsp), %rdx
	cmpq	$-1, %rdx
	movl	%edx, 72(%rsp)
	movl	%r15d, 76(%rsp)
	je	.LBB2_25
# BB#20:                                #   in Loop: Header=BB2_13 Depth=1
	testl	%eax, %eax
	je	.LBB2_25
# BB#21:                                #   in Loop: Header=BB2_13 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsi), %rsi
	movq	(%rsi,%rdx,8), %rdx
	movzwl	2(%rdx), %esi
	testb	$1, %sil
	je	.LBB2_25
# BB#22:                                #   in Loop: Header=BB2_13 Depth=1
	andl	$65, %esi
	cmpl	$65, %esi
	je	.LBB2_24
# BB#23:                                # %_ZNK8NArchive4NZip10CLocalItem14IsAesEncryptedEv.exit
                                        #   in Loop: Header=BB2_13 Depth=1
	movzwl	4(%rdx), %edx
	cmpl	$99, %edx
	jne	.LBB2_25
.LBB2_24:                               # %_ZNK8NArchive4NZip10CLocalItem14IsAesEncryptedEv.exit.thread
                                        #   in Loop: Header=BB2_13 Depth=1
	movb	$1, %dl
	movq	%rdx, 272(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_25:                               # %_ZNK8NArchive4NZip10CLocalItem14IsAesEncryptedEv.exit.thread443
                                        #   in Loop: Header=BB2_13 Depth=1
	testl	%ecx, %ecx
	je	.LBB2_26
# BB#28:                                #   in Loop: Header=BB2_13 Depth=1
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp6:
	movl	$16, %edi
	callq	_Znam
.Ltmp7:
# BB#29:                                #   in Loop: Header=BB2_13 Depth=1
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	$4, 12(%rsp)
	movl	$0, 416(%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp8:
	movl	$9, %edx
	movl	%r15d, %esi
	leaq	416(%rsp), %rbp
	movq	%rbp, %rcx
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp9:
# BB#30:                                #   in Loop: Header=BB2_13 Depth=1
	testl	%r12d, %r12d
	cmovnel	%r12d, %r14d
	movl	$1, %ebx
	jne	.LBB2_35
# BB#31:                                #   in Loop: Header=BB2_13 Depth=1
	movzwl	416(%rsp), %eax
	testw	%ax, %ax
	movl	$0, %ecx
	je	.LBB2_34
# BB#32:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$19, %eax
	jne	.LBB2_35
# BB#33:                                #   in Loop: Header=BB2_13 Depth=1
	movl	424(%rsp), %ecx
.LBB2_34:                               #   in Loop: Header=BB2_13 Depth=1
	movl	%ecx, 80(%rsp)
	xorl	%ebx, %ebx
	movl	%r14d, %r12d
.LBB2_35:                               #   in Loop: Header=BB2_13 Depth=1
.Ltmp13:
	movq	%rbp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp14:
# BB#36:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit298
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	$1, %r13d
	testl	%ebx, %ebx
	jne	.LBB2_226
# BB#37:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$0, 352(%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp16:
	movl	$3, %edx
	movl	%r15d, %esi
	leaq	352(%rsp), %rcx
	callq	*64(%rax)
.Ltmp17:
# BB#38:                                #   in Loop: Header=BB2_13 Depth=1
	testl	%eax, %eax
	movl	%eax, %r14d
	cmovel	%r12d, %r14d
	movl	$1, %ebx
	je	.LBB2_44
# BB#39:                                #   in Loop: Header=BB2_13 Depth=1
	movl	%eax, %r14d
.LBB2_59:                               #   in Loop: Header=BB2_13 Depth=1
.Ltmp23:
	leaq	352(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp24:
# BB#60:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit301
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	$1, %r13d
	testl	%ebx, %ebx
	je	.LBB2_62
# BB#61:                                #   in Loop: Header=BB2_13 Depth=1
	movl	%r14d, %r12d
	jmp	.LBB2_226
.LBB2_26:                               #   in Loop: Header=BB2_13 Depth=1
	movl	%r14d, %r12d
	jmp	.LBB2_231
.LBB2_44:                               #   in Loop: Header=BB2_13 Depth=1
	movzwl	352(%rsp), %eax
	cmpl	$8, %eax
	je	.LBB2_47
# BB#45:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147024809, %r14d     # imm = 0x80070057
	testw	%ax, %ax
	jne	.LBB2_59
# BB#46:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$0, 8(%rsp)
	movq	(%rsp), %rax
	movl	$0, (%rax)
	xorl	%ebx, %ebx
	movl	%r12d, %r14d
	jmp	.LBB2_59
.LBB2_62:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$0, 400(%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp26:
	movl	$6, %edx
	movl	%r15d, %esi
	leaq	400(%rsp), %rbp
	movq	%rbp, %rcx
	callq	*64(%rax)
	movl	%eax, %ebx
.Ltmp27:
# BB#63:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$1, %r12d
	testl	%ebx, %ebx
	jne	.LBB2_71
# BB#64:                                #   in Loop: Header=BB2_13 Depth=1
	movzwl	400(%rsp), %eax
	cmpl	$11, %eax
	je	.LBB2_69
# BB#65:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147024809, %ebx      # imm = 0x80070057
	testw	%ax, %ax
	jne	.LBB2_71
# BB#66:                                #   in Loop: Header=BB2_13 Depth=1
	movb	$0, 66(%rsp)
	jmp	.LBB2_70
.LBB2_47:                               #   in Loop: Header=BB2_13 Depth=1
	movq	360(%rsp), %r12
	movl	$0, 8(%rsp)
	movq	(%rsp), %rbx
	movl	$0, (%rbx)
	xorl	%r13d, %r13d
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB2_48:                               #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r13d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB2_48
# BB#49:                                # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	12(%rsp), %eax
	cmpl	%r13d, %eax
	je	.LBB2_56
# BB#50:                                #   in Loop: Header=BB2_13 Depth=1
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp18:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp19:
# BB#51:                                # %.noexc
                                        #   in Loop: Header=BB2_13 Depth=1
	testq	%rbx, %rbx
	je	.LBB2_52
# BB#53:                                # %.noexc
                                        #   in Loop: Header=BB2_13 Depth=1
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	movl	$0, %eax
	jle	.LBB2_55
# BB#54:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	jmp	.LBB2_55
.LBB2_69:                               #   in Loop: Header=BB2_13 Depth=1
	cmpw	$0, 408(%rsp)
	setne	66(%rsp)
.LBB2_70:                               #   in Loop: Header=BB2_13 Depth=1
	xorl	%r12d, %r12d
	movl	%r14d, %ebx
.LBB2_71:                               #   in Loop: Header=BB2_13 Depth=1
.Ltmp31:
	movq	%rbp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp32:
# BB#72:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit305
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	$1, %r13d
	testl	%r12d, %r12d
	je	.LBB2_74
# BB#73:                                #   in Loop: Header=BB2_13 Depth=1
	movl	%ebx, %r12d
	jmp	.LBB2_226
.LBB2_74:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$0, 384(%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp34:
	movl	$40, %edx
	movl	%r15d, %esi
	leaq	384(%rsp), %r14
	movq	%r14, %rcx
	callq	*64(%rax)
.Ltmp35:
	movq	32(%rsp), %rcx          # 8-byte Reload
# BB#75:                                #   in Loop: Header=BB2_13 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %ebx
	movl	$1, %ebp
	jne	.LBB2_82
# BB#76:                                #   in Loop: Header=BB2_13 Depth=1
	movzwl	384(%rsp), %eax
	cmpl	$19, %eax
	jne	.LBB2_80
# BB#77:                                #   in Loop: Header=BB2_13 Depth=1
	cmpl	$0, 392(%rsp)
	sete	%al
	jmp	.LBB2_81
.LBB2_80:                               #   in Loop: Header=BB2_13 Depth=1
	cmpb	$0, 248(%rcx)
	setne	%al
.LBB2_81:                               #   in Loop: Header=BB2_13 Depth=1
	movb	%al, 67(%rsp)
	xorl	%ebp, %ebp
.LBB2_82:                               #   in Loop: Header=BB2_13 Depth=1
.Ltmp39:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp40:
# BB#83:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit310
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	$1, %r13d
	testl	%ebp, %ebp
	je	.LBB2_85
# BB#84:                                #   in Loop: Header=BB2_13 Depth=1
	movl	%ebx, %r12d
	jmp	.LBB2_226
.LBB2_85:                               #   in Loop: Header=BB2_13 Depth=1
	movq	$0, 112(%rsp)
	movl	$0, 368(%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp42:
	movl	$12, %edx
	movl	%r15d, %esi
	leaq	368(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp43:
# BB#86:                                #   in Loop: Header=BB2_13 Depth=1
	testl	%r12d, %r12d
	jne	.LBB2_91
# BB#87:                                #   in Loop: Header=BB2_13 Depth=1
	movzwl	368(%rsp), %eax
	testw	%ax, %ax
	je	.LBB2_90
# BB#88:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB2_91
# BB#89:                                #   in Loop: Header=BB2_13 Depth=1
	movq	376(%rsp), %rax
	movq	%rax, 112(%rsp)
.LBB2_90:                               #   in Loop: Header=BB2_13 Depth=1
	xorl	%r12d, %r12d
.LBB2_91:                               #   in Loop: Header=BB2_13 Depth=1
.Ltmp48:
	leaq	368(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp49:
# BB#92:                                #   in Loop: Header=BB2_13 Depth=1
	testl	%r12d, %r12d
	cmovnel	%r12d, %ebx
	jne	.LBB2_226
# BB#93:                                #   in Loop: Header=BB2_13 Depth=1
	movq	$0, 120(%rsp)
	movl	$0, 280(%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp50:
	movl	$11, %edx
	movl	%r15d, %esi
	leaq	280(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp51:
# BB#94:                                #   in Loop: Header=BB2_13 Depth=1
	testl	%r12d, %r12d
	jne	.LBB2_99
# BB#95:                                #   in Loop: Header=BB2_13 Depth=1
	movzwl	280(%rsp), %eax
	testw	%ax, %ax
	je	.LBB2_98
# BB#96:                                #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB2_99
# BB#97:                                #   in Loop: Header=BB2_13 Depth=1
	movq	288(%rsp), %rax
	movq	%rax, 120(%rsp)
.LBB2_98:                               #   in Loop: Header=BB2_13 Depth=1
	xorl	%r12d, %r12d
.LBB2_99:                               #   in Loop: Header=BB2_13 Depth=1
.Ltmp56:
	leaq	280(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp57:
# BB#100:                               #   in Loop: Header=BB2_13 Depth=1
	testl	%r12d, %r12d
	cmovnel	%r12d, %ebx
	jne	.LBB2_226
# BB#101:                               #   in Loop: Header=BB2_13 Depth=1
	movq	$0, 128(%rsp)
	movl	$0, 48(%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp58:
	movl	$10, %edx
	movl	%r15d, %esi
	leaq	48(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r12d
.Ltmp59:
# BB#102:                               #   in Loop: Header=BB2_13 Depth=1
	testl	%r12d, %r12d
	jne	.LBB2_107
# BB#103:                               #   in Loop: Header=BB2_13 Depth=1
	movzwl	48(%rsp), %eax
	testw	%ax, %ax
	je	.LBB2_106
# BB#104:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$64, %eax
	jne	.LBB2_107
# BB#105:                               #   in Loop: Header=BB2_13 Depth=1
	movq	56(%rsp), %rax
	movq	%rax, 128(%rsp)
.LBB2_106:                              #   in Loop: Header=BB2_13 Depth=1
	xorl	%r12d, %r12d
.LBB2_107:                              #   in Loop: Header=BB2_13 Depth=1
.Ltmp64:
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp65:
# BB#108:                               #   in Loop: Header=BB2_13 Depth=1
	testl	%r12d, %r12d
	cmovnel	%r12d, %ebx
	je	.LBB2_109
.LBB2_226:                              #   in Loop: Header=BB2_13 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_228
# BB#227:                               #   in Loop: Header=BB2_13 Depth=1
	callq	_ZdaPv
.LBB2_228:                              # %_ZN11CStringBaseIwED2Ev.exit354
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	$1, %ebx
	testl	%r13d, %r13d
	je	.LBB2_230
# BB#229:                               #   in Loop: Header=BB2_13 Depth=1
	movl	%r12d, %r14d
	movl	24(%rsp), %r13d         # 4-byte Reload
	leaq	88(%rsp), %rbp
	jmp	.LBB2_252
.LBB2_230:                              # %_ZN11CStringBaseIwED2Ev.exit354._crit_edge
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	28(%rsp), %eax
	movl	24(%rsp), %r13d         # 4-byte Reload
.LBB2_231:                              #   in Loop: Header=BB2_13 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB2_232
# BB#233:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$0, 336(%rsp)
	movq	(%rcx), %rax
.Ltmp90:
	movl	$7, %edx
	movq	%rcx, %rdi
	movl	%r15d, %esi
	leaq	336(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r14d
.Ltmp91:
# BB#234:                               #   in Loop: Header=BB2_13 Depth=1
	testl	%r14d, %r14d
	je	.LBB2_237
# BB#235:                               #   in Loop: Header=BB2_13 Depth=1
	movl	$1, %r13d
	jmp	.LBB2_238
.LBB2_232:                              #   in Loop: Header=BB2_13 Depth=1
	movl	%r12d, %r14d
	jmp	.LBB2_243
.LBB2_237:                              #   in Loop: Header=BB2_13 Depth=1
	movzwl	336(%rsp), %eax
	xorl	%r13d, %r13d
	cmpl	$21, %eax
	movq	264(%rsp), %rax         # 8-byte Reload
	cmoveq	344(%rsp), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	setne	%r13b
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmovnel	%eax, %r12d
	movl	%r12d, %r14d
.LBB2_238:                              #   in Loop: Header=BB2_13 Depth=1
	leaq	88(%rsp), %rbp
	leaq	28(%rsp), %r12
.Ltmp95:
	leaq	336(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp96:
# BB#239:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit359
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	$1, %ebx
	testl	%r13d, %r13d
	je	.LBB2_242
# BB#240:                               #   in Loop: Header=BB2_13 Depth=1
	movl	24(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB2_253
.LBB2_242:                              # %.thread447
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	264(%rsp), %rax         # 8-byte Reload
	movq	%rax, 88(%rsp)
	movl	24(%rsp), %r13d         # 4-byte Reload
.LBB2_243:                              #   in Loop: Header=BB2_13 Depth=1
.Ltmp98:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp99:
# BB#244:                               # %.noexc360
                                        #   in Loop: Header=BB2_13 Depth=1
	movups	64(%rsp), %xmm0
	movups	80(%rsp), %xmm1
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movslq	104(%rsp), %rax
	leaq	1(%rax), %rbp
	testl	%ebp, %ebp
	je	.LBB2_245
# BB#246:                               #   in Loop: Header=BB2_13 Depth=1
	cmpl	$-1, %eax
	movq	%rbp, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp100:
	callq	_Znam
.Ltmp101:
# BB#247:                               # %.noexc.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	%rax, 32(%rbx)
	movb	$0, (%rax)
	movl	%ebp, 44(%rbx)
	jmp	.LBB2_248
.LBB2_245:                              #   in Loop: Header=BB2_13 Depth=1
	xorl	%eax, %eax
.LBB2_248:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i.i.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	96(%rsp), %rcx
	.p2align	4, 0x90
.LBB2_249:                              #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rax)
	incq	%rax
	testb	%dl, %dl
	jne	.LBB2_249
# BB#250:                               #   in Loop: Header=BB2_13 Depth=1
	movl	104(%rsp), %eax
	movl	%eax, 40(%rbx)
	leaq	112(%rsp), %rax
	movq	%rax, %rcx
	movq	16(%rcx), %rax
	movq	%rax, 64(%rbx)
	movups	(%rcx), %xmm0
	movups	%xmm0, 48(%rbx)
.Ltmp103:
	leaq	304(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp104:
	leaq	88(%rsp), %rbp
# BB#251:                               # %_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE3AddERKS2_.exit
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	320(%rsp), %rax
	movslq	316(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 316(%rsp)
	xorl	%ebx, %ebx
.LBB2_252:                              #   in Loop: Header=BB2_13 Depth=1
	leaq	28(%rsp), %r12
	.p2align	4, 0x90
.LBB2_253:                              #   in Loop: Header=BB2_13 Depth=1
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_255
# BB#254:                               #   in Loop: Header=BB2_13 Depth=1
	callq	_ZdaPv
.LBB2_255:                              # %_ZN8NArchive4NZip11CUpdateItemD2Ev.exit
                                        #   in Loop: Header=BB2_13 Depth=1
	testl	%ebx, %ebx
	je	.LBB2_12
	jmp	.LBB2_395
.LBB2_1:
	xorl	%eax, %eax
	movq	%rax, 272(%rsp)         # 8-byte Spill
                                        # implicit-def: %R14D
.LBB2_2:                                # %._crit_edge
	movq	$0, 256(%rsp)
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB2_4
# BB#3:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp106:
	callq	*8(%rax)
.Ltmp107:
.LBB2_4:                                # %_ZN9CMyComPtrI22IArchiveUpdateCallbackEC2EPS0_.exit
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rax
.Ltmp108:
	leaq	256(%rsp), %rdx
	movl	$IID_ICryptoGetTextPassword2, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
.Ltmp109:
# BB#5:                                 # %_ZNK9CMyComPtrI22IArchiveUpdateCallbackE14QueryInterfaceI23ICryptoGetTextPassword2EEiRK4GUIDPPT_.exit
	movq	(%rbx), %rax
.Ltmp113:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp114:
# BB#6:                                 # %_ZN9CMyComPtrI22IArchiveUpdateCallbackED2Ev.exit
.Ltmp116:
	leaq	144(%rsp), %rdi
	callq	_ZN8NArchive4NZip22CCompressionMethodModeC2Ev
.Ltmp117:
# BB#7:
	movq	256(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_337
# BB#8:
	movq	$0, 136(%rsp)
	movq	(%rdi), %rax
.Ltmp119:
	leaq	280(%rsp), %rsi
	leaq	136(%rsp), %rdx
	callq	*40(%rax)
.Ltmp120:
# BB#9:
	testl	%eax, %eax
	movl	%eax, %r12d
	cmovel	%r14d, %r12d
	movl	$1, %r13d
	je	.LBB2_265
# BB#10:
	movl	%eax, %r14d
.LBB2_322:
	movq	136(%rsp), %rdi
.Ltmp136:
	callq	SysFreeString
.Ltmp137:
# BB#323:                               # %_ZN10CMyComBSTRD2Ev.exit
	testl	%r13d, %r13d
	jne	.LBB2_388
	jmp	.LBB2_338
.LBB2_337:
	movb	$0, 228(%rsp)
.LBB2_338:
	movq	32(%rsp), %r14          # 8-byte Reload
	movl	208(%r14), %eax
	movl	212(%r14), %ecx
	testl	%eax, %eax
	movl	$5, %r12d
	cmovnsl	%eax, %r12d
	xorl	%r13d, %r13d
	testl	%r12d, %r12d
	setne	%r13b
	shll	$3, %r13d
	testl	%ecx, %ecx
	cmovnsl	%ecx, %r13d
.Ltmp139:
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp140:
# BB#339:
	movq	160(%rsp), %rax
	movslq	156(%rsp), %rcx
	movb	%r13b, (%rax,%rcx)
	incl	156(%rsp)
	movl	%r13d, %ebx
	andl	$255, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB2_342
# BB#340:
.Ltmp141:
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp142:
# BB#341:
	movq	160(%rsp), %rax
	movslq	156(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	incl	156(%rsp)
	movb	$1, %dl
	cmpl	$8, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB2_343
.LBB2_342:
	cmpl	$9, %ebx
	sete	%dl
.LBB2_343:
	movq	%rax, %r15
	cmpl	$14, %ebx
	movl	224(%r14), %edi
	movl	%edi, 196(%rsp)
	movl	216(%r14), %esi
	movl	%esi, 212(%rsp)
	movl	228(%r14), %ecx
	movl	%ecx, 200(%rsp)
	movl	232(%r14), %eax
	movl	%eax, 208(%rsp)
	movb	244(%r14), %al
	movb	%al, 204(%rsp)
	movl	220(%r14), %eax
	movl	%eax, 192(%rsp)
	movl	236(%r14), %ebp
	movl	%ebp, 216(%rsp)
	movl	240(%r14), %ebp
	movl	%ebp, 220(%rsp)
	movl	252(%r14), %ebp
	movl	%ebp, 224(%rsp)
	je	.LBB2_348
# BB#344:
	testb	%dl, %dl
	jne	.LBB2_348
# BB#345:
	movq	%r15, %r9
	cmpb	$98, %r13b
	jne	.LBB2_376
	jmp	.LBB2_381
.LBB2_348:
	testb	%dl, %dl
	movq	%r15, %r9
	je	.LBB2_353
# BB#349:
	cmpl	$-1, %edi
	jne	.LBB2_351
# BB#350:
	xorl	%edx, %edx
	cmpl	$6, %r12d
	setg	%dl
	cmpl	$8, %r12d
	leal	1(%rdx,%rdx), %edx
	movl	$10, %esi
	cmovlel	%edx, %esi
	movl	%esi, 196(%rsp)
.LBB2_351:
	cmpl	$-1, %ecx
	jne	.LBB2_373
# BB#352:
	cmpl	$6, %r12d
	movl	$64, %ecx
	movl	$32, %edx
	cmovgl	%ecx, %edx
	cmpl	$8, %r12d
	movl	$128, %ecx
	cmovlel	%edx, %ecx
	movl	%ecx, 200(%rsp)
	cmpl	$-1, %eax
	jne	.LBB2_375
	jmp	.LBB2_374
.LBB2_265:
	cmpl	$0, 280(%rsp)
	setne	228(%rsp)
	je	.LBB2_266
# BB#267:
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 245(%rax)
	movq	272(%rsp), %rcx         # 8-byte Reload
	je	.LBB2_269
# BB#268:
	movb	246(%rax), %cl
.LBB2_269:
	andb	$1, %cl
	movb	%cl, 248(%rsp)
	movb	247(%rax), %al
	movb	%al, 249(%rsp)
	movl	$-1, %ebp
	xorl	%ebx, %ebx
	movq	136(%rsp), %r14
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB2_270:                              # =>This Inner Loop Header: Depth=1
	addq	%rax, %rbx
	incl	%ebp
	cmpl	$0, (%rcx)
	leaq	4(%rcx), %rcx
	jne	.LBB2_270
# BB#271:                               # %_Z11MyStringLenIwEiPKT_.exit.i373
	movq	%rbx, %rax
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp122:
	callq	_Znam
.Ltmp123:
# BB#272:                               # %.noexc377
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rcx, %rbx
	movl	$0, (%rax)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_273:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i376
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB2_273
# BB#274:                               # %_ZN11CStringBaseIwEC2EPKw.exit
	testl	%ebp, %ebp
	jle	.LBB2_280
# BB#275:                               # %.lr.ph.preheader.i
	sarq	$32, %rbx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_277:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx,4), %edx
	addl	$-32, %edx
	cmpl	$96, %edx
	jae	.LBB2_278
# BB#276:                               #   in Loop: Header=BB2_277 Depth=1
	incq	%rcx
	cmpq	%rbx, %rcx
	jl	.LBB2_277
.LBB2_280:                              # %.loopexit
	movq	%rax, %rdi
	callq	_ZdaPv
	cmpb	$0, 248(%rsp)
	je	.LBB2_282
# BB#281:
	movl	$-2147024809, %r14d     # imm = 0x80070057
	cmpl	$99, 240(%rsp)
	ja	.LBB2_322
.LBB2_282:
	movq	136(%rsp), %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB2_283:                              # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB2_283
# BB#284:                               # %_Z11MyStringLenIwEiPKT_.exit.i387
	leal	1(%rbp), %r14d
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp125:
	callq	_Znam
.Ltmp126:
# BB#285:                               # %.noexc391
	movq	%rax, 48(%rsp)
	movl	$0, (%rax)
	movl	%r14d, 60(%rsp)
	.p2align	4, 0x90
.LBB2_286:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i390
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_286
# BB#287:
	movl	%ebp, 56(%rsp)
.Ltmp128:
	leaq	64(%rsp), %rdi
	leaq	48(%rsp), %rsi
	movl	$1, %edx
	callq	_Z24UnicodeStringToMultiByteRK11CStringBaseIwEj
.Ltmp129:
# BB#288:
	movl	$0, 240(%rsp)
	movq	232(%rsp), %rax
	movb	$0, (%rax)
	movslq	72(%rsp), %rax
	leaq	1(%rax), %r14
	movl	244(%rsp), %ebx
	cmpl	%ebx, %r14d
	jne	.LBB2_290
# BB#289:                               # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i394
	movq	232(%rsp), %rbp
	jmp	.LBB2_315
.LBB2_353:
	cmpl	$14, %ebx
	jne	.LBB2_373
# BB#354:
	cmpl	$-1, %esi
	jne	.LBB2_360
# BB#355:
	movl	$67108864, %eax         # imm = 0x4000000
	cmpl	$8, %r12d
	jg	.LBB2_359
# BB#356:
	movl	$33554432, %eax         # imm = 0x2000000
	cmpl	$6, %r12d
	jg	.LBB2_359
# BB#357:
	movl	$16777216, %eax         # imm = 0x1000000
	cmpl	$4, %r12d
	jg	.LBB2_359
# BB#358:
	cmpl	$2, %r12d
	movl	$1048576, %edx          # imm = 0x100000
	movl	$65536, %eax            # imm = 0x10000
	cmovgl	%edx, %eax
.LBB2_359:
	movl	%eax, 212(%rsp)
.LBB2_360:
	cmpl	$-1, %ecx
	jne	.LBB2_362
# BB#361:
	cmpl	$6, %r12d
	movl	$64, %eax
	movl	$32, %ecx
	cmovgl	%eax, %ecx
	movl	%ecx, 200(%rsp)
.LBB2_362:
	cmpl	$4, %r12d
	movl	$.L.str.22, %eax
	movl	$.L.str.23, %ebp
	cmovgq	%rax, %rbp
	movl	$0, 184(%rsp)
	movq	176(%rsp), %rbx
	movl	$0, (%rbx)
	xorl	%r14d, %r14d
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB2_363:                              # =>This Inner Loop Header: Depth=1
	incl	%r14d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB2_363
# BB#364:                               # %_Z11MyStringLenIwEiPKT_.exit.i421
	movl	188(%rsp), %eax
	cmpl	%r14d, %eax
	je	.LBB2_370
# BB#365:
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp144:
	callq	_Znam
.Ltmp145:
# BB#366:                               # %.noexc430
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB2_369
# BB#367:                               # %.noexc430
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_369
# BB#368:                               # %._crit_edge.thread.i.i425
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	184(%rsp), %rcx
.LBB2_369:                              # %._crit_edge16.i.i426
	movq	%rax, 176(%rsp)
	movl	$0, (%rax,%rcx,4)
	movl	%r14d, 188(%rsp)
	movq	%rax, %rbx
.LBB2_370:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i429.preheader
	decl	%r14d
	.p2align	4, 0x90
.LBB2_371:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i429
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %eax
	addq	$4, %rbp
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB2_371
# BB#372:                               # %_ZN11CStringBaseIwEaSEPKw.exit431
	movl	%r14d, 184(%rsp)
	movl	192(%rsp), %eax
	movq	%r15, %r9
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB2_373:
	cmpl	$-1, %eax
	jne	.LBB2_375
.LBB2_374:
	xorl	%eax, %eax
	cmpl	$4, %r12d
	setg	%al
	movl	%eax, 192(%rsp)
.LBB2_375:
	cmpb	$98, %r13b
	je	.LBB2_381
.LBB2_376:
	cmpb	$12, %r13b
	jne	.LBB2_387
# BB#377:
	cmpl	$-1, 196(%rsp)
	jne	.LBB2_379
# BB#378:
	xorl	%eax, %eax
	cmpl	$6, %r12d
	setg	%al
	incl	%eax
	cmpl	$8, %r12d
	movl	$7, %ecx
	cmovlel	%eax, %ecx
	movl	%ecx, 196(%rsp)
.LBB2_379:
	cmpl	$-1, 212(%rsp)
	jne	.LBB2_387
# BB#380:
	cmpl	$2, %r12d
	movl	$500000, %eax           # imm = 0x7A120
	movl	$100000, %ecx           # imm = 0x186A0
	cmovgl	%eax, %ecx
	cmpl	$4, %r12d
	movl	$900000, %eax           # imm = 0xDBBA0
	cmovlel	%ecx, %eax
	movl	%eax, 212(%rsp)
	jmp	.LBB2_387
.LBB2_381:
	testl	%r12d, %r12d
	movl	$1, %ecx
	cmovgl	%r12d, %ecx
	cmpl	$10, %ecx
	movl	$9, %edx
	cmovll	%ecx, %edx
	cmpl	$-1, 216(%rsp)
	jne	.LBB2_383
# BB#382:
	movl	$524288, %esi           # imm = 0x80000
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	cmpl	$8, %edx
	movl	$134217728, %ecx        # imm = 0x8000000
	cmovll	%esi, %ecx
	movl	%ecx, 216(%rsp)
.LBB2_383:
	cmpl	$-1, 220(%rsp)
	jne	.LBB2_385
# BB#384:
	leal	3(%rdx), %ecx
	movl	%ecx, 220(%rsp)
.LBB2_385:
	cmpl	$-1, %eax
	jne	.LBB2_387
# BB#386:
	xorl	%eax, %eax
	cmpl	$6, %edx
	setg	%al
	movl	%eax, 192(%rsp)
.LBB2_387:
	movq	64(%r14), %rax
	leaq	64(%r14), %rcx
	addq	$32, %r14
	testq	%rax, %rax
	cmoveq	%rax, %rcx
.Ltmp146:
	leaq	304(%rsp), %rsi
	leaq	144(%rsp), %r8
	movq	%r14, %rdi
	movq	432(%rsp), %rdx         # 8-byte Reload
	callq	_ZN8NArchive4NZip6UpdateERK13CObjectVectorINS0_7CItemExEERKS1_INS0_11CUpdateItemEEP20ISequentialOutStreamPNS0_10CInArchiveEPNS0_22CCompressionMethodModeEP22IArchiveUpdateCallback
	movl	%eax, %r14d
.Ltmp147:
.LBB2_388:
	movq	232(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_390
# BB#389:
	callq	_ZdaPv
.LBB2_390:                              # %_ZN11CStringBaseIcED2Ev.exit.i381
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_392
# BB#391:
	callq	_ZdaPv
.LBB2_392:                              # %_ZN11CStringBaseIwED2Ev.exit.i382
.Ltmp151:
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp152:
# BB#393:                               # %_ZN8NArchive4NZip22CCompressionMethodModeD2Ev.exit384
	movq	256(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_395
# BB#394:
	movq	(%rdi), %rax
.Ltmp156:
	callq	*16(%rax)
.Ltmp157:
.LBB2_395:                              # %_ZN9CMyComPtrI23ICryptoGetTextPassword2ED2Ev.exit380
	leaq	304(%rsp), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE+16, 304(%rsp)
.Ltmp168:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp169:
# BB#396:
.Ltmp174:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp175:
	jmp	.LBB2_413
.LBB2_266:
	xorl	%r13d, %r13d
	jmp	.LBB2_322
.LBB2_278:                              # %_ZN11CStringBaseIwED2Ev.exit378
	movq	%rax, %rdi
	callq	_ZdaPv
	movl	$-2147024809, %r14d     # imm = 0x80070057
	jmp	.LBB2_322
.LBB2_290:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r14, %rdi
	cmovlq	%rax, %rdi
.Ltmp131:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp132:
# BB#291:                               # %.noexc407
	testl	%ebx, %ebx
	jle	.LBB2_314
# BB#292:                               # %.preheader.i.i395
	movslq	240(%rsp), %rax
	testq	%rax, %rax
	movq	232(%rsp), %rdi
	jle	.LBB2_312
# BB#293:                               # %.lr.ph.preheader.i.i396
	cmpl	$31, %eax
	jbe	.LBB2_294
# BB#301:                               # %min.iters.checked830
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB2_294
# BB#302:                               # %vector.memcheck841
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB2_304
# BB#303:                               # %vector.memcheck841
	leaq	(%rbp,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_304
.LBB2_294:
	xorl	%ecx, %ecx
.LBB2_295:                              # %.lr.ph.i.i401.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB2_298
# BB#296:                               # %.lr.ph.i.i401.prol.preheader
	negq	%rsi
.LBB2_297:                              # %.lr.ph.i.i401.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %ebx
	movb	%bl, (%rbp,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_297
.LBB2_298:                              # %.lr.ph.i.i401.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB2_313
# BB#299:                               # %.lr.ph.i.i401.preheader.new
	subq	%rcx, %rax
	leaq	7(%rbp,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
.LBB2_300:                              # %.lr.ph.i.i401
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB2_300
	jmp	.LBB2_313
.LBB2_312:                              # %._crit_edge.i.i397
	testq	%rdi, %rdi
	je	.LBB2_314
.LBB2_313:                              # %._crit_edge.thread.i.i402
	callq	_ZdaPv
.LBB2_314:                              # %._crit_edge17.i.i403
	movq	%rbp, 232(%rsp)
	movslq	240(%rsp), %rax
	movb	$0, (%rbp,%rax)
	movl	%r14d, 244(%rsp)
.LBB2_315:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i404
	movq	64(%rsp), %rax
	.p2align	4, 0x90
.LBB2_316:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbp)
	incq	%rbp
	testb	%cl, %cl
	jne	.LBB2_316
# BB#317:
	movl	72(%rsp), %eax
	movl	%eax, 240(%rsp)
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_319
# BB#318:
	callq	_ZdaPv
.LBB2_319:                              # %_ZN11CStringBaseIcED2Ev.exit409
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_321
# BB#320:
	callq	_ZdaPv
.LBB2_321:                              # %_ZN11CStringBaseIwED2Ev.exit410
	xorl	%r13d, %r13d
	movl	%r12d, %r14d
	jmp	.LBB2_322
.LBB2_304:                              # %vector.body826.preheader
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_305
# BB#306:                               # %vector.body826.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
.LBB2_307:                              # %vector.body826.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx), %xmm0
	movups	16(%rdi,%rbx), %xmm1
	movups	%xmm0, (%rbp,%rbx)
	movups	%xmm1, 16(%rbp,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB2_307
	jmp	.LBB2_308
.LBB2_305:
	xorl	%ebx, %ebx
.LBB2_308:                              # %vector.body826.prol.loopexit
	cmpq	$96, %rdx
	jb	.LBB2_311
# BB#309:                               # %vector.body826.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%rbp,%rbx), %rsi
	leaq	112(%rdi,%rbx), %rbx
.LBB2_310:                              # %vector.body826
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-128, %rdx
	jne	.LBB2_310
.LBB2_311:                              # %middle.block827
	cmpq	%rcx, %rax
	jne	.LBB2_295
	jmp	.LBB2_313
.LBB2_326:
.Ltmp133:
	movq	%rdx, %r14
	movq	%rax, %rbp
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_328
# BB#327:
	callq	_ZdaPv
	jmp	.LBB2_328
.LBB2_325:
.Ltmp130:
	movq	%rdx, %r14
	movq	%rax, %rbp
.LBB2_328:                              # %_ZN11CStringBaseIcED2Ev.exit411
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_330
# BB#329:
	callq	_ZdaPv
	jmp	.LBB2_330
.LBB2_324:
.Ltmp127:
	jmp	.LBB2_264
.LBB2_279:
.Ltmp124:
	jmp	.LBB2_264
.LBB2_419:
.Ltmp158:
	jmp	.LBB2_404
.LBB2_336:
.Ltmp138:
	jmp	.LBB2_347
.LBB2_263:
.Ltmp121:
.LBB2_264:
	movq	%rdx, %r14
	movq	%rax, %rbp
.LBB2_330:
	movq	136(%rsp), %rdi
.Ltmp134:
	callq	SysFreeString
.Ltmp135:
	jmp	.LBB2_331
.LBB2_418:
.Ltmp148:
	jmp	.LBB2_347
.LBB2_346:
.Ltmp143:
.LBB2_347:
	movq	%rdx, %r14
	movq	%rax, %rbp
.LBB2_331:
	movq	232(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_333
# BB#332:
	callq	_ZdaPv
.LBB2_333:                              # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_335
# BB#334:
	callq	_ZdaPv
.LBB2_335:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp149:
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp150:
	jmp	.LBB2_399
.LBB2_397:
.Ltmp153:
	jmp	.LBB2_398
.LBB2_262:
.Ltmp115:
	jmp	.LBB2_398
.LBB2_261:
.Ltmp110:
	movq	%rdx, %r14
	movq	%rax, %rbp
	movq	(%rbx), %rax
.Ltmp111:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp112:
	jmp	.LBB2_399
.LBB2_409:
.Ltmp176:
	movq	%rdx, %r14
	movq	%rax, %rbp
	jmp	.LBB2_410
.LBB2_401:
.Ltmp170:
	movq	%rdx, %r14
	movq	%rax, %rbp
.Ltmp171:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp172:
	jmp	.LBB2_410
.LBB2_402:
.Ltmp173:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_260:
.Ltmp118:
.LBB2_398:                              # %_ZN9CMyComPtrI22IArchiveUpdateCallbackED2Ev.exit369
	movq	%rdx, %r14
	movq	%rax, %rbp
.LBB2_399:                              # %_ZN9CMyComPtrI22IArchiveUpdateCallbackED2Ev.exit369
	movq	256(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_405
# BB#400:
	movq	(%rdi), %rax
.Ltmp154:
	callq	*16(%rax)
.Ltmp155:
	jmp	.LBB2_405
.LBB2_144:
.Ltmp80:
	jmp	.LBB2_126
.LBB2_216:
.Ltmp86:
	movq	%rdx, %r14
	movq	%rax, %rbp
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_127
# BB#217:
	callq	_ZdaPv
	jmp	.LBB2_127
.LBB2_142:
.Ltmp77:
	movq	%rdx, %r14
	movq	%rax, %rbp
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_127
# BB#143:
	callq	_ZdaPv
	jmp	.LBB2_127
.LBB2_416:
.Ltmp89:
	jmp	.LBB2_126
.LBB2_215:
.Ltmp83:
	jmp	.LBB2_126
.LBB2_141:
.Ltmp74:
	jmp	.LBB2_126
.LBB2_125:
.Ltmp71:
	jmp	.LBB2_126
.LBB2_123:
.Ltmp60:
	movq	%rdx, %r14
	movq	%rax, %rbp
.Ltmp61:
	leaq	48(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp62:
	jmp	.LBB2_127
.LBB2_124:
.Ltmp63:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_121:
.Ltmp52:
	movq	%rdx, %r14
	movq	%rax, %rbp
.Ltmp53:
	leaq	280(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp54:
	jmp	.LBB2_127
.LBB2_122:
.Ltmp55:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_118:
.Ltmp44:
	movq	%rdx, %r14
	movq	%rax, %rbp
.Ltmp45:
	leaq	368(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp46:
	jmp	.LBB2_127
.LBB2_119:
.Ltmp47:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_40:
.Ltmp66:
	jmp	.LBB2_126
.LBB2_120:
.Ltmp41:
	jmp	.LBB2_126
.LBB2_79:
.Ltmp36:
	movq	%rdx, %r14
	movq	%rax, %rbp
.Ltmp37:
	leaq	384(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp38:
	jmp	.LBB2_127
.LBB2_78:
.Ltmp33:
	jmp	.LBB2_126
.LBB2_68:
.Ltmp28:
	movq	%rdx, %r14
	movq	%rax, %rbp
.Ltmp29:
	leaq	400(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp30:
	jmp	.LBB2_127
.LBB2_67:
.Ltmp25:
	jmp	.LBB2_126
.LBB2_417:
.Ltmp102:
	movq	%rdx, %r14
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB2_258
.LBB2_43:
.Ltmp20:
	movq	%rdx, %r14
	movq	%rax, %rbp
.Ltmp21:
	leaq	352(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp22:
	jmp	.LBB2_127
.LBB2_241:
.Ltmp97:
	jmp	.LBB2_257
.LBB2_236:
.Ltmp92:
	movq	%rdx, %r14
	movq	%rax, %rbp
.Ltmp93:
	leaq	336(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp94:
	jmp	.LBB2_258
.LBB2_42:
.Ltmp15:
.LBB2_126:
	movq	%rdx, %r14
	movq	%rax, %rbp
	jmp	.LBB2_127
.LBB2_41:
.Ltmp10:
	movq	%rdx, %r14
	movq	%rax, %rbp
.Ltmp11:
	leaq	416(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp12:
.LBB2_127:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_258
# BB#128:
	callq	_ZdaPv
	jmp	.LBB2_258
.LBB2_27:
.Ltmp105:
	jmp	.LBB2_257
.LBB2_256:
.Ltmp5:
.LBB2_257:
	movq	%rdx, %r14
	movq	%rax, %rbp
.LBB2_258:
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_405
# BB#259:
	callq	_ZdaPv
	jmp	.LBB2_405
.LBB2_403:
.Ltmp2:
.LBB2_404:
	movq	%rdx, %r14
	movq	%rax, %rbp
.LBB2_405:
	leaq	304(%rsp), %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE+16, 304(%rsp)
.Ltmp159:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp160:
# BB#406:
.Ltmp165:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp166:
.LBB2_410:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	cmpl	$2, %r14d
	jne	.LBB2_412
# BB#411:
	movl	(%rax), %r14d
	callq	__cxa_end_catch
	jmp	.LBB2_413
.LBB2_412:
	callq	__cxa_end_catch
	movl	$-2147024882, %r14d     # imm = 0x8007000E
.LBB2_413:
	movl	%r14d, %eax
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_407:
.Ltmp161:
	movq	%rax, %rbp
.Ltmp162:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp163:
	jmp	.LBB2_415
.LBB2_408:
.Ltmp164:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB2_414:
.Ltmp167:
	movq	%rax, %rbp
.LBB2_415:                              # %.body
	movq	%rbp, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end2-_ZN8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\364\005"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\345\005"              # Call site table length
	.long	.Ltmp67-.Lfunc_begin0   # >> Call Site 1 <<
	.long	.Ltmp70-.Ltmp67         #   Call between .Ltmp67 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin0   #     jumps to .Ltmp71
	.byte	3                       #   On action: 2
	.long	.Ltmp72-.Lfunc_begin0   # >> Call Site 2 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin0   #     jumps to .Ltmp74
	.byte	3                       #   On action: 2
	.long	.Ltmp75-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin0   #     jumps to .Ltmp77
	.byte	3                       #   On action: 2
	.long	.Ltmp78-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin0   #     jumps to .Ltmp80
	.byte	3                       #   On action: 2
	.long	.Ltmp81-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin0   #     jumps to .Ltmp83
	.byte	3                       #   On action: 2
	.long	.Ltmp84-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin0   #     jumps to .Ltmp86
	.byte	3                       #   On action: 2
	.long	.Ltmp87-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin0   #     jumps to .Ltmp89
	.byte	3                       #   On action: 2
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 8 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	3                       #   On action: 2
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 9 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	3                       #   On action: 2
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 10 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp105-.Lfunc_begin0  #     jumps to .Ltmp105
	.byte	3                       #   On action: 2
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 11 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	3                       #   On action: 2
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	3                       #   On action: 2
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	3                       #   On action: 2
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	3                       #   On action: 2
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	3                       #   On action: 2
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	3                       #   On action: 2
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	3                       #   On action: 2
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	3                       #   On action: 2
	.long	.Ltmp39-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin0   #     jumps to .Ltmp41
	.byte	3                       #   On action: 2
	.long	.Ltmp42-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin0   #     jumps to .Ltmp44
	.byte	3                       #   On action: 2
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp66-.Lfunc_begin0   #     jumps to .Ltmp66
	.byte	3                       #   On action: 2
	.long	.Ltmp50-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	3                       #   On action: 2
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp66-.Lfunc_begin0   #     jumps to .Ltmp66
	.byte	3                       #   On action: 2
	.long	.Ltmp58-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin0   #     jumps to .Ltmp60
	.byte	3                       #   On action: 2
	.long	.Ltmp64-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin0   #     jumps to .Ltmp66
	.byte	3                       #   On action: 2
	.long	.Ltmp90-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin0   #     jumps to .Ltmp92
	.byte	3                       #   On action: 2
	.long	.Ltmp95-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin0   #     jumps to .Ltmp97
	.byte	3                       #   On action: 2
	.long	.Ltmp98-.Lfunc_begin0   # >> Call Site 28 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp105-.Lfunc_begin0  #     jumps to .Ltmp105
	.byte	3                       #   On action: 2
	.long	.Ltmp100-.Lfunc_begin0  # >> Call Site 29 <<
	.long	.Ltmp101-.Ltmp100       #   Call between .Ltmp100 and .Ltmp101
	.long	.Ltmp102-.Lfunc_begin0  #     jumps to .Ltmp102
	.byte	3                       #   On action: 2
	.long	.Ltmp103-.Lfunc_begin0  # >> Call Site 30 <<
	.long	.Ltmp104-.Ltmp103       #   Call between .Ltmp103 and .Ltmp104
	.long	.Ltmp105-.Lfunc_begin0  #     jumps to .Ltmp105
	.byte	3                       #   On action: 2
	.long	.Ltmp106-.Lfunc_begin0  # >> Call Site 31 <<
	.long	.Ltmp107-.Ltmp106       #   Call between .Ltmp106 and .Ltmp107
	.long	.Ltmp118-.Lfunc_begin0  #     jumps to .Ltmp118
	.byte	3                       #   On action: 2
	.long	.Ltmp108-.Lfunc_begin0  # >> Call Site 32 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin0  #     jumps to .Ltmp110
	.byte	3                       #   On action: 2
	.long	.Ltmp113-.Lfunc_begin0  # >> Call Site 33 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin0  #     jumps to .Ltmp115
	.byte	3                       #   On action: 2
	.long	.Ltmp116-.Lfunc_begin0  # >> Call Site 34 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin0  #     jumps to .Ltmp118
	.byte	3                       #   On action: 2
	.long	.Ltmp119-.Lfunc_begin0  # >> Call Site 35 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin0  #     jumps to .Ltmp121
	.byte	3                       #   On action: 2
	.long	.Ltmp136-.Lfunc_begin0  # >> Call Site 36 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin0  #     jumps to .Ltmp138
	.byte	3                       #   On action: 2
	.long	.Ltmp139-.Lfunc_begin0  # >> Call Site 37 <<
	.long	.Ltmp142-.Ltmp139       #   Call between .Ltmp139 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin0  #     jumps to .Ltmp143
	.byte	3                       #   On action: 2
	.long	.Ltmp122-.Lfunc_begin0  # >> Call Site 38 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin0  #     jumps to .Ltmp124
	.byte	3                       #   On action: 2
	.long	.Ltmp125-.Lfunc_begin0  # >> Call Site 39 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin0  #     jumps to .Ltmp127
	.byte	3                       #   On action: 2
	.long	.Ltmp128-.Lfunc_begin0  # >> Call Site 40 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin0  #     jumps to .Ltmp130
	.byte	3                       #   On action: 2
	.long	.Ltmp144-.Lfunc_begin0  # >> Call Site 41 <<
	.long	.Ltmp147-.Ltmp144       #   Call between .Ltmp144 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin0  #     jumps to .Ltmp148
	.byte	3                       #   On action: 2
	.long	.Ltmp151-.Lfunc_begin0  # >> Call Site 42 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin0  #     jumps to .Ltmp153
	.byte	3                       #   On action: 2
	.long	.Ltmp156-.Lfunc_begin0  # >> Call Site 43 <<
	.long	.Ltmp157-.Ltmp156       #   Call between .Ltmp156 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin0  #     jumps to .Ltmp158
	.byte	3                       #   On action: 2
	.long	.Ltmp168-.Lfunc_begin0  # >> Call Site 44 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin0  #     jumps to .Ltmp170
	.byte	3                       #   On action: 2
	.long	.Ltmp174-.Lfunc_begin0  # >> Call Site 45 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin0  #     jumps to .Ltmp176
	.byte	3                       #   On action: 2
	.long	.Ltmp131-.Lfunc_begin0  # >> Call Site 46 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin0  #     jumps to .Ltmp133
	.byte	3                       #   On action: 2
	.long	.Ltmp134-.Lfunc_begin0  # >> Call Site 47 <<
	.long	.Ltmp112-.Ltmp134       #   Call between .Ltmp134 and .Ltmp112
	.long	.Ltmp167-.Lfunc_begin0  #     jumps to .Ltmp167
	.byte	1                       #   On action: 1
	.long	.Ltmp171-.Lfunc_begin0  # >> Call Site 48 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin0  #     jumps to .Ltmp173
	.byte	1                       #   On action: 1
	.long	.Ltmp154-.Lfunc_begin0  # >> Call Site 49 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp167-.Lfunc_begin0  #     jumps to .Ltmp167
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin0   # >> Call Site 50 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin0   #     jumps to .Ltmp63
	.byte	1                       #   On action: 1
	.long	.Ltmp53-.Lfunc_begin0   # >> Call Site 51 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin0   #     jumps to .Ltmp55
	.byte	1                       #   On action: 1
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 52 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin0   #     jumps to .Ltmp47
	.byte	1                       #   On action: 1
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 53 <<
	.long	.Ltmp12-.Ltmp37         #   Call between .Ltmp37 and .Ltmp12
	.long	.Ltmp167-.Lfunc_begin0  #     jumps to .Ltmp167
	.byte	1                       #   On action: 1
	.long	.Ltmp159-.Lfunc_begin0  # >> Call Site 54 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp161-.Lfunc_begin0  #     jumps to .Ltmp161
	.byte	1                       #   On action: 1
	.long	.Ltmp165-.Lfunc_begin0  # >> Call Site 55 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin0  #     jumps to .Ltmp167
	.byte	1                       #   On action: 1
	.long	.Ltmp166-.Lfunc_begin0  # >> Call Site 56 <<
	.long	.Ltmp162-.Ltmp166       #   Call between .Ltmp166 and .Ltmp162
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin0  # >> Call Site 57 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin0  #     jumps to .Ltmp164
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI16CSystemException  # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.zero	16
	.section	.text._ZN8NArchive4NZip22CCompressionMethodModeC2Ev,"axG",@progbits,_ZN8NArchive4NZip22CCompressionMethodModeC2Ev,comdat
	.weak	_ZN8NArchive4NZip22CCompressionMethodModeC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip22CCompressionMethodModeC2Ev,@function
_ZN8NArchive4NZip22CCompressionMethodModeC2Ev: # @_ZN8NArchive4NZip22CCompressionMethodModeC2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	$1, 24(%rbx)
	movq	$_ZTV13CRecordVectorIhE+16, (%rbx)
	movups	%xmm0, 32(%rbx)
.Ltmp177:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp178:
# BB#1:
	movq	%r14, 32(%rbx)
	movl	$0, (%r14)
	movl	$4, 44(%rbx)
	movb	$0, 60(%rbx)
	movb	$0, 84(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 88(%rbx)
.Ltmp180:
	movl	$4, %edi
	callq	_Znam
.Ltmp181:
# BB#2:
	movq	%rax, 88(%rbx)
	movb	$0, (%rax)
	movl	$4, 100(%rbx)
	movw	$768, 104(%rbx)         # imm = 0x300
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_4:
.Ltmp182:
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB4_5
.LBB4_3:
.Ltmp179:
	movq	%rax, %r15
.LBB4_5:                                # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp183:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp184:
# BB#6:
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB4_7:
.Ltmp185:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive4NZip22CCompressionMethodModeC2Ev, .Lfunc_end4-_ZN8NArchive4NZip22CCompressionMethodModeC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp177-.Lfunc_begin1  # >> Call Site 1 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin1  #     jumps to .Ltmp179
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin1  # >> Call Site 2 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin1  #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin1  # >> Call Site 3 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin1  #     jumps to .Ltmp185
	.byte	1                       #   On action: 1
	.long	.Ltmp184-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp184    #   Call between .Ltmp184 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev: # @_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -24
.Lcfi23:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE+16, (%rbx)
.Ltmp186:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp187:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB5_2:
.Ltmp188:
	movq	%rax, %r14
.Ltmp189:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp190:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp191:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev, .Lfunc_end5-_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp186-.Lfunc_begin2  # >> Call Site 1 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin2  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp189-.Ltmp187       #   Call between .Ltmp187 and .Ltmp189
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin2  #     jumps to .Ltmp191
	.byte	1                       #   On action: 1
	.long	.Ltmp190-.Lfunc_begin2  # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp190    #   Call between .Ltmp190 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback,@function
_ZThn8_N8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback: # @_ZThn8_N8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback # TAILCALL
.Lfunc_end6:
	.size	_ZThn8_N8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback, .Lfunc_end6-_ZThn8_N8NArchive4NZip8CHandler11UpdateItemsEP20ISequentialOutStreamjP22IArchiveUpdateCallback
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.zero	16
	.text
	.globl	_ZN8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZN8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZN8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZN8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 192
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, %ebx
	movl	%ebx, 252(%rbp)
	pcmpeqd	%xmm0, %xmm0
	movdqu	%xmm0, 224(%rbp)
	movdqu	%xmm0, 208(%rbp)
	movl	$-1, 240(%rbp)
	movl	$50331648, 244(%rbp)    # imm = 0x3000000
	movw	$0, 248(%rbp)
	movb	$0, 250(%rbp)
	callq	_ZN8NWindows7NSystem21GetNumberOfProcessorsEv
	movl	%eax, 252(%rbp)
	testl	%r14d, %r14d
	jle	.LBB7_1
# BB#3:                                 # %.lr.ph
	movl	%ebx, 92(%rsp)          # 4-byte Spill
	leaq	252(%rbp), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	208(%rbp), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	248(%rbp), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	leaq	249(%rbp), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	leaq	250(%rbp), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movslq	%r14d, %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
                                        # implicit-def: %R15D
	jmp	.LBB7_4
	.p2align	4, 0x90
.LBB7_260:                              #   in Loop: Header=BB7_4 Depth=1
	incq	%r13
	cmpq	120(%rsp), %r13         # 8-byte Folded Reload
	movl	%r12d, %r15d
	jl	.LBB7_4
	jmp	.LBB7_261
.LBB7_224:                              #   in Loop: Header=BB7_4 Depth=1
	movl	$1, 40(%rsp)
	movl	24(%rsp), %ecx
	decl	%ecx
.Ltmp251:
	movl	$1, %edx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp252:
# BB#225:                               # %_ZNK11CStringBaseIwE3MidEi.exit354
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp254:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	40(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r12d
.Ltmp255:
# BB#226:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movl	$1, %ebx
	je	.LBB7_228
# BB#227:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_228:                              # %_ZN11CStringBaseIwED2Ev.exit355
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	jne	.LBB7_257
# BB#229:                               # %.thread386
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	40(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 220(%rcx)
	jmp	.LBB7_255
.LBB7_238:                              #   in Loop: Header=BB7_4 Depth=1
.Ltmp249:
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r12d
.Ltmp250:
# BB#239:                               #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	jmp	.LBB7_240
.LBB7_243:                              #   in Loop: Header=BB7_4 Depth=1
.Ltmp247:
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r12d
.Ltmp248:
# BB#244:                               #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	movl	$1, %ebx
	jne	.LBB7_257
# BB#245:                               #   in Loop: Header=BB7_4 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax)
	je	.LBB7_255
# BB#246:                               #   in Loop: Header=BB7_4 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	jmp	.LBB7_254
.LBB7_250:                              #   in Loop: Header=BB7_4 Depth=1
.Ltmp245:
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	movl	%eax, %r12d
.Ltmp246:
# BB#251:                               #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	movl	$1, %ebx
	jne	.LBB7_257
# BB#252:                               #   in Loop: Header=BB7_4 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpb	$0, (%rax)
	je	.LBB7_255
# BB#253:                               #   in Loop: Header=BB7_4 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
.LBB7_254:                              #   in Loop: Header=BB7_4 Depth=1
	movb	$0, (%rax)
	jmp	.LBB7_255
	.p2align	4, 0x90
.LBB7_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
                                        #     Child Loop BB7_7 Depth 2
                                        #     Child Loop BB7_63 Depth 2
                                        #     Child Loop BB7_66 Depth 2
                                        #     Child Loop BB7_82 Depth 2
                                        #     Child Loop BB7_26 Depth 2
                                        #     Child Loop BB7_29 Depth 2
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r13,8), %rbp
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 16(%rsp)
	movl	$-1, %ebx
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB7_5:                                #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebx
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB7_5
# BB#6:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB7_4 Depth=1
	leal	1(%rbx), %eax
	movslq	%eax, %r14
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, 16(%rsp)
	movl	$0, (%rax)
	movl	%r14d, 28(%rsp)
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB7_7:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %edx
	addq	$4, %rbp
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB7_7
# BB#8:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	%ebx, 24(%rsp)
.Ltmp192:
	movq	%rax, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp193:
# BB#9:                                 # %_ZN11CStringBaseIwE9MakeUpperEv.exit
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	24(%rsp), %ecx
	movl	$-2147024809, %r12d     # imm = 0x80070057
	movl	$1, %ebx
	testl	%ecx, %ecx
	je	.LBB7_257
# BB#10:                                #   in Loop: Header=BB7_4 Depth=1
	movq	%r13, %r14
	shlq	$4, %r14
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14), %rbp
	movq	16(%rsp), %rdi
	cmpl	$88, (%rdi)
	jne	.LBB7_21
# BB#11:                                #   in Loop: Header=BB7_4 Depth=1
	movl	$9, 40(%rsp)
	decl	%ecx
.Ltmp342:
	movl	$1, %edx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp343:
# BB#12:                                # %_ZNK11CStringBaseIwE3MidEi.exit
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp345:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	40(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %ebx
.Ltmp346:
# BB#13:                                #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_15:                               # %_ZN11CStringBaseIwED2Ev.exit223
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	cmovnel	%ebx, %r15d
	movl	$1, %ebx
	jne	.LBB7_256
# BB#16:                                #   in Loop: Header=BB7_4 Depth=1
	movl	40(%rsp), %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
	movl	$4, %ebx
	jmp	.LBB7_256
	.p2align	4, 0x90
.LBB7_21:                               #   in Loop: Header=BB7_4 Depth=1
.Ltmp195:
	movl	$.L.str, %esi
	callq	_Z15MyStringComparePKwS0_
.Ltmp196:
# BB#22:                                #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_23
# BB#55:                                #   in Loop: Header=BB7_4 Depth=1
.Ltmp197:
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp198:
# BB#56:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp200:
	movl	$.L.str.7, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp201:
# BB#57:                                #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_59
# BB#58:                                #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_59:                               # %_ZN11CStringBaseIwED2Ev.exit251
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_60
# BB#115:                               #   in Loop: Header=BB7_4 Depth=1
	movq	16(%rsp), %rax
	cmpl	$68, (%rax)
	jne	.LBB7_125
# BB#116:                               #   in Loop: Header=BB7_4 Depth=1
	movl	$900000, 40(%rsp)       # imm = 0xDBBA0
	movl	24(%rsp), %ecx
	decl	%ecx
.Ltmp293:
	movl	$1, %edx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp294:
# BB#117:                               # %_ZNK11CStringBaseIwE3MidEi.exit289
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp296:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	40(%rsp), %rdx
	callq	_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r12d
.Ltmp297:
# BB#118:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movl	$1, %ebx
	je	.LBB7_120
# BB#119:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_120:                              # %_ZN11CStringBaseIwED2Ev.exit290
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	jne	.LBB7_257
# BB#121:                               # %.thread
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	40(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 216(%rcx)
	jmp	.LBB7_255
.LBB7_23:                               #   in Loop: Header=BB7_4 Depth=1
	movzwl	(%rbp), %eax
	cmpl	$19, %eax
	je	.LBB7_52
# BB#24:                                #   in Loop: Header=BB7_4 Depth=1
	movzwl	%ax, %eax
	cmpl	$8, %eax
	movl	$1, %ebx
	jne	.LBB7_257
# BB#25:                                #   in Loop: Header=BB7_4 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	8(%rax,%r14), %rbp
	movq	%rbp, %rcx
	xorl	%eax, %eax
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	.p2align	4, 0x90
.LBB7_26:                               #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rdx, %rax
	cmpl	$0, (%rcx)
	leaq	4(%rcx), %rcx
	jne	.LBB7_26
# BB#27:                                # %_Z11MyStringLenIwEiPKT_.exit.i227
                                        #   in Loop: Header=BB7_4 Depth=1
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp324:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp325:
# BB#28:                                # %.noexc
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	$0, (%rbx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_29:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i230
                                        #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB7_29
# BB#30:                                # %_ZN11CStringBaseIwEC2EPKw.exit231
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp327:
	movq	%rbx, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp328:
# BB#31:                                # %_ZN11CStringBaseIwE9MakeUpperEv.exit233
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp329:
	movl	$.L.str.1, %esi
	movq	%rbx, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp330:
# BB#32:                                #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_33
# BB#35:                                #   in Loop: Header=BB7_4 Depth=1
.Ltmp331:
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp332:
# BB#36:                                #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_37
# BB#38:                                #   in Loop: Header=BB7_4 Depth=1
.Ltmp333:
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp334:
# BB#39:                                #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_40
# BB#41:                                #   in Loop: Header=BB7_4 Depth=1
.Ltmp335:
	movl	$.L.str.4, %esi
	movq	%rbx, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp336:
# BB#42:                                #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_43
# BB#44:                                #   in Loop: Header=BB7_4 Depth=1
.Ltmp337:
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp338:
# BB#45:                                #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_46
# BB#47:                                #   in Loop: Header=BB7_4 Depth=1
.Ltmp339:
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp340:
# BB#48:                                #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_49
# BB#51:                                # %_ZN11CStringBaseIwED2Ev.exit247
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movl	$1, %ebx
	jmp	.LBB7_257
.LBB7_60:                               #   in Loop: Header=BB7_4 Depth=1
	movzwl	(%rbp), %eax
	cmpl	$8, %eax
	jne	.LBB7_61
# BB#62:                                #   in Loop: Header=BB7_4 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	8(%rax,%r14), %rbx
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, (%rsp)
	movl	$-1, %ebp
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB7_63:                               #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB7_63
# BB#64:                                # %_Z11MyStringLenIwEiPKT_.exit.i254
                                        #   in Loop: Header=BB7_4 Depth=1
	leal	1(%rbp), %r14d
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp299:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp300:
# BB#65:                                # %.noexc258
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	%rax, (%rsp)
	movl	$0, (%rax)
	movl	%r14d, 12(%rsp)
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB7_66:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i257
                                        #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB7_66
# BB#67:                                #   in Loop: Header=BB7_4 Depth=1
	movl	%ebp, 8(%rsp)
.Ltmp301:
	movq	%rax, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp302:
	movq	%rsp, %rbp
# BB#68:                                # %_ZN11CStringBaseIwE9MakeUpperEv.exit261
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp303:
	xorl	%edx, %edx
	movl	$3, %ecx
	leaq	40(%rsp), %rdi
	movq	%rbp, %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp304:
# BB#69:                                # %_ZNK11CStringBaseIwE4LeftEi.exit263
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	40(%rsp), %rdi
.Ltmp306:
	movl	$.L.str.8, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp307:
# BB#70:                                #   in Loop: Header=BB7_4 Depth=1
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_72
# BB#71:                                #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_72:                               # %_ZN11CStringBaseIwED2Ev.exit266
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_73
# BB#104:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp309:
	movl	$.L.str.12, %esi
	callq	_Z15MyStringComparePKwS0_
.Ltmp310:
# BB#105:                               #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_106
.LBB7_103:                              #   in Loop: Header=BB7_4 Depth=1
	movl	$-2147024809, %r15d     # imm = 0x80070057
	movl	$1, %ebx
	jmp	.LBB7_108
.LBB7_125:                              #   in Loop: Header=BB7_4 Depth=1
.Ltmp203:
	xorl	%edx, %edx
	movl	$3, %ecx
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp204:
# BB#126:                               # %_ZNK11CStringBaseIwE4LeftEi.exit293
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp206:
	movl	$.L.str.13, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp207:
# BB#127:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_129
# BB#128:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_129:                              # %_ZN11CStringBaseIwED2Ev.exit296
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_130
# BB#142:                               #   in Loop: Header=BB7_4 Depth=1
	movq	16(%rsp), %rax
	cmpl	$79, (%rax)
	jne	.LBB7_152
# BB#143:                               #   in Loop: Header=BB7_4 Depth=1
	movl	$8, 40(%rsp)
	movl	24(%rsp), %ecx
	decl	%ecx
.Ltmp281:
	movl	$1, %edx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp282:
# BB#144:                               # %_ZNK11CStringBaseIwE3MidEi.exit305
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp284:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	40(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r12d
.Ltmp285:
# BB#145:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movl	$1, %ebx
	je	.LBB7_147
# BB#146:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_147:                              # %_ZN11CStringBaseIwED2Ev.exit306
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	jne	.LBB7_257
# BB#148:                               # %.thread378
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	40(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 240(%rcx)
	jmp	.LBB7_255
.LBB7_52:                               #   in Loop: Header=BB7_4 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	8(%rax,%r14), %eax
	cmpl	$14, %eax
	movl	$1, %ebx
	ja	.LBB7_257
# BB#53:                                #   in Loop: Header=BB7_4 Depth=1
	movl	$21249, %ecx            # imm = 0x5301
	btl	%eax, %ecx
	jae	.LBB7_257
# BB#54:                                #   in Loop: Header=BB7_4 Depth=1
	movzbl	%al, %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 212(%rcx)
	jmp	.LBB7_255
.LBB7_61:                               #   in Loop: Header=BB7_4 Depth=1
	movl	$1, %ebx
	jmp	.LBB7_257
.LBB7_130:                              #   in Loop: Header=BB7_4 Depth=1
	movl	$16777216, 40(%rsp)     # imm = 0x1000000
	movl	24(%rsp), %ecx
	addl	$-3, %ecx
.Ltmp287:
	movl	$3, %edx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp288:
# BB#131:                               # %_ZNK11CStringBaseIwE3MidEi.exit298
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp290:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	40(%rsp), %rdx
	callq	_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r12d
.Ltmp291:
# BB#132:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movl	$1, %ebx
	je	.LBB7_134
# BB#133:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_134:                              # %_ZN11CStringBaseIwED2Ev.exit299
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	jne	.LBB7_257
# BB#135:                               # %.thread376
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	40(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 236(%rcx)
	jmp	.LBB7_255
.LBB7_152:                              #   in Loop: Header=BB7_4 Depth=1
.Ltmp209:
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp210:
# BB#153:                               # %_ZNK11CStringBaseIwE4LeftEi.exit309
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp212:
	movl	$.L.str.14, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp213:
# BB#154:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_156
# BB#155:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_156:                              # %_ZN11CStringBaseIwED2Ev.exit312
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_157
# BB#169:                               #   in Loop: Header=BB7_4 Depth=1
.Ltmp215:
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp216:
# BB#170:                               # %_ZNK11CStringBaseIwE4LeftEi.exit319
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp218:
	movl	$.L.str.15, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp219:
# BB#171:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_173
# BB#172:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_173:                              # %_ZN11CStringBaseIwED2Ev.exit322
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_174
# BB#186:                               #   in Loop: Header=BB7_4 Depth=1
.Ltmp221:
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp222:
# BB#187:                               # %_ZNK11CStringBaseIwE4LeftEi.exit329
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp224:
	movl	$.L.str.16, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp225:
# BB#188:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_190
# BB#189:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_190:                              # %_ZN11CStringBaseIwED2Ev.exit332
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_191
# BB#203:                               #   in Loop: Header=BB7_4 Depth=1
.Ltmp227:
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp228:
# BB#204:                               # %_ZNK11CStringBaseIwE4LeftEi.exit339
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp230:
	movl	$.L.str.17, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp231:
# BB#205:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_207
# BB#206:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_207:                              # %_ZN11CStringBaseIwED2Ev.exit342
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_208
# BB#219:                               #   in Loop: Header=BB7_4 Depth=1
.Ltmp233:
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp234:
# BB#220:                               # %_ZNK11CStringBaseIwE4LeftEi.exit349
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp236:
	movl	$.L.str.18, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp237:
# BB#221:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_223
# BB#222:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_223:                              # %_ZN11CStringBaseIwED2Ev.exit352
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	je	.LBB7_224
# BB#236:                               #   in Loop: Header=BB7_4 Depth=1
	movq	16(%rsp), %rdi
.Ltmp239:
	movl	$.L.str.19, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp240:
# BB#237:                               # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit359
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_238
# BB#241:                               #   in Loop: Header=BB7_4 Depth=1
	movq	16(%rsp), %rdi
.Ltmp241:
	movl	$.L.str.20, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp242:
# BB#242:                               # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit303
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_243
# BB#247:                               #   in Loop: Header=BB7_4 Depth=1
	movq	16(%rsp), %rdi
.Ltmp243:
	movl	$.L.str.21, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp244:
# BB#248:                               # %_ZNK11CStringBaseIwE13CompareNoCaseEPKw.exit
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_250
# BB#249:                               #   in Loop: Header=BB7_4 Depth=1
	movl	$1, %ebx
	jmp	.LBB7_257
.LBB7_33:                               #   in Loop: Header=BB7_4 Depth=1
	xorl	%eax, %eax
	jmp	.LBB7_50
.LBB7_73:                               #   in Loop: Header=BB7_4 Depth=1
	movl	8(%rsp), %ecx
	addl	$-3, %ecx
.Ltmp311:
	movl	$3, %edx
	leaq	40(%rsp), %rdi
	movq	%rbp, %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp312:
# BB#74:                                # %_ZNK11CStringBaseIwE3MidEi.exit268
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	$0, 8(%rsp)
	movq	(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	48(%rsp), %rbp
	incq	%rbp
	movl	12(%rsp), %r14d
	cmpl	%r14d, %ebp
	je	.LBB7_81
# BB#75:                                #   in Loop: Header=BB7_4 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp314:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp315:
# BB#76:                                # %.noexc272
                                        #   in Loop: Header=BB7_4 Depth=1
	testq	%rbx, %rbx
	je	.LBB7_77
# BB#78:                                # %.noexc272
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB7_80
# BB#79:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	jmp	.LBB7_80
.LBB7_37:                               #   in Loop: Header=BB7_4 Depth=1
	movl	$8, %eax
	jmp	.LBB7_50
.LBB7_106:                              #   in Loop: Header=BB7_4 Depth=1
	xorl	%ebx, %ebx
	movq	56(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB7_107
.LBB7_157:                              #   in Loop: Header=BB7_4 Depth=1
	movl	$10, 40(%rsp)
	movl	24(%rsp), %ecx
	addl	$-4, %ecx
.Ltmp275:
	movl	$4, %edx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp276:
# BB#158:                               # %_ZNK11CStringBaseIwE3MidEi.exit314
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp278:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	40(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r12d
.Ltmp279:
# BB#159:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movl	$1, %ebx
	je	.LBB7_161
# BB#160:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_161:                              # %_ZN11CStringBaseIwED2Ev.exit315
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	jne	.LBB7_257
# BB#162:                               # %.thread380
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	40(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 224(%rcx)
	jmp	.LBB7_255
.LBB7_40:                               #   in Loop: Header=BB7_4 Depth=1
	movl	$9, %eax
	jmp	.LBB7_50
.LBB7_174:                              #   in Loop: Header=BB7_4 Depth=1
	movl	$128, 40(%rsp)
	movl	24(%rsp), %ecx
	addl	$-2, %ecx
.Ltmp269:
	movl	$2, %edx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp270:
# BB#175:                               # %_ZNK11CStringBaseIwE3MidEi.exit324
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp272:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	40(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r12d
.Ltmp273:
# BB#176:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movl	$1, %ebx
	je	.LBB7_178
# BB#177:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_178:                              # %_ZN11CStringBaseIwED2Ev.exit325
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	jne	.LBB7_257
# BB#179:                               # %.thread382
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	40(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 228(%rcx)
	jmp	.LBB7_255
.LBB7_43:                               #   in Loop: Header=BB7_4 Depth=1
	movl	$12, %eax
	jmp	.LBB7_50
.LBB7_191:                              #   in Loop: Header=BB7_4 Depth=1
	movl	$-1, 40(%rsp)
	movl	24(%rsp), %ecx
	addl	$-2, %ecx
.Ltmp263:
	movl	$2, %edx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp264:
# BB#192:                               # %_ZNK11CStringBaseIwE3MidEi.exit334
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp266:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	leaq	40(%rsp), %rdx
	callq	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	movl	%eax, %r12d
.Ltmp267:
# BB#193:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movl	$1, %ebx
	je	.LBB7_195
# BB#194:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_195:                              # %_ZN11CStringBaseIwED2Ev.exit335
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	jne	.LBB7_257
# BB#196:                               # %.thread384
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	40(%rsp), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 232(%rcx)
	movb	$1, 244(%rcx)
	jmp	.LBB7_255
.LBB7_46:                               #   in Loop: Header=BB7_4 Depth=1
	movl	$14, %eax
	jmp	.LBB7_50
.LBB7_208:                              #   in Loop: Header=BB7_4 Depth=1
	movl	24(%rsp), %ecx
	addl	$-2, %ecx
.Ltmp257:
	movl	$2, %edx
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp258:
# BB#209:                               # %_ZNK11CStringBaseIwE3MidEi.exit344
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp260:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	92(%rsp), %edx          # 4-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	callq	_Z11ParseMtPropRK11CStringBaseIwERK14tagPROPVARIANTjRj
	movl	%eax, %r12d
.Ltmp261:
# BB#210:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	movl	$1, %ebx
	je	.LBB7_212
# BB#211:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_212:                              # %_ZN11CStringBaseIwED2Ev.exit345
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%r12d, %r12d
	jne	.LBB7_257
	jmp	.LBB7_255
.LBB7_77:                               #   in Loop: Header=BB7_4 Depth=1
	xorl	%eax, %eax
.LBB7_80:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	%r12, (%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%ebp, 12(%rsp)
	movq	%r12, %rbx
.LBB7_81:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i269
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	40(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_82:                               #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB7_82
# BB#83:                                #   in Loop: Header=BB7_4 Depth=1
	movl	48(%rsp), %eax
	movl	%eax, 8(%rsp)
	testq	%rdi, %rdi
	je	.LBB7_85
# BB#84:                                #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
	movq	(%rsp), %rbx
.LBB7_85:                               # %_ZN11CStringBaseIwED2Ev.exit273
                                        #   in Loop: Header=BB7_4 Depth=1
.Ltmp317:
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp318:
# BB#86:                                #   in Loop: Header=BB7_4 Depth=1
	movb	$1, %bl
	testl	%eax, %eax
	movb	$1, %al
	je	.LBB7_90
# BB#87:                                #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp319:
	movl	$.L.str.10, %esi
	callq	_Z15MyStringComparePKwS0_
.Ltmp320:
# BB#88:                                #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	je	.LBB7_89
# BB#100:                               #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
.Ltmp321:
	movl	$.L.str.11, %esi
	callq	_Z15MyStringComparePKwS0_
.Ltmp322:
# BB#101:                               #   in Loop: Header=BB7_4 Depth=1
	testl	%eax, %eax
	movb	$3, %al
	je	.LBB7_90
# BB#102:                               #   in Loop: Header=BB7_4 Depth=1
	movl	8(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.LBB7_103
	jmp	.LBB7_90
.LBB7_89:                               #   in Loop: Header=BB7_4 Depth=1
	movb	$2, %al
.LBB7_90:                               #   in Loop: Header=BB7_4 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	movb	%al, 247(%rcx)
.LBB7_107:                              #   in Loop: Header=BB7_4 Depth=1
	movb	%bl, 246(%rcx)
	movb	$1, 245(%rcx)
	xorl	%ebx, %ebx
.LBB7_108:                              #   in Loop: Header=BB7_4 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_110
# BB#109:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_110:                              # %_ZN11CStringBaseIwED2Ev.exit285
                                        #   in Loop: Header=BB7_4 Depth=1
	testl	%ebx, %ebx
	movl	%r15d, %r12d
.LBB7_240:                              #   in Loop: Header=BB7_4 Depth=1
	movl	$1, %ebx
	jne	.LBB7_257
	jmp	.LBB7_255
.LBB7_49:                               #   in Loop: Header=BB7_4 Depth=1
	movl	$98, %eax
.LBB7_50:                               # %_ZN11CStringBaseIwED2Ev.exit247.thread
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 212(%rcx)
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB7_255:                              #   in Loop: Header=BB7_4 Depth=1
	xorl	%ebx, %ebx
.LBB7_256:                              #   in Loop: Header=BB7_4 Depth=1
	movl	%r15d, %r12d
.LBB7_257:                              #   in Loop: Header=BB7_4 Depth=1
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_259
# BB#258:                               #   in Loop: Header=BB7_4 Depth=1
	callq	_ZdaPv
.LBB7_259:                              # %_ZN11CStringBaseIwED2Ev.exit222
                                        #   in Loop: Header=BB7_4 Depth=1
	movl	%ebx, %eax
	orl	$4, %eax
	cmpl	$4, %eax
	je	.LBB7_260
	jmp	.LBB7_2
.LBB7_1:
	movl	$2, %ebx
                                        # implicit-def: %R12D
	jmp	.LBB7_2
.LBB7_261:
	movl	$2, %ebx
.LBB7_2:                                # %._crit_edge
	xorl	%eax, %eax
	cmpl	$2, %ebx
	cmovnel	%r12d, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_234:
.Ltmp256:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#235:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_233:
.Ltmp253:
	jmp	.LBB7_263
.LBB7_217:
.Ltmp262:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#218:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_216:
.Ltmp259:
	jmp	.LBB7_263
.LBB7_201:
.Ltmp268:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#202:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_200:
.Ltmp265:
	jmp	.LBB7_263
.LBB7_231:
.Ltmp238:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#232:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_230:
.Ltmp235:
	jmp	.LBB7_263
.LBB7_184:
.Ltmp274:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#185:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_183:
.Ltmp271:
	jmp	.LBB7_263
.LBB7_214:
.Ltmp232:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#215:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_213:
.Ltmp229:
	jmp	.LBB7_263
.LBB7_98:
.Ltmp316:
	movq	%rax, %rbp
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_113
# BB#99:
	callq	_ZdaPv
	jmp	.LBB7_113
.LBB7_167:
.Ltmp280:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#168:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_166:
.Ltmp277:
	jmp	.LBB7_263
.LBB7_198:
.Ltmp226:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#199:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_197:
.Ltmp223:
	jmp	.LBB7_263
.LBB7_181:
.Ltmp220:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#182:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_180:
.Ltmp217:
	jmp	.LBB7_263
.LBB7_97:
.Ltmp313:
	jmp	.LBB7_112
.LBB7_164:
.Ltmp214:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#165:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_163:
.Ltmp211:
	jmp	.LBB7_263
.LBB7_150:
.Ltmp286:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#151:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_149:
.Ltmp283:
	jmp	.LBB7_263
.LBB7_140:
.Ltmp292:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#141:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_139:
.Ltmp289:
	jmp	.LBB7_263
.LBB7_95:
.Ltmp308:
	movq	%rax, %rbp
	movq	40(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_113
# BB#96:
	callq	_ZdaPv
	jmp	.LBB7_113
.LBB7_94:
.Ltmp305:
	jmp	.LBB7_112
.LBB7_137:
.Ltmp208:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#138:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_136:
.Ltmp205:
	jmp	.LBB7_263
.LBB7_123:
.Ltmp298:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#124:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_122:
.Ltmp295:
	jmp	.LBB7_263
.LBB7_111:
.Ltmp323:
.LBB7_112:
	movq	%rax, %rbp
.LBB7_113:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#114:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_34:                               # %_ZN11CStringBaseIwED2Ev.exit236
.Ltmp341:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_92:
.Ltmp202:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#93:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_91:
.Ltmp199:
	jmp	.LBB7_263
.LBB7_19:
.Ltmp347:
	movq	%rax, %rbp
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_264
# BB#20:
	callq	_ZdaPv
	jmp	.LBB7_264
.LBB7_18:
.Ltmp344:
	jmp	.LBB7_263
.LBB7_17:
.Ltmp326:
	jmp	.LBB7_263
.LBB7_262:
.Ltmp194:
.LBB7_263:
	movq	%rax, %rbp
.LBB7_264:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_266
# BB#265:
	callq	_ZdaPv
.LBB7_266:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end7-_ZN8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\200\005"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\375\004"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp251-.Lfunc_begin3  #   Call between .Lfunc_begin3 and .Ltmp251
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp251-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp252-.Ltmp251       #   Call between .Ltmp251 and .Ltmp252
	.long	.Ltmp253-.Lfunc_begin3  #     jumps to .Ltmp253
	.byte	0                       #   On action: cleanup
	.long	.Ltmp254-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp255-.Ltmp254       #   Call between .Ltmp254 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin3  #     jumps to .Ltmp256
	.byte	0                       #   On action: cleanup
	.long	.Ltmp249-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp246-.Ltmp249       #   Call between .Ltmp249 and .Ltmp246
	.long	.Ltmp326-.Lfunc_begin3  #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp246-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp192-.Ltmp246       #   Call between .Ltmp246 and .Ltmp192
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp193-.Ltmp192       #   Call between .Ltmp192 and .Ltmp193
	.long	.Ltmp194-.Lfunc_begin3  #     jumps to .Ltmp194
	.byte	0                       #   On action: cleanup
	.long	.Ltmp342-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp343-.Ltmp342       #   Call between .Ltmp342 and .Ltmp343
	.long	.Ltmp344-.Lfunc_begin3  #     jumps to .Ltmp344
	.byte	0                       #   On action: cleanup
	.long	.Ltmp345-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp346-.Ltmp345       #   Call between .Ltmp345 and .Ltmp346
	.long	.Ltmp347-.Lfunc_begin3  #     jumps to .Ltmp347
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp326-.Lfunc_begin3  #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp197-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp198-.Ltmp197       #   Call between .Ltmp197 and .Ltmp198
	.long	.Ltmp199-.Lfunc_begin3  #     jumps to .Ltmp199
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp201-.Ltmp200       #   Call between .Ltmp200 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin3  #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp293-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin3  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp296-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp297-.Ltmp296       #   Call between .Ltmp296 and .Ltmp297
	.long	.Ltmp298-.Lfunc_begin3  #     jumps to .Ltmp298
	.byte	0                       #   On action: cleanup
	.long	.Ltmp324-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp326-.Lfunc_begin3  #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp327-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp340-.Ltmp327       #   Call between .Ltmp327 and .Ltmp340
	.long	.Ltmp341-.Lfunc_begin3  #     jumps to .Ltmp341
	.byte	0                       #   On action: cleanup
	.long	.Ltmp299-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp300-.Ltmp299       #   Call between .Ltmp299 and .Ltmp300
	.long	.Ltmp326-.Lfunc_begin3  #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp301-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp323-.Lfunc_begin3  #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp303-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp304-.Ltmp303       #   Call between .Ltmp303 and .Ltmp304
	.long	.Ltmp305-.Lfunc_begin3  #     jumps to .Ltmp305
	.byte	0                       #   On action: cleanup
	.long	.Ltmp306-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp307-.Ltmp306       #   Call between .Ltmp306 and .Ltmp307
	.long	.Ltmp308-.Lfunc_begin3  #     jumps to .Ltmp308
	.byte	0                       #   On action: cleanup
	.long	.Ltmp309-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp310-.Ltmp309       #   Call between .Ltmp309 and .Ltmp310
	.long	.Ltmp323-.Lfunc_begin3  #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Ltmp204-.Ltmp203       #   Call between .Ltmp203 and .Ltmp204
	.long	.Ltmp205-.Lfunc_begin3  #     jumps to .Ltmp205
	.byte	0                       #   On action: cleanup
	.long	.Ltmp206-.Lfunc_begin3  # >> Call Site 22 <<
	.long	.Ltmp207-.Ltmp206       #   Call between .Ltmp206 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin3  #     jumps to .Ltmp208
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin3  # >> Call Site 23 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin3  #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.long	.Ltmp284-.Lfunc_begin3  # >> Call Site 24 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin3  #     jumps to .Ltmp286
	.byte	0                       #   On action: cleanup
	.long	.Ltmp287-.Lfunc_begin3  # >> Call Site 25 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin3  #     jumps to .Ltmp289
	.byte	0                       #   On action: cleanup
	.long	.Ltmp290-.Lfunc_begin3  # >> Call Site 26 <<
	.long	.Ltmp291-.Ltmp290       #   Call between .Ltmp290 and .Ltmp291
	.long	.Ltmp292-.Lfunc_begin3  #     jumps to .Ltmp292
	.byte	0                       #   On action: cleanup
	.long	.Ltmp209-.Lfunc_begin3  # >> Call Site 27 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin3  #     jumps to .Ltmp211
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin3  # >> Call Site 28 <<
	.long	.Ltmp213-.Ltmp212       #   Call between .Ltmp212 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin3  #     jumps to .Ltmp214
	.byte	0                       #   On action: cleanup
	.long	.Ltmp215-.Lfunc_begin3  # >> Call Site 29 <<
	.long	.Ltmp216-.Ltmp215       #   Call between .Ltmp215 and .Ltmp216
	.long	.Ltmp217-.Lfunc_begin3  #     jumps to .Ltmp217
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin3  # >> Call Site 30 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin3  #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin3  # >> Call Site 31 <<
	.long	.Ltmp222-.Ltmp221       #   Call between .Ltmp221 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin3  #     jumps to .Ltmp223
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin3  # >> Call Site 32 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin3  #     jumps to .Ltmp226
	.byte	0                       #   On action: cleanup
	.long	.Ltmp227-.Lfunc_begin3  # >> Call Site 33 <<
	.long	.Ltmp228-.Ltmp227       #   Call between .Ltmp227 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin3  #     jumps to .Ltmp229
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin3  # >> Call Site 34 <<
	.long	.Ltmp231-.Ltmp230       #   Call between .Ltmp230 and .Ltmp231
	.long	.Ltmp232-.Lfunc_begin3  #     jumps to .Ltmp232
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin3  # >> Call Site 35 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin3  #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin3  # >> Call Site 36 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin3  #     jumps to .Ltmp238
	.byte	0                       #   On action: cleanup
	.long	.Ltmp239-.Lfunc_begin3  # >> Call Site 37 <<
	.long	.Ltmp244-.Ltmp239       #   Call between .Ltmp239 and .Ltmp244
	.long	.Ltmp326-.Lfunc_begin3  #     jumps to .Ltmp326
	.byte	0                       #   On action: cleanup
	.long	.Ltmp311-.Lfunc_begin3  # >> Call Site 38 <<
	.long	.Ltmp312-.Ltmp311       #   Call between .Ltmp311 and .Ltmp312
	.long	.Ltmp313-.Lfunc_begin3  #     jumps to .Ltmp313
	.byte	0                       #   On action: cleanup
	.long	.Ltmp314-.Lfunc_begin3  # >> Call Site 39 <<
	.long	.Ltmp315-.Ltmp314       #   Call between .Ltmp314 and .Ltmp315
	.long	.Ltmp316-.Lfunc_begin3  #     jumps to .Ltmp316
	.byte	0                       #   On action: cleanup
	.long	.Ltmp275-.Lfunc_begin3  # >> Call Site 40 <<
	.long	.Ltmp276-.Ltmp275       #   Call between .Ltmp275 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin3  #     jumps to .Ltmp277
	.byte	0                       #   On action: cleanup
	.long	.Ltmp278-.Lfunc_begin3  # >> Call Site 41 <<
	.long	.Ltmp279-.Ltmp278       #   Call between .Ltmp278 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin3  #     jumps to .Ltmp280
	.byte	0                       #   On action: cleanup
	.long	.Ltmp269-.Lfunc_begin3  # >> Call Site 42 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp271-.Lfunc_begin3  #     jumps to .Ltmp271
	.byte	0                       #   On action: cleanup
	.long	.Ltmp272-.Lfunc_begin3  # >> Call Site 43 <<
	.long	.Ltmp273-.Ltmp272       #   Call between .Ltmp272 and .Ltmp273
	.long	.Ltmp274-.Lfunc_begin3  #     jumps to .Ltmp274
	.byte	0                       #   On action: cleanup
	.long	.Ltmp263-.Lfunc_begin3  # >> Call Site 44 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin3  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp266-.Lfunc_begin3  # >> Call Site 45 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin3  #     jumps to .Ltmp268
	.byte	0                       #   On action: cleanup
	.long	.Ltmp257-.Lfunc_begin3  # >> Call Site 46 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin3  #     jumps to .Ltmp259
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin3  # >> Call Site 47 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin3  #     jumps to .Ltmp262
	.byte	0                       #   On action: cleanup
	.long	.Ltmp317-.Lfunc_begin3  # >> Call Site 48 <<
	.long	.Ltmp322-.Ltmp317       #   Call between .Ltmp317 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin3  #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp322-.Lfunc_begin3  # >> Call Site 49 <<
	.long	.Lfunc_end7-.Ltmp322    #   Call between .Ltmp322 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn16_N8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.p2align	4, 0x90
	.type	_ZThn16_N8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi,@function
_ZThn16_N8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi: # @_ZThn16_N8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi # TAILCALL
.Lfunc_end8:
	.size	_ZThn16_N8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi, .Lfunc_end8-_ZThn16_N8NArchive4NZip8CHandler13SetPropertiesEPPKwPK14tagPROPVARIANTi
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIhED0Ev,"axG",@progbits,_ZN13CRecordVectorIhED0Ev,comdat
	.weak	_ZN13CRecordVectorIhED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIhED0Ev,@function
_ZN13CRecordVectorIhED0Ev:              # @_ZN13CRecordVectorIhED0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 32
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp348:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp349:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB9_2:
.Ltmp350:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN13CRecordVectorIhED0Ev, .Lfunc_end9-_ZN13CRecordVectorIhED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp348-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp349-.Ltmp348       #   Call between .Ltmp348 and .Ltmp349
	.long	.Ltmp350-.Lfunc_begin4  #     jumps to .Ltmp350
	.byte	0                       #   On action: cleanup
	.long	.Ltmp349-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Lfunc_end9-.Ltmp349    #   Call between .Ltmp349 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED0Ev,@function
_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED0Ev: # @_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE+16, (%rbx)
.Ltmp351:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp352:
# BB#1:
.Ltmp357:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp358:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB10_5:
.Ltmp359:
	movq	%rax, %r14
	jmp	.LBB10_6
.LBB10_3:
.Ltmp353:
	movq	%rax, %r14
.Ltmp354:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp355:
.LBB10_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB10_4:
.Ltmp356:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED0Ev, .Lfunc_end10-_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp351-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp353-.Lfunc_begin5  #     jumps to .Ltmp353
	.byte	0                       #   On action: cleanup
	.long	.Ltmp357-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp358-.Ltmp357       #   Call between .Ltmp357 and .Ltmp358
	.long	.Ltmp359-.Lfunc_begin5  #     jumps to .Ltmp359
	.byte	0                       #   On action: cleanup
	.long	.Ltmp354-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp355-.Ltmp354       #   Call between .Ltmp354 and .Ltmp355
	.long	.Ltmp356-.Lfunc_begin5  #     jumps to .Ltmp356
	.byte	1                       #   On action: 1
	.long	.Ltmp355-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Lfunc_end10-.Ltmp355   #   Call between .Ltmp355 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 64
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB11_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB11_6
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB11_5
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	callq	_ZdaPv
.LBB11_5:                               # %_ZN8NArchive4NZip11CUpdateItemD2Ev.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB11_6:                               #   in Loop: Header=BB11_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB11_2
.LBB11_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end11:
	.size	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE6DeleteEii, .Lfunc_end11-_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE6DeleteEii
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 64
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB12_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB12_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB12_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB12_5
.LBB12_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB12_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp360:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp361:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB12_35
# BB#12:
	movq	%rbx, %r13
.LBB12_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB12_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB12_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB12_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB12_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB12_15
.LBB12_14:
	xorl	%esi, %esi
.LBB12_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB12_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB12_16
.LBB12_29:
	movq	%r13, %rbx
.LBB12_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp362:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp363:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB12_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB12_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB12_8
.LBB12_3:
	xorl	%eax, %eax
.LBB12_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB12_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB12_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB12_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB12_30
.LBB12_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB12_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB12_24
	jmp	.LBB12_25
.LBB12_22:
	xorl	%ecx, %ecx
.LBB12_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB12_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB12_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB12_27
.LBB12_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB12_15
	jmp	.LBB12_29
.LBB12_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp364:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end12-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp360-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp360
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp360-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp363-.Ltmp360       #   Call between .Ltmp360 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin6  #     jumps to .Ltmp364
	.byte	0                       #   On action: cleanup
	.long	.Ltmp363-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Lfunc_end12-.Ltmp363   #   Call between .Ltmp363 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	67                      # 0x43
	.long	79                      # 0x4f
	.long	80                      # 0x50
	.long	89                      # 0x59
	.long	0                       # 0x0
	.size	.L.str.1, 20

	.type	.L.str.2,@object        # @.str.2
	.p2align	2
.L.str.2:
	.long	68                      # 0x44
	.long	69                      # 0x45
	.long	70                      # 0x46
	.long	76                      # 0x4c
	.long	65                      # 0x41
	.long	84                      # 0x54
	.long	69                      # 0x45
	.long	0                       # 0x0
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
	.p2align	2
.L.str.3:
	.long	68                      # 0x44
	.long	69                      # 0x45
	.long	70                      # 0x46
	.long	76                      # 0x4c
	.long	65                      # 0x41
	.long	84                      # 0x54
	.long	69                      # 0x45
	.long	54                      # 0x36
	.long	52                      # 0x34
	.long	0                       # 0x0
	.size	.L.str.3, 40

	.type	.L.str.4,@object        # @.str.4
	.p2align	2
.L.str.4:
	.long	66                      # 0x42
	.long	90                      # 0x5a
	.long	73                      # 0x49
	.long	80                      # 0x50
	.long	50                      # 0x32
	.long	0                       # 0x0
	.size	.L.str.4, 24

	.type	.L.str.5,@object        # @.str.5
	.p2align	2
.L.str.5:
	.long	76                      # 0x4c
	.long	90                      # 0x5a
	.long	77                      # 0x4d
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str.5, 20

	.type	.L.str.6,@object        # @.str.6
	.p2align	2
.L.str.6:
	.long	80                      # 0x50
	.long	80                      # 0x50
	.long	77                      # 0x4d
	.long	68                      # 0x44
	.long	0                       # 0x0
	.size	.L.str.6, 20

	.type	.L.str.7,@object        # @.str.7
	.p2align	2
.L.str.7:
	.long	69                      # 0x45
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.size	.L.str.7, 12

	.type	.L.str.8,@object        # @.str.8
	.p2align	2
.L.str.8:
	.long	65                      # 0x41
	.long	69                      # 0x45
	.long	83                      # 0x53
	.long	0                       # 0x0
	.size	.L.str.8, 16

	.type	.L.str.9,@object        # @.str.9
	.p2align	2
.L.str.9:
	.long	49                      # 0x31
	.long	50                      # 0x32
	.long	56                      # 0x38
	.long	0                       # 0x0
	.size	.L.str.9, 16

	.type	.L.str.10,@object       # @.str.10
	.p2align	2
.L.str.10:
	.long	49                      # 0x31
	.long	57                      # 0x39
	.long	50                      # 0x32
	.long	0                       # 0x0
	.size	.L.str.10, 16

	.type	.L.str.11,@object       # @.str.11
	.p2align	2
.L.str.11:
	.long	50                      # 0x32
	.long	53                      # 0x35
	.long	54                      # 0x36
	.long	0                       # 0x0
	.size	.L.str.11, 16

	.type	.L.str.12,@object       # @.str.12
	.p2align	2
.L.str.12:
	.long	90                      # 0x5a
	.long	73                      # 0x49
	.long	80                      # 0x50
	.long	67                      # 0x43
	.long	82                      # 0x52
	.long	89                      # 0x59
	.long	80                      # 0x50
	.long	84                      # 0x54
	.long	79                      # 0x4f
	.long	0                       # 0x0
	.size	.L.str.12, 40

	.type	.L.str.13,@object       # @.str.13
	.p2align	2
.L.str.13:
	.long	77                      # 0x4d
	.long	69                      # 0x45
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.size	.L.str.13, 16

	.type	.L.str.14,@object       # @.str.14
	.p2align	2
.L.str.14:
	.long	80                      # 0x50
	.long	65                      # 0x41
	.long	83                      # 0x53
	.long	83                      # 0x53
	.long	0                       # 0x0
	.size	.L.str.14, 20

	.type	.L.str.15,@object       # @.str.15
	.p2align	2
.L.str.15:
	.long	70                      # 0x46
	.long	66                      # 0x42
	.long	0                       # 0x0
	.size	.L.str.15, 12

	.type	.L.str.16,@object       # @.str.16
	.p2align	2
.L.str.16:
	.long	77                      # 0x4d
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.16, 12

	.type	.L.str.17,@object       # @.str.17
	.p2align	2
.L.str.17:
	.long	77                      # 0x4d
	.long	84                      # 0x54
	.long	0                       # 0x0
	.size	.L.str.17, 12

	.type	.L.str.18,@object       # @.str.18
	.p2align	2
.L.str.18:
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str.18, 8

	.type	.L.str.19,@object       # @.str.19
	.p2align	2
.L.str.19:
	.long	84                      # 0x54
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.19, 12

	.type	.L.str.20,@object       # @.str.20
	.p2align	2
.L.str.20:
	.long	67                      # 0x43
	.long	76                      # 0x4c
	.long	0                       # 0x0
	.size	.L.str.20, 12

	.type	.L.str.21,@object       # @.str.21
	.p2align	2
.L.str.21:
	.long	67                      # 0x43
	.long	85                      # 0x55
	.long	0                       # 0x0
	.size	.L.str.21, 12

	.type	_ZTV13CRecordVectorIhE,@object # @_ZTV13CRecordVectorIhE
	.section	.rodata._ZTV13CRecordVectorIhE,"aG",@progbits,_ZTV13CRecordVectorIhE,comdat
	.weak	_ZTV13CRecordVectorIhE
	.p2align	3
_ZTV13CRecordVectorIhE:
	.quad	0
	.quad	_ZTI13CRecordVectorIhE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIhED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIhE, 40

	.type	_ZTS13CRecordVectorIhE,@object # @_ZTS13CRecordVectorIhE
	.section	.rodata._ZTS13CRecordVectorIhE,"aG",@progbits,_ZTS13CRecordVectorIhE,comdat
	.weak	_ZTS13CRecordVectorIhE
	.p2align	4
_ZTS13CRecordVectorIhE:
	.asciz	"13CRecordVectorIhE"
	.size	_ZTS13CRecordVectorIhE, 19

	.type	_ZTI13CRecordVectorIhE,@object # @_ZTI13CRecordVectorIhE
	.section	.rodata._ZTI13CRecordVectorIhE,"aG",@progbits,_ZTI13CRecordVectorIhE,comdat
	.weak	_ZTI13CRecordVectorIhE
	.p2align	4
_ZTI13CRecordVectorIhE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIhE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIhE, 24

	.type	.L.str.22,@object       # @.str.22
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.22:
	.long	66                      # 0x42
	.long	84                      # 0x54
	.long	52                      # 0x34
	.long	0                       # 0x0
	.size	.L.str.22, 16

	.type	.L.str.23,@object       # @.str.23
	.p2align	2
.L.str.23:
	.long	72                      # 0x48
	.long	67                      # 0x43
	.long	52                      # 0x34
	.long	0                       # 0x0
	.size	.L.str.23, 16

	.type	_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE,@object # @_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive4NZip11CUpdateItemEE
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive4NZip11CUpdateItemEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive4NZip11CUpdateItemEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive4NZip11CUpdateItemEE,@object # @_ZTS13CObjectVectorIN8NArchive4NZip11CUpdateItemEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive4NZip11CUpdateItemEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive4NZip11CUpdateItemEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive4NZip11CUpdateItemEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive4NZip11CUpdateItemEE:
	.asciz	"13CObjectVectorIN8NArchive4NZip11CUpdateItemEE"
	.size	_ZTS13CObjectVectorIN8NArchive4NZip11CUpdateItemEE, 47

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive4NZip11CUpdateItemEE,@object # @_ZTI13CObjectVectorIN8NArchive4NZip11CUpdateItemEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive4NZip11CUpdateItemEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive4NZip11CUpdateItemEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive4NZip11CUpdateItemEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive4NZip11CUpdateItemEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive4NZip11CUpdateItemEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive4NZip11CUpdateItemEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
