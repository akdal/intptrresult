	.text
	.file	"checkfile.bc"
	.globl	check_file
	.p2align	4, 0x90
	.type	check_file,@function
check_file:                             # @check_file
	.cfi_startproc
# BB#0:
	subq	$152, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 160
	movq	%rdi, %rax
	leaq	8(%rsp), %rdx
	movl	$1, %edi
	movq	%rax, %rsi
	callq	__xstat
	testl	%eax, %eax
	je	.LBB0_1
# BB#2:
	callq	__errno_location
	cmpl	$2, (%rax)
	setne	%al
	addb	%al, %al
	movzbl	%al, %eax
	orl	$-3, %eax
	addq	$152, %rsp
	retq
.LBB0_1:
	xorl	%eax, %eax
	addq	$152, %rsp
	retq
.Lfunc_end0:
	.size	check_file, .Lfunc_end0-check_file
	.cfi_endproc

	.type	ibuf,@object            # @ibuf
	.comm	ibuf,512,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
