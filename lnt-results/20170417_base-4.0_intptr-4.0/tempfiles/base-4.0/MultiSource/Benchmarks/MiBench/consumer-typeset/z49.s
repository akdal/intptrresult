	.text
	.file	"z49.bc"
	.globl	PS_RestoreGraphicState
	.p2align	4, 0x90
	.type	PS_RestoreGraphicState,@function
PS_RestoreGraphicState:                 # @PS_RestoreGraphicState
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %rcx
	movl	$.L.str, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movslq	gs_stack_top(%rip), %rax
	leal	-1(%rax), %ecx
	shlq	$2, %rax
	movl	gs_stack(%rax,%rax,4), %edx
	movl	%edx, currentfont(%rip)
	movl	gs_stack+4(%rax,%rax,4), %edx
	movl	%edx, currentcolour(%rip)
	movl	gs_stack+8(%rax,%rax,4), %edx
	movl	%edx, cpexists(%rip)
	movl	gs_stack+12(%rax,%rax,4), %edx
	movl	%edx, currenty(%rip)
	movzwl	gs_stack+16(%rax,%rax,4), %eax
	movw	%ax, currentxheight2(%rip)
	movl	%ecx, gs_stack_top(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	PS_RestoreGraphicState, .Lfunc_end0-PS_RestoreGraphicState
	.cfi_endproc

	.globl	PS_PrintGraphicObject
	.p2align	4, 0x90
	.type	PS_PrintGraphicObject,@function
PS_PrintGraphicObject:                  # @PS_PrintGraphicObject
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movb	32(%r15), %al
	movl	%eax, %ecx
	addb	$-11, %cl
	cmpb	$2, %cl
	jae	.LBB1_1
# BB#17:
	addq	$64, %r15
	movq	out_fp(%rip), %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	fputs                   # TAILCALL
.LBB1_1:
	leaq	32(%r15), %r14
	cmpb	$17, %al
	jne	.LBB1_16
# BB#2:                                 # %.preheader26
	movq	8(%r15), %rbx
	cmpq	%r15, %rbx
	jne	.LBB1_4
	jmp	.LBB1_14
.LBB1_7:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, 42(%rdi)
	je	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_4 Depth=1
	movq	out_fp(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB1_4
	jmp	.LBB1_14
.LBB1_12:                               #   in Loop: Header=BB1_4 Depth=1
	addb	$-119, %al
	cmpb	$19, %al
	jbe	.LBB1_13
# BB#15:                                #   in Loop: Header=BB1_4 Depth=1
	movl	$49, %edi
	movl	$8, %esi
	movl	$.L.str.3, %edx
	movl	$2, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB1_4
	jmp	.LBB1_14
.LBB1_9:                                #   in Loop: Header=BB1_4 Depth=1
	cmpb	$0, 41(%rdi)
	je	.LBB1_13
# BB#10:                                #   in Loop: Header=BB1_4 Depth=1
	movq	out_fp(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB1_4
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_13:                               # %.backedge
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB1_4
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_4 Depth=1
	callq	PS_PrintGraphicObject
	movq	8(%rbx), %rbx
	cmpq	%r15, %rbx
	je	.LBB1_14
.LBB1_4:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_5 Depth 2
	movq	%rbx, %rdi
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	cmpq	$26, %rax
	ja	.LBB1_12
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=2
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_16:
	movl	$49, %edi
	movl	$9, %esi
	movl	$.L.str.3, %edx
	movl	$2, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	Error                   # TAILCALL
.LBB1_14:                               # %.loopexit27
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	PS_PrintGraphicObject, .Lfunc_end1-PS_PrintGraphicObject
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_5
	.quad	.LBB1_7
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_11
	.quad	.LBB1_11
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_11
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_12
	.quad	.LBB1_13

	.text
	.globl	PS_DefineGraphicNames
	.p2align	4, 0x90
	.type	PS_DefineGraphicNames,@function
PS_DefineGraphicNames:                  # @PS_DefineGraphicNames
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 64
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpb	$97, 32(%rbx)
	je	.LBB2_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_2:
	movl	76(%rbx), %edi
	movl	%edi, %eax
	andl	$4095, %eax             # imm = 0xFFF
	cmpl	currentfont(%rip), %eax
	je	.LBB2_5
# BB#3:
	movl	%eax, currentfont(%rip)
	testl	%eax, %eax
	je	.LBB2_5
# BB#4:
	movl	%eax, %edi
	callq	FontHalfXHeight
	movw	%ax, currentxheight2(%rip)
	movq	out_fp(%rip), %r14
	movl	currentfont(%rip), %edi
	movq	%rbx, %rsi
	callq	FontSize
	movl	%eax, %ebp
	movl	currentfont(%rip), %edi
	callq	FontName
	movq	%rax, %rcx
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	fprintf
	movl	76(%rbx), %edi
.LBB2_5:
	shrl	$12, %edi
	andl	$1023, %edi             # imm = 0x3FF
	cmpl	currentcolour(%rip), %edi
	je	.LBB2_8
# BB#6:
	movl	%edi, currentcolour(%rip)
	testl	%edi, %edi
	je	.LBB2_8
# BB#7:
	movq	out_fp(%rip), %rbp
	callq	ColourCommand
	movq	%rax, %rcx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	fprintf
.LBB2_8:
	movq	out_fp(%rip), %r14
	movl	48(%rbx), %r15d
	movl	60(%rbx), %r12d
	movl	56(%rbx), %r13d
	addl	%r15d, %r13d
	movl	52(%rbx), %ebp
	addl	%r12d, %ebp
	movl	currentfont(%rip), %edi
	testl	%edi, %edi
	je	.LBB2_9
# BB#10:
	movq	%rbx, %rsi
	callq	FontSize
	movl	%eax, %r10d
	jmp	.LBB2_11
.LBB2_9:
	movl	$240, %r10d
.LBB2_11:
	movswl	66(%rbx), %r11d
	movswl	70(%rbx), %ebx
	subq	$8, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.9, %esi
	movl	$0, %eax
	movq	%r14, %rdi
	movl	%r13d, %edx
	movl	%ebp, %ecx
	movl	%r15d, %r8d
	movl	%r12d, %r9d
	pushq	%rbx
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$40, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -32
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	PS_DefineGraphicNames, .Lfunc_end2-PS_DefineGraphicNames
	.cfi_endproc

	.globl	PS_SaveTranslateDefineSave
	.p2align	4, 0x90
	.type	PS_SaveTranslateDefineSave,@function
PS_SaveTranslateDefineSave:             # @PS_SaveTranslateDefineSave
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 64
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movslq	gs_stack_top(%rip), %rax
	cmpq	$48, %rax
	jg	.LBB3_3
# BB#1:
	movl	76(%rbx), %ecx
	movl	%ecx, %edi
	andl	$4095, %edi             # imm = 0xFFF
	cmpl	currentfont(%rip), %edi
	jne	.LBB3_3
# BB#2:
	shrl	$12, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	cmpl	currentcolour(%rip), %ecx
	jne	.LBB3_3
# BB#8:
	movq	%rax, %rdx
	shlq	$2, %rdx
	movl	%edi, gs_stack+20(%rdx,%rdx,4)
	movl	%ecx, gs_stack+24(%rdx,%rdx,4)
	movl	cpexists(%rip), %esi
	movl	%esi, gs_stack+28(%rdx,%rdx,4)
	movl	currenty(%rip), %esi
	movl	%esi, gs_stack+32(%rdx,%rdx,4)
	movzwl	currentxheight2(%rip), %ebp
	movw	%bp, gs_stack+36(%rdx,%rdx,4)
	movl	$0, cpexists(%rip)
	leal	2(%rax), %eax
	movl	%eax, gs_stack_top(%rip)
	movl	%edi, gs_stack+40(%rdx,%rdx,4)
	movl	%ecx, gs_stack+44(%rdx,%rdx,4)
	movl	$0, gs_stack+48(%rdx,%rdx,4)
	movl	%esi, gs_stack+52(%rdx,%rdx,4)
	movw	%bp, gs_stack+56(%rdx,%rdx,4)
	movq	out_fp(%rip), %rbp
	movl	48(%rbx), %r8d
	movl	60(%rbx), %r9d
	movl	56(%rbx), %r12d
	addl	%r8d, %r12d
	movl	52(%rbx), %r13d
	addl	%r9d, %r13d
	testl	%edi, %edi
	je	.LBB3_9
# BB#10:
	movq	%rbx, %rsi
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movl	%r9d, (%rsp)            # 4-byte Spill
	callq	FontSize
	movl	(%rsp), %r9d            # 4-byte Reload
	movl	4(%rsp), %r8d           # 4-byte Reload
	movl	%eax, %r10d
	jmp	.LBB3_11
.LBB3_3:
	movq	out_fp(%rip), %rcx
	movl	$.L.str.31, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movl	gs_stack_top(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, gs_stack_top(%rip)
	cmpl	$49, %ecx
	jl	.LBB3_5
# BB#4:
	leaq	32(%rbx), %r8
	movl	$49, %edi
	movl	$7, %esi
	movl	$.L.str.32, %edx
	movl	$1, %ecx
	movl	$50, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	gs_stack_top(%rip), %eax
.LBB3_5:                                # %PS_SaveGraphicState.exit
	movl	currentfont(%rip), %ecx
	cltq
	shlq	$2, %rax
	movl	%ecx, gs_stack(%rax,%rax,4)
	movl	currentcolour(%rip), %ecx
	movl	%ecx, gs_stack+4(%rax,%rax,4)
	movl	cpexists(%rip), %ecx
	movl	%ecx, gs_stack+8(%rax,%rax,4)
	movl	currenty(%rip), %ecx
	movl	%ecx, gs_stack+12(%rax,%rax,4)
	movzwl	currentxheight2(%rip), %ecx
	movw	%cx, gs_stack+16(%rax,%rax,4)
	movq	out_fp(%rip), %rdi
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	callq	fprintf
	movl	$0, cpexists(%rip)
	movq	%rbx, %rdi
	callq	PS_DefineGraphicNames
	movq	out_fp(%rip), %rcx
	movl	$.L.str.31, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movl	gs_stack_top(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, gs_stack_top(%rip)
	cmpl	$49, %ecx
	jl	.LBB3_7
# BB#6:
	addq	$32, %rbx
	movl	$49, %edi
	movl	$7, %esi
	movl	$.L.str.32, %edx
	movl	$1, %ecx
	movl	$50, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movl	gs_stack_top(%rip), %eax
.LBB3_7:
	movl	currentfont(%rip), %ecx
	cltq
	shlq	$2, %rax
	movl	%ecx, gs_stack(%rax,%rax,4)
	movl	currentcolour(%rip), %ecx
	movl	%ecx, gs_stack+4(%rax,%rax,4)
	movl	cpexists(%rip), %ecx
	movl	%ecx, gs_stack+8(%rax,%rax,4)
	movl	currenty(%rip), %ecx
	movl	%ecx, gs_stack+12(%rax,%rax,4)
	movzwl	currentxheight2(%rip), %ecx
	movw	%cx, gs_stack+16(%rax,%rax,4)
	addq	$8, %rsp
.LBB3_12:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_9:
	movl	$240, %r10d
.LBB3_11:
	movswl	66(%rbx), %r11d
	movswl	70(%rbx), %ebx
	subq	$8, %rsp
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.10, %esi
	movl	$0, %eax
	movq	%rbp, %rdi
	movl	%r12d, %edx
	movl	%r13d, %ecx
	pushq	%r14
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	callq	fprintf
	addq	$56, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB3_12
.Lfunc_end3:
	.size	PS_SaveTranslateDefineSave, .Lfunc_end3-PS_SaveTranslateDefineSave
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_SaveGraphicState,@function
PS_SaveGraphicState:                    # @PS_SaveGraphicState
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
.Lcfi46:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	out_fp(%rip), %rcx
	movl	$.L.str.31, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movl	gs_stack_top(%rip), %ecx
	leal	1(%rcx), %eax
	movl	%eax, gs_stack_top(%rip)
	cmpl	$49, %ecx
	jl	.LBB4_2
# BB#1:
	addq	$32, %rbx
	movl	$49, %edi
	movl	$7, %esi
	movl	$.L.str.32, %edx
	movl	$1, %ecx
	movl	$50, %r9d
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	movl	gs_stack_top(%rip), %eax
.LBB4_2:
	movl	currentfont(%rip), %ecx
	cltq
	shlq	$2, %rax
	movl	%ecx, gs_stack(%rax,%rax,4)
	movl	currentcolour(%rip), %ecx
	movl	%ecx, gs_stack+4(%rax,%rax,4)
	movl	cpexists(%rip), %ecx
	movl	%ecx, gs_stack+8(%rax,%rax,4)
	movl	currenty(%rip), %ecx
	movl	%ecx, gs_stack+12(%rax,%rax,4)
	movzwl	currentxheight2(%rip), %ecx
	movw	%cx, gs_stack+16(%rax,%rax,4)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	PS_SaveGraphicState, .Lfunc_end4-PS_SaveGraphicState
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_CoordTranslate,@function
PS_CoordTranslate:                      # @PS_CoordTranslate
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 16
	movl	%esi, %ecx
	movl	%edi, %edx
	movq	out_fp(%rip), %rdi
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$0, cpexists(%rip)
	popq	%rax
	retq
.Lfunc_end5:
	.size	PS_CoordTranslate, .Lfunc_end5-PS_CoordTranslate
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4626322717216342016     # double 20
	.text
	.globl	PS_PrintGraphicInclude
	.p2align	4, 0x90
	.type	PS_PrintGraphicInclude,@function
PS_PrintGraphicInclude:                 # @PS_PrintGraphicInclude
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$552, %rsp              # imm = 0x228
.Lcfi54:
	.cfi_def_cfa_offset 608
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %ebx
	movq	%rdi, %r13
	movb	32(%r13), %al
	andb	$-2, %al
	cmpb	$94, %al
	je	.LBB6_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_2:
	cmpb	$0, 41(%r13)
	jne	.LBB6_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_4:
	movq	8(%r13), %r15
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %r15
	cmpb	$0, 32(%r15)
	je	.LBB6_5
# BB#6:
	leaq	32(%r15), %rcx
	leaq	64(%r15), %rdi
	movzbl	32(%r13), %esi
	leaq	8(%rsp), %rdx
	leaq	20(%rsp), %r8
	callq	OpenIncGraphicFile
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB6_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_8:
	movl	76(%r13), %edi
	movl	%edi, %eax
	andl	$4095, %eax             # imm = 0xFFF
	cmpl	currentfont(%rip), %eax
	je	.LBB6_10
# BB#9:
	movl	%eax, currentfont(%rip)
	movl	%eax, %edi
	callq	FontHalfXHeight
	movw	%ax, currentxheight2(%rip)
	movq	out_fp(%rip), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	currentfont(%rip), %edi
	movq	%r13, %rsi
	callq	FontSize
	movl	%ebp, %r12d
	movl	%eax, %ebp
	movl	currentfont(%rip), %edi
	callq	FontName
	movq	%rax, %rcx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %edx
	movl	%r12d, %ebp
	callq	fprintf
	movl	76(%r13), %edi
.LBB6_10:
	shrl	$12, %edi
	andl	$1023, %edi             # imm = 0x3FF
	cmpl	currentcolour(%rip), %edi
	je	.LBB6_13
# BB#11:
	movl	%edi, currentcolour(%rip)
	testl	%edi, %edi
	je	.LBB6_13
# BB#12:
	movq	out_fp(%rip), %r12
	callq	ColourCommand
	movq	%rax, %rcx
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	fprintf
.LBB6_13:
	movq	out_fp(%rip), %rcx
	movl	$.L.str.16, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	subl	48(%r13), %ebx
	subl	60(%r13), %ebp
	movq	out_fp(%rip), %rdi
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	fprintf
	movl	$0, cpexists(%rip)
	movq	out_fp(%rip), %rdi
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	$.L.str.34, %esi
	movb	$2, %al
	movaps	%xmm0, %xmm1
	callq	fprintf
	movl	$0, cpexists(%rip)
	xorl	%edx, %edx
	subl	48(%r15), %edx
	xorl	%ecx, %ecx
	subl	52(%r15), %ecx
	movq	out_fp(%rip), %rdi
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$0, cpexists(%rip)
	movq	out_fp(%rip), %rdi
	movq	8(%rsp), %rdx
	addq	$64, %rdx
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	callq	fprintf
	leaq	32(%rsp), %rdi
	movl	$512, %esi              # imm = 0x200
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB6_29
# BB#14:                                # %.lr.ph
	leaq	35(%rsp), %r12
	leaq	32(%rsp), %rbx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB6_15:                               # =>This Inner Loop Header: Depth=1
	testb	$1, %r15b
	je	.LBB6_16
# BB#40:                                #   in Loop: Header=BB6_15 Depth=1
	movl	$.L.str.26, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB6_50
# BB#41:                                #   in Loop: Header=BB6_15 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	movq	%r12, %rsi
	callq	MakeWord
	movq	%rax, %r13
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_42
# BB#43:                                #   in Loop: Header=BB6_15 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_44
	.p2align	4, 0x90
.LBB6_16:                               #   in Loop: Header=BB6_15 Depth=1
	movl	$.L.str.18, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB6_32
# BB#17:                                #   in Loop: Header=BB6_15 Depth=1
	movl	$.L.str.19, %esi
	movq	%rbx, %rdi
	callq	StringContains
	testl	%eax, %eax
	je	.LBB6_18
.LBB6_32:                               #   in Loop: Header=BB6_15 Depth=1
	movl	$.L.str.21, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB6_34
# BB#33:                                #   in Loop: Header=BB6_15 Depth=1
	leaq	32(%r13), %r8
	movq	8(%rsp), %rax
	addq	$64, %rax
	movq	%rax, (%rsp)
	movl	$49, %edi
	movl	$10, %esi
	movl	$.L.str.22, %edx
	movl	$2, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_34:                               #   in Loop: Header=BB6_15 Depth=1
	movl	$.L.str.24, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB6_36
# BB#35:                                #   in Loop: Header=BB6_15 Depth=1
	leaq	32(%r13), %r8
	movq	8(%rsp), %rax
	addq	$64, %rax
	movq	%rax, (%rsp)
	movl	$49, %edi
	movl	$11, %esi
	movl	$.L.str.25, %edx
	movl	$2, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_36:                               #   in Loop: Header=BB6_15 Depth=1
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB6_38
# BB#37:                                #   in Loop: Header=BB6_15 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB6_28
.LBB6_50:                               #   in Loop: Header=BB6_15 Depth=1
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB6_52
# BB#51:                                #   in Loop: Header=BB6_15 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB6_28
.LBB6_38:                               # %strip_out.exit
                                        #   in Loop: Header=BB6_15 Depth=1
	movl	$.L.str.36, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB6_54
# BB#39:                                #   in Loop: Header=BB6_15 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB6_28
.LBB6_42:                               #   in Loop: Header=BB6_15 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_44:                               #   in Loop: Header=BB6_15 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	needs(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_47
# BB#45:                                #   in Loop: Header=BB6_15 Depth=1
	testq	%rcx, %rcx
	je	.LBB6_47
# BB#46:                                #   in Loop: Header=BB6_15 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_47:                               #   in Loop: Header=BB6_15 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	movb	$1, %r15b
	je	.LBB6_28
# BB#48:                                #   in Loop: Header=BB6_15 Depth=1
	testq	%rax, %rax
	je	.LBB6_28
# BB#49:                                #   in Loop: Header=BB6_15 Depth=1
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	jmp	.LBB6_27
.LBB6_18:                               #   in Loop: Header=BB6_15 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	leaq	58(%rsp), %rsi
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB6_19
# BB#20:                                #   in Loop: Header=BB6_15 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB6_21
.LBB6_52:                               # %strip_out.exit58
                                        #   in Loop: Header=BB6_15 Depth=1
	movl	$.L.str.36, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB6_54
# BB#53:                                #   in Loop: Header=BB6_15 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB6_28
.LBB6_54:                               #   in Loop: Header=BB6_15 Depth=1
	movq	out_fp(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
	xorl	%r15d, %r15d
	jmp	.LBB6_28
.LBB6_19:                               #   in Loop: Header=BB6_15 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB6_21:                               #   in Loop: Header=BB6_15 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	needs(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB6_24
# BB#22:                                #   in Loop: Header=BB6_15 Depth=1
	testq	%rcx, %rcx
	je	.LBB6_24
# BB#23:                                #   in Loop: Header=BB6_15 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB6_24:                               #   in Loop: Header=BB6_15 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	movb	$1, %r15b
	je	.LBB6_28
# BB#25:                                #   in Loop: Header=BB6_15 Depth=1
	testq	%rax, %rax
	je	.LBB6_28
# BB#26:                                #   in Loop: Header=BB6_15 Depth=1
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
.LBB6_27:                               # %.sink.split.backedge
                                        #   in Loop: Header=BB6_15 Depth=1
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB6_28:                               # %.sink.split.backedge
                                        #   in Loop: Header=BB6_15 Depth=1
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB6_15
.LBB6_29:                               # %.sink.split._crit_edge
	movq	8(%rsp), %rdi
	callq	DisposeObject
	movq	%r14, %rdi
	callq	fclose
	cmpl	$0, 20(%rsp)
	je	.LBB6_31
# BB#30:
	movl	$.L.str.27, %edi
	callq	remove
.LBB6_31:
	movq	out_fp(%rip), %rdi
	movl	$.L.str.28, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$0, wordcount(%rip)
	addq	$552, %rsp              # imm = 0x228
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	PS_PrintGraphicInclude, .Lfunc_end6-PS_PrintGraphicInclude
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_CoordScale,@function
PS_CoordScale:                          # @PS_CoordScale
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %rdi
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movl	$.L.str.34, %esi
	movb	$2, %al
	callq	fprintf
	movl	$0, cpexists(%rip)
	popq	%rax
	retq
.Lfunc_end7:
	.size	PS_CoordScale, .Lfunc_end7-PS_CoordScale
	.cfi_endproc

	.globl	ConvertToPDFName
	.p2align	4, 0x90
	.type	ConvertToPDFName,@function
ConvertToPDFName:                       # @ConvertToPDFName
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 48
.Lcfi67:
	.cfi_offset %rbx, -48
.Lcfi68:
	.cfi_offset %r12, -40
.Lcfi69:
	.cfi_offset %r13, -32
.Lcfi70:
	.cfi_offset %r14, -24
.Lcfi71:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movb	$0, ConvertToPDFName.buff+4(%rip)
	movl	$1414877004, ConvertToPDFName.buff(%rip) # imm = 0x54554F4C
	movl	$ConvertToPDFName.buff, %edi
	callq	strlen
	leaq	ConvertToPDFName.buff(%rax), %r13
	movb	64(%r15), %al
	testb	%al, %al
	je	.LBB8_9
# BB#1:                                 # %.lr.ph
	leaq	64(%r15), %r14
	leaq	32(%r15), %r12
	leaq	65(%r15), %rbx
	movl	$ConvertToPDFName.buff+199, %r15d
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r15, %r13
	jb	.LBB8_4
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	$49, %edi
	movl	$12, %esi
	movl	$.L.str.30, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	movq	%r14, %r9
	callq	Error
	movzbl	-1(%rbx), %eax
.LBB8_4:                                #   in Loop: Header=BB8_2 Depth=1
	movl	%eax, %edx
	andb	$-33, %dl
	addb	$-65, %dl
	movl	%eax, %ecx
	addb	$-48, %cl
	cmpb	$26, %dl
	movl	%eax, %edx
	jb	.LBB8_6
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=1
	movb	$95, %dl
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	cmpb	$10, %cl
	jb	.LBB8_8
# BB#7:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	%edx, %eax
.LBB8_8:                                #   in Loop: Header=BB8_2 Depth=1
	movb	%al, (%r13)
	incq	%r13
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	jne	.LBB8_2
.LBB8_9:                                # %._crit_edge
	movb	$0, (%r13)
	movl	$ConvertToPDFName.buff, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	ConvertToPDFName, .Lfunc_end8-ConvertToPDFName
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_PrintInitialize,@function
PS_PrintInitialize:                     # @PS_PrintInitialize
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 16
.Lcfi73:
	.cfi_offset %rbx, -16
	movq	%rdi, out_fp(%rip)
	movb	$0, prologue_done(%rip)
	movl	$-1, gs_stack_top(%rip)
	movl	$0, currentfont(%rip)
	movl	$0, currentcolour(%rip)
	movl	$0, cpexists(%rip)
	movl	$0, pagecount(%rip)
	movl	$0, wordcount(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB9_1
# BB#2:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB9_3
.LBB9_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB9_3:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, needs(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB9_4
# BB#5:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB9_6
.LBB9_4:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB9_6:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, supplied(%rip)
	movl	$1608, %edi             # imm = 0x648
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB9_8
# BB#7:
	movq	no_fpos(%rip), %r8
	movl	$43, %edi
	movl	$1, %esi
	movl	$.L.str.38, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB9_8:                                # %ltab_new.exit
	movl	$200, (%rbx)
	movq	%rbx, %rdi
	addq	$4, %rdi
	xorl	%esi, %esi
	movl	$1604, %edx             # imm = 0x644
	callq	memset
	movq	%rbx, link_dest_tab(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB9_9
# BB#10:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB9_11
.LBB9_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB9_11:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, link_source_list(%rip)
	popq	%rbx
	retq
.Lfunc_end9:
	.size	PS_PrintInitialize, .Lfunc_end9-PS_PrintInitialize
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1141751808              # float 567
	.text
	.p2align	4, 0x90
	.type	PS_PrintLength,@function
PS_PrintLength:                         # @PS_PrintLength
	.cfi_startproc
# BB#0:
	cvtsi2ssl	%esi, %xmm0
	divss	.LCPI10_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.39, %esi
	movb	$1, %al
	jmp	sprintf                 # TAILCALL
.Lfunc_end10:
	.size	PS_PrintLength, .Lfunc_end10-PS_PrintLength
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_PrintPageSetupForFont,@function
PS_PrintPageSetupForFont:               # @PS_PrintPageSetupForFont
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 48
.Lcfi79:
	.cfi_offset %rbx, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	out_fp(%rip), %rdi
	movl	$.L.str.40, %esi
	xorl	%eax, %eax
	callq	fprintf
	movb	60(%rbx), %al
	testb	%al, %al
	js	.LBB11_1
# BB#2:
	movq	out_fp(%rip), %rdi
	movl	$.L.str.43, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	movq	%r15, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.LBB11_1:
	andb	$127, %al
	movzbl	%al, %edi
	movl	%ebp, %esi
	callq	MapEnsurePrinted
	movq	out_fp(%rip), %rbp
	movzbl	60(%rbx), %edi
	andl	$127, %edi
	callq	MapEncodingName
	movq	%rax, %rbx
	movl	$.L.str.41, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	%rbx, %r8
	movq	%r15, %r9
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movl	$.L.str.42, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.Lfunc_end11:
	.size	PS_PrintPageSetupForFont, .Lfunc_end11-PS_PrintPageSetupForFont
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_PrintPageResourceForFont,@function
PS_PrintPageResourceForFont:            # @PS_PrintPageResourceForFont
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movq	out_fp(%rip), %rdi
	testl	%esi, %esi
	movl	$.L.str.45, %eax
	movl	$.L.str.26, %edx
	cmovneq	%rax, %rdx
	movl	$.L.str.44, %esi
	xorl	%eax, %eax
	jmp	fprintf                 # TAILCALL
.Lfunc_end12:
	.size	PS_PrintPageResourceForFont, .Lfunc_end12-PS_PrintPageResourceForFont
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_PrintMapping,@function
PS_PrintMapping:                        # @PS_PrintMapping
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %rbp, -16
	movl	%edi, %eax
	movq	MapTable(,%rax,8), %rbx
	movq	out_fp(%rip), %rdi
	movq	24(%rbx), %rdx
	addq	$64, %rdx
	movl	$.L.str.46, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movq	24(%rbx), %rdx
	addq	$64, %rdx
	movl	$.L.str.47, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %r8
	movl	$32, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_1:                               # =>This Inner Loop Header: Depth=1
	movq	32(%rbx,%rbp,8), %rdx
	addq	$64, %rdx
	incq	%rbp
	testb	$7, %bpl
	movl	$10, %ecx
	cmovnel	%r14d, %ecx
	movl	$.L.str.48, %esi
	xorl	%eax, %eax
	movq	%r8, %rdi
	callq	fprintf
	movq	out_fp(%rip), %r8
	cmpq	$256, %rbp              # imm = 0x100
	jne	.LBB13_1
# BB#2:
	movl	$.L.str.49, %edi
	movl	$6, %esi
	movl	$1, %edx
	movq	%r8, %rcx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.Lfunc_end13:
	.size	PS_PrintMapping, .Lfunc_end13-PS_PrintMapping
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4587366580439587226     # double 0.050000000000000003
	.text
	.p2align	4, 0x90
	.type	PS_PrintBeforeFirstPage,@function
PS_PrintBeforeFirstPage:                # @PS_PrintBeforeFirstPage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 56
	subq	$536, %rsp              # imm = 0x218
.Lcfi95:
	.cfi_def_cfa_offset 592
.Lcfi96:
	.cfi_offset %rbx, -56
.Lcfi97:
	.cfi_offset %r12, -48
.Lcfi98:
	.cfi_offset %r13, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %ebx
	movl	%edi, %ebp
	cmpl	$0, Encapsulated(%rip)
	movq	out_fp(%rip), %rcx
	je	.LBB14_2
# BB#1:
	movl	$.L.str.51, %edi
	movl	$24, %esi
	jmp	.LBB14_3
.LBB14_2:
	movl	$.L.str.52, %edi
	movl	$15, %esi
.LBB14_3:
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	movl	$.L.str.53, %esi
	movl	$.L.str.54, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rcx
	movl	$.L.str.55, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.56, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.57, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.58, %edi
	movl	$37, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	cmpl	$12239, %ebp            # imm = 0x2FCF
	jg	.LBB14_15
# BB#4:
	cmpl	$10799, %ebp            # imm = 0x2A2F
	jg	.LBB14_10
# BB#5:
	cmpl	$7920, %ebp             # imm = 0x1EF0
	je	.LBB14_34
# BB#6:
	cmpl	$8400, %ebp             # imm = 0x20D0
	je	.LBB14_42
# BB#7:
	cmpl	$10320, %ebp            # imm = 0x2850
	jne	.LBB14_46
# BB#8:
	cmpl	$14580, %ebx            # imm = 0x38F4
	jne	.LBB14_46
# BB#9:
	movl	$.L.str.157, %edx
	jmp	.LBB14_47
.LBB14_15:
	cmpl	$15839, %ebp            # imm = 0x3DDF
	jg	.LBB14_21
# BB#16:
	cmpl	$12240, %ebp            # imm = 0x2FD0
	je	.LBB14_26
# BB#17:
	cmpl	$14400, %ebp            # imm = 0x3840
	je	.LBB14_44
# BB#18:
	cmpl	$14580, %ebp            # imm = 0x38F4
	jne	.LBB14_46
# BB#19:
	cmpl	$20640, %ebx            # imm = 0x50A0
	jne	.LBB14_46
# BB#20:
	movl	$.L.str.156, %edx
	jmp	.LBB14_47
.LBB14_10:
	cmpl	$10800, %ebp            # imm = 0x2A30
	je	.LBB14_36
# BB#11:
	cmpl	$11900, %ebp            # imm = 0x2E7C
	je	.LBB14_40
# BB#12:
	cmpl	$12200, %ebp            # imm = 0x2FA8
	jne	.LBB14_46
# BB#13:
	cmpl	$15600, %ebx            # imm = 0x3CF0
	jne	.LBB14_46
# BB#14:
	movl	$.L.str.159, %edx
	jmp	.LBB14_47
.LBB14_21:
	cmpl	$15840, %ebp            # imm = 0x3DE0
	je	.LBB14_32
# BB#22:
	cmpl	$16840, %ebp            # imm = 0x41C8
	je	.LBB14_38
# BB#23:
	cmpl	$24480, %ebp            # imm = 0x5FA0
	jne	.LBB14_46
# BB#24:
	cmpl	$15840, %ebx            # imm = 0x3DE0
	jne	.LBB14_46
# BB#25:
	movl	$.L.str.149, %edx
	jmp	.LBB14_47
.LBB14_34:
	cmpl	$12240, %ebx            # imm = 0x2FD0
	jne	.LBB14_46
# BB#35:
	movl	$.L.str.151, %edx
	jmp	.LBB14_47
.LBB14_42:
	cmpl	$11900, %ebx            # imm = 0x2E7C
	jne	.LBB14_46
# BB#43:
	movl	$.L.str.155, %edx
	jmp	.LBB14_47
.LBB14_26:
	cmpl	$15840, %ebx            # imm = 0x3DE0
	je	.LBB14_27
# BB#28:
	cmpl	$18720, %ebx            # imm = 0x4920
	je	.LBB14_31
# BB#29:
	cmpl	$20160, %ebx            # imm = 0x4EC0
	jne	.LBB14_46
# BB#30:                                # %.fold.split.i
	movl	$.L.str.150, %edx
	jmp	.LBB14_47
.LBB14_44:
	cmpl	$20160, %ebx            # imm = 0x4EC0
	jne	.LBB14_46
# BB#45:
	movl	$.L.str.160, %edx
	jmp	.LBB14_47
.LBB14_36:
	cmpl	$14400, %ebx            # imm = 0x3840
	jne	.LBB14_46
# BB#37:
	movl	$.L.str.152, %edx
	jmp	.LBB14_47
.LBB14_40:
	cmpl	$16840, %ebx            # imm = 0x41C8
	jne	.LBB14_46
# BB#41:
	movl	$.L.str.154, %edx
	jmp	.LBB14_47
.LBB14_32:
	cmpl	$24480, %ebx            # imm = 0x5FA0
	jne	.LBB14_46
# BB#33:
	movl	$.L.str.148, %edx
	jmp	.LBB14_47
.LBB14_38:
	cmpl	$23800, %ebx            # imm = 0x5CF8
	jne	.LBB14_46
# BB#39:
	movl	$.L.str.153, %edx
	jmp	.LBB14_47
.LBB14_46:                              # %.thread27.i
	movl	$.L.str.161, %edx
.LBB14_47:                              # %MediaName.exit
	movslq	%ebp, %rax
	imulq	$1717986919, %rax, %rbp # imm = 0x66666667
	movq	%rbp, %rax
	shrq	$63, %rax
	sarq	$35, %rbp
	addl	%eax, %ebp
	movslq	%ebx, %rax
	imulq	$1717986919, %rax, %rbx # imm = 0x66666667
	movq	%rbx, %rax
	shrq	$63, %rax
	sarq	$35, %rbx
	addl	%eax, %ebx
	movl	$.L.str.59, %esi
	xorl	%eax, %eax
	movl	%ebp, %ecx
	movl	%ebx, %r8d
	callq	fprintf
	movq	out_fp(%rip), %rcx
	movl	$.L.str.60, %edi
	movl	$20, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.61, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	movl	$.L.str.62, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	movl	%ebx, %ecx
	callq	fprintf
	movq	out_fp(%rip), %rcx
	movl	$.L.str.63, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.64, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	movl	$.L.str.65, %esi
	movl	$.L.str.66, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rcx
	movl	$.L.str.67, %edi
	movl	$60, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.68, %edi
	movl	$37, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.69, %edi
	movl	$52, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.70, %edi
	movl	$54, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.71, %edi
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.72, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.73, %edi
	movl	$56, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.74, %edi
	movl	$59, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.75, %edi
	movl	$41, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.76, %edi
	movl	$44, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.77, %edi
	movl	$37, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.78, %edi
	movl	$40, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.79, %edi
	movl	$54, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.80, %edi
	movl	$57, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.81, %edi
	movl	$38, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.82, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	movl	$.L.str.83, %esi
	movl	$1440, %edx             # imm = 0x5A0
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movl	$.L.str.84, %esi
	movl	$567, %edx              # imm = 0x237
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movl	$.L.str.85, %esi
	movl	$20, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movl	$.L.str.86, %esi
	movl	$120, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rcx
	movl	$.L.str.87, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.88, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.89, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.90, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.91, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.92, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.93, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.94, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.95, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.96, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.97, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.98, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.99, %edi
	movl	$52, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.100, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.101, %edi
	movl	$34, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.102, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.103, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.104, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.105, %edi
	movl	$52, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.106, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.107, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.108, %edi
	movl	$4, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.109, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.102, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.110, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.111, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.112, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.113, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.114, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.115, %edi
	movl	$20, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.116, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.117, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.118, %edi
	movl	$40, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.119, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.120, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.121, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.122, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.123, %edi
	movl	$9, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.124, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.102, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.125, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.126, %edi
	movl	$36, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.127, %edi
	movl	$47, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.128, %edi
	movl	$24, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.129, %edi
	movl	$11, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.130, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$6, %edi
	callq	FirstFile
	testw	%ax, %ax
	je	.LBB14_69
# BB#48:                                # %.lr.ph44
	leaq	16(%rsp), %rbx
	.p2align	4, 0x90
.LBB14_49:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_66 Depth 2
	movzwl	%ax, %r12d
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r12d, %edi
	callq	OpenFile
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB14_50
# BB#52:                                #   in Loop: Header=BB14_49 Depth=1
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	je	.LBB14_53
# BB#54:                                #   in Loop: Header=BB14_49 Depth=1
	movl	$.L.str.134, %esi
	movq	%rbx, %rdi
	callq	StringBeginsWith
	testl	%eax, %eax
	je	.LBB14_64
# BB#55:                                #   in Loop: Header=BB14_49 Depth=1
	movq	no_fpos(%rip), %rdx
	movl	$11, %edi
	leaq	32(%rsp), %rsi
	callq	MakeWord
	movq	%rax, %r13
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB14_56
# BB#57:                                #   in Loop: Header=BB14_49 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB14_58
	.p2align	4, 0x90
.LBB14_50:                              #   in Loop: Header=BB14_49 Depth=1
	movl	%r12d, %edi
	callq	PosOfFile
	movq	%rax, %rbp
	movl	%r12d, %edi
	callq	FileName
	movq	%rax, (%rsp)
	movl	$49, %edi
	movl	$3, %esi
	movl	$.L.str.131, %edx
	jmp	.LBB14_51
	.p2align	4, 0x90
.LBB14_53:                              #   in Loop: Header=BB14_49 Depth=1
	movl	%r12d, %edi
	callq	PosOfFile
	movq	%rax, %rbp
	movl	%r12d, %edi
	callq	FileName
	movq	%rax, (%rsp)
	movl	$49, %edi
	movl	$4, %esi
	movl	$.L.str.133, %edx
.LBB14_51:                              #   in Loop: Header=BB14_49 Depth=1
	movl	$2, %ecx
	movl	$.L.str.132, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
	jmp	.LBB14_68
.LBB14_64:                              #   in Loop: Header=BB14_49 Depth=1
	movl	%r12d, %edi
	callq	PosOfFile
	movq	%rax, %r13
	movl	%r12d, %edi
	callq	FileName
	movq	%rax, (%rsp)
	movl	$49, %edi
	movl	$5, %esi
	movl	$.L.str.135, %edx
	movl	$2, %ecx
	movl	$.L.str.132, %r9d
	xorl	%eax, %eax
	movq	%r13, %r8
	callq	Error
	jmp	.LBB14_65
.LBB14_56:                              #   in Loop: Header=BB14_49 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB14_58:                              #   in Loop: Header=BB14_49 Depth=1
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	supplied(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB14_61
# BB#59:                                #   in Loop: Header=BB14_49 Depth=1
	testq	%rcx, %rcx
	je	.LBB14_61
# BB#60:                                #   in Loop: Header=BB14_49 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB14_61:                              #   in Loop: Header=BB14_49 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB14_65
# BB#62:                                #   in Loop: Header=BB14_49 Depth=1
	testq	%rax, %rax
	je	.LBB14_65
# BB#63:                                #   in Loop: Header=BB14_49 Depth=1
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB14_65:                              #   in Loop: Header=BB14_49 Depth=1
	movq	out_fp(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
	movq	out_fp(%rip), %r13
	movl	%r12d, %edi
	callq	FileName
	movq	%rax, %r14
	movl	$47, %esi
	movq	%r14, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rcx
	cmoveq	%r14, %rcx
	movl	$.L.str.136, %esi
	movl	$.L.str.132, %edx
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	fprintf
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	movq	out_fp(%rip), %rsi
	je	.LBB14_67
	.p2align	4, 0x90
.LBB14_66:                              # %.lr.ph40
                                        #   Parent Loop BB14_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	fputs
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	movq	out_fp(%rip), %rsi
	testq	%rax, %rax
	jne	.LBB14_66
.LBB14_67:                              # %._crit_edge41
                                        #   in Loop: Header=BB14_49 Depth=1
	movl	$10, %edi
	callq	fputc
	movq	%rbp, %rdi
	callq	fclose
.LBB14_68:                              #   in Loop: Header=BB14_49 Depth=1
	movl	%r12d, %edi
	callq	NextFile
	testw	%ax, %ax
	jne	.LBB14_49
.LBB14_69:                              # %._crit_edge45
	movq	out_fp(%rip), %rcx
	movl	$.L.str.137, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.138, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	xorl	%eax, %eax
	callq	MapPrintEncodings
	movq	out_fp(%rip), %rcx
	movl	$.L.str.139, %edi
	movl	$70, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rcx
	movl	$.L.str.140, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	movl	$.L.str.141, %esi
	xorl	%eax, %eax
	callq	fprintf
	movb	(%r15), %al
	testb	%al, %al
	je	.LBB14_72
# BB#70:                                # %.lr.ph.preheader
	incq	%r15
	.p2align	4, 0x90
.LBB14_71:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%al, %eax
	movq	EightBitToPrintForm(,%rax,8), %rdi
	movq	out_fp(%rip), %rsi
	callq	fputs
	movzbl	(%r15), %eax
	incq	%r15
	testb	%al, %al
	jne	.LBB14_71
.LBB14_72:                              # %._crit_edge
	movq	out_fp(%rip), %rdi
	movl	pagecount(%rip), %edx
	incl	%edx
	movl	%edx, pagecount(%rip)
	movl	$.L.str.142, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movl	$.L.str.143, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	callq	FontPrintPageResources
	movq	out_fp(%rip), %rcx
	movl	$.L.str.144, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	callq	FontPrintPageSetup
	callq	FontAdvanceCurrentPage
	movq	out_fp(%rip), %rdi
	movsd	.LCPI14_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.145, %esi
	movl	$10, %edx
	movb	$1, %al
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movl	$.L.str.146, %esi
	xorl	%eax, %eax
	callq	fprintf
	movb	$1, prologue_done(%rip)
	addq	$536, %rsp              # imm = 0x218
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_27:
	movl	$.L.str.147, %edx
	jmp	.LBB14_47
.LBB14_31:                              # %.fold.split35.i
	movl	$.L.str.158, %edx
	jmp	.LBB14_47
.Lfunc_end14:
	.size	PS_PrintBeforeFirstPage, .Lfunc_end14-PS_PrintBeforeFirstPage
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI15_0:
	.quad	4587366580439587226     # double 0.050000000000000003
	.text
	.p2align	4, 0x90
	.type	PS_PrintBetweenPages,@function
PS_PrintBetweenPages:                   # @PS_PrintBetweenPages
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 16
.Lcfi103:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	movq	out_fp(%rip), %rcx
	movl	$.L.str.162, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$0, gs_stack_top(%rip)
	movl	$0, cpexists(%rip)
	movl	$0, currentfont(%rip)
	movl	$0, currentcolour(%rip)
	cmpl	$0, Encapsulated(%rip)
	je	.LBB15_2
# BB#1:
	callq	PS_PrintAfterLastPage
	movq	no_fpos(%rip), %r8
	movl	$49, %edi
	movl	$6, %esi
	movl	$.L.str.163, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB15_2:
	movq	out_fp(%rip), %rdi
	movl	$.L.str.164, %esi
	xorl	%eax, %eax
	callq	fprintf
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB15_5
# BB#3:                                 # %.lr.ph.preheader
	incq	%rbx
	.p2align	4, 0x90
.LBB15_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	%al, %eax
	movq	EightBitToPrintForm(,%rax,8), %rdi
	movq	out_fp(%rip), %rsi
	callq	fputs
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	jne	.LBB15_4
.LBB15_5:                               # %._crit_edge
	movq	out_fp(%rip), %rdi
	movl	pagecount(%rip), %edx
	incl	%edx
	movl	%edx, pagecount(%rip)
	movl	$.L.str.142, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movl	$.L.str.143, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	callq	FontPrintPageResources
	movq	out_fp(%rip), %rcx
	movl	$.L.str.144, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	callq	FontPrintPageSetup
	callq	FontAdvanceCurrentPage
	movq	out_fp(%rip), %rdi
	movsd	.LCPI15_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movl	$.L.str.145, %esi
	movl	$10, %edx
	movb	$1, %al
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movl	$.L.str.165, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$0, wordcount(%rip)
	popq	%rbx
	retq
.Lfunc_end15:
	.size	PS_PrintBetweenPages, .Lfunc_end15-PS_PrintBetweenPages
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_PrintAfterLastPage,@function
PS_PrintAfterLastPage:                  # @PS_PrintAfterLastPage
	.cfi_startproc
# BB#0:
	cmpb	$1, prologue_done(%rip)
	jne	.LBB16_13
# BB#1:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 48
.Lcfi109:
	.cfi_offset %rbx, -40
.Lcfi110:
	.cfi_offset %r14, -32
.Lcfi111:
	.cfi_offset %r15, -24
.Lcfi112:
	.cfi_offset %rbp, -16
	movq	out_fp(%rip), %rcx
	movl	$.L.str.162, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movq	out_fp(%rip), %rdi
	movl	$.L.str.166, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	callq	FontNeeded
	movl	%eax, %r14d
	movq	needs(%rip), %rax
	movq	8(%rax), %rbp
	cmpq	%rax, %rbp
	je	.LBB16_8
# BB#2:                                 # %.lr.ph31.preheader
	movl	$.L.str.18, %r15d
	.p2align	4, 0x90
.LBB16_3:                               # %.lr.ph31
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_4 Depth 2
	leaq	16(%rbp), %rbx
	jmp	.LBB16_4
	.p2align	4, 0x90
.LBB16_14:                              #   in Loop: Header=BB16_4 Depth=2
	addq	$16, %rbx
.LBB16_4:                               #   Parent Loop BB16_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB16_14
# BB#5:                                 #   in Loop: Header=BB16_3 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB16_7
# BB#6:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.167, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB16_7:                               # %.loopexit
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	out_fp(%rip), %rdi
	testl	%r14d, %r14d
	movl	$.L.str.26, %edx
	cmovneq	%r15, %rdx
	addq	$64, %rbx
	xorl	%r14d, %r14d
	movl	$.L.str.168, %esi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	fprintf
	movq	8(%rbp), %rbp
	cmpq	needs(%rip), %rbp
	jne	.LBB16_3
.LBB16_8:                               # %._crit_edge32
	movq	out_fp(%rip), %rdi
	movl	$.L.str.169, %esi
	movl	$.L.str.66, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	supplied(%rip), %rax
	movq	8(%rax), %rbx
	cmpq	%rax, %rbx
	je	.LBB16_12
	.p2align	4, 0x90
.LBB16_9:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_10 Depth 2
	leaq	16(%rbx), %rax
	.p2align	4, 0x90
.LBB16_10:                              #   Parent Loop BB16_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	leaq	16(%rdx), %rax
	cmpb	$0, 32(%rdx)
	je	.LBB16_10
# BB#11:                                #   in Loop: Header=BB16_9 Depth=1
	movq	out_fp(%rip), %rdi
	addq	$64, %rdx
	movl	$.L.str.170, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%rbx), %rbx
	cmpq	supplied(%rip), %rbx
	jne	.LBB16_9
.LBB16_12:                              # %._crit_edge
	movq	out_fp(%rip), %rdi
	callq	MapPrintPSResources
	movq	out_fp(%rip), %rdi
	movl	pagecount(%rip), %edx
	movl	$.L.str.171, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	out_fp(%rip), %rdi
	movl	$.L.str.172, %esi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.LBB16_13:
	retq
.Lfunc_end16:
	.size	PS_PrintAfterLastPage, .Lfunc_end16-PS_PrintAfterLastPage
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_PrintWord,@function
PS_PrintWord:                           # @PS_PrintWord
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi117:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi119:
	.cfi_def_cfa_offset 128
.Lcfi120:
	.cfi_offset %rbx, -56
.Lcfi121:
	.cfi_offset %r12, -48
.Lcfi122:
	.cfi_offset %r13, -40
.Lcfi123:
	.cfi_offset %r14, -32
.Lcfi124:
	.cfi_offset %r15, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	incl	TotalWordCount(%rip)
	movl	%esi, %r15d
	movq	%rdi, %r14
	movl	$4095, %edi             # imm = 0xFFF
	andl	40(%r14), %edi
	cmpl	currentfont(%rip), %edi
	je	.LBB17_5
# BB#1:
	movl	%edx, %ebp
	movl	%edi, currentfont(%rip)
	callq	FontHalfXHeight
	movw	%ax, currentxheight2(%rip)
	movq	out_fp(%rip), %r12
	movl	currentfont(%rip), %edi
	movq	%r14, %rsi
	callq	FontSize
	movl	%eax, %ebx
	movl	currentfont(%rip), %edi
	callq	FontName
	movq	%rax, %rcx
	movl	$.L.str.173, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebx, %edx
	callq	fprintf
	movl	wordcount(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, wordcount(%rip)
	movq	out_fp(%rip), %rsi
	cmpl	$4, %eax
	jl	.LBB17_3
# BB#2:
	movl	$10, %edi
	callq	_IO_putc
	movl	$0, wordcount(%rip)
	jmp	.LBB17_4
.LBB17_3:
	movl	$32, %edi
	callq	_IO_putc
.LBB17_4:
	movl	%ebp, %edx
.LBB17_5:
	movl	40(%r14), %edi
	shrl	$12, %edi
	andl	$1023, %edi             # imm = 0x3FF
	cmpl	currentcolour(%rip), %edi
	je	.LBB17_11
# BB#6:
	movl	%edi, currentcolour(%rip)
	testl	%edi, %edi
	je	.LBB17_11
# BB#7:
	movl	%edx, %ebp
	movq	out_fp(%rip), %rbx
	callq	ColourCommand
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	fputs
	movl	wordcount(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, wordcount(%rip)
	movq	out_fp(%rip), %rsi
	cmpl	$4, %eax
	jl	.LBB17_9
# BB#8:
	movl	$10, %edi
	callq	_IO_putc
	movl	$0, wordcount(%rip)
	jmp	.LBB17_10
.LBB17_9:
	movl	$32, %edi
	callq	_IO_putc
.LBB17_10:
	movl	%ebp, %edx
.LBB17_11:
	movq	finfo(%rip), %rax
	movl	40(%r14), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	8(%rax,%rcx), %r8
	leaq	64(%r14), %r12
	movb	64(%r14), %al
	movq	%r12, %r10
	movq	%r12, %rdi
	jmp	.LBB17_12
.LBB17_88:                              #   in Loop: Header=BB17_12 Depth=1
	movb	%bl, (%r10)
	jmp	.LBB17_27
	.p2align	4, 0x90
.LBB17_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_15 Depth 2
                                        #       Child Loop BB17_18 Depth 3
                                        #       Child Loop BB17_24 Depth 3
	leaq	1(%rdi), %r11
	movb	%al, (%r10)
	movzbl	%al, %eax
	cmpb	$2, (%r8,%rax)
	jb	.LBB17_26
# BB#13:                                #   in Loop: Header=BB17_12 Depth=1
	movzbl	(%rdi), %r9d
	movzbl	(%r8,%r9), %eax
	cmpb	%r9b, 256(%r8,%rax)
	jne	.LBB17_26
# BB#14:                                # %.preheader225.lr.ph
                                        #   in Loop: Header=BB17_12 Depth=1
	leaq	256(%r8,%rax), %rbp
	movb	(%r11), %al
	.p2align	4, 0x90
.LBB17_15:                              # %.preheader225
                                        #   Parent Loop BB17_12 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_18 Depth 3
                                        #       Child Loop BB17_24 Depth 3
	leaq	1(%rbp), %rsi
	movb	1(%rbp), %bl
	cmpb	%al, %bl
	jne	.LBB17_16
# BB#17:                                # %.lr.ph241.preheader
                                        #   in Loop: Header=BB17_15 Depth=2
	movl	%eax, %ebx
	movq	%r11, %rdi
	.p2align	4, 0x90
.LBB17_18:                              # %.lr.ph241
                                        #   Parent Loop BB17_12 Depth=1
                                        #     Parent Loop BB17_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	%bl, %bl
	je	.LBB17_21
# BB#19:                                # %.lr.ph241
                                        #   in Loop: Header=BB17_18 Depth=3
	movzbl	1(%rsi), %ecx
	testb	%cl, %cl
	je	.LBB17_21
# BB#20:                                #   in Loop: Header=BB17_18 Depth=3
	movzbl	1(%rsi), %ebx
	incq	%rsi
	cmpb	1(%rdi), %bl
	leaq	1(%rdi), %rdi
	je	.LBB17_18
.LBB17_21:                              # %.lr.ph241..critedge.loopexit_crit_edge
                                        #   in Loop: Header=BB17_15 Depth=2
	leaq	-1(%rsi), %rbp
	cmpb	$0, 2(%rbp)
	jne	.LBB17_23
	jmp	.LBB17_88
	.p2align	4, 0x90
.LBB17_16:                              #   in Loop: Header=BB17_15 Depth=2
	movq	%r11, %rdi
	cmpb	$0, 2(%rbp)
	je	.LBB17_88
.LBB17_23:                              # %.preheader224.preheader
                                        #   in Loop: Header=BB17_15 Depth=2
	incq	%rsi
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB17_24:                              # %.preheader224
                                        #   Parent Loop BB17_12 Depth=1
                                        #     Parent Loop BB17_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$0, (%rbp)
	leaq	1(%rbp), %rbp
	jne	.LBB17_24
# BB#25:                                #   in Loop: Header=BB17_15 Depth=2
	cmpb	%r9b, (%rbp)
	je	.LBB17_15
	.p2align	4, 0x90
.LBB17_26:                              #   in Loop: Header=BB17_12 Depth=1
	movq	%r11, %rdi
.LBB17_27:                              # %.loopexit
                                        #   in Loop: Header=BB17_12 Depth=1
	incq	%r10
	movb	(%rdi), %al
	testb	%al, %al
	jne	.LBB17_12
# BB#28:
	movb	$0, (%r10)
	movq	finfo(%rip), %rax
	movl	40(%r14), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movq	16(%rax,%rcx), %r13
	movq	24(%rax,%rcx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movswl	currentxheight2(%rip), %eax
	subl	%eax, %edx
	cmpl	$0, cpexists(%rip)
	movq	%r14, 8(%rsp)           # 8-byte Spill
	je	.LBB17_37
# BB#29:
	cmpl	%edx, currenty(%rip)
	jne	.LBB17_37
# BB#30:
	testl	%r15d, %r15d
	jns	.LBB17_32
# BB#31:
	negl	%r15d
	movq	out_fp(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
.LBB17_32:                              # %.preheader219.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_33:                              # %.preheader219
                                        # =>This Inner Loop Header: Depth=1
	movslq	%r15d, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	leal	(%rdx,%rdx,4), %edx
	negl	%edx
	leal	48(%r15,%rdx), %edx
	movb	%dl, 16(%rsp,%rbx)
	incq	%rbx
	cmpl	$9, %eax
	movl	%ecx, %r15d
	jg	.LBB17_33
# BB#34:                                # %.preheader218.preheader
	movslq	%ebx, %rax
	leaq	15(%rsp,%rax), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_35:                              # %.preheader218
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r14,%rbp), %edi
	movq	out_fp(%rip), %rsi
	callq	_IO_putc
	leal	(%rbx,%rbp), %eax
	decq	%rbp
	cmpl	$1, %eax
	jne	.LBB17_35
# BB#36:
	movl	$.L.str.175, %eax
	movl	$.L.str.176, %ebp
	movq	8(%rsp), %r14           # 8-byte Reload
	testb	$64, 42(%r14)
	cmovneq	%rax, %rbp
	jmp	.LBB17_50
.LBB17_37:
	movl	%edx, currenty(%rip)
	testl	%r15d, %r15d
	jns	.LBB17_39
# BB#38:
	negl	%r15d
	movq	out_fp(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
.LBB17_39:                              # %.preheader223.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_40:                              # %.preheader223
                                        # =>This Inner Loop Header: Depth=1
	movslq	%r15d, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	leal	(%rdx,%rdx,4), %edx
	negl	%edx
	leal	48(%r15,%rdx), %edx
	movb	%dl, 16(%rsp,%rbx)
	incq	%rbx
	cmpl	$9, %eax
	movl	%ecx, %r15d
	jg	.LBB17_40
# BB#41:                                # %.preheader222.preheader
	movslq	%ebx, %rax
	leaq	15(%rsp,%rax), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_42:                              # %.preheader222
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r14,%rbp), %edi
	movq	out_fp(%rip), %rsi
	callq	_IO_putc
	leal	(%rbx,%rbp), %eax
	decq	%rbp
	cmpl	$1, %eax
	jne	.LBB17_42
# BB#43:
	movq	out_fp(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movl	currenty(%rip), %ebp
	testl	%ebp, %ebp
	jns	.LBB17_45
# BB#44:
	negl	%ebp
	movq	out_fp(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
.LBB17_45:                              # %.preheader221.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_46:                              # %.preheader221
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	leal	(%rdx,%rdx,4), %edx
	negl	%edx
	leal	48(%rbp,%rdx), %edx
	movb	%dl, 16(%rsp,%rbx)
	incq	%rbx
	cmpl	$9, %eax
	movl	%ecx, %ebp
	jg	.LBB17_46
# BB#47:                                # %.preheader220.preheader
	movslq	%ebx, %rax
	leaq	15(%rsp,%rax), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_48:                              # %.preheader220
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r14,%rbp), %edi
	movq	out_fp(%rip), %rsi
	callq	_IO_putc
	leal	(%rbx,%rbp), %eax
	decq	%rbp
	cmpl	$1, %eax
	jne	.LBB17_48
# BB#49:
	movl	$.L.str.177, %eax
	movl	$.L.str.178, %ebp
	movq	8(%rsp), %r14           # 8-byte Reload
	testl	$4194304, 40(%r14)      # imm = 0x400000
	cmovneq	%rax, %rbp
	movl	$1, cpexists(%rip)
.LBB17_50:
	movq	out_fp(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movzbl	(%r12), %eax
	cmpw	$0, (%r13,%rax,2)
	je	.LBB17_62
# BB#51:
	movq	out_fp(%rip), %rdi
	movl	$.L.str.179, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	movzbl	(%r12), %ecx
	movzwl	(%r13,%rcx,2), %eax
	leaq	(%rax,%rax,2), %rdx
	movq	48(%rsp), %rsi          # 8-byte Reload
	movb	(%rsi,%rdx,2), %al
	testb	%al, %al
	je	.LBB17_55
# BB#52:                                # %.lr.ph.i
	movq	out_fp(%rip), %r15
	movl	$.L.str.187, %ecx
	movl	$.L.str.188, %ebp
	testb	$64, 42(%r14)
	cmovneq	%rcx, %rbp
	leaq	6(%rsi,%rdx,2), %rbx
	.p2align	4, 0x90
.LBB17_53:                              # =>This Inner Loop Header: Depth=1
	movzbl	%al, %r8d
	movswl	-4(%rbx), %edx
	movswl	-2(%rbx), %ecx
	movl	$.L.str.186, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %r9
	callq	fprintf
	movzbl	(%rbx), %eax
	addq	$6, %rbx
	testb	%al, %al
	jne	.LBB17_53
# BB#54:                                # %PrintComposite.exit.loopexit
	movb	(%r12), %cl
.LBB17_55:                              # %PrintComposite.exit
	movq	finfo(%rip), %rax
	movl	40(%r14), %edx
	andl	$4095, %edx             # imm = 0xFFF
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$5, %rdx
	movq	(%rax,%rdx), %rax
	movzbl	%cl, %ecx
	leaq	(%rcx,%rcx,4), %rcx
	movswl	6(%rax,%rcx,2), %ebp
	testl	%ebp, %ebp
	jns	.LBB17_57
# BB#56:
	negl	%ebp
	movq	out_fp(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
.LBB17_57:                              # %.preheader217.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_58:                              # %.preheader217
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rax
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx), %edx
	leal	(%rdx,%rdx,4), %edx
	negl	%edx
	leal	48(%rbp,%rdx), %edx
	movb	%dl, 16(%rsp,%rbx)
	incq	%rbx
	cmpl	$9, %eax
	movl	%ecx, %ebp
	jg	.LBB17_58
# BB#59:                                # %.preheader216.preheader
	movslq	%ebx, %rax
	leaq	15(%rsp,%rax), %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_60:                              # %.preheader216
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r14,%rbp), %edi
	movq	out_fp(%rip), %rsi
	callq	_IO_putc
	leal	(%rbx,%rbp), %eax
	decq	%rbp
	cmpl	$1, %eax
	jne	.LBB17_60
# BB#61:
	movq	out_fp(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	movq	8(%rsp), %r14           # 8-byte Reload
	movl	40(%r14), %edx
	movl	$.L.str.180, %eax
	movl	$.L.str.181, %ebp
	testl	$4194304, %edx          # imm = 0x400000
	cmovneq	%rax, %rbp
	jmp	.LBB17_63
.LBB17_62:
	movq	EightBitToPrintForm(,%rax,8), %rdi
	movq	out_fp(%rip), %rsi
	callq	fputs
	movl	40(%r14), %edx
.LBB17_63:
	movb	1(%r12), %al
	testb	%al, %al
	je	.LBB17_86
# BB#64:                                # %.lr.ph
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movq	finfo(%rip), %rcx
	movl	%edx, %esi
	andl	$4095, %esi             # imm = 0xFFF
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$5, %rsi
	movq	40(%rcx,%rsi), %rsi
	movzbl	60(%rsi), %esi
	andl	$127, %esi
	movq	MapTable(,%rsi,8), %r15
	movq	%r12, %rsi
	incq	%rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	6(%rdi), %rdi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movabsq	$4294967296, %r13       # imm = 0x100000000
	jmp	.LBB17_65
	.p2align	4, 0x90
.LBB17_84:                              # %.backedge._crit_edge
                                        #   in Loop: Header=BB17_65 Depth=1
	leaq	1(%r12), %rsi
	movq	finfo(%rip), %rcx
	movl	40(%r14), %edx
.LBB17_65:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_67 Depth 2
                                        #     Child Loop BB17_74 Depth 2
                                        #     Child Loop BB17_79 Depth 2
                                        #     Child Loop BB17_81 Depth 2
	movzbl	(%r12), %edi
	movq	%rsi, %r12
	andl	$4095, %edx             # imm = 0xFFF
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$5, %rdx
	movq	64(%rcx,%rdx), %rsi
	movzbl	2945(%r15,%rdi), %edi
	movzwl	(%rsi,%rdi,2), %esi
	testq	%rsi, %rsi
	je	.LBB17_71
# BB#66:                                #   in Loop: Header=BB17_65 Depth=1
	movzbl	%al, %edi
	movb	2945(%r15,%rdi), %bl
	movq	72(%rcx,%rdx), %rdi
	.p2align	4, 0x90
.LBB17_67:                              #   Parent Loop BB17_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	%bl, (%rdi,%rsi)
	leaq	1(%rsi), %rsi
	ja	.LBB17_67
# BB#68:                                #   in Loop: Header=BB17_65 Depth=1
	jne	.LBB17_71
# BB#69:                                #   in Loop: Header=BB17_65 Depth=1
	movq	80(%rcx,%rdx), %rdi
	movq	88(%rcx,%rdx), %rcx
	movzbl	-1(%rdi,%rsi), %edx
	movswl	(%rcx,%rdx,2), %ecx
	testl	%ecx, %ecx
	je	.LBB17_71
# BB#70:                                #   in Loop: Header=BB17_65 Depth=1
	movq	out_fp(%rip), %rdi
	negl	%ecx
	movl	$.L.str.182, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	incl	wordcount(%rip)
	testb	$64, 42(%r14)
	movl	$.L.str.184, %ebp
	movl	$.L.str.183, %eax
	cmovneq	%rax, %rbp
	movb	(%r12), %al
	.p2align	4, 0x90
.LBB17_71:                              # %.thread
                                        #   in Loop: Header=BB17_65 Depth=1
	movzbl	%al, %eax
	movq	64(%rsp), %rbx          # 8-byte Reload
	cmpw	$0, (%rbx,%rax,2)
	je	.LBB17_85
# BB#72:                                #   in Loop: Header=BB17_65 Depth=1
	movq	out_fp(%rip), %rdi
	movl	$.L.str.179, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	movzbl	(%r12), %eax
	movzwl	(%rbx,%rax,2), %ecx
	leaq	(%rcx,%rcx,2), %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movb	(%rcx,%rdx,2), %cl
	testb	%cl, %cl
	je	.LBB17_76
# BB#73:                                # %.lr.ph.i212
                                        #   in Loop: Header=BB17_65 Depth=1
	testb	$64, 42(%r14)
	movq	out_fp(%rip), %rbx
	movl	$.L.str.188, %ebp
	movl	$.L.str.187, %eax
	cmovneq	%rax, %rbp
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,2), %r14
	.p2align	4, 0x90
.LBB17_74:                              #   Parent Loop BB17_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	%cl, %r8d
	movswl	-4(%r14), %edx
	movswl	-2(%r14), %ecx
	movl	$.L.str.186, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %r9
	callq	fprintf
	movzbl	(%r14), %ecx
	addq	$6, %r14
	testb	%cl, %cl
	jne	.LBB17_74
# BB#75:                                # %PrintComposite.exit214.loopexit
                                        #   in Loop: Header=BB17_65 Depth=1
	movb	(%r12), %al
	movq	8(%rsp), %r14           # 8-byte Reload
.LBB17_76:                              # %PrintComposite.exit214
                                        #   in Loop: Header=BB17_65 Depth=1
	movq	finfo(%rip), %rcx
	movl	40(%r14), %edx
	andl	$4095, %edx             # imm = 0xFFF
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$5, %rdx
	movq	(%rcx,%rdx), %rcx
	movzbl	%al, %eax
	leaq	(%rax,%rax,4), %rax
	movswl	6(%rcx,%rax,2), %ebp
	testl	%ebp, %ebp
	jns	.LBB17_78
# BB#77:                                #   in Loop: Header=BB17_65 Depth=1
	negl	%ebp
	movq	out_fp(%rip), %rsi
	movl	$45, %edi
	callq	_IO_putc
.LBB17_78:                              # %.preheader215.preheader
                                        #   in Loop: Header=BB17_65 Depth=1
	movl	$-1, %edi
	movl	$1, %edx
	xorl	%eax, %eax
	leaq	16(%rsp), %rcx
	.p2align	4, 0x90
.LBB17_79:                              # %.preheader215
                                        #   Parent Loop BB17_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %ebx
	movl	%edx, %r8d
	movslq	%ebp, %r9
	imulq	$1717986919, %r9, %rsi  # imm = 0x66666667
	movq	%rsi, %rdx
	shrq	$63, %rdx
	sarq	$34, %rsi
	addl	%edx, %esi
	leal	(%rsi,%rsi), %edx
	leal	(%rdx,%rdx,4), %edx
	negl	%edx
	leal	48(%rbp,%rdx), %edx
	movb	%dl, (%rcx)
	incq	%rcx
	addq	%r13, %rax
	leal	-1(%rbx), %edi
	leal	1(%r8), %edx
	cmpl	$9, %r9d
	movl	%esi, %ebp
	jg	.LBB17_79
# BB#80:                                # %.preheader.preheader
                                        #   in Loop: Header=BB17_65 Depth=1
	movslq	%r8d, %rbp
	leaq	15(%rsp), %rax
	addq	%rax, %rbp
	.p2align	4, 0x90
.LBB17_81:                              # %.preheader
                                        #   Parent Loop BB17_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rbp), %edi
	movq	out_fp(%rip), %rsi
	callq	_IO_putc
	decq	%rbp
	incl	%ebx
	jne	.LBB17_81
# BB#82:                                #   in Loop: Header=BB17_65 Depth=1
	movq	out_fp(%rip), %rsi
	movl	$40, %edi
	callq	_IO_putc
	testb	$64, 42(%r14)
	movl	$.L.str.181, %ebp
	movl	$.L.str.180, %eax
	cmovneq	%rax, %rbp
	jmp	.LBB17_83
	.p2align	4, 0x90
.LBB17_85:                              #   in Loop: Header=BB17_65 Depth=1
	movq	EightBitToPrintForm(,%rax,8), %rdi
	movq	out_fp(%rip), %rsi
	callq	fputs
.LBB17_83:                              # %.backedge
                                        #   in Loop: Header=BB17_65 Depth=1
	movb	1(%r12), %al
	testb	%al, %al
	jne	.LBB17_84
.LBB17_86:                              # %._crit_edge
	movl	wordcount(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, wordcount(%rip)
	cmpl	$3, %eax
	movq	out_fp(%rip), %rdi
	jle	.LBB17_89
# BB#87:
	movl	$.L.str.185, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	movl	$0, wordcount(%rip)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_89:
	movl	$.L.str.179, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.Lfunc_end17:
	.size	PS_PrintWord, .Lfunc_end17-PS_PrintWord
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_PrintPlainGraphic,@function
PS_PrintPlainGraphic:                   # @PS_PrintPlainGraphic
	.cfi_startproc
# BB#0:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.189, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end18:
	.size	PS_PrintPlainGraphic, .Lfunc_end18-PS_PrintPlainGraphic
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_PrintUnderline,@function
PS_PrintUnderline:                      # @PS_PrintUnderline
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi126:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi127:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi129:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 48
.Lcfi131:
	.cfi_offset %rbx, -48
.Lcfi132:
	.cfi_offset %r12, -40
.Lcfi133:
	.cfi_offset %r14, -32
.Lcfi134:
	.cfi_offset %r15, -24
.Lcfi135:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movl	%edi, %ebp
	cmpl	%esi, currentcolour(%rip)
	je	.LBB19_5
# BB#1:
	movl	%esi, currentcolour(%rip)
	testl	%esi, %esi
	je	.LBB19_5
# BB#2:
	movq	out_fp(%rip), %r12
	movl	%esi, %edi
	callq	ColourCommand
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	fputs
	movl	wordcount(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, wordcount(%rip)
	movq	out_fp(%rip), %rsi
	cmpl	$4, %eax
	jl	.LBB19_4
# BB#3:
	movl	$10, %edi
	callq	_IO_putc
	movl	$0, wordcount(%rip)
	jmp	.LBB19_5
.LBB19_4:
	movl	$32, %edi
	callq	_IO_putc
.LBB19_5:
	movq	out_fp(%rip), %rdi
	movq	finfo(%rip), %rax
	movl	%ebp, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movswl	56(%rax,%rcx), %edx
	subl	%edx, %ebx
	movswl	58(%rax,%rcx), %r9d
	movl	$.L.str.190, %esi
	xorl	%eax, %eax
	movl	%r15d, %edx
	movl	%r14d, %ecx
	movl	%ebx, %r8d
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fprintf                 # TAILCALL
.Lfunc_end19:
	.size	PS_PrintUnderline, .Lfunc_end19-PS_PrintUnderline
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI20_0:
	.long	1006632960              # float 0.0078125
	.text
	.p2align	4, 0x90
	.type	PS_CoordRotate,@function
PS_CoordRotate:                         # @PS_CoordRotate
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 16
	movq	out_fp(%rip), %rcx
	cvtsi2ssl	%edi, %xmm0
	mulss	.LCPI20_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.191, %esi
	movb	$1, %al
	movq	%rcx, %rdi
	callq	fprintf
	movl	$0, cpexists(%rip)
	popq	%rax
	retq
.Lfunc_end20:
	.size	PS_CoordRotate, .Lfunc_end20-PS_CoordRotate
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_LinkSource,@function
PS_LinkSource:                          # @PS_LinkSource
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi139:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi140:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi141:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi143:
	.cfi_def_cfa_offset 96
.Lcfi144:
	.cfi_offset %rbx, -56
.Lcfi145:
	.cfi_offset %r12, -48
.Lcfi146:
	.cfi_offset %r13, -40
.Lcfi147:
	.cfi_offset %r14, -32
.Lcfi148:
	.cfi_offset %r15, -24
.Lcfi149:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%edx, %r15d
	movl	%esi, %r13d
	movq	%rdi, %rbp
	movq	out_fp(%rip), %rbx
	movb	$0, ConvertToPDFName.buff+4(%rip)
	movl	$1414877004, ConvertToPDFName.buff(%rip) # imm = 0x54554F4C
	movl	$ConvertToPDFName.buff, %edi
	callq	strlen
	leaq	ConvertToPDFName.buff(%rax), %r12
	movb	64(%rbp), %al
	testb	%al, %al
	movl	%r14d, 20(%rsp)         # 4-byte Spill
	movl	%r15d, 16(%rsp)         # 4-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	%r13d, 12(%rsp)         # 4-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	je	.LBB21_1
# BB#2:                                 # %.lr.ph.i
	leaq	64(%rbp), %r14
	leaq	32(%rbp), %r15
	leaq	65(%rbp), %r13
	movl	$ConvertToPDFName.buff+199, %ebp
	movl	8(%rsp), %ebx           # 4-byte Reload
	.p2align	4, 0x90
.LBB21_3:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, %r12
	jb	.LBB21_5
# BB#4:                                 #   in Loop: Header=BB21_3 Depth=1
	movl	$49, %edi
	movl	$12, %esi
	movl	$.L.str.30, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	%r14, %r9
	callq	Error
	movzbl	-1(%r13), %eax
.LBB21_5:                               #   in Loop: Header=BB21_3 Depth=1
	movl	%eax, %edx
	andb	$-33, %dl
	addb	$-65, %dl
	movl	%eax, %ecx
	addb	$-48, %cl
	cmpb	$26, %dl
	movl	%eax, %edx
	jb	.LBB21_7
# BB#6:                                 #   in Loop: Header=BB21_3 Depth=1
	movb	$95, %dl
.LBB21_7:                               #   in Loop: Header=BB21_3 Depth=1
	cmpb	$10, %cl
	jb	.LBB21_9
# BB#8:                                 #   in Loop: Header=BB21_3 Depth=1
	movl	%edx, %eax
.LBB21_9:                               #   in Loop: Header=BB21_3 Depth=1
	movb	%al, (%r12)
	incq	%r12
	movzbl	(%r13), %eax
	incq	%r13
	testb	%al, %al
	jne	.LBB21_3
	jmp	.LBB21_10
.LBB21_1:
	movl	8(%rsp), %ebx           # 4-byte Reload
.LBB21_10:                              # %ConvertToPDFName.exit
	movb	$0, (%r12)
	movq	$ConvertToPDFName.buff, (%rsp)
	movl	$.L.str.192, %esi
	xorl	%eax, %eax
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %r8d
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	16(%rsp), %ecx          # 4-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	callq	fprintf
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB21_11
# BB#12:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB21_13
.LBB21_11:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB21_13:
	movq	24(%rsp), %rsi          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	link_source_list(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB21_16
# BB#14:
	testq	%rcx, %rcx
	je	.LBB21_16
# BB#15:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB21_16:
	movq	%rax, zz_res(%rip)
	movq	%rsi, zz_hold(%rip)
	testq	%rsi, %rsi
	je	.LBB21_19
# BB#17:
	testq	%rax, %rax
	je	.LBB21_19
# BB#18:
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB21_19:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	PS_LinkSource, .Lfunc_end21-PS_LinkSource
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_LinkDest,@function
PS_LinkDest:                            # @PS_LinkDest
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi150:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi152:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi153:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi154:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi155:
	.cfi_def_cfa_offset 64
.Lcfi156:
	.cfi_offset %rbx, -48
.Lcfi157:
	.cfi_offset %r12, -40
.Lcfi158:
	.cfi_offset %r13, -32
.Lcfi159:
	.cfi_offset %r14, -24
.Lcfi160:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	leaq	64(%r14), %r15
	movq	link_dest_tab(%rip), %rcx
	movzbl	64(%r14), %esi
	leaq	65(%r14), %rdx
	.p2align	4, 0x90
.LBB22_1:                               # =>This Inner Loop Header: Depth=1
	movl	%esi, %eax
	movzbl	(%rdx), %edi
	leal	(%rdi,%rax), %esi
	incq	%rdx
	testl	%edi, %edi
	jne	.LBB22_1
# BB#2:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	(%rcx)
	movslq	%edx, %rax
	movq	8(%rcx,%rax,8), %r12
	testq	%r12, %r12
	je	.LBB22_10
# BB#3:                                 # %.preheader39.i.preheader
	movq	%r12, %r13
	.p2align	4, 0x90
.LBB22_4:                               # %.preheader39.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_6 Depth 2
	movq	8(%r13), %r13
	cmpq	%r12, %r13
	je	.LBB22_10
# BB#5:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB22_6:                               # %.preheader.i
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB22_6
# BB#7:                                 #   in Loop: Header=BB22_4 Depth=1
	leaq	64(%rbx), %rsi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB22_4
# BB#8:                                 # %ltab_retrieve.exit
	testq	%rbx, %rbx
	je	.LBB22_10
# BB#9:
	addq	$32, %r14
	addq	$32, %rbx
	movq	%rbx, %rdi
	callq	EchoFilePos
	movq	%rax, (%rsp)
	movl	$49, %edi
	movl	$13, %esi
	movl	$.L.str.195, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r15, %r9
	callq	Error
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB22_10:                              # %ltab_retrieve.exit.thread
	movq	out_fp(%rip), %rdi
	movl	$.L.str.193, %esi
	movl	$.L.str.194, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$link_dest_tab, %esi
	movq	%r14, %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	ltab_insert             # TAILCALL
.Lfunc_end22:
	.size	PS_LinkDest, .Lfunc_end22-PS_LinkDest
	.cfi_endproc

	.p2align	4, 0x90
	.type	ltab_insert,@function
ltab_insert:                            # @ltab_insert
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi163:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi164:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi165:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi167:
	.cfi_def_cfa_offset 96
.Lcfi168:
	.cfi_offset %rbx, -56
.Lcfi169:
	.cfi_offset %r12, -48
.Lcfi170:
	.cfi_offset %r13, -40
.Lcfi171:
	.cfi_offset %r14, -32
.Lcfi172:
	.cfi_offset %r15, -24
.Lcfi173:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	movq	(%r13), %r12
	movslq	(%r12), %rbp
	leal	-1(%rbp), %eax
	cmpl	%eax, 4(%r12)
	movq	%r13, 16(%rsp)          # 8-byte Spill
	jne	.LBB23_12
# BB#1:
	leal	(%rbp,%rbp), %r14d
	movq	%rbp, %rdi
	shlq	$4, %rdi
	orq	$8, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB23_3
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$43, %edi
	movl	$1, %esi
	movl	$.L.str.38, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB23_3:
	movl	%r14d, (%rbx)
	movl	$0, 4(%rbx)
	testl	%ebp, %ebp
	jle	.LBB23_5
# BB#4:                                 # %.lr.ph.i.i
	movq	%rbx, %rdi
	addq	$8, %rdi
	decl	%r14d
	leaq	8(,%r14,8), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB23_5:                               # %ltab_new.exit.i
	movq	%rbx, 32(%rsp)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.LBB23_11
# BB#6:                                 # %.lr.ph63.preheader
	xorl	%ebp, %ebp
	leaq	32(%rsp), %rbx
	.p2align	4, 0x90
.LBB23_7:                               # %.lr.ph63
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12,%rbp,8), %rdi
	testq	%rdi, %rdi
	je	.LBB23_9
# BB#8:                                 #   in Loop: Header=BB23_7 Depth=1
	movq	%rbx, %rsi
	callq	ltab_insert
	movl	(%r12), %eax
.LBB23_9:                               #   in Loop: Header=BB23_7 Depth=1
	movslq	%eax, %rcx
	incq	%rbp
	cmpq	%rcx, %rbp
	jl	.LBB23_7
# BB#10:                                # %ltab_rehash.exit.loopexit
	movq	32(%rsp), %rbx
.LBB23_11:                              # %ltab_rehash.exit
	movq	%r12, %rdi
	callq	free
	movq	%rbx, (%r13)
	movq	%rbx, %r12
.LBB23_12:
	leaq	64(%r15), %r13
	movzbl	64(%r15), %edx
	leaq	65(%r15), %rcx
	.p2align	4, 0x90
.LBB23_13:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %eax
	movzbl	(%rcx), %esi
	leal	(%rsi,%rax), %edx
	incq	%rcx
	testl	%esi, %esi
	jne	.LBB23_13
# BB#14:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	(%r12)
	movslq	%edx, %rbp
	cmpq	$0, 8(%r12,%rbp,8)
	jne	.LBB23_19
# BB#15:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB23_16
# BB#17:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB23_18
.LBB23_16:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB23_18:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rax, 8(%rcx,%rbp,8)
	movq	(%rdx), %r12
.LBB23_19:
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	8(%r12,%rbp,8), %r14
	movq	8(%r14), %r15
	cmpq	%r14, %r15
	je	.LBB23_25
# BB#20:                                # %.lr.ph
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	32(%rax), %r12
	.p2align	4, 0x90
.LBB23_21:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_22 Depth 2
	leaq	16(%r15), %rax
	.p2align	4, 0x90
.LBB23_22:                              #   Parent Loop BB23_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB23_22
# BB#23:                                #   in Loop: Header=BB23_21 Depth=1
	leaq	64(%rbx), %rsi
	movq	%r13, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB23_24
# BB#35:                                #   in Loop: Header=BB23_21 Depth=1
	addq	$32, %rbx
	movq	%rbx, %rdi
	callq	EchoFilePos
	movq	%rax, (%rsp)
	movl	$43, %edi
	movl	$2, %esi
	movl	$.L.str.196, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r12, %r8
	movq	%r13, %r9
	callq	Error
.LBB23_24:                              # %.backedge
                                        #   in Loop: Header=BB23_21 Depth=1
	movq	8(%r15), %r15
	cmpq	%r14, %r15
	jne	.LBB23_21
.LBB23_25:                              # %._crit_edge
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB23_26
# BB#27:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB23_28
.LBB23_26:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB23_28:
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	8(%rcx,%rbp,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB23_31
# BB#29:
	testq	%rcx, %rcx
	je	.LBB23_31
# BB#30:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB23_31:
	movq	%rax, zz_res(%rip)
	movq	%rsi, zz_hold(%rip)
	testq	%rsi, %rsi
	je	.LBB23_34
# BB#32:
	testq	%rax, %rax
	je	.LBB23_34
# BB#33:
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB23_34:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	ltab_insert, .Lfunc_end23-ltab_insert
	.cfi_endproc

	.p2align	4, 0x90
	.type	PS_LinkCheck,@function
PS_LinkCheck:                           # @PS_LinkCheck
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi174:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi175:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi176:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi177:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi178:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi180:
	.cfi_def_cfa_offset 64
.Lcfi181:
	.cfi_offset %rbx, -56
.Lcfi182:
	.cfi_offset %r12, -48
.Lcfi183:
	.cfi_offset %r13, -40
.Lcfi184:
	.cfi_offset %r14, -32
.Lcfi185:
	.cfi_offset %r15, -24
.Lcfi186:
	.cfi_offset %rbp, -16
	movq	link_source_list(%rip), %rax
	movq	8(%rax), %r15
	cmpq	%rax, %r15
	je	.LBB24_16
	.p2align	4, 0x90
.LBB24_1:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_2 Depth 2
                                        #     Child Loop BB24_6 Depth 2
                                        #     Child Loop BB24_9 Depth 2
                                        #       Child Loop BB24_11 Depth 3
	leaq	16(%r15), %rbx
	jmp	.LBB24_2
	.p2align	4, 0x90
.LBB24_17:                              #   in Loop: Header=BB24_2 Depth=2
	addq	$16, %rbx
.LBB24_2:                               #   Parent Loop BB24_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB24_17
# BB#3:                                 #   in Loop: Header=BB24_1 Depth=1
	addb	$-11, %al
	cmpb	$2, %al
	jb	.LBB24_5
# BB#4:                                 #   in Loop: Header=BB24_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.5, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.197, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB24_5:                               # %.loopexit
                                        #   in Loop: Header=BB24_1 Depth=1
	leaq	64(%rbx), %r12
	movq	link_dest_tab(%rip), %rcx
	movzbl	64(%rbx), %edx
	leaq	32(%rbx), %r14
	addq	$65, %rbx
	.p2align	4, 0x90
.LBB24_6:                               #   Parent Loop BB24_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %eax
	movzbl	(%rbx), %esi
	leal	(%rsi,%rax), %edx
	incq	%rbx
	testl	%esi, %esi
	jne	.LBB24_6
# BB#7:                                 #   in Loop: Header=BB24_1 Depth=1
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	(%rcx)
	movslq	%edx, %rax
	movq	8(%rcx,%rax,8), %r13
	testq	%r13, %r13
	je	.LBB24_14
# BB#8:                                 # %.preheader39.i.preheader
                                        #   in Loop: Header=BB24_1 Depth=1
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB24_9:                               # %.preheader39.i
                                        #   Parent Loop BB24_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_11 Depth 3
	movq	8(%rbx), %rbx
	cmpq	%r13, %rbx
	je	.LBB24_14
# BB#10:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB24_9 Depth=2
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB24_11:                              # %.preheader.i
                                        #   Parent Loop BB24_1 Depth=1
                                        #     Parent Loop BB24_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB24_11
# BB#12:                                #   in Loop: Header=BB24_9 Depth=2
	leaq	64(%rbp), %rsi
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB24_9
# BB#13:                                # %ltab_retrieve.exit
                                        #   in Loop: Header=BB24_1 Depth=1
	testq	%rbp, %rbp
	jne	.LBB24_15
	.p2align	4, 0x90
.LBB24_14:                              # %ltab_retrieve.exit.thread
                                        #   in Loop: Header=BB24_1 Depth=1
	movl	$49, %edi
	movl	$14, %esi
	movl	$.L.str.198, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	%r12, %r9
	callq	Error
.LBB24_15:                              #   in Loop: Header=BB24_1 Depth=1
	movq	8(%r15), %r15
	cmpq	link_source_list(%rip), %r15
	jne	.LBB24_1
.LBB24_16:                              # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	PS_LinkCheck, .Lfunc_end24-PS_LinkCheck
	.cfi_endproc

	.type	out_fp,@object          # @out_fp
	.local	out_fp
	.comm	out_fp,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\ngrestore\n"
	.size	.L.str, 11

	.type	gs_stack,@object        # @gs_stack
	.local	gs_stack
	.comm	gs_stack,1000,16
	.type	gs_stack_top,@object    # @gs_stack_top
	.local	gs_stack_top
	.comm	gs_stack_top,4,4
	.type	currentfont,@object     # @currentfont
	.local	currentfont
	.comm	currentfont,4,4
	.type	currentcolour,@object   # @currentcolour
	.local	currentcolour
	.comm	currentcolour,4,4
	.type	cpexists,@object        # @cpexists
	.local	cpexists
	.comm	cpexists,4,4
	.type	currenty,@object        # @currenty
	.local	currenty
	.comm	currenty,4,4
	.type	currentxheight2,@object # @currentxheight2
	.local	currentxheight2
	.comm	currentxheight2,2,2
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"error in left parameter of %s"
	.size	.L.str.3, 30

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"@Graphic"
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"assert failed in %s"
	.size	.L.str.5, 20

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"PrintGraphic: type(x) != GRAPHIC!"
	.size	.L.str.6, 34

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%hd %s "
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s "
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%d %d %d %d %d %d %d LoutGraphic\n"
	.size	.L.str.9, 34

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%d %d %d %d %d %d %d %d %d LoutGr2\n"
	.size	.L.str.10, 36

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"PrintGraphicInclude!"
	.size	.L.str.11, 21

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"PrintGraphicInclude: !incgraphic_ok(x)!"
	.size	.L.str.12, 40

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"PrintGraphicInclude: fp!"
	.size	.L.str.13, 25

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%hd %s\n"
	.size	.L.str.14, 8

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"%s\n"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"BeginEPSF\n"
	.size	.L.str.16, 11

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%%%%BeginDocument: %s\n"
	.size	.L.str.17, 23

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%%DocumentNeededResources:"
	.size	.L.str.18, 27

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"(atend)"
	.size	.L.str.19, 8

	.type	needs,@object           # @needs
	.local	needs
	.comm	needs,8,8
	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%%LanguageLevel:"
	.size	.L.str.21, 17

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"ignoring LanguageLevel comment in %s file %s"
	.size	.L.str.22, 45

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"@IncludeGraphic"
	.size	.L.str.23, 16

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"%%Extensions:"
	.size	.L.str.24, 14

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"ignoring Extensions comment in %s file %s"
	.size	.L.str.25, 42

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"%%+"
	.size	.L.str.26, 4

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"lout.eps"
	.size	.L.str.27, 9

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"\n%%%%EndDocument\nEndEPSF\n"
	.size	.L.str.28, 26

	.type	wordcount,@object       # @wordcount
	.local	wordcount
	.comm	wordcount,4,4
	.type	ConvertToPDFName.buff,@object # @ConvertToPDFName.buff
	.local	ConvertToPDFName.buff
	.comm	ConvertToPDFName.buff,200,16
	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"LOUT"
	.size	.L.str.29, 5

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"tag %s is too long"
	.size	.L.str.30, 19

	.type	ps_back,@object         # @ps_back
	.data
	.p2align	3
ps_back:
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.37
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	PS_PrintInitialize
	.quad	PS_PrintLength
	.quad	PS_PrintPageSetupForFont
	.quad	PS_PrintPageResourceForFont
	.quad	PS_PrintMapping
	.quad	PS_PrintBeforeFirstPage
	.quad	PS_PrintBetweenPages
	.quad	PS_PrintAfterLastPage
	.quad	PS_PrintWord
	.quad	PS_PrintPlainGraphic
	.quad	PS_PrintUnderline
	.quad	PS_CoordTranslate
	.quad	PS_CoordRotate
	.quad	PS_CoordScale
	.quad	PS_SaveGraphicState
	.quad	PS_RestoreGraphicState
	.quad	PS_PrintGraphicObject
	.quad	PS_DefineGraphicNames
	.quad	PS_SaveTranslateDefineSave
	.quad	PS_PrintGraphicInclude
	.quad	PS_LinkSource
	.quad	PS_LinkDest
	.quad	PS_LinkCheck
	.size	ps_back, 232

	.type	PS_BackEnd,@object      # @PS_BackEnd
	.globl	PS_BackEnd
	.p2align	3
PS_BackEnd:
	.quad	ps_back
	.size	PS_BackEnd, 8

	.type	Encapsulated,@object    # @Encapsulated
	.comm	Encapsulated,4,4
	.type	.L.str.31,@object       # @.str.31
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.31:
	.asciz	"gsave\n"
	.size	.L.str.31, 7

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"rotations, graphics etc. too deeply nested (max is %d)"
	.size	.L.str.32, 55

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%d %d translate\n"
	.size	.L.str.33, 17

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"%.4f %.4f scale\n"
	.size	.L.str.34, 17

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"%%EOF"
	.size	.L.str.35, 6

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"%%Trailer"
	.size	.L.str.36, 10

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"PostScript"
	.size	.L.str.37, 11

	.type	prologue_done,@object   # @prologue_done
	.local	prologue_done
	.comm	prologue_done,1,4
	.type	pagecount,@object       # @pagecount
	.local	pagecount
	.comm	pagecount,4,4
	.type	supplied,@object        # @supplied
	.local	supplied
	.comm	supplied,8,8
	.type	link_dest_tab,@object   # @link_dest_tab
	.local	link_dest_tab
	.comm	link_dest_tab,8,8
	.type	link_source_list,@object # @link_source_list
	.local	link_source_list
	.comm	link_source_list,8,8
	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"run out of memory enlarging link dest table"
	.size	.L.str.38, 44

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"%.3fc"
	.size	.L.str.39, 6

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"%%%%IncludeResource: font %s\n"
	.size	.L.str.40, 30

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"/%s%s %s /%s LoutRecode\n"
	.size	.L.str.41, 25

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"/%s { /%s%s LoutFont } def\n"
	.size	.L.str.42, 28

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"/%s { /%s LoutFont } def\n"
	.size	.L.str.43, 26

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"%s font %s\n"
	.size	.L.str.44, 12

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"%%PageResources:"
	.size	.L.str.45, 17

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"%%%%BeginResource: encoding %s\n"
	.size	.L.str.46, 32

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"/%s [\n"
	.size	.L.str.47, 7

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"/%s%c"
	.size	.L.str.48, 6

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"] def\n"
	.size	.L.str.49, 7

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"%%%%EndResource\n\n"
	.size	.L.str.50, 18

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"%!PS-Adobe-3.0 EPSF-3.0\n"
	.size	.L.str.51, 25

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"%!PS-Adobe-3.0\n"
	.size	.L.str.52, 16

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"%%%%Creator: %s\n"
	.size	.L.str.53, 17

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"Basser Lout Version 3.24 (October 2000)"
	.size	.L.str.54, 40

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"%%%%CreationDate: Sometime Today\n"
	.size	.L.str.55, 34

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"%%DocumentData: Binary\n"
	.size	.L.str.56, 24

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"%%DocumentNeededResources: (atend)\n"
	.size	.L.str.57, 36

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"%%DocumentSuppliedResources: (atend)\n"
	.size	.L.str.58, 38

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"%%%%DocumentMedia: %s %d %d 0 white ()\n"
	.size	.L.str.59, 40

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"%%PageOrder: Ascend\n"
	.size	.L.str.60, 21

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"%%Pages: (atend)\n"
	.size	.L.str.61, 18

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"%%%%BoundingBox: 0 0 %d %d\n"
	.size	.L.str.62, 28

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"%%EndComments\n\n"
	.size	.L.str.63, 16

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"%%BeginProlog\n"
	.size	.L.str.64, 15

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%%%%BeginResource: procset %s\n"
	.size	.L.str.65, 31

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"LoutStartUp"
	.size	.L.str.66, 12

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"/save_cp { currentpoint /cp_y exch def /cp_x exch def } def\n"
	.size	.L.str.67, 61

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"/restore_cp { cp_x cp_y moveto } def\n"
	.size	.L.str.68, 38

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"/outline { gsave 1 1 1 setrgbcolor dup show save_cp\n"
	.size	.L.str.69, 53

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"  grestore true charpath stroke restore_cp } bind def\n"
	.size	.L.str.70, 55

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"/m  { 3 1 roll moveto show } bind def\n"
	.size	.L.str.71, 39

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"/mo { 3 1 roll moveto outline } bind def\n"
	.size	.L.str.72, 42

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"/s  { exch currentpoint exch pop moveto show } bind def\n"
	.size	.L.str.73, 57

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"/so { exch currentpoint exch pop moveto outline } bind def\n"
	.size	.L.str.74, 60

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"/k  { exch neg 0 rmoveto show } bind def\n"
	.size	.L.str.75, 42

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"/ko { exch neg 0 rmoveto outline } bind def\n"
	.size	.L.str.76, 45

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"/r  { exch 0 rmoveto show } bind def\n"
	.size	.L.str.77, 38

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"/ro { exch 0 rmoveto outline } bind def\n"
	.size	.L.str.78, 41

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"/c  { gsave 3 1 roll rmoveto show grestore } bind def\n"
	.size	.L.str.79, 55

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"/co { gsave 3 1 roll rmoveto outline grestore } bind def\n"
	.size	.L.str.80, 58

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"/ul { gsave setlinewidth dup 3 1 roll\n"
	.size	.L.str.81, 39

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"      moveto lineto stroke grestore } bind def\n"
	.size	.L.str.82, 48

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"/in { %d mul } def\n"
	.size	.L.str.83, 20

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"/cm { %d mul } def\n"
	.size	.L.str.84, 20

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"/pt { %d mul } def\n"
	.size	.L.str.85, 20

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"/em { %d mul } def\n"
	.size	.L.str.86, 20

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"/sp { louts mul } def\n"
	.size	.L.str.87, 23

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"/vs { loutv mul } def\n"
	.size	.L.str.88, 23

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"/ft { loutf mul } def\n"
	.size	.L.str.89, 23

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"/dg {           } def\n\n"
	.size	.L.str.90, 24

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"/LoutGraphic {\n"
	.size	.L.str.91, 16

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"  /louts exch def\n"
	.size	.L.str.92, 19

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"  /loutv exch def\n"
	.size	.L.str.93, 19

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"  /loutf exch def\n"
	.size	.L.str.94, 19

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"  /ymark exch def\n"
	.size	.L.str.95, 19

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"  /xmark exch def\n"
	.size	.L.str.96, 19

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"  /ysize exch def\n"
	.size	.L.str.97, 19

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"  /xsize exch def\n} def\n\n"
	.size	.L.str.98, 26

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"/LoutGr2 { gsave translate LoutGraphic gsave } def\n\n"
	.size	.L.str.99, 53

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"/LoutFont\n"
	.size	.L.str.100, 11

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"{ findfont exch scalefont setfont\n"
	.size	.L.str.101, 35

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"} bind def\n\n"
	.size	.L.str.102, 13

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"/LoutRecode {\n"
	.size	.L.str.103, 15

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"  { findfont dup length dict begin\n"
	.size	.L.str.104, 36

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"    {1 index /FID ne {def} {pop pop} ifelse} forall\n"
	.size	.L.str.105, 53

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"    /Encoding exch def\n"
	.size	.L.str.106, 24

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"    currentdict end definefont pop\n"
	.size	.L.str.107, 36

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"  }\n"
	.size	.L.str.108, 5

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"  stopped pop\n"
	.size	.L.str.109, 15

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"/BeginEPSF {\n"
	.size	.L.str.110, 14

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"  /LoutEPSFState save def\n"
	.size	.L.str.111, 27

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"  /dict_count countdictstack def\n"
	.size	.L.str.112, 34

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"  /op_count count 1 sub def\n"
	.size	.L.str.113, 29

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"  userdict begin\n"
	.size	.L.str.114, 18

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"  /showpage { } def\n"
	.size	.L.str.115, 21

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"  0 setgray 0 setlinecap\n"
	.size	.L.str.116, 26

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"  1 setlinewidth 0 setlinejoin\n"
	.size	.L.str.117, 32

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"  10 setmiterlimit [] 0 setdash newpath\n"
	.size	.L.str.118, 41

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"  /languagelevel where\n"
	.size	.L.str.119, 24

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"  { pop languagelevel\n"
	.size	.L.str.120, 23

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"    1 ne\n"
	.size	.L.str.121, 10

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"    { false setstrokeadjust false setoverprint\n"
	.size	.L.str.122, 48

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"    } if\n"
	.size	.L.str.123, 10

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"  } if\n"
	.size	.L.str.124, 8

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"/EndEPSF {\n"
	.size	.L.str.125, 12

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"  count op_count sub { pop } repeat\n"
	.size	.L.str.126, 37

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"  countdictstack dict_count sub { end } repeat\n"
	.size	.L.str.127, 48

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"  LoutEPSFState restore\n"
	.size	.L.str.128, 25

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"} bind def\n"
	.size	.L.str.129, 12

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"%%EndResource\n\n"
	.size	.L.str.130, 16

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"cannot open %s file %s"
	.size	.L.str.131, 23

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"@PrependGraphic"
	.size	.L.str.132, 16

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"%s file %s is empty"
	.size	.L.str.133, 20

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"%%BeginResource:"
	.size	.L.str.134, 17

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"%s file %s lacks PostScript BeginResource comment"
	.size	.L.str.135, 50

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"%% %s file %s\n"
	.size	.L.str.136, 15

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"%%EndProlog\n\n"
	.size	.L.str.137, 14

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"%%BeginSetup\n"
	.size	.L.str.138, 14

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"/pdfmark where {pop} {userdict /pdfmark /cleartomark load put} ifelse\n"
	.size	.L.str.139, 71

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"%%EndSetup\n\n"
	.size	.L.str.140, 13

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"%%%%Page: "
	.size	.L.str.141, 11

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	" %d\n"
	.size	.L.str.142, 5

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"%%%%BeginPageSetup\n"
	.size	.L.str.143, 20

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"/pgsave save def\n"
	.size	.L.str.144, 18

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"%.4f dup scale %d setlinewidth\n"
	.size	.L.str.145, 32

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"%%%%EndPageSetup\n\n"
	.size	.L.str.146, 19

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"Letter"
	.size	.L.str.147, 7

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"Tabloid"
	.size	.L.str.148, 8

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"Ledger"
	.size	.L.str.149, 7

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"Legal"
	.size	.L.str.150, 6

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"Statement"
	.size	.L.str.151, 10

	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"Executive"
	.size	.L.str.152, 10

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"A3"
	.size	.L.str.153, 3

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"A4"
	.size	.L.str.154, 3

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"A5"
	.size	.L.str.155, 3

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"B4"
	.size	.L.str.156, 3

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"B5"
	.size	.L.str.157, 3

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"Folio"
	.size	.L.str.158, 6

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"Quarto"
	.size	.L.str.159, 7

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"10x14"
	.size	.L.str.160, 6

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"Plain"
	.size	.L.str.161, 6

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"\npgsave restore\nshowpage\n"
	.size	.L.str.162, 26

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"truncating -EPS document at end of first page"
	.size	.L.str.163, 46

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"\n%%%%Page: "
	.size	.L.str.164, 12

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	"%%%%EndPageSetup\n"
	.size	.L.str.165, 18

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"\n%%%%Trailer\n"
	.size	.L.str.166, 14

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"PrintAfterLast: needs!"
	.size	.L.str.167, 23

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"%s %s"
	.size	.L.str.168, 6

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"%%%%DocumentSuppliedResources: procset %s\n"
	.size	.L.str.169, 43

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"%%%%+ %s"
	.size	.L.str.170, 9

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"%%%%Pages: %d\n"
	.size	.L.str.171, 15

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"%%%%EOF\n"
	.size	.L.str.172, 9

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"%hd %s"
	.size	.L.str.173, 7

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"so"
	.size	.L.str.175, 3

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"s"
	.size	.L.str.176, 2

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"mo"
	.size	.L.str.177, 3

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"m"
	.size	.L.str.178, 2

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	")%s "
	.size	.L.str.179, 5

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"ro"
	.size	.L.str.180, 3

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"r"
	.size	.L.str.181, 2

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	")%s %d("
	.size	.L.str.182, 8

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"ko"
	.size	.L.str.183, 3

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"k"
	.size	.L.str.184, 2

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	")%s\n"
	.size	.L.str.185, 5

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"%d %d (%c)%s "
	.size	.L.str.186, 14

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"co"
	.size	.L.str.187, 3

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"c"
	.size	.L.str.188, 2

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"PS_PrintPlainGraphic: this routine should never be called!"
	.size	.L.str.189, 59

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"%d %d %d %d ul\n"
	.size	.L.str.190, 16

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"%.4f rotate\n"
	.size	.L.str.191, 13

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"\n[ /Rect [%d %d %d %d] /Subtype /Link /Dest /%s /ANN pdfmark\n"
	.size	.L.str.192, 62

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"\n[ /Dest /%s /DEST pdfmark\n"
	.size	.L.str.193, 28

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"IGNORED"
	.size	.L.str.194, 8

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"link destination %s ignored (there is already one at%s)"
	.size	.L.str.195, 56

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"link name %s used twice (first at%s)"
	.size	.L.str.196, 37

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	" PS_LinkCheck: !is_word(type(y))!"
	.size	.L.str.197, 34

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"link name %s has no destination point"
	.size	.L.str.198, 38


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
