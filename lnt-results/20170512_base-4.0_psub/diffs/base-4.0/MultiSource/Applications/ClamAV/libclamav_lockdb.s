	.text
	.file	"libclamav_lockdb.bc"
	.globl	cli_readlockdb
	.p2align	4, 0x90
	.type	cli_readlockdb,@function
cli_readlockdb:                         # @cli_readlockdb
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	cli_readlockdb, .Lfunc_end0-cli_readlockdb
	.cfi_endproc

	.globl	cli_writelockdb
	.p2align	4, 0x90
	.type	cli_writelockdb,@function
cli_writelockdb:                        # @cli_writelockdb
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	cli_writelockdb, .Lfunc_end1-cli_writelockdb
	.cfi_endproc

	.globl	cli_unlockdb
	.p2align	4, 0x90
	.type	cli_unlockdb,@function
cli_unlockdb:                           # @cli_unlockdb
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	cli_unlockdb, .Lfunc_end2-cli_unlockdb
	.cfi_endproc

	.globl	cli_freelocks
	.p2align	4, 0x90
	.type	cli_freelocks,@function
cli_freelocks:                          # @cli_freelocks
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	cli_freelocks, .Lfunc_end3-cli_freelocks
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
