	.text
	.file	"jmemmgr.bc"
	.globl	jinit_memory_mgr
	.p2align	4, 0x90
	.type	jinit_memory_mgr,@function
jinit_memory_mgr:                       # @jinit_memory_mgr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	$0, 8(%r14)
	callq	jpeg_mem_init
	movq	%rax, %r15
	movq	%r15, 8(%rsp)
	movl	$160, %esi
	movq	%r14, %rdi
	callq	jpeg_get_small
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB0_2
# BB#1:
	movq	%r14, %rdi
	callq	jpeg_mem_term
	movq	(%r14), %rax
	movl	$53, 40(%rax)
	movl	$0, 44(%rax)
	movq	%r14, %rdi
	callq	*(%rax)
.LBB0_2:
	movl	$alloc_large, %eax
	movd	%rax, %xmm0
	movl	$alloc_small, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$alloc_sarray, 16(%rbx)
	movq	$alloc_barray, 24(%rbx)
	movq	$request_virt_sarray, 32(%rbx)
	movq	$request_virt_barray, 40(%rbx)
	movq	$realize_virt_arrays, 48(%rbx)
	movq	$access_virt_sarray, 56(%rbx)
	movq	$access_virt_barray, 64(%rbx)
	movq	$free_pool, 72(%rbx)
	movq	$self_destruct, 80(%rbx)
	movq	%r15, 88(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 96(%rbx)
	movdqu	%xmm0, 112(%rbx)
	movdqu	%xmm0, 128(%rbx)
	movq	$160, 144(%rbx)
	movq	%rbx, 8(%r14)
	movl	$.L.str, %edi
	callq	getenv
	movq	%rax, %rdi
	testq	%rdi, %rdi
	je	.LBB0_8
# BB#3:
	movb	$120, 7(%rsp)
	leaq	8(%rsp), %rdx
	leaq	7(%rsp), %rcx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	sscanf
	testl	%eax, %eax
	jle	.LBB0_8
# BB#4:
	movb	7(%rsp), %al
	orb	$32, %al
	cmpb	$109, %al
	jne	.LBB0_5
# BB#6:
	imulq	$1000, 8(%rsp), %rax    # imm = 0x3E8
	movq	%rax, 8(%rsp)
	jmp	.LBB0_7
.LBB0_5:                                # %._crit_edge
	movq	8(%rsp), %rax
.LBB0_7:
	imulq	$1000, %rax, %rax       # imm = 0x3E8
	movq	%rax, 88(%rbx)
.LBB0_8:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	jinit_memory_mgr, .Lfunc_end0-jinit_memory_mgr
	.cfi_endproc

	.p2align	4, 0x90
	.type	alloc_small,@function
alloc_small:                            # @alloc_small
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 80
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movl	%esi, %r14d
	movq	%rdi, %r13
	movq	8(%r13), %r12
	cmpq	$999999977, %rbp        # imm = 0x3B9AC9E9
	jb	.LBB1_2
# BB#1:
	movq	(%r13), %rax
	movl	$53, 40(%rax)
	movl	$1, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB1_2:
	movq	%rbp, %rax
	andq	$7, %rax
	movl	$8, %ecx
	subq	%rax, %rcx
	xorl	%r15d, %r15d
	movq	%rbp, %rax
	andq	$7, %rax
	cmovneq	%rcx, %r15
	cmpl	$2, %r14d
	jb	.LBB1_4
# BB#3:
	movq	(%r13), %rax
	movl	$12, 40(%rax)
	movl	%r14d, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB1_4:
	addq	%rbp, %r15
	movslq	%r14d, %rax
	movq	96(%r12,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB1_5
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph83
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %r14
	movq	16(%r14), %rbx
	cmpq	%r15, %rbx
	jae	.LBB1_15
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=1
	movq	(%r14), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_6
	jmp	.LBB1_8
.LBB1_5:
	xorl	%r14d, %r14d
.LBB1_8:                                # %._crit_edge84
	leaq	96(%r12,%rax,8), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	testq	%r14, %r14
	movl	$first_pool_slop, %ecx
	movl	$extra_pool_slop, %edx
	cmoveq	%rcx, %rdx
	movq	(%rdx,%rax,8), %rax
	movl	$999999976, %ebx        # imm = 0x3B9AC9E8
	subq	%r15, %rbx
	cmpq	%rbx, %rax
	cmovbeq	%rax, %rbx
	leaq	24(%r15,%rbx), %rbp
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	jpeg_get_small
	testq	%rax, %rax
	jne	.LBB1_14
# BB#9:                                 # %.lr.ph
	movq	%r12, 8(%rsp)           # 8-byte Spill
	leaq	24(%r15), %r12
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rax
	shrq	%rbx
	cmpq	$99, %rax
	ja	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_10 Depth=1
	movq	(%r13), %rax
	movl	$53, 40(%rax)
	movl	$2, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB1_12:                               # %.backedge
                                        #   in Loop: Header=BB1_10 Depth=1
	leaq	(%r12,%rbx), %rbp
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	jpeg_get_small
	testq	%rax, %rax
	je	.LBB1_10
# BB#13:
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB1_14:                               # %._crit_edge
	addq	%rbp, 144(%r12)
	addq	%r15, %rbx
	testq	%r14, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmovneq	%r14, %rcx
	movq	%rax, (%rcx)
	movq	%rax, %r14
.LBB1_15:                               # %.loopexit
	movq	8(%r14), %rcx
	leaq	24(%r14,%rcx), %rax
	addq	%r15, %rcx
	movq	%rcx, 8(%r14)
	subq	%r15, %rbx
	movq	%rbx, 16(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	alloc_small, .Lfunc_end1-alloc_small
	.cfi_endproc

	.p2align	4, 0x90
	.type	alloc_large,@function
alloc_large:                            # @alloc_large
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 64
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movl	%esi, %r14d
	movq	%rdi, %r13
	movq	8(%r13), %r12
	cmpq	$999999977, %rbp        # imm = 0x3B9AC9E9
	jb	.LBB2_2
# BB#1:
	movq	(%r13), %rax
	movl	$53, 40(%rax)
	movl	$3, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB2_2:
	movq	%rbp, %rax
	andq	$7, %rax
	movl	$8, %ecx
	subq	%rax, %rcx
	xorl	%ebx, %ebx
	movq	%rbp, %rax
	andq	$7, %rax
	cmovneq	%rcx, %rbx
	addq	%rbp, %rbx
	cmpl	$2, %r14d
	jb	.LBB2_4
# BB#3:
	movq	(%r13), %rax
	movl	$12, 40(%rax)
	movl	%r14d, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB2_4:
	leaq	24(%rbx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	jpeg_get_large
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB2_6
# BB#5:
	movq	(%r13), %rax
	movl	$53, 40(%rax)
	movl	$4, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB2_6:
	addq	%r15, 144(%r12)
	movslq	%r14d, %rax
	movq	112(%r12,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	%rbx, 8(%rbp)
	movq	$0, 16(%rbp)
	movq	%rbp, 112(%r12,%rax,8)
	addq	$24, %rbp
	movq	%rbp, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	alloc_large, .Lfunc_end2-alloc_large
	.cfi_endproc

	.p2align	4, 0x90
	.type	alloc_sarray,@function
alloc_sarray:                           # @alloc_sarray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 80
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%edx, %ecx
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %rbx
	movq	8(%rbx), %r14
	movl	%ecx, %ebp
	movl	$999999976, %eax        # imm = 0x3B9AC9E8
	xorl	%edx, %edx
	divq	%rbp
	movq	%rax, %r13
	cmpl	$999999977, %ecx        # imm = 0x3B9AC9E9
	jb	.LBB3_2
# BB#1:
	movq	(%rbx), %rax
	movl	$69, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB3_2:
	movl	%r12d, %edx
	cmpq	%rdx, %r13
	cmovael	%r12d, %r13d
	movl	%r13d, 152(%r14)
	shlq	$3, %rdx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	alloc_small
	movq	%rax, %rbx
	testl	%r12d, %r12d
	je	.LBB3_8
# BB#3:                                 # %.lr.ph57.preheader
	movl	%r12d, %eax
	notl	%eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph57
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
	movl	%r13d, %r15d
	movl	%r12d, %r13d
	subl	%r14d, %r13d
	cmpl	%r13d, %r15d
	cmovbl	%r15d, %r13d
	movq	%r13, %rdx
	imulq	%rbp, %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	alloc_large
	testl	%r13d, %r13d
	je	.LBB3_4
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	(%rcx,%r14), %ecx
	notl	%r15d
	cmpl	%r15d, %ecx
	cmoval	%ecx, %r15d
	incl	%r15d
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, %ecx
	incl	%r14d
	movq	%rax, (%rbx,%rcx,8)
	addq	%rbp, %rax
	incl	%r15d
	jne	.LBB3_7
.LBB3_4:                                # %.loopexit
                                        #   in Loop: Header=BB3_5 Depth=1
	cmpl	%r12d, %r14d
	jb	.LBB3_5
.LBB3_8:                                # %._crit_edge
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	alloc_sarray, .Lfunc_end3-alloc_sarray
	.cfi_endproc

	.p2align	4, 0x90
	.type	alloc_barray,@function
alloc_barray:                           # @alloc_barray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 80
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movl	%edx, %ecx
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %rbx
	movq	8(%rbx), %r14
	movl	%ecx, %ebp
	shlq	$7, %rbp
	movl	$999999976, %eax        # imm = 0x3B9AC9E8
	xorl	%edx, %edx
	divq	%rbp
	movq	%rax, %r13
	cmpl	$7812500, %ecx          # imm = 0x773594
	jb	.LBB4_2
# BB#1:
	movq	(%rbx), %rax
	movl	$69, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB4_2:
	movl	%r12d, %edx
	cmpq	%rdx, %r13
	cmovael	%r12d, %r13d
	movl	%r13d, 152(%r14)
	shlq	$3, %rdx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	alloc_small
	movq	%rax, %rbx
	testl	%r12d, %r12d
	je	.LBB4_8
# BB#3:                                 # %.lr.ph57.preheader
	movl	%r12d, %eax
	notl	%eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph57
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_7 Depth 2
	movl	%r13d, %r15d
	movl	%r12d, %r13d
	subl	%r14d, %r13d
	cmpl	%r13d, %r15d
	cmovbl	%r15d, %r13d
	movq	%rbp, %rdx
	imulq	%r13, %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	callq	alloc_large
	testl	%r13d, %r13d
	je	.LBB4_4
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	(%rcx,%r14), %ecx
	notl	%r15d
	cmpl	%r15d, %ecx
	cmoval	%ecx, %r15d
	incl	%r15d
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, %ecx
	incl	%r14d
	movq	%rax, (%rbx,%rcx,8)
	addq	%rbp, %rax
	incl	%r15d
	jne	.LBB4_7
.LBB4_4:                                # %.loopexit
                                        #   in Loop: Header=BB4_5 Depth=1
	cmpl	%r12d, %r14d
	jb	.LBB4_5
.LBB4_8:                                # %._crit_edge
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	alloc_barray, .Lfunc_end4-alloc_barray
	.cfi_endproc

	.p2align	4, 0x90
	.type	request_virt_sarray,@function
request_virt_sarray:                    # @request_virt_sarray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 64
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, %r13d
	movl	%ecx, %r12d
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movq	8(%rbp), %r14
	cmpl	$1, %ebx
	je	.LBB5_2
# BB#1:
	movq	(%rbp), %rax
	movl	$12, 40(%rax)
	movl	%ebx, 44(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB5_2:
	movl	$152, %edx
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	alloc_small
	movq	$0, (%rax)
	movl	%r13d, 8(%rax)
	movl	%r12d, 12(%rax)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 16(%rax)
	movl	%r15d, 36(%rax)
	movl	$0, 44(%rax)
	movq	128(%r14), %rcx
	movq	%rcx, 48(%rax)
	movq	%rax, 128(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	request_virt_sarray, .Lfunc_end5-request_virt_sarray
	.cfi_endproc

	.p2align	4, 0x90
	.type	request_virt_barray,@function
request_virt_barray:                    # @request_virt_barray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 64
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, %r13d
	movl	%ecx, %r12d
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movq	8(%rbp), %r14
	cmpl	$1, %ebx
	je	.LBB6_2
# BB#1:
	movq	(%rbp), %rax
	movl	$12, 40(%rax)
	movl	%ebx, 44(%rax)
	movq	%rbp, %rdi
	callq	*(%rax)
.LBB6_2:
	movl	$152, %edx
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	alloc_small
	movq	$0, (%rax)
	movl	%r13d, 8(%rax)
	movl	%r12d, 12(%rax)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 16(%rax)
	movl	%r15d, 36(%rax)
	movl	$0, 44(%rax)
	movq	136(%r14), %rcx
	movq	%rcx, 48(%rax)
	movq	%rax, 136(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	request_virt_barray, .Lfunc_end6-request_virt_barray
	.cfi_endproc

	.p2align	4, 0x90
	.type	realize_virt_arrays,@function
realize_virt_arrays:                    # @realize_virt_arrays
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 128
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	8(%rdi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	128(%rax), %rax
	testq	%rax, %rax
	je	.LBB7_1
# BB#2:                                 # %.lr.ph139.preheader
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph139
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rax)
	jne	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movl	16(%rax), %ecx
	movl	8(%rax), %edx
	movl	12(%rax), %esi
	imulq	%rsi, %rcx
	addq	%rcx, %r15
	imulq	%rsi, %rdx
	addq	%rdx, %rbx
.LBB7_5:                                #   in Loop: Header=BB7_3 Depth=1
	movq	48(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_3
	jmp	.LBB7_6
.LBB7_1:
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
.LBB7_6:                                # %._crit_edge140
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	136(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_8
	jmp	.LBB7_11
	.p2align	4, 0x90
.LBB7_10:                               #   in Loop: Header=BB7_8 Depth=1
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.LBB7_11
.LBB7_8:                                # %.lr.ph132
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rax)
	jne	.LBB7_10
# BB#9:                                 #   in Loop: Header=BB7_8 Depth=1
	movl	16(%rax), %ecx
	movl	8(%rax), %edx
	movl	12(%rax), %esi
	imulq	%rsi, %rcx
	shlq	$7, %rcx
	addq	%rcx, %r15
	imulq	%rsi, %rdx
	shlq	$7, %rdx
	addq	%rdx, %rbx
	jmp	.LBB7_10
.LBB7_11:                               # %._crit_edge
	testq	%r15, %r15
	jle	.LBB7_56
# BB#12:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	144(%rax), %rcx
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	jpeg_mem_available
	cmpq	%rbx, %rax
	jge	.LBB7_13
# BB#14:
	cqto
	idivq	%r15
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	jmp	.LBB7_15
.LBB7_13:
	movl	$1000000000, %edi       # imm = 0x3B9ACA00
.LBB7_15:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	128(%rax), %rbp
	testq	%rbp, %rbp
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	je	.LBB7_16
	.p2align	4, 0x90
.LBB7_21:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_30 Depth 2
                                        #       Child Loop BB7_37 Depth 3
	cmpq	$0, (%rbp)
	jne	.LBB7_39
# BB#22:                                #   in Loop: Header=BB7_21 Depth=1
	movl	8(%rbp), %r13d
	movl	16(%rbp), %ecx
	leaq	-1(%r13), %rax
	cqto
	idivq	%rcx
	cmpq	%rdi, %rax
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	jge	.LBB7_24
# BB#23:                                #   in Loop: Header=BB7_21 Depth=1
	movl	%r13d, 20(%rbp)
	leaq	12(%rbp), %rbx
	jmp	.LBB7_25
.LBB7_24:                               #   in Loop: Header=BB7_21 Depth=1
	imull	%edi, %ecx
	movl	%ecx, 20(%rbp)
	leaq	56(%rbp), %rsi
	leaq	12(%rbp), %rbx
	movl	12(%rbp), %edx
	imulq	%r13, %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	jpeg_open_backing_store
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	$1, 44(%rbp)
	movl	20(%rbp), %r13d
.LBB7_25:                               #   in Loop: Header=BB7_21 Depth=1
	movl	(%rbx), %ebp
	movl	$999999976, %eax        # imm = 0x3B9AC9E8
	xorl	%edx, %edx
	divq	%rbp
	cmpq	$999999977, %rbp        # imm = 0x3B9AC9E9
	movq	8(%rsi), %rbx
	jb	.LBB7_27
# BB#26:                                #   in Loop: Header=BB7_21 Depth=1
	movq	(%rsi), %rcx
	movl	$69, 40(%rcx)
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %r14
	callq	*(%rcx)
	movq	%r14, %rax
.LBB7_27:                               #   in Loop: Header=BB7_21 Depth=1
	movl	%r13d, %edx
	cmpq	%rdx, %rax
	cmovael	%r13d, %eax
	movl	%eax, 152(%rbx)
	shlq	$3, %rdx
	movl	$1, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rax, %rbx
	callq	alloc_small
	movq	%rbx, %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rax, %r12
	testl	%r13d, %r13d
	je	.LBB7_38
# BB#28:                                # %.lr.ph57.i.preheader
                                        #   in Loop: Header=BB7_21 Depth=1
	movl	%r13d, %eax
	notl	%eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_30:                               # %.lr.ph57.i
                                        #   Parent Loop BB7_21 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_37 Depth 3
	movl	%ecx, %r15d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	subl	%r14d, %r13d
	cmpl	%r13d, %r15d
	cmovbl	%r15d, %r13d
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movq	%r13, %rbx
	imulq	%rbp, %rbx
	movq	8(%rsi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmpq	$999999977, %rbx        # imm = 0x3B9AC9E9
	jb	.LBB7_32
# BB#31:                                #   in Loop: Header=BB7_30 Depth=2
	movq	(%rsi), %rax
	movl	$53, 40(%rax)
	movl	$3, 44(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB7_32:                               #   in Loop: Header=BB7_30 Depth=2
	movq	%rbx, %rax
	andq	$7, %rax
	movl	$8, %ecx
	subq	%rax, %rcx
	movq	%rbx, %rax
	andq	$7, %rax
	movl	$0, %eax
	cmovneq	%rcx, %rax
	leaq	(%rax,%rbx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	24(%rax,%rbx), %r13
	movq	%rsi, %rdi
	movq	%r13, %rsi
	callq	jpeg_get_large
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB7_34
# BB#33:                                #   in Loop: Header=BB7_30 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rax
	movl	$53, 40(%rax)
	movl	$4, 44(%rax)
	callq	*(%rax)
.LBB7_34:                               # %alloc_large.exit
                                        #   in Loop: Header=BB7_30 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	addq	%r13, 144(%rcx)
	movq	120(%rcx), %rax
	movq	%rax, (%rbx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, 120(%rcx)
	movq	56(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	je	.LBB7_35
# BB#36:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_30 Depth=2
	addq	$24, %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r14), %eax
	notl	%r15d
	cmpl	%r15d, %eax
	cmoval	%eax, %r15d
	incl	%r15d
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_37:                               # %.lr.ph.i
                                        #   Parent Loop BB7_21 Depth=1
                                        #     Parent Loop BB7_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r14d, %eax
	incl	%r14d
	movq	%rbx, (%r12,%rax,8)
	addq	%rbp, %rbx
	incl	%r15d
	jne	.LBB7_37
	jmp	.LBB7_29
	.p2align	4, 0x90
.LBB7_35:                               #   in Loop: Header=BB7_30 Depth=2
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB7_29:                               # %.loopexit.i
                                        #   in Loop: Header=BB7_30 Depth=2
	cmpl	%r13d, %r14d
	jb	.LBB7_30
.LBB7_38:                               # %alloc_sarray.exit
                                        #   in Loop: Header=BB7_21 Depth=1
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%r12, (%rbp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	152(%rax), %eax
	movl	%eax, 24(%rbp)
	movl	$0, 28(%rbp)
	movl	$0, 32(%rbp)
	movl	$0, 40(%rbp)
	movq	64(%rsp), %rdi          # 8-byte Reload
.LBB7_39:                               #   in Loop: Header=BB7_21 Depth=1
	movq	48(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB7_21
.LBB7_16:                               # %.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	136(%rax), %rbp
	testq	%rbp, %rbp
	jne	.LBB7_18
	jmp	.LBB7_56
	.p2align	4, 0x90
.LBB7_54:                               # %alloc_barray.exit
                                        #   in Loop: Header=BB7_18 Depth=1
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%r12, (%rbp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	152(%rax), %eax
	movl	%eax, 24(%rbp)
	movl	$0, 28(%rbp)
	movl	$0, 32(%rbp)
	movl	$0, 40(%rbp)
	movq	64(%rsp), %rdi          # 8-byte Reload
.LBB7_55:                               #   in Loop: Header=BB7_18 Depth=1
	movq	48(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB7_56
.LBB7_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_46 Depth 2
                                        #       Child Loop BB7_53 Depth 3
	cmpq	$0, (%rbp)
	jne	.LBB7_55
# BB#19:                                #   in Loop: Header=BB7_18 Depth=1
	movl	8(%rbp), %r13d
	movl	16(%rbp), %ecx
	leaq	-1(%r13), %rax
	cqto
	idivq	%rcx
	cmpq	%rdi, %rax
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	jge	.LBB7_40
# BB#20:                                #   in Loop: Header=BB7_18 Depth=1
	movl	%r13d, 20(%rbp)
	leaq	12(%rbp), %rbx
	jmp	.LBB7_41
.LBB7_40:                               #   in Loop: Header=BB7_18 Depth=1
	imull	%edi, %ecx
	movl	%ecx, 20(%rbp)
	leaq	56(%rbp), %rsi
	leaq	12(%rbp), %rbx
	movl	12(%rbp), %edx
	imulq	%r13, %rdx
	shlq	$7, %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	jpeg_open_backing_store
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	$1, 44(%rbp)
	movl	20(%rbp), %r13d
.LBB7_41:                               #   in Loop: Header=BB7_18 Depth=1
	movl	(%rbx), %ecx
	movq	%rcx, %rbp
	shlq	$7, %rbp
	movl	$999999976, %eax        # imm = 0x3B9AC9E8
	xorl	%edx, %edx
	divq	%rbp
	cmpq	$7812500, %rcx          # imm = 0x773594
	movq	8(%rsi), %rbx
	jb	.LBB7_43
# BB#42:                                #   in Loop: Header=BB7_18 Depth=1
	movq	(%rsi), %rcx
	movl	$69, 40(%rcx)
	movq	%rax, %r14
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*(%rcx)
	movq	%r14, %rax
.LBB7_43:                               #   in Loop: Header=BB7_18 Depth=1
	movl	%r13d, %edx
	cmpq	%rdx, %rax
	cmovael	%r13d, %eax
	movl	%eax, 152(%rbx)
	shlq	$3, %rdx
	movl	$1, %esi
	movq	%rax, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	alloc_small
	movq	%rbx, %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rax, %r12
	testl	%r13d, %r13d
	je	.LBB7_54
# BB#44:                                # %.lr.ph57.i114.preheader
                                        #   in Loop: Header=BB7_18 Depth=1
	movl	%r13d, %eax
	notl	%eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%r13, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_46:                               # %.lr.ph57.i114
                                        #   Parent Loop BB7_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_53 Depth 3
	movl	%ecx, %r15d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	subl	%r14d, %r13d
	cmpl	%r13d, %r15d
	cmovbl	%r15d, %r13d
	movq	%r13, 56(%rsp)          # 8-byte Spill
	movq	%r13, %rbx
	imulq	%rbp, %rbx
	movq	8(%rsi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmpq	$999999977, %rbx        # imm = 0x3B9AC9E9
	jb	.LBB7_48
# BB#47:                                #   in Loop: Header=BB7_46 Depth=2
	movq	(%rsi), %rax
	movl	$53, 40(%rax)
	movl	$3, 44(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	*(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB7_48:                               #   in Loop: Header=BB7_46 Depth=2
	movq	%rbx, %r13
	orq	$24, %r13
	movq	%rsi, %rdi
	movq	%r13, %rsi
	callq	jpeg_get_large
	movq	%rbx, %rdx
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB7_50
# BB#49:                                #   in Loop: Header=BB7_46 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rax
	movl	$53, 40(%rax)
	movl	$4, 44(%rax)
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	callq	*(%rax)
	movq	16(%rsp), %rdx          # 8-byte Reload
.LBB7_50:                               # %alloc_large.exit119
                                        #   in Loop: Header=BB7_46 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	addq	%r13, 144(%rcx)
	movq	120(%rcx), %rax
	movq	%rax, (%rbx)
	movq	%rdx, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, 120(%rcx)
	movq	56(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	je	.LBB7_51
# BB#52:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB7_46 Depth=2
	addq	$24, %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r14), %eax
	notl	%r15d
	cmpl	%r15d, %eax
	cmoval	%eax, %r15d
	incl	%r15d
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_53:                               # %.lr.ph.i118
                                        #   Parent Loop BB7_18 Depth=1
                                        #     Parent Loop BB7_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%r14d, %eax
	incl	%r14d
	movq	%rbx, (%r12,%rax,8)
	addq	%rbp, %rbx
	incl	%r15d
	jne	.LBB7_53
	jmp	.LBB7_45
	.p2align	4, 0x90
.LBB7_51:                               #   in Loop: Header=BB7_46 Depth=2
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB7_45:                               # %.loopexit.i110
                                        #   in Loop: Header=BB7_46 Depth=2
	cmpl	%r13d, %r14d
	jb	.LBB7_46
	jmp	.LBB7_54
.LBB7_56:                               # %.loopexit
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	realize_virt_arrays, .Lfunc_end7-realize_virt_arrays
	.cfi_endproc

	.p2align	4, 0x90
	.type	access_virt_sarray,@function
access_virt_sarray:                     # @access_virt_sarray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi101:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi102:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 96
.Lcfi105:
	.cfi_offset %rbx, -56
.Lcfi106:
	.cfi_offset %r12, -48
.Lcfi107:
	.cfi_offset %r13, -40
.Lcfi108:
	.cfi_offset %r14, -32
.Lcfi109:
	.cfi_offset %r15, -24
.Lcfi110:
	.cfi_offset %rbp, -16
	movl	%r8d, 28(%rsp)          # 4-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %ebx
	movq	%rsi, %r15
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	leal	(%rcx,%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cmpl	8(%r15), %eax
	ja	.LBB8_3
# BB#1:
	cmpl	%ecx, 16(%r15)
	jb	.LBB8_3
# BB#2:
	cmpq	$0, (%r15)
	jne	.LBB8_4
.LBB8_3:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	$20, 40(%rax)
	callq	*(%rax)
.LBB8_4:
	movl	28(%r15), %eax
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	cmpl	%ebx, %eax
	ja	.LBB8_6
# BB#5:
	addl	20(%r15), %eax
	cmpl	%eax, 12(%rsp)          # 4-byte Folded Reload
	jbe	.LBB8_23
.LBB8_6:
	cmpl	$0, 44(%r15)
	jne	.LBB8_8
# BB#7:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	$68, 40(%rax)
	callq	*(%rax)
.LBB8_8:
	cmpl	$0, 40(%r15)
	je	.LBB8_15
# BB#9:
	movl	20(%r15), %eax
	testq	%rax, %rax
	je	.LBB8_14
# BB#10:                                # %.lr.ph.i
	movl	28(%r15), %r13d
	movl	8(%r15), %ebp
	movl	24(%r15), %ecx
	cmpq	%rax, %rcx
	cmovbq	%rcx, %rax
	movl	32(%r15), %ecx
	subq	%r13, %rcx
	cmpq	%rcx, %rax
	cmovleq	%rax, %rcx
	subq	%r13, %rbp
	cmpq	%rbp, %rcx
	cmovleq	%rcx, %rbp
	testq	%rbp, %rbp
	jle	.LBB8_14
# BB#11:                                # %.lr.ph92.preheader
	movl	12(%r15), %r14d
	leaq	56(%r15), %rbx
	imulq	%r14, %r13
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph92
                                        # =>This Inner Loop Header: Depth=1
	imulq	%r14, %rbp
	movq	(%r15), %rax
	movq	(%rax,%r12,8), %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	%rbp, %r8
	callq	*64(%r15)
	movl	20(%r15), %eax
	movl	24(%r15), %ecx
	addq	%rcx, %r12
	subq	%r12, %rax
	jle	.LBB8_14
# BB#13:                                # %..lr.ph.split_crit_edge.i
                                        #   in Loop: Header=BB8_12 Depth=1
	addq	%rbp, %r13
	cmpq	%rax, %rcx
	cmovleq	%rcx, %rax
	movl	8(%r15), %ebp
	movl	28(%r15), %ecx
	addq	%r12, %rcx
	movl	32(%r15), %edx
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	cmovleq	%rax, %rdx
	subq	%rcx, %rbp
	cmpq	%rbp, %rdx
	cmovleq	%rdx, %rbp
	testq	%rbp, %rbp
	jg	.LBB8_12
.LBB8_14:                               # %do_sarray_io.exit
	movl	$0, 40(%r15)
.LBB8_15:
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, 28(%r15)
	jae	.LBB8_17
# BB#16:                                # %._crit_edge
	movl	20(%r15), %ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	jmp	.LBB8_18
.LBB8_17:
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	20(%r15), %ecx
	xorl	%edx, %edx
	subq	%rcx, %rax
	cmovlel	%edx, %eax
.LBB8_18:
	movl	%eax, 28(%r15)
	testl	%ecx, %ecx
	je	.LBB8_23
# BB#19:                                # %.lr.ph.i75
	movl	%ecx, %ecx
	movl	%eax, %r12d
	movl	8(%r15), %ebx
	movl	24(%r15), %eax
	cmpq	%rcx, %rax
	cmovbq	%rax, %rcx
	movl	32(%r15), %eax
	subq	%r12, %rax
	cmpq	%rax, %rcx
	cmovleq	%rcx, %rax
	subq	%r12, %rbx
	cmpq	%rbx, %rax
	cmovleq	%rax, %rbx
	testq	%rbx, %rbx
	jle	.LBB8_23
# BB#20:                                # %.lr.ph86.preheader
	movl	12(%r15), %r14d
	leaq	56(%r15), %r13
	imulq	%r14, %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_21:                               # %.lr.ph86
                                        # =>This Inner Loop Header: Depth=1
	imulq	%r14, %rbx
	movq	(%r15), %rax
	movq	(%rax,%rbp,8), %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movq	%r12, %rcx
	movq	%rbx, %r8
	callq	*56(%r15)
	movl	20(%r15), %eax
	movl	24(%r15), %ecx
	addq	%rcx, %rbp
	subq	%rbp, %rax
	jle	.LBB8_23
# BB#22:                                # %..lr.ph.split.us_crit_edge.i
                                        #   in Loop: Header=BB8_21 Depth=1
	addq	%rbx, %r12
	cmpq	%rax, %rcx
	cmovleq	%rcx, %rax
	movl	8(%r15), %ebx
	movl	28(%r15), %ecx
	addq	%rbp, %rcx
	movl	32(%r15), %edx
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	cmovleq	%rax, %rdx
	subq	%rcx, %rbx
	cmpq	%rbx, %rdx
	cmovleq	%rdx, %rbx
	testq	%rbx, %rbx
	jg	.LBB8_21
.LBB8_23:                               # %do_sarray_io.exit76
	movl	32(%r15), %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
	cmpl	%ebx, %eax
	movq	32(%rsp), %r12          # 8-byte Reload
	movl	28(%rsp), %r13d         # 4-byte Reload
	jae	.LBB8_37
# BB#24:
	cmpl	%r12d, %eax
	jae	.LBB8_27
# BB#25:
	testl	%r13d, %r13d
	je	.LBB8_26
# BB#29:                                # %.thread
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	$20, 40(%rax)
	callq	*(%rax)
	movl	%r12d, %eax
	jmp	.LBB8_30
.LBB8_27:
	testl	%r13d, %r13d
	je	.LBB8_28
.LBB8_30:
	movl	%ebx, 32(%r15)
	movb	$1, %cl
	cmpl	$0, 36(%r15)
	jne	.LBB8_32
	jmp	.LBB8_35
.LBB8_26:
	xorl	%ecx, %ecx
	movl	%r12d, %eax
	cmpl	$0, 36(%r15)
	jne	.LBB8_32
	jmp	.LBB8_35
.LBB8_28:
	xorl	%ecx, %ecx
	cmpl	$0, 36(%r15)
	je	.LBB8_35
.LBB8_32:
	movl	28(%r15), %ecx
	subl	%ecx, %eax
	subl	%ecx, %ebx
	cmpl	%ebx, %eax
	jae	.LBB8_37
# BB#33:                                # %.lr.ph
	movl	12(%r15), %r14d
	movl	%eax, %ebp
	movl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_34:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	%r14, %rsi
	callq	jzero_far
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB8_34
	jmp	.LBB8_37
.LBB8_35:
	testb	%cl, %cl
	jne	.LBB8_37
# BB#36:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	$20, 40(%rax)
	callq	*(%rax)
.LBB8_37:                               # %.loopexit
	testl	%r13d, %r13d
	je	.LBB8_39
# BB#38:
	movl	$1, 40(%r15)
.LBB8_39:
	subl	28(%r15), %r12d
	shlq	$3, %r12
	addq	(%r15), %r12
	movq	%r12, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	access_virt_sarray, .Lfunc_end8-access_virt_sarray
	.cfi_endproc

	.p2align	4, 0x90
	.type	access_virt_barray,@function
access_virt_barray:                     # @access_virt_barray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi115:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi117:
	.cfi_def_cfa_offset 96
.Lcfi118:
	.cfi_offset %rbx, -56
.Lcfi119:
	.cfi_offset %r12, -48
.Lcfi120:
	.cfi_offset %r13, -40
.Lcfi121:
	.cfi_offset %r14, -32
.Lcfi122:
	.cfi_offset %r15, -24
.Lcfi123:
	.cfi_offset %rbp, -16
	movl	%r8d, 28(%rsp)          # 4-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %ebx
	movq	%rsi, %r15
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	leal	(%rcx,%rbx), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cmpl	8(%r15), %eax
	ja	.LBB9_3
# BB#1:
	cmpl	%ecx, 16(%r15)
	jb	.LBB9_3
# BB#2:
	cmpq	$0, (%r15)
	jne	.LBB9_4
.LBB9_3:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	$20, 40(%rax)
	callq	*(%rax)
.LBB9_4:
	movl	28(%r15), %eax
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	cmpl	%ebx, %eax
	ja	.LBB9_6
# BB#5:
	addl	20(%r15), %eax
	cmpl	%eax, 12(%rsp)          # 4-byte Folded Reload
	jbe	.LBB9_23
.LBB9_6:
	cmpl	$0, 44(%r15)
	jne	.LBB9_8
# BB#7:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	$68, 40(%rax)
	callq	*(%rax)
.LBB9_8:
	cmpl	$0, 40(%r15)
	je	.LBB9_15
# BB#9:
	movl	20(%r15), %eax
	testq	%rax, %rax
	je	.LBB9_14
# BB#10:                                # %.lr.ph.i
	movl	28(%r15), %r13d
	movl	8(%r15), %ebp
	movl	24(%r15), %ecx
	cmpq	%rax, %rcx
	cmovbq	%rcx, %rax
	movl	32(%r15), %ecx
	subq	%r13, %rcx
	cmpq	%rcx, %rax
	cmovleq	%rax, %rcx
	subq	%r13, %rbp
	cmpq	%rbp, %rcx
	cmovleq	%rcx, %rbp
	testq	%rbp, %rbp
	jle	.LBB9_14
# BB#11:                                # %.lr.ph92.preheader
	movl	12(%r15), %r14d
	shlq	$7, %r14
	leaq	56(%r15), %rbx
	imulq	%r14, %r13
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_12:                               # %.lr.ph92
                                        # =>This Inner Loop Header: Depth=1
	imulq	%r14, %rbp
	movq	(%r15), %rax
	movq	(%rax,%r12,8), %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	%rbp, %r8
	callq	*64(%r15)
	movl	20(%r15), %eax
	movl	24(%r15), %ecx
	addq	%rcx, %r12
	subq	%r12, %rax
	jle	.LBB9_14
# BB#13:                                # %..lr.ph.split_crit_edge.i
                                        #   in Loop: Header=BB9_12 Depth=1
	addq	%rbp, %r13
	cmpq	%rax, %rcx
	cmovleq	%rcx, %rax
	movl	8(%r15), %ebp
	movl	28(%r15), %ecx
	addq	%r12, %rcx
	movl	32(%r15), %edx
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	cmovleq	%rax, %rdx
	subq	%rcx, %rbp
	cmpq	%rbp, %rdx
	cmovleq	%rdx, %rbp
	testq	%rbp, %rbp
	jg	.LBB9_12
.LBB9_14:                               # %do_barray_io.exit
	movl	$0, 40(%r15)
.LBB9_15:
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, 28(%r15)
	jae	.LBB9_17
# BB#16:                                # %._crit_edge
	movl	20(%r15), %ecx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	jmp	.LBB9_18
.LBB9_17:
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	20(%r15), %ecx
	xorl	%edx, %edx
	subq	%rcx, %rax
	cmovlel	%edx, %eax
.LBB9_18:
	movl	%eax, 28(%r15)
	testl	%ecx, %ecx
	je	.LBB9_23
# BB#19:                                # %.lr.ph.i75
	movl	%ecx, %ecx
	movl	%eax, %r12d
	movl	8(%r15), %ebx
	movl	24(%r15), %eax
	cmpq	%rcx, %rax
	cmovbq	%rax, %rcx
	movl	32(%r15), %eax
	subq	%r12, %rax
	cmpq	%rax, %rcx
	cmovleq	%rcx, %rax
	subq	%r12, %rbx
	cmpq	%rbx, %rax
	cmovleq	%rax, %rbx
	testq	%rbx, %rbx
	jle	.LBB9_23
# BB#20:                                # %.lr.ph86.preheader
	movl	12(%r15), %r14d
	shlq	$7, %r14
	leaq	56(%r15), %r13
	imulq	%r14, %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_21:                               # %.lr.ph86
                                        # =>This Inner Loop Header: Depth=1
	imulq	%r14, %rbx
	movq	(%r15), %rax
	movq	(%rax,%rbp,8), %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movq	%r12, %rcx
	movq	%rbx, %r8
	callq	*56(%r15)
	movl	20(%r15), %eax
	movl	24(%r15), %ecx
	addq	%rcx, %rbp
	subq	%rbp, %rax
	jle	.LBB9_23
# BB#22:                                # %..lr.ph.split.us_crit_edge.i
                                        #   in Loop: Header=BB9_21 Depth=1
	addq	%rbx, %r12
	cmpq	%rax, %rcx
	cmovleq	%rcx, %rax
	movl	8(%r15), %ebx
	movl	28(%r15), %ecx
	addq	%rbp, %rcx
	movl	32(%r15), %edx
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	cmovleq	%rax, %rdx
	subq	%rcx, %rbx
	cmpq	%rbx, %rdx
	cmovleq	%rdx, %rbx
	testq	%rbx, %rbx
	jg	.LBB9_21
.LBB9_23:                               # %do_barray_io.exit76
	movl	32(%r15), %eax
	movl	12(%rsp), %ebx          # 4-byte Reload
	cmpl	%ebx, %eax
	movq	32(%rsp), %r12          # 8-byte Reload
	movl	28(%rsp), %r13d         # 4-byte Reload
	jae	.LBB9_37
# BB#24:
	cmpl	%r12d, %eax
	jae	.LBB9_27
# BB#25:
	testl	%r13d, %r13d
	je	.LBB9_26
# BB#29:                                # %.thread
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	$20, 40(%rax)
	callq	*(%rax)
	movl	%r12d, %eax
	jmp	.LBB9_30
.LBB9_27:
	testl	%r13d, %r13d
	je	.LBB9_28
.LBB9_30:
	movl	%ebx, 32(%r15)
	movb	$1, %cl
	cmpl	$0, 36(%r15)
	jne	.LBB9_32
	jmp	.LBB9_35
.LBB9_26:
	xorl	%ecx, %ecx
	movl	%r12d, %eax
	cmpl	$0, 36(%r15)
	jne	.LBB9_32
	jmp	.LBB9_35
.LBB9_28:
	xorl	%ecx, %ecx
	cmpl	$0, 36(%r15)
	je	.LBB9_35
.LBB9_32:
	movl	28(%r15), %ecx
	subl	%ecx, %eax
	subl	%ecx, %ebx
	cmpl	%ebx, %eax
	jae	.LBB9_37
# BB#33:                                # %.lr.ph
	movl	12(%r15), %r14d
	shlq	$7, %r14
	movl	%eax, %ebp
	movl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_34:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	(%rax,%rbp,8), %rdi
	movq	%r14, %rsi
	callq	jzero_far
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB9_34
	jmp	.LBB9_37
.LBB9_35:
	testb	%cl, %cl
	jne	.LBB9_37
# BB#36:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	$20, 40(%rax)
	callq	*(%rax)
.LBB9_37:                               # %.loopexit
	testl	%r13d, %r13d
	je	.LBB9_39
# BB#38:
	movl	$1, 40(%r15)
.LBB9_39:
	subl	28(%r15), %r12d
	shlq	$3, %r12
	addq	(%r15), %r12
	movq	%r12, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	access_virt_barray, .Lfunc_end9-access_virt_barray
	.cfi_endproc

	.p2align	4, 0x90
	.type	free_pool,@function
free_pool:                              # @free_pool
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi127:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 48
.Lcfi129:
	.cfi_offset %rbx, -48
.Lcfi130:
	.cfi_offset %r12, -40
.Lcfi131:
	.cfi_offset %r14, -32
.Lcfi132:
	.cfi_offset %r15, -24
.Lcfi133:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r12
	movq	8(%r12), %r14
	cmpl	$2, %r15d
	jb	.LBB10_2
# BB#1:                                 # %.thread
	movq	(%r12), %rax
	movl	$12, 40(%rax)
	movl	%r15d, 44(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	jmp	.LBB10_14
.LBB10_2:
	cmpl	$1, %r15d
	jne	.LBB10_14
# BB#3:
	movq	128(%r14), %rbp
	testq	%rbp, %rbp
	jne	.LBB10_5
	jmp	.LBB10_8
	.p2align	4, 0x90
.LBB10_7:                               #   in Loop: Header=BB10_5 Depth=1
	movq	48(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB10_8
.LBB10_5:                               # %.lr.ph89
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 44(%rbp)
	je	.LBB10_7
# BB#6:                                 #   in Loop: Header=BB10_5 Depth=1
	movl	$0, 44(%rbp)
	leaq	56(%rbp), %rsi
	movq	%r12, %rdi
	callq	*72(%rbp)
	jmp	.LBB10_7
.LBB10_8:                               # %._crit_edge90
	movq	$0, 128(%r14)
	movq	136(%r14), %rbp
	testq	%rbp, %rbp
	jne	.LBB10_10
	jmp	.LBB10_13
	.p2align	4, 0x90
.LBB10_12:                              #   in Loop: Header=BB10_10 Depth=1
	movq	48(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB10_13
.LBB10_10:                              # %.lr.ph84
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, 44(%rbp)
	je	.LBB10_12
# BB#11:                                #   in Loop: Header=BB10_10 Depth=1
	movl	$0, 44(%rbp)
	leaq	56(%rbp), %rsi
	movq	%r12, %rdi
	callq	*72(%rbp)
	jmp	.LBB10_12
.LBB10_13:                              # %._crit_edge85
	movq	$0, 136(%r14)
.LBB10_14:
	movslq	%r15d, %r15
	movq	112(%r14,%r15,8), %rsi
	movq	$0, 112(%r14,%r15,8)
	testq	%rsi, %rsi
	je	.LBB10_16
	.p2align	4, 0x90
.LBB10_15:                              # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rbx
	movq	8(%rsi), %rax
	movq	16(%rsi), %rcx
	leaq	24(%rax,%rcx), %rbp
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	jpeg_free_large
	subq	%rbp, 144(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rsi
	jne	.LBB10_15
.LBB10_16:                              # %._crit_edge80
	movq	96(%r14,%r15,8), %rsi
	movq	$0, 96(%r14,%r15,8)
	testq	%rsi, %rsi
	je	.LBB10_18
	.p2align	4, 0x90
.LBB10_17:                              # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rbx
	movq	8(%rsi), %rax
	movq	16(%rsi), %rcx
	leaq	24(%rax,%rcx), %rbp
	movq	%r12, %rdi
	movq	%rbp, %rdx
	callq	jpeg_free_small
	subq	%rbp, 144(%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rsi
	jne	.LBB10_17
.LBB10_18:                              # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	free_pool, .Lfunc_end10-free_pool
	.cfi_endproc

	.p2align	4, 0x90
	.type	self_destruct,@function
self_destruct:                          # @self_destruct
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 16
.Lcfi135:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	free_pool
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	free_pool
	movq	8(%rbx), %rsi
	movl	$160, %edx
	movq	%rbx, %rdi
	callq	jpeg_free_small
	movq	$0, 8(%rbx)
	movq	%rbx, %rdi
	popq	%rbx
	jmp	jpeg_mem_term           # TAILCALL
.Lfunc_end11:
	.size	self_destruct, .Lfunc_end11-self_destruct
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"JPEGMEM"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%ld%c"
	.size	.L.str.1, 6

	.type	first_pool_slop,@object # @first_pool_slop
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
first_pool_slop:
	.quad	1600                    # 0x640
	.quad	16000                   # 0x3e80
	.size	first_pool_slop, 16

	.type	extra_pool_slop,@object # @extra_pool_slop
	.p2align	4
extra_pool_slop:
	.quad	0                       # 0x0
	.quad	5000                    # 0x1388
	.size	extra_pool_slop, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
