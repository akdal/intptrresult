	.text
	.file	"XzIn.bc"
	.globl	Xz_ReadHeader
	.p2align	4, 0x90
	.type	Xz_ReadHeader,@function
Xz_ReadHeader:                          # @Xz_ReadHeader
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	4(%rsp), %rax
	movl	$12, %edx
	movl	$17, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	SeqInStream_Read2
	testl	%eax, %eax
	jne	.LBB0_3
# BB#1:
	leaq	4(%rsp), %rdi
	movl	$XZ_SIG, %esi
	movl	$6, %edx
	callq	memcmp
	testl	%eax, %eax
	movl	$17, %eax
	jne	.LBB0_3
# BB#2:
	leaq	4(%rsp), %rsi
	movq	%rbx, %rdi
	callq	Xz_ParseHeader
.LBB0_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	Xz_ReadHeader, .Lfunc_end0-Xz_ReadHeader
	.cfi_endproc

	.globl	XzBlock_ReadHeader
	.p2align	4, 0x90
	.type	XzBlock_ReadHeader,@function
XzBlock_ReadHeader:                     # @XzBlock_ReadHeader
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 40
	subq	$1032, %rsp             # imm = 0x408
.Lcfi7:
	.cfi_def_cfa_offset 1072
.Lcfi8:
	.cfi_offset %rbx, -40
.Lcfi9:
	.cfi_offset %r12, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$0, (%rbx)
	movq	%rsp, %rsi
	movq	%r15, %rdi
	callq	SeqInStream_ReadByte
	testl	%eax, %eax
	jne	.LBB1_3
# BB#1:
	movzbl	(%rsp), %eax
	leal	4(,%rax,4), %ecx
	movl	$0, (%r12)
	movl	%ecx, (%rbx)
	leaq	1(%rsp), %rsi
	leal	3(,%rax,4), %edx
	movq	%r15, %rdi
	callq	SeqInStream_Read
	testl	%eax, %eax
	jne	.LBB1_3
# BB#2:
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	XzBlock_Parse
.LBB1_3:
	addq	$1032, %rsp             # imm = 0x408
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	XzBlock_ReadHeader, .Lfunc_end1-XzBlock_ReadHeader
	.cfi_endproc

	.globl	Xz_GetUnpackSize
	.p2align	4, 0x90
	.type	Xz_GetUnpackSize,@function
Xz_GetUnpackSize:                       # @Xz_GetUnpackSize
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB2_1
# BB#2:                                 # %.lr.ph
	movq	24(%rdi), %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	addq	(%rdx), %rax
	jb	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_4 Depth=1
	incq	%rsi
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	jb	.LBB2_4
	jmp	.LBB2_6
.LBB2_1:
	xorl	%eax, %eax
	retq
.LBB2_5:
	movq	$-1, %rax
.LBB2_6:                                # %._crit_edge
	retq
.Lfunc_end2:
	.size	Xz_GetUnpackSize, .Lfunc_end2-Xz_GetUnpackSize
	.cfi_endproc

	.globl	Xz_GetPackSize
	.p2align	4, 0x90
	.type	Xz_GetPackSize,@function
Xz_GetPackSize:                         # @Xz_GetPackSize
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB3_1
# BB#2:                                 # %.lr.ph
	movq	24(%rdi), %rdx
	addq	$8, %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdi
	movq	(%rdx), %rax
	addq	$3, %rax
	andq	$-4, %rax
	addq	%rdi, %rax
	jb	.LBB3_5
# BB#3:                                 #   in Loop: Header=BB3_4 Depth=1
	incq	%rsi
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	jb	.LBB3_4
	jmp	.LBB3_6
.LBB3_1:
	xorl	%eax, %eax
	retq
.LBB3_5:
	movq	$-1, %rax
.LBB3_6:                                # %._crit_edge
	retq
.Lfunc_end3:
	.size	Xz_GetPackSize, .Lfunc_end3-Xz_GetPackSize
	.cfi_endproc

	.globl	Xzs_Construct
	.p2align	4, 0x90
	.type	Xzs_Construct,@function
Xzs_Construct:                          # @Xzs_Construct
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movq	$0, 16(%rdi)
	retq
.Lfunc_end4:
	.size	Xzs_Construct, .Lfunc_end4-Xzs_Construct
	.cfi_endproc

	.globl	Xzs_Free
	.p2align	4, 0x90
	.type	Xzs_Free,@function
Xzs_Free:                               # @Xzs_Free
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -40
.Lcfi18:
	.cfi_offset %r12, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	$0, (%r15)
	je	.LBB5_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdi
	addq	%r12, %rdi
	movq	%r14, %rsi
	callq	Xz_Free
	incq	%rbx
	addq	$40, %r12
	cmpq	(%r15), %rbx
	jb	.LBB5_2
.LBB5_3:                                # %._crit_edge
	movq	16(%r15), %rsi
	movq	%r14, %rdi
	callq	*8(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movq	$0, 16(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	Xzs_Free, .Lfunc_end5-Xzs_Free
	.cfi_endproc

	.globl	Xzs_GetNumBlocks
	.p2align	4, 0x90
	.type	Xzs_GetNumBlocks,@function
Xzs_GetNumBlocks:                       # @Xzs_GetNumBlocks
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.LBB6_1
# BB#2:                                 # %.lr.ph
	movq	16(%rdi), %r8
	leaq	-1(%rsi), %r9
	movq	%rsi, %rcx
	andq	$3, %rcx
	je	.LBB6_3
# BB#4:                                 # %.prol.preheader
	leaq	8(%r8), %rdi
	xorl	%edx, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_5:                                # =>This Inner Loop Header: Depth=1
	addq	(%rdi), %rax
	incq	%rdx
	addq	$40, %rdi
	cmpq	%rdx, %rcx
	jne	.LBB6_5
	jmp	.LBB6_6
.LBB6_1:
	xorl	%eax, %eax
	retq
.LBB6_3:
	xorl	%edx, %edx
	xorl	%eax, %eax
.LBB6_6:                                # %.prol.loopexit
	cmpq	$3, %r9
	jb	.LBB6_9
# BB#7:                                 # %.lr.ph.new
	leaq	(%rdx,%rdx,4), %rcx
	leaq	128(%r8,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB6_8:                                # =>This Inner Loop Header: Depth=1
	addq	-120(%rcx), %rax
	addq	-80(%rcx), %rax
	addq	-40(%rcx), %rax
	addq	(%rcx), %rax
	addq	$4, %rdx
	addq	$160, %rcx
	cmpq	%rsi, %rdx
	jb	.LBB6_8
.LBB6_9:                                # %._crit_edge
	retq
.Lfunc_end6:
	.size	Xzs_GetNumBlocks, .Lfunc_end6-Xzs_GetNumBlocks
	.cfi_endproc

	.globl	Xzs_GetUnpackSize
	.p2align	4, 0x90
	.type	Xzs_GetUnpackSize,@function
Xzs_GetUnpackSize:                      # @Xzs_GetUnpackSize
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.LBB7_1
# BB#2:                                 # %.lr.ph
	movq	16(%rdi), %r9
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_8 Depth 2
	leaq	(%r10,%r10,4), %rcx
	movq	8(%r9,%rcx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB7_5
# BB#6:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB7_4 Depth=1
	movq	24(%r9,%rcx,8), %rdx
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB7_8:                                #   Parent Loop BB7_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	(%rdx), %rcx
	jb	.LBB7_9
# BB#7:                                 #   in Loop: Header=BB7_8 Depth=2
	incq	%rsi
	addq	$16, %rdx
	cmpq	%rdi, %rsi
	jb	.LBB7_8
	jmp	.LBB7_10
	.p2align	4, 0x90
.LBB7_5:                                #   in Loop: Header=BB7_4 Depth=1
	xorl	%ecx, %ecx
	addq	%rcx, %rax
	jae	.LBB7_3
	jmp	.LBB7_11
	.p2align	4, 0x90
.LBB7_9:                                #   in Loop: Header=BB7_4 Depth=1
	movq	$-1, %rcx
.LBB7_10:                               # %Xz_GetUnpackSize.exit
                                        #   in Loop: Header=BB7_4 Depth=1
	addq	%rcx, %rax
	jb	.LBB7_11
.LBB7_3:                                #   in Loop: Header=BB7_4 Depth=1
	incq	%r10
	cmpq	%r8, %r10
	jb	.LBB7_4
	jmp	.LBB7_12
.LBB7_1:
	xorl	%eax, %eax
	retq
.LBB7_11:
	movq	$-1, %rax
.LBB7_12:                               # %Xz_GetUnpackSize.exit._crit_edge
	retq
.Lfunc_end7:
	.size	Xzs_GetUnpackSize, .Lfunc_end7-Xzs_GetUnpackSize
	.cfi_endproc

	.globl	Xzs_ReadBackward
	.p2align	4, 0x90
	.type	Xzs_ReadBackward,@function
Xzs_ReadBackward:                       # @Xzs_ReadBackward
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$1208, %rsp             # imm = 0x4B8
.Lcfi27:
	.cfi_def_cfa_offset 1264
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, %rbp
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	$0, 64(%rsp)
	leaq	64(%rsp), %rsi
	movl	$2, %edx
	movq	%r13, %rdi
	callq	*24(%r13)
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB8_80
# BB#1:
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rax
	movq	%rax, (%rbp)
	leaq	72(%rsp), %rdi
	callq	Xz_Construct
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	(%rbp), %rax
	cmpq	$12, %rax
	movl	$17, %r15d
	jl	.LBB8_27
# BB#2:
	movq	%r12, 152(%rsp)         # 8-byte Spill
	andl	$3, %eax
	testq	%rax, %rax
	jne	.LBB8_27
# BB#3:                                 # %.lr.ph.preheader
	leaq	22(%rsp), %rbp
	movq	%r13, 32(%rsp)          # 8-byte Spill
	jmp	.LBB8_4
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_4 Depth=1
	movl	$12, %edx
	movl	$17, %ecx
	movq	%r13, %rdi
	leaq	12(%rsp), %rsi
	callq	LookInStream_Read2
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB8_27
# BB#6:                                 #   in Loop: Header=BB8_4 Depth=1
	movzwl	(%rbp), %eax
	cmpw	XZ_FOOTER_SIG(%rip), %ax
	je	.LBB8_7
# BB#8:                                 #   in Loop: Header=BB8_4 Depth=1
	movq	(%r14), %rax
	leaq	12(%rax), %rbx
	movq	%rbx, (%r14)
	testq	%rax, %rax
	js	.LBB8_26
# BB#9:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB8_4 Depth=1
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph.i
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_13 Depth 3
	cmpq	$1025, %rbx             # imm = 0x401
	movl	$1024, %eax             # imm = 0x400
	cmovgeq	%rax, %rbx
	movq	%rbx, %rax
	negq	%rax
	movq	%rax, (%r14)
	movl	$1, %r12d
	movl	$1, %edx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	*24(%rbp)
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB8_22
# BB#11:                                #   in Loop: Header=BB8_10 Depth=2
	movl	$17, %ecx
	movq	%rbp, %rdi
	leaq	176(%rsp), %rsi
	movq	%rbx, %rdx
	callq	LookInStream_Read2
	movl	%eax, %r14d
	testl	%r14d, %r14d
	cmovnel	%r14d, %r15d
	jne	.LBB8_22
# BB#12:                                #   in Loop: Header=BB8_10 Depth=2
	movslq	%ebx, %rax
	movq	24(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_13:                               #   Parent Loop BB8_4 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testq	%rax, %rax
	jle	.LBB8_16
# BB#14:                                #   in Loop: Header=BB8_13 Depth=3
	leaq	-1(%rax), %rcx
	cmpb	$0, 175(%rsp,%rax)
	movq	%rcx, %rax
	je	.LBB8_13
# BB#15:                                # %.thread.loopexit.i
                                        #   in Loop: Header=BB8_10 Depth=2
	incl	%ecx
	movl	%ecx, %eax
	testb	$3, %al
	je	.LBB8_18
	jmp	.LBB8_26
.LBB8_16:                               #   in Loop: Header=BB8_10 Depth=2
	testl	%eax, %eax
	je	.LBB8_24
# BB#17:                                # %.thread.i
                                        #   in Loop: Header=BB8_10 Depth=2
	testb	$3, %al
	jne	.LBB8_26
.LBB8_18:                               #   in Loop: Header=BB8_10 Depth=2
	cltq
	addq	(%r14), %rax
	movq	%rax, (%r14)
	cmpq	$12, %rax
	jl	.LBB8_26
# BB#19:                                #   in Loop: Header=BB8_10 Depth=2
	addq	$-12, %rax
	movq	%rax, (%r14)
	xorl	%edx, %edx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	*24(%rbp)
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB8_22
# BB#20:                                #   in Loop: Header=BB8_10 Depth=2
	movl	$12, %edx
	movl	$17, %ecx
	movq	%rbp, %rdi
	leaq	12(%rsp), %rsi
	callq	LookInStream_Read2
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB8_22
# BB#21:                                #   in Loop: Header=BB8_10 Depth=2
	leaq	22(%rsp), %rax
	movzwl	(%rax), %eax
	xorl	%r12d, %r12d
	cmpw	XZ_FOOTER_SIG(%rip), %ax
	sete	%r12b
	movl	$17, %eax
	cmovnel	%eax, %r15d
	incl	%r12d
	movl	%r15d, %r14d
	.p2align	4, 0x90
.LBB8_22:                               #   in Loop: Header=BB8_10 Depth=2
	andb	$3, %r12b
	jne	.LBB8_28
# BB#23:                                #   in Loop: Header=BB8_10 Depth=2
	movl	%r14d, %r15d
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB8_24:                               # %.backedge.i
                                        #   in Loop: Header=BB8_10 Depth=2
	addq	%rbx, %r13
	cmpq	$65536, %r13            # imm = 0x10000
	jg	.LBB8_26
# BB#25:                                # %.backedge.i
                                        #   in Loop: Header=BB8_10 Depth=2
	movq	(%r14), %rbx
	cmpq	$12, %rbx
	jge	.LBB8_10
	jmp	.LBB8_26
.LBB8_7:                                #   in Loop: Header=BB8_4 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB8_30
.LBB8_28:                               #   in Loop: Header=BB8_4 Depth=1
	cmpb	$2, %r12b
	jne	.LBB8_71
# BB#29:                                # %.thread167.i.loopexit
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	32(%rsp), %r13          # 8-byte Reload
	leaq	22(%rsp), %rbp
.LBB8_30:                               # %.thread167.i
                                        #   in Loop: Header=BB8_4 Depth=1
	movl	20(%rsp), %eax
	shll	$8, %eax
	movzbl	21(%rsp), %ecx
	orl	%eax, %ecx
	movw	%cx, 72(%rsp)
	movl	$4, %r15d
	movzwl	%cx, %eax
	cmpl	$15, %eax
	ja	.LBB8_27
# BB#31:                                #   in Loop: Header=BB8_4 Depth=1
	movl	12(%rsp), %ebx
	movl	$6, %esi
	leaq	16(%rsp), %rdi
	callq	CrcCalc
	cmpl	%eax, %ebx
	jne	.LBB8_32
# BB#33:                                #   in Loop: Header=BB8_4 Depth=1
	movl	16(%rsp), %ebx
	leaq	(,%rbx,4), %rcx
	movq	$-16, %rax
	movq	%rcx, %r12
	subq	%rcx, %rax
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rax, (%rsi)
	movl	$1, %edx
	movq	%r13, %rdi
	callq	*24(%r13)
	testl	%eax, %eax
	cmovnel	%eax, %r14d
	jne	.LBB8_34
# BB#35:                                #   in Loop: Header=BB8_4 Depth=1
	leaq	4(,%rbx,4), %rbx
	movl	$2147483648, %eax       # imm = 0x80000000
	cmpq	%rax, %rbx
	ja	.LBB8_27
# BB#36:                                #   in Loop: Header=BB8_4 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	*(%rdi)
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB8_37
# BB#38:                                #   in Loop: Header=BB8_4 Depth=1
	movl	$4, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	LookInStream_Read2
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB8_57
# BB#39:                                #   in Loop: Header=BB8_4 Depth=1
	movl	$16, %r15d
	cmpq	$5, %rbx
	jb	.LBB8_57
# BB#40:                                #   in Loop: Header=BB8_4 Depth=1
	cmpb	$0, (%r13)
	jne	.LBB8_57
# BB#41:                                #   in Loop: Header=BB8_4 Depth=1
	movq	%r13, %rdi
	movq	%r12, %rbx
	movq	%rbx, %rsi
	callq	CrcCalc
	cmpl	(%r13,%rbx), %eax
	jne	.LBB8_57
# BB#42:                                #   in Loop: Header=BB8_4 Depth=1
	leaq	1(%r13), %rdi
	leaq	-1(%rbx), %rsi
	leaq	176(%rsp), %rdx
	callq	Xz_ReadVarInt
	testl	%eax, %eax
	je	.LBB8_57
# BB#43:                                #   in Loop: Header=BB8_4 Depth=1
	movq	176(%rsp), %rdx
	leaq	(%rdx,%rdx), %rcx
	cmpq	%rbx, %rcx
	ja	.LBB8_57
# BB#44:                                #   in Loop: Header=BB8_4 Depth=1
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	%eax, %ebp
	incq	%rbp
	leaq	72(%rsp), %rdi
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%rbx, %rsi
	callq	Xz_Free
	movq	112(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB8_53
# BB#45:                                #   in Loop: Header=BB8_4 Depth=1
	movq	%rax, 80(%rsp)
	movq	%rax, 88(%rsp)
	movq	%rax, %rsi
	shlq	$4, %rsi
	movq	%rbx, %rdi
	callq	*(%rbx)
	movq	%rax, %rbx
	movq	%rbx, 96(%rsp)
	testq	%rbx, %rbx
	je	.LBB8_46
# BB#47:                                # %.lr.ph.i.preheader.i.i
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	(%r13,%rbp), %rdi
	movq	%r12, %rsi
	subq	%rbp, %rsi
	leaq	8(%rbx), %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	callq	Xz_ReadVarInt
	testl	%eax, %eax
	je	.LBB8_56
# BB#48:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	%rbx, %rcx
	movl	%eax, %ebx
	addq	%rbp, %rbx
	movl	$24, %ebp
	xorl	%eax, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%r12, %rax
	movq	%r12, 136(%rsp)         # 8-byte Spill
.LBB8_51:                               # %.lr.ph.i.i
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r13,%rbx), %rdi
	movq	%rax, %rsi
	subq	%rbx, %rsi
	leaq	-24(%rcx,%rbp), %rdx
	callq	Xz_ReadVarInt
	testl	%eax, %eax
	je	.LBB8_56
# BB#52:                                #   in Loop: Header=BB8_51 Depth=2
	movq	%rbp, %rdx
	movq	128(%rsp), %rcx         # 8-byte Reload
	cmpq	$0, (%rcx)
	movq	112(%rsp), %rcx         # 8-byte Reload
	je	.LBB8_56
# BB#49:                                #   in Loop: Header=BB8_51 Depth=2
	movl	%eax, %ebp
	addq	%rbx, %rbp
	movq	120(%rsp), %rax         # 8-byte Reload
	incq	%rax
	cmpq	%rcx, %rax
	jae	.LBB8_53
# BB#50:                                # %..lr.ph_crit_edge.i.i.i
                                        #   in Loop: Header=BB8_51 Depth=2
	movq	96(%rsp), %rbx
	leaq	(%r13,%rbp), %rdi
	movq	%r12, %rcx
	movq	%rcx, %rsi
	subq	%rbp, %rsi
	movq	%rax, %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	shlq	$4, %rax
	leaq	8(%rbx,%rax), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	%rdx, %rax
	leaq	(%rbx,%rax), %rdx
	addq	$16, %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	callq	Xz_ReadVarInt
	movq	%rbx, %rcx
	movl	%eax, %ebx
	addq	%rbp, %rbx
	movq	144(%rsp), %rbp         # 8-byte Reload
	testl	%eax, %eax
	movq	136(%rsp), %rax         # 8-byte Reload
	jne	.LBB8_51
	jmp	.LBB8_56
.LBB8_71:                               # %Xz_ReadBackward.exit.loopexit
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	56(%rsp), %r15          # 8-byte Reload
	leaq	22(%rsp), %rbp
	jmp	.LBB8_72
.LBB8_53:                               # %.preheader.i.i.i
                                        #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$3, %bpl
	je	.LBB8_55
# BB#54:                                #   in Loop: Header=BB8_53 Depth=2
	cmpb	$0, (%r13,%rbp)
	leaq	1(%rbp), %rbp
	je	.LBB8_53
	jmp	.LBB8_56
.LBB8_46:                               #   in Loop: Header=BB8_4 Depth=1
	movl	$2, %r15d
	jmp	.LBB8_56
.LBB8_55:                               #   in Loop: Header=BB8_4 Depth=1
	xorl	%r15d, %r15d
	cmpq	%rbp, %r12
	setne	%r15b
	shll	$4, %r15d
.LBB8_56:                               # %Xz_ReadIndex.exit.i
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	22(%rsp), %rbp
.LBB8_57:                               # %Xz_ReadIndex.exit.i
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	*8(%rdi)
	testl	%r15d, %r15d
	cmovnel	%r15d, %r14d
	movq	32(%rsp), %r13          # 8-byte Reload
	jne	.LBB8_27
# BB#58:                                #   in Loop: Header=BB8_4 Depth=1
	movq	80(%rsp), %rax
	testq	%rax, %rax
	je	.LBB8_59
# BB#60:                                # %.lr.ph.i163.i
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	96(%rsp), %rcx
	addq	$8, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_62:                               #   Parent Loop BB8_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rdi
	movq	(%rcx), %rdx
	addq	$3, %rdx
	andq	$-4, %rdx
	addq	%rdi, %rdx
	jb	.LBB8_63
# BB#61:                                #   in Loop: Header=BB8_62 Depth=2
	incq	%rsi
	addq	$16, %rcx
	cmpq	%rax, %rsi
	jb	.LBB8_62
	jmp	.LBB8_64
.LBB8_59:                               #   in Loop: Header=BB8_4 Depth=1
	xorl	%edx, %edx
	jmp	.LBB8_64
.LBB8_63:                               #   in Loop: Header=BB8_4 Depth=1
	movq	$-1, %rdx
.LBB8_64:                               # %Xz_GetPackSize.exit.i
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	16(%r12,%rdx), %rax
	orq	%rax, %rdx
	movl	$16, %r15d
	js	.LBB8_27
# BB#65:                                #   in Loop: Header=BB8_4 Depth=1
	negq	%rax
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rax, (%rsi)
	movl	$1, %edx
	movq	%r13, %rdi
	callq	*24(%r13)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB8_27
# BB#66:                                # %.thread173.i
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	176(%rsp), %rbx
	movq	%rbx, %rdi
	callq	SecToRead_CreateVTable
	movq	%r13, 184(%rsp)
	movl	$12, %edx
	movl	$17, %ecx
	movq	%rbx, %rdi
	leaq	164(%rsp), %rbx
	movq	%rbx, %rsi
	callq	SeqInStream_Read2
	testl	%eax, %eax
	jne	.LBB8_68
# BB#67:                                #   in Loop: Header=BB8_4 Depth=1
	movl	$XZ_SIG, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	memcmp
	movl	%eax, %ecx
	movl	$17, %eax
	testl	%ecx, %ecx
	je	.LBB8_69
.LBB8_68:                               # %Xz_ReadHeader.exit.thread.i
                                        #   in Loop: Header=BB8_4 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r14d
	movq	56(%rsp), %r15          # 8-byte Reload
.LBB8_72:                               # %Xz_ReadBackward.exit
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 104(%rsp)
	testl	%r14d, %r14d
	jne	.LBB8_80
# BB#73:                                #   in Loop: Header=BB8_4 Depth=1
	movq	(%r15), %rbx
	cmpq	8(%r15), %rbx
	jne	.LBB8_74
# BB#75:                                #   in Loop: Header=BB8_4 Depth=1
	movq	%rbp, %r12
	movq	%rbx, %rbp
	shrq	$2, %rbp
	leaq	(%rbx,%rbp), %rax
	leaq	8(,%rax,8), %rax
	leaq	(%rax,%rax,4), %rsi
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	callq	*(%r13)
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB8_76
# BB#77:                                # %.thread
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	1(%rbx,%rbp), %rax
	movq	%rax, 8(%r15)
	movq	(%r15), %rax
	movq	16(%r15), %rsi
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	movq	%r14, %rdi
	callq	memcpy
	movq	16(%r15), %rsi
	movq	%r13, %rdi
	callq	*8(%r13)
	movq	%r14, 16(%r15)
	movq	(%r15), %rbx
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%r12, %rbp
	jmp	.LBB8_78
.LBB8_74:                               # %._crit_edge
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	16(%r15), %r14
.LBB8_78:                               #   in Loop: Header=BB8_4 Depth=1
	leaq	1(%rbx), %rax
	movq	%rax, (%r15)
	leaq	(%rbx,%rbx,4), %rax
	movq	104(%rsp), %rcx
	movq	%rcx, 32(%r14,%rax,8)
	movups	72(%rsp), %xmm0
	movups	88(%rsp), %xmm1
	movups	%xmm1, 16(%r14,%rax,8)
	movups	%xmm0, (%r14,%rax,8)
	movq	24(%rsp), %rsi          # 8-byte Reload
	cmpq	$0, (%rsi)
	je	.LBB8_79
# BB#81:                                #   in Loop: Header=BB8_4 Depth=1
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	*24(%r13)
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB8_80
# BB#82:                                #   in Loop: Header=BB8_4 Depth=1
	movq	152(%rsp), %rdi         # 8-byte Reload
	testq	%rdi, %rdi
	movq	24(%rsp), %rbx          # 8-byte Reload
	je	.LBB8_85
# BB#83:                                #   in Loop: Header=BB8_4 Depth=1
	movq	64(%rsp), %rsi
	subq	(%rbx), %rsi
	movq	$-1, %rdx
	callq	*(%rdi)
	testl	%eax, %eax
	jne	.LBB8_84
.LBB8_85:                               # %.thread70
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	72(%rsp), %rdi
	callq	Xz_Construct
	movq	(%rbx), %rax
	cmpq	$12, %rax
	movl	$17, %r15d
	jl	.LBB8_27
# BB#86:                                # %.thread70
                                        #   in Loop: Header=BB8_4 Depth=1
	andl	$3, %eax
	testq	%rax, %rax
	je	.LBB8_4
	jmp	.LBB8_27
.LBB8_69:                               # %Xz_ReadHeader.exit.i
                                        #   in Loop: Header=BB8_4 Depth=1
	leaq	46(%rsp), %rdi
	movq	%rbx, %rsi
	callq	Xz_ParseHeader
	movl	%eax, %r14d
	testl	%r14d, %r14d
	movq	56(%rsp), %r15          # 8-byte Reload
	jne	.LBB8_72
# BB#70:                                #   in Loop: Header=BB8_4 Depth=1
	movzwl	72(%rsp), %eax
	xorl	%r14d, %r14d
	cmpw	46(%rsp), %ax
	setne	%r14b
	shll	$4, %r14d
	jmp	.LBB8_72
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_10 Depth 2
                                        #       Child Loop BB8_13 Depth 3
                                        #     Child Loop BB8_51 Depth 2
                                        #     Child Loop BB8_53 Depth 2
                                        #     Child Loop BB8_62 Depth 2
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	$-12, (%r14)
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	*24(%r13)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB8_5
	jmp	.LBB8_27
.LBB8_26:                               # %.thread169.i
	movl	$17, %r15d
.LBB8_27:                               # %Xz_ReadBackward.exit.thread
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, 104(%rsp)
	movl	%r15d, %r14d
.LBB8_80:
	movl	%r14d, %eax
	addq	$1208, %rsp             # imm = 0x4B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_79:                               # %.thread71
	xorl	%r14d, %r14d
	jmp	.LBB8_80
.LBB8_32:
	movl	$16, %r15d
	jmp	.LBB8_27
.LBB8_34:
	movl	%eax, %r15d
	jmp	.LBB8_27
.LBB8_37:
	movl	$2, %r15d
	jmp	.LBB8_27
.LBB8_84:
	movl	$10, %r14d
	jmp	.LBB8_80
.LBB8_76:
	movl	$2, %r14d
	jmp	.LBB8_80
.Lfunc_end8:
	.size	Xzs_ReadBackward, .Lfunc_end8-Xzs_ReadBackward
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
