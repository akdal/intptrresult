	.text
	.file	"btGeneric6DofConstraint.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1133903872              # float 300
	.long	1065353216              # float 1
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
.LCPI0_1:
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.long	0                       # float 0
	.long	1036831949              # float 0.100000001
.LCPI0_2:
	.long	1065353216              # float 1
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.long	0                       # float 0
	.text
	.globl	_ZN23btGeneric6DofConstraintC2Ev
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraintC2Ev,@function
_ZN23btGeneric6DofConstraintC2Ev:       # @_ZN23btGeneric6DofConstraintC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$6, %esi
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintType
	movq	$_ZTV23btGeneric6DofConstraint+16, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 760(%rbx)
	movups	%xmm0, 744(%rbx)
	movups	%xmm0, 728(%rbx)
	movl	$1060320051, 776(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 780(%rbx)  # imm = 0x3F800000
	movl	$1056964608, 784(%rbx)  # imm = 0x3F000000
	movb	$0, 788(%rbx)
	movl	$0, 792(%rbx)
	movl	$0, 808(%rbx)
	movb	$0, 789(%rbx)
	movl	$0, 796(%rbx)
	movl	$0, 812(%rbx)
	movb	$0, 790(%rbx)
	movl	$0, 800(%rbx)
	movl	$0, 816(%rbx)
	movl	$0, 920(%rbx)
	movl	$0, 876(%rbx)
	movl	$1036831949, 880(%rbx)  # imm = 0x3DCCCCCD
	movl	$1065353216, 868(%rbx)  # imm = 0x3F800000
	movl	$-1082130432, 872(%rbx) # imm = 0xBF800000
	movl	$0, 900(%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [3.000000e+02,1.000000e+00,5.000000e-01,5.000000e-01]
	movups	%xmm0, 884(%rbx)
	movl	$0, 916(%rbx)
	movl	$0, 908(%rbx)
	movb	$0, 904(%rbx)
	movl	$0, 976(%rbx)
	movl	$1133903872, 940(%rbx)  # imm = 0x43960000
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [1.000000e+00,-1.000000e+00,0.000000e+00,1.000000e-01]
	movups	%xmm0, 924(%rbx)
	movaps	.LCPI0_2(%rip), %xmm1   # xmm1 = [1.000000e+00,5.000000e-01,5.000000e-01,0.000000e+00]
	movups	%xmm1, 944(%rbx)
	movl	$0, 972(%rbx)
	movl	$0, 964(%rbx)
	movb	$0, 960(%rbx)
	movl	$0, 1032(%rbx)
	movl	$1133903872, 996(%rbx)  # imm = 0x43960000
	movups	%xmm0, 980(%rbx)
	movl	$1056964608, 1008(%rbx) # imm = 0x3F000000
	movl	$0, 1012(%rbx)
	movl	$1065353216, 1000(%rbx) # imm = 0x3F800000
	movl	$1056964608, 1004(%rbx) # imm = 0x3F000000
	movl	$0, 1028(%rbx)
	movl	$0, 1020(%rbx)
	movb	$0, 1016(%rbx)
	movb	$1, 1264(%rbx)
	movb	$0, 1265(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN23btGeneric6DofConstraintC2Ev, .Lfunc_end0-_ZN23btGeneric6DofConstraintC2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1133903872              # float 300
	.long	1065353216              # float 1
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
.LCPI1_1:
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.long	0                       # float 0
	.long	1036831949              # float 0.100000001
.LCPI1_2:
	.long	1065353216              # float 1
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.long	0                       # float 0
	.text
	.globl	_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b,@function
_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b: # @_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%r8, %r15
	movq	%rcx, %rbp
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	%rdi, %rbx
	movl	$6, %esi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	movq	$_ZTV23btGeneric6DofConstraint+16, (%rbx)
	movups	(%rbp), %xmm0
	movups	%xmm0, 96(%rbx)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 112(%rbx)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 128(%rbx)
	movups	48(%rbp), %xmm0
	movups	%xmm0, 144(%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 160(%rbx)
	movups	16(%r15), %xmm0
	movups	%xmm0, 176(%rbx)
	movups	32(%r15), %xmm0
	movups	%xmm0, 192(%rbx)
	movups	48(%r15), %xmm0
	movups	%xmm0, 208(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 760(%rbx)
	movups	%xmm0, 744(%rbx)
	movups	%xmm0, 728(%rbx)
	movl	$1060320051, 776(%rbx)  # imm = 0x3F333333
	movl	$1065353216, 780(%rbx)  # imm = 0x3F800000
	movl	$1056964608, 784(%rbx)  # imm = 0x3F000000
	movb	$0, 788(%rbx)
	movl	$0, 792(%rbx)
	movl	$0, 808(%rbx)
	movb	$0, 789(%rbx)
	movl	$0, 796(%rbx)
	movl	$0, 812(%rbx)
	movb	$0, 790(%rbx)
	movl	$0, 800(%rbx)
	movl	$0, 816(%rbx)
	movl	$0, 920(%rbx)
	movl	$0, 876(%rbx)
	movl	$1036831949, 880(%rbx)  # imm = 0x3DCCCCCD
	movl	$1065353216, 868(%rbx)  # imm = 0x3F800000
	movl	$-1082130432, 872(%rbx) # imm = 0xBF800000
	movl	$0, 900(%rbx)
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [3.000000e+02,1.000000e+00,5.000000e-01,5.000000e-01]
	movups	%xmm0, 884(%rbx)
	movl	$0, 916(%rbx)
	movl	$0, 908(%rbx)
	movb	$0, 904(%rbx)
	movl	$0, 976(%rbx)
	movl	$1133903872, 940(%rbx)  # imm = 0x43960000
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [1.000000e+00,-1.000000e+00,0.000000e+00,1.000000e-01]
	movups	%xmm0, 924(%rbx)
	movaps	.LCPI1_2(%rip), %xmm1   # xmm1 = [1.000000e+00,5.000000e-01,5.000000e-01,0.000000e+00]
	movups	%xmm1, 944(%rbx)
	movl	$0, 972(%rbx)
	movl	$0, 964(%rbx)
	movb	$0, 960(%rbx)
	movl	$0, 1032(%rbx)
	movl	$1133903872, 996(%rbx)  # imm = 0x43960000
	movups	%xmm0, 980(%rbx)
	movl	$1056964608, 1008(%rbx) # imm = 0x3F000000
	movl	$0, 1012(%rbx)
	movl	$1065353216, 1000(%rbx) # imm = 0x3F800000
	movl	$1056964608, 1004(%rbx) # imm = 0x3F000000
	movl	$0, 1028(%rbx)
	movl	$0, 1020(%rbx)
	movb	$0, 1016(%rbx)
	movb	%r14b, 1264(%rbx)
	movb	$0, 1265(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b, .Lfunc_end1-_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.cfi_endproc

	.globl	_Z15btGetMatrixElemRK11btMatrix3x3i
	.p2align	4, 0x90
	.type	_Z15btGetMatrixElemRK11btMatrix3x3i,@function
_Z15btGetMatrixElemRK11btMatrix3x3i:    # @_Z15btGetMatrixElemRK11btMatrix3x3i
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	imulq	$1431655766, %rax, %rcx # imm = 0x55555556
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,2), %edx
	subl	%edx, %eax
	cltq
	movslq	%ecx, %rcx
	shlq	$4, %rax
	addq	%rdi, %rax
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end2:
	.size	_Z15btGetMatrixElemRK11btMatrix3x3i, .Lfunc_end2-_Z15btGetMatrixElemRK11btMatrix3x3i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1065353216              # float 1
.LCPI3_1:
	.long	3212836864              # float -1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3
	.p2align	4, 0x90
	.type	_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3,@function
_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3: # @_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI3_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB3_4
# BB#1:
	ucomiss	.LCPI3_1(%rip), %xmm0
	jbe	.LBB3_3
# BB#2:
	movss	36(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	.LCPI3_2(%rip), %xmm0
	callq	atan2f
	movss	%xmm0, (%r14)
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	callq	asinf
	movss	%xmm0, 4(%r14)
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI3_2(%rip), %xmm0
	callq	atan2f
	movb	$1, %al
	jmp	.LBB3_6
.LBB3_4:
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	callq	atan2f
	movss	%xmm0, (%r14)
	movl	$1070141403, 4(%r14)    # imm = 0x3FC90FDB
	jmp	.LBB3_5
.LBB3_3:
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	callq	atan2f
	xorps	.LCPI3_2(%rip), %xmm0
	movss	%xmm0, (%r14)
	movl	$-1077342245, 4(%r14)   # imm = 0xBFC90FDB
.LBB3_5:
	xorps	%xmm0, %xmm0
	xorl	%eax, %eax
.LBB3_6:
	movss	%xmm0, 8(%r14)
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3, .Lfunc_end3-_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3
	.cfi_endproc

	.globl	_ZN22btRotationalLimitMotor14testLimitValueEf
	.p2align	4, 0x90
	.type	_ZN22btRotationalLimitMotor14testLimitValueEf,@function
_ZN22btRotationalLimitMotor14testLimitValueEf: # @_ZN22btRotationalLimitMotor14testLimitValueEf
	.cfi_startproc
# BB#0:
	movss	(%rdi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	ja	.LBB4_5
# BB#1:
	ucomiss	%xmm0, %xmm2
	jbe	.LBB4_3
# BB#2:
	movl	$1, 48(%rdi)
	subss	%xmm2, %xmm0
	movss	%xmm0, 40(%rdi)
	movl	$1, %eax
	retq
.LBB4_3:
	ucomiss	%xmm1, %xmm0
	jbe	.LBB4_5
# BB#4:
	movl	$2, 48(%rdi)
	subss	%xmm1, %xmm0
	movss	%xmm0, 40(%rdi)
	movl	$2, %eax
	retq
.LBB4_5:
	movl	$0, 48(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	_ZN22btRotationalLimitMotor14testLimitValueEf, .Lfunc_end4-_ZN22btRotationalLimitMotor14testLimitValueEf
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_1:
	.long	872415232               # float 1.1920929E-7
.LCPI5_2:
	.long	3019898880              # float -1.1920929E-7
.LCPI5_3:
	.long	1065353216              # float 1
.LCPI5_4:
	.long	3713928043              # float -9.99999984E+17
.LCPI5_5:
	.long	1566444395              # float 9.99999984E+17
.LCPI5_6:
	.long	2147483648              # float -0
	.text
	.globl	_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyR12btSolverBodyS3_S5_
	.p2align	4, 0x90
	.type	_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyR12btSolverBodyS3_S5_,@function
_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyR12btSolverBodyS3_S5_: # @_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyR12btSolverBodyS3_S5_
	.cfi_startproc
# BB#0:
	movaps	%xmm0, %xmm2
	cmpl	$0, 48(%rdi)
	je	.LBB5_1
# BB#4:                                 # %.thread
	movss	28(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	40(%rdi), %xmm3
	xorps	.LCPI5_0(%rip), %xmm3
	divss	%xmm2, %xmm3
	leaq	16(%rdi), %rax
	jmp	.LBB5_5
.LBB5_1:
	cmpb	$0, 36(%rdi)
	je	.LBB5_2
# BB#3:
	movss	8(%rdi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	leaq	12(%rdi), %rax
.LBB5_5:
	mulss	(%rax), %xmm2
	movq	72(%rcx), %rax
	xorps	%xmm5, %xmm5
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	xorps	%xmm4, %xmm4
	je	.LBB5_7
# BB#6:
	movsd	344(%rax), %xmm4        # xmm4 = mem[0],zero
	movsd	16(%rcx), %xmm0         # xmm0 = mem[0],zero
	addps	%xmm4, %xmm0
	movss	352(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	addss	24(%rcx), %xmm6
	xorps	%xmm4, %xmm4
	movss	%xmm6, %xmm4            # xmm4 = xmm6[0],xmm4[1,2,3]
.LBB5_7:                                # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit85
	movq	72(%r9), %rax
	testq	%rax, %rax
	xorps	%xmm6, %xmm6
	je	.LBB5_9
# BB#8:
	movsd	344(%rax), %xmm6        # xmm6 = mem[0],zero
	movsd	16(%r9), %xmm5          # xmm5 = mem[0],zero
	addps	%xmm6, %xmm5
	movss	352(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	addss	24(%r9), %xmm7
	xorps	%xmm6, %xmm6
	movss	%xmm7, %xmm6            # xmm6 = xmm7[0],xmm6[1,2,3]
.LBB5_9:                                # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit
	movaps	%xmm0, %xmm7
	subss	%xmm5, %xmm7
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	subss	%xmm5, %xmm0
	subss	%xmm6, %xmm4
	mulss	(%rsi), %xmm7
	mulss	4(%rsi), %xmm0
	addss	%xmm7, %xmm0
	mulss	8(%rsi), %xmm4
	addss	%xmm0, %xmm4
	mulss	20(%rdi), %xmm4
	subss	%xmm4, %xmm3
	mulss	24(%rdi), %xmm3
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	jbe	.LBB5_11
# BB#10:                                # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit
	xorps	%xmm0, %xmm0
	ucomiss	.LCPI5_2(%rip), %xmm3
	ja	.LBB5_15
.LBB5_11:
	movss	32(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	.LCPI5_3(%rip), %xmm0
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm3
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm3
	jbe	.LBB5_13
# BB#12:
	minss	%xmm3, %xmm2
	jmp	.LBB5_14
.LBB5_13:
	xorps	.LCPI5_0(%rip), %xmm2
	maxss	%xmm3, %xmm2
.LBB5_14:
	movss	52(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	movaps	%xmm2, %xmm3
	cmpltss	.LCPI5_4(%rip), %xmm3
	andnps	%xmm2, %xmm3
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	cmpltss	%xmm2, %xmm0
	andnps	%xmm3, %xmm0
	movss	%xmm0, 52(%rdi)
	subss	%xmm1, %xmm0
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	280(%rdx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	movss	284(%rdx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	addss	%xmm2, %xmm3
	movss	288(%rdx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	296(%rdx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	movss	300(%rdx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	addss	%xmm3, %xmm6
	movss	304(%rdx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm6, %xmm3
	mulss	312(%rdx), %xmm5
	mulss	316(%rdx), %xmm4
	addss	%xmm5, %xmm4
	mulss	320(%rdx), %xmm1
	addss	%xmm4, %xmm1
	xorps	%xmm4, %xmm4
	mulss	%xmm0, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%rcx)
	movss	4(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%rcx)
	addss	8(%rcx), %xmm4
	movss	%xmm4, 8(%rcx)
	movss	32(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	40(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	mulss	%xmm2, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm6
	addss	16(%rcx), %xmm4
	movss	%xmm4, 16(%rcx)
	addss	20(%rcx), %xmm5
	movss	%xmm5, 20(%rcx)
	addss	24(%rcx), %xmm6
	movss	%xmm6, 24(%rcx)
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	280(%r8), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	movss	284(%r8), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	addss	%xmm1, %xmm3
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	288(%r8), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	296(%r8), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	movss	300(%r8), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	addss	%xmm3, %xmm6
	movss	304(%r8), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm6, %xmm3
	mulss	312(%r8), %xmm5
	mulss	316(%r8), %xmm4
	addss	%xmm5, %xmm4
	mulss	320(%r8), %xmm1
	addss	%xmm4, %xmm1
	movss	.LCPI5_6(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	(%r9), %xmm5            # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%r9)
	movss	4(%r9), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%r9)
	addss	8(%r9), %xmm4
	movss	%xmm4, 8(%r9)
	movss	32(%r9), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%r9), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	40(%r9), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	mulss	%xmm2, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm6
	movss	16(%r9), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 16(%r9)
	movss	20(%r9), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	movss	%xmm1, 20(%r9)
	movss	24(%r9), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm1
	movss	%xmm1, 24(%r9)
.LBB5_15:                               # %_ZN22btRotationalLimitMotor16needApplyTorquesEv.exit
	retq
.LBB5_2:
	xorps	%xmm0, %xmm0
	retq
.Lfunc_end5:
	.size	_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyR12btSolverBodyS3_S5_, .Lfunc_end5-_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyR12btSolverBodyS3_S5_
	.cfi_endproc

	.globl	_ZN25btTranslationalLimitMotor14testLimitValueEif
	.p2align	4, 0x90
	.type	_ZN25btTranslationalLimitMotor14testLimitValueEif,@function
_ZN25btTranslationalLimitMotor14testLimitValueEif: # @_ZN25btTranslationalLimitMotor14testLimitValueEif
	.cfi_startproc
# BB#0:
	movslq	%esi, %rcx
	movss	(%rdi,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	16(%rdi,%rcx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	ja	.LBB6_5
# BB#1:
	ucomiss	%xmm0, %xmm2
	jbe	.LBB6_3
# BB#2:
	movl	$2, 128(%rdi,%rcx,4)
	subss	%xmm2, %xmm0
	movl	$2, %eax
	movss	%xmm0, 96(%rdi,%rcx,4)
	retq
.LBB6_3:
	ucomiss	%xmm1, %xmm0
	jbe	.LBB6_5
# BB#4:
	movl	$1, 128(%rdi,%rcx,4)
	subss	%xmm1, %xmm0
	movl	$1, %eax
	movss	%xmm0, 96(%rdi,%rcx,4)
	retq
.LBB6_5:
	movl	$0, 128(%rdi,%rcx,4)
	xorl	%eax, %eax
	xorps	%xmm0, %xmm0
	movss	%xmm0, 96(%rdi,%rcx,4)
	retq
.Lfunc_end6:
	.size	_ZN25btTranslationalLimitMotor14testLimitValueEif, .Lfunc_end6-_ZN25btTranslationalLimitMotor14testLimitValueEif
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_1:
	.long	3713928043              # float -9.99999984E+17
.LCPI7_2:
	.long	1566444395              # float 9.99999984E+17
	.text
	.globl	_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyR12btSolverBodyRK9btVector3S1_S3_S6_iS6_S6_
	.p2align	4, 0x90
	.type	_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyR12btSolverBodyRK9btVector3S1_S3_S6_iS6_S6_,@function
_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyR12btSolverBodyRK9btVector3S1_S3_S6_iS6_S6_: # @_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyR12btSolverBodyRK9btVector3S1_S3_S6_iS6_S6_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movss	%xmm1, -8(%rsp)         # 4-byte Spill
	movss	%xmm0, -4(%rsp)         # 4-byte Spill
	movq	40(%rsp), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm9
	subss	60(%rsi), %xmm9
	movss	8(%rax), %xmm8          # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm8    # xmm8 = xmm8[0],xmm0[0],xmm8[1],xmm0[1]
	movss	56(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movaps	%xmm8, %xmm15
	subps	%xmm1, %xmm15
	subss	60(%r8), %xmm10
	movss	56(%r8), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	64(%r8), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movq	72(%rdx), %rax
	xorps	%xmm2, %xmm2
	testq	%rax, %rax
	xorps	%xmm7, %xmm7
	xorps	%xmm6, %xmm6
	je	.LBB7_2
# BB#1:
	movsd	328(%rax), %xmm1        # xmm1 = mem[0],zero
	movsd	(%rdx), %xmm11          # xmm11 = mem[0],zero
	addps	%xmm1, %xmm11
	movss	336(%rax), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movss	344(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm12
	addss	16(%rdx), %xmm1
	movsd	348(%rax), %xmm3        # xmm3 = mem[0],zero
	movsd	20(%rdx), %xmm6         # xmm6 = mem[0],zero
	addps	%xmm3, %xmm6
	movaps	%xmm15, %xmm7
	mulps	%xmm6, %xmm7
	movaps	%xmm9, %xmm3
	unpcklps	%xmm15, %xmm3   # xmm3 = xmm3[0],xmm15[0],xmm3[1],xmm15[1]
	movaps	%xmm15, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm6, %xmm4
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm9, %xmm5
	mulss	%xmm1, %xmm5
	shufps	$0, %xmm6, %xmm1        # xmm1 = xmm1[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm1      # xmm1 = xmm1[2,0],xmm6[2,3]
	mulps	%xmm3, %xmm1
	subps	%xmm1, %xmm7
	subss	%xmm4, %xmm5
	addps	%xmm11, %xmm7
	addss	%xmm12, %xmm5
	xorps	%xmm6, %xmm6
	movss	%xmm5, %xmm6            # xmm6 = xmm5[0],xmm6[1,2,3]
.LBB7_2:                                # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit130
	movq	32(%rsp), %r11
	movl	24(%rsp), %r10d
	movq	16(%rsp), %rax
	subps	%xmm0, %xmm8
	movq	72(%r9), %rbx
	testq	%rbx, %rbx
	xorps	%xmm11, %xmm11
	je	.LBB7_4
# BB#3:
	movsd	328(%rbx), %xmm1        # xmm1 = mem[0],zero
	movsd	(%r9), %xmm11           # xmm11 = mem[0],zero
	addps	%xmm1, %xmm11
	movss	336(%rbx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movss	344(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	8(%r9), %xmm12
	addss	16(%r9), %xmm1
	movsd	348(%rbx), %xmm2        # xmm2 = mem[0],zero
	movsd	20(%r9), %xmm3          # xmm3 = mem[0],zero
	addps	%xmm2, %xmm3
	movaps	%xmm8, %xmm2
	mulps	%xmm3, %xmm2
	movaps	%xmm10, %xmm5
	unpcklps	%xmm8, %xmm5    # xmm5 = xmm5[0],xmm8[0],xmm5[1],xmm8[1]
	movaps	%xmm8, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm3, %xmm0
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm10, %xmm4
	mulss	%xmm1, %xmm4
	shufps	$0, %xmm3, %xmm1        # xmm1 = xmm1[0,0],xmm3[0,0]
	shufps	$226, %xmm3, %xmm1      # xmm1 = xmm1[2,0],xmm3[2,3]
	mulps	%xmm5, %xmm1
	subps	%xmm1, %xmm2
	subss	%xmm0, %xmm4
	addps	%xmm11, %xmm2
	addss	%xmm12, %xmm4
	xorps	%xmm11, %xmm11
	movss	%xmm4, %xmm11           # xmm11 = xmm4[0],xmm11[1,2,3]
.LBB7_4:                                # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit
	movss	(%r11), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%r11), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	8(%r11), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	(%rax), %xmm0
	subss	4(%rax), %xmm1
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	subss	8(%rax), %xmm5
	mulss	%xmm12, %xmm0
	mulss	%xmm14, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm13, %xmm5
	addss	%xmm1, %xmm5
	xorps	.LCPI7_0(%rip), %xmm5
	movslq	%r10d, %rcx
	movss	(%rdi,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movss	16(%rdi,%rcx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI7_1(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	.LCPI7_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm1
	jbe	.LBB7_9
# BB#5:
	ucomiss	%xmm1, %xmm5
	jbe	.LBB7_7
# BB#6:
	subss	%xmm1, %xmm5
	xorps	%xmm4, %xmm4
	jmp	.LBB7_9
.LBB7_7:
	xorps	%xmm0, %xmm0
	ucomiss	%xmm5, %xmm3
	jbe	.LBB7_10
# BB#8:
	subss	%xmm3, %xmm5
	xorps	%xmm0, %xmm0
.LBB7_9:
	movss	-8(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	subss	%xmm2, %xmm1
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	%xmm2, %xmm7
	subss	%xmm11, %xmm6
	mulss	%xmm1, %xmm12
	mulss	%xmm14, %xmm7
	addss	%xmm12, %xmm7
	mulss	%xmm13, %xmm6
	addss	%xmm7, %xmm6
	mulss	56(%rdi), %xmm5
	divss	-4(%rsp), %xmm5         # 4-byte Folded Reload
	mulss	52(%rdi), %xmm6
	subss	%xmm6, %xmm5
	mulss	48(%rdi), %xmm5
	mulss	%xmm3, %xmm5
	movss	32(%rdi,%rcx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm5
	movaps	%xmm5, %xmm2
	cmpltss	%xmm4, %xmm2
	andnps	%xmm5, %xmm2
	cmpltss	%xmm5, %xmm0
	andnps	%xmm2, %xmm0
	movss	%xmm0, 32(%rdi,%rcx,4)
	subss	%xmm1, %xmm0
	movss	(%r11), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%r11), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%r11), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm3
	mulss	%xmm5, %xmm3
	movaps	%xmm15, %xmm1
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm3
	movaps	%xmm15, %xmm4
	mulss	%xmm7, %xmm4
	shufps	$229, %xmm15, %xmm15    # xmm15 = xmm15[1,1,2,3]
	movaps	%xmm15, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm4
	mulss	%xmm6, %xmm15
	mulss	%xmm7, %xmm9
	subss	%xmm9, %xmm15
	movaps	%xmm10, %xmm9
	mulss	%xmm5, %xmm9
	movaps	%xmm8, %xmm1
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm9
	movaps	%xmm8, %xmm11
	mulss	%xmm7, %xmm11
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	movaps	%xmm8, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm11
	mulss	%xmm6, %xmm8
	mulss	%xmm7, %xmm10
	subss	%xmm10, %xmm8
	movss	360(%rsi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	mulss	%xmm1, %xmm6
	mulss	%xmm1, %xmm5
	movss	280(%rsi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	284(%rsi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%rsi), %xmm10       # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm10
	addss	%xmm2, %xmm10
	movss	296(%rsi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	movss	300(%rsi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	addss	%xmm2, %xmm1
	movss	304(%rsi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm2
	addss	%xmm1, %xmm2
	mulss	312(%rsi), %xmm3
	mulss	316(%rsi), %xmm4
	addss	%xmm3, %xmm4
	mulss	320(%rsi), %xmm15
	addss	%xmm4, %xmm15
	mulss	%xmm0, %xmm7
	addss	(%rdx), %xmm7
	movss	%xmm7, (%rdx)
	mulss	%xmm0, %xmm6
	addss	4(%rdx), %xmm6
	movss	%xmm6, 4(%rdx)
	mulss	%xmm0, %xmm5
	addss	8(%rdx), %xmm5
	movss	%xmm5, 8(%rdx)
	movss	32(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	mulss	%xmm10, %xmm1
	movss	36(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	%xmm2, %xmm3
	movss	40(%rdx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	%xmm15, %xmm2
	addss	16(%rdx), %xmm1
	movss	%xmm1, 16(%rdx)
	addss	20(%rdx), %xmm3
	movss	%xmm3, 20(%rdx)
	addss	24(%rdx), %xmm2
	movss	%xmm2, 24(%rdx)
	movss	280(%r8), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	284(%r8), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%r8), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	addss	%xmm2, %xmm3
	movss	296(%r8), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	300(%r8), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	addss	%xmm1, %xmm2
	movss	304(%r8), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	addss	%xmm2, %xmm1
	movss	360(%r8), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	312(%r8), %xmm9
	mulss	316(%r8), %xmm11
	addss	%xmm9, %xmm11
	movss	(%r11), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	mulss	320(%r8), %xmm8
	addss	%xmm11, %xmm8
	mulss	%xmm0, %xmm4
	movss	(%r9), %xmm5            # xmm5 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm5
	movss	4(%r11), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	mulss	8(%r11), %xmm2
	mulss	%xmm0, %xmm4
	movss	%xmm5, (%r9)
	movss	4(%r9), %xmm5           # xmm5 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm5
	mulss	%xmm0, %xmm2
	movss	%xmm5, 4(%r9)
	movss	8(%r9), %xmm4           # xmm4 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm4
	movss	%xmm4, 8(%r9)
	movss	32(%r9), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	%xmm3, %xmm2
	movss	36(%r9), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm3
	movss	40(%r9), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	mulss	%xmm8, %xmm1
	movss	16(%r9), %xmm4          # xmm4 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm4
	movss	%xmm4, 16(%r9)
	movss	20(%r9), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
	movss	%xmm2, 20(%r9)
	movss	24(%r9), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	%xmm1, %xmm2
	movss	%xmm2, 24(%r9)
.LBB7_10:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyR12btSolverBodyRK9btVector3S1_S3_S6_iS6_S6_, .Lfunc_end7-_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyR12btSolverBodyRK9btVector3S1_S3_S6_iS6_S6_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1065353216              # float 1
.LCPI8_1:
	.long	3212836864              # float -1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN23btGeneric6DofConstraint18calculateAngleInfoEv
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint18calculateAngleInfoEv,@function
_ZN23btGeneric6DofConstraint18calculateAngleInfoEv: # @_ZN23btGeneric6DofConstraint18calculateAngleInfoEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
	subq	$48, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movss	1060(%rbx), %xmm9       # xmm9 = mem[0],zero,zero,zero
	movss	1080(%rbx), %xmm12      # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm12, %xmm1
	movss	1064(%rbx), %xmm13      # xmm13 = mem[0],zero,zero,zero
	movss	1076(%rbx), %xmm10      # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	mulss	%xmm10, %xmm0
	subss	%xmm0, %xmm1
	movss	1072(%rbx), %xmm8       # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm5
	mulss	%xmm8, %xmm5
	movss	1056(%rbx), %xmm3       # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm0
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm5
	movaps	%xmm10, %xmm11
	mulss	%xmm3, %xmm11
	movaps	%xmm9, %xmm0
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm11
	movss	1040(%rbx), %xmm2       # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 4(%rsp)          # 4-byte Spill
	movss	1044(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm2, %xmm4
	movss	%xmm4, (%rsp)           # 4-byte Spill
	movss	1048(%rbx), %xmm14      # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm6
	mulss	%xmm14, %xmm6
	movaps	%xmm10, %xmm7
	mulss	%xmm14, %xmm7
	movaps	%xmm12, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, 16(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm15
	mulss	%xmm0, %xmm15
	movaps	%xmm9, %xmm2
	mulss	%xmm14, %xmm2
	movaps	%xmm8, %xmm4
	mulss	%xmm14, %xmm4
	mulss	%xmm3, %xmm14
	mulss	%xmm0, %xmm8
	mulss	%xmm0, %xmm3
	mulss	%xmm5, %xmm0
	addss	(%rsp), %xmm0           # 4-byte Folded Reload
	addss	%xmm0, %xmm6
	movss	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm6, %xmm0
	subss	16(%rsp), %xmm7         # 4-byte Folded Reload
	subss	%xmm2, %xmm15
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm7
	mulss	%xmm0, %xmm15
	mulss	%xmm0, %xmm5
	movss	4(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm12
	subss	%xmm4, %xmm12
	mulss	%xmm0, %xmm12
	mulss	%xmm2, %xmm13
	subss	%xmm13, %xmm14
	mulss	%xmm0, %xmm14
	mulss	%xmm0, %xmm11
	mulss	%xmm2, %xmm10
	subss	%xmm10, %xmm8
	mulss	%xmm0, %xmm8
	mulss	%xmm2, %xmm9
	subss	%xmm3, %xmm9
	mulss	%xmm0, %xmm9
	movss	1108(%rbx), %xmm4       # xmm4 = mem[0],zero,zero,zero
	movss	%xmm1, 4(%rsp)          # 4-byte Spill
	movaps	%xmm1, %xmm0
	mulss	%xmm4, %xmm0
	movss	1124(%rbx), %xmm10      # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm0, %xmm3
	movss	1140(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm4, (%rsp)           # 4-byte Spill
	movaps	%xmm4, %xmm3
	movss	%xmm5, 16(%rsp)         # 4-byte Spill
	mulss	%xmm5, %xmm3
	movaps	%xmm12, %xmm4
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movaps	%xmm14, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	movss	1104(%rbx), %xmm5       # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm11, %xmm4
	movss	1120(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm6
	mulss	%xmm8, %xmm6
	addss	%xmm4, %xmm6
	movss	1136(%rbx), %xmm13      # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm4
	mulss	%xmm9, %xmm4
	addss	%xmm6, %xmm4
	movss	.LCPI8_0(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm6
	jbe	.LBB8_4
# BB#1:
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	movss	%xmm9, 12(%rsp)         # 4-byte Spill
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm9
	movss	(%rsp), %xmm5           # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	ucomiss	.LCPI8_1(%rip), %xmm4
	jbe	.LBB8_3
# BB#2:
	mulss	%xmm9, %xmm6
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	addss	%xmm6, %xmm7
	movaps	%xmm13, %xmm1
	mulss	%xmm1, %xmm15
	addss	%xmm7, %xmm15
	movss	%xmm15, 4(%rsp)         # 4-byte Spill
	movss	16(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm13
	mulss	%xmm0, %xmm12
	addss	%xmm13, %xmm12
	mulss	%xmm1, %xmm14
	addss	%xmm12, %xmm14
	movaps	%xmm14, 16(%rsp)        # 16-byte Spill
	mulss	%xmm11, %xmm5
	mulss	%xmm8, %xmm10
	addss	%xmm5, %xmm10
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm1, %xmm0
	addss	%xmm10, %xmm0
	mulss	1112(%rbx), %xmm11
	mulss	1128(%rbx), %xmm8
	addss	%xmm11, %xmm8
	mulss	1144(%rbx), %xmm1
	addss	%xmm8, %xmm1
	xorps	.LCPI8_2(%rip), %xmm0
	movss	%xmm4, (%rsp)           # 4-byte Spill
	callq	atan2f
	movss	%xmm0, 1168(%rbx)
	movss	(%rsp), %xmm0           # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	asinf
	movss	%xmm0, 1172(%rbx)
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	xorps	.LCPI8_2(%rip), %xmm0
	movss	4(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	callq	atan2f
	jmp	.LBB8_6
.LBB8_4:
	movaps	%xmm2, %xmm0
	movaps	%xmm3, %xmm1
	callq	atan2f
	movss	%xmm0, 1168(%rbx)
	movl	$1070141403, 1172(%rbx) # imm = 0x3FC90FDB
	jmp	.LBB8_5
.LBB8_3:
	movaps	%xmm2, %xmm0
	movaps	%xmm3, %xmm1
	callq	atan2f
	xorps	.LCPI8_2(%rip), %xmm0
	movss	%xmm0, 1168(%rbx)
	movl	$-1077342245, 1172(%rbx) # imm = 0xBFC90FDB
.LBB8_5:                                # %_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3.exit
	xorps	%xmm0, %xmm0
.LBB8_6:                                # %_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3.exit
	movss	%xmm0, 1176(%rbx)
	movss	1104(%rbx), %xmm8       # xmm8 = mem[0],zero,zero,zero
	movss	1120(%rbx), %xmm10      # xmm10 = mem[0],zero,zero,zero
	movss	1136(%rbx), %xmm2       # xmm2 = mem[0],zero,zero,zero
	movss	1048(%rbx), %xmm13      # xmm13 = mem[0],zero,zero,zero
	movss	1064(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	1080(%rbx), %xmm5       # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	mulss	%xmm0, %xmm3
	movaps	%xmm10, %xmm4
	mulss	%xmm5, %xmm4
	subss	%xmm4, %xmm3
	movaps	%xmm8, %xmm4
	mulss	%xmm5, %xmm4
	movaps	%xmm2, %xmm6
	mulss	%xmm13, %xmm6
	subss	%xmm6, %xmm4
	movaps	%xmm8, %xmm9
	mulss	%xmm0, %xmm9
	movaps	%xmm3, %xmm11
	movaps	%xmm5, %xmm14
	movaps	%xmm0, %xmm12
	mulss	%xmm3, %xmm5
	mulss	%xmm3, %xmm0
	movaps	%xmm10, %xmm7
	movaps	%xmm2, %xmm15
	mulss	%xmm3, %xmm2
	mulss	%xmm10, %xmm3
	mulss	%xmm13, %xmm10
	subss	%xmm9, %xmm10
	unpcklps	%xmm4, %xmm11   # xmm11 = xmm11[0],xmm4[0],xmm11[1],xmm4[1]
	movlps	%xmm11, 1200(%rbx)
	xorps	%xmm1, %xmm1
	movss	%xmm10, %xmm1           # xmm1 = xmm10[0],xmm1[1,2,3]
	movlps	%xmm1, 1208(%rbx)
	mulss	%xmm4, %xmm14
	mulss	%xmm10, %xmm12
	subss	%xmm12, %xmm14
	movaps	%xmm13, %xmm1
	mulss	%xmm10, %xmm1
	subss	%xmm5, %xmm1
	xorps	%xmm5, %xmm5
	mulss	%xmm4, %xmm13
	subss	%xmm13, %xmm0
	movaps	%xmm14, %xmm6
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	movlps	%xmm6, 1184(%rbx)
	xorps	%xmm6, %xmm6
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	movlps	%xmm6, 1192(%rbx)
	mulss	%xmm10, %xmm7
	mulss	%xmm4, %xmm15
	subss	%xmm15, %xmm7
	mulss	%xmm8, %xmm10
	subss	%xmm10, %xmm2
	mulss	%xmm8, %xmm4
	subss	%xmm3, %xmm4
	unpcklps	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	movss	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1,2,3]
	movlps	%xmm7, 1216(%rbx)
	movlps	%xmm5, 1224(%rbx)
	mulss	%xmm14, %xmm14
	mulss	%xmm1, %xmm1
	addss	%xmm14, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_8
# BB#7:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB8_8:                                # %_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3.exit.split
	movss	.LCPI8_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	divss	%xmm1, %xmm0
	movss	1184(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 1184(%rbx)
	movss	1188(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 1188(%rbx)
	mulss	1192(%rbx), %xmm0
	movss	%xmm0, 1192(%rbx)
	movss	1200(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	1204(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	1208(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_10
# BB#9:                                 # %call.sqrt91
	callq	sqrtf
	movss	.LCPI8_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB8_10:                               # %_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3.exit.split.split
	movaps	%xmm2, %xmm0
	divss	%xmm1, %xmm0
	movss	1200(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 1200(%rbx)
	movss	1204(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 1204(%rbx)
	mulss	1208(%rbx), %xmm0
	movss	%xmm0, 1208(%rbx)
	movss	1216(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	1220(%rbx), %xmm1       # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	1224(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB8_12
# BB#11:                                # %call.sqrt92
	callq	sqrtf
	movss	.LCPI8_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB8_12:                               # %_Z16matrixToEulerXYZRK11btMatrix3x3R9btVector3.exit.split.split.split
	divss	%xmm1, %xmm2
	movss	1216(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 1216(%rbx)
	movss	1220(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 1220(%rbx)
	mulss	1224(%rbx), %xmm2
	movss	%xmm2, 1224(%rbx)
	addq	$48, %rsp
	popq	%rbx
	retq
.Lfunc_end8:
	.size	_ZN23btGeneric6DofConstraint18calculateAngleInfoEv, .Lfunc_end8-_ZN23btGeneric6DofConstraint18calculateAngleInfoEv
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint19calculateTransformsEv
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint19calculateTransformsEv,@function
_ZN23btGeneric6DofConstraint19calculateTransformsEv: # @_ZN23btGeneric6DofConstraint19calculateTransformsEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rsi
	movq	32(%rdi), %rdx
	addq	$8, %rsi
	addq	$8, %rdx
	jmp	_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_ # TAILCALL
.Lfunc_end9:
	.size	_ZN23btGeneric6DofConstraint19calculateTransformsEv, .Lfunc_end9-_ZN23btGeneric6DofConstraint19calculateTransformsEv
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_,@function
_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_: # @_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
	subq	$128, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 144
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	96(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	100(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movss	112(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm9
	movaps	%xmm9, 80(%rsp)         # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	128(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm7
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm10, %xmm0
	movss	116(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	132(%rbx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
	movss	104(%rbx), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm8, %xmm0
	movss	120(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	136(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm9, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movss	16(%rsi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movss	20(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm7, %xmm3
	movaps	%xmm7, 32(%rsp)         # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	24(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm10, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm8, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm9, %xmm7
	mulss	%xmm1, %xmm7
	addss	%xmm3, %xmm7
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	movss	36(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	movss	40(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm10
	mulss	%xmm3, %xmm4
	addss	%xmm10, %xmm4
	mulss	%xmm6, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm8
	mulss	%xmm3, %xmm2
	addss	%xmm8, %xmm2
	mulss	%xmm6, %xmm9
	addss	%xmm2, %xmm9
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	144(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	unpcklps	32(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	148(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movss	152(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	unpcklps	16(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	48(%rsi), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	addss	56(%rsi), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 1040(%rbx)
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 1044(%rbx)
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 1048(%rbx)
	movl	$0, 1052(%rbx)
	movss	%xmm15, 1056(%rbx)
	movss	%xmm13, 1060(%rbx)
	movss	%xmm7, 1064(%rbx)
	movl	$0, 1068(%rbx)
	movss	%xmm14, 1072(%rbx)
	movss	%xmm12, 1076(%rbx)
	movss	%xmm9, 1080(%rbx)
	movl	$0, 1084(%rbx)
	movlps	%xmm2, 1088(%rbx)
	movlps	%xmm1, 1096(%rbx)
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	160(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	164(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movss	176(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm7
	movaps	%xmm7, 80(%rsp)         # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	192(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm8
	movaps	%xmm8, 64(%rsp)         # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm10, %xmm0
	movss	180(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	196(%rbx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
	movss	168(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	movss	184(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	200(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movss	16(%rdx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movss	20(%rdx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	24(%rdx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm10, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm9, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm7, %xmm8
	mulss	%xmm1, %xmm8
	addss	%xmm3, %xmm8
	movss	32(%rdx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	movss	36(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm6, %xmm5
	movss	40(%rdx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm10
	mulss	%xmm3, %xmm4
	addss	%xmm10, %xmm4
	mulss	%xmm6, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm9
	mulss	%xmm3, %xmm2
	addss	%xmm9, %xmm2
	mulss	%xmm6, %xmm7
	addss	%xmm2, %xmm7
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	208(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	unpcklps	32(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	212(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	unpcklps	16(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	216(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	48(%rdx), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	xorps	%xmm1, %xmm1
	addss	56(%rdx), %xmm0
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 1104(%rbx)
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 1108(%rbx)
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 1112(%rbx)
	movl	$0, 1116(%rbx)
	movss	%xmm15, 1120(%rbx)
	movss	%xmm13, 1124(%rbx)
	movss	%xmm8, 1128(%rbx)
	movl	$0, 1132(%rbx)
	movss	%xmm14, 1136(%rbx)
	movss	%xmm12, 1140(%rbx)
	movss	%xmm7, 1144(%rbx)
	movl	$0, 1148(%rbx)
	movlps	%xmm2, 1152(%rbx)
	movlps	%xmm1, 1160(%rbx)
	callq	_ZN23btGeneric6DofConstraint19calculateLinearInfoEv
	movq	%rbx, %rdi
	addq	$128, %rsp
	popq	%rbx
	jmp	_ZN23btGeneric6DofConstraint18calculateAngleInfoEv # TAILCALL
.Lfunc_end10:
	.size	_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_, .Lfunc_end10-_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN23btGeneric6DofConstraint19calculateLinearInfoEv
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint19calculateLinearInfoEv,@function
_ZN23btGeneric6DofConstraint19calculateLinearInfoEv: # @_ZN23btGeneric6DofConstraint19calculateLinearInfoEv
	.cfi_startproc
# BB#0:
	movss	1060(%rdi), %xmm13      # xmm13 = mem[0],zero,zero,zero
	movss	1076(%rdi), %xmm9       # xmm9 = mem[0],zero,zero,zero
	movss	1072(%rdi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	movsd	1060(%rdi), %xmm8       # xmm8 = mem[0],zero
	movss	1080(%rdi), %xmm14      # xmm14 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm14   # xmm14 = xmm14[0],xmm1[0],xmm14[1],xmm1[1]
	movss	1040(%rdi), %xmm10      # xmm10 = mem[0],zero,zero,zero
	movss	1056(%rdi), %xmm11      # xmm11 = mem[0],zero,zero,zero
	movss	1064(%rdi), %xmm2       # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movsd	1076(%rdi), %xmm3       # xmm3 = mem[0],zero
	movaps	%xmm2, %xmm6
	mulps	%xmm3, %xmm6
	movq	1044(%rdi), %xmm4       # xmm4 = mem[0],zero
	pshufd	$229, %xmm4, %xmm12     # xmm12 = xmm4[1,1,2,3]
	movaps	%xmm10, %xmm5
	shufps	$0, %xmm12, %xmm5       # xmm5 = xmm5[0,0],xmm12[0,0]
	shufps	$226, %xmm12, %xmm5     # xmm5 = xmm5[2,0],xmm12[2,3]
	mulps	%xmm5, %xmm3
	mulps	%xmm8, %xmm5
	mulps	%xmm14, %xmm8
	subps	%xmm6, %xmm8
	movaps	%xmm9, %xmm6
	mulss	%xmm11, %xmm6
	movaps	%xmm13, %xmm0
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm6
	movaps	%xmm8, %xmm0
	mulss	%xmm10, %xmm0
	movaps	%xmm8, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	%xmm4, %xmm7
	addss	%xmm0, %xmm7
	movaps	%xmm6, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm7, %xmm0
	movss	.LCPI11_0(%rip), %xmm7  # xmm7 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm7
	mulps	%xmm4, %xmm14
	subps	%xmm14, %xmm3
	mulps	%xmm4, %xmm2
	subps	%xmm5, %xmm2
	mulss	%xmm4, %xmm1
	mulss	%xmm10, %xmm9
	subss	%xmm9, %xmm1
	mulss	%xmm10, %xmm13
	mulss	%xmm4, %xmm11
	subss	%xmm11, %xmm13
	mulss	%xmm7, %xmm6
	mulss	%xmm7, %xmm1
	mulss	%xmm7, %xmm13
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm7, %xmm8
	mulps	%xmm7, %xmm3
	mulps	%xmm7, %xmm2
	movss	1152(%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	subss	1088(%rdi), %xmm0
	mulss	%xmm0, %xmm6
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm8, %xmm0
	movss	1156(%rdi), %xmm4       # xmm4 = mem[0],zero,zero,zero
	subss	1092(%rdi), %xmm4
	mulss	%xmm4, %xmm1
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm0, %xmm4
	movss	1160(%rdi), %xmm3       # xmm3 = mem[0],zero,zero,zero
	subss	1096(%rdi), %xmm3
	mulss	%xmm3, %xmm13
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	addps	%xmm4, %xmm3
	addss	%xmm6, %xmm1
	addss	%xmm1, %xmm13
	xorps	%xmm0, %xmm0
	movss	%xmm13, %xmm0           # xmm0 = xmm13[0],xmm0[1,2,3]
	movlps	%xmm3, 1232(%rdi)
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movlps	%xmm0, 1240(%rdi)
	movss	%xmm3, 840(%rdi)
	movss	728(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	744(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm4
	ja	.LBB11_5
# BB#1:
	ucomiss	%xmm3, %xmm4
	jbe	.LBB11_3
# BB#2:
	movl	$2, 856(%rdi)
	subss	%xmm4, %xmm3
	jmp	.LBB11_6
.LBB11_3:
	ucomiss	%xmm2, %xmm3
	jbe	.LBB11_5
# BB#4:
	movl	$1, 856(%rdi)
	subss	%xmm2, %xmm3
	jmp	.LBB11_6
.LBB11_5:
	movl	$0, 856(%rdi)
	xorps	%xmm3, %xmm3
.LBB11_6:                               # %_ZN25btTranslationalLimitMotor14testLimitValueEif.exit
	movss	%xmm3, 824(%rdi)
	movss	%xmm1, 844(%rdi)
	movss	732(%rdi), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	748(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	ja	.LBB11_11
# BB#7:
	ucomiss	%xmm1, %xmm3
	jbe	.LBB11_8
# BB#10:
	movl	$2, 860(%rdi)
	subss	%xmm3, %xmm1
	jmp	.LBB11_12
.LBB11_8:
	ucomiss	%xmm2, %xmm1
	jbe	.LBB11_11
# BB#9:
	movl	$1, 860(%rdi)
	subss	%xmm2, %xmm1
	jmp	.LBB11_12
.LBB11_11:
	movl	$0, 860(%rdi)
	xorps	%xmm1, %xmm1
.LBB11_12:                              # %_ZN25btTranslationalLimitMotor14testLimitValueEif.exit.1
	movss	%xmm1, 828(%rdi)
	movss	%xmm13, 848(%rdi)
	movss	736(%rdi), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	752(%rdi), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	ja	.LBB11_17
# BB#13:
	ucomiss	%xmm13, %xmm2
	jbe	.LBB11_14
# BB#16:
	movl	$2, 864(%rdi)
	subss	%xmm2, %xmm13
	movss	%xmm13, 832(%rdi)
	retq
.LBB11_14:
	ucomiss	%xmm1, %xmm13
	jbe	.LBB11_17
# BB#15:
	movl	$1, 864(%rdi)
	subss	%xmm1, %xmm13
	movss	%xmm13, 832(%rdi)
	retq
.LBB11_17:
	movl	$0, 864(%rdi)
	xorps	%xmm13, %xmm13
	movss	%xmm13, 832(%rdi)
	retq
.Lfunc_end11:
	.size	_ZN23btGeneric6DofConstraint19calculateLinearInfoEv, .Lfunc_end11-_ZN23btGeneric6DofConstraint19calculateLinearInfoEv
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_,@function
_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_: # @_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_
	.cfi_startproc
# BB#0:
	subq	$136, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 144
	movq	%rdx, %r9
	movq	24(%rdi), %rax
	movl	8(%rax), %edx
	movl	%edx, 56(%rsp)
	movl	24(%rax), %edx
	movl	%edx, 60(%rsp)
	movl	40(%rax), %edx
	movl	%edx, 64(%rsp)
	movl	$0, 68(%rsp)
	movl	12(%rax), %edx
	movl	%edx, 72(%rsp)
	movl	28(%rax), %edx
	movl	%edx, 76(%rsp)
	movl	44(%rax), %edx
	movl	%edx, 80(%rsp)
	movl	$0, 84(%rsp)
	movl	16(%rax), %edx
	movl	%edx, 88(%rsp)
	movl	32(%rax), %edx
	movl	%edx, 92(%rsp)
	movl	48(%rax), %eax
	movl	%eax, 96(%rsp)
	movl	$0, 100(%rsp)
	movq	32(%rdi), %rdx
	movl	8(%rdx), %eax
	movl	%eax, 8(%rsp)
	movl	24(%rdx), %eax
	movl	%eax, 12(%rsp)
	movl	40(%rdx), %eax
	movl	%eax, 16(%rsp)
	movl	$0, 20(%rsp)
	movl	12(%rdx), %eax
	movl	%eax, 24(%rsp)
	movl	28(%rdx), %eax
	movl	%eax, 28(%rsp)
	movl	44(%rdx), %eax
	movl	%eax, 32(%rsp)
	movl	$0, 36(%rsp)
	movl	16(%rdx), %eax
	movl	%eax, 40(%rsp)
	movl	32(%rdx), %eax
	movl	%eax, 44(%rsp)
	movl	48(%rdx), %eax
	movl	%eax, 48(%rsp)
	movl	$0, 52(%rsp)
	movq	24(%rdi), %r11
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movsd	56(%r11), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	8(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	64(%r11), %xmm1
	xorps	%xmm2, %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm1, %xmm3            # xmm3 = xmm1[0],xmm3[1,2,3]
	movlps	%xmm0, 120(%rsp)
	movlps	%xmm3, 128(%rsp)
	movsd	(%r8), %xmm0            # xmm0 = mem[0],zero
	movsd	56(%rdx), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	8(%r8), %xmm1           # xmm1 = mem[0],zero,zero,zero
	subss	64(%rdx), %xmm1
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 104(%rsp)
	movlps	%xmm2, 112(%rsp)
	movss	360(%r11), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addq	$428, %r11              # imm = 0x1AC
	movq	32(%rdi), %rax
	movss	360(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addq	$428, %rax              # imm = 0x1AC
	leaq	56(%rsp), %r10
	leaq	8(%rsp), %rdx
	leaq	120(%rsp), %rcx
	leaq	104(%rsp), %r8
	movq	%rsi, %rdi
	movq	%r10, %rsi
	pushq	%rax
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	addq	$152, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -16
	retq
.Lfunc_end12:
	.size	_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_, .Lfunc_end12-_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_
	.cfi_endproc

	.section	.text._ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,"axG",@progbits,_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,comdat
	.weak	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.p2align	4, 0x90
	.type	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,@function
_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f: # @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_startproc
# BB#0:
	movq	16(%rsp), %r10
	movq	8(%rsp), %rax
	movups	(%r9), %xmm2
	movups	%xmm2, (%rdi)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm11, %xmm7
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm9, %xmm5
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm10, %xmm2
	mulss	%xmm9, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1]
	mulps	%xmm4, %xmm6
	movss	20(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm12   # xmm12 = xmm12[0],xmm8[0],xmm12[1],xmm8[1]
	mulps	%xmm3, %xmm12
	addps	%xmm4, %xmm12
	mulss	32(%rsi), %xmm7
	mulss	36(%rsi), %xmm5
	addss	%xmm7, %xmm5
	mulss	40(%rsi), %xmm2
	addss	%xmm5, %xmm2
	xorps	%xmm8, %xmm8
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm12, 16(%rdi)
	movlps	%xmm3, 24(%rdi)
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm11, %xmm5
	movss	8(%r8), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm6
	mulss	%xmm10, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm9, %xmm7
	mulss	%xmm3, %xmm11
	subss	%xmm7, %xmm11
	mulss	%xmm10, %xmm3
	mulss	%xmm9, %xmm4
	subss	%xmm3, %xmm4
	movss	16(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	movss	20(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	movaps	%xmm11, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm7
	addps	%xmm3, %xmm7
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	mulps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	mulss	32(%rdx), %xmm6
	mulss	36(%rdx), %xmm11
	addss	%xmm6, %xmm11
	mulss	40(%rdx), %xmm4
	addss	%xmm11, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm5, 32(%rdi)
	movlps	%xmm3, 40(%rdi)
	movsd	(%rax), %xmm9           # xmm9 = mem[0],zero
	mulps	%xmm12, %xmm9
	mulss	8(%rax), %xmm2
	xorps	%xmm6, %xmm6
	movss	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1,2,3]
	movlps	%xmm9, 48(%rdi)
	movlps	%xmm6, 56(%rdi)
	movsd	(%r10), %xmm6           # xmm6 = mem[0],zero
	mulps	%xmm5, %xmm6
	mulss	8(%r10), %xmm4
	movaps	%xmm6, %xmm7
	movlps	%xmm6, 64(%rdi)
	mulss	%xmm5, %xmm6
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movss	%xmm4, %xmm8            # xmm8 = xmm4[0],xmm8[1,2,3]
	movlps	%xmm8, 72(%rdi)
	movss	16(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	20(%rdi), %xmm9
	addss	%xmm3, %xmm9
	mulss	24(%rdi), %xmm2
	addss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm5, %xmm7
	addss	%xmm6, %xmm7
	mulss	40(%rdi), %xmm4
	addss	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	movss	%xmm4, 80(%rdi)
	retq
.Lfunc_end13:
	.size	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f, .Lfunc_end13-_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN23btGeneric6DofConstraint20buildAngularJacobianER15btJacobianEntryRK9btVector3
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint20buildAngularJacobianER15btJacobianEntryRK9btVector3,@function
_ZN23btGeneric6DofConstraint20buildAngularJacobianER15btJacobianEntryRK9btVector3: # @_ZN23btGeneric6DofConstraint20buildAngularJacobianER15btJacobianEntryRK9btVector3
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rcx
	movq	32(%rdi), %rax
	movsd	8(%rcx), %xmm6          # xmm6 = mem[0],zero
	movsd	24(%rcx), %xmm14        # xmm14 = mem[0],zero
	movsd	40(%rcx), %xmm12        # xmm12 = mem[0],zero
	movss	16(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	32(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	48(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movsd	8(%rax), %xmm10         # xmm10 = mem[0],zero
	movsd	24(%rax), %xmm11        # xmm11 = mem[0],zero
	movsd	40(%rax), %xmm8         # xmm8 = mem[0],zero
	movss	16(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	32(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	48(%rax), %xmm15        # xmm15 = mem[0],zero,zero,zero
	xorps	%xmm13, %xmm13
	movups	%xmm13, (%rsi)
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	mulss	%xmm4, %xmm0
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm14, %xmm4
	addps	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm12, %xmm2
	addps	%xmm4, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm5, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 16(%rsi)
	movlps	%xmm1, 24(%rsi)
	movss	(%rdx), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	.LCPI14_0(%rip), %xmm4  # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm4, %xmm12
	mulss	%xmm1, %xmm7
	xorps	%xmm4, %xmm1
	movss	8(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	xorps	%xmm6, %xmm4
	pshufd	$224, %xmm12, %xmm5     # xmm5 = xmm12[0,0,2,3]
	mulps	%xmm10, %xmm5
	pshufd	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm11, %xmm1
	addps	%xmm5, %xmm1
	pshufd	$224, %xmm4, %xmm5      # xmm5 = xmm4[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm1, %xmm5
	mulss	%xmm9, %xmm12
	subss	%xmm7, %xmm12
	mulss	%xmm6, %xmm15
	subss	%xmm15, %xmm12
	xorps	%xmm1, %xmm1
	movss	%xmm12, %xmm1           # xmm1 = xmm12[0],xmm1[1,2,3]
	movlps	%xmm5, 32(%rsi)
	movlps	%xmm1, 40(%rsi)
	movsd	428(%rcx), %xmm8        # xmm8 = mem[0],zero
	mulps	%xmm2, %xmm8
	movss	436(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm7
	movlps	%xmm8, 48(%rsi)
	mulss	%xmm2, %xmm8
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	mulss	%xmm0, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm1, 56(%rsi)
	movsd	428(%rax), %xmm1        # xmm1 = mem[0],zero
	mulps	%xmm5, %xmm1
	movss	436(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm6
	movlps	%xmm1, 64(%rsi)
	mulss	%xmm5, %xmm1
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	%xmm12, %xmm3
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movss	%xmm3, %xmm13           # xmm13 = xmm3[0],xmm13[1,2,3]
	movlps	%xmm13, 72(%rsi)
	mulss	%xmm2, %xmm7
	addss	%xmm8, %xmm7
	mulss	%xmm0, %xmm4
	addss	%xmm7, %xmm4
	mulss	%xmm5, %xmm6
	addss	%xmm1, %xmm6
	mulss	%xmm12, %xmm3
	addss	%xmm6, %xmm3
	addss	%xmm4, %xmm3
	movss	%xmm3, 80(%rsi)
	retq
.Lfunc_end14:
	.size	_ZN23btGeneric6DofConstraint20buildAngularJacobianER15btJacobianEntryRK9btVector3, .Lfunc_end14-_ZN23btGeneric6DofConstraint20buildAngularJacobianER15btJacobianEntryRK9btVector3
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi,@function
_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi: # @_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movslq	%esi, %rax
	movss	1168(%rbx,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	imulq	$56, %rax, %r14
	movss	868(%rbx,%r14), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	872(%rbx,%r14), %xmm2   # xmm2 = mem[0],zero,zero,zero
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 912(%rbx,%r14)
	movss	868(%rbx,%r14), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	872(%rbx,%r14), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB15_4
# BB#1:
	leaq	916(%rbx,%r14), %rax
	jmp	.LBB15_2
.LBB15_4:
	ucomiss	%xmm0, %xmm1
	jbe	.LBB15_6
# BB#5:
	movl	$1, 916(%rbx,%r14)
	jmp	.LBB15_8
.LBB15_6:
	leaq	916(%rbx,%r14), %rax
	ucomiss	%xmm2, %xmm0
	jbe	.LBB15_2
# BB#7:
	movl	$2, (%rax)
	movaps	%xmm2, %xmm1
.LBB15_8:                               # %_ZN22btRotationalLimitMotor14testLimitValueEf.exit.sink.split
	subss	%xmm1, %xmm0
	movss	%xmm0, 908(%rbx,%r14)
	jmp	.LBB15_9
.LBB15_2:
	movl	$0, (%rax)
	cmpb	$0, 904(%rbx,%r14)
	je	.LBB15_3
.LBB15_9:                               # %_ZN22btRotationalLimitMotor14testLimitValueEf.exit
	movb	$1, %al
	jmp	.LBB15_10
.LBB15_3:
	xorl	%eax, %eax
.LBB15_10:                              # %_ZN22btRotationalLimitMotor16needApplyTorquesEv.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi, .Lfunc_end15-_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI16_0:
	.long	1086918619              # float 6.28318548
.LCPI16_1:
	.long	3226013659              # float -3.14159274
.LCPI16_2:
	.long	1078530011              # float 3.14159274
.LCPI16_3:
	.long	3234402267              # float -6.28318548
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_4:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.text._Z21btAdjustAngleToLimitsfff,"axG",@progbits,_Z21btAdjustAngleToLimitsfff,comdat
	.weak	_Z21btAdjustAngleToLimitsfff
	.p2align	4, 0x90
	.type	_Z21btAdjustAngleToLimitsfff,@function
_Z21btAdjustAngleToLimitsfff:           # @_Z21btAdjustAngleToLimitsfff
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 48
	movaps	%xmm1, %xmm3
	ucomiss	%xmm2, %xmm3
	jae	.LBB16_21
# BB#1:
	ucomiss	%xmm0, %xmm3
	jbe	.LBB16_11
# BB#2:
	movss	%xmm2, (%rsp)           # 4-byte Spill
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	subss	%xmm0, %xmm3
	movss	.LCPI16_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	callq	fmodf
	movss	.LCPI16_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB16_4
# BB#3:
	addss	.LCPI16_0(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB16_6
.LBB16_11:
	ucomiss	%xmm2, %xmm0
	jbe	.LBB16_21
# BB#12:
	movss	%xmm3, (%rsp)           # 4-byte Spill
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	subss	%xmm2, %xmm0
	movss	.LCPI16_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	callq	fmodf
	movaps	%xmm0, %xmm1
	movss	.LCPI16_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB16_14
# BB#13:
	addss	.LCPI16_0(%rip), %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB16_16
.LBB16_4:
	ucomiss	.LCPI16_2(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jbe	.LBB16_6
# BB#5:
	addss	.LCPI16_3(%rip), %xmm0
.LBB16_6:                               # %_Z16btNormalizeAnglef.exit
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	subss	%xmm1, %xmm2
	movss	.LCPI16_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	callq	fmodf
	movss	.LCPI16_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB16_8
# BB#7:
	addss	.LCPI16_0(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movaps	(%rsp), %xmm2           # 16-byte Reload
	jmp	.LBB16_10
.LBB16_8:
	ucomiss	.LCPI16_2(%rip), %xmm0
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movaps	(%rsp), %xmm2           # 16-byte Reload
	jbe	.LBB16_10
# BB#9:
	addss	.LCPI16_3(%rip), %xmm0
.LBB16_10:                              # %_Z16btNormalizeAnglef.exit30
	andps	.LCPI16_4(%rip), %xmm0
	cmpltss	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	andps	%xmm1, %xmm0
	addss	.LCPI16_0(%rip), %xmm1
	andnps	%xmm1, %xmm2
	orps	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	addq	$40, %rsp
	retq
.LBB16_14:
	ucomiss	.LCPI16_2(%rip), %xmm1
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movss	(%rsp), %xmm2           # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jbe	.LBB16_16
# BB#15:
	addss	.LCPI16_3(%rip), %xmm1
.LBB16_16:                              # %_Z16btNormalizeAnglef.exit32
	movss	%xmm1, (%rsp)           # 4-byte Spill
	subss	%xmm2, %xmm0
	movss	.LCPI16_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	callq	fmodf
	movss	.LCPI16_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB16_18
# BB#17:
	addss	.LCPI16_0(%rip), %xmm0
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movss	(%rsp), %xmm3           # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	jmp	.LBB16_20
.LBB16_18:
	ucomiss	.LCPI16_2(%rip), %xmm0
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movss	(%rsp), %xmm3           # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	jbe	.LBB16_20
# BB#19:
	addss	.LCPI16_3(%rip), %xmm0
.LBB16_20:                              # %_Z16btNormalizeAnglef.exit34
	andps	.LCPI16_4(%rip), %xmm0
	movss	.LCPI16_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm1
	cmpltss	%xmm3, %xmm0
	andps	%xmm0, %xmm1
	andnps	%xmm2, %xmm0
	orps	%xmm1, %xmm0
.LBB16_21:
	addq	$40, %rsp
	retq
.Lfunc_end16:
	.size	_Z21btAdjustAngleToLimitsfff, .Lfunc_end16-_Z21btAdjustAngleToLimitsfff
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI17_1:
	.zero	16
	.text
	.globl	_ZN23btGeneric6DofConstraint13buildJacobianEv
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint13buildJacobianEv,@function
_ZN23btGeneric6DofConstraint13buildJacobianEv: # @_ZN23btGeneric6DofConstraint13buildJacobianEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 96
.Lcfi40:
	.cfi_offset %rbx, -48
.Lcfi41:
	.cfi_offset %r12, -40
.Lcfi42:
	.cfi_offset %r13, -32
.Lcfi43:
	.cfi_offset %r14, -24
.Lcfi44:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpb	$0, 1265(%r15)
	je	.LBB17_19
# BB#1:
	movl	$0, 920(%r15)
	movl	$0, 976(%r15)
	movl	$0, 1032(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 760(%r15)
	movq	24(%r15), %rsi
	movq	32(%r15), %rdx
	addq	$8, %rsi
	addq	$8, %rdx
	movq	%r15, %rdi
	callq	_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*56(%rax)
	movups	1248(%r15), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	1248(%r15), %xmm0
	movaps	%xmm0, 16(%rsp)
	leaq	1104(%r15), %r14
	leaq	1040(%r15), %rbx
	movss	744(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	728(%r15), %xmm0
	jb	.LBB17_3
# BB#2:
	cmpb	$0, 1264(%r15)
	movq	%rbx, %rax
	cmoveq	%r14, %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	32(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movlps	%xmm0, (%rsp)
	movlps	%xmm1, 8(%rsp)
	leaq	224(%r15), %rsi
	movq	%rsp, %rdx
	leaq	32(%rsp), %rcx
	leaq	16(%rsp), %r8
	movq	%r15, %rdi
	callq	_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_
.LBB17_3:
	movss	748(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	732(%r15), %xmm0
	jb	.LBB17_5
# BB#4:
	cmpb	$0, 1264(%r15)
	movq	%rbx, %rax
	cmoveq	%r14, %rax
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	20(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	36(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movlps	%xmm0, (%rsp)
	movlps	%xmm1, 8(%rsp)
	leaq	308(%r15), %rsi
	movq	%rsp, %rdx
	leaq	32(%rsp), %rcx
	leaq	16(%rsp), %r8
	movq	%r15, %rdi
	callq	_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_
.LBB17_5:
	movss	752(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	736(%r15), %xmm0
	jb	.LBB17_7
# BB#6:
	cmpb	$0, 1264(%r15)
	cmoveq	%r14, %rbx
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	40(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movlps	%xmm0, (%rsp)
	movlps	%xmm1, 8(%rsp)
	leaq	392(%r15), %rsi
	movq	%rsp, %rdx
	leaq	32(%rsp), %rcx
	leaq	16(%rsp), %r8
	movq	%r15, %rdi
	callq	_ZN23btGeneric6DofConstraint19buildLinearJacobianER15btJacobianEntryRK9btVector3S4_S4_
.LBB17_7:                               # %.preheader46
	movl	$916, %r14d             # imm = 0x394
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.LBB17_8
.LBB17_13:                              #   in Loop: Header=BB17_8 Depth=1
	leaq	(%r15,%r14), %rax
	jmp	.LBB17_14
	.p2align	4, 0x90
.LBB17_8:                               # =>This Inner Loop Header: Depth=1
	movss	1168(%r15,%r12), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movss	-48(%r15,%r14), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	-44(%r15,%r14), %xmm2   # xmm2 = mem[0],zero,zero,zero
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, -4(%r15,%r14)
	movss	-48(%r15,%r14), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	-44(%r15,%r14), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB17_10
# BB#9:                                 #   in Loop: Header=BB17_8 Depth=1
	imulq	$56, %r13, %rax
	leaq	916(%r15,%rax), %rax
.LBB17_14:                              #   in Loop: Header=BB17_8 Depth=1
	movl	$0, (%rax)
	cmpb	$0, -12(%r15,%r14)
	jne	.LBB17_17
	jmp	.LBB17_18
	.p2align	4, 0x90
.LBB17_10:                              #   in Loop: Header=BB17_8 Depth=1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB17_12
# BB#11:                                #   in Loop: Header=BB17_8 Depth=1
	movl	$1, (%r15,%r14)
	jmp	.LBB17_16
	.p2align	4, 0x90
.LBB17_12:                              #   in Loop: Header=BB17_8 Depth=1
	ucomiss	%xmm2, %xmm0
	jbe	.LBB17_13
# BB#15:                                #   in Loop: Header=BB17_8 Depth=1
	movl	$2, (%r15,%r14)
	movaps	%xmm2, %xmm1
.LBB17_16:                              # %_ZN22btRotationalLimitMotor14testLimitValueEf.exit.sink.split.i
                                        #   in Loop: Header=BB17_8 Depth=1
	subss	%xmm1, %xmm0
	movss	%xmm0, -8(%r15,%r14)
.LBB17_17:                              #   in Loop: Header=BB17_8 Depth=1
	movq	1184(%r15,%r12,4), %rdx
	movq	1192(%r15,%r12,4), %rsi
	movq	%rdx, (%rsp)
	movq	%rsi, 8(%rsp)
	movq	24(%r15), %rcx
	movq	32(%r15), %rax
	movsd	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	movsd	24(%rcx), %xmm3         # xmm3 = mem[0],zero
	movsd	40(%rcx), %xmm11        # xmm11 = mem[0],zero
	movsd	8(%rax), %xmm10         # xmm10 = mem[0],zero
	movsd	24(%rax), %xmm9         # xmm9 = mem[0],zero
	movsd	40(%rax), %xmm8         # xmm8 = mem[0],zero
	movd	%edx, %xmm2
	shrq	$32, %rdx
	movd	%edx, %xmm5
	movd	%esi, %xmm6
	movss	16(%rcx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm12
	movss	32(%rcx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm14
	movaps	%xmm2, %xmm13
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	movaps	%xmm5, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm3, %xmm7
	movss	48(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	addps	%xmm2, %xmm7
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm11, %xmm3
	movss	16(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addps	%xmm7, %xmm3
	movss	32(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	%xmm12, %xmm14
	movss	48(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	movups	%xmm0, 476(%r15,%rbx)
	addss	%xmm4, %xmm14
	xorps	%xmm4, %xmm4
	movss	%xmm14, %xmm4           # xmm4 = xmm14[0],xmm4[1,2,3]
	movlps	%xmm3, 492(%r15,%rbx)
	movlps	%xmm4, 500(%r15,%rbx)
	movaps	.LCPI17_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm13
	mulss	%xmm13, %xmm1
	pshufd	$224, %xmm13, %xmm4     # xmm4 = xmm13[0,0,2,3]
	mulps	%xmm10, %xmm4
	mulss	%xmm5, %xmm2
	xorps	%xmm0, %xmm5
	pshufd	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm9, %xmm5
	mulss	%xmm6, %xmm7
	xorps	%xmm0, %xmm6
	addps	%xmm4, %xmm5
	pshufd	$224, %xmm6, %xmm4      # xmm4 = xmm6[0,0,2,3]
	mulps	%xmm8, %xmm4
	addps	%xmm5, %xmm4
	subss	%xmm2, %xmm1
	subss	%xmm7, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm4, 508(%r15,%rbx)
	movlps	%xmm2, 516(%r15,%rbx)
	movsd	428(%rcx), %xmm5        # xmm5 = mem[0],zero
	mulps	%xmm3, %xmm5
	movss	436(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm2
	xorps	%xmm6, %xmm6
	movss	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1,2,3]
	movlps	%xmm5, 524(%r15,%rbx)
	movlps	%xmm6, 532(%r15,%rbx)
	movsd	428(%rax), %xmm6        # xmm6 = mem[0],zero
	mulps	%xmm4, %xmm6
	movss	436(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm6, 540(%r15,%rbx)
	movlps	%xmm0, 548(%r15,%rbx)
	movaps	%xmm5, %xmm0
	mulss	%xmm3, %xmm5
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm3, %xmm0
	addss	%xmm5, %xmm0
	mulss	%xmm14, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm6, %xmm0
	mulss	%xmm4, %xmm6
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm4, %xmm0
	addss	%xmm6, %xmm0
	mulss	%xmm1, %xmm7
	addss	%xmm0, %xmm7
	addss	%xmm2, %xmm7
	movss	%xmm7, 556(%r15,%rbx)
.LBB17_18:                              # %_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi.exit
                                        #   in Loop: Header=BB17_8 Depth=1
	incq	%r13
	addq	$4, %r12
	addq	$56, %r14
	addq	$84, %rbx
	cmpq	$12, %r12
	jne	.LBB17_8
.LBB17_19:
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZN23btGeneric6DofConstraint13buildJacobianEv, .Lfunc_end17-_ZN23btGeneric6DofConstraint13buildJacobianEv
	.cfi_endproc

	.globl	_ZNK23btGeneric6DofConstraint7getAxisEi
	.p2align	4, 0x90
	.type	_ZNK23btGeneric6DofConstraint7getAxisEi,@function
_ZNK23btGeneric6DofConstraint7getAxisEi: # @_ZNK23btGeneric6DofConstraint7getAxisEi
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	shlq	$4, %rax
	movsd	1184(%rdi,%rax), %xmm0  # xmm0 = mem[0],zero
	movsd	1192(%rdi,%rax), %xmm1  # xmm1 = mem[0],zero
	retq
.Lfunc_end18:
	.size	_ZNK23btGeneric6DofConstraint7getAxisEi, .Lfunc_end18-_ZNK23btGeneric6DofConstraint7getAxisEi
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E: # @_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -24
.Lcfi49:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 1265(%rbx)
	je	.LBB19_3
# BB#1:
	movq	$0, (%r14)
	jmp	.LBB19_2
.LBB19_3:
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rdx
	addq	$8, %rsi
	addq	$8, %rdx
	movq	%rbx, %rdi
	callq	_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_
	movabsq	$25769803776, %rax      # imm = 0x600000000
	movq	%rax, (%r14)
	cmpl	$0, 856(%rbx)
	jne	.LBB19_6
# BB#4:
	cmpb	$0, 788(%rbx)
	je	.LBB19_5
.LBB19_6:
	movabsq	$21474836481, %rax      # imm = 0x500000001
	movq	%rax, (%r14)
	movl	$1, %ecx
	movl	$5, %eax
	cmpl	$0, 860(%rbx)
	jne	.LBB19_9
.LBB19_8:
	cmpb	$0, 789(%rbx)
	jne	.LBB19_9
	jmp	.LBB19_10
.LBB19_5:
	xorl	%ecx, %ecx
	movl	$6, %eax
	cmpl	$0, 860(%rbx)
	je	.LBB19_8
.LBB19_9:
	incl	%ecx
	movl	%ecx, (%r14)
	decl	%eax
	movl	%eax, 4(%r14)
.LBB19_10:                              # %_ZN25btTranslationalLimitMotor14needApplyForceEi.exit.1
	cmpl	$0, 864(%rbx)
	jne	.LBB19_12
# BB#11:
	cmpb	$0, 790(%rbx)
	je	.LBB19_13
.LBB19_12:
	incl	%ecx
	movl	%ecx, (%r14)
	decl	%eax
	movl	%eax, 4(%r14)
.LBB19_13:                              # %_ZN25btTranslationalLimitMotor14needApplyForceEi.exit.2
	movss	1168(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	868(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	872(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 912(%rbx)
	movss	868(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	872(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB19_14
.LBB19_36:
	movl	$0, 916(%rbx)
	cmpb	$0, 904(%rbx)
	jne	.LBB19_19
	jmp	.LBB19_20
.LBB19_14:
	ucomiss	%xmm0, %xmm1
	jbe	.LBB19_16
# BB#15:
	movl	$1, 916(%rbx)
	jmp	.LBB19_18
.LBB19_16:
	ucomiss	%xmm2, %xmm0
	jbe	.LBB19_36
# BB#17:
	movl	$2, 916(%rbx)
	movaps	%xmm2, %xmm1
.LBB19_18:                              # %_ZN22btRotationalLimitMotor14testLimitValueEf.exit.sink.split.i
	subss	%xmm1, %xmm0
	movss	%xmm0, 908(%rbx)
.LBB19_19:
	incl	(%r14)
	decl	4(%r14)
.LBB19_20:                              # %_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi.exit
	movss	1172(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	924(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	928(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 968(%rbx)
	movss	924(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	928(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB19_21
.LBB19_26:
	movl	$0, 972(%rbx)
	cmpb	$0, 960(%rbx)
	jne	.LBB19_27
	jmp	.LBB19_28
.LBB19_21:
	ucomiss	%xmm0, %xmm2
	jbe	.LBB19_22
# BB#24:
	movl	$1, 972(%rbx)
	movaps	%xmm2, %xmm1
	jmp	.LBB19_25
.LBB19_22:
	ucomiss	%xmm1, %xmm0
	jbe	.LBB19_26
# BB#23:
	movl	$2, 972(%rbx)
.LBB19_25:                              # %_ZN22btRotationalLimitMotor14testLimitValueEf.exit.sink.split.i.1
	subss	%xmm1, %xmm0
	movss	%xmm0, 964(%rbx)
.LBB19_27:
	incl	(%r14)
	decl	4(%r14)
.LBB19_28:                              # %_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi.exit.1
	movss	1176(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	980(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	984(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 1024(%rbx)
	movss	980(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	984(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB19_29
.LBB19_34:
	movl	$0, 1028(%rbx)
	cmpb	$0, 1016(%rbx)
	jne	.LBB19_35
	jmp	.LBB19_2
.LBB19_29:
	ucomiss	%xmm0, %xmm2
	jbe	.LBB19_30
# BB#32:
	movl	$1, 1028(%rbx)
	movaps	%xmm2, %xmm1
	jmp	.LBB19_33
.LBB19_30:
	ucomiss	%xmm1, %xmm0
	jbe	.LBB19_34
# BB#31:
	movl	$2, 1028(%rbx)
.LBB19_33:                              # %_ZN22btRotationalLimitMotor14testLimitValueEf.exit.sink.split.i.2
	subss	%xmm1, %xmm0
	movss	%xmm0, 1020(%rbx)
.LBB19_35:
	incl	(%r14)
	decl	4(%r14)
.LBB19_2:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end19:
	.size	_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end19-_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN23btGeneric6DofConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E: # @_ZN23btGeneric6DofConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpb	$0, 1265(%rdi)
	movl	$6, %ecx
	cmovnel	%eax, %ecx
	movl	%ecx, (%rsi)
	movl	$0, 4(%rsi)
	retq
.Lfunc_end20:
	.size	_ZN23btGeneric6DofConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end20-_ZN23btGeneric6DofConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E,@function
_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E: # @_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 16
	movq	24(%rdi), %rax
	movq	32(%rdi), %r10
	leaq	8(%rax), %rdx
	leaq	328(%rax), %r8
	leaq	344(%rax), %r11
	leaq	8(%r10), %rcx
	leaq	328(%r10), %r9
	leaq	344(%r10), %rax
	pushq	%rax
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	callq	_ZN23btGeneric6DofConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_
	addq	$16, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset -16
	popq	%rax
	retq
.Lfunc_end21:
	.size	_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E, .Lfunc_end21-_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_,@function
_ZN23btGeneric6DofConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_: # @_ZN23btGeneric6DofConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 64
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%r9, (%rsp)             # 8-byte Spill
	movq	%r8, %r13
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	_ZN23btGeneric6DofConstraint19calculateTransformsERK11btTransformS2_
	movss	1168(%rbp), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	868(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	872(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 912(%rbp)
	movss	868(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	872(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm1
	jbe	.LBB22_3
# BB#1:
	leaq	916(%rbp), %rax
	jmp	.LBB22_2
.LBB22_3:
	ucomiss	%xmm0, %xmm1
	jbe	.LBB22_5
# BB#4:
	movl	$1, 916(%rbp)
	jmp	.LBB22_7
.LBB22_5:
	leaq	916(%rbp), %rax
	ucomiss	%xmm2, %xmm0
	jbe	.LBB22_2
# BB#6:
	movl	$2, (%rax)
	movaps	%xmm2, %xmm1
.LBB22_7:                               # %_ZN22btRotationalLimitMotor14testLimitValueEf.exit.sink.split.i
	subss	%xmm1, %xmm0
	movss	%xmm0, 908(%rbp)
	jmp	.LBB22_8
.LBB22_2:
	movl	$0, (%rax)
.LBB22_8:                               # %_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi.exit
	movss	1172(%rbp), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	924(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	928(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 968(%rbp)
	movss	924(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	928(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB22_9
# BB#14:
	leaq	972(%rbp), %rax
	jmp	.LBB22_15
.LBB22_9:
	ucomiss	%xmm0, %xmm2
	jbe	.LBB22_10
# BB#12:
	movl	$1, 972(%rbp)
	movaps	%xmm2, %xmm1
	jmp	.LBB22_13
.LBB22_10:
	leaq	972(%rbp), %rax
	ucomiss	%xmm1, %xmm0
	jbe	.LBB22_15
# BB#11:
	movl	$2, (%rax)
.LBB22_13:                              # %_ZN22btRotationalLimitMotor14testLimitValueEf.exit.sink.split.i.1
	subss	%xmm1, %xmm0
	movss	%xmm0, 964(%rbp)
	jmp	.LBB22_16
.LBB22_15:
	movl	$0, (%rax)
.LBB22_16:                              # %_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi.exit.1
	movss	1176(%rbp), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	980(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	984(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	callq	_Z21btAdjustAngleToLimitsfff
	movss	%xmm0, 1024(%rbp)
	movss	980(%rbp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	984(%rbp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB22_17
# BB#22:
	leaq	1028(%rbp), %rax
	jmp	.LBB22_23
.LBB22_17:
	ucomiss	%xmm0, %xmm2
	jbe	.LBB22_18
# BB#20:
	movl	$1, 1028(%rbp)
	movaps	%xmm2, %xmm1
	jmp	.LBB22_21
.LBB22_18:
	leaq	1028(%rbp), %rax
	ucomiss	%xmm1, %xmm0
	jbe	.LBB22_23
# BB#19:
	movl	$2, (%rax)
.LBB22_21:                              # %_ZN22btRotationalLimitMotor14testLimitValueEf.exit.sink.split.i.2
	subss	%xmm1, %xmm0
	movss	%xmm0, 1020(%rbp)
	jmp	.LBB22_24
.LBB22_23:
	movl	$0, (%rax)
.LBB22_24:                              # %_ZN23btGeneric6DofConstraint21testAngularLimitMotorEi.exit.2
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	%r13, %r8
	movq	%r14, %r15
	movq	%r13, %r14
	movq	(%rsp), %r13            # 8-byte Reload
	movq	%r13, %r9
	movq	72(%rsp), %rax
	pushq	%rax
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	movq	72(%rsp), %rax
	pushq	%rax
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	callq	_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_
	addq	$16, %rsp
.Lcfi69:
	.cfi_adjust_cfa_offset -16
	subq	$8, %rsp
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movl	%eax, %edx
	movq	%r12, %rcx
	movq	%rbx, %r8
	movq	%r14, %r9
	pushq	80(%rsp)
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	callq	_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_
	addq	$40, %rsp
.Lcfi74:
	.cfi_adjust_cfa_offset -32
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	_ZN23btGeneric6DofConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_, .Lfunc_end22-_ZN23btGeneric6DofConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI23_0:
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.long	0                       # float 0
	.long	1036831949              # float 0.100000001
.LCPI23_1:
	.long	1133903872              # float 300
	.long	1065353216              # float 1
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.text
	.globl	_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_,@function
_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_: # @_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi81:
	.cfi_def_cfa_offset 144
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	$0, 52(%rsp)
	movaps	.LCPI23_0(%rip), %xmm0  # xmm0 = [1.000000e+00,-1.000000e+00,0.000000e+00,1.000000e-01]
	movaps	%xmm0, (%rsp)
	movl	$0, 32(%rsp)
	movaps	.LCPI23_1(%rip), %xmm0  # xmm0 = [3.000000e+02,1.000000e+00,5.000000e-01,5.000000e-01]
	movaps	%xmm0, 16(%rsp)
	movl	$0, 48(%rsp)
	movl	$0, 40(%rsp)
	movb	$0, 36(%rsp)
	xorl	%r14d, %r14d
	movq	$-3, %r15
	.p2align	4, 0x90
.LBB23_1:                               # =>This Inner Loop Header: Depth=1
	movl	868(%rbp,%r15,4), %ecx
	movzbl	791(%rbp,%r15), %eax
	testl	%ecx, %ecx
	jne	.LBB23_3
# BB#2:                                 #   in Loop: Header=BB23_1 Depth=1
	testb	%al, %al
	movb	$1, %al
	je	.LBB23_4
.LBB23_3:                               # %._crit_edge
                                        #   in Loop: Header=BB23_1 Depth=1
	movl	$0, 32(%rsp)
	movl	%ecx, 48(%rsp)
	movl	852(%rbp,%r15,4), %ecx
	movl	%ecx, 44(%rsp)
	movl	836(%rbp,%r15,4), %ecx
	movl	%ecx, 40(%rsp)
	movl	780(%rbp), %ecx
	movl	%ecx, 20(%rsp)
	movb	%al, 36(%rsp)
	movl	784(%rbp), %eax
	movl	%eax, 28(%rsp)
	movl	756(%rbp,%r15,4), %eax
	movl	%eax, 4(%rsp)
	movl	776(%rbp), %eax
	movl	%eax, 24(%rsp)
	movl	740(%rbp,%r15,4), %eax
	movl	%eax, (%rsp)
	movl	$0, 16(%rsp)
	movl	820(%rbp,%r15,4), %eax
	movl	%eax, 12(%rsp)
	movl	804(%rbp,%r15,4), %eax
	movl	%eax, 8(%rsp)
	movss	1068(%rbp,%r15,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	1052(%rbp,%r15,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	movss	1084(%rbp,%r15,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movlps	%xmm1, 72(%rsp)
	movlps	%xmm0, 80(%rsp)
	movq	%rbp, %rdi
	movq	%rsp, %rsi
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	64(%rsp), %r9           # 8-byte Reload
	pushq	$0
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	leaq	80(%rsp), %rax
	pushq	%rax
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	callq	_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i
	addq	$48, %rsp
.Lcfi94:
	.cfi_adjust_cfa_offset -48
	addl	%r14d, %eax
	movl	%eax, %r14d
.LBB23_4:                               # %_ZN25btTranslationalLimitMotor14needApplyForceEi.exit
                                        #   in Loop: Header=BB23_1 Depth=1
	incq	%r15
	jne	.LBB23_1
# BB#5:
	movl	%r14d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_, .Lfunc_end23-_ZN23btGeneric6DofConstraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_S8_S8_
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_,@function
_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_: # @_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi101:
	.cfi_def_cfa_offset 80
.Lcfi102:
	.cfi_offset %rbx, -56
.Lcfi103:
	.cfi_offset %r12, -48
.Lcfi104:
	.cfi_offset %r13, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rcx, %r13
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movq	80(%rsp), %rax
	cmpl	$0, 916(%rbp)
	jne	.LBB24_2
# BB#1:
	cmpb	$0, 904(%rbp)
	je	.LBB24_3
.LBB24_2:
	leaq	868(%rbp), %rsi
	movups	1184(%rbp), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %r10
	movq	%rbp, %rdi
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%rax, %r9
	pushq	$1
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi109:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi110:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	callq	_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i
	addq	$48, %rsp
.Lcfi114:
	.cfi_adjust_cfa_offset -48
	addl	%ebx, %eax
	movl	%eax, %ebx
.LBB24_3:                               # %_ZN22btRotationalLimitMotor16needApplyTorquesEv.exit
	cmpl	$0, 972(%rbp)
	jne	.LBB24_5
# BB#4:
	cmpb	$0, 960(%rbp)
	je	.LBB24_6
.LBB24_5:
	leaq	924(%rbp), %rsi
	movups	1200(%rbp), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rax
	movq	%rbp, %rdi
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	80(%rsp), %r9
	pushq	$1
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi116:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	callq	_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i
	addq	$48, %rsp
.Lcfi121:
	.cfi_adjust_cfa_offset -48
	addl	%ebx, %eax
	movl	%eax, %ebx
.LBB24_6:                               # %_ZN22btRotationalLimitMotor16needApplyTorquesEv.exit.1
	cmpl	$0, 1028(%rbp)
	jne	.LBB24_8
# BB#7:
	cmpb	$0, 1016(%rbp)
	je	.LBB24_9
.LBB24_8:
	movups	1216(%rbp), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rax
	leaq	980(%rbp), %rsi
	movq	%rbp, %rdi
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	80(%rsp), %r9
	pushq	$1
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	callq	_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i
	addq	$48, %rsp
.Lcfi128:
	.cfi_adjust_cfa_offset -48
	addl	%ebx, %eax
	movl	%eax, %ebx
.LBB24_9:                               # %_ZN22btRotationalLimitMotor16needApplyTorquesEv.exit.2
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_, .Lfunc_end24-_ZN23btGeneric6DofConstraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI25_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI25_1:
	.long	4286578687              # float -3.40282347E+38
.LCPI25_2:
	.long	2139095039              # float 3.40282347E+38
	.text
	.globl	_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i,@function
_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i: # @_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi135:
	.cfi_def_cfa_offset 80
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	96(%rsp), %r15
	movslq	40(%r15), %r14
	movb	36(%r12), %r11b
	testb	%r11b, %r11b
	movl	48(%r12), %ebx
	jne	.LBB25_2
# BB#1:
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.LBB25_35
.LBB25_2:
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	120(%rsp), %r8d
	movq	112(%rsp), %r10
	movslq	104(%rsp), %rax
	imulq	%rax, %r14
	leaq	8(%r15), %rax
	leaq	16(%r15), %rsi
	testl	%r8d, %r8d
	cmovneq	%rsi, %rax
	movq	(%rax), %rsi
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	je	.LBB25_3
# BB#4:
	movq	32(%r15), %rax
	jmp	.LBB25_5
.LBB25_3:
	xorl	%eax, %eax
.LBB25_5:
	movl	(%r10), %ebp
	movl	%ebp, (%rsi,%r14,4)
	movl	4(%r10), %ebp
	movslq	%r14d, %rbx
	movl	%ebp, 4(%rsi,%rbx,4)
	movl	8(%r10), %ebp
	movl	%ebp, 8(%rsi,%rbx,4)
	leaq	1(%rbx), %rsi
	leaq	2(%rbx), %r13
	testl	%r8d, %r8d
	je	.LBB25_7
# BB#6:
	movss	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	.LCPI25_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	movss	%xmm0, (%rax,%r14,4)
	movss	4(%r10), %xmm0          # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm0
	movss	%xmm0, (%rax,%rsi,4)
	movss	8(%r10), %xmm0          # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB25_8
.LBB25_7:                               # %.critedge
	movss	1152(%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	1156(%rdi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	subss	48(%rdx), %xmm0
	subss	52(%rdx), %xmm1
	movss	1160(%rdi), %xmm2       # xmm2 = mem[0],zero,zero,zero
	subss	56(%rdx), %xmm2
	movss	8(%r10), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm3, %xmm4
	movss	(%r10), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%r10), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm6, %xmm7
	subss	%xmm7, %xmm4
	mulss	%xmm5, %xmm2
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm2
	mulss	%xmm6, %xmm0
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm0
	movq	16(%r15), %rax
	movss	%xmm4, (%rax,%r14,4)
	movss	%xmm2, (%rax,%rsi,4)
	movss	%xmm0, (%rax,%r13,4)
	movss	1152(%rdi), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movss	1156(%rdi), %xmm2       # xmm2 = mem[0],zero,zero,zero
	subss	48(%rcx), %xmm0
	subss	52(%rcx), %xmm2
	movss	1160(%rdi), %xmm1       # xmm1 = mem[0],zero,zero,zero
	subss	56(%rcx), %xmm1
	movss	8(%r10), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm3, %xmm4
	movss	(%r10), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%r10), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm7
	mulss	%xmm6, %xmm7
	subss	%xmm7, %xmm4
	mulss	%xmm5, %xmm1
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm1
	mulss	%xmm6, %xmm0
	mulss	%xmm5, %xmm2
	subss	%xmm2, %xmm0
	movaps	.LCPI25_0(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm4
	xorps	%xmm2, %xmm1
	movq	32(%r15), %rax
	movss	%xmm4, (%rax,%r14,4)
	movss	%xmm1, (%rax,%rsi,4)
.LBB25_8:
	movl	12(%rsp), %ebp          # 4-byte Reload
	xorps	.LCPI25_0(%rip), %xmm0
	movss	%xmm0, (%rax,%r13,4)
	testl	%ebp, %ebp
	je	.LBB25_11
# BB#9:
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%r12), %xmm0
	jne	.LBB25_11
	jp	.LBB25_11
# BB#10:                                # %.thread228
	movq	48(%r15), %rcx
	movl	$0, (%rcx,%r14,4)
	xorps	%xmm0, %xmm0
	jmp	.LBB25_14
.LBB25_11:
	movq	48(%r15), %rcx
	movl	$0, (%rcx,%r14,4)
	testb	%r11b, %r11b
	je	.LBB25_21
# BB#12:
	movq	56(%r15), %rax
	movl	$0, (%rax,%r14,4)
	testl	%ebp, %ebp
	je	.LBB25_18
# BB#13:                                # %..critedge181_crit_edge
	movss	(%rcx,%r14,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB25_14
.LBB25_21:
	xorps	%xmm0, %xmm0
	testl	%ebp, %ebp
	je	.LBB25_22
.LBB25_14:                              # %.critedge181
	testl	%r8d, %r8d
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	28(%r12), %xmm1
	mulss	40(%r12), %xmm1
	je	.LBB25_16
# BB#15:
	xorps	.LCPI25_0(%rip), %xmm1
.LBB25_16:                              # %.critedge181
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rcx,%r14,4)
	movq	56(%r15), %rax
	movl	$0, (%rax,%r14,4)
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	ucomiss	4(%r12), %xmm0
	jne	.LBB25_23
	jp	.LBB25_23
# BB#17:
	movq	64(%r15), %rax
	movl	$-8388609, (%rax,%r14,4) # imm = 0xFF7FFFFF
	movq	72(%r15), %rax
	movl	$2139095039, (%rax,%r14,4) # imm = 0x7F7FFFFF
	movl	$1, %eax
	jmp	.LBB25_35
.LBB25_23:
	cmpl	$1, %ebp
	movq	64(%r15), %rax
	xorps	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	je	.LBB25_25
# BB#24:
	movss	.LCPI25_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
.LBB25_25:
	je	.LBB25_26
# BB#27:
	xorps	%xmm2, %xmm2
	jmp	.LBB25_28
.LBB25_18:                              # %.thread229
	testl	%r8d, %r8d
	movss	8(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	jne	.LBB25_20
# BB#19:                                # %.thread229
	xorps	.LCPI25_0(%rip), %xmm3
.LBB25_20:                              # %.thread229
	movss	44(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	(%r15), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	4(%r15), %xmm4
	callq	_ZN17btTypedConstraint14getMotorFactorEfffff
	mulss	8(%r12), %xmm0
	movq	48(%r15), %rax
	addss	(%rax,%r14,4), %xmm0
	movss	%xmm0, (%rax,%r14,4)
	movss	12(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI25_0(%rip), %xmm0
	movq	64(%r15), %rax
	movss	%xmm0, (%rax,%r14,4)
	movl	12(%r12), %eax
	movq	72(%r15), %rcx
	movl	%eax, (%rcx,%r14,4)
	movl	$1, %eax
	jmp	.LBB25_35
.LBB25_26:
	movss	.LCPI25_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
.LBB25_28:
	movss	%xmm0, (%rax,%r14,4)
	movq	72(%r15), %rax
	movss	%xmm2, (%rax,%r14,4)
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movl	$1, %eax
	ucomiss	%xmm1, %xmm0
	jbe	.LBB25_35
# BB#29:
	testl	%r8d, %r8d
	movq	16(%rsp), %rdx          # 8-byte Reload
	cmovneq	80(%rsp), %rdx
	cmovneq	88(%rsp), %r9
	movss	(%r9), %xmm1            # xmm1 = mem[0],zero,zero,zero
	movss	4(%r9), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movss	(%r10), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r10), %xmm5          # xmm5 = mem[0],zero,zero,zero
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm3, %xmm1
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm4, %xmm5
	addps	%xmm1, %xmm5
	movss	8(%r9), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movss	8(%r10), %xmm1          # xmm1 = mem[0],zero,zero,zero
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	addps	%xmm5, %xmm1
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	cmpl	$1, %ebp
	jne	.LBB25_32
# BB#30:
	ucomiss	%xmm1, %xmm2
	jbe	.LBB25_35
# BB#31:
	mulss	%xmm1, %xmm0
	xorps	.LCPI25_0(%rip), %xmm0
	ucomiss	(%rcx,%r14,4), %xmm0
	ja	.LBB25_34
	jmp	.LBB25_35
.LBB25_22:
	movl	$1, %eax
	jmp	.LBB25_35
.LBB25_32:
	ucomiss	%xmm2, %xmm1
	jbe	.LBB25_35
# BB#33:
	mulss	%xmm1, %xmm0
	xorps	.LCPI25_0(%rip), %xmm0
	movss	(%rcx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB25_35
.LBB25_34:
	movss	%xmm0, (%rcx,%r14,4)
.LBB25_35:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i, .Lfunc_end25-_ZN23btGeneric6DofConstraint21get_limit_motor_info2EP22btRotationalLimitMotorRK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_i
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI26_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f,@function
_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f: # @_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi146:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi148:
	.cfi_def_cfa_offset 144
.Lcfi149:
	.cfi_offset %rbx, -56
.Lcfi150:
	.cfi_offset %r12, -48
.Lcfi151:
	.cfi_offset %r13, -40
.Lcfi152:
	.cfi_offset %r14, -32
.Lcfi153:
	.cfi_offset %r15, -24
.Lcfi154:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %rbx
	cmpb	$0, 1265(%rbx)
	je	.LBB26_16
# BB#1:
	movss	%xmm0, 1036(%rbx)
	leaq	1040(%rbx), %r13
	movups	1088(%rbx), %xmm1
	movaps	%xmm1, 64(%rsp)
	leaq	1104(%rbx), %rcx
	movups	1152(%rbx), %xmm1
	movaps	%xmm1, 48(%rsp)
	leaq	728(%rbx), %r12
	leaq	1248(%rbx), %r11
	movss	744(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	728(%rbx), %xmm1
	jb	.LBB26_3
# BB#2:
	movss	.LCPI26_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	304(%rbx), %xmm1
	cmpb	$0, 1264(%rbx)
	movq	%r13, %rax
	cmoveq	%rcx, %rax
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movlps	%xmm2, 16(%rsp)
	movlps	%xmm3, 24(%rsp)
	movq	24(%rbx), %rsi
	movq	32(%rbx), %r8
	leaq	16(%rsp), %rax
	leaq	48(%rsp), %r10
	movq	%r15, %r14
	movq	%rcx, %r15
	leaq	64(%rsp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	8(%rsp), %r9            # 8-byte Reload
	pushq	%r11
.Lcfi155:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi156:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi157:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi158:
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rbp
	movq	%r11, %r12
	callq	_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyR12btSolverBodyRK9btVector3S1_S3_S6_iS6_S6_
	movq	%r15, %rcx
	movq	%r14, %r15
	movq	%r12, %r11
	movq	%rbp, %r12
	addq	$32, %rsp
.Lcfi159:
	.cfi_adjust_cfa_offset -32
.LBB26_3:
	movss	748(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	732(%rbx), %xmm0
	jb	.LBB26_5
# BB#4:
	movss	.LCPI26_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	388(%rbx), %xmm1
	cmpb	$0, 1264(%rbx)
	movq	%r13, %rax
	cmoveq	%rcx, %rax
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	20(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movss	36(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	movss	1036(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movq	24(%rbx), %rsi
	movq	32(%rbx), %r8
	leaq	16(%rsp), %rax
	leaq	48(%rsp), %r10
	movq	%rcx, %r14
	leaq	64(%rsp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	8(%rsp), %r9            # 8-byte Reload
	pushq	%r11
.Lcfi160:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi161:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi162:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi163:
	.cfi_adjust_cfa_offset 8
	movq	%r11, %rbp
	callq	_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyR12btSolverBodyRK9btVector3S1_S3_S6_iS6_S6_
	movq	%r14, %rcx
	movq	%rbp, %r11
	addq	$32, %rsp
.Lcfi164:
	.cfi_adjust_cfa_offset -32
.LBB26_5:
	movss	752(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	736(%rbx), %xmm0
	jb	.LBB26_7
# BB#6:
	movss	.LCPI26_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	472(%rbx), %xmm1
	cmpb	$0, 1264(%rbx)
	cmoveq	%rcx, %r13
	movss	8(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	24(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movss	40(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	movss	1036(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movq	24(%rbx), %rsi
	movq	32(%rbx), %r8
	leaq	16(%rsp), %rax
	leaq	48(%rsp), %rbp
	leaq	64(%rsp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	8(%rsp), %r9            # 8-byte Reload
	pushq	%r11
.Lcfi165:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi168:
	.cfi_adjust_cfa_offset 8
	callq	_ZN25btTranslationalLimitMotor15solveLinearAxisEffR11btRigidBodyR12btSolverBodyRK9btVector3S1_S3_S6_iS6_S6_
	addq	$32, %rsp
.Lcfi169:
	.cfi_adjust_cfa_offset -32
.LBB26_7:
	cmpl	$0, 916(%rbx)
	jne	.LBB26_9
# BB#8:
	cmpb	$0, 904(%rbx)
	je	.LBB26_10
.LBB26_9:
	leaq	868(%rbx), %rdi
	movups	1184(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)
	movss	.LCPI26_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	556(%rbx), %xmm1
	movss	1036(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movq	24(%rbx), %rdx
	movq	32(%rbx), %r8
	leaq	32(%rsp), %rsi
	movq	%r15, %rcx
	movq	8(%rsp), %r9            # 8-byte Reload
	callq	_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyR12btSolverBodyS3_S5_
.LBB26_10:                              # %_ZN22btRotationalLimitMotor16needApplyTorquesEv.exit
	cmpl	$0, 972(%rbx)
	jne	.LBB26_12
# BB#11:
	cmpb	$0, 960(%rbx)
	je	.LBB26_13
.LBB26_12:
	leaq	924(%rbx), %rdi
	movups	1200(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)
	movss	.LCPI26_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	640(%rbx), %xmm1
	movss	1036(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movq	24(%rbx), %rdx
	movq	32(%rbx), %r8
	leaq	32(%rsp), %rsi
	movq	%r15, %rcx
	movq	8(%rsp), %r9            # 8-byte Reload
	callq	_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyR12btSolverBodyS3_S5_
.LBB26_13:                              # %_ZN22btRotationalLimitMotor16needApplyTorquesEv.exit.1
	cmpl	$0, 1028(%rbx)
	jne	.LBB26_15
# BB#14:
	cmpb	$0, 1016(%rbx)
	je	.LBB26_16
.LBB26_15:
	movups	1216(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)
	movss	.LCPI26_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	724(%rbx), %xmm1
	movss	1036(%rbx), %xmm0       # xmm0 = mem[0],zero,zero,zero
	movq	24(%rbx), %rdx
	movq	32(%rbx), %r8
	leaq	980(%rbx), %rdi
	leaq	32(%rsp), %rsi
	movq	%r15, %rcx
	movq	8(%rsp), %r9            # 8-byte Reload
	callq	_ZN22btRotationalLimitMotor18solveAngularLimitsEfR9btVector3fP11btRigidBodyR12btSolverBodyS3_S5_
.LBB26_16:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f, .Lfunc_end26-_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_endproc

	.globl	_ZN23btGeneric6DofConstraint9updateRHSEf
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint9updateRHSEf,@function
_ZN23btGeneric6DofConstraint9updateRHSEf: # @_ZN23btGeneric6DofConstraint9updateRHSEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end27:
	.size	_ZN23btGeneric6DofConstraint9updateRHSEf, .Lfunc_end27-_ZN23btGeneric6DofConstraint9updateRHSEf
	.cfi_endproc

	.globl	_ZNK23btGeneric6DofConstraint24getRelativePivotPositionEi
	.p2align	4, 0x90
	.type	_ZNK23btGeneric6DofConstraint24getRelativePivotPositionEi,@function
_ZNK23btGeneric6DofConstraint24getRelativePivotPositionEi: # @_ZNK23btGeneric6DofConstraint24getRelativePivotPositionEi
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movss	1232(%rdi,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end28:
	.size	_ZNK23btGeneric6DofConstraint24getRelativePivotPositionEi, .Lfunc_end28-_ZNK23btGeneric6DofConstraint24getRelativePivotPositionEi
	.cfi_endproc

	.globl	_ZNK23btGeneric6DofConstraint8getAngleEi
	.p2align	4, 0x90
	.type	_ZNK23btGeneric6DofConstraint8getAngleEi,@function
_ZNK23btGeneric6DofConstraint8getAngleEi: # @_ZNK23btGeneric6DofConstraint8getAngleEi
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movss	1168(%rdi,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end29:
	.size	_ZNK23btGeneric6DofConstraint8getAngleEi, .Lfunc_end29-_ZNK23btGeneric6DofConstraint8getAngleEi
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI30_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZN23btGeneric6DofConstraint13calcAnchorPosEv
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraint13calcAnchorPosEv,@function
_ZN23btGeneric6DofConstraint13calcAnchorPosEv: # @_ZN23btGeneric6DofConstraint13calcAnchorPosEv
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	movss	360(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI30_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm2
	movaps	%xmm0, %xmm1
	jne	.LBB30_1
	jnp	.LBB30_2
.LBB30_1:
	movq	24(%rdi), %rax
	movss	360(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	divss	%xmm2, %xmm1
.LBB30_2:
	movsd	1088(%rdi), %xmm2       # xmm2 = mem[0],zero
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	subss	%xmm1, %xmm0
	mulss	1096(%rdi), %xmm1
	movsd	1152(%rdi), %xmm2       # xmm2 = mem[0],zero
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	mulss	1160(%rdi), %xmm0
	addps	%xmm3, %xmm4
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, 1248(%rdi)
	movlps	%xmm1, 1256(%rdi)
	retq
.Lfunc_end30:
	.size	_ZN23btGeneric6DofConstraint13calcAnchorPosEv, .Lfunc_end30-_ZN23btGeneric6DofConstraint13calcAnchorPosEv
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD2Ev,"axG",@progbits,_ZN17btTypedConstraintD2Ev,comdat
	.weak	_ZN17btTypedConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD2Ev,@function
_ZN17btTypedConstraintD2Ev:             # @_ZN17btTypedConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end31:
	.size	_ZN17btTypedConstraintD2Ev, .Lfunc_end31-_ZN17btTypedConstraintD2Ev
	.cfi_endproc

	.section	.text._ZN23btGeneric6DofConstraintD0Ev,"axG",@progbits,_ZN23btGeneric6DofConstraintD0Ev,comdat
	.weak	_ZN23btGeneric6DofConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN23btGeneric6DofConstraintD0Ev,@function
_ZN23btGeneric6DofConstraintD0Ev:       # @_ZN23btGeneric6DofConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end32:
	.size	_ZN23btGeneric6DofConstraintD0Ev, .Lfunc_end32-_ZN23btGeneric6DofConstraintD0Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end33:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end33-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.type	_ZTV23btGeneric6DofConstraint,@object # @_ZTV23btGeneric6DofConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTV23btGeneric6DofConstraint
	.p2align	3
_ZTV23btGeneric6DofConstraint:
	.quad	0
	.quad	_ZTI23btGeneric6DofConstraint
	.quad	_ZN17btTypedConstraintD2Ev
	.quad	_ZN23btGeneric6DofConstraintD0Ev
	.quad	_ZN23btGeneric6DofConstraint13buildJacobianEv
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.quad	_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.quad	_ZN23btGeneric6DofConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.quad	_ZN23btGeneric6DofConstraint13calcAnchorPosEv
	.size	_ZTV23btGeneric6DofConstraint, 80

	.type	_ZTS23btGeneric6DofConstraint,@object # @_ZTS23btGeneric6DofConstraint
	.globl	_ZTS23btGeneric6DofConstraint
	.p2align	4
_ZTS23btGeneric6DofConstraint:
	.asciz	"23btGeneric6DofConstraint"
	.size	_ZTS23btGeneric6DofConstraint, 26

	.type	_ZTS17btTypedConstraint,@object # @_ZTS17btTypedConstraint
	.section	.rodata._ZTS17btTypedConstraint,"aG",@progbits,_ZTS17btTypedConstraint,comdat
	.weak	_ZTS17btTypedConstraint
	.p2align	4
_ZTS17btTypedConstraint:
	.asciz	"17btTypedConstraint"
	.size	_ZTS17btTypedConstraint, 20

	.type	_ZTS13btTypedObject,@object # @_ZTS13btTypedObject
	.section	.rodata._ZTS13btTypedObject,"aG",@progbits,_ZTS13btTypedObject,comdat
	.weak	_ZTS13btTypedObject
_ZTS13btTypedObject:
	.asciz	"13btTypedObject"
	.size	_ZTS13btTypedObject, 16

	.type	_ZTI13btTypedObject,@object # @_ZTI13btTypedObject
	.section	.rodata._ZTI13btTypedObject,"aG",@progbits,_ZTI13btTypedObject,comdat
	.weak	_ZTI13btTypedObject
	.p2align	3
_ZTI13btTypedObject:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13btTypedObject
	.size	_ZTI13btTypedObject, 16

	.type	_ZTI17btTypedConstraint,@object # @_ZTI17btTypedConstraint
	.section	.rodata._ZTI17btTypedConstraint,"aG",@progbits,_ZTI17btTypedConstraint,comdat
	.weak	_ZTI17btTypedConstraint
	.p2align	4
_ZTI17btTypedConstraint:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17btTypedConstraint
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	_ZTI13btTypedObject
	.quad	2050                    # 0x802
	.size	_ZTI17btTypedConstraint, 40

	.type	_ZTI23btGeneric6DofConstraint,@object # @_ZTI23btGeneric6DofConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTI23btGeneric6DofConstraint
	.p2align	4
_ZTI23btGeneric6DofConstraint:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23btGeneric6DofConstraint
	.quad	_ZTI17btTypedConstraint
	.size	_ZTI23btGeneric6DofConstraint, 24


	.globl	_ZN23btGeneric6DofConstraintC1Ev
	.type	_ZN23btGeneric6DofConstraintC1Ev,@function
_ZN23btGeneric6DofConstraintC1Ev = _ZN23btGeneric6DofConstraintC2Ev
	.globl	_ZN23btGeneric6DofConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b
	.type	_ZN23btGeneric6DofConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b,@function
_ZN23btGeneric6DofConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b = _ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
