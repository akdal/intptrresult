	.text
	.file	"me_umhexsmp.bc"
	.globl	smpUMHEX_init
	.p2align	4, 0x90
	.type	smpUMHEX_init,@function
smpUMHEX_init:                          # @smpUMHEX_init
	.cfi_startproc
# BB#0:
	movw	$800, SymmetricalCrossSearchThreshold1(%rip) # imm = 0x320
	movw	$7000, SymmetricalCrossSearchThreshold2(%rip) # imm = 0x1B58
	movw	$1000, ConvergeThreshold(%rip) # imm = 0x3E8
	movw	$1000, SubPelThreshold1(%rip) # imm = 0x3E8
	movw	$400, SubPelThreshold3(%rip) # imm = 0x190
	retq
.Lfunc_end0:
	.size	smpUMHEX_init, .Lfunc_end0-smpUMHEX_init
	.cfi_endproc

	.globl	smpUMHEX_get_mem
	.p2align	4, 0x90
	.type	smpUMHEX_get_mem,@function
smpUMHEX_get_mem:                       # @smpUMHEX_get_mem
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rbx
	movl	52(%rbx), %eax
	sarl	$4, %eax
	incl	%eax
	movslq	%eax, %rdi
	movl	$1, %esi
	callq	calloc
	movq	%rax, smpUMHEX_flag_intra(%rip)
	testq	%rax, %rax
	jne	.LBB1_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
	movq	img(%rip), %rbx
.LBB1_2:
	movl	52(%rbx), %eax
	movl	68(%rbx), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%ecx, %edx
	sarl	$2, %edx
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movl	$smpUMHEX_l0_cost, %edi
	movl	$9, %esi
	callq	get_mem3Dint
	movl	%eax, %ebx
	movq	img(%rip), %rax
	movl	52(%rax), %esi
	movl	68(%rax), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%eax, %edx
	sarl	$2, %edx
	movl	%esi, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%esi, %ecx
	sarl	$2, %ecx
	movl	$smpUMHEX_l1_cost, %edi
	movl	$9, %esi
	callq	get_mem3Dint
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$smpUMHEX_SearchState, %edi
	movl	$7, %esi
	movl	$7, %edx
	callq	get_mem2D
	addl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	smpUMHEX_get_mem, .Lfunc_end1-smpUMHEX_get_mem
	.cfi_endproc

	.globl	smpUMHEX_free_mem
	.p2align	4, 0x90
	.type	smpUMHEX_free_mem,@function
smpUMHEX_free_mem:                      # @smpUMHEX_free_mem
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movq	smpUMHEX_l0_cost(%rip), %rdi
	movl	$9, %esi
	callq	free_mem3Dint
	movq	smpUMHEX_l1_cost(%rip), %rdi
	movl	$9, %esi
	callq	free_mem3Dint
	movq	smpUMHEX_SearchState(%rip), %rdi
	callq	free_mem2D
	movq	smpUMHEX_flag_intra(%rip), %rdi
	popq	%rax
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	smpUMHEX_free_mem, .Lfunc_end2-smpUMHEX_free_mem
	.cfi_endproc

	.globl	smpUMHEXIntegerPelBlockMotionSearch
	.p2align	4, 0x90
	.type	smpUMHEXIntegerPelBlockMotionSearch,@function
smpUMHEXIntegerPelBlockMotionSearch:    # @smpUMHEXIntegerPelBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 256
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%r9d, 88(%rsp)          # 4-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	280(%rsp), %r9
	movq	272(%rsp), %r10
	movq	img(%rip), %rax
	xorl	%r11d, %r11d
	cmpl	$0, 15268(%rax)
	je	.LBB3_3
# BB#1:
	movq	14224(%rax), %rbp
	movslq	12(%rax), %rdi
	imulq	$536, %rdi, %rbx        # imm = 0x218
	cmpl	$0, 424(%rbp,%rbx)
	je	.LBB3_3
# BB#2:
	andl	$1, %edi
	leal	2(%rdi,%rdi), %r11d
.LBB3_3:
	movzwl	264(%rsp), %r12d
	movq	input(%rip), %rdi
	movslq	88(%rsp), %rbp          # 4-byte Folded Reload
	movl	76(%rdi,%rbp,8), %ebx
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	72(%rdi,%rbp,8), %ebx
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movswl	(%r10), %ebp
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movswl	(%r9), %ebp
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	active_pps(%rip), %rbp
	cmpl	$0, 192(%rbp)
	je	.LBB3_6
# BB#4:
	movl	20(%rax), %ebx
	testl	%ebx, %ebx
	je	.LBB3_8
# BB#5:
	cmpl	$3, %ebx
	je	.LBB3_8
.LBB3_6:
	cmpl	$0, 196(%rbp)
	je	.LBB3_10
# BB#7:
	cmpl	$1, 20(%rax)
	jne	.LBB3_10
.LBB3_8:
	cmpl	$0, 2936(%rdi)
	setne	%dil
	jmp	.LBB3_11
.LBB3_10:
	xorl	%edi, %edi
.LBB3_11:
	movswl	256(%rsp), %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movswl	%r12w, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movzbl	%dil, %eax
	leal	(%rax,%rax,2), %r10d
	movl	%r10d, dist_method(%rip)
	addl	%edx, %r11d
	movslq	%r11d, %rbp
	movq	listX(,%rbp,8), %rdx
	movswq	%si, %rsi
	movq	(%rdx,%rsi,8), %rbx
	movq	%rbx, ref_pic_ptr(%rip)
	movq	6448(%rbx), %rdx
	movq	%rdx, ref_pic_sub(%rip)
	movl	6392(%rbx), %r11d
	movw	%r11w, img_width(%rip)
	movl	6396(%rbx), %r9d
	movw	%r9w, img_height(%rip)
	movl	6408(%rbx), %edx
	movl	%edx, width_pad(%rip)
	movl	6412(%rbx), %edx
	movl	%edx, height_pad(%rip)
	testb	%al, %al
	je	.LBB3_13
# BB#12:
	movq	wp_weight(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rsi,8), %rax
	movl	(%rax), %eax
	movl	%eax, weight_luma(%rip)
	movq	wp_offset(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rsi,8), %rax
	movl	(%rax), %eax
	movl	%eax, offset_luma(%rip)
.LBB3_13:
	movl	288(%rsp), %edx
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB3_16
# BB#14:
	movq	6464(%rbx), %rax
	movq	(%rax), %rdx
	movq	%rdx, ref_pic_sub+8(%rip)
	movl	288(%rsp), %edx
	movq	8(%rax), %rax
	movq	%rax, ref_pic_sub+16(%rip)
	movl	6416(%rbx), %eax
	movl	%eax, width_pad_cr(%rip)
	movl	6420(%rbx), %eax
	movl	%eax, height_pad_cr(%rip)
	testb	%dil, %dil
	je	.LBB3_16
# BB#15:
	movq	wp_weight(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rsi,8), %rax
	movl	4(%rax), %edx
	movl	%edx, weight_cr(%rip)
	movl	8(%rax), %eax
	movl	%eax, weight_cr+4(%rip)
	movq	wp_offset(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rsi,8), %rax
	movl	4(%rax), %edx
	movl	%edx, offset_cr(%rip)
	movl	288(%rsp), %edx
	movl	8(%rax), %eax
	movl	%eax, offset_cr+4(%rip)
.LBB3_16:
	movl	296(%rsp), %r13d
	movq	128(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx,4), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movq	136(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r8,4), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r8), %eax
	movl	$1, %esi
	movq	40(%rsp), %rdi          # 8-byte Reload
	cmpl	%edx, %edi
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jle	.LBB3_20
# BB#17:
	cmpl	%edx, %eax
	jle	.LBB3_20
# BB#18:
	movswl	%r11w, %eax
	notl	%edx
	movl	%edx, %edi
	subl	12(%rsp), %edi          # 4-byte Folded Reload
	addl	%eax, %edi
	cmpl	%edi, 40(%rsp)          # 4-byte Folded Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	jge	.LBB3_20
# BB#19:
	movswl	%r9w, %eax
	subl	8(%rsp), %edx           # 4-byte Folded Reload
	addl	%eax, %edx
	xorl	%esi, %esi
	cmpl	%edx, 56(%rsp)          # 4-byte Folded Reload
	setge	%sil
.LBB3_20:
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	leal	(,%rcx,4), %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%r8, 104(%rsp)          # 8-byte Spill
	leal	(,%r8,4), %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	%esi, ref_access_method(%rip)
	movq	mvbits(%rip), %rax
	leal	(,%rdi,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	56(%rsp), %rbp          # 8-byte Reload
	leal	(,%rbp,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	imull	304(%rsp), %ebx
	sarl	$16, %ebx
	movl	%r10d, %eax
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	leal	80(,%rdi,4), %r8d
	leal	80(,%rbp,4), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	%rbp, %r9
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	%eax, %r10d
	addl	%ebx, %r10d
	xorl	%r14d, %r14d
	cmpl	%r13d, %r10d
	movl	$0, %r15d
	cmovll	%r8d, %r15d
	cmovll	%r9d, %r14d
	cmovgl	%r13d, %r10d
	orw	256(%rsp), %r12w
	je	.LBB3_23
# BB#21:
	movq	144(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %eax
	negl	%eax
	testw	%cx, %cx
	cmovgl	%ecx, %eax
	movl	288(%rsp), %ecx
	cmpl	%ecx, %eax
	jg	.LBB3_23
# BB#22:
	movq	120(%rsp), %rdx         # 8-byte Reload
	movl	%edx, %eax
	negl	%eax
	testw	%dx, %dx
	cmovgl	%edx, %eax
	cmpl	%ecx, %eax
	jle	.LBB3_41
.LBB3_23:
	movl	%r15d, %r13d
	movl	%r14d, %r11d
.LBB3_24:
	movq	32(%rsp), %rcx          # 8-byte Reload
	movzwl	ConvergeThreshold(%rip), %eax
	movswl	block_type_shift_factor(%rcx,%rcx), %ecx
	shrl	%cl, %eax
	cmpl	%eax, %r10d
	movl	%r14d, 32(%rsp)         # 4-byte Spill
	movl	%r15d, 48(%rsp)         # 4-byte Spill
	jge	.LBB3_30
# BB#25:                                # %.preheader.preheader
	movq	$-8, %rbp
	movl	288(%rsp), %edx
	.p2align	4, 0x90
.LBB3_26:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movswl	Diamond_X+8(%rbp), %ebx
	addl	%r15d, %ebx
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_29
# BB#27:                                #   in Loop: Header=BB3_26 Depth=1
	movswl	Diamond_Y+8(%rbp), %r12d
	addl	%r14d, %r12d
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_29
# BB#28:                                #   in Loop: Header=BB3_26 Depth=1
	movq	mvbits(%rip), %rax
	leal	(,%rbx,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leal	(,%r12,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	(%rax,%rdx,4), %r13d
	addl	(%rax,%rcx,4), %r13d
	imull	304(%rsp), %r13d
	sarl	$16, %r13d
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	movl	%r10d, %r15d
	leal	80(,%rbx,4), %r8d
	movq	%r11, %r14
	leal	80(,%r12,4), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	%r14, %r11
	movl	32(%rsp), %r14d         # 4-byte Reload
	movl	%r15d, %r10d
	movl	48(%rsp), %r15d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%r13d, %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	cmpl	%r10d, %eax
	cmovll	%ebx, %r13d
	cmovll	%r12d, %r11d
	cmovlel	%eax, %r10d
.LBB3_29:                               #   in Loop: Header=BB3_26 Depth=1
	addq	$2, %rbp
	jne	.LBB3_26
	jmp	.LBB3_115
.LBB3_30:                               # %.preheader742.preheader
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movq	$-8, %rbp
	movl	288(%rsp), %edx
	.p2align	4, 0x90
.LBB3_31:                               # %.preheader742
                                        # =>This Inner Loop Header: Depth=1
	movswl	Diamond_X+8(%rbp), %ebx
	addl	%r15d, %ebx
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_34
# BB#32:                                #   in Loop: Header=BB3_31 Depth=1
	movswl	Diamond_Y+8(%rbp), %r12d
	addl	%r14d, %r12d
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_34
# BB#33:                                #   in Loop: Header=BB3_31 Depth=1
	movq	mvbits(%rip), %rax
	leal	(,%rbx,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leal	(,%r12,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	(%rax,%rdx,4), %r13d
	addl	(%rax,%rcx,4), %r13d
	imull	304(%rsp), %r13d
	sarl	$16, %r13d
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	movl	%r10d, %r15d
	leal	80(,%rbx,4), %r8d
	movq	%r11, %r14
	leal	80(,%r12,4), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	%r14, %r11
	movl	32(%rsp), %r14d         # 4-byte Reload
	movl	%r15d, %r10d
	movl	48(%rsp), %r15d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%r13d, %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	cmpl	%r10d, %eax
	cmovll	%ebx, %r13d
	cmovll	%r12d, %r11d
	cmovlel	%eax, %r10d
.LBB3_34:                               #   in Loop: Header=BB3_31 Depth=1
	addq	$2, %rbp
	jne	.LBB3_31
# BB#35:
	cmpl	$1, 88(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_37
# BB#36:
	movzwl	SymmetricalCrossSearchThreshold1(%rip), %eax
	movl	84(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	cmpl	%eax, %r10d
	jg	.LBB3_38
.LBB3_37:
	movzwl	SymmetricalCrossSearchThreshold2(%rip), %eax
	movl	84(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	cmpl	%eax, %r10d
	jle	.LBB3_40
.LBB3_38:                               # %.preheader741
	cmpl	$2, %edx
	jge	.LBB3_42
# BB#39:
	movl	%r13d, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	%r11d, %r14d
	jmp	.LBB3_56
.LBB3_40:
	movq	%r13, 16(%rsp)          # 8-byte Spill
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jge	.LBB3_71
	jmp	.LBB3_73
.LBB3_41:
	movq	mvbits(%rip), %rax
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	negl	%ecx
	movslq	%ecx, %rcx
	movq	136(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	negl	%edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	imull	304(%rsp), %ebx
	sarl	$16, %ebx
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%ebx, %ecx
	movl	%r10d, %ebp
	movq	168(%rsp), %rdx         # 8-byte Reload
	leal	80(%rdx), %r8d
	movq	160(%rsp), %rdx         # 8-byte Reload
	leal	80(%rdx), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	%ebp, %r10d
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	addl	%ebx, %eax
	cmpl	%r10d, %eax
	movl	%r15d, %r13d
	cmovll	112(%rsp), %r13d        # 4-byte Folded Reload
	movl	%r14d, %r11d
	cmovll	104(%rsp), %r11d        # 4-byte Folded Reload
	cmovlel	%eax, %r10d
	jmp	.LBB3_24
.LBB3_42:                               # %.lr.ph
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, 92(%rsp)          # 4-byte Spill
	movl	%r13d, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, 156(%rsp)         # 4-byte Spill
	leal	(,%r13,4), %eax
	subl	28(%rsp), %eax          # 4-byte Folded Reload
	cltq
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leal	80(,%r13,4), %eax
	movl	%eax, 96(%rsp)          # 4-byte Spill
	movl	%r11d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	leal	(,%r11,4), %eax
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	cltq
	movq	%rax, 184(%rsp)         # 8-byte Spill
	leal	80(,%r11,4), %eax
	movl	%eax, 100(%rsp)         # 4-byte Spill
	movl	$1, %eax
	movw	$2, %di
	movl	%r11d, %r14d
	movl	%r13d, %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movq	%r11, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_43:                               # =>This Inner Loop Header: Depth=1
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leal	-1(%rax,%rax), %r15d
	leal	-1(%r13,%rax,2), %ebx
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_46
# BB#44:                                #   in Loop: Header=BB3_43 Depth=1
	cmpl	%edx, 72(%rsp)          # 4-byte Folded Reload
	jg	.LBB3_46
# BB#45:                                #   in Loop: Header=BB3_43 Depth=1
	movq	mvbits(%rip), %rax
	leal	(,%rbx,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	184(%rsp), %rdx         # 8-byte Reload
	movl	(%rax,%rdx,4), %ebp
	addl	(%rax,%rcx,4), %ebp
	imull	304(%rsp), %ebp
	sarl	$16, %ebp
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%ebp, %ecx
	movl	%r10d, %r12d
	leal	80(,%rbx,4), %r8d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	100(%rsp), %r9d         # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	48(%rsp), %r11          # 8-byte Reload
	movl	%r12d, %r10d
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%ebp, %eax
	movl	16(%rsp), %ecx          # 4-byte Reload
	cmpl	%r10d, %eax
	cmovll	%ebx, %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	cmovll	%r11d, %r14d
	cmovlel	%eax, %r10d
.LBB3_46:                               #   in Loop: Header=BB3_43 Depth=1
	movl	%r13d, %ebx
	subl	%r15d, %ebx
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_49
# BB#47:                                #   in Loop: Header=BB3_43 Depth=1
	cmpl	%edx, 72(%rsp)          # 4-byte Folded Reload
	jg	.LBB3_49
# BB#48:                                #   in Loop: Header=BB3_43 Depth=1
	movq	mvbits(%rip), %rax
	leal	(,%rbx,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	184(%rsp), %rdx         # 8-byte Reload
	movl	(%rax,%rdx,4), %ebp
	addl	(%rax,%rcx,4), %ebp
	imull	304(%rsp), %ebp
	sarl	$16, %ebp
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%ebp, %ecx
	movl	%r10d, %r12d
	leal	80(,%rbx,4), %r8d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	100(%rsp), %r9d         # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	48(%rsp), %r11          # 8-byte Reload
	movl	%r12d, %r10d
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%ebp, %eax
	movl	16(%rsp), %ecx          # 4-byte Reload
	cmpl	%r10d, %eax
	cmovll	%ebx, %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	cmovll	%r11d, %r14d
	cmovlel	%eax, %r10d
.LBB3_49:                               #   in Loop: Header=BB3_43 Depth=1
	cmpl	%edx, 156(%rsp)         # 4-byte Folded Reload
	jg	.LBB3_55
# BB#50:                                #   in Loop: Header=BB3_43 Depth=1
	leal	(%r15,%r11), %ebx
	movl	%ebx, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_52
# BB#51:                                #   in Loop: Header=BB3_43 Depth=1
	movq	mvbits(%rip), %rax
	leal	(,%rbx,4), %ecx
	subl	24(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movl	(%rax,%rcx,4), %ebp
	movq	176(%rsp), %rcx         # 8-byte Reload
	addl	(%rax,%rcx,4), %ebp
	imull	304(%rsp), %ebp
	sarl	$16, %ebp
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%ebp, %ecx
	leal	80(,%rbx,4), %r9d
	movq	%r15, 192(%rsp)         # 8-byte Spill
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%r10d, %r12d
	movl	96(%rsp), %r8d          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	192(%rsp), %r15         # 8-byte Reload
	movq	48(%rsp), %r11          # 8-byte Reload
	movl	%r12d, %r10d
	movq	56(%rsp), %r9           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%ebp, %eax
	movl	16(%rsp), %ecx          # 4-byte Reload
	cmpl	%r10d, %eax
	cmovll	%r13d, %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	cmovll	%ebx, %r14d
	cmovlel	%eax, %r10d
.LBB3_52:                               #   in Loop: Header=BB3_43 Depth=1
	movl	%r11d, %ebx
	subl	%r15d, %ebx
	movl	%ebx, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_54
# BB#53:                                #   in Loop: Header=BB3_43 Depth=1
	movq	mvbits(%rip), %rax
	leal	(,%rbx,4), %ecx
	subl	24(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movl	(%rax,%rcx,4), %ebp
	movq	176(%rsp), %rcx         # 8-byte Reload
	addl	(%rax,%rcx,4), %ebp
	imull	304(%rsp), %ebp
	sarl	$16, %ebp
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%ebp, %ecx
	leal	80(,%rbx,4), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%r10d, %r15d
	movl	96(%rsp), %r8d          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	48(%rsp), %r11          # 8-byte Reload
	movl	%r15d, %r10d
	movq	56(%rsp), %r9           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%ebp, %eax
	movl	16(%rsp), %ecx          # 4-byte Reload
	cmpl	%r10d, %eax
	cmovll	%r13d, %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	cmovll	%ebx, %r14d
	cmovlel	%eax, %r10d
.LBB3_54:                               # %.thread726
                                        #   in Loop: Header=BB3_43 Depth=1
	movq	40(%rsp), %r8           # 8-byte Reload
.LBB3_55:                               # %.thread726
                                        #   in Loop: Header=BB3_43 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movzwl	%di, %eax
	leal	1(%rdi), %edi
	cmpl	92(%rsp), %eax          # 4-byte Folded Reload
	jle	.LBB3_43
.LBB3_56:                               # %.preheader740
	movq	$-12, %rbx
	movl	%r14d, %edi
	movl	16(%rsp), %ebp          # 4-byte Reload
	.p2align	4, 0x90
.LBB3_57:                               # =>This Inner Loop Header: Depth=1
	movswl	Hexagon_X+12(%rbx), %r15d
	addl	16(%rsp), %r15d         # 4-byte Folded Reload
	movl	%r15d, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_60
# BB#58:                                #   in Loop: Header=BB3_57 Depth=1
	movswl	Hexagon_Y+12(%rbx), %r12d
	addl	%r14d, %r12d
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_60
# BB#59:                                #   in Loop: Header=BB3_57 Depth=1
	movq	mvbits(%rip), %rax
	leal	(,%r15,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leal	(,%r12,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r13d
	addl	(%rax,%rcx,4), %r13d
	imull	304(%rsp), %r13d
	sarl	$16, %r13d
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	movl	%r14d, %ebp
	leal	80(,%r15,4), %r8d
	leal	80(,%r12,4), %r9d
	movl	%edi, %r14d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	%r14d, %edi
	movl	%ebp, %r14d
	movl	48(%rsp), %ebp          # 4-byte Reload
	movl	32(%rsp), %r10d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%r13d, %eax
	cmpl	%r10d, %eax
	cmovll	%r15d, %ebp
	cmovll	%r12d, %edi
	cmovlel	%eax, %r10d
.LBB3_60:                               #   in Loop: Header=BB3_57 Depth=1
	addq	$2, %rbx
	jne	.LBB3_57
# BB#61:                                # %.preheader738
	cmpl	$4, %edx
	jge	.LBB3_63
# BB#62:
	movl	%ebp, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%edi, %r11d
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jge	.LBB3_71
	jmp	.LBB3_73
.LBB3_63:                               # %.preheader737.preheader
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%edx, %eax
	sarl	$2, %eax
	movl	%eax, 92(%rsp)          # 4-byte Spill
	movl	$1, %esi
	movl	%edi, %r11d
	movl	%ebp, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_64:                               # %.preheader737
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_65 Depth 2
	movq	$-32, %r13
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_65:                               #   Parent Loop BB3_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	Big_Hexagon_X+32(%r13), %ebx
	imull	%esi, %ebx
	addl	%ebp, %ebx
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_68
# BB#66:                                #   in Loop: Header=BB3_65 Depth=2
	movswl	Big_Hexagon_Y+32(%r13), %r12d
	imull	%esi, %r12d
	addl	%edi, %r12d
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_68
# BB#67:                                #   in Loop: Header=BB3_65 Depth=2
	movq	mvbits(%rip), %rax
	leal	(,%rbx,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leal	(,%r12,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r14d
	addl	(%rax,%rcx,4), %r14d
	imull	304(%rsp), %r14d
	sarl	$16, %r14d
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%r14d, %ecx
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	leal	80(,%rbx,4), %r8d
	movq	%r11, 48(%rsp)          # 8-byte Spill
	leal	80(,%r12,4), %r9d
	movl	%ebp, %r15d
	movl	%edi, %ebp
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%ebp, %edi
	movl	%r15d, %ebp
	movq	48(%rsp), %r11          # 8-byte Reload
	movl	32(%rsp), %r10d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%r14d, %eax
	cmpl	%r10d, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmovll	%ebx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmovll	%r12d, %r11d
	cmovlel	%eax, %r10d
.LBB3_68:                               #   in Loop: Header=BB3_65 Depth=2
	addq	$2, %r13
	jne	.LBB3_65
# BB#69:                                #   in Loop: Header=BB3_64 Depth=1
	leal	1(%rsi), %eax
	movzwl	%ax, %esi
	cmpl	92(%rsp), %esi          # 4-byte Folded Reload
	jle	.LBB3_64
# BB#70:                                # %.loopexit739
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	jl	.LBB3_73
.LBB3_71:
	movzwl	smpUMHEX_pred_MV_uplayer_X(%rip), %eax
	movw	%ax, %cx
	sarw	$15, %cx
	andl	$49152, %ecx            # imm = 0xC000
	shrl	$14, %ecx
	addl	%eax, %ecx
	sarw	$2, %cx
	movswl	%cx, %ebx
	movq	112(%rsp), %r15         # 8-byte Reload
	addl	%r15d, %ebx
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_74
# BB#111:
	movzwl	smpUMHEX_pred_MV_uplayer_Y(%rip), %eax
	movw	%ax, %cx
	sarw	$15, %cx
	andl	$49152, %ecx            # imm = 0xC000
	shrl	$14, %ecx
	addl	%eax, %ecx
	sarw	$2, %cx
	movswl	%cx, %r14d
	addl	104(%rsp), %r14d        # 4-byte Folded Reload
	movl	%r14d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	movq	16(%rsp), %r13          # 8-byte Reload
	jg	.LBB3_75
# BB#112:
	movq	mvbits(%rip), %rax
	leal	(,%rbx,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leal	(,%r14,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebp
	addl	(%rax,%rcx,4), %ebp
	imull	304(%rsp), %ebp
	sarl	$16, %ebp
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%ebp, %ecx
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	leal	80(,%rbx,4), %r8d
	movq	%r11, %r12
	leal	80(,%r14,4), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	%r12, %r11
	movl	32(%rsp), %r10d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%ebp, %eax
	cmpl	%r10d, %eax
	cmovll	%ebx, %r13d
	cmovll	%r14d, %r11d
	cmovlel	%eax, %r10d
	jmp	.LBB3_75
.LBB3_73:
	movq	112(%rsp), %r15         # 8-byte Reload
.LBB3_74:
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB3_75:
	movq	120(%rsp), %rax         # 8-byte Reload
                                        # kill: %AX<def> %AX<kill> %RAX<kill>
	movq	144(%rsp), %rcx         # 8-byte Reload
	orw	%cx, %ax
	je	.LBB3_84
# BB#76:
	movl	%ecx, %eax
	negl	%eax
	testw	%cx, %cx
	cmovgl	%ecx, %eax
	cmpl	%edx, %eax
	jg	.LBB3_79
# BB#77:
	movq	120(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %eax
	negl	%eax
	testw	%cx, %cx
	cmovgl	%ecx, %eax
	cmpl	%edx, %eax
	jg	.LBB3_79
# BB#78:
	movq	mvbits(%rip), %rax
	movq	128(%rsp), %rcx         # 8-byte Reload
	negl	%ecx
	movslq	%ecx, %rcx
	movq	136(%rsp), %rdx         # 8-byte Reload
	negl	%edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	imull	304(%rsp), %ebx
	sarl	$16, %ebx
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%ebx, %ecx
	movq	168(%rsp), %r8          # 8-byte Reload
	addl	$80, %r8d
	movq	160(%rsp), %r9          # 8-byte Reload
	addl	$80, %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%r10d, %r14d
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r11, %rbp
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	*computeUniPred(,%rax,8)
	movq	%rbp, %r11
	movl	%r14d, %r10d
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%ebx, %eax
	cmpl	%r10d, %eax
	cmovll	%r15d, %r13d
	cmovll	104(%rsp), %r11d        # 4-byte Folded Reload
	cmovlel	%eax, %r10d
.LBB3_79:
	movq	$-8, %rbx
	movl	%r11d, %r15d
	movl	%r13d, %edi
	movq	%r13, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_80:                               # =>This Inner Loop Header: Depth=1
	movswl	Diamond_X+8(%rbx), %ebp
	addl	%r13d, %ebp
	movl	%ebp, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_83
# BB#81:                                #   in Loop: Header=BB3_80 Depth=1
	movswl	Diamond_Y+8(%rbx), %r12d
	addl	%r11d, %r12d
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_83
# BB#82:                                #   in Loop: Header=BB3_80 Depth=1
	movq	mvbits(%rip), %rax
	leal	(,%rbp,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leal	(,%r12,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r13d
	addl	(%rax,%rcx,4), %r13d
	imull	304(%rsp), %r13d
	sarl	$16, %r13d
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	movl	%r15d, 48(%rsp)         # 4-byte Spill
	movl	%edi, %r15d
	leal	80(,%rbp,4), %r8d
	movq	%r11, %r14
	leal	80(,%r12,4), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	%r15d, %edi
	movl	48(%rsp), %r15d         # 4-byte Reload
	movq	%r14, %r11
	movl	32(%rsp), %r10d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%r13d, %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	cmpl	%r10d, %eax
	cmovll	%ebp, %edi
	cmovll	%r12d, %r15d
	cmovlel	%eax, %r10d
.LBB3_83:                               #   in Loop: Header=BB3_80 Depth=1
	addq	$2, %rbx
	jne	.LBB3_80
	jmp	.LBB3_85
.LBB3_84:
	movl	%r13d, %edi
	movl	%r11d, %r15d
.LBB3_85:                               # %.loopexit736
	movzwl	ConvergeThreshold(%rip), %eax
	movl	84(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	cmpl	%eax, %r10d
	jge	.LBB3_91
# BB#86:                                # %.preheader729.preheader
	movq	$-8, %rbx
	movl	%r15d, %r11d
	movl	%edi, %r13d
	movl	%r15d, 48(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB3_87:                               # %.preheader729
                                        # =>This Inner Loop Header: Depth=1
	movswl	Diamond_X+8(%rbx), %ebp
	addl	%edi, %ebp
	movl	%ebp, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_90
# BB#88:                                #   in Loop: Header=BB3_87 Depth=1
	movswl	Diamond_Y+8(%rbx), %r12d
	addl	%r15d, %r12d
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_90
# BB#89:                                #   in Loop: Header=BB3_87 Depth=1
	movq	mvbits(%rip), %rax
	leal	(,%rbp,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leal	(,%r12,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	(%rax,%rdx,4), %r13d
	addl	(%rax,%rcx,4), %r13d
	imull	304(%rsp), %r13d
	sarl	$16, %r13d
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	movl	%edi, %r15d
	leal	80(,%rbp,4), %r8d
	movq	%r11, %r14
	leal	80(,%r12,4), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	%r15d, %edi
	movl	48(%rsp), %r15d         # 4-byte Reload
	movq	%r14, %r11
	movl	32(%rsp), %r10d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%r13d, %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	cmpl	%r10d, %eax
	cmovll	%ebp, %r13d
	cmovll	%r12d, %r11d
	cmovlel	%eax, %r10d
.LBB3_90:                               #   in Loop: Header=BB3_87 Depth=1
	addq	$2, %rbx
	jne	.LBB3_87
	jmp	.LBB3_115
.LBB3_91:                               # %.preheader735
	testl	%edx, %edx
	jle	.LBB3_113
# BB#92:                                # %.preheader734.preheader
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB3_93:                               # %.preheader734
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_94 Depth 2
	movl	%edi, %r13d
	movl	%r15d, %r11d
	movq	$-12, %rbx
	movq	%r13, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_94:                               #   Parent Loop BB3_93 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	Hexagon_X+12(%rbx), %ebp
	addl	%r13d, %ebp
	movl	%ebp, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_97
# BB#95:                                #   in Loop: Header=BB3_94 Depth=2
	movswl	Hexagon_Y+12(%rbx), %r12d
	addl	%r11d, %r12d
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_97
# BB#96:                                #   in Loop: Header=BB3_94 Depth=2
	movq	mvbits(%rip), %rax
	leal	(,%rbp,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leal	(,%r12,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r13d
	addl	(%rax,%rcx,4), %r13d
	imull	304(%rsp), %r13d
	sarl	$16, %r13d
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	movl	%r15d, 48(%rsp)         # 4-byte Spill
	movl	%edi, %r15d
	leal	80(,%rbp,4), %r8d
	movq	%r11, %r14
	leal	80(,%r12,4), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	%r15d, %edi
	movl	48(%rsp), %r15d         # 4-byte Reload
	movq	%r14, %r11
	movl	32(%rsp), %r10d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%r13d, %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	cmpl	%r10d, %eax
	cmovll	%ebp, %edi
	cmovll	%r12d, %r15d
	cmovlel	%eax, %r10d
.LBB3_97:                               #   in Loop: Header=BB3_94 Depth=2
	addq	$2, %rbx
	jne	.LBB3_94
# BB#98:                                #   in Loop: Header=BB3_93 Depth=1
	cmpl	%r13d, %edi
	jne	.LBB3_100
# BB#99:                                #   in Loop: Header=BB3_93 Depth=1
	cmpl	%r11d, %r15d
	je	.LBB3_101
.LBB3_100:                              #   in Loop: Header=BB3_93 Depth=1
	movl	72(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movzwl	%ax, %eax
	cmpl	%edx, %eax
	movl	%edi, %r13d
	movl	%r15d, %r11d
	jl	.LBB3_93
.LBB3_101:                              # %.preheader732
	testl	%edx, %edx
	jle	.LBB3_115
# BB#102:                               # %.preheader731.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_103:                              # %.preheader731
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_104 Depth 2
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
                                        # kill: %R13D<def> %R13D<kill> %R13<kill>
	movl	%r11d, %r14d
	movq	$-8, %rbp
	.p2align	4, 0x90
.LBB3_104:                              #   Parent Loop BB3_103 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	Diamond_X+8(%rbp), %ebx
	addl	%r13d, %ebx
	movl	%ebx, %eax
	subl	%r8d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_107
# BB#105:                               #   in Loop: Header=BB3_104 Depth=2
	movswl	Diamond_Y+8(%rbp), %r12d
	addl	%r14d, %r12d
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%edx, %ecx
	jg	.LBB3_107
# BB#106:                               #   in Loop: Header=BB3_104 Depth=2
	movq	mvbits(%rip), %rax
	leal	(,%rbx,4), %ecx
	subl	28(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leal	(,%r12,4), %edx
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r15d
	addl	(%rax,%rcx,4), %r15d
	imull	304(%rsp), %r15d
	sarl	$16, %r15d
	movslq	dist_method(%rip), %rax
	movl	%r10d, %ecx
	subl	%r15d, %ecx
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	leal	80(,%rbx,4), %r8d
	movq	%r11, 48(%rsp)          # 8-byte Spill
	leal	80(,%r12,4), %r9d
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	48(%rsp), %r11          # 8-byte Reload
	movl	32(%rsp), %r10d         # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movl	288(%rsp), %edx
	addl	%r15d, %eax
	cmpl	%r10d, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmovll	%ebx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmovll	%r12d, %r11d
	cmovlel	%eax, %r10d
.LBB3_107:                              #   in Loop: Header=BB3_104 Depth=2
	addq	$2, %rbp
	jne	.LBB3_104
# BB#108:                               #   in Loop: Header=BB3_103 Depth=1
	cmpl	%r13d, 16(%rsp)         # 4-byte Folded Reload
	jne	.LBB3_110
# BB#109:                               #   in Loop: Header=BB3_103 Depth=1
	cmpl	%r14d, %r11d
	je	.LBB3_114
.LBB3_110:                              #   in Loop: Header=BB3_103 Depth=1
	movl	72(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	movzwl	%cx, %eax
	cmpl	%edx, %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	jl	.LBB3_103
	jmp	.LBB3_115
.LBB3_113:
	movl	%edi, %r13d
	movl	%r15d, %r11d
	jmp	.LBB3_115
.LBB3_114:
                                        # kill: %R13D<def> %R13D<kill> %R13<def>
	movl	%r14d, %r11d
.LBB3_115:                              # %.loopexit
	subl	112(%rsp), %r13d        # 4-byte Folded Reload
	movq	272(%rsp), %rax
	movw	%r13w, (%rax)
	subl	104(%rsp), %r11d        # 4-byte Folded Reload
	movq	280(%rsp), %rax
	movw	%r11w, (%rax)
	movl	%r10d, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	smpUMHEXIntegerPelBlockMotionSearch, .Lfunc_end3-smpUMHEXIntegerPelBlockMotionSearch
	.cfi_endproc

	.globl	smpUMHEXFullSubPelBlockMotionSearch
	.p2align	4, 0x90
	.type	smpUMHEXFullSubPelBlockMotionSearch,@function
smpUMHEXFullSubPelBlockMotionSearch:    # @smpUMHEXFullSubPelBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 128
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	160(%rsp), %ebp
	movq	input(%rip), %rbx
	movq	img(%rip), %r11
	cmpl	$0, 4168(%rbx)
	je	.LBB4_2
# BB#1:
	xorl	%r10d, %r10d
	jmp	.LBB4_9
.LBB4_2:
	cmpl	$1, 20(%r11)
	jne	.LBB4_4
# BB#3:
	xorl	%r10d, %r10d
	jmp	.LBB4_9
.LBB4_4:
	xorl	%r10d, %r10d
	testw	%si, %si
	jne	.LBB4_9
# BB#5:
	cmpl	$1, %r9d
	jne	.LBB4_9
# BB#6:
	movq	144(%rsp), %rax
	cmpw	$0, (%rax)
	je	.LBB4_8
# BB#7:
	xorl	%r10d, %r10d
	jmp	.LBB4_9
.LBB4_8:
	movq	152(%rsp), %rax
	cmpw	$0, (%rax)
	sete	%r10b
.LBB4_9:                                # %._crit_edge236
	movslq	%r9d, %rax
	movl	72(%rbx,%rax,8), %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	76(%rbx,%rax,8), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movslq	start_me_refinement_hp(%rip), %r13
	testl	%ebp, %ebp
	movl	$1, %r9d
	cmovgl	%ebp, %r9d
	testq	%r13, %r13
	cmovnel	%ebp, %r9d
	movq	14224(%r11), %rax
	movslq	12(%r11), %rdi
	imulq	$536, %rdi, %rdi        # imm = 0x218
	movslq	432(%rax,%rdi), %rbp
	movq	active_pps(%rip), %rdi
	cmpl	$0, 192(%rdi)
	je	.LBB4_12
# BB#10:
	movl	20(%r11), %eax
	testl	%eax, %eax
	je	.LBB4_15
# BB#11:
	cmpl	$3, %eax
	je	.LBB4_15
.LBB4_12:
	cmpl	$0, 196(%rdi)
	je	.LBB4_13
# BB#14:
	cmpl	$1, 20(%r11)
	jne	.LBB4_13
.LBB4_15:
	cmpl	$0, 2936(%rbx)
	setne	%al
	jmp	.LBB4_16
.LBB4_13:
	xorl	%eax, %eax
.LBB4_16:
	movq	152(%rsp), %r11
	movslq	%edx, %rdx
	addq	%rdx, %rbp
	movq	listX(,%rbp,8), %rdi
	movswq	%si, %rdx
	movq	(%rdi,%rdx,8), %rsi
	movl	6392(%rsi), %r12d
	movl	6396(%rsi), %r14d
	movzbl	%al, %edi
	leal	2(%rdi,%rdi,2), %ebx
	movl	%ebx, dist_method(%rip)
	movq	6448(%rsi), %rbx
	movq	%rbx, ref_pic_sub(%rip)
	movw	%r12w, img_width(%rip)
	movw	%r14w, img_height(%rip)
	movl	6408(%rsi), %ebx
	movl	%ebx, width_pad(%rip)
	movl	6412(%rsi), %ebx
	movl	%ebx, height_pad(%rip)
	testb	%dil, %dil
	je	.LBB4_18
# BB#17:
	movq	wp_weight(%rip), %rdi
	movq	(%rdi,%rbp,8), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movl	(%rdi), %edi
	movl	%edi, weight_luma(%rip)
	movq	wp_offset(%rip), %rdi
	movq	(%rdi,%rbp,8), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movl	(%rdi), %edi
	movl	%edi, offset_luma(%rip)
.LBB4_18:
	leal	80(,%rcx,4), %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB4_21
# BB#19:
	movq	6464(%rsi), %rcx
	movq	(%rcx), %rdi
	movq	%rdi, ref_pic_sub+8(%rip)
	movq	8(%rcx), %rcx
	movq	%rcx, ref_pic_sub+16(%rip)
	movl	6416(%rsi), %ecx
	movl	%ecx, width_pad_cr(%rip)
	movl	6420(%rsi), %ecx
	movl	%ecx, height_pad_cr(%rip)
	testb	%al, %al
	je	.LBB4_21
# BB#20:
	movq	wp_weight(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rdx,8), %rax
	movl	4(%rax), %ecx
	movl	%ecx, weight_cr(%rip)
	movl	8(%rax), %eax
	movl	%eax, weight_cr+4(%rip)
	movq	wp_offset(%rip), %rax
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rdx,8), %rax
	movl	4(%rax), %ecx
	movl	%ecx, offset_cr(%rip)
	movl	8(%rax), %eax
	movl	%eax, offset_cr+4(%rip)
.LBB4_21:
	subl	12(%rsp), %r12d         # 4-byte Folded Reload
	subl	8(%rsp), %r14d          # 4-byte Folded Reload
	leal	80(,%r8,4), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	144(%rsp), %rax
	movswl	(%rax), %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%rcx,%rax), %edx
	movl	$1, %eax
	cmpl	$2, %edx
	jl	.LBB4_25
# BB#22:
	leal	(,%r12,4), %esi
	addl	$159, %esi
	cmpl	%esi, %edx
	jge	.LBB4_25
# BB#23:
	movswl	(%r11), %edx
	addl	(%rsp), %edx            # 4-byte Folded Reload
	cmpl	$2, %edx
	jl	.LBB4_25
# BB#24:
	leal	(,%r14,4), %esi
	addl	$159, %esi
	xorl	%eax, %eax
	cmpl	%esi, %edx
	setge	%al
.LBB4_25:
	movl	176(%rsp), %edx
	movw	136(%rsp), %bx
	movw	128(%rsp), %si
	movl	%eax, ref_access_method(%rip)
	cmpl	%r9d, %r13d
	jge	.LBB4_26
# BB#27:                                # %.lr.ph222
	movswl	%si, %esi
	movswl	%bx, %r15d
	movslq	%r9d, %rdi
	testb	%r10b, %r10b
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	je	.LBB4_28
# BB#33:                                # %.lr.ph222.split.us.preheader
	movl	184(%rsp), %eax
	sarl	$12, %eax
	negl	%eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movq	%r13, %rbx
	incq	%rbx
	xorl	%r12d, %r12d
	movl	%edx, %r10d
	jmp	.LBB4_34
	.p2align	4, 0x90
.LBB4_37:                               # %..lr.ph222.split.us_crit_edge
                                        #   in Loop: Header=BB4_34 Depth=1
	movq	144(%rsp), %rax
	movw	(%rax), %cx
	incl	%r13d
	incq	%rbx
.LBB4_34:                               # %.lr.ph222.split.us
                                        # =>This Inner Loop Header: Depth=1
	movswl	%cx, %eax
	movq	spiral_hpel_search_x(%rip), %rcx
	movswl	-2(%rcx,%rbx,2), %r8d
	addl	%eax, %r8d
	movswl	(%r11), %eax
	movq	spiral_hpel_search_y(%rip), %rcx
	movswl	-2(%rcx,%rbx,2), %r9d
	addl	%eax, %r9d
	movq	mvbits(%rip), %rax
	movl	%r8d, %ecx
	subl	%esi, %ecx
	movslq	%ecx, %rcx
	movl	%r9d, %edx
	subl	%r15d, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r14d
	addl	(%rax,%rcx,4), %r14d
	imull	184(%rsp), %r14d
	sarl	$16, %r14d
	movl	%r10d, %ecx
	subl	%r14d, %ecx
	jle	.LBB4_36
# BB#35:                                #   in Loop: Header=BB4_34 Depth=1
	addl	16(%rsp), %r8d          # 4-byte Folded Reload
	addl	(%rsp), %r9d            # 4-byte Folded Reload
	movslq	dist_method(%rip), %rax
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%r10d, %ebp
	callq	*computeUniPred(,%rax,8)
	movl	%ebp, %r10d
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	152(%rsp), %r11
	testl	%r13d, %r13d
	movl	$0, %ecx
	cmovel	52(%rsp), %ecx          # 4-byte Folded Reload
	addl	%ecx, %r14d
	addl	%eax, %r14d
	cmpl	%r10d, %r14d
	cmovll	%r13d, %r12d
	cmovlel	%r14d, %r10d
	movzwl	SubPelThreshold3(%rip), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movzbl	block_type_shift_factor(%rcx,%rcx), %ecx
	shrl	%cl, %eax
	cmpl	%eax, %r10d
	jl	.LBB4_38
.LBB4_36:                               #   in Loop: Header=BB4_34 Depth=1
	cmpq	%rdi, %rbx
	jl	.LBB4_37
	jmp	.LBB4_38
.LBB4_26:
	movl	%edx, %r10d
	jmp	.LBB4_41
.LBB4_28:                               # %.lr.ph222.split.preheader
	movq	%r13, %r14
	incq	%r14
	xorl	%r12d, %r12d
	movl	%edx, %r10d
	jmp	.LBB4_29
	.p2align	4, 0x90
.LBB4_32:                               # %..lr.ph222.split_crit_edge
                                        #   in Loop: Header=BB4_29 Depth=1
	movq	144(%rsp), %rax
	movw	(%rax), %cx
	incq	%r14
	incl	%r13d
.LBB4_29:                               # %.lr.ph222.split
                                        # =>This Inner Loop Header: Depth=1
	movswl	%cx, %eax
	movq	spiral_hpel_search_x(%rip), %rcx
	movswl	-2(%rcx,%r14,2), %r8d
	addl	%eax, %r8d
	movswl	(%r11), %eax
	movq	spiral_hpel_search_y(%rip), %rcx
	movswl	-2(%rcx,%r14,2), %r9d
	addl	%eax, %r9d
	movq	mvbits(%rip), %rax
	movl	%r8d, %ecx
	subl	%esi, %ecx
	movslq	%ecx, %rcx
	movl	%r9d, %edx
	subl	%r15d, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	imull	184(%rsp), %ebx
	sarl	$16, %ebx
	movl	%r10d, %ecx
	subl	%ebx, %ecx
	jle	.LBB4_31
# BB#30:                                #   in Loop: Header=BB4_29 Depth=1
	addl	16(%rsp), %r8d          # 4-byte Folded Reload
	addl	(%rsp), %r9d            # 4-byte Folded Reload
	movslq	dist_method(%rip), %rax
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%r10d, %ebp
	callq	*computeUniPred(,%rax,8)
	movl	%ebp, %r10d
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	152(%rsp), %r11
	addl	%eax, %ebx
	cmpl	%r10d, %ebx
	cmovll	%r13d, %r12d
	cmovlel	%ebx, %r10d
	movzwl	SubPelThreshold3(%rip), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movzbl	block_type_shift_factor(%rcx,%rcx), %ecx
	shrl	%cl, %eax
	cmpl	%eax, %r10d
	jl	.LBB4_38
.LBB4_31:                               #   in Loop: Header=BB4_29 Depth=1
	cmpq	%rdi, %r14
	jl	.LBB4_32
.LBB4_38:                               # %._crit_edge223
	testl	%r12d, %r12d
	je	.LBB4_40
# BB#39:
	movq	spiral_hpel_search_x(%rip), %rax
	movslq	%r12d, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movq	144(%rsp), %rdx
	addw	%ax, (%rdx)
	movq	spiral_hpel_search_y(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	addw	%ax, (%r11)
.LBB4_40:                               # %._crit_edge223.thread
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	56(%rsp), %r12          # 8-byte Reload
	movw	136(%rsp), %bx
	movw	128(%rsp), %si
.LBB4_41:                               # %._crit_edge223.thread
	movq	144(%rsp), %rax
	movswl	(%rax), %eax
	testl	%eax, %eax
	jne	.LBB4_44
# BB#42:
	movl	%ebx, %ecx
	orl	%esi, %ecx
	orw	(%r11), %cx
	jne	.LBB4_44
# BB#43:
	movzwl	SubPelThreshold1(%rip), %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movb	block_type_shift_factor(%rcx,%rcx), %cl
	shrl	%cl, %edx
	cmpl	%edx, %r10d
	jl	.LBB4_56
.LBB4_44:
	movl	%esi, %ecx
	movl	168(%rsp), %r8d
	movslq	start_me_refinement_qp(%rip), %rbp
	testq	%rbp, %rbp
	movl	$2147483647, %edx       # imm = 0x7FFFFFFF
	cmovel	%edx, %r10d
	movl	$1, %edx
	movl	%eax, %esi
	addl	16(%rsp), %esi          # 4-byte Folded Reload
	jle	.LBB4_48
# BB#45:
	leal	160(,%r12,4), %edi
	cmpl	%edi, %esi
	jge	.LBB4_48
# BB#46:
	movswl	(%r11), %esi
	addl	(%rsp), %esi            # 4-byte Folded Reload
	jle	.LBB4_48
# BB#47:
	leal	160(,%r14,4), %edi
	xorl	%edx, %edx
	cmpl	%edi, %esi
	setge	%dl
.LBB4_48:
	movl	%edx, ref_access_method(%rip)
	cmpl	%r8d, %ebp
	jge	.LBB4_56
# BB#49:                                # %.lr.ph
	movswl	%cx, %esi
	movswl	%bx, %edi
	movslq	%r8d, %r12
	movq	%rbp, %r14
	incq	%r14
	xorl	%ebx, %ebx
	movl	%edi, 4(%rsp)           # 4-byte Spill
	movl	%esi, 24(%rsp)          # 4-byte Spill
	jmp	.LBB4_50
	.p2align	4, 0x90
.LBB4_53:                               # %._crit_edge239
                                        #   in Loop: Header=BB4_50 Depth=1
	movq	144(%rsp), %rax
	movw	(%rax), %ax
	incq	%r14
	incl	%ebp
.LBB4_50:                               # =>This Inner Loop Header: Depth=1
	cwtl
	movq	spiral_search_x(%rip), %rcx
	movswl	-2(%rcx,%r14,2), %r8d
	addl	%eax, %r8d
	movswl	(%r11), %eax
	movq	spiral_search_y(%rip), %rcx
	movswl	-2(%rcx,%r14,2), %r9d
	addl	%eax, %r9d
	movq	mvbits(%rip), %rax
	movl	%r8d, %ecx
	subl	%esi, %ecx
	movslq	%ecx, %rcx
	movl	%r9d, %edx
	subl	%edi, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r13d
	addl	(%rax,%rcx,4), %r13d
	imull	184(%rsp), %r13d
	sarl	$16, %r13d
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	jle	.LBB4_52
# BB#51:                                #   in Loop: Header=BB4_50 Depth=1
	addl	16(%rsp), %r8d          # 4-byte Folded Reload
	addl	(%rsp), %r9d            # 4-byte Folded Reload
	movslq	dist_method(%rip), %rax
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%ebx, %r15d
	movl	%r10d, %ebx
	callq	*computeUniPred(,%rax,8)
	movl	%ebx, %r10d
	movl	%r15d, %ebx
	movl	4(%rsp), %edi           # 4-byte Reload
	movl	24(%rsp), %esi          # 4-byte Reload
	movq	152(%rsp), %r11
	addl	%r13d, %eax
	cmpl	%r10d, %eax
	cmovll	%ebp, %ebx
	cmovlel	%eax, %r10d
	movzwl	SubPelThreshold3(%rip), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movzbl	block_type_shift_factor(%rcx,%rcx), %ecx
	shrl	%cl, %eax
	cmpl	%eax, %r10d
	jl	.LBB4_54
.LBB4_52:                               #   in Loop: Header=BB4_50 Depth=1
	cmpq	%r12, %r14
	jl	.LBB4_53
.LBB4_54:                               # %._crit_edge
	testl	%ebx, %ebx
	je	.LBB4_56
# BB#55:
	movq	spiral_search_x(%rip), %rax
	movslq	%ebx, %rcx
	movzwl	(%rax,%rcx,2), %eax
	movq	144(%rsp), %rdx
	addw	%ax, (%rdx)
	movq	spiral_search_y(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	addw	%ax, (%r11)
.LBB4_56:                               # %._crit_edge.thread
	movl	%r10d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	smpUMHEXFullSubPelBlockMotionSearch, .Lfunc_end4-smpUMHEXFullSubPelBlockMotionSearch
	.cfi_endproc

	.globl	smpUMHEXSubPelBlockMotionSearch
	.p2align	4, 0x90
	.type	smpUMHEXSubPelBlockMotionSearch,@function
smpUMHEXSubPelBlockMotionSearch:        # @smpUMHEXSubPelBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 160
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	img(%rip), %rax
	xorl	%edi, %edi
	cmpl	$0, 15268(%rax)
	je	.LBB5_3
# BB#1:
	movq	14224(%rax), %r10
	movslq	12(%rax), %rbp
	imulq	$536, %rbp, %rbx        # imm = 0x218
	cmpl	$0, 424(%r10,%rbx)
	je	.LBB5_3
# BB#2:
	andl	$1, %ebp
	leal	2(%rbp,%rbp), %edi
.LBB5_3:
	addl	%edx, %edi
	movslq	%edi, %r11
	movq	listX(,%r11,8), %rdx
	movswq	%si, %rbp
	movq	(%rdx,%rbp,8), %rdx
	movq	input(%rip), %rbx
	movslq	%r9d, %rsi
	movswl	72(%rbx,%rsi,8), %edi
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movswl	76(%rbx,%rsi,8), %esi
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movl	6392(%rdx), %esi
	movl	6396(%rdx), %r10d
	movq	active_pps(%rip), %rdx
	cmpl	$0, 192(%rdx)
	je	.LBB5_6
# BB#4:
	movl	20(%rax), %edi
	testl	%edi, %edi
	je	.LBB5_8
# BB#5:
	cmpl	$3, %edi
	je	.LBB5_8
.LBB5_6:
	cmpl	$0, 196(%rdx)
	je	.LBB5_10
# BB#7:
	cmpl	$1, 20(%rax)
	jne	.LBB5_10
.LBB5_8:
	cmpl	$0, 2936(%rbx)
	setne	%bl
	jmp	.LBB5_11
.LBB5_10:
	xorl	%ebx, %ebx
.LBB5_11:
	movzbl	%bl, %edx
	leal	2(%rdx,%rdx,2), %r14d
	movl	%r14d, dist_method(%rip)
	movq	ref_pic_ptr(%rip), %rax
	movq	6448(%rax), %rdi
	movq	%rdi, ref_pic_sub(%rip)
	movzwl	6392(%rax), %edi
	movw	%di, img_width(%rip)
	movzwl	6396(%rax), %edi
	movw	%di, img_height(%rip)
	movl	6408(%rax), %edi
	movl	%edi, width_pad(%rip)
	movl	6412(%rax), %edi
	movl	%edi, height_pad(%rip)
	testb	%dl, %dl
	je	.LBB5_13
# BB#12:
	movq	wp_weight(%rip), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movl	(%rdx), %edx
	movl	%edx, weight_luma(%rip)
	movq	wp_offset(%rip), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movl	(%rdx), %edx
	movl	%edx, offset_luma(%rip)
.LBB5_13:
	leal	80(,%rcx,4), %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB5_16
# BB#14:
	movq	6464(%rax), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, ref_pic_sub+8(%rip)
	movq	8(%rcx), %rcx
	movq	%rcx, ref_pic_sub+16(%rip)
	movl	6416(%rax), %ecx
	movl	%ecx, width_pad_cr(%rip)
	movl	6420(%rax), %eax
	movl	%eax, height_pad_cr(%rip)
	testb	%bl, %bl
	je	.LBB5_16
# BB#15:
	movq	wp_weight(%rip), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%rbp,8), %rax
	movl	4(%rax), %ecx
	movl	%ecx, weight_cr(%rip)
	movl	8(%rax), %eax
	movl	%eax, weight_cr+4(%rip)
	movq	wp_offset(%rip), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%rbp,8), %rax
	movl	4(%rax), %ecx
	movl	%ecx, offset_cr(%rip)
	movl	8(%rax), %eax
	movl	%eax, offset_cr+4(%rip)
.LBB5_16:
	movq	184(%rsp), %r11
	movzwl	168(%rsp), %r15d
	movzwl	160(%rsp), %edx
	leal	80(,%r8,4), %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	176(%rsp), %r9
	movswl	(%r9), %ecx
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	(%rcx,%rax), %ebp
	movl	$1, %eax
	cmpl	$2, %ebp
	movl	%r14d, 8(%rsp)          # 4-byte Spill
	jl	.LBB5_20
# BB#17:
	subl	28(%rsp), %esi          # 4-byte Folded Reload
	shll	$18, %esi
	addl	$10485760, %esi         # imm = 0xA00000
	sarl	$16, %esi
	decl	%esi
	cmpl	%esi, %ebp
	jge	.LBB5_20
# BB#18:
	movswl	(%r11), %esi
	addl	64(%rsp), %esi          # 4-byte Folded Reload
	cmpl	$2, %esi
	jl	.LBB5_20
# BB#19:
	subl	20(%rsp), %r10d         # 4-byte Folded Reload
	shll	$18, %r10d
	addl	$10485760, %r10d        # imm = 0xA00000
	sarl	$16, %r10d
	decl	%r10d
	xorl	%eax, %eax
	cmpl	%r10d, %esi
	setge	%al
.LBB5_20:
	movl	216(%rsp), %r10d
	movl	208(%rsp), %r8d
	movl	%eax, ref_access_method(%rip)
	movswl	%dx, %ebx
	movl	%ebx, %edi
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	subl	%ecx, %edi
	movl	%edi, %r14d
	sarl	$31, %r14d
	shrl	$30, %r14d
	addl	%edi, %r14d
	andl	$-4, %r14d
	movswl	%r15w, %esi
	movswl	(%r11), %ecx
	movl	%esi, %r15d
	subl	%ecx, %r15d
	movl	%r15d, %ebp
	sarl	$31, %ebp
	shrl	$30, %ebp
	addl	%r15d, %ebp
	andl	$-4, %ebp
	movzwl	smpUMHEX_pred_MV_uplayer_X(%rip), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movzwl	smpUMHEX_pred_MV_uplayer_Y(%rip), %eax
	movl	%eax, 92(%rsp)          # 4-byte Spill
	movq	smpUMHEX_SearchState(%rip), %rax
	movq	(%rax), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movb	$0, 48(%rax)
	movq	smpUMHEX_SearchState(%rip), %rax
	movq	24(%rax), %rax
	movb	$1, 3(%rax)
	movzwl	(%r9), %edx
	movswl	%dx, %r13d
	movswl	(%r11), %r12d
	cmpl	$0, start_me_refinement_hp(%rip)
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movl	%ebx, 60(%rsp)          # 4-byte Spill
	je	.LBB5_22
# BB#21:
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%r13, 40(%rsp)          # 8-byte Spill
	jmp	.LBB5_23
.LBB5_22:
	movq	mvbits(%rip), %rax
	movl	%ecx, 88(%rsp)          # 4-byte Spill
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	movslq	%ecx, %rcx
	movl	%r12d, %edx
	subl	%esi, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	imull	%r10d, %ebx
	sarl	$16, %ebx
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%r8d, %ecx
	subl	%ebx, %ecx
	movq	72(%rsp), %rdx          # 8-byte Reload
	leal	(%r13,%rdx), %r8d
	movq	64(%rsp), %rdx          # 8-byte Reload
	leal	(%r12,%rdx), %r9d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	80(%rsp), %rdi          # 8-byte Reload
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	24(%rsp), %esi          # 4-byte Reload
	movl	216(%rsp), %r10d
	movl	208(%rsp), %r8d
	movq	184(%rsp), %r11
	movq	176(%rsp), %r9
	movq	8(%rsp), %rdi           # 8-byte Reload
	addl	%ebx, %eax
	movl	60(%rsp), %ebx          # 4-byte Reload
	xorl	%ecx, %ecx
	cmpl	%r8d, %eax
	cmovgel	%ecx, %r13d
	movq	%r13, 40(%rsp)          # 8-byte Spill
	cmovll	%r12d, %ecx
	cmovlel	%eax, %r8d
	movzwl	(%r9), %edx
	movl	%ecx, %eax
	movl	88(%rsp), %ecx          # 4-byte Reload
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB5_23:
	subl	%r14d, %edi
	subl	%ebp, %r15d
	testw	%dx, %dx
	je	.LBB5_27
# BB#24:                                # %._crit_edge
	movl	%r15d, %eax
	orl	%edi, %eax
.LBB5_25:
	testl	%eax, %eax
	je	.LBB5_33
.LBB5_26:
	movswl	%dx, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%rdi), %r13d
	movswq	(%r11), %r12
	movslq	%r15d, %r15
	leaq	(%r12,%r15), %rbp
	movq	mvbits(%rip), %rax
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	movslq	%ecx, %rcx
	movl	%ebp, %edx
	subl	%esi, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	imull	%r10d, %ebx
	sarl	$16, %ebx
	movslq	dist_method(%rip), %rax
	movl	%r8d, %ecx
	subl	%ebx, %ecx
	movq	72(%rsp), %rdx          # 8-byte Reload
	movl	%r8d, %r14d
	leal	(%r13,%rdx), %r8d
	movq	64(%rsp), %rdx          # 8-byte Reload
	leal	(%rbp,%rdx), %r9d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	80(%rsp), %rdi          # 8-byte Reload
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	216(%rsp), %r10d
	movl	%r14d, %r8d
	movq	184(%rsp), %r11
	movq	176(%rsp), %r9
	addl	%ebx, %eax
	movq	smpUMHEX_SearchState(%rip), %rcx
	movswq	(%r11), %rdx
	leaq	3(%r12,%r15), %rsi
	subq	%rdx, %rsi
	movq	(%rcx,%rsi,8), %rcx
	movswq	(%r9), %rdx
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	leal	3(%rsi,%rdi), %esi
	movslq	%esi, %rsi
	subq	%rdx, %rsi
	movb	$1, (%rcx,%rsi)
	movl	24(%rsp), %esi          # 4-byte Reload
	cmpl	%r8d, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmovll	%r13d, %ecx
	movq	32(%rsp), %rdx          # 8-byte Reload
	cmovll	%ebp, %edx
	cmovlel	%eax, %r8d
	jmp	.LBB5_34
.LBB5_27:
	movl	%ebx, %r14d
	movl	16(%rsp), %ebp          # 4-byte Reload
	subl	48(%rsp), %ebp          # 4-byte Folded Reload
	movl	92(%rsp), %ebx          # 4-byte Reload
	subl	%ecx, %ebx
	andl	$3, %ebp
	andl	$3, %ebx
	movl	%r15d, %eax
	orl	%edi, %eax
	orl	%eax, %ebp
	orl	%ebx, %ebp
	jne	.LBB5_32
# BB#28:
	movzwl	(%r11), %ecx
	testw	%cx, %cx
	jne	.LBB5_32
# BB#29:
	movzwl	SubPelThreshold1(%rip), %esi
	movq	96(%rsp), %rcx          # 8-byte Reload
	movb	block_type_shift_factor(%rcx,%rcx), %cl
	shrl	%cl, %esi
	cmpl	%esi, %r8d
	movl	24(%rsp), %esi          # 4-byte Reload
	movl	%r14d, %ebx
	jge	.LBB5_25
# BB#30:
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	jmp	.LBB5_43
.LBB5_32:
	movl	%r14d, %ebx
	testl	%eax, %eax
	jne	.LBB5_26
.LBB5_33:
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB5_34:                               # %.preheader277
	movl	%edx, %edi
	movl	%ecx, %ebp
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB5_35:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_36 Depth 2
	movl	$1, 8(%rsp)             # 4-byte Folded Spill
	xorl	%r12d, %r12d
	movl	%edi, %r14d
	movl	%ebp, %r15d
	movl	%edi, 48(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB5_36:                               #   Parent Loop BB5_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	Diamond_X(%r12,%r12), %rax
	movslq	%ebp, %r13
	addq	%rax, %r13
	movswq	(%r9), %rcx
	movq	%r13, %rax
	subq	%rcx, %rax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	$3, %ecx
	jg	.LBB5_40
# BB#37:                                #   in Loop: Header=BB5_36 Depth=2
	movswq	Diamond_Y(%r12,%r12), %rcx
	movslq	%edi, %rbx
	addq	%rcx, %rbx
	movswq	(%r11), %rdx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	cmpl	$3, %edx
	jg	.LBB5_40
# BB#38:                                #   in Loop: Header=BB5_36 Depth=2
	movq	smpUMHEX_SearchState(%rip), %rdx
	movq	24(%rdx,%rcx,8), %rcx
	cmpb	$0, 3(%rcx,%rax)
	jne	.LBB5_40
# BB#39:                                #   in Loop: Header=BB5_36 Depth=2
	movq	mvbits(%rip), %rax
	movl	%r13d, %ecx
	subl	60(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movl	%ebx, %edx
	subl	%esi, %edx
	movslq	%edx, %rdx
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movl	(%rax,%rdx,4), %r15d
	addl	(%rax,%rcx,4), %r15d
	imull	%r10d, %r15d
	sarl	$16, %r15d
	movslq	dist_method(%rip), %rax
	movl	%r8d, %ecx
	subl	%r15d, %ecx
	movq	72(%rsp), %rdx          # 8-byte Reload
	movl	%r8d, %r14d
	leal	(%r13,%rdx), %r8d
	movq	64(%rsp), %rdx          # 8-byte Reload
	leal	(%rbx,%rdx), %r9d
	movq	80(%rsp), %rdi          # 8-byte Reload
	movl	20(%rsp), %esi          # 4-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%rax,8)
	movl	216(%rsp), %r10d
	movl	%r14d, %r8d
	movq	184(%rsp), %r11
	movq	176(%rsp), %r9
	addl	%r15d, %eax
	movl	48(%rsp), %edi          # 4-byte Reload
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	smpUMHEX_SearchState(%rip), %rcx
	movswl	(%r11), %edx
	leal	3(%rbx), %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movswl	(%r9), %edx
	leal	3(%r13), %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	movl	24(%rsp), %esi          # 4-byte Reload
	movb	$1, (%rcx,%rdx)
	cmpl	%r8d, %eax
	cmovll	%r13d, %r15d
	cmovll	%ebx, %r14d
	movl	$0, %ecx
	movl	8(%rsp), %edx           # 4-byte Reload
	cmovll	%ecx, %edx
	movl	%edx, 8(%rsp)           # 4-byte Spill
	cmovlel	%eax, %r8d
	movzwl	SubPelThreshold3(%rip), %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movzbl	block_type_shift_factor(%rcx,%rcx), %ecx
	shrl	%cl, %eax
	cmpl	%eax, %r8d
	jl	.LBB5_43
	.p2align	4, 0x90
.LBB5_40:                               #   in Loop: Header=BB5_36 Depth=2
	incq	%r12
	cmpq	$4, %r12
	jl	.LBB5_36
# BB#41:                                #   in Loop: Header=BB5_35 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB5_43
# BB#42:                                #   in Loop: Header=BB5_35 Depth=1
	movl	16(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cwtl
	cmpl	$3, %eax
	movl	%r14d, %edi
	movl	%r15d, %ebp
	jl	.LBB5_35
.LBB5_43:                               # %.loopexit
	movw	%r15w, (%r9)
	movw	%r14w, (%r11)
	movl	%r8d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	smpUMHEXSubPelBlockMotionSearch, .Lfunc_end5-smpUMHEXSubPelBlockMotionSearch
	.cfi_endproc

	.globl	smpUMHEXBipredIntegerPelBlockMotionSearch
	.p2align	4, 0x90
	.type	smpUMHEXBipredIntegerPelBlockMotionSearch,@function
smpUMHEXBipredIntegerPelBlockMotionSearch: # @smpUMHEXBipredIntegerPelBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Lcfi51:
	.cfi_def_cfa_offset 464
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 136(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	496(%rsp), %rbx
	movq	520(%rsp), %r8
	movq	512(%rsp), %r10
	movq	img(%rip), %rax
	movq	14224(%rax), %rbp
	movslq	12(%rax), %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	movslq	432(%rbp,%rax), %r13
	movq	input(%rip), %rax
	movl	%r9d, 184(%rsp)         # 4-byte Spill
	movslq	%r9d, %rdi
	movl	76(%rax,%rdi,8), %ebp
	movl	%ebp, 28(%rsp)          # 4-byte Spill
	movl	72(%rax,%rdi,8), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movzwl	(%rbx), %r14d
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	addl	%ecx, %r14d
	movzwl	(%r10), %ebp
	movzwl	(%r8), %r10d
	movq	active_pps(%rip), %rcx
	movl	196(%rcx), %r12d
	testl	%r12d, %r12d
	movq	504(%rsp), %rcx
	movzwl	(%rcx), %r9d
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movl	%ebp, 104(%rsp)         # 4-byte Spill
	je	.LBB6_3
# BB#1:
	movq	wp_offset(%rip), %rbp
	testl	%edx, %edx
	je	.LBB6_4
# BB#2:
	movq	8(%rbp,%r13,8), %rdi
	movswq	%si, %rcx
	shlq	$2, %rcx
	movq	(%rdi), %rdi
	addq	%rcx, %rdi
	movq	(%rbp,%r13,8), %rbp
	addq	(%rbp), %rcx
	jmp	.LBB6_5
.LBB6_3:
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	jmp	.LBB6_6
.LBB6_4:
	movq	(%rbp,%r13,8), %rcx
	movswq	%si, %rbx
	movq	(%rcx,%rbx,8), %rdi
	movq	8(%rbp,%r13,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
.LBB6_5:
	movl	(%rdi), %r11d
	movl	(%rcx), %r8d
.LBB6_6:
	addl	136(%rsp), %r9d         # 4-byte Folded Reload
	leal	(%r13,%rdx), %edi
	movslq	%edi, %rdi
	movq	listX(,%rdi,8), %rbp
	movswq	%si, %rsi
	movq	(%rbp,%rsi,8), %rdi
	movq	6448(%rdi), %rdi
	movq	%rdi, ref_pic1_sub(%rip)
	leal	1(%r13), %r15d
	testl	%edx, %edx
	movl	%r13d, %edi
	cmovel	%r15d, %edi
	movslq	%edi, %rdi
	movq	listX(,%rdi,8), %rdi
	movq	(%rdi), %rbx
	movq	6448(%rbx), %rbx
	movq	%rbx, ref_pic2_sub(%rip)
	movq	(%rbp,%rsi,8), %rbx
	movl	6392(%rbx), %eax
	movw	%ax, img_width(%rip)
	movl	6396(%rbx), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movw	%cx, img_height(%rip)
	movl	6408(%rbx), %ecx
	movl	%ecx, width_pad(%rip)
	movl	6412(%rbx), %ecx
	movl	%ecx, height_pad(%rip)
	testl	%r12d, %r12d
	movl	%r10d, 96(%rsp)         # 4-byte Spill
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	je	.LBB6_9
# BB#7:
	movq	wbp_weight(%rip), %r9
	testl	%edx, %edx
	je	.LBB6_10
# BB#8:
	movslq	%r15d, %rcx
	movq	(%r9,%rcx,8), %rcx
	movq	(%rcx), %rcx
	leaq	(,%rsi,8), %rbx
	movq	(%rcx,%rsi,8), %rcx
	movzwl	(%rcx), %ecx
	movw	%cx, weight1(%rip)
	movq	(%r9,%r13,8), %rcx
	addq	(%rcx), %rbx
	jmp	.LBB6_11
.LBB6_9:
	movb	luma_log_weight_denom(%rip), %cl
	movl	$1, %ebx
	shll	%cl, %ebx
	movw	%bx, weight1(%rip)
	movw	%bx, weight2(%rip)
	movl	$computeBiPredSAD1, %eax
	xorl	%ecx, %ecx
	jmp	.LBB6_12
.LBB6_10:
	movq	(%r9,%r13,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movzwl	(%rcx), %ecx
	movw	%cx, weight1(%rip)
	movslq	%r15d, %rcx
	movq	(%r9,%rcx,8), %rcx
	movq	(%rcx,%rsi,8), %rbx
.LBB6_11:
	movq	(%rbx), %rcx
	movzwl	(%rcx), %ecx
	movw	%cx, weight2(%rip)
	movswl	%r11w, %ecx
	movswl	%r8w, %ebx
	leal	1(%rcx,%rbx), %ecx
	shrl	%ecx
	movl	$computeBiPredSAD2, %eax
.LBB6_12:
	movw	480(%rsp), %bx
	movswl	%bx, %ebx
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movswl	488(%rsp), %ebx
	movq	%rbx, 200(%rsp)         # 8-byte Spill
	movl	528(%rsp), %r8d
	movswl	%r14w, %r9d
	movw	%cx, offsetBi(%rip)
	movq	%rax, computeBiPred(%rip)
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB6_23
# BB#13:
	movq	(%rbp,%rsi,8), %rcx
	movq	6464(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, ref_pic1_sub+8(%rip)
	movq	(%rbp,%rsi,8), %rcx
	movq	6464(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, ref_pic1_sub+16(%rip)
	movq	(%rdi), %rcx
	movq	6464(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, ref_pic2_sub+8(%rip)
	movq	(%rdi), %rcx
	movq	6464(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, ref_pic2_sub+16(%rip)
	movq	(%rbp,%rsi,8), %rcx
	movl	6416(%rcx), %edi
	movl	%edi, width_pad_cr(%rip)
	movl	6420(%rcx), %ecx
	movl	%ecx, height_pad_cr(%rip)
	testl	%r12d, %r12d
	je	.LBB6_16
# BB#14:
	movq	wbp_weight(%rip), %rcx
	testl	%edx, %edx
	je	.LBB6_17
# BB#15:
	movslq	%r15d, %rdi
	movq	(%rcx,%rdi,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movzwl	4(%rbp), %ebx
	movw	%bx, weight1_cr(%rip)
	movzwl	8(%rbp), %ebp
	movw	%bp, weight1_cr+2(%rip)
	movq	(%rcx,%r13,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movzwl	4(%rcx), %ebp
	movw	%bp, weight2_cr(%rip)
	movzwl	8(%rcx), %ecx
	movw	%cx, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rcx
	movq	(%rcx,%rdi,8), %rbp
	movq	(%rcx,%r13,8), %rdi
	jmp	.LBB6_18
.LBB6_16:
	movb	chroma_log_weight_denom(%rip), %cl
	movl	$1, %edx
	shll	%cl, %edx
	movw	%dx, weight1_cr(%rip)
	movw	%dx, weight1_cr+2(%rip)
	movw	%dx, weight2_cr(%rip)
	movw	%dx, weight2_cr+2(%rip)
	movw	$0, offsetBi_cr(%rip)
	xorl	%ecx, %ecx
	jmp	.LBB6_22
.LBB6_17:
	movq	(%rcx,%r13,8), %rbp
	leaq	(,%rsi,8), %rdi
	movq	(%rbp,%rsi,8), %rbp
	movq	(%rbp), %rbp
	movzwl	4(%rbp), %ebx
	movw	%bx, weight1_cr(%rip)
	movzwl	8(%rbp), %ebp
	movw	%bp, weight1_cr+2(%rip)
	movslq	%r15d, %rbx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movzwl	4(%rcx), %ebp
	movw	%bp, weight2_cr(%rip)
	movzwl	8(%rcx), %ecx
	movw	%cx, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rcx
	movq	(%rcx,%r13,8), %rbp
	addq	%rdi, %rbp
	addq	(%rcx,%rbx,8), %rdi
.LBB6_18:
	movq	(%rbp), %rbp
	movl	4(%rbp), %ebp
	movq	(%rdi), %rdi
	movl	4(%rdi), %edi
	leal	1(%rbp,%rdi), %edi
	shrl	%edi
	movw	%di, offsetBi_cr(%rip)
	testl	%edx, %edx
	je	.LBB6_20
# BB#19:
	movslq	%r15d, %rdx
	movq	(%rcx,%rdx,8), %rdx
	movq	(%rcx,%r13,8), %rsi
	jmp	.LBB6_21
.LBB6_20:
	shlq	$3, %rsi
	movq	(%rcx,%r13,8), %rdx
	addq	%rsi, %rdx
	movslq	%r15d, %rdi
	addq	(%rcx,%rdi,8), %rsi
.LBB6_21:
	movq	(%rdx), %rcx
	movl	8(%rcx), %ecx
	movq	(%rsi), %rdx
	movl	8(%rdx), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
.LBB6_22:
	movw	%cx, offsetBi_cr+2(%rip)
.LBB6_23:
	movq	%r14, %r11
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movzwl	472(%rsp), %edx
	movzwl	464(%rsp), %esi
	movl	96(%rsp), %eax          # 4-byte Reload
	addl	136(%rsp), %eax         # 4-byte Folded Reload
	movswl	16(%rsp), %ecx          # 2-byte Folded Reload
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	$1, %ecx
	cmpl	%r8d, %r9d
	movl	$1, %edi
	jle	.LBB6_27
# BB#24:
	cmpl	%r8d, 72(%rsp)          # 4-byte Folded Reload
	movl	$1, %edi
	jle	.LBB6_27
# BB#25:
	movswl	60(%rsp), %edi          # 2-byte Folded Reload
	movl	%r8d, %ebx
	notl	%ebx
	movl	%ebx, %ebp
	subl	36(%rsp), %ebp          # 4-byte Folded Reload
	addl	%edi, %ebp
	cmpl	%ebp, %r9d
	movl	$1, %edi
	jge	.LBB6_27
# BB#26:
	movswl	8(%rsp), %edi           # 2-byte Folded Reload
	subl	28(%rsp), %ebx          # 4-byte Folded Reload
	addl	%edi, %ebx
	xorl	%edi, %edi
	cmpl	%ebx, 72(%rsp)          # 4-byte Folded Reload
	setge	%dil
.LBB6_27:
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movq	144(%rsp), %rbp         # 8-byte Reload
	leal	(,%rbp,4), %r12d
	movswl	%si, %r14d
	movq	136(%rsp), %rsi         # 8-byte Reload
	leal	(,%rsi,4), %r8d
	movswl	%dx, %r13d
	movq	192(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rbp,4), %ebx
	movq	200(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rsi,4), %r10d
	movl	104(%rsp), %edx         # 4-byte Reload
	addl	%ebp, %edx
	movl	%edx, %ebp
	shll	$16, %r11d
	movq	16(%rsp), %r15          # 8-byte Reload
	shll	$16, %r15d
	movl	%edi, bipred2_access_method(%rip)
	movl	%eax, %r9d
	shll	$16, %r9d
	movswl	%ax, %edx
	movl	528(%rsp), %eax
	cmpl	%eax, %edx
	jle	.LBB6_29
# BB#28:
	movswl	8(%rsp), %ecx           # 2-byte Folded Reload
	movl	%eax, %esi
	notl	%esi
	subl	28(%rsp), %esi          # 4-byte Folded Reload
	addl	%ecx, %esi
	xorl	%ecx, %ecx
	cmpl	%esi, %edx
	setge	%cl
.LBB6_29:
	movl	%ecx, bipred1_access_method(%rip)
	movq	mvbits(%rip), %rdx
	movswl	%bp, %ecx
	leal	(,%rcx,4), %esi
	movq	%r12, 152(%rsp)         # 8-byte Spill
	leal	(%r14,%r12), %edi
	subl	%edi, %esi
	movslq	%esi, %rax
	sarl	$14, %r9d
	movq	%r8, %r12
	leal	(%r13,%r12), %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	movslq	%edi, %rbp
	sarl	$14, %r11d
	movl	%r11d, %esi
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	subl	%ebx, %esi
	movslq	%esi, %rsi
	sarl	$14, %r15d
	movl	%r15d, %edi
	movl	%r10d, 60(%rsp)         # 4-byte Spill
	subl	%r10d, %edi
	movslq	%edi, %rdi
	movl	(%rdx,%rdi,4), %ebx
	addl	(%rdx,%rsi,4), %ebx
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movl	(%rdx,%rbp,4), %ebp
	movq	%rax, 104(%rsp)         # 8-byte Spill
	addl	(%rdx,%rax,4), %ebp
	movl	544(%rsp), %eax
	imull	%eax, %ebp
	sarl	$16, %ebp
	imull	%eax, %ebx
	sarl	$16, %ebx
	leal	80(,%rcx,4), %r8d
	addl	$80, %r9d
	addl	$80, %r11d
	addl	$80, %r15d
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	%r8d, 56(%rsp)          # 4-byte Spill
	movl	%r9d, 32(%rsp)          # 4-byte Spill
	pushq	%r15
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	callq	*96(%rsp)               # 8-byte Folded Reload
	addq	$16, %rsp
.Lcfi60:
	.cfi_adjust_cfa_offset -16
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	addl	%ebp, %eax
	addl	%ebx, %eax
	movl	536(%rsp), %ecx
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	orl	%r14d, %r13d
	movw	480(%rsp), %ax
	orl	%r13d, %eax
	orw	488(%rsp), %ax
	movq	%r12, %rsi
	movq	%rsi, 208(%rsp)         # 8-byte Spill
	je	.LBB6_32
# BB#30:
	movq	144(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	88(%rsp), %r11          # 8-byte Reload
	subl	%r11d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movl	528(%rsp), %r8d
	cmpl	%r8d, %ecx
	jle	.LBB6_33
.LBB6_31:
	movl	%r11d, %ebp
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	jmp	.LBB6_35
.LBB6_32:
	movq	88(%rsp), %r11          # 8-byte Reload
	movl	%r11d, %ebp
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	movl	528(%rsp), %r8d
	jmp	.LBB6_35
.LBB6_33:
	movq	136(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	72(%rsp), %rdx          # 8-byte Reload
	subl	%edx, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jle	.LBB6_59
# BB#34:
	movl	%r11d, %ebp
	movl	%edx, %esi
.LBB6_35:
	movl	8(%rsp), %r15d          # 4-byte Reload
	movq	152(%rsp), %r12         # 8-byte Reload
.LBB6_36:
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(,%rax,8), %eax
	movzwl	ConvergeThreshold(%rip), %edx
	movswl	block_type_shift_factor(%rcx,%rcx), %ecx
	shrl	%cl, %edx
	cmpl	%edx, %eax
	jge	.LBB6_44
# BB#37:                                # %.preheader.preheader
	movl	%r15d, %edi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movq	72(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_38:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movswl	Diamond_X(%rbx), %r15d
	movl	%r15d, %eax
	negl	%eax
	testq	%rbx, %rbx
	cmovnel	%r15d, %eax
	cmpl	%r8d, %eax
	jg	.LBB6_43
# BB#39:                                #   in Loop: Header=BB6_38 Depth=1
	movswl	Diamond_Y(%rbx), %r14d
	movl	%r14d, %eax
	negl	%eax
	cmpq	$4, %rbx
	cmovnel	%r14d, %eax
	cmpl	%r8d, %eax
	jg	.LBB6_43
# BB#40:                                #   in Loop: Header=BB6_38 Depth=1
	movq	%rbp, %r13
	addl	%r11d, %r15d
	addl	%r12d, %r14d
	movq	mvbits(%rip), %rcx
	leal	(,%r15,4), %r10d
	movl	%r10d, %eax
	movl	%edi, %edx
	subl	%edx, %eax
	movslq	%eax, %rdx
	leal	(,%r14,4), %eax
	movl	%eax, %esi
	subl	60(%rsp), %esi          # 4-byte Folded Reload
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %ebp
	addl	(%rcx,%rdx,4), %ebp
	movq	96(%rsp), %rdx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %edx
	movq	104(%rsp), %rsi         # 8-byte Reload
	addl	(%rcx,%rsi,4), %edx
	movl	544(%rsp), %ecx
	imull	%ecx, %edx
	sarl	$16, %edx
	imull	%ecx, %ebp
	sarl	$16, %ebp
	addl	%edx, %ebp
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebp, %ecx
	jle	.LBB6_42
# BB#41:                                #   in Loop: Header=BB6_38 Depth=1
	addl	$80, %r10d
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	88(%rsp), %r12          # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -16
	addl	%ebp, %eax
	cmpl	%edx, %eax
	movq	%r13, %rbp
	cmovll	%r15d, %ebp
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmovll	%r14d, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmovlel	%eax, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	jmp	.LBB6_43
.LBB6_42:                               #   in Loop: Header=BB6_38 Depth=1
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB6_43:                               #   in Loop: Header=BB6_38 Depth=1
	addq	$2, %rbx
	cmpq	$8, %rbx
	jne	.LBB6_38
	jmp	.LBB6_159
.LBB6_44:                               # %.preheader1069.preheader
	movl	%ecx, 132(%rsp)         # 4-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_45:                               # %.preheader1069
                                        # =>This Inner Loop Header: Depth=1
	movl	%r15d, %edx
	movswl	Diamond_X(%rbx), %r15d
	movl	%r15d, %eax
	negl	%eax
	testq	%rbx, %rbx
	cmovnel	%r15d, %eax
	cmpl	%r8d, %eax
	jle	.LBB6_47
# BB#46:                                #   in Loop: Header=BB6_45 Depth=1
	movl	%edx, %r15d
	jmp	.LBB6_52
	.p2align	4, 0x90
.LBB6_47:                               #   in Loop: Header=BB6_45 Depth=1
	movswl	Diamond_Y(%rbx), %r14d
	movl	%r14d, %eax
	negl	%eax
	cmpq	$4, %rbx
	cmovnel	%r14d, %eax
	cmpl	%r8d, %eax
	jle	.LBB6_49
# BB#48:                                #   in Loop: Header=BB6_45 Depth=1
	movl	%edx, %r15d
	jmp	.LBB6_52
	.p2align	4, 0x90
.LBB6_49:                               #   in Loop: Header=BB6_45 Depth=1
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rbp, %r12
	addl	%r11d, %r15d
	addl	72(%rsp), %r14d         # 4-byte Folded Reload
	movq	mvbits(%rip), %rcx
	leal	(,%r15,4), %r10d
	movl	%r10d, %eax
	movl	%edx, %edi
	subl	%edi, %eax
	movslq	%eax, %rdx
	leal	(,%r14,4), %eax
	movl	%eax, %esi
	subl	60(%rsp), %esi          # 4-byte Folded Reload
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %ebp
	addl	(%rcx,%rdx,4), %ebp
	movq	96(%rsp), %rdx          # 8-byte Reload
	movl	(%rcx,%rdx,4), %edx
	movq	104(%rsp), %rsi         # 8-byte Reload
	addl	(%rcx,%rsi,4), %edx
	movl	544(%rsp), %ecx
	imull	%ecx, %edx
	sarl	$16, %edx
	imull	%ecx, %ebp
	sarl	$16, %ebp
	addl	%edx, %ebp
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebp, %ecx
	jle	.LBB6_51
# BB#50:                                #   in Loop: Header=BB6_45 Depth=1
	addl	$80, %r10d
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi66:
	.cfi_adjust_cfa_offset -16
	addl	%ebp, %eax
	cmpl	%ecx, %eax
	movq	%r12, %rbp
	cmovll	%r15d, %ebp
	movl	%edx, %r15d
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmovll	%r14d, %esi
	cmovlel	%eax, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	152(%rsp), %r12         # 8-byte Reload
	jmp	.LBB6_52
.LBB6_51:                               #   in Loop: Header=BB6_45 Depth=1
	movq	%r12, %rbp
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	152(%rsp), %r12         # 8-byte Reload
	movl	%edi, %r15d
	.p2align	4, 0x90
.LBB6_52:                               #   in Loop: Header=BB6_45 Depth=1
	addq	$2, %rbx
	cmpq	$8, %rbx
	jne	.LBB6_45
# BB#53:
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(,%rax,4), %eax
	cmpl	$1, 184(%rsp)           # 4-byte Folded Reload
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	jne	.LBB6_55
# BB#54:
	movzwl	SymmetricalCrossSearchThreshold1(%rip), %edx
	movl	132(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	cmpl	%edx, %eax
	jg	.LBB6_56
.LBB6_55:                               # %._crit_edge
	movzwl	SymmetricalCrossSearchThreshold2(%rip), %edx
	movl	132(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	cmpl	%edx, %eax
	jle	.LBB6_58
.LBB6_56:                               # %.preheader1068
	cmpl	$2, %r8d
	jge	.LBB6_61
# BB#57:
	movl	%ebp, %r10d
	movl	%esi, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB6_84
.LBB6_58:
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rsi          # 8-byte Reload
	cmpl	$2, 184(%rsp)           # 4-byte Folded Reload
	jge	.LBB6_105
	jmp	.LBB6_107
.LBB6_59:
	movq	mvbits(%rip), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	(%rax,%rcx,4), %ecx
	movq	104(%rsp), %rdx         # 8-byte Reload
	addl	(%rax,%rdx,4), %ecx
	movl	544(%rsp), %r12d
	imull	%r12d, %ecx
	sarl	$16, %ecx
	movq	192(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	negl	%edx
	movslq	%edx, %rdx
	movq	200(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	negl	%esi
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %ebx
	addl	(%rax,%rdx,4), %ebx
	imull	%r12d, %ebx
	sarl	$16, %ebx
	addl	%ecx, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%ebx, %ecx
	jle	.LBB6_31
# BB#60:
	movq	152(%rsp), %r12         # 8-byte Reload
	leal	80(%r12), %eax
	movq	208(%rsp), %rdx         # 8-byte Reload
	leal	80(%rdx), %ebp
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rbp
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	movq	32(%rsp), %rbp          # 8-byte Reload
	callq	*computeBiPred(%rip)
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi69:
	.cfi_adjust_cfa_offset -16
	addl	%ebx, %eax
	cmpl	%ebp, %eax
	movl	%r11d, %edx
	cmovll	144(%rsp), %edx         # 4-byte Folded Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	cmovll	136(%rsp), %esi         # 4-byte Folded Reload
	cmovgl	%ebp, %eax
	movq	%rdx, %rbp
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r15d          # 4-byte Reload
	jmp	.LBB6_36
.LBB6_61:                               # %.lr.ph
	movl	%r8d, %eax
	shrl	%eax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movl	%ebp, %r14d
	subl	%r11d, %r14d
	movl	%r14d, %eax
	negl	%eax
	cmovll	%r14d, %eax
	movl	%eax, 220(%rsp)         # 4-byte Spill
	leal	(,%rbp,4), %eax
	movq	%rax, 256(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%r15d, %eax
	movl	%esi, %ebx
	movq	%rbp, %r9
	movq	72(%rsp), %rbp          # 8-byte Reload
	subl	%ebp, %ebx
	movl	%ebx, %ecx
	negl	%ecx
	cmovll	%ebx, %ecx
	movl	%ecx, 188(%rsp)         # 4-byte Spill
	movslq	%r9d, %rcx
	movslq	%r11d, %r15
	movslq	40(%rsp), %rsi          # 4-byte Folded Reload
	movslq	%ebp, %rdx
	leal	1(%rsi), %r13d
	leal	-1(%rsi), %r10d
	movq	%r10, 280(%rsp)         # 8-byte Spill
	leal	1(%rbp), %edi
	subl	%esi, %edi
	movq	%rdi, 360(%rsp)         # 8-byte Spill
	movl	%ebp, %r10d
	subl	%esi, %r10d
	movq	%rsi, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	subq	%rdx, %rdi
	movq	%rdi, 272(%rsp)         # 8-byte Spill
	movl	8(%rsp), %edi           # 4-byte Reload
	movq	%rcx, %rdx
	subq	%r15, %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	leal	1(%r11), %r15d
	subl	%ecx, %r15d
	movq	%r15, 384(%rsp)         # 8-byte Spill
	movl	%r11d, %edx
	subl	%ecx, %edx
	leal	1(%rcx), %r15d
	leal	-1(%rcx), %ecx
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	cltq
	movq	%rax, 248(%rsp)         # 8-byte Spill
	leal	(,%rsi,4), %eax
	movq	%rax, 232(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	60(%rsp), %eax          # 4-byte Folded Reload
	cltq
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	negl	%eax
	subl	208(%rsp), %eax         # 4-byte Folded Reload
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	negl	%ebp
	movq	%r13, 288(%rsp)         # 8-byte Spill
	addq	%r13, %rbp
	movq	%rbp, 352(%rsp)         # 8-byte Spill
	movq	192(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	negl	%ecx
	subl	%r12d, %ecx
	movl	%r11d, %ebp
	negl	%ebp
	movq	%r15, 328(%rsp)         # 8-byte Spill
	addq	%r15, %rbp
	movl	%edi, %r15d
	movq	%rbp, 376(%rsp)         # 8-byte Spill
	decl	%ebx
	movq	%rbx, 368(%rsp)         # 8-byte Spill
	leal	(%rax,%rsi,4), %ebx
	movq	%rbx, 312(%rsp)         # 8-byte Spill
	leal	-1(%rax,%rsi,4), %eax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	leal	(%rcx,%r9,4), %eax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	leal	-1(%rcx,%r9,4), %eax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	decl	%r14d
	movq	%r14, 392(%rsp)         # 8-byte Spill
	leal	80(,%r9,4), %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leal	80(,%rsi,4), %eax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	%esi, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%r10, %rsi
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movl	%r9d, %r10d
	movq	168(%rsp), %r12         # 8-byte Reload
	movq	%rdx, %rbx
	.p2align	4, 0x90
.LBB6_62:                               # =>This Inner Loop Header: Depth=1
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	(%r12,%r13), %rax
	leal	-1(%rbx), %ecx
	movq	376(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r13), %edx
	incq	%rax
	cmovsl	%ecx, %edx
	cmpl	%r8d, %edx
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	jg	.LBB6_67
# BB#63:                                #   in Loop: Header=BB6_62 Depth=1
	cmpl	%r8d, 188(%rsp)         # 4-byte Folded Reload
	jg	.LBB6_67
# BB#64:                                #   in Loop: Header=BB6_62 Depth=1
	movl	%r15d, %edi
	movq	mvbits(%rip), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	(%rax,%rcx,4), %ecx
	movq	104(%rsp), %rdx         # 8-byte Reload
	addl	(%rax,%rdx,4), %ecx
	movl	544(%rsp), %edx
	movl	%edx, %r15d
	imull	%r15d, %ecx
	sarl	$16, %ecx
	movq	344(%rsp), %rdx         # 8-byte Reload
	leal	4(%rdx,%rbp), %edx
	movslq	%edx, %rdx
	movq	264(%rsp), %rbx         # 8-byte Reload
	movl	(%rax,%rbx,4), %ebx
	addl	(%rax,%rdx,4), %ebx
	imull	%r15d, %ebx
	sarl	$16, %ebx
	addl	%ecx, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%ebx, %ecx
	jle	.LBB6_66
# BB#65:                                #   in Loop: Header=BB6_62 Depth=1
	movq	328(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	256(%rsp), %rax         # 8-byte Reload
	leal	84(%rax,%rbp), %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	%rsi, %rbp
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	240(%rsp)               # 8-byte Folded Reload
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	movl	%r10d, %r12d
	callq	*computeBiPred(%rip)
	movq	%rbp, %rsi
	movq	192(%rsp), %rbp         # 8-byte Reload
	movl	%r12d, %r10d
	movq	184(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	24(%rsp), %r15d         # 4-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi72:
	.cfi_adjust_cfa_offset -16
	addl	%ebx, %eax
	cmpl	%edx, %eax
	cmovll	160(%rsp), %r10d        # 4-byte Folded Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovll	40(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	cmovlel	%eax, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB6_67
	.p2align	4, 0x90
.LBB6_66:                               #   in Loop: Header=BB6_62 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	%edi, %r15d
.LBB6_67:                               #   in Loop: Header=BB6_62 Depth=1
	leaq	(%r12,%r14), %rax
	movq	384(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r13), %ecx
	movq	392(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	decq	%rax
	cmovsl	%ecx, %edx
	cmpl	%r8d, %edx
	jg	.LBB6_72
# BB#68:                                #   in Loop: Header=BB6_62 Depth=1
	cmpl	%r8d, 188(%rsp)         # 4-byte Folded Reload
	jg	.LBB6_72
# BB#69:                                #   in Loop: Header=BB6_62 Depth=1
	movl	%r15d, %edi
	movq	mvbits(%rip), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	(%rax,%rcx,4), %ecx
	movq	104(%rsp), %rdx         # 8-byte Reload
	addl	(%rax,%rdx,4), %ecx
	movl	544(%rsp), %edx
	movl	%edx, %ebp
	imull	%ebp, %ecx
	sarl	$16, %ecx
	movq	336(%rsp), %rdx         # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	leal	-3(%rdx,%r15), %edx
	movslq	%edx, %rdx
	movq	264(%rsp), %rbx         # 8-byte Reload
	movl	(%rax,%rbx,4), %ebx
	addl	(%rax,%rdx,4), %ebx
	imull	%ebp, %ebx
	sarl	$16, %ebx
	addl	%ecx, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%ebx, %ecx
	jle	.LBB6_73
# BB#70:                                #   in Loop: Header=BB6_62 Depth=1
	movq	320(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %ebp
	movq	256(%rsp), %rax         # 8-byte Reload
	leal	76(%rax,%r15), %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	240(%rsp)               # 8-byte Folded Reload
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	movl	%r10d, %r12d
	callq	*computeBiPred(%rip)
	movq	176(%rsp), %rsi         # 8-byte Reload
	movl	%r12d, %r10d
	movq	184(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset -16
	addl	%ebx, %eax
	cmpl	%edx, %eax
	cmovll	%ebp, %r10d
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovll	40(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	cmovlel	%eax, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rax
	movl	%edi, %r15d
	movq	176(%rsp), %rbp         # 8-byte Reload
	cmpl	%r8d, 220(%rsp)         # 4-byte Folded Reload
	jle	.LBB6_74
	jmp	.LBB6_83
	.p2align	4, 0x90
.LBB6_72:                               #   in Loop: Header=BB6_62 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	%r8d, 220(%rsp)         # 4-byte Folded Reload
	jle	.LBB6_74
	jmp	.LBB6_83
	.p2align	4, 0x90
.LBB6_73:                               #   in Loop: Header=BB6_62 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rax
	movq	176(%rsp), %rbp         # 8-byte Reload
	movl	%edi, %r15d
	cmpl	%r8d, 220(%rsp)         # 4-byte Folded Reload
	jg	.LBB6_83
.LBB6_74:                               #   in Loop: Header=BB6_62 Depth=1
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13), %rax
	leal	-1(%rsi), %ecx
	movq	352(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r13), %edx
	incq	%rax
	cmovsl	%ecx, %edx
	cmpl	%r8d, %edx
	jg	.LBB6_78
# BB#75:                                #   in Loop: Header=BB6_62 Depth=1
	movl	%r15d, %edi
	movq	mvbits(%rip), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	(%rax,%rcx,4), %ecx
	movq	104(%rsp), %rdx         # 8-byte Reload
	addl	(%rax,%rdx,4), %ecx
	movl	544(%rsp), %edx
	movl	%edx, %r15d
	imull	%r15d, %ecx
	sarl	$16, %ecx
	movq	312(%rsp), %rdx         # 8-byte Reload
	leal	4(%rdx,%rbp), %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	movq	248(%rsp), %rdx         # 8-byte Reload
	addl	(%rax,%rdx,4), %ebx
	imull	%r15d, %ebx
	sarl	$16, %ebx
	addl	%ecx, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%ebx, %ecx
	jle	.LBB6_77
# BB#76:                                #   in Loop: Header=BB6_62 Depth=1
	movq	288(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r13), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	232(%rsp), %rax         # 8-byte Reload
	leal	84(%rax,%rbp), %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)               # 8-byte Folded Reload
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	movl	%r10d, %r12d
	callq	*computeBiPred(%rip)
	movq	176(%rsp), %rsi         # 8-byte Reload
	movl	%r12d, %r10d
	movq	184(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	24(%rsp), %r15d         # 4-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi78:
	.cfi_adjust_cfa_offset -16
	addl	%ebx, %eax
	cmpl	%edx, %eax
	cmovll	48(%rsp), %r10d         # 4-byte Folded Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovll	296(%rsp), %ecx         # 4-byte Folded Reload
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	cmovlel	%eax, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB6_78
.LBB6_77:                               #   in Loop: Header=BB6_62 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	%edi, %r15d
.LBB6_78:                               #   in Loop: Header=BB6_62 Depth=1
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	360(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r13), %ecx
	movq	368(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%r14), %edx
	decq	%rax
	cmovsl	%ecx, %edx
	cmpl	%r8d, %edx
	jle	.LBB6_80
# BB#79:                                #   in Loop: Header=BB6_62 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	jmp	.LBB6_83
	.p2align	4, 0x90
.LBB6_80:                               #   in Loop: Header=BB6_62 Depth=1
	movl	%r15d, %edi
	movq	mvbits(%rip), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	(%rax,%rcx,4), %ecx
	movq	104(%rsp), %rdx         # 8-byte Reload
	addl	(%rax,%rdx,4), %ecx
	movl	544(%rsp), %edx
	movl	%edx, %ebp
	imull	%ebp, %ecx
	sarl	$16, %ecx
	movq	304(%rsp), %rdx         # 8-byte Reload
	movq	80(%rsp), %r15          # 8-byte Reload
	leal	-3(%rdx,%r15), %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebx
	movq	248(%rsp), %rdx         # 8-byte Reload
	addl	(%rax,%rdx,4), %ebx
	imull	%ebp, %ebx
	sarl	$16, %ebx
	addl	%ecx, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%ebx, %ecx
	jle	.LBB6_82
# BB#81:                                #   in Loop: Header=BB6_62 Depth=1
	movq	280(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %ebp
	movq	232(%rsp), %rax         # 8-byte Reload
	leal	76(%rax,%r15), %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)               # 8-byte Folded Reload
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	movl	%r10d, %r12d
	callq	*computeBiPred(%rip)
	movq	176(%rsp), %rsi         # 8-byte Reload
	movl	%r12d, %r10d
	movq	184(%rsp), %r12         # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	24(%rsp), %edi          # 4-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi81:
	.cfi_adjust_cfa_offset -16
	addl	%ebx, %eax
	cmpl	%edx, %eax
	cmovll	48(%rsp), %r10d         # 4-byte Folded Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovll	%ebp, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	cmovlel	%eax, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rax
	movl	%edi, %r15d
	movq	176(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB6_83
.LBB6_82:                               #   in Loop: Header=BB6_62 Depth=1
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%r15, %rax
	movq	176(%rsp), %rbp         # 8-byte Reload
	movl	%edi, %r15d
	.p2align	4, 0x90
.LBB6_83:                               # %.thread1047
                                        #   in Loop: Header=BB6_62 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, %rdx
	incq	%rdx
	addq	$2, %r13
	addl	$8, %ebp
	addq	$-2, %r14
	addl	$-8, %eax
	addl	$-2, %esi
	addl	$-2, %ebx
	movq	%rdx, %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	cmpq	400(%rsp), %rdx         # 8-byte Folded Reload
	jl	.LBB6_62
.LBB6_84:                               # %.preheader1067
	movq	$-12, %rbx
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movl	%r10d, 64(%rsp)         # 4-byte Spill
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%r10d, 48(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB6_85:                               # =>This Inner Loop Header: Depth=1
	movswl	Hexagon_X+12(%rbx), %r13d
	addl	%r10d, %r13d
	movl	%r13d, %eax
	subl	%r11d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_91
# BB#86:                                #   in Loop: Header=BB6_85 Depth=1
	movswl	Hexagon_Y+12(%rbx), %r14d
	addl	12(%rsp), %r14d         # 4-byte Folded Reload
	movl	%r14d, %eax
	subl	%esi, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_91
# BB#87:                                #   in Loop: Header=BB6_85 Depth=1
	movl	%r8d, %r12d
	movq	mvbits(%rip), %rcx
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rcx,%rax,4), %edx
	movq	104(%rsp), %rax         # 8-byte Reload
	addl	(%rcx,%rax,4), %edx
	movl	544(%rsp), %eax
	movl	%r15d, %edi
	movl	%eax, %r15d
	imull	%r15d, %edx
	sarl	$16, %edx
	leal	(,%r13,4), %r10d
	movl	%r10d, %eax
	subl	%edi, %eax
	movslq	%eax, %r9
	leal	(,%r14,4), %eax
	movl	%eax, %edi
	subl	60(%rsp), %edi          # 4-byte Folded Reload
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %ebp
	addl	(%rcx,%r9,4), %ebp
	imull	%r15d, %ebp
	sarl	$16, %ebp
	addl	%edx, %ebp
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebp, %ecx
	jle	.LBB6_89
# BB#88:                                #   in Loop: Header=BB6_85 Depth=1
	addl	$80, %r10d
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi84:
	.cfi_adjust_cfa_offset -16
	addl	%ebp, %eax
	cmpl	%ecx, %eax
	movl	64(%rsp), %edx          # 4-byte Reload
	cmovll	%r13d, %edx
	movl	%edx, 64(%rsp)          # 4-byte Spill
	movl	80(%rsp), %edx          # 4-byte Reload
	cmovll	%r14d, %edx
	movl	%edx, 80(%rsp)          # 4-byte Spill
	cmovlel	%eax, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r15d          # 4-byte Reload
	jmp	.LBB6_90
.LBB6_89:                               #   in Loop: Header=BB6_85 Depth=1
	movl	8(%rsp), %r15d          # 4-byte Reload
	movl	%r12d, %r8d
.LBB6_90:                               #   in Loop: Header=BB6_85 Depth=1
	movl	48(%rsp), %r10d         # 4-byte Reload
.LBB6_91:                               #   in Loop: Header=BB6_85 Depth=1
	addq	$2, %rbx
	jne	.LBB6_85
# BB#92:                                # %.preheader1065
	cmpl	$4, %r8d
	jge	.LBB6_94
# BB#93:
	movl	64(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	80(%rsp), %eax          # 4-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	$2, 184(%rsp)           # 4-byte Folded Reload
	jge	.LBB6_105
	jmp	.LBB6_107
.LBB6_94:                               # %.preheader1064.preheader
	movl	%r8d, %eax
	sarl	$31, %eax
	shrl	$30, %eax
	addl	%r8d, %eax
	sarl	$2, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	$1, %r13d
	movl	80(%rsp), %edx          # 4-byte Reload
	movl	%edx, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	64(%rsp), %edi          # 4-byte Reload
	movl	%edi, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_95:                               # %.preheader1064
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_96 Depth 2
	movq	$-32, %rbp
	.p2align	4, 0x90
.LBB6_96:                               #   Parent Loop BB6_95 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	Big_Hexagon_X+32(%rbp), %ebx
	imull	%r13d, %ebx
	addl	%edi, %ebx
	movl	%ebx, %eax
	subl	%r11d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_102
# BB#97:                                #   in Loop: Header=BB6_96 Depth=2
	movswl	Big_Hexagon_Y+32(%rbp), %r14d
	imull	%r13d, %r14d
	addl	%edx, %r14d
	movl	%r14d, %eax
	subl	%esi, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_102
# BB#98:                                #   in Loop: Header=BB6_96 Depth=2
	movq	%r11, %r8
	movq	%rsi, %r9
	movq	mvbits(%rip), %rcx
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rcx,%rax,4), %edx
	movq	104(%rsp), %rax         # 8-byte Reload
	addl	(%rcx,%rax,4), %edx
	movl	544(%rsp), %r11d
	movq	16(%rsp), %r12          # 8-byte Reload
	imull	%r11d, %edx
	sarl	$16, %edx
	leal	(,%rbx,4), %r10d
	movl	%r10d, %eax
	subl	%r15d, %eax
	movslq	%eax, %rsi
	leal	(,%r14,4), %eax
	movl	%eax, %edi
	subl	60(%rsp), %edi          # 4-byte Folded Reload
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %r15d
	addl	(%rcx,%rsi,4), %r15d
	imull	%r11d, %r15d
	movq	%r12, %rcx
	sarl	$16, %r15d
	addl	%edx, %r15d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r15d, %ecx
	jle	.LBB6_100
# BB#99:                                #   in Loop: Header=BB6_96 Depth=2
	addl	$80, %r10d
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi87:
	.cfi_adjust_cfa_offset -16
	addl	%r15d, %eax
	cmpl	%r12d, %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	cmovll	%ebx, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmovll	%r14d, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmovlel	%eax, %r12d
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r15d          # 4-byte Reload
	jmp	.LBB6_101
.LBB6_100:                              #   in Loop: Header=BB6_96 Depth=2
	movl	8(%rsp), %r15d          # 4-byte Reload
	movq	%r9, %rsi
	movq	%r8, %r11
	movl	528(%rsp), %r8d
.LBB6_101:                              #   in Loop: Header=BB6_96 Depth=2
	movl	80(%rsp), %edx          # 4-byte Reload
	movl	64(%rsp), %edi          # 4-byte Reload
.LBB6_102:                              #   in Loop: Header=BB6_96 Depth=2
	addq	$2, %rbp
	jne	.LBB6_96
# BB#103:                               #   in Loop: Header=BB6_95 Depth=1
	cmpl	12(%rsp), %r13d         # 4-byte Folded Reload
	leal	1(%r13), %eax
	movl	%eax, %r13d
	jl	.LBB6_95
# BB#104:                               # %.loopexit1066
	cmpl	$2, 184(%rsp)           # 4-byte Folded Reload
	jl	.LBB6_107
.LBB6_105:
	movzwl	smpUMHEX_pred_MV_uplayer_X(%rip), %eax
	movw	%ax, %cx
	sarw	$15, %cx
	andl	$49152, %ecx            # imm = 0xC000
	shrl	$14, %ecx
	addl	%eax, %ecx
	sarw	$2, %cx
	movswl	%cx, %ebx
	addl	144(%rsp), %ebx         # 4-byte Folded Reload
	movl	%ebx, %eax
	subl	%r11d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_107
# BB#106:
	movzwl	smpUMHEX_pred_MV_uplayer_Y(%rip), %eax
	movw	%ax, %cx
	sarw	$15, %cx
	andl	$49152, %ecx            # imm = 0xC000
	shrl	$14, %ecx
	addl	%eax, %ecx
	sarw	$2, %cx
	movswl	%cx, %r14d
	addl	136(%rsp), %r14d        # 4-byte Folded Reload
	movl	%r14d, %eax
	subl	%esi, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jle	.LBB6_111
.LBB6_107:
	movl	%r15d, %r14d
	movq	%rsi, %r9
	movl	544(%rsp), %r15d
	movl	32(%rsp), %r12d         # 4-byte Reload
.LBB6_108:
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	cmpl	144(%rsp), %r11d        # 4-byte Folded Reload
	jne	.LBB6_113
.LBB6_109:
	cmpl	136(%rsp), %r9d         # 4-byte Folded Reload
	jne	.LBB6_113
# BB#110:
	movl	%ebp, %edi
	movl	%edx, %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movl	132(%rsp), %ecx         # 4-byte Reload
	jmp	.LBB6_124
.LBB6_111:
	movq	%rsi, %r9
	movq	mvbits(%rip), %rcx
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rcx,%rax,4), %edx
	movq	104(%rsp), %rax         # 8-byte Reload
	addl	(%rcx,%rax,4), %edx
	movl	%r15d, %esi
	movl	544(%rsp), %r15d
	imull	%r15d, %edx
	sarl	$16, %edx
	leal	(,%rbx,4), %r10d
	movl	%r10d, %eax
	subl	%esi, %eax
	movslq	%eax, %rsi
	leal	(,%r14,4), %eax
	movl	%eax, %edi
	subl	60(%rsp), %edi          # 4-byte Folded Reload
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %ebp
	addl	(%rcx,%rsi,4), %ebp
	imull	%r15d, %ebp
	sarl	$16, %ebp
	addl	%edx, %ebp
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebp, %ecx
	movl	32(%rsp), %r12d         # 4-byte Reload
	jle	.LBB6_160
# BB#112:
	addl	$80, %r10d
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	%r12d, %r9d
	pushq	%rax
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi89:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset -16
	addl	%ebp, %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	%ecx, %eax
	movq	48(%rsp), %rbp          # 8-byte Reload
	cmovll	%ebx, %ebp
	movq	40(%rsp), %rdx          # 8-byte Reload
	cmovll	%r14d, %edx
	cmovgl	%ecx, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r14d          # 4-byte Reload
	cmpl	144(%rsp), %r11d        # 4-byte Folded Reload
	je	.LBB6_109
.LBB6_113:
	movq	144(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%r11d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	jg	.LBB6_117
# BB#114:
	movq	136(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_117
# BB#115:
	movq	mvbits(%rip), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	(%rax,%rcx,4), %ecx
	movq	104(%rsp), %rdx         # 8-byte Reload
	addl	(%rax,%rdx,4), %ecx
	imull	%r15d, %ecx
	sarl	$16, %ecx
	movq	192(%rsp), %rdx         # 8-byte Reload
	negl	%edx
	movslq	%edx, %rdx
	movq	200(%rsp), %rsi         # 8-byte Reload
	negl	%esi
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %ebx
	addl	(%rax,%rdx,4), %ebx
	imull	%r15d, %ebx
	sarl	$16, %ebx
	addl	%ecx, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%ebx, %ecx
	jle	.LBB6_117
# BB#116:
	movq	152(%rsp), %r10         # 8-byte Reload
	addl	$80, %r10d
	movq	208(%rsp), %rax         # 8-byte Reload
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	%r12d, %r9d
	pushq	%rax
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	movq	32(%rsp), %r15          # 8-byte Reload
	callq	*computeBiPred(%rip)
	movl	24(%rsp), %r14d         # 4-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi93:
	.cfi_adjust_cfa_offset -16
	addl	%ebx, %eax
	cmpl	%r15d, %eax
	cmovll	144(%rsp), %ebp         # 4-byte Folded Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmovll	136(%rsp), %ecx         # 4-byte Folded Reload
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmovgl	%r15d, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB6_117:
	movq	$-8, %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movl	%ebp, %edi
	movl	132(%rsp), %ecx         # 4-byte Reload
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_118:                              # =>This Inner Loop Header: Depth=1
	movswl	Diamond_X+8(%rbx), %r15d
	addl	%ebp, %r15d
	movl	%r15d, %eax
	subl	%r11d, %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	cmpl	%r8d, %edx
	jg	.LBB6_123
# BB#119:                               #   in Loop: Header=BB6_118 Depth=1
	movswl	Diamond_Y+8(%rbx), %r12d
	addl	40(%rsp), %r12d         # 4-byte Folded Reload
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	cmpl	%r8d, %edx
	jg	.LBB6_123
# BB#120:                               #   in Loop: Header=BB6_118 Depth=1
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	%r11, %r8
	movq	mvbits(%rip), %rcx
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rcx,%rax,4), %edx
	movq	104(%rsp), %rax         # 8-byte Reload
	addl	(%rcx,%rax,4), %edx
	movl	544(%rsp), %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	movl	%eax, %r11d
	imull	%r11d, %edx
	sarl	$16, %edx
	leal	(,%r15,4), %r10d
	movl	%r10d, %eax
	subl	%r14d, %eax
	movslq	%eax, %rsi
	leal	(,%r12,4), %eax
	movl	%eax, %edi
	subl	60(%rsp), %edi          # 4-byte Folded Reload
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %ebp
	addl	(%rcx,%rsi,4), %ebp
	imull	%r11d, %ebp
	movq	%r13, %rcx
	sarl	$16, %ebp
	addl	%edx, %ebp
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebp, %ecx
	jle	.LBB6_122
# BB#121:                               #   in Loop: Header=BB6_118 Depth=1
	addl	$80, %r10d
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi96:
	.cfi_adjust_cfa_offset -16
	addl	%ebp, %eax
	cmpl	%r13d, %eax
	movl	12(%rsp), %edi          # 4-byte Reload
	cmovll	%r15d, %edi
	movl	120(%rsp), %ecx         # 4-byte Reload
	cmovll	%r12d, %ecx
	movl	%ecx, 120(%rsp)         # 4-byte Spill
	cmovlel	%eax, %r13d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r14d          # 4-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	132(%rsp), %ecx         # 4-byte Reload
	addq	$2, %rbx
	jne	.LBB6_118
	jmp	.LBB6_124
.LBB6_122:                              #   in Loop: Header=BB6_118 Depth=1
	movl	8(%rsp), %r14d          # 4-byte Reload
	movq	%r8, %r11
	movl	528(%rsp), %r8d
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	132(%rsp), %ecx         # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	.p2align	4, 0x90
.LBB6_123:                              #   in Loop: Header=BB6_118 Depth=1
	addq	$2, %rbx
	jne	.LBB6_118
.LBB6_124:                              # %.loopexit1063
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(,%rax,4), %eax
	movzwl	ConvergeThreshold(%rip), %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	cmpl	%edx, %eax
	jge	.LBB6_133
# BB#125:                               # %.preheader1056.preheader
	movq	$-8, %rbx
	movl	120(%rsp), %edx         # 4-byte Reload
	movl	%edx, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%edi, %ebp
	movl	%edi, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB6_126:                              # %.preheader1056
                                        # =>This Inner Loop Header: Depth=1
	movswl	Diamond_X+8(%rbx), %r15d
	addl	%edi, %r15d
	movl	%r15d, %eax
	subl	%r11d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_132
# BB#127:                               #   in Loop: Header=BB6_126 Depth=1
	movswl	Diamond_Y+8(%rbx), %r12d
	addl	%edx, %r12d
	movl	%r12d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_132
# BB#128:                               #   in Loop: Header=BB6_126 Depth=1
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%r11, %r8
	movq	mvbits(%rip), %rcx
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rcx,%rax,4), %edx
	movq	104(%rsp), %rax         # 8-byte Reload
	addl	(%rcx,%rax,4), %edx
	movl	544(%rsp), %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	movl	%eax, %r11d
	imull	%r11d, %edx
	sarl	$16, %edx
	leal	(,%r15,4), %r10d
	movl	%r10d, %eax
	subl	%r14d, %eax
	movslq	%eax, %rsi
	leal	(,%r12,4), %eax
	movl	%eax, %edi
	subl	60(%rsp), %edi          # 4-byte Folded Reload
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %ebp
	addl	(%rcx,%rsi,4), %ebp
	imull	%r11d, %ebp
	movq	%r13, %rcx
	sarl	$16, %ebp
	addl	%edx, %ebp
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebp, %ecx
	jle	.LBB6_130
# BB#129:                               #   in Loop: Header=BB6_126 Depth=1
	addl	$80, %r10d
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi99:
	.cfi_adjust_cfa_offset -16
	addl	%ebp, %eax
	cmpl	%r13d, %eax
	movq	48(%rsp), %rbp          # 8-byte Reload
	cmovll	%r15d, %ebp
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmovll	%r12d, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmovlel	%eax, %r13d
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r14d          # 4-byte Reload
	jmp	.LBB6_131
.LBB6_130:                              #   in Loop: Header=BB6_126 Depth=1
	movl	8(%rsp), %r14d          # 4-byte Reload
	movq	%r8, %r11
	movl	528(%rsp), %r8d
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB6_131:                              #   in Loop: Header=BB6_126 Depth=1
	movl	120(%rsp), %edx         # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
.LBB6_132:                              #   in Loop: Header=BB6_126 Depth=1
	addq	$2, %rbx
	jne	.LBB6_126
	jmp	.LBB6_159
.LBB6_133:                              # %.preheader1062
	testl	%r8d, %r8d
	jle	.LBB6_156
# BB#134:                               # %.preheader1061.preheader
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movl	120(%rsp), %edx         # 4-byte Reload
	.p2align	4, 0x90
.LBB6_135:                              # %.preheader1061
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_136 Depth 2
	movq	$-12, %r13
	movl	%edx, 64(%rsp)          # 4-byte Spill
	movl	%edi, %esi
	movl	%edx, 120(%rsp)         # 4-byte Spill
	movl	%edi, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB6_136:                              #   Parent Loop BB6_135 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	Hexagon_X+12(%r13), %ebx
	addl	%edi, %ebx
	movl	%ebx, %eax
	subl	%r11d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_141
# BB#137:                               #   in Loop: Header=BB6_136 Depth=2
	movswl	Hexagon_Y+12(%r13), %ebp
	addl	%edx, %ebp
	movl	%ebp, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_141
# BB#138:                               #   in Loop: Header=BB6_136 Depth=2
	movl	%esi, 80(%rsp)          # 4-byte Spill
	movl	%r8d, %r15d
	movq	%r11, %r8
	movq	mvbits(%rip), %rcx
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rcx,%rax,4), %edx
	movq	104(%rsp), %rax         # 8-byte Reload
	addl	(%rcx,%rax,4), %edx
	movl	544(%rsp), %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	%eax, %r11d
	imull	%r11d, %edx
	sarl	$16, %edx
	leal	(,%rbx,4), %r10d
	movl	%r10d, %eax
	subl	%r14d, %eax
	movslq	%eax, %rsi
	leal	(,%rbp,4), %eax
	movl	%eax, %edi
	subl	60(%rsp), %edi          # 4-byte Folded Reload
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %r14d
	addl	(%rcx,%rsi,4), %r14d
	imull	%r11d, %r14d
	movq	%r12, %rcx
	sarl	$16, %r14d
	addl	%edx, %r14d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r14d, %ecx
	jle	.LBB6_140
# BB#139:                               #   in Loop: Header=BB6_136 Depth=2
	addl	$80, %r10d
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi102:
	.cfi_adjust_cfa_offset -16
	addl	%r14d, %eax
	cmpl	%r12d, %eax
	movl	80(%rsp), %esi          # 4-byte Reload
	cmovll	%ebx, %esi
	movl	64(%rsp), %ecx          # 4-byte Reload
	cmovll	%ebp, %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	cmovlel	%eax, %r12d
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r14d          # 4-byte Reload
	movl	120(%rsp), %edx         # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	addq	$2, %r13
	jne	.LBB6_136
	jmp	.LBB6_142
.LBB6_140:                              #   in Loop: Header=BB6_136 Depth=2
	movl	8(%rsp), %r14d          # 4-byte Reload
	movq	%r8, %r11
	movl	%r15d, %r8d
	movl	120(%rsp), %edx         # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movl	80(%rsp), %esi          # 4-byte Reload
	.p2align	4, 0x90
.LBB6_141:                              #   in Loop: Header=BB6_136 Depth=2
	addq	$2, %r13
	jne	.LBB6_136
.LBB6_142:                              #   in Loop: Header=BB6_135 Depth=1
	cmpl	%edi, %esi
	setne	%al
	movl	64(%rsp), %edi          # 4-byte Reload
	cmpl	%edx, %edi
	setne	%cl
	orb	%al, %cl
	cmpb	$1, %cl
	jne	.LBB6_144
# BB#143:                               #   in Loop: Header=BB6_135 Depth=1
	movl	48(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	cmpl	%r8d, %eax
	movl	%edi, %edx
	movl	%esi, %edi
	jl	.LBB6_135
.LBB6_144:                              # %.preheader1059
	testl	%r8d, %r8d
	jle	.LBB6_157
# BB#145:                               # %.preheader1058.preheader
	xorl	%eax, %eax
	movl	64(%rsp), %edx          # 4-byte Reload
	.p2align	4, 0x90
.LBB6_146:                              # %.preheader1058
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_147 Depth 2
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	$-8, %rbx
	movl	%edx, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%esi, %ebp
	movl	%r14d, %edi
	movl	%edx, 64(%rsp)          # 4-byte Spill
	movl	%esi, 80(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB6_147:                              #   Parent Loop BB6_146 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	Diamond_X+8(%rbx), %r14d
	addl	%esi, %r14d
	movl	%r14d, %eax
	subl	%r11d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_153
# BB#148:                               #   in Loop: Header=BB6_147 Depth=2
	movswl	Diamond_Y+8(%rbx), %r13d
	addl	%edx, %r13d
	movl	%r13d, %eax
	subl	%r9d, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	cmpl	%r8d, %ecx
	jg	.LBB6_153
# BB#149:                               #   in Loop: Header=BB6_147 Depth=2
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movl	%r8d, %r15d
	movq	%r11, %r8
	movq	mvbits(%rip), %rcx
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	(%rcx,%rax,4), %edx
	movq	104(%rsp), %rax         # 8-byte Reload
	addl	(%rcx,%rax,4), %edx
	movl	544(%rsp), %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	%eax, %r11d
	imull	%r11d, %edx
	sarl	$16, %edx
	leal	(,%r14,4), %r10d
	movl	%r10d, %eax
	subl	%edi, %eax
	movslq	%eax, %rsi
	leal	(,%r13,4), %eax
	movl	%eax, %edi
	subl	60(%rsp), %edi          # 4-byte Folded Reload
	movslq	%edi, %rdi
	movl	(%rcx,%rdi,4), %ebp
	addl	(%rcx,%rsi,4), %ebp
	imull	%r11d, %ebp
	movq	%r12, %rcx
	sarl	$16, %ebp
	addl	%edx, %ebp
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%ebp, %ecx
	jle	.LBB6_151
# BB#150:                               #   in Loop: Header=BB6_147 Depth=2
	addl	$80, %r10d
	addl	$80, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movl	36(%rsp), %edx          # 4-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	pushq	%rax
.Lcfi103:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	104(%rsp), %r11         # 8-byte Reload
	movl	544(%rsp), %r8d
	addq	$16, %rsp
.Lcfi105:
	.cfi_adjust_cfa_offset -16
	addl	%ebp, %eax
	cmpl	%r12d, %eax
	movq	48(%rsp), %rbp          # 8-byte Reload
	cmovll	%r14d, %ebp
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmovll	%r13d, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmovlel	%eax, %r12d
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movl	8(%rsp), %edi           # 4-byte Reload
	jmp	.LBB6_152
.LBB6_151:                              #   in Loop: Header=BB6_147 Depth=2
	movl	8(%rsp), %edi           # 4-byte Reload
	movq	%r8, %r11
	movl	%r15d, %r8d
	movq	48(%rsp), %rbp          # 8-byte Reload
.LBB6_152:                              #   in Loop: Header=BB6_147 Depth=2
	movl	64(%rsp), %edx          # 4-byte Reload
	movl	80(%rsp), %esi          # 4-byte Reload
.LBB6_153:                              #   in Loop: Header=BB6_147 Depth=2
	addq	$2, %rbx
	jne	.LBB6_147
# BB#154:                               #   in Loop: Header=BB6_146 Depth=1
	cmpl	%esi, %ebp
	setne	%al
	movl	%edx, %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	cmpl	%ecx, %edx
	setne	%cl
	orb	%al, %cl
	cmpb	$1, %cl
	jne	.LBB6_159
# BB#155:                               #   in Loop: Header=BB6_146 Depth=1
	movl	%edi, %r14d
	movl	12(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	%r8d, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%ebp, %esi
	jl	.LBB6_146
	jmp	.LBB6_159
.LBB6_156:
	movl	%edi, %ebp
	movl	120(%rsp), %eax         # 4-byte Reload
	jmp	.LBB6_158
.LBB6_157:
	movl	%esi, %ebp
	movl	64(%rsp), %eax          # 4-byte Reload
.LBB6_158:                              # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB6_159:                              # %.loopexit
	subl	144(%rsp), %ebp         # 4-byte Folded Reload
	movq	496(%rsp), %rax
	movw	%bp, (%rax)
	movq	40(%rsp), %rcx          # 8-byte Reload
	subl	136(%rsp), %ecx         # 4-byte Folded Reload
	movq	504(%rsp), %rax
	movw	%cx, (%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_160:
	movl	8(%rsp), %r14d          # 4-byte Reload
	jmp	.LBB6_108
.Lfunc_end6:
	.size	smpUMHEXBipredIntegerPelBlockMotionSearch, .Lfunc_end6-smpUMHEXBipredIntegerPelBlockMotionSearch
	.cfi_endproc

	.globl	smpUMHEX_decide_intrabk_SAD
	.p2align	4, 0x90
	.type	smpUMHEX_decide_intrabk_SAD,@function
smpUMHEX_decide_intrabk_SAD:            # @smpUMHEX_decide_intrabk_SAD
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	cmpl	$2, 20(%rax)
	je	.LBB7_12
# BB#1:
	movl	176(%rax), %ecx
	movl	180(%rax), %edx
	testl	%ecx, %ecx
	je	.LBB7_2
# BB#5:
	movq	smpUMHEX_flag_intra(%rip), %rax
	sarl	$4, %ecx
	testl	%edx, %edx
	je	.LBB7_6
# BB#7:
	movslq	%ecx, %rdx
	movb	$1, %cl
	cmpb	$0, (%rax,%rdx)
	jne	.LBB7_10
# BB#8:
	cmpb	$0, -1(%rax,%rdx)
	jne	.LBB7_10
# BB#9:
	cmpb	$0, 1(%rax,%rdx)
	setne	%cl
.LBB7_10:
	movzbl	%cl, %eax
	jmp	.LBB7_11
.LBB7_2:
	testl	%edx, %edx
	je	.LBB7_3
# BB#4:
	movq	smpUMHEX_flag_intra(%rip), %rax
	movzbl	(%rax), %eax
	jmp	.LBB7_11
.LBB7_6:
	movslq	%ecx, %rcx
	movzbl	-1(%rax,%rcx), %eax
	jmp	.LBB7_11
.LBB7_3:
	xorl	%eax, %eax
.LBB7_11:
	movl	%eax, smpUMHEX_flag_intra_SAD(%rip)
.LBB7_12:
	retq
.Lfunc_end7:
	.size	smpUMHEX_decide_intrabk_SAD, .Lfunc_end7-smpUMHEX_decide_intrabk_SAD
	.cfi_endproc

	.globl	smpUMHEX_skip_intrabk_SAD
	.p2align	4, 0x90
	.type	smpUMHEX_skip_intrabk_SAD,@function
smpUMHEX_skip_intrabk_SAD:              # @smpUMHEX_skip_intrabk_SAD
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	addl	$-9, %edi
	cmpl	$0, (%rax)
	jle	.LBB8_2
# BB#1:
	cmpl	$2, %edi
	setb	%cl
	movq	smpUMHEX_flag_intra(%rip), %rdx
	movl	176(%rax), %eax
	sarl	$4, %eax
	cltq
	movb	%cl, (%rdx,%rax)
	movq	img(%rip), %rax
.LBB8_2:                                # %._crit_edge
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	subq	$352, %rsp              # imm = 0x160
.Lcfi112:
	.cfi_def_cfa_offset 408
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	cmpl	$1, %edi
	ja	.LBB8_6
# BB#3:                                 # %._crit_edge
	cmpl	$2, 20(%rax)
	je	.LBB8_6
# BB#4:                                 # %.preheader27
	movq	smpUMHEX_l0_cost(%rip), %rsi
	movq	smpUMHEX_l1_cost(%rip), %rdi
	movq	(%rsi), %rcx
	movq	8(%rsi), %rbx
	movq	(%rdi), %rdx
	movq	8(%rdi), %rax
	movq	16(%rsi), %r12
	movq	16(%rdi), %r15
	movq	24(%rsi), %r14
	movq	24(%rdi), %r11
	movq	32(%rsi), %r9
	movq	32(%rdi), %r8
	movq	40(%rsi), %rbp
	movq	%rbp, -112(%rsp)        # 8-byte Spill
	movq	48(%rsi), %rbp
	movq	%rbp, -120(%rsp)        # 8-byte Spill
	movq	56(%rsi), %rbp
	movq	64(%rsi), %r13
	movq	40(%rdi), %r10
	movq	48(%rdi), %rsi
	movq	%rsi, -128(%rsp)        # 8-byte Spill
	movq	56(%rdi), %rsi
	movq	%rsi, -104(%rsp)        # 8-byte Spill
	movq	64(%rdi), %rdi
	movq	(%rcx), %rsi
	movq	%rsi, 344(%rsp)         # 8-byte Spill
	movq	8(%rcx), %rsi
	movq	%rsi, 336(%rsp)         # 8-byte Spill
	movq	16(%rcx), %rsi
	movq	%rsi, 328(%rsp)         # 8-byte Spill
	movq	24(%rcx), %rcx
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	movq	(%rdx), %rcx
	movq	%rcx, 312(%rsp)         # 8-byte Spill
	movq	8(%rdx), %rcx
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	movq	16(%rdx), %rcx
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movq	24(%rdx), %rcx
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movq	(%rbx), %rcx
	movq	%rcx, 280(%rsp)         # 8-byte Spill
	movq	8(%rbx), %rcx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movq	16(%rbx), %rcx
	movq	%rcx, 264(%rsp)         # 8-byte Spill
	movq	24(%rbx), %rcx
	movq	%rcx, 256(%rsp)         # 8-byte Spill
	movq	(%rax), %rcx
	movq	%rcx, 248(%rsp)         # 8-byte Spill
	movq	8(%rax), %rcx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movq	16(%rax), %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movq	24(%rax), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	(%r12), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movq	8(%r12), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	16(%r12), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	24(%r12), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	(%r15), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	8(%r15), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	16(%r15), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	24(%r15), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	(%r14), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	8(%r14), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	16(%r14), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	24(%r14), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	(%r11), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	8(%r11), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	16(%r11), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	24(%r11), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	(%r9), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	8(%r9), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	16(%r9), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	24(%r9), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	(%r8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	8(%r8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	16(%r8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	24(%r8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	-112(%rsp), %rcx        # 8-byte Reload
	movq	(%rcx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rcx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rcx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rcx), %rax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	movq	(%r10), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	8(%r10), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	16(%r10), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	24(%r10), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	(%rax), %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	8(%rax), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	16(%rax), %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movq	24(%rax), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	-128(%rsp), %rax        # 8-byte Reload
	movq	(%rax), %rcx
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	movq	8(%rax), %rcx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	16(%rax), %rcx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	movq	24(%rax), %rax
	movq	%rax, -128(%rsp)        # 8-byte Spill
	movq	(%rbp), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	movq	8(%rbp), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movq	16(%rbp), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	24(%rbp), %rbp
	movq	-104(%rsp), %rax        # 8-byte Reload
	movq	(%rax), %r8
	movq	8(%rax), %r9
	movq	16(%rax), %r10
	movq	24(%rax), %r11
	movq	(%r13), %r14
	movq	8(%r13), %r15
	movq	16(%r13), %r12
	movq	24(%r13), %r13
	movq	%rdi, %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rbx
	movq	16(%rax), %rdi
	movq	24(%rax), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	344(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	312(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	280(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	248(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	216(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	184(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	152(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	120(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	88(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-56(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-80(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movl	$0, (%r8,%rcx)
	movl	$0, (%r14,%rcx)
	movl	$0, (%rsi,%rcx)
	movq	336(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	304(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	272(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	240(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	208(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	176(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	144(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	112(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	48(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-40(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-88(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movl	$0, (%r9,%rcx)
	movl	$0, (%r15,%rcx)
	movl	$0, (%rbx,%rcx)
	movq	328(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	296(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	264(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	232(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	200(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	168(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	136(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	104(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	72(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-16(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-48(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-72(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-96(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movl	$0, (%r10,%rcx)
	movl	$0, (%r12,%rcx)
	movl	$0, (%rdi,%rcx)
	movq	320(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	288(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	256(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	224(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	192(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	160(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	128(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	96(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-112(%rsp), %rdx        # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-120(%rsp), %rdx        # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movl	$0, (%rdx,%rcx)
	movl	$0, (%rbp,%rcx)
	movl	$0, (%r11,%rcx)
	movl	$0, (%r13,%rcx)
	movl	$0, (%rax,%rcx)
	addq	$4, %rcx
	cmpq	$16, %rcx
	jne	.LBB8_5
.LBB8_6:                                # %.loopexit
	addq	$352, %rsp              # imm = 0x160
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	smpUMHEX_skip_intrabk_SAD, .Lfunc_end8-smpUMHEX_skip_intrabk_SAD
	.cfi_endproc

	.globl	smpUMHEX_setup
	.p2align	4, 0x90
	.type	smpUMHEX_setup,@function
smpUMHEX_setup:                         # @smpUMHEX_setup
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 16
.Lcfi120:
	.cfi_offset %rbx, -16
	cmpl	$7, %r8d
	jl	.LBB9_2
# BB#1:
	movslq	%edx, %rax
	movq	(%r9,%rax,8), %r9
	movslq	%ecx, %rax
	movq	(%r9,%rax,8), %r9
	movslq	%esi, %r10
	leaq	(%r9,%r10,8), %r11
	movl	$5, %ebx
	jmp	.LBB9_8
.LBB9_2:
	cmpl	$5, %r8d
	jl	.LBB9_4
# BB#3:
	movslq	%edx, %rax
	movq	(%r9,%rax,8), %r9
	movslq	%ecx, %rax
	movq	(%r9,%rax,8), %r9
	movslq	%esi, %r10
	leaq	(%r9,%r10,8), %r11
	movl	$4, %ebx
	jmp	.LBB9_8
.LBB9_4:
	cmpl	$4, %r8d
	jne	.LBB9_6
# BB#5:
	movslq	%edx, %rax
	movq	(%r9,%rax,8), %r9
	movslq	%ecx, %rax
	movq	(%r9,%rax,8), %r9
	movslq	%esi, %r10
	leaq	(%r9,%r10,8), %r11
	movl	$2, %ebx
	jmp	.LBB9_8
.LBB9_6:
	cmpl	$2, %r8d
	jl	.LBB9_16
# BB#7:
	movslq	%edx, %rax
	movq	(%r9,%rax,8), %r9
	movslq	%ecx, %rax
	movq	(%r9,%rax,8), %r9
	movslq	%esi, %r10
	leaq	(%r9,%r10,8), %r11
	movl	$1, %ebx
.LBB9_8:
	movq	(%r11), %rax
	movswq	%di, %rdi
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%rbx,8), %rax
	movzwl	(%rax), %eax
	movw	%ax, smpUMHEX_pred_MV_uplayer_X(%rip)
	movq	(%r9,%r10,8), %rax
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%rbx,8), %rax
	movzwl	2(%rax), %eax
	movw	%ax, smpUMHEX_pred_MV_uplayer_Y(%rip)
	cmpl	$7, %r8d
	jl	.LBB9_10
# BB#9:
	cmpl	$1, %esi
	movl	$smpUMHEX_l1_cost, %esi
	movl	$smpUMHEX_l0_cost, %eax
	cmoveq	%rsi, %rax
	movl	$5, %edi
	jmp	.LBB9_13
.LBB9_10:
	cmpl	$5, %r8d
	jl	.LBB9_12
# BB#11:
	cmpl	$1, %esi
	movl	$smpUMHEX_l1_cost, %esi
	movl	$smpUMHEX_l0_cost, %eax
	cmoveq	%rsi, %rax
	movl	$4, %edi
	jmp	.LBB9_13
.LBB9_12:
	xorl	%edi, %edi
	cmpl	$4, %r8d
	sete	%dil
	cmpl	$1, %esi
	movl	$smpUMHEX_l1_cost, %esi
	movl	$smpUMHEX_l0_cost, %eax
	cmoveq	%rsi, %rax
	incq	%rdi
.LBB9_13:
	xorl	%esi, %esi
	cmpl	$0, smpUMHEX_flag_intra_SAD(%rip)
	jne	.LBB9_15
# BB#14:                                # %select.false.sink
	movq	(%rax), %rax
	movq	(%rax,%rdi,8), %rax
	movq	img(%rip), %rsi
	movl	176(%rsi), %edi
	movl	180(%rsi), %esi
	sarl	$2, %esi
	addl	%edx, %esi
	movslq	%esi, %rdx
	sarl	$2, %edi
	movq	(%rax,%rdx,8), %rax
	addl	%ecx, %edi
	movslq	%edi, %rcx
	movl	(%rax,%rcx,4), %eax
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
.LBB9_15:                               # %select.end
	movl	%esi, smpUMHEX_pred_SAD_uplayer(%rip)
.LBB9_16:
	popq	%rbx
	retq
.Lfunc_end9:
	.size	smpUMHEX_setup, .Lfunc_end9-smpUMHEX_setup
	.cfi_endproc

	.type	block_type_shift_factor,@object # @block_type_shift_factor
	.section	.rodata,"a",@progbits
	.globl	block_type_shift_factor
	.p2align	4
block_type_shift_factor:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	1                       # 0x1
	.size	block_type_shift_factor, 16

	.type	SymmetricalCrossSearchThreshold1,@object # @SymmetricalCrossSearchThreshold1
	.comm	SymmetricalCrossSearchThreshold1,2,2
	.type	SymmetricalCrossSearchThreshold2,@object # @SymmetricalCrossSearchThreshold2
	.comm	SymmetricalCrossSearchThreshold2,2,2
	.type	ConvergeThreshold,@object # @ConvergeThreshold
	.comm	ConvergeThreshold,2,2
	.type	SubPelThreshold1,@object # @SubPelThreshold1
	.comm	SubPelThreshold1,2,2
	.type	SubPelThreshold3,@object # @SubPelThreshold3
	.comm	SubPelThreshold3,2,2
	.type	smpUMHEX_flag_intra,@object # @smpUMHEX_flag_intra
	.comm	smpUMHEX_flag_intra,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"smpUMHEX_get_mem: smpUMHEX_flag_intra"
	.size	.L.str, 38

	.type	smpUMHEX_l0_cost,@object # @smpUMHEX_l0_cost
	.comm	smpUMHEX_l0_cost,8,8
	.type	smpUMHEX_l1_cost,@object # @smpUMHEX_l1_cost
	.comm	smpUMHEX_l1_cost,8,8
	.type	smpUMHEX_SearchState,@object # @smpUMHEX_SearchState
	.comm	smpUMHEX_SearchState,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	dist_method,@object     # @dist_method
	.local	dist_method
	.comm	dist_method,4,4
	.type	ref_pic_ptr,@object     # @ref_pic_ptr
	.local	ref_pic_ptr
	.comm	ref_pic_ptr,8,8
	.type	width_pad,@object       # @width_pad
	.comm	width_pad,4,4
	.type	height_pad,@object      # @height_pad
	.comm	height_pad,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	width_pad_cr,@object    # @width_pad_cr
	.comm	width_pad_cr,4,4
	.type	height_pad_cr,@object   # @height_pad_cr
	.comm	height_pad_cr,4,4
	.type	Diamond_X,@object       # @Diamond_X
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	1
Diamond_X:
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	0                       # 0x0
	.size	Diamond_X, 8

	.type	Diamond_Y,@object       # @Diamond_Y
	.p2align	1
Diamond_Y:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.size	Diamond_Y, 8

	.type	Hexagon_X,@object       # @Hexagon_X
	.section	.rodata,"a",@progbits
	.p2align	1
Hexagon_X:
	.short	65534                   # 0xfffe
	.short	2                       # 0x2
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.size	Hexagon_X, 12

	.type	Hexagon_Y,@object       # @Hexagon_Y
	.p2align	1
Hexagon_Y:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	65534                   # 0xfffe
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	65534                   # 0xfffe
	.size	Hexagon_Y, 12

	.type	Big_Hexagon_X,@object   # @Big_Hexagon_X
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
Big_Hexagon_X:
	.short	65532                   # 0xfffc
	.short	4                       # 0x4
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	65532                   # 0xfffc
	.short	4                       # 0x4
	.short	65532                   # 0xfffc
	.short	4                       # 0x4
	.short	65532                   # 0xfffc
	.short	4                       # 0x4
	.short	65532                   # 0xfffc
	.short	4                       # 0x4
	.short	65534                   # 0xfffe
	.short	2                       # 0x2
	.short	65534                   # 0xfffe
	.short	2                       # 0x2
	.size	Big_Hexagon_X, 32

	.type	Big_Hexagon_Y,@object   # @Big_Hexagon_Y
	.p2align	4
Big_Hexagon_Y:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	65532                   # 0xfffc
	.short	4                       # 0x4
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	65535                   # 0xffff
	.short	65534                   # 0xfffe
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	65534                   # 0xfffe
	.short	65533                   # 0xfffd
	.short	3                       # 0x3
	.short	3                       # 0x3
	.short	65533                   # 0xfffd
	.size	Big_Hexagon_Y, 32

	.type	smpUMHEX_pred_MV_uplayer_X,@object # @smpUMHEX_pred_MV_uplayer_X
	.comm	smpUMHEX_pred_MV_uplayer_X,2,2
	.type	smpUMHEX_pred_MV_uplayer_Y,@object # @smpUMHEX_pred_MV_uplayer_Y
	.comm	smpUMHEX_pred_MV_uplayer_Y,2,2
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	smpUMHEX_flag_intra_SAD,@object # @smpUMHEX_flag_intra_SAD
	.comm	smpUMHEX_flag_intra_SAD,4,4
	.type	smpUMHEX_pred_SAD_uplayer,@object # @smpUMHEX_pred_SAD_uplayer
	.comm	smpUMHEX_pred_SAD_uplayer,4,4
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
