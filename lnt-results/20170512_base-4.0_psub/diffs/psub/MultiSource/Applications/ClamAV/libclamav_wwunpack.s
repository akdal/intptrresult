	.text
	.file	"libclamav_wwunpack.bc"
	.globl	wwunpack
	.p2align	4, 0x90
	.type	wwunpack,@function
wwunpack:                               # @wwunpack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, 44(%rsp)          # 4-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movl	%ecx, %r13d
	movl	%edx, %r14d
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movq	176(%rsp), %rbp
	leaq	673(%rbp), %r15
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	184(%rsp), %esi
	movq	%rbx, %rcx
	movq	%rsi, %rdx
	addq	%rbp, %rdx
	movl	%r14d, 40(%rsp)         # 4-byte Spill
	movl	%r14d, %eax
	movl	%r13d, %r14d
	addq	%rcx, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%r14d, %eax
	negq	%rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	movl	%r12d, %eax
	movq	%r15, %r12
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_16 Depth 2
                                        #       Child Loop BB0_252 Depth 3
                                        #       Child Loop BB0_257 Depth 3
                                        #       Child Loop BB0_262 Depth 3
                                        #       Child Loop BB0_265 Depth 3
	cmpl	$17, %esi
	jb	.LBB0_287
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	leaq	17(%r12), %rsi
	cmpq	%rdx, %rsi
	ja	.LBB0_287
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpq	%rbp, %rsi
	jbe	.LBB0_287
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	8(%r12), %ebx
	movl	12(%r12), %eax
	leal	(,%rbx,4), %r13d
	addl	$4, %eax
	cmpl	%eax, %r13d
	jne	.LBB0_289
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	(%r12), %ebp
	movl	%r13d, %edi
	movl	$1, %esi
	movq	%rdi, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	callq	cli_calloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_290
# BB#6:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	%r14d, 24(%rsp)         # 4-byte Spill
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	je	.LBB0_288
# BB#7:                                 #   in Loop: Header=BB0_1 Depth=1
	testl	%ebx, %ebx
	je	.LBB0_288
# BB#8:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpl	28(%rsp), %r13d         # 4-byte Folded Reload
	ja	.LBB0_288
# BB#9:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	48(%rsp), %r14          # 8-byte Reload
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	subl	%ebp, %r14d
	addq	112(%rsp), %r14         # 8-byte Folded Reload
	addq	104(%rsp), %r14         # 8-byte Folded Reload
	movq	%r14, %rbx
	subq	%rcx, %rbx
	jb	.LBB0_288
# BB#10:                                #   in Loop: Header=BB0_1 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%r14,%rax), %rax
	cmpq	96(%rsp), %rax          # 8-byte Folded Reload
	ja	.LBB0_288
# BB#11:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	%rcx, %rax
	jbe	.LBB0_288
# BB#12:                                #   in Loop: Header=BB0_1 Depth=1
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	memcpy
	cmpl	$4, %r13d
	jb	.LBB0_282
# BB#13:                                #   in Loop: Header=BB0_1 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	subq	%rbx, %rax
	leaq	4(%r15), %r9
	movl	(%r15), %esi
	addq	%r15, (%rsp)            # 8-byte Folded Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	addq	%r14, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movb	$32, %r13b
	movq	%r14, %r10
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_16 Depth=2
	movb	(%r9), %bl
	movb	%bl, (%r10)
	movq	%rax, %r9
	movl	%r12d, %esi
	movl	%edx, %r13d
	movq	%rcx, %r10
	jmp	.LBB0_16
.LBB0_15:                               # %.backedge.loopexit.i
                                        #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r10,%r11), %r10
	.p2align	4, 0x90
.LBB0_16:                               # %.backedge.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_252 Depth 3
                                        #       Child Loop BB0_257 Depth 3
                                        #       Child Loop BB0_262 Depth 3
                                        #       Child Loop BB0_265 Depth 3
	leal	(%rsi,%rsi), %r12d
	movl	%r13d, %edx
	decb	%dl
	testl	%esi, %esi
	js	.LBB0_25
# BB#17:                                # %.backedge.i
                                        #   in Loop: Header=BB0_16 Depth=2
	testb	%dl, %dl
	je	.LBB0_25
# BB#18:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#19:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r9), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#20:                                #   in Loop: Header=BB0_16 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB0_282
# BB#21:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rax
	jbe	.LBB0_282
# BB#22:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %r10
	jb	.LBB0_282
# BB#23:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r10), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#24:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %rcx
	ja	.LBB0_14
	jmp	.LBB0_282
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_16 Depth=2
	testb	%dl, %dl
	je	.LBB0_28
# BB#26:                                #   in Loop: Header=BB0_16 Depth=2
	shrl	$29, %esi
	andl	$3, %esi
	cmpb	$2, %dl
	jbe	.LBB0_40
# BB#27:                                #   in Loop: Header=BB0_16 Depth=2
	movb	$2, %al
	movl	$2, %ebx
	movq	%r9, %rbp
	jmp	.LBB0_51
.LBB0_28:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#29:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rbp
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#30:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rbp
	jbe	.LBB0_282
# BB#31:                                #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %r12d
	testl	%esi, %esi
	js	.LBB0_49
# BB#32:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rbp
	jb	.LBB0_282
# BB#33:                                #   in Loop: Header=BB0_16 Depth=2
	addq	$5, %r9
	cmpq	(%rsp), %r9             # 8-byte Folded Reload
	ja	.LBB0_282
# BB#34:                                #   in Loop: Header=BB0_16 Depth=2
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB0_282
# BB#35:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jbe	.LBB0_282
# BB#36:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %r10
	jb	.LBB0_282
# BB#37:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r10), %rax
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#38:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %rax
	jbe	.LBB0_282
# BB#39:                                #   in Loop: Header=BB0_16 Depth=2
	movb	(%rbp), %cl
	movb	%cl, (%r10)
	movl	%r12d, %esi
	movb	$32, %r13b
	movq	%rax, %r10
	jmp	.LBB0_16
.LBB0_40:                               #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_45
# BB#41:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#42:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rbp
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#43:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rbp
	jbe	.LBB0_282
# BB#44:                                # %getbitmap.exit41.i316.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %r12d
	movb	$32, %dl
	jmp	.LBB0_52
.LBB0_45:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %al
	subb	%r13b, %al
	movl	%eax, %ecx
	shrl	%cl, %esi
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#46:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rbp
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#47:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rbp
	jbe	.LBB0_282
# BB#48:                                #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %ebx
	movl	(%r9), %r12d
	movl	%eax, %ecx
	shldl	%cl, %r12d, %esi
	jmp	.LBB0_50
.LBB0_49:                               # %.thread.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%r12d, %esi
	shrl	$30, %esi
	movb	$2, %al
	movl	$2, %ebx
.LBB0_50:                               # %.sink.split.i319.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movb	$32, %dl
.LBB0_51:                               # %.sink.split.i319.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %ecx
	shll	%cl, %r12d
	subb	%al, %dl
.LBB0_52:                               # %getbits.exit321.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movzbl	%sil, %r8d
	cmpl	$3, %r8d
	jb	.LBB0_55
# BB#53:                                #   in Loop: Header=BB0_16 Depth=2
	movl	%r12d, %edi
	shrl	$30, %edi
	cmpb	$2, %dl
	jbe	.LBB0_57
# BB#54:                                #   in Loop: Header=BB0_16 Depth=2
	movb	$2, %al
	movl	$2, %esi
	jmp	.LBB0_71
.LBB0_55:                               #   in Loop: Header=BB0_16 Depth=2
	movl	%r12d, %eax
	shrl	$29, %eax
	cmpb	$3, %dl
	jbe	.LBB0_62
# BB#56:                                #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %bl
	movl	$3, %esi
	jmp	.LBB0_87
.LBB0_57:                               #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_67
# BB#58:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rbp
	jb	.LBB0_282
# BB#59:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rbp), %r8
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	ja	.LBB0_282
# BB#60:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r8
	jbe	.LBB0_282
# BB#61:                                # %getbitmap.exit41.i308.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%rbp), %r12d
	movb	$32, %dl
	jmp	.LBB0_72
.LBB0_62:                               #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_83
# BB#63:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rbp
	jb	.LBB0_282
# BB#64:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rbp), %r9
	cmpq	(%rsp), %r9             # 8-byte Folded Reload
	ja	.LBB0_282
# BB#65:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jbe	.LBB0_282
# BB#66:                                # %getbitmap.exit41.i292.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%rbp), %r12d
	movb	$32, %dl
	jmp	.LBB0_88
.LBB0_67:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$2, %al
	subb	%dl, %al
	movl	%eax, %ecx
	shrl	%cl, %edi
	cmpq	%r15, %rbp
	jb	.LBB0_282
# BB#68:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rbp), %rbx
	cmpq	(%rsp), %rbx            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#69:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rbx
	jbe	.LBB0_282
# BB#70:                                #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	(%rbp), %r12d
	movl	%eax, %ecx
	shldl	%cl, %r12d, %edi
	movb	$32, %dl
	movq	%rbx, %rbp
.LBB0_71:                               # %.sink.split.i311.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r12d
	subb	%al, %dl
	movq	%rbp, %r8
.LBB0_72:                               # %getbits.exit313.i
                                        #   in Loop: Header=BB0_16 Depth=2
	leal	5(%rdi), %eax
	addl	$65535, %edi            # imm = 0xFFFF
	testw	%di, %di
	jle	.LBB0_74
# BB#73:                                #   in Loop: Header=BB0_16 Depth=2
	incb	%al
	movl	$1, %ebp
	movl	%eax, %ecx
	shll	%cl, %ebp
	addl	$65377, %ebp            # imm = 0xFF61
	jmp	.LBB0_75
.LBB0_74:                               #   in Loop: Header=BB0_16 Depth=2
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	movl	%esi, %ecx
	andl	$65280, %ecx            # imm = 0xFF00
	addl	$225, %esi
	movzbl	%sil, %ebp
	orl	%ecx, %ebp
.LBB0_75:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	$32, %ecx
	subl	%esi, %ecx
	movl	%r12d, %r11d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r11d
	movl	%esi, %ebx
	subb	%dl, %bl
	jae	.LBB0_77
# BB#76:                                #   in Loop: Header=BB0_16 Depth=2
	movq	%r8, %r9
	movl	%eax, %ebx
	jmp	.LBB0_82
.LBB0_77:                               #   in Loop: Header=BB0_16 Depth=2
	jbe	.LBB0_95
# BB#78:                                #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %ecx
	shrl	%cl, %r11d
	cmpq	%r15, %r8
	jb	.LBB0_282
# BB#79:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %r9
	cmpq	(%rsp), %r9             # 8-byte Folded Reload
	ja	.LBB0_282
# BB#80:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jbe	.LBB0_282
# BB#81:                                #   in Loop: Header=BB0_16 Depth=2
	movzbl	%bl, %esi
	movl	(%r8), %r12d
	movl	%ebx, %ecx
	shldl	%cl, %r12d, %r11d
	movb	$32, %dl
.LBB0_82:                               # %.sink.split.i303.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r12d
	subb	%bl, %dl
	movl	%r12d, %esi
	movl	%edx, %r13d
	jmp	.LBB0_99
.LBB0_83:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %bl
	subb	%dl, %bl
	movl	%ebx, %ecx
	shrl	%cl, %eax
	cmpq	%r15, %rbp
	jb	.LBB0_282
# BB#84:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rbp), %rdi
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#85:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdi
	jbe	.LBB0_282
# BB#86:                                #   in Loop: Header=BB0_16 Depth=2
	movzbl	%bl, %esi
	movl	(%rbp), %r12d
	movl	%ebx, %ecx
	shldl	%cl, %r12d, %eax
	movb	$32, %dl
	movq	%rdi, %rbp
.LBB0_87:                               # %.sink.split.i295.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r12d
	subb	%bl, %dl
	movq	%rbp, %r9
.LBB0_88:                               # %getbits.exit297.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	cmpl	$3, %esi
	ja	.LBB0_92
# BB#89:                                #   in Loop: Header=BB0_16 Depth=2
	xorl	%ecx, %ecx
	cmpl	$3, %esi
	jne	.LBB0_116
# BB#90:                                #   in Loop: Header=BB0_16 Depth=2
	movl	%r12d, %ecx
	shrl	$31, %ecx
	decb	%dl
	je	.LBB0_112
# BB#91:                                #   in Loop: Header=BB0_16 Depth=2
	addl	%r12d, %r12d
	leal	5(%rax,%rcx), %eax
	jmp	.LBB0_122
.LBB0_92:                               #   in Loop: Header=BB0_16 Depth=2
	cmpl	$4, %esi
	jne	.LBB0_108
# BB#93:                                #   in Loop: Header=BB0_16 Depth=2
	decb	%dl
	je	.LBB0_117
# BB#94:                                #   in Loop: Header=BB0_16 Depth=2
	leal	(%r12,%r12), %ecx
	jmp	.LBB0_121
.LBB0_95:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r8
	jb	.LBB0_282
# BB#96:                                #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %r9
	cmpq	(%rsp), %r9             # 8-byte Folded Reload
	ja	.LBB0_282
# BB#97:                                #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jbe	.LBB0_282
# BB#98:                                # %getbitmap.exit41.i300.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %esi
	movb	$32, %r13b
.LBB0_99:                               # %getbits.exit305.i
                                        #   in Loop: Header=BB0_16 Depth=2
	cmpl	$2, 16(%rsp)            # 4-byte Folded Reload
	movzwl	%r11w, %ebx
	jb	.LBB0_276
# BB#100:                               # %getbits.exit305.i
                                        #   in Loop: Header=BB0_16 Depth=2
	cmpl	$511, %ebx              # imm = 0x1FF
	je	.LBB0_276
# BB#101:                               #   in Loop: Header=BB0_16 Depth=2
	addl	%ebp, %r11d
	movzwl	%r11w, %eax
	movq	%r10, %rcx
	subq	%rax, %rcx
	cmpq	%r14, %rcx
	jb	.LBB0_282
# BB#102:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %r10
	jb	.LBB0_282
# BB#103:                               #   in Loop: Header=BB0_16 Depth=2
	addq	$2, %rcx
	cmpq	%r14, %rcx
	jbe	.LBB0_282
# BB#104:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#105:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	2(%r10), %rcx
	cmpq	8(%rsp), %rcx           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#106:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %rcx
	jbe	.LBB0_282
# BB#107:                               #   in Loop: Header=BB0_16 Depth=2
	negq	%rax
	movb	(%r10,%rax), %dl
	movb	%dl, (%r10)
	movb	1(%r10,%rax), %al
	movb	%al, 1(%r10)
	movq	%rcx, %r10
	jmp	.LBB0_16
.LBB0_108:                              #   in Loop: Header=BB0_16 Depth=2
	addl	$7, %eax
	movzbl	%al, %ecx
	cmpl	$13, %ecx
	jb	.LBB0_122
# BB#109:                               #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_158
# BB#110:                               #   in Loop: Header=BB0_16 Depth=2
	movl	%r12d, %r11d
	shrl	$18, %r11d
	cmpb	$14, %dl
	jbe	.LBB0_168
# BB#111:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$14, %al
	movl	$14, %esi
	jmp	.LBB0_187
.LBB0_112:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#113:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rsi
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#114:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rsi
	jbe	.LBB0_282
# BB#115:                               # %getbitmap.exit289.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %r12d
	movb	$32, %dl
	movq	%rsi, %r9
.LBB0_116:                              # %.critedge.i
                                        #   in Loop: Header=BB0_16 Depth=2
	leal	5(%rax,%rcx), %eax
	jmp	.LBB0_122
.LBB0_117:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#118:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rsi
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#119:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rsi
	jbe	.LBB0_282
# BB#120:                               # %getbitmap.exit286.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %ecx
	movb	$32, %dl
	movq	%rsi, %r9
.LBB0_121:                              # %.critedge215.i
                                        #   in Loop: Header=BB0_16 Depth=2
	shrl	$31, %r12d
	leal	6(%r12,%rax), %eax
	movl	%ecx, %r12d
.LBB0_122:                              #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	$32, %ecx
	subl	%esi, %ecx
	movl	%r12d, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebp
	movl	%esi, %ebx
	subb	%dl, %bl
	jae	.LBB0_124
# BB#123:                               #   in Loop: Header=BB0_16 Depth=2
	movq	%r9, %rdi
	movl	%eax, %ebx
	jmp	.LBB0_129
.LBB0_124:                              #   in Loop: Header=BB0_16 Depth=2
	jbe	.LBB0_130
# BB#125:                               #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %ecx
	shrl	%cl, %ebp
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#126:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rdi
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#127:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdi
	jbe	.LBB0_282
# BB#128:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%bl, %esi
	movl	(%r9), %r12d
	movl	%ebx, %ecx
	shldl	%cl, %r12d, %ebp
	movb	$32, %dl
.LBB0_129:                              # %.sink.split.i265.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r12d
	subb	%bl, %dl
	jmp	.LBB0_134
.LBB0_130:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#131:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rdi
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#132:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdi
	jbe	.LBB0_282
# BB#133:                               # %getbitmap.exit41.i262.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %r12d
	movb	$32, %dl
.LBB0_134:                              # %.critedge218.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	$1, %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	addl	$65505, %esi            # imm = 0xFFE1
	movzwl	%si, %r11d
	addl	%ebp, %r11d
	movq	%rdi, %r9
.LBB0_135:                              #   in Loop: Header=BB0_16 Depth=2
	movl	%r12d, %esi
	movl	%edx, %r13d
.LBB0_136:                              #   in Loop: Header=BB0_16 Depth=2
	xorl	%edi, %edi
	cmpl	$1, %r8d
	setne	%al
	testl	%r8d, %r8d
	je	.LBB0_138
# BB#137:                               #   in Loop: Header=BB0_16 Depth=2
	movb	%al, %dil
	addl	$3, %edi
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_237
	jmp	.LBB0_282
.LBB0_138:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r13b
	je	.LBB0_140
# BB#139:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%rsi,%rsi), %edi
	testl	%esi, %esi
	jns	.LBB0_144
	jmp	.LBB0_146
.LBB0_140:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#141:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#142:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rax
	jbe	.LBB0_282
# BB#143:                               # %getbitmap.exit259.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %edi
	movb	$32, %r13b
	movq	%rax, %r9
	testl	%esi, %esi
	js	.LBB0_146
.LBB0_144:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r13b
	je	.LBB0_148
# BB#145:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%rdi,%rdi), %esi
	jmp	.LBB0_152
.LBB0_146:                              #   in Loop: Header=BB0_16 Depth=2
	movl	%edi, %ebx
	shrl	$29, %ebx
	cmpb	$3, %r13b
	jbe	.LBB0_153
# BB#147:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %al
	movl	$3, %edx
	jmp	.LBB0_164
.LBB0_148:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#149:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#150:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rax
	jbe	.LBB0_282
# BB#151:                               # %getbitmap.exit256.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %esi
	movb	$32, %r13b
	movq	%rax, %r9
.LBB0_152:                              #   in Loop: Header=BB0_16 Depth=2
	shrl	$31, %edi
	addl	$5, %edi
	movq	%r9, %rdx
	jmp	.LBB0_236
.LBB0_153:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_160
# BB#154:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#155:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rdx
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#156:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdx
	jbe	.LBB0_282
# BB#157:                               # %getbitmap.exit41.i248.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %esi
	movb	$32, %r13b
	testb	%bl, %bl
	jne	.LBB0_165
	jmp	.LBB0_166
.LBB0_158:                              #   in Loop: Header=BB0_16 Depth=2
	movl	%r12d, %r11d
	shrl	$17, %r11d
	cmpb	$15, %dl
	jbe	.LBB0_173
# BB#159:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$15, %al
	movl	$15, %esi
	jmp	.LBB0_192
.LBB0_160:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$3, %al
	subb	%r13b, %al
	movl	%eax, %ecx
	shrl	%cl, %ebx
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#161:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rsi
	cmpq	(%rsp), %rsi            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#162:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rsi
	jbe	.LBB0_282
# BB#163:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %edx
	movl	(%r9), %edi
	movl	%eax, %ecx
	shldl	%cl, %edi, %ebx
	movb	$32, %r13b
	movq	%rsi, %r9
.LBB0_164:                              # %.sink.split.i251.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%edx, %ecx
	shll	%cl, %edi
	subb	%al, %r13b
	movq	%r9, %rdx
	movl	%edi, %esi
	testb	%bl, %bl
	je	.LBB0_166
.LBB0_165:                              #   in Loop: Header=BB0_16 Depth=2
	addl	$6, %ebx
	movl	%ebx, %edi
	jmp	.LBB0_236
.LBB0_166:                              #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %edi
	shrl	$28, %edi
	cmpb	$4, %r13b
	jbe	.LBB0_178
# BB#167:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$4, %al
	movl	$4, %ebx
	jmp	.LBB0_197
.LBB0_168:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_183
# BB#169:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#170:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#171:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rax
	jbe	.LBB0_282
# BB#172:                               # %getbitmap.exit41.i278.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %esi
	movb	$32, %r13b
	addl	$8161, %r11d            # imm = 0x1FE1
	movq	%rax, %r9
	jmp	.LBB0_136
.LBB0_173:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_188
# BB#174:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#175:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#176:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rax
	jbe	.LBB0_282
# BB#177:                               # %getbitmap.exit41.i270.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r9), %esi
	movb	$32, %r13b
	addl	$24545, %r11d           # imm = 0x5FE1
	movq	%rax, %r9
	jmp	.LBB0_136
.LBB0_178:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_193
# BB#179:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdx
	jb	.LBB0_282
# BB#180:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rdx), %r8
	cmpq	(%rsp), %r8             # 8-byte Folded Reload
	ja	.LBB0_282
# BB#181:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r8
	jbe	.LBB0_282
# BB#182:                               # %getbitmap.exit41.i240.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%rdx), %esi
	movb	$32, %r13b
	testb	%dil, %dil
	jne	.LBB0_198
	jmp	.LBB0_199
.LBB0_183:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$14, %al
	subb	%dl, %al
	movl	%eax, %ecx
	shrl	%cl, %r11d
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#184:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rdi
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#185:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdi
	jbe	.LBB0_282
# BB#186:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	(%r9), %r12d
	movl	%eax, %ecx
	shldl	%cl, %r12d, %r11d
	movb	$32, %dl
	movq	%rdi, %r9
.LBB0_187:                              # %.sink.split.i281.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r12d
	subb	%al, %dl
	addl	$8161, %r11d            # imm = 0x1FE1
	jmp	.LBB0_135
.LBB0_188:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$15, %al
	subb	%dl, %al
	movl	%eax, %ecx
	shrl	%cl, %r11d
	cmpq	%r15, %r9
	jb	.LBB0_282
# BB#189:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r9), %rdi
	cmpq	(%rsp), %rdi            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#190:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdi
	jbe	.LBB0_282
# BB#191:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %esi
	movl	(%r9), %r12d
	movl	%eax, %ecx
	shldl	%cl, %r12d, %r11d
	movb	$32, %dl
	movq	%rdi, %r9
.LBB0_192:                              # %.sink.split.i273.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%esi, %ecx
	shll	%cl, %r12d
	subb	%al, %dl
	addl	$24545, %r11d           # imm = 0x5FE1
	jmp	.LBB0_135
.LBB0_193:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$4, %al
	subb	%r13b, %al
	movl	%eax, %ecx
	shrl	%cl, %edi
	cmpq	%r15, %rdx
	jb	.LBB0_282
# BB#194:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%rdx), %rbp
	cmpq	(%rsp), %rbp            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#195:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rbp
	jbe	.LBB0_282
# BB#196:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %ebx
	movl	(%rdx), %esi
	movl	%eax, %ecx
	shldl	%cl, %esi, %edi
	movb	$32, %r13b
	movq	%rbp, %rdx
.LBB0_197:                              # %.sink.split.i243.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %ecx
	shll	%cl, %esi
	subb	%al, %r13b
	movq	%rdx, %r8
	testb	%dil, %dil
	je	.LBB0_199
.LBB0_198:                              #   in Loop: Header=BB0_16 Depth=2
	addl	$13, %edi
	movq	%r8, %rdx
	jmp	.LBB0_236
.LBB0_199:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r13b
	je	.LBB0_201
# BB#200:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%rsi,%rsi), %r9d
	testl	%esi, %esi
	jns	.LBB0_205
	jmp	.LBB0_207
.LBB0_201:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r8
	jb	.LBB0_282
# BB#202:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rax
	cmpq	(%rsp), %rax            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#203:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rax
	jbe	.LBB0_282
# BB#204:                               # %getbitmap.exit237.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %r9d
	movb	$32, %r13b
	movq	%rax, %r8
	testl	%esi, %esi
	js	.LBB0_207
.LBB0_205:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r13b
	je	.LBB0_208
# BB#206:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%r9,%r9), %eax
	testl	%r9d, %r9d
	jns	.LBB0_212
	jmp	.LBB0_214
.LBB0_207:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$5, %bpl
	movl	$29, %esi
	jmp	.LBB0_222
.LBB0_208:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r8
	jb	.LBB0_282
# BB#209:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rcx
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#210:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rcx
	jbe	.LBB0_282
# BB#211:                               # %getbitmap.exit237.i.1
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %eax
	movb	$32, %r13b
	movq	%rcx, %r8
	testl	%r9d, %r9d
	js	.LBB0_214
.LBB0_212:                              #   in Loop: Header=BB0_16 Depth=2
	decb	%r13b
	je	.LBB0_215
# BB#213:                               #   in Loop: Header=BB0_16 Depth=2
	leal	(%rax,%rax), %r9d
	testl	%eax, %eax
	jns	.LBB0_219
	jmp	.LBB0_221
.LBB0_214:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$6, %bpl
	movl	$61, %esi
	movl	%eax, %r9d
	jmp	.LBB0_222
.LBB0_215:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r8
	jb	.LBB0_282
# BB#216:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rcx
	cmpq	(%rsp), %rcx            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#217:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rcx
	jbe	.LBB0_282
# BB#218:                               # %getbitmap.exit237.i.2
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %r9d
	movb	$32, %r13b
	movq	%rcx, %r8
	testl	%eax, %eax
	js	.LBB0_221
.LBB0_219:                              # %.us-lcssa.us.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%r9d, %edi
	shrl	$18, %edi
	cmpb	$14, %r13b
	jbe	.LBB0_266
# BB#220:                               #   in Loop: Header=BB0_16 Depth=2
	movb	$14, %al
	movl	$14, %ebx
	jmp	.LBB0_275
.LBB0_221:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$7, %bpl
	movl	$125, %esi
.LBB0_222:                              # %.us-lcssa640.us.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movzbl	%bpl, %ebx
	movl	$32, %ecx
	subl	%ebx, %ecx
	movl	%r9d, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edi
	movl	%ebx, %eax
	subb	%r13b, %al
	jae	.LBB0_224
# BB#223:                               #   in Loop: Header=BB0_16 Depth=2
	movq	%r8, %rdx
	movl	%ebp, %eax
	jmp	.LBB0_229
.LBB0_224:                              #   in Loop: Header=BB0_16 Depth=2
	jbe	.LBB0_230
# BB#225:                               #   in Loop: Header=BB0_16 Depth=2
	movl	%eax, %ecx
	shrl	%cl, %edi
	cmpq	%r15, %r8
	jb	.LBB0_282
# BB#226:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#227:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdx
	jbe	.LBB0_282
# BB#228:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %ebx
	movl	(%r8), %r9d
	movl	%eax, %ecx
	shldl	%cl, %r9d, %edi
	movb	$32, %r13b
.LBB0_229:                              # %.sink.split.i232.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %ecx
	shll	%cl, %r9d
	subb	%al, %r13b
	jmp	.LBB0_234
.LBB0_230:                              #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r8
	jb	.LBB0_282
# BB#231:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#232:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdx
	jbe	.LBB0_282
# BB#233:                               # %getbitmap.exit41.i229.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %r9d
	movb	$32, %r13b
.LBB0_234:                              # %getbits.exit234.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%edi, %eax
	andl	$-256, %eax
	addl	%edi, %esi
	movzbl	%sil, %edi
	orl	%eax, %edi
.LBB0_235:                              # %.critedge219.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%r9d, %esi
.LBB0_236:                              # %.critedge219.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%rdx, %r9
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB0_282
.LBB0_237:                              #   in Loop: Header=BB0_16 Depth=2
	testw	%di, %di
	je	.LBB0_282
# BB#238:                               #   in Loop: Header=BB0_16 Depth=2
	movzwl	%di, %eax
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB0_282
# BB#239:                               #   in Loop: Header=BB0_16 Depth=2
	movzwl	%r11w, %ecx
	movq	%r10, %rbx
	subq	%rcx, %rbx
	cmpq	%r14, %rbx
	jb	.LBB0_282
# BB#240:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %r10
	jb	.LBB0_282
# BB#241:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	(%rbx,%rax), %rdx
	cmpq	%r14, %rdx
	jbe	.LBB0_282
# BB#242:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	8(%rsp), %rdx           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#243:                               #   in Loop: Header=BB0_16 Depth=2
	addq	%r10, %rax
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	ja	.LBB0_282
# BB#244:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r14, %rax
	jbe	.LBB0_282
# BB#245:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%rcx, %rax
	negq	%rax
	leal	-1(%rdi), %edx
	movzwl	%dx, %r11d
	leaq	1(%r11), %r12
	cmpq	$32, %r12
	jae	.LBB0_247
# BB#246:                               #   in Loop: Header=BB0_16 Depth=2
	movq	%r10, %rbx
	jmp	.LBB0_260
.LBB0_247:                              # %min.iters.checked
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%r12, %r8
	andq	$131040, %r8            # imm = 0x1FFE0
	je	.LBB0_253
# BB#248:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_16 Depth=2
	leal	-1(%rdi), %ebp
	movzwl	%bp, %edx
	movq	%rdx, %rbp
	subq	%rcx, %rbp
	leaq	1(%r10,%rbp), %rcx
	cmpq	%rcx, %r10
	jae	.LBB0_250
# BB#249:                               # %vector.memcheck
                                        #   in Loop: Header=BB0_16 Depth=2
	leaq	1(%r10,%rdx), %rcx
	cmpq	%rcx, %rbx
	movq	%r10, %rbx
	jb	.LBB0_260
.LBB0_250:                              # %vector.body.preheader
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%r11, 56(%rsp)          # 8-byte Spill
	leaq	-32(%r8), %rcx
	movq	%rcx, %rdx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	shrl	$5, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB0_254
# BB#251:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_16 Depth=2
	leaq	16(%r10), %rbp
	leaq	16(%r10,%rax), %r11
	negq	%rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_252:                              # %vector.body.prol
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-16(%r11,%rbx), %xmm0
	movups	(%r11,%rbx), %xmm1
	movups	%xmm0, -16(%rbp,%rbx)
	movups	%xmm1, (%rbp,%rbx)
	addq	$32, %rbx
	incq	%rcx
	jne	.LBB0_252
	jmp	.LBB0_255
.LBB0_253:                              #   in Loop: Header=BB0_16 Depth=2
	movq	%r10, %rbx
	jmp	.LBB0_260
.LBB0_254:                              #   in Loop: Header=BB0_16 Depth=2
	xorl	%ebx, %ebx
.LBB0_255:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_16 Depth=2
	cmpq	$96, %rdx
	jb	.LBB0_258
# BB#256:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_16 Depth=2
	movq	%r8, %rcx
	subq	%rbx, %rcx
	leaq	112(%r10,%rbx), %rbx
	.p2align	4, 0x90
.LBB0_257:                              # %vector.body
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-112(%rax,%rbx), %xmm0
	movups	-96(%rax,%rbx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rax,%rbx), %xmm0
	movups	-64(%rax,%rbx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rax,%rbx), %xmm0
	movups	-32(%rax,%rbx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rax,%rbx), %xmm0
	movups	(%rax,%rbx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	addq	$-128, %rcx
	jne	.LBB0_257
.LBB0_258:                              # %middle.block
                                        #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r8, %r12
	movq	56(%rsp), %r11          # 8-byte Reload
	je	.LBB0_15
# BB#259:                               #   in Loop: Header=BB0_16 Depth=2
	subl	%r8d, %edi
	addq	%r10, %r8
	movq	%r8, %rbx
.LBB0_260:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_16 Depth=2
	leal	-1(%rdi), %ebp
	movw	%di, %dx
	andw	$7, %dx
	je	.LBB0_263
# BB#261:                               # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB0_16 Depth=2
	negl	%edx
	.p2align	4, 0x90
.LBB0_262:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	decl	%edi
	movzbl	(%rax,%rbx), %ecx
	movb	%cl, (%rbx)
	incq	%rbx
	incw	%dx
	jne	.LBB0_262
.LBB0_263:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB0_16 Depth=2
	movzwl	%bp, %ecx
	cmpl	$7, %ecx
	jb	.LBB0_15
# BB#264:                               # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB0_16 Depth=2
	addq	$7, %rbx
	.p2align	4, 0x90
.LBB0_265:                              # %.lr.ph.i
                                        #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-7(%rax,%rbx), %ecx
	movb	%cl, -7(%rbx)
	movzbl	-6(%rax,%rbx), %ecx
	movb	%cl, -6(%rbx)
	movzbl	-5(%rax,%rbx), %ecx
	movb	%cl, -5(%rbx)
	movzbl	-4(%rax,%rbx), %ecx
	movb	%cl, -4(%rbx)
	movzbl	-3(%rax,%rbx), %ecx
	movb	%cl, -3(%rbx)
	movzbl	-2(%rax,%rbx), %ecx
	movb	%cl, -2(%rbx)
	movzbl	-1(%rax,%rbx), %ecx
	movb	%cl, -1(%rbx)
	addw	$-8, %di
	movzbl	(%rax,%rbx), %ecx
	movb	%cl, (%rbx)
	leaq	8(%rbx), %rbx
	jne	.LBB0_265
	jmp	.LBB0_15
.LBB0_266:                              #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_271
# BB#267:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %r8
	jb	.LBB0_282
# BB#268:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#269:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdx
	jbe	.LBB0_282
# BB#270:                               # %getbitmap.exit41.i.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	(%r8), %esi
	movb	$32, %r13b
	jmp	.LBB0_236
.LBB0_271:                              #   in Loop: Header=BB0_16 Depth=2
	movb	$14, %al
	subb	%r13b, %al
	movl	%eax, %ecx
	shrl	%cl, %edi
	cmpq	%r15, %r8
	jb	.LBB0_282
# BB#272:                               #   in Loop: Header=BB0_16 Depth=2
	leaq	4(%r8), %rdx
	cmpq	(%rsp), %rdx            # 8-byte Folded Reload
	ja	.LBB0_282
# BB#273:                               #   in Loop: Header=BB0_16 Depth=2
	cmpq	%r15, %rdx
	jbe	.LBB0_282
# BB#274:                               #   in Loop: Header=BB0_16 Depth=2
	movzbl	%al, %ebx
	movl	(%r8), %r9d
	movl	%eax, %ecx
	shldl	%cl, %r9d, %edi
	movb	$32, %r13b
	movq	%rdx, %r8
.LBB0_275:                              # %.sink.split.i.i
                                        #   in Loop: Header=BB0_16 Depth=2
	movl	%ebx, %ecx
	shll	%cl, %r9d
	subb	%al, %r13b
	movq	%r8, %rdx
	jmp	.LBB0_235
.LBB0_276:                              # %wunpsect.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	%r15, %rdi
	callq	free
	cmpl	$511, %ebx              # imm = 0x1FF
	jne	.LBB0_283
# BB#277:                               #   in Loop: Header=BB0_1 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 16(%rax)
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	24(%rsp), %r14d         # 4-byte Reload
	movl	184(%rsp), %ebp
	movq	%rbp, %rsi
	movq	176(%rsp), %rbp
	movq	80(%rsp), %rdx          # 8-byte Reload
	jne	.LBB0_1
# BB#278:
	movl	44(%rsp), %edx          # 4-byte Reload
	movw	192(%rsp), %ax
	movb	%al, 6(%rcx,%rdx)
	movb	%ah, 7(%rcx,%rdx)  # NOREX
	movq	%rax, %r12
	movq	%rdx, %r15
	movl	661(%rbp), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	665(%rcx,%rax), %r14d
	xorl	%ebx, %ebx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%rsi, %rbp
	movl	%r14d, %esi
	callq	cli_dbgmsg
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%r14d, 40(%rdi,%r15)
	xorl	%eax, %eax
	testw	$4095, %bp              # imm = 0xFFF
	setne	%al
	shll	$12, %eax
	addl	%ebp, %eax
	andl	$-4096, %eax            # imm = 0xF000
	subl	%eax, 80(%rdi,%r15)
	movq	%r15, %rbp
	movzwl	20(%rdi,%r15), %esi
	movq	%r12, %rdx
	testw	%r12w, %r12w
	je	.LBB0_292
# BB#279:                               # %.lr.ph
	leaq	24(%rsi), %rcx
	movl	40(%rsp), %eax          # 4-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	movq	%rdx, %r9
	leal	-1(%r9), %edx
	movzwl	%dx, %edx
	leaq	(%rdx,%rdx,4), %rdx
	addq	%rbp, %rcx
	leaq	(%rbp,%rdx,8), %rdx
	leaq	64(%rsi,%rdx), %r8
	leaq	8(%rdi,%rcx), %rcx
	movq	%r9, %rbp
.LBB0_280:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	movl	8(%rcx), %esi
	cmpl	%edx, %esi
	cmoval	%esi, %edx
	xorl	%esi, %esi
	testw	$4095, %dx              # imm = 0xFFF
	setne	%sil
	shll	$12, %esi
	addl	%edx, %esi
	andl	$-4096, %esi            # imm = 0xF000
	movl	%esi, (%rcx)
	movl	%esi, 8(%rcx)
	movl	4(%rcx), %edx
	addl	%eax, %edx
	decw	%bp
	movl	%edx, 12(%rcx)
	leaq	40(%rcx), %rcx
	jne	.LBB0_280
# BB#281:                               # %._crit_edge.loopexit
	addq	%r8, %rdi
	jmp	.LBB0_293
.LBB0_282:                              # %wunpsect.exit.thread
	movq	%r15, %rdi
	callq	free
.LBB0_283:
	movl	$.L.str.5, %edi
.LBB0_284:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_285:
	movl	$1, %ebx
.LBB0_286:
	movl	%ebx, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_287:
	movl	$.L.str.1, %edi
	jmp	.LBB0_284
.LBB0_288:
	movq	%r15, %rdi
	callq	free
	movl	$.L.str.4, %edi
	jmp	.LBB0_284
.LBB0_289:
	movl	$.L.str.2, %edi
	jmp	.LBB0_284
.LBB0_290:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	jmp	.LBB0_285
.LBB0_292:
	addq	%rbp, %rdi
	leaq	24(%rsi,%rdi), %rdi
.LBB0_293:                              # %._crit_edge
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	$0, 32(%rdi)
	jmp	.LBB0_286
.Lfunc_end0:
	.size	wwunpack, .Lfunc_end0-wwunpack
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in wwunpack\n"
	.size	.L.str, 13

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"WWPack: next chunk out ouf file, giving up.\n"
	.size	.L.str.1, 45

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"WWPack: inconsistent/hacked data, go figure!\n"
	.size	.L.str.2, 46

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"WWPack: Can't allocate %d bytes\n"
	.size	.L.str.3, 33

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"WWPack: packed data out of bounds, giving up.\n"
	.size	.L.str.4, 47

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"WWPack: unpacking failed.\n"
	.size	.L.str.5, 27

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"WWPack: found OEP @%x\n"
	.size	.L.str.6, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
