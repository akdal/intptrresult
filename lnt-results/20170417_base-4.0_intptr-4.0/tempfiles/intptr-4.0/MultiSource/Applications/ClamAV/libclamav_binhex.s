	.text
	.file	"libclamav_binhex.bc"
	.globl	cli_binhex
	.p2align	4, 0x90
	.type	cli_binhex,@function
cli_binhex:                             # @cli_binhex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r15
	leaq	40(%rsp), %rdx
	movl	$1, %edi
	callq	__fxstat
	testl	%eax, %eax
	js	.LBB0_1
# BB#2:
	movq	88(%rsp), %r14
	testq	%r14, %r14
	je	.LBB0_3
# BB#4:
	callq	messageCreate
	movq	%rax, %r12
	movl	$-114, %ebx
	testq	%r12, %r12
	je	.LBB0_30
# BB#5:
	xorl	%edi, %edi
	movl	$1, %edx
	movl	$2, %ecx
	xorl	%r9d, %r9d
	movq	%r14, %rsi
	movl	%ebp, %r8d
	callq	mmap
	movq	%rax, %r13
	cmpq	$-1, %r13
	je	.LBB0_6
# BB#7:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testq	%r14, %r14
	jle	.LBB0_31
# BB#8:                                 # %.preheader.preheader
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r13, %rbx
	movq	%r14, %r13
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_9:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
	testq	%r13, %r13
	je	.LBB0_10
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_9 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph
                                        #   Parent Loop BB0_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rbx,%r12), %eax
	cmpb	$10, %al
	je	.LBB0_16
# BB#13:                                # %.lr.ph
                                        #   in Loop: Header=BB0_12 Depth=2
	cmpb	$13, %al
	je	.LBB0_16
# BB#14:                                #   in Loop: Header=BB0_12 Depth=2
	incq	%r12
	decq	%r13
	jne	.LBB0_12
# BB#15:                                # %.critedge.loopexitsplit
                                        #   in Loop: Header=BB0_9 Depth=1
	leaq	(%rbx,%r12), %r15
	xorl	%r13d, %r13d
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_9 Depth=1
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	%rbx, %r15
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_16:                               # %.lr.ph..critedge.loopexit_crit_edge
                                        #   in Loop: Header=BB0_9 Depth=1
	leaq	(%rbx,%r12), %r15
.LBB0_17:                               # %.critedge
                                        #   in Loop: Header=BB0_9 Depth=1
	leal	1(%r12), %eax
	movslq	%eax, %rsi
	movq	%rbp, %rdi
	callq	cli_realloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_22
# BB#18:                                #   in Loop: Header=BB0_9 Depth=1
	movslq	%r12d, %rbp
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movb	$0, (%r14,%rbp)
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	messageAddStr
	testl	%eax, %eax
	js	.LBB0_19
# BB#20:                                #   in Loop: Header=BB0_9 Depth=1
	testq	%r13, %r13
	jle	.LBB0_19
# BB#21:                                #   in Loop: Header=BB0_9 Depth=1
	cmpb	$13, (%r15)
	leaq	1(%r15), %rbx
	movl	$0, %eax
	movq	$-1, %rcx
	cmoveq	%rcx, %rax
	cmovneq	%r15, %rbx
	leaq	(%r13,%rax), %rcx
	incq	%rbx
	leaq	-1(%r13,%rax), %r13
	cmpq	$1, %rcx
	movq	%r14, %rbp
	jg	.LBB0_9
.LBB0_22:                               # %.thread
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	munmap
	testq	%rbp, %rbp
	movq	%rbp, %r14
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jne	.LBB0_23
	jmp	.LBB0_24
.LBB0_1:
	movl	$-115, %ebx
	jmp	.LBB0_30
.LBB0_3:
	xorl	%ebx, %ebx
	jmp	.LBB0_30
.LBB0_6:
	movq	%r12, %rdi
	callq	messageDestroy
	jmp	.LBB0_30
.LBB0_31:                               # %.thread81
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	munmap
	jmp	.LBB0_24
.LBB0_19:                               # %.thread.thread
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	munmap
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB0_23:
	movq	%r14, %rdi
	callq	free
.LBB0_24:
	movq	%r12, %rdi
	callq	binhexBegin
	testq	%rax, %rax
	je	.LBB0_25
# BB#26:
	movl	$.L.str.2, %esi
	movq	%r12, %rdi
	callq	messageSetEncoding
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	messageToFileblob
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_28
# BB#27:
	movq	%rbx, %rdi
	callq	fileblobGetFilename
	movq	%rax, %rcx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	cli_dbgmsg
	movq	%rbx, %rdi
	callq	fileblobDestroy
	jmp	.LBB0_29
.LBB0_25:
	movq	%r12, %rdi
	callq	messageDestroy
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-124, %ebx
	jmp	.LBB0_30
.LBB0_28:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	cli_errmsg
.LBB0_29:
	movq	%r12, %rdi
	callq	messageDestroy
	xorl	%eax, %eax
	testq	%rbx, %rbx
	movl	$-123, %ebx
	cmovnel	%eax, %ebx
.LBB0_30:
	movl	%ebx, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_binhex, .Lfunc_end0-cli_binhex
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"mmap'ed binhex file\n"
	.size	.L.str, 21

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"No binhex line found\n"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"x-binhex"
	.size	.L.str.2, 9

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Binhex file decoded to %s\n"
	.size	.L.str.3, 27

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Couldn't decode binhex file to %s\n"
	.size	.L.str.4, 35


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
