	.text
	.file	"7zAes.bc"
	.globl	_ZNK7NCrypto7NSevenZ8CKeyInfo9IsEqualToERKS1_
	.p2align	4, 0x90
	.type	_ZNK7NCrypto7NSevenZ8CKeyInfo9IsEqualToERKS1_,@function
_ZNK7NCrypto7NSevenZ8CKeyInfo9IsEqualToERKS1_: # @_ZNK7NCrypto7NSevenZ8CKeyInfo9IsEqualToERKS1_
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %eax
	cmpl	4(%rsi), %eax
	jne	.LBB0_11
# BB#1:
	movl	(%rdi), %ecx
	cmpl	(%rsi), %ecx
	jne	.LBB0_11
# BB#2:                                 # %.preheader
	testl	%eax, %eax
	je	.LBB0_6
# BB#3:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	8(%rdi,%rcx), %edx
	cmpb	8(%rsi,%rcx), %dl
	jne	.LBB0_11
# BB#4:                                 #   in Loop: Header=BB0_5 Depth=1
	incq	%rcx
	cmpl	%eax, %ecx
	jb	.LBB0_5
.LBB0_6:                                # %.critedge
	movq	32(%rdi), %rcx
	cmpq	32(%rsi), %rcx
	jne	.LBB0_11
# BB#7:                                 # %.preheader.i
	movb	$1, %al
	testq	%rcx, %rcx
	je	.LBB0_12
# BB#8:                                 # %.lr.ph.i
	movq	40(%rdi), %r8
	movq	40(%rsi), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_10:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%r8,%rdi), %edx
	cmpb	(%rsi,%rdi), %dl
	jne	.LBB0_11
# BB#9:                                 #   in Loop: Header=BB0_10 Depth=1
	incq	%rdi
	cmpq	%rcx, %rdi
	jb	.LBB0_10
	jmp	.LBB0_12
.LBB0_11:
	xorl	%eax, %eax
.LBB0_12:                               # %_ZeqIhEbRK7CBufferIT_ES4_.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end0:
	.size	_ZNK7NCrypto7NSevenZ8CKeyInfo9IsEqualToERKS1_, .Lfunc_end0-_ZNK7NCrypto7NSevenZ8CKeyInfo9IsEqualToERKS1_
	.cfi_endproc

	.globl	_ZN7NCrypto7NSevenZ8CKeyInfo15CalculateDigestEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CKeyInfo15CalculateDigestEv,@function
_ZN7NCrypto7NSevenZ8CKeyInfo15CalculateDigestEv: # @_ZN7NCrypto7NSevenZ8CKeyInfo15CalculateDigestEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$63, (%r14)
	jne	.LBB1_30
# BB#1:                                 # %.preheader31
	movl	4(%r14), %ecx
	testq	%rcx, %rcx
	je	.LBB1_2
# BB#3:                                 # %.lr.ph37
	cmpl	$7, %ecx
	jbe	.LBB1_4
# BB#6:                                 # %min.iters.checked
	movl	%ecx, %edx
	andl	$7, %edx
	movq	%rcx, %rax
	subq	%rdx, %rax
	je	.LBB1_4
# BB#7:                                 # %vector.body.preheader
	leaq	8(%r14), %rsi
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB1_8:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rbp
	movq	%rbp, 40(%rsi)
	addq	$8, %rsi
	addq	$-8, %rdi
	jne	.LBB1_8
# BB#9:                                 # %middle.block
	testl	%edx, %edx
	jne	.LBB1_5
	jmp	.LBB1_10
.LBB1_30:
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	callq	Sha256_Init
	movb	(%r14), %cl
	movl	$1, %r13d
	shlq	%cl, %r13
	movq	$0, 8(%rsp)
	leaq	8(%r14), %r15
	xorl	%ebp, %ebp
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB1_31:                               # =>This Inner Loop Header: Depth=1
	movl	4(%r14), %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	Sha256_Update
	movq	32(%r14), %rdx
	movq	40(%r14), %rsi
	movq	%rbx, %rdi
	callq	Sha256_Update
	movl	$8, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	Sha256_Update
	incb	8(%rsp)
	jne	.LBB1_32
# BB#35:                                #   in Loop: Header=BB1_31 Depth=1
	incb	9(%rsp)
	jne	.LBB1_32
# BB#36:                                #   in Loop: Header=BB1_31 Depth=1
	incb	10(%rsp)
	jne	.LBB1_32
# BB#37:                                #   in Loop: Header=BB1_31 Depth=1
	incb	11(%rsp)
	jne	.LBB1_32
# BB#38:                                #   in Loop: Header=BB1_31 Depth=1
	incb	12(%rsp)
	jne	.LBB1_32
# BB#39:                                #   in Loop: Header=BB1_31 Depth=1
	incb	13(%rsp)
	jne	.LBB1_32
# BB#40:                                #   in Loop: Header=BB1_31 Depth=1
	incb	14(%rsp)
	jne	.LBB1_32
# BB#41:                                #   in Loop: Header=BB1_31 Depth=1
	incb	15(%rsp)
	.p2align	4, 0x90
.LBB1_32:                               #   in Loop: Header=BB1_31 Depth=1
	incq	%rbp
	cmpq	%r13, %rbp
	jb	.LBB1_31
# BB#33:
	addq	$48, %r14
	leaq	16(%rsp), %rdi
	movq	%r14, %rsi
	callq	Sha256_Final
	jmp	.LBB1_34
.LBB1_4:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_5:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	8(%r14,%rax), %edx
	movb	%dl, 48(%r14,%rax)
	incq	%rax
	cmpq	%rcx, %rax
	jb	.LBB1_5
	jmp	.LBB1_10
.LBB1_2:
	xorl	%eax, %eax
.LBB1_10:                               # %.preheader30
	cmpl	$31, %eax
	ja	.LBB1_28
# BB#11:                                # %.preheader30
	movq	32(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB1_28
# BB#12:                                # %.lr.ph35
	movq	40(%r14), %rdx
	movq	%rcx, %rdi
	negq	%rdi
	movl	$31, %r9d
	subl	%eax, %r9d
	notq	%r9
	cmpq	%r9, %rdi
	cmovaq	%rdi, %r9
	negq	%r9
	cmpq	$31, %r9
	jbe	.LBB1_13
# BB#17:                                # %min.iters.checked50
	movabsq	$8589934560, %rsi       # imm = 0x1FFFFFFE0
	andq	%r9, %rsi
	je	.LBB1_13
# BB#18:                                # %vector.memcheck
	movl	%eax, %r10d
	leaq	48(%r14,%r10), %r8
	movl	$31, %ebp
	subl	%eax, %ebp
	notq	%rbp
	cmpq	%rbp, %rdi
	cmovaq	%rdi, %rbp
	movq	%rdx, %rdi
	subq	%rbp, %rdi
	cmpq	%rdi, %r8
	jae	.LBB1_20
# BB#19:                                # %vector.memcheck
	leaq	48(%r10), %rdi
	subq	%rbp, %rdi
	addq	%r14, %rdi
	cmpq	%rdi, %rdx
	jae	.LBB1_20
.LBB1_13:
	xorl	%esi, %esi
.LBB1_14:                               # %scalar.ph48.preheader
	incq	%rsi
	movl	%eax, %eax
	.p2align	4, 0x90
.LBB1_15:                               # %scalar.ph48
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdx,%rsi), %ebx
	movb	%bl, 48(%r14,%rax)
	incq	%rax
	cmpl	$31, %eax
	ja	.LBB1_28
# BB#16:                                # %scalar.ph48
                                        #   in Loop: Header=BB1_15 Depth=1
	cmpq	%rcx, %rsi
	leaq	1(%rsi), %rsi
	jb	.LBB1_15
.LBB1_28:                               # %.preheader
	cmpl	$31, %eax
	ja	.LBB1_34
# BB#29:                                # %.lr.ph.preheader
	movl	%eax, %ecx
	leaq	48(%r14,%rcx), %rdi
	movl	$31, %edx
	subl	%eax, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
.LBB1_34:                               # %.loopexit
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_20:                               # %vector.body46.preheader
	leaq	-32(%rsi), %r8
	movl	%r8d, %ebp
	shrl	$5, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB1_21
# BB#22:                                # %vector.body46.prol.preheader
	negq	%rbp
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_23:                               # %vector.body46.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r10,%rdi), %ebx
	movups	(%rdx,%rdi), %xmm0
	movups	16(%rdx,%rdi), %xmm1
	movups	%xmm0, 48(%r14,%rbx)
	movups	%xmm1, 64(%r14,%rbx)
	addq	$32, %rdi
	incq	%rbp
	jne	.LBB1_23
	jmp	.LBB1_24
.LBB1_21:
	xorl	%edi, %edi
.LBB1_24:                               # %vector.body46.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB1_27
# BB#25:                                # %vector.body46.preheader.new
	movq	%r9, %rbp
	andq	$-32, %rbp
	movq	%rdi, %r8
	subq	%rbp, %r8
	leaq	112(%rdx,%rdi), %rbp
	addq	%rdi, %r10
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_26:                               # %vector.body46
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r10,%rdi), %ebx
	movups	-112(%rbp,%rdi), %xmm0
	movups	-96(%rbp,%rdi), %xmm1
	movups	%xmm0, 48(%r14,%rbx)
	movups	%xmm1, 64(%r14,%rbx)
	leal	32(%r10,%rdi), %ebx
	movups	-80(%rbp,%rdi), %xmm0
	movups	-64(%rbp,%rdi), %xmm1
	movups	%xmm0, 48(%r14,%rbx)
	movups	%xmm1, 64(%r14,%rbx)
	leal	64(%r10,%rdi), %ebx
	movups	-48(%rbp,%rdi), %xmm0
	movups	-32(%rbp,%rdi), %xmm1
	movups	%xmm0, 48(%r14,%rbx)
	movups	%xmm1, 64(%r14,%rbx)
	leal	96(%r10,%rdi), %ebx
	movups	-16(%rbp,%rdi), %xmm0
	movups	(%rbp,%rdi), %xmm1
	movups	%xmm0, 48(%r14,%rbx)
	movups	%xmm1, 64(%r14,%rbx)
	subq	$-128, %rdi
	movq	%r8, %rbx
	addq	%rdi, %rbx
	jne	.LBB1_26
.LBB1_27:                               # %middle.block47
	addl	%esi, %eax
	cmpq	%r9, %rsi
	jne	.LBB1_14
	jmp	.LBB1_28
.Lfunc_end1:
	.size	_ZN7NCrypto7NSevenZ8CKeyInfo15CalculateDigestEv, .Lfunc_end1-_ZN7NCrypto7NSevenZ8CKeyInfo15CalculateDigestEv
	.cfi_endproc

	.globl	_ZN7NCrypto7NSevenZ13CKeyInfoCache4FindERNS0_8CKeyInfoE
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ13CKeyInfoCache4FindERNS0_8CKeyInfoE,@function
_ZN7NCrypto7NSevenZ13CKeyInfoCache4FindERNS0_8CKeyInfoE: # @_ZN7NCrypto7NSevenZ13CKeyInfoCache4FindERNS0_8CKeyInfoE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movslq	20(%r15), %r10
	testq	%r10, %r10
	jle	.LBB2_18
# BB#1:                                 # %.lr.ph
	movq	24(%r15), %r9
	movl	(%rsi), %r8d
	movl	4(%rsi), %ecx
	testl	%ecx, %ecx
	movq	32(%rsi), %rdi
	movq	40(%rsi), %r11
	je	.LBB2_6
# BB#2:                                 # %.lr.ph.split.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_16 Depth 2
                                        #     Child Loop BB2_22 Depth 2
	movq	(%r9,%rbp,8), %r12
	cmpl	4(%r12), %ecx
	jne	.LBB2_17
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	cmpl	(%r12), %r8d
	jne	.LBB2_17
# BB#5:                                 # %.lr.ph.i26.preheader
                                        #   in Loop: Header=BB2_3 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_16:                               # %.lr.ph.i26
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %edx
	movzbl	8(%rsi,%rdx), %ebx
	cmpb	8(%r12,%rdx), %bl
	jne	.LBB2_17
# BB#15:                                #   in Loop: Header=BB2_16 Depth=2
	incl	%eax
	cmpl	%ecx, %eax
	jb	.LBB2_16
# BB#19:                                # %.critedge.i
                                        #   in Loop: Header=BB2_3 Depth=1
	cmpq	32(%r12), %rdi
	jne	.LBB2_17
# BB#20:                                # %.preheader.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	testq	%rdi, %rdi
	je	.LBB2_24
# BB#21:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	40(%r12), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_22:                               #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r11,%rdx), %ebx
	cmpb	(%rax,%rdx), %bl
	jne	.LBB2_17
# BB#23:                                #   in Loop: Header=BB2_22 Depth=2
	incq	%rdx
	cmpq	%rdi, %rdx
	jb	.LBB2_22
	jmp	.LBB2_24
	.p2align	4, 0x90
.LBB2_17:                               # %.loopexit
                                        #   in Loop: Header=BB2_3 Depth=1
	incq	%rbp
	cmpq	%r10, %rbp
	jl	.LBB2_3
	jmp	.LBB2_18
.LBB2_6:                                # %.lr.ph.split.us.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_7:                                # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_12 Depth 2
	movq	(%r9,%rbp,8), %r12
	cmpl	$0, 4(%r12)
	jne	.LBB2_13
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	cmpl	(%r12), %r8d
	jne	.LBB2_13
# BB#9:                                 # %.critedge.i.us
                                        #   in Loop: Header=BB2_7 Depth=1
	cmpq	32(%r12), %rdi
	jne	.LBB2_13
# BB#10:                                # %.preheader.i.i.us
                                        #   in Loop: Header=BB2_7 Depth=1
	testq	%rdi, %rdi
	je	.LBB2_24
# BB#11:                                # %.lr.ph.i.i.us
                                        #   in Loop: Header=BB2_7 Depth=1
	movq	40(%r12), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_12:                               #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r11,%rcx), %edx
	cmpb	(%rax,%rcx), %dl
	jne	.LBB2_13
# BB#14:                                #   in Loop: Header=BB2_12 Depth=2
	incq	%rcx
	cmpq	%rdi, %rcx
	jb	.LBB2_12
	jmp	.LBB2_24
	.p2align	4, 0x90
.LBB2_13:                               # %.loopexit.us
                                        #   in Loop: Header=BB2_7 Depth=1
	incq	%rbp
	cmpq	%r10, %rbp
	jl	.LBB2_7
.LBB2_18:
	xorl	%eax, %eax
	jmp	.LBB2_36
.LBB2_24:                               # %_ZNK7NCrypto7NSevenZ8CKeyInfo9IsEqualToERKS1_.exit.preheader
	movb	48(%r12), %al
	movb	%al, 48(%rsi)
	movb	49(%r12), %al
	movb	%al, 49(%rsi)
	movb	50(%r12), %al
	movb	%al, 50(%rsi)
	movb	51(%r12), %al
	movb	%al, 51(%rsi)
	movb	52(%r12), %al
	movb	%al, 52(%rsi)
	movb	53(%r12), %al
	movb	%al, 53(%rsi)
	movb	54(%r12), %al
	movb	%al, 54(%rsi)
	movb	55(%r12), %al
	movb	%al, 55(%rsi)
	movb	56(%r12), %al
	movb	%al, 56(%rsi)
	movb	57(%r12), %al
	movb	%al, 57(%rsi)
	movb	58(%r12), %al
	movb	%al, 58(%rsi)
	movb	59(%r12), %al
	movb	%al, 59(%rsi)
	movb	60(%r12), %al
	movb	%al, 60(%rsi)
	movb	61(%r12), %al
	movb	%al, 61(%rsi)
	movb	62(%r12), %al
	movb	%al, 62(%rsi)
	movb	63(%r12), %al
	movb	%al, 63(%rsi)
	movb	64(%r12), %al
	movb	%al, 64(%rsi)
	movb	65(%r12), %al
	movb	%al, 65(%rsi)
	movb	66(%r12), %al
	movb	%al, 66(%rsi)
	movb	67(%r12), %al
	movb	%al, 67(%rsi)
	movb	68(%r12), %al
	movb	%al, 68(%rsi)
	movb	69(%r12), %al
	movb	%al, 69(%rsi)
	movb	70(%r12), %al
	movb	%al, 70(%rsi)
	movb	71(%r12), %al
	movb	%al, 71(%rsi)
	movb	72(%r12), %al
	movb	%al, 72(%rsi)
	movb	73(%r12), %al
	movb	%al, 73(%rsi)
	movb	74(%r12), %al
	movb	%al, 74(%rsi)
	movb	75(%r12), %al
	movb	%al, 75(%rsi)
	movb	76(%r12), %al
	movb	%al, 76(%rsi)
	movb	77(%r12), %al
	movb	%al, 77(%rsi)
	movb	78(%r12), %al
	movb	%al, 78(%rsi)
	movb	79(%r12), %al
	movb	%al, 79(%rsi)
	movb	$1, %al
	testl	%ebp, %ebp
	je	.LBB2_36
# BB#25:
	movl	$80, %edi
	callq	_Znwm
	movq	%rax, %r13
	movq	16(%r12), %rax
	movq	%rax, 16(%r13)
	movups	(%r12), %xmm0
	movups	%xmm0, (%r13)
	movq	$_ZTV7CBufferIhE+16, 24(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r13)
	movq	32(%r12), %r14
	testq	%r14, %r14
	je	.LBB2_28
# BB#26:                                # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
.Ltmp0:
	movq	%r14, %rdi
	callq	_Znam
.Ltmp1:
# BB#27:                                # %.noexc.i
	movq	%rax, 40(%r13)
	movq	%r14, 32(%r13)
	movq	40(%r12), %rsi
	movq	%rax, %rdi
	movq	%r14, %rdx
	callq	memmove
.LBB2_28:                               # %_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6InsertEiRKS2_.exit
	leaq	8(%r15), %rdi
	addq	$48, %r12
	movups	(%r12), %xmm0
	movups	16(%r12), %xmm1
	movups	%xmm1, 64(%r13)
	movups	%xmm0, 48(%r13)
	xorl	%esi, %esi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
	movq	24(%r15), %rax
	movq	%r13, (%rax)
	leal	1(%rbp), %edx
	addl	$2, %ebp
	movl	20(%r15), %eax
	movl	%eax, %ecx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	subl	%edx, %ecx
	cmpl	%eax, %ebp
	movl	$1, %r13d
	cmovgl	%ecx, %r13d
	testl	%r13d, %r13d
	jle	.LBB2_35
# BB#29:                                # %.lr.ph.i
	movslq	12(%rsp), %rbx          # 4-byte Folded Reload
	movslq	%r13d, %r14
	shlq	$3, %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_30:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rax
	addq	%rbx, %rax
	movq	(%rax,%r12,8), %rbp
	testq	%rbp, %rbp
	je	.LBB2_34
# BB#31:                                #   in Loop: Header=BB2_30 Depth=1
	movq	$_ZTV7CBufferIhE+16, 24(%rbp)
	movq	40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_33
# BB#32:                                #   in Loop: Header=BB2_30 Depth=1
	callq	_ZdaPv
.LBB2_33:                               # %_ZN7NCrypto7NSevenZ8CKeyInfoD2Ev.exit.i
                                        #   in Loop: Header=BB2_30 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB2_34:                               #   in Loop: Header=BB2_30 Depth=1
	incq	%r12
	cmpq	%r14, %r12
	jl	.LBB2_30
.LBB2_35:                               # %_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii.exit
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%r13d, %edx
	callq	_ZN17CBaseRecordVector6DeleteEii
	movb	$1, %al
.LBB2_36:                               # %.thread
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_37:
.Ltmp2:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN7NCrypto7NSevenZ13CKeyInfoCache4FindERNS0_8CKeyInfoE, .Lfunc_end2-_ZN7NCrypto7NSevenZ13CKeyInfoCache4FindERNS0_8CKeyInfoE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii: # @_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB3_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB3_6
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 24(%rbp)
	movq	40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	callq	_ZdaPv
.LBB3_5:                                # %_ZN7NCrypto7NSevenZ8CKeyInfoD2Ev.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB3_2
.LBB3_7:                                # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end3:
	.size	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii, .Lfunc_end3-_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii
	.cfi_endproc

	.text
	.globl	_ZN7NCrypto7NSevenZ13CKeyInfoCache3AddERNS0_8CKeyInfoE
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ13CKeyInfoCache3AddERNS0_8CKeyInfoE,@function
_ZN7NCrypto7NSevenZ13CKeyInfoCache3AddERNS0_8CKeyInfoE: # @_ZN7NCrypto7NSevenZ13CKeyInfoCache3AddERNS0_8CKeyInfoE
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 48
.Lcfi44:
	.cfi_offset %rbx, -48
.Lcfi45:
	.cfi_offset %r12, -40
.Lcfi46:
	.cfi_offset %r13, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	callq	_ZN7NCrypto7NSevenZ13CKeyInfoCache4FindERNS0_8CKeyInfoE
	testb	%al, %al
	jne	.LBB4_7
# BB#1:
	leaq	8(%r14), %r15
	movl	20(%r14), %eax
	cmpl	(%r14), %eax
	jl	.LBB4_3
# BB#2:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.LBB4_3:
	movl	$80, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	16(%r12), %rax
	movq	%rax, 16(%rbx)
	movups	(%r12), %xmm0
	movups	%xmm0, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 24(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.LBB4_6
# BB#4:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
.Ltmp3:
	movq	%r13, %rdi
	callq	_Znam
.Ltmp4:
# BB#5:                                 # %.noexc.i
	movq	%rax, 40(%rbx)
	movq	%r13, 32(%rbx)
	movq	40(%r12), %rsi
	movq	%rax, %rdi
	movq	%r13, %rdx
	callq	memmove
.LBB4_6:                                # %_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6InsertEiRKS2_.exit
	movups	48(%r12), %xmm0
	movups	64(%r12), %xmm1
	movups	%xmm1, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector13InsertOneItemEi
	movq	24(%r14), %rax
	movq	%rbx, (%rax)
.LBB4_7:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB4_8:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN7NCrypto7NSevenZ13CKeyInfoCache3AddERNS0_8CKeyInfoE, .Lfunc_end4-_ZN7NCrypto7NSevenZ13CKeyInfoCache3AddERNS0_8CKeyInfoE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev,"axG",@progbits,_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev,comdat
	.weak	_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev,@function
_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev: # @_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE+16, 8(%rbx)
	addq	$8, %rbx
.Ltmp6:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp7:
# BB#1:                                 # %_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB5_2:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp10:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev, .Lfunc_end5-_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin2    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin2   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,"axG",@progbits,_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,comdat
	.weak	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev,@function
_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev: # @_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.cfi_startproc
# BB#0:
	jmp	pthread_mutex_destroy   # TAILCALL
.Lfunc_end6:
	.size	_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev, .Lfunc_end6-_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev
	.cfi_endproc

	.text
	.globl	_ZN7NCrypto7NSevenZ5CBaseC2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ5CBaseC2Ev,@function
_ZN7NCrypto7NSevenZ5CBaseC2Ev:          # @_ZN7NCrypto7NSevenZ5CBaseC2Ev
	.cfi_startproc
# BB#0:
	movl	$16, (%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rdi)
	movq	$8, 32(%rdi)
	movq	$_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE+16, 8(%rdi)
	movq	$_ZTV7CBufferIhE+16, 64(%rdi)
	movups	%xmm0, 72(%rdi)
	movups	%xmm0, 40(%rdi)
	movq	$0, 56(%rdi)
	movups	%xmm0, 120(%rdi)
	movl	$0, 136(%rdi)
	retq
.Lfunc_end7:
	.size	_ZN7NCrypto7NSevenZ5CBaseC2Ev, .Lfunc_end7-_ZN7NCrypto7NSevenZ5CBaseC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end8:
	.size	__clang_call_terminate, .Lfunc_end8-__clang_call_terminate

	.text
	.globl	_ZN7NCrypto7NSevenZ5CBase15CalculateDigestEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ5CBase15CalculateDigestEv,@function
_ZN7NCrypto7NSevenZ5CBase15CalculateDigestEv: # @_ZN7NCrypto7NSevenZ5CBase15CalculateDigestEv
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$_ZN7NCrypto7NSevenZL31g_GlobalKeyCacheCriticalSectionE, %edi
	callq	pthread_mutex_lock
	leaq	40(%r14), %rbx
.Ltmp12:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN7NCrypto7NSevenZ13CKeyInfoCache4FindERNS0_8CKeyInfoE
.Ltmp13:
# BB#1:
	testb	%al, %al
	je	.LBB9_3
# BB#2:
.Ltmp22:
	movl	$_ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE, %edi
	movq	%rbx, %rsi
	callq	_ZN7NCrypto7NSevenZ13CKeyInfoCache3AddERNS0_8CKeyInfoE
.Ltmp23:
	jmp	.LBB9_8
.LBB9_3:
.Ltmp14:
	movl	$_ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE, %edi
	movq	%rbx, %rsi
	callq	_ZN7NCrypto7NSevenZ13CKeyInfoCache4FindERNS0_8CKeyInfoE
.Ltmp15:
# BB#4:
	testb	%al, %al
	jne	.LBB9_7
# BB#5:
.Ltmp16:
	movq	%rbx, %rdi
	callq	_ZN7NCrypto7NSevenZ8CKeyInfo15CalculateDigestEv
.Ltmp17:
# BB#6:
.Ltmp18:
	movl	$_ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE, %edi
	movq	%rbx, %rsi
	callq	_ZN7NCrypto7NSevenZ13CKeyInfoCache3AddERNS0_8CKeyInfoE
.Ltmp19:
.LBB9_7:
.Ltmp20:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZN7NCrypto7NSevenZ13CKeyInfoCache3AddERNS0_8CKeyInfoE
.Ltmp21:
.LBB9_8:
	movl	$_ZN7NCrypto7NSevenZL31g_GlobalKeyCacheCriticalSectionE, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	pthread_mutex_unlock    # TAILCALL
.LBB9_9:
.Ltmp24:
	movq	%rax, %rbx
	movl	$_ZN7NCrypto7NSevenZL31g_GlobalKeyCacheCriticalSectionE, %edi
	callq	pthread_mutex_unlock
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end9:
	.size	_ZN7NCrypto7NSevenZ5CBase15CalculateDigestEv, .Lfunc_end9-_ZN7NCrypto7NSevenZ5CBase15CalculateDigestEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp21-.Ltmp12         #   Call between .Ltmp12 and .Ltmp21
	.long	.Ltmp24-.Lfunc_begin3   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end9-.Ltmp21     #   Call between .Ltmp21 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv,@function
_ZN7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv: # @_ZN7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi59:
	.cfi_def_cfa_offset 16
	movl	$8, 160(%rdi)
	leaq	144(%rdi), %rsi
	movl	$g_RandomGenerator, %edi
	movl	$8, %edx
	callq	_ZN16CRandomGenerator8GenerateEPhj
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv, .Lfunc_end10-_ZN7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv
	.cfi_endproc

	.globl	_ZThn184_N7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv
	.p2align	4, 0x90
	.type	_ZThn184_N7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv,@function
_ZThn184_N7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv: # @_ZThn184_N7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi60:
	.cfi_def_cfa_offset 16
	movl	$8, -24(%rdi)
	leaq	-40(%rdi), %rsi
	movl	$g_RandomGenerator, %edi
	movl	$8, %edx
	callq	_ZN16CRandomGenerator8GenerateEPhj
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZThn184_N7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv, .Lfunc_end11-_ZThn184_N7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv
	.cfi_endproc

	.globl	_ZN7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZN7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZN7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -32
.Lcfi66:
	.cfi_offset %r14, -24
.Lcfi67:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	160(%r15), %ebx
	cmpq	$15, %rbx
	ja	.LBB12_2
# BB#1:                                 # %.lr.ph.preheader
	leaq	144(%r15,%rbx), %rdi
	movl	$15, %edx
	subl	%ebx, %edx
	incq	%rdx
	xorl	%esi, %esi
	callq	memset
	movl	160(%r15), %ebx
.LBB12_2:                               # %._crit_edge
	movl	$19, 64(%r15)
	cmpl	$0, 68(%r15)
	setne	%al
	shlb	$7, %al
	testl	%ebx, %ebx
	setne	%cl
	shlb	$6, %cl
	orb	%al, %cl
	orb	$19, %cl
	movb	%cl, 15(%rsp)
	movq	(%r14), %rax
	leaq	15(%rsp), %rsi
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	*40(%rax)
	testl	%eax, %eax
	je	.LBB12_3
.LBB12_13:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB12_3:
	movl	68(%r15), %ecx
	movl	%ecx, %eax
	orl	%ebx, %eax
	movl	$0, %eax
	je	.LBB12_13
# BB#4:
	testl	%ecx, %ecx
	je	.LBB12_5
# BB#6:
	shll	$4, %ecx
	addl	$4080, %ecx             # imm = 0xFF0
	andl	$4080, %ecx             # imm = 0xFF0
	jmp	.LBB12_7
.LBB12_5:
	xorl	%ecx, %ecx
.LBB12_7:
	leal	255(%rbx), %eax
	testl	%ebx, %ebx
	cmovel	%ebx, %eax
	orl	%ecx, %eax
	movb	%al, 14(%rsp)
	movq	(%r14), %rax
	leaq	14(%rsp), %rsi
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	*40(%rax)
	testl	%eax, %eax
	jne	.LBB12_13
# BB#8:
	movl	68(%r15), %edx
	testq	%rdx, %rdx
	je	.LBB12_10
# BB#9:
	leaq	72(%r15), %rsi
	movq	%r14, %rdi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB12_13
.LBB12_10:
	testl	%ebx, %ebx
	je	.LBB12_12
# BB#11:
	addq	$144, %r15
	movl	%ebx, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	testl	%eax, %eax
	jne	.LBB12_13
.LBB12_12:
	xorl	%eax, %eax
	jmp	.LBB12_13
.Lfunc_end12:
	.size	_ZN7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end12-_ZN7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZThn176_N7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream,@function
_ZThn176_N7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream: # @_ZThn176_N7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_startproc
# BB#0:
	addq	$-176, %rdi
	jmp	_ZN7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream # TAILCALL
.Lfunc_end13:
	.size	_ZThn176_N7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream, .Lfunc_end13-_ZThn176_N7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.cfi_endproc

	.globl	_ZN7NCrypto7NSevenZ8CEncoder12CreateFilterEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CEncoder12CreateFilterEv,@function
_ZN7NCrypto7NSevenZ8CEncoder12CreateFilterEv: # @_ZN7NCrypto7NSevenZ8CEncoder12CreateFilterEv
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 32
.Lcfi71:
	.cfi_offset %rbx, -24
.Lcfi72:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$328, %edi              # imm = 0x148
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp25:
	movq	%rbx, %rdi
	callq	_ZN7NCrypto14CAesCbcEncoderC1Ev
.Ltmp26:
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	168(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB14_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB14_3:                               # %_ZN9CMyComPtrI15ICompressFilterEaSEPS0_.exit
	movq	%rbx, 168(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB14_4:
.Ltmp27:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN7NCrypto7NSevenZ8CEncoder12CreateFilterEv, .Lfunc_end14-_ZN7NCrypto7NSevenZ8CEncoder12CreateFilterEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp25-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin4   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Lfunc_end14-.Ltmp26    #   Call between .Ltmp26 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj,@function
_ZN7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj: # @_ZN7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 40
.Lcfi77:
	.cfi_offset %rbx, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rdi)
	movq	$0, 80(%rdi)
	movups	%xmm0, 144(%rdi)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.LBB15_26
# BB#1:
	leaq	64(%rdi), %r8
	movzbl	(%rsi), %ebx
	movl	%ebx, %r14d
	andl	$63, %r14d
	movl	%r14d, (%r8)
	testb	$-64, %bl
	je	.LBB15_26
# BB#2:
	movl	%ebx, %ecx
	shrl	$7, %ecx
	movl	%ecx, 68(%rdi)
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$2, %edx
	jb	.LBB15_26
# BB#3:
	shrl	$6, %ebx
	andl	$1, %ebx
	movzbl	1(%rsi), %r9d
	movl	%r9d, %ebp
	shrl	$4, %ebp
	leal	(%rbp,%rcx), %r10d
	movl	%r10d, 68(%rdi)
	andl	$15, %r9d
	addl	%ebx, %r9d
	leal	2(%r10,%r9), %ebx
	cmpl	%edx, %ebx
	ja	.LBB15_26
# BB#4:                                 # %.preheader42
	testl	%r10d, %r10d
	je	.LBB15_5
# BB#6:                                 # %.lr.ph47.preheader
	leal	-1(%rcx,%rbp), %r11d
	testb	$3, %r10b
	je	.LBB15_7
# BB#8:                                 # %.lr.ph47.prol.preheader
	movl	%r10d, %eax
	andl	$3, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_9:                               # %.lr.ph47.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	2(%rsi,%rdx), %ecx
	movb	%cl, 72(%rdi,%rdx)
	incq	%rdx
	cmpl	%edx, %eax
	jne	.LBB15_9
# BB#10:                                # %.lr.ph47.prol.loopexit.unr-lcssa
	leal	2(%rdx), %r15d
	cmpl	$3, %r11d
	jae	.LBB15_12
	jmp	.LBB15_15
.LBB15_5:
	movl	$2, %r15d
	testl	%r9d, %r9d
	jne	.LBB15_16
	jmp	.LBB15_25
.LBB15_7:
	movl	$2, %r15d
	xorl	%edx, %edx
	cmpl	$3, %r11d
	jb	.LBB15_15
.LBB15_12:                              # %.lr.ph47.preheader.new
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_13:                              # %.lr.ph47
                                        # =>This Inner Loop Header: Depth=1
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	leal	(%rdx,%rcx), %ebp
	leal	(%r15,%rcx), %ebx
	leal	1(%r15,%rcx), %eax
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, 72(%rdi,%rbp)
	leal	1(%rdx,%rcx), %ebp
	leal	2(%r15,%rcx), %ebx
	movzbl	(%rsi,%rax), %eax
	movb	%al, 72(%rdi,%rbp)
	leal	2(%rdx,%rcx), %eax
	leal	3(%r15,%rcx), %ebp
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, 72(%rdi,%rax)
	leal	3(%rdx,%rcx), %eax
	movzbl	(%rsi,%rbp), %ebx
	movb	%bl, 72(%rdi,%rax)
	leal	4(%rdx,%rcx), %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	addl	$4, %ecx
	cmpl	%r10d, %eax
	jb	.LBB15_13
# BB#14:                                # %.preheader.loopexit.unr-lcssa
	addl	%ecx, %r15d
.LBB15_15:                              # %.preheader
	testl	%r9d, %r9d
	je	.LBB15_25
.LBB15_16:                              # %.lr.ph.preheader
	movl	%r9d, %r10d
	leaq	-1(%r10), %r9
	movq	%r10, %rcx
	andq	$3, %rcx
	je	.LBB15_17
# BB#18:                                # %.lr.ph.prol.preheader
	movl	%r15d, %r11d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_19:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r11,%rdx), %ebp
	movzbl	(%rsi,%rbp), %ebx
	movb	%bl, 144(%rdi,%rdx)
	incq	%rdx
	cmpq	%rdx, %rcx
	jne	.LBB15_19
# BB#20:                                # %.lr.ph.prol.loopexit.unr-lcssa
	addl	%edx, %r15d
	cmpq	$3, %r9
	jae	.LBB15_22
	jmp	.LBB15_24
.LBB15_17:
	xorl	%edx, %edx
	cmpq	$3, %r9
	jb	.LBB15_24
.LBB15_22:                              # %.lr.ph.preheader.new
	subq	%rdx, %r10
	leaq	147(%rdi,%rdx), %rdx
	movl	%r15d, %r9d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB15_23:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r9,%rdi), %rcx
	leal	1(%rcx), %ebp
	movl	%ecx, %ebx
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, -3(%rdx,%rdi)
	leal	2(%rcx), %ebx
	movzbl	(%rsi,%rbp), %eax
	movb	%al, -2(%rdx,%rdi)
	addl	$3, %ecx
	movzbl	(%rsi,%rbx), %eax
	movb	%al, -1(%rdx,%rdi)
	movzbl	(%rsi,%rcx), %eax
	movb	%al, (%rdx,%rdi)
	addq	$4, %rdi
	cmpq	%rdi, %r10
	jne	.LBB15_23
.LBB15_24:                              # %._crit_edge.loopexit
	movl	(%r8), %r14d
.LBB15_25:                              # %._crit_edge
	xorl	%ecx, %ecx
	cmpl	$25, %r14d
	movl	$-2147467263, %eax      # imm = 0x80004001
	cmovll	%ecx, %eax
.LBB15_26:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end15-_ZN7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZThn176_N7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj,@function
_ZThn176_N7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj: # @_ZThn176_N7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	xorps	%xmm0, %xmm0
	movups	%xmm0, -112(%rdi)
	movq	$0, -96(%rdi)
	movups	%xmm0, -32(%rdi)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.LBB16_25
# BB#1:
	leaq	-112(%rdi), %r8
	movzbl	(%rsi), %ecx
	movl	%ecx, %r14d
	andl	$63, %r14d
	movl	%r14d, (%r8)
	testb	$-64, %cl
	je	.LBB16_25
# BB#2:
	leaq	-176(%rdi), %rbx
	movl	%ecx, %ebp
	shrl	$7, %ebp
	movl	%ebp, 68(%rbx)
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$2, %edx
	jb	.LBB16_25
# BB#3:
	shrl	$6, %ecx
	andl	$1, %ecx
	movzbl	1(%rsi), %r10d
	movl	%r10d, %r9d
	shrl	$4, %r9d
	addl	%ebp, %r9d
	movl	%r9d, 68(%rbx)
	andl	$15, %r10d
	addl	%ecx, %r10d
	leal	2(%r9,%r10), %ecx
	cmpl	%edx, %ecx
	ja	.LBB16_25
# BB#4:                                 # %.preheader42.i
	testl	%r9d, %r9d
	je	.LBB16_5
# BB#6:                                 # %.lr.ph47.i.preheader
	movl	%r9d, %eax
	leaq	-1(%rax), %r11
	movq	%rax, %rcx
	xorl	%edx, %edx
	andq	$3, %rcx
	je	.LBB16_7
	.p2align	4, 0x90
.LBB16_8:                               # %.lr.ph47.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	2(%rsi,%rdx), %ebx
	movb	%bl, -104(%rdi,%rdx)
	incq	%rdx
	cmpq	%rdx, %rcx
	jne	.LBB16_8
# BB#9:                                 # %.lr.ph47.i.prol.loopexit.unr-lcssa
	leaq	2(%rdx), %rcx
	cmpq	$3, %r11
	jae	.LBB16_11
	jmp	.LBB16_13
.LBB16_5:
	movl	$2, %r9d
	testl	%r10d, %r10d
	jne	.LBB16_15
	jmp	.LBB16_24
.LBB16_7:
	movl	$2, %ecx
	cmpq	$3, %r11
	jb	.LBB16_13
.LBB16_11:                              # %.lr.ph47.i.preheader.new
	leaq	3(%rsi,%rcx), %rcx
	subq	%rdx, %rax
	leaq	-101(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB16_12:                              # %.lr.ph47.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$4, %rcx
	addq	$4, %rdx
	addq	$-4, %rax
	jne	.LBB16_12
.LBB16_13:                              # %.preheader.i.loopexit
	addl	$2, %r9d
	testl	%r10d, %r10d
	je	.LBB16_24
.LBB16_15:                              # %.lr.ph.preheader.i
	movl	%r10d, %r11d
	leaq	-1(%r11), %r10
	movq	%r11, %rax
	andq	$3, %rax
	je	.LBB16_16
# BB#17:                                # %.lr.ph.i.prol.preheader
	movl	%r9d, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_18:                              # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rcx,%rdx), %ebp
	movzbl	(%rsi,%rbp), %ebx
	movb	%bl, -32(%rdi,%rdx)
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB16_18
# BB#19:                                # %.lr.ph.i.prol.loopexit.unr-lcssa
	addl	%edx, %r9d
	cmpq	$3, %r10
	jae	.LBB16_21
	jmp	.LBB16_23
.LBB16_16:
	xorl	%edx, %edx
	cmpq	$3, %r10
	jb	.LBB16_23
.LBB16_21:                              # %.lr.ph.preheader.i.new
	subq	%rdx, %r11
	leaq	-29(%rdi,%rdx), %rcx
	movl	%r9d, %r9d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB16_22:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%r9,%rdi), %rax
	leal	1(%rax), %ebp
	movl	%eax, %ebx
	movzbl	(%rsi,%rbx), %ebx
	movb	%bl, -3(%rcx,%rdi)
	leal	2(%rax), %ebx
	movzbl	(%rsi,%rbp), %edx
	movb	%dl, -2(%rcx,%rdi)
	addl	$3, %eax
	movzbl	(%rsi,%rbx), %edx
	movb	%dl, -1(%rcx,%rdi)
	movzbl	(%rsi,%rax), %eax
	movb	%al, (%rcx,%rdi)
	addq	$4, %rdi
	cmpq	%rdi, %r11
	jne	.LBB16_22
.LBB16_23:                              # %._crit_edge.loopexit.i
	movl	(%r8), %r14d
.LBB16_24:                              # %._crit_edge.i
	xorl	%ecx, %ecx
	cmpl	$25, %r14d
	movl	$-2147467263, %eax      # imm = 0x80004001
	cmovll	%ecx, %eax
.LBB16_25:                              # %_ZN7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end16:
	.size	_ZThn176_N7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj, .Lfunc_end16-_ZThn176_N7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj
	.cfi_endproc

	.globl	_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj,@function
_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj: # @_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 48
.Lcfi92:
	.cfi_offset %rbx, -48
.Lcfi93:
	.cfi_offset %r12, -40
.Lcfi94:
	.cfi_offset %r13, -32
.Lcfi95:
	.cfi_offset %r14, -24
.Lcfi96:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	%edx, %r15d
	movq	96(%r13), %rbx
	cmpq	%r15, %rbx
	jne	.LBB17_2
# BB#1:                                 # %._ZN7CBufferIhE11SetCapacityEm.exit_crit_edge
	movq	104(%r13), %r12
	jmp	.LBB17_9
.LBB17_2:
	testl	%edx, %edx
	je	.LBB17_3
# BB#4:
	movq	%r15, %rdi
	callq	_Znam
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.LBB17_6
# BB#5:
	movq	104(%r13), %rsi
	cmpq	%r15, %rbx
	cmovaeq	%r15, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	memmove
	jmp	.LBB17_6
.LBB17_3:
	xorl	%r12d, %r12d
.LBB17_6:
	movq	104(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB17_8
# BB#7:
	callq	_ZdaPv
.LBB17_8:
	movq	%r12, 104(%r13)
	movq	%r15, 96(%r13)
.LBB17_9:                               # %_ZN7CBufferIhE11SetCapacityEm.exit
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj, .Lfunc_end17-_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZThn8_N7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj,@function
_ZThn8_N7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj: # @_ZThn8_N7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 48
.Lcfi102:
	.cfi_offset %rbx, -48
.Lcfi103:
	.cfi_offset %r12, -40
.Lcfi104:
	.cfi_offset %r13, -32
.Lcfi105:
	.cfi_offset %r14, -24
.Lcfi106:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	%edx, %r15d
	movq	88(%r13), %rbx
	cmpq	%r15, %rbx
	jne	.LBB18_2
# BB#1:                                 # %._ZN7CBufferIhE11SetCapacityEm.exit_crit_edge.i
	movq	96(%r13), %r12
	jmp	.LBB18_9
.LBB18_2:
	testl	%edx, %edx
	je	.LBB18_3
# BB#4:
	movq	%r15, %rdi
	callq	_Znam
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.LBB18_6
# BB#5:
	movq	96(%r13), %rsi
	cmpq	%r15, %rbx
	cmovaeq	%r15, %rbx
	movq	%r12, %rdi
	movq	%rbx, %rdx
	callq	memmove
	jmp	.LBB18_6
.LBB18_3:
	xorl	%r12d, %r12d
.LBB18_6:
	movq	96(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB18_8
# BB#7:
	callq	_ZdaPv
.LBB18_8:
	movq	%r12, 96(%r13)
	movq	%r15, 88(%r13)
.LBB18_9:                               # %_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj.exit
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	_ZThn8_N7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj, .Lfunc_end18-_ZThn8_N7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZN7NCrypto7NSevenZ10CBaseCoder4InitEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ10CBaseCoder4InitEv,@function
_ZN7NCrypto7NSevenZ10CBaseCoder4InitEv: # @_ZN7NCrypto7NSevenZ10CBaseCoder4InitEv
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 32
.Lcfi110:
	.cfi_offset %rbx, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	callq	_ZN7NCrypto7NSevenZ5CBase15CalculateDigestEv
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_1
.LBB19_3:
	movq	$0, (%rsp)
	movq	(%rdi), %rax
.Ltmp28:
	movq	%rsp, %rdx
	movl	$IID_ICryptoProperties, %esi
	callq	*(%rax)
	movl	%eax, %ebp
.Ltmp29:
# BB#4:                                 # %_ZNK9CMyComPtrI15ICompressFilterE14QueryInterfaceI17ICryptoPropertiesEEiRK4GUIDPPT_.exit
	testl	%ebp, %ebp
	jne	.LBB19_8
# BB#5:
	movq	(%rsp), %rdi
	movq	(%rdi), %rax
	leaq	112(%rbx), %rsi
.Ltmp30:
	movl	$32, %edx
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp31:
# BB#6:
	testl	%ebp, %ebp
	jne	.LBB19_8
# BB#7:
	movq	(%rsp), %rdi
	movq	(%rdi), %rax
	addq	$144, %rbx
.Ltmp32:
	movl	$16, %edx
	movq	%rbx, %rsi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp33:
.LBB19_8:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_10
# BB#9:
	movq	(%rdi), %rax
	callq	*16(%rax)
	jmp	.LBB19_10
.LBB19_1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*56(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB19_2
.LBB19_10:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB19_2:                               # %._crit_edge
	movq	168(%rbx), %rdi
	jmp	.LBB19_3
.LBB19_11:
.Ltmp34:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp35:
	callq	*16(%rax)
.Ltmp36:
.LBB19_13:                              # %_ZN9CMyComPtrI17ICryptoPropertiesED2Ev.exit31
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB19_14:
.Ltmp37:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end19:
	.size	_ZN7NCrypto7NSevenZ10CBaseCoder4InitEv, .Lfunc_end19-_ZN7NCrypto7NSevenZ10CBaseCoder4InitEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp28-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp33-.Ltmp28         #   Call between .Ltmp28 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin5   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp35-.Ltmp33         #   Call between .Ltmp33 and .Ltmp35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin5   #     jumps to .Ltmp37
	.byte	1                       #   On action: 1
	.long	.Ltmp36-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Lfunc_end19-.Ltmp36    #   Call between .Ltmp36 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN7NCrypto7NSevenZ10CBaseCoder6FilterEPhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ10CBaseCoder6FilterEPhj,@function
_ZN7NCrypto7NSevenZ10CBaseCoder6FilterEPhj: # @_ZN7NCrypto7NSevenZ10CBaseCoder6FilterEPhj
	.cfi_startproc
# BB#0:
	movq	168(%rdi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end20:
	.size	_ZN7NCrypto7NSevenZ10CBaseCoder6FilterEPhj, .Lfunc_end20-_ZN7NCrypto7NSevenZ10CBaseCoder6FilterEPhj
	.cfi_endproc

	.globl	_ZN7NCrypto7NSevenZ8CDecoder12CreateFilterEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CDecoder12CreateFilterEv,@function
_ZN7NCrypto7NSevenZ8CDecoder12CreateFilterEv: # @_ZN7NCrypto7NSevenZ8CDecoder12CreateFilterEv
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 32
.Lcfi115:
	.cfi_offset %rbx, -24
.Lcfi116:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$328, %edi              # imm = 0x148
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp38:
	movq	%rbx, %rdi
	callq	_ZN7NCrypto14CAesCbcDecoderC1Ev
.Ltmp39:
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	168(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB21_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB21_3:                               # %_ZN9CMyComPtrI15ICompressFilterEaSEPS0_.exit
	movq	%rbx, 168(%r14)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB21_4:
.Ltmp40:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end21:
	.size	_ZN7NCrypto7NSevenZ8CDecoder12CreateFilterEv, .Lfunc_end21-_ZN7NCrypto7NSevenZ8CDecoder12CreateFilterEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp38-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin6   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Lfunc_end21-.Ltmp39    #   Call between .Ltmp39 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7NCrypto7NSevenZ10CBaseCoderD2Ev,"axG",@progbits,_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev,comdat
	.weak	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev,@function
_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev:    # @_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi119:
	.cfi_def_cfa_offset 32
.Lcfi120:
	.cfi_offset %rbx, -24
.Lcfi121:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN7NCrypto7NSevenZ10CBaseCoderE+104, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto7NSevenZ10CBaseCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp41:
	callq	*16(%rax)
.Ltmp42:
.LBB22_2:                               # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit
	movq	$_ZTV7CBufferIhE+16, 88(%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_4
# BB#3:
	callq	_ZdaPv
.LBB22_4:                               # %_ZN7NCrypto7NSevenZ8CKeyInfoD2Ev.exit.i
	movq	$_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE+16, 32(%rbx)
	addq	$32, %rbx
.Ltmp53:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp54:
# BB#5:                                 # %_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev.exit.i
.Ltmp59:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp60:
# BB#6:                                 # %_ZN7NCrypto7NSevenZ5CBaseD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB22_9:
.Ltmp43:
	movq	%rax, %r14
	movq	$_ZTV7CBufferIhE+16, 88(%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_11
# BB#10:
	callq	_ZdaPv
.LBB22_11:                              # %_ZN7NCrypto7NSevenZ8CKeyInfoD2Ev.exit.i5
	movq	$_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE+16, 32(%rbx)
	addq	$32, %rbx
.Ltmp44:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp45:
# BB#12:                                # %_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev.exit.i6
.Ltmp50:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp51:
	jmp	.LBB22_16
.LBB22_17:
.Ltmp52:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB22_13:
.Ltmp46:
	movq	%rax, %r14
.Ltmp47:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp48:
# BB#18:                                # %.body7
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB22_14:
.Ltmp49:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_15:
.Ltmp61:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_7:
.Ltmp55:
	movq	%rax, %r14
.Ltmp56:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp57:
.LBB22_16:                              # %_ZN7NCrypto7NSevenZ5CBaseD2Ev.exit10
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_8:
.Ltmp58:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev, .Lfunc_end22-_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp41-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin7   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin7   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin7   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin7   #     jumps to .Ltmp46
	.byte	1                       #   On action: 1
	.long	.Ltmp50-.Lfunc_begin7   # >> Call Site 5 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin7   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp47-.Lfunc_begin7   # >> Call Site 6 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin7   #     jumps to .Ltmp49
	.byte	1                       #   On action: 1
	.long	.Ltmp48-.Lfunc_begin7   # >> Call Site 7 <<
	.long	.Ltmp56-.Ltmp48         #   Call between .Ltmp48 and .Ltmp56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin7   # >> Call Site 8 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin7   #     jumps to .Ltmp58
	.byte	1                       #   On action: 1
	.long	.Ltmp57-.Lfunc_begin7   # >> Call Site 9 <<
	.long	.Lfunc_end22-.Ltmp57    #   Call between .Ltmp57 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN7NCrypto7NSevenZ10CBaseCoderD0Ev,"axG",@progbits,_ZN7NCrypto7NSevenZ10CBaseCoderD0Ev,comdat
	.weak	_ZN7NCrypto7NSevenZ10CBaseCoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ10CBaseCoderD0Ev,@function
_ZN7NCrypto7NSevenZ10CBaseCoderD0Ev:    # @_ZN7NCrypto7NSevenZ10CBaseCoderD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -24
.Lcfi126:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp62:
	callq	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Ltmp63:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB23_2:
.Ltmp64:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZN7NCrypto7NSevenZ10CBaseCoderD0Ev, .Lfunc_end23-_ZN7NCrypto7NSevenZ10CBaseCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp62-.Lfunc_begin8   # >> Call Site 1 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin8   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin8   # >> Call Site 2 <<
	.long	.Lfunc_end23-.Ltmp63    #   Call between .Ltmp63 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N7NCrypto7NSevenZ10CBaseCoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD1Ev,@function
_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD1Ev: # @_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev # TAILCALL
.Lfunc_end24:
	.size	_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD1Ev, .Lfunc_end24-_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto7NSevenZ10CBaseCoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD0Ev,@function
_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD0Ev: # @_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi129:
	.cfi_def_cfa_offset 32
.Lcfi130:
	.cfi_offset %rbx, -24
.Lcfi131:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp65:
	movq	%rbx, %rdi
	callq	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Ltmp66:
# BB#1:                                 # %_ZN7NCrypto7NSevenZ10CBaseCoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB25_2:
.Ltmp67:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end25:
	.size	_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD0Ev, .Lfunc_end25-_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp65-.Lfunc_begin9   # >> Call Site 1 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin9   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin9   # >> Call Site 2 <<
	.long	.Lfunc_end25-.Ltmp66    #   Call between .Ltmp66 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi132:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB26_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB26_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB26_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB26_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB26_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB26_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB26_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB26_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB26_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB26_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB26_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB26_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB26_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB26_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB26_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB26_16
.LBB26_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICryptoSetPassword(%rip), %cl
	jne	.LBB26_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICryptoSetPassword+1(%rip), %al
	jne	.LBB26_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICryptoSetPassword+2(%rip), %al
	jne	.LBB26_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICryptoSetPassword+3(%rip), %al
	jne	.LBB26_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICryptoSetPassword+4(%rip), %al
	jne	.LBB26_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICryptoSetPassword+5(%rip), %al
	jne	.LBB26_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICryptoSetPassword+6(%rip), %al
	jne	.LBB26_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICryptoSetPassword+7(%rip), %al
	jne	.LBB26_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICryptoSetPassword+8(%rip), %al
	jne	.LBB26_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICryptoSetPassword+9(%rip), %al
	jne	.LBB26_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICryptoSetPassword+10(%rip), %al
	jne	.LBB26_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICryptoSetPassword+11(%rip), %al
	jne	.LBB26_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICryptoSetPassword+12(%rip), %al
	jne	.LBB26_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICryptoSetPassword+13(%rip), %al
	jne	.LBB26_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICryptoSetPassword+14(%rip), %al
	jne	.LBB26_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %al
	cmpb	IID_ICryptoSetPassword+15(%rip), %al
	jne	.LBB26_33
.LBB26_16:
	leaq	8(%rdi), %rax
	jmp	.LBB26_67
.LBB26_33:                              # %_ZeqRK4GUIDS1_.exit10.thread
	cmpb	IID_ICompressWriteCoderProperties(%rip), %cl
	jne	.LBB26_50
# BB#34:
	movb	1(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+1(%rip), %al
	jne	.LBB26_50
# BB#35:
	movb	2(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+2(%rip), %al
	jne	.LBB26_50
# BB#36:
	movb	3(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+3(%rip), %al
	jne	.LBB26_50
# BB#37:
	movb	4(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+4(%rip), %al
	jne	.LBB26_50
# BB#38:
	movb	5(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+5(%rip), %al
	jne	.LBB26_50
# BB#39:
	movb	6(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+6(%rip), %al
	jne	.LBB26_50
# BB#40:
	movb	7(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+7(%rip), %al
	jne	.LBB26_50
# BB#41:
	movb	8(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+8(%rip), %al
	jne	.LBB26_50
# BB#42:
	movb	9(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+9(%rip), %al
	jne	.LBB26_50
# BB#43:
	movb	10(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+10(%rip), %al
	jne	.LBB26_50
# BB#44:
	movb	11(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+11(%rip), %al
	jne	.LBB26_50
# BB#45:
	movb	12(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+12(%rip), %al
	jne	.LBB26_50
# BB#46:
	movb	13(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+13(%rip), %al
	jne	.LBB26_50
# BB#47:
	movb	14(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+14(%rip), %al
	jne	.LBB26_50
# BB#48:                                # %_ZeqRK4GUIDS1_.exit14
	movb	15(%rsi), %al
	cmpb	IID_ICompressWriteCoderProperties+15(%rip), %al
	jne	.LBB26_50
# BB#49:
	leaq	176(%rdi), %rax
	jmp	.LBB26_67
.LBB26_50:                              # %_ZeqRK4GUIDS1_.exit14.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICryptoResetInitVector(%rip), %cl
	jne	.LBB26_68
# BB#51:
	movb	1(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+1(%rip), %cl
	jne	.LBB26_68
# BB#52:
	movb	2(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+2(%rip), %cl
	jne	.LBB26_68
# BB#53:
	movb	3(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+3(%rip), %cl
	jne	.LBB26_68
# BB#54:
	movb	4(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+4(%rip), %cl
	jne	.LBB26_68
# BB#55:
	movb	5(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+5(%rip), %cl
	jne	.LBB26_68
# BB#56:
	movb	6(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+6(%rip), %cl
	jne	.LBB26_68
# BB#57:
	movb	7(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+7(%rip), %cl
	jne	.LBB26_68
# BB#58:
	movb	8(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+8(%rip), %cl
	jne	.LBB26_68
# BB#59:
	movb	9(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+9(%rip), %cl
	jne	.LBB26_68
# BB#60:
	movb	10(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+10(%rip), %cl
	jne	.LBB26_68
# BB#61:
	movb	11(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+11(%rip), %cl
	jne	.LBB26_68
# BB#62:
	movb	12(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+12(%rip), %cl
	jne	.LBB26_68
# BB#63:
	movb	13(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+13(%rip), %cl
	jne	.LBB26_68
# BB#64:
	movb	14(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+14(%rip), %cl
	jne	.LBB26_68
# BB#65:                                # %_ZeqRK4GUIDS1_.exit12
	movb	15(%rsi), %cl
	cmpb	IID_ICryptoResetInitVector+15(%rip), %cl
	jne	.LBB26_68
# BB#66:
	leaq	184(%rdi), %rax
.LBB26_67:                              # %_ZeqRK4GUIDS1_.exit12.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB26_68:                              # %_ZeqRK4GUIDS1_.exit12.thread
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end26-_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN7NCrypto7NSevenZ8CEncoder6AddRefEv,"axG",@progbits,_ZN7NCrypto7NSevenZ8CEncoder6AddRefEv,comdat
	.weak	_ZN7NCrypto7NSevenZ8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CEncoder6AddRefEv,@function
_ZN7NCrypto7NSevenZ8CEncoder6AddRefEv:  # @_ZN7NCrypto7NSevenZ8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end27:
	.size	_ZN7NCrypto7NSevenZ8CEncoder6AddRefEv, .Lfunc_end27-_ZN7NCrypto7NSevenZ8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv,"axG",@progbits,_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv,comdat
	.weak	_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv,@function
_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv: # @_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi133:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB28_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB28_2:
	popq	%rcx
	retq
.Lfunc_end28:
	.size	_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv, .Lfunc_end28-_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN7NCrypto7NSevenZ8CEncoderD2Ev,"axG",@progbits,_ZN7NCrypto7NSevenZ8CEncoderD2Ev,comdat
	.weak	_ZN7NCrypto7NSevenZ8CEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CEncoderD2Ev,@function
_ZN7NCrypto7NSevenZ8CEncoderD2Ev:       # @_ZN7NCrypto7NSevenZ8CEncoderD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev # TAILCALL
.Lfunc_end29:
	.size	_ZN7NCrypto7NSevenZ8CEncoderD2Ev, .Lfunc_end29-_ZN7NCrypto7NSevenZ8CEncoderD2Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto7NSevenZ8CEncoderD0Ev,"axG",@progbits,_ZN7NCrypto7NSevenZ8CEncoderD0Ev,comdat
	.weak	_ZN7NCrypto7NSevenZ8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CEncoderD0Ev,@function
_ZN7NCrypto7NSevenZ8CEncoderD0Ev:       # @_ZN7NCrypto7NSevenZ8CEncoderD0Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi136:
	.cfi_def_cfa_offset 32
.Lcfi137:
	.cfi_offset %rbx, -24
.Lcfi138:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp68:
	callq	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Ltmp69:
# BB#1:                                 # %_ZN7NCrypto7NSevenZ8CEncoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB30_2:
.Ltmp70:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end30:
	.size	_ZN7NCrypto7NSevenZ8CEncoderD0Ev, .Lfunc_end30-_ZN7NCrypto7NSevenZ8CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp68-.Lfunc_begin10  # >> Call Site 1 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin10  #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin10  # >> Call Site 2 <<
	.long	.Lfunc_end30-.Ltmp69    #   Call between .Ltmp69 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end31:
	.size	_ZThn8_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end31-_ZThn8_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CEncoder6AddRefEv,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CEncoder6AddRefEv,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CEncoder6AddRefEv,@function
_ZThn8_N7NCrypto7NSevenZ8CEncoder6AddRefEv: # @_ZThn8_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end32:
	.size	_ZThn8_N7NCrypto7NSevenZ8CEncoder6AddRefEv, .Lfunc_end32-_ZThn8_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CEncoder7ReleaseEv,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CEncoder7ReleaseEv,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CEncoder7ReleaseEv,@function
_ZThn8_N7NCrypto7NSevenZ8CEncoder7ReleaseEv: # @_ZThn8_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi139:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB33_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB33_2:                               # %_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end33:
	.size	_ZThn8_N7NCrypto7NSevenZ8CEncoder7ReleaseEv, .Lfunc_end33-_ZThn8_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CEncoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CEncoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CEncoderD1Ev,@function
_ZThn8_N7NCrypto7NSevenZ8CEncoderD1Ev:  # @_ZThn8_N7NCrypto7NSevenZ8CEncoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev # TAILCALL
.Lfunc_end34:
	.size	_ZThn8_N7NCrypto7NSevenZ8CEncoderD1Ev, .Lfunc_end34-_ZThn8_N7NCrypto7NSevenZ8CEncoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CEncoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CEncoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CEncoderD0Ev,@function
_ZThn8_N7NCrypto7NSevenZ8CEncoderD0Ev:  # @_ZThn8_N7NCrypto7NSevenZ8CEncoderD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi142:
	.cfi_def_cfa_offset 32
.Lcfi143:
	.cfi_offset %rbx, -24
.Lcfi144:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp71:
	movq	%rbx, %rdi
	callq	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Ltmp72:
# BB#1:                                 # %_ZN7NCrypto7NSevenZ8CEncoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB35_2:
.Ltmp73:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end35:
	.size	_ZThn8_N7NCrypto7NSevenZ8CEncoderD0Ev, .Lfunc_end35-_ZThn8_N7NCrypto7NSevenZ8CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp71-.Lfunc_begin11  # >> Call Site 1 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin11  #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin11  # >> Call Site 2 <<
	.long	.Lfunc_end35-.Ltmp72    #   Call between .Ltmp72 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn176_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn176_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-176, %rdi
	jmp	_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end36:
	.size	_ZThn176_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end36-_ZThn176_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CEncoder6AddRefEv,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CEncoder6AddRefEv,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CEncoder6AddRefEv,@function
_ZThn176_N7NCrypto7NSevenZ8CEncoder6AddRefEv: # @_ZThn176_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	-160(%rdi), %eax
	incl	%eax
	movl	%eax, -160(%rdi)
	retq
.Lfunc_end37:
	.size	_ZThn176_N7NCrypto7NSevenZ8CEncoder6AddRefEv, .Lfunc_end37-_ZThn176_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CEncoder7ReleaseEv,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CEncoder7ReleaseEv,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CEncoder7ReleaseEv,@function
_ZThn176_N7NCrypto7NSevenZ8CEncoder7ReleaseEv: # @_ZThn176_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 16
	movl	-160(%rdi), %eax
	decl	%eax
	movl	%eax, -160(%rdi)
	jne	.LBB38_2
# BB#1:
	addq	$-176, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB38_2:                               # %_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end38:
	.size	_ZThn176_N7NCrypto7NSevenZ8CEncoder7ReleaseEv, .Lfunc_end38-_ZThn176_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CEncoderD1Ev,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CEncoderD1Ev,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CEncoderD1Ev,@function
_ZThn176_N7NCrypto7NSevenZ8CEncoderD1Ev: # @_ZThn176_N7NCrypto7NSevenZ8CEncoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-176, %rdi
	jmp	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev # TAILCALL
.Lfunc_end39:
	.size	_ZThn176_N7NCrypto7NSevenZ8CEncoderD1Ev, .Lfunc_end39-_ZThn176_N7NCrypto7NSevenZ8CEncoderD1Ev
	.cfi_endproc

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CEncoderD0Ev,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CEncoderD0Ev,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CEncoderD0Ev,@function
_ZThn176_N7NCrypto7NSevenZ8CEncoderD0Ev: # @_ZThn176_N7NCrypto7NSevenZ8CEncoderD0Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi148:
	.cfi_def_cfa_offset 32
.Lcfi149:
	.cfi_offset %rbx, -24
.Lcfi150:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-176, %rbx
.Ltmp74:
	movq	%rbx, %rdi
	callq	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Ltmp75:
# BB#1:                                 # %_ZN7NCrypto7NSevenZ8CEncoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB40_2:
.Ltmp76:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end40:
	.size	_ZThn176_N7NCrypto7NSevenZ8CEncoderD0Ev, .Lfunc_end40-_ZThn176_N7NCrypto7NSevenZ8CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp74-.Lfunc_begin12  # >> Call Site 1 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin12  #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin12  # >> Call Site 2 <<
	.long	.Lfunc_end40-.Ltmp75    #   Call between .Ltmp75 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn184_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn184_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn184_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn184_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn184_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn184_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-184, %rdi
	jmp	_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end41:
	.size	_ZThn184_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end41-_ZThn184_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn184_N7NCrypto7NSevenZ8CEncoder6AddRefEv,"axG",@progbits,_ZThn184_N7NCrypto7NSevenZ8CEncoder6AddRefEv,comdat
	.weak	_ZThn184_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn184_N7NCrypto7NSevenZ8CEncoder6AddRefEv,@function
_ZThn184_N7NCrypto7NSevenZ8CEncoder6AddRefEv: # @_ZThn184_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	-168(%rdi), %eax
	incl	%eax
	movl	%eax, -168(%rdi)
	retq
.Lfunc_end42:
	.size	_ZThn184_N7NCrypto7NSevenZ8CEncoder6AddRefEv, .Lfunc_end42-_ZThn184_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn184_N7NCrypto7NSevenZ8CEncoder7ReleaseEv,"axG",@progbits,_ZThn184_N7NCrypto7NSevenZ8CEncoder7ReleaseEv,comdat
	.weak	_ZThn184_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn184_N7NCrypto7NSevenZ8CEncoder7ReleaseEv,@function
_ZThn184_N7NCrypto7NSevenZ8CEncoder7ReleaseEv: # @_ZThn184_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi151:
	.cfi_def_cfa_offset 16
	movl	-168(%rdi), %eax
	decl	%eax
	movl	%eax, -168(%rdi)
	jne	.LBB43_2
# BB#1:
	addq	$-184, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB43_2:                               # %_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end43:
	.size	_ZThn184_N7NCrypto7NSevenZ8CEncoder7ReleaseEv, .Lfunc_end43-_ZThn184_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn184_N7NCrypto7NSevenZ8CEncoderD1Ev,"axG",@progbits,_ZThn184_N7NCrypto7NSevenZ8CEncoderD1Ev,comdat
	.weak	_ZThn184_N7NCrypto7NSevenZ8CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn184_N7NCrypto7NSevenZ8CEncoderD1Ev,@function
_ZThn184_N7NCrypto7NSevenZ8CEncoderD1Ev: # @_ZThn184_N7NCrypto7NSevenZ8CEncoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-184, %rdi
	jmp	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev # TAILCALL
.Lfunc_end44:
	.size	_ZThn184_N7NCrypto7NSevenZ8CEncoderD1Ev, .Lfunc_end44-_ZThn184_N7NCrypto7NSevenZ8CEncoderD1Ev
	.cfi_endproc

	.section	.text._ZThn184_N7NCrypto7NSevenZ8CEncoderD0Ev,"axG",@progbits,_ZThn184_N7NCrypto7NSevenZ8CEncoderD0Ev,comdat
	.weak	_ZThn184_N7NCrypto7NSevenZ8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn184_N7NCrypto7NSevenZ8CEncoderD0Ev,@function
_ZThn184_N7NCrypto7NSevenZ8CEncoderD0Ev: # @_ZThn184_N7NCrypto7NSevenZ8CEncoderD0Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi154:
	.cfi_def_cfa_offset 32
.Lcfi155:
	.cfi_offset %rbx, -24
.Lcfi156:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-184, %rbx
.Ltmp77:
	movq	%rbx, %rdi
	callq	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Ltmp78:
# BB#1:                                 # %_ZN7NCrypto7NSevenZ8CEncoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB45_2:
.Ltmp79:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end45:
	.size	_ZThn184_N7NCrypto7NSevenZ8CEncoderD0Ev, .Lfunc_end45-_ZThn184_N7NCrypto7NSevenZ8CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp77-.Lfunc_begin13  # >> Call Site 1 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin13  #     jumps to .Ltmp79
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin13  # >> Call Site 2 <<
	.long	.Lfunc_end45-.Ltmp78    #   Call between .Ltmp78 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi157:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB46_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB46_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB46_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB46_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB46_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB46_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB46_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB46_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB46_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB46_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB46_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB46_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB46_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB46_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB46_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB46_16
.LBB46_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICryptoSetPassword(%rip), %cl
	jne	.LBB46_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICryptoSetPassword+1(%rip), %al
	jne	.LBB46_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICryptoSetPassword+2(%rip), %al
	jne	.LBB46_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICryptoSetPassword+3(%rip), %al
	jne	.LBB46_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICryptoSetPassword+4(%rip), %al
	jne	.LBB46_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICryptoSetPassword+5(%rip), %al
	jne	.LBB46_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICryptoSetPassword+6(%rip), %al
	jne	.LBB46_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICryptoSetPassword+7(%rip), %al
	jne	.LBB46_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICryptoSetPassword+8(%rip), %al
	jne	.LBB46_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICryptoSetPassword+9(%rip), %al
	jne	.LBB46_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICryptoSetPassword+10(%rip), %al
	jne	.LBB46_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICryptoSetPassword+11(%rip), %al
	jne	.LBB46_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICryptoSetPassword+12(%rip), %al
	jne	.LBB46_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICryptoSetPassword+13(%rip), %al
	jne	.LBB46_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICryptoSetPassword+14(%rip), %al
	jne	.LBB46_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ICryptoSetPassword+15(%rip), %al
	jne	.LBB46_33
.LBB46_16:
	leaq	8(%rdi), %rax
	jmp	.LBB46_50
.LBB46_33:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetDecoderProperties2(%rip), %cl
	jne	.LBB46_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+1(%rip), %cl
	jne	.LBB46_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+2(%rip), %cl
	jne	.LBB46_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+3(%rip), %cl
	jne	.LBB46_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+4(%rip), %cl
	jne	.LBB46_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+5(%rip), %cl
	jne	.LBB46_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+6(%rip), %cl
	jne	.LBB46_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+7(%rip), %cl
	jne	.LBB46_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+8(%rip), %cl
	jne	.LBB46_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+9(%rip), %cl
	jne	.LBB46_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+10(%rip), %cl
	jne	.LBB46_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+11(%rip), %cl
	jne	.LBB46_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+12(%rip), %cl
	jne	.LBB46_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+13(%rip), %cl
	jne	.LBB46_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+14(%rip), %cl
	jne	.LBB46_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetDecoderProperties2+15(%rip), %cl
	jne	.LBB46_51
# BB#49:
	leaq	176(%rdi), %rax
.LBB46_50:                              # %_ZeqRK4GUIDS1_.exit10.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB46_51:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end46:
	.size	_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end46-_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN7NCrypto7NSevenZ8CDecoder6AddRefEv,"axG",@progbits,_ZN7NCrypto7NSevenZ8CDecoder6AddRefEv,comdat
	.weak	_ZN7NCrypto7NSevenZ8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CDecoder6AddRefEv,@function
_ZN7NCrypto7NSevenZ8CDecoder6AddRefEv:  # @_ZN7NCrypto7NSevenZ8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end47:
	.size	_ZN7NCrypto7NSevenZ8CDecoder6AddRefEv, .Lfunc_end47-_ZN7NCrypto7NSevenZ8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv,"axG",@progbits,_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv,comdat
	.weak	_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv,@function
_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv: # @_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB48_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB48_2:
	popq	%rcx
	retq
.Lfunc_end48:
	.size	_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv, .Lfunc_end48-_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN7NCrypto7NSevenZ8CDecoderD2Ev,"axG",@progbits,_ZN7NCrypto7NSevenZ8CDecoderD2Ev,comdat
	.weak	_ZN7NCrypto7NSevenZ8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CDecoderD2Ev,@function
_ZN7NCrypto7NSevenZ8CDecoderD2Ev:       # @_ZN7NCrypto7NSevenZ8CDecoderD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev # TAILCALL
.Lfunc_end49:
	.size	_ZN7NCrypto7NSevenZ8CDecoderD2Ev, .Lfunc_end49-_ZN7NCrypto7NSevenZ8CDecoderD2Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto7NSevenZ8CDecoderD0Ev,"axG",@progbits,_ZN7NCrypto7NSevenZ8CDecoderD0Ev,comdat
	.weak	_ZN7NCrypto7NSevenZ8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto7NSevenZ8CDecoderD0Ev,@function
_ZN7NCrypto7NSevenZ8CDecoderD0Ev:       # @_ZN7NCrypto7NSevenZ8CDecoderD0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp80:
	callq	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Ltmp81:
# BB#1:                                 # %_ZN7NCrypto7NSevenZ8CDecoderD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB50_2:
.Ltmp82:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end50:
	.size	_ZN7NCrypto7NSevenZ8CDecoderD0Ev, .Lfunc_end50-_ZN7NCrypto7NSevenZ8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table50:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp80-.Lfunc_begin14  # >> Call Site 1 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin14  #     jumps to .Ltmp82
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin14  # >> Call Site 2 <<
	.long	.Lfunc_end50-.Ltmp81    #   Call between .Ltmp81 and .Lfunc_end50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end51:
	.size	_ZThn8_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end51-_ZThn8_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CDecoder6AddRefEv,@function
_ZThn8_N7NCrypto7NSevenZ8CDecoder6AddRefEv: # @_ZThn8_N7NCrypto7NSevenZ8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end52:
	.size	_ZThn8_N7NCrypto7NSevenZ8CDecoder6AddRefEv, .Lfunc_end52-_ZThn8_N7NCrypto7NSevenZ8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CDecoder7ReleaseEv,@function
_ZThn8_N7NCrypto7NSevenZ8CDecoder7ReleaseEv: # @_ZThn8_N7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi164:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB53_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB53_2:                               # %_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end53:
	.size	_ZThn8_N7NCrypto7NSevenZ8CDecoder7ReleaseEv, .Lfunc_end53-_ZThn8_N7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CDecoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CDecoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CDecoderD1Ev,@function
_ZThn8_N7NCrypto7NSevenZ8CDecoderD1Ev:  # @_ZThn8_N7NCrypto7NSevenZ8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev # TAILCALL
.Lfunc_end54:
	.size	_ZThn8_N7NCrypto7NSevenZ8CDecoderD1Ev, .Lfunc_end54-_ZThn8_N7NCrypto7NSevenZ8CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto7NSevenZ8CDecoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto7NSevenZ8CDecoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto7NSevenZ8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto7NSevenZ8CDecoderD0Ev,@function
_ZThn8_N7NCrypto7NSevenZ8CDecoderD0Ev:  # @_ZThn8_N7NCrypto7NSevenZ8CDecoderD0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 32
.Lcfi168:
	.cfi_offset %rbx, -24
.Lcfi169:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp83:
	movq	%rbx, %rdi
	callq	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Ltmp84:
# BB#1:                                 # %_ZN7NCrypto7NSevenZ8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB55_2:
.Ltmp85:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end55:
	.size	_ZThn8_N7NCrypto7NSevenZ8CDecoderD0Ev, .Lfunc_end55-_ZThn8_N7NCrypto7NSevenZ8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table55:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp83-.Lfunc_begin15  # >> Call Site 1 <<
	.long	.Ltmp84-.Ltmp83         #   Call between .Ltmp83 and .Ltmp84
	.long	.Ltmp85-.Lfunc_begin15  #     jumps to .Ltmp85
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin15  # >> Call Site 2 <<
	.long	.Lfunc_end55-.Ltmp84    #   Call between .Ltmp84 and .Lfunc_end55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn176_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn176_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-176, %rdi
	jmp	_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end56:
	.size	_ZThn176_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end56-_ZThn176_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CDecoder6AddRefEv,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CDecoder6AddRefEv,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CDecoder6AddRefEv,@function
_ZThn176_N7NCrypto7NSevenZ8CDecoder6AddRefEv: # @_ZThn176_N7NCrypto7NSevenZ8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	-160(%rdi), %eax
	incl	%eax
	movl	%eax, -160(%rdi)
	retq
.Lfunc_end57:
	.size	_ZThn176_N7NCrypto7NSevenZ8CDecoder6AddRefEv, .Lfunc_end57-_ZThn176_N7NCrypto7NSevenZ8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CDecoder7ReleaseEv,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CDecoder7ReleaseEv,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CDecoder7ReleaseEv,@function
_ZThn176_N7NCrypto7NSevenZ8CDecoder7ReleaseEv: # @_ZThn176_N7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi170:
	.cfi_def_cfa_offset 16
	movl	-160(%rdi), %eax
	decl	%eax
	movl	%eax, -160(%rdi)
	jne	.LBB58_2
# BB#1:
	addq	$-176, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB58_2:                               # %_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end58:
	.size	_ZThn176_N7NCrypto7NSevenZ8CDecoder7ReleaseEv, .Lfunc_end58-_ZThn176_N7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CDecoderD1Ev,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CDecoderD1Ev,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CDecoderD1Ev,@function
_ZThn176_N7NCrypto7NSevenZ8CDecoderD1Ev: # @_ZThn176_N7NCrypto7NSevenZ8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-176, %rdi
	jmp	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev # TAILCALL
.Lfunc_end59:
	.size	_ZThn176_N7NCrypto7NSevenZ8CDecoderD1Ev, .Lfunc_end59-_ZThn176_N7NCrypto7NSevenZ8CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn176_N7NCrypto7NSevenZ8CDecoderD0Ev,"axG",@progbits,_ZThn176_N7NCrypto7NSevenZ8CDecoderD0Ev,comdat
	.weak	_ZThn176_N7NCrypto7NSevenZ8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn176_N7NCrypto7NSevenZ8CDecoderD0Ev,@function
_ZThn176_N7NCrypto7NSevenZ8CDecoderD0Ev: # @_ZThn176_N7NCrypto7NSevenZ8CDecoderD0Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi173:
	.cfi_def_cfa_offset 32
.Lcfi174:
	.cfi_offset %rbx, -24
.Lcfi175:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-176, %rbx
.Ltmp86:
	movq	%rbx, %rdi
	callq	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
.Ltmp87:
# BB#1:                                 # %_ZN7NCrypto7NSevenZ8CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB60_2:
.Ltmp88:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end60:
	.size	_ZThn176_N7NCrypto7NSevenZ8CDecoderD0Ev, .Lfunc_end60-_ZThn176_N7NCrypto7NSevenZ8CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table60:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp86-.Lfunc_begin16  # >> Call Site 1 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin16  #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin16  # >> Call Site 2 <<
	.long	.Lfunc_end60-.Ltmp87    #   Call between .Ltmp87 and .Lfunc_end60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev,@function
_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev: # @_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi176:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi177:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi178:
	.cfi_def_cfa_offset 32
.Lcfi179:
	.cfi_offset %rbx, -24
.Lcfi180:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE+16, (%rbx)
.Ltmp89:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp90:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB61_2:
.Ltmp91:
	movq	%rax, %r14
.Ltmp92:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp93:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB61_4:
.Ltmp94:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end61:
	.size	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev, .Lfunc_end61-_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table61:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp89-.Lfunc_begin17  # >> Call Site 1 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin17  #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin17  # >> Call Site 2 <<
	.long	.Ltmp92-.Ltmp90         #   Call between .Ltmp90 and .Ltmp92
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin17  # >> Call Site 3 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin17  #     jumps to .Ltmp94
	.byte	1                       #   On action: 1
	.long	.Ltmp93-.Lfunc_begin17  # >> Call Site 4 <<
	.long	.Lfunc_end61-.Ltmp93    #   Call between .Ltmp93 and .Lfunc_end61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED0Ev,@function
_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED0Ev: # @_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi181:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi182:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi183:
	.cfi_def_cfa_offset 32
.Lcfi184:
	.cfi_offset %rbx, -24
.Lcfi185:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE+16, (%rbx)
.Ltmp95:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp96:
# BB#1:
.Ltmp101:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp102:
# BB#2:                                 # %_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB62_5:
.Ltmp103:
	movq	%rax, %r14
	jmp	.LBB62_6
.LBB62_3:
.Ltmp97:
	movq	%rax, %r14
.Ltmp98:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp99:
.LBB62_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB62_4:
.Ltmp100:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end62:
	.size	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED0Ev, .Lfunc_end62-_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table62:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp95-.Lfunc_begin18  # >> Call Site 1 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin18  #     jumps to .Ltmp97
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin18 #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin18  # >> Call Site 3 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin18 #     jumps to .Ltmp100
	.byte	1                       #   On action: 1
	.long	.Ltmp99-.Lfunc_begin18  # >> Call Site 4 <<
	.long	.Lfunc_end62-.Ltmp99    #   Call between .Ltmp99 and .Lfunc_end62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB63_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB63_1:
	retq
.Lfunc_end63:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end63-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi186:
	.cfi_def_cfa_offset 16
.Lcfi187:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB64_2
# BB#1:
	callq	_ZdaPv
.LBB64_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end64:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end64-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_7zAes.ii,@function
_GLOBAL__sub_I_7zAes.ii:                # @_GLOBAL__sub_I_7zAes.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi188:
	.cfi_def_cfa_offset 16
	movl	$32, _ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, _ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE+16(%rip)
	movq	$8, _ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE+32(%rip)
	movq	$_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE+16, _ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE+8(%rip)
	movl	$_ZN7NCrypto7NSevenZ13CKeyInfoCacheD2Ev, %edi
	movl	$_ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	movl	$_ZN7NCrypto7NSevenZL31g_GlobalKeyCacheCriticalSectionE, %edi
	callq	CriticalSection_Init
	movl	$_ZN8NWindows16NSynchronization16CCriticalSectionD2Ev, %edi
	movl	$_ZN7NCrypto7NSevenZL31g_GlobalKeyCacheCriticalSectionE, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end65:
	.size	_GLOBAL__sub_I_7zAes.ii, .Lfunc_end65-_GLOBAL__sub_I_7zAes.ii
	.cfi_endproc

	.type	_ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE,@object # @_ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE
	.local	_ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE
	.comm	_ZN7NCrypto7NSevenZL16g_GlobalKeyCacheE,40,8
	.type	_ZN7NCrypto7NSevenZL31g_GlobalKeyCacheCriticalSectionE,@object # @_ZN7NCrypto7NSevenZL31g_GlobalKeyCacheCriticalSectionE
	.local	_ZN7NCrypto7NSevenZL31g_GlobalKeyCacheCriticalSectionE
	.comm	_ZN7NCrypto7NSevenZL31g_GlobalKeyCacheCriticalSectionE,40,8
	.type	_ZTVN7NCrypto7NSevenZ10CBaseCoderE,@object # @_ZTVN7NCrypto7NSevenZ10CBaseCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN7NCrypto7NSevenZ10CBaseCoderE
	.p2align	3
_ZTVN7NCrypto7NSevenZ10CBaseCoderE:
	.quad	0
	.quad	_ZTIN7NCrypto7NSevenZ10CBaseCoderE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoderD2Ev
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoderD0Ev
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoder4InitEv
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoder6FilterEPhj
	.quad	__cxa_pure_virtual
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto7NSevenZ10CBaseCoderE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD1Ev
	.quad	_ZThn8_N7NCrypto7NSevenZ10CBaseCoderD0Ev
	.quad	_ZThn8_N7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.size	_ZTVN7NCrypto7NSevenZ10CBaseCoderE, 152

	.type	_ZTSN7NCrypto7NSevenZ10CBaseCoderE,@object # @_ZTSN7NCrypto7NSevenZ10CBaseCoderE
	.globl	_ZTSN7NCrypto7NSevenZ10CBaseCoderE
	.p2align	4
_ZTSN7NCrypto7NSevenZ10CBaseCoderE:
	.asciz	"N7NCrypto7NSevenZ10CBaseCoderE"
	.size	_ZTSN7NCrypto7NSevenZ10CBaseCoderE, 31

	.type	_ZTS15ICompressFilter,@object # @_ZTS15ICompressFilter
	.section	.rodata._ZTS15ICompressFilter,"aG",@progbits,_ZTS15ICompressFilter,comdat
	.weak	_ZTS15ICompressFilter
	.p2align	4
_ZTS15ICompressFilter:
	.asciz	"15ICompressFilter"
	.size	_ZTS15ICompressFilter, 18

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI15ICompressFilter,@object # @_ZTI15ICompressFilter
	.section	.rodata._ZTI15ICompressFilter,"aG",@progbits,_ZTI15ICompressFilter,comdat
	.weak	_ZTI15ICompressFilter
	.p2align	4
_ZTI15ICompressFilter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15ICompressFilter
	.quad	_ZTI8IUnknown
	.size	_ZTI15ICompressFilter, 24

	.type	_ZTS18ICryptoSetPassword,@object # @_ZTS18ICryptoSetPassword
	.section	.rodata._ZTS18ICryptoSetPassword,"aG",@progbits,_ZTS18ICryptoSetPassword,comdat
	.weak	_ZTS18ICryptoSetPassword
	.p2align	4
_ZTS18ICryptoSetPassword:
	.asciz	"18ICryptoSetPassword"
	.size	_ZTS18ICryptoSetPassword, 21

	.type	_ZTI18ICryptoSetPassword,@object # @_ZTI18ICryptoSetPassword
	.section	.rodata._ZTI18ICryptoSetPassword,"aG",@progbits,_ZTI18ICryptoSetPassword,comdat
	.weak	_ZTI18ICryptoSetPassword
	.p2align	4
_ZTI18ICryptoSetPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18ICryptoSetPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI18ICryptoSetPassword, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTSN7NCrypto7NSevenZ5CBaseE,@object # @_ZTSN7NCrypto7NSevenZ5CBaseE
	.section	.rodata._ZTSN7NCrypto7NSevenZ5CBaseE,"aG",@progbits,_ZTSN7NCrypto7NSevenZ5CBaseE,comdat
	.weak	_ZTSN7NCrypto7NSevenZ5CBaseE
	.p2align	4
_ZTSN7NCrypto7NSevenZ5CBaseE:
	.asciz	"N7NCrypto7NSevenZ5CBaseE"
	.size	_ZTSN7NCrypto7NSevenZ5CBaseE, 25

	.type	_ZTIN7NCrypto7NSevenZ5CBaseE,@object # @_ZTIN7NCrypto7NSevenZ5CBaseE
	.section	.rodata._ZTIN7NCrypto7NSevenZ5CBaseE,"aG",@progbits,_ZTIN7NCrypto7NSevenZ5CBaseE,comdat
	.weak	_ZTIN7NCrypto7NSevenZ5CBaseE
	.p2align	3
_ZTIN7NCrypto7NSevenZ5CBaseE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN7NCrypto7NSevenZ5CBaseE
	.size	_ZTIN7NCrypto7NSevenZ5CBaseE, 16

	.type	_ZTIN7NCrypto7NSevenZ10CBaseCoderE,@object # @_ZTIN7NCrypto7NSevenZ10CBaseCoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN7NCrypto7NSevenZ10CBaseCoderE
	.p2align	4
_ZTIN7NCrypto7NSevenZ10CBaseCoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN7NCrypto7NSevenZ10CBaseCoderE
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI15ICompressFilter
	.quad	2                       # 0x2
	.quad	_ZTI18ICryptoSetPassword
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.quad	_ZTIN7NCrypto7NSevenZ5CBaseE
	.quad	6146                    # 0x1802
	.size	_ZTIN7NCrypto7NSevenZ10CBaseCoderE, 88

	.type	_ZTVN7NCrypto7NSevenZ8CEncoderE,@object # @_ZTVN7NCrypto7NSevenZ8CEncoderE
	.globl	_ZTVN7NCrypto7NSevenZ8CEncoderE
	.p2align	3
_ZTVN7NCrypto7NSevenZ8CEncoderE:
	.quad	0
	.quad	_ZTIN7NCrypto7NSevenZ8CEncoderE
	.quad	_ZN7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN7NCrypto7NSevenZ8CEncoder6AddRefEv
	.quad	_ZN7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.quad	_ZN7NCrypto7NSevenZ8CEncoderD2Ev
	.quad	_ZN7NCrypto7NSevenZ8CEncoderD0Ev
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoder4InitEv
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoder6FilterEPhj
	.quad	_ZN7NCrypto7NSevenZ8CEncoder12CreateFilterEv
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	_ZN7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.quad	_ZN7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv
	.quad	-8
	.quad	_ZTIN7NCrypto7NSevenZ8CEncoderE
	.quad	_ZThn8_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.quad	_ZThn8_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.quad	_ZThn8_N7NCrypto7NSevenZ8CEncoderD1Ev
	.quad	_ZThn8_N7NCrypto7NSevenZ8CEncoderD0Ev
	.quad	_ZThn8_N7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	-176
	.quad	_ZTIN7NCrypto7NSevenZ8CEncoderE
	.quad	_ZThn176_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn176_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.quad	_ZThn176_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.quad	_ZThn176_N7NCrypto7NSevenZ8CEncoderD1Ev
	.quad	_ZThn176_N7NCrypto7NSevenZ8CEncoderD0Ev
	.quad	_ZThn176_N7NCrypto7NSevenZ8CEncoder20WriteCoderPropertiesEP20ISequentialOutStream
	.quad	-184
	.quad	_ZTIN7NCrypto7NSevenZ8CEncoderE
	.quad	_ZThn184_N7NCrypto7NSevenZ8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn184_N7NCrypto7NSevenZ8CEncoder6AddRefEv
	.quad	_ZThn184_N7NCrypto7NSevenZ8CEncoder7ReleaseEv
	.quad	_ZThn184_N7NCrypto7NSevenZ8CEncoderD1Ev
	.quad	_ZThn184_N7NCrypto7NSevenZ8CEncoderD0Ev
	.quad	_ZThn184_N7NCrypto7NSevenZ8CEncoder15ResetInitVectorEv
	.size	_ZTVN7NCrypto7NSevenZ8CEncoderE, 296

	.type	_ZTSN7NCrypto7NSevenZ8CEncoderE,@object # @_ZTSN7NCrypto7NSevenZ8CEncoderE
	.globl	_ZTSN7NCrypto7NSevenZ8CEncoderE
	.p2align	4
_ZTSN7NCrypto7NSevenZ8CEncoderE:
	.asciz	"N7NCrypto7NSevenZ8CEncoderE"
	.size	_ZTSN7NCrypto7NSevenZ8CEncoderE, 28

	.type	_ZTS29ICompressWriteCoderProperties,@object # @_ZTS29ICompressWriteCoderProperties
	.section	.rodata._ZTS29ICompressWriteCoderProperties,"aG",@progbits,_ZTS29ICompressWriteCoderProperties,comdat
	.weak	_ZTS29ICompressWriteCoderProperties
	.p2align	4
_ZTS29ICompressWriteCoderProperties:
	.asciz	"29ICompressWriteCoderProperties"
	.size	_ZTS29ICompressWriteCoderProperties, 32

	.type	_ZTI29ICompressWriteCoderProperties,@object # @_ZTI29ICompressWriteCoderProperties
	.section	.rodata._ZTI29ICompressWriteCoderProperties,"aG",@progbits,_ZTI29ICompressWriteCoderProperties,comdat
	.weak	_ZTI29ICompressWriteCoderProperties
	.p2align	4
_ZTI29ICompressWriteCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS29ICompressWriteCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI29ICompressWriteCoderProperties, 24

	.type	_ZTS22ICryptoResetInitVector,@object # @_ZTS22ICryptoResetInitVector
	.section	.rodata._ZTS22ICryptoResetInitVector,"aG",@progbits,_ZTS22ICryptoResetInitVector,comdat
	.weak	_ZTS22ICryptoResetInitVector
	.p2align	4
_ZTS22ICryptoResetInitVector:
	.asciz	"22ICryptoResetInitVector"
	.size	_ZTS22ICryptoResetInitVector, 25

	.type	_ZTI22ICryptoResetInitVector,@object # @_ZTI22ICryptoResetInitVector
	.section	.rodata._ZTI22ICryptoResetInitVector,"aG",@progbits,_ZTI22ICryptoResetInitVector,comdat
	.weak	_ZTI22ICryptoResetInitVector
	.p2align	4
_ZTI22ICryptoResetInitVector:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22ICryptoResetInitVector
	.quad	_ZTI8IUnknown
	.size	_ZTI22ICryptoResetInitVector, 24

	.type	_ZTIN7NCrypto7NSevenZ8CEncoderE,@object # @_ZTIN7NCrypto7NSevenZ8CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN7NCrypto7NSevenZ8CEncoderE
	.p2align	4
_ZTIN7NCrypto7NSevenZ8CEncoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN7NCrypto7NSevenZ8CEncoderE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTIN7NCrypto7NSevenZ10CBaseCoderE
	.quad	2                       # 0x2
	.quad	_ZTI29ICompressWriteCoderProperties
	.quad	45058                   # 0xb002
	.quad	_ZTI22ICryptoResetInitVector
	.quad	47106                   # 0xb802
	.size	_ZTIN7NCrypto7NSevenZ8CEncoderE, 72

	.type	_ZTVN7NCrypto7NSevenZ8CDecoderE,@object # @_ZTVN7NCrypto7NSevenZ8CDecoderE
	.globl	_ZTVN7NCrypto7NSevenZ8CDecoderE
	.p2align	3
_ZTVN7NCrypto7NSevenZ8CDecoderE:
	.quad	0
	.quad	_ZTIN7NCrypto7NSevenZ8CDecoderE
	.quad	_ZN7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN7NCrypto7NSevenZ8CDecoder6AddRefEv
	.quad	_ZN7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.quad	_ZN7NCrypto7NSevenZ8CDecoderD2Ev
	.quad	_ZN7NCrypto7NSevenZ8CDecoderD0Ev
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoder4InitEv
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoder6FilterEPhj
	.quad	_ZN7NCrypto7NSevenZ8CDecoder12CreateFilterEv
	.quad	_ZN7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	_ZN7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto7NSevenZ8CDecoderE
	.quad	_ZThn8_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N7NCrypto7NSevenZ8CDecoder6AddRefEv
	.quad	_ZThn8_N7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.quad	_ZThn8_N7NCrypto7NSevenZ8CDecoderD1Ev
	.quad	_ZThn8_N7NCrypto7NSevenZ8CDecoderD0Ev
	.quad	_ZThn8_N7NCrypto7NSevenZ10CBaseCoder17CryptoSetPasswordEPKhj
	.quad	-176
	.quad	_ZTIN7NCrypto7NSevenZ8CDecoderE
	.quad	_ZThn176_N7NCrypto7NSevenZ8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn176_N7NCrypto7NSevenZ8CDecoder6AddRefEv
	.quad	_ZThn176_N7NCrypto7NSevenZ8CDecoder7ReleaseEv
	.quad	_ZThn176_N7NCrypto7NSevenZ8CDecoderD1Ev
	.quad	_ZThn176_N7NCrypto7NSevenZ8CDecoderD0Ev
	.quad	_ZThn176_N7NCrypto7NSevenZ8CDecoder21SetDecoderProperties2EPKhj
	.size	_ZTVN7NCrypto7NSevenZ8CDecoderE, 224

	.type	_ZTSN7NCrypto7NSevenZ8CDecoderE,@object # @_ZTSN7NCrypto7NSevenZ8CDecoderE
	.globl	_ZTSN7NCrypto7NSevenZ8CDecoderE
	.p2align	4
_ZTSN7NCrypto7NSevenZ8CDecoderE:
	.asciz	"N7NCrypto7NSevenZ8CDecoderE"
	.size	_ZTSN7NCrypto7NSevenZ8CDecoderE, 28

	.type	_ZTS30ICompressSetDecoderProperties2,@object # @_ZTS30ICompressSetDecoderProperties2
	.section	.rodata._ZTS30ICompressSetDecoderProperties2,"aG",@progbits,_ZTS30ICompressSetDecoderProperties2,comdat
	.weak	_ZTS30ICompressSetDecoderProperties2
	.p2align	4
_ZTS30ICompressSetDecoderProperties2:
	.asciz	"30ICompressSetDecoderProperties2"
	.size	_ZTS30ICompressSetDecoderProperties2, 33

	.type	_ZTI30ICompressSetDecoderProperties2,@object # @_ZTI30ICompressSetDecoderProperties2
	.section	.rodata._ZTI30ICompressSetDecoderProperties2,"aG",@progbits,_ZTI30ICompressSetDecoderProperties2,comdat
	.weak	_ZTI30ICompressSetDecoderProperties2
	.p2align	4
_ZTI30ICompressSetDecoderProperties2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS30ICompressSetDecoderProperties2
	.quad	_ZTI8IUnknown
	.size	_ZTI30ICompressSetDecoderProperties2, 24

	.type	_ZTIN7NCrypto7NSevenZ8CDecoderE,@object # @_ZTIN7NCrypto7NSevenZ8CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN7NCrypto7NSevenZ8CDecoderE
	.p2align	4
_ZTIN7NCrypto7NSevenZ8CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN7NCrypto7NSevenZ8CDecoderE
	.long	1                       # 0x1
	.long	2                       # 0x2
	.quad	_ZTIN7NCrypto7NSevenZ10CBaseCoderE
	.quad	2                       # 0x2
	.quad	_ZTI30ICompressSetDecoderProperties2
	.quad	45058                   # 0xb002
	.size	_ZTIN7NCrypto7NSevenZ8CDecoderE, 56

	.type	_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE,@object # @_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE
	.section	.rodata._ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE
	.p2align	3
_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE
	.quad	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED2Ev
	.quad	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEED0Ev
	.quad	_ZN13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE, 40

	.type	_ZTS13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE,@object # @_ZTS13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE
	.section	.rodata._ZTS13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE
	.p2align	4
_ZTS13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE:
	.asciz	"13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE"
	.size	_ZTS13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE, 45

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE,@object # @_ZTI13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE
	.section	.rodata._ZTI13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE
	.p2align	4
_ZTI13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN7NCrypto7NSevenZ8CKeyInfoEE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_7zAes.ii

	.globl	_ZN7NCrypto7NSevenZ5CBaseC1Ev
	.type	_ZN7NCrypto7NSevenZ5CBaseC1Ev,@function
_ZN7NCrypto7NSevenZ5CBaseC1Ev = _ZN7NCrypto7NSevenZ5CBaseC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
