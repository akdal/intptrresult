	.text
	.file	"libclamav_special.bc"
	.globl	cli_check_mydoom_log
	.p2align	4, 0x90
	.type	cli_check_mydoom_log,@function
cli_check_mydoom_log:                   # @cli_check_mydoom_log
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r12d
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rsp, %r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	$32, %edx
	movl	%r12d, %edi
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	$32, %eax
	jne	.LBB0_4
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movl	(%rsp), %esi
	bswapl	%esi
	notl	%esi
	movl	%esi, (%rsp)
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	(%rsp), %eax
	movl	4(%rsp), %ecx
	bswapl	%ecx
	xorl	%eax, %ecx
	movl	%ecx, 4(%rsp)
	movl	8(%rsp), %edx
	bswapl	%edx
	xorl	%eax, %edx
	movl	%edx, 8(%rsp)
	addl	%ecx, %edx
	movl	12(%rsp), %ecx
	bswapl	%ecx
	xorl	%eax, %ecx
	movl	%ecx, 12(%rsp)
	addl	%edx, %ecx
	movl	16(%rsp), %edx
	bswapl	%edx
	xorl	%eax, %edx
	movl	%edx, 16(%rsp)
	addl	%ecx, %edx
	movl	20(%rsp), %ecx
	bswapl	%ecx
	xorl	%eax, %ecx
	movl	%ecx, 20(%rsp)
	addl	%edx, %ecx
	movl	24(%rsp), %edx
	bswapl	%edx
	xorl	%eax, %edx
	movl	%edx, 24(%rsp)
	addl	%ecx, %edx
	movl	28(%rsp), %ebp
	bswapl	%ebp
	xorl	%eax, %ebp
	movl	%ebp, 28(%rsp)
	addl	%edx, %ebp
	notl	%ebp
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	cmpl	%ebp, (%rsp)
	jne	.LBB0_7
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	incl	%ebx
	cmpl	$5, %ebx
	jl	.LBB0_1
	jmp	.LBB0_5
.LBB0_4:
	xorl	%r13d, %r13d
	cmpl	$2, %ebx
	jl	.LBB0_7
.LBB0_5:                                # %.thread
	movl	$1, %r13d
	testq	%r14, %r14
	je	.LBB0_7
# BB#6:
	movq	$.L.str.3, (%r14)
.LBB0_7:                                # %.loopexit
	movl	%r13d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	cli_check_mydoom_log, .Lfunc_end0-cli_check_mydoom_log
	.cfi_endproc

	.globl	cli_check_jpeg_exploit
	.p2align	4, 0x90
	.type	cli_check_jpeg_exploit,@function
cli_check_jpeg_exploit:                 # @cli_check_jpeg_exploit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 112
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edi, %r12d
	xorl	%r15d, %r15d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	12(%rsp), %rsi
	movl	$2, %edx
	movl	%r12d, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB1_42
# BB#1:
	cmpb	$-1, 12(%rsp)
	jne	.LBB1_42
# BB#2:
	cmpb	$-40, 13(%rsp)
	jne	.LBB1_42
# BB#3:                                 # %.preheader
	leaq	12(%rsp), %rsi
	movl	$4, %edx
	movl	%r12d, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB1_42
# BB#4:                                 # %.lr.ph
	leaq	12(%rsp), %r13
	.p2align	4, 0x90
.LBB1_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_27 Depth 2
	movb	12(%rsp), %cl
	movb	13(%rsp), %dil
	movl	%edi, %edx
	andb	%cl, %dl
	cmpb	$-1, %dl
	je	.LBB1_14
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=1
	cmpb	$-2, %dil
	sete	%sil
	cmpb	$-1, %cl
	sete	%bl
	movzbl	14(%rsp), %r14d
	setne	%dl
	andb	%sil, %bl
	testq	%r14, %r14
	movzbl	15(%rsp), %ecx
	sete	%al
	andb	%bl, %al
	cmpq	$2, %rcx
	setb	%bl
	andb	%al, %bl
	movzbl	%bl, %eax
	leal	-1(%rax,%rax), %r15d
	jne	.LBB1_42
# BB#7:                                 #   in Loop: Header=BB1_5 Depth=1
	testb	%dl, %dl
	jne	.LBB1_42
# BB#8:                                 #   in Loop: Header=BB1_5 Depth=1
	cmpb	$-38, %dil
	je	.LBB1_39
# BB#9:                                 #   in Loop: Header=BB1_5 Depth=1
	shlq	$8, %r14
	orq	%rcx, %r14
	cmpq	$2, %r14
	jb	.LBB1_40
# BB#10:                                #   in Loop: Header=BB1_5 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	movq	%rax, %rbp
	cmpb	$-19, 13(%rsp)
	jne	.LBB1_36
# BB#11:                                #   in Loop: Header=BB1_5 Depth=1
	movl	$14, %edx
	movl	%r12d, %edi
	leaq	42(%rsp), %rsi
	callq	cli_readn
	cmpl	$14, %eax
	jne	.LBB1_36
# BB#12:                                #   in Loop: Header=BB1_5 Depth=1
	movl	$.L.str.9, %esi
	movl	$14, %edx
	leaq	42(%rsp), %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB1_36
# BB#13:                                #   in Loop: Header=BB1_5 Depth=1
	movq	%r13, %rbx
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_14:                               #   in Loop: Header=BB1_5 Depth=1
	movq	$-3, %rsi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	jmp	.LBB1_37
.LBB1_15:                               #   in Loop: Header=BB1_27 Depth=2
	movl	$2, %edx
	movl	%r12d, %edi
	leaq	16(%rsp), %rsi
	callq	cli_readn
	movl	$-1, %r15d
	cmpl	$2, %eax
	jne	.LBB1_32
# BB#16:                                #   in Loop: Header=BB1_27 Depth=2
	movzwl	16(%rsp), %esi
	movl	%esi, %eax
	shrl	$8, %eax
	movl	%esi, %ecx
	shll	$8, %ecx
	orl	%eax, %ecx
	bswapl	%esi
	shrl	$16, %esi
	movw	%cx, 16(%rsp)
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %edx
	movl	%r12d, %edi
	leaq	19(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB1_32
# BB#17:                                #   in Loop: Header=BB1_27 Depth=2
	movzbl	19(%rsp), %eax
	movl	%eax, %ecx
	notb	%cl
	andb	$1, %cl
	movzbl	%cl, %esi
	addq	%rax, %rsi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	movl	$4, %edx
	movl	%r12d, %edi
	leaq	20(%rsp), %rsi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB1_32
# BB#18:                                #   in Loop: Header=BB1_27 Depth=2
	movl	20(%rsp), %ecx
	movl	%ecx, %eax
	bswapl	%eax
	movl	%eax, 20(%rsp)
	testl	%ecx, %ecx
	je	.LBB1_32
# BB#19:                                #   in Loop: Header=BB1_27 Depth=2
	testl	$16777216, %ecx         # imm = 0x1000000
	je	.LBB1_21
# BB#20:                                #   in Loop: Header=BB1_27 Depth=2
	incl	%eax
	movl	%eax, 20(%rsp)
.LBB1_21:                               #   in Loop: Header=BB1_27 Depth=2
	movzwl	16(%rsp), %ecx
	cmpl	$1033, %ecx             # imm = 0x409
	je	.LBB1_23
# BB#22:                                #   in Loop: Header=BB1_27 Depth=2
	movzwl	%cx, %ecx
	cmpl	$1036, %ecx             # imm = 0x40C
	jne	.LBB1_26
.LBB1_23:                               #   in Loop: Header=BB1_27 Depth=2
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$28, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	movl	%r12d, %edi
	callq	cli_check_jpeg_exploit
	movl	%eax, %r15d
	cmpl	$1, %r15d
	jne	.LBB1_25
# BB#24:                                #   in Loop: Header=BB1_27 Depth=2
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_25:                               #   in Loop: Header=BB1_27 Depth=2
	movl	20(%rsp), %esi
	addq	32(%rsp), %rsi          # 8-byte Folded Reload
	xorl	%edx, %edx
	movl	%r12d, %edi
	callq	lseek
	jmp	.LBB1_32
.LBB1_26:                               #   in Loop: Header=BB1_27 Depth=2
	movl	%eax, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	xorl	%r15d, %r15d
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_27:                               #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	movq	%rax, %r13
	movl	$4, %edx
	movl	%r12d, %edi
	leaq	27(%rsp), %rsi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB1_30
# BB#28:                                #   in Loop: Header=BB1_27 Depth=2
	cmpl	$1296646712, 27(%rsp)   # imm = 0x4D494238
	je	.LBB1_15
# BB#29:                                #   in Loop: Header=BB1_27 Depth=2
	movb	$0, 31(%rsp)
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	leaq	27(%rsp), %rsi
	callq	cli_dbgmsg
	jmp	.LBB1_31
	.p2align	4, 0x90
.LBB1_30:                               #   in Loop: Header=BB1_27 Depth=2
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_31:                               # %jpeg_check_photoshop_8bim.exit.i
                                        #   in Loop: Header=BB1_27 Depth=2
	movl	$-1, %r15d
.LBB1_32:                               # %jpeg_check_photoshop_8bim.exit.i
                                        #   in Loop: Header=BB1_27 Depth=2
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	testl	%r15d, %r15d
	jne	.LBB1_34
# BB#33:                                # %jpeg_check_photoshop_8bim.exit.i
                                        #   in Loop: Header=BB1_27 Depth=2
	cmpq	%r13, %rax
	jg	.LBB1_27
.LBB1_34:                               #   in Loop: Header=BB1_5 Depth=1
	cmpl	$-1, %r15d
	movq	%rbx, %r13
	je	.LBB1_36
# BB#35:                                # %jpeg_check_photoshop.exit
                                        #   in Loop: Header=BB1_5 Depth=1
	testl	%r15d, %r15d
	jne	.LBB1_42
	.p2align	4, 0x90
.LBB1_36:                               #   in Loop: Header=BB1_5 Depth=1
	leaq	-2(%r14,%rbp), %rbp
	xorl	%edx, %edx
	movl	%r12d, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB1_41
.LBB1_37:                               # %.backedge
                                        #   in Loop: Header=BB1_5 Depth=1
	movl	$4, %edx
	movl	%r12d, %edi
	movq	%r13, %rsi
	callq	cli_readn
	cmpl	$4, %eax
	je	.LBB1_5
.LBB1_39:
	xorl	%r15d, %r15d
	jmp	.LBB1_42
.LBB1_40:
	movl	$1, %r15d
	jmp	.LBB1_42
.LBB1_41:
	movl	$-1, %r15d
.LBB1_42:                               # %.loopexit
	movl	%r15d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	cli_check_jpeg_exploit, .Lfunc_end1-cli_check_jpeg_exploit
	.cfi_endproc

	.globl	cli_check_riff_exploit
	.p2align	4, 0x90
	.type	cli_check_riff_exploit,@function
cli_check_riff_exploit:                 # @cli_check_riff_exploit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 48
.Lcfi30:
	.cfi_offset %rbx, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	xorl	%ebp, %ebp
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	8(%rsp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB2_10
# BB#1:
	leaq	4(%rsp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB2_10
# BB#2:
	leaq	12(%rsp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB2_10
# BB#3:
	xorl	%r14d, %r14d
	cmpl	$1179011410, 8(%rsp)    # imm = 0x46464952
	je	.LBB2_6
# BB#4:
	xorl	%ebp, %ebp
	cmpl	$1481001298, 8(%rsp)    # imm = 0x58464952
	jne	.LBB2_10
# BB#5:
	movl	$1, %r14d
.LBB2_6:
	cmpl	$1313817409, 12(%rsp)   # imm = 0x4E4F4341
	movl	$0, %ebp
	jne	.LBB2_10
# BB#7:
	movl	4(%rsp), %eax
	testl	%r14d, %r14d
	movl	%eax, %ecx
	bswapl	%ecx
	cmovel	%eax, %ecx
	movl	%ecx, 4(%rsp)
	.p2align	4, 0x90
.LBB2_8:                                # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movl	%ebx, %edi
	movl	%r14d, %esi
	callq	riff_read_chunk
	movl	%eax, %ebp
	cmpl	$1, %ebp
	je	.LBB2_8
# BB#9:
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	movl	4(%rsp), %ecx
	cmpq	%rcx, %rax
	movl	$2, %eax
	cmovll	%eax, %ebp
.LBB2_10:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	cli_check_riff_exploit, .Lfunc_end2-cli_check_riff_exploit
	.cfi_endproc

	.p2align	4, 0x90
	.type	riff_read_chunk,@function
riff_read_chunk:                        # @riff_read_chunk
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r15d
	movl	%edi, %r14d
	cmpl	$1001, %ebp             # imm = 0x3E9
	jl	.LBB3_3
# BB#1:
	xorl	%ebx, %ebx
	movl	$.L.str.17, %edi
.LBB3_2:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB3_16
.LBB3_3:
	leaq	12(%rsp), %rsi
	movl	$4, %edx
	movl	%r14d, %edi
	callq	cli_readn
	xorl	%ebx, %ebx
	cmpl	$4, %eax
	jne	.LBB3_16
# BB#4:
	leaq	16(%rsp), %rsi
	movl	$4, %edx
	movl	%r14d, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB3_16
# BB#5:
	movl	16(%rsp), %eax
	testl	%r15d, %r15d
	movl	%eax, %ecx
	bswapl	%ecx
	cmovel	%eax, %ecx
	movl	%ecx, 16(%rsp)
	cmpl	$1179011410, 12(%rsp)   # imm = 0x46464952
	je	.LBB3_16
# BB#6:
	cmpl	$1481001298, 12(%rsp)   # imm = 0x58464952
	je	.LBB3_16
# BB#7:
	cmpl	$1414744396, 12(%rsp)   # imm = 0x5453494C
	je	.LBB3_11
# BB#8:
	cmpl	$1347375696, 12(%rsp)   # imm = 0x504F5250
	je	.LBB3_11
# BB#9:
	cmpl	$1297239878, 12(%rsp)   # imm = 0x4D524F46
	je	.LBB3_11
# BB#10:
	cmpl	$542392643, 12(%rsp)    # imm = 0x20544143
	je	.LBB3_11
# BB#14:
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	lseek
	movl	16(%rsp), %ecx
	addq	%rax, %rcx
	movl	%ecx, %ebp
	andl	$1, %ebp
	addq	%rcx, %rbp
	cmpq	%rax, %rbp
	jl	.LBB3_16
# BB#15:
	xorl	%edx, %edx
	movl	%r14d, %edi
	movq	%rbp, %rsi
	callq	lseek
	xorl	%ebx, %ebx
	cmpq	%rbp, %rax
	setne	%bl
	incl	%ebx
	jmp	.LBB3_16
.LBB3_11:
	leaq	20(%rsp), %rsi
	movl	$4, %edx
	movl	%r14d, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB3_12
# BB#13:
	incl	%ebp
	movl	%r14d, %edi
	movl	%r15d, %esi
	movl	%ebp, %edx
	callq	riff_read_chunk
	movl	%eax, %ebx
.LBB3_16:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_12:
	xorl	%ebx, %ebx
	movl	$.L.str.22, %edi
	jmp	.LBB3_2
.Lfunc_end3:
	.size	riff_read_chunk, .Lfunc_end3-riff_read_chunk
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in cli_check_mydoom_log()\n"
	.size	.L.str, 27

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Mydoom: key: %d\n"
	.size	.L.str.1, 17

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Mydoom: check: %d\n"
	.size	.L.str.2, 19

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Worm.Mydoom.M.log"
	.size	.L.str.3, 18

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"in cli_check_jpeg_exploit()\n"
	.size	.L.str.4, 29

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"in cli_check_riff_exploit()\n"
	.size	.L.str.5, 29

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"RIFF"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"RIFX"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"ACON"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Photoshop 3.0"
	.size	.L.str.9, 14

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Found Photoshop segment\n"
	.size	.L.str.10, 25

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"read bim failed\n"
	.size	.L.str.11, 17

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"8BIM"
	.size	.L.str.12, 5

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"missed 8bim: %s\n"
	.size	.L.str.13, 17

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"ID: 0x%.4x\n"
	.size	.L.str.14, 12

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"found thumbnail\n"
	.size	.L.str.15, 17

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Exploit found in thumbnail\n"
	.size	.L.str.16, 28

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"riff_read_chunk: recursion level exceeded\n"
	.size	.L.str.17, 43

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"LIST"
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"PROP"
	.size	.L.str.19, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"FORM"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"CAT "
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"riff_read_chunk: read list type failed\n"
	.size	.L.str.22, 40


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
