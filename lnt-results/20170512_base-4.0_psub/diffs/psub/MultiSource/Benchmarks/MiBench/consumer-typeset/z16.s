	.text
	.file	"z16.bc"
	.globl	FindShift
	.p2align	4, 0x90
	.type	FindShift,@function
FindShift:                              # @FindShift
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movzwl	64(%rbp), %eax
	shrl	$10, %eax
	andb	$7, %al
	cmpb	$5, %al
	je	.LBB0_3
# BB#1:
	cmpb	$1, %al
	jne	.LBB0_4
# BB#2:
	movswl	66(%rbp), %eax
	jmp	.LBB0_5
.LBB0_3:
	movslq	%r14d, %rax
	movl	56(%rbx,%rax,4), %ecx
	addl	48(%rbx,%rax,4), %ecx
	movswl	66(%rbp), %edx
	imull	%ecx, %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$20, %eax
	addl	%edx, %eax
	sarl	$12, %eax
	jmp	.LBB0_5
.LBB0_4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
                                        # implicit-def: %EAX
.LBB0_5:
	movswl	70(%rbp), %ecx
	cmpl	$160, %ecx
	je	.LBB0_9
# BB#6:
	cmpl	$159, %ecx
	je	.LBB0_11
# BB#7:
	cmpl	$158, %ecx
	jne	.LBB0_10
# BB#8:
	movslq	%r14d, %rcx
	subl	48(%rbx,%rcx,4), %eax
	jmp	.LBB0_11
.LBB0_9:
	negl	%eax
	jmp	.LBB0_11
.LBB0_10:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
                                        # implicit-def: %EAX
.LBB0_11:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	FindShift, .Lfunc_end0-FindShift
	.cfi_endproc

	.globl	SetNeighbours
	.p2align	4, 0x90
	.type	SetNeighbours,@function
SetNeighbours:                          # @SetNeighbours
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %r15
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %r12
	movq	$0, (%r15)
	movq	(%r12), %r14
	cmpb	$0, 32(%r14)
	je	.LBB1_2
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                # %.critedge
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	(%r14), %r14
	cmpb	$0, 32(%r14)
	jne	.LBB1_7
.LBB1_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	leaq	16(%r14), %rdi
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_37:                               #   in Loop: Header=BB1_3 Depth=2
	addq	$16, %rdi
.LBB1_3:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rdi
	movq	%rdi, (%rbp)
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB1_37
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpb	$9, %al
	jne	.LBB1_8
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB1_6
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB1_6
.LBB1_9:
	movq	(%r12), %rcx
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	movq	%rcx, (%r15)
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB1_10
# BB#11:                                # %.preheader104
	movl	%eax, %ecx
	addb	$-119, %cl
	cmpb	$19, %cl
	movl	4(%rsp), %r14d          # 4-byte Reload
	ja	.LBB1_12
	.p2align	4, 0x90
.LBB1_25:                               # %.lr.ph125
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_26 Depth 2
	movq	(%r12), %r12
	movq	(%r12), %rcx
	.p2align	4, 0x90
.LBB1_26:                               #   Parent Loop BB1_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movq	%rcx, (%r15)
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB1_26
# BB#24:                                # %.loopexit103
                                        #   in Loop: Header=BB1_25 Depth=1
	movl	%eax, %ecx
	addb	$-119, %cl
	cmpb	$19, %cl
	jbe	.LBB1_25
.LBB1_12:                               # %.critedge1
	cmpb	$1, %al
	je	.LBB1_14
# BB#13:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_14
.LBB1_7:
	movl	4(%rsp), %r14d          # 4-byte Reload
.LBB1_14:                               # %.loopexit105
	movq	$0, (%r13)
	movq	8(%r12), %rbp
	cmpb	$0, 32(%rbp)
	je	.LBB1_16
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_20:                               # %.critedge2.backedge
                                        #   in Loop: Header=BB1_16 Depth=1
	movq	8(%rbp), %rbp
	cmpb	$0, 32(%rbp)
	jne	.LBB1_21
.LBB1_16:                               # %.preheader101
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_17 Depth 2
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB1_17:                               #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rdi), %rdi
	movq	%rdi, (%rbx)
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB1_17
# BB#18:                                #   in Loop: Header=BB1_16 Depth=1
	cmpb	$9, %al
	jne	.LBB1_27
# BB#19:                                #   in Loop: Header=BB1_16 Depth=1
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB1_20
	jmp	.LBB1_28
	.p2align	4, 0x90
.LBB1_27:                               #   in Loop: Header=BB1_16 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB1_20
.LBB1_28:
	movq	(%rbp), %rcx
	.p2align	4, 0x90
.LBB1_29:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rcx), %rcx
	movq	%rcx, (%r13)
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB1_29
	jmp	.LBB1_30
	.p2align	4, 0x90
.LBB1_33:                               # %.lr.ph
                                        #   in Loop: Header=BB1_30 Depth=1
	movq	(%rbp), %rbp
	movq	(%rbp), %rcx
	.p2align	4, 0x90
.LBB1_34:                               #   Parent Loop BB1_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movq	%rcx, (%r13)
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB1_34
.LBB1_30:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_34 Depth 2
	movl	%eax, %ecx
	addb	$-119, %cl
	cmpb	$19, %cl
	jbe	.LBB1_33
# BB#31:                                # %.critedge3
	cmpb	$1, %al
	je	.LBB1_21
# BB#32:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_21:                               # %.loopexit102
	movq	64(%rsp), %rax
	movl	$151, %ecx
	testl	%r14d, %r14d
	jne	.LBB1_36
# BB#22:
	movq	(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB1_23
# BB#35:
	movzwl	44(%rcx), %edx
	andl	$256, %edx              # imm = 0x100
	shrl	$8, %edx
	movl	$153, %ecx
	subl	%edx, %ecx
	jmp	.LBB1_36
.LBB1_23:
	movl	$152, %ecx
.LBB1_36:
	movl	%ecx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	SetNeighbours, .Lfunc_end1-SetNeighbours
	.cfi_endproc

	.globl	AdjustSize
	.p2align	4, 0x90
	.type	AdjustSize,@function
AdjustSize:                             # @AdjustSize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 144
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movl	%esi, %ebx
	movq	%rdi, %r14
	movl	%ebx, (%rsp)
	movl	%edx, 4(%rsp)
	movl	%r13d, %edi
	callq	SetLengthDim
	movslq	%r13d, %r15
	movl	$1, %r12d
	cmpl	48(%r14,%r15,4), %ebx
	jne	.LBB2_4
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_70:                               # %.backedge408.backedge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %ebx
	movq	%rbp, %r14
	cmpl	48(%r14,%r15,4), %ebx
	jne	.LBB2_4
.LBB2_2:
	movl	4(%rsp), %eax
	cmpl	56(%r14,%r15,4), %eax
	jne	.LBB2_4
# BB#3:
	movb	32(%r14), %al
	addb	$-2, %al
	cmpb	$6, %al
	ja	.LBB2_178
.LBB2_4:                                # %.critedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_19 Depth 2
                                        #       Child Loop BB2_22 Depth 3
                                        #     Child Loop BB2_146 Depth 2
                                        #       Child Loop BB2_147 Depth 3
                                        #     Child Loop BB2_141 Depth 2
                                        #       Child Loop BB2_142 Depth 3
                                        #     Child Loop BB2_160 Depth 2
                                        #       Child Loop BB2_161 Depth 3
                                        #     Child Loop BB2_37 Depth 2
                                        #       Child Loop BB2_38 Depth 3
                                        #     Child Loop BB2_32 Depth 2
                                        #       Child Loop BB2_33 Depth 3
                                        #     Child Loop BB2_49 Depth 2
                                        #       Child Loop BB2_50 Depth 3
                                        #     Child Loop BB2_58 Depth 2
	cmpq	%r14, 24(%r14)
	jne	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
	movl	(%rsp), %ebx
.LBB2_6:                                #   in Loop: Header=BB2_4 Depth=1
	orl	4(%rsp), %ebx
	jns	.LBB2_8
# BB#7:                                 #   in Loop: Header=BB2_4 Depth=1
	leaq	32(%r14), %r8
	movl	$16, %edi
	movl	$5, %esi
	movl	$.L.str.6, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB2_8:                                #   in Loop: Header=BB2_4 Depth=1
	movb	32(%r14), %al
	movl	%eax, %ecx
	addb	$-15, %cl
	cmpb	$1, %cl
	jbe	.LBB2_9
# BB#18:                                #   in Loop: Header=BB2_4 Depth=1
	leaq	24(%r14), %rax
	leaq	16(%r14), %rcx
	testl	%r13d, %r13d
	cmoveq	%rax, %rcx
	movq	(%rcx), %rdi
	leaq	8(%rdi), %rcx
	xorl	%esi, %esi
	movq	%rcx, %rbp
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_25:                               # %.loopexit398.loopexit
                                        #   in Loop: Header=BB2_19 Depth=2
	addq	$8, %rbp
.LBB2_19:                               #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_22 Depth 3
	movq	(%rbp), %rbp
	movzbl	32(%rbp), %eax
	cmpq	$99, %rax
	ja	.LBB2_176
# BB#20:                                #   in Loop: Header=BB2_19 Depth=2
	leaq	32(%rbp), %rbx
	jmpq	*.LJTI2_0(,%rax,8)
.LBB2_21:                               # %.preheader397.preheader
                                        #   in Loop: Header=BB2_19 Depth=2
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB2_22:                               # %.preheader397
                                        #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_19 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rax), %rax
	movzbl	32(%rax), %edx
	testb	%dl, %dl
	je	.LBB2_22
# BB#23:                                # %.preheader397
                                        #   in Loop: Header=BB2_19 Depth=2
	cmpb	$1, %dl
	jne	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_19 Depth=2
	testb	$1, 45(%rax)
	cmovnel	%r12d, %esi
	jmp	.LBB2_25
.LBB2_105:                              #   in Loop: Header=BB2_4 Depth=1
	cmpl	$1, %r13d
	sete	%dl
	cmpb	$19, %al
	sete	%bl
	xorb	%dl, %bl
	je	.LBB2_106
# BB#138:                               #   in Loop: Header=BB2_4 Depth=1
	movq	(%rdi), %rax
	movb	$1, %dl
	cmpq	%rbp, %rax
	movq	%rbp, %rsi
	movb	$1, %dil
	je	.LBB2_139
	.p2align	4, 0x90
.LBB2_146:                              # %.preheader395
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_147 Depth 3
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB2_147:                              #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_146 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rsi), %rsi
	movzbl	32(%rsi), %ebx
	testb	%bl, %bl
	je	.LBB2_147
# BB#148:                               #   in Loop: Header=BB2_146 Depth=2
	cmpb	$1, %bl
	jne	.LBB2_151
# BB#149:                               #   in Loop: Header=BB2_146 Depth=2
	testb	$2, 45(%rsi)
	je	.LBB2_150
.LBB2_151:                              # %.loopexit396
                                        #   in Loop: Header=BB2_146 Depth=2
	movq	(%rax), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_146
# BB#152:                               #   in Loop: Header=BB2_4 Depth=1
	movq	%rbp, %rsi
	movb	$1, %dil
	jmp	.LBB2_139
.LBB2_102:                              #   in Loop: Header=BB2_4 Depth=1
	testl	%r13d, %r13d
	sete	%cl
	cmpb	$16, %al
	sete	%al
	cmpb	%al, %cl
	je	.LBB2_104
# BB#103:                               #   in Loop: Header=BB2_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_104:                              #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %eax
	movl	%eax, 48(%r14,%r15,4)
	movl	4(%rsp), %ecx
	movl	%ecx, 56(%r14,%r15,4)
	movl	48(%rbp,%r15,4), %edx
	cmpl	%edx, %eax
	cmovll	%edx, %eax
	movl	%eax, (%rsp)
	jmp	.LBB2_166
.LBB2_81:                               #   in Loop: Header=BB2_4 Depth=1
	testl	%r13d, %r13d
	sete	%cl
	cmpb	$26, %al
	sete	%dl
	movl	(%rsp), %eax
	xorb	%cl, %dl
	je	.LBB2_82
# BB#87:                                #   in Loop: Header=BB2_4 Depth=1
	movl	%eax, 48(%r14,%r15,4)
	jmp	.LBB2_88
.LBB2_89:                               #   in Loop: Header=BB2_4 Depth=1
	testl	%r13d, %r13d
	sete	%bl
	movl	(%rsp), %ecx
	movl	%ecx, 48(%r14,%r15,4)
	movl	4(%rsp), %edx
	movl	%edx, 56(%r14,%r15,4)
	cmpb	$28, %al
	sete	%al
	xorb	%bl, %al
	jne	.LBB2_70
# BB#90:                                #   in Loop: Header=BB2_4 Depth=1
	movzwl	64(%rbp), %eax
	shrl	$10, %eax
	andb	$7, %al
	cmpb	$5, %al
	je	.LBB2_93
# BB#91:                                #   in Loop: Header=BB2_4 Depth=1
	cmpb	$1, %al
	jne	.LBB2_94
# BB#92:                                #   in Loop: Header=BB2_4 Depth=1
	movswl	66(%rbp), %eax
	jmp	.LBB2_95
.LBB2_69:                               #   in Loop: Header=BB2_4 Depth=1
	testl	%r13d, %r13d
	sete	%cl
	movl	(%rsp), %edx
	movl	%edx, 48(%r14,%r15,4)
	movl	4(%rsp), %edx
	movl	%edx, 56(%r14,%r15,4)
	cmpb	$30, %al
	sete	%al
	cmpb	%al, %cl
	jne	.LBB2_70
	jmp	.LBB2_178
.LBB2_177:                              #   in Loop: Header=BB2_4 Depth=1
	testl	%r13d, %r13d
	sete	%cl
	movl	(%rsp), %edx
	movl	%edx, 48(%r14,%r15,4)
	movl	4(%rsp), %edx
	movl	%edx, 56(%r14,%r15,4)
	cmpb	$32, %al
	sete	%al
	xorb	%cl, %al
	jne	.LBB2_70
	jmp	.LBB2_178
.LBB2_171:                              #   in Loop: Header=BB2_4 Depth=1
	cmpl	$1, %r13d
	jne	.LBB2_170
	jmp	.LBB2_172
.LBB2_169:                              #   in Loop: Header=BB2_4 Depth=1
	testl	%r13d, %r13d
	je	.LBB2_172
.LBB2_170:                              #   in Loop: Header=BB2_4 Depth=1
	movl	%eax, %edi
	jmp	.LBB2_168
	.p2align	4, 0x90
.LBB2_172:                              #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %eax
	movl	%eax, 48(%r14,%r15,4)
.LBB2_88:                               # %.backedge408.backedge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	4(%rsp), %eax
	movl	%eax, 56(%r14,%r15,4)
	jmp	.LBB2_70
.LBB2_106:                              #   in Loop: Header=BB2_4 Depth=1
	testl	%r13d, %r13d
	jne	.LBB2_110
# BB#107:                               #   in Loop: Header=BB2_4 Depth=1
	cmpb	$17, %al
	jne	.LBB2_110
# BB#108:                               #   in Loop: Header=BB2_4 Depth=1
	movb	68(%rbp), %al
	andb	$112, %al
	cmpb	$112, %al
	jne	.LBB2_110
# BB#109:                               #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %eax
	movl	%eax, 48(%r14)
	movl	4(%rsp), %eax
	movl	%eax, 56(%r14)
	movl	48(%rbp), %eax
	movl	%eax, (%rsp)
	movl	56(%rbp), %ecx
	movl	%ecx, 4(%rsp)
	jmp	.LBB2_70
.LBB2_110:                              #   in Loop: Header=BB2_4 Depth=1
	subq	$8, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	leaq	64(%rsp), %rdx
	leaq	40(%rsp), %rcx
	leaq	48(%rsp), %r8
	leaq	32(%rsp), %r9
	leaq	76(%rsp), %rax
	pushq	%rax
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	callq	SetNeighbours
	addq	$16, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset -16
	movb	32(%r14), %al
	addb	$-2, %al
	movq	56(%rsp), %rbx
	cmpb	$6, %al
	ja	.LBB2_122
# BB#111:                               #   in Loop: Header=BB2_4 Depth=1
	testq	%rbx, %rbx
	je	.LBB2_112
# BB#113:                               #   in Loop: Header=BB2_4 Depth=1
	movq	32(%rsp), %rax
	movl	56(%rax,%r15,4), %edi
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	leaq	44(%rbx), %rcx
	callq	MinGap
	jmp	.LBB2_114
.LBB2_176:                              #   in Loop: Header=BB2_4 Depth=1
	movq	no_fpos(%rip), %r14
	movl	%eax, %edi
	callq	Image
	movq	%rax, %rbx
	subq	$8, %rsp
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.14, %edx
	movl	$0, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	movq	%r14, %r8
	pushq	%rbx
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	Error
	addq	$16, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB2_70
.LBB2_26:                               #   in Loop: Header=BB2_4 Depth=1
	testb	$1, 43(%rbp)
	je	.LBB2_27
# BB#28:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$1, %r13d
	je	.LBB2_27
# BB#29:                                #   in Loop: Header=BB2_4 Depth=1
	movq	(%rdi), %rax
	movb	$1, %sil
	cmpq	%rbp, %rax
	movq	%rbp, %rdi
	movb	$1, %bl
	je	.LBB2_30
	.p2align	4, 0x90
.LBB2_37:                               # %.preheader390
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_38 Depth 3
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB2_38:                               #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdx), %rdx
	movzbl	32(%rdx), %ebx
	testb	%bl, %bl
	je	.LBB2_38
# BB#39:                                #   in Loop: Header=BB2_37 Depth=2
	cmpb	$1, %bl
	jne	.LBB2_42
# BB#40:                                #   in Loop: Header=BB2_37 Depth=2
	testb	$2, 45(%rdx)
	je	.LBB2_41
.LBB2_42:                               # %.loopexit391
                                        #   in Loop: Header=BB2_37 Depth=2
	movq	(%rax), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_37
# BB#43:                                #   in Loop: Header=BB2_4 Depth=1
	movq	%rbp, %rdi
	movb	$1, %bl
	jmp	.LBB2_30
.LBB2_71:                               #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %ecx
	movl	%ecx, 48(%r14,%r15,4)
	movl	4(%rsp), %eax
	movl	%eax, 56(%r14,%r15,4)
	testl	%r13d, %r13d
	je	.LBB2_72
# BB#74:                                #   in Loop: Header=BB2_4 Depth=1
	movl	72(%rbp), %edx
	jmp	.LBB2_73
.LBB2_167:                              #   in Loop: Header=BB2_4 Depth=1
	movl	$44, %edi
.LBB2_168:                              # %.backedge408.backedge
                                        #   in Loop: Header=BB2_4 Depth=1
	callq	Image
	movq	%rax, %r9
	movl	$16, %edi
	movl	$4, %esi
	movl	$.L.str.12, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	Error
	jmp	.LBB2_70
.LBB2_82:                               #   in Loop: Header=BB2_4 Depth=1
	leaq	64(%rbp), %rdx
	movl	64(%rbp), %edi
	cmpl	%edi, %eax
	jg	.LBB2_85
# BB#83:                                #   in Loop: Header=BB2_4 Depth=1
	movl	4(%rsp), %ecx
	leal	(%rcx,%rax), %esi
	cmpl	68(%rbp), %esi
	jg	.LBB2_85
# BB#84:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	72(%rbp), %ecx
	jle	.LBB2_86
.LBB2_85:                               #   in Loop: Header=BB2_4 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	callq	EchoLength
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	68(%rbp), %edi
	callq	EchoLength
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	72(%rbp), %edi
	callq	EchoLength
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	(%rsp), %edi
	callq	EchoLength
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	4(%rsp), %edi
	callq	EchoLength
	movq	%rax, %r10
	movl	$16, %edi
	movl	$2, %esi
	movl	$.L.str.11, %edx
	movl	$2, %ecx
	movl	$0, %eax
	movq	%rbx, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	pushq	%r10
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	callq	Error
	movq	40(%rsp), %rdx          # 8-byte Reload
	addq	$32, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset -32
	movl	$8388607, 64(%rbp)      # imm = 0x7FFFFF
	movl	(%rsp), %eax
	movl	4(%rsp), %ecx
	leal	(%rcx,%rax), %esi
	movl	%esi, 68(%rbp)
	movl	$8388607, 72(%rbp)      # imm = 0x7FFFFF
.LBB2_86:                               #   in Loop: Header=BB2_4 Depth=1
	movl	%eax, 48(%r14,%r15,4)
	movl	%ecx, 56(%r14,%r15,4)
	movq	%rsp, %rdi
	leaq	4(%rsp), %rsi
	callq	EnlargeToConstraint
	jmp	.LBB2_70
.LBB2_122:                              #   in Loop: Header=BB2_4 Depth=1
	testq	%rbx, %rbx
	je	.LBB2_123
# BB#124:                               #   in Loop: Header=BB2_4 Depth=1
	movq	32(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	56(%rax,%r15,4), %edi
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	addq	$44, %rbx
	movq	%rbx, %rcx
	callq	MinGap
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	56(%rax,%r15,4), %edi
	movl	48(%r14,%r15,4), %esi
	movl	56(%r14,%r15,4), %edx
	movq	%rbx, %rcx
	callq	MinGap
	subl	%eax, 8(%rsp)           # 4-byte Folded Spill
	jmp	.LBB2_125
.LBB2_72:                               #   in Loop: Header=BB2_4 Depth=1
	movl	64(%rbp), %edx
.LBB2_73:                               # %.backedge408.backedge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$25, %esi
	addl	%edx, %esi
	sarl	$7, %esi
	imull	%esi, %ecx
	movl	%ecx, (%rsp)
	imull	%esi, %eax
	movl	%eax, 4(%rsp)
	jmp	.LBB2_70
.LBB2_93:                               #   in Loop: Header=BB2_4 Depth=1
	addl	%edx, %ecx
	movswl	66(%rbp), %edx
	imull	%ecx, %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$20, %eax
	addl	%edx, %eax
	sarl	$12, %eax
	jmp	.LBB2_95
.LBB2_94:                               #   in Loop: Header=BB2_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
                                        # implicit-def: %EAX
.LBB2_95:                               #   in Loop: Header=BB2_4 Depth=1
	movswl	70(%rbp), %ecx
	cmpl	$160, %ecx
	je	.LBB2_99
# BB#96:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$159, %ecx
	je	.LBB2_101
# BB#97:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	$158, %ecx
	jne	.LBB2_100
# BB#98:                                #   in Loop: Header=BB2_4 Depth=1
	subl	48(%r14,%r15,4), %eax
	jmp	.LBB2_101
.LBB2_150:                              #   in Loop: Header=BB2_4 Depth=1
	xorl	%edi, %edi
	movq	%rax, %rsi
.LBB2_139:                              # %.preheader406
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	(%rcx), %rax
	cmpq	%rbp, %rax
	jne	.LBB2_141
	jmp	.LBB2_154
	.p2align	4, 0x90
.LBB2_153:                              # %.loopexit394
                                        #   in Loop: Header=BB2_141 Depth=2
	movq	8(%rax), %rax
	cmpq	%rbp, %rax
	je	.LBB2_154
.LBB2_141:                              # %.preheader393
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_142 Depth 3
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB2_142:                              #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_141 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %ebx
	testb	%bl, %bl
	je	.LBB2_142
# BB#143:                               #   in Loop: Header=BB2_141 Depth=2
	cmpb	$1, %bl
	jne	.LBB2_153
# BB#144:                               #   in Loop: Header=BB2_141 Depth=2
	testb	$2, 45(%rcx)
	jne	.LBB2_153
# BB#145:                               #   in Loop: Header=BB2_4 Depth=1
	xorl	%edx, %edx
	jmp	.LBB2_155
.LBB2_154:                              #   in Loop: Header=BB2_4 Depth=1
	movq	%rbp, %rax
.LBB2_155:                              # %._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %ebx
	movl	%ebx, 48(%r14,%r15,4)
	movl	4(%rsp), %ecx
	movl	%ecx, 56(%r14,%r15,4)
	testb	%dl, %dil
	je	.LBB2_156
# BB#158:                               #   in Loop: Header=BB2_4 Depth=1
	movl	48(%rbp,%r15,4), %eax
	cmpl	%eax, %ebx
	cmovll	%eax, %ebx
	movl	%ebx, (%rsp)
	jmp	.LBB2_166
.LBB2_156:                              # %.preheader403
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	8(%rsi), %rsi
	cmpq	%rax, %rsi
	je	.LBB2_157
# BB#159:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.LBB2_160
.LBB2_157:                              #   in Loop: Header=BB2_4 Depth=1
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.LBB2_165
.LBB2_99:                               #   in Loop: Header=BB2_4 Depth=1
	negl	%eax
	jmp	.LBB2_101
.LBB2_100:                              #   in Loop: Header=BB2_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
                                        # implicit-def: %EAX
.LBB2_101:                              # %FindShift.exit
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %ecx
	addl	%eax, %ecx
	movl	$0, %edx
	cmovsl	%edx, %ecx
	cmpl	$8388608, %ecx          # imm = 0x800000
	movl	$8388607, %esi          # imm = 0x7FFFFF
	cmovgel	%esi, %ecx
	movl	%ecx, (%rsp)
	movl	4(%rsp), %ecx
	subl	%eax, %ecx
	cmovsl	%edx, %ecx
	cmpl	$8388608, %ecx          # imm = 0x800000
	cmovgel	%esi, %ecx
	movl	%ecx, 4(%rsp)
	jmp	.LBB2_70
.LBB2_181:                              # %.outer404
                                        #   in Loop: Header=BB2_160 Depth=2
	movl	48(%rdi,%r15,4), %ebx
	cmpl	%ebx, %ecx
	cmovll	%ebx, %ecx
	movl	56(%rdi,%r15,4), %edi
	cmpl	%edi, %edx
	cmovll	%edi, %edx
	jmp	.LBB2_164
	.p2align	4, 0x90
.LBB2_160:                              #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_161 Depth 3
	leaq	16(%rsi), %rdi
	jmp	.LBB2_161
	.p2align	4, 0x90
.LBB2_180:                              #   in Loop: Header=BB2_161 Depth=3
	addq	$16, %rdi
.LBB2_161:                              #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_160 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi), %rdi
	movzbl	32(%rdi), %ebx
	testb	%bl, %bl
	je	.LBB2_180
# BB#162:                               #   in Loop: Header=BB2_160 Depth=2
	cmpb	$1, %bl
	je	.LBB2_164
# BB#163:                               #   in Loop: Header=BB2_160 Depth=2
	addb	$-119, %bl
	cmpb	$19, %bl
	ja	.LBB2_181
.LBB2_164:                              # %.backedge405
                                        #   in Loop: Header=BB2_160 Depth=2
	movq	8(%rsi), %rsi
	cmpq	%rax, %rsi
	jne	.LBB2_160
.LBB2_165:                              # %.outer404._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	$0, (%rsp)
	addl	%edx, %ecx
.LBB2_166:                              # %.backedge408.backedge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	56(%rbp,%r15,4), %eax
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
	movl	%ecx, 4(%rsp)
	jmp	.LBB2_70
.LBB2_112:                              #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %eax
.LBB2_114:                              #   in Loop: Header=BB2_4 Depth=1
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	40(%rsp), %rcx
	testq	%rcx, %rcx
	movl	4(%rsp), %edi
	je	.LBB2_115
# BB#116:                               #   in Loop: Header=BB2_4 Depth=1
	movq	24(%rsp), %rax
	movl	48(%rax,%r15,4), %esi
	movl	56(%rax,%r15,4), %edx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addq	$44, %rcx
	callq	MinGap
	movl	%eax, %edi
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	jne	.LBB2_120
	jmp	.LBB2_118
.LBB2_123:                              #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %eax
	subl	48(%r14,%r15,4), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB2_125:                              #   in Loop: Header=BB2_4 Depth=1
	movq	40(%rsp), %rbx
	movl	4(%rsp), %edi
	testq	%rbx, %rbx
	je	.LBB2_126
# BB#127:                               #   in Loop: Header=BB2_4 Depth=1
	movq	24(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	48(%rax,%r15,4), %esi
	movl	56(%rax,%r15,4), %edx
	addq	$44, %rbx
	movq	%rbx, %rcx
	callq	MinGap
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	56(%r14,%r15,4), %edi
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	48(%rax,%r15,4), %esi
	movl	56(%rax,%r15,4), %edx
	movq	%rbx, %rcx
	callq	MinGap
	movl	48(%rsp), %edi          # 4-byte Reload
	subl	%eax, %edi
	jmp	.LBB2_128
.LBB2_115:                              #   in Loop: Header=BB2_4 Depth=1
	movb	$1, %cl
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB2_118
.LBB2_120:                              #   in Loop: Header=BB2_4 Depth=1
	movq	32(%rsp), %rax
	movl	56(%rax,%r15,4), %eax
	testb	%cl, %cl
	jne	.LBB2_129
# BB#121:                               #   in Loop: Header=BB2_4 Depth=1
	movq	24(%rsp), %rcx
	movl	48(%rcx,%r15,4), %esi
	movl	56(%rcx,%r15,4), %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	addq	$44, %rcx
	movl	%edi, %ebx
	movl	%eax, %edi
	callq	MinGap
	movl	%ebx, %edi
	jmp	.LBB2_129
.LBB2_126:                              #   in Loop: Header=BB2_4 Depth=1
	subl	56(%r14,%r15,4), %edi
.LBB2_128:                              # %._crit_edge.i
                                        #   in Loop: Header=BB2_4 Depth=1
	xorl	%eax, %eax
.LBB2_129:                              # %._crit_edge.i
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %ecx
	movl	%ecx, 48(%r14,%r15,4)
	movl	4(%rsp), %ecx
	movl	%ecx, 56(%r14,%r15,4)
	movl	68(%rsp), %ecx
	cmpl	$153, %ecx
	je	.LBB2_134
# BB#130:                               # %._crit_edge.i
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpl	$152, %ecx
	je	.LBB2_133
# BB#131:                               # %._crit_edge.i
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpl	$151, %ecx
                                        # implicit-def: %ECX
	jne	.LBB2_136
# BB#132:                               #   in Loop: Header=BB2_4 Depth=1
	addl	8(%rsp), %edi           # 4-byte Folded Reload
	subl	%eax, %edi
	addl	48(%rbp,%r15,4), %edi
	movl	56(%rbp,%r15,4), %ecx
	jmp	.LBB2_136
.LBB2_134:                              #   in Loop: Header=BB2_4 Depth=1
	movl	48(%rbp,%r15,4), %edx
	addl	8(%rsp), %edi           # 4-byte Folded Reload
	subl	%eax, %edi
	jmp	.LBB2_135
.LBB2_133:                              #   in Loop: Header=BB2_4 Depth=1
	movl	8(%rsp), %edx           # 4-byte Reload
	subl	%eax, %edx
	addl	48(%rbp,%r15,4), %edx
.LBB2_135:                              #   in Loop: Header=BB2_4 Depth=1
	movl	%edi, %ecx
	addl	56(%rbp,%r15,4), %ecx
	movl	%edx, %edi
.LBB2_136:                              #   in Loop: Header=BB2_4 Depth=1
	movl	%edi, (%rsp)
	movl	%ecx, 4(%rsp)
	jmp	.LBB2_70
.LBB2_118:                              #   in Loop: Header=BB2_4 Depth=1
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.LBB2_129
# BB#119:                               #   in Loop: Header=BB2_4 Depth=1
	movq	24(%rsp), %rax
	movl	48(%rax,%r15,4), %eax
	jmp	.LBB2_129
.LBB2_41:                               #   in Loop: Header=BB2_4 Depth=1
	xorl	%ebx, %ebx
	movq	%rax, %rdi
.LBB2_30:                               # %.preheader402
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	(%rcx), %rcx
	cmpq	%rbp, %rcx
	jne	.LBB2_32
	jmp	.LBB2_45
	.p2align	4, 0x90
.LBB2_44:                               # %.loopexit389
                                        #   in Loop: Header=BB2_32 Depth=2
	movq	8(%rcx), %rcx
	cmpq	%rbp, %rcx
	je	.LBB2_45
.LBB2_32:                               # %.preheader388
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_33 Depth 3
	movq	%rcx, %rax
	.p2align	4, 0x90
.LBB2_33:                               #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rax), %rax
	movzbl	32(%rax), %edx
	testb	%dl, %dl
	je	.LBB2_33
# BB#34:                                #   in Loop: Header=BB2_32 Depth=2
	cmpb	$1, %dl
	jne	.LBB2_44
# BB#35:                                #   in Loop: Header=BB2_32 Depth=2
	testb	$2, 45(%rax)
	jne	.LBB2_44
# BB#36:                                #   in Loop: Header=BB2_4 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_46
.LBB2_45:                               #   in Loop: Header=BB2_4 Depth=1
	movq	%rbp, %rcx
.LBB2_46:                               # %._crit_edge584
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	(%rsp), %r8d
	movl	%r8d, 48(%r14,%r15,4)
	movl	4(%rsp), %edx
	movl	%edx, 56(%r14,%r15,4)
	testb	%sil, %bl
	je	.LBB2_47
# BB#66:                                #   in Loop: Header=BB2_4 Depth=1
	testb	$32, 42(%rbp)
	jne	.LBB2_47
# BB#67:                                #   in Loop: Header=BB2_4 Depth=1
	movl	48(%rbp,%r15,4), %esi
	cmpl	%esi, %r8d
	cmovll	%esi, %r8d
	movl	%r8d, (%rsp)
	movl	56(%rbp,%r15,4), %ecx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	jmp	.LBB2_55
.LBB2_47:                               # %.preheader401
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	8(%rdi), %rdi
	xorl	%r8d, %r8d
	cmpq	%rcx, %rdi
	movl	$0, %edx
	movl	$0, %esi
	je	.LBB2_54
# BB#48:                                # %.lr.ph593.preheader
                                        #   in Loop: Header=BB2_4 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	.LBB2_49
.LBB2_68:                               # %.outer
                                        #   in Loop: Header=BB2_49 Depth=2
	movl	48(%rbx,%r15,4), %eax
	cmpl	%eax, %edx
	cmovll	%eax, %edx
	movl	56(%rbx,%r15,4), %eax
	cmpl	%eax, %esi
	cmovll	%eax, %esi
	jmp	.LBB2_53
	.p2align	4, 0x90
.LBB2_49:                               #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_50 Depth 3
	leaq	16(%rdi), %rbx
	jmp	.LBB2_50
	.p2align	4, 0x90
.LBB2_179:                              #   in Loop: Header=BB2_50 Depth=3
	addq	$16, %rbx
.LBB2_50:                               #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_49 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_179
# BB#51:                                #   in Loop: Header=BB2_49 Depth=2
	cmpb	$1, %al
	je	.LBB2_53
# BB#52:                                #   in Loop: Header=BB2_49 Depth=2
	addb	$-119, %al
	cmpb	$19, %al
	ja	.LBB2_68
.LBB2_53:                               # %.backedge
                                        #   in Loop: Header=BB2_49 Depth=2
	movq	8(%rdi), %rdi
	cmpq	%rcx, %rdi
	jne	.LBB2_49
.LBB2_54:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	$0, (%rsp)
	addl	%esi, %edx
	movl	56(%rbp,%r15,4), %ecx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	movl	48(%rbp,%r15,4), %esi
.LBB2_55:                               #   in Loop: Header=BB2_4 Depth=1
	movl	%edx, 4(%rsp)
	cmpl	%r8d, %esi
	jne	.LBB2_57
# BB#56:                                #   in Loop: Header=BB2_4 Depth=1
	cmpl	%edx, %ecx
	je	.LBB2_178
.LBB2_57:                               # %._crit_edge751
                                        #   in Loop: Header=BB2_4 Depth=1
	movl	%r8d, 48(%rbp,%r15,4)
	movl	%edx, 56(%rbp,%r15,4)
	movq	24(%rbp), %rbx
	cmpq	%rbp, %rbx
	je	.LBB2_178
	.p2align	4, 0x90
.LBB2_58:                               # %.preheader399
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_58
# BB#59:                                # %.preheader399
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpb	$122, %al
	jne	.LBB2_178
# BB#60:                                #   in Loop: Header=BB2_4 Depth=1
	movq	80(%rbx), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_62
# BB#61:                                #   in Loop: Header=BB2_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	80(%rbx), %rbp
.LBB2_62:                               #   in Loop: Header=BB2_4 Depth=1
	cmpb	$2, 32(%rbp)
	je	.LBB2_64
# BB#63:                                #   in Loop: Header=BB2_4 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	80(%rbx), %rbp
.LBB2_64:                               # %._crit_edge749
                                        #   in Loop: Header=BB2_4 Depth=1
	movq	80(%rbp), %rax
	cmpq	GalleySym(%rip), %rax
	je	.LBB2_70
# BB#65:                                # %._crit_edge749
                                        #   in Loop: Header=BB2_4 Depth=1
	cmpq	ForceGalleySym(%rip), %rax
	je	.LBB2_70
	jmp	.LBB2_178
.LBB2_173:
	testl	%r13d, %r13d
	sete	%cl
	cmpb	$13, %al
	sete	%al
	cmpb	%al, %cl
	je	.LBB2_175
# BB#174:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_175:
	movl	(%rsp), %eax
	movl	%eax, 48(%r14,%r15,4)
	movl	4(%rsp), %ecx
	movl	%ecx, 56(%r14,%r15,4)
	movl	%eax, 48(%rbp,%r15,4)
	movl	%ecx, 56(%rbp,%r15,4)
	jmp	.LBB2_178
.LBB2_9:
	testl	%r13d, %r13d
	sete	%cl
	cmpb	$16, %al
	sete	%al
	cmpb	%al, %cl
	je	.LBB2_11
# BB#10:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_11:
	movl	(%rsp), %eax
	movl	%eax, 48(%r14,%r15,4)
	movl	4(%rsp), %eax
	movl	%eax, 56(%r14,%r15,4)
	movq	24(%r14), %rbp
	cmpq	%r14, %rbp
	jne	.LBB2_13
	jmp	.LBB2_178
	.p2align	4, 0x90
.LBB2_17:                               # %.loopexit
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	(%rsp), %esi
	movl	4(%rsp), %edx
	movq	%rbx, %rdi
	movl	%r13d, %ecx
	callq	AdjustSize
	movq	24(%rbp), %rbp
	cmpq	%r14, %rbp
	je	.LBB2_178
.LBB2_13:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_14 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB2_14:                               #   Parent Loop BB2_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_14
# BB#15:                                #   in Loop: Header=BB2_13 Depth=1
	cmpb	$9, %al
	je	.LBB2_17
# BB#16:                                #   in Loop: Header=BB2_13 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB2_17
.LBB2_75:
	movl	(%rsp), %eax
	movl	%eax, 48(%r14,%r15,4)
	movl	4(%rsp), %eax
	movl	%eax, 56(%r14,%r15,4)
	movl	76(%rbp), %r9d
	leaq	56(%rsp), %rdi
	leaq	32(%rsp), %rsi
	leaq	40(%rsp), %rdx
	leaq	24(%rsp), %rcx
	movq	%r14, %r8
	callq	RotateSize
	movl	56(%rsp), %esi
	movl	32(%rsp), %edx
	cmpl	48(%rbp), %esi
	jne	.LBB2_77
# BB#76:
	cmpl	56(%rbp), %edx
	je	.LBB2_78
.LBB2_77:                               # %._crit_edge735
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	callq	AdjustSize
.LBB2_78:
	movl	40(%rsp), %esi
	movl	24(%rsp), %edx
	cmpl	52(%rbp), %esi
	jne	.LBB2_80
# BB#79:
	cmpl	60(%rbp), %edx
	je	.LBB2_178
.LBB2_80:                               # %._crit_edge737
	movl	$1, %ecx
	movq	%rbp, %rdi
	callq	AdjustSize
.LBB2_178:                              # %.critedge2
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_27:
	movl	(%rsp), %eax
	movl	%eax, 48(%r14,%r15,4)
	movl	4(%rsp), %eax
	movl	%eax, 56(%r14,%r15,4)
	jmp	.LBB2_178
.Lfunc_end2:
	.size	AdjustSize, .Lfunc_end2-AdjustSize
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI2_0:
	.quad	.LBB2_21
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_26
	.quad	.LBB2_172
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_173
	.quad	.LBB2_173
	.quad	.LBB2_102
	.quad	.LBB2_102
	.quad	.LBB2_105
	.quad	.LBB2_105
	.quad	.LBB2_105
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_81
	.quad	.LBB2_81
	.quad	.LBB2_89
	.quad	.LBB2_89
	.quad	.LBB2_69
	.quad	.LBB2_69
	.quad	.LBB2_177
	.quad	.LBB2_177
	.quad	.LBB2_71
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_171
	.quad	.LBB2_169
	.quad	.LBB2_167
	.quad	.LBB2_171
	.quad	.LBB2_169
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_75
	.quad	.LBB2_172
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_176
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172
	.quad	.LBB2_172

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"FindShift: units"
	.size	.L.str.1, 17

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"FindShift: type"
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SetNeighbours: type(*pg)!"
	.size	.L.str.3, 26

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SetNeighbours: type(*sg)!"
	.size	.L.str.4, 26

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"AdjustSize: Up(x) == x!"
	.size	.L.str.5, 24

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"cannot recover from earlier errors"
	.size	.L.str.6, 35

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"AdjustSize: COL_THR!"
	.size	.L.str.7, 21

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"AdjustSize: type(y) != SPLIT!"
	.size	.L.str.8, 30

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"AdjustSize: actual(index)==nilobj!"
	.size	.L.str.9, 35

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"AdjustSize: index non-C!"
	.size	.L.str.10, 25

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"size constraint %s,%s,%s broken by %s,%s"
	.size	.L.str.11, 41

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"size adjustment of %s not implemented"
	.size	.L.str.12, 38

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"AdjustSize: span"
	.size	.L.str.13, 17

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"assert failed in %s %s"
	.size	.L.str.14, 23

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"AdjustSize:"
	.size	.L.str.15, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
