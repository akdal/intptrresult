	.text
	.file	"util.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4652007308841189376     # double 1000
.LCPI0_1:
	.quad	4593671619917905920     # double 0.125
.LCPI0_2:
	.quad	4472406533629990549     # double 1.0000000000000001E-9
	.text
	.globl	getframebits
	.p2align	4, 0x90
	.type	getframebits,@function
getframebits:                           # @getframebits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	cvtsi2sdl	16(%rbx), %xmm1
	divsd	.LCPI0_0(%rip), %xmm1
	movslq	192(%rbx), %rax
	imulq	$60, %rax, %rcx
	cmpl	$1, 204(%rbx)
	movl	$168, %edx
	movl	$288, %esi              # imm = 0x120
	cmovel	%edx, %esi
	movl	$104, %edi
	cmovnel	%edx, %edi
	cmpq	$1, %rax
	movslq	220(%rbx), %rax
	cvtsi2sdl	bitrate_table(%rcx,%rax,4), %xmm0
	cmovel	%esi, %edi
	movl	%edi, %ebp
	orl	$16, %ebp
	cmpl	$0, 60(%rbx)
	cmovel	%edi, %ebp
	cvtsi2sdl	188(%rbx), %xmm2
	divsd	%xmm1, %xmm2
	mulsd	.LCPI0_1(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	.LCPI0_2(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	addl	196(%rbx), %eax
	shll	$3, %eax
	movl	%eax, (%r15)
	subl	%ebp, %eax
	cltd
	idivl	200(%rbx)
	movl	%eax, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	getframebits, .Lfunc_end0-getframebits
	.cfi_endproc

	.globl	display_bitrates
	.p2align	4, 0x90
	.type	display_bitrates,@function
display_bitrates:                       # @display_bitrates
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$10, %edi
	movq	%rbx, %rsi
	callq	fputc
	movl	$.L.str.1, %edi
	movl	$36, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.2, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	bitrate_table+64(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+68(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+72(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+76(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+80(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+84(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+88(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+92(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+96(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+100(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+104(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+108(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+112(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+116(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$10, %edi
	movq	%rbx, %rsi
	callq	fputc
	movl	$10, %edi
	movq	%rbx, %rsi
	callq	fputc
	movl	$.L.str.4, %edi
	movl	$37, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.2, %edi
	movl	$15, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	bitrate_table+4(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+8(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+12(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+16(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+20(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+24(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+28(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+32(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+36(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+40(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+44(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+48(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+52(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	bitrate_table+56(%rip), %edx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$10, %edi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	fputc                   # TAILCALL
.Lfunc_end1:
	.size	display_bitrates, .Lfunc_end1-display_bitrates
	.cfi_endproc

	.globl	BitrateIndex
	.p2align	4, 0x90
	.type	BitrateIndex,@function
BitrateIndex:                           # @BitrateIndex
	.cfi_startproc
# BB#0:
	movl	%edx, %r8d
	movl	%edi, %edx
	movslq	%esi, %rax
	xorl	%esi, %esi
	imulq	$60, %rax, %rdi
	movl	$1, %r9d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	cltq
	xorl	%ecx, %ecx
	cmpl	%edx, bitrate_table(%rdi,%rax,4)
	setne	%cl
	cmovel	%r9d, %esi
	addl	%ecx, %eax
	testl	%esi, %esi
	sete	%cl
	cmpl	$14, %eax
	jg	.LBB2_3
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	testb	%cl, %cl
	jne	.LBB2_1
.LBB2_3:
	testl	%esi, %esi
	je	.LBB2_4
# BB#5:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.LBB2_4:
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movl	%r8d, %ecx
	callq	fprintf
	movl	$-1, %eax
	addq	$8, %rsp
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end2:
	.size	BitrateIndex, .Lfunc_end2-BitrateIndex
	.cfi_endproc

	.globl	SmpFrqIndex
	.p2align	4, 0x90
	.type	SmpFrqIndex,@function
SmpFrqIndex:                            # @SmpFrqIndex
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	movl	$0, (%rsi)
	cmpq	$31999, %rcx            # imm = 0x7CFF
	jg	.LBB3_5
# BB#1:
	cmpq	$16000, %rcx            # imm = 0x3E80
	je	.LBB3_12
# BB#2:
	cmpq	$22050, %rcx            # imm = 0x5622
	je	.LBB3_11
# BB#3:
	cmpq	$24000, %rcx            # imm = 0x5DC0
	jne	.LBB3_13
# BB#4:
	movl	$0, (%rsi)
	movl	$1, %eax
	retq
.LBB3_5:
	cmpq	$32000, %rcx            # imm = 0x7D00
	je	.LBB3_10
# BB#6:
	cmpq	$48000, %rcx            # imm = 0xBB80
	je	.LBB3_9
# BB#7:
	cmpq	$44100, %rcx            # imm = 0xAC44
	jne	.LBB3_13
# BB#8:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.LBB3_12:
	movl	$0, (%rsi)
	movl	$2, %eax
	retq
.LBB3_11:
	movl	$0, (%rsi)
	xorl	%eax, %eax
	retq
.LBB3_10:
	movl	$1, (%rsi)
	movl	$2, %eax
	retq
.LBB3_9:
	movl	$1, (%rsi)
	movl	$1, %eax
	retq
.LBB3_13:
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movl	$-1, %eax
	addq	$8, %rsp
	retq
.Lfunc_end3:
	.size	SmpFrqIndex, .Lfunc_end3-SmpFrqIndex
	.cfi_endproc

	.globl	mem_alloc
	.p2align	4, 0x90
	.type	mem_alloc,@function
mem_alloc:                              # @mem_alloc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_2
# BB#1:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	mem_alloc, .Lfunc_end4-mem_alloc
	.cfi_endproc

	.globl	DetermineByteOrder
	.p2align	4, 0x90
	.type	DetermineByteOrder,@function
DetermineByteOrder:                     # @DetermineByteOrder
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -16
	movl	$1094861636, 8(%rsp)    # imm = 0x41424344
	leaq	3(%rsp), %rbx
	leaq	8(%rsp), %rsi
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	strncpy
	movb	$0, 7(%rsp)
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_1
# BB#2:
	leaq	3(%rsp), %rdi
	movl	$.L.str.9, %esi
	callq	strcmp
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	sete	%al
	addl	%eax, %eax
	jmp	.LBB5_3
.LBB5_1:
	movl	$1, %eax
.LBB5_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end5:
	.size	DetermineByteOrder, .Lfunc_end5-DetermineByteOrder
	.cfi_endproc

	.globl	SwapBytesInWords
	.p2align	4, 0x90
	.type	SwapBytesInWords,@function
SwapBytesInWords:                       # @SwapBytesInWords
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	testl	%esi, %esi
	jle	.LBB6_9
# BB#1:                                 # %.lr.ph.preheader
	leal	-1(%rsi), %eax
	movl	%esi, %edx
	andl	$3, %edx
	je	.LBB6_2
# BB#3:                                 # %.lr.ph.prol.preheader
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_4:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi), %ebx
	movb	%bh, (%rdi)  # NOREX
	movb	%bl, 1(%rdi)
	leaq	2(%rdi), %rdi
	incl	%ecx
	cmpl	%ecx, %edx
	jne	.LBB6_4
# BB#5:
	popq	%rbx
	cmpl	$3, %eax
	jae	.LBB6_7
	jmp	.LBB6_9
.LBB6_2:
	xorl	%ecx, %ecx
	cmpl	$3, %eax
	jb	.LBB6_9
.LBB6_7:                                # %.lr.ph.preheader.new
	subl	%ecx, %esi
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi), %eax
	movb	%ah, (%rdi)  # NOREX
	movb	%al, 1(%rdi)
	movzwl	2(%rdi), %eax
	movb	%ah, 2(%rdi)  # NOREX
	movb	%al, 3(%rdi)
	movzwl	4(%rdi), %eax
	movb	%ah, 4(%rdi)  # NOREX
	movb	%al, 5(%rdi)
	movzwl	6(%rdi), %eax
	movb	%ah, 6(%rdi)  # NOREX
	movb	%al, 7(%rdi)
	addq	$8, %rdi
	addl	$-4, %esi
	jne	.LBB6_8
.LBB6_9:                                # %._crit_edge
	retq
.Lfunc_end6:
	.size	SwapBytesInWords, .Lfunc_end6-SwapBytesInWords
	.cfi_endproc

	.globl	empty_buffer
	.p2align	4, 0x90
	.type	empty_buffer,@function
empty_buffer:                           # @empty_buffer
	.cfi_startproc
# BB#0:
	movl	48(%rdi), %ecx
	incl	%ecx
	movslq	32(%rdi), %rax
	cmpl	%ecx, %eax
	jle	.LBB7_2
# BB#1:
	leaq	-1(%rax), %rcx
	movl	%ecx, 48(%rdi)
	movl	$8, 52(%rdi)
	movq	24(%rdi), %rcx
	movb	$0, -1(%rcx,%rax)
.LBB7_2:
	retq
.Lfunc_end7:
	.size	empty_buffer, .Lfunc_end7-empty_buffer
	.cfi_endproc

	.globl	copy_buffer
	.p2align	4, 0x90
	.type	copy_buffer,@function
copy_buffer:                            # @copy_buffer
	.cfi_startproc
# BB#0:
	movl	32(%rdx), %r8d
	testl	%esi, %esi
	je	.LBB8_1
# BB#2:
	leal	-1(%r8), %r9d
	movl	48(%rdx), %ecx
	subl	%ecx, %r9d
	movl	$-1, %eax
	cmpl	%esi, %r9d
	jle	.LBB8_3
	jmp	.LBB8_9
.LBB8_1:                                # %._crit_edge28
	movl	48(%rdx), %ecx
.LBB8_3:
	movslq	%r8d, %rsi
	decq	%rsi
	xorl	%eax, %eax
	cmpl	%ecx, %esi
	jle	.LBB8_7
# BB#4:                                 # %.lr.ph
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rdx), %rcx
	movzbl	(%rcx,%rsi), %ecx
	movb	%cl, (%rdi,%rax)
	incq	%rax
	decq	%rsi
	movslq	48(%rdx), %rcx
	cmpq	%rcx, %rsi
	jg	.LBB8_5
# BB#6:                                 # %._crit_edge.loopexit
	movl	32(%rdx), %r8d
.LBB8_7:                                # %._crit_edge
	incl	%ecx
	cmpl	%ecx, %r8d
	jle	.LBB8_9
# BB#8:
	movslq	%r8d, %rcx
	leaq	-1(%rcx), %rsi
	movl	%esi, 48(%rdx)
	movl	$8, 52(%rdx)
	movq	24(%rdx), %rdx
	movb	$0, -1(%rdx,%rcx)
.LBB8_9:                                # %empty_buffer.exit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end8:
	.size	copy_buffer, .Lfunc_end8-copy_buffer
	.cfi_endproc

	.globl	init_bit_stream_w
	.p2align	4, 0x90
	.type	init_bit_stream_w,@function
init_bit_stream_w:                      # @init_bit_stream_w
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$16384, %edi            # imm = 0x4000
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB9_2
# BB#1:                                 # %alloc_buffer.exit
	xorl	%esi, %esi
	movl	$16384, %edx            # imm = 0x4000
	movq	%r14, %rdi
	callq	memset
	movq	%r14, 24(%rbx)
	movl	$16384, 32(%rbx)        # imm = 0x4000
	movl	$16383, 48(%rbx)        # imm = 0x3FFF
	movl	$8, 52(%rbx)
	movq	$0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB9_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	movl	$.L.str.10, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end9:
	.size	init_bit_stream_w, .Lfunc_end9-init_bit_stream_w
	.cfi_endproc

	.globl	alloc_buffer
	.p2align	4, 0x90
	.type	alloc_buffer,@function
alloc_buffer:                           # @alloc_buffer
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	%esi, %r15
	movq	%r15, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB10_2
# BB#1:                                 # %mem_alloc.exit
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	memset
	movq	%rbx, 24(%r14)
	movl	%r15d, 32(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB10_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	movl	$.L.str.10, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end10:
	.size	alloc_buffer, .Lfunc_end10-alloc_buffer
	.cfi_endproc

	.globl	desalloc_buffer
	.p2align	4, 0x90
	.type	desalloc_buffer,@function
desalloc_buffer:                        # @desalloc_buffer
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end11:
	.size	desalloc_buffer, .Lfunc_end11-desalloc_buffer
	.cfi_endproc

	.globl	putbits
	.p2align	4, 0x90
	.type	putbits,@function
putbits:                                # @putbits
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -32
.Lcfi39:
	.cfi_offset %r14, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %r14d
	movq	%rdi, %rbp
	cmpl	$33, %ebx
	jge	.LBB12_1
# BB#2:
	movslq	%ebx, %rax
	addq	%rax, 40(%rbp)
	testl	%ebx, %ebx
	jg	.LBB12_4
	jmp	.LBB12_6
.LBB12_1:                               # %.thread
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	movl	$32, %edx
	xorl	%eax, %eax
	callq	fprintf
	movslq	%ebx, %rax
	addq	%rax, 40(%rbp)
	jmp	.LBB12_4
.LBB12_3:                               #   in Loop: Header=BB12_4 Depth=1
	testl	%ebx, %ebx
	jle	.LBB12_6
.LBB12_4:                               # =>This Inner Loop Header: Depth=1
	movl	52(%rbp), %eax
	cmpl	%eax, %ebx
	movl	%eax, %edx
	cmovlel	%ebx, %edx
	subl	%edx, %ebx
	movl	%r14d, %esi
	movl	%ebx, %ecx
	shrl	%cl, %esi
	movslq	%edx, %rdx
	andl	putmask(,%rdx,4), %esi
	subl	%edx, %eax
	movl	%eax, %ecx
	shll	%cl, %esi
	movq	24(%rbp), %rax
	movslq	48(%rbp), %rcx
	movzbl	(%rax,%rcx), %edi
	orl	%esi, %edi
	movb	%dil, (%rax,%rcx)
	movl	52(%rbp), %eax
	subl	%edx, %eax
	movl	%eax, 52(%rbp)
	jne	.LBB12_3
# BB#5:                                 #   in Loop: Header=BB12_4 Depth=1
	movl	$8, 52(%rbp)
	movslq	48(%rbp), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, 48(%rbp)
	movq	24(%rbp), %rcx
	movb	$0, -1(%rcx,%rax)
	testl	%ebx, %ebx
	jg	.LBB12_4
.LBB12_6:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end12:
	.size	putbits, .Lfunc_end12-putbits
	.cfi_endproc

	.type	s_freq_table,@object    # @s_freq_table
	.data
	.globl	s_freq_table
	.p2align	4
s_freq_table:
	.quad	4626899740918598861     # double 22.050000000000001
	.quad	4627448617123184640     # double 24
	.quad	4625196817309499392     # double 16
	.quad	0                       # double 0
	.quad	4631403340545969357     # double 44.100000000000001
	.quad	4631952216750555136     # double 48
	.quad	4629700416936869888     # double 32
	.quad	0                       # double 0
	.size	s_freq_table, 64

	.type	bitrate_table,@object   # @bitrate_table
	.globl	bitrate_table
	.p2align	4
bitrate_table:
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	16                      # 0x10
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	40                      # 0x28
	.long	48                      # 0x30
	.long	56                      # 0x38
	.long	64                      # 0x40
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	112                     # 0x70
	.long	128                     # 0x80
	.long	144                     # 0x90
	.long	160                     # 0xa0
	.long	0                       # 0x0
	.long	32                      # 0x20
	.long	40                      # 0x28
	.long	48                      # 0x30
	.long	56                      # 0x38
	.long	64                      # 0x40
	.long	80                      # 0x50
	.long	96                      # 0x60
	.long	112                     # 0x70
	.long	128                     # 0x80
	.long	160                     # 0xa0
	.long	192                     # 0xc0
	.long	224                     # 0xe0
	.long	256                     # 0x100
	.long	320                     # 0x140
	.size	bitrate_table, 120

	.type	NativeByteOrder,@object # @NativeByteOrder
	.bss
	.globl	NativeByteOrder
	.p2align	2
NativeByteOrder:
	.long	0                       # 0x0
	.size	NativeByteOrder, 4

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"MPEG1 samplerates(kHz): 32 44.1 48 \n"
	.size	.L.str.1, 37

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"bitrates(kbs): "
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%i "
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"MPEG2 samplerates(kHz): 16 22.05 24 \n"
	.size	.L.str.4, 38

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Bitrate %dkbs not legal for %iHz output sampling.\n"
	.size	.L.str.5, 51

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"SmpFrqIndex: %ldHz is not a legal sample rate\n"
	.size	.L.str.6, 47

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Unable to allocate %s\n"
	.size	.L.str.7, 23

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"ABCD"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"DCBA"
	.size	.L.str.9, 5

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"buffer"
	.size	.L.str.10, 7

	.type	putmask,@object         # @putmask
	.data
	.globl	putmask
	.p2align	4
putmask:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	15                      # 0xf
	.long	31                      # 0x1f
	.long	63                      # 0x3f
	.long	127                     # 0x7f
	.long	255                     # 0xff
	.size	putmask, 36

	.type	.L.str.11,@object       # @.str.11
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.11:
	.asciz	"Cannot read or write more than %d bits at a time.\n"
	.size	.L.str.11, 51


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
