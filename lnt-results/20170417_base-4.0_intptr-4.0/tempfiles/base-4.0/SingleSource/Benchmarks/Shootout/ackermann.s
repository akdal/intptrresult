	.text
	.file	"ackermann.bc"
	.globl	Ack
	.p2align	4, 0x90
	.type	Ack,@function
Ack:                                    # @Ack
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	testl	%edi, %edi
	je	.LBB0_5
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rdi), %ebx
	testl	%esi, %esi
	je	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	decl	%esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	Ack
	movl	%eax, %esi
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	movl	$1, %esi
.LBB0_4:                                # %tailrecurse.backedge
                                        #   in Loop: Header=BB0_1 Depth=1
	testl	%ebx, %ebx
	movl	%ebx, %edi
	jne	.LBB0_1
.LBB0_5:                                # %tailrecurse._crit_edge
	incl	%esi
	movl	%esi, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	Ack, .Lfunc_end0-Ack
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movl	$8, %ebx
	cmpl	$2, %edi
	jne	.LBB1_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
.LBB1_2:
	movl	$3, %edi
	movl	%ebx, %esi
	callq	Ack
	movl	%eax, %ecx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%ecx, %edx
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Ack(3,%d): %d\n"
	.size	.L.str, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
