	.text
	.file	"io.bc"
	.globl	logo
	.p2align	4, 0x90
	.type	logo,@function
logo:                                   # @logo
	.cfi_startproc
# BB#0:                                 # %.lr.ph.i
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$104, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 128
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$40, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	%rsp, %rbx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movq	%rbx, %rdi
	callq	strlen
	movl	$79, %ecx
	subl	%eax, %ecx
	cmpl	$-1, %ecx
	movq	stdout(%rip), %rsi
	jl	.LBB0_3
# BB#1:                                 # %.lr.ph.i4.preheader
	movl	%ecx, %ebx
	shrl	$31, %ebx
	addl	%ecx, %ebx
	sarl	%ebx
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph.i4
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	incl	%ebp
	cmpl	%ebx, %ebp
	jl	.LBB0_2
.LBB0_3:                                # %center_print.exit7
	movq	%rsp, %rdi
	callq	fputs
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$.Lstr.1, %edi
	callq	puts
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	logo, .Lfunc_end0-logo
	.cfi_endproc

	.globl	border_print
	.p2align	4, 0x90
	.type	border_print,@function
border_print:                           # @border_print
	.cfi_startproc
# BB#0:
	movl	$.Lstr.1, %edi
	jmp	puts                    # TAILCALL
.Lfunc_end1:
	.size	border_print, .Lfunc_end1-border_print
	.cfi_endproc

	.globl	center_print
	.p2align	4, 0x90
	.type	center_print,@function
center_print:                           # @center_print
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	callq	strlen
	subl	%eax, %ebx
	cmpl	$-1, %ebx
	movq	stdout(%rip), %rsi
	jl	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	movl	%ebx, %ebp
	shrl	$31, %ebp
	addl	%ebx, %ebp
	sarl	%ebp
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	callq	fputc
	movq	stdout(%rip), %rsi
	incl	%ebx
	cmpl	%ebp, %ebx
	jl	.LBB2_2
.LBB2_3:                                # %._crit_edge
	movq	%r14, %rdi
	callq	fputs
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	fputc                   # TAILCALL
.Lfunc_end2:
	.size	center_print, .Lfunc_end2-center_print
	.cfi_endproc

	.globl	print_results
	.p2align	4, 0x90
	.type	print_results,@function
print_results:                          # @print_results
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	testl	%edi, %edi
	je	.LBB3_1
# BB#4:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB3_1:
	leaq	32(%rsp), %r14
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$37, %ebp
	movq	stdout(%rip), %rax
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rax, %rsi
	callq	fputc
	movq	stdout(%rip), %rax
	decl	%ebp
	jne	.LBB3_2
# BB#3:                                 # %center_print.exit
	movl	$.L.str.5, %edi
	movl	$7, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$.Lstr.1, %edi
	callq	puts
	movl	(%r14), %esi
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movl	$.Lstr.1, %edi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	puts                    # TAILCALL
.Lfunc_end3:
	.size	print_results, .Lfunc_end3-print_results
	.cfi_endproc

	.globl	print_inputs
	.p2align	4, 0x90
	.type	print_inputs,@function
print_inputs:                           # @print_inputs
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 80
.Lcfi22:
	.cfi_offset %rbx, -40
.Lcfi23:
	.cfi_offset %r12, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	movl	%esi, %ebx
	leaq	80(%rsp), %r12
	movq	112(%rsp), %rax
	movq	%rax, 32(%rsp)
	movaps	80(%rsp), %xmm0
	movaps	96(%rsp), %xmm1
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	callq	estimate_mem_usage
	movq	%rax, %r14
	movq	%r14, %r15
	movl	%ebx, %edi
	callq	logo
	movl	$34, %ebx
	movq	stdout(%rip), %rax
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rax, %rsi
	callq	fputc
	movq	stdout(%rip), %rax
	decl	%ebx
	jne	.LBB4_1
# BB#2:                                 # %center_print.exit
	movl	$.L.str.8, %edi
	movl	$13, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.L.str.10, %edi
	movl	$12, %esi
	xorl	%eax, %eax
	callq	printf
	movq	32(%r12), %rsi
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%r12), %rsi
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%r12), %rbx
	cmpq	$999, %rbx              # imm = 0x3E7
	jg	.LBB4_4
# BB#3:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	jmp	.LBB4_9
.LBB4_4:
	cmpq	$999999, %rbx           # imm = 0xF423F
	jg	.LBB4_6
# BB#5:
	movq	%rbx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rcx # imm = 0x20C49BA5E353F7CF
	mulq	%rcx
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movq	%rbx, %rdx
	callq	printf
	jmp	.LBB4_9
.LBB4_6:
	cmpq	$999999999, %rbx        # imm = 0x3B9AC9FF
	jg	.LBB4_8
# BB#7:
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	movq	%rbx, %rax
	mulq	%rcx
	movq	%rdx, %rcx
	shrq	$18, %rcx
	imulq	$1000000, %rcx, %rdx    # imm = 0xF4240
	movq	%rbx, %rax
	subq	%rdx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rsi # imm = 0x20C49BA5E353F7CF
	mulq	%rsi
	movq	%rdx, %r8
	shrq	$4, %r8
	movq	%rbx, %rax
	shrq	$3, %rax
	mulq	%rsi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movq	%r8, %rdx
	movq	%rbx, %rcx
	callq	printf
	jmp	.LBB4_9
.LBB4_8:
	movq	%rbx, %rax
	shrq	$9, %rax
	movabsq	$19342813113834067, %rcx # imm = 0x44B82FA09B5A53
	mulq	%rcx
	movq	%rdx, %rsi
	shrq	$11, %rsi
	imulq	$1000000000, %rsi, %rcx # imm = 0x3B9ACA00
	movq	%rbx, %rax
	subq	%rcx, %rax
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	mulq	%rcx
	movq	%rdx, %r8
	shrq	$18, %r8
	movq	%rbx, %rax
	mulq	%rcx
	shrq	$18, %rdx
	imulq	$1000000, %rdx, %rcx    # imm = 0xF4240
	movq	%rbx, %rax
	subq	%rcx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rdi # imm = 0x20C49BA5E353F7CF
	mulq	%rdi
	movq	%rdx, %rcx
	shrq	$4, %rcx
	movq	%rbx, %rax
	shrq	$3, %rax
	mulq	%rdi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movq	%r8, %rdx
	movq	%rbx, %r8
	callq	printf
.LBB4_9:                                # %fancy_int.exit
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%r12), %rbx
	imulq	8(%r12), %rbx
	cmpq	$999, %rbx              # imm = 0x3E7
	jg	.LBB4_11
# BB#10:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	jmp	.LBB4_16
.LBB4_11:
	cmpq	$999999, %rbx           # imm = 0xF423F
	jg	.LBB4_13
# BB#12:
	movq	%rbx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rcx # imm = 0x20C49BA5E353F7CF
	mulq	%rcx
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movq	%rbx, %rdx
	callq	printf
	jmp	.LBB4_16
.LBB4_13:
	cmpq	$999999999, %rbx        # imm = 0x3B9AC9FF
	jg	.LBB4_15
# BB#14:
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	movq	%rbx, %rax
	mulq	%rcx
	movq	%rdx, %rcx
	shrq	$18, %rcx
	imulq	$1000000, %rcx, %rdx    # imm = 0xF4240
	movq	%rbx, %rax
	subq	%rdx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rsi # imm = 0x20C49BA5E353F7CF
	mulq	%rsi
	movq	%rdx, %r8
	shrq	$4, %r8
	movq	%rbx, %rax
	shrq	$3, %rax
	mulq	%rsi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movq	%r8, %rdx
	movq	%rbx, %rcx
	callq	printf
	jmp	.LBB4_16
.LBB4_15:
	movq	%rbx, %rax
	shrq	$9, %rax
	movabsq	$19342813113834067, %rcx # imm = 0x44B82FA09B5A53
	mulq	%rcx
	movq	%rdx, %rsi
	shrq	$11, %rsi
	imulq	$1000000000, %rsi, %rcx # imm = 0x3B9ACA00
	movq	%rbx, %rax
	subq	%rcx, %rax
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	mulq	%rcx
	movq	%rdx, %r8
	shrq	$18, %r8
	movq	%rbx, %rax
	mulq	%rcx
	shrq	$18, %rdx
	imulq	$1000000, %rdx, %rcx    # imm = 0xF4240
	movq	%rbx, %rax
	subq	%rcx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rdi # imm = 0x20C49BA5E353F7CF
	mulq	%rdi
	movq	%rdx, %rcx
	shrq	$4, %rcx
	movq	%rbx, %rax
	shrq	$3, %rax
	mulq	%rdi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movq	%r8, %rdx
	movq	%rbx, %r8
	callq	printf
.LBB4_16:                               # %fancy_int.exit3
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movslq	24(%r12), %rbx
	cmpq	$999, %rbx              # imm = 0x3E7
	jg	.LBB4_18
# BB#17:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	jmp	.LBB4_23
.LBB4_18:
	cmpl	$999999, %ebx           # imm = 0xF423F
	jg	.LBB4_20
# BB#19:
	movq	%rbx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rcx # imm = 0x20C49BA5E353F7CF
	mulq	%rcx
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movq	%rbx, %rdx
	callq	printf
	jmp	.LBB4_23
.LBB4_20:
	cmpl	$999999999, %ebx        # imm = 0x3B9AC9FF
	jg	.LBB4_22
# BB#21:
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	movq	%rbx, %rax
	mulq	%rcx
	movq	%rdx, %rcx
	shrq	$18, %rcx
	imulq	$1000000, %rcx, %rdx    # imm = 0xF4240
	movq	%rbx, %rax
	subq	%rdx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rsi # imm = 0x20C49BA5E353F7CF
	mulq	%rsi
	movq	%rdx, %r8
	shrq	$4, %r8
	movq	%rbx, %rax
	shrq	$3, %rax
	mulq	%rsi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movq	%r8, %rdx
	movq	%rbx, %rcx
	callq	printf
	jmp	.LBB4_23
.LBB4_22:
	movq	%rbx, %rax
	shrq	$9, %rax
	movabsq	$19342813113834067, %rcx # imm = 0x44B82FA09B5A53
	mulq	%rcx
	movq	%rdx, %rsi
	shrq	$11, %rsi
	imulq	$1000000000, %rsi, %rcx # imm = 0x3B9ACA00
	movq	%rbx, %rax
	subq	%rcx, %rax
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	mulq	%rcx
	movq	%rdx, %r8
	shrq	$18, %r8
	movq	%rbx, %rax
	mulq	%rcx
	shrq	$18, %rdx
	imulq	$1000000, %rdx, %rcx    # imm = 0xF4240
	movq	%rbx, %rax
	subq	%rcx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rdi # imm = 0x20C49BA5E353F7CF
	mulq	%rdi
	movq	%rdx, %rcx
	shrq	$4, %rcx
	movq	%rbx, %rax
	shrq	$3, %rax
	mulq	%rdi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movq	%r8, %rdx
	movq	%rbx, %r8
	callq	printf
.LBB4_23:                               # %fancy_int.exit4
	movl	(%r12), %esi
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	printf
	shlq	$32, %r14
	movslq	%r15d, %rbx
	movabsq	$4294967295999, %rax    # imm = 0x3E7FFFFFFFF
	cmpq	%rax, %r14
	jg	.LBB4_25
# BB#24:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	jmp	.LBB4_30
.LBB4_25:
	movabsq	$4294967295999999, %rax # imm = 0xF423FFFFFFFFF
	cmpq	%rax, %r14
	jg	.LBB4_27
# BB#26:
	movq	%rbx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rcx # imm = 0x20C49BA5E353F7CF
	mulq	%rcx
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movq	%rbx, %rdx
	callq	printf
	jmp	.LBB4_30
.LBB4_27:
	movabsq	$4294967295999999999, %rax # imm = 0x3B9AC9FFFFFFFFFF
	cmpq	%rax, %r14
	jg	.LBB4_29
# BB#28:
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	movq	%rbx, %rax
	mulq	%rcx
	movq	%rdx, %rcx
	shrq	$18, %rcx
	imulq	$1000000, %rcx, %rdx    # imm = 0xF4240
	movq	%rbx, %rax
	subq	%rdx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rsi # imm = 0x20C49BA5E353F7CF
	mulq	%rsi
	movq	%rdx, %r8
	shrq	$4, %r8
	movq	%rbx, %rax
	shrq	$3, %rax
	mulq	%rsi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movq	%r8, %rdx
	movq	%rbx, %rcx
	callq	printf
	jmp	.LBB4_30
.LBB4_29:
	movq	%rbx, %rax
	shrq	$9, %rax
	movabsq	$19342813113834067, %rcx # imm = 0x44B82FA09B5A53
	mulq	%rcx
	movq	%rdx, %rsi
	shrq	$11, %rsi
	imulq	$1000000000, %rsi, %rcx # imm = 0x3B9ACA00
	movq	%rbx, %rax
	subq	%rcx, %rax
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	mulq	%rcx
	movq	%rdx, %r8
	shrq	$18, %r8
	movq	%rbx, %rax
	mulq	%rcx
	shrq	$18, %rdx
	imulq	$1000000, %rdx, %rcx    # imm = 0xF4240
	movq	%rbx, %rax
	subq	%rcx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rdi # imm = 0x20C49BA5E353F7CF
	mulq	%rdi
	movq	%rdx, %rcx
	shrq	$4, %rcx
	movq	%rbx, %rax
	shrq	$3, %rax
	mulq	%rdi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %rbx
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movq	%r8, %rdx
	movq	%rbx, %r8
	callq	printf
.LBB4_30:                               # %fancy_int.exit5
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$33, %ebx
	movq	stdout(%rip), %rax
	.p2align	4, 0x90
.LBB4_31:                               # %.lr.ph.i9
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%rax, %rsi
	callq	fputc
	movq	stdout(%rip), %rax
	decl	%ebx
	jne	.LBB4_31
# BB#32:                                # %center_print.exit12
	movl	$.L.str.18, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$.Lstr.1, %edi
	callq	puts
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	print_inputs, .Lfunc_end4-print_inputs
	.cfi_endproc

	.globl	fancy_int
	.p2align	4, 0x90
	.type	fancy_int,@function
fancy_int:                              # @fancy_int
	.cfi_startproc
# BB#0:
	movq	%rdi, %r8
	cmpq	$999, %r8               # imm = 0x3E7
	jg	.LBB5_1
# BB#4:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movq	%r8, %rsi
	jmp	printf                  # TAILCALL
.LBB5_1:
	cmpq	$999999, %r8            # imm = 0xF423F
	jg	.LBB5_2
# BB#5:
	movq	%r8, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rcx # imm = 0x20C49BA5E353F7CF
	mulq	%rcx
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %r8
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movq	%r8, %rdx
	jmp	printf                  # TAILCALL
.LBB5_2:
	cmpq	$999999999, %r8         # imm = 0x3B9AC9FF
	jg	.LBB5_3
# BB#6:
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	movq	%r8, %rax
	mulq	%rcx
	movq	%rdx, %rcx
	shrq	$18, %rcx
	imulq	$1000000, %rcx, %rdx    # imm = 0xF4240
	movq	%r8, %rax
	subq	%rdx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rsi # imm = 0x20C49BA5E353F7CF
	mulq	%rsi
	movq	%rdx, %r9
	shrq	$4, %r9
	movq	%r8, %rax
	shrq	$3, %rax
	mulq	%rsi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %r8
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movq	%r9, %rdx
	movq	%r8, %rcx
	jmp	printf                  # TAILCALL
.LBB5_3:
	movq	%r8, %rax
	shrq	$9, %rax
	movabsq	$19342813113834067, %rcx # imm = 0x44B82FA09B5A53
	mulq	%rcx
	movq	%rdx, %rsi
	shrq	$11, %rsi
	imulq	$1000000000, %rsi, %rcx # imm = 0x3B9ACA00
	movq	%r8, %rax
	subq	%rcx, %rax
	movabsq	$4835703278458516699, %rcx # imm = 0x431BDE82D7B634DB
	mulq	%rcx
	movq	%rdx, %r9
	shrq	$18, %r9
	movq	%r8, %rax
	mulq	%rcx
	shrq	$18, %rdx
	imulq	$1000000, %rdx, %rcx    # imm = 0xF4240
	movq	%r8, %rax
	subq	%rcx, %rax
	shrq	$3, %rax
	movabsq	$2361183241434822607, %rdi # imm = 0x20C49BA5E353F7CF
	mulq	%rdi
	movq	%rdx, %rcx
	shrq	$4, %rcx
	movq	%r8, %rax
	shrq	$3, %rax
	mulq	%rdi
	shrq	$4, %rdx
	imulq	$1000, %rdx, %rax       # imm = 0x3E8
	subq	%rax, %r8
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	movq	%r9, %rdx
	jmp	printf                  # TAILCALL
.Lfunc_end5:
	.size	fancy_int, .Lfunc_end5-fancy_int
	.cfi_endproc

	.globl	print_CLI_error
	.p2align	4, 0x90
	.type	print_CLI_error,@function
print_CLI_error:                        # @print_CLI_error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.Lstr.6, %edi
	callq	puts
	movl	$.Lstr.7, %edi
	callq	puts
	movl	$.Lstr.8, %edi
	callq	puts
	movl	$.Lstr.9, %edi
	callq	puts
	movl	$.Lstr.10, %edi
	callq	puts
	movl	$4, %edi
	callq	exit
.Lfunc_end6:
	.size	print_CLI_error, .Lfunc_end6-print_CLI_error
	.cfi_endproc

	.globl	read_CLI
	.p2align	4, 0x90
	.type	read_CLI,@function
read_CLI:                               # @read_CLI
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 80
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movl	%esi, %r15d
	movq	%rdi, %r14
	movl	$6, %edi
	callq	malloc
	movq	%rax, %rbx
	movl	$1735549292, (%rbx)     # imm = 0x6772616C
	movw	$101, 4(%rbx)
	cmpl	$2, %r15d
	jl	.LBB7_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$1, %r13d
	movl	$15000000, %eax         # imm = 0xE4E1C0
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$11303, %eax            # imm = 0x2C27
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%r12d, %rax
	movq	(%rbp,%rax,8), %rax
	cmpb	$45, (%rax)
	jne	.LBB7_37
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	cmpb	$116, 1(%rax)
	jne	.LBB7_9
# BB#5:                                 #   in Loop: Header=BB7_3 Depth=1
	cmpb	$0, 2(%rax)
	je	.LBB7_6
.LBB7_9:                                #   in Loop: Header=BB7_3 Depth=1
	cmpb	$103, 1(%rax)
	jne	.LBB7_14
# BB#10:                                #   in Loop: Header=BB7_3 Depth=1
	cmpb	$0, 2(%rax)
	je	.LBB7_11
.LBB7_14:                               #   in Loop: Header=BB7_3 Depth=1
	cmpb	$108, 1(%rax)
	jne	.LBB7_19
# BB#15:                                #   in Loop: Header=BB7_3 Depth=1
	cmpb	$0, 2(%rax)
	je	.LBB7_16
.LBB7_19:                               #   in Loop: Header=BB7_3 Depth=1
	cmpb	$115, 1(%rax)
	jne	.LBB7_37
# BB#20:                                #   in Loop: Header=BB7_3 Depth=1
	cmpb	$0, 2(%rax)
	jne	.LBB7_37
# BB#21:                                #   in Loop: Header=BB7_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jge	.LBB7_37
# BB#22:                                #   in Loop: Header=BB7_3 Depth=1
	movslq	%r12d, %rax
	movq	(%rbp,%rax,8), %rbx
	jmp	.LBB7_23
.LBB7_6:                                #   in Loop: Header=BB7_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jge	.LBB7_8
# BB#7:                                 #   in Loop: Header=BB7_3 Depth=1
	movslq	%r12d, %rax
	movq	(%rbp,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r13
	jmp	.LBB7_23
.LBB7_11:                               #   in Loop: Header=BB7_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jge	.LBB7_13
# BB#12:                                #   in Loop: Header=BB7_3 Depth=1
	movslq	%r12d, %rax
	movq	(%rbp,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB7_23
.LBB7_16:                               #   in Loop: Header=BB7_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jge	.LBB7_18
# BB#17:                                #   in Loop: Header=BB7_3 Depth=1
	movslq	%r12d, %rax
	movq	(%rbp,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB7_23:                               #   in Loop: Header=BB7_3 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jl	.LBB7_3
# BB#24:                                # %._crit_edge
	testl	%r13d, %r13d
	jle	.LBB7_37
# BB#25:
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%rbp, %rbp
	movq	(%rsp), %r15            # 8-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	jle	.LBB7_37
# BB#26:
	testl	%r15d, %r15d
	jg	.LBB7_27
	jmp	.LBB7_37
.LBB7_1:
	xorl	%r12d, %r12d
	movl	$1, %r13d
	movl	$15000000, %r15d        # imm = 0xE4E1C0
	movl	$11303, %ebp            # imm = 0x2C27
.LBB7_27:                               # %.thread235
	movl	$.L.str.36, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB7_28
# BB#29:
	movl	$.L.str.37, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB7_32
# BB#30:
	movl	$.L.str.38, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB7_32
# BB#31:
	movl	$.L.str.39, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	testl	%eax, %eax
	jne	.LBB7_37
.LBB7_32:
	movl	$.L.str.38, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	orl	%r12d, %eax
	je	.LBB7_33
# BB#34:
	movl	$.L.str.39, %esi
	movq	%rbx, %rdi
	callq	strcasecmp
	orl	%r12d, %eax
	movl	$501578, %eax           # imm = 0x7A74A
	cmoveq	%rax, %rbp
	jmp	.LBB7_35
.LBB7_28:
	movl	$68, %eax
	jmp	.LBB7_36
.LBB7_33:
	movl	$238847, %ebp           # imm = 0x3A4FF
.LBB7_35:
	movl	$355, %eax              # imm = 0x163
.LBB7_36:
	movl	%r13d, (%r14)
	movq	%rax, 8(%r14)
	movq	%rbp, 16(%r14)
	movl	%r15d, 24(%r14)
	movq	%rbx, 32(%r14)
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_37:
	callq	print_CLI_error
.LBB7_8:
	callq	print_CLI_error
.LBB7_13:
	callq	print_CLI_error
.LBB7_18:
	callq	print_CLI_error
.Lfunc_end7:
	.size	read_CLI, .Lfunc_end7-read_CLI
	.cfi_endproc

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"Developed at Argonne National Laboratory"
	.size	.L.str.1, 41

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Version: %d"
	.size	.L.str.2, 12

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"RESULTS"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Threads:     %d\n"
	.size	.L.str.6, 17

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Verification checksum: %llu\n"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"INPUT SUMMARY"
	.size	.L.str.8, 14

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Materials:                    %d\n"
	.size	.L.str.10, 34

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"H-M Benchmark Size:           %s\n"
	.size	.L.str.11, 34

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Total Nuclides:               %ld\n"
	.size	.L.str.12, 35

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Gridpoints (per Nuclide):     "
	.size	.L.str.13, 31

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Unionized Energy Gridpoints:  "
	.size	.L.str.14, 31

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"XS Lookups:                   "
	.size	.L.str.15, 31

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Threads:                      %d\n"
	.size	.L.str.16, 34

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Est. Memory Usage (MB):       "
	.size	.L.str.17, 31

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"INITIALIZATION"
	.size	.L.str.18, 15

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"%ld\n"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%ld,%03ld\n"
	.size	.L.str.21, 11

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%ld,%03ld,%03ld\n"
	.size	.L.str.22, 17

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"%ld,%03ld,%03ld,%03ld\n"
	.size	.L.str.23, 23

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"-t"
	.size	.L.str.32, 3

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"-g"
	.size	.L.str.33, 3

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"-l"
	.size	.L.str.34, 3

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"-s"
	.size	.L.str.35, 3

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"small"
	.size	.L.str.36, 6

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"large"
	.size	.L.str.37, 6

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"XL"
	.size	.L.str.38, 3

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"XXL"
	.size	.L.str.39, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"                   __   __ ___________                 _                        \n                   \\ \\ / //  ___| ___ \\               | |                       \n                    \\ V / \\ `--.| |_/ / ___ _ __   ___| |__                     \n                    /   \\  `--. \\ ___ \\/ _ \\ '_ \\ / __| '_ \\                    \n                   / /^\\ \\/\\__/ / |_/ /  __/ | | | (__| | | |                   \n                   \\/   \\/\\____/\\____/ \\___|_| |_|\\___|_| |_|                   \n"
	.size	.Lstr, 487

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"================================================================================"
	.size	.Lstr.1, 81

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"Verification Mode:            on"
	.size	.Lstr.2, 33

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"Usage: ./XSBench <options>"
	.size	.Lstr.3, 27

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"Options include:"
	.size	.Lstr.4, 17

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"  -t <threads>     Number of OpenMP threads to run"
	.size	.Lstr.5, 51

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"  -s <size>        Size of H-M Benchmark to run (small, large, XL, XXL)"
	.size	.Lstr.6, 72

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"  -g <gridpoints>  Number of gridpoints per nuclide (overrides -s defaults)"
	.size	.Lstr.7, 76

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"  -l <lookups>     Number of Cross-section (XS) lookups"
	.size	.Lstr.8, 56

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"Default is equivalent to: -s large -l 15000000"
	.size	.Lstr.9, 47

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	"See readme for full description of default run values"
	.size	.Lstr.10, 54


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
