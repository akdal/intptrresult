	.text
	.file	"ParseProperties.bc"
	.globl	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	.p2align	4, 0x90
	.type	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj,@function
_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj: # @_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movzwl	(%rsi), %eax
	movl	$-2147024809, %ebp      # imm = 0x80070057
	testw	%ax, %ax
	je	.LBB0_4
# BB#1:
	movzwl	%ax, %eax
	cmpl	$19, %eax
	jne	.LBB0_8
# BB#2:
	cmpl	$0, 8(%rbx)
	jne	.LBB0_8
# BB#3:
	movl	8(%rsi), %eax
	movl	%eax, (%r14)
	jmp	.LBB0_7
.LBB0_4:
	cmpl	$0, 8(%rbx)
	je	.LBB0_7
# BB#5:
	movq	(%rbx), %r15
	movq	%rsp, %rsi
	movq	%r15, %rdi
	callq	_Z21ConvertStringToUInt64PKwPS0_
	movq	(%rsp), %rcx
	subq	%r15, %rcx
	sarq	$2, %rcx
	movslq	8(%rbx), %rdx
	cmpq	%rdx, %rcx
	jne	.LBB0_8
# BB#6:                                 # %.critedge
	movl	%eax, (%r14)
.LBB0_7:
	xorl	%ebp, %ebp
.LBB0_8:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj, .Lfunc_end0-_Z14ParsePropValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	.cfi_endproc

	.globl	_Z24ParsePropDictionaryValueRK11CStringBaseIwERj
	.p2align	4, 0x90
	.type	_Z24ParsePropDictionaryValueRK11CStringBaseIwERj,@function
_Z24ParsePropDictionaryValueRK11CStringBaseIwERj: # @_Z24ParsePropDictionaryValueRK11CStringBaseIwERj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movslq	8(%rbp), %r15
	leaq	1(%r15), %rax
	testl	%eax, %eax
	je	.LBB1_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movl	$0, (%rbx)
	jmp	.LBB1_3
.LBB1_1:
	xorl	%ebx, %ebx
.LBB1_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%rbp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax,%rcx), %edx
	movl	%edx, (%rbx,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB1_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
.Ltmp0:
	movq	%rbx, %rdi
	callq	_Z13MyStringUpperPw
.Ltmp1:
# BB#6:                                 # %_ZN11CStringBaseIwE9MakeUpperEv.exit
.Ltmp3:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	_Z21ConvertStringToUInt64PKwPS0_
.Ltmp4:
# BB#7:
	movq	(%rsp), %rcx
	subq	%rbx, %rcx
	movq	%rcx, %rdx
	shrq	$2, %rdx
	testl	%edx, %edx
	movl	$-2147024809, %ebp      # imm = 0x80070057
	je	.LBB1_12
# BB#8:
	leal	1(%rdx), %esi
	cmpl	%esi, %r15d
	jg	.LBB1_12
# BB#9:
	cmpl	%edx, %r15d
	jne	.LBB1_13
# BB#10:
	cmpq	$31, %rax
	ja	.LBB1_12
# BB#11:
	movl	$1, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	%edx, (%r14)
	xorl	%ebp, %ebp
.LBB1_12:
	testq	%rbx, %rbx
	je	.LBB1_23
.LBB1_22:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB1_23:                               # %_ZN11CStringBaseIwED2Ev.exit30
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_13:
	shlq	$30, %rcx
	sarq	$30, %rcx
	movl	(%rbx,%rcx), %ecx
	movl	$-2147024809, %ebp      # imm = 0x80070057
	cmpl	$77, %ecx
	je	.LBB1_19
# BB#14:
	cmpl	$75, %ecx
	je	.LBB1_17
# BB#15:
	cmpl	$66, %ecx
	jne	.LBB1_22
# BB#16:
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB1_22
	jmp	.LBB1_21
.LBB1_19:
	cmpq	$4095, %rax             # imm = 0xFFF
	ja	.LBB1_22
# BB#20:
	shll	$20, %eax
	jmp	.LBB1_21
.LBB1_17:
	cmpq	$4194303, %rax          # imm = 0x3FFFFF
	ja	.LBB1_22
# BB#18:
	shll	$10, %eax
.LBB1_21:
	movl	%eax, (%r14)
	xorl	%ebp, %ebp
	jmp	.LBB1_22
.LBB1_28:
.Ltmp5:
	jmp	.LBB1_25
.LBB1_24:
.Ltmp2:
.LBB1_25:
	movq	%rax, %rbp
	testq	%rbx, %rbx
	je	.LBB1_27
# BB#26:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB1_27:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z24ParsePropDictionaryValueRK11CStringBaseIwERj, .Lfunc_end1-_Z24ParsePropDictionaryValueRK11CStringBaseIwERj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	.p2align	4, 0x90
	.type	_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj,@function
_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj: # @_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	cmpl	$0, 8(%rdi)
	je	.LBB2_1
# BB#13:
	movq	%r14, %rsi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_Z24ParsePropDictionaryValueRK11CStringBaseIwERj # TAILCALL
.LBB2_1:
	movzwl	(%rsi), %eax
	cmpl	$8, %eax
	je	.LBB2_5
# BB#2:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	movzwl	%ax, %eax
	cmpl	$19, %eax
	jne	.LBB2_11
# BB#3:
	movl	8(%rsi), %ecx
	cmpl	$31, %ecx
	ja	.LBB2_11
# BB#4:
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movl	%eax, (%r14)
	xorl	%ebp, %ebp
	jmp	.LBB2_11
.LBB2_5:
	movq	8(%rsi), %r12
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movl	$-1, %ebp
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB2_6
# BB#7:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	leal	1(%rbp), %eax
	movslq	%eax, %r15
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	%r15d, 12(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_8:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB2_8
# BB#9:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movl	%ebp, 8(%rsp)
.Ltmp6:
	movq	%rsp, %rdi
	movq	%r14, %rsi
	callq	_Z24ParsePropDictionaryValueRK11CStringBaseIwERj
	movl	%eax, %ebp
.Ltmp7:
# BB#10:                                # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB2_11:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_12:                               # %_ZN11CStringBaseIwED2Ev.exit14
.Ltmp8:
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj, .Lfunc_end2-_Z24ParsePropDictionaryValueRK11CStringBaseIwERK14tagPROPVARIANTRj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp6-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z12StringToBoolRK11CStringBaseIwERb
	.p2align	4, 0x90
	.type	_Z12StringToBoolRK11CStringBaseIwERb,@function
_Z12StringToBoolRK11CStringBaseIwERb:   # @_Z12StringToBoolRK11CStringBaseIwERb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movb	$1, %bpl
	cmpl	$0, 8(%rbx)
	movb	$1, %cl
	je	.LBB3_5
# BB#1:
	movq	(%rbx), %rdi
	movl	$.L.str, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	movb	$1, %cl
	je	.LBB3_5
# BB#2:
	movq	(%rbx), %rdi
	movl	$.L.str.1, %esi
	callq	_Z15MyStringComparePKwS0_
	testl	%eax, %eax
	movb	$1, %cl
	je	.LBB3_5
# BB#3:
	movq	(%rbx), %rdi
	movl	$.L.str.2, %esi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB3_4
# BB#6:
	movq	(%rbx), %rdi
	movl	$.L.str.3, %esi
	callq	_Z15MyStringComparePKwS0_
	xorl	%ecx, %ecx
	testl	%eax, %eax
	je	.LBB3_5
# BB#7:
	xorl	%ebp, %ebp
	jmp	.LBB3_8
.LBB3_4:
	xorl	%ecx, %ecx
.LBB3_5:                                # %.sink.split
	movb	%cl, (%r14)
.LBB3_8:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_Z12StringToBoolRK11CStringBaseIwERb, .Lfunc_end3-_Z12StringToBoolRK11CStringBaseIwERb
	.cfi_endproc

	.globl	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_Z15SetBoolPropertyRbRK14tagPROPVARIANT,@function
_Z15SetBoolPropertyRbRK14tagPROPVARIANT: # @_Z15SetBoolPropertyRbRK14tagPROPVARIANT
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movzwl	(%rsi), %eax
	cmpl	$11, %eax
	je	.LBB4_21
# BB#1:
	movzwl	%ax, %ecx
	cmpl	$8, %ecx
	je	.LBB4_4
# BB#2:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	testw	%ax, %ax
	jne	.LBB4_23
# BB#3:
	movb	$1, (%r14)
	jmp	.LBB4_22
.LBB4_21:
	cmpw	$0, 8(%rsi)
	setne	(%r14)
.LBB4_22:
	xorl	%ebp, %ebp
	jmp	.LBB4_23
.LBB4_4:
	movq	8(%rsi), %rbp
	xorl	%eax, %eax
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	addq	%rcx, %rax
	cmpl	$0, (%rbp,%rbx,4)
	leaq	1(%rbx), %rbx
	jne	.LBB4_5
# BB#6:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	sarq	$32, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	movl	$0, (%r15)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_7:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB4_7
# BB#8:                                 # %_ZN11CStringBaseIwEC2EPKw.exit
	movb	$1, %bpl
	cmpl	$1, %ebx
	je	.LBB4_18
# BB#9:
.Ltmp9:
	movl	$.L.str, %esi
	movq	%r15, %rdi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp10:
# BB#10:                                # %.noexc
	testl	%eax, %eax
	je	.LBB4_18
# BB#11:
.Ltmp11:
	movl	$.L.str.1, %esi
	movq	%r15, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp12:
# BB#12:                                # %.noexc8
	testl	%eax, %eax
	je	.LBB4_18
# BB#13:
.Ltmp13:
	movl	$.L.str.2, %esi
	movq	%r15, %rdi
	callq	_Z21MyStringCompareNoCasePKwS0_
.Ltmp14:
# BB#14:                                # %.noexc9
	testl	%eax, %eax
	je	.LBB4_17
# BB#15:
.Ltmp15:
	movl	$.L.str.3, %esi
	movq	%r15, %rdi
	callq	_Z15MyStringComparePKwS0_
.Ltmp16:
# BB#16:                                # %.noexc10
	movl	$-2147024809, %ebp      # imm = 0x80070057
	testl	%eax, %eax
	jne	.LBB4_19
.LBB4_17:
	xorl	%ebp, %ebp
.LBB4_18:
	movb	%bpl, (%r14)
	xorl	%ebp, %ebp
.LBB4_19:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB4_23:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_20:                               # %_ZN11CStringBaseIwED2Ev.exit11
.Ltmp17:
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_Z15SetBoolPropertyRbRK14tagPROPVARIANT, .Lfunc_end4-_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp9-.Lfunc_begin2    #   Call between .Lfunc_begin2 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp9          #   Call between .Ltmp9 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Lfunc_end4-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z19ParseStringToUInt32RK11CStringBaseIwERj
	.p2align	4, 0x90
	.type	_Z19ParseStringToUInt32RK11CStringBaseIwERj,@function
_Z19ParseStringToUInt32RK11CStringBaseIwERj: # @_Z19ParseStringToUInt32RK11CStringBaseIwERj
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	(%rdi), %rbx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	_Z21ConvertStringToUInt64PKwPS0_
	movq	%rax, %rcx
	shrq	$32, %rcx
	je	.LBB5_2
# BB#1:
	movl	$0, (%r14)
	xorl	%eax, %eax
	jmp	.LBB5_3
.LBB5_2:
	movl	%eax, (%r14)
	movq	(%rsp), %rax
	subq	%rbx, %rax
	shrq	$2, %rax
.LBB5_3:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_Z19ParseStringToUInt32RK11CStringBaseIwERj, .Lfunc_end5-_Z19ParseStringToUInt32RK11CStringBaseIwERj
	.cfi_endproc

	.globl	_Z11ParseMtPropRK11CStringBaseIwERK14tagPROPVARIANTjRj
	.p2align	4, 0x90
	.type	_Z11ParseMtPropRK11CStringBaseIwERK14tagPROPVARIANTjRj,@function
_Z11ParseMtPropRK11CStringBaseIwERK14tagPROPVARIANTjRj: # @_Z11ParseMtPropRK11CStringBaseIwERK14tagPROPVARIANTjRj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 48
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebp
	movq	%rdi, %rbx
	cmpl	$0, 8(%rbx)
	je	.LBB6_5
# BB#1:
	movq	(%rbx), %rbp
	leaq	8(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_Z21ConvertStringToUInt64PKwPS0_
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	shrq	$32, %rdx
	movl	$0, %edx
	jne	.LBB6_3
# BB#2:
	movq	8(%rsp), %rdx
	subq	%rbp, %rdx
	shrq	$2, %rdx
	movl	%eax, %ecx
.LBB6_3:                                # %_Z19ParseStringToUInt32RK11CStringBaseIwERj.exit
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	8(%rbx), %edx
	jne	.LBB6_11
# BB#4:                                 # %.critedge
	movl	%ecx, (%r14)
	jmp	.LBB6_10
.LBB6_5:
	movzwl	(%rsi), %eax
	cmpl	$19, %eax
	jne	.LBB6_7
# BB#6:
	movl	8(%rsi), %eax
	jmp	.LBB6_9
.LBB6_7:
	leaq	7(%rsp), %rdi
	callq	_Z15SetBoolPropertyRbRK14tagPROPVARIANT
	testl	%eax, %eax
	jne	.LBB6_11
# BB#8:                                 # %.thread
	cmpb	$0, 7(%rsp)
	movl	$1, %eax
	cmovnel	%ebp, %eax
.LBB6_9:
	movl	%eax, (%r14)
.LBB6_10:
	xorl	%eax, %eax
.LBB6_11:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_Z11ParseMtPropRK11CStringBaseIwERK14tagPROPVARIANTjRj, .Lfunc_end6-_Z11ParseMtPropRK11CStringBaseIwERK14tagPROPVARIANTjRj
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	79                      # 0x4f
	.long	78                      # 0x4e
	.long	0                       # 0x0
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	43                      # 0x2b
	.long	0                       # 0x0
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
	.p2align	2
.L.str.2:
	.long	79                      # 0x4f
	.long	70                      # 0x46
	.long	70                      # 0x46
	.long	0                       # 0x0
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
	.p2align	2
.L.str.3:
	.long	45                      # 0x2d
	.long	0                       # 0x0
	.size	.L.str.3, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
