	.text
	.file	"7zIn.bc"
	.globl	_ZNK8NArchive3N7z7CFolder14CheckStructureEv
	.p2align	4, 0x90
	.type	_ZNK8NArchive3N7z7CFolder14CheckStructureEv,@function
_ZNK8NArchive3N7z7CFolder14CheckStructureEv: # @_ZNK8NArchive3N7z7CFolder14CheckStructureEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 288
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$32, 12(%r14)
	jg	.LBB0_65
# BB#1:
	movl	44(%r14), %ebx
	cmpl	$32, %ebx
	jg	.LBB0_65
# BB#2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rsp)
	movq	$1, 24(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, (%rsp)
	addl	76(%r14), %ebx
.Ltmp0:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp1:
# BB#3:                                 # %.noexc96
.Ltmp2:
	movq	%rsp, %rdi
	movl	%ebx, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp3:
# BB#4:                                 # %.noexc97
	testl	%ebx, %ebx
	jle	.LBB0_8
# BB#5:                                 # %.lr.ph.i93
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
.Ltmp5:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp6:
# BB#7:                                 # %.noexc98
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	16(%rsp), %rax
	movslq	12(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rsp)
	decl	%ebx
	jne	.LBB0_6
.LBB0_8:                                # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit99.preheader
	movslq	44(%r14), %rax
	testq	%rax, %rax
	jle	.LBB0_13
# BB#9:                                 # %.lr.ph140
	movq	48(%r14), %rcx
	movl	12(%rsp), %edx
	movq	16(%rsp), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_10:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rcx,%rdi,8), %rbp
	cmpl	%ebp, %edx
	jbe	.LBB0_32
# BB#11:                                # %_ZN8NArchive3N7zL20BoolVector_GetAndSetER13CRecordVectorIbEj.exit101
                                        #   in Loop: Header=BB0_10 Depth=1
	cmpb	$0, (%rsi,%rbp)
	movb	$1, (%rsi,%rbp)
	jne	.LBB0_32
# BB#12:                                # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit99
                                        #   in Loop: Header=BB0_10 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB0_10
.LBB0_13:                               # %.preheader112
	movslq	76(%r14), %rax
	testq	%rax, %rax
	jle	.LBB0_21
# BB#14:                                # %.lr.ph137
	movq	80(%r14), %rcx
	movl	12(%rsp), %edx
	movq	16(%rsp), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rcx,%rdi,4), %rbp
	cmpl	%ebp, %edx
	jbe	.LBB0_32
# BB#20:                                # %_ZN8NArchive3N7zL20BoolVector_GetAndSetER13CRecordVectorIbEj.exit103
                                        #   in Loop: Header=BB0_19 Depth=1
	cmpb	$0, (%rsi,%rbp)
	movb	$1, (%rsi,%rbp)
	jne	.LBB0_32
# BB#18:                                #   in Loop: Header=BB0_19 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB0_19
.LBB0_21:                               # %._crit_edge138
	movl	108(%r14), %ebx
.Ltmp8:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp9:
# BB#22:                                # %.noexc
.Ltmp10:
	movq	%rsp, %rdi
	movl	%ebx, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp11:
# BB#23:                                # %.noexc91
	testl	%ebx, %ebx
	jle	.LBB0_27
# BB#24:                                # %.lr.ph.i
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB0_25:                               # =>This Inner Loop Header: Depth=1
.Ltmp13:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp14:
# BB#26:                                # %.noexc92
                                        #   in Loop: Header=BB0_25 Depth=1
	movq	16(%rsp), %rax
	movslq	12(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rsp)
	decl	%ebx
	jne	.LBB0_25
.LBB0_27:                               # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit.preheader
	movslq	44(%r14), %rax
	testq	%rax, %rax
	jle	.LBB0_36
# BB#28:                                # %.lr.ph135
	movq	48(%r14), %rcx
	movl	12(%rsp), %edx
	movq	16(%rsp), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_30:                               # =>This Inner Loop Header: Depth=1
	movslq	4(%rcx,%rdi,8), %rbp
	cmpl	%ebp, %edx
	jbe	.LBB0_32
# BB#31:                                # %_ZN8NArchive3N7zL20BoolVector_GetAndSetER13CRecordVectorIbEj.exit
                                        #   in Loop: Header=BB0_30 Depth=1
	cmpb	$0, (%rsi,%rbp)
	movb	$1, (%rsi,%rbp)
	jne	.LBB0_32
# BB#29:                                # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit
                                        #   in Loop: Header=BB0_30 Depth=1
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB0_30
.LBB0_36:                               # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit._crit_edge
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm0, 176(%rsp)
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movups	%xmm0, 72(%rsp)
	movq	$4, 88(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 64(%rsp)
	movups	%xmm0, 40(%rsp)
	movq	$4, 56(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 32(%rsp)
	cmpl	$0, 12(%r14)
	jle	.LBB0_47
# BB#37:                                # %.lr.ph132
	xorl	%r13d, %r13d
	leaq	32(%rsp), %r15
	leaq	64(%rsp), %r12
	.p2align	4, 0x90
.LBB0_38:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_40 Depth 2
                                        #     Child Loop BB0_44 Depth 2
	movq	16(%r14), %rax
	movq	(%rax,%r13,8), %rbx
	cmpl	$0, 32(%rbx)
	je	.LBB0_42
# BB#39:                                # %.lr.ph124.preheader
                                        #   in Loop: Header=BB0_38 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_40:                               # %.lr.ph124
                                        #   Parent Loop BB0_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp18:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp19:
# BB#41:                                #   in Loop: Header=BB0_40 Depth=2
	movq	80(%rsp), %rax
	movslq	76(%rsp), %rcx
	movl	%r13d, (%rax,%rcx,4)
	incl	76(%rsp)
	incl	%ebp
	cmpl	32(%rbx), %ebp
	jb	.LBB0_40
.LBB0_42:                               # %.preheader106
                                        #   in Loop: Header=BB0_38 Depth=1
	cmpl	$0, 36(%rbx)
	je	.LBB0_46
# BB#43:                                # %.lr.ph126.preheader
                                        #   in Loop: Header=BB0_38 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_44:                               # %.lr.ph126
                                        #   Parent Loop BB0_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp21:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp22:
# BB#45:                                #   in Loop: Header=BB0_44 Depth=2
	movq	48(%rsp), %rax
	movslq	44(%rsp), %rcx
	movl	%r13d, (%rax,%rcx,4)
	incl	44(%rsp)
	incl	%ebp
	cmpl	36(%rbx), %ebp
	jb	.LBB0_44
.LBB0_46:                               # %._crit_edge127
                                        #   in Loop: Header=BB0_38 Depth=1
	incq	%r13
	movslq	12(%r14), %rax
	cmpq	%rax, %r13
	jl	.LBB0_38
.LBB0_47:                               # %.preheader105
	movslq	44(%r14), %rax
	testq	%rax, %rax
	jle	.LBB0_53
# BB#48:                                # %.lr.ph
	movq	48(%r14), %rdx
	movq	48(%rsp), %rsi
	movq	80(%rsp), %rdi
	testb	$1, %al
	jne	.LBB0_50
# BB#49:
	xorl	%ebx, %ebx
	cmpl	$1, %eax
	jne	.LBB0_52
	jmp	.LBB0_53
.LBB0_32:                               # %.critedge88
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.LBB0_65:
	xorl	%eax, %eax
.LBB0_66:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_50:
	movslq	4(%rdx), %rcx
	movb	(%rsi,%rcx,4), %cl
	movl	$1, %ebp
	shll	%cl, %ebp
	movslq	(%rdx), %rcx
	movslq	(%rdi,%rcx,4), %rcx
	orl	%ebp, 96(%rsp,%rcx,4)
	movl	$1, %ebx
	cmpl	$1, %eax
	je	.LBB0_53
	.p2align	4, 0x90
.LBB0_52:                               # =>This Inner Loop Header: Depth=1
	movslq	4(%rdx,%rbx,8), %rcx
	movzbl	(%rsi,%rcx,4), %ecx
	movl	$1, %ebp
	shll	%cl, %ebp
	movslq	(%rdx,%rbx,8), %rcx
	movslq	(%rdi,%rcx,4), %rcx
	orl	%ebp, 96(%rsp,%rcx,4)
	movslq	12(%rdx,%rbx,8), %rcx
	movzbl	(%rsi,%rcx,4), %ecx
	movl	$1, %ebp
	shll	%cl, %ebp
	movslq	8(%rdx,%rbx,8), %rcx
	movslq	(%rdi,%rcx,4), %rcx
	orl	%ebp, 96(%rsp,%rcx,4)
	addq	$2, %rbx
	cmpq	%rax, %rbx
	jl	.LBB0_52
.LBB0_53:                               # %._crit_edge
.Ltmp26:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp27:
# BB#54:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_55:                               # %.preheader104
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_56 Depth 2
	movl	96(%rsp,%rax,4), %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_56:                               #   Parent Loop BB0_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	btl	%edx, %ecx
	jae	.LBB0_58
# BB#57:                                #   in Loop: Header=BB0_56 Depth=2
	orl	96(%rsp,%rdx,4), %ecx
	movl	%ecx, 96(%rsp,%rax,4)
.LBB0_58:                               #   in Loop: Header=BB0_56 Depth=2
	leaq	1(%rdx), %rsi
	btl	%esi, %ecx
	jae	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_56 Depth=2
	orl	100(%rsp,%rdx,4), %ecx
	movl	%ecx, 96(%rsp,%rax,4)
.LBB0_60:                               #   in Loop: Header=BB0_56 Depth=2
	incq	%rsi
	cmpq	$32, %rsi
	movq	%rsi, %rdx
	jne	.LBB0_56
# BB#61:                                #   in Loop: Header=BB0_55 Depth=1
	incq	%rax
	cmpq	$32, %rax
	jne	.LBB0_55
# BB#62:                                # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_63:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	96(%rsp,%rax,4), %ecx
	btl	%eax, %ecx
	jb	.LBB0_65
# BB#64:                                # %.preheader.1160
                                        #   in Loop: Header=BB0_63 Depth=1
	leaq	1(%rax), %rcx
	movl	100(%rsp,%rax,4), %edx
	btl	%ecx, %edx
	jb	.LBB0_65
# BB#73:                                # %.preheader.2161
                                        #   in Loop: Header=BB0_63 Depth=1
	incq	%rcx
	movl	104(%rsp,%rax,4), %edx
	btl	%ecx, %edx
	jb	.LBB0_65
# BB#74:                                # %.preheader.3162
                                        #   in Loop: Header=BB0_63 Depth=1
	incq	%rcx
	movl	108(%rsp,%rax,4), %eax
	btl	%ecx, %eax
	jb	.LBB0_65
# BB#75:                                #   in Loop: Header=BB0_63 Depth=1
	incq	%rcx
	cmpq	$32, %rcx
	movq	%rcx, %rax
	jl	.LBB0_63
# BB#76:
	movb	$1, %al
	jmp	.LBB0_66
.LBB0_70:
.Ltmp28:
	movq	%rax, %r14
	leaq	64(%rsp), %r12
	jmp	.LBB0_71
.LBB0_17:                               # %.loopexit.split-lp108
.Ltmp12:
	jmp	.LBB0_34
.LBB0_15:                               # %.loopexit.split-lp114
.Ltmp4:
	jmp	.LBB0_34
.LBB0_16:                               # %.loopexit107
.Ltmp15:
	jmp	.LBB0_34
.LBB0_33:                               # %.loopexit113
.Ltmp7:
.LBB0_34:
	movq	%rax, %r14
.Ltmp16:
	movq	%rsp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp17:
	jmp	.LBB0_35
.LBB0_67:                               # %.loopexit
.Ltmp23:
	jmp	.LBB0_69
.LBB0_68:                               # %.loopexit.split-lp
.Ltmp20:
.LBB0_69:
	movq	%rax, %r14
.Ltmp24:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp25:
.LBB0_71:
.Ltmp29:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp30:
.LBB0_35:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_72:
.Ltmp31:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZNK8NArchive3N7z7CFolder14CheckStructureEv, .Lfunc_end0-_ZNK8NArchive3N7z7CFolder14CheckStructureEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp11-.Ltmp8          #   Call between .Ltmp8 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp18-.Ltmp14         #   Call between .Ltmp14 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp26-.Ltmp22         #   Call between .Ltmp22 and .Ltmp26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp16-.Ltmp27         #   Call between .Ltmp27 and .Ltmp16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp30-.Ltmp16         #   Call between .Ltmp16 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin0   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp30-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Lfunc_end0-.Ltmp30     #   Call between .Ltmp30 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN8NArchive3N7z13CStreamSwitch6RemoveEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z13CStreamSwitch6RemoveEv,@function
_ZN8NArchive3N7z13CStreamSwitch6RemoveEv: # @_ZN8NArchive3N7z13CStreamSwitch6RemoveEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpb	$0, 8(%rbx)
	je	.LBB2_4
# BB#1:
	movq	(%rbx), %r14
	leaq	8(%r14), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movslq	20(%r14), %rax
	testq	%rax, %rax
	je	.LBB2_3
# BB#2:
	movq	24(%r14), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%r14)
.LBB2_3:                                # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit
	movb	$0, 8(%rbx)
.LBB2_4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	_ZN8NArchive3N7z13CStreamSwitch6RemoveEv, .Lfunc_end2-_ZN8NArchive3N7z13CStreamSwitch6RemoveEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPKhm
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPKhm,@function
_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPKhm: # @_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPKhm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -48
.Lcfi24:
	.cfi_offset %r12, -40
.Lcfi25:
	.cfi_offset %r13, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r12
	cmpb	$0, 8(%r12)
	je	.LBB3_4
# BB#1:
	movq	(%r12), %r13
	leaq	8(%r13), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movslq	20(%r13), %rax
	testq	%rax, %rax
	je	.LBB3_3
# BB#2:
	movq	24(%r13), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%r13)
.LBB3_3:                                # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i
	movb	$0, 8(%r12)
.LBB3_4:                                # %_ZN8NArchive3N7z13CStreamSwitch6RemoveEv.exit
	movq	%rbx, (%r12)
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r13
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	movq	$0, 16(%r13)
	leaq	8(%rbx), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	24(%rbx), %rax
	movslq	20(%rbx), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%rbx)
	movq	24(%rbx), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 40(%rbx)
	movq	%r15, (%rax)
	movq	%r14, 8(%rax)
	movq	$0, 16(%rax)
	movb	$1, 8(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPKhm, .Lfunc_end3-_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPKhm
	.cfi_endproc

	.globl	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveERK7CBufferIhE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveERK7CBufferIhE,@function
_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveERK7CBufferIhE: # @_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveERK7CBufferIhE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -48
.Lcfi34:
	.cfi_offset %r12, -40
.Lcfi35:
	.cfi_offset %r13, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	8(%rdx), %r12
	movq	16(%rdx), %r13
	cmpb	$0, 8(%r14)
	je	.LBB4_4
# BB#1:
	movq	(%r14), %rbx
	leaq	8(%rbx), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movslq	20(%rbx), %rax
	testq	%rax, %rax
	je	.LBB4_3
# BB#2:
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbx)
.LBB4_3:                                # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i
	movb	$0, 8(%r14)
.LBB4_4:                                # %_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPKhm.exit
	movq	%r15, (%r14)
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	leaq	8(%r15), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	24(%r15), %rax
	movslq	20(%r15), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%r15)
	movq	24(%r15), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 40(%r15)
	movq	%r13, (%rax)
	movq	%r12, 8(%rax)
	movq	$0, 16(%rax)
	movb	$1, 8(%r14)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveERK7CBufferIhE, .Lfunc_end4-_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveERK7CBufferIhE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE,@function
_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE: # @_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -48
.Lcfi44:
	.cfi_offset %r12, -40
.Lcfi45:
	.cfi_offset %r13, -32
.Lcfi46:
	.cfi_offset %r14, -24
.Lcfi47:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	cmpb	$0, 8(%r14)
	je	.LBB5_4
# BB#1:
	movq	(%r14), %rbx
	leaq	8(%rbx), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movslq	20(%rbx), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbx)
.LBB5_3:                                # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i
	movb	$0, 8(%r14)
.LBB5_4:                                # %_ZN8NArchive3N7z13CStreamSwitch6RemoveEv.exit
	movq	40(%r12), %rdi
	movq	16(%rdi), %rax
	cmpq	8(%rdi), %rax
	jae	.LBB5_15
# BB#5:                                 # %_ZN8NArchive3N7z10CInArchive8ReadByteEv.exit
	movq	(%rdi), %rcx
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rdi)
	cmpb	$0, (%rcx,%rax)
	je	.LBB5_14
# BB#6:
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	testq	$-2147483648, %rax      # imm = 0x80000000
	jne	.LBB5_15
# BB#7:                                 # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit
	testl	%eax, %eax
	js	.LBB5_15
# BB#8:
	cmpl	12(%r15), %eax
	jge	.LBB5_15
# BB#9:
	movq	16(%r15), %rcx
	cltq
	movq	(%rcx,%rax,8), %rax
	movq	8(%rax), %r15
	movq	16(%rax), %r13
	cmpb	$0, 8(%r14)
	je	.LBB5_13
# BB#10:
	movq	(%r14), %rbx
	leaq	8(%rbx), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movslq	20(%rbx), %rax
	testq	%rax, %rax
	je	.LBB5_12
# BB#11:
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbx)
.LBB5_12:                               # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i.i
	movb	$0, 8(%r14)
.LBB5_13:                               # %_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveERK7CBufferIhE.exit
	movq	%r12, (%r14)
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	leaq	8(%r12), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	24(%r12), %rax
	movslq	20(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%r12)
	movq	24(%r12), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 40(%r12)
	movq	%r13, (%rax)
	movq	%r15, 8(%rax)
	movq	$0, 16(%rax)
	movb	$1, 8(%r14)
.LBB5_14:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_15:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end5:
	.size	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE, .Lfunc_end5-_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CInByte28ReadByteEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CInByte28ReadByteEv,@function
_ZN8NArchive3N7z8CInByte28ReadByteEv:   # @_ZN8NArchive3N7z8CInByte28ReadByteEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 16
	movq	16(%rdi), %rax
	cmpq	8(%rdi), %rax
	jae	.LBB6_2
# BB#1:
	movq	(%rdi), %rcx
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rdi)
	movb	(%rcx,%rax), %al
	popq	%rcx
	retq
.LBB6_2:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end6:
	.size	_ZN8NArchive3N7z8CInByte28ReadByteEv, .Lfunc_end6-_ZN8NArchive3N7z8CInByte28ReadByteEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CInByte29ReadBytesEPhm
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CInByte29ReadBytesEPhm,@function
_ZN8NArchive3N7z8CInByte29ReadBytesEPhm: # @_ZN8NArchive3N7z8CInByte29ReadBytesEPhm
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	subq	%rax, %rcx
	cmpq	%rdx, %rcx
	jb	.LBB7_10
# BB#1:                                 # %.preheader
	testq	%rdx, %rdx
	je	.LBB7_9
# BB#2:                                 # %.lr.ph
	movq	(%rdi), %r8
	leaq	1(%rax), %rcx
	movq	%rcx, 16(%rdi)
	movb	(%r8,%rax), %al
	movb	%al, (%rsi)
	cmpq	$1, %rdx
	je	.LBB7_9
# BB#3:                                 # %._crit_edge8.preheader
	testb	$1, %dl
	jne	.LBB7_4
# BB#5:                                 # %._crit_edge8.prol
	movq	(%rdi), %r8
	movq	16(%rdi), %rcx
	leaq	1(%rcx), %rax
	movq	%rax, 16(%rdi)
	movb	(%r8,%rcx), %al
	movb	%al, 1(%rsi)
	movl	$2, %eax
	cmpq	$2, %rdx
	jne	.LBB7_7
	jmp	.LBB7_9
.LBB7_4:
	movl	$1, %eax
	cmpq	$2, %rdx
	je	.LBB7_9
.LBB7_7:                                # %._crit_edge8.preheader.new
	subq	%rax, %rdx
	leaq	1(%rsi,%rax), %rax
	.p2align	4, 0x90
.LBB7_8:                                # %._crit_edge8
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %r8
	movq	16(%rdi), %rsi
	leaq	1(%rsi), %rcx
	movq	%rcx, 16(%rdi)
	movzbl	(%r8,%rsi), %ecx
	movb	%cl, -1(%rax)
	movq	(%rdi), %r8
	movq	16(%rdi), %rsi
	leaq	1(%rsi), %rcx
	movq	%rcx, 16(%rdi)
	movzbl	(%r8,%rsi), %ecx
	movb	%cl, (%rax)
	addq	$2, %rax
	addq	$-2, %rdx
	jne	.LBB7_8
.LBB7_9:                                # %._crit_edge
	popq	%rax
	retq
.LBB7_10:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end7:
	.size	_ZN8NArchive3N7z8CInByte29ReadBytesEPhm, .Lfunc_end7-_ZN8NArchive3N7z8CInByte29ReadBytesEPhm
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CInByte28SkipDataEy
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CInByte28SkipDataEy,@function
_ZN8NArchive3N7z8CInByte28SkipDataEy:   # @_ZN8NArchive3N7z8CInByte28SkipDataEy
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.LBB8_2
# BB#1:
	addq	%rsi, %rax
	movq	%rax, 16(%rdi)
	popq	%rax
	retq
.LBB8_2:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end8:
	.size	_ZN8NArchive3N7z8CInByte28SkipDataEy, .Lfunc_end8-_ZN8NArchive3N7z8CInByte28SkipDataEy
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CInByte28SkipDataEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CInByte28SkipDataEv,@function
_ZN8NArchive3N7z8CInByte28SkipDataEv:   # @_ZN8NArchive3N7z8CInByte28SkipDataEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 16
.Lcfi52:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jb	.LBB9_2
# BB#1:                                 # %_ZN8NArchive3N7z8CInByte28SkipDataEy.exit
	addq	%rax, %rcx
	movq	%rcx, 16(%rbx)
	popq	%rbx
	retq
.LBB9_2:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end9:
	.size	_ZN8NArchive3N7z8CInByte28SkipDataEv, .Lfunc_end9-_ZN8NArchive3N7z8CInByte28SkipDataEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CInByte210ReadNumberEv,@function
_ZN8NArchive3N7z8CInByte210ReadNumberEv: # @_ZN8NArchive3N7z8CInByte210ReadNumberEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi53:
	.cfi_def_cfa_offset 16
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	cmpq	%rcx, %rax
	jae	.LBB10_28
# BB#1:
	movq	(%rdi), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, 16(%rdi)
	movzbl	(%rdx,%rax), %r10d
	testb	%r10b, %r10b
	js	.LBB10_3
# BB#2:
	movl	$383, %eax              # imm = 0x17F
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	jmp	.LBB10_6
.LBB10_3:
	cmpq	%rcx, %rsi
	jae	.LBB10_28
# BB#4:
	leaq	2(%rax), %rsi
	movq	%rsi, 16(%rdi)
	movzbl	1(%rdx,%rax), %r8d
	testb	$64, %r10b
	jne	.LBB10_8
# BB#5:
	movl	$319, %eax              # imm = 0x13F
	movl	$8, %ecx
	jmp	.LBB10_6
.LBB10_8:
	cmpq	%rcx, %rsi
	jae	.LBB10_28
# BB#9:
	leaq	3(%rax), %r9
	movq	%r9, 16(%rdi)
	movzbl	2(%rdx,%rax), %esi
	shlq	$8, %rsi
	orq	%rsi, %r8
	testb	$32, %r10b
	jne	.LBB10_11
# BB#10:
	movl	$287, %eax              # imm = 0x11F
	movl	$16, %ecx
	jmp	.LBB10_6
.LBB10_11:
	cmpq	%rcx, %r9
	jae	.LBB10_28
# BB#12:
	leaq	4(%rax), %r9
	movq	%r9, 16(%rdi)
	movzbl	3(%rdx,%rax), %esi
	shlq	$16, %rsi
	orq	%rsi, %r8
	testb	$16, %r10b
	jne	.LBB10_14
# BB#13:
	movl	$271, %eax              # imm = 0x10F
	movl	$24, %ecx
	jmp	.LBB10_6
.LBB10_14:
	cmpq	%rcx, %r9
	jae	.LBB10_28
# BB#15:
	leaq	5(%rax), %r9
	movq	%r9, 16(%rdi)
	movzbl	4(%rdx,%rax), %esi
	shlq	$24, %rsi
	orq	%rsi, %r8
	testb	$8, %r10b
	jne	.LBB10_17
# BB#16:
	movl	$263, %eax              # imm = 0x107
	movl	$32, %ecx
	jmp	.LBB10_6
.LBB10_17:
	cmpq	%rcx, %r9
	jae	.LBB10_28
# BB#18:
	leaq	6(%rax), %r9
	movq	%r9, 16(%rdi)
	movzbl	5(%rdx,%rax), %esi
	shlq	$32, %rsi
	orq	%rsi, %r8
	testb	$4, %r10b
	jne	.LBB10_20
# BB#19:
	movl	$259, %eax              # imm = 0x103
	movl	$40, %ecx
	jmp	.LBB10_6
.LBB10_20:
	cmpq	%rcx, %r9
	jae	.LBB10_28
# BB#21:
	leaq	7(%rax), %r9
	movq	%r9, 16(%rdi)
	movzbl	6(%rdx,%rax), %esi
	shlq	$40, %rsi
	orq	%rsi, %r8
	testb	$2, %r10b
	jne	.LBB10_23
# BB#22:
	movl	$257, %eax              # imm = 0x101
	movl	$48, %ecx
	jmp	.LBB10_6
.LBB10_23:
	cmpq	%rcx, %r9
	jae	.LBB10_28
# BB#24:
	leaq	8(%rax), %r9
	movq	%r9, 16(%rdi)
	movzbl	7(%rdx,%rax), %esi
	shlq	$48, %rsi
	orq	%rsi, %r8
	testb	$1, %r10b
	jne	.LBB10_26
# BB#25:
	movl	$256, %eax              # imm = 0x100
	movl	$56, %ecx
.LBB10_6:
	andl	%r10d, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rax
	addq	%r8, %rax
	popq	%rcx
	retq
.LBB10_26:
	cmpq	%rcx, %r9
	jae	.LBB10_28
# BB#27:                                # %.loopexit.loopexit44
	leaq	9(%rax), %rcx
	movq	%rcx, 16(%rdi)
	movzbl	8(%rdx,%rax), %eax
	shlq	$56, %rax
	orq	%r8, %rax
	popq	%rcx
	retq
.LBB10_28:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end10:
	.size	_ZN8NArchive3N7z8CInByte210ReadNumberEv, .Lfunc_end10-_ZN8NArchive3N7z8CInByte210ReadNumberEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CInByte27ReadNumEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CInByte27ReadNumEv,@function
_ZN8NArchive3N7z8CInByte27ReadNumEv:    # @_ZN8NArchive3N7z8CInByte27ReadNumEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 16
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	testq	$-2147483648, %rax      # imm = 0x80000000
	jne	.LBB11_2
# BB#1:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	popq	%rcx
	retq
.LBB11_2:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end11:
	.size	_ZN8NArchive3N7z8CInByte27ReadNumEv, .Lfunc_end11-_ZN8NArchive3N7z8CInByte27ReadNumEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CInByte210ReadUInt32Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CInByte210ReadUInt32Ev,@function
_ZN8NArchive3N7z8CInByte210ReadUInt32Ev: # @_ZN8NArchive3N7z8CInByte210ReadUInt32Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 16
	movq	16(%rdi), %rax
	leaq	4(%rax), %rcx
	cmpq	8(%rdi), %rcx
	ja	.LBB12_2
# BB#1:
	movq	(%rdi), %rdx
	movl	(%rdx,%rax), %eax
	movq	%rcx, 16(%rdi)
	popq	%rcx
	retq
.LBB12_2:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end12:
	.size	_ZN8NArchive3N7z8CInByte210ReadUInt32Ev, .Lfunc_end12-_ZN8NArchive3N7z8CInByte210ReadUInt32Ev
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CInByte210ReadUInt64Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CInByte210ReadUInt64Ev,@function
_ZN8NArchive3N7z8CInByte210ReadUInt64Ev: # @_ZN8NArchive3N7z8CInByte210ReadUInt64Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 16
	movq	16(%rdi), %rax
	leaq	8(%rax), %rcx
	cmpq	8(%rdi), %rcx
	ja	.LBB13_2
# BB#1:
	movq	(%rdi), %rdx
	movq	(%rdx,%rax), %rax
	movq	%rcx, 16(%rdi)
	popq	%rcx
	retq
.LBB13_2:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end13:
	.size	_ZN8NArchive3N7z8CInByte210ReadUInt64Ev, .Lfunc_end13-_ZN8NArchive3N7z8CInByte210ReadUInt64Ev
	.cfi_endproc

	.globl	_ZN8NArchive3N7z8CInByte210ReadStringER11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CInByte210ReadStringER11CStringBaseIwE,@function
_ZN8NArchive3N7z8CInByte210ReadStringER11CStringBaseIwE: # @_ZN8NArchive3N7z8CInByte210ReadStringER11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 96
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	(%r12), %r11
	movq	8(%r12), %rax
	movq	16(%r12), %rdi
	subq	%rdi, %rax
	andq	$-2, %rax
	je	.LBB14_5
# BB#1:                                 # %.lr.ph44.preheader
	leaq	1(%r11,%rdi), %rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_2:                               # %.lr.ph44
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, -1(%rcx,%rbx)
	jne	.LBB14_4
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpb	$0, (%rcx,%rbx)
	je	.LBB14_6
.LBB14_4:                               #   in Loop: Header=BB14_2 Depth=1
	addq	$2, %rbx
	cmpq	%rax, %rbx
	jb	.LBB14_2
	jmp	.LBB14_6
.LBB14_5:
	xorl	%ebx, %ebx
.LBB14_6:                               # %._crit_edge45
	cmpq	%rax, %rbx
	je	.LBB14_46
# BB#7:
	movq	%rbx, %r13
	shrq	%r13
	testl	%r13d, %r13d
	js	.LBB14_46
# BB#8:
	movq	%rbx, %r14
	shlq	$31, %r14
	movq	%r14, %rax
	sarq	$31, %rax
	cmpq	%rbx, %rax
	jne	.LBB14_46
# BB#9:
	movl	12(%rsi), %ebp
	cmpl	%r13d, %ebp
	jg	.LBB14_27
# BB#10:
	leal	1(%r13), %eax
	cmpl	%ebp, %eax
	je	.LBB14_27
# BB#11:
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	callq	_Znam
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rax, %r15
	testl	%ebp, %ebp
	jle	.LBB14_26
# BB#12:                                # %.preheader.i.i
	movslq	8(%rsi), %rax
	testq	%rax, %rax
	movq	(%rsi), %rdi
	jle	.LBB14_24
# BB#13:                                # %.lr.ph.i.i
	cmpl	$7, %eax
	jbe	.LBB14_17
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB14_17
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r15
	jae	.LBB14_38
# BB#16:                                # %vector.memcheck
	leaq	(%r15,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB14_38
.LBB14_17:
	xorl	%ecx, %ecx
.LBB14_18:                              # %scalar.ph.preheader
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rsi
	je	.LBB14_21
# BB#19:                                # %scalar.ph.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB14_20:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %ebp
	movl	%ebp, (%r15,%rcx,4)
	incq	%rcx
	incq	%rsi
	jne	.LBB14_20
.LBB14_21:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB14_25
# BB#22:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r15,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB14_23:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB14_23
	jmp	.LBB14_25
.LBB14_24:                              # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB14_26
.LBB14_25:                              # %._crit_edge.thread.i.i
	callq	_ZdaPv
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB14_26:                              # %._crit_edge16.i.i
	movq	%r15, (%rsi)
	movslq	8(%rsi), %rax
	movl	$0, (%r15,%rax,4)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 12(%rsi)
	movq	32(%rsp), %r11          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB14_27:                              # %_ZN11CStringBaseIwE9GetBufferEi.exit
	movq	(%rsi), %r8
	testl	%r13d, %r13d
	jle	.LBB14_37
# BB#28:                                # %.lr.ph.preheader
	leaq	(%r11,%rdi), %rcx
	movl	%r13d, %edx
	cmpq	$8, %rdx
	jb	.LBB14_34
# BB#30:                                # %min.iters.checked56
	movl	%r13d, %r9d
	andl	$7, %r9d
	movq	%rdx, %r10
	subq	%r9, %r10
	je	.LBB14_34
# BB#31:                                # %vector.body52.preheader
	leaq	(%rcx,%r10,2), %rcx
	leaq	8(%r11,%rdi), %rbp
	leaq	16(%r8), %rax
	pxor	%xmm0, %xmm0
	movq	%r10, %rdi
	.p2align	4, 0x90
.LBB14_32:                              # %vector.body52
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbp), %xmm1         # xmm1 = mem[0],zero
	movq	(%rbp), %xmm2           # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movdqu	%xmm1, -16(%rax)
	movdqu	%xmm2, (%rax)
	addq	$16, %rbp
	addq	$32, %rax
	addq	$-8, %rdi
	jne	.LBB14_32
# BB#33:                                # %middle.block53
	testq	%r9, %r9
	jne	.LBB14_35
	jmp	.LBB14_37
.LBB14_34:
	xorl	%r10d, %r10d
.LBB14_35:                              # %.lr.ph.preheader73
	leaq	(%r8,%r10,4), %rax
	subq	%r10, %rdx
	.p2align	4, 0x90
.LBB14_36:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx), %edi
	movl	%edi, (%rax)
	addq	$2, %rcx
	addq	$4, %rax
	decq	%rdx
	jne	.LBB14_36
.LBB14_37:                              # %._crit_edge
	sarq	$30, %r14
	movl	$0, (%r8,%r14)
	movl	%r13d, 8(%rsi)
	movq	16(%r12), %rax
	leaq	2(%rbx,%rax), %rax
	movq	%rax, 16(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_38:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB14_41
# BB#39:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_40:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rdi,%rbp,4), %xmm0
	movdqu	16(%rdi,%rbp,4), %xmm1
	movdqu	%xmm0, (%r15,%rbp,4)
	movdqu	%xmm1, 16(%r15,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB14_40
	jmp	.LBB14_42
.LBB14_41:
	xorl	%ebp, %ebp
.LBB14_42:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB14_45
# BB#43:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r15,%rbp,4), %rsi
	leaq	112(%rdi,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB14_44:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-32, %rdx
	jne	.LBB14_44
.LBB14_45:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB14_18
	jmp	.LBB14_25
.LBB14_46:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end14:
	.size	_ZN8NArchive3N7z8CInByte210ReadStringER11CStringBaseIwE, .Lfunc_end14-_ZN8NArchive3N7z8CInByte210ReadStringER11CStringBaseIwE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z10CInArchive20FindAndReadSignatureEP9IInStreamPKy
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive20FindAndReadSignatureEP9IInStreamPKy,@function
_ZN8NArchive3N7z10CInArchive20FindAndReadSignatureEP9IInStreamPKy: # @_ZN8NArchive3N7z10CInArchive20FindAndReadSignatureEP9IInStreamPKy
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi74:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi76:
	.cfi_def_cfa_offset 112
.Lcfi77:
	.cfi_offset %rbx, -56
.Lcfi78:
	.cfi_offset %r12, -48
.Lcfi79:
	.cfi_offset %r13, -40
.Lcfi80:
	.cfi_offset %r14, -32
.Lcfi81:
	.cfi_offset %r15, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %r14
	leaq	56(%r14), %r13
	movl	$32, %edx
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %edx
	testl	%edx, %edx
	jne	.LBB15_89
# BB#1:
	movb	(%r13), %al
	cmpb	_ZN8NArchive3N7z10kSignatureE(%rip), %al
	jne	.LBB15_35
# BB#2:
	movb	57(%r14), %al
	cmpb	_ZN8NArchive3N7z10kSignatureE+1(%rip), %al
	jne	.LBB15_35
# BB#3:
	movb	58(%r14), %al
	cmpb	_ZN8NArchive3N7z10kSignatureE+2(%rip), %al
	jne	.LBB15_35
# BB#4:
	movb	59(%r14), %al
	cmpb	_ZN8NArchive3N7z10kSignatureE+3(%rip), %al
	jne	.LBB15_35
# BB#5:
	movb	60(%r14), %al
	cmpb	_ZN8NArchive3N7z10kSignatureE+4(%rip), %al
	jne	.LBB15_35
# BB#6:
	movb	61(%r14), %al
	cmpb	_ZN8NArchive3N7z10kSignatureE+5(%rip), %al
	jne	.LBB15_35
# BB#7:
	leaq	68(%r14), %rbx
	movl	$20, %esi
	movq	%rbx, %rdi
	callq	CrcCalc
	movl	64(%r14), %ecx
	xorl	%edx, %edx
	cmpl	%ecx, %eax
	je	.LBB15_89
# BB#8:                                 # %.preheader.preheader.i
	testb	%cl, %cl
	jne	.LBB15_35
# BB#9:                                 # %.preheader.117.i
	cmpb	$0, 65(%r14)
	jne	.LBB15_35
# BB#10:                                # %.preheader.218.i
	cmpb	$0, 66(%r14)
	jne	.LBB15_35
# BB#11:                                # %.preheader.319.i
	cmpb	$0, 67(%r14)
	jne	.LBB15_35
# BB#12:                                # %.preheader.420.i
	cmpb	$0, (%rbx)
	jne	.LBB15_35
# BB#13:                                # %.preheader.521.i
	cmpb	$0, 69(%r14)
	jne	.LBB15_35
# BB#14:                                # %.preheader.622.i
	cmpb	$0, 70(%r14)
	jne	.LBB15_35
# BB#15:                                # %.preheader.723.i
	cmpb	$0, 71(%r14)
	jne	.LBB15_35
# BB#16:                                # %.preheader.824.i
	cmpb	$0, 72(%r14)
	jne	.LBB15_35
# BB#17:                                # %.preheader.925.i
	cmpb	$0, 73(%r14)
	jne	.LBB15_35
# BB#18:                                # %.preheader.1026.i
	cmpb	$0, 74(%r14)
	jne	.LBB15_35
# BB#19:                                # %.preheader.1127.i
	cmpb	$0, 75(%r14)
	jne	.LBB15_35
# BB#20:                                # %.preheader.1228.i
	cmpb	$0, 76(%r14)
	jne	.LBB15_35
# BB#21:                                # %.preheader.1329.i
	cmpb	$0, 77(%r14)
	jne	.LBB15_35
# BB#22:                                # %.preheader.1430.i
	cmpb	$0, 78(%r14)
	jne	.LBB15_35
# BB#23:                                # %.preheader.1531.i
	cmpb	$0, 79(%r14)
	jne	.LBB15_35
# BB#24:                                # %.preheader.1632.i
	cmpb	$0, 80(%r14)
	jne	.LBB15_35
# BB#25:                                # %.preheader.1733.i
	cmpb	$0, 81(%r14)
	jne	.LBB15_35
# BB#26:                                # %.preheader.1834.i
	cmpb	$0, 82(%r14)
	jne	.LBB15_35
# BB#27:                                # %.preheader.1935.i
	cmpb	$0, 83(%r14)
	jne	.LBB15_35
# BB#28:                                # %.preheader.2036.i
	cmpb	$0, 84(%r14)
	jne	.LBB15_35
# BB#29:                                # %.preheader.2137.i
	cmpb	$0, 85(%r14)
	jne	.LBB15_35
# BB#30:                                # %.preheader.2238.i
	cmpb	$0, 86(%r14)
	jne	.LBB15_35
# BB#31:                                # %.preheader.2339.i
	cmpb	$0, 87(%r14)
	je	.LBB15_32
.LBB15_35:                              # %_ZN8NArchive3N7zL14TestSignature2EPKh.exit.thread
.Ltmp32:
	movl	$65536, %edi            # imm = 0x10000
	callq	_Znam
	movq	%rax, %r12
.Ltmp33:
# BB#36:                                # %_ZN7CBufferIhE11SetCapacityEm.exit
	movups	(%r13), %xmm0
	movups	16(%r13), %xmm1
	movups	%xmm1, 16(%r12)
	movups	%xmm0, (%r12)
	movq	48(%r14), %rcx
	testq	%rbp, %rbp
	je	.LBB15_58
# BB#37:
	movq	%r13, 40(%rsp)          # 8-byte Spill
.LBB15_38:                              # %.preheader129.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_39 Depth 2
                                        #     Child Loop BB15_45 Depth 2
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rbp, %rbx
	movl	$32, %r13d
	.p2align	4, 0x90
.LBB15_39:                              # %.preheader129
                                        #   Parent Loop BB15_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r13d, %ebp
	movl	$65536, %edx            # imm = 0x10000
	subl	%ebp, %edx
	movq	(%r15), %rax
	movl	%ebp, %esi
	addq	%r12, %rsi
.Ltmp35:
	movq	%r15, %rdi
	leaq	12(%rsp), %rcx
	callq	*40(%rax)
.Ltmp36:
# BB#40:                                #   in Loop: Header=BB15_39 Depth=2
	testl	%eax, %eax
	jne	.LBB15_88
# BB#41:                                #   in Loop: Header=BB15_39 Depth=2
	movl	12(%rsp), %ecx
	testl	%ecx, %ecx
	movl	$1, %eax
	je	.LBB15_88
# BB#42:                                # %.thread
                                        #   in Loop: Header=BB15_39 Depth=2
	leal	(%rcx,%rbp), %r13d
	cmpl	$33, %r13d
	jb	.LBB15_39
# BB#43:                                #   in Loop: Header=BB15_38 Depth=1
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	addl	$-32, %r13d
	je	.LBB15_87
# BB#44:                                # %.preheader.preheader
                                        #   in Loop: Header=BB15_38 Depth=1
	leal	-31(%rbp,%rcx), %esi
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB15_45:                              #   Parent Loop BB15_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r15d, %ecx
	movl	%ecx, %r14d
	movzbl	(%r12,%r14), %edx
	cmpl	%r13d, %ecx
	leal	1(%rcx), %r15d
	jae	.LBB15_47
# BB#46:                                #   in Loop: Header=BB15_45 Depth=2
	cmpb	$55, %dl
	jne	.LBB15_45
.LBB15_47:                              # %.critedge
                                        #   in Loop: Header=BB15_45 Depth=2
	cmpl	%r15d, %esi
	je	.LBB15_87
# BB#48:                                #   in Loop: Header=BB15_45 Depth=2
	cmpb	_ZN8NArchive3N7z10kSignatureE(%rip), %dl
	jne	.LBB15_86
# BB#49:                                #   in Loop: Header=BB15_45 Depth=2
	leaq	(%r12,%r14), %rdx
	movzbl	1(%rdx), %ecx
	cmpb	_ZN8NArchive3N7z10kSignatureE+1(%rip), %cl
	jne	.LBB15_86
# BB#50:                                #   in Loop: Header=BB15_45 Depth=2
	movzbl	2(%rdx), %ecx
	cmpb	_ZN8NArchive3N7z10kSignatureE+2(%rip), %cl
	jne	.LBB15_86
# BB#51:                                #   in Loop: Header=BB15_45 Depth=2
	movzbl	3(%rdx), %ecx
	cmpb	_ZN8NArchive3N7z10kSignatureE+3(%rip), %cl
	jne	.LBB15_86
# BB#52:                                #   in Loop: Header=BB15_45 Depth=2
	movzbl	4(%rdx), %ecx
	cmpb	_ZN8NArchive3N7z10kSignatureE+4(%rip), %cl
	jne	.LBB15_86
# BB#53:                                #   in Loop: Header=BB15_45 Depth=2
	movzbl	5(%rdx), %ecx
	cmpb	_ZN8NArchive3N7z10kSignatureE+5(%rip), %cl
	jne	.LBB15_86
# BB#54:                                # %.critedge12.i
                                        #   in Loop: Header=BB15_45 Depth=2
	movl	%esi, 52(%rsp)          # 4-byte Spill
	leaq	12(%rdx), %rdi
.Ltmp38:
	movl	$20, %esi
	movq	%rdx, %rbp
	callq	CrcCalc
.Ltmp39:
# BB#55:                                # %_ZN8NArchive3N7zL13TestSignatureEPKh.exit
                                        #   in Loop: Header=BB15_45 Depth=2
	cmpl	8(%rbp), %eax
	movl	$1, %eax
	movq	%rbp, %rcx
	movl	52(%rsp), %esi          # 4-byte Reload
	je	.LBB15_56
.LBB15_86:                              # %_ZN8NArchive3N7zL13TestSignatureEPKh.exit.thread
                                        #   in Loop: Header=BB15_45 Depth=2
	cmpl	%r13d, %r15d
	jb	.LBB15_45
.LBB15_87:                              # %.thread122
                                        #   in Loop: Header=BB15_38 Depth=1
	movl	%r13d, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	addq	%rdx, %rcx
	movups	(%r12,%rdx), %xmm0
	movups	16(%r12,%rdx), %xmm1
	movups	%xmm1, 16(%r12)
	movups	%xmm0, (%r12)
	movq	%rcx, %rdx
	movq	24(%rsp), %r14          # 8-byte Reload
	subq	48(%r14), %rdx
	movq	%rbx, %rbp
	cmpq	(%rbp), %rdx
	movq	16(%rsp), %r15          # 8-byte Reload
	jbe	.LBB15_38
	jmp	.LBB15_88
.LBB15_58:                              # %_ZN7CBufferIhE11SetCapacityEm.exit.split.us.preheader
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB15_59
.LBB15_79:                              # %.thread122.us
                                        #   in Loop: Header=BB15_59 Depth=1
	movl	%ebp, %eax
	addq	%rax, 32(%rsp)          # 8-byte Folded Spill
	movups	(%r12,%rax), %xmm0
	movups	16(%r12,%rax), %xmm1
	movups	%xmm1, 16(%r12)
	movups	%xmm0, (%r12)
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	40(%rsp), %r13          # 8-byte Reload
.LBB15_59:                              # %_ZN7CBufferIhE11SetCapacityEm.exit.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_60 Depth 2
                                        #     Child Loop BB15_67 Depth 2
	movl	$32, %ebp
	.p2align	4, 0x90
.LBB15_60:                              #   Parent Loop BB15_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebp, %ebx
	movl	$65536, %edx            # imm = 0x10000
	subl	%ebx, %edx
	movq	(%r15), %rax
	movl	%ebx, %esi
	addq	%r12, %rsi
.Ltmp41:
	movq	%r15, %rdi
	leaq	12(%rsp), %rcx
	callq	*40(%rax)
.Ltmp42:
# BB#61:                                #   in Loop: Header=BB15_60 Depth=2
	testl	%eax, %eax
	jne	.LBB15_88
# BB#62:                                #   in Loop: Header=BB15_60 Depth=2
	movl	12(%rsp), %eax
	testl	%eax, %eax
	je	.LBB15_63
# BB#64:                                # %.thread.us
                                        #   in Loop: Header=BB15_60 Depth=2
	leal	(%rax,%rbx), %ebp
	cmpl	$33, %ebp
	jb	.LBB15_60
# BB#65:                                #   in Loop: Header=BB15_59 Depth=1
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	addl	$-32, %ebp
	je	.LBB15_79
# BB#66:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB15_59 Depth=1
	leal	-31(%rbx,%rax), %esi
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB15_67:                              #   Parent Loop BB15_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r13d, %ecx
	movl	%ecx, %r14d
	movzbl	(%r12,%r14), %eax
	cmpl	%ebp, %ecx
	leal	1(%rcx), %r13d
	jae	.LBB15_69
# BB#68:                                #   in Loop: Header=BB15_67 Depth=2
	cmpb	$55, %al
	jne	.LBB15_67
.LBB15_69:                              # %.critedge.us
                                        #   in Loop: Header=BB15_67 Depth=2
	cmpl	%r13d, %esi
	je	.LBB15_79
# BB#70:                                #   in Loop: Header=BB15_67 Depth=2
	cmpb	_ZN8NArchive3N7z10kSignatureE(%rip), %al
	jne	.LBB15_78
# BB#71:                                #   in Loop: Header=BB15_67 Depth=2
	leaq	(%r12,%r14), %rcx
	movzbl	1(%rcx), %eax
	cmpb	_ZN8NArchive3N7z10kSignatureE+1(%rip), %al
	jne	.LBB15_78
# BB#72:                                #   in Loop: Header=BB15_67 Depth=2
	movzbl	2(%rcx), %eax
	cmpb	_ZN8NArchive3N7z10kSignatureE+2(%rip), %al
	jne	.LBB15_78
# BB#73:                                #   in Loop: Header=BB15_67 Depth=2
	movzbl	3(%rcx), %eax
	cmpb	_ZN8NArchive3N7z10kSignatureE+3(%rip), %al
	jne	.LBB15_78
# BB#74:                                #   in Loop: Header=BB15_67 Depth=2
	movzbl	4(%rcx), %eax
	cmpb	_ZN8NArchive3N7z10kSignatureE+4(%rip), %al
	jne	.LBB15_78
# BB#75:                                #   in Loop: Header=BB15_67 Depth=2
	movzbl	5(%rcx), %eax
	cmpb	_ZN8NArchive3N7z10kSignatureE+5(%rip), %al
	jne	.LBB15_78
# BB#76:                                # %.critedge12.i.us
                                        #   in Loop: Header=BB15_67 Depth=2
	movl	%esi, 16(%rsp)          # 4-byte Spill
	leaq	12(%rcx), %rdi
.Ltmp44:
	movl	$20, %esi
	movq	%rcx, %rbx
	callq	CrcCalc
.Ltmp45:
# BB#77:                                # %_ZN8NArchive3N7zL13TestSignatureEPKh.exit.us
                                        #   in Loop: Header=BB15_67 Depth=2
	cmpl	8(%rbx), %eax
	movq	%rbx, %rcx
	movl	16(%rsp), %esi          # 4-byte Reload
	je	.LBB15_57
.LBB15_78:                              # %_ZN8NArchive3N7zL13TestSignatureEPKh.exit.thread.us
                                        #   in Loop: Header=BB15_67 Depth=2
	cmpl	%ebp, %r13d
	jb	.LBB15_67
	jmp	.LBB15_79
.LBB15_63:
	movl	$1, %eax
.LBB15_88:                              # %_ZN7CBufferIhED2Ev.exit100
	movq	%r12, %rdi
	movl	%eax, %ebx
	callq	_ZdaPv
	movl	%ebx, %edx
.LBB15_89:                              # %_ZN8NArchive3N7zL14TestSignature2EPKh.exit.thread112
	movl	%edx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_56:
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB15_57:                              # %.us-lcssa153.us
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movq	40(%rsp), %rax          # 8-byte Reload
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%r14,%rdx), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 48(%rcx)
	movq	(%r15), %rax
	leaq	32(%r14,%rdx), %rsi
.Ltmp47:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	callq	*48(%rax)
.Ltmp48:
	jmp	.LBB15_88
.LBB15_32:
	movq	%r14, %rax
	cmpb	$0, 62(%r14)
	je	.LBB15_34
# BB#33:
	xorl	%edx, %edx
	jmp	.LBB15_89
.LBB15_34:                              # %_ZN8NArchive3N7zL14TestSignature2EPKh.exit
	movq	%rax, %r14
	cmpb	$0, 63(%r14)
	movl	$0, %edx
	jne	.LBB15_89
	jmp	.LBB15_35
.LBB15_85:                              # %.loopexit.split-lp
.Ltmp49:
	jmp	.LBB15_83
.LBB15_81:                              # %.loopexit.us-lcssa.us
.Ltmp46:
	jmp	.LBB15_83
.LBB15_84:                              # %.loopexit.us-lcssa
.Ltmp40:
	jmp	.LBB15_83
.LBB15_90:
.Ltmp34:
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB15_80:                              # %.us-lcssa.us
.Ltmp43:
	jmp	.LBB15_83
.LBB15_82:                              # %.us-lcssa
.Ltmp37:
.LBB15_83:
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZN8NArchive3N7z10CInArchive20FindAndReadSignatureEP9IInStreamPKy, .Lfunc_end15-_ZN8NArchive3N7z10CInArchive20FindAndReadSignatureEP9IInStreamPKy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\352\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp32-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin1   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin1   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin1   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin1   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin1   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin1   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Lfunc_end15-.Ltmp48    #   Call between .Ltmp48 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN7CBufferIhED2Ev,"axG",@progbits,_ZN7CBufferIhED2Ev,comdat
	.weak	_ZN7CBufferIhED2Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED2Ev,@function
_ZN7CBufferIhED2Ev:                     # @_ZN7CBufferIhED2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV7CBufferIhE+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB16_1
# BB#2:
	jmp	_ZdaPv                  # TAILCALL
.LBB16_1:
	retq
.Lfunc_end16:
	.size	_ZN7CBufferIhED2Ev, .Lfunc_end16-_ZN7CBufferIhED2Ev
	.cfi_endproc

	.text
	.globl	_ZN8NArchive3N7z10CInArchive4OpenEP9IInStreamPKy
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive4OpenEP9IInStreamPKy,@function
_ZN8NArchive3N7z10CInArchive4OpenEP9IInStreamPKy: # @_ZN8NArchive3N7z10CInArchive4OpenEP9IInStreamPKy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	$0, 88(%rbx)
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, (%rbx)
.LBB17_2:                               # %_ZN8NArchive3N7z10CInArchive5CloseEv.exit
	movq	(%r14), %rax
	leaq	48(%rbx), %rcx
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB17_7
# BB#3:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZN8NArchive3N7z10CInArchive20FindAndReadSignatureEP9IInStreamPKy
	testl	%eax, %eax
	jne	.LBB17_7
# BB#4:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB17_6
# BB#5:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB17_6:                               # %_ZN9CMyComPtrI9IInStreamEaSEPS0_.exit
	movq	%r14, (%rbx)
	xorl	%eax, %eax
.LBB17_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZN8NArchive3N7z10CInArchive4OpenEP9IInStreamPKy, .Lfunc_end17-_ZN8NArchive3N7z10CInArchive4OpenEP9IInStreamPKy
	.cfi_endproc

	.globl	_ZN8NArchive3N7z10CInArchive5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive5CloseEv,@function
_ZN8NArchive3N7z10CInArchive5CloseEv:   # @_ZN8NArchive3N7z10CInArchive5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 16
.Lcfi90:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, (%rbx)
.LBB18_2:                               # %_ZN9CMyComPtrI9IInStreamE7ReleaseEv.exit
	popq	%rbx
	retq
.Lfunc_end18:
	.size	_ZN8NArchive3N7z10CInArchive5CloseEv, .Lfunc_end18-_ZN8NArchive3N7z10CInArchive5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z10CInArchive21ReadArchivePropertiesERNS0_14CInArchiveInfoE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive21ReadArchivePropertiesERNS0_14CInArchiveInfoE,@function
_ZN8NArchive3N7z10CInArchive21ReadArchivePropertiesERNS0_14CInArchiveInfoE: # @_ZN8NArchive3N7z10CInArchive21ReadArchivePropertiesERNS0_14CInArchiveInfoE
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi93:
	.cfi_def_cfa_offset 32
.Lcfi94:
	.cfi_offset %rbx, -24
.Lcfi95:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	jmp	.LBB19_1
	.p2align	4, 0x90
.LBB19_3:                               # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit
                                        #   in Loop: Header=BB19_1 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbx)
.LBB19_1:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r14), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	testq	%rax, %rax
	je	.LBB19_4
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB19_1 Depth=1
	movq	40(%r14), %rbx
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jae	.LBB19_3
# BB#5:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB19_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end19:
	.size	_ZN8NArchive3N7z10CInArchive21ReadArchivePropertiesERNS0_14CInArchiveInfoE, .Lfunc_end19-_ZN8NArchive3N7z10CInArchive21ReadArchivePropertiesERNS0_14CInArchiveInfoE
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI20_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI20_1:
	.quad	16                      # 0x10
	.quad	16                      # 0x10
.LCPI20_2:
	.quad	4                       # 0x4
	.quad	4                       # 0x4
.LCPI20_3:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z10CInArchive17GetNextFolderItemERNS0_7CFolderE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive17GetNextFolderItemERNS0_7CFolderE,@function
_ZN8NArchive3N7z10CInArchive17GetNextFolderItemERNS0_7CFolderE: # @_ZN8NArchive3N7z10CInArchive17GetNextFolderItemERNS0_7CFolderE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi96:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi97:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi99:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi100:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 128
.Lcfi103:
	.cfi_offset %rbx, -56
.Lcfi104:
	.cfi_offset %r12, -48
.Lcfi105:
	.cfi_offset %r13, -40
.Lcfi106:
	.cfi_offset %r14, -32
.Lcfi107:
	.cfi_offset %r15, -24
.Lcfi108:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	40(%r13), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %rbx
	testq	$-2147483648, %rbx      # imm = 0x80000000
	jne	.LBB20_77
# BB#1:                                 # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%ebx, %ebx
	je	.LBB20_53
# BB#2:                                 # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit.lr.ph
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB20_3:                               # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_9 Depth 2
                                        #     Child Loop BB20_14 Depth 2
                                        #     Child Loop BB20_20 Depth 2
                                        #     Child Loop BB20_24 Depth 2
                                        #     Child Loop BB20_49 Depth 2
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	$0, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 8(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rbx)
	movq	$0, 32(%rbx)
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r12), %rcx
	movslq	12(%r12), %rax
	movq	%rbx, (%rcx,%rax,8)
	leal	1(%rax), %ecx
	movl	%ecx, 12(%r12)
	movq	40(%r13), %rdi
	movq	8(%rdi), %rsi
	movq	16(%rdi), %rdx
	cmpq	%rsi, %rdx
	jae	.LBB20_77
# BB#4:                                 # %_ZN8NArchive3N7z10CInArchive8ReadByteEv.exit
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	16(%r12), %rcx
	movq	(%rcx,%rax,8), %r12
	movq	(%rdi), %r9
	leaq	1(%rdx), %rbp
	movq	%rbp, 16(%rdi)
	movzbl	(%r9,%rdx), %r13d
	movl	%r13d, %eax
	andl	$15, %eax
	subq	%rbp, %rsi
	cmpq	%rax, %rsi
	jb	.LBB20_77
# BB#5:                                 # %.preheader.i.i
                                        #   in Loop: Header=BB20_3 Depth=1
	testl	%eax, %eax
	movdqa	.LCPI20_0(%rip), %xmm8  # xmm8 = [255,0,0,0,0,0,0,0,255,0,0,0,0,0,0,0]
	movdqa	.LCPI20_1(%rip), %xmm9  # xmm9 = [16,16]
	movdqa	.LCPI20_2(%rip), %xmm10 # xmm10 = [4,4]
	je	.LBB20_25
# BB#6:                                 # %.lr.ph.i.i
                                        #   in Loop: Header=BB20_3 Depth=1
	leaq	2(%rdx), %rsi
	movq	%rsi, 16(%rdi)
	movb	1(%r9,%rdx), %bl
	movb	%bl, 25(%rsp)
	cmpl	$1, %eax
	je	.LBB20_17
# BB#7:                                 # %._crit_edge8.i.i.preheader
                                        #   in Loop: Header=BB20_3 Depth=1
	leal	3(%rax), %ebx
	leaq	-2(%rax), %r8
	andq	$3, %rbx
	je	.LBB20_12
# BB#8:                                 # %._crit_edge8.i.i.prol.preheader
                                        #   in Loop: Header=BB20_3 Depth=1
	leaq	(%r9,%rdx), %rsi
	negq	%rbx
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB20_9:                               # %._crit_edge8.i.i.prol
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	2(%rdx,%rbp), %rcx
	movq	%rcx, 16(%rdi)
	movzbl	1(%rsi,%rbp), %ecx
	movb	%cl, 25(%rsp,%rbp)
	leaq	1(%rbx,%rbp), %rcx
	incq	%rbp
	cmpq	$1, %rcx
	jne	.LBB20_9
# BB#10:                                # %._crit_edge8.i.i.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB20_3 Depth=1
	leaq	1(%rdx,%rbp), %rsi
	cmpq	$3, %r8
	jae	.LBB20_13
	jmp	.LBB20_15
.LBB20_12:                              #   in Loop: Header=BB20_3 Depth=1
	movl	$1, %ebp
	cmpq	$3, %r8
	jb	.LBB20_15
.LBB20_13:                              # %._crit_edge8.i.i.preheader.new
                                        #   in Loop: Header=BB20_3 Depth=1
	leaq	3(%r9,%rsi), %rcx
	movq	%rax, %r8
	subq	%rbp, %r8
	leaq	28(%rsp), %rdx
	addq	%rdx, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_14:                              # %._crit_edge8.i.i
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	1(%rsi,%rbx), %rdx
	movq	%rdx, 16(%rdi)
	movzbl	-3(%rcx,%rbx), %edx
	movb	%dl, -3(%rbp,%rbx)
	leaq	2(%rsi,%rbx), %rdx
	movq	%rdx, 16(%rdi)
	movzbl	-2(%rcx,%rbx), %edx
	movb	%dl, -2(%rbp,%rbx)
	leaq	3(%rsi,%rbx), %rdx
	movq	%rdx, 16(%rdi)
	movzbl	-1(%rcx,%rbx), %edx
	movb	%dl, -1(%rbp,%rbx)
	leaq	4(%rsi,%rbx), %rdx
	movq	%rdx, 16(%rdi)
	movzbl	(%rcx,%rbx), %edx
	movb	%dl, (%rbp,%rbx)
	addq	$4, %rbx
	cmpq	%rbx, %r8
	jne	.LBB20_14
.LBB20_15:                              # %_ZN8NArchive3N7z10CInArchive9ReadBytesEPhm.exit
                                        #   in Loop: Header=BB20_3 Depth=1
	cmpl	$8, %eax
	ja	.LBB20_77
# BB#16:                                # %.preheader131
                                        #   in Loop: Header=BB20_3 Depth=1
	testl	%eax, %eax
	je	.LBB20_25
.LBB20_17:                              # %.lr.ph159
                                        #   in Loop: Header=BB20_3 Depth=1
	cmpl	$3, %eax
	jbe	.LBB20_22
# BB#18:                                # %min.iters.checked
                                        #   in Loop: Header=BB20_3 Depth=1
	movl	%r13d, %edx
	andl	$3, %edx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	je	.LBB20_22
# BB#19:                                # %vector.body.preheader
                                        #   in Loop: Header=BB20_3 Depth=1
	leaq	23(%rsp), %rsi
	leaq	(%rsi,%rax), %rsi
	movl	$1, %ebp
	movd	%rbp, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	xorpd	%xmm1, %xmm1
	movq	%rcx, %rbp
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB20_20:                              # %vector.body
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rsi), %ebx
	movd	%ebx, %xmm3
	movzwl	-2(%rsi), %ebx
	movd	%ebx, %xmm4
	punpcklbw	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$5, %xmm3, %xmm3        # xmm3 = xmm3[1,1,0,0,4,5,6,7]
	pshufd	$80, %xmm3, %xmm3       # xmm3 = xmm3[0,0,1,1]
	pand	%xmm8, %xmm3
	punpcklbw	%xmm4, %xmm4    # xmm4 = xmm4[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$5, %xmm4, %xmm4        # xmm4 = xmm4[1,1,0,0,4,5,6,7]
	pshufd	$80, %xmm4, %xmm4       # xmm4 = xmm4[0,0,1,1]
	pand	%xmm8, %xmm4
	movdqa	%xmm0, %xmm5
	psllq	$3, %xmm5
	pshufd	$78, %xmm5, %xmm6       # xmm6 = xmm5[2,3,0,1]
	movdqa	%xmm3, %xmm7
	psllq	%xmm5, %xmm3
	paddq	%xmm9, %xmm5
	psllq	%xmm6, %xmm7
	movsd	%xmm3, %xmm7            # xmm7 = xmm3[0],xmm7[1]
	pshufd	$78, %xmm5, %xmm3       # xmm3 = xmm5[2,3,0,1]
	movdqa	%xmm4, %xmm6
	psllq	%xmm3, %xmm6
	psllq	%xmm5, %xmm4
	movsd	%xmm4, %xmm6            # xmm6 = xmm4[0],xmm6[1]
	orpd	%xmm7, %xmm1
	orpd	%xmm6, %xmm2
	paddq	%xmm10, %xmm0
	addq	$-4, %rsi
	addq	$-4, %rbp
	jne	.LBB20_20
# BB#21:                                # %middle.block
                                        #   in Loop: Header=BB20_3 Depth=1
	orpd	%xmm1, %xmm2
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	por	%xmm2, %xmm0
	movd	%xmm0, %rsi
	testl	%edx, %edx
	jne	.LBB20_23
	jmp	.LBB20_26
	.p2align	4, 0x90
.LBB20_25:                              #   in Loop: Header=BB20_3 Depth=1
	xorl	%esi, %esi
	jmp	.LBB20_26
	.p2align	4, 0x90
.LBB20_22:                              #   in Loop: Header=BB20_3 Depth=1
	xorl	%ecx, %ecx
	xorl	%esi, %esi
.LBB20_23:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB20_3 Depth=1
	subq	%rcx, %rax
	shlq	$3, %rcx
	.p2align	4, 0x90
.LBB20_24:                              # %scalar.ph
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	24(%rsp,%rax), %edx
	shlq	%cl, %rdx
	orq	%rdx, %rsi
	addq	$8, %rcx
	decq	%rax
	jne	.LBB20_24
.LBB20_26:                              # %._crit_edge160
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	%rsi, (%r12)
	testb	$16, %r13b
	jne	.LBB20_28
# BB#27:                                #   in Loop: Header=BB20_3 Depth=1
	movl	$1, 32(%r12)
	movl	$1, %eax
	jmp	.LBB20_30
	.p2align	4, 0x90
.LBB20_28:                              #   in Loop: Header=BB20_3 Depth=1
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	testq	$-2147483648, %rax      # imm = 0x80000000
	jne	.LBB20_77
# BB#29:                                # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit100
                                        #   in Loop: Header=BB20_3 Depth=1
	movl	%eax, 32(%r12)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	40(%rax), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	testq	$-2147483648, %rax      # imm = 0x80000000
	jne	.LBB20_79
.LBB20_30:                              #   in Loop: Header=BB20_3 Depth=1
	movl	%eax, 36(%r12)
	testb	$32, %r13b
	je	.LBB20_50
# BB#31:                                #   in Loop: Header=BB20_3 Depth=1
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movl	%r15d, %r13d
	movq	%r14, %r15
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	40(%rax), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %rbx
	testq	$-2147483648, %rbx      # imm = 0x80000000
	jne	.LBB20_77
# BB#32:                                # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit102
                                        #   in Loop: Header=BB20_3 Depth=1
	movl	%ebx, %ebp
	movq	%r12, %rax
	movq	16(%rax), %r14
	cmpq	%rbp, %r14
	jne	.LBB20_34
# BB#33:                                # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit102._ZN7CBufferIhE11SetCapacityEm.exit_crit_edge
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	24(%rax), %rdi
	movq	%r15, %r14
	jmp	.LBB20_42
	.p2align	4, 0x90
.LBB20_34:                              #   in Loop: Header=BB20_3 Depth=1
	testq	%rbp, %rbp
	je	.LBB20_37
# BB#35:                                #   in Loop: Header=BB20_3 Depth=1
	movq	%rbp, %rdi
	callq	_Znam
	testq	%r14, %r14
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB20_38
# BB#36:                                #   in Loop: Header=BB20_3 Depth=1
	movq	%r12, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	24(%rcx), %rsi
	cmpq	%rbp, %r14
	cmovaeq	%rbp, %r14
	movq	%rax, %rdi
	movq	%r14, %rdx
	callq	memmove
	movq	40(%rsp), %rax          # 8-byte Reload
	jmp	.LBB20_39
.LBB20_37:                              #   in Loop: Header=BB20_3 Depth=1
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB20_38:                              #   in Loop: Header=BB20_3 Depth=1
	movq	%r12, %rax
.LBB20_39:                              #   in Loop: Header=BB20_3 Depth=1
	movq	24(%rax), %rdi
	testq	%rdi, %rdi
	movq	%r15, %r14
	je	.LBB20_41
# BB#40:                                #   in Loop: Header=BB20_3 Depth=1
	callq	_ZdaPv
.LBB20_41:                              #   in Loop: Header=BB20_3 Depth=1
	movq	%r12, %rax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, 24(%rax)
	movq	%rbp, 16(%rax)
.LBB20_42:                              # %_ZN7CBufferIhE11SetCapacityEm.exit
                                        #   in Loop: Header=BB20_3 Depth=1
	movl	%r13d, %r15d
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	40(%rax), %rax
	movq	8(%rax), %rdx
	movq	16(%rax), %rcx
	subq	%rcx, %rdx
	cmpq	%rbp, %rdx
	movq	48(%rsp), %r13          # 8-byte Reload
	jb	.LBB20_77
# BB#43:                                # %.preheader.i.i103
                                        #   in Loop: Header=BB20_3 Depth=1
	testq	%rbp, %rbp
	je	.LBB20_50
# BB#44:                                # %.lr.ph.i.i105
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	(%rax), %rdx
	leaq	1(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movb	(%rdx,%rcx), %cl
	movb	%cl, (%rdi)
	cmpq	$1, %rbp
	je	.LBB20_50
# BB#45:                                # %._crit_edge8.i.i108.preheader
                                        #   in Loop: Header=BB20_3 Depth=1
	testb	$1, %bl
	jne	.LBB20_47
# BB#46:                                # %._crit_edge8.i.i108.prol
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	(%rax), %rcx
	movq	16(%rax), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 16(%rax)
	movb	(%rcx,%rdx), %cl
	movb	%cl, 1(%rdi)
	movl	$2, %ecx
	cmpq	$2, %rbp
	jne	.LBB20_48
	jmp	.LBB20_50
.LBB20_47:                              #   in Loop: Header=BB20_3 Depth=1
	movl	$1, %ecx
	cmpq	$2, %rbp
	je	.LBB20_50
.LBB20_48:                              # %._crit_edge8.i.i108.preheader.new
                                        #   in Loop: Header=BB20_3 Depth=1
	subq	%rcx, %rbp
	leaq	1(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB20_49:                              # %._crit_edge8.i.i108
                                        #   Parent Loop BB20_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rdx
	movq	16(%rax), %rsi
	leaq	1(%rsi), %rdi
	movq	%rdi, 16(%rax)
	movzbl	(%rdx,%rsi), %edx
	movb	%dl, -1(%rcx)
	movq	(%rax), %rdx
	movq	16(%rax), %rsi
	leaq	1(%rsi), %rdi
	movq	%rdi, 16(%rax)
	movzbl	(%rdx,%rsi), %edx
	movb	%dl, (%rcx)
	addq	$2, %rcx
	addq	$-2, %rbp
	jne	.LBB20_49
	.p2align	4, 0x90
.LBB20_50:                              # %_ZN8NArchive3N7z10CInArchive9ReadBytesEPhm.exit109
                                        #   in Loop: Header=BB20_3 Depth=1
	testb	%r13b, %r13b
	js	.LBB20_77
# BB#51:                                #   in Loop: Header=BB20_3 Depth=1
	addl	32(%r12), %r14d
	addl	36(%r12), %r15d
	movl	(%rsp), %eax            # 4-byte Reload
	incl	%eax
	movq	56(%rsp), %rbx          # 8-byte Reload
	cmpl	%ebx, %eax
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %r13           # 8-byte Reload
	jb	.LBB20_3
# BB#52:                                # %._crit_edge164.loopexit
	movq	%r14, (%rsp)            # 8-byte Spill
	decl	%r15d
	jmp	.LBB20_54
.LBB20_53:
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	$-1, %r15d
.LBB20_54:                              # %._crit_edge164
	leaq	32(%r12), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r15d, %r15d
	je	.LBB20_59
# BB#55:                                # %.lr.ph156
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB20_56:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r13), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %rbp
	testq	$-2147483648, %rbp      # imm = 0x80000000
	jne	.LBB20_77
# BB#57:                                # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit111
                                        #   in Loop: Header=BB20_56 Depth=1
	movq	40(%r13), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	testq	$-2147483648, %rax      # imm = 0x80000000
	jne	.LBB20_77
# BB#58:                                # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit112
                                        #   in Loop: Header=BB20_56 Depth=1
	shlq	$32, %rax
	movl	%ebp, %ebp
	orq	%rax, %rbp
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	48(%r12), %rax
	movslq	44(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	incl	44(%r12)
	incl	%ebx
	cmpl	%r15d, %ebx
	jb	.LBB20_56
.LBB20_59:                              # %._crit_edge
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%ebx, %esi
	subl	%r15d, %esi
	movl	%r15d, %ebp
	jb	.LBB20_77
# BB#60:
	leaq	64(%r12), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	movl	%ebx, %r15d
	subl	%ebp, %r15d
	je	.LBB20_76
# BB#61:
	cmpl	$1, %r15d
	jne	.LBB20_70
# BB#62:                                # %.preheader
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB20_75
# BB#63:                                # %.lr.ph
	movslq	44(%r12), %rax
	testq	%rax, %rax
	jle	.LBB20_73
# BB#64:                                # %.lr.ph.split.us.preheader
	movq	48(%r12), %rcx
	xorl	%ebx, %ebx
.LBB20_65:                              # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_66 Depth 2
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB20_66:                              #   Parent Loop BB20_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebx, (%rcx,%rdx,8)
	je	.LBB20_68
# BB#67:                                #   in Loop: Header=BB20_66 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB20_66
	jmp	.LBB20_74
.LBB20_68:                              # %_ZNK8NArchive3N7z7CFolder23FindBindPairForInStreamEj.exit.us
                                        #   in Loop: Header=BB20_65 Depth=1
	testl	%edx, %edx
	js	.LBB20_74
# BB#69:                                #   in Loop: Header=BB20_65 Depth=1
	incl	%ebx
	cmpl	(%rsp), %ebx            # 4-byte Folded Reload
	jb	.LBB20_65
	jmp	.LBB20_75
.LBB20_70:                              # %.lr.ph154
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB20_71:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r13), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %rbx
	testq	$-2147483648, %rbx      # imm = 0x80000000
	jne	.LBB20_78
# BB#72:                                # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit115
                                        #   in Loop: Header=BB20_71 Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	80(%r12), %rax
	movslq	76(%r12), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	76(%r12)
	incl	%ebp
	cmpl	%r15d, %ebp
	jb	.LBB20_71
	jmp	.LBB20_76
.LBB20_73:
	xorl	%ebx, %ebx
.LBB20_74:                              # %_ZNK8NArchive3N7z7CFolder23FindBindPairForInStreamEj.exit.thread
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	80(%r12), %rax
	movslq	76(%r12), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	76(%r12)
.LBB20_75:                              # %.loopexit
	cmpl	$1, 76(%r12)
	jne	.LBB20_80
.LBB20_76:                              # %.loopexit130
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_77:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB20_78:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB20_79:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB20_80:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end20:
	.size	_ZN8NArchive3N7z10CInArchive17GetNextFolderItemERNS0_7CFolderE, .Lfunc_end20-_ZN8NArchive3N7z10CInArchive17GetNextFolderItemERNS0_7CFolderE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z10CInArchive13WaitAttributeEy
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive13WaitAttributeEy,@function
_ZN8NArchive3N7z10CInArchive13WaitAttributeEy: # @_ZN8NArchive3N7z10CInArchive13WaitAttributeEy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -32
.Lcfi113:
	.cfi_offset %r14, -24
.Lcfi114:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	jmp	.LBB21_1
	.p2align	4, 0x90
.LBB21_4:                               # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit
                                        #   in Loop: Header=BB21_1 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbx)
.LBB21_1:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r15), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	cmpq	%r14, %rax
	je	.LBB21_5
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB21_1 Depth=1
	testq	%rax, %rax
	je	.LBB21_6
# BB#3:                                 # %.critedge
                                        #   in Loop: Header=BB21_1 Depth=1
	movq	40(%r15), %rbx
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jae	.LBB21_4
.LBB21_6:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB21_5:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	_ZN8NArchive3N7z10CInArchive13WaitAttributeEy, .Lfunc_end21-_ZN8NArchive3N7z10CInArchive13WaitAttributeEy
	.cfi_endproc

	.globl	_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE,@function
_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE: # @_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 64
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	%rdi, %r15
	callq	_ZN8NArchive3N7z10CInArchive15ReadBoolVector2EiR13CRecordVectorIbE
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%ebp, %ebp
	jle	.LBB22_6
# BB#1:                                 # %.lr.ph
	movslq	%ebp, %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB22_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	cmpb	$0, (%rax,%rbp)
	movl	$0, %ebx
	je	.LBB22_5
# BB#3:                                 #   in Loop: Header=BB22_2 Depth=1
	movq	40(%r15), %rax
	movq	16(%rax), %rcx
	leaq	4(%rcx), %rdx
	cmpq	8(%rax), %rdx
	ja	.LBB22_7
# BB#4:                                 # %_ZN8NArchive3N7z10CInArchive10ReadUInt32Ev.exit
                                        #   in Loop: Header=BB22_2 Depth=1
	movq	(%rax), %rsi
	movl	(%rsi,%rcx), %ebx
	movq	%rdx, 16(%rax)
.LBB22_5:                               #   in Loop: Header=BB22_2 Depth=1
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	12(%r13)
	incq	%rbp
	cmpq	%r12, %rbp
	jl	.LBB22_2
.LBB22_6:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_7:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end22:
	.size	_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE, .Lfunc_end22-_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z10CInArchive15ReadBoolVector2EiR13CRecordVectorIbE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive15ReadBoolVector2EiR13CRecordVectorIbE,@function
_ZN8NArchive3N7z10CInArchive15ReadBoolVector2EiR13CRecordVectorIbE: # @_ZN8NArchive3N7z10CInArchive15ReadBoolVector2EiR13CRecordVectorIbE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi131:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi132:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 64
.Lcfi135:
	.cfi_offset %rbx, -56
.Lcfi136:
	.cfi_offset %r12, -48
.Lcfi137:
	.cfi_offset %r13, -40
.Lcfi138:
	.cfi_offset %r14, -32
.Lcfi139:
	.cfi_offset %r15, -24
.Lcfi140:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r13d
	movq	%rdi, %r14
	movq	40(%r14), %rax
	movq	16(%rax), %rcx
	cmpq	8(%rax), %rcx
	jae	.LBB23_11
# BB#1:                                 # %_ZN8NArchive3N7z10CInArchive8ReadByteEv.exit
	movq	(%rax), %rdx
	leaq	1(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movb	(%rdx,%rcx), %bl
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r15, %rdi
	movl	%r13d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	cmpb	$0, %bl
	je	.LBB23_4
# BB#2:                                 # %.preheader
	testl	%r13d, %r13d
	jle	.LBB23_10
	.p2align	4, 0x90
.LBB23_3:                               # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movb	$1, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	decl	%r13d
	jne	.LBB23_3
	jmp	.LBB23_10
.LBB23_4:
	testl	%r13d, %r13d
	jle	.LBB23_10
# BB#5:                                 # %.lr.ph.i
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB23_6:                               # =>This Inner Loop Header: Depth=1
	testb	%bpl, %bpl
	jne	.LBB23_9
# BB#7:                                 #   in Loop: Header=BB23_6 Depth=1
	movq	40(%r14), %rax
	movq	16(%rax), %rcx
	cmpq	8(%rax), %rcx
	jae	.LBB23_11
# BB#8:                                 # %_ZN8NArchive3N7z10CInArchive8ReadByteEv.exit.i
                                        #   in Loop: Header=BB23_6 Depth=1
	movq	(%rax), %rdx
	leaq	1(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movb	(%rdx,%rcx), %r12b
	movb	$-128, %bpl
.LBB23_9:                               #   in Loop: Header=BB23_6 Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	testb	%r12b, %bpl
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	setne	(%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	shrb	%bpl
	incl	%ebx
	cmpl	%r13d, %ebx
	jl	.LBB23_6
.LBB23_10:                              # %_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_11:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end23:
	.size	_ZN8NArchive3N7z10CInArchive15ReadBoolVector2EiR13CRecordVectorIbE, .Lfunc_end23-_ZN8NArchive3N7z10CInArchive15ReadBoolVector2EiR13CRecordVectorIbE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z10CInArchive12ReadPackInfoERyR13CRecordVectorIyERS3_IbERS3_IjE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive12ReadPackInfoERyR13CRecordVectorIyERS3_IbERS3_IjE,@function
_ZN8NArchive3N7z10CInArchive12ReadPackInfoERyR13CRecordVectorIyERS3_IbERS3_IjE: # @_ZN8NArchive3N7z10CInArchive12ReadPackInfoERyR13CRecordVectorIyERS3_IbERS3_IjE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi144:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi145:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi147:
	.cfi_def_cfa_offset 64
.Lcfi148:
	.cfi_offset %rbx, -56
.Lcfi149:
	.cfi_offset %r12, -48
.Lcfi150:
	.cfi_offset %r13, -40
.Lcfi151:
	.cfi_offset %r14, -32
.Lcfi152:
	.cfi_offset %r15, -24
.Lcfi153:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	40(%rbx), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, (%rbp)
	movq	40(%rbx), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %r12
	testq	$-2147483648, %r12      # imm = 0x80000000
	je	.LBB24_1
	jmp	.LBB24_20
	.p2align	4, 0x90
.LBB24_4:                               # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit.i
                                        #   in Loop: Header=BB24_1 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbp)
.LBB24_1:                               # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	cmpq	$9, %rax
	je	.LBB24_5
# BB#2:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB24_1 Depth=1
	testq	%rax, %rax
	je	.LBB24_20
# BB#3:                                 # %.critedge.i
                                        #   in Loop: Header=BB24_1 Depth=1
	movq	40(%rbx), %rbp
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbp), %rdx
	movq	16(%rbp), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jae	.LBB24_4
.LBB24_20:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB24_5:                               # %_ZN8NArchive3N7z10CInArchive13WaitAttributeEy.exit
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r13, %rdi
	movl	%r12d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	je	.LBB24_8
# BB#6:                                 # %.lr.ph34
	movl	%r12d, %r14d
	.p2align	4, 0x90
.LBB24_7:                               # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %rbp
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r13)
	decl	%r14d
	jne	.LBB24_7
.LBB24_8:                               # %.preheader.preheader
	movq	(%rsp), %r14            # 8-byte Reload
	jmp	.LBB24_9
	.p2align	4, 0x90
.LBB24_21:                              #   in Loop: Header=BB24_9 Depth=1
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE
.LBB24_9:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	cmpq	$10, %rax
	je	.LBB24_21
# BB#10:                                # %.preheader
                                        #   in Loop: Header=BB24_9 Depth=1
	testq	%rax, %rax
	je	.LBB24_13
# BB#11:                                #   in Loop: Header=BB24_9 Depth=1
	movq	40(%rbx), %rbp
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbp), %rdx
	movq	16(%rbp), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jb	.LBB24_22
# BB#12:                                # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit
                                        #   in Loop: Header=BB24_9 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbp)
	jmp	.LBB24_9
.LBB24_13:
	cmpl	$0, 12(%r15)
	jne	.LBB24_19
# BB#14:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r15, %rdi
	movl	%r12d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r12d, %r12d
	jle	.LBB24_17
# BB#15:                                # %.lr.ph.i31
	movl	%r12d, %ebx
	.p2align	4, 0x90
.LBB24_16:                              # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	decl	%ebx
	jne	.LBB24_16
.LBB24_17:                              # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit
	movq	%r14, %rdi
	movl	%r12d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	testl	%r12d, %r12d
	je	.LBB24_19
	.p2align	4, 0x90
.LBB24_18:                              # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movl	$0, (%rax,%rcx,4)
	incl	12(%r14)
	decl	%r12d
	jne	.LBB24_18
.LBB24_19:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_22:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end24:
	.size	_ZN8NArchive3N7z10CInArchive12ReadPackInfoERyR13CRecordVectorIyERS3_IbERS3_IjE, .Lfunc_end24-_ZN8NArchive3N7z10CInArchive12ReadPackInfoERyR13CRecordVectorIyERS3_IbERS3_IjE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z10CInArchive14ReadUnpackInfoEPK13CObjectVectorI7CBufferIhEERS2_INS0_7CFolderEE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive14ReadUnpackInfoEPK13CObjectVectorI7CBufferIhEERS2_INS0_7CFolderEE,@function
_ZN8NArchive3N7z10CInArchive14ReadUnpackInfoEPK13CObjectVectorI7CBufferIhEERS2_INS0_7CFolderEE: # @_ZN8NArchive3N7z10CInArchive14ReadUnpackInfoEPK13CObjectVectorI7CBufferIhEERS2_INS0_7CFolderEE
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi157:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi158:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi160:
	.cfi_def_cfa_offset 288
.Lcfi161:
	.cfi_offset %rbx, -56
.Lcfi162:
	.cfi_offset %r12, -48
.Lcfi163:
	.cfi_offset %r13, -40
.Lcfi164:
	.cfi_offset %r14, -32
.Lcfi165:
	.cfi_offset %r15, -24
.Lcfi166:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %r12
	jmp	.LBB25_1
	.p2align	4, 0x90
.LBB25_4:                               # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit.i
                                        #   in Loop: Header=BB25_1 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbp)
.LBB25_1:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r12), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	cmpq	$11, %rax
	je	.LBB25_5
# BB#2:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB25_1 Depth=1
	testq	%rax, %rax
	je	.LBB25_80
# BB#3:                                 # %.critedge.i
                                        #   in Loop: Header=BB25_1 Depth=1
	movq	40(%r12), %rbp
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbp), %rdx
	movq	16(%rbp), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jae	.LBB25_4
	jmp	.LBB25_80
.LBB25_5:                               # %_ZN8NArchive3N7z10CInArchive13WaitAttributeEy.exit
	movq	40(%r12), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, (%rsp)            # 8-byte Spill
	testq	$-2147483648, %rax      # imm = 0x80000000
	jne	.LBB25_80
# BB#6:                                 # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit
	movb	$0, 16(%rsp)
.Ltmp50:
	leaq	8(%rsp), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE
.Ltmp51:
# BB#7:
.Ltmp52:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp53:
# BB#8:
.Ltmp54:
	movq	%r13, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp55:
# BB#9:                                 # %.preheader74
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB25_17
# BB#10:                                # %.lr.ph97
	leaq	104(%rsp), %r15
	xorl	%ebx, %ebx
	leaq	96(%rsp), %r14
	.p2align	4, 0x90
.LBB25_11:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movq	$8, 120(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, 96(%rsp)
	movups	%xmm0, 32(%r15)
	movq	$8, 152(%rsp)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE+16, 128(%rsp)
	movups	%xmm0, 64(%r15)
	movq	$4, 184(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 160(%rsp)
	movups	%xmm0, 96(%r15)
	movq	$8, 216(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 192(%rsp)
	movb	$0, 228(%rsp)
.Ltmp57:
	movl	$136, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp58:
# BB#12:                                # %.noexc
                                        #   in Loop: Header=BB25_11 Depth=1
.Ltmp59:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	_ZN8NArchive3N7z7CFolderC2ERKS1_
.Ltmp60:
# BB#13:                                #   in Loop: Header=BB25_11 Depth=1
.Ltmp62:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp63:
# BB#14:                                #   in Loop: Header=BB25_11 Depth=1
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r13)
.Ltmp67:
	movq	%r14, %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp68:
# BB#15:                                #   in Loop: Header=BB25_11 Depth=1
	movslq	12(%r13), %rax
	movq	16(%r13), %rcx
	movq	-8(%rcx,%rax,8), %rsi
.Ltmp70:
	movq	%r12, %rdi
	callq	_ZN8NArchive3N7z10CInArchive17GetNextFolderItemERNS0_7CFolderE
.Ltmp71:
# BB#16:                                #   in Loop: Header=BB25_11 Depth=1
	incl	%ebx
	cmpl	(%rsp), %ebx            # 4-byte Folded Reload
	jb	.LBB25_11
.LBB25_17:                              # %._crit_edge98
	cmpb	$0, 16(%rsp)
	je	.LBB25_21
# BB#18:
	movq	8(%rsp), %rbx
	leaq	8(%rbx), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movslq	20(%rbx), %rax
	testq	%rax, %rax
	je	.LBB25_20
# BB#19:
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbx)
.LBB25_20:                              # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i
	movb	$0, 16(%rsp)
	jmp	.LBB25_21
	.p2align	4, 0x90
.LBB25_24:                              # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit.i67
                                        #   in Loop: Header=BB25_21 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbx)
.LBB25_21:                              # %_ZN8NArchive3N7z13CStreamSwitchD2Ev.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%r12), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	cmpq	$12, %rax
	je	.LBB25_25
# BB#22:                                # %.lr.ph.i65
                                        #   in Loop: Header=BB25_21 Depth=1
	testq	%rax, %rax
	je	.LBB25_80
# BB#23:                                # %.critedge.i66
                                        #   in Loop: Header=BB25_21 Depth=1
	movq	40(%r12), %rbx
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jae	.LBB25_24
.LBB25_80:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB25_25:                              # %_ZN8NArchive3N7z10CInArchive13WaitAttributeEy.exit68.preheader
	movq	(%rsp), %rax            # 8-byte Reload
	testl	%eax, %eax
	movq	%r13, 24(%rsp)          # 8-byte Spill
	je	.LBB25_34
# BB#26:                                # %.lr.ph95.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB25_27:                              # %.lr.ph95
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_54 Depth 2
                                        #     Child Loop BB25_57 Depth 2
                                        #     Child Loop BB25_68 Depth 2
	movq	16(%r13), %rax
	movslq	%r15d, %rcx
	movq	(%rax,%rcx,8), %rbx
	movslq	12(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB25_28
# BB#53:                                # %.lr.ph.i70
                                        #   in Loop: Header=BB25_27 Depth=1
	movq	16(%rbx), %rcx
	leaq	-1(%rax), %r8
	movq	%rax, %rdi
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	andq	$3, %rdi
	je	.LBB25_55
	.p2align	4, 0x90
.LBB25_54:                              #   Parent Loop BB25_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	addl	36(%rsi), %ebp
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB25_54
.LBB25_55:                              # %.prol.loopexit133
                                        #   in Loop: Header=BB25_27 Depth=1
	cmpq	$3, %r8
	jb	.LBB25_58
# BB#56:                                # %.lr.ph.i70.new
                                        #   in Loop: Header=BB25_27 Depth=1
	subq	%rdx, %rax
	leaq	24(%rcx,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB25_57:                              #   Parent Loop BB25_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rcx), %rdx
	movq	-16(%rcx), %rsi
	addl	36(%rdx), %ebp
	addl	36(%rsi), %ebp
	movq	-8(%rcx), %rdx
	addl	36(%rdx), %ebp
	movq	(%rcx), %rdx
	addl	36(%rdx), %ebp
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB25_57
.LBB25_58:                              # %_ZNK8NArchive3N7z7CFolder16GetNumOutStreamsEv.exit
                                        #   in Loop: Header=BB25_27 Depth=1
	leaq	96(%rbx), %r13
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%ebp, %ebp
	je	.LBB25_59
	.p2align	4, 0x90
.LBB25_68:                              #   Parent Loop BB25_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%r12), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	112(%rbx), %rax
	movslq	108(%rbx), %rcx
	movq	%r14, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 108(%rbx)
	decl	%ebp
	jne	.LBB25_68
	jmp	.LBB25_59
	.p2align	4, 0x90
.LBB25_28:                              # %_ZNK8NArchive3N7z7CFolder16GetNumOutStreamsEv.exit.thread
                                        #   in Loop: Header=BB25_27 Depth=1
	addq	$96, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.LBB25_59:                              # %_ZN8NArchive3N7z10CInArchive13WaitAttributeEy.exit68
                                        #   in Loop: Header=BB25_27 Depth=1
	incl	%r15d
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	%eax, %r15d
	movq	24(%rsp), %r13          # 8-byte Reload
	jne	.LBB25_27
.LBB25_34:                              # %.thread.preheader
	testl	%eax, %eax
	je	.LBB25_35
# BB#40:                                # %.thread.us.preheader
	movl	%eax, %r15d
	andl	$1, %r15d
	leaq	64(%rsp), %r13
	leaq	32(%rsp), %r14
	jmp	.LBB25_41
	.p2align	4, 0x90
.LBB25_44:                              # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit.us
                                        #   in Loop: Header=BB25_41 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbp)
.LBB25_41:                              # %.thread.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_81 Depth 2
	movq	40(%r12), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	cmpq	$10, %rax
	je	.LBB25_45
# BB#42:                                # %.thread.us
                                        #   in Loop: Header=BB25_41 Depth=1
	testq	%rax, %rax
	je	.LBB25_78
# BB#43:                                #   in Loop: Header=BB25_41 Depth=1
	movq	40(%r12), %rbp
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbp), %rdx
	movq	16(%rbp), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jae	.LBB25_44
	jmp	.LBB25_39
	.p2align	4, 0x90
.LBB25_45:                              #   in Loop: Header=BB25_41 Depth=1
	leaq	72(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$1, 88(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 64(%rsp)
	leaq	40(%rsp), %rax
	movups	%xmm0, (%rax)
	movq	$4, 56(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 32(%rsp)
.Ltmp75:
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE
.Ltmp76:
# BB#46:                                # %.lr.ph.us
                                        #   in Loop: Header=BB25_41 Depth=1
	xorl	%esi, %esi
	testl	%r15d, %r15d
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	80(%rsp), %rcx
	movq	48(%rsp), %rdx
	je	.LBB25_48
# BB#47:                                #   in Loop: Header=BB25_41 Depth=1
	movq	(%rax), %rsi
	movb	(%rcx), %bl
	movb	%bl, 132(%rsi)
	movl	(%rdx), %edi
	movl	%edi, 128(%rsi)
	movl	$1, %esi
.LBB25_48:                              # %.prol.loopexit
                                        #   in Loop: Header=BB25_41 Depth=1
	movq	(%rsp), %r8             # 8-byte Reload
	cmpl	$1, %r8d
	je	.LBB25_49
	.p2align	4, 0x90
.LBB25_81:                              #   Parent Loop BB25_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rdi
	movzbl	(%rcx,%rsi), %ebx
	movb	%bl, 132(%rdi)
	movl	(%rdx,%rsi,4), %ebp
	movl	%ebp, 128(%rdi)
	leal	1(%rsi), %edi
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rbp
	movzbl	(%rcx,%rdi), %ebx
	movb	%bl, 132(%rbp)
	movl	(%rdx,%rdi,4), %edi
	movl	%edi, 128(%rbp)
	addl	$2, %esi
	cmpl	%esi, %r8d
	jne	.LBB25_81
.LBB25_49:                              # %._crit_edge.us
                                        #   in Loop: Header=BB25_41 Depth=1
.Ltmp78:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp79:
# BB#50:                                #   in Loop: Header=BB25_41 Depth=1
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	jmp	.LBB25_41
.LBB25_35:
	leaq	64(%rsp), %r14
	leaq	32(%rsp), %rbp
	jmp	.LBB25_36
	.p2align	4, 0x90
.LBB25_77:                              # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit
                                        #   in Loop: Header=BB25_36 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbx)
.LBB25_36:                              # %.thread
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%r12), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	cmpq	$10, %rax
	je	.LBB25_71
# BB#37:                                # %.thread
                                        #   in Loop: Header=BB25_36 Depth=1
	testq	%rax, %rax
	je	.LBB25_78
# BB#38:                                #   in Loop: Header=BB25_36 Depth=1
	movq	40(%r12), %rbx
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jae	.LBB25_77
	jmp	.LBB25_39
	.p2align	4, 0x90
.LBB25_71:                              #   in Loop: Header=BB25_36 Depth=1
	leaq	72(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$1, 88(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 64(%rsp)
	leaq	40(%rsp), %rax
	movups	%xmm0, (%rax)
	movq	$4, 56(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 32(%rsp)
.Ltmp81:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	callq	_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE
.Ltmp82:
# BB#72:                                # %.preheader
                                        #   in Loop: Header=BB25_36 Depth=1
.Ltmp86:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp87:
# BB#73:                                #   in Loop: Header=BB25_36 Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	jmp	.LBB25_36
.LBB25_78:                              # %.us-lcssa.us
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_39:                              # %.us-lcssa90.us
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB25_60:
.Ltmp56:
	jmp	.LBB25_61
.LBB25_74:                              # %.us-lcssa88
.Ltmp88:
	jmp	.LBB25_75
.LBB25_69:                              # %.us-lcssa87
.Ltmp83:
	jmp	.LBB25_70
.LBB25_52:                              # %.us-lcssa88.us
.Ltmp80:
.LBB25_75:
	movq	%rax, %rbx
	jmp	.LBB25_76
.LBB25_51:                              # %.us-lcssa87.us
.Ltmp77:
.LBB25_70:
	movq	%rax, %rbx
.Ltmp84:
	leaq	32(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp85:
.LBB25_76:
.Ltmp89:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp90:
	jmp	.LBB25_67
.LBB25_33:
.Ltmp72:
	jmp	.LBB25_61
.LBB25_30:
.Ltmp69:
.LBB25_61:
	movq	%rax, %rbx
	cmpb	$0, 16(%rsp)
	jne	.LBB25_63
	jmp	.LBB25_67
.LBB25_29:
.Ltmp61:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB25_32
.LBB25_31:
.Ltmp64:
	movq	%rax, %rbx
.LBB25_32:                              # %.body
.Ltmp65:
	leaq	96(%rsp), %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp66:
# BB#62:
	cmpb	$0, 16(%rsp)
	je	.LBB25_67
.LBB25_63:
	movq	8(%rsp), %rbp
	leaq	8(%rbp), %rdi
.Ltmp73:
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp74:
# BB#64:                                # %.noexc72
	movslq	20(%rbp), %rax
	testq	%rax, %rax
	je	.LBB25_66
# BB#65:
	movq	24(%rbp), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbp)
.LBB25_66:                              # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i71
	movb	$0, 16(%rsp)
.LBB25_67:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB25_79:
.Ltmp91:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN8NArchive3N7z10CInArchive14ReadUnpackInfoEPK13CObjectVectorI7CBufferIhEERS2_INS0_7CFolderEE, .Lfunc_end25-_ZN8NArchive3N7z10CInArchive14ReadUnpackInfoEPK13CObjectVectorI7CBufferIhEERS2_INS0_7CFolderEE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\331\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp50-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp55-.Ltmp50         #   Call between .Ltmp50 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin2   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp64-.Lfunc_begin2   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin2   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin2   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin2   #     jumps to .Ltmp69
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin2   #     jumps to .Ltmp72
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp75-.Ltmp71         #   Call between .Ltmp71 and .Ltmp75
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin2   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin2   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp81-.Ltmp79         #   Call between .Ltmp79 and .Ltmp81
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin2   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp86-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin2   #     jumps to .Ltmp88
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin2   # >> Call Site 14 <<
	.long	.Ltmp84-.Ltmp87         #   Call between .Ltmp87 and .Ltmp84
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin2   # >> Call Site 15 <<
	.long	.Ltmp74-.Ltmp84         #   Call between .Ltmp84 and .Ltmp74
	.long	.Ltmp91-.Lfunc_begin2   #     jumps to .Ltmp91
	.byte	1                       #   On action: 1
	.long	.Ltmp74-.Lfunc_begin2   # >> Call Site 16 <<
	.long	.Lfunc_end25-.Ltmp74    #   Call between .Ltmp74 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z7CFolderD2Ev,"axG",@progbits,_ZN8NArchive3N7z7CFolderD2Ev,comdat
	.weak	_ZN8NArchive3N7z7CFolderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z7CFolderD2Ev,@function
_ZN8NArchive3N7z7CFolderD2Ev:           # @_ZN8NArchive3N7z7CFolderD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi167:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi168:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi169:
	.cfi_def_cfa_offset 32
.Lcfi170:
	.cfi_offset %rbx, -24
.Lcfi171:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	96(%rbx), %rdi
.Ltmp92:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp93:
# BB#1:
	leaq	64(%rbx), %rdi
.Ltmp97:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp98:
# BB#2:
	leaq	32(%rbx), %rdi
.Ltmp102:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp103:
# BB#3:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp114:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp115:
# BB#4:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB26_5:
.Ltmp116:
	movq	%rax, %r14
.Ltmp117:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp118:
	jmp	.LBB26_6
.LBB26_7:
.Ltmp119:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB26_9:
.Ltmp104:
	movq	%rax, %r14
	jmp	.LBB26_12
.LBB26_10:
.Ltmp99:
	movq	%rax, %r14
	jmp	.LBB26_11
.LBB26_8:
.Ltmp94:
	movq	%rax, %r14
	leaq	64(%rbx), %rdi
.Ltmp95:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp96:
.LBB26_11:
	leaq	32(%rbx), %rdi
.Ltmp100:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp101:
.LBB26_12:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp105:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp106:
# BB#13:
.Ltmp111:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp112:
.LBB26_6:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB26_14:
.Ltmp107:
	movq	%rax, %r14
.Ltmp108:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp109:
	jmp	.LBB26_17
.LBB26_15:
.Ltmp110:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB26_16:
.Ltmp113:
	movq	%rax, %r14
.LBB26_17:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end26:
	.size	_ZN8NArchive3N7z7CFolderD2Ev, .Lfunc_end26-_ZN8NArchive3N7z7CFolderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Ltmp92-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin3   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin3   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin3  #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin3  #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp117-.Ltmp115       #   Call between .Ltmp115 and .Ltmp117
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin3  #     jumps to .Ltmp119
	.byte	1                       #   On action: 1
	.long	.Ltmp95-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp101-.Ltmp95        #   Call between .Ltmp95 and .Ltmp101
	.long	.Ltmp113-.Lfunc_begin3  #     jumps to .Ltmp113
	.byte	1                       #   On action: 1
	.long	.Ltmp105-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp106-.Ltmp105       #   Call between .Ltmp105 and .Ltmp106
	.long	.Ltmp107-.Lfunc_begin3  #     jumps to .Ltmp107
	.byte	1                       #   On action: 1
	.long	.Ltmp111-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin3  #     jumps to .Ltmp113
	.byte	1                       #   On action: 1
	.long	.Ltmp112-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp108-.Ltmp112       #   Call between .Ltmp112 and .Ltmp108
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp108-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp109-.Ltmp108       #   Call between .Ltmp108 and .Ltmp109
	.long	.Ltmp110-.Lfunc_begin3  #     jumps to .Ltmp110
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z10CInArchive18ReadSubStreamsInfoERK13CObjectVectorINS0_7CFolderEER13CRecordVectorIjERS7_IyERS7_IbES9_
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive18ReadSubStreamsInfoERK13CObjectVectorINS0_7CFolderEER13CRecordVectorIjERS7_IyERS7_IbES9_,@function
_ZN8NArchive3N7z10CInArchive18ReadSubStreamsInfoERK13CObjectVectorINS0_7CFolderEER13CRecordVectorIjERS7_IyERS7_IbES9_: # @_ZN8NArchive3N7z10CInArchive18ReadSubStreamsInfoERK13CObjectVectorINS0_7CFolderEER13CRecordVectorIjERS7_IyERS7_IbES9_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi172:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi173:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi174:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi175:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi176:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi177:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi178:
	.cfi_def_cfa_offset 192
.Lcfi179:
	.cfi_offset %rbx, -56
.Lcfi180:
	.cfi_offset %r12, -48
.Lcfi181:
	.cfi_offset %r13, -40
.Lcfi182:
	.cfi_offset %r14, -32
.Lcfi183:
	.cfi_offset %r15, -24
.Lcfi184:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r14
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	12(%r12), %esi
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	movq	%rbx, %rax
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB27_1
	.p2align	4, 0x90
.LBB27_10:                              # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit
                                        #   in Loop: Header=BB27_1 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%r13)
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB27_1:                               # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_8 Depth 2
	movq	40(%rax), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	cmpq	$13, %rax
	je	.LBB27_6
# BB#2:                                 # %.backedge
                                        #   in Loop: Header=BB27_1 Depth=1
	leaq	-9(%rax), %rcx
	cmpq	$2, %rcx
	jb	.LBB27_11
# BB#3:                                 # %.backedge
                                        #   in Loop: Header=BB27_1 Depth=1
	testq	%rax, %rax
	je	.LBB27_11
# BB#4:                                 #   in Loop: Header=BB27_1 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rax), %r13
	movq	%r13, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%r13), %rdx
	movq	16(%r13), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jae	.LBB27_10
	jmp	.LBB27_5
	.p2align	4, 0x90
.LBB27_6:                               # %.preheader158
                                        #   in Loop: Header=BB27_1 Depth=1
	cmpl	$0, 12(%r12)
	movq	16(%rsp), %rax          # 8-byte Reload
	jle	.LBB27_1
# BB#7:                                 # %.lr.ph212.preheader
                                        #   in Loop: Header=BB27_1 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_8:                               # %.lr.ph212
                                        #   Parent Loop BB27_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%rax), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %rbp
	testq	$-2147483648, %rbp      # imm = 0x80000000
	jne	.LBB27_106
# BB#9:                                 # %_ZN8NArchive3N7z10CInArchive7ReadNumEv.exit
                                        #   in Loop: Header=BB27_8 Depth=2
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	12(%r13)
	incl	%ebx
	cmpl	12(%r12), %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	jl	.LBB27_8
	jmp	.LBB27_1
.LBB27_11:
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	12(%r13), %eax
	testl	%eax, %eax
	jne	.LBB27_15
# BB#12:
	movl	12(%r12), %ecx
	testl	%ecx, %ecx
	jle	.LBB27_15
# BB#13:                                # %.lr.ph210.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_14:                              # %.lr.ph210
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	movl	$1, (%rax,%rcx,4)
	movl	12(%r13), %eax
	incl	%eax
	movl	%eax, 12(%r13)
	incl	%ebx
	cmpl	12(%r12), %ebx
	jl	.LBB27_14
.LBB27_15:                              # %.preheader156
	testl	%eax, %eax
	movq	%r15, %rsi
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	jle	.LBB27_47
# BB#16:                                # %.lr.ph201
	cmpq	$9, 40(%rsp)            # 8-byte Folded Reload
	jne	.LBB27_17
# BB#21:                                # %.lr.ph201.split.us.preheader
	xorl	%edx, %edx
	movq	32(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_22:                              # %.lr.ph201.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_37 Depth 2
                                        #     Child Loop BB27_27 Depth 2
                                        #       Child Loop BB27_30 Depth 3
	movq	16(%r13), %rcx
	movl	(%rcx,%rdx,4), %ebx
	testl	%ebx, %ebx
	je	.LBB27_35
# BB#23:                                # %.lr.ph201.split.us
                                        #   in Loop: Header=BB27_22 Depth=1
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	cmpl	$1, %ebx
	jne	.LBB27_36
# BB#24:                                #   in Loop: Header=BB27_22 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB27_25
	.p2align	4, 0x90
.LBB27_36:                              # %.lr.ph197.split.us.us.preheader
                                        #   in Loop: Header=BB27_22 Depth=1
	movl	$1, %r12d
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB27_37:                              # %.lr.ph197.split.us.us
                                        #   Parent Loop BB27_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rax), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	addq	%rbp, %r13
	incl	%r12d
	cmpl	%ebx, %r12d
	jb	.LBB27_37
.LBB27_25:                              # %._crit_edge198.us-lcssa.us.us
                                        #   in Loop: Header=BB27_22 Depth=1
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	16(%r12), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movl	108(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB27_26
.LBB27_27:                              #   Parent Loop BB27_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_30 Depth 3
	testl	%ecx, %ecx
	jle	.LBB27_107
# BB#28:                                #   in Loop: Header=BB27_27 Depth=2
	decl	%ecx
	movslq	44(%rax), %rdx
	testq	%rdx, %rdx
	movq	32(%rsp), %rbp          # 8-byte Reload
	jle	.LBB27_33
# BB#29:                                # %.lr.ph.i.i.us
                                        #   in Loop: Header=BB27_27 Depth=2
	movq	48(%rax), %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB27_30:                              #   Parent Loop BB27_22 Depth=1
                                        #     Parent Loop BB27_27 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ecx, 4(%rdi,%rsi,8)
	je	.LBB27_32
# BB#31:                                #   in Loop: Header=BB27_30 Depth=3
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB27_30
	jmp	.LBB27_33
	.p2align	4, 0x90
.LBB27_32:                              # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.i.us
                                        #   in Loop: Header=BB27_27 Depth=2
	testl	%esi, %esi
	jns	.LBB27_27
	.p2align	4, 0x90
.LBB27_33:                              # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.thread.i.us
                                        #   in Loop: Header=BB27_22 Depth=1
	movq	112(%rax), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rbx
	jmp	.LBB27_34
	.p2align	4, 0x90
.LBB27_26:                              #   in Loop: Header=BB27_22 Depth=1
	xorl	%ebx, %ebx
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB27_34:                              # %_ZNK8NArchive3N7z7CFolder13GetUnpackSizeEv.exit.us
                                        #   in Loop: Header=BB27_22 Depth=1
	subq	%r13, %rbx
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%rbp), %rax
	movslq	12(%rbp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbp)
	movq	48(%rsp), %r13          # 8-byte Reload
	movl	12(%r13), %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB27_35:                              #   in Loop: Header=BB27_22 Depth=1
	incq	%rdx
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	movq	64(%rsp), %rsi          # 8-byte Reload
	jl	.LBB27_22
	jmp	.LBB27_47
.LBB27_17:                              # %.lr.ph201.split.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB27_18:                              # %.lr.ph201.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_38 Depth 2
                                        #       Child Loop BB27_41 Depth 3
	movq	16(%r13), %rcx
	cmpl	$0, (%rcx,%rbp,4)
	je	.LBB27_46
# BB#19:                                # %._crit_edge198
                                        #   in Loop: Header=BB27_18 Depth=1
	movq	16(%r12), %rax
	movq	(%rax,%rbp,8), %rax
	movl	108(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB27_20
.LBB27_38:                              #   Parent Loop BB27_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_41 Depth 3
	testl	%ecx, %ecx
	jle	.LBB27_107
# BB#39:                                #   in Loop: Header=BB27_38 Depth=2
	decl	%ecx
	movslq	44(%rax), %rdx
	testq	%rdx, %rdx
	movq	48(%rsp), %r13          # 8-byte Reload
	jle	.LBB27_44
# BB#40:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB27_38 Depth=2
	movq	48(%rax), %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB27_41:                              #   Parent Loop BB27_18 Depth=1
                                        #     Parent Loop BB27_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ecx, 4(%rdi,%rsi,8)
	je	.LBB27_43
# BB#42:                                #   in Loop: Header=BB27_41 Depth=3
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB27_41
	jmp	.LBB27_44
	.p2align	4, 0x90
.LBB27_43:                              # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.i
                                        #   in Loop: Header=BB27_38 Depth=2
	testl	%esi, %esi
	jns	.LBB27_38
	.p2align	4, 0x90
.LBB27_44:                              # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.thread.i
                                        #   in Loop: Header=BB27_18 Depth=1
	movq	112(%rax), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	jmp	.LBB27_45
	.p2align	4, 0x90
.LBB27_20:                              #   in Loop: Header=BB27_18 Depth=1
	xorl	%eax, %eax
.LBB27_45:                              # %_ZNK8NArchive3N7z7CFolder13GetUnpackSizeEv.exit
                                        #   in Loop: Header=BB27_18 Depth=1
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbx)
	movl	12(%r13), %eax
	movq	%r15, %rsi
.LBB27_46:                              #   in Loop: Header=BB27_18 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB27_18
.LBB27_47:                              # %._crit_edge202
	movq	%rsi, %r15
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpq	$9, %rax
	jne	.LBB27_49
# BB#48:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rax), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
.LBB27_49:
	movq	%rax, %r8
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB27_50
# BB#51:                                # %.lr.ph192
	movq	16(%r13), %rcx
	leaq	16(%r12), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	16(%r12), %r9
	testb	$1, %al
	jne	.LBB27_53
# BB#52:
	xorl	%esi, %esi
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	cmpl	$1, %eax
	jne	.LBB27_67
	jmp	.LBB27_58
.LBB27_50:                              # %..preheader153_crit_edge
	leaq	16(%r12), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	jmp	.LBB27_58
.LBB27_53:
	movl	(%rcx), %esi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	cmpl	$1, %esi
	jne	.LBB27_56
# BB#54:
	movq	(%r9), %rsi
	cmpb	$0, 132(%rsi)
	je	.LBB27_56
# BB#55:
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$1, %esi
	cmpl	$1, %eax
	jne	.LBB27_67
	jmp	.LBB27_58
.LBB27_56:
	movl	$1, %esi
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%edx, 12(%rsp)          # 4-byte Spill
	cmpl	$1, %eax
	je	.LBB27_58
	.p2align	4, 0x90
.LBB27_67:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edi
	cmpl	$1, %edi
	jne	.LBB27_69
# BB#68:                                #   in Loop: Header=BB27_67 Depth=1
	movq	(%r9,%rsi,8), %rbp
	cmpb	$0, 132(%rbp)
	jne	.LBB27_70
.LBB27_69:                              #   in Loop: Header=BB27_67 Depth=1
	addl	%edi, 12(%rsp)          # 4-byte Folded Spill
.LBB27_70:                              #   in Loop: Header=BB27_67 Depth=1
	addl	8(%rsp), %edi           # 4-byte Folded Reload
	movl	4(%rcx,%rsi,4), %ebx
	movl	%ebx, %edx
	cmpl	$1, %ebx
	jne	.LBB27_104
# BB#71:                                #   in Loop: Header=BB27_67 Depth=1
	movq	8(%r9,%rsi,8), %rbp
	cmpb	$0, 132(%rbp)
	je	.LBB27_104
# BB#72:                                #   in Loop: Header=BB27_67 Depth=1
	movl	%edx, %ebx
	jmp	.LBB27_105
	.p2align	4, 0x90
.LBB27_104:                             #   in Loop: Header=BB27_67 Depth=1
	movl	%edx, %ebx
	addl	%ebx, 12(%rsp)          # 4-byte Folded Spill
.LBB27_105:                             #   in Loop: Header=BB27_67 Depth=1
	addl	%edi, %ebx
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	addq	$2, %rsi
	cmpq	%rax, %rsi
	jl	.LBB27_67
.LBB27_58:                              # %.preheader153
	movq	%r8, %rax
	jmp	.LBB27_59
	.p2align	4, 0x90
.LBB27_89:                              # %._crit_edge
                                        #   in Loop: Header=BB27_59 Depth=1
.Ltmp135:
	movq	%rsi, %r15
	leaq	72(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp136:
# BB#90:                                #   in Loop: Header=BB27_59 Depth=1
	leaq	104(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.LBB27_102:                             #   in Loop: Header=BB27_59 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rax), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
.LBB27_59:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_76 Depth 2
                                        #       Child Loop BB27_80 Depth 3
	cmpq	$10, %rax
	movq	16(%rsp), %rdi          # 8-byte Reload
	je	.LBB27_73
# BB#60:                                #   in Loop: Header=BB27_59 Depth=1
	testq	%rax, %rax
	je	.LBB27_61
# BB#100:                               #   in Loop: Header=BB27_59 Depth=1
	movq	40(%rdi), %rbp
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbp), %rdx
	movq	16(%rbp), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jb	.LBB27_108
# BB#101:                               # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit147
                                        #   in Loop: Header=BB27_59 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbp)
	jmp	.LBB27_102
	.p2align	4, 0x90
.LBB27_73:                              #   in Loop: Header=BB27_59 Depth=1
	leaq	112(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$1, 128(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 104(%rsp)
	leaq	80(%rsp), %rax
	movups	%xmm0, (%rax)
	movq	$4, 96(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 72(%rsp)
.Ltmp120:
	movl	12(%rsp), %esi          # 4-byte Reload
	leaq	104(%rsp), %rdx
	leaq	72(%rsp), %rcx
	callq	_ZN8NArchive3N7z10CInArchive15ReadHashDigestsEiR13CRecordVectorIbERS2_IjE
.Ltmp121:
# BB#74:                                # %.preheader152
                                        #   in Loop: Header=BB27_59 Depth=1
	cmpl	$0, 12(%r12)
	movq	%r15, %rsi
	jle	.LBB27_89
# BB#75:                                # %.lr.ph187.preheader
                                        #   in Loop: Header=BB27_59 Depth=1
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB27_76:                              # %.lr.ph187
                                        #   Parent Loop BB27_59 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_80 Depth 3
	movq	16(%r13), %rax
	movl	(%rax,%rbp,4), %r12d
	testl	%r12d, %r12d
	je	.LBB27_77
# BB#78:                                # %.lr.ph187
                                        #   in Loop: Header=BB27_76 Depth=2
	cmpl	$1, %r12d
	jne	.LBB27_79
# BB#84:                                #   in Loop: Header=BB27_76 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rbx
	cmpb	$0, 132(%rbx)
	je	.LBB27_79
# BB#85:                                #   in Loop: Header=BB27_76 Depth=2
.Ltmp123:
	movl	%ecx, %r15d
	movq	%r14, %rdi
	movq	%rsi, %r12
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp124:
	movq	56(%rsp), %r13          # 8-byte Reload
# BB#86:                                #   in Loop: Header=BB27_76 Depth=2
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movb	$1, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	movl	128(%rbx), %ebx
.Ltmp125:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp126:
# BB#87:                                # %_ZN13CRecordVectorIjE3AddEj.exit149
                                        #   in Loop: Header=BB27_76 Depth=2
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	12(%r12)
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r13, %r12
	movq	48(%rsp), %r13          # 8-byte Reload
	jmp	.LBB27_88
	.p2align	4, 0x90
.LBB27_79:                              # %.lr.ph184.preheader
                                        #   in Loop: Header=BB27_76 Depth=2
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movslq	%ecx, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB27_80:                              # %.lr.ph184
                                        #   Parent Loop BB27_59 Depth=1
                                        #     Parent Loop BB27_76 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	120(%rsp), %rax
	movzbl	(%rax,%r13), %ebx
.Ltmp128:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp129:
	movq	64(%rsp), %rbp          # 8-byte Reload
# BB#81:                                #   in Loop: Header=BB27_80 Depth=3
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movb	%bl, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	movq	88(%rsp), %rax
	movl	(%rax,%r13,4), %ebx
.Ltmp130:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp131:
# BB#82:                                #   in Loop: Header=BB27_80 Depth=3
	movq	16(%rbp), %rax
	movslq	12(%rbp), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	12(%rbp)
	incl	%r15d
	incq	%r13
	cmpl	%r12d, %r15d
	jb	.LBB27_80
# BB#83:                                # %.loopexit151.loopexit
                                        #   in Loop: Header=BB27_76 Depth=2
	movl	24(%rsp), %ecx          # 4-byte Reload
	addl	%r15d, %ecx
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB27_88
	.p2align	4, 0x90
.LBB27_77:                              #   in Loop: Header=BB27_76 Depth=2
	movq	56(%rsp), %r12          # 8-byte Reload
.LBB27_88:                              # %.loopexit151
                                        #   in Loop: Header=BB27_76 Depth=2
	incq	%rbp
	movslq	12(%r12), %rax
	cmpq	%rax, %rbp
	jl	.LBB27_76
	jmp	.LBB27_89
.LBB27_61:
	cmpl	$0, 12(%r14)
	movl	8(%rsp), %ebp           # 4-byte Reload
	je	.LBB27_62
.LBB27_99:                              # %.loopexit
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB27_62:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%ebp, %ebp
	jle	.LBB27_98
# BB#63:
	movl	%ebp, %ebx
	.p2align	4, 0x90
.LBB27_64:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	16(%r14), %rax
	movslq	12(%r14), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r14)
	decl	%ebx
	jne	.LBB27_64
# BB#65:                                # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit
	movq	%r15, %rbx
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%rbx, %rdi
	testl	%ebp, %ebp
	jle	.LBB27_99
	.p2align	4, 0x90
.LBB27_66:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	%r15, %rdi
	movq	16(%rdi), %rax
	movslq	12(%rdi), %rcx
	movl	$0, (%rax,%rcx,4)
	incl	12(%rdi)
	decl	%ebp
	jne	.LBB27_66
	jmp	.LBB27_99
.LBB27_98:                              # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit.thread
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	jmp	.LBB27_99
.LBB27_106:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB27_5:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB27_107:                             # %.us-lcssa.us
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.LBB27_108:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB27_93:
.Ltmp137:
	movq	%rax, %rbx
	jmp	.LBB27_96
.LBB27_94:
.Ltmp122:
	jmp	.LBB27_95
.LBB27_91:
.Ltmp127:
	jmp	.LBB27_95
.LBB27_92:
.Ltmp132:
.LBB27_95:
	movq	%rax, %rbx
.Ltmp133:
	leaq	72(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp134:
.LBB27_96:
.Ltmp138:
	leaq	104(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp139:
# BB#97:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB27_103:
.Ltmp140:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN8NArchive3N7z10CInArchive18ReadSubStreamsInfoERK13CObjectVectorINS0_7CFolderEER13CRecordVectorIjERS7_IyERS7_IbES9_, .Lfunc_end27-_ZN8NArchive3N7z10CInArchive18ReadSubStreamsInfoERK13CObjectVectorINS0_7CFolderEER13CRecordVectorIjERS7_IyERS7_IbES9_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp135-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp135
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin4  #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp120-.Ltmp136       #   Call between .Ltmp136 and .Ltmp120
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin4  #     jumps to .Ltmp122
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp126-.Ltmp123       #   Call between .Ltmp123 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin4  #     jumps to .Ltmp127
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp131-.Ltmp128       #   Call between .Ltmp128 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin4  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp133-.Ltmp131       #   Call between .Ltmp131 and .Ltmp133
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp139-.Ltmp133       #   Call between .Ltmp133 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin4  #     jumps to .Ltmp140
	.byte	1                       #   On action: 1
	.long	.Ltmp139-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Lfunc_end27-.Ltmp139   #   Call between .Ltmp139 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z10CInArchive15ReadStreamsInfoEPK13CObjectVectorI7CBufferIhEERyR13CRecordVectorIyERS9_IbERS9_IjERS2_INS0_7CFolderEESF_SB_SD_SF_
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive15ReadStreamsInfoEPK13CObjectVectorI7CBufferIhEERyR13CRecordVectorIyERS9_IbERS9_IjERS2_INS0_7CFolderEESF_SB_SD_SF_,@function
_ZN8NArchive3N7z10CInArchive15ReadStreamsInfoEPK13CObjectVectorI7CBufferIhEERyR13CRecordVectorIyERS9_IbERS9_IjERS2_INS0_7CFolderEESF_SB_SD_SF_: # @_ZN8NArchive3N7z10CInArchive15ReadStreamsInfoEPK13CObjectVectorI7CBufferIhEERyR13CRecordVectorIyERS9_IbERS9_IjERS2_INS0_7CFolderEESF_SB_SD_SF_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi187:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi188:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi189:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi190:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi191:
	.cfi_def_cfa_offset 64
.Lcfi192:
	.cfi_offset %rbx, -56
.Lcfi193:
	.cfi_offset %r12, -48
.Lcfi194:
	.cfi_offset %r13, -40
.Lcfi195:
	.cfi_offset %r14, -32
.Lcfi196:
	.cfi_offset %r15, -24
.Lcfi197:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	jmp	.LBB28_1
	.p2align	4, 0x90
.LBB28_7:                               #   in Loop: Header=BB28_1 Depth=1
	movq	%rbp, %rdi
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rdx
	movq	80(%rsp), %rcx
	movq	88(%rsp), %r8
	movq	96(%rsp), %r9
	callq	_ZN8NArchive3N7z10CInArchive18ReadSubStreamsInfoERK13CObjectVectorINS0_7CFolderEER13CRecordVectorIjERS7_IyERS7_IbES9_
.LBB28_1:                               # =>This Inner Loop Header: Depth=1
	movq	40(%rbp), %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	cmpq	$1073741824, %rax       # imm = 0x40000000
	ja	.LBB28_5
# BB#2:                                 # %.lr.ph
                                        #   in Loop: Header=BB28_1 Depth=1
	cmpl	$8, %eax
	ja	.LBB28_8
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB28_1 Depth=1
	movl	%eax, %eax
	jmpq	*.LJTI28_0(,%rax,8)
.LBB28_4:                               #   in Loop: Header=BB28_1 Depth=1
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	_ZN8NArchive3N7z10CInArchive12ReadPackInfoERyR13CRecordVectorIyERS3_IbERS3_IjE
	jmp	.LBB28_1
	.p2align	4, 0x90
.LBB28_6:                               #   in Loop: Header=BB28_1 Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	64(%rsp), %rdx
	callq	_ZN8NArchive3N7z10CInArchive14ReadUnpackInfoEPK13CObjectVectorI7CBufferIhEERS2_INS0_7CFolderEE
	jmp	.LBB28_1
.LBB28_9:                               # %.critedge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB28_8:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB28_5:                               # %._crit_edge
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end28:
	.size	_ZN8NArchive3N7z10CInArchive15ReadStreamsInfoEPK13CObjectVectorI7CBufferIhEERyR13CRecordVectorIyERS9_IbERS9_IjERS2_INS0_7CFolderEESF_SB_SD_SF_, .Lfunc_end28-_ZN8NArchive3N7z10CInArchive15ReadStreamsInfoEPK13CObjectVectorI7CBufferIhEERyR13CRecordVectorIyERS9_IbERS9_IjERS2_INS0_7CFolderEESF_SB_SD_SF_
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI28_0:
	.quad	.LBB28_9
	.quad	.LBB28_8
	.quad	.LBB28_8
	.quad	.LBB28_8
	.quad	.LBB28_8
	.quad	.LBB28_8
	.quad	.LBB28_4
	.quad	.LBB28_6
	.quad	.LBB28_7

	.text
	.globl	_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE,@function
_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE: # @_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi198:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi199:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi200:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi201:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi202:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi204:
	.cfi_def_cfa_offset 64
.Lcfi205:
	.cfi_offset %rbx, -56
.Lcfi206:
	.cfi_offset %r12, -48
.Lcfi207:
	.cfi_offset %r13, -40
.Lcfi208:
	.cfi_offset %r14, -32
.Lcfi209:
	.cfi_offset %r15, -24
.Lcfi210:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movl	%esi, %r15d
	movq	%rdi, %r14
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%r13, %rdi
	movl	%r15d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
	testl	%r15d, %r15d
	jle	.LBB29_6
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB29_2:                               # =>This Inner Loop Header: Depth=1
	testb	%bl, %bl
	jne	.LBB29_5
# BB#3:                                 #   in Loop: Header=BB29_2 Depth=1
	movq	40(%r14), %rax
	movq	16(%rax), %rcx
	cmpq	8(%rax), %rcx
	jae	.LBB29_7
# BB#4:                                 # %_ZN8NArchive3N7z10CInArchive8ReadByteEv.exit
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	(%rax), %rdx
	leaq	1(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movb	(%rdx,%rcx), %r12b
	movb	$-128, %bl
.LBB29_5:                               #   in Loop: Header=BB29_2 Depth=1
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	testb	%r12b, %bl
	movq	16(%r13), %rax
	movslq	12(%r13), %rcx
	setne	(%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r13)
	shrb	%bl
	incl	%ebp
	cmpl	%r15d, %ebp
	jl	.LBB29_2
.LBB29_6:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_7:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end29:
	.size	_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE, .Lfunc_end29-_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE
	.cfi_endproc

	.globl	_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi,@function
_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi: # @_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi211:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi212:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi213:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi214:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi215:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi216:
	.cfi_def_cfa_offset 64
.Lcfi217:
	.cfi_offset %rbx, -48
.Lcfi218:
	.cfi_offset %r12, -40
.Lcfi219:
	.cfi_offset %r14, -32
.Lcfi220:
	.cfi_offset %r15, -24
.Lcfi221:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r14
	leaq	32(%rbx), %rdx
	movl	%ebp, %esi
	callq	_ZN8NArchive3N7z10CInArchive15ReadBoolVector2EiR13CRecordVectorIbE
	movb	$0, 8(%rsp)
.Ltmp141:
	movq	%rsp, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE
.Ltmp142:
# BB#1:
.Ltmp143:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp144:
# BB#2:                                 # %.preheader
	testl	%ebp, %ebp
	jle	.LBB30_6
# BB#3:                                 # %.lr.ph
	movslq	%ebp, %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_4:                               # =>This Inner Loop Header: Depth=1
	movq	48(%rbx), %rax
	cmpb	$0, (%rax,%rbp)
	je	.LBB30_5
# BB#11:                                #   in Loop: Header=BB30_4 Depth=1
	movq	40(%r14), %rax
	movq	16(%rax), %rcx
	leaq	8(%rcx), %rdx
	cmpq	8(%rax), %rdx
	ja	.LBB30_12
# BB#16:                                # %_ZN8NArchive3N7z10CInArchive10ReadUInt64Ev.exit
                                        #   in Loop: Header=BB30_4 Depth=1
	movq	(%rax), %rsi
	movq	(%rsi,%rcx), %r12
	movq	%rdx, 16(%rax)
	jmp	.LBB30_17
	.p2align	4, 0x90
.LBB30_5:                               #   in Loop: Header=BB30_4 Depth=1
	xorl	%r12d, %r12d
.LBB30_17:                              #   in Loop: Header=BB30_4 Depth=1
.Ltmp149:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp150:
# BB#18:                                #   in Loop: Header=BB30_4 Depth=1
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbx)
	incq	%rbp
	cmpq	%r15, %rbp
	jl	.LBB30_4
.LBB30_6:                               # %._crit_edge
	cmpb	$0, 8(%rsp)
	je	.LBB30_10
# BB#7:
	movq	(%rsp), %rbx
	leaq	8(%rbx), %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movslq	20(%rbx), %rax
	testq	%rax, %rax
	je	.LBB30_9
# BB#8:
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbx)
.LBB30_9:                               # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i
	movb	$0, 8(%rsp)
.LBB30_10:                              # %_ZN8NArchive3N7z13CStreamSwitchD2Ev.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB30_12:
.Ltmp146:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp147:
# BB#13:                                # %.noexc25
.LBB30_15:                              # %.loopexit.split-lp
.Ltmp148:
	jmp	.LBB30_20
.LBB30_19:
.Ltmp145:
	jmp	.LBB30_20
.LBB30_14:                              # %.loopexit
.Ltmp151:
.LBB30_20:
	movq	%rax, %rbx
	cmpb	$0, 8(%rsp)
	je	.LBB30_25
# BB#21:
	movq	(%rsp), %rbp
	leaq	8(%rbp), %rdi
.Ltmp152:
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp153:
# BB#22:                                # %.noexc
	movslq	20(%rbp), %rax
	testq	%rax, %rax
	je	.LBB30_24
# BB#23:
	movq	24(%rbp), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbp)
.LBB30_24:                              # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i23
	movb	$0, 8(%rsp)
.LBB30_25:                              # %_ZN8NArchive3N7z13CStreamSwitchD2Ev.exit24
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB30_26:
.Ltmp154:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi, .Lfunc_end30-_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp141-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp141
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp141-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp144-.Ltmp141       #   Call between .Ltmp141 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin5  #     jumps to .Ltmp145
	.byte	0                       #   On action: cleanup
	.long	.Ltmp149-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp150-.Ltmp149       #   Call between .Ltmp149 and .Ltmp150
	.long	.Ltmp151-.Lfunc_begin5  #     jumps to .Ltmp151
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp146-.Ltmp150       #   Call between .Ltmp150 and .Ltmp146
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp147-.Ltmp146       #   Call between .Ltmp146 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin5  #     jumps to .Ltmp148
	.byte	0                       #   On action: cleanup
	.long	.Ltmp152-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp153-.Ltmp152       #   Call between .Ltmp152 and .Ltmp153
	.long	.Ltmp154-.Lfunc_begin5  #     jumps to .Ltmp154
	.byte	1                       #   On action: 1
	.long	.Ltmp153-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Lfunc_end30-.Ltmp153   #   Call between .Ltmp153 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI31_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z10CInArchive26ReadAndDecodePackedStreamsEyRyR13CObjectVectorI7CBufferIhEEP22ICryptoGetTextPasswordRb
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive26ReadAndDecodePackedStreamsEyRyR13CObjectVectorI7CBufferIhEEP22ICryptoGetTextPasswordRb,@function
_ZN8NArchive3N7z10CInArchive26ReadAndDecodePackedStreamsEyRyR13CObjectVectorI7CBufferIhEEP22ICryptoGetTextPasswordRb: # @_ZN8NArchive3N7z10CInArchive26ReadAndDecodePackedStreamsEyRyR13CObjectVectorI7CBufferIhEEP22ICryptoGetTextPasswordRb
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi222:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi223:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi224:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi225:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi226:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi227:
	.cfi_def_cfa_offset 56
	subq	$568, %rsp              # imm = 0x238
.Lcfi228:
	.cfi_def_cfa_offset 624
.Lcfi229:
	.cfi_offset %rbx, -56
.Lcfi230:
	.cfi_offset %r12, -48
.Lcfi231:
	.cfi_offset %r13, -40
.Lcfi232:
	.cfi_offset %r14, -32
.Lcfi233:
	.cfi_offset %r15, -24
.Lcfi234:
	.cfi_offset %rbp, -16
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movq	%rcx, %r12
	movq	%rdx, %r15
	movq	%rsi, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 120(%rsp)
	movq	$8, 136(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 112(%rsp)
	movups	%xmm0, 312(%rsp)
	movq	$1, 328(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 304(%rsp)
	movups	%xmm0, 280(%rsp)
	movq	$4, 296(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 272(%rsp)
	movups	%xmm0, 56(%rsp)
	movq	$8, 72(%rsp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 48(%rsp)
	movups	%xmm0, 248(%rsp)
	movq	$4, 264(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 240(%rsp)
	movups	%xmm0, 216(%rsp)
	movq	$8, 232(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 208(%rsp)
	movups	%xmm0, 184(%rsp)
	movq	$1, 200(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 176(%rsp)
	movups	%xmm0, 152(%rsp)
	movq	$4, 168(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 144(%rsp)
.Ltmp155:
.Lcfi235:
	.cfi_escape 0x2e, 0x30
	subq	$8, %rsp
.Lcfi236:
	.cfi_adjust_cfa_offset 8
	leaq	152(%rsp), %rax
	leaq	184(%rsp), %r14
	leaq	216(%rsp), %r10
	leaq	248(%rsp), %r11
	leaq	56(%rsp), %rbx
	leaq	120(%rsp), %rcx
	leaq	312(%rsp), %r8
	leaq	280(%rsp), %r9
	movl	$0, %esi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	pushq	%rax
.Lcfi237:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi238:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi239:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi240:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	pushq	%rbx
.Lcfi241:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive3N7z10CInArchive15ReadStreamsInfoEPK13CObjectVectorI7CBufferIhEERyR13CRecordVectorIyERS9_IbERS9_IjERS2_INS0_7CFolderEESF_SB_SD_SF_
	addq	$48, %rsp
.Lcfi242:
	.cfi_adjust_cfa_offset -48
.Ltmp156:
# BB#1:
.Ltmp158:
.Lcfi243:
	.cfi_escape 0x2e, 0x00
	leaq	336(%rsp), %rdi
	movl	$1, %esi
	callq	_ZN8NArchive3N7z8CDecoderC1Eb
.Ltmp159:
# BB#2:
	cmpl	$0, 60(%rsp)
	jle	.LBB31_43
# BB#3:                                 # %.lr.ph169
	addq	(%r15), %rbp
	xorl	%r13d, %r13d
                                        # implicit-def: %EAX
	movl	%eax, 12(%rsp)          # 4-byte Spill
	xorl	%r14d, %r14d
	movq	%r12, 88(%rsp)          # 8-byte Spill
	jmp	.LBB31_14
	.p2align	4, 0x90
.LBB31_4:                               #   in Loop: Header=BB31_14 Depth=1
	incq	%r13
	movslq	60(%rsp), %rax
	cmpq	%rax, %r13
	jl	.LBB31_14
	jmp	.LBB31_43
	.p2align	4, 0x90
.LBB31_5:                               #   in Loop: Header=BB31_14 Depth=1
	cmpb	$0, 132(%r15)
	je	.LBB31_8
# BB#6:                                 #   in Loop: Header=BB31_14 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
.Ltmp180:
.Lcfi244:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rsi
	callq	CrcCalc
.Ltmp181:
# BB#7:                                 #   in Loop: Header=BB31_14 Depth=1
	cmpl	128(%r15), %eax
	jne	.LBB31_56
.LBB31_8:                               # %.preheader
                                        #   in Loop: Header=BB31_14 Depth=1
	movl	76(%r15), %r8d
	testl	%r8d, %r8d
	movq	40(%rsp), %r13          # 8-byte Reload
	jle	.LBB31_11
# BB#9:                                 # %.lr.ph
                                        #   in Loop: Header=BB31_14 Depth=1
	movq	128(%rsp), %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	88(%rax), %rdx
	xorl	%r15d, %r15d
	testb	$1, %r8b
	jne	.LBB31_12
# BB#10:                                #   in Loop: Header=BB31_14 Depth=1
	xorl	%esi, %esi
	cmpl	$1, %r8d
	jne	.LBB31_13
	jmp	.LBB31_40
.LBB31_11:                              #   in Loop: Header=BB31_14 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB31_40
.LBB31_12:                              #   in Loop: Header=BB31_14 Depth=1
	leal	1(%rbx), %r14d
	movq	(%rcx,%rbx,8), %rsi
	addq	%rsi, %rbp
	addq	%rsi, %rdx
	movq	%rdx, 88(%rax)
	movl	$1, %esi
	cmpl	$1, %r8d
	je	.LBB31_40
	.p2align	4, 0x90
.LBB31_13:                              #   Parent Loop BB31_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r14d, %rdi
	incl	%r14d
	movq	(%rcx,%rdi,8), %rbx
	addq	%rbx, %rbp
	addq	%rbx, %rdx
	movq	%rdx, 88(%rax)
	movslq	%r14d, %rbx
	movl	%edi, %r14d
	addl	$2, %r14d
	movq	(%rcx,%rbx,8), %rdi
	addq	%rdi, %rbp
	addq	%rdi, %rdx
	movq	%rdx, 88(%rax)
	addl	$2, %esi
	cmpl	%r8d, %esi
	jl	.LBB31_13
	jmp	.LBB31_40
	.p2align	4, 0x90
.LBB31_14:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_17 Depth 2
                                        #       Child Loop BB31_20 Depth 3
                                        #     Child Loop BB31_13 Depth 2
	movq	64(%rsp), %rax
	movq	(%rax,%r13,8), %r15
.Ltmp161:
.Lcfi245:
	.cfi_escape 0x2e, 0x00
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp162:
# BB#15:                                # %_ZN7CBufferIhEC2ERKS0_.exit.i
                                        #   in Loop: Header=BB31_14 Depth=1
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
.Ltmp163:
.Lcfi246:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp164:
# BB#16:                                #   in Loop: Header=BB31_14 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	movq	16(%r12), %rax
	movq	(%rax,%rcx,8), %rdi
	movl	108(%r15), %eax
	testl	%eax, %eax
	movq	%r13, 40(%rsp)          # 8-byte Spill
	je	.LBB31_24
.LBB31_17:                              #   Parent Loop BB31_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_20 Depth 3
	testl	%eax, %eax
	jle	.LBB31_54
# BB#18:                                #   in Loop: Header=BB31_17 Depth=2
	decl	%eax
	movslq	44(%r15), %rcx
	testq	%rcx, %rcx
	jle	.LBB31_23
# BB#19:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB31_17 Depth=2
	movq	48(%r15), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB31_20:                              #   Parent Loop BB31_14 Depth=1
                                        #     Parent Loop BB31_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%eax, 4(%rsi,%rdx,8)
	je	.LBB31_22
# BB#21:                                #   in Loop: Header=BB31_20 Depth=3
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB31_20
	jmp	.LBB31_23
	.p2align	4, 0x90
.LBB31_22:                              # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.i
                                        #   in Loop: Header=BB31_17 Depth=2
	testl	%edx, %edx
	jns	.LBB31_17
	.p2align	4, 0x90
.LBB31_23:                              # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.thread.i
                                        #   in Loop: Header=BB31_14 Depth=1
	movq	112(%r15), %rcx
	cltq
	movq	(%rcx,%rax,8), %r13
	jmp	.LBB31_25
	.p2align	4, 0x90
.LBB31_24:                              #   in Loop: Header=BB31_14 Depth=1
	xorl	%r13d, %r13d
.LBB31_25:                              # %_ZNK8NArchive3N7z7CFolder13GetUnpackSizeEv.exit
                                        #   in Loop: Header=BB31_14 Depth=1
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rbx
	cmpq	%r13, %rbx
	je	.LBB31_35
# BB#26:                                #   in Loop: Header=BB31_14 Depth=1
	testq	%r13, %r13
	je	.LBB31_30
# BB#27:                                #   in Loop: Header=BB31_14 Depth=1
.Ltmp169:
.Lcfi247:
	.cfi_escape 0x2e, 0x00
	movq	%r13, %rdi
	callq	_Znam
.Ltmp170:
# BB#28:                                # %.noexc118
                                        #   in Loop: Header=BB31_14 Depth=1
	testq	%rbx, %rbx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB31_31
# BB#29:                                #   in Loop: Header=BB31_14 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	16(%r12), %rsi
	cmpq	%r13, %rbx
	cmovaeq	%r13, %rbx
.Lcfi248:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memmove
	jmp	.LBB31_32
.LBB31_30:                              #   in Loop: Header=BB31_14 Depth=1
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
.LBB31_31:                              #   in Loop: Header=BB31_14 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB31_32:                              #   in Loop: Header=BB31_14 Depth=1
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB31_34
# BB#33:                                #   in Loop: Header=BB31_14 Depth=1
.Lcfi249:
	.cfi_escape 0x2e, 0x00
	callq	_ZdaPv
.LBB31_34:                              #   in Loop: Header=BB31_14 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%r12)
	movq	%r13, 8(%r12)
.LBB31_35:                              # %_ZN7CBufferIhE11SetCapacityEm.exit
                                        #   in Loop: Header=BB31_14 Depth=1
.Ltmp171:
.Lcfi250:
	.cfi_escape 0x2e, 0x00
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp172:
# BB#36:                                #   in Loop: Header=BB31_14 Depth=1
	movl	$0, 8(%r12)
	movq	$_ZTV19CBufPtrSeqOutStream+16, (%r12)
.Ltmp174:
.Lcfi251:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*_ZTV19CBufPtrSeqOutStream+24(%rip)
.Ltmp175:
# BB#37:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
                                        #   in Loop: Header=BB31_14 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	%rax, 16(%r12)
	movq	$0, 32(%r12)
	movq	%r13, 24(%r12)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movslq	%r14d, %rbx
	leaq	(,%rbx,8), %rcx
	addq	128(%rsp), %rcx
.Ltmp177:
.Lcfi252:
	.cfi_escape 0x2e, 0x30
	subq	$8, %rsp
.Lcfi253:
	.cfi_adjust_cfa_offset 8
	leaq	344(%rsp), %rdi
	movq	%rbp, %rdx
	movq	%r15, %r8
	movq	%r12, %r9
	pushq	$1
.Lcfi254:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi255:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi256:
	.cfi_adjust_cfa_offset 8
	pushq	128(%rsp)               # 8-byte Folded Reload
.Lcfi257:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi258:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive3N7z8CDecoder6DecodeEP9IInStreamyPKyRKNS0_7CFolderEP20ISequentialOutStreamP21ICompressProgressInfoP22ICryptoGetTextPasswordRbbj
	addq	$48, %rsp
.Lcfi259:
	.cfi_adjust_cfa_offset -48
.Ltmp178:
# BB#38:                                #   in Loop: Header=BB31_14 Depth=1
	testl	%eax, %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	je	.LBB31_5
# BB#39:                                #   in Loop: Header=BB31_14 Depth=1
	movq	40(%rsp), %r13          # 8-byte Reload
	movl	$1, %r15d
.LBB31_40:                              # %.loopexit
                                        #   in Loop: Header=BB31_14 Depth=1
	movq	(%r12), %rax
.Ltmp188:
.Lcfi260:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp189:
# BB#41:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit122
                                        #   in Loop: Header=BB31_14 Depth=1
	testl	%r15d, %r15d
	movq	88(%rsp), %r12          # 8-byte Reload
	je	.LBB31_4
# BB#42:
	movl	12(%rsp), %ebx          # 4-byte Reload
	jmp	.LBB31_44
.LBB31_43:
	xorl	%ebx, %ebx
.LBB31_44:                              # %._crit_edge
.Ltmp193:
.Lcfi261:
	.cfi_escape 0x2e, 0x00
	leaq	336(%rsp), %rdi
	callq	_ZN8NArchive3N7z8CDecoderD2Ev
.Ltmp194:
# BB#45:
.Ltmp198:
.Lcfi262:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp199:
# BB#46:
.Ltmp203:
.Lcfi263:
	.cfi_escape 0x2e, 0x00
	leaq	176(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp204:
# BB#47:
.Ltmp208:
.Lcfi264:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp209:
# BB#48:
.Ltmp213:
.Lcfi265:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp214:
# BB#49:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 48(%rsp)
.Ltmp224:
.Lcfi266:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp225:
# BB#50:
.Ltmp230:
.Lcfi267:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp231:
# BB#51:                                # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit112
.Ltmp235:
.Lcfi268:
	.cfi_escape 0x2e, 0x00
	leaq	272(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp236:
# BB#52:
.Ltmp240:
.Lcfi269:
	.cfi_escape 0x2e, 0x00
	leaq	304(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp241:
# BB#53:
.Lcfi270:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movl	%ebx, %eax
	addq	$568, %rsp              # imm = 0x238
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB31_54:
.Lcfi271:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp166:
.Lcfi272:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp167:
# BB#55:                                # %.noexc116
.LBB31_56:
.Ltmp183:
.Lcfi273:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp184:
# BB#57:                                # %.noexc123
.LBB31_58:                              # %.loopexit.split-lp
.Ltmp185:
	jmp	.LBB31_75
.LBB31_59:
.Ltmp168:
	jmp	.LBB31_80
.LBB31_60:
.Ltmp242:
	movq	%rax, %rbx
	jmp	.LBB31_90
.LBB31_61:
.Ltmp237:
	movq	%rax, %rbx
	jmp	.LBB31_89
.LBB31_62:
.Ltmp232:
	movq	%rax, %rbx
	jmp	.LBB31_88
.LBB31_63:
.Ltmp226:
	movq	%rax, %rbx
.Ltmp227:
.Lcfi274:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp228:
	jmp	.LBB31_88
.LBB31_64:
.Ltmp229:
.Lcfi275:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB31_65:
.Ltmp215:
	movq	%rax, %rbx
	jmp	.LBB31_86
.LBB31_66:
.Ltmp210:
	movq	%rax, %rbx
	jmp	.LBB31_85
.LBB31_67:
.Ltmp205:
	movq	%rax, %rbx
	jmp	.LBB31_84
.LBB31_68:
.Ltmp200:
	movq	%rax, %rbx
	jmp	.LBB31_83
.LBB31_69:
.Ltmp195:
	jmp	.LBB31_72
.LBB31_70:
.Ltmp160:
	jmp	.LBB31_72
.LBB31_71:
.Ltmp157:
.LBB31_72:
	movq	%rax, %rbx
	jmp	.LBB31_82
.LBB31_73:                              # %.loopexit138
.Ltmp182:
	jmp	.LBB31_75
.LBB31_74:
.Ltmp179:
.LBB31_75:
	movq	%rax, %rbx
	movq	(%r12), %rax
.Ltmp186:
.Lcfi276:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp187:
	jmp	.LBB31_81
.LBB31_76:
.Ltmp190:
	jmp	.LBB31_80
.LBB31_77:
.Ltmp176:
	jmp	.LBB31_80
.LBB31_78:
.Ltmp173:
	jmp	.LBB31_80
.LBB31_79:                              # %_ZN7CBufferIhED2Ev.exit117
.Ltmp165:
.LBB31_80:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	%rax, %rbx
.LBB31_81:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
.Ltmp191:
.Lcfi277:
	.cfi_escape 0x2e, 0x00
	leaq	336(%rsp), %rdi
	callq	_ZN8NArchive3N7z8CDecoderD2Ev
.Ltmp192:
.LBB31_82:
.Ltmp196:
.Lcfi278:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp197:
.LBB31_83:
.Ltmp201:
.Lcfi279:
	.cfi_escape 0x2e, 0x00
	leaq	176(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp202:
.LBB31_84:
.Ltmp206:
.Lcfi280:
	.cfi_escape 0x2e, 0x00
	leaq	208(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp207:
.LBB31_85:
.Ltmp211:
.Lcfi281:
	.cfi_escape 0x2e, 0x00
	leaq	240(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp212:
.LBB31_86:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, 48(%rsp)
.Ltmp216:
.Lcfi282:
	.cfi_escape 0x2e, 0x00
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp217:
# BB#87:
.Ltmp222:
.Lcfi283:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp223:
.LBB31_88:
.Ltmp233:
.Lcfi284:
	.cfi_escape 0x2e, 0x00
	leaq	272(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp234:
.LBB31_89:
.Ltmp238:
.Lcfi285:
	.cfi_escape 0x2e, 0x00
	leaq	304(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp239:
.LBB31_90:
.Ltmp243:
.Lcfi286:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp244:
# BB#91:
.Lcfi287:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB31_92:
.Ltmp218:
	movq	%rax, %rbx
.Ltmp219:
.Lcfi288:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp220:
	jmp	.LBB31_95
.LBB31_93:
.Ltmp221:
.Lcfi289:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB31_94:
.Ltmp245:
	movq	%rax, %rbx
.LBB31_95:                              # %.body
.Lcfi290:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end31:
	.size	_ZN8NArchive3N7z10CInArchive26ReadAndDecodePackedStreamsEyRyR13CObjectVectorI7CBufferIhEEP22ICryptoGetTextPasswordRb, .Lfunc_end31-_ZN8NArchive3N7z10CInArchive26ReadAndDecodePackedStreamsEyRyR13CObjectVectorI7CBufferIhEEP22ICryptoGetTextPasswordRb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\365\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\354\002"              # Call site table length
	.long	.Ltmp155-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin6  #     jumps to .Ltmp157
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp159-.Ltmp158       #   Call between .Ltmp158 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin6  #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin6  #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp164-.Ltmp161       #   Call between .Ltmp161 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin6  #     jumps to .Ltmp165
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp173-.Lfunc_begin6  #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp170-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp171-.Ltmp170       #   Call between .Ltmp170 and .Ltmp171
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin6  #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin6  #     jumps to .Ltmp176
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin6  #     jumps to .Ltmp179
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp189-.Ltmp188       #   Call between .Ltmp188 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin6  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin6  #     jumps to .Ltmp195
	.byte	0                       #   On action: cleanup
	.long	.Ltmp198-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp199-.Ltmp198       #   Call between .Ltmp198 and .Ltmp199
	.long	.Ltmp200-.Lfunc_begin6  #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp204-.Ltmp203       #   Call between .Ltmp203 and .Ltmp204
	.long	.Ltmp205-.Lfunc_begin6  #     jumps to .Ltmp205
	.byte	0                       #   On action: cleanup
	.long	.Ltmp208-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp209-.Ltmp208       #   Call between .Ltmp208 and .Ltmp209
	.long	.Ltmp210-.Lfunc_begin6  #     jumps to .Ltmp210
	.byte	0                       #   On action: cleanup
	.long	.Ltmp213-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp214-.Ltmp213       #   Call between .Ltmp213 and .Ltmp214
	.long	.Ltmp215-.Lfunc_begin6  #     jumps to .Ltmp215
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin6  #     jumps to .Ltmp226
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin6  # >> Call Site 17 <<
	.long	.Ltmp231-.Ltmp230       #   Call between .Ltmp230 and .Ltmp231
	.long	.Ltmp232-.Lfunc_begin6  #     jumps to .Ltmp232
	.byte	0                       #   On action: cleanup
	.long	.Ltmp235-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Ltmp236-.Ltmp235       #   Call between .Ltmp235 and .Ltmp236
	.long	.Ltmp237-.Lfunc_begin6  #     jumps to .Ltmp237
	.byte	0                       #   On action: cleanup
	.long	.Ltmp240-.Lfunc_begin6  # >> Call Site 19 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin6  #     jumps to .Ltmp242
	.byte	0                       #   On action: cleanup
	.long	.Ltmp241-.Lfunc_begin6  # >> Call Site 20 <<
	.long	.Ltmp166-.Ltmp241       #   Call between .Ltmp241 and .Ltmp166
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin6  # >> Call Site 21 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin6  #     jumps to .Ltmp168
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin6  # >> Call Site 22 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin6  #     jumps to .Ltmp185
	.byte	0                       #   On action: cleanup
	.long	.Ltmp227-.Lfunc_begin6  # >> Call Site 23 <<
	.long	.Ltmp228-.Ltmp227       #   Call between .Ltmp227 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin6  #     jumps to .Ltmp229
	.byte	1                       #   On action: 1
	.long	.Ltmp186-.Lfunc_begin6  # >> Call Site 24 <<
	.long	.Ltmp212-.Ltmp186       #   Call between .Ltmp186 and .Ltmp212
	.long	.Ltmp245-.Lfunc_begin6  #     jumps to .Ltmp245
	.byte	1                       #   On action: 1
	.long	.Ltmp216-.Lfunc_begin6  # >> Call Site 25 <<
	.long	.Ltmp217-.Ltmp216       #   Call between .Ltmp216 and .Ltmp217
	.long	.Ltmp218-.Lfunc_begin6  #     jumps to .Ltmp218
	.byte	1                       #   On action: 1
	.long	.Ltmp222-.Lfunc_begin6  # >> Call Site 26 <<
	.long	.Ltmp244-.Ltmp222       #   Call between .Ltmp222 and .Ltmp244
	.long	.Ltmp245-.Lfunc_begin6  #     jumps to .Ltmp245
	.byte	1                       #   On action: 1
	.long	.Ltmp244-.Lfunc_begin6  # >> Call Site 27 <<
	.long	.Ltmp219-.Ltmp244       #   Call between .Ltmp244 and .Ltmp219
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp219-.Lfunc_begin6  # >> Call Site 28 <<
	.long	.Ltmp220-.Ltmp219       #   Call between .Ltmp219 and .Ltmp220
	.long	.Ltmp221-.Lfunc_begin6  #     jumps to .Ltmp221
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive3N7z8CDecoderD2Ev,"axG",@progbits,_ZN8NArchive3N7z8CDecoderD2Ev,comdat
	.weak	_ZN8NArchive3N7z8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z8CDecoderD2Ev,@function
_ZN8NArchive3N7z8CDecoderD2Ev:          # @_ZN8NArchive3N7z8CDecoderD2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi291:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi293:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi294:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi295:
	.cfi_def_cfa_offset 48
.Lcfi296:
	.cfi_offset %rbx, -40
.Lcfi297:
	.cfi_offset %r12, -32
.Lcfi298:
	.cfi_offset %r14, -24
.Lcfi299:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	leaq	200(%r12), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, 200(%r12)
.Ltmp246:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp247:
# BB#1:
.Ltmp252:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp253:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev.exit
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB32_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp257:
	callq	*16(%rax)
.Ltmp258:
.LBB32_4:                               # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	leaq	8(%r12), %r15
	leaq	136(%r12), %rdi
.Ltmp301:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp302:
# BB#5:                                 # %_ZN8NArchive3N7z11CBindInfoExD2Ev.exit
	leaq	104(%r12), %rdi
.Ltmp323:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp324:
# BB#6:
	leaq	72(%r12), %rdi
.Ltmp328:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp329:
# BB#7:
	addq	$40, %r12
.Ltmp333:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp334:
# BB#8:                                 # %_ZN11NCoderMixer9CBindInfoD2Ev.exit
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB32_33:
.Ltmp259:
	movq	%rax, %r14
	jmp	.LBB32_34
.LBB32_25:
.Ltmp335:
	movq	%rax, %r14
	jmp	.LBB32_28
.LBB32_26:
.Ltmp330:
	movq	%rax, %r14
	jmp	.LBB32_27
.LBB32_24:
.Ltmp325:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp326:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp327:
.LBB32_27:
	addq	$40, %r12
.Ltmp331:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp332:
.LBB32_28:
.Ltmp336:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp337:
	jmp	.LBB32_15
.LBB32_29:
.Ltmp338:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB32_11:
.Ltmp303:
	movq	%rax, %r14
	leaq	104(%r12), %rdi
.Ltmp304:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp305:
# BB#12:
	leaq	72(%r12), %rdi
.Ltmp309:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp310:
# BB#13:
	addq	$40, %r12
.Ltmp314:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp315:
# BB#14:
.Ltmp320:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp321:
	jmp	.LBB32_15
.LBB32_22:
.Ltmp322:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB32_17:
.Ltmp316:
	movq	%rax, %r14
	jmp	.LBB32_20
.LBB32_18:
.Ltmp311:
	movq	%rax, %r14
	jmp	.LBB32_19
.LBB32_16:
.Ltmp306:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp307:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp308:
.LBB32_19:
	addq	$40, %r12
.Ltmp312:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp313:
.LBB32_20:
.Ltmp317:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp318:
# BB#23:                                # %.body14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB32_21:
.Ltmp319:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB32_30:
.Ltmp254:
	movq	%rax, %r14
	jmp	.LBB32_31
.LBB32_9:
.Ltmp248:
	movq	%rax, %r14
.Ltmp249:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp250:
.LBB32_31:                              # %.body
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB32_34
# BB#32:
	movq	(%rdi), %rax
.Ltmp255:
	callq	*16(%rax)
.Ltmp256:
.LBB32_34:                              # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit4
	leaq	8(%r12), %r15
	leaq	136(%r12), %rdi
.Ltmp260:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp261:
# BB#35:
	leaq	104(%r12), %rdi
.Ltmp282:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp283:
# BB#36:
	leaq	72(%r12), %rdi
.Ltmp287:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp288:
# BB#37:
	addq	$40, %r12
.Ltmp292:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp293:
# BB#38:
.Ltmp298:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp299:
.LBB32_15:                              # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB32_10:
.Ltmp251:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB32_40:
.Ltmp294:
	movq	%rax, %r14
	jmp	.LBB32_43
.LBB32_41:
.Ltmp289:
	movq	%rax, %r14
	jmp	.LBB32_42
.LBB32_39:
.Ltmp284:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp285:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp286:
.LBB32_42:
	addq	$40, %r12
.Ltmp290:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp291:
.LBB32_43:
.Ltmp295:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp296:
	jmp	.LBB32_58
.LBB32_44:
.Ltmp297:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB32_45:
.Ltmp262:
	movq	%rax, %r14
	leaq	104(%r12), %rdi
.Ltmp263:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp264:
# BB#46:
	leaq	72(%r12), %rdi
.Ltmp268:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp269:
# BB#47:
	addq	$40, %r12
.Ltmp273:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp274:
# BB#48:
.Ltmp279:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp280:
	jmp	.LBB32_58
.LBB32_55:
.Ltmp281:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB32_50:
.Ltmp275:
	movq	%rax, %r14
	jmp	.LBB32_53
.LBB32_51:
.Ltmp270:
	movq	%rax, %r14
	jmp	.LBB32_52
.LBB32_49:
.Ltmp265:
	movq	%rax, %r14
	leaq	72(%r12), %rdi
.Ltmp266:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp267:
.LBB32_52:
	addq	$40, %r12
.Ltmp271:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp272:
.LBB32_53:
.Ltmp276:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp277:
# BB#56:                                # %.body30
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB32_54:
.Ltmp278:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB32_57:
.Ltmp300:
	movq	%rax, %r14
.LBB32_58:                              # %.body6
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end32:
	.size	_ZN8NArchive3N7z8CDecoderD2Ev, .Lfunc_end32-_ZN8NArchive3N7z8CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\365\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\354\002"              # Call site table length
	.long	.Ltmp246-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp247-.Ltmp246       #   Call between .Ltmp246 and .Ltmp247
	.long	.Ltmp248-.Lfunc_begin7  #     jumps to .Ltmp248
	.byte	0                       #   On action: cleanup
	.long	.Ltmp252-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp253-.Ltmp252       #   Call between .Ltmp252 and .Ltmp253
	.long	.Ltmp254-.Lfunc_begin7  #     jumps to .Ltmp254
	.byte	0                       #   On action: cleanup
	.long	.Ltmp257-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin7  #     jumps to .Ltmp259
	.byte	0                       #   On action: cleanup
	.long	.Ltmp301-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp303-.Lfunc_begin7  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp323-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin7  #     jumps to .Ltmp325
	.byte	0                       #   On action: cleanup
	.long	.Ltmp328-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp329-.Ltmp328       #   Call between .Ltmp328 and .Ltmp329
	.long	.Ltmp330-.Lfunc_begin7  #     jumps to .Ltmp330
	.byte	0                       #   On action: cleanup
	.long	.Ltmp333-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp334-.Ltmp333       #   Call between .Ltmp333 and .Ltmp334
	.long	.Ltmp335-.Lfunc_begin7  #     jumps to .Ltmp335
	.byte	0                       #   On action: cleanup
	.long	.Ltmp334-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp326-.Ltmp334       #   Call between .Ltmp334 and .Ltmp326
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp326-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp337-.Ltmp326       #   Call between .Ltmp326 and .Ltmp337
	.long	.Ltmp338-.Lfunc_begin7  #     jumps to .Ltmp338
	.byte	1                       #   On action: 1
	.long	.Ltmp304-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Ltmp305-.Ltmp304       #   Call between .Ltmp304 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin7  #     jumps to .Ltmp306
	.byte	1                       #   On action: 1
	.long	.Ltmp309-.Lfunc_begin7  # >> Call Site 11 <<
	.long	.Ltmp310-.Ltmp309       #   Call between .Ltmp309 and .Ltmp310
	.long	.Ltmp311-.Lfunc_begin7  #     jumps to .Ltmp311
	.byte	1                       #   On action: 1
	.long	.Ltmp314-.Lfunc_begin7  # >> Call Site 12 <<
	.long	.Ltmp315-.Ltmp314       #   Call between .Ltmp314 and .Ltmp315
	.long	.Ltmp316-.Lfunc_begin7  #     jumps to .Ltmp316
	.byte	1                       #   On action: 1
	.long	.Ltmp320-.Lfunc_begin7  # >> Call Site 13 <<
	.long	.Ltmp321-.Ltmp320       #   Call between .Ltmp320 and .Ltmp321
	.long	.Ltmp322-.Lfunc_begin7  #     jumps to .Ltmp322
	.byte	1                       #   On action: 1
	.long	.Ltmp307-.Lfunc_begin7  # >> Call Site 14 <<
	.long	.Ltmp318-.Ltmp307       #   Call between .Ltmp307 and .Ltmp318
	.long	.Ltmp319-.Lfunc_begin7  #     jumps to .Ltmp319
	.byte	1                       #   On action: 1
	.long	.Ltmp249-.Lfunc_begin7  # >> Call Site 15 <<
	.long	.Ltmp250-.Ltmp249       #   Call between .Ltmp249 and .Ltmp250
	.long	.Ltmp251-.Lfunc_begin7  #     jumps to .Ltmp251
	.byte	1                       #   On action: 1
	.long	.Ltmp255-.Lfunc_begin7  # >> Call Site 16 <<
	.long	.Ltmp256-.Ltmp255       #   Call between .Ltmp255 and .Ltmp256
	.long	.Ltmp300-.Lfunc_begin7  #     jumps to .Ltmp300
	.byte	1                       #   On action: 1
	.long	.Ltmp260-.Lfunc_begin7  # >> Call Site 17 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin7  #     jumps to .Ltmp262
	.byte	1                       #   On action: 1
	.long	.Ltmp282-.Lfunc_begin7  # >> Call Site 18 <<
	.long	.Ltmp283-.Ltmp282       #   Call between .Ltmp282 and .Ltmp283
	.long	.Ltmp284-.Lfunc_begin7  #     jumps to .Ltmp284
	.byte	1                       #   On action: 1
	.long	.Ltmp287-.Lfunc_begin7  # >> Call Site 19 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp289-.Lfunc_begin7  #     jumps to .Ltmp289
	.byte	1                       #   On action: 1
	.long	.Ltmp292-.Lfunc_begin7  # >> Call Site 20 <<
	.long	.Ltmp293-.Ltmp292       #   Call between .Ltmp292 and .Ltmp293
	.long	.Ltmp294-.Lfunc_begin7  #     jumps to .Ltmp294
	.byte	1                       #   On action: 1
	.long	.Ltmp298-.Lfunc_begin7  # >> Call Site 21 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin7  #     jumps to .Ltmp300
	.byte	1                       #   On action: 1
	.long	.Ltmp299-.Lfunc_begin7  # >> Call Site 22 <<
	.long	.Ltmp285-.Ltmp299       #   Call between .Ltmp299 and .Ltmp285
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp285-.Lfunc_begin7  # >> Call Site 23 <<
	.long	.Ltmp296-.Ltmp285       #   Call between .Ltmp285 and .Ltmp296
	.long	.Ltmp297-.Lfunc_begin7  #     jumps to .Ltmp297
	.byte	1                       #   On action: 1
	.long	.Ltmp263-.Lfunc_begin7  # >> Call Site 24 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin7  #     jumps to .Ltmp265
	.byte	1                       #   On action: 1
	.long	.Ltmp268-.Lfunc_begin7  # >> Call Site 25 <<
	.long	.Ltmp269-.Ltmp268       #   Call between .Ltmp268 and .Ltmp269
	.long	.Ltmp270-.Lfunc_begin7  #     jumps to .Ltmp270
	.byte	1                       #   On action: 1
	.long	.Ltmp273-.Lfunc_begin7  # >> Call Site 26 <<
	.long	.Ltmp274-.Ltmp273       #   Call between .Ltmp273 and .Ltmp274
	.long	.Ltmp275-.Lfunc_begin7  #     jumps to .Ltmp275
	.byte	1                       #   On action: 1
	.long	.Ltmp279-.Lfunc_begin7  # >> Call Site 27 <<
	.long	.Ltmp280-.Ltmp279       #   Call between .Ltmp279 and .Ltmp280
	.long	.Ltmp281-.Lfunc_begin7  #     jumps to .Ltmp281
	.byte	1                       #   On action: 1
	.long	.Ltmp266-.Lfunc_begin7  # >> Call Site 28 <<
	.long	.Ltmp277-.Ltmp266       #   Call between .Ltmp266 and .Ltmp277
	.long	.Ltmp278-.Lfunc_begin7  #     jumps to .Ltmp278
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi300:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi301:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi302:
	.cfi_def_cfa_offset 32
.Lcfi303:
	.cfi_offset %rbx, -24
.Lcfi304:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, (%rbx)
.Ltmp339:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp340:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB33_2:
.Ltmp341:
	movq	%rax, %r14
.Ltmp342:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp343:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB33_4:
.Ltmp344:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end33:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev, .Lfunc_end33-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp339-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp340-.Ltmp339       #   Call between .Ltmp339 and .Ltmp340
	.long	.Ltmp341-.Lfunc_begin8  #     jumps to .Ltmp341
	.byte	0                       #   On action: cleanup
	.long	.Ltmp340-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp342-.Ltmp340       #   Call between .Ltmp340 and .Ltmp342
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp342-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp343-.Ltmp342       #   Call between .Ltmp342 and .Ltmp343
	.long	.Ltmp344-.Lfunc_begin8  #     jumps to .Ltmp344
	.byte	1                       #   On action: 1
	.long	.Ltmp343-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Lfunc_end33-.Ltmp343   #   Call between .Ltmp343 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI34_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z10CInArchive10ReadHeaderERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive10ReadHeaderERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb,@function
_ZN8NArchive3N7z10CInArchive10ReadHeaderERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb: # @_ZN8NArchive3N7z10CInArchive10ReadHeaderERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi305:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi306:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi307:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi308:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi309:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi310:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi311:
	.cfi_def_cfa_offset 432
.Lcfi312:
	.cfi_offset %rbx, -56
.Lcfi313:
	.cfi_offset %r12, -48
.Lcfi314:
	.cfi_offset %r13, -40
.Lcfi315:
	.cfi_offset %r14, -32
.Lcfi316:
	.cfi_offset %r15, -24
.Lcfi317:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	40(%rdi), %rdi
.Lcfi318:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %r15
	cmpq	$2, %r15
	jne	.LBB34_6
# BB#1:
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	40(%rbp), %rdi
.Lcfi319:
	.cfi_escape 0x2e, 0x00
	jmp	.LBB34_2
	.p2align	4, 0x90
.LBB34_4:                               # %_ZN8NArchive3N7z10CInArchive8SkipDataEv.exit.i
                                        #   in Loop: Header=BB34_2 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 16(%rbx)
	movq	40(%rbp), %rdi
.Lcfi320:
	.cfi_escape 0x2e, 0x00
.LBB34_2:                               # =>This Inner Loop Header: Depth=1
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	testq	%rax, %rax
	je	.LBB34_5
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	40(%rbp), %rbx
.Lcfi321:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rcx
	subq	%rcx, %rdx
	cmpq	%rax, %rdx
	jae	.LBB34_4
# BB#286:
.Lcfi322:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB34_5:                               # %_ZN8NArchive3N7z10CInArchive21ReadArchivePropertiesERNS0_14CInArchiveInfoE.exit
	movq	40(%rbp), %rdi
.Lcfi323:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %r15
.LBB34_6:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 152(%rsp)
	movq	$8, 168(%rsp)
	movq	$_ZTV13CObjectVectorI7CBufferIhEE+16, 144(%rsp)
	cmpq	$3, %r15
	jne	.LBB34_11
# BB#7:
	movq	496(%r13), %rsi
	leaq	512(%r13), %rdx
.Ltmp345:
.Lcfi324:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r12, %r8
	movq	%r14, %r9
	callq	_ZN8NArchive3N7z10CInArchive26ReadAndDecodePackedStreamsEyRyR13CObjectVectorI7CBufferIhEEP22ICryptoGetTextPasswordRb
	movl	%eax, %ebx
.Ltmp346:
# BB#8:
	testl	%ebx, %ebx
	jne	.LBB34_257
# BB#9:
	movq	496(%r13), %rax
	addq	%rax, 512(%r13)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	40(%rax), %rdi
.Ltmp348:
.Lcfi325:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %r15
.Ltmp349:
# BB#10:
	pxor	%xmm0, %xmm0
.LBB34_11:                              # %_ZN8NArchive3N7z10CInArchive6ReadIDEv.exit.thread
	movdqu	%xmm0, 288(%rsp)
	movq	$8, 304(%rsp)
	movq	$_ZTV13CRecordVectorIyE+16, 280(%rsp)
	movdqu	%xmm0, 256(%rsp)
	movq	$1, 272(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 248(%rsp)
	movdqu	%xmm0, 224(%rsp)
	movq	$4, 240(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 216(%rsp)
	cmpq	$4, %r15
	movq	%r13, 40(%rsp)          # 8-byte Spill
	jne	.LBB34_12
# BB#18:
	leaq	504(%r13), %rdx
	leaq	32(%r13), %r8
	leaq	64(%r13), %r9
	leaq	96(%r13), %rax
	movq	%r13, %rbp
	subq	$-128, %rbp
.Ltmp364:
.Lcfi326:
	.cfi_escape 0x2e, 0x30
	subq	$8, %rsp
.Lcfi327:
	.cfi_adjust_cfa_offset 8
	leaq	224(%rsp), %rbx
	leaq	256(%rsp), %r10
	leaq	288(%rsp), %r11
	leaq	152(%rsp), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rcx
	pushq	%rbx
.Lcfi328:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi329:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi330:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	movq	%rdi, %rbx
.Lcfi331:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi332:
	.cfi_adjust_cfa_offset 8
	callq	_ZN8NArchive3N7z10CInArchive15ReadStreamsInfoEPK13CObjectVectorI7CBufferIhEERyR13CRecordVectorIyERS9_IbERS9_IjERS2_INS0_7CFolderEESF_SB_SD_SF_
	addq	$48, %rsp
.Lcfi333:
	.cfi_adjust_cfa_offset -48
.Ltmp365:
# BB#19:
	movq	496(%r13), %rax
	addq	%rax, 504(%r13)
	movq	40(%rbx), %rdi
.Ltmp366:
.Lcfi334:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %r15
.Ltmp367:
	jmp	.LBB34_14
.LBB34_12:                              # %.preheader385
	cmpl	$0, 108(%r13)
	jle	.LBB34_13
# BB#21:                                # %.lr.ph461
	movq	%r13, %rbp
	subq	$-128, %rbp
	xorl	%r13d, %r13d
	leaq	280(%rsp), %r12
	.p2align	4, 0x90
.LBB34_22:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_25 Depth 2
                                        #       Child Loop BB34_28 Depth 3
.Ltmp351:
.Lcfi335:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp352:
# BB#23:                                #   in Loop: Header=BB34_22 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	144(%rdx), %rax
	movslq	140(%rdx), %rcx
	movl	$1, (%rax,%rcx,4)
	incl	140(%rdx)
	movq	112(%rdx), %rax
	movq	(%rax,%r13,8), %r14
	movl	108(%r14), %eax
	testl	%eax, %eax
	je	.LBB34_24
.LBB34_25:                              #   Parent Loop BB34_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB34_28 Depth 3
	testl	%eax, %eax
	jle	.LBB34_37
# BB#26:                                #   in Loop: Header=BB34_25 Depth=2
	decl	%eax
	movslq	44(%r14), %rcx
	testq	%rcx, %rcx
	jle	.LBB34_31
# BB#27:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB34_25 Depth=2
	movq	48(%r14), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB34_28:                              #   Parent Loop BB34_22 Depth=1
                                        #     Parent Loop BB34_25 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%eax, 4(%rsi,%rdx,8)
	je	.LBB34_30
# BB#29:                                #   in Loop: Header=BB34_28 Depth=3
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB34_28
	jmp	.LBB34_31
	.p2align	4, 0x90
.LBB34_30:                              # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.i
                                        #   in Loop: Header=BB34_25 Depth=2
	testl	%edx, %edx
	jns	.LBB34_25
	.p2align	4, 0x90
.LBB34_31:                              # %_ZNK8NArchive3N7z7CFolder24FindBindPairForOutStreamEj.exit.thread.i
                                        #   in Loop: Header=BB34_22 Depth=1
	movq	112(%r14), %rcx
	cltq
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB34_32
	.p2align	4, 0x90
.LBB34_24:                              #   in Loop: Header=BB34_22 Depth=1
	xorl	%ebx, %ebx
.LBB34_32:                              # %_ZNK8NArchive3N7z7CFolder13GetUnpackSizeEv.exit
                                        #   in Loop: Header=BB34_22 Depth=1
.Ltmp357:
.Lcfi336:
	.cfi_escape 0x2e, 0x00
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp358:
# BB#33:                                #   in Loop: Header=BB34_22 Depth=1
	movq	296(%rsp), %rax
	movslq	292(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 292(%rsp)
	movb	132(%r14), %bl
.Ltmp359:
.Lcfi337:
	.cfi_escape 0x2e, 0x00
	leaq	248(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp360:
# BB#34:                                #   in Loop: Header=BB34_22 Depth=1
	movq	264(%rsp), %rax
	movslq	260(%rsp), %rcx
	movb	%bl, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 260(%rsp)
	movl	128(%r14), %ebx
.Ltmp361:
.Lcfi338:
	.cfi_escape 0x2e, 0x00
	leaq	216(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp362:
# BB#35:                                #   in Loop: Header=BB34_22 Depth=1
	movq	232(%rsp), %rax
	movslq	228(%rsp), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	228(%rsp)
	incq	%r13
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	108(%rax), %rax
	cmpq	%rax, %r13
	jl	.LBB34_22
# BB#36:
	movq	40(%rsp), %r13          # 8-byte Reload
.LBB34_13:
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB34_14:                              # %_ZN8NArchive3N7z10CInArchive6ReadIDEv.exit238
	leaq	160(%r13), %r14
.Ltmp368:
.Lcfi339:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp369:
# BB#15:
	testq	%r15, %r15
	je	.LBB34_253
# BB#16:
	cmpq	$5, %r15
	jne	.LBB34_17
# BB#42:
	movq	40(%rbx), %rdi
.Ltmp370:
.Lcfi340:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %r15
.Ltmp371:
# BB#43:                                # %.noexc244
	testq	$-2147483648, %r15      # imm = 0x80000000
	jne	.LBB34_44
# BB#46:
.Ltmp372:
.Lcfi341:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp373:
# BB#47:                                # %.preheader383
	testl	%r15d, %r15d
	je	.LBB34_53
# BB#48:                                # %.lr.ph458
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB34_49:                              # =>This Inner Loop Header: Depth=1
.Ltmp375:
.Lcfi342:
	.cfi_escape 0x2e, 0x00
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp376:
# BB#50:                                # %._crit_edge16.i.i.i.i
                                        #   in Loop: Header=BB34_49 Depth=1
	movdqu	48(%rsp), %xmm0
	movdqu	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rbx)
.Ltmp377:
.Lcfi343:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	_Znam
.Ltmp378:
# BB#51:                                #   in Loop: Header=BB34_49 Depth=1
	movq	%rax, 16(%rbx)
	movl	$1, 28(%rbx)
	movl	$0, (%rax)
	movl	$0, 24(%rbx)
	movl	$1, 32(%rbx)
.Ltmp380:
.Lcfi344:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp381:
# BB#52:                                #   in Loop: Header=BB34_49 Depth=1
	movq	176(%r13), %rax
	movslq	172(%r13), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 172(%r13)
	incl	%ebp
	cmpl	%r15d, %ebp
	jb	.LBB34_49
.LBB34_53:                              # %._crit_edge459
	leaq	520(%r13), %rdi
.Ltmp383:
.Lcfi345:
	.cfi_escape 0x2e, 0x00
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp384:
# BB#54:
	movq	536(%r13), %rax
	movslq	532(%r13), %rcx
	movq	$9, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 532(%r13)
	cmpl	$0, 12(%r13)
	je	.LBB34_57
# BB#55:
.Ltmp385:
.Lcfi346:
	.cfi_escape 0x2e, 0x00
	movq	176(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp386:
# BB#56:                                # %_ZN13CRecordVectorIyE3AddEy.exit255
	movq	536(%r13), %rax
	movslq	532(%r13), %rcx
	movq	$6, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 532(%r13)
.LBB34_57:
	testl	%r15d, %r15d
	je	.LBB34_61
# BB#58:
	cmpl	$0, 228(%rsp)
	je	.LBB34_61
# BB#59:
.Ltmp387:
.Lcfi347:
	.cfi_escape 0x2e, 0x00
	movq	176(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp388:
# BB#60:                                # %_ZN13CRecordVectorIyE3AddEy.exit257
	movq	536(%r13), %rax
	movslq	532(%r13), %rcx
	movq	$10, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 532(%r13)
.LBB34_61:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 56(%rsp)
	movq	$1, 72(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 48(%rsp)
.Ltmp390:
.Lcfi348:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp391:
# BB#62:                                # %.noexc260
.Ltmp392:
.Lcfi349:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	movl	%r15d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp393:
# BB#63:                                # %.noexc261
	testl	%r15d, %r15d
	jle	.LBB34_67
# BB#64:                                # %.lr.ph.i258.preheader
	leaq	48(%rsp), %rbx
	movl	%r15d, %ebp
	.p2align	4, 0x90
.LBB34_65:                              # %.lr.ph.i258
                                        # =>This Inner Loop Header: Depth=1
.Ltmp395:
.Lcfi350:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp396:
# BB#66:                                # %.noexc262
                                        #   in Loop: Header=BB34_65 Depth=1
	movq	64(%rsp), %rax
	movslq	60(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 60(%rsp)
	decl	%ebp
	jne	.LBB34_65
.LBB34_67:                              # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 120(%rsp)
	movq	$1, 136(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 112(%rsp)
	movdqu	%xmm0, 88(%rsp)
	movq	$1, 104(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 80(%rsp)
	leaq	384(%r13), %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	leaq	192(%r13), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	leaq	256(%r13), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	leaq	320(%r13), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB34_68
	.p2align	4, 0x90
.LBB34_98:                              #   in Loop: Header=BB34_68 Depth=1
	movq	40(%r14), %rdi
.Ltmp401:
.Lcfi351:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
.Ltmp402:
# BB#99:                                # %_ZN8NArchive3N7z10CInArchive10ReadNumberEv.exit
                                        #   in Loop: Header=BB34_68 Depth=1
	movq	40(%r14), %rbx
	movq	16(%rbx), %rbp
	cmpq	$1073741824, %r12       # imm = 0x40000000
	pxor	%xmm0, %xmm0
	jbe	.LBB34_100
.LBB34_231:                             # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit297
                                        #   in Loop: Header=BB34_68 Depth=1
	movq	8(%rbx), %rcx
	subq	%rbp, %rcx
	cmpq	%rax, %rcx
	jb	.LBB34_232
# BB#234:                               # %_ZN8NArchive3N7z10CInArchive8SkipDataEy.exit
                                        #   in Loop: Header=BB34_68 Depth=1
	leaq	(%rbp,%rax), %rcx
	movq	%rcx, 16(%rbx)
	cmpb	$0, 480(%r13)
	je	.LBB34_236
	jmp	.LBB34_237
	.p2align	4, 0x90
.LBB34_100:                             #   in Loop: Header=BB34_68 Depth=1
	leal	-14(%r12), %ecx
	cmpl	$11, %ecx
	ja	.LBB34_231
# BB#101:                               #   in Loop: Header=BB34_68 Depth=1
	jmpq	*.LJTI34_0(,%rcx,8)
.LBB34_157:                             #   in Loop: Header=BB34_68 Depth=1
.Ltmp435:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Lcfi352:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp436:
# BB#158:                               # %.noexc280
                                        #   in Loop: Header=BB34_68 Depth=1
.Ltmp437:
.Lcfi353:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	movl	%r15d, %esi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp438:
# BB#159:                               # %.noexc281
                                        #   in Loop: Header=BB34_68 Depth=1
	testl	%r15d, %r15d
	jle	.LBB34_290
# BB#160:                               # %.lr.ph.i278.preheader
                                        #   in Loop: Header=BB34_68 Depth=1
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB34_161:                             # %.lr.ph.i278
                                        #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%r13b, %r13b
	jne	.LBB34_166
# BB#162:                               #   in Loop: Header=BB34_161 Depth=2
	movq	40(%r14), %rax
	movq	16(%rax), %rcx
	cmpq	8(%rax), %rcx
	jae	.LBB34_163
# BB#165:                               # %_ZN8NArchive3N7z10CInArchive8ReadByteEv.exit.i
                                        #   in Loop: Header=BB34_161 Depth=2
	movq	(%rax), %rdx
	leaq	1(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movb	(%rdx,%rcx), %bl
	movb	$-128, %r13b
.LBB34_166:                             #   in Loop: Header=BB34_161 Depth=2
.Ltmp441:
.Lcfi354:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp442:
# BB#167:                               # %.noexc283
                                        #   in Loop: Header=BB34_161 Depth=2
	testb	%bl, %r13b
	movq	64(%rsp), %rax
	movslq	60(%rsp), %rcx
	setne	(%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 60(%rsp)
	shrb	%r13b
	incl	%r15d
	cmpl	32(%rsp), %r15d         # 4-byte Folded Reload
	jl	.LBB34_161
	jmp	.LBB34_168
.LBB34_205:                             #   in Loop: Header=BB34_68 Depth=1
.Ltmp417:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Lcfi355:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp418:
# BB#206:                               # %.noexc317
                                        #   in Loop: Header=BB34_68 Depth=1
.Ltmp419:
.Lcfi356:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp420:
# BB#207:                               # %.noexc318
                                        #   in Loop: Header=BB34_68 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB34_229
# BB#208:                               # %.lr.ph.i310.preheader
                                        #   in Loop: Header=BB34_68 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB34_209:                             # %.lr.ph.i310
                                        #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%bpl, %bpl
	jne	.LBB34_214
# BB#210:                               #   in Loop: Header=BB34_209 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	40(%rax), %rax
	movq	16(%rax), %rcx
	cmpq	8(%rax), %rcx
	jae	.LBB34_211
# BB#213:                               # %_ZN8NArchive3N7z10CInArchive8ReadByteEv.exit.i314
                                        #   in Loop: Header=BB34_209 Depth=2
	movq	(%rax), %rdx
	leaq	1(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movb	(%rdx,%rcx), %r15b
	movb	$-128, %bpl
.LBB34_214:                             #   in Loop: Header=BB34_209 Depth=2
.Ltmp423:
.Lcfi357:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp424:
# BB#215:                               # %.noexc320
                                        #   in Loop: Header=BB34_209 Depth=2
	testb	%r15b, %bpl
	movq	96(%rsp), %rax
	movslq	92(%rsp), %rcx
	setne	(%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 92(%rsp)
	shrb	%bpl
	incl	%ebx
	cmpl	%r14d, %ebx
	jl	.LBB34_209
	jmp	.LBB34_216
.LBB34_218:                             #   in Loop: Header=BB34_68 Depth=1
.Ltmp411:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Lcfi358:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	leaq	144(%rsp), %rsi
	movq	320(%rsp), %rdx         # 8-byte Reload
	movl	%r15d, %ecx
	callq	_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi
.Ltmp412:
	jmp	.LBB34_229
.LBB34_194:                             #   in Loop: Header=BB34_68 Depth=1
.Ltmp426:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Lcfi359:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp427:
# BB#195:                               # %.noexc305
                                        #   in Loop: Header=BB34_68 Depth=1
.Ltmp428:
.Lcfi360:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp429:
# BB#196:                               # %.noexc306
                                        #   in Loop: Header=BB34_68 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB34_229
# BB#197:                               # %.lr.ph.i298.preheader
                                        #   in Loop: Header=BB34_68 Depth=1
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB34_198:                             # %.lr.ph.i298
                                        #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	%bpl, %bpl
	jne	.LBB34_203
# BB#199:                               #   in Loop: Header=BB34_198 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	40(%rax), %rax
	movq	16(%rax), %rcx
	cmpq	8(%rax), %rcx
	jae	.LBB34_200
# BB#202:                               # %_ZN8NArchive3N7z10CInArchive8ReadByteEv.exit.i302
                                        #   in Loop: Header=BB34_198 Depth=2
	movq	(%rax), %rdx
	leaq	1(%rcx), %rsi
	movq	%rsi, 16(%rax)
	movb	(%rdx,%rcx), %r15b
	movb	$-128, %bpl
.LBB34_203:                             #   in Loop: Header=BB34_198 Depth=2
.Ltmp432:
.Lcfi361:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp433:
# BB#204:                               # %.noexc308
                                        #   in Loop: Header=BB34_198 Depth=2
	testb	%r15b, %bpl
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	setne	(%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 124(%rsp)
	shrb	%bpl
	incl	%ebx
	cmpl	%r14d, %ebx
	jl	.LBB34_198
.LBB34_216:                             #   in Loop: Header=BB34_68 Depth=1
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB34_229
.LBB34_219:                             #   in Loop: Header=BB34_68 Depth=1
.Ltmp409:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Lcfi362:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	leaq	144(%rsp), %rsi
	movq	312(%rsp), %rdx         # 8-byte Reload
	movl	%r15d, %ecx
	callq	_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi
.Ltmp410:
	jmp	.LBB34_229
.LBB34_129:                             #   in Loop: Header=BB34_68 Depth=1
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	352(%rsp), %rax
	movdqu	%xmm0, (%rax)
	movq	$1, 368(%rsp)
	movq	$_ZTV13CRecordVectorIbE+16, 344(%rsp)
	movl	172(%r13), %esi
.Ltmp458:
.Lcfi363:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	leaq	344(%rsp), %rdx
	callq	_ZN8NArchive3N7z10CInArchive15ReadBoolVector2EiR13CRecordVectorIbE
.Ltmp459:
# BB#130:                               #   in Loop: Header=BB34_68 Depth=1
	movb	$0, 192(%rsp)
.Ltmp461:
.Lcfi364:
	.cfi_escape 0x2e, 0x00
	leaq	184(%rsp), %rdi
	movq	%r14, %rsi
	leaq	144(%rsp), %rdx
	callq	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE
.Ltmp462:
# BB#131:                               # %.preheader360
                                        #   in Loop: Header=BB34_68 Depth=1
	testl	%r15d, %r15d
	je	.LBB34_140
# BB#132:                               # %.lr.ph452
                                        #   in Loop: Header=BB34_68 Depth=1
	movq	176(%r13), %r8
	xorl	%esi, %esi
	movq	360(%rsp), %rdi
	.p2align	4, 0x90
.LBB34_133:                             #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rdx
	movq	(%r8,%rdx,8), %rax
	movzbl	(%rdi,%rdx), %edx
	testb	%dl, %dl
	movb	%dl, 35(%rax)
	je	.LBB34_139
# BB#134:                               #   in Loop: Header=BB34_133 Depth=2
	movq	40(%r14), %rdx
	movq	16(%rdx), %rbp
	leaq	4(%rbp), %rbx
	cmpq	8(%rdx), %rbx
	ja	.LBB34_135
# BB#138:                               #   in Loop: Header=BB34_133 Depth=2
	movq	(%rdx), %rcx
	movl	(%rcx,%rbp), %ecx
	movq	%rbx, 16(%rdx)
	movl	%ecx, 8(%rax)
	movq	8(%rsp), %r14           # 8-byte Reload
.LBB34_139:                             #   in Loop: Header=BB34_133 Depth=2
	incl	%esi
	cmpl	%r15d, %esi
	jb	.LBB34_133
.LBB34_140:                             # %._crit_edge453
                                        #   in Loop: Header=BB34_68 Depth=1
	cmpb	$0, 192(%rsp)
	je	.LBB34_145
# BB#141:                               #   in Loop: Header=BB34_68 Depth=1
	movq	184(%rsp), %rbx
	leaq	8(%rbx), %rdi
.Ltmp469:
.Lcfi365:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp470:
# BB#142:                               # %.noexc273
                                        #   in Loop: Header=BB34_68 Depth=1
	movslq	20(%rbx), %rax
	testq	%rax, %rax
	je	.LBB34_144
# BB#143:                               #   in Loop: Header=BB34_68 Depth=1
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbx)
.LBB34_144:                             # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i272
                                        #   in Loop: Header=BB34_68 Depth=1
	movb	$0, 192(%rsp)
.LBB34_145:                             # %_ZN8NArchive3N7z13CStreamSwitchD2Ev.exit274
                                        #   in Loop: Header=BB34_68 Depth=1
.Ltmp474:
.Lcfi366:
	.cfi_escape 0x2e, 0x00
	leaq	344(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp475:
	jmp	.LBB34_229
.LBB34_104:                             #   in Loop: Header=BB34_68 Depth=1
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movb	$0, 208(%rsp)
.Ltmp477:
.Lcfi367:
	.cfi_escape 0x2e, 0x00
	leaq	200(%rsp), %rdi
	movq	%r14, %rsi
	leaq	144(%rsp), %rdx
	callq	_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPK13CObjectVectorI7CBufferIhEE
.Ltmp478:
# BB#105:                               # %.preheader359
                                        #   in Loop: Header=BB34_68 Depth=1
	cmpl	$0, 172(%r13)
	jle	.LBB34_109
# BB#106:                               # %.lr.ph455.preheader
                                        #   in Loop: Header=BB34_68 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB34_107:                             # %.lr.ph455
                                        #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%r14), %rdi
	movq	176(%r13), %rax
	movq	(%rax,%rbx,8), %rsi
	addq	$16, %rsi
.Ltmp480:
.Lcfi368:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7z8CInByte210ReadStringER11CStringBaseIwE
.Ltmp481:
# BB#108:                               #   in Loop: Header=BB34_107 Depth=2
	incq	%rbx
	movslq	172(%r13), %rax
	cmpq	%rax, %rbx
	jl	.LBB34_107
.LBB34_109:                             # %._crit_edge456
                                        #   in Loop: Header=BB34_68 Depth=1
	cmpb	$0, 208(%rsp)
	je	.LBB34_229
# BB#110:                               #   in Loop: Header=BB34_68 Depth=1
	movq	200(%rsp), %rbx
	leaq	8(%rbx), %rdi
.Ltmp485:
.Lcfi369:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp486:
# BB#111:                               # %.noexc266
                                        #   in Loop: Header=BB34_68 Depth=1
	movslq	20(%rbx), %rax
	testq	%rax, %rax
	je	.LBB34_113
# BB#112:                               #   in Loop: Header=BB34_68 Depth=1
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbx)
.LBB34_113:                             # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i
                                        #   in Loop: Header=BB34_68 Depth=1
	movb	$0, 208(%rsp)
	jmp	.LBB34_229
.LBB34_217:                             #   in Loop: Header=BB34_68 Depth=1
.Ltmp413:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Lcfi370:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	leaq	144(%rsp), %rsi
	movq	328(%rsp), %rdx         # 8-byte Reload
	movl	%r15d, %ecx
	callq	_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi
.Ltmp414:
	jmp	.LBB34_229
.LBB34_102:                             # %.preheader374
                                        #   in Loop: Header=BB34_68 Depth=1
	testq	%rax, %rax
	je	.LBB34_235
# BB#103:                               # %.lr.ph446.preheader
                                        #   in Loop: Header=BB34_68 Depth=1
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB34_221:                             # %.lr.ph446
                                        #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rbp,%rdx), %rsi
	cmpq	%rcx, %rsi
	jae	.LBB34_222
# BB#224:                               #   in Loop: Header=BB34_221 Depth=2
	leaq	1(%rbp,%rdx), %rsi
	movq	(%rbx), %rdi
	addq	%rbp, %rdi
	movq	%rsi, 16(%rbx)
	cmpb	$0, (%rdx,%rdi)
	jne	.LBB34_225
# BB#220:                               #   in Loop: Header=BB34_221 Depth=2
	incq	%rdx
	cmpq	%rax, %rdx
	jb	.LBB34_221
	jmp	.LBB34_235
.LBB34_228:                             #   in Loop: Header=BB34_68 Depth=1
.Ltmp415:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
.Lcfi371:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	leaq	144(%rsp), %rsi
	movq	336(%rsp), %rdx         # 8-byte Reload
	movl	%r15d, %ecx
	callq	_ZN8NArchive3N7z10CInArchive19ReadUInt64DefVectorERK13CObjectVectorI7CBufferIhEERNS0_16CUInt64DefVectorEi
.Ltmp416:
	jmp	.LBB34_229
.LBB34_290:                             # %.noexc281._ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE.exit.preheader_crit_edge
                                        #   in Loop: Header=BB34_68 Depth=1
	movl	60(%rsp), %eax
.LBB34_168:                             # %_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE.exit.preheader
                                        #   in Loop: Header=BB34_68 Depth=1
	testl	%eax, %eax
	je	.LBB34_169
# BB#170:                               # %.lr.ph449
                                        #   in Loop: Header=BB34_68 Depth=1
	movq	64(%rsp), %rcx
	cmpl	$8, %eax
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	jae	.LBB34_172
.LBB34_171:                             #   in Loop: Header=BB34_68 Depth=1
	xorl	%edx, %edx
	movq	(%rsp), %rdi            # 8-byte Reload
	.p2align	4, 0x90
.LBB34_181:                             # %_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE.exit
                                        #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %esi
	addl	%esi, %edi
	incl	%edx
	cmpl	%eax, %edx
	jb	.LBB34_181
	jmp	.LBB34_182
.LBB34_169:                             #   in Loop: Header=BB34_68 Depth=1
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB34_182:                             # %_ZN8NArchive3N7z10CInArchive14ReadBoolVectorEiR13CRecordVectorIbE.exit._crit_edge
                                        #   in Loop: Header=BB34_68 Depth=1
.Ltmp444:
	movq	%rdi, (%rsp)            # 8-byte Spill
.Lcfi372:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp445:
# BB#183:                               # %.noexc287
                                        #   in Loop: Header=BB34_68 Depth=1
.Ltmp446:
.Lcfi373:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp447:
# BB#184:                               # %.noexc288
                                        #   in Loop: Header=BB34_68 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jle	.LBB34_188
# BB#185:                               # %.lr.ph.i284.preheader
                                        #   in Loop: Header=BB34_68 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB34_186:                             # %.lr.ph.i284
                                        #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp448:
.Lcfi374:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp449:
# BB#187:                               # %.noexc289
                                        #   in Loop: Header=BB34_186 Depth=2
	movq	128(%rsp), %rax
	movslq	124(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 124(%rsp)
	decl	%ebx
	jne	.LBB34_186
.LBB34_188:                             # %_ZN8NArchive3N7zL21BoolVector_Fill_FalseER13CRecordVectorIbEi.exit290
                                        #   in Loop: Header=BB34_68 Depth=1
.Ltmp451:
.Lcfi375:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp452:
# BB#189:                               # %.noexc294
                                        #   in Loop: Header=BB34_68 Depth=1
.Ltmp453:
.Lcfi376:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp454:
# BB#190:                               # %.noexc295
                                        #   in Loop: Header=BB34_68 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB34_229
# BB#191:                               # %.lr.ph.i291.preheader
                                        #   in Loop: Header=BB34_68 Depth=1
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB34_192:                             # %.lr.ph.i291
                                        #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
.Ltmp455:
.Lcfi377:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp456:
# BB#193:                               # %.noexc296
                                        #   in Loop: Header=BB34_192 Depth=2
	movq	96(%rsp), %rax
	movslq	92(%rsp), %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 92(%rsp)
	decl	%ebx
	jne	.LBB34_192
	.p2align	4, 0x90
.LBB34_229:                             # %.loopexit361
                                        #   in Loop: Header=BB34_68 Depth=1
.Ltmp488:
.Lcfi378:
	.cfi_escape 0x2e, 0x00
	movq	176(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp489:
# BB#230:                               # %_ZN13CRecordVectorIyE3AddEy.exit326
                                        #   in Loop: Header=BB34_68 Depth=1
	movq	536(%r13), %rax
	movslq	532(%r13), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 532(%r13)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB34_235:                             # %.loopexit375
                                        #   in Loop: Header=BB34_68 Depth=1
	cmpb	$0, 480(%r13)
	jne	.LBB34_237
.LBB34_236:                             # %.loopexit375
                                        #   in Loop: Header=BB34_68 Depth=1
	cmpb	$3, 481(%r13)
	jb	.LBB34_68
.LBB34_237:                             #   in Loop: Header=BB34_68 Depth=1
	movq	40(%r14), %rcx
	movq	16(%rcx), %rcx
	subq	%rbp, %rcx
	cmpq	%rax, %rcx
	je	.LBB34_68
	jmp	.LBB34_238
.LBB34_172:                             # %min.iters.checked
                                        #   in Loop: Header=BB34_68 Depth=1
	movl	%eax, %edx
	andl	$-8, %edx
	je	.LBB34_171
# BB#173:                               # %min.iters.checked
                                        #   in Loop: Header=BB34_68 Depth=1
	movl	%eax, %esi
	decl	%esi
	js	.LBB34_171
# BB#174:                               # %vector.body.preheader
                                        #   in Loop: Header=BB34_68 Depth=1
	movd	(%rsp), %xmm2           # 4-byte Folded Reload
                                        # xmm2 = mem[0],zero,zero,zero
	leal	-8(%rdx), %esi
	movl	%esi, %edi
	shrl	$3, %edi
	btl	$3, %esi
	jb	.LBB34_175
# BB#176:                               # %vector.body.prol
                                        #   in Loop: Header=BB34_68 Depth=1
	movd	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movd	4(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	pxor	%xmm4, %xmm4
	punpcklbw	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3],xmm1[4],xmm4[4],xmm1[5],xmm4[5],xmm1[6],xmm4[6],xmm1[7],xmm4[7]
	punpcklwd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3]
	punpcklbw	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3],xmm0[4],xmm4[4],xmm0[5],xmm4[5],xmm0[6],xmm4[6],xmm0[7],xmm4[7]
	punpcklwd	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3]
	paddd	%xmm2, %xmm1
	movl	$8, %esi
	movdqa	%xmm1, %xmm2
	testl	%edi, %edi
	jne	.LBB34_178
	jmp	.LBB34_180
.LBB34_175:                             #   in Loop: Header=BB34_68 Depth=1
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
                                        # implicit-def: %XMM1
	pxor	%xmm4, %xmm4
	testl	%edi, %edi
	je	.LBB34_180
.LBB34_178:                             # %vector.body.preheader.new
                                        #   in Loop: Header=BB34_68 Depth=1
	movdqa	%xmm2, %xmm1
	.p2align	4, 0x90
.LBB34_179:                             # %vector.body
                                        #   Parent Loop BB34_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%esi, %rsi
	movd	(%rcx,%rsi), %xmm2      # xmm2 = mem[0],zero,zero,zero
	movd	4(%rcx,%rsi), %xmm3     # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3],xmm2[4],xmm4[4],xmm2[5],xmm4[5],xmm2[6],xmm4[6],xmm2[7],xmm4[7]
	punpcklwd	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3]
	punpcklbw	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3],xmm3[4],xmm4[4],xmm3[5],xmm4[5],xmm3[6],xmm4[6],xmm3[7],xmm4[7]
	punpcklwd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3]
	paddd	%xmm1, %xmm2
	paddd	%xmm0, %xmm3
	leal	8(%rsi), %edi
	movslq	%edi, %rdi
	movd	(%rcx,%rdi), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movd	4(%rcx,%rdi), %xmm0     # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3],xmm1[4],xmm4[4],xmm1[5],xmm4[5],xmm1[6],xmm4[6],xmm1[7],xmm4[7]
	punpcklwd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3]
	punpcklbw	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3],xmm0[4],xmm4[4],xmm0[5],xmm4[5],xmm0[6],xmm4[6],xmm0[7],xmm4[7]
	punpcklwd	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3]
	paddd	%xmm2, %xmm1
	paddd	%xmm3, %xmm0
	addl	$16, %esi
	cmpl	%esi, %edx
	jne	.LBB34_179
.LBB34_180:                             # %middle.block
                                        #   in Loop: Header=BB34_68 Depth=1
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %edi
	cmpl	%edx, %eax
	jne	.LBB34_181
	jmp	.LBB34_182
	.p2align	4, 0x90
.LBB34_68:                              # %.thread
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_221 Depth 2
                                        #     Child Loop BB34_133 Depth 2
                                        #     Child Loop BB34_107 Depth 2
                                        #     Child Loop BB34_209 Depth 2
                                        #     Child Loop BB34_198 Depth 2
                                        #     Child Loop BB34_161 Depth 2
                                        #     Child Loop BB34_179 Depth 2
                                        #     Child Loop BB34_181 Depth 2
                                        #     Child Loop BB34_186 Depth 2
                                        #     Child Loop BB34_192 Depth 2
	movq	40(%r14), %rdi
.Ltmp398:
.Lcfi379:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
	movq	%rax, %r12
.Ltmp399:
# BB#69:                                # %_ZN8NArchive3N7z10CInArchive6ReadIDEv.exit264
                                        #   in Loop: Header=BB34_68 Depth=1
	testq	%r12, %r12
	jne	.LBB34_98
# BB#70:                                # %.preheader358
	movq	(%rsp), %r8             # 8-byte Reload
	testl	%r8d, %r8d
	je	.LBB34_71
# BB#75:                                # %.lr.ph444
	movq	96(%rsp), %rax
	xorl	%esi, %esi
	cmpl	$7, %r8d
	ja	.LBB34_82
.LBB34_76:
	xorl	%edi, %edi
.LBB34_77:                              # %scalar.ph583.preheader
	movl	%r8d, %edx
	subl	%edi, %edx
	leal	-1(%r8), %ecx
	subl	%edi, %ecx
	andl	$3, %edx
	je	.LBB34_80
# BB#78:                                # %scalar.ph583.prol.preheader
	movslq	%edi, %rbp
	addq	%rax, %rbp
	negl	%edx
	.p2align	4, 0x90
.LBB34_79:                              # %scalar.ph583.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %ebx
	addl	%ebx, %esi
	incl	%edi
	incq	%rbp
	incl	%edx
	jne	.LBB34_79
.LBB34_80:                              # %scalar.ph583.prol.loopexit
	cmpl	$3, %ecx
	jb	.LBB34_91
	.p2align	4, 0x90
.LBB34_81:                              # %scalar.ph583
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edi, %rdi
	movzbl	(%rax,%rdi), %ecx
	addl	%esi, %ecx
	leal	1(%rdi), %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %edx
	addl	%ecx, %edx
	leal	2(%rdi), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %ecx
	addl	%edx, %ecx
	leal	3(%rdi), %edx
	movslq	%edx, %rdx
	movzbl	(%rax,%rdx), %esi
	addl	%ecx, %esi
	addl	$4, %edi
	cmpl	%edi, %r8d
	jne	.LBB34_81
	jmp	.LBB34_91
.LBB34_71:
	xorl	%esi, %esi
.LBB34_91:                              # %.preheader
	testl	%r15d, %r15d
	je	.LBB34_250
# BB#92:                                # %.lr.ph
	testl	%esi, %esi
	je	.LBB34_241
# BB#93:                                # %.lr.ph.split.preheader
	leaq	448(%r13), %r14
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB34_94:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	176(%r13), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	64(%rsp), %rdx
	movzbl	(%rdx,%rcx), %ecx
	movl	%ecx, %edx
	xorb	$1, %dl
	movb	%dl, 32(%rax)
	testb	%cl, %cl
	je	.LBB34_95
# BB#247:                               #   in Loop: Header=BB34_94 Depth=1
	movq	128(%rsp), %rcx
	movslq	%ebx, %rbx
	movzbl	(%rcx,%rbx), %ecx
	xorb	$1, %cl
	movb	%cl, 33(%rax)
	movq	96(%rsp), %rcx
	movb	(%rcx,%rbx), %r12b
	incl	%ebx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	.LBB34_248
	.p2align	4, 0x90
.LBB34_95:                              #   in Loop: Header=BB34_94 Depth=1
	movb	$0, 33(%rax)
	movq	296(%rsp), %rcx
	movslq	%r15d, %r15
	movq	(%rcx,%r15,8), %rcx
	movq	232(%rsp), %rdx
	movl	(%rdx,%r15,4), %edx
	movl	%edx, 12(%rax)
	movq	264(%rsp), %rdx
	movb	(%rdx,%r15), %dl
	incl	%r15d
	xorl	%r12d, %r12d
.LBB34_248:                             #   in Loop: Header=BB34_94 Depth=1
	movq	%rcx, (%rax)
	movb	%dl, 34(%rax)
.Ltmp497:
.Lcfi380:
	.cfi_escape 0x2e, 0x00
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp498:
# BB#249:                               # %_ZN13CRecordVectorIbE3AddEb.exit331
                                        #   in Loop: Header=BB34_94 Depth=1
	movq	464(%r13), %rax
	movslq	460(%r13), %rcx
	movb	%r12b, (%rax,%rcx)
	leal	1(%rcx), %eax
	movl	%eax, 460(%r13)
	incl	%ebp
	cmpl	32(%rsp), %ebp          # 4-byte Folded Reload
	jb	.LBB34_94
	jmp	.LBB34_250
.LBB34_82:                              # %min.iters.checked585
	movl	%r8d, %ecx
	andl	$-8, %ecx
	je	.LBB34_76
# BB#83:                                # %min.iters.checked585
	movl	%r8d, %edx
	decl	%edx
	movl	$0, %edi
	js	.LBB34_77
# BB#84:                                # %vector.body581.preheader
	leal	-8(%rcx), %edx
	movl	%edx, %esi
	shrl	$3, %esi
	btl	$3, %edx
	jb	.LBB34_85
# BB#86:                                # %vector.body581.prol
	movd	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movd	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	pxor	%xmm2, %xmm2
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	movl	$8, %edx
	testl	%esi, %esi
	jne	.LBB34_88
	jmp	.LBB34_90
.LBB34_241:                             # %.lr.ph.split.us.preheader
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB34_242:                             # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	176(%r13), %rsi
	movslq	%edx, %rdi
	movq	(%rsi,%rdi,8), %rsi
	movq	64(%rsp), %rbp
	movzbl	(%rbp,%rdi), %eax
	movl	%eax, %ebx
	xorb	$1, %bl
	testb	%al, %al
	movb	%bl, 32(%rsi)
	je	.LBB34_244
# BB#243:                               #   in Loop: Header=BB34_242 Depth=1
	movq	128(%rsp), %rax
	movslq	%ecx, %rcx
	movzbl	(%rax,%rcx), %eax
	xorb	$1, %al
	movb	%al, 33(%rsi)
	incl	%ecx
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	jmp	.LBB34_245
	.p2align	4, 0x90
.LBB34_244:                             #   in Loop: Header=BB34_242 Depth=1
	movb	$0, 33(%rsi)
	movq	296(%rsp), %rax
	movslq	%r8d, %r8
	movq	(%rax,%r8,8), %rdi
	movq	232(%rsp), %rax
	movl	(%rax,%r8,4), %eax
	movl	%eax, 12(%rsi)
	movq	264(%rsp), %rax
	movb	(%rax,%r8), %bpl
	incl	%r8d
.LBB34_245:                             #   in Loop: Header=BB34_242 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rdi, (%rsi)
	movb	%bpl, 34(%rsi)
	incl	%edx
	cmpl	%edx, %eax
	jne	.LBB34_242
.LBB34_250:                             # %._crit_edge
.Ltmp502:
.Lcfi381:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp503:
# BB#251:
.Ltmp507:
.Lcfi382:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp508:
# BB#252:
.Ltmp512:
.Lcfi383:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp513:
.LBB34_253:
.Ltmp517:
.Lcfi384:
	.cfi_escape 0x2e, 0x00
	leaq	216(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp518:
# BB#254:
.Ltmp520:
.Lcfi385:
	.cfi_escape 0x2e, 0x00
	leaq	248(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp521:
# BB#255:
.Ltmp523:
.Lcfi386:
	.cfi_escape 0x2e, 0x00
	leaq	280(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp524:
# BB#256:
	xorl	%ebx, %ebx
.LBB34_257:                             # %_ZN8NArchive3N7z10CInArchive6ReadIDEv.exit
	movq	$_ZTV13CObjectVectorI7CBufferIhEE+16, 144(%rsp)
.Ltmp526:
.Lcfi387:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp527:
# BB#258:                               # %_ZN13CObjectVectorI7CBufferIhEED2Ev.exit270
.Lcfi388:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
	movl	%ebx, %eax
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB34_85:
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	pxor	%xmm1, %xmm1
	testl	%esi, %esi
	je	.LBB34_90
.LBB34_88:                              # %vector.body581.preheader.new
	pxor	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB34_89:                              # %vector.body581
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rdx
	movd	(%rax,%rdx), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movd	4(%rax,%rdx), %xmm4     # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	punpcklwd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3]
	punpcklbw	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3],xmm4[4],xmm2[4],xmm4[5],xmm2[5],xmm4[6],xmm2[6],xmm4[7],xmm2[7]
	punpcklwd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1],xmm4[2],xmm2[2],xmm4[3],xmm2[3]
	paddd	%xmm0, %xmm3
	paddd	%xmm1, %xmm4
	leal	8(%rdx), %esi
	movslq	%esi, %rsi
	movd	(%rax,%rsi), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movd	4(%rax,%rsi), %xmm1     # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3],xmm0[4],xmm2[4],xmm0[5],xmm2[5],xmm0[6],xmm2[6],xmm0[7],xmm2[7]
	punpcklwd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1],xmm0[2],xmm2[2],xmm0[3],xmm2[3]
	punpcklbw	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3],xmm1[4],xmm2[4],xmm1[5],xmm2[5],xmm1[6],xmm2[6],xmm1[7],xmm2[7]
	punpcklwd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1],xmm1[2],xmm2[2],xmm1[3],xmm2[3]
	paddd	%xmm3, %xmm0
	paddd	%xmm4, %xmm1
	addl	$16, %edx
	cmpl	%edx, %ecx
	jne	.LBB34_89
.LBB34_90:                              # %middle.block582
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %esi
	cmpl	%ecx, %r8d
	movl	%ecx, %edi
	jne	.LBB34_77
	jmp	.LBB34_91
.LBB34_225:
.Ltmp406:
.Lcfi389:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp407:
# BB#226:                               # %.noexc323
.LBB34_222:
.Ltmp404:
.Lcfi390:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp405:
# BB#223:                               # %.noexc322
.LBB34_37:
.Lcfi391:
	.cfi_escape 0x2e, 0x00
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1, (%rax)
.Ltmp354:
.Lcfi392:
	.cfi_escape 0x2e, 0x00
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp355:
# BB#38:                                # %.noexc
.LBB34_135:
.Ltmp464:
.Lcfi393:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp465:
# BB#136:                               # %.noexc271
.LBB34_232:
.Ltmp494:
.Lcfi394:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp495:
# BB#233:                               # %.noexc327
.LBB34_238:
.Ltmp491:
.Lcfi395:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp492:
# BB#239:                               # %.noexc328
.LBB34_163:
.Ltmp439:
.Lcfi396:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp440:
# BB#164:                               # %.noexc282
.LBB34_200:
.Ltmp430:
.Lcfi397:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp431:
# BB#201:                               # %.noexc307
.LBB34_211:
.Ltmp421:
.Lcfi398:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp422:
# BB#212:                               # %.noexc319
.LBB34_17:
.Ltmp532:
.Lcfi399:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp533:
# BB#41:                                # %.noexc243
.LBB34_44:
.Ltmp515:
.Lcfi400:
	.cfi_escape 0x2e, 0x00
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp516:
# BB#45:                                # %.noexc245
.LBB34_240:
.Ltmp493:
	jmp	.LBB34_260
.LBB34_137:
.Ltmp466:
	jmp	.LBB34_148
.LBB34_40:                              # %.loopexit.split-lp387
.Ltmp356:
	jmp	.LBB34_269
.LBB34_120:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp
.Ltmp496:
	jmp	.LBB34_260
.LBB34_227:
.Ltmp408:
	jmp	.LBB34_260
.LBB34_287:
.Ltmp350:
	jmp	.LBB34_279
.LBB34_122:
.Ltmp487:
	jmp	.LBB34_260
.LBB34_146:
.Ltmp471:
	jmp	.LBB34_155
.LBB34_291:
.Ltmp514:
	jmp	.LBB34_269
.LBB34_264:
.Ltmp509:
	jmp	.LBB34_265
.LBB34_262:
.Ltmp504:
	movq	%rax, %rbp
	jmp	.LBB34_263
.LBB34_72:
.Ltmp374:
	jmp	.LBB34_269
.LBB34_123:
.Ltmp479:
	jmp	.LBB34_124
.LBB34_153:
.Ltmp476:
	jmp	.LBB34_260
.LBB34_147:
.Ltmp463:
.LBB34_148:
	movq	%rax, %rbp
	cmpb	$0, 192(%rsp)
	je	.LBB34_156
# BB#149:
	movq	184(%rsp), %rbx
	leaq	8(%rbx), %rdi
.Ltmp467:
.Lcfi401:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp468:
# BB#150:                               # %.noexc276
	movslq	20(%rbx), %rax
	testq	%rax, %rax
	je	.LBB34_152
# BB#151:
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbx)
.LBB34_152:                             # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i275
	movb	$0, 192(%rsp)
	jmp	.LBB34_156
.LBB34_154:
.Ltmp460:
.LBB34_155:
	movq	%rax, %rbp
.LBB34_156:
.Ltmp472:
.Lcfi402:
	.cfi_escape 0x2e, 0x00
	leaq	344(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp473:
	jmp	.LBB34_261
.LBB34_278:
.Ltmp347:
	jmp	.LBB34_279
.LBB34_274:
.Ltmp525:
.LBB34_279:
	movq	%rax, %rbp
	jmp	.LBB34_280
.LBB34_272:
.Ltmp522:
	movq	%rax, %rbp
	jmp	.LBB34_273
.LBB34_267:
.Ltmp519:
	movq	%rax, %rbp
	jmp	.LBB34_271
.LBB34_97:                              # %.loopexit.split-lp380
.Ltmp394:
	jmp	.LBB34_265
.LBB34_74:
.Ltmp389:
	jmp	.LBB34_269
.LBB34_275:
.Ltmp528:
	movq	%rax, %rbp
.Ltmp529:
.Lcfi403:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp530:
	jmp	.LBB34_276
.LBB34_277:
.Ltmp531:
.Lcfi404:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB34_268:
.Ltmp534:
	jmp	.LBB34_269
.LBB34_246:                             # %.us-lcssa
.Ltmp499:
	jmp	.LBB34_260
.LBB34_20:
.Ltmp353:
	jmp	.LBB34_269
.LBB34_96:                              # %.loopexit379
.Ltmp397:
.LBB34_265:
	movq	%rax, %rbp
	jmp	.LBB34_266
.LBB34_288:
.Ltmp379:
	movq	%rax, %rbp
.Lcfi405:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB34_270
.LBB34_115:                             # %.loopexit.split-lp.loopexit
.Ltmp450:
	jmp	.LBB34_260
.LBB34_114:                             # %.loopexit
.Ltmp457:
	jmp	.LBB34_260
.LBB34_121:
.Ltmp482:
.LBB34_124:
	movq	%rax, %rbp
	cmpb	$0, 208(%rsp)
	je	.LBB34_261
# BB#125:
	movq	200(%rsp), %rbx
	leaq	8(%rbx), %rdi
.Ltmp483:
.Lcfi406:
	.cfi_escape 0x2e, 0x00
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp484:
# BB#126:                               # %.noexc268
	movslq	20(%rbx), %rax
	testq	%rax, %rax
	je	.LBB34_128
# BB#127:
	movq	24(%rbx), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%rbx)
.LBB34_128:                             # %_ZN8NArchive3N7z10CInArchive16DeleteByteStreamEv.exit.i.i267
	movb	$0, 208(%rsp)
	jmp	.LBB34_261
.LBB34_116:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp443:
	jmp	.LBB34_260
.LBB34_117:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp434:
	jmp	.LBB34_260
.LBB34_118:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp425:
	jmp	.LBB34_260
.LBB34_289:
.Ltmp403:
	jmp	.LBB34_260
.LBB34_259:
.Ltmp400:
	jmp	.LBB34_260
.LBB34_119:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp490:
.LBB34_260:
	movq	%rax, %rbp
.LBB34_261:
.Ltmp500:
.Lcfi407:
	.cfi_escape 0x2e, 0x00
	leaq	80(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp501:
.LBB34_263:
.Ltmp505:
.Lcfi408:
	.cfi_escape 0x2e, 0x00
	leaq	112(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp506:
.LBB34_266:
.Ltmp510:
.Lcfi409:
	.cfi_escape 0x2e, 0x00
	leaq	48(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp511:
	jmp	.LBB34_270
.LBB34_73:
.Ltmp382:
	jmp	.LBB34_269
.LBB34_39:                              # %.loopexit386
.Ltmp363:
.LBB34_269:
	movq	%rax, %rbp
.LBB34_270:
.Ltmp535:
.Lcfi410:
	.cfi_escape 0x2e, 0x00
	leaq	216(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp536:
.LBB34_271:
.Ltmp537:
.Lcfi411:
	.cfi_escape 0x2e, 0x00
	leaq	248(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp538:
.LBB34_273:
.Ltmp539:
.Lcfi412:
	.cfi_escape 0x2e, 0x00
	leaq	280(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp540:
.LBB34_280:
	movq	$_ZTV13CObjectVectorI7CBufferIhEE+16, 144(%rsp)
.Ltmp541:
.Lcfi413:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp542:
# BB#281:
.Ltmp547:
.Lcfi414:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp548:
.LBB34_276:                             # %unwind_resume
.Lcfi415:
	.cfi_escape 0x2e, 0x00
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB34_282:
.Ltmp543:
	movq	%rax, %rbx
.Ltmp544:
.Lcfi416:
	.cfi_escape 0x2e, 0x00
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp545:
	jmp	.LBB34_285
.LBB34_283:
.Ltmp546:
.Lcfi417:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB34_284:
.Ltmp549:
	movq	%rax, %rbx
.LBB34_285:                             # %.body
.Lcfi418:
	.cfi_escape 0x2e, 0x00
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end34:
	.size	_ZN8NArchive3N7z10CInArchive10ReadHeaderERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb, .Lfunc_end34-_ZN8NArchive3N7z10CInArchive10ReadHeaderERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI34_0:
	.quad	.LBB34_157
	.quad	.LBB34_194
	.quad	.LBB34_205
	.quad	.LBB34_104
	.quad	.LBB34_217
	.quad	.LBB34_218
	.quad	.LBB34_219
	.quad	.LBB34_129
	.quad	.LBB34_231
	.quad	.LBB34_231
	.quad	.LBB34_228
	.quad	.LBB34_102
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\210\006"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\377\005"              # Call site table length
	.long	.Lfunc_begin9-.Lfunc_begin9 # >> Call Site 1 <<
	.long	.Ltmp345-.Lfunc_begin9  #   Call between .Lfunc_begin9 and .Ltmp345
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp345-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp346-.Ltmp345       #   Call between .Ltmp345 and .Ltmp346
	.long	.Ltmp347-.Lfunc_begin9  #     jumps to .Ltmp347
	.byte	0                       #   On action: cleanup
	.long	.Ltmp348-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp349-.Ltmp348       #   Call between .Ltmp348 and .Ltmp349
	.long	.Ltmp350-.Lfunc_begin9  #     jumps to .Ltmp350
	.byte	0                       #   On action: cleanup
	.long	.Ltmp364-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp367-.Ltmp364       #   Call between .Ltmp364 and .Ltmp367
	.long	.Ltmp534-.Lfunc_begin9  #     jumps to .Ltmp534
	.byte	0                       #   On action: cleanup
	.long	.Ltmp351-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp353-.Lfunc_begin9  #     jumps to .Ltmp353
	.byte	0                       #   On action: cleanup
	.long	.Ltmp357-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Ltmp362-.Ltmp357       #   Call between .Ltmp357 and .Ltmp362
	.long	.Ltmp363-.Lfunc_begin9  #     jumps to .Ltmp363
	.byte	0                       #   On action: cleanup
	.long	.Ltmp368-.Lfunc_begin9  # >> Call Site 7 <<
	.long	.Ltmp371-.Ltmp368       #   Call between .Ltmp368 and .Ltmp371
	.long	.Ltmp534-.Lfunc_begin9  #     jumps to .Ltmp534
	.byte	0                       #   On action: cleanup
	.long	.Ltmp372-.Lfunc_begin9  # >> Call Site 8 <<
	.long	.Ltmp373-.Ltmp372       #   Call between .Ltmp372 and .Ltmp373
	.long	.Ltmp374-.Lfunc_begin9  #     jumps to .Ltmp374
	.byte	0                       #   On action: cleanup
	.long	.Ltmp375-.Lfunc_begin9  # >> Call Site 9 <<
	.long	.Ltmp376-.Ltmp375       #   Call between .Ltmp375 and .Ltmp376
	.long	.Ltmp382-.Lfunc_begin9  #     jumps to .Ltmp382
	.byte	0                       #   On action: cleanup
	.long	.Ltmp377-.Lfunc_begin9  # >> Call Site 10 <<
	.long	.Ltmp378-.Ltmp377       #   Call between .Ltmp377 and .Ltmp378
	.long	.Ltmp379-.Lfunc_begin9  #     jumps to .Ltmp379
	.byte	0                       #   On action: cleanup
	.long	.Ltmp380-.Lfunc_begin9  # >> Call Site 11 <<
	.long	.Ltmp381-.Ltmp380       #   Call between .Ltmp380 and .Ltmp381
	.long	.Ltmp382-.Lfunc_begin9  #     jumps to .Ltmp382
	.byte	0                       #   On action: cleanup
	.long	.Ltmp383-.Lfunc_begin9  # >> Call Site 12 <<
	.long	.Ltmp388-.Ltmp383       #   Call between .Ltmp383 and .Ltmp388
	.long	.Ltmp389-.Lfunc_begin9  #     jumps to .Ltmp389
	.byte	0                       #   On action: cleanup
	.long	.Ltmp390-.Lfunc_begin9  # >> Call Site 13 <<
	.long	.Ltmp393-.Ltmp390       #   Call between .Ltmp390 and .Ltmp393
	.long	.Ltmp394-.Lfunc_begin9  #     jumps to .Ltmp394
	.byte	0                       #   On action: cleanup
	.long	.Ltmp395-.Lfunc_begin9  # >> Call Site 14 <<
	.long	.Ltmp396-.Ltmp395       #   Call between .Ltmp395 and .Ltmp396
	.long	.Ltmp397-.Lfunc_begin9  #     jumps to .Ltmp397
	.byte	0                       #   On action: cleanup
	.long	.Ltmp401-.Lfunc_begin9  # >> Call Site 15 <<
	.long	.Ltmp402-.Ltmp401       #   Call between .Ltmp401 and .Ltmp402
	.long	.Ltmp403-.Lfunc_begin9  #     jumps to .Ltmp403
	.byte	0                       #   On action: cleanup
	.long	.Ltmp435-.Lfunc_begin9  # >> Call Site 16 <<
	.long	.Ltmp438-.Ltmp435       #   Call between .Ltmp435 and .Ltmp438
	.long	.Ltmp490-.Lfunc_begin9  #     jumps to .Ltmp490
	.byte	0                       #   On action: cleanup
	.long	.Ltmp441-.Lfunc_begin9  # >> Call Site 17 <<
	.long	.Ltmp442-.Ltmp441       #   Call between .Ltmp441 and .Ltmp442
	.long	.Ltmp443-.Lfunc_begin9  #     jumps to .Ltmp443
	.byte	0                       #   On action: cleanup
	.long	.Ltmp417-.Lfunc_begin9  # >> Call Site 18 <<
	.long	.Ltmp420-.Ltmp417       #   Call between .Ltmp417 and .Ltmp420
	.long	.Ltmp490-.Lfunc_begin9  #     jumps to .Ltmp490
	.byte	0                       #   On action: cleanup
	.long	.Ltmp423-.Lfunc_begin9  # >> Call Site 19 <<
	.long	.Ltmp424-.Ltmp423       #   Call between .Ltmp423 and .Ltmp424
	.long	.Ltmp425-.Lfunc_begin9  #     jumps to .Ltmp425
	.byte	0                       #   On action: cleanup
	.long	.Ltmp411-.Lfunc_begin9  # >> Call Site 20 <<
	.long	.Ltmp429-.Ltmp411       #   Call between .Ltmp411 and .Ltmp429
	.long	.Ltmp490-.Lfunc_begin9  #     jumps to .Ltmp490
	.byte	0                       #   On action: cleanup
	.long	.Ltmp432-.Lfunc_begin9  # >> Call Site 21 <<
	.long	.Ltmp433-.Ltmp432       #   Call between .Ltmp432 and .Ltmp433
	.long	.Ltmp434-.Lfunc_begin9  #     jumps to .Ltmp434
	.byte	0                       #   On action: cleanup
	.long	.Ltmp409-.Lfunc_begin9  # >> Call Site 22 <<
	.long	.Ltmp410-.Ltmp409       #   Call between .Ltmp409 and .Ltmp410
	.long	.Ltmp490-.Lfunc_begin9  #     jumps to .Ltmp490
	.byte	0                       #   On action: cleanup
	.long	.Ltmp458-.Lfunc_begin9  # >> Call Site 23 <<
	.long	.Ltmp459-.Ltmp458       #   Call between .Ltmp458 and .Ltmp459
	.long	.Ltmp460-.Lfunc_begin9  #     jumps to .Ltmp460
	.byte	0                       #   On action: cleanup
	.long	.Ltmp461-.Lfunc_begin9  # >> Call Site 24 <<
	.long	.Ltmp462-.Ltmp461       #   Call between .Ltmp461 and .Ltmp462
	.long	.Ltmp463-.Lfunc_begin9  #     jumps to .Ltmp463
	.byte	0                       #   On action: cleanup
	.long	.Ltmp469-.Lfunc_begin9  # >> Call Site 25 <<
	.long	.Ltmp470-.Ltmp469       #   Call between .Ltmp469 and .Ltmp470
	.long	.Ltmp471-.Lfunc_begin9  #     jumps to .Ltmp471
	.byte	0                       #   On action: cleanup
	.long	.Ltmp474-.Lfunc_begin9  # >> Call Site 26 <<
	.long	.Ltmp475-.Ltmp474       #   Call between .Ltmp474 and .Ltmp475
	.long	.Ltmp476-.Lfunc_begin9  #     jumps to .Ltmp476
	.byte	0                       #   On action: cleanup
	.long	.Ltmp477-.Lfunc_begin9  # >> Call Site 27 <<
	.long	.Ltmp478-.Ltmp477       #   Call between .Ltmp477 and .Ltmp478
	.long	.Ltmp479-.Lfunc_begin9  #     jumps to .Ltmp479
	.byte	0                       #   On action: cleanup
	.long	.Ltmp480-.Lfunc_begin9  # >> Call Site 28 <<
	.long	.Ltmp481-.Ltmp480       #   Call between .Ltmp480 and .Ltmp481
	.long	.Ltmp482-.Lfunc_begin9  #     jumps to .Ltmp482
	.byte	0                       #   On action: cleanup
	.long	.Ltmp485-.Lfunc_begin9  # >> Call Site 29 <<
	.long	.Ltmp486-.Ltmp485       #   Call between .Ltmp485 and .Ltmp486
	.long	.Ltmp487-.Lfunc_begin9  #     jumps to .Ltmp487
	.byte	0                       #   On action: cleanup
	.long	.Ltmp413-.Lfunc_begin9  # >> Call Site 30 <<
	.long	.Ltmp447-.Ltmp413       #   Call between .Ltmp413 and .Ltmp447
	.long	.Ltmp490-.Lfunc_begin9  #     jumps to .Ltmp490
	.byte	0                       #   On action: cleanup
	.long	.Ltmp448-.Lfunc_begin9  # >> Call Site 31 <<
	.long	.Ltmp449-.Ltmp448       #   Call between .Ltmp448 and .Ltmp449
	.long	.Ltmp450-.Lfunc_begin9  #     jumps to .Ltmp450
	.byte	0                       #   On action: cleanup
	.long	.Ltmp451-.Lfunc_begin9  # >> Call Site 32 <<
	.long	.Ltmp454-.Ltmp451       #   Call between .Ltmp451 and .Ltmp454
	.long	.Ltmp490-.Lfunc_begin9  #     jumps to .Ltmp490
	.byte	0                       #   On action: cleanup
	.long	.Ltmp455-.Lfunc_begin9  # >> Call Site 33 <<
	.long	.Ltmp456-.Ltmp455       #   Call between .Ltmp455 and .Ltmp456
	.long	.Ltmp457-.Lfunc_begin9  #     jumps to .Ltmp457
	.byte	0                       #   On action: cleanup
	.long	.Ltmp488-.Lfunc_begin9  # >> Call Site 34 <<
	.long	.Ltmp489-.Ltmp488       #   Call between .Ltmp488 and .Ltmp489
	.long	.Ltmp490-.Lfunc_begin9  #     jumps to .Ltmp490
	.byte	0                       #   On action: cleanup
	.long	.Ltmp398-.Lfunc_begin9  # >> Call Site 35 <<
	.long	.Ltmp399-.Ltmp398       #   Call between .Ltmp398 and .Ltmp399
	.long	.Ltmp400-.Lfunc_begin9  #     jumps to .Ltmp400
	.byte	0                       #   On action: cleanup
	.long	.Ltmp497-.Lfunc_begin9  # >> Call Site 36 <<
	.long	.Ltmp498-.Ltmp497       #   Call between .Ltmp497 and .Ltmp498
	.long	.Ltmp499-.Lfunc_begin9  #     jumps to .Ltmp499
	.byte	0                       #   On action: cleanup
	.long	.Ltmp502-.Lfunc_begin9  # >> Call Site 37 <<
	.long	.Ltmp503-.Ltmp502       #   Call between .Ltmp502 and .Ltmp503
	.long	.Ltmp504-.Lfunc_begin9  #     jumps to .Ltmp504
	.byte	0                       #   On action: cleanup
	.long	.Ltmp507-.Lfunc_begin9  # >> Call Site 38 <<
	.long	.Ltmp508-.Ltmp507       #   Call between .Ltmp507 and .Ltmp508
	.long	.Ltmp509-.Lfunc_begin9  #     jumps to .Ltmp509
	.byte	0                       #   On action: cleanup
	.long	.Ltmp512-.Lfunc_begin9  # >> Call Site 39 <<
	.long	.Ltmp513-.Ltmp512       #   Call between .Ltmp512 and .Ltmp513
	.long	.Ltmp514-.Lfunc_begin9  #     jumps to .Ltmp514
	.byte	0                       #   On action: cleanup
	.long	.Ltmp517-.Lfunc_begin9  # >> Call Site 40 <<
	.long	.Ltmp518-.Ltmp517       #   Call between .Ltmp517 and .Ltmp518
	.long	.Ltmp519-.Lfunc_begin9  #     jumps to .Ltmp519
	.byte	0                       #   On action: cleanup
	.long	.Ltmp520-.Lfunc_begin9  # >> Call Site 41 <<
	.long	.Ltmp521-.Ltmp520       #   Call between .Ltmp520 and .Ltmp521
	.long	.Ltmp522-.Lfunc_begin9  #     jumps to .Ltmp522
	.byte	0                       #   On action: cleanup
	.long	.Ltmp523-.Lfunc_begin9  # >> Call Site 42 <<
	.long	.Ltmp524-.Ltmp523       #   Call between .Ltmp523 and .Ltmp524
	.long	.Ltmp525-.Lfunc_begin9  #     jumps to .Ltmp525
	.byte	0                       #   On action: cleanup
	.long	.Ltmp526-.Lfunc_begin9  # >> Call Site 43 <<
	.long	.Ltmp527-.Ltmp526       #   Call between .Ltmp526 and .Ltmp527
	.long	.Ltmp528-.Lfunc_begin9  #     jumps to .Ltmp528
	.byte	0                       #   On action: cleanup
	.long	.Ltmp527-.Lfunc_begin9  # >> Call Site 44 <<
	.long	.Ltmp406-.Ltmp527       #   Call between .Ltmp527 and .Ltmp406
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp406-.Lfunc_begin9  # >> Call Site 45 <<
	.long	.Ltmp405-.Ltmp406       #   Call between .Ltmp406 and .Ltmp405
	.long	.Ltmp408-.Lfunc_begin9  #     jumps to .Ltmp408
	.byte	0                       #   On action: cleanup
	.long	.Ltmp405-.Lfunc_begin9  # >> Call Site 46 <<
	.long	.Ltmp354-.Ltmp405       #   Call between .Ltmp405 and .Ltmp354
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp354-.Lfunc_begin9  # >> Call Site 47 <<
	.long	.Ltmp355-.Ltmp354       #   Call between .Ltmp354 and .Ltmp355
	.long	.Ltmp356-.Lfunc_begin9  #     jumps to .Ltmp356
	.byte	0                       #   On action: cleanup
	.long	.Ltmp464-.Lfunc_begin9  # >> Call Site 48 <<
	.long	.Ltmp465-.Ltmp464       #   Call between .Ltmp464 and .Ltmp465
	.long	.Ltmp466-.Lfunc_begin9  #     jumps to .Ltmp466
	.byte	0                       #   On action: cleanup
	.long	.Ltmp494-.Lfunc_begin9  # >> Call Site 49 <<
	.long	.Ltmp495-.Ltmp494       #   Call between .Ltmp494 and .Ltmp495
	.long	.Ltmp496-.Lfunc_begin9  #     jumps to .Ltmp496
	.byte	0                       #   On action: cleanup
	.long	.Ltmp491-.Lfunc_begin9  # >> Call Site 50 <<
	.long	.Ltmp492-.Ltmp491       #   Call between .Ltmp491 and .Ltmp492
	.long	.Ltmp493-.Lfunc_begin9  #     jumps to .Ltmp493
	.byte	0                       #   On action: cleanup
	.long	.Ltmp439-.Lfunc_begin9  # >> Call Site 51 <<
	.long	.Ltmp422-.Ltmp439       #   Call between .Ltmp439 and .Ltmp422
	.long	.Ltmp496-.Lfunc_begin9  #     jumps to .Ltmp496
	.byte	0                       #   On action: cleanup
	.long	.Ltmp532-.Lfunc_begin9  # >> Call Site 52 <<
	.long	.Ltmp516-.Ltmp532       #   Call between .Ltmp532 and .Ltmp516
	.long	.Ltmp534-.Lfunc_begin9  #     jumps to .Ltmp534
	.byte	0                       #   On action: cleanup
	.long	.Ltmp467-.Lfunc_begin9  # >> Call Site 53 <<
	.long	.Ltmp473-.Ltmp467       #   Call between .Ltmp467 and .Ltmp473
	.long	.Ltmp549-.Lfunc_begin9  #     jumps to .Ltmp549
	.byte	1                       #   On action: 1
	.long	.Ltmp529-.Lfunc_begin9  # >> Call Site 54 <<
	.long	.Ltmp530-.Ltmp529       #   Call between .Ltmp529 and .Ltmp530
	.long	.Ltmp531-.Lfunc_begin9  #     jumps to .Ltmp531
	.byte	1                       #   On action: 1
	.long	.Ltmp483-.Lfunc_begin9  # >> Call Site 55 <<
	.long	.Ltmp540-.Ltmp483       #   Call between .Ltmp483 and .Ltmp540
	.long	.Ltmp549-.Lfunc_begin9  #     jumps to .Ltmp549
	.byte	1                       #   On action: 1
	.long	.Ltmp541-.Lfunc_begin9  # >> Call Site 56 <<
	.long	.Ltmp542-.Ltmp541       #   Call between .Ltmp541 and .Ltmp542
	.long	.Ltmp543-.Lfunc_begin9  #     jumps to .Ltmp543
	.byte	1                       #   On action: 1
	.long	.Ltmp547-.Lfunc_begin9  # >> Call Site 57 <<
	.long	.Ltmp548-.Ltmp547       #   Call between .Ltmp547 and .Ltmp548
	.long	.Ltmp549-.Lfunc_begin9  #     jumps to .Ltmp549
	.byte	1                       #   On action: 1
	.long	.Ltmp548-.Lfunc_begin9  # >> Call Site 58 <<
	.long	.Ltmp544-.Ltmp548       #   Call between .Ltmp548 and .Ltmp544
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp544-.Lfunc_begin9  # >> Call Site 59 <<
	.long	.Ltmp545-.Ltmp544       #   Call between .Ltmp544 and .Ltmp545
	.long	.Ltmp546-.Lfunc_begin9  #     jumps to .Ltmp546
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI7CBufferIhEED2Ev,"axG",@progbits,_ZN13CObjectVectorI7CBufferIhEED2Ev,comdat
	.weak	_ZN13CObjectVectorI7CBufferIhEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI7CBufferIhEED2Ev,@function
_ZN13CObjectVectorI7CBufferIhEED2Ev:    # @_ZN13CObjectVectorI7CBufferIhEED2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi419:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi420:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi421:
	.cfi_def_cfa_offset 32
.Lcfi422:
	.cfi_offset %rbx, -24
.Lcfi423:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI7CBufferIhEE+16, (%rbx)
.Ltmp550:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp551:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB35_2:
.Ltmp552:
	movq	%rax, %r14
.Ltmp553:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp554:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_4:
.Ltmp555:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN13CObjectVectorI7CBufferIhEED2Ev, .Lfunc_end35-_ZN13CObjectVectorI7CBufferIhEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp550-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp551-.Ltmp550       #   Call between .Ltmp550 and .Ltmp551
	.long	.Ltmp552-.Lfunc_begin10 #     jumps to .Ltmp552
	.byte	0                       #   On action: cleanup
	.long	.Ltmp551-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp553-.Ltmp551       #   Call between .Ltmp551 and .Ltmp553
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp553-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp554-.Ltmp553       #   Call between .Ltmp553 and .Ltmp554
	.long	.Ltmp555-.Lfunc_begin10 #     jumps to .Ltmp555
	.byte	1                       #   On action: 1
	.long	.Ltmp554-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp554   #   Call between .Ltmp554 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z18CArchiveDatabaseEx25FillFolderStartPackStreamEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z18CArchiveDatabaseEx25FillFolderStartPackStreamEv,@function
_ZN8NArchive3N7z18CArchiveDatabaseEx25FillFolderStartPackStreamEv: # @_ZN8NArchive3N7z18CArchiveDatabaseEx25FillFolderStartPackStreamEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi424:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi425:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi426:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi427:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi428:
	.cfi_def_cfa_offset 48
.Lcfi429:
	.cfi_offset %rbx, -40
.Lcfi430:
	.cfi_offset %r14, -32
.Lcfi431:
	.cfi_offset %r15, -24
.Lcfi432:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	leaq	584(%r15), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	108(%r15), %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	cmpl	$0, 108(%r15)
	jle	.LBB36_3
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB36_2:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	600(%r15), %rax
	movslq	596(%r15), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	596(%r15)
	movq	112(%r15), %rax
	movq	(%rax,%rbx,8), %rax
	addl	76(%rax), %ebp
	incq	%rbx
	movslq	108(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB36_2
.LBB36_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end36:
	.size	_ZN8NArchive3N7z18CArchiveDatabaseEx25FillFolderStartPackStreamEv, .Lfunc_end36-_ZN8NArchive3N7z18CArchiveDatabaseEx25FillFolderStartPackStreamEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z18CArchiveDatabaseEx12FillStartPosEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z18CArchiveDatabaseEx12FillStartPosEv,@function
_ZN8NArchive3N7z18CArchiveDatabaseEx12FillStartPosEv: # @_ZN8NArchive3N7z18CArchiveDatabaseEx12FillStartPosEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi433:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi434:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi435:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi436:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi437:
	.cfi_def_cfa_offset 48
.Lcfi438:
	.cfi_offset %rbx, -40
.Lcfi439:
	.cfi_offset %r12, -32
.Lcfi440:
	.cfi_offset %r14, -24
.Lcfi441:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	552(%r15), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	12(%r15), %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	cmpl	$0, 12(%r15)
	jle	.LBB37_3
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB37_2:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	568(%r15), %rax
	movslq	564(%r15), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 564(%r15)
	movq	16(%r15), %rax
	addq	(%rax,%rbx,8), %r12
	incq	%rbx
	movslq	12(%r15), %rax
	cmpq	%rax, %rbx
	jl	.LBB37_2
.LBB37_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end37:
	.size	_ZN8NArchive3N7z18CArchiveDatabaseEx12FillStartPosEv, .Lfunc_end37-_ZN8NArchive3N7z18CArchiveDatabaseEx12FillStartPosEv
	.cfi_endproc

	.globl	_ZN8NArchive3N7z18CArchiveDatabaseEx24FillFolderStartFileIndexEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z18CArchiveDatabaseEx24FillFolderStartFileIndexEv,@function
_ZN8NArchive3N7z18CArchiveDatabaseEx24FillFolderStartFileIndexEv: # @_ZN8NArchive3N7z18CArchiveDatabaseEx24FillFolderStartFileIndexEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi442:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi443:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi444:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi445:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi446:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi447:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi448:
	.cfi_def_cfa_offset 64
.Lcfi449:
	.cfi_offset %rbx, -56
.Lcfi450:
	.cfi_offset %r12, -48
.Lcfi451:
	.cfi_offset %r13, -40
.Lcfi452:
	.cfi_offset %r14, -32
.Lcfi453:
	.cfi_offset %r15, -24
.Lcfi454:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	616(%rbx), %r14
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	108(%rbx), %esi
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	leaq	648(%rbx), %rbp
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movl	172(%rbx), %esi
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	cmpl	$0, 172(%rbx)
	jle	.LBB38_14
# BB#1:                                 # %.lr.ph39
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB38_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_8 Depth 2
	movq	176(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movb	32(%rax), %r15b
	testl	%r12d, %r12d
	jne	.LBB38_5
# BB#3:                                 #   in Loop: Header=BB38_2 Depth=1
	testb	%r15b, %r15b
	jne	.LBB38_5
# BB#4:                                 #   in Loop: Header=BB38_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	664(%rbx), %rax
	movslq	660(%rbx), %rcx
	movl	$-1, (%rax,%rcx,4)
	incl	660(%rbx)
	xorl	%r12d, %r12d
	jmp	.LBB38_13
	.p2align	4, 0x90
.LBB38_5:                               #   in Loop: Header=BB38_2 Depth=1
	testl	%r12d, %r12d
	jne	.LBB38_11
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB38_2 Depth=1
	cmpl	108(%rbx), %ebp
	jge	.LBB38_10
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB38_2 Depth=1
	movslq	%ebp, %rbp
	.p2align	4, 0x90
.LBB38_8:                               # %.lr.ph
                                        #   Parent Loop BB38_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	632(%rbx), %rax
	movslq	628(%rbx), %rcx
	movl	%r13d, (%rax,%rcx,4)
	incl	628(%rbx)
	movq	144(%rbx), %rax
	cmpl	$0, (%rax,%rbp,4)
	jne	.LBB38_11
# BB#9:                                 #   in Loop: Header=BB38_8 Depth=2
	incq	%rbp
	movslq	108(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB38_8
	jmp	.LBB38_10
	.p2align	4, 0x90
.LBB38_11:                              # %.loopexit
                                        #   in Loop: Header=BB38_2 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	664(%rbx), %rax
	movslq	660(%rbx), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	660(%rbx)
	testb	%r15b, %r15b
	je	.LBB38_13
# BB#12:                                #   in Loop: Header=BB38_2 Depth=1
	incl	%r12d
	movq	144(%rbx), %rax
	movslq	%ebp, %rcx
	xorl	%edx, %edx
	cmpl	(%rax,%rcx,4), %r12d
	movl	$0, %eax
	cmovael	%eax, %r12d
	setae	%dl
	addl	%ebp, %edx
	movl	%edx, %ebp
.LBB38_13:                              #   in Loop: Header=BB38_2 Depth=1
	incq	%r13
	movslq	172(%rbx), %rax
	cmpq	%rax, %r13
	jl	.LBB38_2
.LBB38_14:                              # %._crit_edge40
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB38_10:                              # %.preheader._crit_edge
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Lfunc_end38:
	.size	_ZN8NArchive3N7z18CArchiveDatabaseEx24FillFolderStartFileIndexEv, .Lfunc_end38-_ZN8NArchive3N7z18CArchiveDatabaseEx24FillFolderStartFileIndexEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI39_0:
	.zero	16
	.text
	.globl	_ZN8NArchive3N7z10CInArchive13ReadDatabase2ERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive13ReadDatabase2ERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb,@function
_ZN8NArchive3N7z10CInArchive13ReadDatabase2ERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb: # @_ZN8NArchive3N7z10CInArchive13ReadDatabase2ERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%rbp
.Lcfi455:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi456:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi457:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi458:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi459:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi460:
	.cfi_def_cfa_offset 56
	subq	$568, %rsp              # imm = 0x238
.Lcfi461:
	.cfi_def_cfa_offset 624
.Lcfi462:
	.cfi_offset %rbx, -56
.Lcfi463:
	.cfi_offset %r12, -48
.Lcfi464:
	.cfi_offset %r13, -40
.Lcfi465:
	.cfi_offset %r14, -32
.Lcfi466:
	.cfi_offset %r15, -24
.Lcfi467:
	.cfi_offset %rbp, -16
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	%rbx, %rdi
	callq	_ZN8NArchive3N7z16CArchiveDatabase5ClearEv
	leaq	520(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	552(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	584(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	616(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	648(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	xorps	%xmm0, %xmm0
	movups	%xmm0, 680(%rbx)
	movq	48(%r12), %rax
	movq	%rax, 488(%rbx)
	movb	62(%r12), %al
	movb	%al, 480(%rbx)
	movb	63(%r12), %cl
	movb	%cl, 481(%rbx)
	testb	%al, %al
	jne	.LBB39_55
# BB#1:
	movl	64(%r12), %ebp
	leaq	68(%r12), %rdi
	movq	68(%r12), %r14
	movq	76(%r12), %r13
	movl	84(%r12), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	$20, %esi
	callq	CrcCalc
	movq	%r13, %rcx
	orq	%r14, %rcx
	jne	.LBB39_8
# BB#2:
	movl	8(%rsp), %ecx           # 4-byte Reload
	orl	%ebp, %ecx
	jne	.LBB39_8
# BB#3:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	leaq	24(%rsp), %rcx
	xorl	%esi, %esi
	movl	$1, %edx
	callq	*48(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB39_42
# BB#4:
	movl	$1, (%rsp)              # 4-byte Folded Spill
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	leaq	16(%rsp), %rcx
	xorl	%esi, %esi
	movl	$2, %edx
	callq	*48(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB39_7
# BB#5:
	movq	16(%rsp), %rax
	movq	24(%rsp), %rcx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	subq	%rcx, %rax
	cmpq	$500, %rax              # imm = 0x1F4
	movl	$500, %ebp              # imm = 0x1F4
	cmovbq	%rax, %rbp
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	%ebp, %ecx
	negl	%ecx
	movslq	%ecx, %rsi
	leaq	16(%rsp), %rcx
	movl	$2, %edx
	callq	*48(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB39_7
# BB#6:
	movq	(%r12), %rdi
	leaq	64(%rsp), %rsi
	movq	%rbp, %rdx
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %r15d
	testl	%r15d, %r15d
	je	.LBB39_43
.LBB39_7:                               # %.loopexit
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB39_42
	jmp	.LBB39_9
.LBB39_8:
	cmpl	%ebp, %eax
	jne	.LBB39_55
.LBB39_9:
	movq	48(%r12), %rax
	addq	$32, %rax
	movq	%rax, 496(%rbx)
	testq	%r13, %r13
	je	.LBB39_41
# BB#10:
	movq	%r13, %rax
	shrq	$32, %rax
	movl	$1, %r15d
	jne	.LBB39_42
# BB#11:
	testq	%r14, %r14
	js	.LBB39_42
# BB#12:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	callq	*48(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB39_42
# BB#13:
.Ltmp556:
	movq	%r13, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp557:
# BB#14:
	movq	(%r12), %rdi
.Ltmp559:
	movq	%rbp, %rsi
	movq	%r13, %rdx
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	movl	%eax, %r15d
.Ltmp560:
# BB#15:
	testl	%r15d, %r15d
	jne	.LBB39_40
# BB#16:
	leaq	32(%r13), %rax
	addq	%rax, 88(%r12)
	leaq	32(%r13,%r14), %rax
	movq	%rax, 688(%rbx)
	movl	%r13d, %esi
.Ltmp561:
	movq	%rbp, %rdi
	callq	CrcCalc
.Ltmp562:
# BB#17:
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jne	.LBB39_56
# BB#18:                                # %_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPKhm.exit.i
.Ltmp566:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp567:
# BB#19:                                # %.noexc193
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movq	$0, 16(%r15)
	leaq	8(%r12), %r14
.Ltmp568:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp569:
# BB#20:
	movq	24(%r12), %rax
	movslq	20(%r12), %rcx
	movq	%r15, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%r12)
	movq	24(%r12), %rax
	movq	(%rax,%rcx,8), %rdi
	movq	%rdi, 40(%r12)
	movq	%rbp, (%rdi)
	movq	%r13, 8(%rdi)
	movq	$0, 16(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 72(%rsp)
	movq	$8, 88(%rsp)
	movq	$_ZTV13CObjectVectorI7CBufferIhEE+16, 64(%rsp)
.Ltmp571:
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
.Ltmp572:
# BB#21:                                # %_ZN8NArchive3N7z10CInArchive6ReadIDEv.exit
	cmpq	$1, %rax
	je	.LBB39_34
# BB#22:                                # %_ZN8NArchive3N7z10CInArchive6ReadIDEv.exit
	cmpq	$23, %rax
	jne	.LBB39_58
# BB#23:
	movq	496(%rbx), %rsi
	leaq	512(%rbx), %rdx
.Ltmp574:
	leaq	64(%rsp), %rcx
	movq	%r12, %rdi
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	callq	_ZN8NArchive3N7z10CInArchive26ReadAndDecodePackedStreamsEyRyR13CObjectVectorI7CBufferIhEEP22ICryptoGetTextPasswordRb
	movl	%eax, %r15d
.Ltmp575:
# BB#24:
	testl	%r15d, %r15d
	jne	.LBB39_35
# BB#25:
	movl	76(%rsp), %eax
	testl	%eax, %eax
	je	.LBB39_52
# BB#26:
	movq	%rbp, (%rsp)            # 8-byte Spill
	cmpl	$2, %eax
	jge	.LBB39_60
# BB#27:
.Ltmp576:
	movb	$1, %r15b
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp577:
# BB#28:                                # %.noexc200
	movslq	20(%r12), %rax
	testq	%rax, %rax
	je	.LBB39_30
# BB#29:
	movq	24(%r12), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%r12)
.LBB39_30:                              # %_ZN8NArchive3N7z13CStreamSwitch3SetEPNS0_10CInArchiveEPKhm.exit.i203
	movq	80(%rsp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	16(%rax), %r13
	xorl	%r15d, %r15d
.Ltmp578:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp579:
# BB#31:                                # %.noexc205
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbp)
	movq	$0, 16(%rbp)
	xorl	%r15d, %r15d
.Ltmp580:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp581:
# BB#32:
	movb	$1, %r15b
	movq	24(%r12), %rax
	movslq	20(%r12), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%r12)
	movq	24(%r12), %rax
	movq	(%rax,%rcx,8), %rdi
	movq	%rdi, 40(%r12)
	movq	%r13, (%rdi)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 8(%rdi)
	movq	$0, 16(%rdi)
.Ltmp582:
	callq	_ZN8NArchive3N7z8CInByte210ReadNumberEv
.Ltmp583:
# BB#33:                                # %_ZN8NArchive3N7z10CInArchive6ReadIDEv.exit209
	cmpq	$1, %rax
	movq	(%rsp), %rbp            # 8-byte Reload
	jne	.LBB39_62
.LBB39_34:                              # %.thread246
	movq	88(%r12), %rax
	movq	%rax, 680(%rbx)
.Ltmp589:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	_ZN8NArchive3N7z10CInArchive10ReadHeaderERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
	movl	%eax, %r15d
.Ltmp590:
.LBB39_35:                              # %.thread248
	movq	$_ZTV13CObjectVectorI7CBufferIhEE+16, 64(%rsp)
.Ltmp591:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp592:
# BB#36:
.Ltmp597:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp598:
# BB#37:
.Ltmp600:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp601:
# BB#38:                                # %.noexc217
	movslq	20(%r12), %rax
	testq	%rax, %rax
	je	.LBB39_40
# BB#39:
	movq	24(%r12), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%r12)
.LBB39_40:                              # %_ZN8NArchive3N7z13CStreamSwitchD2Ev.exit218
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB39_42
.LBB39_41:
	xorl	%r15d, %r15d
.LBB39_42:                              # %_ZN7CBufferIhED2Ev.exit187
	movl	%r15d, %eax
	addq	$568, %rsp              # imm = 0x238
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB39_43:
	movl	$1, (%rsp)              # 4-byte Folded Spill
	movl	%ebp, %eax
	addl	$-2, %eax
	js	.LBB39_51
# BB#44:                                # %.lr.ph.preheader
	movq	56(%rsp), %rax          # 8-byte Reload
	decq	%rax
	subq	48(%rsp), %rax          # 8-byte Folded Reload
	cmpq	$-501, %rax             # imm = 0xFE0B
	movq	$-501, %rcx             # imm = 0xFE0B
	cmovaq	%rax, %rcx
	movq	$-3, %rax
	subq	%rcx, %rax
.LBB39_45:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	64(%rsp,%rax), %ecx
	cmpb	$1, %cl
	je	.LBB39_48
# BB#46:                                # %.lr.ph
                                        #   in Loop: Header=BB39_45 Depth=1
	cmpb	$23, %cl
	jne	.LBB39_49
# BB#47:                                #   in Loop: Header=BB39_45 Depth=1
	cmpb	$6, 65(%rsp,%rax)
	jne	.LBB39_49
	jmp	.LBB39_54
.LBB39_48:                              #   in Loop: Header=BB39_45 Depth=1
	cmpb	$4, 65(%rsp,%rax)
	je	.LBB39_53
.LBB39_49:                              # %thread-pre-split.thread
                                        #   in Loop: Header=BB39_45 Depth=1
	testq	%rax, %rax
	leaq	-1(%rax), %rax
	jg	.LBB39_45
.LBB39_51:
	movl	$1, %r15d
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB39_42
	jmp	.LBB39_9
.LBB39_52:
	xorl	%r15d, %r15d
	jmp	.LBB39_35
.LBB39_53:
	testl	%eax, %eax
	movl	$1, %r15d
	js	.LBB39_7
.LBB39_54:                              # %.thread245
	subl	%eax, %ebp
	movslq	%ebp, %r13
	movslq	%eax, %r14
	leaq	64(%rsp,%r14), %rdi
	addq	16(%rsp), %r14
	subq	24(%rsp), %r14
	movq	%r13, %rsi
	callq	CrcCalc
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	24(%rsp), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %r15d
	xorl	%eax, %eax
	testl	%r15d, %r15d
	setne	%al
	movl	%eax, (%rsp)            # 4-byte Spill
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB39_42
	jmp	.LBB39_9
.LBB39_55:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.LBB39_56:
.Ltmp563:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp564:
# BB#57:                                # %.noexc189
.LBB39_58:
.Ltmp603:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp604:
# BB#59:                                # %.noexc196
.LBB39_60:
	movb	$1, %r15b
.Ltmp586:
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp587:
# BB#61:                                # %.noexc198
.LBB39_62:
.Ltmp584:
	movb	$1, %r15b
	callq	_ZN8NArchive3N7zL14ThrowExceptionEv
.Ltmp585:
# BB#63:                                # %.noexc210
.LBB39_64:
.Ltmp588:
	movq	%rax, %rbx
	jmp	.LBB39_72
.LBB39_65:
.Ltmp602:
	jmp	.LBB39_85
.LBB39_66:
.Ltmp599:
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
	jmp	.LBB39_75
.LBB39_67:
.Ltmp593:
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
.Ltmp594:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp595:
	jmp	.LBB39_75
.LBB39_68:
.Ltmp596:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB39_69:
.Ltmp573:
	jmp	.LBB39_71
.LBB39_70:
.Ltmp605:
.LBB39_71:
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
	movb	$1, %r15b
.LBB39_72:
	movq	$_ZTV13CObjectVectorI7CBufferIhEE+16, 64(%rsp)
.Ltmp606:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp607:
# BB#73:
.Ltmp612:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp613:
# BB#74:
	testb	%r15b, %r15b
	je	.LBB39_86
.LBB39_75:
.Ltmp614:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
.Ltmp615:
# BB#76:                                # %.noexc188
	movslq	20(%r12), %rax
	testq	%rax, %rax
	je	.LBB39_86
# BB#77:
	movq	24(%r12), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	%rax, 40(%r12)
	jmp	.LBB39_86
.LBB39_78:
.Ltmp608:
	movq	%rax, %rbx
.Ltmp609:
	leaq	64(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp610:
	jmp	.LBB39_81
.LBB39_79:
.Ltmp611:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB39_80:
.Ltmp616:
	movq	%rax, %rbx
.LBB39_81:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.LBB39_82:                              # %.thread250
.Ltmp570:
	jmp	.LBB39_85
.LBB39_83:                              # %_ZN8NArchive3N7z13CStreamSwitchD2Ev.exit
.Ltmp558:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB39_84:                              # %_ZN8NArchive3N7z13CStreamSwitchD2Ev.exit.thread273
.Ltmp565:
.LBB39_85:                              # %_ZN8NArchive3N7z13CStreamSwitchD2Ev.exit.thread
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%rax, %rbx
.LBB39_86:                              # %_ZN8NArchive3N7z13CStreamSwitchD2Ev.exit.thread
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end39:
	.size	_ZN8NArchive3N7z10CInArchive13ReadDatabase2ERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb, .Lfunc_end39-_ZN8NArchive3N7z10CInArchive13ReadDatabase2ERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\215\202\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\204\002"              # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp556-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp556
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp556-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp557-.Ltmp556       #   Call between .Ltmp556 and .Ltmp557
	.long	.Ltmp558-.Lfunc_begin11 #     jumps to .Ltmp558
	.byte	0                       #   On action: cleanup
	.long	.Ltmp559-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp562-.Ltmp559       #   Call between .Ltmp559 and .Ltmp562
	.long	.Ltmp565-.Lfunc_begin11 #     jumps to .Ltmp565
	.byte	0                       #   On action: cleanup
	.long	.Ltmp566-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp569-.Ltmp566       #   Call between .Ltmp566 and .Ltmp569
	.long	.Ltmp570-.Lfunc_begin11 #     jumps to .Ltmp570
	.byte	0                       #   On action: cleanup
	.long	.Ltmp571-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp572-.Ltmp571       #   Call between .Ltmp571 and .Ltmp572
	.long	.Ltmp573-.Lfunc_begin11 #     jumps to .Ltmp573
	.byte	0                       #   On action: cleanup
	.long	.Ltmp574-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Ltmp575-.Ltmp574       #   Call between .Ltmp574 and .Ltmp575
	.long	.Ltmp605-.Lfunc_begin11 #     jumps to .Ltmp605
	.byte	0                       #   On action: cleanup
	.long	.Ltmp576-.Lfunc_begin11 # >> Call Site 7 <<
	.long	.Ltmp583-.Ltmp576       #   Call between .Ltmp576 and .Ltmp583
	.long	.Ltmp588-.Lfunc_begin11 #     jumps to .Ltmp588
	.byte	0                       #   On action: cleanup
	.long	.Ltmp589-.Lfunc_begin11 # >> Call Site 8 <<
	.long	.Ltmp590-.Ltmp589       #   Call between .Ltmp589 and .Ltmp590
	.long	.Ltmp605-.Lfunc_begin11 #     jumps to .Ltmp605
	.byte	0                       #   On action: cleanup
	.long	.Ltmp591-.Lfunc_begin11 # >> Call Site 9 <<
	.long	.Ltmp592-.Ltmp591       #   Call between .Ltmp591 and .Ltmp592
	.long	.Ltmp593-.Lfunc_begin11 #     jumps to .Ltmp593
	.byte	0                       #   On action: cleanup
	.long	.Ltmp597-.Lfunc_begin11 # >> Call Site 10 <<
	.long	.Ltmp598-.Ltmp597       #   Call between .Ltmp597 and .Ltmp598
	.long	.Ltmp599-.Lfunc_begin11 #     jumps to .Ltmp599
	.byte	0                       #   On action: cleanup
	.long	.Ltmp600-.Lfunc_begin11 # >> Call Site 11 <<
	.long	.Ltmp601-.Ltmp600       #   Call between .Ltmp600 and .Ltmp601
	.long	.Ltmp602-.Lfunc_begin11 #     jumps to .Ltmp602
	.byte	0                       #   On action: cleanup
	.long	.Ltmp601-.Lfunc_begin11 # >> Call Site 12 <<
	.long	.Ltmp563-.Ltmp601       #   Call between .Ltmp601 and .Ltmp563
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp563-.Lfunc_begin11 # >> Call Site 13 <<
	.long	.Ltmp564-.Ltmp563       #   Call between .Ltmp563 and .Ltmp564
	.long	.Ltmp565-.Lfunc_begin11 #     jumps to .Ltmp565
	.byte	0                       #   On action: cleanup
	.long	.Ltmp603-.Lfunc_begin11 # >> Call Site 14 <<
	.long	.Ltmp604-.Ltmp603       #   Call between .Ltmp603 and .Ltmp604
	.long	.Ltmp605-.Lfunc_begin11 #     jumps to .Ltmp605
	.byte	0                       #   On action: cleanup
	.long	.Ltmp586-.Lfunc_begin11 # >> Call Site 15 <<
	.long	.Ltmp585-.Ltmp586       #   Call between .Ltmp586 and .Ltmp585
	.long	.Ltmp588-.Lfunc_begin11 #     jumps to .Ltmp588
	.byte	0                       #   On action: cleanup
	.long	.Ltmp594-.Lfunc_begin11 # >> Call Site 16 <<
	.long	.Ltmp595-.Ltmp594       #   Call between .Ltmp594 and .Ltmp595
	.long	.Ltmp596-.Lfunc_begin11 #     jumps to .Ltmp596
	.byte	1                       #   On action: 1
	.long	.Ltmp606-.Lfunc_begin11 # >> Call Site 17 <<
	.long	.Ltmp607-.Ltmp606       #   Call between .Ltmp606 and .Ltmp607
	.long	.Ltmp608-.Lfunc_begin11 #     jumps to .Ltmp608
	.byte	1                       #   On action: 1
	.long	.Ltmp612-.Lfunc_begin11 # >> Call Site 18 <<
	.long	.Ltmp615-.Ltmp612       #   Call between .Ltmp612 and .Ltmp615
	.long	.Ltmp616-.Lfunc_begin11 #     jumps to .Ltmp616
	.byte	1                       #   On action: 1
	.long	.Ltmp609-.Lfunc_begin11 # >> Call Site 19 <<
	.long	.Ltmp610-.Ltmp609       #   Call between .Ltmp609 and .Ltmp610
	.long	.Ltmp611-.Lfunc_begin11 #     jumps to .Ltmp611
	.byte	1                       #   On action: 1
	.long	.Ltmp610-.Lfunc_begin11 # >> Call Site 20 <<
	.long	.Lfunc_end39-.Ltmp610   #   Call between .Ltmp610 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive3N7z10CInArchive12ReadDatabaseERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z10CInArchive12ReadDatabaseERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb,@function
_ZN8NArchive3N7z10CInArchive12ReadDatabaseERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb: # @_ZN8NArchive3N7z10CInArchive12ReadDatabaseERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%rax
.Lcfi468:
	.cfi_def_cfa_offset 16
.Ltmp617:
	callq	_ZN8NArchive3N7z10CInArchive13ReadDatabase2ERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
.Ltmp618:
# BB#2:
	popq	%rcx
	retq
.LBB40_1:
.Ltmp619:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	callq	__cxa_end_catch
	movl	$1, %eax
	popq	%rcx
	retq
.Lfunc_end40:
	.size	_ZN8NArchive3N7z10CInArchive12ReadDatabaseERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb, .Lfunc_end40-_ZN8NArchive3N7z10CInArchive12ReadDatabaseERNS0_18CArchiveDatabaseExEP22ICryptoGetTextPasswordRb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\242\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp617-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp618-.Ltmp617       #   Call between .Ltmp617 and .Ltmp618
	.long	.Ltmp619-.Lfunc_begin12 #     jumps to .Ltmp619
	.byte	1                       #   On action: 1
	.long	.Ltmp618-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Lfunc_end40-.Ltmp618   #   Call between .Ltmp618 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	_ZTIN8NArchive3N7z19CInArchiveExceptionE # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7zL14ThrowExceptionEv,@function
_ZN8NArchive3N7zL14ThrowExceptionEv:    # @_ZN8NArchive3N7zL14ThrowExceptionEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi469:
	.cfi_def_cfa_offset 16
	movl	$1, %edi
	callq	__cxa_allocate_exception
	movl	$_ZTIN8NArchive3N7z19CInArchiveExceptionE, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end41:
	.size	_ZN8NArchive3N7zL14ThrowExceptionEv, .Lfunc_end41-_ZN8NArchive3N7zL14ThrowExceptionEv
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi470:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi471:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi472:
	.cfi_def_cfa_offset 32
.Lcfi473:
	.cfi_offset %rbx, -24
.Lcfi474:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp620:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp621:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB42_2:
.Ltmp622:
	movq	%rax, %r14
.Ltmp623:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp624:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB42_4:
.Ltmp625:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end42:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev, .Lfunc_end42-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp620-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp621-.Ltmp620       #   Call between .Ltmp620 and .Ltmp621
	.long	.Ltmp622-.Lfunc_begin13 #     jumps to .Ltmp622
	.byte	0                       #   On action: cleanup
	.long	.Ltmp621-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp623-.Ltmp621       #   Call between .Ltmp621 and .Ltmp623
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp623-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp624-.Ltmp623       #   Call between .Ltmp623 and .Ltmp624
	.long	.Ltmp625-.Lfunc_begin13 #     jumps to .Ltmp625
	.byte	1                       #   On action: 1
	.long	.Ltmp624-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Lfunc_end42-.Ltmp624   #   Call between .Ltmp624 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi475:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi476:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi477:
	.cfi_def_cfa_offset 32
.Lcfi478:
	.cfi_offset %rbx, -24
.Lcfi479:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbx)
.Ltmp626:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp627:
# BB#1:
.Ltmp632:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp633:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_5:
.Ltmp634:
	movq	%rax, %r14
	jmp	.LBB43_6
.LBB43_3:
.Ltmp628:
	movq	%rax, %r14
.Ltmp629:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp630:
.LBB43_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB43_4:
.Ltmp631:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end43:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev, .Lfunc_end43-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp626-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp627-.Ltmp626       #   Call between .Ltmp626 and .Ltmp627
	.long	.Ltmp628-.Lfunc_begin14 #     jumps to .Ltmp628
	.byte	0                       #   On action: cleanup
	.long	.Ltmp632-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp633-.Ltmp632       #   Call between .Ltmp632 and .Ltmp633
	.long	.Ltmp634-.Lfunc_begin14 #     jumps to .Ltmp634
	.byte	0                       #   On action: cleanup
	.long	.Ltmp629-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp630-.Ltmp629       #   Call between .Ltmp629 and .Ltmp630
	.long	.Ltmp631-.Lfunc_begin14 #     jumps to .Ltmp631
	.byte	1                       #   On action: 1
	.long	.Ltmp630-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end43-.Ltmp630   #   Call between .Ltmp630 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi480:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi481:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi482:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi483:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi484:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi485:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi486:
	.cfi_def_cfa_offset 64
.Lcfi487:
	.cfi_offset %rbx, -56
.Lcfi488:
	.cfi_offset %r12, -48
.Lcfi489:
	.cfi_offset %r13, -40
.Lcfi490:
	.cfi_offset %r14, -32
.Lcfi491:
	.cfi_offset %r15, -24
.Lcfi492:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB44_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB44_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB44_6
# BB#3:                                 #   in Loop: Header=BB44_2 Depth=1
	movq	$_ZTV7CBufferIhE+16, 8(%rbp)
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB44_5
# BB#4:                                 #   in Loop: Header=BB44_2 Depth=1
	callq	_ZdaPv
.LBB44_5:                               # %_ZN8NArchive3N7z10CCoderInfoD2Ev.exit
                                        #   in Loop: Header=BB44_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB44_6:                               #   in Loop: Header=BB44_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB44_2
.LBB44_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end44:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii, .Lfunc_end44-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev,@function
_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev: # @_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi493:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi494:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi495:
	.cfi_def_cfa_offset 32
.Lcfi496:
	.cfi_offset %rbx, -24
.Lcfi497:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp635:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp636:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB45_2:
.Ltmp637:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end45:
	.size	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev, .Lfunc_end45-_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp635-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp636-.Ltmp635       #   Call between .Ltmp635 and .Ltmp636
	.long	.Ltmp637-.Lfunc_begin15 #     jumps to .Ltmp637
	.byte	0                       #   On action: cleanup
	.long	.Ltmp636-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Lfunc_end45-.Ltmp636   #   Call between .Ltmp636 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi498:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi499:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi500:
	.cfi_def_cfa_offset 32
.Lcfi501:
	.cfi_offset %rbx, -24
.Lcfi502:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, (%rbx)
.Ltmp638:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp639:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB46_2:
.Ltmp640:
	movq	%rax, %r14
.Ltmp641:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp642:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB46_4:
.Ltmp643:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end46:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev, .Lfunc_end46-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table46:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp638-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp639-.Ltmp638       #   Call between .Ltmp638 and .Ltmp639
	.long	.Ltmp640-.Lfunc_begin16 #     jumps to .Ltmp640
	.byte	0                       #   On action: cleanup
	.long	.Ltmp639-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp641-.Ltmp639       #   Call between .Ltmp639 and .Ltmp641
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp641-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp642-.Ltmp641       #   Call between .Ltmp641 and .Ltmp642
	.long	.Ltmp643-.Lfunc_begin16 #     jumps to .Ltmp643
	.byte	1                       #   On action: 1
	.long	.Ltmp642-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Lfunc_end46-.Ltmp642   #   Call between .Ltmp642 and .Lfunc_end46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi503:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi504:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi505:
	.cfi_def_cfa_offset 32
.Lcfi506:
	.cfi_offset %rbx, -24
.Lcfi507:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE+16, (%rbx)
.Ltmp644:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp645:
# BB#1:
.Ltmp650:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp651:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB47_5:
.Ltmp652:
	movq	%rax, %r14
	jmp	.LBB47_6
.LBB47_3:
.Ltmp646:
	movq	%rax, %r14
.Ltmp647:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp648:
.LBB47_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB47_4:
.Ltmp649:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end47:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev, .Lfunc_end47-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table47:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp644-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp645-.Ltmp644       #   Call between .Ltmp644 and .Ltmp645
	.long	.Ltmp646-.Lfunc_begin17 #     jumps to .Ltmp646
	.byte	0                       #   On action: cleanup
	.long	.Ltmp650-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp651-.Ltmp650       #   Call between .Ltmp650 and .Ltmp651
	.long	.Ltmp652-.Lfunc_begin17 #     jumps to .Ltmp652
	.byte	0                       #   On action: cleanup
	.long	.Ltmp647-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp648-.Ltmp647       #   Call between .Ltmp647 and .Ltmp648
	.long	.Ltmp649-.Lfunc_begin17 #     jumps to .Ltmp649
	.byte	1                       #   On action: 1
	.long	.Ltmp648-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end47-.Ltmp648   #   Call between .Ltmp648 and .Lfunc_end47
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi508:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi509:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi510:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi511:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi512:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi513:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi514:
	.cfi_def_cfa_offset 64
.Lcfi515:
	.cfi_offset %rbx, -56
.Lcfi516:
	.cfi_offset %r12, -48
.Lcfi517:
	.cfi_offset %r13, -40
.Lcfi518:
	.cfi_offset %r14, -32
.Lcfi519:
	.cfi_offset %r15, -24
.Lcfi520:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB48_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB48_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB48_6
# BB#3:                                 #   in Loop: Header=BB48_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB48_5
# BB#4:                                 #   in Loop: Header=BB48_2 Depth=1
	movq	(%rdi), %rax
.Ltmp653:
	callq	*16(%rax)
.Ltmp654:
.LBB48_5:                               # %_ZN9CMyComPtrI8IUnknownED2Ev.exit
                                        #   in Loop: Header=BB48_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB48_6:                               #   in Loop: Header=BB48_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB48_2
.LBB48_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB48_8:
.Ltmp655:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end48:
	.size	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii, .Lfunc_end48-_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table48:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp653-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp654-.Ltmp653       #   Call between .Ltmp653 and .Ltmp654
	.long	.Ltmp655-.Lfunc_begin18 #     jumps to .Ltmp655
	.byte	0                       #   On action: cleanup
	.long	.Ltmp654-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Lfunc_end48-.Ltmp654   #   Call between .Ltmp654 and .Lfunc_end48
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive3N7z16CArchiveDatabase5ClearEv,"axG",@progbits,_ZN8NArchive3N7z16CArchiveDatabase5ClearEv,comdat
	.weak	_ZN8NArchive3N7z16CArchiveDatabase5ClearEv
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z16CArchiveDatabase5ClearEv,@function
_ZN8NArchive3N7z16CArchiveDatabase5ClearEv: # @_ZN8NArchive3N7z16CArchiveDatabase5ClearEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi521:
	.cfi_def_cfa_offset 16
.Lcfi522:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	32(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	64(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	96(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	%rbx, %rdi
	subq	$-128, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	160(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	192(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	224(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	256(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	288(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	320(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	352(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	384(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	leaq	416(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	addq	$448, %rbx              # imm = 0x1C0
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZN17CBaseRecordVector5ClearEv # TAILCALL
.Lfunc_end49:
	.size	_ZN8NArchive3N7z16CArchiveDatabase5ClearEv, .Lfunc_end49-_ZN8NArchive3N7z16CArchiveDatabase5ClearEv
	.cfi_endproc

	.section	.text._ZN7CBufferIhED0Ev,"axG",@progbits,_ZN7CBufferIhED0Ev,comdat
	.weak	_ZN7CBufferIhED0Ev
	.p2align	4, 0x90
	.type	_ZN7CBufferIhED0Ev,@function
_ZN7CBufferIhED0Ev:                     # @_ZN7CBufferIhED0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi523:
	.cfi_def_cfa_offset 16
.Lcfi524:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV7CBufferIhE+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB50_2
# BB#1:
	callq	_ZdaPv
.LBB50_2:                               # %_ZN7CBufferIhED2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end50:
	.size	_ZN7CBufferIhED0Ev, .Lfunc_end50-_ZN7CBufferIhED0Ev
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi525:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi526:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi527:
	.cfi_def_cfa_offset 32
.Lcfi528:
	.cfi_offset %rbx, -24
.Lcfi529:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp656:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp657:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB51_2:
.Ltmp658:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end51:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end51-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table51:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp656-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp657-.Ltmp656       #   Call between .Ltmp656 and .Ltmp657
	.long	.Ltmp658-.Lfunc_begin19 #     jumps to .Ltmp658
	.byte	0                       #   On action: cleanup
	.long	.Ltmp657-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end51-.Ltmp657   #   Call between .Ltmp657 and .Lfunc_end51
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi530:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi531:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi532:
	.cfi_def_cfa_offset 32
.Lcfi533:
	.cfi_offset %rbx, -24
.Lcfi534:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp659:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp660:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB52_2:
.Ltmp661:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end52:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end52-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table52:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp659-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp660-.Ltmp659       #   Call between .Ltmp659 and .Ltmp660
	.long	.Ltmp661-.Lfunc_begin20 #     jumps to .Ltmp661
	.byte	0                       #   On action: cleanup
	.long	.Ltmp660-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Lfunc_end52-.Ltmp660   #   Call between .Ltmp660 and .Lfunc_end52
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIbED0Ev,"axG",@progbits,_ZN13CRecordVectorIbED0Ev,comdat
	.weak	_ZN13CRecordVectorIbED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIbED0Ev,@function
_ZN13CRecordVectorIbED0Ev:              # @_ZN13CRecordVectorIbED0Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi535:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi536:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi537:
	.cfi_def_cfa_offset 32
.Lcfi538:
	.cfi_offset %rbx, -24
.Lcfi539:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp662:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp663:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB53_2:
.Ltmp664:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end53:
	.size	_ZN13CRecordVectorIbED0Ev, .Lfunc_end53-_ZN13CRecordVectorIbED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table53:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp662-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp663-.Ltmp662       #   Call between .Ltmp662 and .Ltmp663
	.long	.Ltmp664-.Lfunc_begin21 #     jumps to .Ltmp664
	.byte	0                       #   On action: cleanup
	.long	.Ltmp663-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Lfunc_end53-.Ltmp663   #   Call between .Ltmp663 and .Lfunc_end53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi540:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi541:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi542:
	.cfi_def_cfa_offset 32
.Lcfi543:
	.cfi_offset %rbx, -24
.Lcfi544:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp665:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp666:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB54_2:
.Ltmp667:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end54:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end54-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table54:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp665-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp666-.Ltmp665       #   Call between .Ltmp665 and .Ltmp666
	.long	.Ltmp667-.Lfunc_begin22 #     jumps to .Ltmp667
	.byte	0                       #   On action: cleanup
	.long	.Ltmp666-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Lfunc_end54-.Ltmp666   #   Call between .Ltmp666 and .Lfunc_end54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive3N7z7CFolderC2ERKS1_,"axG",@progbits,_ZN8NArchive3N7z7CFolderC2ERKS1_,comdat
	.weak	_ZN8NArchive3N7z7CFolderC2ERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive3N7z7CFolderC2ERKS1_,@function
_ZN8NArchive3N7z7CFolderC2ERKS1_:       # @_ZN8NArchive3N7z7CFolderC2ERKS1_
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r15
.Lcfi545:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi546:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi547:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi548:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi549:
	.cfi_def_cfa_offset 48
.Lcfi550:
	.cfi_offset %rbx, -48
.Lcfi551:
	.cfi_offset %r12, -40
.Lcfi552:
	.cfi_offset %r13, -32
.Lcfi553:
	.cfi_offset %r14, -24
.Lcfi554:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	callq	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
	leaq	32(%r12), %r14
	leaq	32(%rbx), %rsi
.Ltmp668:
	movq	%r14, %rdi
	callq	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
.Ltmp669:
# BB#1:
	leaq	64(%r12), %r13
	leaq	64(%rbx), %rsi
.Ltmp671:
	movq	%r13, %rdi
	callq	_ZN13CRecordVectorIjEC2ERKS0_
.Ltmp672:
# BB#2:
	leaq	96(%r12), %rdi
	leaq	96(%rbx), %rsi
.Ltmp674:
	callq	_ZN13CRecordVectorIyEC2ERKS0_
.Ltmp675:
# BB#3:
	movb	132(%rbx), %al
	movb	%al, 132(%r12)
	movl	128(%rbx), %eax
	movl	%eax, 128(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB55_6:
.Ltmp676:
	movq	%rax, %r15
.Ltmp677:
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp678:
	jmp	.LBB55_7
.LBB55_5:
.Ltmp673:
	movq	%rax, %r15
.LBB55_7:
.Ltmp679:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp680:
	jmp	.LBB55_8
.LBB55_4:
.Ltmp670:
	movq	%rax, %r15
.LBB55_8:
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%r12)
.Ltmp681:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp682:
# BB#9:
.Ltmp687:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp688:
# BB#10:                                # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB55_11:
.Ltmp683:
	movq	%rax, %rbx
.Ltmp684:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp685:
	jmp	.LBB55_14
.LBB55_12:
.Ltmp686:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB55_13:
.Ltmp689:
	movq	%rax, %rbx
.LBB55_14:                              # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end55:
	.size	_ZN8NArchive3N7z7CFolderC2ERKS1_, .Lfunc_end55-_ZN8NArchive3N7z7CFolderC2ERKS1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table55:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin23-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp668-.Lfunc_begin23 #   Call between .Lfunc_begin23 and .Ltmp668
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp668-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Ltmp669-.Ltmp668       #   Call between .Ltmp668 and .Ltmp669
	.long	.Ltmp670-.Lfunc_begin23 #     jumps to .Ltmp670
	.byte	0                       #   On action: cleanup
	.long	.Ltmp671-.Lfunc_begin23 # >> Call Site 3 <<
	.long	.Ltmp672-.Ltmp671       #   Call between .Ltmp671 and .Ltmp672
	.long	.Ltmp673-.Lfunc_begin23 #     jumps to .Ltmp673
	.byte	0                       #   On action: cleanup
	.long	.Ltmp674-.Lfunc_begin23 # >> Call Site 4 <<
	.long	.Ltmp675-.Ltmp674       #   Call between .Ltmp674 and .Ltmp675
	.long	.Ltmp676-.Lfunc_begin23 #     jumps to .Ltmp676
	.byte	0                       #   On action: cleanup
	.long	.Ltmp677-.Lfunc_begin23 # >> Call Site 5 <<
	.long	.Ltmp680-.Ltmp677       #   Call between .Ltmp677 and .Ltmp680
	.long	.Ltmp689-.Lfunc_begin23 #     jumps to .Ltmp689
	.byte	1                       #   On action: 1
	.long	.Ltmp681-.Lfunc_begin23 # >> Call Site 6 <<
	.long	.Ltmp682-.Ltmp681       #   Call between .Ltmp681 and .Ltmp682
	.long	.Ltmp683-.Lfunc_begin23 #     jumps to .Ltmp683
	.byte	1                       #   On action: 1
	.long	.Ltmp687-.Lfunc_begin23 # >> Call Site 7 <<
	.long	.Ltmp688-.Ltmp687       #   Call between .Ltmp687 and .Ltmp688
	.long	.Ltmp689-.Lfunc_begin23 #     jumps to .Ltmp689
	.byte	1                       #   On action: 1
	.long	.Ltmp688-.Lfunc_begin23 # >> Call Site 8 <<
	.long	.Ltmp684-.Ltmp688       #   Call between .Ltmp688 and .Ltmp684
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp684-.Lfunc_begin23 # >> Call Site 9 <<
	.long	.Ltmp685-.Ltmp684       #   Call between .Ltmp684 and .Ltmp685
	.long	.Ltmp686-.Lfunc_begin23 #     jumps to .Ltmp686
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI56_0:
	.zero	16
	.section	.text._ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_,@function
_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_: # @_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%rbp
.Lcfi555:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi556:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi557:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi558:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi559:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi560:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi561:
	.cfi_def_cfa_offset 80
.Lcfi562:
	.cfi_offset %rbx, -56
.Lcfi563:
	.cfi_offset %r12, -48
.Lcfi564:
	.cfi_offset %r13, -40
.Lcfi565:
	.cfi_offset %r14, -32
.Lcfi566:
	.cfi_offset %r15, -24
.Lcfi567:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbp)
	movq	$8, 24(%rbp)
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE+16, (%rbp)
.Ltmp690:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp691:
# BB#1:                                 # %.noexc
	movl	12(%rbx), %r15d
	movl	12(%rbp), %esi
	addl	%r15d, %esi
.Ltmp692:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp693:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB56_10
# BB#3:                                 # %.lr.ph.i.i
	xorl	%r13d, %r13d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB56_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movq	(%rax,%r13,8), %r14
.Ltmp695:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp696:
# BB#5:                                 # %.noexc5
                                        #   in Loop: Header=BB56_4 Depth=1
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movq	$_ZTV7CBufferIhE+16, 8(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.LBB56_8
# BB#6:                                 # %_ZN7CBufferIhE11SetCapacityEm.exit.i.i.i.i
                                        #   in Loop: Header=BB56_4 Depth=1
.Ltmp697:
	movq	%r12, %rdi
	callq	_Znam
.Ltmp698:
# BB#7:                                 # %.noexc.i
                                        #   in Loop: Header=BB56_4 Depth=1
	movq	%rax, 24(%rbx)
	movq	%r12, 16(%rbx)
	movq	24(%r14), %rsi
	movq	%rax, %rdi
	movq	%r12, %rdx
	callq	memmove
.LBB56_8:                               #   in Loop: Header=BB56_4 Depth=1
	movq	32(%r14), %rax
	movq	%rax, 32(%rbx)
.Ltmp700:
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp701:
# BB#9:                                 # %.noexc4
                                        #   in Loop: Header=BB56_4 Depth=1
	movq	16(%rbp), %rax
	movslq	12(%rbp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rbp)
	incq	%r13
	cmpq	%r13, %r15
	movq	16(%rsp), %rbx          # 8-byte Reload
	jne	.LBB56_4
.LBB56_10:                              # %_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEaSERKS3_.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB56_12:                              # %.loopexit.split-lp
.Ltmp694:
	jmp	.LBB56_13
.LBB56_17:
.Ltmp699:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB56_14
.LBB56_11:                              # %.loopexit
.Ltmp702:
.LBB56_13:                              # %.body
	movq	%rax, %r14
.LBB56_14:                              # %.body
.Ltmp703:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp704:
# BB#15:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB56_16:
.Ltmp705:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end56:
	.size	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_, .Lfunc_end56-_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEEC2ERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table56:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp690-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp693-.Ltmp690       #   Call between .Ltmp690 and .Ltmp693
	.long	.Ltmp694-.Lfunc_begin24 #     jumps to .Ltmp694
	.byte	0                       #   On action: cleanup
	.long	.Ltmp695-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp696-.Ltmp695       #   Call between .Ltmp695 and .Ltmp696
	.long	.Ltmp702-.Lfunc_begin24 #     jumps to .Ltmp702
	.byte	0                       #   On action: cleanup
	.long	.Ltmp697-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp698-.Ltmp697       #   Call between .Ltmp697 and .Ltmp698
	.long	.Ltmp699-.Lfunc_begin24 #     jumps to .Ltmp699
	.byte	0                       #   On action: cleanup
	.long	.Ltmp698-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Ltmp700-.Ltmp698       #   Call between .Ltmp698 and .Ltmp700
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp700-.Lfunc_begin24 # >> Call Site 5 <<
	.long	.Ltmp701-.Ltmp700       #   Call between .Ltmp700 and .Ltmp701
	.long	.Ltmp702-.Lfunc_begin24 #     jumps to .Ltmp702
	.byte	0                       #   On action: cleanup
	.long	.Ltmp703-.Lfunc_begin24 # >> Call Site 6 <<
	.long	.Ltmp704-.Ltmp703       #   Call between .Ltmp703 and .Ltmp704
	.long	.Ltmp705-.Lfunc_begin24 #     jumps to .Ltmp705
	.byte	1                       #   On action: 1
	.long	.Ltmp704-.Lfunc_begin24 # >> Call Site 7 <<
	.long	.Lfunc_end56-.Ltmp704   #   Call between .Ltmp704 and .Lfunc_end56
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_,"axG",@progbits,_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_,comdat
	.weak	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_,@function
_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_: # @_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r15
.Lcfi568:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi569:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi570:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi571:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi572:
	.cfi_def_cfa_offset 48
.Lcfi573:
	.cfi_offset %rbx, -48
.Lcfi574:
	.cfi_offset %r12, -40
.Lcfi575:
	.cfi_offset %r13, -32
.Lcfi576:
	.cfi_offset %r14, -24
.Lcfi577:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE+16, (%r12)
.Ltmp706:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp707:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp708:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp709:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB57_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB57_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp711:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp712:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB57_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB57_4
.LBB57_6:                               # %_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEaSERKS3_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB57_8:                               # %.loopexit.split-lp
.Ltmp710:
	jmp	.LBB57_9
.LBB57_7:                               # %.loopexit
.Ltmp713:
.LBB57_9:
	movq	%rax, %r14
.Ltmp714:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp715:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB57_11:
.Ltmp716:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end57:
	.size	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_, .Lfunc_end57-_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEEC2ERKS3_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table57:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp706-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp709-.Ltmp706       #   Call between .Ltmp706 and .Ltmp709
	.long	.Ltmp710-.Lfunc_begin25 #     jumps to .Ltmp710
	.byte	0                       #   On action: cleanup
	.long	.Ltmp711-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp712-.Ltmp711       #   Call between .Ltmp711 and .Ltmp712
	.long	.Ltmp713-.Lfunc_begin25 #     jumps to .Ltmp713
	.byte	0                       #   On action: cleanup
	.long	.Ltmp714-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp715-.Ltmp714       #   Call between .Ltmp714 and .Ltmp715
	.long	.Ltmp716-.Lfunc_begin25 #     jumps to .Ltmp716
	.byte	1                       #   On action: 1
	.long	.Ltmp715-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Lfunc_end57-.Ltmp715   #   Call between .Ltmp715 and .Lfunc_end57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIjEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIjEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIjEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjEC2ERKS0_,@function
_ZN13CRecordVectorIjEC2ERKS0_:          # @_ZN13CRecordVectorIjEC2ERKS0_
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:
	pushq	%rbp
.Lcfi578:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi579:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi580:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi581:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi582:
	.cfi_def_cfa_offset 48
.Lcfi583:
	.cfi_offset %rbx, -48
.Lcfi584:
	.cfi_offset %r12, -40
.Lcfi585:
	.cfi_offset %r14, -32
.Lcfi586:
	.cfi_offset %r15, -24
.Lcfi587:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$4, 24(%r12)
	movq	$_ZTV13CRecordVectorIjE+16, (%r12)
.Ltmp717:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp718:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp719:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp720:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB58_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB58_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movl	(%rax,%rbx,4), %ebp
.Ltmp722:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp723:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB58_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movl	%ebp, (%rax,%rcx,4)
	incl	12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB58_4
.LBB58_6:                               # %_ZN13CRecordVectorIjEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB58_8:                               # %.loopexit.split-lp
.Ltmp721:
	jmp	.LBB58_9
.LBB58_7:                               # %.loopexit
.Ltmp724:
.LBB58_9:
	movq	%rax, %r14
.Ltmp725:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp726:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB58_11:
.Ltmp727:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end58:
	.size	_ZN13CRecordVectorIjEC2ERKS0_, .Lfunc_end58-_ZN13CRecordVectorIjEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table58:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp717-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp720-.Ltmp717       #   Call between .Ltmp717 and .Ltmp720
	.long	.Ltmp721-.Lfunc_begin26 #     jumps to .Ltmp721
	.byte	0                       #   On action: cleanup
	.long	.Ltmp722-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Ltmp723-.Ltmp722       #   Call between .Ltmp722 and .Ltmp723
	.long	.Ltmp724-.Lfunc_begin26 #     jumps to .Ltmp724
	.byte	0                       #   On action: cleanup
	.long	.Ltmp725-.Lfunc_begin26 # >> Call Site 3 <<
	.long	.Ltmp726-.Ltmp725       #   Call between .Ltmp725 and .Ltmp726
	.long	.Ltmp727-.Lfunc_begin26 #     jumps to .Ltmp727
	.byte	1                       #   On action: 1
	.long	.Ltmp726-.Lfunc_begin26 # >> Call Site 4 <<
	.long	.Lfunc_end58-.Ltmp726   #   Call between .Ltmp726 and .Lfunc_end58
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CRecordVectorIyEC2ERKS0_,"axG",@progbits,_ZN13CRecordVectorIyEC2ERKS0_,comdat
	.weak	_ZN13CRecordVectorIyEC2ERKS0_
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyEC2ERKS0_,@function
_ZN13CRecordVectorIyEC2ERKS0_:          # @_ZN13CRecordVectorIyEC2ERKS0_
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%r15
.Lcfi588:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi589:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi590:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi591:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi592:
	.cfi_def_cfa_offset 48
.Lcfi593:
	.cfi_offset %rbx, -48
.Lcfi594:
	.cfi_offset %r12, -40
.Lcfi595:
	.cfi_offset %r13, -32
.Lcfi596:
	.cfi_offset %r14, -24
.Lcfi597:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	movq	$8, 24(%r12)
	movq	$_ZTV13CRecordVectorIyE+16, (%r12)
.Ltmp728:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp729:
# BB#1:                                 # %.noexc
	movl	12(%r14), %r15d
	movl	12(%r12), %esi
	addl	%r15d, %esi
.Ltmp730:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp731:
# BB#2:                                 # %.noexc3
	testl	%r15d, %r15d
	jle	.LBB59_6
# BB#3:                                 # %.lr.ph.i.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB59_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rax
	movq	(%rax,%rbx,8), %r13
.Ltmp733:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp734:
# BB#5:                                 # %.noexc4
                                        #   in Loop: Header=BB59_4 Depth=1
	movq	16(%r12), %rax
	movslq	12(%r12), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r12)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB59_4
.LBB59_6:                               # %_ZN13CRecordVectorIyEaSERKS0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB59_8:                               # %.loopexit.split-lp
.Ltmp732:
	jmp	.LBB59_9
.LBB59_7:                               # %.loopexit
.Ltmp735:
.LBB59_9:
	movq	%rax, %r14
.Ltmp736:
	movq	%r12, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp737:
# BB#10:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB59_11:
.Ltmp738:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end59:
	.size	_ZN13CRecordVectorIyEC2ERKS0_, .Lfunc_end59-_ZN13CRecordVectorIyEC2ERKS0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table59:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp728-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp731-.Ltmp728       #   Call between .Ltmp728 and .Ltmp731
	.long	.Ltmp732-.Lfunc_begin27 #     jumps to .Ltmp732
	.byte	0                       #   On action: cleanup
	.long	.Ltmp733-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Ltmp734-.Ltmp733       #   Call between .Ltmp733 and .Ltmp734
	.long	.Ltmp735-.Lfunc_begin27 #     jumps to .Ltmp735
	.byte	0                       #   On action: cleanup
	.long	.Ltmp736-.Lfunc_begin27 # >> Call Site 3 <<
	.long	.Ltmp737-.Ltmp736       #   Call between .Ltmp736 and .Ltmp737
	.long	.Ltmp738-.Lfunc_begin27 #     jumps to .Ltmp738
	.byte	1                       #   On action: 1
	.long	.Ltmp737-.Lfunc_begin27 # >> Call Site 4 <<
	.long	.Lfunc_end59-.Ltmp737   #   Call between .Ltmp737 and .Lfunc_end59
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:
	pushq	%r14
.Lcfi598:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi599:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi600:
	.cfi_def_cfa_offset 32
.Lcfi601:
	.cfi_offset %rbx, -24
.Lcfi602:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE+16, (%rbx)
.Ltmp739:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp740:
# BB#1:
.Ltmp745:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp746:
# BB#2:                                 # %_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB60_5:
.Ltmp747:
	movq	%rax, %r14
	jmp	.LBB60_6
.LBB60_3:
.Ltmp741:
	movq	%rax, %r14
.Ltmp742:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp743:
.LBB60_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB60_4:
.Ltmp744:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end60:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev, .Lfunc_end60-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table60:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp739-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp740-.Ltmp739       #   Call between .Ltmp739 and .Ltmp740
	.long	.Ltmp741-.Lfunc_begin28 #     jumps to .Ltmp741
	.byte	0                       #   On action: cleanup
	.long	.Ltmp745-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Ltmp746-.Ltmp745       #   Call between .Ltmp745 and .Ltmp746
	.long	.Ltmp747-.Lfunc_begin28 #     jumps to .Ltmp747
	.byte	0                       #   On action: cleanup
	.long	.Ltmp742-.Lfunc_begin28 # >> Call Site 3 <<
	.long	.Ltmp743-.Ltmp742       #   Call between .Ltmp742 and .Ltmp743
	.long	.Ltmp744-.Lfunc_begin28 #     jumps to .Ltmp744
	.byte	1                       #   On action: 1
	.long	.Ltmp743-.Lfunc_begin28 # >> Call Site 4 <<
	.long	.Lfunc_end60-.Ltmp743   #   Call between .Ltmp743 and .Lfunc_end60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii,@function
_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii: # @_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi603:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi604:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi605:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi606:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi607:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi608:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi609:
	.cfi_def_cfa_offset 64
.Lcfi610:
	.cfi_offset %rbx, -56
.Lcfi611:
	.cfi_offset %r12, -48
.Lcfi612:
	.cfi_offset %r13, -40
.Lcfi613:
	.cfi_offset %r14, -32
.Lcfi614:
	.cfi_offset %r15, -24
.Lcfi615:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB61_6
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB61_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB61_5
# BB#3:                                 #   in Loop: Header=BB61_2 Depth=1
.Ltmp748:
	movq	%rbp, %rdi
	callq	_ZN8NArchive3N7z7CFolderD2Ev
.Ltmp749:
# BB#4:                                 #   in Loop: Header=BB61_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB61_5:                               #   in Loop: Header=BB61_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB61_2
.LBB61_6:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB61_7:
.Ltmp750:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end61:
	.size	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii, .Lfunc_end61-_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table61:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp748-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp749-.Ltmp748       #   Call between .Ltmp748 and .Ltmp749
	.long	.Ltmp750-.Lfunc_begin29 #     jumps to .Ltmp750
	.byte	0                       #   On action: cleanup
	.long	.Ltmp749-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Lfunc_end61-.Ltmp749   #   Call between .Ltmp749 and .Lfunc_end61
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI7CBufferIhEED0Ev,"axG",@progbits,_ZN13CObjectVectorI7CBufferIhEED0Ev,comdat
	.weak	_ZN13CObjectVectorI7CBufferIhEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI7CBufferIhEED0Ev,@function
_ZN13CObjectVectorI7CBufferIhEED0Ev:    # @_ZN13CObjectVectorI7CBufferIhEED0Ev
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:
	pushq	%r14
.Lcfi616:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi617:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi618:
	.cfi_def_cfa_offset 32
.Lcfi619:
	.cfi_offset %rbx, -24
.Lcfi620:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI7CBufferIhEE+16, (%rbx)
.Ltmp751:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp752:
# BB#1:
.Ltmp757:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp758:
# BB#2:                                 # %_ZN13CObjectVectorI7CBufferIhEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB62_5:
.Ltmp759:
	movq	%rax, %r14
	jmp	.LBB62_6
.LBB62_3:
.Ltmp753:
	movq	%rax, %r14
.Ltmp754:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp755:
.LBB62_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB62_4:
.Ltmp756:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end62:
	.size	_ZN13CObjectVectorI7CBufferIhEED0Ev, .Lfunc_end62-_ZN13CObjectVectorI7CBufferIhEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table62:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp751-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp752-.Ltmp751       #   Call between .Ltmp751 and .Ltmp752
	.long	.Ltmp753-.Lfunc_begin30 #     jumps to .Ltmp753
	.byte	0                       #   On action: cleanup
	.long	.Ltmp757-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Ltmp758-.Ltmp757       #   Call between .Ltmp757 and .Ltmp758
	.long	.Ltmp759-.Lfunc_begin30 #     jumps to .Ltmp759
	.byte	0                       #   On action: cleanup
	.long	.Ltmp754-.Lfunc_begin30 # >> Call Site 3 <<
	.long	.Ltmp755-.Ltmp754       #   Call between .Ltmp754 and .Ltmp755
	.long	.Ltmp756-.Lfunc_begin30 #     jumps to .Ltmp756
	.byte	1                       #   On action: 1
	.long	.Ltmp755-.Lfunc_begin30 # >> Call Site 4 <<
	.long	.Lfunc_end62-.Ltmp755   #   Call between .Ltmp755 and .Lfunc_end62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI7CBufferIhEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI7CBufferIhEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI7CBufferIhEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI7CBufferIhEE6DeleteEii,@function
_ZN13CObjectVectorI7CBufferIhEE6DeleteEii: # @_ZN13CObjectVectorI7CBufferIhEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi621:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi622:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi623:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi624:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi625:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi626:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi627:
	.cfi_def_cfa_offset 64
.Lcfi628:
	.cfi_offset %rbx, -56
.Lcfi629:
	.cfi_offset %r12, -48
.Lcfi630:
	.cfi_offset %r13, -40
.Lcfi631:
	.cfi_offset %r14, -32
.Lcfi632:
	.cfi_offset %r15, -24
.Lcfi633:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r14d
	movq	%rdi, %r12
	leal	(%rdx,%r14), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	subl	%r14d, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB63_5
# BB#1:                                 # %.lr.ph
	movslq	%r14d, %rbp
	movslq	%r15d, %r13
	shlq	$3, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB63_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbp, %rax
	movq	(%rax,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB63_4
# BB#3:                                 #   in Loop: Header=BB63_2 Depth=1
	movq	(%rdi), %rax
	callq	*8(%rax)
.LBB63_4:                               #   in Loop: Header=BB63_2 Depth=1
	incq	%rbx
	cmpq	%r13, %rbx
	jl	.LBB63_2
.LBB63_5:                               # %._crit_edge
	movq	%r12, %rdi
	movl	%r14d, %esi
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end63:
	.size	_ZN13CObjectVectorI7CBufferIhEE6DeleteEii, .Lfunc_end63-_ZN13CObjectVectorI7CBufferIhEE6DeleteEii
	.cfi_endproc

	.type	_ZTSN8NArchive3N7z19CInArchiveExceptionE,@object # @_ZTSN8NArchive3N7z19CInArchiveExceptionE
	.section	.rodata._ZTSN8NArchive3N7z19CInArchiveExceptionE,"aG",@progbits,_ZTSN8NArchive3N7z19CInArchiveExceptionE,comdat
	.weak	_ZTSN8NArchive3N7z19CInArchiveExceptionE
	.p2align	4
_ZTSN8NArchive3N7z19CInArchiveExceptionE:
	.asciz	"N8NArchive3N7z19CInArchiveExceptionE"
	.size	_ZTSN8NArchive3N7z19CInArchiveExceptionE, 37

	.type	_ZTIN8NArchive3N7z19CInArchiveExceptionE,@object # @_ZTIN8NArchive3N7z19CInArchiveExceptionE
	.section	.rodata._ZTIN8NArchive3N7z19CInArchiveExceptionE,"aG",@progbits,_ZTIN8NArchive3N7z19CInArchiveExceptionE,comdat
	.weak	_ZTIN8NArchive3N7z19CInArchiveExceptionE
	.p2align	3
_ZTIN8NArchive3N7z19CInArchiveExceptionE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NArchive3N7z19CInArchiveExceptionE
	.size	_ZTIN8NArchive3N7z19CInArchiveExceptionE, 16

	.type	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z10CCoderInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z10CCoderInfoEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 45

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z10CCoderInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z10CCoderInfoEE, 24

	.type	_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE,@object # @_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.section	.rodata._ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE,"aG",@progbits,_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE,comdat
	.weak	_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.p2align	3
_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE:
	.quad	0
	.quad	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIN8NArchive3N7z9CBindPairEED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIN8NArchive3N7z9CBindPairEE, 40

	.type	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE,@object # @_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.section	.rodata._ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE,"aG",@progbits,_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE,comdat
	.weak	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.p2align	4
_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE:
	.asciz	"13CRecordVectorIN8NArchive3N7z9CBindPairEE"
	.size	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE, 43

	.type	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE,@object # @_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.section	.rodata._ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE,"aG",@progbits,_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE,comdat
	.weak	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.p2align	4
_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIN8NArchive3N7z9CBindPairEE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIN8NArchive3N7z9CBindPairEE, 24

	.type	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI8IUnknownEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI8IUnknownEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.asciz	"13CObjectVectorI9CMyComPtrI8IUnknownEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE, 39

	.type	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI8IUnknownEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI8IUnknownEE, 24

	.type	_ZTV7CBufferIhE,@object # @_ZTV7CBufferIhE
	.section	.rodata._ZTV7CBufferIhE,"aG",@progbits,_ZTV7CBufferIhE,comdat
	.weak	_ZTV7CBufferIhE
	.p2align	3
_ZTV7CBufferIhE:
	.quad	0
	.quad	_ZTI7CBufferIhE
	.quad	_ZN7CBufferIhED2Ev
	.quad	_ZN7CBufferIhED0Ev
	.size	_ZTV7CBufferIhE, 32

	.type	_ZTS7CBufferIhE,@object # @_ZTS7CBufferIhE
	.section	.rodata._ZTS7CBufferIhE,"aG",@progbits,_ZTS7CBufferIhE,comdat
	.weak	_ZTS7CBufferIhE
_ZTS7CBufferIhE:
	.asciz	"7CBufferIhE"
	.size	_ZTS7CBufferIhE, 12

	.type	_ZTI7CBufferIhE,@object # @_ZTI7CBufferIhE
	.section	.rodata._ZTI7CBufferIhE,"aG",@progbits,_ZTI7CBufferIhE,comdat
	.weak	_ZTI7CBufferIhE
	.p2align	3
_ZTI7CBufferIhE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS7CBufferIhE
	.size	_ZTI7CBufferIhE, 16

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTV13CRecordVectorIbE,@object # @_ZTV13CRecordVectorIbE
	.section	.rodata._ZTV13CRecordVectorIbE,"aG",@progbits,_ZTV13CRecordVectorIbE,comdat
	.weak	_ZTV13CRecordVectorIbE
	.p2align	3
_ZTV13CRecordVectorIbE:
	.quad	0
	.quad	_ZTI13CRecordVectorIbE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIbED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIbE, 40

	.type	_ZTS13CRecordVectorIbE,@object # @_ZTS13CRecordVectorIbE
	.section	.rodata._ZTS13CRecordVectorIbE,"aG",@progbits,_ZTS13CRecordVectorIbE,comdat
	.weak	_ZTS13CRecordVectorIbE
	.p2align	4
_ZTS13CRecordVectorIbE:
	.asciz	"13CRecordVectorIbE"
	.size	_ZTS13CRecordVectorIbE, 19

	.type	_ZTI13CRecordVectorIbE,@object # @_ZTI13CRecordVectorIbE
	.section	.rodata._ZTI13CRecordVectorIbE,"aG",@progbits,_ZTI13CRecordVectorIbE,comdat
	.weak	_ZTI13CRecordVectorIbE
	.p2align	4
_ZTI13CRecordVectorIbE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIbE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIbE, 24

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24

	.type	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	3
_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED2Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEED0Ev
	.quad	_ZN13CObjectVectorIN8NArchive3N7z7CFolderEE6DeleteEii
	.size	_ZTV13CObjectVectorIN8NArchive3N7z7CFolderEE, 40

	.type	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	4
_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.asciz	"13CObjectVectorIN8NArchive3N7z7CFolderEE"
	.size	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE, 41

	.type	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,@object # @_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.section	.rodata._ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,"aG",@progbits,_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE,comdat
	.weak	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE
	.p2align	4
_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN8NArchive3N7z7CFolderEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN8NArchive3N7z7CFolderEE, 24

	.type	_ZTV13CObjectVectorI7CBufferIhEE,@object # @_ZTV13CObjectVectorI7CBufferIhEE
	.section	.rodata._ZTV13CObjectVectorI7CBufferIhEE,"aG",@progbits,_ZTV13CObjectVectorI7CBufferIhEE,comdat
	.weak	_ZTV13CObjectVectorI7CBufferIhEE
	.p2align	3
_ZTV13CObjectVectorI7CBufferIhEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI7CBufferIhEE
	.quad	_ZN13CObjectVectorI7CBufferIhEED2Ev
	.quad	_ZN13CObjectVectorI7CBufferIhEED0Ev
	.quad	_ZN13CObjectVectorI7CBufferIhEE6DeleteEii
	.size	_ZTV13CObjectVectorI7CBufferIhEE, 40

	.type	_ZTS13CObjectVectorI7CBufferIhEE,@object # @_ZTS13CObjectVectorI7CBufferIhEE
	.section	.rodata._ZTS13CObjectVectorI7CBufferIhEE,"aG",@progbits,_ZTS13CObjectVectorI7CBufferIhEE,comdat
	.weak	_ZTS13CObjectVectorI7CBufferIhEE
	.p2align	4
_ZTS13CObjectVectorI7CBufferIhEE:
	.asciz	"13CObjectVectorI7CBufferIhEE"
	.size	_ZTS13CObjectVectorI7CBufferIhEE, 29

	.type	_ZTI13CObjectVectorI7CBufferIhEE,@object # @_ZTI13CObjectVectorI7CBufferIhEE
	.section	.rodata._ZTI13CObjectVectorI7CBufferIhEE,"aG",@progbits,_ZTI13CObjectVectorI7CBufferIhEE,comdat
	.weak	_ZTI13CObjectVectorI7CBufferIhEE
	.p2align	4
_ZTI13CObjectVectorI7CBufferIhEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI7CBufferIhEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI7CBufferIhEE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
