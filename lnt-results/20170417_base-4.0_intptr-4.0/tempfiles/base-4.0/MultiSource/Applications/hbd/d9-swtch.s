	.text
	.file	"d9-swtch.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	4294967288              # 0xfffffff8
	.long	4294967288              # 0xfffffff8
	.long	4294967288              # 0xfffffff8
	.long	4294967288              # 0xfffffff8
.LCPI0_1:
	.long	0                       # 0x0
	.long	9                       # 0x9
	.long	0                       # 0x0
	.long	39                      # 0x27
	.text
	.globl	_Z13dotableswitchP9Classfile
	.p2align	4, 0x90
	.type	_Z13dotableswitchP9Classfile,@function
_Z13dotableswitchP9Classfile:           # @_Z13dotableswitchP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %r15d
	leal	-1(%r15), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	testb	$3, %r15b
	movl	bufflength(%rip), %r13d
	movq	inbuff(%rip), %rax
	je	.LBB0_15
# BB#1:                                 # %.lr.ph42.preheader
	leal	1(%r15), %edi
	movl	%edi, %esi
	andl	$3, %esi
	negl	%esi
	leaq	1(%rsi), %rcx
	cmpq	$8, %rcx
	jb	.LBB0_13
# BB#2:                                 # %min.iters.checked
	movabsq	$8589934584, %rbp       # imm = 0x1FFFFFFF8
	andq	%rcx, %rbp
	je	.LBB0_13
# BB#3:                                 # %vector.scevcheck
	andb	$3, %dil
	movl	%esi, %edx
	andb	$3, %dl
	addb	%dil, %dl
	movl	%edx, %ebx
	andb	$3, %bl
	cmpb	%dl, %bl
	jne	.LBB0_13
# BB#4:                                 # %vector.scevcheck
	cmpl	$3, %esi
	ja	.LBB0_13
# BB#5:                                 # %vector.body.preheader
	movd	%r13d, %xmm0
	leaq	-8(%rbp), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB0_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rsi
	pxor	%xmm1, %xmm1
	xorl	%edi, %edi
	pcmpeqd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB0_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	paddd	%xmm2, %xmm0
	paddd	%xmm2, %xmm1
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB0_8
	jmp	.LBB0_9
.LBB0_6:
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
.LBB0_9:                                # %vector.body.prol.loopexit
	cmpq	$56, %rdx
	jb	.LBB0_12
# BB#10:                                # %vector.body.preheader.new
	movq	%rbp, %rsi
	subq	%rdi, %rsi
	movdqa	.LCPI0_0(%rip), %xmm2   # xmm2 = [4294967288,4294967288,4294967288,4294967288]
	.p2align	4, 0x90
.LBB0_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	paddd	%xmm2, %xmm0
	paddd	%xmm2, %xmm1
	addq	$-64, %rsi
	jne	.LBB0_11
.LBB0_12:                               # %middle.block
	addq	%rbp, %rax
	addl	%ebp, %r15d
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %r13d
	cmpq	%rbp, %rcx
	je	.LBB0_14
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph42
                                        # =>This Inner Loop Header: Depth=1
	incl	%r15d
	decl	%r13d
	incq	%rax
	testb	$3, %r15b
	jne	.LBB0_13
.LBB0_14:                               # %._crit_edge43
	movl	%r15d, currpc(%rip)
	movl	%r13d, bufflength(%rip)
	movq	%rax, inbuff(%rip)
.LBB0_15:                               # %._crit_edge48
	movq	stkptr(%rip), %rcx
	leaq	-8(%rcx), %rdx
	movq	%rdx, stkptr(%rip)
	movq	-8(%rcx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leal	4(%r15), %ecx
	movl	%ecx, currpc(%rip)
	leal	-4(%r13), %ecx
	movl	%ecx, bufflength(%rip)
	leaq	4(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movzbl	1(%rax), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movzbl	2(%rax), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movzbl	3(%rax), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	leaq	8(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	4(%rax), %ecx
	shll	$24, %ecx
	movzbl	5(%rax), %edx
	shll	$16, %edx
	orl	%ecx, %edx
	movzbl	6(%rax), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movzbl	7(%rax), %ebp
	orl	%ecx, %ebp
	leal	12(%r15), %ecx
	movl	%ecx, currpc(%rip)
	leal	-12(%r13), %ecx
	movl	%ecx, bufflength(%rip)
	leaq	12(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	8(%rax), %ecx
	shll	$24, %ecx
	movzbl	9(%rax), %edx
	shll	$16, %edx
	orl	%ecx, %edx
	movzbl	10(%rax), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movzbl	11(%rax), %r12d
	orl	%ecx, %r12d
	movl	%r12d, %edi
	subl	%ebp, %edi
	incl	%edi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	shlq	$4, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	16(%rax), %eax
	movl	$1, 8(%r14)
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 12(%r14)
	movl	%eax, 16(%r14)
.Ltmp0:
	movl	$24, %edi
	callq	_Znwm
.Ltmp1:
# BB#16:
	movl	4(%rsp), %ecx           # 4-byte Reload
	shll	$24, %ecx
	movl	8(%rsp), %edx           # 4-byte Reload
	shll	$16, %edx
	orl	%ecx, %edx
	movl	16(%rsp), %ecx          # 4-byte Reload
	shll	$8, %ecx
	orl	%edx, %ecx
	addl	12(%rsp), %ecx          # 4-byte Folded Reload
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [0,9,0,39]
	movups	%xmm0, (%rax)
	movq	%rax, (%r14)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%r14)
	movl	%ecx, 40(%r14)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, 48(%r14)
	movq	%rbx, 56(%r14)
	movq	donestkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, donestkptr(%rip)
	movq	%r14, (%rax)
	cmpl	%ebp, %r12d
	jb	.LBB0_19
# BB#17:                                # %.lr.ph.preheader
	movq	inbuff(%rip), %rax
	addq	$4, %rax
	addl	$-16, %r13d
	addl	$16, %r15d
	.p2align	4, 0x90
.LBB0_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ecx
	incl	%ebp
	movq	%rcx, (%rbx)
	movl	%r15d, currpc(%rip)
	movl	%r13d, bufflength(%rip)
	movq	%rax, inbuff(%rip)
	movzbl	-4(%rax), %ecx
	shlq	$24, %rcx
	movzbl	-3(%rax), %edx
	shlq	$16, %rdx
	orq	%rcx, %rdx
	movzbl	-2(%rax), %ecx
	shlq	$8, %rcx
	orq	%rdx, %rcx
	movzbl	-1(%rax), %edx
	orq	%rcx, %rdx
	movq	%rdx, 8(%rbx)
	addq	$4, %rax
	addl	$-4, %r13d
	addl	$4, %r15d
	addq	$16, %rbx
	cmpl	%r12d, %ebp
	jbe	.LBB0_18
.LBB0_19:                               # %._crit_edge
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_20:
.Ltmp2:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z13dotableswitchP9Classfile, .Lfunc_end0-_Z13dotableswitchP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	4294967288              # 0xfffffff8
	.long	4294967288              # 0xfffffff8
	.long	4294967288              # 0xfffffff8
	.long	4294967288              # 0xfffffff8
.LCPI1_1:
	.long	0                       # 0x0
	.long	9                       # 0x9
	.long	0                       # 0x0
	.long	39                      # 0x27
	.text
	.globl	_Z10doluswitchP9Classfile
	.p2align	4, 0x90
	.type	_Z10doluswitchP9Classfile,@function
_Z10doluswitchP9Classfile:              # @_Z10doluswitchP9Classfile
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %r12d
	leal	-1(%r12), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	testb	$3, %r12b
	movl	bufflength(%rip), %ebx
	movq	inbuff(%rip), %rax
	je	.LBB1_15
# BB#1:                                 # %.lr.ph32.preheader
	leal	1(%r12), %edi
	movl	%edi, %esi
	andl	$3, %esi
	negl	%esi
	leaq	1(%rsi), %rbp
	cmpq	$8, %rbp
	jb	.LBB1_13
# BB#2:                                 # %min.iters.checked
	movabsq	$8589934584, %r8        # imm = 0x1FFFFFFF8
	andq	%rbp, %r8
	je	.LBB1_13
# BB#3:                                 # %vector.scevcheck
	andb	$3, %dil
	movl	%esi, %ecx
	andb	$3, %cl
	addb	%dil, %cl
	movl	%ecx, %edx
	andb	$3, %dl
	cmpb	%cl, %dl
	jne	.LBB1_13
# BB#4:                                 # %vector.scevcheck
	cmpl	$3, %esi
	ja	.LBB1_13
# BB#5:                                 # %vector.body.preheader
	movd	%ebx, %xmm0
	leaq	-8(%r8), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB1_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rsi
	pxor	%xmm1, %xmm1
	xorl	%edi, %edi
	pcmpeqd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB1_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	paddd	%xmm2, %xmm0
	paddd	%xmm2, %xmm1
	addq	$8, %rdi
	incq	%rsi
	jne	.LBB1_8
	jmp	.LBB1_9
.LBB1_6:
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
.LBB1_9:                                # %vector.body.prol.loopexit
	cmpq	$56, %rdx
	jb	.LBB1_12
# BB#10:                                # %vector.body.preheader.new
	movq	%r8, %rsi
	subq	%rdi, %rsi
	movdqa	.LCPI1_0(%rip), %xmm2   # xmm2 = [4294967288,4294967288,4294967288,4294967288]
	.p2align	4, 0x90
.LBB1_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	paddd	%xmm2, %xmm0
	paddd	%xmm2, %xmm1
	addq	$-64, %rsi
	jne	.LBB1_11
.LBB1_12:                               # %middle.block
	addq	%r8, %rax
	addl	%r8d, %r12d
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebx
	cmpq	%r8, %rbp
	je	.LBB1_14
	.p2align	4, 0x90
.LBB1_13:                               # %.lr.ph32
                                        # =>This Inner Loop Header: Depth=1
	incl	%r12d
	decl	%ebx
	incq	%rax
	testb	$3, %r12b
	jne	.LBB1_13
.LBB1_14:                               # %._crit_edge33
	movl	%r12d, currpc(%rip)
	movl	%ebx, bufflength(%rip)
	movq	%rax, inbuff(%rip)
.LBB1_15:                               # %._crit_edge38
	movq	stkptr(%rip), %rcx
	leaq	-8(%rcx), %rdx
	movq	%rdx, stkptr(%rip)
	movq	-8(%rcx), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leal	4(%r12), %ecx
	movl	%ecx, currpc(%rip)
	leal	-4(%rbx), %ecx
	movl	%ecx, bufflength(%rip)
	leaq	4(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	movzbl	1(%rax), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movzbl	2(%rax), %r13d
	movzbl	3(%rax), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	leal	8(%r12), %ecx
	movl	%ecx, currpc(%rip)
	leal	-8(%rbx), %ecx
	movl	%ecx, bufflength(%rip)
	leaq	8(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	4(%rax), %ecx
	shll	$24, %ecx
	movzbl	5(%rax), %edx
	shll	$16, %edx
	orl	%ecx, %edx
	movzbl	6(%rax), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	movzbl	7(%rax), %ebp
	orl	%ecx, %ebp
	movq	%rbp, %rdi
	shlq	$4, %rdi
	callq	_Znam
	movq	%rax, %r15
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r14
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	16(%rax), %eax
	movl	$1, 8(%r14)
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, 12(%r14)
	movl	%eax, 16(%r14)
.Ltmp3:
	movl	$24, %edi
	callq	_Znwm
.Ltmp4:
# BB#16:
	movl	(%rsp), %edx            # 4-byte Reload
	shll	$24, %edx
	movl	4(%rsp), %ecx           # 4-byte Reload
	shll	$16, %ecx
	orl	%edx, %ecx
	shll	$8, %r13d
	orl	%ecx, %r13d
	addl	8(%rsp), %r13d          # 4-byte Folded Reload
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [0,9,0,39]
	movups	%xmm0, (%rax)
	movq	%rax, (%r14)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%r14)
	movl	%r13d, 40(%r14)
	movl	%ebp, 48(%r14)
	movq	%r15, 56(%r14)
	movq	donestkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, donestkptr(%rip)
	movq	%r14, (%rax)
	testl	%ebp, %ebp
	je	.LBB1_19
# BB#17:                                # %.lr.ph.preheader
	movq	inbuff(%rip), %rax
	addl	$-16, %ebx
	movl	%r12d, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	12(%rcx,%rdx), %esi
	movl	%esi, currpc(%rip)
	leal	4(%rbx), %esi
	movl	%esi, bufflength(%rip)
	leaq	4(%rax,%rdx), %rsi
	movq	%rsi, inbuff(%rip)
	movzbl	(%rax,%rdx), %esi
	shlq	$24, %rsi
	movzbl	1(%rax,%rdx), %edi
	shlq	$16, %rdi
	orq	%rsi, %rdi
	movzbl	2(%rax,%rdx), %esi
	shlq	$8, %rsi
	orq	%rdi, %rsi
	movzbl	3(%rax,%rdx), %edi
	orq	%rsi, %rdi
	movq	%rdi, (%r15,%rdx,2)
	leal	16(%rcx,%rdx), %esi
	movl	%esi, currpc(%rip)
	movl	%ebx, bufflength(%rip)
	leaq	8(%rax,%rdx), %rsi
	movq	%rsi, inbuff(%rip)
	movzbl	4(%rax,%rdx), %esi
	shlq	$24, %rsi
	movzbl	5(%rax,%rdx), %edi
	shlq	$16, %rdi
	orq	%rsi, %rdi
	movzbl	6(%rax,%rdx), %esi
	shlq	$8, %rsi
	orq	%rdi, %rsi
	movzbl	7(%rax,%rdx), %edi
	orq	%rsi, %rdi
	addl	$-8, %ebx
	decl	%ebp
	movq	%rdi, 8(%r15,%rdx,2)
	leaq	8(%rdx), %rdx
	jne	.LBB1_18
.LBB1_19:                               # %._crit_edge
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_20:
.Ltmp5:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z10doluswitchP9Classfile, .Lfunc_end1-_Z10doluswitchP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
