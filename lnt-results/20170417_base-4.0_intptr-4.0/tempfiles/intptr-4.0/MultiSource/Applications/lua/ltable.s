	.text
	.file	"ltable.bc"
	.hidden	luaH_next
	.globl	luaH_next
	.p2align	4, 0x90
	.type	luaH_next,@function
luaH_next:                              # @luaH_next
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	8(%r12), %eax
	cmpq	$4, %rax
	ja	.LBB0_3
# BB#1:
	movl	$-1, %ebx
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_2:
	movb	11(%r14), %cl
	movl	$1, %eax
	shll	%cl, %eax
	decl	%eax
	andl	(%r12), %eax
	cltq
	jmp	.LBB0_10
.LBB0_3:
	movl	(%r12), %eax
	movb	11(%r14), %cl
	movl	$1, %esi
	shll	%cl, %esi
	decl	%esi
	orl	$1, %esi
	xorl	%edx, %edx
	divl	%esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %r13
	jmp	.LBB0_11
.LBB0_4:
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	cvttsd2si	%xmm0, %ebx
	testl	%ebx, %ebx
	jle	.LBB0_7
# BB#5:
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB0_7
	jp	.LBB0_7
# BB#6:
	cmpl	56(%r14), %ebx
	jle	.LBB0_30
.LBB0_7:                                # %arrayindex.exit.thread.thread.i
	movq	32(%r14), %r13
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB0_8
	jnp	.LBB0_12
.LBB0_8:
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%r14), %cl
	movl	$1, %esi
	shll	%cl, %esi
	decl	%esi
	orl	$1, %esi
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%r13,%rax,8), %r13
	jmp	.LBB0_12
.LBB0_9:
	movq	(%r12), %rax
	movb	11(%r14), %cl
	movl	$1, %edx
	shll	%cl, %edx
	decl	%edx
	andl	12(%rax), %edx
	movslq	%edx, %rax
.LBB0_10:                               # %mainposition.exit.preheader.i
	leaq	(%rax,%rax,4), %r13
.LBB0_11:                               # %mainposition.exit.preheader.i
	shlq	$3, %r13
	addq	32(%r14), %r13
	.p2align	4, 0x90
.LBB0_12:                               # %mainposition.exit.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	16(%r13), %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	luaO_rawequalObj
	testl	%eax, %eax
	jne	.LBB0_18
# BB#13:                                #   in Loop: Header=BB0_12 Depth=1
	cmpl	$11, 24(%r13)
	jne	.LBB0_16
# BB#14:                                #   in Loop: Header=BB0_12 Depth=1
	cmpl	$4, 8(%r12)
	jl	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_12 Depth=1
	movq	(%rbx), %rax
	cmpq	(%r12), %rax
	je	.LBB0_18
.LBB0_16:                               #   in Loop: Header=BB0_12 Depth=1
	movq	32(%r13), %r13
	testq	%r13, %r13
	jne	.LBB0_12
# BB#17:
	xorl	%ebx, %ebx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	luaG_runerror
	jmp	.LBB0_19
.LBB0_18:
	subq	32(%r14), %r13
	shrq	$3, %r13
	imull	$-858993459, %r13d, %ebx # imm = 0xCCCCCCCD
	addl	56(%r14), %ebx
.LBB0_19:                               # %findindex.exit.preheader
	movslq	%ebx, %rdx
	movslq	56(%r14), %rcx
	movq	%rdx, %rax
	shlq	$4, %rax
	incq	%rdx
	.p2align	4, 0x90
.LBB0_20:                               # %findindex.exit
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rcx, %rdx
	jge	.LBB0_23
# BB#21:                                #   in Loop: Header=BB0_20 Depth=1
	movq	24(%r14), %rsi
	incq	%rdx
	cmpl	$0, 24(%rsi,%rax)
	leaq	16(%rax), %rax
	je	.LBB0_20
# BB#22:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	%xmm0, (%r12)
	movl	$3, 8(%r12)
	movq	(%rsi,%rax), %rcx
	movq	%rcx, 16(%r12)
	leaq	8(%rsi,%rax), %rax
	jmp	.LBB0_28
.LBB0_23:
	subl	%ecx, %edx
	movb	11(%r14), %cl
	movl	$1, %edi
	shll	%cl, %edi
	xorl	%eax, %eax
	cmpl	%edi, %edx
	jge	.LBB0_29
# BB#24:                                # %.lr.ph
	movq	32(%r14), %rcx
	movslq	%edx, %rsi
	movslq	%edi, %rdi
	movslq	%edx, %rdx
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB0_25:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, 8(%rcx,%rdx)
	jne	.LBB0_27
# BB#26:                                #   in Loop: Header=BB0_25 Depth=1
	incq	%rsi
	addq	$40, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB0_25
	jmp	.LBB0_29
.LBB0_27:
	movq	16(%rcx,%rdx), %rax
	movq	%rax, (%r12)
	movl	24(%rcx,%rdx), %eax
	movl	%eax, 8(%r12)
	movq	32(%r14), %rax
	movq	(%rax,%rdx), %rcx
	movq	%rcx, 16(%r12)
	leaq	8(%rax,%rdx), %rax
.LBB0_28:                               # %.sink.split
	movl	(%rax), %eax
	movl	%eax, 24(%r12)
	movl	$1, %eax
.LBB0_29:                               # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB0_30:
	decl	%ebx
	jmp	.LBB0_19
.Lfunc_end0:
	.size	luaH_next, .Lfunc_end0-luaH_next
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_19
	.quad	.LBB0_2
	.quad	.LBB0_3
	.quad	.LBB0_4
	.quad	.LBB0_9

	.text
	.hidden	luaH_resizearray
	.globl	luaH_resizearray
	.p2align	4, 0x90
	.type	luaH_resizearray,@function
luaH_resizearray:                       # @luaH_resizearray
	.cfi_startproc
# BB#0:
	movl	$dummynode_, %eax
	cmpq	%rax, 32(%rsi)
	je	.LBB1_1
# BB#2:
	movb	11(%rsi), %cl
	movl	$1, %eax
	shll	%cl, %eax
	movl	%eax, %ecx
	jmp	resize                  # TAILCALL
.LBB1_1:
	xorl	%eax, %eax
	movl	%eax, %ecx
	jmp	resize                  # TAILCALL
.Lfunc_end1:
	.size	luaH_resizearray, .Lfunc_end1-luaH_resizearray
	.cfi_endproc

	.p2align	4, 0x90
	.type	resize,@function
resize:                                 # @resize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 128
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %r12
	movslq	56(%r14), %rbx
	movzbl	11(%r14), %r13d
	movq	32(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%edx, %eax
	cmpl	%eax, %ebx
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jge	.LBB2_12
# BB#1:
	cmpl	$-1, %eax
	jl	.LBB2_3
# BB#2:
	movq	24(%r14), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	shlq	$4, %rdx
	movslq	%eax, %rcx
	shlq	$4, %rcx
	movq	%r12, %rdi
	movl	%eax, %ebp
	callq	luaM_realloc_
	jmp	.LBB2_4
.LBB2_3:
	movq	%r12, %rdi
	movl	%eax, %ebp
	callq	luaM_toobig
.LBB2_4:
	movq	%rax, 24(%r14)
	movslq	56(%r14), %rbx
	cmpl	%ebp, %ebx
	jge	.LBB2_11
# BB#5:                                 # %.lr.ph.i
	movl	4(%rsp), %edx           # 4-byte Reload
	movslq	%edx, %rcx
	movl	%edx, %edi
	subl	%ebx, %edi
	leaq	-1(%rcx), %rsi
	subq	%rbx, %rsi
	andq	$7, %rdi
	je	.LBB2_8
# BB#6:                                 # %.prol.preheader
	movq	%rbx, %rdx
	shlq	$4, %rdx
	leaq	8(%rax,%rdx), %rbp
	negq	%rdi
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movl	$0, (%rbp)
	incq	%rbx
	addq	$16, %rbp
	incq	%rdi
	jne	.LBB2_7
.LBB2_8:                                # %.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB2_11
# BB#9:                                 # %.lr.ph.i.new
	subq	%rbx, %rcx
	shlq	$4, %rbx
	leaq	120(%rax,%rbx), %rax
	.p2align	4, 0x90
.LBB2_10:                               # =>This Inner Loop Header: Depth=1
	movl	$0, -112(%rax)
	movl	$0, -96(%rax)
	movl	$0, -80(%rax)
	movl	$0, -64(%rax)
	movl	$0, -48(%rax)
	movl	$0, -32(%rax)
	movl	$0, -16(%rax)
	movl	$0, (%rax)
	subq	$-128, %rax
	addq	$-8, %rcx
	jne	.LBB2_10
.LBB2_11:                               # %setarrayvector.exit
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 56(%r14)
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB2_12:
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	setnodevector
	movl	4(%rsp), %eax           # 4-byte Reload
	cmpl	%eax, %ebx
	movq	%rbx, %r8
	jle	.LBB2_31
# BB#13:                                # %.lr.ph76
	movl	%r13d, 20(%rsp)         # 4-byte Spill
	movl	%eax, 56(%r14)
	leaq	24(%r14), %rbx
	cltq
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_14:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_21 Depth 2
	movq	(%rbx), %r12
	movq	%rax, %r15
	shlq	$4, %r15
	leaq	1(%rax), %r13
	cmpl	$0, 8(%r12,%r15)
	je	.LBB2_26
# BB#15:                                #   in Loop: Header=BB2_14 Depth=1
	leaq	8(%r12,%r15), %rbp
	cmpl	56(%r14), %eax
	jae	.LBB2_19
# BB#16:                                #   in Loop: Header=BB2_14 Depth=1
	leaq	(%r12,%r15), %rsi
	jmp	.LBB2_17
	.p2align	4, 0x90
.LBB2_19:                               #   in Loop: Header=BB2_14 Depth=1
	cvtsi2sdl	%r13d, %xmm0
	testl	%r13d, %r13d
	movq	32(%r14), %rsi
	je	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_14 Depth=1
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%r14), %cl
	movl	$1, %edi
	shll	%cl, %edi
	decl	%edi
	orl	$1, %edi
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rsi,%rax,8), %rsi
	.p2align	4, 0x90
.LBB2_21:                               # %hashnum.exit.i.i
                                        #   Parent Loop BB2_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$3, 24(%rsi)
	jne	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_21 Depth=2
	movsd	16(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB2_23
	jnp	.LBB2_17
.LBB2_23:                               #   in Loop: Header=BB2_21 Depth=2
	movq	32(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB2_21
	jmp	.LBB2_24
	.p2align	4, 0x90
.LBB2_17:                               # %luaH_getnum.exit.i
                                        #   in Loop: Header=BB2_14 Depth=1
	movl	$luaO_nilobject_, %eax
	cmpq	%rax, %rsi
	jne	.LBB2_25
# BB#18:                                # %luaH_getnum.exit.i.luaH_getnum.exit.i.thread_crit_edge
                                        #   in Loop: Header=BB2_14 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
.LBB2_24:                               # %luaH_getnum.exit.i.thread
                                        #   in Loop: Header=BB2_14 Depth=1
	movsd	%xmm0, 56(%rsp)
	movl	$3, 64(%rsp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	leaq	56(%rsp), %rdx
	callq	newkey
	movq	%rax, %rsi
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
.LBB2_25:                               # %luaH_setnum.exit
                                        #   in Loop: Header=BB2_14 Depth=1
	movq	(%r12,%r15), %rax
	movq	%rax, (%rsi)
	movl	(%rbp), %eax
	movl	%eax, 8(%rsi)
.LBB2_26:                               # %._crit_edge81
                                        #   in Loop: Header=BB2_14 Depth=1
	cmpl	%r8d, %r13d
	movq	%r13, %rax
	jne	.LBB2_14
# BB#27:                                # %._crit_edge77
	cmpl	$-1, 4(%rsp)            # 4-byte Folded Reload
	jl	.LBB2_29
# BB#28:
	movq	(%rbx), %rsi
	shlq	$4, %r8
	movq	40(%rsp), %rcx          # 8-byte Reload
	shlq	$4, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r8, %rdx
	callq	luaM_realloc_
	jmp	.LBB2_30
.LBB2_29:
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	luaM_toobig
.LBB2_30:
	movl	20(%rsp), %r13d         # 4-byte Reload
	movq	%rax, (%rbx)
.LBB2_31:
	movl	$1, %r15d
	movl	%r13d, %ecx
	shll	%cl, %r15d
	cmpb	$31, %r13b
	je	.LBB2_44
# BB#32:                                # %.lr.ph
	movslq	%r15d, %rax
	leaq	(%rax,%rax,4), %rcx
	leaq	1(%rax), %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	-16(%rax,%rcx,8), %rbp
	movl	$luaO_nilobject_, %r13d
	.p2align	4, 0x90
.LBB2_33:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, -16(%rbp)
	je	.LBB2_43
# BB#34:                                #   in Loop: Header=BB2_33 Depth=1
	leaq	-8(%rbp), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	luaH_get
	movb	$0, 10(%r14)
	cmpq	%r13, %rax
	jne	.LBB2_42
# BB#35:                                #   in Loop: Header=BB2_33 Depth=1
	movl	(%rbp), %eax
	cmpl	$3, %eax
	je	.LBB2_38
# BB#36:                                #   in Loop: Header=BB2_33 Depth=1
	testl	%eax, %eax
	jne	.LBB2_41
# BB#37:                                #   in Loop: Header=BB2_33 Depth=1
	movl	$.L.str, %esi
	jmp	.LBB2_40
.LBB2_38:                               #   in Loop: Header=BB2_33 Depth=1
	movsd	-8(%rbp), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_41
# BB#39:                                #   in Loop: Header=BB2_33 Depth=1
	movl	$.L.str.1, %esi
.LBB2_40:                               #   in Loop: Header=BB2_33 Depth=1
	xorl	%eax, %eax
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	luaG_runerror
.LBB2_41:                               #   in Loop: Header=BB2_33 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	newkey
.LBB2_42:                               # %luaH_set.exit
                                        #   in Loop: Header=BB2_33 Depth=1
	movq	-24(%rbp), %rcx
	movq	%rcx, (%rax)
	movl	-16(%rbp), %ecx
	movl	%ecx, 8(%rax)
.LBB2_43:                               # %.backedge
                                        #   in Loop: Header=BB2_33 Depth=1
	decq	%rbx
	addq	$-40, %rbp
	cmpq	$1, %rbx
	jg	.LBB2_33
.LBB2_44:                               # %._crit_edge
	movl	$dummynode_, %eax
	movq	32(%rsp), %rsi          # 8-byte Reload
	cmpq	%rax, %rsi
	je	.LBB2_46
# BB#45:
	movslq	%r15d, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	xorl	%ecx, %ecx
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	luaM_realloc_
.LBB2_46:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	resize, .Lfunc_end2-resize
	.cfi_endproc

	.hidden	luaH_new
	.globl	luaH_new
	.p2align	4, 0x90
	.type	luaH_new,@function
luaH_new:                               # @luaH_new
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %r15
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$64, %ecx
	callq	luaM_realloc_
	movq	%rax, %rbx
	movl	$5, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	luaC_link
	movq	$0, 16(%rbx)
	movb	$-1, 10(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 56(%rbx)
	movb	$0, 11(%rbx)
	movq	$dummynode_, 32(%rbx)
	cmpl	$-1, %ebp
	jl	.LBB3_2
# BB#1:
	movslq	%ebp, %rcx
	shlq	$4, %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	luaM_realloc_
	jmp	.LBB3_3
.LBB3_2:
	movq	%r15, %rdi
	callq	luaM_toobig
.LBB3_3:
	movq	%rax, 24(%rbx)
	movslq	56(%rbx), %rdx
	cmpl	%ebp, %edx
	jge	.LBB3_10
# BB#4:                                 # %.lr.ph.i
	movslq	%ebp, %rcx
	movl	%ebp, %edi
	subl	%edx, %edi
	leaq	-1(%rcx), %r8
	subq	%rdx, %r8
	andq	$7, %rdi
	je	.LBB3_7
# BB#5:                                 # %.prol.preheader
	movq	%rdx, %rsi
	shlq	$4, %rsi
	leaq	8(%rax,%rsi), %rsi
	negq	%rdi
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	movl	$0, (%rsi)
	incq	%rdx
	addq	$16, %rsi
	incq	%rdi
	jne	.LBB3_6
.LBB3_7:                                # %.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB3_10
# BB#8:                                 # %.lr.ph.i.new
	subq	%rdx, %rcx
	shlq	$4, %rdx
	leaq	120(%rax,%rdx), %rax
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movl	$0, -112(%rax)
	movl	$0, -96(%rax)
	movl	$0, -80(%rax)
	movl	$0, -64(%rax)
	movl	$0, -48(%rax)
	movl	$0, -32(%rax)
	movl	$0, -16(%rax)
	movl	$0, (%rax)
	subq	$-128, %rax
	addq	$-8, %rcx
	jne	.LBB3_9
.LBB3_10:                               # %setarrayvector.exit
	movl	%ebp, 56(%rbx)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	%r14d, %edx
	callq	setnodevector
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	luaH_new, .Lfunc_end3-luaH_new
	.cfi_endproc

	.p2align	4, 0x90
	.type	setnodevector,@function
setnodevector:                          # @setnodevector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testl	%edx, %edx
	je	.LBB4_1
# BB#2:
	decl	%edx
	movl	%edx, %edi
	callq	luaO_log2
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%rax), %r14d
	cmpl	$26, %eax
	jl	.LBB4_4
# BB#3:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	luaG_runerror
.LBB4_4:
	movl	$1, %ebp
	movl	%r14d, %ecx
	shll	%cl, %ebp
	cmpl	$-1, %ebp
	jl	.LBB4_6
# BB#5:
	movslq	%ebp, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	luaM_realloc_
	jmp	.LBB4_7
.LBB4_1:
	movq	$dummynode_, 32(%rbx)
	movl	$dummynode_, %eax
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	jmp	.LBB4_10
.LBB4_6:
	movq	%r15, %rdi
	callq	luaM_toobig
.LBB4_7:
	movq	%rax, 32(%rbx)
	cmpl	$31, %r14d
	movslq	%ebp, %rcx
	je	.LBB4_10
# BB#8:                                 # %.lr.ph.preheader
	leaq	32(%rax), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_9:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	$0, (%rdx)
	movl	$0, -8(%rdx)
	movl	$0, -24(%rdx)
	incq	%rsi
	addq	$40, %rdx
	cmpq	%rcx, %rsi
	jl	.LBB4_9
.LBB4_10:
	movb	%r14b, 11(%rbx)
	leaq	(%rcx,%rcx,4), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	setnodevector, .Lfunc_end4-setnodevector
	.cfi_endproc

	.hidden	luaH_free
	.globl	luaH_free
	.p2align	4, 0x90
	.type	luaH_free,@function
luaH_free:                              # @luaH_free
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi43:
	.cfi_def_cfa_offset 32
.Lcfi44:
	.cfi_offset %rbx, -24
.Lcfi45:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	32(%rbx), %rsi
	movl	$dummynode_, %eax
	cmpq	%rax, %rsi
	je	.LBB5_2
# BB#1:
	movb	11(%rbx), %cl
	movl	$1, %eax
	shll	%cl, %eax
	cltq
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	luaM_realloc_
.LBB5_2:
	movq	24(%rbx), %rsi
	movslq	56(%rbx), %rdx
	shlq	$4, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	luaM_realloc_
	movl	$64, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	luaM_realloc_           # TAILCALL
.Lfunc_end5:
	.size	luaH_free, .Lfunc_end5-luaH_free
	.cfi_endproc

	.hidden	luaH_getnum
	.globl	luaH_getnum
	.p2align	4, 0x90
	.type	luaH_getnum,@function
luaH_getnum:                            # @luaH_getnum
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	leal	-1(%rsi), %eax
	cmpl	56(%rdi), %eax
	jae	.LBB6_2
# BB#1:
	movslq	%eax, %rsi
	shlq	$4, %rsi
	addq	24(%rdi), %rsi
	movq	%rsi, %rax
	retq
.LBB6_2:
	cvtsi2sdl	%esi, %xmm0
	testl	%esi, %esi
	movq	32(%rdi), %rsi
	je	.LBB6_4
# BB#3:
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%rdi), %cl
	movl	$1, %edi
	shll	%cl, %edi
	decl	%edi
	orl	$1, %edi
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rsi,%rax,8), %rsi
	.p2align	4, 0x90
.LBB6_4:                                # %hashnum.exit
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$3, 24(%rsi)
	jne	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=1
	movsd	16(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB6_6
	jnp	.LBB6_8
.LBB6_6:                                #   in Loop: Header=BB6_4 Depth=1
	movq	32(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_4
# BB#7:
	movl	$luaO_nilobject_, %esi
.LBB6_8:                                # %.loopexit
	movq	%rsi, %rax
	retq
.Lfunc_end6:
	.size	luaH_getnum, .Lfunc_end6-luaH_getnum
	.cfi_endproc

	.hidden	luaH_getstr
	.globl	luaH_getstr
	.p2align	4, 0x90
	.type	luaH_getstr,@function
luaH_getstr:                            # @luaH_getstr
	.cfi_startproc
# BB#0:
	movb	11(%rdi), %cl
	movl	$1, %eax
	shll	%cl, %eax
	decl	%eax
	andl	12(%rsi), %eax
	cltq
	leaq	(%rax,%rax,4), %rax
	shlq	$3, %rax
	addq	32(%rdi), %rax
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	$4, 24(%rax)
	jne	.LBB7_3
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	cmpq	%rsi, 16(%rax)
	je	.LBB7_5
.LBB7_3:                                #   in Loop: Header=BB7_1 Depth=1
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_1
# BB#4:
	movl	$luaO_nilobject_, %eax
.LBB7_5:                                # %.loopexit
	retq
.Lfunc_end7:
	.size	luaH_getstr, .Lfunc_end7-luaH_getstr
	.cfi_endproc

	.hidden	luaH_get
	.globl	luaH_get
	.p2align	4, 0x90
	.type	luaH_get,@function
luaH_get:                               # @luaH_get
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -24
.Lcfi50:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	8(%r14), %eax
	cmpq	$4, %rax
	ja	.LBB8_17
# BB#1:
	movl	$luaO_nilobject_, %ebx
	jmpq	*.LJTI8_0(,%rax,8)
.LBB8_16:
	movb	11(%rdi), %cl
	movl	$1, %eax
	shll	%cl, %eax
	decl	%eax
	andl	(%r14), %eax
	cltq
	leaq	(%rax,%rax,4), %rbx
	jmp	.LBB8_18
.LBB8_17:
	movl	(%r14), %eax
	movb	11(%rdi), %cl
	movl	$1, %esi
	shll	%cl, %esi
	decl	%esi
	orl	$1, %esi
	xorl	%edx, %edx
	divl	%esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rbx
.LBB8_18:                               # %mainposition.exit.preheader
	shlq	$3, %rbx
	addq	32(%rdi), %rbx
	.p2align	4, 0x90
.LBB8_19:                               # %mainposition.exit
                                        # =>This Inner Loop Header: Depth=1
	leaq	16(%rbx), %rdi
	movq	%r14, %rsi
	callq	luaO_rawequalObj
	testl	%eax, %eax
	jne	.LBB8_22
# BB#20:                                #   in Loop: Header=BB8_19 Depth=1
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_19
	jmp	.LBB8_21
.LBB8_6:
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	cvttsd2si	%xmm1, %eax
	cvtsi2sdl	%eax, %xmm0
	ucomisd	%xmm1, %xmm0
	jne	.LBB8_14
	jp	.LBB8_14
# BB#7:
	leal	-1(%rax), %ecx
	cmpl	56(%rdi), %ecx
	jae	.LBB8_9
# BB#8:
	movslq	%ecx, %rbx
	shlq	$4, %rbx
	addq	24(%rdi), %rbx
	jmp	.LBB8_22
.LBB8_2:
	movq	(%r14), %rax
	movb	11(%rdi), %cl
	movl	$1, %edx
	shll	%cl, %edx
	decl	%edx
	andl	12(%rax), %edx
	movslq	%edx, %rcx
	leaq	(%rcx,%rcx,4), %rbx
	shlq	$3, %rbx
	addq	32(%rdi), %rbx
	.p2align	4, 0x90
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	$4, 24(%rbx)
	jne	.LBB8_5
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=1
	cmpq	%rax, 16(%rbx)
	je	.LBB8_22
.LBB8_5:                                #   in Loop: Header=BB8_3 Depth=1
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_3
	jmp	.LBB8_21
.LBB8_14:                               # %luaH_getnum.exit.thread
	movq	(%r14), %xmm0           # xmm0 = mem[0],zero
	movq	32(%rdi), %rbx
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB8_15
	jnp	.LBB8_19
.LBB8_15:
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%rdi), %cl
	movl	$1, %esi
	shll	%cl, %esi
	decl	%esi
	orl	$1, %esi
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rbx,%rax,8), %rbx
	jmp	.LBB8_19
.LBB8_9:
	testl	%eax, %eax
	movq	32(%rdi), %rbx
	je	.LBB8_11
# BB#10:
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%rdi), %cl
	movl	$1, %esi
	shll	%cl, %esi
	decl	%esi
	orl	$1, %esi
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rbx,%rax,8), %rbx
	.p2align	4, 0x90
.LBB8_11:                               # %hashnum.exit.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$3, 24(%rbx)
	jne	.LBB8_13
# BB#12:                                #   in Loop: Header=BB8_11 Depth=1
	movsd	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB8_13
	jnp	.LBB8_22
.LBB8_13:                               #   in Loop: Header=BB8_11 Depth=1
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_11
.LBB8_21:
	movl	$luaO_nilobject_, %ebx
.LBB8_22:                               # %luaH_getstr.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	luaH_get, .Lfunc_end8-luaH_get
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_22
	.quad	.LBB8_16
	.quad	.LBB8_17
	.quad	.LBB8_6
	.quad	.LBB8_2

	.text
	.hidden	luaH_set
	.globl	luaH_set
	.p2align	4, 0x90
	.type	luaH_set,@function
luaH_set:                               # @luaH_set
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	luaH_get
	movb	$0, 10(%rbx)
	movl	$luaO_nilobject_, %ecx
	cmpq	%rcx, %rax
	je	.LBB9_1
# BB#8:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB9_1:
	movl	8(%r14), %eax
	cmpl	$3, %eax
	je	.LBB9_4
# BB#2:
	testl	%eax, %eax
	jne	.LBB9_7
# BB#3:
	movl	$.L.str, %esi
	jmp	.LBB9_6
.LBB9_4:
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB9_7
# BB#5:
	movl	$.L.str.1, %esi
.LBB9_6:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	luaG_runerror
.LBB9_7:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	newkey                  # TAILCALL
.Lfunc_end9:
	.size	luaH_set, .Lfunc_end9-luaH_set
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.p2align	4, 0x90
	.type	newkey,@function
newkey:                                 # @newkey
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 192
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	8(%rbp), %eax
	decl	%eax
	cmpl	$3, %eax
	ja	.LBB10_3
# BB#1:
	jmpq	*.LJTI10_0(,%rax,8)
.LBB10_2:
	movq	32(%r12), %rsi
	movb	11(%r12), %cl
	movl	$1, %eax
	shll	%cl, %eax
	decl	%eax
	andl	(%rbp), %eax
	cltq
	jmp	.LBB10_8
.LBB10_3:
	movq	32(%r12), %rsi
	movl	(%rbp), %eax
	movb	11(%r12), %cl
	movl	$1, %edi
	shll	%cl, %edi
	decl	%edi
	orl	$1, %edi
	xorl	%edx, %edx
.LBB10_4:                               # %mainposition.exit
	divl	%edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	jmp	.LBB10_9
.LBB10_5:
	movq	(%rbp), %xmm0           # xmm0 = mem[0],zero
	movq	32(%r12), %rsi
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB10_57
	jp	.LBB10_57
# BB#6:
	movq	%rsi, %rbx
	jmp	.LBB10_10
.LBB10_7:
	movq	32(%r12), %rsi
	movq	(%rbp), %rax
	movb	11(%r12), %cl
	movl	$1, %edx
	shll	%cl, %edx
	decl	%edx
	andl	12(%rax), %edx
	movslq	%edx, %rax
.LBB10_8:                               # %mainposition.exit
	leaq	(%rax,%rax,4), %rax
.LBB10_9:                               # %mainposition.exit
	leaq	(%rsi,%rax,8), %rbx
.LBB10_10:                              # %mainposition.exit
	movl	$dummynode_, %eax
	cmpq	%rax, %rbx
	je	.LBB10_12
# BB#11:                                # %mainposition.exit
	movl	8(%rbx), %eax
	testl	%eax, %eax
	je	.LBB10_67
.LBB10_12:
	movq	40(%r12), %rax
	.p2align	4, 0x90
.LBB10_13:                              # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdi
	cmpq	%rsi, %rdi
	jbe	.LBB10_18
# BB#14:                                #   in Loop: Header=BB10_13 Depth=1
	leaq	-40(%rdi), %rax
	cmpl	$0, -16(%rdi)
	jne	.LBB10_13
# BB#15:
	addq	$-40, %rdi
	movq	%rdi, 40(%r12)
	movl	24(%rbx), %eax
	decl	%eax
	cmpl	$3, %eax
	ja	.LBB10_55
# BB#16:
	jmpq	*.LJTI10_1(,%rax,8)
.LBB10_17:
	movb	11(%r12), %cl
	movl	$1, %eax
	shll	%cl, %eax
	decl	%eax
	andl	16(%rbx), %eax
	cltq
	jmp	.LBB10_61
.LBB10_18:
	movq	%rbp, (%rsp)            # 8-byte Spill
	addq	$-40, %rdi
	movq	%rdi, 40(%r12)
	pxor	%xmm8, %xmm8
	movdqu	%xmm8, 108(%rsp)
	movdqa	%xmm8, 96(%rsp)
	movdqa	%xmm8, 80(%rsp)
	movdqa	%xmm8, 64(%rsp)
	movdqa	%xmm8, 48(%rsp)
	movdqa	%xmm8, 32(%rsp)
	movdqa	%xmm8, 16(%rsp)
	movl	56(%r12), %r10d
	xorl	%r11d, %r11d
	movl	$1, %edi
	movl	$8, %r8d
	movdqa	.LCPI10_0(%rip), %xmm9  # xmm9 = [1,1,1,1]
	xorl	%r15d, %r15d
	movl	$1, %esi
	.p2align	4, 0x90
.LBB10_19:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_26 Depth 2
                                        #     Child Loop BB10_30 Depth 2
	cmpl	%r10d, %esi
	movl	%esi, %edx
	jle	.LBB10_21
# BB#20:                                #   in Loop: Header=BB10_19 Depth=1
	cmpl	%r10d, %edi
	movl	%r10d, %edx
	jg	.LBB10_32
.LBB10_21:                              # %.preheader.i.i
                                        #   in Loop: Header=BB10_19 Depth=1
	xorl	%eax, %eax
	cmpl	%edx, %edi
	jg	.LBB10_31
# BB#22:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB10_19 Depth=1
	movq	24(%r12), %r9
	movslq	%edi, %rbx
	movslq	%edx, %rdx
	cmpq	%rdx, %rbx
	movq	%rdx, %rbp
	cmovgeq	%rbx, %rbp
	incq	%rbp
	subq	%rbx, %rbp
	xorl	%eax, %eax
	cmpq	$8, %rbp
	jb	.LBB10_28
# BB#24:                                # %min.iters.checked
                                        #   in Loop: Header=BB10_19 Depth=1
	movq	%rbp, %rcx
	andq	$7, %rcx
	cmoveq	%r8, %rcx
	movq	%rbp, %rdi
	subq	%rcx, %rdi
	subq	%rcx, %rbp
	je	.LBB10_28
# BB#25:                                # %vector.body.preheader
                                        #   in Loop: Header=BB10_19 Depth=1
	addq	%rbx, %rdi
	shlq	$4, %rbx
	leaq	56(%r9,%rbx), %rax
	pxor	%xmm2, %xmm2
	pxor	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB10_26:                              # %vector.body
                                        #   Parent Loop BB10_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-64(%rax), %xmm4
	movdqu	-48(%rax), %xmm11
	movdqu	-32(%rax), %xmm6
	movdqu	-16(%rax), %xmm7
	movdqu	16(%rax), %xmm10
	movdqu	(%rax), %xmm1
	movdqu	48(%rax), %xmm0
	movdqu	32(%rax), %xmm5
	punpckldq	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	punpckldq	%xmm11, %xmm4   # xmm4 = xmm4[0],xmm11[0],xmm4[1],xmm11[1]
	punpcklqdq	%xmm6, %xmm4    # xmm4 = xmm4[0],xmm6[0]
	punpckldq	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	punpckldq	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1]
	punpcklqdq	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0]
	pcmpeqd	%xmm8, %xmm4
	pandn	%xmm9, %xmm4
	pcmpeqd	%xmm8, %xmm1
	pandn	%xmm9, %xmm1
	paddd	%xmm4, %xmm2
	paddd	%xmm1, %xmm3
	subq	$-128, %rax
	addq	$-8, %rbp
	jne	.LBB10_26
# BB#27:                                # %middle.block
                                        #   in Loop: Header=BB10_19 Depth=1
	paddd	%xmm2, %xmm3
	pshufd	$78, %xmm3, %xmm0       # xmm0 = xmm3[2,3,0,1]
	paddd	%xmm3, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %eax
	jmp	.LBB10_29
	.p2align	4, 0x90
.LBB10_28:                              #   in Loop: Header=BB10_19 Depth=1
	movq	%rbx, %rdi
.LBB10_29:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB10_19 Depth=1
	movq	%rdi, %rcx
	shlq	$4, %rcx
	leaq	-8(%r9,%rcx), %rbp
	.p2align	4, 0x90
.LBB10_30:                              # %scalar.ph
                                        #   Parent Loop BB10_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, (%rbp)
	sbbl	$-1, %eax
	addq	$16, %rbp
	cmpq	%rdx, %rdi
	leaq	1(%rdi), %rdi
	jl	.LBB10_30
.LBB10_31:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB10_19 Depth=1
	addl	%eax, 16(%rsp,%r11,4)
	addl	%eax, %r15d
	incq	%r11
	addl	%esi, %esi
	cmpq	$27, %r11
	jl	.LBB10_19
.LBB10_32:                              # %numusearray.exit.i
	movb	11(%r12), %cl
	movl	$1, %ebp
	shll	%cl, %ebp
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	jmp	.LBB10_38
	.p2align	4, 0x90
.LBB10_33:                              #   in Loop: Header=BB10_38 Depth=1
	xorl	%eax, %eax
	cmpl	$3, (%rdx,%rcx)
	jne	.LBB10_37
# BB#34:                                #   in Loop: Header=BB10_38 Depth=1
	movq	-8(%rdx,%rcx), %xmm0    # xmm0 = mem[0],zero
	cvttsd2si	%xmm0, %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	ucomisd	%xmm0, %xmm1
	jne	.LBB10_37
	jp	.LBB10_37
# BB#35:                                # %arrayindex.exit.i.i.i
                                        #   in Loop: Header=BB10_38 Depth=1
	decl	%edi
	cmpl	$67108863, %edi         # imm = 0x3FFFFFF
	ja	.LBB10_37
# BB#36:                                #   in Loop: Header=BB10_38 Depth=1
	callq	luaO_log2
	cltq
	incl	20(%rsp,%rax,4)
	movl	$1, %eax
	.p2align	4, 0x90
.LBB10_37:                              # %countint.exit.i.i
                                        #   in Loop: Header=BB10_38 Depth=1
	negl	%ebp
	addl	%eax, %r13d
	incl	%ebx
.LBB10_38:                              # %.outer.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_39 Depth 2
	movslq	%ebp, %rax
	leaq	(%rax,%rax,4), %rcx
	movl	%eax, %ebp
	negl	%ebp
	leaq	24(,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB10_39:                              #   Parent Loop BB10_38 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ebp
	cmpl	$1, %ebp
	je	.LBB10_41
# BB#40:                                #   in Loop: Header=BB10_39 Depth=2
	movq	32(%r12), %rdx
	cmpl	$0, -56(%rdx,%rcx)
	leaq	-40(%rcx), %rcx
	je	.LBB10_39
	jmp	.LBB10_33
.LBB10_41:                              # %numusehash.exit.i
	movq	%r14, 8(%rsp)           # 8-byte Spill
	addl	%r15d, %r13d
	xorl	%r14d, %r14d
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$3, 8(%rax)
	movl	$0, %eax
	jne	.LBB10_45
# BB#42:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	(%rax), %xmm0           # xmm0 = mem[0],zero
	cvttsd2si	%xmm0, %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	ucomisd	%xmm0, %xmm1
	movl	$0, %eax
	jne	.LBB10_45
	jp	.LBB10_45
# BB#43:                                # %arrayindex.exit.i.i
	decl	%edi
	cmpl	$67108863, %edi         # imm = 0x3FFFFFF
	movl	$0, %eax
	ja	.LBB10_45
# BB#44:
	callq	luaO_log2
	cltq
	incl	20(%rsp,%rax,4)
	movl	$1, %eax
.LBB10_45:                              # %countint.exit.i
	addl	%eax, %r13d
	movl	$0, %edx
	jle	.LBB10_51
# BB#46:                                # %.lr.ph.i19.i.preheader
	movl	$1, %ecx
	xorl	%edi, %edi
	leaq	16(%rsp), %rax
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_47:                              # %.lr.ph.i19.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ebp
	testl	%ebp, %ebp
	jle	.LBB10_49
# BB#48:                                #   in Loop: Header=BB10_47 Depth=1
	addl	%ebp, %esi
	cmpl	%edi, %esi
	cmovgl	%esi, %r14d
	cmovgl	%ecx, %edx
.LBB10_49:                              #   in Loop: Header=BB10_47 Depth=1
	cmpl	%r13d, %esi
	je	.LBB10_51
# BB#50:                                #   in Loop: Header=BB10_47 Depth=1
	addq	$4, %rax
	cmpl	%r13d, %ecx
	movl	%ecx, %edi
	leal	(%rcx,%rcx), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jl	.LBB10_47
.LBB10_51:                              # %rehash.exit
	leal	1(%r15,%rbx), %ecx
	subl	%r14d, %ecx
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	resize
	movq	%r12, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rsi
	callq	luaH_get
	movq	%rax, %rbx
	movb	$0, 10(%r12)
	movl	$luaO_nilobject_, %eax
	cmpq	%rax, %rbx
	jne	.LBB10_71
# BB#52:
	movl	8(%rbp), %eax
	cmpl	$3, %eax
	je	.LBB10_72
# BB#53:
	testl	%eax, %eax
	jne	.LBB10_75
# BB#54:
	movl	$.L.str, %esi
	jmp	.LBB10_74
.LBB10_55:
	movl	16(%rbx), %eax
	movb	11(%r12), %cl
	movl	$1, %r8d
	shll	%cl, %r8d
	decl	%r8d
	orl	$1, %r8d
	xorl	%edx, %edx
.LBB10_56:                              # %mainposition.exit53
	divl	%r8d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	jmp	.LBB10_62
.LBB10_57:
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%r12), %cl
	movl	$1, %edi
	shll	%cl, %edi
	decl	%edi
	orl	$1, %edi
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	jmp	.LBB10_4
.LBB10_58:
	movq	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB10_59
	jnp	.LBB10_63
.LBB10_59:
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%r12), %cl
	movl	$1, %r8d
	shll	%cl, %r8d
	decl	%r8d
	orl	$1, %r8d
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	jmp	.LBB10_56
.LBB10_60:
	movq	16(%rbx), %rax
	movb	11(%r12), %cl
	movl	$1, %edx
	shll	%cl, %edx
	decl	%edx
	andl	12(%rax), %edx
	movslq	%edx, %rax
.LBB10_61:                              # %mainposition.exit53
	leaq	(%rax,%rax,4), %rax
.LBB10_62:                              # %mainposition.exit53
	leaq	(%rsi,%rax,8), %rsi
.LBB10_63:                              # %mainposition.exit53
	cmpq	%rbx, %rsi
	je	.LBB10_66
	.p2align	4, 0x90
.LBB10_64:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rax
	movq	32(%rax), %rsi
	cmpq	%rbx, %rsi
	jne	.LBB10_64
# BB#65:
	movq	%rdi, 32(%rax)
	movq	32(%rbx), %rax
	movq	%rax, 32(%rdi)
	movdqu	(%rbx), %xmm0
	movdqu	16(%rbx), %xmm1
	movdqu	%xmm1, 16(%rdi)
	movdqu	%xmm0, (%rdi)
	movq	$0, 32(%rbx)
	movl	$0, 8(%rbx)
	jmp	.LBB10_67
.LBB10_66:
	movq	32(%rbx), %rax
	movq	%rax, 32(%rdi)
	movq	%rdi, 32(%rbx)
	movq	%rdi, %rbx
.LBB10_67:                              # %luaH_set.exit.thread
	movq	(%rbp), %rax
	movq	%rax, 16(%rbx)
	movl	8(%rbp), %eax
	movl	%eax, 24(%rbx)
	cmpl	$4, %eax
	jl	.LBB10_71
# BB#68:
	movq	(%rbp), %rax
	testb	$3, 9(%rax)
	je	.LBB10_71
# BB#69:
	testb	$4, 9(%r12)
	je	.LBB10_71
# BB#70:
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	luaC_barrierback
.LBB10_71:                              # %luaH_set.exit
	movq	%rbx, %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_72:
	movq	(%rbp), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB10_75
# BB#73:
	movl	$.L.str.1, %esi
.LBB10_74:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	luaG_runerror
.LBB10_75:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	newkey                  # TAILCALL
.Lfunc_end10:
	.size	newkey, .Lfunc_end10-newkey
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI10_0:
	.quad	.LBB10_2
	.quad	.LBB10_3
	.quad	.LBB10_5
	.quad	.LBB10_7
.LJTI10_1:
	.quad	.LBB10_17
	.quad	.LBB10_55
	.quad	.LBB10_58
	.quad	.LBB10_60

	.text
	.hidden	luaH_setnum
	.globl	luaH_setnum
	.p2align	4, 0x90
	.type	luaH_setnum,@function
luaH_setnum:                            # @luaH_setnum
	.cfi_startproc
# BB#0:
	movl	%edx, %r8d
	leal	-1(%r8), %eax
	cmpl	56(%rsi), %eax
	jae	.LBB11_4
# BB#1:
	movslq	%eax, %r9
	shlq	$4, %r9
	addq	24(%rsi), %r9
	jmp	.LBB11_2
.LBB11_4:
	cvtsi2sdl	%r8d, %xmm0
	testl	%r8d, %r8d
	movq	32(%rsi), %r9
	je	.LBB11_6
# BB#5:
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%rsi), %cl
	movl	$1, %r10d
	shll	%cl, %r10d
	decl	%r10d
	orl	$1, %r10d
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%r10d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%r9,%rax,8), %r9
	.p2align	4, 0x90
.LBB11_6:                               # %hashnum.exit.i
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$3, 24(%r9)
	jne	.LBB11_8
# BB#7:                                 #   in Loop: Header=BB11_6 Depth=1
	movsd	16(%r9), %xmm1          # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB11_8
	jnp	.LBB11_2
.LBB11_8:                               #   in Loop: Header=BB11_6 Depth=1
	movq	32(%r9), %r9
	testq	%r9, %r9
	jne	.LBB11_6
	jmp	.LBB11_9
.LBB11_2:                               # %luaH_getnum.exit
	movl	$luaO_nilobject_, %eax
	cmpq	%rax, %r9
	jne	.LBB11_10
# BB#3:                                 # %luaH_getnum.exit.luaH_getnum.exit.thread_crit_edge
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
.LBB11_9:                               # %luaH_getnum.exit.thread
	subq	$24, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 32
	movsd	%xmm0, 8(%rsp)
	movl	$3, 16(%rsp)
	leaq	8(%rsp), %rdx
	callq	newkey
	movq	%rax, %r9
	addq	$24, %rsp
.LBB11_10:
	movq	%r9, %rax
	retq
.Lfunc_end11:
	.size	luaH_setnum, .Lfunc_end11-luaH_setnum
	.cfi_endproc

	.hidden	luaH_setstr
	.globl	luaH_setstr
	.p2align	4, 0x90
	.type	luaH_setstr,@function
luaH_setstr:                            # @luaH_setstr
	.cfi_startproc
# BB#0:
	movb	11(%rsi), %cl
	movl	$1, %eax
	shll	%cl, %eax
	decl	%eax
	andl	12(%rdx), %eax
	cltq
	leaq	(%rax,%rax,4), %rax
	shlq	$3, %rax
	addq	32(%rsi), %rax
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$4, 24(%rax)
	jne	.LBB12_3
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	cmpq	%rdx, 16(%rax)
	je	.LBB12_4
.LBB12_3:                               #   in Loop: Header=BB12_1 Depth=1
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB12_1
.LBB12_5:                               # %luaH_getstr.exit.thread
	subq	$24, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 32
	movq	%rdx, 8(%rsp)
	movl	$4, 16(%rsp)
	leaq	8(%rsp), %rdx
	callq	newkey
	addq	$24, %rsp
	retq
.LBB12_4:                               # %luaH_getstr.exit
	movl	$luaO_nilobject_, %ecx
	cmpq	%rcx, %rax
	je	.LBB12_5
# BB#6:
	retq
.Lfunc_end12:
	.size	luaH_setstr, .Lfunc_end12-luaH_setstr
	.cfi_endproc

	.hidden	luaH_getn
	.globl	luaH_getn
	.p2align	4, 0x90
	.type	luaH_getn,@function
luaH_getn:                              # @luaH_getn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 16
.Lcfi73:
	.cfi_offset %rbx, -16
	movl	56(%rdi), %r8d
	testl	%r8d, %r8d
	je	.LBB13_5
# BB#1:
	movq	24(%rdi), %rax
	leal	-1(%r8), %ecx
	shlq	$4, %rcx
	cmpl	$0, 8(%rax,%rcx)
	je	.LBB13_2
.LBB13_5:
	movq	32(%rdi), %r9
	movl	$dummynode_, %eax
	cmpq	%rax, %r9
	je	.LBB13_6
# BB#7:
	leal	1(%r8), %eax
	movl	%r8d, %r10d
	.p2align	4, 0x90
.LBB13_8:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_12 Depth 2
	movl	%eax, %ebx
	leal	-1(%rbx), %eax
	cmpl	%r8d, %eax
	jae	.LBB13_10
# BB#9:                                 #   in Loop: Header=BB13_8 Depth=1
	cltq
	shlq	$4, %rax
	addq	24(%rdi), %rax
	cmpl	$0, 8(%rax)
	jne	.LBB13_20
	jmp	.LBB13_17
	.p2align	4, 0x90
.LBB13_10:                              #   in Loop: Header=BB13_8 Depth=1
	cvtsi2sdl	%ebx, %xmm0
	testl	%ebx, %ebx
	movq	%r9, %rax
	je	.LBB13_12
# BB#11:                                #   in Loop: Header=BB13_8 Depth=1
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%rdi), %cl
	movl	$1, %esi
	shll	%cl, %esi
	decl	%esi
	orl	$1, %esi
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%r9,%rax,8), %rax
	.p2align	4, 0x90
.LBB13_12:                              # %hashnum.exit.i.i
                                        #   Parent Loop BB13_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$3, 24(%rax)
	jne	.LBB13_14
# BB#13:                                #   in Loop: Header=BB13_12 Depth=2
	movsd	16(%rax), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB13_14
	jnp	.LBB13_16
.LBB13_14:                              #   in Loop: Header=BB13_12 Depth=2
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB13_12
# BB#15:                                #   in Loop: Header=BB13_8 Depth=1
	movl	$luaO_nilobject_, %eax
.LBB13_16:                              # %luaH_getnum.exit.i
                                        #   in Loop: Header=BB13_8 Depth=1
	cmpl	$0, 8(%rax)
	je	.LBB13_17
.LBB13_20:                              #   in Loop: Header=BB13_8 Depth=1
	leal	(%rbx,%rbx), %eax
	cmpl	$2147483646, %eax       # imm = 0x7FFFFFFE
	movl	%ebx, %r10d
	jb	.LBB13_8
# BB#21:                                # %.preheader33.i.preheader
	movl	$1, %esi
	.p2align	4, 0x90
.LBB13_22:                              # %.preheader33.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_26 Depth 2
	leal	-1(%rsi), %r10d
	cmpl	%r8d, %r10d
	jae	.LBB13_24
# BB#23:                                #   in Loop: Header=BB13_22 Depth=1
	movslq	%r10d, %rax
	shlq	$4, %rax
	addq	24(%rdi), %rax
	jmp	.LBB13_30
	.p2align	4, 0x90
.LBB13_24:                              #   in Loop: Header=BB13_22 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	testl	%esi, %esi
	movq	%r9, %rax
	je	.LBB13_26
# BB#25:                                #   in Loop: Header=BB13_22 Depth=1
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%rdi), %cl
	movl	$1, %ebx
	shll	%cl, %ebx
	decl	%ebx
	orl	$1, %ebx
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%ebx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%r9,%rax,8), %rax
	.p2align	4, 0x90
.LBB13_26:                              # %hashnum.exit.i30.i
                                        #   Parent Loop BB13_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$3, 24(%rax)
	jne	.LBB13_28
# BB#27:                                #   in Loop: Header=BB13_26 Depth=2
	movsd	16(%rax), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB13_28
	jnp	.LBB13_30
.LBB13_28:                              #   in Loop: Header=BB13_26 Depth=2
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB13_26
# BB#29:                                #   in Loop: Header=BB13_22 Depth=1
	movl	$luaO_nilobject_, %eax
.LBB13_30:                              # %luaH_getnum.exit32.i
                                        #   in Loop: Header=BB13_22 Depth=1
	incl	%esi
	cmpl	$0, 8(%rax)
	jne	.LBB13_22
	jmp	.LBB13_38
.LBB13_6:
	movl	%r8d, %r10d
	jmp	.LBB13_38
.LBB13_2:                               # %.preheader
	xorl	%r10d, %r10d
	cmpl	$2, %r8d
	jb	.LBB13_38
# BB#3:                                 # %.lr.ph.preheader
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB13_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	(%r10,%r8), %ecx
	shrl	%ecx
	leal	-1(%rcx), %edx
	shlq	$4, %rdx
	cmpl	$0, 8(%rax,%rdx)
	cmovel	%ecx, %r8d
	cmovnel	%ecx, %r10d
	movl	%r8d, %ecx
	subl	%r10d, %ecx
	cmpl	$1, %ecx
	ja	.LBB13_4
	jmp	.LBB13_38
.LBB13_17:                              # %.preheader.i
	movl	%ebx, %eax
	subl	%r10d, %eax
	cmpl	$2, %eax
	jb	.LBB13_38
	.p2align	4, 0x90
.LBB13_18:                              # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_33 Depth 2
	leal	(%rbx,%r10), %r11d
	shrl	%r11d
	leal	-1(%r11), %eax
	cmpl	%r8d, %eax
	jae	.LBB13_31
# BB#19:                                #   in Loop: Header=BB13_18 Depth=1
	cltq
	shlq	$4, %rax
	addq	24(%rdi), %rax
	jmp	.LBB13_37
	.p2align	4, 0x90
.LBB13_31:                              #   in Loop: Header=BB13_18 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r11d, %xmm0
	testl	%r11d, %r11d
	movq	%r9, %rax
	je	.LBB13_33
# BB#32:                                #   in Loop: Header=BB13_18 Depth=1
	movd	%xmm0, %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	addl	%ecx, %eax
	movb	11(%rdi), %cl
	movl	$1, %esi
	shll	%cl, %esi
	decl	%esi
	orl	$1, %esi
	xorl	%edx, %edx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	divl	%esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%r9,%rax,8), %rax
	.p2align	4, 0x90
.LBB13_33:                              # %hashnum.exit.i26.i
                                        #   Parent Loop BB13_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$3, 24(%rax)
	jne	.LBB13_35
# BB#34:                                #   in Loop: Header=BB13_33 Depth=2
	movsd	16(%rax), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB13_35
	jnp	.LBB13_37
.LBB13_35:                              #   in Loop: Header=BB13_33 Depth=2
	movq	32(%rax), %rax
	testq	%rax, %rax
	jne	.LBB13_33
# BB#36:                                #   in Loop: Header=BB13_18 Depth=1
	movl	$luaO_nilobject_, %eax
.LBB13_37:                              # %luaH_getnum.exit28.i
                                        #   in Loop: Header=BB13_18 Depth=1
	cmpl	$0, 8(%rax)
	cmovel	%r11d, %ebx
	cmovnel	%r11d, %r10d
	movl	%ebx, %eax
	subl	%r10d, %eax
	cmpl	$1, %eax
	ja	.LBB13_18
.LBB13_38:                              # %unbound_search.exit
	movl	%r10d, %eax
	popq	%rbx
	retq
.Lfunc_end13:
	.size	luaH_getn, .Lfunc_end13-luaH_getn
	.cfi_endproc

	.type	dummynode_,@object      # @dummynode_
	.section	.rodata,"a",@progbits
	.p2align	3
dummynode_:
	.zero	40
	.size	dummynode_, 40

	.hidden	luaO_nilobject_
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"table index is nil"
	.size	.L.str, 19

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"table index is NaN"
	.size	.L.str.1, 19

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"invalid key to 'next'"
	.size	.L.str.2, 22

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"table overflow"
	.size	.L.str.3, 15

	.hidden	luaM_realloc_
	.hidden	luaC_link
	.hidden	luaO_rawequalObj
	.hidden	luaG_runerror
	.hidden	luaM_toobig
	.hidden	luaO_log2
	.hidden	luaC_barrierback

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
