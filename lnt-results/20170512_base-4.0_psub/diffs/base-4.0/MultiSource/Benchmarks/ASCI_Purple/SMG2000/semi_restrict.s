	.text
	.file	"semi_restrict.bc"
	.globl	hypre_SemiRestrictCreate
	.p2align	4, 0x90
	.type	hypre_SemiRestrictCreate,@function
hypre_SemiRestrictCreate:               # @hypre_SemiRestrictCreate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$56, %esi
	callq	hypre_CAlloc
	movq	%rax, %rbx
	movl	$.L.str, %edi
	callq	hypre_InitializeTiming
	movl	%eax, 48(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	hypre_SemiRestrictCreate, .Lfunc_end0-hypre_SemiRestrictCreate
	.cfi_endproc

	.globl	hypre_SemiRestrictSetup
	.p2align	4, 0x90
	.type	hypre_SemiRestrictSetup,@function
hypre_SemiRestrictSetup:                # @hypre_SemiRestrictSetup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 128
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rcx, %r13
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movq	128(%rsp), %rbp
	movq	136(%rsp), %r15
	movq	8(%r13), %r12
	movq	24(%rsi), %rsi
	leaq	8(%rsp), %rax
	leaq	16(%rsp), %r10
	leaq	32(%rsp), %rdx
	leaq	24(%rsp), %rcx
	leaq	64(%rsp), %r8
	leaq	56(%rsp), %r9
	movq	%r12, %rdi
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	hypre_CreateComputeInfo
	addq	$16, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -16
	movq	32(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	24(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	16(%rsp), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	8(%rsp), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	hypre_ProjectBoxArrayArray
	movq	32(%rsp), %rdi
	movq	24(%rsp), %rsi
	movq	64(%rsp), %r8
	movq	56(%rsp), %r9
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	leaq	56(%rsp), %rax
	movq	%r15, %rdx
	movq	%r15, %rcx
	pushq	%rax
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	16(%r13)
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	hypre_ComputePkgCreate
	addq	$64, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -64
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	hypre_StructMatrixRef
	movq	%rax, (%rbx)
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 8(%rbx)
	movq	48(%rsp), %rax
	movq	%rax, 16(%rbx)
	movl	(%r14), %eax
	movl	%eax, 24(%rbx)
	movl	4(%r14), %eax
	movl	%eax, 28(%rbx)
	movl	8(%r14), %eax
	movl	%eax, 32(%rbx)
	movl	(%r15), %eax
	movl	%eax, 36(%rbx)
	movl	4(%r15), %eax
	movl	%eax, 40(%rbx)
	movl	8(%r15), %eax
	movl	%eax, 44(%rbx)
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_SemiRestrictSetup, .Lfunc_end1-hypre_SemiRestrictSetup
	.cfi_endproc

	.globl	hypre_SemiRestrict
	.p2align	4, 0x90
	.type	hypre_SemiRestrict,@function
hypre_SemiRestrict:                     # @hypre_SemiRestrict
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$504, %rsp              # imm = 0x1F8
.Lcfi33:
	.cfi_def_cfa_offset 560
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	48(%rbx), %edi
	callq	hypre_BeginTiming
	movl	8(%rbx), %eax
	movl	%eax, 188(%rsp)         # 4-byte Spill
	movq	16(%rbx), %rcx
	leaq	24(%rbx), %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	leaq	36(%rbx), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movq	%rbp, 368(%rsp)         # 8-byte Spill
	movq	24(%rbp), %rax
	movq	(%rax), %r12
	movq	8(%r14), %rax
	movq	16(%rax), %rbp
	movq	8(%r15), %rax
	movq	8(%rax), %rdx
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	movq	16(%rax), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	leaq	8(%rcx), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	leaq	16(%rcx), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
                                        # implicit-def: %RAX
	movq	%rax, 240(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%r15, 352(%rsp)         # 8-byte Spill
	movq	%r14, 344(%rsp)         # 8-byte Spill
	movq	%r12, 336(%rsp)         # 8-byte Spill
	movq	%rbp, 328(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
                                        #       Child Loop BB2_9 Depth 3
                                        #       Child Loop BB2_15 Depth 3
                                        #         Child Loop BB2_19 Depth 4
                                        #           Child Loop BB2_21 Depth 5
                                        #             Child Loop BB2_34 Depth 6
                                        #             Child Loop BB2_24 Depth 6
	cmpl	$1, %eax
	movl	%eax, 184(%rsp)         # 4-byte Spill
	je	.LBB2_4
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	testl	%eax, %eax
	jne	.LBB2_6
# BB#3:                                 #   in Loop: Header=BB2_1 Depth=1
	movq	24(%r14), %rsi
	movq	320(%rsp), %rdi         # 8-byte Reload
	leaq	496(%rsp), %rdx
	callq	hypre_InitializeIndtComputations
	movq	312(%rsp), %rax         # 8-byte Reload
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_1 Depth=1
	movq	496(%rsp), %rdi
	callq	hypre_FinalizeIndtComputations
	movq	304(%rsp), %rax         # 8-byte Reload
.LBB2_5:                                # %.sink.split
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	(%rax), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
.LBB2_6:                                #   in Loop: Header=BB2_1 Depth=1
	movq	232(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %r8d
	testl	%r8d, %r8d
	jle	.LBB2_42
# BB#7:                                 # %.preheader488.lr.ph
                                        #   in Loop: Header=BB2_1 Depth=1
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_8:                                # %.preheader488
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_9 Depth 3
                                        #       Child Loop BB2_15 Depth 3
                                        #         Child Loop BB2_19 Depth 4
                                        #           Child Loop BB2_21 Depth 5
                                        #             Child Loop BB2_34 Depth 6
                                        #             Child Loop BB2_24 Depth 6
	movq	360(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r11,4), %ecx
	movslq	%edx, %rdx
	leaq	-1(%rdx,%rdx,2), %r9
	leaq	-1(%rdx), %rbx
	.p2align	4, 0x90
.LBB2_9:                                #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbx, %rsi
	leaq	1(%rsi), %rbx
	addq	$3, %r9
	cmpl	%ecx, 4(%rbp,%rsi,4)
	jne	.LBB2_9
# BB#10:                                #   in Loop: Header=BB2_8 Depth=2
	cmpl	$0, 188(%rsp)           # 4-byte Folded Reload
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rcx
	movq	8(%rcx,%rsi,8), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rdx
	movq	48(%rax), %r10
	movq	(%rdx), %rcx
	movq	16(%r14), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	16(%r15), %rdx
	movq	(%rdx), %rdx
	movq	64(%rax), %rdi
	movq	8(%rdi,%rsi,8), %rdi
	movq	%rcx, 256(%rsp)         # 8-byte Spill
	movq	%rbx, 384(%rsp)         # 8-byte Spill
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	je	.LBB2_12
# BB#11:                                #   in Loop: Header=BB2_8 Depth=2
	movl	-4(%rcx,%r9,8), %eax
	movl	-16(%rcx,%r9,8), %ebx
	movq	%r10, %r13
	movq	%r14, %r10
	movq	%r15, %r14
	movl	%r8d, %r15d
	movq	%rcx, %r8
	movl	%eax, %ecx
	subl	%ebx, %ecx
	incl	%ecx
	cmpl	%ebx, %eax
	movl	(%r8,%r9,8), %edx
	movl	-12(%r8,%r9,8), %ebx
	movl	%r15d, %r8d
	movq	%r14, %r15
	movq	%r10, %r14
	movq	%r13, %r10
	movl	$0, %r13d
	cmovsl	%r13d, %ecx
	movl	%edx, %eax
	subl	%ebx, %eax
	incl	%eax
	cmpl	%ebx, %edx
	movslq	4(%rdi), %rdx
	leaq	(%r10,%rdx,8), %rbx
	movslq	12(%r12), %rdx
	cmovsl	%r13d, %eax
	imull	20(%r12), %eax
	addl	16(%r12), %eax
	imull	%ecx, %eax
	cltq
	addq	%rdx, %rax
	shlq	$3, %rax
	subq	%rax, %rbx
	movq	%rbx, (%rsp)            # 8-byte Spill
	jmp	.LBB2_13
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_8 Depth=2
	movslq	(%rdi), %rax
	leaq	(%r10,%rax,8), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	addq	$4, %rdi
.LBB2_13:                               #   in Loop: Header=BB2_8 Depth=2
	movq	160(%rsp), %rbx         # 8-byte Reload
	movl	-4(%rbx,%r9,8), %ecx
	movl	-16(%rbx,%r9,8), %edx
	movl	%ecx, %eax
	subl	%edx, %eax
	incl	%eax
	cmpl	%edx, %ecx
	movl	(%rbx,%r9,8), %ecx
	movl	-12(%rbx,%r9,8), %edx
	cmovsl	%r13d, %eax
	movl	%ecx, %ebx
	subl	%edx, %ebx
	incl	%ebx
	cmpl	%edx, %ecx
	cmovsl	%r13d, %ebx
	movq	152(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, 8(%rcx)
	jle	.LBB2_41
# BB#14:                                # %.lr.ph539
                                        #   in Loop: Header=BB2_8 Depth=2
	movslq	(%rdi), %r8
	movq	24(%r14), %rdi
	movq	40(%r14), %rdx
	movslq	4(%rdx,%rsi,4), %rsi
	movslq	(%r12), %rdx
	addq	%r9, %r9
	movq	%r9, 168(%rsp)          # 8-byte Spill
	movl	8(%r12), %ecx
	imull	%ebx, %ecx
	addl	4(%r12), %ecx
	imull	%eax, %ecx
	movslq	%ecx, %r13
	addq	%rdx, %r13
	movslq	12(%r12), %rcx
	imull	20(%r12), %ebx
	addl	16(%r12), %ebx
	imull	%eax, %ebx
	movslq	%ebx, %r14
	addq	%rcx, %r14
	movq	24(%r15), %rcx
	movq	40(%r15), %rdx
	movslq	(%rdx,%r11,4), %rdx
	movq	%r11, 376(%rsp)         # 8-byte Spill
	leaq	(%r11,%r11,2), %rbp
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbp,8), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rdx,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%rsi,%r14), %rcx
	leaq	(%rdi,%rcx,8), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	leaq	(%rsi,%r13), %rcx
	leaq	(%rdi,%rcx,8), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	(%rdi,%rsi,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	(%r10,%r8,8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_15:                               #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_19 Depth 4
                                        #           Child Loop BB2_21 Depth 5
                                        #             Child Loop BB2_34 Depth 6
                                        #             Child Loop BB2_24 Depth 6
	movq	(%rcx), %rcx
	movq	%rdx, 416(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rdx,2), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rax,8), %rbp
	movq	%rbp, %rdi
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	400(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rdx
	leaq	280(%rsp), %rcx
	callq	hypre_StructMapFineToCoarse
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	leaq	292(%rsp), %rdx
	callq	hypre_BoxGetStrideSize
	movq	248(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %r8d
	movl	12(%rcx), %eax
	movl	%eax, %esi
	subl	%r8d, %esi
	incl	%esi
	cmpl	%r8d, %eax
	movl	4(%rcx), %edi
	movl	16(%rcx), %eax
	movl	$0, %edx
	cmovsl	%edx, %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%eax, %r9d
	subl	%edi, %r9d
	incl	%r9d
	movl	%edi, 56(%rsp)          # 4-byte Spill
	cmpl	%edi, %eax
	movq	168(%rsp), %rdi         # 8-byte Reload
	movq	160(%rsp), %rbp         # 8-byte Reload
	movl	-16(%rbp,%rdi,4), %eax
	movl	-4(%rbp,%rdi,4), %ecx
	cmovsl	%edx, %r9d
	movl	%ecx, %r10d
	subl	%eax, %r10d
	incl	%r10d
	cmpl	%eax, %ecx
	movl	-12(%rbp,%rdi,4), %esi
	movl	(%rbp,%rdi,4), %ecx
	cmovsl	%edx, %r10d
	movl	%ecx, %r11d
	subl	%esi, %r11d
	incl	%r11d
	movl	%esi, 16(%rsp)          # 4-byte Spill
	cmpl	%esi, %ecx
	movq	256(%rsp), %rbp         # 8-byte Reload
	movl	-16(%rbp,%rdi,4), %ebx
	movl	-4(%rbp,%rdi,4), %esi
	cmovsl	%edx, %r11d
	movl	%esi, %ecx
	subl	%ebx, %ecx
	incl	%ecx
	cmpl	%ebx, %esi
	movl	-12(%rbp,%rdi,4), %esi
	movl	(%rbp,%rdi,4), %edi
	cmovsl	%edx, %ecx
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	movl	%edi, %ebp
	subl	%esi, %ebp
	incl	%ebp
	cmpl	%esi, %edi
	cmovsl	%edx, %ebp
	movl	292(%rsp), %ecx
	movl	296(%rsp), %edx
	cmpl	%ecx, %edx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	cmovgel	%edx, %ecx
	movl	300(%rsp), %edx
	cmpl	%ecx, %edx
	movl	%edx, 116(%rsp)         # 4-byte Spill
	cmovgel	%edx, %ecx
	testl	%ecx, %ecx
	jle	.LBB2_39
# BB#16:                                # %.lr.ph533
                                        #   in Loop: Header=BB2_15 Depth=3
	cmpl	$0, 116(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_39
# BB#17:                                # %.lr.ph533
                                        #   in Loop: Header=BB2_15 Depth=3
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_39
# BB#18:                                # %.preheader487.us.preheader
                                        #   in Loop: Header=BB2_15 Depth=3
	movq	144(%rsp), %rcx         # 8-byte Reload
	movslq	36(%rcx), %rdi
	movl	40(%rcx), %r15d
	imull	%r10d, %r15d
	movq	120(%rsp), %r12         # 8-byte Reload
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	%r12d, %eax
	imull	%edi, %eax
	movl	%r15d, %ecx
	movl	%r15d, 76(%rsp)         # 4-byte Spill
	subl	%eax, %ecx
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	movq	128(%rsp), %rdx         # 8-byte Reload
	leal	-1(%rdx), %edx
	imull	%edx, %ecx
	addl	%r15d, %ecx
	subl	%eax, %ecx
	movl	%ecx, 224(%rsp)         # 4-byte Spill
	movl	44(%rsp), %r15d         # 4-byte Reload
	subl	%r12d, %r15d
	movl	%ebx, 96(%rsp)          # 4-byte Spill
	movl	12(%rsp), %r8d          # 4-byte Reload
	subl	%r12d, %r8d
	imull	%edx, %r15d
	imull	%edx, %r8d
	movl	280(%rsp), %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movl	%esi, 48(%rsp)          # 4-byte Spill
	movl	%eax, %esi
	subl	8(%rsp), %esi           # 4-byte Folded Reload
	movl	284(%rsp), %ebx
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	subl	56(%rsp), %ebx          # 4-byte Folded Reload
	movl	288(%rsp), %edx
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%edx, %r12d
	movq	248(%rsp), %rcx         # 8-byte Reload
	subl	8(%rcx), %r12d
	imull	%r9d, %r12d
	addl	%ebx, %r12d
	imull	12(%rsp), %r12d         # 4-byte Folded Reload
	addl	%esi, %r12d
	movl	%r12d, 40(%rsp)         # 4-byte Spill
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	subl	64(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	4(%rcx,%rax,8), %r12d
	subl	16(%rsp), %r12d         # 4-byte Folded Reload
	movl	8(%rcx,%rax,8), %esi
	movl	%r10d, %eax
	imull	%r11d, %eax
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %rbx         # 8-byte Reload
	subl	-8(%rcx,%rbx,4), %esi
	imull	%r11d, %esi
	addl	%r12d, %esi
	imull	%r10d, %esi
	addl	80(%rsp), %esi          # 4-byte Folded Reload
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movq	144(%rsp), %rcx         # 8-byte Reload
	imull	44(%rcx), %eax
	movl	72(%rsp), %r10d         # 4-byte Reload
	subl	96(%rsp), %r10d         # 4-byte Folded Reload
	movl	%ebp, %ecx
	movq	128(%rsp), %rbx         # 8-byte Reload
	subl	%ebx, %ecx
	movl	44(%rsp), %esi          # 4-byte Reload
	imull	%esi, %ecx
	movl	%ecx, 208(%rsp)         # 4-byte Spill
	movl	8(%rsp), %r11d          # 4-byte Reload
	subl	48(%rsp), %r11d         # 4-byte Folded Reload
	movl	%ebx, %ecx
	imull	76(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 200(%rsp)         # 4-byte Spill
	subl	%ecx, %eax
	movl	%eax, 212(%rsp)         # 4-byte Spill
	subl	%ebx, %r9d
	movl	12(%rsp), %ecx          # 4-byte Reload
	imull	%ecx, %r9d
	movl	%r9d, 204(%rsp)         # 4-byte Spill
	addl	%esi, %r15d
	movq	120(%rsp), %rax         # 8-byte Reload
	subl	%eax, %r15d
	movl	%r15d, 220(%rsp)        # 4-byte Spill
	addl	%ecx, %r8d
	movl	%ecx, %r9d
	subl	%eax, %r8d
	movq	%rax, %r15
	movl	%r8d, 216(%rsp)         # 4-byte Spill
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	subl	-8(%rax,%rcx,4), %edx
	imull	%ebp, %edx
	leal	-1(%r15), %eax
	addl	%r11d, %edx
	movl	%esi, %ecx
	movl	%esi, %r8d
	imull	%ebx, %ecx
	movl	%ecx, 196(%rsp)         # 4-byte Spill
	imull	%r8d, %edx
	addl	%r10d, %edx
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movl	%r9d, %ecx
	imull	%ebx, %ecx
	movl	%ecx, 192(%rsp)         # 4-byte Spill
	leaq	1(%rax), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rcx, %rdx
	movabsq	$8589934590, %rcx       # imm = 0x1FFFFFFFE
	andq	%rcx, %rdx
	setne	%cl
	cmpl	$1, %edi
	sete	%bl
	andb	%cl, %bl
	movb	%bl, 176(%rsp)          # 1-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rcx
	movq	%rcx, 488(%rsp)         # 8-byte Spill
	movq	272(%rsp), %rcx         # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rcx
	movq	%rcx, 480(%rsp)         # 8-byte Spill
	movq	264(%rsp), %rcx         # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rcx
	movq	%rcx, 472(%rsp)         # 8-byte Spill
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rcx
	movq	%rcx, 464(%rsp)         # 8-byte Spill
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	8(%rcx,%rax,8), %rcx
	movq	%rcx, 456(%rsp)         # 8-byte Spill
	movq	392(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movq	%rdi, %rax
	movq	%rdx, 440(%rsp)         # 8-byte Spill
	imulq	%rdx, %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	movq	%rdi, %rax
	shlq	$4, %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	shlq	$3, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_19:                               # %.preheader487.us
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB2_21 Depth 5
                                        #             Child Loop BB2_34 Depth 6
                                        #             Child Loop BB2_24 Depth 6
	cmpl	$0, 120(%rsp)           # 4-byte Folded Reload
	movl	216(%rsp), %eax         # 4-byte Reload
	movl	224(%rsp), %ecx         # 4-byte Reload
	movl	220(%rsp), %edx         # 4-byte Reload
	jle	.LBB2_38
# BB#20:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB2_19 Depth=4
	movl	%esi, 228(%rsp)         # 4-byte Spill
	xorl	%r15d, %r15d
	movl	32(%rsp), %r9d          # 4-byte Reload
	movl	40(%rsp), %r8d          # 4-byte Reload
	movl	36(%rsp), %r10d         # 4-byte Reload
	movq	(%rsp), %r12            # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	%r12, (%rsp)            # 8-byte Spill
	jmp	.LBB2_21
.LBB2_31:                               #   in Loop: Header=BB2_21 Depth=5
	movq	48(%rsp), %rdx          # 8-byte Reload
	xorl	%ebx, %ebx
	movq	(%rsp), %r12            # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB2_23
.LBB2_27:                               #   in Loop: Header=BB2_21 Depth=5
	movq	48(%rsp), %rdx          # 8-byte Reload
	xorl	%ebx, %ebx
	movq	(%rsp), %r12            # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	jmp	.LBB2_23
	.p2align	4, 0x90
.LBB2_21:                               # %.preheader.us.us
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        #         Parent Loop BB2_19 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB2_34 Depth 6
                                        #             Child Loop BB2_24 Depth 6
	movslq	%r10d, %rbp
	movslq	%r8d, %rax
	movslq	%r9d, %rdx
	cmpq	$1, 80(%rsp)            # 8-byte Folded Reload
	jbe	.LBB2_22
# BB#25:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_21 Depth=5
	cmpb	$0, 176(%rsp)           # 1-byte Folded Reload
	je	.LBB2_22
# BB#26:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_21 Depth=5
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movl	76(%rsp), %eax          # 4-byte Reload
	imull	%r15d, %eax
	addl	32(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %rsi
	movl	12(%rsp), %ecx          # 4-byte Reload
	imull	%r15d, %ecx
	addl	40(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	leaq	(%r11,%rcx,8), %rbp
	movq	488(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rdx
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %r11
	movq	480(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rcx
	movq	264(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	472(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rbx
	movl	44(%rsp), %r12d         # 4-byte Reload
	imull	%r15d, %r12d
	addl	36(%rsp), %r12d         # 4-byte Folded Reload
	cmpq	%rcx, %rbp
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	sbbb	%al, %al
	cmpq	%rdx, %r11
	sbbb	%r11b, %r11b
	andb	%al, %r11b
	movq	464(%rsp), %rcx         # 8-byte Reload
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rsi,8), %rax
	movslq	%r12d, %rsi
	cmpq	%rbx, %rbp
	sbbb	%bl, %bl
	cmpq	%rdx, 64(%rsp)          # 8-byte Folded Reload
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	sbbb	%cl, %cl
	movb	%cl, 8(%rsp)            # 1-byte Spill
	cmpq	%rax, %rbp
	sbbb	%cl, %cl
	cmpq	%rdx, 96(%rsp)          # 8-byte Folded Reload
	movq	456(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %r12
	sbbb	%al, %al
	movb	%al, 96(%rsp)           # 1-byte Spill
	cmpq	%r12, %rbp
	sbbb	%r12b, %r12b
	cmpq	%rdx, 64(%rsp)          # 8-byte Folded Reload
	movq	448(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	sbbb	%al, %al
	movb	%al, 72(%rsp)           # 1-byte Spill
	cmpq	64(%rsp), %rbp          # 8-byte Folded Reload
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%rax,%rsi,8), %rsi
	sbbb	%al, %al
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	testb	$1, %r11b
	jne	.LBB2_27
# BB#28:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_21 Depth=5
	andb	8(%rsp), %bl            # 1-byte Folded Reload
	andb	$1, %bl
	jne	.LBB2_27
# BB#29:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_21 Depth=5
	andb	96(%rsp), %cl           # 1-byte Folded Reload
	andb	$1, %cl
	jne	.LBB2_27
# BB#30:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_21 Depth=5
	andb	72(%rsp), %r12b         # 1-byte Folded Reload
	andb	$1, %r12b
	movq	56(%rsp), %rbp          # 8-byte Reload
	jne	.LBB2_31
# BB#32:                                # %vector.memcheck
                                        #   in Loop: Header=BB2_21 Depth=5
	andb	%dl, %al
	andb	$1, %al
	jne	.LBB2_31
# BB#33:                                # %vector.body.preheader
                                        #   in Loop: Header=BB2_21 Depth=5
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,8), %rdx
	movq	(%rsp), %r12            # 8-byte Reload
	leaq	(%r12,%rbp,8), %r11
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, %rsi
	movq	16(%rsp), %rbx          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rbp
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rcx
	movq	440(%rsp), %rax         # 8-byte Reload
	addq	%rax, %rbx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	104(%rsp), %rbx         # 8-byte Reload
	addq	432(%rsp), %rbx         # 8-byte Folded Reload
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	addq	%rax, %rsi
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movq	424(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_34:                               # %vector.body
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        #         Parent Loop BB2_19 Depth=4
                                        #           Parent Loop BB2_21 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movupd	(%rcx), %xmm0
	movupd	(%r11,%rsi,8), %xmm1
	movupd	(%rcx,%r13,8), %xmm2
	mulpd	%xmm1, %xmm2
	movupd	(%rdx,%rsi,8), %xmm1
	movupd	(%rcx,%r14,8), %xmm3
	mulpd	%xmm1, %xmm3
	addpd	%xmm2, %xmm3
	addpd	%xmm0, %xmm3
	movupd	%xmm3, (%rbp,%rsi,8)
	addq	$2, %rsi
	addq	%rbx, %rcx
	cmpq	%rsi, %rax
	jne	.LBB2_34
# BB#35:                                # %middle.block
                                        #   in Loop: Header=BB2_21 Depth=5
	cmpq	%rax, 80(%rsp)          # 8-byte Folded Reload
	movl	%eax, %ebx
	movq	24(%rsp), %r11          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	104(%rsp), %rdx         # 8-byte Reload
	jne	.LBB2_23
	jmp	.LBB2_36
	.p2align	4, 0x90
.LBB2_22:                               #   in Loop: Header=BB2_21 Depth=5
	xorl	%ebx, %ebx
.LBB2_23:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_21 Depth=5
	leaq	(%r11,%rax,8), %rcx
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rax
	leaq	(%r12,%rbp,8), %rdx
	movq	136(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rbp,8), %rbp
	movq	120(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill> %RSI<def>
	subl	%ebx, %esi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_24:                               # %scalar.ph
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_8 Depth=2
                                        #       Parent Loop BB2_15 Depth=3
                                        #         Parent Loop BB2_19 Depth=4
                                        #           Parent Loop BB2_21 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movsd	(%rdx,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rax,%r13,8), %xmm0
	movsd	(%rbp,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rax,%r14,8), %xmm1
	addsd	%xmm0, %xmm1
	addsd	(%rax), %xmm1
	movsd	%xmm1, (%rcx,%rbx,8)
	incq	%rbx
	addq	%rdi, %rax
	cmpl	%ebx, %esi
	jne	.LBB2_24
.LBB2_36:                               # %._crit_edge.us.us
                                        #   in Loop: Header=BB2_21 Depth=5
	addl	44(%rsp), %r10d         # 4-byte Folded Reload
	addl	76(%rsp), %r9d          # 4-byte Folded Reload
	addl	12(%rsp), %r8d          # 4-byte Folded Reload
	incl	%r15d
	cmpl	128(%rsp), %r15d        # 4-byte Folded Reload
	jne	.LBB2_21
# BB#37:                                #   in Loop: Header=BB2_19 Depth=4
	movl	192(%rsp), %eax         # 4-byte Reload
	movl	200(%rsp), %ecx         # 4-byte Reload
	movl	196(%rsp), %edx         # 4-byte Reload
	movl	228(%rsp), %esi         # 4-byte Reload
.LBB2_38:                               # %._crit_edge500.us
                                        #   in Loop: Header=BB2_19 Depth=4
	addl	36(%rsp), %edx          # 4-byte Folded Reload
	addl	32(%rsp), %ecx          # 4-byte Folded Reload
	addl	40(%rsp), %eax          # 4-byte Folded Reload
	addl	208(%rsp), %edx         # 4-byte Folded Reload
	addl	212(%rsp), %ecx         # 4-byte Folded Reload
	addl	204(%rsp), %eax         # 4-byte Folded Reload
	incl	%esi
	cmpl	116(%rsp), %esi         # 4-byte Folded Reload
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movl	%eax, 40(%rsp)          # 4-byte Spill
	jne	.LBB2_19
.LBB2_39:                               # %._crit_edge534
                                        #   in Loop: Header=BB2_15 Depth=3
	movq	416(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	152(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %rdx
	jl	.LBB2_15
# BB#40:                                # %._crit_edge540.loopexit
                                        #   in Loop: Header=BB2_8 Depth=2
	movq	232(%rsp), %rax         # 8-byte Reload
	movl	8(%rax), %r8d
	movq	352(%rsp), %r15         # 8-byte Reload
	movq	344(%rsp), %r14         # 8-byte Reload
	movq	336(%rsp), %r12         # 8-byte Reload
	movq	328(%rsp), %rbp         # 8-byte Reload
	xorl	%r13d, %r13d
	movq	376(%rsp), %r11         # 8-byte Reload
.LBB2_41:                               # %._crit_edge540
                                        #   in Loop: Header=BB2_8 Depth=2
	incq	%r11
	movslq	%r8d, %rax
	cmpq	%rax, %r11
	movq	384(%rsp), %rdx         # 8-byte Reload
	jl	.LBB2_8
.LBB2_42:                               # %._crit_edge547
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	184(%rsp), %eax         # 4-byte Reload
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB2_1
# BB#43:
	movl	72(%r15), %edi
	shll	$2, %edi
	callq	hypre_IncFLOPCount
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	48(%rax), %edi
	callq	hypre_EndTiming
	xorl	%eax, %eax
	addq	$504, %rsp              # imm = 0x1F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_SemiRestrict, .Lfunc_end2-hypre_SemiRestrict
	.cfi_endproc

	.globl	hypre_SemiRestrictDestroy
	.p2align	4, 0x90
	.type	hypre_SemiRestrictDestroy,@function
hypre_SemiRestrictDestroy:              # @hypre_SemiRestrictDestroy
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_2
# BB#1:
	movq	(%rbx), %rdi
	callq	hypre_StructMatrixDestroy
	movq	16(%rbx), %rdi
	callq	hypre_ComputePkgDestroy
	movl	48(%rbx), %edi
	callq	hypre_FinalizeTiming
	movq	%rbx, %rdi
	callq	hypre_Free
.LBB3_2:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	hypre_SemiRestrictDestroy, .Lfunc_end3-hypre_SemiRestrictDestroy
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SemiRestrict"
	.size	.L.str, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
