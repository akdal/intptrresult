	.text
	.file	"OutMemStream.bc"
	.globl	_ZN13COutMemStream4FreeEv
	.p2align	4, 0x90
	.type	_ZN13COutMemStream4FreeEv,@function
_ZN13COutMemStream4FreeEv:              # @_ZN13COutMemStream4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	104(%rbx), %rdi
	movq	16(%rbx), %rsi
	callq	_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt
	movb	$1, 144(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN13COutMemStream4FreeEv, .Lfunc_end0-_ZN13COutMemStream4FreeEv
	.cfi_endproc

	.globl	_ZN13COutMemStream4InitEv
	.p2align	4, 0x90
	.type	_ZN13COutMemStream4InitEv,@function
_ZN13COutMemStream4InitEv:              # @_ZN13COutMemStream4InitEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	80(%rbx), %rdi
	callq	pthread_mutex_lock
	movb	$0, 89(%rbx)
	movq	80(%rbx), %rdi
	callq	pthread_mutex_unlock
	movw	$0, 40(%rbx)
	leaq	104(%rbx), %rdi
	movq	16(%rbx), %rsi
	callq	_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt
	movb	$1, 144(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN13COutMemStream4InitEv, .Lfunc_end1-_ZN13COutMemStream4InitEv
	.cfi_endproc

	.globl	_ZN13COutMemStream10DetachDataER14CMemLockBlocks
	.p2align	4, 0x90
	.type	_ZN13COutMemStream10DetachDataER14CMemLockBlocks,@function
_ZN13COutMemStream10DetachDataER14CMemLockBlocks: # @_ZN13COutMemStream10DetachDataER14CMemLockBlocks
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 32
.Lcfi7:
	.cfi_offset %rbx, -24
.Lcfi8:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	104(%rbx), %r14
	movq	16(%rbx), %rdx
	movq	%r14, %rdi
	callq	_ZN14CMemLockBlocks6DetachERS_P18CMemBlockManagerMt
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt
	movb	$1, 144(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	_ZN13COutMemStream10DetachDataER14CMemLockBlocks, .Lfunc_end2-_ZN13COutMemStream10DetachDataER14CMemLockBlocks
	.cfi_endproc

	.globl	_ZN13COutMemStream17WriteToRealStreamEv
	.p2align	4, 0x90
	.type	_ZN13COutMemStream17WriteToRealStreamEv,@function
_ZN13COutMemStream17WriteToRealStreamEv: # @_ZN13COutMemStream17WriteToRealStreamEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	104(%rbx), %r14
	movq	16(%rbx), %rax
	movq	152(%rbx), %rdx
	movq	8(%rax), %rsi
	movq	%r14, %rdi
	callq	_ZNK10CMemBlocks13WriteToStreamEmP20ISequentialOutStream
	testl	%eax, %eax
	jne	.LBB3_2
# BB#1:
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt
	xorl	%eax, %eax
.LBB3_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN13COutMemStream17WriteToRealStreamEv, .Lfunc_end3-_ZN13COutMemStream17WriteToRealStreamEv
	.cfi_endproc

	.globl	_ZN13COutMemStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN13COutMemStream5WriteEPKvjPj,@function
_ZN13COutMemStream5WriteEPKvjPj:        # @_ZN13COutMemStream5WriteEPKvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 112
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r14
	movq	%rdi, %rbp
	cmpb	$0, 40(%rbp)
	je	.LBB4_1
# BB#24:
	movq	152(%rbp), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	%r14, %rsi
	movl	%r13d, %edx
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB4_1:
	testq	%rcx, %rcx
	je	.LBB4_3
# BB#2:
	movl	$0, (%rcx)
.LBB4_3:                                # %.preheader
	testl	%r13d, %r13d
	je	.LBB4_4
# BB#6:                                 # %.lr.ph.lr.ph
	leaq	104(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	48(%rbp), %r15
	leaq	72(%rbp), %r12
	movq	%rcx, 24(%rsp)          # 8-byte Spill
.LBB4_7:                                # %.lr.ph.split.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_15 Depth 2
	movl	%r13d, 4(%rsp)          # 4-byte Spill
	movq	24(%rbp), %rax
	cmpl	116(%rbp), %eax
	jl	.LBB4_8
	.p2align	4, 0x90
.LBB4_15:                               # %.lr.ph
                                        #   Parent Loop BB4_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, 32(%rsp)
	movq	%r12, 40(%rsp)
	movq	16(%rbp), %rax
	addq	$64, %rax
	movq	%rax, 48(%rsp)
	movzbl	144(%rbp), %edi
	orl	$2, %edi
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	32(%rsp), %rsi
	callq	_Z22WaitForMultipleObjectsjPKPN8NWindows16NSynchronization15CBaseHandleWFMOEij
	movl	$-2147467259, %r13d     # imm = 0x80004005
	cmpl	$2, %eax
	jne	.LBB4_16
# BB#22:                                #   in Loop: Header=BB4_15 Depth=2
	movq	16(%rbp), %rdi
	callq	_ZN18CMemBlockManagerMt13AllocateBlockEv
	movq	%rax, %rbx
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	120(%rbp), %rax
	movslq	116(%rbp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 116(%rbp)
	movq	120(%rbp), %rax
	cmpq	$0, (%rax,%rcx,8)
	je	.LBB4_5
# BB#23:                                # %.lr.ph.split
                                        #   in Loop: Header=BB4_15 Depth=2
	movq	24(%rbp), %rax
	cmpl	%ecx, %eax
	jg	.LBB4_15
.LBB4_8:                                # %.us-lcssa.us
                                        #   in Loop: Header=BB4_7 Depth=1
	movq	120(%rbp), %rcx
	cltq
	movq	16(%rbp), %rdx
	movq	32(%rbp), %rsi
	movq	(%rcx,%rax,8), %rdi
	addq	%rsi, %rdi
	movq	8(%rdx), %rbx
	subq	%rsi, %rbx
	movl	4(%rsp), %r13d          # 4-byte Reload
	movl	%r13d, %eax
	cmpq	%rbx, %rax
	cmovbq	%rax, %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memmove
	movq	24(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_7 Depth=1
	addl	%ebx, (%rcx)
.LBB4_10:                               # %.us-lcssa.us._crit_edge
                                        #   in Loop: Header=BB4_7 Depth=1
	movq	32(%rbp), %rdi
	addq	%rbx, %rdi
	movq	%rdi, 32(%rbp)
	movq	16(%rbp), %rdx
	movq	24(%rbp), %rax
	movq	8(%rdx), %rdx
	movq	%rdx, %rsi
	imulq	%rax, %rsi
	addq	%rdi, %rsi
	cmpq	136(%rbp), %rsi
	jbe	.LBB4_12
# BB#11:                                #   in Loop: Header=BB4_7 Depth=1
	movq	%rsi, 136(%rbp)
.LBB4_12:                               #   in Loop: Header=BB4_7 Depth=1
	subl	%ebx, %r13d
	cmpq	%rdx, %rdi
	jne	.LBB4_14
# BB#13:                                #   in Loop: Header=BB4_7 Depth=1
	incq	%rax
	movq	%rax, 24(%rbp)
	movq	$0, 32(%rbp)
.LBB4_14:                               # %.outer.backedge
                                        #   in Loop: Header=BB4_7 Depth=1
	addq	%rbx, %r14
	testl	%r13d, %r13d
	jne	.LBB4_7
.LBB4_4:
	xorl	%r13d, %r13d
.LBB4_5:                                # %.loopexit
	movl	%r13d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_16:                               # %.lr.ph
	cmpl	$1, %eax
	je	.LBB4_19
# BB#17:                                # %.lr.ph
	testl	%eax, %eax
	jne	.LBB4_5
# BB#18:                                # %.us-lcssa78.us
	movl	96(%rbp), %r13d
	jmp	.LBB4_5
.LBB4_19:                               # %.us-lcssa79.us
	movq	16(%rbp), %rax
	movq	152(%rbp), %rdx
	movq	8(%rax), %rsi
	movb	$1, 40(%rbp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZNK10CMemBlocks13WriteToStreamEmP20ISequentialOutStream
	movl	%eax, %r13d
	testl	%r13d, %r13d
	jne	.LBB4_5
# BB#20:
	movq	16(%rbp), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt
	movq	152(%rbp), %rdi
	movq	(%rdi), %rax
	leaq	20(%rsp), %rcx
	movq	%r14, %rsi
	movl	4(%rsp), %edx           # 4-byte Reload
	callq	*40(%rax)
	movl	%eax, %r13d
	movq	24(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB4_5
# BB#21:
	movl	20(%rsp), %eax
	addl	%eax, (%rcx)
	jmp	.LBB4_5
.Lfunc_end4:
	.size	_ZN13COutMemStream5WriteEPKvjPj, .Lfunc_end4-_ZN13COutMemStream5WriteEPKvjPj
	.cfi_endproc

	.globl	_ZN13COutMemStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN13COutMemStream4SeekExjPy,@function
_ZN13COutMemStream4SeekExjPy:           # @_ZN13COutMemStream4SeekExjPy
	.cfi_startproc
# BB#0:
	cmpb	$0, 40(%rdi)
	je	.LBB5_4
# BB#1:
	movq	160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#3:
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.LBB5_4:
	movl	$-2147467263, %eax      # imm = 0x80004001
	testl	%edx, %edx
	je	.LBB5_7
# BB#5:
	cmpl	$1, %edx
	jne	.LBB5_11
# BB#6:
	testq	%rsi, %rsi
	jne	.LBB5_11
	jmp	.LBB5_9
.LBB5_2:
	movl	$-2147467259, %eax      # imm = 0x80004005
	retq
.LBB5_7:
	testq	%rsi, %rsi
	jne	.LBB5_11
# BB#8:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rdi)
.LBB5_9:
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB5_11
# BB#10:
	movq	16(%rdi), %rdx
	movq	8(%rdx), %rdx
	imulq	24(%rdi), %rdx
	addq	32(%rdi), %rdx
	movq	%rdx, (%rcx)
.LBB5_11:
	retq
.Lfunc_end5:
	.size	_ZN13COutMemStream4SeekExjPy, .Lfunc_end5-_ZN13COutMemStream4SeekExjPy
	.cfi_endproc

	.globl	_ZN13COutMemStream7SetSizeEy
	.p2align	4, 0x90
	.type	_ZN13COutMemStream7SetSizeEy,@function
_ZN13COutMemStream7SetSizeEy:           # @_ZN13COutMemStream7SetSizeEy
	.cfi_startproc
# BB#0:
	cmpb	$0, 40(%rdi)
	je	.LBB6_4
# BB#1:
	movq	160(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB6_2
# BB#3:
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.LBB6_4:
	movq	%rsi, 136(%rdi)
	xorl	%eax, %eax
	retq
.LBB6_2:
	movl	$-2147467259, %eax      # imm = 0x80004005
	retq
.Lfunc_end6:
	.size	_ZN13COutMemStream7SetSizeEy, .Lfunc_end6-_ZN13COutMemStream7SetSizeEy
	.cfi_endproc

	.section	.text._ZN13COutMemStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN13COutMemStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN13COutMemStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN13COutMemStream14QueryInterfaceERK4GUIDPPv,@function
_ZN13COutMemStream14QueryInterfaceERK4GUIDPPv: # @_ZN13COutMemStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB7_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB7_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB7_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB7_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB7_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB7_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB7_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB7_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB7_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB7_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB7_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB7_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB7_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB7_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB7_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB7_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB7_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end7:
	.size	_ZN13COutMemStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end7-_ZN13COutMemStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN13COutMemStream6AddRefEv,"axG",@progbits,_ZN13COutMemStream6AddRefEv,comdat
	.weak	_ZN13COutMemStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN13COutMemStream6AddRefEv,@function
_ZN13COutMemStream6AddRefEv:            # @_ZN13COutMemStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end8:
	.size	_ZN13COutMemStream6AddRefEv, .Lfunc_end8-_ZN13COutMemStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN13COutMemStream7ReleaseEv,"axG",@progbits,_ZN13COutMemStream7ReleaseEv,comdat
	.weak	_ZN13COutMemStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN13COutMemStream7ReleaseEv,@function
_ZN13COutMemStream7ReleaseEv:           # @_ZN13COutMemStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB9_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB9_2:
	popq	%rcx
	retq
.Lfunc_end9:
	.size	_ZN13COutMemStream7ReleaseEv, .Lfunc_end9-_ZN13COutMemStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN13COutMemStreamD2Ev,"axG",@progbits,_ZN13COutMemStreamD2Ev,comdat
	.weak	_ZN13COutMemStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN13COutMemStreamD2Ev,@function
_ZN13COutMemStreamD2Ev:                 # @_ZN13COutMemStreamD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	$_ZTV13COutMemStream+16, (%rbx)
	leaq	104(%rbx), %r14
	movq	16(%rbx), %rsi
.Ltmp0:
	movq	%r14, %rdi
	callq	_ZN14CMemLockBlocks4FreeEP18CMemBlockManagerMt
.Ltmp1:
# BB#1:
	movb	$1, 144(%rbx)
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp5:
	callq	*16(%rax)
.Ltmp6:
.LBB10_3:                               # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp10:
	callq	*16(%rax)
.Ltmp11:
.LBB10_5:                               # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
.Ltmp16:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp17:
# BB#6:
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 48(%rbx)
	movq	$0, 56(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB10_13:
.Ltmp12:
	movq	%rax, %r15
	jmp	.LBB10_14
.LBB10_9:
.Ltmp7:
	movq	%rax, %r15
	jmp	.LBB10_10
.LBB10_12:
.Ltmp18:
	movq	%rax, %r15
	jmp	.LBB10_15
.LBB10_7:
.Ltmp2:
	movq	%rax, %r15
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_10
# BB#8:
	movq	(%rdi), %rax
.Ltmp3:
	callq	*16(%rax)
.Ltmp4:
.LBB10_10:                              # %_ZN9CMyComPtrI10IOutStreamED2Ev.exit14
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_14
# BB#11:
	movq	(%rdi), %rax
.Ltmp8:
	callq	*16(%rax)
.Ltmp9:
.LBB10_14:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit12
.Ltmp13:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp14:
.LBB10_15:
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE+16, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB10_16:
.Ltmp15:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN13COutMemStreamD2Ev, .Lfunc_end10-_ZN13COutMemStreamD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin0   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin0   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp14-.Ltmp3          #   Call between .Ltmp3 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin0   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Lfunc_end10-.Ltmp14    #   Call between .Ltmp14 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13COutMemStreamD0Ev,"axG",@progbits,_ZN13COutMemStreamD0Ev,comdat
	.weak	_ZN13COutMemStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN13COutMemStreamD0Ev,@function
_ZN13COutMemStreamD0Ev:                 # @_ZN13COutMemStreamD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp19:
	callq	_ZN13COutMemStreamD2Ev
.Ltmp20:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB11_2:
.Ltmp21:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN13COutMemStreamD0Ev, .Lfunc_end11-_ZN13COutMemStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp19-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end11-.Ltmp20    #   Call between .Ltmp20 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end12:
	.size	__clang_call_terminate, .Lfunc_end12-__clang_call_terminate

	.section	.text._ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,"axG",@progbits,_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,comdat
	.weak	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.p2align	4, 0x90
	.type	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv,@function
_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv: # @_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_startproc
# BB#0:
	cmpb	$0, 17(%rdi)
	je	.LBB13_1
# BB#2:
	movb	$1, %al
	cmpb	$0, 16(%rdi)
	jne	.LBB13_4
# BB#3:
	movb	$0, 17(%rdi)
.LBB13_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB13_1:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end13:
	.size	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv, .Lfunc_end13-_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.cfi_endproc

	.type	_ZTV13COutMemStream,@object # @_ZTV13COutMemStream
	.section	.rodata,"a",@progbits
	.globl	_ZTV13COutMemStream
	.p2align	3
_ZTV13COutMemStream:
	.quad	0
	.quad	_ZTI13COutMemStream
	.quad	_ZN13COutMemStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN13COutMemStream6AddRefEv
	.quad	_ZN13COutMemStream7ReleaseEv
	.quad	_ZN13COutMemStreamD2Ev
	.quad	_ZN13COutMemStreamD0Ev
	.quad	_ZN13COutMemStream5WriteEPKvjPj
	.quad	_ZN13COutMemStream4SeekExjPy
	.quad	_ZN13COutMemStream7SetSizeEy
	.size	_ZTV13COutMemStream, 80

	.type	_ZTS13COutMemStream,@object # @_ZTS13COutMemStream
	.globl	_ZTS13COutMemStream
_ZTS13COutMemStream:
	.asciz	"13COutMemStream"
	.size	_ZTS13COutMemStream, 16

	.type	_ZTS10IOutStream,@object # @_ZTS10IOutStream
	.section	.rodata._ZTS10IOutStream,"aG",@progbits,_ZTS10IOutStream,comdat
	.weak	_ZTS10IOutStream
_ZTS10IOutStream:
	.asciz	"10IOutStream"
	.size	_ZTS10IOutStream, 13

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTI10IOutStream,@object # @_ZTI10IOutStream
	.section	.rodata._ZTI10IOutStream,"aG",@progbits,_ZTI10IOutStream,comdat
	.weak	_ZTI10IOutStream
	.p2align	4
_ZTI10IOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IOutStream
	.quad	_ZTI20ISequentialOutStream
	.size	_ZTI10IOutStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI13COutMemStream,@object # @_ZTI13COutMemStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI13COutMemStream
	.p2align	4
_ZTI13COutMemStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS13COutMemStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI10IOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI13COutMemStream, 56

	.type	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	3
_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	0
	.quad	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZN8NWindows16NSynchronization14CBaseEventWFMO19IsSignaledAndUpdateEv
	.size	_ZTVN8NWindows16NSynchronization14CBaseEventWFMOE, 24

	.type	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE:
	.asciz	"N8NWindows16NSynchronization14CBaseEventWFMOE"
	.size	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE, 46

	.type	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	4
_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.asciz	"N8NWindows16NSynchronization15CBaseHandleWFMOE"
	.size	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE, 47

	.type	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,@object # @_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.p2align	3
_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE, 16

	.type	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,@object # @_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.section	.rodata._ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,"aG",@progbits,_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE,comdat
	.weak	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE
	.p2align	4
_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows16NSynchronization14CBaseEventWFMOE
	.quad	_ZTIN8NWindows16NSynchronization15CBaseHandleWFMOE
	.size	_ZTIN8NWindows16NSynchronization14CBaseEventWFMOE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
