	.text
	.file	"rc_quadratic.bc"
	.globl	rc_alloc
	.p2align	4, 0x90
	.type	rc_alloc,@function
rc_alloc:                               # @rc_alloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	img(%rip), %rax
	movl	15352(%rax), %eax
	movq	input(%rip), %rcx
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	divl	5128(%rcx)
	movl	%eax, %r14d
	movl	$1600, %edi             # imm = 0x640
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, (%r15)
	testq	%rbx, %rbx
	jne	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
	movq	(%r15), %rbx
.LBB0_2:
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 1424(%rbx)
	movq	%rax, 1400(%rbx)
	movl	$0, 1536(%rbx)
	movl	$0, 1540(%rbx)
	movl	$0, 1564(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 1512(%rbx)
	movl	$2147483647, 1556(%rbx) # imm = 0x7FFFFFFF
	movl	$2147483647, 1560(%rbx) # imm = 0x7FFFFFFF
	movups	%xmm0, 1568(%rbx)
	movq	input(%rip), %rax
	movl	12(%rax), %eax
	movl	%eax, 1384(%rbx)
	movl	%eax, 1344(%rbx)
	movl	%eax, 1456(%rbx)
	movl	%eax, 1460(%rbx)
	movl	%eax, 76(%rbx)
	movl	%eax, 72(%rbx)
	movl	$51, 64(%rbx)
	movq	img(%rip), %rax
	subl	15452(%rax), %ebp
	movl	%ebp, 68(%rbx)
	movslq	%r14d, %r14
	movl	$8, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 1472(%rbx)
	testq	%rax, %rax
	jne	.LBB0_4
# BB#3:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB0_4:
	movl	$8, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 1480(%rbx)
	testq	%rax, %rax
	jne	.LBB0_6
# BB#5:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
.LBB0_6:
	movl	$8, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 1488(%rbx)
	testq	%rax, %rax
	jne	.LBB0_8
# BB#7:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
.LBB0_8:
	movl	$8, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 1496(%rbx)
	testq	%rax, %rax
	je	.LBB0_10
# BB#9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_10:
	movl	$.L.str.4, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	no_mem_exit             # TAILCALL
.Lfunc_end0:
	.size	rc_alloc, .Lfunc_end0-rc_alloc
	.cfi_endproc

	.globl	copy_rc_jvt
	.p2align	4, 0x90
	.type	copy_rc_jvt,@function
copy_rc_jvt:                            # @copy_rc_jvt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	img(%rip), %rax
	movl	15352(%rax), %eax
	movq	input(%rip), %rcx
	xorl	%edx, %edx
	divl	5128(%rcx)
	movl	%eax, %ebp
	movups	1472(%rbx), %xmm0
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movups	1488(%rbx), %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movl	$1600, %edx             # imm = 0x640
	callq	memcpy
	movdqa	(%rsp), %xmm0           # 16-byte Reload
	movdqu	%xmm0, 1472(%rbx)
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movups	%xmm1, 1488(%rbx)
	movd	%xmm0, %rdi
	movq	1472(%r14), %rsi
	movslq	%ebp, %rbp
	shlq	$3, %rbp
	movq	%rbp, %rdx
	callq	memcpy
	movq	1480(%rbx), %rdi
	movq	1480(%r14), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	1496(%rbx), %rdi
	movq	1496(%r14), %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	1488(%rbx), %rdi
	movq	1488(%r14), %rsi
	movq	%rbp, %rdx
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	memcpy                  # TAILCALL
.Lfunc_end1:
	.size	copy_rc_jvt, .Lfunc_end1-copy_rc_jvt
	.cfi_endproc

	.globl	rc_free
	.p2align	4, 0x90
	.type	rc_free,@function
rc_free:                                # @rc_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movq	1472(%rdi), %rax
	testq	%rax, %rax
	je	.LBB2_2
# BB#1:
	movq	%rax, %rdi
	callq	free
	movq	(%rbx), %rax
	movq	$0, 1472(%rax)
	movq	(%rbx), %rdi
.LBB2_2:
	movq	1480(%rdi), %rax
	testq	%rax, %rax
	je	.LBB2_4
# BB#3:
	movq	%rax, %rdi
	callq	free
	movq	(%rbx), %rax
	movq	$0, 1480(%rax)
	movq	(%rbx), %rdi
.LBB2_4:
	movq	1488(%rdi), %rax
	testq	%rax, %rax
	je	.LBB2_6
# BB#5:
	movq	%rax, %rdi
	callq	free
	movq	(%rbx), %rax
	movq	$0, 1488(%rax)
	movq	(%rbx), %rdi
.LBB2_6:
	movq	1496(%rdi), %rax
	testq	%rax, %rax
	je	.LBB2_8
# BB#7:
	movq	%rax, %rdi
	callq	free
	movq	(%rbx), %rax
	movq	$0, 1496(%rax)
	movq	(%rbx), %rdi
.LBB2_8:
	testq	%rdi, %rdi
	je	.LBB2_10
# BB#9:
	callq	free
	movq	$0, (%rbx)
.LBB2_10:
	popq	%rbx
	retq
.Lfunc_end2:
	.size	rc_free, .Lfunc_end2-rc_free
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
.LCPI3_1:
	.quad	4598175219545276416     # double 0.25
	.quad	4606281698874543309     # double 0.90000000000000002
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_2:
	.quad	4591870180066957722     # double 0.10000000000000001
.LCPI3_3:
	.quad	4603579539098121011     # double 0.59999999999999998
.LCPI3_4:
	.quad	4599075939470750515     # double 0.29999999999999999
.LCPI3_5:
	.quad	4596373779694328218     # double 0.20000000000000001
.LCPI3_6:
	.quad	4608083138725491507     # double 1.2
.LCPI3_7:
	.quad	4612586738352862003     # double 2.3999999999999999
.LCPI3_8:
	.quad	4608983858650965606     # double 1.3999999999999999
	.text
	.globl	rc_init_seq
	.p2align	4, 0x90
	.type	rc_init_seq,@function
rc_init_seq:                            # @rc_init_seq
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	input(%rip), %rdx
	movslq	5136(%rdx), %rax
	cmpq	$3, %rax
	ja	.LBB3_1
# BB#2:                                 # %switch.lookup
	movq	.Lswitch.table(,%rax,8), %rax
	jmp	.LBB3_3
.LBB3_1:
	movl	$updateQPRC0, %eax
.LBB3_3:
	movq	%rax, updateQP(%rip)
	movq	$0, 1528(%rbx)
	cvtsi2ssl	5120(%rdx), %xmm0
	movss	%xmm0, (%rbx)
	movq	img(%rip), %rax
	movl	2096(%rdx), %ecx
	leal	1(%rcx), %esi
	cvtsi2ssl	%esi, %xmm1
	mulss	48(%rax), %xmm1
	movl	20(%rdx), %esi
	incl	%esi
	cvtsi2ssl	%esi, %xmm2
	divss	%xmm2, %xmm1
	movss	%xmm1, 4(%rbx)
	movss	%xmm0, 8(%rbx)
	movl	5128(%rdx), %esi
	movl	15352(%rax), %eax
	cmpl	%eax, %esi
	jbe	.LBB3_5
# BB#4:                                 # %.thread
	movl	%eax, 5128(%rdx)
	jmp	.LBB3_7
.LBB3_5:
	jae	.LBB3_7
# BB#6:
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, 1388(%rbx)
.LBB3_7:
	movq	generic_RC(%rip), %rax
	movq	$0, 72(%rax)
	movq	$0, 32(%rbx)
	movl	$0, 1340(%rbx)
	movl	$0, 1336(%rbx)
	movl	$0, 32(%rax)
	movl	$0, 36(%rax)
	movl	$0, 40(%rax)
	movl	$0, 80(%rax)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setg	%al
	movd	%eax, %xmm1
	pshufd	$68, %xmm1, %xmm1       # xmm1 = xmm1[0,1,0,1]
	psllq	$63, %xmm1
	psrad	$31, %xmm1
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	movdqa	%xmm1, %xmm2
	pandn	.LCPI3_0(%rip), %xmm2
	pand	.LCPI3_1(%rip), %xmm1
	por	%xmm2, %xmm1
	movdqu	%xmm1, 16(%rbx)
	movl	$0, 1348(%rbx)
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 1312(%rbx)
	movq	$0, 1320(%rbx)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 104(%rbx)
	leaq	960(%rbx), %rdi
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 272(%rbx)
	movupd	%xmm0, 256(%rbx)
	movupd	%xmm0, 240(%rbx)
	movupd	%xmm0, 224(%rbx)
	movupd	%xmm0, 208(%rbx)
	movupd	%xmm0, 192(%rbx)
	movupd	%xmm0, 176(%rbx)
	movupd	%xmm0, 160(%rbx)
	movupd	%xmm0, 144(%rbx)
	movupd	%xmm0, 128(%rbx)
	movupd	%xmm0, 112(%rbx)
	xorl	%esi, %esi
	movl	$336, %edx              # imm = 0x150
	callq	memset
	movl	$2, 1596(%rbx)
	movl	$0, 1372(%rbx)
	movl	$0, 1380(%rbx)
	xorl	%eax, %eax
	cmpl	$9, 1388(%rbx)
	setl	%al
	incl	%eax
	movl	%eax, 1440(%rbx)
	movq	img(%rip), %rax
	movl	15336(%rax), %ecx
	movl	%ecx, 1444(%rbx)
	movq	generic_RC(%rip), %rcx
	movl	$0, 4(%rcx)
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	cvtsi2ssl	88(%rax), %xmm1
	mulss	4(%rbx), %xmm1
	movl	52(%rax), %eax
	cmpl	$176, %eax
	je	.LBB3_8
# BB#9:
	cmpl	$352, %eax              # imm = 0x160
	jne	.LBB3_11
# BB#10:
	movsd	.LCPI3_5(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI3_6(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI3_3(%rip), %xmm3   # xmm3 = mem[0],zero
	jmp	.LBB3_12
.LBB3_8:
	movsd	.LCPI3_2(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI3_3(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI3_4(%rip), %xmm3   # xmm3 = mem[0],zero
	jmp	.LBB3_12
.LBB3_11:
	movsd	.LCPI3_3(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI3_7(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI3_8(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB3_12:
	movq	input(%rip), %rax
	cmpl	$0, 5124(%rax)
	jne	.LBB3_17
# BB#13:
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	divsd	%xmm1, %xmm0
	movl	$35, %ecx
	ucomisd	%xmm0, %xmm4
	jae	.LBB3_16
# BB#14:
	movl	$25, %ecx
	ucomisd	%xmm0, %xmm3
	jae	.LBB3_16
# BB#15:
	ucomisd	%xmm0, %xmm2
	movl	$10, %edx
	movl	$20, %ecx
	cmovbl	%edx, %ecx
.LBB3_16:
	movl	%ecx, 5124(%rax)
.LBB3_17:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	rc_init_seq, .Lfunc_end3-rc_init_seq
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4602678819172646912     # double 0.5
.LCPI4_2:
	.quad	4616189618054758400     # double 4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_1:
	.long	1082130432              # float 4
	.text
	.globl	updateQPRC0
	.p2align	4, 0x90
	.type	updateQPRC0,@function
updateQPRC0:                            # @updateQPRC0
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	img(%rip), %rax
	movl	15404(%rax), %ecx
	cmpl	15352(%rax), %ecx
	jne	.LBB4_58
# BB#1:
	testl	%ebp, %ebp
	je	.LBB4_2
.LBB4_3:
	movl	20(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB4_25
# BB#4:
	cmpl	$1, %ecx
	je	.LBB4_7
# BB#5:
	cmpl	$2, %ecx
	je	.LBB4_6
	jmp	.LBB4_31
.LBB4_58:
	movl	20(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB4_6
# BB#59:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	jne	.LBB4_60
.LBB4_6:
	movl	72(%rbx), %eax
	movl	%eax, 1344(%rbx)
	jmp	.LBB4_121
.LBB4_2:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 4(%rcx)
	je	.LBB4_3
# BB#50:
	cmpl	$0, 20(%rax)
	jne	.LBB4_57
# BB#51:
	cmpl	$0, 12(%rcx)
	jne	.LBB4_57
# BB#52:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB4_57
# BB#53:
	movq	input(%rip), %rdx
	movl	1344(%rbx), %eax
	cmpl	$1, 4704(%rdx)
	jne	.LBB4_55
# BB#54:
	addl	%eax, 44(%rcx)
	movl	1356(%rbx), %ecx
	incl	%ecx
	movl	%ecx, 1352(%rbx)
	movl	%eax, 1356(%rbx)
	leaq	1328(%rbx), %rcx
	jmp	.LBB4_56
.LBB4_60:
	testl	%ecx, %ecx
	je	.LBB4_72
# BB#61:
	cmpl	$1, %ecx
	jne	.LBB4_57
# BB#62:
	testl	%ebp, %ebp
	je	.LBB4_63
.LBB4_64:
	movq	input(%rip), %rsi
	movl	2096(%rsi), %ecx
	cmpl	$1, %ecx
	jne	.LBB4_13
# BB#65:
	cmpl	$2, 4704(%rsi)
	je	.LBB4_67
# BB#66:
	cmpl	$0, 4708(%rsi)
	je	.LBB4_69
.LBB4_67:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB4_69
# BB#68:
	leaq	1456(%rbx), %rcx
	leaq	1460(%rbx), %rdx
	cmpl	$1, 8(%rax)
	movl	1356(%rbx), %eax
	movl	%eax, 1352(%rbx)
	cmoveq	%rdx, %rcx
	movl	(%rcx), %eax
	movl	%eax, 1356(%rbx)
.LBB4_69:                               # %updateQPInterlace.exit222
	movl	1352(%rbx), %edi
	movl	1356(%rbx), %eax
	cmpl	%eax, %edi
	jne	.LBB4_71
# BB#70:
	addl	$2, %edi
	jmp	.LBB4_24
.LBB4_25:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 48(%rcx)
	jne	.LBB4_31
# BB#26:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	jne	.LBB4_27
.LBB4_31:
	movq	input(%rip), %rax
	cmpl	$2, 4704(%rax)
	je	.LBB4_33
# BB#32:
	cmpl	$0, 4708(%rax)
	je	.LBB4_35
.LBB4_33:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB4_35
# BB#34:
	leaq	1456(%rbx), %rcx
	leaq	1460(%rbx), %rdx
	cmpl	$1, 8(%rax)
	cmoveq	%rdx, %rcx
	movl	(%rcx), %edx
	addl	%edx, 44(%rax)
	movl	(%rcx), %eax
	movl	%eax, 1328(%rbx)
.LBB4_35:
	movq	1312(%rbx), %rax
	movq	%rax, 1296(%rbx)
	movq	1320(%rbx), %rcx
	movq	%rcx, 1304(%rbx)
	movq	104(%rbx), %rdx
	movq	%rdx, 88(%rbx)
	movq	112(%rbx), %rdi
	movq	%rdi, 96(%rbx)
	movsd	120(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 80(%rbx)
	movl	1596(%rbx), %r14d
	movl	1328(%rbx), %ebp
	movl	1348(%rbx), %esi
	movd	%rdx, %xmm1
	mulsd	%xmm0, %xmm1
	movd	%rdi, %xmm5
	addsd	%xmm1, %xmm5
	movsd	%xmm5, 1400(%rbx)
	movl	1536(%rbx), %edx
	testl	%edx, %edx
	js	.LBB4_36
# BB#37:
	movd	%rax, %xmm1
	movd	%rcx, %xmm2
	subl	%esi, %edx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	.LCPI4_1(%rip), %xmm3
	divss	%xmm3, %xmm0
	cvttss2si	%xmm0, %eax
	cmpl	%eax, %edx
	cmovll	%eax, %edx
	cvtsi2sdl	%edx, %xmm6
	xorps	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm2
	jne	.LBB4_38
	jnp	.LBB4_42
.LBB4_38:
	movapd	%xmm5, %xmm3
	movhpd	.LCPI4_2(%rip), %xmm3   # xmm3 = xmm3[0],mem[0]
	movdqa	%xmm1, %xmm4
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	mulpd	%xmm3, %xmm4
	movapd	%xmm5, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm4, %xmm2
	movdqa	%xmm1, %xmm3
	unpcklpd	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0]
	mulpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	%xmm3, %xmm4
	ucomisd	%xmm4, %xmm0
	ja	.LBB4_42
# BB#39:
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_41
# BB#40:                                # %call.sqrt
	movapd	%xmm4, %xmm0
	movapd	%xmm6, (%rsp)           # 16-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	(%rsp), %xmm6           # 16-byte Reload
.LBB4_41:                               # %.split
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	movsd	1400(%rbx), %xmm5       # xmm5 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm5, %xmm2
	subsd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jae	.LBB4_42
# BB#43:
	movsd	1304(%rbx), %xmm0       # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm5
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB4_45
# BB#44:                                # %call.sqrt234
	movapd	%xmm4, %xmm0
	movapd	%xmm5, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm5           # 16-byte Reload
.LBB4_45:                               # %.split233
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	mulsd	1400(%rbx), %xmm1
	subsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm5
	jmp	.LBB4_46
.LBB4_7:
	movq	input(%rip), %rsi
	movl	2096(%rsi), %ecx
	cmpl	$1, %ecx
	jne	.LBB4_13
# BB#8:
	cmpl	$2, 4704(%rsi)
	je	.LBB4_10
# BB#9:
	cmpl	$0, 4708(%rsi)
	je	.LBB4_12
.LBB4_10:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB4_12
# BB#11:
	leaq	1456(%rbx), %rcx
	leaq	1460(%rbx), %rdx
	cmpl	$1, 8(%rax)
	movl	1356(%rbx), %eax
	movl	%eax, 1352(%rbx)
	cmoveq	%rdx, %rcx
	movl	(%rcx), %eax
	movl	%eax, 1356(%rbx)
.LBB4_12:                               # %updateQPInterlace.exit
	movl	1352(%rbx), %eax
	movl	1356(%rbx), %edi
	cmpl	%edi, %eax
	movl	%edi, %ecx
	cmovlel	%eax, %ecx
	cmovll	%edi, %eax
	addl	$2, %ecx
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
	incl	%edi
	cmpl	%edi, %ecx
	cmovgel	%ecx, %edi
	jmp	.LBB4_24
.LBB4_42:
	mulsd	%xmm1, %xmm5
	divsd	%xmm6, %xmm5
.LBB4_46:                               # %updateModelQPFrame.exit
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm5, %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	Qstep2QP
	leaq	1344(%rbx), %rcx
	movl	64(%rbx), %edx
	movl	68(%rbx), %esi
	cmpl	%esi, %eax
	cmovgel	%eax, %esi
	cmpl	%edx, %esi
	cmovgl	%edx, %esi
	movl	%esi, 1344(%rbx)
	movl	%ebp, %eax
	subl	%r14d, %eax
	addl	%r14d, %ebp
	cmpl	%eax, %esi
	cmovgel	%esi, %eax
	cmpl	%ebp, %eax
	cmovgl	%ebp, %eax
	jmp	.LBB4_47
.LBB4_36:
	addl	%r14d, %ebp
	leaq	1344(%rbx), %rcx
	movl	%ebp, 1344(%rbx)
	movl	64(%rbx), %eax
	movl	68(%rbx), %edx
	cmpl	%edx, %ebp
	cmovll	%edx, %ebp
	cmpl	%eax, %ebp
	cmovgl	%eax, %ebp
	movl	%ebp, %eax
.LBB4_47:
	movl	%eax, (%rcx)
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 4(%rcx)
	jne	.LBB4_121
# BB#48:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 1148(%rdx)
	je	.LBB4_49
.LBB4_29:
	addl	%eax, 44(%rcx)
	movl	1356(%rbx), %ecx
	movl	%ecx, 1352(%rbx)
	movl	%eax, 1356(%rbx)
	movl	%eax, 1328(%rbx)
	jmp	.LBB4_121
.LBB4_13:
	movl	1360(%rbx), %eax
	incl	%eax
	cltd
	idivl	%ecx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	testl	%edx, %edx
	cmovel	%ecx, %edx
	cmpl	$1, %edx
	jne	.LBB4_18
# BB#14:
	cmpl	$2, 4704(%rsi)
	je	.LBB4_16
# BB#15:
	cmpl	$0, 4708(%rsi)
	je	.LBB4_18
.LBB4_16:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB4_18
# BB#17:
	leaq	1456(%rbx), %rsi
	leaq	1460(%rbx), %rdi
	cmpl	$1, 8(%rax)
	movl	1356(%rbx), %eax
	movl	%eax, 1352(%rbx)
	cmoveq	%rdi, %rsi
	movl	(%rsi), %eax
	movl	%eax, 1356(%rbx)
.LBB4_18:                               # %updateQPInterlace.exit226
	movl	1352(%rbx), %r8d
	movl	1356(%rbx), %eax
	subl	%r8d, %eax
	leal	(%rcx,%rcx), %edi
	movl	$-3, %ebp
	movl	$-3, %esi
	subl	%edi, %esi
	cmpl	%esi, %eax
	jle	.LBB4_23
# BB#19:
	negl	%edi
	leal	-2(%rdi), %esi
	movl	$-2, %ebp
	cmpl	%esi, %eax
	je	.LBB4_23
# BB#20:
	leal	-1(%rdi), %esi
	movl	$-1, %ebp
	cmpl	%esi, %eax
	je	.LBB4_23
# BB#21:
	xorl	%ebp, %ebp
	cmpl	%edi, %eax
	je	.LBB4_23
# BB#22:
	orl	$1, %edi
	xorl	%ebp, %ebp
	cmpl	%edi, %eax
	setne	%bpl
	incl	%ebp
.LBB4_23:
	addl	%ebp, %r8d
	leal	-1(%rdx), %esi
	leal	-2(%rdx,%rdx), %ebp
	movl	%ebp, %edi
	negl	%edi
	imull	%esi, %eax
	decl	%ecx
	cltd
	idivl	%ecx
	cmpl	%edi, %eax
	cmovgel	%eax, %edi
	cmpl	%ebp, %edi
	cmovgl	%ebp, %edi
	addl	%r8d, %edi
.LBB4_24:
	movl	%edi, 1344(%rbx)
	movl	64(%rbx), %ecx
	movl	68(%rbx), %eax
	cmpl	%eax, %edi
	cmovgel	%edi, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movl	%eax, 1344(%rbx)
	jmp	.LBB4_121
.LBB4_72:
	movq	generic_RC(%rip), %rdx
	cmpl	$1, 40(%rdx)
	jne	.LBB4_87
# BB#73:
	cmpl	$0, 48(%rdx)
	je	.LBB4_74
.LBB4_87:
	movups	1312(%rbx), %xmm0
	movups	%xmm0, 1296(%rbx)
	movupd	104(%rbx), %xmm0
	movupd	%xmm0, 88(%rbx)
	movl	4(%rdx), %eax
	movl	1388(%rbx), %esi
	testl	%eax, %eax
	setne	%cl
	sarl	%cl, %esi
	cmpl	%esi, 1368(%rbx)
	jne	.LBB4_105
# BB#88:
	movq	input(%rip), %rcx
	cmpl	$2, 4704(%rcx)
	je	.LBB4_90
# BB#89:
	cmpl	$0, 4708(%rcx)
	je	.LBB4_99
.LBB4_90:
	testl	%eax, %eax
	jne	.LBB4_99
# BB#91:
	movl	48(%rdx), %eax
	cmpl	$1, 8(%rdx)
	jne	.LBB4_95
# BB#92:
	movl	1460(%rbx), %ecx
	testl	%eax, %eax
	jle	.LBB4_94
# BB#93:
	addl	%ecx, 44(%rdx)
.LBB4_94:                               # %._crit_edge27.i
	movl	%ecx, 1384(%rbx)
	leaq	1464(%rbx), %rax
	jmp	.LBB4_98
.LBB4_49:
	movl	%eax, 1460(%rbx)
	jmp	.LBB4_121
.LBB4_27:
	movl	72(%rbx), %eax
	movl	%eax, 1344(%rbx)
	cmpl	$0, 4(%rcx)
	jne	.LBB4_121
# BB#28:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 1148(%rdx)
	jne	.LBB4_29
# BB#30:
	movl	%eax, 1460(%rbx)
	jmp	.LBB4_121
.LBB4_105:
	movl	1328(%rbx), %r14d
	movl	28(%rdx), %eax
	movl	1536(%rbx), %ecx
	addl	24(%rdx), %eax
	subl	%eax, %ecx
	movl	%ecx, 1536(%rbx)
	movq	$0, 24(%rdx)
	js	.LBB4_122
# BB#106:
	movq	%rbx, %rdi
	callq	predictCurrPicMAD
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	updateModelQPBU
	movl	1344(%rbx), %eax
	movl	1364(%rbx), %ecx
	addl	%eax, %ecx
	movl	%ecx, 1364(%rbx)
	movl	%eax, 1328(%rbx)
	decl	1368(%rbx)
	jne	.LBB4_121
# BB#107:
	movq	img(%rip), %rdx
	cmpl	$0, 20(%rdx)
	jne	.LBB4_121
# BB#108:
	movl	(%rdx), %edx
	cmpl	start_frame_no_in_this_IGOP(%rip), %edx
	je	.LBB4_121
# BB#109:
	testl	%ebp, %ebp
	je	.LBB4_111
# BB#110:
	movq	generic_RC(%rip), %rdx
	cmpl	$0, 4(%rdx)
	jne	.LBB4_121
.LBB4_111:
	movq	active_sps(%rip), %rsi
	movq	input(%rip), %rdx
	cmpl	$0, 1148(%rsi)
	je	.LBB4_112
.LBB4_114:                              # %._crit_edge.i
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%rbx), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI4_0(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
	movq	generic_RC(%rip), %rsi
	movl	1560(%rdx), %edx
	addl	$-2, %edx
	cmpl	%edx, 48(%rsi)
	jne	.LBB4_116
# BB#115:
	movl	%ecx, 1448(%rbx)
.LBB4_116:
	addl	%ecx, 44(%rsi)
	movl	1356(%rbx), %edx
	movl	%edx, 1352(%rbx)
	movl	%ecx, 1356(%rbx)
	movl	%ecx, 1384(%rbx)
	movl	1376(%rbx), %ecx
	movl	%ecx, 1380(%rbx)
	jmp	.LBB4_121
.LBB4_63:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB4_57
	jmp	.LBB4_64
.LBB4_71:
	addl	%edi, %eax
	movl	%eax, %edi
	shrl	$31, %edi
	addl	%eax, %edi
	sarl	%edi
	incl	%edi
	jmp	.LBB4_24
.LBB4_74:
	movl	4(%rdx), %ecx
	testl	%ecx, %ecx
	je	.LBB4_77
# BB#75:
	cmpl	$1, %ecx
	jne	.LBB4_57
# BB#76:
	cmpl	$0, 12(%rdx)
	jne	.LBB4_57
.LBB4_77:
	movl	72(%rbx), %eax
	movl	%eax, 1344(%rbx)
	movq	$0, 24(%rdx)
	movl	1368(%rbx), %esi
	decl	%esi
	movl	%esi, 1368(%rbx)
	orl	%ebp, %esi
	jne	.LBB4_86
# BB#78:
	movq	active_sps(%rip), %rsi
	cmpl	$0, 1148(%rsi)
	je	.LBB4_79
.LBB4_81:
	addl	%eax, 44(%rdx)
	movl	1356(%rbx), %ecx
	movl	%ecx, 1352(%rbx)
	movl	%eax, 1356(%rbx)
	movl	%eax, 1384(%rbx)
	movl	1376(%rbx), %ecx
	movl	%ecx, 1380(%rbx)
.LBB4_86:                               # %updateFirstP.exit
	movl	%eax, 1328(%rbx)
	addl	%eax, 1364(%rbx)
	jmp	.LBB4_121
.LBB4_122:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	updateNegativeTarget    # TAILCALL
.LBB4_55:
	leaq	1456(%rbx), %rcx
.LBB4_56:                               # %updateBottomField.exit
	movl	%eax, (%rcx)
.LBB4_57:
	movl	1344(%rbx), %eax
.LBB4_121:                              # %updateQPNonPicAFF.exit228
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB4_95:
	movl	1456(%rbx), %ecx
	testl	%eax, %eax
	jle	.LBB4_97
# BB#96:
	addl	%ecx, 44(%rdx)
.LBB4_97:                               # %._crit_edge.i214
	movl	%ecx, 1384(%rbx)
	leaq	1468(%rbx), %rax
.LBB4_98:
	movl	(%rax), %eax
	movl	%eax, 1380(%rbx)
.LBB4_99:
	movl	1384(%rbx), %ecx
	cmpl	$0, 1536(%rbx)
	jle	.LBB4_100
# BB#103:
	movl	%ecx, 1344(%rbx)
	movl	%ecx, %eax
	jmp	.LBB4_104
.LBB4_100:
	leal	2(%rcx), %eax
	movl	64(%rbx), %edi
	cmpl	%edi, %eax
	cmovgl	%edi, %eax
	movl	%eax, 1344(%rbx)
	testl	%ebp, %ebp
	je	.LBB4_101
.LBB4_102:
	movl	$1, 1504(%rbx)
	jmp	.LBB4_104
.LBB4_101:
	cmpl	$0, 4(%rdx)
	je	.LBB4_102
.LBB4_104:                              # %updateFirstBU.exit
	addl	%eax, 1364(%rbx)
	decl	%esi
	movl	%esi, 1368(%rbx)
	movl	%ecx, 1328(%rbx)
	jmp	.LBB4_121
.LBB4_79:
	movq	input(%rip), %rsi
	movl	4704(%rsi), %edi
	cmpl	$2, %edi
	je	.LBB4_83
# BB#80:
	cmpl	$1, %edi
	je	.LBB4_81
# BB#82:
	cmpl	$0, 4708(%rsi)
	je	.LBB4_86
.LBB4_83:
	movl	1376(%rbx), %edx
	testl	%ecx, %ecx
	je	.LBB4_84
# BB#85:
	movl	%eax, 1456(%rbx)
	movl	%edx, 1468(%rbx)
	jmp	.LBB4_86
.LBB4_112:
	movl	4704(%rdx), %esi
	cmpl	$2, %esi
	je	.LBB4_118
# BB#113:
	cmpl	$1, %esi
	je	.LBB4_114
# BB#117:
	cmpl	$0, 4708(%rdx)
	je	.LBB4_121
.LBB4_118:
	movq	generic_RC(%rip), %rsi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%rbx), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI4_0(%rip), %xmm0
	cvttsd2si	%xmm0, %edx
	movl	1376(%rbx), %ecx
	cmpl	$0, 4(%rsi)
	je	.LBB4_119
# BB#120:
	movl	%edx, 1456(%rbx)
	movl	%ecx, 1468(%rbx)
	jmp	.LBB4_121
.LBB4_84:
	movl	%eax, 1460(%rbx)
	movl	%edx, 1464(%rbx)
	jmp	.LBB4_86
.LBB4_119:
	movl	%edx, 1460(%rbx)
	movl	%ecx, 1464(%rbx)
	jmp	.LBB4_121
.Lfunc_end4:
	.size	updateQPRC0, .Lfunc_end4-updateQPRC0
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4602678819172646912     # double 0.5
.LCPI5_2:
	.quad	4616189618054758400     # double 4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_1:
	.long	1082130432              # float 4
	.text
	.globl	updateQPRC1
	.p2align	4, 0x90
	.type	updateQPRC1,@function
updateQPRC1:                            # @updateQPRC1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 64
.Lcfi31:
	.cfi_offset %rbx, -32
.Lcfi32:
	.cfi_offset %r14, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	img(%rip), %rax
	movl	15404(%rax), %ecx
	cmpl	15352(%rax), %ecx
	jne	.LBB5_36
# BB#1:
	testl	%ebp, %ebp
	je	.LBB5_2
.LBB5_3:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB5_4
# BB#5:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 48(%rcx)
	je	.LBB5_6
# BB#10:
	movq	input(%rip), %rax
	cmpl	$2, 4704(%rax)
	je	.LBB5_12
# BB#11:
	cmpl	$0, 4708(%rax)
	je	.LBB5_14
.LBB5_12:
	cmpl	$0, 4(%rcx)
	jne	.LBB5_14
# BB#13:
	leaq	1456(%rbx), %rax
	leaq	1460(%rbx), %rdx
	cmpl	$1, 8(%rcx)
	cmoveq	%rdx, %rax
	movl	(%rax), %edx
	addl	%edx, 44(%rcx)
	movl	(%rax), %eax
	movl	%eax, 1328(%rbx)
.LBB5_14:
	movq	1312(%rbx), %rax
	movq	%rax, 1296(%rbx)
	movq	1320(%rbx), %rdx
	movq	%rdx, 1304(%rbx)
	movq	104(%rbx), %rsi
	movq	%rsi, 88(%rbx)
	movq	112(%rbx), %rdi
	movq	%rdi, 96(%rbx)
	movsd	120(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 80(%rbx)
	movl	1596(%rbx), %r14d
	movl	1328(%rbx), %ebp
	movl	1348(%rbx), %r8d
	movd	%rsi, %xmm1
	mulsd	%xmm0, %xmm1
	movd	%rdi, %xmm5
	addsd	%xmm1, %xmm5
	movsd	%xmm5, 1400(%rbx)
	movl	1536(%rbx), %esi
	testl	%esi, %esi
	js	.LBB5_15
# BB#16:
	movd	%rax, %xmm1
	movd	%rdx, %xmm2
	subl	%r8d, %esi
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	.LCPI5_1(%rip), %xmm3
	divss	%xmm3, %xmm0
	cvttss2si	%xmm0, %eax
	cmpl	%eax, %esi
	cmovll	%eax, %esi
	cvtsi2sdl	%esi, %xmm6
	xorps	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm2
	jne	.LBB5_17
	jnp	.LBB5_21
.LBB5_17:
	movapd	%xmm5, %xmm3
	movhpd	.LCPI5_2(%rip), %xmm3   # xmm3 = xmm3[0],mem[0]
	movdqa	%xmm1, %xmm4
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	mulpd	%xmm3, %xmm4
	movapd	%xmm5, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm4, %xmm2
	movdqa	%xmm1, %xmm3
	unpcklpd	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0]
	mulpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	%xmm3, %xmm4
	ucomisd	%xmm4, %xmm0
	ja	.LBB5_21
# BB#18:
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB5_20
# BB#19:                                # %call.sqrt
	movapd	%xmm4, %xmm0
	movapd	%xmm6, (%rsp)           # 16-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	(%rsp), %xmm6           # 16-byte Reload
.LBB5_20:                               # %.split
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	movsd	1400(%rbx), %xmm5       # xmm5 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm5, %xmm2
	subsd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jae	.LBB5_21
# BB#22:
	movsd	1304(%rbx), %xmm0       # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm5
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB5_24
# BB#23:                                # %call.sqrt111
	movapd	%xmm4, %xmm0
	movapd	%xmm5, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm5           # 16-byte Reload
.LBB5_24:                               # %.split110
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	mulsd	1400(%rbx), %xmm1
	subsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm5
	jmp	.LBB5_25
.LBB5_36:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	jne	.LBB5_37
.LBB5_4:
	movl	72(%rbx), %eax
	movl	%eax, 1344(%rbx)
	jmp	.LBB5_85
.LBB5_37:
	movq	generic_RC(%rip), %rdx
	cmpl	$1, 40(%rdx)
	jne	.LBB5_52
# BB#38:
	cmpl	$0, 48(%rdx)
	je	.LBB5_39
.LBB5_52:
	movups	1312(%rbx), %xmm0
	movups	%xmm0, 1296(%rbx)
	movupd	104(%rbx), %xmm0
	movupd	%xmm0, 88(%rbx)
	movl	4(%rdx), %eax
	movl	1388(%rbx), %esi
	testl	%eax, %eax
	setne	%cl
	sarl	%cl, %esi
	cmpl	%esi, 1368(%rbx)
	jne	.LBB5_70
# BB#53:
	movq	input(%rip), %rcx
	cmpl	$2, 4704(%rcx)
	je	.LBB5_55
# BB#54:
	cmpl	$0, 4708(%rcx)
	je	.LBB5_64
.LBB5_55:
	testl	%eax, %eax
	jne	.LBB5_64
# BB#56:
	movl	48(%rdx), %eax
	cmpl	$1, 8(%rdx)
	jne	.LBB5_60
# BB#57:
	movl	1460(%rbx), %ecx
	testl	%eax, %eax
	jle	.LBB5_59
# BB#58:
	addl	%ecx, 44(%rdx)
.LBB5_59:                               # %._crit_edge27.i
	movl	%ecx, 1384(%rbx)
	leaq	1464(%rbx), %rax
	jmp	.LBB5_63
.LBB5_2:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 4(%rcx)
	je	.LBB5_3
# BB#29:
	cmpl	$0, 12(%rcx)
	jne	.LBB5_35
# BB#30:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB5_35
# BB#31:
	movq	input(%rip), %rdx
	movl	1344(%rbx), %eax
	cmpl	$1, 4704(%rdx)
	jne	.LBB5_33
# BB#32:
	addl	%eax, 44(%rcx)
	movl	1356(%rbx), %ecx
	incl	%ecx
	movl	%ecx, 1352(%rbx)
	movl	%eax, 1356(%rbx)
	leaq	1328(%rbx), %rcx
	jmp	.LBB5_34
.LBB5_70:
	movl	1328(%rbx), %r14d
	movl	28(%rdx), %eax
	movl	1536(%rbx), %ecx
	addl	24(%rdx), %eax
	subl	%eax, %ecx
	movl	%ecx, 1536(%rbx)
	movq	$0, 24(%rdx)
	js	.LBB5_86
# BB#71:
	movq	%rbx, %rdi
	callq	predictCurrPicMAD
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	updateModelQPBU
	movl	1344(%rbx), %eax
	movl	1364(%rbx), %ecx
	addl	%eax, %ecx
	movl	%ecx, 1364(%rbx)
	movl	%eax, 1328(%rbx)
	decl	1368(%rbx)
	jne	.LBB5_85
# BB#72:
	movq	img(%rip), %rdx
	movl	(%rdx), %edx
	cmpl	start_frame_no_in_this_IGOP(%rip), %edx
	je	.LBB5_85
# BB#73:
	testl	%ebp, %ebp
	je	.LBB5_75
# BB#74:
	movq	generic_RC(%rip), %rdx
	cmpl	$0, 4(%rdx)
	jne	.LBB5_85
.LBB5_75:
	movq	active_sps(%rip), %rsi
	movq	input(%rip), %rdx
	cmpl	$0, 1148(%rsi)
	je	.LBB5_76
.LBB5_78:                               # %._crit_edge.i
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%rbx), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI5_0(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
	movq	generic_RC(%rip), %rsi
	movl	1560(%rdx), %edx
	addl	$-2, %edx
	cmpl	%edx, 48(%rsi)
	jne	.LBB5_80
# BB#79:
	movl	%ecx, 1448(%rbx)
.LBB5_80:
	addl	%ecx, 44(%rsi)
	movl	1356(%rbx), %edx
	movl	%edx, 1352(%rbx)
	movl	%ecx, 1356(%rbx)
	movl	%ecx, 1384(%rbx)
	movl	1376(%rbx), %ecx
	movl	%ecx, 1380(%rbx)
	jmp	.LBB5_85
.LBB5_6:
	movl	72(%rbx), %eax
	movl	%eax, 1344(%rbx)
	cmpl	$0, 4(%rcx)
	jne	.LBB5_85
# BB#7:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 1148(%rdx)
	jne	.LBB5_8
# BB#9:
	movl	%eax, 1460(%rbx)
	jmp	.LBB5_85
.LBB5_21:
	mulsd	%xmm1, %xmm5
	divsd	%xmm6, %xmm5
.LBB5_25:                               # %updateModelQPFrame.exit
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm5, %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	Qstep2QP
	leaq	1344(%rbx), %rdx
	movl	64(%rbx), %ecx
	movl	68(%rbx), %esi
	cmpl	%esi, %eax
	cmovgel	%eax, %esi
	cmpl	%ecx, %esi
	cmovgl	%ecx, %esi
	movl	%esi, 1344(%rbx)
	movl	%ebp, %eax
	subl	%r14d, %eax
	addl	%r14d, %ebp
	cmpl	%eax, %esi
	cmovgel	%esi, %eax
	cmpl	%ebp, %eax
	cmovgl	%ebp, %eax
	movq	generic_RC(%rip), %rcx
	jmp	.LBB5_26
.LBB5_39:
	movl	4(%rdx), %ecx
	testl	%ecx, %ecx
	je	.LBB5_42
# BB#40:
	cmpl	$1, %ecx
	jne	.LBB5_35
# BB#41:
	cmpl	$0, 12(%rdx)
	jne	.LBB5_35
.LBB5_42:
	movl	72(%rbx), %eax
	movl	%eax, 1344(%rbx)
	movq	$0, 24(%rdx)
	movl	1368(%rbx), %esi
	decl	%esi
	movl	%esi, 1368(%rbx)
	orl	%ebp, %esi
	jne	.LBB5_51
# BB#43:
	movq	active_sps(%rip), %rsi
	cmpl	$0, 1148(%rsi)
	je	.LBB5_44
.LBB5_46:
	addl	%eax, 44(%rdx)
	movl	1356(%rbx), %ecx
	movl	%ecx, 1352(%rbx)
	movl	%eax, 1356(%rbx)
	movl	%eax, 1384(%rbx)
	movl	1376(%rbx), %ecx
	movl	%ecx, 1380(%rbx)
.LBB5_51:                               # %updateFirstP.exit
	movl	%eax, 1328(%rbx)
	addl	%eax, 1364(%rbx)
	jmp	.LBB5_85
.LBB5_15:
	addl	%r14d, %ebp
	leaq	1344(%rbx), %rdx
	movl	%ebp, 1344(%rbx)
	movl	64(%rbx), %eax
	movl	68(%rbx), %esi
	cmpl	%esi, %ebp
	cmovll	%esi, %ebp
	cmpl	%eax, %ebp
	cmovgl	%eax, %ebp
	movl	%ebp, %eax
.LBB5_26:
	movl	%eax, (%rdx)
	cmpl	$0, 4(%rcx)
	jne	.LBB5_85
# BB#27:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 1148(%rdx)
	je	.LBB5_28
.LBB5_8:
	addl	%eax, 44(%rcx)
	movl	1356(%rbx), %ecx
	movl	%ecx, 1352(%rbx)
	movl	%eax, 1356(%rbx)
	movl	%eax, 1328(%rbx)
	jmp	.LBB5_85
.LBB5_86:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	updateNegativeTarget    # TAILCALL
.LBB5_60:
	movl	1456(%rbx), %ecx
	testl	%eax, %eax
	jle	.LBB5_62
# BB#61:
	addl	%ecx, 44(%rdx)
.LBB5_62:                               # %._crit_edge.i104
	movl	%ecx, 1384(%rbx)
	leaq	1468(%rbx), %rax
.LBB5_63:
	movl	(%rax), %eax
	movl	%eax, 1380(%rbx)
.LBB5_64:
	movl	1384(%rbx), %ecx
	cmpl	$0, 1536(%rbx)
	jle	.LBB5_65
# BB#68:
	movl	%ecx, 1344(%rbx)
	movl	%ecx, %eax
	jmp	.LBB5_69
.LBB5_65:
	leal	2(%rcx), %eax
	movl	64(%rbx), %edi
	cmpl	%edi, %eax
	cmovgl	%edi, %eax
	movl	%eax, 1344(%rbx)
	testl	%ebp, %ebp
	je	.LBB5_66
.LBB5_67:
	movl	$1, 1504(%rbx)
	jmp	.LBB5_69
.LBB5_28:
	movl	%eax, 1460(%rbx)
	jmp	.LBB5_85
.LBB5_66:
	cmpl	$0, 4(%rdx)
	je	.LBB5_67
.LBB5_69:                               # %updateFirstBU.exit
	addl	%eax, 1364(%rbx)
	decl	%esi
	movl	%esi, 1368(%rbx)
	movl	%ecx, 1328(%rbx)
	jmp	.LBB5_85
.LBB5_33:
	leaq	1456(%rbx), %rcx
.LBB5_34:                               # %updateBottomField.exit
	movl	%eax, (%rcx)
.LBB5_35:
	movl	1344(%rbx), %eax
.LBB5_85:                               # %updateQPNonPicAFF.exit
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB5_44:
	movq	input(%rip), %rsi
	movl	4704(%rsi), %edi
	cmpl	$2, %edi
	je	.LBB5_48
# BB#45:
	cmpl	$1, %edi
	je	.LBB5_46
# BB#47:
	cmpl	$0, 4708(%rsi)
	je	.LBB5_51
.LBB5_48:
	movl	1376(%rbx), %edx
	testl	%ecx, %ecx
	je	.LBB5_49
# BB#50:
	movl	%eax, 1456(%rbx)
	movl	%edx, 1468(%rbx)
	jmp	.LBB5_51
.LBB5_76:
	movl	4704(%rdx), %esi
	cmpl	$2, %esi
	je	.LBB5_82
# BB#77:
	cmpl	$1, %esi
	je	.LBB5_78
# BB#81:
	cmpl	$0, 4708(%rdx)
	je	.LBB5_85
.LBB5_82:
	movq	generic_RC(%rip), %rsi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%rbx), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI5_0(%rip), %xmm0
	cvttsd2si	%xmm0, %edx
	movl	1376(%rbx), %ecx
	cmpl	$0, 4(%rsi)
	je	.LBB5_83
# BB#84:
	movl	%edx, 1456(%rbx)
	movl	%ecx, 1468(%rbx)
	jmp	.LBB5_85
.LBB5_49:
	movl	%eax, 1460(%rbx)
	movl	%edx, 1464(%rbx)
	jmp	.LBB5_51
.LBB5_83:
	movl	%edx, 1460(%rbx)
	movl	%ecx, 1464(%rbx)
	jmp	.LBB5_85
.Lfunc_end5:
	.size	updateQPRC1, .Lfunc_end5-updateQPRC1
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4602678819172646912     # double 0.5
.LCPI6_2:
	.quad	4616189618054758400     # double 4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_1:
	.long	1082130432              # float 4
	.text
	.globl	updateQPRC2
	.p2align	4, 0x90
	.type	updateQPRC2,@function
updateQPRC2:                            # @updateQPRC2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 64
.Lcfi38:
	.cfi_offset %rbx, -32
.Lcfi39:
	.cfi_offset %r14, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	img(%rip), %rax
	movl	15404(%rax), %ecx
	cmpl	15352(%rax), %ecx
	jne	.LBB6_54
# BB#1:
	testl	%ebp, %ebp
	je	.LBB6_2
.LBB6_3:
	movl	(%rax), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	je	.LBB6_4
# BB#5:
	movl	20(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB6_22
# BB#6:
	cmpl	$1, %ecx
	je	.LBB6_13
# BB#7:
	cmpl	$2, %ecx
	jne	.LBB6_27
# BB#8:
	movq	input(%rip), %rax
	cmpl	$2, 4704(%rax)
	je	.LBB6_10
# BB#9:
	cmpl	$0, 4708(%rax)
	je	.LBB6_12
.LBB6_10:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB6_12
# BB#11:
	leaq	1456(%rbx), %rcx
	leaq	1460(%rbx), %rdx
	cmpl	$1, 8(%rax)
	movl	1356(%rbx), %eax
	movl	%eax, 1352(%rbx)
	cmoveq	%rdx, %rcx
	movl	(%rcx), %eax
	movl	%eax, 1356(%rbx)
.LBB6_12:                               # %updateQPInterlace.exit
	movl	1356(%rbx), %eax
	movl	%eax, 1344(%rbx)
	jmp	.LBB6_112
.LBB6_54:
	movl	(%rax), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	jne	.LBB6_55
.LBB6_4:
	movl	72(%rbx), %eax
	movl	%eax, 1344(%rbx)
	jmp	.LBB6_112
.LBB6_55:
	movl	20(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB6_63
# BB#56:
	cmpl	$1, %ecx
	je	.LBB6_13
# BB#57:
	cmpl	$2, %ecx
	jne	.LBB6_53
# BB#58:
	movq	input(%rip), %rax
	cmpl	$2, 4704(%rax)
	je	.LBB6_60
# BB#59:
	cmpl	$0, 4708(%rax)
	je	.LBB6_62
.LBB6_60:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB6_62
# BB#61:
	leaq	1456(%rbx), %rcx
	leaq	1460(%rbx), %rdx
	cmpl	$1, 8(%rax)
	movl	1356(%rbx), %eax
	movl	%eax, 1352(%rbx)
	cmoveq	%rdx, %rcx
	movl	(%rcx), %eax
	movl	%eax, 1356(%rbx)
.LBB6_62:                               # %updateQPInterlace.exit171
	movl	1352(%rbx), %eax
	movl	%eax, 1344(%rbx)
	movl	1356(%rbx), %ecx
	movl	%ecx, 1352(%rbx)
	movl	%ecx, 1384(%rbx)
	jmp	.LBB6_112
.LBB6_2:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 4(%rcx)
	je	.LBB6_3
# BB#46:
	cmpl	$0, 20(%rax)
	jne	.LBB6_53
# BB#47:
	cmpl	$0, 12(%rcx)
	jne	.LBB6_53
# BB#48:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB6_53
# BB#49:
	movq	input(%rip), %rdx
	movl	1344(%rbx), %eax
	cmpl	$1, 4704(%rdx)
	jne	.LBB6_51
# BB#50:
	addl	%eax, 44(%rcx)
	movl	1356(%rbx), %ecx
	incl	%ecx
	movl	%ecx, 1352(%rbx)
	movl	%eax, 1356(%rbx)
	leaq	1328(%rbx), %rcx
	jmp	.LBB6_52
.LBB6_13:
	movl	1352(%rbx), %ecx
	movl	1356(%rbx), %esi
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
	movq	input(%rip), %rdx
	cmpl	$2, 4704(%rdx)
	je	.LBB6_15
# BB#14:
	cmpl	$0, 4708(%rdx)
	je	.LBB6_17
.LBB6_15:
	movq	generic_RC(%rip), %rdi
	cmpl	$0, 4(%rdi)
	jne	.LBB6_17
# BB#16:
	leaq	1456(%rbx), %rbp
	leaq	1460(%rbx), %r8
	cmpl	$1, 8(%rdi)
	movl	%esi, 1352(%rbx)
	cmoveq	%r8, %rbp
	movl	(%rbp), %esi
	movl	%esi, 1356(%rbx)
.LBB6_17:                               # %updateQPInterlace.exit162
	cmpl	$0, 2968(%rdx)
	je	.LBB6_20
# BB#18:
	movslq	14364(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB6_21
# BB#19:
	addl	15612(%rax), %ecx
	movq	gop_structure(%rip), %rax
	leaq	(%rdx,%rdx,2), %rdx
	subl	-8(%rax,%rdx,8), %ecx
	jmp	.LBB6_21
.LBB6_63:
	movq	generic_RC(%rip), %rdx
	cmpl	$1, 40(%rdx)
	jne	.LBB6_78
# BB#64:
	cmpl	$0, 48(%rdx)
	je	.LBB6_65
.LBB6_78:
	movups	1312(%rbx), %xmm0
	movups	%xmm0, 1296(%rbx)
	movupd	104(%rbx), %xmm0
	movupd	%xmm0, 88(%rbx)
	movl	4(%rdx), %eax
	movl	1388(%rbx), %esi
	testl	%eax, %eax
	setne	%cl
	sarl	%cl, %esi
	cmpl	%esi, 1368(%rbx)
	jne	.LBB6_96
# BB#79:
	movq	input(%rip), %rcx
	cmpl	$2, 4704(%rcx)
	je	.LBB6_81
# BB#80:
	cmpl	$0, 4708(%rcx)
	je	.LBB6_90
.LBB6_81:
	testl	%eax, %eax
	jne	.LBB6_90
# BB#82:
	movl	48(%rdx), %eax
	cmpl	$1, 8(%rdx)
	jne	.LBB6_86
# BB#83:
	movl	1460(%rbx), %ecx
	testl	%eax, %eax
	jle	.LBB6_85
# BB#84:
	addl	%ecx, 44(%rdx)
.LBB6_85:                               # %._crit_edge27.i
	movl	%ecx, 1384(%rbx)
	leaq	1464(%rbx), %rax
	jmp	.LBB6_89
.LBB6_22:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 48(%rcx)
	je	.LBB6_23
.LBB6_27:
	movq	input(%rip), %rax
	cmpl	$2, 4704(%rax)
	je	.LBB6_29
# BB#28:
	cmpl	$0, 4708(%rax)
	je	.LBB6_31
.LBB6_29:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB6_31
# BB#30:
	leaq	1456(%rbx), %rcx
	leaq	1460(%rbx), %rdx
	cmpl	$1, 8(%rax)
	cmoveq	%rdx, %rcx
	movl	(%rcx), %edx
	addl	%edx, 44(%rax)
	movl	(%rcx), %eax
	movl	%eax, 1328(%rbx)
.LBB6_31:
	movq	1312(%rbx), %rax
	movq	%rax, 1296(%rbx)
	movq	1320(%rbx), %rcx
	movq	%rcx, 1304(%rbx)
	movq	104(%rbx), %rdx
	movq	%rdx, 88(%rbx)
	movq	112(%rbx), %rdi
	movq	%rdi, 96(%rbx)
	movsd	120(%rbx), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 80(%rbx)
	movl	1596(%rbx), %r14d
	movl	1328(%rbx), %ebp
	movl	1348(%rbx), %esi
	movd	%rdx, %xmm1
	mulsd	%xmm0, %xmm1
	movd	%rdi, %xmm5
	addsd	%xmm1, %xmm5
	movsd	%xmm5, 1400(%rbx)
	movl	1536(%rbx), %edx
	testl	%edx, %edx
	js	.LBB6_32
# BB#33:
	movd	%rax, %xmm1
	movd	%rcx, %xmm2
	subl	%esi, %edx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	.LCPI6_1(%rip), %xmm3
	divss	%xmm3, %xmm0
	cvttss2si	%xmm0, %eax
	cmpl	%eax, %edx
	cmovll	%eax, %edx
	cvtsi2sdl	%edx, %xmm6
	xorps	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm2
	jne	.LBB6_34
	jnp	.LBB6_38
.LBB6_34:
	movapd	%xmm5, %xmm3
	movhpd	.LCPI6_2(%rip), %xmm3   # xmm3 = xmm3[0],mem[0]
	movdqa	%xmm1, %xmm4
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	mulpd	%xmm3, %xmm4
	movapd	%xmm5, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm4, %xmm2
	movdqa	%xmm1, %xmm3
	unpcklpd	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0]
	mulpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	addsd	%xmm3, %xmm4
	ucomisd	%xmm4, %xmm0
	ja	.LBB6_38
# BB#35:
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB6_37
# BB#36:                                # %call.sqrt
	movapd	%xmm4, %xmm0
	movapd	%xmm6, (%rsp)           # 16-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	(%rsp), %xmm6           # 16-byte Reload
.LBB6_37:                               # %.split
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	movsd	1400(%rbx), %xmm5       # xmm5 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm5, %xmm2
	subsd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jae	.LBB6_38
# BB#39:
	movsd	1304(%rbx), %xmm0       # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm5
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB6_41
# BB#40:                                # %call.sqrt182
	movapd	%xmm4, %xmm0
	movapd	%xmm5, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm5           # 16-byte Reload
.LBB6_41:                               # %.split181
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	mulsd	1400(%rbx), %xmm1
	subsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm5
	jmp	.LBB6_42
.LBB6_20:
	addl	$2, %ecx
	subl	15360(%rax), %ecx
.LBB6_21:
	movl	64(%rbx), %edx
	movl	68(%rbx), %eax
	cmpl	%eax, %ecx
	cmovgel	%ecx, %eax
	cmpl	%edx, %eax
	cmovgl	%edx, %eax
	movl	%eax, 1344(%rbx)
	jmp	.LBB6_112
.LBB6_38:
	mulsd	%xmm1, %xmm5
	divsd	%xmm6, %xmm5
.LBB6_42:                               # %updateModelQPFrame.exit
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm5, %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	Qstep2QP
	leaq	1344(%rbx), %rcx
	movl	64(%rbx), %edx
	movl	68(%rbx), %esi
	cmpl	%esi, %eax
	cmovgel	%eax, %esi
	cmpl	%edx, %esi
	cmovgl	%edx, %esi
	movl	%esi, 1344(%rbx)
	movl	%ebp, %eax
	subl	%r14d, %eax
	addl	%r14d, %ebp
	cmpl	%eax, %esi
	cmovgel	%esi, %eax
	cmpl	%ebp, %eax
	cmovgl	%ebp, %eax
	jmp	.LBB6_43
.LBB6_32:
	addl	%r14d, %ebp
	leaq	1344(%rbx), %rcx
	movl	%ebp, 1344(%rbx)
	movl	64(%rbx), %eax
	movl	68(%rbx), %edx
	cmpl	%edx, %ebp
	cmovll	%edx, %ebp
	cmpl	%eax, %ebp
	cmovgl	%eax, %ebp
	movl	%ebp, %eax
.LBB6_43:
	movl	%eax, (%rcx)
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 4(%rcx)
	jne	.LBB6_112
# BB#44:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 1148(%rdx)
	je	.LBB6_45
.LBB6_25:
	addl	%eax, 44(%rcx)
	movl	1356(%rbx), %ecx
	movl	%ecx, 1352(%rbx)
	movl	%eax, 1356(%rbx)
	movl	%eax, 1328(%rbx)
	jmp	.LBB6_112
.LBB6_96:
	movl	1328(%rbx), %r14d
	movl	28(%rdx), %eax
	movl	1536(%rbx), %ecx
	addl	24(%rdx), %eax
	subl	%eax, %ecx
	movl	%ecx, 1536(%rbx)
	movq	$0, 24(%rdx)
	js	.LBB6_113
# BB#97:
	movq	%rbx, %rdi
	callq	predictCurrPicMAD
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	updateModelQPBU
	movl	1344(%rbx), %eax
	movl	1364(%rbx), %ecx
	addl	%eax, %ecx
	movl	%ecx, 1364(%rbx)
	movl	%eax, 1328(%rbx)
	decl	1368(%rbx)
	jne	.LBB6_112
# BB#98:
	movq	img(%rip), %rdx
	cmpl	$0, 20(%rdx)
	jne	.LBB6_112
# BB#99:
	movl	(%rdx), %edx
	cmpl	start_frame_no_in_this_IGOP(%rip), %edx
	je	.LBB6_112
# BB#100:
	testl	%ebp, %ebp
	je	.LBB6_102
# BB#101:
	movq	generic_RC(%rip), %rdx
	cmpl	$0, 4(%rdx)
	jne	.LBB6_112
.LBB6_102:
	movq	active_sps(%rip), %rsi
	movq	input(%rip), %rdx
	cmpl	$0, 1148(%rsi)
	je	.LBB6_103
.LBB6_105:                              # %._crit_edge.i
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%rbx), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI6_0(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
	movq	generic_RC(%rip), %rsi
	movl	1560(%rdx), %edx
	addl	$-2, %edx
	cmpl	%edx, 48(%rsi)
	jne	.LBB6_107
# BB#106:
	movl	%ecx, 1448(%rbx)
.LBB6_107:
	addl	%ecx, 44(%rsi)
	movl	1356(%rbx), %edx
	movl	%edx, 1352(%rbx)
	movl	%ecx, 1356(%rbx)
	movl	%ecx, 1384(%rbx)
	movl	1376(%rbx), %ecx
	movl	%ecx, 1380(%rbx)
	jmp	.LBB6_112
.LBB6_23:
	movl	72(%rbx), %eax
	movl	%eax, 1344(%rbx)
	cmpl	$0, 4(%rcx)
	jne	.LBB6_112
# BB#24:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 1148(%rdx)
	jne	.LBB6_25
# BB#26:
	movl	%eax, 1460(%rbx)
	jmp	.LBB6_112
.LBB6_65:
	movl	4(%rdx), %ecx
	testl	%ecx, %ecx
	je	.LBB6_68
# BB#66:
	cmpl	$1, %ecx
	jne	.LBB6_53
# BB#67:
	cmpl	$0, 12(%rdx)
	jne	.LBB6_53
.LBB6_68:
	movl	72(%rbx), %eax
	movl	%eax, 1344(%rbx)
	movq	$0, 24(%rdx)
	movl	1368(%rbx), %esi
	decl	%esi
	movl	%esi, 1368(%rbx)
	orl	%ebp, %esi
	jne	.LBB6_77
# BB#69:
	movq	active_sps(%rip), %rsi
	cmpl	$0, 1148(%rsi)
	je	.LBB6_70
.LBB6_72:
	addl	%eax, 44(%rdx)
	movl	1356(%rbx), %ecx
	movl	%ecx, 1352(%rbx)
	movl	%eax, 1356(%rbx)
	movl	%eax, 1384(%rbx)
	movl	1376(%rbx), %ecx
	movl	%ecx, 1380(%rbx)
.LBB6_77:                               # %updateFirstP.exit
	movl	%eax, 1328(%rbx)
	addl	%eax, 1364(%rbx)
	jmp	.LBB6_112
.LBB6_45:
	movl	%eax, 1460(%rbx)
	jmp	.LBB6_112
.LBB6_113:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	updateNegativeTarget    # TAILCALL
.LBB6_86:
	movl	1456(%rbx), %ecx
	testl	%eax, %eax
	jle	.LBB6_88
# BB#87:
	addl	%ecx, 44(%rdx)
.LBB6_88:                               # %._crit_edge.i163
	movl	%ecx, 1384(%rbx)
	leaq	1468(%rbx), %rax
.LBB6_89:
	movl	(%rax), %eax
	movl	%eax, 1380(%rbx)
.LBB6_90:
	movl	1384(%rbx), %ecx
	cmpl	$0, 1536(%rbx)
	jle	.LBB6_91
# BB#94:
	movl	%ecx, 1344(%rbx)
	movl	%ecx, %eax
	jmp	.LBB6_95
.LBB6_91:
	leal	2(%rcx), %eax
	movl	64(%rbx), %edi
	cmpl	%edi, %eax
	cmovgl	%edi, %eax
	movl	%eax, 1344(%rbx)
	testl	%ebp, %ebp
	je	.LBB6_92
.LBB6_93:
	movl	$1, 1504(%rbx)
	jmp	.LBB6_95
.LBB6_51:
	leaq	1456(%rbx), %rcx
.LBB6_52:                               # %updateBottomField.exit
	movl	%eax, (%rcx)
.LBB6_53:
	movl	1344(%rbx), %eax
.LBB6_112:                              # %updateQPNonPicAFF.exit173
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB6_92:
	cmpl	$0, 4(%rdx)
	je	.LBB6_93
.LBB6_95:                               # %updateFirstBU.exit
	addl	%eax, 1364(%rbx)
	decl	%esi
	movl	%esi, 1368(%rbx)
	movl	%ecx, 1328(%rbx)
	jmp	.LBB6_112
.LBB6_70:
	movq	input(%rip), %rsi
	movl	4704(%rsi), %edi
	cmpl	$2, %edi
	je	.LBB6_74
# BB#71:
	cmpl	$1, %edi
	je	.LBB6_72
# BB#73:
	cmpl	$0, 4708(%rsi)
	je	.LBB6_77
.LBB6_74:
	movl	1376(%rbx), %edx
	testl	%ecx, %ecx
	je	.LBB6_75
# BB#76:
	movl	%eax, 1456(%rbx)
	movl	%edx, 1468(%rbx)
	jmp	.LBB6_77
.LBB6_103:
	movl	4704(%rdx), %esi
	cmpl	$2, %esi
	je	.LBB6_109
# BB#104:
	cmpl	$1, %esi
	je	.LBB6_105
# BB#108:
	cmpl	$0, 4708(%rdx)
	je	.LBB6_112
.LBB6_109:
	movq	generic_RC(%rip), %rsi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%rbx), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI6_0(%rip), %xmm0
	cvttsd2si	%xmm0, %edx
	movl	1376(%rbx), %ecx
	cmpl	$0, 4(%rsi)
	je	.LBB6_110
# BB#111:
	movl	%edx, 1456(%rbx)
	movl	%ecx, 1468(%rbx)
	jmp	.LBB6_112
.LBB6_75:
	movl	%eax, 1460(%rbx)
	movl	%edx, 1464(%rbx)
	jmp	.LBB6_77
.LBB6_110:
	movl	%edx, 1460(%rbx)
	movl	%ecx, 1464(%rbx)
	jmp	.LBB6_112
.Lfunc_end6:
	.size	updateQPRC2, .Lfunc_end6-updateQPRC2
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4602678819172646912     # double 0.5
.LCPI7_2:
	.quad	4616189618054758400     # double 4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_1:
	.long	1082130432              # float 4
	.text
	.globl	updateQPRC3
	.p2align	4, 0x90
	.type	updateQPRC3,@function
updateQPRC3:                            # @updateQPRC3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 64
.Lcfi47:
	.cfi_offset %rbx, -48
.Lcfi48:
	.cfi_offset %r12, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %r12
	movq	img(%rip), %rcx
	movl	15352(%rcx), %esi
	movl	15404(%rcx), %edi
	cmpl	%esi, %edi
	je	.LBB7_2
# BB#1:
	cmpl	$0, 20(%rcx)
	je	.LBB7_54
.LBB7_2:
	testl	%ebp, %ebp
	je	.LBB7_3
.LBB7_4:
	movl	(%rcx), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB7_5
# BB#6:
	movl	20(%rcx), %r11d
	testl	%r11d, %r11d
	je	.LBB7_7
.LBB7_17:
	cmpl	%esi, %edi
	setae	%r10b
	testl	%r11d, %r11d
	movq	1312(%r12), %r8
	movq	%r8, 1296(%r12)
	movq	1320(%r12), %r9
	movq	%r9, 1304(%r12)
	movq	104(%r12), %rdx
	movq	%rdx, 88(%r12)
	movq	112(%r12), %rbx
	movq	%rbx, 96(%r12)
	movq	120(%r12), %rax
	movq	%rax, 80(%r12)
	movl	1596(%r12), %r14d
	movl	1328(%r12), %r15d
	movl	1348(%r12), %ebp
	movd	%rdx, %xmm3
	movd	%rbx, %xmm0
	je	.LBB7_20
# BB#18:
	testb	%r10b, %r10b
	jne	.LBB7_20
# BB#19:
	movq	1432(%r12), %rax
	movq	%rax, 80(%r12)
.LBB7_20:
	movd	%rax, %xmm1
	mulsd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, 1400(%r12)
	movl	1536(%r12), %eax
	testl	%eax, %eax
	js	.LBB7_36
# BB#21:
	movd	%r9, %xmm4
	xorl	%ecx, %ecx
	cmpl	$2, %r11d
	cmovel	%ecx, %ebp
	subl	%ebp, %eax
	testl	%r11d, %r11d
	je	.LBB7_24
# BB#22:
	cmpl	%esi, %edi
	jae	.LBB7_25
# BB#23:
	cltd
	idivl	1388(%r12)
	jmp	.LBB7_25
.LBB7_3:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	je	.LBB7_4
# BB#46:
	cmpl	$0, 20(%rcx)
	jne	.LBB7_53
# BB#47:
	cmpl	$0, 12(%rax)
	jne	.LBB7_53
# BB#48:
	movl	(%rcx), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	je	.LBB7_53
# BB#49:
	movq	input(%rip), %rdx
	movl	1344(%r12), %ecx
	cmpl	$1, 4704(%rdx)
	jne	.LBB7_51
# BB#50:
	addl	%ecx, 44(%rax)
	movl	1356(%r12), %eax
	incl	%eax
	movl	%eax, 1352(%r12)
	movl	%ecx, 1356(%r12)
	leaq	1328(%r12), %rax
	jmp	.LBB7_52
.LBB7_54:
	movl	(%rcx), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	jne	.LBB7_55
.LBB7_5:
	movl	72(%r12), %eax
	movl	%eax, 1344(%r12)
	jmp	.LBB7_104
.LBB7_7:
	movq	generic_RC(%rip), %rbp
	cmpl	$0, 48(%rbp)
	je	.LBB7_8
# BB#13:
	movq	input(%rip), %rax
	cmpl	$2, 4704(%rax)
	je	.LBB7_15
# BB#14:
	cmpl	$0, 4708(%rax)
	je	.LBB7_17
.LBB7_15:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB7_17
# BB#16:
	leaq	1456(%r12), %r9
	leaq	1460(%r12), %r8
	cmpl	$1, 8(%rax)
	cmoveq	%r8, %r9
	movl	(%r9), %ebp
	addl	%ebp, 44(%rax)
	movl	(%r9), %eax
	movl	%eax, 1328(%r12)
	jmp	.LBB7_17
.LBB7_36:
	addl	%r14d, %r15d
	movl	64(%r12), %esi
	movl	68(%r12), %edi
	cmpl	%edi, %r15d
	cmovll	%edi, %r15d
	cmpl	%esi, %r15d
	cmovgl	%esi, %r15d
	movl	%r15d, 1344(%r12)
	testl	%r11d, %r11d
	movl	%r15d, %eax
	jne	.LBB7_41
	jmp	.LBB7_37
.LBB7_55:
	movq	generic_RC(%rip), %rdx
	cmpl	$1, 40(%rdx)
	jne	.LBB7_70
# BB#56:
	cmpl	$0, 48(%rdx)
	je	.LBB7_57
.LBB7_70:
	movups	1312(%r12), %xmm0
	movups	%xmm0, 1296(%r12)
	movupd	104(%r12), %xmm0
	movupd	%xmm0, 88(%r12)
	movl	4(%rdx), %eax
	movl	1388(%r12), %esi
	testl	%eax, %eax
	setne	%cl
	sarl	%cl, %esi
	cmpl	%esi, 1368(%r12)
	jne	.LBB7_88
# BB#71:
	movq	input(%rip), %rcx
	cmpl	$2, 4704(%rcx)
	je	.LBB7_73
# BB#72:
	cmpl	$0, 4708(%rcx)
	je	.LBB7_82
.LBB7_73:
	testl	%eax, %eax
	jne	.LBB7_82
# BB#74:
	movl	48(%rdx), %eax
	cmpl	$1, 8(%rdx)
	jne	.LBB7_78
# BB#75:
	movl	1460(%r12), %ecx
	testl	%eax, %eax
	jle	.LBB7_77
# BB#76:
	addl	%ecx, 44(%rdx)
.LBB7_77:                               # %._crit_edge27.i
	movl	%ecx, 1384(%r12)
	leaq	1464(%r12), %rax
	jmp	.LBB7_81
.LBB7_24:
	movss	(%r12), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	.LCPI7_1(%rip), %xmm1
	divss	%xmm1, %xmm0
	cvttss2si	%xmm0, %ecx
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
.LBB7_25:
	movd	%r8, %xmm1
	cvtsi2sdl	%eax, %xmm5
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm4
	jne	.LBB7_26
	jnp	.LBB7_30
.LBB7_26:
	movapd	%xmm3, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm3, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	.LCPI7_2(%rip), %xmm4
	mulsd	%xmm3, %xmm4
	mulsd	%xmm5, %xmm4
	addsd	%xmm2, %xmm4
	ucomisd	%xmm4, %xmm0
	ja	.LBB7_30
# BB#27:
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB7_29
# BB#28:                                # %call.sqrt
	movapd	%xmm4, %xmm0
	movsd	%xmm4, (%rsp)           # 8-byte Spill
	movsd	%xmm5, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm5          # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
.LBB7_29:                               # %.split
	movsd	1296(%r12), %xmm1       # xmm1 = mem[0],zero
	movsd	1400(%r12), %xmm3       # xmm3 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jae	.LBB7_30
# BB#31:
	movsd	1304(%r12), %xmm0       # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB7_33
# BB#32:                                # %call.sqrt138
	movapd	%xmm4, %xmm0
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB7_33:                               # %.split137
	movsd	1296(%r12), %xmm1       # xmm1 = mem[0],zero
	mulsd	1400(%r12), %xmm1
	subsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm3
	jmp	.LBB7_34
.LBB7_30:
	mulsd	%xmm1, %xmm3
	divsd	%xmm5, %xmm3
.LBB7_34:                               # %updateModelQPFrame.exit
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm3, %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	Qstep2QP
	movl	64(%r12), %esi
	movl	68(%r12), %edi
	cmpl	%edi, %eax
	cmovll	%edi, %eax
	cmpl	%esi, %eax
	cmovgl	%esi, %eax
	movl	%eax, 1344(%r12)
	movq	img(%rip), %rcx
	movl	20(%rcx), %r11d
	testl	%r11d, %r11d
	je	.LBB7_35
.LBB7_41:                               # %updateQPNonPicAFF.exit127
	cmpl	$1, %r11d
	jne	.LBB7_104
# BB#42:
	movl	1356(%r12), %ebp
	addl	1352(%r12), %ebp
	sarl	%ebp
	leal	1(%rbp), %edx
	movq	input(%rip), %rbx
	movl	2968(%rbx), %r8d
	testl	%r8d, %r8d
	je	.LBB7_45
# BB#43:
	movslq	14364(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB7_45
# BB#44:
	movq	gop_structure(%rip), %rbx
	leaq	(%rcx,%rcx,2), %rcx
	subl	-8(%rbx,%rcx,8), %eax
	movl	%eax, 1344(%r12)
.LBB7_45:
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	movl	$-5, %ebx
	cmovnel	%ecx, %ebx
	addl	%ebx, %edx
	addl	$6, %ebp
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	cmpl	%ebp, %edx
	cmovgl	%ebp, %edx
	cmpl	%edi, %edx
	cmovll	%edi, %edx
	cmpl	%esi, %edx
	cmovgl	%esi, %edx
	movl	%edx, 1344(%r12)
	movl	%edx, %eax
	jmp	.LBB7_104
.LBB7_35:                               # %.thread134
	movl	%r15d, %ecx
	subl	%r14d, %ecx
	addl	%r14d, %r15d
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	cmpl	%r15d, %ecx
	cmovgl	%r15d, %ecx
	movl	%ecx, 1344(%r12)
	movl	%ecx, %eax
.LBB7_37:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 4(%rcx)
	jne	.LBB7_104
# BB#38:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 1148(%rdx)
	je	.LBB7_40
# BB#39:
	addl	%eax, 44(%rcx)
	jmp	.LBB7_11
.LBB7_8:
	movl	72(%r12), %eax
	movl	%eax, 1344(%r12)
	cmpl	$0, 4(%rbp)
	jne	.LBB7_104
# BB#9:
	movq	active_sps(%rip), %rcx
	cmpl	$0, 1148(%rcx)
	je	.LBB7_12
# BB#10:
	addl	%eax, 44(%rbp)
.LBB7_11:                               # %updateQPNonPicAFF.exit
	movl	1356(%r12), %ecx
	movl	%ecx, 1352(%r12)
	movl	%eax, 1356(%r12)
	movl	%eax, 1328(%r12)
	jmp	.LBB7_104
.LBB7_88:
	movl	1328(%r12), %r14d
	movl	28(%rdx), %eax
	movl	1536(%r12), %ecx
	addl	24(%rdx), %eax
	subl	%eax, %ecx
	movl	%ecx, 1536(%r12)
	movq	$0, 24(%rdx)
	js	.LBB7_105
# BB#89:
	movq	%r12, %rdi
	callq	predictCurrPicMAD
	movq	%r12, %rdi
	movl	%r14d, %edx
	callq	updateModelQPBU
	movl	1344(%r12), %eax
	movl	1364(%r12), %ecx
	addl	%eax, %ecx
	movl	%ecx, 1364(%r12)
	movl	%eax, 1328(%r12)
	decl	1368(%r12)
	jne	.LBB7_104
# BB#90:
	movq	img(%rip), %rdx
	cmpl	$0, 20(%rdx)
	jne	.LBB7_104
# BB#91:
	movl	(%rdx), %edx
	cmpl	start_frame_no_in_this_IGOP(%rip), %edx
	je	.LBB7_104
# BB#92:
	testl	%ebp, %ebp
	je	.LBB7_94
# BB#93:
	movq	generic_RC(%rip), %rdx
	cmpl	$0, 4(%rdx)
	jne	.LBB7_104
.LBB7_94:
	movq	active_sps(%rip), %rsi
	movq	input(%rip), %rdx
	cmpl	$0, 1148(%rsi)
	je	.LBB7_95
.LBB7_97:                               # %._crit_edge.i
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%r12), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI7_0(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
	movq	generic_RC(%rip), %rsi
	movl	1560(%rdx), %edx
	addl	$-2, %edx
	cmpl	%edx, 48(%rsi)
	jne	.LBB7_99
# BB#98:
	movl	%ecx, 1448(%r12)
.LBB7_99:
	addl	%ecx, 44(%rsi)
	movl	1356(%r12), %edx
	movl	%edx, 1352(%r12)
	movl	%ecx, 1356(%r12)
	movl	%ecx, 1384(%r12)
	movl	1376(%r12), %ecx
	movl	%ecx, 1380(%r12)
	jmp	.LBB7_104
.LBB7_57:
	movl	4(%rdx), %ecx
	testl	%ecx, %ecx
	je	.LBB7_60
# BB#58:
	cmpl	$1, %ecx
	jne	.LBB7_53
# BB#59:
	cmpl	$0, 12(%rdx)
	jne	.LBB7_53
.LBB7_60:
	movl	72(%r12), %eax
	movl	%eax, 1344(%r12)
	movq	$0, 24(%rdx)
	movl	1368(%r12), %esi
	decl	%esi
	movl	%esi, 1368(%r12)
	orl	%ebp, %esi
	jne	.LBB7_69
# BB#61:
	movq	active_sps(%rip), %rsi
	cmpl	$0, 1148(%rsi)
	je	.LBB7_62
.LBB7_64:
	addl	%eax, 44(%rdx)
	movl	1356(%r12), %ecx
	movl	%ecx, 1352(%r12)
	movl	%eax, 1356(%r12)
	movl	%eax, 1384(%r12)
	movl	1376(%r12), %ecx
	movl	%ecx, 1380(%r12)
.LBB7_69:                               # %updateFirstP.exit
	movl	%eax, 1328(%r12)
	addl	%eax, 1364(%r12)
	jmp	.LBB7_104
.LBB7_105:
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	updateNegativeTarget    # TAILCALL
.LBB7_40:
	movl	%eax, 1460(%r12)
	jmp	.LBB7_104
.LBB7_78:
	movl	1456(%r12), %ecx
	testl	%eax, %eax
	jle	.LBB7_80
# BB#79:
	addl	%ecx, 44(%rdx)
.LBB7_80:                               # %._crit_edge.i124
	movl	%ecx, 1384(%r12)
	leaq	1468(%r12), %rax
.LBB7_81:
	movl	(%rax), %eax
	movl	%eax, 1380(%r12)
.LBB7_82:
	movl	1384(%r12), %ecx
	cmpl	$0, 1536(%r12)
	jle	.LBB7_83
# BB#86:
	movl	%ecx, 1344(%r12)
	movl	%ecx, %eax
	jmp	.LBB7_87
.LBB7_83:
	leal	2(%rcx), %eax
	movl	64(%r12), %edi
	cmpl	%edi, %eax
	cmovgl	%edi, %eax
	movl	%eax, 1344(%r12)
	testl	%ebp, %ebp
	je	.LBB7_84
.LBB7_85:
	movl	$1, 1504(%r12)
	jmp	.LBB7_87
.LBB7_51:
	leaq	1456(%r12), %rax
.LBB7_52:                               # %updateBottomField.exit
	movl	%ecx, (%rax)
.LBB7_53:
	movl	1344(%r12), %eax
.LBB7_104:                              # %updateQPNonPicAFF.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_12:
	movl	%eax, 1460(%r12)
	jmp	.LBB7_104
.LBB7_84:
	cmpl	$0, 4(%rdx)
	je	.LBB7_85
.LBB7_87:                               # %updateFirstBU.exit
	addl	%eax, 1364(%r12)
	decl	%esi
	movl	%esi, 1368(%r12)
	movl	%ecx, 1328(%r12)
	jmp	.LBB7_104
.LBB7_62:
	movq	input(%rip), %rsi
	movl	4704(%rsi), %edi
	cmpl	$2, %edi
	je	.LBB7_66
# BB#63:
	cmpl	$1, %edi
	je	.LBB7_64
# BB#65:
	cmpl	$0, 4708(%rsi)
	je	.LBB7_69
.LBB7_66:
	movl	1376(%r12), %edx
	testl	%ecx, %ecx
	je	.LBB7_67
# BB#68:
	movl	%eax, 1456(%r12)
	movl	%edx, 1468(%r12)
	jmp	.LBB7_69
.LBB7_95:
	movl	4704(%rdx), %esi
	cmpl	$2, %esi
	je	.LBB7_101
# BB#96:
	cmpl	$1, %esi
	je	.LBB7_97
# BB#100:
	cmpl	$0, 4708(%rdx)
	je	.LBB7_104
.LBB7_101:
	movq	generic_RC(%rip), %rsi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%r12), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI7_0(%rip), %xmm0
	cvttsd2si	%xmm0, %edx
	movl	1376(%r12), %ecx
	cmpl	$0, 4(%rsi)
	je	.LBB7_102
# BB#103:
	movl	%edx, 1456(%r12)
	movl	%ecx, 1468(%r12)
	jmp	.LBB7_104
.LBB7_67:
	movl	%eax, 1460(%r12)
	movl	%edx, 1464(%r12)
	jmp	.LBB7_69
.LBB7_102:
	movl	%edx, 1460(%r12)
	movl	%ecx, 1464(%r12)
	jmp	.LBB7_104
.Lfunc_end7:
	.size	updateQPRC3, .Lfunc_end7-updateQPRC3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1065353216              # float 1
.LCPI8_1:
	.long	3212836864              # float -1
.LCPI8_2:
	.long	1056964608              # float 0.5
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_3:
	.quad	4602678819172646912     # double 0.5
.LCPI8_4:
	.quad	4611794104818444796     # double 2.048
.LCPI8_5:
	.quad	4624633867356078080     # double 15
	.text
	.globl	rc_init_GOP
	.p2align	4, 0x90
	.type	rc_init_GOP,@function
rc_init_GOP:                            # @rc_init_GOP
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 112
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %r12
	movq	input(%rip), %r14
	cmpl	$3, 5136(%r14)
	jne	.LBB8_1
# BB#2:
	movl	2096(%r14), %ecx
	leal	1(%rcx), %eax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$0, 32(%rsp)
	testl	%ecx, %ecx
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	je	.LBB8_3
# BB#4:
	movl	2968(%r14), %esi
	cmpl	$1, %esi
	je	.LBB8_50
# BB#5:
	cmpl	$2, %esi
	je	.LBB8_6
# BB#11:
	cmpl	$3, %esi
	je	.LBB8_51
# BB#12:
	movl	%ecx, 16(%rsp)
	movl	$1, %ebx
	jmp	.LBB8_13
.LBB8_1:                                # %._crit_edge181
	movq	generic_RC(%rip), %r13
	jmp	.LBB8_39
.LBB8_3:                                # %.preheader
	movups	%xmm0, 5184(%r14)
	movups	%xmm0, 5168(%r14)
	movq	$0, 5200(%r14)
	xorl	%ebx, %ebx
	movq	input(%rip), %r14
	jmp	.LBB8_13
.LBB8_50:
	movl	%ecx, %edx
	sarl	%edx
	movl	%edx, 16(%rsp)
	xorl	%esi, %esi
	subl	%edx, %ecx
	cmovsl	%esi, %ecx
	movl	%ecx, 20(%rsp)
	movl	$2, %ebx
	jmp	.LBB8_13
.LBB8_6:
	movl	%eax, %edi
	.p2align	4, 0x90
.LBB8_7:                                # %.preheader150
                                        # =>This Inner Loop Header: Depth=1
	movl	%edi, %esi
	sarl	%esi
	testl	%edi, %edi
	movl	%esi, %edi
	jne	.LBB8_7
# BB#8:                                 # %.preheader148
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jle	.LBB8_13
# BB#9:                                 # %.lr.ph161.preheader
	movl	%eax, %edx
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph161
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	shrl	%esi
	movl	$1, %edi
	movl	%ebx, %ecx
	shll	%cl, %edi
	movl	%edi, 16(%rsp,%rbx,4)
	incq	%rbx
	cmpl	$3, %edx
	movl	%esi, %edx
	ja	.LBB8_10
.LBB8_13:                               # %.loopexit
	movq	generic_RC(%rip), %r13
	movl	%ebx, 112(%r13)
	movl	1560(%r14), %r15d
	testl	%r15d, %r15d
	movl	$1, %ecx
	cmovnel	%r15d, %ecx
	imull	%eax, %ecx
	cvtsi2sdl	%ecx, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	5120(%r14), %xmm0
	divsd	4080(%r14), %xmm0
	mulsd	%xmm1, %xmm0
	testl	%ebx, %ebx
	jle	.LBB8_14
# BB#15:                                # %.lr.ph155.preheader
	movl	%ebx, %eax
	testb	$1, %al
	jne	.LBB8_17
# BB#16:
	xorpd	%xmm1, %xmm1
	xorl	%ecx, %ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	cmpl	$1, %ebx
	jne	.LBB8_19
	jmp	.LBB8_20
.LBB8_14:
	movss	.LCPI8_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	jmp	.LBB8_21
.LBB8_17:                               # %.lr.ph155.prol
	movl	16(%rsp), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	5168(%r14), %xmm1
	cvtsd2ss	%xmm1, %xmm2
	xorpd	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movq	8(%rsp), %rsi           # 8-byte Reload
	imull	%esi, %ecx
	movl	%ecx, 116(%r13)
	movl	$1, %ecx
	cmpl	$1, %ebx
	je	.LBB8_20
	.p2align	4, 0x90
.LBB8_19:                               # %.lr.ph155
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rsp,%rcx,4), %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	mulsd	5168(%r14,%rcx,8), %xmm2
	cvtsd2ss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	imull	%esi, %edx
	movl	%edx, 116(%r13,%rcx,4)
	movl	20(%rsp,%rcx,4), %edx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	mulsd	5176(%r14,%rcx,8), %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	imull	%esi, %edx
	movl	%edx, 120(%r13,%rcx,4)
	addq	$2, %rcx
	cmpq	%rcx, %rax
	jne	.LBB8_19
.LBB8_20:                               # %._crit_edge156.loopexit
	movl	1560(%r14), %r15d
	addss	.LCPI8_0(%rip), %xmm1
.LBB8_21:                               # %._crit_edge156
	cvtsd2ss	%xmm0, %xmm0
	testl	%r15d, %r15d
	jle	.LBB8_23
# BB#22:
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%r15d, %xmm2
	mulss	%xmm2, %xmm1
	movsd	5160(%r14), %xmm2       # xmm2 = mem[0],zero
	cvtsd2ss	%xmm2, %xmm2
	addss	.LCPI8_1(%rip), %xmm2
	addss	%xmm2, %xmm1
.LBB8_23:
	divss	%xmm1, %xmm0
	addss	.LCPI8_2(%rip), %xmm0
	callq	floorf
	cvttss2si	%xmm0, %eax
	movl	%eax, 84(%r13)
	testl	%r15d, %r15d
	je	.LBB8_24
# BB#25:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	5160(%r14), %xmm0
	addsd	.LCPI8_3(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
	jmp	.LBB8_26
.LBB8_24:
	xorl	%ecx, %ecx
.LBB8_26:
	movl	%ecx, 88(%r13)
	testl	%ebx, %ebx
	jle	.LBB8_35
# BB#27:                                # %.lr.ph.preheader
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	5168(%r14), %xmm0
	addsd	.LCPI8_3(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, 92(%r13)
	cmpl	$1, %ebx
	je	.LBB8_34
# BB#28:                                # %._crit_edge177.preheader
	movl	%ebx, %r15d
	testb	$1, %r15b
	jne	.LBB8_29
# BB#30:                                # %._crit_edge177.prol
	xorps	%xmm0, %xmm0
	cvtsi2sdl	84(%r13), %xmm0
	mulsd	5176(%r14), %xmm0
	addsd	.LCPI8_3(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, 96(%r13)
	movl	$2, %eax
	cmpl	$2, %ebx
	jne	.LBB8_32
	jmp	.LBB8_34
.LBB8_29:
	movl	$1, %eax
	cmpl	$2, %ebx
	je	.LBB8_34
.LBB8_32:                               # %._crit_edge177.preheader.new
	subq	%rax, %r15
	leaq	96(%r13,%rax,4), %rbx
	leaq	5176(%r14,%rax,8), %rbp
	.p2align	4, 0x90
.LBB8_33:                               # %._crit_edge177
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	84(%r13), %xmm0
	mulsd	-8(%rbp), %xmm0
	movsd	.LCPI8_3(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, -4(%rbx)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	84(%r13), %xmm0
	mulsd	(%rbp), %xmm0
	addsd	.LCPI8_3(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, (%rbx)
	addq	$8, %rbx
	addq	$16, %rbp
	addq	$-2, %r15
	jne	.LBB8_33
.LBB8_34:                               # %._crit_edge.loopexit
	movl	1560(%r14), %r15d
.LBB8_35:                               # %._crit_edge
	testl	%r15d, %r15d
	movl	8(%r14), %ecx
	je	.LBB8_36
# BB#37:
	leal	-1(%rcx), %eax
	cltd
	idivl	%r15d
	jmp	.LBB8_38
.LBB8_36:
	xorl	%eax, %eax
.LBB8_38:                               # %._crit_edge._crit_edge
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movl	%eax, 140(%r13)
	decl	%ecx
	subl	%eax, %ecx
	movl	%ecx, 136(%r13)
.LBB8_39:
	movl	80(%r13), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm3
	leal	1(%rbx,%r15), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	cvtsi2ssl	%eax, %xmm4
	mulss	%xmm1, %xmm4
	divss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	cvttss2si	%xmm1, %eax
	movl	%eax, 1564(%r12)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	.LCPI8_4(%rip), %xmm3
	addsd	%xmm0, %xmm3
	cvttsd2si	%xmm3, %eax
	movl	%eax, 1556(%r12)
	divss	%xmm2, %xmm4
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm4, %xmm0
	addsd	.LCPI8_3(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	addl	%ebp, %eax
	movl	%eax, 80(%r13)
	movl	%ebx, 1544(%r12)
	movl	%r15d, 1548(%r12)
	movl	$0, 1504(%r12)
	movl	4704(%r14), %eax
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB8_42
# BB#40:
	cmpl	$0, 4708(%r14)
	je	.LBB8_42
# BB#41:
	movl	5128(%r14), %edx
	movq	img(%rip), %rsi
	xorl	%ecx, %ecx
	cmpl	15352(%rsi), %edx
	setne	%cl
.LBB8_42:
	movl	%ecx, 12(%r13)
	movl	%ebx, 1592(%r12)
	movl	40(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 40(%r13)
	testl	%ecx, %ecx
	je	.LBB8_43
# BB#44:
	cmpl	$2, %eax
	je	.LBB8_47
# BB#45:
	cmpl	$0, 4708(%r14)
	je	.LBB8_46
.LBB8_47:
	leaq	1456(%r12), %rcx
	leaq	1460(%r12), %rax
	movl	44(%r13), %edx
	cmpl	$1, 8(%r13)
	cmoveq	%rax, %rcx
	leaq	44(%r13), %rax
	addl	(%rcx), %edx
	movl	%edx, 44(%r13)
	movl	(%rcx), %ecx
	movl	%ecx, 1448(%r12)
.LBB8_48:
	movl	4(%rsp), %esi           # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	48(%r13), %xmm1
	addq	$48, %r13
	divsd	%xmm1, %xmm0
	movsd	.LCPI8_3(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	divsd	.LCPI8_5(%rip), %xmm0
	addsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %esi
	cmpl	$3, %esi
	movl	$2, %edi
	cmovll	%esi, %edi
	subl	%edi, %edx
	addl	$-2, %ecx
	xorl	%esi, %esi
	cmpl	%ecx, %edx
	movl	$-1, %ecx
	cmovlel	%esi, %ecx
	addl	%edx, %ecx
	movl	1452(%r12), %edx
	leal	-2(%rdx), %esi
	addl	$2, %edx
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	movl	64(%r12), %edx
	movl	68(%r12), %esi
	cmpl	%esi, %ecx
	cmovll	%esi, %ecx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	movl	%ecx, 76(%r12)
	movl	%ecx, 72(%r12)
	movl	%ecx, 1328(%r12)
	movl	%ecx, 1384(%r12)
	movl	%ecx, 1452(%r12)
	movl	1356(%r12), %edx
	movl	%edx, 1352(%r12)
	decl	%ecx
	movl	%ecx, 1356(%r12)
	jmp	.LBB8_49
.LBB8_43:
	movl	5124(%r14), %eax
	movl	%eax, 72(%r12)
	leal	-1(%rax), %ecx
	movl	%ecx, 1356(%r12)
	movl	%eax, 1452(%r12)
	movl	%eax, 1384(%r12)
	movl	%eax, 1344(%r12)
	movl	%eax, 1456(%r12)
	movl	%eax, 1460(%r12)
	movl	%eax, 76(%r12)
	leaq	44(%r13), %rax
	addq	$48, %r13
.LBB8_49:
	movl	$0, (%rax)
	movl	$0, (%r13)
	movl	$0, 1360(%r12)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_46:                               # %._crit_edge183
	leaq	44(%r13), %rax
	movl	44(%r13), %edx
	movl	1448(%r12), %ecx
	jmp	.LBB8_48
.LBB8_51:
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$72, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end8:
	.size	rc_init_GOP, .Lfunc_end8-rc_init_GOP
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	1069547520              # float 1.5
.LCPI9_4:
	.long	1056964608              # float 0.5
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_1:
	.quad	4602678819172646912     # double 0.5
.LCPI9_2:
	.quad	4619567317775286272     # double 7
.LCPI9_3:
	.quad	4593671619917905920     # double 0.125
.LCPI9_5:
	.quad	4616189618054758400     # double 4
.LCPI9_6:
	.quad	4603579539098121011     # double 0.59999999999999998
	.text
	.globl	rc_init_pict
	.p2align	4, 0x90
	.type	rc_init_pict,@function
rc_init_pict:                           # @rc_init_pict
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 80
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movaps	%xmm0, %xmm6
	movl	%edx, %r15d
	movq	%rdi, %r14
	movq	input(%rip), %r13
	movl	4708(%r13), %ebx
	testl	%ebx, %ebx
	movq	img(%rip), %rbp
	je	.LBB9_2
# BB#1:
	movl	15352(%rbp), %eax
	xorl	%edx, %edx
	divl	15404(%rbp)
	movl	%eax, 1388(%r14)
.LBB9_2:                                # %._crit_edge
	movl	$0, 15388(%rbp)
	cmpl	$1, 5132(%r13)
	jne	.LBB9_7
# BB#3:
	movq	generic_RC(%rip), %rax
	movl	36(%rax), %eax
	cmpl	$59, %eax
	je	.LBB9_6
# BB#4:
	cmpl	$58, %eax
	jne	.LBB9_7
# BB#5:
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	.LCPI9_0(%rip), %xmm0
	movss	%xmm0, (%r14)
	jmp	.LBB9_7
.LBB9_6:
	movl	(%r14), %eax
	movl	%eax, 8(%r14)
.LBB9_7:
	movl	%r15d, %r12d
	orl	%esi, %r12d
	je	.LBB9_109
# BB#8:
	testl	%ecx, %ecx
	je	.LBB9_109
# BB#9:
	movl	20(%rbp), %eax
	testl	%eax, %eax
	je	.LBB9_11
# BB#10:
	cmpl	$1, 5136(%r13)
	jne	.LBB9_46
.LBB9_11:
	movl	(%rbp), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	jne	.LBB9_12
.LBB9_46:
	cmpl	$1, %eax
	jne	.LBB9_58
# BB#47:
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB9_49
	jnp	.LBB9_48
.LBB9_49:
	subss	%xmm1, %xmm0
	movl	1548(%r14), %eax
	addl	1544(%r14), %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	mulss	%xmm0, %xmm1
	divss	4(%r14), %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	addsd	.LCPI9_1(%rip), %xmm0
	movl	%esi, %ebx
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	callq	floor
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movl	%ebx, %esi
	cvttsd2si	%xmm0, %eax
	movq	generic_RC(%rip), %rcx
	addl	%eax, 80(%rcx)
	jmp	.LBB9_50
.LBB9_12:
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB9_14
	jnp	.LBB9_13
.LBB9_14:
	subss	%xmm1, %xmm0
	movl	1548(%r14), %eax
	addl	1544(%r14), %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	mulss	%xmm0, %xmm1
	divss	4(%r14), %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	addsd	.LCPI9_1(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %ecx
	movq	generic_RC(%rip), %rax
	addl	%ecx, 80(%rax)
	jmp	.LBB9_15
.LBB9_13:                               # %._crit_edge180
	movq	generic_RC(%rip), %rax
.LBB9_15:
	movl	15404(%rbp), %ecx
	cmpl	15352(%rbp), %ecx
	jne	.LBB9_19
# BB#16:
	movl	48(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB9_18
# BB#17:
	xorps	%xmm0, %xmm0
	cvtsi2sdq	72(%rax), %xmm0
	movapd	%xmm0, %xmm1
	subsd	32(%r14), %xmm1
	movl	1592(%r14), %ecx
	decl	%ecx
	cvtsi2sdl	%ecx, %xmm2
	divsd	%xmm2, %xmm1
	movsd	%xmm1, 1584(%r14)
	subsd	%xmm1, %xmm0
	movsd	%xmm0, 40(%r14)
	movl	8(%rsp), %esi           # 4-byte Reload
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	jmp	.LBB9_37
.LBB9_19:
	cmpl	$0, 36(%rax)
	jle	.LBB9_26
# BB#20:
	testl	%ebx, %ebx
	setne	%cl
	cmpl	$2, 4704(%r13)
	je	.LBB9_22
# BB#21:
	testb	%cl, %cl
	je	.LBB9_24
.LBB9_22:
	cmpl	$1, 4(%rax)
	jne	.LBB9_24
# BB#23:
	movq	1488(%r14), %rsi
	movq	1496(%r14), %rdi
	jmp	.LBB9_25
.LBB9_48:                               # %._crit_edge182
	movq	generic_RC(%rip), %rcx
.LBB9_50:
	movl	32(%rcx), %eax
	cmpl	$1, 36(%rcx)
	jne	.LBB9_53
# BB#51:
	cmpl	$1, %eax
	jne	.LBB9_53
# BB#52:
	movupd	1568(%r14), %xmm0
	movupd	%xmm0, 48(%r14)
	jmp	.LBB9_58
.LBB9_53:                               # %._crit_edge184
	cmpl	$2, %eax
	jl	.LBB9_58
# BB#54:
	movsd	56(%r14), %xmm0         # xmm0 = mem[0],zero
	movsd	1576(%r14), %xmm1       # xmm1 = mem[0],zero
	cmpl	$7, %eax
	jg	.LBB9_56
# BB#55:
	leal	-1(%rax), %ecx
	cvtsi2sdl	%ecx, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	divsd	%xmm1, %xmm0
	jmp	.LBB9_57
.LBB9_18:
	cmpl	$2, %ecx
	movl	8(%rsp), %esi           # 4-byte Reload
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	jge	.LBB9_35
	jmp	.LBB9_37
.LBB9_24:
	movq	1472(%r14), %rdi
	movq	1480(%r14), %rsi
.LBB9_25:
	movslq	1388(%r14), %rdx
	shlq	$3, %rdx
	callq	memcpy
.LBB9_26:
	movq	generic_RC(%rip), %rax
	movl	40(%rax), %ecx
	cmpl	$1, %ecx
	movl	8(%rsp), %esi           # 4-byte Reload
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	jne	.LBB9_30
# BB#27:
	movl	48(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB9_29
# BB#28:
	xorps	%xmm0, %xmm0
	cvtsi2sdq	72(%rax), %xmm0
	movapd	%xmm0, %xmm1
	subsd	32(%r14), %xmm1
	movl	1592(%r14), %ecx
	decl	%ecx
	cvtsi2sdl	%ecx, %xmm2
	jmp	.LBB9_33
.LBB9_30:
	cmpl	$2, %ecx
	jl	.LBB9_37
# BB#31:
	movl	48(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB9_32
# BB#34:
	jg	.LBB9_35
	jmp	.LBB9_37
.LBB9_29:
	cmpl	$2, %ecx
	jl	.LBB9_37
.LBB9_35:
	movsd	40(%r14), %xmm0         # xmm0 = mem[0],zero
	subsd	1584(%r14), %xmm0
	jmp	.LBB9_36
.LBB9_56:
	mulsd	.LCPI9_2(%rip), %xmm0
	addsd	%xmm0, %xmm1
	mulsd	.LCPI9_3(%rip), %xmm1
	movapd	%xmm1, %xmm0
.LBB9_57:
	movsd	%xmm0, 56(%r14)
	jmp	.LBB9_58
.LBB9_32:
	xorps	%xmm0, %xmm0
	cvtsi2sdq	72(%rax), %xmm0
	movapd	%xmm0, %xmm1
	subsd	32(%r14), %xmm1
	cvtsi2sdl	1592(%r14), %xmm2
.LBB9_33:
	divsd	%xmm2, %xmm1
	movsd	%xmm1, 1584(%r14)
	subsd	%xmm1, %xmm0
.LBB9_36:
	movsd	%xmm0, 40(%r14)
.LBB9_37:
	movl	36(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB9_39
# BB#38:                                # %.thread192
	movq	1568(%r14), %rax
	movq	%rax, 48(%r14)
	jmp	.LBB9_44
.LBB9_39:
	leal	-2(%rcx), %eax
	cmpl	$5, %eax
	ja	.LBB9_41
# BB#40:
	leaq	48(%r14), %rax
	leal	-1(%rcx), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	1568(%r14), %xmm0
	addsd	48(%r14), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	divsd	%xmm1, %xmm0
	jmp	.LBB9_43
.LBB9_41:
	cmpl	$2, %ecx
	jl	.LBB9_44
# BB#42:
	leaq	48(%r14), %rax
	movsd	48(%r14), %xmm0         # xmm0 = mem[0],zero
	mulsd	.LCPI9_2(%rip), %xmm0
	addsd	1568(%r14), %xmm0
	mulsd	.LCPI9_3(%rip), %xmm0
.LBB9_43:                               # %.sink.split
	movsd	%xmm0, (%rax)
.LBB9_44:
	movq	input(%rip), %r13
	movl	2096(%r13), %eax
	testl	%eax, %eax
	jle	.LBB9_58
# BB#45:
	movsd	48(%r14), %xmm0         # xmm0 = mem[0],zero
	leal	1(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%r14), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm4
	mulsd	%xmm1, %xmm4
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm3, %xmm1
	cvtsi2sdl	%eax, %xmm5
	mulsd	56(%r14), %xmm5
	addsd	%xmm0, %xmm5
	mulsd	%xmm1, %xmm5
	divsd	%xmm5, %xmm4
	divss	%xmm3, %xmm2
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm2, %xmm0
	subsd	%xmm0, %xmm4
	addsd	40(%r14), %xmm4
	movsd	%xmm4, 40(%r14)
.LBB9_58:
	movq	img(%rip), %rbp
	movl	20(%rbp), %ebx
	testl	%ebx, %ebx
	je	.LBB9_60
# BB#59:
	movl	5136(%r13), %eax
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB9_109
.LBB9_60:
	movl	(%rbp), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB9_109
# BB#61:
	movl	15404(%rbp), %eax
	cmpl	15352(%rbp), %eax
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	je	.LBB9_63
# BB#62:
	movl	5136(%r13), %edx
	cmpl	$3, %edx
	jne	.LBB9_95
.LBB9_63:
	movq	generic_RC(%rip), %r15
	movl	5136(%r13), %edx
	cmpl	$0, 36(%r15)
	jle	.LBB9_100
# BB#64:
	cmpl	$3, %edx
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	jne	.LBB9_94
# BB#65:
	cmpl	$1, %ebx
	jne	.LBB9_70
# BB#66:
	cmpl	$0, 2968(%r13)
	je	.LBB9_67
# BB#68:
	movl	112(%r15), %eax
	decl	%eax
	movq	gop_structure(%rip), %rcx
	movslq	14364(%rbp), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	subl	-8(%rcx,%rdx,8), %eax
	cltq
	leaq	92(%r15,%rax,4), %rax
	jmp	.LBB9_73
.LBB9_95:
	movq	generic_RC(%rip), %r15
	movl	40(%r15), %eax
	cmpl	$1, %eax
	jne	.LBB9_97
# BB#96:
	cmpl	$0, 36(%r15)
	jg	.LBB9_98
	jmp	.LBB9_100
.LBB9_94:
	movsd	1568(%r14), %xmm1       # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	cvtsi2sdl	80(%r15), %xmm0
	mulsd	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	cvtsi2sdl	1544(%r14), %xmm2
	mulsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	1548(%r14), %xmm1
	mulsd	1576(%r14), %xmm1
	addsd	%xmm2, %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI9_1(%rip), %xmm0
	jmp	.LBB9_99
.LBB9_97:
	cmpl	$2, %eax
	jl	.LBB9_100
.LBB9_98:
	movsd	1568(%r14), %xmm1       # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	cvtsi2sdl	80(%r15), %xmm0
	mulsd	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	cvtsi2sdl	1544(%r14), %xmm2
	mulsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	1548(%r14), %xmm1
	mulsd	1576(%r14), %xmm1
	addsd	%xmm2, %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI9_1(%rip), %xmm0
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
.LBB9_99:                               # %.thread194
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	movl	%edx, 16(%rsp)          # 4-byte Spill
	callq	floor
	cvttsd2si	%xmm0, %ebx
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	divss	4(%r14), %xmm0
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdq	72(%r15), %xmm1
	subsd	40(%r14), %xmm1
	mulsd	16(%r14), %xmm1
	subsd	%xmm1, %xmm0
	addsd	.LCPI9_1(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmovnsl	%eax, %ecx
	subl	%ecx, %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	24(%r14), %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	addsd	%xmm1, %xmm0
	addsd	.LCPI9_1(%rip), %xmm0
	callq	floor
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	20(%rsp), %ebx          # 4-byte Reload
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movl	8(%rsp), %esi           # 4-byte Reload
	cvttsd2si	%xmm0, %eax
	movl	%eax, 1536(%r14)
.LBB9_100:                              # %.thread194
	movl	12(%rsp), %r15d         # 4-byte Reload
.LBB9_101:                              # %.thread194
	xorps	%xmm0, %xmm0
	cvtsi2ssl	1536(%r14), %xmm0
	mulss	%xmm6, %xmm0
	cvttss2si	%xmm0, %eax
	movl	%eax, 1536(%r14)
	cmpl	$3, %edx
	jne	.LBB9_103
# BB#102:                               # %.thread194
	testl	%ebx, %ebx
	jne	.LBB9_104
.LBB9_103:
	movl	1560(%r14), %ecx
	movl	1564(%r14), %edx
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	cmpl	%ecx, %edx
	cmovgl	%ecx, %edx
	movl	%edx, 1536(%r14)
	movl	%edx, %eax
.LBB9_104:
	testl	%r15d, %r15d
	je	.LBB9_105
.LBB9_108:
	movl	%eax, 1540(%r14)
.LBB9_109:
	testl	%r12d, %r12d
	je	.LBB9_112
# BB#110:
	movq	generic_RC(%rip), %rax
	movl	$0, 16(%rax)
	movl	$0, 20(%rax)
	movl	15404(%rbp), %ecx
	cmpl	15352(%rbp), %ecx
	jae	.LBB9_112
# BB#111:
	movl	$0, 1364(%r14)
	movl	$0, 24(%rax)
	movl	$0, 28(%rax)
	movq	$0, 56(%rax)
	cmpl	$0, 4(%rax)
	movl	1388(%r14), %eax
	setne	%cl
	sarl	%cl, %eax
	movl	%eax, 1368(%r14)
.LBB9_112:
	cmpl	$0, 20(%rbp)
	je	.LBB9_114
# BB#113:
	cmpl	$1, 5136(%r13)
	jne	.LBB9_120
.LBB9_114:
	movl	15404(%rbp), %eax
	cmpl	15352(%rbp), %eax
	jae	.LBB9_120
# BB#115:
	movq	generic_RC(%rip), %rax
	cmpl	$1, 4(%rax)
	jne	.LBB9_120
# BB#116:
	movl	(%rbp), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	je	.LBB9_120
# BB#117:
	movl	1540(%r14), %ecx
	testl	%r15d, %r15d
	je	.LBB9_119
# BB#118:
	movl	$0, 1552(%r14)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	.LCPI9_6(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, 1536(%r14)
	jmp	.LBB9_120
.LBB9_105:
	testl	%esi, %esi
	je	.LBB9_109
# BB#106:
	cmpl	$2, 4704(%r13)
	je	.LBB9_108
# BB#107:
	cmpl	$0, 4708(%r13)
	jne	.LBB9_108
	jmp	.LBB9_109
.LBB9_119:
	subl	1552(%r14), %ecx
	movl	%ecx, 1536(%r14)
	movl	$0, 24(%rax)
	movl	$0, 28(%rax)
	movq	$0, 56(%rax)
	movl	1388(%r14), %eax
	sarl	%eax
	movl	%eax, 1368(%r14)
.LBB9_120:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_70:
	testl	%ebx, %ebx
	je	.LBB9_71
# BB#72:
	leaq	88(%r15), %rax
	jmp	.LBB9_73
.LBB9_67:
	xorl	%eax, %eax
	leaq	92(%r15,%rax,4), %rax
	jmp	.LBB9_73
.LBB9_71:
	leaq	84(%r15), %rax
.LBB9_73:
	movl	(%rax), %eax
	movl	84(%r15), %esi
	movl	88(%r15), %ecx
	imull	140(%r15), %ecx
	imull	136(%r15), %esi
	addl	%ecx, %esi
	cmpl	$0, 2968(%r13)
	je	.LBB9_87
# BB#74:                                # %.preheader
	movslq	112(%r15), %rcx
	testq	%rcx, %rcx
	jle	.LBB9_88
# BB#75:                                # %.lr.ph
	cmpl	$8, %ecx
	jae	.LBB9_77
# BB#76:
	xorl	%edx, %edx
	jmp	.LBB9_86
.LBB9_87:
	movl	92(%r15), %ecx
	imull	116(%r15), %ecx
	addl	%ecx, %esi
	jmp	.LBB9_88
.LBB9_77:                               # %min.iters.checked
	movq	%rcx, %rdx
	andq	$-8, %rdx
	je	.LBB9_78
# BB#79:                                # %vector.ph
	movd	%esi, %xmm2
	leaq	-8(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	btl	$3, %edi
	jb	.LBB9_80
# BB#81:                                # %vector.body.prol
	movdqu	92(%r15), %xmm0
	movdqu	108(%r15), %xmm1
	movdqu	116(%r15), %xmm3
	movdqu	132(%r15), %xmm4
	pshufd	$245, %xmm3, %xmm5      # xmm5 = xmm3[1,1,3,3]
	pshufd	$245, %xmm0, %xmm6      # xmm6 = xmm0[1,1,3,3]
	pmuludq	%xmm5, %xmm6
	pshufd	$232, %xmm6, %xmm5      # xmm5 = xmm6[0,2,2,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	pshufd	$245, %xmm4, %xmm3      # xmm3 = xmm4[1,1,3,3]
	pshufd	$245, %xmm1, %xmm5      # xmm5 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	pmuludq	%xmm4, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	paddd	%xmm2, %xmm0
	movl	$8, %edi
	movdqa	%xmm0, %xmm2
	testq	%rsi, %rsi
	jne	.LBB9_83
	jmp	.LBB9_85
.LBB9_78:
	xorl	%edx, %edx
	jmp	.LBB9_86
.LBB9_80:
	xorpd	%xmm1, %xmm1
	xorl	%edi, %edi
                                        # implicit-def: %XMM0
	testq	%rsi, %rsi
	je	.LBB9_85
.LBB9_83:                               # %vector.ph.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	164(%r15,%rdi,4), %rdi
	movdqa	%xmm2, %xmm0
.LBB9_84:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-32(%rdi), %xmm8
	movdqu	-72(%rdi), %xmm2
	movdqu	-56(%rdi), %xmm3
	movdqu	-48(%rdi), %xmm5
	movdqu	-40(%rdi), %xmm6
	pshufd	$245, %xmm5, %xmm7      # xmm7 = xmm5[1,1,3,3]
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm7, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm5, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	pshufd	$245, %xmm3, %xmm4      # xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm8, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm8, %xmm5      # xmm5 = xmm8[1,1,3,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm4      # xmm4 = xmm5[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movdqu	-16(%rdi), %xmm1
	movdqu	(%rdi), %xmm4
	movdqu	-24(%rdi), %xmm5
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm1, %xmm6
	pshufd	$232, %xmm6, %xmm0      # xmm0 = xmm6[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm7, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	pshufd	$245, %xmm5, %xmm6      # xmm6 = xmm5[1,1,3,3]
	pmuludq	%xmm4, %xmm5
	pshufd	$232, %xmm5, %xmm1      # xmm1 = xmm5[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	paddd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	addq	$64, %rdi
	addq	$-16, %rsi
	jne	.LBB9_84
.LBB9_85:                               # %middle.block
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %esi
	cmpq	%rdx, %rcx
	je	.LBB9_88
	.p2align	4, 0x90
.LBB9_86:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	92(%r15,%rdx,4), %edi
	imull	116(%r15,%rdx,4), %edi
	addl	%edi, %esi
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB9_86
.LBB9_88:                               # %.loopexit
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	80(%r15), %xmm1
	mulsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%esi, %xmm1
	divss	%xmm1, %xmm0
	addss	.LCPI9_4(%rip), %xmm0
	callq	floorf
	cvttss2si	%xmm0, %eax
	movl	%eax, 1536(%r14)
	movl	$3, %edx
	cmpl	$2, %ebx
	je	.LBB9_92
# BB#89:                                # %.loopexit
	cmpl	$1, %ebx
	jne	.LBB9_90
# BB#91:
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	divsd	5152(%r13), %xmm0
	jmp	.LBB9_93
.LBB9_92:
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	5144(%r13), %xmm1       # xmm1 = mem[0],zero
	mulsd	.LCPI9_5(%rip), %xmm1
	divsd	%xmm1, %xmm0
.LBB9_93:                               # %.sink.split14
	movl	12(%rsp), %r15d         # 4-byte Reload
	addsd	.LCPI9_1(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	%eax, 1536(%r14)
	movl	8(%rsp), %esi           # 4-byte Reload
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movl	$3, %edx
	jmp	.LBB9_101
.LBB9_90:
	movl	12(%rsp), %r15d         # 4-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	jmp	.LBB9_101
.Lfunc_end9:
	.size	rc_init_pict, .Lfunc_end9-rc_init_pict
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_0:
	.long	1056964608              # float 0.5
.LCPI10_1:
	.long	1063675494              # float 0.899999976
	.text
	.globl	rc_update_pict
	.p2align	4, 0x90
	.type	rc_update_pict,@function
rc_update_pict:                         # @rc_update_pict
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi80:
	.cfi_def_cfa_offset 32
.Lcfi81:
	.cfi_offset %rbx, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	divss	4(%rbx), %xmm0
	addss	.LCPI10_0(%rip), %xmm0
	callq	floorf
	cvttss2si	%xmm0, %eax
	movq	generic_RC(%rip), %rcx
	subl	%ebp, 80(%rcx)
	subl	%eax, %ebp
	movslq	%ebp, %rax
	addq	%rax, 72(%rcx)
	subl	%eax, 1564(%rbx)
	movl	1556(%rbx), %ecx
	subl	%eax, %ecx
	movl	%ecx, 1556(%rbx)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	mulss	.LCPI10_1(%rip), %xmm0
	cvttss2si	%xmm0, %eax
	movl	%eax, 1560(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end10:
	.size	rc_update_pict, .Lfunc_end10-rc_update_pict
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	updateComplexity
	.p2align	4, 0x90
	.type	updateComplexity,@function
updateComplexity:                       # @updateComplexity
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 16
	movq	img(%rip), %rcx
	movl	15404(%rcx), %eax
	cmpl	15352(%rcx), %eax
	jne	.LBB11_1
.LBB11_6:
	imull	1344(%rdi), %edx
	cvtsi2sdl	%edx, %xmm0
	jmp	.LBB11_7
.LBB11_1:
	testl	%esi, %esi
	je	.LBB11_5
# BB#2:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 12(%rax)
	sete	%sil
	movl	4(%rax), %ecx
	cmpl	$1, %ecx
	sete	%al
	testb	%al, %sil
	jne	.LBB11_4
# BB#3:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.LBB11_8
.LBB11_4:
	cvtsi2sdl	1364(%rdi), %xmm1
	cvtsi2sdl	1388(%rdi), %xmm0
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	%xmm1, %xmm0
.LBB11_7:
	addsd	.LCPI11_0(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
.LBB11_8:
	popq	%rcx
	retq
.LBB11_5:
	xorl	%eax, %eax
	cmpl	$1, 20(%rcx)
	je	.LBB11_6
	jmp	.LBB11_8
.Lfunc_end11:
	.size	updateComplexity, .Lfunc_end11-updateComplexity
	.cfi_endproc

	.globl	updatePparams
	.p2align	4, 0x90
	.type	updatePparams,@function
updatePparams:                          # @updatePparams
	.cfi_startproc
# BB#0:
	movl	%esi, 1528(%rdi)
	decl	1544(%rdi)
	cvtsi2sdl	%esi, %xmm0
	movsd	%xmm0, 1568(%rdi)
	movq	generic_RC(%rip), %rax
	movl	16(%rax), %ecx
	movl	%ecx, 1332(%rdi)
	incl	36(%rax)
	incl	48(%rax)
	retq
.Lfunc_end12:
	.size	updatePparams, .Lfunc_end12-updatePparams
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_0:
	.long	1068403314              # float 1.36360002
	.text
	.globl	updateBparams
	.p2align	4, 0x90
	.type	updateBparams,@function
updateBparams:                          # @updateBparams
	.cfi_startproc
# BB#0:
	movl	%esi, 1532(%rdi)
	decl	1548(%rdi)
	cvtsi2ssl	%esi, %xmm0
	divss	.LCPI13_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 1576(%rdi)
	incl	1360(%rdi)
	movq	generic_RC(%rip), %rax
	incl	32(%rax)
	retq
.Lfunc_end13:
	.size	updateBparams, .Lfunc_end13-updateBparams
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_1:
	.long	1068403314              # float 1.36360002
	.text
	.globl	rc_update_pict_frame
	.p2align	4, 0x90
	.type	rc_update_pict_frame,@function
rc_update_pict_frame:                   # @rc_update_pict_frame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 48
.Lcfi89:
	.cfi_offset %rbx, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	input(%rip), %rbx
	movl	5136(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB14_21
# BB#1:
	cmpl	$3, %eax
	jne	.LBB14_2
# BB#31:
	movq	img(%rip), %r15
	movl	20(%r15), %ebp
	testl	%ebp, %ebp
	je	.LBB14_33
# BB#32:
	xorl	%eax, %eax
	jmp	.LBB14_34
.LBB14_21:
	movq	img(%rip), %rbx
	movl	15404(%rbx), %eax
	cmpl	15352(%rbx), %eax
	jne	.LBB14_23
# BB#22:
	imull	1344(%r14), %esi
	cvtsi2sdl	%esi, %xmm0
	addsd	.LCPI14_0(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movl	(%rbx), %ebx
	movl	start_frame_no_in_this_IGOP(%rip), %ebp
	cmpl	%ebp, %ebx
	jne	.LBB14_28
	jmp	.LBB14_57
.LBB14_2:
	movq	img(%rip), %rbx
	movl	20(%rbx), %ebp
	testl	%ebp, %ebp
	je	.LBB14_4
# BB#3:
	xorl	%eax, %eax
	jmp	.LBB14_5
.LBB14_23:
	movl	(%rbx), %ebx
	movl	start_frame_no_in_this_IGOP(%rip), %ebp
	cmpl	%ebp, %ebx
	je	.LBB14_57
# BB#24:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 12(%rax)
	sete	%cl
	movl	4(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	testl	%eax, %eax
	je	.LBB14_26
# BB#25:
	xorl	%eax, %eax
	andb	%dl, %cl
	je	.LBB14_27
.LBB14_26:
	cvtsi2sdl	1364(%r14), %xmm1
	cvtsi2sdl	1388(%r14), %xmm0
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LCPI14_0(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
.LBB14_27:                              # %updateComplexity.exit19
	cmpl	%ebp, %ebx
	je	.LBB14_57
.LBB14_28:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 12(%rcx)
	je	.LBB14_17
# BB#29:
	cmpl	$0, 4(%rcx)
	je	.LBB14_17
# BB#30:
	movl	$0, 12(%rcx)
	jmp	.LBB14_57
.LBB14_33:
	movl	(%r15), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	setne	%al
.LBB14_34:
	movl	15404(%r15), %ecx
	cmpl	15352(%r15), %ecx
	jne	.LBB14_40
# BB#35:
	imull	1344(%r14), %esi
	cvtsi2sdl	%esi, %xmm0
	jmp	.LBB14_36
.LBB14_40:
	testb	%al, %al
	je	.LBB14_44
# BB#41:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 12(%rax)
	sete	%cl
	movl	4(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	testl	%eax, %eax
	je	.LBB14_43
# BB#42:
	xorl	%eax, %eax
	andb	%dl, %cl
	je	.LBB14_37
.LBB14_43:
	cvtsi2sdl	1364(%r14), %xmm1
	cvtsi2sdl	1388(%r14), %xmm0
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	mulsd	%xmm1, %xmm0
.LBB14_36:                              # %updateComplexity.exit23
	addsd	.LCPI14_0(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
.LBB14_37:                              # %updateComplexity.exit23
	cmpl	$2, %ebp
	jne	.LBB14_46
# BB#38:
	movl	(%r15), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	je	.LBB14_46
# BB#39:
	movq	generic_RC(%rip), %rcx
	decl	140(%rcx)
	testl	%ebp, %ebp
	jne	.LBB14_52
	jmp	.LBB14_47
.LBB14_4:
	movl	(%rbx), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	setne	%al
.LBB14_5:
	movl	15404(%rbx), %ecx
	cmpl	15352(%rbx), %ecx
	je	.LBB14_11
# BB#6:
	testb	%al, %al
	je	.LBB14_10
# BB#7:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 12(%rax)
	sete	%cl
	movl	4(%rax), %eax
	cmpl	$1, %eax
	sete	%dl
	testl	%eax, %eax
	je	.LBB14_9
# BB#8:
	xorl	%eax, %eax
	andb	%dl, %cl
	je	.LBB14_13
.LBB14_9:
	cvtsi2sdl	1364(%r14), %xmm1
	cvtsi2sdl	1388(%r14), %xmm0
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%esi, %xmm0
	mulsd	%xmm1, %xmm0
	jmp	.LBB14_12
.LBB14_44:
	xorl	%eax, %eax
	cmpl	$1, %ebp
	jne	.LBB14_37
# BB#45:                                # %updateComplexity.exit23.thread
	imull	1344(%r14), %esi
	cvtsi2sdl	%esi, %xmm0
	addsd	.LCPI14_0(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
.LBB14_46:
	testl	%ebp, %ebp
	je	.LBB14_47
.LBB14_52:
	cmpl	$1, %ebp
	jne	.LBB14_57
# BB#53:
	movl	%eax, 1532(%r14)
	decl	1548(%r14)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI14_1(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 1576(%r14)
	incl	1360(%r14)
	movq	generic_RC(%rip), %rax
	incl	32(%rax)
	cmpl	$0, 2968(%rbx)
	je	.LBB14_54
# BB#55:
	movl	112(%rax), %ecx
	decl	%ecx
	movq	gop_structure(%rip), %rdx
	movslq	14364(%r15), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	subl	-8(%rdx,%rsi,8), %ecx
	movslq	%ecx, %rcx
	jmp	.LBB14_56
.LBB14_47:
	movl	(%r15), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	je	.LBB14_57
# BB#48:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 12(%rcx)
	je	.LBB14_50
# BB#49:
	cmpl	$0, 4(%rcx)
	je	.LBB14_50
# BB#51:
	movl	$0, 12(%rcx)
	jmp	.LBB14_57
.LBB14_10:
	xorl	%eax, %eax
	cmpl	$1, %ebp
	jne	.LBB14_13
.LBB14_11:
	imull	1344(%r14), %esi
	cvtsi2sdl	%esi, %xmm0
.LBB14_12:                              # %updateComplexity.exit
	addsd	.LCPI14_0(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
.LBB14_13:                              # %updateComplexity.exit
	testl	%ebp, %ebp
	je	.LBB14_14
# BB#19:
	cmpl	$1, %ebp
	jne	.LBB14_57
# BB#20:
	movl	%eax, 1532(%r14)
	decl	1548(%r14)
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI14_1(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	movsd	%xmm0, 1576(%r14)
	incl	1360(%r14)
	movq	generic_RC(%rip), %rax
	incl	32(%rax)
	jmp	.LBB14_57
.LBB14_14:
	movl	(%rbx), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	je	.LBB14_57
# BB#15:
	movq	generic_RC(%rip), %rcx
	cmpl	$0, 12(%rcx)
	je	.LBB14_17
# BB#16:
	cmpl	$0, 4(%rcx)
	je	.LBB14_17
# BB#18:
	movl	$0, 12(%rcx)
	jmp	.LBB14_57
.LBB14_17:
	movl	%eax, 1528(%r14)
	decl	1544(%r14)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 1568(%r14)
	movl	16(%rcx), %eax
	movl	%eax, 1332(%r14)
	incl	36(%rcx)
	incl	48(%rcx)
	jmp	.LBB14_57
.LBB14_54:
	xorl	%ecx, %ecx
.LBB14_56:
	decl	116(%rax,%rcx,4)
	jmp	.LBB14_57
.LBB14_50:
	movl	%eax, 1528(%r14)
	decl	1544(%r14)
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 1568(%r14)
	movl	16(%rcx), %eax
	movl	%eax, 1332(%r14)
	incl	36(%rcx)
	incl	48(%rcx)
	decl	136(%rcx)
.LBB14_57:                              # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	rc_update_pict_frame, .Lfunc_end14-rc_update_pict_frame
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI15_0:
	.quad	4602678819172646912     # double 0.5
.LCPI15_1:
	.quad	4626322717216342016     # double 20
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	updateRCModel
	.p2align	4, 0x90
	.type	updateRCModel,@function
updateRCModel:                          # @updateRCModel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 48
.Lcfi98:
	.cfi_offset %rbx, -40
.Lcfi99:
	.cfi_offset %r12, -32
.Lcfi100:
	.cfi_offset %r14, -24
.Lcfi101:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	img(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB15_2
# BB#1:
	movq	input(%rip), %rcx
	cmpl	$1, 5136(%rcx)
	jne	.LBB15_42
.LBB15_2:
	movl	(%rax), %ecx
	cmpl	start_frame_no_in_this_IGOP(%rip), %ecx
	jne	.LBB15_3
.LBB15_42:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB15_3:
	movl	15404(%rax), %esi
	cmpl	15352(%rax), %esi
	jne	.LBB15_5
# BB#4:
	callq	ComputeFrameMAD
	leaq	1400(%rbx), %r12
	movsd	%xmm0, 1400(%rbx)
	movq	generic_RC(%rip), %rcx
	movl	36(%rcx), %r15d
	jmp	.LBB15_17
.LBB15_5:
	movq	generic_RC(%rip), %rcx
	movq	56(%rcx), %rax
	sarq	$8, %rax
	cqto
	idivq	%rsi
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm0, 1400(%rbx)
	movq	$0, 56(%rcx)
	movl	1368(%rbx), %edx
	movl	1388(%rbx), %r15d
	movl	%r15d, %eax
	subl	%edx, %eax
	movl	%eax, 1392(%rbx)
	testl	%eax, %eax
	jle	.LBB15_9
# BB#6:
	leal	-1(%rax), %esi
	imull	1372(%rbx), %esi
	addl	24(%rcx), %esi
	cvtsi2sdl	%esi, %xmm1
	cvtsi2sdl	%eax, %xmm2
	divsd	%xmm2, %xmm1
	addsd	.LCPI15_0(%rip), %xmm1
	cvttsd2si	%xmm1, %esi
	movl	%esi, 1372(%rbx)
	movl	1380(%rbx), %edi
	testl	%edi, %edi
	je	.LBB15_8
# BB#7:
	imull	%eax, %esi
	imull	%edx, %edi
	addl	%esi, %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%r15d, %xmm2
	divsd	%xmm2, %xmm1
	addsd	.LCPI15_0(%rip), %xmm1
	cvttsd2si	%xmm1, %esi
.LBB15_8:
	movl	%esi, 1376(%rbx)
.LBB15_9:
	movq	input(%rip), %rsi
	cmpl	$2, 4704(%rsi)
	je	.LBB15_11
# BB#10:
	cmpl	$0, 4708(%rsi)
	je	.LBB15_13
.LBB15_11:
	cmpl	$1, 4(%rcx)
	jne	.LBB15_13
# BB#12:
	leaq	1488(%rbx), %rsi
	jmp	.LBB15_14
.LBB15_13:
	leaq	1480(%rbx), %rsi
.LBB15_14:
	leaq	1400(%rbx), %r12
	leal	-1(%r15), %edi
	movq	(%rsi), %rsi
	subl	%edx, %edi
	movslq	%edi, %rdi
	movsd	%xmm0, (%rsi,%rdi,8)
	testl	%edx, %edx
	movl	36(%rcx), %edx
	jne	.LBB15_16
# BB#15:
	decl	%edx
.LBB15_16:
	imull	%edx, %r15d
	addl	%eax, %r15d
.LBB15_17:
	movl	16(%rcx), %eax
	movl	%eax, 1348(%rbx)
	movq	1104(%rbx), %rax
	movq	%rax, 1112(%rbx)
	movq	%rax, 776(%rbx)
	movq	1272(%rbx), %rax
	movq	%rax, 1280(%rbx)
	movq	%rax, 944(%rbx)
	movq	1096(%rbx), %rax
	movq	%rax, 1104(%rbx)
	movq	%rax, 768(%rbx)
	movq	1264(%rbx), %rax
	movq	%rax, 1272(%rbx)
	movq	%rax, 936(%rbx)
	movq	1088(%rbx), %rax
	movq	%rax, 1096(%rbx)
	movq	%rax, 760(%rbx)
	movq	1256(%rbx), %rax
	movq	%rax, 1264(%rbx)
	movq	%rax, 928(%rbx)
	movq	1080(%rbx), %rax
	movq	%rax, 1088(%rbx)
	movq	%rax, 752(%rbx)
	movq	1248(%rbx), %rax
	movq	%rax, 1256(%rbx)
	movq	%rax, 920(%rbx)
	movq	1072(%rbx), %rax
	movq	%rax, 1080(%rbx)
	movq	%rax, 744(%rbx)
	movq	1240(%rbx), %rax
	movq	%rax, 1248(%rbx)
	movq	%rax, 912(%rbx)
	movq	1064(%rbx), %rax
	movq	%rax, 1072(%rbx)
	movq	%rax, 736(%rbx)
	movq	1232(%rbx), %rax
	movq	%rax, 1240(%rbx)
	movq	%rax, 904(%rbx)
	movq	1056(%rbx), %rax
	movq	%rax, 1064(%rbx)
	movq	%rax, 728(%rbx)
	movq	1224(%rbx), %rax
	movq	%rax, 1232(%rbx)
	movq	%rax, 896(%rbx)
	movq	1048(%rbx), %rax
	movq	%rax, 1056(%rbx)
	movq	%rax, 720(%rbx)
	movq	1216(%rbx), %rax
	movq	%rax, 1224(%rbx)
	movq	%rax, 888(%rbx)
	movq	1040(%rbx), %rax
	movq	%rax, 1048(%rbx)
	movq	%rax, 712(%rbx)
	movq	1208(%rbx), %rax
	movq	%rax, 1216(%rbx)
	movq	%rax, 880(%rbx)
	movq	1032(%rbx), %rax
	movq	%rax, 1040(%rbx)
	movq	%rax, 704(%rbx)
	movq	1200(%rbx), %rax
	movq	%rax, 1208(%rbx)
	movq	%rax, 872(%rbx)
	movq	1024(%rbx), %rax
	movq	%rax, 1032(%rbx)
	movq	%rax, 696(%rbx)
	movq	1192(%rbx), %rax
	movq	%rax, 1200(%rbx)
	movq	%rax, 864(%rbx)
	movq	1016(%rbx), %rax
	movq	%rax, 1024(%rbx)
	movq	%rax, 688(%rbx)
	movq	1184(%rbx), %rax
	movq	%rax, 1192(%rbx)
	movq	%rax, 856(%rbx)
	movq	1008(%rbx), %rax
	movq	%rax, 1016(%rbx)
	movq	%rax, 680(%rbx)
	movq	1176(%rbx), %rax
	movq	%rax, 1184(%rbx)
	movq	%rax, 848(%rbx)
	movq	1000(%rbx), %rax
	movq	%rax, 1008(%rbx)
	movq	%rax, 672(%rbx)
	movq	1168(%rbx), %rax
	movq	%rax, 1176(%rbx)
	movq	%rax, 840(%rbx)
	movq	992(%rbx), %rax
	movq	%rax, 1000(%rbx)
	movq	%rax, 664(%rbx)
	movq	1160(%rbx), %rax
	movq	%rax, 1168(%rbx)
	movq	%rax, 832(%rbx)
	movq	984(%rbx), %rax
	movq	%rax, 992(%rbx)
	movq	%rax, 656(%rbx)
	movq	1152(%rbx), %rax
	movq	%rax, 1160(%rbx)
	movq	%rax, 824(%rbx)
	movq	976(%rbx), %rax
	movq	%rax, 984(%rbx)
	movq	%rax, 648(%rbx)
	movq	1144(%rbx), %rax
	movq	%rax, 1152(%rbx)
	movq	%rax, 816(%rbx)
	movq	968(%rbx), %rax
	movq	%rax, 976(%rbx)
	movq	%rax, 640(%rbx)
	movq	1136(%rbx), %rax
	movq	%rax, 1144(%rbx)
	movq	%rax, 808(%rbx)
	movq	960(%rbx), %rax
	movq	%rax, 968(%rbx)
	movq	%rax, 632(%rbx)
	movq	1128(%rbx), %rax
	movq	%rax, 1136(%rbx)
	movq	%rax, 800(%rbx)
	movl	1344(%rbx), %edi
	callq	QP2Qstep
	movsd	%xmm0, 960(%rbx)
	movq	img(%rip), %rax
	movl	15404(%rax), %ecx
	movq	generic_RC(%rip), %rdx
	leaq	28(%rdx), %rsi
	addq	$20, %rdx
	cmpl	15352(%rax), %ecx
	cmovneq	%rsi, %rdx
	cvtsi2sdl	(%rdx), %xmm1
	movsd	(%r12), %xmm2           # xmm2 = mem[0],zero
	divsd	%xmm2, %xmm1
	movsd	%xmm1, 1128(%rbx)
	movsd	%xmm0, 624(%rbx)
	movsd	%xmm1, 792(%rbx)
	movups	1312(%rbx), %xmm0
	movups	%xmm0, 1296(%rbx)
	movsd	1424(%rbx), %xmm0       # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm1
	minsd	%xmm2, %xmm1
	maxsd	%xmm0, %xmm2
	divsd	%xmm2, %xmm1
	mulsd	.LCPI15_1(%rip), %xmm1
	cvttsd2si	%xmm1, %eax
	testl	%eax, %eax
	movl	$1, %ecx
	cmovgl	%eax, %ecx
	cmpl	%r15d, %ecx
	cmovgl	%r15d, %ecx
	movl	1340(%rbx), %eax
	incl	%eax
	cmpl	%eax, %ecx
	cmovlel	%ecx, %eax
	cmpl	$21, %eax
	movl	$20, %esi
	cmovll	%eax, %esi
	movl	%esi, 1340(%rbx)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, updateRCModel.m_rgRejected+64(%rip)
	movapd	%xmm0, updateRCModel.m_rgRejected+48(%rip)
	movapd	%xmm0, updateRCModel.m_rgRejected+32(%rip)
	movapd	%xmm0, updateRCModel.m_rgRejected+16(%rip)
	movapd	%xmm0, updateRCModel.m_rgRejected(%rip)
	movl	$updateRCModel.m_rgRejected, %edx
	movq	%rbx, %rdi
	callq	RCModelEstimator
	movl	1340(%rbx), %r14d
	testl	%r14d, %r14d
	jle	.LBB15_18
# BB#19:                                # %.lr.ph159
	movsd	1296(%rbx), %xmm0       # xmm0 = mem[0],zero
	movsd	1304(%rbx), %xmm2       # xmm2 = mem[0],zero
	xorpd	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB15_20:                              # =>This Inner Loop Header: Depth=1
	movsd	624(%rbx,%rax,8), %xmm3 # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm4
	divsd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm3
	movapd	%xmm2, %xmm5
	divsd	%xmm3, %xmm5
	addsd	%xmm4, %xmm5
	subsd	792(%rbx,%rax,8), %xmm5
	movsd	%xmm5, updateRCModel.error(,%rax,8)
	mulsd	%xmm5, %xmm5
	addsd	%xmm5, %xmm1
	incq	%rax
	cmpq	%rax, %r14
	jne	.LBB15_20
# BB#21:                                # %._crit_edge160
	xorpd	%xmm0, %xmm0
	cmpl	$2, %r14d
	jne	.LBB15_22
	jmp	.LBB15_24
.LBB15_18:
	xorpd	%xmm1, %xmm1
.LBB15_22:                              # %._crit_edge160.thread
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	divsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB15_24
# BB#23:                                # %call.sqrt
	movapd	%xmm1, %xmm0
	callq	sqrt
.LBB15_24:
	testl	%r14d, %r14d
	jle	.LBB15_37
# BB#25:                                # %.lr.ph.preheader
	testb	$1, %r14b
	jne	.LBB15_27
# BB#26:
	xorl	%eax, %eax
	cmpl	$1, %r14d
	jne	.LBB15_31
	jmp	.LBB15_37
.LBB15_27:                              # %.lr.ph.prol
	movsd	updateRCModel.error(%rip), %xmm1 # xmm1 = mem[0],zero
	andpd	.LCPI15_2(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.LBB15_29
# BB#28:
	movl	$1, updateRCModel.m_rgRejected(%rip)
.LBB15_29:                              # %.lr.ph.prol.loopexit
	movl	$1, %eax
	cmpl	$1, %r14d
	je	.LBB15_37
.LBB15_31:                              # %.lr.ph.preheader.new
	movapd	.LCPI15_2(%rip), %xmm1  # xmm1 = [nan,nan]
	.p2align	4, 0x90
.LBB15_32:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	updateRCModel.error(,%rax,8), %xmm2 # xmm2 = mem[0],zero
	andpd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jbe	.LBB15_34
# BB#33:                                #   in Loop: Header=BB15_32 Depth=1
	movl	$1, updateRCModel.m_rgRejected(,%rax,4)
.LBB15_34:                              # %.lr.ph.1177
                                        #   in Loop: Header=BB15_32 Depth=1
	movsd	updateRCModel.error+8(,%rax,8), %xmm2 # xmm2 = mem[0],zero
	andpd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jbe	.LBB15_36
# BB#35:                                #   in Loop: Header=BB15_32 Depth=1
	movl	$1, updateRCModel.m_rgRejected+4(,%rax,4)
.LBB15_36:                              #   in Loop: Header=BB15_32 Depth=1
	addq	$2, %rax
	cmpq	%rax, %r14
	jne	.LBB15_32
.LBB15_37:                              # %._crit_edge
	movl	$0, updateRCModel.m_rgRejected(%rip)
	movl	$updateRCModel.m_rgRejected, %edx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	RCModelEstimator
	cmpl	$2, %r15d
	jl	.LBB15_38
# BB#43:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	updateMADModel          # TAILCALL
.LBB15_38:
	movq	img(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB15_40
# BB#39:
	movq	input(%rip), %rcx
	cmpl	$1, 5136(%rcx)
	jne	.LBB15_42
.LBB15_40:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB15_42
# BB#41:
	movq	(%r12), %rax
	movq	%rax, 120(%rbx)
	jmp	.LBB15_42
.Lfunc_end15:
	.size	updateRCModel, .Lfunc_end15-updateRCModel
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI16_0:
	.quad	4607182418800017408     # double 1
.LCPI16_2:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	RCModelEstimator
	.p2align	4, 0x90
	.type	RCModelEstimator,@function
RCModelEstimator:                       # @RCModelEstimator
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi102:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 48
.Lcfi107:
	.cfi_offset %rbx, -48
.Lcfi108:
	.cfi_offset %r12, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	testl	%esi, %esi
	jle	.LBB16_12
# BB#1:                                 # %.lr.ph148.preheader
	movl	%esi, %r9d
	cmpl	$7, %esi
	jbe	.LBB16_2
# BB#5:                                 # %min.iters.checked
	movl	%esi, %eax
	andl	$7, %eax
	movq	%r9, %r8
	subq	%rax, %r8
	je	.LBB16_2
# BB#6:                                 # %vector.ph
	movd	%esi, %xmm0
	leaq	16(%rdx), %rcx
	pxor	%xmm2, %xmm2
	pcmpeqd	%xmm3, %xmm3
	movq	%r8, %rbp
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB16_7:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm4
	movdqu	(%rcx), %xmm5
	pcmpeqd	%xmm2, %xmm4
	pxor	%xmm3, %xmm4
	pcmpeqd	%xmm2, %xmm5
	pxor	%xmm3, %xmm5
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB16_7
# BB#8:                                 # %middle.block
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r10d
	testl	%eax, %eax
	jne	.LBB16_3
	jmp	.LBB16_9
.LBB16_2:
	xorl	%r8d, %r8d
	movl	%esi, %r10d
.LBB16_3:                               # %.lr.ph148.preheader176
	leaq	(%rdx,%r8,4), %rax
	movq	%r9, %rcx
	subq	%r8, %rcx
	.p2align	4, 0x90
.LBB16_4:                               # %.lr.ph148
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, (%rax)
	adcl	$-1, %r10d
	addq	$4, %rax
	decq	%rcx
	jne	.LBB16_4
.LBB16_9:                               # %._crit_edge149
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 1296(%rdi)
	testl	%esi, %esi
	jle	.LBB16_45
# BB#10:                                # %.lr.ph143.preheader
	leaq	-1(%r9), %rcx
	movq	%r9, %rbp
	andq	$3, %rbp
	je	.LBB16_11
# BB#13:                                # %.lr.ph143.prol.preheader
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_14:                              # %.lr.ph143.prol
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rdx,%rax,4)
	jne	.LBB16_16
# BB#15:                                #   in Loop: Header=BB16_14 Depth=1
	movq	624(%rdi,%rax,8), %xmm1 # xmm1 = mem[0],zero
.LBB16_16:                              #   in Loop: Header=BB16_14 Depth=1
	incq	%rax
	cmpq	%rax, %rbp
	jne	.LBB16_14
	jmp	.LBB16_17
.LBB16_12:                              # %._crit_edge149.thread
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 1296(%rdi)
	jmp	.LBB16_45
.LBB16_11:
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
.LBB16_17:                              # %.lr.ph143.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB16_28
# BB#18:                                # %.lr.ph143.preheader.new
	movq	%r9, %r8
	subq	%rax, %r8
	leaq	648(%rdi,%rax,8), %rcx
	leaq	12(%rdx,%rax,4), %rax
	.p2align	4, 0x90
.LBB16_19:                              # %.lr.ph143
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, -12(%rax)
	jne	.LBB16_21
# BB#20:                                #   in Loop: Header=BB16_19 Depth=1
	movq	-24(%rcx), %xmm1        # xmm1 = mem[0],zero
.LBB16_21:                              # %.lr.ph143.1183
                                        #   in Loop: Header=BB16_19 Depth=1
	cmpl	$0, -8(%rax)
	jne	.LBB16_23
# BB#22:                                #   in Loop: Header=BB16_19 Depth=1
	movq	-16(%rcx), %xmm1        # xmm1 = mem[0],zero
.LBB16_23:                              # %.lr.ph143.2184
                                        #   in Loop: Header=BB16_19 Depth=1
	cmpl	$0, -4(%rax)
	jne	.LBB16_25
# BB#24:                                #   in Loop: Header=BB16_19 Depth=1
	movq	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
.LBB16_25:                              # %.lr.ph143.3185
                                        #   in Loop: Header=BB16_19 Depth=1
	cmpl	$0, (%rax)
	jne	.LBB16_27
# BB#26:                                #   in Loop: Header=BB16_19 Depth=1
	movq	(%rcx), %xmm1           # xmm1 = mem[0],zero
.LBB16_27:                              #   in Loop: Header=BB16_19 Depth=1
	addq	$32, %rcx
	addq	$16, %rax
	addq	$-4, %r8
	jne	.LBB16_19
.LBB16_28:                              # %.preheader122
	testl	%esi, %esi
	jle	.LBB16_45
# BB#29:                                # %.lr.ph137
	leaq	1296(%rdi), %r8
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r10d, %xmm0
	leaq	624(%rdi), %rax
	xorl	%r14d, %r14d
	pxor	%xmm2, %xmm2
	movl	$1, %r11d
	movq	%r9, %r12
	movq	%rdx, %rcx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB16_30:                              # %._crit_edge163
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax), %xmm3           # xmm3 = mem[0],zero
	movl	(%rcx), %ebp
	cmpl	$0, %ebp
	movl	%r15d, %ebx
	cmovel	%r11d, %ebx
	ucomisd	%xmm3, %xmm1
	cmovnel	%ebx, %r15d
	cmovpl	%ebx, %r15d
	cmpl	$0, %ebp
	jne	.LBB16_32
# BB#31:                                #   in Loop: Header=BB16_30 Depth=1
	mulsd	168(%rax), %xmm3
	divsd	%xmm0, %xmm3
	addsd	%xmm3, %xmm2
	movsd	%xmm2, (%r8)
	movd	%xmm2, %r14
.LBB16_32:                              #   in Loop: Header=BB16_30 Depth=1
	addq	$4, %rcx
	addq	$8, %rax
	decq	%r12
	jne	.LBB16_30
# BB#33:                                # %._crit_edge138
	movd	%r14, %xmm0
	testl	%r10d, %r10d
	jle	.LBB16_45
# BB#34:                                # %._crit_edge138
	testl	%r15d, %r15d
	je	.LBB16_45
# BB#35:                                # %.preheader
	testl	%esi, %esi
	jle	.LBB16_36
# BB#37:                                # %.lr.ph.preheader
	leaq	624(%rdi), %rcx
	pxor	%xmm0, %xmm0
	movsd	.LCPI16_0(%rip), %xmm3  # xmm3 = mem[0],zero
	pxor	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB16_38:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rdx)
	jne	.LBB16_40
# BB#39:                                #   in Loop: Header=BB16_38 Depth=1
	movsd	(%rcx), %xmm4           # xmm4 = mem[0],zero
	movsd	168(%rcx), %xmm5        # xmm5 = mem[0],zero
	movapd	%xmm3, %xmm6
	divsd	%xmm4, %xmm6
	addsd	%xmm6, %xmm2
	movapd	%xmm4, %xmm6
	mulsd	%xmm6, %xmm6
	movapd	%xmm3, %xmm7
	divsd	%xmm6, %xmm7
	unpcklpd	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0]
	addpd	%xmm7, %xmm0
	mulsd	%xmm5, %xmm4
	unpcklpd	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0]
	addpd	%xmm4, %xmm1
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
.LBB16_40:                              #   in Loop: Header=BB16_38 Depth=1
	addq	$4, %rdx
	addq	$8, %rcx
	decq	%r9
	jne	.LBB16_38
	jmp	.LBB16_41
.LBB16_36:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
.LBB16_41:                              # %._crit_edge
	movapd	%xmm0, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	movaps	%xmm4, %xmm3
	mulsd	%xmm0, %xmm3
	movaps	%xmm2, %xmm5
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movapd	.LCPI16_1(%rip), %xmm5  # xmm5 = [nan,nan]
	andpd	%xmm3, %xmm5
	ucomisd	.LCPI16_2(%rip), %xmm5
	jbe	.LBB16_43
# BB#42:
	mulpd	%xmm1, %xmm0
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	mulpd	%xmm1, %xmm2
	subpd	%xmm2, %xmm0
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	divpd	%xmm3, %xmm0
	movlpd	%xmm0, (%r8)
	jmp	.LBB16_44
.LBB16_43:
	divsd	%xmm4, %xmm1
	movsd	%xmm1, (%r8)
	movq	%xmm1, %xmm0            # xmm0 = xmm1[0],zero
.LBB16_44:
	movhpd	%xmm0, 1304(%rdi)
.LBB16_45:                              # %._crit_edge138.thread
	movq	img(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB16_47
# BB#46:
	movq	input(%rip), %rcx
	cmpl	$1, 5136(%rcx)
	jne	.LBB16_49
.LBB16_47:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB16_49
# BB#48:
	movdqu	%xmm0, 1312(%rdi)
.LBB16_49:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	RCModelEstimator, .Lfunc_end16-RCModelEstimator
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI17_0:
	.quad	4626322717216342016     # double 20
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	updateMADModel
	.p2align	4, 0x90
	.type	updateMADModel,@function
updateMADModel:                         # @updateMADModel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 32
.Lcfi115:
	.cfi_offset %rbx, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	generic_RC(%rip), %rax
	movl	36(%rax), %eax
	testl	%eax, %eax
	jle	.LBB17_39
# BB#1:
	movq	img(%rip), %rcx
	movl	15404(%rcx), %edx
	cmpl	15352(%rcx), %edx
	je	.LBB17_3
# BB#2:
	imull	1388(%rbx), %eax
	addl	1392(%rbx), %eax
.LBB17_3:
	movq	264(%rbx), %rcx
	movq	%rcx, 272(%rbx)
	movq	%rcx, 440(%rbx)
	movq	600(%rbx), %rcx
	movq	%rcx, 608(%rbx)
	movq	256(%rbx), %rcx
	movq	%rcx, 264(%rbx)
	movq	%rcx, 432(%rbx)
	movq	248(%rbx), %rcx
	movq	%rcx, 256(%rbx)
	movq	%rcx, 424(%rbx)
	movups	584(%rbx), %xmm0
	movups	%xmm0, 592(%rbx)
	movq	240(%rbx), %rcx
	movq	%rcx, 248(%rbx)
	movq	%rcx, 416(%rbx)
	movq	232(%rbx), %rcx
	movq	%rcx, 240(%rbx)
	movq	%rcx, 408(%rbx)
	movups	568(%rbx), %xmm0
	movups	%xmm0, 576(%rbx)
	movq	224(%rbx), %rcx
	movq	%rcx, 232(%rbx)
	movq	%rcx, 400(%rbx)
	movq	560(%rbx), %rcx
	movq	%rcx, 568(%rbx)
	movq	216(%rbx), %rcx
	movq	%rcx, 224(%rbx)
	movq	%rcx, 392(%rbx)
	movq	208(%rbx), %rcx
	movq	%rcx, 216(%rbx)
	movq	%rcx, 384(%rbx)
	movups	544(%rbx), %xmm0
	movups	%xmm0, 552(%rbx)
	movq	200(%rbx), %rcx
	movq	%rcx, 208(%rbx)
	movq	%rcx, 376(%rbx)
	movq	192(%rbx), %rcx
	movq	%rcx, 200(%rbx)
	movq	%rcx, 368(%rbx)
	movups	528(%rbx), %xmm0
	movups	%xmm0, 536(%rbx)
	movq	184(%rbx), %rcx
	movq	%rcx, 192(%rbx)
	movq	%rcx, 360(%rbx)
	movq	176(%rbx), %rcx
	movq	%rcx, 184(%rbx)
	movq	%rcx, 352(%rbx)
	movups	512(%rbx), %xmm0
	movups	%xmm0, 520(%rbx)
	movq	168(%rbx), %rcx
	movq	%rcx, 176(%rbx)
	movq	%rcx, 344(%rbx)
	movq	160(%rbx), %rcx
	movq	%rcx, 168(%rbx)
	movq	%rcx, 336(%rbx)
	movups	496(%rbx), %xmm0
	movups	%xmm0, 504(%rbx)
	movq	152(%rbx), %rcx
	movq	%rcx, 160(%rbx)
	movq	%rcx, 328(%rbx)
	movq	144(%rbx), %rcx
	movq	%rcx, 152(%rbx)
	movq	%rcx, 320(%rbx)
	movups	480(%rbx), %xmm0
	movups	%xmm0, 488(%rbx)
	movq	136(%rbx), %rcx
	movq	%rcx, 144(%rbx)
	movq	%rcx, 312(%rbx)
	movq	472(%rbx), %rcx
	movq	%rcx, 480(%rbx)
	movq	128(%rbx), %rcx
	movq	%rcx, 136(%rbx)
	movq	%rcx, 304(%rbx)
	movq	120(%rbx), %rcx
	movq	%rcx, 128(%rbx)
	movq	%rcx, 296(%rbx)
	movups	456(%rbx), %xmm0
	movups	%xmm0, 464(%rbx)
	movq	1400(%rbx), %rcx
	movq	%rcx, 120(%rbx)
	movq	%rcx, 288(%rbx)
	movq	img(%rip), %rdx
	movl	15404(%rdx), %esi
	cmpl	15352(%rdx), %esi
	movd	%rcx, %xmm0
	jne	.LBB17_5
# BB#4:
	leaq	296(%rbx), %rdi
	jmp	.LBB17_11
.LBB17_39:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB17_5:
	movq	input(%rip), %rsi
	cmpl	$2, 4704(%rsi)
	je	.LBB17_7
# BB#6:
	cmpl	$0, 4708(%rsi)
	je	.LBB17_9
.LBB17_7:
	movq	generic_RC(%rip), %rsi
	cmpl	$1, 4(%rsi)
	jne	.LBB17_9
# BB#8:
	leaq	1496(%rbx), %rsi
	jmp	.LBB17_10
.LBB17_9:
	leaq	1472(%rbx), %rsi
.LBB17_10:                              # %.sink.split
	movl	1388(%rbx), %edi
	decl	%edi
	subl	1368(%rbx), %edi
	movslq	%edi, %rdi
	shlq	$3, %rdi
	addq	(%rsi), %rdi
.LBB17_11:
	movq	(%rdi), %rsi
	movq	%rsi, 456(%rbx)
	movups	104(%rbx), %xmm1
	movups	%xmm1, 88(%rbx)
	movsd	1424(%rbx), %xmm1       # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	minsd	%xmm0, %xmm2
	mulsd	.LCPI17_0(%rip), %xmm2
	maxsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %esi
	decl	%eax
	testl	%esi, %esi
	movl	$1, %edi
	cmovgl	%esi, %edi
	cmpl	%eax, %edi
	cmovgl	%eax, %edi
	movl	1336(%rbx), %eax
	incl	%eax
	cmpl	$21, %eax
	movl	$20, %ebp
	cmovll	%eax, %ebp
	cmpl	%ebp, %edi
	cmovlel	%edi, %ebp
	movl	%ebp, 1336(%rbx)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, updateMADModel.PictureRejected+64(%rip)
	movapd	%xmm0, updateMADModel.PictureRejected+48(%rip)
	movapd	%xmm0, updateMADModel.PictureRejected+32(%rip)
	movapd	%xmm0, updateMADModel.PictureRejected+16(%rip)
	movapd	%xmm0, updateMADModel.PictureRejected(%rip)
	cmpl	$0, 20(%rdx)
	je	.LBB17_13
# BB#12:
	movq	input(%rip), %rax
	cmpl	$1, 5136(%rax)
	jne	.LBB17_15
.LBB17_13:
	movl	(%rdx), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB17_15
# BB#14:
	movq	%rcx, 1424(%rbx)
.LBB17_15:
	movl	$updateMADModel.PictureRejected, %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	MADModelEstimator
	testl	%ebp, %ebp
	jle	.LBB17_16
# BB#17:                                # %.lr.ph102
	movsd	88(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	96(%rbx), %xmm2         # xmm2 = mem[0],zero
	movslq	%ebp, %rax
	testb	$1, %al
	jne	.LBB17_19
# BB#18:
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	cmpl	$1, %ebp
	jne	.LBB17_21
	jmp	.LBB17_22
.LBB17_16:
	xorpd	%xmm0, %xmm0
	jmp	.LBB17_23
.LBB17_19:
	movsd	456(%rbx), %xmm3        # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	subsd	288(%rbx), %xmm3
	movsd	%xmm3, updateMADModel.error(%rip)
	mulsd	%xmm3, %xmm3
	xorpd	%xmm0, %xmm0
	addsd	%xmm3, %xmm0
	movl	$1, %ecx
	cmpl	$1, %ebp
	je	.LBB17_22
	.p2align	4, 0x90
.LBB17_21:                              # =>This Inner Loop Header: Depth=1
	movsd	456(%rbx,%rcx,8), %xmm3 # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	subsd	288(%rbx,%rcx,8), %xmm3
	movsd	%xmm3, updateMADModel.error(,%rcx,8)
	mulsd	%xmm3, %xmm3
	addsd	%xmm0, %xmm3
	movsd	464(%rbx,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	subsd	296(%rbx,%rcx,8), %xmm0
	movsd	%xmm0, updateMADModel.error+8(,%rcx,8)
	mulsd	%xmm0, %xmm0
	addsd	%xmm3, %xmm0
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jl	.LBB17_21
.LBB17_22:                              # %._crit_edge103
	xorpd	%xmm1, %xmm1
	cmpl	$2, %ebp
	je	.LBB17_25
.LBB17_23:                              # %._crit_edge103.thread
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	divsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB17_25
# BB#24:                                # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB17_25:
	testl	%ebp, %ebp
	jle	.LBB17_38
# BB#26:                                # %.lr.ph.preheader
	movslq	%ebp, %rax
	testb	$1, %al
	jne	.LBB17_28
# BB#27:
	xorl	%ecx, %ecx
	cmpl	$1, %ebp
	jne	.LBB17_32
	jmp	.LBB17_38
.LBB17_28:                              # %.lr.ph.prol
	movsd	updateMADModel.error(%rip), %xmm0 # xmm0 = mem[0],zero
	andpd	.LCPI17_1(%rip), %xmm0
	ucomisd	%xmm1, %xmm0
	jbe	.LBB17_30
# BB#29:
	movl	$1, updateMADModel.PictureRejected(%rip)
.LBB17_30:                              # %.lr.ph.prol.loopexit
	movl	$1, %ecx
	cmpl	$1, %ebp
	je	.LBB17_38
.LBB17_32:                              # %.lr.ph.preheader.new
	movapd	.LCPI17_1(%rip), %xmm0  # xmm0 = [nan,nan]
	.p2align	4, 0x90
.LBB17_33:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	updateMADModel.error(,%rcx,8), %xmm2 # xmm2 = mem[0],zero
	andpd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	jbe	.LBB17_35
# BB#34:                                #   in Loop: Header=BB17_33 Depth=1
	movl	$1, updateMADModel.PictureRejected(,%rcx,4)
.LBB17_35:                              # %.lr.ph.1124
                                        #   in Loop: Header=BB17_33 Depth=1
	movsd	updateMADModel.error+8(,%rcx,8), %xmm2 # xmm2 = mem[0],zero
	andpd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	jbe	.LBB17_37
# BB#36:                                #   in Loop: Header=BB17_33 Depth=1
	movl	$1, updateMADModel.PictureRejected+4(,%rcx,4)
.LBB17_37:                              #   in Loop: Header=BB17_33 Depth=1
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jl	.LBB17_33
.LBB17_38:                              # %._crit_edge
	movl	$0, updateMADModel.PictureRejected(%rip)
	movl	$updateMADModel.PictureRejected, %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	MADModelEstimator       # TAILCALL
.Lfunc_end17:
	.size	updateMADModel, .Lfunc_end17-updateMADModel
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI18_0:
	.quad	4607182418800017408     # double 1
.LCPI18_2:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_1:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	MADModelEstimator
	.p2align	4, 0x90
	.type	MADModelEstimator,@function
MADModelEstimator:                      # @MADModelEstimator
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi119:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi120:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 48
.Lcfi122:
	.cfi_offset %rbx, -48
.Lcfi123:
	.cfi_offset %r12, -40
.Lcfi124:
	.cfi_offset %r14, -32
.Lcfi125:
	.cfi_offset %r15, -24
.Lcfi126:
	.cfi_offset %rbp, -16
	testl	%esi, %esi
	jle	.LBB18_12
# BB#1:                                 # %.lr.ph146.preheader
	movl	%esi, %r9d
	cmpl	$7, %esi
	jbe	.LBB18_2
# BB#5:                                 # %min.iters.checked
	movl	%esi, %eax
	andl	$7, %eax
	movq	%r9, %r8
	subq	%rax, %r8
	je	.LBB18_2
# BB#6:                                 # %vector.ph
	movd	%esi, %xmm0
	leaq	16(%rdx), %rcx
	pxor	%xmm2, %xmm2
	pcmpeqd	%xmm3, %xmm3
	movq	%r8, %rbp
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB18_7:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm4
	movdqu	(%rcx), %xmm5
	pcmpeqd	%xmm2, %xmm4
	pxor	%xmm3, %xmm4
	pcmpeqd	%xmm2, %xmm5
	pxor	%xmm3, %xmm5
	paddd	%xmm4, %xmm0
	paddd	%xmm5, %xmm1
	addq	$32, %rcx
	addq	$-8, %rbp
	jne	.LBB18_7
# BB#8:                                 # %middle.block
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %r10d
	testl	%eax, %eax
	jne	.LBB18_3
	jmp	.LBB18_9
.LBB18_2:
	xorl	%r8d, %r8d
	movl	%esi, %r10d
.LBB18_3:                               # %.lr.ph146.preheader174
	leaq	(%rdx,%r8,4), %rax
	movq	%r9, %rcx
	subq	%r8, %rcx
	.p2align	4, 0x90
.LBB18_4:                               # %.lr.ph146
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, (%rax)
	adcl	$-1, %r10d
	addq	$4, %rax
	decq	%rcx
	jne	.LBB18_4
.LBB18_9:                               # %._crit_edge147
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 88(%rdi)
	testl	%esi, %esi
	jle	.LBB18_44
# BB#10:                                # %.lr.ph141.preheader
	leaq	-1(%r9), %rcx
	movq	%r9, %rbp
	andq	$3, %rbp
	je	.LBB18_11
# BB#13:                                # %.lr.ph141.prol.preheader
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB18_14:                              # %.lr.ph141.prol
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rdx,%rax,4)
	jne	.LBB18_16
# BB#15:                                #   in Loop: Header=BB18_14 Depth=1
	movq	288(%rdi,%rax,8), %xmm1 # xmm1 = mem[0],zero
.LBB18_16:                              #   in Loop: Header=BB18_14 Depth=1
	incq	%rax
	cmpq	%rax, %rbp
	jne	.LBB18_14
	jmp	.LBB18_17
.LBB18_12:                              # %._crit_edge147.thread
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 88(%rdi)
	jmp	.LBB18_44
.LBB18_11:
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
.LBB18_17:                              # %.lr.ph141.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB18_28
# BB#18:                                # %.lr.ph141.preheader.new
	movq	%r9, %r8
	subq	%rax, %r8
	leaq	312(%rdi,%rax,8), %rcx
	leaq	12(%rdx,%rax,4), %rax
	.p2align	4, 0x90
.LBB18_19:                              # %.lr.ph141
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, -12(%rax)
	jne	.LBB18_21
# BB#20:                                #   in Loop: Header=BB18_19 Depth=1
	movq	-24(%rcx), %xmm1        # xmm1 = mem[0],zero
.LBB18_21:                              # %.lr.ph141.1181
                                        #   in Loop: Header=BB18_19 Depth=1
	cmpl	$0, -8(%rax)
	jne	.LBB18_23
# BB#22:                                #   in Loop: Header=BB18_19 Depth=1
	movq	-16(%rcx), %xmm1        # xmm1 = mem[0],zero
.LBB18_23:                              # %.lr.ph141.2182
                                        #   in Loop: Header=BB18_19 Depth=1
	cmpl	$0, -4(%rax)
	jne	.LBB18_25
# BB#24:                                #   in Loop: Header=BB18_19 Depth=1
	movq	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
.LBB18_25:                              # %.lr.ph141.3183
                                        #   in Loop: Header=BB18_19 Depth=1
	cmpl	$0, (%rax)
	jne	.LBB18_27
# BB#26:                                #   in Loop: Header=BB18_19 Depth=1
	movq	(%rcx), %xmm1           # xmm1 = mem[0],zero
.LBB18_27:                              #   in Loop: Header=BB18_19 Depth=1
	addq	$32, %rcx
	addq	$16, %rax
	addq	$-4, %r8
	jne	.LBB18_19
.LBB18_28:                              # %.preheader120
	testl	%esi, %esi
	jle	.LBB18_44
# BB#29:                                # %.lr.ph135
	leaq	88(%rdi), %r8
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r10d, %xmm0
	leaq	288(%rdi), %rax
	xorl	%r14d, %r14d
	pxor	%xmm2, %xmm2
	movl	$1, %r11d
	movq	%r9, %r12
	movq	%rdx, %rcx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB18_30:                              # %._crit_edge161
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax), %xmm3           # xmm3 = mem[0],zero
	movl	(%rcx), %ebp
	cmpl	$0, %ebp
	movl	%r15d, %ebx
	cmovel	%r11d, %ebx
	ucomisd	%xmm3, %xmm1
	cmovnel	%ebx, %r15d
	cmovpl	%ebx, %r15d
	cmpl	$0, %ebp
	jne	.LBB18_32
# BB#31:                                #   in Loop: Header=BB18_30 Depth=1
	movsd	168(%rax), %xmm4        # xmm4 = mem[0],zero
	mulsd	%xmm0, %xmm4
	divsd	%xmm4, %xmm3
	addsd	%xmm3, %xmm2
	movsd	%xmm2, (%r8)
	movd	%xmm2, %r14
.LBB18_32:                              #   in Loop: Header=BB18_30 Depth=1
	addq	$4, %rcx
	addq	$8, %rax
	decq	%r12
	jne	.LBB18_30
# BB#33:                                # %._crit_edge136
	movd	%r14, %xmm0
	testl	%r10d, %r10d
	jle	.LBB18_44
# BB#34:                                # %._crit_edge136
	testl	%r15d, %r15d
	je	.LBB18_44
# BB#35:                                # %.preheader
	testl	%esi, %esi
	jle	.LBB18_36
# BB#37:                                # %.lr.ph.preheader
	leaq	288(%rdi), %rcx
	pxor	%xmm0, %xmm0
	movsd	.LCPI18_0(%rip), %xmm3  # xmm3 = mem[0],zero
	pxor	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB18_38:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rdx)
	jne	.LBB18_40
# BB#39:                                #   in Loop: Header=BB18_38 Depth=1
	movsd	(%rcx), %xmm4           # xmm4 = mem[0],zero
	movsd	168(%rcx), %xmm5        # xmm5 = mem[0],zero
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	addsd	%xmm5, %xmm2
	movapd	%xmm5, %xmm6
	mulsd	%xmm6, %xmm6
	movapd	%xmm3, %xmm7
	unpcklpd	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0]
	addpd	%xmm7, %xmm0
	mulsd	%xmm4, %xmm5
	unpcklpd	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0]
	addpd	%xmm5, %xmm1
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
.LBB18_40:                              #   in Loop: Header=BB18_38 Depth=1
	addq	$4, %rdx
	addq	$8, %rcx
	decq	%r9
	jne	.LBB18_38
	jmp	.LBB18_41
.LBB18_36:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
.LBB18_41:                              # %._crit_edge
	movapd	%xmm0, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	mulsd	%xmm0, %xmm3
	movaps	%xmm2, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	movaps	%xmm4, %xmm5
	mulsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	movapd	.LCPI18_1(%rip), %xmm5  # xmm5 = [nan,nan]
	andpd	%xmm3, %xmm5
	ucomisd	.LCPI18_2(%rip), %xmm5
	jbe	.LBB18_43
# BB#42:
	mulpd	%xmm1, %xmm0
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	mulpd	%xmm1, %xmm2
	subpd	%xmm2, %xmm0
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	divpd	%xmm3, %xmm0
	movupd	%xmm0, (%r8)
	jmp	.LBB18_44
.LBB18_43:
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	divsd	%xmm4, %xmm1
	movsd	%xmm1, 88(%rdi)
	movq	$0, 96(%rdi)
	movd	%xmm1, %rax
	movd	%rax, %xmm0
.LBB18_44:                              # %._crit_edge136.thread
	movq	img(%rip), %rax
	cmpl	$0, 20(%rax)
	je	.LBB18_46
# BB#45:
	movq	input(%rip), %rcx
	cmpl	$1, 5136(%rcx)
	jne	.LBB18_48
.LBB18_46:
	movl	(%rax), %eax
	cmpl	start_frame_no_in_this_IGOP(%rip), %eax
	je	.LBB18_48
# BB#47:
	movdqu	%xmm0, 104(%rdi)
.LBB18_48:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	MADModelEstimator, .Lfunc_end18-MADModelEstimator
	.cfi_endproc

	.globl	updateQPInterlace
	.p2align	4, 0x90
	.type	updateQPInterlace,@function
updateQPInterlace:                      # @updateQPInterlace
	.cfi_startproc
# BB#0:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB19_2
# BB#1:
	leaq	1456(%rdi), %rcx
	leaq	1460(%rdi), %rdx
	cmpl	$1, 8(%rax)
	movl	1356(%rdi), %eax
	movl	%eax, 1352(%rdi)
	cmoveq	%rdx, %rcx
	movl	(%rcx), %eax
	movl	%eax, 1356(%rdi)
.LBB19_2:
	retq
.Lfunc_end19:
	.size	updateQPInterlace, .Lfunc_end19-updateQPInterlace
	.cfi_endproc

	.globl	updateQPNonPicAFF
	.p2align	4, 0x90
	.type	updateQPNonPicAFF,@function
updateQPNonPicAFF:                      # @updateQPNonPicAFF
	.cfi_startproc
# BB#0:
	movq	active_sps(%rip), %rax
	cmpl	$0, 1148(%rax)
	movl	1344(%rdi), %eax
	je	.LBB20_2
# BB#1:
	movq	generic_RC(%rip), %rcx
	addl	%eax, 44(%rcx)
	movl	1356(%rdi), %ecx
	movl	%ecx, 1352(%rdi)
	movl	%eax, 1356(%rdi)
	movl	%eax, 1328(%rdi)
	retq
.LBB20_2:
	movl	%eax, 1460(%rdi)
	retq
.Lfunc_end20:
	.size	updateQPNonPicAFF, .Lfunc_end20-updateQPNonPicAFF
	.cfi_endproc

	.globl	updateQPInterlaceBU
	.p2align	4, 0x90
	.type	updateQPInterlaceBU,@function
updateQPInterlaceBU:                    # @updateQPInterlaceBU
	.cfi_startproc
# BB#0:
	movq	generic_RC(%rip), %rax
	leaq	1456(%rdi), %rcx
	leaq	1460(%rdi), %rdx
	cmpl	$1, 8(%rax)
	cmoveq	%rdx, %rcx
	movl	(%rcx), %edx
	addl	%edx, 44(%rax)
	movl	(%rcx), %eax
	movl	%eax, 1328(%rdi)
	retq
.Lfunc_end21:
	.size	updateQPInterlaceBU, .Lfunc_end21-updateQPInterlaceBU
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI22_0:
	.quad	4616189618054758400     # double 4
	.text
	.globl	updateModelQPFrame
	.p2align	4, 0x90
	.type	updateModelQPFrame,@function
updateModelQPFrame:                     # @updateModelQPFrame
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi128:
	.cfi_def_cfa_offset 48
.Lcfi129:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movsd	1400(%rbx), %xmm4       # xmm4 = mem[0],zero
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	movsd	1304(%rbx), %xmm2       # xmm2 = mem[0],zero
	cvtsi2sdl	%esi, %xmm5
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm2
	jne	.LBB22_1
	jnp	.LBB22_5
.LBB22_1:
	movapd	%xmm4, %xmm2
	movhpd	.LCPI22_0(%rip), %xmm2  # xmm2 = xmm2[0],mem[0]
	movupd	1296(%rbx), %xmm3
	mulpd	%xmm2, %xmm3
	movapd	%xmm4, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm3, %xmm2
	movapd	%xmm1, %xmm3
	unpcklpd	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0]
	mulpd	%xmm2, %xmm3
	movapd	%xmm3, %xmm6
	movhlps	%xmm6, %xmm6            # xmm6 = xmm6[1,1]
	addsd	%xmm3, %xmm6
	ucomisd	%xmm6, %xmm0
	ja	.LBB22_5
# BB#2:
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm6, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB22_4
# BB#3:                                 # %call.sqrt
	movapd	%xmm6, %xmm0
	movapd	%xmm5, (%rsp)           # 16-byte Spill
	movapd	%xmm6, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm6         # 16-byte Reload
	movapd	(%rsp), %xmm5           # 16-byte Reload
.LBB22_4:                               # %.split
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	movsd	1400(%rbx), %xmm4       # xmm4 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jae	.LBB22_5
# BB#6:
	movsd	1304(%rbx), %xmm0       # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm6, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB22_8
# BB#7:                                 # %call.sqrt26
	movapd	%xmm6, %xmm0
	movapd	%xmm4, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm4           # 16-byte Reload
.LBB22_8:                               # %.split25
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	mulsd	1400(%rbx), %xmm1
	subsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm4
	jmp	.LBB22_9
.LBB22_5:
	mulsd	%xmm1, %xmm4
	divsd	%xmm5, %xmm4
.LBB22_9:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm4, %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	Qstep2QP
	movl	%eax, 1344(%rbx)
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end22:
	.size	updateModelQPFrame, .Lfunc_end22-updateModelQPFrame
	.cfi_endproc

	.globl	updateBottomField
	.p2align	4, 0x90
	.type	updateBottomField,@function
updateBottomField:                      # @updateBottomField
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rcx
	movl	1344(%rdi), %eax
	cmpl	$1, 4704(%rcx)
	jne	.LBB23_2
# BB#1:
	movq	generic_RC(%rip), %rcx
	addl	%eax, 44(%rcx)
	movl	1356(%rdi), %ecx
	incl	%ecx
	movl	%ecx, 1352(%rdi)
	movl	%eax, 1356(%rdi)
	addq	$1328, %rdi             # imm = 0x530
	movl	%eax, (%rdi)
	retq
.LBB23_2:
	addq	$1456, %rdi             # imm = 0x5B0
	movl	%eax, (%rdi)
	retq
.Lfunc_end23:
	.size	updateBottomField, .Lfunc_end23-updateBottomField
	.cfi_endproc

	.globl	updateFirstP
	.p2align	4, 0x90
	.type	updateFirstP,@function
updateFirstP:                           # @updateFirstP
	.cfi_startproc
# BB#0:
	movl	72(%rdi), %eax
	movl	%eax, 1344(%rdi)
	movq	generic_RC(%rip), %rcx
	movl	$0, 24(%rcx)
	movl	$0, 28(%rcx)
	movl	1368(%rdi), %edx
	decl	%edx
	movl	%edx, 1368(%rdi)
	orl	%esi, %edx
	jne	.LBB24_9
# BB#1:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 1148(%rdx)
	je	.LBB24_2
.LBB24_4:
	addl	%eax, 44(%rcx)
	movl	1356(%rdi), %ecx
	movl	%ecx, 1352(%rdi)
	movl	%eax, 1356(%rdi)
	movl	%eax, 1384(%rdi)
	movl	1376(%rdi), %ecx
	movl	%ecx, 1380(%rdi)
.LBB24_9:
	movl	%eax, 1328(%rdi)
	addl	%eax, 1364(%rdi)
	retq
.LBB24_2:
	movq	input(%rip), %rdx
	movl	4704(%rdx), %esi
	cmpl	$2, %esi
	je	.LBB24_6
# BB#3:
	cmpl	$1, %esi
	je	.LBB24_4
# BB#5:
	cmpl	$0, 4708(%rdx)
	je	.LBB24_9
.LBB24_6:
	movl	1376(%rdi), %edx
	cmpl	$0, 4(%rcx)
	je	.LBB24_7
# BB#8:
	movl	%eax, 1456(%rdi)
	movl	%edx, 1468(%rdi)
	jmp	.LBB24_9
.LBB24_7:
	movl	%eax, 1460(%rdi)
	movl	%edx, 1464(%rdi)
	jmp	.LBB24_9
.Lfunc_end24:
	.size	updateFirstP, .Lfunc_end24-updateFirstP
	.cfi_endproc

	.globl	updateFirstBU
	.p2align	4, 0x90
	.type	updateFirstBU,@function
updateFirstBU:                          # @updateFirstBU
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rax
	cmpl	$2, 4704(%rax)
	je	.LBB25_2
# BB#1:
	cmpl	$0, 4708(%rax)
	je	.LBB25_11
.LBB25_2:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	jne	.LBB25_11
# BB#3:
	movl	48(%rax), %ecx
	cmpl	$1, 8(%rax)
	jne	.LBB25_7
# BB#4:
	movl	1460(%rdi), %edx
	testl	%ecx, %ecx
	jle	.LBB25_6
# BB#5:
	addl	%edx, 44(%rax)
.LBB25_6:                               # %._crit_edge27
	movl	%edx, 1384(%rdi)
	leaq	1464(%rdi), %rax
	jmp	.LBB25_10
.LBB25_7:
	movl	1456(%rdi), %edx
	testl	%ecx, %ecx
	jle	.LBB25_9
# BB#8:
	addl	%edx, 44(%rax)
.LBB25_9:                               # %._crit_edge
	movl	%edx, 1384(%rdi)
	leaq	1468(%rdi), %rax
.LBB25_10:
	movl	(%rax), %eax
	movl	%eax, 1380(%rdi)
.LBB25_11:
	movl	1384(%rdi), %ecx
	cmpl	$0, 1536(%rdi)
	jle	.LBB25_12
# BB#15:
	movl	%ecx, 1344(%rdi)
	movl	%ecx, %eax
	jmp	.LBB25_16
.LBB25_12:
	leal	2(%rcx), %eax
	movl	64(%rdi), %edx
	cmpl	%edx, %eax
	cmovgl	%edx, %eax
	movl	%eax, 1344(%rdi)
	testl	%esi, %esi
	jne	.LBB25_14
# BB#13:
	movq	generic_RC(%rip), %rdx
	cmpl	$0, 4(%rdx)
	jne	.LBB25_16
.LBB25_14:
	movl	$1, 1504(%rdi)
.LBB25_16:
	addl	%eax, 1364(%rdi)
	decl	1368(%rdi)
	movl	%ecx, 1328(%rdi)
	retq
.Lfunc_end25:
	.size	updateFirstBU, .Lfunc_end25-updateFirstBU
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI26_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	updateNegativeTarget
	.p2align	4, 0x90
	.type	updateNegativeTarget,@function
updateNegativeTarget:                   # @updateNegativeTarget
	.cfi_startproc
# BB#0:
	movl	1504(%rdi), %r8d
	cmpl	$1, %r8d
	jne	.LBB26_2
# BB#1:
	addl	$2, %edx
	jmp	.LBB26_3
.LBB26_2:
	addl	1440(%rdi), %edx
.LBB26_3:
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 24
.Lcfi132:
	.cfi_offset %rbx, -24
.Lcfi133:
	.cfi_offset %r14, -16
	leaq	1344(%rdi), %r10
	movl	64(%rdi), %eax
	cmpl	%eax, %edx
	cmovlel	%edx, %eax
	movq	input(%rip), %r14
	movl	5128(%r14), %ecx
	leaq	1384(%rdi), %r9
	movl	1384(%rdi), %edx
	xorl	%ebx, %ebx
	cmpl	1444(%rdi), %ecx
	setae	%bl
	leal	(%rbx,%rbx,2), %ecx
	leal	3(%rdx,%rcx), %ecx
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	movl	%eax, 1344(%rdi)
	movl	1364(%rdi), %ecx
	addl	%eax, %ecx
	movl	%ecx, 1364(%rdi)
	decl	1368(%rdi)
	jne	.LBB26_23
# BB#4:
	testl	%esi, %esi
	je	.LBB26_6
# BB#5:
	movq	generic_RC(%rip), %rdx
	cmpl	$0, 4(%rdx)
	jne	.LBB26_23
.LBB26_6:
	movq	active_sps(%rip), %rdx
	cmpl	$0, 1148(%rdx)
	je	.LBB26_7
.LBB26_9:
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%rdi), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI26_0(%rip), %xmm0
	cvttsd2si	%xmm0, %r11d
	movq	generic_RC(%rip), %rcx
	movl	48(%rcx), %esi
	movl	1560(%r14), %edx
	addl	$-2, %edx
	cmpl	%edx, %esi
	jne	.LBB26_11
# BB#10:
	movl	%r11d, 1448(%rdi)
.LBB26_11:
	addl	%r11d, 44(%rcx)
	cmpl	$1, %r8d
	je	.LBB26_16
# BB#12:
	testl	%esi, %esi
	je	.LBB26_13
# BB#15:
	jle	.LBB26_18
.LBB26_16:
	movl	1356(%rdi), %ecx
	incl	%ecx
.LBB26_17:                              # %.sink.split
	movl	%ecx, 1352(%rdi)
	movl	%r11d, 1356(%rdi)
.LBB26_18:                              # %.thread
	movl	%r11d, 1384(%rdi)
	movl	1376(%rdi), %ecx
	movl	%ecx, 1380(%rdi)
.LBB26_23:
	cmpl	$1, %r8d
	cmoveq	%r9, %r10
	movl	(%r10), %ecx
	movl	%ecx, 1328(%rdi)
	popq	%rbx
	popq	%r14
	retq
.LBB26_7:
	movl	4704(%r14), %esi
	cmpl	$2, %esi
	je	.LBB26_20
# BB#8:
	cmpl	$1, %esi
	je	.LBB26_9
# BB#19:
	cmpl	$0, 4708(%r14)
	je	.LBB26_23
.LBB26_20:
	movq	generic_RC(%rip), %rsi
	cvtsi2sdl	%ecx, %xmm0
	cvtsi2sdl	1388(%rdi), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI26_0(%rip), %xmm0
	cvttsd2si	%xmm0, %edx
	movl	1376(%rdi), %ecx
	cmpl	$0, 4(%rsi)
	je	.LBB26_21
# BB#22:
	movl	%edx, 1456(%rdi)
	movl	%ecx, 1468(%rdi)
	jmp	.LBB26_23
.LBB26_13:
	cmpl	$2, 40(%rcx)
	jl	.LBB26_18
# BB#14:
	movl	1356(%rdi), %ecx
	jmp	.LBB26_17
.LBB26_21:
	movl	%edx, 1460(%rdi)
	movl	%ecx, 1464(%rdi)
	jmp	.LBB26_23
.Lfunc_end26:
	.size	updateNegativeTarget, .Lfunc_end26-updateNegativeTarget
	.cfi_endproc

	.globl	predictCurrPicMAD
	.p2align	4, 0x90
	.type	predictCurrPicMAD,@function
predictCurrPicMAD:                      # @predictCurrPicMAD
	.cfi_startproc
# BB#0:
	movq	input(%rip), %rax
	cmpl	$2, 4704(%rax)
	je	.LBB27_2
# BB#1:
	cmpl	$0, 4708(%rax)
	je	.LBB27_6
.LBB27_2:
	movq	generic_RC(%rip), %rax
	cmpl	$1, 4(%rax)
	jne	.LBB27_6
# BB#3:
	movsd	88(%rdi), %xmm0         # xmm0 = mem[0],zero
	movsd	96(%rdi), %xmm1         # xmm1 = mem[0],zero
	movq	1496(%rdi), %rax
	movslq	1388(%rdi), %rcx
	movslq	1368(%rdi), %rsi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	testq	%rsi, %rsi
	movsd	(%rax,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 1400(%rdi)
	movq	$0, 1416(%rdi)
	jle	.LBB27_9
# BB#4:                                 # %.lr.ph
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB27_5:                               # =>This Inner Loop Header: Depth=1
	movsd	-8(%rax,%rcx,8), %xmm3  # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	decq	%rcx
	addsd	%xmm1, %xmm3
	movsd	%xmm3, 1408(%rdi)
	mulsd	%xmm3, %xmm3
	addsd	%xmm3, %xmm2
	movsd	%xmm2, 1416(%rdi)
	cmpq	%rdx, %rcx
	jg	.LBB27_5
	jmp	.LBB27_9
.LBB27_6:
	movsd	88(%rdi), %xmm0         # xmm0 = mem[0],zero
	movsd	96(%rdi), %xmm1         # xmm1 = mem[0],zero
	movq	1472(%rdi), %rax
	movslq	1388(%rdi), %rcx
	movslq	1368(%rdi), %rsi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	testq	%rsi, %rsi
	movsd	(%rax,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 1400(%rdi)
	movq	$0, 1416(%rdi)
	jle	.LBB27_9
# BB#7:                                 # %.lr.ph45
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB27_8:                               # =>This Inner Loop Header: Depth=1
	movsd	-8(%rax,%rcx,8), %xmm3  # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	decq	%rcx
	addsd	%xmm1, %xmm3
	movsd	%xmm3, 1408(%rdi)
	mulsd	%xmm3, %xmm3
	addsd	%xmm3, %xmm2
	movsd	%xmm2, 1416(%rdi)
	cmpq	%rdx, %rcx
	jg	.LBB27_8
.LBB27_9:                               # %.loopexit
	retq
.Lfunc_end27:
	.size	predictCurrPicMAD, .Lfunc_end27-predictCurrPicMAD
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI28_0:
	.long	1082130432              # float 4
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI28_1:
	.quad	4616189618054758400     # double 4
	.text
	.globl	updateModelQPBU
	.p2align	4, 0x90
	.type	updateModelQPBU,@function
updateModelQPBU:                        # @updateModelQPBU
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi136:
	.cfi_def_cfa_offset 64
.Lcfi137:
	.cfi_offset %rbx, -24
.Lcfi138:
	.cfi_offset %r14, -16
	movl	%edx, %r14d
	movq	%rdi, %rbx
	cvtsi2sdl	1536(%rbx), %xmm0
	movsd	1400(%rbx), %xmm4       # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm0
	mulsd	%xmm4, %xmm0
	divsd	1416(%rbx), %xmm0
	cvttsd2si	%xmm0, %eax
	subl	1376(%rbx), %eax
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	.LCPI28_0(%rip), %xmm1
	cvtsi2ssl	1388(%rbx), %xmm2
	mulss	%xmm1, %xmm2
	divss	%xmm2, %xmm0
	cvttss2si	%xmm0, %ecx
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	movsd	1304(%rbx), %xmm2       # xmm2 = mem[0],zero
	cvtsi2sdl	%eax, %xmm5
	xorps	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm2
	jne	.LBB28_1
	jnp	.LBB28_5
.LBB28_1:
	movapd	%xmm4, %xmm3
	unpcklpd	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	movapd	%xmm4, %xmm2
	movhpd	.LCPI28_1(%rip), %xmm2  # xmm2 = xmm2[0],mem[0]
	mulpd	%xmm3, %xmm2
	movapd	%xmm1, %xmm3
	unpcklpd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0]
	mulpd	%xmm2, %xmm3
	movapd	%xmm1, %xmm2
	unpcklpd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
	mulpd	%xmm3, %xmm2
	movapd	%xmm2, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	addsd	%xmm2, %xmm3
	ucomisd	%xmm3, %xmm0
	ja	.LBB28_5
# BB#2:
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB28_4
# BB#3:                                 # %call.sqrt
	movapd	%xmm3, %xmm0
	movapd	%xmm5, (%rsp)           # 16-byte Spill
	movapd	%xmm3, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm3         # 16-byte Reload
	movapd	(%rsp), %xmm5           # 16-byte Reload
.LBB28_4:                               # %.split
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	movsd	1400(%rbx), %xmm4       # xmm4 = mem[0],zero
	movapd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jae	.LBB28_5
# BB#6:
	movsd	1304(%rbx), %xmm0       # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB28_8
# BB#7:                                 # %call.sqrt70
	movapd	%xmm3, %xmm0
	movapd	%xmm4, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm4           # 16-byte Reload
.LBB28_8:                               # %.split69
	movsd	1296(%rbx), %xmm1       # xmm1 = mem[0],zero
	mulsd	1400(%rbx), %xmm1
	subsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm4
	jmp	.LBB28_9
.LBB28_5:
	mulsd	%xmm1, %xmm4
	divsd	%xmm5, %xmm4
.LBB28_9:
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm4, %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	Qstep2QP
	movl	1440(%rbx), %ecx
	leal	(%rcx,%r14), %edx
	cmpl	%eax, %edx
	cmovlel	%edx, %eax
	movq	input(%rip), %rdx
	movl	5128(%rdx), %edx
	xorl	%esi, %esi
	cmpl	1444(%rbx), %edx
	movl	1384(%rbx), %edx
	setae	%sil
	leal	(%rsi,%rsi,2), %esi
	leal	3(%rdx,%rsi), %esi
	sbbl	%edi, %edi
	cmpl	%eax, %esi
	cmovlel	%esi, %eax
	subl	%ecx, %r14d
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	movl	64(%rbx), %eax
	movl	68(%rbx), %ecx
	cmpl	%eax, %r14d
	cmovgl	%eax, %r14d
	andl	$1, %edi
	leal	(%rdi,%rdi,2), %eax
	leal	-6(%rdx,%rax), %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	cmpl	%r14d, %ecx
	cmovgel	%ecx, %r14d
	movl	%r14d, 1344(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end28:
	.size	updateModelQPBU, .Lfunc_end28-updateModelQPBU
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI29_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	updateLastBU
	.p2align	4, 0x90
	.type	updateLastBU,@function
updateLastBU:                           # @updateLastBU
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	je	.LBB29_2
# BB#1:
	movq	generic_RC(%rip), %rax
	cmpl	$0, 4(%rax)
	je	.LBB29_2
.LBB29_12:
	retq
.LBB29_2:
	movq	active_sps(%rip), %rcx
	movq	input(%rip), %rax
	cmpl	$0, 1148(%rcx)
	je	.LBB29_3
.LBB29_5:                               # %._crit_edge
	cvtsi2sdl	1364(%rdi), %xmm0
	cvtsi2sdl	1388(%rdi), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI29_0(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
	movq	generic_RC(%rip), %rdx
	movl	1560(%rax), %eax
	addl	$-2, %eax
	cmpl	%eax, 48(%rdx)
	jne	.LBB29_7
# BB#6:
	movl	%ecx, 1448(%rdi)
.LBB29_7:
	addl	%ecx, 44(%rdx)
	movl	1356(%rdi), %eax
	movl	%eax, 1352(%rdi)
	movl	%ecx, 1356(%rdi)
	movl	%ecx, 1384(%rdi)
	movl	1376(%rdi), %eax
	movl	%eax, 1380(%rdi)
	retq
.LBB29_3:
	movl	4704(%rax), %ecx
	cmpl	$2, %ecx
	je	.LBB29_9
# BB#4:
	cmpl	$1, %ecx
	je	.LBB29_5
# BB#8:
	cmpl	$0, 4708(%rax)
	je	.LBB29_12
.LBB29_9:
	movq	generic_RC(%rip), %rdx
	cvtsi2sdl	1364(%rdi), %xmm0
	cvtsi2sdl	1388(%rdi), %xmm1
	divsd	%xmm1, %xmm0
	addsd	.LCPI29_0(%rip), %xmm0
	cvttsd2si	%xmm0, %ecx
	movl	1376(%rdi), %eax
	cmpl	$0, 4(%rdx)
	je	.LBB29_10
# BB#11:
	movl	%ecx, 1456(%rdi)
	movl	%eax, 1468(%rdi)
	retq
.LBB29_10:
	movl	%ecx, 1460(%rdi)
	movl	%eax, 1464(%rdi)
	retq
.Lfunc_end29:
	.size	updateLastBU, .Lfunc_end29-updateLastBU
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"init_global_buffers: (*prc)"
	.size	.L.str, 28

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"rc_alloc: lprc->BUPFMAD"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rc_alloc: lprc->BUCFMAD"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"rc_alloc: lprc->FCBUCFMAD"
	.size	.L.str.3, 26

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"rc_alloc: lprc->FCBUPFMAD"
	.size	.L.str.4, 26

	.type	updateQP,@object        # @updateQP
	.comm	updateQP,8,8
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n RCUpdateMode=3 and HierarchicalCoding == 3 are currently not supported"
	.size	.L.str.5, 73

	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	updateRCModel.m_rgRejected,@object # @updateRCModel.m_rgRejected
	.local	updateRCModel.m_rgRejected
	.comm	updateRCModel.m_rgRejected,84,16
	.type	updateRCModel.error,@object # @updateRCModel.error
	.local	updateRCModel.error
	.comm	updateRCModel.error,168,16
	.type	updateMADModel.PictureRejected,@object # @updateMADModel.PictureRejected
	.local	updateMADModel.PictureRejected
	.comm	updateMADModel.PictureRejected,84,16
	.type	updateMADModel.error,@object # @updateMADModel.error
	.local	updateMADModel.error
	.comm	updateMADModel.error,168,16
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	quadratic_RC,@object    # @quadratic_RC
	.comm	quadratic_RC,8,8
	.type	quadratic_RC_init,@object # @quadratic_RC_init
	.comm	quadratic_RC_init,8,8
	.type	quadratic_RC_best,@object # @quadratic_RC_best
	.comm	quadratic_RC_best,8,8
	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	updateQPRC0
	.quad	updateQPRC1
	.quad	updateQPRC2
	.quad	updateQPRC3
	.size	.Lswitch.table, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
