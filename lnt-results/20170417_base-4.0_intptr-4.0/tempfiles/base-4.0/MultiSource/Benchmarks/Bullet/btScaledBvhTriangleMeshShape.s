	.text
	.file	"btScaledBvhTriangleMeshShape.bc"
	.globl	_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3
	.p2align	4, 0x90
	.type	_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3,@function
_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3: # @_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN14btConcaveShapeC2Ev
	movq	$_ZTV28btScaledBvhTriangleMeshShape+16, (%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 28(%rbx)
	movq	%r14, 48(%rbx)
	movl	$22, 8(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3, .Lfunc_end0-_ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3
	.cfi_endproc

	.globl	_ZN28btScaledBvhTriangleMeshShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN28btScaledBvhTriangleMeshShapeD2Ev,@function
_ZN28btScaledBvhTriangleMeshShapeD2Ev:  # @_ZN28btScaledBvhTriangleMeshShapeD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN14btConcaveShapeD2Ev # TAILCALL
.Lfunc_end1:
	.size	_ZN28btScaledBvhTriangleMeshShapeD2Ev, .Lfunc_end1-_ZN28btScaledBvhTriangleMeshShapeD2Ev
	.cfi_endproc

	.globl	_ZN28btScaledBvhTriangleMeshShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN28btScaledBvhTriangleMeshShapeD0Ev,@function
_ZN28btScaledBvhTriangleMeshShapeD0Ev:  # @_ZN28btScaledBvhTriangleMeshShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN14btConcaveShapeD2Ev
.Ltmp1:
# BB#1:                                 # %_ZN28btScaledBvhTriangleMeshShapeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN28btScaledBvhTriangleMeshShapeD0Ev, .Lfunc_end2-_ZN28btScaledBvhTriangleMeshShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1065353216              # float 1
	.text
	.globl	_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_,@function
_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_: # @_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
	subq	$64, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 80
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	$_ZTV24btScaledTriangleCallback+16, 32(%rsp)
	movq	%rsi, 40(%rsp)
	movups	28(%rdi), %xmm0
	movups	%xmm0, 48(%rsp)
	movss	28(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	32(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm5
	divss	%xmm6, %xmm5
	movaps	%xmm0, %xmm1
	divss	%xmm2, %xmm1
	movss	36(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm0
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm6
	movq	%rcx, %rax
	cmovaeq	%rdx, %rax
	movss	(%rax), %xmm7           # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	movss	%xmm7, 16(%rsp)
	ucomiss	%xmm4, %xmm2
	movq	%rcx, %rax
	cmovaeq	%rdx, %rax
	movss	4(%rax), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	movss	%xmm7, 20(%rsp)
	ucomiss	%xmm4, %xmm3
	movq	%rcx, %rax
	cmovaeq	%rdx, %rax
	movss	8(%rax), %xmm7          # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	movss	%xmm7, 24(%rsp)
	ucomiss	%xmm6, %xmm4
	movq	%rcx, %rax
	cmovaeq	%rdx, %rax
	mulss	(%rax), %xmm5
	movss	%xmm5, (%rsp)
	ucomiss	%xmm2, %xmm4
	movq	%rcx, %rax
	cmovaeq	%rdx, %rax
	mulss	4(%rax), %xmm1
	movss	%xmm1, 4(%rsp)
	ucomiss	%xmm3, %xmm4
	cmovaeq	%rdx, %rcx
	mulss	8(%rcx), %xmm0
	movss	%xmm0, 8(%rsp)
	movq	48(%rdi), %rdi
	movq	(%rdi), %rax
.Ltmp3:
	leaq	32(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%rsp, %rcx
	callq	*96(%rax)
.Ltmp4:
# BB#1:
	leaq	32(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
	addq	$64, %rsp
	popq	%rbx
	retq
.LBB3_2:
.Ltmp5:
	movq	%rax, %rbx
.Ltmp6:
	leaq	32(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp7:
# BB#3:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_4:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_, .Lfunc_end3-_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp4           #   Call between .Ltmp4 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1056964608              # float 0.5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 32
	subq	$144, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 176
.Lcfi18:
	.cfi_offset %rbx, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %rax
	movq	48(%rax), %rdi
	movsd	28(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	44(%rdi), %xmm3         # xmm3 = mem[0],zero
	movss	52(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movsd	28(%rax), %xmm1         # xmm1 = mem[0],zero
	mulps	%xmm1, %xmm2
	movss	36(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	xorps	%xmm6, %xmm6
	movss	%xmm5, %xmm6            # xmm6 = xmm5[0],xmm6[1,2,3]
	movlps	%xmm2, 32(%rsp)
	movlps	%xmm6, 40(%rsp)
	mulps	%xmm1, %xmm3
	mulss	%xmm0, %xmm4
	xorps	%xmm2, %xmm2
	movss	%xmm4, %xmm2            # xmm2 = xmm4[0],xmm2[1,2,3]
	movlps	%xmm3, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	leaq	32(%rsp), %rax
	leaq	16(%rsp), %rcx
	movq	%rcx, %rdx
	cmovaeq	%rax, %rdx
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, 128(%rsp)        # 16-byte Spill
	pshufd	$229, %xmm1, %xmm3      # xmm3 = xmm1[1,1,2,3]
	ucomiss	%xmm2, %xmm3
	movq	%rcx, %rdx
	cmovaeq	%rax, %rdx
	orq	$4, %rdx
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, 112(%rsp)        # 16-byte Spill
	ucomiss	%xmm2, %xmm0
	movq	%rcx, %rdx
	cmovaeq	%rax, %rdx
	movss	8(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, 96(%rsp)         # 16-byte Spill
	ucomiss	%xmm1, %xmm2
	movq	%rcx, %rdx
	cmovaeq	%rax, %rdx
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	ucomiss	%xmm3, %xmm2
	movq	%rcx, %rdx
	cmovaeq	%rax, %rdx
	orq	$4, %rdx
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	ucomiss	%xmm0, %xmm2
	cmovaeq	%rax, %rcx
	movss	8(%rcx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	subss	%xmm4, %xmm1
	movaps	%xmm3, %xmm2
	subss	%xmm5, %xmm2
	subss	%xmm6, %xmm0
	movss	.LCPI5_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movaps	%xmm1, 48(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm2
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movq	(%rdi), %rax
	callq	*88(%rax)
	movaps	48(%rsp), %xmm14        # 16-byte Reload
	addss	%xmm0, %xmm14
	movaps	64(%rsp), %xmm13        # 16-byte Reload
	addss	%xmm0, %xmm13
	movaps	80(%rsp), %xmm12        # 16-byte Reload
	addss	%xmm0, %xmm12
	movaps	128(%rsp), %xmm1        # 16-byte Reload
	addss	12(%rsp), %xmm1         # 4-byte Folded Reload
	movaps	112(%rsp), %xmm2        # 16-byte Reload
	addss	8(%rsp), %xmm2          # 4-byte Folded Reload
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	addss	4(%rsp), %xmm0          # 4-byte Folded Reload
	movss	.LCPI5_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movaps	%xmm1, %xmm9
	mulss	%xmm3, %xmm2
	movaps	%xmm2, %xmm11
	mulss	%xmm3, %xmm0
	movaps	%xmm0, %xmm8
	movss	16(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	movaps	%xmm9, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	andps	%xmm0, %xmm2
	movss	20(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	movaps	%xmm11, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm1, %xmm4
	movaps	%xmm1, %xmm5
	andps	%xmm0, %xmm5
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	movaps	%xmm8, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm6, %xmm7
	andps	%xmm0, %xmm6
	addps	%xmm3, %xmm4
	movss	32(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm9
	movaps	%xmm1, %xmm3
	andps	%xmm0, %xmm3
	addps	%xmm4, %xmm7
	movsd	48(%rbx), %xmm10        # xmm10 = mem[0],zero
	addps	%xmm7, %xmm10
	movss	36(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm7
	mulss	%xmm4, %xmm7
	andps	%xmm0, %xmm4
	addss	%xmm9, %xmm7
	movaps	%xmm7, %xmm1
	movss	40(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	andps	%xmm7, %xmm0
	mulss	%xmm7, %xmm8
	addss	%xmm1, %xmm8
	movaps	%xmm14, %xmm9
	movaps	%xmm9, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm2, %xmm7
	movaps	%xmm13, %xmm11
	movaps	%xmm11, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	addps	%xmm7, %xmm2
	movaps	%xmm12, %xmm1
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	addps	%xmm2, %xmm5
	mulss	%xmm9, %xmm3
	mulss	%xmm11, %xmm4
	addss	%xmm3, %xmm4
	addss	56(%rbx), %xmm8
	mulss	%xmm1, %xmm0
	addss	%xmm4, %xmm0
	movaps	%xmm8, %xmm2
	subss	%xmm0, %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movaps	%xmm10, %xmm2
	subps	%xmm5, %xmm2
	movlps	%xmm2, (%r15)
	movlps	%xmm3, 8(%r15)
	addps	%xmm10, %xmm5
	addss	%xmm8, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm5, (%r14)
	movlps	%xmm1, 8(%r14)
	addq	$144, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end5-_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.globl	_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3,@function
_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3: # @_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movups	(%rsi), %xmm0
	movups	%xmm0, 28(%rdi)
	retq
.Lfunc_end6:
	.size	_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3, .Lfunc_end6-_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv,@function
_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv: # @_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	leaq	28(%rdi), %rax
	retq
.Lfunc_end7:
	.size	_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv, .Lfunc_end7-_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv
	.cfi_endproc

	.globl	_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3: # @_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end8-_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.text._ZNK28btScaledBvhTriangleMeshShape7getNameEv,"axG",@progbits,_ZNK28btScaledBvhTriangleMeshShape7getNameEv,comdat
	.weak	_ZNK28btScaledBvhTriangleMeshShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK28btScaledBvhTriangleMeshShape7getNameEv,@function
_ZNK28btScaledBvhTriangleMeshShape7getNameEv: # @_ZNK28btScaledBvhTriangleMeshShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end9:
	.size	_ZNK28btScaledBvhTriangleMeshShape7getNameEv, .Lfunc_end9-_ZNK28btScaledBvhTriangleMeshShape7getNameEv
	.cfi_endproc

	.section	.text._ZN14btConcaveShape9setMarginEf,"axG",@progbits,_ZN14btConcaveShape9setMarginEf,comdat
	.weak	_ZN14btConcaveShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN14btConcaveShape9setMarginEf,@function
_ZN14btConcaveShape9setMarginEf:        # @_ZN14btConcaveShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 24(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN14btConcaveShape9setMarginEf, .Lfunc_end10-_ZN14btConcaveShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK14btConcaveShape9getMarginEv,"axG",@progbits,_ZNK14btConcaveShape9getMarginEv,comdat
	.weak	_ZNK14btConcaveShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK14btConcaveShape9getMarginEv,@function
_ZNK14btConcaveShape9getMarginEv:       # @_ZNK14btConcaveShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	24(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end11:
	.size	_ZNK14btConcaveShape9getMarginEv, .Lfunc_end11-_ZNK14btConcaveShape9getMarginEv
	.cfi_endproc

	.section	.text._ZN24btScaledTriangleCallbackD0Ev,"axG",@progbits,_ZN24btScaledTriangleCallbackD0Ev,comdat
	.weak	_ZN24btScaledTriangleCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN24btScaledTriangleCallbackD0Ev,@function
_ZN24btScaledTriangleCallbackD0Ev:      # @_ZN24btScaledTriangleCallbackD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp9:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp10:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_2:
.Ltmp11:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN24btScaledTriangleCallbackD0Ev, .Lfunc_end12-_ZN24btScaledTriangleCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin2   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp10    #   Call between .Ltmp10 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii,"axG",@progbits,_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii,comdat
	.weak	_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii,@function
_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii: # @_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 64
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movsd	16(%rdi), %xmm1         # xmm1 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	24(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	xorps	%xmm4, %xmm4
	xorps	%xmm5, %xmm5
	movss	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1,2,3]
	movlps	%xmm0, (%rsp)
	movlps	%xmm5, 8(%rsp)
	movsd	16(%rsi), %xmm0         # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	movss	24(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	xorps	%xmm5, %xmm5
	movss	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm5, 24(%rsp)
	movsd	32(%rsi), %xmm0         # xmm0 = mem[0],zero
	mulps	%xmm1, %xmm0
	mulss	40(%rsi), %xmm2
	movss	%xmm2, %xmm4            # xmm4 = xmm2[0],xmm4[1,2,3]
	movlps	%xmm0, 32(%rsp)
	movlps	%xmm4, 40(%rsp)
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rsi
	callq	*16(%rax)
	addq	$56, %rsp
	retq
.Lfunc_end13:
	.size	_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii, .Lfunc_end13-_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii
	.cfi_endproc

	.type	_ZTV28btScaledBvhTriangleMeshShape,@object # @_ZTV28btScaledBvhTriangleMeshShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV28btScaledBvhTriangleMeshShape
	.p2align	3
_ZTV28btScaledBvhTriangleMeshShape:
	.quad	0
	.quad	_ZTI28btScaledBvhTriangleMeshShape
	.quad	_ZN28btScaledBvhTriangleMeshShapeD2Ev
	.quad	_ZN28btScaledBvhTriangleMeshShapeD0Ev
	.quad	_ZNK28btScaledBvhTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN28btScaledBvhTriangleMeshShape15setLocalScalingERK9btVector3
	.quad	_ZNK28btScaledBvhTriangleMeshShape15getLocalScalingEv
	.quad	_ZNK28btScaledBvhTriangleMeshShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK28btScaledBvhTriangleMeshShape7getNameEv
	.quad	_ZN14btConcaveShape9setMarginEf
	.quad	_ZNK14btConcaveShape9getMarginEv
	.quad	_ZNK28btScaledBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_
	.size	_ZTV28btScaledBvhTriangleMeshShape, 120

	.type	_ZTS28btScaledBvhTriangleMeshShape,@object # @_ZTS28btScaledBvhTriangleMeshShape
	.globl	_ZTS28btScaledBvhTriangleMeshShape
	.p2align	4
_ZTS28btScaledBvhTriangleMeshShape:
	.asciz	"28btScaledBvhTriangleMeshShape"
	.size	_ZTS28btScaledBvhTriangleMeshShape, 31

	.type	_ZTI28btScaledBvhTriangleMeshShape,@object # @_ZTI28btScaledBvhTriangleMeshShape
	.globl	_ZTI28btScaledBvhTriangleMeshShape
	.p2align	4
_ZTI28btScaledBvhTriangleMeshShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS28btScaledBvhTriangleMeshShape
	.quad	_ZTI14btConcaveShape
	.size	_ZTI28btScaledBvhTriangleMeshShape, 24

	.type	_ZTV24btScaledTriangleCallback,@object # @_ZTV24btScaledTriangleCallback
	.section	.rodata._ZTV24btScaledTriangleCallback,"aG",@progbits,_ZTV24btScaledTriangleCallback,comdat
	.weak	_ZTV24btScaledTriangleCallback
	.p2align	3
_ZTV24btScaledTriangleCallback:
	.quad	0
	.quad	_ZTI24btScaledTriangleCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZN24btScaledTriangleCallbackD0Ev
	.quad	_ZN24btScaledTriangleCallback15processTriangleEP9btVector3ii
	.size	_ZTV24btScaledTriangleCallback, 40

	.type	_ZTS24btScaledTriangleCallback,@object # @_ZTS24btScaledTriangleCallback
	.section	.rodata._ZTS24btScaledTriangleCallback,"aG",@progbits,_ZTS24btScaledTriangleCallback,comdat
	.weak	_ZTS24btScaledTriangleCallback
	.p2align	4
_ZTS24btScaledTriangleCallback:
	.asciz	"24btScaledTriangleCallback"
	.size	_ZTS24btScaledTriangleCallback, 27

	.type	_ZTI24btScaledTriangleCallback,@object # @_ZTI24btScaledTriangleCallback
	.section	.rodata._ZTI24btScaledTriangleCallback,"aG",@progbits,_ZTI24btScaledTriangleCallback,comdat
	.weak	_ZTI24btScaledTriangleCallback
	.p2align	4
_ZTI24btScaledTriangleCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS24btScaledTriangleCallback
	.quad	_ZTI18btTriangleCallback
	.size	_ZTI24btScaledTriangleCallback, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SCALEDBVHTRIANGLEMESH"
	.size	.L.str, 22


	.globl	_ZN28btScaledBvhTriangleMeshShapeC1EP22btBvhTriangleMeshShapeRK9btVector3
	.type	_ZN28btScaledBvhTriangleMeshShapeC1EP22btBvhTriangleMeshShapeRK9btVector3,@function
_ZN28btScaledBvhTriangleMeshShapeC1EP22btBvhTriangleMeshShapeRK9btVector3 = _ZN28btScaledBvhTriangleMeshShapeC2EP22btBvhTriangleMeshShapeRK9btVector3
	.globl	_ZN28btScaledBvhTriangleMeshShapeD1Ev
	.type	_ZN28btScaledBvhTriangleMeshShapeD1Ev,@function
_ZN28btScaledBvhTriangleMeshShapeD1Ev = _ZN28btScaledBvhTriangleMeshShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
