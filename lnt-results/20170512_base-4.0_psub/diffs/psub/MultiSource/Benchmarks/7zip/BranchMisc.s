	.text
	.file	"BranchMisc.bc"
	.globl	_ZN15CBC_ARM_Encoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN15CBC_ARM_Encoder9SubFilterEPhj,@function
_ZN15CBC_ARM_Encoder9SubFilterEPhj:     # @_ZN15CBC_ARM_Encoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	movl	$1, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	ARM_Convert             # TAILCALL
.Lfunc_end0:
	.size	_ZN15CBC_ARM_Encoder9SubFilterEPhj, .Lfunc_end0-_ZN15CBC_ARM_Encoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN15CBC_ARM_Decoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN15CBC_ARM_Decoder9SubFilterEPhj,@function
_ZN15CBC_ARM_Decoder9SubFilterEPhj:     # @_ZN15CBC_ARM_Decoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	ARM_Convert             # TAILCALL
.Lfunc_end1:
	.size	_ZN15CBC_ARM_Decoder9SubFilterEPhj, .Lfunc_end1-_ZN15CBC_ARM_Decoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN16CBC_ARMT_Encoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN16CBC_ARMT_Encoder9SubFilterEPhj,@function
_ZN16CBC_ARMT_Encoder9SubFilterEPhj:    # @_ZN16CBC_ARMT_Encoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	movl	$1, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	ARMT_Convert            # TAILCALL
.Lfunc_end2:
	.size	_ZN16CBC_ARMT_Encoder9SubFilterEPhj, .Lfunc_end2-_ZN16CBC_ARMT_Encoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN16CBC_ARMT_Decoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN16CBC_ARMT_Decoder9SubFilterEPhj,@function
_ZN16CBC_ARMT_Decoder9SubFilterEPhj:    # @_ZN16CBC_ARMT_Decoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	ARMT_Convert            # TAILCALL
.Lfunc_end3:
	.size	_ZN16CBC_ARMT_Decoder9SubFilterEPhj, .Lfunc_end3-_ZN16CBC_ARMT_Decoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN15CBC_PPC_Encoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN15CBC_PPC_Encoder9SubFilterEPhj,@function
_ZN15CBC_PPC_Encoder9SubFilterEPhj:     # @_ZN15CBC_PPC_Encoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	movl	$1, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	PPC_Convert             # TAILCALL
.Lfunc_end4:
	.size	_ZN15CBC_PPC_Encoder9SubFilterEPhj, .Lfunc_end4-_ZN15CBC_PPC_Encoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN15CBC_PPC_Decoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN15CBC_PPC_Decoder9SubFilterEPhj,@function
_ZN15CBC_PPC_Decoder9SubFilterEPhj:     # @_ZN15CBC_PPC_Decoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	PPC_Convert             # TAILCALL
.Lfunc_end5:
	.size	_ZN15CBC_PPC_Decoder9SubFilterEPhj, .Lfunc_end5-_ZN15CBC_PPC_Decoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN17CBC_SPARC_Encoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN17CBC_SPARC_Encoder9SubFilterEPhj,@function
_ZN17CBC_SPARC_Encoder9SubFilterEPhj:   # @_ZN17CBC_SPARC_Encoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	movl	$1, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	SPARC_Convert           # TAILCALL
.Lfunc_end6:
	.size	_ZN17CBC_SPARC_Encoder9SubFilterEPhj, .Lfunc_end6-_ZN17CBC_SPARC_Encoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN17CBC_SPARC_Decoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN17CBC_SPARC_Decoder9SubFilterEPhj,@function
_ZN17CBC_SPARC_Decoder9SubFilterEPhj:   # @_ZN17CBC_SPARC_Decoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	SPARC_Convert           # TAILCALL
.Lfunc_end7:
	.size	_ZN17CBC_SPARC_Decoder9SubFilterEPhj, .Lfunc_end7-_ZN17CBC_SPARC_Decoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN16CBC_IA64_Encoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN16CBC_IA64_Encoder9SubFilterEPhj,@function
_ZN16CBC_IA64_Encoder9SubFilterEPhj:    # @_ZN16CBC_IA64_Encoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	movl	$1, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	IA64_Convert            # TAILCALL
.Lfunc_end8:
	.size	_ZN16CBC_IA64_Encoder9SubFilterEPhj, .Lfunc_end8-_ZN16CBC_IA64_Encoder9SubFilterEPhj
	.cfi_endproc

	.globl	_ZN16CBC_IA64_Decoder9SubFilterEPhj
	.p2align	4, 0x90
	.type	_ZN16CBC_IA64_Decoder9SubFilterEPhj,@function
_ZN16CBC_IA64_Decoder9SubFilterEPhj:    # @_ZN16CBC_IA64_Decoder9SubFilterEPhj
	.cfi_startproc
# BB#0:
	movl	%edx, %eax
	movl	12(%rdi), %edx
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	IA64_Convert            # TAILCALL
.Lfunc_end9:
	.size	_ZN16CBC_IA64_Decoder9SubFilterEPhj, .Lfunc_end9-_ZN16CBC_IA64_Decoder9SubFilterEPhj
	.cfi_endproc

	.section	.text._ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv,@function
_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv: # @_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB10_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB10_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB10_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB10_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB10_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB10_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB10_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB10_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB10_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB10_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB10_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB10_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB10_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB10_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB10_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB10_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB10_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv, .Lfunc_end10-_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN16CBranchConverter6AddRefEv,"axG",@progbits,_ZN16CBranchConverter6AddRefEv,comdat
	.weak	_ZN16CBranchConverter6AddRefEv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter6AddRefEv,@function
_ZN16CBranchConverter6AddRefEv:         # @_ZN16CBranchConverter6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end11:
	.size	_ZN16CBranchConverter6AddRefEv, .Lfunc_end11-_ZN16CBranchConverter6AddRefEv
	.cfi_endproc

	.section	.text._ZN16CBranchConverter7ReleaseEv,"axG",@progbits,_ZN16CBranchConverter7ReleaseEv,comdat
	.weak	_ZN16CBranchConverter7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter7ReleaseEv,@function
_ZN16CBranchConverter7ReleaseEv:        # @_ZN16CBranchConverter7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB12_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB12_2:
	popq	%rcx
	retq
.Lfunc_end12:
	.size	_ZN16CBranchConverter7ReleaseEv, .Lfunc_end12-_ZN16CBranchConverter7ReleaseEv
	.cfi_endproc

	.section	.text._ZN15CBC_ARM_EncoderD0Ev,"axG",@progbits,_ZN15CBC_ARM_EncoderD0Ev,comdat
	.weak	_ZN15CBC_ARM_EncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN15CBC_ARM_EncoderD0Ev,@function
_ZN15CBC_ARM_EncoderD0Ev:               # @_ZN15CBC_ARM_EncoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end13:
	.size	_ZN15CBC_ARM_EncoderD0Ev, .Lfunc_end13-_ZN15CBC_ARM_EncoderD0Ev
	.cfi_endproc

	.section	.text._ZN16CBranchConverter7SubInitEv,"axG",@progbits,_ZN16CBranchConverter7SubInitEv,comdat
	.weak	_ZN16CBranchConverter7SubInitEv
	.p2align	4, 0x90
	.type	_ZN16CBranchConverter7SubInitEv,@function
_ZN16CBranchConverter7SubInitEv:        # @_ZN16CBranchConverter7SubInitEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end14:
	.size	_ZN16CBranchConverter7SubInitEv, .Lfunc_end14-_ZN16CBranchConverter7SubInitEv
	.cfi_endproc

	.section	.text._ZN15CBC_ARM_DecoderD0Ev,"axG",@progbits,_ZN15CBC_ARM_DecoderD0Ev,comdat
	.weak	_ZN15CBC_ARM_DecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN15CBC_ARM_DecoderD0Ev,@function
_ZN15CBC_ARM_DecoderD0Ev:               # @_ZN15CBC_ARM_DecoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end15:
	.size	_ZN15CBC_ARM_DecoderD0Ev, .Lfunc_end15-_ZN15CBC_ARM_DecoderD0Ev
	.cfi_endproc

	.section	.text._ZN16CBC_ARMT_EncoderD0Ev,"axG",@progbits,_ZN16CBC_ARMT_EncoderD0Ev,comdat
	.weak	_ZN16CBC_ARMT_EncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN16CBC_ARMT_EncoderD0Ev,@function
_ZN16CBC_ARMT_EncoderD0Ev:              # @_ZN16CBC_ARMT_EncoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end16:
	.size	_ZN16CBC_ARMT_EncoderD0Ev, .Lfunc_end16-_ZN16CBC_ARMT_EncoderD0Ev
	.cfi_endproc

	.section	.text._ZN16CBC_ARMT_DecoderD0Ev,"axG",@progbits,_ZN16CBC_ARMT_DecoderD0Ev,comdat
	.weak	_ZN16CBC_ARMT_DecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN16CBC_ARMT_DecoderD0Ev,@function
_ZN16CBC_ARMT_DecoderD0Ev:              # @_ZN16CBC_ARMT_DecoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end17:
	.size	_ZN16CBC_ARMT_DecoderD0Ev, .Lfunc_end17-_ZN16CBC_ARMT_DecoderD0Ev
	.cfi_endproc

	.section	.text._ZN15CBC_PPC_EncoderD0Ev,"axG",@progbits,_ZN15CBC_PPC_EncoderD0Ev,comdat
	.weak	_ZN15CBC_PPC_EncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN15CBC_PPC_EncoderD0Ev,@function
_ZN15CBC_PPC_EncoderD0Ev:               # @_ZN15CBC_PPC_EncoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end18:
	.size	_ZN15CBC_PPC_EncoderD0Ev, .Lfunc_end18-_ZN15CBC_PPC_EncoderD0Ev
	.cfi_endproc

	.section	.text._ZN15CBC_PPC_DecoderD0Ev,"axG",@progbits,_ZN15CBC_PPC_DecoderD0Ev,comdat
	.weak	_ZN15CBC_PPC_DecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN15CBC_PPC_DecoderD0Ev,@function
_ZN15CBC_PPC_DecoderD0Ev:               # @_ZN15CBC_PPC_DecoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end19:
	.size	_ZN15CBC_PPC_DecoderD0Ev, .Lfunc_end19-_ZN15CBC_PPC_DecoderD0Ev
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end20-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN17CBC_SPARC_EncoderD0Ev,"axG",@progbits,_ZN17CBC_SPARC_EncoderD0Ev,comdat
	.weak	_ZN17CBC_SPARC_EncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN17CBC_SPARC_EncoderD0Ev,@function
_ZN17CBC_SPARC_EncoderD0Ev:             # @_ZN17CBC_SPARC_EncoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end21:
	.size	_ZN17CBC_SPARC_EncoderD0Ev, .Lfunc_end21-_ZN17CBC_SPARC_EncoderD0Ev
	.cfi_endproc

	.section	.text._ZN17CBC_SPARC_DecoderD0Ev,"axG",@progbits,_ZN17CBC_SPARC_DecoderD0Ev,comdat
	.weak	_ZN17CBC_SPARC_DecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN17CBC_SPARC_DecoderD0Ev,@function
_ZN17CBC_SPARC_DecoderD0Ev:             # @_ZN17CBC_SPARC_DecoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end22:
	.size	_ZN17CBC_SPARC_DecoderD0Ev, .Lfunc_end22-_ZN17CBC_SPARC_DecoderD0Ev
	.cfi_endproc

	.section	.text._ZN16CBC_IA64_EncoderD0Ev,"axG",@progbits,_ZN16CBC_IA64_EncoderD0Ev,comdat
	.weak	_ZN16CBC_IA64_EncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN16CBC_IA64_EncoderD0Ev,@function
_ZN16CBC_IA64_EncoderD0Ev:              # @_ZN16CBC_IA64_EncoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end23:
	.size	_ZN16CBC_IA64_EncoderD0Ev, .Lfunc_end23-_ZN16CBC_IA64_EncoderD0Ev
	.cfi_endproc

	.section	.text._ZN16CBC_IA64_DecoderD0Ev,"axG",@progbits,_ZN16CBC_IA64_DecoderD0Ev,comdat
	.weak	_ZN16CBC_IA64_DecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN16CBC_IA64_DecoderD0Ev,@function
_ZN16CBC_IA64_DecoderD0Ev:              # @_ZN16CBC_IA64_DecoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end24:
	.size	_ZN16CBC_IA64_DecoderD0Ev, .Lfunc_end24-_ZN16CBC_IA64_DecoderD0Ev
	.cfi_endproc

	.type	_ZTV15CBC_ARM_Encoder,@object # @_ZTV15CBC_ARM_Encoder
	.section	.rodata,"a",@progbits
	.globl	_ZTV15CBC_ARM_Encoder
	.p2align	3
_ZTV15CBC_ARM_Encoder:
	.quad	0
	.quad	_ZTI15CBC_ARM_Encoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN15CBC_ARM_EncoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN15CBC_ARM_Encoder9SubFilterEPhj
	.size	_ZTV15CBC_ARM_Encoder, 88

	.type	_ZTS15CBC_ARM_Encoder,@object # @_ZTS15CBC_ARM_Encoder
	.globl	_ZTS15CBC_ARM_Encoder
	.p2align	4
_ZTS15CBC_ARM_Encoder:
	.asciz	"15CBC_ARM_Encoder"
	.size	_ZTS15CBC_ARM_Encoder, 18

	.type	_ZTI15CBC_ARM_Encoder,@object # @_ZTI15CBC_ARM_Encoder
	.globl	_ZTI15CBC_ARM_Encoder
	.p2align	4
_ZTI15CBC_ARM_Encoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15CBC_ARM_Encoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI15CBC_ARM_Encoder, 24

	.type	_ZTV15CBC_ARM_Decoder,@object # @_ZTV15CBC_ARM_Decoder
	.globl	_ZTV15CBC_ARM_Decoder
	.p2align	3
_ZTV15CBC_ARM_Decoder:
	.quad	0
	.quad	_ZTI15CBC_ARM_Decoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN15CBC_ARM_DecoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN15CBC_ARM_Decoder9SubFilterEPhj
	.size	_ZTV15CBC_ARM_Decoder, 88

	.type	_ZTS15CBC_ARM_Decoder,@object # @_ZTS15CBC_ARM_Decoder
	.globl	_ZTS15CBC_ARM_Decoder
	.p2align	4
_ZTS15CBC_ARM_Decoder:
	.asciz	"15CBC_ARM_Decoder"
	.size	_ZTS15CBC_ARM_Decoder, 18

	.type	_ZTI15CBC_ARM_Decoder,@object # @_ZTI15CBC_ARM_Decoder
	.globl	_ZTI15CBC_ARM_Decoder
	.p2align	4
_ZTI15CBC_ARM_Decoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15CBC_ARM_Decoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI15CBC_ARM_Decoder, 24

	.type	_ZTV16CBC_ARMT_Encoder,@object # @_ZTV16CBC_ARMT_Encoder
	.globl	_ZTV16CBC_ARMT_Encoder
	.p2align	3
_ZTV16CBC_ARMT_Encoder:
	.quad	0
	.quad	_ZTI16CBC_ARMT_Encoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN16CBC_ARMT_EncoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN16CBC_ARMT_Encoder9SubFilterEPhj
	.size	_ZTV16CBC_ARMT_Encoder, 88

	.type	_ZTS16CBC_ARMT_Encoder,@object # @_ZTS16CBC_ARMT_Encoder
	.globl	_ZTS16CBC_ARMT_Encoder
	.p2align	4
_ZTS16CBC_ARMT_Encoder:
	.asciz	"16CBC_ARMT_Encoder"
	.size	_ZTS16CBC_ARMT_Encoder, 19

	.type	_ZTI16CBC_ARMT_Encoder,@object # @_ZTI16CBC_ARMT_Encoder
	.globl	_ZTI16CBC_ARMT_Encoder
	.p2align	4
_ZTI16CBC_ARMT_Encoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16CBC_ARMT_Encoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI16CBC_ARMT_Encoder, 24

	.type	_ZTV16CBC_ARMT_Decoder,@object # @_ZTV16CBC_ARMT_Decoder
	.globl	_ZTV16CBC_ARMT_Decoder
	.p2align	3
_ZTV16CBC_ARMT_Decoder:
	.quad	0
	.quad	_ZTI16CBC_ARMT_Decoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN16CBC_ARMT_DecoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN16CBC_ARMT_Decoder9SubFilterEPhj
	.size	_ZTV16CBC_ARMT_Decoder, 88

	.type	_ZTS16CBC_ARMT_Decoder,@object # @_ZTS16CBC_ARMT_Decoder
	.globl	_ZTS16CBC_ARMT_Decoder
	.p2align	4
_ZTS16CBC_ARMT_Decoder:
	.asciz	"16CBC_ARMT_Decoder"
	.size	_ZTS16CBC_ARMT_Decoder, 19

	.type	_ZTI16CBC_ARMT_Decoder,@object # @_ZTI16CBC_ARMT_Decoder
	.globl	_ZTI16CBC_ARMT_Decoder
	.p2align	4
_ZTI16CBC_ARMT_Decoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16CBC_ARMT_Decoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI16CBC_ARMT_Decoder, 24

	.type	_ZTV15CBC_PPC_Encoder,@object # @_ZTV15CBC_PPC_Encoder
	.globl	_ZTV15CBC_PPC_Encoder
	.p2align	3
_ZTV15CBC_PPC_Encoder:
	.quad	0
	.quad	_ZTI15CBC_PPC_Encoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN15CBC_PPC_EncoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN15CBC_PPC_Encoder9SubFilterEPhj
	.size	_ZTV15CBC_PPC_Encoder, 88

	.type	_ZTS15CBC_PPC_Encoder,@object # @_ZTS15CBC_PPC_Encoder
	.globl	_ZTS15CBC_PPC_Encoder
	.p2align	4
_ZTS15CBC_PPC_Encoder:
	.asciz	"15CBC_PPC_Encoder"
	.size	_ZTS15CBC_PPC_Encoder, 18

	.type	_ZTI15CBC_PPC_Encoder,@object # @_ZTI15CBC_PPC_Encoder
	.globl	_ZTI15CBC_PPC_Encoder
	.p2align	4
_ZTI15CBC_PPC_Encoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15CBC_PPC_Encoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI15CBC_PPC_Encoder, 24

	.type	_ZTV15CBC_PPC_Decoder,@object # @_ZTV15CBC_PPC_Decoder
	.globl	_ZTV15CBC_PPC_Decoder
	.p2align	3
_ZTV15CBC_PPC_Decoder:
	.quad	0
	.quad	_ZTI15CBC_PPC_Decoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN15CBC_PPC_DecoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN15CBC_PPC_Decoder9SubFilterEPhj
	.size	_ZTV15CBC_PPC_Decoder, 88

	.type	_ZTS15CBC_PPC_Decoder,@object # @_ZTS15CBC_PPC_Decoder
	.globl	_ZTS15CBC_PPC_Decoder
	.p2align	4
_ZTS15CBC_PPC_Decoder:
	.asciz	"15CBC_PPC_Decoder"
	.size	_ZTS15CBC_PPC_Decoder, 18

	.type	_ZTI15CBC_PPC_Decoder,@object # @_ZTI15CBC_PPC_Decoder
	.globl	_ZTI15CBC_PPC_Decoder
	.p2align	4
_ZTI15CBC_PPC_Decoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15CBC_PPC_Decoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI15CBC_PPC_Decoder, 24

	.type	_ZTV17CBC_SPARC_Encoder,@object # @_ZTV17CBC_SPARC_Encoder
	.globl	_ZTV17CBC_SPARC_Encoder
	.p2align	3
_ZTV17CBC_SPARC_Encoder:
	.quad	0
	.quad	_ZTI17CBC_SPARC_Encoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN17CBC_SPARC_EncoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN17CBC_SPARC_Encoder9SubFilterEPhj
	.size	_ZTV17CBC_SPARC_Encoder, 88

	.type	_ZTS17CBC_SPARC_Encoder,@object # @_ZTS17CBC_SPARC_Encoder
	.globl	_ZTS17CBC_SPARC_Encoder
	.p2align	4
_ZTS17CBC_SPARC_Encoder:
	.asciz	"17CBC_SPARC_Encoder"
	.size	_ZTS17CBC_SPARC_Encoder, 20

	.type	_ZTI17CBC_SPARC_Encoder,@object # @_ZTI17CBC_SPARC_Encoder
	.globl	_ZTI17CBC_SPARC_Encoder
	.p2align	4
_ZTI17CBC_SPARC_Encoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS17CBC_SPARC_Encoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI17CBC_SPARC_Encoder, 24

	.type	_ZTV17CBC_SPARC_Decoder,@object # @_ZTV17CBC_SPARC_Decoder
	.globl	_ZTV17CBC_SPARC_Decoder
	.p2align	3
_ZTV17CBC_SPARC_Decoder:
	.quad	0
	.quad	_ZTI17CBC_SPARC_Decoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN17CBC_SPARC_DecoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN17CBC_SPARC_Decoder9SubFilterEPhj
	.size	_ZTV17CBC_SPARC_Decoder, 88

	.type	_ZTS17CBC_SPARC_Decoder,@object # @_ZTS17CBC_SPARC_Decoder
	.globl	_ZTS17CBC_SPARC_Decoder
	.p2align	4
_ZTS17CBC_SPARC_Decoder:
	.asciz	"17CBC_SPARC_Decoder"
	.size	_ZTS17CBC_SPARC_Decoder, 20

	.type	_ZTI17CBC_SPARC_Decoder,@object # @_ZTI17CBC_SPARC_Decoder
	.globl	_ZTI17CBC_SPARC_Decoder
	.p2align	4
_ZTI17CBC_SPARC_Decoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS17CBC_SPARC_Decoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI17CBC_SPARC_Decoder, 24

	.type	_ZTV16CBC_IA64_Encoder,@object # @_ZTV16CBC_IA64_Encoder
	.globl	_ZTV16CBC_IA64_Encoder
	.p2align	3
_ZTV16CBC_IA64_Encoder:
	.quad	0
	.quad	_ZTI16CBC_IA64_Encoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN16CBC_IA64_EncoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN16CBC_IA64_Encoder9SubFilterEPhj
	.size	_ZTV16CBC_IA64_Encoder, 88

	.type	_ZTS16CBC_IA64_Encoder,@object # @_ZTS16CBC_IA64_Encoder
	.globl	_ZTS16CBC_IA64_Encoder
	.p2align	4
_ZTS16CBC_IA64_Encoder:
	.asciz	"16CBC_IA64_Encoder"
	.size	_ZTS16CBC_IA64_Encoder, 19

	.type	_ZTI16CBC_IA64_Encoder,@object # @_ZTI16CBC_IA64_Encoder
	.globl	_ZTI16CBC_IA64_Encoder
	.p2align	4
_ZTI16CBC_IA64_Encoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16CBC_IA64_Encoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI16CBC_IA64_Encoder, 24

	.type	_ZTV16CBC_IA64_Decoder,@object # @_ZTV16CBC_IA64_Decoder
	.globl	_ZTV16CBC_IA64_Decoder
	.p2align	3
_ZTV16CBC_IA64_Decoder:
	.quad	0
	.quad	_ZTI16CBC_IA64_Decoder
	.quad	_ZN16CBranchConverter14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CBranchConverter6AddRefEv
	.quad	_ZN16CBranchConverter7ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN16CBC_IA64_DecoderD0Ev
	.quad	_ZN16CBranchConverter4InitEv
	.quad	_ZN16CBranchConverter6FilterEPhj
	.quad	_ZN16CBranchConverter7SubInitEv
	.quad	_ZN16CBC_IA64_Decoder9SubFilterEPhj
	.size	_ZTV16CBC_IA64_Decoder, 88

	.type	_ZTS16CBC_IA64_Decoder,@object # @_ZTS16CBC_IA64_Decoder
	.globl	_ZTS16CBC_IA64_Decoder
	.p2align	4
_ZTS16CBC_IA64_Decoder:
	.asciz	"16CBC_IA64_Decoder"
	.size	_ZTS16CBC_IA64_Decoder, 19

	.type	_ZTI16CBC_IA64_Decoder,@object # @_ZTI16CBC_IA64_Decoder
	.globl	_ZTI16CBC_IA64_Decoder
	.p2align	4
_ZTI16CBC_IA64_Decoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16CBC_IA64_Decoder
	.quad	_ZTI16CBranchConverter
	.size	_ZTI16CBC_IA64_Decoder, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
