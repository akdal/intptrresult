	.text
	.file	"ZipCrypto.bc"
	.globl	_ZN7NCrypto4NZip7CCipher10UpdateKeysEh
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip7CCipher10UpdateKeysEh,@function
_ZN7NCrypto4NZip7CCipher10UpdateKeysEh: # @_ZN7NCrypto4NZip7CCipher10UpdateKeysEh
	.cfi_startproc
# BB#0:
	movl	20(%rdi), %eax
	movzbl	%al, %ecx
	xorl	%esi, %ecx
	shrl	$8, %eax
	xorl	g_CrcTable(,%rcx,4), %eax
	movl	%eax, 20(%rdi)
	movzbl	%al, %eax
	addl	24(%rdi), %eax
	imull	$134775813, %eax, %eax  # imm = 0x8088405
	incl	%eax
	movl	%eax, 24(%rdi)
	movl	28(%rdi), %ecx
	shrl	$24, %eax
	movzbl	%cl, %edx
	xorl	%eax, %edx
	shrl	$8, %ecx
	xorl	g_CrcTable(,%rdx,4), %ecx
	movl	%ecx, 28(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN7NCrypto4NZip7CCipher10UpdateKeysEh, .Lfunc_end0-_ZN7NCrypto4NZip7CCipher10UpdateKeysEh
	.cfi_endproc

	.globl	_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj,@function
_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj: # @_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	movabsq	$2541551403134113400, %rax # imm = 0x2345678912345678
	movq	%rax, 20(%rdi)
	movl	$878082192, 28(%rdi)    # imm = 0x34567890
	testl	%edx, %edx
	je	.LBB1_1
# BB#2:                                 # %.lr.ph.preheader
	movl	%edx, %r8d
	movl	$305419896, %eax        # imm = 0x12345678
	movl	$591751049, %r11d       # imm = 0x23456789
	movl	$878082192, %ecx        # imm = 0x34567890
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %r9d
	movzbl	%al, %r10d
	xorl	%r9d, %r10d
	shrl	$8, %eax
	xorl	g_CrcTable(,%r10,4), %eax
	movl	%eax, 20(%rdi)
	movzbl	%al, %edx
	addl	%r11d, %edx
	imull	$134775813, %edx, %r11d # imm = 0x8088405
	incl	%r11d
	movl	%r11d, 24(%rdi)
	movl	%r11d, %edx
	shrl	$24, %edx
	movzbl	%cl, %r9d
	xorl	%edx, %r9d
	shrl	$8, %ecx
	xorl	g_CrcTable(,%r9,4), %ecx
	movl	%ecx, 28(%rdi)
	incq	%rsi
	decq	%r8
	jne	.LBB1_3
	jmp	.LBB1_4
.LBB1_1:
	movl	$305419896, %eax        # imm = 0x12345678
	movl	$591751049, %r11d       # imm = 0x23456789
	movl	$878082192, %ecx        # imm = 0x34567890
.LBB1_4:                                # %.preheader.preheader
	movl	%eax, 32(%rdi)
	movl	%r11d, 36(%rdi)
	movl	%ecx, 40(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj, .Lfunc_end1-_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZThn8_N7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj,@function
_ZThn8_N7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj: # @_ZThn8_N7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.cfi_startproc
# BB#0:
	movabsq	$2541551403134113400, %rax # imm = 0x2345678912345678
	movq	%rax, 12(%rdi)
	movl	$878082192, 20(%rdi)    # imm = 0x34567890
	testl	%edx, %edx
	je	.LBB2_1
# BB#2:                                 # %.lr.ph.preheader.i
	movl	%edx, %r8d
	movl	$305419896, %eax        # imm = 0x12345678
	movl	$591751049, %r11d       # imm = 0x23456789
	movl	$878082192, %ecx        # imm = 0x34567890
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %r9d
	movzbl	%al, %r10d
	xorl	%r9d, %r10d
	shrl	$8, %eax
	xorl	g_CrcTable(,%r10,4), %eax
	movl	%eax, 12(%rdi)
	movzbl	%al, %edx
	addl	%r11d, %edx
	imull	$134775813, %edx, %r11d # imm = 0x8088405
	incl	%r11d
	movl	%r11d, 16(%rdi)
	movl	%r11d, %edx
	shrl	$24, %edx
	movzbl	%cl, %r9d
	xorl	%edx, %r9d
	shrl	$8, %ecx
	xorl	g_CrcTable(,%r9,4), %ecx
	movl	%ecx, 20(%rdi)
	incq	%rsi
	decq	%r8
	jne	.LBB2_3
	jmp	.LBB2_4
.LBB2_1:
	movl	$305419896, %eax        # imm = 0x12345678
	movl	$591751049, %r11d       # imm = 0x23456789
	movl	$878082192, %ecx        # imm = 0x34567890
.LBB2_4:                                # %_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj.exit
	movl	%eax, 24(%rdi)
	movl	%r11d, 28(%rdi)
	movl	%ecx, 32(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZThn8_N7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj, .Lfunc_end2-_ZThn8_N7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.cfi_endproc

	.globl	_ZN7NCrypto4NZip7CCipher4InitEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip7CCipher4InitEv,@function
_ZN7NCrypto4NZip7CCipher4InitEv:        # @_ZN7NCrypto4NZip7CCipher4InitEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	_ZN7NCrypto4NZip7CCipher4InitEv, .Lfunc_end3-_ZN7NCrypto4NZip7CCipher4InitEv
	.cfi_endproc

	.globl	_ZN7NCrypto4NZip7CCipher15DecryptByteSpecEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip7CCipher15DecryptByteSpecEv,@function
_ZN7NCrypto4NZip7CCipher15DecryptByteSpecEv: # @_ZN7NCrypto4NZip7CCipher15DecryptByteSpecEv
	.cfi_startproc
# BB#0:
	movl	28(%rdi), %ecx
	orl	$2, %ecx
	movl	%ecx, %eax
	xorl	$1, %eax
	imull	%ecx, %eax
	shrl	$8, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end4:
	.size	_ZN7NCrypto4NZip7CCipher15DecryptByteSpecEv, .Lfunc_end4-_ZN7NCrypto4NZip7CCipher15DecryptByteSpecEv
	.cfi_endproc

	.globl	_ZN7NCrypto4NZip8CEncoder11WriteHeaderEP20ISequentialOutStreamj
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CEncoder11WriteHeaderEP20ISequentialOutStreamj,@function
_ZN7NCrypto4NZip8CEncoder11WriteHeaderEP20ISequentialOutStreamj: # @_ZN7NCrypto4NZip8CEncoder11WriteHeaderEP20ISequentialOutStreamj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 64
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	12(%rsp), %r15
	movl	$g_RandomGenerator, %edi
	movl	$10, %edx
	movq	%r15, %rsi
	callq	_ZN16CRandomGenerator8GenerateEPhj
	movl	%ebp, %eax
	shrl	$24, %eax
	movb	%al, 23(%rsp)
	shrl	$16, %ebp
	movb	%bpl, 22(%rsp)
	movl	32(%rbx), %eax
	movl	%eax, 20(%rbx)
	movl	36(%rbx), %eax
	movl	%eax, 24(%rbx)
	movl	40(%rbx), %eax
	movl	%eax, 28(%rbx)
	movq	(%rbx), %rax
	movl	$12, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	*48(%rax)
	movl	$12, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_Z11WriteStreamP20ISequentialOutStreamPKvm
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN7NCrypto4NZip8CEncoder11WriteHeaderEP20ISequentialOutStreamj, .Lfunc_end5-_ZN7NCrypto4NZip8CEncoder11WriteHeaderEP20ISequentialOutStreamj
	.cfi_endproc

	.globl	_ZN7NCrypto4NZip8CEncoder6FilterEPhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CEncoder6FilterEPhj,@function
_ZN7NCrypto4NZip8CEncoder6FilterEPhj:   # @_ZN7NCrypto4NZip8CEncoder6FilterEPhj
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	je	.LBB6_3
# BB#1:                                 # %.lr.ph
	movl	28(%rdi), %ecx
	movl	%edx, %r8d
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	orl	$2, %ecx
	movl	%ecx, %eax
	xorl	$1, %eax
	imull	%ecx, %eax
	shrl	$8, %eax
	movzbl	(%rsi), %r9d
	xorl	%r9d, %eax
	movb	%al, (%rsi)
	movl	20(%rdi), %eax
	movzbl	%al, %ecx
	xorl	%r9d, %ecx
	shrl	$8, %eax
	xorl	g_CrcTable(,%rcx,4), %eax
	movl	%eax, 20(%rdi)
	movzbl	%al, %eax
	addl	24(%rdi), %eax
	imull	$134775813, %eax, %eax  # imm = 0x8088405
	incl	%eax
	movl	%eax, 24(%rdi)
	movl	28(%rdi), %ecx
	shrl	$24, %eax
	movzbl	%cl, %r9d
	xorl	%eax, %r9d
	shrl	$8, %ecx
	xorl	g_CrcTable(,%r9,4), %ecx
	movl	%ecx, 28(%rdi)
	incq	%rsi
	decq	%r8
	jne	.LBB6_2
.LBB6_3:                                # %._crit_edge
	movl	%edx, %eax
	retq
.Lfunc_end6:
	.size	_ZN7NCrypto4NZip8CEncoder6FilterEPhj, .Lfunc_end6-_ZN7NCrypto4NZip8CEncoder6FilterEPhj
	.cfi_endproc

	.globl	_ZN7NCrypto4NZip8CDecoder10ReadHeaderEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CDecoder10ReadHeaderEP19ISequentialInStream,@function
_ZN7NCrypto4NZip8CDecoder10ReadHeaderEP19ISequentialInStream: # @_ZN7NCrypto4NZip8CDecoder10ReadHeaderEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	4(%rsp), %rax
	movl	$12, %edx
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	_Z15ReadStream_FAILP19ISequentialInStreamPvm
	testl	%eax, %eax
	jne	.LBB7_2
# BB#1:
	movl	32(%rbx), %eax
	movl	%eax, 20(%rbx)
	movl	36(%rbx), %eax
	movl	%eax, 24(%rbx)
	movl	40(%rbx), %eax
	movl	%eax, 28(%rbx)
	movq	(%rbx), %rax
	leaq	4(%rsp), %rsi
	movl	$12, %edx
	movq	%rbx, %rdi
	callq	*48(%rax)
	xorl	%eax, %eax
.LBB7_2:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end7:
	.size	_ZN7NCrypto4NZip8CDecoder10ReadHeaderEP19ISequentialInStream, .Lfunc_end7-_ZN7NCrypto4NZip8CDecoder10ReadHeaderEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN7NCrypto4NZip8CDecoder6FilterEPhj
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CDecoder6FilterEPhj,@function
_ZN7NCrypto4NZip8CDecoder6FilterEPhj:   # @_ZN7NCrypto4NZip8CDecoder6FilterEPhj
	.cfi_startproc
# BB#0:
	testl	%edx, %edx
	je	.LBB8_3
# BB#1:                                 # %.lr.ph
	movl	%edx, %r8d
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movl	20(%rdi), %r10d
	movl	28(%rdi), %r9d
	movl	%r9d, %eax
	orl	$2, %eax
	movl	%eax, %ecx
	xorl	$1, %ecx
	imull	%eax, %ecx
	shrl	$8, %ecx
	xorb	(%rsi), %cl
	movzbl	%cl, %r11d
	movzbl	%r10b, %ecx
	xorl	%r11d, %ecx
	shrl	$8, %r10d
	xorl	g_CrcTable(,%rcx,4), %r10d
	movl	%r10d, 20(%rdi)
	movzbl	%r10b, %ecx
	addl	24(%rdi), %ecx
	imull	$134775813, %ecx, %ecx  # imm = 0x8088405
	incl	%ecx
	movl	%ecx, 24(%rdi)
	shrl	$24, %ecx
	movzbl	%r9b, %eax
	xorl	%ecx, %eax
	shrl	$8, %r9d
	xorl	g_CrcTable(,%rax,4), %r9d
	movl	%r9d, 28(%rdi)
	movb	%r11b, (%rsi)
	incq	%rsi
	decq	%r8
	jne	.LBB8_2
.LBB8_3:                                # %._crit_edge
	movl	%edx, %eax
	retq
.Lfunc_end8:
	.size	_ZN7NCrypto4NZip8CDecoder6FilterEPhj, .Lfunc_end8-_ZN7NCrypto4NZip8CDecoder6FilterEPhj
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip7CCipherD0Ev,"axG",@progbits,_ZN7NCrypto4NZip7CCipherD0Ev,comdat
	.weak	_ZN7NCrypto4NZip7CCipherD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip7CCipherD0Ev,@function
_ZN7NCrypto4NZip7CCipherD0Ev:           # @_ZN7NCrypto4NZip7CCipherD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end9:
	.size	_ZN7NCrypto4NZip7CCipherD0Ev, .Lfunc_end9-_ZN7NCrypto4NZip7CCipherD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip7CCipherD1Ev,"axG",@progbits,_ZThn8_N7NCrypto4NZip7CCipherD1Ev,comdat
	.weak	_ZThn8_N7NCrypto4NZip7CCipherD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip7CCipherD1Ev,@function
_ZThn8_N7NCrypto4NZip7CCipherD1Ev:      # @_ZThn8_N7NCrypto4NZip7CCipherD1Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZThn8_N7NCrypto4NZip7CCipherD1Ev, .Lfunc_end10-_ZThn8_N7NCrypto4NZip7CCipherD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip7CCipherD0Ev,"axG",@progbits,_ZThn8_N7NCrypto4NZip7CCipherD0Ev,comdat
	.weak	_ZThn8_N7NCrypto4NZip7CCipherD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip7CCipherD0Ev,@function
_ZThn8_N7NCrypto4NZip7CCipherD0Ev:      # @_ZThn8_N7NCrypto4NZip7CCipherD0Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end11:
	.size	_ZThn8_N7NCrypto4NZip7CCipherD0Ev, .Lfunc_end11-_ZThn8_N7NCrypto4NZip7CCipherD0Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB12_3
# BB#1:
	movl	$IID_ICryptoSetPassword, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB12_2
.LBB12_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB12_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB12_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB12_4
.Lfunc_end12:
	.size	_ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end12-_ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip8CEncoder6AddRefEv,"axG",@progbits,_ZN7NCrypto4NZip8CEncoder6AddRefEv,comdat
	.weak	_ZN7NCrypto4NZip8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CEncoder6AddRefEv,@function
_ZN7NCrypto4NZip8CEncoder6AddRefEv:     # @_ZN7NCrypto4NZip8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end13:
	.size	_ZN7NCrypto4NZip8CEncoder6AddRefEv, .Lfunc_end13-_ZN7NCrypto4NZip8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip8CEncoder7ReleaseEv,"axG",@progbits,_ZN7NCrypto4NZip8CEncoder7ReleaseEv,comdat
	.weak	_ZN7NCrypto4NZip8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CEncoder7ReleaseEv,@function
_ZN7NCrypto4NZip8CEncoder7ReleaseEv:    # @_ZN7NCrypto4NZip8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB14_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB14_2:
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZN7NCrypto4NZip8CEncoder7ReleaseEv, .Lfunc_end14-_ZN7NCrypto4NZip8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip7CCipherD2Ev,"axG",@progbits,_ZN7NCrypto4NZip7CCipherD2Ev,comdat
	.weak	_ZN7NCrypto4NZip7CCipherD2Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip7CCipherD2Ev,@function
_ZN7NCrypto4NZip7CCipherD2Ev:           # @_ZN7NCrypto4NZip7CCipherD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end15:
	.size	_ZN7NCrypto4NZip7CCipherD2Ev, .Lfunc_end15-_ZN7NCrypto4NZip7CCipherD2Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip8CEncoderD0Ev,"axG",@progbits,_ZN7NCrypto4NZip8CEncoderD0Ev,comdat
	.weak	_ZN7NCrypto4NZip8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CEncoderD0Ev,@function
_ZN7NCrypto4NZip8CEncoderD0Ev:          # @_ZN7NCrypto4NZip8CEncoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end16:
	.size	_ZN7NCrypto4NZip8CEncoderD0Ev, .Lfunc_end16-_ZN7NCrypto4NZip8CEncoderD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 16
	movq	%rdi, %r8
	leaq	-8(%r8), %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB17_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB17_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB17_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB17_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB17_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB17_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB17_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB17_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB17_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB17_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB17_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB17_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB17_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB17_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB17_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB17_32
.LBB17_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICryptoSetPassword(%rip), %cl
	jne	.LBB17_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+1(%rip), %cl
	jne	.LBB17_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+2(%rip), %cl
	jne	.LBB17_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+3(%rip), %cl
	jne	.LBB17_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+4(%rip), %cl
	jne	.LBB17_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+5(%rip), %cl
	jne	.LBB17_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+6(%rip), %cl
	jne	.LBB17_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+7(%rip), %cl
	jne	.LBB17_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+8(%rip), %cl
	jne	.LBB17_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+9(%rip), %cl
	jne	.LBB17_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+10(%rip), %cl
	jne	.LBB17_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+11(%rip), %cl
	jne	.LBB17_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+12(%rip), %cl
	jne	.LBB17_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+13(%rip), %cl
	jne	.LBB17_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+14(%rip), %cl
	jne	.LBB17_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+15(%rip), %cl
	jne	.LBB17_33
.LBB17_32:
	movq	%r8, (%rdx)
	movq	-8(%r8), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB17_33:                              # %_ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end17:
	.size	_ZThn8_N7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end17-_ZThn8_N7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CEncoder6AddRefEv,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CEncoder6AddRefEv,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CEncoder6AddRefEv,@function
_ZThn8_N7NCrypto4NZip8CEncoder6AddRefEv: # @_ZThn8_N7NCrypto4NZip8CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end18:
	.size	_ZThn8_N7NCrypto4NZip8CEncoder6AddRefEv, .Lfunc_end18-_ZThn8_N7NCrypto4NZip8CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CEncoder7ReleaseEv,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CEncoder7ReleaseEv,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CEncoder7ReleaseEv,@function
_ZThn8_N7NCrypto4NZip8CEncoder7ReleaseEv: # @_ZThn8_N7NCrypto4NZip8CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB19_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB19_2:                               # %_ZN7NCrypto4NZip8CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZThn8_N7NCrypto4NZip8CEncoder7ReleaseEv, .Lfunc_end19-_ZThn8_N7NCrypto4NZip8CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CEncoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CEncoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CEncoderD1Ev,@function
_ZThn8_N7NCrypto4NZip8CEncoderD1Ev:     # @_ZThn8_N7NCrypto4NZip8CEncoderD1Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	_ZThn8_N7NCrypto4NZip8CEncoderD1Ev, .Lfunc_end20-_ZThn8_N7NCrypto4NZip8CEncoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CEncoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CEncoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CEncoderD0Ev,@function
_ZThn8_N7NCrypto4NZip8CEncoderD0Ev:     # @_ZThn8_N7NCrypto4NZip8CEncoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end21:
	.size	_ZThn8_N7NCrypto4NZip8CEncoderD0Ev, .Lfunc_end21-_ZThn8_N7NCrypto4NZip8CEncoderD0Ev
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB22_3
# BB#1:
	movl	$IID_ICryptoSetPassword, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB22_2
.LBB22_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB22_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB22_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB22_4
.Lfunc_end22:
	.size	_ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end22-_ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip8CDecoder6AddRefEv,"axG",@progbits,_ZN7NCrypto4NZip8CDecoder6AddRefEv,comdat
	.weak	_ZN7NCrypto4NZip8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CDecoder6AddRefEv,@function
_ZN7NCrypto4NZip8CDecoder6AddRefEv:     # @_ZN7NCrypto4NZip8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end23:
	.size	_ZN7NCrypto4NZip8CDecoder6AddRefEv, .Lfunc_end23-_ZN7NCrypto4NZip8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip8CDecoder7ReleaseEv,"axG",@progbits,_ZN7NCrypto4NZip8CDecoder7ReleaseEv,comdat
	.weak	_ZN7NCrypto4NZip8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CDecoder7ReleaseEv,@function
_ZN7NCrypto4NZip8CDecoder7ReleaseEv:    # @_ZN7NCrypto4NZip8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB24_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB24_2:
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZN7NCrypto4NZip8CDecoder7ReleaseEv, .Lfunc_end24-_ZN7NCrypto4NZip8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN7NCrypto4NZip8CDecoderD0Ev,"axG",@progbits,_ZN7NCrypto4NZip8CDecoderD0Ev,comdat
	.weak	_ZN7NCrypto4NZip8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN7NCrypto4NZip8CDecoderD0Ev,@function
_ZN7NCrypto4NZip8CDecoderD0Ev:          # @_ZN7NCrypto4NZip8CDecoderD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end25:
	.size	_ZN7NCrypto4NZip8CDecoderD0Ev, .Lfunc_end25-_ZN7NCrypto4NZip8CDecoderD0Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 16
	movq	%rdi, %r8
	leaq	-8(%r8), %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB26_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB26_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB26_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB26_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB26_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB26_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB26_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB26_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB26_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB26_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB26_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB26_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB26_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB26_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB26_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB26_32
.LBB26_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICryptoSetPassword(%rip), %cl
	jne	.LBB26_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+1(%rip), %cl
	jne	.LBB26_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+2(%rip), %cl
	jne	.LBB26_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+3(%rip), %cl
	jne	.LBB26_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+4(%rip), %cl
	jne	.LBB26_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+5(%rip), %cl
	jne	.LBB26_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+6(%rip), %cl
	jne	.LBB26_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+7(%rip), %cl
	jne	.LBB26_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+8(%rip), %cl
	jne	.LBB26_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+9(%rip), %cl
	jne	.LBB26_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+10(%rip), %cl
	jne	.LBB26_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+11(%rip), %cl
	jne	.LBB26_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+12(%rip), %cl
	jne	.LBB26_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+13(%rip), %cl
	jne	.LBB26_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+14(%rip), %cl
	jne	.LBB26_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICryptoSetPassword+15(%rip), %cl
	jne	.LBB26_33
.LBB26_32:
	movq	%r8, (%rdx)
	movq	-8(%r8), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB26_33:                              # %_ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZThn8_N7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end26-_ZThn8_N7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CDecoder6AddRefEv,@function
_ZThn8_N7NCrypto4NZip8CDecoder6AddRefEv: # @_ZThn8_N7NCrypto4NZip8CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end27:
	.size	_ZThn8_N7NCrypto4NZip8CDecoder6AddRefEv, .Lfunc_end27-_ZThn8_N7NCrypto4NZip8CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CDecoder7ReleaseEv,@function
_ZThn8_N7NCrypto4NZip8CDecoder7ReleaseEv: # @_ZThn8_N7NCrypto4NZip8CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB28_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB28_2:                               # %_ZN7NCrypto4NZip8CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end28:
	.size	_ZThn8_N7NCrypto4NZip8CDecoder7ReleaseEv, .Lfunc_end28-_ZThn8_N7NCrypto4NZip8CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CDecoderD1Ev,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CDecoderD1Ev,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CDecoderD1Ev,@function
_ZThn8_N7NCrypto4NZip8CDecoderD1Ev:     # @_ZThn8_N7NCrypto4NZip8CDecoderD1Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end29:
	.size	_ZThn8_N7NCrypto4NZip8CDecoderD1Ev, .Lfunc_end29-_ZThn8_N7NCrypto4NZip8CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N7NCrypto4NZip8CDecoderD0Ev,"axG",@progbits,_ZThn8_N7NCrypto4NZip8CDecoderD0Ev,comdat
	.weak	_ZThn8_N7NCrypto4NZip8CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N7NCrypto4NZip8CDecoderD0Ev,@function
_ZThn8_N7NCrypto4NZip8CDecoderD0Ev:     # @_ZThn8_N7NCrypto4NZip8CDecoderD0Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end30:
	.size	_ZThn8_N7NCrypto4NZip8CDecoderD0Ev, .Lfunc_end30-_ZThn8_N7NCrypto4NZip8CDecoderD0Ev
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB31_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB31_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB31_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB31_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB31_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB31_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB31_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB31_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB31_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB31_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB31_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB31_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB31_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB31_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB31_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB31_16:
	xorl	%eax, %eax
	retq
.Lfunc_end31:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end31-_ZeqRK4GUIDS1_
	.cfi_endproc

	.type	_ZTVN7NCrypto4NZip7CCipherE,@object # @_ZTVN7NCrypto4NZip7CCipherE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN7NCrypto4NZip7CCipherE
	.p2align	3
_ZTVN7NCrypto4NZip7CCipherE:
	.quad	0
	.quad	_ZTIN7NCrypto4NZip7CCipherE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN7NCrypto4NZip7CCipherD2Ev
	.quad	_ZN7NCrypto4NZip7CCipherD0Ev
	.quad	_ZN7NCrypto4NZip7CCipher4InitEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto4NZip7CCipherE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZThn8_N7NCrypto4NZip7CCipherD1Ev
	.quad	_ZThn8_N7NCrypto4NZip7CCipherD0Ev
	.quad	_ZThn8_N7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.size	_ZTVN7NCrypto4NZip7CCipherE, 144

	.type	_ZTSN7NCrypto4NZip7CCipherE,@object # @_ZTSN7NCrypto4NZip7CCipherE
	.globl	_ZTSN7NCrypto4NZip7CCipherE
	.p2align	4
_ZTSN7NCrypto4NZip7CCipherE:
	.asciz	"N7NCrypto4NZip7CCipherE"
	.size	_ZTSN7NCrypto4NZip7CCipherE, 24

	.type	_ZTS15ICompressFilter,@object # @_ZTS15ICompressFilter
	.section	.rodata._ZTS15ICompressFilter,"aG",@progbits,_ZTS15ICompressFilter,comdat
	.weak	_ZTS15ICompressFilter
	.p2align	4
_ZTS15ICompressFilter:
	.asciz	"15ICompressFilter"
	.size	_ZTS15ICompressFilter, 18

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI15ICompressFilter,@object # @_ZTI15ICompressFilter
	.section	.rodata._ZTI15ICompressFilter,"aG",@progbits,_ZTI15ICompressFilter,comdat
	.weak	_ZTI15ICompressFilter
	.p2align	4
_ZTI15ICompressFilter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15ICompressFilter
	.quad	_ZTI8IUnknown
	.size	_ZTI15ICompressFilter, 24

	.type	_ZTS18ICryptoSetPassword,@object # @_ZTS18ICryptoSetPassword
	.section	.rodata._ZTS18ICryptoSetPassword,"aG",@progbits,_ZTS18ICryptoSetPassword,comdat
	.weak	_ZTS18ICryptoSetPassword
	.p2align	4
_ZTS18ICryptoSetPassword:
	.asciz	"18ICryptoSetPassword"
	.size	_ZTS18ICryptoSetPassword, 21

	.type	_ZTI18ICryptoSetPassword,@object # @_ZTI18ICryptoSetPassword
	.section	.rodata._ZTI18ICryptoSetPassword,"aG",@progbits,_ZTI18ICryptoSetPassword,comdat
	.weak	_ZTI18ICryptoSetPassword
	.p2align	4
_ZTI18ICryptoSetPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18ICryptoSetPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI18ICryptoSetPassword, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN7NCrypto4NZip7CCipherE,@object # @_ZTIN7NCrypto4NZip7CCipherE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN7NCrypto4NZip7CCipherE
	.p2align	4
_ZTIN7NCrypto4NZip7CCipherE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN7NCrypto4NZip7CCipherE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI15ICompressFilter
	.quad	2                       # 0x2
	.quad	_ZTI18ICryptoSetPassword
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN7NCrypto4NZip7CCipherE, 72

	.type	_ZTVN7NCrypto4NZip8CEncoderE,@object # @_ZTVN7NCrypto4NZip8CEncoderE
	.globl	_ZTVN7NCrypto4NZip8CEncoderE
	.p2align	3
_ZTVN7NCrypto4NZip8CEncoderE:
	.quad	0
	.quad	_ZTIN7NCrypto4NZip8CEncoderE
	.quad	_ZN7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN7NCrypto4NZip8CEncoder6AddRefEv
	.quad	_ZN7NCrypto4NZip8CEncoder7ReleaseEv
	.quad	_ZN7NCrypto4NZip7CCipherD2Ev
	.quad	_ZN7NCrypto4NZip8CEncoderD0Ev
	.quad	_ZN7NCrypto4NZip7CCipher4InitEv
	.quad	_ZN7NCrypto4NZip8CEncoder6FilterEPhj
	.quad	_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto4NZip8CEncoderE
	.quad	_ZThn8_N7NCrypto4NZip8CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N7NCrypto4NZip8CEncoder6AddRefEv
	.quad	_ZThn8_N7NCrypto4NZip8CEncoder7ReleaseEv
	.quad	_ZThn8_N7NCrypto4NZip8CEncoderD1Ev
	.quad	_ZThn8_N7NCrypto4NZip8CEncoderD0Ev
	.quad	_ZThn8_N7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.size	_ZTVN7NCrypto4NZip8CEncoderE, 144

	.type	_ZTSN7NCrypto4NZip8CEncoderE,@object # @_ZTSN7NCrypto4NZip8CEncoderE
	.globl	_ZTSN7NCrypto4NZip8CEncoderE
	.p2align	4
_ZTSN7NCrypto4NZip8CEncoderE:
	.asciz	"N7NCrypto4NZip8CEncoderE"
	.size	_ZTSN7NCrypto4NZip8CEncoderE, 25

	.type	_ZTIN7NCrypto4NZip8CEncoderE,@object # @_ZTIN7NCrypto4NZip8CEncoderE
	.globl	_ZTIN7NCrypto4NZip8CEncoderE
	.p2align	4
_ZTIN7NCrypto4NZip8CEncoderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7NCrypto4NZip8CEncoderE
	.quad	_ZTIN7NCrypto4NZip7CCipherE
	.size	_ZTIN7NCrypto4NZip8CEncoderE, 24

	.type	_ZTVN7NCrypto4NZip8CDecoderE,@object # @_ZTVN7NCrypto4NZip8CDecoderE
	.globl	_ZTVN7NCrypto4NZip8CDecoderE
	.p2align	3
_ZTVN7NCrypto4NZip8CDecoderE:
	.quad	0
	.quad	_ZTIN7NCrypto4NZip8CDecoderE
	.quad	_ZN7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN7NCrypto4NZip8CDecoder6AddRefEv
	.quad	_ZN7NCrypto4NZip8CDecoder7ReleaseEv
	.quad	_ZN7NCrypto4NZip7CCipherD2Ev
	.quad	_ZN7NCrypto4NZip8CDecoderD0Ev
	.quad	_ZN7NCrypto4NZip7CCipher4InitEv
	.quad	_ZN7NCrypto4NZip8CDecoder6FilterEPhj
	.quad	_ZN7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.quad	-8
	.quad	_ZTIN7NCrypto4NZip8CDecoderE
	.quad	_ZThn8_N7NCrypto4NZip8CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N7NCrypto4NZip8CDecoder6AddRefEv
	.quad	_ZThn8_N7NCrypto4NZip8CDecoder7ReleaseEv
	.quad	_ZThn8_N7NCrypto4NZip8CDecoderD1Ev
	.quad	_ZThn8_N7NCrypto4NZip8CDecoderD0Ev
	.quad	_ZThn8_N7NCrypto4NZip7CCipher17CryptoSetPasswordEPKhj
	.size	_ZTVN7NCrypto4NZip8CDecoderE, 144

	.type	_ZTSN7NCrypto4NZip8CDecoderE,@object # @_ZTSN7NCrypto4NZip8CDecoderE
	.globl	_ZTSN7NCrypto4NZip8CDecoderE
	.p2align	4
_ZTSN7NCrypto4NZip8CDecoderE:
	.asciz	"N7NCrypto4NZip8CDecoderE"
	.size	_ZTSN7NCrypto4NZip8CDecoderE, 25

	.type	_ZTIN7NCrypto4NZip8CDecoderE,@object # @_ZTIN7NCrypto4NZip8CDecoderE
	.globl	_ZTIN7NCrypto4NZip8CDecoderE
	.p2align	4
_ZTIN7NCrypto4NZip8CDecoderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN7NCrypto4NZip8CDecoderE
	.quad	_ZTIN7NCrypto4NZip7CCipherE
	.size	_ZTIN7NCrypto4NZip8CDecoderE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
