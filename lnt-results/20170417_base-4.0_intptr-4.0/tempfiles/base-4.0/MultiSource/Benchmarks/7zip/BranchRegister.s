	.text
	.file	"BranchRegister.bc"
	.p2align	4, 0x90
	.type	_ZL17CreateCodecBC_PPCv,@function
_ZL17CreateCodecBC_PPCv:                # @_ZL17CreateCodecBC_PPCv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV15CBC_PPC_Decoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end0:
	.size	_ZL17CreateCodecBC_PPCv, .Lfunc_end0-_ZL17CreateCodecBC_PPCv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL20CreateCodecBC_PPCOutv,@function
_ZL20CreateCodecBC_PPCOutv:             # @_ZL20CreateCodecBC_PPCOutv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV15CBC_PPC_Encoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end1:
	.size	_ZL20CreateCodecBC_PPCOutv, .Lfunc_end1-_ZL20CreateCodecBC_PPCOutv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL18CreateCodecBC_IA64v,@function
_ZL18CreateCodecBC_IA64v:               # @_ZL18CreateCodecBC_IA64v
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV16CBC_IA64_Decoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end2:
	.size	_ZL18CreateCodecBC_IA64v, .Lfunc_end2-_ZL18CreateCodecBC_IA64v
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL21CreateCodecBC_IA64Outv,@function
_ZL21CreateCodecBC_IA64Outv:            # @_ZL21CreateCodecBC_IA64Outv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV16CBC_IA64_Encoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end3:
	.size	_ZL21CreateCodecBC_IA64Outv, .Lfunc_end3-_ZL21CreateCodecBC_IA64Outv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL17CreateCodecBC_ARMv,@function
_ZL17CreateCodecBC_ARMv:                # @_ZL17CreateCodecBC_ARMv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV15CBC_ARM_Decoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZL17CreateCodecBC_ARMv, .Lfunc_end4-_ZL17CreateCodecBC_ARMv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL20CreateCodecBC_ARMOutv,@function
_ZL20CreateCodecBC_ARMOutv:             # @_ZL20CreateCodecBC_ARMOutv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV15CBC_ARM_Encoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end5:
	.size	_ZL20CreateCodecBC_ARMOutv, .Lfunc_end5-_ZL20CreateCodecBC_ARMOutv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL18CreateCodecBC_ARMTv,@function
_ZL18CreateCodecBC_ARMTv:               # @_ZL18CreateCodecBC_ARMTv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV16CBC_ARMT_Decoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end6:
	.size	_ZL18CreateCodecBC_ARMTv, .Lfunc_end6-_ZL18CreateCodecBC_ARMTv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL21CreateCodecBC_ARMTOutv,@function
_ZL21CreateCodecBC_ARMTOutv:            # @_ZL21CreateCodecBC_ARMTOutv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV16CBC_ARMT_Encoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end7:
	.size	_ZL21CreateCodecBC_ARMTOutv, .Lfunc_end7-_ZL21CreateCodecBC_ARMTOutv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL19CreateCodecBC_SPARCv,@function
_ZL19CreateCodecBC_SPARCv:              # @_ZL19CreateCodecBC_SPARCv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV17CBC_SPARC_Decoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end8:
	.size	_ZL19CreateCodecBC_SPARCv, .Lfunc_end8-_ZL19CreateCodecBC_SPARCv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL22CreateCodecBC_SPARCOutv,@function
_ZL22CreateCodecBC_SPARCOutv:           # @_ZL22CreateCodecBC_SPARCOutv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV17CBC_SPARC_Encoder+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end9:
	.size	_ZL22CreateCodecBC_SPARCOutv, .Lfunc_end9-_ZL22CreateCodecBC_SPARCOutv
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_BranchRegister.ii,@function
_GLOBAL__sub_I_BranchRegister.ii:       # @_GLOBAL__sub_I_BranchRegister.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movl	$_ZL12g_CodecsInfo, %edi
	callq	_Z13RegisterCodecPK10CCodecInfo
	movl	$_ZL12g_CodecsInfo+40, %edi
	callq	_Z13RegisterCodecPK10CCodecInfo
	movl	$_ZL12g_CodecsInfo+80, %edi
	callq	_Z13RegisterCodecPK10CCodecInfo
	movl	$_ZL12g_CodecsInfo+120, %edi
	callq	_Z13RegisterCodecPK10CCodecInfo
	movl	$_ZL12g_CodecsInfo+160, %edi
	popq	%rax
	jmp	_Z13RegisterCodecPK10CCodecInfo # TAILCALL
.Lfunc_end10:
	.size	_GLOBAL__sub_I_BranchRegister.ii, .Lfunc_end10-_GLOBAL__sub_I_BranchRegister.ii
	.cfi_endproc

	.type	_ZL12g_CodecsInfo,@object # @_ZL12g_CodecsInfo
	.data
	.p2align	4
_ZL12g_CodecsInfo:
	.quad	_ZL17CreateCodecBC_PPCv
	.quad	_ZL20CreateCodecBC_PPCOutv
	.quad	50528773                # 0x3030205
	.quad	.L.str
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZL18CreateCodecBC_IA64v
	.quad	_ZL21CreateCodecBC_IA64Outv
	.quad	50529281                # 0x3030401
	.quad	.L.str.1
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZL17CreateCodecBC_ARMv
	.quad	_ZL20CreateCodecBC_ARMOutv
	.quad	50529537                # 0x3030501
	.quad	.L.str.2
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZL18CreateCodecBC_ARMTv
	.quad	_ZL21CreateCodecBC_ARMTOutv
	.quad	50530049                # 0x3030701
	.quad	.L.str.3
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZL19CreateCodecBC_SPARCv
	.quad	_ZL22CreateCodecBC_SPARCOutv
	.quad	50530309                # 0x3030805
	.quad	.L.str.4
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.size	_ZL12g_CodecsInfo, 200

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	80                      # 0x50
	.long	80                      # 0x50
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	73                      # 0x49
	.long	65                      # 0x41
	.long	54                      # 0x36
	.long	52                      # 0x34
	.long	0                       # 0x0
	.size	.L.str.1, 20

	.type	.L.str.2,@object        # @.str.2
	.p2align	2
.L.str.2:
	.long	65                      # 0x41
	.long	82                      # 0x52
	.long	77                      # 0x4d
	.long	0                       # 0x0
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
	.p2align	2
.L.str.3:
	.long	65                      # 0x41
	.long	82                      # 0x52
	.long	77                      # 0x4d
	.long	84                      # 0x54
	.long	0                       # 0x0
	.size	.L.str.3, 20

	.type	.L.str.4,@object        # @.str.4
	.p2align	2
.L.str.4:
	.long	83                      # 0x53
	.long	80                      # 0x50
	.long	65                      # 0x41
	.long	82                      # 0x52
	.long	67                      # 0x43
	.long	0                       # 0x0
	.size	.L.str.4, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_BranchRegister.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
