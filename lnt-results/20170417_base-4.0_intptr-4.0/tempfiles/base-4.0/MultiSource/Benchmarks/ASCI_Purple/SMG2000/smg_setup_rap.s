	.text
	.file	"smg_setup_rap.bc"
	.globl	hypre_SMGCreateRAPOp
	.p2align	4, 0x90
	.type	hypre_SMGCreateRAPOp,@function
hypre_SMGCreateRAPOp:                   # @hypre_SMGCreateRAPOp
	.cfi_startproc
# BB#0:
	movq	24(%rsi), %rax
	movl	16(%rax), %eax
	cmpl	$3, %eax
	je	.LBB0_3
# BB#1:
	cmpl	$2, %eax
	jne	.LBB0_4
# BB#2:
	jmp	hypre_SMG2CreateRAPOp   # TAILCALL
.LBB0_3:
	jmp	hypre_SMG3CreateRAPOp   # TAILCALL
.LBB0_4:
	retq
.Lfunc_end0:
	.size	hypre_SMGCreateRAPOp, .Lfunc_end0-hypre_SMGCreateRAPOp
	.cfi_endproc

	.globl	hypre_SMGSetupRAPOp
	.p2align	4, 0x90
	.type	hypre_SMGSetupRAPOp,@function
hypre_SMGSetupRAPOp:                    # @hypre_SMGSetupRAPOp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %rbx
	movq	%rcx, %r15
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	24(%rbp), %rax
	movl	16(%rax), %eax
	cmpl	$3, %eax
	je	.LBB1_5
# BB#1:
	movq	%r14, %r9
	xorl	%r14d, %r14d
	cmpl	$2, %eax
	jne	.LBB1_9
# BB#2:
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r14
	movq	%r14, %r8
	movq	%r9, %rbx
	callq	hypre_SMG2BuildRAPSym
	cmpl	$0, 72(%rbp)
	je	.LBB1_3
# BB#4:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	hypre_SMG2RAPPeriodicSym
	jmp	.LBB1_8
.LBB1_5:
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	movq	%r14, %r9
	callq	hypre_SMG3BuildRAPSym
	cmpl	$0, 72(%rbp)
	je	.LBB1_6
# BB#7:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	hypre_SMG3RAPPeriodicSym
	jmp	.LBB1_8
.LBB1_3:
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	movq	%rbx, %r9
	callq	hypre_SMG2BuildRAPNoSym
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	hypre_SMG2RAPPeriodicNoSym
	jmp	.LBB1_8
.LBB1_6:
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%rbx, %r8
	movq	%r14, %r9
	callq	hypre_SMG3BuildRAPNoSym
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	hypre_SMG3RAPPeriodicNoSym
.LBB1_8:
	movl	%eax, %r14d
.LBB1_9:
	movq	%r15, %rdi
	callq	hypre_StructMatrixAssemble
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_SMGSetupRAPOp, .Lfunc_end1-hypre_SMGSetupRAPOp
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
