	.text
	.file	"libclamav_vba_extract.bc"
	.globl	vba56_dir_read
	.p2align	4, 0x90
	.type	vba56_dir_read,@function
vba56_dir_read:                         # @vba56_dir_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi6:
	.cfi_def_cfa_offset 416
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	96(%rsp), %rbx
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.1, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rcx
	callq	snprintf
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB0_6
# BB#1:
	leaq	48(%rsp), %rsi
	movl	$34, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$34, %eax
	jne	.LBB0_3
# BB#2:
	movzwl	48(%rsp), %eax
	cmpl	$25036, %eax            # imm = 0x61CC
	je	.LBB0_8
.LBB0_3:
	movl	%ebx, %edi
.LBB0_4:
	callq	close
.LBB0_5:
	xorl	%ebp, %ebp
	jmp	.LBB0_7
.LBB0_6:
	xorl	%ebp, %ebp
	leaq	96(%rsp), %rsi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_7:
	movq	%rbp, %rax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_8:                                # %.preheader138
	cmpl	$16777310, 50(%rsp)     # imm = 0x100005E
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	je	.LBB0_25
# BB#9:
	leaq	50(%rsp), %rax
	cmpl	$16777311, (%rax)       # imm = 0x100005F
	je	.LBB0_26
# BB#10:
	cmpl	$16777317, (%rax)       # imm = 0x1000065
	je	.LBB0_27
# BB#11:
	cmpl	$16777323, (%rax)       # imm = 0x100006B
	je	.LBB0_28
# BB#12:
	cmpl	$16777325, (%rax)       # imm = 0x100006D
	je	.LBB0_29
# BB#13:
	cmpl	$16777327, (%rax)       # imm = 0x100006F
	je	.LBB0_30
# BB#14:
	cmpl	$16777328, (%rax)       # imm = 0x1000070
	je	.LBB0_31
# BB#15:
	cmpl	$16777331, (%rax)       # imm = 0x1000073
	je	.LBB0_32
# BB#16:
	cmpl	$16777334, (%rax)       # imm = 0x1000076
	je	.LBB0_33
# BB#17:
	cmpl	$16777337, (%rax)       # imm = 0x1000079
	je	.LBB0_34
# BB#18:
	cmpl	$234881120, (%rax)      # imm = 0xE000060
	je	.LBB0_35
# BB#19:
	cmpl	$234881122, (%rax)      # imm = 0xE000062
	je	.LBB0_36
# BB#20:
	cmpl	$234881123, (%rax)      # imm = 0xE000063
	je	.LBB0_37
# BB#21:
	cmpl	$234881124, (%rax)      # imm = 0xE000064
	je	.LBB0_38
# BB#22:
	movzbl	50(%rsp), %esi
	movzbl	51(%rsp), %edx
	movzbl	52(%rsp), %ecx
	movzbl	53(%rsp), %r8d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movb	53(%rsp), %al
	cmpb	$14, %al
	je	.LBB0_103
# BB#23:
	cmpb	$1, %al
	jne	.LBB0_104
# BB#24:
	xorl	%r12d, %r12d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	jmp	.LBB0_40
.LBB0_25:
	xorl	%ebp, %ebp
	jmp	.LBB0_39
.LBB0_26:
	movl	$1, %ebp
	jmp	.LBB0_39
.LBB0_27:
	movl	$2, %ebp
	jmp	.LBB0_39
.LBB0_28:
	movl	$3, %ebp
	jmp	.LBB0_39
.LBB0_29:
	movl	$4, %ebp
	jmp	.LBB0_39
.LBB0_30:
	movl	$5, %ebp
	jmp	.LBB0_39
.LBB0_31:
	movl	$6, %ebp
	jmp	.LBB0_39
.LBB0_32:
	movl	$7, %ebp
	jmp	.LBB0_39
.LBB0_33:
	movl	$8, %ebp
	jmp	.LBB0_39
.LBB0_34:
	movl	$9, %ebp
	jmp	.LBB0_39
.LBB0_35:
	movl	$10, %ebp
	jmp	.LBB0_39
.LBB0_36:
	movl	$11, %ebp
	jmp	.LBB0_39
.LBB0_37:
	movl	$12, %ebp
	jmp	.LBB0_39
.LBB0_38:
	movl	$13, %ebp
.LBB0_39:                               # %.thread
	shlq	$3, %rbp
	movq	vba_version+8(%rbp,%rbp,2), %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	vba_version+16(%rbp,%rbp,2), %r12d
.LBB0_40:
	movq	%rsp, %rsi
	movl	$2, %edx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_60
# BB#41:                                # %.lr.ph.i
	movq	%rsp, %r15
	leaq	16(%rsp), %r13
	movl	%r12d, 12(%rsp)         # 4-byte Spill
.LBB0_42:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	testl	%r12d, %r12d
	cmovew	%ax, %cx
	movw	%cx, (%rsp)
	movzwl	%cx, %edi
	cmpl	$5, %edi
	jbe	.LBB0_58
# BB#43:                                #   in Loop: Header=BB0_42 Depth=1
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_59
# BB#44:                                #   in Loop: Header=BB0_42 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	movl	4(%rsp), %ebx           # 4-byte Reload
	movl	%ebx, %edi
	callq	lseek
	movq	%rax, %r14
	movzwl	(%rsp), %edx
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	cli_readn
	movzwl	(%rsp), %ecx
	cmpl	%ecx, %eax
	jne	.LBB0_61
# BB#45:                                #   in Loop: Header=BB0_42 Depth=1
	movq	%rbp, %rdi
	movl	%eax, %esi
	movl	%r12d, %edx
	callq	get_unicode_name
	movq	%rax, %r12
	movzwl	(%rsp), %esi
	testq	%r12, %r12
	je	.LBB0_62
# BB#46:                                #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	movq	%r12, %rdx
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	free
	movzwl	(%r12), %eax
	cmpl	$23594, %eax            # imm = 0x5C2A
	movb	$1, %bl
	jne	.LBB0_63
# BB#47:                                #   in Loop: Header=BB0_42 Depth=1
	movsbl	2(%r12), %esi
	movl	$.L.str.42, %edi
	movl	$5, %edx
	callq	memchr
	testq	%rax, %rax
	je	.LBB0_63
# BB#48:                                #   in Loop: Header=BB0_42 Depth=1
	movq	%r12, %rdi
	callq	free
	movl	$2, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	movl	12(%rsp), %r12d         # 4-byte Reload
	jne	.LBB0_60
# BB#49:                                #   in Loop: Header=BB0_42 Depth=1
	movzwl	(%rsp), %eax
	movw	%ax, %cx
	rolw	$8, %cx
	testl	%r12d, %r12d
	cmovew	%ax, %cx
	movw	%cx, (%rsp)
	leal	1(%rcx), %eax
	movzwl	%ax, %eax
	cmpl	$2, %eax
	jae	.LBB0_54
# BB#50:                                #   in Loop: Header=BB0_42 Depth=1
	movl	$10, %esi
	movl	$1, %edx
	movl	4(%rsp), %ebp           # 4-byte Reload
	movl	%ebp, %edi
	callq	lseek
	movq	%rax, %rcx
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	cli_dbgmsg
	movl	$20, %edx
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	cli_readn
	cmpl	$20, %eax
	jne	.LBB0_57
# BB#51:                                #   in Loop: Header=BB0_42 Depth=1
	movl	$vba56_test_middle.middle1_str, %esi
	movl	$20, %edx
	movq	%r13, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_56
# BB#52:                                #   in Loop: Header=BB0_42 Depth=1
	movl	$vba56_test_middle.middle2_str, %esi
	movl	$20, %edx
	movq	%r13, %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB0_56
# BB#53:                                #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	$-20, %rsi
	jmp	.LBB0_55
.LBB0_54:                               #   in Loop: Header=BB0_42 Depth=1
	movq	$-2, %rsi
.LBB0_55:                               # %.thread41.i
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$1, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	callq	lseek
	jmp	.LBB0_57
.LBB0_56:                               #   in Loop: Header=BB0_42 Depth=1
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_57:                               # %.thread41.i
                                        #   in Loop: Header=BB0_42 Depth=1
	movl	$2, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	je	.LBB0_42
	jmp	.LBB0_60
.LBB0_58:
	movq	$-2, %rsi
	movl	$1, %edx
	movl	4(%rsp), %r13d          # 4-byte Reload
	movl	%r13d, %edi
	callq	lseek
	jmp	.LBB0_66
.LBB0_59:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB0_60:                               # %.loopexit137
	movl	4(%rsp), %edi           # 4-byte Reload
	jmp	.LBB0_4
.LBB0_61:
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r14d, %esi
	xorl	%edx, %edx
	movl	4(%rsp), %r13d          # 4-byte Reload
	movl	%r13d, %edi
	callq	lseek
	movq	%rbp, %rdi
	jmp	.LBB0_65
.LBB0_62:                               # %.thread.i
	xorl	%r12d, %r12d
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%rbp, %rdi
	callq	free
	xorl	%ebx, %ebx
.LBB0_63:                               # %.loopexit.i
	movzwl	(%rsp), %eax
	movq	$-2, %rsi
	subq	%rax, %rsi
	movl	$1, %edx
	movl	4(%rsp), %r13d          # 4-byte Reload
	movl	%r13d, %edi
	callq	lseek
	testb	%bl, %bl
	je	.LBB0_66
# BB#64:
	movq	%r12, %rdi
.LBB0_65:                               # %vba_read_project_strings.exit
	callq	free
.LBB0_66:                               # %vba_read_project_strings.exit
	leaq	2(%rsp), %rbx
	movq	88(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_67:                               # =>This Inner Loop Header: Depth=1
	movl	$2, %edx
	movl	%r13d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_107
# BB#68:                                #   in Loop: Header=BB0_67 Depth=1
	movzwl	2(%rsp), %eax
	cmpl	$65535, %eax            # imm = 0xFFFF
	jne	.LBB0_67
# BB#69:
	movq	$-3, %rsi
	movl	$1, %edx
	movl	%r13d, %edi
	callq	lseek
	leaq	2(%rsp), %rsi
	movl	$2, %edx
	movl	%r13d, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_107
# BB#70:
	movzwl	2(%rsp), %eax
	cmpl	$65535, %eax            # imm = 0xFFFF
	movl	12(%rsp), %r12d         # 4-byte Reload
	je	.LBB0_72
# BB#71:
	movl	$1, %esi
	movl	$1, %edx
	movl	%r13d, %edi
	callq	lseek
.LBB0_72:
	leaq	2(%rsp), %rsi
	movl	$2, %edx
	movl	%r13d, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_107
# BB#73:
	movzwl	2(%rsp), %eax
	cmpl	$65535, %eax            # imm = 0xFFFF
	je	.LBB0_75
# BB#74:
	movl	%eax, %ecx
	rolw	$8, %cx
	testl	%r12d, %r12d
	cmovew	%ax, %cx
	movw	%cx, 2(%rsp)
	movzwl	%cx, %esi
	movl	$1, %edx
	movl	%r13d, %edi
	callq	lseek
.LBB0_75:
	leaq	2(%rsp), %rsi
	movl	$2, %edx
	movl	%r13d, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_107
# BB#76:
	movzwl	2(%rsp), %eax
	cmpl	$65535, %eax            # imm = 0xFFFF
	je	.LBB0_78
# BB#77:
	movl	%eax, %ecx
	rolw	$8, %cx
	testl	%r12d, %r12d
	cmovew	%ax, %cx
	movw	%cx, 2(%rsp)
	movzwl	%cx, %esi
	movl	$1, %edx
	movl	%r13d, %edi
	callq	lseek
.LBB0_78:
	movl	$100, %esi
	movl	$1, %edx
	movl	%r13d, %edi
	callq	lseek
	movq	%rsp, %rsi
	movl	$2, %edx
	movl	%r13d, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_107
# BB#79:
	movzwl	(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	testl	%r12d, %r12d
	cmovew	%ax, %cx
	movw	%cx, (%rsp)
	movzwl	%cx, %esi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	(%rsp), %eax
	testw	%ax, %ax
	je	.LBB0_107
# BB#80:
	movzwl	%ax, %eax
	cmpl	$1001, %eax             # imm = 0x3E9
	jb	.LBB0_82
# BB#81:
	xorl	%ebp, %ebp
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%r13d, %edi
	callq	close
	jmp	.LBB0_7
.LBB0_82:
	movl	$48, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_107
# BB#83:
	movzwl	(%rsp), %edi
	shlq	$3, %rdi
	callq	cli_malloc
	movq	%rax, 8(%rbp)
	testq	%rax, %rax
	je	.LBB0_106
# BB#84:
	movq	%r15, %rdi
	callq	cli_strdup
	movq	%rax, 40(%rbp)
	movzwl	(%rsp), %edi
	shlq	$2, %rdi
	callq	cli_malloc
	movq	%rax, 16(%rbp)
	testq	%rax, %rax
	je	.LBB0_105
# BB#85:
	movzwl	(%rsp), %eax
	testl	%eax, %eax
	movl	%eax, (%rbp)
	je	.LBB0_108
# BB#86:                                # %.lr.ph179.preheader
	leaq	8(%rsp), %r13
	xorl	%r15d, %r15d
	movl	4(%rsp), %r14d          # 4-byte Reload
.LBB0_87:                               # %.lr.ph179
                                        # =>This Inner Loop Header: Depth=1
	movl	$2, %edx
	movl	%r14d, %edi
	movq	%r13, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_115
# BB#88:                                #   in Loop: Header=BB0_87 Depth=1
	movq	%r13, %rbx
	movzwl	8(%rsp), %ecx
	movl	%ecx, %eax
	rolw	$8, %ax
	testl	%r12d, %r12d
	cmovew	%cx, %ax
	movw	%ax, 8(%rsp)
	testw	%ax, %ax
	je	.LBB0_110
# BB#89:                                #   in Loop: Header=BB0_87 Depth=1
	movzwl	%ax, %edi
	callq	cli_malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_111
# BB#90:                                #   in Loop: Header=BB0_87 Depth=1
	movzwl	8(%rsp), %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	movq	%r13, %rsi
	callq	cli_readn
	movzwl	8(%rsp), %ecx
	cmpl	%ecx, %eax
	jne	.LBB0_113
# BB#91:                                #   in Loop: Header=BB0_87 Depth=1
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%r12d, %edx
	callq	get_unicode_name
	movq	8(%rbp), %rcx
	movq	%rax, (%rcx,%r15,8)
	movq	%r13, %rdi
	callq	free
	movq	8(%rbp), %rax
	movq	(%rax,%r15,8), %rsi
	testq	%rsi, %rsi
	movq	%rbx, %r13
	jne	.LBB0_94
# BB#92:                                #   in Loop: Header=BB0_87 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	movl	4(%rsp), %edi           # 4-byte Reload
	callq	lseek
	movl	%eax, 16(%rsp)
	movl	$18, %edi
	callq	cli_malloc
	movq	8(%rbp), %rcx
	movq	%rax, (%rcx,%r15,8)
	movq	8(%rbp), %rax
	movq	(%rax,%r15,8), %rdi
	testq	%rdi, %rdi
	je	.LBB0_115
# BB#93:                                #   in Loop: Header=BB0_87 Depth=1
	movl	16(%rsp), %ecx
	movl	$18, %esi
	movl	$.L.str.13, %edx
	xorl	%eax, %eax
	callq	snprintf
	movq	8(%rbp), %rax
	movq	(%rax,%r15,8), %rsi
.LBB0_94:                               #   in Loop: Header=BB0_87 Depth=1
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$2, %edx
	movl	4(%rsp), %ebx           # 4-byte Reload
	movl	%ebx, %edi
	movq	%r13, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_109
# BB#95:                                #   in Loop: Header=BB0_87 Depth=1
	movzwl	8(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	movl	%r12d, %r14d
	testl	%r14d, %r14d
	cmovew	%ax, %cx
	movw	%cx, 8(%rsp)
	movzwl	%cx, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	movl	$2, %edx
	movl	%ebx, %edi
	leaq	2(%rsp), %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_109
# BB#96:                                #   in Loop: Header=BB0_87 Depth=1
	movzwl	2(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	testl	%r14d, %r14d
	cmovew	%ax, %cx
	movw	%cx, 2(%rsp)
	movzwl	%cx, %esi
	cmpl	$65535, %esi            # imm = 0xFFFF
	je	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_87 Depth=1
	addq	$2, %rsi
	jmp	.LBB0_100
.LBB0_98:                               #   in Loop: Header=BB0_87 Depth=1
	movl	$2, %esi
	movl	$1, %edx
	movl	4(%rsp), %ebx           # 4-byte Reload
	movl	%ebx, %edi
	callq	lseek
	movl	$2, %edx
	movl	%ebx, %edi
	leaq	2(%rsp), %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_109
# BB#99:                                #   in Loop: Header=BB0_87 Depth=1
	movzwl	2(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	testl	%r12d, %r12d
	cmovew	%ax, %cx
	movw	%cx, 2(%rsp)
	movzwl	%cx, %esi
.LBB0_100:                              #   in Loop: Header=BB0_87 Depth=1
	movl	$1, %edx
	movl	4(%rsp), %r14d          # 4-byte Reload
	movl	%r14d, %edi
	callq	lseek
	movl	$8, %esi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	lseek
	movl	$2, %edx
	movl	%r14d, %edi
	leaq	10(%rsp), %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB0_109
# BB#101:                               #   in Loop: Header=BB0_87 Depth=1
	movzwl	10(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	movl	%r12d, %ebx
	testl	%ebx, %ebx
	cmovew	%ax, %cx
	movw	%cx, 10(%rsp)
	movzwl	%cx, %eax
	leaq	5(,%rax,8), %rsi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	lseek
	movl	$4, %edx
	movl	%r14d, %edi
	leaq	16(%rsp), %rsi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB0_109
# BB#102:                               #   in Loop: Header=BB0_87 Depth=1
	testl	%ebx, %ebx
	movl	16(%rsp), %eax
	movl	%eax, %ecx
	bswapl	%ecx
	cmovel	%eax, %ecx
	movl	%ecx, 16(%rsp)
	movq	16(%rbp), %rax
	movl	%ecx, (%rax,%r15,4)
	movl	16(%rsp), %esi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$2, %esi
	movl	$1, %edx
	movl	%r14d, %edi
	callq	lseek
	incq	%r15
	movzwl	(%rsp), %eax
	cmpq	%rax, %r15
	jl	.LBB0_87
	jmp	.LBB0_115
.LBB0_103:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$1, %r12d
	jmp	.LBB0_40
.LBB0_104:
	xorl	%ebp, %ebp
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	%ebx, %edi
	callq	close
	jmp	.LBB0_7
.LBB0_105:
	movq	40(%rbp), %rdi
	callq	free
	movq	8(%rbp), %rdi
	callq	free
.LBB0_106:
	movq	%rbp, %rdi
	callq	free
.LBB0_107:
	movl	%r13d, %edi
	jmp	.LBB0_4
.LBB0_108:
	xorl	%r15d, %r15d
	jmp	.LBB0_115
.LBB0_109:
	movq	8(%rbp), %rax
	movq	(%rax,%r15,8), %rdi
.LBB0_114:                              # %.loopexit
	callq	free
	jmp	.LBB0_115
.LBB0_110:
	movl	$.L.str.10, %edi
	jmp	.LBB0_112
.LBB0_111:
	movl	$.L.str.11, %edi
.LBB0_112:                              # %.loopexit
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_115:                              # %.loopexit
	movl	4(%rsp), %edi           # 4-byte Reload
	callq	close
	movzwl	(%rsp), %eax
	cmpl	%eax, %r15d
	jge	.LBB0_7
# BB#116:                               # %.preheader
	movq	8(%rbp), %rdi
	testl	%r15d, %r15d
	jle	.LBB0_119
# BB#117:                               # %.lr.ph.preheader
	movslq	%r15d, %rbx
	incq	%rbx
.LBB0_118:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rdi,%rbx,8), %rdi
	callq	free
	movq	8(%rbp), %rdi
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB0_118
.LBB0_119:                              # %._crit_edge
	callq	free
	movq	40(%rbp), %rdi
	callq	free
	movq	16(%rbp), %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB0_5
.LBB0_113:
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r13, %rdi
	jmp	.LBB0_114
.Lfunc_end0:
	.size	vba56_dir_read, .Lfunc_end0-vba56_dir_read
	.cfi_endproc

	.p2align	4, 0x90
	.type	get_unicode_name,@function
get_unicode_name:                       # @get_unicode_name
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r15d
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB1_18
# BB#1:
	testl	%r15d, %r15d
	jle	.LBB1_18
# BB#2:
	movb	(%rbx), %al
	testb	%al, %al
	je	.LBB1_18
# BB#3:
	leal	(,%r15,8), %eax
	subl	%r15d, %eax
	movslq	%eax, %rdi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_4
# BB#5:
	testl	%ebp, %ebp
	jne	.LBB1_8
# BB#6:
	movl	%r15d, %eax
	andl	$1, %eax
	je	.LBB1_8
# BB#7:
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	decl	%r15d
.LBB1_8:
	testl	%r15d, %r15d
	movq	%r14, %rdi
	jle	.LBB1_17
# BB#9:                                 # %.lr.ph
	callq	__ctype_b_loc
	cmpl	$1, %ebp
	movl	$1, %r9d
	adcq	$0, %r9
	movslq	%r15d, %r8
	xorl	%esi, %esi
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rbp
	movsbq	(%rbx,%rsi), %rdx
	testb	$64, 1(%rbp,%rdx,2)
	jne	.LBB1_11
# BB#12:                                #   in Loop: Header=BB1_10 Depth=1
	cmpb	$9, %dl
	ja	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_10 Depth=1
	movb	$95, (%rdi)
	movzbl	(%rbx,%rsi), %edx
	addb	$48, %dl
	leaq	2(%rdi), %rcx
	jmp	.LBB1_15
	.p2align	4, 0x90
.LBB1_11:                               #   in Loop: Header=BB1_10 Depth=1
	movb	%dl, (%rdi)
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_14:                               #   in Loop: Header=BB1_10 Depth=1
	movzbl	%dl, %edx
	shll	$8, %edx
	movsbl	1(%rbx,%rsi), %ecx
	movb	$95, (%rdi)
	movl	%ecx, %ebp
	andl	$3840, %ebp             # imm = 0xF00
	orl	%edx, %ebp
	movl	%ecx, %edx
	andb	$15, %dl
	addb	$97, %dl
	shrl	$4, %ecx
	andl	$15, %ecx
	addl	$97, %ecx
	movb	%cl, 2(%rdi)
	shrl	$8, %ebp
	andl	$15, %ebp
	addl	$97, %ebp
	leaq	4(%rdi), %rcx
	movb	%bpl, 3(%rdi)
.LBB1_15:                               #   in Loop: Header=BB1_10 Depth=1
	movb	%dl, 1(%rdi)
	movb	$95, (%rcx)
	movq	%rcx, %rdi
.LBB1_16:                               #   in Loop: Header=BB1_10 Depth=1
	incq	%rdi
	addq	%r9, %rsi
	cmpq	%r8, %rsi
	jl	.LBB1_10
.LBB1_17:                               # %._crit_edge
	movb	$0, (%rdi)
	jmp	.LBB1_18
.LBB1_4:
	xorl	%r14d, %r14d
.LBB1_18:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	get_unicode_name, .Lfunc_end1-get_unicode_name
	.cfi_endproc

	.globl	vba_decompress
	.p2align	4, 0x90
	.type	vba_decompress,@function
vba_decompress:                         # @vba_decompress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$4136, %rsp             # imm = 0x1028
.Lcfi28:
	.cfi_def_cfa_offset 4192
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdx, %r13
	movl	%esi, %ebx
	movl	%edi, %r15d
	callq	blobCreate
	testq	%rax, %rax
	je	.LBB2_41
# BB#1:
	movq	%rax, 16(%rsp)          # 8-byte Spill
	addl	$3, %ebx
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	movl	%r15d, %edi
	movq	%rbx, %rsi
	callq	lseek
	leaq	13(%rsp), %rsi
	movl	$1, %r14d
	movl	$1, %edx
	movl	%r15d, %edi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB2_33
# BB#2:                                 # %.preheader.preheader
	movq	%r13, 24(%rsp)          # 8-byte Spill
	jmp	.LBB2_3
.LBB2_9:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$2, %edx
	movl	%r15d, %edi
	leaq	14(%rsp), %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB2_37
# BB#10:                                #   in Loop: Header=BB2_3 Depth=1
	movl	$4096, %edx             # imm = 0x1000
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	32(%rsp), %rsi
	callq	blobAddData
	xorl	%r14d, %r14d
	jmp	.LBB2_30
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
                                        #       Child Loop BB2_25 Depth 3
	movl	$1, %r13d
	.p2align	4, 0x90
.LBB2_4:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_25 Depth 3
	movzbl	13(%rsp), %eax
	testl	%r13d, %eax
	je	.LBB2_11
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=2
	movl	$2, %edx
	movl	%r15d, %edi
	leaq	14(%rsp), %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB2_37
# BB#6:                                 #   in Loop: Header=BB2_4 Depth=2
	movl	%r12d, %edx
	andl	$4095, %edx             # imm = 0xFFF
	cmpl	$128, %edx
	ja	.LBB2_14
# BB#7:                                 #   in Loop: Header=BB2_4 Depth=2
	cmpl	$32, %edx
	ja	.LBB2_16
# BB#8:                                 #   in Loop: Header=BB2_4 Depth=2
	cmpl	$17, %edx
	movl	$11, %ecx
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_11:                               #   in Loop: Header=BB2_4 Depth=2
	testl	%r12d, %r12d
	je	.LBB2_27
# BB#12:                                #   in Loop: Header=BB2_4 Depth=2
	movl	%r12d, %eax
	andl	$4095, %eax             # imm = 0xFFF
	testl	%r14d, %r14d
	je	.LBB2_28
# BB#13:                                #   in Loop: Header=BB2_4 Depth=2
	testl	%eax, %eax
	jne	.LBB2_28
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_14:                               #   in Loop: Header=BB2_4 Depth=2
	cmpl	$512, %edx              # imm = 0x200
	ja	.LBB2_17
# BB#15:                                #   in Loop: Header=BB2_4 Depth=2
	cmpl	$257, %edx              # imm = 0x101
	movl	$7, %ecx
	jmp	.LBB2_19
.LBB2_16:                               #   in Loop: Header=BB2_4 Depth=2
	cmpl	$65, %edx
	movl	$9, %ecx
	jmp	.LBB2_19
.LBB2_17:                               #   in Loop: Header=BB2_4 Depth=2
	movl	$4, %ecx
	cmpl	$2048, %edx             # imm = 0x800
	ja	.LBB2_20
# BB#18:                                #   in Loop: Header=BB2_4 Depth=2
	cmpl	$1025, %edx             # imm = 0x401
	movl	$5, %ecx
	.p2align	4, 0x90
.LBB2_19:                               #   in Loop: Header=BB2_4 Depth=2
	adcl	$0, %ecx
.LBB2_20:                               #   in Loop: Header=BB2_4 Depth=2
	movzwl	14(%rsp), %eax
	movl	$1, %edi
	shll	%cl, %edi
	addl	$65535, %edi            # imm = 0xFFFF
	andl	%eax, %edi
	addl	$3, %edi
	movl	%edi, %esi
	andl	$65535, %esi            # imm = 0xFFFF
	je	.LBB2_29
# BB#21:                                # %.lr.ph
                                        #   in Loop: Header=BB2_4 Depth=2
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	xorl	%ebx, %ebx
	testb	$1, %dil
	movl	%r12d, %ebp
	je	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_4 Depth=2
	movl	$4095, %ecx             # imm = 0xFFF
	subl	%eax, %ecx
	addl	%r12d, %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	movb	32(%rsp,%rcx), %bl
	leal	1(%r12), %ebp
	movb	%bl, 32(%rsp,%rdx)
	movl	$1, %ebx
.LBB2_23:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_4 Depth=2
	cmpl	$1, %esi
	je	.LBB2_26
# BB#24:                                # %.lr.ph.new
                                        #   in Loop: Header=BB2_4 Depth=2
	movl	%esi, %edx
	subl	%ebx, %edx
	negl	%eax
	.p2align	4, 0x90
.LBB2_25:                               #   Parent Loop BB2_3 Depth=1
                                        #     Parent Loop BB2_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	4095(%rax,%rbp), %edi
	andl	$4095, %edi             # imm = 0xFFF
	movzbl	32(%rsp,%rdi), %ebx
	leal	1(%rbp), %edi
	movl	%ebp, %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	movb	%bl, 32(%rsp,%rcx)
	leal	4096(%rax,%rbp), %ecx
	andl	$4095, %ecx             # imm = 0xFFF
	movzbl	32(%rsp,%rcx), %ecx
	addl	$2, %ebp
	andl	$4095, %edi             # imm = 0xFFF
	movb	%cl, 32(%rsp,%rdi)
	addl	$-2, %edx
	jne	.LBB2_25
.LBB2_26:                               # %.loopexit.loopexit
                                        #   in Loop: Header=BB2_4 Depth=2
	addl	%esi, %r12d
	jmp	.LBB2_29
.LBB2_27:                               #   in Loop: Header=BB2_4 Depth=2
	xorl	%eax, %eax
.LBB2_28:                               # %._crit_edge107
                                        #   in Loop: Header=BB2_4 Depth=2
	movl	%eax, %eax
	leaq	32(%rsp,%rax), %rsi
	movl	$1, %edx
	movl	%r15d, %edi
	callq	cli_readn
	xorl	%ecx, %ecx
	cmpl	$1, %eax
	sete	%cl
	addl	%ecx, %r12d
.LBB2_29:                               # %.loopexit
                                        #   in Loop: Header=BB2_4 Depth=2
	addl	%r13d, %r13d
	movl	$1, %r14d
	cmpl	$256, %r13d             # imm = 0x100
	jb	.LBB2_4
.LBB2_30:                               # %.backedge
                                        #   in Loop: Header=BB2_3 Depth=1
	movl	$1, %edx
	movl	%r15d, %edi
	leaq	13(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	je	.LBB2_3
# BB#31:                                # %._crit_edge
	andl	$4095, %r12d            # imm = 0xFFF
	movq	24(%rsp), %r13          # 8-byte Reload
	je	.LBB2_33
# BB#32:
	movl	%r12d, %edx
	leaq	32(%rsp), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	blobAddData
	testl	%eax, %eax
	js	.LBB2_43
.LBB2_33:                               # %._crit_edge.thread
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	blobGetDataSize
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_39
# BB#34:
	testq	%r13, %r13
	je	.LBB2_36
# BB#35:
	movl	%r14d, (%r13)
.LBB2_36:
	movq	%rbp, %rdi
	callq	blobGetData
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	callq	memcpy
	movq	%rbp, %rdi
	callq	blobDestroy
	jmp	.LBB2_42
.LBB2_37:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	blobDestroy
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB2_41
# BB#38:
	movl	$0, (%rax)
	jmp	.LBB2_41
.LBB2_39:
	movq	%rbp, %rdi
	callq	blobDestroy
	testq	%r13, %r13
	je	.LBB2_41
# BB#40:
	movl	$0, (%r13)
.LBB2_41:
	xorl	%ebx, %ebx
.LBB2_42:
	movq	%rbx, %rax
	addq	$4136, %rsp             # imm = 0x1028
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_43:
	testq	%r13, %r13
	je	.LBB2_45
# BB#44:
	movl	$0, (%r13)
.LBB2_45:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	blobDestroy
	jmp	.LBB2_41
.Lfunc_end2:
	.size	vba_decompress, .Lfunc_end2-vba_decompress
	.cfi_endproc

	.globl	cli_decode_ole_object
	.p2align	4, 0x90
	.type	cli_decode_ole_object,@function
cli_decode_ole_object:                  # @cli_decode_ole_object
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 48
	subq	$8624, %rsp             # imm = 0x21B0
.Lcfi40:
	.cfi_def_cfa_offset 8672
.Lcfi41:
	.cfi_offset %rbx, -48
.Lcfi42:
	.cfi_offset %r12, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r12d
	leaq	16(%rsp), %rdx
	movl	$1, %edi
	movl	%r12d, %esi
	callq	__fxstat
	movl	$-1, %r15d
	cmpl	$-1, %eax
	je	.LBB3_21
# BB#1:
	leaq	12(%rsp), %rsi
	movl	$4, %edx
	movl	%r12d, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB3_21
# BB#2:
	movq	64(%rsp), %rax
	movl	12(%rsp), %ecx
	subq	%rcx, %rax
	cmpq	$4, %rax
	jl	.LBB3_15
# BB#3:
	movl	$2, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB3_21
# BB#4:                                 # %.preheader17.preheader
	leaq	11(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_5:                                # %.preheader17
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movl	%r12d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB3_21
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	cmpb	$0, 11(%rsp)
	jne	.LBB3_5
# BB#7:                                 # %.preheader15.preheader
	leaq	11(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_8:                                # %.preheader15
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movl	%r12d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB3_21
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=1
	cmpb	$0, 11(%rsp)
	jne	.LBB3_8
# BB#10:
	movl	$8, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB3_21
# BB#11:                                # %.preheader.preheader
	leaq	11(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_12:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movl	%r12d, %edi
	movq	%rbx, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB3_21
# BB#13:                                #   in Loop: Header=BB3_12 Depth=1
	cmpb	$0, 11(%rsp)
	jne	.LBB3_12
# BB#14:
	leaq	12(%rsp), %rsi
	movl	$4, %edx
	movl	%r12d, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB3_21
.LBB3_15:
	leaq	160(%rsp), %rbx
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.16, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rcx
	callq	snprintf
	movl	$578, %esi              # imm = 0x242
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	testl	%eax, %eax
	js	.LBB3_21
# BB#16:
	movl	%eax, %r15d
	movl	12(%rsp), %ebp
	leaq	432(%rsp), %r14
	.p2align	4, 0x90
.LBB3_17:                               # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	je	.LBB3_20
# BB#18:                                #   in Loop: Header=BB3_17 Depth=1
	cmpl	$8192, %ebp             # imm = 0x2000
	movl	$8192, %ebx             # imm = 0x2000
	cmovbl	%ebp, %ebx
	movl	%r12d, %edi
	movq	%r14, %rsi
	movl	%ebx, %edx
	callq	cli_readn
	cmpl	%ebx, %eax
	jne	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_17 Depth=1
	movl	%r15d, %edi
	movq	%r14, %rsi
	movl	%ebx, %edx
	callq	cli_writen
	cmpl	%ebx, %eax
	movl	$0, %ecx
	cmovel	%ebx, %ecx
	subl	%ecx, %ebp
	cmpl	%ebx, %eax
	je	.LBB3_17
.LBB3_20:                               # %ole_copy_file_data.exit
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r15d, %edi
	callq	lseek
.LBB3_21:                               # %.loopexit
	movl	%r15d, %eax
	addq	$8624, %rsp             # imm = 0x21B0
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	cli_decode_ole_object, .Lfunc_end3-cli_decode_ole_object
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.zero	16
	.text
	.globl	ppt_vba_read
	.p2align	4, 0x90
	.type	ppt_vba_read,@function
ppt_vba_read:                           # @ppt_vba_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$17096, %rsp            # imm = 0x42C8
.Lcfi52:
	.cfi_def_cfa_offset 17152
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rdi, %rcx
	leaq	432(%rsp), %rbx
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.17, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	snprintf
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %r12d
	cmpl	$-1, %r12d
	je	.LBB4_4
# BB#1:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbx
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbx, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB4_5
# BB#2:
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	cli_errmsg
.LBB4_3:                                # %ppt_stream_iter.exit
	movq	%rbx, %rdi
	callq	free
	xorl	%ebx, %ebx
	jmp	.LBB4_35
.LBB4_4:
	xorl	%ebx, %ebx
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB4_36
.LBB4_5:                                # %.preheader.i
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	movq	%rax, 8(%rsp)
	leaq	16(%rsp), %r15
	movl	$2, %edx
	movl	%r12d, %edi
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB4_33
# BB#6:                                 # %.lr.ph.i
	leaq	22(%rsp), %r13
	leaq	8896(%rsp), %r14
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	movzwl	16(%rsp), %eax
	movl	%eax, %ecx
	andb	$15, %cl
	movb	%cl, 18(%rsp)
	shrl	$4, %eax
	movw	%ax, 20(%rsp)
	movl	$2, %edx
	movl	%r12d, %edi
	movq	%r13, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB4_33
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=1
	movl	$4, %edx
	movl	%r12d, %edi
	leaq	24(%rsp), %rsi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB4_33
# BB#9:                                 # %ppt_read_atom_header.exit.i
                                        #   in Loop: Header=BB4_7 Depth=1
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzbl	18(%rsp), %esi
	movl	$.L.str.53, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	20(%rsp), %esi
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	22(%rsp), %esi
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	24(%rsp), %esi
	movl	$.L.str.56, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$0, 24(%rsp)
	je	.LBB4_40
# BB#10:                                #   in Loop: Header=BB4_7 Depth=1
	movzwl	22(%rsp), %eax
	cmpl	$4113, %eax             # imm = 0x1011
	jne	.LBB4_26
# BB#11:                                #   in Loop: Header=BB4_7 Depth=1
	movl	$4, %edx
	movl	%r12d, %edi
	leaq	44(%rsp), %rsi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB4_37
# BB#12:                                #   in Loop: Header=BB4_7 Depth=1
	movl	44(%rsp), %esi
	movl	24(%rsp), %edx
	addl	$-4, %edx
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	24(%rsp), %r13d
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	movq	%rax, %rbp
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.57, %edx
	xorl	%eax, %eax
	leaq	160(%rsp), %rbx
	movq	%rbx, %rdi
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %r8
	callq	snprintf
	movl	$577, %esi              # imm = 0x241
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %r15d
	cmpl	$-1, %r15d
	je	.LBB4_27
# BB#13:                                #   in Loop: Header=BB4_7 Depth=1
	addl	$-4, %r13d
	leaq	112(%rsp), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	$0, 16(%rax)
	movq	%r14, 48(%rsp)
	cmpl	$8192, %r13d            # imm = 0x2000
	movl	$8192, %ebp             # imm = 0x2000
	cmovbl	%r13d, %ebp
	movl	%ebp, 56(%rsp)
	movl	%r12d, %edi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	cli_readn
	cltq
	cmpq	%rbp, %rax
	jne	.LBB4_28
# BB#14:                                #   in Loop: Header=BB4_7 Depth=1
	movl	$.L.str.59, %esi
	movl	$112, %edx
	leaq	48(%rsp), %rbx
	movq	%rbx, %rdi
	callq	inflateInit_
	movl	%eax, %ecx
	testl	%ecx, %ecx
	je	.LBB4_16
# BB#15:                                #   in Loop: Header=BB4_7 Depth=1
	movl	$.L.str.60, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	cli_dbgmsg
.LBB4_16:                               #   in Loop: Header=BB4_7 Depth=1
	subl	%ebp, %r13d
	leaq	704(%rsp), %rax
	movq	%rax, 72(%rsp)
	movl	$8192, 80(%rsp)         # imm = 0x2000
	movl	$8192, %eax             # imm = 0x2000
	testl	%eax, %eax
	jne	.LBB4_20
	.p2align	4, 0x90
.LBB4_18:                               #   in Loop: Header=BB4_7 Depth=1
	movl	$8192, %edx             # imm = 0x2000
	movl	%r15d, %edi
	leaq	704(%rsp), %rbx
	movq	%rbx, %rsi
	callq	cli_writen
	cmpl	$8192, %eax             # imm = 0x2000
	jne	.LBB4_29
# BB#19:                                #   in Loop: Header=BB4_7 Depth=1
	movq	%rbx, 72(%rsp)
	movl	$8192, 80(%rsp)         # imm = 0x2000
	cmpl	$0, 56(%rsp)
	jne	.LBB4_23
	jmp	.LBB4_21
	.p2align	4, 0x90
.LBB4_17:                               # %thread-pre-split.i.i
                                        #   in Loop: Header=BB4_7 Depth=1
	movl	80(%rsp), %eax
	testl	%eax, %eax
	je	.LBB4_18
.LBB4_20:                               #   in Loop: Header=BB4_7 Depth=1
	cmpl	$0, 56(%rsp)
	jne	.LBB4_23
.LBB4_21:                               #   in Loop: Header=BB4_7 Depth=1
	movq	%r14, 48(%rsp)
	cmpl	$8192, %r13d            # imm = 0x2000
	movl	$8192, %ebp             # imm = 0x2000
	cmovbl	%r13d, %ebp
	movl	%ebp, 56(%rsp)
	movl	%r12d, %edi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	cli_readn
	movslq	%eax, %rcx
	movl	56(%rsp), %eax
	cmpq	%rax, %rcx
	jne	.LBB4_29
# BB#22:                                #   in Loop: Header=BB4_7 Depth=1
	subl	%eax, %r13d
.LBB4_23:                               #   in Loop: Header=BB4_7 Depth=1
	xorl	%esi, %esi
	leaq	48(%rsp), %rdi
	callq	inflate
	testl	%eax, %eax
	je	.LBB4_17
# BB#24:                                #   in Loop: Header=BB4_7 Depth=1
	movl	%r15d, %edi
	leaq	704(%rsp), %rsi
	movl	%ebp, %edx
	callq	cli_writen
	cltq
	movl	%ebp, %ecx
	cmpq	%rcx, %rax
	jne	.LBB4_29
# BB#25:                                #   in Loop: Header=BB4_7 Depth=1
	leaq	48(%rsp), %rdi
	callq	inflateEnd
	movl	%r15d, %edi
	callq	close
	movl	%eax, %ebp
	jmp	.LBB4_31
	.p2align	4, 0x90
.LBB4_26:                               #   in Loop: Header=BB4_7 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	movl	24(%rsp), %ebp
	addq	%rax, %rbp
	xorl	%edx, %edx
	movl	%r12d, %edi
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	je	.LBB4_32
	jmp	.LBB4_34
.LBB4_27:                               #   in Loop: Header=BB4_7 Depth=1
	xorl	%ebp, %ebp
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB4_31
.LBB4_28:                               #   in Loop: Header=BB4_7 Depth=1
	movl	%r15d, %edi
	callq	close
	leaq	160(%rsp), %rdi
	callq	unlink
	jmp	.LBB4_30
.LBB4_29:                               #   in Loop: Header=BB4_7 Depth=1
	movl	%r15d, %edi
	callq	close
	leaq	48(%rsp), %rdi
	callq	inflateEnd
.LBB4_30:                               # %ppt_unlzw.exit.i
                                        #   in Loop: Header=BB4_7 Depth=1
	xorl	%ebp, %ebp
.LBB4_31:                               # %ppt_unlzw.exit.i
                                        #   in Loop: Header=BB4_7 Depth=1
	leaq	16(%rsp), %r15
	leaq	22(%rsp), %r13
	testl	%ebp, %ebp
	je	.LBB4_38
.LBB4_32:                               # %.backedge.i
                                        #   in Loop: Header=BB4_7 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%r12d, %edi
	callq	lseek
	movq	%rax, 8(%rsp)
	movl	$2, %edx
	movl	%r12d, %edi
	movq	%r15, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	je	.LBB4_7
.LBB4_33:                               # %._crit_edge.i
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB4_34:                               # %ppt_stream_iter.exit
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB4_35:                               # %ppt_stream_iter.exit
	movl	%r12d, %edi
	callq	close
.LBB4_36:
	movq	%rbx, %rax
	addq	$17096, %rsp            # imm = 0x42C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_37:
	movl	$.L.str.48, %edi
	jmp	.LBB4_39
.LBB4_38:
	movl	$.L.str.50, %edi
.LBB4_39:                               # %ppt_stream_iter.exit
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB4_40:
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	cli_rmdirs
	jmp	.LBB4_3
.Lfunc_end4:
	.size	ppt_vba_read, .Lfunc_end4-ppt_vba_read
	.cfi_endproc

	.globl	wm_dir_read
	.p2align	4, 0x90
	.type	wm_dir_read,@function
wm_dir_read:                            # @wm_dir_read
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$328, %rsp              # imm = 0x148
.Lcfi65:
	.cfi_def_cfa_offset 384
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	leaq	64(%rsp), %rbx
	movl	$256, %esi              # imm = 0x100
	movl	$.L.str.19, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rcx
	callq	snprintf
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	open
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB5_8
# BB#1:
	movl	$280, %esi              # imm = 0x118
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	$280, %rax              # imm = 0x118
	jne	.LBB5_9
# BB#2:
	leaq	52(%rsp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB5_10
# BB#3:
	leaq	56(%rsp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB5_10
# BB#4:                                 # %wm_read_fib.exit
	cmpl	$0, 56(%rsp)
	je	.LBB5_102
# BB#5:
	movl	52(%rsp), %esi
	movl	$.L.str.63, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	56(%rsp), %esi
	movl	$.L.str.64, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	52(%rsp), %esi
	incl	%esi
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	movl	52(%rsp), %r12d
	leal	1(%r12), %ecx
	cmpq	%rcx, %rax
	jne	.LBB5_103
# BB#6:
	addl	56(%rsp), %r12d
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	%r12, %rax
	jge	.LBB5_12
# BB#7:                                 # %.lr.ph112
	leaq	11(%rsp), %rbp
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	jmp	.LBB5_100
.LBB5_8:
	xorl	%ebp, %ebp
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB5_14
.LBB5_9:
	movl	$.L.str.61, %edi
	jmp	.LBB5_11
.LBB5_10:
	movl	$.L.str.62, %edi
.LBB5_11:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB5_12:
	movl	%ebx, %edi
	callq	close
.LBB5_13:
	xorl	%ebp, %ebp
.LBB5_14:
	movq	%rbp, %rax
	addq	$328, %rsp              # imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_15:                               #   in Loop: Header=BB5_100 Depth=1
	movzbl	11(%rsp), %esi
	movl	$1, %r15d
	movl	%esi, %eax
	decb	%al
	cmpb	$63, %al
	ja	.LBB5_36
# BB#16:                                #   in Loop: Header=BB5_100 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI5_0(,%rax,8)
.LBB5_17:                               #   in Loop: Header=BB5_100 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB5_19
# BB#18:                                # %wm_free_macro_info.exit
                                        #   in Loop: Header=BB5_100 Depth=1
	movq	8(%rax), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
.LBB5_19:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$16, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB5_78
# BB#20:                                #   in Loop: Header=BB5_100 Depth=1
	movl	$2, %edx
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB5_75
# BB#21:                                #   in Loop: Header=BB5_100 Depth=1
	movzwl	(%rbp), %esi
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	(%rbp), %eax
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %rdi
	callq	cli_malloc
	movq	%rbp, %rdi
	movq	%rax, %r13
	movq	%r13, 8(%rdi)
	testq	%r13, %r13
	je	.LBB5_77
# BB#22:                                # %.preheader.i
                                        #   in Loop: Header=BB5_100 Depth=1
	cmpw	$0, (%rdi)
	leaq	11(%rsp), %rbp
	je	.LBB5_87
# BB#23:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB5_100 Depth=1
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	$1, %edx
	movl	%ebx, %edi
	movq	%r13, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB5_35
# BB#24:                                # %.lr.ph109.preheader
                                        #   in Loop: Header=BB5_100 Depth=1
	movl	$12, %ebp
	xorl	%r15d, %r15d
.LBB5_25:                               # %.lr.ph109
                                        #   Parent Loop BB5_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-11(%r13,%rbp), %rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB5_35
# BB#26:                                #   in Loop: Header=BB5_25 Depth=2
	leaq	-10(%r13,%rbp), %rsi
	movl	$2, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB5_35
# BB#27:                                #   in Loop: Header=BB5_25 Depth=2
	leaq	-8(%r13,%rbp), %rsi
	movl	$2, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB5_35
# BB#28:                                #   in Loop: Header=BB5_25 Depth=2
	leaq	-6(%r13,%rbp), %rsi
	movl	$2, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB5_35
# BB#29:                                #   in Loop: Header=BB5_25 Depth=2
	leaq	-4(%r13,%rbp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB5_35
# BB#30:                                #   in Loop: Header=BB5_25 Depth=2
	leaq	(%r13,%rbp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB5_35
# BB#31:                                #   in Loop: Header=BB5_25 Depth=2
	leaq	4(%r13,%rbp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB5_35
# BB#32:                                #   in Loop: Header=BB5_25 Depth=2
	leaq	8(%r13,%rbp), %rsi
	movl	$4, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$4, %eax
	jne	.LBB5_35
# BB#33:                                #   in Loop: Header=BB5_25 Depth=2
	incq	%r15
	movq	16(%rsp), %r13          # 8-byte Reload
	movzwl	(%r13), %eax
	cmpq	%rax, %r15
	jge	.LBB5_99
# BB#34:                                # %..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB5_25 Depth=2
	movq	8(%r13), %r13
	leaq	12(%r13,%rbp), %rsi
	addq	$24, %rbp
	movl	$1, %edx
	movl	%ebx, %edi
	callq	cli_readn
	cmpl	$1, %eax
	je	.LBB5_25
.LBB5_35:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rbp), %rdi
	callq	free
	jmp	.LBB5_76
.LBB5_36:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.24, %edi
.LBB5_37:                               # %.backedge
                                        #   in Loop: Header=BB5_100 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB5_85
.LBB5_38:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$1, %edx
	movl	%ebx, %edi
	leaq	12(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB5_71
# BB#39:                                #   in Loop: Header=BB5_100 Depth=1
	movzbl	12(%rsp), %eax
	imulq	$14, %rax, %rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB5_82
# BB#40:                                #   in Loop: Header=BB5_100 Depth=1
	movzbl	12(%rsp), %esi
	movl	$.L.str.69, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %edx
	movl	%ebx, %edi
	leaq	12(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB5_83
# BB#41:                                #   in Loop: Header=BB5_100 Depth=1
	movb	12(%rsp), %al
	testb	%al, %al
	je	.LBB5_91
.LBB5_42:                               # %.thread.i91
                                        #   in Loop: Header=BB5_100 Depth=1
	movzbl	%al, %eax
	leaq	1(,%rax,4), %rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB5_96
# BB#43:                                # %.thread._crit_edge.i
                                        #   in Loop: Header=BB5_100 Depth=1
	movzbl	12(%rsp), %esi
.LBB5_44:                               #   in Loop: Header=BB5_100 Depth=1
	xorl	%r15d, %r15d
	movl	$.L.str.73, %edi
	jmp	.LBB5_37
.LBB5_45:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$2, %edx
	movl	%ebx, %edi
	leaq	12(%rsp), %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB5_72
# BB#46:                                #   in Loop: Header=BB5_100 Depth=1
	movzwl	12(%rsp), %esi
	movl	$.L.str.75, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movzwl	12(%rsp), %eax
	testq	%rax, %rax
	je	.LBB5_48
# BB#47:                                #   in Loop: Header=BB5_100 Depth=1
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rsi
	movl	$1, %r15d
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB5_85
.LBB5_48:                               #   in Loop: Header=BB5_100 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB5_85
.LBB5_49:                               #   in Loop: Header=BB5_100 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	movq	%rax, %rbp
	movl	$2, %edx
	movl	%ebx, %edi
	leaq	12(%rsp), %rsi
	callq	cli_readn
	cmpl	$2, %eax
	movq	%r13, 16(%rsp)          # 8-byte Spill
	jne	.LBB5_70
# BB#50:                                #   in Loop: Header=BB5_100 Depth=1
	movzwl	12(%rsp), %eax
	xorl	%r15d, %r15d
	cmpl	$65535, %eax            # imm = 0xFFFF
	movl	$0, %ecx
	jne	.LBB5_53
# BB#51:                                #   in Loop: Header=BB5_100 Depth=1
	movl	$2, %edx
	movl	%ebx, %edi
	leaq	12(%rsp), %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB5_70
# BB#52:                                # %._crit_edge.i
                                        #   in Loop: Header=BB5_100 Depth=1
	movl	$1, %ecx
	movw	12(%rsp), %ax
.LBB5_53:                               #   in Loop: Header=BB5_100 Depth=1
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	movswl	%ax, %esi
	movl	$.L.str.77, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movswq	12(%rsp), %r13
	addq	%rbp, %r13
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	%r13, %rax
	leaq	11(%rsp), %rbp
	jge	.LBB5_69
# BB#54:                                # %.lr.ph.i96
                                        #   in Loop: Header=BB5_100 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB5_66
.LBB5_55:                               # %.lr.ph.split.i
                                        #   Parent Loop BB5_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %edx
	movl	%ebx, %edi
	leaq	15(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB5_88
# BB#56:                                #   in Loop: Header=BB5_55 Depth=2
	movzbl	15(%rsp), %eax
	leaq	3(%rax,%rax), %rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB5_89
# BB#57:                                #   in Loop: Header=BB5_55 Depth=2
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	%r13, %rax
	jl	.LBB5_55
	jmp	.LBB5_69
.LBB5_58:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$2, %edx
	movl	%ebx, %edi
	leaq	12(%rsp), %rsi
	callq	cli_readn
	cmpl	$2, %eax
	jne	.LBB5_73
# BB#59:                                #   in Loop: Header=BB5_100 Depth=1
	movzwl	12(%rsp), %esi
	xorl	%r15d, %r15d
	movl	$.L.str.80, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpw	$0, 12(%rsp)
	je	.LBB5_85
# BB#60:                                # %.lr.ph.i97.preheader
                                        #   in Loop: Header=BB5_100 Depth=1
	xorl	%ebp, %ebp
.LBB5_61:                               # %.lr.ph.i97
                                        #   Parent Loop BB5_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$2, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB5_81
# BB#62:                                #   in Loop: Header=BB5_61 Depth=2
	movl	$1, %edx
	movl	%ebx, %edi
	leaq	15(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB5_81
# BB#63:                                #   in Loop: Header=BB5_61 Depth=2
	movzbl	15(%rsp), %esi
	incq	%rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB5_81
# BB#64:                                #   in Loop: Header=BB5_61 Depth=2
	incl	%ebp
	cmpw	12(%rsp), %bp
	jb	.LBB5_61
# BB#65:                                #   in Loop: Header=BB5_100 Depth=1
	xorl	%r15d, %r15d
	leaq	11(%rsp), %rbp
	jmp	.LBB5_85
.LBB5_66:                               # %.lr.ph.split.us.i
                                        #   Parent Loop BB5_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %edx
	movl	%ebx, %edi
	leaq	15(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB5_88
# BB#67:                                #   in Loop: Header=BB5_66 Depth=2
	movzbl	15(%rsp), %esi
	addq	$2, %rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB5_89
# BB#68:                                #   in Loop: Header=BB5_66 Depth=2
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	%r13, %rax
	jl	.LBB5_66
.LBB5_69:                               # %wm_skip_macro_extnames.exit
                                        #   in Loop: Header=BB5_100 Depth=1
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB5_85
.LBB5_70:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.76, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %r15d
	leaq	11(%rsp), %rbp
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB5_85
.LBB5_71:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.67, %edi
	jmp	.LBB5_84
.LBB5_72:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.74, %edi
	jmp	.LBB5_74
.LBB5_73:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.79, %edi
.LBB5_74:                               # %.backedge
                                        #   in Loop: Header=BB5_100 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %r15d
	jmp	.LBB5_85
.LBB5_75:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB5_76:                               # %wm_read_macro_info.exit
                                        #   in Loop: Header=BB5_100 Depth=1
	movq	%rbp, %rdi
.LBB5_77:                               # %wm_read_macro_info.exit
                                        #   in Loop: Header=BB5_100 Depth=1
	callq	free
.LBB5_78:                               # %wm_read_macro_info.exit
                                        #   in Loop: Header=BB5_100 Depth=1
	xorl	%r13d, %r13d
	xorl	%eax, %eax
.LBB5_79:                               # %wm_read_macro_info.exit
                                        #   in Loop: Header=BB5_100 Depth=1
	leaq	11(%rsp), %rbp
.LBB5_80:                               # %wm_read_macro_info.exit
                                        #   in Loop: Header=BB5_100 Depth=1
	xorl	%r15d, %r15d
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	sete	%r15b
	jmp	.LBB5_85
.LBB5_81:                               # %.critedge.i98
                                        #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.81, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %r15d
	leaq	11(%rsp), %rbp
	jmp	.LBB5_85
.LBB5_82:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.68, %edi
	jmp	.LBB5_84
.LBB5_83:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.70, %edi
.LBB5_84:                               # %.backedge
                                        #   in Loop: Header=BB5_100 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB5_85:                               # %.backedge
                                        #   in Loop: Header=BB5_100 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	testl	%r15d, %r15d
	jne	.LBB5_105
# BB#86:                                # %.backedge
                                        #   in Loop: Header=BB5_100 Depth=1
	cmpq	%r12, %rax
	jl	.LBB5_100
	jmp	.LBB5_105
.LBB5_87:                               #   in Loop: Header=BB5_100 Depth=1
	movq	%rdi, %rax
	movq	%rdi, %r13
	jmp	.LBB5_80
.LBB5_88:                               # %.critedge.i
                                        #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.76, %edi
	jmp	.LBB5_90
.LBB5_89:                               # %.critedge21.i
                                        #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.78, %edi
.LBB5_90:                               # %wm_skip_macro_extnames.exit
                                        #   in Loop: Header=BB5_100 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %r15d
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB5_85
.LBB5_91:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$1, %edx
	movl	%ebx, %edi
	leaq	12(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB5_97
# BB#92:                                #   in Loop: Header=BB5_100 Depth=1
	cmpb	$2, 12(%rsp)
	jne	.LBB5_98
# BB#93:                                #   in Loop: Header=BB5_100 Depth=1
	movl	$1, %edx
	movl	%ebx, %edi
	leaq	12(%rsp), %rsi
	callq	cli_readn
	cmpl	$1, %eax
	jne	.LBB5_97
# BB#94:                                #   in Loop: Header=BB5_100 Depth=1
	movb	12(%rsp), %al
	testb	%al, %al
	jne	.LBB5_42
# BB#95:                                #   in Loop: Header=BB5_100 Depth=1
	xorl	%esi, %esi
	jmp	.LBB5_44
.LBB5_96:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.72, %edi
	jmp	.LBB5_84
.LBB5_97:                               #   in Loop: Header=BB5_100 Depth=1
	movl	$.L.str.71, %edi
	jmp	.LBB5_84
.LBB5_98:                               #   in Loop: Header=BB5_100 Depth=1
	movq	$-1, %rsi
	movl	$1, %edx
	movl	%ebx, %edi
	callq	lseek
	xorl	%r15d, %r15d
	jmp	.LBB5_85
.LBB5_99:                               #   in Loop: Header=BB5_100 Depth=1
	movq	%r13, %rax
	jmp	.LBB5_79
.LBB5_100:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_61 Depth 2
                                        #     Child Loop BB5_55 Depth 2
                                        #     Child Loop BB5_66 Depth 2
                                        #     Child Loop BB5_25 Depth 2
	movl	$1, %edx
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	cli_readn
	cmpl	$1, %eax
	je	.LBB5_15
# BB#101:
	xorl	%ebp, %ebp
	movl	$.L.str.23, %edi
	jmp	.LBB5_104
.LBB5_102:
	xorl	%ebp, %ebp
	movl	$.L.str.21, %edi
	jmp	.LBB5_104
.LBB5_103:
	xorl	%ebp, %ebp
	movl	$.L.str.22, %edi
.LBB5_104:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebx, %edi
	callq	close
	jmp	.LBB5_14
.LBB5_105:                              # %._crit_edge
	movl	%ebx, %edi
	callq	close
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB5_13
# BB#106:
	movl	$48, %edi
	callq	cli_malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB5_115
# BB#107:
	movq	24(%rsp), %rbx          # 8-byte Reload
	movzwl	(%rbx), %edi
	shlq	$3, %rdi
	callq	cli_malloc
	movq	%rax, 8(%rbp)
	testq	%rax, %rax
	je	.LBB5_121
# BB#108:
	movq	%r14, %rdi
	callq	cli_strdup
	movq	%rax, 40(%rbp)
	movzwl	(%rbx), %edi
	shlq	$2, %rdi
	callq	cli_malloc
	movq	%rax, 16(%rbp)
	testq	%rax, %rax
	je	.LBB5_116
# BB#109:
	movzwl	(%rbx), %edi
	shlq	$2, %rdi
	callq	cli_malloc
	movq	%rax, 24(%rbp)
	testq	%rax, %rax
	je	.LBB5_120
# BB#110:
	movzwl	(%rbx), %edi
	callq	cli_malloc
	movq	%rax, 32(%rbp)
	testq	%rax, %rax
	je	.LBB5_119
# BB#111:
	movzwl	(%rbx), %eax
	testl	%eax, %eax
	movl	%eax, (%rbp)
	movq	%rbx, %rsi
	je	.LBB5_122
# BB#112:                               # %.lr.ph
	xorl	%ebx, %ebx
	movl	$5, %r14d
.LBB5_113:                              # =>This Inner Loop Header: Depth=1
	movl	$.L.str.25, %edi
	callq	cli_strdup
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	8(%rbp), %rcx
	movq	%rax, (%rcx,%rbx,8)
	movq	8(%rsi), %rax
	movl	(%rax,%r14,4), %ecx
	movq	16(%rbp), %rdx
	movl	%ecx, (%rdx,%rbx,4)
	movl	-8(%rax,%r14,4), %ecx
	movq	24(%rbp), %rdx
	movl	%ecx, (%rdx,%rbx,4)
	movzbl	-19(%rax,%r14,4), %eax
	movq	32(%rbp), %rcx
	movb	%al, (%rcx,%rbx)
	incq	%rbx
	movzwl	(%rsi), %eax
	addq	$6, %r14
	cmpq	%rax, %rbx
	jl	.LBB5_113
	jmp	.LBB5_122
.LBB5_115:
	movq	24(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB5_122
.LBB5_116:
	movq	8(%rbp), %rdi
	callq	free
	movq	40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_118
# BB#117:
	callq	free
.LBB5_118:
	movq	%rbp, %rdi
	callq	free
	xorl	%ebp, %ebp
	movq	24(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB5_122
.LBB5_119:
	movq	24(%rbp), %rdi
	callq	free
.LBB5_120:
	movq	16(%rbp), %rdi
	callq	free
	movq	8(%rbp), %rdi
	callq	free
	movq	40(%rbp), %rdi
	callq	free
.LBB5_121:
	movq	%rbp, %rdi
	callq	free
	xorl	%ebp, %ebp
	movq	%rbx, %rsi
.LBB5_122:                              # %wm_free_macro_info.exit100
	movq	8(%rsi), %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	jmp	.LBB5_14
.Lfunc_end5:
	.size	wm_dir_read, .Lfunc_end5-wm_dir_read
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_17
	.quad	.LBB5_36
	.quad	.LBB5_38
	.quad	.LBB5_36
	.quad	.LBB5_45
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_49
	.quad	.LBB5_58
	.quad	.LBB5_85
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_36
	.quad	.LBB5_85

	.text
	.globl	wm_decrypt_macro
	.p2align	4, 0x90
	.type	wm_decrypt_macro,@function
wm_decrypt_macro:                       # @wm_decrypt_macro
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 64
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movl	%edi, %r13d
	movl	%esi, %ebp
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	movq	%rbp, %rsi
	callq	lseek
	cmpq	%rbp, %rax
	jne	.LBB6_15
# BB#1:
	movl	%r15d, %r12d
	movq	%r12, %rdi
	callq	cli_malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB6_2
# BB#3:
	movl	%r13d, %edi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	callq	cli_readn
	cmpl	%r15d, %eax
	jne	.LBB6_4
# BB#5:
	testb	%r14b, %r14b
	je	.LBB6_15
# BB#6:
	testl	%r15d, %r15d
	je	.LBB6_15
# BB#7:                                 # %.lr.ph.preheader
	cmpl	$32, %r15d
	jae	.LBB6_9
# BB#8:
	xorl	%eax, %eax
	jmp	.LBB6_14
.LBB6_2:
	xorl	%ebx, %ebx
	jmp	.LBB6_15
.LBB6_4:
	movq	%rbx, %rdi
	callq	free
	xorl	%ebx, %ebx
	jmp	.LBB6_15
.LBB6_9:                                # %min.iters.checked
	andl	$31, %r15d
	movq	%r12, %rax
	subq	%r15, %rax
	je	.LBB6_10
# BB#11:                                # %vector.ph
	movzbl	%r14b, %ecx
	movd	%ecx, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_12:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rbx,%rcx), %xmm1
	movdqu	16(%rbx,%rcx), %xmm2
	pxor	%xmm0, %xmm1
	pxor	%xmm0, %xmm2
	movdqu	%xmm1, (%rbx,%rcx)
	movdqu	%xmm2, 16(%rbx,%rcx)
	addq	$32, %rcx
	cmpq	%rcx, %rax
	jne	.LBB6_12
# BB#13:                                # %middle.block
	testl	%r15d, %r15d
	jne	.LBB6_14
	jmp	.LBB6_15
.LBB6_10:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	xorb	%r14b, (%rbx,%rax)
	incq	%rax
	cmpq	%rax, %r12
	jne	.LBB6_14
.LBB6_15:                               # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	wm_decrypt_macro, .Lfunc_end6-wm_decrypt_macro
	.cfi_endproc

	.type	vba56_dir_read.vba56_signature,@object # @vba56_dir_read.vba56_signature
	.section	.rodata,"a",@progbits
vba56_dir_read.vba56_signature:
	.ascii	"\314a"
	.size	vba56_dir_read.vba56_signature, 2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in vba56_dir_read()\n"
	.size	.L.str, 21

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%s/_VBA_PROJECT"
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Can't open %s\n"
	.size	.L.str.2, 15

	.type	vba_version,@object     # @vba_version
	.section	.rodata,"a",@progbits
	.p2align	4
vba_version:
	.ascii	"^\000\000\001"
	.zero	4
	.quad	.L.str.26
	.long	0                       # 0x0
	.zero	4
	.ascii	"_\000\000\001"
	.zero	4
	.quad	.L.str.27
	.long	0                       # 0x0
	.zero	4
	.ascii	"e\000\000\001"
	.zero	4
	.quad	.L.str.28
	.long	0                       # 0x0
	.zero	4
	.ascii	"k\000\000\001"
	.zero	4
	.quad	.L.str.29
	.long	0                       # 0x0
	.zero	4
	.ascii	"m\000\000\001"
	.zero	4
	.quad	.L.str.30
	.long	0                       # 0x0
	.zero	4
	.ascii	"o\000\000\001"
	.zero	4
	.quad	.L.str.30
	.long	0                       # 0x0
	.zero	4
	.ascii	"p\000\000\001"
	.zero	4
	.quad	.L.str.31
	.long	0                       # 0x0
	.zero	4
	.ascii	"s\000\000\001"
	.zero	4
	.quad	.L.str.32
	.long	0                       # 0x0
	.zero	4
	.ascii	"v\000\000\001"
	.zero	4
	.quad	.L.str.33
	.long	0                       # 0x0
	.zero	4
	.ascii	"y\000\000\001"
	.zero	4
	.quad	.L.str.33
	.long	0                       # 0x0
	.zero	4
	.ascii	"`\000\000\016"
	.zero	4
	.quad	.L.str.34
	.long	1                       # 0x1
	.zero	4
	.ascii	"b\000\000\016"
	.zero	4
	.quad	.L.str.35
	.long	1                       # 0x1
	.zero	4
	.ascii	"c\000\000\016"
	.zero	4
	.quad	.L.str.36
	.long	1                       # 0x1
	.zero	4
	.ascii	"d\000\000\016"
	.zero	4
	.quad	.L.str.37
	.long	1                       # 0x1
	.zero	4
	.size	vba_version, 336

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"Unknown VBA version signature %x %x %x %x\n"
	.size	.L.str.3, 43

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Guessing little-endian\n"
	.size	.L.str.4, 24

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Guessing big-endian\n"
	.size	.L.str.5, 21

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Unable to guess VBA type\n"
	.size	.L.str.6, 26

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"VBA Project: %s\n"
	.size	.L.str.7, 17

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\nVBA Record count: %d\n"
	.size	.L.str.8, 23

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\nVBA Record count too big"
	.size	.L.str.9, 26

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"zero name length\n"
	.size	.L.str.10, 18

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"cli_malloc failed\n"
	.size	.L.str.11, 19

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"read name failed\n"
	.size	.L.str.12, 18

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"clamav-%.10d"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"project name: %s, "
	.size	.L.str.14, 19

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"offset:%u\n"
	.size	.L.str.15, 11

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s/_clam_ole_object"
	.size	.L.str.16, 20

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%s/PowerPoint Document"
	.size	.L.str.17, 23

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Open PowerPoint Document failed\n"
	.size	.L.str.18, 33

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"%s/WordDocument"
	.size	.L.str.19, 16

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Open WordDocument failed\n"
	.size	.L.str.20, 26

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"No macros detected\n"
	.size	.L.str.21, 20

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"lseek macro_offset failed\n"
	.size	.L.str.22, 27

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"read macro_info failed\n"
	.size	.L.str.23, 24

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"unknown type: 0x%x\n"
	.size	.L.str.24, 20

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"WordDocument"
	.size	.L.str.25, 13

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Office 97"
	.size	.L.str.26, 10

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Office 97 SR1"
	.size	.L.str.27, 14

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Office 2000 alpha?"
	.size	.L.str.28, 19

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Office 2000 beta?"
	.size	.L.str.29, 18

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Office 2000"
	.size	.L.str.30, 12

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Office XP beta 1/2"
	.size	.L.str.31, 19

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Office XP"
	.size	.L.str.32, 10

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Office 2003"
	.size	.L.str.33, 12

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"MacOffice 98"
	.size	.L.str.34, 13

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"MacOffice 2001"
	.size	.L.str.35, 15

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"MacOffice X"
	.size	.L.str.36, 12

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"MacOffice 2004"
	.size	.L.str.37, 15

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"read name failed - rewinding\n"
	.size	.L.str.38, 30

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"length: %d, name: %s\n"
	.size	.L.str.39, 22

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"length: %d, name: [null]\n"
	.size	.L.str.40, 26

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"*\\"
	.size	.L.str.41, 3

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"GCHD"
	.size	.L.str.42, 5

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"offset: %u\n"
	.size	.L.str.43, 12

	.type	vba56_test_middle.middle1_str,@object # @vba56_test_middle.middle1_str
	.section	.rodata,"a",@progbits
	.p2align	4
vba56_test_middle.middle1_str:
	.asciz	"\000\001\rE.\341\340\217\020\032\205.\002`\214M\013\264\000"
	.size	vba56_test_middle.middle1_str, 20

	.type	vba56_test_middle.middle2_str,@object # @vba56_test_middle.middle2_str
	.p2align	4
vba56_test_middle.middle2_str:
	.asciz	"\000\000\341.E\r\217\340\032\020\205.\002`\214M\013\264\000"
	.size	vba56_test_middle.middle2_str, 20

	.type	.L.str.44,@object       # @.str.44
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.44:
	.asciz	"middle not found\n"
	.size	.L.str.44, 18

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"middle found\n"
	.size	.L.str.45, 14

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"get_unicode_name: odd number of bytes %d\n"
	.size	.L.str.46, 42

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"ScanOLE2 -> Can't create temporary directory %s\n"
	.size	.L.str.47, 49

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"read ole_id failed\n"
	.size	.L.str.48, 20

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"OleID: %d, length: %d\n"
	.size	.L.str.49, 23

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"ppt_unlzw failed\n"
	.size	.L.str.50, 18

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"read ppt_current_user failed\n"
	.size	.L.str.51, 30

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"Atom Hdr:\n"
	.size	.L.str.52, 11

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"  Version: 0x%.2x\n"
	.size	.L.str.53, 19

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"  Instance: 0x%.4x\n"
	.size	.L.str.54, 20

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"  Type: 0x%.4x\n"
	.size	.L.str.55, 16

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"  Length: 0x%.8x\n"
	.size	.L.str.56, 18

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"%s/ppt%.8lx.doc"
	.size	.L.str.57, 16

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"ppt_unlzw Open outfile failed\n"
	.size	.L.str.58, 31

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"1.2.8"
	.size	.L.str.59, 6

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	" ppt_unlzw !Z_OK: %d\n"
	.size	.L.str.60, 22

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"lseek wm_fib failed\n"
	.size	.L.str.61, 21

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"read wm_fib failed\n"
	.size	.L.str.62, 20

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"macro offset: 0x%.4x\n"
	.size	.L.str.63, 22

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"macro len: 0x%.4x\n\n"
	.size	.L.str.64, 20

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"macro count: %d\n"
	.size	.L.str.65, 17

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"read macro_entry failed\n"
	.size	.L.str.66, 25

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"read oxo3 record1 failed\n"
	.size	.L.str.67, 26

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"lseek oxo3 record1 failed\n"
	.size	.L.str.68, 27

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"oxo3 records1: %d\n"
	.size	.L.str.69, 19

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"read oxo3 record2 failed\n"
	.size	.L.str.70, 26

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"read oxo3 failed\n"
	.size	.L.str.71, 18

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"lseek oxo3 failed\n"
	.size	.L.str.72, 19

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"oxo3 records2: %d\n"
	.size	.L.str.73, 19

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"read menu_info failed\n"
	.size	.L.str.74, 23

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"menu_info count: %d\n"
	.size	.L.str.75, 21

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"read macro_extnames failed\n"
	.size	.L.str.76, 28

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"ext names size: 0x%x\n"
	.size	.L.str.77, 22

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"read macro_extnames failed to seek\n"
	.size	.L.str.78, 36

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"read macro_intnames failed\n"
	.size	.L.str.79, 28

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"int names count: %u\n"
	.size	.L.str.80, 21

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"skip_macro_intnames failed\n"
	.size	.L.str.81, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
