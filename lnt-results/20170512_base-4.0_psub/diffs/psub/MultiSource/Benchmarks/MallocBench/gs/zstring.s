	.text
	.file	"zstring.bc"
	.globl	zstring
	.p2align	4, 0x90
	.type	zstring,@function
zstring:                                # @zstring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	andl	$252, %eax
	movl	$-20, %ebp
	cmpl	$20, %eax
	jne	.LBB0_5
# BB#1:
	movq	(%rbx), %r14
	movq	%r14, %rax
	shrq	$32, %rax
	movl	$-15, %ebp
	jne	.LBB0_5
# BB#2:
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	%r14d, %edi
	callq	alloc
	testq	%rax, %rax
	je	.LBB0_3
# BB#4:
	movq	%rax, (%rbx)
	movw	$822, 8(%rbx)           # imm = 0x336
	movw	%r14w, 10(%rbx)
	movl	%r14d, %edx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	memset
	jmp	.LBB0_5
.LBB0_3:
	movl	$-25, %ebp
.LBB0_5:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	zstring, .Lfunc_end0-zstring
	.cfi_endproc

	.globl	zanchorsearch
	.p2align	4, 0x90
	.type	zanchorsearch,@function
zanchorsearch:                          # @zanchorsearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 64
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movzwl	-8(%rbp), %ebx
	movl	%ebx, %eax
	andl	$252, %eax
	movl	$-20, %r12d
	cmpl	$52, %eax
	jne	.LBB1_12
# BB#1:
	testb	$2, %bh
	jne	.LBB1_3
# BB#2:
	movl	$-7, %r12d
	jmp	.LBB1_12
.LBB1_3:
	movzwl	8(%rbp), %eax
	movl	%eax, %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	jne	.LBB1_12
# BB#4:
	testb	$2, %ah
	movl	$-7, %r12d
	je	.LBB1_12
# BB#5:
	movw	10(%rbp), %r14w
	movw	-6(%rbp), %r13w
	xorl	%r12d, %r12d
	cmpw	%r14w, %r13w
	jae	.LBB1_7
# BB#6:
	movq	%rbp, %rax
	xorl	%ecx, %ecx
	jmp	.LBB1_9
.LBB1_7:
	movzwl	%r14w, %edx
	movq	-16(%rbp), %r15
	movq	(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rdx, (%rsp)            # 8-byte Spill
	callq	memcmp
	testl	%eax, %eax
	je	.LBB1_10
# BB#8:
	xorl	%ecx, %ecx
	movq	%rbp, %rax
	jmp	.LBB1_9
.LBB1_10:
	leaq	-16(%rbp), %rax
	orl	$32768, %ebx            # imm = 0x8000
	movw	%bx, -8(%rbp)
	movups	(%rax), %xmm0
	movups	%xmm0, (%rbp)
	movw	%r14w, 10(%rbp)
	addq	(%rsp), %r15            # 8-byte Folded Reload
	movq	%r15, -16(%rbp)
	subl	%r14d, %r13d
	movw	%r13w, -6(%rbp)
	leaq	16(%rbp), %rax
	movq	%rax, osp(%rip)
	movw	$1, %cx
	cmpq	ostop(%rip), %rax
	jbe	.LBB1_9
# BB#11:
	movq	%rbp, osp(%rip)
	movl	$-16, %r12d
	jmp	.LBB1_12
.LBB1_9:
	movw	%cx, (%rax)
	movw	$4, 8(%rax)
.LBB1_12:
	movl	%r12d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	zanchorsearch, .Lfunc_end1-zanchorsearch
	.cfi_endproc

	.globl	zsearch
	.p2align	4, 0x90
	.type	zsearch,@function
zsearch:                                # @zsearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 80
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movzwl	-8(%r15), %ebx
	movl	%ebx, %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$52, %ecx
	jne	.LBB2_16
# BB#1:
	testb	$2, %bh
	jne	.LBB2_3
# BB#2:
	movl	$-7, %eax
	jmp	.LBB2_16
.LBB2_3:
	movzwl	8(%r15), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB2_16
# BB#4:
	testb	$2, %ch
	movl	$-7, %eax
	je	.LBB2_16
# BB#5:
	movzwl	10(%r15), %r14d
	movzwl	-6(%r15), %eax
	cmpw	%r14w, %ax
	jb	.LBB2_14
# BB#6:
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	leaq	-16(%r15), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	-16(%r15), %rbx
	movq	(%r15), %r12
	movl	%eax, %ebp
	subl	%r14d, %ebp
	movl	%r14d, %r13d
	decl	%r13d
	subl	%eax, %r13d
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB2_8
# BB#13:                                #   in Loop: Header=BB2_7 Depth=1
	incq	%rbx
	decl	%ebp
	incl	%r13d
	jne	.LBB2_7
.LBB2_14:
	movw	$0, (%r15)
	movw	$4, 8(%r15)
.LBB2_15:
	xorl	%eax, %eax
.LBB2_16:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_8:
	movl	12(%rsp), %eax          # 4-byte Reload
	orl	$32768, %eax            # imm = 0x8000
	movw	%ax, 8(%r15)
	movq	%rbx, (%r15)
	leaq	16(%r15), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB2_10
# BB#9:
	movq	%r15, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB2_16
.LBB2_10:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movups	(%rcx), %xmm0
	movups	%xmm0, (%rax)
	movl	16(%r15), %ecx
	movl	%ebx, %edx
	subl	%ecx, %edx
	movw	%dx, 26(%r15)
	leaq	(%r14,%rbx), %rcx
	movq	%rcx, -16(%r15)
	movw	%bp, -6(%r15)
	leaq	32(%r15), %rcx
	movq	%rcx, osp(%rip)
	cmpq	ostop(%rip), %rcx
	jbe	.LBB2_12
# BB#11:
	movq	%rax, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB2_16
.LBB2_12:
	movw	$1, 32(%r15)
	movw	$4, 40(%r15)
	jmp	.LBB2_15
.Lfunc_end2:
	.size	zsearch, .Lfunc_end2-zsearch
	.cfi_endproc

	.globl	ztoken
	.p2align	4, 0x90
	.type	ztoken,@function
ztoken:                                 # @ztoken
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 32
	subq	$144, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 176
.Lcfi36:
	.cfi_offset %rbx, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movw	8(%rbx), %ax
	movl	%eax, %ecx
	shrb	$2, %cl
	cmpb	$13, %cl
	je	.LBB3_3
# BB#1:
	movl	$-20, %ebp
	cmpb	$3, %cl
	jne	.LBB3_10
# BB#2:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	ztoken_file
	movl	%eax, %ebp
	jmp	.LBB3_10
.LBB3_3:
	movl	$-7, %ebp
	testb	$2, %ah
	je	.LBB3_10
# BB#4:
	movq	(%rbx), %rsi
	movzwl	10(%rbx), %edx
	leaq	24(%rsp), %r14
	movq	%r14, %rdi
	callq	sread_string
	xorl	%ebp, %ebp
	leaq	8(%rsp), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	scan_token
	cmpl	$1, %eax
	je	.LBB3_9
# BB#5:
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.LBB3_10
# BB#6:
	movq	24(%rsp), %rax
	incq	%rax
	subq	40(%rsp), %rax
	addq	56(%rsp), %rax
	movl	%eax, %ecx
	addq	%rcx, (%rbx)
	subw	%ax, 10(%rbx)
	orb	$-128, 9(%rbx)
	leaq	32(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB3_8
# BB#7:
	movq	%rbx, osp(%rip)
	movl	$-16, %ebp
	jmp	.LBB3_10
.LBB3_9:
	movw	$0, (%rbx)
	movw	$4, 8(%rbx)
	jmp	.LBB3_10
.LBB3_8:
	movups	8(%rsp), %xmm0
	movups	%xmm0, 16(%rbx)
	movw	$1, 32(%rbx)
	movw	$4, 40(%rbx)
	xorl	%ebp, %ebp
.LBB3_10:
	movl	%ebp, %eax
	addq	$144, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	ztoken, .Lfunc_end3-ztoken
	.cfi_endproc

	.globl	zstring_op_init
	.p2align	4, 0x90
	.type	zstring_op_init,@function
zstring_op_init:                        # @zstring_op_init
	.cfi_startproc
# BB#0:
	movl	$zstring_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end4:
	.size	zstring_op_init, .Lfunc_end4-zstring_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"string"
	.size	.L.str, 7

	.type	zstring_op_init.my_defs,@object # @zstring_op_init.my_defs
	.data
	.p2align	4
zstring_op_init.my_defs:
	.quad	.L.str.1
	.quad	zanchorsearch
	.quad	.L.str.2
	.quad	zsearch
	.quad	.L.str.3
	.quad	zstring
	.quad	.L.str.4
	.quad	ztoken
	.zero	16
	.size	zstring_op_init.my_defs, 80

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"2anchorsearch"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"2search"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"1string"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"1token"
	.size	.L.str.4, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
