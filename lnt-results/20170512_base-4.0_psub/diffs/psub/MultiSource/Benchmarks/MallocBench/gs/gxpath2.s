	.text
	.file	"gxpath2.bc"
	.globl	gx_path_current_point
	.p2align	4, 0x90
	.type	gx_path_current_point,@function
gx_path_current_point:                  # @gx_path_current_point
	.cfi_startproc
# BB#0:
	cmpb	$0, 136(%rdi)
	je	.LBB0_1
# BB#2:
	movups	120(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	xorl	%eax, %eax
	retq
.LBB0_1:
	movl	$-14, %eax
	retq
.Lfunc_end0:
	.size	gx_path_current_point, .Lfunc_end0-gx_path_current_point
	.cfi_endproc

	.globl	gx_path_bbox
	.p2align	4, 0x90
	.type	gx_path_bbox,@function
gx_path_bbox:                           # @gx_path_bbox
	.cfi_startproc
# BB#0:
	movq	88(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB1_1
# BB#4:
	movq	48(%rdi), %rax
	movq	96(%rdi), %r8
	cmpq	40(%r8), %rax
	je	.LBB1_5
# BB#6:
	testq	%rax, %rax
	je	.LBB1_7
# BB#8:                                 # %.preheader
	movups	16(%rdi), %xmm0
	movups	32(%rdi), %xmm1
	movaps	%xmm1, -24(%rsp)
	movaps	%xmm0, -40(%rsp)
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_9
	jmp	.LBB1_31
.LBB1_1:
	cmpb	$0, 136(%rdi)
	je	.LBB1_2
# BB#3:
	movups	120(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	movups	120(%rdi), %xmm0
	movups	%xmm0, 16(%rsi)
	xorl	%eax, %eax
	retq
.LBB1_5:
	movups	16(%rdi), %xmm0
	movups	32(%rdi), %xmm1
	jmp	.LBB1_32
.LBB1_7:                                # %.preheader.thread
	movq	24(%rcx), %rax
	movq	%rax, -24(%rsp)
	movq	%rax, -40(%rsp)
	movq	32(%rcx), %rax
	movq	%rax, -16(%rsp)
	movq	%rax, -32(%rsp)
.LBB1_9:                                # %.lr.ph
	leaq	-24(%rsp), %r9
	leaq	-40(%rsp), %r10
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	cmpl	$3, 16(%rcx)
	jne	.LBB1_24
# BB#11:                                #   in Loop: Header=BB1_10 Depth=1
	movq	40(%rcx), %rdx
	cmpq	-40(%rsp), %rdx
	movq	%r10, %rax
	jl	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_10 Depth=1
	cmpq	-24(%rsp), %rdx
	movq	%r9, %rax
	jle	.LBB1_14
.LBB1_13:                               # %.sink.split
                                        #   in Loop: Header=BB1_10 Depth=1
	movq	%rdx, (%rax)
.LBB1_14:                               #   in Loop: Header=BB1_10 Depth=1
	movq	48(%rcx), %rdx
	cmpq	-32(%rsp), %rdx
	movq	%r10, %rax
	jl	.LBB1_16
# BB#15:                                #   in Loop: Header=BB1_10 Depth=1
	cmpq	-16(%rsp), %rdx
	movq	%r9, %rax
	jle	.LBB1_17
.LBB1_16:                               # %.sink.split3
                                        #   in Loop: Header=BB1_10 Depth=1
	movq	%rdx, 8(%rax)
.LBB1_17:                               #   in Loop: Header=BB1_10 Depth=1
	movq	56(%rcx), %rdx
	cmpq	-40(%rsp), %rdx
	movq	%r10, %rax
	jl	.LBB1_19
# BB#18:                                #   in Loop: Header=BB1_10 Depth=1
	cmpq	-24(%rsp), %rdx
	movq	%r9, %rax
	jle	.LBB1_20
.LBB1_19:                               # %.sink.split7
                                        #   in Loop: Header=BB1_10 Depth=1
	movq	%rdx, (%rax)
.LBB1_20:                               #   in Loop: Header=BB1_10 Depth=1
	movq	64(%rcx), %rdx
	cmpq	-32(%rsp), %rdx
	jge	.LBB1_22
# BB#21:                                #   in Loop: Header=BB1_10 Depth=1
	movq	%rdx, -32(%rsp)
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_22:                               #   in Loop: Header=BB1_10 Depth=1
	cmpq	-16(%rsp), %rdx
	jle	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_10 Depth=1
	movq	%rdx, -16(%rsp)
	.p2align	4, 0x90
.LBB1_24:                               #   in Loop: Header=BB1_10 Depth=1
	movq	24(%rcx), %rdx
	cmpq	-40(%rsp), %rdx
	movq	%r10, %rax
	jl	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_10 Depth=1
	cmpq	-24(%rsp), %rdx
	movq	%r9, %rax
	jle	.LBB1_27
.LBB1_26:                               # %.sink.split11
                                        #   in Loop: Header=BB1_10 Depth=1
	movq	%rdx, (%rax)
.LBB1_27:                               #   in Loop: Header=BB1_10 Depth=1
	movq	32(%rcx), %rdx
	cmpq	-32(%rsp), %rdx
	movq	%r10, %rax
	jl	.LBB1_29
# BB#28:                                #   in Loop: Header=BB1_10 Depth=1
	cmpq	-16(%rsp), %rdx
	movq	%r9, %rax
	jle	.LBB1_30
.LBB1_29:                               # %.sink.split15
                                        #   in Loop: Header=BB1_10 Depth=1
	movq	%rdx, 8(%rax)
.LBB1_30:                               #   in Loop: Header=BB1_10 Depth=1
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_10
.LBB1_31:                               # %._crit_edge
	movaps	-40(%rsp), %xmm0
	movaps	-24(%rsp), %xmm1
	movups	%xmm1, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	movq	40(%r8), %rax
	movq	%rax, 48(%rdi)
	movaps	-40(%rsp), %xmm0
	movaps	-24(%rsp), %xmm1
.LBB1_32:                               # %gx_path_current_point.exit60
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	xorl	%eax, %eax
	retq
.LBB1_2:
	movl	$-14, %eax
	retq
.Lfunc_end1:
	.size	gx_path_bbox, .Lfunc_end1-gx_path_bbox
	.cfi_endproc

	.globl	gx_path_has_curves
	.p2align	4, 0x90
	.type	gx_path_has_curves,@function
gx_path_has_curves:                     # @gx_path_has_curves
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$0, 112(%rdi)
	setne	%al
	retq
.Lfunc_end2:
	.size	gx_path_has_curves, .Lfunc_end2-gx_path_has_curves
	.cfi_endproc

	.globl	gx_path_is_void
	.p2align	4, 0x90
	.type	gx_path_is_void,@function
gx_path_is_void:                        # @gx_path_is_void
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$0, 108(%rdi)
	sete	%al
	retq
.Lfunc_end3:
	.size	gx_path_is_void, .Lfunc_end3-gx_path_is_void
	.cfi_endproc

	.globl	gx_path_is_rectangle
	.p2align	4, 0x90
	.type	gx_path_is_rectangle,@function
gx_path_is_rectangle:                   # @gx_path_is_rectangle
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	xorl	%eax, %eax
	cmpl	$1, 104(%rdi)
	jne	.LBB4_13
# BB#1:
	cmpl	$4, 108(%rdi)
	jne	.LBB4_13
# BB#2:
	cmpl	$0, 112(%rdi)
	jne	.LBB4_13
# BB#3:
	movq	88(%rdi), %rdi
	movq	40(%rdi), %rcx
	cmpl	$2, 16(%rcx)
	jne	.LBB4_13
# BB#4:
	movq	8(%rdi), %rcx
	movq	24(%rdi), %rbx
	movq	32(%rdi), %r8
	movq	8(%rcx), %rdx
	movq	24(%rcx), %r11
	movq	8(%rdx), %rdi
	movq	24(%rdx), %r9
	movq	32(%rdx), %r10
	cmpq	%r11, %rbx
	jne	.LBB4_8
# BB#5:
	cmpq	%r10, 32(%rcx)
	jne	.LBB4_8
# BB#6:
	cmpq	24(%rdi), %r9
	jne	.LBB4_8
# BB#7:
	cmpq	%r8, 32(%rdi)
	je	.LBB4_12
.LBB4_8:
	cmpq	24(%rdi), %rbx
	jne	.LBB4_13
# BB#9:
	cmpq	%r10, 32(%rdi)
	jne	.LBB4_13
# BB#10:
	cmpq	%r11, %r9
	jne	.LBB4_13
# BB#11:
	cmpq	%r8, 32(%rcx)
	jne	.LBB4_13
.LBB4_12:                               # %.critedge
	cmpq	%r9, %rbx
	movq	%r9, %rax
	cmovleq	%rbx, %rax
	cmovlq	%r9, %rbx
	movq	%rax, (%rsi)
	movq	%rbx, 16(%rsi)
	cmpq	%r10, %r8
	movq	%r10, %rax
	cmovleq	%r8, %rax
	cmovlq	%r10, %r8
	movq	%rax, 8(%rsi)
	movq	%r8, 24(%rsi)
	movl	$1, %eax
.LBB4_13:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	gx_path_is_rectangle, .Lfunc_end4-gx_path_is_rectangle
	.cfi_endproc

	.globl	gx_cpath_box_for_check
	.p2align	4, 0x90
	.type	gx_cpath_box_for_check,@function
gx_cpath_box_for_check:                 # @gx_cpath_box_for_check
	.cfi_startproc
# BB#0:
	movups	56(%rdi), %xmm0
	movups	72(%rdi), %xmm1
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	gx_cpath_box_for_check, .Lfunc_end5-gx_cpath_box_for_check
	.cfi_endproc

	.globl	gx_cpath_includes_rectangle
	.p2align	4, 0x90
	.type	gx_cpath_includes_rectangle,@function
gx_cpath_includes_rectangle:            # @gx_cpath_includes_rectangle
	.cfi_startproc
# BB#0:
	cmpq	%rcx, %rsi
	movq	56(%rdi), %rax
	jle	.LBB6_1
# BB#5:
	cmpq	%rcx, %rax
	jle	.LBB6_7
# BB#6:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB6_1:
	cmpq	%rsi, %rax
	jle	.LBB6_3
# BB#2:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB6_7:
	cmpq	%rsi, 72(%rdi)
	jge	.LBB6_9
# BB#8:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB6_3:
	cmpq	%rcx, 72(%rdi)
	jge	.LBB6_9
# BB#4:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB6_9:
	cmpq	%r8, %rdx
	movq	64(%rdi), %rax
	jle	.LBB6_10
# BB#13:
	cmpq	%r8, %rax
	jle	.LBB6_15
# BB#14:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB6_10:
	cmpq	%rdx, %rax
	jle	.LBB6_12
# BB#11:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB6_15:
	cmpq	%rdx, 80(%rdi)
	jmp	.LBB6_16
.LBB6_12:
	cmpq	%r8, 80(%rdi)
.LBB6_16:
	setge	%al
	movzbl	%al, %eax
	retq
.Lfunc_end6:
	.size	gx_cpath_includes_rectangle, .Lfunc_end6-gx_cpath_includes_rectangle
	.cfi_endproc

	.globl	gx_path_copy
	.p2align	4, 0x90
	.type	gx_path_copy,@function
gx_path_copy:                           # @gx_path_copy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 208
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	80(%rbx), %rax
	movq	%rax, 144(%rsp)
	movups	64(%rbx), %xmm0
	movaps	%xmm0, 128(%rsp)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	48(%rbx), %xmm3
	movaps	%xmm3, 112(%rsp)
	movaps	%xmm2, 96(%rsp)
	movaps	%xmm1, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movq	88(%rbx), %r13
	movq	128(%rbx), %rax
	movq	%rax, 48(%rsp)
	movups	96(%rbx), %xmm0
	movups	112(%rbx), %xmm1
	movaps	%xmm1, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	movq	136(%rbx), %r12
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	gx_path_init
	testq	%r13, %r13
	je	.LBB7_12
# BB#1:                                 # %.lr.ph.preheader.i
	leaq	96(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r13, %r15
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%r15), %eax
	cmpq	$3, %rax
	ja	.LBB7_11
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB7_2 Depth=1
	jmpq	*.LJTI7_0(,%rax,8)
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=1
	movq	24(%r15), %rsi
	movq	32(%r15), %rdx
	movq	%r14, %rdi
	callq	gx_path_add_point
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_6:                                #   in Loop: Header=BB7_2 Depth=1
	movq	24(%r15), %rsi
	movq	32(%r15), %rdx
	movq	%r14, %rdi
	callq	gx_path_add_line
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_7:                                #   in Loop: Header=BB7_2 Depth=1
	movq	%r14, %rdi
	callq	gx_path_close_subpath
	jmp	.LBB7_8
	.p2align	4, 0x90
.LBB7_5:                                #   in Loop: Header=BB7_2 Depth=1
	movq	40(%r15), %rsi
	movq	48(%r15), %rdx
	movq	56(%r15), %rcx
	movq	64(%r15), %r8
	movq	24(%r15), %r9
	movq	32(%r15), %rax
	movq	%rax, (%rsp)
	movq	%r14, %rdi
	callq	gx_path_add_curve
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=1
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB7_9
.LBB7_11:                               # %.thread.i
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB7_2
.LBB7_12:                               # %._crit_edge.i
	movups	40(%rsp), %xmm0
	movups	%xmm0, 120(%r14)
	xorl	%ebp, %ebp
	jmp	.LBB7_13
.LBB7_9:
	movq	%r14, %rdi
	callq	gx_path_release
	cmpq	%rbx, %r14
	jne	.LBB7_13
# BB#10:
	movq	144(%rsp), %rax
	movq	%rax, 80(%rbx)
	movaps	128(%rsp), %xmm0
	movups	%xmm0, 64(%rbx)
	movaps	64(%rsp), %xmm0
	movaps	80(%rsp), %xmm1
	movaps	96(%rsp), %xmm2
	movaps	112(%rsp), %xmm3
	movups	%xmm3, 48(%rbx)
	movups	%xmm2, 32(%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	%r13, 88(%rbx)
	movq	48(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rax, 32(%rcx)
	movaps	16(%rsp), %xmm0
	movaps	32(%rsp), %xmm1
	movups	%xmm1, 16(%rcx)
	movups	%xmm0, (%rcx)
	movq	%r12, 136(%rbx)
.LBB7_13:                               # %copy_path.exit
	movl	%ebp, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	gx_path_copy, .Lfunc_end7-gx_path_copy
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_4
	.quad	.LBB7_6
	.quad	.LBB7_7
	.quad	.LBB7_5

	.text
	.globl	copy_path
	.p2align	4, 0x90
	.type	copy_path,@function
copy_path:                              # @copy_path
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi21:
	.cfi_def_cfa_offset 224
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	80(%rbx), %rax
	movq	%rax, 160(%rsp)
	movups	64(%rbx), %xmm0
	movaps	%xmm0, 144(%rsp)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	48(%rbx), %xmm3
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movq	88(%rbx), %r13
	movq	128(%rbx), %rax
	movq	%rax, 64(%rsp)
	movups	96(%rbx), %xmm0
	movups	112(%rbx), %xmm1
	movaps	%xmm1, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movq	136(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	gx_path_init
	testq	%r13, %r13
	je	.LBB8_12
# BB#1:                                 # %.lr.ph.preheader
	leaq	96(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r13, %r12
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%r12), %eax
	cmpq	$3, %rax
	ja	.LBB8_11
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_2 Depth=1
	jmpq	*.LJTI8_0(,%rax,8)
.LBB8_4:                                #   in Loop: Header=BB8_2 Depth=1
	movq	24(%r12), %rsi
	movq	32(%r12), %rdx
	movq	%r14, %rdi
	callq	gx_path_add_point
	jmp	.LBB8_8
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=1
	movq	24(%r12), %rsi
	movq	32(%r12), %rdx
	movq	%r14, %rdi
	callq	gx_path_add_line
	jmp	.LBB8_8
	.p2align	4, 0x90
.LBB8_7:                                #   in Loop: Header=BB8_2 Depth=1
	movq	%r14, %rdi
	callq	gx_path_close_subpath
	jmp	.LBB8_8
	.p2align	4, 0x90
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	movq	40(%r12), %rsi
	movq	48(%r12), %rdx
	movq	56(%r12), %rcx
	movq	64(%r12), %r8
	movq	24(%r12), %r9
	movq	32(%r12), %rax
	movq	%rax, (%rsp)
	movq	%r14, %rdi
	callq	*%r15
.LBB8_8:                                #   in Loop: Header=BB8_2 Depth=1
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB8_9
.LBB8_11:                               # %.thread
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.LBB8_2
.LBB8_12:                               # %._crit_edge
	movups	56(%rsp), %xmm0
	movups	%xmm0, 120(%r14)
	xorl	%ebp, %ebp
	jmp	.LBB8_13
.LBB8_9:
	movq	%r14, %rdi
	callq	gx_path_release
	cmpq	%rbx, %r14
	jne	.LBB8_13
# BB#10:
	movq	160(%rsp), %rax
	movq	%rax, 80(%rbx)
	movaps	144(%rsp), %xmm0
	movups	%xmm0, 64(%rbx)
	movaps	80(%rsp), %xmm0
	movaps	96(%rsp), %xmm1
	movaps	112(%rsp), %xmm2
	movaps	128(%rsp), %xmm3
	movups	%xmm3, 48(%rbx)
	movups	%xmm2, 32(%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	%r13, 88(%rbx)
	movq	64(%rsp), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 32(%rcx)
	movaps	32(%rsp), %xmm0
	movaps	48(%rsp), %xmm1
	movups	%xmm1, 16(%rcx)
	movups	%xmm0, (%rcx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 136(%rbx)
.LBB8_13:
	movl	%ebp, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	copy_path, .Lfunc_end8-copy_path
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI8_0:
	.quad	.LBB8_4
	.quad	.LBB8_6
	.quad	.LBB8_7
	.quad	.LBB8_5

	.text
	.globl	gx_path_merge
	.p2align	4, 0x90
	.type	gx_path_merge,@function
gx_path_merge:                          # @gx_path_merge
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -24
.Lcfi32:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	96(%r14), %rax
	testq	%rax, %rax
	je	.LBB9_3
# BB#1:
	movq	96(%rbx), %rcx
	movq	40(%rcx), %rcx
	cmpq	40(%rax), %rcx
	je	.LBB9_3
# BB#2:
	movq	%r14, %rdi
	callq	gx_path_release
.LBB9_3:
	movl	$144, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memcpy
	movb	$1, 138(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	gx_path_merge, .Lfunc_end9-gx_path_merge
	.cfi_endproc

	.globl	gx_path_translate
	.p2align	4, 0x90
	.type	gx_path_translate,@function
gx_path_translate:                      # @gx_path_translate
	.cfi_startproc
# BB#0:
	movdqu	16(%rdi), %xmm1
	movd	%rdx, %xmm2
	movd	%rsi, %xmm0
	punpcklqdq	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 16(%rdi)
	movdqu	32(%rdi), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 32(%rdi)
	movdqu	120(%rdi), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 120(%rdi)
	movq	88(%rdi), %rax
	testq	%rax, %rax
	jne	.LBB10_2
	jmp	.LBB10_5
	.p2align	4, 0x90
.LBB10_4:                               #   in Loop: Header=BB10_2 Depth=1
	movdqu	24(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 24(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.LBB10_5
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$3, 16(%rax)
	jne	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movdqu	40(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 40(%rax)
	movdqu	56(%rax), %xmm1
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 56(%rax)
	jmp	.LBB10_4
.LBB10_5:                               # %._crit_edge
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	gx_path_translate, .Lfunc_end10-gx_path_translate
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4661225614328463360     # double 4096
.LCPI11_1:
	.quad	4601778099247172813     # double 0.45000000000000001
	.text
	.globl	gx_path_flatten
	.p2align	4, 0x90
	.type	gx_path_flatten,@function
gx_path_flatten:                        # @gx_path_flatten
	.cfi_startproc
# BB#0:
	mulsd	.LCPI11_0(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvttss2si	%xmm0, %rax
	movq	%rax, scaled_flat(%rip)
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI11_1(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, scaled_flat_sq(%rip)
	movl	$flatten_curve, %edx
	jmp	copy_path               # TAILCALL
.Lfunc_end11:
	.size	gx_path_flatten, .Lfunc_end11-gx_path_flatten
	.cfi_endproc

	.globl	flatten_curve
	.p2align	4, 0x90
	.type	flatten_curve,@function
flatten_curve:                          # @flatten_curve
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 96
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rsi, %r13
	movq	96(%rsp), %r15
	movq	120(%rdi), %rsi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	128(%rdi), %rdx
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	movq	%r12, %rax
	subq	%rsi, %rax
	movq	%rax, %rdi
	negq	%rdi
	cmovlq	%rax, %rdi
	movq	%r15, %rbx
	subq	%rdx, %rbx
	movq	%rbx, %rcx
	negq	%rcx
	cmovlq	%rbx, %rcx
	cmpq	%rcx, %rdi
	jge	.LBB12_2
# BB#8:                                 #   in Loop: Header=BB12_1 Depth=1
	cvtsi2ssq	%rax, %xmm0
	cvtsi2ssq	%rbx, %xmm1
	divss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	mulss	scaled_flat_sq(%rip), %xmm1
	cvttss2si	%xmm1, %r8
	addq	scaled_flat(%rip), %r8
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rcx, %xmm1
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %rcx
	subq	%r13, %rcx
	addq	%rsi, %rcx
	movq	%rcx, %rdi
	negq	%rdi
	cmovlq	%rcx, %rdi
	cmpq	%r8, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jg	.LBB12_10
# BB#9:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rcx, %xmm1
	mulss	%xmm1, %xmm0
	cvttss2si	%xmm0, %rcx
	subq	%rax, %rcx
	addq	%rsi, %rcx
	jmp	.LBB12_6
	.p2align	4, 0x90
.LBB12_2:                               #   in Loop: Header=BB12_1 Depth=1
	testq	%rax, %rax
	je	.LBB12_3
# BB#4:                                 #   in Loop: Header=BB12_1 Depth=1
	cvtsi2ssq	%rbx, %xmm0
	cvtsi2ssq	%rax, %xmm1
	divss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	mulss	scaled_flat_sq(%rip), %xmm1
	cvttss2si	%xmm1, %r8
	addq	scaled_flat(%rip), %r8
	movq	%r13, %rcx
	subq	%rsi, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rcx, %xmm1
	mulss	%xmm0, %xmm1
	cvttss2si	%xmm1, %rcx
	subq	%rbp, %rcx
	addq	%rdx, %rcx
	movq	%rcx, %rdi
	negq	%rdi
	cmovlq	%rcx, %rdi
	cmpq	%r8, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	jg	.LBB12_10
# BB#5:                                 #   in Loop: Header=BB12_1 Depth=1
	movq	%rax, %rcx
	subq	%rsi, %rcx
	xorps	%xmm1, %xmm1
	cvtsi2ssq	%rcx, %xmm1
	mulss	%xmm1, %xmm0
	cvttss2si	%xmm0, %rcx
	subq	%rbx, %rcx
	addq	%rdx, %rcx
.LBB12_6:                               #   in Loop: Header=BB12_1 Depth=1
	movq	%rcx, %rdi
	negq	%rdi
	cmovlq	%rcx, %rdi
	cmpq	%r8, %rdi
	jle	.LBB12_7
.LBB12_10:                              #   in Loop: Header=BB12_1 Depth=1
	addq	%r13, %rsi
	sarq	%rsi
	addq	%rbp, %rdx
	sarq	%rdx
	addq	%rax, %r13
	sarq	%r13
	addq	%rbx, %rbp
	sarq	%rbp
	leaq	(%rsi,%r13), %rcx
	sarq	%rcx
	leaq	(%rdx,%rbp), %r8
	sarq	%r8
	addq	%r12, %rax
	sarq	%rax
	addq	%r15, %rbx
	sarq	%rbx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	addq	%rax, %r13
	sarq	%r13
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	addq	%rbx, %rbp
	sarq	%rbp
	leaq	(%rcx,%r13), %r14
	sarq	%r14
	leaq	(%r8,%rbp), %rbx
	sarq	%rbx
	movq	%rbx, (%rsp)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %r9
	callq	flatten_curve
	testl	%eax, %eax
	movq	%rbx, %rdx
	movq	%r14, %rsi
	jns	.LBB12_1
	jmp	.LBB12_11
.LBB12_3:
	xorl	%eax, %eax
.LBB12_11:                              # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_7:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	movq	%r15, %rdx
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	gx_path_add_line        # TAILCALL
.Lfunc_end12:
	.size	flatten_curve, .Lfunc_end12-flatten_curve
	.cfi_endproc

	.type	scaled_flat,@object     # @scaled_flat
	.comm	scaled_flat,8,8
	.type	scaled_flat_sq,@object  # @scaled_flat_sq
	.comm	scaled_flat_sq,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
