	.text
	.file	"me_epzs.bc"
	.globl	allocEPZScolocated
	.p2align	4, 0x90
	.type	allocEPZScolocated,@function
allocEPZScolocated:                     # @allocEPZScolocated
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movl	%edi, %r12d
	movl	$1, %edi
	movl	$40, %esi
	callq	calloc
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_2:
	movl	%r12d, 4(%r13)
	movl	%r15d, 8(%r13)
	leaq	16(%r13), %rdi
	movl	%r15d, %ebx
	sarl	$31, %ebx
	movl	%ebx, %edx
	shrl	$30, %edx
	addl	%r15d, %edx
	sarl	$2, %edx
	movl	%r12d, %ebp
	sarl	$31, %ebp
	shrl	$30, %ebp
	addl	%r12d, %ebp
	sarl	$2, %ebp
	movl	$2, %esi
	movl	$2, %r8d
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	testl	%r14d, %r14d
	je	.LBB0_4
# BB#3:
	leaq	24(%r13), %rdi
	shrl	$29, %ebx
	addl	%ebx, %r15d
	sarl	$3, %r15d
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r15d, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	movq	%r13, %rdi
	addq	$32, %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r15d, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
.LBB0_4:
	movl	%r14d, (%r13)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	allocEPZScolocated, .Lfunc_end0-allocEPZScolocated
	.cfi_endproc

	.globl	freeEPZScolocated
	.p2align	4, 0x90
	.type	freeEPZScolocated,@function
freeEPZScolocated:                      # @freeEPZScolocated
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_4
# BB#1:
	movq	16(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%eax, %edx
	sarl	$2, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	cmpl	$0, (%rbx)
	je	.LBB1_3
# BB#2:
	movq	24(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%eax, %edx
	sarl	$3, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	32(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%eax, %edx
	sarl	$3, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
.LBB1_3:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB1_4:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	freeEPZScolocated, .Lfunc_end1-freeEPZScolocated
	.cfi_endproc

	.globl	allocEPZSpattern
	.p2align	4, 0x90
	.type	allocEPZSpattern,@function
allocEPZSpattern:                       # @allocEPZSpattern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#1:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB2_2:
	movl	%ebp, (%rbx)
	movslq	%ebp, %rdi
	movl	$16, %esi
	callq	calloc
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	allocEPZSpattern, .Lfunc_end2-allocEPZSpattern
	.cfi_endproc

	.globl	freeEPZSpattern
	.p2align	4, 0x90
	.type	freeEPZSpattern,@function
freeEPZSpattern:                        # @freeEPZSpattern
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
.Lcfi21:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#2:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB3_1:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	freeEPZSpattern, .Lfunc_end3-freeEPZSpattern
	.cfi_endproc

	.globl	assignEPZSpattern
	.p2align	4, 0x90
	.type	assignEPZSpattern,@function
assignEPZSpattern:                      # @assignEPZSpattern
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movl	%ecx, %r9d
	cmpl	$0, (%rdi)
	jle	.LBB4_3
# BB#1:                                 # %.lr.ph
	movslq	%esi, %rax
	movl	mv_rescale(%rip), %ecx
	movq	8(%rdi), %rbx
	addq	$12, %rbx
	leaq	(%rax,%rax,2), %rax
	shlq	$6, %rax
	leaq	pattern_data+12(%rax), %rax
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movl	-12(%rax), %r11d
	sarl	%cl, %r11d
	movl	%r11d, -12(%rbx)
	movl	-8(%rax), %r11d
	sarl	%cl, %r11d
	movl	%r11d, -8(%rbx)
	movl	-4(%rax), %esi
	movl	%esi, -4(%rbx)
	movl	(%rax), %esi
	movl	%esi, (%rbx)
	incq	%r10
	movslq	(%rdi), %rsi
	addq	$16, %rbx
	addq	$16, %rax
	cmpq	%rsi, %r10
	jl	.LBB4_2
.LBB4_3:                                # %._crit_edge
	movl	%edx, 16(%rdi)
	movl	%r9d, 20(%rdi)
	movq	%r8, 24(%rdi)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	assignEPZSpattern, .Lfunc_end4-assignEPZSpattern
	.cfi_endproc

	.globl	EPZSWindowPredictorInit
	.p2align	4, 0x90
	.type	EPZSWindowPredictorInit,@function
EPZSWindowPredictorInit:                # @EPZSWindowPredictorInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	input(%rip), %rax
	xorl	%r8d, %r8d
	cmpl	$0, 4120(%rax)
	setne	%r8b
	addl	%r8d, %r8d
	movl	%edi, %eax
	imull	%eax, %eax
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	incl	%r9d
	movl	$1, %ebx
	movl	%r9d, %ecx
	shll	%cl, %ebx
	cmpl	%eax, %ebx
	jle	.LBB5_1
# BB#2:                                 # %RoundLog2.exit
	sarl	%r9d
	leal	-2(%r9), %eax
	testw	%dx, %dx
	je	.LBB5_6
# BB#3:                                 # %.preheader138
	testl	%eax, %eax
	js	.LBB5_10
# BB#4:                                 # %.lr.ph150
	movl	%r8d, %ecx
	shll	%cl, %edi
	movq	8(%rsi), %rax
	decl	%r9d
	xorl	%ebx, %ebx
	movabsq	$34359738368, %r10      # imm = 0x800000000
	.p2align	4, 0x90
.LBB5_5:                                # %.preheader137157
                                        # =>This Inner Loop Header: Depth=1
	decl	%r9d
	movl	%edi, %r11d
	movl	%r9d, %ecx
	sarl	%cl, %r11d
	leal	1(%r11,%r11,2), %ebp
	movl	%r8d, %ecx
	shll	%cl, %ebp
	movl	%r11d, %ecx
	negl	%ecx
	shlq	$32, %rbx
	movq	%rbx, %rdx
	sarq	$28, %rdx
	movl	%r11d, (%rax,%rdx)
	movl	$0, 4(%rax,%rdx)
	movl	%r11d, 16(%rax,%rdx)
	movl	%r11d, 20(%rax,%rdx)
	movl	$0, 32(%rax,%rdx)
	movl	%r11d, 36(%rax,%rdx)
	movl	%ecx, 48(%rax,%rdx)
	movl	%r11d, 52(%rax,%rdx)
	movl	%ecx, 64(%rax,%rdx)
	movl	$0, 68(%rax,%rdx)
	movl	%ecx, 80(%rax,%rdx)
	movl	%ecx, 84(%rax,%rdx)
	movl	$0, 96(%rax,%rdx)
	movl	%ecx, 100(%rax,%rdx)
	movl	%r11d, 112(%rax,%rdx)
	movl	%ecx, 116(%rax,%rdx)
	sarl	%ebp
	addq	%r10, %rbx
	movq	%rbx, %rdx
	sarq	$32, %rdx
	sarq	$28, %rbx
	movl	%ebp, (%rax,%rbx)
	movl	%ecx, 4(%rax,%rbx)
	movl	%ebp, 16(%rax,%rbx)
	movl	$0, 20(%rax,%rbx)
	movl	%ebp, 32(%rax,%rbx)
	movl	%r11d, 36(%rax,%rbx)
	movl	%r11d, 48(%rax,%rbx)
	movl	%ebp, 52(%rax,%rbx)
	movl	$0, 64(%rax,%rbx)
	movl	%ebp, 68(%rax,%rbx)
	movl	%ecx, 80(%rax,%rbx)
	movl	%ebp, 84(%rax,%rbx)
	negl	%ebp
	movl	%ebp, 96(%rax,%rbx)
	movl	%r11d, 100(%rax,%rbx)
	movl	%ebp, 112(%rax,%rbx)
	movl	$0, 116(%rax,%rbx)
	movl	%ebp, 128(%rax,%rbx)
	movl	%ecx, 132(%rax,%rbx)
	movl	%ecx, 144(%rax,%rbx)
	movl	%ebp, 148(%rax,%rbx)
	movl	$0, 160(%rax,%rbx)
	movl	%ebp, 164(%rax,%rbx)
	movl	%r11d, 176(%rax,%rbx)
	movl	%ebp, 180(%rax,%rbx)
	movq	%rdx, %rbx
	addq	$12, %rbx
	testl	%r9d, %r9d
	jg	.LBB5_5
	jmp	.LBB5_11
.LBB5_6:                                # %.preheader
	testl	%eax, %eax
	js	.LBB5_10
# BB#7:                                 # %.lr.ph
	movl	%r8d, %ecx
	shll	%cl, %edi
	movq	8(%rsi), %rax
	decl	%r9d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_8:                                # =>This Inner Loop Header: Depth=1
	decl	%r9d
	movl	%edi, %edx
	movl	%r9d, %ecx
	sarl	%cl, %edx
	movl	%edx, %r8d
	negl	%r8d
	movslq	%ebx, %rbx
	movq	%rbx, %rcx
	shlq	$4, %rcx
	movl	%edx, (%rax,%rcx)
	movl	$0, 4(%rax,%rcx)
	movl	%edx, 16(%rax,%rcx)
	movl	%edx, 20(%rax,%rcx)
	movl	$0, 32(%rax,%rcx)
	movl	%edx, 36(%rax,%rcx)
	movl	%r8d, 48(%rax,%rcx)
	movl	%edx, 52(%rax,%rcx)
	movl	%r8d, 64(%rax,%rcx)
	movl	$0, 68(%rax,%rcx)
	movl	%r8d, 80(%rax,%rcx)
	movl	%r8d, 84(%rax,%rcx)
	movl	$0, 96(%rax,%rcx)
	movl	%r8d, 100(%rax,%rcx)
	movl	%edx, 112(%rax,%rcx)
	movl	%r8d, 116(%rax,%rcx)
	addq	$8, %rbx
	testl	%r9d, %r9d
	jg	.LBB5_8
	jmp	.LBB5_11
.LBB5_10:
	xorl	%ebx, %ebx
.LBB5_11:                               # %.loopexit
	movl	%ebx, (%rsi)
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	EPZSWindowPredictorInit, .Lfunc_end5-EPZSWindowPredictorInit
	.cfi_endproc

	.globl	EPZSInit
	.p2align	4, 0x90
	.type	EPZSInit,@function
EPZSInit:                               # @EPZSInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 80
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movl	15444(%rax), %eax
	movq	input(%rip), %rdx
	movl	28(%rdx), %esi
	movl	%esi, %edi
	imull	%edi, %edi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	incl	%ebx
	movl	$1, %ebp
	movl	%ebx, %ecx
	shll	%cl, %ebp
	cmpl	%edi, %ebp
	jle	.LBB6_1
# BB#2:                                 # %RoundLog2.exit
	addl	$-8, %eax
	cmpl	$0, 2120(%rdx)
	je	.LBB6_4
# BB#3:
	movl	2128(%rdx), %ecx
	cmpl	%ecx, %esi
	cmovgel	%esi, %ecx
	movl	%ecx, %esi
.LBB6_4:
	sarl	%ebx
	leal	1(%rsi,%rsi), %esi
	movl	4120(%rdx), %edi
	movl	%edi, %ecx
	addb	%cl, %cl
	shll	%cl, %esi
	movl	%esi, searcharray(%rip)
	xorl	%ecx, %ecx
	testl	%edi, %edi
	sete	%cl
	addl	%ecx, %ecx
	movl	%ecx, mv_rescale(%rip)
	movl	4116(%rdx), %r11d
	movl	4108(%rdx), %r10d
	movl	4112(%rdx), %r8d
	movl	4132(%rdx), %r9d
	movl	$0, medthres(%rip)
	movl	$0, maxthres(%rip)
	movl	$0, minthres(%rip)
	movl	$0, subthres(%rip)
	movl	%r11d, %ebp
	shll	$8, %ebp
	movl	%eax, %ecx
	shll	%cl, %ebp
	movl	%ebp, medthres+4(%rip)
	movl	%r8d, %ecx
	shll	$8, %ecx
	leal	(%rcx,%rcx,2), %ebp
	movl	%eax, %ecx
	shll	%cl, %ebp
	movl	%ebp, maxthres+4(%rip)
	movl	%r10d, %ebp
	shll	$6, %ebp
	shll	%cl, %ebp
	movl	%ebp, minthres+4(%rip)
	movl	%r9d, %edx
	shll	$8, %edx
	shll	%cl, %edx
	movl	%edx, subthres+4(%rip)
	movl	%r11d, %edx
	shll	$7, %edx
	shll	%cl, %edx
	movl	%edx, medthres+8(%rip)
	movl	%r8d, %ecx
	shll	$7, %ecx
	leal	(%rcx,%rcx,2), %ebp
	movl	%eax, %ecx
	shll	%cl, %ebp
	movl	%ebp, maxthres+8(%rip)
	movl	%r10d, %esi
	shll	$5, %esi
	shll	%cl, %esi
	movl	%esi, minthres+8(%rip)
	movl	%r9d, %edi
	shll	$7, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	movl	%edi, subthres+8(%rip)
	movl	%edx, medthres+12(%rip)
	movl	%ebp, maxthres+12(%rip)
	movl	%esi, minthres+12(%rip)
	movl	%edi, subthres+12(%rip)
	movl	%r11d, %edx
	shll	$6, %edx
	shll	%cl, %edx
	movl	%edx, medthres+16(%rip)
	movl	%r8d, %ecx
	shll	$6, %ecx
	leal	(%rcx,%rcx,2), %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	%edx, maxthres+16(%rip)
	movl	%r10d, %edx
	shll	$4, %edx
	shll	%cl, %edx
	movl	%edx, minthres+16(%rip)
	movl	%r9d, %edx
	shll	$6, %edx
	shll	%cl, %edx
	movl	%edx, subthres+16(%rip)
	movl	%r11d, %edx
	shll	$5, %edx
	shll	%cl, %edx
	movl	%edx, medthres+20(%rip)
	movl	%r8d, %ecx
	shll	$5, %ecx
	leal	(%rcx,%rcx,2), %esi
	movl	%eax, %ecx
	shll	%cl, %esi
	movl	%esi, maxthres+20(%rip)
	leal	(,%r10,8), %edi
	shll	%cl, %edi
	movl	%edi, minthres+20(%rip)
	movl	%r9d, %ebp
	shll	$5, %ebp
	shll	%cl, %ebp
	movl	%ebp, subthres+20(%rip)
	movl	%edx, medthres+24(%rip)
	movl	%esi, maxthres+24(%rip)
	movl	%edi, minthres+24(%rip)
	movl	%ebp, subthres+24(%rip)
	shll	$4, %r11d
	shll	%cl, %r11d
	movl	%r11d, medthres+28(%rip)
	shll	$4, %r8d
	leal	(%r8,%r8,2), %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	%edx, maxthres+28(%rip)
	shll	$2, %r10d
	shll	%cl, %r10d
	movl	%r10d, minthres+28(%rip)
	shll	$4, %r9d
	shll	%cl, %r9d
	movl	%r9d, subthres+28(%rip)
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB6_6
# BB#5:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB6_6:                                # %.lr.ph.i
	decl	%ebx
	movl	$4, (%r14)
	movl	$4, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, 8(%r14)
	movq	%r14, sdiamond(%rip)
	movl	mv_rescale(%rip), %ecx
	movslq	(%r14), %rdx
	xorl	%esi, %esi
	movl	$12, %edi
	.p2align	4, 0x90
.LBB6_7:                                # =>This Inner Loop Header: Depth=1
	movl	pattern_data-12(%rdi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, -12(%rax,%rdi)
	movl	pattern_data-8(%rdi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, -8(%rax,%rdi)
	movl	pattern_data-4(%rdi), %ebp
	movl	%ebp, -4(%rax,%rdi)
	movl	pattern_data(%rdi), %ebp
	movl	%ebp, (%rax,%rdi)
	incq	%rsi
	addq	$16, %rdi
	cmpq	%rdx, %rsi
	jl	.LBB6_7
# BB#8:                                 # %assignEPZSpattern.exit
	movl	$1, 16(%r14)
	movl	$1, 20(%r14)
	movq	%r14, 24(%r14)
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB6_10
# BB#9:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB6_10:                               # %.lr.ph.i44
	movl	$8, (%r14)
	movl	$8, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, 8(%r14)
	movq	%r14, square(%rip)
	movl	mv_rescale(%rip), %ecx
	movslq	(%r14), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	movl	pattern_data+192(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, (%rax,%rsi)
	movl	pattern_data+196(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, 4(%rax,%rsi)
	movl	pattern_data+200(%rsi), %ebp
	movl	%ebp, 8(%rax,%rsi)
	movl	pattern_data+204(%rsi), %ebp
	movl	%ebp, 12(%rax,%rsi)
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdx, %rdi
	jl	.LBB6_11
# BB#12:                                # %assignEPZSpattern.exit47
	movl	$1, 16(%r14)
	movl	$1, 20(%r14)
	movq	%r14, 24(%r14)
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB6_14
# BB#13:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB6_14:                               # %.lr.ph.i49
	movl	$12, (%r14)
	movl	$12, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, 8(%r14)
	movq	%r14, ediamond(%rip)
	movl	mv_rescale(%rip), %ecx
	movslq	(%r14), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_15:                               # =>This Inner Loop Header: Depth=1
	movl	pattern_data+384(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, (%rax,%rsi)
	movl	pattern_data+388(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, 4(%rax,%rsi)
	movl	pattern_data+392(%rsi), %ebp
	movl	%ebp, 8(%rax,%rsi)
	movl	pattern_data+396(%rsi), %ebp
	movl	%ebp, 12(%rax,%rsi)
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdx, %rdi
	jl	.LBB6_15
# BB#16:                                # %assignEPZSpattern.exit52
	movl	$1, 16(%r14)
	movl	$1, 20(%r14)
	movq	%r14, 24(%r14)
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB6_18
# BB#17:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB6_18:                               # %.lr.ph.i54
	movl	$8, (%r14)
	movl	$8, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, 8(%r14)
	movq	%r14, ldiamond(%rip)
	movl	mv_rescale(%rip), %ecx
	movslq	(%r14), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_19:                               # =>This Inner Loop Header: Depth=1
	movl	pattern_data+576(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, (%rax,%rsi)
	movl	pattern_data+580(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, 4(%rax,%rsi)
	movl	pattern_data+584(%rsi), %ebp
	movl	%ebp, 8(%rax,%rsi)
	movl	pattern_data+588(%rsi), %ebp
	movl	%ebp, 12(%rax,%rsi)
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdx, %rdi
	jl	.LBB6_19
# BB#20:                                # %assignEPZSpattern.exit57
	movl	$1, 16(%r14)
	movl	$1, 20(%r14)
	movq	%r14, 24(%r14)
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB6_22
# BB#21:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB6_22:                               # %.lr.ph.i59
	movl	$12, (%r15)
	movl	$12, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, 8(%r15)
	movq	%r15, sbdiamond(%rip)
	movq	sdiamond(%rip), %r14
	movl	mv_rescale(%rip), %ecx
	movslq	(%r15), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_23:                               # =>This Inner Loop Header: Depth=1
	movl	pattern_data+768(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, (%rax,%rsi)
	movl	pattern_data+772(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, 4(%rax,%rsi)
	movl	pattern_data+776(%rsi), %ebp
	movl	%ebp, 8(%rax,%rsi)
	movl	pattern_data+780(%rsi), %ebp
	movl	%ebp, 12(%rax,%rsi)
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdx, %rdi
	jl	.LBB6_23
# BB#24:                                # %assignEPZSpattern.exit62
	movl	$0, 16(%r15)
	movl	$1, 20(%r15)
	movq	%r14, 24(%r15)
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB6_26
# BB#25:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
	movq	sdiamond(%rip), %r14
.LBB6_26:                               # %.lr.ph.i64
	movl	$8, (%r15)
	movl	$8, %edi
	movl	$16, %esi
	callq	calloc
	movq	%rax, 8(%r15)
	movq	%r15, pmvfast(%rip)
	movl	mv_rescale(%rip), %ecx
	movslq	(%r15), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB6_27:                               # =>This Inner Loop Header: Depth=1
	movl	pattern_data+576(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, (%rax,%rsi)
	movl	pattern_data+580(%rsi), %ebp
	sarl	%cl, %ebp
	movl	%ebp, 4(%rax,%rsi)
	movl	pattern_data+584(%rsi), %ebp
	movl	%ebp, 8(%rax,%rsi)
	movl	pattern_data+588(%rsi), %ebp
	movl	%ebp, 12(%rax,%rsi)
	incq	%rdi
	addq	$16, %rsi
	cmpq	%rdx, %rdi
	jl	.LBB6_27
# BB#28:                                # %assignEPZSpattern.exit67
	movl	$0, 16(%r15)
	movl	$1, 20(%r15)
	movq	%r14, 24(%r15)
	leal	(,%rbx,8), %r14d
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB6_30
# BB#29:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB6_30:                               # %allocEPZSpattern.exit68
	movl	%r14d, (%rbp)
	movslq	%r14d, %rdi
	movl	$16, %esi
	callq	calloc
	movq	%rax, 8(%rbp)
	movq	%rbp, window_predictor(%rip)
	shll	$2, %ebx
	leal	(%rbx,%rbx,4), %r14d
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB6_32
# BB#31:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
	movq	window_predictor(%rip), %rbp
.LBB6_32:                               # %allocEPZSpattern.exit69
	movl	%r14d, (%rbx)
	movslq	%r14d, %r14
	movl	$16, %esi
	movq	%r14, %rdi
	callq	calloc
	movq	%rax, 8(%rbx)
	movq	%rbx, window_predictor_extended(%rip)
	movq	input(%rip), %rax
	movswl	28(%rax), %edi
	xorl	%edx, %edx
	movq	%rbp, %rsi
	callq	EPZSWindowPredictorInit
	movq	input(%rip), %rax
	movq	window_predictor_extended(%rip), %rsi
	movswl	28(%rax), %edi
	movl	$1, %edx
	callq	EPZSWindowPredictorInit
	movq	input(%rip), %rax
	movslq	4100(%rax), %rcx
	leaq	(%rcx,%rcx,8), %rcx
	addq	%r14, %rcx
	movslq	4104(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	10(%rcx,%rax), %rbx
	movl	$1, %edi
	movl	$32, %esi
	callq	calloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB6_34
# BB#33:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
.LBB6_34:                               # %allocEPZSpattern.exit70
	movl	%ebx, (%rbp)
	movl	$16, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 8(%rbp)
	movq	%rbp, predictor(%rip)
	movq	img(%rip), %rax
	movl	52(%rax), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movl	$EPZSDistortion, %edi
	movl	$6, %esi
	movl	$7, %edx
	callq	get_mem3Dint
	movl	%eax, %ebx
	movl	searcharray(%rip), %esi
	movl	$EPZSMap, %edi
	movl	%esi, %edx
	callq	get_mem2Dshort
	movl	%eax, %r14d
	addl	%ebx, %r14d
	movq	input(%rip), %rax
	cmpl	$0, 4104(%rax)
	je	.LBB6_36
# BB#35:
	movq	img(%rip), %rax
	movl	32(%rax), %edx
	movl	52(%rax), %eax
	movl	%eax, %r9d
	sarl	$31, %r9d
	shrl	$30, %r9d
	addl	%eax, %r9d
	sarl	$2, %r9d
	movl	$2, (%rsp)
	movl	$EPZSMotion, %edi
	movl	$6, %esi
	movl	$7, %ecx
	movl	$4, %r8d
	callq	get_mem6Dshort
	addl	%eax, %r14d
	movq	input(%rip), %rax
.LBB6_36:
	cmpl	$0, 4100(%rax)
	je	.LBB6_42
# BB#37:
	movq	img(%rip), %rax
	movl	52(%rax), %r13d
	movl	68(%rax), %r12d
	movq	active_sps(%rip), %rax
	movl	1152(%rax), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	$1, %edi
	movl	$40, %esi
	callq	calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB6_39
# BB#38:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB6_39:
	movl	%r13d, 4(%rbx)
	movl	%r12d, 8(%rbx)
	leaq	16(%rbx), %rdi
	movl	%r12d, %r15d
	sarl	$31, %r15d
	movl	%r15d, %edx
	shrl	$30, %edx
	addl	%r12d, %edx
	sarl	$2, %edx
	movl	%r13d, %ebp
	sarl	$31, %ebp
	shrl	$30, %ebp
	addl	%r13d, %ebp
	sarl	$2, %ebp
	movl	$2, %esi
	movl	$2, %r8d
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	movl	20(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movl	%eax, %r13d
	je	.LBB6_41
# BB#40:
	leaq	24(%rbx), %rdi
	shrl	$29, %r15d
	addl	%r15d, %r12d
	sarl	$3, %r12d
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r12d, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
	movq	%rbx, %rdi
	addq	$32, %rdi
	movl	$2, %esi
	movl	$2, %r8d
	movl	%r12d, %edx
	movl	%ebp, %ecx
	callq	get_mem4Dshort
.LBB6_41:                               # %allocEPZScolocated.exit
	movl	%r13d, (%rbx)
	movq	%rbx, EPZSCo_located(%rip)
	movq	input(%rip), %rax
.LBB6_42:
	movl	4088(%rax), %ecx
	decl	%ecx
	cmpl	$4, %ecx
	ja	.LBB6_43
# BB#44:                                # %switch.lookup
	movslq	%ecx, %rcx
	movq	.Lswitch.table.1(,%rcx,8), %rcx
	jmp	.LBB6_45
.LBB6_43:
	movl	$sdiamond, %ecx
.LBB6_45:
	movq	(%rcx), %rcx
	movq	%rcx, searchPattern(%rip)
	movl	4092(%rax), %eax
	addl	$-2, %eax
	cmpl	$4, %eax
	ja	.LBB6_46
# BB#47:                                # %switch.lookup9
	cltq
	movq	.Lswitch.table.1(,%rax,8), %rax
	jmp	.LBB6_48
.LBB6_46:
	movl	$sdiamond, %eax
.LBB6_48:
	movq	(%rax), %rax
	movq	%rax, searchPatternD(%rip)
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	EPZSInit, .Lfunc_end6-EPZSInit
	.cfi_endproc

	.globl	EPZSDelete
	.p2align	4, 0x90
	.type	EPZSDelete,@function
EPZSDelete:                             # @EPZSDelete
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 16
.Lcfi42:
	.cfi_offset %rbx, -16
	movq	input(%rip), %rax
	cmpl	$0, 4100(%rax)
	je	.LBB7_5
# BB#1:
	movq	EPZSCo_located(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_5
# BB#2:
	movq	16(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%eax, %edx
	sarl	$2, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	cmpl	$0, (%rbx)
	je	.LBB7_4
# BB#3:
	movq	24(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%eax, %edx
	sarl	$3, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
	movq	32(%rbx), %rdi
	movl	8(%rbx), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	addl	%eax, %edx
	sarl	$3, %edx
	movl	$2, %esi
	callq	free_mem4Dshort
.LBB7_4:
	movq	%rbx, %rdi
	callq	free
.LBB7_5:                                # %freeEPZScolocated.exit
	movq	EPZSMap(%rip), %rdi
	callq	free_mem2Dshort
	movq	EPZSDistortion(%rip), %rdi
	movl	$6, %esi
	callq	free_mem3Dint
	movq	window_predictor_extended(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_7
# BB#6:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_7:                                # %freeEPZSpattern.exit
	movq	window_predictor(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_9
# BB#8:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_9:                                # %freeEPZSpattern.exit1
	movq	predictor(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_11
# BB#10:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_11:                               # %freeEPZSpattern.exit2
	movq	pmvfast(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_13
# BB#12:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_13:                               # %freeEPZSpattern.exit3
	movq	sbdiamond(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_15
# BB#14:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_15:                               # %freeEPZSpattern.exit4
	movq	ldiamond(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_17
# BB#16:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_17:                               # %freeEPZSpattern.exit5
	movq	ediamond(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_19
# BB#18:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_19:                               # %freeEPZSpattern.exit6
	movq	sdiamond(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_21
# BB#20:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_21:                               # %freeEPZSpattern.exit7
	movq	square(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB7_23
# BB#22:
	movq	8(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB7_23:                               # %freeEPZSpattern.exit8
	movq	input(%rip), %rax
	cmpl	$0, 4104(%rax)
	je	.LBB7_24
# BB#25:
	movq	EPZSMotion(%rip), %rdi
	movq	img(%rip), %rax
	movl	32(%rax), %edx
	movl	$6, %esi
	movl	$7, %ecx
	movl	$4, %r8d
	popq	%rbx
	jmp	free_mem6Dshort         # TAILCALL
.LBB7_24:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	EPZSDelete, .Lfunc_end7-EPZSDelete
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	256                     # 0x100
	.long	256                     # 0x100
	.long	256                     # 0x100
	.long	256                     # 0x100
	.text
	.globl	EPZSSliceInit
	.p2align	4, 0x90
	.type	EPZSSliceInit,@function
EPZSSliceInit:                          # @EPZSSliceInit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$1576, %rsp             # imm = 0x628
.Lcfi49:
	.cfi_def_cfa_offset 1632
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	movq	%rdi, -112(%rsp)        # 8-byte Spill
	movq	img(%rip), %rax
	movl	20(%rax), %ecx
	xorl	%edx, %edx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	movl	%ecx, -32(%rsp)         # 4-byte Spill
	cmpl	$1, %ecx
	sete	-72(%rsp)               # 1-byte Folded Spill
	movl	15268(%rax), %eax
	leal	2(,%rax,4), %eax
	testl	%eax, %eax
	jle	.LBB8_23
# BB#1:                                 # %.preheader1036.lr.ph
	movq	enc_picture(%rip), %rcx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movl	$mv_scale, %ecx
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	cltq
	movq	%rax, -56(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movl	$-128, %r15d
	movl	$127, %r10d
	movl	$-2048, %r12d           # imm = 0xF800
	movl	$2047, %r13d            # imm = 0x7FF
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader1036
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_4 Depth 2
                                        #       Child Loop BB8_12 Depth 3
                                        #       Child Loop BB8_15 Depth 3
                                        #       Child Loop BB8_6 Depth 3
	movslq	listXsize(,%rax,4), %r11
	leaq	1(%rax), %rcx
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	testq	%r11, %r11
	jle	.LBB8_22
# BB#3:                                 # %.preheader1035.lr.ph
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	%eax, %ecx
	andl	$-2, %ecx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	movq	-80(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %r8
	movq	-88(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rdx
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph1096
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_12 Depth 3
                                        #       Child Loop BB8_15 Depth 3
                                        #       Child Loop BB8_6 Depth 3
	movq	(%r8,%r9,8), %rax
	movl	4(%rax), %eax
	cmpq	$2, -128(%rsp)          # 8-byte Folded Reload
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	ja	.LBB8_10
# BB#5:                                 # %.lr.ph1096.split.us.preheader
                                        #   in Loop: Header=BB8_4 Depth=2
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movl	4(%rcx), %ecx
	movl	%ecx, %esi
	subl	%eax, %esi
	cmpl	$-129, %esi
	cmovlel	%r15d, %esi
	cmpl	$128, %esi
	cmovgel	%r10d, %esi
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	sarl	%eax
	movl	%eax, %r14d
	negl	%r14d
	cmpl	$-1, %esi
	cmovgel	%eax, %r14d
	addl	$16384, %r14d           # imm = 0x4000
	movq	%rdx, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_6:                                # %.lr.ph1096.split.us
                                        #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rbx,8), %rax
	movl	%ecx, %edi
	subl	4(%rax), %edi
	cmpl	$-129, %edi
	cmovlel	%r15d, %edi
	testl	%esi, %esi
	je	.LBB8_7
# BB#8:                                 #   in Loop: Header=BB8_6 Depth=3
	cmpl	$128, %edi
	cmovgel	%r10d, %edi
	movl	%r14d, %eax
	cltd
	idivl	%esi
	imull	%edi, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-2049, %eax            # imm = 0xF7FF
	cmovlel	%r12d, %eax
	cmpl	$2048, %eax             # imm = 0x800
	cmovgel	%r13d, %eax
	jmp	.LBB8_9
	.p2align	4, 0x90
.LBB8_7:                                #   in Loop: Header=BB8_6 Depth=3
	movl	$256, %eax              # imm = 0x100
.LBB8_9:                                #   in Loop: Header=BB8_6 Depth=3
	movl	%eax, (%rbp)
	incq	%rbx
	subq	$-128, %rbp
	cmpq	%r11, %rbx
	jl	.LBB8_6
	jmp	.LBB8_21
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph1096.split
                                        #   in Loop: Header=BB8_4 Depth=2
	cmpq	$2, -104(%rsp)          # 8-byte Folded Reload
	jne	.LBB8_11
# BB#14:                                # %.lr.ph1096.split.split.us.preheader
                                        #   in Loop: Header=BB8_4 Depth=2
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movl	8(%rcx), %ecx
	movl	%ecx, %edi
	subl	%eax, %edi
	cmpl	$-129, %edi
	cmovlel	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r10d, %edi
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	movl	%eax, %r14d
	negl	%r14d
	cmpl	$-1, %edi
	cmovgel	%eax, %r14d
	addl	$16384, %r14d           # imm = 0x4000
	movq	%rdx, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_15:                               # %.lr.ph1096.split.split.us
                                        #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rbx,8), %rax
	movl	%ecx, %esi
	subl	4(%rax), %esi
	cmpl	$-129, %esi
	cmovlel	%r15d, %esi
	testl	%edi, %edi
	je	.LBB8_16
# BB#17:                                #   in Loop: Header=BB8_15 Depth=3
	cmpl	$128, %esi
	cmovgel	%r10d, %esi
	movl	%r14d, %eax
	cltd
	idivl	%edi
	imull	%esi, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-2049, %eax            # imm = 0xF7FF
	cmovlel	%r12d, %eax
	cmpl	$2048, %eax             # imm = 0x800
	cmovgel	%r13d, %eax
	jmp	.LBB8_18
	.p2align	4, 0x90
.LBB8_16:                               #   in Loop: Header=BB8_15 Depth=3
	movl	$256, %eax              # imm = 0x100
.LBB8_18:                               #   in Loop: Header=BB8_15 Depth=3
	movl	%eax, (%rbp)
	incq	%rbx
	subq	$-128, %rbp
	cmpq	%r11, %rbx
	jl	.LBB8_15
	jmp	.LBB8_21
	.p2align	4, 0x90
.LBB8_11:                               # %.lr.ph1096.split.split.preheader
                                        #   in Loop: Header=BB8_4 Depth=2
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movl	12(%rcx), %r14d
	movl	%r14d, %edi
	subl	%eax, %edi
	cmpl	$-129, %edi
	cmovlel	%r15d, %edi
	cmpl	$128, %edi
	cmovgel	%r10d, %edi
	movl	%edi, %eax
	shrl	$31, %eax
	addl	%edi, %eax
	sarl	%eax
	movl	%eax, %ecx
	negl	%ecx
	cmpl	$-1, %edi
	cmovgel	%eax, %ecx
	addl	$16384, %ecx            # imm = 0x4000
	movq	%rdx, %rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph1096.split.split
                                        #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rbp,8), %rax
	movl	%r14d, %ebx
	subl	4(%rax), %ebx
	cmpl	$-129, %ebx
	cmovlel	%r15d, %ebx
	testl	%edi, %edi
	je	.LBB8_13
# BB#19:                                #   in Loop: Header=BB8_12 Depth=3
	cmpl	$128, %ebx
	cmovgel	%r10d, %ebx
	movl	%ecx, %eax
	cltd
	idivl	%edi
	imull	%ebx, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-2049, %eax            # imm = 0xF7FF
	cmovlel	%r12d, %eax
	cmpl	$2048, %eax             # imm = 0x800
	cmovgel	%r13d, %eax
	jmp	.LBB8_20
	.p2align	4, 0x90
.LBB8_13:                               #   in Loop: Header=BB8_12 Depth=3
	movl	$256, %eax              # imm = 0x100
.LBB8_20:                               #   in Loop: Header=BB8_12 Depth=3
	movl	%eax, (%rsi)
	incq	%rbp
	subq	$-128, %rsi
	cmpq	%r11, %rbp
	jl	.LBB8_12
.LBB8_21:                               # %._crit_edge1097
                                        #   in Loop: Header=BB8_4 Depth=2
	incq	%r9
	movq	-96(%rsp), %rdx         # 8-byte Reload
	addq	$4, %rdx
	cmpq	%r11, %r9
	jl	.LBB8_4
.LBB8_22:                               # %._crit_edge1104
                                        #   in Loop: Header=BB8_2 Depth=1
	addq	$4096, -88(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x1000
	movq	-128(%rsp), %rax        # 8-byte Reload
	cmpq	-56(%rsp), %rax         # 8-byte Folded Reload
	jl	.LBB8_2
.LBB8_23:                               # %._crit_edge1106
	movq	input(%rip), %rax
	cmpl	$0, 4100(%rax)
	je	.LBB8_184
# BB#24:
	xorl	%edi, %edi
	cmpl	$1, -32(%rsp)           # 4-byte Folded Reload
	sete	%dil
	movq	-80(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdi,8), %rax
	movq	(%rax), %rdx
	cmpl	$2, listXsize(,%rdi,4)
	movq	%rdx, -96(%rsp)         # 8-byte Spill
	jl	.LBB8_26
# BB#25:
	movq	8(%rax), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
.LBB8_26:                               # %.preheader1034
	movaps	.LCPI8_0(%rip), %xmm0   # xmm0 = [256,256,256,256]
	movaps	%xmm0, -16(%rsp)
	movups	%xmm0, 776(%rsp)
	movl	$256, (%rsp)            # imm = 0x100
	movl	$256, 792(%rsp)         # imm = 0x100
	movl	$256, 4(%rsp)           # imm = 0x100
	movl	$256, 796(%rsp)         # imm = 0x100
	movl	$256, 116(%rsp)         # imm = 0x100
	movl	$256, 908(%rsp)         # imm = 0x100
	movl	$256, 120(%rsp)         # imm = 0x100
	movl	$256, 912(%rsp)         # imm = 0x100
	movups	%xmm0, 124(%rsp)
	movups	%xmm0, 916(%rsp)
	movups	%xmm0, 248(%rsp)
	movaps	%xmm0, 1040(%rsp)
	movl	$256, 264(%rsp)         # imm = 0x100
	movl	$256, 1056(%rsp)        # imm = 0x100
	movl	$256, 268(%rsp)         # imm = 0x100
	movl	$256, 1060(%rsp)        # imm = 0x100
	movups	%xmm0, 380(%rsp)
	movups	%xmm0, 1172(%rsp)
	movl	$256, 396(%rsp)         # imm = 0x100
	movl	$256, 1188(%rsp)        # imm = 0x100
	movl	$256, 400(%rsp)         # imm = 0x100
	movl	$256, 1192(%rsp)        # imm = 0x100
	movaps	%xmm0, 512(%rsp)
	movups	%xmm0, 1304(%rsp)
	movl	$256, 528(%rsp)         # imm = 0x100
	movl	$256, 1320(%rsp)        # imm = 0x100
	movl	$256, 532(%rsp)         # imm = 0x100
	movl	$256, 1324(%rsp)        # imm = 0x100
	movl	$256, 644(%rsp)         # imm = 0x100
	movl	$256, 1436(%rsp)        # imm = 0x100
	movl	$256, 648(%rsp)         # imm = 0x100
	movl	$256, 1440(%rsp)        # imm = 0x100
	movups	%xmm0, 652(%rsp)
	movups	%xmm0, 1444(%rsp)
	movq	img(%rip), %rsi
	movl	15268(%rsi), %eax
	leal	2(,%rax,4), %ecx
	testl	%ecx, %ecx
	movq	%rdx, -128(%rsp)        # 8-byte Spill
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	jle	.LBB8_45
# BB#27:                                # %.preheader1032.lr.ph
	movq	-64(%rsp), %rcx         # 8-byte Reload
	movb	-72(%rsp), %dl          # 1-byte Reload
	movb	%dl, %cl
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movq	enc_picture(%rip), %rcx
	leaq	4(%rcx), %rdx
	movq	%rdx, -120(%rsp)        # 8-byte Spill
	leaq	8(%rcx), %rdx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	leaq	12(%rcx), %rcx
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	leaq	908(%rsp), %r11
	xorl	%ebp, %ebp
	movl	$mv_scale+128, %r13d
	movl	$-128, %ebx
	movl	$127, %edi
	.p2align	4, 0x90
.LBB8_28:                               # %.preheader1032
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_30 Depth 2
	movslq	listXsize(,%rbp,4), %r8
	testq	%r8, %r8
	jle	.LBB8_44
# BB#29:                                # %.lr.ph1088
                                        #   in Loop: Header=BB8_28 Depth=1
	movq	-80(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rbp,8), %r10
	movl	%ebp, %eax
	orl	-64(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %r14
	movq	(%rcx,%r14,8), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movq	(%rax), %r9
	movq	%r13, -40(%rsp)         # 8-byte Spill
	movq	%r11, -72(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB8_30:                               #   Parent Loop BB8_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebp, %ebp
	movq	-120(%rsp), %rax        # 8-byte Reload
	je	.LBB8_33
# BB#31:                                #   in Loop: Header=BB8_30 Depth=2
	cmpl	$2, %ebp
	movq	-104(%rsp), %rax        # 8-byte Reload
	je	.LBB8_33
# BB#32:                                #   in Loop: Header=BB8_30 Depth=2
	movq	-56(%rsp), %rax         # 8-byte Reload
.LBB8_33:                               #   in Loop: Header=BB8_30 Depth=2
	movl	(%rax), %r15d
	movq	(%r10,%r12,8), %rcx
	movl	4(%rcx), %eax
	subl	%eax, %r15d
	cmpl	$-129, %r15d
	cmovlel	%ebx, %r15d
	cmpl	$128, %r15d
	cmovgel	%edi, %r15d
	movl	4(%r9), %esi
	subl	%eax, %esi
	cmpl	$-129, %esi
	cmovlel	%ebx, %esi
	cmpl	$128, %esi
	cmovgel	%edi, %esi
	testl	%esi, %esi
	je	.LBB8_34
# BB#35:                                #   in Loop: Header=BB8_30 Depth=2
	movl	%esi, %edx
	shrl	$31, %edx
	addl	%esi, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %esi
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%esi
	imull	%r15d, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-2049, %eax            # imm = 0xF7FF
	movl	$-2048, %edx            # imm = 0xF800
	cmovlel	%edx, %eax
	cmpl	$2048, %eax             # imm = 0x800
	movl	$2047, %edx             # imm = 0x7FF
	cmovgel	%edx, %eax
	jmp	.LBB8_36
	.p2align	4, 0x90
.LBB8_34:                               #   in Loop: Header=BB8_30 Depth=2
	movl	$256, %eax              # imm = 0x100
.LBB8_36:                               #   in Loop: Header=BB8_30 Depth=2
	movl	-128(%r13), %edx
	imull	%eax, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	movl	%edx, -924(%r11)
	addl	$-256, %eax
	movl	%eax, -792(%r11)
	cmpl	$2, listXsize(,%r14,4)
	jl	.LBB8_41
# BB#37:                                #   in Loop: Header=BB8_30 Depth=2
	movq	-88(%rsp), %rax         # 8-byte Reload
	movq	8(%rax), %rax
	movl	4(%rax), %esi
	subl	4(%rcx), %esi
	cmpl	$-129, %esi
	cmovlel	%ebx, %esi
	cmpl	$128, %esi
	cmovgel	%edi, %esi
	testl	%esi, %esi
	je	.LBB8_38
# BB#39:                                #   in Loop: Header=BB8_30 Depth=2
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	sarl	%ecx
	movl	%ecx, %eax
	negl	%eax
	cmpl	$-1, %esi
	cmovgel	%ecx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%esi
	imull	%r15d, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-2049, %eax            # imm = 0xF7FF
	movl	$-2048, %ecx            # imm = 0xF800
	cmovlel	%ecx, %eax
	cmpl	$2048, %eax             # imm = 0x800
	movl	$2047, %ecx             # imm = 0x7FF
	cmovgel	%ecx, %eax
	jmp	.LBB8_40
	.p2align	4, 0x90
.LBB8_41:                               #   in Loop: Header=BB8_30 Depth=2
	movl	%edx, -132(%r11)
	jmp	.LBB8_42
.LBB8_38:                               #   in Loop: Header=BB8_30 Depth=2
	movl	$256, %eax              # imm = 0x100
.LBB8_40:                               #   in Loop: Header=BB8_30 Depth=2
	movl	(%r13), %ecx
	imull	%eax, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	movl	%ecx, -132(%r11)
	addl	$-256, %eax
.LBB8_42:                               #   in Loop: Header=BB8_30 Depth=2
	movl	%eax, (%r11)
	incq	%r12
	addq	$4, %r11
	addq	$4, %r13
	cmpq	%r8, %r12
	jl	.LBB8_30
# BB#43:                                # %._crit_edge1089.loopexit
                                        #   in Loop: Header=BB8_28 Depth=1
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	15268(%rax), %eax
	movq	-72(%rsp), %r11         # 8-byte Reload
	movq	-40(%rsp), %r13         # 8-byte Reload
.LBB8_44:                               # %._crit_edge1089
                                        #   in Loop: Header=BB8_28 Depth=1
	addq	$2, %rbp
	leal	2(,%rax,4), %ecx
	movslq	%ecx, %rcx
	addq	$264, %r11              # imm = 0x108
	addq	$8192, %r13             # imm = 0x2000
	cmpq	%rcx, %rbp
	jl	.LBB8_28
.LBB8_45:                               # %._crit_edge1091
	testl	%eax, %eax
	je	.LBB8_50
# BB#46:
	xorl	%eax, %eax
	cmpl	$1, -32(%rsp)           # 4-byte Folded Reload
	sete	%al
	movq	-80(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx,%rax,8), %rcx
	movq	(%rcx), %r14
	movq	32(%rdx,%rax,8), %rax
	cmpl	$2, listXsize(%rip)
	jl	.LBB8_47
# BB#49:
	movq	8(%rcx), %rcx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	movq	8(%rax), %r12
	jmp	.LBB8_48
.LBB8_50:
	movq	-24(%rsp), %rbp         # 8-byte Reload
	movl	24(%rbp), %eax
	testl	%eax, %eax
	je	.LBB8_51
# BB#52:
	movq	-128(%rsp), %r8         # 8-byte Reload
	cmpl	(%r8), %eax
	je	.LBB8_53
# BB#54:
	cmpl	$0, 6428(%r8)
	movq	%r8, %r12
	movq	-96(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	movq	%r8, %r14
	je	.LBB8_56
# BB#55:
	cmpl	$1, %eax
	movq	-80(%rsp), %rax         # 8-byte Reload
	movq	-48(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax), %rax
	movq	6536(%rax), %rcx
	movq	6544(%rax), %rax
	movq	%rax, %r12
	cmoveq	%rcx, %r12
	cmoveq	%rax, %rcx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	movq	%r12, %r14
	movq	%r12, %r8
	jmp	.LBB8_56
.LBB8_47:
	movq	(%rax), %r12
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	%rax, -104(%rsp)        # 8-byte Spill
.LBB8_48:
	movq	-128(%rsp), %r8         # 8-byte Reload
	movq	-24(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB8_56
.LBB8_51:
	movq	-128(%rsp), %r8         # 8-byte Reload
.LBB8_53:
	movq	%r8, %r12
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movq	%r8, %r14
.LBB8_56:
	movq	active_sps(%rip), %rax
	movl	1148(%rax), %eax
	movl	%eax, -48(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	movq	%r8, -128(%rsp)         # 8-byte Spill
	movq	%r12, -56(%rsp)         # 8-byte Spill
	movq	%r14, -88(%rsp)         # 8-byte Spill
	jne	.LBB8_92
# BB#57:                                # %.preheader1030
	movl	6396(%r8), %eax
	sarl	$2, %eax
	testl	%eax, %eax
	jle	.LBB8_92
# BB#58:                                # %.lr.ph1086
	movl	6392(%r8), %ecx
	sarl	$2, %ecx
	movl	%ecx, -64(%rsp)         # 4-byte Spill
	movslq	%ecx, %rcx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	cltq
	movq	%rax, -32(%rsp)         # 8-byte Spill
	xorl	%r10d, %r10d
	movl	$-32768, %r9d           # imm = 0x8000
	movw	$32767, %r13w           # imm = 0x7FFF
	.p2align	4, 0x90
.LBB8_59:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_61 Depth 2
                                        #       Child Loop BB8_76 Depth 3
                                        #       Child Loop BB8_68 Depth 3
                                        #       Child Loop BB8_84 Depth 3
	cmpl	$0, -64(%rsp)           # 4-byte Folded Reload
	jle	.LBB8_90
# BB#60:                                # %.lr.ph1083
                                        #   in Loop: Header=BB8_59 Depth=1
	movl	%r10d, %eax
	shrl	$31, %eax
	addl	%r10d, %eax
	sarl	%eax
	movl	%r10d, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%r10d, %ecx
	sarl	$3, %ecx
	leal	(%rax,%rcx,4), %ecx
	movslq	%ecx, %rcx
	cltq
	movq	%rax, -80(%rsp)         # 8-byte Spill
	movq	%rcx, %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	leal	4(%rcx), %eax
	cltq
	movq	%rax, -40(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_61:                               #   Parent Loop BB8_59 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_76 Depth 3
                                        #       Child Loop BB8_68 Depth 3
                                        #       Child Loop BB8_84 Depth 3
	cmpl	$0, 15268(%rbp)
	je	.LBB8_80
# BB#62:                                #   in Loop: Header=BB8_61 Depth=2
	movq	6528(%r8), %rax
	movq	(%rax,%r10,8), %rax
	cmpb	$0, (%rax,%r15)
	je	.LBB8_80
# BB#63:                                #   in Loop: Header=BB8_61 Depth=2
	movq	enc_picture(%rip), %rdi
	movl	4(%rdi), %eax
	movl	%eax, %ecx
	subl	4(%r12), %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	subl	4(%r14), %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movq	6504(%r8), %rax
	movq	(%rax), %rax
	cmpl	%ecx, %edx
	jle	.LBB8_72
# BB#64:                                #   in Loop: Header=BB8_61 Depth=2
	movq	%rbp, %rsi
	movq	-72(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%r15,8), %rbx
	testq	%rbx, %rbx
	sets	%al
	movl	listXsize(%rip), %ebp
	cmpl	$1, %ebp
	setg	%dl
	andb	%al, %dl
	movq	%r14, %rax
	cmovneq	-104(%rsp), %rax        # 8-byte Folded Reload
	cmpq	$-1, %rbx
	je	.LBB8_71
# BB#65:                                # %.preheader1026
                                        #   in Loop: Header=BB8_61 Depth=2
	movq	img(%rip), %rcx
	movq	%rcx, %r11
	movl	14456(%rcx), %ecx
	cmpl	%ebp, %ecx
	cmovlel	%ecx, %ebp
	movl	$256, %ecx              # imm = 0x100
	xorl	%r8d, %r8d
	testl	%ebp, %ebp
	jle	.LBB8_70
# BB#66:                                # %.lr.ph1078.preheader
                                        #   in Loop: Header=BB8_61 Depth=2
	movzbl	%dl, %edx
	movslq	%ebp, %rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_68:                               # %.lr.ph1078
                                        #   Parent Loop BB8_59 Depth=1
                                        #     Parent Loop BB8_61 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, 24(%rdi,%rbp,8)
	je	.LBB8_69
# BB#67:                                #   in Loop: Header=BB8_68 Depth=3
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB8_68
	jmp	.LBB8_70
	.p2align	4, 0x90
.LBB8_80:                               #   in Loop: Header=BB8_61 Depth=2
	movq	6504(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r10,8), %rax
	cmpq	$0, (%rax,%r15,8)
	sets	%al
	movl	listXsize(%rip), %ebx
	cmpl	$1, %ebx
	setg	%dl
	andb	%al, %dl
	movq	%r8, %rax
	cmovneq	-96(%rsp), %rax         # 8-byte Folded Reload
	movq	6504(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%r15,8), %rdi
	cmpq	$-1, %rdi
	je	.LBB8_88
# BB#81:                                # %.preheader1024
                                        #   in Loop: Header=BB8_61 Depth=2
	movq	img(%rip), %rcx
	movq	%rcx, %r11
	movl	14456(%rcx), %ecx
	cmpl	%ebx, %ecx
	cmovlel	%ecx, %ebx
	movl	$256, %ecx              # imm = 0x100
	xorl	%ebp, %ebp
	testl	%ebx, %ebx
	jle	.LBB8_87
# BB#82:                                # %.lr.ph1080
                                        #   in Loop: Header=BB8_61 Depth=2
	movzbl	%dl, %r8d
	movq	enc_picture(%rip), %rsi
	movslq	%ebx, %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_84:                               #   Parent Loop BB8_59 Depth=1
                                        #     Parent Loop BB8_61 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rdi, 24(%rsi,%rbx,8)
	je	.LBB8_85
# BB#83:                                #   in Loop: Header=BB8_84 Depth=3
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB8_84
	jmp	.LBB8_86
	.p2align	4, 0x90
.LBB8_88:                               #   in Loop: Header=BB8_61 Depth=2
	movq	-112(%rsp), %rax        # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%r15,8), %rcx
	movl	$0, (%rcx)
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%r15,8), %rdi
	movw	$0, (%rdi)
	xorl	%ecx, %ecx
	jmp	.LBB8_89
.LBB8_72:                               #   in Loop: Header=BB8_61 Depth=2
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%r15,8), %rbx
	testq	%rbx, %rbx
	sets	%al
	movl	listXsize(%rip), %r14d
	cmpl	$1, %r14d
	setg	%dl
	andb	%al, %dl
	movq	%r12, %rax
	cmovneq	-96(%rsp), %rax         # 8-byte Folded Reload
	cmpq	$-1, %rbx
	je	.LBB8_79
# BB#73:                                # %.preheader1028
                                        #   in Loop: Header=BB8_61 Depth=2
	movl	14456(%rbp), %esi
	cmpl	%r14d, %esi
	cmovgl	%r14d, %esi
	movl	$256, %ecx              # imm = 0x100
	xorl	%r12d, %r12d
	testl	%esi, %esi
	jle	.LBB8_78
# BB#74:                                # %.lr.ph1076
                                        #   in Loop: Header=BB8_61 Depth=2
	movzbl	%dl, %edx
	movq	img(%rip), %rbp
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB8_76:                               #   Parent Loop BB8_59 Depth=1
                                        #     Parent Loop BB8_61 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, 24(%rdi,%r11,8)
	je	.LBB8_77
# BB#75:                                #   in Loop: Header=BB8_76 Depth=3
	incq	%r11
	movl	14456(%rbp), %esi
	cmpl	%r14d, %esi
	cmovgl	%r14d, %esi
	movslq	%esi, %rsi
	cmpq	%rsi, %r11
	jl	.LBB8_76
	jmp	.LBB8_78
.LBB8_85:                               #   in Loop: Header=BB8_61 Depth=2
	imulq	$792, %r8, %rdx         # imm = 0x318
	leaq	116(%rsp), %rcx
	addq	%rcx, %rdx
	movl	-132(%rdx,%rbx,4), %ecx
	movl	(%rdx,%rbx,4), %ebp
.LBB8_86:                               # %.loopexit1025
                                        #   in Loop: Header=BB8_61 Depth=2
	movq	-128(%rsp), %r8         # 8-byte Reload
.LBB8_87:                               # %.loopexit1025
                                        #   in Loop: Header=BB8_61 Depth=2
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%r15,8), %rax
	movswl	(%rax), %edx
	imull	%ecx, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	cmpl	$-32769, %edx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %edx
	cmpl	$32767, %edx            # imm = 0x7FFF
	cmovgew	%r13w, %dx
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movq	16(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	(%rdi,%r10,8), %rdi
	movq	(%rdi,%r15,8), %rdi
	movw	%dx, (%rdi)
	movswl	2(%rax), %edx
	imull	%ecx, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	cmpl	$-32769, %edx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %edx
	cmpl	$32767, %edx            # imm = 0x7FFF
	cmovgew	%r13w, %dx
	movw	%dx, 2(%rdi)
	movswl	(%rax), %ecx
	imull	%ebp, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r13w, %cx
	movq	8(%rsi), %rdx
	movq	(%rdx,%r10,8), %rdx
	movq	(%rdx,%r15,8), %rdi
	movw	%cx, (%rdi)
	movswl	2(%rax), %ecx
	imull	%ebp, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r13w, %cx
	movq	%r11, %rbp
	jmp	.LBB8_89
.LBB8_71:                               #   in Loop: Header=BB8_61 Depth=2
	movq	-112(%rsp), %rax        # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%r15,8), %rcx
	movl	$0, (%rcx)
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%r15,8), %rdi
	movw	$0, (%rdi)
	xorl	%ecx, %ecx
	movq	%rsi, %rbp
	jmp	.LBB8_89
.LBB8_79:                               #   in Loop: Header=BB8_61 Depth=2
	movq	-112(%rsp), %rax        # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%r15,8), %rcx
	movl	$0, (%rcx)
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movq	(%rax,%r15,8), %rdi
	movw	$0, (%rdi)
	xorl	%ecx, %ecx
	movq	-88(%rsp), %r14         # 8-byte Reload
	jmp	.LBB8_89
.LBB8_69:                               #   in Loop: Header=BB8_61 Depth=2
	imulq	$792, %rdx, %rdx        # imm = 0x318
	leaq	116(%rsp), %rcx
	addq	%rcx, %rdx
	movl	-132(%rdx,%rbp,4), %ecx
	movl	(%rdx,%rbp,4), %r8d
.LBB8_70:                               # %.loopexit1027
                                        #   in Loop: Header=BB8_61 Depth=2
	movq	%r11, %rbp
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	-80(%rsp), %rdx         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r15,8), %rax
	movswl	(%rax), %edx
	imull	%ecx, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	cmpl	$-32769, %edx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %edx
	cmpl	$32767, %edx            # imm = 0x7FFF
	cmovgew	%r13w, %dx
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movq	16(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	(%rdi,%r10,8), %rdi
	movq	(%rdi,%r15,8), %rdi
	movw	%dx, (%rdi)
	movswl	2(%rax), %edx
	imull	%ecx, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	cmpl	$-32769, %edx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %edx
	cmpl	$32767, %edx            # imm = 0x7FFF
	cmovgew	%r13w, %dx
	movw	%dx, 2(%rdi)
	movswl	(%rax), %ecx
	imull	%r8d, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r13w, %cx
	movq	8(%rsi), %rdx
	movq	(%rdx,%r10,8), %rdx
	movq	(%rdx,%r15,8), %rdi
	movw	%cx, (%rdi)
	movswl	2(%rax), %ecx
	imull	%r8d, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r13w, %cx
	movq	-128(%rsp), %r8         # 8-byte Reload
	jmp	.LBB8_89
.LBB8_77:                               #   in Loop: Header=BB8_61 Depth=2
	imulq	$792, %rdx, %rdx        # imm = 0x318
	leaq	116(%rsp), %rcx
	addq	%rcx, %rdx
	movl	-132(%rdx,%r11,4), %ecx
	movl	(%rdx,%r11,4), %r12d
.LBB8_78:                               # %.loopexit1029
                                        #   in Loop: Header=BB8_61 Depth=2
	movq	-88(%rsp), %r14         # 8-byte Reload
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	-80(%rsp), %rdx         # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	movq	(%rax,%r15,8), %rax
	movswl	(%rax), %edx
	imull	%ecx, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	cmpl	$-32769, %edx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %edx
	cmpl	$32767, %edx            # imm = 0x7FFF
	cmovgew	%r13w, %dx
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movq	16(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	(%rdi,%r10,8), %rdi
	movq	(%rdi,%r15,8), %rdi
	movw	%dx, (%rdi)
	movswl	2(%rax), %edx
	imull	%ecx, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	cmpl	$-32769, %edx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %edx
	cmpl	$32767, %edx            # imm = 0x7FFF
	cmovgew	%r13w, %dx
	movw	%dx, 2(%rdi)
	movswl	(%rax), %ecx
	imull	%r12d, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r13w, %cx
	movq	8(%rsi), %rdx
	movq	(%rdx,%r10,8), %rdx
	movq	(%rdx,%r15,8), %rdi
	movw	%cx, (%rdi)
	movswl	2(%rax), %ecx
	imull	%r12d, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r9d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r13w, %cx
	movq	-56(%rsp), %r12         # 8-byte Reload
	.p2align	4, 0x90
.LBB8_89:                               #   in Loop: Header=BB8_61 Depth=2
	movw	%cx, 2(%rdi)
	incq	%r15
	cmpq	-120(%rsp), %r15        # 8-byte Folded Reload
	jl	.LBB8_61
.LBB8_90:                               # %._crit_edge1084
                                        #   in Loop: Header=BB8_59 Depth=1
	incq	%r10
	cmpq	-32(%rsp), %r10         # 8-byte Folded Reload
	jl	.LBB8_59
# BB#91:                                # %.loopexit1031.loopexit
	movq	img(%rip), %rbp
.LBB8_92:                               # %.loopexit1031
	cmpl	$0, 24(%rbp)
	jne	.LBB8_94
# BB#93:
	cmpl	$0, 15268(%rbp)
	je	.LBB8_133
.LBB8_94:                               # %.preheader1022
	movl	6396(%r8), %eax
	cmpl	$8, %eax
	jl	.LBB8_133
# BB#95:                                # %.preheader1021.lr.ph
	movq	-112(%rsp), %rcx        # 8-byte Reload
	leaq	16(%rcx), %rdx
	movq	%rdx, -72(%rsp)         # 8-byte Spill
	leaq	24(%rcx), %rcx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movl	6392(%r8), %edx
	movl	%edx, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	movl	%edx, -40(%rsp)         # 4-byte Spill
	addl	%edx, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %r10
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	xorl	%r11d, %r11d
	movl	$-32768, %r12d          # imm = 0x8000
	movw	$32767, %r13w           # imm = 0x7FFF
	.p2align	4, 0x90
.LBB8_96:                               # %.preheader1021
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_98 Depth 2
                                        #       Child Loop BB8_112 Depth 3
                                        #       Child Loop BB8_123 Depth 3
                                        #       Child Loop BB8_103 Depth 3
	cmpl	$4, -40(%rsp)           # 4-byte Folded Reload
	jl	.LBB8_132
# BB#97:                                # %.lr.ph1072
                                        #   in Loop: Header=BB8_96 Depth=1
	leal	(%r11,%r11), %eax
	cltq
	movq	%rax, -80(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_98:                               #   Parent Loop BB8_96 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_112 Depth 3
                                        #       Child Loop BB8_123 Depth 3
                                        #       Child Loop BB8_103 Depth 3
	cmpl	$0, 15268(%rbp)
	je	.LBB8_99
# BB#108:                               #   in Loop: Header=BB8_98 Depth=2
	movq	-56(%rsp), %rcx         # 8-byte Reload
	movq	6504(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	cmpq	$0, (%rax,%r14,8)
	sets	%al
	cmpl	$1, listXsize(%rip)
	setg	%bl
	andb	%al, %bl
	movq	%rcx, %rdx
	cmovneq	-96(%rsp), %rdx         # 8-byte Folded Reload
	movq	6504(%rdx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r14,8), %rax
	cmpq	$-1, %rax
	je	.LBB8_116
# BB#109:                               # %.preheader1019
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	%rbp, %rdi
	movl	14456(%rdi), %ebp
	addl	%ebp, %ebp
	movl	listXsize+16(%rip), %esi
	cmpl	%esi, %ebp
	cmovgl	%esi, %ebp
	movl	$256, %ecx              # imm = 0x100
	xorl	%r9d, %r9d
	testl	%ebp, %ebp
	jle	.LBB8_115
# BB#110:                               # %.lr.ph1065
                                        #   in Loop: Header=BB8_98 Depth=2
	movzbl	%bl, %r15d
	movq	enc_picture(%rip), %r8
	movq	img(%rip), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_112:                              #   Parent Loop BB8_96 Depth=1
                                        #     Parent Loop BB8_98 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rax, 1080(%r8,%rbx,8)
	je	.LBB8_113
# BB#111:                               #   in Loop: Header=BB8_112 Depth=3
	incq	%rbx
	movl	14456(%rdi), %ebp
	addl	%ebp, %ebp
	cmpl	%esi, %ebp
	cmovgl	%esi, %ebp
	movslq	%ebp, %rbp
	cmpq	%rbp, %rbx
	jl	.LBB8_112
	jmp	.LBB8_114
	.p2align	4, 0x90
.LBB8_99:                               #   in Loop: Header=BB8_98 Depth=2
	movq	6504(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	cmpq	$0, (%rax,%r14,8)
	sets	%al
	movl	listXsize(%rip), %edi
	cmpl	$1, %edi
	setg	%bl
	andb	%al, %bl
	movq	%r8, %rax
	cmovneq	-96(%rsp), %rax         # 8-byte Folded Reload
	movq	6504(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r14,8), %rsi
	cmpq	$-1, %rsi
	je	.LBB8_107
# BB#100:                               # %.preheader1015
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	img(%rip), %r9
	movl	14456(%r9), %ecx
	cmpl	%edi, %ecx
	cmovlel	%ecx, %edi
	movl	$256, %ecx              # imm = 0x100
	xorl	%edx, %edx
	testl	%edi, %edi
	jle	.LBB8_106
# BB#101:                               # %.lr.ph1069
                                        #   in Loop: Header=BB8_98 Depth=2
	movzbl	%bl, %r8d
	movq	enc_picture(%rip), %rbp
	movslq	%edi, %rbx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB8_103:                              #   Parent Loop BB8_96 Depth=1
                                        #     Parent Loop BB8_98 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rsi, 24(%rbp,%rdi,8)
	je	.LBB8_104
# BB#102:                               #   in Loop: Header=BB8_103 Depth=3
	incq	%rdi
	cmpq	%rbx, %rdi
	jl	.LBB8_103
	jmp	.LBB8_105
	.p2align	4, 0x90
.LBB8_116:                              #   in Loop: Header=BB8_98 Depth=2
	movq	-112(%rsp), %rax        # 8-byte Reload
	movq	32(%rax), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r14,8), %rdx
	movl	$0, (%rdx)
	movq	8(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r14,8), %rax
	movw	$0, (%rax)
	xorl	%ecx, %ecx
	jmp	.LBB8_117
.LBB8_107:                              #   in Loop: Header=BB8_98 Depth=2
	movq	-72(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r14,8), %rcx
	movl	$0, (%rcx)
	movq	8(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r14,8), %rax
	movw	$0, (%rax)
	xorl	%eax, %eax
	movq	%rdx, %rcx
	jmp	.LBB8_130
.LBB8_113:                              #   in Loop: Header=BB8_98 Depth=2
	imulq	$792, %r15, %rax        # imm = 0x318
	leaq	644(%rsp), %rcx
	addq	%rcx, %rax
	movl	-132(%rax,%rbx,4), %ecx
	movl	(%rax,%rbx,4), %r9d
.LBB8_114:                              # %.loopexit1020
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	-128(%rsp), %r8         # 8-byte Reload
.LBB8_115:                              # %.loopexit1020
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	6512(%rdx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r14,8), %rsi
	movswl	(%rsi), %eax
	imull	%ecx, %eax
	subl	$-128, %eax
	sarl	$8, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %eax
	cmpl	$32767, %eax            # imm = 0x7FFF
	cmovgew	%r13w, %ax
	movq	-112(%rsp), %rdx        # 8-byte Reload
	movq	32(%rdx), %rbp
	movq	(%rbp), %rdx
	movq	(%rdx,%r11,8), %rdx
	movq	(%rdx,%r14,8), %rdx
	movw	%ax, (%rdx)
	movswl	2(%rsi), %eax
	imull	%ecx, %eax
	subl	$-128, %eax
	sarl	$8, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %eax
	cmpl	$32767, %eax            # imm = 0x7FFF
	cmovgew	%r13w, %ax
	movw	%ax, 2(%rdx)
	movswl	(%rsi), %ecx
	imull	%r9d, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r13w, %cx
	movq	8(%rbp), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r14,8), %rax
	movw	%cx, (%rax)
	movswl	2(%rsi), %ecx
	imull	%r9d, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r13w, %cx
	movq	%rdi, %rbp
.LBB8_117:                              #   in Loop: Header=BB8_98 Depth=2
	movw	%cx, 2(%rax)
	movq	6528(%r8), %rcx
	movq	-80(%rsp), %rsi         # 8-byte Reload
	movq	(%rcx,%rsi,8), %r15
	cmpb	$0, (%r15,%r14)
	jne	.LBB8_119
# BB#118:                               #   in Loop: Header=BB8_98 Depth=2
	movswl	2(%rdx), %ecx
	incl	%ecx
	shrl	%ecx
	movw	%cx, 2(%rdx)
	movswl	2(%rax), %ecx
	incl	%ecx
	shrl	%ecx
	movw	%cx, 2(%rax)
.LBB8_119:                              #   in Loop: Header=BB8_98 Depth=2
	movq	-88(%rsp), %rcx         # 8-byte Reload
	movq	6504(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	cmpq	$0, (%rax,%r14,8)
	sets	%al
	cmpl	$1, listXsize(%rip)
	setg	%bl
	andb	%al, %bl
	movq	%rcx, %rax
	cmovneq	-104(%rsp), %rax        # 8-byte Folded Reload
	movq	6504(%rax), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r14,8), %rsi
	cmpq	$-1, %rsi
	je	.LBB8_127
# BB#120:                               # %.preheader1017
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	img(%rip), %rcx
	movq	%rcx, %r9
	movl	14456(%rcx), %ebp
	addl	%ebp, %ebp
	movl	listXsize+8(%rip), %ecx
	cmpl	%ecx, %ebp
	cmovgl	%ecx, %ebp
	movl	$256, %ecx              # imm = 0x100
	xorl	%edx, %edx
	testl	%ebp, %ebp
	jle	.LBB8_126
# BB#121:                               # %.lr.ph1067
                                        #   in Loop: Header=BB8_98 Depth=2
	movzbl	%bl, %r8d
	movq	enc_picture(%rip), %rbx
	movslq	%ebp, %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_123:                              #   Parent Loop BB8_96 Depth=1
                                        #     Parent Loop BB8_98 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rsi, 552(%rbx,%rbp,8)
	je	.LBB8_124
# BB#122:                               #   in Loop: Header=BB8_123 Depth=3
	incq	%rbp
	cmpq	%rdi, %rbp
	jl	.LBB8_123
	jmp	.LBB8_125
	.p2align	4, 0x90
.LBB8_127:                              #   in Loop: Header=BB8_98 Depth=2
	movq	-120(%rsp), %rax        # 8-byte Reload
	movq	(%rax), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r14,8), %rsi
	movl	$0, (%rsi)
	movq	8(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r14,8), %rcx
	movw	$0, (%rcx)
	xorl	%eax, %eax
	jmp	.LBB8_128
.LBB8_124:                              #   in Loop: Header=BB8_98 Depth=2
	imulq	$792, %r8, %rdx         # imm = 0x318
	leaq	380(%rsp), %rcx
	addq	%rcx, %rdx
	movl	-132(%rdx,%rbp,4), %ecx
	movl	(%rdx,%rbp,4), %edx
.LBB8_125:                              # %.loopexit1018
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	-128(%rsp), %r8         # 8-byte Reload
.LBB8_126:                              # %.loopexit1018
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r14,8), %rax
	movswl	(%rax), %edi
	imull	%ecx, %edi
	subl	$-128, %edi
	sarl	$8, %edi
	cmpl	$-32769, %edi           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %edi
	cmpl	$32767, %edi            # imm = 0x7FFF
	cmovgew	%r13w, %di
	movq	-120(%rsp), %rsi        # 8-byte Reload
	movq	(%rsi), %rbp
	movq	(%rbp), %rsi
	movq	(%rsi,%r11,8), %rsi
	movq	(%rsi,%r14,8), %rsi
	movw	%di, (%rsi)
	movswl	2(%rax), %edi
	imull	%ecx, %edi
	subl	$-128, %edi
	sarl	$8, %edi
	cmpl	$-32769, %edi           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %edi
	cmpl	$32767, %edi            # imm = 0x7FFF
	cmovgew	%r13w, %di
	movw	%di, 2(%rsi)
	movswl	(%rax), %edi
	imull	%edx, %edi
	subl	$-128, %edi
	sarl	$8, %edi
	cmpl	$-32769, %edi           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %edi
	cmpl	$32767, %edi            # imm = 0x7FFF
	cmovgew	%r13w, %di
	movq	8(%rbp), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r14,8), %rcx
	movw	%di, (%rcx)
	movswl	2(%rax), %eax
	imull	%edx, %eax
	subl	$-128, %eax
	sarl	$8, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %eax
	cmpl	$32767, %eax            # imm = 0x7FFF
	cmovgew	%r13w, %ax
	movq	%r9, %rbp
.LBB8_128:                              #   in Loop: Header=BB8_98 Depth=2
	movw	%ax, 2(%rcx)
	cmpb	$0, (%r15,%r14)
	jne	.LBB8_131
# BB#129:                               #   in Loop: Header=BB8_98 Depth=2
	movswl	2(%rsi), %eax
	incl	%eax
	shrl	%eax
	movw	%ax, 2(%rsi)
	movswl	2(%rcx), %eax
	incl	%eax
	shrl	%eax
	movq	-120(%rsp), %rcx        # 8-byte Reload
	jmp	.LBB8_130
.LBB8_104:                              #   in Loop: Header=BB8_98 Depth=2
	imulq	$792, %r8, %rdx         # imm = 0x318
	leaq	116(%rsp), %rcx
	addq	%rcx, %rdx
	movl	-132(%rdx,%rdi,4), %ecx
	movl	(%rdx,%rdi,4), %edx
.LBB8_105:                              # %.loopexit1016
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	-128(%rsp), %r8         # 8-byte Reload
.LBB8_106:                              # %.loopexit1016
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r11,8), %rax
	movq	(%rax,%r14,8), %rax
	movswl	(%rax), %esi
	imull	%ecx, %esi
	subl	$-128, %esi
	sarl	$8, %esi
	cmpl	$-32769, %esi           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %esi
	cmpl	$32767, %esi            # imm = 0x7FFF
	cmovgew	%r13w, %si
	movq	-72(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx), %rdi
	movq	(%rdi), %rbp
	movq	(%rbp,%r11,8), %rbp
	movq	(%rbp,%r14,8), %rbp
	movw	%si, (%rbp)
	movswl	2(%rax), %esi
	imull	%ecx, %esi
	subl	$-128, %esi
	sarl	$8, %esi
	cmpl	$-32769, %esi           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %esi
	cmpl	$32767, %esi            # imm = 0x7FFF
	cmovgew	%r13w, %si
	movw	%si, 2(%rbp)
	movswl	(%rax), %ecx
	imull	%edx, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r13w, %cx
	movq	8(%rdi), %rsi
	movq	(%rsi,%r11,8), %rsi
	movq	(%rsi,%r14,8), %rsi
	movw	%cx, (%rsi)
	movswl	2(%rax), %eax
	imull	%edx, %eax
	subl	$-128, %eax
	sarl	$8, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	cmovlel	%r12d, %eax
	cmpl	$32767, %eax            # imm = 0x7FFF
	cmovgew	%r13w, %ax
	movq	%rbx, %rcx
	movq	%r9, %rbp
.LBB8_130:                              # %.sink.split
                                        #   in Loop: Header=BB8_98 Depth=2
	movq	(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r11,8), %rcx
	movq	(%rcx,%r14,8), %rcx
	movw	%ax, 2(%rcx)
.LBB8_131:                              #   in Loop: Header=BB8_98 Depth=2
	incq	%r14
	cmpq	%r10, %r14
	jl	.LBB8_98
.LBB8_132:                              # %._crit_edge1073
                                        #   in Loop: Header=BB8_96 Depth=1
	incq	%r11
	cmpq	-64(%rsp), %r11         # 8-byte Folded Reload
	jl	.LBB8_96
.LBB8_133:                              # %.loopexit1023
	cmpl	$0, -48(%rsp)           # 4-byte Folded Reload
	je	.LBB8_134
.LBB8_155:                              # %.preheader1009
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	6396(%rax), %eax
	sarl	$2, %eax
	testl	%eax, %eax
	jle	.LBB8_184
# BB#156:                               # %.preheader1008.lr.ph
	movq	-128(%rsp), %r8         # 8-byte Reload
	movl	6392(%r8), %ecx
	sarl	$2, %ecx
	cltq
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movl	%ecx, -120(%rsp)        # 4-byte Spill
	movslq	%ecx, %r11
	leaq	116(%rsp), %r10
	xorl	%edi, %edi
	movl	$-32768, %r15d          # imm = 0x8000
	movw	$32767, %r12w           # imm = 0x7FFF
	.p2align	4, 0x90
.LBB8_157:                              # %.preheader1008
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_159 Depth 2
                                        #       Child Loop BB8_163 Depth 3
	cmpl	$0, -120(%rsp)          # 4-byte Folded Reload
	jle	.LBB8_169
# BB#158:                               # %.lr.ph1053
                                        #   in Loop: Header=BB8_157 Depth=1
	movq	6504(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax,%rdi,8), %r14
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_159:                              #   Parent Loop BB8_157 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_163 Depth 3
	cmpq	$0, (%r14,%rax,8)
	sets	%cl
	movl	listXsize(%rip), %ebx
	cmpl	$1, %ebx
	setg	%dl
	andb	%cl, %dl
	movq	%r8, %rcx
	cmovneq	-96(%rsp), %rcx         # 8-byte Folded Reload
	movq	6504(%rcx), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movq	(%rsi,%rax,8), %rbp
	cmpq	$-1, %rbp
	je	.LBB8_167
# BB#160:                               # %.preheader1006
                                        #   in Loop: Header=BB8_159 Depth=2
	movq	img(%rip), %rsi
	movl	14456(%rsi), %esi
	cmpl	%ebx, %esi
	cmovlel	%esi, %ebx
	movl	$256, %esi              # imm = 0x100
	xorl	%r13d, %r13d
	testl	%ebx, %ebx
	jle	.LBB8_166
# BB#161:                               # %.lr.ph1051
                                        #   in Loop: Header=BB8_159 Depth=2
	movzbl	%dl, %edx
	movq	enc_picture(%rip), %r8
	movslq	%ebx, %r9
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_163:                              #   Parent Loop BB8_157 Depth=1
                                        #     Parent Loop BB8_159 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, 24(%r8,%rbx,8)
	je	.LBB8_164
# BB#162:                               #   in Loop: Header=BB8_163 Depth=3
	incq	%rbx
	cmpq	%r9, %rbx
	jl	.LBB8_163
	jmp	.LBB8_165
	.p2align	4, 0x90
.LBB8_167:                              #   in Loop: Header=BB8_159 Depth=2
	movq	-112(%rsp), %rcx        # 8-byte Reload
	movq	16(%rcx), %rcx
	movq	(%rcx), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rdx,%rax,8), %rdx
	movl	$0, (%rdx)
	movq	8(%rcx), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%rax,8), %rdx
	movw	$0, (%rdx)
	xorl	%ecx, %ecx
	jmp	.LBB8_168
.LBB8_164:                              #   in Loop: Header=BB8_159 Depth=2
	imulq	$792, %rdx, %rdx        # imm = 0x318
	addq	%r10, %rdx
	movl	-132(%rdx,%rbx,4), %esi
	movl	(%rdx,%rbx,4), %r13d
.LBB8_165:                              # %.loopexit1007
                                        #   in Loop: Header=BB8_159 Depth=2
	movq	-128(%rsp), %r8         # 8-byte Reload
.LBB8_166:                              # %.loopexit1007
                                        #   in Loop: Header=BB8_159 Depth=2
	movq	6512(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movswl	(%rcx), %edx
	imull	%esi, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	cmpl	$-32769, %edx           # imm = 0xFFFF7FFF
	cmovlel	%r15d, %edx
	cmpl	$32767, %edx            # imm = 0x7FFF
	cmovgew	%r12w, %dx
	movq	-112(%rsp), %rbp        # 8-byte Reload
	movq	16(%rbp), %rbp
	movq	(%rbp), %rbx
	movq	(%rbx,%rdi,8), %rbx
	movq	(%rbx,%rax,8), %rbx
	movw	%dx, (%rbx)
	movswl	2(%rcx), %edx
	imull	%esi, %edx
	subl	$-128, %edx
	sarl	$8, %edx
	cmpl	$-32769, %edx           # imm = 0xFFFF7FFF
	cmovlel	%r15d, %edx
	cmpl	$32767, %edx            # imm = 0x7FFF
	cmovgew	%r12w, %dx
	movw	%dx, 2(%rbx)
	movswl	(%rcx), %esi
	imull	%r13d, %esi
	subl	$-128, %esi
	sarl	$8, %esi
	cmpl	$-32769, %esi           # imm = 0xFFFF7FFF
	cmovlel	%r15d, %esi
	cmpl	$32767, %esi            # imm = 0x7FFF
	cmovgew	%r12w, %si
	movq	8(%rbp), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rdx,%rax,8), %rdx
	movw	%si, (%rdx)
	movswl	2(%rcx), %ecx
	imull	%r13d, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%r15d, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r12w, %cx
.LBB8_168:                              #   in Loop: Header=BB8_159 Depth=2
	movw	%cx, 2(%rdx)
	incq	%rax
	cmpq	%r11, %rax
	jl	.LBB8_159
.LBB8_169:                              # %._crit_edge1054
                                        #   in Loop: Header=BB8_157 Depth=1
	incq	%rdi
	cmpq	-104(%rsp), %rdi        # 8-byte Folded Reload
	jl	.LBB8_157
# BB#170:                               # %.loopexit1010
	cmpl	$0, -48(%rsp)           # 4-byte Folded Reload
	jne	.LBB8_184
	jmp	.LBB8_171
.LBB8_134:
	movq	img(%rip), %rax
	cmpl	$0, 24(%rax)
	je	.LBB8_135
.LBB8_171:                              # %.preheader1005
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	6396(%rax), %ecx
	sarl	$2, %ecx
	testl	%ecx, %ecx
	jle	.LBB8_184
# BB#172:                               # %.preheader.lr.ph
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	6392(%rax), %eax
	sarl	$2, %eax
	movslq	%ecx, %r8
	movslq	%eax, %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_173:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_175 Depth 2
	testl	%eax, %eax
	jle	.LBB8_183
# BB#174:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB8_173 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB8_175:                              # %.lr.ph
                                        #   Parent Loop BB8_173 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	img(%rip), %rbp
	cmpl	$0, 15268(%rbp)
	je	.LBB8_176
.LBB8_177:                              #   in Loop: Header=BB8_175 Depth=2
	movq	-128(%rsp), %rcx        # 8-byte Reload
	movq	6528(%rcx), %rbx
	movq	(%rbx,%rsi,8), %rbx
	cmpb	$0, (%rbx,%rdi)
	je	.LBB8_179
# BB#178:                               #   in Loop: Header=BB8_175 Depth=2
	movq	-112(%rsp), %rcx        # 8-byte Reload
	movq	16(%rcx), %rbp
	movq	(%rbp), %rbx
	movq	(%rbx,%rsi,8), %rbx
	movq	(%rbx,%rdi,8), %rbx
	shlw	2(%rbx)
	movq	8(%rbp), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movq	(%rbp,%rdi,8), %rbp
	shlw	2(%rbp)
	jmp	.LBB8_182
	.p2align	4, 0x90
.LBB8_176:                              #   in Loop: Header=BB8_175 Depth=2
	cmpl	$0, 24(%rbp)
	je	.LBB8_177
.LBB8_179:                              # %.thread
                                        #   in Loop: Header=BB8_175 Depth=2
	cmpl	$0, 24(%rbp)
	je	.LBB8_182
# BB#180:                               #   in Loop: Header=BB8_175 Depth=2
	movq	-128(%rsp), %rcx        # 8-byte Reload
	movq	6528(%rcx), %rbp
	movq	(%rbp,%rsi,8), %rbp
	cmpb	$0, (%rbp,%rdi)
	jne	.LBB8_182
# BB#181:                               #   in Loop: Header=BB8_175 Depth=2
	movq	-112(%rsp), %rcx        # 8-byte Reload
	movq	16(%rcx), %rbp
	movq	(%rbp), %rbx
	movq	(%rbx,%rsi,8), %rbx
	movq	(%rbx,%rdi,8), %rbx
	movswl	2(%rbx), %ecx
	incl	%ecx
	shrl	%ecx
	movw	%cx, 2(%rbx)
	movq	8(%rbp), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movswl	2(%rcx), %ebp
	incl	%ebp
	shrl	%ebp
	movw	%bp, 2(%rcx)
	.p2align	4, 0x90
.LBB8_182:                              #   in Loop: Header=BB8_175 Depth=2
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB8_175
.LBB8_183:                              # %._crit_edge
                                        #   in Loop: Header=BB8_173 Depth=1
	incq	%rsi
	cmpq	%r8, %rsi
	jl	.LBB8_173
.LBB8_184:                              # %.loopexit
	addq	$1576, %rsp             # imm = 0x628
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_135:                              # %.preheader1013
	movq	-128(%rsp), %rax        # 8-byte Reload
	movl	6396(%rax), %eax
	sarl	$2, %eax
	testl	%eax, %eax
	jle	.LBB8_171
# BB#136:                               # %.lr.ph1063
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movl	6392(%rsi), %ecx
	sarl	$2, %ecx
	movl	%ecx, -104(%rsp)        # 4-byte Spill
	movslq	%ecx, %r9
	cltq
	movq	%rax, -88(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB8_137:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_139 Depth 2
                                        #       Child Loop BB8_144 Depth 3
	cmpl	$0, -104(%rsp)          # 4-byte Folded Reload
	jle	.LBB8_153
# BB#138:                               # %.lr.ph1060
                                        #   in Loop: Header=BB8_137 Depth=1
	movl	%r12d, %eax
	sarl	%eax
	movl	%r12d, %ecx
	sarl	$3, %ecx
	leal	(%rax,%rcx,4), %ecx
	movslq	%ecx, %r15
	cltq
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	6528(%rsi), %rax
	movq	(%rax,%r12,8), %r13
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_139:                              #   Parent Loop BB8_137 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_144 Depth 3
	cmpb	$0, (%r13,%rbx)
	je	.LBB8_152
# BB#140:                               #   in Loop: Header=BB8_139 Depth=2
	movq	6504(%rsi), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rax
	cmpq	$0, (%rax,%rbx,8)
	sets	%al
	movl	listXsize(%rip), %r11d
	cmpl	$1, %r11d
	setg	%r14b
	andb	%al, %r14b
	movq	%rsi, %rcx
	cmovneq	-96(%rsp), %rcx         # 8-byte Folded Reload
	movq	6504(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rbx,8), %r10
	cmpq	$-1, %r10
	je	.LBB8_150
# BB#141:                               # %.preheader1011
                                        #   in Loop: Header=BB8_139 Depth=2
	movq	img(%rip), %rax
	movl	14456(%rax), %eax
	cmpl	%r11d, %eax
	cmovlel	%eax, %r11d
	movl	$256, %edx              # imm = 0x100
	xorl	%r8d, %r8d
	movq	enc_picture(%rip), %rsi
	testl	%r11d, %r11d
	jle	.LBB8_146
# BB#142:                               # %.lr.ph1057
                                        #   in Loop: Header=BB8_139 Depth=2
	movzbl	%r14b, %eax
	movslq	%r11d, %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_144:                              #   Parent Loop BB8_137 Depth=1
                                        #     Parent Loop BB8_139 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r10, 24(%rsi,%rbp,8)
	je	.LBB8_145
# BB#143:                               #   in Loop: Header=BB8_144 Depth=3
	incq	%rbp
	cmpq	%rdi, %rbp
	jl	.LBB8_144
	jmp	.LBB8_146
	.p2align	4, 0x90
.LBB8_150:                              #   in Loop: Header=BB8_139 Depth=2
	movq	-112(%rsp), %rax        # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movl	$0, (%rcx)
	movq	8(%rax), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax,%rbx,8), %rdx
	movw	$0, (%rdx)
	xorl	%ecx, %ecx
	jmp	.LBB8_151
.LBB8_145:                              #   in Loop: Header=BB8_139 Depth=2
	imulq	$792, %rax, %rax        # imm = 0x318
	leaq	116(%rsp), %rdx
	addq	%rdx, %rax
	movl	-132(%rax,%rbp,4), %edx
	movl	(%rax,%rbp,4), %r8d
.LBB8_146:                              # %.loopexit1012
                                        #   in Loop: Header=BB8_139 Depth=2
	movl	4(%rsi), %ebp
	movq	6536(%rcx), %rax
	movq	6544(%rcx), %rcx
	movl	%ebp, %esi
	subl	4(%rcx), %esi
	movl	%esi, %edi
	negl	%edi
	cmovll	%esi, %edi
	subl	4(%rax), %ebp
	movl	%ebp, %esi
	negl	%esi
	cmovll	%ebp, %esi
	cmpl	%esi, %edi
	jle	.LBB8_149
# BB#147:                               #   in Loop: Header=BB8_139 Depth=2
	movq	6512(%rax), %rax
	jmp	.LBB8_148
.LBB8_149:                              #   in Loop: Header=BB8_139 Depth=2
	movq	6512(%rcx), %rax
.LBB8_148:                              #   in Loop: Header=BB8_139 Depth=2
	movq	(%rax), %rax
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rbx,8), %rcx
	movswl	(%rcx), %eax
	imull	%edx, %eax
	subl	$-128, %eax
	sarl	$8, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	movl	$-32768, %ebp           # imm = 0x8000
	cmovlel	%ebp, %eax
	cmpl	$32767, %eax            # imm = 0x7FFF
	movw	$32767, %r10w           # imm = 0x7FFF
	cmovgew	%r10w, %ax
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movq	16(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	(%rdi,%r12,8), %rdi
	movq	(%rdi,%rbx,8), %rdi
	movw	%ax, (%rdi)
	movswl	2(%rcx), %eax
	imull	%edx, %eax
	subl	$-128, %eax
	sarl	$8, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	cmovlel	%ebp, %eax
	cmpl	$32767, %eax            # imm = 0x7FFF
	cmovgew	%r10w, %ax
	movw	%ax, 2(%rdi)
	movswl	(%rcx), %eax
	imull	%r8d, %eax
	subl	$-128, %eax
	sarl	$8, %eax
	cmpl	$-32769, %eax           # imm = 0xFFFF7FFF
	cmovlel	%ebp, %eax
	cmpl	$32767, %eax            # imm = 0x7FFF
	cmovgew	%r10w, %ax
	movq	8(%rsi), %rdx
	movq	(%rdx,%r12,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movw	%ax, (%rdx)
	movswl	2(%rcx), %ecx
	imull	%r8d, %ecx
	subl	$-128, %ecx
	sarl	$8, %ecx
	cmpl	$-32769, %ecx           # imm = 0xFFFF7FFF
	cmovlel	%ebp, %ecx
	cmpl	$32767, %ecx            # imm = 0x7FFF
	cmovgew	%r10w, %cx
	movq	-128(%rsp), %rsi        # 8-byte Reload
.LBB8_151:                              #   in Loop: Header=BB8_139 Depth=2
	movw	%cx, 2(%rdx)
.LBB8_152:                              #   in Loop: Header=BB8_139 Depth=2
	incq	%rbx
	cmpq	%r9, %rbx
	jl	.LBB8_139
.LBB8_153:                              # %._crit_edge1061
                                        #   in Loop: Header=BB8_137 Depth=1
	incq	%r12
	cmpq	-88(%rsp), %r12         # 8-byte Folded Reload
	jl	.LBB8_137
# BB#154:                               # %.loopexit1014
	cmpl	$0, -48(%rsp)           # 4-byte Folded Reload
	jne	.LBB8_155
	jmp	.LBB8_171
.Lfunc_end8:
	.size	EPZSSliceInit, .Lfunc_end8-EPZSSliceInit
	.cfi_endproc

	.globl	EPZSPelBlockMotionSearch
	.p2align	4, 0x90
	.type	EPZSPelBlockMotionSearch,@function
EPZSPelBlockMotionSearch:               # @EPZSPelBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi62:
	.cfi_def_cfa_offset 496
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%r9, 232(%rsp)          # 8-byte Spill
	movq	%r8, 216(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, 312(%rsp)         # 8-byte Spill
	movl	504(%rsp), %ebp
	movq	520(%rsp), %rax
	movl	496(%rsp), %r14d
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	leal	(%rcx,%rdx), %ecx
	movslq	%ecx, %r10
	movq	listX(,%r10,8), %rcx
	movl	%esi, 156(%rsp)         # 4-byte Spill
	movslq	%esi, %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	(%rcx,%rdx,8), %rbx
	movq	img(%rip), %rdx
	movl	%r14d, %ecx
	subl	192(%rdx), %ecx
	movl	%ecx, 168(%rsp)         # 4-byte Spill
	movl	%ebp, %r13d
	subl	196(%rdx), %r13d
	movswl	(%rax), %ecx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movswl	2(%rax), %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	mv_rescale(%rip), %eax
	movl	$2, %ecx
	subl	%eax, %ecx
	movl	%r14d, %edi
	shll	%cl, %edi
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	movq	%rbp, 288(%rsp)         # 8-byte Spill
	movq	528(%rsp), %rsi
	movzwl	(%rsi), %ecx
	movw	%cx, 248(%rsp)          # 2-byte Spill
	movswl	%cx, %ecx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movq	%rdi, 280(%rsp)         # 8-byte Spill
	leal	(%rdi,%rcx), %r8d
	movl	%r8d, 192(%rsp)         # 4-byte Spill
	movl	%eax, %ecx
	shll	%cl, %r8d
	movzwl	2(%rsi), %ecx
	movw	%cx, 164(%rsp)          # 2-byte Spill
	movswl	%cx, %ecx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	leal	(%rcx,%rbp), %r9d
	movl	%r9d, 208(%rsp)         # 4-byte Spill
	movl	%eax, %ecx
	shll	%cl, %r9d
	movq	active_pps(%rip), %rax
	cmpl	$0, 192(%rax)
	movq	input(%rip), %rdi
	movslq	512(%rsp), %rsi
	movswl	76(%rdi,%rsi,8), %ecx
	movl	%ecx, 116(%rsp)         # 4-byte Spill
	movswl	72(%rdi,%rsi,8), %ecx
	movl	%ecx, 100(%rsp)         # 4-byte Spill
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movl	medthres(,%rsi,4), %ecx
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	je	.LBB9_9
.LBB9_1:
	cmpl	$0, 2936(%rdi)
	setne	%r15b
.LBB9_2:
	movl	%r13d, %r12d
	shll	$16, %r12d
	movq	EPZSDistortion(%rip), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	-8(%rcx,%rdx,8), %rcx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movq	searchPattern(%rip), %rcx
	movq	%rcx, 360(%rsp)         # 8-byte Spill
	movzwl	EPZSBlkCount(%rip), %ecx
	incl	%ecx
	movw	%cx, EPZSBlkCount(%rip)
	movq	6448(%rbx), %rdx
	movq	%rdx, ref_pic_sub(%rip)
	movl	6392(%rbx), %edx
	movw	%dx, img_width(%rip)
	movl	6396(%rbx), %r11d
	movw	%r11w, img_height(%rip)
	movl	6408(%rbx), %ebp
	movl	%ebp, width_pad(%rip)
	movl	6412(%rbx), %ebp
	movl	%ebp, height_pad(%rip)
	testb	%r15b, %r15b
	je	.LBB9_4
# BB#3:
	movq	wp_weight(%rip), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	104(%rsp), %rsi         # 8-byte Reload
	movq	(%rbp,%rsi,8), %rbp
	movl	(%rbp), %ebp
	movl	%ebp, weight_luma(%rip)
	movq	wp_offset(%rip), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movl	(%rbp), %ebp
	movl	%ebp, offset_luma(%rip)
.LBB9_4:
	movl	%r14d, %ebp
	shrl	$2, %ebp
	movl	%ebp, 112(%rsp)         # 4-byte Spill
	movl	%r12d, %eax
	sarl	$18, %eax
	cmpl	$0, ChromaMEEnable(%rip)
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	%r10, 144(%rsp)         # 8-byte Spill
	je	.LBB9_7
# BB#5:
	movq	6464(%rbx), %rbp
	movq	(%rbp), %r10
	movq	%r10, ref_pic_sub+8(%rip)
	movq	144(%rsp), %r10         # 8-byte Reload
	movq	272(%rsp), %rax         # 8-byte Reload
	movq	8(%rbp), %rbp
	movq	%rbp, ref_pic_sub+16(%rip)
	movl	6416(%rbx), %ebp
	movl	%ebp, width_pad_cr(%rip)
	movl	6420(%rbx), %ebp
	movl	%ebp, height_pad_cr(%rip)
	testb	%r15b, %r15b
	je	.LBB9_7
# BB#6:
	movq	wp_weight(%rip), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	104(%rsp), %rsi         # 8-byte Reload
	movq	(%rbp,%rsi,8), %rbp
	movl	4(%rbp), %ebx
	movl	%ebx, weight_cr(%rip)
	movl	8(%rbp), %ebp
	movl	%ebp, weight_cr+4(%rip)
	movq	wp_offset(%rip), %rbp
	movq	(%rbp,%r10,8), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movl	4(%rbp), %ebx
	movl	%ebx, offset_cr(%rip)
	movl	8(%rbp), %ebp
	movl	%ebp, offset_cr+4(%rip)
.LBB9_7:
	movq	176(%rsp), %rbp         # 8-byte Reload
	leaq	-1(%rbp), %rsi
	cmpl	$0, 4104(%rdi)
	je	.LBB9_12
# BB#8:
	movq	EPZSMotion(%rip), %rdi
	movq	(%rdi,%r10,8), %rdi
	movq	104(%rsp), %rbp         # 8-byte Reload
	movq	(%rdi,%rbp,8), %rdi
	movq	(%rdi,%rsi,8), %rdi
	movswq	%ax, %rbp
	movq	(%rdi,%rbp,8), %rdi
	movswq	112(%rsp), %rbp         # 2-byte Folded Reload
	movq	(%rdi,%rbp,8), %rbp
	jmp	.LBB9_13
.LBB9_9:
	cmpl	$0, 196(%rax)
	je	.LBB9_36
# BB#10:
	cmpl	$1, 20(%rdx)
	je	.LBB9_1
# BB#11:
	xorl	%r15d, %r15d
	jmp	.LBB9_2
.LBB9_12:
	xorl	%ebp, %ebp
.LBB9_13:
	movq	136(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14,4), %edi
	movl	%edi, 256(%rsp)         # 4-byte Spill
	movl	504(%rsp), %eax
	movq	120(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%rax,4), %edi
	movl	%edi, 252(%rsp)         # 4-byte Spill
	testl	%r8d, %r8d
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	js	.LBB9_17
# BB#14:
	xorl	%edi, %edi
	testl	%r9d, %r9d
	js	.LBB9_18
# BB#15:
	movswl	%dx, %edx
	subl	100(%rsp), %edx         # 4-byte Folded Reload
	cmpl	%edx, %r8d
	jge	.LBB9_18
# BB#16:
	movswl	%r11w, %edx
	subl	116(%rsp), %edx         # 4-byte Folded Reload
	cmpl	%edx, %r9d
	setl	%dil
	jmp	.LBB9_18
.LBB9_17:
	xorl	%edi, %edi
.LBB9_18:
	xorb	$1, %dil
	movzbl	%dil, %edx
	movl	%edx, ref_access_method(%rip)
	movq	EPZSMap(%rip), %rdx
	movslq	536(%rsp), %rsi
	movq	(%rdx,%rsi,8), %rdx
	movw	%cx, (%rdx,%rsi,2)
	movq	mvbits(%rip), %rcx
	movl	%r8d, %edx
	subl	256(%rsp), %edx         # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	%r9d, %esi
	subl	252(%rsp), %esi         # 4-byte Folded Reload
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %ebx
	addl	(%rcx,%rdx,4), %ebx
	imull	552(%rsp), %ebx
	sarl	$16, %ebx
	movzbl	%r15b, %eax
	leaq	(%rax,%rax,2), %rax
	addl	$80, %r8d
	addl	$80, %r9d
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	movq	312(%rsp), %rdi         # 8-byte Reload
	movl	116(%rsp), %esi         # 4-byte Reload
	movl	100(%rsp), %edx         # 4-byte Reload
	movq	%rax, 328(%rsp)         # 8-byte Spill
	callq	*computeUniPred(,%rax,8)
	addl	%ebx, %eax
	movl	%eax, 136(%rsp)         # 4-byte Spill
	cmpw	$0, 156(%rsp)           # 2-byte Folded Reload
	jle	.LBB9_20
# BB#19:
	movq	img(%rip), %rax
	cmpl	$0, 24(%rax)
	je	.LBB9_31
.LBB9_20:
	movl	$1, %eax
	movl	536(%rsp), %ecx
	movl	192(%rsp), %edi         # 4-byte Reload
	cmpl	%ecx, %edi
	jle	.LBB9_24
# BB#21:
	movl	%ecx, %esi
	movswl	img_width(%rip), %edx
	subl	100(%rsp), %edx         # 4-byte Folded Reload
	movq	input(%rip), %rcx
	movl	4120(%rcx), %ecx
	addl	%ecx, %ecx
	shll	%cl, %edx
	movl	208(%rsp), %ebx         # 4-byte Reload
	cmpl	%esi, %ebx
	jle	.LBB9_24
# BB#22:
	subl	%esi, %edx
	cmpl	%edx, %edi
	movq	224(%rsp), %r11         # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movl	96(%rsp), %edi          # 4-byte Reload
	jge	.LBB9_25
# BB#23:
	movswl	img_height(%rip), %edx
	subl	116(%rsp), %edx         # 4-byte Folded Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	subl	536(%rsp), %edx
	xorl	%eax, %eax
	cmpl	%edx, %ebx
	setge	%al
	jmp	.LBB9_25
.LBB9_24:
	movq	224(%rsp), %r11         # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movl	96(%rsp), %edi          # 4-byte Reload
.LBB9_25:
	movl	%eax, ref_access_method(%rip)
	cmpl	%edi, 136(%rsp)         # 4-byte Folded Reload
	jle	.LBB9_30
# BB#26:
	movq	%rbp, 208(%rsp)         # 8-byte Spill
	movl	168(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r14d
	shll	$16, %r14d
	movswl	%ax, %ebx
	movswl	%r13w, %r13d
	movq	img(%rip), %rax
	movl	160(%rax), %ecx
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	movl	164(%rax), %ecx
	movl	%ecx, 120(%rsp)         # 4-byte Spill
	movswl	img_width(%rip), %ebp
	sarl	$4, %ebp
	decl	%ebp
	movl	%r12d, 168(%rsp)        # 4-byte Spill
	movswl	img_height(%rip), %r12d
	sarl	$4, %r12d
	decl	%r12d
	movl	12(%rax), %edi
	leal	-1(%rbx), %r15d
	leaq	392(%rsp), %rcx
	movl	%r15d, %esi
	movl	%r13d, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	movq	%r13, 192(%rsp)         # 8-byte Spill
	leal	-1(%r13), %r13d
	leaq	368(%rsp), %rcx
	movl	%ebx, %esi
	movl	%r13d, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	addl	100(%rsp), %ebx         # 4-byte Folded Reload
	leaq	336(%rsp), %rcx
	movl	%ebx, %esi
	movl	%r13d, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leaq	416(%rsp), %rcx
	movl	%r15d, %esi
	movl	%r13d, %edx
	callq	getLuma4x4Neighbour
	cmpl	$0, 168(%rsp)           # 4-byte Folded Reload
	movl	%r14d, 248(%rsp)        # 4-byte Spill
	jle	.LBB9_35
# BB#27:
	movl	96(%rsp), %eax          # 4-byte Reload
	cmpl	$524287, %r14d          # imm = 0x7FFFF
	movl	136(%rsp), %edi         # 4-byte Reload
	movq	200(%rsp), %rsi         # 8-byte Reload
	jg	.LBB9_37
# BB#28:
	movq	192(%rsp), %r15         # 8-byte Reload
	cmpl	$8, %r15d
	jne	.LBB9_38
# BB#29:
	cmpl	%ebp, %eax
	setl	%al
	movl	100(%rsp), %edx         # 4-byte Reload
	cmpl	$16, %edx
	setne	%cl
	orb	%al, %cl
	cmpl	$16, %edx
	jmp	.LBB9_39
.LBB9_30:
	movq	528(%rsp), %r10
	movq	%rsi, %rdi
	movl	136(%rsp), %r12d        # 4-byte Reload
	jmp	.LBB9_193
.LBB9_31:
	movswq	112(%rsp), %rax         # 2-byte Folded Reload
	movq	240(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %eax
	cmpl	136(%rsp), %eax         # 4-byte Folded Reload
	jge	.LBB9_20
# BB#32:
	movq	176(%rsp), %rcx         # 8-byte Reload
	cmpl	medthres(,%rcx,4), %eax
	jge	.LBB9_20
# BB#33:
	movq	input(%rip), %rax
	cmpl	$0, 4104(%rax)
	je	.LBB9_204
# BB#34:
	movzwl	248(%rsp), %eax         # 2-byte Folded Reload
	movw	%ax, (%rbp)
	movzwl	164(%rsp), %eax         # 2-byte Folded Reload
	movw	%ax, 2(%rbp)
	movl	136(%rsp), %r12d        # 4-byte Reload
	jmp	.LBB9_199
.LBB9_35:
	cmpl	%ebp, 96(%rsp)          # 4-byte Folded Reload
	setl	%al
	cmpl	$16, %ebx
	setne	%cl
	orb	%al, %cl
	movzbl	%cl, %eax
	movl	%eax, 164(%rsp)         # 4-byte Spill
	movl	136(%rsp), %edi         # 4-byte Reload
	movq	200(%rsp), %rbp         # 8-byte Reload
	movq	192(%rsp), %r15         # 8-byte Reload
	jmp	.LBB9_42
.LBB9_36:
	xorl	%r15d, %r15d
	jmp	.LBB9_2
.LBB9_37:
	cmpl	%ebp, %eax
	setl	%al
	cmpl	$16, %ebx
	setne	%cl
	orb	%al, %cl
	cmpl	$16, %ebx
	movzbl	%cl, %eax
	movl	%eax, 164(%rsp)         # 4-byte Spill
	movq	192(%rsp), %r15         # 8-byte Reload
	je	.LBB9_40
	jmp	.LBB9_41
.LBB9_38:
	cmpl	%ebp, %eax
	setl	%al
	cmpl	$8, %ebx
	setne	%cl
	orb	%al, %cl
	cmpl	$8, %ebx
.LBB9_39:
	movzbl	%cl, %eax
	movl	%eax, 164(%rsp)         # 4-byte Spill
	jne	.LBB9_41
.LBB9_40:
	movl	$0, 336(%rsp)
.LBB9_41:
	movq	%rsi, %rbp
.LBB9_42:
	movl	100(%rsp), %esi         # 4-byte Reload
	sarl	$2, %esi
	cmpl	%r12d, 120(%rsp)        # 4-byte Folded Reload
	setl	163(%rsp)               # 1-byte Folded Spill
	addl	116(%rsp), %r15d        # 4-byte Folded Reload
	cmpl	$16, %r15d
	setne	%bl
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	cmpl	$0, 392(%rsp)
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	je	.LBB9_44
# BB#43:
	movswl	112(%rsp), %ecx         # 2-byte Folded Reload
	subl	%esi, %ecx
	movslq	%ecx, %rcx
	movq	240(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx,%rcx,4), %ecx
.LBB9_44:
	cmpl	$0, 368(%rsp)
	movl	%edi, %r12d
	je	.LBB9_46
# BB#45:
	movswq	112(%rsp), %rax         # 2-byte Folded Reload
	movq	240(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx,%rax,4), %eax
.LBB9_46:
	movswl	156(%rsp), %r13d        # 2-byte Folded Reload
	cmpl	$0, 336(%rsp)
	movl	%esi, 96(%rsp)          # 4-byte Spill
	movb	%bl, 168(%rsp)          # 1-byte Spill
	je	.LBB9_48
# BB#47:
	movswl	112(%rsp), %edx         # 2-byte Folded Reload
	addl	%esi, %edx
	movslq	%edx, %rdx
	movq	240(%rsp), %rsi         # 8-byte Reload
	movl	(%rsi,%rdx,4), %edx
	jmp	.LBB9_49
.LBB9_48:
	movl	$2147483647, %edx       # imm = 0x7FFFFFFF
.LBB9_49:
	movl	116(%rsp), %esi         # 4-byte Reload
	movl	%esi, %r15d
	sarl	$2, %r15d
	cmpl	%edx, %eax
	cmovlel	%eax, %edx
	cmpl	%edx, %ecx
	cmovlel	%ecx, %edx
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	minthres(,%rcx,4), %eax
	cmpl	%eax, %edx
	cmovll	%eax, %edx
	movl	maxthres(,%rcx,4), %eax
	cmpl	%eax, %edx
	cmovgl	%eax, %edx
	movl	medthres(,%rcx,4), %eax
	cmpl	%edx, %eax
	cmovgel	%eax, %edx
	leal	(%rdx,%rdx,8), %ecx
	leal	(%rcx,%rax,2), %r14d
	movslq	184(%rsp), %rdi         # 4-byte Folded Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdi,8), %rcx
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdi,8), %r8
	movq	predictor(%rip), %rax
	movq	8(%rax), %r9
	movq	432(%rsp), %rax
	movq	%rax, 88(%rsp)
	movups	416(%rsp), %xmm0
	movups	%xmm0, 72(%rsp)
	movq	352(%rsp), %rax
	movq	%rax, 64(%rsp)
	movups	336(%rsp), %xmm0
	movups	%xmm0, 48(%rsp)
	movq	384(%rsp), %rax
	movq	%rax, 40(%rsp)
	movups	368(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	408(%rsp), %rax
	movq	%rax, 16(%rsp)
	movups	392(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	movq	%rdi, 192(%rsp)         # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%ebp, %esi
	movl	%r13d, %edx
	callq	EPZSSpatialPredictors
	movw	%ax, 262(%rsp)          # 2-byte Spill
	movq	input(%rip), %r10
	cmpl	$0, 4104(%r10)
	movl	%r13d, 120(%rsp)        # 4-byte Spill
	movq	%r15, 304(%rsp)         # 8-byte Spill
	je	.LBB9_52
# BB#50:
	movl	112(%rsp), %eax         # 4-byte Reload
	movl	%eax, %edi
	shll	$16, %edi
	movswl	%ax, %edx
	movswl	img_width(%rip), %r9d
	sarl	$2, %r9d
	movq	predictor(%rip), %rax
	movq	8(%rax), %rax
	movq	EPZSMotion(%rip), %rsi
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rsi,%rcx,8), %rsi
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	(%rsi,%rcx,8), %rsi
	movq	264(%rsp), %rcx         # 8-byte Reload
	movq	(%rsi,%rcx,8), %r8
	testl	%edi, %edi
	movq	272(%rsp), %rsi         # 8-byte Reload
	jle	.LBB9_53
# BB#51:
	movslq	%esi, %rdi
	movq	(%r8,%rdi,8), %rdi
	movl	%edx, %ecx
	movl	96(%rsp), %r13d         # 4-byte Reload
	subl	%r13d, %ecx
	movslq	%ecx, %rcx
	movq	(%rdi,%rcx,8), %rcx
	movswl	(%rcx), %ebp
	movl	%ebp, 80(%rax)
	movswl	2(%rcx), %ebx
	movl	%r14d, %r11d
	jmp	.LBB9_54
.LBB9_52:
	movl	$5, %r15d
	movl	%r14d, %r11d
	jmp	.LBB9_58
.LBB9_53:                               # %.critedge.i
	movl	$0, 80(%rax)
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movl	%r14d, %r11d
	movl	96(%rsp), %r13d         # 4-byte Reload
.LBB9_54:
	movl	%ebx, 84(%rax)
	xorl	%edi, %edi
	orl	%ebx, %ebp
	setne	%dil
	leal	5(%rdi), %ecx
	shlq	$4, %rcx
	testl	%esi, %esi
	movl	$4, %ebp
	cmovgl	%esi, %ebp
	subl	%r15d, %ebp
	movslq	%ebp, %rbp
	movq	(%r8,%rbp,8), %rbp
	movslq	%edx, %rsi
	movq	(%rbp,%rsi,8), %rsi
	movswl	(%rsi), %ebx
	movl	%ebx, (%rax,%rcx)
	movswl	2(%rsi), %esi
	movl	%esi, 4(%rax,%rcx)
	xorl	%ecx, %ecx
	orw	%bx, %si
	setne	%cl
	leal	5(%rcx,%rdi), %esi
	movq	%rsi, %rdi
	shlq	$4, %rdi
	leaq	(%rax,%rdi), %rbx
	addl	%r13d, %edx
	cmpl	%r9d, %edx
	jge	.LBB9_56
# BB#55:
	movslq	%edx, %rcx
	movq	(%rbp,%rcx,8), %rdx
	movswl	(%rdx), %ecx
	movl	%ecx, (%rbx)
	movswl	2(%rdx), %edx
	jmp	.LBB9_57
.LBB9_56:                               # %.critedge107.i
	movl	$0, (%rbx)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.LBB9_57:                               # %EPZSSpatialMemPredictors.exit
	movl	120(%rsp), %r13d        # 4-byte Reload
	movq	200(%rsp), %rbp         # 8-byte Reload
	movl	%edx, 4(%rax,%rdi)
	xorl	%r15d, %r15d
	orl	%edx, %ecx
	setne	%r15b
	addl	%esi, %r15d
.LBB9_58:
	sarl	$3, %r11d
	movq	%r10, 216(%rsp)         # 8-byte Spill
	cmpl	$0, 4100(%r10)
	movq	528(%rsp), %r10
	movl	%r11d, 232(%rsp)        # 4-byte Spill
	je	.LBB9_85
# BB#59:
	movswl	112(%rsp), %ebx         # 2-byte Folded Reload
	movl	504(%rsp), %eax
	shrl	$2, %eax
	movswl	%ax, %edi
	movq	predictor(%rip), %rcx
	movl	392(%rsp), %eax
	movl	%eax, 264(%rsp)         # 4-byte Spill
	movl	368(%rsp), %eax
	movl	%eax, 184(%rsp)         # 4-byte Spill
	movq	104(%rsp), %rdx         # 8-byte Reload
	shlq	$7, %rdx
	movq	144(%rsp), %rax         # 8-byte Reload
	shlq	$12, %rax
	movl	mv_scale(%rax,%rdx), %r9d
	testl	%ebp, %ebp
	je	.LBB9_62
# BB#60:
	movq	EPZSCo_located(%rip), %rdx
	cmpl	$2, %ebp
	jne	.LBB9_63
# BB#61:
	addq	$24, %rdx
	jmp	.LBB9_64
.LBB9_62:
	movq	EPZSCo_located(%rip), %rdx
	addq	$16, %rdx
	jmp	.LBB9_64
.LBB9_63:
	addq	$32, %rdx
.LBB9_64:
	movq	(%rdx), %rdx
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	(%rdx,%rax,8), %rbp
	movl	mv_rescale(%rip), %edx
	movq	8(%rcx), %r13
	movslq	%r15d, %r15
	movq	%r15, %rsi
	shlq	$4, %rsi
	movq	%rdi, %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movslq	%edi, %rax
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	(%rbp,%rax,8), %rbp
	movl	%ebx, 324(%rsp)         # 4-byte Spill
	movslq	%ebx, %rdi
	movq	(%rbp,%rdi,8), %r12
	movswl	(%r12), %r11d
	imull	%r9d, %r11d
	shll	$16, %edx
	addl	$524288, %edx           # imm = 0x80000
	sarl	$16, %edx
	leal	-1(%rdx), %ecx
	movl	$1, %r14d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r14d
	addl	%r14d, %r11d
	movl	%r11d, %eax
	movl	%edx, %ecx
	sarl	%cl, %eax
	movl	%eax, (%r13,%rsi)
	movswl	2(%r12), %eax
	imull	%r9d, %eax
	addl	%r14d, %eax
	orl	%eax, %r11d
	sarl	%cl, %eax
	movl	%eax, 4(%r13,%rsi)
	sarl	%cl, %r11d
	cmpl	$1, %r11d
	sbbl	$-1, %r15d
	cmpl	$1, 120(%rsp)           # 4-byte Folded Reload
	jg	.LBB9_79
# BB#65:
	movl	136(%rsp), %r12d        # 4-byte Reload
	movl	232(%rsp), %r11d        # 4-byte Reload
	cmpl	%r11d, %r12d
	jle	.LBB9_84
# BB#66:
	movb	168(%rsp), %r10b        # 1-byte Reload
	orb	163(%rsp), %r10b        # 1-byte Folded Reload
	cmpl	$0, 264(%rsp)           # 4-byte Folded Reload
	movq	%rdi, 168(%rsp)         # 8-byte Spill
	je	.LBB9_72
# BB#67:
	movslq	%r15d, %r15
	movq	%r15, %rsi
	shlq	$4, %rsi
	leaq	-1(%rdi), %rbx
	movq	%rbp, 264(%rsp)         # 8-byte Spill
	movq	-8(%rbp,%rdi,8), %rax
	movswl	(%rax), %edi
	imull	%r9d, %edi
	addl	%r14d, %edi
	movl	%edi, %ebp
	movl	%edx, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%r13,%rsi)
	movswl	2(%rax), %eax
	imull	%r9d, %eax
	addl	%r14d, %eax
	orl	%eax, %edi
	sarl	%cl, %eax
	movl	%eax, 4(%r13,%rsi)
	sarl	%cl, %edi
	cmpl	$1, %edi
	sbbl	$-1, %r15d
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	je	.LBB9_69
# BB#68:
	movslq	%r15d, %r15
	movq	%r15, %rax
	shlq	$4, %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	-8(%rcx,%r8,8), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movswl	(%rsi), %edi
	imull	%r9d, %edi
	addl	%r14d, %edi
	movl	%edi, %ebp
	movl	%edx, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%r13,%rax)
	movswl	2(%rsi), %esi
	imull	%r9d, %esi
	addl	%r14d, %esi
	orl	%esi, %edi
	sarl	%cl, %esi
	movl	%esi, 4(%r13,%rax)
	sarl	%cl, %edi
	cmpl	$1, %edi
	sbbl	$-1, %r15d
.LBB9_69:
	testb	%r10b, %r10b
	je	.LBB9_71
# BB#70:
	movslq	%r15d, %r15
	movq	%r15, %rax
	shlq	$4, %rax
	movq	304(%rsp), %rcx         # 8-byte Reload
	movq	296(%rsp), %rsi         # 8-byte Reload
	leal	(%rcx,%rsi), %ecx
	movslq	%ecx, %rcx
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rcx,8), %rcx
	movq	(%rcx,%rbx,8), %rsi
	movswl	(%rsi), %edi
	imull	%r9d, %edi
	addl	%r14d, %edi
	movl	%edi, %ebp
	movl	%edx, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%r13,%rax)
	movswl	2(%rsi), %esi
	imull	%r9d, %esi
	addl	%r14d, %esi
	orl	%esi, %edi
	sarl	%cl, %esi
	movl	%esi, 4(%r13,%rax)
	sarl	%cl, %edi
	cmpl	$1, %edi
	sbbl	$-1, %r15d
.LBB9_71:
	movq	168(%rsp), %rdi         # 8-byte Reload
	movq	264(%rsp), %rbp         # 8-byte Reload
.LBB9_72:
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	je	.LBB9_74
# BB#73:
	movslq	%r15d, %r15
	movq	%r15, %rax
	shlq	$4, %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	-8(%rcx,%r8,8), %rcx
	movq	(%rcx,%rdi,8), %rsi
	movswl	(%rsi), %edi
	imull	%r9d, %edi
	addl	%r14d, %edi
	movq	%rbp, %rbx
	movl	%edi, %ebp
	movl	%edx, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%r13,%rax)
	movq	%rbx, %rbp
	movswl	2(%rsi), %esi
	imull	%r9d, %esi
	addl	%r14d, %esi
	orl	%esi, %edi
	sarl	%cl, %esi
	movl	%esi, 4(%r13,%rax)
	sarl	%cl, %edi
	cmpl	$1, %edi
	movq	168(%rsp), %rdi         # 8-byte Reload
	sbbl	$-1, %r15d
.LBB9_74:
	cmpl	$0, 164(%rsp)           # 4-byte Folded Reload
	je	.LBB9_80
# BB#75:
	movq	%r8, %rbx
	movslq	%r15d, %r15
	movq	%r15, %rax
	shlq	$4, %rax
	movl	96(%rsp), %ecx          # 4-byte Reload
	addl	324(%rsp), %ecx         # 4-byte Folded Reload
	movslq	%ecx, %r8
	movq	(%rbp,%r8,8), %rdi
	movswl	(%rdi), %ebp
	imull	%r9d, %ebp
	addl	%r14d, %ebp
	movl	%ebp, %esi
	movl	%edx, %ecx
	sarl	%cl, %esi
	movl	%esi, (%r13,%rax)
	movswl	2(%rdi), %esi
	imull	%r9d, %esi
	addl	%r14d, %esi
	orl	%esi, %ebp
	sarl	%cl, %esi
	movl	%esi, 4(%r13,%rax)
	sarl	%cl, %ebp
	cmpl	$1, %ebp
	sbbl	$-1, %r15d
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	je	.LBB9_77
# BB#76:
	movslq	%r15d, %r15
	movq	%r15, %rax
	shlq	$4, %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	-8(%rcx,%rbx,8), %rcx
	movq	(%rcx,%r8,8), %rsi
	movswl	(%rsi), %edi
	imull	%r9d, %edi
	addl	%r14d, %edi
	movl	%edi, %ebp
	movl	%edx, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%r13,%rax)
	movswl	2(%rsi), %esi
	imull	%r9d, %esi
	addl	%r14d, %esi
	orl	%esi, %edi
	sarl	%cl, %esi
	movl	%esi, 4(%r13,%rax)
	sarl	%cl, %edi
	cmpl	$1, %edi
	sbbl	$-1, %r15d
.LBB9_77:
	testb	%r10b, %r10b
	je	.LBB9_83
# BB#78:                                # %.thread.i611
	movslq	%r15d, %r15
	movq	%r15, %rsi
	shlq	$4, %rsi
	movq	304(%rsp), %rax         # 8-byte Reload
	addl	296(%rsp), %eax         # 4-byte Folded Reload
	cltq
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %r10
	movq	(%r10,%r8,8), %rdi
	movswl	(%rdi), %ebp
	imull	%r9d, %ebp
	addl	%r14d, %ebp
	movl	%ebp, %eax
	movl	%edx, %ecx
	sarl	%cl, %eax
	movl	%eax, (%r13,%rsi)
	movswl	2(%rdi), %eax
	imull	%r9d, %eax
	addl	%r14d, %eax
	orl	%eax, %ebp
	sarl	%cl, %eax
	movl	%eax, 4(%r13,%rsi)
	sarl	%cl, %ebp
	cmpl	$1, %ebp
	sbbl	$-1, %r15d
	movq	168(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB9_82
.LBB9_79:
	movl	136(%rsp), %r12d        # 4-byte Reload
	movl	232(%rsp), %r11d        # 4-byte Reload
	jmp	.LBB9_84
.LBB9_80:
	testb	%r10b, %r10b
	je	.LBB9_83
# BB#81:                                # %._crit_edge.i
	movq	304(%rsp), %rax         # 8-byte Reload
	addl	296(%rsp), %eax         # 4-byte Folded Reload
	cltq
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %r10
.LBB9_82:
	movslq	%r15d, %r15
	movq	%r15, %rax
	shlq	$4, %rax
	movq	(%r10,%rdi,8), %rsi
	movswl	(%rsi), %edi
	imull	%r9d, %edi
	addl	%r14d, %edi
	movl	%edi, %ebp
	movl	%edx, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%r13,%rax)
	movswl	2(%rsi), %esi
	imull	%r9d, %esi
	addl	%r14d, %esi
	orl	%esi, %edi
	sarl	%cl, %esi
	movl	%esi, 4(%r13,%rax)
	sarl	%cl, %edi
	cmpl	$1, %edi
	sbbl	$-1, %r15d
.LBB9_83:                               # %EPZSTemporalPredictors.exit
	movq	528(%rsp), %r10
.LBB9_84:                               # %EPZSTemporalPredictors.exit
	movq	200(%rsp), %rbp         # 8-byte Reload
	movl	120(%rsp), %r13d        # 4-byte Reload
.LBB9_85:                               # %EPZSTemporalPredictors.exit
	cmpl	%r11d, %r12d
	jle	.LBB9_100
# BB#86:
	cmpl	$1, %r13d
	jg	.LBB9_88
# BB#87:
	cmpl	$5, 512(%rsp)
	jl	.LBB9_90
.LBB9_88:
	cmpl	$2, %r13d
	jg	.LBB9_100
# BB#89:
	movq	img(%rip), %rax
	movl	24(%rax), %eax
	orl	%ebp, %eax
	je	.LBB9_100
.LBB9_90:
	movq	216(%rsp), %rax         # 8-byte Reload
	movl	4096(%rax), %eax
	cmpl	$1, %eax
	jle	.LBB9_94
.LBB9_91:                               # %.critedge589
	movq	predictor(%rip), %rdx
	cmpl	$4, 512(%rsp)
	jg	.LBB9_96
# BB#92:                                # %.critedge589
	movswl	262(%rsp), %eax         # 2-byte Folded Reload
	cmpl	$2, %eax
	jle	.LBB9_96
# BB#93:
	movq	img(%rip), %rax
	xorl	%ecx, %ecx
	orl	24(%rax), %ebp
	setne	%cl
	incl	%ecx
	cmpl	%ecx, %r13d
	movl	$window_predictor_extended, %eax
	movl	$window_predictor, %ecx
	cmovlq	%rax, %rcx
	movq	(%rcx), %rax
	cmpl	$0, (%rax)
	jg	.LBB9_97
	jmp	.LBB9_100
.LBB9_94:
	testl	%eax, %eax
	je	.LBB9_100
# BB#95:
	movq	img(%rip), %rax
	cmpl	$0, 20(%rax)
	jne	.LBB9_100
	jmp	.LBB9_91
.LBB9_96:                               # %.thread
	movq	window_predictor(%rip), %rax
	cmpl	$0, (%rax)
	jle	.LBB9_100
.LBB9_97:                               # %.lr.ph.i
	movswl	(%r10), %ecx
	movq	8(%rax), %rsi
	movq	8(%rdx), %rbx
	movswl	2(%r10), %edx
	movslq	%r15d, %rbp
	shlq	$4, %rbp
	leaq	4(%rbx,%rbp), %rbp
	addq	$4, %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB9_98:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi), %ebx
	addl	%ecx, %ebx
	movl	%ebx, -4(%rbp)
	movl	(%rsi), %ebx
	addl	%edx, %ebx
	movl	%ebx, (%rbp)
	incq	%rdi
	movslq	(%rax), %rbx
	addq	$16, %rbp
	addq	$16, %rsi
	cmpq	%rbx, %rdi
	jl	.LBB9_98
# BB#99:                                # %.critedge.loopexit
	addl	%edi, %r15d
	movq	528(%rsp), %r10
.LBB9_100:                              # %.critedge
	cmpl	%r11d, %r12d
	setg	%al
	movl	156(%rsp), %edx         # 4-byte Reload
	testw	%dx, %dx
	setg	%cl
	testw	%dx, %dx
	je	.LBB9_102
# BB#101:                               # %.critedge
	andb	%al, %cl
	je	.LBB9_119
.LBB9_102:                              # %.critedge593
	movq	img(%rip), %rdx
	cmpl	$0, 12(%rdx)
	je	.LBB9_119
# BB#103:
	movl	%r13d, %r8d
	movl	248(%rsp), %esi         # 4-byte Reload
	sarl	$18, %esi
	movq	predictor(%rip), %rax
	movq	8(%rax), %r13
	movq	14384(%rdx), %rax
	movslq	272(%rsp), %rcx         # 4-byte Folded Reload
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %r10
	movl	mv_rescale(%rip), %r11d
	movslq	%r15d, %rax
	shlq	$4, %rax
	leaq	(%r13,%rax), %rsi
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	(%r10,%rcx,8), %r9
	movq	176(%rsp), %rcx         # 8-byte Reload
	movswq	blk_parent(%rcx,%rcx), %rcx
	movq	(%r9,%rcx,8), %rcx
	movswl	(%rcx), %ebp
	movswl	2(%rcx), %edi
	testl	%r11d, %r11d
	jle	.LBB9_105
# BB#104:
	leal	-1(%r11), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%ebx, %ebp
	movl	%r11d, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%rsi)
	addl	%ebx, %edi
	sarl	%cl, %edi
	jmp	.LBB9_106
.LBB9_105:
	movl	%r11d, %ecx
	negl	%ecx
	shll	%cl, %ebp
	movl	%ebp, (%rsi)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
.LBB9_106:                              # %rshift_rnd.exit88.i
	movl	%edi, 4(%r13,%rax)
	xorl	%eax, %eax
	orl	%edi, %ebp
	setne	%al
	addl	%r15d, %eax
	cmpw	$0, 156(%rsp)           # 2-byte Folded Reload
	jle	.LBB9_110
# BB#107:
	cmpl	$5, 512(%rsp)
	jl	.LBB9_109
# BB#108:
	cmpl	$0, 24(%rdx)
	je	.LBB9_111
.LBB9_109:
	movslq	%eax, %rsi
	movq	%rsi, %rdi
	shlq	$4, %rdi
	movslq	%r8d, %rax
	movq	104(%rsp), %r8          # 8-byte Reload
	shlq	$7, %r8
	movq	%r9, 104(%rsp)          # 8-byte Spill
	movq	192(%rsp), %r9          # 8-byte Reload
	shlq	$12, %r9
	leaq	(%r9,%r8), %rcx
	movl	mv_scale-4(%rcx,%rax,4), %r14d
	movq	-8(%r10,%rax,8), %rax
	movq	%r10, 144(%rsp)         # 8-byte Spill
	movq	176(%rsp), %r10         # 8-byte Reload
	movq	(%rax,%r10,8), %r15
	movswl	(%r15), %eax
	imull	%r14d, %eax
	movl	%r11d, %edx
	shll	$16, %edx
	addl	$524288, %edx           # imm = 0x80000
	sarl	$16, %edx
	leal	-1(%rdx), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	%ebp, %eax
	movl	%eax, %ebx
	movl	%edx, %ecx
	sarl	%cl, %ebx
	movl	%ebx, (%r13,%rdi)
	movswl	2(%r15), %ebx
	imull	%r14d, %ebx
	addl	%ebp, %ebx
	orl	%ebx, %eax
	sarl	%cl, %ebx
	movl	%ebx, 4(%r13,%rdi)
	sarl	%cl, %eax
	cmpl	$1, %eax
	sbbl	$-1, %esi
	movslq	%esi, %rax
	movq	%rax, %rsi
	shlq	$4, %rsi
	movl	mv_scale(%r9,%r8), %r14d
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %rcx
	movq	(%rcx,%r10,8), %r9
	movswl	(%r9), %edi
	imull	%r14d, %edi
	addl	%ebp, %edi
	movl	%edi, %ebx
	movl	%edx, %ecx
	sarl	%cl, %ebx
	movl	%ebx, (%r13,%rsi)
	movswl	2(%r9), %ebx
	movq	104(%rsp), %r9          # 8-byte Reload
	imull	%r14d, %ebx
	addl	%ebp, %ebx
	orl	%ebx, %edi
	sarl	%cl, %ebx
	movl	%ebx, 4(%r13,%rsi)
	sarl	%cl, %edi
	cmpl	$1, %edi
	sbbl	$-1, %eax
.LBB9_110:
	cmpl	$1, 512(%rsp)
	je	.LBB9_115
.LBB9_111:                              # %.thread.i
	movslq	%eax, %rdx
	shlq	$4, %rdx
	leaq	(%r13,%rdx), %rbp
	movq	8(%r9), %rcx
	movswl	(%rcx), %esi
	movswl	2(%rcx), %edi
	testl	%r11d, %r11d
	jle	.LBB9_113
# BB#112:
	leal	-1(%r11), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%ebx, %esi
	movl	%r11d, %ecx
	sarl	%cl, %esi
	movl	%esi, (%rbp)
	addl	%ebx, %edi
	sarl	%cl, %edi
	jmp	.LBB9_114
.LBB9_204:
	movl	136(%rsp), %r12d        # 4-byte Reload
	jmp	.LBB9_199
.LBB9_113:
	movl	%r11d, %ecx
	negl	%ecx
	shll	%cl, %esi
	movl	%esi, (%rbp)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
.LBB9_114:                              # %rshift_rnd.exit86.i
	movl	%edi, 4(%r13,%rdx)
	xorl	%ecx, %ecx
	orl	%edi, %esi
	setne	%cl
	addl	%ecx, %eax
	cmpl	$4, 512(%rsp)
	movl	%eax, %r15d
	movq	528(%rsp), %r10
	je	.LBB9_119
.LBB9_115:
	movslq	%eax, %rdx
	shlq	$4, %rdx
	leaq	(%r13,%rdx), %rsi
	movq	32(%r9), %rcx
	movswl	(%rcx), %ebp
	movswl	2(%rcx), %edi
	testl	%r11d, %r11d
	jle	.LBB9_117
# BB#116:
	leal	-1(%r11), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%ebx, %ebp
	movl	%r11d, %ecx
	sarl	%cl, %ebp
	movl	%ebp, (%rsi)
	addl	%ebx, %edi
	sarl	%cl, %edi
	jmp	.LBB9_118
.LBB9_117:
	negl	%r11d
	movl	%r11d, %ecx
	shll	%cl, %ebp
	movl	%ebp, (%rsi)
	shll	%cl, %edi
.LBB9_118:                              # %rshift_rnd.exit84.i
	movl	%edi, 4(%r13,%rdx)
	xorl	%r15d, %r15d
	orl	%edi, %ebp
	setne	%r15b
	addl	%eax, %r15d
	movq	528(%rsp), %r10
.LBB9_119:                              # %EPZSBlockTypePredictors.exit.preheader
	movl	536(%rsp), %edx
	movl	%edx, %eax
	movq	224(%rsp), %r11         # 8-byte Reload
	subl	%r11d, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movl	%edx, %eax
	movq	128(%rsp), %rdi         # 8-byte Reload
	subl	%edi, %eax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	testl	%r15d, %r15d
	jle	.LBB9_137
# BB#120:                               # %.lr.ph.preheader
	movl	%r15d, %r15d
	movl	$0, 96(%rsp)            # 4-byte Folded Spill
	movl	$2147483647, 104(%rsp)  # 4-byte Folded Spill
                                        # imm = 0x7FFFFFFF
	movl	$4, %ebp
	movl	$0, 184(%rsp)           # 4-byte Folded Spill
	movl	$0, 216(%rsp)           # 4-byte Folded Spill
	movq	280(%rsp), %rsi         # 8-byte Reload
	.p2align	4, 0x90
.LBB9_121:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	predictor(%rip), %rax
	movq	8(%rax), %rax
	movl	-4(%rax,%rbp), %ebx
	movswl	(%r10), %ecx
	movl	%ebx, %edx
	subl	%ecx, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	536(%rsp), %edx
	cmpl	%edx, %ecx
	jg	.LBB9_136
# BB#122:                               #   in Loop: Header=BB9_121 Depth=1
	movl	(%rax,%rbp), %r14d
	movswl	2(%r10), %eax
	movl	%r14d, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	cmpl	%edx, %eax
	jg	.LBB9_136
# BB#123:                               #   in Loop: Header=BB9_121 Depth=1
	movq	EPZSMap(%rip), %rax
	movq	272(%rsp), %rcx         # 8-byte Reload
	leal	(%r14,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	leal	(%rbx,%rcx), %ecx
	movslq	%ecx, %rcx
	movzwl	EPZSBlkCount(%rip), %edx
	cmpw	%dx, (%rax,%rcx,2)
	je	.LBB9_135
# BB#124:                               #   in Loop: Header=BB9_121 Depth=1
	movw	%dx, (%rax,%rcx,2)
	leal	(%rbx,%rsi), %r8d
	movzbl	mv_rescale(%rip), %ecx
	shll	%cl, %r8d
	movq	288(%rsp), %rax         # 8-byte Reload
	leal	(%r14,%rax), %r9d
	shll	%cl, %r9d
	movq	mvbits(%rip), %rax
	movl	%r8d, %ecx
	subl	256(%rsp), %ecx         # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movl	%r9d, %edx
	subl	252(%rsp), %edx         # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %r13d
	addl	(%rax,%rcx,4), %r13d
	imull	552(%rsp), %r13d
	sarl	$16, %r13d
	movl	104(%rsp), %ecx         # 4-byte Reload
	subl	%r13d, %ecx
	jle	.LBB9_135
# BB#125:                               #   in Loop: Header=BB9_121 Depth=1
	testl	%r8d, %r8d
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%r11, 224(%rsp)         # 8-byte Spill
	js	.LBB9_129
# BB#126:                               #   in Loop: Header=BB9_121 Depth=1
	xorl	%eax, %eax
	testl	%r9d, %r9d
	js	.LBB9_130
# BB#127:                               #   in Loop: Header=BB9_121 Depth=1
	movswl	img_width(%rip), %edx
	subl	100(%rsp), %edx         # 4-byte Folded Reload
	cmpl	%edx, %r8d
	jge	.LBB9_130
# BB#128:                               #   in Loop: Header=BB9_121 Depth=1
	movswl	img_height(%rip), %eax
	subl	116(%rsp), %eax         # 4-byte Folded Reload
	cmpl	%eax, %r9d
	setl	%al
	jmp	.LBB9_130
.LBB9_129:                              #   in Loop: Header=BB9_121 Depth=1
	xorl	%eax, %eax
.LBB9_130:                              #   in Loop: Header=BB9_121 Depth=1
	xorb	$1, %al
	movzbl	%al, %eax
	movl	%eax, ref_access_method(%rip)
	addl	$80, %r8d
	addl	$80, %r9d
	movq	312(%rsp), %rdi         # 8-byte Reload
	movl	116(%rsp), %esi         # 4-byte Reload
	movl	100(%rsp), %edx         # 4-byte Reload
	movq	328(%rsp), %rax         # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	addl	%r13d, %eax
	movl	%r12d, %edx
	cmpl	%edx, %eax
	jge	.LBB9_132
# BB#131:                               #   in Loop: Header=BB9_121 Depth=1
	movq	224(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ecx, 216(%rsp)         # 4-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ecx, 184(%rsp)         # 4-byte Spill
	movl	%ebx, %r11d
	movl	%edx, 104(%rsp)         # 4-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	movb	$1, %cl
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	movl	%eax, %r12d
	movq	528(%rsp), %r10
	movq	%r14, %rdi
	movq	280(%rsp), %rsi         # 8-byte Reload
	jmp	.LBB9_135
.LBB9_132:                              #   in Loop: Header=BB9_121 Depth=1
	movl	104(%rsp), %edx         # 4-byte Reload
	cmpl	%edx, %eax
	movl	216(%rsp), %ecx         # 4-byte Reload
	cmovll	%ebx, %ecx
	movl	%ecx, 216(%rsp)         # 4-byte Spill
	movl	184(%rsp), %ecx         # 4-byte Reload
	cmovll	%r14d, %ecx
	movl	%ecx, 184(%rsp)         # 4-byte Spill
	cmovlel	%eax, %edx
	movl	%edx, 104(%rsp)         # 4-byte Spill
	movb	$1, %al
	movq	528(%rsp), %r10
	movq	224(%rsp), %r11         # 8-byte Reload
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	280(%rsp), %rsi         # 8-byte Reload
	jl	.LBB9_134
# BB#133:                               #   in Loop: Header=BB9_121 Depth=1
	movl	96(%rsp), %eax          # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
.LBB9_134:                              #   in Loop: Header=BB9_121 Depth=1
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movl	%eax, 96(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB9_135:                              # %EPZSBlockTypePredictors.exit
                                        #   in Loop: Header=BB9_121 Depth=1
	movl	536(%rsp), %edx
.LBB9_136:                              # %EPZSBlockTypePredictors.exit
                                        #   in Loop: Header=BB9_121 Depth=1
	addq	$16, %rbp
	decq	%r15
	jne	.LBB9_121
	jmp	.LBB9_138
.LBB9_137:
	movl	$0, 216(%rsp)           # 4-byte Folded Spill
	movl	$0, 184(%rsp)           # 4-byte Folded Spill
	movl	$0, 96(%rsp)            # 4-byte Folded Spill
	movq	280(%rsp), %rsi         # 8-byte Reload
.LBB9_138:                              # %EPZSBlockTypePredictors.exit._crit_edge
	movb	$1, %al
	cmpl	232(%rsp), %r12d        # 4-byte Folded Reload
	jle	.LBB9_203
# BB#139:
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	input(%rip), %rax
	cmpl	$0, 4088(%rax)
	movq	288(%rsp), %rbp         # 8-byte Reload
	movq	360(%rsp), %rdi         # 8-byte Reload
	je	.LBB9_151
# BB#140:
	movq	176(%rsp), %rax         # 8-byte Reload
	imull	$3, medthres(,%rax,4), %eax
	sarl	%eax
	addl	232(%rsp), %eax         # 4-byte Folded Reload
	cmpl	%eax, %r12d
	jge	.LBB9_145
# BB#141:
	movq	128(%rsp), %rdi         # 8-byte Reload
	movl	%edi, %eax
	orl	%r11d, %eax
	je	.LBB9_144
# BB#142:
	movswl	(%r10), %eax
	movl	%r11d, %ecx
	subl	%eax, %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movl	$2, %eax
	movl	$2, %ecx
	subl	mv_rescale(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	cmpl	%eax, %edx
	movl	536(%rsp), %edx
	jge	.LBB9_150
# BB#143:
	movswl	2(%r10), %ecx
	movl	%edi, %edx
	subl	%ecx, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	movl	536(%rsp), %edx
	cmpl	%eax, %ecx
	jge	.LBB9_150
.LBB9_144:
	movq	sdiamond(%rip), %rdi
	jmp	.LBB9_151
.LBB9_145:
	cmpw	$0, 156(%rsp)           # 2-byte Folded Reload
	setg	%al
	movl	512(%rsp), %ecx
	movl	%ecx, %edx
	cmpl	$1, %edx
	setne	%cl
	cmpl	$5, %edx
	jg	.LBB9_148
# BB#146:
	andb	%cl, %al
	jne	.LBB9_148
# BB#147:
	movq	searchPattern(%rip), %rdi
	jmp	.LBB9_149
.LBB9_148:
	movq	square(%rip), %rdi
.LBB9_149:                              # %.preheader
	movl	536(%rsp), %edx
	jmp	.LBB9_151
.LBB9_150:
	movq	square(%rip), %rdi
.LBB9_151:                              # %.preheader
	movl	%r12d, 136(%rsp)        # 4-byte Spill
	movswq	112(%rsp), %rax         # 2-byte Folded Reload
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	%r11d, %eax
	jmp	.LBB9_154
.LBB9_174:                              #   in Loop: Header=BB9_155 Depth=2
	movq	8(%rdi), %rcx
	movslq	120(%rsp), %rdx         # 4-byte Folded Reload
	shlq	$4, %rdx
	leaq	12(%rcx,%rdx), %rax
	movl	8(%rcx,%rdx), %r12d
	movl	536(%rsp), %edx
	xorl	%ebx, %ebx
	movq	128(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ecx, 104(%rsp)         # 4-byte Spill
	movl	%r11d, %ecx
	movl	%ecx, 176(%rsp)         # 4-byte Spill
	movl	192(%rsp), %ecx         # 4-byte Reload
	cmpl	$1, %ecx
	jne	.LBB9_155
	.p2align	4, 0x90
.LBB9_175:                              #   in Loop: Header=BB9_154 Depth=1
	cmpw	$0, 156(%rsp)           # 2-byte Folded Reload
	movl	136(%rsp), %r12d        # 4-byte Reload
	jle	.LBB9_180
# BB#176:                               #   in Loop: Header=BB9_154 Depth=1
	movq	img(%rip), %rax
	cmpl	$0, 24(%rax)
	jne	.LBB9_180
# BB#177:                               #   in Loop: Header=BB9_154 Depth=1
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	movl	(%rax,%rcx,4), %eax
	leal	(,%rax,4), %ecx
	cmpl	%r12d, %ecx
	jl	.LBB9_200
# BB#178:                               #   in Loop: Header=BB9_154 Depth=1
	cmpl	232(%rsp), %eax         # 4-byte Folded Reload
	jg	.LBB9_180
# BB#179:                               #   in Loop: Header=BB9_154 Depth=1
	leal	(%rax,%rax,2), %eax
	cmpl	%r12d, %eax
	jl	.LBB9_200
	.p2align	4, 0x90
.LBB9_180:                              #   in Loop: Header=BB9_154 Depth=1
	cmpb	$1, 96(%rsp)            # 1-byte Folded Reload
	movq	128(%rsp), %rdi         # 8-byte Reload
	jne	.LBB9_192
# BB#181:                               #   in Loop: Header=BB9_154 Depth=1
	cmpl	$5, 512(%rsp)
	setl	%al
	movq	img(%rip), %rcx
	cmpl	$0, 20(%rcx)
	sete	%cl
	cmpl	232(%rsp), %r12d        # 4-byte Folded Reload
	jle	.LBB9_192
# BB#182:                               #   in Loop: Header=BB9_154 Depth=1
	orb	%cl, %al
	je	.LBB9_192
# BB#183:                               #   in Loop: Header=BB9_154 Depth=1
	movq	input(%rip), %rax
	cmpl	$0, 4092(%rax)
	jle	.LBB9_192
# BB#184:                               #   in Loop: Header=BB9_154 Depth=1
	movl	%edi, %eax
	orl	%r11d, %eax
	movswl	(%r10), %eax
	je	.LBB9_187
# BB#185:                               #   in Loop: Header=BB9_154 Depth=1
	cmpl	%eax, %r11d
	jne	.LBB9_152
# BB#186:                               #   in Loop: Header=BB9_154 Depth=1
	movswl	2(%r10), %ecx
	cmpl	%ecx, %edi
	jne	.LBB9_152
.LBB9_187:                              # %._crit_edge
                                        #   in Loop: Header=BB9_154 Depth=1
	movl	%r11d, %ecx
	subl	%eax, %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movl	$2, %ecx
	subl	mv_rescale(%rip), %ecx
	movl	$2, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	cmpl	%eax, %edx
	jge	.LBB9_190
# BB#188:                               #   in Loop: Header=BB9_154 Depth=1
	movswl	2(%r10), %ecx
	movl	%edi, %edx
	subl	%ecx, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	cmpl	%eax, %ecx
	jge	.LBB9_190
# BB#189:                               #   in Loop: Header=BB9_154 Depth=1
	movl	$sdiamond, %eax
	jmp	.LBB9_191
	.p2align	4, 0x90
.LBB9_152:                              #   in Loop: Header=BB9_154 Depth=1
	movl	$searchPatternD, %eax
	jmp	.LBB9_153
	.p2align	4, 0x90
.LBB9_190:                              #   in Loop: Header=BB9_154 Depth=1
	movl	$square, %eax
.LBB9_191:                              #   in Loop: Header=BB9_154 Depth=1
	movl	536(%rsp), %edx
.LBB9_153:                              #   in Loop: Header=BB9_154 Depth=1
	movq	(%rax), %rdi
	movl	$0, 96(%rsp)            # 4-byte Folded Spill
	movl	184(%rsp), %eax         # 4-byte Reload
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	216(%rsp), %eax         # 4-byte Reload
.LBB9_154:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_155 Depth 2
                                        #       Child Loop BB9_156 Depth 3
	movl	%eax, 176(%rsp)         # 4-byte Spill
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	movq	%rdi, %rax
	movl	$0, 120(%rsp)           # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB9_155:                              #   Parent Loop BB9_154 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_156 Depth 3
	movl	%ebx, 200(%rsp)         # 4-byte Spill
	movl	%ecx, 192(%rsp)         # 4-byte Spill
	movl	(%rax), %ebx
	incl	%ebx
	.p2align	4, 0x90
.LBB9_156:                              #   Parent Loop BB9_154 Depth=1
                                        #     Parent Loop BB9_155 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdi, %r13
	movq	8(%rdi), %rax
	movslq	%r12d, %rcx
	shlq	$4, %rcx
	movl	(%rax,%rcx), %r15d
	addl	176(%rsp), %r15d        # 4-byte Folded Reload
	movl	4(%rax,%rcx), %r14d
	addl	104(%rsp), %r14d        # 4-byte Folded Reload
	leal	(%r15,%rsi), %r8d
	movzbl	mv_rescale(%rip), %ecx
	shll	%cl, %r8d
	leal	(%r14,%rbp), %r9d
	shll	%cl, %r9d
	movswl	(%r10), %eax
	movl	%r15d, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	cmpl	%edx, %eax
	jg	.LBB9_168
# BB#157:                               #   in Loop: Header=BB9_156 Depth=3
	movswl	2(%r10), %eax
	movl	%r14d, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	cmpl	%edx, %eax
	jg	.LBB9_168
# BB#158:                               #   in Loop: Header=BB9_156 Depth=3
	movq	EPZSMap(%rip), %rax
	movq	272(%rsp), %rcx         # 8-byte Reload
	leal	(%r14,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	leal	(%r15,%rcx), %ecx
	movslq	%ecx, %rcx
	movzwl	EPZSBlkCount(%rip), %edx
	cmpw	%dx, (%rax,%rcx,2)
	jne	.LBB9_160
# BB#159:                               #   in Loop: Header=BB9_156 Depth=3
	incl	%r12d
	movl	%r12d, %eax
	movq	%r13, %rdi
	subl	(%rdi), %eax
	setl	%cl
	movl	536(%rsp), %edx
	jmp	.LBB9_169
.LBB9_160:                              #   in Loop: Header=BB9_156 Depth=3
	movw	%dx, (%rax,%rcx,2)
	movq	mvbits(%rip), %rax
	movl	%r8d, %ecx
	subl	256(%rsp), %ecx         # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movl	%r9d, %edx
	subl	252(%rsp), %edx         # 4-byte Folded Reload
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %ebp
	addl	(%rax,%rcx,4), %ebp
	imull	552(%rsp), %ebp
	sarl	$16, %ebp
	movl	136(%rsp), %ecx         # 4-byte Reload
	subl	%ebp, %ecx
	jle	.LBB9_167
# BB#161:                               #   in Loop: Header=BB9_156 Depth=3
	testl	%r8d, %r8d
	movq	%r11, 224(%rsp)         # 8-byte Spill
	js	.LBB9_165
# BB#162:                               #   in Loop: Header=BB9_156 Depth=3
	xorl	%eax, %eax
	testl	%r9d, %r9d
	js	.LBB9_166
# BB#163:                               #   in Loop: Header=BB9_156 Depth=3
	movswl	img_width(%rip), %edx
	subl	100(%rsp), %edx         # 4-byte Folded Reload
	cmpl	%edx, %r8d
	jge	.LBB9_166
# BB#164:                               #   in Loop: Header=BB9_156 Depth=3
	movswl	img_height(%rip), %eax
	subl	116(%rsp), %eax         # 4-byte Folded Reload
	cmpl	%eax, %r9d
	setl	%al
	jmp	.LBB9_166
.LBB9_165:                              #   in Loop: Header=BB9_156 Depth=3
	xorl	%eax, %eax
.LBB9_166:                              #   in Loop: Header=BB9_156 Depth=3
	xorb	$1, %al
	movzbl	%al, %eax
	movl	%eax, ref_access_method(%rip)
	addl	$80, %r8d
	addl	$80, %r9d
	movq	312(%rsp), %rdi         # 8-byte Reload
	movl	116(%rsp), %esi         # 4-byte Reload
	movl	100(%rsp), %edx         # 4-byte Reload
	movq	328(%rsp), %rax         # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	addl	%ebp, %eax
	movl	136(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, %eax
	movq	224(%rsp), %r11         # 8-byte Reload
	cmovll	%r15d, %r11d
	movq	128(%rsp), %rdx         # 8-byte Reload
	cmovll	%r14d, %edx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	cmovlel	%eax, %ecx
	movl	%ecx, 136(%rsp)         # 4-byte Spill
	movl	120(%rsp), %eax         # 4-byte Reload
	cmovll	%r12d, %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movq	528(%rsp), %r10
	movq	280(%rsp), %rsi         # 8-byte Reload
.LBB9_167:                              #   in Loop: Header=BB9_156 Depth=3
	movq	288(%rsp), %rbp         # 8-byte Reload
	movl	536(%rsp), %edx
	.p2align	4, 0x90
.LBB9_168:                              #   in Loop: Header=BB9_156 Depth=3
	movq	%r13, %rdi
	incl	%r12d
	movl	%r12d, %eax
	subl	(%rdi), %eax
	setl	%cl
.LBB9_169:                              #   in Loop: Header=BB9_156 Depth=3
	testb	%cl, %cl
	cmovel	%eax, %r12d
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB9_156
# BB#170:                               #   in Loop: Header=BB9_155 Depth=2
	cmpl	$0, 200(%rsp)           # 4-byte Folded Reload
	jne	.LBB9_173
# BB#171:                               #   in Loop: Header=BB9_155 Depth=2
	cmpl	176(%rsp), %r11d        # 4-byte Folded Reload
	jne	.LBB9_174
# BB#172:                               #   in Loop: Header=BB9_155 Depth=2
	movl	104(%rsp), %eax         # 4-byte Reload
	cmpl	%eax, 128(%rsp)         # 4-byte Folded Reload
	jne	.LBB9_174
.LBB9_173:                              #   in Loop: Header=BB9_155 Depth=2
	movl	16(%rdi), %ecx
	movq	24(%rdi), %rdi
	movl	20(%rdi), %ebx
	xorl	%r12d, %r12d
	movq	%rdi, %rax
	movl	$0, 120(%rsp)           # 4-byte Folded Spill
	cmpl	$1, %ecx
	jne	.LBB9_155
	jmp	.LBB9_175
.LBB9_192:
	movq	208(%rsp), %rbp         # 8-byte Reload
	movb	$1, %al
	testb	%al, %al
	je	.LBB9_199
	jmp	.LBB9_193
.LBB9_200:
	movw	%r11w, (%r10)
	movq	128(%rsp), %rdi         # 8-byte Reload
	movw	%di, 2(%r10)
	movq	input(%rip), %rax
	cmpl	$0, 4104(%rax)
	je	.LBB9_202
# BB#201:
	movq	208(%rsp), %rbp         # 8-byte Reload
	movw	%r11w, (%rbp)
	movw	%di, 2(%rbp)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB9_193
	jmp	.LBB9_199
.LBB9_202:
	xorl	%eax, %eax
.LBB9_203:
	movq	208(%rsp), %rbp         # 8-byte Reload
	testb	%al, %al
	je	.LBB9_199
.LBB9_193:
	cmpw	$0, 156(%rsp)           # 2-byte Folded Reload
	movswq	112(%rsp), %rax         # 2-byte Folded Reload
	je	.LBB9_195
# BB#194:
	movq	240(%rsp), %rcx         # 8-byte Reload
	cmpl	%r12d, (%rcx,%rax,4)
	jle	.LBB9_196
.LBB9_195:                              # %._crit_edge675
	movq	240(%rsp), %rcx         # 8-byte Reload
	movl	%r12d, (%rcx,%rax,4)
.LBB9_196:
	movq	input(%rip), %rax
	cmpl	$0, 4104(%rax)
	je	.LBB9_198
# BB#197:
	movw	%r11w, (%rbp)
	movw	%di, 2(%rbp)
.LBB9_198:
	movw	%r11w, (%r10)
	movw	%di, 2(%r10)
.LBB9_199:
	movl	%r12d, %eax
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	EPZSPelBlockMotionSearch, .Lfunc_end9-EPZSPelBlockMotionSearch
	.cfi_endproc

	.p2align	4, 0x90
	.type	EPZSSpatialPredictors,@function
EPZSSpatialPredictors:                  # @EPZSSpatialPredictors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%r8, -8(%rsp)           # 8-byte Spill
	movq	%rcx, %rax
	leaq	128(%rsp), %rbp
	leaq	104(%rsp), %rbx
	leaq	80(%rsp), %r11
	leaq	56(%rsp), %r8
	addl	%esi, %edi
	movslq	%edi, %r13
	movslq	%edx, %r12
	movl	mv_rescale(%rip), %ecx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	leal	8(%rcx), %ecx
	movl	%ecx, -36(%rsp)         # 4-byte Spill
	movl	$0, (%r9)
	movl	$0, 4(%r9)
	movq	img(%rip), %r15
	cmpl	$0, 15268(%r15)
	je	.LBB10_1
# BB#21:
	testl	%esi, %esi
	movl	(%r8), %r14d
	je	.LBB10_50
# BB#22:
	movl	$-1, %edx
	testl	%r14d, %r14d
	movl	$-1, %edi
	je	.LBB10_24
# BB#23:
	movq	14224(%r15), %rcx
	movslq	4(%r8), %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	movslq	20(%r8), %rbp
	movq	(%rax,%rbp,8), %rbp
	movslq	16(%r8), %rbx
	movsbl	(%rbp,%rbx), %edi
	leaq	104(%rsp), %rbx
	cmpl	$0, 424(%rcx,%rsi)
	sete	%cl
	shll	%cl, %edi
.LBB10_24:
	cmpl	$0, (%r11)
	je	.LBB10_26
# BB#25:
	movq	14224(%r15), %rcx
	movslq	4(%r11), %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	movslq	20(%r11), %rbx
	movq	(%rax,%rbx,8), %rbx
	movslq	16(%r11), %rbp
	movsbl	(%rbx,%rbp), %edx
	leaq	104(%rsp), %rbx
	cmpl	$0, 424(%rcx,%rsi)
	sete	%cl
	shll	%cl, %edx
.LBB10_26:
	movl	$-1, -32(%rsp)          # 4-byte Folded Spill
	cmpl	$0, (%rbx)
	movl	$-1, -12(%rsp)          # 4-byte Folded Spill
	movq	%rbx, %rbp
	je	.LBB10_28
# BB#27:
	movq	14224(%r15), %rcx
	movslq	4(%rbp), %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	movslq	20(%rbp), %rbx
	movq	(%rax,%rbx,8), %rbx
	movslq	16(%rbp), %rbp
	movsbl	(%rbx,%rbp), %ebp
	cmpl	$0, 424(%rcx,%rsi)
	sete	%cl
	shll	%cl, %ebp
	movl	%ebp, -12(%rsp)         # 4-byte Spill
.LBB10_28:
	leaq	128(%rsp), %rbp
	cmpl	$0, (%rbp)
	je	.LBB10_30
# BB#29:
	movq	14224(%r15), %rcx
	movslq	4(%rbp), %rsi
	imulq	$536, %rsi, %rsi        # imm = 0x218
	movslq	20(%rbp), %rbx
	movq	(%rax,%rbx,8), %rax
	movslq	16(%rbp), %rbx
	movsbl	(%rax,%rbx), %eax
	cmpl	$0, 424(%rcx,%rsi)
	sete	%cl
	shll	%cl, %eax
	movl	%eax, -32(%rsp)         # 4-byte Spill
.LBB10_30:
	testl	%r14d, %r14d
	je	.LBB10_32
# BB#31:
	movslq	%edi, %rax
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rsi
	shlq	$12, %rsi
	addq	%rcx, %rsi
	movslq	20(%r8), %rcx
	movq	-8(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rcx,8), %rcx
	movslq	16(%r8), %rbx
	movq	(%rcx,%rbx,8), %rcx
	movswl	(%rcx), %ebx
	imull	mv_scale(%rsi,%rax,4), %ebx
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%ebx, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
	jmp	.LBB10_33
.LBB10_1:
	movl	(%r8), %ecx
	movl	$-1, %r14d
	testl	%ecx, %ecx
	movl	$-1, %r10d
	je	.LBB10_3
# BB#2:
	movslq	20(%r8), %rdx
	movq	(%rax,%rdx,8), %rdx
	movslq	16(%r8), %rsi
	movsbl	(%rdx,%rsi), %r10d
.LBB10_3:
	cmpl	$0, (%r11)
	je	.LBB10_5
# BB#4:
	movslq	20(%r11), %rdx
	movq	(%rax,%rdx,8), %rdx
	movslq	16(%r11), %rsi
	movsbl	(%rdx,%rsi), %r14d
.LBB10_5:
	movl	$-1, %r15d
	cmpl	$0, (%rbx)
	movl	$-1, %edi
	je	.LBB10_7
# BB#6:
	movslq	20(%rbx), %rdx
	movq	(%rax,%rdx,8), %rdx
	movslq	16(%rbx), %rsi
	movsbl	(%rdx,%rsi), %edi
.LBB10_7:
	cmpl	$0, (%rbp)
	je	.LBB10_9
# BB#8:
	movslq	20(%rbp), %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	16(%rbp), %rdx
	movsbl	(%rax,%rdx), %r15d
.LBB10_9:
	testl	%ecx, %ecx
	je	.LBB10_11
# BB#10:
	movslq	%r10d, %rax
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movl	mv_scale(%rdx,%rax,4), %esi
	movslq	20(%r8), %rax
	movq	-8(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rax,8), %rax
	movslq	16(%r8), %rcx
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %edx
	imull	%esi, %edx
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	%ebp, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, 16(%r9)
	movslq	20(%r8), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movslq	16(%r8), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movswl	2(%rcx), %edx
	imull	%esi, %edx
	addl	%ebp, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	jmp	.LBB10_12
.LBB10_50:
	movl	$-1, %ebx
	testl	%r14d, %r14d
	movl	$-1, %edi
	je	.LBB10_52
# BB#51:
	movq	14224(%r15), %rcx
	movslq	4(%r8), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	cmpl	$0, 424(%rcx,%rdx)
	setne	%cl
	movslq	20(%r8), %rdx
	movq	(%rax,%rdx,8), %rdx
	movslq	16(%r8), %rsi
	movsbl	(%rdx,%rsi), %edi
	sarl	%cl, %edi
.LBB10_52:
	cmpl	$0, (%r11)
	je	.LBB10_54
# BB#53:
	movq	14224(%r15), %rcx
	movslq	4(%r11), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	cmpl	$0, 424(%rcx,%rdx)
	setne	%cl
	movslq	20(%r11), %rdx
	movq	(%rax,%rdx,8), %rdx
	movslq	16(%r11), %rsi
	movsbl	(%rdx,%rsi), %ebx
	sarl	%cl, %ebx
.LBB10_54:
	movl	$-1, -32(%rsp)          # 4-byte Folded Spill
	leaq	104(%rsp), %rsi
	cmpl	$0, (%rsi)
	movl	$-1, -12(%rsp)          # 4-byte Folded Spill
	je	.LBB10_56
# BB#55:
	movq	14224(%r15), %rcx
	movslq	4(%rsi), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	cmpl	$0, 424(%rcx,%rdx)
	setne	%cl
	movslq	20(%rsi), %rdx
	movq	(%rax,%rdx,8), %rdx
	movslq	16(%rsi), %rsi
	movsbl	(%rdx,%rsi), %edx
	sarl	%cl, %edx
	movl	%edx, -12(%rsp)         # 4-byte Spill
.LBB10_56:
	cmpl	$0, (%rbp)
	je	.LBB10_58
# BB#57:
	movq	14224(%r15), %rcx
	movslq	4(%rbp), %rdx
	imulq	$536, %rdx, %rdx        # imm = 0x218
	cmpl	$0, 424(%rcx,%rdx)
	setne	%cl
	movslq	20(%rbp), %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	16(%rbp), %rdx
	movsbl	(%rax,%rdx), %eax
	sarl	%cl, %eax
	movl	%eax, -32(%rsp)         # 4-byte Spill
.LBB10_58:
	testl	%r14d, %r14d
	movl	%edi, -28(%rsp)         # 4-byte Spill
	je	.LBB10_60
# BB#59:
	movslq	%edi, %rax
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movslq	20(%r8), %rcx
	movq	-8(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%rcx,8), %rcx
	movslq	16(%r8), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movswl	(%rcx), %edi
	imull	mv_scale(%rdx,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edi, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
	jmp	.LBB10_61
.LBB10_32:
	movl	$12, %esi
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %esi
.LBB10_33:
	movl	%edx, %r14d
	movl	%edi, -28(%rsp)         # 4-byte Spill
	movl	%esi, 16(%r9)
	xorl	%esi, %esi
	cmpl	$0, (%r8)
	movl	$0, %ebp
	je	.LBB10_35
# BB#34:
	movq	14224(%r15), %rcx
	movslq	4(%r8), %rax
	imulq	$536, %rax, %rbx        # imm = 0x218
	movslq	-28(%rsp), %rax         # 4-byte Folded Reload
	movq	%r12, %rbp
	shlq	$7, %rbp
	movq	%r13, %rdi
	shlq	$12, %rdi
	addq	%rbp, %rdi
	movslq	20(%r8), %rbp
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rbp,8), %rbp
	movslq	16(%r8), %rdx
	movq	(%rbp,%rdx,8), %rdx
	movswl	2(%rdx), %edx
	imull	mv_scale(%rdi,%rax,4), %edx
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	cmpl	$1, 424(%rcx,%rbx)
	adcl	$0, %eax
	leal	-1(%rax), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	%edx, %ebp
	movl	%eax, %ecx
	sarl	%cl, %ebp
.LBB10_35:
	movl	%ebp, 20(%r9)
	cmpl	$0, (%r11)
	movq	-8(%rsp), %r10          # 8-byte Reload
	je	.LBB10_37
# BB#36:
	movslq	%r14d, %rax
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movslq	20(%r11), %rcx
	movq	(%r10,%rcx,8), %rcx
	movslq	16(%r11), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movswl	(%rcx), %edi
	imull	mv_scale(%rdx,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edi, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
.LBB10_37:
	movl	%esi, 32(%r9)
	cmpl	$0, (%r11)
	leaq	104(%rsp), %rbx
	movl	-12(%rsp), %r8d         # 4-byte Reload
	je	.LBB10_39
# BB#38:
	movq	14224(%r15), %rcx
	movslq	4(%r11), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	movslq	%r14d, %rax
	movq	%r12, %rsi
	shlq	$7, %rsi
	movq	%r13, %rdi
	shlq	$12, %rdi
	addq	%rsi, %rdi
	movslq	20(%r11), %rsi
	movq	(%r10,%rsi,8), %rsi
	movslq	16(%r11), %rbp
	movq	(%rsi,%rbp,8), %rsi
	movswl	2(%rsi), %ebp
	imull	mv_scale(%rdi,%rax,4), %ebp
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	cmpl	$1, 424(%rcx,%rdx)
	adcl	$0, %eax
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%ebp, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
	jmp	.LBB10_40
.LBB10_39:
	movl	$12, %esi
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %esi
.LBB10_40:
	movl	%esi, 36(%r9)
	cmpl	$0, (%rbx)
	je	.LBB10_42
# BB#41:
	movslq	%r8d, %rax
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movslq	20(%rbx), %rcx
	movq	(%r10,%rcx,8), %rcx
	movslq	16(%rbx), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movswl	(%rcx), %edi
	imull	mv_scale(%rdx,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edi, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
	jmp	.LBB10_43
.LBB10_42:
	movl	$12, %esi
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %esi
	negl	%esi
.LBB10_43:
	movl	%esi, 48(%r9)
	xorl	%esi, %esi
	cmpl	$0, (%rbx)
	movl	$0, %ebp
	je	.LBB10_45
# BB#44:
	movq	14224(%r15), %rcx
	movslq	4(%rbx), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	movslq	%r8d, %rax
	movq	%r12, %rdi
	shlq	$7, %rdi
	movq	%r13, %rbp
	shlq	$12, %rbp
	addq	%rdi, %rbp
	movslq	20(%rbx), %rdi
	movq	(%r10,%rdi,8), %rdi
	movslq	16(%rbx), %rbx
	movq	(%rdi,%rbx,8), %rdi
	movswl	2(%rdi), %edi
	imull	mv_scale(%rbp,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	cmpl	$1, 424(%rcx,%rdx)
	adcl	$0, %eax
	leal	-1(%rax), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	%edi, %ebp
	movl	%eax, %ecx
	sarl	%cl, %ebp
.LBB10_45:
	movl	%ebp, 52(%r9)
	leaq	128(%rsp), %rbp
	cmpl	$0, (%rbp)
	je	.LBB10_47
# BB#46:
	movslq	-32(%rsp), %rax         # 4-byte Folded Reload
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movslq	20(%rbp), %rcx
	movq	(%r10,%rcx,8), %rcx
	movslq	16(%rbp), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movswl	(%rcx), %edi
	imull	mv_scale(%rdx,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edi, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
.LBB10_47:
	movl	%esi, 64(%r9)
	cmpl	$0, (%rbp)
	je	.LBB10_77
# BB#48:
	movq	14224(%r15), %rcx
	movslq	4(%rbp), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	movl	-32(%rsp), %r15d        # 4-byte Reload
	movslq	%r15d, %rax
	shlq	$7, %r12
	shlq	$12, %r13
	addq	%r12, %r13
	movslq	20(%rbp), %rsi
	movq	(%r10,%rsi,8), %rsi
	movslq	16(%rbp), %rdi
	movq	(%rsi,%rdi,8), %rsi
	movswl	2(%rsi), %esi
	imull	mv_scale(%r13,%rax,4), %esi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	cmpl	$1, 424(%rcx,%rdx)
	adcl	$0, %eax
	jmp	.LBB10_49
.LBB10_11:
	movl	$12, %eax
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %eax
	movl	%eax, 16(%r9)
	xorl	%edx, %edx
.LBB10_12:
	movl	%edi, %r8d
	movl	%edx, 20(%r9)
	cmpl	$0, (%r11)
	je	.LBB10_14
# BB#13:
	movslq	%r14d, %rax
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movl	mv_scale(%rdx,%rax,4), %esi
	movslq	20(%r11), %rax
	movq	-8(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rax,8), %rax
	movslq	16(%r11), %rcx
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %edx
	imull	%esi, %edx
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	%ebp, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, 32(%r9)
	movslq	20(%r11), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movslq	16(%r11), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movswl	2(%rcx), %edx
	imull	%esi, %edx
	addl	%ebp, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	jmp	.LBB10_15
.LBB10_14:
	movl	$0, 32(%r9)
	movl	$12, %edx
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %edx
	movq	-8(%rsp), %rbx          # 8-byte Reload
.LBB10_15:
	movl	%edx, 36(%r9)
	leaq	104(%rsp), %rdi
	cmpl	$0, (%rdi)
	je	.LBB10_17
# BB#16:
	movslq	%r8d, %rax
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movl	mv_scale(%rdx,%rax,4), %esi
	movslq	20(%rdi), %rax
	movq	(%rbx,%rax,8), %rax
	movslq	16(%rdi), %rcx
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %edx
	imull	%esi, %edx
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	%ebp, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, 48(%r9)
	movslq	20(%rdi), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movslq	16(%rdi), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movswl	2(%rcx), %edx
	imull	%esi, %edx
	addl	%ebp, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	jmp	.LBB10_18
.LBB10_17:
	movl	$12, %eax
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %eax
	negl	%eax
	movl	%eax, 48(%r9)
	xorl	%edx, %edx
.LBB10_18:
	leaq	128(%rsp), %rbp
	movl	%edx, 52(%r9)
	cmpl	$0, (%rbp)
	je	.LBB10_20
# BB#19:
	movslq	%r15d, %rax
	shlq	$7, %r12
	shlq	$12, %r13
	addq	%r12, %r13
	movl	mv_scale(%r13,%rax,4), %esi
	movslq	20(%rbp), %rax
	movq	(%rbx,%rax,8), %rax
	movslq	16(%rbp), %rcx
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %edx
	imull	%esi, %edx
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	addl	%edi, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	%edx, 64(%r9)
	movslq	20(%rbp), %rcx
	movq	(%rbx,%rcx,8), %rcx
	movslq	16(%rbp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movswl	2(%rcx), %edx
	imull	%esi, %edx
	addl	%edi, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	jmp	.LBB10_78
.LBB10_20:
	movl	$0, 64(%r9)
	movl	$12, %edx
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %edx
	negl	%edx
	jmp	.LBB10_78
.LBB10_60:
	movl	$12, %esi
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %esi
.LBB10_61:
	movl	%ebx, %r14d
	movl	%esi, 16(%r9)
	xorl	%esi, %esi
	cmpl	$0, (%r8)
	movl	$0, %ebp
	je	.LBB10_63
# BB#62:
	movq	14224(%r15), %rcx
	movslq	4(%r8), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	movslq	-28(%rsp), %rax         # 4-byte Folded Reload
	movq	%r12, %rdi
	shlq	$7, %rdi
	movq	%r13, %rbx
	shlq	$12, %rbx
	addq	%rdi, %rbx
	movslq	20(%r8), %rdi
	movq	-8(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rdi,8), %rdi
	movslq	16(%r8), %rbp
	movq	(%rdi,%rbp,8), %rdi
	movswl	2(%rdi), %edi
	imull	mv_scale(%rbx,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	cmpl	$1, 424(%rcx,%rdx)
	adcl	$-1, %eax
	leal	-1(%rax), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	%edi, %ebp
	movl	%eax, %ecx
	sarl	%cl, %ebp
.LBB10_63:
	movl	%ebp, 20(%r9)
	cmpl	$0, (%r11)
	movq	-8(%rsp), %r10          # 8-byte Reload
	je	.LBB10_65
# BB#64:
	movslq	%r14d, %rax
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movslq	20(%r11), %rcx
	movq	(%r10,%rcx,8), %rcx
	movslq	16(%r11), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movswl	(%rcx), %edi
	imull	mv_scale(%rdx,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edi, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
.LBB10_65:
	movl	%esi, 32(%r9)
	cmpl	$0, (%r11)
	leaq	104(%rsp), %rbx
	movl	-12(%rsp), %r8d         # 4-byte Reload
	je	.LBB10_67
# BB#66:
	movq	14224(%r15), %rcx
	movslq	4(%r11), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	movslq	%r14d, %rax
	movq	%r12, %rsi
	shlq	$7, %rsi
	movq	%r13, %rdi
	shlq	$12, %rdi
	addq	%rsi, %rdi
	movslq	20(%r11), %rsi
	movq	(%r10,%rsi,8), %rsi
	movslq	16(%r11), %rbp
	movq	(%rsi,%rbp,8), %rsi
	movswl	2(%rsi), %ebp
	imull	mv_scale(%rdi,%rax,4), %ebp
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	cmpl	$1, 424(%rcx,%rdx)
	adcl	$-1, %eax
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%ebp, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
	jmp	.LBB10_68
.LBB10_67:
	movl	$12, %esi
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %esi
.LBB10_68:
	movl	%esi, 36(%r9)
	cmpl	$0, (%rbx)
	je	.LBB10_70
# BB#69:
	movslq	%r8d, %rax
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movslq	20(%rbx), %rcx
	movq	(%r10,%rcx,8), %rcx
	movslq	16(%rbx), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movswl	(%rcx), %edi
	imull	mv_scale(%rdx,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edi, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
	jmp	.LBB10_71
.LBB10_70:
	movl	$12, %esi
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %esi
	negl	%esi
.LBB10_71:
	movl	%esi, 48(%r9)
	xorl	%esi, %esi
	cmpl	$0, (%rbx)
	movl	$0, %ebp
	je	.LBB10_73
# BB#72:
	movq	14224(%r15), %rcx
	movslq	4(%rbx), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	movslq	%r8d, %rax
	movq	%r12, %rdi
	shlq	$7, %rdi
	movq	%r13, %rbp
	shlq	$12, %rbp
	addq	%rdi, %rbp
	movslq	20(%rbx), %rdi
	movq	(%r10,%rdi,8), %rdi
	movslq	16(%rbx), %rbx
	movq	(%rdi,%rbx,8), %rdi
	movswl	2(%rdi), %edi
	imull	mv_scale(%rbp,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	cmpl	$1, 424(%rcx,%rdx)
	adcl	$-1, %eax
	leal	-1(%rax), %ecx
	movl	$1, %ebp
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebp
	addl	%edi, %ebp
	movl	%eax, %ecx
	sarl	%cl, %ebp
.LBB10_73:
	movl	%ebp, 52(%r9)
	leaq	128(%rsp), %rbp
	cmpl	$0, (%rbp)
	je	.LBB10_75
# BB#74:
	movslq	-32(%rsp), %rax         # 4-byte Folded Reload
	movq	%r12, %rcx
	shlq	$7, %rcx
	movq	%r13, %rdx
	shlq	$12, %rdx
	addq	%rcx, %rdx
	movslq	20(%rbp), %rcx
	movq	(%r10,%rcx,8), %rcx
	movslq	16(%rbp), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movswl	(%rcx), %edi
	imull	mv_scale(%rdx,%rax,4), %edi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edi, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
.LBB10_75:
	movl	%esi, 64(%r9)
	cmpl	$0, (%rbp)
	je	.LBB10_77
# BB#76:
	movq	14224(%r15), %rcx
	movslq	4(%rbp), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	movl	-32(%rsp), %r15d        # 4-byte Reload
	movslq	%r15d, %rax
	shlq	$7, %r12
	shlq	$12, %r13
	addq	%r12, %r13
	movslq	20(%rbp), %rsi
	movq	(%r10,%rsi,8), %rsi
	movslq	16(%rbp), %rdi
	movq	(%rsi,%rdi,8), %rsi
	movswl	2(%rsi), %esi
	imull	mv_scale(%r13,%rax,4), %esi
	movswl	-36(%rsp), %eax         # 2-byte Folded Reload
	cmpl	$1, 424(%rcx,%rdx)
	adcl	$-1, %eax
.LBB10_49:
	leal	-1(%rax), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	addl	%esi, %edx
	movl	%eax, %ecx
	sarl	%cl, %edx
	movl	-28(%rsp), %r10d        # 4-byte Reload
	jmp	.LBB10_78
.LBB10_77:
	movl	$12, %edx
	movq	-24(%rsp), %rcx         # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %edx
	negl	%edx
	movl	-28(%rsp), %r10d        # 4-byte Reload
	movl	-32(%rsp), %r15d        # 4-byte Reload
.LBB10_78:
	movl	%edx, 68(%r9)
	xorl	%ecx, %ecx
	cmpl	$-1, %r10d
	sete	%cl
	xorl	%edx, %edx
	cmpl	$-1, %r14d
	sete	%dl
	andl	%r15d, %r8d
	xorl	%eax, %eax
	cmpl	$-1, %r8d
	sete	%al
	addl	%edx, %eax
	addl	%ecx, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	EPZSSpatialPredictors, .Lfunc_end10-EPZSSpatialPredictors
	.cfi_endproc

	.globl	EPZSBiPredBlockMotionSearch
	.p2align	4, 0x90
	.type	EPZSBiPredBlockMotionSearch,@function
EPZSBiPredBlockMotionSearch:            # @EPZSBiPredBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi87:
	.cfi_def_cfa_offset 368
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movl	%edx, %r12d
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movl	376(%rsp), %ebx
	movl	368(%rsp), %r15d
	movq	400(%rsp), %rax
	movq	392(%rsp), %rcx
	movq	img(%rip), %rdx
	movl	%r15d, %edi
	subl	192(%rdx), %edi
	movl	%edi, 188(%rsp)         # 4-byte Spill
	movl	%ebx, %edi
	subl	196(%rdx), %edi
	movswl	(%rcx), %r11d
	movswl	2(%rcx), %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movswl	(%rax), %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movswl	2(%rax), %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	input(%rip), %rax
	movslq	384(%rsp), %rdx
	movl	76(%rax,%rdx,8), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	72(%rax,%rdx,8), %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movl	4120(%rax), %ecx
	addl	%ecx, %ecx
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	shll	%cl, %r15d
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	movl	%ecx, (%rsp)            # 4-byte Spill
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	416(%rsp), %rax
	movswl	(%rax), %ebx
	movswl	2(%rax), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	leal	(%rbp,%r12), %eax
	cltq
	movq	listX(,%rax,8), %rax
	movl	%esi, 100(%rsp)         # 4-byte Spill
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %r14
	movl	%r12d, %eax
	xorl	$1, %eax
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	addl	%ebp, %eax
	cltq
	movq	listX(,%rax,8), %rax
	movq	(%rax), %rbp
	movq	active_pps(%rip), %rax
	movl	196(%rax), %r13d
	testl	%r13d, %r13d
	movq	408(%rsp), %rax
	movswl	(%rax), %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movswl	2(%rax), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movl	medthres(,%rdx,4), %eax
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movq	%r8, 200(%rsp)          # 8-byte Spill
	movl	%edi, 184(%rsp)         # 4-byte Spill
	movl	%eax, 168(%rsp)         # 4-byte Spill
	movq	%r11, 136(%rsp)         # 8-byte Spill
	movq	%r15, 88(%rsp)          # 8-byte Spill
	je	.LBB11_3
# BB#1:
	movq	wp_offset(%rip), %rdx
	testl	%r12d, %r12d
	je	.LBB11_4
# BB#2:
	movq	56(%rsp), %r10          # 8-byte Reload
	movslq	%r10d, %rax
	movq	8(%rdx,%rax,8), %rcx
	movq	(%rdx,%rax,8), %rax
	jmp	.LBB11_5
.LBB11_3:
	xorl	%r11d, %r11d
	xorl	%r15d, %r15d
	movq	56(%rsp), %r10          # 8-byte Reload
	movl	368(%rsp), %r9d
	movq	%r9, %rdx
	jmp	.LBB11_6
.LBB11_4:
	movq	56(%rsp), %r10          # 8-byte Reload
	movslq	%r10d, %rdi
	leaq	(,%rsi,8), %rax
	movq	(%rdx,%rdi,8), %rcx
	addq	%rax, %rcx
	addq	8(%rdx,%rdi,8), %rax
.LBB11_5:
	movl	368(%rsp), %r9d
	movq	%r9, %rdx
	movq	(%rcx), %rcx
	movl	(%rcx), %r11d
	movq	(%rax), %rax
	movl	(%rax), %r15d
.LBB11_6:
	movq	searchPattern(%rip), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movzwl	EPZSBlkCount(%rip), %eax
	incl	%eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movw	%ax, EPZSBlkCount(%rip)
	movl	mv_rescale(%rip), %edi
	movl	$2, %ecx
	subl	%edi, %ecx
	movl	%edx, %eax
	shll	%cl, %eax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	376(%rsp), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	6448(%r14), %rax
	movq	%rax, ref_pic1_sub(%rip)
	movq	6448(%rbp), %rax
	movq	%rax, ref_pic2_sub(%rip)
	movl	6392(%r14), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movw	%ax, img_width(%rip)
	movl	6396(%r14), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movw	%ax, img_height(%rip)
	movl	6408(%r14), %eax
	movl	%eax, width_pad(%rip)
	movl	6412(%r14), %eax
	movl	%eax, height_pad(%rip)
	testl	%r13d, %r13d
	movl	%edi, 160(%rsp)         # 4-byte Spill
	je	.LBB11_9
# BB#7:
	movq	wbp_weight(%rip), %rax
	movq	%rbp, %r9
	testl	%r12d, %r12d
	je	.LBB11_10
# BB#8:
	movq	%r10, %rbp
	movl	%ebx, %r8d
	movslq	%ebp, %rbx
	movq	8(%rax,%rbx,8), %rcx
	movq	(%rcx), %rdi
	leaq	(,%rsi,8), %rcx
	movq	(%rdi,%rsi,8), %rdi
	movzwl	(%rdi), %edi
	movw	%di, weight1(%rip)
	movq	(%rax,%rbx,8), %rax
	movl	%r8d, %ebx
	addq	(%rax), %rcx
	jmp	.LBB11_11
.LBB11_9:
	movq	%rbp, %r9
	movb	luma_log_weight_denom(%rip), %cl
	movl	$1, %eax
	shll	%cl, %eax
	movw	%ax, weight1(%rip)
	movw	%ax, weight2(%rip)
	movl	$computeBiPred1, %ecx
	xorl	%eax, %eax
	jmp	.LBB11_12
.LBB11_10:
	movslq	%r10d, %rcx
	movl	%ebx, %edi
	movq	(%rax,%rcx,8), %rbx
	movq	(%rbx,%rsi,8), %rbx
	movq	(%rbx), %rbx
	movzwl	(%rbx), %ebx
	movw	%bx, weight1(%rip)
	movl	%edi, %ebx
	movq	8(%rax,%rcx,8), %rax
	movq	(%rax,%rsi,8), %rcx
.LBB11_11:
	movq	(%rcx), %rax
	movzwl	(%rax), %eax
	movw	%ax, weight2(%rip)
	movswl	%r11w, %eax
	movswl	%r15w, %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
	movl	$computeBiPred2, %ecx
.LBB11_12:
	movq	88(%rsp), %r11          # 8-byte Reload
	movl	424(%rsp), %edi
	movq	24(%rsp), %rdx          # 8-byte Reload
	leal	(%r11,%rdx), %ebp
	movw	%ax, offsetBi(%rip)
	movq	(%rcx), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%rax, computeBiPred(%rip)
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB11_23
# BB#13:
	movq	6464(%r14), %rax
	movq	(%rax), %rcx
	movq	%rcx, ref_pic1_sub+8(%rip)
	movq	8(%rax), %rax
	movq	%rax, ref_pic1_sub+16(%rip)
	movq	6464(%r9), %rax
	movq	(%rax), %rcx
	movq	%rcx, ref_pic2_sub+8(%rip)
	movq	8(%rax), %rax
	movq	%rax, ref_pic2_sub+16(%rip)
	movl	6416(%r14), %eax
	movl	%eax, width_pad_cr(%rip)
	movl	6420(%r14), %eax
	movl	%eax, height_pad_cr(%rip)
	testl	%r13d, %r13d
	je	.LBB11_16
# BB#14:
	movq	wbp_weight(%rip), %rcx
	testl	%r12d, %r12d
	je	.LBB11_17
# BB#15:
	movslq	%r10d, %rax
	movq	8(%rcx,%rax,8), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movzwl	4(%rdx), %edi
	movw	%di, weight1_cr(%rip)
	movzwl	8(%rdx), %edx
	movw	%dx, weight1_cr+2(%rip)
	movq	(%rcx,%rax,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movzwl	4(%rcx), %edx
	movw	%dx, weight2_cr(%rip)
	movzwl	8(%rcx), %ecx
	movw	%cx, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rcx
	movq	8(%rcx,%rax,8), %rdx
	movq	(%rcx,%rax,8), %rax
	jmp	.LBB11_18
.LBB11_16:
	movb	chroma_log_weight_denom(%rip), %cl
	movl	$1, %eax
	shll	%cl, %eax
	movw	%ax, weight1_cr(%rip)
	movw	%ax, weight1_cr+2(%rip)
	movw	%ax, weight2_cr(%rip)
	movw	%ax, weight2_cr+2(%rip)
	movw	$0, offsetBi_cr(%rip)
	xorl	%eax, %eax
	jmp	.LBB11_22
.LBB11_17:
	movslq	%r10d, %rdi
	movq	(%rcx,%rdi,8), %rdx
	leaq	(,%rsi,8), %rax
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rdx), %rdx
	movl	%ebx, %r8d
	movzwl	4(%rdx), %ebx
	movw	%bx, weight1_cr(%rip)
	movl	%r8d, %ebx
	movzwl	8(%rdx), %edx
	movw	%dx, weight1_cr+2(%rip)
	movq	8(%rcx,%rdi,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx), %rcx
	movzwl	4(%rcx), %edx
	movw	%dx, weight2_cr(%rip)
	movzwl	8(%rcx), %ecx
	movw	%cx, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rcx
	movq	(%rcx,%rdi,8), %rdx
	addq	%rax, %rdx
	addq	8(%rcx,%rdi,8), %rax
.LBB11_18:
	movq	(%rdx), %rdx
	movl	4(%rdx), %edx
	movq	(%rax), %rax
	movl	4(%rax), %eax
	leal	1(%rdx,%rax), %eax
	shrl	%eax
	movw	%ax, offsetBi_cr(%rip)
	movslq	%r10d, %rdx
	testl	%r12d, %r12d
	je	.LBB11_20
# BB#19:
	movq	8(%rcx,%rdx,8), %rax
	movq	(%rcx,%rdx,8), %rsi
	jmp	.LBB11_21
.LBB11_20:
	shlq	$3, %rsi
	movq	(%rcx,%rdx,8), %rax
	addq	%rsi, %rax
	addq	8(%rcx,%rdx,8), %rsi
.LBB11_21:
	movl	424(%rsp), %edi
	movq	(%rax), %rax
	movl	8(%rax), %eax
	movq	(%rsi), %rcx
	movl	8(%rcx), %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
.LBB11_22:
	movw	%ax, offsetBi_cr+2(%rip)
.LBB11_23:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	128(%rsp), %r13         # 8-byte Reload
	leal	(%rax,%r13), %r10d
	addl	%r11d, %ebx
	movl	%ebx, %r15d
	movl	$1, %edx
	cmpl	%edi, %ebp
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	%r12, %r11
	jle	.LBB11_27
# BB#24:
	movswl	20(%rsp), %esi          # 2-byte Folded Reload
	movswl	40(%rsp), %ecx          # 2-byte Folded Reload
	subl	%ecx, %esi
	movl	(%rsp), %ecx            # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	cmpl	%edi, %r10d
	jle	.LBB11_27
# BB#25:
	subl	%edi, %esi
	cmpl	%esi, %ebp
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	12(%rsp), %r12d         # 4-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	368(%rsp), %ebp
	jge	.LBB11_28
# BB#26:
	movswl	16(%rsp), %esi          # 2-byte Folded Reload
	movswl	8(%rsp), %ecx           # 2-byte Folded Reload
	subl	%ecx, %esi
	movl	(%rsp), %ecx            # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	subl	%edi, %esi
	xorl	%edx, %edx
	cmpl	%esi, %r10d
	setge	%dl
	jmp	.LBB11_28
.LBB11_27:
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	12(%rsp), %r12d         # 4-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	368(%rsp), %ebp
.LBB11_28:
	movq	136(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rbp,4), %r9d
	movl	376(%rsp), %ecx
	movq	%rcx, %rsi
	leal	(%rbx,%rsi,4), %ebx
	leal	(%rax,%rbp,4), %ecx
	leal	(%r8,%rsi,4), %esi
	addl	%r13d, %r12d
	movl	%edx, bipred2_access_method(%rip)
	cmpl	%edi, %r15d
	movq	%r11, 104(%rsp)         # 8-byte Spill
	jle	.LBB11_32
# BB#29:
	movl	%ecx, %r13d
	movswl	20(%rsp), %edx          # 2-byte Folded Reload
	movswl	40(%rsp), %ecx          # 2-byte Folded Reload
	movl	%ecx, %r8d
	subl	%ecx, %edx
	movl	(%rsp), %ecx            # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movl	$1, %ecx
	cmpl	%edi, %r12d
	movl	%r8d, 48(%rsp)          # 4-byte Spill
	movl	%esi, %r11d
	jle	.LBB11_33
# BB#30:
	subl	%edi, %edx
	cmpl	%edx, %r15d
	jge	.LBB11_33
# BB#31:
	movswl	16(%rsp), %edx          # 2-byte Folded Reload
	movswl	8(%rsp), %ecx           # 2-byte Folded Reload
	subl	%ecx, %edx
	movl	(%rsp), %ecx            # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	subl	%edi, %edx
	xorl	%ecx, %ecx
	cmpl	%edx, %r12d
	setge	%cl
	jmp	.LBB11_33
.LBB11_32:                              # %._crit_edge546
	movl	%esi, %r11d
	movl	%ecx, %r13d
	movswl	40(%rsp), %eax          # 2-byte Folded Reload
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	$1, %ecx
.LBB11_33:
	movl	4(%rsp), %esi           # 4-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	%ecx, bipred1_access_method(%rip)
	movq	EPZSMap(%rip), %rcx
	movl	424(%rsp), %edx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movw	%si, (%rcx,%rdx,2)
	movq	mvbits(%rip), %rdx
	movl	%r15d, %r8d
	movl	160(%rsp), %ecx         # 4-byte Reload
	shll	%cl, %r8d
	movl	%r8d, %esi
	subl	%r9d, %esi
	movl	%r9d, 128(%rsp)         # 4-byte Spill
	movslq	%esi, %rsi
	movl	%r12d, %r9d
	shll	%cl, %r9d
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	movl	%r9d, %ebp
	movl	%ebx, 88(%rsp)          # 4-byte Spill
	subl	%ebx, %ebp
	movslq	%ebp, %rbx
	movl	%r15d, 136(%rsp)        # 4-byte Spill
	movl	(%rdx,%rbx,4), %ebx
	addl	(%rdx,%rsi,4), %ebx
	movq	112(%rsp), %rax         # 8-byte Reload
	shll	%cl, %eax
	movl	%eax, %esi
	subl	%r13d, %esi
	movslq	%esi, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r10d
	movl	%r10d, %ecx
	movl	%r11d, %r15d
	subl	%r15d, %ecx
	movslq	%ecx, %rcx
	movl	(%rdx,%rcx,4), %ebp
	addl	(%rdx,%rsi,4), %ebp
	movl	440(%rsp), %ecx
	imull	%ecx, %ebx
	sarl	$16, %ebx
	imull	%ecx, %ebp
	sarl	$16, %ebp
	movswl	8(%rsp), %r12d          # 2-byte Folded Reload
	addl	$80, %r8d
	addl	$80, %r9d
	addl	$80, %eax
	addl	$80, %r10d
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	movl	%r12d, %esi
	movl	48(%rsp), %edx          # 4-byte Reload
	pushq	%r10
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	callq	*136(%rsp)              # 8-byte Folded Reload
	addq	$16, %rsp
.Lcfi96:
	.cfi_adjust_cfa_offset -16
	addl	%ebx, %eax
	addl	%ebp, %eax
	cmpl	168(%rsp), %eax         # 4-byte Folded Reload
	jle	.LBB11_38
# BB#34:
	movl	%r12d, 64(%rsp)         # 4-byte Spill
	movl	%eax, (%rsp)            # 4-byte Spill
	movl	%r15d, 112(%rsp)        # 4-byte Spill
	movl	%r13d, 120(%rsp)        # 4-byte Spill
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	movl	188(%rsp), %r15d        # 4-byte Reload
	movswl	%r15w, %r12d
	leal	-1(%r12), %esi
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movl	184(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebx
	shll	$16, %ebx
	movswl	%ax, %ebp
	leaq	288(%rsp), %rcx
	movl	%ebp, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	leal	-1(%rbp), %r13d
	leaq	264(%rsp), %rcx
	movl	%r12d, %esi
	movl	%r13d, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	movl	48(%rsp), %ebp          # 4-byte Reload
	addl	%ebp, %r12d
	leaq	216(%rsp), %rcx
	movl	%r12d, %esi
	movl	%r13d, %edx
	callq	getLuma4x4Neighbour
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	leaq	240(%rsp), %rcx
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	%r13d, %edx
	callq	getLuma4x4Neighbour
	testl	%ebx, %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	424(%rsp), %ecx
	movl	%ebp, %edx
	movl	100(%rsp), %ebx         # 4-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %r8          # 8-byte Reload
	movq	200(%rsp), %r9          # 8-byte Reload
	jle	.LBB11_42
# BB#35:
	shll	$16, %r15d
	cmpl	$524287, %r15d          # imm = 0x7FFFF
	jg	.LBB11_39
# BB#36:
	cmpl	$8, 40(%rsp)            # 4-byte Folded Reload
	jne	.LBB11_40
# BB#37:
	cmpl	$16, %edx
	je	.LBB11_41
	jmp	.LBB11_42
.LBB11_38:
	movq	408(%rsp), %r11
	movq	32(%rsp), %r10          # 8-byte Reload
	jmp	.LBB11_105
.LBB11_39:
	cmpl	$16, %r12d
	je	.LBB11_41
	jmp	.LBB11_42
.LBB11_40:
	cmpl	$8, %r12d
	jne	.LBB11_42
.LBB11_41:
	movl	$0, 216(%rsp)
.LBB11_42:
	movl	%ecx, %edx
	subl	%r14d, %edx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	subl	%eax, %ecx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	192(%rsp), %rax         # 8-byte Reload
	imull	$11, medthres(,%rax,4), %r12d
	movslq	%edi, %rax
	movq	(%r9,%rax,8), %rcx
	movq	(%r8,%rax,8), %r8
	movq	predictor(%rip), %rax
	movq	8(%rax), %r9
	subq	$96, %rsp
.Lcfi97:
	.cfi_adjust_cfa_offset 96
	movq	352(%rsp), %rax
	movq	%rax, 88(%rsp)
	movups	336(%rsp), %xmm0
	movups	%xmm0, 72(%rsp)
	movq	328(%rsp), %rax
	movq	%rax, 64(%rsp)
	movups	312(%rsp), %xmm0
	movups	%xmm0, 48(%rsp)
	movq	376(%rsp), %rax
	movq	%rax, 40(%rsp)
	movups	360(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	400(%rsp), %rax
	movq	%rax, 16(%rsp)
	movups	384(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	movswl	%bx, %edx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	EPZSSpatialPredictors
	movl	520(%rsp), %edi
	addq	$96, %rsp
.Lcfi98:
	.cfi_adjust_cfa_offset -96
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	movl	$4, %ebx
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movq	408(%rsp), %r11
	.p2align	4, 0x90
.LBB11_43:                              # =>This Inner Loop Header: Depth=1
	movq	predictor(%rip), %rax
	movq	8(%rax), %rax
	movl	-4(%rax,%rbx), %r15d
	movl	(%rax,%rbx), %r13d
	movswl	(%r11), %eax
	movl	%r15d, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	cmpl	%edi, %eax
	jle	.LBB11_45
# BB#44:                                #   in Loop: Header=BB11_43 Depth=1
	movl	%r13d, %eax
	orl	%r15d, %eax
	jne	.LBB11_57
	jmp	.LBB11_50
	.p2align	4, 0x90
.LBB11_45:                              #   in Loop: Header=BB11_43 Depth=1
	movswl	2(%r11), %eax
	movl	%r13d, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	movl	%r13d, %ecx
	orl	%r15d, %ecx
	je	.LBB11_47
# BB#46:                                #   in Loop: Header=BB11_43 Depth=1
	cmpl	%edi, %eax
	jg	.LBB11_57
.LBB11_47:                              #   in Loop: Header=BB11_43 Depth=1
	movswl	2(%r11), %eax
	movl	%r13d, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	cmovll	%ecx, %eax
	cmpl	%edi, %eax
	jg	.LBB11_50
# BB#48:                                #   in Loop: Header=BB11_43 Depth=1
	movq	EPZSMap(%rip), %rax
	movq	160(%rsp), %rcx         # 8-byte Reload
	leal	(%r13,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	168(%rsp), %rcx         # 8-byte Reload
	leal	(%r15,%rcx), %ecx
	movslq	%ecx, %rcx
	movzwl	EPZSBlkCount(%rip), %edx
	cmpw	%dx, (%rax,%rcx,2)
	je	.LBB11_57
# BB#49:                                #   in Loop: Header=BB11_43 Depth=1
	movw	%dx, (%rax,%rcx,2)
	.p2align	4, 0x90
.LBB11_50:                              # %.thread
                                        #   in Loop: Header=BB11_43 Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	leal	(%r15,%rax), %r10d
	movzbl	mv_rescale(%rip), %ecx
	shll	%cl, %r10d
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%r13,%rax), %eax
	shll	%cl, %eax
	movq	mvbits(%rip), %rdx
	movl	136(%rsp), %r8d         # 4-byte Reload
	shll	%cl, %r8d
	movl	%r8d, %esi
	subl	128(%rsp), %esi         # 4-byte Folded Reload
	movslq	%esi, %rsi
	movl	12(%rsp), %r9d          # 4-byte Reload
	shll	%cl, %r9d
	movl	%r9d, %ecx
	subl	88(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movl	(%rdx,%rcx,4), %ecx
	addl	(%rdx,%rsi,4), %ecx
	movl	%r10d, %esi
	subl	120(%rsp), %esi         # 4-byte Folded Reload
	movslq	%esi, %rsi
	movl	%eax, %edi
	subl	112(%rsp), %edi         # 4-byte Folded Reload
	movslq	%edi, %rdi
	movl	(%rdx,%rdi,4), %ebp
	addl	(%rdx,%rsi,4), %ebp
	movl	440(%rsp), %edx
	imull	%edx, %ecx
	sarl	$16, %ecx
	imull	%edx, %ebp
	sarl	$16, %ebp
	addl	%ecx, %ebp
	movl	%r14d, %ecx
	subl	%ebp, %ecx
	jle	.LBB11_53
# BB#51:                                #   in Loop: Header=BB11_43 Depth=1
	addl	$80, %r8d
	addl	$80, %r9d
	addl	$80, %r10d
	addl	$80, %eax
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	64(%rsp), %esi          # 4-byte Reload
	movl	48(%rsp), %edx          # 4-byte Reload
	pushq	%rax
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	addq	$16, %rsp
.Lcfi101:
	.cfi_adjust_cfa_offset -16
	addl	%ebp, %eax
	movl	(%rsp), %edx            # 4-byte Reload
	cmpl	%edx, %eax
	jge	.LBB11_54
# BB#52:                                #   in Loop: Header=BB11_43 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movl	%edx, %r14d
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	movq	%r15, 24(%rsp)          # 8-byte Spill
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movl	%eax, (%rsp)            # 4-byte Spill
	movb	$1, %al
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	408(%rsp), %r11
.LBB11_53:                              #   in Loop: Header=BB11_43 Depth=1
	movl	424(%rsp), %edi
	jmp	.LBB11_57
.LBB11_54:                              #   in Loop: Header=BB11_43 Depth=1
	cmpl	%r14d, %eax
	movl	20(%rsp), %ecx          # 4-byte Reload
	cmovll	%r15d, %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movl	16(%rsp), %ecx          # 4-byte Reload
	cmovll	%r13d, %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	cmovlel	%eax, %r14d
	movb	$1, %al
	movl	424(%rsp), %edi
	jl	.LBB11_56
# BB#55:                                #   in Loop: Header=BB11_43 Depth=1
	movl	4(%rsp), %eax           # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
.LBB11_56:                              #   in Loop: Header=BB11_43 Depth=1
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	408(%rsp), %r11
	.p2align	4, 0x90
.LBB11_57:                              #   in Loop: Header=BB11_43 Depth=1
	addq	$16, %rbx
	cmpq	$84, %rbx
	jne	.LBB11_43
# BB#58:
	sarl	$3, %r12d
	movl	%r12d, 104(%rsp)        # 4-byte Spill
	cmpl	%r12d, (%rsp)           # 4-byte Folded Reload
	movq	152(%rsp), %r8          # 8-byte Reload
	movq	144(%rsp), %r13         # 8-byte Reload
	jle	.LBB11_65
# BB#59:
	movq	input(%rip), %rax
	cmpl	$0, 4088(%rax)
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	176(%rsp), %rbx         # 8-byte Reload
	je	.LBB11_73
# BB#60:
	movq	192(%rsp), %rax         # 8-byte Reload
	imull	$3, medthres(,%rax,4), %eax
	sarl	%eax
	addl	104(%rsp), %eax         # 4-byte Folded Reload
	cmpl	%eax, (%rsp)            # 4-byte Folded Reload
	jge	.LBB11_67
# BB#61:
	movl	%r10d, %eax
	orl	%esi, %eax
	je	.LBB11_64
# BB#62:
	movswl	(%r11), %eax
	movl	%esi, %ecx
	subl	%eax, %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movl	$2, %eax
	movl	$2, %ecx
	subl	mv_rescale(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	cmpl	%eax, %edx
	jge	.LBB11_71
# BB#63:
	movswl	2(%r11), %ecx
	movl	%r10d, %edx
	subl	%ecx, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	cmpl	%eax, %ecx
	jge	.LBB11_71
.LBB11_64:
	movq	sdiamond(%rip), %rbx
	jmp	.LBB11_73
.LBB11_65:
	movq	32(%rsp), %r10          # 8-byte Reload
.LBB11_66:                              # %.critedge
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB11_104
.LBB11_67:
	cmpl	$5, 384(%rsp)
	jg	.LBB11_71
# BB#68:
	cmpw	$0, 100(%rsp)           # 2-byte Folded Reload
	jle	.LBB11_72
# BB#69:
	cmpl	$1, 384(%rsp)
	je	.LBB11_72
.LBB11_71:
	movq	square(%rip), %rbx
.LBB11_73:                              # %.preheader
	movl	%r10d, %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%esi, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	12(%rsp), %r9d          # 4-byte Reload
	jmp	.LBB11_75
.LBB11_90:                              #   in Loop: Header=BB11_76 Depth=2
	movq	8(%rbx), %rcx
	movslq	32(%rsp), %rdx          # 4-byte Folded Reload
	shlq	$4, %rdx
	leaq	12(%rcx,%rdx), %rax
	movl	8(%rcx,%rdx), %r14d
	xorl	%edx, %edx
	movl	%r10d, %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	80(%rsp), %ecx          # 4-byte Reload
	cmpl	$1, %ecx
	jne	.LBB11_76
	.p2align	4, 0x90
.LBB11_91:                              #   in Loop: Header=BB11_75 Depth=1
	cmpl	$4, 384(%rsp)
	jg	.LBB11_66
# BB#92:                                #   in Loop: Header=BB11_75 Depth=1
	cmpb	$1, 4(%rsp)             # 1-byte Folded Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	jne	.LBB11_104
# BB#93:                                #   in Loop: Header=BB11_75 Depth=1
	movl	104(%rsp), %eax         # 4-byte Reload
	cmpl	%eax, (%rsp)            # 4-byte Folded Reload
	jle	.LBB11_104
# BB#94:                                #   in Loop: Header=BB11_75 Depth=1
	movq	input(%rip), %rax
	cmpl	$0, 4092(%rax)
	jle	.LBB11_104
# BB#95:                                #   in Loop: Header=BB11_75 Depth=1
	movl	%r10d, %eax
	orl	%r14d, %eax
	movswl	(%r11), %eax
	je	.LBB11_98
# BB#96:                                #   in Loop: Header=BB11_75 Depth=1
	cmpl	%eax, %r14d
	jne	.LBB11_103
# BB#97:                                #   in Loop: Header=BB11_75 Depth=1
	movswl	2(%r11), %ecx
	cmpl	%ecx, %r10d
	jne	.LBB11_103
.LBB11_98:                              # %._crit_edge
                                        #   in Loop: Header=BB11_75 Depth=1
	movl	%r14d, %ecx
	subl	%eax, %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movl	$2, %ecx
	subl	mv_rescale(%rip), %ecx
	movl	$2, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	cmpl	%eax, %edx
	jge	.LBB11_101
# BB#99:                                #   in Loop: Header=BB11_75 Depth=1
	movswl	2(%r11), %ecx
	movl	%r10d, %edx
	subl	%ecx, %edx
	movl	%edx, %ecx
	negl	%ecx
	cmovll	%edx, %ecx
	cmpl	%eax, %ecx
	jge	.LBB11_101
# BB#100:                               #   in Loop: Header=BB11_75 Depth=1
	movl	$sdiamond, %eax
	jmp	.LBB11_74
	.p2align	4, 0x90
.LBB11_103:                             #   in Loop: Header=BB11_75 Depth=1
	movl	$searchPatternD, %eax
	jmp	.LBB11_74
	.p2align	4, 0x90
.LBB11_101:                             #   in Loop: Header=BB11_75 Depth=1
	movl	$square, %eax
.LBB11_74:                              #   in Loop: Header=BB11_75 Depth=1
	movq	(%rax), %rbx
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB11_75:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_76 Depth 2
                                        #       Child Loop BB11_77 Depth 3
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	movq	%rbx, %rax
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB11_76:                              #   Parent Loop BB11_75 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_77 Depth 3
	movl	%edx, 56(%rsp)          # 4-byte Spill
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	movl	(%rax), %ebp
	incl	%ebp
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB11_77:                              #   Parent Loop BB11_75 Depth=1
                                        #     Parent Loop BB11_76 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbx), %rax
	movslq	%r14d, %rcx
	shlq	$4, %rcx
	movl	(%rax,%rcx), %r12d
	addl	8(%rsp), %r12d          # 4-byte Folded Reload
	movl	4(%rax,%rcx), %r15d
	addl	40(%rsp), %r15d         # 4-byte Folded Reload
	leal	(%r12,%r8), %eax
	movl	mv_rescale(%rip), %ecx
	shll	%cl, %eax
	leal	(%r15,%r13), %ebx
	shll	%cl, %ebx
	movswl	(%r11), %edx
	movl	%r12d, %esi
	subl	%edx, %esi
	movl	%esi, %edx
	negl	%edx
	cmovll	%esi, %edx
	cmpl	%edi, %edx
	jg	.LBB11_84
# BB#78:                                #   in Loop: Header=BB11_77 Depth=3
	movswl	2(%r11), %edx
	movl	%r15d, %esi
	subl	%edx, %esi
	movl	%esi, %edx
	negl	%edx
	cmovll	%esi, %edx
	cmpl	%edi, %edx
	jg	.LBB11_84
# BB#79:                                #   in Loop: Header=BB11_77 Depth=3
	movq	EPZSMap(%rip), %rdx
	movq	160(%rsp), %rsi         # 8-byte Reload
	leal	(%r15,%rsi), %esi
	movslq	%esi, %rsi
	movq	(%rdx,%rsi,8), %rdx
	movq	168(%rsp), %rsi         # 8-byte Reload
	leal	(%r12,%rsi), %esi
	movslq	%esi, %rsi
	movzwl	EPZSBlkCount(%rip), %edi
	cmpw	%di, (%rdx,%rsi,2)
	jne	.LBB11_81
# BB#80:                                #   in Loop: Header=BB11_77 Depth=3
	incl	%r14d
	movl	%r14d, %eax
	movq	176(%rsp), %rbx         # 8-byte Reload
	subl	(%rbx), %eax
	setl	%cl
	movl	424(%rsp), %edi
	jmp	.LBB11_85
.LBB11_81:                              #   in Loop: Header=BB11_77 Depth=3
	movw	%di, (%rdx,%rsi,2)
	movq	mvbits(%rip), %rdx
	movl	136(%rsp), %r8d         # 4-byte Reload
	shll	%cl, %r8d
	movl	%r8d, %esi
	subl	128(%rsp), %esi         # 4-byte Folded Reload
	movslq	%esi, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r9d
	movl	%r9d, %ecx
	subl	88(%rsp), %ecx          # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movl	(%rdx,%rcx,4), %ecx
	addl	(%rdx,%rsi,4), %ecx
	movl	%eax, %esi
	subl	120(%rsp), %esi         # 4-byte Folded Reload
	movslq	%esi, %rsi
	movl	%ebx, %edi
	subl	112(%rsp), %edi         # 4-byte Folded Reload
	movslq	%edi, %rdi
	movl	(%rdx,%rdi,4), %r13d
	addl	(%rdx,%rsi,4), %r13d
	movl	440(%rsp), %edx
	imull	%edx, %ecx
	sarl	$16, %ecx
	imull	%edx, %r13d
	sarl	$16, %r13d
	addl	%ecx, %r13d
	movl	(%rsp), %edx            # 4-byte Reload
	movl	%edx, %ecx
	subl	%r13d, %ecx
	jle	.LBB11_83
# BB#82:                                #   in Loop: Header=BB11_77 Depth=3
	addl	$80, %r8d
	addl	$80, %r9d
	addl	$80, %eax
	addl	$80, %ebx
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	64(%rsp), %esi          # 4-byte Reload
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	48(%rsp), %edx          # 4-byte Reload
	pushq	%rbx
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi103:
	.cfi_adjust_cfa_offset 8
	movq	%r10, %rbx
	callq	*computeBiPred(%rip)
	movq	%rbx, %r10
	movq	424(%rsp), %r11
	addq	$16, %rsp
.Lcfi104:
	.cfi_adjust_cfa_offset -16
	addl	%r13d, %eax
	movl	(%rsp), %edx            # 4-byte Reload
	cmpl	%edx, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmovll	%r12d, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmovll	%r15d, %r10d
	cmovlel	%eax, %edx
	movl	%edx, (%rsp)            # 4-byte Spill
	movl	32(%rsp), %eax          # 4-byte Reload
	cmovll	%r14d, %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
.LBB11_83:                              #   in Loop: Header=BB11_77 Depth=3
	movl	12(%rsp), %r9d          # 4-byte Reload
	movq	152(%rsp), %r8          # 8-byte Reload
	movq	144(%rsp), %r13         # 8-byte Reload
	movl	424(%rsp), %edi
	.p2align	4, 0x90
.LBB11_84:                              #   in Loop: Header=BB11_77 Depth=3
	incl	%r14d
	movl	%r14d, %eax
	movq	176(%rsp), %rbx         # 8-byte Reload
	subl	(%rbx), %eax
	setl	%cl
.LBB11_85:                              #   in Loop: Header=BB11_77 Depth=3
	testb	%cl, %cl
	cmovel	%eax, %r14d
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB11_77
# BB#86:                                #   in Loop: Header=BB11_76 Depth=2
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	jne	.LBB11_89
# BB#87:                                #   in Loop: Header=BB11_76 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	%eax, 24(%rsp)          # 4-byte Folded Reload
	jne	.LBB11_90
# BB#88:                                #   in Loop: Header=BB11_76 Depth=2
	cmpl	40(%rsp), %r10d         # 4-byte Folded Reload
	jne	.LBB11_90
.LBB11_89:                              #   in Loop: Header=BB11_76 Depth=2
	movl	16(%rbx), %ecx
	movq	24(%rbx), %rbx
	movl	20(%rbx), %edx
	xorl	%r14d, %r14d
	movq	%rbx, %rax
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	cmpl	$1, %ecx
	jne	.LBB11_76
	jmp	.LBB11_91
.LBB11_104:                             # %.critedge
	movl	(%rsp), %eax            # 4-byte Reload
.LBB11_105:
	movw	%r14w, (%r11)
	movw	%r10w, 2(%r11)
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_72:
	movq	searchPattern(%rip), %rbx
	jmp	.LBB11_73
.Lfunc_end11:
	.size	EPZSBiPredBlockMotionSearch, .Lfunc_end11-EPZSBiPredBlockMotionSearch
	.cfi_endproc

	.globl	EPZSOutputStats
	.p2align	4, 0x90
	.type	EPZSOutputStats,@function
EPZSOutputStats:                        # @EPZSOutputStats
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 16
.Lcfi106:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	input(%rip), %rax
	movslq	4088(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSPattern(,%rax,4), %rdx
	movzwl	%si, %eax
	cmpl	$1, %eax
	jne	.LBB12_3
# BB#1:
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4092(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSDualPattern(,%rax,4), %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4096(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSFixed(,%rax,4), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4100(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSOther(,%rax,4), %rdx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4104(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSOther(,%rax,4), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	medthres+4(%rip), %edx
	movl	minthres+4(%rip), %ecx
	movl	maxthres+4(%rip), %r8d
	movl	$.L.str.7, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4124(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSOther(,%rax,4), %rdx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4128(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSOther(,%rax,4), %rdx
	movl	$.L.str.9, %esi
	jmp	.LBB12_2
.LBB12_3:
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4092(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSDualPattern(,%rax,4), %rdx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4096(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSFixed(,%rax,4), %rdx
	movl	$.L.str.12, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4100(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSOther(,%rax,4), %rdx
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4104(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSOther(,%rax,4), %rdx
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	medthres+4(%rip), %edx
	movl	minthres+4(%rip), %ecx
	movl	maxthres+4(%rip), %r8d
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4124(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSOther(,%rax,4), %rdx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	input(%rip), %rax
	movslq	4128(%rax), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	c_EPZSOther(,%rax,4), %rdx
	movl	$.L.str.17, %esi
.LBB12_2:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fprintf                 # TAILCALL
.Lfunc_end12:
	.size	EPZSOutputStats, .Lfunc_end12-EPZSOutputStats
	.cfi_endproc

	.globl	EPZSSubPelBlockMotionSearch
	.p2align	4, 0x90
	.type	EPZSSubPelBlockMotionSearch,@function
EPZSSubPelBlockMotionSearch:            # @EPZSSubPelBlockMotionSearch
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi110:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi111:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi113:
	.cfi_def_cfa_offset 160
.Lcfi114:
	.cfi_offset %rbx, -56
.Lcfi115:
	.cfi_offset %r12, -48
.Lcfi116:
	.cfi_offset %r13, -40
.Lcfi117:
	.cfi_offset %r14, -32
.Lcfi118:
	.cfi_offset %r15, -24
.Lcfi119:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movq	200(%rsp), %r10
	movl	176(%rsp), %eax
	movq	input(%rip), %rdi
	movslq	%r9d, %rbx
	movl	72(%rdi,%rbx,8), %ebp
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movl	76(%rdi,%rbx,8), %ebp
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	cmpl	$0, start_me_refinement_qp(%rip)
	movslq	start_me_refinement_hp(%rip), %r13
	movl	$1, %ebp
	movl	$1, %r11d
	cmovnel	%eax, %r11d
	testq	%r13, %r13
	cmovel	%ebp, %r11d
	cmpl	$1, %eax
	cmovgl	%eax, %r11d
	movq	img(%rip), %rbp
	movq	14224(%rbp), %rax
	movslq	12(%rbp), %rbx
	imulq	$536, %rbx, %rbx        # imm = 0x218
	movslq	432(%rax,%rbx), %r9
	movq	active_pps(%rip), %rbx
	cmpl	$0, 192(%rbx)
	je	.LBB13_3
# BB#1:
	movl	20(%rbp), %eax
	testl	%eax, %eax
	je	.LBB13_5
# BB#2:
	cmpl	$3, %eax
	je	.LBB13_5
.LBB13_3:
	cmpl	$0, 196(%rbx)
	je	.LBB13_7
# BB#4:
	cmpl	$1, 20(%rbp)
	jne	.LBB13_7
.LBB13_5:
	cmpl	$0, 2936(%rdi)
	setne	%bpl
	jmp	.LBB13_8
.LBB13_7:
	xorl	%ebp, %ebp
.LBB13_8:
	movslq	%edx, %rax
	addq	%rax, %r9
	movq	listX(,%r9,8), %rax
	movswq	%si, %rdx
	movq	(%rax,%rdx,8), %rsi
	movl	6392(%rsi), %ebx
	movl	6396(%rsi), %r14d
	movl	4(%r10), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	6448(%rsi), %rax
	movq	%rax, ref_pic_sub(%rip)
	movl	6408(%rsi), %eax
	movl	%eax, width_pad(%rip)
	movl	6412(%rsi), %eax
	movl	%eax, height_pad(%rip)
	testb	%bpl, %bpl
	je	.LBB13_10
# BB#9:
	movq	wp_weight(%rip), %rax
	movq	(%rax,%r9,8), %rax
	movq	(%rax,%rdx,8), %rax
	movl	(%rax), %eax
	movl	%eax, weight_luma(%rip)
	movq	wp_offset(%rip), %rax
	movq	(%rax,%r9,8), %rax
	movq	(%rax,%rdx,8), %rax
	movl	(%rax), %eax
	movl	%eax, offset_luma(%rip)
.LBB13_10:
	movzbl	%bpl, %r10d
	movq	168(%rsp), %rdi
	leal	80(,%rcx,4), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB13_13
# BB#11:
	movq	6464(%rsi), %rax
	movq	(%rax), %rcx
	movq	%rcx, ref_pic_sub+8(%rip)
	movq	8(%rax), %rax
	movq	%rax, ref_pic_sub+16(%rip)
	movl	6416(%rsi), %eax
	movl	%eax, width_pad_cr(%rip)
	movl	6420(%rsi), %eax
	movl	%eax, height_pad_cr(%rip)
	testb	%bpl, %bpl
	je	.LBB13_13
# BB#12:
	movq	wp_weight(%rip), %rax
	movq	(%rax,%r9,8), %rax
	movq	(%rax,%rdx,8), %rax
	movl	4(%rax), %ecx
	movl	%ecx, weight_cr(%rip)
	movl	8(%rax), %eax
	movl	%eax, weight_cr+4(%rip)
	movq	wp_offset(%rip), %rax
	movq	(%rax,%r9,8), %rax
	movq	(%rax,%rdx,8), %rax
	movl	4(%rax), %ecx
	movl	%ecx, offset_cr(%rip)
	movl	8(%rax), %eax
	movl	%eax, offset_cr+4(%rip)
.LBB13_13:
	subl	12(%rsp), %ebx          # 4-byte Folded Reload
	subl	8(%rsp), %r14d          # 4-byte Folded Reload
	leal	80(,%r8,4), %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movswl	(%rdi), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	leal	(%rax,%rcx), %edx
	movl	$1, %ecx
	cmpl	$2, %edx
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	jl	.LBB13_17
# BB#14:
	leal	(,%rbx,4), %esi
	addl	$159, %esi
	cmpl	%esi, %edx
	jge	.LBB13_17
# BB#15:
	movswl	2(%rdi), %edx
	addl	48(%rsp), %edx          # 4-byte Folded Reload
	cmpl	$2, %edx
	jl	.LBB13_17
# BB#16:
	leal	(,%r14,4), %esi
	addl	$159, %esi
	xorl	%ecx, %ecx
	cmpl	%esi, %edx
	setge	%cl
.LBB13_17:
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movl	192(%rsp), %r12d
	leal	1(%r10,%r10,2), %esi
	movl	%ecx, ref_access_method(%rip)
	xorl	%edx, %edx
	cmpl	$4, %r13d
	movl	%r11d, 40(%rsp)         # 4-byte Spill
	movq	%r10, 16(%rsp)          # 8-byte Spill
	jg	.LBB13_29
# BB#18:                                # %.lr.ph388
	movl	%r12d, %r15d
	movl	%esi, 68(%rsp)          # 4-byte Spill
	movl	%esi, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	search_point_hp+2(,%r13,4), %r12
	movl	$2147483647, %ebp       # imm = 0x7FFFFFFF
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	xorl	%r14d, %r14d
	jmp	.LBB13_20
	.p2align	4, 0x90
.LBB13_19:                              # %._crit_edge405
                                        #   in Loop: Header=BB13_20 Depth=1
	movw	(%rdi), %ax
	addq	$4, %r12
	incl	%r13d
.LBB13_20:                              # =>This Inner Loop Header: Depth=1
	movswq	-2(%r12), %rcx
	movswq	%ax, %rax
	addq	%rcx, %rax
	movswq	2(%rdi), %rcx
	movswq	(%r12), %rdx
	addq	%rcx, %rdx
	movq	mvbits(%rip), %rcx
	movq	160(%rsp), %rsi
	movq	%rsi, %rbx
	movswq	(%rbx), %rsi
	movq	56(%rsp), %rdi          # 8-byte Reload
	leal	(%rax,%rdi), %r8d
	subq	%rsi, %rax
	movswq	2(%rbx), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	leal	(%rdx,%rdi), %r9d
	subq	%rsi, %rdx
	movl	(%rcx,%rdx,4), %ebx
	addl	(%rcx,%rax,4), %ebx
	imull	32(%rsp), %ebx          # 4-byte Folded Reload
	sarl	$16, %ebx
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	addl	%ebx, %eax
	movl	%r15d, %ecx
	cmpl	%ecx, %eax
	jge	.LBB13_22
# BB#21:                                #   in Loop: Header=BB13_20 Depth=1
	movl	%ecx, %ebp
	movl	4(%rsp), %r14d          # 4-byte Reload
	movl	%r13d, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	%eax, %r15d
	jmp	.LBB13_23
	.p2align	4, 0x90
.LBB13_22:                              #   in Loop: Header=BB13_20 Depth=1
	cmpl	%ebp, %eax
	cmovlel	%eax, %ebp
	cmovll	%r13d, %r14d
.LBB13_23:                              #   in Loop: Header=BB13_20 Depth=1
	movq	168(%rsp), %rdi
	cmpl	$4, %r13d
	jne	.LBB13_19
# BB#24:                                # %._crit_edge389
	movl	4(%rsp), %ecx           # 4-byte Reload
	testl	%ecx, %ecx
	je	.LBB13_30
# BB#25:
	testl	%r14d, %r14d
	movl	%r15d, %r12d
	movq	160(%rsp), %r13
	movl	68(%rsp), %esi          # 4-byte Reload
	je	.LBB13_36
# BB#26:
	xorl	%ecx, %r14d
	decl	%r14d
	movl	$5, %r15d
	cmpl	$7, %r14d
	jae	.LBB13_43
# BB#27:                                # %switch.hole_check
	movl	$85, %eax
	btl	%r14d, %eax
	jae	.LBB13_43
# BB#28:                                # %switch.lookup
	movslq	%r14d, %rax
	movl	.Lswitch.table.14(,%rax,4), %r15d
	movl	.Lswitch.table.15(,%rax,4), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	cmpl	40(%rsp), %r15d         # 4-byte Folded Reload
	jl	.LBB13_44
	jmp	.LBB13_48
.LBB13_29:
	xorl	%r14d, %r14d
	movq	160(%rsp), %rcx
	jmp	.LBB13_31
.LBB13_30:
	movl	%r15d, %r12d
	movq	160(%rsp), %rcx
	movl	68(%rsp), %esi          # 4-byte Reload
	xorl	%edx, %edx
.LBB13_31:                              # %._crit_edge389.thread
	movzwl	(%rcx), %eax
	cmpw	(%rdi), %ax
	jne	.LBB13_34
# BB#32:
	movzwl	2(%rcx), %eax
	xorl	%ecx, %ecx
	cmpw	2(%rdi), %ax
	jne	.LBB13_35
# BB#33:
	xorl	%ecx, %ecx
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	subthres(,%rax,4), %r12d
	movl	$0, %edx
	jge	.LBB13_37
	jmp	.LBB13_90
.LBB13_34:
	xorl	%ecx, %ecx
	jmp	.LBB13_37
.LBB13_35:
	xorl	%edx, %edx
	jmp	.LBB13_37
.LBB13_36:
	movb	$1, %dl
	xorl	%r14d, %r14d
.LBB13_37:                              # %.thread
	addl	%ecx, %r14d
	cmpl	$8, %r14d
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	jae	.LBB13_39
# BB#38:                                # %switch.hole_check313
	movl	$167, %eax
	btl	%r14d, %eax
	jb	.LBB13_40
.LBB13_39:
	movl	$5, %r15d
	jmp	.LBB13_41
.LBB13_40:                              # %switch.lookup314
	movslq	%r14d, %rax
	movl	.Lswitch.table.12(,%rax,4), %r15d
	movl	.Lswitch.table.13(,%rax,4), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
.LBB13_41:
	testb	%dl, %dl
	movq	160(%rsp), %r13
	jne	.LBB13_43
# BB#42:
	movswl	(%r13), %eax
	movswl	(%rdi), %ecx
	subl	%ecx, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movswl	2(%r13), %eax
	movswl	2(%rdi), %edx
	subl	%edx, %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	addl	%ecx, %edx
	je	.LBB13_48
.LBB13_43:                              # %.critedge.preheader
	cmpl	40(%rsp), %r15d         # 4-byte Folded Reload
	jge	.LBB13_48
.LBB13_44:                              # %.lr.ph379
	movl	%esi, %r14d
	movslq	%r15d, %rax
	leaq	search_point_hp(,%rax,4), %rbp
	.p2align	4, 0x90
.LBB13_45:                              # =>This Inner Loop Header: Depth=1
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pshufd	$212, %xmm0, %xmm1      # xmm1 = xmm0[0,1,1,3]
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	paddq	%xmm1, %xmm0
	movq	mvbits(%rip), %rax
	movd	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	pshufd	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	movdqa	%xmm0, %xmm2
	psubd	%xmm1, %xmm2
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movd	%xmm1, %rcx
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %rdx
	movl	(%rax,%rdx,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	imull	32(%rsp), %ebx          # 4-byte Folded Reload
	sarl	$16, %ebx
	movl	%r12d, %ecx
	subl	%ebx, %ecx
	jle	.LBB13_47
# BB#46:                                #   in Loop: Header=BB13_45 Depth=1
	movd	%xmm0, %r8d
	addl	56(%rsp), %r8d          # 4-byte Folded Reload
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %r9d
	addl	48(%rsp), %r9d          # 4-byte Folded Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	*computeUniPred(,%r14,8)
	movq	168(%rsp), %rdi
	addl	%ebx, %eax
	cmpl	%r12d, %eax
	movl	4(%rsp), %ecx           # 4-byte Reload
	cmovll	%r15d, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	cmovlel	%eax, %r12d
.LBB13_47:                              # %.critedge
                                        #   in Loop: Header=BB13_45 Depth=1
	addq	$4, %rbp
	incl	%r15d
	cmpl	%r15d, 40(%rsp)         # 4-byte Folded Reload
	jne	.LBB13_45
.LBB13_48:                              # %.loopexit358
	movl	4(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	je	.LBB13_50
# BB#49:
	movslq	%eax, %rcx
	movzwl	(%rdi), %eax
	addw	search_point_hp(,%rcx,4), %ax
	movw	%ax, (%rdi)
	movzwl	search_point_hp+2(,%rcx,4), %ecx
	addw	%cx, 2(%rdi)
	jmp	.LBB13_51
.LBB13_50:                              # %.loopexit358._crit_edge
	movzwl	(%rdi), %eax
.LBB13_51:
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rcx,2), %r8d
	movslq	start_me_refinement_qp(%rip), %rbx
	testq	%rbx, %rbx
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	cmovel	%ecx, %r12d
	movswl	%ax, %edx
	movl	$1, %ecx
	addl	56(%rsp), %edx          # 4-byte Folded Reload
	jle	.LBB13_55
# BB#52:
	movq	96(%rsp), %rsi          # 8-byte Reload
	leal	160(,%rsi,4), %esi
	cmpl	%esi, %edx
	jge	.LBB13_55
# BB#53:
	movswl	2(%rdi), %edx
	addl	48(%rsp), %edx          # 4-byte Folded Reload
	jle	.LBB13_55
# BB#54:
	movq	88(%rsp), %rcx          # 8-byte Reload
	leal	160(,%rcx,4), %esi
	xorl	%ecx, %ecx
	cmpl	%esi, %edx
	setge	%cl
.LBB13_55:
	movl	184(%rsp), %r14d
	movl	%ecx, ref_access_method(%rip)
	addl	$2, %r8d
	movq	200(%rsp), %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	xorl	%edx, %edx
	cmpl	$4, %ebx
	jg	.LBB13_68
# BB#56:                                # %.lr.ph372
	movl	%r12d, %r13d
	movl	%r8d, 16(%rsp)          # 4-byte Spill
	movl	%r8d, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	search_point_qp+2(,%rbx,4), %r14
	movl	$2147483647, %r15d      # imm = 0x7FFFFFFF
	xorl	%ecx, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB13_58
	.p2align	4, 0x90
.LBB13_57:                              # %._crit_edge407
                                        #   in Loop: Header=BB13_58 Depth=1
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	%r12d, %r13d
	movzwl	(%rdi), %eax
	addq	$4, %r14
	incl	%ebx
.LBB13_58:                              # =>This Inner Loop Header: Depth=1
	movswq	-2(%r14), %rcx
	movswq	%ax, %rax
	addq	%rcx, %rax
	movswq	2(%rdi), %rcx
	movswq	(%r14), %rdx
	addq	%rcx, %rdx
	movq	mvbits(%rip), %rcx
	movq	160(%rsp), %rsi
	movq	%rsi, %rbp
	movswq	(%rbp), %rsi
	movq	56(%rsp), %rdi          # 8-byte Reload
	leal	(%rax,%rdi), %r8d
	subq	%rsi, %rax
	movswq	2(%rbp), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	leal	(%rdx,%rdi), %r9d
	subq	%rsi, %rdx
	movl	(%rcx,%rdx,4), %r12d
	addl	(%rcx,%rax,4), %r12d
	imull	4(%rsp), %r12d          # 4-byte Folded Reload
	sarl	$16, %r12d
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	addl	%r12d, %eax
	movl	%r13d, %r12d
	cmpl	%r12d, %eax
	jge	.LBB13_60
# BB#59:                                #   in Loop: Header=BB13_58 Depth=1
	movl	%r12d, %r15d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %ebp
	movl	%ebx, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	%eax, %r12d
	jmp	.LBB13_61
	.p2align	4, 0x90
.LBB13_60:                              #   in Loop: Header=BB13_58 Depth=1
	cmpl	%r15d, %eax
	cmovlel	%eax, %r15d
	movq	32(%rsp), %rbp          # 8-byte Reload
	cmovll	%ebx, %ebp
.LBB13_61:                              #   in Loop: Header=BB13_58 Depth=1
	movq	168(%rsp), %rdi
	cmpl	$4, %ebx
	jne	.LBB13_57
# BB#62:                                # %._crit_edge
	movq	24(%rsp), %r10          # 8-byte Reload
	testl	%r10d, %r10d
	je	.LBB13_69
# BB#63:
	testl	%ebp, %ebp
	movl	184(%rsp), %r14d
	je	.LBB13_75
# BB#64:
	xorl	%r10d, %ebp
	decl	%ebp
	movl	$5, %r15d
	cmpl	$7, %ebp
	jae	.LBB13_82
# BB#65:                                # %switch.hole_check325
	movl	$85, %eax
	btl	%ebp, %eax
	movl	16(%rsp), %r8d          # 4-byte Reload
	jae	.LBB13_67
# BB#66:                                # %switch.lookup326
	movslq	%ebp, %rax
	movl	.Lswitch.table.14(,%rax,4), %r15d
	movl	.Lswitch.table.15(,%rax,4), %r14d
.LBB13_67:
	movq	160(%rsp), %r13
	movq	%r13, %rsi
	cmpl	%r14d, %r15d
	jl	.LBB13_84
	jmp	.LBB13_88
.LBB13_68:
	xorl	%ebp, %ebp
	jmp	.LBB13_70
.LBB13_69:
	movl	184(%rsp), %r14d
	movl	16(%rsp), %r8d          # 4-byte Reload
	xorl	%edx, %edx
.LBB13_70:                              # %._crit_edge.thread
	movq	160(%rsp), %rcx
	movzwl	(%rcx), %eax
	cmpw	(%rdi), %ax
	jne	.LBB13_73
# BB#71:
	movzwl	2(%rcx), %eax
	xorl	%r10d, %r10d
	cmpw	2(%rdi), %ax
	jne	.LBB13_74
# BB#72:
	xorl	%r10d, %r10d
	movq	80(%rsp), %rax          # 8-byte Reload
	cmpl	subthres(,%rax,4), %r12d
	movl	$0, %edx
	jge	.LBB13_76
	jmp	.LBB13_90
.LBB13_73:
	xorl	%r10d, %r10d
	jmp	.LBB13_76
.LBB13_74:
	xorl	%edx, %edx
	jmp	.LBB13_76
.LBB13_75:
	movb	$1, %dl
	xorl	%ebp, %ebp
	movl	16(%rsp), %r8d          # 4-byte Reload
.LBB13_76:                              # %.thread350
	leal	-1(%rbp,%r10), %eax
	cmpl	$7, %eax
	jae	.LBB13_78
# BB#77:                                # %switch.hole_check336
	movl	$83, %ecx
	btl	%eax, %ecx
	jb	.LBB13_79
.LBB13_78:
	movl	$5, %r15d
	jmp	.LBB13_80
.LBB13_79:                              # %switch.lookup337
	cltq
	movl	.Lswitch.table.16(,%rax,4), %r15d
	movl	.Lswitch.table.17(,%rax,4), %r14d
.LBB13_80:
	testb	%dl, %dl
	movq	160(%rsp), %r13
	movq	%r13, %rsi
	jne	.LBB13_83
# BB#81:
	movswl	(%rsi), %eax
	movswl	(%rdi), %ecx
	subl	%ecx, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movswl	2(%rsi), %eax
	movswl	2(%rdi), %edx
	subl	%edx, %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	addl	%ecx, %edx
	jne	.LBB13_83
	jmp	.LBB13_88
.LBB13_82:
	movq	160(%rsp), %r13
	movq	%r13, %rsi
	movl	16(%rsp), %r8d          # 4-byte Reload
.LBB13_83:                              # %.critedge356.preheader
	cmpl	%r14d, %r15d
	jge	.LBB13_88
.LBB13_84:                              # %.lr.ph
	movl	%r8d, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	%r15d, %rax
	leaq	search_point_qp(,%rax,4), %rbp
	.p2align	4, 0x90
.LBB13_85:                              # =>This Inner Loop Header: Depth=1
	movd	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pshufd	$212, %xmm0, %xmm1      # xmm1 = xmm0[0,1,1,3]
	movd	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	paddq	%xmm1, %xmm0
	movq	mvbits(%rip), %rax
	movd	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	pshufd	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	movdqa	%xmm0, %xmm2
	psubd	%xmm1, %xmm2
	pshufd	$232, %xmm2, %xmm1      # xmm1 = xmm2[0,2,2,3]
	movdqa	%xmm1, %xmm2
	psrad	$31, %xmm2
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movd	%xmm1, %rcx
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %rdx
	movl	(%rax,%rdx,4), %ebx
	addl	(%rax,%rcx,4), %ebx
	imull	4(%rsp), %ebx           # 4-byte Folded Reload
	sarl	$16, %ebx
	movl	%r12d, %ecx
	subl	%ebx, %ecx
	jle	.LBB13_87
# BB#86:                                #   in Loop: Header=BB13_85 Depth=1
	movd	%xmm0, %r8d
	addl	56(%rsp), %r8d          # 4-byte Folded Reload
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movl	%r12d, 32(%rsp)         # 4-byte Spill
	movq	%rsi, %r12
	movd	%xmm0, %r9d
	addl	48(%rsp), %r9d          # 4-byte Folded Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%r10, %r13
	movl	8(%rsp), %esi           # 4-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	callq	*computeUniPred(,%rax,8)
	movq	%r12, %rsi
	movl	32(%rsp), %r12d         # 4-byte Reload
	movq	%r13, %r10
	movq	168(%rsp), %rdi
	addl	%ebx, %eax
	cmpl	%r12d, %eax
	cmovll	%r15d, %r10d
	cmovlel	%eax, %r12d
.LBB13_87:                              # %.critedge356
                                        #   in Loop: Header=BB13_85 Depth=1
	addq	$4, %rbp
	incl	%r15d
	cmpl	%r15d, %r14d
	jne	.LBB13_85
.LBB13_88:                              # %.loopexit
	testl	%r10d, %r10d
	je	.LBB13_90
# BB#89:
	movslq	%r10d, %rax
	movzwl	search_point_qp(,%rax,4), %ecx
	addw	%cx, (%rdi)
	movzwl	search_point_qp+2(,%rax,4), %eax
	addw	%ax, 2(%rdi)
.LBB13_90:
	movl	%r12d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	EPZSSubPelBlockMotionSearch, .Lfunc_end13-EPZSSubPelBlockMotionSearch
	.cfi_endproc

	.globl	EPZSSubPelBlockSearchBiPred
	.p2align	4, 0x90
	.type	EPZSSubPelBlockSearchBiPred,@function
EPZSSubPelBlockSearchBiPred:            # @EPZSSubPelBlockSearchBiPred
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi124:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi126:
	.cfi_def_cfa_offset 160
.Lcfi127:
	.cfi_offset %rbx, -56
.Lcfi128:
	.cfi_offset %r12, -48
.Lcfi129:
	.cfi_offset %r13, -40
.Lcfi130:
	.cfi_offset %r14, -32
.Lcfi131:
	.cfi_offset %r15, -24
.Lcfi132:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%ecx, %r10d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movl	208(%rsp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	192(%rsp), %r15d
	movq	active_pps(%rip), %rax
	movl	196(%rax), %edi
	movq	img(%rip), %rax
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	$536, %rax, %rax        # imm = 0x218
	movslq	432(%rcx,%rax), %r14
	xorl	%ebp, %ebp
	movl	%edi, 60(%rsp)          # 4-byte Spill
	testl	%edi, %edi
	movl	$0, %r13d
	movl	$0, %r11d
	je	.LBB14_5
# BB#1:
	movq	wp_offset(%rip), %rbx
	testl	%edx, %edx
	je	.LBB14_2
# BB#3:
	movq	8(%rbx,%r14,8), %rax
	movq	(%rbx,%r14,8), %rcx
	jmp	.LBB14_4
.LBB14_2:
	movswq	%si, %rcx
	shlq	$3, %rcx
	movq	(%rbx,%r14,8), %rax
	addq	%rcx, %rax
	addq	8(%rbx,%r14,8), %rcx
.LBB14_4:
	movq	(%rax), %rax
	movl	(%rax), %r13d
	movq	(%rcx), %rax
	movl	(%rax), %r11d
.LBB14_5:
	movq	input(%rip), %rax
	movslq	%r9d, %rbx
	movl	72(%rax,%rbx,8), %edi
	movl	76(%rax,%rbx,8), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	cmpl	$2147483647, 208(%rsp)  # imm = 0x7FFFFFFF
	movl	start_me_refinement_hp(%rip), %eax
	cmovnel	%eax, %ebp
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	cmpl	$0, start_me_refinement_qp(%rip)
	movl	$1, %ebx
	movl	$1, %ecx
	cmovnel	%r15d, %ecx
	testl	%eax, %eax
	cmovel	%ebx, %ecx
	cmpl	$1, %r15d
	cmovgl	%r15d, %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movq	184(%rsp), %rax
	movswl	(%rax), %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movswl	2(%rax), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leal	(%r14,%rdx), %eax
	cltq
	movq	listX(,%rax,8), %rax
	movswq	%si, %r12
	movq	(%rax,%r12,8), %rbp
	movl	%edx, %eax
	xorl	$1, %eax
	addl	%r14d, %eax
	cltq
	movq	listX(,%rax,8), %rax
	movq	(%rax), %r15
	movl	6392(%rbp), %eax
	movl	6396(%rbp), %esi
	movq	216(%rsp), %rcx
	movl	4(%rcx), %ecx
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movq	6448(%rbp), %r9
	movq	%r9, ref_pic1_sub(%rip)
	movq	6448(%r15), %rcx
	movq	%rcx, ref_pic2_sub(%rip)
	movw	%ax, img_width(%rip)
	movq	%rsi, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movw	%si, img_height(%rip)
	movl	6408(%rbp), %ecx
	movl	%ecx, width_pad(%rip)
	movl	6412(%rbp), %ecx
	movl	%ecx, height_pad(%rip)
	movl	%eax, %esi
	movl	%edi, 20(%rsp)          # 4-byte Spill
	subl	%edi, %esi
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	je	.LBB14_10
# BB#6:
	movq	wbp_weight(%rip), %rbx
	testl	%edx, %edx
	je	.LBB14_7
# BB#8:
	movq	8(%rbx,%r14,8), %rax
	movq	(%rax), %rcx
	leaq	(,%r12,8), %rax
	movq	(%rcx,%r12,8), %rcx
	movzwl	(%rcx), %ecx
	movw	%cx, weight1(%rip)
	movq	(%rbx,%r14,8), %rcx
	addq	(%rcx), %rax
	jmp	.LBB14_9
.LBB14_10:
	movb	luma_log_weight_denom(%rip), %cl
	shll	%cl, %ebx
	movw	%bx, weight1(%rip)
	movw	%bx, weight2(%rip)
	movl	$computeBiPred1+8, %ecx
	xorl	%eax, %eax
	jmp	.LBB14_11
.LBB14_7:
	movq	(%rbx,%r14,8), %rax
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rax
	movzwl	(%rax), %eax
	movw	%ax, weight1(%rip)
	movq	8(%rbx,%r14,8), %rax
	movq	(%rax,%r12,8), %rax
.LBB14_9:
	movq	(%rax), %rax
	movzwl	(%rax), %eax
	movw	%ax, weight2(%rip)
	movswl	%r13w, %eax
	movswl	%r11w, %ecx
	leal	1(%rax,%rcx), %eax
	shrl	%eax
	movl	$computeBiPred2+8, %ecx
.LBB14_11:
	movq	80(%rsp), %rdi          # 8-byte Reload
	subl	16(%rsp), %edi          # 4-byte Folded Reload
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	176(%rsp), %rbx
	leal	80(,%r10,4), %edi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	leal	(,%rsi,4), %r9d
	movw	%ax, offsetBi(%rip)
	movq	(%rcx), %r11
	movq	%r11, computeBiPred(%rip)
	cmpl	$0, ChromaMEEnable(%rip)
	je	.LBB14_22
# BB#12:
	movq	6464(%rbp), %rcx
	movq	(%rcx), %rbx
	movq	%rbx, ref_pic1_sub+8(%rip)
	movq	8(%rcx), %rcx
	movq	%rcx, ref_pic1_sub+16(%rip)
	movq	6464(%r15), %rcx
	movq	(%rcx), %rbx
	movq	%rbx, ref_pic2_sub+8(%rip)
	movq	8(%rcx), %rcx
	movq	%rcx, ref_pic2_sub+16(%rip)
	movl	6416(%rbp), %ecx
	movl	%ecx, width_pad_cr(%rip)
	movl	6420(%rbp), %ecx
	movl	%ecx, height_pad_cr(%rip)
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	je	.LBB14_20
# BB#13:
	movq	wbp_weight(%rip), %rcx
	testl	%edx, %edx
	je	.LBB14_14
# BB#15:
	movq	8(%rcx,%r14,8), %rbp
	movq	(%rbp), %rbp
	movq	(%rbp,%r12,8), %rbp
	movzwl	4(%rbp), %ebx
	movw	%bx, weight1_cr(%rip)
	movzwl	8(%rbp), %ebp
	movw	%bp, weight1_cr+2(%rip)
	movq	(%rcx,%r14,8), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r12,8), %rcx
	movzwl	4(%rcx), %ebp
	movw	%bp, weight2_cr(%rip)
	movzwl	8(%rcx), %ecx
	movw	%cx, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rcx
	movq	8(%rcx,%r14,8), %rbp
	movq	(%rcx,%r14,8), %r15
	jmp	.LBB14_16
.LBB14_20:
	movb	chroma_log_weight_denom(%rip), %cl
	movl	$1, %edx
	shll	%cl, %edx
	movw	%dx, weight1_cr(%rip)
	movw	%dx, weight1_cr+2(%rip)
	movw	%dx, weight2_cr(%rip)
	movw	%dx, weight2_cr+2(%rip)
	movw	$0, offsetBi_cr(%rip)
	xorl	%ecx, %ecx
	movq	176(%rsp), %rbx
	jmp	.LBB14_21
.LBB14_14:
	movq	(%rcx,%r14,8), %rbp
	leaq	(,%r12,8), %r15
	movq	(%rbp,%r12,8), %rbp
	movq	(%rbp), %rbp
	movzwl	4(%rbp), %ebx
	movw	%bx, weight1_cr(%rip)
	movzwl	8(%rbp), %ebp
	movw	%bp, weight1_cr+2(%rip)
	movq	8(%rcx,%r14,8), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	(%rcx), %rcx
	movzwl	4(%rcx), %ebp
	movw	%bp, weight2_cr(%rip)
	movzwl	8(%rcx), %ecx
	movw	%cx, weight2_cr+2(%rip)
	movq	wp_offset(%rip), %rcx
	movq	(%rcx,%r14,8), %rbp
	addq	%r15, %rbp
	addq	8(%rcx,%r14,8), %r15
.LBB14_16:
	movq	(%rbp), %rbp
	movl	4(%rbp), %ebp
	movq	(%r15), %rbx
	movl	4(%rbx), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, offsetBi_cr(%rip)
	testl	%edx, %edx
	je	.LBB14_17
# BB#18:
	movq	8(%rcx,%r14,8), %rdx
	movq	(%rcx,%r14,8), %r12
	jmp	.LBB14_19
.LBB14_17:
	shlq	$3, %r12
	movq	(%rcx,%r14,8), %rdx
	addq	%r12, %rdx
	addq	8(%rcx,%r14,8), %r12
.LBB14_19:
	movq	176(%rsp), %rbx
	movq	(%rdx), %rcx
	movl	8(%rcx), %ecx
	movq	(%r12), %rdx
	movl	8(%rdx), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
.LBB14_21:
	movw	%cx, offsetBi_cr+2(%rip)
.LBB14_22:
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	leal	80(,%r8,4), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(,%rax,4), %edi
	movswl	(%rbx), %edx
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rdx,%rax), %ebp
	addl	$159, %r9d
	movl	$1, %esi
	cmpl	$2, %ebp
	movl	$1, %ecx
	jl	.LBB14_26
# BB#23:
	cmpl	%r9d, %ebp
	movl	$1, %ecx
	jge	.LBB14_26
# BB#24:
	movswl	2(%rbx), %ebp
	addl	40(%rsp), %ebp          # 4-byte Folded Reload
	cmpl	$2, %ebp
	movl	$1, %ecx
	jl	.LBB14_26
# BB#25:
	leal	159(%rdi), %ebx
	xorl	%ecx, %ecx
	cmpl	%ebx, %ebp
	setge	%cl
.LBB14_26:
	movl	%ecx, bipred2_access_method(%rip)
	movq	184(%rsp), %rax
	movswl	(%rax), %r13d
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%r13,%rax), %ebp
	cmpl	$2, %ebp
	movl	56(%rsp), %r15d         # 4-byte Reload
	movl	8(%rsp), %ecx           # 4-byte Reload
	jl	.LBB14_27
# BB#28:
	cmpl	%r9d, %ebp
	jge	.LBB14_27
# BB#29:
	movq	184(%rsp), %r9
	movswl	2(%r9), %ebx
	addl	40(%rsp), %ebx          # 4-byte Folded Reload
	cmpl	$2, %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	jl	.LBB14_31
# BB#30:
	addl	$159, %edi
	xorl	%esi, %esi
	cmpl	%edi, %ebx
	setge	%sil
	jmp	.LBB14_31
.LBB14_27:
	movq	184(%rsp), %r9
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB14_31:
	leal	80(%rax,%r10,4), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	leal	80(%rbp,%r8,4), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%esi, bipred1_access_method(%rip)
	xorl	%esi, %esi
	cmpl	$4, %ecx
	jg	.LBB14_32
# BB#33:                                # %.lr.ph510
	movslq	%ecx, %rsi
	leaq	search_point_hp+2(,%rsi,4), %r15
	movl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	movq	176(%rsp), %rbx
	jmp	.LBB14_34
	.p2align	4, 0x90
.LBB14_38:                              # %._crit_edge527
                                        #   in Loop: Header=BB14_34 Depth=1
	movl	%edx, 64(%rsp)          # 4-byte Spill
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movw	(%rbx), %dx
	movw	(%r9), %r13w
	movq	computeBiPred(%rip), %r11
	addq	$4, %r15
	incl	%ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
.LBB14_34:                              # =>This Inner Loop Header: Depth=1
	movswq	-2(%r15), %rdi
	movswq	%dx, %rsi
	addq	%rdi, %rsi
	movswq	2(%rbx), %rdx
	movswq	(%r15), %rdi
	addq	%rdx, %rdi
	movq	mvbits(%rip), %rdx
	movq	160(%rsp), %rax
	movq	%rax, %rcx
	movswq	(%rcx), %rax
	movq	48(%rsp), %rbp          # 8-byte Reload
	leal	(%rsi,%rbp), %ebx
	subq	%rax, %rsi
	movswq	2(%rcx), %rax
	movq	40(%rsp), %rbp          # 8-byte Reload
	leal	(%rdi,%rbp), %ebp
	subq	%rax, %rdi
	movl	(%rdx,%rdi,4), %r12d
	addl	(%rdx,%rsi,4), %r12d
	movl	72(%rsp), %edi          # 4-byte Reload
	imull	%edi, %r12d
	sarl	$16, %r12d
	movswq	%r13w, %rax
	movq	168(%rsp), %rcx
	movq	%rcx, %rsi
	movswq	(%rsi), %rcx
	subq	%rcx, %rax
	movswq	2(%r9), %rcx
	movswq	2(%rsi), %rsi
	subq	%rsi, %rcx
	movl	(%rdx,%rcx,4), %r13d
	addl	(%rdx,%rax,4), %r13d
	imull	%edi, %r13d
	sarl	$16, %r13d
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	movq	88(%rsp), %rdi          # 8-byte Reload
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	32(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	pushq	%rbp
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi134:
	.cfi_adjust_cfa_offset 8
	callq	*%r11
	addq	$16, %rsp
.Lcfi135:
	.cfi_adjust_cfa_offset -16
	addl	%r12d, %eax
	addl	%r13d, %eax
	movl	12(%rsp), %r8d          # 4-byte Reload
	cmpl	%r8d, %eax
	jge	.LBB14_36
# BB#35:                                #   in Loop: Header=BB14_34 Depth=1
	movl	%r8d, %r14d
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	%eax, %r8d
	jmp	.LBB14_37
	.p2align	4, 0x90
.LBB14_36:                              #   in Loop: Header=BB14_34 Depth=1
	cmpl	%r14d, %eax
	cmovlel	%eax, %r14d
	movl	8(%rsp), %ecx           # 4-byte Reload
	movl	64(%rsp), %edx          # 4-byte Reload
	cmovll	%ecx, %edx
.LBB14_37:                              #   in Loop: Header=BB14_34 Depth=1
	cmpl	$4, %ecx
	movq	184(%rsp), %r9
	movq	176(%rsp), %rbx
	jne	.LBB14_38
# BB#39:                                # %._crit_edge511
	movl	4(%rsp), %r11d          # 4-byte Reload
	testl	%r11d, %r11d
	setne	%sil
	testl	%edx, %edx
	je	.LBB14_40
# BB#44:                                # %._crit_edge511
	testl	%r11d, %r11d
	movl	56(%rsp), %r15d         # 4-byte Reload
	je	.LBB14_41
# BB#45:
	xorl	%r11d, %edx
	decl	%edx
	cmpl	$7, %edx
	jae	.LBB14_48
# BB#46:                                # %switch.hole_check
	movl	$85, %eax
	btl	%edx, %eax
	jae	.LBB14_48
# BB#47:                                # %switch.lookup
	movslq	%edx, %rax
	movl	.Lswitch.table.14(,%rax,4), %r12d
	movl	.Lswitch.table.15(,%rax,4), %r15d
	cmpl	%r15d, %r12d
	jl	.LBB14_52
	jmp	.LBB14_56
.LBB14_32:
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	movl	208(%rsp), %r8d
	movq	176(%rsp), %rbx
	jmp	.LBB14_41
.LBB14_40:
	movl	56(%rsp), %r15d         # 4-byte Reload
.LBB14_41:                              # %._crit_edge511.thread
	addl	%r11d, %edx
	cmpl	$8, %edx
	jae	.LBB14_48
# BB#42:                                # %switch.hole_check445
	movl	$167, %eax
	btl	%edx, %eax
	jb	.LBB14_43
.LBB14_48:
	movl	$5, %r12d
	testb	%sil, %sil
	je	.LBB14_50
	jmp	.LBB14_51
.LBB14_43:                              # %switch.lookup446
	movslq	%edx, %rax
	movl	.Lswitch.table.12(,%rax,4), %r12d
	movl	.Lswitch.table.13(,%rax,4), %r15d
	testb	%sil, %sil
	jne	.LBB14_51
.LBB14_50:
	movq	160(%rsp), %rax
	movq	%rax, %rsi
	movswl	(%rsi), %ecx
	movswl	(%rbx), %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	negl	%edx
	cmovll	%ecx, %edx
	movswl	2(%rsi), %ecx
	movswl	2(%rbx), %esi
	subl	%esi, %ecx
	movl	%ecx, %esi
	negl	%esi
	cmovll	%ecx, %esi
	xorl	%r11d, %r11d
	addl	%edx, %esi
	je	.LBB14_59
.LBB14_51:                              # %.preheader481
	cmpl	%r15d, %r12d
	jge	.LBB14_56
.LBB14_52:                              # %.lr.ph500
	movslq	%r12d, %rax
	leaq	search_point_hp+2(,%rax,4), %r13
	.p2align	4, 0x90
.LBB14_53:                              # =>This Inner Loop Header: Depth=1
	movswq	(%rbx), %rcx
	movswq	-2(%r13), %r10
	addq	%rcx, %r10
	movswq	2(%rbx), %rcx
	movswq	(%r13), %rbp
	addq	%rcx, %rbp
	movq	mvbits(%rip), %rcx
	movq	160(%rsp), %rdx
	movq	%rdx, %rdi
	movswq	(%rdi), %rdx
	movq	%r10, %rsi
	subq	%rdx, %rsi
	movswq	2(%rdi), %rdx
	movq	%rbp, %rdi
	subq	%rdx, %rdi
	movl	(%rcx,%rdi,4), %edx
	addl	(%rcx,%rsi,4), %edx
	movl	72(%rsp), %eax          # 4-byte Reload
	imull	%eax, %edx
	sarl	$16, %edx
	movswq	(%r9), %rsi
	movq	168(%rsp), %rdi
	movq	%rdi, %rbx
	movswq	(%rbx), %rdi
	subq	%rdi, %rsi
	movswq	2(%r9), %rdi
	movswq	2(%rbx), %rbx
	subq	%rbx, %rdi
	movl	(%rcx,%rdi,4), %ebx
	addl	(%rcx,%rsi,4), %ebx
	imull	%eax, %ebx
	sarl	$16, %ebx
	addl	%edx, %ebx
	movl	%r8d, %ecx
	subl	%ebx, %ecx
	jle	.LBB14_55
# BB#54:                                #   in Loop: Header=BB14_53 Depth=1
	addl	48(%rsp), %r10d         # 4-byte Folded Reload
	addl	40(%rsp), %ebp          # 4-byte Folded Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	%r11d, 4(%rsp)          # 4-byte Spill
	movl	%r8d, %r14d
	movl	32(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	pushq	%rbp
.Lcfi136:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi137:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	200(%rsp), %r9
	movl	%r14d, %r8d
	movl	20(%rsp), %r11d         # 4-byte Reload
	addq	$16, %rsp
.Lcfi138:
	.cfi_adjust_cfa_offset -16
	addl	%ebx, %eax
	cmpl	%r8d, %eax
	cmovll	%r12d, %r11d
	cmovlel	%eax, %r8d
.LBB14_55:                              #   in Loop: Header=BB14_53 Depth=1
	addq	$4, %r13
	incl	%r12d
	cmpl	%r12d, %r15d
	movq	176(%rsp), %rbx
	jne	.LBB14_53
.LBB14_56:                              # %._crit_edge501
	testl	%r11d, %r11d
	je	.LBB14_57
# BB#58:
	movslq	%r11d, %rcx
	movw	(%rbx), %ax
	addw	search_point_hp(,%rcx,4), %ax
	movw	%ax, (%rbx)
	movzwl	search_point_hp+2(,%rcx,4), %ecx
	addw	%cx, 2(%rbx)
	jmp	.LBB14_59
.LBB14_57:                              # %._crit_edge501..thread_crit_edge
	movw	(%rbx), %ax
.LBB14_59:                              # %.thread
	movq	96(%rsp), %rcx          # 8-byte Reload
	leal	160(,%rcx,4), %edi
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	160(,%rcx,4), %esi
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	movl	$computeBiPred2+16, %ecx
	movl	$computeBiPred1+16, %edx
	cmovneq	%rcx, %rdx
	movq	(%rdx), %r10
	movq	%r10, computeBiPred(%rip)
	movswl	%ax, %ebp
	movl	$1, %edx
	addl	48(%rsp), %ebp          # 4-byte Folded Reload
	movl	$1, %ecx
	jle	.LBB14_63
# BB#60:                                # %.thread
	cmpl	%edi, %ebp
	movl	$1, %ecx
	jge	.LBB14_63
# BB#61:
	movswl	2(%rbx), %ebp
	addl	40(%rsp), %ebp          # 4-byte Folded Reload
	movl	$1, %ecx
	jle	.LBB14_63
# BB#62:
	xorl	%ecx, %ecx
	cmpl	%esi, %ebp
	setge	%cl
.LBB14_63:
	movl	%ecx, bipred2_access_method(%rip)
	movswl	(%r9), %r11d
	movl	%r11d, %ebp
	addl	48(%rsp), %ebp          # 4-byte Folded Reload
	jle	.LBB14_67
# BB#64:
	cmpl	%edi, %ebp
	jge	.LBB14_67
# BB#65:
	movswl	2(%r9), %edi
	addl	40(%rsp), %edi          # 4-byte Folded Reload
	jle	.LBB14_67
# BB#66:
	xorl	%edx, %edx
	cmpl	%esi, %edi
	setge	%dl
.LBB14_67:
	movl	%edx, bipred1_access_method(%rip)
	movslq	start_me_refinement_qp(%rip), %r12
	testq	%r12, %r12
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	cmovel	%ecx, %r8d
	xorl	%edx, %edx
	cmpq	$4, %r12
	movq	216(%rsp), %rsi
	movl	8(%rsi), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	jg	.LBB14_68
# BB#72:                                # %.lr.ph492
	movl	$2147483647, 4(%rsp)    # 4-byte Folded Spill
                                        # imm = 0x7FFFFFFF
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	leaq	search_point_qp+2(,%r12,4), %r13
	xorl	%ecx, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jmp	.LBB14_73
	.p2align	4, 0x90
.LBB14_77:                              # %._crit_edge531
                                        #   in Loop: Header=BB14_73 Depth=1
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movw	(%rbx), %ax
	movw	(%r9), %r11w
	movq	computeBiPred(%rip), %r10
	addq	$4, %r13
	incl	%r12d
.LBB14_73:                              # =>This Inner Loop Header: Depth=1
	movswq	-2(%r13), %rsi
	movswq	%ax, %rdx
	addq	%rsi, %rdx
	movswq	2(%rbx), %rax
	movswq	(%r13), %rsi
	addq	%rax, %rsi
	movq	mvbits(%rip), %rax
	movq	160(%rsp), %rdi
	movq	%rdi, %rcx
	movswq	(%rcx), %rdi
	movq	48(%rsp), %rbp          # 8-byte Reload
	leal	(%rdx,%rbp), %ebx
	subq	%rdi, %rdx
	movswq	2(%rcx), %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	leal	(%rsi,%rbp), %ebp
	subq	%rdi, %rsi
	movl	(%rax,%rsi,4), %r15d
	addl	(%rax,%rdx,4), %r15d
	movl	8(%rsp), %edi           # 4-byte Reload
	imull	%edi, %r15d
	sarl	$16, %r15d
	movswq	%r11w, %rcx
	movq	168(%rsp), %rdx
	movq	%rdx, %rsi
	movswq	(%rsi), %rdx
	subq	%rdx, %rcx
	movswq	2(%r9), %rdx
	movswq	2(%rsi), %rsi
	subq	%rsi, %rdx
	movl	(%rax,%rdx,4), %r14d
	addl	(%rax,%rcx,4), %r14d
	imull	%edi, %r14d
	sarl	$16, %r14d
	movl	$2147483647, %ecx       # imm = 0x7FFFFFFF
	movq	88(%rsp), %rdi          # 8-byte Reload
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movl	32(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	pushq	%rbp
.Lcfi139:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi140:
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$16, %rsp
.Lcfi141:
	.cfi_adjust_cfa_offset -16
	addl	%r15d, %eax
	addl	%r14d, %eax
	movl	12(%rsp), %r8d          # 4-byte Reload
	cmpl	%r8d, %eax
	jge	.LBB14_75
# BB#74:                                #   in Loop: Header=BB14_73 Depth=1
	movl	%r8d, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %esi
	movl	%r12d, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	%eax, %r8d
	jmp	.LBB14_76
	.p2align	4, 0x90
.LBB14_75:                              #   in Loop: Header=BB14_73 Depth=1
	movl	4(%rsp), %edx           # 4-byte Reload
	cmpl	%edx, %eax
	cmovlel	%eax, %edx
	movq	72(%rsp), %rsi          # 8-byte Reload
	cmovll	%r12d, %esi
.LBB14_76:                              #   in Loop: Header=BB14_73 Depth=1
	cmpl	$4, %r12d
	movq	184(%rsp), %r9
	movq	176(%rsp), %rbx
	jne	.LBB14_77
# BB#78:                                # %._crit_edge493
	movq	64(%rsp), %r11          # 8-byte Reload
	testl	%r11d, %r11d
	setne	%dl
	testl	%esi, %esi
	je	.LBB14_69
# BB#79:                                # %._crit_edge493
	testl	%r11d, %r11d
	je	.LBB14_69
# BB#80:
	xorl	%r11d, %esi
	decl	%esi
	cmpl	$7, %esi
	jae	.LBB14_83
# BB#81:                                # %switch.hole_check458
	movl	$85, %eax
	btl	%esi, %eax
	jae	.LBB14_83
# BB#82:                                # %switch.lookup459
	movslq	%esi, %rax
	movl	.Lswitch.table.14(,%rax,4), %r14d
	movl	.Lswitch.table.15(,%rax,4), %r15d
	cmpl	%r15d, %r14d
	jl	.LBB14_87
	jmp	.LBB14_91
.LBB14_68:
	xorl	%r11d, %r11d
	xorl	%esi, %esi
.LBB14_69:                              # %._crit_edge493.thread
	leal	-1(%rsi,%r11), %eax
	cmpl	$7, %eax
	jae	.LBB14_83
# BB#70:                                # %switch.hole_check469
	movl	$83, %ecx
	btl	%eax, %ecx
	jb	.LBB14_71
.LBB14_83:
	movl	200(%rsp), %r15d
	movl	$5, %r14d
	testb	%dl, %dl
	je	.LBB14_85
	jmp	.LBB14_86
.LBB14_71:                              # %switch.lookup470
	cltq
	movl	.Lswitch.table.16(,%rax,4), %r14d
	movl	.Lswitch.table.17(,%rax,4), %r15d
	testb	%dl, %dl
	jne	.LBB14_86
.LBB14_85:
	movq	160(%rsp), %rax
	movq	%rax, %rdx
	movswl	(%rdx), %eax
	movswl	(%rbx), %ecx
	subl	%ecx, %eax
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movswl	2(%rdx), %eax
	movswl	2(%rbx), %edx
	subl	%edx, %eax
	movl	%eax, %edx
	negl	%edx
	cmovll	%eax, %edx
	xorl	%r11d, %r11d
	addl	%ecx, %edx
	je	.LBB14_93
.LBB14_86:                              # %.preheader
	cmpl	%r15d, %r14d
	jge	.LBB14_91
.LBB14_87:                              # %.lr.ph
	movslq	%r14d, %rax
	leaq	search_point_qp+2(,%rax,4), %r13
	movl	%r15d, 12(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB14_88:                              # =>This Inner Loop Header: Depth=1
	movswq	(%rbx), %rcx
	movswq	-2(%r13), %r10
	addq	%rcx, %r10
	movswq	2(%rbx), %rcx
	movswq	(%r13), %rbx
	addq	%rcx, %rbx
	movq	mvbits(%rip), %rcx
	movq	160(%rsp), %rdx
	movq	%rdx, %rdi
	movswq	(%rdi), %rdx
	movq	%r10, %rsi
	subq	%rdx, %rsi
	movswq	2(%rdi), %rdx
	movq	%rbx, %rdi
	subq	%rdx, %rdi
	movl	(%rcx,%rdi,4), %edx
	addl	(%rcx,%rsi,4), %edx
	movl	8(%rsp), %eax           # 4-byte Reload
	imull	%eax, %edx
	sarl	$16, %edx
	movswq	(%r9), %rsi
	movq	168(%rsp), %rdi
	movq	%rdi, %rbp
	movswq	(%rbp), %rdi
	subq	%rdi, %rsi
	movswq	2(%r9), %rdi
	movswq	2(%rbp), %rbp
	subq	%rbp, %rdi
	movl	(%rcx,%rdi,4), %ebp
	addl	(%rcx,%rsi,4), %ebp
	imull	%eax, %ebp
	sarl	$16, %ebp
	addl	%edx, %ebp
	movl	%r8d, %ecx
	subl	%ebp, %ecx
	jle	.LBB14_90
# BB#89:                                #   in Loop: Header=BB14_88 Depth=1
	addl	48(%rsp), %r10d         # 4-byte Folded Reload
	addl	40(%rsp), %ebx          # 4-byte Folded Reload
	movq	88(%rsp), %rdi          # 8-byte Reload
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
	movq	%r11, %r15
	movl	%r8d, %r12d
	movl	32(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	pushq	%rbx
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi143:
	.cfi_adjust_cfa_offset 8
	callq	*computeBiPred(%rip)
	movq	200(%rsp), %r9
	movl	%r12d, %r8d
	movq	%r15, %r11
	movl	28(%rsp), %r15d         # 4-byte Reload
	addq	$16, %rsp
.Lcfi144:
	.cfi_adjust_cfa_offset -16
	addl	%ebp, %eax
	cmpl	%r8d, %eax
	cmovll	%r14d, %r11d
	cmovlel	%eax, %r8d
.LBB14_90:                              #   in Loop: Header=BB14_88 Depth=1
	addq	$4, %r13
	incl	%r14d
	cmpl	%r14d, %r15d
	movq	176(%rsp), %rbx
	jne	.LBB14_88
.LBB14_91:                              # %._crit_edge
	testl	%r11d, %r11d
	je	.LBB14_93
# BB#92:
	movslq	%r11d, %rax
	movzwl	search_point_qp(,%rax,4), %ecx
	addw	%cx, (%rbx)
	movzwl	search_point_qp+2(,%rax,4), %eax
	addw	%ax, 2(%rbx)
.LBB14_93:                              # %.thread479
	movl	%r8d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	EPZSSubPelBlockSearchBiPred, .Lfunc_end14-EPZSSubPelBlockSearchBiPred
	.cfi_endproc

	.type	c_EPZSPattern,@object   # @c_EPZSPattern
	.section	.rodata,"a",@progbits
	.globl	c_EPZSPattern
	.p2align	4
c_EPZSPattern:
	.asciz	"Diamond\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"Square\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"Extended Diamond\000\000\000"
	.asciz	"Large Diamond\000\000\000\000\000\000"
	.asciz	"SBP Large Diamond\000\000"
	.asciz	"PMVFAST\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	c_EPZSPattern, 120

	.type	c_EPZSDualPattern,@object # @c_EPZSDualPattern
	.globl	c_EPZSDualPattern
	.p2align	4
c_EPZSDualPattern:
	.asciz	"Disabled\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"Diamond\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"Square\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"Extended Diamond\000\000\000"
	.asciz	"Large Diamond\000\000\000\000\000\000"
	.asciz	"SBP Large Diamond\000\000"
	.asciz	"PMVFAST\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	c_EPZSDualPattern, 140

	.type	c_EPZSFixed,@object     # @c_EPZSFixed
	.globl	c_EPZSFixed
	.p2align	4
c_EPZSFixed:
	.asciz	"Disabled\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"All P\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"All P + B\000\000\000\000\000\000\000\000\000\000"
	.size	c_EPZSFixed, 60

	.type	c_EPZSOther,@object     # @c_EPZSOther
	.globl	c_EPZSOther
	.p2align	4
c_EPZSOther:
	.asciz	"Disabled\000\000\000\000\000\000\000\000\000\000\000"
	.asciz	"Enabled\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	c_EPZSOther, 40

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"alloc_EPZScolocated: s"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"alloc_EPZSpattern: s"
	.size	.L.str.1, 21

	.type	pattern_data,@object    # @pattern_data
	.section	.rodata,"a",@progbits
	.p2align	4
pattern_data:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	4294967292              # 0xfffffffc
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	4294967292              # 0xfffffffc
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	3                       # 0x3
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	7                       # 0x7
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	4294967292              # 0xfffffffc
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	4294967292              # 0xfffffffc
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	4294967292              # 0xfffffffc
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	3                       # 0x3
	.long	4294967292              # 0xfffffffc
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.long	4294967292              # 0xfffffffc
	.long	4                       # 0x4
	.long	10                      # 0xa
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	10                      # 0xa
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	5                       # 0x5
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	8                       # 0x8
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	7                       # 0x7
	.long	4                       # 0x4
	.long	4294967292              # 0xfffffffc
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	0                       # 0x0
	.long	4294967288              # 0xfffffff8
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	4294967292              # 0xfffffffc
	.long	4                       # 0x4
	.long	7                       # 0x7
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	4294967288              # 0xfffffff8
	.long	0                       # 0x0
	.long	7                       # 0x7
	.long	8                       # 0x8
	.long	4294967292              # 0xfffffffc
	.long	0                       # 0x0
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	5                       # 0x5
	.long	4                       # 0x4
	.long	4294967292              # 0xfffffffc
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	4294967288              # 0xfffffff8
	.long	2                       # 0x2
	.long	5                       # 0x5
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	4                       # 0x4
	.long	3                       # 0x3
	.long	4294967288              # 0xfffffff8
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	4294967292              # 0xfffffffc
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	3                       # 0x3
	.zero	16
	.zero	16
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	0                       # 0x0
	.long	12                      # 0xc
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	12                      # 0xc
	.long	4                       # 0x4
	.long	4294967292              # 0xfffffffc
	.long	2                       # 0x2
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	4294967288              # 0xfffffff8
	.long	2                       # 0x2
	.long	12                      # 0xc
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	4294967288              # 0xfffffff8
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	12                      # 0xc
	.long	4294967292              # 0xfffffffc
	.long	4                       # 0x4
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	12                      # 0xc
	.long	0                       # 0x0
	.long	4294967294              # 0xfffffffe
	.long	2                       # 0x2
	.long	12                      # 0xc
	.long	4294967294              # 0xfffffffe
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	12                      # 0xc
	.size	pattern_data, 960

	.type	mv_rescale,@object      # @mv_rescale
	.local	mv_rescale
	.comm	mv_rescale,4,4
	.type	searcharray,@object     # @searcharray
	.local	searcharray
	.comm	searcharray,4,4
	.type	medthres,@object        # @medthres
	.local	medthres
	.comm	medthres,32,16
	.type	maxthres,@object        # @maxthres
	.local	maxthres
	.comm	maxthres,32,16
	.type	minthres,@object        # @minthres
	.local	minthres
	.comm	minthres,32,16
	.type	subthres,@object        # @subthres
	.local	subthres
	.comm	subthres,32,16
	.type	sdiamond,@object        # @sdiamond
	.comm	sdiamond,8,8
	.type	square,@object          # @square
	.comm	square,8,8
	.type	ediamond,@object        # @ediamond
	.comm	ediamond,8,8
	.type	ldiamond,@object        # @ldiamond
	.comm	ldiamond,8,8
	.type	sbdiamond,@object       # @sbdiamond
	.comm	sbdiamond,8,8
	.type	pmvfast,@object         # @pmvfast
	.comm	pmvfast,8,8
	.type	window_predictor,@object # @window_predictor
	.comm	window_predictor,8,8
	.type	window_predictor_extended,@object # @window_predictor_extended
	.comm	window_predictor_extended,8,8
	.type	predictor,@object       # @predictor
	.comm	predictor,8,8
	.type	EPZSDistortion,@object  # @EPZSDistortion
	.comm	EPZSDistortion,8,8
	.type	EPZSMap,@object         # @EPZSMap
	.local	EPZSMap
	.comm	EPZSMap,8,8
	.type	EPZSMotion,@object      # @EPZSMotion
	.comm	EPZSMotion,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	EPZSCo_located,@object  # @EPZSCo_located
	.comm	EPZSCo_located,8,8
	.type	searchPattern,@object   # @searchPattern
	.comm	searchPattern,8,8
	.type	searchPatternD,@object  # @searchPatternD
	.comm	searchPatternD,8,8
	.type	mv_scale,@object        # @mv_scale
	.local	mv_scale
	.comm	mv_scale,24576,16
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	EPZSBlkCount,@object    # @EPZSBlkCount
	.local	EPZSBlkCount
	.comm	EPZSBlkCount,2,2
	.type	width_pad,@object       # @width_pad
	.comm	width_pad,4,4
	.type	height_pad,@object      # @height_pad
	.comm	height_pad,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	width_pad_cr,@object    # @width_pad_cr
	.comm	width_pad_cr,4,4
	.type	height_pad_cr,@object   # @height_pad_cr
	.comm	height_pad_cr,4,4
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	" EPZS Pattern                 : %s\n"
	.size	.L.str.2, 36

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" EPZS Dual Pattern            : %s\n"
	.size	.L.str.3, 36

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" EPZS Fixed Predictors        : %s\n"
	.size	.L.str.4, 36

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" EPZS Temporal Predictors     : %s\n"
	.size	.L.str.5, 36

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" EPZS Spatial Predictors      : %s\n"
	.size	.L.str.6, 36

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	" EPZS Thresholds (16x16)      : (%d %d %d)\n"
	.size	.L.str.7, 44

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" EPZS Subpel ME               : %s\n"
	.size	.L.str.8, 36

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	" EPZS Subpel ME BiPred        : %s\n"
	.size	.L.str.9, 36

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" EPZS Pattern                      : %s\n"
	.size	.L.str.10, 41

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	" EPZS Dual Pattern                 : %s\n"
	.size	.L.str.11, 41

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	" EPZS Fixed Predictors             : %s\n"
	.size	.L.str.12, 41

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	" EPZS Temporal Predictors          : %s\n"
	.size	.L.str.13, 41

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	" EPZS Spatial Predictors           : %s\n"
	.size	.L.str.14, 41

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" EPZS Thresholds (16x16)           : (%d %d %d)\n"
	.size	.L.str.15, 49

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	" EPZS Subpel ME                    : %s\n"
	.size	.L.str.16, 41

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	" EPZS Subpel ME BiPred             : %s\n"
	.size	.L.str.17, 41

	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	search_point_hp,@object # @search_point_hp
	.section	.rodata,"a",@progbits
	.p2align	4
search_point_hp:
	.zero	4
	.short	65534                   # 0xfffe
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	65534                   # 0xfffe
	.short	2                       # 0x2
	.size	search_point_hp, 40

	.type	search_point_qp,@object # @search_point_qp
	.p2align	4
search_point_qp:
	.zero	4
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	65535                   # 0xffff
	.zero	4,255
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.size	search_point_qp, 40

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	blk_parent,@object      # @blk_parent
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
blk_parent:
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	2                       # 0x2
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	5                       # 0x5
	.size	blk_parent, 16

	.type	.Lswitch.table.1,@object # @switch.table.1
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table.1:
	.quad	square
	.quad	ediamond
	.quad	ldiamond
	.quad	sbdiamond
	.quad	pmvfast
	.size	.Lswitch.table.1, 40

	.type	.Lswitch.table.12,@object # @switch.table.12
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
.Lswitch.table.12:
	.long	5                       # 0x5
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	7                       # 0x7
	.size	.Lswitch.table.12, 32

	.type	.Lswitch.table.13,@object # @switch.table.13
	.p2align	4
.Lswitch.table.13:
	.long	5                       # 0x5
	.long	10                      # 0xa
	.long	7                       # 0x7
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	9                       # 0x9
	.size	.Lswitch.table.13, 32

	.type	.Lswitch.table.14,@object # @switch.table.14
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table.14:
	.long	6                       # 0x6
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	7                       # 0x7
	.size	.Lswitch.table.14, 28

	.type	.Lswitch.table.15,@object # @switch.table.15
	.p2align	4
.Lswitch.table.15:
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	9                       # 0x9
	.long	7                       # 0x7
	.long	8                       # 0x8
	.size	.Lswitch.table.15, 28

	.type	.Lswitch.table.16,@object # @switch.table.16
	.p2align	4
.Lswitch.table.16:
	.long	8                       # 0x8
	.long	5                       # 0x5
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	7                       # 0x7
	.size	.Lswitch.table.16, 28

	.type	.Lswitch.table.17,@object # @switch.table.17
	.p2align	4
.Lswitch.table.17:
	.long	10                      # 0xa
	.long	7                       # 0x7
	.long	10                      # 0xa
	.long	10                      # 0xa
	.long	8                       # 0x8
	.long	10                      # 0xa
	.long	9                       # 0x9
	.size	.Lswitch.table.17, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
