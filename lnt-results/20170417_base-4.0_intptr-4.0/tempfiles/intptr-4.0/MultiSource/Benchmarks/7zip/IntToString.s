	.text
	.file	"IntToString.bc"
	.globl	_Z21ConvertUInt64ToStringyPcj
	.p2align	4, 0x90
	.type	_Z21ConvertUInt64ToStringyPcj,@function
_Z21ConvertUInt64ToStringyPcj:          # @_Z21ConvertUInt64ToStringyPcj
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %rcx
	leal	-2(%rdx), %eax
	cmpl	$35, %eax
	jb	.LBB0_2
# BB#1:
	movb	$0, (%rsi)
	retq
.LBB0_2:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edx, %r9d
	xorl	%r8d, %r8d
	movl	$48, %r10d
	movabsq	$4294967296, %r11       # imm = 0x100000000
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r9
	cmpl	$10, %edx
	movl	$87, %ebx
	cmovll	%r10d, %ebx
	addl	%edx, %ebx
	movb	%bl, -80(%rsp,%rdi)
	incq	%rdi
	addq	%r11, %r8
	cmpq	%rcx, %r9
	movq	%rax, %rcx
	jbe	.LBB0_3
# BB#4:                                 # %.preheader.preheader
	sarq	$32, %r8
	movq	%r8, %rcx
	notq	%rcx
	cmpq	$-3, %rcx
	movq	$-2, %rax
	cmovgq	%rcx, %rax
	leaq	2(%rax,%r8), %rax
	cmpq	$32, %rax
	jb	.LBB0_16
# BB#5:                                 # %min.iters.checked
	movq	%rax, %r9
	andq	$-32, %r9
	je	.LBB0_16
# BB#6:                                 # %vector.memcheck
	cmpq	$-3, %rcx
	movq	$-2, %rdx
	cmovleq	%rdx, %rcx
	leaq	-80(%rsp,%r8), %rbx
	cmpq	%rsi, %rbx
	jbe	.LBB0_8
# BB#7:                                 # %vector.memcheck
	leaq	(%rcx,%r8), %rbx
	leaq	2(%rsi,%rbx), %rbx
	subq	%rcx, %rdx
	leaq	-80(%rsp,%rdx), %rcx
	cmpq	%rbx, %rcx
	jb	.LBB0_16
.LBB0_8:                                # %vector.body.preheader
	leaq	-32(%r9), %rdx
	movq	%rdx, %rcx
	shrq	$5, %rcx
	btl	$5, %edx
	jb	.LBB0_9
# BB#10:                                # %vector.body.prol
	movdqu	-112(%rsp,%r8), %xmm0
	movdqu	-96(%rsp,%r8), %xmm1
	pxor	%xmm2, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm2, %xmm1    # xmm1 = xmm1[8],xmm2[8],xmm1[9],xmm2[9],xmm1[10],xmm2[10],xmm1[11],xmm2[11],xmm1[12],xmm2[12],xmm1[13],xmm2[13],xmm1[14],xmm2[14],xmm1[15],xmm2[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqa	%xmm0, %xmm3
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm2, %xmm0    # xmm0 = xmm0[8],xmm2[8],xmm0[9],xmm2[9],xmm0[10],xmm2[10],xmm0[11],xmm2[11],xmm0[12],xmm2[12],xmm0[13],xmm2[13],xmm0[14],xmm2[14],xmm0[15],xmm2[15]
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	pshuflw	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm0, %xmm0       # xmm0 = xmm0[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm0
	movdqu	%xmm1, (%rsi)
	movdqu	%xmm0, 16(%rsi)
	movl	$32, %r10d
	testq	%rcx, %rcx
	jne	.LBB0_12
	jmp	.LBB0_14
.LBB0_9:
	xorl	%r10d, %r10d
	testq	%rcx, %rcx
	je	.LBB0_14
.LBB0_12:                               # %vector.body.preheader.new
	movslq	%edi, %rbx
	movq	%rbx, %rcx
	notq	%rcx
	cmpq	$-3, %rcx
	movq	$-2, %rdx
	cmovgq	%rcx, %rdx
	leaq	2(%rdx,%rbx), %rdx
	andq	$-32, %rdx
	subq	%r10, %rdx
	leaq	48(%rsi,%r10), %rdi
	movq	$-16, %rcx
	subq	%r10, %rcx
	leaq	-80(%rsp,%rcx), %rcx
	addq	%rbx, %rcx
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm1
	movdqu	(%rcx), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -48(%rdi)
	movdqu	%xmm1, -32(%rdi)
	movdqu	-48(%rcx), %xmm1
	movdqu	-32(%rcx), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$64, %rdi
	addq	$-64, %rcx
	addq	$-64, %rdx
	jne	.LBB0_13
.LBB0_14:                               # %middle.block
	addq	%r9, %rsi
	cmpq	%r9, %rax
	je	.LBB0_18
# BB#15:
	subq	%r9, %r8
.LBB0_16:                               # %.preheader.preheader33
	incq	%r8
	.p2align	4, 0x90
.LBB0_17:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-82(%rsp,%r8), %eax
	movb	%al, (%rsi)
	incq	%rsi
	decq	%r8
	cmpq	$1, %r8
	jg	.LBB0_17
.LBB0_18:                               # %.loopexit
	movb	$0, (%rsi)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_Z21ConvertUInt64ToStringyPcj, .Lfunc_end0-_Z21ConvertUInt64ToStringyPcj
	.cfi_endproc

	.globl	_Z21ConvertUInt64ToStringyPw
	.p2align	4, 0x90
	.type	_Z21ConvertUInt64ToStringyPw,@function
_Z21ConvertUInt64ToStringyPw:           # @_Z21ConvertUInt64ToStringyPw
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rcx
	xorl	%r11d, %r11d
	movabsq	$-3689348814741910323, %r9 # imm = 0xCCCCCCCCCCCCCCCD
	movabsq	$4294967296, %r10       # imm = 0x100000000
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	mulq	%r9
	shrq	$3, %rdx
	leal	(%rdx,%rdx), %eax
	leal	(%rax,%rax,4), %eax
	movl	%ecx, %edi
	subl	%eax, %edi
	orl	$48, %edi
	movl	%edi, -128(%rsp,%r8,4)
	incq	%r8
	addq	%r10, %r11
	cmpq	$9, %rcx
	movq	%rdx, %rcx
	ja	.LBB1_1
# BB#2:                                 # %.preheader.preheader
	sarq	$32, %r11
	movq	%r11, %rax
	notq	%rax
	cmpq	$-3, %rax
	movq	$-2, %rcx
	cmovgq	%rax, %rcx
	leaq	2(%rcx,%r11), %r10
	cmpq	$8, %r10
	jb	.LBB1_14
# BB#3:                                 # %min.iters.checked
	movq	%r10, %r9
	andq	$-8, %r9
	je	.LBB1_14
# BB#4:                                 # %vector.memcheck
	cmpq	$-3, %rax
	movq	$-2, %rcx
	cmovleq	%rcx, %rax
	leaq	-128(%rsp,%r11,4), %rdx
	cmpq	%rsi, %rdx
	jbe	.LBB1_6
# BB#5:                                 # %vector.memcheck
	leaq	(%rax,%r11), %rdx
	leaq	8(%rsi,%rdx,4), %rdx
	subq	%rax, %rcx
	leaq	-128(%rsp,%rcx,4), %rax
	cmpq	%rdx, %rax
	jb	.LBB1_14
.LBB1_6:                                # %vector.body.preheader
	leaq	-8(%r9), %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	btl	$3, %eax
	jb	.LBB1_7
# BB#8:                                 # %vector.body.prol
	movdqu	-144(%rsp,%r11,4), %xmm0
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	-160(%rsp,%r11,4), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm1, 16(%rsi)
	movl	$8, %ebx
	testq	%rcx, %rcx
	jne	.LBB1_10
	jmp	.LBB1_12
.LBB1_7:
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	je	.LBB1_12
.LBB1_10:                               # %vector.body.preheader.new
	movslq	%r8d, %rdi
	movq	%rdi, %rcx
	notq	%rcx
	cmpq	$-3, %rcx
	movq	$-2, %rdx
	cmovgq	%rcx, %rdx
	leaq	2(%rdx,%rdi), %rdx
	andq	$-8, %rdx
	subq	%rbx, %rdx
	leaq	48(%rsi,%rbx,4), %rcx
	movq	$-4, %rax
	subq	%rbx, %rax
	leaq	-128(%rsp,%rax,4), %rax
	leaq	(%rax,%rdi,4), %rax
	.p2align	4, 0x90
.LBB1_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	%xmm1, -48(%rcx)
	movdqu	%xmm0, -32(%rcx)
	movdqu	-48(%rax), %xmm0
	movdqu	-32(%rax), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	%xmm1, -16(%rcx)
	movdqu	%xmm0, (%rcx)
	addq	$64, %rcx
	addq	$-64, %rax
	addq	$-16, %rdx
	jne	.LBB1_11
.LBB1_12:                               # %middle.block
	leaq	(%rsi,%r9,4), %rsi
	cmpq	%r9, %r10
	je	.LBB1_16
# BB#13:
	subq	%r9, %r11
.LBB1_14:                               # %.preheader.preheader26
	incq	%r11
	.p2align	4, 0x90
.LBB1_15:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	-136(%rsp,%r11,4), %eax
	movl	%eax, (%rsi)
	addq	$4, %rsi
	decq	%r11
	cmpq	$1, %r11
	jg	.LBB1_15
.LBB1_16:                               # %.loopexit
	movl	$0, (%rsi)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_Z21ConvertUInt64ToStringyPw, .Lfunc_end1-_Z21ConvertUInt64ToStringyPw
	.cfi_endproc

	.globl	_Z21ConvertUInt32ToStringjPc
	.p2align	4, 0x90
	.type	_Z21ConvertUInt32ToStringjPc,@function
_Z21ConvertUInt32ToStringjPc:           # @_Z21ConvertUInt32ToStringjPc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movl	%edi, %ecx
	xorl	%r8d, %r8d
	movabsq	$-3689348814741910323, %r9 # imm = 0xCCCCCCCCCCCCCCCD
	movl	$48, %r10d
	movabsq	$4294967296, %r11       # imm = 0x100000000
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	mulq	%r9
	shrq	$3, %rdx
	leal	(%rdx,%rdx), %eax
	leal	(%rax,%rax,4), %eax
	movl	%ecx, %ebx
	subl	%eax, %ebx
	cmpl	$10, %ebx
	movl	$87, %eax
	cmovbl	%r10d, %eax
	addl	%ebx, %eax
	movb	%al, -80(%rsp,%rdi)
	incq	%rdi
	addq	%r11, %r8
	cmpq	$9, %rcx
	movq	%rdx, %rcx
	ja	.LBB2_1
# BB#2:                                 # %.preheader.preheader.i
	sarq	$32, %r8
	movq	%r8, %rcx
	notq	%rcx
	cmpq	$-3, %rcx
	movq	$-2, %rax
	cmovgq	%rcx, %rax
	leaq	2(%rax,%r8), %rax
	cmpq	$32, %rax
	jb	.LBB2_14
# BB#3:                                 # %min.iters.checked
	movq	%rax, %r9
	andq	$-32, %r9
	je	.LBB2_14
# BB#4:                                 # %vector.memcheck
	cmpq	$-3, %rcx
	movq	$-2, %rdx
	cmovleq	%rdx, %rcx
	leaq	-80(%rsp,%r8), %rbx
	cmpq	%rsi, %rbx
	jbe	.LBB2_6
# BB#5:                                 # %vector.memcheck
	leaq	(%rcx,%r8), %rbx
	leaq	2(%rsi,%rbx), %rbx
	subq	%rcx, %rdx
	leaq	-80(%rsp,%rdx), %rcx
	cmpq	%rbx, %rcx
	jb	.LBB2_14
.LBB2_6:                                # %vector.body.preheader
	leaq	-32(%r9), %rdx
	movq	%rdx, %rcx
	shrq	$5, %rcx
	btl	$5, %edx
	jb	.LBB2_7
# BB#8:                                 # %vector.body.prol
	movdqu	-112(%rsp,%r8), %xmm0
	movdqu	-96(%rsp,%r8), %xmm1
	pxor	%xmm2, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm2, %xmm1    # xmm1 = xmm1[8],xmm2[8],xmm1[9],xmm2[9],xmm1[10],xmm2[10],xmm1[11],xmm2[11],xmm1[12],xmm2[12],xmm1[13],xmm2[13],xmm1[14],xmm2[14],xmm1[15],xmm2[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqa	%xmm0, %xmm3
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm2, %xmm0    # xmm0 = xmm0[8],xmm2[8],xmm0[9],xmm2[9],xmm0[10],xmm2[10],xmm0[11],xmm2[11],xmm0[12],xmm2[12],xmm0[13],xmm2[13],xmm0[14],xmm2[14],xmm0[15],xmm2[15]
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	pshuflw	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm0, %xmm0       # xmm0 = xmm0[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm0
	movdqu	%xmm1, (%rsi)
	movdqu	%xmm0, 16(%rsi)
	movl	$32, %r10d
	testq	%rcx, %rcx
	jne	.LBB2_10
	jmp	.LBB2_12
.LBB2_7:
	xorl	%r10d, %r10d
	testq	%rcx, %rcx
	je	.LBB2_12
.LBB2_10:                               # %vector.body.preheader.new
	movslq	%edi, %rbx
	movq	%rbx, %rcx
	notq	%rcx
	cmpq	$-3, %rcx
	movq	$-2, %rdx
	cmovgq	%rcx, %rdx
	leaq	2(%rdx,%rbx), %rdx
	andq	$-32, %rdx
	subq	%r10, %rdx
	leaq	48(%rsi,%r10), %rdi
	movq	$-16, %rcx
	subq	%r10, %rcx
	leaq	-80(%rsp,%rcx), %rcx
	addq	%rbx, %rcx
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm1
	movdqu	(%rcx), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -48(%rdi)
	movdqu	%xmm1, -32(%rdi)
	movdqu	-48(%rcx), %xmm1
	movdqu	-32(%rcx), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$64, %rdi
	addq	$-64, %rcx
	addq	$-64, %rdx
	jne	.LBB2_11
.LBB2_12:                               # %middle.block
	addq	%r9, %rsi
	cmpq	%r9, %rax
	je	.LBB2_16
# BB#13:
	subq	%r9, %r8
.LBB2_14:                               # %.preheader.i.preheader
	incq	%r8
	.p2align	4, 0x90
.LBB2_15:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-82(%rsp,%r8), %eax
	movb	%al, (%rsi)
	incq	%rsi
	decq	%r8
	cmpq	$1, %r8
	jg	.LBB2_15
.LBB2_16:                               # %_Z21ConvertUInt64ToStringyPcj.exit
	movb	$0, (%rsi)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_Z21ConvertUInt32ToStringjPc, .Lfunc_end2-_Z21ConvertUInt32ToStringjPc
	.cfi_endproc

	.globl	_Z21ConvertUInt32ToStringjPw
	.p2align	4, 0x90
	.type	_Z21ConvertUInt32ToStringjPw,@function
_Z21ConvertUInt32ToStringjPw:           # @_Z21ConvertUInt32ToStringjPw
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movl	%edi, %ecx
	xorl	%r11d, %r11d
	movabsq	$-3689348814741910323, %r9 # imm = 0xCCCCCCCCCCCCCCCD
	movabsq	$4294967296, %r10       # imm = 0x100000000
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	mulq	%r9
	shrq	$3, %rdx
	leal	(%rdx,%rdx), %eax
	leal	(%rax,%rax,4), %eax
	movl	%ecx, %edi
	subl	%eax, %edi
	orl	$48, %edi
	movl	%edi, -128(%rsp,%r8,4)
	incq	%r8
	addq	%r10, %r11
	cmpq	$9, %rcx
	movq	%rdx, %rcx
	ja	.LBB3_1
# BB#2:                                 # %.preheader.preheader.i
	sarq	$32, %r11
	movq	%r11, %rax
	notq	%rax
	cmpq	$-3, %rax
	movq	$-2, %rcx
	cmovgq	%rax, %rcx
	leaq	2(%rcx,%r11), %r10
	cmpq	$8, %r10
	jb	.LBB3_14
# BB#3:                                 # %min.iters.checked
	movq	%r10, %r9
	andq	$-8, %r9
	je	.LBB3_14
# BB#4:                                 # %vector.memcheck
	cmpq	$-3, %rax
	movq	$-2, %rcx
	cmovleq	%rcx, %rax
	leaq	-128(%rsp,%r11,4), %rdx
	cmpq	%rsi, %rdx
	jbe	.LBB3_6
# BB#5:                                 # %vector.memcheck
	leaq	(%rax,%r11), %rdx
	leaq	8(%rsi,%rdx,4), %rdx
	subq	%rax, %rcx
	leaq	-128(%rsp,%rcx,4), %rax
	cmpq	%rdx, %rax
	jb	.LBB3_14
.LBB3_6:                                # %vector.body.preheader
	leaq	-8(%r9), %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	btl	$3, %eax
	jb	.LBB3_7
# BB#8:                                 # %vector.body.prol
	movdqu	-144(%rsp,%r11,4), %xmm0
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	-160(%rsp,%r11,4), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm1, 16(%rsi)
	movl	$8, %ebx
	testq	%rcx, %rcx
	jne	.LBB3_10
	jmp	.LBB3_12
.LBB3_7:
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	je	.LBB3_12
.LBB3_10:                               # %vector.body.preheader.new
	movslq	%r8d, %rdi
	movq	%rdi, %rcx
	notq	%rcx
	cmpq	$-3, %rcx
	movq	$-2, %rdx
	cmovgq	%rcx, %rdx
	leaq	2(%rdx,%rdi), %rdx
	andq	$-8, %rdx
	subq	%rbx, %rdx
	leaq	48(%rsi,%rbx,4), %rcx
	movq	$-4, %rax
	subq	%rbx, %rax
	leaq	-128(%rsp,%rax,4), %rax
	leaq	(%rax,%rdi,4), %rax
	.p2align	4, 0x90
.LBB3_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	%xmm1, -48(%rcx)
	movdqu	%xmm0, -32(%rcx)
	movdqu	-48(%rax), %xmm0
	movdqu	-32(%rax), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	%xmm1, -16(%rcx)
	movdqu	%xmm0, (%rcx)
	addq	$64, %rcx
	addq	$-64, %rax
	addq	$-16, %rdx
	jne	.LBB3_11
.LBB3_12:                               # %middle.block
	leaq	(%rsi,%r9,4), %rsi
	cmpq	%r9, %r10
	je	.LBB3_16
# BB#13:
	subq	%r9, %r11
.LBB3_14:                               # %.preheader.i.preheader
	incq	%r11
	.p2align	4, 0x90
.LBB3_15:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-136(%rsp,%r11,4), %eax
	movl	%eax, (%rsi)
	addq	$4, %rsi
	decq	%r11
	cmpq	$1, %r11
	jg	.LBB3_15
.LBB3_16:                               # %_Z21ConvertUInt64ToStringyPw.exit
	movl	$0, (%rsi)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_Z21ConvertUInt32ToStringjPw, .Lfunc_end3-_Z21ConvertUInt32ToStringjPw
	.cfi_endproc

	.globl	_Z20ConvertInt64ToStringxPc
	.p2align	4, 0x90
	.type	_Z20ConvertInt64ToStringxPc,@function
_Z20ConvertInt64ToStringxPc:            # @_Z20ConvertInt64ToStringxPc
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	testq	%rcx, %rcx
	jns	.LBB4_2
# BB#1:
	movb	$45, (%rsi)
	incq	%rsi
	negq	%rcx
.LBB4_2:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	xorl	%r8d, %r8d
	movabsq	$-3689348814741910323, %r9 # imm = 0xCCCCCCCCCCCCCCCD
	movl	$48, %r10d
	movabsq	$4294967296, %r11       # imm = 0x100000000
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	mulq	%r9
	shrq	$3, %rdx
	leal	(%rdx,%rdx), %eax
	leal	(%rax,%rax,4), %eax
	movl	%ecx, %ebx
	subl	%eax, %ebx
	cmpl	$10, %ebx
	movl	$87, %eax
	cmovbl	%r10d, %eax
	addl	%ebx, %eax
	movb	%al, -80(%rsp,%rdi)
	incq	%rdi
	addq	%r11, %r8
	cmpq	$9, %rcx
	movq	%rdx, %rcx
	ja	.LBB4_3
# BB#4:                                 # %.preheader.preheader.i
	sarq	$32, %r8
	movq	%r8, %rcx
	notq	%rcx
	cmpq	$-3, %rcx
	movq	$-2, %rax
	cmovgq	%rcx, %rax
	leaq	2(%rax,%r8), %rax
	cmpq	$32, %rax
	jb	.LBB4_16
# BB#5:                                 # %min.iters.checked
	movq	%rax, %r9
	andq	$-32, %r9
	je	.LBB4_16
# BB#6:                                 # %vector.memcheck
	cmpq	$-3, %rcx
	movq	$-2, %rdx
	cmovleq	%rdx, %rcx
	leaq	-80(%rsp,%r8), %rbx
	cmpq	%rbx, %rsi
	jae	.LBB4_8
# BB#7:                                 # %vector.memcheck
	leaq	(%rcx,%r8), %rbx
	leaq	2(%rsi,%rbx), %rbx
	subq	%rcx, %rdx
	leaq	-80(%rsp,%rdx), %rcx
	cmpq	%rbx, %rcx
	jb	.LBB4_16
.LBB4_8:                                # %vector.body.preheader
	leaq	-32(%r9), %rdx
	movq	%rdx, %rcx
	shrq	$5, %rcx
	btl	$5, %edx
	jb	.LBB4_9
# BB#10:                                # %vector.body.prol
	movdqu	-112(%rsp,%r8), %xmm0
	movdqu	-96(%rsp,%r8), %xmm1
	pxor	%xmm2, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm2, %xmm1    # xmm1 = xmm1[8],xmm2[8],xmm1[9],xmm2[9],xmm1[10],xmm2[10],xmm1[11],xmm2[11],xmm1[12],xmm2[12],xmm1[13],xmm2[13],xmm1[14],xmm2[14],xmm1[15],xmm2[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqa	%xmm0, %xmm3
	punpcklbw	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1],xmm3[2],xmm2[2],xmm3[3],xmm2[3],xmm3[4],xmm2[4],xmm3[5],xmm2[5],xmm3[6],xmm2[6],xmm3[7],xmm2[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm2, %xmm0    # xmm0 = xmm0[8],xmm2[8],xmm0[9],xmm2[9],xmm0[10],xmm2[10],xmm0[11],xmm2[11],xmm0[12],xmm2[12],xmm0[13],xmm2[13],xmm0[14],xmm2[14],xmm0[15],xmm2[15]
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	pshuflw	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm0, %xmm0       # xmm0 = xmm0[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm0
	movdqu	%xmm1, (%rsi)
	movdqu	%xmm0, 16(%rsi)
	movl	$32, %r10d
	testq	%rcx, %rcx
	jne	.LBB4_12
	jmp	.LBB4_14
.LBB4_9:
	xorl	%r10d, %r10d
	testq	%rcx, %rcx
	je	.LBB4_14
.LBB4_12:                               # %vector.body.preheader.new
	movslq	%edi, %rbx
	movq	%rbx, %rcx
	notq	%rcx
	cmpq	$-3, %rcx
	movq	$-2, %rdx
	cmovgq	%rcx, %rdx
	leaq	2(%rdx,%rbx), %rdx
	andq	$-32, %rdx
	subq	%r10, %rdx
	leaq	48(%rsi,%r10), %rdi
	movq	$-16, %rcx
	subq	%r10, %rcx
	leaq	-80(%rsp,%rcx), %rcx
	addq	%rbx, %rcx
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rcx), %xmm1
	movdqu	(%rcx), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -48(%rdi)
	movdqu	%xmm1, -32(%rdi)
	movdqu	-48(%rcx), %xmm1
	movdqu	-32(%rcx), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$64, %rdi
	addq	$-64, %rcx
	addq	$-64, %rdx
	jne	.LBB4_13
.LBB4_14:                               # %middle.block
	addq	%r9, %rsi
	cmpq	%r9, %rax
	je	.LBB4_18
# BB#15:
	subq	%r9, %r8
.LBB4_16:                               # %.preheader.i.preheader
	incq	%r8
	.p2align	4, 0x90
.LBB4_17:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-82(%rsp,%r8), %eax
	movb	%al, (%rsi)
	incq	%rsi
	decq	%r8
	cmpq	$1, %r8
	jg	.LBB4_17
.LBB4_18:                               # %_Z21ConvertUInt64ToStringyPcj.exit
	movb	$0, (%rsi)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_Z20ConvertInt64ToStringxPc, .Lfunc_end4-_Z20ConvertInt64ToStringxPc
	.cfi_endproc

	.globl	_Z20ConvertInt64ToStringxPw
	.p2align	4, 0x90
	.type	_Z20ConvertInt64ToStringxPw,@function
_Z20ConvertInt64ToStringxPw:            # @_Z20ConvertInt64ToStringxPw
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	testq	%rcx, %rcx
	jns	.LBB5_2
# BB#1:
	movl	$45, (%rsi)
	addq	$4, %rsi
	negq	%rcx
.LBB5_2:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	xorl	%r11d, %r11d
	movabsq	$-3689348814741910323, %r9 # imm = 0xCCCCCCCCCCCCCCCD
	movabsq	$4294967296, %r10       # imm = 0x100000000
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	mulq	%r9
	shrq	$3, %rdx
	leal	(%rdx,%rdx), %eax
	leal	(%rax,%rax,4), %eax
	movl	%ecx, %edi
	subl	%eax, %edi
	orl	$48, %edi
	movl	%edi, -128(%rsp,%r8,4)
	incq	%r8
	addq	%r10, %r11
	cmpq	$9, %rcx
	movq	%rdx, %rcx
	ja	.LBB5_3
# BB#4:                                 # %.preheader.preheader.i
	sarq	$32, %r11
	movq	%r11, %rax
	notq	%rax
	cmpq	$-3, %rax
	movq	$-2, %rcx
	cmovgq	%rax, %rcx
	leaq	2(%rcx,%r11), %r10
	cmpq	$8, %r10
	jb	.LBB5_16
# BB#5:                                 # %min.iters.checked
	movq	%r10, %r9
	andq	$-8, %r9
	je	.LBB5_16
# BB#6:                                 # %vector.memcheck
	cmpq	$-3, %rax
	movq	$-2, %rcx
	cmovleq	%rcx, %rax
	leaq	-128(%rsp,%r11,4), %rdx
	cmpq	%rdx, %rsi
	jae	.LBB5_8
# BB#7:                                 # %vector.memcheck
	leaq	(%rax,%r11), %rdx
	leaq	8(%rsi,%rdx,4), %rdx
	subq	%rax, %rcx
	leaq	-128(%rsp,%rcx,4), %rax
	cmpq	%rdx, %rax
	jb	.LBB5_16
.LBB5_8:                                # %vector.body.preheader
	leaq	-8(%r9), %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	btl	$3, %eax
	jb	.LBB5_9
# BB#10:                                # %vector.body.prol
	movdqu	-144(%rsp,%r11,4), %xmm0
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	-160(%rsp,%r11,4), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm1, 16(%rsi)
	movl	$8, %ebx
	testq	%rcx, %rcx
	jne	.LBB5_12
	jmp	.LBB5_14
.LBB5_9:
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	je	.LBB5_14
.LBB5_12:                               # %vector.body.preheader.new
	movslq	%r8d, %rdi
	movq	%rdi, %rcx
	notq	%rcx
	cmpq	$-3, %rcx
	movq	$-2, %rdx
	cmovgq	%rcx, %rdx
	leaq	2(%rdx,%rdi), %rdx
	andq	$-8, %rdx
	subq	%rbx, %rdx
	leaq	48(%rsi,%rbx,4), %rcx
	movq	$-4, %rax
	subq	%rbx, %rax
	leaq	-128(%rsp,%rax,4), %rax
	leaq	(%rax,%rdi,4), %rax
	.p2align	4, 0x90
.LBB5_13:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	%xmm1, -48(%rcx)
	movdqu	%xmm0, -32(%rcx)
	movdqu	-48(%rax), %xmm0
	movdqu	-32(%rax), %xmm1
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	pshufd	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0]
	movdqu	%xmm1, -16(%rcx)
	movdqu	%xmm0, (%rcx)
	addq	$64, %rcx
	addq	$-64, %rax
	addq	$-16, %rdx
	jne	.LBB5_13
.LBB5_14:                               # %middle.block
	leaq	(%rsi,%r9,4), %rsi
	cmpq	%r9, %r10
	je	.LBB5_18
# BB#15:
	subq	%r9, %r11
.LBB5_16:                               # %.preheader.i.preheader
	incq	%r11
	.p2align	4, 0x90
.LBB5_17:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-136(%rsp,%r11,4), %eax
	movl	%eax, (%rsi)
	addq	$4, %rsi
	decq	%r11
	cmpq	$1, %r11
	jg	.LBB5_17
.LBB5_18:                               # %_Z21ConvertUInt64ToStringyPw.exit
	movl	$0, (%rsi)
	popq	%rbx
	retq
.Lfunc_end5:
	.size	_Z20ConvertInt64ToStringxPw, .Lfunc_end5-_Z20ConvertInt64ToStringxPw
	.cfi_endproc

	.globl	_Z27ConvertUInt32ToHexWithZerosjPc
	.p2align	4, 0x90
	.type	_Z27ConvertUInt32ToHexWithZerosjPc,@function
_Z27ConvertUInt32ToHexWithZerosjPc:     # @_Z27ConvertUInt32ToHexWithZerosjPc
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	andl	$15, %eax
	movl	%edi, %ecx
	shrl	$4, %ecx
	leal	48(%rax), %r8d
	leal	55(%rax), %edx
	cmpl	$10, %eax
	cmovbl	%r8d, %edx
	movb	%dl, 7(%rsi)
	andl	$15, %ecx
	movl	%edi, %eax
	shrl	$8, %eax
	leal	48(%rcx), %r8d
	leal	55(%rcx), %edx
	cmpl	$10, %ecx
	cmovbl	%r8d, %edx
	movb	%dl, 6(%rsi)
	andl	$15, %eax
	movl	%edi, %ecx
	shrl	$12, %ecx
	leal	48(%rax), %r8d
	leal	55(%rax), %edx
	cmpl	$10, %eax
	cmovbl	%r8d, %edx
	movb	%dl, 5(%rsi)
	andl	$15, %ecx
	movl	%edi, %eax
	shrl	$16, %eax
	leal	48(%rcx), %r8d
	leal	55(%rcx), %edx
	cmpl	$10, %ecx
	cmovbl	%r8d, %edx
	movb	%dl, 4(%rsi)
	andl	$15, %eax
	movl	%edi, %ecx
	shrl	$20, %ecx
	leal	48(%rax), %r8d
	leal	55(%rax), %edx
	cmpl	$10, %eax
	cmovbl	%r8d, %edx
	movb	%dl, 3(%rsi)
	andl	$15, %ecx
	movl	%edi, %eax
	shrl	$24, %eax
	leal	48(%rcx), %r8d
	leal	55(%rcx), %edx
	cmpl	$10, %ecx
	cmovbl	%r8d, %edx
	movb	%dl, 2(%rsi)
	andl	$15, %eax
	movl	%edi, %ecx
	shrl	$28, %ecx
	leal	48(%rax), %r8d
	leal	55(%rax), %edx
	cmpl	$10, %eax
	cmovbl	%r8d, %edx
	movb	%dl, 1(%rsi)
	leal	48(%rcx), %eax
	addl	$55, %ecx
	cmpl	$-1610612736, %edi      # imm = 0xA0000000
	cmovbl	%eax, %ecx
	movb	%cl, (%rsi)
	movb	$0, 8(%rsi)
	retq
.Lfunc_end6:
	.size	_Z27ConvertUInt32ToHexWithZerosjPc, .Lfunc_end6-_Z27ConvertUInt32ToHexWithZerosjPc
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
