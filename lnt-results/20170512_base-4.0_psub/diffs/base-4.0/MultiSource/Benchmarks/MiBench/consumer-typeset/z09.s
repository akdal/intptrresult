	.text
	.file	"z09.bc"
	.globl	SearchEnv
	.p2align	4, 0x90
	.type	SearchEnv,@function
SearchEnv:                              # @SearchEnv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	jmp	.LBB0_1
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_7
# BB#8:                                 # %.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpb	$2, %al
	je	.LBB0_10
# BB#9:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_10:                               # %.loopexit26
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpq	%r14, 80(%rbx)
	je	.LBB0_6
# BB#11:                                #   in Loop: Header=BB0_1 Depth=1
	cmpq	%rbx, (%rbx)
	jne	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_13:                               #   in Loop: Header=BB0_1 Depth=1
	movq	(%r15), %rax
	cmpq	8(%r15), %rax
	cmovneq	%r15, %rbx
	movq	(%rbx), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %r15
	cmpb	$0, 32(%r15)
	leaq	16(%r15), %rax
	je	.LBB0_14
.LBB0_1:                                # %.loopexit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #     Child Loop BB0_14 Depth 2
	testq	%r15, %r15
	je	.LBB0_3
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpb	$82, 32(%r15)
	je	.LBB0_4
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movq	8(%r15), %rbx
	cmpq	%r15, %rbx
	jne	.LBB0_7
# BB#5:
	xorl	%ebx, %ebx
.LBB0_6:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	SearchEnv, .Lfunc_end0-SearchEnv
	.cfi_endproc

	.globl	SetEnv
	.p2align	4, 0x90
	.type	SetEnv,@function
SetEnv:                                 # @SetEnv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB1_2
# BB#1:
	cmpb	$2, 32(%r15)
	je	.LBB1_3
.LBB1_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_3:
	testq	%r14, %r14
	je	.LBB1_6
# BB#4:
	cmpb	$82, 32(%r14)
	je	.LBB1_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_6:
	movzbl	zz_lengths+82(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_7
# BB#8:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_9
.LBB1_7:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_9:
	movb	$82, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_10
# BB#11:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_12
.LBB1_10:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_12:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_15
# BB#13:
	testq	%rax, %rax
	je	.LBB1_15
# BB#14:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_15:
	testq	%r15, %r15
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB1_18
# BB#16:
	testq	%rax, %rax
	je	.LBB1_18
# BB#17:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_18:
	testq	%r14, %r14
	je	.LBB1_27
# BB#19:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_20
# BB#21:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_22
.LBB1_20:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_22:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB1_25
# BB#23:
	testq	%rax, %rax
	je	.LBB1_25
# BB#24:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_25:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_27
# BB#26:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_27:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	SetEnv, .Lfunc_end1-SetEnv
	.cfi_endproc

	.globl	AttachEnv
	.p2align	4, 0x90
	.type	AttachEnv,@function
AttachEnv:                              # @AttachEnv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB2_2
# BB#1:
	cmpb	$82, 32(%r14)
	je	.LBB2_3
.LBB2_2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_3:
	movb	32(%rbx), %al
	cmpb	$2, %al
	je	.LBB2_6
# BB#4:
	cmpb	$81, %al
	je	.LBB2_6
# BB#5:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_6:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_7
# BB#8:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_9
.LBB2_7:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_9:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_12
# BB#10:
	testq	%rax, %rax
	je	.LBB2_12
# BB#11:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_12:
	testq	%r14, %r14
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB2_15
# BB#13:
	testq	%rax, %rax
	je	.LBB2_15
# BB#14:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	AttachEnv, .Lfunc_end2-AttachEnv
	.cfi_endproc

	.globl	GetEnv
	.p2align	4, 0x90
	.type	GetEnv,@function
GetEnv:                                 # @GetEnv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpb	$2, 32(%r14)
	je	.LBB3_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_2:
	movq	(%r14), %rbx
	cmpq	%r14, %rbx
	jne	.LBB3_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r14), %rbx
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_4
# BB#5:
	cmpb	$82, %al
	je	.LBB3_7
# BB#6:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_7:                                # %.loopexit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	GetEnv, .Lfunc_end3-GetEnv
	.cfi_endproc

	.globl	DetachEnv
	.p2align	4, 0x90
	.type	DetachEnv,@function
DetachEnv:                              # @DetachEnv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpb	$2, 32(%rbx)
	je	.LBB4_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_2:
	movq	(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB4_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%rbx), %rax
.LBB4_4:
	movq	%rax, %rdx
	addq	$16, %rdx
	.p2align	4, 0x90
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rbx
	movzbl	32(%rbx), %ecx
	leaq	16(%rbx), %rdx
	testb	%cl, %cl
	je	.LBB4_5
# BB#6:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rdx
	cmpq	%rax, %rdx
	je	.LBB4_8
# BB#7:
	movq	%rdx, zz_res(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rax), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB4_8:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rdx
	cmpq	%rax, %rdx
	je	.LBB4_10
# BB#9:
	movq	%rdx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rax, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rax
.LBB4_10:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %edx
	leaq	zz_lengths(%rdx), %rsi
                                        # kill: %DL<def> %DL<kill> %RDX<kill>
	addb	$-11, %dl
	leaq	33(%rax), %rdi
	cmpb	$2, %dl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %edx
	movl	%edx, zz_size(%rip)
	movq	zz_free(,%rdx,8), %rsi
	movq	%rsi, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rdx,8)
	cmpb	$82, %cl
	je	.LBB4_12
# BB#11:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.14, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_12:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	DetachEnv, .Lfunc_end4-DetachEnv
	.cfi_endproc

	.globl	ClosureExpand
	.p2align	4, 0x90
	.type	ClosureExpand,@function
ClosureExpand:                          # @ClosureExpand
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 96
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r15
	movl	%edx, %ebx
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	cmpb	$2, 32(%r13)
	je	.LBB5_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_2:
	leaq	80(%r13), %r14
	movq	80(%r13), %rax
	cmpw	$0, 120(%rax)
	je	.LBB5_4
# BB#3:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.16, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r14), %rax
.LBB5_4:
	movzwl	41(%rax), %eax
	testb	$2, %al
	je	.LBB5_6
# BB#5:
	movq	%r13, %rdi
	callq	CrossAddTag
.LBB5_6:
	testl	%ebx, %ebx
	je	.LBB5_34
# BB#7:
	movq	(%r14), %rax
	movzwl	41(%rax), %eax
	testb	$2, %al
	je	.LBB5_34
# BB#8:
	movq	no_fpos(%rip), %rsi
	movq	%r13, %rdi
	callq	CopyObject
	movq	%rax, %rbx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rsi
	callq	AttachEnv
	movq	80(%r13), %rdi
	movl	$131, %edx
	movq	%rbx, %rsi
	callq	CrossMake
	movq	%rax, %rbx
	movzbl	zz_lengths+131(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB5_9
# BB#10:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB5_11
.LBB5_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB5_11:
	movb	$-125, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rbx, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_12
# BB#13:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_14
.LBB5_12:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_14:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_17
# BB#15:
	testq	%rax, %rax
	je	.LBB5_17
# BB#16:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_17:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB5_20
# BB#18:
	testq	%rax, %rax
	je	.LBB5_20
# BB#19:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_20:
	cmpq	$0, (%r15)
	jne	.LBB5_25
# BB#21:
	movzbl	zz_lengths+148(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_22
# BB#23:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_24
.LBB5_22:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_24:
	movb	$-108, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, (%r15)
.LBB5_25:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_26
# BB#27:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_28
.LBB5_26:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB5_28:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	(%r15), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB5_31
# BB#29:
	testq	%rcx, %rcx
	je	.LBB5_31
# BB#30:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_31:
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB5_34
# BB#32:
	testq	%rax, %rax
	je	.LBB5_34
# BB#33:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB5_34:
	leaq	32(%r13), %rdx
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	$0, (%r12)
	movq	(%r14), %rax
	movb	32(%rax), %cl
	addb	$112, %cl
	cmpb	$2, %cl
	ja	.LBB5_88
# BB#35:
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	48(%rax), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	SearchEnv
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_86
# BB#36:
	cmpb	$2, 32(%rbx)
	je	.LBB5_38
# BB#37:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_38:
	movq	(%rbx), %r15
	cmpq	%rbx, %r15
	jne	.LBB5_40
# BB#39:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%rbx), %r15
.LBB5_40:
	addq	$16, %r15
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB5_41
	.p2align	4, 0x90
.LBB5_101:                              #   in Loop: Header=BB5_41 Depth=1
	addq	$16, %r15
.LBB5_41:                               # =>This Inner Loop Header: Depth=1
	movq	(%r15), %r15
	movzbl	32(%r15), %eax
	testb	%al, %al
	je	.LBB5_101
# BB#42:
	cmpb	$82, %al
	je	.LBB5_44
# BB#43:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB5_44:                               # %GetEnv.exit.preheader
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB5_46
	jmp	.LBB5_88
	.p2align	4, 0x90
.LBB5_50:                               # %GetEnv.exit.backedge
                                        #   in Loop: Header=BB5_46 Depth=1
	movq	8(%rax), %rax
	cmpq	%rbx, %rax
	je	.LBB5_88
.LBB5_46:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_47 Depth 2
	movq	%rax, %r12
	.p2align	4, 0x90
.LBB5_47:                               #   Parent Loop BB5_46 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r12), %r12
	movzbl	32(%r12), %ecx
	testb	%cl, %cl
	je	.LBB5_47
# BB#48:                                #   in Loop: Header=BB5_46 Depth=1
	cmpb	$10, %cl
	jne	.LBB5_50
# BB#49:                                #   in Loop: Header=BB5_46 Depth=1
	movq	80(%r12), %rcx
	cmpq	(%r14), %rcx
	jne	.LBB5_50
# BB#51:
	movq	8(%r12), %rax
	cmpq	%r12, %rax
	jne	.LBB5_53
# BB#52:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%r12), %rax
.LBB5_53:
	movq	%rax, %rcx
	addq	$16, %rcx
	.p2align	4, 0x90
.LBB5_54:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbp
	leaq	16(%rbp), %rcx
	cmpb	$0, 32(%rbp)
	je	.LBB5_54
# BB#55:
	movq	80(%r12), %rcx
	movq	48(%rcx), %rdx
	cmpw	$0, 41(%rdx)
	js	.LBB5_57
# BB#56:
	testb	$16, 126(%rcx)
	jne	.LBB5_57
# BB#58:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB5_60
# BB#59:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB5_60:
	leaq	32(%rbp), %rdx
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB5_62
# BB#61:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rsi
	movq	%rax, 8(%rsi)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB5_62:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rsi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rdi
	cmpb	$2, %cl
	cmovbq	%rdi, %rsi
	movzbl	(%rsi), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rsi
	movq	%rsi, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movl	$11, %edi
	movl	$.L.str.18, %esi
	callq	MakeWord
	movq	%rax, %rsi
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB5_63
# BB#64:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB5_65
.LBB5_86:
	movq	(%r14), %rdi
	cmpq	$0, 56(%rdi)
	movq	8(%rsp), %rdx           # 8-byte Reload
	jne	.LBB5_88
# BB#87:
	callq	SymName
	movq	%rax, %rbx
	movq	(%r14), %rax
	movq	48(%rax), %rdi
	callq	SymName
	movq	%rax, (%rsp)
	movl	$9, %edi
	movl	$2, %esi
	movl	$.L.str.19, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%rbx, %r9
	callq	Error
	movl	$9, %edi
	movl	$1, %esi
	movl	$.L.str.20, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	8(%rsp), %r8            # 8-byte Reload
	callq	Error
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB5_88
.LBB5_57:
	movq	no_fpos(%rip), %rsi
	movq	%rbp, %rdi
	callq	CopyObject
	movq	%rax, %rbp
.LBB5_71:
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB5_72
# BB#73:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbp, %rbp
	sete	%r12b
	je	.LBB5_76
# BB#74:
	testq	%rax, %rax
	je	.LBB5_76
# BB#75:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbp), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbp)
	movq	%rbp, 24(%rcx)
	jmp	.LBB5_76
.LBB5_72:                               # %.thread176
	movq	$0, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	$0, zz_hold(%rip)
	testq	%rbp, %rbp
	sete	%r12b
.LBB5_76:
	movq	(%r14), %rax
	cmpb	$-110, 32(%rax)
	jne	.LBB5_81
# BB#77:
	movq	48(%rax), %rcx
	movzwl	41(%rcx), %ecx
	testb	$1, %ch
	jne	.LBB5_78
.LBB5_81:
	cmpb	$-111, 32(%rax)
	jne	.LBB5_85
# BB#82:
	movzbl	43(%rax), %eax
	shll	$16, %eax
	testl	$4194304, %eax          # imm = 0x400000
	jne	.LBB5_83
.LBB5_85:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	AttachEnv
	movq	%r13, %rdi
	movq	%r15, %rsi
	jmp	.LBB5_84
.LBB5_78:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	SetEnv
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	%r13, %rdi
	callq	DisposeObject
	jmp	.LBB5_79
.LBB5_83:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	AttachEnv
	xorl	%esi, %esi
	movq	%r13, %rdi
.LBB5_84:
	callq	SetEnv
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
.LBB5_79:
	movq	8(%rsp), %rdx           # 8-byte Reload
	testb	%r12b, %r12b
	je	.LBB5_80
.LBB5_88:                               # %.thread
	movq	(%r14), %rax
	movq	56(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_89
# BB#90:
	movq	%rdx, %rsi
	callq	CopyObject
	jmp	.LBB5_91
.LBB5_89:
	movl	$11, %edi
	movl	$.L.str.18, %esi
	callq	MakeWord
.LBB5_91:
	movq	%rax, %rbp
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r13, zz_hold(%rip)
	movq	24(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB5_92
# BB#93:
	movq	16(%r13), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r13), %rcx
	movq	%rax, 24(%rcx)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%rax, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB5_96
# BB#94:
	testq	%rax, %rax
	je	.LBB5_96
# BB#95:
	movq	16(%rax), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rbp), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 16(%rbp)
	movq	%rbp, 24(%rcx)
	jmp	.LBB5_96
.LBB5_92:                               # %.thread178
	movq	$0, xx_tmp(%rip)
	movq	%rbp, zz_res(%rip)
	movq	$0, zz_hold(%rip)
.LBB5_96:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	AttachEnv
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	SetEnv
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB5_99
.LBB5_98:
	cmpb	$82, 32(%rax)
	je	.LBB5_100
.LBB5_99:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.21, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB5_100:
	movq	%rbp, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_80:                               # %thread-pre-split
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB5_98
	jmp	.LBB5_99
.LBB5_63:
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rax, zz_hold(%rip)
.LBB5_65:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB5_68
# BB#66:
	testq	%rax, %rax
	je	.LBB5_68
# BB#67:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB5_68:
	movq	%rax, zz_res(%rip)
	movq	%rsi, zz_hold(%rip)
	testq	%rsi, %rsi
	je	.LBB5_71
# BB#69:
	testq	%rax, %rax
	je	.LBB5_71
# BB#70:
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
	jmp	.LBB5_71
.Lfunc_end5:
	.size	ClosureExpand, .Lfunc_end5-ClosureExpand
	.cfi_endproc

	.globl	ParameterCheck
	.p2align	4, 0x90
	.type	ParameterCheck,@function
ParameterCheck:                         # @ParameterCheck
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -40
.Lcfi43:
	.cfi_offset %r12, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
.LBB6_1:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
                                        #     Child Loop BB6_13 Depth 2
                                        #       Child Loop BB6_14 Depth 3
	cmpb	$2, 32(%r14)
	je	.LBB6_3
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.22, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_3:                                #   in Loop: Header=BB6_1 Depth=1
	movq	80(%r14), %rax
	movq	48(%rax), %rsi
	movq	%r12, %rdi
	callq	SearchEnv
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB6_29
# BB#4:                                 #   in Loop: Header=BB6_1 Depth=1
	cmpb	$2, 32(%r15)
	je	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_6:                                #   in Loop: Header=BB6_1 Depth=1
	movq	(%r15), %r12
	cmpq	%r15, %r12
	jne	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r15), %r12
	.p2align	4, 0x90
.LBB6_8:                                #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %r12
	movq	(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB6_8
# BB#9:                                 #   in Loop: Header=BB6_1 Depth=1
	cmpb	$82, %al
	je	.LBB6_11
# BB#10:                                #   in Loop: Header=BB6_1 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB6_11:                               # %GetEnv.exit.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	movq	8(%r15), %rax
	cmpq	%r15, %rax
	jne	.LBB6_13
	jmp	.LBB6_18
	.p2align	4, 0x90
.LBB6_17:                               # %GetEnv.exit.backedge
                                        #   in Loop: Header=BB6_13 Depth=2
	movq	8(%rax), %rax
	cmpq	%r15, %rax
	je	.LBB6_18
.LBB6_13:                               # %.preheader
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_14 Depth 3
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB6_14:                               #   Parent Loop BB6_1 Depth=1
                                        #     Parent Loop BB6_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB6_14
# BB#15:                                #   in Loop: Header=BB6_13 Depth=2
	cmpb	$10, %cl
	jne	.LBB6_17
# BB#16:                                #   in Loop: Header=BB6_13 Depth=2
	movq	80(%rbx), %rcx
	cmpq	80(%r14), %rcx
	jne	.LBB6_17
	jmp	.LBB6_22
	.p2align	4, 0x90
.LBB6_18:                               # %GetEnv.exit._crit_edge
                                        #   in Loop: Header=BB6_1 Depth=1
	movq	80(%r14), %rax
	movq	56(%rax), %r14
	testq	%r14, %r14
	je	.LBB6_29
# BB#19:                                #   in Loop: Header=BB6_1 Depth=1
	movb	32(%r14), %al
	cmpb	$2, %al
	jne	.LBB6_20
# BB#28:                                #   in Loop: Header=BB6_1 Depth=1
	movq	80(%r14), %rax
	movb	32(%rax), %al
	addb	$112, %al
	cmpb	$3, %al
	jb	.LBB6_1
	jmp	.LBB6_29
.LBB6_22:
	movq	8(%rbx), %rdi
	cmpq	%rbx, %rdi
	jne	.LBB6_24
# BB#23:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	8(%rbx), %rdi
	.p2align	4, 0x90
.LBB6_24:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rdi
	movq	(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB6_24
# BB#25:
	addb	$-11, %al
	cmpb	$2, %al
	jae	.LBB6_29
# BB#26:
	movq	no_fpos(%rip), %rsi
.LBB6_27:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	CopyObject              # TAILCALL
.LBB6_20:
	addb	$-11, %al
	cmpb	$1, %al
	ja	.LBB6_29
# BB#21:
	leaq	32(%r14), %rsi
	movq	%r14, %rdi
	jmp	.LBB6_27
.LBB6_29:                               # %.loopexit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	ParameterCheck, .Lfunc_end6-ParameterCheck
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SearchEnv: env!"
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SearchEnv: type(y) != CLOSURE!"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SearchEnv: LastDown(y) == y!"
	.size	.L.str.3, 29

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SetEnv: x==nilobj or not CLOSURE!"
	.size	.L.str.4, 34

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SetEnv: y!=nilobj && type(y) != ENV!"
	.size	.L.str.5, 37

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"AttachEnv: type(env) != ENV!"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"AttachEnv: type(x)!"
	.size	.L.str.8, 20

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"GetEnv: type(x) != CLOSURE!"
	.size	.L.str.9, 28

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"GetEnv: LastDown(x) == x!"
	.size	.L.str.10, 26

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"GetEnv: type(env) != ENV!"
	.size	.L.str.11, 26

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"DetachEnv: type(x) != CLOSURE!"
	.size	.L.str.12, 31

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"DetachEnv: LastDown(x) == x!"
	.size	.L.str.13, 29

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"DetachEnv: type(env) != ENV!"
	.size	.L.str.14, 29

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"ClosureExpand given non-CLOSURE!"
	.size	.L.str.15, 33

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"ClosureExpand given predefined!"
	.size	.L.str.16, 32

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"ExpandCLosure: Down(par)!"
	.size	.L.str.17, 26

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"??"
	.size	.L.str.18, 3

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"no value for parameter %s of symbol %s:"
	.size	.L.str.19, 40

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"symbol with import list misused"
	.size	.L.str.20, 32

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"ClosureExpand: *res_env!"
	.size	.L.str.21, 25

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"ParameterCheck given non-CLOSURE!"
	.size	.L.str.22, 34

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"ParameterCheck: Down(par)!"
	.size	.L.str.23, 27


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
