	.text
	.file	"EndianPortable.bc"
	.globl	Swap16NtoB
	.p2align	4, 0x90
	.type	Swap16NtoB,@function
Swap16NtoB:                             # @Swap16NtoB
	.cfi_startproc
# BB#0:
	rolw	$8, %di
	movl	%edi, %eax
	retq
.Lfunc_end0:
	.size	Swap16NtoB, .Lfunc_end0-Swap16NtoB
	.cfi_endproc

	.globl	Swap16BtoN
	.p2align	4, 0x90
	.type	Swap16BtoN,@function
Swap16BtoN:                             # @Swap16BtoN
	.cfi_startproc
# BB#0:
	rolw	$8, %di
	movl	%edi, %eax
	retq
.Lfunc_end1:
	.size	Swap16BtoN, .Lfunc_end1-Swap16BtoN
	.cfi_endproc

	.globl	Swap32NtoB
	.p2align	4, 0x90
	.type	Swap32NtoB,@function
Swap32NtoB:                             # @Swap32NtoB
	.cfi_startproc
# BB#0:
	bswapl	%edi
	movl	%edi, %eax
	retq
.Lfunc_end2:
	.size	Swap32NtoB, .Lfunc_end2-Swap32NtoB
	.cfi_endproc

	.globl	Swap32BtoN
	.p2align	4, 0x90
	.type	Swap32BtoN,@function
Swap32BtoN:                             # @Swap32BtoN
	.cfi_startproc
# BB#0:
	bswapl	%edi
	movl	%edi, %eax
	retq
.Lfunc_end3:
	.size	Swap32BtoN, .Lfunc_end3-Swap32BtoN
	.cfi_endproc

	.globl	Swap64BtoN
	.p2align	4, 0x90
	.type	Swap64BtoN,@function
Swap64BtoN:                             # @Swap64BtoN
	.cfi_startproc
# BB#0:
	bswapq	%rdi
	movq	%rdi, %rax
	retq
.Lfunc_end4:
	.size	Swap64BtoN, .Lfunc_end4-Swap64BtoN
	.cfi_endproc

	.globl	Swap64NtoB
	.p2align	4, 0x90
	.type	Swap64NtoB,@function
Swap64NtoB:                             # @Swap64NtoB
	.cfi_startproc
# BB#0:
	bswapq	%rdi
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	Swap64NtoB, .Lfunc_end5-Swap64NtoB
	.cfi_endproc

	.globl	SwapFloat32BtoN
	.p2align	4, 0x90
	.type	SwapFloat32BtoN,@function
SwapFloat32BtoN:                        # @SwapFloat32BtoN
	.cfi_startproc
# BB#0:
	movd	%xmm0, %eax
	bswapl	%eax
	movd	%eax, %xmm0
	retq
.Lfunc_end6:
	.size	SwapFloat32BtoN, .Lfunc_end6-SwapFloat32BtoN
	.cfi_endproc

	.globl	SwapFloat32NtoB
	.p2align	4, 0x90
	.type	SwapFloat32NtoB,@function
SwapFloat32NtoB:                        # @SwapFloat32NtoB
	.cfi_startproc
# BB#0:
	movd	%xmm0, %eax
	bswapl	%eax
	movd	%eax, %xmm0
	retq
.Lfunc_end7:
	.size	SwapFloat32NtoB, .Lfunc_end7-SwapFloat32NtoB
	.cfi_endproc

	.globl	SwapFloat64BtoN
	.p2align	4, 0x90
	.type	SwapFloat64BtoN,@function
SwapFloat64BtoN:                        # @SwapFloat64BtoN
	.cfi_startproc
# BB#0:
	movd	%xmm0, %rax
	bswapq	%rax
	movd	%rax, %xmm0
	retq
.Lfunc_end8:
	.size	SwapFloat64BtoN, .Lfunc_end8-SwapFloat64BtoN
	.cfi_endproc

	.globl	SwapFloat64NtoB
	.p2align	4, 0x90
	.type	SwapFloat64NtoB,@function
SwapFloat64NtoB:                        # @SwapFloat64NtoB
	.cfi_startproc
# BB#0:
	movd	%xmm0, %rax
	bswapq	%rax
	movd	%rax, %xmm0
	retq
.Lfunc_end9:
	.size	SwapFloat64NtoB, .Lfunc_end9-SwapFloat64NtoB
	.cfi_endproc

	.globl	Swap16
	.p2align	4, 0x90
	.type	Swap16,@function
Swap16:                                 # @Swap16
	.cfi_startproc
# BB#0:
	rolw	$8, (%rdi)
	retq
.Lfunc_end10:
	.size	Swap16, .Lfunc_end10-Swap16
	.cfi_endproc

	.globl	Swap24
	.p2align	4, 0x90
	.type	Swap24,@function
Swap24:                                 # @Swap24
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	movb	2(%rdi), %cl
	movb	%cl, (%rdi)
	movb	%al, 2(%rdi)
	retq
.Lfunc_end11:
	.size	Swap24, .Lfunc_end11-Swap24
	.cfi_endproc

	.globl	Swap32
	.p2align	4, 0x90
	.type	Swap32,@function
Swap32:                                 # @Swap32
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	bswapl	%eax
	movl	%eax, (%rdi)
	retq
.Lfunc_end12:
	.size	Swap32, .Lfunc_end12-Swap32
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
