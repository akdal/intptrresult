	.text
	.file	"util.bc"
	.globl	strcopyof
	.p2align	4, 0x90
	.type	strcopyof,@function
strcopyof:                              # @strcopyof
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	strcopyof, .Lfunc_end0-strcopyof
	.cfi_endproc

	.globl	nextarg
	.p2align	4, 0x90
	.type	nextarg,@function
nextarg:                                # @nextarg
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movl	$16, %edi
	callq	malloc
	movl	%ebx, (%rax)
	movq	%r14, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	nextarg, .Lfunc_end1-nextarg
	.cfi_endproc

	.globl	arg_str
	.p2align	4, 0x90
	.type	arg_str,@function
arg_str:                                # @arg_str
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	arglist2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	callq	free
.LBB2_2:
	movq	arglist1(%rip), %rax
	movq	%rax, arglist2(%rip)
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	make_arg_str
	movq	%rax, arglist1(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	arg_str, .Lfunc_end2-arg_str
	.cfi_endproc

	.p2align	4, 0x90
	.type	make_arg_str,@function
make_arg_str:                           # @make_arg_str
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB3_7
# BB#1:
	movq	8(%rbp), %rdi
	leal	11(%rbx), %esi
	movl	%r15d, %edx
	callq	make_arg_str
	movq	%rax, %r14
	movl	(%rbp), %edx
	cmpl	$1, %ebx
	je	.LBB3_4
# BB#2:
	testl	%r15d, %r15d
	je	.LBB3_4
# BB#3:
	movq	%rsp, %rdi
	movl	$.L.str.41, %esi
	jmp	.LBB3_5
.LBB3_7:
	movslq	%ebx, %rdi
	callq	malloc
	movq	%rax, %r14
	movb	$0, (%r14)
	jmp	.LBB3_6
.LBB3_4:
	movq	%rsp, %rdi
	movl	$.L.str.42, %esi
.LBB3_5:
	xorl	%eax, %eax
	callq	sprintf
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	strcat
.LBB3_6:
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	make_arg_str, .Lfunc_end3-make_arg_str
	.cfi_endproc

	.globl	free_args
	.p2align	4, 0x90
	.type	free_args,@function
free_args:                              # @free_args
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	je	.LBB4_2
	.p2align	4, 0x90
.LBB4_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB4_1
.LBB4_2:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end4:
	.size	free_args, .Lfunc_end4-free_args
	.cfi_endproc

	.globl	check_params
	.p2align	4, 0x90
	.type	check_params,@function
check_params:                           # @check_params
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r12, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB5_10
# BB#1:                                 # %.lr.ph56.preheader
	movq	%r15, %r12
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph56
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_4 Depth 2
	movq	8(%r12), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_4
	jmp	.LBB5_7
	.p2align	4, 0x90
.LBB5_6:                                #   in Loop: Header=BB5_4 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB5_7
.LBB5_4:                                # %.lr.ph53
                                        #   Parent Loop BB5_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	cmpl	(%r12), %eax
	jne	.LBB5_6
# BB#5:                                 #   in Loop: Header=BB5_4 Depth=2
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	yyerror
	jmp	.LBB5_6
	.p2align	4, 0x90
.LBB5_7:                                # %._crit_edge54
                                        #   in Loop: Header=BB5_2 Depth=1
	cmpl	$0, (%r12)
	jns	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_2 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	warn
.LBB5_9:                                #   in Loop: Header=BB5_2 Depth=1
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.LBB5_2
.LBB5_10:                               # %.loopexit
	testq	%r14, %r14
	je	.LBB5_24
# BB#11:                                # %.lr.ph48.preheader
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph48
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_13 Depth 2
	movq	8(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB5_17
	.p2align	4, 0x90
.LBB5_13:                               #   Parent Loop BB5_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	cmpl	(%r12), %eax
	jne	.LBB5_15
# BB#14:                                #   in Loop: Header=BB5_13 Depth=2
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	yyerror
.LBB5_15:                               #   in Loop: Header=BB5_13 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_13
# BB#16:                                # %._crit_edge46
                                        #   in Loop: Header=BB5_12 Depth=1
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.LBB5_12
.LBB5_17:                               # %._crit_edge49
	testq	%r15, %r15
	sete	%al
	je	.LBB5_24
# BB#18:                                # %._crit_edge49
	testb	%al, %al
	jne	.LBB5_24
	.p2align	4, 0x90
.LBB5_19:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_20 Depth 2
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB5_20:                               #   Parent Loop BB5_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %eax
	cmpl	(%r15), %eax
	jne	.LBB5_22
# BB#21:                                #   in Loop: Header=BB5_20 Depth=2
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	yyerror
.LBB5_22:                               #   in Loop: Header=BB5_20 Depth=2
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_20
# BB#23:                                # %._crit_edge
                                        #   in Loop: Header=BB5_19 Depth=1
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.LBB5_19
.LBB5_24:                               # %.thread
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	check_params, .Lfunc_end5-check_params
	.cfi_endproc

	.globl	yyerror
	.p2align	4, 0x90
	.type	yyerror,@function
yyerror:                                # @yyerror
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 16
	subq	$208, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 224
.Lcfi37:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testb	%al, %al
	je	.LBB6_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB6_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	cmpb	$0, is_std_in(%rip)
	je	.LBB6_4
# BB#3:
	movl	$.L.str.36, %edx
	jmp	.LBB6_5
.LBB6_4:
	movq	g_argv(%rip), %rax
	movslq	optind(%rip), %rcx
	movq	-8(%rax,%rcx,8), %rdx
.LBB6_5:
	movq	stderr(%rip), %rdi
	movl	line_no(%rip), %ecx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	%rsp, %rdx
	movq	%rbx, %rsi
	callq	vfprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$1, had_error(%rip)
	addq	$208, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	yyerror, .Lfunc_end6-yyerror
	.cfi_endproc

	.globl	warn
	.p2align	4, 0x90
	.type	warn,@function
warn:                                   # @warn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
	subq	$208, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 224
.Lcfi40:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testb	%al, %al
	je	.LBB7_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB7_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	cmpb	$0, std_only(%rip)
	jne	.LBB7_3
# BB#7:
	cmpb	$0, warn_not_std(%rip)
	jne	.LBB7_8
.LBB7_12:
	addq	$208, %rsp
	popq	%rbx
	retq
.LBB7_3:
	cmpb	$0, is_std_in(%rip)
	jne	.LBB7_4
# BB#5:
	movq	g_argv(%rip), %rax
	movslq	optind(%rip), %rcx
	movq	-8(%rax,%rcx,8), %rdx
	jmp	.LBB7_6
.LBB7_8:
	cmpb	$0, is_std_in(%rip)
	jne	.LBB7_9
# BB#10:
	movq	g_argv(%rip), %rax
	movslq	optind(%rip), %rcx
	movq	-8(%rax,%rcx,8), %rdx
	jmp	.LBB7_11
.LBB7_4:
	movl	$.L.str.36, %edx
.LBB7_6:
	movq	stderr(%rip), %rdi
	movl	line_no(%rip), %ecx
	movl	$.L.str.37, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	%rsp, %rdx
	movq	%rbx, %rsi
	callq	vfprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$1, had_error(%rip)
	jmp	.LBB7_12
.LBB7_9:
	movl	$.L.str.36, %edx
.LBB7_11:
	movq	stderr(%rip), %rdi
	movl	line_no(%rip), %ecx
	movl	$.L.str.38, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movq	%rsp, %rdx
	movq	%rbx, %rsi
	callq	vfprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	jmp	.LBB7_12
.Lfunc_end7:
	.size	warn, .Lfunc_end7-warn
	.cfi_endproc

	.globl	init_gen
	.p2align	4, 0x90
	.type	init_gen,@function
init_gen:                               # @init_gen
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 16
	movl	$0, break_label(%rip)
	movl	$0, continue_label(%rip)
	movl	$1, next_label(%rip)
	movl	$2, out_count(%rip)
	cmpb	$0, compile_only(%rip)
	je	.LBB8_2
# BB#1:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB8_3
.LBB8_2:
	callq	init_load
.LBB8_3:
	movl	$0, had_error(%rip)
	movb	$0, did_gen(%rip)
	popq	%rax
	retq
.Lfunc_end8:
	.size	init_gen, .Lfunc_end8-init_gen
	.cfi_endproc

	.globl	generate
	.p2align	4, 0x90
	.type	generate,@function
generate:                               # @generate
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$1, did_gen(%rip)
	cmpb	$0, compile_only(%rip)
	je	.LBB9_4
# BB#1:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
	movq	%rbx, %rdi
	callq	strlen
	addl	out_count(%rip), %eax
	movl	%eax, out_count(%rip)
	cmpl	$61, %eax
	jl	.LBB9_3
# BB#2:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$0, out_count(%rip)
.LBB9_3:
	popq	%rbx
	retq
.LBB9_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	load_code               # TAILCALL
.Lfunc_end9:
	.size	generate, .Lfunc_end9-generate
	.cfi_endproc

	.globl	run_code
	.p2align	4, 0x90
	.type	run_code,@function
run_code:                               # @run_code
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 16
	cmpl	$0, had_error(%rip)
	movb	did_gen(%rip), %al
	jne	.LBB10_6
# BB#1:
	testb	%al, %al
	je	.LBB10_6
# BB#2:
	cmpb	$0, compile_only(%rip)
	je	.LBB10_4
# BB#3:
	movl	$.Lstr, %edi
	callq	puts
	movl	$0, out_count(%rip)
	jmp	.LBB10_5
.LBB10_4:
	callq	execute
.LBB10_5:                               # %thread-pre-split
	movb	did_gen(%rip), %al
.LBB10_6:
	testb	%al, %al
	je	.LBB10_11
# BB#7:
	movl	$0, break_label(%rip)
	movl	$0, continue_label(%rip)
	movl	$1, next_label(%rip)
	movl	$2, out_count(%rip)
	cmpb	$0, compile_only(%rip)
	je	.LBB10_9
# BB#8:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB10_10
.LBB10_11:
	movl	$0, had_error(%rip)
	popq	%rax
	retq
.LBB10_9:
	callq	init_load
.LBB10_10:                              # %init_gen.exit
	movl	$0, had_error(%rip)
	movb	$0, did_gen(%rip)
	popq	%rax
	retq
.Lfunc_end10:
	.size	run_code, .Lfunc_end10-run_code
	.cfi_endproc

	.globl	out_char
	.p2align	4, 0x90
	.type	out_char,@function
out_char:                               # @out_char
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
.Lcfi46:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	cmpb	$10, %bl
	jne	.LBB11_1
# BB#4:
	movl	$0, out_col(%rip)
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	popq	%rbx
	jmp	_IO_putc                # TAILCALL
.LBB11_1:
	movl	out_col(%rip), %eax
	incl	%eax
	movl	%eax, out_col(%rip)
	cmpl	$70, %eax
	jne	.LBB11_3
# BB#2:
	movq	stdout(%rip), %rsi
	movl	$92, %edi
	callq	_IO_putc
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$1, out_col(%rip)
.LBB11_3:
	movq	stdout(%rip), %rsi
	movl	%ebx, %edi
	popq	%rbx
	jmp	_IO_putc                # TAILCALL
.Lfunc_end11:
	.size	out_char, .Lfunc_end11-out_char
	.cfi_endproc

	.globl	find_id
	.p2align	4, 0x90
	.type	find_id,@function
find_id:                                # @find_id
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -24
.Lcfi51:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB12_2
	jmp	.LBB12_4
	.p2align	4, 0x90
.LBB12_3:                               # %tailrecurse.backedge
                                        #   in Loop: Header=BB12_2 Depth=1
	leaq	24(%rbx), %rcx
	addq	$32, %rbx
	testl	%eax, %eax
	cmovsq	%rcx, %rbx
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB12_4
.LBB12_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB12_3
	jmp	.LBB12_5
.LBB12_4:
	xorl	%ebx, %ebx
.LBB12_5:                               # %._crit_edge
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end12:
	.size	find_id, .Lfunc_end12-find_id
	.cfi_endproc

	.globl	insert_id_rec
	.p2align	4, 0x90
	.type	insert_id_rec,@function
insert_id_rec:                          # @insert_id_rec
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 48
.Lcfi57:
	.cfi_offset %rbx, -40
.Lcfi58:
	.cfi_offset %r12, -32
.Lcfi59:
	.cfi_offset %r14, -24
.Lcfi60:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB13_1
# BB#2:
	movabsq	$281470681743360, %r12  # imm = 0xFFFF00000000
	movq	(%r15), %rdi
	movq	(%rbx), %rsi
	callq	strcmp
	testl	%eax, %eax
	js	.LBB13_3
# BB#10:
	addq	$32, %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	insert_id_rec
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB13_18
# BB#11:
	movq	(%r14), %rdx
	movzwl	20(%rdx), %ecx
	incl	%ecx
	movw	%cx, 20(%rdx)
	movzwl	%cx, %ecx
	cmpl	$2, %ecx
	jne	.LBB13_18
# BB#12:
	movq	32(%rdx), %rsi
	movq	24(%rsi), %rcx
	cmpw	$0, 20(%rsi)
	js	.LBB13_14
# BB#13:
	movq	%rcx, 32(%rdx)
	movq	%rdx, 24(%rsi)
	jmp	.LBB13_7
.LBB13_1:
	movq	%r15, (%r14)
	movw	$0, 20(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%r15)
	movl	$1, %eax
	jmp	.LBB13_18
.LBB13_3:
	addq	$24, %rbx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	insert_id_rec
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.LBB13_18
# BB#4:
	movq	(%r14), %rdx
	movzwl	20(%rdx), %ecx
	decl	%ecx
	movw	%cx, 20(%rdx)
	movzwl	%cx, %ecx
	cmpl	$65534, %ecx            # imm = 0xFFFE
	jne	.LBB13_18
# BB#5:
	movq	24(%rdx), %rsi
	movq	32(%rsi), %rcx
	cmpw	$0, 20(%rsi)
	jle	.LBB13_6
# BB#8:
	movq	%rcx, (%r14)
	movq	24(%rcx), %rcx
	movq	%rcx, 32(%rsi)
	movq	(%r14), %rcx
	movq	32(%rcx), %rcx
	movq	%rcx, 24(%rdx)
	movq	(%r14), %rcx
	movq	%rsi, 24(%rcx)
	movq	(%r14), %rcx
	movq	%rdx, 32(%rcx)
	movq	(%r14), %rdi
	movswl	20(%rdi), %ecx
	incl	%ecx
	cmpl	$2, %ecx
	ja	.LBB13_16
# BB#9:                                 # %switch.lookup
	shlb	$4, %cl
	movl	$1, %r8d
	shrq	%cl, %r8
	movabsq	$281474976710655, %rbx  # imm = 0xFFFFFFFFFFFF
	andq	%rbx, %r12
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %r12
	movw	%r8w, 20(%rdx)
	movw	%r12w, 20(%rsi)
	jmp	.LBB13_16
.LBB13_14:
	movq	%rcx, (%r14)
	movq	32(%rcx), %rcx
	movq	%rcx, 24(%rsi)
	movq	(%r14), %rcx
	movq	24(%rcx), %rcx
	movq	%rcx, 32(%rdx)
	movq	(%r14), %rcx
	movq	%rdx, 24(%rcx)
	movq	(%r14), %rcx
	movq	%rsi, 32(%rcx)
	movq	(%r14), %rdi
	movswl	20(%rdi), %ecx
	incl	%ecx
	cmpl	$2, %ecx
	ja	.LBB13_16
# BB#15:                                # %switch.lookup95
	movabsq	$281474976710655, %rbx  # imm = 0xFFFFFFFFFFFF
	andq	%rbx, %r12
	shlb	$4, %cl
	shrq	%cl, %r12
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrq	%cl, %rbx
	movw	%r12w, 20(%rdx)
	movw	%bx, 20(%rsi)
.LBB13_16:
	movq	%rdi, %rsi
	jmp	.LBB13_17
.LBB13_6:
	movq	%rcx, 24(%rdx)
	movq	%rdx, 32(%rsi)
.LBB13_7:
	movq	%rsi, (%r14)
	movw	$0, 20(%rdx)
.LBB13_17:
	movw	$0, 20(%rsi)
.LBB13_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	insert_id_rec, .Lfunc_end13-insert_id_rec
	.cfi_endproc

	.globl	init_tree
	.p2align	4, 0x90
	.type	init_tree,@function
init_tree:                              # @init_tree
	.cfi_startproc
# BB#0:
	movq	$0, name_tree(%rip)
	movl	$1, next_array(%rip)
	movl	$1, next_func(%rip)
	movl	$4, next_var(%rip)
	retq
.Lfunc_end14:
	.size	init_tree, .Lfunc_end14-init_tree
	.cfi_endproc

	.globl	lookup
	.p2align	4, 0x90
	.type	lookup,@function
lookup:                                 # @lookup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 48
.Lcfi66:
	.cfi_offset %rbx, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	callq	strlen
	cmpq	$1, %rax
	je	.LBB15_2
# BB#1:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	warn
.LBB15_2:
	movq	name_tree(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_4
	jmp	.LBB15_6
	.p2align	4, 0x90
.LBB15_5:                               # %tailrecurse.backedge.i
                                        #   in Loop: Header=BB15_4 Depth=1
	leaq	24(%rbx), %rcx
	addq	$32, %rbx
	testl	%eax, %eax
	cmovsq	%rcx, %rbx
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB15_6
.LBB15_4:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB15_5
	jmp	.LBB15_7
.LBB15_6:                               # %.loopexit
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	%rbp, (%rbx)
	movq	$0, 8(%rbx)
	movl	$0, 16(%rbx)
	movl	$name_tree, %edi
	movq	%rbx, %rsi
	callq	insert_id_rec
.LBB15_7:                               # %find_id.exit
	testl	%r15d, %r15d
	je	.LBB15_24
# BB#8:                                 # %find_id.exit
	cmpl	$2, %r15d
	je	.LBB15_18
# BB#9:                                 # %find_id.exit
	cmpl	$1, %r15d
                                        # implicit-def: %EAX
	jne	.LBB15_30
# BB#10:
	cmpl	$0, 8(%rbx)
	je	.LBB15_12
# BB#11:
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	subl	8(%rbx), %eax
	jmp	.LBB15_30
.LBB15_24:
	cmpl	$0, 16(%rbx)
	je	.LBB15_26
# BB#25:
	movq	%r14, %rdi
	callq	free
	jmp	.LBB15_29
.LBB15_18:
	cmpl	$0, 12(%rbx)
	je	.LBB15_20
# BB#19:
	movq	%r14, %rdi
	callq	free
	movl	12(%rbx), %eax
	jmp	.LBB15_30
.LBB15_26:
	movslq	next_var(%rip), %rax
	leal	1(%rax), %ecx
	cmpq	$32767, %rax            # imm = 0x7FFF
	movl	%ecx, next_var(%rip)
	movl	%eax, 16(%rbx)
	movq	v_names(%rip), %rcx
	movq	%r14, -8(%rcx,%rax,8)
	jg	.LBB15_31
# BB#27:
	cmpl	v_count(%rip), %eax
	jl	.LBB15_30
# BB#28:
	callq	more_variables
.LBB15_29:
	movl	16(%rbx), %eax
	jmp	.LBB15_30
.LBB15_20:
	movslq	next_func(%rip), %rax
	leal	1(%rax), %ecx
	cmpq	$32766, %rax            # imm = 0x7FFE
	movl	%ecx, next_func(%rip)
	movl	%eax, 12(%rbx)
	movq	f_names(%rip), %rcx
	movq	%r14, (%rcx,%rax,8)
	jg	.LBB15_23
# BB#21:
	cmpl	f_count(%rip), %eax
	jl	.LBB15_30
# BB#22:
	callq	more_functions
	movl	12(%rbx), %eax
	jmp	.LBB15_30
.LBB15_12:
	movslq	next_array(%rip), %rax
	leal	1(%rax), %ecx
	cmpq	$32766, %rax            # imm = 0x7FFE
	movl	%ecx, next_array(%rip)
	movl	%eax, 8(%rbx)
	movq	a_names(%rip), %rcx
	movq	%r14, (%rcx,%rax,8)
	jg	.LBB15_16
# BB#13:
	cmpl	a_count(%rip), %eax
	jl	.LBB15_15
# BB#14:
	callq	more_arrays
	movl	8(%rbx), %eax
.LBB15_15:
	negl	%eax
.LBB15_30:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_31:
	movl	$.L.str.11, %edi
	jmp	.LBB15_17
.LBB15_23:
	movl	$.L.str.10, %edi
	jmp	.LBB15_17
.LBB15_16:
	movl	$.L.str.9, %edi
.LBB15_17:
	xorl	%eax, %eax
	callq	yyerror
	movl	$1, %edi
	callq	exit
.Lfunc_end15:
	.size	lookup, .Lfunc_end15-lookup
	.cfi_endproc

	.globl	welcome
	.p2align	4, 0x90
	.type	welcome,@function
welcome:                                # @welcome
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 16
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	popq	%rax
	jmp	puts                    # TAILCALL
.Lfunc_end16:
	.size	welcome, .Lfunc_end16-welcome
	.cfi_endproc

	.globl	warranty
	.p2align	4, 0x90
	.type	warranty,@function
warranty:                               # @warranty
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movl	$.L.str.14, %edi
	movl	$.L.str.15, %edx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movl	$.L.str.16, %edi
	movl	$.L.str.17, %esi
	movl	$.L.str.18, %edx
	movl	$.L.str.19, %ecx
	movl	$.L.str.20, %r8d
	movl	$.L.str.21, %r9d
	movl	$0, %eax
	pushq	$.L.str.27
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.26
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.25
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.24
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.23
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.22
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	callq	printf
	addq	$48, %rsp
.Lcfi78:
	.cfi_adjust_cfa_offset -48
	popq	%rax
	retq
.Lfunc_end17:
	.size	warranty, .Lfunc_end17-warranty
	.cfi_endproc

	.globl	limits
	.p2align	4, 0x90
	.type	limits,@function
limits:                                 # @limits
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 16
	movl	$.L.str.28, %edi
	movl	$99, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.29, %edi
	movl	$2048, %esi             # imm = 0x800
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.30, %edi
	movl	$99, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.31, %edi
	movl	$1000, %esi             # imm = 0x3E8
	xorl	%eax, %eax
	callq	printf
	movabsq	$9223372036854775807, %rsi # imm = 0x7FFFFFFFFFFFFFFF
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.33, %edi
	movl	$16384, %esi            # imm = 0x4000
	xorl	%eax, %eax
	callq	printf
	movabsq	$102481911520608620, %rsi # imm = 0x16C16C16C16C16C
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.35, %edi
	movl	$32767, %esi            # imm = 0x7FFF
	xorl	%eax, %eax
	popq	%rcx
	jmp	printf                  # TAILCALL
.Lfunc_end18:
	.size	limits, .Lfunc_end18-limits
	.cfi_endproc

	.globl	rt_error
	.p2align	4, 0x90
	.type	rt_error,@function
rt_error:                               # @rt_error
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 16
	subq	$464, %rsp              # imm = 0x1D0
.Lcfi81:
	.cfi_def_cfa_offset 480
.Lcfi82:
	.cfi_offset %rbx, -16
	movq	%rdi, %r10
	testb	%al, %al
	je	.LBB19_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB19_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	480(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	leaq	208(%rsp), %rbx
	movq	%rsp, %rdx
	movq	%rbx, %rdi
	movq	%r10, %rsi
	callq	vsprintf
	movq	stderr(%rip), %rdi
	movq	f_names(%rip), %rax
	movslq	pc(%rip), %rcx
	movq	(%rax,%rcx,8), %rdx
	movl	pc+4(%rip), %ecx
	movl	$.L.str.39, %esi
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	fprintf
	movb	$1, runtime_error(%rip)
	addq	$464, %rsp              # imm = 0x1D0
	popq	%rbx
	retq
.Lfunc_end19:
	.size	rt_error, .Lfunc_end19-rt_error
	.cfi_endproc

	.globl	rt_warn
	.p2align	4, 0x90
	.type	rt_warn,@function
rt_warn:                                # @rt_warn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 16
	subq	$464, %rsp              # imm = 0x1D0
.Lcfi84:
	.cfi_def_cfa_offset 480
.Lcfi85:
	.cfi_offset %rbx, -16
	movq	%rdi, %r10
	testb	%al, %al
	je	.LBB20_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB20_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	480(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	leaq	208(%rsp), %rbx
	movq	%rsp, %rdx
	movq	%rbx, %rdi
	movq	%r10, %rsi
	callq	vsprintf
	movq	stderr(%rip), %rdi
	movq	f_names(%rip), %rax
	movslq	pc(%rip), %rcx
	movq	(%rax,%rcx,8), %rdx
	movl	pc+4(%rip), %ecx
	movl	$.L.str.40, %esi
	xorl	%eax, %eax
	movq	%rbx, %r8
	callq	fprintf
	addq	$464, %rsp              # imm = 0x1D0
	popq	%rbx
	retq
.Lfunc_end20:
	.size	rt_warn, .Lfunc_end20-rt_warn
	.cfi_endproc

	.type	arglist2,@object        # @arglist2
	.local	arglist2
	.comm	arglist2,8,8
	.type	arglist1,@object        # @arglist1
	.local	arglist1
	.comm	arglist1,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"duplicate parameter names"
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Array parameter"
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"duplicate auto variable names"
	.size	.L.str.2, 30

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"variable in both parameter and auto lists"
	.size	.L.str.3, 42

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"@i"
	.size	.L.str.4, 3

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s"
	.size	.L.str.5, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"multiple letter name - %s"
	.size	.L.str.8, 26

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Too many array variables"
	.size	.L.str.9, 25

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Too many functions"
	.size	.L.str.10, 19

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Too many variables"
	.size	.L.str.11, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\n%s%s\n\n"
	.size	.L.str.14, 8

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"bc 1.02 (Mar 3, 92) Copyright (C) 1991, 1992 Free Software Foundation, Inc."
	.size	.L.str.15, 76

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"%s%s%s%s%s%s%s%s%s%s%s"
	.size	.L.str.16, 23

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"    This program is free software; you can redistribute it and/or modify\n"
	.size	.L.str.17, 74

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"    it under the terms of the GNU General Public License as published by\n"
	.size	.L.str.18, 74

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"    the Free Software Foundation; either version 2 of the License , or\n"
	.size	.L.str.19, 72

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"    (at your option) any later version.\n\n"
	.size	.L.str.20, 42

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"    This program is distributed in the hope that it will be useful,\n"
	.size	.L.str.21, 69

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"    but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	.size	.L.str.22, 68

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	.size	.L.str.23, 67

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"    GNU General Public License for more details.\n\n"
	.size	.L.str.24, 51

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"    You should have received a copy of the GNU General Public License\n"
	.size	.L.str.25, 71

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"    along with this program. If not, write to the Free Software\n"
	.size	.L.str.26, 65

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"    Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.\n\n"
	.size	.L.str.27, 58

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"BC_BASE_MAX     = %d\n"
	.size	.L.str.28, 22

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"BC_DIM_MAX      = %ld\n"
	.size	.L.str.29, 23

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"BC_SCALE_MAX    = %d\n"
	.size	.L.str.30, 22

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"BC_STRING_MAX   = %d\n"
	.size	.L.str.31, 22

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"MAX Exponent    = %ld\n"
	.size	.L.str.32, 23

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"MAX code        = %ld\n"
	.size	.L.str.33, 23

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"multiply digits = %ld\n"
	.size	.L.str.34, 23

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Number of vars  = %ld\n"
	.size	.L.str.35, 23

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"(standard_in)"
	.size	.L.str.36, 14

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"%s %d: "
	.size	.L.str.37, 8

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"%s %d: (Warning) "
	.size	.L.str.38, 18

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Runtime error (func=%s, adr=%d): %s\n"
	.size	.L.str.39, 37

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Runtime warning (func=%s, adr=%d): %s\n"
	.size	.L.str.40, 39

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"%d,"
	.size	.L.str.41, 4

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"%d"
	.size	.L.str.42, 3

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"@r"
	.size	.Lstr, 3

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"This is free software with ABSOLUTELY NO WARRANTY."
	.size	.Lstr.1, 51

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"For details type . "
	.size	.Lstr.2, 20


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
