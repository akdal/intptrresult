	.text
	.file	"parse.bc"
	.globl	P_addalpha
	.p2align	4, 0x90
	.type	P_addalpha,@function
P_addalpha:                             # @P_addalpha
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$1032, %rsp             # imm = 0x408
.Lcfi2:
	.cfi_def_cfa_offset 1056
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rcx
	movq	%rsp, %r14
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rsi
	callq	S_wordcpy
	movl	$_P_alpha, %edi
	callq	strlen
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	strlen
	addq	%rbx, %rax
	cmpq	$256, %rax              # imm = 0x100
	jb	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB0_2:
	movq	%rsp, %rsi
	movl	$_P_alpha, %edi
	callq	strcat
	addq	$1032, %rsp             # imm = 0x408
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	P_addalpha, .Lfunc_end0-P_addalpha
	.cfi_endproc

	.globl	P_file_parse
	.p2align	4, 0x90
	.type	P_file_parse,@function
P_file_parse:                           # @P_file_parse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi11:
	.cfi_def_cfa_offset 96
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%edi, _P_fnumb(%rip)
	movl	%esi, _P_start(%rip)
	movl	%edx, _P_lcount(%rip)
	movl	%ecx, _P_flags(%rip)
	movb	$0, _P_dummyline(%rip)
	movl	$_P_dummyline, %ebx
	xorl	%eax, %eax
	callq	C_clear_cmd
	xorl	%eax, %eax
	callq	T_clear_tols
	xorl	%eax, %eax
	callq	W_clearcoms
	xorl	%eax, %eax
	callq	W_clearlits
	movb	$0, _P_alpha(%rip)
	xorl	%eax, %eax
	callq	C_docmds
	movq	$_P_dummyline, _P_nextchr(%rip)
	movl	$0, _P_has_content(%rip)
	movl	$0, _P_next_tol(%rip)
	cmpl	$0, _P_fnumb(%rip)
	movl	$_L_bc, %eax
	movl	$_L_ac, %ecx
	cmovneq	%rax, %rcx
	movl	$_L_btlm, %r13d
	movl	$_L_atlm, %eax
	cmovneq	%r13, %rax
	movslq	(%rax), %rax
	movl	$0, (%rcx,%rax,4)
	movl	_P_start(%rip), %eax
	decl	%eax
	movl	%eax, _P_realline(%rip)
	cmpb	$0, (%rbx)
	jne	.LBB1_19
	jmp	.LBB1_2
	.p2align	4, 0x90
.LBB1_3:
	cmpl	$0, _P_has_content(%rip)
	je	.LBB1_18
# BB#4:
	movq	_P_firstchr(%rip), %rdi
	xorl	%eax, %eax
	callq	W_isbol
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_19
# BB#5:
	movq	%r15, %rdi
	callq	strlen
	addq	_P_nextchr(%rip), %rax
	movq	%rax, _P_nextchr(%rip)
	cmpb	$0, 16(%r15)
	je	.LBB1_18
# BB#6:                                 # %.preheader.i.i
	leaq	16(%r15), %r12
	movq	%r15, %rbx
	addq	$32, %rbx
	cmpb	$0, (%rax)
	jne	.LBB1_11
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_15:                               #   in Loop: Header=BB1_11 Depth=1
	movl	$1, %eax
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_8:
	callq	_P_nextline
	testl	%eax, %eax
	jne	.LBB1_102
# BB#9:
	cmpl	$0, _P_has_content(%rip)
	jne	.LBB1_11
# BB#10:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	W_is_bol
	testl	%eax, %eax
	jne	.LBB1_11
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_16:                               #   in Loop: Header=BB1_11 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	strlen
	addq	%rbp, %rax
	.p2align	4, 0x90
.LBB1_7:                                # %.sink.split.i.i
                                        #   in Loop: Header=BB1_11 Depth=1
	addq	_P_nextchr(%rip), %rax
	movq	%rax, _P_nextchr(%rip)
	cmpb	$0, (%rax)
	je	.LBB1_8
.LBB1_11:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rbx)
	je	.LBB1_14
# BB#12:                                #   in Loop: Header=BB1_11 Depth=1
	movq	_P_nextchr(%rip), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB1_14
# BB#13:                                #   in Loop: Header=BB1_11 Depth=1
	movq	_P_nextchr(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	addq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB1_16
	.p2align	4, 0x90
.LBB1_14:                               #   in Loop: Header=BB1_11 Depth=1
	movq	_P_nextchr(%rip), %rdi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB1_15
# BB#17:
	movq	%r12, %rdi
	callq	strlen
	addq	%rax, _P_nextchr(%rip)
	.p2align	4, 0x90
.LBB1_18:
	movq	_P_nextchr(%rip), %rbx
	cmpb	$0, (%rbx)
	je	.LBB1_2
.LBB1_19:
	testb	$1, _P_flags(%rip)
	jne	.LBB1_22
# BB#20:
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movq	_P_nextchr(%rip), %rdi
	movsbq	(%rdi), %rcx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB1_23
# BB#21:
	incq	%rdi
	movq	%rdi, _P_nextchr(%rip)
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_22:                               # %._crit_edge147.i
	movq	_P_nextchr(%rip), %rdi
.LBB1_23:
	movl	$_L_bl, %ebx
	xorl	%eax, %eax
	callq	W_iscom
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_43
# BB#24:
	movq	%r15, %rdi
	callq	strlen
	addq	_P_nextchr(%rip), %rax
	movq	%rax, _P_nextchr(%rip)
	cmpb	$0, 16(%r15)
	je	.LBB1_18
# BB#25:                                # %.preheader.i125.i
	leaq	16(%r15), %r12
	movq	%r15, %rbx
	addq	$32, %rbx
	movl	$1, %r14d
	cmpb	$0, (%rax)
	jne	.LBB1_30
	jmp	.LBB1_27
.LBB1_41:                               # %..outer.backedge_crit_edge.i.i
                                        #   in Loop: Header=BB1_30 Depth=1
	movq	_P_nextchr(%rip), %rax
	cmpb	$0, (%rax)
	jne	.LBB1_30
	jmp	.LBB1_27
.LBB1_42:                               #   in Loop: Header=BB1_30 Depth=1
	movq	%r12, %rdi
	callq	strlen
	addq	_P_nextchr(%rip), %rax
	movq	%rax, _P_nextchr(%rip)
	incl	%r14d
	cmpb	$0, (%rax)
	jne	.LBB1_30
	.p2align	4, 0x90
.LBB1_27:
	callq	_P_nextline
	testl	%eax, %eax
	jne	.LBB1_102
# BB#28:
	cmpl	$0, _P_has_content(%rip)
	jne	.LBB1_30
# BB#29:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	W_is_com
	testl	%eax, %eax
	jne	.LBB1_30
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_38:                               #   in Loop: Header=BB1_30 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	strlen
	addq	%rbp, %rax
	.p2align	4, 0x90
.LBB1_26:                               # %.backedge.i.i
                                        #   in Loop: Header=BB1_30 Depth=1
	addq	_P_nextchr(%rip), %rax
	movq	%rax, _P_nextchr(%rip)
	cmpb	$0, (%rax)
	je	.LBB1_27
.LBB1_30:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rbx)
	je	.LBB1_33
# BB#31:                                #   in Loop: Header=BB1_30 Depth=1
	movq	_P_nextchr(%rip), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB1_33
# BB#32:                                #   in Loop: Header=BB1_30 Depth=1
	movq	_P_nextchr(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	addq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB1_38
	.p2align	4, 0x90
.LBB1_33:                               #   in Loop: Header=BB1_30 Depth=1
	movq	_P_nextchr(%rip), %rdi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB1_39
# BB#34:                                #   in Loop: Header=BB1_30 Depth=1
	movq	%r15, %rdi
	callq	W_is_nesting
	testl	%eax, %eax
	je	.LBB1_37
# BB#35:                                #   in Loop: Header=BB1_30 Depth=1
	movq	_P_nextchr(%rip), %rdi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB1_42
.LBB1_37:                               #   in Loop: Header=BB1_30 Depth=1
	movl	$1, %eax
	jmp	.LBB1_26
.LBB1_39:                               #   in Loop: Header=BB1_30 Depth=1
	movq	%r12, %rdi
	callq	strlen
	addq	%rax, _P_nextchr(%rip)
	movq	%r15, %rdi
	callq	W_is_nesting
	testl	%eax, %eax
	je	.LBB1_18
# BB#40:                                #   in Loop: Header=BB1_30 Depth=1
	decl	%r14d
	jne	.LBB1_41
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_43:
	movl	_P_fnumb(%rip), %eax
	testl	%eax, %eax
	movl	$_L_atlm, %ecx
	cmovneq	%r13, %rcx
	movslq	(%rcx), %rcx
	je	.LBB1_45
# BB#44:
	cmpl	$0, _L_bc(,%rcx,4)
	jne	.LBB1_47
	jmp	.LBB1_46
	.p2align	4, 0x90
.LBB1_45:
	cmpl	$0, _L_ac(,%rcx,4)
	jne	.LBB1_47
.LBB1_46:
	testl	%eax, %eax
	movl	$_L_aclm, %edx
	movl	$_L_bclm, %esi
	cmovneq	%rsi, %rdx
	movl	(%rdx), %edx
	movl	$_L_atlindex, %esi
	movl	$_L_btlindex, %edi
	cmovneq	%rdi, %rsi
	movl	%edx, (%rsi,%rcx,4)
	movl	$_K_atm, %edx
	movl	$_K_btm, %esi
	cmovneq	%rsi, %rdx
	movl	(%rdx), %edx
	movl	$_L_ai, %esi
	movl	$_L_bi, %edi
	cmovneq	%rdi, %rsi
	movl	%edx, (%rsi,%rcx,4)
.LBB1_47:
	testl	%eax, %eax
	movl	$_L_atlindex, %eax
	movl	$_L_btlindex, %edx
	cmovneq	%rdx, %rax
	movslq	(%rax,%rcx,4), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	_P_nextchr(%rip), %r15
	subq	_P_firstchr(%rip), %r15
	movl	$40, %edi
	xorl	%eax, %eax
	callq	_Z_myalloc
	movq	%rax, %rbp
	cmpl	$0, _P_fnumb(%rip)
	movl	$_L_atlm, %eax
	cmovneq	%r13, %rax
	movl	(%rax), %eax
	movl	%eax, (%rbp)
	movl	%r15d, 4(%rbp)
	movq	_P_nextchr(%rip), %rdi
	xorl	%eax, %eax
	callq	W_islit
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB1_60
# BB#48:
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movl	$0, _P_stringsize(%rip)
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rbp
	addq	_P_nextchr(%rip), %rbp
	movq	%rbp, _P_nextchr(%rip)
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	%ecx, _P_stringsize(%rip)
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	cmpb	$0, 16(%r12)
	je	.LBB1_67
# BB#49:                                # %.preheader.i126.i
	leaq	16(%r12), %r13
	movq	%r12, %rbx
	addq	$32, %rbx
	cmpb	$0, (%rbp)
	jne	.LBB1_54
	jmp	.LBB1_51
	.p2align	4, 0x90
.LBB1_58:                               #   in Loop: Header=BB1_54 Depth=1
	movq	_P_nextchr(%rip), %rbp
	incq	%rbp
	movq	%rbp, _P_nextchr(%rip)
	movl	_P_stringsize(%rip), %eax
	incl	%eax
	jmp	.LBB1_50
	.p2align	4, 0x90
.LBB1_51:
	callq	_P_nextline
	testl	%eax, %eax
	jne	.LBB1_78
# BB#52:
	cmpl	$0, _P_has_content(%rip)
	jne	.LBB1_54
# BB#53:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	W_is_lit
	testl	%eax, %eax
	jne	.LBB1_54
	jmp	.LBB1_79
	.p2align	4, 0x90
.LBB1_59:                               #   in Loop: Header=BB1_54 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	addq	%r14, %rbp
	addq	_P_nextchr(%rip), %rbp
	movq	%rbp, _P_nextchr(%rip)
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	movq	%r13, %rdi
	callq	strlen
	addl	%r14d, %eax
	addl	_P_stringsize(%rip), %eax
	.p2align	4, 0x90
.LBB1_50:                               # %.sink.split.i127.i
                                        #   in Loop: Header=BB1_54 Depth=1
	movl	%eax, _P_stringsize(%rip)
	cmpb	$0, (%rbp)
	je	.LBB1_51
.LBB1_54:                               # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rbx)
	je	.LBB1_57
# BB#55:                                #   in Loop: Header=BB1_54 Depth=1
	movq	_P_nextchr(%rip), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB1_57
# BB#56:                                #   in Loop: Header=BB1_54 Depth=1
	movq	_P_nextchr(%rip), %rbp
	movq	%rbx, %rdi
	callq	strlen
	addq	%rax, %rbp
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	je	.LBB1_59
	.p2align	4, 0x90
.LBB1_57:                               #   in Loop: Header=BB1_54 Depth=1
	movq	_P_nextchr(%rip), %rdi
	xorl	%eax, %eax
	movq	%r13, %rsi
	callq	S_wordcmp
	testl	%eax, %eax
	jne	.LBB1_58
# BB#66:
	movq	%r13, %rdi
	callq	strlen
	addq	%rax, _P_nextchr(%rip)
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rcx
	addl	_P_stringsize(%rip), %ecx
	movl	%ecx, _P_stringsize(%rip)
	jmp	.LBB1_80
	.p2align	4, 0x90
.LBB1_60:
	movq	_P_nextchr(%rip), %rdi
	movl	_P_flags(%rip), %edx
	movl	%edx, %esi
	andl	$32, %esi
	andl	$16, %edx
	callq	F_isfloat
	movl	%eax, %ebx
	testl	%ebx, %ebx
	je	.LBB1_68
# BB#61:
	movq	%rbp, %rdi
	addq	$16, %rdi
	movq	_P_nextchr(%rip), %rsi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	S_savenstr
	movl	$2, 8(%rbp)
	testb	$2, _P_flags(%rip)
	jne	.LBB1_63
# BB#62:
	movq	16(%rbp), %rdi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	F_atof
	movq	%rax, 24(%rbp)
	movl	_P_next_tol(%rip), %edi
	xorl	%eax, %eax
	callq	T_gettol
	movq	%rax, 32(%rbp)
.LBB1_63:
	movl	_P_next_tol(%rip), %edi
	callq	T_moretols
	testl	%eax, %eax
	je	.LBB1_65
# BB#64:
	incl	_P_next_tol(%rip)
.LBB1_65:
	movslq	%ebx, %rax
	addq	_P_nextchr(%rip), %rax
	jmp	.LBB1_93
.LBB1_67:
	movl	$_L_bclindex, %r12d
	jmp	.LBB1_81
.LBB1_68:
	callq	__ctype_b_loc
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	_P_nextchr(%rip), %r12
	movsbq	(%r12), %rsi
	movzwl	(%rax,%rsi,2), %eax
	testb	$8, %ah
	jne	.LBB1_89
# BB#69:
	testb	$4, %ah
	jne	.LBB1_72
# BB#70:
	testb	%sil, %sil
	je	.LBB1_100
# BB#71:                                # %_P_in_alpha.exit.i
	movl	$_P_alpha, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	index
	testl	%eax, %eax
	je	.LBB1_100
.LBB1_72:
	movq	%rbp, %r14
	movq	%r12, 8(%rsp)
	movq	%r12, %rbp
	jmp	.LBB1_74
	.p2align	4, 0x90
.LBB1_73:                               # %.critedge.i
                                        #   in Loop: Header=BB1_74 Depth=1
	incq	%rbp
	movq	%rbp, 8(%rsp)
.LBB1_74:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movsbq	(%rbp), %rsi
	testb	$12, 1(%rax,%rsi,2)
	jne	.LBB1_73
# BB#75:                                #   in Loop: Header=BB1_74 Depth=1
	testb	%sil, %sil
	je	.LBB1_77
# BB#76:                                # %_P_in_alpha.exit132.i
                                        #   in Loop: Header=BB1_74 Depth=1
	movl	$_P_alpha, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	index
	testl	%eax, %eax
	jne	.LBB1_73
.LBB1_77:                               # %_P_in_alpha.exit132.thread.i
	movq	%r14, %rdi
	addq	$16, %rdi
	subq	%r12, %rbp
	xorl	%eax, %eax
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r14, %rbp
	jmp	.LBB1_92
.LBB1_78:
	movl	$1, %ebp
.LBB1_79:                               # %_P_litsnarf.exit.loopexit.i
	movl	_P_stringsize(%rip), %ecx
	movl	%ebp, 20(%rsp)          # 4-byte Spill
.LBB1_80:                               # %_P_litsnarf.exit.i
	movl	$_L_btlm, %r13d
	movl	$_L_bclindex, %r12d
	movl	$_L_bl, %ebx
.LBB1_81:                               # %_P_litsnarf.exit.i
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	$1, 8(%rbp)
	xorl	%eax, %eax
	leaq	8(%rsp), %rdi
	movl	%ecx, %esi
	callq	S_allocstr
	cmpl	$0, _P_fnumb(%rip)
	movl	$_L_aclm, %eax
	movl	$_L_bclm, %ecx
	cmovneq	%rcx, %rax
	movl	(%rax), %r14d
	movq	8(%rsp), %rdi
	movl	$_L_aclindex, %eax
	cmovneq	%r12, %rax
	movl	$_L_al, %ecx
	cmovneq	%rbx, %rcx
	movq	32(%rsp), %rbx          # 8-byte Reload
	movslq	(%rax,%rbx,4), %rax
	movslq	%r15d, %rsi
	addq	(%rcx,%rax,8), %rsi
	cmpl	%ebx, %r14d
	jle	.LBB1_85
# BB#82:
	callq	strcpy
	leal	1(%rbx), %ecx
	movq	8(%rsp), %r15
	cmpl	$0, _P_fnumb(%rip)
	setne	%al
	cmpl	%r14d, %ecx
	jge	.LBB1_86
# BB#83:                                # %.lr.ph.preheader.i
	leal	-1(%r14), %ebp
	subl	%ebx, %ebp
	leaq	4(,%rbx,4), %rbx
	movl	$_L_bl, %r13d
	.p2align	4, 0x90
.LBB1_84:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	andb	$1, %al
	movl	$_L_al, %ecx
	cmovneq	%r13, %rcx
	testb	%al, %al
	movl	$_L_aclindex, %eax
	cmovneq	%r12, %rax
	movslq	(%rax,%rbx), %rax
	movq	(%rcx,%rax,8), %rsi
	movq	%r15, %rdi
	callq	strcat
	cmpl	$0, _P_fnumb(%rip)
	movq	8(%rsp), %r15
	setne	%al
	addq	$4, %rbx
	decl	%ebp
	jne	.LBB1_84
	jmp	.LBB1_87
.LBB1_85:
	movslq	_P_stringsize(%rip), %rdx
	callq	strncpy
	movq	8(%rsp), %rax
	movslq	_P_stringsize(%rip), %rcx
	movb	$0, (%rax,%rcx)
	jmp	.LBB1_88
.LBB1_86:
	movl	$_L_bl, %r13d
.LBB1_87:                               # %._crit_edge.i
	movslq	%r14d, %rcx
	testb	%al, %al
	movl	$_L_aclindex, %eax
	cmovneq	%r12, %rax
	movl	$_L_al, %edx
	cmovneq	%r13, %rdx
	movslq	(%rax,%rcx,4), %rax
	movq	(%rdx,%rax,8), %r14
	movslq	_P_stringsize(%rip), %rbp
	movq	%r15, %rdi
	callq	strlen
	subq	%rax, %rbp
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	strncat
	movl	$_L_btlm, %r13d
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB1_88:
	movl	20(%rsp), %ebx          # 4-byte Reload
	movq	8(%rsp), %rax
	movq	%rax, 16(%rbp)
	jmp	.LBB1_94
.LBB1_89:                               # %.preheader.i.preheader
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB1_90:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, 8(%rsp)
	movq	(%rbx), %rax
	movsbq	(%rdx), %rcx
	incq	%rdx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB1_90
# BB#91:
	movq	%rbp, %rdi
	addq	$16, %rdi
	decq	%rdx
	subq	%r12, %rdx
	xorl	%eax, %eax
	movq	%r12, %rsi
.LBB1_92:
	callq	S_savenstr
	movl	$1, 8(%rbp)
	movq	8(%rsp), %rax
.LBB1_93:
	movq	%rax, _P_nextchr(%rip)
	xorl	%ebx, %ebx
.LBB1_94:
	movl	_P_fnumb(%rip), %edi
	testl	%edi, %edi
	movl	$_K_atm, %eax
	movl	$_K_btm, %ecx
	cmovneq	%rcx, %rax
	movl	(%rax), %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	K_settoken
	movl	_P_fnumb(%rip), %ecx
	testl	%ecx, %ecx
	movl	$_L_atlm, %eax
	cmovneq	%r13, %rax
	movslq	(%rax), %rax
	leaq	_L_ac(,%rax,4), %rdx
	leaq	_L_bc(,%rax,4), %rax
	cmoveq	%rdx, %rax
	incl	(%rax)
	testl	%ecx, %ecx
	je	.LBB1_96
# BB#95:
	movl	_K_btm(%rip), %eax
	incl	%eax
	movl	%eax, _K_btm(%rip)
	cmpl	$50000, %eax            # imm = 0xC350
	jl	.LBB1_97
	jmp	.LBB1_101
	.p2align	4, 0x90
.LBB1_96:
	movl	_K_atm(%rip), %eax
	incl	%eax
	movl	%eax, _K_atm(%rip)
	cmpl	$50000, %eax            # imm = 0xC350
	jge	.LBB1_101
.LBB1_97:
	testl	%ecx, %ecx
	movl	$_K_atm, %eax
	movl	$_K_btm, %edx
	cmovneq	%rdx, %rax
	movslq	(%rax), %rdx
	imulq	$274877907, %rdx, %rax  # imm = 0x10624DD3
	movq	%rax, %rsi
	shrq	$63, %rsi
	sarq	$38, %rax
	addl	%esi, %eax
	imull	$1000, %eax, %eax       # imm = 0x3E8
	cmpl	%eax, %edx
	jne	.LBB1_99
# BB#98:
	incl	%ecx
	movl	$Z_err_buf, %edi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_chatter
.LBB1_99:
	testl	%ebx, %ebx
	je	.LBB1_18
	jmp	.LBB1_102
.LBB1_100:                              # %_P_in_alpha.exit.thread.i
	movq	%rbp, %rdi
	addq	$16, %rdi
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	S_savenstr
	movl	$1, 8(%rbp)
	movq	_P_nextchr(%rip), %rax
	incq	%rax
	jmp	.LBB1_93
	.p2align	4, 0x90
.LBB1_2:
	callq	_P_nextline
	testl	%eax, %eax
	je	.LBB1_3
	jmp	.LBB1_102
.LBB1_101:
	movl	$Z_err_buf, %edi
	movl	$.L.str.2, %esi
	movl	$50000, %edx            # imm = 0xC350
	xorl	%eax, %eax
	callq	sprintf
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_complain
.LBB1_102:                              # %_P_do_parse.exit
	cmpl	$0, _P_has_content(%rip)
	je	.LBB1_107
# BB#103:
	movups	.L.str.1+16(%rip), %xmm0
	movups	%xmm0, Z_err_buf+16(%rip)
	movups	.L.str.1(%rip), %xmm0
	movups	%xmm0, Z_err_buf(%rip)
	movl	$681324, Z_err_buf+32(%rip) # imm = 0xA656C
	movl	$Z_err_buf, %edi
	xorl	%eax, %eax
	callq	Z_complain
	movl	_P_fnumb(%rip), %eax
	movl	$_L_aclm, %ecx
	movl	$_L_bclm, %edx
	testl	%eax, %eax
	cmoveq	%rcx, %rdx
	incl	(%rdx)
	testl	%eax, %eax
	movl	$_L_btlm, %ecx
	movl	$_L_atlm, %edx
	cmovneq	%rcx, %rdx
	movslq	(%rdx), %rcx
	je	.LBB1_105
# BB#104:
	cmpl	$0, _L_bc(,%rcx,4)
	jne	.LBB1_106
	jmp	.LBB1_107
.LBB1_105:
	cmpl	$0, _L_ac(,%rcx,4)
	je	.LBB1_107
.LBB1_106:
	testl	%eax, %eax
	movl	$_L_btlm, %eax
	movl	$_L_atlm, %edx
	cmovneq	%rax, %rdx
	incl	%ecx
	movl	%ecx, (%rdx)
.LBB1_107:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	P_file_parse, .Lfunc_end1-P_file_parse
	.cfi_endproc

	.p2align	4, 0x90
	.type	_P_nextline,@function
_P_nextline:                            # @_P_nextline
	.cfi_startproc
# BB#0:
	cmpl	$0, _P_has_content(%rip)
	je	.LBB2_6
# BB#1:
	movl	_P_fnumb(%rip), %eax
	movl	$_L_aclm, %ecx
	movl	$_L_bclm, %edx
	testl	%eax, %eax
	cmoveq	%rcx, %rdx
	incl	(%rdx)
	testl	%eax, %eax
	movl	$_L_btlm, %ecx
	movl	$_L_atlm, %edx
	cmovneq	%rcx, %rdx
	movslq	(%rdx), %rcx
	je	.LBB2_3
# BB#2:
	cmpl	$0, _L_bc(,%rcx,4)
	jne	.LBB2_4
	jmp	.LBB2_5
.LBB2_3:
	cmpl	$0, _L_ac(,%rcx,4)
	je	.LBB2_5
.LBB2_4:
	incl	%ecx
	testl	%eax, %eax
	movl	$_L_btlm, %eax
	movl	$_L_atlm, %edx
	cmovneq	%rax, %rdx
	movl	%ecx, (%rdx)
	movl	$_L_bc, %eax
	movl	$_L_ac, %ecx
	cmovneq	%rax, %rcx
	movslq	(%rdx), %rax
	movl	$0, (%rcx,%rax,4)
.LBB2_5:
	movl	$0, _P_has_content(%rip)
.LBB2_6:
	movl	$0, _P_next_tol(%rip)
	movslq	_P_realline(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, _P_realline(%rip)
	movl	_P_lcount(%rip), %esi
	addl	_P_start(%rip), %esi
	movl	$1, %eax
	cmpl	%esi, %edx
	jge	.LBB2_11
# BB#7:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	cmpl	$0, _P_fnumb(%rip)
	movl	$_L_bl, %eax
	movl	$_L_al, %edx
	cmovneq	%rax, %rdx
	movq	8(%rdx,%rcx,8), %rdi
	movq	%rdi, _P_nextchr(%rip)
	movq	%rdi, _P_firstchr(%rip)
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	C_is_cmd
	testl	%eax, %eax
	je	.LBB2_9
# BB#8:
	movq	$_P_dummyline, _P_nextchr(%rip)
	jmp	.LBB2_10
.LBB2_9:
	cmpl	$0, _P_fnumb(%rip)
	movl	_P_realline(%rip), %eax
	movl	$_L_bclindex, %ecx
	movl	$_L_aclindex, %edx
	cmovneq	%rcx, %rdx
	movl	$_L_bclm, %ecx
	movl	$_L_aclm, %esi
	cmovneq	%rcx, %rsi
	movslq	(%rsi), %rcx
	movl	%eax, (%rdx,%rcx,4)
	movl	$1, %ebx
.LBB2_10:
	movl	%ebx, _P_has_content(%rip)
	xorl	%eax, %eax
	popq	%rbx
.LBB2_11:
	retq
.Lfunc_end2:
	.size	_P_nextline, .Lfunc_end2-_P_nextline
	.cfi_endproc

	.type	_P_alpha,@object        # @_P_alpha
	.local	_P_alpha
	.comm	_P_alpha,256,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"too many characters added to extended alphabet"
	.size	.L.str, 47

	.type	_P_fnumb,@object        # @_P_fnumb
	.local	_P_fnumb
	.comm	_P_fnumb,4,4
	.type	_P_start,@object        # @_P_start
	.local	_P_start
	.comm	_P_start,4,4
	.type	_P_lcount,@object       # @_P_lcount
	.local	_P_lcount
	.comm	_P_lcount,4,4
	.type	_P_flags,@object        # @_P_flags
	.local	_P_flags
	.comm	_P_flags,4,4
	.type	_P_dummyline,@object    # @_P_dummyline
	.local	_P_dummyline
	.comm	_P_dummyline,2,1
	.type	_P_nextchr,@object      # @_P_nextchr
	.local	_P_nextchr
	.comm	_P_nextchr,8,8
	.type	_P_has_content,@object  # @_P_has_content
	.local	_P_has_content
	.comm	_P_has_content,4,4
	.type	_P_next_tol,@object     # @_P_next_tol
	.local	_P_next_tol
	.comm	_P_next_tol,4,4
	.type	_P_realline,@object     # @_P_realline
	.local	_P_realline
	.comm	_P_realline,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"parser got confused at end of file\n"
	.size	.L.str.1, 36

	.type	_P_firstchr,@object     # @_P_firstchr
	.local	_P_firstchr
	.comm	_P_firstchr,8,8
	.type	_P_stringsize,@object   # @_P_stringsize
	.local	_P_stringsize
	.comm	_P_stringsize,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"warning -- to many tokens in file only first %d tokens will be used.\n"
	.size	.L.str.2, 70

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"scanned %d words from file #%d\n"
	.size	.L.str.3, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
