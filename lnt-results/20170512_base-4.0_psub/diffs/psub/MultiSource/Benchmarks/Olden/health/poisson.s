	.text
	.file	"poisson.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4467570830353629184     # double 4.6566128752457969E-10
	.text
	.globl	my_rand
	.p2align	4, 0x90
	.type	my_rand,@function
my_rand:                                # @my_rand
	.cfi_startproc
# BB#0:
	xorq	$123459876, %rdi        # imm = 0x75BD924
	movabsq	$4730756183288445817, %rcx # imm = 0x41A705AF1FE3FB79
	movq	%rdi, %rax
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$-127773, %rdx, %rax    # imm = 0xFFFE0CE3
	addq	%rdi, %rax
	imulq	$16807, %rax, %rax      # imm = 0x41A7
	imulq	$-2836, %rdx, %rcx      # imm = 0xF4EC
	addq	%rax, %rcx
	movq	%rcx, %rax
	xorq	$123459876, %rax        # imm = 0x75BD924
	leaq	2147483647(%rax), %rdx
	testq	%rcx, %rcx
	cmovnsq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	retq
.Lfunc_end0:
	.size	my_rand, .Lfunc_end0-my_rand
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
