	.text
	.file	"bitarray.bc"
	.globl	alloc_bit_array
	.p2align	4, 0x90
	.type	alloc_bit_array,@function
alloc_bit_array:                        # @alloc_bit_array
	.cfi_startproc
# BB#0:
	addq	$7, %rdi
	shrq	$3, %rdi
	movl	$1, %esi
	jmp	calloc                  # TAILCALL
.Lfunc_end0:
	.size	alloc_bit_array, .Lfunc_end0-alloc_bit_array
	.cfi_endproc

	.globl	getbit
	.p2align	4, 0x90
	.type	getbit,@function
getbit:                                 # @getbit
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%esi, %eax
	sarl	$3, %eax
	cltq
	movsbl	(%rdi,%rax), %eax
	andb	$7, %sil
	movzbl	%sil, %ecx
	btl	%ecx, %eax
	sbbl	%eax, %eax
	andl	$1, %eax
	retq
.Lfunc_end1:
	.size	getbit, .Lfunc_end1-getbit
	.cfi_endproc

	.globl	setbit
	.p2align	4, 0x90
	.type	setbit,@function
setbit:                                 # @setbit
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%esi, %eax
	sarl	$3, %eax
	movslq	%eax, %r8
	andb	$7, %sil
	movl	$1, %eax
	movl	%esi, %ecx
	shll	%cl, %eax
	testl	%edx, %edx
	je	.LBB2_2
# BB#1:
	movsbl	(%rdi,%r8), %ecx
	orl	%eax, %ecx
	movb	%cl, (%rdi,%r8)
	retq
.LBB2_2:
	notl	%eax
	movsbl	(%rdi,%r8), %ecx
	andl	%eax, %ecx
	movb	%cl, (%rdi,%r8)
	retq
.Lfunc_end2:
	.size	setbit, .Lfunc_end2-setbit
	.cfi_endproc

	.globl	flipbit
	.p2align	4, 0x90
	.type	flipbit,@function
flipbit:                                # @flipbit
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$29, %eax
	addl	%esi, %eax
	sarl	$3, %eax
	cltq
	andb	$7, %sil
	movl	$1, %edx
	movl	%esi, %ecx
	shll	%cl, %edx
	movzbl	(%rdi,%rax), %ecx
	xorl	%edx, %ecx
	movb	%cl, (%rdi,%rax)
	retq
.Lfunc_end3:
	.size	flipbit, .Lfunc_end3-flipbit
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
