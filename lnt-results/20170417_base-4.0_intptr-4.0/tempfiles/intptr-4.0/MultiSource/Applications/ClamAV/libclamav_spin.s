	.text
	.file	"libclamav_spin.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.short	0                       # 0x0
	.short	255                     # 0xff
	.short	254                     # 0xfe
	.short	253                     # 0xfd
	.short	252                     # 0xfc
	.short	251                     # 0xfb
	.short	250                     # 0xfa
	.short	249                     # 0xf9
.LCPI0_1:
	.short	248                     # 0xf8
	.short	247                     # 0xf7
	.short	246                     # 0xf6
	.short	245                     # 0xf5
	.short	244                     # 0xf4
	.short	243                     # 0xf3
	.short	242                     # 0xf2
	.short	241                     # 0xf1
.LCPI0_2:
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.text
	.globl	unspin
	.p2align	4, 0x90
	.type	unspin,@function
unspin:                                 # @unspin
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movl	%r8d, %r12d
	movl	%ecx, %ebp
	movq	%rdx, %r15
	movl	%esi, 8(%rsp)           # 4-byte Spill
	movq	%rdi, %r14
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movslq	%ebp, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax,8), %r13
	movl	12(%r15,%r13,4), %edi
	callq	cli_malloc
	movq	%rax, %rcx
	movl	$1, %eax
	testq	%rcx, %rcx
	je	.LBB0_31
# BB#1:
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	leaq	12(%r15,%r13,4), %rax
	movl	8(%r15,%r13,4), %esi
	addq	%r14, %rsi
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	(%rax), %edx
	movq	%rcx, %rdi
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	callq	memcpy
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%r12d, %ebp
	leaq	(%rdi,%rbp), %r12
	movl	(%r15,%r13,4), %ebx
	subq	%rbx, %r12
	cmpb	$-69, 219(%r12)
	jne	.LBB0_27
# BB#2:
	cmpb	$-71, 224(%r12)
	jne	.LBB0_27
# BB#3:
	cmpl	$4606, 225(%r12)        # imm = 0x11FE
	jne	.LBB0_27
# BB#4:
	movzbl	220(%r12), %esi
	movl	$.L.str.2, %edi
	movl	$4606, %edx             # imm = 0x11FE
	xorl	%eax, %eax
	movq	%rsi, 16(%rsp)          # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	testq	%rax, %rax
	je	.LBB0_11
# BB#5:
	cmpq	%rdi, %r12
	jb	.LBB0_11
# BB#6:
	cmpl	$12770, %eax            # imm = 0x31E2
	jb	.LBB0_11
# BB#7:
	cmpl	$4, %eax
	jb	.LBB0_11
# BB#8:
	leaq	12770(%r12), %rcx
	cmpq	%rdi, %rcx
	jbe	.LBB0_11
# BB#9:
	addq	%rdi, %rax
	cmpq	%rax, %rcx
	ja	.LBB0_11
# BB#10:
	leaq	12823(%r12), %rdx
	cmpq	%rdi, %rdx
	jae	.LBB0_12
.LBB0_11:
	callq	free
	movl	$.L.str.3, %edi
	jmp	.LBB0_29
.LBB0_12:
	leaq	4(%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.LBB0_11
# BB#13:
	cmpq	%rax, %rcx
	ja	.LBB0_11
# BB#14:
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	cmpb	$-72, 480(%r12)
	je	.LBB0_16
# BB#15:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB0_16:
	testb	$32, 483(%r12)
	je	.LBB0_18
# BB#17:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB0_18:                               # %.lr.ph638.preheader
	leaq	8(%r15,%r13,4), %r13
	leaq	8179(%r12), %r8
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	addb	$16, %cl
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	leaq	12763(%rbp), %rsi
	subq	%rbx, %rsi
	movq	%rdi, %rdx
	addq	%rsi, %rdx
	xorl	%esi, %esi
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,255,254,253,252,251,250,249]
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [248,247,246,245,244,243,242,241]
	movdqa	.LCPI0_2(%rip), %xmm2   # xmm2 = [255,255,255,255,255,255,255,255]
.LBB0_19:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rsi), %ebp
	movd	%ebp, %xmm3
	pshuflw	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm3, %xmm3       # xmm3 = xmm3[0,0,1,1]
	movdqa	%xmm3, %xmm4
	paddw	%xmm0, %xmm4
	paddw	%xmm1, %xmm3
	movq	(%rdx,%rsi), %xmm5      # xmm5 = mem[0],zero
	punpcklbw	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3],xmm5[4],xmm0[4],xmm5[5],xmm0[5],xmm5[6],xmm0[6],xmm5[7],xmm0[7]
	pshufd	$78, %xmm5, %xmm5       # xmm5 = xmm5[2,3,0,1]
	pshuflw	$27, %xmm5, %xmm5       # xmm5 = xmm5[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm5, %xmm5       # xmm5 = xmm5[0,1,2,3,7,6,5,4]
	movq	-8(%rdx,%rsi), %xmm6    # xmm6 = mem[0],zero
	punpcklbw	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1],xmm6[2],xmm0[2],xmm6[3],xmm0[3],xmm6[4],xmm0[4],xmm6[5],xmm0[5],xmm6[6],xmm0[6],xmm6[7],xmm0[7]
	pshufd	$78, %xmm6, %xmm6       # xmm6 = xmm6[2,3,0,1]
	pshuflw	$27, %xmm6, %xmm6       # xmm6 = xmm6[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm6, %xmm6       # xmm6 = xmm6[0,1,2,3,7,6,5,4]
	pxor	%xmm4, %xmm5
	pxor	%xmm3, %xmm6
	pand	%xmm2, %xmm5
	pshufd	$78, %xmm5, %xmm3       # xmm3 = xmm5[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	packuswb	%xmm0, %xmm3
	movq	%xmm3, (%rdx,%rsi)
	pand	%xmm2, %xmm6
	pshufd	$78, %xmm6, %xmm3       # xmm3 = xmm6[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	packuswb	%xmm0, %xmm3
	movq	%xmm3, -8(%rdx,%rsi)
	addq	$-16, %rsi
	cmpq	$-4592, %rsi            # imm = 0xEE10
	jne	.LBB0_19
# BB#20:                                # %.lr.ph638
	movl	%eax, %edx
	addb	$15, %dl
	xorb	%cl, 8178(%r12)
	movl	%eax, %ecx
	addb	$14, %cl
	xorb	%dl, 8177(%r12)
	movl	%eax, %edx
	addb	$13, %dl
	xorb	%cl, -3(%r8)
	movl	%eax, %ecx
	xorb	%dl, -4(%r8)
	addb	$12, %cl
	movl	%eax, %edx
	addb	$11, %dl
	xorb	%cl, -5(%r8)
	movl	%eax, %ecx
	addb	$10, %cl
	xorb	%dl, -6(%r8)
	movl	%eax, %edx
	addb	$9, %dl
	xorb	%cl, -7(%r8)
	movl	%eax, %ecx
	xorb	%dl, -8(%r8)
	addb	$8, %cl
	movl	%eax, %edx
	addb	$7, %dl
	xorb	%cl, -9(%r8)
	movl	%eax, %ecx
	addb	$6, %cl
	xorb	%dl, -10(%r8)
	movl	%eax, %edx
	addb	$5, %dl
	xorb	%cl, -11(%r8)
	movl	%eax, %ecx
	xorb	%dl, -12(%r8)
	addb	$4, %cl
	addb	$3, %al
	xorb	%cl, -13(%r8)
	xorb	%al, -14(%r8)
	cmpl	$1440, 9968(%r12)       # imm = 0x5A0
	jne	.LBB0_27
# BB#21:
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movl	9963(%r12), %ebx
	xorl	%ebp, %ebp
	movl	$.L.str.6, %edi
	movl	$1440, %edx             # imm = 0x5A0
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
.LBB0_22:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	shrl	%eax
	movl	%eax, %ecx
	xorl	$-1942845388, %ecx      # imm = 0x8C328834
	testb	$1, %bl
	cmovel	%eax, %ecx
	movzbl	725(%r12,%rbp), %eax
	xorl	%ecx, %eax
	movb	%al, 725(%r12,%rbp)
	movl	%ecx, %eax
	shrl	%eax
	movl	%eax, %ebx
	xorl	$-1942845388, %ebx      # imm = 0x8C328834
	testb	$1, %cl
	cmovel	%eax, %ebx
	movzbl	726(%r12,%rbp), %eax
	xorl	%ebx, %eax
	movb	%al, 726(%r12,%rbp)
	addq	$2, %rbp
	cmpl	$1440, %ebp             # imm = 0x5A0
	jne	.LBB0_22
# BB#23:
	movl	1065(%r12), %esi
	movl	8(%rsp), %ebp           # 4-byte Reload
	movl	%ebp, %eax
	subl	%esi, %eax
	cmpl	%ebp, %eax
	movq	24(%rsp), %r8           # 8-byte Reload
	jae	.LBB0_32
# BB#24:
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %r15d
	testl	%eax, %eax
	movl	%r15d, 32(%rsp)         # 4-byte Spill
	je	.LBB0_33
# BB#25:                                # %.lr.ph.i.preheader
	subl	%ebp, %esi
	movl	$-1, %edx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movl	$-1, %eax
.LBB0_26:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi), %ecx
	shll	$8, %ecx
	movzwl	%cx, %ebx
	xorl	%eax, %ebx
	shrl	$3, %ebx
	movl	%edx, %ecx
	movzbl	%ch, %edx  # NOREX
	xorl	%ebx, %edx
	addl	$2013372680, %edx       # imm = 0x7801A108
	xorl	%ecx, %edx
	rorl	%cl, %edx
	movzbl	%dh, %ebx  # NOREX
	xorl	%ecx, %ebx
	addl	$2013372680, %ebx       # imm = 0x7801A108
	xorl	%edx, %ebx
	movl	%edx, %ecx
	rorl	%cl, %ebx
	movzbl	%bh, %eax  # NOREX
	xorl	%edx, %eax
	addl	$2013372680, %eax       # imm = 0x7801A108
	xorl	%ebx, %eax
	movl	%ebx, %ecx
	rorl	%cl, %eax
	movzbl	%ah, %edx  # NOREX
	xorl	%ebx, %edx
	addl	$2013372680, %edx       # imm = 0x7801A108
	xorl	%eax, %edx
	movl	%eax, %ecx
	rorl	%cl, %edx
	incq	%rdi
	incl	%esi
	jne	.LBB0_26
	jmp	.LBB0_34
.LBB0_27:
	callq	free
.LBB0_28:                               # %.thread553
	movl	$.L.str.1, %edi
.LBB0_29:                               # %.thread553
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_30:                               # %.thread553
	movl	$1, %eax
.LBB0_31:                               # %.thread553
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_32:
	movq	%r8, %rdi
	callq	free
	movl	$.L.str.7, %edi
	jmp	.LBB0_29
.LBB0_33:
	movl	$-1, %edx
	movq	16(%rsp), %r14          # 8-byte Reload
.LBB0_34:                               # %summit.exit
	movl	%edx, %r15d
	movq	%r13, %r12
	movl	(%r12), %edi
	addq	%r14, %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	movq	%r8, %rsi
	movq	%r8, %rbx
	callq	memcpy
	movq	%rbx, %rdi
	callq	free
	cmpl	$4, %ebp
	jl	.LBB0_75
# BB#35:
	movq	72(%rsp), %r13          # 8-byte Reload
	addq	%r14, %r13
	movl	(%r12), %edx
	addq	%r13, %rdx
	movl	-8(%r12), %eax
	subq	%rax, %rdx
	leaq	12807(%rdx), %rax
	cmpq	%r14, %rax
	jb	.LBB0_75
# BB#36:
	leaq	4(%rax), %rcx
	movslq	%ebp, %r13
	addq	%r14, %r13
	cmpq	%r13, %rcx
	ja	.LBB0_75
# BB#37:
	cmpq	%r14, %rcx
	jbe	.LBB0_75
# BB#38:
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	32(%rsp), %r12d         # 4-byte Reload
	subl	%r15d, %r12d
	movl	(%rax), %r14d
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	movl	%r14d, %edx
	callq	cli_dbgmsg
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	jle	.LBB0_49
# BB#39:                                # %.lr.ph628
	movl	8(%rsp), %r8d           # 4-byte Reload
	addq	16(%rsp), %r8           # 8-byte Folded Reload
	movl	%r12d, %edx
	shrl	%edx
	movl	%edx, %r9d
	xorl	$-314331343, %r9d       # imm = 0xED43AF31
	testb	$1, %r12b
	cmovnel	%edx, %r9d
	xorl	%esi, %esi
.LBB0_40:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_47 Depth 2
	testb	$1, %r14b
	je	.LBB0_48
# BB#41:                                #   in Loop: Header=BB0_40 Depth=1
	leaq	(%rsi,%rsi,8), %rdx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	12(%rax,%rdx,4), %edi
	leal	-1(%rdi), %ebp
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jae	.LBB0_77
# BB#42:                                #   in Loop: Header=BB0_40 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	8(%rax,%rdx,4), %edx
	addq	16(%rsp), %rdx          # 8-byte Folded Reload
	leaq	(%rdx,%rdi), %rbx
	cmpq	%r8, %rbx
	ja	.LBB0_77
# BB#43:                                #   in Loop: Header=BB0_40 Depth=1
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jbe	.LBB0_77
# BB#44:                                # %.lr.ph624.preheader
                                        #   in Loop: Header=BB0_40 Depth=1
	testb	$1, %dil
	jne	.LBB0_46
# BB#45:                                #   in Loop: Header=BB0_40 Depth=1
	movl	%r12d, %ebx
	testl	%ebp, %ebp
	jne	.LBB0_47
	jmp	.LBB0_48
.LBB0_46:                               # %.lr.ph624.prol
                                        #   in Loop: Header=BB0_40 Depth=1
	movzbl	(%rdx), %edi
	xorl	%r9d, %edi
	movb	%dil, (%rdx)
	incq	%rdx
	movl	%r9d, %ebx
	movl	%ebp, %edi
	testl	%ebp, %ebp
	je	.LBB0_48
.LBB0_47:                               # %.lr.ph624
                                        #   Parent Loop BB0_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %eax
	shrl	%eax
	movl	%eax, %ebp
	xorl	$-314331343, %ebp       # imm = 0xED43AF31
	testb	$1, %bl
	cmovnel	%eax, %ebp
	movzbl	(%rdx), %eax
	xorl	%ebp, %eax
	movb	%al, (%rdx)
	movl	%ebp, %eax
	shrl	%eax
	movl	%eax, %ebx
	xorl	$-314331343, %ebx       # imm = 0xED43AF31
	testb	$1, %bpl
	cmovnel	%eax, %ebx
	movzbl	1(%rdx), %eax
	xorl	%ebx, %eax
	addl	$-2, %edi
	movb	%al, 1(%rdx)
	leaq	2(%rdx), %rdx
	jne	.LBB0_47
.LBB0_48:                               # %.loopexit569
                                        #   in Loop: Header=BB0_40 Depth=1
	shrl	%r14d
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB0_40
.LBB0_49:                               # %._crit_edge629
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$384, 1604(%rax)        # imm = 0x180
	jne	.LBB0_28
# BB#50:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	1616(%rbp), %ebx
	movl	$.L.str.6, %edi
	movl	$384, %edx              # imm = 0x180
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	leaq	10451(%rbp), %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	jb	.LBB0_75
# BB#51:
	cmpl	$384, 8(%rsp)           # 4-byte Folded Reload
                                        # imm = 0x180
	jb	.LBB0_75
# BB#52:
	leaq	384(%rax), %rcx
	movl	8(%rsp), %r14d          # 4-byte Reload
	addq	16(%rsp), %r14          # 8-byte Folded Reload
	cmpq	%r14, %rcx
	ja	.LBB0_75
# BB#53:
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	jbe	.LBB0_75
# BB#54:                                # %.preheader567.preheader
	xorl	%ecx, %ecx
.LBB0_55:                               # %.preheader567
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebx, %edx
	shrl	%edx
	movl	%edx, %esi
	xorl	$-314331342, %esi       # imm = 0xED43AF32
	testb	$1, %bl
	cmovel	%edx, %esi
	movzbl	(%rax,%rcx), %edx
	xorl	%esi, %edx
	movb	%dl, (%rax,%rcx)
	movl	%esi, %edx
	shrl	%edx
	movl	%edx, %ebx
	xorl	$-314331342, %ebx       # imm = 0xED43AF32
	testb	$1, %sil
	cmovel	%edx, %ebx
	movzbl	1(%rax,%rcx), %edx
	xorl	%ebx, %edx
	movb	%dl, 1(%rax,%rcx)
	addq	$2, %rcx
	cmpl	$384, %ecx              # imm = 0x180
	jne	.LBB0_55
# BB#56:
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$417, 10461(%rax)       # imm = 0x1A1
	jne	.LBB0_28
# BB#57:
	movl	$.L.str.13, %edi
	movl	$417, %esi              # imm = 0x1A1
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	1748(%rax), %rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jb	.LBB0_78
# BB#58:
	cmpl	$417, 8(%rsp)           # 4-byte Folded Reload
                                        # imm = 0x1A1
	jb	.LBB0_78
# BB#59:
	leaq	417(%rbx), %rax
	cmpq	%r14, %rax
	ja	.LBB0_78
# BB#60:
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	jbe	.LBB0_78
# BB#61:                                # %.preheader566
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	10461(%rax), %r14
	addq	$15, %r14
	movl	$417, %ebp              # imm = 0x1A1
	leaq	68(%rsp), %r15
.LBB0_62:                               # =>This Inner Loop Header: Depth=1
	movl	$0, 68(%rsp)
	leal	-256(%rbp), %eax
	movzbl	(%rbx), %edi
	movzbl	%al, %esi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	exec86
	movb	%al, (%rbx)
	cmpl	$0, 68(%rsp)
	jne	.LBB0_79
# BB#63:                                # %.thread
                                        #   in Loop: Header=BB0_62 Depth=1
	incq	%rbx
	decl	%ebp
	jne	.LBB0_62
# BB#64:
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	1777(%rax), %r14d
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_83
# BB#65:                                # %.lr.ph613
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	1877(%rax), %r15
	leaq	36(%r15), %rax
	cmpq	%r13, %rax
	setbe	%r12b
	cmpq	%rax, 16(%rsp)          # 8-byte Folded Reload
	sbbb	%al, %al
	cmpl	$36, 8(%rsp)            # 4-byte Folded Reload
	jl	.LBB0_80
# BB#66:                                # %.lr.ph613
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	jb	.LBB0_80
# BB#67:                                # %.lr.ph613.split.preheader
	andb	%al, %r12b
	xorl	%r13d, %r13d
.LBB0_68:                               # %.lr.ph613.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_72 Depth 2
	testb	$1, %r14b
	je	.LBB0_74
# BB#69:                                #   in Loop: Header=BB0_68 Depth=1
	testb	%r12b, %r12b
	je	.LBB0_117
# BB#70:                                # %.preheader565
                                        #   in Loop: Header=BB0_68 Depth=1
	leaq	(%r13,%r13,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	12(%rcx,%rax,4), %ebx
	testl	%ebx, %ebx
	je	.LBB0_74
# BB#71:                                # %.lr.ph609.preheader
                                        #   in Loop: Header=BB0_68 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	8(%rcx,%rax,4), %ebp
	addq	16(%rsp), %rbp          # 8-byte Folded Reload
.LBB0_72:                               # %.lr.ph609
                                        #   Parent Loop BB0_68 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, 64(%rsp)
	movzbl	(%rbp), %edi
	movzbl	%bl, %esi
	movq	%r15, %rdx
	leaq	64(%rsp), %rcx
	callq	exec86
	movb	%al, (%rbp)
	cmpl	$0, 64(%rsp)
	jne	.LBB0_116
# BB#73:                                # %.thread551
                                        #   in Loop: Header=BB0_72 Depth=2
	incq	%rbp
	decl	%ebx
	jne	.LBB0_72
.LBB0_74:                               # %.loopexit
                                        #   in Loop: Header=BB0_68 Depth=1
	shrl	%r14d
	incq	%r13
	cmpq	56(%rsp), %r13          # 8-byte Folded Reload
	jl	.LBB0_68
	jmp	.LBB0_83
.LBB0_75:
	movl	$.L.str.8, %edi
	jmp	.LBB0_29
.LBB0_77:                               # %.critedge
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB0_30
.LBB0_78:
	movl	$.L.str.14, %edi
	jmp	.LBB0_29
.LBB0_79:
	movl	$.L.str.15, %edi
	jmp	.LBB0_29
.LBB0_80:                               # %.lr.ph613.split.us.preheader
	xorl	%eax, %eax
.LBB0_81:                               # %.lr.ph613.split.us
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %r14b
	jne	.LBB0_117
# BB#82:                                #   in Loop: Header=BB0_81 Depth=1
	shrl	%r14d
	incl	%eax
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	jl	.LBB0_81
.LBB0_83:                               # %._crit_edge614
	xorl	%r13d, %r13d
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	12385(%rax), %esi
	movl	$.L.str.19, %edi
	xorl	%eax, %eax
	movl	%esi, 8(%rsp)           # 4-byte Spill
	callq	cli_dbgmsg
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(,%rax,8), %rdi
	callq	cli_malloc
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_30
# BB#84:                                # %.preheader564
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_95
# BB#85:                                # %.lr.ph604
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax), %rbp
	xorl	%r14d, %r14d
	movl	8(%rsp), %ebx           # 4-byte Reload
	xorl	%r15d, %r15d
.LBB0_86:                               # =>This Inner Loop Header: Depth=1
	leaq	-8(%rbp), %rcx
	movl	%ebx, %eax
	andl	$1, %eax
	cmoveq	%rbp, %rcx
	movl	(%rcx), %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rcx
	movq	32(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_90
# BB#87:                                #   in Loop: Header=BB0_86 Depth=1
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_90
# BB#88:                                #   in Loop: Header=BB0_86 Depth=1
	movq	%rcx, %rdx
	subq	48(%rsp), %rdx          # 8-byte Folded Reload
	jb	.LBB0_118
# BB#89:                                #   in Loop: Header=BB0_86 Depth=1
	movslq	%r15d, %rsi
	cmpq	%rdx, %rsi
	ja	.LBB0_118
.LBB0_90:                               #   in Loop: Header=BB0_86 Depth=1
	testl	%eax, %eax
	je	.LBB0_93
# BB#91:                                #   in Loop: Header=BB0_86 Depth=1
	movl	$1, %esi
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	cli_calloc
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%r14,8)
	testq	%rax, %rax
	je	.LBB0_124
# BB#92:                                #   in Loop: Header=BB0_86 Depth=1
	movl	(%rbp), %edx
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movq	48(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %ecx
	callq	cli_dbgmsg
	movl	-4(%rbp), %edi
	movl	(%rbp), %edx
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r14,8), %rsi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%r13d, %ecx
	callq	cli_unfsg
	cmpl	$-1, %eax
	jne	.LBB0_94
	jmp	.LBB0_125
.LBB0_93:                               #   in Loop: Header=BB0_86 Depth=1
	movl	-4(%rbp), %eax
	addq	16(%rsp), %rax          # 8-byte Folded Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx,%r14,8)
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	cli_dbgmsg
.LBB0_94:                               #   in Loop: Header=BB0_86 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	addl	%r15d, %eax
	shrl	%ebx
	incq	%r14
	addq	$36, %rbp
	cmpq	56(%rsp), %r14          # 8-byte Folded Reload
	movq	%rax, %r13
	movl	%eax, %r15d
	jl	.LBB0_86
.LBB0_95:                               # %._crit_edge605
	xorl	%ebx, %ebx
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	12270(%rax), %r15d
	testq	%r15, %r15
	je	.LBB0_104
# BB#96:                                # %.preheader563
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_101
# BB#97:                                # %.lr.ph597.preheader
	xorl	%ebx, %ebx
	movq	40(%rsp), %rax          # 8-byte Reload
.LBB0_98:                               # %.lr.ph597
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	cmpl	%r15d, %ecx
	ja	.LBB0_100
# BB#99:                                #   in Loop: Header=BB0_98 Depth=1
	addl	12(%rax), %ecx
	cmpl	%r15d, %ecx
	ja	.LBB0_101
.LBB0_100:                              #   in Loop: Header=BB0_98 Depth=1
	incq	%rbx
	addq	$36, %rax
	cmpq	56(%rsp), %rbx          # 8-byte Folded Reload
	jl	.LBB0_98
.LBB0_101:                              # %._crit_edge598
	cmpl	12(%rsp), %ebx          # 4-byte Folded Reload
	je	.LBB0_103
# BB#102:
	movl	$1, %r14d
	movl	%ebx, %ecx
	shll	%cl, %r14d
	movl	8(%rsp), %eax           # 4-byte Reload
	btl	%ebx, %eax
	jae	.LBB0_132
.LBB0_103:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_104:
	movslq	%r13d, %rdi
	callq	cli_malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_120
# BB#105:
	movq	56(%rsp), %rax          # 8-byte Reload
	shlq	$2, %rax
	leaq	(%rax,%rax,8), %rdi
	callq	cli_malloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_119
# BB#106:                               # %.preheader
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jle	.LBB0_113
# BB#107:                               # %.lr.ph594.preheader
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rbp          # 8-byte Reload
	addq	$12, %rbp
	addq	$12, 40(%rsp)           # 8-byte Folded Spill
	xorl	%ebx, %ebx
	movq	%r15, %r13
.LBB0_108:                              # %.lr.ph594
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	movl	$0, %eax
	jle	.LBB0_110
# BB#109:                               #   in Loop: Header=BB0_108 Depth=1
	movl	-36(%rbp), %eax
	addl	-40(%rbp), %eax
.LBB0_110:                              #   in Loop: Header=BB0_108 Depth=1
	movl	%eax, -4(%rbp)
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	-8(%rcx), %rax
	movl	8(%rsp), %r14d          # 4-byte Reload
	andl	$1, %r14d
	cmoveq	%rcx, %rax
	movl	(%rax), %edx
	movl	%edx, (%rbp)
	movl	-12(%rcx), %eax
	movl	%eax, -12(%rbp)
	movl	-8(%rcx), %eax
	movl	%eax, -8(%rbp)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	movq	%r13, %rdi
	callq	memcpy
	movl	(%rbp), %r12d
	testl	%r14d, %r14d
	je	.LBB0_112
# BB#111:                               #   in Loop: Header=BB0_108 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rdi
	callq	free
.LBB0_112:                              #   in Loop: Header=BB0_108 Depth=1
	addq	%r12, %r13
	shrl	8(%rsp)                 # 4-byte Folded Spill
	incq	%rbx
	addq	$36, %rbp
	addq	$36, 40(%rsp)           # 8-byte Folded Spill
	cmpq	%rbx, 24(%rsp)          # 8-byte Folded Reload
	jne	.LBB0_108
.LBB0_113:                              # %._crit_edge595
	xorl	%ebp, %ebp
	movl	$4194304, %ecx          # imm = 0x400000
	movl	$4096, %r8d             # imm = 0x1000
	movl	$0, %r9d
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB0_115
# BB#114:
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$1, %ebp
.LBB0_115:
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	%ebp, %eax
	jmp	.LBB0_31
.LBB0_116:                              # %.us-lcssa615
	movl	$.L.str.18, %edi
	jmp	.LBB0_29
.LBB0_117:                              # %.us-lcssa
	movl	$.L.str.17, %edi
	jmp	.LBB0_29
.LBB0_118:
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movq	48(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	cli_dbgmsg
	jmp	.LBB0_126
.LBB0_119:                              # %.thread562
	movq	%r15, %rdi
	callq	free
.LBB0_120:
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	movl	8(%rsp), %ebx           # 4-byte Reload
	movl	%ebx, %esi
	callq	cli_dbgmsg
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	setle	%al
	testb	$1, %bl
	je	.LBB0_131
# BB#121:
	testb	%al, %al
	jne	.LBB0_131
# BB#122:                               # %.lr.ph589.split.preheader
	movl	12(%rsp), %ebx          # 4-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB0_123:                              # %.lr.ph589.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	callq	free
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB0_123
	jmp	.LBB0_131
.LBB0_124:
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	movq	48(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB0_126
.LBB0_125:
	incl	%r14d
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_126:                              # %.thread560
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testl	%r14d, %r14d
	jle	.LBB0_131
# BB#127:                               # %.lr.ph.preheader
	movl	%r14d, %ebx
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB0_128:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, 8(%rsp)             # 1-byte Folded Reload
	je	.LBB0_130
# BB#129:                               #   in Loop: Header=BB0_128 Depth=1
	movq	(%rbp), %rdi
	callq	free
.LBB0_130:                              #   in Loop: Header=BB0_128 Depth=1
	shrl	8(%rsp)                 # 4-byte Folded Spill
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB0_128
.LBB0_131:                              # %._crit_edge590
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB0_30
.LBB0_132:
	movslq	%ebx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax,8), %r12
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	(%rbp,%r12,4), %edx
	movl	%r15d, %ecx
	subl	%edx, %ecx
	movl	4(%rbp,%r12,4), %r9d
	subl	%ecx, %r9d
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%r15d, %r8d
	callq	cli_dbgmsg
	movl	4(%rbp,%r12,4), %edi
	callq	cli_malloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_135
# BB#133:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r12,4), %rbp
	movl	8(%rax,%r12,4), %esi
	addq	16(%rsp), %rsi          # 8-byte Folded Reload
	movl	%r15d, %edx
	subl	(%rbp), %edx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	memcpy
	addq	%r15, %rbx
	movl	(%rbp), %edx
	movq	%rbx, %rdi
	subq	%rdx, %rdi
	subl	%r15d, %edx
	addl	4(%rbp), %edx
	xorl	%esi, %esi
	callq	memset
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	8(%rcx,%r12,4), %edi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	addq	%r15, %rdi
	movl	(%rbp), %eax
	subq	%rax, %rdi
	subq	%rax, %rbx
	movl	12(%rcx,%r12,4), %edx
	subl	%eax, %r15d
	subl	%r15d, %edx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movl	4(%rbp), %ecx
	subl	%r15d, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	callq	cli_unfsg
	testl	%eax, %eax
	je	.LBB0_136
# BB#134:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax,%r12,4), %rbx
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	addl	(%rbx), %r13d
	jmp	.LBB0_104
.LBB0_135:
	movq	40(%rsp), %rax          # 8-byte Reload
	addl	12(%rax,%r12,4), %r13d
	jmp	.LBB0_104
.LBB0_136:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rax,%rcx,8)
	orl	%r14d, 8(%rsp)          # 4-byte Folded Spill
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	16(%rsp), %rax          # 8-byte Reload
	addl	4(%rax), %r13d
	jmp	.LBB0_104
.Lfunc_end0:
	.size	unspin, .Lfunc_end0-unspin
	.cfi_endproc

	.p2align	4, 0x90
	.type	exec86,@function
exec86:                                 # @exec86
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edi, %r15d
	movl	$0, (%r14)
	xorl	%ebp, %ebp
	jmp	.LBB1_3
.LBB1_1:                                #   in Loop: Header=BB1_3 Depth=1
	movl	%r15d, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %eax
	movl	$8, %ecx
	subl	%r8d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r15d
.LBB1_2:                                #   in Loop: Header=BB1_3 Depth=1
	orl	%eax, %r15d
	addl	$3, %ebp
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rbx
	movzbl	(%rdx,%rbx), %eax
	leaq	1(%rbx), %rdi
	movl	%eax, %ecx
	addb	$64, %cl
	cmpb	$116, %cl
	ja	.LBB1_7
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	movzbl	%cl, %ecx
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_5:                                #   in Loop: Header=BB1_3 Depth=1
	movzbl	%r15b, %r15d
	movzbl	2(%rdx,%rbx), %ecx
	movl	%ecx, %r8d
	andl	$7, %r8d
	andl	$7, %ecx
	cmpb	$-64, (%rdx,%rdi)
	jne	.LBB1_1
# BB#6:                                 #   in Loop: Header=BB1_3 Depth=1
	movl	%r15d, %eax
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	movl	$8, %ecx
	subl	%r8d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %r15d
	jmp	.LBB1_2
.LBB1_7:                                #   in Loop: Header=BB1_3 Depth=1
	cmpb	$-112, %al
	je	.LBB1_21
	jmp	.LBB1_25
.LBB1_8:                                #   in Loop: Header=BB1_3 Depth=1
	addl	$2, %ebp
	movl	%ebp, %edi
.LBB1_9:                                #   in Loop: Header=BB1_3 Depth=1
	incl	%edi
	jmp	.LBB1_21
.LBB1_10:                               #   in Loop: Header=BB1_3 Depth=1
	cmpb	$-64, (%rdx,%rdi)
	movb	$1, %cl
	je	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_3 Depth=1
	movb	$-1, %cl
.LBB1_12:                               #   in Loop: Header=BB1_3 Depth=1
	addb	%cl, %r15b
	jmp	.LBB1_19
.LBB1_13:                               #   in Loop: Header=BB1_3 Depth=1
	addb	%sil, %r15b
	jmp	.LBB1_19
.LBB1_14:                               #   in Loop: Header=BB1_3 Depth=1
	addb	(%rdx,%rdi), %r15b
	jmp	.LBB1_19
.LBB1_15:                               #   in Loop: Header=BB1_3 Depth=1
	subb	%sil, %r15b
	jmp	.LBB1_19
.LBB1_16:                               #   in Loop: Header=BB1_3 Depth=1
	subb	(%rdx,%rdi), %r15b
	jmp	.LBB1_19
.LBB1_17:                               #   in Loop: Header=BB1_3 Depth=1
	xorb	%sil, %r15b
	jmp	.LBB1_19
.LBB1_18:                               #   in Loop: Header=BB1_3 Depth=1
	xorb	(%rdx,%rdi), %r15b
	.p2align	4, 0x90
.LBB1_19:                               # %.thread.backedge
                                        #   in Loop: Header=BB1_3 Depth=1
	addl	$2, %ebp
.LBB1_20:                               # %.thread.backedge
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	%ebp, %edi
.LBB1_21:                               # %.thread.backedge
                                        #   in Loop: Header=BB1_3 Depth=1
	cmpl	$36, %edi
	movl	%edi, %ebp
	jl	.LBB1_3
# BB#22:
	jne	.LBB1_24
# BB#23:
	cmpb	$-86, 36(%rdx)
	je	.LBB1_27
.LBB1_24:
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_26
.LBB1_25:
	movzbl	%al, %esi
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB1_26:                               # %.sink.split
	movl	$1, (%r14)
.LBB1_27:
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	exec86, .Lfunc_end1-exec86
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_5
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_8
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_21
	.quad	.LBB1_21
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_10
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_13
	.quad	.LBB1_25
	.quad	.LBB1_14
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_9
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_15
	.quad	.LBB1_25
	.quad	.LBB1_16
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_25
	.quad	.LBB1_17
	.quad	.LBB1_25
	.quad	.LBB1_18

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in unspin\n"
	.size	.L.str, 11

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"spin: Not spinned or bad version\n"
	.size	.L.str.1, 34

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"spin: Key8 is %x, Len is %x\n"
	.size	.L.str.2, 29

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"spin: len or key out of bounds, giving up\n"
	.size	.L.str.3, 43

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"spin: prolly not spinned, expect failure\n"
	.size	.L.str.4, 42

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"spin: password protected, expect failure\n"
	.size	.L.str.5, 42

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"spin: Key is %x, Len is %x\n"
	.size	.L.str.6, 28

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"spin: crc out of bounds, giving up\n"
	.size	.L.str.7, 36

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"spin: key out of bounds, giving up\n"
	.size	.L.str.8, 36

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"spin: Key32 is %x - XORbitmap is %x\n"
	.size	.L.str.9, 37

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"spin: Decrypting sects (xor)\n"
	.size	.L.str.10, 30

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"spin: sect %d out of file, giving up\n"
	.size	.L.str.11, 38

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"spin: done\n"
	.size	.L.str.12, 12

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"spin: POLY1 len is %x\n"
	.size	.L.str.13, 23

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"spin: poly1 out of bounds\n"
	.size	.L.str.14, 27

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"spin: cannot exec poly1\n"
	.size	.L.str.15, 25

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"spin: POLYbitmap is %x - decrypting sects (poly)\n"
	.size	.L.str.16, 50

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"spin: poly1 emucode is out of file?\n"
	.size	.L.str.17, 37

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"spin: cannot exec section\n"
	.size	.L.str.18, 27

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"spin: Compression bitmap is %x\n"
	.size	.L.str.19, 32

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"spin: section %d size exceeded (%u, %lu)\n"
	.size	.L.str.20, 42

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"spin: malloc(%d) failed\n"
	.size	.L.str.21, 25

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"spin: Growing sect%d: was %x will be %x\n"
	.size	.L.str.22, 41

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"spin: Unpack failure\n"
	.size	.L.str.23, 22

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"spin: Not growing sect%d\n"
	.size	.L.str.24, 26

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"spin: decompression complete\n"
	.size	.L.str.25, 30

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"spin: Resources (sect%d) appear to be compressed\n\tuncompressed offset %x, len %x\n\tcompressed offset %x, len %x\n"
	.size	.L.str.26, 112

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"spin: Failed to grow resources, continuing anyway\n"
	.size	.L.str.27, 51

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"spin: Resources grown\n"
	.size	.L.str.28, 23

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"spin: No res?!\n"
	.size	.L.str.29, 16

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"spin: Cannot write unpacked file\n"
	.size	.L.str.30, 34

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"spin: free bitmap is %x\n"
	.size	.L.str.31, 25

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"spin: bogus opcode %x\n"
	.size	.L.str.32, 23

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"spin: bad emucode\n"
	.size	.L.str.33, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
