	.text
	.file	"strings.bc"
	.globl	string_StringIsNumber
	.p2align	4, 0x90
	.type	string_StringIsNumber,@function
string_StringIsNumber:                  # @string_StringIsNumber
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#1:
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB0_6
# BB#2:                                 # %.lr.ph.preheader
	incq	%rdi
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addb	$-48, %cl
	cmpb	$10, %cl
	jae	.LBB0_6
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movzbl	(%rdi), %ecx
	incq	%rdi
	testb	%cl, %cl
	jne	.LBB0_3
# BB#5:
	movl	$1, %eax
.LBB0_6:                                # %.loopexit
	retq
.Lfunc_end0:
	.size	string_StringIsNumber, .Lfunc_end0-string_StringIsNumber
	.cfi_endproc

	.globl	string_StringCopy
	.p2align	4, 0x90
	.type	string_StringCopy,@function
string_StringCopy:                      # @string_StringCopy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	callq	strlen
	leal	1(%rax), %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	string_StringCopy, .Lfunc_end1-string_StringCopy
	.cfi_endproc

	.globl	string_StringFree
	.p2align	4, 0x90
	.type	string_StringFree,@function
string_StringFree:                      # @string_StringFree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	strlen
	movq	%rax, %rcx
	incq	%rcx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB2_1
# BB#6:                                 # %memory_Free.exit
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rbx, (%rax)
	popq	%rbx
	retq
.LBB2_1:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rbx, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %edi
	cmovneq	%rdx, %rdi
	movq	%r8, (%rdi)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_3
# BB#2:
	negq	%rax
	movq	-16(%rbx,%rax), %rax
	movq	%rax, (%rcx)
.LBB2_3:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB2_5
# BB#4:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB2_5:
	addq	$-16, %rbx
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end2:
	.size	string_StringFree, .Lfunc_end2-string_StringFree
	.cfi_endproc

	.globl	string_IntToString
	.p2align	4, 0x90
	.type	string_IntToString,@function
string_IntToString:                     # @string_IntToString
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	movl	$2, %ebx
	cmpl	$9, %ebp
	movl	%ebp, %eax
	jg	.LBB3_3
# BB#1:
	movl	$2, %edi
	testl	%ebp, %ebp
	jns	.LBB3_4
# BB#2:
	movl	%ebp, %eax
	negl	%eax
	movl	$3, %ebx
.LBB3_3:                                # %.sink.split
	cvtsi2sdl	%eax, %xmm0
	callq	log10
	cvttsd2si	%xmm0, %rdi
	addl	%ebx, %edi
.LBB3_4:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	memory_Malloc
	movq	%rax, %rbx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end3:
	.size	string_IntToString, .Lfunc_end3-string_IntToString
	.cfi_endproc

	.globl	string_StringToInt
	.p2align	4, 0x90
	.type	string_StringToInt,@function
string_StringToInt:                     # @string_StringToInt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %ebp
	movq	%rdi, %r14
	movq	$1, 8(%rsp)
	leaq	8(%rsp), %rsi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rcx
	subq	$-2147483648, %rcx      # imm = 0x80000000
	shrq	$32, %rcx
	jne	.LBB4_3
# BB#1:
	movq	8(%rsp), %rcx
	cmpb	$0, (%rcx)
	je	.LBB4_2
.LBB4_3:
	movl	$0, (%rbx)
	xorl	%eax, %eax
	testl	%ebp, %ebp
	jne	.LBB4_5
.LBB4_4:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB4_2:
	movl	%eax, (%rbx)
	movl	$1, %eax
	jmp	.LBB4_4
.LBB4_5:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	misc_UserErrorReport
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	string_StringToInt, .Lfunc_end4-string_StringToInt
	.cfi_endproc

	.globl	string_Conc
	.p2align	4, 0x90
	.type	string_Conc,@function
string_Conc:                            # @string_Conc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -40
.Lcfi25:
	.cfi_offset %r12, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	callq	strlen
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	strlen
	leal	1(%r15,%rax), %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	strcat                  # TAILCALL
.Lfunc_end5:
	.size	string_Conc, .Lfunc_end5-string_Conc
	.cfi_endproc

	.globl	string_Nconc
	.p2align	4, 0x90
	.type	string_Nconc,@function
string_Nconc:                           # @string_Nconc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 48
.Lcfi33:
	.cfi_offset %rbx, -40
.Lcfi34:
	.cfi_offset %r12, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	callq	strlen
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	strlen
	leal	1(%r15,%rax), %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	strcpy
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcat
	movq	%rax, %rbx
	movq	%r12, %rdi
	callq	string_StringFree
	movq	%r14, %rdi
	callq	string_StringFree
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	string_Nconc, .Lfunc_end6-string_Nconc
	.cfi_endproc

	.globl	string_EmptyString
	.p2align	4, 0x90
	.type	string_EmptyString,@function
string_EmptyString:                     # @string_EmptyString
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 16
	movl	$1, %edi
	callq	memory_Malloc
	movb	$0, (%rax)
	popq	%rcx
	retq
.Lfunc_end7:
	.size	string_EmptyString, .Lfunc_end7-string_EmptyString
	.cfi_endproc

	.globl	string_Prefix
	.p2align	4, 0x90
	.type	string_Prefix,@function
string_Prefix:                          # @string_Prefix
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	leal	1(%r15), %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movslq	%r15d, %r15
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	strncpy
	movb	$0, (%rbx,%r15)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	string_Prefix, .Lfunc_end8-string_Prefix
	.cfi_endproc

	.globl	string_Suffix
	.p2align	4, 0x90
	.type	string_Suffix,@function
string_Suffix:                          # @string_Suffix
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	callq	strlen
	movl	$1, %edi
	cmpl	%r14d, %eax
	jle	.LBB9_1
# BB#2:
	subl	%r14d, %edi
	addl	%eax, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movslq	%r14d, %rax
	addq	%rax, %rbx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	jmp	.LBB9_3
.LBB9_1:
	callq	memory_Malloc
	movq	%rax, %rbp
	movb	$0, (%rbp)
.LBB9_3:
	movq	%rbp, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end9:
	.size	string_Suffix, .Lfunc_end9-string_Suffix
	.cfi_endproc

	.globl	string_Tokens
	.p2align	4, 0x90
	.type	string_Tokens,@function
string_Tokens:                          # @string_Tokens
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 80
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	callq	strlen
	leaq	-1(%r15,%rax), %r14
	cmpq	%r15, %r14
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	jae	.LBB10_2
# BB#1:
	xorl	%ebx, %ebx
	jmp	.LBB10_14
.LBB10_2:                               # %.preheader.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB10_3:                               # %.preheader.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_4 Depth 2
                                        #       Child Loop BB10_8 Depth 3
                                        #     Child Loop BB10_11 Depth 2
	movq	%r14, %rbp
.LBB10_4:                               # %.preheader
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_8 Depth 3
	cmpq	%r15, %rbp
	jb	.LBB10_5
# BB#7:                                 # %.lr.ph53
                                        #   in Loop: Header=BB10_4 Depth=2
	callq	__ctype_b_loc
	movq	(%rax), %rax
	leaq	-1(%rbp), %rcx
	.p2align	4, 0x90
.LBB10_8:                               #   Parent Loop BB10_3 Depth=1
                                        #     Parent Loop BB10_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsbq	(%rbp), %rdx
	testb	$32, 1(%rax,%rdx,2)
	je	.LBB10_10
# BB#9:                                 #   in Loop: Header=BB10_8 Depth=3
	decq	%rbp
	decq	%rcx
	cmpq	%r15, %rbp
	jae	.LBB10_8
.LBB10_5:                               # %.critedge46.loopexit
                                        #   in Loop: Header=BB10_4 Depth=2
	cmpq	%r15, %rbp
	jae	.LBB10_4
	jmp	.LBB10_6
	.p2align	4, 0x90
.LBB10_10:                              # %.critedge45.loopexit
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	%r12, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_11:                              # %.critedge45
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %r14
	cmpq	%r15, %r14
	jb	.LBB10_13
# BB#12:                                #   in Loop: Header=BB10_11 Depth=2
	movsbq	(%r14), %rdx
	leaq	-1(%r14), %rcx
	testb	$32, 1(%rax,%rdx,2)
	je	.LBB10_11
.LBB10_13:                              # %.critedge
                                        #   in Loop: Header=BB10_3 Depth=1
	movb	1(%rbp), %r13b
	movb	$0, 1(%rbp)
	leaq	1(%r14), %rbx
	movq	%rbx, %rdi
	callq	strlen
	leal	1(%rax), %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r12, 8(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movb	%r13b, 1(%rbp)
	cmpq	%r15, %r14
	movq	%rbx, %r12
	jae	.LBB10_3
	jmp	.LBB10_14
.LBB10_6:
	movq	%r12, %rbx
.LBB10_14:                              # %.critedge46.outer._crit_edge
	movq	%rbx, %rdi
	callq	list_Length
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	2(%rax), %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ecx, (%rbp)
	leal	16(,%rax,8), %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movl	$6, %edi
	callq	memory_Malloc
	movw	$83, 4(%rax)
	movl	$1396789331, (%rax)     # imm = 0x53415053
	movq	%rax, (%r15)
	testq	%rbx, %rbx
	je	.LBB10_15
# BB#16:                                # %.lr.ph.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB10_17:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rcx
	movl	%eax, %edx
	movq	%rcx, (%r15,%rdx,8)
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	incl	%eax
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB10_17
# BB#18:                                # %._crit_edge.loopexit
	movl	%eax, %eax
	jmp	.LBB10_19
.LBB10_15:
	movl	$1, %eax
.LBB10_19:                              # %._crit_edge
	movq	$0, (%r15,%rax,8)
	decl	(%rbp)
	movq	%r15, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	string_Tokens, .Lfunc_end10-string_Tokens
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\nString isn't a number or number to large: %s\n"
	.size	.L.str.1, 47

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SPASS"
	.size	.L.str.2, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
