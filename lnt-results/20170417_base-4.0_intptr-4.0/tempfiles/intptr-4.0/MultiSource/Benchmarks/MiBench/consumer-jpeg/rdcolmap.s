	.text
	.file	"rdcolmap.bc"
	.globl	read_color_map
	.p2align	4, 0x90
	.type	read_color_map,@function
read_color_map:                         # @read_color_map
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r12
	movq	8(%r12), %rax
	movl	$1, %esi
	movl	$256, %edx              # imm = 0x100
	movl	$3, %ecx
	callq	*16(%rax)
	movq	%rax, 152(%r12)
	movl	$0, 148(%r12)
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$80, %eax
	movq	%r12, (%rsp)            # 8-byte Spill
	je	.LBB0_46
# BB#1:
	cmpl	$71, %eax
	jne	.LBB0_185
# BB#2:
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.LBB0_4
# BB#3:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_4:
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$-1, %r14d
	jne	.LBB0_6
# BB#5:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_6:
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB0_8
# BB#7:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_8:
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB0_10
# BB#9:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_10:
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB0_12
# BB#11:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_12:
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB0_14
# BB#13:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_14:
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB0_16
# BB#15:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_16:
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB0_18
# BB#17:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_18:
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB0_20
# BB#19:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_20:
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB0_22
# BB#21:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_22:
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB0_24
# BB#23:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_24:
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	jne	.LBB0_26
# BB#25:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_26:
	cmpl	$73, %ebx
	jne	.LBB0_27
# BB#184:
	cmpl	$70, %r14d
	je	.LBB0_28
.LBB0_27:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_28:
	testb	%bpl, %bpl
	js	.LBB0_30
# BB#29:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_30:
	andb	$7, %bpl
	movl	$2, %eax
	movl	%ebp, %ecx
	shll	%cl, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	jle	.LBB0_183
# BB#31:                                # %.lr.ph.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_32:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_38 Depth 2
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%r13, %rdi
	callq	_IO_getc
	cmpl	$-1, %ebx
	je	.LBB0_35
# BB#33:                                # %.lr.ph.i
                                        #   in Loop: Header=BB0_32 Depth=1
	cmpl	$-1, %ebp
	je	.LBB0_35
# BB#34:                                # %.lr.ph.i
                                        #   in Loop: Header=BB0_32 Depth=1
	cmpl	$-1, %eax
	jne	.LBB0_36
.LBB0_35:                               #   in Loop: Header=BB0_32 Depth=1
	movq	(%r12), %rcx
	movl	$1038, 40(%rcx)         # imm = 0x40E
	movq	%r12, %rdi
	movl	%eax, %r14d
	callq	*(%rcx)
	movl	%r14d, %eax
.LBB0_36:                               #   in Loop: Header=BB0_32 Depth=1
	movq	152(%r12), %rcx
	movq	(%rcx), %r15
	movq	%r12, %rdx
	movq	8(%rcx), %r12
	movq	16(%rcx), %rsi
	movslq	148(%rdx), %r14
	testq	%r14, %r14
	jle	.LBB0_44
# BB#37:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_32 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_38:                               # %.lr.ph.i.i
                                        #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r15,%rdx), %ecx
	cmpl	%ebx, %ecx
	jne	.LBB0_41
# BB#39:                                #   in Loop: Header=BB0_38 Depth=2
	movzbl	(%r12,%rdx), %ecx
	cmpl	%ebp, %ecx
	jne	.LBB0_41
# BB#40:                                #   in Loop: Header=BB0_38 Depth=2
	movzbl	(%rsi,%rdx), %ecx
	cmpl	%eax, %ecx
	je	.LBB0_45
.LBB0_41:                               #   in Loop: Header=BB0_38 Depth=2
	incq	%rdx
	cmpq	%r14, %rdx
	jl	.LBB0_38
# BB#42:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_32 Depth=1
	cmpl	$256, %r14d             # imm = 0x100
	movq	(%rsp), %rdx            # 8-byte Reload
	jl	.LBB0_44
# BB#43:                                #   in Loop: Header=BB0_32 Depth=1
	movq	(%rdx), %rcx
	movl	$56, 40(%rcx)
	movl	$256, 44(%rcx)          # imm = 0x100
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	callq	*(%rcx)
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	16(%rsp), %eax          # 4-byte Reload
.LBB0_44:                               # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movb	%bl, (%r15,%r14)
	movb	%bpl, (%r12,%r14)
	movb	%al, (%rsi,%r14)
	incl	148(%rdx)
.LBB0_45:                               # %add_map_entry.exit.i
                                        #   in Loop: Header=BB0_32 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	movq	(%rsp), %r12            # 8-byte Reload
	jne	.LBB0_32
	jmp	.LBB0_183
.LBB0_46:
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	movabsq	$8589954048, %r15       # imm = 0x200004C00
	.p2align	4, 0x90
.LBB0_47:                               # %.critedge.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_48 Depth 2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$35, %ebp
	jne	.LBB0_50
	.p2align	4, 0x90
.LBB0_48:                               # %.preheader.i.i.i
                                        #   Parent Loop BB0_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB0_50
# BB#49:                                # %.preheader.i.i.i
                                        #   in Loop: Header=BB0_48 Depth=2
	cmpl	$-1, %ebp
	jne	.LBB0_48
.LBB0_50:                               # %pbm_getc.exit.i.i
                                        #   in Loop: Header=BB0_47 Depth=1
	leal	1(%rbp), %eax
	cmpl	$33, %eax
	ja	.LBB0_54
# BB#51:                                # %pbm_getc.exit.i.i
                                        #   in Loop: Header=BB0_47 Depth=1
	btq	%rax, %r15
	jb	.LBB0_47
# BB#52:                                # %pbm_getc.exit.i.i
	testq	%rax, %rax
	jne	.LBB0_54
# BB#53:                                # %.thread.i.i
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	movl	$-49, %ebp
	jmp	.LBB0_55
.LBB0_54:
	addl	$-48, %ebp
	cmpl	$10, %ebp
	jb	.LBB0_56
.LBB0_55:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	jmp	.LBB0_56
	.p2align	4, 0x90
.LBB0_59:                               # %pbm_getc.exit33.i.i
                                        #   in Loop: Header=BB0_56 Depth=1
	addl	$-48, %eax
	cmpl	$10, %eax
	jae	.LBB0_61
# BB#60:                                #   in Loop: Header=BB0_56 Depth=1
	leal	(%rbp,%rbp,4), %ecx
	leal	(%rax,%rcx,2), %ebp
.LBB0_56:                               # %.preheader.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_57 Depth 2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB0_59
	.p2align	4, 0x90
.LBB0_57:                               # %.preheader.i31.i.i
                                        #   Parent Loop BB0_56 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB0_59
# BB#58:                                # %.preheader.i31.i.i
                                        #   in Loop: Header=BB0_57 Depth=2
	cmpl	$-1, %eax
	jne	.LBB0_57
	jmp	.LBB0_59
	.p2align	4, 0x90
.LBB0_61:                               # %.critedge.i81.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_62 Depth 2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB0_64
	.p2align	4, 0x90
.LBB0_62:                               # %.preheader.i.i82.i
                                        #   Parent Loop BB0_61 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB0_64
# BB#63:                                # %.preheader.i.i82.i
                                        #   in Loop: Header=BB0_62 Depth=2
	cmpl	$-1, %eax
	jne	.LBB0_62
.LBB0_64:                               # %pbm_getc.exit.i84.i
                                        #   in Loop: Header=BB0_61 Depth=1
	movq	%rax, %rcx
	leal	1(%rax), %eax
	cmpl	$33, %eax
	ja	.LBB0_68
# BB#65:                                # %pbm_getc.exit.i84.i
                                        #   in Loop: Header=BB0_61 Depth=1
	btq	%rax, %r15
	jb	.LBB0_61
# BB#66:                                # %pbm_getc.exit.i84.i
	testq	%rax, %rax
	jne	.LBB0_68
# BB#67:                                # %.thread.i85.i
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	movl	$-49, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB0_69
.LBB0_68:
	addl	$-48, %ecx
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpl	$10, %ecx
	jb	.LBB0_70
.LBB0_69:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	jmp	.LBB0_70
	.p2align	4, 0x90
.LBB0_73:                               # %pbm_getc.exit33.i98.i
                                        #   in Loop: Header=BB0_70 Depth=1
	addl	$-48, %eax
	cmpl	$10, %eax
	jae	.LBB0_75
# BB#74:                                #   in Loop: Header=BB0_70 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rcx,4), %ecx
	leal	(%rax,%rcx,2), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB0_70:                               # %.preheader.i94.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_71 Depth 2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB0_73
	.p2align	4, 0x90
.LBB0_71:                               # %.preheader.i31.i95.i
                                        #   Parent Loop BB0_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB0_73
# BB#72:                                # %.preheader.i31.i95.i
                                        #   in Loop: Header=BB0_71 Depth=2
	cmpl	$-1, %eax
	jne	.LBB0_71
	jmp	.LBB0_73
	.p2align	4, 0x90
.LBB0_75:                               # %.critedge.i100.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_76 Depth 2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB0_78
	.p2align	4, 0x90
.LBB0_76:                               # %.preheader.i.i101.i
                                        #   Parent Loop BB0_75 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$10, %ebx
	je	.LBB0_78
# BB#77:                                # %.preheader.i.i101.i
                                        #   in Loop: Header=BB0_76 Depth=2
	cmpl	$-1, %ebx
	jne	.LBB0_76
.LBB0_78:                               # %pbm_getc.exit.i103.i
                                        #   in Loop: Header=BB0_75 Depth=1
	leal	1(%rbx), %eax
	cmpl	$33, %eax
	ja	.LBB0_82
# BB#79:                                # %pbm_getc.exit.i103.i
                                        #   in Loop: Header=BB0_75 Depth=1
	btq	%rax, %r15
	jb	.LBB0_75
# BB#80:                                # %pbm_getc.exit.i103.i
	testq	%rax, %rax
	jne	.LBB0_82
# BB#81:                                # %.thread.i104.i
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	movl	$-49, %ebx
	jmp	.LBB0_83
.LBB0_82:
	addl	$-48, %ebx
	cmpl	$10, %ebx
	jb	.LBB0_84
.LBB0_83:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	jmp	.LBB0_84
	.p2align	4, 0x90
.LBB0_87:                               # %pbm_getc.exit33.i117.i
                                        #   in Loop: Header=BB0_84 Depth=1
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB0_89
# BB#88:                                #   in Loop: Header=BB0_84 Depth=1
	leal	(%rbx,%rbx,4), %ecx
	leal	(%rax,%rcx,2), %ebx
.LBB0_84:                               # %.preheader.i113.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_85 Depth 2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB0_87
	.p2align	4, 0x90
.LBB0_85:                               # %.preheader.i31.i114.i
                                        #   Parent Loop BB0_84 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB0_87
# BB#86:                                # %.preheader.i31.i114.i
                                        #   in Loop: Header=BB0_85 Depth=2
	cmpl	$-1, %eax
	jne	.LBB0_85
	jmp	.LBB0_87
.LBB0_89:                               # %read_pbm_integer.exit118.i
	testl	%ebp, %ebp
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB0_92
# BB#90:                                # %read_pbm_integer.exit118.i
	testl	%eax, %eax
	je	.LBB0_92
# BB#91:                                # %read_pbm_integer.exit118.i
	testl	%ebx, %ebx
	jne	.LBB0_93
.LBB0_92:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB0_93:
	cmpl	$255, %ebx
	je	.LBB0_95
# BB#94:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
.LBB0_95:
	cmpl	$51, %r14d
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	je	.LBB0_125
# BB#96:
	cmpl	$54, %r14d
	jne	.LBB0_185
# BB#97:                                # %.preheader194.i
	testl	%eax, %eax
	je	.LBB0_183
# BB#98:                                # %.preheader193.lr.ph.i
	testl	%ebp, %ebp
	je	.LBB0_183
# BB#99:                                # %.preheader193.us.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_100:                              # %.preheader193.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_101 Depth 2
                                        #       Child Loop BB0_102 Depth 3
                                        #       Child Loop BB0_105 Depth 3
                                        #       Child Loop BB0_108 Depth 3
                                        #       Child Loop BB0_116 Depth 3
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_101:                              #   Parent Loop BB0_100 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_102 Depth 3
                                        #       Child Loop BB0_105 Depth 3
                                        #       Child Loop BB0_108 Depth 3
                                        #       Child Loop BB0_116 Depth 3
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$35, %r14d
	jne	.LBB0_104
	.p2align	4, 0x90
.LBB0_102:                              # %.preheader.i177.us.i
                                        #   Parent Loop BB0_100 Depth=1
                                        #     Parent Loop BB0_101 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$10, %r14d
	je	.LBB0_104
# BB#103:                               # %.preheader.i177.us.i
                                        #   in Loop: Header=BB0_102 Depth=3
	cmpl	$-1, %r14d
	jne	.LBB0_102
.LBB0_104:                              # %pbm_getc.exit.us.i
                                        #   in Loop: Header=BB0_101 Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$35, %ebp
	movq	(%rsp), %r12            # 8-byte Reload
	jne	.LBB0_107
	.p2align	4, 0x90
.LBB0_105:                              # %.preheader.i179.us.i
                                        #   Parent Loop BB0_100 Depth=1
                                        #     Parent Loop BB0_101 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB0_107
# BB#106:                               # %.preheader.i179.us.i
                                        #   in Loop: Header=BB0_105 Depth=3
	cmpl	$-1, %ebp
	jne	.LBB0_105
.LBB0_107:                              # %pbm_getc.exit181.us.i
                                        #   in Loop: Header=BB0_101 Depth=2
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB0_110
	.p2align	4, 0x90
.LBB0_108:                              # %.preheader.i182.us.i
                                        #   Parent Loop BB0_100 Depth=1
                                        #     Parent Loop BB0_101 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$10, %ebx
	je	.LBB0_110
# BB#109:                               # %.preheader.i182.us.i
                                        #   in Loop: Header=BB0_108 Depth=3
	cmpl	$-1, %ebx
	jne	.LBB0_108
.LBB0_110:                              # %pbm_getc.exit184.us.i
                                        #   in Loop: Header=BB0_101 Depth=2
	cmpl	$-1, %r14d
	je	.LBB0_113
# BB#111:                               # %pbm_getc.exit184.us.i
                                        #   in Loop: Header=BB0_101 Depth=2
	cmpl	$-1, %ebp
	je	.LBB0_113
# BB#112:                               # %pbm_getc.exit184.us.i
                                        #   in Loop: Header=BB0_101 Depth=2
	cmpl	$-1, %ebx
	jne	.LBB0_114
.LBB0_113:                              #   in Loop: Header=BB0_101 Depth=2
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
.LBB0_114:                              #   in Loop: Header=BB0_101 Depth=2
	movq	152(%r12), %rax
	movq	(%rax), %r15
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	movq	%r12, %rcx
	movslq	148(%rcx), %r12
	testq	%r12, %r12
	jle	.LBB0_122
# BB#115:                               # %.lr.ph.i187.us.i.preheader
                                        #   in Loop: Header=BB0_101 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_116:                              # %.lr.ph.i187.us.i
                                        #   Parent Loop BB0_100 Depth=1
                                        #     Parent Loop BB0_101 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r15,%rax), %ecx
	cmpl	%r14d, %ecx
	jne	.LBB0_119
# BB#117:                               #   in Loop: Header=BB0_116 Depth=3
	movzbl	(%rsi,%rax), %ecx
	cmpl	%ebp, %ecx
	jne	.LBB0_119
# BB#118:                               #   in Loop: Header=BB0_116 Depth=3
	movzbl	(%rdx,%rax), %ecx
	cmpl	%ebx, %ecx
	je	.LBB0_123
.LBB0_119:                              #   in Loop: Header=BB0_116 Depth=3
	incq	%rax
	cmpq	%r12, %rax
	jl	.LBB0_116
# BB#120:                               # %._crit_edge.i189.us.i
                                        #   in Loop: Header=BB0_101 Depth=2
	cmpl	$256, %r12d             # imm = 0x100
	movq	(%rsp), %rcx            # 8-byte Reload
	jl	.LBB0_122
# BB#121:                               #   in Loop: Header=BB0_101 Depth=2
	movq	(%rcx), %rax
	movl	$56, 40(%rax)
	movl	$256, 44(%rax)          # imm = 0x100
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	callq	*(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB0_122:                              # %._crit_edge.thread.i190.us.i
                                        #   in Loop: Header=BB0_101 Depth=2
	movb	%r14b, (%r15,%r12)
	movb	%bpl, (%rsi,%r12)
	movb	%bl, (%rdx,%r12)
	incl	148(%rcx)
.LBB0_123:                              # %add_map_entry.exit191.us.i
                                        #   in Loop: Header=BB0_101 Depth=2
	movl	12(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB0_101
# BB#124:                               # %._crit_edge222.us.i
                                        #   in Loop: Header=BB0_100 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB0_100
	jmp	.LBB0_183
.LBB0_185:
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.LBB0_125:                              # %.preheader192.i
	testl	%eax, %eax
	je	.LBB0_183
# BB#126:                               # %.preheader.lr.ph.i
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_127:                              # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_129 Depth 2
                                        #       Child Loop BB0_130 Depth 3
                                        #         Child Loop BB0_131 Depth 4
                                        #       Child Loop BB0_139 Depth 3
                                        #         Child Loop BB0_140 Depth 4
                                        #       Child Loop BB0_144 Depth 3
                                        #         Child Loop BB0_145 Depth 4
                                        #       Child Loop BB0_153 Depth 3
                                        #         Child Loop BB0_154 Depth 4
                                        #       Child Loop BB0_158 Depth 3
                                        #         Child Loop BB0_159 Depth 4
                                        #       Child Loop BB0_167 Depth 3
                                        #         Child Loop BB0_168 Depth 4
                                        #       Child Loop BB0_174 Depth 3
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	je	.LBB0_182
# BB#128:                               # %.critedge.i119.preheader.i.preheader
                                        #   in Loop: Header=BB0_127 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_129:                              # %.critedge.i119.preheader.i
                                        #   Parent Loop BB0_127 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_130 Depth 3
                                        #         Child Loop BB0_131 Depth 4
                                        #       Child Loop BB0_139 Depth 3
                                        #         Child Loop BB0_140 Depth 4
                                        #       Child Loop BB0_144 Depth 3
                                        #         Child Loop BB0_145 Depth 4
                                        #       Child Loop BB0_153 Depth 3
                                        #         Child Loop BB0_154 Depth 4
                                        #       Child Loop BB0_158 Depth 3
                                        #         Child Loop BB0_159 Depth 4
                                        #       Child Loop BB0_167 Depth 3
                                        #         Child Loop BB0_168 Depth 4
                                        #       Child Loop BB0_174 Depth 3
	movl	%eax, 12(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB0_130:                              # %.critedge.i119.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_131 Depth 4
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$35, %ebp
	jne	.LBB0_133
	.p2align	4, 0x90
.LBB0_131:                              # %.preheader.i.i120.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        #       Parent Loop BB0_130 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	cmpl	$10, %ebp
	je	.LBB0_133
# BB#132:                               # %.preheader.i.i120.i
                                        #   in Loop: Header=BB0_131 Depth=4
	cmpl	$-1, %ebp
	jne	.LBB0_131
.LBB0_133:                              # %pbm_getc.exit.i122.i
                                        #   in Loop: Header=BB0_130 Depth=3
	leal	1(%rbp), %eax
	cmpl	$33, %eax
	ja	.LBB0_137
# BB#134:                               # %pbm_getc.exit.i122.i
                                        #   in Loop: Header=BB0_130 Depth=3
	btq	%rax, %r15
	jb	.LBB0_130
# BB#135:                               # %pbm_getc.exit.i122.i
                                        #   in Loop: Header=BB0_129 Depth=2
	testq	%rax, %rax
	jne	.LBB0_137
# BB#136:                               # %.thread.i123.i
                                        #   in Loop: Header=BB0_129 Depth=2
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	movl	$-49, %ebp
	jmp	.LBB0_138
	.p2align	4, 0x90
.LBB0_137:                              #   in Loop: Header=BB0_129 Depth=2
	addl	$-48, %ebp
	cmpl	$10, %ebp
	jb	.LBB0_139
.LBB0_138:                              # %._crit_edge.i127.i
                                        #   in Loop: Header=BB0_129 Depth=2
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	jmp	.LBB0_139
	.p2align	4, 0x90
.LBB0_142:                              # %pbm_getc.exit33.i136.i
                                        #   in Loop: Header=BB0_139 Depth=3
	addl	$-48, %eax
	cmpl	$10, %eax
	jae	.LBB0_144
# BB#143:                               #   in Loop: Header=BB0_139 Depth=3
	leal	(%rbp,%rbp,4), %ecx
	leal	(%rax,%rcx,2), %ebp
.LBB0_139:                              # %.preheader.i132.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_140 Depth 4
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB0_142
	.p2align	4, 0x90
.LBB0_140:                              # %.preheader.i31.i133.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        #       Parent Loop BB0_139 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB0_142
# BB#141:                               # %.preheader.i31.i133.i
                                        #   in Loop: Header=BB0_140 Depth=4
	cmpl	$-1, %eax
	jne	.LBB0_140
	jmp	.LBB0_142
	.p2align	4, 0x90
.LBB0_144:                              # %.critedge.i138.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_145 Depth 4
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$35, %ebx
	jne	.LBB0_147
	.p2align	4, 0x90
.LBB0_145:                              # %.preheader.i.i139.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        #       Parent Loop BB0_144 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$10, %ebx
	je	.LBB0_147
# BB#146:                               # %.preheader.i.i139.i
                                        #   in Loop: Header=BB0_145 Depth=4
	cmpl	$-1, %ebx
	jne	.LBB0_145
.LBB0_147:                              # %pbm_getc.exit.i141.i
                                        #   in Loop: Header=BB0_144 Depth=3
	leal	1(%rbx), %eax
	cmpl	$33, %eax
	ja	.LBB0_151
# BB#148:                               # %pbm_getc.exit.i141.i
                                        #   in Loop: Header=BB0_144 Depth=3
	btq	%rax, %r15
	jb	.LBB0_144
# BB#149:                               # %pbm_getc.exit.i141.i
                                        #   in Loop: Header=BB0_129 Depth=2
	testq	%rax, %rax
	jne	.LBB0_151
# BB#150:                               # %.thread.i142.i
                                        #   in Loop: Header=BB0_129 Depth=2
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	movl	$-49, %ebx
	jmp	.LBB0_152
	.p2align	4, 0x90
.LBB0_151:                              #   in Loop: Header=BB0_129 Depth=2
	addl	$-48, %ebx
	cmpl	$10, %ebx
	jb	.LBB0_153
.LBB0_152:                              # %._crit_edge.i146.i
                                        #   in Loop: Header=BB0_129 Depth=2
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	jmp	.LBB0_153
	.p2align	4, 0x90
.LBB0_156:                              # %pbm_getc.exit33.i155.i
                                        #   in Loop: Header=BB0_153 Depth=3
	addl	$-48, %eax
	cmpl	$10, %eax
	jae	.LBB0_158
# BB#157:                               #   in Loop: Header=BB0_153 Depth=3
	leal	(%rbx,%rbx,4), %ecx
	leal	(%rax,%rcx,2), %ebx
.LBB0_153:                              # %.preheader.i151.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_154 Depth 4
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB0_156
	.p2align	4, 0x90
.LBB0_154:                              # %.preheader.i31.i152.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        #       Parent Loop BB0_153 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB0_156
# BB#155:                               # %.preheader.i31.i152.i
                                        #   in Loop: Header=BB0_154 Depth=4
	cmpl	$-1, %eax
	jne	.LBB0_154
	jmp	.LBB0_156
	.p2align	4, 0x90
.LBB0_158:                              # %.critedge.i157.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_159 Depth 4
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$35, %r14d
	jne	.LBB0_161
	.p2align	4, 0x90
.LBB0_159:                              # %.preheader.i.i158.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        #       Parent Loop BB0_158 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r13, %rdi
	callq	_IO_getc
	movl	%eax, %r14d
	cmpl	$10, %r14d
	je	.LBB0_161
# BB#160:                               # %.preheader.i.i158.i
                                        #   in Loop: Header=BB0_159 Depth=4
	cmpl	$-1, %r14d
	jne	.LBB0_159
.LBB0_161:                              # %pbm_getc.exit.i160.i
                                        #   in Loop: Header=BB0_158 Depth=3
	leal	1(%r14), %eax
	cmpl	$33, %eax
	ja	.LBB0_165
# BB#162:                               # %pbm_getc.exit.i160.i
                                        #   in Loop: Header=BB0_158 Depth=3
	btq	%rax, %r15
	jb	.LBB0_158
# BB#163:                               # %pbm_getc.exit.i160.i
                                        #   in Loop: Header=BB0_129 Depth=2
	testq	%rax, %rax
	jne	.LBB0_165
# BB#164:                               # %.thread.i161.i
                                        #   in Loop: Header=BB0_129 Depth=2
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	movl	$-49, %r14d
	jmp	.LBB0_166
	.p2align	4, 0x90
.LBB0_165:                              #   in Loop: Header=BB0_129 Depth=2
	addl	$-48, %r14d
	cmpl	$10, %r14d
	jb	.LBB0_167
.LBB0_166:                              # %._crit_edge.i165.i
                                        #   in Loop: Header=BB0_129 Depth=2
	movq	(%r12), %rax
	movl	$1038, 40(%rax)         # imm = 0x40E
	movq	%r12, %rdi
	callq	*(%rax)
	jmp	.LBB0_167
	.p2align	4, 0x90
.LBB0_170:                              # %pbm_getc.exit33.i174.i
                                        #   in Loop: Header=BB0_167 Depth=3
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB0_172
# BB#171:                               #   in Loop: Header=BB0_167 Depth=3
	leal	(%r14,%r14,4), %ecx
	leal	(%rax,%rcx,2), %r14d
.LBB0_167:                              # %.preheader.i170.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_168 Depth 4
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	jne	.LBB0_170
	.p2align	4, 0x90
.LBB0_168:                              # %.preheader.i31.i171.i
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        #       Parent Loop BB0_167 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%r13, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$10, %eax
	je	.LBB0_170
# BB#169:                               # %.preheader.i31.i171.i
                                        #   in Loop: Header=BB0_168 Depth=4
	cmpl	$-1, %eax
	jne	.LBB0_168
	jmp	.LBB0_170
	.p2align	4, 0x90
.LBB0_172:                              # %read_pbm_integer.exit175.i
                                        #   in Loop: Header=BB0_129 Depth=2
	movq	152(%r12), %rax
	movq	(%rax), %r15
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	movq	%r12, %rcx
	movslq	148(%rcx), %r12
	testq	%r12, %r12
	jle	.LBB0_180
# BB#173:                               # %.lr.ph.i.i16.preheader
                                        #   in Loop: Header=BB0_129 Depth=2
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_174:                              # %.lr.ph.i.i16
                                        #   Parent Loop BB0_127 Depth=1
                                        #     Parent Loop BB0_129 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%r15,%rax), %ecx
	cmpl	%ebp, %ecx
	jne	.LBB0_177
# BB#175:                               #   in Loop: Header=BB0_174 Depth=3
	movzbl	(%rsi,%rax), %ecx
	cmpl	%ebx, %ecx
	jne	.LBB0_177
# BB#176:                               #   in Loop: Header=BB0_174 Depth=3
	movzbl	(%rdx,%rax), %ecx
	cmpl	%r14d, %ecx
	je	.LBB0_181
.LBB0_177:                              #   in Loop: Header=BB0_174 Depth=3
	incq	%rax
	cmpq	%r12, %rax
	jl	.LBB0_174
# BB#178:                               # %._crit_edge.i176.i
                                        #   in Loop: Header=BB0_129 Depth=2
	cmpl	$256, %r12d             # imm = 0x100
	movq	(%rsp), %rcx            # 8-byte Reload
	jl	.LBB0_180
# BB#179:                               #   in Loop: Header=BB0_129 Depth=2
	movq	(%rcx), %rax
	movl	$56, 40(%rax)
	movl	$256, 44(%rax)          # imm = 0x100
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	callq	*(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
.LBB0_180:                              # %._crit_edge.thread.i.i18
                                        #   in Loop: Header=BB0_129 Depth=2
	movb	%bpl, (%r15,%r12)
	movb	%bl, (%rsi,%r12)
	movb	%r14b, (%rdx,%r12)
	incl	148(%rcx)
.LBB0_181:                              # %add_map_entry.exit.i20
                                        #   in Loop: Header=BB0_129 Depth=2
	movl	12(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movq	32(%rsp), %rbp          # 8-byte Reload
	cmpl	%ebp, %eax
	movq	(%rsp), %r12            # 8-byte Reload
	movabsq	$8589954048, %r15       # imm = 0x200004C00
	jne	.LBB0_129
.LBB0_182:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_127 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB0_127
.LBB0_183:                              # %read_ppm_map.exit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	read_color_map, .Lfunc_end0-read_color_map
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
