	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	callq	dealwithargs
	movl	%eax, %r14d
	xorl	%r13d, %r13d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movl	NumNodes(%rip), %esi
	movl	%r14d, %edi
	callq	MakeGraph
	movq	%rax, %r12
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	NumNodes(%rip), %r15d
	movl	$.Lstr.2, %edi
	callq	puts
	movq	(%r12), %rbp
	movq	8(%rbp), %rax
	movq	%rax, (%r12)
	movq	%rax, MyVertexList(%rip)
	movl	$.Lstr.3, %edi
	callq	puts
	cmpl	$1, %r14d
	je	.LBB0_3
# BB#1:                                 # %.lr.ph.i.preheader
	movl	$1, %ebx
	subl	%r14d, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movl	%r15d, %esi
	callq	Do_all_BlueRule
	movq	%rax, %rbp
	addl	%edx, %r13d
	incl	%ebx
	jne	.LBB0_2
.LBB0_3:                                # %ComputeMst.exit
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	Do_all_BlueRule,@function
Do_all_BlueRule:                        # @Do_all_BlueRule
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %r15
	cmpl	$2, %ebp
	jl	.LBB1_2
# BB#1:
	shrl	%ebp
	leal	(%rbp,%r14), %edx
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	Do_all_BlueRule
	movq	%rax, %rbx
	movl	%edx, %r12d
	movq	%r15, %rdi
	movl	%ebp, %esi
	movl	%r14d, %edx
	callq	Do_all_BlueRule
	movq	%rax, %r14
	movl	%edx, %r13d
	cmpl	%r13d, %r12d
	cmovlq	%rbx, %r14
	cmovlel	%r12d, %r13d
	jmp	.LBB1_20
.LBB1_2:
	movq	MyVertexList(%rip), %r14
	cmpq	%r15, %r14
	jne	.LBB1_4
# BB#3:
	movq	8(%r15), %r14
	movq	%r14, MyVertexList(%rip)
.LBB1_4:
	testq	%r14, %r14
	je	.LBB1_5
# BB#6:
	movl	(%r14), %r13d
	movq	16(%r14), %rsi
	movl	%r15d, %edi
	callq	HashLookup
	testl	%eax, %eax
	je	.LBB1_9
# BB#7:
	cmpl	%r13d, %eax
	jge	.LBB1_10
# BB#8:
	movl	%eax, (%r14)
	movl	%eax, %r13d
	jmp	.LBB1_10
.LBB1_5:
	movl	$999999, %r13d          # imm = 0xF423F
                                        # implicit-def: %R14
	jmp	.LBB1_20
.LBB1_9:
	movl	$.Lstr.5, %edi
	callq	puts
.LBB1_10:                               # %.preheader.i
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.LBB1_20
# BB#11:                                # %.lr.ph.preheader.i
	leaq	8(%r14), %rbx
	leaq	8(%r15), %rbp
.LBB1_12:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_13 Depth 2
	movq	%rbx, %rcx
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB1_13:                               #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r15, %rbx
	jne	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_13 Depth=2
	movq	(%rbp), %rax
	movq	%rax, (%rcx)
	movq	(%rbp), %rbx
	testq	%rbx, %rbx
	movq	%rbp, %rcx
	jne	.LBB1_13
	jmp	.LBB1_20
.LBB1_15:                               #   in Loop: Header=BB1_12 Depth=1
	movq	16(%rbx), %rsi
	movl	(%rbx), %r12d
	movl	%r15d, %edi
	callq	HashLookup
	testl	%eax, %eax
	je	.LBB1_18
# BB#16:                                #   in Loop: Header=BB1_12 Depth=1
	cmpl	%r12d, %eax
	jge	.LBB1_19
# BB#17:                                #   in Loop: Header=BB1_12 Depth=1
	movl	%eax, (%rbx)
	movl	%eax, %r12d
	jmp	.LBB1_19
.LBB1_18:                               #   in Loop: Header=BB1_12 Depth=1
	movl	$.Lstr.5, %edi
	callq	puts
.LBB1_19:                               # %.outer.i
                                        #   in Loop: Header=BB1_12 Depth=1
	cmpl	%r13d, %r12d
	cmovlel	%r12d, %r13d
	cmovlq	%rbx, %r14
	movq	8(%rbx), %rax
	addq	$8, %rbx
	testq	%rax, %rax
	jne	.LBB1_12
.LBB1_20:                               # %BlueRule.exit
	movq	%r14, %rax
	movl	%r13d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	Do_all_BlueRule, .Lfunc_end1-Do_all_BlueRule
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Making graph of size %d\n"
	.size	.L.str, 25

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"MST has cost %d\n"
	.size	.L.str.3, 17

	.type	MyVertexList,@object    # @MyVertexList
	.local	MyVertexList
	.comm	MyVertexList,8,8
	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"Graph completed"
	.size	.Lstr, 16

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"About to compute mst "
	.size	.Lstr.1, 22

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.2:
	.asciz	"Compute phase 1"
	.size	.Lstr.2, 16

	.type	.Lstr.3,@object         # @str.3
.Lstr.3:
	.asciz	"Compute phase 2"
	.size	.Lstr.3, 16

	.type	.Lstr.5,@object         # @str.5
.Lstr.5:
	.asciz	"Not found"
	.size	.Lstr.5, 10


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
