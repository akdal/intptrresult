	.text
	.file	"LR0.bc"
	.globl	allocate_itemsets
	.p2align	4, 0x90
	.type	allocate_itemsets,@function
allocate_itemsets:                      # @allocate_itemsets
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	nsyms(%rip), %edi
	addl	%edi, %edi
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r15
	movq	ritem(%rip), %rax
	jmp	.LBB0_1
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	incl	%r14d
	incw	(%r15,%rcx,2)
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movswq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB0_4
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	addq	$2, %rax
	testw	%cx, %cx
	jle	.LBB0_1
	jmp	.LBB0_3
.LBB0_4:
	movl	nsyms(%rip), %edi
	shll	$3, %edi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, kernel_base(%rip)
	addl	%r14d, %r14d
	xorl	%eax, %eax
	movl	%r14d, %edi
	callq	mallocate
	movq	%rax, kernel_items(%rip)
	movslq	nsyms(%rip), %rdi
	testq	%rdi, %rdi
	jle	.LBB0_10
# BB#5:                                 # %.lr.ph
	movq	kernel_base(%rip), %rcx
	leaq	-1(%rdi), %r8
	movq	%rdi, %r9
	xorl	%edx, %edx
	andq	$3, %r9
	je	.LBB0_8
# BB#6:                                 # %.prol.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rbp
	leaq	(%rax,%rbp,2), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movswl	(%r15,%rdx,2), %esi
	addl	%esi, %ebp
	incq	%rdx
	cmpq	%rdx, %r9
	jne	.LBB0_7
.LBB0_8:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB0_10
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rsi
	leaq	(%rax,%rsi,2), %rbp
	movq	%rbp, (%rcx,%rdx,8)
	movswq	(%r15,%rdx,2), %rbp
	addq	%rsi, %rbp
	leaq	(%rax,%rbp,2), %rsi
	movq	%rsi, 8(%rcx,%rdx,8)
	movswq	2(%r15,%rdx,2), %rsi
	movslq	%ebp, %rbp
	addq	%rsi, %rbp
	leaq	(%rax,%rbp,2), %rsi
	movq	%rsi, 16(%rcx,%rdx,8)
	movswq	4(%r15,%rdx,2), %rsi
	movslq	%ebp, %rbx
	addq	%rsi, %rbx
	leaq	(%rax,%rbx,2), %rsi
	movq	%rsi, 24(%rcx,%rdx,8)
	movswl	6(%r15,%rdx,2), %ebp
	addl	%ebx, %ebp
	addq	$4, %rdx
	cmpq	%rdi, %rdx
	jl	.LBB0_9
.LBB0_10:                               # %._crit_edge
	movq	%r15, shift_symbol(%rip)
	shll	$3, %edi
	xorl	%eax, %eax
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	mallocate
	movq	%rax, kernel_end(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	allocate_itemsets, .Lfunc_end0-allocate_itemsets
	.cfi_endproc

	.globl	allocate_storage
	.p2align	4, 0x90
	.type	allocate_storage,@function
allocate_storage:                       # @allocate_storage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	nsyms(%rip), %edi
	addl	%edi, %edi
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r15
	movq	ritem(%rip), %rax
	jmp	.LBB1_1
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	incl	%r14d
	incw	(%r15,%rcx,2)
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movswq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB1_4
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	addq	$2, %rax
	testw	%cx, %cx
	jle	.LBB1_1
	jmp	.LBB1_3
.LBB1_4:
	movl	nsyms(%rip), %edi
	shll	$3, %edi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, kernel_base(%rip)
	addl	%r14d, %r14d
	xorl	%eax, %eax
	movl	%r14d, %edi
	callq	mallocate
	movq	%rax, kernel_items(%rip)
	movslq	nsyms(%rip), %rdi
	testq	%rdi, %rdi
	jle	.LBB1_11
# BB#5:                                 # %.lr.ph.i
	movq	kernel_base(%rip), %rsi
	leaq	-1(%rdi), %r8
	movq	%rdi, %rdx
	xorl	%ebx, %ebx
	andq	$3, %rdx
	je	.LBB1_8
# BB#6:                                 # %.prol.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rbp
	leaq	(%rax,%rbp,2), %rcx
	movq	%rcx, (%rsi,%rbx,8)
	movswl	(%r15,%rbx,2), %ecx
	addl	%ecx, %ebp
	incq	%rbx
	cmpq	%rbx, %rdx
	jne	.LBB1_7
.LBB1_8:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB1_11
# BB#9:                                 # %.lr.ph.i.new
	movq	%rdi, %rcx
	subq	%rbx, %rcx
	leaq	6(%r15,%rbx,2), %rdx
	leaq	24(%rsi,%rbx,8), %rsi
	.p2align	4, 0x90
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	movslq	%ebp, %rbp
	leaq	(%rax,%rbp,2), %rbx
	movq	%rbx, -24(%rsi)
	movswq	-6(%rdx), %rbx
	addq	%rbp, %rbx
	leaq	(%rax,%rbx,2), %rbp
	movq	%rbp, -16(%rsi)
	movswq	-4(%rdx), %rbp
	movslq	%ebx, %rbx
	addq	%rbp, %rbx
	leaq	(%rax,%rbx,2), %rbp
	movq	%rbp, -8(%rsi)
	movswq	-2(%rdx), %rbp
	movslq	%ebx, %rbx
	addq	%rbp, %rbx
	leaq	(%rax,%rbx,2), %rbp
	movq	%rbp, (%rsi)
	movswl	(%rdx), %ebp
	addl	%ebx, %ebp
	addq	$8, %rdx
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB1_10
.LBB1_11:                               # %allocate_itemsets.exit
	movq	%r15, shift_symbol(%rip)
	shll	$3, %edi
	xorl	%eax, %eax
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	mallocate
	movq	%rax, kernel_end(%rip)
	movl	nsyms(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, shiftset(%rip)
	movl	nrules(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, redset(%rip)
	movl	$8072, %edi             # imm = 0x1F88
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, state_table(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	allocate_storage, .Lfunc_end1-allocate_storage
	.cfi_endproc

	.globl	free_storage
	.p2align	4, 0x90
	.type	free_storage,@function
free_storage:                           # @free_storage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 16
	movq	shift_symbol(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_2
# BB#1:
	callq	free
.LBB2_2:
	movq	redset(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	callq	free
.LBB2_4:
	movq	shiftset(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_6
# BB#5:
	callq	free
.LBB2_6:
	movq	kernel_base(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#7:
	callq	free
.LBB2_8:
	movq	kernel_end(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_10
# BB#9:
	callq	free
.LBB2_10:
	movq	kernel_items(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#11:
	callq	free
.LBB2_12:
	movq	state_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_13
# BB#14:
	popq	%rax
	jmp	free                    # TAILCALL
.LBB2_13:
	popq	%rax
	retq
.Lfunc_end2:
	.size	free_storage, .Lfunc_end2-free_storage
	.cfi_endproc

	.globl	generate_states
	.p2align	4, 0x90
	.type	generate_states,@function
generate_states:                        # @generate_states
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	callq	allocate_storage
	movl	nitems(%rip), %edi
	callq	initialize_closure
	movl	$22, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rdi
	movq	%rdi, this_state(%rip)
	movq	%rdi, last_state(%rip)
	movq	%rdi, first_state(%rip)
	movl	$1, nstates(%rip)
	testq	%rdi, %rdi
	je	.LBB3_86
# BB#1:                                 # %.lr.ph.preheader
	movl	$first_reduction, %r14d
	movl	$first_shift, %r15d
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_8 Depth 2
                                        #     Child Loop BB3_22 Depth 2
                                        #     Child Loop BB3_25 Depth 2
                                        #     Child Loop BB3_30 Depth 2
                                        #     Child Loop BB3_32 Depth 2
                                        #     Child Loop BB3_37 Depth 2
                                        #       Child Loop BB3_38 Depth 3
                                        #     Child Loop BB3_47 Depth 2
                                        #     Child Loop BB3_52 Depth 2
                                        #       Child Loop BB3_53 Depth 3
                                        #       Child Loop BB3_56 Depth 3
                                        #     Child Loop BB3_62 Depth 2
                                        #     Child Loop BB3_73 Depth 2
                                        #     Child Loop BB3_76 Depth 2
                                        #     Child Loop BB3_81 Depth 2
                                        #     Child Loop BB3_83 Depth 2
	movswl	20(%rdi), %esi
	addq	$22, %rdi
	callq	closure
	movq	itemset(%rip), %rax
	movq	itemsetend(%rip), %rbp
	cmpq	%rbp, %rax
	jae	.LBB3_34
# BB#3:                                 # %.lr.ph41.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	ritem(%rip), %rdx
	movq	redset(%rip), %rsi
	movq	%rbp, %rcx
	subq	%rax, %rcx
	decq	%rcx
	movq	%rcx, %rdi
	shrq	%rdi
	xorl	%ebx, %ebx
	btl	$1, %ecx
	jb	.LBB3_7
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	movswq	(%rax), %rcx
	movw	(%rdx,%rcx,2), %r8w
	xorl	%ebx, %ebx
	testw	%r8w, %r8w
	jns	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	negl	%r8d
	movw	%r8w, (%rsi)
	movl	$1, %ebx
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	addq	$2, %rax
.LBB3_7:                                # %.prol.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	testq	%rdi, %rdi
	je	.LBB3_13
	.p2align	4, 0x90
.LBB3_8:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	(%rax), %rcx
	movw	(%rdx,%rcx,2), %di
	testw	%di, %di
	jns	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_8 Depth=2
	negl	%edi
	movslq	%ebx, %rcx
	incl	%ebx
	movw	%di, (%rsi,%rcx,2)
.LBB3_10:                               #   in Loop: Header=BB3_8 Depth=2
	movswq	2(%rax), %rcx
	movw	(%rdx,%rcx,2), %di
	testw	%di, %di
	jns	.LBB3_12
# BB#11:                                #   in Loop: Header=BB3_8 Depth=2
	negl	%edi
	movslq	%ebx, %rcx
	incl	%ebx
	movw	%di, (%rsi,%rcx,2)
.LBB3_12:                               #   in Loop: Header=BB3_8 Depth=2
	addq	$4, %rax
	cmpq	%rbp, %rax
	jb	.LBB3_8
.LBB3_13:                               # %._crit_edge42.i
                                        #   in Loop: Header=BB3_2 Depth=1
	testl	%ebx, %ebx
	je	.LBB3_34
# BB#14:                                #   in Loop: Header=BB3_2 Depth=1
	leal	14(%rbx,%rbx), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	this_state(%rip), %rcx
	movzwl	16(%rcx), %ecx
	movw	%cx, 8(%rax)
	movw	%bx, 10(%rax)
	testl	%ebx, %ebx
	jle	.LBB3_33
# BB#15:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	redset(%rip), %rcx
	movslq	%ebx, %rdx
	leaq	(%rcx,%rdx,2), %r10
	leaq	12(%rax), %rsi
	leaq	2(%rcx), %rdx
	cmpq	%rdx, %r10
	movq	%rdx, %r11
	cmovaq	%r10, %r11
	subq	%rcx, %r11
	decq	%r11
	shrq	%r11
	incq	%r11
	cmpq	$16, %r11
	jb	.LBB3_28
# BB#16:                                # %min.iters.checked55
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%r11, %r9
	andq	$-16, %r9
	je	.LBB3_28
# BB#17:                                # %vector.memcheck76
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%rdx, %r10
	cmovaq	%r10, %rdx
	subq	%rcx, %rdx
	decq	%rdx
	andq	$-2, %rdx
	leaq	2(%rcx,%rdx), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB3_19
# BB#18:                                # %vector.memcheck76
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	14(%rax,%rdx), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB3_28
.LBB3_19:                               # %vector.body44.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	-16(%r9), %r8
	movl	%r8d, %edx
	shrl	$4, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB3_20
# BB#21:                                # %vector.body44.prol.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	negq	%rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_22:                               # %vector.body44.prol
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rcx,%rdi,2), %xmm0
	movups	16(%rcx,%rdi,2), %xmm1
	movups	%xmm0, 12(%rax,%rdi,2)
	movups	%xmm1, 28(%rax,%rdi,2)
	addq	$16, %rdi
	incq	%rdx
	jne	.LBB3_22
	jmp	.LBB3_23
.LBB3_20:                               #   in Loop: Header=BB3_2 Depth=1
	xorl	%edi, %edi
.LBB3_23:                               # %vector.body44.prol.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	$48, %r8
	jb	.LBB3_26
# BB#24:                                # %vector.body44.preheader.new
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%r9, %rbx
	subq	%rdi, %rbx
	leaq	124(%rax,%rdi,2), %rdx
	leaq	112(%rcx,%rdi,2), %rdi
	.p2align	4, 0x90
.LBB3_25:                               # %vector.body44
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rdi
	addq	$-64, %rbx
	jne	.LBB3_25
.LBB3_26:                               # %middle.block45
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%r9, %r11
	je	.LBB3_33
# BB#27:                                #   in Loop: Header=BB3_2 Depth=1
	leaq	(%rcx,%r9,2), %rcx
	leaq	(%rsi,%r9,2), %rsi
.LBB3_28:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	2(%rcx), %rdi
	cmpq	%rdi, %r10
	cmovaq	%r10, %rdi
	subq	%rcx, %rdi
	decq	%rdi
	movl	%edi, %edx
	shrl	%edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB3_31
# BB#29:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB3_30:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rcx), %ebp
	addq	$2, %rcx
	movw	%bp, (%rsi)
	addq	$2, %rsi
	incq	%rdx
	jne	.LBB3_30
.LBB3_31:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	$14, %rdi
	jb	.LBB3_33
	.p2align	4, 0x90
.LBB3_32:                               # %.lr.ph.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rcx), %edx
	movw	%dx, (%rsi)
	movzwl	2(%rcx), %edx
	movw	%dx, 2(%rsi)
	movzwl	4(%rcx), %edx
	movw	%dx, 4(%rsi)
	movzwl	6(%rcx), %edx
	movw	%dx, 6(%rsi)
	movzwl	8(%rcx), %edx
	movw	%dx, 8(%rsi)
	movzwl	10(%rcx), %edx
	movw	%dx, 10(%rsi)
	movzwl	12(%rcx), %edx
	movw	%dx, 12(%rsi)
	movzwl	14(%rcx), %edx
	movw	%dx, 14(%rsi)
	addq	$16, %rcx
	addq	$16, %rsi
	cmpq	%r10, %rcx
	jb	.LBB3_32
.LBB3_33:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	last_reduction(%rip), %rcx
	testq	%rcx, %rcx
	cmoveq	%r14, %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_reduction(%rip)
.LBB3_34:                               # %save_reductions.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	nsyms(%rip), %eax
	movq	kernel_end(%rip), %rbx
	testl	%eax, %eax
	jle	.LBB3_36
# BB#35:                                # %.lr.ph.i1
                                        #   in Loop: Header=BB3_2 Depth=1
	decl	%eax
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	memset
.LBB3_36:                               # %._crit_edge.i2
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	itemset(%rip), %rcx
	movq	shift_symbol(%rip), %r10
	xorl	%r8d, %r8d
	movq	kernel_base(%rip), %r9
	jmp	.LBB3_37
	.p2align	4, 0x90
.LBB3_40:                               #   in Loop: Header=BB3_37 Depth=2
	movq	(%rbx,%rax,8), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_42
# BB#41:                                #   in Loop: Header=BB3_37 Depth=2
	movslq	%r8d, %rdx
	incl	%r8d
	movw	%ax, (%r10,%rdx,2)
	movq	(%r9,%rax,8), %rdi
.LBB3_42:                               #   in Loop: Header=BB3_37 Depth=2
	incl	%esi
	movw	%si, (%rdi)
	addq	$2, %rdi
	movq	%rdi, (%rbx,%rax,8)
.LBB3_37:                               # %.outer.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_38 Depth 3
	movq	itemsetend(%rip), %rdi
	movq	ritem(%rip), %rdx
	.p2align	4, 0x90
.LBB3_38:                               #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rdi, %rcx
	jae	.LBB3_43
# BB#39:                                #   in Loop: Header=BB3_38 Depth=3
	movswq	(%rcx), %rsi
	addq	$2, %rcx
	movswq	(%rdx,%rsi,2), %rax
	testq	%rax, %rax
	jle	.LBB3_38
	jmp	.LBB3_40
	.p2align	4, 0x90
.LBB3_43:                               # %new_itemsets.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	%r8d, nshifts(%rip)
	cmpl	$2, %r8d
	jl	.LBB3_59
# BB#44:                                # %.lr.ph31.i
                                        #   in Loop: Header=BB3_2 Depth=1
	testb	$1, %r8b
	jne	.LBB3_45
# BB#46:                                # %.lr.ph26.preheader.i.prol
                                        #   in Loop: Header=BB3_2 Depth=1
	movzwl	2(%r10), %eax
	movl	$1, %ecx
	movl	$2, %edx
	.p2align	4, 0x90
.LBB3_47:                               # %.lr.ph26.i.prol
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-4(%r10,%rdx,2), %esi
	cmpw	%ax, %si
	jle	.LBB3_49
# BB#48:                                #   in Loop: Header=BB3_47 Depth=2
	decl	%ecx
	movw	%si, -2(%r10,%rdx,2)
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB3_47
.LBB3_49:                               # %.critedge.i.prol
                                        #   in Loop: Header=BB3_2 Depth=1
	movslq	%ecx, %rcx
	movw	%ax, (%r10,%rcx,2)
	movl	$2, %ecx
	cmpl	$2, %r8d
	jne	.LBB3_51
	jmp	.LBB3_59
	.p2align	4, 0x90
.LBB3_45:                               #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %ecx
	cmpl	$2, %r8d
	je	.LBB3_59
.LBB3_51:                               # %.lr.ph31.i.new
                                        #   in Loop: Header=BB3_2 Depth=1
	movslq	%r8d, %r9
	leaq	2(%r10,%rcx,2), %rdi
	xorl	%r11d, %r11d
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB3_52:                               # %.lr.ph26.preheader.i
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_53 Depth 3
                                        #       Child Loop BB3_56 Depth 3
	movzwl	(%r10,%rdx,2), %esi
	movq	%r10, %rbx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_53:                               # %.lr.ph26.i
                                        #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-2(%rbx,%rdx,2), %ebp
	cmpw	%si, %bp
	jle	.LBB3_55
# BB#54:                                #   in Loop: Header=BB3_53 Depth=3
	movw	%bp, (%rbx,%rdx,2)
	leaq	-1(%rdx,%rax), %rbp
	decq	%rax
	incq	%rbp
	addq	$-2, %rbx
	cmpq	$1, %rbp
	jg	.LBB3_53
.LBB3_55:                               # %.lr.ph26.i..critedge.i_crit_edge
                                        #   in Loop: Header=BB3_52 Depth=2
	addl	%edx, %eax
	cltq
	movw	%si, (%r10,%rax,2)
	movzwl	2(%r10,%rdx,2), %eax
	movq	%r11, %rsi
	.p2align	4, 0x90
.LBB3_56:                               # %.lr.ph26.i.1
                                        #   Parent Loop BB3_2 Depth=1
                                        #     Parent Loop BB3_52 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-2(%rdi,%rsi,2), %ebx
	cmpw	%ax, %bx
	jle	.LBB3_58
# BB#57:                                #   in Loop: Header=BB3_56 Depth=3
	movw	%bx, (%rdi,%rsi,2)
	leaq	1(%rcx,%rsi), %rbx
	decq	%rsi
	cmpq	$1, %rbx
	jg	.LBB3_56
.LBB3_58:                               # %.lr.ph26.i.1..critedge.i.1_crit_edge
                                        #   in Loop: Header=BB3_52 Depth=2
	leal	1(%rcx,%rsi), %esi
	movslq	%esi, %rsi
	movw	%ax, (%r10,%rsi,2)
	addq	$2, %rdx
	addq	$2, %r11
	cmpq	%r9, %rdx
	jne	.LBB3_52
.LBB3_59:                               # %.preheader.i
                                        #   in Loop: Header=BB3_2 Depth=1
	testl	%r8d, %r8d
	jle	.LBB3_64
# BB#60:                                # %.lr.ph.i4.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	movswl	(%r10), %edi
	callq	get_state
	movq	shiftset(%rip), %rcx
	movw	%ax, (%rcx)
	movl	nshifts(%rip), %eax
	cmpl	$2, %eax
	jl	.LBB3_63
# BB#61:                                # %.lr.ph.i4..lr.ph.i4_crit_edge.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB3_62:                               # %.lr.ph.i4..lr.ph.i4_crit_edge
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	shift_symbol(%rip), %rax
	movswl	(%rax,%rbx,2), %edi
	callq	get_state
	movq	shiftset(%rip), %rcx
	movw	%ax, (%rcx,%rbx,2)
	incq	%rbx
	movslq	nshifts(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_62
.LBB3_63:                               # %append_states.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	testl	%eax, %eax
	jle	.LBB3_64
# BB#65:                                #   in Loop: Header=BB3_2 Depth=1
	leal	14(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	this_state(%rip), %r10
	movzwl	16(%r10), %ecx
	movw	%cx, 8(%rax)
	movslq	nshifts(%rip), %rcx
	testq	%rcx, %rcx
	movw	%cx, 10(%rax)
	jle	.LBB3_84
# BB#66:                                # %.lr.ph.preheader.i6
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	shiftset(%rip), %rdx
	leaq	(%rdx,%rcx,2), %r11
	leaq	12(%rax), %rdi
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %r11
	movq	%rcx, %r12
	cmovaq	%r11, %r12
	subq	%rdx, %r12
	decq	%r12
	shrq	%r12
	incq	%r12
	cmpq	$16, %r12
	jb	.LBB3_79
# BB#67:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%r12, %r8
	andq	$-16, %r8
	je	.LBB3_79
# BB#68:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%rcx, %r11
	cmovaq	%r11, %rcx
	subq	%rdx, %rcx
	decq	%rcx
	andq	$-2, %rcx
	leaq	2(%rdx,%rcx), %rsi
	cmpq	%rsi, %rdi
	jae	.LBB3_70
# BB#69:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	14(%rax,%rcx), %rcx
	cmpq	%rcx, %rdx
	jb	.LBB3_79
.LBB3_70:                               # %vector.body.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	-16(%r8), %r9
	movl	%r9d, %ecx
	shrl	$4, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB3_71
# BB#72:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	negq	%rcx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_73:                               # %vector.body.prol
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rdx,%rbx,2), %xmm0
	movups	16(%rdx,%rbx,2), %xmm1
	movups	%xmm0, 12(%rax,%rbx,2)
	movups	%xmm1, 28(%rax,%rbx,2)
	addq	$16, %rbx
	incq	%rcx
	jne	.LBB3_73
	jmp	.LBB3_74
	.p2align	4, 0x90
.LBB3_64:                               # %append_states.exit._crit_edge
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	this_state(%rip), %r10
	jmp	.LBB3_85
.LBB3_71:                               #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebx, %ebx
.LBB3_74:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	$48, %r9
	jb	.LBB3_77
# BB#75:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%r8, %rcx
	subq	%rbx, %rcx
	leaq	124(%rax,%rbx,2), %rsi
	leaq	112(%rdx,%rbx,2), %rbx
	.p2align	4, 0x90
.LBB3_76:                               # %vector.body
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-64, %rcx
	jne	.LBB3_76
.LBB3_77:                               # %middle.block
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	%r8, %r12
	je	.LBB3_84
# BB#78:                                #   in Loop: Header=BB3_2 Depth=1
	leaq	(%rdx,%r8,2), %rdx
	leaq	(%rdi,%r8,2), %rdi
	.p2align	4, 0x90
.LBB3_79:                               # %.lr.ph.i7.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	leaq	2(%rdx), %rbx
	cmpq	%rbx, %r11
	cmovaq	%r11, %rbx
	subq	%rdx, %rbx
	decq	%rbx
	movl	%ebx, %ecx
	shrl	%ecx
	incl	%ecx
	andq	$7, %rcx
	je	.LBB3_82
# BB#80:                                # %.lr.ph.i7.prol.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB3_81:                               # %.lr.ph.i7.prol
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdx), %esi
	addq	$2, %rdx
	movw	%si, (%rdi)
	addq	$2, %rdi
	incq	%rcx
	jne	.LBB3_81
.LBB3_82:                               # %.lr.ph.i7.prol.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpq	$14, %rbx
	jb	.LBB3_84
	.p2align	4, 0x90
.LBB3_83:                               # %.lr.ph.i7
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdx), %ecx
	movw	%cx, (%rdi)
	movzwl	2(%rdx), %ecx
	movw	%cx, 2(%rdi)
	movzwl	4(%rdx), %ecx
	movw	%cx, 4(%rdi)
	movzwl	6(%rdx), %ecx
	movw	%cx, 6(%rdi)
	movzwl	8(%rdx), %ecx
	movw	%cx, 8(%rdi)
	movzwl	10(%rdx), %ecx
	movw	%cx, 10(%rdi)
	movzwl	12(%rdx), %ecx
	movw	%cx, 12(%rdi)
	movzwl	14(%rdx), %ecx
	movw	%cx, 14(%rdi)
	addq	$16, %rdx
	addq	$16, %rdi
	cmpq	%r11, %rdx
	jb	.LBB3_83
.LBB3_84:                               # %save_shifts.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	last_shift(%rip), %rcx
	testq	%rcx, %rcx
	cmoveq	%r15, %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_shift(%rip)
.LBB3_85:                               #   in Loop: Header=BB3_2 Depth=1
	movq	(%r10), %rdi
	movq	%rdi, this_state(%rip)
	testq	%rdi, %rdi
	jne	.LBB3_2
.LBB3_86:                               # %._crit_edge
	callq	finalize_closure
	callq	free_storage
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	augment_automaton       # TAILCALL
.Lfunc_end3:
	.size	generate_states, .Lfunc_end3-generate_states
	.cfi_endproc

	.globl	initialize_states
	.p2align	4, 0x90
	.type	initialize_states,@function
initialize_states:                      # @initialize_states
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 16
	movl	$22, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, this_state(%rip)
	movq	%rax, last_state(%rip)
	movq	%rax, first_state(%rip)
	movl	$1, nstates(%rip)
	popq	%rax
	retq
.Lfunc_end4:
	.size	initialize_states, .Lfunc_end4-initialize_states
	.cfi_endproc

	.globl	save_reductions
	.p2align	4, 0x90
	.type	save_reductions,@function
save_reductions:                        # @save_reductions
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 16
.Lcfi31:
	.cfi_offset %rbx, -16
	movq	itemset(%rip), %rax
	movq	itemsetend(%rip), %rcx
	cmpq	%rcx, %rax
	jae	.LBB5_33
# BB#1:                                 # %.lr.ph41
	movq	ritem(%rip), %rdx
	movq	redset(%rip), %r9
	movq	%rax, %rbx
	notq	%rbx
	addq	%rcx, %rbx
	movq	%rbx, %rdi
	shrq	%rdi
	btl	$1, %ebx
	jb	.LBB5_2
# BB#3:
	movswq	(%rax), %rbx
	movw	(%rdx,%rbx,2), %r8w
	xorl	%ebx, %ebx
	testw	%r8w, %r8w
	jns	.LBB5_5
# BB#4:
	negl	%r8d
	movw	%r8w, (%r9)
	movl	$1, %ebx
.LBB5_5:
	addq	$2, %rax
	testq	%rdi, %rdi
	jne	.LBB5_7
	jmp	.LBB5_12
.LBB5_2:
	xorl	%ebx, %ebx
	testq	%rdi, %rdi
	je	.LBB5_12
	.p2align	4, 0x90
.LBB5_7:                                # =>This Inner Loop Header: Depth=1
	movswq	(%rax), %rdi
	movw	(%rdx,%rdi,2), %di
	testw	%di, %di
	jns	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_7 Depth=1
	negl	%edi
	movslq	%ebx, %rsi
	incl	%ebx
	movw	%di, (%r9,%rsi,2)
.LBB5_9:                                #   in Loop: Header=BB5_7 Depth=1
	movswq	2(%rax), %rsi
	movw	(%rdx,%rsi,2), %di
	testw	%di, %di
	jns	.LBB5_11
# BB#10:                                #   in Loop: Header=BB5_7 Depth=1
	negl	%edi
	movslq	%ebx, %rsi
	incl	%ebx
	movw	%di, (%r9,%rsi,2)
.LBB5_11:                               #   in Loop: Header=BB5_7 Depth=1
	addq	$4, %rax
	cmpq	%rcx, %rax
	jb	.LBB5_7
.LBB5_12:                               # %._crit_edge42
	testl	%ebx, %ebx
	je	.LBB5_33
# BB#13:
	leal	14(%rbx,%rbx), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	this_state(%rip), %rcx
	movzwl	16(%rcx), %ecx
	movw	%cx, 8(%rax)
	movw	%bx, 10(%rax)
	testl	%ebx, %ebx
	jle	.LBB5_32
# BB#14:                                # %.lr.ph.preheader
	movq	redset(%rip), %rcx
	movslq	%ebx, %rdx
	leaq	(%rcx,%rdx,2), %r10
	leaq	12(%rax), %rsi
	leaq	2(%rcx), %rdx
	cmpq	%rdx, %r10
	movq	%rdx, %r9
	cmovaq	%r10, %r9
	movq	%rcx, %rdi
	notq	%rdi
	addq	%rdi, %r9
	shrq	%r9
	incq	%r9
	cmpq	$16, %r9
	jb	.LBB5_27
# BB#15:                                # %min.iters.checked
	movq	%r9, %r8
	andq	$-16, %r8
	je	.LBB5_27
# BB#16:                                # %vector.memcheck
	cmpq	%rdx, %r10
	cmovaq	%r10, %rdx
	addq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	2(%rcx,%rdx), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB5_18
# BB#17:                                # %vector.memcheck
	leaq	14(%rax,%rdx), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB5_27
.LBB5_18:                               # %vector.body.preheader
	leaq	-16(%r8), %rbx
	movl	%ebx, %edi
	shrl	$4, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB5_19
# BB#20:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB5_21:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdx,2), %xmm0
	movups	16(%rcx,%rdx,2), %xmm1
	movups	%xmm0, 12(%rax,%rdx,2)
	movups	%xmm1, 28(%rax,%rdx,2)
	addq	$16, %rdx
	incq	%rdi
	jne	.LBB5_21
	jmp	.LBB5_22
.LBB5_19:
	xorl	%edx, %edx
.LBB5_22:                               # %vector.body.prol.loopexit
	cmpq	$48, %rbx
	jb	.LBB5_25
# BB#23:                                # %vector.body.preheader.new
	movq	%r8, %rbx
	subq	%rdx, %rbx
	leaq	124(%rax,%rdx,2), %rdi
	leaq	112(%rcx,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB5_24:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rdx
	addq	$-64, %rbx
	jne	.LBB5_24
.LBB5_25:                               # %middle.block
	cmpq	%r8, %r9
	je	.LBB5_32
# BB#26:
	leaq	(%rcx,%r8,2), %rcx
	leaq	(%rsi,%r8,2), %rsi
.LBB5_27:                               # %.lr.ph.preheader59
	leaq	2(%rcx), %rdx
	cmpq	%rdx, %r10
	cmovaq	%r10, %rdx
	movq	%rcx, %rdi
	notq	%rdi
	addq	%rdx, %rdi
	movl	%edi, %edx
	shrl	%edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB5_30
# BB#28:                                # %.lr.ph.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB5_29:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx), %ebx
	addq	$2, %rcx
	movw	%bx, (%rsi)
	addq	$2, %rsi
	incq	%rdx
	jne	.LBB5_29
.LBB5_30:                               # %.lr.ph.prol.loopexit
	cmpq	$14, %rdi
	jb	.LBB5_32
	.p2align	4, 0x90
.LBB5_31:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx), %edx
	movw	%dx, (%rsi)
	movzwl	2(%rcx), %edx
	movw	%dx, 2(%rsi)
	movzwl	4(%rcx), %edx
	movw	%dx, 4(%rsi)
	movzwl	6(%rcx), %edx
	movw	%dx, 6(%rsi)
	movzwl	8(%rcx), %edx
	movw	%dx, 8(%rsi)
	movzwl	10(%rcx), %edx
	movw	%dx, 10(%rsi)
	movzwl	12(%rcx), %edx
	movw	%dx, 12(%rsi)
	movzwl	14(%rcx), %edx
	movw	%dx, 14(%rsi)
	addq	$16, %rcx
	addq	$16, %rsi
	cmpq	%r10, %rcx
	jb	.LBB5_31
.LBB5_32:                               # %._crit_edge
	movq	last_reduction(%rip), %rcx
	testq	%rcx, %rcx
	movl	$first_reduction, %edx
	cmovneq	%rcx, %rdx
	movq	%rax, (%rdx)
	movq	%rax, last_reduction(%rip)
.LBB5_33:                               # %._crit_edge42.thread
	popq	%rbx
	retq
.Lfunc_end5:
	.size	save_reductions, .Lfunc_end5-save_reductions
	.cfi_endproc

	.globl	new_itemsets
	.p2align	4, 0x90
	.type	new_itemsets,@function
new_itemsets:                           # @new_itemsets
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movl	nsyms(%rip), %eax
	movq	kernel_end(%rip), %r14
	testl	%eax, %eax
	jle	.LBB6_2
# BB#1:                                 # %.lr.ph
	decl	%eax
	leaq	8(,%rax,8), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
.LBB6_2:                                # %._crit_edge
	movq	itemset(%rip), %rax
	movq	shift_symbol(%rip), %r8
	xorl	%ecx, %ecx
	movq	kernel_base(%rip), %r9
	jmp	.LBB6_3
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_3 Depth=1
	movq	(%r14,%rdi,8), %rsi
	testq	%rsi, %rsi
	jne	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_3 Depth=1
	movslq	%ecx, %rsi
	incl	%ecx
	movw	%di, (%r8,%rsi,2)
	movq	(%r9,%rdi,8), %rsi
.LBB6_8:                                #   in Loop: Header=BB6_3 Depth=1
	incl	%edx
	movw	%dx, (%rsi)
	addq	$2, %rsi
	movq	%rsi, (%r14,%rdi,8)
.LBB6_3:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	movq	itemsetend(%rip), %rsi
	movq	ritem(%rip), %rbx
	.p2align	4, 0x90
.LBB6_4:                                #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rsi, %rax
	jae	.LBB6_9
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=2
	movswq	(%rax), %rdx
	addq	$2, %rax
	movswq	(%rbx,%rdx,2), %rdi
	testq	%rdi, %rdi
	jle	.LBB6_4
	jmp	.LBB6_6
.LBB6_9:
	movl	%ecx, nshifts(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	new_itemsets, .Lfunc_end6-new_itemsets
	.cfi_endproc

	.globl	append_states
	.p2align	4, 0x90
	.type	append_states,@function
append_states:                          # @append_states
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
.Lcfi38:
	.cfi_offset %rbx, -16
	movslq	nshifts(%rip), %r8
	cmpq	$2, %r8
	jl	.LBB7_16
# BB#1:                                 # %.lr.ph31
	movq	shift_symbol(%rip), %r9
	testb	$1, %r8b
	jne	.LBB7_2
# BB#3:                                 # %.lr.ph26.preheader.prol
	movzwl	2(%r9), %eax
	movl	$1, %ecx
	movl	$2, %edx
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph26.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-4(%r9,%rdx,2), %esi
	cmpw	%ax, %si
	jle	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=1
	decl	%ecx
	movw	%si, -2(%r9,%rdx,2)
	decq	%rdx
	cmpq	$1, %rdx
	jg	.LBB7_4
.LBB7_6:                                # %.critedge.prol
	movslq	%ecx, %rcx
	movw	%ax, (%r9,%rcx,2)
	movl	$2, %r11d
	cmpl	$2, %r8d
	jne	.LBB7_8
	jmp	.LBB7_16
.LBB7_2:
	movl	$1, %r11d
	cmpl	$2, %r8d
	je	.LBB7_16
.LBB7_8:                                # %.lr.ph31.new
	leaq	2(%r9,%r11,2), %rsi
	xorl	%r10d, %r10d
	movq	%r11, %rdx
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph26.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_10 Depth 2
                                        #     Child Loop BB7_13 Depth 2
	movzwl	(%r9,%rdx,2), %ecx
	movq	%r9, %rax
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB7_10:                               # %.lr.ph26
                                        #   Parent Loop BB7_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%rax,%rdx,2), %ebx
	cmpw	%cx, %bx
	jle	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_10 Depth=2
	movw	%bx, (%rax,%rdx,2)
	leaq	-1(%rdx,%rdi), %rbx
	decq	%rdi
	incq	%rbx
	addq	$-2, %rax
	cmpq	$1, %rbx
	jg	.LBB7_10
.LBB7_12:                               # %.lr.ph26..critedge_crit_edge
                                        #   in Loop: Header=BB7_9 Depth=1
	addl	%edx, %edi
	movslq	%edi, %rax
	movw	%cx, (%r9,%rax,2)
	movzwl	2(%r9,%rdx,2), %ecx
	movq	%r10, %rdi
	.p2align	4, 0x90
.LBB7_13:                               # %.lr.ph26.1
                                        #   Parent Loop BB7_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%rsi,%rdi,2), %eax
	cmpw	%cx, %ax
	jle	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_13 Depth=2
	movw	%ax, (%rsi,%rdi,2)
	leaq	1(%r11,%rdi), %rax
	decq	%rdi
	cmpq	$1, %rax
	jg	.LBB7_13
.LBB7_15:                               # %.lr.ph26.1..critedge.1_crit_edge
                                        #   in Loop: Header=BB7_9 Depth=1
	leal	1(%r11,%rdi), %eax
	cltq
	movw	%cx, (%r9,%rax,2)
	addq	$2, %rdx
	addq	$2, %r10
	cmpq	%r8, %rdx
	jl	.LBB7_9
.LBB7_16:                               # %.preheader
	testl	%r8d, %r8d
	jle	.LBB7_19
# BB#17:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	shift_symbol(%rip), %rax
	movswl	(%rax,%rbx,2), %edi
	callq	get_state
	movq	shiftset(%rip), %rcx
	movw	%ax, (%rcx,%rbx,2)
	incq	%rbx
	movslq	nshifts(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB7_18
.LBB7_19:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end7:
	.size	append_states, .Lfunc_end7-append_states
	.cfi_endproc

	.globl	save_shifts
	.p2align	4, 0x90
	.type	save_shifts,@function
save_shifts:                            # @save_shifts
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 16
	movl	nshifts(%rip), %eax
	leal	14(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	this_state(%rip), %rcx
	movzwl	16(%rcx), %ecx
	movw	%cx, 8(%rax)
	movslq	nshifts(%rip), %rdx
	testq	%rdx, %rdx
	movw	%dx, 10(%rax)
	jle	.LBB8_19
# BB#1:                                 # %.lr.ph.preheader
	movq	shiftset(%rip), %rcx
	leaq	(%rcx,%rdx,2), %r11
	leaq	12(%rax), %rsi
	leaq	2(%rcx), %rdx
	cmpq	%rdx, %r11
	movq	%rdx, %r10
	cmovaq	%r11, %r10
	movq	%rcx, %rdi
	notq	%rdi
	addq	%rdi, %r10
	shrq	%r10
	incq	%r10
	cmpq	$16, %r10
	jb	.LBB8_14
# BB#2:                                 # %min.iters.checked
	movq	%r10, %r8
	andq	$-16, %r8
	je	.LBB8_14
# BB#3:                                 # %vector.memcheck
	cmpq	%rdx, %r11
	cmovaq	%r11, %rdx
	addq	%rdi, %rdx
	andq	$-2, %rdx
	leaq	2(%rcx,%rdx), %rdi
	cmpq	%rdi, %rsi
	jae	.LBB8_5
# BB#4:                                 # %vector.memcheck
	leaq	14(%rax,%rdx), %rdx
	cmpq	%rdx, %rcx
	jb	.LBB8_14
.LBB8_5:                                # %vector.body.preheader
	leaq	-16(%r8), %r9
	movl	%r9d, %edx
	shrl	$4, %edx
	incl	%edx
	andq	$3, %rdx
	je	.LBB8_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rdx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB8_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdi,2), %xmm0
	movups	16(%rcx,%rdi,2), %xmm1
	movups	%xmm0, 12(%rax,%rdi,2)
	movups	%xmm1, 28(%rax,%rdi,2)
	addq	$16, %rdi
	incq	%rdx
	jne	.LBB8_8
	jmp	.LBB8_9
.LBB8_6:
	xorl	%edi, %edi
.LBB8_9:                                # %vector.body.prol.loopexit
	cmpq	$48, %r9
	jb	.LBB8_12
# BB#10:                                # %vector.body.preheader.new
	movq	%r8, %r9
	subq	%rdi, %r9
	leaq	124(%rax,%rdi,2), %rdx
	leaq	112(%rcx,%rdi,2), %rdi
	.p2align	4, 0x90
.LBB8_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdi), %xmm0
	movups	-96(%rdi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rdi), %xmm0
	movups	-64(%rdi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rdi
	addq	$-64, %r9
	jne	.LBB8_11
.LBB8_12:                               # %middle.block
	cmpq	%r8, %r10
	je	.LBB8_19
# BB#13:
	leaq	(%rcx,%r8,2), %rcx
	leaq	(%rsi,%r8,2), %rsi
.LBB8_14:                               # %.lr.ph.preheader36
	leaq	2(%rcx), %rdx
	cmpq	%rdx, %r11
	cmovaq	%r11, %rdx
	movq	%rcx, %r8
	notq	%r8
	addq	%rdx, %r8
	movl	%r8d, %edx
	shrl	%edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB8_17
# BB#15:                                # %.lr.ph.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB8_16:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx), %edi
	addq	$2, %rcx
	movw	%di, (%rsi)
	addq	$2, %rsi
	incq	%rdx
	jne	.LBB8_16
.LBB8_17:                               # %.lr.ph.prol.loopexit
	cmpq	$14, %r8
	jb	.LBB8_19
	.p2align	4, 0x90
.LBB8_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx), %edx
	movw	%dx, (%rsi)
	movzwl	2(%rcx), %edx
	movw	%dx, 2(%rsi)
	movzwl	4(%rcx), %edx
	movw	%dx, 4(%rsi)
	movzwl	6(%rcx), %edx
	movw	%dx, 6(%rsi)
	movzwl	8(%rcx), %edx
	movw	%dx, 8(%rsi)
	movzwl	10(%rcx), %edx
	movw	%dx, 10(%rsi)
	movzwl	12(%rcx), %edx
	movw	%dx, 12(%rsi)
	movzwl	14(%rcx), %edx
	movw	%dx, 14(%rsi)
	addq	$16, %rcx
	addq	$16, %rsi
	cmpq	%r11, %rcx
	jb	.LBB8_18
.LBB8_19:                               # %._crit_edge
	movq	last_shift(%rip), %rcx
	testq	%rcx, %rcx
	movl	$first_shift, %edx
	cmovneq	%rcx, %rdx
	movq	%rax, (%rdx)
	movq	%rax, last_shift(%rip)
	popq	%rax
	retq
.Lfunc_end8:
	.size	save_shifts, .Lfunc_end8-save_shifts
	.cfi_endproc

	.globl	augment_automaton
	.p2align	4, 0x90
	.type	augment_automaton,@function
augment_automaton:                      # @augment_automaton
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 48
.Lcfi45:
	.cfi_offset %rbx, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	first_shift(%rip), %r15
	testq	%r15, %r15
	je	.LBB9_3
# BB#1:
	cmpw	$0, 8(%r15)
	je	.LBB9_4
# BB#2:
	movl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	first_shift(%rip), %rcx
	movq	%rcx, (%rax)
	movw	$1, 10(%rax)
	movzwl	nstates(%rip), %ecx
	movw	%cx, 12(%rax)
	movq	%rax, first_shift(%rip)
	jmp	.LBB9_42
.LBB9_3:
	movl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movw	$1, 10(%rax)
	movzwl	nstates(%rip), %ecx
	movw	%cx, 12(%rax)
	movq	%rax, first_shift(%rip)
	movq	%rax, last_shift(%rip)
	jmp	.LBB9_42
.LBB9_4:
	movswl	10(%r15), %eax
	movq	first_state(%rip), %rdx
	movl	start_symbol(%rip), %ecx
	.p2align	4, 0x90
.LBB9_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rdx
	movswl	18(%rdx), %esi
	movzwl	16(%rdx), %ebp
	cmpl	%ecx, %esi
	jge	.LBB9_7
# BB#6:                                 #   in Loop: Header=BB9_5 Depth=1
	cmpw	%ax, %bp
	jl	.LBB9_5
.LBB9_7:                                # %.critedge
	cmpl	%ecx, %esi
	jne	.LBB9_10
# BB#8:                                 # %.preheader
	testw	%bp, %bp
	jle	.LBB9_13
	.p2align	4, 0x90
.LBB9_9:                                # %.lr.ph106
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %r14
	movq	(%r14), %r15
	movw	8(%r15), %ax
	cmpw	%bp, %ax
	jl	.LBB9_9
	jmp	.LBB9_14
.LBB9_10:
	movswq	%bp, %r14
	leal	16(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	(%r15), %rcx
	movq	%rcx, (%rax)
	movzwl	10(%r15), %ecx
	incw	%cx
	movw	%cx, 10(%rax)
	leaq	12(%rax), %r9
	testw	%bp, %bp
	jle	.LBB9_37
# BB#11:                                # %.lr.ph116
	movl	%r14d, %edx
	cmpq	$16, %rdx
	jae	.LBB9_23
# BB#12:
	xorl	%esi, %esi
	jmp	.LBB9_31
.LBB9_13:
	xorl	%eax, %eax
                                        # implicit-def: %R14
.LBB9_14:                               # %._crit_edge107
	cmpw	%bp, %ax
	jne	.LBB9_22
# BB#15:
	movswl	10(%r15), %eax
	leal	16(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	(%r15), %rcx
	movq	%rcx, (%rax)
	movw	%bp, 8(%rax)
	movzwl	10(%r15), %ecx
	incw	%cx
	movw	%cx, 10(%rax)
	movzwl	nstates(%rip), %ecx
	movw	%cx, 12(%rax)
	movswq	10(%r15), %rcx
	testq	%rcx, %rcx
	jle	.LBB9_51
# BB#16:                                # %.lr.ph.preheader
	movq	%rcx, %rbp
	notq	%rbp
	cmpq	$-3, %rbp
	movq	$-2, %rdx
	cmovgq	%rbp, %rdx
	leaq	2(%rdx,%rcx), %rdx
	cmpq	$16, %rdx
	jb	.LBB9_50
# BB#17:                                # %min.iters.checked149
	movq	%rdx, %rsi
	andq	$-16, %rsi
	je	.LBB9_50
# BB#18:                                # %vector.memcheck165
	cmpq	$-3, %rbp
	movq	$-2, %rdi
	cmovgq	%rbp, %rdi
	addq	%rdi, %rdi
	movl	$10, %ebp
	subq	%rdi, %rbp
	addq	%rax, %rbp
	leaq	12(%r15,%rcx,2), %rbx
	cmpq	%rbx, %rbp
	jae	.LBB9_20
# BB#19:                                # %vector.memcheck165
	leaq	14(%rax,%rcx,2), %rbp
	movq	%r15, %rbx
	subq	%rdi, %rbx
	addq	$8, %rbx
	cmpq	%rbp, %rbx
	jb	.LBB9_50
.LBB9_20:                               # %vector.body145.preheader
	leaq	-16(%rsi), %rdi
	movq	%rdi, %rbp
	shrq	$4, %rbp
	btl	$4, %edi
	jb	.LBB9_45
# BB#21:                                # %vector.body145.prol
	movups	-4(%r15,%rcx,2), %xmm0
	movups	-20(%r15,%rcx,2), %xmm1
	movups	%xmm0, -2(%rax,%rcx,2)
	movups	%xmm1, -18(%rax,%rcx,2)
	movl	$16, %ebx
	testq	%rbp, %rbp
	jne	.LBB9_46
	jmp	.LBB9_48
.LBB9_22:
	movl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%r15, (%rax)
	movw	%bp, 8(%rax)
	movw	$1, 10(%rax)
	movzwl	nstates(%rip), %ecx
	movw	%cx, 12(%rax)
	movq	%rax, (%r14)
	testq	%r15, %r15
	jne	.LBB9_44
	jmp	.LBB9_43
.LBB9_23:                               # %min.iters.checked
	movl	%r14d, %r8d
	andl	$15, %r8d
	movq	%rdx, %rsi
	subq	%r8, %rsi
	je	.LBB9_27
# BB#24:                                # %vector.memcheck
	leaq	12(%r15,%rdx,2), %rcx
	cmpq	%rcx, %r9
	jae	.LBB9_28
# BB#25:                                # %vector.memcheck
	leaq	12(%rax,%rdx,2), %rcx
	leaq	12(%r15), %rdi
	cmpq	%rcx, %rdi
	jae	.LBB9_28
.LBB9_27:
	xorl	%esi, %esi
	jmp	.LBB9_31
.LBB9_28:                               # %vector.body.preheader
	leaq	28(%r15), %rbx
	leaq	28(%rax), %rdi
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB9_29:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rbx
	addq	$32, %rdi
	addq	$-16, %rcx
	jne	.LBB9_29
# BB#30:                                # %middle.block
	testq	%r8, %r8
	je	.LBB9_37
.LBB9_31:                               # %scalar.ph.preheader
	movl	%r14d, %ebx
	subl	%esi, %ebx
	leaq	-1(%rdx), %rdi
	subq	%rsi, %rdi
	andq	$7, %rbx
	je	.LBB9_34
# BB#32:                                # %scalar.ph.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB9_33:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	12(%r15,%rsi,2), %ecx
	movw	%cx, (%r9,%rsi,2)
	incq	%rsi
	incq	%rbx
	jne	.LBB9_33
.LBB9_34:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdi
	jb	.LBB9_37
# BB#35:                                # %scalar.ph.preheader.new
	subq	%rsi, %rdx
	leaq	26(%rax,%rsi,2), %rdi
	leaq	26(%r15,%rsi,2), %rsi
	.p2align	4, 0x90
.LBB9_36:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-14(%rsi), %ecx
	movw	%cx, -14(%rdi)
	movzwl	-12(%rsi), %ecx
	movw	%cx, -12(%rdi)
	movzwl	-10(%rsi), %ecx
	movw	%cx, -10(%rdi)
	movzwl	-8(%rsi), %ecx
	movw	%cx, -8(%rdi)
	movzwl	-6(%rsi), %ecx
	movw	%cx, -6(%rdi)
	movzwl	-4(%rsi), %ecx
	movw	%cx, -4(%rdi)
	movzwl	-2(%rsi), %ecx
	movw	%cx, -2(%rdi)
	movzwl	(%rsi), %ecx
	movw	%cx, (%rdi)
	addq	$16, %rdi
	addq	$16, %rsi
	addq	$-8, %rdx
	jne	.LBB9_36
.LBB9_37:                               # %._crit_edge117
	movzwl	nstates(%rip), %ecx
	movw	%cx, (%r9,%r14,2)
	cmpw	10(%r15), %bp
	jge	.LBB9_39
	.p2align	4, 0x90
.LBB9_38:                               # %.lr.ph112
                                        # =>This Inner Loop Header: Depth=1
	movzwl	12(%r15,%r14,2), %ecx
	movw	%cx, 14(%rax,%r14,2)
	incq	%r14
	movswq	10(%r15), %rcx
	cmpq	%rcx, %r14
	jl	.LBB9_38
.LBB9_39:                               # %._crit_edge113
	movq	%rax, first_shift(%rip)
	cmpq	%r15, last_shift(%rip)
	jne	.LBB9_41
# BB#40:
	movq	%rax, last_shift(%rip)
.LBB9_41:
	movq	%r15, %rdi
	callq	free
.LBB9_42:
	movl	$22, %edi
	xorl	%eax, %eax
	callq	mallocate
	movzwl	nstates(%rip), %ecx
	movw	%cx, 16(%rax)
	movzwl	start_symbol(%rip), %ecx
	movw	%cx, 18(%rax)
	movq	last_state(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_state(%rip)
	movl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movl	nstates(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, nstates(%rip)
	movw	%cx, 8(%rax)
	movw	$1, 10(%rax)
	movw	%dx, 12(%rax)
	movq	last_shift(%rip), %rcx
	movq	%rax, (%rcx)
.LBB9_43:
	movq	%rax, last_shift(%rip)
.LBB9_44:
	movl	$22, %edi
	xorl	%eax, %eax
	callq	mallocate
	movzwl	nstates(%rip), %ecx
	movw	%cx, 16(%rax)
	movq	last_state(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_state(%rip)
	movl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movl	nstates(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, nstates(%rip)
	movw	%cx, 8(%rax)
	movw	$1, 10(%rax)
	movw	%dx, 12(%rax)
	movq	last_shift(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_shift(%rip)
	movl	%edx, final_state(%rip)
	movl	$22, %edi
	xorl	%eax, %eax
	callq	mallocate
	movl	nstates(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, nstates(%rip)
	movw	%cx, 16(%rax)
	movq	last_state(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_state(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_45:
	xorl	%ebx, %ebx
	testq	%rbp, %rbp
	je	.LBB9_48
.LBB9_46:                               # %vector.body145.preheader.new
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	leaq	-4(%rcx,%rcx), %rbp
	addq	%rbx, %rbx
	subq	%rbx, %rbp
	addq	%r15, %rbp
	leaq	-2(%rcx,%rcx), %r8
	subq	%rbx, %r8
	movq	%rax, %rbx
	addq	%r8, %rbx
.LBB9_47:                               # %vector.body145
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm1, (%rbx)
	movups	%xmm0, -16(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm1, -32(%rbx)
	movups	%xmm0, -48(%rbx)
	addq	$-64, %rbp
	addq	$-64, %rbx
	addq	$-32, %rdi
	jne	.LBB9_47
.LBB9_48:                               # %middle.block146
	cmpq	%rsi, %rdx
	je	.LBB9_51
# BB#49:
	subq	%rsi, %rcx
	.p2align	4, 0x90
.LBB9_50:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	10(%r15,%rcx,2), %edx
	movw	%dx, 12(%rax,%rcx,2)
	cmpq	$1, %rcx
	leaq	-1(%rcx), %rcx
	jg	.LBB9_50
.LBB9_51:                               # %._crit_edge
	movq	%rax, (%r14)
	movq	%r15, %rdi
	callq	free
	jmp	.LBB9_44
.Lfunc_end9:
	.size	augment_automaton, .Lfunc_end9-augment_automaton
	.cfi_endproc

	.globl	get_state
	.p2align	4, 0x90
	.type	get_state,@function
get_state:                              # @get_state
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 80
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movl	%edi, %r12d
	movq	kernel_base(%rip), %r8
	movslq	%r12d, %r15
	movq	(%r8,%r15,8), %r14
	movq	kernel_end(%rip), %rax
	movq	(%rax,%r15,8), %rbx
	xorl	%ebp, %ebp
	cmpq	%rbx, %r14
	jae	.LBB10_14
# BB#1:                                 # %.lr.ph60.preheader
	movq	%r14, %rax
	notq	%rax
	addq	%rbx, %rax
	shrq	%rax
	incq	%rax
	cmpq	$8, %rax
	jae	.LBB10_3
# BB#2:
	movq	%r14, %rsi
	jmp	.LBB10_12
.LBB10_3:                               # %min.iters.checked
	movq	%rax, %rdx
	andq	$-8, %rdx
	movq	%r14, %rsi
	je	.LBB10_12
# BB#4:                                 # %vector.body.preheader
	leaq	-8(%rdx), %rcx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	btl	$3, %ecx
	jb	.LBB10_5
# BB#6:                                 # %vector.body.prol
	movq	(%r14), %xmm0           # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	movq	8(%r14), %xmm1          # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	movl	$8, %edi
	testq	%rsi, %rsi
	jne	.LBB10_8
	jmp	.LBB10_10
.LBB10_5:
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	pxor	%xmm1, %xmm1
	testq	%rsi, %rsi
	je	.LBB10_10
.LBB10_8:                               # %vector.body.preheader.new
	movq	%rdx, %rsi
	subq	%rdi, %rsi
	leaq	24(%r14,%rdi,2), %rdi
	.p2align	4, 0x90
.LBB10_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rdi), %xmm2        # xmm2 = mem[0],zero
	punpcklwd	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm2
	movq	-16(%rdi), %xmm3        # xmm3 = mem[0],zero
	punpcklwd	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm3
	paddd	%xmm0, %xmm2
	paddd	%xmm1, %xmm3
	movq	-8(%rdi), %xmm0         # xmm0 = mem[0],zero
	punpcklwd	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm0
	movq	(%rdi), %xmm1           # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	paddd	%xmm2, %xmm0
	paddd	%xmm3, %xmm1
	addq	$32, %rdi
	addq	$-16, %rsi
	jne	.LBB10_9
.LBB10_10:                              # %middle.block
	paddd	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ebp
	cmpq	%rdx, %rax
	je	.LBB10_13
# BB#11:
	leaq	(%r14,%rdx,2), %rsi
	.p2align	4, 0x90
.LBB10_12:                              # %.lr.ph60
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rsi), %eax
	addq	$2, %rsi
	addl	%eax, %ebp
	cmpq	%rbx, %rsi
	jb	.LBB10_12
.LBB10_13:                              # %._crit_edge.loopexit
	movslq	%ebp, %rax
	imulq	$-2115558717, %rax, %rcx # imm = 0x81E722C3
	shrq	$32, %rcx
	addl	%eax, %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	sarl	$9, %ecx
	addl	%edx, %ecx
	imull	$1009, %ecx, %ecx       # imm = 0x3F1
	subl	%ecx, %eax
	movslq	%eax, %rbp
.LBB10_14:                              # %._crit_edge
	movabsq	$9223372032559808512, %r13 # imm = 0x7FFFFFFF00000000
	movq	state_table(%rip), %rax
	movq	(%rax,%rbp,8), %r9
	testq	%r9, %r9
	je	.LBB10_50
# BB#15:                                # %.preheader
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	movq	%rbx, %r13
	subq	%r14, %r13
	shrq	%r13
.LBB10_16:                              # %.lr.ph71
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_17 Depth 2
                                        #       Child Loop BB10_20 Depth 3
                                        #     Child Loop BB10_37 Depth 2
                                        #     Child Loop BB10_40 Depth 2
                                        #     Child Loop BB10_45 Depth 2
                                        #     Child Loop BB10_47 Depth 2
	leaq	22(%r9), %rax
	movswl	20(%r9), %edx
	.p2align	4, 0x90
.LBB10_17:                              #   Parent Loop BB10_16 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_20 Depth 3
	cmpl	%r13d, %edx
	jne	.LBB10_26
# BB#18:                                #   in Loop: Header=BB10_17 Depth=2
	movq	(%r8,%r15,8), %rsi
	movl	$1, %edi
	cmpq	%rbx, %rsi
	jae	.LBB10_24
# BB#19:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB10_17 Depth=2
	addq	$2, %rsi
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB10_20:                              # %.lr.ph
                                        #   Parent Loop BB10_16 Depth=1
                                        #     Parent Loop BB10_17 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	-2(%rsi), %ebp
	movzwl	(%rdi), %ecx
	cmpq	%rbx, %rsi
	jae	.LBB10_22
# BB#21:                                # %.lr.ph
                                        #   in Loop: Header=BB10_20 Depth=3
	addq	$2, %rdi
	addq	$2, %rsi
	cmpw	%cx, %bp
	je	.LBB10_20
.LBB10_22:                              # %.critedge
                                        #   in Loop: Header=BB10_17 Depth=2
	xorl	%edi, %edi
	cmpw	%cx, %bp
	sete	%cl
	jne	.LBB10_26
# BB#23:                                #   in Loop: Header=BB10_17 Depth=2
	movb	%cl, %dil
.LBB10_24:                              # %.critedge.thread69.backedge
                                        #   in Loop: Header=BB10_17 Depth=2
	testl	%edi, %edi
	je	.LBB10_17
	jmp	.LBB10_25
	.p2align	4, 0x90
.LBB10_26:                              # %.critedge.thread
                                        #   in Loop: Header=BB10_16 Depth=1
	movq	8(%r9), %rax
	movb	$1, %cl
	testq	%rax, %rax
	jne	.LBB10_49
# BB#27:                                #   in Loop: Header=BB10_16 Depth=1
	movq	%r9, 16(%rsp)           # 8-byte Spill
	cmpl	$32767, nstates(%rip)   # imm = 0x7FFF
	jl	.LBB10_29
# BB#28:                                #   in Loop: Header=BB10_16 Depth=1
	movl	$.L.str, %edi
	callq	toomany
	movq	kernel_base(%rip), %r8
.LBB10_29:                              #   in Loop: Header=BB10_16 Depth=1
	movq	(%r8,%r15,8), %rbp
	movq	kernel_end(%rip), %rax
	movq	(%rax,%r15,8), %r14
	movq	%r14, 8(%rsp)           # 8-byte Spill
	subq	%rbp, %r14
	movq	%r14, %r12
	shrq	%r12
	movq	%r14, %rdi
	shlq	$31, %rdi
	movabsq	$9223372032559808512, %rax # imm = 0x7FFFFFFF00000000
	addq	%rax, %rdi
	shrq	$31, %rdi
	addl	$24, %edi
	xorl	%eax, %eax
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	mallocate
	movl	4(%rsp), %ecx           # 4-byte Reload
	movw	%cx, 18(%rax)
	movl	nstates(%rip), %r10d
	movw	%r10w, 16(%rax)
	movw	%r12w, 20(%rax)
	movq	8(%rsp), %r12           # 8-byte Reload
	cmpq	%r12, %rbp
	jae	.LBB10_48
# BB#30:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB10_16 Depth=1
	leaq	22(%rax), %rdx
	decq	%r14
	movq	%r14, %r11
	shrq	%r11
	incq	%r11
	cmpq	$16, %r11
	jb	.LBB10_43
# BB#31:                                # %min.iters.checked88
                                        #   in Loop: Header=BB10_16 Depth=1
	movq	%r11, %r9
	andq	$-16, %r9
	je	.LBB10_43
# BB#32:                                # %vector.memcheck
                                        #   in Loop: Header=BB10_16 Depth=1
	andq	$-2, %r14
	leaq	2(%rbp,%r14), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB10_34
# BB#33:                                # %vector.memcheck
                                        #   in Loop: Header=BB10_16 Depth=1
	leaq	24(%rax,%r14), %rcx
	cmpq	%rcx, %rbp
	jb	.LBB10_43
.LBB10_34:                              # %vector.body82.preheader
                                        #   in Loop: Header=BB10_16 Depth=1
	leaq	-16(%r9), %r8
	movl	%r8d, %ecx
	shrl	$4, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB10_35
# BB#36:                                # %vector.body82.prol.preheader
                                        #   in Loop: Header=BB10_16 Depth=1
	negq	%rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_37:                              # %vector.body82.prol
                                        #   Parent Loop BB10_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rbp,%rsi,2), %xmm0
	movdqu	16(%rbp,%rsi,2), %xmm1
	movdqu	%xmm0, 22(%rax,%rsi,2)
	movdqu	%xmm1, 38(%rax,%rsi,2)
	addq	$16, %rsi
	incq	%rcx
	jne	.LBB10_37
	jmp	.LBB10_38
.LBB10_35:                              #   in Loop: Header=BB10_16 Depth=1
	xorl	%esi, %esi
.LBB10_38:                              # %vector.body82.prol.loopexit
                                        #   in Loop: Header=BB10_16 Depth=1
	cmpq	$48, %r8
	jb	.LBB10_41
# BB#39:                                # %vector.body82.preheader.new
                                        #   in Loop: Header=BB10_16 Depth=1
	movq	%r9, %rdi
	subq	%rsi, %rdi
	leaq	134(%rax,%rsi,2), %rcx
	leaq	112(%rbp,%rsi,2), %rsi
	.p2align	4, 0x90
.LBB10_40:                              # %vector.body82
                                        #   Parent Loop BB10_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rcx)
	movdqu	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rsi
	addq	$-64, %rdi
	jne	.LBB10_40
.LBB10_41:                              # %middle.block83
                                        #   in Loop: Header=BB10_16 Depth=1
	cmpq	%r9, %r11
	je	.LBB10_48
# BB#42:                                #   in Loop: Header=BB10_16 Depth=1
	leaq	(%rbp,%r9,2), %rbp
	leaq	(%rdx,%r9,2), %rdx
.LBB10_43:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB10_16 Depth=1
	leaq	2(%rbp), %rsi
	cmpq	%rsi, %r12
	cmovaq	%r12, %rsi
	subq	%rbp, %rsi
	decq	%rsi
	movl	%esi, %ecx
	shrl	%ecx
	incl	%ecx
	andq	$7, %rcx
	je	.LBB10_46
# BB#44:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB10_16 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB10_45:                              # %.lr.ph.i.prol
                                        #   Parent Loop BB10_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbp), %edi
	addq	$2, %rbp
	movw	%di, (%rdx)
	addq	$2, %rdx
	incq	%rcx
	jne	.LBB10_45
.LBB10_46:                              # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB10_16 Depth=1
	cmpq	$14, %rsi
	jb	.LBB10_48
	.p2align	4, 0x90
.LBB10_47:                              # %.lr.ph.i
                                        #   Parent Loop BB10_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbp), %ecx
	movw	%cx, (%rdx)
	movzwl	2(%rbp), %ecx
	movw	%cx, 2(%rdx)
	movzwl	4(%rbp), %ecx
	movw	%cx, 4(%rdx)
	movzwl	6(%rbp), %ecx
	movw	%cx, 6(%rdx)
	movzwl	8(%rbp), %ecx
	movw	%cx, 8(%rdx)
	movzwl	10(%rbp), %ecx
	movw	%cx, 10(%rdx)
	movzwl	12(%rbp), %ecx
	movw	%cx, 12(%rdx)
	movzwl	14(%rbp), %ecx
	movw	%cx, 14(%rdx)
	addq	$16, %rbp
	addq	$16, %rdx
	cmpq	%r12, %rbp
	jb	.LBB10_47
.LBB10_48:                              # %new_state.exit
                                        #   in Loop: Header=BB10_16 Depth=1
	movq	last_state(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_state(%rip)
	incl	%r10d
	movl	%r10d, nstates(%rip)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 8(%rcx)
	xorl	%ecx, %ecx
.LBB10_49:                              # %.outer.backedge
                                        #   in Loop: Header=BB10_16 Depth=1
	movq	kernel_base(%rip), %r8
	testb	%cl, %cl
	movq	%rax, %r9
	jne	.LBB10_16
	jmp	.LBB10_72
.LBB10_25:
	movq	%r9, %rax
	jmp	.LBB10_72
.LBB10_50:
	cmpl	$32767, nstates(%rip)   # imm = 0x7FFF
	jl	.LBB10_52
# BB#51:
	movl	$.L.str, %edi
	callq	toomany
	movq	kernel_base(%rip), %rax
	movq	(%rax,%r15,8), %r14
	movq	kernel_end(%rip), %rax
	movq	(%rax,%r15,8), %rbx
.LBB10_52:
	movq	%rbx, %rdi
	subq	%r14, %rdi
	movq	%rdi, %r15
	shrq	%r15
	shlq	$31, %rdi
	addq	%r13, %rdi
	shrq	$31, %rdi
	addl	$24, %edi
	xorl	%eax, %eax
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	mallocate
	movw	%r12w, 18(%rax)
	movl	nstates(%rip), %r10d
	movw	%r10w, 16(%rax)
	movw	%r15w, 20(%rax)
	cmpq	%rbx, %r14
	jae	.LBB10_71
# BB#53:                                # %.lr.ph.preheader.i48
	leaq	22(%rax), %rdx
	movq	%r14, %rcx
	notq	%rcx
	addq	%rbx, %rcx
	movq	%rcx, %r11
	shrq	%r11
	incq	%r11
	cmpq	$16, %r11
	jb	.LBB10_66
# BB#54:                                # %min.iters.checked121
	movq	%r11, %r9
	andq	$-16, %r9
	je	.LBB10_66
# BB#55:                                # %vector.memcheck135
	andq	$-2, %rcx
	leaq	2(%r14,%rcx), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB10_57
# BB#56:                                # %vector.memcheck135
	leaq	24(%rax,%rcx), %rcx
	cmpq	%rcx, %r14
	jb	.LBB10_66
.LBB10_57:                              # %vector.body115.preheader
	leaq	-16(%r9), %r8
	movl	%r8d, %ecx
	shrl	$4, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB10_58
# BB#59:                                # %vector.body115.prol.preheader
	negq	%rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB10_60:                              # %vector.body115.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r14,%rsi,2), %xmm0
	movdqu	16(%r14,%rsi,2), %xmm1
	movdqu	%xmm0, 22(%rax,%rsi,2)
	movdqu	%xmm1, 38(%rax,%rsi,2)
	addq	$16, %rsi
	incq	%rcx
	jne	.LBB10_60
	jmp	.LBB10_61
.LBB10_58:
	xorl	%esi, %esi
.LBB10_61:                              # %vector.body115.prol.loopexit
	cmpq	$48, %r8
	jb	.LBB10_64
# BB#62:                                # %vector.body115.preheader.new
	movq	%r9, %rdi
	subq	%rsi, %rdi
	leaq	134(%rax,%rsi,2), %rcx
	leaq	112(%r14,%rsi,2), %rsi
	.p2align	4, 0x90
.LBB10_63:                              # %vector.body115
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movdqu	-16(%rsi), %xmm0
	movdqu	(%rsi), %xmm1
	movdqu	%xmm0, -16(%rcx)
	movdqu	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rsi
	addq	$-64, %rdi
	jne	.LBB10_63
.LBB10_64:                              # %middle.block116
	cmpq	%r9, %r11
	je	.LBB10_71
# BB#65:
	leaq	(%r14,%r9,2), %r14
	leaq	(%rdx,%r9,2), %rdx
.LBB10_66:                              # %.lr.ph.i51.preheader
	leaq	2(%r14), %rcx
	cmpq	%rcx, %rbx
	cmovaq	%rbx, %rcx
	movq	%r14, %rsi
	notq	%rsi
	addq	%rcx, %rsi
	movl	%esi, %ecx
	shrl	%ecx
	incl	%ecx
	andq	$7, %rcx
	je	.LBB10_69
# BB#67:                                # %.lr.ph.i51.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB10_68:                              # %.lr.ph.i51.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%r14), %edi
	addq	$2, %r14
	movw	%di, (%rdx)
	addq	$2, %rdx
	incq	%rcx
	jne	.LBB10_68
.LBB10_69:                              # %.lr.ph.i51.prol.loopexit
	cmpq	$14, %rsi
	jb	.LBB10_71
	.p2align	4, 0x90
.LBB10_70:                              # %.lr.ph.i51
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%r14), %ecx
	movw	%cx, (%rdx)
	movzwl	2(%r14), %ecx
	movw	%cx, 2(%rdx)
	movzwl	4(%r14), %ecx
	movw	%cx, 4(%rdx)
	movzwl	6(%r14), %ecx
	movw	%cx, 6(%rdx)
	movzwl	8(%r14), %ecx
	movw	%cx, 8(%rdx)
	movzwl	10(%r14), %ecx
	movw	%cx, 10(%rdx)
	movzwl	12(%r14), %ecx
	movw	%cx, 12(%rdx)
	movzwl	14(%r14), %ecx
	movw	%cx, 14(%rdx)
	addq	$16, %r14
	addq	$16, %rdx
	cmpq	%rbx, %r14
	jb	.LBB10_70
.LBB10_71:                              # %new_state.exit52
	movq	last_state(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_state(%rip)
	incl	%r10d
	movl	%r10d, nstates(%rip)
	movq	state_table(%rip), %rcx
	movq	%rax, (%rcx,%rbp,8)
.LBB10_72:                              # %.loopexit
	movswl	16(%rax), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	get_state, .Lfunc_end10-get_state
	.cfi_endproc

	.globl	new_state
	.p2align	4, 0x90
	.type	new_state,@function
new_state:                              # @new_state
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 48
.Lcfi67:
	.cfi_offset %rbx, -40
.Lcfi68:
	.cfi_offset %r12, -32
.Lcfi69:
	.cfi_offset %r14, -24
.Lcfi70:
	.cfi_offset %r15, -16
	movl	%edi, %ebx
	cmpl	$32767, nstates(%rip)   # imm = 0x7FFF
	jl	.LBB11_2
# BB#1:
	movl	$.L.str, %edi
	callq	toomany
.LBB11_2:
	movq	kernel_base(%rip), %rax
	movslq	%ebx, %r15
	movq	(%rax,%r15,8), %rbx
	movq	kernel_end(%rip), %rax
	movq	(%rax,%r15,8), %r14
	movq	%r14, %rax
	subq	%rbx, %rax
	movq	%rax, %r12
	shrq	%r12
	shlq	$31, %rax
	movabsq	$9223372032559808512, %rdi # imm = 0x7FFFFFFF00000000
	addq	%rax, %rdi
	shrq	$31, %rdi
	addl	$24, %edi
	xorl	%eax, %eax
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	mallocate
	movw	%r15w, 18(%rax)
	movl	nstates(%rip), %r10d
	movw	%r10w, 16(%rax)
	movw	%r12w, 20(%rax)
	cmpq	%r14, %rbx
	jae	.LBB11_21
# BB#3:                                 # %.lr.ph.preheader
	leaq	22(%rax), %rdx
	movq	%rbx, %rcx
	notq	%rcx
	addq	%r14, %rcx
	movq	%rcx, %r11
	shrq	%r11
	incq	%r11
	cmpq	$16, %r11
	jb	.LBB11_16
# BB#4:                                 # %min.iters.checked
	movq	%r11, %r9
	andq	$-16, %r9
	je	.LBB11_16
# BB#5:                                 # %vector.memcheck
	andq	$-2, %rcx
	leaq	2(%rbx,%rcx), %rsi
	cmpq	%rsi, %rdx
	jae	.LBB11_7
# BB#6:                                 # %vector.memcheck
	leaq	24(%rax,%rcx), %rcx
	cmpq	%rcx, %rbx
	jb	.LBB11_16
.LBB11_7:                               # %vector.body.preheader
	leaq	-16(%r9), %r8
	movl	%r8d, %ecx
	shrl	$4, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB11_8
# BB#9:                                 # %vector.body.prol.preheader
	negq	%rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_10:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbx,%rsi,2), %xmm0
	movups	16(%rbx,%rsi,2), %xmm1
	movups	%xmm0, 22(%rax,%rsi,2)
	movups	%xmm1, 38(%rax,%rsi,2)
	addq	$16, %rsi
	incq	%rcx
	jne	.LBB11_10
	jmp	.LBB11_11
.LBB11_8:
	xorl	%esi, %esi
.LBB11_11:                              # %vector.body.prol.loopexit
	cmpq	$48, %r8
	jb	.LBB11_14
# BB#12:                                # %vector.body.preheader.new
	movq	%r9, %rdi
	subq	%rsi, %rdi
	leaq	134(%rax,%rsi,2), %rcx
	leaq	112(%rbx,%rsi,2), %rsi
	.p2align	4, 0x90
.LBB11_13:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rcx)
	movups	%xmm1, -96(%rcx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rcx)
	movups	%xmm1, -64(%rcx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rcx)
	movups	%xmm1, (%rcx)
	subq	$-128, %rcx
	subq	$-128, %rsi
	addq	$-64, %rdi
	jne	.LBB11_13
.LBB11_14:                              # %middle.block
	cmpq	%r9, %r11
	je	.LBB11_21
# BB#15:
	leaq	(%rbx,%r9,2), %rbx
	leaq	(%rdx,%r9,2), %rdx
.LBB11_16:                              # %.lr.ph.preheader40
	leaq	2(%rbx), %rcx
	cmpq	%rcx, %r14
	cmovaq	%r14, %rcx
	movq	%rbx, %rsi
	notq	%rsi
	addq	%rcx, %rsi
	movl	%esi, %ecx
	shrl	%ecx
	incl	%ecx
	andq	$7, %rcx
	je	.LBB11_19
# BB#17:                                # %.lr.ph.prol.preheader
	negq	%rcx
	.p2align	4, 0x90
.LBB11_18:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %edi
	addq	$2, %rbx
	movw	%di, (%rdx)
	addq	$2, %rdx
	incq	%rcx
	jne	.LBB11_18
.LBB11_19:                              # %.lr.ph.prol.loopexit
	cmpq	$14, %rsi
	jb	.LBB11_21
	.p2align	4, 0x90
.LBB11_20:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx), %ecx
	movw	%cx, (%rdx)
	movzwl	2(%rbx), %ecx
	movw	%cx, 2(%rdx)
	movzwl	4(%rbx), %ecx
	movw	%cx, 4(%rdx)
	movzwl	6(%rbx), %ecx
	movw	%cx, 6(%rdx)
	movzwl	8(%rbx), %ecx
	movw	%cx, 8(%rdx)
	movzwl	10(%rbx), %ecx
	movw	%cx, 10(%rdx)
	movzwl	12(%rbx), %ecx
	movw	%cx, 12(%rdx)
	movzwl	14(%rbx), %ecx
	movw	%cx, 14(%rdx)
	addq	$16, %rbx
	addq	$16, %rdx
	cmpq	%r14, %rbx
	jb	.LBB11_20
.LBB11_21:                              # %._crit_edge
	movq	last_state(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_state(%rip)
	incl	%r10d
	movl	%r10d, nstates(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	new_state, .Lfunc_end11-new_state
	.cfi_endproc

	.globl	insert_start_shift
	.p2align	4, 0x90
	.type	insert_start_shift,@function
insert_start_shift:                     # @insert_start_shift
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 16
	movl	$22, %edi
	xorl	%eax, %eax
	callq	mallocate
	movzwl	nstates(%rip), %ecx
	movw	%cx, 16(%rax)
	movzwl	start_symbol(%rip), %ecx
	movw	%cx, 18(%rax)
	movq	last_state(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_state(%rip)
	movl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movl	nstates(%rip), %ecx
	leal	1(%rcx), %edx
	movl	%edx, nstates(%rip)
	movw	%cx, 8(%rax)
	movw	$1, 10(%rax)
	movw	%dx, 12(%rax)
	movq	last_shift(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rax, last_shift(%rip)
	popq	%rax
	retq
.Lfunc_end12:
	.size	insert_start_shift, .Lfunc_end12-insert_start_shift
	.cfi_endproc

	.type	kernel_base,@object     # @kernel_base
	.local	kernel_base
	.comm	kernel_base,8,8
	.type	kernel_items,@object    # @kernel_items
	.local	kernel_items
	.comm	kernel_items,8,8
	.type	shift_symbol,@object    # @shift_symbol
	.local	shift_symbol
	.comm	shift_symbol,8,8
	.type	kernel_end,@object      # @kernel_end
	.local	kernel_end
	.comm	kernel_end,8,8
	.type	shiftset,@object        # @shiftset
	.local	shiftset
	.comm	shiftset,8,8
	.type	redset,@object          # @redset
	.local	redset
	.comm	redset,8,8
	.type	state_table,@object     # @state_table
	.local	state_table
	.comm	state_table,8,8
	.type	this_state,@object      # @this_state
	.local	this_state
	.comm	this_state,8,8
	.type	nshifts,@object         # @nshifts
	.local	nshifts
	.comm	nshifts,4,4
	.type	nstates,@object         # @nstates
	.comm	nstates,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"states"
	.size	.L.str, 7

	.type	last_state,@object      # @last_state
	.local	last_state
	.comm	last_state,8,8
	.type	first_state,@object     # @first_state
	.comm	first_state,8,8
	.type	last_shift,@object      # @last_shift
	.local	last_shift
	.comm	last_shift,8,8
	.type	first_shift,@object     # @first_shift
	.comm	first_shift,8,8
	.type	last_reduction,@object  # @last_reduction
	.local	last_reduction
	.comm	last_reduction,8,8
	.type	first_reduction,@object # @first_reduction
	.comm	first_reduction,8,8
	.type	final_state,@object     # @final_state
	.comm	final_state,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
