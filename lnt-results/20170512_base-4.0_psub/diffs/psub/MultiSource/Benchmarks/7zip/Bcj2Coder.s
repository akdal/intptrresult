	.text
	.file	"Bcj2Coder.bc"
	.globl	_ZN9NCompress5NBcj28CEncoder6CreateEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CEncoder6CreateEv,@function
_ZN9NCompress5NBcj28CEncoder6CreateEv:  # @_ZN9NCompress5NBcj28CEncoder6CreateEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	movl	$262144, %esi           # imm = 0x40000
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB0_10
# BB#1:
	leaq	80(%rbx), %rdi
	movl	$262144, %esi           # imm = 0x40000
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB0_10
# BB#2:
	leaq	136(%rbx), %rdi
	movl	$262144, %esi           # imm = 0x40000
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB0_10
# BB#3:
	leaq	216(%rbx), %rdi
	movl	$1048576, %esi          # imm = 0x100000
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB0_10
# BB#4:
	cmpq	$0, 16(%rbx)
	jne	.LBB0_6
# BB#5:
	movl	$131072, %edi           # imm = 0x20000
	callq	MidAlloc
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.LBB0_10
.LBB0_6:
	movb	$1, %al
	jmp	.LBB0_11
.LBB0_10:
	xorl	%eax, %eax
.LBB0_11:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN9NCompress5NBcj28CEncoder6CreateEv, .Lfunc_end0-_ZN9NCompress5NBcj28CEncoder6CreateEv
	.cfi_endproc

	.globl	_ZN9NCompress5NBcj28CEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CEncoderD2Ev,@function
_ZN9NCompress5NBcj28CEncoderD2Ev:       # @_ZN9NCompress5NBcj28CEncoderD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTVN9NCompress5NBcj28CEncoderE+16, (%rbx)
	movq	16(%rbx), %rdi
.Ltmp0:
	callq	MidFree
.Ltmp1:
# BB#1:
	leaq	216(%rbx), %rdi
.Ltmp11:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp12:
# BB#2:
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp17:
	callq	*16(%rax)
.Ltmp18:
.LBB1_4:                                # %_ZN9NCompress11NRangeCoder8CEncoderD2Ev.exit
	leaq	136(%rbx), %rdi
.Ltmp28:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp29:
# BB#5:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp34:
	callq	*16(%rax)
.Ltmp35:
.LBB1_7:                                # %_ZN10COutBufferD2Ev.exit
	leaq	80(%rbx), %rdi
.Ltmp45:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp46:
# BB#8:
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_10
# BB#9:
	movq	(%rdi), %rax
.Ltmp51:
	callq	*16(%rax)
.Ltmp52:
.LBB1_10:                               # %_ZN10COutBufferD2Ev.exit13
	leaq	24(%rbx), %rdi
.Ltmp63:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp64:
# BB#11:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_13
# BB#12:
	movq	(%rdi), %rax
.Ltmp69:
	callq	*16(%rax)
.Ltmp70:
.LBB1_13:                               # %_ZN10COutBufferD2Ev.exit18
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_51:
.Ltmp71:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_47:
.Ltmp53:
	movq	%rax, %r14
	jmp	.LBB1_35
.LBB1_43:
.Ltmp36:
	movq	%rax, %r14
	jmp	.LBB1_32
.LBB1_42:
.Ltmp19:
	movq	%rax, %r14
	jmp	.LBB1_29
.LBB1_23:
.Ltmp65:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_38
# BB#24:
	movq	(%rdi), %rax
.Ltmp66:
	callq	*16(%rax)
.Ltmp67:
	jmp	.LBB1_38
.LBB1_25:
.Ltmp68:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_20:
.Ltmp47:
	movq	%rax, %r14
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_35
# BB#21:
	movq	(%rdi), %rax
.Ltmp48:
	callq	*16(%rax)
.Ltmp49:
	jmp	.LBB1_35
.LBB1_22:
.Ltmp50:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_17:
.Ltmp30:
	movq	%rax, %r14
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_32
# BB#18:
	movq	(%rdi), %rax
.Ltmp31:
	callq	*16(%rax)
.Ltmp32:
	jmp	.LBB1_32
.LBB1_19:
.Ltmp33:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_14:
.Ltmp13:
	movq	%rax, %r14
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_29
# BB#15:
	movq	(%rdi), %rax
.Ltmp14:
	callq	*16(%rax)
.Ltmp15:
	jmp	.LBB1_29
.LBB1_16:
.Ltmp16:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_26:
.Ltmp2:
	movq	%rax, %r14
	leaq	216(%rbx), %rdi
.Ltmp3:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp4:
# BB#27:
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_29
# BB#28:
	movq	(%rdi), %rax
.Ltmp9:
	callq	*16(%rax)
.Ltmp10:
.LBB1_29:                               # %_ZN9NCompress11NRangeCoder8CEncoderD2Ev.exit23
	leaq	136(%rbx), %rdi
.Ltmp20:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp21:
# BB#30:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_32
# BB#31:
	movq	(%rdi), %rax
.Ltmp26:
	callq	*16(%rax)
.Ltmp27:
.LBB1_32:                               # %_ZN10COutBufferD2Ev.exit28
	leaq	80(%rbx), %rdi
.Ltmp37:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp38:
# BB#33:
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_35
# BB#34:
	movq	(%rdi), %rax
.Ltmp43:
	callq	*16(%rax)
.Ltmp44:
.LBB1_35:                               # %_ZN10COutBufferD2Ev.exit33
	leaq	24(%rbx), %rdi
.Ltmp54:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp55:
# BB#36:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_38
# BB#37:
	movq	(%rdi), %rax
.Ltmp60:
	callq	*16(%rax)
.Ltmp61:
.LBB1_38:                               # %_ZN10COutBufferD2Ev.exit38
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB1_39:
.Ltmp5:
	movq	%rax, %r14
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_56
# BB#40:
	movq	(%rdi), %rax
.Ltmp6:
	callq	*16(%rax)
.Ltmp7:
	jmp	.LBB1_56
.LBB1_41:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_44:
.Ltmp22:
	movq	%rax, %r14
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_56
# BB#45:
	movq	(%rdi), %rax
.Ltmp23:
	callq	*16(%rax)
.Ltmp24:
	jmp	.LBB1_56
.LBB1_46:
.Ltmp25:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_48:
.Ltmp39:
	movq	%rax, %r14
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_56
# BB#49:
	movq	(%rdi), %rax
.Ltmp40:
	callq	*16(%rax)
.Ltmp41:
	jmp	.LBB1_56
.LBB1_50:
.Ltmp42:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_52:
.Ltmp56:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_56
# BB#53:
	movq	(%rdi), %rax
.Ltmp57:
	callq	*16(%rax)
.Ltmp58:
	jmp	.LBB1_56
.LBB1_54:
.Ltmp59:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_55:
.Ltmp62:
	movq	%rax, %r14
.LBB1_56:                               # %.body21
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN9NCompress5NBcj28CEncoderD2Ev, .Lfunc_end1-_ZN9NCompress5NBcj28CEncoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\350\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\337\002"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin0   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin0   #     jumps to .Ltmp47
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin0   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin0   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin0   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp70-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp66-.Ltmp70         #   Call between .Ltmp70 and .Ltmp66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin0   #     jumps to .Ltmp68
	.byte	1                       #   On action: 1
	.long	.Ltmp48-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin0   #     jumps to .Ltmp50
	.byte	1                       #   On action: 1
	.long	.Ltmp31-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin0   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	1                       #   On action: 1
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 15 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 16 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp62-.Lfunc_begin0   #     jumps to .Ltmp62
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	1                       #   On action: 1
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp62-.Lfunc_begin0   #     jumps to .Ltmp62
	.byte	1                       #   On action: 1
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin0   #     jumps to .Ltmp39
	.byte	1                       #   On action: 1
	.long	.Ltmp43-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp62-.Lfunc_begin0   #     jumps to .Ltmp62
	.byte	1                       #   On action: 1
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin0   #     jumps to .Ltmp56
	.byte	1                       #   On action: 1
	.long	.Ltmp60-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin0   #     jumps to .Ltmp62
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp6-.Ltmp61          #   Call between .Ltmp61 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 24 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp40-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin0   #     jumps to .Ltmp42
	.byte	1                       #   On action: 1
	.long	.Ltmp57-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin0   #     jumps to .Ltmp59
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN9NCompress5NBcj28CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CEncoderD0Ev,@function
_ZN9NCompress5NBcj28CEncoderD0Ev:       # @_ZN9NCompress5NBcj28CEncoderD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp72:
	callq	_ZN9NCompress5NBcj28CEncoderD2Ev
.Ltmp73:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp74:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN9NCompress5NBcj28CEncoderD0Ev, .Lfunc_end3-_ZN9NCompress5NBcj28CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp72-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin1   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp73-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp73     #   Call between .Ltmp73 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress5NBcj28CEncoder5FlushEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CEncoder5FlushEv,@function
_ZN9NCompress5NBcj28CEncoder5FlushEv:   # @_ZN9NCompress5NBcj28CEncoder5FlushEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	leaq	24(%rbx), %rdi
	callq	_ZN10COutBuffer5FlushEv
	testl	%eax, %eax
	jne	.LBB4_14
# BB#1:
	leaq	80(%rbx), %rdi
	callq	_ZN10COutBuffer5FlushEv
	testl	%eax, %eax
	jne	.LBB4_14
# BB#2:
	leaq	136(%rbx), %rdi
	callq	_ZN10COutBuffer5FlushEv
	testl	%eax, %eax
	je	.LBB4_3
.LBB4_14:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB4_3:
	leaq	216(%rbx), %r14
	movq	200(%rbx), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB4_7
# BB#5:                                 #   in Loop: Header=BB4_4 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB4_7
# BB#6:                                 # %._crit_edge.i.i
                                        #   in Loop: Header=BB4_4 Depth=1
	movl	192(%rbx), %ecx
	incl	%ecx
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_4 Depth=1
	movb	196(%rbx), %cl
	.p2align	4, 0x90
.LBB4_8:                                # %_ZN10COutBuffer9WriteByteEh.exit._crit_edge.i.i
                                        #   Parent Loop BB4_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movq	216(%rbx), %rcx
	movl	224(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 224(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	224(%rbx), %eax
	cmpl	228(%rbx), %eax
	jne	.LBB4_10
# BB#9:                                 #   in Loop: Header=BB4_8 Depth=2
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB4_10:                               # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB4_8 Depth=2
	decl	192(%rbx)
	movq	200(%rbx), %rax
	movb	$-1, %cl
	jne	.LBB4_8
# BB#11:                                #   in Loop: Header=BB4_4 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 196(%rbx)
	movl	$1, %ecx
.LBB4_12:                               # %_ZN9NCompress11NRangeCoder8CEncoder8ShiftLowEv.exit.i
                                        #   in Loop: Header=BB4_4 Depth=1
	movl	%ecx, 192(%rbx)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 200(%rbx)
	incl	%ebp
	cmpl	$5, %ebp
	jne	.LBB4_4
# BB#13:                                # %_ZN9NCompress11NRangeCoder8CEncoder9FlushDataEv.exit
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	_ZN10COutBuffer5FlushEv # TAILCALL
.Lfunc_end4:
	.size	_ZN9NCompress5NBcj28CEncoder5FlushEv, .Lfunc_end4-_ZN9NCompress5NBcj28CEncoder5FlushEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	1024                    # 0x400
	.long	1024                    # 0x400
	.long	1024                    # 0x400
	.long	1024                    # 0x400
	.text
	.globl	_ZN9NCompress5NBcj28CEncoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CEncoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo,@function
_ZN9NCompress5NBcj28CEncoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo: # @_ZN9NCompress5NBcj28CEncoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 256
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %r13
	cmpl	$1, %ecx
	movl	$-2147024809, %ebx      # imm = 0x80070057
	jne	.LBB5_152
# BB#1:
	cmpl	$4, 256(%rsp)
	jne	.LBB5_152
# BB#2:
	leaq	24(%r13), %rdi
	movl	$262144, %esi           # imm = 0x40000
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	callq	_ZN10COutBuffer6CreateEj
	movl	$-2147024882, %ebx      # imm = 0x8007000E
	testb	%al, %al
	je	.LBB5_152
# BB#3:
	leaq	80(%r13), %rdi
	movl	$262144, %esi           # imm = 0x40000
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB5_152
# BB#4:
	leaq	136(%r13), %rdi
	movl	$262144, %esi           # imm = 0x40000
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB5_152
# BB#5:
	leaq	216(%r13), %rdi
	movl	$1048576, %esi          # imm = 0x100000
	movq	%rdi, %r12
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB5_152
# BB#6:
	cmpq	$0, 16(%r13)
	jne	.LBB5_8
# BB#7:
	movl	$131072, %edi           # imm = 0x20000
	callq	MidAlloc
	movq	%rax, 16(%r13)
	testq	%rax, %rax
	je	.LBB5_152
.LBB5_8:                                # %_ZN9NCompress5NBcj28CEncoder6CreateEv.exit
	testq	%rbp, %rbp
	je	.LBB5_11
# BB#9:
	movq	(%rbp), %rax
	testq	%rax, %rax
	movq	%r12, %rbx
	je	.LBB5_12
# BB#10:
	movq	(%rax), %rcx
	movq	%rcx, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	cmpq	$16777217, %rcx         # imm = 0x1000001
	setb	%al
	jmp	.LBB5_13
.LBB5_11:
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%r12, %rbx
	jmp	.LBB5_14
.LBB5_12:
	xorl	%eax, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
.LBB5_13:
	movq	%rax, 88(%rsp)          # 8-byte Spill
.LBB5_14:
	movq	(%r14), %r12
	movq	(%r15), %rsi
.Ltmp75:
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%r13, 8(%rsp)           # 8-byte Spill
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
.Ltmp76:
# BB#15:
.Ltmp77:
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer4InitEv
.Ltmp78:
# BB#16:
	movq	8(%r15), %rsi
.Ltmp79:
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
.Ltmp80:
# BB#17:
.Ltmp81:
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	_ZN10COutBuffer4InitEv
.Ltmp82:
# BB#18:
	movq	16(%r15), %rsi
.Ltmp83:
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
.Ltmp84:
# BB#19:
.Ltmp85:
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	_ZN10COutBuffer4InitEv
.Ltmp86:
# BB#20:
	movq	24(%r15), %rsi
.Ltmp87:
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
.Ltmp88:
# BB#21:                                # %_ZN9NCompress11NRangeCoder8CEncoder9SetStreamEP20ISequentialOutStream.exit
.Ltmp89:
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer4InitEv
.Ltmp90:
# BB#22:                                # %scalar.ph
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	$0, 200(%r13)
	movl	$-1, 208(%r13)
	movl	$1, 192(%r13)
	movb	$0, 196(%r13)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1024,1024,1024,1024]
	movups	%xmm0, 272(%r13)
	movups	%xmm0, 288(%r13)
	movups	%xmm0, 304(%r13)
	movups	%xmm0, 320(%r13)
	movups	%xmm0, 336(%r13)
	movups	%xmm0, 352(%r13)
	movups	%xmm0, 368(%r13)
	movups	%xmm0, 384(%r13)
	movups	%xmm0, 400(%r13)
	movups	%xmm0, 416(%r13)
	movups	%xmm0, 432(%r13)
	movups	%xmm0, 448(%r13)
	movups	%xmm0, 464(%r13)
	movups	%xmm0, 480(%r13)
	movups	%xmm0, 496(%r13)
	movups	%xmm0, 512(%r13)
	movups	%xmm0, 528(%r13)
	movups	%xmm0, 544(%r13)
	movups	%xmm0, 560(%r13)
	movups	%xmm0, 576(%r13)
	movups	%xmm0, 592(%r13)
	movups	%xmm0, 608(%r13)
	movups	%xmm0, 624(%r13)
	movups	%xmm0, 640(%r13)
	movups	%xmm0, 656(%r13)
	movups	%xmm0, 672(%r13)
	movups	%xmm0, 688(%r13)
	movups	%xmm0, 704(%r13)
	movups	%xmm0, 720(%r13)
	movups	%xmm0, 736(%r13)
	movups	%xmm0, 752(%r13)
	movups	%xmm0, 768(%r13)
	movups	%xmm0, 784(%r13)
	movups	%xmm0, 800(%r13)
	movups	%xmm0, 816(%r13)
	movups	%xmm0, 832(%r13)
	movups	%xmm0, 848(%r13)
	movups	%xmm0, 864(%r13)
	movups	%xmm0, 880(%r13)
	movups	%xmm0, 896(%r13)
	movups	%xmm0, 912(%r13)
	movups	%xmm0, 928(%r13)
	movups	%xmm0, 944(%r13)
	movups	%xmm0, 960(%r13)
	movups	%xmm0, 976(%r13)
	movups	%xmm0, 992(%r13)
	movups	%xmm0, 1008(%r13)
	movups	%xmm0, 1024(%r13)
	movups	%xmm0, 1040(%r13)
	movups	%xmm0, 1056(%r13)
	movups	%xmm0, 1072(%r13)
	movups	%xmm0, 1088(%r13)
	movups	%xmm0, 1104(%r13)
	movups	%xmm0, 1120(%r13)
	movups	%xmm0, 1136(%r13)
	movups	%xmm0, 1152(%r13)
	movups	%xmm0, 1168(%r13)
	movups	%xmm0, 1184(%r13)
	movups	%xmm0, 1200(%r13)
	movups	%xmm0, 1216(%r13)
	movups	%xmm0, 1232(%r13)
	movups	%xmm0, 1248(%r13)
	movups	%xmm0, 1264(%r13)
	movups	%xmm0, 1280(%r13)
	movabsq	$4398046512128, %rax    # imm = 0x40000000400
	movq	%rax, 1296(%r13)
	movq	$0, 24(%rsp)
	movq	(%r12), %rax
.Ltmp92:
	leaq	24(%rsp), %rdx
	movl	$IID_ICompressGetSubStreamSize, %esi
	movq	%r12, %rdi
	callq	*(%rax)
.Ltmp93:
# BB#23:
	movq	$0, 136(%rsp)
	xorl	%eax, %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	112(%rsp), %r15
	movl	$0, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
                                        # implicit-def: %EAX
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	%r12, 168(%rsp)         # 8-byte Spill
	jmp	.LBB5_25
.LBB5_24:                               #   in Loop: Header=BB5_25 Depth=1
	movl	%r8d, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
.LBB5_25:                               # %.thread370.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_26 Depth 2
                                        #       Child Loop BB5_27 Depth 3
                                        #     Child Loop BB5_40 Depth 2
                                        #       Child Loop BB5_41 Depth 3
                                        #       Child Loop BB5_52 Depth 3
                                        #       Child Loop BB5_98 Depth 3
                                        #       Child Loop BB5_80 Depth 3
                                        #     Child Loop BB5_111 Depth 2
                                        #     Child Loop BB5_116 Depth 2
	movl	$131072, %ebx           # imm = 0x20000
	subl	%r9d, %ebx
	movl	%r9d, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
.LBB5_26:                               # %.thread370
                                        #   Parent Loop BB5_25 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_27 Depth 3
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_27:                               #   Parent Loop BB5_25 Depth=1
                                        #     Parent Loop BB5_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%ebx, %edx
	subl	%esi, %edx
	je	.LBB5_35
# BB#28:                                #   in Loop: Header=BB5_27 Depth=3
	movq	(%r12), %rax
	movq	16(%r13), %rcx
	addq	104(%rsp), %rcx         # 8-byte Folded Reload
	movq	%rsi, %rbp
	movl	%esi, %esi
	addq	%rcx, %rsi
.Ltmp95:
	movq	%r12, %rdi
	movq	%r15, %rcx
	callq	*40(%rax)
.Ltmp96:
# BB#29:                                #   in Loop: Header=BB5_27 Depth=3
	testl	%eax, %eax
	je	.LBB5_31
# BB#30:                                #   in Loop: Header=BB5_27 Depth=3
	movl	$1, %ecx
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	%rbp, %rsi
	jmp	.LBB5_32
	.p2align	4, 0x90
.LBB5_31:                               #   in Loop: Header=BB5_27 Depth=3
	movl	112(%rsp), %eax
	movq	%rbp, %rsi
	addl	%eax, %esi
	testl	%eax, %eax
	movl	$0, %ecx
	movl	$7, %eax
	cmovel	%eax, %ecx
.LBB5_32:                               #   in Loop: Header=BB5_27 Depth=3
	movl	%ecx, %eax
	andb	$7, %al
	je	.LBB5_27
# BB#33:                                #   in Loop: Header=BB5_26 Depth=2
	cmpb	$7, %al
	je	.LBB5_35
# BB#34:                                # %.loopexit373
                                        #   in Loop: Header=BB5_26 Depth=2
	testl	%ecx, %ecx
	je	.LBB5_26
	jmp	.LBB5_142
.LBB5_35:                               # %.loopexit374
                                        #   in Loop: Header=BB5_25 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rsi), %eax
	cmpl	$4, %eax
	jbe	.LBB5_118
# BB#36:                                #   in Loop: Header=BB5_25 Depth=1
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leal	-5(%rax), %r12d
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	5(%rax), %eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	jmp	.LBB5_40
.LBB5_37:                               #   in Loop: Header=BB5_40 Depth=2
	cmpb	$0, 88(%rsp)            # 1-byte Folded Reload
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	je	.LBB5_39
# BB#38:                                #   in Loop: Header=BB5_40 Depth=2
	movl	44(%rsp), %eax          # 4-byte Reload
	cmpq	80(%rsp), %rax          # 8-byte Folded Reload
	setb	%al
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	jmp	.LBB5_70
.LBB5_39:                               #   in Loop: Header=BB5_40 Depth=2
	movl	16(%rsp), %eax          # 4-byte Reload
	testb	%al, %al
	sete	%dl
	cmpb	$-1, %al
	sete	%al
	orb	%dl, %al
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	jmp	.LBB5_70
	.p2align	4, 0x90
.LBB5_40:                               # %.lr.ph465
                                        #   Parent Loop BB5_25 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_41 Depth 3
                                        #       Child Loop BB5_52 Depth 3
                                        #       Child Loop BB5_98 Depth 3
                                        #       Child Loop BB5_80 Depth 3
	xorl	%r15d, %r15d
	movl	%r14d, %ebx
	.p2align	4, 0x90
.LBB5_41:                               #   Parent Loop BB5_25 Depth=1
                                        #     Parent Loop BB5_40 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rsi,%r15), %ebp
	movq	16(%r13), %rax
	movq	24(%r13), %rcx
	movb	(%rax,%rbp), %r14b
	movl	32(%r13), %eax
	leal	1(%rax), %edx
	movl	%edx, 32(%r13)
	movb	%r14b, (%rcx,%rax)
	movl	32(%r13), %eax
	cmpl	36(%r13), %eax
	jne	.LBB5_43
# BB#42:                                #   in Loop: Header=BB5_41 Depth=3
.Ltmp98:
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%r14, %r13
	movl	%r12d, %r14d
	movq	%rsi, %r12
	callq	_ZN10COutBuffer14FlushWithCheckEv
	movq	%r12, %rsi
	movl	%r14d, %r12d
	movq	%r13, %r14
.Ltmp99:
.LBB5_43:                               # %_ZN10COutBuffer9WriteByteEh.exit318
                                        #   in Loop: Header=BB5_41 Depth=3
	movl	%r14d, %eax
	andb	$-2, %al
	cmpb	$-24, %al
	je	.LBB5_48
# BB#44:                                # %_ZN9NCompress5NBcj23IsJEhh.exit
                                        #   in Loop: Header=BB5_41 Depth=3
	cmpb	$15, %bl
	jne	.LBB5_46
# BB#45:                                # %_ZN9NCompress5NBcj23IsJEhh.exit
                                        #   in Loop: Header=BB5_41 Depth=3
	movl	%r14d, %eax
	andb	$-16, %al
	cmpb	$-128, %al
	je	.LBB5_47
.LBB5_46:                               # %_ZN9NCompress11NRangeCoder11CBitEncoderILi5EE6EncodeEPNS0_8CEncoderEj.exit356.thread
                                        #   in Loop: Header=BB5_41 Depth=3
	leal	1(%r15), %eax
	leal	1(%rsi,%r15), %ecx
	cmpl	%r12d, %ecx
	movl	%eax, %r15d
	movl	%r14d, %ebx
	movq	8(%rsp), %r13           # 8-byte Reload
	jbe	.LBB5_41
	jmp	.LBB5_104
.LBB5_47:                               #   in Loop: Header=BB5_40 Depth=2
	movb	$15, %bl
.LBB5_48:                               # %_ZN9NCompress5NBcj23IsJEhh.exit.thread
                                        #   in Loop: Header=BB5_40 Depth=2
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	16(%r13), %rax
	leal	4(%rsi,%r15), %ecx
	movzbl	(%rax,%rcx), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	shll	$24, %ecx
	leal	3(%rsi,%r15), %edx
	movzbl	(%rax,%rdx), %edx
	shll	$16, %edx
	orl	%ecx, %edx
	leal	2(%rsi,%r15), %ecx
	movzbl	(%rax,%rcx), %ecx
	shll	$8, %ecx
	orl	%edx, %ecx
	leal	1(%rsi,%r15), %edx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movzbl	(%rax,%rdx), %r8d
	orl	%ecx, %r8d
	movq	192(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rsi), %eax
	addl	%r8d, %eax
	addl	%r15d, %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	je	.LBB5_56
# BB#49:                                #   in Loop: Header=BB5_40 Depth=2
	addq	136(%rsp), %rbp
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpq	%rbp, %rax
	jae	.LBB5_58
# BB#50:                                # %.lr.ph480.preheader
                                        #   in Loop: Header=BB5_40 Depth=2
	movl	%r8d, 32(%rsp)          # 4-byte Spill
	movl	%r12d, 148(%rsp)        # 4-byte Spill
	movq	152(%rsp), %rsi         # 8-byte Reload
	jmp	.LBB5_52
	.p2align	4, 0x90
.LBB5_51:                               # %_ZN9CMyComPtrI25ICompressGetSubStreamSizeE7ReleaseEv.exit.thread..lr.ph480_crit_edge
                                        #   in Loop: Header=BB5_52 Depth=3
	movq	%r12, %rsi
	incq	%rsi
	movq	24(%rsp), %rdi
	movq	%rcx, 56(%rsp)          # 8-byte Spill
.LBB5_52:                               # %.lr.ph480
                                        #   Parent Loop BB5_25 Depth=1
                                        #     Parent Loop BB5_40 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdi), %rax
.Ltmp101:
	movq	%rsi, %r12
	leaq	112(%rsp), %rdx
	callq	*40(%rax)
.Ltmp102:
# BB#53:                                #   in Loop: Header=BB5_52 Depth=3
	testl	%eax, %eax
	jne	.LBB5_59
# BB#54:                                # %_ZN9CMyComPtrI25ICompressGetSubStreamSizeE7ReleaseEv.exit.thread
                                        #   in Loop: Header=BB5_52 Depth=3
	movq	112(%rsp), %rcx
	movq	56(%rsp), %rax          # 8-byte Reload
	addq	%rax, %rcx
	cmpq	%rbp, %rcx
	jb	.LBB5_51
# BB#55:                                # %._crit_edge481.loopexit
                                        #   in Loop: Header=BB5_40 Depth=2
	movq	%rax, %rdx
	movq	%r12, %rax
	incq	%rax
	movq	%rdx, %rsi
	movl	32(%rsp), %r8d          # 4-byte Reload
	jmp	.LBB5_65
.LBB5_56:                               #   in Loop: Header=BB5_40 Depth=2
	cmpb	$0, 88(%rsp)            # 1-byte Folded Reload
	je	.LBB5_68
# BB#57:                                #   in Loop: Header=BB5_40 Depth=2
	movl	44(%rsp), %eax          # 4-byte Reload
	cmpq	80(%rsp), %rax          # 8-byte Folded Reload
	setb	%al
	jmp	.LBB5_70
.LBB5_58:                               #   in Loop: Header=BB5_40 Depth=2
	movq	32(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB5_67
.LBB5_59:                               #   in Loop: Header=BB5_40 Depth=2
	cmpl	$-2147467263, %eax      # imm = 0x80004001
	movl	32(%rsp), %r8d          # 4-byte Reload
	je	.LBB5_61
# BB#60:                                #   in Loop: Header=BB5_40 Depth=2
	cmpl	$1, %eax
	jne	.LBB5_141
.LBB5_61:                               #   in Loop: Header=BB5_40 Depth=2
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	movq	%r12, %rax
	je	.LBB5_64
# BB#62:                                #   in Loop: Header=BB5_40 Depth=2
	movq	(%rdi), %rax
.Ltmp104:
	callq	*16(%rax)
.Ltmp105:
# BB#63:                                # %.noexc323
                                        #   in Loop: Header=BB5_40 Depth=2
	movq	$0, 24(%rsp)
	movq	%r12, %rax
	movl	32(%rsp), %r8d          # 4-byte Reload
.LBB5_64:                               # %_ZN9CMyComPtrI25ICompressGetSubStreamSizeE7ReleaseEv.exit.thread.thread
                                        #   in Loop: Header=BB5_40 Depth=2
	movq	$-1, %rcx
	xorl	%esi, %esi
.LBB5_65:                               # %._crit_edge481
                                        #   in Loop: Header=BB5_40 Depth=2
	cmpq	$0, 24(%rsp)
	movl	148(%rsp), %r12d        # 4-byte Reload
	movq	%rax, 152(%rsp)         # 8-byte Spill
	je	.LBB5_37
# BB#66:                                #   in Loop: Header=BB5_40 Depth=2
	movq	%rcx, %rax
.LBB5_67:                               # %._crit_edge481.thread
                                        #   in Loop: Header=BB5_40 Depth=2
	movq	%rax, %rcx
	subq	%rsi, %rax
	cmpq	$16777217, %rax         # imm = 0x1000001
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	jb	.LBB5_69
.LBB5_68:                               #   in Loop: Header=BB5_40 Depth=2
	movl	16(%rsp), %eax          # 4-byte Reload
	testb	%al, %al
	sete	%cl
	cmpb	$-1, %al
	sete	%al
	orb	%cl, %al
	jmp	.LBB5_70
.LBB5_69:                               #   in Loop: Header=BB5_40 Depth=2
	movslq	%r8d, %rax
	leaq	5(%rax,%rbp), %rax
	cmpq	%rcx, %rax
	sbbb	%cl, %cl
	cmpq	%rsi, %rax
	setae	%al
	andb	%cl, %al
.LBB5_70:                               # %.thread365
                                        #   in Loop: Header=BB5_40 Depth=2
	movq	48(%rsp), %rbp          # 8-byte Reload
	xorl	%edx, %edx
	cmpb	$-23, %r14b
	setne	%dl
	orq	$256, %rdx              # imm = 0x100
	cmpb	$-24, %r14b
	movzbl	%bl, %ecx
	cmovneq	%rdx, %rcx
	movl	208(%r13), %esi
	movl	%esi, %edx
	shrl	$11, %edx
	movl	272(%r13,%rcx,4), %edi
	imull	%edi, %edx
	testb	%al, %al
	je	.LBB5_75
# BB#71:                                #   in Loop: Header=BB5_40 Depth=2
	movl	%edx, %eax
	addq	200(%r13), %rax
	movq	%rax, 200(%r13)
	subl	%edx, %esi
	movl	%esi, 208(%r13)
	movl	%edi, %edx
	shrl	$5, %edx
	subl	%edx, %edi
	movl	%edi, 272(%r13,%rcx,4)
	cmpl	$16777215, %esi         # imm = 0xFFFFFF
	movq	64(%rsp), %rbx          # 8-byte Reload
	ja	.LBB5_85
# BB#72:                                #   in Loop: Header=BB5_40 Depth=2
	shll	$8, %esi
	movl	%esi, 208(%r13)
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB5_79
# BB#73:                                #   in Loop: Header=BB5_40 Depth=2
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB5_79
# BB#74:                                # %._crit_edge.i.i335
                                        #   in Loop: Header=BB5_40 Depth=2
	movl	192(%r13), %ecx
	incl	%ecx
	jmp	.LBB5_84
.LBB5_75:                               #   in Loop: Header=BB5_40 Depth=2
	movl	%edx, 208(%r13)
	movl	$2048, %eax             # imm = 0x800
	subl	%edi, %eax
	shrl	$5, %eax
	addl	%edi, %eax
	movl	%eax, 272(%r13,%rcx,4)
	cmpl	$16777215, %edx         # imm = 0xFFFFFF
	movq	64(%rsp), %rbx          # 8-byte Reload
	ja	.LBB5_103
# BB#76:                                #   in Loop: Header=BB5_40 Depth=2
	shll	$8, %edx
	movl	%edx, 208(%r13)
	movq	200(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB5_97
# BB#77:                                #   in Loop: Header=BB5_40 Depth=2
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB5_97
# BB#78:                                # %._crit_edge.i.i349
                                        #   in Loop: Header=BB5_40 Depth=2
	movl	192(%r13), %ecx
	incl	%ecx
	jmp	.LBB5_102
.LBB5_79:                               #   in Loop: Header=BB5_40 Depth=2
	movb	196(%r13), %cl
	.p2align	4, 0x90
.LBB5_80:                               # %_ZN10COutBuffer9WriteByteEh.exit._crit_edge.i.i337
                                        #   Parent Loop BB5_25 Depth=1
                                        #     Parent Loop BB5_40 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shrq	$32, %rax
	addb	%cl, %al
	movq	216(%r13), %rcx
	movl	224(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 224(%r13)
	movb	%al, (%rcx,%rdx)
	movl	224(%r13), %eax
	cmpl	228(%r13), %eax
	jne	.LBB5_82
# BB#81:                                #   in Loop: Header=BB5_80 Depth=3
.Ltmp110:
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp111:
.LBB5_82:                               # %_ZN10COutBuffer9WriteByteEh.exit.i.i338
                                        #   in Loop: Header=BB5_80 Depth=3
	decl	192(%r13)
	movq	200(%r13), %rax
	movb	$-1, %cl
	jne	.LBB5_80
# BB#83:                                #   in Loop: Header=BB5_40 Depth=2
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 196(%r13)
	movl	$1, %ecx
.LBB5_84:                               # %_ZN9NCompress11NRangeCoder8CEncoder8ShiftLowEv.exit.i340
                                        #   in Loop: Header=BB5_40 Depth=2
	movl	%ecx, 192(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 200(%r13)
.LBB5_85:                               # %_ZN9NCompress11NRangeCoder11CBitEncoderILi5EE6EncodeEPNS0_8CEncoderEj.exit342
                                        #   in Loop: Header=BB5_40 Depth=2
	cmpb	$-24, %r14b
	movq	120(%rsp), %r14         # 8-byte Reload
	cmoveq	128(%rsp), %r14         # 8-byte Folded Reload
	movl	44(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %eax
	shrl	$24, %eax
	movq	(%r14), %rcx
	movl	8(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 8(%r14)
	movb	%al, (%rcx,%rdx)
	movl	8(%r14), %eax
	cmpl	12(%r14), %eax
	jne	.LBB5_88
# BB#86:                                #   in Loop: Header=BB5_40 Depth=2
.Ltmp113:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp114:
# BB#87:                                # %._ZN10COutBuffer9WriteByteEh.exit344_crit_edge
                                        #   in Loop: Header=BB5_40 Depth=2
	movl	8(%r14), %eax
.LBB5_88:                               # %_ZN10COutBuffer9WriteByteEh.exit344
                                        #   in Loop: Header=BB5_40 Depth=2
	movl	%ebx, %ecx
	shrl	$16, %ecx
	movq	(%r14), %rdx
	leal	1(%rax), %esi
	movl	%esi, 8(%r14)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	8(%r14), %eax
	cmpl	12(%r14), %eax
	jne	.LBB5_91
# BB#89:                                #   in Loop: Header=BB5_40 Depth=2
.Ltmp115:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp116:
# BB#90:                                # %._ZN10COutBuffer9WriteByteEh.exit344.1_crit_edge
                                        #   in Loop: Header=BB5_40 Depth=2
	movl	8(%r14), %eax
.LBB5_91:                               # %_ZN10COutBuffer9WriteByteEh.exit344.1
                                        #   in Loop: Header=BB5_40 Depth=2
	movq	(%r14), %rcx
	leal	1(%rax), %edx
	movl	%edx, 8(%r14)
	movl	%eax, %eax
	movb	%bh, (%rcx,%rax)  # NOREX
	movl	8(%r14), %eax
	cmpl	12(%r14), %eax
	jne	.LBB5_94
# BB#92:                                #   in Loop: Header=BB5_40 Depth=2
.Ltmp117:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp118:
# BB#93:                                # %._ZN10COutBuffer9WriteByteEh.exit344.2_crit_edge
                                        #   in Loop: Header=BB5_40 Depth=2
	movl	8(%r14), %eax
.LBB5_94:                               # %_ZN10COutBuffer9WriteByteEh.exit344.2
                                        #   in Loop: Header=BB5_40 Depth=2
	movq	184(%rsp), %rcx         # 8-byte Reload
	leal	5(%rcx,%r15), %ebp
	movq	(%r14), %rcx
	leal	1(%rax), %edx
	movl	%edx, 8(%r14)
	movl	%eax, %eax
	movb	%bl, (%rcx,%rax)
	movl	8(%r14), %eax
	cmpl	12(%r14), %eax
	jne	.LBB5_96
# BB#95:                                #   in Loop: Header=BB5_40 Depth=2
.Ltmp119:
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp120:
	movl	16(%rsp), %eax          # 4-byte Reload
	movb	%al, %r14b
	jmp	.LBB5_103
.LBB5_96:                               #   in Loop: Header=BB5_40 Depth=2
	movl	16(%rsp), %eax          # 4-byte Reload
	movb	%al, %r14b
	jmp	.LBB5_103
.LBB5_97:                               #   in Loop: Header=BB5_40 Depth=2
	movb	196(%r13), %cl
	.p2align	4, 0x90
.LBB5_98:                               # %_ZN10COutBuffer9WriteByteEh.exit._crit_edge.i.i351
                                        #   Parent Loop BB5_25 Depth=1
                                        #     Parent Loop BB5_40 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	shrq	$32, %rax
	addb	%cl, %al
	movq	216(%r13), %rcx
	movl	224(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 224(%r13)
	movb	%al, (%rcx,%rdx)
	movl	224(%r13), %eax
	cmpl	228(%r13), %eax
	jne	.LBB5_100
# BB#99:                                #   in Loop: Header=BB5_98 Depth=3
.Ltmp107:
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp108:
.LBB5_100:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i352
                                        #   in Loop: Header=BB5_98 Depth=3
	decl	192(%r13)
	movq	200(%r13), %rax
	movb	$-1, %cl
	jne	.LBB5_98
# BB#101:                               #   in Loop: Header=BB5_40 Depth=2
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 196(%r13)
	movl	$1, %ecx
.LBB5_102:                              # %_ZN9NCompress11NRangeCoder8CEncoder8ShiftLowEv.exit.i354
                                        #   in Loop: Header=BB5_40 Depth=2
	movl	%ecx, 192(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 200(%r13)
.LBB5_103:                              # %_ZN9NCompress11NRangeCoder11CBitEncoderILi5EE6EncodeEPNS0_8CEncoderEj.exit356.thread.outer.backedge
                                        #   in Loop: Header=BB5_40 Depth=2
	cmpl	%r12d, %ebp
	movl	%ebp, %esi
	jbe	.LBB5_40
	jmp	.LBB5_105
.LBB5_104:                              # %_ZN9NCompress11NRangeCoder11CBitEncoderILi5EE6EncodeEPNS0_8CEncoderEj.exit356.thread.outer._crit_edge.loopexit
                                        #   in Loop: Header=BB5_25 Depth=1
	addl	%eax, %esi
	movl	%esi, %ebp
.LBB5_105:                              # %_ZN9NCompress11NRangeCoder11CBitEncoderILi5EE6EncodeEPNS0_8CEncoderEj.exit356.thread.outer._crit_edge
                                        #   in Loop: Header=BB5_25 Depth=1
	movl	%ebp, %ebx
	addq	%rbx, 136(%rsp)
	cmpq	$0, 264(%rsp)
	movq	168(%rsp), %r12         # 8-byte Reload
	leaq	112(%rsp), %r15
	je	.LBB5_108
# BB#106:                               #   in Loop: Header=BB5_25 Depth=1
	movq	264(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp122:
	xorl	%edx, %edx
	leaq	136(%rsp), %rsi
	callq	*40(%rax)
.Ltmp123:
# BB#107:                               #   in Loop: Header=BB5_25 Depth=1
	testl	%eax, %eax
	jne	.LBB5_141
.LBB5_108:                              # %.preheader372
                                        #   in Loop: Header=BB5_25 Depth=1
	addl	72(%rsp), %ebp          # 4-byte Folded Reload
	xorl	%r9d, %r9d
	movq	176(%rsp), %rcx         # 8-byte Reload
	cmpl	%ebx, %ecx
	movl	%ebp, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jbe	.LBB5_25
# BB#109:                               # %.lr.ph494.preheader
                                        #   in Loop: Header=BB5_25 Depth=1
	movl	%ecx, %r9d
	subl	%ebx, %r9d
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	leal	-1(%rax,%rcx), %r8d
	subl	%ebx, %r8d
	testb	$3, %r9b
	je	.LBB5_113
# BB#110:                               # %.lr.ph494.prol.preheader
                                        #   in Loop: Header=BB5_25 Depth=1
	movq	%rbp, %rcx
	movl	%r9d, %edx
	andl	$3, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_111:                              # %.lr.ph494.prol
                                        #   Parent Loop BB5_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rdi
	leaq	(%rdi,%rbx), %rbp
	movzbl	(%rsi,%rbp), %eax
	movb	%al, (%rdi,%rsi)
	incq	%rsi
	cmpl	%esi, %edx
	jne	.LBB5_111
# BB#112:                               # %.lr.ph494.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB5_25 Depth=1
	leaq	(%rbx,%rsi), %rdi
	movq	%rcx, %rbp
	jmp	.LBB5_114
.LBB5_113:                              #   in Loop: Header=BB5_25 Depth=1
	movq	%rbx, %rdi
	xorl	%esi, %esi
.LBB5_114:                              # %.lr.ph494.prol.loopexit
                                        #   in Loop: Header=BB5_25 Depth=1
	cmpl	$3, %r8d
	movl	%ebp, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jb	.LBB5_25
# BB#115:                               # %.lr.ph494.preheader.new
                                        #   in Loop: Header=BB5_25 Depth=1
	movq	%rbp, %r8
	movq	160(%rsp), %rcx         # 8-byte Reload
	addl	104(%rsp), %ecx         # 4-byte Folded Reload
	subl	%ebx, %ecx
	subl	%esi, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_116:                              # %.lr.ph494
                                        #   Parent Loop BB5_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %rdx
	leaq	(%rdx,%rdi), %rbp
	movzbl	(%rax,%rbp), %ebx
	addq	%rsi, %rdx
	movb	%bl, (%rax,%rdx)
	movq	16(%r13), %rdx
	leaq	1(%rdx,%rdi), %rbp
	movzbl	(%rax,%rbp), %ebx
	leaq	1(%rdx,%rsi), %rdx
	movb	%bl, (%rax,%rdx)
	movq	16(%r13), %rdx
	leaq	2(%rdx,%rdi), %rbp
	movzbl	(%rax,%rbp), %ebx
	leaq	2(%rdx,%rsi), %rdx
	movb	%bl, (%rax,%rdx)
	movq	16(%r13), %rdx
	leaq	3(%rdx,%rdi), %rbp
	movzbl	(%rax,%rbp), %ebx
	leaq	3(%rdx,%rsi), %rdx
	movb	%bl, (%rax,%rdx)
	addq	$4, %rax
	cmpl	%eax, %ecx
	jne	.LBB5_116
	jmp	.LBB5_24
.LBB5_141:
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jmp	.LBB5_142
.LBB5_118:                              # %.preheader
	testl	%eax, %eax
	je	.LBB5_140
# BB#119:                               # %.lr.ph
	movl	%eax, %r15d
	xorl	%ebp, %ebp
.LBB5_120:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_134 Depth 2
	movl	%r14d, %ebx
	movq	16(%r13), %rax
	movq	24(%r13), %rcx
	movb	(%rax,%rbp), %r14b
	movl	32(%r13), %eax
	leal	1(%rax), %edx
	movl	%edx, 32(%r13)
	movb	%r14b, (%rcx,%rax)
	movl	32(%r13), %eax
	cmpl	36(%r13), %eax
	jne	.LBB5_122
# BB#121:                               #   in Loop: Header=BB5_120 Depth=1
.Ltmp125:
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp126:
.LBB5_122:                              # %_ZN10COutBuffer9WriteByteEh.exit
                                        #   in Loop: Header=BB5_120 Depth=1
	cmpb	$-23, %r14b
	je	.LBB5_125
# BB#123:                               # %_ZN10COutBuffer9WriteByteEh.exit
                                        #   in Loop: Header=BB5_120 Depth=1
	cmpb	$-24, %r14b
	jne	.LBB5_126
# BB#124:                               #   in Loop: Header=BB5_120 Depth=1
	movzbl	%bl, %ecx
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB5_129
.LBB5_125:                              #   in Loop: Header=BB5_120 Depth=1
	movl	$256, %ecx              # imm = 0x100
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB5_129
.LBB5_126:                              #   in Loop: Header=BB5_120 Depth=1
	cmpb	$15, %bl
	movq	64(%rsp), %rbx          # 8-byte Reload
	jne	.LBB5_139
# BB#127:                               #   in Loop: Header=BB5_120 Depth=1
	movl	%r14d, %eax
	andb	$-16, %al
	cmpb	$-128, %al
	jne	.LBB5_139
# BB#128:                               #   in Loop: Header=BB5_120 Depth=1
	movl	$257, %ecx              # imm = 0x101
.LBB5_129:                              #   in Loop: Header=BB5_120 Depth=1
	movl	208(%r13), %eax
	shrl	$11, %eax
	movl	272(%r13,%rcx,4), %edx
	imull	%edx, %eax
	movl	%eax, 208(%r13)
	movl	$2048, %esi             # imm = 0x800
	subl	%edx, %esi
	shrl	$5, %esi
	addl	%edx, %esi
	movl	%esi, 272(%r13,%rcx,4)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB5_139
# BB#130:                               #   in Loop: Header=BB5_120 Depth=1
	shll	$8, %eax
	movl	%eax, 208(%r13)
	movq	200(%r13), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	jne	.LBB5_133
# BB#131:                               #   in Loop: Header=BB5_120 Depth=1
	cmpl	$-16777216, %eax        # imm = 0xFF000000
	jb	.LBB5_133
# BB#132:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB5_120 Depth=1
	movl	192(%r13), %ecx
	incl	%ecx
	jmp	.LBB5_138
.LBB5_133:                              #   in Loop: Header=BB5_120 Depth=1
	movb	196(%r13), %cl
	.p2align	4, 0x90
.LBB5_134:                              # %_ZN10COutBuffer9WriteByteEh.exit._crit_edge.i.i
                                        #   Parent Loop BB5_120 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	$32, %rax
	addb	%cl, %al
	movq	216(%r13), %rcx
	movl	224(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 224(%r13)
	movb	%al, (%rcx,%rdx)
	movl	224(%r13), %eax
	cmpl	228(%r13), %eax
	jne	.LBB5_136
# BB#135:                               #   in Loop: Header=BB5_134 Depth=2
.Ltmp128:
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp129:
.LBB5_136:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB5_134 Depth=2
	decl	192(%r13)
	movq	200(%r13), %rax
	movb	$-1, %cl
	jne	.LBB5_134
# BB#137:                               #   in Loop: Header=BB5_120 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	movb	%cl, 196(%r13)
	movl	$1, %ecx
.LBB5_138:                              # %_ZN9NCompress11NRangeCoder8CEncoder8ShiftLowEv.exit.i
                                        #   in Loop: Header=BB5_120 Depth=1
	movl	%ecx, 192(%r13)
	andq	$16777215, %rax         # imm = 0xFFFFFF
	shlq	$8, %rax
	movq	%rax, 200(%r13)
.LBB5_139:                              # %_ZN9NCompress11NRangeCoder11CBitEncoderILi5EE6EncodeEPNS0_8CEncoderEj.exit
                                        #   in Loop: Header=BB5_120 Depth=1
	incq	%rbp
	cmpq	%r15, %rbp
	jb	.LBB5_120
.LBB5_140:                              # %._crit_edge
.Ltmp131:
	movq	%r13, %rdi
	callq	_ZN9NCompress5NBcj28CEncoder5FlushEv
	movl	%eax, 20(%rsp)          # 4-byte Spill
.Ltmp132:
.LBB5_142:                              # %.thread367
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_144
# BB#143:
	movq	(%rdi), %rax
.Ltmp136:
	callq	*16(%rax)
.Ltmp137:
.LBB5_144:                              # %_ZN9CMyComPtrI25ICompressGetSubStreamSizeED2Ev.exit358
	movq	48(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB5_146
# BB#145:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 48(%r13)
.LBB5_146:                              # %_ZN10COutBuffer13ReleaseStreamEv.exit.i.i326
	movq	104(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB5_148
# BB#147:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 104(%r13)
.LBB5_148:                              # %_ZN10COutBuffer13ReleaseStreamEv.exit1.i.i327
	movq	160(%r13), %rdi
	testq	%rdi, %rdi
	movl	20(%rsp), %ebx          # 4-byte Reload
	je	.LBB5_150
# BB#149:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 160(%r13)
.LBB5_150:                              # %_ZN10COutBuffer13ReleaseStreamEv.exit2.i.i328
	movq	240(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB5_152
# BB#151:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 240(%r13)
.LBB5_152:                              # %_ZN9NCompress5NBcj28CEncoder14CCoderReleaserD2Ev.exit329
	movl	%ebx, %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_153:
.Ltmp133:
	jmp	.LBB5_167
.LBB5_154:
.Ltmp138:
	movq	%rax, %rbp
	jmp	.LBB5_169
.LBB5_155:
.Ltmp106:
	jmp	.LBB5_167
.LBB5_156:
.Ltmp94:
	jmp	.LBB5_167
.LBB5_157:
.Ltmp127:
	jmp	.LBB5_167
.LBB5_158:
.Ltmp124:
	jmp	.LBB5_167
.LBB5_159:
.Ltmp91:
	movq	%rax, %rbp
	jmp	.LBB5_169
.LBB5_160:
.Ltmp121:
	jmp	.LBB5_167
.LBB5_161:
.Ltmp130:
	jmp	.LBB5_167
.LBB5_162:                              # %.loopexit.split-lp
.Ltmp109:
	jmp	.LBB5_167
.LBB5_163:                              # %.loopexit
.Ltmp112:
	jmp	.LBB5_167
.LBB5_164:
.Ltmp103:
	jmp	.LBB5_167
.LBB5_165:
.Ltmp100:
	jmp	.LBB5_167
.LBB5_166:
.Ltmp97:
.LBB5_167:
	movq	%rax, %rbp
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB5_169
# BB#168:
	movq	(%rdi), %rax
.Ltmp134:
	callq	*16(%rax)
.Ltmp135:
.LBB5_169:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_172
# BB#170:
	movq	(%rdi), %rax
.Ltmp139:
	callq	*16(%rax)
.Ltmp140:
# BB#171:                               # %.noexc319
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, 48(%rax)
.LBB5_172:                              # %_ZN10COutBuffer13ReleaseStreamEv.exit.i.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	104(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_175
# BB#173:
	movq	(%rdi), %rax
.Ltmp141:
	callq	*16(%rax)
.Ltmp142:
# BB#174:                               # %.noexc320
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, 104(%rax)
.LBB5_175:                              # %_ZN10COutBuffer13ReleaseStreamEv.exit1.i.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	160(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_178
# BB#176:
	movq	(%rdi), %rax
.Ltmp143:
	callq	*16(%rax)
.Ltmp144:
# BB#177:                               # %.noexc321
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, 160(%rax)
.LBB5_178:                              # %_ZN10COutBuffer13ReleaseStreamEv.exit2.i.i
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	240(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB5_181
# BB#179:
	movq	(%rdi), %rax
.Ltmp145:
	callq	*16(%rax)
.Ltmp146:
# BB#180:                               # %.noexc322
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	$0, 240(%rax)
.LBB5_181:                              # %_ZN9NCompress5NBcj28CEncoder14CCoderReleaserD2Ev.exit
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.LBB5_182:
.Ltmp147:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN9NCompress5NBcj28CEncoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo, .Lfunc_end5-_ZN9NCompress5NBcj28CEncoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp75-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp75
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp90-.Ltmp75         #   Call between .Ltmp75 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin2   #     jumps to .Ltmp91
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin2   #     jumps to .Ltmp94
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin2   #     jumps to .Ltmp97
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin2  #     jumps to .Ltmp100
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin2  # >> Call Site 6 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin2  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin2  # >> Call Site 7 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin2  #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin2  # >> Call Site 8 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin2  #     jumps to .Ltmp112
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin2  # >> Call Site 9 <<
	.long	.Ltmp120-.Ltmp113       #   Call between .Ltmp113 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin2  #     jumps to .Ltmp121
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin2  # >> Call Site 10 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin2  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin2  # >> Call Site 11 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin2  #     jumps to .Ltmp124
	.byte	0                       #   On action: cleanup
	.long	.Ltmp125-.Lfunc_begin2  # >> Call Site 12 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin2  #     jumps to .Ltmp127
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin2  # >> Call Site 13 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin2  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin2  # >> Call Site 14 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin2  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin2  # >> Call Site 15 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin2  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin2  # >> Call Site 16 <<
	.long	.Ltmp134-.Ltmp137       #   Call between .Ltmp137 and .Ltmp134
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin2  # >> Call Site 17 <<
	.long	.Ltmp146-.Ltmp134       #   Call between .Ltmp134 and .Ltmp146
	.long	.Ltmp147-.Lfunc_begin2  #     jumps to .Ltmp147
	.byte	1                       #   On action: 1
	.long	.Ltmp146-.Lfunc_begin2  # >> Call Site 18 <<
	.long	.Lfunc_end5-.Ltmp146    #   Call between .Ltmp146 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress5NBcj28CEncoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CEncoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo,@function
_ZN9NCompress5NBcj28CEncoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo: # @_ZN9NCompress5NBcj28CEncoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 16
.Lcfi32:
	.cfi_offset %rbx, -16
	movl	16(%rsp), %eax
	movq	24(%rsp), %rbx
.Ltmp148:
.Lcfi33:
	.cfi_escape 0x2e, 0x10
	pushq	%rbx
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	callq	_ZN9NCompress5NBcj28CEncoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	addq	$16, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebx
.Ltmp149:
.LBB6_4:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB6_1:
.Ltmp150:
	movq	%rdx, %rbx
.Lcfi37:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB6_3
# BB#2:
	movl	(%rax), %ebx
.Lcfi38:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	jmp	.LBB6_4
.LBB6_3:
.Lcfi39:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	movl	$1, %ebx
	jmp	.LBB6_4
.Lfunc_end6:
	.size	_ZN9NCompress5NBcj28CEncoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo, .Lfunc_end6-_ZN9NCompress5NBcj28CEncoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\250"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp148-.Lfunc_begin3  # >> Call Site 1 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin3  #     jumps to .Ltmp150
	.byte	3                       #   On action: 2
	.long	.Ltmp149-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Lfunc_end6-.Ltmp149    #   Call between .Ltmp149 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress5NBcj28CDecoder12SetInBufSizeEjj
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoder12SetInBufSizeEjj,@function
_ZN9NCompress5NBcj28CDecoder12SetInBufSizeEjj: # @_ZN9NCompress5NBcj28CDecoder12SetInBufSizeEjj
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	movl	%edx, 1312(%rdi,%rax,4)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	_ZN9NCompress5NBcj28CDecoder12SetInBufSizeEjj, .Lfunc_end7-_ZN9NCompress5NBcj28CDecoder12SetInBufSizeEjj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress5NBcj28CDecoder12SetInBufSizeEjj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NBcj28CDecoder12SetInBufSizeEjj,@function
_ZThn8_N9NCompress5NBcj28CDecoder12SetInBufSizeEjj: # @_ZThn8_N9NCompress5NBcj28CDecoder12SetInBufSizeEjj
	.cfi_startproc
# BB#0:
	movl	%esi, %eax
	movl	%edx, 1304(%rdi,%rax,4)
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	_ZThn8_N9NCompress5NBcj28CDecoder12SetInBufSizeEjj, .Lfunc_end8-_ZThn8_N9NCompress5NBcj28CDecoder12SetInBufSizeEjj
	.cfi_endproc

	.globl	_ZN9NCompress5NBcj28CDecoder13SetOutBufSizeEjj
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoder13SetOutBufSizeEjj,@function
_ZN9NCompress5NBcj28CDecoder13SetOutBufSizeEjj: # @_ZN9NCompress5NBcj28CDecoder13SetOutBufSizeEjj
	.cfi_startproc
# BB#0:
	movl	%edx, 1328(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	_ZN9NCompress5NBcj28CDecoder13SetOutBufSizeEjj, .Lfunc_end9-_ZN9NCompress5NBcj28CDecoder13SetOutBufSizeEjj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress5NBcj28CDecoder13SetOutBufSizeEjj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NBcj28CDecoder13SetOutBufSizeEjj,@function
_ZThn8_N9NCompress5NBcj28CDecoder13SetOutBufSizeEjj: # @_ZThn8_N9NCompress5NBcj28CDecoder13SetOutBufSizeEjj
	.cfi_startproc
# BB#0:
	movl	%edx, 1320(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	_ZThn8_N9NCompress5NBcj28CDecoder13SetOutBufSizeEjj, .Lfunc_end10-_ZThn8_N9NCompress5NBcj28CDecoder13SetOutBufSizeEjj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	1048576                 # 0x100000
	.long	1048576                 # 0x100000
	.long	1048576                 # 0x100000
	.long	1048576                 # 0x100000
	.text
	.globl	_ZN9NCompress5NBcj28CDecoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoderC2Ev,@function
_ZN9NCompress5NBcj28CDecoderC2Ev:       # @_ZN9NCompress5NBcj28CDecoderC2Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 48
.Lcfi45:
	.cfi_offset %rbx, -48
.Lcfi46:
	.cfi_offset %r12, -40
.Lcfi47:
	.cfi_offset %r13, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 16(%rbx)
	movl	$_ZTVN9NCompress5NBcj28CDecoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NBcj28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	leaq	24(%rbx), %r14
.Ltmp151:
	movq	%r14, %rdi
	callq	_ZN9CInBufferC1Ev
.Ltmp152:
# BB#1:
	leaq	72(%rbx), %r12
.Ltmp154:
	movq	%r12, %rdi
	callq	_ZN9CInBufferC1Ev
.Ltmp155:
# BB#2:
	leaq	120(%rbx), %r13
.Ltmp157:
	movq	%r13, %rdi
	callq	_ZN9CInBufferC1Ev
.Ltmp158:
# BB#3:
	leaq	168(%rbx), %rdi
.Ltmp160:
	callq	_ZN9CInBufferC1Ev
.Ltmp161:
# BB#4:
	movq	$0, 1256(%rbx)
	movl	$0, 1264(%rbx)
	movq	$0, 1280(%rbx)
	movq	$0, 1296(%rbx)
	movl	$65536, 1328(%rbx)      # imm = 0x10000
	movaps	.LCPI11_0(%rip), %xmm0  # xmm0 = [1048576,1048576,1048576,1048576]
	movups	%xmm0, 1312(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB11_5:
.Ltmp162:
	movq	%rax, %r15
.Ltmp163:
	movq	%r13, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp164:
# BB#6:
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_12
# BB#7:
	movq	(%rdi), %rax
.Ltmp169:
	callq	*16(%rax)
.Ltmp170:
	jmp	.LBB11_12
.LBB11_8:
.Ltmp165:
	movq	%rax, %r14
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_28
# BB#9:
	movq	(%rdi), %rax
.Ltmp166:
	callq	*16(%rax)
.Ltmp167:
	jmp	.LBB11_28
.LBB11_10:
.Ltmp168:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_11:
.Ltmp159:
	movq	%rax, %r15
.LBB11_12:                              # %_ZN9CInBufferD2Ev.exit14
.Ltmp171:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp172:
# BB#13:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_19
# BB#14:
	movq	(%rdi), %rax
.Ltmp177:
	callq	*16(%rax)
.Ltmp178:
	jmp	.LBB11_19
.LBB11_15:
.Ltmp173:
	movq	%rax, %r14
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_28
# BB#16:
	movq	(%rdi), %rax
.Ltmp174:
	callq	*16(%rax)
.Ltmp175:
	jmp	.LBB11_28
.LBB11_17:
.Ltmp176:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_18:
.Ltmp156:
	movq	%rax, %r15
.LBB11_19:                              # %_ZN9CInBufferD2Ev.exit19
.Ltmp179:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp180:
# BB#20:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_26
# BB#21:
	movq	(%rdi), %rax
.Ltmp185:
	callq	*16(%rax)
.Ltmp186:
	jmp	.LBB11_26
.LBB11_27:
.Ltmp187:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB11_22:
.Ltmp181:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_28
# BB#23:
	movq	(%rdi), %rax
.Ltmp182:
	callq	*16(%rax)
.Ltmp183:
.LBB11_28:                              # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB11_24:
.Ltmp184:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB11_25:
.Ltmp153:
	movq	%rax, %r15
.LBB11_26:                              # %_ZN9CInBufferD2Ev.exit
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end11:
	.size	_ZN9NCompress5NBcj28CDecoderC2Ev, .Lfunc_end11-_ZN9NCompress5NBcj28CDecoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Ltmp151-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin4  #     jumps to .Ltmp153
	.byte	0                       #   On action: cleanup
	.long	.Ltmp154-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin4  #     jumps to .Ltmp156
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin4  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin4  #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin4  #     jumps to .Ltmp165
	.byte	1                       #   On action: 1
	.long	.Ltmp169-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp187-.Lfunc_begin4  #     jumps to .Ltmp187
	.byte	1                       #   On action: 1
	.long	.Ltmp166-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin4  #     jumps to .Ltmp168
	.byte	1                       #   On action: 1
	.long	.Ltmp171-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin4  #     jumps to .Ltmp173
	.byte	1                       #   On action: 1
	.long	.Ltmp177-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp187-.Lfunc_begin4  #     jumps to .Ltmp187
	.byte	1                       #   On action: 1
	.long	.Ltmp174-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin4  #     jumps to .Ltmp176
	.byte	1                       #   On action: 1
	.long	.Ltmp179-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp180-.Ltmp179       #   Call between .Ltmp179 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin4  #     jumps to .Ltmp181
	.byte	1                       #   On action: 1
	.long	.Ltmp185-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp186-.Ltmp185       #   Call between .Ltmp185 and .Ltmp186
	.long	.Ltmp187-.Lfunc_begin4  #     jumps to .Ltmp187
	.byte	1                       #   On action: 1
	.long	.Ltmp182-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin4  #     jumps to .Ltmp184
	.byte	1                       #   On action: 1
	.long	.Ltmp183-.Lfunc_begin4  # >> Call Site 14 <<
	.long	.Lfunc_end11-.Ltmp183   #   Call between .Ltmp183 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	1024                    # 0x400
	.long	1024                    # 0x400
	.long	1024                    # 0x400
	.long	1024                    # 0x400
	.text
	.globl	_ZN9NCompress5NBcj28CDecoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo,@function
_ZN9NCompress5NBcj28CDecoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo: # @_ZN9NCompress5NBcj28CDecoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi53:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi54:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi56:
	.cfi_def_cfa_offset 112
.Lcfi57:
	.cfi_offset %rbx, -56
.Lcfi58:
	.cfi_offset %r12, -48
.Lcfi59:
	.cfi_offset %r13, -40
.Lcfi60:
	.cfi_offset %r14, -32
.Lcfi61:
	.cfi_offset %r15, -24
.Lcfi62:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	cmpl	$4, %ecx
	movl	$-2147024809, %r13d     # imm = 0x80070057
	jne	.LBB12_52
# BB#1:
	cmpl	$1, 112(%rsp)
	jne	.LBB12_52
# BB#2:
	leaq	24(%r14), %rdi
	movl	1312(%r14), %esi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	callq	_ZN9CInBuffer6CreateEj
	movl	$-2147024882, %r13d     # imm = 0x8007000E
	testb	%al, %al
	je	.LBB12_52
# BB#3:
	leaq	72(%r14), %rbx
	movl	1316(%r14), %esi
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer6CreateEj
	testb	%al, %al
	je	.LBB12_52
# BB#4:
	leaq	120(%r14), %rbp
	movl	1320(%r14), %esi
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer6CreateEj
	testb	%al, %al
	je	.LBB12_52
# BB#5:
	leaq	168(%r14), %rdi
	movl	1324(%r14), %esi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	callq	_ZN9CInBuffer6CreateEj
	testb	%al, %al
	je	.LBB12_52
# BB#6:
	leaq	1256(%r14), %rdi
	movl	1328(%r14), %esi
	movq	%rdi, (%rsp)            # 8-byte Spill
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	je	.LBB12_52
# BB#7:
	movq	(%r15), %rsi
.Ltmp188:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
.Ltmp189:
	movq	(%rsp), %r13            # 8-byte Reload
# BB#8:
	movq	8(%r15), %rsi
.Ltmp190:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
.Ltmp191:
# BB#9:
	movq	16(%r15), %rsi
.Ltmp192:
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
.Ltmp193:
# BB#10:
	movq	24(%r15), %rsi
.Ltmp194:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
.Ltmp195:
# BB#11:                                # %_ZN9NCompress11NRangeCoder8CDecoder9SetStreamEP19ISequentialInStream.exit
	movq	(%r12), %rsi
.Ltmp196:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
.Ltmp197:
# BB#12:
.Ltmp198:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZN9CInBuffer4InitEv
.Ltmp199:
# BB#13:
.Ltmp200:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer4InitEv
.Ltmp201:
# BB#14:
.Ltmp202:
	movq	%rbp, %rdi
	callq	_ZN9CInBuffer4InitEv
.Ltmp203:
# BB#15:
.Ltmp204:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN9NCompress11NRangeCoder8CDecoder4InitEv
.Ltmp205:
# BB#16:
.Ltmp206:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer4InitEv
.Ltmp207:
# BB#17:                                # %.preheader
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	120(%rsp), %rbx
	movaps	.LCPI12_0(%rip), %xmm0  # xmm0 = [1024,1024,1024,1024]
	movups	%xmm0, 224(%r14)
	movups	%xmm0, 240(%r14)
	movups	%xmm0, 256(%r14)
	movups	%xmm0, 272(%r14)
	movups	%xmm0, 288(%r14)
	movups	%xmm0, 304(%r14)
	movups	%xmm0, 320(%r14)
	movups	%xmm0, 336(%r14)
	movups	%xmm0, 352(%r14)
	movups	%xmm0, 368(%r14)
	movups	%xmm0, 384(%r14)
	movups	%xmm0, 400(%r14)
	movups	%xmm0, 416(%r14)
	movups	%xmm0, 432(%r14)
	movups	%xmm0, 448(%r14)
	movups	%xmm0, 464(%r14)
	movups	%xmm0, 480(%r14)
	movups	%xmm0, 496(%r14)
	movups	%xmm0, 512(%r14)
	movups	%xmm0, 528(%r14)
	movups	%xmm0, 544(%r14)
	movups	%xmm0, 560(%r14)
	movups	%xmm0, 576(%r14)
	movups	%xmm0, 592(%r14)
	movups	%xmm0, 608(%r14)
	movups	%xmm0, 624(%r14)
	movups	%xmm0, 640(%r14)
	movups	%xmm0, 656(%r14)
	movups	%xmm0, 672(%r14)
	movups	%xmm0, 688(%r14)
	movups	%xmm0, 704(%r14)
	movups	%xmm0, 720(%r14)
	movups	%xmm0, 736(%r14)
	movups	%xmm0, 752(%r14)
	movups	%xmm0, 768(%r14)
	movups	%xmm0, 784(%r14)
	movups	%xmm0, 800(%r14)
	movups	%xmm0, 816(%r14)
	movups	%xmm0, 832(%r14)
	movups	%xmm0, 848(%r14)
	movups	%xmm0, 864(%r14)
	movups	%xmm0, 880(%r14)
	movups	%xmm0, 896(%r14)
	movups	%xmm0, 912(%r14)
	movups	%xmm0, 928(%r14)
	movups	%xmm0, 944(%r14)
	movups	%xmm0, 960(%r14)
	movups	%xmm0, 976(%r14)
	movups	%xmm0, 992(%r14)
	movups	%xmm0, 1008(%r14)
	movups	%xmm0, 1024(%r14)
	movups	%xmm0, 1040(%r14)
	movups	%xmm0, 1056(%r14)
	movups	%xmm0, 1072(%r14)
	movups	%xmm0, 1088(%r14)
	movups	%xmm0, 1104(%r14)
	movups	%xmm0, 1120(%r14)
	movups	%xmm0, 1136(%r14)
	movups	%xmm0, 1152(%r14)
	movups	%xmm0, 1168(%r14)
	movups	%xmm0, 1184(%r14)
	movups	%xmm0, 1200(%r14)
	movups	%xmm0, 1216(%r14)
	movups	%xmm0, 1232(%r14)
	movabsq	$4398046512128, %rax    # imm = 0x40000000400
	movq	%rax, 1248(%r14)
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
	movq	24(%rsp), %r12          # 8-byte Reload
	testq	%rbx, %rbx
	jne	.LBB12_82
	jmp	.LBB12_18
.LBB12_38:                              # %.us-lcssa193
	testb	%bpl, %bpl
	movq	32(%rsp), %r13          # 8-byte Reload
	cmovneq	40(%rsp), %r13          # 8-byte Folded Reload
	movq	(%r13), %rcx
	movq	8(%r13), %rax
	cmpq	%rax, %rcx
	jb	.LBB12_56
# BB#39:
.Ltmp234:
	movq	%r13, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp235:
# BB#40:                                # %.noexc124
	testb	%al, %al
	je	.LBB12_41
# BB#55:                                # %._crit_edge.i122
	movq	(%r13), %rcx
	movq	8(%r13), %rax
.LBB12_56:
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r13)
	movzbl	(%rcx), %ebp
	cmpq	%rax, %rdx
	jb	.LBB12_60
# BB#57:
.Ltmp236:
	movq	%r13, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp237:
# BB#58:                                # %.noexc124.1
	testb	%al, %al
	je	.LBB12_41
# BB#59:                                # %._crit_edge.i122.1
	movq	(%r13), %rdx
	movq	8(%r13), %rax
.LBB12_60:
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r13)
	movzbl	(%rdx), %r12d
	cmpq	%rax, %rcx
	jb	.LBB12_64
# BB#61:
.Ltmp238:
	movq	%r13, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp239:
# BB#62:                                # %.noexc124.2
	testb	%al, %al
	je	.LBB12_41
# BB#63:                                # %._crit_edge.i122.2
	movq	(%r13), %rcx
	movq	8(%r13), %rax
.LBB12_64:
	leaq	1(%rcx), %rdx
	movq	%rdx, (%r13)
	movzbl	(%rcx), %ebx
	cmpq	%rax, %rdx
	jb	.LBB12_68
# BB#65:
.Ltmp240:
	movq	%r13, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp241:
# BB#66:                                # %.noexc124.3
	testb	%al, %al
	je	.LBB12_41
# BB#67:                                # %._crit_edge.i122.3
	movq	(%r13), %rdx
.LBB12_68:
	leaq	1(%rdx), %rax
	movq	%rax, (%r13)
	movzbl	(%rdx), %r13d
.Ltmp243:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp244:
# BB#69:
	shll	$8, %ebp
	orl	%ebp, %r12d
	shll	$8, %r12d
	orl	%r12d, %ebx
	shll	$8, %ebx
	leal	-4(%r13,%rbx), %esi
	subl	%eax, %esi
	movq	1256(%r14), %rax
	movl	1264(%r14), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 1264(%r14)
	movl	%esi, %ebp
	movb	%sil, (%rax,%rcx)
	movl	1264(%r14), %eax
	cmpl	1268(%r14), %eax
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	120(%rsp), %rbx
	movq	(%rsp), %r13            # 8-byte Reload
	jne	.LBB12_72
# BB#70:
.Ltmp246:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp247:
# BB#71:                                # %._ZN10COutBuffer9WriteByteEh.exit127_crit_edge
	movl	1264(%r14), %eax
.LBB12_72:                              # %_ZN10COutBuffer9WriteByteEh.exit127
	movq	1256(%r14), %rcx
	leal	1(%rax), %edx
	movl	%edx, 1264(%r14)
	movl	%eax, %eax
	movl	%ebp, %edx
	movb	%dh, (%rcx,%rax)  # NOREX
	movl	%edx, %ecx
	movl	1264(%r14), %eax
	cmpl	1268(%r14), %eax
	jne	.LBB12_75
# BB#73:
.Ltmp248:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp249:
# BB#74:                                # %._ZN10COutBuffer9WriteByteEh.exit129_crit_edge
	movl	1264(%r14), %eax
	movl	%ebp, %ecx
.LBB12_75:                              # %_ZN10COutBuffer9WriteByteEh.exit129
	shrl	$16, %ecx
	movq	1256(%r14), %rdx
	leal	1(%rax), %esi
	movl	%esi, 1264(%r14)
	movl	%eax, %eax
	movb	%cl, (%rdx,%rax)
	movl	1264(%r14), %eax
	cmpl	1268(%r14), %eax
	jne	.LBB12_78
# BB#76:
.Ltmp250:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp251:
# BB#77:                                # %._ZN10COutBuffer9WriteByteEh.exit131_crit_edge
	movl	1264(%r14), %eax
.LBB12_78:                              # %_ZN10COutBuffer9WriteByteEh.exit131
	movl	%ebp, %esi
	shrl	$24, %esi
	movq	1256(%r14), %rcx
	leal	1(%rax), %edx
	movl	%edx, 1264(%r14)
	movl	%eax, %eax
	movl	%esi, %ebp
	movb	%sil, (%rcx,%rax)
	movl	1264(%r14), %eax
	cmpl	1268(%r14), %eax
	jne	.LBB12_80
# BB#79:
.Ltmp252:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp253:
.LBB12_80:                              # %_ZN9CInBuffer8ReadByteERh.exit125.thread
	addl	$4, %r15d
	movl	%ebp, %ecx
	testq	%rbx, %rbx
	je	.LBB12_18
.LBB12_82:                              # %_ZN9NCompress5NBcj28CDecoder5FlushEv.exit.thread.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_87 Depth 2
	cmpl	$1048576, %r15d         # imm = 0x100000
	jb	.LBB12_86
# BB#83:                                #   in Loop: Header=BB12_82 Depth=1
.Ltmp209:
	movl	%ecx, %ebx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp210:
	movq	120(%rsp), %rdi
# BB#84:                                #   in Loop: Header=BB12_82 Depth=1
	movq	%rax, 48(%rsp)
	movq	(%rdi), %rax
.Ltmp212:
	xorl	%esi, %esi
	leaq	48(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %r13d
.Ltmp213:
# BB#85:                                #   in Loop: Header=BB12_82 Depth=1
	xorl	%r15d, %r15d
	testl	%r13d, %r13d
	movl	%ebx, %ecx
	jne	.LBB12_42
.LBB12_86:                              #   in Loop: Header=BB12_82 Depth=1
	movq	%r15, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
	movq	(%rsp), %r13            # 8-byte Reload
	.p2align	4, 0x90
.LBB12_87:                              #   Parent Loop BB12_82 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %r15d
	movq	24(%r14), %rax
	cmpq	32(%r14), %rax
	jb	.LBB12_91
# BB#88:                                #   in Loop: Header=BB12_87 Depth=2
.Ltmp215:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp216:
# BB#89:                                # %.noexc118.us
                                        #   in Loop: Header=BB12_87 Depth=2
	testb	%al, %al
	je	.LBB12_27
# BB#90:                                # %._crit_edge.i.us
                                        #   in Loop: Header=BB12_87 Depth=2
	movq	(%r12), %rax
.LBB12_91:                              #   in Loop: Header=BB12_87 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, 24(%r14)
	movzbl	(%rax), %ebx
	movq	1256(%r14), %rax
	movl	1264(%r14), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 1264(%r14)
	movb	%bl, (%rax,%rcx)
	movl	1264(%r14), %eax
	cmpl	1268(%r14), %eax
	jne	.LBB12_93
# BB#92:                                #   in Loop: Header=BB12_87 Depth=2
.Ltmp217:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp218:
.LBB12_93:                              # %_ZN10COutBuffer9WriteByteEh.exit.us
                                        #   in Loop: Header=BB12_87 Depth=2
	movl	%ebx, %eax
	andb	$-2, %al
	cmpb	$-24, %al
	je	.LBB12_94
# BB#95:                                # %_ZN9NCompress5NBcj23IsJEhh.exit.us
                                        #   in Loop: Header=BB12_87 Depth=2
	cmpb	$15, %r15b
	jne	.LBB12_99
# BB#96:                                # %_ZN9NCompress5NBcj23IsJEhh.exit.us
                                        #   in Loop: Header=BB12_87 Depth=2
	movl	%ebx, %eax
	andb	$-16, %al
	cmpb	$-128, %al
	je	.LBB12_97
.LBB12_99:                              #   in Loop: Header=BB12_87 Depth=2
	incl	%ebp
	cmpl	$262144, %ebp           # imm = 0x40000
	movb	%bl, %cl
	jb	.LBB12_87
# BB#100:                               # %_ZN9NCompress5NBcj23IsJEhh.exit.thread.us
                                        #   in Loop: Header=BB12_82 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	addl	%ebp, %r15d
	cmpl	$262144, %ebp           # imm = 0x40000
	movb	%bl, %cl
	je	.LBB12_82
	jmp	.LBB12_101
.LBB12_94:                              #   in Loop: Header=BB12_82 Depth=1
	movl	%r15d, %ecx
	jmp	.LBB12_98
.LBB12_97:                              #   in Loop: Header=BB12_82 Depth=1
	movb	$15, %cl
.LBB12_98:                              # %_ZN9NCompress5NBcj23IsJEhh.exit.thread.us.thread
                                        #   in Loop: Header=BB12_82 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	addl	%ebp, %r15d
.LBB12_101:                             #   in Loop: Header=BB12_82 Depth=1
	xorl	%eax, %eax
	cmpb	$-23, %bl
	setne	%al
	orq	$256, %rax              # imm = 0x100
	cmpb	$-24, %bl
	sete	%bpl
	movzbl	%cl, %ecx
	cmovneq	%rax, %rcx
	leaq	224(%r14,%rcx,4), %rdi
.Ltmp220:
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	_ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE
.Ltmp221:
# BB#102:                               #   in Loop: Header=BB12_82 Depth=1
	cmpl	$1, %eax
	movb	%bl, %cl
	jne	.LBB12_82
	jmp	.LBB12_38
.LBB12_18:                              # %_ZN9NCompress5NBcj28CDecoder5FlushEv.exit.thread
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_19 Depth 2
	movq	%r15, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
	movq	(%rsp), %r13            # 8-byte Reload
	.p2align	4, 0x90
.LBB12_19:                              #   Parent Loop BB12_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %r15d
	movq	24(%r14), %rax
	cmpq	32(%r14), %rax
	jb	.LBB12_23
# BB#20:                                #   in Loop: Header=BB12_19 Depth=2
.Ltmp223:
	movq	%r12, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
.Ltmp224:
# BB#21:                                # %.noexc118
                                        #   in Loop: Header=BB12_19 Depth=2
	testb	%al, %al
	je	.LBB12_27
# BB#22:                                # %._crit_edge.i
                                        #   in Loop: Header=BB12_19 Depth=2
	movq	(%r12), %rax
.LBB12_23:                              #   in Loop: Header=BB12_19 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, 24(%r14)
	movzbl	(%rax), %ebx
	movq	1256(%r14), %rax
	movl	1264(%r14), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 1264(%r14)
	movb	%bl, (%rax,%rcx)
	movl	1264(%r14), %eax
	cmpl	1268(%r14), %eax
	jne	.LBB12_25
# BB#24:                                #   in Loop: Header=BB12_19 Depth=2
.Ltmp228:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp229:
.LBB12_25:                              # %_ZN10COutBuffer9WriteByteEh.exit
                                        #   in Loop: Header=BB12_19 Depth=2
	movl	%ebx, %eax
	andb	$-2, %al
	cmpb	$-24, %al
	je	.LBB12_26
# BB#30:                                # %_ZN9NCompress5NBcj23IsJEhh.exit
                                        #   in Loop: Header=BB12_19 Depth=2
	cmpb	$15, %r15b
	jne	.LBB12_33
# BB#31:                                # %_ZN9NCompress5NBcj23IsJEhh.exit
                                        #   in Loop: Header=BB12_19 Depth=2
	movl	%ebx, %eax
	andb	$-16, %al
	cmpb	$-128, %al
	je	.LBB12_32
.LBB12_33:                              #   in Loop: Header=BB12_19 Depth=2
	incl	%ebp
	cmpl	$262144, %ebp           # imm = 0x40000
	movb	%bl, %cl
	jb	.LBB12_19
# BB#34:                                #   in Loop: Header=BB12_18 Depth=1
	movb	%bl, %cl
	jmp	.LBB12_35
.LBB12_26:                              #   in Loop: Header=BB12_18 Depth=1
	movl	%r15d, %ecx
	jmp	.LBB12_35
.LBB12_32:                              #   in Loop: Header=BB12_18 Depth=1
	movb	$15, %cl
.LBB12_35:                              # %_ZN9NCompress5NBcj23IsJEhh.exit.thread
                                        #   in Loop: Header=BB12_18 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	addl	%ebp, %r15d
	cmpl	$262144, %ebp           # imm = 0x40000
	je	.LBB12_18
# BB#36:                                #   in Loop: Header=BB12_18 Depth=1
	xorl	%eax, %eax
	cmpb	$-23, %bl
	setne	%al
	orq	$256, %rax              # imm = 0x100
	cmpb	$-24, %bl
	sete	%bpl
	movzbl	%cl, %ecx
	cmovneq	%rax, %rcx
	leaq	224(%r14,%rcx,4), %rdi
.Ltmp231:
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	_ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE
.Ltmp232:
# BB#37:                                #   in Loop: Header=BB12_18 Depth=1
	cmpl	$1, %eax
	movb	%bl, %cl
	jne	.LBB12_18
	jmp	.LBB12_38
.LBB12_27:                              # %_ZN9CInBuffer8ReadByteERh.exit
.Ltmp225:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %r13d
.Ltmp226:
.LBB12_42:                              # %_ZN9NCompress5NBcj28CDecoder5FlushEv.exit
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_44
# BB#43:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 48(%r14)
.LBB12_44:                              # %_ZN9CInBuffer13ReleaseStreamEv.exit.i.i134
	movq	96(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_46
# BB#45:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 96(%r14)
.LBB12_46:                              # %_ZN9CInBuffer13ReleaseStreamEv.exit1.i.i135
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_48
# BB#47:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 144(%r14)
.LBB12_48:                              # %_ZN9CInBuffer13ReleaseStreamEv.exit2.i.i136
	movq	192(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_50
# BB#49:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 192(%r14)
.LBB12_50:                              # %_ZN9NCompress11NRangeCoder8CDecoder13ReleaseStreamEv.exit.i.i137
	movq	1280(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_52
# BB#51:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 1280(%r14)
.LBB12_52:                              # %_ZN9NCompress5NBcj28CDecoder14CCoderReleaserD2Ev.exit138
	movl	%r13d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_41:
	movl	$1, %r13d
	jmp	.LBB12_42
.LBB12_107:
.Ltmp245:
	jmp	.LBB12_110
.LBB12_108:
.Ltmp254:
	jmp	.LBB12_110
.LBB12_54:
.Ltmp242:
	jmp	.LBB12_110
.LBB12_29:                              # %.loopexit.split-lp
.Ltmp227:
	jmp	.LBB12_110
.LBB12_53:                              # %.us-lcssa191
.Ltmp233:
	jmp	.LBB12_110
.LBB12_104:                             # %.us-lcssa188.us
.Ltmp214:
	jmp	.LBB12_110
.LBB12_103:                             # %.us-lcssa.us
.Ltmp211:
	jmp	.LBB12_110
.LBB12_106:                             # %.us-lcssa191.us
.Ltmp222:
	jmp	.LBB12_110
.LBB12_109:
.Ltmp208:
	jmp	.LBB12_110
.LBB12_28:                              # %.loopexit.us-lcssa
.Ltmp230:
	jmp	.LBB12_110
.LBB12_105:                             # %.loopexit.us-lcssa.us
.Ltmp219:
.LBB12_110:
	movq	%rax, %rbx
	movq	48(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_113
# BB#111:
	movq	(%rdi), %rax
.Ltmp255:
	callq	*16(%rax)
.Ltmp256:
# BB#112:                               # %.noexc
	movq	$0, 48(%r14)
.LBB12_113:                             # %_ZN9CInBuffer13ReleaseStreamEv.exit.i.i
	movq	96(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_116
# BB#114:
	movq	(%rdi), %rax
.Ltmp257:
	callq	*16(%rax)
.Ltmp258:
# BB#115:                               # %.noexc114
	movq	$0, 96(%r14)
.LBB12_116:                             # %_ZN9CInBuffer13ReleaseStreamEv.exit1.i.i
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_119
# BB#117:
	movq	(%rdi), %rax
.Ltmp259:
	callq	*16(%rax)
.Ltmp260:
# BB#118:                               # %.noexc115
	movq	$0, 144(%r14)
.LBB12_119:                             # %_ZN9CInBuffer13ReleaseStreamEv.exit2.i.i
	movq	192(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_122
# BB#120:
	movq	(%rdi), %rax
.Ltmp261:
	callq	*16(%rax)
.Ltmp262:
# BB#121:                               # %.noexc116
	movq	$0, 192(%r14)
.LBB12_122:                             # %_ZN9NCompress11NRangeCoder8CDecoder13ReleaseStreamEv.exit.i.i
	movq	1280(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_125
# BB#123:
	movq	(%rdi), %rax
.Ltmp263:
	callq	*16(%rax)
.Ltmp264:
# BB#124:                               # %.noexc117
	movq	$0, 1280(%r14)
.LBB12_125:                             # %_ZN9NCompress5NBcj28CDecoder14CCoderReleaserD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_126:
.Ltmp265:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN9NCompress5NBcj28CDecoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo, .Lfunc_end12-_ZN9NCompress5NBcj28CDecoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\314\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp188-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp188
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp207-.Ltmp188       #   Call between .Ltmp188 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin5  #     jumps to .Ltmp208
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp241-.Ltmp234       #   Call between .Ltmp234 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin5  #     jumps to .Ltmp242
	.byte	0                       #   On action: cleanup
	.long	.Ltmp243-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp244-.Ltmp243       #   Call between .Ltmp243 and .Ltmp244
	.long	.Ltmp245-.Lfunc_begin5  #     jumps to .Ltmp245
	.byte	0                       #   On action: cleanup
	.long	.Ltmp246-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp253-.Ltmp246       #   Call between .Ltmp246 and .Ltmp253
	.long	.Ltmp254-.Lfunc_begin5  #     jumps to .Ltmp254
	.byte	0                       #   On action: cleanup
	.long	.Ltmp209-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin5  #     jumps to .Ltmp211
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp213-.Ltmp212       #   Call between .Ltmp212 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin5  #     jumps to .Ltmp214
	.byte	0                       #   On action: cleanup
	.long	.Ltmp215-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp218-.Ltmp215       #   Call between .Ltmp215 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin5  #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin5  #     jumps to .Ltmp222
	.byte	0                       #   On action: cleanup
	.long	.Ltmp223-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp229-.Ltmp223       #   Call between .Ltmp223 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin5  #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp231-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin5  #     jumps to .Ltmp233
	.byte	0                       #   On action: cleanup
	.long	.Ltmp225-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp226-.Ltmp225       #   Call between .Ltmp225 and .Ltmp226
	.long	.Ltmp227-.Lfunc_begin5  #     jumps to .Ltmp227
	.byte	0                       #   On action: cleanup
	.long	.Ltmp226-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp255-.Ltmp226       #   Call between .Ltmp226 and .Ltmp255
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp255-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp264-.Ltmp255       #   Call between .Ltmp255 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin5  #     jumps to .Ltmp265
	.byte	1                       #   On action: 1
	.long	.Ltmp264-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Lfunc_end12-.Ltmp264   #   Call between .Ltmp264 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress11NRangeCoder8CDecoder4InitEv,"axG",@progbits,_ZN9NCompress11NRangeCoder8CDecoder4InitEv,comdat
	.weak	_ZN9NCompress11NRangeCoder8CDecoder4InitEv
	.p2align	4, 0x90
	.type	_ZN9NCompress11NRangeCoder8CDecoder4InitEv,@function
_ZN9NCompress11NRangeCoder8CDecoder4InitEv: # @_ZN9NCompress11NRangeCoder8CDecoder4InitEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi63:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 32
.Lcfi66:
	.cfi_offset %rbx, -32
.Lcfi67:
	.cfi_offset %r14, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	_ZN9CInBuffer4InitEv
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 48(%r14)
	movq	(%r14), %rax
	movq	8(%r14), %rcx
	cmpq	%rcx, %rax
	jae	.LBB13_1
# BB#2:
	leaq	1(%rax), %rsi
	movq	%rsi, (%r14)
	movb	(%rax), %al
	jmp	.LBB13_3
.LBB13_1:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	(%r14), %rsi
	movq	8(%r14), %rcx
.LBB13_3:                               # %_ZN9CInBuffer8ReadByteEv.exit
	movzbl	%al, %ebx
	movl	%ebx, 52(%r14)
	shll	$8, %ebx
	cmpq	%rcx, %rsi
	jae	.LBB13_4
# BB#5:
	leaq	1(%rsi), %rdx
	movq	%rdx, (%r14)
	movb	(%rsi), %al
	jmp	.LBB13_6
.LBB13_4:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	(%r14), %rdx
	movq	8(%r14), %rcx
.LBB13_6:                               # %_ZN9CInBuffer8ReadByteEv.exit.1
	movzbl	%al, %ebp
	orl	%ebx, %ebp
	movl	%ebp, 52(%r14)
	shll	$8, %ebp
	cmpq	%rcx, %rdx
	jae	.LBB13_7
# BB#8:
	leaq	1(%rdx), %rsi
	movq	%rsi, (%r14)
	movb	(%rdx), %al
	jmp	.LBB13_9
.LBB13_7:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	(%r14), %rsi
	movq	8(%r14), %rcx
.LBB13_9:                               # %_ZN9CInBuffer8ReadByteEv.exit.2
	movzbl	%al, %ebx
	orl	%ebp, %ebx
	movl	%ebx, 52(%r14)
	shll	$8, %ebx
	cmpq	%rcx, %rsi
	jae	.LBB13_10
# BB#11:
	leaq	1(%rsi), %rdx
	movq	%rdx, (%r14)
	movb	(%rsi), %al
	jmp	.LBB13_12
.LBB13_10:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movq	(%r14), %rdx
	movq	8(%r14), %rcx
.LBB13_12:                              # %_ZN9CInBuffer8ReadByteEv.exit.3
	movzbl	%al, %ebp
	orl	%ebx, %ebp
	movl	%ebp, 52(%r14)
	shll	$8, %ebp
	cmpq	%rcx, %rdx
	jae	.LBB13_13
# BB#14:
	leaq	1(%rdx), %rax
	movq	%rax, (%r14)
	movb	(%rdx), %al
	jmp	.LBB13_15
.LBB13_13:
	movq	%r14, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
.LBB13_15:                              # %_ZN9CInBuffer8ReadByteEv.exit.4
	movzbl	%al, %eax
	orl	%ebp, %eax
	movl	%eax, 52(%r14)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZN9NCompress11NRangeCoder8CDecoder4InitEv, .Lfunc_end13-_ZN9NCompress11NRangeCoder8CDecoder4InitEv
	.cfi_endproc

	.section	.text._ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE,"axG",@progbits,_ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE,comdat
	.weak	_ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE
	.p2align	4, 0x90
	.type	_ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE,@function
_ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE: # @_ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 48
.Lcfi74:
	.cfi_offset %rbx, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	48(%rbx), %edx
	movl	52(%rbx), %r15d
	movl	%edx, %ecx
	shrl	$11, %ecx
	movl	(%rdi), %eax
	imull	%eax, %ecx
	movl	%r15d, %ebp
	subl	%ecx, %ebp
	jae	.LBB14_6
# BB#1:
	movl	%ecx, 48(%rbx)
	movl	$2048, %edx             # imm = 0x800
	subl	%eax, %edx
	shrl	$5, %edx
	addl	%eax, %edx
	movl	%edx, (%rdi)
	xorl	%r14d, %r14d
	cmpl	$16777215, %ecx         # imm = 0xFFFFFF
	ja	.LBB14_11
# BB#2:
	shll	$8, %r15d
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB14_3
# BB#4:
	leaq	1(%rax), %rdx
	movq	%rdx, (%rbx)
	movb	(%rax), %al
	jmp	.LBB14_5
.LBB14_6:
	subl	%ecx, %edx
	movl	%edx, 48(%rbx)
	movl	%ebp, 52(%rbx)
	movl	%eax, %ecx
	shrl	$5, %ecx
	subl	%ecx, %eax
	movl	%eax, (%rdi)
	movl	$1, %r14d
	cmpl	$16777215, %edx         # imm = 0xFFFFFF
	ja	.LBB14_11
# BB#7:
	shll	$8, %ebp
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	jae	.LBB14_8
# BB#9:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movb	(%rax), %al
	jmp	.LBB14_10
.LBB14_3:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	48(%rbx), %ecx
.LBB14_5:                               # %_ZN9CInBuffer8ReadByteEv.exit
	movzbl	%al, %eax
	orl	%r15d, %eax
	movl	%eax, 52(%rbx)
	shll	$8, %ecx
	movl	%ecx, 48(%rbx)
	jmp	.LBB14_11
.LBB14_8:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer10ReadBlock2Ev
	movl	48(%rbx), %edx
.LBB14_10:                              # %_ZN9CInBuffer8ReadByteEv.exit20
	movzbl	%al, %eax
	orl	%ebp, %eax
	movl	%eax, 52(%rbx)
	shll	$8, %edx
	movl	%edx, 48(%rbx)
.LBB14_11:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE, .Lfunc_end14-_ZN9NCompress11NRangeCoder11CBitDecoderILi5EE6DecodeEPNS0_8CDecoderE
	.cfi_endproc

	.text
	.globl	_ZN9NCompress5NBcj28CDecoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo,@function
_ZN9NCompress5NBcj28CDecoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo: # @_ZN9NCompress5NBcj28CDecoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 16
.Lcfi79:
	.cfi_offset %rbx, -16
	movl	16(%rsp), %eax
	movq	24(%rsp), %rdx
.Ltmp266:
.Lcfi80:
	.cfi_escape 0x2e, 0x10
	pushq	%rdx
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	callq	_ZN9NCompress5NBcj28CDecoder8CodeRealEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	addq	$16, %rsp
.Lcfi83:
	.cfi_adjust_cfa_offset -16
	movl	%eax, %ebx
.Ltmp267:
.LBB15_6:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB15_1:
.Ltmp268:
	movq	%rdx, %rbx
	cmpl	$3, %ebx
	jne	.LBB15_3
# BB#2:
.Lcfi84:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movl	(%rax), %ebx
.Lcfi85:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	jmp	.LBB15_6
.LBB15_3:
.Lcfi86:
	.cfi_escape 0x2e, 0x00
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB15_5
# BB#4:
	movl	(%rax), %ebx
.Lcfi87:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	jmp	.LBB15_6
.LBB15_5:
.Lcfi88:
	.cfi_escape 0x2e, 0x00
	callq	__cxa_end_catch
	movl	$1, %ebx
	jmp	.LBB15_6
.Lfunc_end15:
	.size	_ZN9NCompress5NBcj28CDecoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo, .Lfunc_end15-_ZN9NCompress5NBcj28CDecoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\256\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp266-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin6  #     jumps to .Ltmp268
	.byte	5                       #   On action: 3
	.long	.Ltmp267-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Lfunc_end15-.Ltmp267   #   Call between .Ltmp267 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 3
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress5NBcj28CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress5NBcj28CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress5NBcj28CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress5NBcj28CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress5NBcj28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB16_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB16_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB16_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB16_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB16_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB16_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB16_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB16_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB16_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB16_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB16_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB16_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB16_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB16_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB16_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB16_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB16_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end16:
	.size	_ZN9NCompress5NBcj28CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end16-_ZN9NCompress5NBcj28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress5NBcj28CEncoder6AddRefEv,"axG",@progbits,_ZN9NCompress5NBcj28CEncoder6AddRefEv,comdat
	.weak	_ZN9NCompress5NBcj28CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CEncoder6AddRefEv,@function
_ZN9NCompress5NBcj28CEncoder6AddRefEv:  # @_ZN9NCompress5NBcj28CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end17:
	.size	_ZN9NCompress5NBcj28CEncoder6AddRefEv, .Lfunc_end17-_ZN9NCompress5NBcj28CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress5NBcj28CEncoder7ReleaseEv,"axG",@progbits,_ZN9NCompress5NBcj28CEncoder7ReleaseEv,comdat
	.weak	_ZN9NCompress5NBcj28CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CEncoder7ReleaseEv,@function
_ZN9NCompress5NBcj28CEncoder7ReleaseEv: # @_ZN9NCompress5NBcj28CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB18_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB18_2:
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZN9NCompress5NBcj28CEncoder7ReleaseEv, .Lfunc_end18-_ZN9NCompress5NBcj28CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 32
.Lcfi94:
	.cfi_offset %rbx, -32
.Lcfi95:
	.cfi_offset %r14, -24
.Lcfi96:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB19_3
# BB#1:
	movl	$IID_ICompressSetBufSize, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB19_2
.LBB19_3:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB19_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB19_2:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB19_4
.Lfunc_end19:
	.size	_ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end19-_ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress5NBcj28CDecoder6AddRefEv,"axG",@progbits,_ZN9NCompress5NBcj28CDecoder6AddRefEv,comdat
	.weak	_ZN9NCompress5NBcj28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoder6AddRefEv,@function
_ZN9NCompress5NBcj28CDecoder6AddRefEv:  # @_ZN9NCompress5NBcj28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end20:
	.size	_ZN9NCompress5NBcj28CDecoder6AddRefEv, .Lfunc_end20-_ZN9NCompress5NBcj28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress5NBcj28CDecoder7ReleaseEv,"axG",@progbits,_ZN9NCompress5NBcj28CDecoder7ReleaseEv,comdat
	.weak	_ZN9NCompress5NBcj28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoder7ReleaseEv,@function
_ZN9NCompress5NBcj28CDecoder7ReleaseEv: # @_ZN9NCompress5NBcj28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB21_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB21_2:
	popq	%rcx
	retq
.Lfunc_end21:
	.size	_ZN9NCompress5NBcj28CDecoder7ReleaseEv, .Lfunc_end21-_ZN9NCompress5NBcj28CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZN9NCompress5NBcj28CDecoderD2Ev,"axG",@progbits,_ZN9NCompress5NBcj28CDecoderD2Ev,comdat
	.weak	_ZN9NCompress5NBcj28CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoderD2Ev,@function
_ZN9NCompress5NBcj28CDecoderD2Ev:       # @_ZN9NCompress5NBcj28CDecoderD2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 32
.Lcfi101:
	.cfi_offset %rbx, -24
.Lcfi102:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress5NBcj28CDecoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress5NBcj28CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	leaq	1256(%rbx), %rdi
.Ltmp269:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp270:
# BB#1:
	movq	1280(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp275:
	callq	*16(%rax)
.Ltmp276:
.LBB22_3:                               # %_ZN10COutBufferD2Ev.exit
	leaq	168(%rbx), %rdi
.Ltmp286:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp287:
# BB#4:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp292:
	callq	*16(%rax)
.Ltmp293:
.LBB22_6:                               # %_ZN9NCompress11NRangeCoder8CDecoderD2Ev.exit
	leaq	120(%rbx), %rdi
.Ltmp303:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp304:
# BB#7:
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp309:
	callq	*16(%rax)
.Ltmp310:
.LBB22_9:                               # %_ZN9CInBufferD2Ev.exit
	leaq	72(%rbx), %rdi
.Ltmp320:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp321:
# BB#10:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_12
# BB#11:
	movq	(%rdi), %rax
.Ltmp326:
	callq	*16(%rax)
.Ltmp327:
.LBB22_12:                              # %_ZN9CInBufferD2Ev.exit17
	leaq	24(%rbx), %rdi
.Ltmp338:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp339:
# BB#13:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_15
# BB#14:
	movq	(%rdi), %rax
.Ltmp344:
	callq	*16(%rax)
.Ltmp345:
.LBB22_15:                              # %_ZN9CInBufferD2Ev.exit22
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB22_57:
.Ltmp346:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_53:
.Ltmp328:
	movq	%rax, %r14
	jmp	.LBB22_41
.LBB22_49:
.Ltmp311:
	movq	%rax, %r14
	jmp	.LBB22_38
.LBB22_48:
.Ltmp294:
	movq	%rax, %r14
	jmp	.LBB22_35
.LBB22_31:
.Ltmp277:
	movq	%rax, %r14
	jmp	.LBB22_32
.LBB22_28:
.Ltmp340:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_44
# BB#29:
	movq	(%rdi), %rax
.Ltmp341:
	callq	*16(%rax)
.Ltmp342:
	jmp	.LBB22_44
.LBB22_30:
.Ltmp343:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_25:
.Ltmp322:
	movq	%rax, %r14
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_41
# BB#26:
	movq	(%rdi), %rax
.Ltmp323:
	callq	*16(%rax)
.Ltmp324:
	jmp	.LBB22_41
.LBB22_27:
.Ltmp325:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_22:
.Ltmp305:
	movq	%rax, %r14
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_38
# BB#23:
	movq	(%rdi), %rax
.Ltmp306:
	callq	*16(%rax)
.Ltmp307:
	jmp	.LBB22_38
.LBB22_24:
.Ltmp308:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_19:
.Ltmp288:
	movq	%rax, %r14
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_35
# BB#20:
	movq	(%rdi), %rax
.Ltmp289:
	callq	*16(%rax)
.Ltmp290:
	jmp	.LBB22_35
.LBB22_21:
.Ltmp291:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_16:
.Ltmp271:
	movq	%rax, %r14
	movq	1280(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_32
# BB#17:
	movq	(%rdi), %rax
.Ltmp272:
	callq	*16(%rax)
.Ltmp273:
.LBB22_32:                              # %.body
	leaq	168(%rbx), %rdi
.Ltmp278:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp279:
# BB#33:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_35
# BB#34:
	movq	(%rdi), %rax
.Ltmp284:
	callq	*16(%rax)
.Ltmp285:
.LBB22_35:                              # %_ZN9NCompress11NRangeCoder8CDecoderD2Ev.exit27
	leaq	120(%rbx), %rdi
.Ltmp295:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp296:
# BB#36:
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_38
# BB#37:
	movq	(%rdi), %rax
.Ltmp301:
	callq	*16(%rax)
.Ltmp302:
.LBB22_38:                              # %_ZN9CInBufferD2Ev.exit32
	leaq	72(%rbx), %rdi
.Ltmp312:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp313:
# BB#39:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_41
# BB#40:
	movq	(%rdi), %rax
.Ltmp318:
	callq	*16(%rax)
.Ltmp319:
.LBB22_41:                              # %_ZN9CInBufferD2Ev.exit37
	leaq	24(%rbx), %rdi
.Ltmp329:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp330:
# BB#42:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_44
# BB#43:
	movq	(%rdi), %rax
.Ltmp335:
	callq	*16(%rax)
.Ltmp336:
.LBB22_44:                              # %_ZN9CInBufferD2Ev.exit42
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB22_18:
.Ltmp274:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_45:
.Ltmp280:
	movq	%rax, %r14
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_62
# BB#46:
	movq	(%rdi), %rax
.Ltmp281:
	callq	*16(%rax)
.Ltmp282:
	jmp	.LBB22_62
.LBB22_47:
.Ltmp283:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_50:
.Ltmp297:
	movq	%rax, %r14
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_62
# BB#51:
	movq	(%rdi), %rax
.Ltmp298:
	callq	*16(%rax)
.Ltmp299:
	jmp	.LBB22_62
.LBB22_52:
.Ltmp300:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_54:
.Ltmp314:
	movq	%rax, %r14
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_62
# BB#55:
	movq	(%rdi), %rax
.Ltmp315:
	callq	*16(%rax)
.Ltmp316:
	jmp	.LBB22_62
.LBB22_56:
.Ltmp317:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_58:
.Ltmp331:
	movq	%rax, %r14
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB22_62
# BB#59:
	movq	(%rdi), %rax
.Ltmp332:
	callq	*16(%rax)
.Ltmp333:
	jmp	.LBB22_62
.LBB22_60:
.Ltmp334:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB22_61:
.Ltmp337:
	movq	%rax, %r14
.LBB22_62:                              # %.body25
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZN9NCompress5NBcj28CDecoderD2Ev, .Lfunc_end22-_ZN9NCompress5NBcj28CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\202\203\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\371\002"              # Call site table length
	.long	.Ltmp269-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp270-.Ltmp269       #   Call between .Ltmp269 and .Ltmp270
	.long	.Ltmp271-.Lfunc_begin7  #     jumps to .Ltmp271
	.byte	0                       #   On action: cleanup
	.long	.Ltmp275-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp276-.Ltmp275       #   Call between .Ltmp275 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin7  #     jumps to .Ltmp277
	.byte	0                       #   On action: cleanup
	.long	.Ltmp286-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp287-.Ltmp286       #   Call between .Ltmp286 and .Ltmp287
	.long	.Ltmp288-.Lfunc_begin7  #     jumps to .Ltmp288
	.byte	0                       #   On action: cleanup
	.long	.Ltmp292-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp293-.Ltmp292       #   Call between .Ltmp292 and .Ltmp293
	.long	.Ltmp294-.Lfunc_begin7  #     jumps to .Ltmp294
	.byte	0                       #   On action: cleanup
	.long	.Ltmp303-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp304-.Ltmp303       #   Call between .Ltmp303 and .Ltmp304
	.long	.Ltmp305-.Lfunc_begin7  #     jumps to .Ltmp305
	.byte	0                       #   On action: cleanup
	.long	.Ltmp309-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp310-.Ltmp309       #   Call between .Ltmp309 and .Ltmp310
	.long	.Ltmp311-.Lfunc_begin7  #     jumps to .Ltmp311
	.byte	0                       #   On action: cleanup
	.long	.Ltmp320-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp321-.Ltmp320       #   Call between .Ltmp320 and .Ltmp321
	.long	.Ltmp322-.Lfunc_begin7  #     jumps to .Ltmp322
	.byte	0                       #   On action: cleanup
	.long	.Ltmp326-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp328-.Lfunc_begin7  #     jumps to .Ltmp328
	.byte	0                       #   On action: cleanup
	.long	.Ltmp338-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin7  #     jumps to .Ltmp340
	.byte	0                       #   On action: cleanup
	.long	.Ltmp344-.Lfunc_begin7  # >> Call Site 10 <<
	.long	.Ltmp345-.Ltmp344       #   Call between .Ltmp344 and .Ltmp345
	.long	.Ltmp346-.Lfunc_begin7  #     jumps to .Ltmp346
	.byte	0                       #   On action: cleanup
	.long	.Ltmp345-.Lfunc_begin7  # >> Call Site 11 <<
	.long	.Ltmp341-.Ltmp345       #   Call between .Ltmp345 and .Ltmp341
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp341-.Lfunc_begin7  # >> Call Site 12 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp343-.Lfunc_begin7  #     jumps to .Ltmp343
	.byte	1                       #   On action: 1
	.long	.Ltmp323-.Lfunc_begin7  # >> Call Site 13 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin7  #     jumps to .Ltmp325
	.byte	1                       #   On action: 1
	.long	.Ltmp306-.Lfunc_begin7  # >> Call Site 14 <<
	.long	.Ltmp307-.Ltmp306       #   Call between .Ltmp306 and .Ltmp307
	.long	.Ltmp308-.Lfunc_begin7  #     jumps to .Ltmp308
	.byte	1                       #   On action: 1
	.long	.Ltmp289-.Lfunc_begin7  # >> Call Site 15 <<
	.long	.Ltmp290-.Ltmp289       #   Call between .Ltmp289 and .Ltmp290
	.long	.Ltmp291-.Lfunc_begin7  #     jumps to .Ltmp291
	.byte	1                       #   On action: 1
	.long	.Ltmp272-.Lfunc_begin7  # >> Call Site 16 <<
	.long	.Ltmp273-.Ltmp272       #   Call between .Ltmp272 and .Ltmp273
	.long	.Ltmp274-.Lfunc_begin7  #     jumps to .Ltmp274
	.byte	1                       #   On action: 1
	.long	.Ltmp278-.Lfunc_begin7  # >> Call Site 17 <<
	.long	.Ltmp279-.Ltmp278       #   Call between .Ltmp278 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin7  #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.long	.Ltmp284-.Lfunc_begin7  # >> Call Site 18 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp337-.Lfunc_begin7  #     jumps to .Ltmp337
	.byte	1                       #   On action: 1
	.long	.Ltmp295-.Lfunc_begin7  # >> Call Site 19 <<
	.long	.Ltmp296-.Ltmp295       #   Call between .Ltmp295 and .Ltmp296
	.long	.Ltmp297-.Lfunc_begin7  #     jumps to .Ltmp297
	.byte	1                       #   On action: 1
	.long	.Ltmp301-.Lfunc_begin7  # >> Call Site 20 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp337-.Lfunc_begin7  #     jumps to .Ltmp337
	.byte	1                       #   On action: 1
	.long	.Ltmp312-.Lfunc_begin7  # >> Call Site 21 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin7  #     jumps to .Ltmp314
	.byte	1                       #   On action: 1
	.long	.Ltmp318-.Lfunc_begin7  # >> Call Site 22 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp337-.Lfunc_begin7  #     jumps to .Ltmp337
	.byte	1                       #   On action: 1
	.long	.Ltmp329-.Lfunc_begin7  # >> Call Site 23 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp331-.Lfunc_begin7  #     jumps to .Ltmp331
	.byte	1                       #   On action: 1
	.long	.Ltmp335-.Lfunc_begin7  # >> Call Site 24 <<
	.long	.Ltmp336-.Ltmp335       #   Call between .Ltmp335 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin7  #     jumps to .Ltmp337
	.byte	1                       #   On action: 1
	.long	.Ltmp336-.Lfunc_begin7  # >> Call Site 25 <<
	.long	.Ltmp281-.Ltmp336       #   Call between .Ltmp336 and .Ltmp281
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin7  # >> Call Site 26 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin7  #     jumps to .Ltmp283
	.byte	1                       #   On action: 1
	.long	.Ltmp298-.Lfunc_begin7  # >> Call Site 27 <<
	.long	.Ltmp299-.Ltmp298       #   Call between .Ltmp298 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin7  #     jumps to .Ltmp300
	.byte	1                       #   On action: 1
	.long	.Ltmp315-.Lfunc_begin7  # >> Call Site 28 <<
	.long	.Ltmp316-.Ltmp315       #   Call between .Ltmp315 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin7  #     jumps to .Ltmp317
	.byte	1                       #   On action: 1
	.long	.Ltmp332-.Lfunc_begin7  # >> Call Site 29 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp334-.Lfunc_begin7  #     jumps to .Ltmp334
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress5NBcj28CDecoderD0Ev,"axG",@progbits,_ZN9NCompress5NBcj28CDecoderD0Ev,comdat
	.weak	_ZN9NCompress5NBcj28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress5NBcj28CDecoderD0Ev,@function
_ZN9NCompress5NBcj28CDecoderD0Ev:       # @_ZN9NCompress5NBcj28CDecoderD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 32
.Lcfi106:
	.cfi_offset %rbx, -24
.Lcfi107:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp347:
	callq	_ZN9NCompress5NBcj28CDecoderD2Ev
.Ltmp348:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB23_2:
.Ltmp349:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZN9NCompress5NBcj28CDecoderD0Ev, .Lfunc_end23-_ZN9NCompress5NBcj28CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp347-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp348-.Ltmp347       #   Call between .Ltmp347 and .Ltmp348
	.long	.Ltmp349-.Lfunc_begin8  #     jumps to .Ltmp349
	.byte	0                       #   On action: cleanup
	.long	.Ltmp348-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end23-.Ltmp348   #   Call between .Ltmp348 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB24_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB24_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB24_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB24_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB24_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB24_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB24_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB24_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB24_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB24_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB24_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB24_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB24_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB24_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB24_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB24_32
.LBB24_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetBufSize(%rip), %cl
	jne	.LBB24_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+1(%rip), %cl
	jne	.LBB24_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+2(%rip), %cl
	jne	.LBB24_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+3(%rip), %cl
	jne	.LBB24_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+4(%rip), %cl
	jne	.LBB24_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+5(%rip), %cl
	jne	.LBB24_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+6(%rip), %cl
	jne	.LBB24_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+7(%rip), %cl
	jne	.LBB24_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+8(%rip), %cl
	jne	.LBB24_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+9(%rip), %cl
	jne	.LBB24_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+10(%rip), %cl
	jne	.LBB24_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+11(%rip), %cl
	jne	.LBB24_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+12(%rip), %cl
	jne	.LBB24_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+13(%rip), %cl
	jne	.LBB24_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+14(%rip), %cl
	jne	.LBB24_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetBufSize+15(%rip), %cl
	jne	.LBB24_33
.LBB24_32:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB24_33:                              # %_ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZThn8_N9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end24-_ZThn8_N9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NBcj28CDecoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress5NBcj28CDecoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress5NBcj28CDecoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NBcj28CDecoder6AddRefEv,@function
_ZThn8_N9NCompress5NBcj28CDecoder6AddRefEv: # @_ZThn8_N9NCompress5NBcj28CDecoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end25:
	.size	_ZThn8_N9NCompress5NBcj28CDecoder6AddRefEv, .Lfunc_end25-_ZThn8_N9NCompress5NBcj28CDecoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NBcj28CDecoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress5NBcj28CDecoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress5NBcj28CDecoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NBcj28CDecoder7ReleaseEv,@function
_ZThn8_N9NCompress5NBcj28CDecoder7ReleaseEv: # @_ZThn8_N9NCompress5NBcj28CDecoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB26_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB26_2:                               # %_ZN9NCompress5NBcj28CDecoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end26:
	.size	_ZThn8_N9NCompress5NBcj28CDecoder7ReleaseEv, .Lfunc_end26-_ZThn8_N9NCompress5NBcj28CDecoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NBcj28CDecoderD1Ev,"axG",@progbits,_ZThn8_N9NCompress5NBcj28CDecoderD1Ev,comdat
	.weak	_ZThn8_N9NCompress5NBcj28CDecoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NBcj28CDecoderD1Ev,@function
_ZThn8_N9NCompress5NBcj28CDecoderD1Ev:  # @_ZThn8_N9NCompress5NBcj28CDecoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress5NBcj28CDecoderD2Ev # TAILCALL
.Lfunc_end27:
	.size	_ZThn8_N9NCompress5NBcj28CDecoderD1Ev, .Lfunc_end27-_ZThn8_N9NCompress5NBcj28CDecoderD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress5NBcj28CDecoderD0Ev,"axG",@progbits,_ZThn8_N9NCompress5NBcj28CDecoderD0Ev,comdat
	.weak	_ZThn8_N9NCompress5NBcj28CDecoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress5NBcj28CDecoderD0Ev,@function
_ZThn8_N9NCompress5NBcj28CDecoderD0Ev:  # @_ZThn8_N9NCompress5NBcj28CDecoderD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi112:
	.cfi_def_cfa_offset 32
.Lcfi113:
	.cfi_offset %rbx, -24
.Lcfi114:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp350:
	movq	%rbx, %rdi
	callq	_ZN9NCompress5NBcj28CDecoderD2Ev
.Ltmp351:
# BB#1:                                 # %_ZN9NCompress5NBcj28CDecoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB28_2:
.Ltmp352:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZThn8_N9NCompress5NBcj28CDecoderD0Ev, .Lfunc_end28-_ZThn8_N9NCompress5NBcj28CDecoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp350-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin9  #     jumps to .Ltmp352
	.byte	0                       #   On action: cleanup
	.long	.Ltmp351-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp351   #   Call between .Ltmp351 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB29_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB29_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB29_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB29_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB29_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB29_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB29_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB29_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB29_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB29_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB29_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB29_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB29_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB29_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB29_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB29_16:
	xorl	%eax, %eax
	retq
.Lfunc_end29:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end29-_ZeqRK4GUIDS1_
	.cfi_endproc

	.type	_ZTVN9NCompress5NBcj28CEncoderE,@object # @_ZTVN9NCompress5NBcj28CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress5NBcj28CEncoderE
	.p2align	3
_ZTVN9NCompress5NBcj28CEncoderE:
	.quad	0
	.quad	_ZTIN9NCompress5NBcj28CEncoderE
	.quad	_ZN9NCompress5NBcj28CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress5NBcj28CEncoder6AddRefEv
	.quad	_ZN9NCompress5NBcj28CEncoder7ReleaseEv
	.quad	_ZN9NCompress5NBcj28CEncoderD2Ev
	.quad	_ZN9NCompress5NBcj28CEncoderD0Ev
	.quad	_ZN9NCompress5NBcj28CEncoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.size	_ZTVN9NCompress5NBcj28CEncoderE, 64

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTVN9NCompress5NBcj28CDecoderE,@object # @_ZTVN9NCompress5NBcj28CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress5NBcj28CDecoderE
	.p2align	3
_ZTVN9NCompress5NBcj28CDecoderE:
	.quad	0
	.quad	_ZTIN9NCompress5NBcj28CDecoderE
	.quad	_ZN9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress5NBcj28CDecoder6AddRefEv
	.quad	_ZN9NCompress5NBcj28CDecoder7ReleaseEv
	.quad	_ZN9NCompress5NBcj28CDecoderD2Ev
	.quad	_ZN9NCompress5NBcj28CDecoderD0Ev
	.quad	_ZN9NCompress5NBcj28CDecoder4CodeEPP19ISequentialInStreamPPKyjPP20ISequentialOutStreamS7_jP21ICompressProgressInfo
	.quad	_ZN9NCompress5NBcj28CDecoder12SetInBufSizeEjj
	.quad	_ZN9NCompress5NBcj28CDecoder13SetOutBufSizeEjj
	.quad	-8
	.quad	_ZTIN9NCompress5NBcj28CDecoderE
	.quad	_ZThn8_N9NCompress5NBcj28CDecoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress5NBcj28CDecoder6AddRefEv
	.quad	_ZThn8_N9NCompress5NBcj28CDecoder7ReleaseEv
	.quad	_ZThn8_N9NCompress5NBcj28CDecoderD1Ev
	.quad	_ZThn8_N9NCompress5NBcj28CDecoderD0Ev
	.quad	_ZThn8_N9NCompress5NBcj28CDecoder12SetInBufSizeEjj
	.quad	_ZThn8_N9NCompress5NBcj28CDecoder13SetOutBufSizeEjj
	.size	_ZTVN9NCompress5NBcj28CDecoderE, 152

	.type	_ZTS18CInBufferException,@object # @_ZTS18CInBufferException
	.section	.rodata._ZTS18CInBufferException,"aG",@progbits,_ZTS18CInBufferException,comdat
	.weak	_ZTS18CInBufferException
	.p2align	4
_ZTS18CInBufferException:
	.asciz	"18CInBufferException"
	.size	_ZTS18CInBufferException, 21

	.type	_ZTI18CInBufferException,@object # @_ZTI18CInBufferException
	.section	.rodata._ZTI18CInBufferException,"aG",@progbits,_ZTI18CInBufferException,comdat
	.weak	_ZTI18CInBufferException
	.p2align	4
_ZTI18CInBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CInBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI18CInBufferException, 24

	.type	_ZTSN9NCompress5NBcj28CEncoderE,@object # @_ZTSN9NCompress5NBcj28CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTSN9NCompress5NBcj28CEncoderE
	.p2align	4
_ZTSN9NCompress5NBcj28CEncoderE:
	.asciz	"N9NCompress5NBcj28CEncoderE"
	.size	_ZTSN9NCompress5NBcj28CEncoderE, 28

	.type	_ZTS15ICompressCoder2,@object # @_ZTS15ICompressCoder2
	.section	.rodata._ZTS15ICompressCoder2,"aG",@progbits,_ZTS15ICompressCoder2,comdat
	.weak	_ZTS15ICompressCoder2
	.p2align	4
_ZTS15ICompressCoder2:
	.asciz	"15ICompressCoder2"
	.size	_ZTS15ICompressCoder2, 18

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI15ICompressCoder2,@object # @_ZTI15ICompressCoder2
	.section	.rodata._ZTI15ICompressCoder2,"aG",@progbits,_ZTI15ICompressCoder2,comdat
	.weak	_ZTI15ICompressCoder2
	.p2align	4
_ZTI15ICompressCoder2:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15ICompressCoder2
	.quad	_ZTI8IUnknown
	.size	_ZTI15ICompressCoder2, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress5NBcj28CEncoderE,@object # @_ZTIN9NCompress5NBcj28CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress5NBcj28CEncoderE
	.p2align	4
_ZTIN9NCompress5NBcj28CEncoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress5NBcj28CEncoderE
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI15ICompressCoder2
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTIN9NCompress5NBcj28CEncoderE, 56

	.type	_ZTSN9NCompress5NBcj28CDecoderE,@object # @_ZTSN9NCompress5NBcj28CDecoderE
	.globl	_ZTSN9NCompress5NBcj28CDecoderE
	.p2align	4
_ZTSN9NCompress5NBcj28CDecoderE:
	.asciz	"N9NCompress5NBcj28CDecoderE"
	.size	_ZTSN9NCompress5NBcj28CDecoderE, 28

	.type	_ZTS19ICompressSetBufSize,@object # @_ZTS19ICompressSetBufSize
	.section	.rodata._ZTS19ICompressSetBufSize,"aG",@progbits,_ZTS19ICompressSetBufSize,comdat
	.weak	_ZTS19ICompressSetBufSize
	.p2align	4
_ZTS19ICompressSetBufSize:
	.asciz	"19ICompressSetBufSize"
	.size	_ZTS19ICompressSetBufSize, 22

	.type	_ZTI19ICompressSetBufSize,@object # @_ZTI19ICompressSetBufSize
	.section	.rodata._ZTI19ICompressSetBufSize,"aG",@progbits,_ZTI19ICompressSetBufSize,comdat
	.weak	_ZTI19ICompressSetBufSize
	.p2align	4
_ZTI19ICompressSetBufSize:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ICompressSetBufSize
	.quad	_ZTI8IUnknown
	.size	_ZTI19ICompressSetBufSize, 24

	.type	_ZTIN9NCompress5NBcj28CDecoderE,@object # @_ZTIN9NCompress5NBcj28CDecoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress5NBcj28CDecoderE
	.p2align	4
_ZTIN9NCompress5NBcj28CDecoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress5NBcj28CDecoderE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI15ICompressCoder2
	.quad	2                       # 0x2
	.quad	_ZTI19ICompressSetBufSize
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN9NCompress5NBcj28CDecoderE, 72


	.globl	_ZN9NCompress5NBcj28CEncoderD1Ev
	.type	_ZN9NCompress5NBcj28CEncoderD1Ev,@function
_ZN9NCompress5NBcj28CEncoderD1Ev = _ZN9NCompress5NBcj28CEncoderD2Ev
	.globl	_ZN9NCompress5NBcj28CDecoderC1Ev
	.type	_ZN9NCompress5NBcj28CDecoderC1Ev,@function
_ZN9NCompress5NBcj28CDecoderC1Ev = _ZN9NCompress5NBcj28CDecoderC2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
