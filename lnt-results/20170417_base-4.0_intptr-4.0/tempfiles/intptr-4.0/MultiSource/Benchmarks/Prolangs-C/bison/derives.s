	.text
	.file	"derives.bc"
	.globl	set_derives
	.p2align	4, 0x90
	.type	set_derives,@function
set_derives:                            # @set_derives
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	nvars(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rbx
	movslq	ntokens(%rip), %rax
	shlq	$3, %rax
	subq	%rax, %rbx
	movl	nrules(%rip), %edi
	shll	$4, %edi
	addl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r14
	movslq	nrules(%rip), %rax
	testq	%rax, %rax
	jle	.LBB0_3
# BB#1:                                 # %.lr.ph53.preheader
	movq	%rax, %rcx
	incq	%rcx
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph53
                                        # =>This Inner Loop Header: Depth=1
	movq	rlhs(%rip), %rsi
	movswq	-2(%rsi,%rcx,2), %rsi
	movq	(%rbx,%rsi,8), %rdi
	movq	%rdi, (%rdx)
	movw	%ax, 8(%rdx)
	movq	%rdx, (%rbx,%rsi,8)
	decl	%eax
	decq	%rcx
	addq	$16, %rdx
	cmpq	$1, %rcx
	jg	.LBB0_2
.LBB0_3:                                # %._crit_edge54
	movl	nvars(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movslq	ntokens(%rip), %rcx
	shlq	$3, %rcx
	subq	%rcx, %rax
	movq	%rax, derives(%rip)
	movl	nrules(%rip), %edi
	addl	nvars(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movslq	ntokens(%rip), %r8
	movslq	nsyms(%rip), %rdx
	cmpl	%edx, %r8d
	jge	.LBB0_9
# BB#4:                                 # %.lr.ph48
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB0_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
	movq	derives(%rip), %rdi
	movq	%rax, (%rdi,%rsi,8)
	movq	(%rbx,%rsi,8), %rdi
	testq	%rdi, %rdi
	je	.LBB0_8
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	8(%rdi), %ecx
	movw	%cx, (%rax)
	addq	$2, %rax
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_6
.LBB0_8:                                # %._crit_edge
                                        #   in Loop: Header=BB0_5 Depth=1
	movw	$-1, (%rax)
	addq	$2, %rax
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB0_5
.LBB0_9:                                # %._crit_edge49
	shlq	$3, %r8
	addq	%r8, %rbx
	je	.LBB0_11
# BB#10:
	movq	%rbx, %rdi
	callq	free
.LBB0_11:
	testq	%r14, %r14
	je	.LBB0_12
# BB#13:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.LBB0_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	set_derives, .Lfunc_end0-set_derives
	.cfi_endproc

	.globl	free_derives
	.p2align	4, 0x90
	.type	free_derives,@function
free_derives:                           # @free_derives
	.cfi_startproc
# BB#0:
	movq	derives(%rip), %rax
	movslq	ntokens(%rip), %rcx
	movq	(%rax,%rcx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	callq	free
	movq	derives(%rip), %rax
	movl	ntokens(%rip), %ecx
	addq	$8, %rsp
.LBB1_2:
	movslq	%ecx, %rdi
	shlq	$3, %rdi
	addq	%rax, %rdi
	je	.LBB1_3
# BB#4:
	jmp	free                    # TAILCALL
.LBB1_3:
	retq
.Lfunc_end1:
	.size	free_derives, .Lfunc_end1-free_derives
	.cfi_endproc

	.type	derives,@object         # @derives
	.comm	derives,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
