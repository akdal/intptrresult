	.text
	.file	"pconst.bc"
	.type	pzeroConst,@object      # @pzeroConst
	.data
	.p2align	1
pzeroConst:
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.byte	0                       # 0x0
	.zero	1
	.zero	2
	.size	pzeroConst, 10

	.type	pzero,@object           # @pzero
	.globl	pzero
	.p2align	3
pzero:
	.quad	pzeroConst
	.size	pzero, 8

	.type	poneConst,@object       # @poneConst
	.p2align	1
poneConst:
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.byte	0                       # 0x0
	.zero	1
	.short	1                       # 0x1
	.size	poneConst, 10

	.type	pone,@object            # @pone
	.globl	pone
	.p2align	3
pone:
	.quad	poneConst
	.size	pone, 8

	.type	ptwoConst,@object       # @ptwoConst
	.p2align	1
ptwoConst:
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.byte	0                       # 0x0
	.zero	1
	.short	2                       # 0x2
	.size	ptwoConst, 10

	.type	ptwo,@object            # @ptwo
	.globl	ptwo
	.p2align	3
ptwo:
	.quad	ptwoConst
	.size	ptwo, 8

	.type	p_oneConst,@object      # @p_oneConst
	.p2align	1
p_oneConst:
	.short	1                       # 0x1
	.short	1                       # 0x1
	.short	1                       # 0x1
	.byte	1                       # 0x1
	.zero	1
	.short	1                       # 0x1
	.size	p_oneConst, 10

	.type	p_one,@object           # @p_one
	.globl	p_one
	.p2align	3
p_one:
	.quad	p_oneConst
	.size	p_one, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
