	.text
	.file	"uudecode.bc"
	.globl	skip_to_newline
	.p2align	4, 0x90
	.type	skip_to_newline,@function
skip_to_newline:                        # @skip_to_newline
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	addq	%rdi, %rax
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	incl	%esi
	cmpb	$10, (%rax)
	leaq	1(%rax), %rax
	jne	.LBB0_1
# BB#2:
	movl	%esi, %eax
	retq
.Lfunc_end0:
	.size	skip_to_newline, .Lfunc_end0-skip_to_newline
	.cfi_endproc

	.globl	decode_char
	.p2align	4, 0x90
	.type	decode_char,@function
decode_char:                            # @decode_char
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	32(%rdi), %eax
	andl	$63, %eax
	retq
.Lfunc_end1:
	.size	decode_char, .Lfunc_end1-decode_char
	.cfi_endproc

	.globl	decode
	.p2align	4, 0x90
	.type	decode,@function
decode:                                 # @decode
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movabsq	$4294967296, %r8        # imm = 0x100000000
	movslq	%esi, %r10
	movb	(%rdi,%r10), %r9b
	xorl	%eax, %eax
	cmpb	$32, %r9b
	je	.LBB2_14
# BB#1:                                 # %.lr.ph80.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph80
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
                                        #     Child Loop BB2_12 Depth 2
	movzbl	%r9b, %r9d
	addl	$32, %r9d
	incl	%esi
	andl	$63, %r9d
	je	.LBB2_11
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	%esi, %rsi
	addq	$4, %rsi
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-4(%rdi,%rsi), %r10d
	movzbl	-3(%rdi,%rsi), %ecx
	addl	$32, %ecx
	shrl	$4, %ecx
	andl	$3, %ecx
	leal	128(%rcx,%r10,4), %ecx
	movslq	%eax, %r10
	movb	%cl, (%rdx,%r10)
	incq	%r10
	cmpl	$3, %r9d
	jl	.LBB2_6
# BB#5:                                 #   in Loop: Header=BB2_4 Depth=2
	movzbl	-3(%rdi,%rsi), %r11d
	shll	$4, %r11d
	movzbl	-2(%rdi,%rsi), %ecx
	addl	$32, %ecx
	shrl	$2, %ecx
	andl	$15, %ecx
	orl	%r11d, %ecx
	addl	$2, %eax
	movb	%cl, (%rdx,%r10)
	movzbl	-2(%rdi,%rsi), %r11d
	shll	$6, %r11d
	addl	$2048, %r11d            # imm = 0x800
	andl	$4032, %r11d            # imm = 0xFC0
	movzbl	-1(%rdi,%rsi), %ecx
	addl	$32, %ecx
	andl	$63, %ecx
	movl	%eax, %r10d
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_4 Depth=2
	cmpl	$2, %r9d
	jne	.LBB2_7
# BB#8:                                 #   in Loop: Header=BB2_4 Depth=2
	movzbl	-3(%rdi,%rsi), %r11d
	shll	$4, %r11d
	addl	$512, %r11d             # imm = 0x200
	andl	$1008, %r11d            # imm = 0x3F0
	movzbl	-2(%rdi,%rsi), %ecx
	addl	$32, %ecx
	shrl	$2, %ecx
	andl	$15, %ecx
.LBB2_9:                                #   in Loop: Header=BB2_4 Depth=2
	orl	%r11d, %ecx
	movslq	%r10d, %r11
	movl	%r10d, %eax
	incl	%eax
	movb	%cl, (%rdx,%r11)
	addq	$4, %rsi
	cmpl	$3, %r9d
	leal	-3(%r9), %ecx
	movl	%ecx, %r9d
	jg	.LBB2_4
# BB#10:                                # %._crit_edge.loopexit.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	addq	$-4, %rsi
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_7:                                #   in Loop: Header=BB2_2 Depth=1
	movl	%r10d, %eax
.LBB2_11:                               # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	movslq	%esi, %r10
	leaq	(%rdi,%r10), %rcx
	shlq	$32, %r10
	.p2align	4, 0x90
.LBB2_12:                               #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%r8, %r10
	incl	%esi
	cmpb	$10, (%rcx)
	leaq	1(%rcx), %rcx
	jne	.LBB2_12
# BB#13:                                # %skip_to_newline.exit68
                                        #   in Loop: Header=BB2_2 Depth=1
	sarq	$32, %r10
	movb	(%rdi,%r10), %r9b
	cmpb	$32, %r9b
	jne	.LBB2_2
.LBB2_14:                               # %.preheader
	leaq	(%rdi,%r10), %rcx
	shlq	$32, %r10
	.p2align	4, 0x90
.LBB2_15:                               # =>This Inner Loop Header: Depth=1
	addq	%r8, %r10
	cmpb	$10, (%rcx)
	leaq	1(%rcx), %rcx
	jne	.LBB2_15
# BB#16:                                # %skip_to_newline.exit
	movq	%r10, %rcx
	sarq	$32, %rcx
	cmpb	$101, (%rdi,%rcx)
	jne	.LBB2_20
# BB#17:
	addq	%r10, %r8
	sarq	$32, %r8
	cmpb	$110, (%rdi,%r8)
	jne	.LBB2_20
# BB#18:
	movabsq	$8589934592, %rcx       # imm = 0x200000000
	addq	%rcx, %r10
	sarq	$32, %r10
	cmpb	$100, (%rdi,%r10)
	jne	.LBB2_20
# BB#19:
	retq
.LBB2_20:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$1, %edi
	callq	exit
.Lfunc_end2:
	.size	decode, .Lfunc_end2-decode
	.cfi_endproc

	.globl	do_decode
	.p2align	4, 0x90
	.type	do_decode,@function
do_decode:                              # @do_decode
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	12(%rsp), %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	sscanf
	cmpl	$2, %eax
	jne	.LBB3_2
# BB#1:
	movq	%rbx, %rdi
	callq	strlen
	leal	12(%rax), %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	decode
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_2:
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	do_decode, .Lfunc_end3-do_decode
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 48
	subq	$144, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 192
.Lcfi14:
	.cfi_offset %rbx, -48
.Lcfi15:
	.cfi_offset %r12, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %ebp
	movl	$10000000, %edi         # imm = 0x989680
	callq	malloc
	movq	%rax, %r12
	movl	$10000000, %edi         # imm = 0x989680
	callq	malloc
	movq	%rax, %r14
	cmpl	$2, %ebp
	jl	.LBB4_3
# BB#1:
	movq	8(%r15), %rdi
	movl	$.L.str.1, %esi
	callq	fopen
	movq	%rax, %rcx
	testq	%rcx, %rcx
	je	.LBB4_9
# BB#2:
	decl	%ebp
	cmpl	$1, %ebp
	je	.LBB4_5
	jmp	.LBB4_10
.LBB4_3:
	movq	stdin(%rip), %rcx
	cmpl	$1, %ebp
	jne	.LBB4_10
.LBB4_5:
	movl	$1, %esi
	movl	$10000000, %edx         # imm = 0x989680
	movq	%r12, %rdi
	callq	fread
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	leaq	12(%rsp), %r15
	leaq	32(%rsp), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_6:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	sscanf
	cmpl	$2, %eax
	jne	.LBB4_11
# BB#7:                                 # %do_decode.exit
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	leal	12(%rax), %esi
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	decode
	movl	%eax, %ecx
	incl	%ebx
	cmpl	$100, %ebx
	jl	.LBB4_6
# BB#8:
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	xorl	%edi, %edi
	callq	exit
.LBB4_11:
	movl	$1, %edi
	callq	exit
.LBB4_10:
	movl	$.Lstr, %edi
	callq	puts
	movl	$2, %edi
	callq	exit
.LBB4_9:
	movq	8(%r15), %rdi
	callq	perror
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"begin %o %s \n"
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"r"
	.size	.L.str.1, 2

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%d\n"
	.size	.L.str.3, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Usage: uudecode [infile]"
	.size	.Lstr, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
