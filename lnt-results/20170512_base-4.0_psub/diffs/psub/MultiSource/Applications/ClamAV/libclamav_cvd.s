	.text
	.file	"libclamav_cvd.bc"
	.globl	cli_untgz
	.p2align	4, 0x90
	.type	cli_untgz,@function
cli_untgz:                              # @cli_untgz
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$680, %rsp              # imm = 0x2A8
.Lcfi6:
	.cfi_def_cfa_offset 736
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %ebx
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r14
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	%ebx, %edi
	callq	dup
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB0_1
# BB#2:
	movl	$.L.str.2, %esi
	movl	%ebp, %edi
	callq	gzdopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_3
# BB#4:
	addq	$105, %r14
	movl	%r14d, %ebp
	movl	$1, %edi
	movq	%rbp, %rsi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB0_6
# BB#5:                                 # %.preheader
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	160(%rsp), %rbp
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_37:                               #   in Loop: Header=BB0_28 Depth=1
	movl	12(%rsp), %eax
	subl	%r12d, %eax
	movl	%eax, 12(%rsp)
	cmovel	%eax, %r14d
.LBB0_28:                               # %.outer
                                        # =>This Inner Loop Header: Depth=1
	movl	$512, %edx              # imm = 0x200
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	gzread
	movl	%eax, %ecx
	orl	%r14d, %ecx
	je	.LBB0_29
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_28 Depth=1
	cmpl	$512, %eax              # imm = 0x200
	jne	.LBB0_8
# BB#10:                                #   in Loop: Header=BB0_28 Depth=1
	testl	%r14d, %r14d
	je	.LBB0_11
# BB#35:                                #   in Loop: Header=BB0_28 Depth=1
	movl	12(%rsp), %r12d
	cmpl	$512, %r12d             # imm = 0x200
	movl	$512, %eax              # imm = 0x200
	cmovael	%eax, %r12d
	movl	$1, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movq	%r13, %rcx
	callq	fwrite
	movq	%rax, %rcx
	cmpl	%r12d, %ecx
	je	.LBB0_37
	jmp	.LBB0_36
.LBB0_11:                               #   in Loop: Header=BB0_28 Depth=1
	cmpb	$0, 160(%rsp)
	je	.LBB0_29
# BB#12:                                #   in Loop: Header=BB0_28 Depth=1
	movl	$100, %edx
	leaq	48(%rsp), %r14
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	strncpy
	movb	$0, 148(%rsp)
	movl	$47, %esi
	movq	%r14, %rdi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB0_13
# BB#14:                                #   in Loop: Header=BB0_28 Depth=1
	movl	$.L.str.7, %edx
	xorl	%eax, %eax
	movq	%r14, %r8
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rcx
	callq	snprintf
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movsbl	316(%rsp), %esi
	testl	%esi, %esi
	je	.LBB0_21
# BB#15:                                #   in Loop: Header=BB0_28 Depth=1
	cmpl	$48, %esi
	jne	.LBB0_16
.LBB0_21:                               #   in Loop: Header=BB0_28 Depth=1
	testq	%r13, %r13
	je	.LBB0_25
# BB#22:                                #   in Loop: Header=BB0_28 Depth=1
	movq	%r13, %rdi
	callq	fclose
	testl	%eax, %eax
	jne	.LBB0_23
.LBB0_25:                               #   in Loop: Header=BB0_28 Depth=1
	movl	$.L.str.12, %esi
	movq	%r14, %rdi
	callq	fopen
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_26
# BB#27:                                #   in Loop: Header=BB0_28 Depth=1
	movl	$12, %edx
	leaq	35(%rsp), %r14
	movq	%r14, %rdi
	leaq	284(%rsp), %rsi
	callq	strncpy
	movb	$0, 47(%rsp)
	movl	$.L.str.14, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	leaq	12(%rsp), %rdx
	callq	sscanf
	movl	$1, %r14d
	testl	%eax, %eax
	jne	.LBB0_28
# BB#32:
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	gzclose
	movq	%r13, %rdi
	callq	fclose
	jmp	.LBB0_33
.LBB0_1:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_errmsg
	jmp	.LBB0_33
.LBB0_3:
	callq	__errno_location
	movl	(%rax), %edx
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_errmsg
	jmp	.LBB0_33
.LBB0_6:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB0_33
.LBB0_29:                               # %.loopexit
	testq	%r13, %r13
	je	.LBB0_31
# BB#30:
	movq	%r13, %rdi
	callq	fclose
.LBB0_31:
	movq	%rbx, %rdi
	callq	gzclose
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%eax, %eax
	jmp	.LBB0_34
.LBB0_8:
	movl	$.L.str.5, %edi
.LBB0_9:
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB0_19
.LBB0_36:
	movl	$.L.str.16, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	movl	%r12d, %edx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	callq	cli_errmsg
	movq	%rbp, %rdi
.LBB0_19:
	callq	free
	movq	%rbx, %rdi
	callq	gzclose
.LBB0_33:
	movl	$-1, %eax
.LBB0_34:
	addq	$680, %rsp              # imm = 0x2A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_13:
	movl	$.L.str.6, %edi
	jmp	.LBB0_9
.LBB0_16:
	cmpl	$53, %esi
	jne	.LBB0_20
# BB#17:
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB0_18
.LBB0_26:
	movl	$.L.str.13, %edi
	jmp	.LBB0_24
.LBB0_23:
	movl	$.L.str.11, %edi
.LBB0_24:
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_errmsg
	jmp	.LBB0_18
.LBB0_20:
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB0_18:
	movq	%r14, %rdi
	jmp	.LBB0_19
.Lfunc_end0:
	.size	cli_untgz, .Lfunc_end0-cli_untgz
	.cfi_endproc

	.globl	cl_cvdparse
	.p2align	4, 0x90
	.type	cl_cvdparse,@function
cl_cvdparse:                            # @cl_cvdparse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$.L.str.17, %esi
	movl	$11, %edx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_3
# BB#1:
	xorl	%ebx, %ebx
	movl	$.L.str.18, %edi
.LBB1_2:
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB1_26
.LBB1_3:
	movl	$56, %edi
	callq	cli_malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_4
# BB#5:
	movl	$1, %esi
	movl	$.L.str.20, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB1_6
# BB#7:
	movl	$2, %esi
	movl	$.L.str.20, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_8
# BB#12:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strtol
	movl	%eax, 8(%r14)
	movq	%r15, %rdi
	callq	free
	movl	$3, %esi
	movl	$.L.str.20, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_13
# BB#14:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strtol
	movl	%eax, 12(%r14)
	movq	%r15, %rdi
	callq	free
	movl	$4, %esi
	movl	$.L.str.20, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_15
# BB#16:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strtol
	movl	%eax, 16(%r14)
	movq	%r15, %rdi
	callq	free
	movl	$5, %esi
	movl	$.L.str.20, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, 24(%r14)
	testq	%rax, %rax
	je	.LBB1_17
# BB#18:
	movl	$6, %esi
	movl	$.L.str.20, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, 32(%r14)
	testq	%rax, %rax
	je	.LBB1_19
# BB#20:
	movl	$7, %esi
	movl	$.L.str.20, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, 40(%r14)
	testq	%rax, %rax
	je	.LBB1_21
# BB#22:
	movl	$8, %esi
	movl	$.L.str.20, %edx
	movq	%rbx, %rdi
	callq	cli_strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_24
# BB#23:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movl	%eax, 48(%r14)
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB1_25
.LBB1_4:
	xorl	%ebx, %ebx
	movl	$.L.str.19, %edi
	jmp	.LBB1_2
.LBB1_6:
	xorl	%ebx, %ebx
	movl	$.L.str.21, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB1_11
.LBB1_8:
	xorl	%ebx, %ebx
	movl	$.L.str.22, %edi
	jmp	.LBB1_9
.LBB1_13:
	xorl	%ebx, %ebx
	movl	$.L.str.23, %edi
	jmp	.LBB1_9
.LBB1_15:
	xorl	%ebx, %ebx
	movl	$.L.str.24, %edi
	jmp	.LBB1_9
.LBB1_17:
	xorl	%ebx, %ebx
	movl	$.L.str.25, %edi
.LBB1_9:
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%r14), %rdi
.LBB1_10:
	callq	free
.LBB1_11:
	movq	%r14, %rdi
	callq	free
.LBB1_26:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_19:
	xorl	%ebx, %ebx
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%r14), %rdi
	callq	free
	movq	24(%r14), %rdi
	jmp	.LBB1_10
.LBB1_21:
	xorl	%ebx, %ebx
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	(%r14), %rdi
	callq	free
	movq	24(%r14), %rdi
	callq	free
	movq	32(%r14), %rdi
	jmp	.LBB1_10
.LBB1_24:
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$0, 48(%r14)
.LBB1_25:
	movq	%r14, %rbx
	jmp	.LBB1_26
.Lfunc_end1:
	.size	cl_cvdparse, .Lfunc_end1-cl_cvdparse
	.cfi_endproc

	.globl	cl_cvdhead
	.p2align	4, 0x90
	.type	cl_cvdhead,@function
cl_cvdhead:                             # @cl_cvdhead
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 40
	subq	$520, %rsp              # imm = 0x208
.Lcfi23:
	.cfi_def_cfa_offset 560
.Lcfi24:
	.cfi_offset %rbx, -40
.Lcfi25:
	.cfi_offset %r12, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$.L.str.2, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_1
# BB#2:
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$512, %edx              # imm = 0x200
	movq	%r15, %rcx
	callq	fread
	movq	%rax, %r12
	testl	%r12d, %r12d
	je	.LBB2_3
# BB#4:
	movq	%r15, %rdi
	callq	fclose
	movl	%r12d, %eax
	movb	$0, (%rsp,%rax)
	leaq	-1(%rsp), %rax
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movzbl	1(%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	setne	%dl
	cmpb	$10, %cl
	setne	%bl
	cmpb	$13, %cl
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	andb	%bl, %dl
	jne	.LBB2_5
.LBB2_7:                                # %.critedge.i
	testb	%cl, %cl
	jne	.LBB2_8
	jmp	.LBB2_9
.LBB2_1:
	xorl	%ebx, %ebx
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_errmsg
	jmp	.LBB2_14
.LBB2_3:
	xorl	%ebx, %ebx
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_errmsg
	movq	%r15, %rdi
	callq	fclose
	jmp	.LBB2_14
.LBB2_8:                                # %.sink.split
	movb	$0, (%rax)
.LBB2_9:                                # %__strpbrk_c2.exit.thread
	decl	%r12d
	testl	%r12d, %r12d
	jle	.LBB2_13
# BB#10:
	movslq	%r12d, %rax
	movzbl	(%rsp,%rax), %ecx
	cmpq	$32, %rcx
	ja	.LBB2_13
# BB#11:
	movabsq	$4294976512, %rdx       # imm = 0x100002400
	btq	%rcx, %rdx
	jae	.LBB2_13
# BB#12:
	leaq	(%rsp,%rax), %rax
	jmp	.LBB2_8
.LBB2_13:                               # %.critedge
	movq	%rsp, %rdi
	callq	cl_cvdparse
	movq	%rax, %rbx
.LBB2_14:
	movq	%rbx, %rax
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	cl_cvdhead, .Lfunc_end2-cl_cvdhead
	.cfi_endproc

	.globl	cl_cvdfree
	.p2align	4, 0x90
	.type	cl_cvdfree,@function
cl_cvdfree:                             # @cl_cvdfree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	callq	free
	movq	24(%rbx), %rdi
	callq	free
	movq	32(%rbx), %rdi
	callq	free
	movq	40(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end3:
	.size	cl_cvdfree, .Lfunc_end3-cl_cvdfree
	.cfi_endproc

	.globl	cl_cvdverify
	.p2align	4, 0x90
	.type	cl_cvdverify,@function
cl_cvdverify:                           # @cl_cvdverify
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$.L.str.2, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB4_2
# BB#1:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	cli_cvdverify
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	fclose
	movl	%ebp, %eax
	jmp	.LBB4_3
.LBB4_2:
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_errmsg
	movl	$-115, %eax
.LBB4_3:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	cl_cvdverify, .Lfunc_end4-cl_cvdverify
	.cfi_endproc

	.p2align	4, 0x90
	.type	cli_cvdverify,@function
cli_cvdverify:                          # @cli_cvdverify
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
	subq	$528, %rsp              # imm = 0x210
.Lcfi39:
	.cfi_def_cfa_offset 560
.Lcfi40:
	.cfi_offset %rbx, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	fseek
	movq	%rsp, %rdi
	movl	$1, %esi
	movl	$512, %edx              # imm = 0x200
	movq	%r14, %rcx
	callq	fread
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB5_17
# BB#1:
	movb	$0, 512(%rsp)
	movl	$511, %eax              # imm = 0x1FF
	jmp	.LBB5_3
	.p2align	4, 0x90
.LBB5_2:                                # %.critedge1.3
                                        #   in Loop: Header=BB5_3 Depth=1
	movb	$0, -3(%rsp,%rax)
	addq	$-4, %rax
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rsp,%rax), %ecx
	cmpb	$32, %cl
	je	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	cmpb	$10, %cl
	jne	.LBB5_12
.LBB5_5:                                # %.critedge1
                                        #   in Loop: Header=BB5_3 Depth=1
	movb	$0, (%rsp,%rax)
	movzbl	-1(%rsp,%rax), %ecx
	cmpb	$10, %cl
	je	.LBB5_7
# BB#6:                                 # %.critedge1
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpb	$32, %cl
	jne	.LBB5_12
.LBB5_7:                                # %.critedge1.1
                                        #   in Loop: Header=BB5_3 Depth=1
	movb	$0, -1(%rsp,%rax)
	movzbl	-2(%rsp,%rax), %ecx
	cmpb	$32, %cl
	je	.LBB5_9
# BB#8:                                 # %.critedge1.1
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpb	$10, %cl
	jne	.LBB5_12
.LBB5_9:                                # %.critedge1.2
                                        #   in Loop: Header=BB5_3 Depth=1
	leaq	-2(%rax), %rcx
	movb	$0, -2(%rsp,%rax)
	cmpq	$2, %rcx
	jl	.LBB5_12
# BB#10:                                #   in Loop: Header=BB5_3 Depth=1
	movzbl	-3(%rsp,%rax), %ecx
	cmpb	$32, %cl
	je	.LBB5_2
# BB#11:                                #   in Loop: Header=BB5_3 Depth=1
	cmpb	$10, %cl
	je	.LBB5_2
.LBB5_12:                               # %.critedge
	movq	%rsp, %rdi
	callq	cl_cvdparse
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_18
# BB#13:
	testq	%r15, %r15
	je	.LBB5_15
# BB#14:
	movq	48(%rbx), %rax
	movq	%rax, 48(%r15)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	%xmm2, 32(%r15)
	movups	%xmm1, 16(%r15)
	movups	%xmm0, (%r15)
.LBB5_15:
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	cli_md5stream
	movq	%rax, %r14
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	cli_dbgmsg
	movq	24(%rbx), %rsi
	movl	$32, %edx
	movq	%r14, %rdi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB5_19
# BB#16:
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	%r14, %rdi
	callq	free
	movq	(%rbx), %rdi
	callq	free
	movq	24(%rbx), %rdi
	callq	free
	movq	32(%rbx), %rdi
	callq	free
	movq	40(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movl	$-121, %r15d
	jmp	.LBB5_20
.LBB5_17:
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
.LBB5_18:
	movl	$-119, %r15d
.LBB5_20:
	movl	%r15d, %eax
	addq	$528, %rsp              # imm = 0x210
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB5_19:
	movq	%r14, %rdi
	callq	free
	movq	(%rbx), %rdi
	callq	free
	movq	24(%rbx), %rdi
	callq	free
	movq	32(%rbx), %rdi
	callq	free
	movq	40(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB5_20
.Lfunc_end5:
	.size	cli_cvdverify, .Lfunc_end5-cli_cvdverify
	.cfi_endproc

	.globl	cli_cvdload
	.p2align	4, 0x90
	.type	cli_cvdload,@function
cli_cvdload:                            # @cli_cvdload
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 128
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movl	%ecx, %ebp
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	16(%rsp), %rsi
	movq	%r13, %rdi
	callq	cli_cvdverify
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jne	.LBB6_14
# BB#1:
	testw	%bp, %bp
	je	.LBB6_5
# BB#2:
	movl	64(%rsp), %eax
	testl	%eax, %eax
	je	.LBB6_5
# BB#3:
	leaq	8(%rsp), %rdi
	callq	time
	movl	8(%rsp), %eax
	subl	64(%rsp), %eax
	cmpl	$604801, %eax           # imm = 0x93A81
	jb	.LBB6_5
# BB#4:
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB6_5:
	movl	32(%rsp), %ebx
	callq	cl_retflevel
	cmpl	%eax, %ebx
	jbe	.LBB6_7
# BB#6:
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	cli_warnmsg
.LBB6_7:
	xorl	%edi, %edi
	callq	cli_gentemp
	movq	%rax, %rbp
	movl	$448, %esi              # imm = 0x1C0
	movq	%rbp, %rdi
	callq	mkdir
	testl	%eax, %eax
	je	.LBB6_9
# BB#8:
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	cli_errmsg
	movq	%rbp, %rdi
	callq	free
	movl	$-118, %ebx
.LBB6_14:
	movl	%ebx, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_9:
	movq	%r13, %rdi
	callq	fileno
	movl	%eax, %ebx
	movl	$512, %esi              # imm = 0x200
	xorl	%edx, %edx
	movl	%ebx, %edi
	callq	lseek
	cmpq	$-1, %rax
	je	.LBB6_10
# BB#11:
	movl	%ebx, %edi
	movq	%rbp, %rsi
	callq	cli_untgz
	testl	%eax, %eax
	je	.LBB6_13
# BB#12:
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movq	%rbp, %rdi
	callq	free
	movl	$-120, %ebx
	jmp	.LBB6_14
.LBB6_10:
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-123, %ebx
	jmp	.LBB6_14
.LBB6_13:
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movl	%r14d, %ecx
	callq	cl_load
	movl	%eax, %ebx
	movq	%rbp, %rdi
	callq	cli_rmdirs
	movq	%rbp, %rdi
	callq	free
	jmp	.LBB6_14
.Lfunc_end6:
	.size	cli_cvdload, .Lfunc_end6-cli_cvdload
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in cli_untgz()\n"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"cli_untgz: Can't duplicate descriptor %d\n"
	.size	.L.str.1, 42

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rb"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"cli_untgz: Can't gzdopen() descriptor %d, errno = %d\n"
	.size	.L.str.3, 54

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"cli_untgz: Can't allocate memory for path\n"
	.size	.L.str.4, 43

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"cli_untgz: Incomplete block read\n"
	.size	.L.str.5, 34

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"cli_untgz: Slash separators are not allowed in CVD\n"
	.size	.L.str.6, 52

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s/%s"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"cli_untgz: Unpacking %s\n"
	.size	.L.str.8, 25

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"cli_untgz: Directories are not supported in CVD\n"
	.size	.L.str.9, 49

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cli_untgz: Unknown type flag '%c'\n"
	.size	.L.str.10, 35

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"cli_untgz: Cannot close file %s\n"
	.size	.L.str.11, 33

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"wb"
	.size	.L.str.12, 3

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"cli_untgz: Cannot create file %s\n"
	.size	.L.str.13, 34

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%o"
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"cli_untgz: Invalid size in header\n"
	.size	.L.str.15, 35

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"cli_untgz: Wrote %d instead of %d (%s)\n"
	.size	.L.str.16, 40

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"ClamAV-VDB:"
	.size	.L.str.17, 12

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"cli_cvdparse: Not a CVD file\n"
	.size	.L.str.18, 30

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"cl_cvdparse: Can't allocate memory for cvd\n"
	.size	.L.str.19, 44

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	":"
	.size	.L.str.20, 2

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"cli_cvdparse: Can't parse the creation time\n"
	.size	.L.str.21, 45

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"cli_cvdparse: Can't parse the version number\n"
	.size	.L.str.22, 46

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"cli_cvdparse: Can't parse the number of signatures\n"
	.size	.L.str.23, 52

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"cli_cvdparse: Can't parse the functionality level\n"
	.size	.L.str.24, 51

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"cli_cvdparse: Can't parse the MD5 checksum\n"
	.size	.L.str.25, 44

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"cli_cvdparse: Can't parse the digital signature\n"
	.size	.L.str.26, 49

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"cli_cvdparse: Can't parse the builder name\n"
	.size	.L.str.27, 44

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"cli_cvdparse: No creation time in seconds (old file format)\n"
	.size	.L.str.28, 61

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"cl_cvdhead: Can't open file %s\n"
	.size	.L.str.29, 32

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"cl_cvdhead: Can't read CVD header in %s\n"
	.size	.L.str.30, 41

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"\n\r"
	.size	.L.str.31, 3

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"cl_cvdverify: Can't open file %s\n"
	.size	.L.str.32, 34

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"in cli_cvdload()\n"
	.size	.L.str.33, 18

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"**************************************************\n"
	.size	.L.str.34, 52

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"***  The virus database is older than 7 days!  ***\n"
	.size	.L.str.35, 52

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"***   Please update it as soon as possible.    ***\n"
	.size	.L.str.36, 52

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"***********************************************************\n"
	.size	.L.str.37, 61

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"***  This version of the ClamAV engine is outdated.     ***\n"
	.size	.L.str.38, 61

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"*** DON'T PANIC! Read http://www.clamav.net/support/faq ***\n"
	.size	.L.str.39, 61

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"cli_cvdload(): Can't create temporary directory %s\n"
	.size	.L.str.40, 52

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"cli_cvdload(): lseek(fs, 512, SEEK_SET) failed\n"
	.size	.L.str.41, 48

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"cli_cvdload(): Can't unpack CVD file.\n"
	.size	.L.str.42, 39

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"cli_cvdverify: Can't read CVD header\n"
	.size	.L.str.43, 38

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"MD5(.tar.gz) = %s\n"
	.size	.L.str.44, 19

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"cli_cvdverify: MD5 verification error\n"
	.size	.L.str.45, 39


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
