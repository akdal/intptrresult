	.text
	.file	"FilePathAutoRename.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	_Z14AutoRenamePathR11CStringBaseIwE
	.p2align	4, 0x90
	.type	_Z14AutoRenamePathR11CStringBaseIwE,@function
_Z14AutoRenamePathR11CStringBaseIwE:    # @_Z14AutoRenamePathR11CStringBaseIwE
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movq	$0, 80(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
	movq	%r12, 72(%rsp)
	movl	$0, (%r12)
	movl	$4, 84(%rsp)
	movslq	8(%r13), %rbp
	testq	%rbp, %rbp
	movl	$-1, %r14d
	je	.LBB0_1
# BB#2:
	movq	(%r13), %rax
	leaq	(,%rbp,4), %rcx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rax,%rcx)
	je	.LBB0_4
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	addq	$-4, %rcx
	jne	.LBB0_3
# BB#6:
	movl	$-1, %r15d
	jmp	.LBB0_7
.LBB0_1:
	movl	$-1, %r15d
	jmp	.LBB0_11
.LBB0_4:
	leaq	-4(%rax,%rcx), %r15
	subq	%rax, %r15
	shrq	$2, %r15
.LBB0_7:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	leaq	(,%rbp,4), %rcx
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%rax,%rcx)
	je	.LBB0_9
# BB#10:                                #   in Loop: Header=BB0_8 Depth=1
	addq	$-4, %rcx
	jne	.LBB0_8
	jmp	.LBB0_11
.LBB0_9:
	leaq	-4(%rax,%rcx), %r14
	subq	%rax, %r14
	shrq	$2, %r14
.LBB0_11:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit49
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
.Ltmp0:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp1:
# BB#12:
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	$4, 12(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
.Ltmp3:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp4:
# BB#13:
	movq	%r12, 48(%rsp)
	movl	$0, (%r12)
	movl	$4, 60(%rsp)
	testl	%r15d, %r15d
	movq	%r13, 64(%rsp)          # 8-byte Spill
	jle	.LBB0_44
# BB#14:
	cmpl	%r14d, %r15d
	jle	.LBB0_44
# BB#15:
.Ltmp8:
	leaq	32(%rsp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	%r15d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp9:
# BB#16:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
	movl	$0, 8(%rsp)
	movq	(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	40(%rsp), %r13
	incq	%r13
	movl	12(%rsp), %ebp
	cmpl	%ebp, %r13d
	je	.LBB0_22
# BB#17:
	movl	$4, %ecx
	movq	%r13, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp11:
	callq	_Znam
	movq	%rax, %r14
.Ltmp12:
# BB#18:                                # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB0_21
# BB#19:                                # %.noexc
	testl	%ebp, %ebp
	jle	.LBB0_21
# BB#20:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
.LBB0_21:                               # %._crit_edge16.i.i
	movq	%r14, (%rsp)
	movl	$0, (%r14,%rax,4)
	movl	%r13d, 12(%rsp)
	movq	%r14, %rbx
.LBB0_22:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	leaq	8(%rsp), %r14
	movq	32(%rsp), %rdi
	xorl	%eax, %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_23:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_23
# BB#24:
	movl	40(%rsp), %eax
	movl	%eax, 8(%rsp)
	testq	%rdi, %rdi
	je	.LBB0_26
# BB#25:
	callq	_ZdaPv
.LBB0_26:                               # %_ZN11CStringBaseIwED2Ev.exit52
	movl	8(%rbp), %ecx
	subl	%r15d, %ecx
.Ltmp14:
	leaq	32(%rsp), %rdi
	movq	%rbp, %rsi
	movl	%r15d, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp15:
# BB#27:                                # %_ZNK11CStringBaseIwE3MidEi.exit
	movl	$0, 56(%rsp)
	movl	$0, (%r12)
	movslq	40(%rsp), %rbx
	incq	%rbx
	cmpl	$4, %ebx
	je	.LBB0_30
# BB#28:
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp17:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp18:
# BB#29:                                # %._crit_edge16.i.i58
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	%rbp, 48(%rsp)
	movl	$0, (%rbp)
	movl	%ebx, 60(%rsp)
	movq	%rbp, %r12
.LBB0_30:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i59
	movq	32(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_31:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%r12,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB0_31
# BB#32:
	movl	40(%rsp), %eax
	movl	%eax, 56(%rsp)
	testq	%rdi, %rdi
	je	.LBB0_34
# BB#33:
	callq	_ZdaPv
.LBB0_34:                               # %_ZN11CStringBaseIwED2Ev.exit64
	movl	8(%rsp), %ebx
	movl	12(%rsp), %ebp
	jmp	.LBB0_53
.LBB0_44:
	movq	%rsp, %rax
	cmpq	%r13, %rax
	leaq	8(%rsp), %r14
	je	.LBB0_45
# BB#46:
	movl	$0, 8(%rsp)
	movl	$0, (%rbx)
	incl	%ebp
	cmpl	$4, %ebp
	jne	.LBB0_48
# BB#47:
	movl	$4, %ebp
	jmp	.LBB0_50
.LBB0_45:
	xorl	%ebx, %ebx
	movl	$4, %ebp
	jmp	.LBB0_53
.LBB0_48:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp6:
	callq	_Znam
	movq	%rax, %r15
.Ltmp7:
# BB#49:                                # %._crit_edge16.i.i71
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%rsp), %rax
	movq	%r15, (%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 12(%rsp)
	movq	%r15, %rbx
	movq	64(%rsp), %r13          # 8-byte Reload
.LBB0_50:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i72
	movq	(%r13), %rax
	.p2align	4, 0x90
.LBB0_51:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB0_51
# BB#52:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r13), %ebx
	movl	%ebx, 8(%rsp)
.LBB0_53:                               # %_ZN11CStringBaseIwEaSERKS0_.exit76
	movl	%ebp, %eax
	subl	%ebx, %eax
	cmpl	$1, %eax
	jg	.LBB0_82
# BB#54:
	leal	-1(%rax), %ecx
	cmpl	$8, %ebp
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	jl	.LBB0_56
# BB#55:                                # %select.true.sink
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB0_56:                               # %select.end
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbp,%rsi), %r13d
	cmpl	%ebp, %r13d
	je	.LBB0_82
# BB#57:
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp20:
	callq	_Znam
	movq	%rax, %r15
.Ltmp21:
# BB#58:                                # %.noexc82
	testl	%ebp, %ebp
	jle	.LBB0_81
# BB#59:                                # %.preheader.i.i
	movq	(%rsp), %rdi
	testl	%ebx, %ebx
	jle	.LBB0_79
# BB#60:                                # %.lr.ph.i.i
	movslq	%ebx, %rax
	cmpl	$7, %ebx
	jbe	.LBB0_61
# BB#68:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB0_61
# BB#69:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r15
	jae	.LBB0_71
# BB#70:                                # %vector.memcheck
	leaq	(%r15,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_71
.LBB0_61:
	xorl	%ecx, %ecx
.LBB0_62:                               # %scalar.ph.preheader
	subl	%ecx, %ebx
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbx
	je	.LBB0_65
# BB#63:                                # %scalar.ph.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB0_64:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r15,%rcx,4)
	incq	%rcx
	incq	%rbx
	jne	.LBB0_64
.LBB0_65:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB0_80
# BB#66:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r15,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_67:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB0_67
	jmp	.LBB0_80
.LBB0_79:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB0_81
.LBB0_80:                               # %._crit_edge.thread.i.i79
	callq	_ZdaPv
	movl	(%r14), %ebx
.LBB0_81:                               # %._crit_edge16.i.i80
	movq	%r15, (%rsp)
	movslq	%ebx, %rax
	movl	$0, (%r15,%rax,4)
	movl	%r13d, 12(%rsp)
.LBB0_82:                               # %_ZN11CStringBaseIwEpLEw.exit
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	(%rsp), %rax
	movslq	(%r14), %rcx
	movl	$95, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, (%r14)
	movl	$0, 4(%rax,%rcx,4)
	movl	$1, %ebx
	movl	$1073741824, %r12d      # imm = 0x40000000
	movq	%rsp, %r15
	leaq	48(%rsp), %r13
	leaq	72(%rsp), %rbp
	.p2align	4, 0x90
.LBB0_83:                               # =>This Inner Loop Header: Depth=1
	leal	(%r12,%rbx), %r14d
	shrl	%r14d
.Ltmp23:
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	%r14d, %edx
	movq	%rbp, %rcx
	callq	_ZL12MakeAutoNameRK11CStringBaseIwES2_jRS0_
.Ltmp24:
# BB#84:                                #   in Loop: Header=BB0_83 Depth=1
	leal	1(%r14), %ecx
	testb	%al, %al
	cmovnel	%ecx, %ebx
	cmovnel	%r12d, %r14d
	cmpl	%r14d, %ebx
	movl	%r14d, %r12d
	jne	.LBB0_83
# BB#85:
.Ltmp26:
	movq	%rsp, %rdi
	leaq	48(%rsp), %rsi
	movl	%ebx, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	_ZL12MakeAutoNameRK11CStringBaseIwES2_jRS0_
	movl	%eax, %ebx
.Ltmp27:
# BB#86:                                # %_ZN11CStringBaseIwED2Ev.exit83
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_88
# BB#87:
	callq	_ZdaPv
.LBB0_88:                               # %_ZN11CStringBaseIwED2Ev.exit84
	xorb	$1, %bl
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_90
# BB#89:
	callq	_ZdaPv
.LBB0_90:                               # %_ZN11CStringBaseIwED2Ev.exit85
	movl	%ebx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_71:                               # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_72
# BB#73:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_74:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbp,4), %xmm0
	movups	16(%rdi,%rbp,4), %xmm1
	movups	%xmm0, (%r15,%rbp,4)
	movups	%xmm1, 16(%r15,%rbp,4)
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB0_74
	jmp	.LBB0_75
.LBB0_72:
	xorl	%ebp, %ebp
.LBB0_75:                               # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB0_78
# BB#76:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r15,%rbp,4), %rsi
	leaq	112(%rdi,%rbp,4), %rbp
	.p2align	4, 0x90
.LBB0_77:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-32, %rdx
	jne	.LBB0_77
.LBB0_78:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB0_62
	jmp	.LBB0_80
.LBB0_42:
.Ltmp19:
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%rax, %r14
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_93
# BB#43:
	callq	_ZdaPv
	jmp	.LBB0_93
.LBB0_39:
.Ltmp13:
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%rax, %r14
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_93
# BB#40:
	callq	_ZdaPv
	jmp	.LBB0_93
.LBB0_98:
.Ltmp22:
	movq	%r12, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_92
.LBB0_41:
.Ltmp16:
	movq	%r12, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_92
.LBB0_37:
.Ltmp10:
	movq	%r12, 24(%rsp)          # 8-byte Spill
	jmp	.LBB0_92
.LBB0_91:
.Ltmp28:
	jmp	.LBB0_92
.LBB0_36:                               # %.thread
.Ltmp5:
	movq	%rax, %r14
	jmp	.LBB0_94
.LBB0_35:                               # %.thread116
.Ltmp2:
	movq	%rax, %r14
	jmp	.LBB0_96
.LBB0_99:
.Ltmp25:
.LBB0_92:
	movq	%rax, %r14
.LBB0_93:
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movq	(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB0_95
.LBB0_94:
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB0_95:
	movq	72(%rsp), %r12
	testq	%r12, %r12
	je	.LBB0_97
.LBB0_96:
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB0_97:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z14AutoRenamePathR11CStringBaseIwE, .Lfunc_end0-_Z14AutoRenamePathR11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 8 <<
	.long	.Ltmp21-.Ltmp6          #   Call between .Ltmp6 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin0   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin0   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Lfunc_end0-.Ltmp27     #   Call between .Ltmp27 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL12MakeAutoNameRK11CStringBaseIwES2_jRS0_,@function
_ZL12MakeAutoNameRK11CStringBaseIwES2_jRS0_: # @_ZL12MakeAutoNameRK11CStringBaseIwES2_jRS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	leaq	16(%rsp), %rbx
	movl	%edx, %edi
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt32ToStringjPw
	cmpq	%r15, %r12
	je	.LBB1_11
# BB#1:
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movl	$0, 8(%r15)
	movq	(%r15), %r13
	movl	$0, (%r13)
	movslq	8(%r12), %rbp
	incq	%rbp
	movl	12(%r15), %r14d
	cmpl	%r14d, %ebp
	jne	.LBB1_3
# BB#2:
	movq	8(%rsp), %r14           # 8-byte Reload
	jmp	.LBB1_8
.LBB1_3:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	xorl	%ecx, %ecx
	testq	%r13, %r13
	je	.LBB1_4
# BB#5:
	testl	%r14d, %r14d
	movq	8(%rsp), %r14           # 8-byte Reload
	jle	.LBB1_7
# BB#6:                                 # %._crit_edge.thread.i.i
	movq	%r13, %rdi
	movq	%rax, %r13
	callq	_ZdaPv
	movq	%r13, %rax
	movslq	8(%r15), %rcx
	jmp	.LBB1_7
.LBB1_4:
	movq	8(%rsp), %r14           # 8-byte Reload
.LBB1_7:                                # %._crit_edge16.i.i
	movq	%rax, (%r15)
	movl	$0, (%rax,%rcx,4)
	movl	%ebp, 12(%r15)
	movq	%rax, %r13
.LBB1_8:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r12), %rax
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%r13)
	addq	$4, %r13
	testl	%ecx, %ecx
	jne	.LBB1_9
# BB#10:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r12), %eax
	movl	%eax, 8(%r15)
.LBB1_11:                               # %_ZN11CStringBaseIwEaSERKS0_.exit.preheader
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB1_12:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rbx)
	leaq	4(%rbx), %rbx
	jne	.LBB1_12
# BB#13:                                # %_Z11MyStringLenIwEiPKT_.exit.i
	leaq	16(%rsp), %rbx
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	movslq	8(%r15), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r15), %rcx
	.p2align	4, 0x90
.LBB1_14:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB1_14
# BB#15:                                # %_ZN11CStringBaseIwEpLEPKw.exit
	addl	%eax, %ebp
	movl	%ebp, 8(%r15)
	movl	8(%r14), %esi
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	movq	(%r15), %rdi
	movslq	8(%r15), %rax
	leaq	(%rdi,%rax,4), %rcx
	movq	(%r14), %rdx
	.p2align	4, 0x90
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB1_16
# BB#17:                                # %_ZN11CStringBaseIwEpLERKS0_.exit
	movl	8(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%r15)
	callq	_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKw
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZL12MakeAutoNameRK11CStringBaseIwES2_jRS0_, .Lfunc_end1-_ZL12MakeAutoNameRK11CStringBaseIwES2_jRS0_
	.cfi_endproc

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -48
.Lcfi32:
	.cfi_offset %r12, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB2_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB2_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB2_3:                                # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB2_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB2_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB2_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB2_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB2_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB2_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_17
.LBB2_7:
	xorl	%ecx, %ecx
.LBB2_8:                                # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB2_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB2_10:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB2_10
.LBB2_11:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB2_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB2_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB2_13
	jmp	.LBB2_26
.LBB2_25:                               # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB2_27
.LBB2_26:                               # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB2_27:                               # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB2_28:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_17:                               # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB2_20
	jmp	.LBB2_21
.LBB2_18:
	xorl	%ebx, %ebx
.LBB2_21:                               # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB2_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB2_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB2_23
.LBB2_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB2_8
	jmp	.LBB2_26
.Lfunc_end2:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end2-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB3_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB3_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB3_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB3_5
.LBB3_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB3_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp29:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp30:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB3_35
# BB#12:
	movq	%rbx, %r13
.LBB3_13:                               # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB3_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB3_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB3_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB3_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB3_15
.LBB3_14:
	xorl	%esi, %esi
.LBB3_15:                               # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB3_16:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB3_16
.LBB3_29:
	movq	%r13, %rbx
.LBB3_30:                               # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp31:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp32:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB3_32:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB3_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB3_8
.LBB3_3:
	xorl	%eax, %eax
.LBB3_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB3_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB3_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB3_30
.LBB3_21:                               # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB3_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_24:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB3_24
	jmp	.LBB3_25
.LBB3_22:
	xorl	%ecx, %ecx
.LBB3_25:                               # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB3_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB3_27
.LBB3_28:                               # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB3_15
	jmp	.LBB3_29
.LBB3_34:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp33:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end3-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp29-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp32-.Ltmp29         #   Call between .Ltmp29 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp32     #   Call between .Ltmp32 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
