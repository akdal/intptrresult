	.text
	.file	"IOtestA.bc"
	.globl	initA
	.p2align	4, 0x90
	.type	initA,@function
initA:                                  # @initA
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%edi, %edi
	callq	setac
	movl	$16777471, (%rbx)       # imm = 0x10000FF
	popq	%rbx
	retq
.Lfunc_end0:
	.size	initA, .Lfunc_end0-initA
	.cfi_endproc

	.globl	stepA
	.p2align	4, 0x90
	.type	stepA,@function
stepA:                                  # @stepA
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	callq	getac
	movq	%rax, %rdi
	callq	array
	movsbl	(%rbx), %edi
	movsbl	%al, %ebp
	movl	%ebp, %esi
	callq	min
	movb	%al, (%rbx)
	movsbl	1(%rbx), %edi
	movl	%ebp, %esi
	callq	max
	movb	%al, 1(%rbx)
	movsbl	2(%rbx), %edi
	movl	%ebp, %esi
	callq	add
	movb	%al, 2(%rbx)
	movsbl	3(%rbx), %edi
	movl	%ebp, %esi
	callq	mult
	movb	%al, 3(%rbx)
	callq	getac
	leaq	1(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	setac                   # TAILCALL
.Lfunc_end1:
	.size	stepA, .Lfunc_end1-stepA
	.cfi_endproc

	.globl	testA
	.p2align	4, 0x90
	.type	testA,@function
testA:                                  # @testA
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	callq	initarray
	movq	%rsp, %rdx
	movl	$initA, %edi
	movl	$stepA, %esi
	callq	loop
	movsbl	(%rsp), %esi
	movsbl	1(%rsp), %edx
	movsbl	2(%rsp), %ecx
	movsbl	3(%rsp), %r8d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	popq	%rax
	retq
.Lfunc_end2:
	.size	testA, .Lfunc_end2-testA
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"A %d min %d max %d add %d mult \n"
	.size	.L.str, 33


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
