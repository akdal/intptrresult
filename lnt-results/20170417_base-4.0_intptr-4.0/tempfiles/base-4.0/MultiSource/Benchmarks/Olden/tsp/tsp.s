	.text
	.file	"tsp.bc"
	.globl	tsp
	.p2align	4, 0x90
	.type	tsp,@function
tsp:                                    # @tsp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r13
	cmpl	%r15d, (%r13)
	jle	.LBB0_1
# BB#22:
	movl	%edx, %ebp
	shrl	$31, %ebp
	addl	%edx, %ebp
	sarl	%ebp
	movq	24(%r13), %rdi
	movq	32(%r13), %r12
	movl	%r15d, %esi
	movl	%ebp, %edx
	callq	tsp
	movq	%rax, %r14
	movq	%r12, %rdi
	movl	%r15d, %esi
	movl	%ebp, %edx
	callq	tsp
	movq	%rax, %r15
	movsd	8(%r13), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r13), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r14), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r14), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	sqrtsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm2
	jnp	.LBB0_24
# BB#23:                                # %call.sqrt36
	callq	sqrt
	movapd	%xmm0, %xmm2
.LBB0_24:                               # %.split35
	movq	40(%r14), %rbp
	cmpq	%r14, %rbp
	je	.LBB0_25
# BB#26:                                # %.lr.ph82.i.preheader
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph82.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%rbp), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbp), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r13), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r13), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_29
# BB#28:                                # %call.sqrt37
                                        #   in Loop: Header=BB0_27 Depth=1
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB0_29:                               # %.lr.ph82.i.split
                                        #   in Loop: Header=BB0_27 Depth=1
	ucomisd	%xmm1, %xmm2
	minsd	%xmm2, %xmm1
	cmovaq	%rbp, %rbx
	movq	40(%rbp), %rbp
	cmpq	%r14, %rbp
	movapd	%xmm1, %xmm2
	jne	.LBB0_27
# BB#30:                                # %._crit_edge83.loopexit.i
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movq	40(%rbx), %r14
	jmp	.LBB0_31
.LBB0_1:
	movq	%r13, %rdi
	callq	makelist
	movq	%rax, %r13
	movq	40(%r13), %r14
	movq	%r13, 40(%r13)
	movq	%r13, 48(%r13)
	testq	%r14, %r14
	je	.LBB0_86
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_6 Depth 2
	movq	40(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r13), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r13), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm2
	jnp	.LBB0_4
# BB#3:                                 # %call.sqrt
                                        #   in Loop: Header=BB0_2 Depth=1
	callq	sqrt
	movapd	%xmm0, %xmm2
.LBB0_4:                                # %.split
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	40(%r13), %rbp
	cmpq	%r13, %rbp
	movq	%r13, %r15
	movq	%r13, %rbx
	je	.LBB0_10
# BB#5:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB0_6:                                # %.lr.ph.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	8(%rbp), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%rbp), %xmm1         # xmm1 = mem[0],zero
	subsd	8(%r14), %xmm0
	mulsd	%xmm0, %xmm0
	subsd	16(%r14), %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_8
# BB#7:                                 # %call.sqrt30
                                        #   in Loop: Header=BB0_6 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_8:                                # %.lr.ph.i.split
                                        #   in Loop: Header=BB0_6 Depth=2
	ucomisd	%xmm0, %xmm2
	minsd	%xmm2, %xmm0
	cmovaq	%rbp, %rbx
	movq	40(%rbp), %rbp
	cmpq	%r13, %rbp
	movapd	%xmm0, %xmm2
	jne	.LBB0_6
# BB#9:                                 # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	40(%rbx), %r15
.LBB0_10:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	48(%rbx), %r12
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r15), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r15), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_12
# BB#11:                                # %call.sqrt31
                                        #   in Loop: Header=BB0_2 Depth=1
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB0_12:                               # %._crit_edge.i.split
                                        #   in Loop: Header=BB0_2 Depth=1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r12), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r12), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm2
	jnp	.LBB0_14
# BB#13:                                # %call.sqrt32
                                        #   in Loop: Header=BB0_2 Depth=1
	callq	sqrt
	movapd	%xmm0, %xmm2
.LBB0_14:                               # %._crit_edge.i.split.split
                                        #   in Loop: Header=BB0_2 Depth=1
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r15), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r15), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	sqrtsd	%xmm0, %xmm3
	ucomisd	%xmm3, %xmm3
	jnp	.LBB0_16
# BB#15:                                # %call.sqrt33
                                        #   in Loop: Header=BB0_2 Depth=1
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm3
.LBB0_16:                               # %._crit_edge.i.split.split.split
                                        #   in Loop: Header=BB0_2 Depth=1
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r12), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r12), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_18
# BB#17:                                # %call.sqrt34
                                        #   in Loop: Header=BB0_2 Depth=1
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB0_18:                               # %._crit_edge.i.split.split.split.split
                                        #   in Loop: Header=BB0_2 Depth=1
	subsd	%xmm2, %xmm1
	subsd	(%rsp), %xmm3           # 8-byte Folded Reload
	ucomisd	%xmm1, %xmm3
	jbe	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_2 Depth=1
	movq	%r14, 40(%r12)
	movq	%rbx, 40(%r14)
	movq	%r12, 48(%r14)
	movq	%rbx, %rax
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_20:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r14, 48(%r15)
	movq	%r15, 40(%r14)
	movq	%r14, 40(%rbx)
	movq	%r14, %rax
	movq	%rbx, %r14
.LBB0_21:                               # %.backedge.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r14, 48(%rax)
	testq	%rcx, %rcx
	movq	%rcx, %r14
	jne	.LBB0_2
	jmp	.LBB0_86
.LBB0_25:
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	movq	%r14, %rbx
.LBB0_31:                               # %._crit_edge83.i
	movq	48(%rbx), %r12
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r14), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r14), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_33
# BB#32:                                # %call.sqrt38
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB0_33:                               # %._crit_edge83.i.split
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r12), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r12), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm2
	jnp	.LBB0_35
# BB#34:                                # %call.sqrt39
	callq	sqrt
	movapd	%xmm0, %xmm2
.LBB0_35:                               # %._crit_edge83.i.split.split
	movsd	8(%r13), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r13), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r14), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r14), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_37
# BB#36:                                # %call.sqrt40
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB0_37:                               # %._crit_edge83.i.split.split.split
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	8(%r13), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%r13), %xmm1         # xmm1 = mem[0],zero
	subsd	8(%r12), %xmm0
	mulsd	%xmm0, %xmm0
	subsd	16(%r12), %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_39
# BB#38:                                # %call.sqrt41
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	sqrt
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_39:                               # %._crit_edge83.i.split.split.split.split
	movapd	%xmm0, %xmm1
	subsd	%xmm2, %xmm1
	movsd	32(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	subsd	(%rsp), %xmm2           # 8-byte Folded Reload
	ucomisd	%xmm1, %xmm2
	cmovaq	%rbx, %r14
	cmovbeq	%rbx, %r12
	jbe	.LBB0_41
# BB#40:
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
.LBB0_41:                               # %._crit_edge83.i.split.split.split.split
	movsd	8(%r13), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r13), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r15), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r15), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm2
	jnp	.LBB0_43
# BB#42:                                # %call.sqrt42
	callq	sqrt
	movapd	%xmm0, %xmm2
.LBB0_43:                               # %._crit_edge83.i.split.split.split.split.split
	movq	40(%r15), %rbp
	cmpq	%r15, %rbp
	je	.LBB0_44
# BB#45:                                # %.lr.ph.i26.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB0_46:                               # %.lr.ph.i26
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%rbp), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbp), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r13), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r13), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_48
# BB#47:                                # %call.sqrt43
                                        #   in Loop: Header=BB0_46 Depth=1
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB0_48:                               # %.lr.ph.i26.split
                                        #   in Loop: Header=BB0_46 Depth=1
	ucomisd	%xmm1, %xmm2
	minsd	%xmm2, %xmm1
	cmovaq	%rbp, %rbx
	movq	40(%rbp), %rbp
	cmpq	%r15, %rbp
	movapd	%xmm1, %xmm2
	jne	.LBB0_46
# BB#49:                                # %._crit_edge.loopexit.i27
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movq	40(%rbx), %r15
	jmp	.LBB0_50
.LBB0_44:
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movq	%r15, %rbx
.LBB0_50:                               # %._crit_edge.i29
	movq	48(%rbx), %rbp
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r15), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r15), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_52
# BB#51:                                # %call.sqrt44
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB0_52:                               # %._crit_edge.i29.split
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	movsd	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%rbp), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%rbp), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm2
	jnp	.LBB0_54
# BB#53:                                # %call.sqrt45
	callq	sqrt
	movapd	%xmm0, %xmm2
.LBB0_54:                               # %._crit_edge.i29.split.split
	movsd	8(%r13), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r13), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r15), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r15), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	sqrtsd	%xmm0, %xmm4
	ucomisd	%xmm4, %xmm4
	jnp	.LBB0_56
# BB#55:                                # %call.sqrt46
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm4
.LBB0_56:                               # %._crit_edge.i29.split.split.split
	movsd	8(%r13), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%r13), %xmm1         # xmm1 = mem[0],zero
	subsd	8(%rbp), %xmm0
	mulsd	%xmm0, %xmm0
	subsd	16(%rbp), %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_58
# BB#57:                                # %call.sqrt47
	movapd	%xmm1, %xmm0
	movsd	%xmm4, (%rsp)           # 8-byte Spill
	movsd	%xmm2, 24(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	24(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
.LBB0_58:                               # %._crit_edge.i29.split.split.split.split
	movapd	%xmm0, %xmm1
	subsd	%xmm2, %xmm1
	movapd	%xmm4, %xmm2
	subsd	40(%rsp), %xmm2         # 8-byte Folded Reload
	ucomisd	%xmm1, %xmm2
	cmovaq	%rbx, %r15
	cmovbeq	%rbx, %rbp
	jbe	.LBB0_60
# BB#59:
	movsd	8(%rsp), %xmm4          # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
.LBB0_60:                               # %._crit_edge.i29.split.split.split.split
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r15), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r15), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	movsd	%xmm4, (%rsp)           # 8-byte Spill
	jnp	.LBB0_62
# BB#61:                                # %call.sqrt48
	callq	sqrt
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB0_62:                               # %._crit_edge.i29.split.split.split.split.split
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%rbp), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%rbp), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	sqrtsd	%xmm0, %xmm5
	ucomisd	%xmm5, %xmm5
	jnp	.LBB0_64
# BB#63:                                # %call.sqrt49
	callq	sqrt
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm5
.LBB0_64:                               # %._crit_edge.i29.split.split.split.split.split.split
	movsd	8(%r12), %xmm1          # xmm1 = mem[0],zero
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%r15), %xmm1
	mulsd	%xmm1, %xmm1
	subsd	16(%r15), %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	sqrtsd	%xmm0, %xmm6
	ucomisd	%xmm6, %xmm6
	jnp	.LBB0_66
# BB#65:                                # %call.sqrt50
	movsd	%xmm5, 24(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	24(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm6
.LBB0_66:                               # %._crit_edge.i29.split.split.split.split.split.split.split
	movsd	8(%r12), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%r12), %xmm1         # xmm1 = mem[0],zero
	subsd	8(%rbp), %xmm0
	mulsd	%xmm0, %xmm0
	subsd	16(%rbp), %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_68
# BB#67:                                # %call.sqrt51
	movapd	%xmm1, %xmm0
	movsd	%xmm5, 24(%rsp)         # 8-byte Spill
	movsd	%xmm6, 48(%rsp)         # 8-byte Spill
	callq	sqrt
	movsd	48(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	24(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
.LBB0_68:                               # %._crit_edge.i29.split.split.split.split.split.split.split.split
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm1
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	addsd	%xmm3, %xmm1
	addsd	40(%rsp), %xmm1         # 8-byte Folded Reload
	addsd	%xmm4, %xmm2
	addsd	%xmm5, %xmm2
	ucomisd	%xmm2, %xmm1
	seta	%al
	minsd	%xmm1, %xmm2
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm3
	addsd	%xmm6, %xmm3
	ucomisd	%xmm3, %xmm2
	minsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm2
	addsd	%xmm4, %xmm1
	addsd	%xmm0, %xmm1
	movb	$3, %cl
	ja	.LBB0_70
# BB#69:                                # %._crit_edge.i29.split.split.split.split.split.split.split.split
	incb	%al
	movl	%eax, %ecx
.LBB0_70:                               # %._crit_edge.i29.split.split.split.split.split.split.split.split
	ucomisd	%xmm1, %xmm2
	movb	$4, %al
	ja	.LBB0_72
# BB#71:                                # %._crit_edge.i29.split.split.split.split.split.split.split.split
	movl	%ecx, %eax
.LBB0_72:                               # %._crit_edge.i29.split.split.split.split.split.split.split.split
	decb	%al
	cmpb	$3, %al
	ja	.LBB0_86
# BB#73:                                # %._crit_edge.i29.split.split.split.split.split.split.split.split
	movzbl	%al, %edx
	movq	%r12, %rax
	movq	%r15, %rcx
	jmpq	*.LJTI0_0(,%rdx,8)
.LBB0_74:
	testq	%r15, %r15
	je	.LBB0_75
# BB#76:
	movq	48(%r15), %rax
	movq	$0, 40(%rax)
	movq	$0, 48(%r15)
	movq	40(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB0_79
# BB#77:                                # %.lr.ph.i.i.preheader
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB0_78:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rsi
	movq	40(%rsi), %rcx
	movq	%rdx, 40(%rsi)
	movq	%rsi, 48(%rdx)
	testq	%rcx, %rcx
	movq	%rsi, %rdx
	jne	.LBB0_78
.LBB0_79:                               # %._crit_edge.i.i
	movq	%rax, 40(%r15)
	movq	%r15, 48(%rax)
	movq	%r12, %rax
	movq	%rbp, %rcx
	movq	%r15, %rbp
	jmp	.LBB0_85
.LBB0_80:
	movq	%rbp, %rax
	movq	%r14, %rcx
	movq	%r12, %rbp
	movq	%r15, %r14
	jmp	.LBB0_85
.LBB0_81:
	movq	48(%r14), %rax
	movq	$0, 40(%rax)
	movq	$0, 48(%r14)
	movq	40(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB0_84
# BB#82:                                # %.lr.ph.i67.i.preheader
	movq	%r14, %rdx
	.p2align	4, 0x90
.LBB0_83:                               # %.lr.ph.i67.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rsi
	movq	40(%rsi), %rcx
	movq	%rdx, 40(%rsi)
	movq	%rsi, 48(%rdx)
	testq	%rcx, %rcx
	movq	%rsi, %rdx
	jne	.LBB0_83
.LBB0_84:                               # %reverse.exit69.i
	movq	%rax, 40(%r14)
	movq	%r14, 48(%rax)
	movq	%r14, %rax
	movq	%r15, %rcx
	movq	%r12, %r14
	jmp	.LBB0_85
.LBB0_75:
	movq	%r12, %rax
	movq	%rbp, %rcx
	xorl	%ebp, %ebp
.LBB0_85:                               # %.sink.split.i
	movq	%r13, 40(%rax)
	movq	%rax, 48(%r13)
	movq	%rcx, 40(%r13)
	movq	%r13, 48(%rcx)
	movq	%r14, 40(%rbp)
	movq	%rbp, 48(%r14)
.LBB0_86:                               # %conquer.exit
	movq	%r13, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	tsp, .Lfunc_end0-tsp
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_74
	.quad	.LBB0_85
	.quad	.LBB0_80
	.quad	.LBB0_81

	.text
	.p2align	4, 0x90
	.type	makelist,@function
makelist:                               # @makelist
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	movq	24(%rbx), %rdi
	callq	makelist
	movq	%rax, %r14
	movq	32(%rbx), %rdi
	callq	makelist
	testq	%rax, %rax
	movq	%rbx, %rcx
	je	.LBB1_4
# BB#3:
	movq	32(%rbx), %rcx
	movq	%rbx, 40(%rcx)
	movq	%rax, %rcx
.LBB1_4:
	testq	%r14, %r14
	je	.LBB1_6
# BB#5:
	testq	%rax, %rax
	movq	24(%rbx), %rcx
	cmoveq	%rbx, %rax
	movq	%rax, 40(%rcx)
	movq	%r14, %rcx
.LBB1_6:
	movq	$0, 40(%rbx)
	jmp	.LBB1_7
.LBB1_1:
	xorl	%ecx, %ecx
.LBB1_7:
	movq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	makelist, .Lfunc_end1-makelist
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
