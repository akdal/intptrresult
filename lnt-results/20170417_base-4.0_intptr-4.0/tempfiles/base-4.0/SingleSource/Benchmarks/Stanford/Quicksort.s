	.text
	.file	"Quicksort.bc"
	.globl	Initrand
	.p2align	4, 0x90
	.type	Initrand,@function
Initrand:                               # @Initrand
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	retq
.Lfunc_end0:
	.size	Initrand, .Lfunc_end0-Initrand
	.cfi_endproc

	.globl	Rand
	.p2align	4, 0x90
	.type	Rand,@function
Rand:                                   # @Rand
	.cfi_startproc
# BB#0:
	imull	$1309, seed(%rip), %eax # imm = 0x51D
	addl	$13849, %eax            # imm = 0x3619
	movzwl	%ax, %eax
	movq	%rax, seed(%rip)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end1:
	.size	Rand, .Lfunc_end1-Rand
	.cfi_endproc

	.globl	Initarr
	.p2align	4, 0x90
	.type	Initarr,@function
Initarr:                                # @Initarr
	.cfi_startproc
# BB#0:
	movq	$74755, seed(%rip)      # imm = 0x12403
	movl	$0, biggest(%rip)
	movl	$0, littlest(%rip)
	xorl	%edx, %edx
	movq	$-4999, %rax            # imm = 0xEC79
	movl	$74755, %ecx            # imm = 0x12403
	jmp	.LBB2_1
	.p2align	4, 0x90
.LBB2_7:                                # %._crit_edge
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	biggest(%rip), %edx
	incq	%rax
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	imull	$1309, %ecx, %ecx       # imm = 0x51D
	addl	$13849, %ecx            # imm = 0x3619
	movzwl	%cx, %ecx
	leal	-50000(%rcx), %esi
	movl	%esi, sortlist+20000(,%rax,4)
	cmpl	%edx, %esi
	jle	.LBB2_3
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	$biggest, %edx
	jmp	.LBB2_5
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	cmpl	littlest(%rip), %esi
	jge	.LBB2_6
# BB#4:                                 #   in Loop: Header=BB2_1 Depth=1
	movl	$littlest, %edx
.LBB2_5:                                # %.sink.split
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	%esi, (%rdx)
.LBB2_6:                                #   in Loop: Header=BB2_1 Depth=1
	testq	%rax, %rax
	jne	.LBB2_7
# BB#8:
	movq	%rcx, seed(%rip)
	retq
.Lfunc_end2:
	.size	Initarr, .Lfunc_end2-Initarr
	.cfi_endproc

	.globl	Quicksort
	.p2align	4, 0x90
	.type	Quicksort,@function
Quicksort:                              # @Quicksort
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r13d
	movq	%rdi, %r12
	leaq	4(%r12), %r15
	.p2align	4, 0x90
.LBB3_1:                                # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_2 Depth 2
                                        #       Child Loop BB3_3 Depth 3
                                        #       Child Loop BB3_5 Depth 3
	movl	%r13d, %esi
	leal	(%rsi,%r14), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	movl	(%r12,%rax,4), %r8d
	movl	%r14d, %edx
	.p2align	4, 0x90
.LBB3_2:                                #   Parent Loop BB3_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_3 Depth 3
                                        #       Child Loop BB3_5 Depth 3
	movslq	%r13d, %rax
	leaq	(%r12,%rax,4), %rdi
	movl	%r13d, %ecx
	.p2align	4, 0x90
.LBB3_3:                                #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi), %r9d
	incl	%ecx
	addq	$4, %rdi
	cmpl	%r8d, %r9d
	jl	.LBB3_3
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB3_2 Depth=2
	movslq	%edx, %rax
	leaq	(%r15,%rax,4), %rbp
	movl	%edx, %eax
	.p2align	4, 0x90
.LBB3_5:                                #   Parent Loop BB3_1 Depth=1
                                        #     Parent Loop BB3_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rbp), %ebx
	addq	$-4, %rbp
	decl	%eax
	cmpl	%ebx, %r8d
	jl	.LBB3_5
# BB#6:                                 #   in Loop: Header=BB3_2 Depth=2
	leal	-1(%rcx), %r13d
	leal	1(%rax), %edx
	cmpl	%edx, %r13d
	jg	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_2 Depth=2
	movl	%ebx, -4(%rdi)
	movl	%r9d, (%rbp)
	movl	%eax, %edx
	movl	%ecx, %r13d
.LBB3_8:                                #   in Loop: Header=BB3_2 Depth=2
	cmpl	%edx, %r13d
	jle	.LBB3_2
# BB#9:                                 #   in Loop: Header=BB3_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB3_11
# BB#10:                                #   in Loop: Header=BB3_1 Depth=1
	movq	%r12, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	Quicksort
.LBB3_11:                               #   in Loop: Header=BB3_1 Depth=1
	cmpl	%r14d, %r13d
	jl	.LBB3_1
# BB#12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	Quicksort, .Lfunc_end3-Quicksort
	.cfi_endproc

	.globl	Quick
	.p2align	4, 0x90
	.type	Quick,@function
Quick:                                  # @Quick
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movq	$74755, seed(%rip)      # imm = 0x12403
	movl	$0, biggest(%rip)
	movl	$0, littlest(%rip)
	xorl	%edx, %edx
	movq	$-4999, %rax            # imm = 0xEC79
	movl	$74755, %ecx            # imm = 0x12403
	jmp	.LBB4_1
	.p2align	4, 0x90
.LBB4_7:                                # %._crit_edge.i
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	biggest(%rip), %edx
	incq	%rax
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	imull	$1309, %ecx, %ecx       # imm = 0x51D
	addl	$13849, %ecx            # imm = 0x3619
	movzwl	%cx, %ecx
	leal	-50000(%rcx), %esi
	movl	%esi, sortlist+20000(,%rax,4)
	cmpl	%edx, %esi
	jle	.LBB4_3
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	$biggest, %edx
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_3:                                #   in Loop: Header=BB4_1 Depth=1
	cmpl	littlest(%rip), %esi
	jge	.LBB4_6
# BB#4:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	$littlest, %edx
.LBB4_5:                                # %.sink.split.i
                                        #   in Loop: Header=BB4_1 Depth=1
	movl	%esi, (%rdx)
.LBB4_6:                                #   in Loop: Header=BB4_1 Depth=1
	testq	%rax, %rax
	jne	.LBB4_7
# BB#8:                                 # %Initarr.exit
	movq	%rcx, seed(%rip)
	movl	$sortlist, %edi
	movl	$1, %esi
	movl	$5000, %edx             # imm = 0x1388
	callq	Quicksort
	movl	sortlist+4(%rip), %eax
	cmpl	littlest(%rip), %eax
	jne	.LBB4_10
# BB#9:
	movl	sortlist+20000(%rip), %eax
	cmpl	biggest(%rip), %eax
	je	.LBB4_11
.LBB4_10:
	movl	$.Lstr, %edi
	callq	puts
.LBB4_11:
	movslq	%ebx, %rax
	movl	sortlist+4(,%rax,4), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	popq	%rbx
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	Quick, .Lfunc_end4-Quick
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
.Lcfi16:
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_2 Depth 2
	movq	$74755, seed(%rip)      # imm = 0x12403
	movl	$0, biggest(%rip)
	movl	$0, littlest(%rip)
	movq	$-4999, %rax            # imm = 0xEC79
	xorl	%edx, %edx
	movl	$74755, %ecx            # imm = 0x12403
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_8:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB5_2 Depth=2
	movl	biggest(%rip), %edx
	incq	%rax
.LBB5_2:                                #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imull	$1309, %ecx, %ecx       # imm = 0x51D
	addl	$13849, %ecx            # imm = 0x3619
	movzwl	%cx, %ecx
	leal	-50000(%rcx), %esi
	movl	%esi, sortlist+20000(,%rax,4)
	cmpl	%edx, %esi
	jle	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=2
	movl	$biggest, %edx
	jmp	.LBB5_6
	.p2align	4, 0x90
.LBB5_4:                                #   in Loop: Header=BB5_2 Depth=2
	cmpl	littlest(%rip), %esi
	jge	.LBB5_7
# BB#5:                                 #   in Loop: Header=BB5_2 Depth=2
	movl	$littlest, %edx
.LBB5_6:                                # %.sink.split.i.i
                                        #   in Loop: Header=BB5_2 Depth=2
	movl	%esi, (%rdx)
.LBB5_7:                                #   in Loop: Header=BB5_2 Depth=2
	testq	%rax, %rax
	jne	.LBB5_8
# BB#9:                                 # %Initarr.exit.i
                                        #   in Loop: Header=BB5_1 Depth=1
	movq	%rcx, seed(%rip)
	movl	$sortlist, %edi
	movl	$1, %esi
	movl	$5000, %edx             # imm = 0x1388
	callq	Quicksort
	movl	sortlist+4(%rip), %eax
	cmpl	littlest(%rip), %eax
	jne	.LBB5_11
# BB#10:                                #   in Loop: Header=BB5_1 Depth=1
	movl	sortlist+20000(%rip), %eax
	cmpl	biggest(%rip), %eax
	je	.LBB5_12
.LBB5_11:                               #   in Loop: Header=BB5_1 Depth=1
	movl	$.Lstr, %edi
	callq	puts
.LBB5_12:                               # %Quick.exit
                                        #   in Loop: Header=BB5_1 Depth=1
	movl	sortlist+4(,%rbx,4), %esi
	incq	%rbx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	cmpq	$100, %rbx
	jne	.LBB5_1
# BB#13:
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc

	.type	seed,@object            # @seed
	.comm	seed,8,8
	.type	biggest,@object         # @biggest
	.comm	biggest,4,4
	.type	littlest,@object        # @littlest
	.comm	littlest,4,4
	.type	sortlist,@object        # @sortlist
	.comm	sortlist,20004,16
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%d\n"
	.size	.L.str.1, 4

	.type	value,@object           # @value
	.comm	value,4,4
	.type	fixed,@object           # @fixed
	.comm	fixed,4,4
	.type	floated,@object         # @floated
	.comm	floated,4,4
	.type	permarray,@object       # @permarray
	.comm	permarray,44,16
	.type	pctr,@object            # @pctr
	.comm	pctr,4,4
	.type	tree,@object            # @tree
	.comm	tree,8,8
	.type	stack,@object           # @stack
	.comm	stack,16,16
	.type	cellspace,@object       # @cellspace
	.comm	cellspace,152,16
	.type	freelist,@object        # @freelist
	.comm	freelist,4,4
	.type	movesdone,@object       # @movesdone
	.comm	movesdone,4,4
	.type	ima,@object             # @ima
	.comm	ima,6724,16
	.type	imb,@object             # @imb
	.comm	imb,6724,16
	.type	imr,@object             # @imr
	.comm	imr,6724,16
	.type	rma,@object             # @rma
	.comm	rma,6724,16
	.type	rmb,@object             # @rmb
	.comm	rmb,6724,16
	.type	rmr,@object             # @rmr
	.comm	rmr,6724,16
	.type	piececount,@object      # @piececount
	.comm	piececount,16,16
	.type	class,@object           # @class
	.comm	class,52,16
	.type	piecemax,@object        # @piecemax
	.comm	piecemax,52,16
	.type	puzzl,@object           # @puzzl
	.comm	puzzl,2048,16
	.type	p,@object               # @p
	.comm	p,26624,16
	.type	n,@object               # @n
	.comm	n,4,4
	.type	kount,@object           # @kount
	.comm	kount,4,4
	.type	top,@object             # @top
	.comm	top,4,4
	.type	z,@object               # @z
	.comm	z,2056,16
	.type	w,@object               # @w
	.comm	w,2056,16
	.type	e,@object               # @e
	.comm	e,1040,16
	.type	zr,@object              # @zr
	.comm	zr,4,4
	.type	zi,@object              # @zi
	.comm	zi,4,4
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	" Error in Quick."
	.size	.Lstr, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
