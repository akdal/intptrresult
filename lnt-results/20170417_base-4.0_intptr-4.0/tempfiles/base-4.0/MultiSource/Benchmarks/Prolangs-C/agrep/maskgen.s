	.text
	.file	"maskgen.bc"
	.globl	maskgen
	.p2align	4, 0x90
	.type	maskgen,@function
maskgen:                                # @maskgen
	.cfi_startproc
# BB#0:                                 # %.preheader211.preheader312
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1528, %rsp             # imm = 0x5F8
.Lcfi6:
	.cfi_def_cfa_offset 1584
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movb	$0, 20(%rsp)
	movb	$0, 56(%rsp)
	movb	$0, 92(%rsp)
	movb	$0, 128(%rsp)
	movb	$0, 164(%rsp)
	movb	$0, 200(%rsp)
	movb	$0, 236(%rsp)
	movb	$0, 272(%rsp)
	movb	$0, 308(%rsp)
	movb	$0, 344(%rsp)
	movb	$0, 380(%rsp)
	movb	$0, 416(%rsp)
	movb	$0, 452(%rsp)
	movb	$0, 488(%rsp)
	movb	$0, 524(%rsp)
	movb	$0, 560(%rsp)
	movb	$0, 596(%rsp)
	movb	$0, 632(%rsp)
	movb	$0, 668(%rsp)
	movb	$0, 704(%rsp)
	movb	$0, 740(%rsp)
	movb	$0, 776(%rsp)
	movb	$0, 812(%rsp)
	movb	$0, 848(%rsp)
	movb	$0, 884(%rsp)
	movb	$0, 920(%rsp)
	movb	$0, 956(%rsp)
	movb	$0, 992(%rsp)
	movb	$0, 1028(%rsp)
	movb	$0, 1064(%rsp)
	movb	$0, 1100(%rsp)
	movb	$0, 1136(%rsp)
	movl	$0, 16(%rsp)
	movl	$0, 52(%rsp)
	movl	$0, 88(%rsp)
	movl	$0, 124(%rsp)
	movl	$0, 160(%rsp)
	movl	$0, 196(%rsp)
	movl	$0, 232(%rsp)
	movl	$0, 268(%rsp)
	movl	$0, 304(%rsp)
	movl	$0, 340(%rsp)
	movl	$0, 376(%rsp)
	movl	$0, 412(%rsp)
	movl	$0, 448(%rsp)
	movl	$0, 484(%rsp)
	movl	$0, 520(%rsp)
	movl	$0, 556(%rsp)
	movl	$0, 592(%rsp)
	movl	$0, 628(%rsp)
	movl	$0, 664(%rsp)
	movl	$0, 700(%rsp)
	movl	$0, 736(%rsp)
	movl	$0, 772(%rsp)
	movl	$0, 808(%rsp)
	movl	$0, 844(%rsp)
	movl	$0, 880(%rsp)
	movl	$0, 916(%rsp)
	movl	$0, 952(%rsp)
	movl	$0, 988(%rsp)
	movl	$0, 1024(%rsp)
	movl	$0, 1060(%rsp)
	movl	$0, 1096(%rsp)
	movl	$0, 1132(%rsp)
	movl	$0, endposition(%rip)
	movl	$0, NO_ERR_MASK(%rip)
	movl	$0, wildmask(%rip)
	callq	strlen
	movq	%rax, %r14
	cmpl	$0, NOUPPER(%rip)
	je	.LBB0_6
# BB#1:                                 # %.preheader211.preheader312
	testl	%r14d, %r14d
	jle	.LBB0_6
# BB#2:                                 # %.lr.ph267
	callq	__ctype_b_loc
	movq	%rax, %r12
	movl	%r14d, %ebp
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movzbl	(%rbx), %r13d
	movzwl	(%rax,%r13,2), %eax
	andl	$1280, %eax             # imm = 0x500
	cmpl	$1280, %eax             # imm = 0x500
	jne	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movzbl	(%rax,%r13,4), %eax
	movb	%al, (%rbx)
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	incq	%rbx
	decq	%rbp
	jne	.LBB0_3
.LBB0_6:                                # %.preheader209
	testl	%r14d, %r14d
	jle	.LBB0_55
# BB#7:                                 # %.lr.ph262.preheader
	movslq	%r14d, %r13
	movl	$1, %r11d
	xorl	%esi, %esi
	movabsq	$9186041809988103937, %r8 # imm = 0x7F7B605B403A2F01
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	jmp	.LBB0_8
.LBB0_47:                               #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	movl	Bit-4(,%rax,4), %eax
	orl	%eax, wildmask(%rip)
	jmp	.LBB0_52
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph262
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_39 Depth 2
                                        #       Child Loop BB0_40 Depth 3
	movslq	%esi, %rcx
	movb	(%r15,%rcx), %bl
	movl	%ebx, %eax
	addb	$17, %al
	cmpb	$27, %al
	ja	.LBB0_11
# BB#9:                                 # %.lr.ph262
                                        #   in Loop: Header=BB0_8 Depth=1
	movzbl	%al, %eax
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_10:                               #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	movl	Bit(,%rax,4), %ecx
	orl	%ecx, NO_ERR_MASK(%rip)
	leaq	(%rax,%rax,8), %rax
	movl	$-269546998, 20(%rsp,%rax,4) # imm = 0xEFEF0A0A
	movb	$0, 24(%rsp,%rax,4)
	cmpl	$32, %r11d
	jl	.LBB0_51
	jmp	.LBB0_105
.LBB0_11:                               #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	cmpl	$1, %r10d
	jne	.LBB0_13
# BB#12:                                #   in Loop: Header=BB0_8 Depth=1
	movl	Bit(,%rax,4), %edi
	orl	%edi, NO_ERR_MASK(%rip)
	movb	(%r15,%rcx), %bl
.LBB0_13:                               #   in Loop: Header=BB0_8 Depth=1
	leaq	(%rax,%rax,8), %rax
	movl	$0, 16(%rsp,%rax,4)
	movb	%bl, 21(%rsp,%rax,4)
	movb	%bl, 20(%rsp,%rax,4)
	jmp	.LBB0_34
.LBB0_14:                               #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	movl	Bit(,%rax,4), %ecx
	orl	%ecx, NO_ERR_MASK(%rip)
	leaq	(%rax,%rax,8), %rax
	movq	%r8, 20(%rsp,%rax,4)
	movb	$0, 28(%rsp,%rax,4)
	cmpl	$32, %r11d
	jl	.LBB0_51
	jmp	.LBB0_105
.LBB0_15:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$1, %r10d
	jne	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	movl	Bit(,%rax,4), %eax
	orl	%eax, NO_ERR_MASK(%rip)
.LBB0_17:                               #   in Loop: Header=BB0_8 Depth=1
	movb	1(%r15,%rcx), %bl
	cmpb	$-8, %bl
	jne	.LBB0_35
# BB#18:                                #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	leaq	(%rax,%rax,8), %rax
	movl	$1, 16(%rsp,%rax,4)
	movb	2(%r15,%rcx), %bl
	addq	$2, %rcx
	jmp	.LBB0_36
.LBB0_19:                               #   in Loop: Header=BB0_8 Depth=1
	incl	%r9d
	movl	$1, %r10d
	jmp	.LBB0_52
.LBB0_20:                               #   in Loop: Header=BB0_8 Depth=1
	testl	%r9d, %r9d
	jle	.LBB0_54
# BB#21:                                #   in Loop: Header=BB0_8 Depth=1
	decl	%r9d
	xorl	%r10d, %r10d
	jmp	.LBB0_52
.LBB0_22:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$0, REGEX(%rip)
	je	.LBB0_47
# BB#23:                                #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	leaq	(%rax,%rax,8), %rax
	movw	$11822, 20(%rsp,%rax,4) # imm = 0x2E2E
	jmp	.LBB0_34
.LBB0_24:                               #   in Loop: Header=BB0_8 Depth=1
	cmpl	$1, REGEX(%rip)
	je	.LBB0_107
# BB#25:                                #   in Loop: Header=BB0_8 Depth=1
	cmpl	$1, AND(%rip)
	je	.LBB0_107
# BB#26:                                #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	leaq	(%rax,%rax,8), %rcx
	movl	$2, 16(%rsp,%rcx,4)
	movb	$0, 20(%rsp,%rcx,4)
	movl	Bit(,%rax,4), %eax
	orl	%eax, endposition(%rip)
	movl	$1, %edx
	cmpl	$32, %r11d
	jl	.LBB0_51
	jmp	.LBB0_105
.LBB0_27:                               #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	leaq	(%rax,%rax,8), %rcx
	movl	$2, 16(%rsp,%rcx,4)
	movb	$0, 20(%rsp,%rcx,4)
	movl	D_length(%rip), %ecx
	cmpl	%ecx, %r11d
	jle	.LBB0_29
# BB#28:                                #   in Loop: Header=BB0_8 Depth=1
	movl	$1, AND(%rip)
.LBB0_29:                               #   in Loop: Header=BB0_8 Depth=1
	testl	%edx, %edx
	jne	.LBB0_107
# BB#30:                                #   in Loop: Header=BB0_8 Depth=1
	cmpl	$1, REGEX(%rip)
	jne	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_8 Depth=1
	cmpl	%ecx, %r11d
	jg	.LBB0_107
.LBB0_32:                               #   in Loop: Header=BB0_8 Depth=1
	movl	Bit(,%rax,4), %eax
	orl	%eax, endposition(%rip)
	xorl	%edx, %edx
	cmpl	$32, %r11d
	jl	.LBB0_51
	jmp	.LBB0_105
.LBB0_33:                               #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	movl	Bit(,%rax,4), %ecx
	orl	%ecx, NO_ERR_MASK(%rip)
	leaq	(%rax,%rax,8), %rax
	movw	$2570, 20(%rsp,%rax,4)  # imm = 0xA0A
.LBB0_34:                               #   in Loop: Header=BB0_8 Depth=1
	movb	$0, 22(%rsp,%rax,4)
	cmpl	$32, %r11d
	jl	.LBB0_51
	jmp	.LBB0_105
.LBB0_35:                               #   in Loop: Header=BB0_8 Depth=1
	incq	%rcx
.LBB0_36:                               # %.preheader207
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	%ecx, %esi
	cmpl	%r14d, %esi
	jge	.LBB0_49
# BB#37:                                # %.preheader207
                                        #   in Loop: Header=BB0_8 Depth=1
	cmpb	$-11, %bl
	je	.LBB0_49
# BB#38:                                # %.lr.ph247.lr.ph
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%r8, %rdx
	movslq	%r11d, %r12
	xorl	%ebp, %ebp
.LBB0_39:                               # %.lr.ph247
                                        #   Parent Loop BB0_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_40 Depth 3
	leaq	(%r12,%r12,8), %rax
	leaq	16(%rsp,%rax,4), %rdi
	leaq	3(%rbp,%rdi), %rax
	movslq	%esi, %r8
	addq	$2, %r8
	movl	%esi, %ecx
	.p2align	4, 0x90
.LBB0_40:                               #   Parent Loop BB0_8 Depth=1
                                        #     Parent Loop BB0_39 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$-19, %bl
	jne	.LBB0_43
# BB#41:                                #   in Loop: Header=BB0_40 Depth=3
	movzbl	-1(%r15,%r8), %ebx
	movb	%bl, (%rax)
	addl	$2, %ecx
	cmpq	%r13, %r8
	jge	.LBB0_46
# BB#42:                                #   in Loop: Header=BB0_40 Depth=3
	movzbl	(%r15,%r8), %ebx
	addq	$2, %r8
	cmpb	$-11, %bl
	jne	.LBB0_40
	jmp	.LBB0_46
	.p2align	4, 0x90
.LBB0_43:                               # %.outer
                                        #   in Loop: Header=BB0_39 Depth=2
	movq	%rbp, %rax
	orq	$1, %rax
	movb	%bl, 4(%rax,%rdi)
	movb	%bl, 4(%rbp,%rdi)
	addq	$2, %rbp
	leal	1(%rcx), %esi
	cmpl	%r14d, %esi
	jge	.LBB0_45
# BB#44:                                # %.outer
                                        #   in Loop: Header=BB0_39 Depth=2
	movslq	%esi, %rax
	movb	(%r15,%rax), %bl
	cmpb	$-11, %bl
	jne	.LBB0_39
.LBB0_45:                               # %.critedge.loopexit347
                                        #   in Loop: Header=BB0_8 Depth=1
	incl	%ecx
.LBB0_46:                               # %.critedge
                                        #   in Loop: Header=BB0_8 Depth=1
	movl	%ecx, %esi
	movq	%rdx, %r8
	movl	12(%rsp), %edx          # 4-byte Reload
	cmpl	%r14d, %esi
	jne	.LBB0_50
	jmp	.LBB0_104
.LBB0_49:                               #   in Loop: Header=BB0_8 Depth=1
	xorl	%ebp, %ebp
	cmpl	%r14d, %esi
	je	.LBB0_104
.LBB0_50:                               #   in Loop: Header=BB0_8 Depth=1
	movslq	%r11d, %rax
	movslq	%ebp, %rcx
	leaq	(%rax,%rax,8), %rax
	leaq	16(%rsp,%rax,4), %rax
	movb	$0, 4(%rcx,%rax)
	cmpl	$32, %r11d
	jge	.LBB0_105
	.p2align	4, 0x90
.LBB0_51:                               #   in Loop: Header=BB0_8 Depth=1
	incl	%r11d
.LBB0_52:                               # %.thread
                                        #   in Loop: Header=BB0_8 Depth=1
	incl	%esi
	cmpl	%r14d, %esi
	jl	.LBB0_8
# BB#53:                                # %._crit_edge263
	testl	%r9d, %r9d
	je	.LBB0_56
.LBB0_54:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	jmp	.LBB0_106
.LBB0_55:
	movl	$1, %r11d
.LBB0_56:                               # %._crit_edge263.thread
	leal	-1(%r11), %r14d
	movl	$33, %eax
	subl	%r11d, %eax
	movl	wildmask(%rip), %edx
	movl	%eax, %ecx
	shrl	%cl, %edx
	movl	%edx, wildmask(%rip)
	movl	endposition(%rip), %edi
	shrl	%cl, %edi
	movl	%edi, endposition(%rip)
	movl	NO_ERR_MASK(%rip), %esi
	sarl	%esi
	movl	Bit+4(%rip), %ebp
	notl	%esi
	orl	%ebp, %esi
	movl	$32, %ecx
	subl	%r11d, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %esi
	movl	%esi, NO_ERR_MASK(%rip)
	testl	%eax, %eax
	movl	Init(%rip), %esi
	jle	.LBB0_72
# BB#57:                                # %.lr.ph.preheader
	movl	$34, %ecx
	subl	%r11d, %ecx
	orl	%esi, %ebp
	cmpl	$2, %ecx
	je	.LBB0_71
# BB#58:                                # %.lr.ph..lr.ph_crit_edge.lr.ph
	leaq	-2(%rcx), %r8
	cmpq	$8, %r8
	jae	.LBB0_60
# BB#59:
	movl	$2, %ebx
	jmp	.LBB0_70
.LBB0_60:                               # %min.iters.checked
	movq	%r8, %rbx
	andq	$-8, %rbx
	movq	%r8, %r9
	andq	$-8, %r9
	je	.LBB0_64
# BB#61:                                # %vector.ph
	movd	%ebp, %xmm0
	leaq	-8(%r9), %r10
	movl	%r10d, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_65
# BB#62:                                # %vector.body.prol.preheader
	negq	%rsi
	pxor	%xmm1, %xmm1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_63:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	Bit+8(,%rbp,4), %xmm2
	movdqu	Bit+24(,%rbp,4), %xmm3
	por	%xmm2, %xmm0
	por	%xmm3, %xmm1
	addq	$8, %rbp
	incq	%rsi
	jne	.LBB0_63
	jmp	.LBB0_66
.LBB0_64:
	movl	$2, %ebx
	jmp	.LBB0_70
.LBB0_65:
	xorl	%ebp, %ebp
	pxor	%xmm1, %xmm1
.LBB0_66:                               # %vector.body.prol.loopexit
	cmpq	$24, %r10
	jb	.LBB0_68
	.p2align	4, 0x90
.LBB0_67:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(,%rbp,4), %rsi
	orq	$8, %rsi
	movdqu	Bit(%rsi), %xmm2
	movdqu	Bit+16(%rsi), %xmm3
	por	%xmm0, %xmm2
	por	%xmm1, %xmm3
	leaq	8(%rbp), %rsi
	orq	$2, %rsi
	movdqu	Bit(,%rsi,4), %xmm0
	movdqu	Bit+16(,%rsi,4), %xmm1
	leaq	16(%rbp), %rsi
	orq	$2, %rsi
	movdqu	Bit(,%rsi,4), %xmm4
	movdqu	Bit+16(,%rsi,4), %xmm5
	por	%xmm0, %xmm4
	por	%xmm2, %xmm4
	por	%xmm1, %xmm5
	por	%xmm3, %xmm5
	leaq	24(%rbp), %rsi
	orq	$2, %rsi
	movdqu	Bit(,%rsi,4), %xmm0
	movdqu	Bit+16(,%rsi,4), %xmm1
	por	%xmm4, %xmm0
	por	%xmm5, %xmm1
	addq	$32, %rbp
	cmpq	%r9, %rbp
	jne	.LBB0_67
.LBB0_68:                               # %middle.block
	por	%xmm1, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	por	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	por	%xmm1, %xmm0
	movd	%xmm0, %ebp
	cmpq	%r9, %r8
	je	.LBB0_71
# BB#69:
	orq	$2, %rbx
	.p2align	4, 0x90
.LBB0_70:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	orl	Bit(,%rbx,4), %ebp
	incq	%rbx
	cmpq	%rbx, %rcx
	jne	.LBB0_70
.LBB0_71:                               # %._crit_edge243
	movl	%ebp, Init(%rip)
	movl	%ebp, %esi
.LBB0_72:
	orl	%edi, %esi
	movl	%esi, Init(%rip)
	leal	1(%rdi,%rdi), %edi
	orl	%edi, %esi
	orl	%edx, %esi
	movl	%esi, Init1(%rip)
	movl	%r14d, %ecx
	subl	D_length(%rip), %ecx
	movl	%edi, %edx
	shrl	%cl, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movl	%edx, D_endpos(%rip)
	xorl	%edi, %edx
	movl	%edx, endposition(%rip)
	movl	REGEX(%rip), %r10d
	movslq	%eax, %r15
	movl	%r11d, %esi
	leaq	58(%rsp), %r8
	leaq	57(%rsp), %r9
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_73:                               # %.preheader206
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_77 Depth 2
                                        #       Child Loop BB0_79 Depth 3
                                        #     Child Loop BB0_87 Depth 2
                                        #       Child Loop BB0_89 Depth 3
	cmpl	$2, %r11d
	jl	.LBB0_97
# BB#74:                                # %.preheader204.lr.ph
                                        #   in Loop: Header=BB0_73 Depth=1
	cmpq	$10, %rbp
	jne	.LBB0_86
# BB#75:                                # %.preheader204.lr.ph
                                        #   in Loop: Header=BB0_73 Depth=1
	testl	%r10d, %r10d
	jne	.LBB0_86
# BB#76:                                # %.preheader204.preheader
                                        #   in Loop: Header=BB0_73 Depth=1
	movq	%r8, %rdx
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_77:                               # %.preheader204
                                        #   Parent Loop BB0_73 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_79 Depth 3
	leaq	(%rbx,%rbx,8), %rcx
	movb	20(%rsp,%rcx,4), %al
	testb	%al, %al
	je	.LBB0_83
# BB#78:                                # %.lr.ph337.preheader
                                        #   in Loop: Header=BB0_77 Depth=2
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB0_79:                               # %.lr.ph337
                                        #   Parent Loop BB0_73 Depth=1
                                        #     Parent Loop BB0_77 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$10, %al
	ja	.LBB0_81
# BB#80:                                #   in Loop: Header=BB0_79 Depth=3
	cmpb	$9, -1(%rdi)
	ja	.LBB0_82
.LBB0_81:                               #   in Loop: Header=BB0_79 Depth=3
	movzbl	(%rdi), %eax
	addq	$2, %rdi
	testb	%al, %al
	jne	.LBB0_79
	jmp	.LBB0_83
	.p2align	4, 0x90
.LBB0_82:                               # %.sink.split
                                        #   in Loop: Header=BB0_77 Depth=2
	leaq	(%rbx,%r15), %rax
	movl	Bit(,%rax,4), %eax
	orl	%eax, Mask(,%rbp,4)
.LBB0_83:                               # %.loopexit205
                                        #   in Loop: Header=BB0_77 Depth=2
	cmpl	$1, 16(%rsp,%rcx,4)
	jne	.LBB0_85
# BB#84:                                #   in Loop: Header=BB0_77 Depth=2
	leaq	(%rbx,%r15), %rax
	movl	Bit(,%rax,4), %eax
	xorl	%eax, Mask(,%rbp,4)
.LBB0_85:                               #   in Loop: Header=BB0_77 Depth=2
	incq	%rbx
	addq	$36, %rdx
	cmpq	%rsi, %rbx
	jne	.LBB0_77
	jmp	.LBB0_97
	.p2align	4, 0x90
.LBB0_86:                               # %.preheader204.us.preheader
                                        #   in Loop: Header=BB0_73 Depth=1
	movq	%r9, %rdi
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_87:                               # %.preheader204.us
                                        #   Parent Loop BB0_73 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_89 Depth 3
	movq	%rdi, %rcx
	jmp	.LBB0_89
	.p2align	4, 0x90
.LBB0_88:                               #   in Loop: Header=BB0_89 Depth=3
	addq	$2, %rcx
.LBB0_89:                               #   Parent Loop BB0_73 Depth=1
                                        #     Parent Loop BB0_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	-1(%rcx), %eax
	cmpq	$238, %rax
	je	.LBB0_93
# BB#90:                                #   in Loop: Header=BB0_89 Depth=3
	testb	%al, %al
	je	.LBB0_94
# BB#91:                                #   in Loop: Header=BB0_89 Depth=3
	cmpq	%rax, %rbp
	jb	.LBB0_88
# BB#92:                                #   in Loop: Header=BB0_89 Depth=3
	movzbl	(%rcx), %eax
	cmpq	%rax, %rbp
	ja	.LBB0_88
.LBB0_93:                               # %.sink.split.us-lcssa.us.us
                                        #   in Loop: Header=BB0_87 Depth=2
	leaq	(%rbx,%r15), %rax
	movl	Bit(,%rax,4), %eax
	orl	%eax, Mask(,%rbp,4)
.LBB0_94:                               # %.loopexit205.us-lcssa.us.us
                                        #   in Loop: Header=BB0_87 Depth=2
	leaq	(%rbx,%rbx,8), %rax
	cmpl	$1, 16(%rsp,%rax,4)
	jne	.LBB0_96
# BB#95:                                #   in Loop: Header=BB0_87 Depth=2
	leaq	(%rbx,%r15), %rax
	movl	Bit(,%rax,4), %eax
	xorl	%eax, Mask(,%rbp,4)
.LBB0_96:                               #   in Loop: Header=BB0_87 Depth=2
	incq	%rbx
	addq	$36, %rdi
	cmpq	%rsi, %rbx
	jne	.LBB0_87
.LBB0_97:                               # %._crit_edge
                                        #   in Loop: Header=BB0_73 Depth=1
	incq	%rbp
	cmpq	$256, %rbp              # imm = 0x100
	jne	.LBB0_73
# BB#98:
	cmpl	$0, NOUPPER(%rip)
	je	.LBB0_103
# BB#99:                                # %.preheader
	callq	__ctype_b_loc
	movq	(%rax), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_100:                              # =>This Inner Loop Header: Depth=1
	testb	$1, 131(%rbx,%rbp,2)
	je	.LBB0_102
# BB#101:                               #   in Loop: Header=BB0_100 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movslq	260(%rax,%rbp,4), %rax
	movl	Mask(,%rax,4), %eax
	movl	%eax, Mask+260(,%rbp,4)
.LBB0_102:                              #   in Loop: Header=BB0_100 Depth=1
	incq	%rbp
	cmpq	$26, %rbp
	jne	.LBB0_100
.LBB0_103:                              # %.loopexit
	movl	%r14d, %eax
	addq	$1528, %rsp             # imm = 0x5F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_104:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	jmp	.LBB0_106
.LBB0_105:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
.LBB0_106:
	movl	$Progname, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$2, %edi
	callq	exit
.LBB0_107:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$2, %edi
	callq	exit
.Lfunc_end0:
	.size	maskgen, .Lfunc_end0-maskgen
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_10
	.quad	.LBB0_11
	.quad	.LBB0_14
	.quad	.LBB0_52
	.quad	.LBB0_52
	.quad	.LBB0_15
	.quad	.LBB0_104
	.quad	.LBB0_19
	.quad	.LBB0_20
	.quad	.LBB0_11
	.quad	.LBB0_22
	.quad	.LBB0_52
	.quad	.LBB0_24
	.quad	.LBB0_27
	.quad	.LBB0_52
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_11
	.quad	.LBB0_33

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s: illegal pattern, unmatched '<', '>'\n"
	.size	.L.str, 41

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%s: illegal pattern, unmatched '[', ']'\n"
	.size	.L.str.1, 41

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"illegal pattern \n"
	.size	.L.str.2, 18

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s: pattern too long\n"
	.size	.L.str.3, 22


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
