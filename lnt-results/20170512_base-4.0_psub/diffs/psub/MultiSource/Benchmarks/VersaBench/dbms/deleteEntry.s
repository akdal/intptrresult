	.text
	.file	"deleteEntry.bc"
	.globl	deleteEntry
	.p2align	4, 0x90
	.type	deleteEntry,@function
deleteEntry:                            # @deleteEntry
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	movb	$0, (%r15)
	leaq	8(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	$0, (%r13)
	movq	8(%r13), %rbx
	jle	.LBB0_1
# BB#18:                                # %.preheader
	testq	%rbx, %rbx
	je	.LBB0_32
# BB#19:                                # %.lr.ph.preheader
	xorl	%r14d, %r14d
	jmp	.LBB0_20
.LBB0_22:                               #   in Loop: Header=BB0_20 Depth=1
	movq	40(%rbx), %rbp
	movq	(%r13), %rsi
	movq	%rbx, %rdi
	callq	deleteIndexEntry
	movb	$1, (%r15)
	leaq	40(%r14), %rax
	testq	%r14, %r14
	cmoveq	16(%rsp), %rax          # 8-byte Folded Reload
	movq	%rbp, (%rax)
	movq	%r14, %rbx
	jmp	.LBB0_26
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	8(%rbx), %rbp
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	consistentKey
	cmpb	$1, %al
	jne	.LBB0_25
# BB#21:                                #   in Loop: Header=BB0_20 Depth=1
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	15(%rsp), %rcx
	callq	deleteEntry
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_22
# BB#23:                                #   in Loop: Header=BB0_20 Depth=1
	cmpb	$1, 15(%rsp)
	jne	.LBB0_25
# BB#24:                                #   in Loop: Header=BB0_20 Depth=1
	movq	%rbp, %rsi
	callq	keysUnion
	movb	$1, (%r15)
	.p2align	4, 0x90
.LBB0_25:                               #   in Loop: Header=BB0_20 Depth=1
	movq	40(%rbx), %rbp
.LBB0_26:                               # %.backedge
                                        #   in Loop: Header=BB0_20 Depth=1
	movq	%rbx, %r14
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	jne	.LBB0_20
	jmp	.LBB0_32
.LBB0_1:                                # %.preheader84
	testq	%rbx, %rbx
	je	.LBB0_32
# BB#2:                                 # %.lr.ph95.lr.ph
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB0_27
# BB#3:                                 # %.lr.ph95.us.preheader
	xorl	%r13d, %r13d
	movq	%r15, 32(%rsp)          # 8-byte Spill
.LBB0_4:                                # %.lr.ph95.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
                                        #       Child Loop BB0_10 Depth 3
	testq	%r13, %r13
	leaq	40(%r13), %r14
	cmoveq	16(%rsp), %r14          # 8-byte Folded Reload
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB0_5:                                #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_10 Depth 3
	leaq	8(%r13), %rdi
	movq	%r12, %rsi
	callq	consistentKey
	cmpb	$1, %al
	jne	.LBB0_16
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=2
	movq	(%r13), %rbx
	movl	(%rbx), %eax
	decl	%eax
	cmpl	$2, %eax
	ja	.LBB0_7
# BB#8:                                 # %switch.lookup.us.us
                                        #   in Loop: Header=BB0_5 Depth=2
	cltq
	movq	.Lswitch.table(,%rax,8), %rbp
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_5 Depth=2
	xorl	%ebp, %ebp
.LBB0_9:                                # %.lr.ph93.us.us
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	24(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_10:                               #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15), %rcx
	movb	$1, %al
	cmpq	%rbp, %rcx
	jge	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_10 Depth=3
	movq	8(%rbx), %rax
	movq	(%rax,%rcx,8), %rdi
	movq	8(%r15), %rsi
	callq	consistentNonKey
.LBB0_12:                               #   in Loop: Header=BB0_10 Depth=3
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.LBB0_13
# BB#15:                                #   in Loop: Header=BB0_10 Depth=3
	cmpb	$1, %al
	je	.LBB0_10
.LBB0_13:                               # %..critedge_crit_edge.us.us
                                        #   in Loop: Header=BB0_5 Depth=2
	testb	%al, %al
	movq	40(%r13), %rbx
	je	.LBB0_17
# BB#14:                                #   in Loop: Header=BB0_5 Depth=2
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	deleteIndexEntry
	movq	32(%rsp), %rax          # 8-byte Reload
	movb	$1, (%rax)
	movq	%rbx, (%r14)
	testq	%rbx, %rbx
	movq	%rbx, %r13
	jne	.LBB0_5
	jmp	.LBB0_32
.LBB0_16:                               # %.us-lcssa.us.us
                                        #   in Loop: Header=BB0_4 Depth=1
	movq	40(%r13), %rbx
.LBB0_17:                               # %.outer.backedge.us
                                        #   in Loop: Header=BB0_4 Depth=1
	testq	%rbx, %rbx
	jne	.LBB0_4
	jmp	.LBB0_32
.LBB0_27:                               # %.lr.ph95.preheader
	xorl	%ebp, %ebp
.LBB0_28:                               # %.lr.ph95
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_29 Depth 2
	testq	%rbp, %rbp
	leaq	40(%rbp), %r14
	cmoveq	16(%rsp), %r14          # 8-byte Folded Reload
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB0_29:                               #   Parent Loop BB0_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	8(%rbp), %rdi
	movq	%r12, %rsi
	callq	consistentKey
	cmpb	$1, %al
	movq	40(%rbp), %rbx
	jne	.LBB0_31
# BB#30:                                #   in Loop: Header=BB0_29 Depth=2
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	deleteIndexEntry
	movb	$1, (%r15)
	movq	%rbx, (%r14)
	testq	%rbx, %rbx
	movq	%rbx, %rbp
	jne	.LBB0_29
	jmp	.LBB0_32
.LBB0_31:                               # %.outer.backedge
                                        #   in Loop: Header=BB0_28 Depth=1
	testq	%rbx, %rbx
	jne	.LBB0_28
.LBB0_32:                               # %.loopexit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	deleteEntry, .Lfunc_end0-deleteEntry
	.cfi_endproc

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	18                      # 0x12
	.quad	25                      # 0x19
	.quad	51                      # 0x33
	.size	.Lswitch.table, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
