	.text
	.file	"Triang.bc"
	.globl	norm
	.p2align	4, 0x90
	.type	norm,@function
norm:                                   # @norm
	.cfi_startproc
# BB#0:
	xorpd	%xmm0, %xmm0
	cmpl	%ecx, %edx
	jg	.LBB0_3
# BB#1:                                 # %.lr.ph
	movslq	%esi, %rax
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	decq	%rdx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rdi,%rdx,8), %rsi
	movsd	(%rsi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm0
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.LBB0_5
# BB#4:                                 # %._crit_edge.split
	movapd	%xmm1, %xmm0
	retq
.LBB0_5:                                # %call.sqrt
	jmp	sqrt                    # TAILCALL
.Lfunc_end0:
	.size	norm, .Lfunc_end0-norm
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	House
	.p2align	4, 0x90
	.type	House,@function
House:                                  # @House
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebx
	movl	%ecx, %r13d
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpl	%ebx, %r13d
	jle	.LBB1_2
# BB#1:                                 # %.norm.exit_crit_edge
	movslq	%r13d, %r12
	movslq	%edx, %rbp
	xorpd	%xmm0, %xmm0
	jmp	.LBB1_4
.LBB1_2:                                # %.lr.ph.i
	movslq	%edx, %rbp
	movslq	%r13d, %r12
	movslq	%ebx, %rax
	leaq	-1(%r12), %rcx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r15,%rcx,8), %rdx
	movsd	(%rdx,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm0
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB1_3
.LBB1_4:                                # %norm.exit
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB1_6
# BB#5:                                 # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB1_6:                                # %norm.exit.split
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movq	(%r15,%r12,8), %rax
	movsd	(%rax,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	sign
	cmpl	%ebx, %r13d
	jge	.LBB1_13
# BB#7:                                 # %.lr.ph.preheader
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	movslq	%ebx, %rax
	subl	%r12d, %ebx
	leaq	-1(%rax), %rcx
	subq	%r12, %rcx
	andq	$3, %rbx
	movq	%r12, %rdx
	je	.LBB1_10
# BB#8:                                 # %.lr.ph.prol.preheader
	negq	%rbx
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15,%rdx,8), %rsi
	movsd	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%r14,%rdx,8)
	incq	%rdx
	incq	%rbx
	jne	.LBB1_9
.LBB1_10:                               # %.lr.ph.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB1_13
# BB#11:                                # %.lr.ph.preheader.new
	subq	%rdx, %rax
	leaq	32(%r14,%rdx,8), %rcx
	leaq	32(%r15,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	movsd	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -24(%rcx)
	movq	-16(%rdx), %rsi
	movsd	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -16(%rcx)
	movq	-8(%rdx), %rsi
	movsd	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%rcx)
	movq	(%rdx), %rsi
	movsd	(%rsi,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rcx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB1_12
.LBB1_13:                               # %._crit_edge
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, (%r14,%r12,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	House, .Lfunc_end1-House
	.cfi_endproc

	.globl	xty
	.p2align	4, 0x90
	.type	xty,@function
xty:                                    # @xty
	.cfi_startproc
# BB#0:
	xorpd	%xmm0, %xmm0
	cmpl	%ecx, %edx
	jg	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	movslq	%edx, %rax
	movslq	%ecx, %rcx
	decq	%rax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%rdi,%rax,8), %xmm1   # xmm1 = mem[0],zero
	mulsd	8(%rsi,%rax,8), %xmm1
	addsd	%xmm1, %xmm0
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB2_2
.LBB2_3:                                # %._crit_edge
	retq
.Lfunc_end2:
	.size	xty, .Lfunc_end2-xty
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4607182418800017408     # double 1
.LCPI3_1:
	.quad	0                       # double 0
	.text
	.globl	Trianglelise
	.p2align	4, 0x90
	.type	Trianglelise,@function
Trianglelise:                           # @Trianglelise
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 272
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r12
	callq	newMatrix
	callq	newIdMatrix
	movq	%rax, %r15
	movl	$408, %edi              # imm = 0x198
	callq	malloc
	movq	%rax, %rbx
	movl	$408, %edi              # imm = 0x198
	callq	malloc
	movq	%rax, %rbp
	movl	$408, %edi              # imm = 0x198
	callq	malloc
	cmpl	$2, %r14d
	jge	.LBB3_2
# BB#1:
	movq	%r12, %rax
	jmp	.LBB3_115
.LBB3_2:                                # %.preheader186
	leal	-1(%r14), %ecx
	movl	%ecx, 100(%rsp)         # 4-byte Spill
	movl	%r14d, %ecx
	notl	%ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	leaq	32(%r12), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	leaq	16(%rax), %rcx
	movq	%rax, 56(%rsp)          # 8-byte Spill
	addq	$56, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	addq	$56, %rbp
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	leaq	32(%rbx), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%r15, %rdx
	leaq	56(%rbx), %r15
	leaq	8(%rbx), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movb	$2, 11(%rsp)            # 1-byte Folded Spill
	movq	$-1, 72(%rsp)           # 8-byte Folded Spill
	movl	$2, %r8d
	xorl	%r13d, %r13d
	movl	$1, %eax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	$8, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$7, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movl	$1, %r9d
	movq	%r14, %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movl	%r14d, %r10d
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
                                        #     Child Loop BB3_11 Depth 2
                                        #     Child Loop BB3_17 Depth 2
                                        #     Child Loop BB3_20 Depth 2
                                        #     Child Loop BB3_26 Depth 2
                                        #     Child Loop BB3_30 Depth 2
                                        #     Child Loop BB3_34 Depth 2
                                        #       Child Loop BB3_50 Depth 3
                                        #       Child Loop BB3_53 Depth 3
                                        #     Child Loop BB3_41 Depth 2
                                        #     Child Loop BB3_44 Depth 2
                                        #     Child Loop BB3_48 Depth 2
                                        #     Child Loop BB3_59 Depth 2
                                        #     Child Loop BB3_62 Depth 2
                                        #     Child Loop BB3_87 Depth 2
                                        #     Child Loop BB3_66 Depth 2
                                        #     Child Loop BB3_69 Depth 2
                                        #       Child Loop BB3_74 Depth 3
                                        #     Child Loop BB3_90 Depth 2
                                        #     Child Loop BB3_79 Depth 2
                                        #       Child Loop BB3_99 Depth 3
                                        #       Child Loop BB3_103 Depth 3
                                        #     Child Loop BB3_94 Depth 2
                                        #       Child Loop BB3_108 Depth 3
                                        #       Child Loop BB3_111 Depth 3
	movq	%r13, %r14
	xorq	$3, %r14
	movq	$-2, %rax
	subq	%r13, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leal	(%r13,%r10), %ecx
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	leal	(%rcx,%rax), %eax
	cmpl	$51, %eax
	movl	$50, %ecx
	cmovgel	%ecx, %eax
	leaq	1(%r13), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	%eax, %rbp
	cmpq	%rbp, %r13
	xorpd	%xmm0, %xmm0
	jge	.LBB3_12
# BB#4:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	leaq	1(%rbp), %rcx
	addl	%r14d, %ecx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax,%rbp), %rax
	testb	$3, %cl
	je	.LBB3_5
# BB#6:                                 # %.lr.ph.i.i.prol.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	12(%rsp), %edx          # 4-byte Reload
	subl	%r10d, %edx
	cmpl	$-52, %edx
	movl	$-51, %ecx
	cmovlel	%ecx, %edx
	movl	%r14d, %ecx
	subl	%edx, %ecx
	andl	$3, %ecx
	negq	%rcx
	xorpd	%xmm0, %xmm0
	movq	%r12, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph.i.i.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi,%r9,8), %rdi
	movsd	(%rdi,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm0
	decq	%rdx
	addq	$8, %rsi
	cmpq	%rdx, %rcx
	jne	.LBB3_7
# BB#8:                                 # %.lr.ph.i.i.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r9, %rcx
	subq	%rdx, %rcx
	cmpq	$3, %rax
	jae	.LBB3_10
	jmp	.LBB3_12
.LBB3_5:                                #   in Loop: Header=BB3_3 Depth=1
	xorpd	%xmm0, %xmm0
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpq	$3, %rax
	jb	.LBB3_12
.LBB3_10:                               # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB3_3 Depth=1
	decq	%rcx
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph.i.i
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r12,%rcx,8), %rax
	movsd	(%rax,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movq	16(%r12,%rcx,8), %rax
	movsd	(%rax,%r13,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movq	24(%r12,%rcx,8), %rax
	movsd	(%rax,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movq	32(%r12,%rcx,8), %rax
	movsd	(%rax,%r13,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	addq	$4, %rcx
	cmpq	%rbp, %rcx
	jl	.LBB3_11
.LBB3_12:                               # %norm.exit.i
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%r8, 136(%rsp)          # 8-byte Spill
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB3_14
# BB#13:                                # %call.sqrt
                                        #   in Loop: Header=BB3_3 Depth=1
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB3_14:                               # %norm.exit.i.split
                                        #   in Loop: Header=BB3_3 Depth=1
	movsd	%xmm1, 200(%rsp)        # 8-byte Spill
	movq	$-3, %rax
	subq	%r13, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	8(%r12,%r13,8), %rax
	movsd	(%rax,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, 192(%rsp)        # 8-byte Spill
	callq	sign
	cmpq	%rbp, 32(%rsp)          # 8-byte Folded Reload
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	136(%rsp), %r8          # 8-byte Reload
	movsd	.LCPI3_0(%rip), %xmm2   # xmm2 = mem[0],zero
	jge	.LBB3_21
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	200(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	192(%rsp), %xmm3        # 8-byte Reload
                                        # xmm3 = mem[0],zero
	addsd	%xmm1, %xmm3
	movapd	%xmm2, %xmm0
	divsd	%xmm3, %xmm0
	leaq	1(%rbp), %rcx
	addl	40(%rsp), %ecx          # 4-byte Folded Reload
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax,%rbp), %rax
	testb	$3, %cl
	movq	32(%rsp), %rdx          # 8-byte Reload
	je	.LBB3_18
# BB#16:                                # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	12(%rsp), %ecx          # 4-byte Reload
	subl	24(%rsp), %ecx          # 4-byte Folded Reload
	cmpl	$-52, %ecx
	movl	$-51, %edx
	cmovlel	%edx, %ecx
	negl	%ecx
	addb	11(%rsp), %cl           # 1-byte Folded Reload
	movzbl	%cl, %ecx
	andl	$3, %ecx
	negq	%rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_17:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r12,%rdx,8), %rsi
	movsd	(%rsi,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rbx,%rdx,8)
	incq	%rdx
	incq	%rcx
	jne	.LBB3_17
.LBB3_18:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$3, %rax
	jb	.LBB3_21
# BB#19:                                # %.lr.ph.i.preheader.new
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$-52, %eax
	movl	$-51, %ecx
	cmovlel	%ecx, %eax
	negl	%eax
	cltq
	decq	%rax
	subq	%rdx, %rax
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	176(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB3_20:                               # %.lr.ph.i
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rdx), %rsi
	movsd	(%rsi,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -24(%rcx)
	movq	-16(%rdx), %rsi
	movsd	(%rsi,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -16(%rcx)
	movq	-8(%rdx), %rsi
	movsd	(%rsi,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, -8(%rcx)
	movq	(%rdx), %rsi
	movsd	(%rsi,%r13,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rcx)
	addq	$32, %rcx
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB3_20
.LBB3_21:                               # %House.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	$1, %eax
	cmpq	%rbp, %r13
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
	movq	%rcx, 8(%rbx,%r13,8)
	xorpd	%xmm1, %xmm1
	jge	.LBB3_31
# BB#22:                                # %.lr.ph.i160.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rbp, 32(%rsp)          # 8-byte Folded Reload
	movapd	%xmm2, %xmm1
	jge	.LBB3_31
# BB#23:                                # %.lr.ph.i160..lr.ph.i160_crit_edge.lr.ph
                                        #   in Loop: Header=BB3_3 Depth=1
	leaq	1(%rbp), %rdx
	addl	40(%rsp), %edx          # 4-byte Folded Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx,%rbp), %rcx
	testb	$3, %dl
	je	.LBB3_24
# BB#25:                                # %.lr.ph.i160..lr.ph.i160_crit_edge.prol.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	12(%rsp), %edx          # 4-byte Reload
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	cmpl	$-52, %edx
	movl	$-51, %esi
	cmovlel	%esi, %edx
	negl	%edx
	addb	11(%rsp), %dl           # 1-byte Folded Reload
	movzbl	%dl, %edx
	andl	$3, %edx
	negq	%rdx
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movapd	%xmm2, %xmm1
	.p2align	4, 0x90
.LBB3_26:                               # %.lr.ph.i160..lr.ph.i160_crit_edge.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi,%r8,8), %xmm0     # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm0, %xmm1
	decq	%rsi
	addq	$8, %rdi
	cmpq	%rsi, %rdx
	jne	.LBB3_26
# BB#27:                                # %.lr.ph.i160..lr.ph.i160_crit_edge.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r8, %rdx
	subq	%rsi, %rdx
	cmpq	$3, %rcx
	jae	.LBB3_29
	jmp	.LBB3_31
.LBB3_24:                               #   in Loop: Header=BB3_3 Depth=1
	leaq	2(%r13), %rdx
	movapd	%xmm2, %xmm1
	cmpq	$3, %rcx
	jb	.LBB3_31
.LBB3_29:                               # %.lr.ph.i160..lr.ph.i160_crit_edge.lr.ph.new
                                        #   in Loop: Header=BB3_3 Depth=1
	decq	%rdx
	.p2align	4, 0x90
.LBB3_30:                               # %.lr.ph.i160..lr.ph.i160_crit_edge
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	8(%rbx,%rdx,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movsd	16(%rbx,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movsd	24(%rbx,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movsd	32(%rbx,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	addq	$4, %rdx
	cmpq	%rbp, %rdx
	jl	.LBB3_30
	.p2align	4, 0x90
.LBB3_31:                               # %xty.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	subq	%r13, %rax
	movapd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	cmpq	%rbp, %r13
	jg	.LBB3_55
# BB#32:                                # %.lr.ph
                                        #   in Loop: Header=BB3_3 Depth=1
	jl	.LBB3_33
# BB#36:                                # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movapd	%xmm0, %xmm1
	mulsd	.LCPI3_1, %xmm1
	cmpq	%rbp, %r13
	movq	%rbp, %rcx
	cmovgeq	%r13, %rcx
	addq	%rax, %rcx
	cmpq	$4, %rcx
	movq	%r13, %rsi
	jb	.LBB3_47
# BB#37:                                # %min.iters.checked329
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%rcx, %r9
	andq	$-4, %r9
	movq	%rcx, %rdx
	andq	$-4, %rdx
	movq	%r13, %rsi
	je	.LBB3_47
# BB#38:                                # %vector.ph333
                                        #   in Loop: Header=BB3_3 Depth=1
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	leaq	-4(%rdx), %r8
	movl	%r8d, %eax
	shrl	$2, %eax
	incl	%eax
	testb	$3, %al
	je	.LBB3_39
# BB#40:                                # %vector.body325.prol.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$-52, %eax
	movl	$-51, %esi
	cmovlel	%esi, %eax
	negl	%eax
	cltq
	decq	%rax
	cmpq	%rax, %r13
	movl	%r13d, %esi
	cmovlel	%eax, %esi
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rsi), %esi
	andl	$12, %esi
	addl	$-4, %esi
	shrl	$2, %esi
	incl	%esi
	andl	$3, %esi
	negq	%rsi
	movq	112(%rsp), %rax         # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_41:                               # %vector.body325.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm2, -16(%rax)
	movups	%xmm2, (%rax)
	addq	$4, %rdi
	addq	$32, %rax
	incq	%rsi
	jne	.LBB3_41
	jmp	.LBB3_42
	.p2align	4, 0x90
.LBB3_33:                               # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	leaq	1(%rbp), %rax
	addl	%r14d, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx,%rbp), %r9
	andl	$3, %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	subl	24(%rsp), %ecx          # 4-byte Folded Reload
	cmpl	$-52, %ecx
	movl	$-51, %edx
	cmovlel	%edx, %ecx
	movl	%r14d, %r8d
	subl	%ecx, %r8d
	andl	$3, %r8d
	negq	%r8
	movq	%r13, %rsi
	.p2align	4, 0x90
.LBB3_34:                               # %.lr.ph.split
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_50 Depth 3
                                        #       Child Loop BB3_53 Depth 3
	testq	%rax, %rax
	movq	(%r12,%rsi,8), %rdi
	xorpd	%xmm1, %xmm1
	je	.LBB3_35
# BB#49:                                # %.lr.ph.i165.prol.preheader
                                        #   in Loop: Header=BB3_34 Depth=2
	movq	%r8, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_50:                               # %.lr.ph.i165.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdi,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	(%rbx,%rdx,8), %xmm2
	addsd	%xmm2, %xmm1
	incq	%rdx
	incq	%rcx
	jne	.LBB3_50
	jmp	.LBB3_51
	.p2align	4, 0x90
.LBB3_35:                               #   in Loop: Header=BB3_34 Depth=2
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB3_51:                               # %.lr.ph.i165.prol.loopexit
                                        #   in Loop: Header=BB3_34 Depth=2
	cmpq	$3, %r9
	jb	.LBB3_54
# BB#52:                                # %.lr.ph.split.new
                                        #   in Loop: Header=BB3_34 Depth=2
	decq	%rdx
	.p2align	4, 0x90
.LBB3_53:                               # %.lr.ph.i165
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	8(%rdi,%rdx,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	8(%rbx,%rdx,8), %xmm2
	addsd	%xmm1, %xmm2
	movsd	16(%rdi,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	16(%rbx,%rdx,8), %xmm1
	addsd	%xmm2, %xmm1
	movsd	24(%rdi,%rdx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	24(%rbx,%rdx,8), %xmm2
	addsd	%xmm1, %xmm2
	movsd	32(%rdi,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	32(%rbx,%rdx,8), %xmm1
	addsd	%xmm2, %xmm1
	addq	$4, %rdx
	cmpq	%rbp, %rdx
	jl	.LBB3_53
.LBB3_54:                               # %xty.exit167
                                        #   in Loop: Header=BB3_34 Depth=2
	addsd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%r10,%rsi,8)
	cmpq	%rbp, %rsi
	leaq	1(%rsi), %rsi
	jl	.LBB3_34
	jmp	.LBB3_55
.LBB3_39:                               #   in Loop: Header=BB3_3 Depth=1
	xorl	%edi, %edi
.LBB3_42:                               # %vector.body325.prol.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$12, %r8
	jb	.LBB3_45
# BB#43:                                # %vector.ph333.new
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$-52, %eax
	movl	$-51, %esi
	cmovlel	%esi, %eax
	negl	%eax
	movslq	%eax, %rsi
	decq	%rsi
	cmpq	%rsi, %r13
	cmovgeq	%r13, %rsi
	addq	80(%rsp), %rsi          # 8-byte Folded Reload
	andq	$-4, %rsi
	subq	%rdi, %rsi
	addq	104(%rsp), %rdi         # 8-byte Folded Reload
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB3_44:                               # %vector.body325
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm2, -80(%rdi)
	movups	%xmm2, -64(%rdi)
	movups	%xmm2, -48(%rdi)
	movups	%xmm2, -32(%rdi)
	movups	%xmm2, -16(%rdi)
	movups	%xmm2, (%rdi)
	movups	%xmm2, 16(%rdi)
	movups	%xmm2, 32(%rdi)
	subq	$-128, %rdi
	addq	$-16, %rsi
	jne	.LBB3_44
.LBB3_45:                               # %middle.block326
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rdx, %rcx
	je	.LBB3_55
# BB#46:                                #   in Loop: Header=BB3_3 Depth=1
	addq	%r13, %r9
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB3_47:                               # %.lr.ph.split.us.preheader349
                                        #   in Loop: Header=BB3_3 Depth=1
	decq	%rsi
	.p2align	4, 0x90
.LBB3_48:                               # %.lr.ph.split.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, 8(%r10,%rsi,8)
	incq	%rsi
	cmpq	%rbp, %rsi
	jl	.LBB3_48
.LBB3_55:                               # %._crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	xorpd	%xmm1, %xmm1
	cmpq	%rbp, %r13
	jge	.LBB3_63
# BB#56:                                # %.lr.ph.i179.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	leaq	1(%rbp), %rax
	addl	%r14d, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx,%rbp), %rcx
	testb	$3, %al
	je	.LBB3_57
# BB#58:                                # %.lr.ph.i179.prol.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$-52, %eax
	movl	$-51, %edx
	cmovlel	%edx, %eax
	movl	%r14d, %edx
	subl	%eax, %edx
	andl	$3, %edx
	negq	%rdx
	xorpd	%xmm1, %xmm1
	movq	32(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_59:                               # %.lr.ph.i179.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r10,%rax,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	(%rbx,%rax,8), %xmm2
	addsd	%xmm2, %xmm1
	incq	%rax
	incq	%rdx
	jne	.LBB3_59
	jmp	.LBB3_60
.LBB3_57:                               #   in Loop: Header=BB3_3 Depth=1
	xorpd	%xmm1, %xmm1
	movq	32(%rsp), %rax          # 8-byte Reload
.LBB3_60:                               # %.lr.ph.i179.prol.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$3, %rcx
	jb	.LBB3_63
# BB#61:                                # %.lr.ph.i179.preheader.new
                                        #   in Loop: Header=BB3_3 Depth=1
	decq	%rax
	.p2align	4, 0x90
.LBB3_62:                               # %.lr.ph.i179
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	8(%r10,%rax,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	8(%rbx,%rax,8), %xmm2
	addsd	%xmm1, %xmm2
	movsd	16(%r10,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	16(%rbx,%rax,8), %xmm1
	addsd	%xmm2, %xmm1
	movsd	24(%r10,%rax,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	24(%rbx,%rax,8), %xmm2
	addsd	%xmm1, %xmm2
	movsd	32(%r10,%rax,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	32(%rbx,%rax,8), %xmm1
	addsd	%xmm2, %xmm1
	addq	$4, %rax
	cmpq	%rbp, %rax
	jl	.LBB3_62
.LBB3_63:                               # %xty.exit181
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r13, %r8
	notq	%r8
	cmpq	%rbp, %r13
	jge	.LBB3_76
# BB#64:                                # %.lr.ph193.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	mulsd	%xmm0, %xmm1
	leaq	1(%rbp,%r8), %r11
	cmpq	$3, %r11
	movq	16(%rsp), %rax          # 8-byte Reload
	jbe	.LBB3_65
# BB#81:                                # %min.iters.checked304
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r11, %r9
	andq	$-4, %r9
	movq	%r11, %r10
	andq	$-4, %r10
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB3_65
# BB#82:                                # %vector.ph308
                                        #   in Loop: Header=BB3_3 Depth=1
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	leaq	-4(%r10), %rcx
	movq	%rcx, %rax
	shrq	$2, %rax
	btl	$2, %ecx
	jb	.LBB3_83
# BB#84:                                # %vector.body300.prol
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movupd	(%rcx,%rdx,8), %xmm3
	movupd	16(%rcx,%rdx,8), %xmm4
	movupd	(%rbx,%rdx,8), %xmm5
	movupd	16(%rbx,%rdx,8), %xmm6
	mulpd	%xmm2, %xmm5
	mulpd	%xmm2, %xmm6
	subpd	%xmm5, %xmm3
	subpd	%xmm6, %xmm4
	movq	48(%rsp), %rcx          # 8-byte Reload
	movupd	%xmm3, (%rcx,%rdx,8)
	movupd	%xmm4, 16(%rcx,%rdx,8)
	movl	$4, %ecx
	testq	%rax, %rax
	jne	.LBB3_86
	jmp	.LBB3_88
.LBB3_83:                               #   in Loop: Header=BB3_3 Depth=1
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.LBB3_88
.LBB3_86:                               # %vector.ph308.new
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$-52, %eax
	movl	$-51, %edx
	cmovlel	%edx, %eax
	negl	%eax
	cltq
	addq	72(%rsp), %rax          # 8-byte Folded Reload
	andq	$-4, %rax
	subq	%rcx, %rax
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdi
	leaq	(%r15,%rcx,8), %rdx
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_87:                               # %vector.body300
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rdi,%rcx,8), %xmm3
	movupd	-32(%rdi,%rcx,8), %xmm4
	movupd	-48(%rdx,%rcx,8), %xmm5
	movupd	-32(%rdx,%rcx,8), %xmm6
	mulpd	%xmm2, %xmm5
	mulpd	%xmm2, %xmm6
	subpd	%xmm5, %xmm3
	subpd	%xmm6, %xmm4
	movupd	%xmm3, -48(%rsi,%rcx,8)
	movupd	%xmm4, -32(%rsi,%rcx,8)
	movupd	-16(%rdi,%rcx,8), %xmm3
	movupd	(%rdi,%rcx,8), %xmm4
	movupd	-16(%rdx,%rcx,8), %xmm5
	movupd	(%rdx,%rcx,8), %xmm6
	mulpd	%xmm2, %xmm5
	mulpd	%xmm2, %xmm6
	subpd	%xmm5, %xmm3
	subpd	%xmm6, %xmm4
	movupd	%xmm3, -16(%rsi,%rcx,8)
	movupd	%xmm4, (%rsi,%rcx,8)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB3_87
.LBB3_88:                               # %middle.block301
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%r10, %r11
	movq	48(%rsp), %r11          # 8-byte Reload
	je	.LBB3_67
# BB#89:                                #   in Loop: Header=BB3_3 Depth=1
	addq	16(%rsp), %r9           # 8-byte Folded Reload
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB3_65:                               # %.lr.ph193.preheader348
                                        #   in Loop: Header=BB3_3 Depth=1
	decq	%rax
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_66:                               # %.lr.ph193
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	8(%rcx,%rax,8), %xmm2   # xmm2 = mem[0],zero
	movsd	8(%rbx,%rax,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm2
	movsd	%xmm2, 8(%r11,%rax,8)
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB3_66
.LBB3_67:                               # %.preheader185
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rbp, %r13
	jge	.LBB3_76
# BB#68:                                # %.preheader182.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax,%rbp), %r10
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax,%rbp), %r9
	xorl	%esi, %esi
	movq	16(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_69:                               # %.preheader182
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_74 Depth 3
	cmpq	%rbp, %rdx
	jg	.LBB3_75
# BB#70:                                # %.lr.ph195
                                        #   in Loop: Header=BB3_69 Depth=2
	movsd	(%rbx,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movsd	(%r11,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	movq	(%r12,%rdx,8), %rax
	movapd	%xmm1, %xmm3
	mulsd	%xmm2, %xmm3
	addsd	%xmm3, %xmm3
	movsd	(%rax,%rdx,8), %xmm4    # xmm4 = mem[0],zero
	subsd	%xmm3, %xmm4
	movsd	%xmm4, (%rax,%rdx,8)
	cmpq	%rbp, %rdx
	jge	.LBB3_75
# BB#71:                                # %._crit_edge257.preheader
                                        #   in Loop: Header=BB3_69 Depth=2
	movq	%r10, %rdi
	subq	%rsi, %rdi
	testb	$1, %dil
	movq	%rdx, %rdi
	je	.LBB3_73
# BB#72:                                # %._crit_edge257.prol
                                        #   in Loop: Header=BB3_69 Depth=2
	leaq	1(%rdx), %rdi
	movq	8(%r12,%rdx,8), %rcx
	movsd	8(%r11,%rdx,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	movsd	8(%rbx,%rdx,8), %xmm4   # xmm4 = mem[0],zero
	mulsd	%xmm2, %xmm4
	addsd	%xmm3, %xmm4
	movsd	8(%rax,%rdx,8), %xmm3   # xmm3 = mem[0],zero
	subsd	%xmm4, %xmm3
	movsd	%xmm3, 8(%rax,%rdx,8)
	movsd	%xmm3, (%rcx,%rdx,8)
.LBB3_73:                               # %._crit_edge257.prol.loopexit
                                        #   in Loop: Header=BB3_69 Depth=2
	cmpq	%rsi, %r9
	je	.LBB3_75
	.p2align	4, 0x90
.LBB3_74:                               # %._crit_edge257
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_69 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%r12,%rdi,8), %rcx
	movsd	8(%r11,%rdi,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	movsd	8(%rbx,%rdi,8), %xmm4   # xmm4 = mem[0],zero
	mulsd	%xmm2, %xmm4
	addsd	%xmm3, %xmm4
	movsd	8(%rax,%rdi,8), %xmm3   # xmm3 = mem[0],zero
	subsd	%xmm4, %xmm3
	movsd	%xmm3, 8(%rax,%rdi,8)
	movsd	%xmm3, (%rcx,%rdx,8)
	movq	16(%r12,%rdi,8), %rcx
	movsd	16(%r11,%rdi,8), %xmm3  # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	movsd	16(%rbx,%rdi,8), %xmm4  # xmm4 = mem[0],zero
	mulsd	%xmm2, %xmm4
	addsd	%xmm3, %xmm4
	movsd	16(%rax,%rdi,8), %xmm3  # xmm3 = mem[0],zero
	subsd	%xmm4, %xmm3
	movsd	%xmm3, 16(%rax,%rdi,8)
	leaq	2(%rdi), %rdi
	movsd	%xmm3, (%rcx,%rdx,8)
	cmpq	%rbp, %rdi
	jl	.LBB3_74
	.p2align	4, 0x90
.LBB3_75:                               # %._crit_edge196
                                        #   in Loop: Header=BB3_69 Depth=2
	incq	%rsi
	cmpq	%rbp, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB3_69
.LBB3_76:                               # %._crit_edge198
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	(%r12,%r13,8), %rax
	movsd	8(%rax,%r13,8), %xmm1   # xmm1 = mem[0],zero
	movq	56(%rsp), %rcx          # 8-byte Reload
	subsd	(%rcx,%r13,8), %xmm1
	movsd	%xmm1, 8(%rax,%r13,8)
	movq	8(%r12,%r13,8), %rcx
	movsd	%xmm1, (%rcx,%r13,8)
	leaq	2(%r13), %rcx
	cmpq	%rbp, %rcx
	movq	16(%rsp), %rcx          # 8-byte Reload
	jg	.LBB3_77
	.p2align	4, 0x90
.LBB3_90:                               # %.lr.ph201
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r12,%rcx,8), %rdx
	movq	$0, (%rdx,%r13,8)
	movq	$0, 8(%rax,%rcx,8)
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB3_90
.LBB3_77:                               # %.preheader184
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rbp, %r13
	addsd	%xmm0, %xmm0
	jl	.LBB3_78
# BB#91:                                # %.preheader184.split.us
                                        #   in Loop: Header=BB3_3 Depth=1
	mulsd	.LCPI3_1, %xmm0
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movups	%xmm1, (%r11)
	movups	%xmm1, 16(%r11)
	movups	%xmm1, 32(%r11)
	movups	%xmm1, 48(%r11)
	movups	%xmm1, 64(%r11)
	movups	%xmm1, 80(%r11)
	movups	%xmm1, 96(%r11)
	movups	%xmm1, 112(%r11)
	movups	%xmm1, 128(%r11)
	movups	%xmm1, 144(%r11)
	movups	%xmm1, 160(%r11)
	movups	%xmm1, 176(%r11)
	movups	%xmm1, 192(%r11)
	movups	%xmm1, 208(%r11)
	movups	%xmm1, 224(%r11)
	movups	%xmm1, 240(%r11)
	movups	%xmm1, 256(%r11)
	movups	%xmm1, 272(%r11)
	movups	%xmm1, 288(%r11)
	movups	%xmm1, 304(%r11)
	movups	%xmm1, 320(%r11)
	movups	%xmm1, 336(%r11)
	movups	%xmm1, 352(%r11)
	movups	%xmm1, 368(%r11)
	movups	%xmm1, 384(%r11)
	movsd	%xmm0, 400(%r11)
	movq	64(%rsp), %rdx          # 8-byte Reload
	cmpq	%rbp, %r13
	jl	.LBB3_93
	jmp	.LBB3_113
	.p2align	4, 0x90
.LBB3_78:                               # %.preheader184.split.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	leaq	1(%rbp), %r11
	addl	%r14d, %r11d
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax,%rbp), %r10
	andl	$3, %r11d
	movq	72(%rsp), %r9           # 8-byte Reload
	negq	%r9
	movl	12(%rsp), %eax          # 4-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$-52, %eax
	movl	$-51, %ecx
	cmovlel	%ecx, %eax
	subl	%eax, %r14d
	andl	$3, %r14d
	xorl	%edi, %edi
	movq	64(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_79:                               # %.preheader184.split
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_99 Depth 3
                                        #       Child Loop BB3_103 Depth 3
	testq	%r11, %r11
	movq	(%rdx,%rdi,8), %rax
	je	.LBB3_80
# BB#98:                                # %.lr.ph.i172.prol.preheader
                                        #   in Loop: Header=BB3_79 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx,8), %rdx
	xorpd	%xmm1, %xmm1
	movq	152(%rsp), %rcx         # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_99:                               # %.lr.ph.i172.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_79 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rdx,%rsi,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	(%rcx), %xmm2
	addsd	%xmm2, %xmm1
	incq	%rsi
	addq	$8, %rcx
	cmpq	%rsi, %r14
	jne	.LBB3_99
# BB#100:                               # %.lr.ph.i172.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_79 Depth=2
	addq	%r9, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	cmpq	$3, %r10
	jae	.LBB3_102
	jmp	.LBB3_104
	.p2align	4, 0x90
.LBB3_80:                               #   in Loop: Header=BB3_79 Depth=2
	xorpd	%xmm1, %xmm1
	movq	32(%rsp), %rsi          # 8-byte Reload
	cmpq	$3, %r10
	jb	.LBB3_104
.LBB3_102:                              # %.preheader184.split.new
                                        #   in Loop: Header=BB3_79 Depth=2
	decq	%rsi
	.p2align	4, 0x90
.LBB3_103:                              # %.lr.ph.i172
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_79 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	8(%rax,%rsi,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	8(%rbx,%rsi,8), %xmm2
	addsd	%xmm1, %xmm2
	movsd	16(%rax,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	16(%rbx,%rsi,8), %xmm1
	addsd	%xmm2, %xmm1
	movsd	24(%rax,%rsi,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	24(%rbx,%rsi,8), %xmm2
	addsd	%xmm1, %xmm2
	movsd	32(%rax,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	mulsd	32(%rbx,%rsi,8), %xmm1
	addsd	%xmm2, %xmm1
	addq	$4, %rsi
	cmpq	%rbp, %rsi
	jl	.LBB3_103
.LBB3_104:                              # %xty.exit174
                                        #   in Loop: Header=BB3_79 Depth=2
	mulsd	%xmm0, %xmm1
	movq	48(%rsp), %rax          # 8-byte Reload
	movsd	%xmm1, (%rax,%rdi,8)
	incq	%rdi
	cmpq	$51, %rdi
	jne	.LBB3_79
# BB#92:                                # %.preheader183
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rbp, %r13
	jge	.LBB3_113
.LBB3_93:                               # %.preheader.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	leaq	1(%rbp,%r8), %r14
	leaq	-3(%rbp,%r8), %r10
	shrq	$2, %r10
	movq	%r14, %r11
	andq	$-4, %r11
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r11), %r8
	movl	%r10d, %r9d
	andl	$1, %r9d
	movl	12(%rsp), %eax          # 4-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	cmpl	$-52, %eax
	movl	$-51, %ecx
	cmovlel	%ecx, %eax
	negl	%eax
	movslq	%eax, %rsi
	addq	72(%rsp), %rsi          # 8-byte Folded Reload
	andq	$-4, %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_94:                               # %.preheader
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_108 Depth 3
                                        #       Child Loop BB3_111 Depth 3
	cmpq	$4, %r14
	movq	48(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movq	(%rdx,%rdi,8), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	jb	.LBB3_110
# BB#95:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_94 Depth=2
	testq	%r11, %r11
	movq	16(%rsp), %rcx          # 8-byte Reload
	je	.LBB3_110
# BB#96:                                # %vector.ph
                                        #   in Loop: Header=BB3_94 Depth=2
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	testq	%r9, %r9
	jne	.LBB3_97
# BB#105:                               # %vector.body.prol
                                        #   in Loop: Header=BB3_94 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	movupd	(%rbx,%rcx,8), %xmm2
	movupd	16(%rbx,%rcx,8), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	(%rax,%rcx,8), %xmm4
	movupd	16(%rax,%rcx,8), %xmm5
	subpd	%xmm2, %xmm4
	subpd	%xmm3, %xmm5
	movupd	%xmm4, (%rax,%rcx,8)
	movupd	%xmm5, 16(%rax,%rcx,8)
	movl	$4, %edx
	testq	%r10, %r10
	jne	.LBB3_107
	jmp	.LBB3_109
.LBB3_97:                               #   in Loop: Header=BB3_94 Depth=2
	xorl	%edx, %edx
	testq	%r10, %r10
	je	.LBB3_109
.LBB3_107:                              # %vector.ph.new
                                        #   in Loop: Header=BB3_94 Depth=2
	movq	144(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB3_108:                              # %vector.body
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_94 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-48(%r15,%rdx,8), %xmm2
	movupd	-32(%r15,%rdx,8), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	-48(%rcx,%rdx,8), %xmm4
	movupd	-32(%rcx,%rdx,8), %xmm5
	subpd	%xmm2, %xmm4
	subpd	%xmm3, %xmm5
	movupd	%xmm4, -48(%rcx,%rdx,8)
	movupd	%xmm5, -32(%rcx,%rdx,8)
	movupd	-16(%r15,%rdx,8), %xmm2
	movupd	(%r15,%rdx,8), %xmm3
	mulpd	%xmm1, %xmm2
	mulpd	%xmm1, %xmm3
	movupd	-16(%rcx,%rdx,8), %xmm4
	movupd	(%rcx,%rdx,8), %xmm5
	subpd	%xmm2, %xmm4
	subpd	%xmm3, %xmm5
	movupd	%xmm4, -16(%rcx,%rdx,8)
	movupd	%xmm5, (%rcx,%rdx,8)
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.LBB3_108
.LBB3_109:                              # %middle.block
                                        #   in Loop: Header=BB3_94 Depth=2
	cmpq	%r11, %r14
	movq	%r8, %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	je	.LBB3_112
	.p2align	4, 0x90
.LBB3_110:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_94 Depth=2
	decq	%rcx
	.p2align	4, 0x90
.LBB3_111:                              # %scalar.ph
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_94 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	8(%rbx,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	8(%rax,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%rax,%rcx,8)
	incq	%rcx
	cmpq	%rbp, %rcx
	jl	.LBB3_111
.LBB3_112:                              # %._crit_edge205
                                        #   in Loop: Header=BB3_94 Depth=2
	incq	%rdi
	cmpq	$51, %rdi
	jne	.LBB3_94
.LBB3_113:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	$49, %eax
	subl	%r13d, %eax
	movl	100(%rsp), %ecx         # 4-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	addl	%ecx, %esi
	movq	24(%rsp), %rdx          # 8-byte Reload
	addl	%ecx, %edx
	cmpl	$50, %esi
	cmovll	%edx, %eax
	movq	16(%rsp), %r9           # 8-byte Reload
	incq	%r9
	movq	136(%rsp), %r8          # 8-byte Reload
	incq	%r8
	decl	12(%rsp)                # 4-byte Folded Spill
	addb	$3, 11(%rsp)            # 1-byte Folded Spill
	addq	$8, 112(%rsp)           # 8-byte Folded Spill
	decq	80(%rsp)                # 8-byte Folded Spill
	incq	104(%rsp)               # 8-byte Folded Spill
	decq	72(%rsp)                # 8-byte Folded Spill
	addq	$8, 128(%rsp)           # 8-byte Folded Spill
	addq	$8, %r15
	addq	$8, 120(%rsp)           # 8-byte Folded Spill
	addq	$8, 152(%rsp)           # 8-byte Folded Spill
	incq	144(%rsp)               # 8-byte Folded Spill
	movq	32(%rsp), %r13          # 8-byte Reload
	cmpq	$49, %r13
	movl	%eax, %r10d
	jne	.LBB3_3
# BB#114:
	movq	%rbx, %rdi
	callq	free
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	64(%rsp), %rax          # 8-byte Reload
.LBB3_115:
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	Trianglelise, .Lfunc_end3-Trianglelise
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
