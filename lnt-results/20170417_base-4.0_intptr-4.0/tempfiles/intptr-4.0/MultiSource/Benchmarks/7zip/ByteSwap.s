	.text
	.file	"ByteSwap.bc"
	.globl	_ZN10CByteSwap24InitEv
	.p2align	4, 0x90
	.type	_ZN10CByteSwap24InitEv,@function
_ZN10CByteSwap24InitEv:                 # @_ZN10CByteSwap24InitEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN10CByteSwap24InitEv, .Lfunc_end0-_ZN10CByteSwap24InitEv
	.cfi_endproc

	.globl	_ZN10CByteSwap26FilterEPhj
	.p2align	4, 0x90
	.type	_ZN10CByteSwap26FilterEPhj,@function
_ZN10CByteSwap26FilterEPhj:             # @_ZN10CByteSwap26FilterEPhj
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$2, %edx
	jb	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r9d
	movzbl	(%rsi,%r9), %r8d
	leal	1(%rcx), %edi
	movzbl	(%rsi,%rdi), %eax
	movb	%al, (%rsi,%r9)
	movb	%r8b, (%rsi,%rdi)
	leal	2(%rcx), %eax
	addl	$4, %ecx
	cmpl	%edx, %ecx
	movl	%eax, %ecx
	jbe	.LBB1_2
.LBB1_3:                                # %._crit_edge
	retq
.Lfunc_end1:
	.size	_ZN10CByteSwap26FilterEPhj, .Lfunc_end1-_ZN10CByteSwap26FilterEPhj
	.cfi_endproc

	.globl	_ZN10CByteSwap44InitEv
	.p2align	4, 0x90
	.type	_ZN10CByteSwap44InitEv,@function
_ZN10CByteSwap44InitEv:                 # @_ZN10CByteSwap44InitEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN10CByteSwap44InitEv, .Lfunc_end2-_ZN10CByteSwap44InitEv
	.cfi_endproc

	.globl	_ZN10CByteSwap46FilterEPhj
	.p2align	4, 0x90
	.type	_ZN10CByteSwap46FilterEPhj,@function
_ZN10CByteSwap46FilterEPhj:             # @_ZN10CByteSwap46FilterEPhj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	xorl	%eax, %eax
	cmpl	$4, %edx
	jb	.LBB3_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r11d
	movzbl	(%rsi,%r11), %r8d
	leal	1(%rcx), %r10d
	movzbl	(%rsi,%r10), %r9d
	leal	3(%rcx), %ebx
	movzbl	(%rsi,%rbx), %eax
	movb	%al, (%rsi,%r11)
	leal	2(%rcx), %edi
	movzbl	(%rsi,%rdi), %eax
	movb	%al, (%rsi,%r10)
	movb	%r9b, (%rsi,%rdi)
	movb	%r8b, (%rsi,%rbx)
	leal	4(%rcx), %eax
	addl	$8, %ecx
	cmpl	%edx, %ecx
	movl	%eax, %ecx
	jbe	.LBB3_2
.LBB3_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end3:
	.size	_ZN10CByteSwap46FilterEPhj, .Lfunc_end3-_ZN10CByteSwap46FilterEPhj
	.cfi_endproc

	.section	.text._ZN10CByteSwap214QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN10CByteSwap214QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN10CByteSwap214QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN10CByteSwap214QueryInterfaceERK4GUIDPPv,@function
_ZN10CByteSwap214QueryInterfaceERK4GUIDPPv: # @_ZN10CByteSwap214QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB4_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB4_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB4_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB4_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB4_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB4_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB4_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB4_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB4_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB4_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB4_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB4_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB4_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB4_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB4_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB4_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB4_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end4:
	.size	_ZN10CByteSwap214QueryInterfaceERK4GUIDPPv, .Lfunc_end4-_ZN10CByteSwap214QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN10CByteSwap26AddRefEv,"axG",@progbits,_ZN10CByteSwap26AddRefEv,comdat
	.weak	_ZN10CByteSwap26AddRefEv
	.p2align	4, 0x90
	.type	_ZN10CByteSwap26AddRefEv,@function
_ZN10CByteSwap26AddRefEv:               # @_ZN10CByteSwap26AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN10CByteSwap26AddRefEv, .Lfunc_end5-_ZN10CByteSwap26AddRefEv
	.cfi_endproc

	.section	.text._ZN10CByteSwap27ReleaseEv,"axG",@progbits,_ZN10CByteSwap27ReleaseEv,comdat
	.weak	_ZN10CByteSwap27ReleaseEv
	.p2align	4, 0x90
	.type	_ZN10CByteSwap27ReleaseEv,@function
_ZN10CByteSwap27ReleaseEv:              # @_ZN10CByteSwap27ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB6_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB6_2:
	popq	%rcx
	retq
.Lfunc_end6:
	.size	_ZN10CByteSwap27ReleaseEv, .Lfunc_end6-_ZN10CByteSwap27ReleaseEv
	.cfi_endproc

	.section	.text._ZN10CByteSwap2D0Ev,"axG",@progbits,_ZN10CByteSwap2D0Ev,comdat
	.weak	_ZN10CByteSwap2D0Ev
	.p2align	4, 0x90
	.type	_ZN10CByteSwap2D0Ev,@function
_ZN10CByteSwap2D0Ev:                    # @_ZN10CByteSwap2D0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end7:
	.size	_ZN10CByteSwap2D0Ev, .Lfunc_end7-_ZN10CByteSwap2D0Ev
	.cfi_endproc

	.section	.text._ZN10CByteSwap414QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN10CByteSwap414QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN10CByteSwap414QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN10CByteSwap414QueryInterfaceERK4GUIDPPv,@function
_ZN10CByteSwap414QueryInterfaceERK4GUIDPPv: # @_ZN10CByteSwap414QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB8_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB8_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB8_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB8_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB8_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB8_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB8_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB8_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB8_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB8_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB8_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB8_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB8_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB8_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB8_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB8_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB8_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end8:
	.size	_ZN10CByteSwap414QueryInterfaceERK4GUIDPPv, .Lfunc_end8-_ZN10CByteSwap414QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN10CByteSwap46AddRefEv,"axG",@progbits,_ZN10CByteSwap46AddRefEv,comdat
	.weak	_ZN10CByteSwap46AddRefEv
	.p2align	4, 0x90
	.type	_ZN10CByteSwap46AddRefEv,@function
_ZN10CByteSwap46AddRefEv:               # @_ZN10CByteSwap46AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN10CByteSwap46AddRefEv, .Lfunc_end9-_ZN10CByteSwap46AddRefEv
	.cfi_endproc

	.section	.text._ZN10CByteSwap47ReleaseEv,"axG",@progbits,_ZN10CByteSwap47ReleaseEv,comdat
	.weak	_ZN10CByteSwap47ReleaseEv
	.p2align	4, 0x90
	.type	_ZN10CByteSwap47ReleaseEv,@function
_ZN10CByteSwap47ReleaseEv:              # @_ZN10CByteSwap47ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB10_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB10_2:
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN10CByteSwap47ReleaseEv, .Lfunc_end10-_ZN10CByteSwap47ReleaseEv
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end11-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text._ZN10CByteSwap4D0Ev,"axG",@progbits,_ZN10CByteSwap4D0Ev,comdat
	.weak	_ZN10CByteSwap4D0Ev
	.p2align	4, 0x90
	.type	_ZN10CByteSwap4D0Ev,@function
_ZN10CByteSwap4D0Ev:                    # @_ZN10CByteSwap4D0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end12:
	.size	_ZN10CByteSwap4D0Ev, .Lfunc_end12-_ZN10CByteSwap4D0Ev
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZL12CreateCodec2v,@function
_ZL12CreateCodec2v:                     # @_ZL12CreateCodec2v
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV10CByteSwap2+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_ZL12CreateCodec2v, .Lfunc_end13-_ZL12CreateCodec2v
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZL12CreateCodec4v,@function
_ZL12CreateCodec4v:                     # @_ZL12CreateCodec4v
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movl	$16, %edi
	callq	_Znwm
	movl	$0, 8(%rax)
	movq	$_ZTV10CByteSwap4+16, (%rax)
	popq	%rcx
	retq
.Lfunc_end14:
	.size	_ZL12CreateCodec4v, .Lfunc_end14-_ZL12CreateCodec4v
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_ByteSwap.ii,@function
_GLOBAL__sub_I_ByteSwap.ii:             # @_GLOBAL__sub_I_ByteSwap.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 16
	movl	$_ZL12g_CodecsInfo, %edi
	callq	_Z13RegisterCodecPK10CCodecInfo
	movl	$_ZL12g_CodecsInfo+40, %edi
	popq	%rax
	jmp	_Z13RegisterCodecPK10CCodecInfo # TAILCALL
.Lfunc_end15:
	.size	_GLOBAL__sub_I_ByteSwap.ii, .Lfunc_end15-_GLOBAL__sub_I_ByteSwap.ii
	.cfi_endproc

	.type	_ZTV10CByteSwap2,@object # @_ZTV10CByteSwap2
	.section	.rodata,"a",@progbits
	.globl	_ZTV10CByteSwap2
	.p2align	3
_ZTV10CByteSwap2:
	.quad	0
	.quad	_ZTI10CByteSwap2
	.quad	_ZN10CByteSwap214QueryInterfaceERK4GUIDPPv
	.quad	_ZN10CByteSwap26AddRefEv
	.quad	_ZN10CByteSwap27ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN10CByteSwap2D0Ev
	.quad	_ZN10CByteSwap24InitEv
	.quad	_ZN10CByteSwap26FilterEPhj
	.size	_ZTV10CByteSwap2, 72

	.type	_ZTS10CByteSwap2,@object # @_ZTS10CByteSwap2
	.globl	_ZTS10CByteSwap2
_ZTS10CByteSwap2:
	.asciz	"10CByteSwap2"
	.size	_ZTS10CByteSwap2, 13

	.type	_ZTS15ICompressFilter,@object # @_ZTS15ICompressFilter
	.section	.rodata._ZTS15ICompressFilter,"aG",@progbits,_ZTS15ICompressFilter,comdat
	.weak	_ZTS15ICompressFilter
	.p2align	4
_ZTS15ICompressFilter:
	.asciz	"15ICompressFilter"
	.size	_ZTS15ICompressFilter, 18

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI15ICompressFilter,@object # @_ZTI15ICompressFilter
	.section	.rodata._ZTI15ICompressFilter,"aG",@progbits,_ZTI15ICompressFilter,comdat
	.weak	_ZTI15ICompressFilter
	.p2align	4
_ZTI15ICompressFilter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15ICompressFilter
	.quad	_ZTI8IUnknown
	.size	_ZTI15ICompressFilter, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI10CByteSwap2,@object # @_ZTI10CByteSwap2
	.section	.rodata,"a",@progbits
	.globl	_ZTI10CByteSwap2
	.p2align	4
_ZTI10CByteSwap2:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS10CByteSwap2
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI15ICompressFilter
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI10CByteSwap2, 56

	.type	_ZTV10CByteSwap4,@object # @_ZTV10CByteSwap4
	.globl	_ZTV10CByteSwap4
	.p2align	3
_ZTV10CByteSwap4:
	.quad	0
	.quad	_ZTI10CByteSwap4
	.quad	_ZN10CByteSwap414QueryInterfaceERK4GUIDPPv
	.quad	_ZN10CByteSwap46AddRefEv
	.quad	_ZN10CByteSwap47ReleaseEv
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN10CByteSwap4D0Ev
	.quad	_ZN10CByteSwap44InitEv
	.quad	_ZN10CByteSwap46FilterEPhj
	.size	_ZTV10CByteSwap4, 72

	.type	_ZTS10CByteSwap4,@object # @_ZTS10CByteSwap4
	.globl	_ZTS10CByteSwap4
_ZTS10CByteSwap4:
	.asciz	"10CByteSwap4"
	.size	_ZTS10CByteSwap4, 13

	.type	_ZTI10CByteSwap4,@object # @_ZTI10CByteSwap4
	.globl	_ZTI10CByteSwap4
	.p2align	4
_ZTI10CByteSwap4:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS10CByteSwap4
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI15ICompressFilter
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI10CByteSwap4, 56

	.type	_ZL12g_CodecsInfo,@object # @_ZL12g_CodecsInfo
	.data
	.p2align	4
_ZL12g_CodecsInfo:
	.quad	_ZL12CreateCodec2v
	.quad	_ZL12CreateCodec2v
	.quad	131842                  # 0x20302
	.quad	.L.str
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZL12CreateCodec4v
	.quad	_ZL12CreateCodec4v
	.quad	131844                  # 0x20304
	.quad	.L.str.1
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.size	_ZL12g_CodecsInfo, 80

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	83                      # 0x53
	.long	119                     # 0x77
	.long	97                      # 0x61
	.long	112                     # 0x70
	.long	50                      # 0x32
	.long	0                       # 0x0
	.size	.L.str, 24

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	83                      # 0x53
	.long	119                     # 0x77
	.long	97                      # 0x61
	.long	112                     # 0x70
	.long	52                      # 0x34
	.long	0                       # 0x0
	.size	.L.str.1, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_ByteSwap.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
