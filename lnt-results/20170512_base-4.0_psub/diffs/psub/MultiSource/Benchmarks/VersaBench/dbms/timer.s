	.text
	.file	"timer.bc"
	.globl	initTime
	.p2align	4, 0x90
	.type	initTime,@function
initTime:                               # @initTime
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	xorl	%edi, %edi
	callq	time
	movq	%rax, startTime(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	initTime, .Lfunc_end0-initTime
	.cfi_endproc

	.globl	getTime
	.p2align	4, 0x90
	.type	getTime,@function
getTime:                                # @getTime
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	xorl	%edi, %edi
	callq	time
	movq	startTime(%rip), %rsi
	movq	%rax, %rdi
	callq	difftime
	cvttsd2si	%xmm0, %rax
	imulq	$1000, %rax, %rax       # imm = 0x3E8
	popq	%rcx
	retq
.Lfunc_end1:
	.size	getTime, .Lfunc_end1-getTime
	.cfi_endproc

	.type	startTime,@object       # @startTime
	.local	startTime
	.comm	startTime,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
