	.text
	.file	"SPEdriver.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4576918229304087675     # double 0.01
	.text
	.globl	SPEdriver
	.p2align	4, 0x90
	.type	SPEdriver,@function
SPEdriver:                              # @SPEdriver
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	104(%rsp), %r14
	leaq	24(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	callq	clock
	movl	$2000000, %r15d         # imm = 0x1E8480
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	$12, %edi
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	pushq	%r14
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	112(%rsp)
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	Crystal_div
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	decl	%r15d
	jne	.LBB0_1
# BB#2:                                 # %.preheader63.preheader
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	8(%rsp), %r15           # 8-byte Reload
	movl	$2000000, %ebp          # imm = 0x1E8480
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader63
                                        # =>This Inner Loop Header: Depth=1
	movl	$12, %edi
	movq	%r12, %rsi
	callq	Crystal_pow
	decl	%ebp
	jne	.LBB0_3
# BB#4:                                 # %.preheader61.preheader
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movl	$88, %eax
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader61
                                        # =>This Inner Loop Header: Depth=1
	movq	-88(%rbx,%rax), %rcx
	movq	%rcx, -88(%r14,%rax)
	movq	-80(%rbx,%rax), %rcx
	movq	%rcx, -80(%r14,%rax)
	movq	-72(%rbx,%rax), %rcx
	movq	%rcx, -72(%r14,%rax)
	movq	-64(%rbx,%rax), %rcx
	movq	%rcx, -64(%r14,%rax)
	movq	-56(%rbx,%rax), %rcx
	movq	%rcx, -56(%r14,%rax)
	movq	-48(%rbx,%rax), %rcx
	movq	%rcx, -48(%r14,%rax)
	movq	-40(%rbx,%rax), %rcx
	movq	%rcx, -40(%r14,%rax)
	movq	-32(%rbx,%rax), %rcx
	movq	%rcx, -32(%r14,%rax)
	movq	-24(%rbx,%rax), %rcx
	movq	%rcx, -24(%r14,%rax)
	movq	-16(%rbx,%rax), %rcx
	movq	%rcx, -16(%r14,%rax)
	movq	-8(%rbx,%rax), %rcx
	movq	%rcx, -8(%r14,%rax)
	movq	(%rbx,%rax), %rcx
	movq	%rcx, (%r14,%rax)
	addq	$96, %rax
	cmpq	$1240, %rax             # imm = 0x4D8
	jne	.LBB0_5
# BB#6:                                 # %.preheader59.preheader
	xorl	%ebp, %ebp
	movq	(%rsp), %r12            # 8-byte Reload
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader59
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
	movl	$88, %eax
	.p2align	4, 0x90
.LBB0_8:                                # %.preheader
                                        #   Parent Loop BB0_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-88(%r14,%rax), %rcx
	movq	%rcx, -88(%rbx,%rax)
	movq	-80(%r14,%rax), %rcx
	movq	%rcx, -80(%rbx,%rax)
	movq	-72(%r14,%rax), %rcx
	movq	%rcx, -72(%rbx,%rax)
	movq	-64(%r14,%rax), %rcx
	movq	%rcx, -64(%rbx,%rax)
	movq	-56(%r14,%rax), %rcx
	movq	%rcx, -56(%rbx,%rax)
	movq	-48(%r14,%rax), %rcx
	movq	%rcx, -48(%rbx,%rax)
	movq	-40(%r14,%rax), %rcx
	movq	%rcx, -40(%rbx,%rax)
	movq	-32(%r14,%rax), %rcx
	movq	%rcx, -32(%rbx,%rax)
	movq	-24(%r14,%rax), %rcx
	movq	%rcx, -24(%rbx,%rax)
	movq	-16(%r14,%rax), %rcx
	movq	%rcx, -16(%rbx,%rax)
	movq	-8(%r14,%rax), %rcx
	movq	%rcx, -8(%rbx,%rax)
	movq	(%r14,%rax), %rcx
	movq	%rcx, (%rbx,%rax)
	addq	$96, %rax
	cmpq	$1240, %rax             # imm = 0x4D8
	jne	.LBB0_8
# BB#9:                                 #   in Loop: Header=BB0_7 Depth=1
	movl	$12, %edi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	Crystal_Cholesky
	incl	%ebp
	cmpl	$2000000, %ebp          # imm = 0x1E8480
	jne	.LBB0_7
# BB#10:
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	SPEdriver, .Lfunc_end0-SPEdriver
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
