	.text
	.file	"partition.bc"
	.globl	part_Create
	.p2align	4, 0x90
	.type	part_Create,@function
part_Create:                            # @part_Create
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	leal	3(%rbx,%rbx,2), %ebp
	movl	$4, %esi
	movl	%ebp, %edi
	callq	memory_Calloc
	movslq	%ebx, %rdx
	leaq	12(%rax,%rdx,4), %rcx
	movl	%edx, 8(%rax,%rdx,4)
	movl	%ebp, 4(%rax,%rdx,4)
	movl	$1, (%rax,%rdx,4)
	movq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	part_Create, .Lfunc_end0-part_Create
	.cfi_endproc

	.globl	part_Init
	.p2align	4, 0x90
	.type	part_Init,@function
part_Init:                              # @part_Init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	-8(%rbx), %ecx
	leal	-3(%rcx), %edx
	movslq	%edx, %rax
	imulq	$1431655766, %rax, %rax # imm = 0x55555556
	movq	%rax, %rsi
	shrq	$63, %rsi
	shrq	$32, %rax
	addl	%esi, %eax
	cmpl	%r14d, %eax
	jge	.LBB1_9
# BB#1:
	cltq
	shlq	$2, %rax
	subq	%rax, %rbx
	addq	$-12, %rbx
	shll	$2, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB1_2
# BB#7:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rbx, (%rax)
	jmp	.LBB1_8
.LBB1_9:
	movl	%r14d, -4(%rbx)
	movl	-12(%rbx), %ecx
	leal	1(%rcx), %esi
	movl	%esi, -12(%rbx)
	testl	%ecx, %ecx
	jns	.LBB1_13
# BB#10:                                # %.preheader
	leaq	-12(%rbx), %r15
	cmpl	$3, %edx
	jl	.LBB1_12
# BB#11:                                # %.lr.ph.preheader
	testl	%eax, %eax
	movl	$1, %ecx
	cmovgl	%eax, %ecx
	decl	%ecx
	movq	$-4, %rax
	subq	%rcx, %rax
	leaq	(%rbx,%rax,4), %rdi
	leaq	4(,%rcx,4), %rdx
	xorl	%esi, %esi
	callq	memset
	jmp	.LBB1_12
.LBB1_2:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rbx, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %rdi
	testq	%rdx, %rdx
	leaq	8(%rdx), %rdx
	movl	$memory_BIGBLOCKS, %ebp
	cmovneq	%rdx, %rbp
	movq	%rdi, (%rbp)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_4
# BB#3:
	negq	%rax
	movq	-16(%rbx,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_4:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_6
# BB#5:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_6:
	addq	$-16, %rbx
	movq	%rbx, %rdi
	callq	free
.LBB1_8:                                # %part_Free.exit
	leal	3(%r14,%r14,2), %ebp
	movl	$4, %esi
	movl	%ebp, %edi
	callq	memory_Calloc
	movslq	%r14d, %rcx
	leaq	(%rax,%rcx,4), %r15
	leaq	12(%rax,%rcx,4), %rbx
	movl	%r14d, 8(%rax,%rcx,4)
	movl	%ebp, 4(%rax,%rcx,4)
.LBB1_12:                               # %.sink.split
	movl	$1, (%r15)
.LBB1_13:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	part_Init, .Lfunc_end1-part_Init
	.cfi_endproc

	.globl	part_Find
	.p2align	4, 0x90
	.type	part_Find,@function
part_Find:                              # @part_Find
	.cfi_startproc
# BB#0:
	movl	%esi, %ecx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r8d
	movl	$-4, %ecx
	subl	%r8d, %ecx
	movslq	%ecx, %rcx
	movl	(%rdi,%rcx,4), %edx
	cmpl	-12(%rdi), %edx
	jne	.LBB2_3
# BB#2:                                 # %.part_DelayedInit.exit_crit_edge.i
                                        #   in Loop: Header=BB2_1 Depth=1
	movslq	%r8d, %rcx
	leaq	(%rdi,%rcx,4), %r9
	jmp	.LBB2_4
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	movl	%r8d, %eax
	notl	%eax
	movslq	%r8d, %rdx
	leaq	(%rdi,%rdx,4), %r9
	movl	%eax, (%rdi,%rdx,4)
	movslq	-4(%rdi), %rax
	addq	%rdx, %rax
	movl	$1, (%rdi,%rax,4)
	movl	-12(%rdi), %eax
	movl	%eax, (%rdi,%rcx,4)
.LBB2_4:                                # %part_DelayedInit.exit.i
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	(%r9), %ecx
	testl	%ecx, %ecx
	jns	.LBB2_1
# BB#5:                                 # %.preheader.i
	cmpl	%esi, %r8d
	je	.LBB2_8
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rax
	movl	(%rdi,%rax,4), %esi
	movl	%r8d, (%rdi,%rax,4)
	cmpl	%r8d, %esi
	jne	.LBB2_6
# BB#7:
	movl	%r8d, %esi
.LBB2_8:                                # %part_NF.exit
	movslq	%esi, %rax
	movl	(%rdi,%rax,4), %eax
	notl	%eax
	retq
.Lfunc_end2:
	.size	part_Find, .Lfunc_end2-part_Find
	.cfi_endproc

	.globl	part_Union
	.p2align	4, 0x90
	.type	part_Union,@function
part_Union:                             # @part_Union
	.cfi_startproc
# BB#0:
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movl	%esi, %ecx
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r10d
	movl	$-4, %ecx
	subl	%r10d, %ecx
	movslq	%ecx, %r9
	movl	(%rdi,%r9,4), %ecx
	cmpl	-12(%rdi), %ecx
	jne	.LBB3_3
# BB#2:                                 # %.part_DelayedInit.exit_crit_edge.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movslq	%r10d, %rcx
	leaq	(%rdi,%rcx,4), %r8
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_3:                                #   in Loop: Header=BB3_1 Depth=1
	movl	%r10d, %ecx
	notl	%ecx
	movslq	%r10d, %rax
	leaq	(%rdi,%rax,4), %r8
	movl	%ecx, (%rdi,%rax,4)
	movslq	-4(%rdi), %rcx
	addq	%rax, %rcx
	movl	$1, (%rdi,%rcx,4)
	movl	-12(%rdi), %eax
	movl	%eax, (%rdi,%r9,4)
.LBB3_4:                                # %part_DelayedInit.exit.i
                                        #   in Loop: Header=BB3_1 Depth=1
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	jns	.LBB3_1
# BB#5:                                 # %.preheader.i
	cmpl	%esi, %r10d
	je	.LBB3_8
	.p2align	4, 0x90
.LBB3_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	%esi, %rax
	movl	(%rdi,%rax,4), %esi
	movl	%r10d, (%rdi,%rax,4)
	cmpl	%r10d, %esi
	jne	.LBB3_6
# BB#7:
	movl	%r10d, %esi
.LBB3_8:                                # %part_NF.exit
	movl	%edx, %ecx
	.p2align	4, 0x90
.LBB3_9:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, %r10d
	movl	$-4, %ecx
	subl	%r10d, %ecx
	movslq	%ecx, %r9
	movl	(%rdi,%r9,4), %ecx
	cmpl	-12(%rdi), %ecx
	jne	.LBB3_11
# BB#10:                                # %.part_DelayedInit.exit_crit_edge.i39
                                        #   in Loop: Header=BB3_9 Depth=1
	movslq	%r10d, %rcx
	leaq	(%rdi,%rcx,4), %r8
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_9 Depth=1
	movl	%r10d, %ecx
	notl	%ecx
	movslq	%r10d, %rax
	leaq	(%rdi,%rax,4), %r8
	movl	%ecx, (%rdi,%rax,4)
	movslq	-4(%rdi), %rcx
	addq	%rax, %rcx
	movl	$1, (%rdi,%rcx,4)
	movl	-12(%rdi), %eax
	movl	%eax, (%rdi,%r9,4)
.LBB3_12:                               # %part_DelayedInit.exit.i41
                                        #   in Loop: Header=BB3_9 Depth=1
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	jns	.LBB3_9
# BB#13:                                # %.preheader.i42
	cmpl	%edx, %r10d
	je	.LBB3_16
	.p2align	4, 0x90
.LBB3_14:                               # %.lr.ph.i44
                                        # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rax
	movl	(%rdi,%rax,4), %edx
	movl	%r10d, (%rdi,%rax,4)
	cmpl	%r10d, %edx
	jne	.LBB3_14
# BB#15:
	movl	%r10d, %edx
.LBB3_16:                               # %part_NF.exit46
	cmpl	%edx, %esi
	je	.LBB3_21
# BB#17:
	movl	-4(%rdi), %eax
	leal	(%rax,%rsi), %ecx
	movslq	%ecx, %rcx
	movl	(%rdi,%rcx,4), %ecx
	addl	%edx, %eax
	cltq
	cmpl	(%rdi,%rax,4), %ecx
	jge	.LBB3_18
# BB#19:
	movslq	%esi, %rax
	movl	(%rdi,%rax,4), %r8d
	movslq	%edx, %rcx
	movl	%r8d, (%rdi,%rcx,4)
	movslq	(%rdi,%rax,4), %rax
	notq	%rax
	movl	%edx, (%rdi,%rax,4)
	movl	%esi, %eax
	jmp	.LBB3_20
.LBB3_18:
	movl	%edx, %eax
	movl	%esi, %edx
.LBB3_20:
	cltq
	movl	%edx, (%rdi,%rax,4)
	movl	-4(%rdi), %ecx
	addl	%ecx, %edx
	movslq	%edx, %rdx
	addl	%ecx, %eax
	cltq
	movl	(%rdi,%rax,4), %eax
	addl	%eax, (%rdi,%rdx,4)
.LBB3_21:
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	part_Union, .Lfunc_end3-part_Union
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
