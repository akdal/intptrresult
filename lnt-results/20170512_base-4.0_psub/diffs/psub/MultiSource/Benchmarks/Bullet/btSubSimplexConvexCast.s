	.text
	.file	"btSubSimplexConvexCast.bc"
	.globl	_ZN22btSubsimplexConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.p2align	4, 0x90
	.type	_ZN22btSubsimplexConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver,@function
_ZN22btSubsimplexConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver: # @_ZN22btSubsimplexConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.cfi_startproc
# BB#0:
	movq	$_ZTV22btSubsimplexConvexCast+16, (%rdi)
	movq	%rcx, 8(%rdi)
	movq	%rsi, 16(%rdi)
	movq	%rdx, 24(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN22btSubsimplexConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver, .Lfunc_end0-_ZN22btSubsimplexConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	953267991               # float 9.99999974E-5
.LCPI1_2:
	.long	1065353216              # float 1
.LCPI1_3:
	.long	2826960896              # float -1.42108547E-14
.LCPI1_4:
	.long	679477248               # float 1.42108547E-14
.LCPI1_5:
	.long	0                       # float 0
	.text
	.globl	_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.p2align	4, 0x90
	.type	_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE,@function
_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE: # @_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$568, %rsp              # imm = 0x238
.Lcfi6:
	.cfi_def_cfa_offset 624
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 256(%rsp)          # 8-byte Spill
	movq	%r8, %rbp
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	8(%r14), %rdi
	callq	_ZN22btVoronoiSimplexSolver5resetEv
	movss	48(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	48(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	52(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	subss	%xmm0, %xmm2
	movaps	%xmm2, %xmm4
	movss	52(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 416(%rsp)        # 16-byte Spill
	subss	%xmm0, %xmm1
	movaps	%xmm1, %xmm5
	movss	56(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	56(%r13), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)         # 4-byte Spill
	subss	%xmm0, %xmm1
	movaps	%xmm1, %xmm7
	movss	48(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	48(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	52(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	subss	%xmm2, %xmm0
	movss	52(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	subss	%xmm2, %xmm1
	movq	%rbp, 264(%rsp)         # 8-byte Spill
	movss	56(%rbp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movq	%r14, %rbp
	movss	56(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 160(%rsp)        # 16-byte Spill
	subss	%xmm3, %xmm2
	movsd	(%r13), %xmm3           # xmm3 = mem[0],zero
	movsd	16(%r13), %xmm9         # xmm9 = mem[0],zero
	movsd	32(%r13), %xmm8         # xmm8 = mem[0],zero
	subss	%xmm0, %xmm4
	movaps	%xmm4, 176(%rsp)        # 16-byte Spill
	subss	%xmm1, %xmm5
	subss	%xmm2, %xmm7
	movaps	%xmm7, 192(%rsp)        # 16-byte Spill
	movaps	.LCPI1_0(%rip), %xmm6   # xmm6 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm4, %xmm0
	xorps	%xmm6, %xmm0
	movaps	%xmm5, %xmm1
	movaps	%xmm5, 208(%rsp)        # 16-byte Spill
	xorps	%xmm6, %xmm1
	pshufd	$224, %xmm0, %xmm2      # xmm2 = xmm0[0,0,2,3]
	movaps	%xmm3, 400(%rsp)        # 16-byte Spill
	mulps	%xmm3, %xmm2
	pshufd	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	%xmm9, 384(%rsp)        # 16-byte Spill
	mulps	%xmm9, %xmm1
	addps	%xmm2, %xmm1
	movaps	%xmm7, %xmm2
	xorps	%xmm6, %xmm2
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	%xmm8, 240(%rsp)        # 16-byte Spill
	mulps	%xmm8, %xmm2
	addps	%xmm1, %xmm2
	movss	8(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 368(%rsp)        # 16-byte Spill
	mulss	%xmm1, %xmm0
	movss	24(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 352(%rsp)        # 16-byte Spill
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm0
	movss	40(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 28(%rsp)         # 4-byte Spill
	mulss	%xmm7, %xmm1
	subss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movaps	%xmm0, 336(%rsp)        # 16-byte Spill
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 320(%rsp)        # 16-byte Spill
	movss	24(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 304(%rsp)        # 16-byte Spill
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 288(%rsp)        # 16-byte Spill
	movsd	32(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	40(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 24(%rsp)         # 4-byte Spill
	movlps	%xmm2, (%rsp)
	movlps	%xmm1, 8(%rsp)
	movq	%rsp, %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	movss	32(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	16(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%r13), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	20(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm0, %xmm6
	addps	%xmm2, %xmm6
	movss	40(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	24(%r13), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm1, %xmm5
	addps	%xmm6, %xmm5
	movsd	48(%r13), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm5, %xmm4
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	movss	36(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	addss	%xmm1, %xmm2
	addss	56(%r13), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, 32(%rsp)
	movlps	%xmm0, 40(%rsp)
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movaps	176(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm5, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	208(%rsp), %xmm4        # 16-byte Reload
	movaps	%xmm4, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	addps	%xmm1, %xmm2
	movsd	32(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	192(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	addps	%xmm2, %xmm1
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm0
	movss	24(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	movss	40(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, (%rsp)
	movlps	%xmm2, 8(%rsp)
	movq	%rsp, %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	movss	32(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	16(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%r12), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	20(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm0, %xmm6
	addps	%xmm2, %xmm6
	movss	40(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	24(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm1, %xmm5
	addps	%xmm6, %xmm5
	movsd	48(%r12), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm5, %xmm1
	movss	36(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm2
	addss	56(%r12), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 144(%rsp)
	movlps	%xmm0, 152(%rsp)
	movss	36(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	shufps	$0, %xmm4, %xmm0        # xmm0 = xmm0[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm0      # xmm0 = xmm0[2,0],xmm4[2,3]
	subps	%xmm1, %xmm0
	subss	%xmm2, %xmm3
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movlps	%xmm0, (%rsp)
	movlps	%xmm2, 8(%rsp)
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm3, %xmm3
	addss	%xmm1, %xmm3
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	ucomiss	.LCPI1_1(%rip), %xmm3
	xorps	%xmm3, %xmm3
	xorps	%xmm2, %xmm2
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	jbe	.LBB1_11
# BB#1:                                 # %.lr.ph
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	unpcklps	64(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	unpcklps	160(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movaps	368(%rsp), %xmm0        # 16-byte Reload
	unpcklps	352(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movaps	320(%rsp), %xmm0        # 16-byte Reload
	unpcklps	304(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	movaps	%xmm0, 528(%rsp)        # 16-byte Spill
	movaps	400(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm2
	movaps	384(%rsp), %xmm1        # 16-byte Reload
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm2, 512(%rsp)        # 16-byte Spill
	shufps	$17, %xmm0, %xmm1       # xmm1 = xmm1[1,0],xmm0[1,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movaps	%xmm1, 496(%rsp)        # 16-byte Spill
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 480(%rsp)        # 16-byte Spill
	movaps	336(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm2
	movaps	288(%rsp), %xmm1        # 16-byte Reload
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm2, 464(%rsp)        # 16-byte Spill
	shufps	$17, %xmm0, %xmm1       # xmm1 = xmm1[1,0],xmm0[1,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movaps	%xmm1, 448(%rsp)        # 16-byte Spill
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 432(%rsp)        # 16-byte Spill
	movl	$-33, %r14d
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	xorps	%xmm2, %xmm2
	xorps	%xmm3, %xmm3
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	incl	%r14d
	je	.LBB1_11
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	movq	16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	movd	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movd	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movdqa	.LCPI1_0(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movdqa	%xmm2, %xmm5
	pxor	%xmm5, %xmm0
	movdqa	%xmm1, %xmm2
	pxor	%xmm5, %xmm2
	movd	8(%rsp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movdqa	%xmm3, %xmm4
	pxor	%xmm5, %xmm4
	pshufd	$224, %xmm0, %xmm5      # xmm5 = xmm0[0,0,2,3]
	mulps	400(%rsp), %xmm5        # 16-byte Folded Reload
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	384(%rsp), %xmm2        # 16-byte Folded Reload
	addps	%xmm5, %xmm2
	pshufd	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	240(%rsp), %xmm4        # 16-byte Folded Reload
	addps	%xmm2, %xmm4
	mulss	368(%rsp), %xmm0        # 16-byte Folded Reload
	mulss	352(%rsp), %xmm1        # 16-byte Folded Reload
	subss	%xmm1, %xmm0
	mulss	28(%rsp), %xmm3         # 4-byte Folded Reload
	subss	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, 112(%rsp)
	movlps	%xmm1, 120(%rsp)
	leaq	112(%rsp), %r15
	movq	%r15, %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	240(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	512(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	496(%rsp), %xmm4        # 16-byte Reload
	mulps	%xmm2, %xmm4
	addps	%xmm0, %xmm4
	movss	28(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	160(%rsp), %xmm1        # 16-byte Folded Reload
	addps	%xmm4, %xmm1
	movaps	96(%rsp), %xmm4         # 16-byte Reload
	shufps	$231, %xmm4, %xmm4      # xmm4 = xmm4[3,1,2,3]
	movaps	416(%rsp), %xmm5        # 16-byte Reload
	shufps	$0, %xmm4, %xmm5        # xmm5 = xmm5[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm5      # xmm5 = xmm5[2,0],xmm4[2,3]
	addps	%xmm1, %xmm5
	mulss	480(%rsp), %xmm2        # 16-byte Folded Reload
	addss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	addss	20(%rsp), %xmm2         # 4-byte Folded Reload
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm5, 32(%rsp)
	movlps	%xmm0, 40(%rsp)
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	320(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	336(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	304(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm1, %xmm4
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	288(%rsp), %xmm1        # 16-byte Folded Reload
	addps	%xmm0, %xmm1
	movss	24(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	224(%rsp), %xmm2        # 16-byte Folded Reload
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movq	24(%rbp), %rdi
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	movlps	%xmm2, 112(%rsp)
	movlps	%xmm0, 120(%rsp)
	movq	%r15, %rsi
	callq	*%rax
	movaps	%xmm0, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	224(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	464(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	448(%rsp), %xmm4        # 16-byte Reload
	mulps	%xmm2, %xmm4
	addps	%xmm0, %xmm4
	movss	24(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	528(%rsp), %xmm1        # 16-byte Folded Reload
	addps	%xmm4, %xmm1
	movaps	96(%rsp), %xmm4         # 16-byte Reload
	addps	%xmm4, %xmm1
	mulss	432(%rsp), %xmm2        # 16-byte Folded Reload
	addss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm4, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	addss	%xmm2, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm1, 144(%rsp)
	movlps	%xmm0, 152(%rsp)
	movsd	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	40(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm5
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm0, 272(%rsp)
	movlps	%xmm1, 280(%rsp)
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	ucomiss	.LCPI1_2(%rip), %xmm2
	ja	.LBB1_18
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	movaps	%xmm0, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movss	(%rsp), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm4
	mulss	%xmm0, %xmm4
	mulss	%xmm9, %xmm6
	addss	%xmm4, %xmm6
	movss	8(%rsp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	addss	%xmm6, %xmm5
	ucomiss	.LCPI1_5, %xmm5
	jbe	.LBB1_7
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	movaps	176(%rsp), %xmm6        # 16-byte Reload
	mulss	%xmm8, %xmm6
	movaps	208(%rsp), %xmm7        # 16-byte Reload
	mulss	%xmm9, %xmm7
	addss	%xmm6, %xmm7
	movaps	192(%rsp), %xmm6        # 16-byte Reload
	mulss	%xmm4, %xmm6
	addss	%xmm7, %xmm6
	ucomiss	.LCPI1_3(%rip), %xmm6
	jae	.LBB1_18
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	divss	%xmm6, %xmm5
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	subss	%xmm5, %xmm3
	movss	.LCPI1_2(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm5
	movss	52(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm6
	movss	52(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	movaps	%xmm2, 416(%rsp)        # 16-byte Spill
	movss	56(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm6
	movss	56(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	movss	%xmm2, 20(%rsp)         # 4-byte Spill
	shufps	$0, %xmm5, %xmm5        # xmm5 = xmm5[0,0,0,0]
	movss	48(%r13), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	48(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	52(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	movss	56(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	unpcklps	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	mulps	%xmm5, %xmm7
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	movaps	%xmm3, %xmm2
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	movss	48(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movq	264(%rsp), %rax         # 8-byte Reload
	movss	48(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	52(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movss	56(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	mulps	%xmm2, %xmm3
	addps	%xmm7, %xmm3
	movaps	%xmm3, 96(%rsp)         # 16-byte Spill
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movupd	%xmm0, 272(%rsp)
	movaps	%xmm8, 128(%rsp)        # 16-byte Spill
	movaps	%xmm9, 48(%rsp)         # 16-byte Spill
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	movq	8(%rbp), %rdi
	leaq	272(%rsp), %rsi
	leaq	32(%rsp), %rdx
	leaq	144(%rsp), %rcx
	callq	_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_
	movq	8(%rbp), %rdi
	movq	%rsp, %rsi
	callq	_ZN22btVoronoiSimplexSolver7closestER9btVector3
	testb	%al, %al
	je	.LBB1_10
# BB#8:                                 # %.thread.backedge
                                        #   in Loop: Header=BB1_2 Depth=1
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI1_1(%rip), %xmm0
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	ja	.LBB1_2
	jmp	.LBB1_11
.LBB1_10:
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	48(%rsp), %xmm3         # 16-byte Reload
.LBB1_11:                               # %.critedge
	movq	256(%rsp), %rbx         # 8-byte Reload
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 168(%rbx)
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm0, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI1_4(%rip), %xmm0
	jae	.LBB1_13
# BB#12:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 136(%rbx)
	xorps	%xmm2, %xmm2
	xorps	%xmm3, %xmm3
	xorps	%xmm0, %xmm0
	jmp	.LBB1_16
.LBB1_13:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB1_15
# BB#14:                                # %call.sqrt
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm3         # 16-byte Reload
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB1_15:                               # %.split
	movss	.LCPI1_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movaps	128(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm2
	movaps	%xmm4, %xmm0
	unpcklps	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 136(%rbx)
	movaps	%xmm4, %xmm0
	movlps	%xmm1, 144(%rbx)
.LBB1_16:
	movaps	176(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm3, %xmm0
	addss	%xmm1, %xmm0
	movaps	192(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	184(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI1_0(%rip), %xmm0
	ucomiss	%xmm0, %xmm1
	jae	.LBB1_18
# BB#17:
	movq	8(%rbp), %rdi
	leaq	112(%rsp), %rsi
	leaq	552(%rsp), %rdx
	callq	_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_
	movups	552(%rsp), %xmm0
	movups	%xmm0, 152(%rbx)
	movb	$1, %al
	jmp	.LBB1_19
.LBB1_18:
	xorl	%eax, %eax
.LBB1_19:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$568, %rsp              # imm = 0x238
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE, .Lfunc_end1-_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.cfi_endproc

	.section	.text._ZN22btSubsimplexConvexCastD0Ev,"axG",@progbits,_ZN22btSubsimplexConvexCastD0Ev,comdat
	.weak	_ZN22btSubsimplexConvexCastD0Ev
	.p2align	4, 0x90
	.type	_ZN22btSubsimplexConvexCastD0Ev,@function
_ZN22btSubsimplexConvexCastD0Ev:        # @_ZN22btSubsimplexConvexCastD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN12btConvexCastD2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN22btSubsimplexConvexCastD0Ev, .Lfunc_end2-_ZN22btSubsimplexConvexCastD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV22btSubsimplexConvexCast,@object # @_ZTV22btSubsimplexConvexCast
	.section	.rodata,"a",@progbits
	.globl	_ZTV22btSubsimplexConvexCast
	.p2align	3
_ZTV22btSubsimplexConvexCast:
	.quad	0
	.quad	_ZTI22btSubsimplexConvexCast
	.quad	_ZN12btConvexCastD2Ev
	.quad	_ZN22btSubsimplexConvexCastD0Ev
	.quad	_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
	.size	_ZTV22btSubsimplexConvexCast, 40

	.type	_ZTS22btSubsimplexConvexCast,@object # @_ZTS22btSubsimplexConvexCast
	.globl	_ZTS22btSubsimplexConvexCast
	.p2align	4
_ZTS22btSubsimplexConvexCast:
	.asciz	"22btSubsimplexConvexCast"
	.size	_ZTS22btSubsimplexConvexCast, 25

	.type	_ZTI22btSubsimplexConvexCast,@object # @_ZTI22btSubsimplexConvexCast
	.globl	_ZTI22btSubsimplexConvexCast
	.p2align	4
_ZTI22btSubsimplexConvexCast:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22btSubsimplexConvexCast
	.quad	_ZTI12btConvexCast
	.size	_ZTI22btSubsimplexConvexCast, 24


	.globl	_ZN22btSubsimplexConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.type	_ZN22btSubsimplexConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver,@function
_ZN22btSubsimplexConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver = _ZN22btSubsimplexConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
