	.text
	.file	"iname.bc"
	.globl	name_init
	.p2align	4, 0x90
	.type	name_init,@function
name_init:                              # @name_init
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$1, %edi
	movl	$6152, %esi             # imm = 0x1808
	movl	$.L.str, %edx
	callq	alloc
	movq	%rax, the_nt(%rip)
	xorl	%esi, %esi
	movl	$6152, %edx             # imm = 0x1808
	movq	%rax, %rdi
	callq	memset
	movq	the_nt(%rip), %r14
	movl	$0, 6144(%r14)
	movl	$1, %edi
	movl	$4096, %esi             # imm = 0x1000
	movl	$.L.str.5, %edx
	callq	alloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%rbx, %rdi
	callq	memset
	movl	6144(%r14), %eax
	shrl	$7, %eax
	movq	%rbx, 2048(%r14,%rax,8)
.LBB0_2:                                # %name_alloc_sub.exit
	movq	the_nt(%rip), %r14
	movl	$128, 6144(%r14)
	movl	$1, %edi
	movl	$4096, %esi             # imm = 0x1000
	movl	$.L.str.5, %edx
	callq	alloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_4
# BB#3:
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%rbx, %rdi
	callq	memset
	movl	6144(%r14), %eax
	shrl	$7, %eax
	movq	%rbx, 2048(%r14,%rax,8)
.LBB0_4:                                # %name_alloc_sub.exit.1
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	name_init, .Lfunc_end0-name_init
	.cfi_endproc

	.globl	name_alloc_sub
	.p2align	4, 0x90
	.type	name_alloc_sub,@function
name_alloc_sub:                         # @name_alloc_sub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$1, %edi
	movl	$4096, %esi             # imm = 0x1000
	movl	$.L.str.5, %edx
	callq	alloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%rbx, %rdi
	callq	memset
	movl	6144(%r14), %eax
	shrl	$7, %eax
	movq	%rbx, 2048(%r14,%rax,8)
	jmp	.LBB1_3
.LBB1_1:
	movl	$-25, %ebp
.LBB1_3:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	name_alloc_sub, .Lfunc_end1-name_alloc_sub
	.cfi_endproc

	.globl	name_ref
	.p2align	4, 0x90
	.type	name_ref,@function
name_ref:                               # @name_ref
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi17:
	.cfi_def_cfa_offset 80
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r13
	movl	%esi, %ebp
	movq	%rdi, %r15
	movzwl	%bp, %ebx
	cmpl	$1, %ebx
	jne	.LBB2_5
# BB#1:
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movq	the_nt(%rip), %rcx
	movzbl	(%r15), %eax
	movq	%rax, %rdx
	shrq	$7, %rdx
	movq	2048(%rcx,%rdx,8), %rcx
	movl	%eax, %edx
	andl	$127, %edx
	shlq	$5, %rdx
	leaq	(%rcx,%rdx), %r14
	cmpw	$0, 10(%rcx,%rdx)
	jne	.LBB2_24
# BB#2:
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	js	.LBB2_3
# BB#4:
	movw	%ax, 8(%rcx,%rdx)
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB2_20
	jmp	.LBB2_23
.LBB2_5:
	movq	%r13, 16(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	string_hash
	movq	the_nt(%rip), %r13
	movzbl	%al, %ecx
	movq	(%r13,%rcx,8), %rax
	testq	%rax, %rax
	je	.LBB2_6
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movq	%rax, %r12
	cmpw	%bp, 10(%r12)
	jne	.LBB2_10
# BB#8:                                 #   in Loop: Header=BB2_7 Depth=1
	movq	16(%r12), %rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB2_9
.LBB2_10:                               #   in Loop: Header=BB2_7 Depth=1
	movq	(%r12), %rax
	testq	%rax, %rax
	jne	.LBB2_7
	jmp	.LBB2_11
.LBB2_6:
	leaq	(%r13,%rcx,8), %r12
.LBB2_11:                               # %._crit_edge
	testl	%r14d, %r14d
	js	.LBB2_12
# BB#13:
	movl	6144(%r13), %eax
	testb	$127, %al
	je	.LBB2_15
# BB#14:
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	jmp	.LBB2_18
.LBB2_12:
	movl	$-21, %eax
	jmp	.LBB2_26
.LBB2_9:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r12, (%rax)
	movw	$28, 8(%rax)
	jmp	.LBB2_25
.LBB2_15:
	movl	$1, %edi
	movl	$4096, %esi             # imm = 0x1000
	movl	$.L.str.5, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB2_16
# BB#17:                                # %name_alloc_sub.exit.thread
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	movq	%rax, %rdi
	movq	%rax, %r14
	callq	memset
	movl	6144(%r13), %eax
	shrl	$7, %eax
	movq	%r14, 2048(%r13,%rax,8)
	movq	the_nt(%rip), %r13
	movl	6144(%r13), %eax
.LBB2_18:
	movl	%eax, %ecx
	shrl	$7, %ecx
	movq	2048(%r13,%rcx,8), %rcx
	movl	%eax, %edx
	andl	$127, %edx
	shlq	$5, %rdx
	leaq	(%rcx,%rdx), %r14
	leal	1(%rax), %esi
	movl	%esi, 6144(%r13)
	movw	%ax, 8(%rcx,%rdx)
	movq	%r14, (%r12)
	movq	16(%rsp), %r13          # 8-byte Reload
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB2_23
.LBB2_20:
	movl	$1, %esi
	movl	$.L.str.1, %edx
	movl	%ebx, %edi
	callq	alloc
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB2_21
# BB#22:
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	%r12, %r15
.LBB2_23:
	movw	%bp, 10(%r14)
	movq	%r15, 16(%r14)
	movq	$0, (%r14)
	movq	$0, 24(%r14)
.LBB2_24:                               # %.thread
	movq	%r14, (%r13)
	movw	$28, 8(%r13)
.LBB2_25:                               # %.thread
	xorl	%eax, %eax
.LBB2_26:                               # %.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_21:
	movl	$-25, %eax
	jmp	.LBB2_26
.LBB2_3:
	movl	$-21, %eax
	jmp	.LBB2_26
.LBB2_16:
	movl	$-25, %eax
	jmp	.LBB2_26
.Lfunc_end2:
	.size	name_ref, .Lfunc_end2-name_ref
	.cfi_endproc

	.globl	name_string_ref
	.p2align	4, 0x90
	.type	name_string_ref,@function
name_string_ref:                        # @name_string_ref
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	16(%rax), %rcx
	movq	%rcx, (%rsi)
	movw	$566, 8(%rsi)           # imm = 0x236
	movzwl	10(%rax), %eax
	movw	%ax, 10(%rsi)
	retq
.Lfunc_end3:
	.size	name_string_ref, .Lfunc_end3-name_string_ref
	.cfi_endproc

	.globl	name_enter
	.p2align	4, 0x90
	.type	name_enter,@function
name_enter:                             # @name_enter
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 32
.Lcfi27:
	.cfi_offset %rbx, -24
.Lcfi28:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	strlen
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movl	%eax, %esi
	movq	%rbx, %rdx
	callq	name_ref
	testl	%eax, %eax
	jne	.LBB4_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	movl	$.L.str.3, %edx
	movl	$135, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	name_enter, .Lfunc_end4-name_enter
	.cfi_endproc

	.globl	name_index_ref
	.p2align	4, 0x90
	.type	name_index_ref,@function
name_index_ref:                         # @name_index_ref
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	movq	the_nt(%rip), %rax
	movl	%edi, %ecx
	shrl	$7, %ecx
	andl	$127, %edi
	shlq	$5, %rdi
	addq	2048(%rax,%rcx,8), %rdi
	movq	%rdi, (%rsi)
	movw	$28, 8(%rsi)
	retq
.Lfunc_end5:
	.size	name_index_ref, .Lfunc_end5-name_index_ref
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"name_init"
	.size	.L.str, 10

	.type	the_nt,@object          # @the_nt
	.comm	the_nt,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"name_ref(string)"
	.size	.L.str.1, 17

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s(%d): "
	.size	.L.str.2, 9

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/MallocBench/gs/iname.c"
	.size	.L.str.3, 84

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"name_enter failed - %s"
	.size	.L.str.4, 23

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"name_alloc_sub"
	.size	.L.str.5, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
