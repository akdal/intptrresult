	.text
	.file	"stream.bc"
	.globl	sread_string
	.p2align	4, 0x90
	.type	sread_string,@function
sread_string:                           # @sread_string
	.cfi_startproc
# BB#0:
	movq	%rsi, 16(%rdi)
	leaq	-1(%rsi), %rax
	movq	%rax, (%rdi)
	movl	%edx, %eax
	leaq	-1(%rsi,%rax), %rax
	movq	%rax, 8(%rdi)
	movl	%edx, 24(%rdi)
	movb	$1, 29(%rdi)
	movq	$0, 32(%rdi)
	movups	sread_string.p+32(%rip), %xmm0
	movups	%xmm0, 72(%rdi)
	movups	sread_string.p+16(%rip), %xmm0
	movups	%xmm0, 56(%rdi)
	movups	sread_string.p(%rip), %xmm0
	movups	%xmm0, 40(%rdi)
	movb	$0, 28(%rdi)
	retq
.Lfunc_end0:
	.size	sread_string, .Lfunc_end0-sread_string
	.cfi_endproc

	.globl	ssread
	.p2align	4, 0x90
	.type	ssread,@function
ssread:                                 # @ssread
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movq	%rax, (%rdi)
	movl	$-1, %eax
	retq
.Lfunc_end1:
	.size	ssread, .Lfunc_end1-ssread
	.cfi_endproc

	.globl	ssavailable
	.p2align	4, 0x90
	.type	ssavailable,@function
ssavailable:                            # @ssavailable
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	subq	(%rdi), %rax
	movq	$-1, %rcx
	cmovneq	%rax, %rcx
	movq	%rcx, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	ssavailable, .Lfunc_end2-ssavailable
	.cfi_endproc

	.globl	ssseek
	.p2align	4, 0x90
	.type	ssseek,@function
ssseek:                                 # @ssseek
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	testq	%rsi, %rsi
	js	.LBB3_3
# BB#1:
	movl	24(%rdi), %ecx
	cmpq	%rsi, %rcx
	jl	.LBB3_3
# BB#2:
	movq	16(%rdi), %rax
	leaq	-1(%rax,%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
.LBB3_3:
	retq
.Lfunc_end3:
	.size	ssseek, .Lfunc_end3-ssseek
	.cfi_endproc

	.globl	snull
	.p2align	4, 0x90
	.type	snull,@function
snull:                                  # @snull
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end4:
	.size	snull, .Lfunc_end4-snull
	.cfi_endproc

	.globl	ssinit
	.p2align	4, 0x90
	.type	ssinit,@function
ssinit:                                 # @ssinit
	.cfi_startproc
# BB#0:
	movq	%rsi, 16(%rdi)
	leaq	-1(%rsi), %rax
	movq	%rax, (%rdi)
	movl	%edx, %eax
	leaq	-1(%rsi,%rax), %rax
	movq	%rax, 8(%rdi)
	movl	%edx, 24(%rdi)
	movb	$1, 29(%rdi)
	movq	$0, 32(%rdi)
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	32(%rcx), %xmm2
	movups	%xmm2, 72(%rdi)
	movups	%xmm1, 56(%rdi)
	movups	%xmm0, 40(%rdi)
	retq
.Lfunc_end5:
	.size	ssinit, .Lfunc_end5-ssinit
	.cfi_endproc

	.globl	swrite_string
	.p2align	4, 0x90
	.type	swrite_string,@function
swrite_string:                          # @swrite_string
	.cfi_startproc
# BB#0:
	movq	%rsi, 16(%rdi)
	leaq	-1(%rsi), %rax
	movq	%rax, (%rdi)
	movl	%edx, %eax
	leaq	-1(%rsi,%rax), %rax
	movq	%rax, 8(%rdi)
	movl	%edx, 24(%rdi)
	movb	$1, 29(%rdi)
	movq	$0, 32(%rdi)
	movups	swrite_string.p+32(%rip), %xmm0
	movups	%xmm0, 72(%rdi)
	movups	swrite_string.p+16(%rip), %xmm0
	movups	%xmm0, 56(%rdi)
	movups	swrite_string.p(%rip), %xmm0
	movups	%xmm0, 40(%rdi)
	movb	$1, 28(%rdi)
	retq
.Lfunc_end6:
	.size	swrite_string, .Lfunc_end6-swrite_string
	.cfi_endproc

	.globl	sswrite
	.p2align	4, 0x90
	.type	sswrite,@function
sswrite:                                # @sswrite
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movq	%rax, (%rdi)
	movl	$-1, %eax
	retq
.Lfunc_end7:
	.size	sswrite, .Lfunc_end7-sswrite
	.cfi_endproc

	.globl	snoavailable
	.p2align	4, 0x90
	.type	snoavailable,@function
snoavailable:                           # @snoavailable
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	retq
.Lfunc_end8:
	.size	snoavailable, .Lfunc_end8-snoavailable
	.cfi_endproc

	.globl	sread_file
	.p2align	4, 0x90
	.type	sread_file,@function
sread_file:                             # @sread_file
	.cfi_startproc
# BB#0:
	movq	%rdx, 16(%rdi)
	decq	%rdx
	movq	%rdx, (%rdi)
	movl	%ecx, 24(%rdi)
	movq	%rsi, 96(%rdi)
	movq	%rdx, 8(%rdi)
	movb	$0, 29(%rdi)
	movups	sread_file.p+32(%rip), %xmm0
	movups	%xmm0, 72(%rdi)
	movups	sread_file.p+16(%rip), %xmm0
	movups	%xmm0, 56(%rdi)
	movups	sread_file.p(%rip), %xmm0
	movups	%xmm0, 40(%rdi)
	movb	$0, 28(%rdi)
	xorl	%eax, %eax
	cmpq	%rsi, stdin(%rip)
	movq	$-1, %rcx
	cmovneq	%rax, %rcx
	movq	%rcx, 32(%rdi)
	retq
.Lfunc_end9:
	.size	sread_file, .Lfunc_end9-sread_file
	.cfi_endproc

	.globl	sfread
	.p2align	4, 0x90
	.type	sfread,@function
sfread:                                 # @sfread
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpb	$0, 29(%rbx)
	je	.LBB10_2
# BB#1:
	movq	8(%rbx), %rax
	movq	%rax, (%rbx)
.LBB10_8:
	movl	$-1, %eax
	jmp	.LBB10_9
.LBB10_2:
	cmpq	$0, 32(%rbx)
	js	.LBB10_4
# BB#3:
	movq	96(%rbx), %rdi
	callq	ftell
	movq	%rax, 32(%rbx)
.LBB10_4:                               # %._crit_edge
	movq	16(%rbx), %rdi
	movl	24(%rbx), %edx
	movq	96(%rbx), %rcx
	movl	$1, %esi
	callq	fread
	movq	%rax, %r14
	movq	16(%rbx), %rax
	decq	%rax
	movq	%rax, (%rbx)
	movq	96(%rbx), %rdi
	callq	feof
	movb	%al, 29(%rbx)
	testl	%r14d, %r14d
	jle	.LBB10_6
# BB#5:
	movq	(%rbx), %rax
	movslq	%r14d, %rcx
	addq	%rax, %rcx
	movq	%rcx, 8(%rbx)
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB10_9
.LBB10_6:
	movq	(%rbx), %rax
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	testl	%r14d, %r14d
	je	.LBB10_7
.LBB10_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB10_7:
	movb	$1, 29(%rbx)
	jmp	.LBB10_8
.Lfunc_end10:
	.size	sfread, .Lfunc_end10-sfread
	.cfi_endproc

	.globl	sfavailable
	.p2align	4, 0x90
	.type	sfavailable,@function
sfavailable:                            # @sfavailable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -48
.Lcfi11:
	.cfi_offset %r12, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	subq	(%rbx), %rax
	movq	%rax, (%r14)
	cmpq	$0, 32(%rbx)
	js	.LBB11_4
# BB#1:
	movq	96(%rbx), %rdi
	callq	ftell
	movq	%rax, %r15
	movq	96(%rbx), %rdi
	xorl	%esi, %esi
	movl	$2, %edx
	callq	fseek
	movl	$-1, %ebp
	testl	%eax, %eax
	jne	.LBB11_7
# BB#2:
	movq	96(%rbx), %rdi
	callq	ftell
	movq	%rax, %r12
	movq	96(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	callq	fseek
	testl	%eax, %eax
	jne	.LBB11_7
# BB#3:                                 # %.critedge
	subq	%r15, %r12
	addq	(%r14), %r12
	cmpq	$1, %r12
	sbbq	%rax, %rax
	orq	%r12, %rax
	movq	%rax, (%r14)
	xorl	%ebp, %ebp
	jmp	.LBB11_7
.LBB11_4:
	xorl	%ebp, %ebp
	testq	%rax, %rax
	jne	.LBB11_7
# BB#5:
	movq	96(%rbx), %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB11_7
# BB#6:
	movq	$-1, (%r14)
.LBB11_7:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	sfavailable, .Lfunc_end11-sfavailable
	.cfi_endproc

	.globl	srseek
	.p2align	4, 0x90
	.type	srseek,@function
srseek:                                 # @srseek
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 16
.Lcfi16:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rsi, %rax
	subq	32(%rbx), %rax
	js	.LBB12_3
# BB#1:
	movq	16(%rbx), %rcx
	movl	$1, %edx
	subl	%ecx, %edx
	addl	8(%rbx), %edx
	cmpq	%rdx, %rax
	jle	.LBB12_2
.LBB12_3:
	movq	96(%rbx), %rdi
	xorl	%edx, %edx
	callq	fseek
	movl	%eax, %ecx
	movl	$-1, %eax
	testl	%ecx, %ecx
	jne	.LBB12_6
# BB#4:
	movq	16(%rbx), %rax
	decq	%rax
	movq	%rax, (%rbx)
	movq	%rax, 8(%rbx)
	movb	$0, 29(%rbx)
	jmp	.LBB12_5
.LBB12_2:
	leaq	-1(%rcx,%rax), %rax
	movq	%rax, (%rbx)
.LBB12_5:
	xorl	%eax, %eax
.LBB12_6:
	popq	%rbx
	retq
.Lfunc_end12:
	.size	srseek, .Lfunc_end12-srseek
	.cfi_endproc

	.globl	srclose
	.p2align	4, 0x90
	.type	srclose,@function
srclose:                                # @srclose
	.cfi_startproc
# BB#0:
	movq	96(%rdi), %rdi
	jmp	fclose                  # TAILCALL
.Lfunc_end13:
	.size	srclose, .Lfunc_end13-srclose
	.cfi_endproc

	.globl	sfinit
	.p2align	4, 0x90
	.type	sfinit,@function
sfinit:                                 # @sfinit
	.cfi_startproc
# BB#0:
	movq	%rdx, 16(%rdi)
	decq	%rdx
	movq	%rdx, (%rdi)
	movl	%ecx, 24(%rdi)
	movq	%rsi, 96(%rdi)
	movq	%rdx, 8(%rdi)
	movb	$0, 29(%rdi)
	movups	(%r8), %xmm0
	movups	16(%r8), %xmm1
	movups	32(%r8), %xmm2
	movups	%xmm2, 72(%rdi)
	movups	%xmm1, 56(%rdi)
	movups	%xmm0, 40(%rdi)
	retq
.Lfunc_end14:
	.size	sfinit, .Lfunc_end14-sfinit
	.cfi_endproc

	.globl	swrite_file
	.p2align	4, 0x90
	.type	swrite_file,@function
swrite_file:                            # @swrite_file
	.cfi_startproc
# BB#0:
	movq	%rdx, 16(%rdi)
	decq	%rdx
	movq	%rdx, (%rdi)
	movl	%ecx, 24(%rdi)
	movq	%rsi, 96(%rdi)
	movq	%rdx, 8(%rdi)
	movb	$0, 29(%rdi)
	movups	swrite_file.p+32(%rip), %xmm0
	movups	%xmm0, 72(%rdi)
	movups	swrite_file.p+16(%rip), %xmm0
	movups	%xmm0, 56(%rdi)
	movups	swrite_file.p(%rip), %xmm0
	movups	%xmm0, 40(%rdi)
	movb	$1, 28(%rdi)
	cmpq	%rsi, stdout(%rip)
	sete	%al
	cmpq	%rsi, stderr(%rip)
	sete	%cl
	orb	%al, %cl
	movzbl	%cl, %eax
	negq	%rax
	movq	%rax, 32(%rdi)
	retq
.Lfunc_end15:
	.size	swrite_file, .Lfunc_end15-swrite_file
	.cfi_endproc

	.globl	sfwrite
	.p2align	4, 0x90
	.type	sfwrite,@function
sfwrite:                                # @sfwrite
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movl	(%rbx), %edx
	incl	%edx
	subl	%edi, %edx
	movq	96(%rbx), %rcx
	movl	$1, %esi
	callq	fwrite
	cmpq	$0, 32(%rbx)
	js	.LBB16_2
# BB#1:
	movq	96(%rbx), %rdi
	callq	ftell
	movq	%rax, 32(%rbx)
.LBB16_2:
	movq	16(%rbx), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rbx)
	movl	24(%rbx), %ecx
	testq	%rcx, %rcx
	leaq	-1(%rax,%rcx), %rcx
	movq	%rcx, 8(%rbx)
	je	.LBB16_4
# BB#3:
	movq	%rax, (%rbx)
	movb	%bpl, (%rax)
	movzbl	%bpl, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB16_4:
	movq	48(%rbx), %rax
	movzbl	%bpl, %esi
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end16:
	.size	sfwrite, .Lfunc_end16-sfwrite
	.cfi_endproc

	.globl	swseek
	.p2align	4, 0x90
	.type	swseek,@function
swseek:                                 # @swseek
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	retq
.Lfunc_end17:
	.size	swseek, .Lfunc_end17-swseek
	.cfi_endproc

	.globl	swflush
	.p2align	4, 0x90
	.type	swflush,@function
swflush:                                # @swflush
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movl	(%rbx), %edx
	incl	%edx
	subl	%edi, %edx
	movq	96(%rbx), %rcx
	movl	$1, %esi
	callq	fwrite
	movq	%rax, %r14
	movq	96(%rbx), %rdi
	callq	fflush
	movq	96(%rbx), %rdi
	callq	ftell
	movq	%rax, 32(%rbx)
	movq	16(%rbx), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%rbx)
	movl	24(%rbx), %ecx
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 8(%rbx)
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	swflush, .Lfunc_end18-swflush
	.cfi_endproc

	.globl	swclose
	.p2align	4, 0x90
	.type	swclose,@function
swclose:                                # @swclose
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 16
.Lcfi28:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movl	(%rbx), %edx
	incl	%edx
	subl	%edi, %edx
	movq	96(%rbx), %rcx
	movl	$1, %esi
	callq	fwrite
	movq	96(%rbx), %rdi
	popq	%rbx
	jmp	fclose                  # TAILCALL
.Lfunc_end19:
	.size	swclose, .Lfunc_end19-swclose
	.cfi_endproc

	.globl	sread_decrypt
	.p2align	4, 0x90
	.type	sread_decrypt,@function
sread_decrypt:                          # @sread_decrypt
	.cfi_startproc
# BB#0:
	movq	%rdx, 16(%rdi)
	decq	%rdx
	movq	%rdx, (%rdi)
	movl	%ecx, 24(%rdi)
	movq	$0, 96(%rdi)
	movq	%rdx, 8(%rdi)
	movb	$0, 29(%rdi)
	movups	sread_decrypt.p+32(%rip), %xmm0
	movups	%xmm0, 72(%rdi)
	movups	sread_decrypt.p+16(%rip), %xmm0
	movups	%xmm0, 56(%rdi)
	movups	sread_decrypt.p(%rip), %xmm0
	movups	%xmm0, 40(%rdi)
	movb	$0, 28(%rdi)
	movq	$-1, 32(%rdi)
	movq	%rsi, 104(%rdi)
	movw	%r8w, 112(%rdi)
	movl	$-1, 116(%rdi)
	retq
.Lfunc_end20:
	.size	sread_decrypt, .Lfunc_end20-sread_decrypt
	.cfi_endproc

	.globl	sxread
	.p2align	4, 0x90
	.type	sxread,@function
sxread:                                 # @sxread
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 208
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	movq	16(%rbp), %rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-1(%rcx), %rax
	movq	%rax, (%rbp)
	movl	24(%rbp), %r15d
	testl	%r15d, %r15d
	je	.LBB21_14
# BB#1:                                 # %.lr.ph.i.lr.ph
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	32(%rsp), %r12
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	jmp	.LBB21_2
	.p2align	4, 0x90
.LBB21_15:                              #   in Loop: Header=BB21_2 Depth=1
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r13, 48(%rsp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%rsp)
	movl	%r15d, %r14d
	leaq	(%rax,%r14), %rax
	movq	%rax, 40(%rsp)
	movl	%r15d, 56(%rsp)
	movb	$1, 61(%rsp)
	movq	$0, 64(%rsp)
	movups	sread_string.p+32(%rip), %xmm0
	leaq	72(%rsp), %rax
	movups	%xmm0, 32(%rax)
	movups	sread_string.p+16(%rip), %xmm0
	movups	%xmm0, 16(%rax)
	movups	sread_string.p(%rip), %xmm0
	movups	%xmm0, (%rax)
	movb	$0, 60(%rsp)
	movl	116(%rbp), %ebx
	movl	%ebx, %eax
	andl	$240, %eax
	cmpl	$15, %eax
	ja	.LBB21_20
# BB#16:                                # %..loopexit_crit_edge.i
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %r13
.LBB21_17:                              # %.loopexit.i
                                        #   in Loop: Header=BB21_2 Depth=1
	leaq	(%rax,%r14), %rbp
	.p2align	4, 0x90
.LBB21_18:                              #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rsp), %rax
	cmpq	40(%rsp), %rax
	jae	.LBB21_27
# BB#19:                                #   in Loop: Header=BB21_18 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movzbl	1(%rax), %eax
	jmp	.LBB21_28
	.p2align	4, 0x90
.LBB21_27:                              #   in Loop: Header=BB21_18 Depth=2
	movq	%r12, %rdi
	callq	*72(%rsp)
.LBB21_28:                              #   in Loop: Header=BB21_18 Depth=2
	cltq
	movzbl	scan_char_array+1(%rax), %eax
	cmpb	$16, %al
	jb	.LBB21_32
# BB#29:                                #   in Loop: Header=BB21_18 Depth=2
	cmpb	$103, %al
	jne	.LBB21_18
# BB#30:                                #   in Loop: Header=BB21_2 Depth=1
	movzbl	%bl, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB21_31
	.p2align	4, 0x90
.LBB21_32:                              #   in Loop: Header=BB21_2 Depth=1
	shlb	$4, %bl
	addb	%bl, %al
	movb	%al, (%r13)
	incq	%r13
	cmpq	%rbp, %r13
	movq	16(%rsp), %rbp          # 8-byte Reload
	jae	.LBB21_33
	.p2align	4, 0x90
.LBB21_20:                              #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	32(%rsp), %rax
	cmpq	40(%rsp), %rax
	jae	.LBB21_22
# BB#21:                                #   in Loop: Header=BB21_20 Depth=2
	leaq	1(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movzbl	1(%rax), %eax
	jmp	.LBB21_23
	.p2align	4, 0x90
.LBB21_22:                              #   in Loop: Header=BB21_20 Depth=2
	movq	%r12, %rdi
	callq	*72(%rsp)
.LBB21_23:                              #   in Loop: Header=BB21_20 Depth=2
	cltq
	movb	scan_char_array+1(%rax), %bl
	cmpb	$16, %bl
	jb	.LBB21_24
# BB#25:                                #   in Loop: Header=BB21_20 Depth=2
	cmpb	$103, %bl
	jne	.LBB21_20
# BB#26:                                #   in Loop: Header=BB21_2 Depth=1
	movl	$-1, %eax
.LBB21_31:                              # %.loopexit39.i
                                        #   in Loop: Header=BB21_2 Depth=1
	movl	%eax, 116(%rbp)
	subl	8(%rsp), %r13d          # 4-byte Folded Reload
	movl	%r13d, %r15d
.LBB21_33:                              # %sreadhex.exit
                                        #   in Loop: Header=BB21_2 Depth=1
	testl	%r15d, %r15d
	jne	.LBB21_35
# BB#34:                                # %.thread
                                        #   in Loop: Header=BB21_2 Depth=1
	movl	24(%rbp), %r15d
	testl	%r15d, %r15d
	jne	.LBB21_2
	jmp	.LBB21_13
	.p2align	4, 0x90
.LBB21_24:                              #   in Loop: Header=BB21_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	jmp	.LBB21_17
	.p2align	4, 0x90
.LBB21_2:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_3 Depth 2
                                        #     Child Loop BB21_18 Depth 2
                                        #     Child Loop BB21_20 Depth 2
	movq	104(%rbp), %rbx
	movl	%r15d, %r13d
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB21_3:                               #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rsi
	movq	8(%rbx), %rax
	movl	%eax, %r14d
	subl	%esi, %r14d
	je	.LBB21_5
# BB#4:                                 #   in Loop: Header=BB21_3 Depth=2
	cmpl	%r13d, %r14d
	cmoval	%r13d, %r14d
	incq	%rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	addq	%r14, (%rbx)
	addq	%r14, %rbp
	subl	%r14d, %r13d
	jmp	.LBB21_10
	.p2align	4, 0x90
.LBB21_5:                               #   in Loop: Header=BB21_3 Depth=2
	cmpq	%rax, %rsi
	jae	.LBB21_7
# BB#6:                                 #   in Loop: Header=BB21_3 Depth=2
	leaq	1(%rsi), %rax
	movq	%rax, (%rbx)
	movzbl	1(%rsi), %eax
	cmpb	$0, 29(%rbx)
	je	.LBB21_9
	jmp	.LBB21_11
.LBB21_7:                               #   in Loop: Header=BB21_3 Depth=2
	movq	%rbx, %rdi
	callq	*40(%rbx)
	cmpb	$0, 29(%rbx)
	jne	.LBB21_11
.LBB21_9:                               # %.thread.i
                                        #   in Loop: Header=BB21_3 Depth=2
	movb	%al, (%rbp)
	incq	%rbp
	decl	%r13d
.LBB21_10:                              # %.thread48.backedge.i
                                        #   in Loop: Header=BB21_3 Depth=2
	testl	%r13d, %r13d
	jne	.LBB21_3
	jmp	.LBB21_12
	.p2align	4, 0x90
.LBB21_11:                              #   in Loop: Header=BB21_2 Depth=1
	subl	%r13d, %r15d
.LBB21_12:                              # %sgets.exit
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	testl	%r15d, %r15d
	jne	.LBB21_15
.LBB21_13:                              # %sgets.exit.thread.loopexit
	movq	(%rbp), %rax
.LBB21_14:                              # %sgets.exit.thread
	movq	%rax, 8(%rbp)
	movb	$1, 29(%rbp)
	movl	$-1, %eax
	jmp	.LBB21_36
.LBB21_35:
	leaq	112(%rbp), %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rdi, %rsi
	movl	%r15d, %edx
	callq	gs_type1_decrypt
	movq	(%rbp), %rax
	movl	%r15d, %ecx
	addq	%rax, %rcx
	movq	%rcx, 8(%rbp)
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	1(%rax), %eax
.LBB21_36:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	sxread, .Lfunc_end21-sxread
	.cfi_endproc

	.globl	sxavailable
	.p2align	4, 0x90
	.type	sxavailable,@function
sxavailable:                            # @sxavailable
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	104(%rdi), %rdi
	callq	*56(%rdi)
	testl	%eax, %eax
	js	.LBB22_1
# BB#2:
	movq	(%rbx), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	js	.LBB22_4
# BB#3:
	shrq	%rcx
	movq	%rcx, (%rbx)
.LBB22_4:
	popq	%rbx
	retq
.LBB22_1:
	movl	$-1, %eax
	popq	%rbx
	retq
.Lfunc_end22:
	.size	sxavailable, .Lfunc_end22-sxavailable
	.cfi_endproc

	.globl	sgets
	.p2align	4, 0x90
	.type	sgets,@function
sgets:                                  # @sgets
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 48
.Lcfi49:
	.cfi_offset %rbx, -48
.Lcfi50:
	.cfi_offset %r12, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %r12
	testl	%r14d, %r14d
	je	.LBB23_1
# BB#2:                                 # %.lr.ph
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB23_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rsi
	movq	8(%r12), %rax
	movl	%eax, %ebx
	subl	%esi, %ebx
	je	.LBB23_5
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpl	%ebp, %ebx
	cmoval	%ebp, %ebx
	incq	%rsi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, (%r12)
	addq	%rbx, %r15
	subl	%ebx, %ebp
	jmp	.LBB23_10
	.p2align	4, 0x90
.LBB23_5:                               #   in Loop: Header=BB23_3 Depth=1
	cmpq	%rax, %rsi
	jae	.LBB23_7
# BB#6:                                 #   in Loop: Header=BB23_3 Depth=1
	leaq	1(%rsi), %rax
	movq	%rax, (%r12)
	movzbl	1(%rsi), %eax
	cmpb	$0, 29(%r12)
	je	.LBB23_9
	jmp	.LBB23_11
.LBB23_7:                               #   in Loop: Header=BB23_3 Depth=1
	movq	%r12, %rdi
	callq	*40(%r12)
	cmpb	$0, 29(%r12)
	jne	.LBB23_11
.LBB23_9:                               # %.thread
                                        #   in Loop: Header=BB23_3 Depth=1
	movb	%al, (%r15)
	incq	%r15
	decl	%ebp
.LBB23_10:                              # %.thread48.backedge
                                        #   in Loop: Header=BB23_3 Depth=1
	testl	%ebp, %ebp
	jne	.LBB23_3
	jmp	.LBB23_12
.LBB23_1:
	xorl	%r14d, %r14d
	jmp	.LBB23_12
.LBB23_11:
	subl	%ebp, %r14d
.LBB23_12:                              # %.loopexit
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	sgets, .Lfunc_end23-sgets
	.cfi_endproc

	.globl	sreadhex
	.p2align	4, 0x90
	.type	sreadhex,@function
sreadhex:                               # @sreadhex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 96
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movl	%edx, %ebp
	movq	%rdi, %rbx
	xorl	%eax, %eax
	testl	%ebp, %ebp
	je	.LBB24_1
# BB#2:
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	(%r8), %r14d
	movl	%r14d, %eax
	andl	$240, %eax
	cmpl	$15, %eax
	movq	%r8, 32(%rsp)           # 8-byte Spill
	ja	.LBB24_7
# BB#3:                                 # %..loopexit_crit_edge
	movq	%r13, 16(%rsp)          # 8-byte Spill
	leaq	8(%rbx), %r15
	leaq	40(%rbx), %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB24_4
.LBB24_1:
	xorl	%ebp, %ebp
	jmp	.LBB24_23
.LBB24_7:                               # %..preheader_crit_edge
	leaq	8(%rbx), %r15
	leaq	40(%rbx), %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	jmp	.LBB24_8
.LBB24_4:                               # %.loopexit
	movl	%ebp, %r13d
	addq	%rax, %r13
	.p2align	4, 0x90
.LBB24_5:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	cmpq	(%r15), %rax
	jae	.LBB24_16
# BB#6:                                 #   in Loop: Header=BB24_5 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB24_17
	.p2align	4, 0x90
.LBB24_16:                              #   in Loop: Header=BB24_5 Depth=1
	movq	%rbx, %rdi
	callq	*(%r12)
.LBB24_17:                              #   in Loop: Header=BB24_5 Depth=1
	cltq
	movzbl	scan_char_array+1(%rax), %eax
	cmpb	$16, %al
	jb	.LBB24_21
# BB#18:                                #   in Loop: Header=BB24_5 Depth=1
	cmpb	$103, %al
	jne	.LBB24_5
# BB#19:
	movzbl	%r14b, %eax
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB24_20
.LBB24_21:
	shlb	$4, %r14b
	addb	%r14b, %al
	movq	24(%rsp), %rcx          # 8-byte Reload
	movb	%al, (%rcx)
	movq	%rcx, %rax
	incq	%rax
	cmpq	%r13, %rax
	movq	16(%rsp), %r13          # 8-byte Reload
	jae	.LBB24_22
.LBB24_8:                               # %.preheader
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB24_9:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	cmpq	(%r15), %rax
	jae	.LBB24_11
# BB#10:                                #   in Loop: Header=BB24_9 Depth=1
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	1(%rax), %eax
	jmp	.LBB24_12
	.p2align	4, 0x90
.LBB24_11:                              #   in Loop: Header=BB24_9 Depth=1
	movq	%rbx, %rdi
	callq	*(%r12)
.LBB24_12:                              #   in Loop: Header=BB24_9 Depth=1
	cltq
	movb	scan_char_array+1(%rax), %r14b
	cmpb	$16, %r14b
	jb	.LBB24_13
# BB#14:                                #   in Loop: Header=BB24_9 Depth=1
	cmpb	$103, %r14b
	jne	.LBB24_9
# BB#15:
	movl	$-1, %eax
.LBB24_20:                              # %.loopexit39
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movq	24(%rsp), %rcx          # 8-byte Reload
	subl	8(%rsp), %ecx           # 4-byte Folded Reload
	movl	$1, %eax
	movl	%ecx, %ebp
	jmp	.LBB24_23
.LBB24_13:
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	jmp	.LBB24_4
.LBB24_22:
	xorl	%eax, %eax
.LBB24_23:
	movl	%ebp, (%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	sreadhex, .Lfunc_end24-sreadhex
	.cfi_endproc

	.globl	sungetc
	.p2align	4, 0x90
	.type	sungetc,@function
sungetc:                                # @sungetc
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	cmpb	$0, 28(%rdi)
	jne	.LBB25_3
# BB#1:
	movq	(%rdi), %rcx
	cmpq	16(%rdi), %rcx
	jb	.LBB25_3
# BB#2:
	leaq	-1(%rcx), %rax
	movq	%rax, (%rdi)
	movb	%sil, (%rcx)
	xorl	%eax, %eax
.LBB25_3:
	retq
.Lfunc_end25:
	.size	sungetc, .Lfunc_end25-sungetc
	.cfi_endproc

	.globl	sputs
	.p2align	4, 0x90
	.type	sputs,@function
sputs:                                  # @sputs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 48
.Lcfi72:
	.cfi_offset %rbx, -48
.Lcfi73:
	.cfi_offset %r12, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %r12
	testl	%r14d, %r14d
	je	.LBB26_1
# BB#2:                                 # %.lr.ph
	movl	%r14d, %ebp
	.p2align	4, 0x90
.LBB26_3:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rdi
	movq	8(%r12), %rax
	movl	%eax, %ebx
	subl	%edi, %ebx
	je	.LBB26_6
# BB#4:                                 #   in Loop: Header=BB26_3 Depth=1
	cmpl	%ebp, %ebx
	cmoval	%ebp, %ebx
	incq	%rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	addq	%rbx, (%r12)
	addq	%rbx, %r15
	subl	%ebx, %ebp
	jmp	.LBB26_5
	.p2align	4, 0x90
.LBB26_6:                               #   in Loop: Header=BB26_3 Depth=1
	movzbl	(%r15), %ecx
	cmpq	%rax, %rdi
	jae	.LBB26_8
# BB#7:                                 #   in Loop: Header=BB26_3 Depth=1
	leaq	1(%rdi), %rax
	movq	%rax, (%r12)
	movb	%cl, 1(%rdi)
	cmpb	$0, 29(%r12)
	je	.LBB26_10
	jmp	.LBB26_11
.LBB26_8:                               #   in Loop: Header=BB26_3 Depth=1
	movzbl	%cl, %esi
	movq	%r12, %rdi
	callq	*48(%r12)
	cmpb	$0, 29(%r12)
	jne	.LBB26_11
.LBB26_10:                              #   in Loop: Header=BB26_3 Depth=1
	incq	%r15
	decl	%ebp
.LBB26_5:                               # %.thread.backedge
                                        #   in Loop: Header=BB26_3 Depth=1
	testl	%ebp, %ebp
	jne	.LBB26_3
	jmp	.LBB26_12
.LBB26_1:
	xorl	%r14d, %r14d
	jmp	.LBB26_12
.LBB26_11:                              # %._crit_edge
	subl	%ebp, %r14d
.LBB26_12:                              # %.thread._crit_edge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	sputs, .Lfunc_end26-sputs
	.cfi_endproc

	.type	sread_string.p,@object  # @sread_string.p
	.data
	.p2align	3
sread_string.p:
	.quad	ssread
	.quad	0
	.quad	ssavailable
	.quad	ssseek
	.quad	snull
	.quad	snull
	.size	sread_string.p, 48

	.type	swrite_string.p,@object # @swrite_string.p
	.p2align	3
swrite_string.p:
	.quad	0
	.quad	sswrite
	.quad	snoavailable
	.quad	ssseek
	.quad	snull
	.quad	snull
	.size	swrite_string.p, 48

	.type	sread_file.p,@object    # @sread_file.p
	.p2align	3
sread_file.p:
	.quad	sfread
	.quad	0
	.quad	sfavailable
	.quad	srseek
	.quad	snull
	.quad	srclose
	.size	sread_file.p, 48

	.type	swrite_file.p,@object   # @swrite_file.p
	.p2align	3
swrite_file.p:
	.quad	0
	.quad	sfwrite
	.quad	snoavailable
	.quad	swseek
	.quad	swflush
	.quad	swclose
	.size	swrite_file.p, 48

	.type	sread_decrypt.p,@object # @sread_decrypt.p
	.p2align	3
sread_decrypt.p:
	.quad	sxread
	.quad	0
	.quad	sxavailable
	.quad	0
	.quad	snull
	.quad	snull
	.size	sread_decrypt.p, 48


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
