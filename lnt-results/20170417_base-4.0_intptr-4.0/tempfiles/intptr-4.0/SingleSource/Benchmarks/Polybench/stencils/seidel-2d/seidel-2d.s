	.text
	.file	"seidel-2d.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4611686018427387904     # double 2
.LCPI6_1:
	.quad	4652007308841189376     # double 1000
.LCPI6_2:
	.quad	4621256167635550208     # double 9
.LCPI6_4:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_3:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -48
.Lcfi8:
	.cfi_offset %r12, -40
.Lcfi9:
	.cfi_offset %r13, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_38
# BB#1:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_38
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, 8(%rsp)
	leaq	8(%rsp), %rdi
	movl	$32, %esi
	movl	$8000000, %edx          # imm = 0x7A1200
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_38
# BB#3:                                 # %polybench_alloc_data.exit
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_38
# BB#4:                                 # %polybench_alloc_data.exit25
	leaq	8(%r14), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_0(%rip), %xmm8   # xmm8 = mem[0],zero
	movsd	.LCPI6_1(%rip), %xmm9   # xmm9 = mem[0],zero
	.p2align	4, 0x90
.LBB6_5:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_6 Depth 2
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	$-1000, %rdx            # imm = 0xFC18
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_6:                                #   Parent Loop BB6_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1002(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm8, %xmm1
	divsd	%xmm9, %xmm1
	movsd	%xmm1, -8(%rsi)
	leal	1003(%rdx), %edi
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm8, %xmm1
	divsd	%xmm9, %xmm1
	movsd	%xmm1, (%rsi)
	addq	$16, %rsi
	addq	$2, %rdx
	jne	.LBB6_6
# BB#7:                                 #   in Loop: Header=BB6_5 Depth=1
	incq	%rcx
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_5
# BB#8:                                 # %.preheader1.i.preheader
	leaq	16016(%r14), %r8
	xorl	%ecx, %ecx
	movsd	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_9:                                # %.preheader1.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_10 Depth 2
                                        #       Child Loop BB6_11 Depth 3
	movq	%r8, %rax
	movl	$1, %esi
	.p2align	4, 0x90
.LBB6_10:                               # %.preheader.i26
                                        #   Parent Loop BB6_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_11 Depth 3
	imulq	$8000, %rsi, %rdx       # imm = 0x1F40
	incq	%rsi
	movsd	-8000(%r14,%rdx), %xmm3 # xmm3 = mem[0],zero
	movsd	-7992(%r14,%rdx), %xmm5 # xmm5 = mem[0],zero
	movsd	(%r14,%rdx), %xmm2      # xmm2 = mem[0],zero
	movsd	8(%r14,%rdx), %xmm4     # xmm4 = mem[0],zero
	movsd	8000(%r14,%rdx), %xmm7  # xmm7 = mem[0],zero
	movsd	8008(%r14,%rdx), %xmm1  # xmm1 = mem[0],zero
	movl	$998, %edi              # imm = 0x3E6
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB6_11:                               #   Parent Loop BB6_9 Depth=1
                                        #     Parent Loop BB6_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movapd	%xmm5, %xmm6
	addsd	%xmm6, %xmm3
	movsd	-16000(%rdx), %xmm5     # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm3
	addsd	%xmm2, %xmm3
	addsd	%xmm4, %xmm3
	movsd	-8000(%rdx), %xmm4      # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm3
	addsd	%xmm7, %xmm3
	addsd	%xmm1, %xmm3
	movapd	%xmm1, %xmm7
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm3
	divsd	%xmm0, %xmm3
	movsd	%xmm3, -8008(%rdx)
	addq	$8, %rdx
	decq	%rdi
	movapd	%xmm3, %xmm2
	movapd	%xmm6, %xmm3
	jne	.LBB6_11
# BB#12:                                #   in Loop: Header=BB6_10 Depth=2
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$999, %rsi              # imm = 0x3E7
	jne	.LBB6_10
# BB#13:                                #   in Loop: Header=BB6_9 Depth=1
	incl	%ecx
	cmpl	$20, %ecx
	jne	.LBB6_9
# BB#14:                                # %kernel_seidel_2d.exit
	leaq	8(%rbx), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_15:                               # %.preheader.i31
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_16 Depth 2
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movq	$-1000, %rdx            # imm = 0xFC18
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_16:                               #   Parent Loop BB6_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1002(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm8, %xmm2
	divsd	%xmm9, %xmm2
	movsd	%xmm2, -8(%rsi)
	leal	1003(%rdx), %edi
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edi, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm8, %xmm2
	divsd	%xmm9, %xmm2
	movsd	%xmm2, (%rsi)
	addq	$16, %rsi
	addq	$2, %rdx
	jne	.LBB6_16
# BB#17:                                #   in Loop: Header=BB6_15 Depth=1
	incq	%rcx
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$1000, %rcx             # imm = 0x3E8
	jne	.LBB6_15
# BB#18:                                # %.preheader1.i39.preheader
	leaq	16016(%rbx), %r8
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_19:                               # %.preheader1.i39
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_20 Depth 2
                                        #       Child Loop BB6_21 Depth 3
	movq	%r8, %rax
	movl	$1, %esi
	.p2align	4, 0x90
.LBB6_20:                               # %.preheader.i54
                                        #   Parent Loop BB6_19 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_21 Depth 3
	imulq	$8000, %rsi, %rdx       # imm = 0x1F40
	incq	%rsi
	movsd	-8000(%rbx,%rdx), %xmm1 # xmm1 = mem[0],zero
	movsd	-7992(%rbx,%rdx), %xmm3 # xmm3 = mem[0],zero
	movsd	(%rbx,%rdx), %xmm7      # xmm7 = mem[0],zero
	movsd	8(%rbx,%rdx), %xmm2     # xmm2 = mem[0],zero
	movsd	8000(%rbx,%rdx), %xmm5  # xmm5 = mem[0],zero
	movsd	8008(%rbx,%rdx), %xmm6  # xmm6 = mem[0],zero
	movl	$998, %edi              # imm = 0x3E6
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB6_21:                               #   Parent Loop BB6_19 Depth=1
                                        #     Parent Loop BB6_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movapd	%xmm3, %xmm4
	addsd	%xmm4, %xmm1
	movsd	-16000(%rdx), %xmm3     # xmm3 = mem[0],zero
	addsd	%xmm3, %xmm1
	addsd	%xmm7, %xmm1
	addsd	%xmm2, %xmm1
	movsd	-8000(%rdx), %xmm2      # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm1
	addsd	%xmm5, %xmm1
	addsd	%xmm6, %xmm1
	movapd	%xmm6, %xmm5
	movsd	(%rdx), %xmm6           # xmm6 = mem[0],zero
	addsd	%xmm6, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, -8008(%rdx)
	addq	$8, %rdx
	decq	%rdi
	movapd	%xmm1, %xmm7
	movapd	%xmm4, %xmm1
	jne	.LBB6_21
# BB#22:                                #   in Loop: Header=BB6_20 Depth=2
	addq	$8000, %rax             # imm = 0x1F40
	cmpq	$999, %rsi              # imm = 0x3E7
	jne	.LBB6_20
# BB#23:                                #   in Loop: Header=BB6_19 Depth=1
	incl	%ecx
	cmpl	$20, %ecx
	jne	.LBB6_19
# BB#24:                                # %.preheader.i61.preheader
	xorl	%edx, %edx
	movl	$1, %eax
	movapd	.LCPI6_3(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_4(%rip), %xmm3   # xmm3 = mem[0],zero
.LBB6_25:                               # %.preheader.i61
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_26 Depth 2
	movq	%rax, %rsi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB6_26:                               #   Parent Loop BB6_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%r14,%rsi,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%rbx,%rsi,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_27
# BB#29:                                #   in Loop: Header=BB6_26 Depth=2
	movsd	(%r14,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbx,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_28
# BB#30:                                #   in Loop: Header=BB6_26 Depth=2
	addq	$2, %rsi
	leaq	2(%rcx), %rdi
	incq	%rcx
	cmpq	$1000, %rcx             # imm = 0x3E8
	movq	%rdi, %rcx
	jl	.LBB6_26
# BB#31:                                #   in Loop: Header=BB6_25 Depth=1
	incq	%rdx
	addq	$1000, %rax             # imm = 0x3E8
	cmpq	$1000, %rdx             # imm = 0x3E8
	jl	.LBB6_25
# BB#32:                                # %check_FP.exit
	movl	$16001, %edi            # imm = 0x3E81
	callq	malloc
	movq	%rax, %r15
	movb	$0, 16000(%r15)
	xorl	%r12d, %r12d
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB6_33:                               # %.preheader.i65
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_34 Depth 2
	movq	%r13, %rsi
	movl	$15, %ecx
	.p2align	4, 0x90
.LBB6_34:                               #   Parent Loop BB6_33 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdx
	movl	%edx, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -15(%r15,%rcx)
	movb	%al, -14(%r15,%rcx)
	movq	%rdx, %rax
	shrq	$8, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -13(%r15,%rcx)
	movb	%al, -12(%r15,%rcx)
	movq	%rdx, %rax
	shrq	$16, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -11(%r15,%rcx)
	movb	%al, -10(%r15,%rcx)
	movl	%edx, %eax
	shrl	$24, %eax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -9(%r15,%rcx)
	movb	%al, -8(%r15,%rcx)
	movq	%rdx, %rax
	shrq	$32, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -7(%r15,%rcx)
	movb	%al, -6(%r15,%rcx)
	movq	%rdx, %rax
	shrq	$40, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -5(%r15,%rcx)
	movb	%al, -4(%r15,%rcx)
	movq	%rdx, %rax
	shrq	$48, %rax
	andb	$15, %al
	orb	$48, %al
	movb	%al, -3(%r15,%rcx)
	movb	%al, -2(%r15,%rcx)
	shrq	$56, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, -1(%r15,%rcx)
	movb	%dl, (%r15,%rcx)
	addq	$16, %rcx
	addq	$8, %rsi
	cmpq	$16015, %rcx            # imm = 0x3E8F
	jne	.LBB6_34
# BB#35:                                #   in Loop: Header=BB6_33 Depth=1
	movq	stderr(%rip), %rsi
	movq	%r15, %rdi
	callq	fputs
	incq	%r12
	addq	$8000, %r13             # imm = 0x1F40
	cmpq	$1000, %r12             # imm = 0x3E8
	jne	.LBB6_33
# BB#36:                                # %print_array.exit
	movq	%r15, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_37
.LBB6_27:                               # %check_FP.exit.threadsplit
	decq	%rcx
.LBB6_28:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_4(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %r8d
	movl	%ecx, %r9d
	callq	fprintf
	movl	$1, %eax
.LBB6_37:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB6_38:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d][%d] = %lf and B[%d][%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 76


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
