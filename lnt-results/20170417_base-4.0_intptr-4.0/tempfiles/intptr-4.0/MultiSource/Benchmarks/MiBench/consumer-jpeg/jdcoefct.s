	.text
	.file	"jdcoefct.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	128                     # 0x80
	.quad	256                     # 0x100
.LCPI0_1:
	.quad	384                     # 0x180
	.quad	512                     # 0x200
.LCPI0_2:
	.quad	640                     # 0x280
	.quad	768                     # 0x300
.LCPI0_3:
	.quad	896                     # 0x380
	.quad	1024                    # 0x400
	.text
	.globl	jinit_d_coef_controller
	.p2align	4, 0x90
	.type	jinit_d_coef_controller,@function
jinit_d_coef_controller:                # @jinit_d_coef_controller
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$224, %edx
	callq	*(%rax)
	movq	%rax, %rbp
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%rbp, 544(%rbx)
	movq	$start_input_pass, (%rbp)
	movq	$start_output_pass, 16(%rbp)
	movq	$0, 216(%rbp)
	testl	%r14d, %r14d
	je	.LBB0_7
# BB#1:
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$0, 48(%rax)
	jle	.LBB0_2
# BB#3:                                 # %.lr.ph
	movq	(%rsp), %rax            # 8-byte Reload
	movq	296(%rax), %rbx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %r15
	addq	$136, %r15
	addq	$8, %rbx
	xorl	%r14d, %r14d
	movq	(%rsp), %r12            # 8-byte Reload
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	cmpl	$0, 304(%r12)
	movl	4(%rbx), %eax
	movl	20(%rbx), %edi
	leal	(%rax,%rax,2), %ebp
	cmovel	%eax, %ebp
	movq	8(%r12), %rax
	movq	40(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	(%rbx), %rsi
	callq	jround_up
	movq	%rax, %r13
	movl	24(%rbx), %edi
	movslq	4(%rbx), %rsi
	callq	jround_up
	movl	$1, %esi
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%r13d, %ecx
	movl	%eax, %r8d
	movl	%ebp, %r9d
	callq	*16(%rsp)               # 8-byte Folded Reload
	movq	%rax, (%r15,%r14,8)
	incq	%r14
	movslq	48(%r12), %rax
	addq	$96, %rbx
	cmpq	%rax, %r14
	jl	.LBB0_4
# BB#5:
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB0_6
.LBB0_7:
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rdi), %rax
	movl	$1, %esi
	movl	$1280, %edx             # imm = 0x500
	callq	*8(%rax)
	movq	%rax, 56(%rbp)
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqa	.LCPI0_0(%rip), %xmm1   # xmm1 = [128,256]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 64(%rbp)
	movdqa	.LCPI0_1(%rip), %xmm1   # xmm1 = [384,512]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 80(%rbp)
	movdqa	.LCPI0_2(%rip), %xmm1   # xmm1 = [640,768]
	paddq	%xmm0, %xmm1
	movdqu	%xmm1, 96(%rbp)
	paddq	.LCPI0_3(%rip), %xmm0
	movdqu	%xmm0, 112(%rbp)
	addq	$1152, %rax             # imm = 0x480
	movq	%rax, 128(%rbp)
	movq	$dummy_consume_data, 8(%rbp)
	movq	$decompress_onepass, 24(%rbp)
	movq	$0, 32(%rbp)
	jmp	.LBB0_8
.LBB0_2:                                # %.._crit_edge_crit_edge
	movq	%rbp, %r15
	addq	$136, %r15
.LBB0_6:                                # %._crit_edge
	movq	$consume_data, 8(%rbp)
	movq	$decompress_data, 24(%rbp)
	movq	%r15, 32(%rbp)
.LBB0_8:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_d_coef_controller, .Lfunc_end0-jinit_d_coef_controller
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_input_pass,@function
start_input_pass:                       # @start_input_pass
	.cfi_startproc
# BB#0:
	movl	$0, 168(%rdi)
	movq	544(%rdi), %rax
	movl	$1, %ecx
	cmpl	$1, 416(%rdi)
	jg	.LBB1_4
# BB#1:
	cmpl	$1, 400(%rdi)
	movq	424(%rdi), %rcx
	jne	.LBB1_2
# BB#3:
	movl	72(%rcx), %ecx
	jmp	.LBB1_4
.LBB1_2:
	movl	12(%rcx), %ecx
.LBB1_4:                                # %start_iMCU_row.exit
	movl	%ecx, 48(%rax)
	movq	$0, 40(%rax)
	retq
.Lfunc_end1:
	.size	start_input_pass, .Lfunc_end1-start_input_pass
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_output_pass,@function
start_output_pass:                      # @start_output_pass
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	544(%rbx), %r14
	cmpq	$0, 32(%r14)
	je	.LBB2_22
# BB#1:
	cmpl	$0, 96(%rbx)
	je	.LBB2_20
# BB#2:
	cmpl	$0, 304(%rbx)
	je	.LBB2_20
# BB#3:
	cmpq	$0, 184(%rbx)
	je	.LBB2_20
# BB#4:
	movq	216(%r14), %rax
	testq	%rax, %rax
	je	.LBB2_6
# BB#5:                                 # %._crit_edge.i
	leaq	48(%rbx), %r15
	cmpl	$0, (%r15)
	jg	.LBB2_8
	jmp	.LBB2_20
.LBB2_6:
	movq	8(%rbx), %rax
	leaq	48(%rbx), %r15
	movslq	48(%rbx), %rcx
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	%rax, 216(%r14)
	cmpl	$0, (%r15)
	jle	.LBB2_20
.LBB2_8:                                # %.lr.ph.preheader.i
	movq	296(%rbx), %r11
	addq	$20, %rax
	addq	$80, %r11
	xorl	%r10d, %r10d
	movl	$5, %esi
	movl	$1, %r8d
	xorl	%r9d, %r9d
.LBB2_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r11), %rdi
	testq	%rdi, %rdi
	je	.LBB2_20
# BB#10:                                #   in Loop: Header=BB2_9 Depth=1
	cmpw	$0, (%rdi)
	je	.LBB2_20
# BB#11:                                #   in Loop: Header=BB2_9 Depth=1
	cmpw	$0, 2(%rdi)
	je	.LBB2_20
# BB#12:                                #   in Loop: Header=BB2_9 Depth=1
	cmpw	$0, 16(%rdi)
	je	.LBB2_20
# BB#13:                                #   in Loop: Header=BB2_9 Depth=1
	cmpw	$0, 32(%rdi)
	je	.LBB2_20
# BB#14:                                #   in Loop: Header=BB2_9 Depth=1
	cmpw	$0, 18(%rdi)
	je	.LBB2_20
# BB#15:                                #   in Loop: Header=BB2_9 Depth=1
	cmpw	$0, 4(%rdi)
	je	.LBB2_20
# BB#16:                                #   in Loop: Header=BB2_9 Depth=1
	movq	184(%rbx), %rdi
	cmpl	$0, -20(%rdi,%rsi,4)
	js	.LBB2_20
# BB#17:                                # %.preheader.preheader.i
                                        #   in Loop: Header=BB2_9 Depth=1
	movl	-16(%rdi,%rsi,4), %edx
	movl	%edx, -16(%rax)
	movl	-16(%rdi,%rsi,4), %edx
	movl	-12(%rdi,%rsi,4), %ecx
	movl	%ecx, -12(%rax)
	orl	-12(%rdi,%rsi,4), %edx
	movl	-8(%rdi,%rsi,4), %ecx
	movl	%ecx, -8(%rax)
	orl	-8(%rdi,%rsi,4), %edx
	movl	-4(%rdi,%rsi,4), %ecx
	movl	%ecx, -4(%rax)
	orl	-4(%rdi,%rsi,4), %edx
	movl	(%rdi,%rsi,4), %ecx
	movl	%ecx, (%rax)
	orl	(%rdi,%rsi,4), %edx
	cmovnel	%r8d, %r9d
	incq	%r10
	movslq	(%r15), %rcx
	addq	$64, %rsi
	addq	$24, %rax
	addq	$96, %r11
	cmpq	%rcx, %r10
	jl	.LBB2_9
# BB#18:                                # %smoothing_ok.exit
	testl	%r9d, %r9d
	je	.LBB2_20
# BB#19:
	movl	$decompress_smooth_data, %eax
	jmp	.LBB2_21
.LBB2_20:                               # %smoothing_ok.exit.thread
	movl	$decompress_data, %eax
.LBB2_21:
	movq	%rax, 24(%r14)
.LBB2_22:
	movl	$0, 176(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	start_output_pass, .Lfunc_end2-start_output_pass
	.cfi_endproc

	.p2align	4, 0x90
	.type	consume_data,@function
consume_data:                           # @consume_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 176
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	544(%rbx), %r12
	cmpl	$0, 416(%rbx)
	jle	.LBB3_3
# BB#1:                                 # %.lr.ph104
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	424(%rbx,%rbp,8), %rax
	movq	8(%rbx), %r9
	movslq	4(%rax), %rcx
	movq	136(%r12,%rcx,8), %rsi
	movl	12(%rax), %ecx
	movl	168(%rbx), %edx
	imull	%ecx, %edx
	movl	$1, %r8d
	movq	%rbx, %rdi
	callq	*64(%r9)
	movq	%rax, 80(%rsp,%rbp,8)
	incq	%rbp
	movslq	416(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB3_2
.LBB3_3:                                # %._crit_edge105
	movslq	44(%r12), %rsi
	movl	48(%r12), %eax
	cmpl	%eax, %esi
	jge	.LBB3_25
# BB#4:                                 # %.lr.ph100
	leaq	56(%r12), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	40(%r12), %edi
	movl	456(%rbx), %ecx
	leaq	72(%r12), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
.LBB3_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
                                        #       Child Loop BB3_9 Depth 3
                                        #         Child Loop BB3_12 Depth 4
                                        #           Child Loop BB3_15 Depth 5
                                        #           Child Loop BB3_17 Depth 5
	cmpl	%ecx, %edi
	jae	.LBB3_24
# BB#6:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_7:                                # %.preheader
                                        #   Parent Loop BB3_5 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_9 Depth 3
                                        #         Child Loop BB3_12 Depth 4
                                        #           Child Loop BB3_15 Depth 5
                                        #           Child Loop BB3_17 Depth 5
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movslq	416(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jle	.LBB3_20
# BB#8:                                 # %.lr.ph93.preheader
                                        #   in Loop: Header=BB3_7 Depth=2
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_9:                                # %.lr.ph93
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_12 Depth 4
                                        #           Child Loop BB3_15 Depth 5
                                        #           Child Loop BB3_17 Depth 5
	movq	424(%rbx,%rcx,8), %rax
	movslq	56(%rax), %r10
	testq	%r10, %r10
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	jle	.LBB3_19
# BB#10:                                # %.lr.ph88
                                        #   in Loop: Header=BB3_9 Depth=3
	movl	52(%rax), %r15d
	testl	%r15d, %r15d
	jle	.LBB3_19
# BB#11:                                # %.lr.ph88.split.us.preheader
                                        #   in Loop: Header=BB3_9 Depth=3
	movl	%r15d, %ebx
	imull	12(%rsp), %ebx          # 4-byte Folded Reload
	movq	80(%rsp,%rcx,8), %r13
	leal	-1(%r15), %r11d
	incq	%r11
	movq	%r11, %r9
	movabsq	$8589934588, %rax       # imm = 0x1FFFFFFFC
	andq	%rax, %r9
	movq	%rbx, %rax
	shlq	$6, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph88.split.us
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_7 Depth=2
                                        #       Parent Loop BB3_9 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB3_15 Depth 5
                                        #           Child Loop BB3_17 Depth 5
	leaq	(%r14,%rsi), %rax
	movq	(%r13,%rax,8), %rax
	movq	%rbx, %rbp
	shlq	$7, %rbp
	addq	%rax, %rbp
	xorl	%r8d, %r8d
	cmpq	$4, %r11
	movslq	%edx, %rdx
	jb	.LBB3_17
# BB#13:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_12 Depth=4
	testq	%r9, %r9
	je	.LBB3_17
# BB#14:                                # %vector.body.preheader
                                        #   in Loop: Header=BB3_12 Depth=4
	movq	%r9, %rcx
	shlq	$7, %rcx
	addq	%rcx, %rbp
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	384(%rax,%rcx,2), %rsi
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rcx
	addq	%r9, %rdx
	movq	%r9, %rax
	.p2align	4, 0x90
.LBB3_15:                               # %vector.body
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_7 Depth=2
                                        #       Parent Loop BB3_9 Depth=3
                                        #         Parent Loop BB3_12 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leaq	-384(%rsi), %rbx
	leaq	-256(%rsi), %rdi
	leaq	-128(%rsi), %r8
	movd	%rdi, %xmm0
	movd	%rbx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movd	%rsi, %xmm0
	movd	%r8, %xmm2
	punpcklqdq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0]
	movdqu	%xmm1, -16(%rcx)
	movdqu	%xmm2, (%rcx)
	addq	$512, %rsi              # imm = 0x200
	addq	$32, %rcx
	addq	$-4, %rax
	jne	.LBB3_15
# BB#16:                                # %middle.block
                                        #   in Loop: Header=BB3_12 Depth=4
	cmpq	%r9, %r11
	movl	%r9d, %r8d
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	je	.LBB3_18
	.p2align	4, 0x90
.LBB3_17:                               # %scalar.ph
                                        #   Parent Loop BB3_5 Depth=1
                                        #     Parent Loop BB3_7 Depth=2
                                        #       Parent Loop BB3_9 Depth=3
                                        #         Parent Loop BB3_12 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rbp, 56(%r12,%rdx,8)
	incq	%rdx
	subq	$-128, %rbp
	incl	%r8d
	cmpl	%r15d, %r8d
	jl	.LBB3_17
.LBB3_18:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_12 Depth=4
	incq	%r14
	cmpq	%r10, %r14
	jl	.LBB3_12
.LBB3_19:                               # %._crit_edge89
                                        #   in Loop: Header=BB3_9 Depth=3
	movq	48(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	40(%rsp), %rcx          # 8-byte Folded Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	jl	.LBB3_9
.LBB3_20:                               # %._crit_edge94
                                        #   in Loop: Header=BB3_7 Depth=2
	movq	576(%rbx), %rax
	movq	%rbx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	callq	*8(%rax)
	testl	%eax, %eax
	je	.LBB3_21
# BB#22:                                #   in Loop: Header=BB3_7 Depth=2
	movl	12(%rsp), %edi          # 4-byte Reload
	incl	%edi
	movl	456(%rbx), %ecx
	cmpl	%ecx, %edi
	movq	16(%rsp), %rsi          # 8-byte Reload
	jb	.LBB3_7
# BB#23:                                # %._crit_edge96.loopexit
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	48(%r12), %eax
.LBB3_24:                               # %._crit_edge96
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	$0, 40(%r12)
	incq	%rsi
	movslq	%eax, %rdx
	xorl	%edi, %edi
	cmpq	%rdx, %rsi
	jl	.LBB3_5
.LBB3_25:                               # %._crit_edge101
	movl	168(%rbx), %ecx
	incl	%ecx
	movl	%ecx, 168(%rbx)
	movl	400(%rbx), %edx
	cmpl	%edx, %ecx
	jae	.LBB3_31
# BB#26:
	movq	544(%rbx), %rax
	movl	$1, %esi
	cmpl	$1, 416(%rbx)
	jg	.LBB3_30
# BB#27:
	decl	%edx
	movq	424(%rbx), %rsi
	cmpl	%edx, %ecx
	jae	.LBB3_29
# BB#28:
	movl	12(%rsi), %esi
	jmp	.LBB3_30
.LBB3_21:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 44(%r12)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 40(%r12)
	xorl	%eax, %eax
	jmp	.LBB3_32
.LBB3_31:
	movq	560(%rbx), %rax
	movq	%rbx, %rdi
	callq	*24(%rax)
	movl	$4, %eax
	jmp	.LBB3_32
.LBB3_29:
	movl	72(%rsi), %esi
.LBB3_30:                               # %start_iMCU_row.exit
	movl	%esi, 48(%rax)
	movq	$0, 40(%rax)
	movl	$3, %eax
.LBB3_32:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	consume_data, .Lfunc_end3-consume_data
	.cfi_endproc

	.p2align	4, 0x90
	.type	decompress_data,@function
decompress_data:                        # @decompress_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 128
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	movq	544(%rbp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	400(%rbp), %eax
	decl	%eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movl	172(%rbp), %eax
	cmpl	%eax, 164(%rbp)
	jl	.LBB4_4
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	jne	.LBB4_6
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	movl	168(%rbp), %eax
	cmpl	176(%rbp), %eax
	ja	.LBB4_6
.LBB4_4:                                # %.critedge
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	560(%rbp), %rax
	movq	%rbp, %rdi
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB4_1
# BB#5:
	xorl	%eax, %eax
	jmp	.LBB4_21
.LBB4_6:                                # %.critedge1
	cmpl	$0, 48(%rbp)
	jle	.LBB4_20
# BB#7:                                 # %.lr.ph97
	movq	296(%rbp), %r13
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
                                        #       Child Loop BB4_17 Depth 3
	cmpl	$0, 48(%r13)
	je	.LBB4_19
# BB#9:                                 #   in Loop: Header=BB4_8 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rbp), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	136(%rcx,%rdx,8), %rsi
	movl	12(%r13), %ecx
	movl	176(%rbp), %edx
	imull	%ecx, %edx
	xorl	%r8d, %r8d
	movq	%rbp, %rdi
	callq	*64(%rax)
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	28(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, 176(%rbp)
	jae	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_8 Depth=1
	movl	12(%r13), %edx
	testl	%edx, %edx
	jg	.LBB4_13
	jmp	.LBB4_19
	.p2align	4, 0x90
.LBB4_11:                               #   in Loop: Header=BB4_8 Depth=1
	movl	12(%r13), %ecx
	movl	32(%r13), %eax
	xorl	%edx, %edx
	divl	%ecx
	testl	%edx, %edx
	cmovel	%ecx, %edx
	testl	%edx, %edx
	jle	.LBB4_19
.LBB4_13:                               # %.lr.ph94
                                        #   in Loop: Header=BB4_8 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	584(%rax), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	8(%rax,%rcx,8), %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rcx,8), %r12
	movl	28(%r13), %eax
	movl	%edx, %edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_14:                               #   Parent Loop BB4_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_17 Depth 3
	testl	%eax, %eax
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	je	.LBB4_15
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_14 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rbp
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph
                                        #   Parent Loop BB4_8 Depth=1
                                        #     Parent Loop BB4_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	movl	%r14d, %r8d
	callq	*%rbx
	subq	$-128, %rbp
	movl	28(%r13), %eax
	movl	36(%r13), %edx
	addl	%edx, %r14d
	incl	%r15d
	cmpl	%eax, %r15d
	jb	.LBB4_17
	jmp	.LBB4_18
	.p2align	4, 0x90
.LBB4_15:                               # %.._crit_edge_crit_edge
                                        #   in Loop: Header=BB4_14 Depth=2
	movl	36(%r13), %edx
	xorl	%eax, %eax
.LBB4_18:                               # %._crit_edge
                                        #   in Loop: Header=BB4_14 Depth=2
	movslq	%edx, %rdx
	leaq	(%r12,%rdx,8), %r12
	movq	64(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	cmpq	56(%rsp), %rdx          # 8-byte Folded Reload
	jne	.LBB4_14
.LBB4_19:                               # %.loopexit
                                        #   in Loop: Header=BB4_8 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	addq	$96, %r13
	movq	16(%rsp), %rbp          # 8-byte Reload
	movslq	48(%rbp), %rax
	movq	%rdx, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpq	%rax, %rdx
	jl	.LBB4_8
.LBB4_20:                               # %._crit_edge98
	movl	176(%rbp), %ecx
	incl	%ecx
	movl	%ecx, 176(%rbp)
	xorl	%eax, %eax
	cmpl	400(%rbp), %ecx
	setae	%al
	addl	$3, %eax
.LBB4_21:                               # %.loopexit87
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	decompress_data, .Lfunc_end4-decompress_data
	.cfi_endproc

	.p2align	4, 0x90
	.type	dummy_consume_data,@function
dummy_consume_data:                     # @dummy_consume_data
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	dummy_consume_data, .Lfunc_end5-dummy_consume_data
	.cfi_endproc

	.p2align	4, 0x90
	.type	decompress_onepass,@function
decompress_onepass:                     # @decompress_onepass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 160
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %r8
	movq	544(%r8), %rcx
	movl	400(%r8), %r14d
	movl	44(%rcx), %esi
	movl	48(%rcx), %eax
	movq	%rsi, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpl	%eax, %esi
	jge	.LBB6_35
# BB#1:                                 # %.lr.ph130
	movl	456(%r8), %r15d
	decl	%r15d
	decl	%r14d
	leaq	56(%rcx), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movl	40(%rcx), %r12d
	movl	%r14d, 28(%rsp)         # 4-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%r15d, 20(%rsp)         # 4-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
.LBB6_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_3 Depth 2
                                        #       Child Loop BB6_6 Depth 3
                                        #         Child Loop BB6_16 Depth 4
                                        #         Child Loop BB6_19 Depth 4
                                        #         Child Loop BB6_21 Depth 4
                                        #         Child Loop BB6_23 Depth 4
                                        #           Child Loop BB6_26 Depth 5
	cmpl	%r15d, %r12d
	ja	.LBB6_33
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph125
                                        #   Parent Loop BB6_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_6 Depth 3
                                        #         Child Loop BB6_16 Depth 4
                                        #         Child Loop BB6_19 Depth 4
                                        #         Child Loop BB6_21 Depth 4
                                        #         Child Loop BB6_23 Depth 4
                                        #           Child Loop BB6_26 Depth 5
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rdi
	movslq	464(%r8), %rsi
	shlq	$7, %rsi
	movq	%r8, %rbx
	callq	jzero_far
	movq	576(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	*8(%rax)
	testl	%eax, %eax
	je	.LBB6_43
# BB#4:                                 # %.preheader108
                                        #   in Loop: Header=BB6_3 Depth=2
	movl	416(%rbx), %ecx
	testl	%ecx, %ecx
	movq	%rbx, %r8
	jle	.LBB6_31
# BB#5:                                 # %.lr.ph121
                                        #   in Loop: Header=BB6_3 Depth=2
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%r12d, 24(%rsp)         # 4-byte Spill
	jmp	.LBB6_6
.LBB6_14:                               #   in Loop: Header=BB6_6 Depth=3
	xorl	%ebp, %ebp
	pxor	%xmm2, %xmm2
.LBB6_17:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB6_6 Depth=3
	cmpl	$56, %ebx
	jb	.LBB6_20
# BB#18:                                # %vector.ph.new
                                        #   in Loop: Header=BB6_6 Depth=3
	movl	%esi, %edi
	subl	%ebp, %edi
	.p2align	4, 0x90
.LBB6_19:                               # %vector.body
                                        #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_3 Depth=2
                                        #       Parent Loop BB6_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	paddd	%xmm1, %xmm0
	paddd	%xmm1, %xmm2
	paddd	%xmm1, %xmm0
	paddd	%xmm1, %xmm2
	paddd	%xmm1, %xmm0
	paddd	%xmm1, %xmm2
	paddd	%xmm1, %xmm0
	paddd	%xmm1, %xmm2
	paddd	%xmm1, %xmm0
	paddd	%xmm1, %xmm2
	paddd	%xmm1, %xmm0
	paddd	%xmm1, %xmm2
	paddd	%xmm1, %xmm0
	paddd	%xmm1, %xmm2
	paddd	%xmm1, %xmm0
	paddd	%xmm1, %xmm2
	addl	$-64, %edi
	jne	.LBB6_19
.LBB6_20:                               # %middle.block
                                        #   in Loop: Header=BB6_6 Depth=3
	paddd	%xmm2, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %edi
	cmpl	%esi, %eax
	jne	.LBB6_21
	jmp	.LBB6_30
	.p2align	4, 0x90
.LBB6_6:                                #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB6_16 Depth 4
                                        #         Child Loop BB6_19 Depth 4
                                        #         Child Loop BB6_21 Depth 4
                                        #         Child Loop BB6_23 Depth 4
                                        #           Child Loop BB6_26 Depth 5
	movq	424(%r8,%r9,8), %rsi
	cmpl	$0, 48(%rsi)
	je	.LBB6_7
# BB#8:                                 #   in Loop: Header=BB6_6 Depth=3
	leaq	68(%rsi), %rdx
	leaq	52(%rsi), %rax
	cmpl	%r15d, %r12d
	movq	%rax, 56(%rsp)          # 8-byte Spill
	cmovbq	%rax, %rdx
	movl	56(%rsi), %eax
	testl	%eax, %eax
	jle	.LBB6_30
# BB#9:                                 # %.lr.ph118
                                        #   in Loop: Header=BB6_6 Depth=3
	movl	(%rdx), %edx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	testl	%edx, %edx
	jle	.LBB6_10
# BB#22:                                # %.lr.ph118.split.us.preheader
                                        #   in Loop: Header=BB6_6 Depth=3
	movq	584(%r8), %rcx
	movslq	4(%rsi), %rdx
	movq	8(%rcx,%rdx,8), %rbx
	movl	64(%rsi), %ecx
	imull	%r12d, %ecx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movl	36(%rsi), %ecx
	movl	%ecx, %edx
	imull	8(%rsp), %edx           # 4-byte Folded Reload
	movslq	%edx, %r13
	shlq	$3, %r13
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%r9, 72(%rsp)           # 8-byte Spill
	addq	(%rdx,%r9,8), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_23:                               # %.lr.ph118.split.us
                                        #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_3 Depth=2
                                        #       Parent Loop BB6_6 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB6_26 Depth 5
	cmpl	%r14d, 168(%r8)
	jb	.LBB6_25
# BB#24:                                #   in Loop: Header=BB6_23 Depth=4
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	(%rbp,%rdx), %edx
	cmpl	72(%rsi), %edx
	jge	.LBB6_28
.LBB6_25:                               # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB6_23 Depth=4
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movl	%edi, 36(%rsp)          # 4-byte Spill
	movslq	%edi, %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %r12
	movq	80(%rsp), %r14          # 8-byte Reload
	movl	32(%rsp), %ebp          # 4-byte Reload
	.p2align	4, 0x90
.LBB6_26:                               # %.lr.ph.us
                                        #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_3 Depth=2
                                        #       Parent Loop BB6_6 Depth=3
                                        #         Parent Loop BB6_23 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	(%r12), %rdx
	movq	%r8, %rdi
	movq	%rsi, %r15
	movq	%r13, %rcx
	movl	%ebp, %r8d
	callq	*%rbx
	movq	%r15, %rsi
	movq	96(%rsp), %r8           # 8-byte Reload
	movl	36(%rsi), %ecx
	addl	%ecx, %ebp
	addq	$8, %r12
	decq	%r14
	jne	.LBB6_26
# BB#27:                                # %..loopexit_crit_edge.us.loopexit
                                        #   in Loop: Header=BB6_23 Depth=4
	movl	56(%rsi), %eax
	movl	28(%rsp), %r14d         # 4-byte Reload
	movl	36(%rsp), %edi          # 4-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
.LBB6_28:                               # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB6_23 Depth=4
	movq	56(%rsp), %rdx          # 8-byte Reload
	addl	(%rdx), %edi
	movslq	%ecx, %rdx
	leaq	(%r13,%rdx,8), %r13
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB6_23
# BB#29:                                # %.loopexit107.loopexit
                                        #   in Loop: Header=BB6_6 Depth=3
	movl	416(%r8), %ecx
	movl	20(%rsp), %r15d         # 4-byte Reload
	movl	24(%rsp), %r12d         # 4-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	jmp	.LBB6_30
	.p2align	4, 0x90
.LBB6_7:                                #   in Loop: Header=BB6_6 Depth=3
	addl	60(%rsi), %edi
	jmp	.LBB6_30
.LBB6_10:                               # %.lr.ph118.split.preheader
                                        #   in Loop: Header=BB6_6 Depth=3
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %edx
	cmpl	$7, %eax
	jbe	.LBB6_11
# BB#12:                                # %min.iters.checked
                                        #   in Loop: Header=BB6_6 Depth=3
	movl	%eax, %esi
	andl	$-8, %esi
	je	.LBB6_11
# BB#13:                                # %vector.ph
                                        #   in Loop: Header=BB6_6 Depth=3
	movd	%edi, %xmm0
	movd	%edx, %xmm1
	pshufd	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	leal	-8(%rsi), %ebx
	movl	%ebx, %edi
	shrl	$3, %edi
	incl	%edi
	andl	$7, %edi
	je	.LBB6_14
# BB#15:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB6_6 Depth=3
	negl	%edi
	pxor	%xmm2, %xmm2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_16:                               # %vector.body.prol
                                        #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_3 Depth=2
                                        #       Parent Loop BB6_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	paddd	%xmm1, %xmm0
	paddd	%xmm1, %xmm2
	addl	$8, %ebp
	incl	%edi
	jne	.LBB6_16
	jmp	.LBB6_17
.LBB6_11:                               #   in Loop: Header=BB6_6 Depth=3
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_21:                               # %.lr.ph118.split
                                        #   Parent Loop BB6_2 Depth=1
                                        #     Parent Loop BB6_3 Depth=2
                                        #       Parent Loop BB6_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%esi
	addl	%edx, %edi
	cmpl	%eax, %esi
	jl	.LBB6_21
	.p2align	4, 0x90
.LBB6_30:                               # %.loopexit107
                                        #   in Loop: Header=BB6_6 Depth=3
	incq	%r9
	movslq	%ecx, %rax
	cmpq	%rax, %r9
	jl	.LBB6_6
.LBB6_31:                               # %._crit_edge
                                        #   in Loop: Header=BB6_3 Depth=2
	incl	%r12d
	cmpl	%r15d, %r12d
	jbe	.LBB6_3
# BB#32:                                # %._crit_edge126.loopexit
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	48(%rcx), %eax
.LBB6_33:                               # %._crit_edge126
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	$0, 40(%rcx)
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, %rsi
	incl	%esi
	xorl	%r12d, %r12d
	movq	%rsi, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpl	%eax, %esi
	jl	.LBB6_2
# BB#34:                                # %._crit_edge131.loopexit
	movl	400(%r8), %r14d
.LBB6_35:                               # %._crit_edge131
	incl	176(%r8)
	movl	168(%r8), %ecx
	incl	%ecx
	movl	%ecx, 168(%r8)
	cmpl	%r14d, %ecx
	jae	.LBB6_41
# BB#36:
	movq	544(%r8), %rax
	movl	$1, %edx
	cmpl	$1, 416(%r8)
	jg	.LBB6_40
# BB#37:
	decl	%r14d
	movq	424(%r8), %rdx
	cmpl	%r14d, %ecx
	jae	.LBB6_39
# BB#38:
	movl	12(%rdx), %edx
	jmp	.LBB6_40
.LBB6_43:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%ecx, 44(%rax)
	movl	%r12d, 40(%rax)
	xorl	%eax, %eax
	jmp	.LBB6_42
.LBB6_41:
	movq	560(%r8), %rax
	movq	%r8, %rdi
	callq	*24(%rax)
	movl	$4, %eax
	jmp	.LBB6_42
.LBB6_39:
	movl	72(%rdx), %edx
.LBB6_40:                               # %start_iMCU_row.exit
	movl	%edx, 48(%rax)
	movq	$0, 40(%rax)
	movl	$3, %eax
.LBB6_42:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	decompress_onepass, .Lfunc_end6-decompress_onepass
	.cfi_endproc

	.p2align	4, 0x90
	.type	decompress_smooth_data,@function
decompress_smooth_data:                 # @decompress_smooth_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi58:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi59:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi60:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi61:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 56
	subq	$424, %rsp              # imm = 0x1A8
.Lcfi64:
	.cfi_def_cfa_offset 480
.Lcfi65:
	.cfi_offset %rbx, -56
.Lcfi66:
	.cfi_offset %r12, -48
.Lcfi67:
	.cfi_offset %r13, -40
.Lcfi68:
	.cfi_offset %r14, -32
.Lcfi69:
	.cfi_offset %r15, -24
.Lcfi70:
	.cfi_offset %rbp, -16
	movq	%rsi, 208(%rsp)         # 8-byte Spill
	movq	%rdi, %r12
	movq	544(%r12), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	400(%r12), %eax
	decl	%eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	movl	164(%r12), %ecx
	movl	172(%r12), %edx
	cmpl	%edx, %ecx
	jg	.LBB7_7
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	movq	560(%r12), %rax
	cmpl	$0, 36(%rax)
	jne	.LBB7_7
# BB#3:                                 #   in Loop: Header=BB7_1 Depth=1
	cmpl	%edx, %ecx
	jne	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_1 Depth=1
	xorl	%ecx, %ecx
	cmpl	$0, 508(%r12)
	sete	%cl
	addl	176(%r12), %ecx
	cmpl	%ecx, 168(%r12)
	ja	.LBB7_7
.LBB7_5:                                #   in Loop: Header=BB7_1 Depth=1
	movq	%r12, %rdi
	callq	*(%rax)
	testl	%eax, %eax
	jne	.LBB7_1
# BB#6:
	xorl	%eax, %eax
	jmp	.LBB7_76
.LBB7_7:                                # %.critedge
	cmpl	$0, 48(%r12)
	jle	.LBB7_75
# BB#8:                                 # %.lr.ph347
	movq	296(%r12), %r15
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r12, 256(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB7_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_18 Depth 2
                                        #       Child Loop BB7_25 Depth 3
	cmpl	$0, 48(%r15)
	je	.LBB7_74
# BB#10:                                #   in Loop: Header=BB7_9 Depth=1
	movl	176(%r12), %ebp
	movl	12(%r15), %edi
	cmpl	76(%rsp), %ebp          # 4-byte Folded Reload
	jae	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_9 Depth=1
	leal	(%rdi,%rdi), %ecx
	movl	$0, 48(%rsp)            # 4-byte Folded Spill
	movl	%edi, %r14d
	testl	%ebp, %ebp
	jne	.LBB7_14
	jmp	.LBB7_15
	.p2align	4, 0x90
.LBB7_12:                               #   in Loop: Header=BB7_9 Depth=1
	movl	32(%r15), %eax
	xorl	%edx, %edx
	divl	%edi
	movl	%edx, %ecx
	testl	%ecx, %ecx
	cmovel	%edi, %ecx
	movl	$1, 48(%rsp)            # 4-byte Folded Spill
	movl	%ecx, %r14d
	testl	%ebp, %ebp
	je	.LBB7_15
.LBB7_14:                               #   in Loop: Header=BB7_9 Depth=1
	addl	%edi, %ecx
	movq	8(%r12), %rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	136(%rdx,%rsi,8), %rsi
	decl	%ebp
	imull	%edi, %ebp
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	*64(%rax)
	leaq	288(%rsp), %rbp
	movslq	12(%r15), %rcx
	leaq	(%rax,%rcx,8), %rsi
	testl	%r14d, %r14d
	jg	.LBB7_17
	jmp	.LBB7_74
	.p2align	4, 0x90
.LBB7_15:                               #   in Loop: Header=BB7_9 Depth=1
	movq	8(%r12), %rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	136(%rdx,%rsi,8), %rsi
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	callq	*64(%rax)
	leaq	288(%rsp), %rbp
	movq	%rax, %rsi
	movl	$1, 44(%rsp)            # 4-byte Folded Spill
	testl	%r14d, %r14d
	jle	.LBB7_74
.LBB7_17:                               # %.lr.ph
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	32(%rsp), %r8           # 8-byte Reload
	leaq	(%r8,%r8), %rax
	leaq	(%rax,%rax,2), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	80(%r15), %rcx
	movzwl	(%rcx), %eax
	movzwl	2(%rcx), %r9d
	movzwl	16(%rcx), %r10d
	movzwl	32(%rcx), %edx
	movzwl	18(%rcx), %edi
	movzwl	4(%rcx), %ebx
	movq	584(%r12), %rcx
	movq	8(%rcx,%r8,8), %rcx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	leaq	(%rax,%rax,8), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	%rbx, %rcx
	shlq	$7, %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	shlq	$8, %rbx
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	leaq	(%rax,%rax,4), %rcx
	movq	%rcx, 248(%rsp)         # 8-byte Spill
	movq	%rdi, %rcx
	shlq	$7, %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	shlq	$8, %rdi
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movq	%rdx, %rcx
	shlq	$7, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	shlq	$8, %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	shlq	$2, %rax
	leaq	(%rax,%rax,8), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%r10, %rax
	shlq	$7, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	shlq	$8, %r10
	movq	%r10, 152(%rsp)         # 8-byte Spill
	movq	%r9, %rax
	shlq	$7, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	shlq	$8, %r9
	movq	%r9, 160(%rsp)          # 8-byte Spill
	leal	-1(%r14), %eax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movl	%r14d, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	216(%rax), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r8,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%r15, 184(%rsp)         # 8-byte Spill
	movq	%rsi, 232(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB7_18:                               #   Parent Loop BB7_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_25 Depth 3
	movq	(%rsi,%rax,8), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	je	.LBB7_20
# BB#19:                                #   in Loop: Header=BB7_18 Depth=2
	testq	%rax, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	je	.LBB7_21
.LBB7_20:                               #   in Loop: Header=BB7_18 Depth=2
	movq	-8(%rsi,%rax,8), %rcx
.LBB7_21:                               #   in Loop: Header=BB7_18 Depth=2
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB7_23
# BB#22:                                #   in Loop: Header=BB7_18 Depth=2
	cmpq	216(%rsp), %rax         # 8-byte Folded Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	je	.LBB7_24
.LBB7_23:                               #   in Loop: Header=BB7_18 Depth=2
	movq	8(%rsi,%rax,8), %rdx
.LBB7_24:                               #   in Loop: Header=BB7_18 Depth=2
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movswl	(%rcx), %eax
	movq	24(%rsp), %rsi          # 8-byte Reload
	movswl	(%rsi), %edi
	movswl	(%rdx), %ebx
	movl	28(%r15), %esi
	decl	%esi
	subq	$-128, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	subq	$-128, %rdx
	xorl	%ecx, %ecx
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	movl	%edi, %r15d
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	$0, 52(%rsp)            # 4-byte Folded Spill
	movl	%esi, 80(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB7_25:                               #   Parent Loop BB7_9 Depth=1
                                        #     Parent Loop BB7_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	movl	8(%rsp), %r13d          # 4-byte Reload
	movl	16(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%r15d, %r14d
	movl	%edi, %r15d
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	movl	20(%rsp), %r12d         # 4-byte Reload
	movl	$1, %edx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movl	%esi, %ebx
	movq	%rbp, %rsi
	movl	%ecx, %ebp
	callq	jcopy_block_row
	movl	%ebp, 84(%rsp)          # 4-byte Spill
	cmpl	%ebx, %ebp
	movl	%r15d, %r8d
	jae	.LBB7_27
# BB#26:                                #   in Loop: Header=BB7_25 Depth=3
	movq	192(%rsp), %rax         # 8-byte Reload
	movswl	(%rax), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movswl	128(%rax), %r8d
	movq	200(%rsp), %rax         # 8-byte Reload
	movswl	(%rax), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
.LBB7_27:                               #   in Loop: Header=BB7_25 Depth=3
	movq	280(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdi         # 8-byte Reload
	movl	4(%rdi,%rsi,4), %ecx
	testl	%ecx, %ecx
	je	.LBB7_36
# BB#28:                                #   in Loop: Header=BB7_25 Depth=3
	movzwl	290(%rsp), %eax
	testw	%ax, %ax
	jne	.LBB7_36
# BB#29:                                #   in Loop: Header=BB7_25 Depth=3
	movl	%r14d, %eax
	subl	%r8d, %eax
	cltq
	imulq	168(%rsp), %rax         # 8-byte Folded Reload
	testq	%rax, %rax
	js	.LBB7_32
# BB#30:                                #   in Loop: Header=BB7_25 Depth=3
	addq	88(%rsp), %rax          # 8-byte Folded Reload
	cqto
	idivq	160(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_35
# BB#31:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	jmp	.LBB7_35
.LBB7_32:                               #   in Loop: Header=BB7_25 Depth=3
	movq	88(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movq	%rdx, %rax
	cqto
	idivq	160(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_34
# BB#33:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
.LBB7_34:                               #   in Loop: Header=BB7_25 Depth=3
	negl	%eax
.LBB7_35:                               #   in Loop: Header=BB7_25 Depth=3
	movw	%ax, 290(%rsp)
.LBB7_36:                               #   in Loop: Header=BB7_25 Depth=3
	movl	8(%rdi,%rsi,4), %ecx
	testl	%ecx, %ecx
	je	.LBB7_45
# BB#37:                                #   in Loop: Header=BB7_25 Depth=3
	movzwl	304(%rsp), %eax
	testw	%ax, %ax
	jne	.LBB7_45
# BB#38:                                #   in Loop: Header=BB7_25 Depth=3
	movl	8(%rsp), %eax           # 4-byte Reload
	subl	%r12d, %eax
	cltq
	imulq	168(%rsp), %rax         # 8-byte Folded Reload
	testq	%rax, %rax
	js	.LBB7_41
# BB#39:                                #   in Loop: Header=BB7_25 Depth=3
	addq	96(%rsp), %rax          # 8-byte Folded Reload
	cqto
	idivq	152(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_44
# BB#40:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	jmp	.LBB7_44
.LBB7_41:                               #   in Loop: Header=BB7_25 Depth=3
	movq	96(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movq	%rdx, %rax
	cqto
	idivq	152(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_43
# BB#42:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
.LBB7_43:                               #   in Loop: Header=BB7_25 Depth=3
	negl	%eax
.LBB7_44:                               #   in Loop: Header=BB7_25 Depth=3
	movw	%ax, 304(%rsp)
.LBB7_45:                               #   in Loop: Header=BB7_25 Depth=3
	movl	12(%rdi,%rsi,4), %ecx
	testl	%ecx, %ecx
	je	.LBB7_54
# BB#46:                                #   in Loop: Header=BB7_25 Depth=3
	movzwl	320(%rsp), %eax
	testw	%ax, %ax
	jne	.LBB7_54
# BB#47:                                #   in Loop: Header=BB7_25 Depth=3
	leal	(%r15,%r15), %eax
	movl	8(%rsp), %edx           # 4-byte Reload
	subl	%eax, %edx
	addl	%r12d, %edx
	movslq	%edx, %rax
	imulq	176(%rsp), %rax         # 8-byte Folded Reload
	testq	%rax, %rax
	js	.LBB7_50
# BB#48:                                #   in Loop: Header=BB7_25 Depth=3
	addq	104(%rsp), %rax         # 8-byte Folded Reload
	cqto
	idivq	144(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_53
# BB#49:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	jmp	.LBB7_53
.LBB7_50:                               #   in Loop: Header=BB7_25 Depth=3
	movq	104(%rsp), %rdx         # 8-byte Reload
	subq	%rax, %rdx
	movq	%rdx, %rax
	cqto
	idivq	144(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_52
# BB#51:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
.LBB7_52:                               #   in Loop: Header=BB7_25 Depth=3
	negl	%eax
.LBB7_53:                               #   in Loop: Header=BB7_25 Depth=3
	movw	%ax, 320(%rsp)
.LBB7_54:                               #   in Loop: Header=BB7_25 Depth=3
	movl	16(%rdi,%rsi,4), %ecx
	testl	%ecx, %ecx
	je	.LBB7_63
# BB#55:                                #   in Loop: Header=BB7_25 Depth=3
	movzwl	306(%rsp), %eax
	testw	%ax, %ax
	jne	.LBB7_63
# BB#56:                                #   in Loop: Header=BB7_25 Depth=3
	subl	12(%rsp), %r13d         # 4-byte Folded Reload
	subl	16(%rsp), %r13d         # 4-byte Folded Reload
	addl	20(%rsp), %r13d         # 4-byte Folded Reload
	movslq	%r13d, %rax
	imulq	248(%rsp), %rax         # 8-byte Folded Reload
	testq	%rax, %rax
	js	.LBB7_59
# BB#57:                                #   in Loop: Header=BB7_25 Depth=3
	addq	112(%rsp), %rax         # 8-byte Folded Reload
	cqto
	idivq	136(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_62
# BB#58:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	jmp	.LBB7_62
.LBB7_59:                               #   in Loop: Header=BB7_25 Depth=3
	movq	112(%rsp), %rdx         # 8-byte Reload
	subq	%rax, %rdx
	movq	%rdx, %rax
	cqto
	idivq	136(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_61
# BB#60:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
.LBB7_61:                               #   in Loop: Header=BB7_25 Depth=3
	negl	%eax
.LBB7_62:                               #   in Loop: Header=BB7_25 Depth=3
	movw	%ax, 306(%rsp)
.LBB7_63:                               #   in Loop: Header=BB7_25 Depth=3
	movl	%r12d, 12(%rsp)         # 4-byte Spill
	movl	20(%rdi,%rsi,4), %ecx
	testl	%ecx, %ecx
	movq	256(%rsp), %rdi         # 8-byte Reload
	je	.LBB7_72
# BB#64:                                #   in Loop: Header=BB7_25 Depth=3
	movzwl	292(%rsp), %eax
	testw	%ax, %ax
	jne	.LBB7_72
# BB#65:                                #   in Loop: Header=BB7_25 Depth=3
	leal	(%r15,%r15), %eax
	subl	%eax, %r14d
	addl	%r8d, %r14d
	movslq	%r14d, %rax
	imulq	176(%rsp), %rax         # 8-byte Folded Reload
	testq	%rax, %rax
	js	.LBB7_68
# BB#66:                                #   in Loop: Header=BB7_25 Depth=3
	addq	120(%rsp), %rax         # 8-byte Folded Reload
	cqto
	idivq	128(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_71
# BB#67:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
	jmp	.LBB7_71
.LBB7_68:                               #   in Loop: Header=BB7_25 Depth=3
	movq	120(%rsp), %rdx         # 8-byte Reload
	subq	%rax, %rdx
	movq	%rdx, %rax
	cqto
	idivq	128(%rsp)               # 8-byte Folded Reload
	testl	%ecx, %ecx
	jle	.LBB7_70
# BB#69:                                #   in Loop: Header=BB7_25 Depth=3
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	leal	-1(%rdx), %ecx
	cmpl	%edx, %eax
	cmovll	%eax, %ecx
	movl	%ecx, %eax
.LBB7_70:                               #   in Loop: Header=BB7_25 Depth=3
	negl	%eax
.LBB7_71:                               #   in Loop: Header=BB7_25 Depth=3
	movw	%ax, 292(%rsp)
.LBB7_72:                               #   in Loop: Header=BB7_25 Depth=3
	movq	%rdi, %r14
	movq	184(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rsi
	leaq	288(%rsp), %rbp
	movq	%rbp, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%r8d, %r13d
	movl	52(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r8d
	callq	*272(%rsp)              # 8-byte Folded Reload
	movl	%r13d, %edi
	subq	$-128, 24(%rsp)         # 8-byte Folded Spill
	movslq	36(%r12), %rax
	movq	%r14, %r12
	addl	%eax, %ebx
	movl	%ebx, 52(%rsp)          # 4-byte Spill
	movl	84(%rsp), %ecx          # 4-byte Reload
	incl	%ecx
	subq	$-128, 192(%rsp)        # 8-byte Folded Spill
	movq	200(%rsp), %rdx         # 8-byte Reload
	subq	$-128, %rdx
	movl	80(%rsp), %esi          # 4-byte Reload
	cmpl	%esi, %ecx
	movl	12(%rsp), %ebx          # 4-byte Reload
	jbe	.LBB7_25
# BB#73:                                #   in Loop: Header=BB7_18 Depth=2
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	240(%rsp), %rax         # 8-byte Reload
	incq	%rax
	cmpq	224(%rsp), %rax         # 8-byte Folded Reload
	movq	184(%rsp), %r15         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	jne	.LBB7_18
.LBB7_74:                               # %.loopexit
                                        #   in Loop: Header=BB7_9 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	addq	$96, %r15
	movslq	48(%r12), %rax
	movq	%rdx, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	cmpq	%rax, %rdx
	jl	.LBB7_9
.LBB7_75:                               # %._crit_edge
	movl	176(%r12), %ecx
	incl	%ecx
	movl	%ecx, 176(%r12)
	xorl	%eax, %eax
	cmpl	400(%r12), %ecx
	setae	%al
	addl	$3, %eax
.LBB7_76:                               # %.loopexit330
	addq	$424, %rsp              # imm = 0x1A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	decompress_smooth_data, .Lfunc_end7-decompress_smooth_data
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
