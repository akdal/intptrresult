	.text
	.file	"img_luma.bc"
	.globl	getSubImagesLuma
	.p2align	4, 0x90
	.type	getSubImagesLuma,@function
getSubImagesLuma:                       # @getSubImagesLuma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	6396(%r13), %r14d
	leal	20(%r14), %eax
	cmpl	$-19, %eax
	jl	.LBB0_18
# BB#1:                                 # %.lr.ph65
	movq	6440(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	decl	%r14d
	movslq	6392(%r13), %rdx
	movq	%rdx, %r12
	decq	%r12
	movq	$-20, %r15
	jmp	.LBB0_2
	.p2align	4, 0x90
.LBB0_17:                               # %._crit_edge._crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	incq	%r15
	movl	6392(%r13), %edx
.LBB0_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_11 Depth 2
                                        #     Child Loop BB0_15 Depth 2
	testq	%r15, %r15
	movl	$0, %ecx
	cmovgl	%r15d, %ecx
	cmpl	%r14d, %ecx
	cmovgl	%r14d, %ecx
	movq	6448(%r13), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	160(%rax,%r15,8), %rax
	leaq	40(%rax), %rdi
	movslq	%ecx, %rcx
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	(%rsi,%rcx,8), %rsi
	movzwl	(%rsi), %ecx
	movw	%cx, (%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 2(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 4(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 6(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 8(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 10(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 12(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 14(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 16(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 18(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 20(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 22(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 24(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 26(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 28(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 30(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 32(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 34(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 36(%rax)
	movzwl	(%rsi), %ecx
	movw	%cx, 38(%rax)
	leaq	(%rsi,%r12,2), %rcx
	movslq	%edx, %rdx
	leaq	19(%rdx), %rbp
	xorl	%ebx, %ebx
	testb	%bl, %bl
	movq	%rdx, %rbx
	jne	.LBB0_14
# BB#3:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$20, %r8d
	andq	$-16, %r8
	movq	%rdx, %rbx
	je	.LBB0_14
# BB#4:                                 # %vector.memcheck
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	40(%rax,%rdx,2), %rbx
	cmpq	%rcx, %rbx
	jae	.LBB0_6
# BB#5:                                 # %vector.memcheck
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	42(%rax,%rbp,2), %rbx
	cmpq	%rbx, %rcx
	movq	%rdx, %rbx
	jb	.LBB0_14
.LBB0_6:                                # %vector.body.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	leaq	-16(%r8), %rbx
	movq	%rbx, %r10
	shrq	$4, %r10
	btl	$4, %ebx
	jb	.LBB0_7
# BB#8:                                 # %vector.body.prol
                                        #   in Loop: Header=BB0_2 Depth=1
	movzwl	(%rcx), %ebx
	movd	%ebx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movdqu	%xmm0, (%rdi,%rdx,2)
	movdqu	%xmm0, 16(%rdi,%rdx,2)
	movl	$16, %r9d
	testq	%r10, %r10
	jne	.LBB0_10
	jmp	.LBB0_12
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	xorl	%r9d, %r9d
	testq	%r10, %r10
	je	.LBB0_12
.LBB0_10:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_2 Depth=1
	movzwl	(%rcx), %ebx
	movd	%ebx, %xmm0
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	movq	%r8, %rbx
	subq	%r9, %rbx
	addq	%rdx, %r9
	leaq	88(%rax,%r9,2), %rax
	.p2align	4, 0x90
.LBB0_11:                               # %vector.body
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -48(%rax)
	movdqu	%xmm0, -32(%rax)
	movdqu	%xmm0, -16(%rax)
	movdqu	%xmm0, (%rax)
	addq	$64, %rax
	addq	$-32, %rbx
	jne	.LBB0_11
.LBB0_12:                               # %middle.block
                                        #   in Loop: Header=BB0_2 Depth=1
	movl	$20, %eax
	cmpq	%r8, %rax
	je	.LBB0_16
# BB#13:                                #   in Loop: Header=BB0_2 Depth=1
	addq	%rdx, %r8
	movq	%r8, %rbx
	.p2align	4, 0x90
.LBB0_14:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	decq	%rbx
	.p2align	4, 0x90
.LBB0_15:                               # %scalar.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rcx), %eax
	movw	%ax, 2(%rdi,%rbx,2)
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB0_15
.LBB0_16:                               # %._crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	addq	%rdx, %rdx
	callq	memcpy
	movslq	6396(%r13), %rax
	addq	$19, %rax
	cmpq	%rax, %r15
	jl	.LBB0_17
.LBB0_18:                               # %._crit_edge66
	xorl	%esi, %esi
	movl	$2, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	callq	getHorSubImageSixTap
	movl	$2, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	callq	getVerSubImageSixTap
	movl	$2, %esi
	movl	$2, %edx
	xorl	%ecx, %ecx
	movl	$2, %r8d
	movl	$1, %r9d
	movq	%r13, %rdi
	callq	getVerSubImageSixTap
	xorl	%esi, %esi
	movl	$1, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	pushq	$0
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	getHorSubImageBiLinear
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	xorl	%esi, %esi
	movl	$3, %edx
	xorl	%ecx, %ecx
	movl	$2, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	pushq	$1
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	getHorSubImageBiLinear
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	movl	$2, %esi
	movl	$1, %edx
	movl	$2, %ecx
	movl	$0, %r8d
	movl	$2, %r9d
	movq	%r13, %rdi
	pushq	$0
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	callq	getHorSubImageBiLinear
	addq	$16, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset -16
	movl	$2, %esi
	movl	$3, %edx
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%r13, %rdi
	pushq	$1
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	getHorSubImageBiLinear
	addq	$16, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -16
	movl	$1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$2, %r9d
	movq	%r13, %rdi
	pushq	$0
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	getVerSubImageBiLinear
	addq	$16, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -16
	movl	$1, %esi
	movl	$2, %edx
	movl	$0, %ecx
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%r13, %rdi
	pushq	$0
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	callq	getVerSubImageBiLinear
	addq	$16, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset -16
	movl	$3, %esi
	xorl	%edx, %edx
	movl	$2, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	pushq	$1
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	getVerSubImageBiLinear
	addq	$16, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -16
	movl	$3, %esi
	movl	$2, %edx
	movl	$2, %ecx
	movl	$2, %r8d
	movl	$0, %r9d
	movq	%r13, %rdi
	pushq	$1
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	callq	getVerSubImageBiLinear
	addq	$16, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset -16
	subq	$8, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	movl	$1, %esi
	movl	$1, %edx
	movl	$0, %ecx
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%r13, %rdi
	pushq	$0
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	getDiagSubImageBiLinear
	addq	$48, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -48
	subq	$8, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	movl	$1, %esi
	movl	$3, %edx
	movl	$0, %ecx
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%r13, %rdi
	pushq	$1
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	callq	getDiagSubImageBiLinear
	addq	$48, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset -48
	subq	$8, %rsp
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	movl	$3, %esi
	movl	$1, %edx
	movl	$2, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%r13, %rdi
	pushq	$0
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	callq	getDiagSubImageBiLinear
	addq	$48, %rsp
.Lcfi57:
	.cfi_adjust_cfa_offset -48
	subq	$8, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	movl	$3, %esi
	movl	$3, %edx
	movl	$0, %ecx
	movl	$2, %r8d
	movl	$2, %r9d
	movq	%r13, %rdi
	pushq	$1
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	callq	getDiagSubImageBiLinear
	addq	$56, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset -48
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	getSubImagesLuma, .Lfunc_end0-getSubImagesLuma
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
.LCPI1_1:
	.long	4294967291              # 0xfffffffb
	.long	4294967291              # 0xfffffffb
	.long	4294967291              # 0xfffffffb
	.long	4294967291              # 0xfffffffb
.LCPI1_2:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.text
	.globl	getHorSubImageSixTap
	.p2align	4, 0x90
	.type	getHorSubImageSixTap,@function
getHorSubImageSixTap:                   # @getHorSubImageSixTap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movslq	6396(%rdi), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	cmpq	$-39, %rax
	jl	.LBB1_14
# BB#1:                                 # %.lr.ph118
	movslq	6392(%rdi), %r12
	leaq	39(%r12), %rbp
	movq	6448(%rdi), %rdi
	movslq	%ecx, %rcx
	movq	(%rdi,%rcx,8), %rcx
	movslq	%r8d, %rbx
	movq	(%rcx,%rbx,8), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movslq	%esi, %rcx
	movq	(%rdi,%rcx,8), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	imgY_sub_tmp(%rip), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movl	$15520, %r13d           # imm = 0x3CA0
	addq	img(%rip), %r13
	leaq	37(%r12), %rcx
	leal	1(%rcx), %edx
	movslq	%edx, %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	cmpq	%rbp, %rbp
	movl	%ebp, %edx
	cmovll	%edx, %edx
	movslq	%edx, %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leaq	40(%r12), %rdx
	cmpq	%rdx, %rbp
	cmovll	%ebp, %edx
	movslq	%edx, %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	leaq	41(%r12), %rdx
	cmpq	%rdx, %rbp
	cmovll	%ebp, %edx
	movslq	%edx, %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	leaq	42(%r12), %rdx
	cmpq	%rdx, %rbp
	cmovll	%ebp, %edx
	movslq	%edx, %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	addq	$39, -88(%rsp)          # 8-byte Folded Spill
	movl	%ecx, %eax
	leaq	-2(%rax), %rcx
	movq	%rax, -120(%rsp)        # 8-byte Spill
	leaq	3(%rax), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	andq	$-4, %rcx
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	leaq	2(%rcx), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	pxor	%xmm0, %xmm0
	movdqa	.LCPI1_0(%rip), %xmm1   # xmm1 = [20,20,20,20]
	movdqa	.LCPI1_1(%rip), %xmm2   # xmm2 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI1_2(%rip), %xmm8   # xmm8 = [16,16,16,16]
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader113119
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_9 Depth 2
                                        #     Child Loop BB1_12 Depth 2
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r8,8), %r10
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r8,8), %rbx
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r8,8), %rax
	movzwl	(%r10), %ecx
	movzwl	2(%r10), %edx
	addl	%ecx, %edx
	leal	(%rdx,%rdx,4), %edx
	movzwl	4(%r10), %esi
	addl	%ecx, %esi
	imull	$-5, %esi, %esi
	movzwl	6(%r10), %ebp
	leal	(%rcx,%rdx,4), %ecx
	addl	%esi, %ecx
	leal	(%rcx,%rbp), %edx
	movl	(%r13), %esi
	leal	16(%rbp,%rcx), %ecx
	sarl	$5, %ecx
	cmovsl	%edi, %ecx
	cmpl	%esi, %ecx
	cmovlw	%cx, %si
	movw	%si, (%rbx)
	movl	%edx, (%rax)
	movzwl	2(%r10), %ecx
	movzwl	4(%r10), %edx
	addl	%ecx, %edx
	leal	(%rdx,%rdx,4), %ecx
	movzwl	(%r10), %edx
	movzwl	6(%r10), %esi
	addl	%edx, %esi
	imull	$-5, %esi, %esi
	movzwl	8(%r10), %ebp
	leal	(%rdx,%rcx,4), %ecx
	addl	%esi, %ecx
	leal	(%rcx,%rbp), %edx
	movl	(%r13), %esi
	leal	16(%rbp,%rcx), %ecx
	sarl	$5, %ecx
	cmovsl	%edi, %ecx
	cmpl	%esi, %ecx
	cmovlw	%cx, %si
	movq	%rbx, -104(%rsp)        # 8-byte Spill
	movw	%si, 2(%rbx)
	movq	%rax, -112(%rsp)        # 8-byte Spill
	movl	%edx, 4(%rax)
	cmpl	$3, -120(%rsp)          # 4-byte Folded Reload
	movl	$0, %r15d
	jl	.LBB1_13
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpq	$4, -96(%rsp)           # 8-byte Folded Reload
	jb	.LBB1_4
# BB#5:                                 # %min.iters.checked
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpq	$0, -128(%rsp)          # 8-byte Folded Reload
	je	.LBB1_4
# BB#6:                                 # %vector.memcheck
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	-104(%rsp), %rcx        # 8-byte Reload
	leaq	4(%rcx), %r11
	movq	-120(%rsp), %rax        # 8-byte Reload
	leaq	(%rcx,%rax,2), %rcx
	movq	-72(%rsp), %rdx         # 8-byte Reload
	leaq	(%r10,%rdx,2), %rdx
	movq	-112(%rsp), %rsi        # 8-byte Reload
	leaq	8(%rsi), %r14
	leaq	(%rsi,%rax,4), %rsi
	cmpq	%rdx, %r11
	sbbb	%dl, %dl
	cmpq	%rcx, %r10
	sbbb	%bl, %bl
	andb	%dl, %bl
	cmpq	%r13, %r14
	sbbb	%cl, %cl
	cmpq	%rsi, %r13
	sbbb	%dl, %dl
	testb	$1, %bl
	jne	.LBB1_4
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB1_2 Depth=1
	andb	%dl, %cl
	andb	$1, %cl
	movl	$2, %ebp
	movl	$0, %r15d
	jne	.LBB1_11
# BB#8:                                 # %vector.body.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movd	(%r13), %xmm4           # xmm4 = mem[0],zero,zero,zero
	pshufd	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	leaq	10(%r10), %r15
	movq	-128(%rsp), %r9         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_9:                                # %vector.body
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-6(%r15), %xmm5         # xmm5 = mem[0],zero
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	movq	-4(%r15), %xmm6         # xmm6 = mem[0],zero
	punpcklwd	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1],xmm6[2],xmm0[2],xmm6[3],xmm0[3]
	paddd	%xmm5, %xmm6
	pshufd	$245, %xmm6, %xmm5      # xmm5 = xmm6[1,1,3,3]
	pmuludq	%xmm1, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm1, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movq	-8(%r15), %xmm5         # xmm5 = mem[0],zero
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	movq	-2(%r15), %xmm7         # xmm7 = mem[0],zero
	punpcklwd	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1],xmm7[2],xmm0[2],xmm7[3],xmm0[3]
	paddd	%xmm5, %xmm7
	pshufd	$245, %xmm7, %xmm5      # xmm5 = xmm7[1,1,3,3]
	pmuludq	%xmm2, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	movq	-10(%r15), %xmm5        # xmm5 = mem[0],zero
	punpcklwd	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1],xmm5[2],xmm0[2],xmm5[3],xmm0[3]
	movq	(%r15), %xmm3           # xmm3 = mem[0],zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	paddd	%xmm6, %xmm5
	paddd	%xmm7, %xmm3
	paddd	%xmm5, %xmm3
	movdqa	%xmm3, %xmm5
	paddd	%xmm8, %xmm5
	psrad	$5, %xmm5
	movdqa	%xmm5, %xmm6
	pcmpgtd	%xmm0, %xmm6
	pand	%xmm5, %xmm6
	movdqa	%xmm4, %xmm5
	pcmpgtd	%xmm6, %xmm5
	pand	%xmm5, %xmm6
	pandn	%xmm4, %xmm5
	por	%xmm6, %xmm5
	pshuflw	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	movq	%xmm5, (%r11)
	movdqu	%xmm3, (%r14)
	addq	$8, %r15
	addq	$8, %r11
	addq	$16, %r14
	addq	$-4, %r9
	jne	.LBB1_9
# BB#10:                                # %middle.block
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	-128(%rsp), %rcx        # 8-byte Reload
	cmpq	%rcx, -96(%rsp)         # 8-byte Folded Reload
	movq	-80(%rsp), %rbp         # 8-byte Reload
	movl	$0, %r15d
	jne	.LBB1_11
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movl	$2, %ebp
	xorl	%r15d, %r15d
.LBB1_11:                               # %.lr.ph.preheader142
                                        #   in Loop: Header=BB1_2 Depth=1
	leaq	(%r10,%rbp,2), %rcx
	movq	-104(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rbp,2), %rdx
	movq	-112(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rbp,4), %rbx
	movq	-120(%rsp), %rsi        # 8-byte Reload
	subq	%rbp, %rsi
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rcx), %ebp
	movzwl	2(%rcx), %eax
	addl	%ebp, %eax
	leal	(%rax,%rax,4), %r9d
	movzwl	-2(%rcx), %ebp
	movzwl	4(%rcx), %edi
	addl	%ebp, %edi
	imull	$-5, %edi, %edi
	movzwl	-4(%rcx), %ebp
	movzwl	6(%rcx), %eax
	leaq	2(%rcx), %rcx
	leal	(%rbp,%r9,4), %ebp
	addl	%edi, %ebp
	leal	(%rbp,%rax), %r9d
	movl	(%r13), %edi
	leal	16(%rax,%rbp), %eax
	sarl	$5, %eax
	cmovsl	%r15d, %eax
	cmpl	%edi, %eax
	cmovlw	%ax, %di
	movw	%di, (%rdx)
	movl	%r9d, (%rbx)
	addq	$2, %rdx
	addq	$4, %rbx
	decq	%rsi
	jne	.LBB1_12
.LBB1_13:                               # %.loopexit
                                        #   in Loop: Header=BB1_2 Depth=1
	movzwl	74(%r10,%r12,2), %eax
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movzwl	(%r10,%rcx,2), %ecx
	addl	%eax, %ecx
	leal	(%rcx,%rcx,4), %eax
	movzwl	72(%r10,%r12,2), %ecx
	movq	-40(%rsp), %rsi         # 8-byte Reload
	movzwl	(%r10,%rsi,2), %edx
	addl	%ecx, %edx
	imull	$-5, %edx, %ecx
	movzwl	70(%r10,%r12,2), %edx
	leal	(%rdx,%rax,4), %eax
	movq	-48(%rsp), %rdi         # 8-byte Reload
	movzwl	(%r10,%rdi,2), %edx
	addl	%ecx, %eax
	leal	(%rax,%rdx), %ecx
	leal	16(%rdx,%rax), %eax
	movl	(%r13), %edx
	sarl	$5, %eax
	cmovsl	%r15d, %eax
	cmpl	%edx, %eax
	cmovlw	%ax, %dx
	movq	-104(%rsp), %rbp        # 8-byte Reload
	movw	%dx, 74(%rbp,%r12,2)
	movq	-112(%rsp), %rbx        # 8-byte Reload
	movl	%ecx, 148(%rbx,%r12,4)
	movzwl	76(%r10,%r12,2), %eax
	movzwl	(%r10,%rsi,2), %ecx
	addl	%eax, %ecx
	leal	(%rcx,%rcx,4), %eax
	movzwl	74(%r10,%r12,2), %ecx
	movzwl	(%r10,%rdi,2), %edx
	addl	%ecx, %edx
	imull	$-5, %edx, %ecx
	movzwl	72(%r10,%r12,2), %edx
	leal	(%rdx,%rax,4), %eax
	movq	-56(%rsp), %rsi         # 8-byte Reload
	movzwl	(%r10,%rsi,2), %edx
	addl	%ecx, %eax
	leal	(%rax,%rdx), %ecx
	leal	16(%rdx,%rax), %eax
	movl	(%r13), %edx
	sarl	$5, %eax
	cmovsl	%r15d, %eax
	cmpl	%edx, %eax
	cmovlw	%ax, %dx
	movw	%dx, 76(%rbp,%r12,2)
	movl	%ecx, 152(%rbx,%r12,4)
	movzwl	78(%r10,%r12,2), %eax
	movzwl	(%r10,%rdi,2), %ecx
	addl	%eax, %ecx
	leal	(%rcx,%rcx,4), %eax
	movzwl	76(%r10,%r12,2), %ecx
	movzwl	(%r10,%rsi,2), %edx
	addl	%ecx, %edx
	imull	$-5, %edx, %ecx
	movzwl	74(%r10,%r12,2), %edx
	movq	-64(%rsp), %rsi         # 8-byte Reload
	movzwl	(%r10,%rsi,2), %esi
	leal	(%rdx,%rax,4), %eax
	addl	%ecx, %eax
	movl	(%r13), %ecx
	leal	16(%rsi,%rax), %edx
	sarl	$5, %edx
	cmovsl	%r15d, %edx
	cmpl	%ecx, %edx
	cmovlw	%dx, %cx
	movw	%cx, 78(%rbp,%r12,2)
	leal	(%rax,%rsi), %eax
	movl	%eax, 156(%rbx,%r12,4)
	cmpq	-88(%rsp), %r8          # 8-byte Folded Reload
	leaq	1(%r8), %r8
	movl	$0, %edi
	jl	.LBB1_2
.LBB1_14:                               # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	getHorSubImageSixTap, .Lfunc_end1-getHorSubImageSixTap
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	20                      # 0x14
.LCPI2_1:
	.long	4294967291              # 0xfffffffb
	.long	4294967291              # 0xfffffffb
	.long	4294967291              # 0xfffffffb
	.long	4294967291              # 0xfffffffb
.LCPI2_2:
	.long	512                     # 0x200
	.long	512                     # 0x200
	.long	512                     # 0x200
	.long	512                     # 0x200
.LCPI2_3:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.text
	.globl	getVerSubImageSixTap
	.p2align	4, 0x90
	.type	getVerSubImageSixTap,@function
getVerSubImageSixTap:                   # @getVerSubImageSixTap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$16, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 72
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movslq	6396(%rdi), %r15
	leaq	40(%r15), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	6392(%rdi), %rbp
	movq	%rbp, %rbx
	addq	$40, %rbx
	leaq	39(%r15), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	6448(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rsi
	movslq	%edx, %rdx
	movq	(%rsi,%rdx,8), %rdx
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	testl	%r9d, %r9d
	movq	%rbx, %rdx
	movq	%rdx, -80(%rsp)         # 8-byte Spill
	movq	%rbp, -24(%rsp)         # 8-byte Spill
	je	.LBB2_21
# BB#1:                                 # %.preheader269
	cmpl	$-39, %ebp
	jl	.LBB2_16
# BB#2:                                 # %.preheader269.split.us.preheader
	movq	%r15, -104(%rsp)        # 8-byte Spill
	movq	imgY_sub_tmp(%rip), %r14
	movq	img(%rip), %rax
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx), %r12
	movq	(%r14), %r9
	movq	8(%r14), %r8
	movq	16(%r14), %r10
	movq	24(%r14), %r13
	movl	15520(%rax), %r11d
	testq	%rbx, %rbx
	movl	$1, %edi
	cmovgq	%rbx, %rdi
	xorl	%r15d, %r15d
	cmpq	$4, %rdi
	jb	.LBB2_3
# BB#4:                                 # %min.iters.checked
	movq	%rdi, %rbp
	movabsq	$9223372036854775804, %rax # imm = 0x7FFFFFFFFFFFFFFC
	andq	%rax, %rbp
	je	.LBB2_3
# BB#5:                                 # %vector.ph
	movq	%r14, -128(%rsp)        # 8-byte Spill
	movd	%r11d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	.LCPI2_0(%rip), %xmm1   # xmm1 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm2   # xmm2 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_2(%rip), %xmm8   # xmm8 = [512,512,512,512]
	pxor	%xmm4, %xmm4
	movq	%rbp, %rcx
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r10, %rax
	movq	%r8, %rbx
	movq	%r9, %r14
	.p2align	4, 0x90
.LBB2_6:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r14), %xmm5
	movdqu	(%rbx), %xmm6
	paddd	%xmm5, %xmm6
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm1, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm1, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	punpckldq	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	movdqu	(%rax), %xmm7
	paddd	%xmm5, %xmm7
	pshufd	$245, %xmm7, %xmm3      # xmm3 = xmm7[1,1,3,3]
	pmuludq	%xmm2, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	movdqu	(%rdx), %xmm3
	paddd	%xmm6, %xmm7
	paddd	%xmm5, %xmm3
	paddd	%xmm7, %xmm3
	paddd	%xmm8, %xmm3
	psrad	$10, %xmm3
	movdqa	%xmm3, %xmm5
	pcmpgtd	%xmm4, %xmm5
	pand	%xmm3, %xmm5
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm5, %xmm3
	pand	%xmm3, %xmm5
	pandn	%xmm0, %xmm3
	por	%xmm5, %xmm3
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	movq	%xmm3, (%rsi)
	addq	$16, %r14
	addq	$16, %rbx
	addq	$16, %rax
	addq	$16, %rdx
	addq	$8, %rsi
	addq	$-4, %rcx
	jne	.LBB2_6
# BB#7:                                 # %middle.block
	cmpq	%rbp, %rdi
	movq	-80(%rsp), %rdi         # 8-byte Reload
	movq	-128(%rsp), %r14        # 8-byte Reload
	jne	.LBB2_8
	jmp	.LBB2_9
.LBB2_21:                               # %.preheader262
	movq	%r15, -104(%rsp)        # 8-byte Spill
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%r8d, %rcx
	movq	(%rax,%rcx,8), %r13
	cmpl	$-39, %ebp
	movq	%r13, -40(%rsp)         # 8-byte Spill
	jl	.LBB2_50
# BB#22:                                # %.preheader262.split.us.preheader
	movq	img(%rip), %rax
	movslq	-80(%rsp), %r15         # 4-byte Folded Reload
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	%r13, %rdx
	movq	(%rcx), %r13
	movq	(%rdx), %r9
	movq	8(%rdx), %rsi
	movq	16(%rdx), %r14
	movq	24(%rdx), %r11
	movl	15520(%rax), %r10d
	testq	%r15, %r15
	movl	$1, %edx
	cmovgq	%r15, %rdx
	xorl	%r8d, %r8d
	cmpq	$8, %rdx
	movq	%rsi, -128(%rsp)        # 8-byte Spill
	jae	.LBB2_24
# BB#23:
	xorl	%eax, %eax
	jmp	.LBB2_35
.LBB2_3:
	xorl	%ebp, %ebp
	movq	-80(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_8:                                # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r9,%rbp,4), %eax
	movl	(%r8,%rbp,4), %ecx
	addl	%eax, %ecx
	leal	(%rcx,%rcx,4), %ecx
	movl	(%r10,%rbp,4), %edx
	addl	%eax, %edx
	imull	$-5, %edx, %edx
	movl	(%r13,%rbp,4), %esi
	leal	(%rdx,%rcx,4), %ecx
	addl	%eax, %ecx
	leal	512(%rsi,%rcx), %eax
	sarl	$10, %eax
	cmovsl	%r15d, %eax
	cmpl	%r11d, %eax
	cmovgew	%r11w, %ax
	movw	%ax, (%r12,%rbp,2)
	incq	%rbp
	cmpq	%rdi, %rbp
	jl	.LBB2_8
.LBB2_9:                                # %..loopexit268_crit_edge.us
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	8(%rax), %r12
	movq	32(%r14), %r14
	testq	%rdi, %rdi
	movl	$1, %ecx
	cmovgq	%rdi, %rcx
	xorl	%ebp, %ebp
	cmpq	$3, %rcx
	jbe	.LBB2_10
# BB#12:                                # %min.iters.checked355
	movq	%rcx, %rbx
	movabsq	$9223372036854775804, %rax # imm = 0x7FFFFFFFFFFFFFFC
	andq	%rax, %rbx
	je	.LBB2_10
# BB#13:                                # %vector.ph359
	movq	%rcx, %rdi
	movd	%r11d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	.LCPI2_0(%rip), %xmm1   # xmm1 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm2   # xmm2 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_2(%rip), %xmm8   # xmm8 = [512,512,512,512]
	pxor	%xmm4, %xmm4
	movq	%rbx, %rbp
	movq	%r12, -128(%rsp)        # 8-byte Spill
	movq	%r12, %rax
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r9, %rdx
	movq	%r10, %r12
	movq	%r8, %r15
	.p2align	4, 0x90
.LBB2_14:                               # %vector.body350
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r15), %xmm5
	movdqu	(%r12), %xmm6
	paddd	%xmm5, %xmm6
	pshufd	$245, %xmm6, %xmm5      # xmm5 = xmm6[1,1,3,3]
	pmuludq	%xmm1, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm1, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movdqu	(%rdx), %xmm5
	movdqu	(%rsi), %xmm7
	paddd	%xmm5, %xmm7
	pshufd	$245, %xmm7, %xmm3      # xmm3 = xmm7[1,1,3,3]
	pmuludq	%xmm2, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	movdqu	(%rcx), %xmm3
	paddd	%xmm6, %xmm7
	paddd	%xmm5, %xmm3
	paddd	%xmm7, %xmm3
	paddd	%xmm8, %xmm3
	psrad	$10, %xmm3
	movdqa	%xmm3, %xmm5
	pcmpgtd	%xmm4, %xmm5
	pand	%xmm3, %xmm5
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm5, %xmm3
	pand	%xmm3, %xmm5
	pandn	%xmm0, %xmm3
	por	%xmm5, %xmm3
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	movq	%xmm3, (%rax)
	addq	$16, %r15
	addq	$16, %r12
	addq	$16, %rdx
	addq	$16, %rsi
	addq	$16, %rcx
	addq	$8, %rax
	addq	$-4, %rbp
	jne	.LBB2_14
# BB#15:                                # %middle.block351
	cmpq	%rbx, %rdi
	movq	-104(%rsp), %r15        # 8-byte Reload
	movq	-80(%rsp), %rdi         # 8-byte Reload
	movq	-128(%rsp), %r12        # 8-byte Reload
	movl	$0, %ebp
	jne	.LBB2_11
	jmp	.LBB2_16
.LBB2_10:
	xorl	%ebx, %ebx
	movq	-104(%rsp), %r15        # 8-byte Reload
	.p2align	4, 0x90
.LBB2_11:                               # %scalar.ph352
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r10,%rbx,4), %eax
	addl	(%r8,%rbx,4), %eax
	leal	(%rax,%rax,4), %eax
	movl	(%r9,%rbx,4), %ecx
	movl	(%r13,%rbx,4), %edx
	addl	%ecx, %edx
	imull	$-5, %edx, %edx
	movl	(%r14,%rbx,4), %esi
	leal	(%rdx,%rax,4), %eax
	addl	%ecx, %eax
	leal	512(%rsi,%rax), %eax
	sarl	$10, %eax
	cmovsl	%ebp, %eax
	cmpl	%r11d, %eax
	cmovgew	%r11w, %ax
	movw	%ax, (%r12,%rbx,2)
	incq	%rbx
	cmpq	%rdi, %rbx
	jl	.LBB2_11
.LBB2_16:                               # %.preheader267
	addl	$37, %r15d
	movq	%r15, -104(%rsp)        # 8-byte Spill
	cmpl	$3, %r15d
	movabsq	$9223372036854775804, %rbx # imm = 0x7FFFFFFFFFFFFFFC
	movq	%rbx, %r8
	jl	.LBB2_99
# BB#17:                                # %.lr.ph286
	cmpl	$-39, -24(%rsp)         # 4-byte Folded Reload
	movq	-104(%rsp), %rcx        # 8-byte Reload
	jl	.LBB2_123
# BB#18:                                # %.lr.ph286.split.us.preheader
	movq	imgY_sub_tmp(%rip), %r10
	movq	img(%rip), %rax
	movslq	-80(%rsp), %rbx         # 4-byte Folded Reload
	movq	(%r10), %r11
	movq	16(%r10), %r15
	movq	24(%r10), %rbp
	movq	32(%r10), %r13
	movl	15520(%rax), %edi
	movl	%ecx, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	testq	%rbx, %rbx
	movl	$1, %eax
	cmovgq	%rbx, %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	andq	%r8, %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movd	%edi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movl	$2, %ecx
	movdqa	.LCPI2_0(%rip), %xmm1   # xmm1 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm2   # xmm2 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_2(%rip), %xmm8   # xmm8 = [512,512,512,512]
	pxor	%xmm4, %xmm4
	movq	%r10, -112(%rsp)        # 8-byte Spill
	movq	%rbx, -72(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph286.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_95 Depth 2
                                        #     Child Loop BB2_97 Depth 2
	movq	%rbp, %r12
	movq	%r13, %rbp
	cmpq	$4, -120(%rsp)          # 8-byte Folded Reload
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %r9
	movq	-8(%r10,%rcx,8), %r14
	movq	24(%r10,%rcx,8), %r13
	leaq	1(%rcx), %rcx
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	jae	.LBB2_92
# BB#20:                                #   in Loop: Header=BB2_19 Depth=1
	xorl	%eax, %eax
	jmp	.LBB2_97
	.p2align	4, 0x90
.LBB2_92:                               # %min.iters.checked379
                                        #   in Loop: Header=BB2_19 Depth=1
	movq	-40(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB2_93
# BB#94:                                # %vector.ph383
                                        #   in Loop: Header=BB2_19 Depth=1
	movq	%rbp, %r8
	movq	%rax, %r10
	movq	%r9, %rax
	movq	%r13, %rsi
	movq	%r11, %rcx
	movq	%r8, -96(%rsp)          # 8-byte Spill
	movq	%r14, %rdx
	movq	%r12, %rbp
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB2_95:                               # %vector.body374
                                        #   Parent Loop BB2_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rbx), %xmm5
	movdqu	(%rbp), %xmm6
	paddd	%xmm5, %xmm6
	pshufd	$245, %xmm6, %xmm5      # xmm5 = xmm6[1,1,3,3]
	pmuludq	%xmm1, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm1, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movdqu	(%rdx), %xmm5
	movdqu	(%r8), %xmm7
	paddd	%xmm5, %xmm7
	pshufd	$245, %xmm7, %xmm5      # xmm5 = xmm7[1,1,3,3]
	pmuludq	%xmm2, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	movdqu	(%rcx), %xmm5
	movdqu	(%rsi), %xmm3
	paddd	%xmm6, %xmm7
	paddd	%xmm5, %xmm3
	paddd	%xmm7, %xmm3
	paddd	%xmm8, %xmm3
	psrad	$10, %xmm3
	movdqa	%xmm3, %xmm5
	pcmpgtd	%xmm4, %xmm5
	pand	%xmm3, %xmm5
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm5, %xmm3
	pand	%xmm3, %xmm5
	pandn	%xmm0, %xmm3
	por	%xmm5, %xmm3
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	movq	%xmm3, (%rax)
	addq	$16, %rbx
	addq	$16, %rbp
	addq	$16, %rdx
	addq	$16, %r8
	addq	$16, %rcx
	addq	$16, %rsi
	addq	$8, %rax
	addq	$-4, %r10
	jne	.LBB2_95
# BB#96:                                # %middle.block375
                                        #   in Loop: Header=BB2_19 Depth=1
	movq	-40(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, -120(%rsp)        # 8-byte Folded Reload
	movabsq	$9223372036854775804, %rbx # imm = 0x7FFFFFFFFFFFFFFC
	movq	%rbx, %r8
	movq	-112(%rsp), %r10        # 8-byte Reload
	movq	-72(%rsp), %rbx         # 8-byte Reload
	movq	-96(%rsp), %rbp         # 8-byte Reload
	jne	.LBB2_97
	jmp	.LBB2_98
.LBB2_93:                               #   in Loop: Header=BB2_19 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_97:                               # %scalar.ph376
                                        #   Parent Loop BB2_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12,%rax,4), %ecx
	addl	(%r15,%rax,4), %ecx
	leal	(%rcx,%rcx,4), %ecx
	movl	(%rbp,%rax,4), %edx
	addl	(%r14,%rax,4), %edx
	imull	$-5, %edx, %edx
	movl	(%r13,%rax,4), %esi
	leal	(%rdx,%rcx,4), %ecx
	xorl	%edx, %edx
	addl	(%r11,%rax,4), %ecx
	leal	512(%rsi,%rcx), %ecx
	sarl	$10, %ecx
	cmovsl	%edx, %ecx
	cmpl	%edi, %ecx
	cmovgew	%di, %cx
	movw	%cx, (%r9,%rax,2)
	incq	%rax
	cmpq	%rbx, %rax
	jl	.LBB2_97
.LBB2_98:                               # %..loopexit266_crit_edge.us
                                        #   in Loop: Header=BB2_19 Depth=1
	movq	-128(%rsp), %rcx        # 8-byte Reload
	cmpq	-88(%rsp), %rcx         # 8-byte Folded Reload
	movq	%r14, %r11
	movq	%r12, %r15
	jne	.LBB2_19
.LBB2_99:                               # %.preheader264
	cmpl	$-39, -24(%rsp)         # 4-byte Folded Reload
	movq	-104(%rsp), %rcx        # 8-byte Reload
	jl	.LBB2_123
# BB#100:                               # %.preheader264.split.us.preheader
	movq	%r8, %rbx
	movq	imgY_sub_tmp(%rip), %r8
	movq	img(%rip), %rax
	movslq	-80(%rsp), %r10         # 4-byte Folded Reload
	movslq	%ecx, %r11
	movslq	-8(%rsp), %rsi          # 4-byte Folded Reload
	movslq	(%rsp), %rcx            # 4-byte Folded Reload
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r11,8), %rcx
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	leaq	1(%r11), %rdi
	leaq	2(%r11), %rcx
	cmpq	%rcx, %rsi
	movq	%rcx, -24(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	cmovll	%esi, %ecx
	leaq	3(%r11), %rdx
	cmpq	%rdx, %rsi
	movq	%rdx, -40(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%rsi, -72(%rsp)         # 8-byte Spill
	cmovll	%esi, %edx
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movslq	%edi, %rsi
	movq	(%r8,%rsi,8), %rsi
	movq	%rsi, -104(%rsp)        # 8-byte Spill
	movslq	%ecx, %rcx
	movq	(%r8,%rcx,8), %rcx
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	movslq	%edx, %rcx
	movq	(%r8,%rcx,8), %r13
	movl	15520(%rax), %r12d
	testq	%r10, %r10
	movl	$1, %ecx
	cmovgq	%r10, %rcx
	cmpq	$4, %rcx
	movq	(%r8,%r11,8), %rbp
	movq	-8(%r8,%r11,8), %r15
	movq	-16(%r8,%r11,8), %r14
	movq	%rbp, -128(%rsp)        # 8-byte Spill
	jb	.LBB2_101
# BB#102:                               # %min.iters.checked404
	movq	%rcx, %r9
	andq	%rbx, %r9
	je	.LBB2_101
# BB#103:                               # %vector.ph408
	movq	%r11, -64(%rsp)         # 8-byte Spill
	movq	%r8, -56(%rsp)          # 8-byte Spill
	movq	%r10, -112(%rsp)        # 8-byte Spill
	movd	%r12d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	.LCPI2_0(%rip), %xmm1   # xmm1 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm2   # xmm2 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_2(%rip), %xmm8   # xmm8 = [512,512,512,512]
	pxor	%xmm4, %xmm4
	movq	%r9, %r8
	movq	-120(%rsp), %rbx        # 8-byte Reload
	movq	%rbp, %rax
	movq	%r13, %r10
	movq	%r14, %r11
	movq	-88(%rsp), %rsi         # 8-byte Reload
	movq	%r15, -96(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rdx        # 8-byte Reload
	.p2align	4, 0x90
.LBB2_104:                              # %vector.body399
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%rax), %xmm5
	movdqu	(%rdx), %xmm6
	paddd	%xmm5, %xmm6
	pshufd	$245, %xmm6, %xmm5      # xmm5 = xmm6[1,1,3,3]
	pmuludq	%xmm1, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm1, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movdqu	(%r15), %xmm5
	movdqu	(%rsi), %xmm7
	paddd	%xmm5, %xmm7
	pshufd	$245, %xmm7, %xmm5      # xmm5 = xmm7[1,1,3,3]
	pmuludq	%xmm2, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	movdqu	(%r11), %xmm5
	movdqu	(%r10), %xmm3
	paddd	%xmm6, %xmm7
	paddd	%xmm5, %xmm3
	paddd	%xmm7, %xmm3
	paddd	%xmm8, %xmm3
	psrad	$10, %xmm3
	movdqa	%xmm3, %xmm5
	pcmpgtd	%xmm4, %xmm5
	pand	%xmm3, %xmm5
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm5, %xmm3
	pand	%xmm3, %xmm5
	pandn	%xmm0, %xmm3
	por	%xmm5, %xmm3
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	movq	%xmm3, (%rbx)
	addq	$16, %rax
	addq	$16, %rdx
	addq	$16, %r15
	addq	$16, %rsi
	addq	$16, %r11
	addq	$16, %r10
	addq	$8, %rbx
	addq	$-4, %r8
	jne	.LBB2_104
# BB#105:                               # %middle.block400
	cmpq	%r9, %rcx
	movq	-112(%rsp), %r10        # 8-byte Reload
	movq	-128(%rsp), %rbp        # 8-byte Reload
	movq	-56(%rsp), %r8          # 8-byte Reload
	movq	-64(%rsp), %r11         # 8-byte Reload
	movq	-96(%rsp), %r15         # 8-byte Reload
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	-104(%rsp), %rbx        # 8-byte Reload
	movq	-88(%rsp), %rdi         # 8-byte Reload
	jne	.LBB2_106
	jmp	.LBB2_107
.LBB2_101:
	xorl	%r9d, %r9d
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movq	-104(%rsp), %rbx        # 8-byte Reload
	movq	-88(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_106:                              # %scalar.ph401
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx,%r9,4), %eax
	addl	(%rbp,%r9,4), %eax
	leal	(%rax,%rax,4), %eax
	movl	(%rdi,%r9,4), %edx
	addl	(%r15,%r9,4), %edx
	imull	$-5, %edx, %edx
	movl	(%r13,%r9,4), %esi
	leal	(%rdx,%rax,4), %eax
	xorl	%edx, %edx
	addl	(%r14,%r9,4), %eax
	leal	512(%rsi,%rax), %eax
	sarl	$10, %eax
	cmovsl	%edx, %eax
	cmpl	%r12d, %eax
	cmovgew	%r12w, %ax
	movw	%ax, (%rcx,%r9,2)
	incq	%r9
	cmpq	%r10, %r9
	jl	.LBB2_106
.LBB2_107:                              # %..loopexit263_crit_edge.us
	movq	(%rsp), %rax            # 8-byte Reload
	cmpq	%rax, -8(%rsp)          # 8-byte Folded Reload
	jge	.LBB2_123
# BB#108:                               # %.preheader264.split.us.1
	movq	%r15, -96(%rsp)         # 8-byte Spill
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	%r10, %rbx
	movq	8(%rax,%r11,8), %r10
	movq	%r8, %rsi
	movq	-72(%rsp), %r15         # 8-byte Reload
	movq	-24(%rsp), %rdx         # 8-byte Reload
	cmpq	%rdx, %r15
	cmovll	%r15d, %edx
	movq	-40(%rsp), %r13         # 8-byte Reload
	cmpq	%r13, %r15
	movl	%r13d, %eax
	cmovll	%r15d, %eax
	leaq	4(%r11), %rcx
	cmpq	%rcx, %r15
	movq	%rcx, -120(%rsp)        # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	cmovll	%r15d, %ecx
	movq	%r11, -64(%rsp)         # 8-byte Spill
	movq	8(%rsi,%r11,8), %r11
	movslq	%edx, %rdx
	movq	(%rsi,%rdx,8), %r9
	cltq
	movq	(%rsi,%rax,8), %r8
	movslq	%ecx, %rax
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	movq	(%rsi,%rax,8), %r14
	testq	%rbx, %rbx
	movl	$1, %ecx
	movq	%rbx, -112(%rsp)        # 8-byte Spill
	cmovgq	%rbx, %rcx
	xorl	%ebp, %ebp
	cmpq	$4, %rcx
	jb	.LBB2_109
# BB#110:                               # %min.iters.checked429
	movq	%rcx, %rdi
	movabsq	$9223372036854775804, %rax # imm = 0x7FFFFFFFFFFFFFFC
	andq	%rax, %rdi
	je	.LBB2_109
# BB#111:                               # %vector.ph433
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	movd	%r12d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	.LCPI2_0(%rip), %xmm1   # xmm1 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm2   # xmm2 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_2(%rip), %xmm8   # xmm8 = [512,512,512,512]
	pxor	%xmm4, %xmm4
	movq	%rdi, %r15
	movq	%r10, %rbx
	movq	%r14, %rcx
	movq	-96(%rsp), %rax         # 8-byte Reload
	movq	%r8, %rsi
	movq	-128(%rsp), %rdx        # 8-byte Reload
	movq	%r9, %rbp
	movq	%r11, %r13
	.p2align	4, 0x90
.LBB2_112:                              # %vector.body424
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r13), %xmm5
	movdqu	(%rbp), %xmm6
	paddd	%xmm5, %xmm6
	pshufd	$245, %xmm6, %xmm5      # xmm5 = xmm6[1,1,3,3]
	pmuludq	%xmm1, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm1, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movdqu	(%rdx), %xmm5
	movdqu	(%rsi), %xmm7
	paddd	%xmm5, %xmm7
	pshufd	$245, %xmm7, %xmm5      # xmm5 = xmm7[1,1,3,3]
	pmuludq	%xmm2, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	movdqu	(%rax), %xmm5
	movdqu	(%rcx), %xmm3
	paddd	%xmm6, %xmm7
	paddd	%xmm5, %xmm3
	paddd	%xmm7, %xmm3
	paddd	%xmm8, %xmm3
	psrad	$10, %xmm3
	movdqa	%xmm3, %xmm5
	pcmpgtd	%xmm4, %xmm5
	pand	%xmm3, %xmm5
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm5, %xmm3
	pand	%xmm3, %xmm5
	pandn	%xmm0, %xmm3
	por	%xmm5, %xmm3
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	movq	%xmm3, (%rbx)
	addq	$16, %r13
	addq	$16, %rbp
	addq	$16, %rdx
	addq	$16, %rsi
	addq	$16, %rax
	addq	$16, %rcx
	addq	$8, %rbx
	addq	$-4, %r15
	jne	.LBB2_112
# BB#113:                               # %middle.block425
	cmpq	%rdi, -104(%rsp)        # 8-byte Folded Reload
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movq	-128(%rsp), %rbx        # 8-byte Reload
	movq	-72(%rsp), %r15         # 8-byte Reload
	movq	-96(%rsp), %r13         # 8-byte Reload
	movl	$0, %ebp
	jne	.LBB2_114
	jmp	.LBB2_115
.LBB2_24:                               # %min.iters.checked479
	movabsq	$9223372036854775804, %rax # imm = 0x7FFFFFFFFFFFFFFC
	leaq	-4(%rax), %rax
	andq	%rdx, %rax
	je	.LBB2_25
# BB#26:                                # %vector.memcheck
	movq	%rax, -120(%rsp)        # 8-byte Spill
	testq	%r15, %r15
	movl	$1, %eax
	cmovgq	%r15, %rax
	leaq	(%r13,%rax,2), %r12
	leaq	(%r9,%rax,2), %rcx
	leaq	(%rsi,%rax,2), %rdi
	leaq	(%r14,%rax,2), %rbx
	leaq	(%r11,%rax,2), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	cmpq	%rcx, %r13
	sbbb	%cl, %cl
	cmpq	%r12, %r9
	sbbb	%bpl, %bpl
	andb	%cl, %bpl
	cmpq	%rdi, %r13
	sbbb	%cl, %cl
	cmpq	%r12, %rsi
	sbbb	%al, %al
	movb	%al, -96(%rsp)          # 1-byte Spill
	cmpq	%rbx, %r13
	sbbb	%bl, %bl
	cmpq	%r12, %r14
	sbbb	%sil, %sil
	cmpq	-88(%rsp), %r13         # 8-byte Folded Reload
	sbbb	%dil, %dil
	cmpq	%r12, %r11
	sbbb	%r12b, %r12b
	xorl	%eax, %eax
	testb	$1, %bpl
	jne	.LBB2_27
# BB#28:                                # %vector.memcheck
	andb	-96(%rsp), %cl          # 1-byte Folded Reload
	andb	$1, %cl
	jne	.LBB2_29
# BB#30:                                # %vector.memcheck
	andb	%sil, %bl
	andb	$1, %bl
	movq	-128(%rsp), %rsi        # 8-byte Reload
	jne	.LBB2_35
# BB#31:                                # %vector.memcheck
	andb	%r12b, %dil
	andb	$1, %dil
	jne	.LBB2_35
# BB#32:                                # %vector.ph505
	movd	%r10d, %xmm0
	pshufd	$0, %xmm0, %xmm11       # xmm11 = xmm0[0,0,0,0]
	pxor	%xmm12, %xmm12
	movdqa	.LCPI2_0(%rip), %xmm9   # xmm9 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm10  # xmm10 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_3(%rip), %xmm8   # xmm8 = [16,16,16,16]
	movq	-120(%rsp), %rdi        # 8-byte Reload
	movq	%r13, %rax
	movq	%r11, %rbx
	movq	%r14, %rcx
	movq	%rsi, %rbp
	movq	%r9, %r12
	.p2align	4, 0x90
.LBB2_33:                               # %vector.body474
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r12), %xmm6
	movdqa	%xmm6, %xmm5
	punpckhwd	%xmm12, %xmm5   # xmm5 = xmm5[4],xmm12[4],xmm5[5],xmm12[5],xmm5[6],xmm12[6],xmm5[7],xmm12[7]
	punpcklwd	%xmm12, %xmm6   # xmm6 = xmm6[0],xmm12[0],xmm6[1],xmm12[1],xmm6[2],xmm12[2],xmm6[3],xmm12[3]
	movdqu	(%rbp), %xmm4
	movdqa	%xmm4, %xmm7
	punpckhwd	%xmm12, %xmm7   # xmm7 = xmm7[4],xmm12[4],xmm7[5],xmm12[5],xmm7[6],xmm12[6],xmm7[7],xmm12[7]
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	paddd	%xmm6, %xmm4
	paddd	%xmm5, %xmm7
	pshufd	$245, %xmm7, %xmm2      # xmm2 = xmm7[1,1,3,3]
	pmuludq	%xmm9, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm9, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	pshufd	$245, %xmm4, %xmm2      # xmm2 = xmm4[1,1,3,3]
	pmuludq	%xmm9, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pmuludq	%xmm9, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movdqu	(%rcx), %xmm3
	movdqa	%xmm3, %xmm2
	punpckhwd	%xmm12, %xmm2   # xmm2 = xmm2[4],xmm12[4],xmm2[5],xmm12[5],xmm2[6],xmm12[6],xmm2[7],xmm12[7]
	punpcklwd	%xmm12, %xmm3   # xmm3 = xmm3[0],xmm12[0],xmm3[1],xmm12[1],xmm3[2],xmm12[2],xmm3[3],xmm12[3]
	paddd	%xmm6, %xmm3
	paddd	%xmm5, %xmm2
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm10, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	pshufd	$245, %xmm3, %xmm0      # xmm0 = xmm3[1,1,3,3]
	pmuludq	%xmm10, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pmuludq	%xmm10, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	movdqu	(%rbx), %xmm0
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm12, %xmm1   # xmm1 = xmm1[4],xmm12[4],xmm1[5],xmm12[5],xmm1[6],xmm12[6],xmm1[7],xmm12[7]
	punpcklwd	%xmm12, %xmm0   # xmm0 = xmm0[0],xmm12[0],xmm0[1],xmm12[1],xmm0[2],xmm12[2],xmm0[3],xmm12[3]
	paddd	%xmm4, %xmm3
	paddd	%xmm7, %xmm2
	paddd	%xmm6, %xmm0
	paddd	%xmm5, %xmm1
	paddd	%xmm8, %xmm1
	paddd	%xmm2, %xmm1
	paddd	%xmm8, %xmm0
	paddd	%xmm3, %xmm0
	psrad	$5, %xmm0
	psrad	$5, %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm12, %xmm2
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm12, %xmm3
	pand	%xmm0, %xmm3
	pand	%xmm1, %xmm2
	movdqa	%xmm11, %xmm0
	pcmpgtd	%xmm2, %xmm0
	movdqa	%xmm11, %xmm1
	pcmpgtd	%xmm3, %xmm1
	pand	%xmm1, %xmm3
	pandn	%xmm11, %xmm1
	por	%xmm3, %xmm1
	pand	%xmm0, %xmm2
	pandn	%xmm11, %xmm0
	por	%xmm2, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm0, %xmm1
	movdqu	%xmm1, (%rax)
	addq	$16, %r12
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$16, %rbx
	addq	$16, %rax
	addq	$-8, %rdi
	jne	.LBB2_33
# BB#34:                                # %middle.block475
	movq	-120(%rsp), %rax        # 8-byte Reload
	cmpq	%rax, %rdx
	movq	-128(%rsp), %rsi        # 8-byte Reload
	jne	.LBB2_35
	jmp	.LBB2_36
.LBB2_109:
	xorl	%edi, %edi
	movq	-112(%rsp), %rsi        # 8-byte Reload
	movq	-128(%rsp), %rbx        # 8-byte Reload
	movq	-96(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_114:                              # %scalar.ph426
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r9,%rdi,4), %eax
	addl	(%r11,%rdi,4), %eax
	leal	(%rax,%rax,4), %eax
	movl	(%r8,%rdi,4), %ecx
	addl	(%rbx,%rdi,4), %ecx
	imull	$-5, %ecx, %ecx
	movl	(%r14,%rdi,4), %edx
	leal	(%rcx,%rax,4), %eax
	addl	(%r13,%rdi,4), %eax
	leal	512(%rdx,%rax), %eax
	sarl	$10, %eax
	cmovsl	%ebp, %eax
	cmpl	%r12d, %eax
	cmovgew	%r12w, %ax
	movw	%ax, (%r10,%rdi,2)
	incq	%rdi
	cmpq	%rsi, %rdi
	jl	.LBB2_114
.LBB2_115:                              # %..loopexit263_crit_edge.us.1
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	-64(%rsp), %rcx         # 8-byte Reload
	movq	16(%rax,%rcx,8), %r10
	movq	-40(%rsp), %rbp         # 8-byte Reload
	cmpq	%rbp, %r15
	cmovll	%r15d, %ebp
	movq	-120(%rsp), %rdx        # 8-byte Reload
	cmpq	%rdx, %r15
	cmovll	%r15d, %edx
	leaq	5(%rcx), %rax
	cmpq	%rax, %r15
	cmovll	%r15d, %eax
	movq	-56(%rsp), %rdi         # 8-byte Reload
	movq	16(%rdi,%rcx,8), %r8
	movslq	%ebp, %rcx
	movq	(%rdi,%rcx,8), %r9
	movslq	%edx, %rcx
	movq	%rdi, %rdx
	movq	(%rdx,%rcx,8), %r15
	cltq
	movq	(%rdx,%rax,8), %r13
	testq	%rsi, %rsi
	movl	$1, %ecx
	cmovgq	%rsi, %rcx
	xorl	%ebp, %ebp
	cmpq	$4, %rcx
	movq	%rsi, %rdi
	jae	.LBB2_117
# BB#116:
	xorl	%esi, %esi
	movq	-128(%rsp), %rbx        # 8-byte Reload
	jmp	.LBB2_122
.LBB2_117:                              # %min.iters.checked454
	movabsq	$9223372036854775804, %rax # imm = 0x7FFFFFFFFFFFFFFC
	andq	%rcx, %rax
	movq	-128(%rsp), %rbx        # 8-byte Reload
	je	.LBB2_118
# BB#119:                               # %vector.ph458
	movq	%rcx, -120(%rsp)        # 8-byte Spill
	movd	%r12d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	.LCPI2_0(%rip), %xmm1   # xmm1 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm2   # xmm2 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_2(%rip), %xmm8   # xmm8 = [512,512,512,512]
	pxor	%xmm4, %xmm4
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	%r10, -32(%rsp)         # 8-byte Spill
	movq	%r10, %rbp
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rax
	movq	%r11, %rdx
	movq	%r9, %r14
	movq	%r8, %r10
	.p2align	4, 0x90
.LBB2_120:                              # %vector.body449
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r10), %xmm5
	movdqu	(%r14), %xmm6
	paddd	%xmm5, %xmm6
	pshufd	$245, %xmm6, %xmm5      # xmm5 = xmm6[1,1,3,3]
	pmuludq	%xmm1, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm1, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movdqu	(%rdx), %xmm5
	movdqu	(%rax), %xmm7
	paddd	%xmm5, %xmm7
	pshufd	$245, %xmm7, %xmm5      # xmm5 = xmm7[1,1,3,3]
	pmuludq	%xmm2, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm2, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	punpckldq	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	movdqu	(%rsi), %xmm5
	movdqu	(%rcx), %xmm3
	paddd	%xmm6, %xmm7
	paddd	%xmm5, %xmm3
	paddd	%xmm7, %xmm3
	paddd	%xmm8, %xmm3
	psrad	$10, %xmm3
	movdqa	%xmm3, %xmm5
	pcmpgtd	%xmm4, %xmm5
	pand	%xmm3, %xmm5
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm5, %xmm3
	pand	%xmm3, %xmm5
	pandn	%xmm0, %xmm3
	por	%xmm5, %xmm3
	pshuflw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	movq	%xmm3, (%rbp)
	addq	$16, %r10
	addq	$16, %r14
	addq	$16, %rdx
	addq	$16, %rax
	addq	$16, %rsi
	addq	$16, %rcx
	addq	$8, %rbp
	addq	$-4, %rdi
	jne	.LBB2_120
# BB#121:                               # %middle.block450
	movq	-96(%rsp), %rsi         # 8-byte Reload
	cmpq	%rsi, -120(%rsp)        # 8-byte Folded Reload
	movq	-112(%rsp), %rdi        # 8-byte Reload
	movq	-128(%rsp), %rbx        # 8-byte Reload
	movq	-32(%rsp), %r10         # 8-byte Reload
	movl	$0, %ebp
	jne	.LBB2_122
	jmp	.LBB2_123
.LBB2_25:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_35:                               # %scalar.ph476
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%r9,%rax,2), %ecx
	movzwl	(%rsi,%rax,2), %edi
	addl	%ecx, %edi
	leal	(%rdi,%rdi,4), %edi
	movzwl	(%r14,%rax,2), %ebp
	addl	%ecx, %ebp
	imull	$-5, %ebp, %ebp
	movzwl	(%r11,%rax,2), %ebx
	leal	(%rbp,%rdi,4), %edi
	addl	%ecx, %edi
	leal	16(%rbx,%rdi), %ecx
	sarl	$5, %ecx
	cmovsl	%r8d, %ecx
	cmpl	%r10d, %ecx
	cmovgew	%r10w, %cx
	movw	%cx, (%r13,%rax,2)
	incq	%rax
	cmpq	%r15, %rax
	jl	.LBB2_35
.LBB2_36:                               # %..loopexit261_crit_edge.us
	movq	%r9, -120(%rsp)         # 8-byte Spill
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	8(%rax), %r12
	movq	-40(%rsp), %r13         # 8-byte Reload
	movq	32(%r13), %r9
	testq	%r15, %r15
	movl	$1, %ecx
	cmovgq	%r15, %rcx
	xorl	%edx, %edx
	cmpq	$7, %rcx
	jbe	.LBB2_37
# BB#39:                                # %min.iters.checked524
	movabsq	$9223372036854775804, %rax # imm = 0x7FFFFFFFFFFFFFFC
	leaq	-4(%rax), %rax
	andq	%rcx, %rax
	je	.LBB2_37
# BB#40:                                # %vector.memcheck562
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movq	%rcx, -112(%rsp)        # 8-byte Spill
	testq	%r15, %r15
	movl	$1, %eax
	cmovgq	%r15, %rax
	movq	%r9, -88(%rsp)          # 8-byte Spill
	leaq	(%r12,%rax,2), %r9
	movq	-128(%rsp), %rbx        # 8-byte Reload
	leaq	(%rbx,%rax,2), %rcx
	leaq	(%r14,%rax,2), %rdi
	movq	-120(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rax,2), %rbp
	leaq	(%r11,%rax,2), %r8
	movq	-88(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,2), %rax
	cmpq	%rcx, %r12
	sbbb	%cl, %cl
	cmpq	%r9, %rbx
	sbbb	%r13b, %r13b
	andb	%cl, %r13b
	cmpq	%rdi, %r12
	sbbb	%cl, %cl
	cmpq	%r9, %r14
	sbbb	%dl, %dl
	movb	%dl, -96(%rsp)          # 1-byte Spill
	cmpq	%rbp, %r12
	sbbb	%bpl, %bpl
	cmpq	%r9, %rsi
	sbbb	%dl, %dl
	cmpq	%r8, %r12
	sbbb	%r8b, %r8b
	cmpq	%r9, %r11
	sbbb	%bl, %bl
	cmpq	%rax, %r12
	sbbb	%dil, %dil
	cmpq	%r9, -88(%rsp)          # 8-byte Folded Reload
	movq	-88(%rsp), %r9          # 8-byte Reload
	sbbb	%sil, %sil
	xorl	%eax, %eax
	testb	$1, %r13b
	jne	.LBB2_41
# BB#42:                                # %vector.memcheck562
	andb	-96(%rsp), %cl          # 1-byte Folded Reload
	andb	$1, %cl
	movq	-40(%rsp), %r13         # 8-byte Reload
	jne	.LBB2_43
# BB#44:                                # %vector.memcheck562
	andb	%dl, %bpl
	andb	$1, %bpl
	jne	.LBB2_43
# BB#45:                                # %vector.memcheck562
	andb	%bl, %r8b
	andb	$1, %r8b
	jne	.LBB2_43
# BB#46:                                # %vector.memcheck562
	andb	%sil, %dil
	andb	$1, %dil
	movq	-120(%rsp), %r8         # 8-byte Reload
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movl	$0, %edx
	jne	.LBB2_38
# BB#47:                                # %vector.ph563
	movd	%r10d, %xmm0
	pshufd	$0, %xmm0, %xmm11       # xmm11 = xmm0[0,0,0,0]
	pxor	%xmm12, %xmm12
	movdqa	.LCPI2_0(%rip), %xmm9   # xmm9 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm10  # xmm10 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_3(%rip), %xmm8   # xmm8 = [16,16,16,16]
	movq	-72(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rbp
	movq	%r12, %rax
	movq	%r9, %rcx
	movq	%r11, %rdi
	movq	%r14, %r13
	movq	%rsi, %r9
	.p2align	4, 0x90
.LBB2_48:                               # %vector.body519
                                        # =>This Inner Loop Header: Depth=1
	movdqu	(%r9), %xmm4
	movdqa	%xmm4, %xmm5
	punpckhwd	%xmm12, %xmm5   # xmm5 = xmm5[4],xmm12[4],xmm5[5],xmm12[5],xmm5[6],xmm12[6],xmm5[7],xmm12[7]
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	movdqu	(%r13), %xmm6
	movdqa	%xmm6, %xmm7
	punpckhwd	%xmm12, %xmm7   # xmm7 = xmm7[4],xmm12[4],xmm7[5],xmm12[5],xmm7[6],xmm12[6],xmm7[7],xmm12[7]
	punpcklwd	%xmm12, %xmm6   # xmm6 = xmm6[0],xmm12[0],xmm6[1],xmm12[1],xmm6[2],xmm12[2],xmm6[3],xmm12[3]
	paddd	%xmm4, %xmm6
	paddd	%xmm5, %xmm7
	pshufd	$245, %xmm7, %xmm4      # xmm4 = xmm7[1,1,3,3]
	pmuludq	%xmm9, %xmm7
	pshufd	$232, %xmm7, %xmm5      # xmm5 = xmm7[0,2,2,3]
	pmuludq	%xmm9, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	pshufd	$245, %xmm6, %xmm4      # xmm4 = xmm6[1,1,3,3]
	pmuludq	%xmm9, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pmuludq	%xmm9, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movdqu	(%r8), %xmm7
	movdqa	%xmm7, %xmm4
	punpckhwd	%xmm12, %xmm4   # xmm4 = xmm4[4],xmm12[4],xmm4[5],xmm12[5],xmm4[6],xmm12[6],xmm4[7],xmm12[7]
	punpcklwd	%xmm12, %xmm7   # xmm7 = xmm7[0],xmm12[0],xmm7[1],xmm12[1],xmm7[2],xmm12[2],xmm7[3],xmm12[3]
	movdqu	(%rdi), %xmm3
	movdqa	%xmm3, %xmm2
	punpckhwd	%xmm12, %xmm2   # xmm2 = xmm2[4],xmm12[4],xmm2[5],xmm12[5],xmm2[6],xmm12[6],xmm2[7],xmm12[7]
	punpcklwd	%xmm12, %xmm3   # xmm3 = xmm3[0],xmm12[0],xmm3[1],xmm12[1],xmm3[2],xmm12[2],xmm3[3],xmm12[3]
	paddd	%xmm7, %xmm3
	paddd	%xmm4, %xmm2
	pshufd	$245, %xmm2, %xmm0      # xmm0 = xmm2[1,1,3,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm10, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	pshufd	$245, %xmm3, %xmm0      # xmm0 = xmm3[1,1,3,3]
	pmuludq	%xmm10, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pmuludq	%xmm10, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	movdqu	(%rcx), %xmm0
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm12, %xmm1   # xmm1 = xmm1[4],xmm12[4],xmm1[5],xmm12[5],xmm1[6],xmm12[6],xmm1[7],xmm12[7]
	punpcklwd	%xmm12, %xmm0   # xmm0 = xmm0[0],xmm12[0],xmm0[1],xmm12[1],xmm0[2],xmm12[2],xmm0[3],xmm12[3]
	paddd	%xmm6, %xmm3
	paddd	%xmm5, %xmm2
	paddd	%xmm7, %xmm0
	paddd	%xmm4, %xmm1
	paddd	%xmm8, %xmm1
	paddd	%xmm2, %xmm1
	paddd	%xmm8, %xmm0
	paddd	%xmm3, %xmm0
	psrad	$5, %xmm0
	psrad	$5, %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm12, %xmm2
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm12, %xmm3
	pand	%xmm0, %xmm3
	pand	%xmm1, %xmm2
	movdqa	%xmm11, %xmm0
	pcmpgtd	%xmm2, %xmm0
	movdqa	%xmm11, %xmm1
	pcmpgtd	%xmm3, %xmm1
	pand	%xmm1, %xmm3
	pandn	%xmm11, %xmm1
	por	%xmm3, %xmm1
	pand	%xmm0, %xmm2
	pandn	%xmm11, %xmm0
	por	%xmm2, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm0, %xmm1
	movdqu	%xmm1, (%rax)
	addq	$16, %r9
	addq	$16, %r13
	addq	$16, %r8
	addq	$16, %rdi
	addq	$16, %rcx
	addq	$16, %rax
	addq	$-8, %rbp
	jne	.LBB2_48
# BB#49:                                # %middle.block520
	cmpq	%rdx, -112(%rsp)        # 8-byte Folded Reload
	movq	%rdx, %rax
	movq	-40(%rsp), %r13         # 8-byte Reload
	movq	-88(%rsp), %r9          # 8-byte Reload
	movq	-120(%rsp), %r8         # 8-byte Reload
	movq	-128(%rsp), %rsi        # 8-byte Reload
	movl	$0, %edx
	jne	.LBB2_38
	jmp	.LBB2_50
.LBB2_37:
	xorl	%eax, %eax
	movq	-120(%rsp), %r8         # 8-byte Reload
	movq	-128(%rsp), %rsi        # 8-byte Reload
	.p2align	4, 0x90
.LBB2_38:                               # %scalar.ph521
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rsi,%rax,2), %ecx
	movzwl	(%r14,%rax,2), %edi
	addl	%ecx, %edi
	leal	(%rdi,%rdi,4), %ecx
	movzwl	(%r8,%rax,2), %edi
	movzwl	(%r11,%rax,2), %ebp
	addl	%edi, %ebp
	imull	$-5, %ebp, %ebp
	movzwl	(%r9,%rax,2), %ebx
	leal	(%rbp,%rcx,4), %ecx
	addl	%edi, %ecx
	leal	16(%rbx,%rcx), %ecx
	sarl	$5, %ecx
	cmovsl	%edx, %ecx
	cmpl	%r10d, %ecx
	cmovgew	%r10w, %cx
	movw	%cx, (%r12,%rax,2)
	incq	%rax
	cmpq	%r15, %rax
	jl	.LBB2_38
.LBB2_50:                               # %.preheader260
	movq	-104(%rsp), %rax        # 8-byte Reload
	addq	$37, %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	cmpl	$3, %eax
	jl	.LBB2_72
# BB#51:                                # %.lr.ph276
	cmpl	$-39, -24(%rsp)         # 4-byte Folded Reload
	movq	-104(%rsp), %rcx        # 8-byte Reload
	jl	.LBB2_123
# BB#52:                                # %.lr.ph276.split.us.preheader
	movq	img(%rip), %rax
	movslq	-80(%rsp), %rbp         # 4-byte Folded Reload
	movq	(%r13), %rbx
	movq	16(%r13), %rdx
	movq	24(%r13), %r14
	movq	32(%r13), %r10
	movl	15520(%rax), %r9d
	movl	%ecx, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	testq	%rbp, %rbp
	movl	$1, %eax
	cmovgq	%rbp, %rax
	movabsq	$9223372036854775804, %rcx # imm = 0x7FFFFFFFFFFFFFFC
	leaq	-4(%rcx), %rcx
	movq	%rax, -120(%rsp)        # 8-byte Spill
	andq	%rax, %rcx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	movd	%r9d, %xmm0
	pshufd	$0, %xmm0, %xmm11       # xmm11 = xmm0[0,0,0,0]
	movl	$2, %ecx
	pxor	%xmm12, %xmm12
	movdqa	.LCPI2_0(%rip), %xmm9   # xmm9 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm10  # xmm10 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_3(%rip), %xmm8   # xmm8 = [16,16,16,16]
	movq	%rbp, -16(%rsp)         # 8-byte Spill
	jmp	.LBB2_53
.LBB2_64:                               #   in Loop: Header=BB2_53 Depth=1
	xorl	%eax, %eax
	jmp	.LBB2_65
.LBB2_59:                               #   in Loop: Header=BB2_53 Depth=1
	xorl	%eax, %eax
.LBB2_60:                               # %scalar.ph580.preheader
                                        #   in Loop: Header=BB2_53 Depth=1
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movq	%rdx, %rbx
	movq	-112(%rsp), %r14        # 8-byte Reload
.LBB2_65:                               # %scalar.ph580.preheader
                                        #   in Loop: Header=BB2_53 Depth=1
	movq	%rdi, %r10
	movq	-72(%rsp), %r12         # 8-byte Reload
	movq	-56(%rsp), %r15         # 8-byte Reload
	movq	-64(%rsp), %r13         # 8-byte Reload
	jmp	.LBB2_70
.LBB2_57:                               #   in Loop: Header=BB2_53 Depth=1
	xorl	%eax, %eax
	movq	-40(%rsp), %r11         # 8-byte Reload
	jmp	.LBB2_60
	.p2align	4, 0x90
.LBB2_53:                               # %.lr.ph276.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_68 Depth 2
                                        #     Child Loop BB2_70 Depth 2
	movq	%r14, %rsi
	movq	%r10, %r14
	cmpq	$8, -120(%rsp)          # 8-byte Folded Reload
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	-8(%r13,%rcx,8), %r15
	movq	24(%r13,%rcx,8), %rdi
	leaq	1(%rcx), %rcx
	movq	%rsi, %r8
	movq	%rcx, -128(%rsp)        # 8-byte Spill
	jb	.LBB2_54
# BB#55:                                # %min.iters.checked583
                                        #   in Loop: Header=BB2_53 Depth=1
	cmpq	$0, -96(%rsp)           # 8-byte Folded Reload
	je	.LBB2_54
# BB#56:                                # %vector.memcheck627
                                        #   in Loop: Header=BB2_53 Depth=1
	movq	-120(%rsp), %rbp        # 8-byte Reload
	leaq	(%rax,%rbp,2), %r13
	leaq	(%rsi,%rbp,2), %rcx
	leaq	(%rdx,%rbp,2), %r10
	movq	%rsi, %r11
	leaq	(%r15,%rbp,2), %rsi
	movq	%rsi, -56(%rsp)         # 8-byte Spill
	leaq	(%r14,%rbp,2), %rsi
	movq	%rsi, -112(%rsp)        # 8-byte Spill
	leaq	(%rbx,%rbp,2), %r12
	movq	%r12, -64(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rbp,2), %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	cmpq	%rcx, %rax
	sbbb	%cl, %cl
	cmpq	%r13, %r11
	sbbb	%r11b, %r11b
	andb	%cl, %r11b
	cmpq	%r10, %rax
	sbbb	%cl, %cl
	movq	%rdx, -72(%rsp)         # 8-byte Spill
	cmpq	%r13, %rdx
	sbbb	%dl, %dl
	movb	%dl, -41(%rsp)          # 1-byte Spill
	cmpq	-56(%rsp), %rax         # 8-byte Folded Reload
	sbbb	%bpl, %bpl
	movq	%r15, -56(%rsp)         # 8-byte Spill
	cmpq	%r13, %r15
	sbbb	%dl, %dl
	movb	%dl, -42(%rsp)          # 1-byte Spill
	cmpq	-112(%rsp), %rax        # 8-byte Folded Reload
	movq	%r14, %rdx
	sbbb	%r14b, %r14b
	movq	%rdx, -112(%rsp)        # 8-byte Spill
	cmpq	%r13, %rdx
	sbbb	%r15b, %r15b
	cmpq	-64(%rsp), %rax         # 8-byte Folded Reload
	sbbb	%r10b, %r10b
	movq	%rbx, %rdx
	cmpq	%r13, %rbx
	sbbb	%bl, %bl
	movq	%rax, -64(%rsp)         # 8-byte Spill
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	sbbb	%sil, %sil
	cmpq	%r13, %rdi
	sbbb	%al, %al
	testb	$1, %r11b
	jne	.LBB2_57
# BB#58:                                # %vector.memcheck627
                                        #   in Loop: Header=BB2_53 Depth=1
	andb	-41(%rsp), %cl          # 1-byte Folded Reload
	andb	$1, %cl
	movq	-40(%rsp), %r11         # 8-byte Reload
	jne	.LBB2_59
# BB#61:                                # %vector.memcheck627
                                        #   in Loop: Header=BB2_53 Depth=1
	andb	-42(%rsp), %bpl         # 1-byte Folded Reload
	andb	$1, %bpl
	jne	.LBB2_59
# BB#62:                                # %vector.memcheck627
                                        #   in Loop: Header=BB2_53 Depth=1
	andb	%r15b, %r14b
	andb	$1, %r14b
	jne	.LBB2_59
# BB#63:                                # %vector.memcheck627
                                        #   in Loop: Header=BB2_53 Depth=1
	andb	%bl, %r10b
	andb	$1, %r10b
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movq	%rdx, %rbx
	movq	-112(%rsp), %r14        # 8-byte Reload
	jne	.LBB2_64
# BB#66:                                # %vector.memcheck627
                                        #   in Loop: Header=BB2_53 Depth=1
	andb	%al, %sil
	andb	$1, %sil
	movl	$0, %eax
	movq	%rdi, %r10
	movq	-72(%rsp), %r12         # 8-byte Reload
	movq	%r8, %rcx
	movq	-56(%rsp), %r15         # 8-byte Reload
	movq	-64(%rsp), %r13         # 8-byte Reload
	jne	.LBB2_70
# BB#67:                                # %vector.ph628
                                        #   in Loop: Header=BB2_53 Depth=1
	movq	-96(%rsp), %r11         # 8-byte Reload
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movq	%r10, %rdi
	movq	%r14, %r10
	movq	%rbx, %r14
	.p2align	4, 0x90
.LBB2_68:                               # %vector.body578
                                        #   Parent Loop BB2_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r12), %xmm5
	movdqa	%xmm5, %xmm6
	punpckhwd	%xmm12, %xmm6   # xmm6 = xmm6[4],xmm12[4],xmm6[5],xmm12[5],xmm6[6],xmm12[6],xmm6[7],xmm12[7]
	punpcklwd	%xmm12, %xmm5   # xmm5 = xmm5[0],xmm12[0],xmm5[1],xmm12[1],xmm5[2],xmm12[2],xmm5[3],xmm12[3]
	movdqu	(%rcx), %xmm4
	movdqa	%xmm4, %xmm7
	punpckhwd	%xmm12, %xmm7   # xmm7 = xmm7[4],xmm12[4],xmm7[5],xmm12[5],xmm7[6],xmm12[6],xmm7[7],xmm12[7]
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	paddd	%xmm5, %xmm4
	paddd	%xmm6, %xmm7
	pshufd	$245, %xmm7, %xmm6      # xmm6 = xmm7[1,1,3,3]
	pmuludq	%xmm9, %xmm7
	pshufd	$232, %xmm7, %xmm5      # xmm5 = xmm7[0,2,2,3]
	pmuludq	%xmm9, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	pshufd	$245, %xmm4, %xmm7      # xmm7 = xmm4[1,1,3,3]
	pmuludq	%xmm9, %xmm4
	pshufd	$232, %xmm4, %xmm6      # xmm6 = xmm4[0,2,2,3]
	pmuludq	%xmm9, %xmm7
	pshufd	$232, %xmm7, %xmm4      # xmm4 = xmm7[0,2,2,3]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movdqu	(%r15), %xmm4
	movdqa	%xmm4, %xmm7
	punpckhwd	%xmm12, %xmm7   # xmm7 = xmm7[4],xmm12[4],xmm7[5],xmm12[5],xmm7[6],xmm12[6],xmm7[7],xmm12[7]
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	movdqu	(%r10), %xmm3
	movdqa	%xmm3, %xmm2
	punpckhwd	%xmm12, %xmm2   # xmm2 = xmm2[4],xmm12[4],xmm2[5],xmm12[5],xmm2[6],xmm12[6],xmm2[7],xmm12[7]
	punpcklwd	%xmm12, %xmm3   # xmm3 = xmm3[0],xmm12[0],xmm3[1],xmm12[1],xmm3[2],xmm12[2],xmm3[3],xmm12[3]
	paddd	%xmm4, %xmm3
	paddd	%xmm7, %xmm2
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm7      # xmm7 = xmm2[0,2,2,3]
	pmuludq	%xmm10, %xmm4
	pshufd	$232, %xmm4, %xmm2      # xmm2 = xmm4[0,2,2,3]
	punpckldq	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	pshufd	$245, %xmm3, %xmm2      # xmm2 = xmm3[1,1,3,3]
	pmuludq	%xmm10, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqu	(%r14), %xmm2
	movdqa	%xmm2, %xmm4
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	punpckhwd	%xmm12, %xmm2   # xmm2 = xmm2[4],xmm12[4],xmm2[5],xmm12[5],xmm2[6],xmm12[6],xmm2[7],xmm12[7]
	movdqu	(%rdi), %xmm0
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm12, %xmm1   # xmm1 = xmm1[4],xmm12[4],xmm1[5],xmm12[5],xmm1[6],xmm12[6],xmm1[7],xmm12[7]
	punpcklwd	%xmm12, %xmm0   # xmm0 = xmm0[0],xmm12[0],xmm0[1],xmm12[1],xmm0[2],xmm12[2],xmm0[3],xmm12[3]
	paddd	%xmm6, %xmm3
	paddd	%xmm5, %xmm7
	paddd	%xmm4, %xmm0
	paddd	%xmm2, %xmm1
	paddd	%xmm8, %xmm1
	paddd	%xmm7, %xmm1
	paddd	%xmm8, %xmm0
	paddd	%xmm3, %xmm0
	psrad	$5, %xmm0
	psrad	$5, %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm12, %xmm2
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm12, %xmm3
	pand	%xmm0, %xmm3
	pand	%xmm1, %xmm2
	movdqa	%xmm11, %xmm0
	pcmpgtd	%xmm2, %xmm0
	movdqa	%xmm11, %xmm1
	pcmpgtd	%xmm3, %xmm1
	pand	%xmm1, %xmm3
	pandn	%xmm11, %xmm1
	por	%xmm3, %xmm1
	pand	%xmm0, %xmm2
	pandn	%xmm11, %xmm0
	por	%xmm2, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm0, %xmm1
	movdqu	%xmm1, (%r13)
	addq	$16, %r12
	addq	$16, %rcx
	addq	$16, %r15
	addq	$16, %r10
	addq	$16, %r14
	addq	$16, %rdi
	addq	$16, %r13
	addq	$-8, %r11
	jne	.LBB2_68
# BB#69:                                # %middle.block579
                                        #   in Loop: Header=BB2_53 Depth=1
	movq	-96(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, -120(%rsp)        # 8-byte Folded Reload
	movq	-40(%rsp), %r11         # 8-byte Reload
	movq	-16(%rsp), %rbp         # 8-byte Reload
	movq	%rdx, %rbx
	movq	-112(%rsp), %r14        # 8-byte Reload
	movq	-72(%rsp), %r12         # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	%r8, %rdx
	movq	-56(%rsp), %r15         # 8-byte Reload
	movq	-64(%rsp), %r13         # 8-byte Reload
	jne	.LBB2_70
	jmp	.LBB2_71
	.p2align	4, 0x90
.LBB2_54:                               #   in Loop: Header=BB2_53 Depth=1
	movq	%r13, %r11
	movq	%rax, %r13
	xorl	%eax, %eax
	movq	%rdi, %r10
	movq	%rdx, %r12
	.p2align	4, 0x90
.LBB2_70:                               # %scalar.ph580
                                        #   Parent Loop BB2_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r12,%rax,2), %ecx
	movzwl	(%r8,%rax,2), %esi
	addl	%ecx, %esi
	leal	(%rsi,%rsi,4), %ecx
	movzwl	(%r15,%rax,2), %esi
	movzwl	(%r14,%rax,2), %edi
	addl	%esi, %edi
	imull	$-5, %edi, %esi
	movzwl	(%rbx,%rax,2), %edi
	movzwl	(%r10,%rax,2), %edx
	leal	(%rsi,%rcx,4), %ecx
	xorl	%esi, %esi
	addl	%edi, %ecx
	leal	16(%rdx,%rcx), %ecx
	sarl	$5, %ecx
	cmovsl	%esi, %ecx
	movq	%r8, %rdx
	cmpl	%r9d, %ecx
	cmovgew	%r9w, %cx
	movw	%cx, (%r13,%rax,2)
	incq	%rax
	cmpq	%rbp, %rax
	jl	.LBB2_70
.LBB2_71:                               # %..loopexit259_crit_edge.us
                                        #   in Loop: Header=BB2_53 Depth=1
	movq	-128(%rsp), %rcx        # 8-byte Reload
	cmpq	-88(%rsp), %rcx         # 8-byte Folded Reload
	movq	%r15, %rbx
	movq	%r11, %r13
	jne	.LBB2_53
.LBB2_72:                               # %.preheader
	cmpl	$-39, -24(%rsp)         # 4-byte Folded Reload
	movq	-104(%rsp), %r11        # 8-byte Reload
	jl	.LBB2_123
# BB#73:                                # %.preheader.split.us.preheader
	movq	img(%rip), %rax
	movslq	-80(%rsp), %rsi         # 4-byte Folded Reload
	movl	15520(%rax), %r15d
	testq	%rsi, %rsi
	movl	$1, %ecx
	cmovgq	%rsi, %rcx
	movabsq	$9223372036854775804, %rax # imm = 0x7FFFFFFFFFFFFFFC
	addq	$-4, %rax
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	andq	%rcx, %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm11       # xmm11 = xmm0[0,0,0,0]
	xorl	%r10d, %r10d
	pxor	%xmm12, %xmm12
	movdqa	.LCPI2_0(%rip), %xmm9   # xmm9 = [20,20,20,20]
	movdqa	.LCPI2_1(%rip), %xmm10  # xmm10 = [4294967291,4294967291,4294967291,4294967291]
	movdqa	.LCPI2_3(%rip), %xmm8   # xmm8 = [16,16,16,16]
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	jmp	.LBB2_74
.LBB2_85:                               #   in Loop: Header=BB2_74 Depth=1
	xorl	%ecx, %ecx
	movq	-104(%rsp), %r11        # 8-byte Reload
	movq	%rsi, %r12
	movq	-80(%rsp), %rsi         # 8-byte Reload
	movq	-112(%rsp), %r13        # 8-byte Reload
	jmp	.LBB2_90
.LBB2_80:                               #   in Loop: Header=BB2_74 Depth=1
	xorl	%ecx, %ecx
	movq	-104(%rsp), %r11        # 8-byte Reload
	movq	%rsi, %r12
	movq	-80(%rsp), %rsi         # 8-byte Reload
.LBB2_81:                               # %scalar.ph646.preheader
                                        #   in Loop: Header=BB2_74 Depth=1
	movq	-128(%rsp), %rbx        # 8-byte Reload
	movq	-112(%rsp), %r13        # 8-byte Reload
	jmp	.LBB2_90
.LBB2_78:                               #   in Loop: Header=BB2_74 Depth=1
	xorl	%ecx, %ecx
	movq	-104(%rsp), %r11        # 8-byte Reload
	movq	%rsi, %r12
	movq	-80(%rsp), %rsi         # 8-byte Reload
	xorl	%r10d, %r10d
	jmp	.LBB2_81
	.p2align	4, 0x90
.LBB2_74:                               # %.preheader.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_88 Depth 2
                                        #     Child Loop BB2_90 Depth 2
	movq	%r11, %rax
	movq	-8(%rsp), %rbp          # 8-byte Reload
	cmpq	%rax, %rbp
	leaq	2(%rax), %rcx
	leaq	3(%rax), %rdx
	movq	-32(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%rax,8), %r14
	movq	(%r13,%rax,8), %rbx
	movq	-8(%r13,%rax,8), %rdi
	movq	%rdi, -120(%rsp)        # 8-byte Spill
	movq	-16(%r13,%rax,8), %r12
	leaq	1(%rax), %r11
	movl	%r11d, %eax
	cmovlel	%ebp, %eax
	cmpq	%rcx, %rbp
	cmovll	%ebp, %ecx
	cmpq	%rdx, %rbp
	cmovll	%ebp, %edx
	cmpq	$8, -88(%rsp)           # 8-byte Folded Reload
	cltq
	movq	(%r13,%rax,8), %rdi
	movslq	%ecx, %rax
	movq	(%r13,%rax,8), %r8
	movslq	%edx, %rax
	movq	(%r13,%rax,8), %r9
	movq	%rbx, -128(%rsp)        # 8-byte Spill
	jb	.LBB2_75
# BB#76:                                # %min.iters.checked649
                                        #   in Loop: Header=BB2_74 Depth=1
	movq	-96(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	je	.LBB2_75
# BB#77:                                # %vector.memcheck693
                                        #   in Loop: Header=BB2_74 Depth=1
	movq	%r11, -104(%rsp)        # 8-byte Spill
	movq	-88(%rsp), %rcx         # 8-byte Reload
	leaq	(%r14,%rcx,2), %r13
	leaq	(%rbx,%rcx,2), %r10
	leaq	(%rdi,%rcx,2), %rdx
	movq	%rbx, %rsi
	movq	-120(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rcx,2), %rbx
	leaq	(%r8,%rcx,2), %rbp
	leaq	(%r12,%rcx,2), %r11
	leaq	(%r9,%rcx,2), %rcx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	cmpq	%r10, %r14
	sbbb	%cl, %cl
	cmpq	%r13, %rsi
	movq	%rax, %rsi
	sbbb	%r10b, %r10b
	andb	%cl, %r10b
	cmpq	%rdx, %r14
	sbbb	%dl, %dl
	cmpq	%r13, %rdi
	sbbb	%al, %al
	movb	%al, -56(%rsp)          # 1-byte Spill
	cmpq	%rbx, %r14
	sbbb	%cl, %cl
	cmpq	%r13, %rsi
	sbbb	%al, %al
	movb	%al, -64(%rsp)          # 1-byte Spill
	cmpq	%rbp, %r14
	sbbb	%bl, %bl
	cmpq	%r13, %r8
	sbbb	%al, %al
	movb	%al, -24(%rsp)          # 1-byte Spill
	cmpq	%r11, %r14
	movq	%r12, %rbp
	sbbb	%r12b, %r12b
	movq	%rbp, -112(%rsp)        # 8-byte Spill
	cmpq	%r13, %rbp
	sbbb	%al, %al
	cmpq	-72(%rsp), %r14         # 8-byte Folded Reload
	sbbb	%r11b, %r11b
	cmpq	%r13, %r9
	sbbb	%r13b, %r13b
	testb	$1, %r10b
	jne	.LBB2_78
# BB#79:                                # %vector.memcheck693
                                        #   in Loop: Header=BB2_74 Depth=1
	andb	-56(%rsp), %dl          # 1-byte Folded Reload
	andb	$1, %dl
	movl	$0, %r10d
	jne	.LBB2_80
# BB#82:                                # %vector.memcheck693
                                        #   in Loop: Header=BB2_74 Depth=1
	andb	-64(%rsp), %cl          # 1-byte Folded Reload
	andb	$1, %cl
	jne	.LBB2_80
# BB#83:                                # %vector.memcheck693
                                        #   in Loop: Header=BB2_74 Depth=1
	andb	-24(%rsp), %bl          # 1-byte Folded Reload
	andb	$1, %bl
	jne	.LBB2_80
# BB#84:                                # %vector.memcheck693
                                        #   in Loop: Header=BB2_74 Depth=1
	andb	%al, %r12b
	andb	$1, %r12b
	movq	-128(%rsp), %rbx        # 8-byte Reload
	jne	.LBB2_85
# BB#86:                                # %vector.memcheck693
                                        #   in Loop: Header=BB2_74 Depth=1
	andb	%r13b, %r11b
	andb	$1, %r11b
	movl	$0, %ecx
	movq	-104(%rsp), %r11        # 8-byte Reload
	movq	%rsi, %r12
	movq	-80(%rsp), %rsi         # 8-byte Reload
	movq	-112(%rsp), %r13        # 8-byte Reload
	jne	.LBB2_90
# BB#87:                                # %vector.ph694
                                        #   in Loop: Header=BB2_74 Depth=1
	movq	%rbx, %rsi
	movq	-96(%rsp), %rbx         # 8-byte Reload
	movq	%r14, %rcx
	movq	%r9, %rbp
	movq	%r13, %rdx
	movq	%r8, %r13
	movq	%r12, %r11
	movq	%rdi, %r10
	movq	%rsi, %r12
	.p2align	4, 0x90
.LBB2_88:                               # %vector.body644
                                        #   Parent Loop BB2_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r12), %xmm5
	movdqa	%xmm5, %xmm6
	punpckhwd	%xmm12, %xmm6   # xmm6 = xmm6[4],xmm12[4],xmm6[5],xmm12[5],xmm6[6],xmm12[6],xmm6[7],xmm12[7]
	punpcklwd	%xmm12, %xmm5   # xmm5 = xmm5[0],xmm12[0],xmm5[1],xmm12[1],xmm5[2],xmm12[2],xmm5[3],xmm12[3]
	movdqu	(%r10), %xmm4
	movdqa	%xmm4, %xmm7
	punpckhwd	%xmm12, %xmm7   # xmm7 = xmm7[4],xmm12[4],xmm7[5],xmm12[5],xmm7[6],xmm12[6],xmm7[7],xmm12[7]
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	paddd	%xmm5, %xmm4
	paddd	%xmm6, %xmm7
	pshufd	$245, %xmm7, %xmm6      # xmm6 = xmm7[1,1,3,3]
	pmuludq	%xmm9, %xmm7
	pshufd	$232, %xmm7, %xmm5      # xmm5 = xmm7[0,2,2,3]
	pmuludq	%xmm9, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	pshufd	$245, %xmm4, %xmm7      # xmm7 = xmm4[1,1,3,3]
	pmuludq	%xmm9, %xmm4
	pshufd	$232, %xmm4, %xmm6      # xmm6 = xmm4[0,2,2,3]
	pmuludq	%xmm9, %xmm7
	pshufd	$232, %xmm7, %xmm4      # xmm4 = xmm7[0,2,2,3]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movdqu	(%r11), %xmm4
	movdqa	%xmm4, %xmm7
	punpckhwd	%xmm12, %xmm7   # xmm7 = xmm7[4],xmm12[4],xmm7[5],xmm12[5],xmm7[6],xmm12[6],xmm7[7],xmm12[7]
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	movdqu	(%r13), %xmm3
	movdqa	%xmm3, %xmm2
	punpckhwd	%xmm12, %xmm2   # xmm2 = xmm2[4],xmm12[4],xmm2[5],xmm12[5],xmm2[6],xmm12[6],xmm2[7],xmm12[7]
	punpcklwd	%xmm12, %xmm3   # xmm3 = xmm3[0],xmm12[0],xmm3[1],xmm12[1],xmm3[2],xmm12[2],xmm3[3],xmm12[3]
	paddd	%xmm4, %xmm3
	paddd	%xmm7, %xmm2
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm7      # xmm7 = xmm2[0,2,2,3]
	pmuludq	%xmm10, %xmm4
	pshufd	$232, %xmm4, %xmm2      # xmm2 = xmm4[0,2,2,3]
	punpckldq	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	pshufd	$245, %xmm3, %xmm2      # xmm2 = xmm3[1,1,3,3]
	pmuludq	%xmm10, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pmuludq	%xmm10, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqu	(%rdx), %xmm2
	movdqa	%xmm2, %xmm4
	punpcklwd	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1],xmm4[2],xmm12[2],xmm4[3],xmm12[3]
	punpckhwd	%xmm12, %xmm2   # xmm2 = xmm2[4],xmm12[4],xmm2[5],xmm12[5],xmm2[6],xmm12[6],xmm2[7],xmm12[7]
	movdqu	(%rbp), %xmm0
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm12, %xmm1   # xmm1 = xmm1[4],xmm12[4],xmm1[5],xmm12[5],xmm1[6],xmm12[6],xmm1[7],xmm12[7]
	punpcklwd	%xmm12, %xmm0   # xmm0 = xmm0[0],xmm12[0],xmm0[1],xmm12[1],xmm0[2],xmm12[2],xmm0[3],xmm12[3]
	paddd	%xmm6, %xmm3
	paddd	%xmm5, %xmm7
	paddd	%xmm4, %xmm0
	paddd	%xmm2, %xmm1
	paddd	%xmm8, %xmm1
	paddd	%xmm7, %xmm1
	paddd	%xmm8, %xmm0
	paddd	%xmm3, %xmm0
	psrad	$5, %xmm0
	psrad	$5, %xmm1
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm12, %xmm2
	movdqa	%xmm0, %xmm3
	pcmpgtd	%xmm12, %xmm3
	pand	%xmm0, %xmm3
	pand	%xmm1, %xmm2
	movdqa	%xmm11, %xmm0
	pcmpgtd	%xmm2, %xmm0
	movdqa	%xmm11, %xmm1
	pcmpgtd	%xmm3, %xmm1
	pand	%xmm1, %xmm3
	pandn	%xmm11, %xmm1
	por	%xmm3, %xmm1
	pand	%xmm0, %xmm2
	pandn	%xmm11, %xmm0
	por	%xmm2, %xmm0
	pslld	$16, %xmm0
	psrad	$16, %xmm0
	pslld	$16, %xmm1
	psrad	$16, %xmm1
	packssdw	%xmm0, %xmm1
	movdqu	%xmm1, (%rcx)
	addq	$16, %r12
	addq	$16, %r10
	addq	$16, %r11
	addq	$16, %r13
	addq	$16, %rdx
	addq	$16, %rbp
	addq	$16, %rcx
	addq	$-8, %rbx
	jne	.LBB2_88
# BB#89:                                # %middle.block645
                                        #   in Loop: Header=BB2_74 Depth=1
	movq	-96(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, -88(%rsp)         # 8-byte Folded Reload
	movq	-104(%rsp), %r11        # 8-byte Reload
	movq	-80(%rsp), %rsi         # 8-byte Reload
	movl	$0, %r10d
	movq	-128(%rsp), %rbx        # 8-byte Reload
	movq	-120(%rsp), %r12        # 8-byte Reload
	movq	-112(%rsp), %r13        # 8-byte Reload
	jne	.LBB2_90
	jmp	.LBB2_91
	.p2align	4, 0x90
.LBB2_75:                               #   in Loop: Header=BB2_74 Depth=1
	xorl	%ecx, %ecx
	movq	%r12, %r13
	movq	-120(%rsp), %r12        # 8-byte Reload
	.p2align	4, 0x90
.LBB2_90:                               # %scalar.ph646
                                        #   Parent Loop BB2_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbx,%rcx,2), %edx
	movzwl	(%rdi,%rcx,2), %ebp
	addl	%edx, %ebp
	leal	(%rbp,%rbp,4), %edx
	movzwl	(%r12,%rcx,2), %ebp
	movzwl	(%r8,%rcx,2), %ebx
	addl	%ebp, %ebx
	imull	$-5, %ebx, %ebp
	movzwl	(%r13,%rcx,2), %ebx
	movzwl	(%r9,%rcx,2), %eax
	leal	(%rbp,%rdx,4), %edx
	addl	%ebx, %edx
	movq	-128(%rsp), %rbx        # 8-byte Reload
	leal	16(%rax,%rdx), %eax
	sarl	$5, %eax
	cmovsl	%r10d, %eax
	cmpl	%r15d, %eax
	cmovgew	%r15w, %ax
	movw	%ax, (%r14,%rcx,2)
	incq	%rcx
	cmpq	%rsi, %rcx
	jl	.LBB2_90
.LBB2_91:                               # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB2_74 Depth=1
	cmpq	(%rsp), %r11            # 8-byte Folded Reload
	movq	-40(%rsp), %r13         # 8-byte Reload
	jl	.LBB2_74
	jmp	.LBB2_123
.LBB2_118:
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_122:                              # %scalar.ph451
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r9,%rsi,4), %eax
	addl	(%r8,%rsi,4), %eax
	leal	(%rax,%rax,4), %eax
	movl	(%r15,%rsi,4), %ecx
	addl	(%r11,%rsi,4), %ecx
	imull	$-5, %ecx, %ecx
	movl	(%r13,%rsi,4), %edx
	leal	(%rcx,%rax,4), %eax
	addl	(%rbx,%rsi,4), %eax
	leal	512(%rdx,%rax), %eax
	sarl	$10, %eax
	cmovsl	%ebp, %eax
	cmpl	%r12d, %eax
	cmovgew	%r12w, %ax
	movw	%ax, (%r10,%rsi,2)
	incq	%rsi
	cmpq	%rdi, %rsi
	jl	.LBB2_122
.LBB2_123:                              # %.loopexit258
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_29:
	movq	-128(%rsp), %rsi        # 8-byte Reload
	jmp	.LBB2_35
.LBB2_27:
	movq	-128(%rsp), %rsi        # 8-byte Reload
	jmp	.LBB2_35
.LBB2_41:
	movq	-40(%rsp), %r13         # 8-byte Reload
.LBB2_43:
	movq	-120(%rsp), %r8         # 8-byte Reload
	movq	-128(%rsp), %rsi        # 8-byte Reload
	xorl	%edx, %edx
	jmp	.LBB2_38
.Lfunc_end2:
	.size	getVerSubImageSixTap, .Lfunc_end2-getVerSubImageSixTap
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
	.short	65535                   # 0xffff
	.short	0                       # 0x0
.LCPI3_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	getHorSubImageBiLinear
	.p2align	4, 0x90
	.type	getHorSubImageBiLinear,@function
getHorSubImageBiLinear:                 # @getHorSubImageBiLinear
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi94:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi96:
	.cfi_def_cfa_offset 80
.Lcfi97:
	.cfi_offset %rbx, -56
.Lcfi98:
	.cfi_offset %r12, -48
.Lcfi99:
	.cfi_offset %r13, -40
.Lcfi100:
	.cfi_offset %r14, -32
.Lcfi101:
	.cfi_offset %r15, -24
.Lcfi102:
	.cfi_offset %rbp, -16
	movslq	6396(%rdi), %r13
	cmpq	$-39, %r13
	jl	.LBB3_53
# BB#1:                                 # %.lr.ph62
	movl	88(%rsp), %r10d
	addq	$40, %r13
	movslq	6392(%rdi), %r11
	leal	40(%r11), %ebp
	movq	%r11, %rbx
	addq	$39, %rbx
	movq	%rbx, %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movl	%ebx, %r12d
	subl	%r10d, %r12d
	movabsq	$8589934584, %rax       # imm = 0x1FFFFFFF8
	movq	6448(%rdi), %rdi
	movslq	%ecx, %rcx
	movq	(%rdi,%rcx,8), %rcx
	movslq	%r8d, %rbx
	movq	(%rcx,%rbx,8), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movslq	%r9d, %rcx
	movq	(%rdi,%rcx,8), %rcx
	movslq	80(%rsp), %rbx
	movq	(%rcx,%rbx,8), %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movslq	%esi, %rcx
	movq	(%rdi,%rcx,8), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	testl	%r12d, %r12d
	movl	%ebp, -124(%rsp)        # 4-byte Spill
	jle	.LBB3_34
# BB#2:                                 # %.lr.ph62.split.us.preheader
	movslq	%r10d, %rdx
	movslq	%r12d, %rsi
	movl	%r12d, %r8d
	movl	%r10d, %edi
	leaq	1(%rsi,%rdi), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	incq	%rdi
	movq	%rdx, %rcx
	movq	%rcx, -112(%rsp)        # 8-byte Spill
	leaq	(%rdx,%r8), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leaq	-1(%r8), %rcx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	movq	%rdi, -72(%rsp)         # 8-byte Spill
	andq	%rdi, %rax
	leaq	-8(%rax), %rcx
	shrq	$3, %rcx
	leal	1(%r11), %edx
	movl	%edx, -84(%rsp)         # 4-byte Spill
	movq	%rax, -80(%rsp)         # 8-byte Spill
	movq	%rsi, -120(%rsp)        # 8-byte Spill
	leaq	(%rsi,%rax), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	%rcx, -16(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movl	%r12d, %eax
	andl	$7, %eax
	movq	%r8, %rcx
	movq	%rax, (%rsp)            # 8-byte Spill
	subq	%rax, %rcx
	movq	%rcx, -104(%rsp)        # 8-byte Spill
	xorl	%r11d, %r11d
	movdqa	.LCPI3_0(%rip), %xmm0   # xmm0 = [65535,0,65535,0,65535,0,65535,0]
	pxor	%xmm1, %xmm1
	movdqa	.LCPI3_1(%rip), %xmm2   # xmm2 = [1,1,1,1]
	movq	%r13, -8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph62.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_14 Depth 2
                                        #     Child Loop BB3_9 Depth 2
                                        #     Child Loop BB3_31 Depth 2
                                        #     Child Loop BB3_22 Depth 2
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r11,8), %r15
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r11,8), %rbp
	movq	-56(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r11,8), %r10
	cmpl	$7, %r12d
	jbe	.LBB3_4
# BB#10:                                # %min.iters.checked141
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$0, -104(%rsp)          # 8-byte Folded Reload
	je	.LBB3_4
# BB#11:                                # %vector.memcheck162
                                        #   in Loop: Header=BB3_3 Depth=1
	leaq	(%r10,%r8,2), %rsi
	leaq	(%r15,%r8,2), %rcx
	movq	-112(%rsp), %rax        # 8-byte Reload
	leaq	(%rbp,%rax,2), %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rbp,%rdx,2), %rdi
	cmpq	%rcx, %r10
	sbbb	%cl, %cl
	cmpq	%rsi, %r15
	sbbb	%dl, %dl
	andb	%cl, %dl
	cmpq	%rdi, %r10
	sbbb	%cl, %cl
	cmpq	%rsi, %rax
	sbbb	%bl, %bl
	testb	$1, %dl
	jne	.LBB3_4
# BB#12:                                # %vector.memcheck162
                                        #   in Loop: Header=BB3_3 Depth=1
	andb	%bl, %cl
	andb	$1, %cl
	movl	$0, %ecx
	movl	-124(%rsp), %r9d        # 4-byte Reload
	jne	.LBB3_5
# BB#13:                                # %vector.body137.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movq	%r10, %rsi
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB3_14:                               # %vector.body137
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rbx), %xmm3
	movdqu	(%rax), %xmm4
	pavgw	%xmm3, %xmm4
	movdqu	%xmm4, (%rsi)
	addq	$16, %rbx
	addq	$16, %rax
	addq	$16, %rsi
	addq	$-8, %rcx
	jne	.LBB3_14
# BB#15:                                # %middle.block138
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	movq	-104(%rsp), %rcx        # 8-byte Reload
	jne	.LBB3_5
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_4:                                #   in Loop: Header=BB3_3 Depth=1
	xorl	%ecx, %ecx
	movl	-124(%rsp), %r9d        # 4-byte Reload
.LBB3_5:                                # %scalar.ph139.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%r8d, %eax
	subl	%ecx, %eax
	testb	$1, %al
	movq	%rcx, %rdi
	je	.LBB3_7
# BB#6:                                 # %scalar.ph139.prol
                                        #   in Loop: Header=BB3_3 Depth=1
	movzwl	(%r15,%rcx,2), %eax
	movq	-112(%rsp), %rdx        # 8-byte Reload
	leaq	(%rcx,%rdx), %rsi
	movzwl	(%rbp,%rsi,2), %esi
	leal	1(%rax,%rsi), %eax
	shrl	%eax
	movw	%ax, (%r10,%rcx,2)
	leaq	1(%rcx), %rdi
.LBB3_7:                                # %scalar.ph139.prol.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rcx, -96(%rsp)         # 8-byte Folded Reload
	je	.LBB3_16
# BB#8:                                 # %scalar.ph139.preheader.new
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r8, %rax
	subq	%rdi, %rax
	movq	-112(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdi), %rcx
	leaq	2(%rbp,%rcx,2), %rbx
	leaq	2(%r15,%rdi,2), %rsi
	leaq	2(%r10,%rdi,2), %rcx
	.p2align	4, 0x90
.LBB3_9:                                # %scalar.ph139
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%rsi), %edx
	movzwl	-2(%rbx), %edi
	leal	1(%rdx,%rdi), %edx
	shrl	%edx
	movw	%dx, -2(%rcx)
	movzwl	(%rsi), %edx
	movzwl	(%rbx), %edi
	leal	1(%rdx,%rdi), %edx
	shrl	%edx
	movw	%dx, (%rcx)
	addq	$4, %rbx
	addq	$4, %rsi
	addq	$4, %rcx
	addq	$-2, %rax
	jne	.LBB3_9
.LBB3_16:                               # %..preheader_crit_edge.us
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	%r9d, %r12d
	jge	.LBB3_33
# BB#17:                                # %.lr.ph59.us
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax,2), %r14
	cmpq	$7, -72(%rsp)           # 8-byte Folded Reload
	movq	-120(%rsp), %rax        # 8-byte Reload
	jbe	.LBB3_18
# BB#23:                                # %min.iters.checked102
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$0, -80(%rsp)           # 8-byte Folded Reload
	movq	-120(%rsp), %rax        # 8-byte Reload
	je	.LBB3_18
# BB#24:                                # %vector.memcheck125
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	-120(%rsp), %rdi        # 8-byte Reload
	leaq	(%r10,%rdi,2), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	(%r10,%rsi,2), %rdx
	leaq	(%r15,%rdi,2), %rcx
	leaq	(%r15,%rsi,2), %rsi
	cmpq	%rsi, %rax
	sbbb	%sil, %sil
	cmpq	%rdx, %rcx
	sbbb	%bl, %bl
	andb	%sil, %bl
	cmpq	%r14, %rax
	sbbb	%cl, %cl
	cmpq	%rdx, %r14
	sbbb	%dl, %dl
	testb	$1, %bl
	movq	%rdi, %rax
	jne	.LBB3_18
# BB#25:                                # %vector.memcheck125
                                        #   in Loop: Header=BB3_3 Depth=1
	andb	%dl, %cl
	andb	$1, %cl
	movq	-120(%rsp), %rax        # 8-byte Reload
	jne	.LBB3_18
# BB#26:                                # %vector.body98.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$0, -32(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_27
# BB#28:                                # %vector.body98.prol
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	-120(%rsp), %rcx        # 8-byte Reload
	movdqu	(%r15,%rcx,2), %xmm3
	movzwl	(%r14), %eax
	movd	%eax, %xmm4
	pshuflw	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm4, %xmm4       # xmm4 = xmm4[0,0,1,1]
	pavgw	%xmm3, %xmm4
	movdqu	%xmm4, (%r10,%rcx,2)
	movl	$8, %eax
	jmp	.LBB3_29
.LBB3_27:                               #   in Loop: Header=BB3_3 Depth=1
	xorl	%eax, %eax
.LBB3_29:                               # %vector.body98.prol.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$0, -16(%rsp)           # 8-byte Folded Reload
	movq	-80(%rsp), %rsi         # 8-byte Reload
	je	.LBB3_32
# BB#30:                                # %vector.body98.preheader.new
                                        #   in Loop: Header=BB3_3 Depth=1
	movzwl	(%r14), %ecx
	movd	%ecx, %xmm3
	pshuflw	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm3, %xmm3       # xmm3 = xmm3[0,0,1,1]
	pand	%xmm0, %xmm3
	movq	%rsi, %rdx
	subq	%rax, %rdx
	addq	-120(%rsp), %rax        # 8-byte Folded Reload
	leaq	16(%r10,%rax,2), %r9
	leaq	16(%r15,%rax,2), %r13
	.p2align	4, 0x90
.LBB3_31:                               # %vector.body98
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%r13), %xmm4
	movdqa	%xmm4, %xmm5
	punpckhwd	%xmm1, %xmm5    # xmm5 = xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	paddd	%xmm3, %xmm4
	paddd	%xmm3, %xmm5
	paddd	%xmm2, %xmm5
	paddd	%xmm2, %xmm4
	psrld	$1, %xmm4
	psrld	$1, %xmm5
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm5, %xmm4
	movdqu	%xmm4, -16(%r9)
	movdqu	(%r13), %xmm4
	movdqa	%xmm4, %xmm5
	punpckhwd	%xmm1, %xmm5    # xmm5 = xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	paddd	%xmm3, %xmm4
	paddd	%xmm3, %xmm5
	paddd	%xmm2, %xmm5
	paddd	%xmm2, %xmm4
	psrld	$1, %xmm4
	psrld	$1, %xmm5
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm5, %xmm4
	movdqu	%xmm4, (%r9)
	addq	$32, %r9
	addq	$32, %r13
	addq	$-16, %rdx
	jne	.LBB3_31
.LBB3_32:                               # %middle.block99
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	%rsi, -72(%rsp)         # 8-byte Folded Reload
	movq	-24(%rsp), %rax         # 8-byte Reload
	movl	-124(%rsp), %r9d        # 4-byte Reload
	movq	-8(%rsp), %r13          # 8-byte Reload
	je	.LBB3_33
	.p2align	4, 0x90
.LBB3_18:                               # %scalar.ph100.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	leal	1(%rax), %ecx
	movl	-84(%rsp), %edx         # 4-byte Reload
	subl	%ecx, %edx
	testb	$1, %dl
	je	.LBB3_20
# BB#19:                                # %scalar.ph100.prol
                                        #   in Loop: Header=BB3_3 Depth=1
	movzwl	(%r15,%rax,2), %edx
	movzwl	(%r14), %esi
	leal	1(%rdx,%rsi), %edx
	shrl	%edx
	movw	%dx, (%r10,%rax,2)
	incq	%rax
.LBB3_20:                               # %scalar.ph100.prol.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	%ecx, %r9d
	je	.LBB3_33
# BB#21:                                # %scalar.ph100.preheader.new
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%r9d, %ecx
	subl	%eax, %ecx
	leaq	2(%r15,%rax,2), %rdx
	leaq	2(%r10,%rax,2), %rax
	.p2align	4, 0x90
.LBB3_22:                               # %scalar.ph100
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%rdx), %esi
	movzwl	(%r14), %edi
	leal	1(%rsi,%rdi), %esi
	shrl	%esi
	movw	%si, -2(%rax)
	movzwl	(%rdx), %esi
	movzwl	(%r14), %edi
	leal	1(%rsi,%rdi), %esi
	shrl	%esi
	movw	%si, (%rax)
	addq	$4, %rdx
	addq	$4, %rax
	addl	$-2, %ecx
	jne	.LBB3_22
.LBB3_33:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_3 Depth=1
	incq	%r11
	cmpq	%r13, %r11
	jl	.LBB3_3
	jmp	.LBB3_53
.LBB3_34:                               # %.lr.ph62.split
	cmpl	%ebp, %r12d
	jge	.LBB3_53
# BB#35:                                # %.lr.ph62.split.split.us.preheader
	movslq	%r12d, %r12
	movslq	%r13d, %r8
	movl	%r10d, %r9d
	leaq	1(%r12,%r9), %r15
	incq	%r9
	andq	%r9, %rax
	leaq	-8(%rax), %rcx
	shrq	$3, %rcx
	leal	1(%r11), %r13d
	movq	%rax, %r14
	leaq	(%r12,%rax), %rax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	%rcx, -112(%rsp)        # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	movdqa	.LCPI3_0(%rip), %xmm0   # xmm0 = [65535,0,65535,0,65535,0,65535,0]
	pxor	%xmm1, %xmm1
	movdqa	.LCPI3_1(%rip), %xmm2   # xmm2 = [1,1,1,1]
	.p2align	4, 0x90
.LBB3_36:                               # %.lr.ph62.split.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_45 Depth 2
                                        #     Child Loop BB3_51 Depth 2
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax), %rdi
	movq	-48(%rsp), %rax         # 8-byte Reload
	addq	(%rax,%rbp,8), %rdi
	cmpq	$8, %r9
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %r11
	movq	-56(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %r10
	movq	%r12, %rax
	jb	.LBB3_47
# BB#37:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_36 Depth=1
	testq	%r14, %r14
	movq	%r12, %rax
	je	.LBB3_47
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_36 Depth=1
	leaq	(%r10,%r12,2), %rax
	leaq	(%r10,%r15,2), %rcx
	leaq	(%r11,%r12,2), %rdx
	leaq	(%r11,%r15,2), %rsi
	cmpq	%rsi, %rax
	sbbb	%sil, %sil
	cmpq	%rcx, %rdx
	sbbb	%bl, %bl
	andb	%sil, %bl
	cmpq	%rdi, %rax
	sbbb	%dl, %dl
	cmpq	%rcx, %rdi
	sbbb	%cl, %cl
	testb	$1, %bl
	movq	%r12, %rax
	jne	.LBB3_47
# BB#39:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_36 Depth=1
	andb	%cl, %dl
	andb	$1, %dl
	movq	%r12, %rax
	jne	.LBB3_47
# BB#40:                                # %vector.body.preheader
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpq	$0, -96(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_41
# BB#42:                                # %vector.body.prol
                                        #   in Loop: Header=BB3_36 Depth=1
	movdqu	(%r11,%r12,2), %xmm3
	movzwl	(%rdi), %eax
	movd	%eax, %xmm4
	pshuflw	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm4, %xmm4       # xmm4 = xmm4[0,0,1,1]
	pavgw	%xmm3, %xmm4
	movdqu	%xmm4, (%r10,%r12,2)
	movl	$8, %eax
	cmpq	$0, -112(%rsp)          # 8-byte Folded Reload
	jne	.LBB3_44
	jmp	.LBB3_46
.LBB3_41:                               #   in Loop: Header=BB3_36 Depth=1
	xorl	%eax, %eax
	cmpq	$0, -112(%rsp)          # 8-byte Folded Reload
	je	.LBB3_46
.LBB3_44:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_36 Depth=1
	movzwl	(%rdi), %ecx
	movd	%ecx, %xmm3
	pshuflw	$0, %xmm3, %xmm3        # xmm3 = xmm3[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm3, %xmm3       # xmm3 = xmm3[0,0,1,1]
	pand	%xmm0, %xmm3
	movq	%r14, %rdx
	subq	%rax, %rdx
	addq	%r12, %rax
	leaq	16(%r10,%rax,2), %rsi
	leaq	16(%r11,%rax,2), %rax
	.p2align	4, 0x90
.LBB3_45:                               # %vector.body
                                        #   Parent Loop BB3_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rax), %xmm4
	movdqa	%xmm4, %xmm5
	punpckhwd	%xmm1, %xmm5    # xmm5 = xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	paddd	%xmm3, %xmm4
	paddd	%xmm3, %xmm5
	paddd	%xmm2, %xmm5
	paddd	%xmm2, %xmm4
	psrld	$1, %xmm4
	psrld	$1, %xmm5
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm5, %xmm4
	movdqu	%xmm4, -16(%rsi)
	movdqu	(%rax), %xmm4
	movdqa	%xmm4, %xmm5
	punpckhwd	%xmm1, %xmm5    # xmm5 = xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	paddd	%xmm3, %xmm4
	paddd	%xmm3, %xmm5
	paddd	%xmm2, %xmm5
	paddd	%xmm2, %xmm4
	psrld	$1, %xmm4
	psrld	$1, %xmm5
	pslld	$16, %xmm5
	psrad	$16, %xmm5
	pslld	$16, %xmm4
	psrad	$16, %xmm4
	packssdw	%xmm5, %xmm4
	movdqu	%xmm4, (%rsi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-16, %rdx
	jne	.LBB3_45
.LBB3_46:                               # %middle.block
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpq	%r14, %r9
	movq	-120(%rsp), %rax        # 8-byte Reload
	je	.LBB3_52
	.p2align	4, 0x90
.LBB3_47:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_36 Depth=1
	leal	1(%rax), %edx
	movl	%r13d, %ecx
	subl	%edx, %ecx
	testb	$1, %cl
	je	.LBB3_49
# BB#48:                                # %scalar.ph.prol
                                        #   in Loop: Header=BB3_36 Depth=1
	movzwl	(%r11,%rax,2), %ecx
	movzwl	(%rdi), %esi
	leal	1(%rcx,%rsi), %ecx
	shrl	%ecx
	movw	%cx, (%r10,%rax,2)
	incq	%rax
.LBB3_49:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB3_36 Depth=1
	cmpl	%edx, -124(%rsp)        # 4-byte Folded Reload
	je	.LBB3_52
# BB#50:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB3_36 Depth=1
	movl	-124(%rsp), %edx        # 4-byte Reload
	subl	%eax, %edx
	leaq	2(%r11,%rax,2), %rsi
	leaq	2(%r10,%rax,2), %rax
	.p2align	4, 0x90
.LBB3_51:                               # %scalar.ph
                                        #   Parent Loop BB3_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-2(%rsi), %ecx
	movzwl	(%rdi), %ebx
	leal	1(%rcx,%rbx), %ecx
	shrl	%ecx
	movw	%cx, -2(%rax)
	movzwl	(%rsi), %ecx
	movzwl	(%rdi), %ebx
	leal	1(%rcx,%rbx), %ecx
	shrl	%ecx
	movw	%cx, (%rax)
	addq	$4, %rsi
	addq	$4, %rax
	addl	$-2, %edx
	jne	.LBB3_51
.LBB3_52:                               # %._crit_edge.us71
                                        #   in Loop: Header=BB3_36 Depth=1
	incq	%rbp
	cmpq	%r8, %rbp
	jl	.LBB3_36
.LBB3_53:                               # %._crit_edge63
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	getHorSubImageBiLinear, .Lfunc_end3-getHorSubImageBiLinear
	.cfi_endproc

	.globl	getVerSubImageBiLinear
	.p2align	4, 0x90
	.type	getVerSubImageBiLinear,@function
getVerSubImageBiLinear:                 # @getVerSubImageBiLinear
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi106:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi107:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 56
.Lcfi109:
	.cfi_offset %rbx, -56
.Lcfi110:
	.cfi_offset %r12, -48
.Lcfi111:
	.cfi_offset %r13, -40
.Lcfi112:
	.cfi_offset %r14, -32
.Lcfi113:
	.cfi_offset %r15, -24
.Lcfi114:
	.cfi_offset %rbp, -16
	movl	%r9d, -60(%rsp)         # 4-byte Spill
	movl	%r8d, -68(%rsp)         # 4-byte Spill
	movl	%ecx, -76(%rsp)         # 4-byte Spill
	movl	%edx, -64(%rsp)         # 4-byte Spill
	movl	%esi, -72(%rsp)         # 4-byte Spill
	movl	64(%rsp), %ecx
	movslq	6396(%rdi), %rdx
	leal	40(%rdx), %eax
	movl	%eax, -92(%rsp)         # 4-byte Spill
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	movslq	6392(%rdi), %rbx
	movq	%rbx, %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	addq	$40, %rbx
	movq	%rdx, %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leal	39(%rdx), %eax
	subl	%ecx, %eax
	movl	%eax, -88(%rsp)         # 4-byte Spill
	jle	.LBB4_19
# BB#1:                                 # %.lr.ph83
	cmpl	$-39, -40(%rsp)         # 4-byte Folded Reload
	jl	.LBB4_19
# BB#2:                                 # %.lr.ph83.split.us.preheader
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	6448(%rax), %rax
	movslq	-76(%rsp), %rdx         # 4-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movslq	-68(%rsp), %rsi         # 4-byte Folded Reload
	movq	(%rdx,%rsi,8), %r13
	movslq	-72(%rsp), %rdx         # 4-byte Folded Reload
	movq	(%rax,%rdx,8), %rdx
	movslq	-64(%rsp), %rsi         # 4-byte Folded Reload
	movq	(%rdx,%rsi,8), %r10
	movslq	-60(%rsp), %rdx         # 4-byte Folded Reload
	movq	(%rax,%rdx,8), %rax
	movslq	56(%rsp), %rdx
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movslq	%ecx, %r14
	movl	-88(%rsp), %r9d         # 4-byte Reload
	testq	%rbx, %rbx
	movl	$1, %r8d
	cmovgq	%rbx, %r8
	movq	%r8, %r12
	movabsq	$9223372036854775800, %rax # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rax, %r12
	leaq	-8(%r12), %rax
	shrq	$3, %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%r13, -8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph83.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_15 Depth 2
                                        #     Child Loop BB4_17 Depth 2
	leaq	(%rcx,%r14), %rax
	cmpq	$8, %r8
	movq	(%r13,%rcx,8), %r15
	movq	(%r10,%rcx,8), %rsi
	movq	-56(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rax,8), %rdi
	jae	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	xorl	%edx, %edx
	jmp	.LBB4_17
	.p2align	4, 0x90
.LBB4_5:                                # %min.iters.checked
                                        #   in Loop: Header=BB4_3 Depth=1
	testq	%r12, %r12
	je	.LBB4_6
# BB#7:                                 # %vector.memcheck
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%r10, %r11
	leaq	(%rsi,%r8,2), %rbp
	leaq	(%r15,%r8,2), %rdx
	leaq	(%rdi,%r8,2), %r10
	cmpq	%rdx, %rsi
	sbbb	%dl, %dl
	cmpq	%rbp, %r15
	sbbb	%al, %al
	andb	%dl, %al
	cmpq	%r10, %rsi
	sbbb	%dl, %dl
	cmpq	%rbp, %rdi
	sbbb	%r10b, %r10b
	testb	$1, %al
	jne	.LBB4_8
# BB#9:                                 # %vector.memcheck
                                        #   in Loop: Header=BB4_3 Depth=1
	andb	%r10b, %dl
	andb	$1, %dl
	movl	$0, %edx
	movq	%r11, %r10
	jne	.LBB4_17
# BB#10:                                # %vector.body.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpq	$0, -24(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_11
# BB#12:                                # %vector.body.prol
                                        #   in Loop: Header=BB4_3 Depth=1
	movdqu	(%r15), %xmm0
	movdqu	(%rdi), %xmm1
	pavgw	%xmm0, %xmm1
	movdqu	%xmm1, (%rsi)
	movl	$8, %ebp
	cmpq	$0, -16(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_14
	jmp	.LBB4_16
.LBB4_6:                                #   in Loop: Header=BB4_3 Depth=1
	xorl	%edx, %edx
	jmp	.LBB4_17
.LBB4_8:                                #   in Loop: Header=BB4_3 Depth=1
	xorl	%edx, %edx
	movq	%r11, %r10
	jmp	.LBB4_17
.LBB4_11:                               #   in Loop: Header=BB4_3 Depth=1
	xorl	%ebp, %ebp
	cmpq	$0, -16(%rsp)           # 8-byte Folded Reload
	je	.LBB4_16
.LBB4_14:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%r12, %rdx
	subq	%rbp, %rdx
	leaq	16(%r15,%rbp,2), %r10
	leaq	16(%rdi,%rbp,2), %r13
	leaq	16(%rsi,%rbp,2), %rbp
	.p2align	4, 0x90
.LBB4_15:                               # %vector.body
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%r10), %xmm0
	movdqu	-16(%r13), %xmm1
	pavgw	%xmm0, %xmm1
	movdqu	%xmm1, -16(%rbp)
	movdqu	(%r10), %xmm0
	movdqu	(%r13), %xmm1
	pavgw	%xmm0, %xmm1
	movdqu	%xmm1, (%rbp)
	addq	$32, %r10
	addq	$32, %r13
	addq	$32, %rbp
	addq	$-16, %rdx
	jne	.LBB4_15
.LBB4_16:                               # %middle.block
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpq	%r12, %r8
	movq	%r12, %rdx
	movq	-8(%rsp), %r13          # 8-byte Reload
	movq	%r11, %r10
	je	.LBB4_18
	.p2align	4, 0x90
.LBB4_17:                               # %scalar.ph
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r15,%rdx,2), %eax
	movzwl	(%rdi,%rdx,2), %ebp
	leal	1(%rax,%rbp), %eax
	shrl	%eax
	movw	%ax, (%rsi,%rdx,2)
	incq	%rdx
	cmpq	%rbx, %rdx
	jl	.LBB4_17
.LBB4_18:                               # %._crit_edge80.us
                                        #   in Loop: Header=BB4_3 Depth=1
	incq	%rcx
	cmpq	%r9, %rcx
	jne	.LBB4_3
.LBB4_19:                               # %.preheader
	movl	-92(%rsp), %r11d        # 4-byte Reload
	cmpl	%r11d, -88(%rsp)        # 4-byte Folded Reload
	jge	.LBB4_38
# BB#20:                                # %.lr.ph75
	cmpl	$-39, -40(%rsp)         # 4-byte Folded Reload
	jl	.LBB4_38
# BB#21:                                # %.lr.ph75.split.us.preheader
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	6448(%rax), %rax
	movslq	-76(%rsp), %rcx         # 4-byte Folded Reload
	movq	(%rax,%rcx,8), %rcx
	movslq	-68(%rsp), %rdx         # 4-byte Folded Reload
	movq	(%rcx,%rdx,8), %r15
	movslq	-72(%rsp), %rcx         # 4-byte Folded Reload
	movq	(%rax,%rcx,8), %rcx
	movslq	-64(%rsp), %rdx         # 4-byte Folded Reload
	movq	(%rcx,%rdx,8), %r12
	movslq	-60(%rsp), %rcx         # 4-byte Folded Reload
	movq	(%rax,%rcx,8), %rax
	movslq	56(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	-32(%rsp), %rcx         # 8-byte Reload
	movq	312(%rax,%rcx,8), %rdx
	movslq	%ebx, %rsi
	movslq	-88(%rsp), %r13         # 4-byte Folded Reload
	testq	%rsi, %rsi
	movl	$1, %r10d
	cmovgq	%rsi, %r10
	leaq	(%rdx,%r10,2), %r14
	movabsq	$9223372036854775800, %rax # imm = 0x7FFFFFFFFFFFFFF8
	andq	%r10, %rax
	movq	%rax, %r9
	leaq	-8(%rax), %r8
	shrq	$3, %r8
	movl	%r8d, %eax
	andl	$1, %eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	leaq	16(%rdx), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_22:                               # %.lr.ph75.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_34 Depth 2
                                        #     Child Loop BB4_36 Depth 2
	cmpq	$8, %r10
	movq	(%r15,%r13,8), %rbx
	movq	(%r12,%r13,8), %rcx
	jae	.LBB4_24
# BB#23:                                #   in Loop: Header=BB4_22 Depth=1
	xorl	%eax, %eax
	jmp	.LBB4_36
	.p2align	4, 0x90
.LBB4_24:                               # %min.iters.checked110
                                        #   in Loop: Header=BB4_22 Depth=1
	testq	%r9, %r9
	je	.LBB4_25
# BB#26:                                # %vector.memcheck130
                                        #   in Loop: Header=BB4_22 Depth=1
	leaq	(%rcx,%r10,2), %rdi
	leaq	(%rbx,%r10,2), %rax
	cmpq	%rax, %rcx
	sbbb	%r11b, %r11b
	cmpq	%rdi, %rbx
	sbbb	%al, %al
	andb	%r11b, %al
	cmpq	%r14, %rcx
	sbbb	%r11b, %r11b
	cmpq	%rdi, %rdx
	sbbb	%dil, %dil
	testb	$1, %al
	jne	.LBB4_27
# BB#28:                                # %vector.memcheck130
                                        #   in Loop: Header=BB4_22 Depth=1
	andb	%dil, %r11b
	andb	$1, %r11b
	movl	$0, %eax
	movl	-92(%rsp), %r11d        # 4-byte Reload
	jne	.LBB4_36
# BB#29:                                # %vector.body105.preheader
                                        #   in Loop: Header=BB4_22 Depth=1
	cmpq	$0, -56(%rsp)           # 8-byte Folded Reload
	jne	.LBB4_30
# BB#31:                                # %vector.body105.prol
                                        #   in Loop: Header=BB4_22 Depth=1
	movdqu	(%rbx), %xmm0
	movdqu	(%rdx), %xmm1
	pavgw	%xmm0, %xmm1
	movdqu	%xmm1, (%rcx)
	movl	$8, %r11d
	testq	%r8, %r8
	jne	.LBB4_33
	jmp	.LBB4_35
.LBB4_25:                               #   in Loop: Header=BB4_22 Depth=1
	xorl	%eax, %eax
	jmp	.LBB4_36
.LBB4_27:                               #   in Loop: Header=BB4_22 Depth=1
	xorl	%eax, %eax
	movl	-92(%rsp), %r11d        # 4-byte Reload
	jmp	.LBB4_36
.LBB4_30:                               #   in Loop: Header=BB4_22 Depth=1
	xorl	%r11d, %r11d
	testq	%r8, %r8
	je	.LBB4_35
.LBB4_33:                               # %vector.body105.preheader.new
                                        #   in Loop: Header=BB4_22 Depth=1
	movq	%r9, %rax
	subq	%r11, %rax
	leaq	16(%rbx,%r11,2), %rbp
	movq	-88(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r11,2), %rdi
	leaq	16(%rcx,%r11,2), %r11
	.p2align	4, 0x90
.LBB4_34:                               # %vector.body105
                                        #   Parent Loop BB4_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rbp), %xmm0
	movdqu	-16(%rdi), %xmm1
	pavgw	%xmm0, %xmm1
	movdqu	%xmm1, -16(%r11)
	movdqu	(%rbp), %xmm0
	movdqu	(%rdi), %xmm1
	pavgw	%xmm0, %xmm1
	movdqu	%xmm1, (%r11)
	addq	$32, %rbp
	addq	$32, %rdi
	addq	$32, %r11
	addq	$-16, %rax
	jne	.LBB4_34
.LBB4_35:                               # %middle.block106
                                        #   in Loop: Header=BB4_22 Depth=1
	movq	%r9, %rax
	cmpq	%rax, %r10
	movl	-92(%rsp), %r11d        # 4-byte Reload
	je	.LBB4_37
	.p2align	4, 0x90
.LBB4_36:                               # %scalar.ph107
                                        #   Parent Loop BB4_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbx,%rax,2), %edi
	movzwl	(%rdx,%rax,2), %ebp
	leal	1(%rdi,%rbp), %edi
	shrl	%edi
	movw	%di, (%rcx,%rax,2)
	incq	%rax
	cmpq	%rsi, %rax
	jl	.LBB4_36
.LBB4_37:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_22 Depth=1
	incq	%r13
	cmpl	%r11d, %r13d
	jne	.LBB4_22
.LBB4_38:                               # %._crit_edge76
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	getVerSubImageBiLinear, .Lfunc_end4-getVerSubImageBiLinear
	.cfi_endproc

	.globl	getDiagSubImageBiLinear
	.p2align	4, 0x90
	.type	getDiagSubImageBiLinear,@function
getDiagSubImageBiLinear:                # @getDiagSubImageBiLinear
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
.Lcfi121:
	.cfi_offset %rbx, -56
.Lcfi122:
	.cfi_offset %r12, -48
.Lcfi123:
	.cfi_offset %r13, -40
.Lcfi124:
	.cfi_offset %r14, -32
.Lcfi125:
	.cfi_offset %r15, -24
.Lcfi126:
	.cfi_offset %rbp, -16
	movl	88(%rsp), %r15d
	movl	80(%rsp), %r12d
	movl	72(%rsp), %r13d
	movl	56(%rsp), %r11d
	movslq	6396(%rdi), %rax
	movslq	6392(%rdi), %r10
	leaq	19(%rax), %rbp
	movq	%rbp, -72(%rsp)         # 8-byte Spill
	movq	%rax, -56(%rsp)         # 8-byte Spill
	cmpq	$-19, %rax
	leaq	40(%r10), %r14
	movq	6448(%rdi), %rax
	movslq	%ecx, %rcx
	leaq	39(%r10), %rbx
	jle	.LBB5_1
# BB#2:                                 # %.lr.ph96
	cmpl	$-39, %r10d
	jl	.LBB5_13
# BB#3:                                 # %.lr.ph96.split.us.preheader
	movq	%r10, -16(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rcx
	movslq	%r8d, %rdi
	movq	(%rcx,%rdi,8), %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movslq	%r9d, %rcx
	movq	(%rax,%rcx,8), %rcx
	movslq	%r11d, %rdi
	movq	(%rcx,%rdi,8), %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movslq	%r13d, %r9
	movslq	%r15d, %r10
	movslq	64(%rsp), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	movslq	%r12d, %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movl	%r10d, %r15d
	movl	%r9d, %r11d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_4:                                # %.lr.ph96.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_5 Depth 2
	movq	-64(%rsp), %rax         # 8-byte Reload
	leaq	(%rcx,%rax), %rsi
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rsi,8), %r13
	movq	-8(%rsp), %rax          # 8-byte Reload
	leaq	(%rcx,%rax), %rsi
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rsi,8), %rsi
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_5:                                #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r9,%rbp), %r12
	leal	(%r11,%rbp), %r8d
	cmpq	%r12, %rbx
	cmovll	%ebx, %r8d
	movslq	%r8d, %rcx
	movzwl	(%r13,%rcx,2), %ecx
	leaq	(%r10,%rbp), %rdx
	leal	(%r15,%rbp), %eax
	cmpq	%rdx, %rbx
	cmovll	%ebx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	leal	1(%rcx,%rax), %eax
	shrl	%eax
	movw	%ax, (%rdi,%rbp,2)
	incq	%rbp
	cmpq	%r14, %rbp
	jl	.LBB5_5
# BB#6:                                 # %._crit_edge93.us
                                        #   in Loop: Header=BB5_4 Depth=1
	movq	-48(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpq	-72(%rsp), %rcx         # 8-byte Folded Reload
	jl	.LBB5_4
# BB#7:
	movl	72(%rsp), %r13d
	movl	88(%rsp), %r15d
	movl	80(%rsp), %r12d
	movq	-16(%rsp), %r10         # 8-byte Reload
	jmp	.LBB5_8
.LBB5_1:                                # %..preheader_crit_edge
	movslq	%r8d, %rdi
	movslq	%r9d, %rbp
	movslq	%r11d, %r8
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	movq	(%rax,%rcx,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	movq	(%rax,%rbp,8), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movq	(%rax,%rsi,8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
.LBB5_8:                                # %.preheader
	cmpl	$-39, %r10d
	movl	64(%rsp), %edx
	jl	.LBB5_13
# BB#9:                                 # %.preheader.split.us.preheader
	movq	-56(%rsp), %rax         # 8-byte Reload
	leaq	40(%rax), %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	addq	$39, %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movslq	%r13d, %r10
	movslq	%ebx, %rbx
	movl	%r15d, %eax
	movslq	%eax, %r15
	movl	%r12d, %ecx
	movslq	%r14d, %r12
	movslq	-72(%rsp), %r11         # 4-byte Folded Reload
	movslq	%edx, %rdx
	movq	%rdx, -72(%rsp)         # 8-byte Spill
	movslq	%ecx, %rcx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movl	%eax, %esi
	movl	%r13d, %eax
	.p2align	4, 0x90
.LBB5_10:                               # %.preheader.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_11 Depth 2
	movq	-72(%rsp), %rcx         # 8-byte Reload
	leaq	(%r11,%rcx), %rdx
	movq	-56(%rsp), %rcx         # 8-byte Reload
	cmpq	%rdx, %rcx
	cmovll	%ecx, %edx
	movslq	%edx, %rdx
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%rdx,8), %rdx
	movq	-64(%rsp), %rdi         # 8-byte Reload
	leaq	(%r11,%rdi), %r8
	cmpq	%r8, %rcx
	cmovll	%ecx, %r8d
	movslq	%r8d, %rcx
	movq	-32(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi,%rcx,8), %r8
	movq	-40(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r11,8), %r9
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB5_11:                               #   Parent Loop BB5_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r10,%r14), %rcx
	leal	(%rax,%r14), %r13d
	cmpq	%rcx, %rbx
	cmovll	%ebx, %r13d
	movslq	%r13d, %rcx
	movzwl	(%rdx,%rcx,2), %ecx
	leaq	(%r15,%r14), %rdi
	leal	(%rsi,%r14), %ebp
	cmpq	%rdi, %rbx
	cmovll	%ebx, %ebp
	movslq	%ebp, %rdi
	movzwl	(%r8,%rdi,2), %edi
	leal	1(%rcx,%rdi), %ecx
	shrl	%ecx
	movw	%cx, (%r9,%r14,2)
	incq	%r14
	cmpq	%r12, %r14
	jl	.LBB5_11
# BB#12:                                # %._crit_edge.us
                                        #   in Loop: Header=BB5_10 Depth=1
	incq	%r11
	cmpq	-48(%rsp), %r11         # 8-byte Folded Reload
	jl	.LBB5_10
.LBB5_13:                               # %.us-lcssa.us
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	getDiagSubImageBiLinear, .Lfunc_end5-getDiagSubImageBiLinear
	.cfi_endproc

	.type	ONE_FOURTH_TAP,@object  # @ONE_FOURTH_TAP
	.section	.rodata,"a",@progbits
	.globl	ONE_FOURTH_TAP
	.p2align	4
ONE_FOURTH_TAP:
	.long	20                      # 0x14
	.long	4294967291              # 0xfffffffb
	.long	1                       # 0x1
	.long	20                      # 0x14
	.long	4294967292              # 0xfffffffc
	.long	0                       # 0x0
	.size	ONE_FOURTH_TAP, 24

	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
