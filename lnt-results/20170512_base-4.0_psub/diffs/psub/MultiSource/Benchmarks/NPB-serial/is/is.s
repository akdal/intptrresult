	.text
	.file	"is.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
.LCPI0_1:
	.quad	4710765210229538816     # double 8388608
.LCPI0_2:
	.quad	4400016835940974592     # double 1.4210854715202004E-14
.LCPI0_3:
	.quad	4814348001659060224     # double 70368744177664
	.text
	.globl	randlc
	.p2align	4, 0x90
	.type	randlc,@function
randlc:                                 # @randlc
	.cfi_startproc
# BB#0:
	cmpb	$1, randlc.KS(%rip)
	jne	.LBB0_2
# BB#1:                                 # %._crit_edge
	movsd	randlc.R23(%rip), %xmm4 # xmm4 = mem[0],zero
	movsd	randlc.T23(%rip), %xmm2 # xmm2 = mem[0],zero
	movsd	randlc.R46(%rip), %xmm1 # xmm1 = mem[0],zero
	movsd	randlc.T46(%rip), %xmm8 # xmm8 = mem[0],zero
	jmp	.LBB0_3
.LBB0_2:                                # %.preheader59
	movabsq	$4503599627370496000, %rax # imm = 0x3E80000000000000
	movq	%rax, randlc.R23(%rip)
	movabsq	$4710765210229538816, %rax # imm = 0x4160000000000000
	movq	%rax, randlc.T23(%rip)
	movabsq	$4400016835940974592, %rax # imm = 0x3D10000000000000
	movq	%rax, randlc.R46(%rip)
	movabsq	$4814348001659060224, %rax # imm = 0x42D0000000000000
	movq	%rax, randlc.T46(%rip)
	movb	$1, randlc.KS(%rip)
	movsd	.LCPI0_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	.LCPI0_1(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm8   # xmm8 = mem[0],zero
.LBB0_3:
	movsd	(%rsi), %xmm5           # xmm5 = mem[0],zero
	movapd	%xmm4, %xmm0
	mulsd	%xmm5, %xmm0
	cvttsd2si	%xmm0, %eax
	cvtsi2sdl	%eax, %xmm6
	movapd	%xmm2, %xmm0
	mulsd	%xmm6, %xmm0
	subsd	%xmm0, %xmm5
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movapd	%xmm4, %xmm7
	mulsd	%xmm0, %xmm7
	cvttsd2si	%xmm7, %eax
	xorps	%xmm7, %xmm7
	cvtsi2sdl	%eax, %xmm7
	movapd	%xmm2, %xmm3
	mulsd	%xmm7, %xmm3
	subsd	%xmm3, %xmm0
	mulsd	%xmm0, %xmm6
	mulsd	%xmm5, %xmm7
	addsd	%xmm6, %xmm7
	mulsd	%xmm7, %xmm4
	cvttsd2si	%xmm4, %eax
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%eax, %xmm3
	mulsd	%xmm2, %xmm3
	subsd	%xmm3, %xmm7
	mulsd	%xmm2, %xmm7
	mulsd	%xmm5, %xmm0
	addsd	%xmm7, %xmm0
	movapd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	mulsd	%xmm8, %xmm2
	subsd	%xmm2, %xmm0
	movsd	%xmm0, (%rdi)
	mulsd	%xmm1, %xmm0
	retq
.Lfunc_end0:
	.size	randlc, .Lfunc_end0-randlc
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
.LCPI1_1:
	.quad	4710765210229538816     # double 8388608
.LCPI1_2:
	.quad	4400016835940974592     # double 1.4210854715202004E-14
.LCPI1_3:
	.quad	4814348001659060224     # double 70368744177664
.LCPI1_4:
	.quad	4692750811720056832     # double 524288
	.text
	.globl	create_seq
	.p2align	4, 0x90
	.type	create_seq,@function
create_seq:                             # @create_seq
	.cfi_startproc
# BB#0:
	movb	randlc.KS(%rip), %cl
	xorl	%eax, %eax
	movsd	.LCPI1_4(%rip), %xmm12  # xmm12 = mem[0],zero
	movabsq	$4503599627370496000, %r8 # imm = 0x3E80000000000000
	movabsq	$4710765210229538816, %rdx # imm = 0x4160000000000000
	movabsq	$4400016835940974592, %rsi # imm = 0x3D10000000000000
	movabsq	$4814348001659060224, %rdi # imm = 0x42D0000000000000
	movsd	.LCPI1_0(%rip), %xmm8   # xmm8 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm9   # xmm9 = mem[0],zero
	movsd	.LCPI1_2(%rip), %xmm10  # xmm10 = mem[0],zero
	movsd	.LCPI1_3(%rip), %xmm11  # xmm11 = mem[0],zero
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	testb	$1, %cl
	je	.LBB1_3
# BB#2:                                 # %._crit_edge.i
                                        #   in Loop: Header=BB1_1 Depth=1
	movsd	randlc.R23(%rip), %xmm15 # xmm15 = mem[0],zero
	movsd	randlc.T23(%rip), %xmm5 # xmm5 = mem[0],zero
	movsd	randlc.R46(%rip), %xmm2 # xmm2 = mem[0],zero
	movsd	randlc.T46(%rip), %xmm13 # xmm13 = mem[0],zero
	jmp	.LBB1_4
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader59.i
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	%r8, randlc.R23(%rip)
	movq	%rdx, randlc.T23(%rip)
	movq	%rsi, randlc.R46(%rip)
	movq	%rdi, randlc.T46(%rip)
	movb	$1, randlc.KS(%rip)
	movapd	%xmm11, %xmm13
	movapd	%xmm10, %xmm2
	movapd	%xmm9, %xmm5
	movapd	%xmm8, %xmm15
.LBB1_4:                                # %randlc.exit19
                                        #   in Loop: Header=BB1_1 Depth=1
	movapd	%xmm15, %xmm3
	mulsd	%xmm1, %xmm3
	cvttsd2si	%xmm3, %ecx
	cvtsi2sdl	%ecx, %xmm14
	movapd	%xmm5, %xmm6
	mulsd	%xmm14, %xmm6
	movapd	%xmm1, %xmm3
	subsd	%xmm6, %xmm3
	movapd	%xmm0, %xmm6
	mulsd	%xmm15, %xmm6
	cvttsd2si	%xmm6, %ecx
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%ecx, %xmm6
	movapd	%xmm5, %xmm7
	mulsd	%xmm6, %xmm7
	subsd	%xmm7, %xmm0
	movapd	%xmm14, %xmm7
	mulsd	%xmm0, %xmm7
	mulsd	%xmm3, %xmm6
	addsd	%xmm7, %xmm6
	movapd	%xmm15, %xmm7
	mulsd	%xmm6, %xmm7
	cvttsd2si	%xmm7, %ecx
	xorps	%xmm7, %xmm7
	cvtsi2sdl	%ecx, %xmm7
	mulsd	%xmm5, %xmm7
	subsd	%xmm7, %xmm6
	mulsd	%xmm5, %xmm6
	mulsd	%xmm3, %xmm0
	addsd	%xmm6, %xmm0
	movapd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm6
	cvttsd2si	%xmm6, %ecx
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%ecx, %xmm6
	mulsd	%xmm13, %xmm6
	subsd	%xmm6, %xmm0
	movapd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm6
	movapd	%xmm0, %xmm7
	mulsd	%xmm15, %xmm7
	cvttsd2si	%xmm7, %ecx
	xorps	%xmm7, %xmm7
	cvtsi2sdl	%ecx, %xmm7
	movapd	%xmm5, %xmm4
	mulsd	%xmm7, %xmm4
	subsd	%xmm4, %xmm0
	movapd	%xmm14, %xmm4
	mulsd	%xmm0, %xmm4
	mulsd	%xmm3, %xmm7
	addsd	%xmm4, %xmm7
	movapd	%xmm15, %xmm4
	mulsd	%xmm7, %xmm4
	cvttsd2si	%xmm4, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	mulsd	%xmm5, %xmm4
	subsd	%xmm4, %xmm7
	mulsd	%xmm5, %xmm7
	mulsd	%xmm3, %xmm0
	addsd	%xmm7, %xmm0
	movapd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvttsd2si	%xmm4, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	mulsd	%xmm13, %xmm4
	subsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm7
	mulsd	%xmm0, %xmm7
	addsd	%xmm6, %xmm7
	movapd	%xmm0, %xmm4
	mulsd	%xmm15, %xmm4
	cvttsd2si	%xmm4, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	movapd	%xmm5, %xmm6
	mulsd	%xmm4, %xmm6
	subsd	%xmm6, %xmm0
	movapd	%xmm14, %xmm6
	mulsd	%xmm0, %xmm6
	mulsd	%xmm3, %xmm4
	addsd	%xmm6, %xmm4
	movapd	%xmm15, %xmm6
	mulsd	%xmm4, %xmm6
	cvttsd2si	%xmm6, %ecx
	xorps	%xmm6, %xmm6
	cvtsi2sdl	%ecx, %xmm6
	mulsd	%xmm5, %xmm6
	subsd	%xmm6, %xmm4
	mulsd	%xmm5, %xmm4
	mulsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvttsd2si	%xmm4, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	mulsd	%xmm13, %xmm4
	subsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm6
	addsd	%xmm7, %xmm6
	movapd	%xmm0, %xmm4
	mulsd	%xmm15, %xmm4
	cvttsd2si	%xmm4, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2sdl	%ecx, %xmm4
	movapd	%xmm5, %xmm7
	mulsd	%xmm4, %xmm7
	subsd	%xmm7, %xmm0
	mulsd	%xmm0, %xmm14
	mulsd	%xmm3, %xmm4
	addsd	%xmm14, %xmm4
	mulsd	%xmm4, %xmm15
	cvttsd2si	%xmm15, %ecx
	xorps	%xmm7, %xmm7
	cvtsi2sdl	%ecx, %xmm7
	mulsd	%xmm5, %xmm7
	subsd	%xmm7, %xmm4
	mulsd	%xmm5, %xmm4
	mulsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	cvttsd2si	%xmm3, %ecx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm13, %xmm3
	subsd	%xmm3, %xmm0
	mulsd	%xmm0, %xmm2
	addsd	%xmm6, %xmm2
	mulsd	%xmm12, %xmm2
	cvttsd2si	%xmm2, %ecx
	movl	%ecx, key_array(%rax)
	addq	$4, %rax
	movb	$1, %cl
	cmpq	$134217728, %rax        # imm = 0x8000000
	jne	.LBB1_1
# BB#5:
	retq
.Lfunc_end1:
	.size	create_seq, .Lfunc_end1-create_seq
	.cfi_endproc

	.globl	full_verify
	.p2align	4, 0x90
	.type	full_verify,@function
full_verify:                            # @full_verify
	.cfi_startproc
# BB#0:
	movq	key_buff_ptr_global(%rip), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movslq	key_buff2(%rcx), %rdx
	movslq	(%rax,%rdx,4), %rsi
	leaq	-1(%rsi), %rdi
	movl	%edi, (%rax,%rdx,4)
	movl	%edx, key_array-4(,%rsi,4)
	movslq	key_buff2+4(%rcx), %rdx
	movslq	(%rax,%rdx,4), %rsi
	leaq	-1(%rsi), %rdi
	movl	%edi, (%rax,%rdx,4)
	movl	%edx, key_array-4(,%rsi,4)
	addq	$8, %rcx
	cmpq	$134217728, %rcx        # imm = 0x8000000
	jne	.LBB2_1
# BB#2:                                 # %.preheader.preheader
	movd	key_array(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	pshufd	$36, %xmm0, %xmm0       # xmm0 = xmm0[0,1,2,0]
	pxor	%xmm4, %xmm4
	xorl	%eax, %eax
	pxor	%xmm2, %xmm2
	jmp	.LBB2_3
	.p2align	4, 0x90
.LBB2_6:                                # %vector.body.1
                                        #   in Loop: Header=BB2_3 Depth=1
	movups	key_array+36(%rax), %xmm2
	shufps	$3, %xmm2, %xmm1        # xmm1 = xmm1[3,0],xmm2[0,0]
	shufps	$152, %xmm2, %xmm1      # xmm1 = xmm1[0,2],xmm2[1,2]
	pcmpgtd	%xmm2, %xmm1
	psrld	$31, %xmm1
	paddd	%xmm0, %xmm1
	movups	key_array+52(%rax), %xmm0
	shufps	$3, %xmm0, %xmm2        # xmm2 = xmm2[3,0],xmm0[0,0]
	shufps	$152, %xmm0, %xmm2      # xmm2 = xmm2[0,2],xmm0[1,2]
	pcmpgtd	%xmm0, %xmm2
	psrld	$31, %xmm2
	paddd	%xmm3, %xmm2
	addq	$64, %rax
	movdqa	%xmm1, %xmm4
.LBB2_3:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	key_array+4(%rax), %xmm3
	movups	key_array+20(%rax), %xmm1
	shufps	$3, %xmm3, %xmm0        # xmm0 = xmm0[3,0],xmm3[0,0]
	shufps	$152, %xmm3, %xmm0      # xmm0 = xmm0[0,2],xmm3[1,2]
	pcmpgtd	%xmm3, %xmm0
	shufps	$3, %xmm1, %xmm3        # xmm3 = xmm3[3,0],xmm1[0,0]
	shufps	$152, %xmm1, %xmm3      # xmm3 = xmm3[0,2],xmm1[1,2]
	psrld	$31, %xmm0
	pcmpgtd	%xmm1, %xmm3
	psrld	$31, %xmm3
	paddd	%xmm4, %xmm0
	paddd	%xmm2, %xmm3
	cmpq	$134217664, %rax        # imm = 0x7FFFFC0
	jne	.LBB2_6
# BB#4:                                 # %.preheader
	paddd	%xmm0, %xmm3
	pshufd	$78, %xmm3, %xmm0       # xmm0 = xmm3[2,3,0,1]
	paddd	%xmm3, %xmm0
	pshufd	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3]
	movd	%xmm1, %eax
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ecx
	movl	$134217700, %edx        # imm = 0x7FFFFE4
	movl	key_array(%rdx), %edx
	xorl	%esi, %esi
	cmpl	%edx, %eax
	setg	%sil
	addl	%ecx, %esi
	movl	$134217704, %eax        # imm = 0x7FFFFE8
	movl	key_array(%rax), %eax
	xorl	%ecx, %ecx
	cmpl	%eax, %edx
	setg	%cl
	addl	%esi, %ecx
	movl	$134217708, %edx        # imm = 0x7FFFFEC
	movl	key_array(%rdx), %edx
	xorl	%esi, %esi
	cmpl	%edx, %eax
	setg	%sil
	addl	%ecx, %esi
	movl	$134217712, %eax        # imm = 0x7FFFFF0
	movl	key_array(%rax), %eax
	xorl	%ecx, %ecx
	cmpl	%eax, %edx
	setg	%cl
	addl	%esi, %ecx
	movl	$134217716, %edx        # imm = 0x7FFFFF4
	movl	key_array(%rdx), %edx
	xorl	%esi, %esi
	cmpl	%edx, %eax
	setg	%sil
	addl	%ecx, %esi
	movl	$134217720, %eax        # imm = 0x7FFFFF8
	movl	key_array(%rax), %ecx
	xorl	%edi, %edi
	cmpl	%ecx, %edx
	setg	%dil
	addl	%esi, %edi
	movl	$134217724, %edx        # imm = 0x7FFFFFC
	xorl	%eax, %eax
	cmpl	key_array(%rdx), %ecx
	setg	%al
	addl	%edi, %eax
	je	.LBB2_5
# BB#7:
	movslq	%eax, %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.LBB2_5:
	incl	passed_verification(%rip)
	retq
.Lfunc_end2:
	.size	full_verify, .Lfunc_end2-full_verify
	.cfi_endproc

	.globl	c_print_results
	.p2align	4, 0x90
	.type	c_print_results,@function
c_print_results:                        # @c_print_results
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movl	%r8d, %ebp
	movl	%ecx, %ebx
	movl	%edx, %r13d
	movl	%esi, %r14d
	movq	%rdi, %rcx
	movq	96(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	88(%rsp), %r15d
	movq	80(%rsp), %r12
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	testl	%ebp, %ebp
	je	.LBB3_1
# BB#2:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	callq	printf
	jmp	.LBB3_3
.LBB3_1:
	movslq	%r13d, %rax
	testl	%ebx, %ebx
	movslq	%ebx, %rcx
	movl	$1, %esi
	cmovneq	%rcx, %rsi
	imulq	%rax, %rsi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
.LBB3_3:
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	printf
	testl	%r15d, %r15d
	js	.LBB3_4
# BB#5:
	je	.LBB3_7
# BB#6:
	movl	$.Lstr.5, %edi
	jmp	.LBB3_8
.LBB3_4:
	movl	$.Lstr.6, %edi
	jmp	.LBB3_8
.LBB3_7:
	movl	$.Lstr, %edi
.LBB3_8:
	callq	puts
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	puts                    # TAILCALL
.Lfunc_end3:
	.size	c_print_results, .Lfunc_end3-c_print_results
	.cfi_endproc

	.globl	rank
	.p2align	4, 0x90
	.type	rank,@function
rank:                                   # @rank
	.cfi_startproc
# BB#0:                                 # %.preheader84.preheader119
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movl	%edi, %r14d
	movslq	%r14d, %rax
	movl	%eax, key_array(,%rax,4)
	movl	$2097152, %ecx          # imm = 0x200000
	subl	%eax, %ecx
	movl	%ecx, key_array+40(,%rax,4)
	movslq	test_index_array(%rip), %rax
	movl	key_array(,%rax,4), %ebx
	movl	%ebx, partial_verify_vals(%rip)
	movslq	test_index_array+4(%rip), %rax
	movl	key_array(,%rax,4), %r15d
	movl	%r15d, partial_verify_vals+4(%rip)
	movslq	test_index_array+8(%rip), %rax
	movl	key_array(,%rax,4), %eax
	movl	%eax, partial_verify_vals+8(%rip)
	movslq	test_index_array+12(%rip), %rax
	movl	key_array(,%rax,4), %eax
	movl	%eax, partial_verify_vals+12(%rip)
	movslq	test_index_array+16(%rip), %rax
	movl	key_array(,%rax,4), %eax
	movl	%eax, partial_verify_vals+16(%rip)
	xorl	%ebp, %ebp
	movl	$bucket_size, %edi
	xorl	%esi, %esi
	movl	$4096, %edx             # imm = 0x1000
	callq	memset
	.p2align	4, 0x90
.LBB4_1:                                # %.preheader83
                                        # =>This Inner Loop Header: Depth=1
	movl	key_array(%rbp), %eax
	sarl	$11, %eax
	cltq
	incl	bucket_size(,%rax,4)
	movl	key_array+4(%rbp), %eax
	sarl	$11, %eax
	cltq
	incl	bucket_size(,%rax,4)
	addq	$8, %rbp
	cmpq	$134217728, %rbp        # imm = 0x8000000
	jne	.LBB4_1
# BB#2:
	movl	$0, bucket_ptrs(%rip)
	xorl	%eax, %eax
	movq	$-4092, %rcx            # imm = 0xF004
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	addl	bucket_size+4092(%rcx), %eax
	movl	%eax, bucket_ptrs+4096(%rcx)
	addl	bucket_size+4096(%rcx), %eax
	movl	%eax, bucket_ptrs+4100(%rcx)
	addl	bucket_size+4100(%rcx), %eax
	movl	%eax, bucket_ptrs+4104(%rcx)
	addq	$12, %rcx
	jne	.LBB4_3
# BB#4:                                 # %.preheader82.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader82
                                        # =>This Inner Loop Header: Depth=1
	movl	key_array(%rax), %ecx
	movl	%ecx, %edx
	sarl	$11, %edx
	movslq	%edx, %rdx
	movslq	bucket_ptrs(,%rdx,4), %rsi
	leal	1(%rsi), %edi
	movl	%edi, bucket_ptrs(,%rdx,4)
	movl	%ecx, key_buff2(,%rsi,4)
	movl	key_array+4(%rax), %ecx
	movl	%ecx, %edx
	sarl	$11, %edx
	movslq	%edx, %rdx
	movslq	bucket_ptrs(,%rdx,4), %rsi
	leal	1(%rsi), %edi
	movl	%edi, bucket_ptrs(,%rdx,4)
	movl	%ecx, key_buff2(,%rsi,4)
	addq	$8, %rax
	cmpq	$134217728, %rax        # imm = 0x8000000
	jne	.LBB4_5
# BB#6:                                 # %.preheader81.preheader
	xorl	%ebp, %ebp
	movl	$key_buff1, %edi
	xorl	%esi, %esi
	movl	$8388608, %edx          # imm = 0x800000
	callq	memset
	.p2align	4, 0x90
.LBB4_7:                                # %.preheader80
                                        # =>This Inner Loop Header: Depth=1
	movslq	key_buff2(%rbp), %rax
	incl	key_buff1(,%rax,4)
	movslq	key_buff2+4(%rbp), %rax
	incl	key_buff1(,%rax,4)
	movslq	key_buff2+8(%rbp), %rax
	incl	key_buff1(,%rax,4)
	movslq	key_buff2+12(%rbp), %rax
	incl	key_buff1(,%rax,4)
	addq	$16, %rbp
	cmpq	$134217728, %rbp        # imm = 0x8000000
	jne	.LBB4_7
# BB#8:                                 # %.preheader79.preheader
	movq	$-8388592, %rax         # imm = 0xFF800010
	movl	key_buff1(%rip), %ecx
	jmp	.LBB4_9
	.p2align	4, 0x90
.LBB4_33:                               # %.preheader79.3
                                        #   in Loop: Header=BB4_9 Depth=1
	addl	key_buff1+8388608(%rax), %ecx
	movl	%ecx, key_buff1+8388608(%rax)
	addq	$16, %rax
.LBB4_9:                                # %.preheader79
                                        # =>This Inner Loop Header: Depth=1
	addl	key_buff1+8388596(%rax), %ecx
	movl	%ecx, key_buff1+8388596(%rax)
	addl	key_buff1+8388600(%rax), %ecx
	movl	%ecx, key_buff1+8388600(%rax)
	addl	key_buff1+8388604(%rax), %ecx
	movl	%ecx, key_buff1+8388604(%rax)
	testq	%rax, %rax
	jne	.LBB4_33
# BB#10:                                # %.preheader.preheader
	decl	%ebx
	cmpl	$33554430, %ebx         # imm = 0x1FFFFFE
	ja	.LBB4_14
# BB#11:
	movl	test_rank_array(%rip), %eax
	movslq	%ebx, %rcx
	subl	%r14d, %eax
	cmpl	%eax, key_buff1(,%rcx,4)
	jne	.LBB4_13
# BB#12:                                # %.critedge78
	incl	passed_verification(%rip)
	jmp	.LBB4_14
.LBB4_13:
	movl	$.L.str.15, %edi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movl	partial_verify_vals+4(%rip), %r15d
.LBB4_14:                               # %.preheader.194
	decl	%r15d
	cmpl	$33554431, %r15d        # imm = 0x1FFFFFF
	jae	.LBB4_20
# BB#15:
	movslq	%r15d, %rax
	movl	test_rank_array+4(%rip), %ecx
	addl	%r14d, %ecx
	cmpl	%ecx, key_buff1(,%rax,4)
	jne	.LBB4_19
# BB#16:                                # %.critedge.1
	incl	passed_verification(%rip)
	jmp	.LBB4_20
.LBB4_19:
	movl	$.L.str.15, %edi
	movl	$1, %edx
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
.LBB4_20:                               # %.preheader.295
	movl	partial_verify_vals+8(%rip), %eax
	decl	%eax
	cmpl	$33554430, %eax         # imm = 0x1FFFFFE
	ja	.LBB4_24
# BB#21:
	cltq
	movl	test_rank_array+8(%rip), %ecx
	addl	%r14d, %ecx
	cmpl	%ecx, key_buff1(,%rax,4)
	jne	.LBB4_23
# BB#22:                                # %.critedge.2
	incl	passed_verification(%rip)
	jmp	.LBB4_24
.LBB4_23:
	movl	$.L.str.15, %edi
	movl	$2, %edx
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
.LBB4_24:                               # %.preheader.396
	movl	partial_verify_vals+12(%rip), %eax
	decl	%eax
	cmpl	$33554430, %eax         # imm = 0x1FFFFFE
	ja	.LBB4_28
# BB#25:
	movl	test_rank_array+12(%rip), %ecx
	cltq
	subl	%r14d, %ecx
	cmpl	%ecx, key_buff1(,%rax,4)
	jne	.LBB4_26
# BB#27:                                # %.critedge78.3
	incl	passed_verification(%rip)
	jmp	.LBB4_28
.LBB4_26:
	movl	$.L.str.15, %edi
	movl	$3, %edx
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
.LBB4_28:                               # %.preheader.497
	movl	partial_verify_vals+16(%rip), %eax
	decl	%eax
	cmpl	$33554430, %eax         # imm = 0x1FFFFFE
	ja	.LBB4_32
# BB#29:
	cltq
	movl	test_rank_array+16(%rip), %ecx
	addl	%r14d, %ecx
	cmpl	%ecx, key_buff1(,%rax,4)
	jne	.LBB4_31
# BB#30:                                # %.critedge.4
	incl	passed_verification(%rip)
	jmp	.LBB4_32
.LBB4_31:
	movl	$.L.str.15, %edi
	movl	$4, %edx
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
.LBB4_32:
	cmpl	$10, %r14d
	jne	.LBB4_18
# BB#17:
	movq	$key_buff1, key_buff_ptr_global(%rip)
.LBB4_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	rank, .Lfunc_end4-rank
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4734050326586327040     # double 314159265
.LCPI5_1:
	.quad	4742906807993761792     # double 1220703125
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movl	B_test_index_array+16(%rip), %eax
	movl	%eax, test_index_array+16(%rip)
	movaps	B_test_index_array(%rip), %xmm0
	movaps	%xmm0, test_index_array(%rip)
	movl	B_test_rank_array+16(%rip), %eax
	movl	%eax, test_rank_array+16(%rip)
	movaps	B_test_rank_array(%rip), %xmm0
	movaps	%xmm0, test_rank_array(%rip)
	movl	$.Lstr.7, %edi
	callq	puts
	xorl	%ebx, %ebx
	movl	$.L.str.17, %edi
	movl	$33554432, %esi         # imm = 0x2000000
	movl	$66, %edx
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.18, %edi
	movl	$10, %esi
	xorl	%eax, %eax
	callq	printf
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	create_seq
	movl	$1, %edi
	callq	rank
	movl	$0, passed_verification(%rip)
	movl	$.Lstr.8, %edi
	callq	puts
	movl	$.L.str.20, %edi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$1, %edi
	callq	rank
	movl	$.L.str.20, %edi
	movl	$2, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$2, %edi
	callq	rank
	movl	$.L.str.20, %edi
	movl	$3, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$3, %edi
	callq	rank
	movl	$.L.str.20, %edi
	movl	$4, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$4, %edi
	callq	rank
	movl	$.L.str.20, %edi
	movl	$5, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$5, %edi
	callq	rank
	movl	$.L.str.20, %edi
	movl	$6, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$6, %edi
	callq	rank
	movl	$.L.str.20, %edi
	movl	$7, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$7, %edi
	callq	rank
	movl	$.L.str.20, %edi
	movl	$8, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$8, %edi
	callq	rank
	movl	$.L.str.20, %edi
	movl	$9, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$9, %edi
	callq	rank
	movl	$.L.str.20, %edi
	movl	$10, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	rank
	movq	key_buff_ptr_global(%rip), %rax
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movslq	key_buff2(%rbx), %rcx
	movslq	(%rax,%rcx,4), %rdx
	leaq	-1(%rdx), %rsi
	movl	%esi, (%rax,%rcx,4)
	movl	%ecx, key_array-4(,%rdx,4)
	movslq	key_buff2+4(%rbx), %rcx
	movslq	(%rax,%rcx,4), %rdx
	leaq	-1(%rdx), %rsi
	movl	%esi, (%rax,%rcx,4)
	movl	%ecx, key_array-4(,%rdx,4)
	addq	$8, %rbx
	cmpq	$134217728, %rbx        # imm = 0x8000000
	jne	.LBB5_1
# BB#2:                                 # %.preheader.preheader.i
	movd	key_array(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	pshufd	$36, %xmm0, %xmm0       # xmm0 = xmm0[0,1,2,0]
	pxor	%xmm4, %xmm4
	xorl	%eax, %eax
	pxor	%xmm2, %xmm2
	jmp	.LBB5_3
	.p2align	4, 0x90
.LBB5_10:                               # %vector.body.1
                                        #   in Loop: Header=BB5_3 Depth=1
	movups	key_array+36(%rax), %xmm2
	shufps	$3, %xmm2, %xmm1        # xmm1 = xmm1[3,0],xmm2[0,0]
	shufps	$152, %xmm2, %xmm1      # xmm1 = xmm1[0,2],xmm2[1,2]
	pcmpgtd	%xmm2, %xmm1
	psrld	$31, %xmm1
	paddd	%xmm0, %xmm1
	movups	key_array+52(%rax), %xmm0
	shufps	$3, %xmm0, %xmm2        # xmm2 = xmm2[3,0],xmm0[0,0]
	shufps	$152, %xmm0, %xmm2      # xmm2 = xmm2[0,2],xmm0[1,2]
	pcmpgtd	%xmm0, %xmm2
	psrld	$31, %xmm2
	paddd	%xmm3, %xmm2
	addq	$64, %rax
	movdqa	%xmm1, %xmm4
.LBB5_3:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	key_array+4(%rax), %xmm3
	movups	key_array+20(%rax), %xmm1
	shufps	$3, %xmm3, %xmm0        # xmm0 = xmm0[3,0],xmm3[0,0]
	shufps	$152, %xmm3, %xmm0      # xmm0 = xmm0[0,2],xmm3[1,2]
	pcmpgtd	%xmm3, %xmm0
	shufps	$3, %xmm1, %xmm3        # xmm3 = xmm3[3,0],xmm1[0,0]
	shufps	$152, %xmm1, %xmm3      # xmm3 = xmm3[0,2],xmm1[1,2]
	psrld	$31, %xmm0
	pcmpgtd	%xmm1, %xmm3
	psrld	$31, %xmm3
	paddd	%xmm4, %xmm0
	paddd	%xmm2, %xmm3
	cmpq	$134217664, %rax        # imm = 0x7FFFFC0
	jne	.LBB5_10
# BB#4:                                 # %.preheader.i
	paddd	%xmm0, %xmm3
	pshufd	$78, %xmm3, %xmm0       # xmm0 = xmm3[2,3,0,1]
	paddd	%xmm3, %xmm0
	pshufd	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3]
	movd	%xmm1, %eax
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ecx
	movl	$134217700, %edx        # imm = 0x7FFFFE4
	movl	key_array(%rdx), %edx
	xorl	%esi, %esi
	cmpl	%edx, %eax
	setg	%sil
	addl	%ecx, %esi
	movl	$134217704, %eax        # imm = 0x7FFFFE8
	movl	key_array(%rax), %eax
	xorl	%ecx, %ecx
	cmpl	%eax, %edx
	setg	%cl
	addl	%esi, %ecx
	movl	$134217708, %edx        # imm = 0x7FFFFEC
	movl	key_array(%rdx), %edx
	xorl	%esi, %esi
	cmpl	%edx, %eax
	setg	%sil
	addl	%ecx, %esi
	movl	$134217712, %eax        # imm = 0x7FFFFF0
	movl	key_array(%rax), %eax
	xorl	%ecx, %ecx
	cmpl	%eax, %edx
	setg	%cl
	addl	%esi, %ecx
	movl	$134217716, %edx        # imm = 0x7FFFFF4
	movl	key_array(%rdx), %edx
	xorl	%esi, %esi
	cmpl	%edx, %eax
	setg	%sil
	addl	%ecx, %esi
	movl	$134217720, %eax        # imm = 0x7FFFFF8
	movl	key_array(%rax), %ecx
	xorl	%edi, %edi
	cmpl	%ecx, %edx
	setg	%dil
	addl	%esi, %edi
	movl	$134217724, %edx        # imm = 0x7FFFFFC
	xorl	%eax, %eax
	cmpl	key_array(%rdx), %ecx
	setg	%al
	addl	%edi, %eax
	je	.LBB5_6
# BB#5:
	movslq	%eax, %rsi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movl	passed_verification(%rip), %ecx
	jmp	.LBB5_7
.LBB5_6:
	movl	passed_verification(%rip), %ecx
	incl	%ecx
	movl	%ecx, passed_verification(%rip)
.LBB5_7:                                # %full_verify.exit
	movl	$51, %eax
	cmpl	$51, %ecx
	je	.LBB5_9
# BB#8:
	movl	$0, passed_verification(%rip)
	xorl	%eax, %eax
.LBB5_9:
	subq	$8, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.21, %edi
	movl	$66, %esi
	movl	$524288, %edx           # imm = 0x80000
	movl	$64, %ecx
	movl	$0, %r8d
	movl	$10, %r9d
	pushq	$.L.str.23
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$.L.str.22
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	c_print_results
	addq	$32, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -32
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	main, .Lfunc_end5-main
	.cfi_endproc

	.type	S_test_index_array,@object # @S_test_index_array
	.data
	.globl	S_test_index_array
	.p2align	4
S_test_index_array:
	.long	48427                   # 0xbd2b
	.long	17148                   # 0x42fc
	.long	23627                   # 0x5c4b
	.long	62548                   # 0xf454
	.long	4431                    # 0x114f
	.size	S_test_index_array, 20

	.type	S_test_rank_array,@object # @S_test_rank_array
	.globl	S_test_rank_array
	.p2align	4
S_test_rank_array:
	.long	0                       # 0x0
	.long	18                      # 0x12
	.long	346                     # 0x15a
	.long	64917                   # 0xfd95
	.long	65463                   # 0xffb7
	.size	S_test_rank_array, 20

	.type	W_test_index_array,@object # @W_test_index_array
	.globl	W_test_index_array
	.p2align	4
W_test_index_array:
	.long	357773                  # 0x5758d
	.long	934767                  # 0xe436f
	.long	875723                  # 0xd5ccb
	.long	898999                  # 0xdb7b7
	.long	404505                  # 0x62c19
	.size	W_test_index_array, 20

	.type	W_test_rank_array,@object # @W_test_rank_array
	.globl	W_test_rank_array
	.p2align	4
W_test_rank_array:
	.long	1249                    # 0x4e1
	.long	11698                   # 0x2db2
	.long	1039987                 # 0xfde73
	.long	1043896                 # 0xfedb8
	.long	1048018                 # 0xffdd2
	.size	W_test_rank_array, 20

	.type	A_test_index_array,@object # @A_test_index_array
	.globl	A_test_index_array
	.p2align	4
A_test_index_array:
	.long	2112377                 # 0x203b79
	.long	662041                  # 0xa1a19
	.long	5336171                 # 0x516c6b
	.long	3642833                 # 0x3795d1
	.long	4250760                 # 0x40dc88
	.size	A_test_index_array, 20

	.type	A_test_rank_array,@object # @A_test_rank_array
	.globl	A_test_rank_array
	.p2align	4
A_test_rank_array:
	.long	104                     # 0x68
	.long	17523                   # 0x4473
	.long	123928                  # 0x1e418
	.long	8288932                 # 0x7e7aa4
	.long	8388264                 # 0x7ffea8
	.size	A_test_rank_array, 20

	.type	B_test_index_array,@object # @B_test_index_array
	.globl	B_test_index_array
	.p2align	4
B_test_index_array:
	.long	41869                   # 0xa38d
	.long	812306                  # 0xc6512
	.long	5102857                 # 0x4ddd09
	.long	18232239                # 0x11633af
	.long	26860214                # 0x199dab6
	.size	B_test_index_array, 20

	.type	B_test_rank_array,@object # @B_test_rank_array
	.globl	B_test_rank_array
	.p2align	4
B_test_rank_array:
	.long	33422937                # 0x1fdfe59
	.long	10244                   # 0x2804
	.long	59149                   # 0xe70d
	.long	33135281                # 0x1f99ab1
	.long	99                      # 0x63
	.size	B_test_rank_array, 20

	.type	C_test_index_array,@object # @C_test_index_array
	.globl	C_test_index_array
	.p2align	4
C_test_index_array:
	.long	44172927                # 0x2a2067f
	.long	72999161                # 0x459e0f9
	.long	74326391                # 0x46e2177
	.long	129606274               # 0x7b9a282
	.long	21736814                # 0x14bad6e
	.size	C_test_index_array, 20

	.type	C_test_rank_array,@object # @C_test_rank_array
	.globl	C_test_rank_array
	.p2align	4
C_test_rank_array:
	.long	61147                   # 0xeedb
	.long	882988                  # 0xd792c
	.long	266290                  # 0x41032
	.long	133997595               # 0x7fca41b
	.long	133525895               # 0x7f57187
	.size	C_test_rank_array, 20

	.type	D_test_index_array,@object # @D_test_index_array
	.globl	D_test_index_array
	.p2align	4
D_test_index_array:
	.long	1317351170              # 0x4e852f02
	.long	995930646               # 0x3b5cb216
	.long	1157283250              # 0x44fabdb2
	.long	1503301535              # 0x599a8f9f
	.long	1453734525              # 0x56a63a7d
	.size	D_test_index_array, 20

	.type	D_test_rank_array,@object # @D_test_rank_array
	.globl	D_test_rank_array
	.p2align	4
D_test_rank_array:
	.long	1                       # 0x1
	.long	36538729                # 0x22d8969
	.long	1978098519              # 0x75e76357
	.long	2145192618              # 0x7fdd0aaa
	.long	2147425337              # 0x7fff1c39
	.size	D_test_rank_array, 20

	.type	randlc.KS,@object       # @randlc.KS
	.local	randlc.KS
	.comm	randlc.KS,1,4
	.type	randlc.R23,@object      # @randlc.R23
	.local	randlc.R23
	.comm	randlc.R23,8,8
	.type	randlc.R46,@object      # @randlc.R46
	.local	randlc.R46
	.comm	randlc.R46,8,8
	.type	randlc.T23,@object      # @randlc.T23
	.local	randlc.T23
	.comm	randlc.T23,8,8
	.type	randlc.T46,@object      # @randlc.T46
	.local	randlc.T46
	.comm	randlc.T46,8,8
	.type	key_array,@object       # @key_array
	.comm	key_array,134217728,16
	.type	key_buff2,@object       # @key_buff2
	.comm	key_buff2,134217728,16
	.type	key_buff_ptr_global,@object # @key_buff_ptr_global
	.comm	key_buff_ptr_global,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Full_verify: number of keys out of sort: %ld\n"
	.size	.L.str, 46

	.type	passed_verification,@object # @passed_verification
	.comm	passed_verification,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n\n %s Benchmark Completed\n"
	.size	.L.str.1, 27

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" Class           =                        %c\n"
	.size	.L.str.2, 46

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	" Size            =             %12ld\n"
	.size	.L.str.3, 38

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" Size            =             %4dx%4dx%4d\n"
	.size	.L.str.4, 44

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" Iterations      =             %12d\n"
	.size	.L.str.5, 37

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" Operation type  = %24s\n"
	.size	.L.str.6, 25

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	" Version         =             %12s\n"
	.size	.L.str.10, 37

	.type	test_index_array,@object # @test_index_array
	.comm	test_index_array,20,16
	.type	partial_verify_vals,@object # @partial_verify_vals
	.comm	partial_verify_vals,20,16
	.type	bucket_size,@object     # @bucket_size
	.comm	bucket_size,4096,16
	.type	bucket_ptrs,@object     # @bucket_ptrs
	.comm	bucket_ptrs,4096,16
	.type	key_buff1,@object       # @key_buff1
	.comm	key_buff1,8388608,16
	.type	test_rank_array,@object # @test_rank_array
	.comm	test_rank_array,20,16
	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Failed partial verification: iteration %d, test key %d\n"
	.size	.L.str.15, 56

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	" Size:  %ld  (class %c)\n"
	.size	.L.str.17, 25

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	" Iterations:   %d\n"
	.size	.L.str.18, 19

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"        %d\n"
	.size	.L.str.20, 12

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"IS"
	.size	.L.str.21, 3

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"keys ranked"
	.size	.L.str.22, 12

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"3.3"
	.size	.L.str.23, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	" Verification    =             UNSUCCESSFUL"
	.size	.Lstr, 44

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.1:
	.asciz	"\n"
	.size	.Lstr.1, 2

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	" Please send all errors/feedbacks to:\n"
	.size	.Lstr.2, 39

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	" NPB Development Team"
	.size	.Lstr.3, 22

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	" npb@nas.nasa.gov\n"
	.size	.Lstr.4, 19

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	" Verification    =               SUCCESSFUL"
	.size	.Lstr.5, 44

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	" Verification    =            NOT PERFORMED"
	.size	.Lstr.6, 44

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"\n\n NAS Parallel Benchmarks (NPB3.3-SER) - IS Benchmark\n"
	.size	.Lstr.7, 56

	.type	.Lstr.8,@object         # @str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.8:
	.asciz	"\n   iteration"
	.size	.Lstr.8, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
