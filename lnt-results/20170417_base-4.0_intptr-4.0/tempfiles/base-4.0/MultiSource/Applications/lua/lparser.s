	.text
	.file	"lparser.bc"
	.hidden	luaY_parser
	.globl	luaY_parser
	.p2align	4, 0x90
	.type	luaY_parser,@function
luaY_parser:                            # @luaY_parser
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$696, %rsp              # imm = 0x2B8
.Lcfi4:
	.cfi_def_cfa_offset 736
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movq	%rdx, 72(%rsp)
	movq	%rbx, %rdi
	callq	strlen
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	luaS_newlstr
	movq	%rsp, %r15
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%rax, %rcx
	callq	luaX_setinput
	movq	56(%rsp), %rbx
	movq	%rbx, %rdi
	callq	luaF_newproto
	movq	%rax, %r14
	movq	%r14, 96(%rsp)
	movq	48(%rsp), %rax
	movq	%rax, 112(%rsp)
	movq	%r15, 120(%rsp)
	movq	%rbx, 128(%rsp)
	leaq	96(%rsp), %rax
	movq	%rax, 48(%rsp)
	movl	$0, 144(%rsp)
	movl	$-1, 148(%rsp)
	movl	$-1, 152(%rsp)
	movq	$0, 136(%rsp)
	movq	$0, 163(%rsp)
	movq	$0, 156(%rsp)
	movq	80(%rsp), %rax
	movq	%rax, 64(%r14)
	movb	$2, 115(%r14)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaH_new
	movq	%rax, 104(%rsp)
	movq	16(%rbx), %rcx
	movq	%rax, (%rcx)
	movl	$5, 8(%rcx)
	movq	16(%rbx), %rax
	movq	56(%rbx), %rcx
	subq	%rax, %rcx
	cmpq	$16, %rcx
	jg	.LBB0_2
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	luaD_growstack
	movq	16(%rbx), %rax
.LBB0_2:
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%rbx)
	movq	%r14, 16(%rax)
	movl	$9, 24(%rax)
	movq	16(%rbx), %rax
	movq	56(%rbx), %rcx
	subq	%rax, %rcx
	cmpq	$16, %rcx
	jg	.LBB0_4
# BB#3:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	luaD_growstack
	movq	16(%rbx), %rax
.LBB0_4:                                # %open_func.exit
	addq	$16, %rax
	movq	%rax, 16(%rbx)
	movq	96(%rsp), %rax
	movb	$2, 114(%rax)
	movq	%rsp, %rdi
	callq	luaX_next
	movq	56(%rsp), %rax
	movzwl	96(%rax), %ecx
	incl	%ecx
	movw	%cx, 96(%rax)
	movzwl	%cx, %eax
	cmpl	$201, %eax
	jb	.LBB0_6
# BB#5:
	movq	%rsp, %rdi
	movl	$.L.str.1, %esi
	xorl	%edx, %edx
	callq	luaX_lexerror
.LBB0_6:                                # %enterlevel.exit.preheader.i
	movq	%rsp, %rbx
	movl	$134283271, %r14d       # imm = 0x8010007
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movl	16(%rsp), %eax
	leal	-260(%rax), %ecx
	cmpl	$27, %ecx
	ja	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	btl	%ecx, %r14d
	jb	.LBB0_13
.LBB0_9:                                #   in Loop: Header=BB0_7 Depth=1
	movq	%rbx, %rdi
	callq	statement
	movl	%eax, %ebp
	cmpl	$59, 16(%rsp)
	jne	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_7 Depth=1
	movq	%rbx, %rdi
	callq	luaX_next
.LBB0_11:                               # %testnext.exit.i
                                        #   in Loop: Header=BB0_7 Depth=1
	movq	48(%rsp), %rax
	movzbl	74(%rax), %ecx
	movl	%ecx, 60(%rax)
	testl	%ebp, %ebp
	je	.LBB0_7
# BB#12:                                # %testnext.exit.i.chunk.exit_crit_edge
	movl	16(%rsp), %eax
.LBB0_13:                               # %chunk.exit
	movq	56(%rsp), %rbx
	decw	96(%rbx)
	cmpl	$287, %eax              # imm = 0x11F
	je	.LBB0_15
# BB#14:
	movq	%rsp, %rbp
	movl	$287, %esi              # imm = 0x11F
	movq	%rbp, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB0_15:                               # %check.exit
	movq	%rsp, %rdi
	callq	close_func
	movq	96(%rsp), %rax
	addq	$696, %rsp              # imm = 0x2B8
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	luaY_parser, .Lfunc_end0-luaY_parser
	.cfi_endproc

	.p2align	4, 0x90
	.type	open_func,@function
open_func:                              # @open_func
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	56(%r12), %r15
	movq	%r15, %rdi
	callq	luaF_newproto
	movq	%rax, %r14
	movq	%r14, (%rbx)
	movq	48(%r12), %rax
	movq	%rax, 16(%rbx)
	movq	%r12, 24(%rbx)
	movq	%r15, 32(%rbx)
	movq	%rbx, 48(%r12)
	movl	$0, 48(%rbx)
	movl	$-1, 52(%rbx)
	movl	$-1, 56(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 67(%rbx)
	movq	$0, 60(%rbx)
	movq	80(%r12), %rax
	movq	%rax, 64(%r14)
	movb	$2, 115(%r14)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	luaH_new
	movq	%rax, 8(%rbx)
	movq	16(%r15), %rcx
	movq	%rax, (%rcx)
	movl	$5, 8(%rcx)
	movq	16(%r15), %rax
	movq	56(%r15), %rcx
	subq	%rax, %rcx
	cmpq	$16, %rcx
	jg	.LBB1_2
# BB#1:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	luaD_growstack
	movq	16(%r15), %rax
.LBB1_2:
	leaq	16(%rax), %rcx
	movq	%rcx, 16(%r15)
	movq	%r14, 16(%rax)
	movl	$9, 24(%rax)
	movq	16(%r15), %rax
	movq	56(%r15), %rcx
	subq	%rax, %rcx
	cmpq	$16, %rcx
	jg	.LBB1_4
# BB#3:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	luaD_growstack
	movq	16(%r15), %rax
.LBB1_4:
	addq	$16, %rax
	movq	%rax, 16(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	open_func, .Lfunc_end1-open_func
	.cfi_endproc

	.p2align	4, 0x90
	.type	close_func,@function
close_func:                             # @close_func
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	48(%r14), %rbx
	movq	56(%r14), %r15
	movq	(%rbx), %r12
	movzbl	74(%rbx), %esi
	testq	%rsi, %rsi
	je	.LBB2_6
# BB#1:                                 # %.lr.ph.i
	movq	48(%r12), %rax
	movl	48(%rbx), %ecx
	testb	$1, %sil
	movq	%rsi, %rdx
	je	.LBB2_3
# BB#2:
	movl	%esi, %edx
	decb	%dl
	movb	%dl, 74(%rbx)
	movzbl	%dl, %edx
	movzwl	196(%rbx,%rdx,2), %edx
	shlq	$4, %rdx
	movl	%ecx, 12(%rax,%rdx)
	leaq	-1(%rsi), %rdx
.LBB2_3:                                # %.prol.loopexit
	cmpb	$1, %sil
	je	.LBB2_6
# BB#4:                                 # %.lr.ph.i.new
	movb	%dl, %sil
	decb	%sil
	addl	$255, %edx
	decb	%dl
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movb	%sil, 74(%rbx)
	movzbl	%sil, %esi
	movzwl	196(%rbx,%rsi,2), %edi
	shlq	$4, %rdi
	movl	%ecx, 12(%rax,%rdi)
	movb	%dl, 74(%rbx)
	movzbl	%dl, %edx
	movzwl	196(%rbx,%rdx,2), %edi
	shlq	$4, %rdi
	movl	%ecx, 12(%rax,%rdi)
	addb	$-2, %sil
	addb	$-2, %dl
	cmpb	$-2, %dl
	jne	.LBB2_5
.LBB2_6:                                # %removevars.exit
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaK_ret
	movslq	48(%rbx), %rcx
	cmpq	$-1, %rcx
	jl	.LBB2_8
# BB#7:
	leaq	24(%r12), %rbp
	movq	24(%r12), %rsi
	leaq	80(%r12), %r13
	movslq	80(%r12), %rdx
	shlq	$2, %rdx
	shlq	$2, %rcx
	movq	%r15, %rdi
	callq	luaM_realloc_
	jmp	.LBB2_9
.LBB2_8:
	movq	%r15, %rdi
	callq	luaM_toobig
	leaq	24(%r12), %rbp
	leaq	80(%r12), %r13
.LBB2_9:
	movq	%rax, (%rbp)
	movslq	48(%rbx), %rcx
	cmpq	$-1, %rcx
	movl	%ecx, (%r13)
	jl	.LBB2_11
# BB#10:
	leaq	40(%r12), %r13
	movq	40(%r12), %rsi
	leaq	84(%r12), %rbp
	movslq	84(%r12), %rdx
	shlq	$2, %rdx
	shlq	$2, %rcx
	movq	%r15, %rdi
	callq	luaM_realloc_
	jmp	.LBB2_12
.LBB2_11:
	movq	%r15, %rdi
	callq	luaM_toobig
	leaq	40(%r12), %r13
	leaq	84(%r12), %rbp
.LBB2_12:
	movq	%rax, (%r13)
	movl	48(%rbx), %eax
	movl	%eax, (%rbp)
	movslq	64(%rbx), %rcx
	cmpq	$-1, %rcx
	jl	.LBB2_14
# BB#13:
	leaq	16(%r12), %r13
	movq	16(%r12), %rsi
	leaq	76(%r12), %rbp
	movslq	76(%r12), %rdx
	shlq	$4, %rdx
	shlq	$4, %rcx
	movq	%r15, %rdi
	callq	luaM_realloc_
	jmp	.LBB2_15
.LBB2_14:
	movq	%r15, %rdi
	callq	luaM_toobig
	leaq	16(%r12), %r13
	leaq	76(%r12), %rbp
.LBB2_15:
	movq	%rax, (%r13)
	movl	64(%rbx), %eax
	movl	%eax, (%rbp)
	movslq	68(%rbx), %rcx
	cmpq	$-1, %rcx
	jl	.LBB2_17
# BB#16:
	leaq	32(%r12), %r13
	movq	32(%r12), %rsi
	leaq	88(%r12), %rbp
	movslq	88(%r12), %rdx
	shlq	$3, %rdx
	shlq	$3, %rcx
	movq	%r15, %rdi
	callq	luaM_realloc_
	jmp	.LBB2_18
.LBB2_17:
	movq	%r15, %rdi
	callq	luaM_toobig
	leaq	32(%r12), %r13
	leaq	88(%r12), %rbp
.LBB2_18:
	movq	%rax, (%r13)
	movl	68(%rbx), %eax
	movl	%eax, (%rbp)
	movswq	72(%rbx), %rcx
	leaq	1(%rcx), %rax
	shrq	$60, %rax
	jne	.LBB2_20
# BB#19:
	movq	48(%r12), %rsi
	movslq	92(%r12), %rdx
	shlq	$4, %rdx
	shlq	$4, %rcx
	movq	%r15, %rdi
	callq	luaM_realloc_
	jmp	.LBB2_21
.LBB2_20:
	movq	%r15, %rdi
	callq	luaM_toobig
.LBB2_21:
	movq	%rax, 48(%r12)
	movswl	72(%rbx), %eax
	movl	%eax, 92(%r12)
	movq	56(%r12), %rsi
	movslq	72(%r12), %rdx
	shlq	$3, %rdx
	movzbl	112(%r12), %ecx
	shlq	$3, %rcx
	movq	%r15, %rdi
	callq	luaM_realloc_
	movq	%rax, 56(%r12)
	movzbl	112(%r12), %eax
	movl	%eax, 72(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 48(%r14)
	addq	$-32, 16(%r15)
	movl	$-285, %eax             # imm = 0xFEE3
	addl	16(%r14), %eax
	cmpl	$1, %eax
	ja	.LBB2_22
# BB#23:
	movq	24(%r14), %rsi
	movq	16(%rsi), %rdx
	addq	$24, %rsi
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaX_newstring          # TAILCALL
.LBB2_22:                               # %anchor_token.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	close_func, .Lfunc_end2-close_func
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4607182418800017408     # double 1
	.text
	.p2align	4, 0x90
	.type	statement,@function
statement:                              # @statement
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 160
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	$-258, %eax             # imm = 0xFEFE
	addl	16(%r13), %eax
	cmpl	$19, %eax
	ja	.LBB3_53
# BB#1:
	movl	4(%r13), %ebp
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_2:
	movq	%r13, %rdi
	callq	luaX_next
	movq	48(%r13), %r14
	movq	40(%r14), %rbp
	xorl	%ebx, %ebx
	testq	%rbp, %rbp
	je	.LBB3_68
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 14(%rbp)
	jne	.LBB3_69
# BB#3:                                 #   in Loop: Header=BB3_4 Depth=1
	movzbl	13(%rbp), %eax
	orl	%eax, %ebx
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB3_4
.LBB3_68:                               # %.critedge19.i
	movl	$.L.str.25, %esi
	movq	%r13, %rdi
	callq	luaX_syntaxerror
	xorl	%ebp, %ebp
.LBB3_69:                               # %.critedge.i
	testl	%ebx, %ebx
	je	.LBB3_71
# BB#70:
	movzbl	12(%rbp), %edx
	movl	$35, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	callq	luaK_codeABC
.LBB3_71:                               # %breakstat.exit
	addq	$8, %rbp
	movq	%r14, %rdi
	callq	luaK_jump
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movl	%eax, %edx
	callq	luaK_concat
	movl	$1, %ebp
	jmp	.LBB3_174
.LBB3_5:
	movq	%r13, %rdi
	callq	luaX_next
	movq	%r13, %rdi
	callq	block
	movl	$262, %esi              # imm = 0x106
	movl	$259, %edx              # imm = 0x103
	movq	%r13, %rdi
	movl	%ebp, %ecx
	callq	check_match
	jmp	.LBB3_173
.LBB3_6:
	movq	48(%r13), %r14
	movl	$-1, 56(%rsp)
	movb	$1, 62(%rsp)
	movb	74(%r14), %al
	movb	%al, 60(%rsp)
	movb	$0, 61(%rsp)
	movq	40(%r14), %rax
	movq	%rax, 48(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 40(%r14)
	movq	%r13, %rdi
	callq	luaX_next
	cmpl	$285, 16(%r13)          # imm = 0x11D
	je	.LBB3_8
# BB#7:
	movq	56(%r13), %rbx
	movl	$285, %esi              # imm = 0x11D
	movq	%r13, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB3_8:                                # %str_checkname.exit.i
	movq	24(%r13), %r12
	movq	%r13, %rdi
	callq	luaX_next
	movl	16(%r13), %eax
	cmpl	$44, %eax
	je	.LBB3_72
# BB#9:                                 # %str_checkname.exit.i
	cmpl	$267, %eax              # imm = 0x10B
	je	.LBB3_72
# BB#10:                                # %str_checkname.exit.i
	cmpl	$61, %eax
	jne	.LBB3_139
# BB#11:
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	48(%r13), %rbp
	movl	60(%rbp), %r15d
	movl	$.L.str.19, %esi
	movl	$11, %edx
	movq	%r13, %rdi
	callq	luaX_newstring
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	new_localvar
	movl	$.L.str.20, %esi
	movl	$11, %edx
	movq	%r13, %rdi
	callq	luaX_newstring
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	new_localvar
	movl	$.L.str.21, %esi
	movl	$10, %edx
	movq	%r13, %rdi
	callq	luaX_newstring
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	new_localvar
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	new_localvar
	cmpl	$61, 16(%r13)
	je	.LBB3_13
# BB#12:
	movq	56(%r13), %rbx
	movl	$61, %esi
	movq	%r13, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB3_13:                               # %checknext.exit.i18.i
	movq	%r13, %rdi
	callq	luaX_next
	leaq	16(%rsp), %rbx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	subexpr
	movq	48(%r13), %rdi
	movq	%rbx, %rsi
	callq	luaK_exp2nextreg
	cmpl	$44, 16(%r13)
	je	.LBB3_15
# BB#14:
	movq	56(%r13), %rbx
	movl	$44, %esi
	movq	%r13, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB3_15:                               # %checknext.exit25.i.i
	movq	%r13, %rdi
	callq	luaX_next
	leaq	16(%rsp), %rbx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	subexpr
	movq	48(%r13), %rdi
	movq	%rbx, %rsi
	callq	luaK_exp2nextreg
	cmpl	$44, 16(%r13)
	jne	.LBB3_160
# BB#16:
	movq	%r13, %rdi
	callq	luaX_next
	leaq	16(%rsp), %rbx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	subexpr
	movq	48(%r13), %rdi
	movq	%rbx, %rsi
	callq	luaK_exp2nextreg
	jmp	.LBB3_161
.LBB3_17:
	movq	%r13, %rdi
	callq	luaX_next
	leaq	16(%rsp), %rbx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	singlevar
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_18:                               #   in Loop: Header=BB3_19 Depth=1
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	field
.LBB3_19:                               # =>This Inner Loop Header: Depth=1
	movl	16(%r13), %eax
	cmpl	$46, %eax
	je	.LBB3_18
# BB#20:
	cmpl	$58, %eax
	jne	.LBB3_86
# BB#21:
	leaq	16(%rsp), %rsi
	movq	%r13, %rdi
	callq	field
	movl	$1, %edx
	jmp	.LBB3_87
.LBB3_22:
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	48(%r13), %rbp
	movl	$-1, 48(%rsp)
	leaq	16(%rsp), %r14
	leaq	48(%rsp), %r12
	jmp	.LBB3_24
	.p2align	4, 0x90
.LBB3_23:                               #   in Loop: Header=BB3_24 Depth=1
	movq	%rbp, %rdi
	callq	luaK_jump
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movl	%eax, %edx
	callq	luaK_concat
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	luaK_patchtohere
.LBB3_24:                               # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	callq	luaX_next
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	subexpr
	cmpl	$1, 16(%rsp)
	jne	.LBB3_26
# BB#25:                                #   in Loop: Header=BB3_24 Depth=1
	movl	$3, 16(%rsp)
.LBB3_26:                               # %cond.exit.i.i
                                        #   in Loop: Header=BB3_24 Depth=1
	movq	48(%r13), %rdi
	movq	%r14, %rsi
	callq	luaK_goiftrue
	movl	36(%rsp), %ebx
	cmpl	$274, 16(%r13)          # imm = 0x112
	je	.LBB3_28
# BB#27:                                #   in Loop: Header=BB3_24 Depth=1
	movq	56(%r13), %r15
	movl	$274, %esi              # imm = 0x112
	movq	%r13, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB3_28:                               # %test_then_block.exit.i
                                        #   in Loop: Header=BB3_24 Depth=1
	movq	%r13, %rdi
	callq	luaX_next
	movq	%r13, %rdi
	callq	block
	movl	16(%r13), %eax
	cmpl	$261, %eax              # imm = 0x105
	je	.LBB3_23
# BB#29:                                # %test_then_block.exit.i
	cmpl	$260, %eax              # imm = 0x104
	jne	.LBB3_88
# BB#30:
	movq	%rbp, %rdi
	callq	luaK_jump
	leaq	48(%rsp), %rsi
	movq	%rbp, %rdi
	movl	%eax, %edx
	callq	luaK_concat
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	luaK_patchtohere
	movq	%r13, %rdi
	callq	luaX_next
	movq	%r13, %rdi
	callq	block
	jmp	.LBB3_89
.LBB3_31:
	movq	%r13, %rdi
	callq	luaX_next
	movl	16(%r13), %eax
	cmpl	$265, %eax              # imm = 0x109
	jne	.LBB3_90
# BB#32:
	movq	%r13, %rdi
	callq	luaX_next
	movq	48(%r13), %r14
	cmpl	$285, 16(%r13)          # imm = 0x11D
	je	.LBB3_34
# BB#33:
	movq	56(%r13), %rbx
	movl	$285, %esi              # imm = 0x11D
	movq	%r13, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB3_34:                               # %str_checkname.exit.i43
	movq	24(%r13), %rbx
	movq	%r13, %rdi
	callq	luaX_next
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	new_localvar
	movl	60(%r14), %eax
	movl	$-1, 32(%rsp)
	movl	$-1, 36(%rsp)
	movl	$6, 16(%rsp)
	movl	%eax, 24(%rsp)
	movl	$1, %esi
	movq	%r14, %rdi
	callq	luaK_reserveregs
	movq	48(%r13), %rax
	movzbl	74(%rax), %ecx
	incl	%ecx
	movb	%cl, 74(%rax)
	movl	48(%rax), %edx
	movq	(%rax), %rsi
	movq	48(%rsi), %rsi
	movzbl	%cl, %ecx
	movzwl	194(%rax,%rcx,2), %eax
	shlq	$4, %rax
	movl	%edx, 8(%rsi,%rax)
	movl	4(%r13), %ecx
	leaq	48(%rsp), %rbx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	body
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	luaK_storevar
	movl	48(%r14), %eax
	movq	(%r14), %rcx
	movq	48(%rcx), %rcx
	movzbl	74(%r14), %edx
	movzwl	194(%r14,%rdx,2), %edx
	shlq	$4, %rdx
	movl	%eax, 8(%rcx,%rdx)
	jmp	.LBB3_174
.LBB3_35:
	movl	%ebp, %r12d
	movq	48(%r13), %r15
	movq	%r15, %rdi
	callq	luaK_getlabel
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	$-1, 56(%rsp)
	movb	$1, 62(%rsp)
	movb	74(%r15), %al
	movb	%al, 60(%rsp)
	movb	$0, 61(%rsp)
	movq	40(%r15), %rcx
	movq	%rcx, 48(%rsp)
	movl	$-1, 88(%rsp)
	movb	$0, 94(%rsp)
	movb	%al, 92(%rsp)
	movb	$0, 93(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	80(%rsp), %rax
	movq	%rax, 40(%r15)
	movq	%r13, %rdi
	callq	luaX_next
	movq	56(%r13), %rax
	movzwl	96(%rax), %ecx
	incl	%ecx
	movw	%cx, 96(%rax)
	movzwl	%cx, %eax
	cmpl	$201, %eax
	jb	.LBB3_37
# BB#36:
	movl	$.L.str.1, %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	luaX_lexerror
.LBB3_37:                               # %enterlevel.exit.i.i.preheader.preheader
	movl	$-260, %ebp             # imm = 0xFEFC
	movl	$134283271, %r14d       # imm = 0x8010007
	.p2align	4, 0x90
.LBB3_38:                               # %enterlevel.exit.i.i.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%r13), %eax
	addl	%ebp, %eax
	cmpl	$27, %eax
	ja	.LBB3_40
# BB#39:                                # %enterlevel.exit.i.i.preheader
                                        #   in Loop: Header=BB3_38 Depth=1
	btl	%eax, %r14d
	jb	.LBB3_43
.LBB3_40:                               #   in Loop: Header=BB3_38 Depth=1
	movq	%r13, %rdi
	callq	statement
	movl	%eax, %ebx
	cmpl	$59, 16(%r13)
	jne	.LBB3_42
# BB#41:                                #   in Loop: Header=BB3_38 Depth=1
	movq	%r13, %rdi
	callq	luaX_next
.LBB3_42:                               # %testnext.exit.i.i33
                                        #   in Loop: Header=BB3_38 Depth=1
	movq	48(%r13), %rax
	movzbl	74(%rax), %ecx
	movl	%ecx, 60(%rax)
	testl	%ebx, %ebx
	je	.LBB3_38
.LBB3_43:                               # %chunk.exit.i
	movq	56(%r13), %rax
	decw	96(%rax)
	movl	$276, %esi              # imm = 0x114
	movl	$272, %edx              # imm = 0x110
	movq	%r13, %rdi
	movl	%r12d, %ecx
	callq	check_match
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	subexpr
	cmpl	$1, 16(%rsp)
	jne	.LBB3_45
# BB#44:
	movl	$3, 16(%rsp)
.LBB3_45:                               # %cond.exit.i34
	movq	48(%r13), %rdi
	leaq	16(%rsp), %rsi
	callq	luaK_goiftrue
	movl	36(%rsp), %ebp
	cmpb	$0, 93(%rsp)
	je	.LBB3_112
# BB#46:
	movq	48(%r13), %r12
	movq	40(%r12), %rbx
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	je	.LBB3_100
	.p2align	4, 0x90
.LBB3_48:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 14(%rbx)
	jne	.LBB3_101
# BB#47:                                #   in Loop: Header=BB3_48 Depth=1
	movzbl	13(%rbx), %eax
	orl	%eax, %r14d
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB3_48
.LBB3_100:                              # %.critedge19.i.i
	movl	$.L.str.25, %esi
	movq	%r13, %rdi
	callq	luaX_syntaxerror
	xorl	%ebx, %ebx
.LBB3_101:                              # %.critedge.i.i
	testl	%r14d, %r14d
	je	.LBB3_103
# BB#102:
	movzbl	12(%rbx), %edx
	movl	$35, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	callq	luaK_codeABC
.LBB3_103:                              # %breakstat.exit.i
	addq	$8, %rbx
	movq	%r12, %rdi
	callq	luaK_jump
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%eax, %edx
	callq	luaK_concat
	movq	48(%r13), %rdi
	movl	%ebp, %esi
	callq	luaK_patchtohere
	movq	40(%r15), %r14
	movq	(%r14), %rax
	movq	%rax, 40(%r15)
	movq	24(%r15), %rax
	movzbl	12(%r14), %edx
	movq	48(%rax), %rax
	movzbl	74(%rax), %ecx
	cmpb	%dl, %cl
	jbe	.LBB3_109
# BB#104:                               # %.lr.ph.i.i28.i
	movq	(%rax), %rsi
	movq	48(%rsi), %r9
	movl	48(%rax), %edi
	movl	%ecx, %ebx
	subb	%dl, %bl
	movl	%ecx, %r8d
	decb	%r8b
	testb	$1, %bl
	je	.LBB3_106
# BB#105:
	movl	%ecx, %ebx
	decb	%bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebp
	movzwl	196(%rax,%rbp,2), %ebp
	shlq	$4, %rbp
	movl	%edi, 12(%r9,%rbp)
	decq	%rcx
.LBB3_106:                              # %.prol.loopexit149
	cmpb	%dl, %r8b
	je	.LBB3_109
# BB#107:                               # %.lr.ph.i.i28.i.new
	leal	255(%rcx), %ebx
	incb	%bl
	decb	%cl
	.p2align	4, 0x90
.LBB3_108:                              # =>This Inner Loop Header: Depth=1
	movb	%cl, 74(%rax)
	movzbl	%cl, %ecx
	movzwl	196(%rax,%rcx,2), %ebp
	shlq	$4, %rbp
	movl	%edi, 12(%r9,%rbp)
	addb	$-2, %bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebp
	movzwl	196(%rax,%rbp,2), %esi
	shlq	$4, %rsi
	movl	%edi, 12(%r9,%rsi)
	addb	$-2, %cl
	cmpb	%dl, %bpl
	ja	.LBB3_108
.LBB3_109:                              # %removevars.exit.i31.i
	cmpb	$0, 13(%r14)
	je	.LBB3_111
# BB#110:
	movl	$35, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	callq	luaK_codeABC
.LBB3_111:                              # %leaveblock.exit32.i
	movzbl	74(%r15), %eax
	movl	%eax, 60(%r15)
	movl	8(%r14), %esi
	movq	%r15, %rdi
	callq	luaK_patchtohere
	movq	48(%r13), %rbx
	movq	%r15, %rdi
	callq	luaK_jump
	movq	%rbx, %rdi
	movl	%eax, %esi
	jmp	.LBB3_121
.LBB3_49:
	movq	48(%r13), %r14
	movq	%r13, %rdi
	callq	luaX_next
	movl	16(%r13), %eax
	xorl	%esi, %esi
	leal	-260(%rax), %ecx
	cmpl	$27, %ecx
	ja	.LBB3_130
# BB#50:
	movl	$134283271, %edx        # imm = 0x8010007
	btl	%ecx, %edx
	jae	.LBB3_130
# BB#51:
	xorl	%ebp, %ebp
.LBB3_52:                               # %retstat.exit
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	luaK_ret
	movl	$1, %ebp
	jmp	.LBB3_174
.LBB3_53:
	movq	48(%r13), %rbx
	leaq	24(%rsp), %rsi
	movq	%r13, %rdi
	callq	primaryexp
	cmpl	$13, 24(%rsp)
	jne	.LBB3_99
# BB#54:
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	movslq	32(%rsp), %rcx
	movl	$-8372225, %edx         # imm = 0xFF803FFF
	andl	(%rax,%rcx,4), %edx
	orl	$16384, %edx            # imm = 0x4000
	movl	%edx, (%rax,%rcx,4)
	jmp	.LBB3_173
.LBB3_55:
	movq	48(%r13), %r14
	movq	%r13, %rdi
	callq	luaX_next
	movq	%r14, %rdi
	callq	luaK_getlabel
	movl	%eax, %r12d
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	subexpr
	cmpl	$1, 16(%rsp)
	jne	.LBB3_57
# BB#56:
	movl	$3, 16(%rsp)
.LBB3_57:                               # %cond.exit.i
	movq	48(%r13), %rdi
	leaq	16(%rsp), %rbx
	movq	%rbx, %rsi
	callq	luaK_goiftrue
	movl	36(%rsp), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	$-1, 24(%rsp)
	movb	$1, 30(%rsp)
	movb	74(%r14), %al
	movb	%al, 28(%rsp)
	movb	$0, 29(%rsp)
	movq	40(%r14), %rax
	movq	%rax, 16(%rsp)
	movq	%rbx, 40(%r14)
	cmpl	$259, 16(%r13)          # imm = 0x103
	je	.LBB3_59
# BB#58:
	movq	56(%r13), %rbx
	movl	$259, %esi              # imm = 0x103
	movq	%r13, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB3_59:                               # %checknext.exit.i
	movq	%r13, %rdi
	callq	luaX_next
	movq	%r13, %rdi
	callq	block
	movq	%r14, %rdi
	callq	luaK_jump
	movq	%r14, %rdi
	movl	%eax, %esi
	movl	%r12d, %edx
	callq	luaK_patchlist
	movl	$262, %esi              # imm = 0x106
	movl	$277, %edx              # imm = 0x115
	movq	%r13, %rdi
	movl	%ebp, %ecx
	callq	check_match
	movq	40(%r14), %r15
	movq	(%r15), %rax
	movq	%rax, 40(%r14)
	movq	24(%r14), %rax
	movzbl	12(%r15), %edx
	movq	48(%rax), %rax
	movzbl	74(%rax), %ecx
	cmpb	%dl, %cl
	jbe	.LBB3_65
# BB#60:                                # %.lr.ph.i.i.i
	movq	(%rax), %rsi
	movq	48(%rsi), %r9
	movl	48(%rax), %edi
	movl	%ecx, %ebx
	subb	%dl, %bl
	movl	%ecx, %r8d
	decb	%r8b
	testb	$1, %bl
	je	.LBB3_62
# BB#61:
	movl	%ecx, %ebx
	decb	%bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebx
	movzwl	196(%rax,%rbx,2), %ebx
	shlq	$4, %rbx
	movl	%edi, 12(%r9,%rbx)
	decq	%rcx
.LBB3_62:                               # %.prol.loopexit
	cmpb	%dl, %r8b
	je	.LBB3_65
# BB#63:                                # %.lr.ph.i.i.i.new
	leal	255(%rcx), %ebx
	incb	%bl
	decb	%cl
	.p2align	4, 0x90
.LBB3_64:                               # =>This Inner Loop Header: Depth=1
	movb	%cl, 74(%rax)
	movzbl	%cl, %ecx
	movzwl	196(%rax,%rcx,2), %ebp
	shlq	$4, %rbp
	movl	%edi, 12(%r9,%rbp)
	addb	$-2, %bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebp
	movzwl	196(%rax,%rbp,2), %esi
	shlq	$4, %rsi
	movl	%edi, 12(%r9,%rsi)
	addb	$-2, %cl
	cmpb	%dl, %bpl
	ja	.LBB3_64
.LBB3_65:                               # %removevars.exit.i.i
	cmpb	$0, 13(%r15)
	je	.LBB3_67
# BB#66:
	movl	$35, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	callq	luaK_codeABC
.LBB3_67:                               # %whilestat.exit
	movzbl	74(%r14), %eax
	movl	%eax, 60(%r14)
	movl	8(%r15), %esi
	movq	%r14, %rdi
	callq	luaK_patchtohere
	movq	%r14, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	jmp	.LBB3_172
.LBB3_72:
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	48(%r13), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	60(%rax), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movl	$.L.str.22, %esi
	movl	$15, %edx
	movq	%r13, %rdi
	callq	luaX_newstring
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	new_localvar
	movl	$.L.str.23, %esi
	movl	$11, %edx
	movq	%r13, %rdi
	callq	luaX_newstring
	movl	$1, %r15d
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	new_localvar
	movl	$.L.str.24, %esi
	movl	$13, %edx
	movq	%r13, %rdi
	callq	luaX_newstring
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	new_localvar
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	new_localvar
	movl	16(%r13), %eax
	cmpl	$44, %eax
	jne	.LBB3_78
# BB#73:                                # %.lr.ph74
	movl	$4, %ebx
	.p2align	4, 0x90
.LBB3_74:                               # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	callq	luaX_next
	cmpl	$285, 16(%r13)          # imm = 0x11D
	je	.LBB3_76
# BB#75:                                #   in Loop: Header=BB3_74 Depth=1
	movq	56(%r13), %rbp
	movl	$285, %esi              # imm = 0x11D
	movq	%r13, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB3_76:                               # %str_checkname.exit.i.i
                                        #   in Loop: Header=BB3_74 Depth=1
	movq	24(%r13), %rbp
	movq	%r13, %rdi
	callq	luaX_next
	leal	1(%rbx), %r15d
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movl	%ebx, %edx
	callq	new_localvar
	movl	16(%r13), %eax
	cmpl	$44, %eax
	movl	%r15d, %ebx
	je	.LBB3_74
# BB#77:                                # %._crit_edge.loopexit
	addl	$-3, %r15d
.LBB3_78:                               # %._crit_edge
	cmpl	$267, %eax              # imm = 0x10B
	je	.LBB3_80
# BB#79:
	movq	56(%r13), %rbx
	movl	$267, %esi              # imm = 0x10B
	movq	%r13, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB3_80:                               # %checknext.exit.i.i
	movq	%r13, %rdi
	callq	luaX_next
	movl	4(%r13), %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	subexpr
	movl	$1, %ebp
	cmpl	$44, 16(%r13)
	jne	.LBB3_83
# BB#81:                                # %.lr.ph.preheader
	movl	$1, %ebp
	leaq	16(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_82:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	callq	luaX_next
	movq	48(%r13), %rdi
	movq	%rbx, %rsi
	callq	luaK_exp2nextreg
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	subexpr
	incl	%ebp
	cmpl	$44, 16(%r13)
	je	.LBB3_82
.LBB3_83:                               # %explist1.exit
	movq	48(%r13), %r12
	movl	$3, %ebx
	subl	%ebp, %ebx
	movl	16(%rsp), %eax
	leal	-13(%rax), %ecx
	cmpl	$2, %ecx
	jae	.LBB3_140
# BB#84:
	xorl	%eax, %eax
	incl	%ebx
	cmovsl	%eax, %ebx
	leaq	16(%rsp), %rsi
	movq	%r12, %rdi
	movl	%ebx, %edx
	callq	luaK_setreturns
	cmpl	$2, %ebx
	movl	12(%rsp), %ebp          # 4-byte Reload
	jl	.LBB3_144
# BB#85:
	decl	%ebx
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	luaK_reserveregs
	jmp	.LBB3_144
.LBB3_86:                               # %funcstat.exit.loopexit
	xorl	%edx, %edx
.LBB3_87:                               # %funcstat.exit
	leaq	48(%rsp), %rbx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movl	%ebp, %ecx
	callq	body
	movq	48(%r13), %rdi
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdx
	callq	luaK_storevar
	movq	48(%r13), %rdi
	movl	%ebp, %esi
	callq	luaK_fixline
	jmp	.LBB3_173
.LBB3_88:
	leaq	48(%rsp), %rsi
	movq	%rbp, %rdi
	movl	%ebx, %edx
	callq	luaK_concat
.LBB3_89:                               # %ifstat.exit
	movl	48(%rsp), %esi
	movq	%rbp, %rdi
	callq	luaK_patchtohere
	movl	$262, %esi              # imm = 0x106
	movl	$266, %edx              # imm = 0x10A
	movq	%r13, %rdi
	movl	12(%rsp), %ecx          # 4-byte Reload
	callq	check_match
	jmp	.LBB3_173
.LBB3_90:
	movl	$1, %ebp
	jmp	.LBB3_92
	.p2align	4, 0x90
.LBB3_91:                               # %testnext.exit.i
                                        #   in Loop: Header=BB3_92 Depth=1
	movq	%r13, %rdi
	callq	luaX_next
	movl	16(%r13), %eax
	incl	%ebp
.LBB3_92:                               # =>This Inner Loop Header: Depth=1
	leal	-1(%rbp), %r12d
	cmpl	$285, %eax              # imm = 0x11D
	je	.LBB3_94
# BB#93:                                #   in Loop: Header=BB3_92 Depth=1
	movq	56(%r13), %rbx
	movl	$285, %esi              # imm = 0x11D
	movq	%r13, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB3_94:                               # %str_checkname.exit.i46
                                        #   in Loop: Header=BB3_92 Depth=1
	movq	24(%r13), %rbx
	movq	%r13, %rdi
	callq	luaX_next
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movl	%r12d, %edx
	callq	new_localvar
	movl	16(%r13), %eax
	cmpl	$44, %eax
	je	.LBB3_91
# BB#95:                                # %str_checkname.exit.i46
	cmpl	$61, %eax
	jne	.LBB3_145
# BB#96:
	movq	%r13, %rdi
	callq	luaX_next
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	subexpr
	movl	$1, %ebx
	cmpl	$44, 16(%r13)
	jne	.LBB3_146
# BB#97:                                # %.lr.ph78
	movl	$1, %ebx
	leaq	16(%rsp), %r14
	.p2align	4, 0x90
.LBB3_98:                               # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	callq	luaX_next
	movq	48(%r13), %rdi
	movq	%r14, %rsi
	callq	luaK_exp2nextreg
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	subexpr
	incl	%ebx
	cmpl	$44, 16(%r13)
	je	.LBB3_98
	jmp	.LBB3_146
.LBB3_99:
	movq	$0, 16(%rsp)
	leaq	16(%rsp), %rsi
	movl	$1, %edx
	movq	%r13, %rdi
	callq	assignment
	jmp	.LBB3_173
.LBB3_112:
	movl	%ebp, %r12d
	movq	40(%r15), %r14
	movq	(%r14), %rax
	movq	%rax, 40(%r15)
	movq	24(%r15), %rax
	movzbl	12(%r14), %edx
	movq	48(%rax), %rax
	movzbl	74(%rax), %ecx
	cmpb	%dl, %cl
	jbe	.LBB3_118
# BB#113:                               # %.lr.ph.i.i.i38
	movq	(%rax), %rsi
	movq	48(%rsi), %r9
	movl	48(%rax), %edi
	movl	%ecx, %ebx
	subb	%dl, %bl
	movl	%ecx, %r8d
	decb	%r8b
	testb	$1, %bl
	je	.LBB3_115
# BB#114:
	movl	%ecx, %ebx
	decb	%bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebx
	movzwl	196(%rax,%rbx,2), %ebx
	shlq	$4, %rbx
	movl	%edi, 12(%r9,%rbx)
	decq	%rcx
.LBB3_115:                              # %.prol.loopexit145
	cmpb	%dl, %r8b
	je	.LBB3_118
# BB#116:                               # %.lr.ph.i.i.i38.new
	leal	255(%rcx), %ebx
	incb	%bl
	decb	%cl
	.p2align	4, 0x90
.LBB3_117:                              # =>This Inner Loop Header: Depth=1
	movb	%cl, 74(%rax)
	movzbl	%cl, %ecx
	movzwl	196(%rax,%rcx,2), %ebp
	shlq	$4, %rbp
	movl	%edi, 12(%r9,%rbp)
	addb	$-2, %bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebp
	movzwl	196(%rax,%rbp,2), %esi
	shlq	$4, %rsi
	movl	%edi, 12(%r9,%rsi)
	addb	$-2, %cl
	cmpb	%dl, %bpl
	ja	.LBB3_117
.LBB3_118:                              # %removevars.exit.i.i41
	cmpb	$0, 13(%r14)
	je	.LBB3_120
# BB#119:
	movl	$35, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	callq	luaK_codeABC
.LBB3_120:                              # %leaveblock.exit.i
	movzbl	74(%r15), %eax
	movl	%eax, 60(%r15)
	movl	8(%r14), %esi
	movq	%r15, %rdi
	callq	luaK_patchtohere
	movq	48(%r13), %rdi
	movl	%r12d, %esi
.LBB3_121:
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	luaK_patchlist
	movq	40(%r15), %r14
	movq	(%r14), %rax
	movq	%rax, 40(%r15)
	movq	24(%r15), %rax
	movzbl	12(%r14), %edx
	movq	48(%rax), %rax
	movzbl	74(%rax), %ecx
	cmpb	%dl, %cl
	jbe	.LBB3_127
# BB#122:                               # %.lr.ph.i.i36.i
	movq	(%rax), %rsi
	movq	48(%rsi), %r9
	movl	48(%rax), %edi
	movl	%ecx, %ebx
	subb	%dl, %bl
	movl	%ecx, %r8d
	decb	%r8b
	testb	$1, %bl
	je	.LBB3_124
# BB#123:
	movl	%ecx, %ebx
	decb	%bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebp
	movzwl	196(%rax,%rbp,2), %ebp
	shlq	$4, %rbp
	movl	%edi, 12(%r9,%rbp)
	decq	%rcx
.LBB3_124:                              # %.prol.loopexit141
	cmpb	%dl, %r8b
	je	.LBB3_127
# BB#125:                               # %.lr.ph.i.i36.i.new
	leal	255(%rcx), %ebp
	incb	%bpl
	decb	%cl
	.p2align	4, 0x90
.LBB3_126:                              # =>This Inner Loop Header: Depth=1
	movb	%cl, 74(%rax)
	movzbl	%cl, %ecx
	movzwl	196(%rax,%rcx,2), %ebx
	shlq	$4, %rbx
	movl	%edi, 12(%r9,%rbx)
	addb	$-2, %bpl
	movb	%bpl, 74(%rax)
	movzbl	%bpl, %ebx
	movzwl	196(%rax,%rbx,2), %esi
	shlq	$4, %rsi
	movl	%edi, 12(%r9,%rsi)
	addb	$-2, %cl
	cmpb	%dl, %bl
	ja	.LBB3_126
.LBB3_127:                              # %removevars.exit.i39.i
	cmpb	$0, 13(%r14)
	je	.LBB3_129
# BB#128:
	movl	$35, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	callq	luaK_codeABC
.LBB3_129:                              # %repeatstat.exit
	movzbl	74(%r15), %eax
	movl	%eax, 60(%r15)
	movl	8(%r14), %esi
	movq	%r15, %rdi
	jmp	.LBB3_172
.LBB3_130:
	cmpl	$59, %eax
	movl	$0, %ebp
	je	.LBB3_52
# BB#131:
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	subexpr
	movl	$1, %ebx
	cmpl	$44, 16(%r13)
	jne	.LBB3_134
# BB#132:                               # %.lr.ph82.preheader
	movl	$1, %ebx
	leaq	16(%rsp), %rbp
	.p2align	4, 0x90
.LBB3_133:                              # %.lr.ph82
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	callq	luaX_next
	movq	48(%r13), %rdi
	movq	%rbp, %rsi
	callq	luaK_exp2nextreg
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	subexpr
	incl	%ebx
	cmpl	$44, 16(%r13)
	je	.LBB3_133
.LBB3_134:                              # %explist1.exit.i59
	movl	16(%rsp), %eax
	addl	$-13, %eax
	leaq	16(%rsp), %rsi
	cmpl	$1, %eax
	ja	.LBB3_175
# BB#135:
	movl	$-1, %ebp
	movl	$-1, %edx
	movq	%r14, %rdi
	callq	luaK_setreturns
	cmpl	$1, %ebx
	jne	.LBB3_138
# BB#136:
	cmpl	$13, 16(%rsp)
	jne	.LBB3_138
# BB#137:
	movq	(%r14), %rax
	movq	24(%rax), %rax
	movslq	24(%rsp), %rcx
	movl	(%rax,%rcx,4), %edx
	andl	$-64, %edx
	orl	$29, %edx
	movl	%edx, (%rax,%rcx,4)
.LBB3_138:
	movzbl	74(%r14), %esi
	jmp	.LBB3_52
.LBB3_139:
	movl	$.L.str.18, %esi
	movq	%r13, %rdi
	callq	luaX_syntaxerror
	jmp	.LBB3_163
.LBB3_140:                              # %explist1.exit
	testl	%eax, %eax
	movl	12(%rsp), %ebp          # 4-byte Reload
	je	.LBB3_142
# BB#141:
	leaq	16(%rsp), %rsi
	movq	%r12, %rdi
	callq	luaK_exp2nextreg
.LBB3_142:
	testl	%ebx, %ebx
	jle	.LBB3_144
# BB#143:
	movl	60(%r12), %ebp
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	luaK_reserveregs
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebx, %edx
	callq	luaK_nil
.LBB3_144:                              # %forlist.exit.i
	movl	$3, %esi
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	luaK_checkstack
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	movl	76(%rsp), %esi          # 4-byte Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	movl	%r15d, %ecx
	jmp	.LBB3_162
.LBB3_145:
	movl	$0, 16(%rsp)
	xorl	%ebx, %ebx
.LBB3_146:                              # %explist1.exit.i
	movq	48(%r13), %r14
	movl	%ebp, %r15d
	subl	%ebx, %r15d
	movl	16(%rsp), %eax
	leal	-13(%rax), %ecx
	cmpl	$2, %ecx
	jae	.LBB3_149
# BB#147:
	xorl	%eax, %eax
	incl	%r15d
	cmovsl	%eax, %r15d
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	movl	%r15d, %edx
	callq	luaK_setreturns
	cmpl	$2, %r15d
	jl	.LBB3_153
# BB#148:
	decl	%r15d
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	luaK_reserveregs
	jmp	.LBB3_153
.LBB3_149:                              # %explist1.exit.i
	testl	%eax, %eax
	je	.LBB3_151
# BB#150:
	leaq	16(%rsp), %rsi
	movq	%r14, %rdi
	callq	luaK_exp2nextreg
.LBB3_151:
	testl	%r15d, %r15d
	jle	.LBB3_153
# BB#152:
	movl	60(%r14), %ebx
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	luaK_reserveregs
	movq	%r14, %rdi
	movl	%ebx, %esi
	movl	%r15d, %edx
	callq	luaK_nil
.LBB3_153:                              # %.lr.ph.i.i53
	movq	48(%r13), %rsi
	movzbl	74(%rsi), %r8d
	addl	%ebp, %r8d
	movb	%r8b, 74(%rsi)
	movl	48(%rsi), %eax
	movq	(%rsi), %rcx
	movq	48(%rcx), %rcx
	testb	$3, %bpl
	je	.LBB3_157
# BB#154:                               # %.prol.preheader152
	movslq	%ebp, %rdi
	negq	%rdi
	movzbl	%r8b, %ebx
	leaq	196(%rsi,%rbx,2), %rbx
	andb	$3, %bpl
	movzbl	%bpl, %ebp
	negl	%ebp
	.p2align	4, 0x90
.LBB3_155:                              # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx,%rdi,2), %edx
	shlq	$4, %rdx
	movl	%eax, 8(%rcx,%rdx)
	incq	%rdi
	incl	%ebp
	jne	.LBB3_155
# BB#156:                               # %.prol.loopexit153.unr-lcssa
	negq	%rdi
	cmpl	$3, %r12d
	jae	.LBB3_158
	jmp	.LBB3_173
.LBB3_157:
	movslq	%ebp, %rdi
	cmpl	$3, %r12d
	jb	.LBB3_173
.LBB3_158:                              # %.lr.ph.i.i53.new
	movl	%edi, %ebp
	negl	%ebp
	movl	$3, %edx
	subq	%rdi, %rdx
	leaq	(%rsi,%rdx,2), %rdx
	movzbl	%r8b, %esi
	leaq	196(%rdx,%rsi,2), %rdx
	.p2align	4, 0x90
.LBB3_159:                              # =>This Inner Loop Header: Depth=1
	movzwl	-6(%rdx), %esi
	shlq	$4, %rsi
	movl	%eax, 8(%rcx,%rsi)
	movzwl	-4(%rdx), %esi
	shlq	$4, %rsi
	movl	%eax, 8(%rcx,%rsi)
	movzwl	-2(%rdx), %esi
	shlq	$4, %rsi
	movl	%eax, 8(%rcx,%rsi)
	movzwl	(%rdx), %esi
	shlq	$4, %rsi
	movl	%eax, 8(%rcx,%rsi)
	addq	$8, %rdx
	addl	$4, %ebp
	jne	.LBB3_159
	jmp	.LBB3_173
.LBB3_160:
	movl	60(%rbp), %ebx
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	%rbp, %rdi
	callq	luaK_numberK
	movl	$1, %esi
	movq	%rbp, %rdi
	movl	%ebx, %edx
	movl	%eax, %ecx
	callq	luaK_codeABx
	movl	$1, %esi
	movq	%rbp, %rdi
	callq	luaK_reserveregs
.LBB3_161:                              # %fornum.exit.i
	movl	$1, %ecx
	movl	$1, %r8d
	movq	%r13, %rdi
	movl	%r15d, %esi
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %edx
.LBB3_162:
	callq	forbody
.LBB3_163:
	movl	$262, %esi              # imm = 0x106
	movl	$264, %edx              # imm = 0x108
	movq	%r13, %rdi
	movl	%ebp, %ecx
	callq	check_match
	movq	40(%r14), %r15
	movq	(%r15), %rax
	movq	%rax, 40(%r14)
	movq	24(%r14), %rax
	movzbl	12(%r15), %edx
	movq	48(%rax), %rax
	movzbl	74(%rax), %ecx
	cmpb	%dl, %cl
	jbe	.LBB3_169
# BB#164:                               # %.lr.ph.i.i.i27
	movq	(%rax), %rsi
	movq	48(%rsi), %r9
	movl	48(%rax), %edi
	movl	%ecx, %ebx
	subb	%dl, %bl
	movl	%ecx, %r8d
	decb	%r8b
	testb	$1, %bl
	je	.LBB3_166
# BB#165:
	movl	%ecx, %ebx
	decb	%bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebp
	movzwl	196(%rax,%rbp,2), %ebp
	shlq	$4, %rbp
	movl	%edi, 12(%r9,%rbp)
	decq	%rcx
.LBB3_166:                              # %.prol.loopexit137
	cmpb	%dl, %r8b
	je	.LBB3_169
# BB#167:                               # %.lr.ph.i.i.i27.new
	leal	255(%rcx), %ebp
	incb	%bpl
	decb	%cl
	.p2align	4, 0x90
.LBB3_168:                              # =>This Inner Loop Header: Depth=1
	movb	%cl, 74(%rax)
	movzbl	%cl, %ecx
	movzwl	196(%rax,%rcx,2), %ebx
	shlq	$4, %rbx
	movl	%edi, 12(%r9,%rbx)
	addb	$-2, %bpl
	movb	%bpl, 74(%rax)
	movzbl	%bpl, %ebx
	movzwl	196(%rax,%rbx,2), %esi
	shlq	$4, %rsi
	movl	%edi, 12(%r9,%rsi)
	addb	$-2, %cl
	cmpb	%dl, %bl
	ja	.LBB3_168
.LBB3_169:                              # %removevars.exit.i.i30
	cmpb	$0, 13(%r15)
	je	.LBB3_171
# BB#170:
	movl	$35, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	callq	luaK_codeABC
.LBB3_171:                              # %forstat.exit
	movzbl	74(%r14), %eax
	movl	%eax, 60(%r14)
	movl	8(%r15), %esi
	movq	%r14, %rdi
.LBB3_172:                              # %exprstat.exit
	callq	luaK_patchtohere
.LBB3_173:                              # %exprstat.exit
	xorl	%ebp, %ebp
.LBB3_174:
	movl	%ebp, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_175:
	movq	%r14, %rdi
	cmpl	$1, %ebx
	jne	.LBB3_177
# BB#176:
	callq	luaK_exp2anyreg
	movl	%eax, %esi
	movl	$1, %ebp
	jmp	.LBB3_52
.LBB3_177:
	callq	luaK_exp2nextreg
	movzbl	74(%r14), %esi
	movl	%ebx, %ebp
	jmp	.LBB3_52
.Lfunc_end3:
	.size	statement, .Lfunc_end3-statement
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_2
	.quad	.LBB3_5
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_6
	.quad	.LBB3_17
	.quad	.LBB3_22
	.quad	.LBB3_53
	.quad	.LBB3_31
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_35
	.quad	.LBB3_49
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_53
	.quad	.LBB3_55

	.text
	.p2align	4, 0x90
	.type	block,@function
block:                                  # @block
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 64
.Lcfi50:
	.cfi_offset %rbx, -48
.Lcfi51:
	.cfi_offset %r12, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	48(%rbx), %r14
	movl	$-1, 8(%rsp)
	movb	$0, 14(%rsp)
	movb	74(%r14), %al
	movb	%al, 12(%rsp)
	movb	$0, 13(%rsp)
	movq	40(%r14), %rax
	movq	%rax, (%rsp)
	movq	%rsp, %rax
	movq	%rax, 40(%r14)
	movq	56(%rbx), %rax
	movzwl	96(%rax), %ecx
	incl	%ecx
	movw	%cx, 96(%rax)
	movzwl	%cx, %eax
	cmpl	$201, %eax
	jb	.LBB4_2
# BB#1:
	movl	$.L.str.1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaX_lexerror
.LBB4_2:                                # %enterlevel.exit.i.preheader
	movl	$-260, %r15d            # imm = 0xFEFC
	movl	$134283271, %r12d       # imm = 0x8010007
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movl	16(%rbx), %eax
	addl	%r15d, %eax
	cmpl	$27, %eax
	ja	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	btl	%eax, %r12d
	jb	.LBB4_8
.LBB4_5:                                #   in Loop: Header=BB4_3 Depth=1
	movq	%rbx, %rdi
	callq	statement
	movl	%eax, %ebp
	cmpl	$59, 16(%rbx)
	jne	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	%rbx, %rdi
	callq	luaX_next
.LBB4_7:                                # %testnext.exit.i
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	48(%rbx), %rax
	movzbl	74(%rax), %ecx
	movl	%ecx, 60(%rax)
	testl	%ebp, %ebp
	je	.LBB4_3
.LBB4_8:                                # %chunk.exit
	movq	56(%rbx), %rax
	decw	96(%rax)
	movq	40(%r14), %r15
	movq	(%r15), %rax
	movq	%rax, 40(%r14)
	movq	24(%r14), %rax
	movzbl	12(%r15), %edx
	movq	48(%rax), %rax
	movzbl	74(%rax), %ecx
	cmpb	%dl, %cl
	jbe	.LBB4_14
# BB#9:                                 # %.lr.ph.i.i
	movq	(%rax), %rsi
	movq	48(%rsi), %r9
	movl	48(%rax), %edi
	movl	%ecx, %ebx
	subb	%dl, %bl
	movl	%ecx, %r8d
	decb	%r8b
	testb	$1, %bl
	je	.LBB4_11
# BB#10:
	movl	%ecx, %ebx
	decb	%bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebp
	movzwl	196(%rax,%rbp,2), %ebp
	shlq	$4, %rbp
	movl	%edi, 12(%r9,%rbp)
	decq	%rcx
.LBB4_11:                               # %.prol.loopexit
	cmpb	%dl, %r8b
	je	.LBB4_14
# BB#12:                                # %.lr.ph.i.i.new
	leal	255(%rcx), %ebp
	incb	%bpl
	decb	%cl
	.p2align	4, 0x90
.LBB4_13:                               # =>This Inner Loop Header: Depth=1
	movb	%cl, 74(%rax)
	movzbl	%cl, %ecx
	movzwl	196(%rax,%rcx,2), %ebx
	shlq	$4, %rbx
	movl	%edi, 12(%r9,%rbx)
	addb	$-2, %bpl
	movb	%bpl, 74(%rax)
	movzbl	%bpl, %ebx
	movzwl	196(%rax,%rbx,2), %esi
	shlq	$4, %rsi
	movl	%edi, 12(%r9,%rsi)
	addb	$-2, %cl
	cmpb	%dl, %bl
	ja	.LBB4_13
.LBB4_14:                               # %removevars.exit.i
	cmpb	$0, 13(%r15)
	je	.LBB4_16
# BB#15:
	movl	$35, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	callq	luaK_codeABC
.LBB4_16:                               # %leaveblock.exit
	movzbl	74(%r14), %eax
	movl	%eax, 60(%r14)
	movl	8(%r15), %esi
	movq	%r14, %rdi
	callq	luaK_patchtohere
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	block, .Lfunc_end4-block
	.cfi_endproc

	.p2align	4, 0x90
	.type	check_match,@function
check_match:                            # @check_match
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 64
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movl	%edx, %r12d
	movq	%rdi, %rbx
	cmpl	%esi, 16(%rbx)
	jne	.LBB5_1
# BB#5:                                 # %testnext.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaX_next               # TAILCALL
.LBB5_1:
	movl	4(%rbx), %ebp
	movq	56(%rbx), %r14
	movq	%rbx, %rdi
	callq	luaX_token2str
	movq	%rax, %r15
	cmpl	%r13d, %ebp
	jne	.LBB5_4
# BB#2:
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	luaO_pushfstring
	jmp	.LBB5_3
.LBB5_4:
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r15, %rdx
	movl	%r13d, %r8d
	callq	luaO_pushfstring
.LBB5_3:
	movq	%rbx, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaX_syntaxerror        # TAILCALL
.Lfunc_end5:
	.size	check_match, .Lfunc_end5-check_match
	.cfi_endproc

	.p2align	4, 0x90
	.type	subexpr,@function
subexpr:                                # @subexpr
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 96
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	56(%r13), %rax
	movzwl	96(%rax), %ecx
	incl	%ecx
	movw	%cx, 96(%rax)
	movzwl	%cx, %eax
	cmpl	$201, %eax
	jb	.LBB6_2
# BB#1:
	movl	$.L.str.1, %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	callq	luaX_lexerror
.LBB6_2:                                # %enterlevel.exit
	movl	16(%r13), %eax
	cmpl	$262, %eax              # imm = 0x106
	jle	.LBB6_6
# BB#3:                                 # %enterlevel.exit
	addl	$-263, %eax             # imm = 0xFEF9
	cmpl	$23, %eax
	ja	.LBB6_23
# BB#4:                                 # %enterlevel.exit
	movl	$1, %ebx
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_5:
	movl	$-1, 16(%r12)
	movl	$-1, 20(%r12)
	movl	$3, (%r12)
	movl	$0, 8(%r12)
	jmp	.LBB6_19
.LBB6_6:                                # %enterlevel.exit
	cmpl	$35, %eax
	je	.LBB6_20
# BB#7:                                 # %enterlevel.exit
	cmpl	$45, %eax
	je	.LBB6_21
# BB#8:                                 # %enterlevel.exit
	cmpl	$123, %eax
	jne	.LBB6_23
# BB#9:
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	constructor
	jmp	.LBB6_24
.LBB6_10:
	movq	%r13, %rdi
	callq	luaX_next
	movl	4(%r13), %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	body
	jmp	.LBB6_24
.LBB6_11:
	movl	$-1, 16(%r12)
	movl	$-1, 20(%r12)
	movl	$1, (%r12)
	movl	$0, 8(%r12)
	jmp	.LBB6_19
.LBB6_12:
	movl	$-1, 16(%r12)
	movl	$-1, 20(%r12)
	movl	$2, (%r12)
	movl	$0, 8(%r12)
	jmp	.LBB6_19
.LBB6_13:
	movq	48(%r13), %rbx
	movq	(%rbx), %rax
	movb	114(%rax), %cl
	testb	%cl, %cl
	jne	.LBB6_15
# BB#14:
	movl	$.L.str.2, %esi
	movq	%r13, %rdi
	callq	luaX_syntaxerror
	movq	(%rbx), %rax
	movb	114(%rax), %cl
.LBB6_15:
	andb	$-5, %cl
	movb	%cl, 114(%rax)
	movl	$37, %esi
	xorl	%edx, %edx
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	callq	luaK_codeABC
	movl	$-1, 16(%r12)
	movl	$-1, 20(%r12)
	movl	$14, (%r12)
	jmp	.LBB6_18
.LBB6_16:
	movl	$-1, 16(%r12)
	movl	$-1, 20(%r12)
	movl	$5, (%r12)
	movl	$0, 8(%r12)
	movq	24(%r13), %rax
	movq	%rax, 8(%r12)
	jmp	.LBB6_19
.LBB6_17:
	movq	24(%r13), %rsi
	movq	48(%r13), %rdi
	callq	luaK_stringK
	movl	$-1, 16(%r12)
	movl	$-1, 20(%r12)
	movl	$4, (%r12)
.LBB6_18:
	movl	%eax, 8(%r12)
.LBB6_19:
	movq	%r13, %rdi
	callq	luaX_next
	jmp	.LBB6_24
.LBB6_20:
	movl	$2, %ebx
	jmp	.LBB6_22
.LBB6_21:
	xorl	%ebx, %ebx
.LBB6_22:
	movq	%r13, %rdi
	callq	luaX_next
	movl	$8, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	subexpr
	movq	48(%r13), %rdi
	movl	%ebx, %esi
	movq	%r12, %rdx
	callq	luaK_prefix
	jmp	.LBB6_24
.LBB6_23:
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	primaryexp
.LBB6_24:                               # %simpleexp.exit
	movl	16(%r13), %edx
	movl	$15, %eax
	leal	-37(%rdx), %ecx
	cmpl	$57, %ecx
	ja	.LBB6_27
# BB#25:                                # %simpleexp.exit
	xorl	%ebp, %ebp
	jmpq	*.LJTI6_1(,%rcx,8)
.LBB6_26:
	movl	$4, %ebp
	jmp	.LBB6_42
.LBB6_27:                               # %simpleexp.exit
	addl	$-257, %edx             # imm = 0xFEFF
	cmpl	$26, %edx
	ja	.LBB6_47
# BB#28:                                # %simpleexp.exit
	jmpq	*.LJTI6_2(,%rdx,8)
.LBB6_29:
	movl	$13, %ebp
	jmp	.LBB6_42
.LBB6_30:
	movl	$2, %ebp
	jmp	.LBB6_42
.LBB6_31:
	movl	$1, %ebp
	jmp	.LBB6_42
.LBB6_32:
	movl	$3, %ebp
	jmp	.LBB6_42
.LBB6_33:
	movl	$9, %ebp
	jmp	.LBB6_42
.LBB6_34:
	movl	$11, %ebp
	jmp	.LBB6_42
.LBB6_35:
	movl	$5, %ebp
	jmp	.LBB6_42
.LBB6_36:
	movl	$14, %ebp
	jmp	.LBB6_42
.LBB6_37:
	movl	$6, %ebp
	jmp	.LBB6_42
.LBB6_38:
	movl	$8, %ebp
	jmp	.LBB6_42
.LBB6_39:
	movl	$12, %ebp
	jmp	.LBB6_42
.LBB6_40:
	movl	$10, %ebp
	jmp	.LBB6_42
.LBB6_41:
	movl	$7, %ebp
.LBB6_42:                               # %.lr.ph
	leaq	16(%rsp), %r14
	.p2align	4, 0x90
.LBB6_43:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ebx
	movzbl	priority(%rbx,%rbx), %eax
	cmpl	12(%rsp), %eax          # 4-byte Folded Reload
	jbe	.LBB6_46
# BB#44:                                # %getbinopr.exit
                                        #   in Loop: Header=BB6_43 Depth=1
	movq	%r13, %rdi
	callq	luaX_next
	movq	48(%r13), %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	luaK_infix
	movzbl	priority+1(%rbx,%rbx), %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	callq	subexpr
	movl	%eax, %r15d
	movq	48(%r13), %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	luaK_posfix
	cmpl	$15, %r15d
	movl	%r15d, %ebp
	jne	.LBB6_43
# BB#45:
	movl	$15, %eax
	jmp	.LBB6_47
.LBB6_46:
	movl	%ebp, %eax
.LBB6_47:                               # %.critedge
	movq	56(%r13), %rcx
	decw	96(%rcx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	subexpr, .Lfunc_end6-subexpr
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_5
	.quad	.LBB6_23
	.quad	.LBB6_10
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_11
	.quad	.LBB6_22
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_12
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_13
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_23
	.quad	.LBB6_16
	.quad	.LBB6_23
	.quad	.LBB6_17
.LJTI6_1:
	.quad	.LBB6_26
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_30
	.quad	.LBB6_42
	.quad	.LBB6_47
	.quad	.LBB6_31
	.quad	.LBB6_47
	.quad	.LBB6_32
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_33
	.quad	.LBB6_47
	.quad	.LBB6_34
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_35
.LJTI6_2:
	.quad	.LBB6_29
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_36
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_47
	.quad	.LBB6_37
	.quad	.LBB6_47
	.quad	.LBB6_38
	.quad	.LBB6_39
	.quad	.LBB6_40
	.quad	.LBB6_41

	.text
	.p2align	4, 0x90
	.type	constructor,@function
constructor:                            # @constructor
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi87:
	.cfi_def_cfa_offset 112
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	48(%rbp), %r13
	movl	4(%rbp), %r15d
	movl	$10, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	callq	luaK_codeABC
	movl	%eax, %r14d
	movl	$0, 48(%rsp)
	movl	$0, 40(%rsp)
	movl	$0, 44(%rsp)
	movq	%rbx, 32(%rsp)
	movl	$-1, 16(%rbx)
	movl	$-1, 20(%rbx)
	movl	$11, (%rbx)
	movl	%r14d, 8(%rbx)
	movl	$-1, 24(%rsp)
	movl	$-1, 28(%rsp)
	movl	$0, 8(%rsp)
	movl	$0, 16(%rsp)
	movq	48(%rbp), %rdi
	movq	%rbx, %rsi
	callq	luaK_exp2nextreg
	cmpl	$123, 16(%rbp)
	je	.LBB7_2
# BB#1:
	movq	56(%rbp), %rbx
	movl	$123, %esi
	movq	%rbp, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB7_2:                                # %checknext.exit
	movq	%rbp, %rdi
	callq	luaX_next
	cmpl	$125, 16(%rbp)
	je	.LBB7_21
# BB#3:                                 # %.lr.ph
	leaq	8(%rsp), %r12
	xorl	%eax, %eax
	testl	%eax, %eax
	jne	.LBB7_5
	jmp	.LBB7_7
	.p2align	4, 0x90
.LBB7_20:                               # %.critedge.backedge._crit_edge
                                        #   in Loop: Header=BB7_7 Depth=1
	movl	8(%rsp), %eax
	testl	%eax, %eax
	je	.LBB7_7
.LBB7_5:
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	luaK_exp2nextreg
	movl	$0, 8(%rsp)
	cmpl	$50, 48(%rsp)
	jne	.LBB7_7
# BB#6:
	movq	32(%rsp), %rax
	movl	8(%rax), %esi
	movl	44(%rsp), %edx
	movl	$50, %ecx
	movq	%r13, %rdi
	callq	luaK_setlist
	movl	$0, 48(%rsp)
.LBB7_7:                                # %closelistfield.exit
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rbp), %eax
	cmpl	$91, %eax
	je	.LBB7_13
# BB#8:                                 # %closelistfield.exit
                                        #   in Loop: Header=BB7_7 Depth=1
	cmpl	$285, %eax              # imm = 0x11D
	jne	.LBB7_10
# BB#9:                                 #   in Loop: Header=BB7_7 Depth=1
	movq	%rbp, %rdi
	callq	luaX_lookahead
	cmpl	$61, 32(%rbp)
	jne	.LBB7_10
.LBB7_13:                               #   in Loop: Header=BB7_7 Depth=1
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	recfield
	jmp	.LBB7_17
	.p2align	4, 0x90
.LBB7_10:                               #   in Loop: Header=BB7_7 Depth=1
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%r12, %rsi
	callq	subexpr
	movl	44(%rsp), %eax
	cmpl	$2147483646, %eax       # imm = 0x7FFFFFFE
	jl	.LBB7_16
# BB#11:                                #   in Loop: Header=BB7_7 Depth=1
	movq	48(%rbp), %rbx
	movq	(%rbx), %rax
	movq	32(%rbx), %rdi
	movl	96(%rax), %edx
	testl	%edx, %edx
	je	.LBB7_12
# BB#14:                                #   in Loop: Header=BB7_7 Depth=1
	movl	$.L.str.5, %esi
	movl	$2147483645, %ecx       # imm = 0x7FFFFFFD
	movl	$.L.str.3, %r8d
	xorl	%eax, %eax
	callq	luaO_pushfstring
	jmp	.LBB7_15
.LBB7_12:                               #   in Loop: Header=BB7_7 Depth=1
	movl	$.L.str.4, %esi
	movl	$2147483645, %edx       # imm = 0x7FFFFFFD
	movl	$.L.str.3, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
.LBB7_15:                               # %errorlimit.exit.i36
                                        #   in Loop: Header=BB7_7 Depth=1
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	luaX_lexerror
	movl	44(%rsp), %eax
.LBB7_16:                               # %listfield.exit37
                                        #   in Loop: Header=BB7_7 Depth=1
	incl	%eax
	movl	%eax, 44(%rsp)
	incl	48(%rsp)
.LBB7_17:                               #   in Loop: Header=BB7_7 Depth=1
	movl	16(%rbp), %eax
	cmpl	$59, %eax
	je	.LBB7_19
# BB#18:                                #   in Loop: Header=BB7_7 Depth=1
	cmpl	$44, %eax
	jne	.LBB7_21
.LBB7_19:                               # %testnext.exit35
                                        #   in Loop: Header=BB7_7 Depth=1
	movq	%rbp, %rdi
	callq	luaX_next
	cmpl	$125, 16(%rbp)
	jne	.LBB7_20
.LBB7_21:                               # %testnext.exit.thread
	movl	$125, %esi
	movl	$123, %edx
	movq	%rbp, %rdi
	movl	%r15d, %ecx
	callq	check_match
	movl	48(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB7_26
# BB#22:
	movl	8(%rsp), %eax
	leal	-13(%rax), %edx
	cmpl	$2, %edx
	jae	.LBB7_23
# BB#27:
	leaq	8(%rsp), %rsi
	movl	$-1, %edx
	movq	%r13, %rdi
	callq	luaK_setreturns
	movq	32(%rsp), %rax
	movl	8(%rax), %esi
	movl	44(%rsp), %edx
	movl	$-1, %ecx
	movq	%r13, %rdi
	callq	luaK_setlist
	decl	44(%rsp)
	jmp	.LBB7_26
.LBB7_23:
	testl	%eax, %eax
	je	.LBB7_25
# BB#24:
	leaq	8(%rsp), %rsi
	movq	%r13, %rdi
	callq	luaK_exp2nextreg
	movl	48(%rsp), %ecx
.LBB7_25:
	movq	32(%rsp), %rax
	movl	8(%rax), %esi
	movl	44(%rsp), %edx
	movq	%r13, %rdi
	callq	luaK_setlist
.LBB7_26:                               # %lastlistfield.exit
	movq	(%r13), %rax
	movq	24(%rax), %rax
	movslq	%r14d, %r14
	movl	$8388607, %ebp          # imm = 0x7FFFFF
	andl	(%rax,%r14,4), %ebp
	movl	44(%rsp), %edi
	callq	luaO_int2fb
	movl	%eax, %ebx
	shll	$23, %ebx
	orl	%ebp, %ebx
	movq	(%r13), %rax
	movq	24(%rax), %rax
	movl	%ebx, (%rax,%r14,4)
	andl	$-8372225, %ebx         # imm = 0xFF803FFF
	movl	40(%rsp), %edi
	callq	luaO_int2fb
	shll	$14, %eax
	andl	$8372224, %eax          # imm = 0x7FC000
	orl	%ebx, %eax
	movq	(%r13), %rcx
	movq	24(%rcx), %rcx
	movl	%eax, (%rcx,%r14,4)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	constructor, .Lfunc_end7-constructor
	.cfi_endproc

	.p2align	4, 0x90
	.type	body,@function
body:                                   # @body
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 56
	subq	$632, %rsp              # imm = 0x278
.Lcfi100:
	.cfi_def_cfa_offset 688
.Lcfi101:
	.cfi_offset %rbx, -56
.Lcfi102:
	.cfi_offset %r12, -48
.Lcfi103:
	.cfi_offset %r13, -40
.Lcfi104:
	.cfi_offset %r14, -32
.Lcfi105:
	.cfi_offset %r15, -24
.Lcfi106:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebx
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	32(%rsp), %rsi
	callq	open_func
	movq	32(%rsp), %rax
	movl	%ebx, 96(%rax)
	cmpl	$40, 16(%r15)
	je	.LBB8_2
# BB#1:
	movq	56(%r15), %r12
	movl	$40, %esi
	movq	%r15, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB8_2:                                # %checknext.exit
	movq	%r15, %rdi
	callq	luaX_next
	testl	%ebp, %ebp
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	je	.LBB8_3
# BB#4:                                 # %adjustlocalvars.exit.loopexit24
	movl	$.L.str.6, %esi
	movl	$4, %edx
	movq	%r15, %rdi
	callq	luaX_newstring
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	new_localvar
	leaq	48(%r15), %rbx
	movq	48(%r15), %rsi
	movzbl	74(%rsi), %eax
	incl	%eax
	movb	%al, 74(%rsi)
	movl	48(%rsi), %ecx
	movq	(%rsi), %r14
	movq	48(%r14), %rdx
	movzbl	%al, %eax
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movzwl	194(%rsi,%rax,2), %eax
	shlq	$4, %rax
	movl	%ecx, 8(%rdx,%rax)
	jmp	.LBB8_5
.LBB8_3:                                # %checknext.exit.adjustlocalvars.exit_crit_edge
	leaq	48(%r15), %rbx
	movq	48(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%rax), %r14
.LBB8_5:                                # %adjustlocalvars.exit
	movb	$0, 114(%r14)
	movl	16(%r15), %ecx
	xorl	%r10d, %r10d
	cmpl	$41, %ecx
	movl	$0, %ebp
	je	.LBB8_16
# BB#6:                                 # %.preheader.i18
	xorl	%ebp, %ebp
	cmpl	$285, %ecx              # imm = 0x11D
	jne	.LBB8_9
	jmp	.LBB8_50
	.p2align	4, 0x90
.LBB8_7:                                # %thread-pre-split.i
	movq	%r15, %rdi
	callq	luaX_next
	movl	16(%r15), %ecx
	cmpl	$285, %ecx              # imm = 0x11D
	je	.LBB8_50
.LBB8_9:
	cmpl	$279, %ecx              # imm = 0x117
	je	.LBB8_10
# BB#11:
	movl	$.L.str.10, %esi
	movq	%r15, %rdi
	callq	luaX_syntaxerror
	jmp	.LBB8_12
	.p2align	4, 0x90
.LBB8_50:                               # %str_checkname.exit.i
	movq	24(%r15), %r13
	movq	%r15, %rdi
	callq	luaX_next
	leal	1(%rbp), %r12d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	callq	new_localvar
	movl	%r12d, %ebp
.LBB8_12:
	movb	114(%r14), %r10b
	testb	%r10b, %r10b
	jne	.LBB8_15
# BB#13:
	cmpl	$44, 16(%r15)
	je	.LBB8_7
# BB#14:
	xorl	%r10d, %r10d
.LBB8_15:                               # %.critedge.loopexit.i
	andb	$1, %r10b
	jmp	.LBB8_16
.LBB8_10:                               # %.thread.i
	movq	%r15, %rdi
	callq	luaX_next
	movl	$.L.str.9, %esi
	movl	$3, %edx
	movq	%r15, %rdi
	callq	luaX_newstring
	leal	1(%rbp), %r12d
	movq	%r15, %rdi
	movq	%rax, %rsi
	movl	%ebp, %edx
	callq	new_localvar
	movb	$7, 114(%r14)
	movb	$1, %r10b
	movl	%r12d, %ebp
.LBB8_16:                               # %.critedge.i
	movq	(%rbx), %rcx
	movzbl	74(%rcx), %eax
	addl	%ebp, %eax
	movb	%al, 74(%rcx)
	testl	%ebp, %ebp
	je	.LBB8_24
# BB#17:                                # %.lr.ph.i.i
	movl	48(%rcx), %edx
	movq	(%rcx), %rsi
	movq	48(%rsi), %rsi
	movzbl	%al, %r8d
	movslq	%ebp, %rdi
	leal	-1(%rbp), %r9d
	andl	$3, %ebp
	je	.LBB8_21
# BB#18:                                # %.prol.preheader34
	movq	%rbx, %r11
	negq	%rdi
	leaq	196(%rcx,%r8,2), %rax
	negl	%ebp
	.p2align	4, 0x90
.LBB8_19:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rax,%rdi,2), %ebx
	shlq	$4, %rbx
	movl	%edx, 8(%rsi,%rbx)
	incq	%rdi
	incl	%ebp
	jne	.LBB8_19
# BB#20:                                # %.prol.loopexit35.unr-lcssa
	negq	%rdi
	movq	%r11, %rbx
.LBB8_21:                               # %.prol.loopexit35
	cmpl	$3, %r9d
	jb	.LBB8_24
# BB#22:                                # %.lr.ph.i.i.new
	movl	%edi, %ebp
	negl	%ebp
	leaq	202(%r8,%r8), %rax
	addq	%rdi, %rdi
	subq	%rdi, %rax
	addq	%rax, %rcx
	.p2align	4, 0x90
.LBB8_23:                               # =>This Inner Loop Header: Depth=1
	movzwl	-6(%rcx), %eax
	shlq	$4, %rax
	movl	%edx, 8(%rsi,%rax)
	movzwl	-4(%rcx), %eax
	shlq	$4, %rax
	movl	%edx, 8(%rsi,%rax)
	movzwl	-2(%rcx), %eax
	shlq	$4, %rax
	movl	%edx, 8(%rsi,%rax)
	movzwl	(%rcx), %eax
	shlq	$4, %rax
	movl	%edx, 8(%rsi,%rax)
	addq	$8, %rcx
	addl	$4, %ebp
	jne	.LBB8_23
.LBB8_24:                               # %parlist.exit
	movq	16(%rsp), %rdi          # 8-byte Reload
	movzbl	74(%rdi), %esi
	movl	%esi, %eax
	subb	%r10b, %al
	movb	%al, 113(%r14)
	callq	luaK_reserveregs
	cmpl	$41, 16(%r15)
	je	.LBB8_26
# BB#25:
	movq	56(%r15), %rbp
	movl	$41, %esi
	movq	%r15, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB8_26:                               # %checknext.exit19
	movq	%r15, %rdi
	callq	luaX_next
	movq	56(%r15), %rax
	movzwl	96(%rax), %ecx
	incl	%ecx
	movw	%cx, 96(%rax)
	movzwl	%cx, %eax
	cmpl	$201, %eax
	jb	.LBB8_28
# BB#27:
	movl	$.L.str.1, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	luaX_lexerror
.LBB8_28:                               # %enterlevel.exit.i.preheader.preheader
	movl	$-260, %r14d            # imm = 0xFEFC
	movl	$134283271, %r12d       # imm = 0x8010007
	.p2align	4, 0x90
.LBB8_29:                               # %enterlevel.exit.i.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%r15), %eax
	addl	%r14d, %eax
	cmpl	$27, %eax
	ja	.LBB8_31
# BB#30:                                # %enterlevel.exit.i.preheader
                                        #   in Loop: Header=BB8_29 Depth=1
	btl	%eax, %r12d
	jb	.LBB8_34
.LBB8_31:                               #   in Loop: Header=BB8_29 Depth=1
	movq	%r15, %rdi
	callq	statement
	movl	%eax, %ebp
	cmpl	$59, 16(%r15)
	jne	.LBB8_33
# BB#32:                                #   in Loop: Header=BB8_29 Depth=1
	movq	%r15, %rdi
	callq	luaX_next
.LBB8_33:                               # %testnext.exit.i
                                        #   in Loop: Header=BB8_29 Depth=1
	movq	(%rbx), %rax
	movzbl	74(%rax), %ecx
	movl	%ecx, 60(%rax)
	testl	%ebp, %ebp
	je	.LBB8_29
.LBB8_34:                               # %chunk.exit
	movq	56(%r15), %rax
	decw	96(%rax)
	movl	4(%r15), %eax
	movq	32(%rsp), %rcx
	movl	%eax, 100(%rcx)
	movl	$262, %esi              # imm = 0x106
	movl	$265, %edx              # imm = 0x109
	movq	%r15, %rdi
	movl	12(%rsp), %ecx          # 4-byte Reload
	callq	check_match
	movq	%r15, %rdi
	callq	close_func
	movq	48(%r15), %r14
	movq	(%r14), %rbx
	movl	88(%rbx), %r12d
	cmpl	%r12d, 68(%r14)
	jge	.LBB8_36
# BB#35:                                # %chunk.exit..preheader.i_crit_edge
	leaq	32(%rbx), %rbp
	movl	%r12d, %edx
	cmpl	%edx, %r12d
	jl	.LBB8_38
	jmp	.LBB8_43
.LBB8_36:
	leaq	88(%rbx), %rdx
	movq	56(%r15), %rdi
	leaq	32(%rbx), %rbp
	movq	32(%rbx), %rsi
	movl	$8, %ecx
	movl	$262143, %r8d           # imm = 0x3FFFF
	movl	$.L.str.11, %r9d
	callq	luaM_growaux_
	movq	%rax, 32(%rbx)
	movl	88(%rbx), %edx
	cmpl	%edx, %r12d
	jge	.LBB8_43
.LBB8_38:                               # %.lr.ph44.i
	movslq	%r12d, %rax
	movslq	%edx, %rcx
	subl	%r12d, %edx
	leaq	-1(%rcx), %rsi
	subq	%rax, %rsi
	andq	$7, %rdx
	je	.LBB8_41
# BB#39:                                # %.prol.preheader
	negq	%rdx
	.p2align	4, 0x90
.LBB8_40:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	$0, (%rdi,%rax,8)
	incq	%rax
	incq	%rdx
	jne	.LBB8_40
.LBB8_41:                               # %.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB8_43
	.p2align	4, 0x90
.LBB8_42:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	(%rbp), %rdx
	movq	$0, 8(%rdx,%rax,8)
	movq	(%rbp), %rdx
	movq	$0, 16(%rdx,%rax,8)
	movq	(%rbp), %rdx
	movq	$0, 24(%rdx,%rax,8)
	movq	(%rbp), %rdx
	movq	$0, 32(%rdx,%rax,8)
	movq	(%rbp), %rdx
	movq	$0, 40(%rdx,%rax,8)
	movq	(%rbp), %rdx
	movq	$0, 48(%rdx,%rax,8)
	movq	(%rbp), %rdx
	movq	$0, 56(%rdx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.LBB8_42
.LBB8_43:                               # %._crit_edge45.i
	movq	32(%rsp), %rax
	movq	(%rbp), %rdx
	movslq	68(%r14), %rsi
	leal	1(%rsi), %ecx
	movl	%ecx, 68(%r14)
	movq	%rax, (%rdx,%rsi,8)
	movq	32(%rsp), %rdx
	testb	$3, 9(%rdx)
	je	.LBB8_46
# BB#44:
	testb	$4, 9(%rbx)
	je	.LBB8_46
# BB#45:
	movq	56(%r15), %rdi
	movq	%rbx, %rsi
	callq	luaC_barrierf
	movl	68(%r14), %ecx
.LBB8_46:
	decl	%ecx
	movl	$36, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	luaK_codeABx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	$-1, 16(%rcx)
	movl	$-1, 20(%rcx)
	movl	$11, (%rcx)
	movl	%eax, 8(%rcx)
	movq	32(%rsp), %rax
	cmpb	$0, 112(%rax)
	je	.LBB8_49
# BB#47:                                # %.lr.ph.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_48:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	cmpb	$6, 107(%rsp,%rbx,2)
	setne	%sil
	shll	$2, %esi
	movzbl	108(%rsp,%rbx,2), %ecx
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	callq	luaK_codeABC
	incq	%rbx
	movq	32(%rsp), %rax
	movzbl	112(%rax), %eax
	cmpq	%rax, %rbx
	jl	.LBB8_48
.LBB8_49:                               # %pushclosure.exit
	addq	$632, %rsp              # imm = 0x278
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	body, .Lfunc_end8-body
	.cfi_endproc

	.p2align	4, 0x90
	.type	primaryexp,@function
primaryexp:                             # @primaryexp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi112:
	.cfi_def_cfa_offset 80
.Lcfi113:
	.cfi_offset %rbx, -48
.Lcfi114:
	.cfi_offset %r12, -40
.Lcfi115:
	.cfi_offset %r14, -32
.Lcfi116:
	.cfi_offset %r15, -24
.Lcfi117:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	48(%rbx), %r15
	movl	16(%rbx), %eax
	cmpl	$285, %eax              # imm = 0x11D
	je	.LBB9_3
# BB#1:
	cmpl	$40, %eax
	jne	.LBB9_4
# BB#2:
	movl	4(%rbx), %ebp
	movq	%rbx, %rdi
	callq	luaX_next
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	subexpr
	movl	$41, %esi
	movl	$40, %edx
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	callq	check_match
	movq	48(%rbx), %rdi
	movq	%r14, %rsi
	callq	luaK_dischargevars
	jmp	.LBB9_5
.LBB9_3:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	singlevar
	jmp	.LBB9_5
.LBB9_4:
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	luaX_syntaxerror
.LBB9_5:                                # %prefixexp.exit.preheader
	leaq	8(%rsp), %r12
	jmp	.LBB9_6
	.p2align	4, 0x90
.LBB9_17:                               # %prefixexp.exit
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	funcargs
.LBB9_6:                                # %prefixexp.exit
                                        # =>This Inner Loop Header: Depth=1
	movl	16(%rbx), %eax
	cmpl	$90, %eax
	jg	.LBB9_13
# BB#7:                                 # %prefixexp.exit
                                        #   in Loop: Header=BB9_6 Depth=1
	cmpl	$40, %eax
	je	.LBB9_16
# BB#8:                                 # %prefixexp.exit
                                        #   in Loop: Header=BB9_6 Depth=1
	cmpl	$46, %eax
	je	.LBB9_18
# BB#9:                                 # %prefixexp.exit
                                        #   in Loop: Header=BB9_6 Depth=1
	cmpl	$58, %eax
	jne	.LBB9_20
# BB#10:                                #   in Loop: Header=BB9_6 Depth=1
	movq	%rbx, %rdi
	callq	luaX_next
	cmpl	$285, 16(%rbx)          # imm = 0x11D
	je	.LBB9_12
# BB#11:                                #   in Loop: Header=BB9_6 Depth=1
	movq	56(%rbx), %rbp
	movl	$285, %esi              # imm = 0x11D
	movq	%rbx, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB9_12:                               # %checkname.exit
                                        #   in Loop: Header=BB9_6 Depth=1
	movq	24(%rbx), %rbp
	movq	%rbx, %rdi
	callq	luaX_next
	movq	48(%rbx), %rdi
	movq	%rbp, %rsi
	callq	luaK_stringK
	movl	$-1, 24(%rsp)
	movl	$-1, 28(%rsp)
	movl	$4, 8(%rsp)
	movl	%eax, 16(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	luaK_self
	jmp	.LBB9_17
	.p2align	4, 0x90
.LBB9_13:                               # %prefixexp.exit
                                        #   in Loop: Header=BB9_6 Depth=1
	cmpl	$91, %eax
	je	.LBB9_19
# BB#14:                                # %prefixexp.exit
                                        #   in Loop: Header=BB9_6 Depth=1
	cmpl	$123, %eax
	je	.LBB9_16
# BB#15:                                # %prefixexp.exit
                                        #   in Loop: Header=BB9_6 Depth=1
	cmpl	$286, %eax              # imm = 0x11E
	jne	.LBB9_20
.LBB9_16:                               #   in Loop: Header=BB9_6 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaK_exp2nextreg
	jmp	.LBB9_17
.LBB9_18:                               #   in Loop: Header=BB9_6 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	field
	jmp	.LBB9_6
.LBB9_19:                               #   in Loop: Header=BB9_6 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaK_exp2anyreg
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	yindex
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	luaK_indexed
	jmp	.LBB9_6
.LBB9_20:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	primaryexp, .Lfunc_end9-primaryexp
	.cfi_endproc

	.p2align	4, 0x90
	.type	recfield,@function
recfield:                               # @recfield
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi118:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi119:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi120:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi121:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi122:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi124:
	.cfi_def_cfa_offset 112
.Lcfi125:
	.cfi_offset %rbx, -56
.Lcfi126:
	.cfi_offset %r12, -48
.Lcfi127:
	.cfi_offset %r13, -40
.Lcfi128:
	.cfi_offset %r14, -32
.Lcfi129:
	.cfi_offset %r15, -24
.Lcfi130:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	48(%rbx), %r15
	movl	60(%r15), %r13d
	cmpl	$285, 16(%rbx)          # imm = 0x11D
	jne	.LBB10_8
# BB#1:
	cmpl	$2147483646, 32(%r14)   # imm = 0x7FFFFFFE
	jl	.LBB10_7
# BB#2:
	movq	(%r15), %rax
	movq	32(%r15), %rdi
	movl	96(%rax), %edx
	testl	%edx, %edx
	je	.LBB10_3
# BB#4:
	movl	$.L.str.5, %esi
	movl	$2147483645, %ecx       # imm = 0x7FFFFFFD
	movl	$.L.str.3, %r8d
	xorl	%eax, %eax
	callq	luaO_pushfstring
	jmp	.LBB10_5
.LBB10_8:
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	yindex
	leaq	32(%r14), %rbp
	jmp	.LBB10_9
.LBB10_3:
	movl	$.L.str.4, %esi
	movl	$2147483645, %edx       # imm = 0x7FFFFFFD
	movl	$.L.str.3, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
.LBB10_5:
	movq	24(%r15), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	luaX_lexerror
	cmpl	$285, 16(%rbx)          # imm = 0x11D
	je	.LBB10_7
# BB#6:
	movq	56(%rbx), %r12
	movl	$285, %esi              # imm = 0x11D
	movq	%rbx, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB10_7:                               # %checkname.exit
	leaq	32(%r14), %rbp
	movq	24(%rbx), %r12
	movq	%rbx, %rdi
	callq	luaX_next
	movq	48(%rbx), %rdi
	movq	%r12, %rsi
	callq	luaK_stringK
	movl	$-1, 24(%rsp)
	movl	$-1, 28(%rsp)
	movl	$4, 8(%rsp)
	movl	%eax, 16(%rsp)
.LBB10_9:
	incl	(%rbp)
	cmpl	$61, 16(%rbx)
	je	.LBB10_11
# BB#10:
	movq	56(%rbx), %r12
	movl	$61, %esi
	movq	%rbx, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB10_11:                              # %checknext.exit
	movq	%rbx, %rdi
	callq	luaX_next
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	luaK_exp2RK
	movl	%eax, %ebp
	leaq	32(%rsp), %r12
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	subexpr
	movq	24(%r14), %rax
	movl	8(%rax), %ebx
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	luaK_exp2RK
	movl	$9, %esi
	movq	%r15, %rdi
	movl	%ebx, %edx
	movl	%ebp, %ecx
	movl	%eax, %r8d
	callq	luaK_codeABC
	movl	%r13d, 60(%r15)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	recfield, .Lfunc_end10-recfield
	.cfi_endproc

	.p2align	4, 0x90
	.type	yindex,@function
yindex:                                 # @yindex
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi133:
	.cfi_def_cfa_offset 32
.Lcfi134:
	.cfi_offset %rbx, -24
.Lcfi135:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	luaX_next
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	subexpr
	movq	48(%rbx), %rdi
	movq	%r14, %rsi
	callq	luaK_exp2val
	cmpl	$93, 16(%rbx)
	je	.LBB11_2
# BB#1:
	movq	56(%rbx), %r14
	movl	$93, %esi
	movq	%rbx, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB11_2:                               # %checknext.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	luaX_next               # TAILCALL
.Lfunc_end11:
	.size	yindex, .Lfunc_end11-yindex
	.cfi_endproc

	.p2align	4, 0x90
	.type	new_localvar,@function
new_localvar:                           # @new_localvar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi139:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi140:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi142:
	.cfi_def_cfa_offset 64
.Lcfi143:
	.cfi_offset %rbx, -56
.Lcfi144:
	.cfi_offset %r12, -48
.Lcfi145:
	.cfi_offset %r13, -40
.Lcfi146:
	.cfi_offset %r14, -32
.Lcfi147:
	.cfi_offset %r15, -24
.Lcfi148:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %rbp
	movq	%rdi, %r12
	movq	48(%r12), %r15
	movzbl	74(%r15), %eax
	addl	%ebx, %eax
	cmpl	$200, %eax
	movq	%r15, %r13
	jl	.LBB12_5
# BB#1:
	movq	(%r15), %rax
	movq	32(%r15), %rdi
	movl	96(%rax), %edx
	testl	%edx, %edx
	je	.LBB12_2
# BB#3:
	movl	$.L.str.5, %esi
	movl	$200, %ecx
	movl	$.L.str.7, %r8d
	xorl	%eax, %eax
	callq	luaO_pushfstring
	jmp	.LBB12_4
.LBB12_2:
	movl	$.L.str.4, %esi
	movl	$200, %edx
	movl	$.L.str.7, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
.LBB12_4:                               # %errorlimit.exit
	movq	24(%r15), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	luaX_lexerror
	movq	48(%r12), %r13
.LBB12_5:
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movq	(%r13), %rbx
	movl	92(%rbx), %r14d
	movswl	72(%r13), %eax
	cmpl	%r14d, %eax
	jge	.LBB12_7
# BB#6:                                 # %..preheader_crit_edge.i
	movq	48(%rbx), %rax
	movl	%r14d, %esi
	cmpl	%esi, %r14d
	jl	.LBB12_9
	jmp	.LBB12_15
.LBB12_7:
	leaq	92(%rbx), %rdx
	movq	56(%r12), %rdi
	movq	48(%rbx), %rsi
	movl	$16, %ecx
	movl	$32767, %r8d            # imm = 0x7FFF
	movl	$.L.str.8, %r9d
	callq	luaM_growaux_
	movq	%rax, 48(%rbx)
	movl	92(%rbx), %esi
	cmpl	%esi, %r14d
	jge	.LBB12_15
.LBB12_9:                               # %.lr.ph.i
	movslq	%r14d, %rdi
	movslq	%esi, %rcx
	subl	%r14d, %esi
	leaq	-1(%rcx), %r8
	subq	%rdi, %r8
	andq	$7, %rsi
	je	.LBB12_12
# BB#10:                                # %.prol.preheader
	movq	%rdi, %rdx
	shlq	$4, %rdx
	addq	%rax, %rdx
	negq	%rsi
	.p2align	4, 0x90
.LBB12_11:                              # =>This Inner Loop Header: Depth=1
	incq	%rdi
	movq	$0, (%rdx)
	addq	$16, %rdx
	incq	%rsi
	jne	.LBB12_11
.LBB12_12:                              # %.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB12_15
# BB#13:                                # %.lr.ph.i.new
	subq	%rdi, %rcx
	shlq	$4, %rdi
	leaq	112(%rax,%rdi), %rdx
	.p2align	4, 0x90
.LBB12_14:                              # =>This Inner Loop Header: Depth=1
	movq	$0, -112(%rdx)
	movq	$0, -96(%rdx)
	movq	$0, -80(%rdx)
	movq	$0, -64(%rdx)
	movq	$0, -48(%rdx)
	movq	$0, -32(%rdx)
	movq	$0, -16(%rdx)
	movq	$0, (%rdx)
	subq	$-128, %rdx
	addq	$-8, %rcx
	jne	.LBB12_14
.LBB12_15:                              # %._crit_edge.i
	movswq	72(%r13), %rcx
	movq	%rcx, %rdx
	shlq	$4, %rdx
	movq	%rbp, (%rax,%rdx)
	testb	$3, 9(%rbp)
	je	.LBB12_16
# BB#17:
	movq	%rbp, %rdx
	testb	$4, 9(%rbx)
	movl	4(%rsp), %ebp           # 4-byte Reload
	je	.LBB12_19
# BB#18:
	movq	56(%r12), %rdi
	movq	%rbx, %rsi
	callq	luaC_barrierf
	movw	72(%r13), %cx
	jmp	.LBB12_19
.LBB12_16:
	movl	4(%rsp), %ebp           # 4-byte Reload
.LBB12_19:                              # %registerlocalvar.exit
	leal	1(%rcx), %eax
	movw	%ax, 72(%r13)
	movzbl	74(%r15), %eax
	movslq	%ebp, %rdx
	addq	%rax, %rdx
	movw	%cx, 196(%r15,%rdx,2)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	new_localvar, .Lfunc_end12-new_localvar
	.cfi_endproc

	.p2align	4, 0x90
	.type	field,@function
field:                                  # @field
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi153:
	.cfi_def_cfa_offset 64
.Lcfi154:
	.cfi_offset %rbx, -40
.Lcfi155:
	.cfi_offset %r12, -32
.Lcfi156:
	.cfi_offset %r14, -24
.Lcfi157:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	48(%rbx), %r15
	movq	%r15, %rdi
	callq	luaK_exp2anyreg
	movq	%rbx, %rdi
	callq	luaX_next
	cmpl	$285, 16(%rbx)          # imm = 0x11D
	je	.LBB13_2
# BB#1:
	movq	56(%rbx), %r12
	movl	$285, %esi              # imm = 0x11D
	movq	%rbx, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB13_2:                               # %checkname.exit
	movq	24(%rbx), %r12
	movq	%rbx, %rdi
	callq	luaX_next
	movq	48(%rbx), %rdi
	movq	%r12, %rsi
	callq	luaK_stringK
	movl	$-1, 16(%rsp)
	movl	$-1, 20(%rsp)
	movl	$4, (%rsp)
	movl	%eax, 8(%rsp)
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	luaK_indexed
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	field, .Lfunc_end13-field
	.cfi_endproc

	.p2align	4, 0x90
	.type	funcargs,@function
funcargs:                               # @funcargs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi161:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi163:
	.cfi_def_cfa_offset 80
.Lcfi164:
	.cfi_offset %rbx, -48
.Lcfi165:
	.cfi_offset %r12, -40
.Lcfi166:
	.cfi_offset %r14, -32
.Lcfi167:
	.cfi_offset %r15, -24
.Lcfi168:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	48(%rbx), %r15
	movl	4(%rbx), %r14d
	movl	16(%rbx), %eax
	cmpl	$286, %eax              # imm = 0x11E
	je	.LBB14_12
# BB#1:
	cmpl	$123, %eax
	je	.LBB14_14
# BB#2:
	cmpl	$40, %eax
	jne	.LBB14_13
# BB#3:
	cmpl	8(%rbx), %r14d
	je	.LBB14_5
# BB#4:
	movl	$.L.str.15, %esi
	movq	%rbx, %rdi
	callq	luaX_syntaxerror
.LBB14_5:
	movq	%rbx, %rdi
	callq	luaX_next
	cmpl	$41, 16(%rbx)
	jne	.LBB14_7
# BB#6:
	movl	$0, 8(%rsp)
	jmp	.LBB14_11
.LBB14_12:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	callq	luaK_stringK
	movl	$-1, 24(%rsp)
	movl	$-1, 28(%rsp)
	movl	$4, 8(%rsp)
	movl	%eax, 16(%rsp)
	movq	%rbx, %rdi
	callq	luaX_next
	jmp	.LBB14_15
.LBB14_14:
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	constructor
	jmp	.LBB14_15
.LBB14_13:
	movl	$.L.str.16, %esi
	movq	%rbx, %rdi
	callq	luaX_syntaxerror
	jmp	.LBB14_20
.LBB14_7:
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	subexpr
	cmpl	$44, 16(%rbx)
	jne	.LBB14_10
# BB#8:                                 # %.lr.ph.preheader
	leaq	8(%rsp), %rbp
	.p2align	4, 0x90
.LBB14_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	luaX_next
	movq	48(%rbx), %rdi
	movq	%rbp, %rsi
	callq	luaK_exp2nextreg
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	subexpr
	cmpl	$44, 16(%rbx)
	je	.LBB14_9
.LBB14_10:                              # %explist1.exit
	leaq	8(%rsp), %rsi
	movl	$-1, %edx
	movq	%r15, %rdi
	callq	luaK_setreturns
.LBB14_11:
	movl	$41, %esi
	movl	$40, %edx
	movq	%rbx, %rdi
	movl	%r14d, %ecx
	callq	check_match
.LBB14_15:
	movl	8(%r12), %ebx
	xorl	%ecx, %ecx
	movl	8(%rsp), %eax
	leal	-13(%rax), %edx
	cmpl	$2, %edx
	jb	.LBB14_19
# BB#16:
	testl	%eax, %eax
	je	.LBB14_18
# BB#17:
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	luaK_exp2nextreg
.LBB14_18:
	movl	60(%r15), %ecx
	subl	%ebx, %ecx
.LBB14_19:
	movl	$28, %esi
	movl	$2, %r8d
	movq	%r15, %rdi
	movl	%ebx, %edx
	callq	luaK_codeABC
	movl	$-1, 16(%r12)
	movl	$-1, 20(%r12)
	movl	$13, (%r12)
	movl	%eax, 8(%r12)
	movq	%r15, %rdi
	movl	%r14d, %esi
	callq	luaK_fixline
	incl	%ebx
	movl	%ebx, 60(%r15)
.LBB14_20:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	funcargs, .Lfunc_end14-funcargs
	.cfi_endproc

	.p2align	4, 0x90
	.type	singlevar,@function
singlevar:                              # @singlevar
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 32
.Lcfi172:
	.cfi_offset %rbx, -32
.Lcfi173:
	.cfi_offset %r14, -24
.Lcfi174:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpl	$285, 16(%rbx)          # imm = 0x11D
	je	.LBB15_2
# BB#1:
	movq	56(%rbx), %r15
	movl	$285, %esi              # imm = 0x11D
	movq	%rbx, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB15_2:                               # %str_checkname.exit
	movq	24(%rbx), %r15
	movq	%rbx, %rdi
	callq	luaX_next
	movq	48(%rbx), %rbx
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	singlevaraux
	cmpl	$8, %eax
	jne	.LBB15_4
# BB#3:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	luaK_stringK
	movl	%eax, 8(%r14)
.LBB15_4:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	singlevar, .Lfunc_end15-singlevar
	.cfi_endproc

	.p2align	4, 0x90
	.type	singlevaraux,@function
singlevaraux:                           # @singlevaraux
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi175:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi176:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi177:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi178:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi179:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi180:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi181:
	.cfi_def_cfa_offset 64
.Lcfi182:
	.cfi_offset %rbx, -56
.Lcfi183:
	.cfi_offset %r12, -48
.Lcfi184:
	.cfi_offset %r13, -40
.Lcfi185:
	.cfi_offset %r14, -32
.Lcfi186:
	.cfi_offset %r15, -24
.Lcfi187:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB16_1
# BB#2:
	movzbl	74(%rbx), %edx
	.p2align	4, 0x90
.LBB16_3:                               # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jle	.LBB16_12
# BB#4:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	(%rbx), %rax
	movq	48(%rax), %rax
	movzwl	194(%rbx,%rdx,2), %esi
	decq	%rdx
	shlq	$4, %rsi
	cmpq	%r15, (%rax,%rsi)
	jne	.LBB16_3
# BB#5:                                 # %searchvar.exit
	testl	%edx, %edx
	js	.LBB16_12
# BB#6:
	movl	$-1, 16(%r14)
	movl	$-1, 20(%r14)
	movl	$6, (%r14)
	movl	%edx, 8(%r14)
	movl	$6, %eax
	testl	%ecx, %ecx
	jne	.LBB16_37
# BB#7:
	movq	40(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.LBB16_9
	jmp	.LBB16_37
	.p2align	4, 0x90
.LBB16_10:                              #   in Loop: Header=BB16_9 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB16_37
.LBB16_9:                               # %.lr.ph.i22
                                        # =>This Inner Loop Header: Depth=1
	movzbl	12(%rcx), %esi
	cmpl	%edx, %esi
	jg	.LBB16_10
# BB#11:                                # %.critedge.i
	movb	$1, 13(%rcx)
	jmp	.LBB16_37
.LBB16_12:                              # %searchvar.exit.thread
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	singlevaraux
	movl	%eax, %ecx
	movl	$8, %eax
	cmpl	$8, %ecx
	je	.LBB16_37
# BB#13:
	movq	(%rbx), %r12
	movl	72(%r12), %r13d
	movzbl	112(%r12), %ecx
	testl	%ecx, %ecx
	je	.LBB16_18
# BB#14:                                # %.lr.ph54.i
	movl	(%r14), %esi
	leaq	8(%r14), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_15:                              # =>This Inner Loop Header: Depth=1
	movzbl	75(%rbx,%rax,2), %edi
	cmpl	%esi, %edi
	jne	.LBB16_17
# BB#16:                                #   in Loop: Header=BB16_15 Depth=1
	movzbl	76(%rbx,%rax,2), %edi
	cmpl	(%rdx), %edi
	je	.LBB16_36
.LBB16_17:                              #   in Loop: Header=BB16_15 Depth=1
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB16_15
.LBB16_18:                              # %._crit_edge55.i
	movl	%ecx, %eax
	incl	%eax
	cmpl	$61, %eax
	movl	%r13d, %esi
	jb	.LBB16_23
# BB#19:
	movl	96(%r12), %edx
	movq	32(%rbx), %rdi
	testl	%edx, %edx
	je	.LBB16_20
# BB#21:
	movl	$.L.str.5, %esi
	movl	$60, %ecx
	movl	$.L.str.13, %r8d
	xorl	%eax, %eax
	callq	luaO_pushfstring
	jmp	.LBB16_22
.LBB16_1:
	movl	$-1, 16(%r14)
	movl	$-1, 20(%r14)
	movl	$8, (%r14)
	movl	$255, 8(%r14)
	movl	$8, %eax
	jmp	.LBB16_37
.LBB16_20:
	movl	$.L.str.4, %esi
	movl	$60, %edx
	movl	$.L.str.13, %ecx
	xorl	%eax, %eax
	callq	luaO_pushfstring
.LBB16_22:                              # %errorlimit.exit.i
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	luaX_lexerror
	movb	112(%r12), %cl
	movl	72(%r12), %esi
.LBB16_23:
	movzbl	%cl, %eax
	cmpl	%esi, %eax
	jge	.LBB16_25
# BB#24:                                # %..preheader_crit_edge.i
	leaq	56(%r12), %rbp
	movq	56(%r12), %rax
	cmpl	%esi, %r13d
	jl	.LBB16_27
	jmp	.LBB16_32
.LBB16_25:
	leaq	72(%r12), %rdx
	movq	32(%rbx), %rdi
	leaq	56(%r12), %rbp
	movq	56(%r12), %rsi
	movl	$8, %ecx
	movl	$2147483645, %r8d       # imm = 0x7FFFFFFD
	movl	$.L.str.14, %r9d
	callq	luaM_growaux_
	movq	%rax, 56(%r12)
	movl	72(%r12), %esi
	cmpl	%esi, %r13d
	jge	.LBB16_32
.LBB16_27:                              # %.lr.ph.i
	movslq	%r13d, %rcx
	movslq	%esi, %rdx
	subl	%r13d, %esi
	leaq	-1(%rdx), %rdi
	subq	%rcx, %rdi
	andq	$7, %rsi
	je	.LBB16_30
# BB#28:                                # %.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB16_29:                              # =>This Inner Loop Header: Depth=1
	movq	$0, (%rax,%rcx,8)
	incq	%rcx
	movq	(%rbp), %rax
	incq	%rsi
	jne	.LBB16_29
.LBB16_30:                              # %.prol.loopexit
	cmpq	$7, %rdi
	jb	.LBB16_32
	.p2align	4, 0x90
.LBB16_31:                              # =>This Inner Loop Header: Depth=1
	movq	$0, (%rax,%rcx,8)
	movq	(%rbp), %rax
	movq	$0, 8(%rax,%rcx,8)
	movq	(%rbp), %rax
	movq	$0, 16(%rax,%rcx,8)
	movq	(%rbp), %rax
	movq	$0, 24(%rax,%rcx,8)
	movq	(%rbp), %rax
	movq	$0, 32(%rax,%rcx,8)
	movq	(%rbp), %rax
	movq	$0, 40(%rax,%rcx,8)
	movq	(%rbp), %rax
	movq	$0, 48(%rax,%rcx,8)
	movq	(%rbp), %rax
	movq	$0, 56(%rax,%rcx,8)
	addq	$8, %rcx
	movq	(%rbp), %rax
	cmpq	%rcx, %rdx
	jne	.LBB16_31
.LBB16_32:                              # %._crit_edge.i
	movzbl	112(%r12), %ecx
	movq	%r15, (%rax,%rcx,8)
	testb	$3, 9(%r15)
	je	.LBB16_35
# BB#33:
	testb	$4, 9(%r12)
	je	.LBB16_35
# BB#34:
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	luaC_barrierf
	movb	112(%r12), %cl
.LBB16_35:
	movb	(%r14), %dl
	movzbl	%cl, %eax
	movb	%dl, 75(%rbx,%rax,2)
	movb	8(%r14), %cl
	movq	%r14, %rdx
	addq	$8, %rdx
	movb	%cl, 76(%rbx,%rax,2)
	movl	%eax, %ecx
	incb	%cl
	movb	%cl, 112(%r12)
.LBB16_36:                              # %indexupvalue.exit
	movl	%eax, (%rdx)
	movl	$7, (%r14)
	movl	$7, %eax
.LBB16_37:                              # %markupval.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	singlevaraux, .Lfunc_end16-singlevaraux
	.cfi_endproc

	.p2align	4, 0x90
	.type	forbody,@function
forbody:                                # @forbody
	.cfi_startproc
# BB#0:                                 # %adjustlocalvars.exit36
	pushq	%rbp
.Lcfi188:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi189:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi190:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi191:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi192:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi193:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi194:
	.cfi_def_cfa_offset 96
.Lcfi195:
	.cfi_offset %rbx, -56
.Lcfi196:
	.cfi_offset %r12, -48
.Lcfi197:
	.cfi_offset %r13, -40
.Lcfi198:
	.cfi_offset %r14, -32
.Lcfi199:
	.cfi_offset %r15, -24
.Lcfi200:
	.cfi_offset %rbp, -16
	movl	%r8d, 20(%rsp)          # 4-byte Spill
	movl	%ecx, %r15d
	movl	%edx, %r13d
	movl	%esi, %ebx
	movq	%rdi, %r14
	movq	48(%r14), %r12
	movzbl	74(%r12), %eax
	addl	$3, %eax
	movb	%al, 74(%r12)
	movl	48(%r12), %ecx
	movq	(%r12), %rdx
	movq	48(%rdx), %rdx
	movzbl	%al, %eax
	movzwl	190(%r12,%rax,2), %esi
	shlq	$4, %rsi
	movl	%ecx, 8(%rdx,%rsi)
	movzwl	192(%r12,%rax,2), %esi
	shlq	$4, %rsi
	movl	%ecx, 8(%rdx,%rsi)
	movzwl	194(%r12,%rax,2), %eax
	shlq	$4, %rax
	movl	%ecx, 8(%rdx,%rax)
	cmpl	$259, 16(%r14)          # imm = 0x103
	je	.LBB17_2
# BB#1:
	movq	56(%r14), %rbp
	movl	$259, %esi              # imm = 0x103
	movq	%r14, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB17_2:                               # %checknext.exit
	movq	%r14, %rdi
	callq	luaX_next
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	movl	%r13d, 16(%rsp)         # 4-byte Spill
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	je	.LBB17_4
# BB#3:
	movl	$32, %esi
	movl	$131070, %ecx           # imm = 0x1FFFE
	movq	%r12, %rdi
	movl	%ebx, %edx
	callq	luaK_codeABx
	jmp	.LBB17_5
.LBB17_4:
	movq	%r12, %rdi
	callq	luaK_jump
.LBB17_5:
	movl	%eax, %r13d
	movl	$-1, 32(%rsp)
	movb	$0, 38(%rsp)
	movb	74(%r12), %al
	movb	%al, 36(%rsp)
	movb	$0, 37(%rsp)
	movq	40(%r12), %rax
	movq	%rax, 24(%rsp)
	leaq	24(%rsp), %rax
	movq	%rax, 40(%r12)
	movq	48(%r14), %rax
	movzbl	74(%rax), %esi
	addl	%r15d, %esi
	movb	%sil, 74(%rax)
	testl	%r15d, %r15d
	je	.LBB17_13
# BB#6:                                 # %.lr.ph.i
	movl	48(%rax), %ecx
	movq	(%rax), %rdx
	movq	48(%rdx), %rdx
	movzbl	%sil, %r8d
	movslq	%r15d, %rsi
	leal	-1(%r15), %r9d
	movl	%r15d, %edi
	andl	$3, %edi
	je	.LBB17_10
# BB#7:                                 # %.prol.preheader37
	negq	%rsi
	leaq	196(%rax,%r8,2), %rbp
	negl	%edi
	.p2align	4, 0x90
.LBB17_8:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rbp,%rsi,2), %ebx
	shlq	$4, %rbx
	movl	%ecx, 8(%rdx,%rbx)
	incq	%rsi
	incl	%edi
	jne	.LBB17_8
# BB#9:                                 # %.prol.loopexit38.unr-lcssa
	negq	%rsi
.LBB17_10:                              # %.prol.loopexit38
	cmpl	$3, %r9d
	jb	.LBB17_13
# BB#11:                                # %.lr.ph.i.new
	movl	%esi, %ebp
	negl	%ebp
	leaq	202(%r8,%r8), %rdi
	addq	%rsi, %rsi
	subq	%rsi, %rdi
	addq	%rdi, %rax
	.p2align	4, 0x90
.LBB17_12:                              # =>This Inner Loop Header: Depth=1
	movzwl	-6(%rax), %esi
	shlq	$4, %rsi
	movl	%ecx, 8(%rdx,%rsi)
	movzwl	-4(%rax), %esi
	shlq	$4, %rsi
	movl	%ecx, 8(%rdx,%rsi)
	movzwl	-2(%rax), %esi
	shlq	$4, %rsi
	movl	%ecx, 8(%rdx,%rsi)
	movzwl	(%rax), %esi
	shlq	$4, %rsi
	movl	%ecx, 8(%rdx,%rsi)
	addq	$8, %rax
	addl	$4, %ebp
	jne	.LBB17_12
.LBB17_13:                              # %adjustlocalvars.exit33
	movq	%r12, %rdi
	movl	%r15d, %esi
	callq	luaK_reserveregs
	movq	%r14, %rdi
	callq	block
	movq	40(%r12), %r14
	movq	(%r14), %rax
	movq	%rax, 40(%r12)
	movq	24(%r12), %rax
	movzbl	12(%r14), %edx
	movq	48(%rax), %rax
	movzbl	74(%rax), %ecx
	cmpb	%dl, %cl
	jbe	.LBB17_19
# BB#14:                                # %.lr.ph.i.i
	movq	(%rax), %rsi
	movq	48(%rsi), %r9
	movl	48(%rax), %edi
	movl	%ecx, %ebx
	subb	%dl, %bl
	movl	%ecx, %r8d
	decb	%r8b
	testb	$1, %bl
	je	.LBB17_16
# BB#15:
	movl	%ecx, %ebx
	decb	%bl
	movb	%bl, 74(%rax)
	movzbl	%bl, %ebp
	movzwl	196(%rax,%rbp,2), %ebp
	shlq	$4, %rbp
	movl	%edi, 12(%r9,%rbp)
	decq	%rcx
.LBB17_16:                              # %.prol.loopexit
	cmpb	%dl, %r8b
	je	.LBB17_19
# BB#17:                                # %.lr.ph.i.i.new
	leal	255(%rcx), %ebp
	incb	%bpl
	decb	%cl
	.p2align	4, 0x90
.LBB17_18:                              # =>This Inner Loop Header: Depth=1
	movb	%cl, 74(%rax)
	movzbl	%cl, %ecx
	movzwl	196(%rax,%rcx,2), %ebx
	shlq	$4, %rbx
	movl	%edi, 12(%r9,%rbx)
	addb	$-2, %bpl
	movb	%bpl, 74(%rax)
	movzbl	%bpl, %ebx
	movzwl	196(%rax,%rbx,2), %esi
	shlq	$4, %rsi
	movl	%edi, 12(%r9,%rsi)
	addb	$-2, %cl
	cmpb	%dl, %bl
	ja	.LBB17_18
.LBB17_19:                              # %removevars.exit.i
	cmpb	$0, 13(%r14)
	je	.LBB17_21
# BB#20:
	movl	$35, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	callq	luaK_codeABC
.LBB17_21:                              # %leaveblock.exit
	movzbl	74(%r12), %eax
	movl	%eax, 60(%r12)
	movl	8(%r14), %esi
	movq	%r12, %rdi
	callq	luaK_patchtohere
	movq	%r12, %rdi
	movl	%r13d, %esi
	callq	luaK_patchtohere
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB17_23
# BB#22:                                # %.thread
	movl	$31, %esi
	movl	$131070, %ecx           # imm = 0x1FFFE
	movq	%r12, %rdi
	movl	12(%rsp), %edx          # 4-byte Reload
	callq	luaK_codeABx
	movl	%eax, %ebp
	movq	%r12, %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	callq	luaK_fixline
	jmp	.LBB17_24
.LBB17_23:
	movl	$33, %esi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	%r15d, %r8d
	callq	luaK_codeABC
	movq	%r12, %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	callq	luaK_fixline
	movq	%r12, %rdi
	callq	luaK_jump
	movl	%eax, %ebp
.LBB17_24:
	incl	%r13d
	movq	%r12, %rdi
	movl	%ebp, %esi
	movl	%r13d, %edx
	callq	luaK_patchlist
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	forbody, .Lfunc_end17-forbody
	.cfi_endproc

	.p2align	4, 0x90
	.type	assignment,@function
assignment:                             # @assignment
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi201:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi202:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi203:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi204:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi205:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi206:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi207:
	.cfi_def_cfa_offset 96
.Lcfi208:
	.cfi_offset %rbx, -56
.Lcfi209:
	.cfi_offset %r12, -48
.Lcfi210:
	.cfi_offset %r13, -40
.Lcfi211:
	.cfi_offset %r14, -32
.Lcfi212:
	.cfi_offset %r15, -24
.Lcfi213:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	8(%rbp), %eax
	addl	$-6, %eax
	cmpl	$4, %eax
	jb	.LBB18_2
# BB#1:
	movl	$.L.str.26, %esi
	movq	%rbx, %rdi
	callq	luaX_syntaxerror
.LBB18_2:
	leaq	8(%rbp), %r12
	movl	16(%rbx), %eax
	cmpl	$61, %eax
	je	.LBB18_22
# BB#3:
	cmpl	$44, %eax
	jne	.LBB18_21
# BB#4:
	movq	%r12, %r14
	movq	%rbx, %rdi
	callq	luaX_next
	movq	%rbp, (%rsp)
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	callq	primaryexp
	cmpl	$6, 8(%rsp)
	jne	.LBB18_15
# BB#5:
	testq	%rbp, %rbp
	je	.LBB18_15
# BB#6:                                 # %.lr.ph.i
	movq	48(%rbx), %r12
	movl	60(%r12), %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB18_7:                               # =>This Inner Loop Header: Depth=1
	cmpl	$9, 8(%rbp)
	jne	.LBB18_12
# BB#8:                                 #   in Loop: Header=BB18_7 Depth=1
	movl	16(%rsp), %ecx
	cmpl	%ecx, 16(%rbp)
	jne	.LBB18_10
# BB#9:                                 #   in Loop: Header=BB18_7 Depth=1
	movl	%edx, 16(%rbp)
	movl	$1, %eax
	movl	16(%rsp), %ecx
.LBB18_10:                              #   in Loop: Header=BB18_7 Depth=1
	cmpl	%ecx, 20(%rbp)
	jne	.LBB18_12
# BB#11:                                #   in Loop: Header=BB18_7 Depth=1
	movl	%edx, 20(%rbp)
	movl	$1, %eax
.LBB18_12:                              #   in Loop: Header=BB18_7 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB18_7
# BB#13:                                # %._crit_edge.i
	testl	%eax, %eax
	je	.LBB18_15
# BB#14:
	movl	16(%rsp), %ecx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	callq	luaK_codeABC
	movl	$1, %esi
	movq	%r12, %rdi
	callq	luaK_reserveregs
.LBB18_15:                              # %check_conflict.exit
	movq	56(%rbx), %rax
	movzwl	96(%rax), %eax
	movl	$200, %ebp
	subl	%eax, %ebp
	leaq	48(%rbx), %r13
	cmpl	%r15d, %ebp
	movq	%r14, %r12
	jge	.LBB18_20
# BB#16:
	movq	(%r13), %r14
	movq	(%r14), %rax
	movq	32(%r14), %rdi
	movl	96(%rax), %edx
	testl	%edx, %edx
	je	.LBB18_17
# BB#18:
	movl	$.L.str.5, %esi
	movl	$.L.str.27, %r8d
	xorl	%eax, %eax
	movl	%ebp, %ecx
	callq	luaO_pushfstring
	jmp	.LBB18_19
.LBB18_21:
	movq	56(%rbx), %rbp
	movl	$61, %esi
	movq	%rbx, %rdi
	callq	luaX_token2str
	movq	%rax, %rcx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	luaO_pushfstring
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaX_syntaxerror
.LBB18_22:                              # %checknext.exit
	movq	%rbx, %rdi
	callq	luaX_next
	movq	%rsp, %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	subexpr
	leaq	48(%rbx), %r13
	movl	$1, %r14d
	cmpl	$44, 16(%rbx)
	jne	.LBB18_25
# BB#23:                                # %.lr.ph.preheader
	movl	$1, %r14d
	movq	%rsp, %rbp
	.p2align	4, 0x90
.LBB18_24:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	luaX_next
	movq	48(%rbx), %rdi
	movq	%rbp, %rsi
	callq	luaK_exp2nextreg
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	subexpr
	incl	%r14d
	cmpl	$44, 16(%rbx)
	je	.LBB18_24
.LBB18_25:                              # %explist1.exit
	cmpl	%r15d, %r14d
	movq	(%r13), %rbp
	jne	.LBB18_26
# BB#38:                                # %.critedge
	movq	%rsp, %rbx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	luaK_setoneret
	movq	(%r13), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	jmp	.LBB18_37
.LBB18_26:
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movl	%r15d, %r12d
	subl	%r14d, %r12d
	movl	(%rsp), %eax
	leal	-13(%rax), %ecx
	cmpl	$2, %ecx
	jae	.LBB18_27
# BB#31:
	xorl	%eax, %eax
	movl	%r12d, %ebx
	incl	%ebx
	cmovsl	%eax, %ebx
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	movl	%ebx, %edx
	callq	luaK_setreturns
	cmpl	$2, %ebx
	jl	.LBB18_33
# BB#32:
	decl	%ebx
	movq	%rbp, %rdi
	movl	%ebx, %esi
	callq	luaK_reserveregs
	cmpl	%r15d, %r14d
	jg	.LBB18_34
	jmp	.LBB18_35
.LBB18_27:
	testl	%eax, %eax
	je	.LBB18_29
# BB#28:
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	callq	luaK_exp2nextreg
.LBB18_29:
	testl	%r12d, %r12d
	jle	.LBB18_33
# BB#30:
	movl	60(%rbp), %ebx
	movq	%rbp, %rdi
	movl	%r12d, %esi
	callq	luaK_reserveregs
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movl	%r12d, %edx
	callq	luaK_nil
.LBB18_33:                              # %adjust_assign.exit
	cmpl	%r15d, %r14d
	jle	.LBB18_35
.LBB18_34:
	movq	(%r13), %rax
	addl	%r12d, 60(%rax)
.LBB18_35:
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB18_36
.LBB18_17:
	movl	$.L.str.4, %esi
	movl	$.L.str.27, %ecx
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	luaO_pushfstring
.LBB18_19:                              # %errorlimit.exit
	movq	24(%r14), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	luaX_lexerror
.LBB18_20:                              # %check_conflict.exit._crit_edge
	incl	%r15d
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	movl	%r15d, %edx
	callq	assignment
.LBB18_36:
	movq	(%r13), %rdi
	movl	60(%rdi), %eax
	decl	%eax
	movl	$-1, 16(%rsp)
	movl	$-1, 20(%rsp)
	movl	$12, (%rsp)
	movl	%eax, 8(%rsp)
	movq	%rsp, %rdx
	movq	%r12, %rsi
.LBB18_37:
	callq	luaK_storevar
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	assignment, .Lfunc_end18-assignment
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"'%s' expected"
	.size	.L.str, 14

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"chunk has too many syntax levels"
	.size	.L.str.1, 33

	.type	priority,@object        # @priority
	.section	.rodata,"a",@progbits
	.p2align	4
priority:
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	7                       # 0x7
	.byte	10                      # 0xa
	.byte	9                       # 0x9
	.byte	5                       # 0x5
	.byte	4                       # 0x4
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	3                       # 0x3
	.byte	2                       # 0x2
	.byte	2                       # 0x2
	.byte	1                       # 0x1
	.byte	1                       # 0x1
	.size	priority, 30

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"cannot use '...' outside a vararg function"
	.size	.L.str.2, 43

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"items in a constructor"
	.size	.L.str.3, 23

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"main function has more than %d %s"
	.size	.L.str.4, 34

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"function at line %d has more than %d %s"
	.size	.L.str.5, 40

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"self"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"local variables"
	.size	.L.str.7, 16

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"too many local variables"
	.size	.L.str.8, 25

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"arg"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"<name> or '...' expected"
	.size	.L.str.10, 25

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"constant table overflow"
	.size	.L.str.11, 24

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"unexpected symbol"
	.size	.L.str.12, 18

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"upvalues"
	.size	.L.str.13, 9

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.zero	1
	.size	.L.str.14, 1

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"ambiguous syntax (function call x new statement)"
	.size	.L.str.15, 49

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"function arguments expected"
	.size	.L.str.16, 28

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"'%s' expected (to close '%s' at line %d)"
	.size	.L.str.17, 41

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"'=' or 'in' expected"
	.size	.L.str.18, 21

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"(for index)"
	.size	.L.str.19, 12

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"(for limit)"
	.size	.L.str.20, 12

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"(for step)"
	.size	.L.str.21, 11

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"(for generator)"
	.size	.L.str.22, 16

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"(for state)"
	.size	.L.str.23, 12

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"(for control)"
	.size	.L.str.24, 14

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"no loop to break"
	.size	.L.str.25, 17

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"syntax error"
	.size	.L.str.26, 13

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"variables in assignment"
	.size	.L.str.27, 24

	.hidden	luaX_setinput
	.hidden	luaS_newlstr
	.hidden	luaX_next
	.hidden	luaF_newproto
	.hidden	luaH_new
	.hidden	luaD_growstack
	.hidden	luaX_syntaxerror
	.hidden	luaO_pushfstring
	.hidden	luaX_token2str
	.hidden	luaK_ret
	.hidden	luaM_realloc_
	.hidden	luaM_toobig
	.hidden	luaX_newstring
	.hidden	luaX_lexerror
	.hidden	luaK_concat
	.hidden	luaK_jump
	.hidden	luaK_patchtohere
	.hidden	luaK_goiftrue
	.hidden	luaK_prefix
	.hidden	luaK_infix
	.hidden	luaK_posfix
	.hidden	luaK_codeABC
	.hidden	luaK_stringK
	.hidden	luaK_exp2nextreg
	.hidden	luaX_lookahead
	.hidden	luaO_int2fb
	.hidden	luaK_setlist
	.hidden	luaK_exp2RK
	.hidden	luaK_exp2val
	.hidden	luaK_setreturns
	.hidden	luaM_growaux_
	.hidden	luaC_barrierf
	.hidden	luaK_reserveregs
	.hidden	luaK_codeABx
	.hidden	luaK_exp2anyreg
	.hidden	luaK_indexed
	.hidden	luaK_self
	.hidden	luaK_dischargevars
	.hidden	luaK_fixline
	.hidden	luaK_getlabel
	.hidden	luaK_patchlist
	.hidden	luaK_numberK
	.hidden	luaK_checkstack
	.hidden	luaK_nil
	.hidden	luaK_storevar
	.hidden	luaK_setoneret

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
