	.text
	.file	"roadlet.bc"
	.globl	_ZN7roadlet4initEPKc
	.p2align	4, 0x90
	.type	_ZN7roadlet4initEPKc,@function
_ZN7roadlet4initEPKc:                   # @_ZN7roadlet4initEPKc
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 24(%rdi)
	movups	%xmm0, 8(%rdi)
	movq	$0, 72(%rdi)
	movq	%rsi, (%rdi)
	movl	$_Z11return_nullP7roadletP7vehicle9direction, %eax
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	movdqu	%xmm0, 80(%rdi)
	movdqu	%xmm0, 96(%rdi)
	movdqu	%xmm0, 112(%rdi)
	movdqu	%xmm0, 128(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN7roadlet4initEPKc, .Lfunc_end0-_ZN7roadlet4initEPKc
	.cfi_endproc

	.globl	_ZlsRSo7roadlet
	.p2align	4, 0x90
	.type	_ZlsRSo7roadlet,@function
_ZlsRSo7roadlet:                        # @_ZlsRSo7roadlet
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$.L.str, %esi
	movl	$8, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	32(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB1_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB1_3
.LBB1_1:
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	leaq	(%r14,%rax), %rdi
	movl	32(%r14,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB1_3:                                # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZlsRSo7roadlet, .Lfunc_end1-_ZlsRSo7roadlet
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_roadlet.ii,@function
_GLOBAL__sub_I_roadlet.ii:              # @_GLOBAL__sub_I_roadlet.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end2:
	.size	_GLOBAL__sub_I_roadlet.ii, .Lfunc_end2-_GLOBAL__sub_I_roadlet.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"roadlet "
	.size	.L.str, 9

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_roadlet.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
