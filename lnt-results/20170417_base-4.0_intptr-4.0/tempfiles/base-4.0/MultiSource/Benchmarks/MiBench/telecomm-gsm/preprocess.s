	.text
	.file	"preprocess.bc"
	.globl	Gsm_Preprocess
	.p2align	4, 0x90
	.type	Gsm_Preprocess,@function
Gsm_Preprocess:                         # @Gsm_Preprocess
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movw	560(%rdi), %r8w
	movq	568(%rdi), %rcx
	movl	576(%rdi), %r12d
	xorl	%r9d, %r9d
	movq	$-2147483648, %r10      # imm = 0x80000000
	movabsq	$-242064356802560, %r11 # imm = 0xFFFF23D800000000
	movabsq	$140737488355328, %r15  # imm = 0x800000000000
	movl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movw	%r8w, %bp
	movswl	(%rsi,%r9,2), %r8d
	sarl	$3, %r8d
	shll	$2, %r8d
	movl	%r8d, %ebx
	subl	%ebp, %ebx
	movswq	%bx, %rbp
	shlq	$15, %rbp
	movq	%rcx, %rbx
	shrq	$15, %rbx
	movswq	%bx, %rax
	shll	$15, %ebx
	subl	%ebx, %ecx
	movswq	%cx, %rcx
	imulq	$32735, %rcx, %rbx      # imm = 0x7FDF
	addq	$16384, %rbx            # imm = 0x4000
	sarq	$15, %rbx
	addq	%rbp, %rbx
	imulq	$32735, %rax, %rcx      # imm = 0x7FDF
	testq	%rax, %rax
	js	.LBB0_2
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	addq	%rbx, %rcx
	testq	%rbx, %rbx
	jle	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpq	$2147483647, %rcx       # imm = 0x7FFFFFFF
	cmovaeq	%r14, %rcx
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	testq	%rbx, %rbx
	js	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	addq	%rbx, %rcx
	jmp	.LBB0_7
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	notq	%rcx
	notq	%rbx
	addq	%rcx, %rbx
	movq	$-2, %rcx
	subq	%rbx, %rcx
	cmpq	$2147483646, %rbx       # imm = 0x7FFFFFFE
	cmovaq	%r10, %rcx
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_1 Depth=1
	leaq	16384(%rcx), %rax
	movq	%rax, %rbx
	shrq	$15, %rbx
	cmpq	$2147483647, %rax       # imm = 0x7FFFFFFF
	movswq	%r12w, %rax
	movl	$65535, %r12d           # imm = 0xFFFF
	cmovbq	%rbx, %r12
	testq	%rcx, %rcx
	cmovsq	%rbx, %r12
	movswq	%r12w, %rbx
	imulq	%r11, %rax
	addq	%r15, %rax
	sarq	$48, %rax
	leaq	(%rbx,%rax), %rbp
	leaq	32768(%rbx,%rax), %rax
	xorl	%ebx, %ebx
	testq	%rbp, %rbp
	setle	%bl
	addl	$32767, %ebx            # imm = 0x7FFF
	cmpq	$65535, %rax            # imm = 0xFFFF
	cmovbew	%bp, %bx
	movw	%bx, (%rdx,%r9,2)
	incq	%r9
	cmpl	$160, %r9d
	jne	.LBB0_1
# BB#8:
	movw	%r8w, 560(%rdi)
	movq	%rcx, 568(%rdi)
	movswl	%r12w, %eax
	movl	%eax, 576(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Gsm_Preprocess, .Lfunc_end0-Gsm_Preprocess
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
