	.text
	.file	"smallpt.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4906019910204099648     # double 1.0E+20
.LCPI0_1:
	.quad	4547007122018943789     # double 1.0E-4
.LCPI0_2:
	.quad	4607182418800017408     # double 1
.LCPI0_4:
	.quad	4618760256179416344     # double 6.2831853071795862
.LCPI0_6:
	.quad	4591870180066957722     # double 0.10000000000000001
.LCPI0_9:
	.quad	4606822130829827768     # double 0.95999999999999996
.LCPI0_10:
	.quad	4585925428558828667     # double 0.040000000000000001
.LCPI0_11:
	.quad	4602678819172646912     # double 0.5
.LCPI0_12:
	.quad	4598175219545276416     # double 0.25
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_3:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI0_5:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI0_7:
	.quad	4609434218613702656     # double 1.5
	.quad	4604180019048437077     # double 0.66666666666666663
.LCPI0_8:
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.text
	.globl	_Z8radianceRK3RayiPt
	.p2align	4, 0x90
	.type	_Z8radianceRK3RayiPt,@function
_Z8radianceRK3RayiPt:                   # @_Z8radianceRK3RayiPt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$520, %rsp              # imm = 0x208
.Lcfi6:
	.cfi_def_cfa_offset 576
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movl	%edx, %r13d
	movq	%rsi, %rbp
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movsd	.LCPI0_0(%rip), %xmm14  # xmm14 = mem[0],zero
	xorl	%r15d, %r15d
	movl	$8, %ebx
	movl	$792, %r14d             # imm = 0x318
	xorpd	%xmm5, %xmm5
	movsd	.LCPI0_1(%rip), %xmm7   # xmm7 = mem[0],zero
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movsd	spheres-80(%r14), %xmm0 # xmm0 = mem[0],zero
	subsd	(%rbp), %xmm0
	movsd	spheres-72(%r14), %xmm1 # xmm1 = mem[0],zero
	subsd	8(%rbp), %xmm1
	movsd	spheres-64(%r14), %xmm2 # xmm2 = mem[0],zero
	subsd	16(%rbp), %xmm2
	movsd	24(%rbp), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	movsd	32(%rbp), %xmm4         # xmm4 = mem[0],zero
	mulsd	%xmm1, %xmm4
	addsd	%xmm3, %xmm4
	movsd	40(%rbp), %xmm6         # xmm6 = mem[0],zero
	mulsd	%xmm2, %xmm6
	addsd	%xmm4, %xmm6
	movapd	%xmm6, %xmm3
	mulsd	%xmm3, %xmm3
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm3
	movsd	spheres-88(%r14), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm5
	ja	.LBB0_9
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_4
# BB#3:                                 # %call.sqrt
                                        #   in Loop: Header=BB0_1 Depth=1
	movapd	%xmm1, %xmm0
	movapd	%xmm14, 160(%rsp)       # 16-byte Spill
	movapd	%xmm6, 128(%rsp)        # 16-byte Spill
	callq	sqrt
	movapd	128(%rsp), %xmm6        # 16-byte Reload
	movsd	.LCPI0_1(%rip), %xmm7   # xmm7 = mem[0],zero
	xorpd	%xmm5, %xmm5
	movapd	160(%rsp), %xmm14       # 16-byte Reload
.LBB0_4:                                # %.split
                                        #   in Loop: Header=BB0_1 Depth=1
	movapd	%xmm6, %xmm1
	subsd	%xmm0, %xmm1
	ucomisd	%xmm7, %xmm1
	ja	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_1 Depth=1
	addsd	%xmm0, %xmm6
	movapd	%xmm7, %xmm1
	cmpltsd	%xmm6, %xmm1
	andpd	%xmm6, %xmm1
.LBB0_6:                                # %_ZNK6Sphere9intersectERK3Ray.exit.i
                                        #   in Loop: Header=BB0_1 Depth=1
	ucomisd	%xmm5, %xmm1
	setp	%al
	setne	%cl
	orb	%al, %cl
	ucomisd	%xmm1, %xmm14
	seta	%al
	andb	%cl, %al
	jne	.LBB0_8
# BB#7:                                 # %_ZNK6Sphere9intersectERK3Ray.exit.i
                                        #   in Loop: Header=BB0_1 Depth=1
	movapd	%xmm14, %xmm1
.LBB0_8:                                # %_ZNK6Sphere9intersectERK3Ray.exit.i
                                        #   in Loop: Header=BB0_1 Depth=1
	testb	%al, %al
	cmovnel	%ebx, %r15d
	movapd	%xmm1, %xmm14
.LBB0_9:                                # %_ZNK6Sphere9intersectERK3Ray.exit.thread.backedge.i
                                        #   in Loop: Header=BB0_1 Depth=1
	decl	%ebx
	addq	$-88, %r14
	jne	.LBB0_1
# BB#10:                                # %_Z9intersectRK3RayRdRi.exit
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm14, %xmm0
	jbe	.LBB0_11
# BB#12:
	movslq	%r15d, %rax
	movupd	(%rbp), %xmm0
	movupd	24(%rbp), %xmm1
	movapd	%xmm14, %xmm8
	movlhps	%xmm8, %xmm8            # xmm8 = xmm8[0,0]
	mulpd	%xmm1, %xmm8
	mulsd	40(%rbp), %xmm14
	addpd	%xmm0, %xmm8
	addsd	16(%rbp), %xmm14
	imulq	$88, %rax, %rbx
	movupd	spheres+8(%rbx), %xmm0
	movapd	%xmm8, %xmm5
	subpd	%xmm0, %xmm5
	movapd	%xmm14, %xmm12
	subsd	spheres+24(%rbx), %xmm12
	movapd	%xmm5, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm5, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm2
	movapd	%xmm12, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	movapd	%xmm14, 160(%rsp)       # 16-byte Spill
	movapd	%xmm8, 192(%rsp)        # 16-byte Spill
	jnp	.LBB0_14
# BB#13:                                # %call.sqrt570
	movapd	%xmm1, %xmm0
	movapd	%xmm12, (%rsp)          # 16-byte Spill
	movapd	%xmm5, 32(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	32(%rsp), %xmm5         # 16-byte Reload
	movapd	(%rsp), %xmm12          # 16-byte Reload
	movapd	192(%rsp), %xmm8        # 16-byte Reload
	movapd	160(%rsp), %xmm14       # 16-byte Reload
.LBB0_14:                               # %.split569
	movsd	.LCPI0_2(%rip), %xmm9   # xmm9 = mem[0],zero
	movapd	%xmm9, %xmm1
	divsd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm12
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm1, %xmm5
	movsd	24(%rbp), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm5, %xmm0
	movapd	%xmm5, %xmm13
	movhlps	%xmm13, %xmm13          # xmm13 = xmm13[1,1]
	movsd	32(%rbp), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm13, %xmm1
	addsd	%xmm0, %xmm1
	movsd	40(%rbp), %xmm7         # xmm7 = mem[0],zero
	mulsd	%xmm12, %xmm7
	addsd	%xmm1, %xmm7
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm7, %xmm0
	movapd	.LCPI0_3(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00]
	movapd	%xmm5, %xmm1
	ja	.LBB0_16
# BB#15:                                # %.split569
	movapd	%xmm5, %xmm1
	xorpd	%xmm4, %xmm1
.LBB0_16:                               # %.split569
	movups	spheres+56(%rbx), %xmm3
	movsd	spheres+72(%rbx), %xmm2 # xmm2 = mem[0],zero
	cmpl	$5, %r13d
	jl	.LBB0_21
# BB#17:
	movaps	%xmm13, 112(%rsp)       # 16-byte Spill
	movapd	%xmm5, 32(%rsp)         # 16-byte Spill
	movapd	%xmm12, (%rsp)          # 16-byte Spill
	movapd	%xmm1, 64(%rsp)         # 16-byte Spill
	movapd	%xmm7, 16(%rsp)         # 16-byte Spill
	movaps	%xmm3, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movaps	%xmm0, %xmm1
	maxsd	%xmm2, %xmm1
	cmpltsd	%xmm3, %xmm0
	movapd	%xmm2, %xmm4
	cmpltsd	%xmm3, %xmm4
	movsd	%xmm2, 128(%rsp)        # 8-byte Spill
	movapd	%xmm4, %xmm2
	andnpd	%xmm1, %xmm2
	andpd	%xmm3, %xmm4
	orpd	%xmm2, %xmm4
	andpd	%xmm0, %xmm4
	andnpd	%xmm1, %xmm0
	orpd	%xmm0, %xmm4
	movapd	%xmm4, 80(%rsp)         # 16-byte Spill
	movq	%r12, %rdi
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	callq	erand48
	movapd	80(%rsp), %xmm2         # 16-byte Reload
	ucomisd	%xmm0, %xmm2
	jbe	.LBB0_18
# BB#19:
	movapd	176(%rsp), %xmm3        # 16-byte Reload
	movsd	128(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cmpl	$128, %r13d
	movapd	160(%rsp), %xmm14       # 16-byte Reload
	movapd	192(%rsp), %xmm8        # 16-byte Reload
	jl	.LBB0_20
.LBB0_18:
	movq	spheres+48(%rbx), %rcx
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 16(%rax)
	movupd	spheres+32(%rbx), %xmm0
	movupd	%xmm0, (%rax)
	jmp	.LBB0_50
.LBB0_11:
	xorpd	%xmm0, %xmm0
	movq	56(%rsp), %rax          # 8-byte Reload
	movupd	%xmm0, (%rax)
	movq	$0, 16(%rax)
	jmp	.LBB0_50
.LBB0_20:
	movsd	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm2, %xmm0
	movapd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm0, %xmm3
	movapd	16(%rsp), %xmm7         # 16-byte Reload
	movsd	.LCPI0_2(%rip), %xmm9   # xmm9 = mem[0],zero
	movapd	64(%rsp), %xmm1         # 16-byte Reload
	movapd	(%rsp), %xmm12          # 16-byte Reload
	movapd	32(%rsp), %xmm5         # 16-byte Reload
	movaps	112(%rsp), %xmm13       # 16-byte Reload
	movapd	.LCPI0_3(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm0, %xmm0
.LBB0_21:                               # %.thread
	leal	1(%r13), %r15d
	movl	spheres+80(%rbx), %eax
	cmpl	$1, %eax
	movsd	%xmm2, 128(%rsp)        # 8-byte Spill
	movapd	%xmm3, 176(%rsp)        # 16-byte Spill
	je	.LBB0_35
# BB#22:                                # %.thread
	xorpd	%xmm12, %xmm4
	cmpltsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm0
	andpd	%xmm12, %xmm0
	andnpd	%xmm4, %xmm7
	orpd	%xmm0, %xmm7
	testl	%eax, %eax
	jne	.LBB0_36
# BB#23:
	movapd	%xmm1, 64(%rsp)         # 16-byte Spill
	movapd	%xmm7, 16(%rsp)         # 16-byte Spill
	movq	%r12, %rdi
	callq	erand48
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%r12, %rdi
	callq	erand48
	movsd	%xmm0, 256(%rsp)        # 8-byte Spill
	sqrtsd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_25
# BB#24:                                # %call.sqrt572
	movsd	256(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sqrt
.LBB0_25:                               # %.split571
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
	movapd	.LCPI0_5(%rip), %xmm1   # xmm1 = [nan,nan]
	movapd	64(%rsp), %xmm9         # 16-byte Reload
	andpd	%xmm9, %xmm1
	movsd	.LCPI0_6(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	cmpltsd	%xmm1, %xmm0
	movq	.LCPI0_2(%rip), %xmm3   # xmm3 = mem[0],zero
	pandn	%xmm3, %xmm0
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	ja	.LBB0_27
# BB#26:                                # %.split571
	pxor	%xmm3, %xmm3
.LBB0_27:                               # %.split571
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	mulsd	.LCPI0_4(%rip), %xmm8
	movapd	%xmm2, %xmm10
	unpcklpd	%xmm9, %xmm10   # xmm10 = xmm10[0],xmm9[0]
	movq	%xmm3, %xmm5            # xmm5 = xmm3[0],zero
	mulpd	%xmm10, %xmm5
	movdqa	%xmm0, %xmm1
	pslldq	$8, %xmm1               # xmm1 = zero,zero,zero,zero,zero,zero,zero,zero,xmm1[0,1,2,3,4,5,6,7]
	movapd	%xmm9, %xmm7
	movhlps	%xmm7, %xmm7            # xmm7 = xmm7[1,1]
	movaps	%xmm7, %xmm6
	unpcklpd	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0]
	mulpd	%xmm6, %xmm1
	subpd	%xmm1, %xmm5
	movaps	%xmm7, %xmm4
	mulsd	%xmm0, %xmm4
	mulsd	%xmm9, %xmm3
	subsd	%xmm3, %xmm4
	movapd	%xmm5, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm5, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm2
	movapd	%xmm4, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	movsd	%xmm8, (%rsp)           # 8-byte Spill
	jnp	.LBB0_29
# BB#28:                                # %call.sqrt573
	movapd	%xmm1, %xmm0
	movapd	%xmm6, 112(%rsp)        # 16-byte Spill
	movapd	%xmm10, 32(%rsp)        # 16-byte Spill
	movaps	%xmm7, 96(%rsp)         # 16-byte Spill
	movapd	%xmm5, 144(%rsp)        # 16-byte Spill
	movapd	%xmm4, 320(%rsp)        # 16-byte Spill
	callq	sqrt
	movapd	320(%rsp), %xmm4        # 16-byte Reload
	movapd	144(%rsp), %xmm5        # 16-byte Reload
	movaps	96(%rsp), %xmm7         # 16-byte Reload
	movapd	32(%rsp), %xmm10        # 16-byte Reload
	movapd	112(%rsp), %xmm6        # 16-byte Reload
	movsd	(%rsp), %xmm8           # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movapd	64(%rsp), %xmm9         # 16-byte Reload
.LBB0_29:                               # %.split571.split
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm5, %xmm2
	mulsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm0
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	unpcklpd	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0]
	mulpd	%xmm0, %xmm6
	movapd	%xmm2, %xmm0
	movapd	%xmm2, 144(%rsp)        # 16-byte Spill
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	mulsd	%xmm0, %xmm9
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	mulpd	%xmm0, %xmm10
	subpd	%xmm10, %xmm6
	movapd	%xmm6, 112(%rsp)        # 16-byte Spill
	mulsd	%xmm2, %xmm7
	subsd	%xmm7, %xmm9
	movsd	%xmm9, 96(%rsp)         # 8-byte Spill
	movapd	%xmm8, %xmm0
	callq	cos
	movapd	32(%rsp), %xmm1         # 16-byte Reload
	mulsd	%xmm0, %xmm1
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	144(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	movaps	%xmm3, 320(%rsp)        # 16-byte Spill
	mulpd	%xmm3, %xmm0
	movapd	%xmm0, 144(%rsp)        # 16-byte Spill
	mulsd	%xmm2, %xmm1
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sin
	movsd	96(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	mulsd	%xmm0, %xmm4
	movapd	%xmm0, %xmm5
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	mulpd	112(%rsp), %xmm5        # 16-byte Folded Reload
	mulpd	320(%rsp), %xmm5        # 16-byte Folded Reload
	mulsd	80(%rsp), %xmm4         # 16-byte Folded Reload
	addpd	144(%rsp), %xmm5        # 16-byte Folded Reload
	addsd	32(%rsp), %xmm4         # 16-byte Folded Reload
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	256(%rsp), %xmm1        # 8-byte Folded Reload
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_31
# BB#30:                                # %call.sqrt574
	movapd	%xmm1, %xmm0
	movsd	%xmm4, 96(%rsp)         # 8-byte Spill
	movapd	%xmm5, (%rsp)           # 16-byte Spill
	callq	sqrt
	movapd	(%rsp), %xmm5           # 16-byte Reload
	movsd	96(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
.LBB0_31:                               # %.split571.split.split
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	mulsd	%xmm0, %xmm2
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movapd	64(%rsp), %xmm3         # 16-byte Reload
	mulpd	%xmm0, %xmm3
	addpd	%xmm5, %xmm3
	addsd	%xmm4, %xmm2
	movapd	%xmm3, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm3, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_33
# BB#32:                                # %call.sqrt575
	movapd	%xmm2, 16(%rsp)         # 16-byte Spill
	movapd	%xmm3, 64(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	64(%rsp), %xmm3         # 16-byte Reload
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	movapd	%xmm0, %xmm1
.LBB0_33:                               # %.split571.split.split.split
	movsd	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero
	movapd	%xmm0, %xmm4
	divsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm3, %xmm0
	mulsd	%xmm2, %xmm4
	movaps	192(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm1, 208(%rsp)
	movapd	160(%rsp), %xmm1        # 16-byte Reload
	movsd	%xmm1, 224(%rsp)
	movupd	%xmm0, 232(%rsp)
	movsd	%xmm4, 248(%rsp)
	jmp	.LBB0_34
.LBB0_35:
	movapd	%xmm5, %xmm0
	addpd	%xmm0, %xmm0
	movapd	%xmm12, %xmm1
	addsd	%xmm1, %xmm1
	movupd	24(%rbp), %xmm2
	mulsd	%xmm2, %xmm5
	movapd	%xmm2, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	mulsd	%xmm3, %xmm13
	addsd	%xmm5, %xmm13
	movsd	40(%rbp), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm3, %xmm12
	addsd	%xmm13, %xmm12
	mulsd	%xmm12, %xmm1
	movlhps	%xmm12, %xmm12          # xmm12 = xmm12[0,0]
	mulpd	%xmm0, %xmm12
	subpd	%xmm12, %xmm2
	subsd	%xmm1, %xmm3
	movapd	%xmm8, 208(%rsp)
	movsd	%xmm14, 224(%rsp)
	movupd	%xmm2, 232(%rsp)
	movsd	%xmm3, 248(%rsp)
	jmp	.LBB0_34
.LBB0_36:
	movapd	%xmm5, %xmm2
	addpd	%xmm2, %xmm2
	movapd	%xmm12, %xmm3
	addsd	%xmm3, %xmm3
	movupd	24(%rbp), %xmm10
	movapd	%xmm5, %xmm4
	mulsd	%xmm10, %xmm4
	movapd	%xmm10, %xmm11
	movhlps	%xmm11, %xmm11          # xmm11 = xmm11[1,1]
	movaps	%xmm13, %xmm0
	mulsd	%xmm11, %xmm0
	addsd	%xmm4, %xmm0
	movsd	40(%rbp), %xmm6         # xmm6 = mem[0],zero
	movapd	%xmm12, %xmm4
	mulsd	%xmm6, %xmm4
	addsd	%xmm0, %xmm4
	mulsd	%xmm4, %xmm3
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm2, %xmm4
	movapd	%xmm10, %xmm2
	subpd	%xmm4, %xmm2
	movapd	%xmm6, %xmm4
	subsd	%xmm3, %xmm4
	movapd	%xmm8, 208(%rsp)
	movsd	%xmm14, 224(%rsp)
	movupd	%xmm2, 232(%rsp)
	movsd	%xmm4, 248(%rsp)
	movapd	%xmm5, %xmm2
	mulsd	%xmm1, %xmm2
	movapd	%xmm1, %xmm3
	movhlps	%xmm3, %xmm3            # xmm3 = xmm3[1,1]
	movaps	%xmm13, %xmm4
	mulsd	%xmm3, %xmm4
	addsd	%xmm2, %xmm4
	movapd	%xmm12, %xmm15
	mulsd	%xmm7, %xmm15
	addsd	%xmm4, %xmm15
	xorpd	%xmm0, %xmm0
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm15
	seta	%al
	movsd	.LCPI0_7(,%rax,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	%xmm10, %xmm1
	mulsd	%xmm11, %xmm3
	addsd	%xmm1, %xmm3
	xorpd	%xmm1, %xmm1
	mulsd	%xmm6, %xmm7
	addsd	%xmm3, %xmm7
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm7, %xmm3
	mulsd	%xmm3, %xmm3
	movapd	%xmm9, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movapd	%xmm9, %xmm0
	subsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm1
	jbe	.LBB0_37
.LBB0_34:
	leaq	272(%rsp), %rdi
	leaq	208(%rsp), %rsi
	movl	%r15d, %edx
	movq	%r12, %rcx
	callq	_Z8radianceRK3RayiPt
	movapd	176(%rsp), %xmm2        # 16-byte Reload
	mulpd	272(%rsp), %xmm2
	movsd	128(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	288(%rsp), %xmm1
	movupd	spheres+32(%rbx), %xmm0
	addpd	%xmm2, %xmm0
	addsd	spheres+48(%rbx), %xmm1
	movq	56(%rsp), %rax          # 8-byte Reload
	movupd	%xmm0, (%rax)
	movsd	%xmm1, 16(%rax)
.LBB0_50:
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_37:
	xorl	%eax, %eax
	ucomisd	%xmm1, %xmm15
	seta	%al
	movapd	%xmm2, %xmm4
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	mulpd	%xmm10, %xmm4
	mulsd	%xmm2, %xmm6
	movsd	.LCPI0_8(,%rax,8), %xmm3 # xmm3 = mem[0],zero
	mulsd	%xmm7, %xmm2
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_39
# BB#38:                                # %call.sqrt577
	movapd	%xmm7, 16(%rsp)         # 16-byte Spill
	movapd	%xmm12, (%rsp)          # 16-byte Spill
	movapd	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm13, 112(%rsp)       # 16-byte Spill
	movsd	%xmm6, 64(%rsp)         # 8-byte Spill
	movapd	%xmm15, 80(%rsp)        # 16-byte Spill
	movapd	%xmm4, 96(%rsp)         # 16-byte Spill
	movapd	%xmm2, 256(%rsp)        # 16-byte Spill
	movapd	%xmm3, 144(%rsp)        # 16-byte Spill
	callq	sqrt
	movapd	144(%rsp), %xmm3        # 16-byte Reload
	movapd	256(%rsp), %xmm2        # 16-byte Reload
	movapd	96(%rsp), %xmm4         # 16-byte Reload
	movapd	80(%rsp), %xmm15        # 16-byte Reload
	movsd	64(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movaps	112(%rsp), %xmm13       # 16-byte Reload
	movapd	32(%rsp), %xmm5         # 16-byte Reload
	movapd	(%rsp), %xmm12          # 16-byte Reload
	movsd	.LCPI0_2(%rip), %xmm9   # xmm9 = mem[0],zero
	movapd	16(%rsp), %xmm7         # 16-byte Reload
	movapd	%xmm0, %xmm1
.LBB0_39:                               # %.split576
	addsd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm3
	movapd	%xmm12, %xmm0
	mulsd	%xmm3, %xmm0
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	movapd	%xmm5, %xmm1
	mulpd	%xmm3, %xmm1
	subpd	%xmm1, %xmm4
	subsd	%xmm0, %xmm6
	movapd	%xmm4, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm4, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm6, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB0_41
# BB#40:                                # %call.sqrt578
	movapd	%xmm7, 16(%rsp)         # 16-byte Spill
	movapd	%xmm12, (%rsp)          # 16-byte Spill
	movapd	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm13, 112(%rsp)       # 16-byte Spill
	movsd	%xmm6, 64(%rsp)         # 8-byte Spill
	movapd	%xmm15, 80(%rsp)        # 16-byte Spill
	movapd	%xmm4, 96(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	96(%rsp), %xmm4         # 16-byte Reload
	movapd	80(%rsp), %xmm15        # 16-byte Reload
	movsd	64(%rsp), %xmm6         # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movaps	112(%rsp), %xmm13       # 16-byte Reload
	movapd	32(%rsp), %xmm5         # 16-byte Reload
	movapd	(%rsp), %xmm12          # 16-byte Reload
	movsd	.LCPI0_2(%rip), %xmm9   # xmm9 = mem[0],zero
	movapd	16(%rsp), %xmm7         # 16-byte Reload
	movapd	%xmm0, %xmm1
.LBB0_41:                               # %.split576.split
	movapd	%xmm9, %xmm3
	divsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm4, %xmm1
	mulsd	%xmm6, %xmm3
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm15
	jbe	.LBB0_43
# BB#42:
	xorpd	.LCPI0_3(%rip), %xmm7
	jmp	.LBB0_44
.LBB0_43:
	mulsd	%xmm1, %xmm5
	movapd	%xmm1, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	mulsd	%xmm0, %xmm13
	addsd	%xmm5, %xmm13
	mulsd	%xmm3, %xmm12
	addsd	%xmm13, %xmm12
	movapd	%xmm12, %xmm7
.LBB0_44:
	movapd	%xmm1, 64(%rsp)         # 16-byte Spill
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	movapd	%xmm9, %xmm0
	subsd	%xmm7, %xmm0
	movsd	.LCPI0_9(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	.LCPI0_10(%rip), %xmm1
	subsd	%xmm1, %xmm9
	cmpl	$2, %r13d
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movapd	%xmm9, (%rsp)           # 16-byte Spill
	jl	.LBB0_48
# BB#45:
	movsd	.LCPI0_11(%rip), %xmm0  # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	addsd	.LCPI0_12(%rip), %xmm0
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	callq	erand48
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB0_47
# BB#46:
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	divsd	%xmm1, %xmm0
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	leaq	432(%rsp), %rdi
	leaq	208(%rsp), %rsi
	movl	%r15d, %edx
	movq	%r12, %rcx
	callq	_Z8radianceRK3RayiPt
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	432(%rsp), %xmm0
	mulsd	448(%rsp), %xmm3
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.LBB0_49
.LBB0_48:
	leaq	368(%rsp), %rdi
	leaq	208(%rsp), %rsi
	movl	%r15d, %edx
	movq	%r12, %rcx
	callq	_Z8radianceRK3RayiPt
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	368(%rsp), %xmm0
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	mulsd	384(%rsp), %xmm0
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, 464(%rsp)
	movapd	160(%rsp), %xmm0        # 16-byte Reload
	movsd	%xmm0, 480(%rsp)
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	movups	%xmm0, 488(%rsp)
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	movsd	%xmm0, 504(%rsp)
	leaq	336(%rsp), %rdi
	leaq	464(%rsp), %rsi
	movl	%r15d, %edx
	movq	%r12, %rcx
	callq	_Z8radianceRK3RayiPt
	movapd	16(%rsp), %xmm3         # 16-byte Reload
	movaps	(%rsp), %xmm1           # 16-byte Reload
	movaps	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	336(%rsp), %xmm0
	mulsd	352(%rsp), %xmm1
	addpd	80(%rsp), %xmm0         # 16-byte Folded Reload
	addsd	%xmm1, %xmm3
	movb	$1, %cl
	xorl	%edx, %edx
	jmp	.LBB0_49
.LBB0_47:
	movsd	.LCPI0_2(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movapd	(%rsp), %xmm1           # 16-byte Reload
	divsd	%xmm0, %xmm1
	movapd	%xmm1, (%rsp)           # 16-byte Spill
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, 272(%rsp)
	movapd	160(%rsp), %xmm0        # 16-byte Reload
	movsd	%xmm0, 288(%rsp)
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	movups	%xmm0, 296(%rsp)
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	movsd	%xmm0, 312(%rsp)
	leaq	400(%rsp), %rdi
	leaq	272(%rsp), %rsi
	movl	%r15d, %edx
	movq	%r12, %rcx
	callq	_Z8radianceRK3RayiPt
	movaps	(%rsp), %xmm1           # 16-byte Reload
	movaps	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	400(%rsp), %xmm0
	mulsd	416(%rsp), %xmm1
	movb	$1, %dl
	xorl	%ecx, %ecx
	movapd	%xmm1, %xmm3
.LBB0_49:
	movapd	176(%rsp), %xmm2        # 16-byte Reload
	mulpd	%xmm0, %xmm2
	movsd	128(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm3, %xmm1
	movupd	spheres+32(%rbx), %xmm0
	addpd	%xmm2, %xmm0
	addsd	spheres+48(%rbx), %xmm1
	movq	56(%rsp), %rax          # 8-byte Reload
	movupd	%xmm0, (%rax)
	movsd	%xmm1, 16(%rax)
	testb	%cl, %cl
	testb	%dl, %dl
	jmp	.LBB0_50
.Lfunc_end0:
	.size	_Z8radianceRK3RayiPt, .Lfunc_end0-_Z8radianceRK3RayiPt
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
.LCPI1_1:
	.quad	4602678819172646912     # double 0.5
.LCPI1_2:
	.quad	4611686018427387904     # double 2
.LCPI1_3:
	.quad	4562146422526312448     # double 9.765625E-4
.LCPI1_4:
	.quad	-4620693217682128896    # double -0.5
.LCPI1_5:
	.quad	4604342148635022415     # double 0.68466666666666665
.LCPI1_6:
	.quad	4649966615260037120     # double 768
.LCPI1_7:
	.quad	4602796222896717441     # double 0.513034431763984
.LCPI1_8:
	.quad	-4641413689212380515    # double -0.021861423206326881
.LCPI1_9:
	.quad	-4637075747236730785    # double -0.042573365542992951
.LCPI1_10:
	.quad	-4616197784492671692    # double -0.99909334325994914
.LCPI1_12:
	.quad	4639129828656676864     # double 140
.LCPI1_14:
	.quad	4643907866386340250     # double 295.60000000000002
.LCPI1_15:
	.quad	4598175219545276416     # double 0.25
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_11:
	.quad	4639129828656676864     # double 140
	.quad	4639129828656676864     # double 140
.LCPI1_13:
	.quad	4632233691727265792     # double 50
	.quad	4632515166703976448     # double 52
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi19:
	.cfi_def_cfa_offset 416
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	$1, %ebp
	cmpl	$2, %edi
	jne	.LBB1_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, %ebp
	sarl	$31, %ebp
	shrl	$30, %ebp
	addl	%eax, %ebp
	sarl	$2, %ebp
.LBB1_2:
	movl	$18874368, %edi         # imm = 0x1200000
	callq	_Znam
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$18874368, %edx         # imm = 0x1200000
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	callq	memset
	movq	stderr(%rip), %rdi
	leal	(,%rbp,4), %edx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
	cvtsi2sdl	%ebp, %xmm0
	movsd	.LCPI1_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	divsd	%xmm0, %xmm2
	movapd	%xmm2, 224(%rsp)        # 16-byte Spill
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movaps	%xmm2, 208(%rsp)        # 16-byte Spill
	leaq	6(%rsp), %rbx
	movq	.LCPI1_7(%rip), %xmm0   # xmm0 = mem[0],zero
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	%xmm0, 192(%rsp)        # 16-byte Spill
	movq	.LCPI1_9(%rip), %xmm0   # xmm0 = mem[0],zero
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movdqa	%xmm0, 176(%rsp)        # 16-byte Spill
	leaq	272(%rsp), %r15
	leaq	304(%rsp), %r12
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #       Child Loop BB1_5 Depth 3
                                        #         Child Loop BB1_6 Depth 4
                                        #           Child Loop BB1_13 Depth 5
	movw	$0, 6(%rsp)
	movw	$0, 8(%rsp)
	movl	%r14d, %eax
	imull	%eax, %eax
	imull	%r14d, %eax
	movw	%ax, 10(%rsp)
	movl	$768, %eax              # imm = 0x300
	subq	%r14, %rax
	shlq	$10, %rax
	addq	$-1024, %rax            # imm = 0xFC00
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	movsd	%xmm0, 96(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_5 Depth 3
                                        #         Child Loop BB1_6 Depth 4
                                        #           Child Loop BB1_13 Depth 5
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx), %rax
	leaq	(%rax,%rax,2), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movzwl	%dx, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 104(%rsp)        # 8-byte Spill
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_5:                                # %.preheader246
                                        #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_6 Depth 4
                                        #           Child Loop BB1_13 Depth 5
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	addsd	.LCPI1_1(%rip), %xmm0
	movsd	%xmm0, 112(%rsp)        # 8-byte Spill
	xorps	%xmm2, %xmm2
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader
                                        #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        #       Parent Loop BB1_5 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB1_13 Depth 5
	testl	%ebp, %ebp
	movsd	%xmm2, 88(%rsp)         # 8-byte Spill
	jle	.LBB1_7
# BB#12:                                # %.lr.ph
                                        #   in Loop: Header=BB1_6 Depth=4
	addsd	.LCPI1_1(%rip), %xmm2
	movsd	%xmm2, 120(%rsp)        # 8-byte Spill
	xorpd	%xmm7, %xmm7
	xorpd	%xmm6, %xmm6
	movl	%ebp, %r13d
	.p2align	4, 0x90
.LBB1_13:                               #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        #       Parent Loop BB1_5 Depth=3
                                        #         Parent Loop BB1_6 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movapd	%xmm7, 240(%rsp)        # 16-byte Spill
	movapd	%xmm6, 256(%rsp)        # 16-byte Spill
	movq	%rbx, %rdi
	callq	erand48
	movapd	%xmm0, %xmm1
	addsd	%xmm1, %xmm1
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movapd	%xmm0, %xmm3
	jbe	.LBB1_16
# BB#14:                                #   in Loop: Header=BB1_13 Depth=5
	sqrtsd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm4
	movapd	%xmm3, %xmm0
	jnp	.LBB1_18
# BB#15:                                # %call.sqrt
                                        #   in Loop: Header=BB1_13 Depth=5
	movapd	%xmm1, %xmm0
	callq	sqrt
	movapd	%xmm0, %xmm4
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	jmp	.LBB1_18
	.p2align	4, 0x90
.LBB1_16:                               #   in Loop: Header=BB1_13 Depth=5
	movsd	.LCPI1_2(%rip), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm0
	movapd	%xmm3, %xmm4
	jnp	.LBB1_18
# BB#17:                                # %call.sqrt278
                                        #   in Loop: Header=BB1_13 Depth=5
	movapd	%xmm2, %xmm0
	callq	sqrt
	movsd	.LCPI1_0(%rip), %xmm4   # xmm4 = mem[0],zero
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_13 Depth=5
	subsd	%xmm0, %xmm4
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	movq	%rbx, %rdi
	callq	erand48
	movapd	%xmm0, %xmm2
	addsd	%xmm2, %xmm2
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	movapd	%xmm0, %xmm4
	jbe	.LBB1_21
# BB#19:                                #   in Loop: Header=BB1_13 Depth=5
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm2, %xmm0
	ucomisd	%xmm0, %xmm0
	movapd	%xmm4, %xmm1
	jnp	.LBB1_23
# BB#20:                                # %call.sqrt280
                                        #   in Loop: Header=BB1_13 Depth=5
	movapd	%xmm2, %xmm0
	callq	sqrt
	movsd	.LCPI1_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	%xmm4, %xmm1
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_21:                               #   in Loop: Header=BB1_13 Depth=5
	movsd	.LCPI1_2(%rip), %xmm3   # xmm3 = mem[0],zero
	subsd	%xmm2, %xmm3
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm1
	movapd	%xmm4, %xmm0
	jnp	.LBB1_23
# BB#22:                                # %call.sqrt282
                                        #   in Loop: Header=BB1_13 Depth=5
	movapd	%xmm3, %xmm0
	callq	sqrt
	movsd	.LCPI1_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm1
	movapd	%xmm4, %xmm0
	.p2align	4, 0x90
.LBB1_23:                               #   in Loop: Header=BB1_13 Depth=5
	subsd	%xmm1, %xmm0
	movapd	16(%rsp), %xmm5         # 16-byte Reload
	addsd	120(%rsp), %xmm5        # 8-byte Folded Reload
	movsd	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm5
	addsd	104(%rsp), %xmm5        # 8-byte Folded Reload
	mulsd	.LCPI1_3(%rip), %xmm5
	movsd	.LCPI1_4(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm5
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	movsd	.LCPI1_5(%rip), %xmm3   # xmm3 = mem[0],zero
	mulpd	%xmm3, %xmm5
	addsd	112(%rsp), %xmm0        # 8-byte Folded Reload
	mulsd	%xmm1, %xmm0
	addsd	96(%rsp), %xmm0         # 8-byte Folded Reload
	divsd	.LCPI1_6(%rip), %xmm0
	addsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	192(%rsp), %xmm2        # 16-byte Folded Reload
	mulsd	.LCPI1_8(%rip), %xmm0
	addpd	%xmm5, %xmm2
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	addsd	%xmm0, %xmm5
	addpd	176(%rsp), %xmm2        # 16-byte Folded Reload
	addsd	.LCPI1_10(%rip), %xmm5
	movapd	%xmm2, %xmm3
	mulpd	.LCPI1_11(%rip), %xmm3
	movapd	%xmm5, %xmm6
	mulsd	.LCPI1_12(%rip), %xmm6
	addpd	.LCPI1_13(%rip), %xmm3
	addsd	.LCPI1_14(%rip), %xmm6
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm2, %xmm1
	movhlps	%xmm1, %xmm1            # xmm1 = xmm1[1,1]
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm5, %xmm0
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB1_25
# BB#24:                                # %call.sqrt284
                                        #   in Loop: Header=BB1_13 Depth=5
	movapd	%xmm5, 16(%rsp)         # 16-byte Spill
	movapd	%xmm2, 160(%rsp)        # 16-byte Spill
	movapd	%xmm3, 144(%rsp)        # 16-byte Spill
	movapd	%xmm6, 128(%rsp)        # 16-byte Spill
	callq	sqrt
	movapd	128(%rsp), %xmm6        # 16-byte Reload
	movapd	144(%rsp), %xmm3        # 16-byte Reload
	movapd	160(%rsp), %xmm2        # 16-byte Reload
	movapd	16(%rsp), %xmm5         # 16-byte Reload
	movsd	.LCPI1_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB1_25:                               # %.split283
                                        #   in Loop: Header=BB1_13 Depth=5
	movapd	%xmm4, %xmm0
	divsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm2, %xmm1
	mulsd	%xmm5, %xmm0
	movapd	%xmm3, 304(%rsp)
	movsd	%xmm6, 320(%rsp)
	movupd	%xmm1, 328(%rsp)
	movsd	%xmm0, 344(%rsp)
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rcx
	callq	_Z8radianceRK3RayiPt
	movapd	272(%rsp), %xmm0
	mulpd	208(%rsp), %xmm0        # 16-byte Folded Reload
	movsd	288(%rsp), %xmm1        # xmm1 = mem[0],zero
	mulsd	224(%rsp), %xmm1        # 16-byte Folded Reload
	movapd	240(%rsp), %xmm7        # 16-byte Reload
	addpd	%xmm0, %xmm7
	movapd	256(%rsp), %xmm6        # 16-byte Reload
	addsd	%xmm1, %xmm6
	decl	%r13d
	jne	.LBB1_13
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_6 Depth=4
	xorpd	%xmm7, %xmm7
	xorpd	%xmm6, %xmm6
.LBB1_26:                               # %._crit_edge
                                        #   in Loop: Header=BB1_6 Depth=4
	movsd	.LCPI1_0(%rip), %xmm5   # xmm5 = mem[0],zero
	movapd	%xmm5, %xmm1
	minsd	%xmm7, %xmm1
	movapd	%xmm7, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movapd	%xmm5, %xmm2
	minsd	%xmm0, %xmm2
	movapd	%xmm5, %xmm3
	minsd	%xmm6, %xmm3
	movsd	.LCPI1_15(%rip), %xmm4  # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm1
	xorps	%xmm8, %xmm8
	cmpltsd	%xmm8, %xmm7
	andnpd	%xmm1, %xmm7
	mulsd	%xmm4, %xmm2
	cmpltsd	%xmm8, %xmm0
	andnpd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm3
	cmpltsd	%xmm8, %xmm6
	andnpd	%xmm3, %xmm6
	movq	72(%rsp), %rax          # 8-byte Reload
	addsd	(%rax), %xmm7
	addsd	8(%rax), %xmm0
	addsd	16(%rax), %xmm6
	movsd	%xmm7, (%rax)
	movsd	%xmm0, 8(%rax)
	movsd	%xmm6, 16(%rax)
	movsd	88(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm5, %xmm2
	incl	%r14d
	cmpl	$2, %r14d
	movq	64(%rsp), %rbp          # 8-byte Reload
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jne	.LBB1_6
# BB#10:                                #   in Loop: Header=BB1_5 Depth=3
	addsd	%xmm5, %xmm0
	movl	12(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	$2, %eax
	jne	.LBB1_5
# BB#11:                                #   in Loop: Header=BB1_4 Depth=2
	movq	56(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	cmpq	$1024, %rdx             # imm = 0x400
	jne	.LBB1_4
# BB#8:                                 #   in Loop: Header=BB1_3 Depth=1
	movq	32(%rsp), %r14          # 8-byte Reload
	incq	%r14
	cmpq	$768, %r14              # imm = 0x300
	jne	.LBB1_3
# BB#9:
	xorl	%eax, %eax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	4681608360884174848     # double 1.0E+5
	.quad	4681608429603651584     # double 100001
.LCPI2_1:
	.quad	4630938906834396774     # double 40.799999999999997
	.quad	4635442506461767270     # double 81.599999999999994
.LCPI2_2:
	.quad	4604930618986332160     # double 0.75
	.quad	4598175219545276416     # double 0.25
.LCPI2_3:
	.quad	4681608360884174848     # double 1.0E+5
	.quad	-4541770479198797824    # double -99901
.LCPI2_4:
	.quad	4598175219545276416     # double 0.25
	.quad	4598175219545276416     # double 0.25
.LCPI2_5:
	.quad	4681608360884174848     # double 1.0E+5
	.quad	4632233691727265792     # double 50
.LCPI2_6:
	.quad	4630938906834396774     # double 40.799999999999997
	.quad	4681608360884174848     # double 1.0E+5
.LCPI2_7:
	.quad	4604930618986332160     # double 0.75
	.quad	4604930618986332160     # double 0.75
.LCPI2_8:
	.quad	4630938906834396774     # double 40.799999999999997
	.quad	-4541775358281646080    # double -99830
.LCPI2_9:
	.quad	4681608360884174848     # double 1.0E+5
	.quad	4635442506461767270     # double 81.599999999999994
.LCPI2_10:
	.quad	-4541769283479902618    # double -99918.399999999994
	.quad	4635442506461767270     # double 81.599999999999994
.LCPI2_11:
	.quad	4625337554797854720     # double 16.5
	.quad	4628293042053316608     # double 27
.LCPI2_12:
	.quad	4625337554797854720     # double 16.5
	.quad	4631811479262199808     # double 47
.LCPI2_13:
	.quad	4607173411600762667     # double 0.99899999999999999
	.quad	4607173411600762667     # double 0.99899999999999999
.LCPI2_14:
	.quad	4625337554797854720     # double 16.5
	.quad	4634837335261839360     # double 73
.LCPI2_15:
	.quad	4625337554797854720     # double 16.5
	.quad	4635189178982727680     # double 78
.LCPI2_16:
	.quad	4648488871632306176     # double 600
	.quad	4632233691727265792     # double 50
.LCPI2_17:
	.quad	4649204257877802353     # double 681.33000000000004
	.quad	4635442506461767270     # double 81.599999999999994
.LCPI2_18:
	.quad	4622945017495814144     # double 12
	.quad	4622945017495814144     # double 12
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_smallpt.ii,@function
_GLOBAL__sub_I_smallpt.ii:              # @_GLOBAL__sub_I_smallpt.ii
	.cfi_startproc
# BB#0:
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+05,1.000010e+05]
	movaps	%xmm0, spheres(%rip)
	movaps	.LCPI2_1(%rip), %xmm1   # xmm1 = [4.080000e+01,8.160000e+01]
	movaps	%xmm1, spheres+16(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, spheres+32(%rip)
	movq	$0, spheres+48(%rip)
	movaps	.LCPI2_2(%rip), %xmm2   # xmm2 = [7.500000e-01,2.500000e-01]
	movups	%xmm2, spheres+56(%rip)
	movabsq	$4598175219545276416, %rax # imm = 0x3FD0000000000000
	movq	%rax, spheres+72(%rip)
	movl	$0, spheres+80(%rip)
	movaps	.LCPI2_3(%rip), %xmm2   # xmm2 = [1.000000e+05,-9.990100e+04]
	movups	%xmm2, spheres+88(%rip)
	movups	%xmm1, spheres+104(%rip)
	movups	%xmm0, spheres+120(%rip)
	movq	$0, spheres+136(%rip)
	movaps	.LCPI2_4(%rip), %xmm1   # xmm1 = [2.500000e-01,2.500000e-01]
	movaps	%xmm1, spheres+144(%rip)
	movabsq	$4604930618986332160, %rax # imm = 0x3FE8000000000000
	movq	%rax, spheres+160(%rip)
	movl	$0, spheres+168(%rip)
	movaps	.LCPI2_5(%rip), %xmm1   # xmm1 = [1.000000e+05,5.000000e+01]
	movaps	%xmm1, spheres+176(%rip)
	movaps	.LCPI2_6(%rip), %xmm2   # xmm2 = [4.080000e+01,1.000000e+05]
	movaps	%xmm2, spheres+192(%rip)
	movaps	%xmm0, spheres+208(%rip)
	movq	$0, spheres+224(%rip)
	movaps	.LCPI2_7(%rip), %xmm2   # xmm2 = [7.500000e-01,7.500000e-01]
	movups	%xmm2, spheres+232(%rip)
	movq	%rax, spheres+248(%rip)
	movl	$0, spheres+256(%rip)
	movups	%xmm1, spheres+264(%rip)
	movaps	.LCPI2_8(%rip), %xmm3   # xmm3 = [4.080000e+01,-9.983000e+04]
	movups	%xmm3, spheres+280(%rip)
	movups	%xmm0, spheres+328(%rip)
	movups	%xmm0, spheres+312(%rip)
	movups	%xmm0, spheres+296(%rip)
	movl	$0, spheres+344(%rip)
	movaps	%xmm1, spheres+352(%rip)
	movaps	.LCPI2_9(%rip), %xmm3   # xmm3 = [1.000000e+05,8.160000e+01]
	movaps	%xmm3, spheres+368(%rip)
	movaps	%xmm0, spheres+384(%rip)
	movq	$0, spheres+400(%rip)
	movups	%xmm2, spheres+408(%rip)
	movq	%rax, spheres+424(%rip)
	movl	$0, spheres+432(%rip)
	movups	%xmm1, spheres+440(%rip)
	movaps	.LCPI2_10(%rip), %xmm1  # xmm1 = [-9.991840e+04,8.160000e+01]
	movups	%xmm1, spheres+456(%rip)
	movups	%xmm0, spheres+472(%rip)
	movq	$0, spheres+488(%rip)
	movaps	%xmm2, spheres+496(%rip)
	movq	%rax, spheres+512(%rip)
	movl	$0, spheres+520(%rip)
	movaps	.LCPI2_11(%rip), %xmm1  # xmm1 = [1.650000e+01,2.700000e+01]
	movaps	%xmm1, spheres+528(%rip)
	movaps	.LCPI2_12(%rip), %xmm1  # xmm1 = [1.650000e+01,4.700000e+01]
	movaps	%xmm1, spheres+544(%rip)
	movaps	%xmm0, spheres+560(%rip)
	movq	$0, spheres+576(%rip)
	movaps	.LCPI2_13(%rip), %xmm1  # xmm1 = [9.990000e-01,9.990000e-01]
	movups	%xmm1, spheres+584(%rip)
	movabsq	$4607173411600762667, %rax # imm = 0x3FEFF7CED916872B
	movq	%rax, spheres+600(%rip)
	movl	$1, spheres+608(%rip)
	movaps	.LCPI2_14(%rip), %xmm2  # xmm2 = [1.650000e+01,7.300000e+01]
	movups	%xmm2, spheres+616(%rip)
	movaps	.LCPI2_15(%rip), %xmm2  # xmm2 = [1.650000e+01,7.800000e+01]
	movups	%xmm2, spheres+632(%rip)
	movups	%xmm0, spheres+648(%rip)
	movq	$0, spheres+664(%rip)
	movaps	%xmm1, spheres+672(%rip)
	movq	%rax, spheres+688(%rip)
	movl	$2, spheres+696(%rip)
	movaps	.LCPI2_16(%rip), %xmm1  # xmm1 = [6.000000e+02,5.000000e+01]
	movaps	%xmm1, spheres+704(%rip)
	movaps	.LCPI2_17(%rip), %xmm1  # xmm1 = [6.813300e+02,8.160000e+01]
	movaps	%xmm1, spheres+720(%rip)
	movaps	.LCPI2_18(%rip), %xmm1  # xmm1 = [1.200000e+01,1.200000e+01]
	movaps	%xmm1, spheres+736(%rip)
	movabsq	$4622945017495814144, %rax # imm = 0x4028000000000000
	movq	%rax, spheres+752(%rip)
	movups	%xmm0, spheres+772(%rip)
	movups	%xmm0, spheres+760(%rip)
	retq
.Lfunc_end2:
	.size	_GLOBAL__sub_I_smallpt.ii, .Lfunc_end2-_GLOBAL__sub_I_smallpt.ii
	.cfi_endproc

	.type	spheres,@object         # @spheres
	.bss
	.globl	spheres
	.p2align	4
spheres:
	.zero	792
	.size	spheres, 792

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Rendering (%d spp)\n"
	.size	.L.str, 20

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_smallpt.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
