	.text
	.file	"util.bc"
	.globl	init_random
	.p2align	4, 0x90
	.type	init_random,@function
init_random:                            # @init_random
	.cfi_startproc
# BB#0:
	movslq	%edi, %rdi
	jmp	srand48                 # TAILCALL
.Lfunc_end0:
	.size	init_random, .Lfunc_end0-init_random
	.cfi_endproc

	.globl	gen_number
	.p2align	4, 0x90
	.type	gen_number,@function
gen_number:                             # @gen_number
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	callq	lrand48
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	%ebx
	movl	%edx, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	gen_number, .Lfunc_end1-gen_number
	.cfi_endproc

	.globl	gen_signed_number
	.p2align	4, 0x90
	.type	gen_signed_number,@function
gen_signed_number:                      # @gen_signed_number
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	callq	lrand48
	leal	(%rbx,%rbx), %ecx
	movslq	%ecx, %rcx
	cqto
	idivq	%rcx
	subl	%ebx, %edx
	movl	%edx, %eax
	shrl	$31, %eax
	xorl	$1, %eax
	leal	(%rax,%rdx), %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	gen_signed_number, .Lfunc_end2-gen_signed_number
	.cfi_endproc

	.globl	gen_uniform_double
	.p2align	4, 0x90
	.type	gen_uniform_double,@function
gen_uniform_double:                     # @gen_uniform_double
	.cfi_startproc
# BB#0:
	jmp	drand48                 # TAILCALL
.Lfunc_end3:
	.size	gen_uniform_double, .Lfunc_end3-gen_uniform_double
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4636737291354636288     # double 100
	.text
	.globl	check_percent
	.p2align	4, 0x90
	.type	check_percent,@function
check_percent:                          # @check_percent
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	callq	drand48
	cvtsi2sdl	%ebx, %xmm1
	incl	percentcheck(%rip)
	divsd	.LCPI4_0(%rip), %xmm1
	xorl	%eax, %eax
	ucomisd	%xmm0, %xmm1
	seta	%al
	jbe	.LBB4_2
# BB#1:
	incl	numlocal(%rip)
.LBB4_2:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	check_percent, .Lfunc_end4-check_percent
	.cfi_endproc

	.globl	printstats
	.p2align	4, 0x90
	.type	printstats,@function
printstats:                             # @printstats
	.cfi_startproc
# BB#0:
	movl	percentcheck(%rip), %esi
	movl	numlocal(%rip), %edx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.Lfunc_end5:
	.size	printstats, .Lfunc_end5-printstats
	.cfi_endproc

	.type	percentcheck,@object    # @percentcheck
	.local	percentcheck
	.comm	percentcheck,4,4
	.type	numlocal,@object        # @numlocal
	.local	numlocal
	.comm	numlocal,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"percentcheck=%d,numlocal=%d\n"
	.size	.L.str, 29


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
